//#region Variables
var TIPOCAMPOGS = {
    USUARIO: -2,
    PETICIONARIO: -1,
    DESCBREVE: 1,
    DESCRDETALLADA: 2,
    IMPORTE: 3,
    CANTIDAD: 4,
    FECNECESIDAD: 5,
    INISUMINISTRO: 6,
    FINSUMINISTRO: 7,
    ARCHIVOESPECIFIC: 8,
    PRECIOUNITARIO: 9,
    PRECIOUNITARIOADJ: 10,
    PROVEEDORADJ: 11,
    CANTIDADADJ: 12,
    TOTALLINEAADJ: 13,
    TOTALLINEAPREADJ: 14,
    ESTADOINTERNONOCONF: 43,
    IMPORTEREPERCUTIDO: 45,
    PROVEEDOR: 100,
    FORMAPAGO: 101,
    MONEDA: 102,
    MATERIAL: 103,
    CODARTICULO: 104,
    UNIDAD: 105,
    DESGLOSE: 106,
    PAIS: 107,
    PROVINCIA: 108,
    DEST: 109,
    PRES1: 110,
    PRES2: 111,
    PRES3: 112,
    PRES4: 113,
    CONTACTO: 114,
    PERSONA: 115,
    PROVECONTACTO: 116,
    ROL: 117,
    DENARTICULO: 118,
    NUEVOCODARTICULO: 119,
    NUMSOLICTERP: 120,
    UNIDADORGANIZATIVA: 121,
    DEPARTAMENTO: 122,
    ORGANIZACIONCOMPRAS: 123,
    CENTRO: 124,
    ALMACEN: 125,
    LISTADOSPERSONALIZADOS: 126,
    IMPORTESOLICITUDESVINCULADAS: 127,
    REFSOLICITUD: 128,
    CENTROCOSTE: 129,
    PARTIDA: 130,
    ACTIVO: 131,
    TIPOPEDIDO: 132,
    DESGLOSEACTIVIDAD: 136,
    UNIDADPEDIDO: 142,
    PROVEEDORERP: 143,
    COMPRADOR: 144,
    EMPRESA: 149,
    ANYOPARTIDA: 151,
    LINEA: 3000
};
var SUBTIPO = {
    NUMERO: 2,
    FECHA: 3,
    BOOLEAN: 4,
    TEXTO_CORTO: 5,
    TEXTO_MEDIO: 6,
    TEXTO_LARGO: 7,
    ARCHIVO: 8,
    DESGLOSE: 9,
    EDITOR: 15
};
var sIdEntryMaterial;
var oGridsDesglose = new Array();
var arrEstadosInternos = new Array();
var arrFechasSuministros = new Array();
var arrGSOcultos = new Array();
var oWinDesglose;
var sDataEntryMaterialFORM;
var sContextKey;
var sAtribsListaExtContextKey;
var popupListaExternaCargado = false;
var idAtributoListaExterna, atributos;
var OrgCompras = "";
var UON = "";
var CodProveedor = "";
var oEditAtribListaExt;
//#endregion
$(document).ready(function () {
    $(window).keydown(function (event) {
        if (event.keyCode == 13 & !$(event.srcElement).is('textarea')) {
            event.preventDefault();
            return false;
        }
    });
});
/*  Revisado por: blp. Fecha: 03/10/2011
''' <summary>
''' Cargar el popup con las opciones de copiar fila, eliminar fila y mover fila.
''' </summary>
''' <param name="relativeElement">this, es decir, el control desde el que se lanza la llamada a la funci�n</param>
''' <param name="text0">this.parentElement, es decir, el desglose</param>
''' <param name="text1">Me.ClientID, es decir, pantalla</param>        
''' <param name="text2">linea</param>
''' <param name="text3">idCampo.ToString(), es decir, id del desglose</param>        
''' <param name="tipo">Donde esta ubicado el boton a la izq o la derecha</param>
''' <param name="habilitaCopiar">si se permitira copiar filas</param>        
''' <param name="habilitarMover">si se permitira mover filas</param>     
''' <param name="solicitud">si se permitira mover filas, indica a q solicitud se puede mover</param>  
''' <param name="instancia">si se permitira mover filas, indica de q instancia se va a mover</param> 
''' <param name="PopUp">Indica si es Popup o no</param>   
''' <remarks>Llamada desde: alta/desglose.ascx/page_load; Tiempo m�ximo:0</remarks>*/
function popupDesgloseClickEvent(relativeElement, text0, text1, text2, text3, tipo, habilitaCopiar, habilitarMover, solicitud, instancia, PopUp, event) {
    var urlPopupDesglose;
    var posicion = [0, 0];
    posicion = showPosition(relativeElement, null, 136, 24, 10);
    var x = posicion[0];
    var y = posicion[1];

    var MiHabilitarMover;
    MiHabilitarMover = 0;

    if (habilitarMover == '##Si##') {
        if (PopUp == 0) {
            if (htControlArrVincMovible[text3 + '_' + text2] == "##Si##") MiHabilitarMover = 1;
        };
        if (PopUp == 1) {
            var p = window.opener;
            var sPre = document.getElementById("INPUTDESGLOSE").value;
            var numTotRows = p.document.getElementById(sPre + "__numRowsVinc").value;
            var i = 0;
            for (k = 1; k <= numTotRows; k++) {
                var oLineaV = p.document.getElementById(sPre + "__" + k.toString() + "__LineaVinc");
                if (oLineaV) {
                    if (oLineaV.value > "0") {
                        i++;
                        if (i == text2) {
                            if (p.document.getElementById(sPre + "__" + k.toString() + "__LineaVincMovible")) {
                                if (p.document.getElementById(sPre + "__" + k.toString() + "__LineaVincMovible").value == "##Si##") MiHabilitarMover = 1;
                            };
                            break;
                        };
                    }
                    oLineaV = null;
                };
            };
        };
    };

    if ((habilitaCopiar > 0) && (MiHabilitarMover == 0)) {
        mostrarFRAME("ddpopupDesglose", x, y, 136, 24);
        urlPopupDesglose = "../_common/popupDesglose_Delete.aspx?FrameId=ddpopupDesglose&celda=" + text0.id + "&ClientID=" + text1 + "&index=" + text2 + "&idCampo=" + text3;
    } else {
        if ((habilitaCopiar == 0) && (MiHabilitarMover == 0)) {
            mostrarFRAME("ddpopupDesglose", x, y, 136, 46);
            urlPopupDesglose = "../_common/popupDesglose.aspx?FrameId=ddpopupDesglose&celda=" + text0.id + "&ClientID=" + text1 + "&index=" + text2 + "&idCampo=" + text3;
        } else {
            if ((habilitaCopiar > 0) && (MiHabilitarMover == 1)) {
                mostrarFRAME("ddpopupDesglose", x, y, 136, 46);
                urlPopupDesglose = "../_common/popupDesglose_Delete_Mover.aspx?FrameId=ddpopupDesglose&celda=" + text0.id + "&ClientID=" + text1 + "&index=" + text2 + "&idCampo=" + text3 + "&solicitud=" + solicitud + "&instancia=" + instancia + "&PopUp=" + PopUp;
            } else {
                mostrarFRAME("ddpopupDesglose", x, y, 136, 68);
                urlPopupDesglose = "../_common/popupDesglose_Mover.aspx?FrameId=ddpopupDesglose&celda=" + text0.id + "&ClientID=" + text1 + "&index=" + text2 + "&idCampo=" + text3 + "&solicitud=" + solicitud + "&instancia=" + instancia + "&PopUp=" + PopUp;
            };
        };
    };

    if (document.getElementById("ddpopupDesglose") != undefined) document.getElementById("ddpopupDesglose").src = urlPopupDesglose;
};
/*''' <summary>
''' Para desgloses vinculados a�adir linea significa lanzar el buscador de solictudes. Y el propio buscador se ocupara de a�adir lo q sea 
''' seleccionado en su momento
''' </summary>
''' <param name="sRoot">nombre entry del desglose/param>
''' <param name="idCampo">id de bbdd del desglose</param>  
''' <param name="Instancia">Es 0-> solictud->idCampo de desglose_vinculado
'''                         No 0-> instancia->idCampo de copia_desglose_vinculado</param>  
''' <remarks>Llamada desde:javascript introducido para responder al onclick de todo boton 'a�adir fila' de los desglose vinculados
''' ; Tiempo m�ximo:0</remarks>*/
function copiarFilaVinculada(sRoot, idCampo, Instancia) {
    var newWindow = window.open(rutaFS + "_common/BuscadorSolicitudes.aspx?sRoot=" + sRoot + "&IdCampo=" + idCampo + "&Instancia=" + Instancia, "_blank", "width=910,height=500,status=yes,resizable=no,top=100,left=100");
    newWindow.focus();
};
/*  Revisado por: blp. Fecha: 05/10/2011
''' <summary>
''' A�ade una fila a un desglose. Si hay linea por defecto la linea nueva tiene los datos por defecto 
''' rellenos, sino la linea va en blanco. Para Qa, el estado interno de la fila nueva es 1 (sin revisar) y
''' deben ver los botones de aceptar y rechazar. Si la columna es readonly no se crea dataentry solo una label
''  con un boton tres puntos si tiene texto, si no tiene texto no debe ir el bot�n. 
''' </summary>
''' <param name="sRoot">nombre entry del desglose/param>
''' <param name="idCampo">id de bbdd del desglose</param>        
''' <param name="EsFavorita">Si se esta con favoritos o no</param>
''' <param name="Row">UltragridRow de la linea vinculada</param> 
''' <param name="sId">Id de la instancia vinculada</param>
''' <param name="sDen">Descrip de la instancia vinculada</param> 
''' <remarks>Llamada desde:javascript introducido para responder al onclick de todo boton 'a�adir fila' de desgloses no vinculados
''' ; Tiempo m�ximo:0</remarks>*/
function copiarFilaVacia(sRoot, idCampo, EsFavorita, Row, sId, sDen) {
    var bAlta = false;
    p = window.opener;
    if (p)
        if (p.document.forms["frmAlta"] != undefined) bAlta = true;
        else
            if (document.forms["frmAlta"] != undefined) bAlta = true;

    var bMensajePorMostrar = document.getElementById("bMensajePorMostrar");
    if (bMensajePorMostrar) {
        if (bMensajePorMostrar.value == "1") {
            bMensajePorMostrar.value = "0";
            return;
        };
    };
    bMensajePorMostrar = null;

    var s;
    //uwtGrupos__ctl0__ctl0
    var sClase = document.getElementById(sRoot + "_tblDesglose").rows[document.getElementById(sRoot + "_tblDesglose").rows.length - 1].cells[0].className;
    if (sClase == "ugfilatablaDesglose") sClase = "ugfilaalttablaDesglose";
    else sClase = "ugfilatablaDesglose";

    var oTR = document.getElementById(sRoot + "_tblDesglose").insertRow(-1);
    var lIndex;
    if (document.getElementById(sRoot + "_numRows")) {
        lIndex = parseFloat(document.getElementById(sRoot + "_numRows").value) + 1;
        document.getElementById(sRoot + "_numRows").value = lIndex;
    } else {
        lIndex = document.getElementById(sRoot + "_tblDesglose").rows.length - 1
        s = "<input type=hidden name=" + sRoot + "_numRows id=" + sRoot + "_numRows value=" + lIndex.toString() + ">";
        document.getElementById("divAlta").insertAdjacentHTML("beforeEnd", s);
    };
    var lLineaGrid;
    if (document.getElementById(sRoot + "_numRowsGrid")) {
        lLineaGrid = parseFloat(document.getElementById(sRoot + "_numRowsGrid").value) + 1;
        document.getElementById(sRoot + "_numRowsGrid").value = lLineaGrid;
        if ((!bAlta) || (bAlta && p)) lLineaGrid = lIndex;
    } else {
        lLineaGrid = document.getElementById(sRoot + "_tblDesglose").rows.length - 1;
        s = "<input type=hidden name=" + sRoot + "_numRowsGrid id=" + sRoot + "_numRowsGrid value=" + lLineaGrid.toString() + ">";
        document.getElementById("divAlta").insertAdjacentHTML("beforeEnd", s);
    };

    var pp = window.parent;
    var Salta = 0;
    if (pp) {
        if (window.parent.location.toString().search("altacertificado.aspx") != -1) {
            Salta = 1;
        };
    };
    pp = null;
    var pv = window.opener;
    if (pv) {
        if (Salta == 0) {
            var oFrm = pv.document.forms["frmDesglose"];
            if (oFrm) {
                if (document.getElementById("INPUTDESGLOSE")) {
                    var oRowVinc = pv.document.getElementById(document.getElementById("INPUTDESGLOSE").value + "__numRowsVinc");
                    if (oRowVinc) {
                        var lLineaRowsVinc = parseFloat(oRowVinc.value) + 1;
                        oRowVinc.value = lLineaRowsVinc;
                        lLineaRowsVinc = null;
                    };
                    oRowVinc = null;
                };
            };
            oFrm = null;
        };
    };
    pv = null;

    //Controlar las filas que se a�aden para optimizar MontarFormularioSubmit
    var ultimaFila = 0
    var filaNum;
    for (var indice in htControlFilas) {
        if (String(idCampo) == indice.substring(0, indice.indexOf("_"))) {
            filaNum = indice.substr(indice.lastIndexOf("_") + 1);
            if (parseInt(ultimaFila) < parseInt(filaNum)) {
                if (htControlFilas[String(idCampo) + "_" + String(filaNum)] != 0) {
                    ultimaFila = htControlFilas[String(idCampo) + "_" + String(filaNum)];
                }
            }
        };
    };
    htControlFilas[String(idCampo) + "_" + String(lIndex)] = String(parseInt(ultimaFila) + 1);
    ultimaFila = null;
    htControlFilasVinc[String(idCampo) + "_" + String(lIndex)] = String(0);
    var sMovible;
    sMovible = String(idCampo) + "_" + String(lIndex);
    htControlArrVincMovible[sMovible] = "##No##";

    //A continuacion se obtiene lPrimeraLinea, que es la fila de donde se copian los datos
    var lPrimeraLinea;
    if (lLineaGrid == 1) {
        lPrimeraLinea = lIndex;
        var js = "<input type=hidden name=" + sRoot + "_numPrimeraLinea id=" + sRoot + "_numPrimeraLinea value=" + lIndex.toString() + ">";
        document.forms[0].insertAdjacentHTML("beforeEnd", js);
        js = null;
    } else {
        if (document.getElementById(sRoot + "_numPrimeraLinea")) {
            lPrimeraLinea = document.getElementById(sRoot + "_numPrimeraLinea").value;
        } else {
            lPrimeraLinea = 1;
        };
    };

    lPrimeraLinea = parseInt(lPrimeraLinea.toString(), 10);
    //En este array se guardan en cada fila el nuevo data entry y del que se va acoger el valor (_1_)
    var arraynuevafila = new Array();
    var arrayEntrysReadOnlyConDefecto = new Array();
    var insertarnuevoelemento = 0;
    var VecesEntre = 0;

    for (var i = 0; i < document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells.length; i++) {
        arraynuevafila[i] = new Array();
        arrayEntrysReadOnlyConDefecto[i] = "";
        for (var j = 0; j < 2; j++) {
            arraynuevafila[i][j] = "";
        };
    };

    for (i = 0; i < document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells.length; i++) {
        s = document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells[i].innerHTML;
        var oTD = oTR.insertCell(-1);
        oTD.className = sClase;
        if (document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells[i].id) {
            oTD.id = document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells[i].id + '_' + lIndex;
        };
        var entryActual;
        var stemp = s;
        var entryReadOnlyConDefecto = false;
        var stemp2;
        var tempinicio;
        if (stemp.search(' id="') > 0) { //En FireFox los ids vienen con comillas
            tempinicio = stemp.search(' id="');
            tempinicio = tempinicio + 5;
        } else {
            tempinicio = stemp.search(" id="); //En IE los ids vienen sin comillas
            tempinicio = tempinicio + 4;
        };
        stemp2 = stemp.substr(tempinicio, stemp.length - tempinicio);
        var tempfin = stemp2.search("__tbl");
        entryActual = stemp2.substr(0, tempfin);
        ///////////////////
        var re = "class=trasparent";
        var re2 = /class="trasparent"/;
        if ((s.search(re) >= 0) || (s.search(re2) >= 0)) {
            var den = '';
            var valor = '';
            var inicio = s.search(sRoot + "_fsentry");
            if (inicio > 1) {
                var cadena = s.substr(inicio + sRoot.length + 8);
                var fin = cadena.indexOf("_");
                cadena = cadena.substring(0, fin);
                if (document.getElementById(sRoot + "_fsdsentry_" + lPrimeraLinea + "_" + cadena)) {
                    etiqueta = document.getElementById(sRoot + "_fsdsentry_" + lPrimeraLinea + "_" + cadena).innerHTML;
                    inicio = etiqueta.indexOf("<SPAN>") + 6;
                    if (inicio < 6) {
                        inicio = etiqueta.indexOf("<span>") + 6;
                    };
                    if (inicio < 6) { //no existe <SPAN> sino <SPAN class="TipoArchivo">
                        inicio = etiqueta.indexOf("<SPAN");
                        fin = etiqueta.indexOf(">", inicio);
                        inicio = inicio + (fin - inicio) + 1; //etiqueta.indexOf("<SPAN ") + 24
                    };
                    fin = etiqueta.indexOf("</SPAN>");
                    if (fin < 0) {
                        fin = etiqueta.indexOf("</span>");
                    };
                    den = etiqueta.substring(inicio, fin);
                    etiqueta = null;
                    if (den == '&nbsp;') {
                        den = '';
                        s = s.replace("<IMG src='" + rutaPM + "_common/images/trespuntos.gif' align=middle border=0 unselectable=\"on\">", "");
                        s = s.replace("BACKGROUND-IMAGE: url(" + rutaPM + "_common/images/trespuntos.gif);", "display: none;");
                    };
                }
                if (document.getElementById(sRoot + "_" + lPrimeraLinea + "_" + cadena)) {
                    valor = document.getElementById(sRoot + "_" + lPrimeraLinea + "_" + cadena).value;
                };
            };

            if (valor != '') {
                if (s.search("__h type=hidden") >= 0) {
                    s = s.replace("__h type=hidden", "__h type='hidden' value='" + valor + "'");
                };
                if (s.search("__hAct type=hidden") >= 0) {
                    s = s.replace("__hAct type=hidden", "__hAct type='hidden' value='" + valor + "'");
                }; //para tipo archivo    
                if (s.search("__hNew type=hidden") >= 0) {
                    s = s.replace("__hNew type=hidden", "__hNew type='hidden'");
                }; //para tipo archivo    
                if (s.search("__h\" type=\"hidden\"") >= 0) {
                    s = s.replace("__h\" type=\"hidden\"", "__h\" type=\"hidden\" value='" + valor + "'");
                };
            };
            if (den != '') {
                if (s.search("</TEXTAREA") >= 0) {
                    s = s.replace("</TEXTAREA", den + "</TEXTAREA");
                } else {
                    s = s.replace("readOnly", "readOnly value='" + den + "'");
                };
                while (s.search("type=hidden") >= 0) {
                    s = s.replace("type=hidden", "type='hidden' value='" + den + "'");
                };
                while (s.indexOf('[\"\"') >= 0) {
                    s = s.replace('[\"\"', '[\"' + den + '\"');
                };
            };
        };
        den = null;
        valor = null;
        inicio = null;
        cadena = null;
        ///////////////////	
        re = /fsentry/;
        while (s.search(re) >= 0) {
            s = s.replace(re, "fsdsentry_" + lIndex.toString() + "_")
        };
        //Para evitar generar los contenidos de los webdropdown con los mismos IDs y a fin de que podamos localizar esos datos con m�s facilidad, vamos a editar los IDs
        var webdropdownReplace = '';
        var finID = s.search(".0:mkr:Target");
        if (finID > -1) {
            var extraer = s.substr(0, finID);
            var inicioID = extraer.lastIndexOf("x:");
            if (inicioID > -1) {
                extraer = extraer.substr(inicioID + 2, finID);
                webdropdownReplace = extraer;
                extraer = null;
            };
            inicioID = null;
        };
        finID = null;
        if (webdropdownReplace != '') {
            var numAleatorio = Aleatorio(1, parseInt(webdropdownReplace));
            re = new RegExp(); // empty constructor
            re.compile(webdropdownReplace, 'gi');
            s = s.replace(re, parseInt(webdropdownReplace) + parseInt(numAleatorio));
            //Reemplazar la clase seleccionada para que no se seleccione nada por defecto al copiar l�nea vac�a
            re.compile('igdd_FullstepListItemSelected', 'gi');
            s = s.replace(re, parseInt(webdropdownReplace) + parseInt(numAleatorio));
            numAleatorio = null;
        };
        webdropdownReplace = null;
        var se = /<SCRIPT language=javascript>/;
        var se2 = /<script language="javascript">/;
        var arrJS = new Array();
        var ni = /##newIndex##/g;
        if ((s.search(se) > 0) || (s.search(se2) > 0)) {
            while (s.search(se) > 0) {
                var iComienzo = s.search(se) + 28;
                se = /<\/SCRIPT>/;
                var iFin = s.search(se);
                var sEval = s.substr(iComienzo, iFin - iComienzo);
                s = s.substr(0, iComienzo - 28) + s.substr(iFin + 9);
                arrJS[arrJS.length] = sEval;
                sEval = null;
                se = /<SCRIPT language=javascript>/;
            };
            while (s.search(se2) > 0) {
                var iComienzo = s.search(se2) + 30;
                se = /<\/script>/;
                var iFin = s.search(se);
                var sEval = s.substr(iComienzo, iFin - iComienzo);
                s = s.substr(0, iComienzo - 30) + s.substr(iFin + 9);
                arrJS[arrJS.length] = sEval;
                sEval = null;
                se2 = /<script language="javascript">/;
            };
            if (s.search(ni) > 0) {
                oTD.align = "center";
            };
            s = replaceAll(s, "##newIndex##", lIndex.toString());
            oTD.insertAdjacentHTML("beforeEnd", s);

            for (var k = 0; k < arrJS.length; k++) {
                eval(arrJS[k]);
            };
        } else {
            if (s.search(ni) > 0) {
                oTD.align = "center";
            };
            ni = "_cmdRechazarAccion";
            if (s.search(ni) > 0) {
                s = s.replace("_cmdRechazarAccion", "_cmdRechazarAccion_'##newIndex##'");
            };
            ni = "_cmdAprobarAccion";
            if (s.search(ni) > 0) {
                s = s.replace("_cmdAprobarAccion", "_cmdAprobarAccion_'##newIndex##'");
            };
            s = replaceAll(s, "'##newIndex##'", lIndex.toString());

            re = /'##Si##'/
            if (s.search(re) > 0) {
                htControlArrVincMovible[sMovible] = "##Nueva##";
            };
            oTD.insertAdjacentHTML("beforeEnd", s);
        };
        se = null;
        se2 = null;
        iComienzo = null;
        iFin = null;
        arrJS = null;
        ni = null;
        //Aqui se agrega el AutoCompleteExtender al control si tiene la propiedad RecordarValores a True
        var oContr;
        oContr = fsGeneralEntry_getById(entryActual);
        if (oContr) {
            if (oContr.tipo == 8 && oContr.CampoOrigenVinculado != null) {
                var entryArchivo = entryActual;
                re = /fsentry/;
                while (entryArchivo.search(re) >= 0) {
                    entryArchivo = entryArchivo.replace(re, "fsdsentry_" + lIndex.toString() + "_");
                };
                var Ids = sacarValorCelda("VALOR_" + oContr.CampoOrigenVinculado, Row);
                var Textos = sacarValorCelda("VALORMOSTRAR_" + oContr.CampoOrigenVinculado, Row);
                var div;
                if (Ids != null) {
                    var TextosCortado = Textos
                    if (Textos.length > 21) {
                        TextosCortado = TextosCortado.substr(0, 21) + '...';
                    };
                    var IdInstancia = 0;
                    if (document.getElementById("Instancia")) IdInstancia = document.getElementById("Instancia").value;
                    if (Ids.search("xx") > 0) {
                        var IdsX = replaceAll(Ids, "xx", ",4xx") + ',4';
                        var IdsComa = replaceAll(Ids, "xx", ",");

                        div = document.getElementById(entryArchivo + "_divcontenedor");
                        div.innerHTML = '<table cellpadding="0" cellspacing="0" border="0" style="display:inline;table-layout:fixed;"><tr><td width="150px" align="right"><a id="' + entryArchivo + '__t" onclick="descargarAdjuntos(\'' + IdsComa + '\',\'\', \'0\', \'' + IdsX + '\',\'' + entryArchivo + '\')" text-align="left" style="cursor:hand;" class="enlaceAdj">' + TextosCortado + '</a></td><td align="right" width="70px"><a style="cursor:hand;text-decoration:underline;padding-left:7px;color:#0000FF;" class="enlaceAdj" onclick="show_atach_fileDesglose(\'' + IdInstancia + '\', \'\',\'' + Ids + '\', \'3\', \'' + entryArchivo + '\',0)">Detalle</a></td></tr></table>';
                        IdsX = null;
                        IdsComa = null;
                    } else {
                        div = document.getElementById(entryArchivo + "_divcontenedor");
                        div.innerHTML = '<table cellpadding="0" cellspacing="0" border="0" style="display:inline;table-layout:fixed;"><tr><td width="150px" align="right"><a id="' + entryArchivo + '__t" onclick="descargarAdjunto(\'' + Ids + '\', \'4\', \'0\',\'' + entryArchivo + '\')" text-align="left" style="cursor:hand;" class="enlaceAdj">' + TextosCortado + '</a></td><td align="right" width="70px"><a style="cursor:hand;text-decoration:underline;padding-left:7px;color:#0000FF;" class="enlaceAdj" onclick="show_atach_fileDesglose(\'' + IdInstancia + '\', \'\',\'' + Ids + '\', \'3\', \'' + entryArchivo + '\',0)">Detalle</a></td></tr></table>';
                    };
                } else {
                    div = document.getElementById(entryArchivo + "_divcontenedor");
                    div.innerHTML = '<table cellpadding="0" cellspacing="0" border="0" style="display:inline; width:100%;"><tr><td width="100%" align="right"><a id="' + entryArchivo + '__t" style="DISPLAY: none;"></a></td></tr></table>';
                };
                entryArchivo = null;
                Ids = null;
                TextosCortado = null;
                IdInstancia = null;
                div = null;
            };
        };
        var campo;
        var a = new Array();
        a = entryActual.split("_");
        //Si es un desglose que esta en la propia pagina          
        if (a.length >= 6) {
            if (oContr.tipoGS == 118) {
                entryActual = a[0] + '__' + a[2] + '_' + a[3] + '_' + a[4] + '_' + a[5] + '_' + a[6] + '__tden';
            } else {
                entryActual = a[0] + '__' + a[2] + '_' + a[3] + '_' + a[4] + '_' + a[5] + '_' + a[6] + '__t';
            }
            re = /fsentry/;
            while (entryActual.search(re) >= 0) {
                entryActual = entryActual.replace(re, "fsdsentry_" + lIndex.toString() + "_");
            };
            campo = a[6];
            campo = campo.replace(re, "");
            if (oContr) {
                if (oContr.RecordarValores == true) {
                    if (oContr.campo_origen != 0) {
                        campo = oContr.campo_origen;
                    };
                    Sys.Application.add_init(function () {
                        $create(Sys.Extended.UI.AutoCompleteBehavior, {
                            "completionInterval": 500,
                            "completionListCssClass": "autoCompleteList",
                            "completionListItemCssClass": "autoCompleteListItem",
                            "contextKey": campo,
                            "delimiterCharacters": "",
                            "enableCaching": false,
                            "highlightedItemCssClass": "autoCompleteSelectedListItem",
                            "id": "autocompleteextender_" + lIndex.toString() + "_" + campo.toString(),
                            "minimumPrefixLength": 0,
                            "serviceMethod": "DevolverValores",
                            "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                            "useContextKey": true
                        }, {
                            "shown": resetPosition
                        }, null, $get(entryActual));
                    });
                }
                else if (oContr.tipoGS == 118 || oContr.tipoGS == 119) {
                    if (oContr.tipoGS == 119) {    //C�digo art�culo
                        Sys.Application.add_init(function () {
                            $create(Sys.Extended.UI.AutoCompleteBehavior, {
                                "completionInterval": 500,
                                "completionListCssClass": "autoCompleteList",
                                "completionListItemCssClass": "autoCompleteListItem",
                                "enabled": true,
                                "minimumPrefixLength": 3,
                                "delimiterCharacters": "",
                                "enableCaching": false,
                                "completionListHighlightedItemCssClass": "autoCompleteSelectedListItem",
                                "id": entryActual + "_ext",
                                "serviceMethod": "DevolverArticulos",
                                "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                                "useContextKey": true,
                                "completionSetCount": oContr.FilasAutoCompleteExtender,
                                "targetControlID": entryActual + "_t"
                            }, {
                                "populated": onCodListPopulated,
                                "shown": resetPosition
                            }, null, $get(entryActual + "_t"));
                        });
                    }
                    if (oContr.tipoGS == 118) { //Denominaci�n art�culo
                        Sys.Application.add_init(function () {
                            $create(Sys.Extended.UI.AutoCompleteBehavior, {
                                "completionInterval": 500,
                                "completionListCssClass": "autoCompleteList",
                                "completionListItemCssClass": "autoCompleteListItem",
                                "enabled": true,
                                "minimumPrefixLength": 3,
                                "delimiterCharacters": "",
                                "enableCaching": false,
                                "completionListHighlightedItemCssClass": "autoCompleteSelectedListItem",
                                "id": entryActual + "_ext",
                                "serviceMethod": "DevolverArticulos",
                                "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                                "useContextKey": true,
                                "completionSetCount": oContr.FilasAutoCompleteExtender,
                                "targetControlID": entryActual + "_t"
                            }, {
                                "itemSelected": onDenExtenderItemSelected,
                                "populated": onCodListPopulated,
                                "shown": resetPosition
                            }, null, $get(entryActual + "_t"));
                        });
                    }
                } else if (oContr.tipoGS == 0 && oContr.intro == 2) {    //Atributos de lista externa                    
                    Sys.Application.add_init(function () {
                        $create(Sys.Extended.UI.AutoCompleteBehavior, {
                            "completionInterval": 500,
                            "completionListCssClass": "autoCompleteList",
                            "completionListItemCssClass": "autoCompleteListItem",
                            "delimiterCharacters": "",
                            "enableCaching": false,
                            "highlightedItemCssClass": "autoCompleteSelectedListItem",
                            "id": entryActual + "_ext",
                            "minimumPrefixLength": 2,
                            "serviceMethod": "Obtener_Datos_ListaExterna_Extender",
                            "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                            "useContextKey": true,
                            "completionSetCount": oContr.FilasAutoCompleteExtender
                        }, {
                            "itemSelected": onListaExternaExtenderItemSelected,
                            "populated": onCodListPopulated,
                            "shown": resetPosition
                        }, null, $get(entryActual + "_t"));
                    });
                };
            };
        };
        //Si es un desglose que se abre en PopUp
        if (a[0] == "ucDesglose") {
            if (oContr.tipoGS == 118) {
                entryActual = a[0] + '_' + a[1] + '__tden';
            }
            else {
                entryActual = a[0] + '_' + a[1] + '__t';
            }

            re = /fsentry/;
            while (entryActual.search(re) >= 0) {
                entryActual = entryActual.replace(re, "fsdsentry_" + lIndex.toString() + "_");
            };
            campo = a[1];
            campo = campo.replace(re, "");
            if (oContr) {
                if (oContr.RecordarValores == true) {
                    if (oContr.campo_origen != 0) {
                        campo = oContr.campo_origen;
                    };
                    Sys.Application.add_init(function () {
                        $create(Sys.Extended.UI.AutoCompleteBehavior, {
                            "completionInterval": 500,
                            "completionListCssClass": "autoCompleteList",
                            "completionListItemCssClass": "autoCompleteListItem",
                            "contextKey": campo,
                            "delimiterCharacters": "",
                            "enableCaching": false,
                            "highlightedItemCssClass": "autoCompleteSelectedListItem",
                            "id": "autocompleteextender_" + lIndex.toString() + "_" + campo.toString(),
                            "minimumPrefixLength": 0,
                            "serviceMethod": "DevolverValores",
                            "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                            "useContextKey": true
                        }, {
                            "shown": resetPosition
                        }, null, $get(entryActual));
                    });
                }
                else if (oContr.tipoGS == 118 || oContr.tipoGS == 119) {
                    if (oContr.tipoGS == 119) {    //C�digo art�culo
                        Sys.Application.add_init(function () {
                            $create(Sys.Extended.UI.AutoCompleteBehavior, {
                                "completionInterval": 500,
                                "completionListCssClass": "autoCompleteList",
                                "completionListItemCssClass": "autoCompleteListItem",
                                "enabled": true,
                                "minimumPrefixLength": 3,
                                "delimiterCharacters": "",
                                "enableCaching": false,
                                "completionListHighlightedItemCssClass": "autoCompleteSelectedListItem",
                                "id": entryActual + "_ext",
                                "serviceMethod": "DevolverArticulos",
                                "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                                "useContextKey": true,
                                "completionSetCount": oContr.FilasAutoCompleteExtender,
                                "targetControlID": entryActual + "_t"
                            }, {
                                "populated": onCodListPopulated,
                                "shown": resetPosition
                            }, null, $get(entryActual + "_t"));
                        });
                    }
                    if (oContr.tipoGS == 118) { //Denominaci�n art�culo
                        Sys.Application.add_init(function () {
                            $create(Sys.Extended.UI.AutoCompleteBehavior, {
                                "completionInterval": 500,
                                "completionListCssClass": "autoCompleteList",
                                "completionListItemCssClass": "autoCompleteListItem",
                                "enabled": true,
                                "minimumPrefixLength": 3,
                                "delimiterCharacters": "",
                                "enableCaching": false,
                                "completionListHighlightedItemCssClass": "autoCompleteSelectedListItem",
                                "id": entryActual + "_ext",
                                "serviceMethod": "DevolverArticulos",
                                "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                                "useContextKey": true,
                                "completionSetCount": oContr.FilasAutoCompleteExtender,
                                "targetControlID": entryActual + "_t"
                            }, {
                                "itemSelected": onDenExtenderItemSelected,
                                "populated": onCodListPopulated,
                                "shown": resetPosition
                            }, null, $get(entryActual + "_t"));
                        });
                    }
                } else if (oContr.tipoGS == 0 && oContr.intro == 2) {    //Atributos de lista externa                    
                    Sys.Application.add_init(function () {
                        $create(Sys.Extended.UI.AutoCompleteBehavior, {
                            "completionInterval": 500,
                            "completionListCssClass": "autoCompleteList",
                            "completionListItemCssClass": "autoCompleteListItem",
                            "delimiterCharacters": "",
                            "enableCaching": false,
                            "highlightedItemCssClass": "autoCompleteSelectedListItem",
                            "id": entryActual + "_ext",
                            "minimumPrefixLength": 2,
                            "serviceMethod": "Obtener_Datos_ListaExterna_Extender",
                            "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                            "useContextKey": true,
                            "completionSetCount": oContr.FilasAutoCompleteExtender
                        }, {
                            "itemSelected": onListaExternaExtenderItemSelected,
                            "populated": onCodListPopulated,
                            "shown": resetPosition
                        }, null, $get(entryActual + "_t"));
                    });
                };
            };
        };
        campo = null;
        //Aqui es donde se introducen en el array los controles	
        stemp = s;
        stemp2;
        tempinicio;
        if (stemp.search(' id="') > 0) { //En FireFox los ids vienen con comillas
            tempinicio = stemp.search(' id="');
            tempinicio = tempinicio + 5;
        } else {
            tempinicio = stemp.search(" id="); //En IE los ids vienen sin comillas
            tempinicio = tempinicio + 4;
        };
        stemp2 = stemp.substr(tempinicio, stemp.length - tempinicio);
        tempfin = stemp2.search("__tbl");
        var mientry = stemp2.substr(0, tempfin);
        a = new Array();
        var entry1;
        var entryControl;
        var numero;
        a = mientry.split("_");
        if ((a.length == 7) || (a.length == 4) || (a.length == 9)) {
            entry1 = mientry;
            entryControl = mientry;
            if (a.length == 7) { //cuando no es popup
                numero = a[5];
            } else {
                if (a.length == 4) {
                    numero = a[2];
                } //Cuando es popup
                else {
                    if (a.length == 9) {
                        numero = a[7];
                    }; //Cuando el desglose no es una ventana emergente
                };
            };
            numero = parseInt(numero.toString(), 10);
            var cadenaareemplazar = "_fsdsentry_" + numero.toString() + "_";
            entry1 = entry1.replace(cadenaareemplazar, "_fsdsentry_" + lPrimeraLinea.toString() + "_");
            if (numero.toString() == lPrimeraLinea.toString()) {
                entry1 = entry1.replace("fsdsentry_" + numero.toString() + "_", "fsentry");
            };
            arraynuevafila[insertarnuevoelemento][0] = mientry;
            arraynuevafila[insertarnuevoelemento][1] = entry1;
            oContr = fsGeneralEntry_getById(arraynuevafila[insertarnuevoelemento][1]);
            while (!oContr && VecesEntre == 0 && numero > lPrimeraLinea) {
                lPrimeraLinea = lPrimeraLinea + 1;
                entry1 = entryControl.replace(cadenaareemplazar, "_fsdsentry_" + lPrimeraLinea.toString() + "_");
                arraynuevafila[insertarnuevoelemento][1] = entry1;
                oContr = fsGeneralEntry_getById(arraynuevafila[insertarnuevoelemento][1]);
            };
            cadenaareemplazar = null;
            var entryACopiar = entry1.replace("fsdsentry_" + lPrimeraLinea.toString() + "_", "fsentry");
            oContr = fsGeneralEntry_getById(entryACopiar);
            if (oContr.PoseeDefecto == true) {
                arrayEntrysReadOnlyConDefecto[insertarnuevoelemento] = entry1;
            } //Se agregan los dataentys que estan en ReadOnly y tienen valor por defecto para que en la asignacion de valores no se borren  
            else {
                arrayEntrysReadOnlyConDefecto[insertarnuevoelemento] = "";
            };
            if (oContr) {
                if (oContr.CampoOrigenVinculado == null) {
                    if ((oContr.tipoGS != 119) && (oContr.tipoGS != 118) && (oContr.tipoGS != 104) && (oContr.tipoGS != 127) && (oContr.tipoGS != 121) && (oContr.tipoGS != 116) && (oContr.intro == 1) && (oContr.readOnly == false)) {
                        var param;
                        if (a.length == 9) {
                            param = a[0] + '$_' + a[2] + '$' + a[3] + '$' + a[4] + '_' + a[5] + '$' + a[6] + '_' + a[7] + '_' + a[8] + '$_t';
                        } else {
                            if (a.length >= 6) {
                                param = a[0] + '$_' + a[2] + '$' + a[3] + '$' + a[4] + '_' + a[5] + '$' + a[6] + '$_t';
                            };
                        };
                        if (a[0] == "ucDesglose") {
                            param = a[0] + '$' + a[1] + '$_t';
                        };
                        var bClientBinding;
                        switch (oContr.tipoGS) {
                            case TIPOCAMPOGS.FORMAPAGO:
                            case TIPOCAMPOGS.MONEDA:
                            case TIPOCAMPOGS.UNIDAD:
                            case TIPOCAMPOGS.UNIDADPEDIDO:
                            case TIPOCAMPOGS.PAIS:
                            case TIPOCAMPOGS.PROVINCIA:
                            case TIPOCAMPOGS.DEST:
                            case TIPOCAMPOGS.DEPARTAMENTO:
                            case TIPOCAMPOGS.ORGANIZACIONCOMPRAS:
                            case TIPOCAMPOGS.CENTRO:
                            case TIPOCAMPOGS.ALMACEN:
                            case TIPOCAMPOGS.PROVEEDORERP:
                            case TIPOCAMPOGS.ANYOPARTIDA:
                                bClientBinding = true;
                                break;
                            default:
                                bClientBinding = false;
                                break;
                        };
                        if ((oContr.tipoGS == 0) && (oContr.tipo != 2) && (oContr.tipo != 3) && (oContr.tipo != 4)) {
                            bClientBinding = true;
                        };
                        var valores;
                        if (bClientBinding == false) {
                            var codigo = document.documentElement.innerHTML;
                            var posicion = codigo.search("Infragistics.Web.UI.WebDropDown");
                            if (posicion > 0) {
                                codigo = codigo.substring(posicion);
                                posicion = codigo.search(entryACopiar);
                                if (posicion > 0) {
                                    codigo = codigo.substring(posicion);
                                    posicion = codigo.search("{'0':");
                                    fin = codigo.search(']]}]');
                                    if ((posicion > 0) && (fin > 0)) {
                                        valores = codigo.substring(posicion, fin + 3);
                                    };
                                };
                            };
                            codigo = null;
                            posicion = null;
                        };
                        if (bClientBinding == true) {
                            Sys.Application.add_init(function () {
                                $create(Infragistics.Web.UI.WebDropDown, {
                                    "clientbindingprops": [
                                        [
                                            [, 1, 120, , , , , , , , 1, , 1, , 2, 0, , , 0, , 300, 120, , "", , , , 6, , 1, , , , , , , , , , 0, , , 1, 0, , , 0, , , , 1], {
                                                "c": {
                                                    "dropDownInputFocusClass": "igdd_FullstepValueDisplayFocus",
                                                    "dropDownButtonPressedImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonPressed.png",
                                                    "_pi": "[\"" + InfraStylePath + "/Fullstep/images/ig_ajaxIndicator.gif\",,,\"ig_FullstepPMWebAjaxIndicator\",,1,,,,\"ig_FullstepPMWebAjaxIndicatorBlock\",,3,,3,\"Async post\"]",
                                                    "pageCount": 0,
                                                    "vse": 0,
                                                    "textField": "Texto",
                                                    "controlAreaHoverClass": "ig_FullstepPMWebHover igdd_FullstepControlHover",
                                                    "controlAreaClass": "igdd_FullstepControlArea",
                                                    "valueField": "Valor",
                                                    "controlClass": "ig_FullstepPMWebControl igdd_FullstepControl",
                                                    "dropDownInputHoverClass": "ig_FullstepPMWebHover igdd_FullstepValueDisplayHover",
                                                    "dropDownItemDisabled": "igdd_FullstepListItemDisabled",
                                                    "uid": mientry + "_t",
                                                    "nullTextCssClass": "igdd_FullstepNullText",
                                                    "controlAreaFocusClass": "igdd_FullstepControlFocus",
                                                    "dropDownItemHover": "igdd_FullstepListItemHover",
                                                    "dropDownButtonNormalImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButton.png",
                                                    "dropDownInputClass": "igdd_FullstepValueDisplay",
                                                    "controlDisabledClass": "ig_FullstepPMWebDisabled igdd_FullstepControlDisabled",
                                                    "dropDownButtonDisabledImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonDisabled.png",
                                                    "dropDownItemClass": "igdd_FullstepListItem",
                                                    "pi": "[,,9,,1,1,,80,,,,3,,3,\"Async post\"]",
                                                    "dropDownItemSelected": "igdd_FullstepListItemSelected",
                                                    "ocs": 1,
                                                    "dropDownButtonClass": "igdd_FullstepDropDownButton",
                                                    "dropDownItemActiveClass": "igdd_FullstepListItemActive",
                                                    "dropDownButtonHoverImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonHover.png",
                                                    "dropDownInputDisabledClass": "ig_FullstepPMWebDisabled igdd_FullstepValueDisplayDisabled"
                                                }
                                            }], , [{}],
                                        ["InputKeyDown:WebDropDown_KeyDown", "DropDownOpening:WebDropDown_DataCheck", "SelectionChanged:WebDropDown_SelectionChanging", "Blur:WebDropDown_Focus", "ValueChanged:WebDropDown_valueChanged"],
                                        []
                                    ],
                                    "id": mientry + "__t",
                                    "name": "_t"
                                }, null, null, $get(mientry + "__t"));
                            });
                        } else {
                            Sys.Application.add_init(function () {
                                $create(Infragistics.Web.UI.WebDropDown, {
                                    "id": mientry + "__t",
                                    "name": "_t",
                                    "props": [
                                        [
                                            [, 1, 160, , , , , -1, , , 1, , 1, , 2, 0, , , 0, , , 0, , "", , , , 6, , 1, , , , , , , , 0, , 0, , -1, 0, 0, , 0, 0, , , ], {
                                                "c": {
                                                    "controlDisabledClass": "ig_FullstepPMWebDisabled igdd_FullstepControlDisabled",
                                                    "controlAreaClass": "igdd_FullstepControlArea",
                                                    "controlAreaHoverClass": "ig_FullstepPMWebHover igdd_FullstepControlHover",
                                                    "vse": 0,
                                                    "dropDownInputHoverClass": "ig_FullstepPMWebHover igdd_FullstepValueDisplayHover",
                                                    "dropDownItemHover": "igdd_FullstepListItemHover",
                                                    "dropDownButtonNormalImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButton.png",
                                                    "pi": "[]",
                                                    "dropDownButtonHoverImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonHover.png",
                                                    "dropDownButtonPressedImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonPressed.png",
                                                    "dropDownInputClass": "igdd_FullstepValueDisplay",
                                                    "dropDownItemClass": "igdd_FullstepListItem",
                                                    "dropDownInputDisabledClass": "ig_FullstepPMWebDisabled igdd_FullstepValueDisplayDisabled",
                                                    "controlClass": "ig_FullstepPMWebControl igdd_FullstepControl",
                                                    "dropDownInputFocusClass": "igdd_FullstepValueDisplayFocus",
                                                    "dropDownButtonDisabledImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonDisabled.png",
                                                    "pageCount": 4,
                                                    "controlAreaFocusClass": "igdd_FullstepControlFocus",
                                                    "dropDownItemDisabled": "igdd_FullstepListItemDisabled",
                                                    "ocs": 1,
                                                    "nullTextCssClass": "igdd_FullstepNullText",
                                                    "dropDownButtonClass": "igdd_FullstepDropDownButton",
                                                    "uid": param,
                                                    "dropDownItemSelected": "igdd_FullstepListItemSelected",
                                                    "dropDownItemActiveClass": "igdd_FullstepListItemActive"
                                                }
                                            }], , eval("[" + valores + "]"), ["DropDownOpening:WebDropDown_DropDownOpening", "SelectionChanged:WebDropDown_SelectionChanging", "Blur:WebDropDown_Focus", "ValueChanged:WebDropDown_valueChanged"]
                                    ]
                                }, null, null, $get(mientry + "__t"));
                            });
                        };
                        Sys.Application.add_load(function () {
                            $find(mientry + "__t").behavior.set_enableMovingTargetWithSource(false)
                        });
                        param = null;
                        valores = null;
                    };
                };
            };
            entryACopiar = null;
            //las no conf grabadas tienen cols 1020 y 1010 que me obligan
            VecesEntre = 1;
            insertarnuevoelemento = insertarnuevoelemento + 1
        };
        oContr = null;
        a = null;
        mientry = null;
        entry1 = null;
        entryControl = null;
        numero = null;
    };
    s = "<input type=hidden name=" + sRoot + "_" + lIndex + "_Deleted id=" + sRoot + "_" + lIndex + "_Deleted value='no'>";
    oTD.insertAdjacentHTML("beforeEnd", s);
    oTD = null;
    entryActual = null;
    stemp = null;
    stemp2 = null;
    tempinicio = null;
    tempfin = null;
    var iNumCeldas; //indica el n�mero de celdas de la tabla 
    var iCeldaIni = 1;
    iNumCeldas = document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells.length - 2; //si es una linea de desglose estan todas menos la �ltima que es el bot�n de eliminar
    for (i = 0; i < iNumCeldas; i++) {
        re = /fsentry/;
        //------------------------------------------------------------------------------------------------------------
        var s2 = eval("arrEntrys_" + idCampo + "[i]");
        if (!s2) {
            continue;
        };
        //------------------------------------------------------------------------------------------------------------			
        if (s2.search("arrCalculados") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_");
        };
        //------------------------------------------------------------------------------------------------------------			
        if (s2.search("arrObligatorios") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_");
        };
        //------------------------------------------------------------------------------------------------------------			
        if (s2.search("arrArticulos") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_");
            if (!(s2.search("arrArticulos\[iNumArts][0]=\"\"") > 0)) {
                s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_");
            };
        };
        //------------------------------------------------------------------------------------------------------------			
        if (s2.search("arrPres1") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_");
        };
        //------------------------------------------------------------------------------------------------------------			
        if (s2.search("arrPres2") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_");
        };
        //------------------------------------------------------------------------------------------------------------			
        if (s2.search("arrPres3") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_");
        };
        //------------------------------------------------------------------------------------------------------------			
        if (s2.search("arrPres4") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_");
        };
        s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_");
        s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_");
        if (s2.search(", 104,") > 0 || s2.search(",108,") > 0 || s2.search(", 114,") > 0) {
            s2 = s2.replace(re, "fsdsentryx" + lIndex.toString() + "x"); //en el caso del art�culo,de la provincia y del contacto del proveedor, la grid del dropdown es �nica para cada fila
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_"); //tambien es �nico el campo dependiente (dependentfield)
        };
        eval(s2);
        //------------------------------------------------------------------------------------------------------------			
        if (s2.search("arrObligatorios") > 0) {
            var oEntry = fsGeneralEntry_getById(arrObligatorios[arrObligatorios.length - 1]);
            oEntry.basedesglose = false;
        };
        s2 = null;
    };
    iNumCeldas = null;
    //Se hace la asignacion de valores para la nueva fila   --------------------------------------------------------
    var o;
    var o1;
    for (i = 0; i < insertarnuevoelemento; i++) {
        o = fsGeneralEntry_getById(arraynuevafila[i][0]);
        o1 = fsGeneralEntry_getById(arraynuevafila[i][1]);
        //  Columnas No Escritura � No Visible son labels no son entrys. fsGeneralEntry_getById de una label es null. null.PoseeDefecto es error 
        if (o) {
            o.nColumna = i + iCeldaIni;
            o.nLinea = lLineaGrid;
            if (o.errMsg) {
                o.errMsg = o.errMsg.replace('\n', '\\n');
                o.errMsg = o.errMsg.replace('\n', '\\n');
            };
            if (document.getElementById(sRoot + "_numRowsGrid").value == 1) {
                if (o.CampoOrigenVinculado == null) {
                    if (o1) {
                        if (o1.PoseeDefecto == true) {
                            o.PoseeDefecto = o1.PoseeDefecto;
                            o.TextoValorDefecto = o1.TextoValorDefecto;
                            o.DataValorDefecto = o1.DataValorDefecto;
                        };
                    };
                } else {
                    o.PoseeDefecto = false;
                };
            } else {
                if (o1) {
                    o.PoseeDefecto = o1.PoseeDefecto;
                    o.TextoValorDefecto = o1.TextoValorDefecto;
                    o.DataValorDefecto = o1.DataValorDefecto;
                };
            };
            if ((o.tipoGS == 8 || o.tipo == 8)) { //Fichero ADJUNTO               			
                var vaciar = new Array();
                vaciar[0] = "";
                vaciar[1] = "";
                if (o.CampoOrigenVinculado == null) {
                    if (o.PoseeDefecto == true) {
                        o.setValue(o.TextoValorDefecto);
                        vaciar[0] = o.DataValorDefecto;
                        if (vaciar[0] == null) {
                            vaciar[0] = "";
                        };
                        vaciar[1] = "";
                        //Si es un adjunto se tiene que generar un nuevo id para el adjunto/s copiados
                        CreateXmlHttp();
                        if (xmlHttp) {
                            var params = "accion=9&AdjuntosAct=" + vaciar[0] + "&AdjuntosNew=" + vaciar[1];
                            xmlHttp.open("POST", rutaPM + "GestionaAjax.aspx", false);
                            xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                            xmlHttp.send(params);
                            var retorno;
                            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                                retorno = xmlHttp.responseText;
                                vaciar[0] = "";
                                vaciar[1] = retorno;
                                o.setDataValue(vaciar);
                            };
                        };
                    } else {
                        o.setValue('');
                        o.setDataValue(vaciar);
                        //Hago lo mismo que cuando se borra el adjunto para que si hab�a algo cargado en la l�nea oculta de la primera l�nea  
                        //pero no es un valor por defecto se borre tambi�n el enlace de detalle
                        if (o1.getValue() != "") {
                            entryId = o.id;
                            mioEntryId = entryId;
                            while (entryId.indexOf('$') >= 0)
                                entryId = entryId.replace("$", "_");
                            if (typeof (idContrato) == 'undefined') idContrato = 0;
                            var instancia = document.getElementById(entryId + '__hInstancia').value;
                            var tipo = document.getElementById(entryId + '__hTipo').value;
                            var adjunto = document.getElementById(entryId + '__hAct').value;
                            var adjuntoNew = document.getElementById(entryId + '__hNew').value;
                            var nombre = document.getElementById(entryId + '__hNombre').value;
                            var idCampo = document.getElementById(entryId + '__hCampo').value;

                            o.removeFile(tipo, adjunto)
                            // 1.- Creamos el objeto xmlHttpRequest
                            CreateXmlHttp();
                            // 2.- Definimos la llamada para hacer un simple GET.
                            var ajaxRequest = rutaPM + 'GestionaAjax.aspx?accion=1&EntryID=' + mioEntryId + '&instancia=' + instancia + '&tipo=' + tipo + '&nombre=' + nombre + '&adjunto=' + adjunto + '&adjuntoNew=' + adjuntoNew + '&campo=' + idCampo + '&readOnly=' + o.readOnly.toString() + '&origen=null&IdContrato=' + idContrato.toString() + '&defecto=' + o.DataValorDefecto;
                            // 3.- Marcar qu� funci�n manejar� la respuesta
                            xmlHttp.onreadystatechange = recogeInfo;
                            // 4.- Enviar
                            xmlHttp.open("GET", ajaxRequest, true);
                            xmlHttp.send("");
                        }
                    };
                } else {
                    try {
                        Textos = sacarValorCelda("VALORMOSTRAR_" + o.CampoOrigenVinculado, Row);
                        if (Textos == null) {
                            o.setValue('');
                        } else {
                            vaciar[1] = sacarValorCelda("VALOR_" + o.CampoOrigenVinculado, Row);
                        };
                        o.setValue(sacarValorCelda("VALORMOSTRAR_" + o.CampoOrigenVinculado, Row));
                        o.setDataValue(vaciar);
                    } catch (e) {
                        //Ha ocurrido q: La instancia x se hizo con 3 cols, luego la configuraci�n se cambio a 5 columnas. Y una
                        //de estas dos columnas nuevas se vinculo a nivel de campo. Nos hemos encontrado con q la columna x espera
                        //una columna q no exist�a en el momento de grabar instancia x.                    
                        o.setValue('');
                        o.setDataValue(vaciar);
                    };
                };
                vaciar = null;
            } else {
                switch (o.tipoGS) {
                    case 119:
                        //Cod Articulo Nuevo 
                    case 104:
                        //CodArt Antiguo
                    case 118:
                        switch (o.tipoGS) {
                            case 119:
                                //Cod Articulo Nuevo 
                            case 104:
                                //CodArt Antiguo
                                o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_")
                                break;
                            case 118:
                                break;
                        };
                        if (o.idDataEntryDependent) {
                            if (o.idDataEntryDependent.search("fsentry_") < 0) { //La organizacion de compras de encuentra fuera del desglose
                                oEntry = fsGeneralEntry_getById(o.idDataEntryDependent)
                                if ((oEntry) && (oEntry.tipoGS == 123)) {
                                    oEntry.idDataEntryDependent = arraynuevafila[i][0];
                                };
                            };
                            o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        if (o.idDataEntryDependent2) {
                            o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        if (o.idDataEntryDependent3) {
                            if (o.idDataEntryDependent3.search("fsentry_") < 0) { //La organizacion de compras de encuentra fuera del desglose
                                oEntry = fsGeneralEntry_getById(o.idDataEntryDependent3);
                                if ((oEntry) && (oEntry.tipoGS == 121)) {
                                    oEntry.idDataEntryDependent3 = arraynuevafila[i][0];
                                };
                            };
                            o.idDataEntryDependent3 = o.idDataEntryDependent3.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        if (o.idDataEntryDependentCentroCoste) {
                            o.idDataEntryDependentCentroCoste = o.idDataEntryDependentCentroCoste.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        if (o.idEntryPREC) {
                            o.idEntryPREC = o.idEntryPREC.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        if (o.idEntryPROV) {
                            o.idEntryPROV = o.idEntryPROV.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        if (o.idEntryCANT) {
                            o.idEntryCANT = o.idEntryCANT.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        if (o.idEntryUNI) {
                            o.idEntryUNI = o.idEntryUNI.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        if (o.IdEntryUniPedido) {
                            o.IdEntryUniPedido = o.IdEntryUniPedido.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        if (o.idEntryCC) {
                            o.idEntryCC = o.idEntryCC.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        if (o.idEntryProveSumiArt) o.idEntryProveSumiArt = o.idEntryProveSumiArt.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        if (o.IdMaterialOculta) {
                            o.IdMaterialOculta = o.IdMaterialOculta.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        if (o.IdCodArticuloOculta) {
                            o.IdCodArticuloOculta = o.IdCodArticuloOculta.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        break;
                    case 6:
                    case 7:
                        if (o.idDataEntryDependent) {
                            o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        break;
                    case 100:
                        //Proveedores
                        if (o.dependentfield) {
                            o.dependentfield = o.dependentfield.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        if (o.idDataEntryDependent2) {
                            o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        break;
                    case 103:
                        if (o.idDataEntryDependent2) {
                            o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        break;
                    case 123:
                        if (o.dependentfield) {
                            o.dependentfield = o.dependentfield.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        if (o.idDataEntryDependent) {
                            o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        if (o.idDataEntryDependent2) {
                            o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        }; //org-centro                        
                        break;
                    case 107:
                        //pais
                        if (o.dependentfield) {
                            o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        break;
                    case 124:
                        //centro
                        if (o.idDataEntryDependent) {
                            o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                            var oEntryArt = fsGeneralEntry_getById(o.idDataEntryDependent);
                            if ((oEntryArt) && (oEntryArt.idDataEntryDependent)) {
                                if (oEntryArt.idDataEntryDependent.search("fsentry_") < 0) { //La organizacion de compras de encuentra fuera del desglose
                                    oEntry = fsGeneralEntry_getById(oEntryArt.idDataEntryDependent);
                                    if ((oEntry) && (oEntry.tipoGS == 123)) {
                                        oEntry.idDataEntryDependent2 = arraynuevafila[i][0];
                                    };
                                };
                            };
                        };
                        if (o.idDataEntryDependent3) {
                            o.idDataEntryDependent3 = o.idDataEntryDependent3.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        break;
                    case 109:
                        //destino
                        if (o.idDataEntryDependent3) {
                            o.idDataEntryDependent3 = o.idDataEntryDependent3.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        break;
                    case 45:
                        o.MonRepercutido = o.MonCentral;
                        o.CambioRepercutido = o.CambioCentral;
                        if (o.readOnly == false) {
                            var lnk = document.getElementById(o.dependentfield)
                            lnk.innerHTML = o.MonCentral;
                        };
                        break;
                    case 110:
                    case 111:
                    case 112:
                    case 113:
                        if ((o.readOnly) && (EsFavorita == 0) && (o.CampoOrigenVinculado == null)) {
                            if (o1) {
                                o.PoseeDefecto = true
                                o.TextoValorDefecto = o1.getValue()
                                o.DataValorDefecto = o1.getDataValue()
                            };
                        };
                        break;
                    case 105:
                        //Unidad
                        if (o.idEntryCANT) {
                            o.idEntryCANT = o.idEntryCANT.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        break;
                    case 4:
                        //Cantidad
                        if (o1) {
                            o.NumeroDeDecimales = o1.NumeroDeDecimales
                            o.UnidadOculta = o1.UnidadOculta
                        }
                        break;
                    case 121:
                        //Unidad organizativa
                        if (o.idDataEntryDependent) {
                            if (o.idDataEntryDependent.search("fsentry_") < 0) { //La organizacion de compras de encuentra fuera del desglose
                                oEntry = fsGeneralEntry_getById(o.idDataEntryDependent);
                                if ((oEntry) && (oEntry.tipoGS == 118)) {
                                    oEntry.idDataEntryDependent = arraynuevafila[i][0];
                                };
                            };
                            o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        break;
                    case 129:
                        //Centro de coste
                        if ((!o.PoseeDefecto) && (!o.dataValue)) {
                            var centro = comprobarCentroUnico(o.VerUON);
                            if (centro != "") {
                                o.PoseeDefecto = true;
                                o.TextoValorDefecto = centro.Denominacion;
                                o.DataValorDefecto = centro.Texto;
                            }
                        }
                        break;
                    case 151:
                        var codPartida0 = '';
                        var codPartida = ' ';
                        if ((!o.PoseeDefecto) && (!o.dataValue)) {
                            if (o.IdEntryPartidaPlurianual) {
                                oAux = fsGeneralEntry_getById(o.IdEntryPartidaPlurianual)
                                if ((oAux) && (o.EntryPartidaPlurianualCampo == 1)) {
                                    codPartida0 = oAux.PRES5
                                    codPartida = ' ' + oAux.getDataValue();
                                } else {
                                    if (o.IdEntryPartidaPlurianual) {
                                        o.IdEntryPartidaPlurianual = o.IdEntryPartidaPlurianual.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                                    };

                                    for (arrInput in arrInputs) {
                                        oAux = fsGeneralEntry_getById(arrInputs[arrInput]);
                                        if (oAux) {
                                            if ((oAux.tipoGS == 130) && ((o.IdEntryPartidaPlurianual == oAux.campo_origen) || (o.IdEntryPartidaPlurianual == oAux.id) || (o.IdEntryPartidaPlurianual == oAux.idCampo))) {
                                                codPartida0 = oAux.PRES5
                                                if (oAux.idCampoPadre) { //Todas las filas desglose tendran el mismo DataValorDefecto. As� q para linea 4ta cojo el valordefecto de la 1ra.
                                                    codPartida = ' ' + oAux.getDataValue();
                                                } else { // esta fuera del desglose
                                                    codPartida = ' ' + oAux.getDataValue();
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                                var AnyoPartida = comprobarAnyoPartidaUnica(codPartida0, codPartida);
                                if (AnyoPartida.length == 1) {
                                    o.PoseeDefecto = true;
                                    o.TextoValorDefecto = AnyoPartida[0].Valor;
                                    o.DataValorDefecto = AnyoPartida[0].Valor;
                                }
                            }
                        }
                        break;
                    case 130:
                        //Partida
                        var codCCoste = "";
                        if ((!o.PoseeDefecto) && (!o.dataValue)) {
                            if (o.idEntryCC) {
                                var desglose = o.id.substr(0, o.id.search("_fsdsentry"))
                                var index = o.id.substr(o.id.search("_fsdsentry_") + 11, o.id.search(o.idCampo) - o.id.search("_fsdsentry_") - 12)
                                oAux = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + o.idEntryCC);
                                if (oAux) {
                                    codCCoste = oAux.DataValorDefecto;
                                } else {
                                    for (arrInput in arrInputs) {
                                        oAux = fsGeneralEntry_getById(arrInputs[arrInput]);
                                        if (oAux) {
                                            if ((oAux.tipoGS == 129) && ((o.idEntryCC == oAux.campo_origen) || (o.idEntryCC == oAux.idCampo))) {
                                                if (oAux.idCampoPadre) { //Todas las filas desglose tendran el mismo DataValorDefecto. As� q para linea 4ta cojo el valordefecto de la 1ra.
                                                    codCCoste = oAux.DataValorDefecto;
                                                } else { //El centro coste esta fuera del desglose
                                                    codCCoste = oAux.getDataValue();
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                            };
                            partida = comprobarPartidaUnica(o.PRES5, codCCoste);
                            if (partida != "") {
                                o.PoseeDefecto = true;
                                o.TextoValorDefecto = partida.Denominacion;
                                o.DataValorDefecto = partida.Texto;
                            }
                        }
                        break;
                };
                if (o.CampoOrigenVinculado == null) {
                    if (o.PoseeDefecto == true) {
                        if ((o.TextoValorDefecto == "") && (o.DataValorDefecto != "") && ((o.tipo == 2) || (o.tipo == 3) || ((o.tipo == 5 || o.tipo == 6 || o.tipo == 7))) && (o.intro == 0)) {
                            o.TextoValorDefecto = o.DataValorDefecto;
                        };
                        o.setValue(o.TextoValorDefecto);
                        o.setDataValue(o.DataValorDefecto);
                        //Nuevo campo/atrib en alta/desglose tienes Texto null Data valor
                        if (o.TextoValorDefecto == "") {
                            if ((o.tipo == 4) && (o.getValue() == "")) {
                                o.setValue(o.DataValorDefecto);
                                if (o.DataValorDefecto == "1") {
                                    o.setValue(sTextoSi);
                                } else {
                                    o.setValue(sTextoNo);
                                };
                            };
                            if ((o.intro == 1) && (o.tipo == 2) && (o.getValue() == "")) {
                                //combo numerico. TextoValorDefecto es "" DataValorDefecto 21. Lo ves 21 pero no hay value. 
                                //Si se usa en calculados, el value no esta establecido y no lo hace bien
                                o.setValue(o.DataValorDefecto);
                            };
                        };
                    } else {
                        //si es un campo Read only que tiene valor con defecto no borramos el valor
                        //Si es un campo de texto tipo medio o largo y no tiene valor dejaremos el valor que indique que pulse para introducir el texto
                        if (arrayEntrysReadOnlyConDefecto[i] != arraynuevafila[i][1]) {
                            o.setValue('');
                            o.setDataValue('');
                            if (o.tipo == 2) {
                                //Para los numeros el o.setDataValue('') consigue q o.getDataValue sea null y o.dataValue=""
                                o.dataValue = null;
                            };
                        };
                    };
                    if (o.tipoGS == 43) {
                        o.setValue(arrEstadosInternos[1][1]);
                        o.setDataValue(1);
                    };
                    if (o.tipoGS == 2000) {
                        if (Row != null) {
                            o.setValue(sacarValorCelda("DENSOL", Row));
                            o.setDataValue(sacarValorCelda("INSTANCIAORIGEN", Row) + '@' + sacarValorCelda("DESGLOSEORIGEN", Row) + '@' + sacarValorCelda("LINEAORIGEN", Row));
                        } else {
                            o.setValue(sId + ' - ' + sDen);
                            o.setDataValue(sId);
                        };
                    };
                    if (o.tipoGS == 3000) {
                        o.setValue(o.nLinea);
                    };
                    if (o.readOnly == false) {
                        var sID = '';
                        if (o.Editor) {
                            if (o.Editor.ID) {
                                sID = o.Editor.ID;
                            } else {
                                sID = o.Editor.id;
                            };
                            if (sID != '' && sID != undefined) {
                                if (o.getDataValue() != '' && o.getDataValue() != null) {
                                    //setTimeout('copiarSeleccionWebDropDown("' + sID + '","' + o.getDataValue() + '")', 200)
                                    copiarSeleccionWebDropDown(sID, o.getDataValue());
                                } else {
                                    //setTimeout('CambiarSeleccion("' + sID + '","BORRAR")', 200)
                                    CambiarSeleccion(sID, "BORRAR");
                                };
                            };
                        };
                        sID = null;
                    };
                } else {
                    try {
                        switch (o.tipoGS) {
                            case 11:
                            case 100:
                            case 101:
                            case 102:
                            case 103:
                            case 105:
                            case 142:
                                //Unidad de Pedido                                
                            case 107:
                            case 108:
                            case 109:
                            case 110:
                            case 111:
                            case 112:
                            case 113:
                            case 115:
                            case 121:
                            case 122:
                            case 123:
                            case 124:
                            case 125:
                            case 129:
                            case 130:
                            case 131:
                            case 151:
                                o.setValue(sacarValorCelda("VALORMOSTRAR_" + o.CampoOrigenVinculado, Row));
                                o.setDataValue(sacarValorCelda("VALOR_" + o.CampoOrigenVinculado, Row));
                                break;                            
                            default:
                                if ((o.tipoGS == 0) && (o.intro == 1) && ((o.tipo == 1) || (o.tipo == 5) || (o.tipo == 6))) {
                                    o.setValue(sacarValorCelda("VALORMOSTRAR_" + o.CampoOrigenVinculado, Row));
                                    o.setDataValue(sacarValorCelda("VALOR_" + o.CampoOrigenVinculado, Row));
                                } else {
                                    if (o.TablaExterna != 0) { //tabla externa
                                        o.setValue(sacarValorCelda("VALORMOSTRAR_" + o.CampoOrigenVinculado, Row));
                                        o.setDataValue(sacarValorCelda("VALOR_" + o.CampoOrigenVinculado, Row));
                                    } else {
                                        if (o.tipo == 4) { //boolean
                                            o.setValue(sacarValorCelda("VALORMOSTRAR_" + o.CampoOrigenVinculado, Row));
                                            o.setDataValue(sacarValorCelda("VALOR_" + o.CampoOrigenVinculado, Row));
                                        } else {
                                            o.setValue(sacarValorCelda("VALOR_" + o.CampoOrigenVinculado, Row));
                                            o.setDataValue(sacarValorCelda("VALOR_" + o.CampoOrigenVinculado, Row));
                                        };
                                    };
                                };
                                break;
                        };
                        if (o.getDataValue() == "null") {
                            o.setDataValue('');
                        }
                        if (o.readOnly == false) {
                            sID = '';
                            if (o.Editor) {
                                if (o.Editor.ID) {
                                    sID = o.Editor.ID;
                                } else {
                                    sID = o.Editor.id;
                                };
                                if (sID != '' && sID != undefined) {
                                    copiarSeleccionWebDropDown(sID, o.getDataValue());
                                } else {
                                    CambiarSeleccion(sID, "BORRAR");
                                };
                            };
                            sID = null;
                        };
                    } catch (e) {
                        //Ha ocurrido q: La instancia x se hizo con 3 cols, luego la configuraci�n se cambio a 5 columnas. Y una
                        //de estas dos columnas nuevas se vinculo a nivel de campo. Nos hemos encontrado con q la columna x espera
                        //una columna q no exist�a en el momento de grabar instancia x.
                        //Ha ocurrido q se trae una solicitud
                        o.setValue('');
                        o.setDataValue('');
                        if (o.tipo == 2) {
                            //Para los numeros el o.setDataValue('') consigue q o.getDataValue sea null y o.dataValue=""
                            o.dataValue = null;
                        };
                    };
                };
            };
            if (o.masterField && o.masterField != '') {
                o.masterField = o.masterField.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
            };
            if (o.dependentfield && o.dependentfield != '') {
                //Se han usado tanto 'fsentry_' como con 'fsentry'. Ante la duda, controlamos ambos casos
                o.dependentfield = o.dependentfield.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
            };
        };
    };
    s = null;
    sClase = null;
    oTR = null;
    lIndex = null;
    lLineaGrid = null;
    sMovible = null;
    lPrimeraLinea = null;
    arraynuevafila = null;
    insertarnuevoelemento = null;
    VecesEntre = null;
    re = null;
    fin = null;
    Textos = null;
    iCeldaIni = null;
    o = null;
    o1 = null;
    oEntry = null;
};
/*
''' La funcion copiarFilaVacia recibe el parametro Row pero ahora este parametro puede venir 
''' de un WebDataGrid y la forma en que se saca el valor de una celda es diferente. Como se usa mucho
''' esta funcion se encargara de comprobar el tipo de Row que llega y sacar el valor del campo en cuestion.
*/
function sacarValorCelda(campo, Row) {
    var celda = "";
    if (Row.getCellFromKey) {
        celda = Row.getCellFromKey(campo).getValue();
    } else {
        celda = Row.get_cellByColumnKey(campo).get_text();
    };
    return celda;
};
/*  Revisado por: blp. Fecha: 05/10/2011
''' <summary>
''' Copia una fila seleccionada de un desglose en una fila nueva. Para Qa, el estado interno de la 
''' fila nueva es 1 (sin revisar) y	deben ver los botones de aceptar y rechazar. Si la columna es readonly 
''  no se crea dataentry solo una label con un boton tres puntos si tiene texto, si no tiene texto no debe 
''' ir el bot�n. 
''' </summary>
''' <param name="sRoot">nombre entry del desglose</param>
''' <param name="index">id de la fila seleccionada</param>        
''' <param name="idCampo">id de bbdd del desglose</param>
''' <param name="ProveCod">Cod de bbdd del proveedor</param>     
''' <param name="ProveDen">Descrip de bbdd del proveedor</param>
''' <param name="lIdContacto">id de bbdd del contacto del proveedor</param>     
''' <param name="sDenContacto">Descrip de bbdd del contacto del proveedor</param>     
''' <returns>Nada</returns>
''' <remarks>Llamada desde:javascript introducido para responder al onclick de todo boton 'copiar fila' de todo desglose
''' ; Tiempo m�ximo:0</remarks>
*/
function copiarFilaSeleccionada(sRoot, index, idCampo, ProveCod, ProveDen, lIdContacto, sDenContacto) {
    var bAlta = false;
    p = window.opener;
    if (p) {
        if (p.document.forms["frmAlta"] != undefined) bAlta = true;
    } else {
        if (document.forms["frmAlta"] != undefined) bAlta = true;
    }
    var bMensajePorMostrar = document.getElementById("bMensajePorMostrar");
    if (bMensajePorMostrar) {
        if (bMensajePorMostrar.value == "1") {
            bMensajePorMostrar.value = "0";
            return;
        };
    };
    bMensajePorMostrar = null;
    var s;
    //uwtGrupos__ctl0__ctl0
    var sClase = document.getElementById(sRoot + "_tblDesglose").rows[document.getElementById(sRoot + "_tblDesglose").rows.length - 1].cells[0].className;
    if (sClase == "ugfilatablaDesglose") {
        sClase = "ugfilaalttablaDesglose";
    } else {
        sClase = "ugfilatablaDesglose";
    };
    var oTR = document.getElementById(sRoot + "_tblDesglose").insertRow(-1);
    var lIndex;
    if (document.getElementById(sRoot + "_numRows")) {
        lIndex = parseFloat(document.getElementById(sRoot + "_numRows").value) + 1;
        document.getElementById(sRoot + "_numRows").value = lIndex;
    } else {
        lIndex = document.getElementById(sRoot + "_tblDesglose").rows.length - 1;
        s = "<input type=hidden name=" + sRoot + "_numRows id=" + sRoot + "_numRows value=" + lIndex.toString() + ">";
        document.getElementById("divAlta").insertAdjacentHTML("beforeEnd", s);
    };
    var lLineaGrid;
    if (document.getElementById(sRoot + "_numRowsGrid")) {
        lLineaGrid = parseFloat(document.getElementById(sRoot + "_numRowsGrid").value) + 1;
        document.getElementById(sRoot + "_numRowsGrid").value = lLineaGrid;
        if ((!bAlta) || (bAlta && p)) lLineaGrid = lIndex;
    } else {
        lLineaGrid = document.getElementById(sRoot + "_tblDesglose").rows.length - 1;
        s = "<input type=hidden name=" + sRoot + "_numRowsGrid id=" + sRoot + "_numRowsGrid value=" + lLineaGrid.toString() + ">";
        document.getElementById("divAlta").insertAdjacentHTML("beforeEnd", s);
    };
    var pp = window.parent;
    var Salta = 0;
    if (pp) {
        if (window.parent.location.toString().search("altacertificado.aspx") != -1) {
            Salta = 1;
        };
    };
    pp = null;
    var pv = window.opener;
    if (pv) {
        if (Salta == 0) {
            var oFrm = pv.document.forms["frmDesglose"];
            if (oFrm) {
                var oRowVinc = pv.document.getElementById(document.getElementById("INPUTDESGLOSE").value + "__numRowsVinc");
                if (oRowVinc) {
                    lLineaRowsVinc = parseFloat(oRowVinc.value) + 1;
                    oRowVinc.value = lLineaRowsVinc;
                };
            };
            oFrm = null;
        };
    };
    pv = null;
    //Controlar las filas que se a�aden para optimizar MontarFormularioSubmit
    var ultimaFila = 0;
    var filaNum;
    for (var indice in htControlFilas) {
        if (String(idCampo) == indice.substring(0, indice.indexOf("_"))) {
            filaNum = indice.substr(indice.lastIndexOf("_") + 1);
            if (parseInt(ultimaFila) < parseInt(filaNum)) {
                if (htControlFilas[String(idCampo) + "_" + String(filaNum)] != 0) {
                    ultimaFila = htControlFilas[String(idCampo) + "_" + String(filaNum)];
                }
            }
        };
    };
    htControlFilas[String(idCampo) + "_" + String(lIndex)] = String(parseInt(ultimaFila) + 1);
    ultimaFila = null;
    htControlFilasVinc[String(idCampo) + "_" + String(lIndex)] = String(0);
    var sMovible;
    sMovible = String(idCampo) + "_" + String(lIndex);
    htControlArrVincMovible[sMovible] = "##No##";
    //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
    //A continuacion se obtiene lPrimeraLinea, que es la fila de donde se copian los datos
    var lPrimeraLinea = index;
    //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //En este array se guardan en cada fila el nuevo data entry y del que se va acoger el valor (_1_)
    var arraynuevafila = new Array();
    var insertarnuevoelemento = 0;
    for (var i = 0; i < document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells.length; i++) {
        arraynuevafila[i] = new Array();
        for (var j = 0; j < 2; j++) {
            arraynuevafila[i][j] = "";
        };
    };
    var _arDependenceRelations = new Array();
    var oTD;
    for (i = 0; i < document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells.length; i++) {
        s = document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells[i].innerHTML;
        oTD = oTR.insertCell(-1);
        oTD.className = sClase;
        if (document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells[i].id) {
            oTD.id = document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells[i].id + '_' + lIndex;
        };
        var sArchivo;
        var entryActual;
        var stemp = s;
        var stemp2;
        var tempinicio;
        var tempfin;
        if (stemp.search(' id="') > 0) { //En FireFox los ids vienen con comillas
            tempinicio = stemp.search(' id="');
            tempinicio = tempinicio + 5;
        } else {
            tempinicio = stemp.search(" id="); //En IE los ids vienen sin comillas
            tempinicio = tempinicio + 4;
        };
        stemp2 = stemp.substr(tempinicio, stemp.length - tempinicio);
        tempfin = stemp2.search("__tbl");
        entryActual = stemp2.substr(0, tempfin);
        var entryArchivo = entryActual;
        var re = /fsentry/;
        while (entryArchivo.search(re) >= 0) {
            entryArchivo = entryArchivo.replace(re, "fsdsentry_" + lPrimeraLinea.toString() + "_");
        }
        var oContrAr = null;
        if (entryArchivo != '') {
            oContrAr = fsGeneralEntry_getById(entryArchivo);
        };
        entryArchivo = null;
        if (oContrAr) {
            if (oContrAr.tipo == 8) {
                sArchivo = s;
                s = oContrAr.Element.parentNode.innerHTML;
                while (s.search("fsdsentry_" + lPrimeraLinea.toString() + "_") >= 0) {
                    s = s.replace("fsdsentry_" + lPrimeraLinea.toString() + "_", "fsdsentry_" + lIndex.toString() + "_");
                };
            };
        };
        ///////////////////
        re = "class=trasparent"
        var re2 = /class="trasparent"/
        if ((s.search(re) >= 0) || (s.search(re2) >= 0)) {
            var den = '';
            var valor = '';
            var inicio = s.search(sRoot + "_fsentry");
            if (inicio > 1) {
                var cadena = s.substr(inicio + sRoot.length + 8);
                var fin = cadena.indexOf("_");
                cadena = cadena.substring(0, fin);
                if (document.getElementById(sRoot + "_fsdsentry_" + index + "_" + cadena)) {
                    var etiqueta = document.getElementById(sRoot + "_fsdsentry_" + index + "_" + cadena).innerHTML;
                    inicio = etiqueta.indexOf("<SPAN>") + 6;
                    if (inicio < 6) inicio = etiqueta.indexOf("<span>") + 6;
                    fin = etiqueta.indexOf("</SPAN>");
                    if (fin < 0) fin = etiqueta.indexOf("</span>");
                    den = etiqueta.substring(inicio, fin);
                    etiqueta = null;
                    if (den == '&nbsp;') {
                        den = '';
                        s = s.replace("<IMG src='" + rutaPM + "_common/images/trespuntos.gif' align=middle border=0 unselectable=\"on\">", "");
                        s = s.replace("BACKGROUND-IMAGE: url(" + rutaPM + "_common/images/trespuntos.gif);", "display: none;");
                    };
                };
                if (document.getElementById(sRoot + "_" + index + "_" + cadena)) {
                    valor = document.getElementById(sRoot + "_" + index + "_" + cadena).value;
                };
                cadena = null;
                fin = null;
            };
            if (valor != '') {
                if (s.search("__h type=hidden") >= 0) {
                    s = s.replace("__h type=hidden", "__h type='hidden' value='" + valor + "'");
                };
                if (s.search("__h\" type=\"hidden\"") >= 0) {
                    s = s.replace("__h\" type=\"hidden\"", "__h\" type=\"hidden\" value='" + valor + "'");
                };
            };
            if (den != '') {
                if (s.search("</TEXTAREA") >= 0) {
                    s = s.replace("</TEXTAREA", den + "</TEXTAREA");
                } else {
                    s = s.replace("readOnly", "readOnly value='" + den + "'");
                };
                while (s.search("type=hidden") >= 0) {
                    s = s.replace("type=hidden", "type='hidden' value='" + den + "'");
                };
                while (s.indexOf('[\"\"') >= 0) {
                    s = s.replace('[\"\"', '[\"' + den + '\"');
                };
            };
            den = null;
            valor = null;
            inicio = null;
        };
        ///////////////////	
        re = /fsentry/;
        while (s.search(re) >= 0) {
            s = s.replace(re, "fsdsentry_" + lIndex.toString() + "_");
        };
        /////////////////////////////////////////////////////////
        // revisar si es necesario mantener este c�digo
        var se = /<STYLE type=text\/css>/;
        if (s.search(se) > 0) {
            while ((s.search(se) > 0)) {
                se = /<STYLE type=text\/css>/;
                var iComienzo = s.search(se);
                se = /<\/STYLE>/;
                var iFin = s.search(se);
                var sStyle = s.substr(iComienzo + 21, iFin - (iComienzo + 21));
                s = s.substr(0, iComienzo) + s.substr(iFin + 8);
                iComienzo = null;
                iFin = null;
                var rllave = /{/;
                var rllaveCierre = /}/;
                var sAux = sStyle;
                sStyle = null;
                while (sAux.search(rllave) > -1) {
                    sSelector = sAux.substr(0, sAux.search(rllave));
                    sStyleNew = sAux.substr(sAux.search(rllave) + 1, sAux.search(rllaveCierre) - sAux.search(rllave) - 1);
                    document.styleSheets[document.styleSheets.length - 1].addRule(sSelector, sStyleNew);
                    sAux = sAux.substr(sAux.search(rllaveCierre) + 2);
                };
                rllave = null;
                rllaveCierre = null;
                sAux = null;
            };
        };
        // Fin revisar
        /////////////////////////////////////////////////////////
        //  WEBDROPDOWNS RENDERIZADOS EN EL CLIENTE 
        //Si el control tiene un control dependiente, a�adimos ambos a un array de maestros - dependientes q usaremos m�s tarde
        if (oContrAr) {
            if (oContrAr.dependentfield) {
                var oDependentField = fsGeneralEntry_getById(oContrAr.dependentfield);
                if (oDependentField) {
                    _arDependenceRelations[_arDependenceRelations.length] = new Array(oContrAr.id, oContrAr.dependentfield, oDependentField.idCampo);
                    oDependentField = null;
                };
            };
        };
        //Para evitar generar los contenidos de los webdropdown con los mismos IDs y a fin de que podamos localizar esos datos con m�s facilidad, vamos a editar los IDs
        var webdropdownReplace = '';
        var finID = s.search(".0:mkr:Target");
        if (finID > -1) {
            var extraer = s.substr(0, finID);
            var inicioID = extraer.lastIndexOf("x:");
            if (inicioID > -1) {
                extraer = extraer.substr(inicioID + 2, finID);
                webdropdownReplace = extraer;
                extraer = null;
            };
            inicioID = null;
        };
        finID = null;
        if (webdropdownReplace != '') {
            var numAleatorio = Aleatorio(1, parseInt(webdropdownReplace));
            var re = new RegExp(); // empty constructor
            re.compile(webdropdownReplace, 'gi');
            s = s.replace(re, parseInt(webdropdownReplace) + parseInt(numAleatorio));
            numAleatorio = null;
        };
        webdropdownReplace = null;
        se = /<SCRIPT language=javascript>/;
        se2 = /<script language="javascript">/;
        var arrJS = new Array();
        var ni = /##newIndex##/g;
        if ((s.search(se) > 0) || (s.search(se2) > 0)) {
            while (s.search(se) > 0) {
                iComienzo = s.search(se) + 28;
                se = /<\/SCRIPT>/;
                iFin = s.search(se);
                var sEval = s.substr(iComienzo, iFin - iComienzo);
                s = s.substr(0, iComienzo - 28) + s.substr(iFin + 9);
                iComienzo = null;
                iFin = null;
                arrJS[arrJS.length] = sEval;
                sEval = null;
                se = /<SCRIPT language=javascript>/;
            };
            while (s.search(se2) > 0) {
                var iComienzo = s.search(se2) + 30;
                se = /<\/script>/;
                var iFin = s.search(se);
                var sEval = s.substr(iComienzo, iFin - iComienzo);
                s = s.substr(0, iComienzo - 30) + s.substr(iFin + 9);
                arrJS[arrJS.length] = sEval;
                sEval = null;
                se2 = /<script language="javascript">/;
            };
            if (s.search(ni) > 0) {
                oTD.align = "center";
            };
            s = replaceAll(s, "##newIndex##", lIndex.toString());
            oTD.insertAdjacentHTML("beforeEnd", s);
            for (k = 0; k < arrJS.length; k++) {
                eval(arrJS[k]);
            };
        } else {
            if (s.search(ni) > 0) {
                oTD.align = "center";
            };
            ni = "_cmdRechazarAccion";
            if (s.search(ni) > 0) {
                s = s.replace("_cmdRechazarAccion", "_cmdRechazarAccion_'##newIndex##'");
            };
            ni = "_cmdAprobarAccion";
            if (s.search(ni) > 0) {
                s = s.replace("_cmdAprobarAccion", "_cmdAprobarAccion_'##newIndex##'");
            };
            s = replaceAll(s, "'##newIndex##'", lIndex.toString());
            re = /'##Si##'/;
            if (s.search(re) > 0) {
                htControlArrVincMovible[sMovible] = "##Nueva##";
            };
            oTD.insertAdjacentHTML("beforeEnd", s);
        };
        ni = null;
        arrJS = null;
        //Aqui se agrega el AutoCompleteExtender al control si tiene la propiedad RecordarValores a True
        var oContr;
        oContr = fsGeneralEntry_getById(entryActual);
        var campo;
        var a = new Array();
        a = entryActual.split("_");
        if (a.length >= 6) {
            if (oContr.tipoGS == 118) {
                entryActual = a[0] + '__' + a[2] + '_' + a[3] + '_' + a[4] + '_' + a[5] + '_' + a[6] + '__tden';
            }
            else {
                entryActual = a[0] + '__' + a[2] + '_' + a[3] + '_' + a[4] + '_' + a[5] + '_' + a[6] + '__t';
            }
            re = /fsentry/;
            while (entryActual.search(re) >= 0) {
                entryActual = entryActual.replace(re, "fsdsentry_" + lIndex.toString() + "_");
            };
            campo = a[6];
            campo = campo.replace(re, "");
            if (oContr) {
                if (oContr.RecordarValores == true) {
                    if (oContr.campo_origen != 0) {
                        campo = oContr.campo_origen;
                    };
                    Sys.Application.add_init(function () {
                        $create(Sys.Extended.UI.AutoCompleteBehavior, {
                            "completionInterval": 500,
                            "completionListCssClass": "autoCompleteList",
                            "completionListItemCssClass": "autoCompleteListItem",
                            "contextKey": campo,
                            "delimiterCharacters": "",
                            "enableCaching": false,
                            "highlightedItemCssClass": "autoCompleteSelectedListItem",
                            "id": "autocompleteextender_" + lIndex.toString() + "_" + campo.toString(),
                            "minimumPrefixLength": 0,
                            "serviceMethod": "DevolverValores",
                            "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                            "useContextKey": true
                        }, {
                            "shown": resetPosition
                        }, null, $get(entryActual));
                    });
                }
                else if (oContr.tipoGS == 118 || oContr.tipoGS == 119) {
                    if (oContr.tipoGS == 119) {    //C�digo art�culo
                        Sys.Application.add_init(function () {
                            $create(Sys.Extended.UI.AutoCompleteBehavior, {
                                "completionInterval": 500,
                                "completionListCssClass": "autoCompleteList",
                                "completionListItemCssClass": "autoCompleteListItem",
                                "enabled": true,
                                "minimumPrefixLength": 3,
                                "delimiterCharacters": "",
                                "enableCaching": false,
                                "completionListHighlightedItemCssClass": "autoCompleteSelectedListItem",
                                "id": entryActual + "_ext",
                                "serviceMethod": "DevolverArticulos",
                                "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                                "useContextKey": true,
                                "completionSetCount": oContr.FilasAutoCompleteExtender,
                                "targetControlID": entryActual + "_t"
                            }, {
                                "populated": onCodListPopulated,
                                "shown": resetPosition
                            }, null, $get(entryActual + "_t"));
                        });
                    }
                    if (oContr.tipoGS == 118) { //Denominaci�n art�culo
                        Sys.Application.add_init(function () {
                            $create(Sys.Extended.UI.AutoCompleteBehavior, {
                                "completionInterval": 500,
                                "completionListCssClass": "autoCompleteList",
                                "completionListItemCssClass": "autoCompleteListItem",
                                "enabled": true,
                                "minimumPrefixLength": 3,
                                "delimiterCharacters": "",
                                "enableCaching": false,
                                "completionListHighlightedItemCssClass": "autoCompleteSelectedListItem",
                                "id": entryActual + "_ext",
                                "serviceMethod": "DevolverArticulos",
                                "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                                "useContextKey": true,
                                "completionSetCount": oContr.FilasAutoCompleteExtender,
                                "targetControlID": entryActual + "_t"
                            }, {
                                "itemSelected": onDenExtenderItemSelected,
                                "populated": onCodListPopulated,
                                "shown": resetPosition
                            }, null, $get(entryActual + "_t"));
                        });
                    }
                } else if (oContr.tipoGS == 0 && oContr.intro == 2) {    //Atributos de lista externa
                    Sys.Application.add_init(function () {
                        $create(Sys.Extended.UI.AutoCompleteBehavior, {
                            "completionInterval": 500,
                            "completionListCssClass": "autoCompleteList",
                            "completionListItemCssClass": "autoCompleteListItem",
                            "delimiterCharacters": "",
                            "enableCaching": false,
                            "highlightedItemCssClass": "autoCompleteSelectedListItem",
                            "id": entryActual + "_ext",
                            "minimumPrefixLength": 2,
                            "serviceMethod": "Obtener_Datos_ListaExterna_Extender",
                            "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                            "useContextKey": true,
                            "completionSetCount": oContr.FilasAutoCompleteExtender
                        }, {
                            "itemSelected": onListaExternaExtenderItemSelected,
                            "populated": onCodListPopulated,
                            "shown": resetPosition
                        }, null, $get(entryActual + "_t"));
                    });
                };
            }
        };
        //Si es un desglose que se abre en PopUp
        if (a[0] == "ucDesglose") {
            if (oContr.tipoGS == 118) {
                entryActual = a[0] + '_' + a[1] + '__tden';
            }
            else {
                entryActual = a[0] + '_' + a[1] + '__t';
            }
            re = /fsentry/;
            while (entryActual.search(re) >= 0) {
                entryActual = entryActual.replace(re, "fsdsentry_" + lIndex.toString() + "_");
            };
            campo = a[1];
            campo = campo.replace(re, "");
            if (oContr) {
                if (oContr.RecordarValores == true) {
                    if (oContr.campo_origen != 0) {
                        campo = oContr.campo_origen;
                    };
                    Sys.Application.add_init(function () {
                        $create(Sys.Extended.UI.AutoCompleteBehavior, {
                            "completionInterval": 500,
                            "completionListCssClass": "autoCompleteList",
                            "completionListItemCssClass": "autoCompleteListItem",
                            "contextKey": campo,
                            "delimiterCharacters": "",
                            "enableCaching": false,
                            "highlightedItemCssClass": "autoCompleteSelectedListItem",
                            "id": "autocompleteextender_" + lIndex.toString() + "_" + campo.toString(),
                            "minimumPrefixLength": 0,
                            "serviceMethod": "DevolverValores",
                            "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                            "useContextKey": true
                        }, {
                            "shown": resetPosition
                        }, null, $get(entryActual));
                    });
                }
                else if (oContr.tipoGS == 118 || oContr.tipoGS == 119) {
                    if (oContr.tipoGS == 119) {    //C�digo art�culo
                        Sys.Application.add_init(function () {
                            $create(Sys.Extended.UI.AutoCompleteBehavior, {
                                "completionInterval": 500,
                                "completionListCssClass": "autoCompleteList",
                                "completionListItemCssClass": "autoCompleteListItem",
                                "enabled": true,
                                "minimumPrefixLength": 3,
                                "delimiterCharacters": "",
                                "enableCaching": false,
                                "completionListHighlightedItemCssClass": "autoCompleteSelectedListItem",
                                "id": entryActual + "_ext",
                                "serviceMethod": "DevolverArticulos",
                                "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                                "useContextKey": true,
                                "completionSetCount": oContr.FilasAutoCompleteExtender,
                                "targetControlID": entryActual + "_t"
                            }, {
                                "populated": onCodListPopulated,
                                "shown": resetPosition
                            }, null, $get(entryActual + "_t"));
                        });
                    }
                    if (oContr.tipoGS == 118) { //Denominaci�n art�culo
                        Sys.Application.add_init(function () {
                            $create(Sys.Extended.UI.AutoCompleteBehavior, {
                                "completionInterval": 500,
                                "completionListCssClass": "autoCompleteList",
                                "completionListItemCssClass": "autoCompleteListItem",
                                "enabled": true,
                                "minimumPrefixLength": 3,
                                "delimiterCharacters": "",
                                "enableCaching": false,
                                "completionListHighlightedItemCssClass": "autoCompleteSelectedListItem",
                                "id": entryActual + "_ext",
                                "serviceMethod": "DevolverArticulos",
                                "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                                "useContextKey": true,
                                "completionSetCount": oContr.FilasAutoCompleteExtender,
                                "targetControlID": entryActual + "_t"
                            }, {
                                "itemSelected": onDenExtenderItemSelected,
                                "populated": onCodListPopulated,
                                "shown": resetPosition
                            }, null, $get(entryActual + "_t"));
                        });
                    }
                } else if (oContr.tipoGS == 0 && oContr.intro == 2) {    //Atributos de lista externa
                    Sys.Application.add_init(function () {
                        $create(Sys.Extended.UI.AutoCompleteBehavior, {
                            "completionInterval": 500,
                            "completionListCssClass": "autoCompleteList",
                            "completionListItemCssClass": "autoCompleteListItem",
                            "delimiterCharacters": "",
                            "enableCaching": false,
                            "highlightedItemCssClass": "autoCompleteSelectedListItem",
                            "id": entryActual + "_ext",
                            "minimumPrefixLength": 2,
                            "serviceMethod": "Obtener_Datos_ListaExterna_Extender",
                            "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                            "useContextKey": true,
                            "completionSetCount": oContr.FilasAutoCompleteExtender
                        }, {
                            "itemSelected": onListaExternaExtenderItemSelected,
                            "populated": onCodListPopulated,
                            "shown": resetPosition
                        }, null, $get(entryActual + "_t"));
                    });
                };
            }
        };
        //Aqui es donde se introducen en el array los controles	
        stemp = s;
        if (stemp.search(' id="') > 0) { //En FireFox los ids vienen con comillas
            tempinicio = stemp.search(' id="');
            tempinicio = tempinicio + 5;
        } else {
            tempinicio = stemp.search(" id="); //En IE los ids vienen sin comillas
            tempinicio = tempinicio + 4;
        };
        stemp2 = stemp.substr(tempinicio, stemp.length - tempinicio);
        tempfin = stemp2.search("__tbl");
        var mientry = stemp2.substr(0, tempfin);
        a = new Array();
        var entry1;
        var numero;
        a = mientry.split("_");
        entry1 = mientry;
        if ((a.length == 7) || (a.length == 4) || (a.length == 9)) {
            if (a.length == 7) { //cuando no es popup
                numero = a[5];
            } else {
                if (a.length == 4) { //Cuando es popup
                    numero = a[2];
                } else {
                    if (a.length == 9) { //Cuando el desglose no es una ventana emergente
                        numero = a[7];
                    };
                };
            };
            var cadenaareemplazar = "_fsdsentry_" + numero + "_";
            entry1 = entry1.replace(cadenaareemplazar, "_fsdsentry_" + lPrimeraLinea.toString() + "_");
            cadenaareemplazar = null;
            arraynuevafila[insertarnuevoelemento][0] = mientry;
            arraynuevafila[insertarnuevoelemento][1] = entry1;
            insertarnuevoelemento = insertarnuevoelemento + 1;
        };
        if ((entry1) && (mientry != '')) {
            var entryACopiar = entry1.replace("fsdsentry_" + lPrimeraLinea.toString() + "_", "fsentry");
            oContr = fsGeneralEntry_getById(entryACopiar);
            if (oContr) {
                if (oContr.CampoOrigenVinculado == null) {
                    if ((oContr.tipoGS != 119) && (oContr.tipoGS != 118) && (oContr.tipoGS != 104) && (oContr.tipoGS != 127) && (oContr.tipoGS != 121) && (oContr.tipoGS != 116) && (oContr.intro == 1) && (oContr.readOnly == false)) {
                        var param;
                        if (a.length >= 6) {
                            param = a[0] + '$_' + a[2] + '$' + a[3] + '$' + a[4] + '_' + a[5] + '$' + a[6] + '$_t';
                        };
                        if (a[0] == "ucDesglose") {
                            param = a[0] + '$' + a[1] + '$_t';
                        };
                        var bClientBinding;
                        switch (oContr.tipoGS) {
                            case 101:
                                //Forma pago
                            case 102:
                                //Moneda
                            case 105:
                                //Unidad
                            case 142:
                                //Unidad de Pedido
                            case 107:
                                //Pais
                            case 108:
                                //Provincia
                            case 109:
                                //Destino
                            case 122:
                                //Departamento
                            case 123:
                                //Org. Compras
                            case 124:
                                //Centro
                            case 125:
                                //Almac�n
                            case 143:
                                //Proveedor ERP
                            case 151:
                                bClientBinding = true;
                                break;
                            default:
                                bClientBinding = false;
                                break;
                        };
                        if ((oContr.tipoGS == 0) && (oContr.tipo != 2) && (oContr.tipo != 3) && (oContr.tipo != 4)) {
                            bClientBinding = true;
                        };
                        var valores;
                        if (bClientBinding == false) {
                            var codigo = document.documentElement.innerHTML;
                            var posicion = codigo.search("Infragistics.Web.UI.WebDropDown");
                            if (posicion > 0) {
                                codigo = codigo.substring(posicion);
                                posicion = codigo.search(entryACopiar);
                                if (posicion > 0) {
                                    codigo = codigo.substring(posicion);
                                    posicion = codigo.search("{'0':");
                                    fin = codigo.search(']]}]');
                                    if ((posicion > 0) && (fin > 0)) {
                                        valores = codigo.substring(posicion, fin + 3);
                                    };
                                };
                            };
                            codigo = null;
                            fin = null;
                            posicion = null;
                        };
                        if (bClientBinding == true) {
                            Sys.Application.add_init(function () {
                                $create(Infragistics.Web.UI.WebDropDown, {
                                    "clientbindingprops": [
                                        [
                                            [, 1, 120, , , , , , , , 1, , 1, , 2, 0, , , 0, , 300, 120, , "", , , , 6, , 1, , , , , , , , , , 0, , , 1, 0, , , 0, , , , 1], {
                                                "c": {
                                                    "dropDownInputFocusClass": "igdd_FullstepValueDisplayFocus",
                                                    "dropDownButtonPressedImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonPressed.png",
                                                    "_pi": "[\"" + InfraStylePath + "/Fullstep/images/ig_ajaxIndicator.gif\",,,\"ig_FullstepPMWebAjaxIndicator\",,1,,,,\"ig_FullstepPMWebAjaxIndicatorBlock\",,3,,3,\"Async post\"]",
                                                    "pageCount": 0,
                                                    "vse": 0,
                                                    "textField": "Texto",
                                                    "controlAreaHoverClass": "ig_FullstepPMWebHover igdd_FullstepControlHover",
                                                    "controlAreaClass": "igdd_FullstepControlArea",
                                                    "valueField": "Valor",
                                                    "controlClass": "ig_FullstepPMWebControl igdd_FullstepControl",
                                                    "dropDownInputHoverClass": "ig_FullstepPMWebHover igdd_FullstepValueDisplayHover",
                                                    "dropDownItemDisabled": "igdd_FullstepListItemDisabled",
                                                    "uid": mientry + "_t",
                                                    "nullTextCssClass": "igdd_FullstepNullText",
                                                    "controlAreaFocusClass": "igdd_FullstepControlFocus",
                                                    "dropDownItemHover": "igdd_FullstepListItemHover",
                                                    "dropDownButtonNormalImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButton.png",
                                                    "dropDownInputClass": "igdd_FullstepValueDisplay",
                                                    "controlDisabledClass": "ig_FullstepPMWebDisabled igdd_FullstepControlDisabled",
                                                    "dropDownButtonDisabledImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonDisabled.png",
                                                    "dropDownItemClass": "igdd_FullstepListItem",
                                                    "pi": "[,,9,,1,1,,80,,,,3,,3,\"Async post\"]",
                                                    "dropDownItemSelected": "igdd_FullstepListItemSelected",
                                                    "ocs": 1,
                                                    "dropDownButtonClass": "igdd_FullstepDropDownButton",
                                                    "dropDownItemActiveClass": "igdd_FullstepListItemActive",
                                                    "dropDownButtonHoverImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonHover.png",
                                                    "dropDownInputDisabledClass": "ig_FullstepPMWebDisabled igdd_FullstepValueDisplayDisabled"
                                                }
                                            }], , [{}],
                                        ["InputKeyDown:WebDropDown_KeyDown", "DropDownOpening:WebDropDown_DataCheck", "SelectionChanged:WebDropDown_SelectionChanging", "Blur:WebDropDown_Focus", "ValueChanged:WebDropDown_valueChanged"],
                                        []
                                    ],
                                    "id": mientry + "__t",
                                    "name": "_t"
                                }, null, null, $get(mientry + "__t"));
                            });
                        } else {
                            Sys.Application.add_init(function () {
                                $create(Infragistics.Web.UI.WebDropDown, {
                                    "id": mientry + "__t",
                                    "name": "_t",
                                    "props": [
                                    [
                                        [, 1, 160, , , , , -1, , , 1, , 1, , 2, 0, , , 0, 0, , 120, , "", , , , 6, , 1, , , , , , , , 0, , 0, , -1, 0, 0, , 0, 0, , , ], {
                                            "c": {
                                                "controlDisabledClass": "ig_FullstepPMWebDisabled igdd_FullstepControlDisabled",
                                                "controlAreaClass": "igdd_FullstepControlArea",
                                                "controlAreaHoverClass": "ig_FullstepPMWebHover igdd_FullstepControlHover",
                                                "vse": 0,
                                                "dropDownInputHoverClass": "ig_FullstepPMWebHover igdd_FullstepValueDisplayHover",
                                                "dropDownItemHover": "igdd_FullstepListItemHover",
                                                "dropDownButtonNormalImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButton.png",
                                                "pi": "[]",
                                                "dropDownButtonHoverImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonHover.png",
                                                "dropDownButtonPressedImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonPressed.png",
                                                "dropDownInputClass": "igdd_FullstepValueDisplay",
                                                "dropDownItemClass": "igdd_FullstepListItem",
                                                "dropDownInputDisabledClass": "ig_FullstepPMWebDisabled igdd_FullstepValueDisplayDisabled",
                                                "controlClass": "ig_FullstepPMWebControl igdd_FullstepControl",
                                                "dropDownInputFocusClass": "igdd_FullstepValueDisplayFocus",
                                                "dropDownButtonDisabledImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonDisabled.png",
                                                "pageCount": 4,
                                                "controlAreaFocusClass": "igdd_FullstepControlFocus",
                                                "dropDownItemDisabled": "igdd_FullstepListItemDisabled",
                                                "ocs": 1,
                                                "nullTextCssClass": "igdd_FullstepNullText",
                                                "dropDownButtonClass": "igdd_FullstepDropDownButton",
                                                "uid": param,
                                                "dropDownItemSelected": "igdd_FullstepListItemSelected",
                                                "dropDownItemActiveClass": "igdd_FullstepListItemActive"
                                            }
                                        }], , eval("[" + valores + "]"), ["DropDownOpening:WebDropDown_DropDownOpening", "SelectionChanged:WebDropDown_SelectionChanging", "Blur:WebDropDown_Focus", "ValueChanged:WebDropDown_valueChanged"]
                                    ]
                                }, null, null, $get(mientry + "__t"));
                            });
                        };
                        Sys.Application.add_load(function () {
                            $find(mientry + "__t").behavior.set_enableMovingTargetWithSource(false)
                        });
                        if (oContrAr) {
                            var valorSeleccionado = oContrAr.getDataValue();
                            setTimeout('copiarSeleccionWebDropDown("' + mientry + '__t","' + JSText(valorSeleccionado) + '")', 500);
                            valorSeleccionado = null;
                        };
                        valores = null;
                        param = null;
                    };
                };
            };
            entryACopiar = null
        };
        sArchivo = null;
        entryActual = null;
        stemp = null;
        stemp2 = null;
        tempinicio = null;
        tempfin = null;
        re = null;
        oContrAr = null;
        se = null;
        oContr = null;
        campo = null;
        a = null;
        mientry = null;
        entry1 = null;
        numero = null;
    };
    sClase = null;
    sMovible = null;
    lPrimeraLinea = null;
    s = "<input type=hidden name=" + sRoot + "_" + lIndex + "_Deleted id=" + sRoot + "_" + lIndex + "_Deleted value='no'>";
    oTD.insertAdjacentHTML("beforeEnd", s);
    oTD = null;
    oTR = null;
    s = null;
    var iNumCeldas; //indica el n�mero de celdas de la tabla 
    var iCeldaIni;
    if (ProveCod) {
        iCeldaIni = 0;
        iNumCeldas = document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells.length; // si estamos a�adiendo un proveedor para emitirle un certificado est�n todas las celdas
    } else {
        iCeldaIni = 1;
        iNumCeldas = document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells.length - 1; //si es una linea de desglose estan todas menos la �ltima que es el bot�n de eliminar
    };
    for (i = 0; i < iNumCeldas; i++) {
        re = /fsentry/;
        var s2 = eval("arrEntrys_" + idCampo + "[i]");
        if (!s2) {
            continue;
        };
        if (s2.search("arrCalculados") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_");
        };
        if (s2.search("arrObligatorios") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_");
        };
        if (s2.search("arrArticulos") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_");
            if (!(s2.search("arrArticulos\[iNumArts][0]=\"\"") > 0));
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_");
        };
        if (s2.search("arrPres1") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_");
        };
        if (s2.search("arrPres2") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_");
        };
        if (s2.search("arrPres3") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_");
        };
        if (s2.search("arrPres4") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_");
        };
        s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_");
        s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_");
        if (s2.search(", 104,") > 0 || s2.search(",108,") > 0 || s2.search(", 114,") > 0) {
            s2 = s2.replace(re, "fsdsentryx" + lIndex.toString() + "x"); //en el caso del art�culo,de la provincia y del contacto del proveedor, la grid del dropdown es �nica para cada fila
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_"); //tambien es �nico el campo dependiente (dependentfield)
        };
        eval(s2);
        if (s2.search("arrObligatorios") > 0) {
            var oEntry = fsGeneralEntry_getById(arrObligatorios[arrObligatorios.length - 1]);
            oEntry.basedesglose = false;
            oEntry = null;
        };
        s2 = null;
        re = null;
    };
    iNumCeldas = null;
    //Esto ocurre al a�adir un proveedor al que emitir un certificado
    //A�ade los datos del proveedor:
    oEntry = fsGeneralEntry_getById(sRoot + "_fsdsentry_" + lIndex.toString() + "_PROVE");
    if (oEntry) {
        oEntry.setValue(ProveCod + ' - ' + ProveDen);
        oEntry.setDataValue(ProveCod);
    };
    //A�ade los datos del contacto:
    oEntry = fsGeneralEntry_getById(sRoot + "_fsdsentry_" + lIndex.toString() + "_CON")
    if (oEntry) {
        oEntry.setValue(sDenContacto);
        oEntry.setDataValue(lIdContacto);
    }
    oEntry = null;
    //Se hace la asignacion de valores para la nueva fila
    var o, o1;
    for (i = 0; i < insertarnuevoelemento; i++) {
        o = fsGeneralEntry_getById(arraynuevafila[i][0]);
        o1 = fsGeneralEntry_getById(arraynuevafila[i][1]);
        if (o) {
            o.nColumna = i + iCeldaIni;
            o.nLinea = lLineaGrid;
            if (o.errMsg) {
                o.errMsg = o.errMsg.replace('\n', '\\n');
                o.errMsg = o.errMsg.replace('\n', '\\n');
            };
            if (o1) {
                o.PoseeDefecto = o1.PoseeDefecto;
                o.TextoValorDefecto = o1.TextoValorDefecto;
                o.DataValorDefecto = o1.DataValorDefecto;
            };
        };
        if ((o != null) && (o1 != null) && (lLineaGrid != 1)) {
            //esto tiene que estar antes del setValue, si no, coge los decimales de las opciones del usuario
            if (o.tipoGS == 4) { //Cantidad
                if (o1.NumeroDeDecimales == null) {
                    o.Editor.decimals = 15;
                } else {
                    o.Editor.decimals = o1.NumeroDeDecimales;
                };
            };
            //campo de tipo texto medio o largo
            if (o1.getValue() != "") { // si no esta en blanco se le asigna el valor al campo de la linea copiada
                o.setValue(o1.getValue());
            };
            if ((o.tipoGS == 0) && (o.intro == 1) && (o.readOnly == false)) {
                var dropDown = $find(o.Editor.id);
                if (dropDown && dropDown.set_currentValue) {
                    if (o1.getValue() == null) {
                        dropDown.set_currentValue('', true);
                    } else {
                        copiarSeleccionWebDropDown(o.Editor.id, o1.getDataValue());
                        dropDown.set_currentValue(o1.getValue(), true);
                    };
                };
                dropDown = null;
            };
            if ((o.tipoGS != 0) && (o.tipoGS != 119) && (o.tipoGS != 118) && (o.tipoGS != 104) && (o.tipoGS != 127) && (o.tipoGS != 121) && (o.tipoGS != 116) && (o.intro == 1) && (!o.readOnly)) {
                var dropDown = $find(o.Editor.id);
                if (dropDown && dropDown.set_currentValue) {
                    if (o1.getValue() == null || o1.getValue() == '') {
                        dropDown.set_currentValue('', true);
                    } else {
                        copiarSeleccionWebDropDown(o.Editor.id, o1.getDataValue())
                        dropDown.set_currentValue(o1.getValue(), true);
                        if (o.tipoGS == 109) {
                            if (o1.getValue() != null) {
                                document.getElementById("igtxt" + o.Editor2.ID).style.visibility = "visible";
                            }
                        }
                        if (o.tipoGS == 151) {
                            if ((o1.getDataValue() != null) && (o.getDataValue() == null)) {
                                //set_currentValue no ha puesto el DataValue Pq no hay dropDown.datos
                                o.setDataValue(o1.getDataValue());
                            }
                        }
                    };
                };
                dropDown = null;
            };
            if (o.tipoGS == 105) { //Unidad
                if (o.idEntryCANT) o.idEntryCANT = o.idEntryCANT.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
            };
            if (o.tipoGS == 4) { //Cantidad
                o.NumeroDeDecimales = o1.NumeroDeDecimales;
                o.UnidadOculta = o1.UnidadOculta;
            };
            if ((o.tipoGS == 119) || (o.tipoGS == 118) || (o.tipoGS == 104)) {
                if (o.tipoGS == 118) { //Denominacion
                    o.codigoArticulo = o1.codigoArticulo;
                    o.articuloGenerico = o1.articuloGenerico;
                    if (o.Dependent) { //Cod Articulo Nuevo
                        o.Dependent.value = o1.Dependent.value;
                    };
                    if (o1.dependentfield) {
                        o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                    };
                } else if ((o.tipoGS == 119) || (o.tipoGS == 104)) {
                    if (o.Dependent) { //Cod Articulo Nuevo
                        o.Dependent.value = o1.Dependent.value;
                    };
                    if (o.tipoGS == 104) {
                        if (o1.dependentfield) {
                            o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                        };
                    };
                };
                if (o.idEntryPREC) {
                    o.idEntryPREC = o.idEntryPREC.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
                if (o.idEntryPROV) {
                    o.idEntryPROV = o.idEntryPROV.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
                if (o.idEntryCANT) {
                    o.idEntryCANT = o.idEntryCANT.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
                if (o.idEntryUNI) {
                    o.idEntryUNI = o.idEntryUNI.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
                if (o.IdEntryUniPedido) {
                    o.IdEntryUniPedido = o.IdEntryUniPedido.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
                if (o.idEntryCC) {
                    o.idEntryCC = o.idEntryCC.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
                if (o.idDataEntryDependent) {
                    o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
                if (o.idDataEntryDependent2) {
                    o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
                if (o.idDataEntryDependentCentroCoste) {
                    o.idDataEntryDependentCentroCoste = o.idDataEntryDependentCentroCoste.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
                if (o.idEntryProveSumiArt) o.idEntryProveSumiArt = o.idEntryProveSumiArt.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                if (o.OrgComprasDependent) {
                    o.OrgComprasDependent.value = o1.OrgComprasDependent.value;
                };
                if (o.CentroDependent) {
                    o.CentroDependent.value = o1.CentroDependent.value;
                };
                if (o.UnidadDependent) {
                    o.UnidadDependent.value = o1.UnidadDependent.value;
                };
                if (o.IdMaterialOculta) {
                    o.IdMaterialOculta = o.IdMaterialOculta.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
                if (o.IdCodArticuloOculta) {
                    o.IdCodArticuloOculta = o.IdCodArticuloOculta.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
            } else if ((o.tipoGS == 9) || (o.tipoGS == 100)) { // Precio Unitario = 9 Y Proveedores = 100
                o.cargarUltADJ = o1.cargarUltADJ;
                if (o.tipoGS == 100) {
                    if (o.dependentfield) {
                        o.dependentfield = o.dependentfield.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                    };
                    if (o.idDataEntryDependent2) {
                        o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                    };
                };
            } else if ((o.tipoGS == 6) || (o.tipoGS == 7)) {
                if (o.idDataEntryDependent) {
                    o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
            } else if (o.tipoGS == 103) {
                if (o.idDataEntryDependent2) {
                    o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
            };
            if (o.tipoGS == 123) {
                if (o.dependentfield) {
                    o.dependentfield = o.dependentfield.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
                if (o.idDataEntryDependent2) { //org-centro
                    o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
            };
            if (o.tipoGS == 107) { //pais-provincia
                if (o.dependentfield) {
                    o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                };
            };
            if (o.tipoGS == 124 || (o.tipoGS == 109)) { //centro-almacen / destino-almacen
                if (o.idDataEntryDependent3) {
                    o.idDataEntryDependent3 = o.idDataEntryDependent3.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                };
            }
            if ((o.tipoGS == 123) || (o.tipoGS == 124)) {
                if (o.idDataEntryDependent) {
                    o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
                o.setDataValue(o1.getDataValue());
            } else if ((o.tipoGS == 10) || (o.tipoGS == 12)) {
                o.setValue('');
                o.setDataValue('');
            } else if ((o.tipoGS == 11)) {
                o.setValue('');
                o.setDataValue('');
            } else if ((o.tipoGS == 43)) {
                o.setValue(arrEstadosInternos[1][1]);
                o.setDataValue(1);
            } else if (o.tipoGS == 1020) {
                o.setValue('');
                o.setDataValue('');
            } else if (o.tipoGS == 45) {
                if (o1) {
                    o.setValue(o1.getValue());
                    o.setDataValue(o1.getDataValue());
                    o.MonRepercutido = o1.MonRepercutido;
                    o.CambioRepercutido = o1.CambioRepercutido;
                    if (o.readOnly == false) {
                        var lnk = document.getElementById(o.dependentfield);
                        lnk.innerHTML = o1.MonRepercutido;
                        lnk = null;
                    };
                };
            } else if (o.tipoGS == 2000) {
                o.setValue(o1.getValue());
                o.setDataValue(o1.dataValue);
            } else if (o.tipoGS == 3000) {
                o.setValue(o.nLinea);
            } else if (o.tipoGS == 151) {
                if ((o.IdEntryPartidaPlurianual) && (o.EntryPartidaPlurianualCampo == 0)) {
                    o.IdEntryPartidaPlurianual = o.IdEntryPartidaPlurianual.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                };
            } else if (o1.tipo == 8) {
                //Si es un adjunto se tiene que generar un nuevo id para el adjunto/s copiados
                var arrayAdjuntos = new Array();
                arrayAdjuntos[0] = "";
                arrayAdjuntos[1] = "";
                arrayAdjuntos = o1.getDataValue();
                CreateXmlHttp();
                if (xmlHttp && (arrayAdjuntos[0] != "" || arrayAdjuntos[1] != "")) {
                    if (o1.PoseeDefecto == true) var params = "accion=9&AdjuntosAct=" + arrayAdjuntos[0] + "&AdjuntosNew=" + arrayAdjuntos[1];
                    else var params = "accion=8&AdjuntosAct=" + arrayAdjuntos[0] + "&AdjuntosNew=" + arrayAdjuntos[1];
                    xmlHttp.open("POST", rutaPM + "GestionaAjax.aspx", false);
                    xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                    xmlHttp.send(params);
                    var retorno;
                    if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                        retorno = xmlHttp.responseText;
                        arrayAdjuntos[0] = "";
                        arrayAdjuntos[1] = retorno;
                        o.setDataValue(arrayAdjuntos);
                    };
                } else {
                    o.setDataValue(arrayAdjuntos);
                };
            } else {
                o.setValue(o1.getValue());
                o.setDataValue(o1.getDataValue());
            };
        } else { // else ---> if ((o != null)&&(o1 != null))
            if ((o != null) && (o1 == null) && (o.readOnly == true)) {
                o.setDataValue(o.getDataValue()); //si no, se guarda en blanco
            };
        };
        if ((o.tipoGS == 8 || o.tipo == 8) && (lLineaGrid == 1)) { //Fichero ADJUNTO				
            var vaciar = new Array();
            vaciar[0] = "";
            vaciar[1] = "";
            o.setValue('');
            o.setDataValue(vaciar);
            vaciar = null;
        } else {
            if ((o.tipoGS == 119) || (o.tipoGS == 104) || (o.tipoGS == 118)) {
                o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                if ((o.tipoGS == 119) || (o.tipoGS == 104)) { //Cod Articulo Nuevo o CodArt Antiguo
                    if (o.tipoGS == 104) {
                        o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                    };
                };
                if (o.idDataEntryDependent) {
                    if (o.idDataEntryDependent.search("fsentry_") < 0) { //La organizacion de compras de encuentra fuera del desglose
                        oEntry = fsGeneralEntry_getById(o.idDataEntryDependent);
                        if ((oEntry) && (oEntry.tipoGS == 123)) oEntry.idDataEntryDependent = arraynuevafila[i][0];
                        oEntry = null;
                    };
                    o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
                if (o.idDataEntryDependent2) {
                    o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
                if (o.idEntryPREC) {
                    o.idEntryPREC = o.idEntryPREC.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
                if (o.idEntryPROV) {
                    o.idEntryPROV = o.idEntryPROV.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                }
                if (o.idEntryCANT) {
                    o.idEntryCANT = o.idEntryCANT.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
                if (o.idEntryUNI) {
                    o.idEntryUNI = o.idEntryUNI.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
                if (o.IdEntryUniPedido) {
                    o.IdEntryUniPedido = o.IdEntryUniPedido.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
                if (o.idEntryCC) {
                    o.idEntryCC = o.idEntryCC.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
                if (o.IdMaterialOculta) {
                    o.IdMaterialOculta = o.IdMaterialOculta.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
                if (o.IdCodArticuloOculta) {
                    o.IdCodArticuloOculta = o.IdCodArticuloOculta.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
            } else if ((o.tipoGS == 6) || (o.tipoGS == 7)) {
                if (o.idDataEntryDependent) {
                    o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
            } else if ((o.tipoGS == 9) || (o.tipoGS == 100)) { // Precio Unitario = 9 Y Proveedores = 100
                if (o.tipoGS == 100) {
                    if (o.dependentfield) {
                        o.dependentfield = o.dependentfield.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                    };
                    if (o.idDataEntryDependent2) {
                        o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                    };
                };
            } else if (o.tipoGS == 103) {
                if (o.idDataEntryDependent2) {
                    o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
            };
            if (o.tipoGS == 123) {
                if (o.dependentfield) {
                    o.dependentfield = o.dependentfield.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
                if (o.idDataEntryDependent2) { //org-centro
                    o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
            };
            if (o.tipoGS == 107) { //pais-provincia
                if (o.dependentfield) {
                    o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                };
            };
            if (o.tipoGS == 124 || (o.tipoGS == 109)) { //centro-almacen / destino-almacen
                if (o.idDataEntryDependent3) {
                    o.idDataEntryDependent3 = o.idDataEntryDependent3.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                };
            };
            if ((o.tipoGS == 123) || (o.tipoGS == 124)) {
                if (o.idDataEntryDependent) {
                    o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                };
                if (o.tipoGS == 124) {
                    var oEntryArt = fsGeneralEntry_getById(o.idDataEntryDependent);
                    if ((oEntryArt) && (oEntryArt.idDataEntryDependent)) {
                        if (oEntryArt.idDataEntryDependent.search("fsentry_") < 0) { //La organizacion de compras de encuentra fuera del desglose
                            oEntry = fsGeneralEntry_getById(oEntryArt.idDataEntryDependent);
                            if ((oEntry) && (oEntry.tipoGS == 123)) oEntry.idDataEntryDependent2 = arraynuevafila[i][0];
                            oEntry = null;
                        };
                    };
                    oEntryArt = null;
                };
            };
            if (lLineaGrid == 1) {
                o.setValue('');
                o.setDataValue('');
            };
        };
        if (o.masterField && o.masterField != '') {
            o.masterField = o.masterField.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
        };
        if (o.dependentfield && o.dependentfield != '') {
            //Se han usado tanto 'fsentry_' como con 'fsentry'. Ante la duda, controlamos ambos casos
            o.dependentfield = o.dependentfield.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
            o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
        };
        o = null;
        o1 = null;
    };
    lIndex = null;
    lLineaGrid = null;
    Salta = null;
    arraynuevafila = null;
    insertarnuevoelemento = null;
    _arDependenceRelations = null;
    iCeldaIni = null;
};
function show_help(idcampo, instancia, solicitud) {
    var newWindow;
    newWindow = window.open(rutaFS + "_common/AyudaCampo.aspx?Campo=" + idcampo + "&Instancia=" + (instancia ? instancia : "") + "&Solicitud=" + (solicitud ? solicitud.toString() : ""), "_blank", "width=900,height=270,status=yes,resizable=yes,top=200,left=200");
    newWindow.focus();
};
//Descripci�n:	Abre la ventana de texto largo para un dataentry
//Param. Entrada: 
//	otxt		nombre del dataentry
//	svalor		valor que tiene asociado
//  sTextoAceptar: Literal que saldr� en el bot�n para Aceptar
//  sTextoCancelar: Literal que saldr� en el bot�n para Cancelar
//  sTextoCerrar: Literal que saldr� en el bot�n para Cerrar
//	oEvent	    Evento producido
//Param. Salida: -
//LLamada:	javascript introducido para responder al onclick de todos los campos de texto medio
//Tiempo:	0	
function show_popup(oTxt, sValor, sTextoAceptar, sTextoCancelar, sTextoCerrar, oEvent, windowTitle) {
    if ((ggWinPopUp == null) || (ggWinPopUp.closed)) {
        ggWinPopUp = window.open("about:blank", "winPopUp", "width=775,height=650,status=no,resizable=1,top=30,left=200");
        ggWinPopUp.focus()
        ggWinPopUp.opener = self;

        Build(oTxt, sValor, sTextoAceptar, sTextoCancelar, sTextoCerrar, windowTitle)
    }
};
//Descripci�n:	Abre la ventana de texto medio para un dataentry
//Param. Entrada: 
//	otxt		nombre del dataentry
//	svalor		valor que tiene asociado
//  sTextoAceptar: Literal que saldr� en el bot�n para Aceptar
//  sTextoCancelar: Literal que saldr� en el bot�n para Cancelar
//  sTextoCerrar: Literal que saldr� en el bot�n para Cerrar
//	oEvent	    Evento producido
//Param. Salida: -
//LLamada:	javascript introducido para responder al onclick de todos los campos de texto medio
//Tiempo:	0	
function show_popup_texto_medio(oTxt, sValor, sTextoAceptar, sTextoCancelar, sTextoCerrar, oEvent, windowTitle) {
    if ((ggWinPopUp == null) || (ggWinPopUp.closed)) {
        CerrarDesplegables();
        ggWinPopUp = window.open("about:blank", "winPopUp", "width=350,height=215,status=no,resizable=yes,top=200,left=200");
        ggWinPopUp.focus();
        ggWinPopUp.opener = self;
        Build_texto_medio(oTxt, sValor, sTextoAceptar, sTextoCancelar, sTextoCerrar, windowTitle);
    };
};
function validar_fechas_acciones() {
    var fechasCorrectas = true;
    //prompt ("codigo fuente", document.body.innerHTML)
    for (arrInput in arrInputs) {
        oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
        if (oEntry) {
            var oEntryLimite = null;
            var FechaLimite = null;
            var FechaInput = null;

            if (oEntry.idCampoPadre) //Este es un input perteneciente a un desglose
            {
                re = /fsdsentry/
                if (oEntry.id.search(re) >= 0) //y no es el input base del desglose
                {
                    if (oEntry.tipoGS == 36) {
                        FechaInput = oEntry.getValue();
                        oEntryLimite = fsGeneralEntry_getById(oEntry.validationcontrol);
                        if (oEntryLimite) {
                            FechaLimite = oEntryLimite.getValue();
                        }
                        if (FechaInput != null && FechaLimite != null) {
                            fechasCorrectas = fechasCorrectas && (FechaInput <= FechaLimite);
                        }
                    }
                }
            } else {
                if (oEntry.tipo == 9 && oEntry.tipoGS != 136) //desglose
                {
                    for (k = 1; k <= document.getElementById(oEntry.id + "__numTotRows").value; k++) {
                        if (document.getElementById(oEntry.id + "__" + k.toString() + "__Deleted")) {
                            for (arrCampoHijo in oEntry.arrCamposHijos) {
                                var oHijoSrc = document.getElementById(oEntry.id + "__" + oEntry.arrCamposHijos[arrCampoHijo]);
                                if (oHijoSrc) {
                                    if (oHijoSrc.tipoGS == 36) {
                                        var oHijo = document.getElementById(oEntry.id + "__" + k.toString() + "__" + oEntry.arrCamposHijos[arrCampoHijo]);
                                        if (oHijo) {
                                            var sFecha = oHijo.value;
                                            if (sFecha) {
                                                var partesFecha = sFecha.split("-");
                                                FechaInput = new Date(partesFecha[0], (partesFecha[1] - 1), partesFecha[2], partesFecha[3], partesFecha[4], partesFecha[5]);
                                                oEntryLimite = fsGeneralEntry_getById("NoConfFECLIM_" + oEntry.idCampo);
                                                if (oEntryLimite) {
                                                    FechaLimite = oEntryLimite.getValue();
                                                }
                                                if (FechaInput != null && FechaLimite != null) {
                                                    fechasCorrectas = fechasCorrectas && (FechaInput <= FechaLimite);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return fechasCorrectas;
};
function ddArtsPreInitDropDown(param) {
    //fsdsentry_1_1792###uwtGrupos__ctl0__ctl0###uwtGrupos__ctl0__ctl0_fsdsentry_1_1793
    var oGrid = new Object();
    var arrParam = param.split("###");
    oGrid.ID = arrParam[1];
    oGrid.IdEntryMaterial = arrParam[0];
    oGrid.Editor = arrParam[2];
    oGridsDesglose[oGridsDesglose.length] = oGrid;

}

/*
''' <summary>
''' Abre la ventana de articulos favoritos para un dataentry art�culo
''' </summary>
''' <param name="oEdit">Dataentry Art�culo</param>
''' <param name="text">texto a buscar como parte de la denominaci�n del art�culo</param>
''' <param name="oEvent">Evento click</param>        
''' <remarks>Llamada desde:javascript introducido para responder al onclick de todos los campos de tipo art�culo favorito; Tiempo m�ximo:0</remarks>
*/
function ddArticuloFavopenDropDownEvent(oEdit, text, oEvent) {
    var x = 0;
    var y = 0;
    var oEntry;
    oEntry = oEdit.fsEntry;
    var posicion = showPosition(oEdit.Element, document.getElementById("ddFavoritos"), 300, 200, 5);
    x = posicion[0];
    y = posicion[1];
    mostrarFRAME("ddFavoritos", x, y, 300, 200)

    var sMaterial
    if (oEntry.dependentfield) {
        var oMaterial = fsGeneralEntry_getById(oEntry.dependentfield)
        if (oMaterial) {
            sMaterial = oMaterial.getDataValue()
        }
    }
    var sCodOrgCompras;
    sCodOrgCompras = "";
    var oOrgCompras;
    if (oEdit.fsEntry.idDataEntryDependent) {
        oOrgCompras = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent);
        if (oOrgCompras) {
            sCodOrgCompras = oOrgCompras.getDataValue();
            if (sCodOrgCompras == "") sCodOrgCompras = "-1";
        } else if ((oEdit.fsEntry.OrgComprasDependent) && (oEdit.fsEntry.OrgComprasDependent.value)) {
            oOrgCompras = fsGeneralEntry_getById(oEdit.fsEntry.OrgComprasDependent.value);
            sCodOrgCompras = oEdit.fsEntry.OrgComprasDependent.value;
            if (sCodOrgCompras == "") sCodOrgCompras = "-1";
        };
    } else {
        if ((oEdit.fsEntry.OrgComprasDependent) && (oEdit.fsEntry.OrgComprasDependent.value)) {
            oOrgCompras = fsGeneralEntry_getById(oEdit.fsEntry.OrgComprasDependent.value);
            sCodOrgCompras = oEdit.fsEntry.OrgComprasDependent.value;
            if (sCodOrgCompras == "") sCodOrgCompras = "-1";
        } else {
            p = window.opener;
            if (p) {
                if (p.sDataEntryOrgComprasFORM) { // Si antes que un desglose de pop-up hay un dataEntry ORGANIZACION COMPRAS
                    oOrgCompras = p.fsGeneralEntry_getById(p.sDataEntryOrgComprasFORM);
                    if (oOrgCompras) {
                        sCodOrgCompras = oOrgCompras.getDataValue();
                        if (sCodOrgCompras == "") sCodOrgCompras = "-1";
                    };
                } else {
                    if (p.sCodOrgComprasFORM) {
                        sCodOrgCompras = p.sCodOrgComprasFORM;
                    };
                };
            };
        };
    };
    sCodCentro = "";
    var oCentro;
    if (oEdit.fsEntry.idDataEntryDependent2) {
        oCentro = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent2);
        if (oCentro) {
            sCodCentro = oCentro.getDataValue();
            if (sCodCentro == "") sCodCentro = "-1";
        } else {
            if ((oEdit.fsEntry.CentroDependent) && (oEdit.fsEntry.CentroDependent.value)) {
                oCentro = fsGeneralEntry_getById(oEdit.fsEntry.CentroDependent.value);
                sCodCentro = oEdit.fsEntry.CentroDependent.value;
                if (sCodCentro == "") sCodCentro = "-1";
            };
        };
    } else {
        if ((oEdit.fsEntry.CentroDependent) && (oEdit.fsEntry.CentroDependent.value)) {
            oCentro = fsGeneralEntry_getById(oEdit.fsEntry.CentroDependent.value)
            sCodCentro = oEdit.fsEntry.CentroDependent.value
            if (sCodCentro == "") sCodCentro = "-1";
        } else {
            p = window.opener;
            if (p) {
                if (p.sDataEntryCentroFORM) { // Si antes que un desglose de pop-up hay un dataEntry CENTRO
                    oCentro = p.fsGeneralEntry_getById(p.sDataEntryCentroFORM);
                    if (oCentro) {
                        sCodCentro = oCentro.getDataValue();
                        if (sCodCentro == "") sCodCentro = "-1";
                    };
                } else {
                    if (p.sCodCentroFORM) {
                        sCodCentro = p.sCodCentroFORM;
                    }
                };
            }
        }
    };

    var sCodCentroCoste = "";
    var oCentroCoste;
    if (oEdit.fsEntry.ActivoSM) {
        if (oEdit.fsEntry.idDataEntryDependentCentroCoste) {
            oCentroCoste = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependentCentroCoste);
            if (oCentroCoste) {
                sCodCentroCoste = oCentroCoste.getDataValue();
            }
        } else {
            p = window.opener;
            if (p) {
                if (p.sDataEntryCentroCosteFORM) { // Si antes que un desglose de pop-up hay un dataEntry CENTRO
                    oCentroCoste = p.fsGeneralEntry_getById(p.sDataEntryCentroCosteFORM);
                    if (oCentroCoste) {
                        sCodCentroCoste = oCentroCoste.getDataValue();
                    }
                }
            }
        }
    }

    //Proveedor que suministra el art�culo    
    var sProveSumiArt = '';
    if (oEdit.fsEntry.idEntryProveSumiArt) {
        var o = fsGeneralEntry_getById(oEdit.fsEntry.idEntryProveSumiArt);
        if (o.CargarProveSumiArt) {
            sProveSumiArt = o.getDataValue();
        }
    }
    var yaOrgCompras = 0;
    var yaCentro = 0;
    var yaCentroCoste = 0;
    if (oOrgCompras) yaOrgCompras = 1;
    if (oCentro) {
        //Asociado al artic de la linea desglose esta el centro: No buscar. La org compra puede no estar asociada pq esta fuera del desglose-> buscar org pero no centro.
        //Asociado al artic q es campo (no desglose) esta el centro: No buscar. 
        yaCentro = 1;
    };
    if (oCentroCoste) yaCentroCoste = 1;
    for (arrInput in arrInputs) {
        var o = fsGeneralEntry_getById(arrInputs[arrInput])
        if (o) {
            //Por si te han puesto la org y centro en otro grupo
            if (!oOrgCompras) {
                //Solo si no lo has obtenido a traves de oEdit
                if ((o.tipoGS == 123) && (yaOrgCompras == 0)) {
                    sCodOrgCompras = o.getDataValue()
                    if (sCodOrgCompras == "") sCodOrgCompras = "-1"
                    yaOrgCompras = 1;
                }
                if ((o.tipoGS == 124) && (yaCentro == 0)) {
                    sCodCentro = o.getDataValue()
                    if (sCodCentro == "") sCodCentro = "-1"
                    yaCentro = 1;
                }
                if ((o.tipoGS == 129) && (yaCentroCoste == 0)) {
                    sCodCentroCoste = o.getDataValue()
                    if (sCodCentroCoste == "") sCodCentroCoste = "-1"
                    yaCentroCoste = 1;
                }
            };
            //Si todo ya cargado , acabaste
            if ((yaOrgCompras == 1) && (yaCentro == 1) && (yaCentroCoste == 1)) {
                break;
            };
        };
    };
    var miUON = BuscarUonEnSolicitud(oEdit);

    //Si no hay unidad organizativa y s� centro de coste se asigna a la unidad organizativa las UONs correspondientes al centro de coste    
    if ((miUON == '' || miUON == '-1') && sCodCentroCoste != '') miUON = sCodCentroCoste.replace('#', ' - ')

    if (document.getElementById("ddFavoritos") != undefined) {
        document.getElementById("ddFavoritos").src = "../_common/articulosfavserver.aspx?FrameId=ddFavoritos&Entry=" + oEntry.id + "&Material=" + sMaterial + "&OrgCompras=" + sCodOrgCompras + "&Centro=" + sCodCentro + "&UnidadOrganizativa=" + miUON + "&ProveSumiArt=" + sProveSumiArt;
    }
};

var moEdit;
var motext;
var moEvent;
//Descripci�n:	Abre la ventana del buscador de art�culos
//Param. Entrada:
//	oEdit		nombre del dataentry
//	text		valor que tiene asociado
//	oEvent	    Evento producido
//Param. Salida: -
//LLamada:	javascript introducido para responder al onclick de todos los campos de tipo art�culo
//Tiempo:	0	
function ddArtsopenDropDownEvent(oEdit, text, oEvent) {
    oEdit = oEdit[0];
    moEdit = oEdit;
    motext = text;
    moEvent = oEvent;
    var nivelSeleccion = 0;
    var familiaMateriales = '';
    var oEntryMaterial;
    oEntryMaterial = getMaterialFormulario();

    //uwtGruposxctl0xctl0xfsdsentryx1x1793xdd
    for (i = 0; i < oGridsDesglose.length; i++) {
        if (oGridsDesglose[i].Editor + "__t" == oEdit.id || oGridsDesglose[i].Editor + "__tden" == oEdit.id) vGrid = oGridsDesglose[i]

    }
    var sClientID;
    if (vGrid.Editor + "__tden" == oEdit.id) {
        sClientID = vGrid.Editor + "__t"
    }
    else {
        sClientID = oEdit.id;
    }

    sGridName = vGrid.ID

    sGridName += '_tblDesglose'

    sAux = oEdit.id
    if (sAux.search("fsdsentry") >= 0) {
        sInit = sAux.substr(0, sAux.search("fsdsentry"))
        sAux = sAux.substr(sAux.search("fsdsentry"), sAux.length)
        arrAux = sAux.split("_")
        lIndex = arrAux[1]
    } else {
        sInit = sAux.substr(0, sAux.search("fsentry"))
        lIndex = "-1"
    }

    var oGrid = document.getElementById(sGridName)
    o = fsGeneralEntry_getById(sInit + vGrid.IdEntryMaterial)
    var sMat = ""
    var sRead = ""
    var sMatId = ""

    //Nivel de seleccion de material 
    if (oEntryMaterial && oEntryMaterial.NivelSeleccion != 0) {
        nivelSeleccion = oEntryMaterial.NivelSeleccion
    }

    if (oEntryMaterial && oEntryMaterial.NivelSeleccion != 0) {
        //si existe un campo mat fuera desglose con nivel de seleccion obligatorio se carga este material
        familiaMateriales = oEntryMaterial.dataValue;
        //si no se ha seleccionado ningun material
        if (!familiaMateriales || familiaMateriales == "") {
            show_MensajeMaterialObligatorio();
            return false;
        }
    }

    if (o) {
        sMat = o.getDataValue();
        sRead = (o.readOnly ? "1" : "0");
        sMatId = o.id;
    } else {
        sMat = oEdit.fsEntry.Dependent.value;
    }

    if (sMat == "")
        if (oEdit.fsEntry.Restrict)
            sMat = oEdit.fsEntry.Restrict;

    var bCargarUltAdj = false;
    if ((oEdit.fsEntry.idEntryPREC) || (oEdit.fsEntry.idEntryPROV)) bCargarUltAdj = true;
    var bCargarCC = false;
    if (oEdit.fsEntry.idEntryCC) bCargarCC = true;
    sCodOrgCompras = "";
    sReadOrgCompras = "";
    var oOrgCompras;
    if (oEdit.fsEntry.idDataEntryDependent) {
        oOrgCompras = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent);
        if (oOrgCompras) {
            sCodOrgCompras = oOrgCompras.getDataValue();
            if (sCodOrgCompras == "") sCodOrgCompras = "-1";
            sReadOrgCompras = (oOrgCompras.readOnly ? "1" : "0");
        } else if ((oEdit.fsEntry.OrgComprasDependent) && (oEdit.fsEntry.OrgComprasDependent.value)) {
            oOrgCompras = fsGeneralEntry_getById(oEdit.fsEntry.OrgComprasDependent.value);
            sCodOrgCompras = oEdit.fsEntry.OrgComprasDependent.value;
            if (sCodOrgCompras == "") sCodOrgCompras = "-1";
            sReadOrgCompras = "1";
        };
    } else {
        if ((oEdit.fsEntry.OrgComprasDependent) && (oEdit.fsEntry.OrgComprasDependent.value)) {
            oOrgCompras = fsGeneralEntry_getById(oEdit.fsEntry.OrgComprasDependent.value);
            sCodOrgCompras = oEdit.fsEntry.OrgComprasDependent.value;
            if (sCodOrgCompras == "") sCodOrgCompras = "-1";
            sReadOrgCompras = "1";
        } else {
            p = window.opener;
            if (p) {
                if (p.sDataEntryOrgComprasFORM) { // Si antes que un desglose de pop-up hay un dataEntry ORGANIZACION COMPRAS
                    oOrgCompras = p.fsGeneralEntry_getById(p.sDataEntryOrgComprasFORM);
                    if (oOrgCompras) {
                        sCodOrgCompras = oOrgCompras.getDataValue();
                        if (sCodOrgCompras == "") sCodOrgCompras = "-1";
                        sReadOrgCompras = (oOrgCompras.readOnly ? "1" : "0");
                    };
                } else {
                    if (p.sCodOrgComprasFORM) {
                        sCodOrgCompras = p.sCodOrgComprasFORM;
                        sReadOrgCompras = "1";
                    };
                };
            };
        };
    };
    sCodCentro = "";
    sReadCentro = "";
    var oCentro;
    if (oEdit.fsEntry.idDataEntryDependent2) {
        oCentro = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent2);
        if (oCentro) {
            sCodCentro = oCentro.getDataValue();
            if (sCodCentro == "") sCodCentro = "-1";
            sReadCentro = (oCentro.readOnly ? "1" : "0");
        } else {
            if ((oEdit.fsEntry.CentroDependent) && (oEdit.fsEntry.CentroDependent.value)) {
                oCentro = fsGeneralEntry_getById(oEdit.fsEntry.CentroDependent.value);
                sCodCentro = oEdit.fsEntry.CentroDependent.value;
                if (sCodCentro == "") sCodCentro = "-1";
                sReadCentro = "1";
            };
        };
    } else {
        if ((oEdit.fsEntry.CentroDependent) && (oEdit.fsEntry.CentroDependent.value)) {
            oCentro = fsGeneralEntry_getById(oEdit.fsEntry.CentroDependent.value)
            sCodCentro = oEdit.fsEntry.CentroDependent.value
            if (sCodCentro == "") sCodCentro = "-1";
            sReadCentro = "1";
        } else {
            p = window.opener;
            if (p) {
                if (p.sDataEntryCentroFORM) { // Si antes que un desglose de pop-up hay un dataEntry CENTRO
                    oCentro = p.fsGeneralEntry_getById(p.sDataEntryCentroFORM);
                    if (oCentro) {
                        sCodCentro = oCentro.getDataValue();
                        if (sCodCentro == "") sCodCentro = "-1";
                        sReadCentro = (oCentro.readOnly ? "1" : "0");
                    };
                } else {
                    if (p.sCodCentroFORM) {
                        sCodCentro = p.sCodCentroFORM;
                        sReadCentro = "1";
                    }
                };
            }
        }
    };

    sCodUnidadOrganizativa = "";
    sReadUnidadOrganizativa = "";
    var oUnidadOrganizativa;
    if (oEdit.fsEntry.idDataEntryDependent3) {
        oUnidadOrganizativa = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent3);
        if (oUnidadOrganizativa) {
            sCodUnidadOrganizativa = oUnidadOrganizativa.getDataValue();
            if (sCodUnidadOrganizativa == "") sCodUnidadOrganizativa = "-1";
            sReadUnidadOrganizativa = (oUnidadOrganizativa.readOnly ? "1" : "0");
        } else {
            if ((oEdit.fsEntry.UnidadOrganizativaDependent) && (oEdit.fsEntry.UnidadOrganizativaDependent.value)) {
                oUnidadOrganizativa = fsGeneralEntry_getById(oEdit.fsEntry.UnidadOrganizativaDependent.value);
                sCodUnidadOrganizativa = oEdit.fsEntry.UnidadOrganizativaDependent.value;
                if (sCodUnidadOrganizativa == "") sCodUnidadOrganizativa = "-1";
                sReadUnidadOrganizativa = "1";
            };
        };
    } else {
        if ((oEdit.fsEntry.UnidadOrganizativaDependent) && (oEdit.fsEntry.UnidadOrganizativaDependent.value)) {
            oUnidadOrganizativa = fsGeneralEntry_getById(oEdit.fsEntry.UnidadOrganizativaDependent.value)
            sCodUnidadOrganizativa = oEdit.fsEntry.UnidadOrganizativaDependent.value
            if (sCodUnidadOrganizativa == "") sCodUnidadOrganizativa = "-1";
            sReadUnidadOrganizativa = "1";
        } else {
            p = window.opener;
            if (p) {
                if (p.sDataEntryUnidadOrganizativaFORM) { // Si antes que un desglose de pop-up hay un dataEntry CENTRO
                    oUnidadOrganizativa = p.fsGeneralEntry_getById(p.sDataEntryUnidadOrganizativaFORM);
                    if (oUnidadOrganizativa) {
                        sCodUnidadOrganizativa = oUnidadOrganizativa.getDataValue();
                        if (sCodUnidadOrganizativa == "") sCodUnidadOrganizativa = "-1";
                        sReadUnidadOrganizativa = (oUnidadOrganizativa.readOnly ? "1" : "0");
                    };
                } else {
                    if (p.sCodUnidadOrganizativaFORM) {
                        sCodUnidadOrganizativa = p.sCodUnidadOrganizativaFORM;
                        sReadUnidadOrganizativa = "1";
                    }
                };
            }
        }
    };
    var sCodCentroCoste = "";
    var oCentroCoste;
    if (oEdit.fsEntry.ActivoSM) {
        if (oEdit.fsEntry.idDataEntryDependentCentroCoste) {
            oCentroCoste = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependentCentroCoste);
            if (oCentroCoste) {
                sCodCentroCoste = oCentroCoste.getDataValue();
            }
        } else {
            p = window.opener;
            if (p) {
                if (p.sDataEntryCentroCosteFORM) { // Si antes que un desglose de pop-up hay un dataEntry CENTRO
                    oCentroCoste = p.fsGeneralEntry_getById(p.sDataEntryCentroCosteFORM);
                    if (oCentroCoste) {
                        sCodCentroCoste = oCentroCoste.getDataValue();
                    }
                }
            }
        }
    }

    //Proveedor que suministra el art�culo    
    var sProveSumiArt = '';
    if (oEdit.fsEntry.idEntryProveSumiArt) {
        var o = fsGeneralEntry_getById(oEdit.fsEntry.idEntryProveSumiArt);
        if (o.CargarProveSumiArt) {
            sProveSumiArt = o.getDataValue();
        }
    }

    //buscar el dataentry tipo de pedido en el formulario
    //Si es pedido negociado
    //  Por si te han puesto la org y centro en otro grupo. Buscar el datentry en el formulario
    //  Por si te han dado proveedor. Buscar el datentry en el formulario
    var concepto = "";
    var almacenamiento = "";
    var recepcion = "";
    var yaTipoPedido = 0;
    var yaOrgCompras = 0;
    var yaCentro = 0;
    var yaUnidadOrganizativa = 0;
    var yaCentroCoste = 0;
    if (oOrgCompras) {
        yaOrgCompras = 1;
    };
    if (oCentro) {
        //Asociado al artic de la linea desglose esta el centro: No buscar. La org compra puede no estar asociada pq esta fuera del desglose-> buscar org pero no centro.
        //Asociado al artic q es campo (no desglose) esta el centro: No buscar. 
        yaCentro = 1;
    };
    if (oUnidadOrganizativa) {
        yaUnidadOrganizativa = 1;
    };
    if (oCentroCoste || !oEdit.fsEntry.ActivoSM) yaCentroCoste = 1;

    var sCodProve = "";
    var sIDCodProve = "";
    var yaProve = 0;
    if (((oEdit.fsEntry.TipoSolicit != 9) && (oEdit.fsEntry.TipoSolicit != 8)) || sProveSumiArt != "") {
        yaProve = 1;
    }

    var sIdFPago = "";
    var sIdFPagoHidden = "";
    var param = "";
    var yaForma = 0;
    var yaUons = 0;
    if (oEdit.fsEntry.TipoSolicit != 9) {
        yaForma = 1
        yaUons = 1
    }

    //Numero de articulos ya rellenos
    var Rellenos = 0;
    var NoRepetirPop = 0;
    if (oEdit.fsEntry.TipoSolicit == 9 || oEdit.fsEntry.TipoSolicit == 8) {
        for (arrInput in arrInputs) {
            oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
            if (oEntry) {
                if (oEntry.tipoGS == 119) { //Art nuevo
                    if (oEntry.getDataValue() != "") {
                        Rellenos = 1;
                        break;
                    }
                }
                if (oEntry.tipoGS == 104) { //Art viejo
                    if (oEntry.getDataValue() != "") {
                        Rellenos = 1;
                        break;
                    }
                }

                if ((oEntry.idCampoPadre) && (NoRepetirPop == 0)) {
                    NoRepetirPop = oEntry.idCampoPadre;
                }

                if (oEntry.tipo == 9 && oEntry.tipoGS != 136) { //desglose
                    for (k = 1; k <= document.getElementById(oEntry.id + "__numTotRows").value; k++) {
                        if (document.getElementById(oEntry.id + "__" + k.toString() + "__Deleted")) {
                            for (arrCampoHijo in oEntry.arrCamposHijos) {
                                var oHijoSrc = document.getElementById(oEntry.id + "__" + oEntry.arrCamposHijos[arrCampoHijo]);
                                if (oHijoSrc) {
                                    var oHijo = document.getElementById(oEntry.id + "__" + k.toString() + "__" + oEntry.arrCamposHijos[arrCampoHijo]);
                                    if (oHijo) {
                                        if (oHijoSrc.getAttribute("tipoGS") == 119) { //Art nuevo
                                            if (oHijo.value != "") {
                                                Rellenos = 1;
                                                break;
                                            }
                                        }
                                        if (oHijoSrc.getAttribute("tipoGS") == 104) { //Art viejo
                                            if (oHijo.value != "") {
                                                Rellenos = 1;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if (Rellenos == 0) { //Si estas en un popup el anterior bucle solo mira ese desglose, pero puede haber mas
        p = window.opener;
        if (p) {
            for (arrInput in p.arrInputs) {
                oEntry = p.fsGeneralEntry_getById(p.arrInputs[arrInput]);
                if (oEntry) {
                    if (oEntry.tipoGS == 119) { //Art nuevo
                        if (oEntry.getDataValue() != "") {
                            Rellenos = 1;
                            break;
                        }
                    }
                    if (oEntry.tipoGS == 104) { //Art viejo
                        if (oEntry.getDataValue() != "") {
                            Rellenos = 1;
                            break;
                        }
                    }
                    //Mas popup
                    if (oEntry.tipo == 9 && oEntry.tipoGS != 136) { //desglose
                        if (NoRepetirPop != oEntry.idCampo) { // solo otros popup
                            for (k = 1; k <= p.document.getElementById(oEntry.id + "__numTotRows").value; k++) {
                                if (p.document.getElementById(oEntry.id + "__" + k.toString() + "__Deleted")) {
                                    for (arrCampoHijo in oEntry.arrCamposHijos) {
                                        var oHijoSrc = p.document.getElementById(oEntry.id + "__" + oEntry.arrCamposHijos[arrCampoHijo]);
                                        if (oHijoSrc) {
                                            var oHijo = p.document.getElementById(oEntry.id + "__" + k.toString() + "__" + oEntry.arrCamposHijos[arrCampoHijo]);
                                            if (oHijo) {
                                                if (oHijoSrc.getAttribute("tipoGS") == 119) { //Art nuevo
                                                    if (oHijo.value != "") {
                                                        Rellenos = 1;
                                                        break;
                                                    }
                                                }
                                                if (oHijoSrc.getAttribute("tipoGS") == 104) { //Art viejo
                                                    if (oHijo.value != "") {
                                                        Rellenos = 1;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    for (arrInput in arrInputs) {
        var o = fsGeneralEntry_getById(arrInputs[arrInput])
        if (o) {
            if ((o.tipoGS == 132) && (yaTipoPedido == 0)) {
                if (o.getDataValue() != '') {
                    concepto = o.Concepto;
                    almacenamiento = o.Almacenamiento;
                    recepcion = o.Recepcion;
                    yaTipoPedido = 1;
                }
            }
            //Por si te han puesto la org y centro en otro grupo
            if (!oOrgCompras) {
                //Solo si no lo has obtenido a traves de oEdit
                if ((o.tipoGS == 123) && (yaOrgCompras == 0)) {
                    sCodOrgCompras = o.getDataValue()
                    if (sCodOrgCompras == "") sCodOrgCompras = "-1"
                    sReadOrgCompras = (o.readOnly ? "1" : "0")
                    yaOrgCompras = 1;
                }
                if ((o.tipoGS == 124) && (yaCentro == 0)) {
                    sCodCentro = o.getDataValue()
                    if (sCodCentro == "") sCodCentro = "-1"
                    sReadCentro = (o.readOnly ? "1" : "0")
                    yaCentro = 1;
                }
            };
            if (!oUnidadOrganizativa) {
                //Por si te han puesto la uon en otro grupo
                if ((o.tipoGS == 121) && (yaUnidadOrganizativa == 0)) {
                    sCodUnidadOrganizativa = o.getDataValue()
                    if (sCodUnidadOrganizativa == "") sCodUnidadOrganizativa = "-1"
                    sReadUnidadOrganizativa = (o.readOnly ? "1" : "0")
                    yaUnidadOrganizativa = 1;
                };
            };
            if (o.ActivoSM && !oCentroCoste) {
                //Por si te han puesto el centro de coste en otro grupo
                if ((o.tipoGS == 129) && (yaCentroCoste == 0)) {
                    sCodCentroCoste = o.getDataValue();
                    oCentroCoste = o;
                    yaCentroCoste = 1;
                };
            };
            //Determinar proveedor, si es pedido negociado/express
            if ((o.tipoGS == 100) && (yaProve == 0)) {
                sCodProve = o.getDataValue();
                yaProve = 1;

                if (oEdit.fsEntry.TipoSolicit == 9 || oEdit.fsEntry.TipoSolicit == 8) {
                    sIDCodProve = o.id;
                    if (o.idDataEntryDependent3) { //Forma Pago existe pero invisible
                        sIdFPagoHidden = o.id;
                    }
                }
            }
            //Determinar Forma pago, si es pedido negociado
            if ((o.tipoGS == 101) && (yaForma == 0)) {
                sIdFPago = o.id;
                yaForma = 1;
            }
            //Determinar CC, si es pedido negociado. Acceso SM y partida
            if (o.tipoGS == 130) {
                yaUons = 1;

                sPartida = o.getDataValue();
                arrAux = sPartida.split("#");

                if (arrAux[2] == null) {
                    //00#212
                    sUon1 = arrAux[0]

                    param = '&Uon1=' + sUon1
                } else {
                    if (arrAux[3] == null) {
                        //00#01#212|200
                        sUon1 = arrAux[0]
                        sUon2 = arrAux[1]

                        param = '&Uon1=' + sUon1 + '&Uon2=' + sUon2
                    } else {
                        if (arrAux[4] == null) {
                            //00#01#01#212|200
                            sUon1 = arrAux[0]
                            sUon2 = arrAux[1]
                            sUon3 = arrAux[2]

                            param = '&Uon1=' + sUon1 + '&Uon2=' + sUon2 + '&Uon3=' + sUon3
                        } else {
                            //00#01#01#01#212|200
                            sUon1 = arrAux[0]
                            sUon2 = arrAux[1]
                            sUon3 = arrAux[2]
                            sUon4 = arrAux[3]

                            param = '&Uon1=' + sUon1 + '&Uon2=' + sUon2 + '&Uon3=' + sUon3 + '&Uon4=' + sUon4
                        }
                    }
                }
            }
            //Si todo ya cargado , acabaste
            if ((yaTipoPedido == 1) && (yaOrgCompras == 1) && (yaCentro == 1) && (yaProve == 1) && (yaForma == 1) && (yaUons == 1) && (yaUnidadOrganizativa == 1) && (yaCentroCoste == 1)) {
                break;
            };
        };
    };

    if ((yaProve == 0) || (yaOrgCompras == 0) || (yaCentro == 0)) {
        //Si es pedido negociado/expres
        //Si es popup el arrInputs no contiene prove ni formapago ni org compras ni centro ni unidadorganizativa
        if ((oEdit.fsEntry.TipoSolicit == 9) || (oEdit.fsEntry.TipoSolicit == 8)) {
            p = window.opener;

            if (oEdit.fsEntry.TipoSolicit == 8) { //Express no carga ni prove ni formapago tras selecc art
                yaForma = 1;
                //Solo pedidos Negociados admite lo de meter la org compras y centro en otro grupo.... En el resto esta como antes, lo q NO este en tu mismo grupo NO existe
                yaOrgCompras = 1;
                yaCentro = 1;
            }

            if (p) {
                for (arrInput in p.arrInputs) {
                    var o = p.fsGeneralEntry_getById(p.arrInputs[arrInput])
                    if (o) {
                        //Determinar proveedor, si es pedido negociado/express
                        if ((o.tipoGS == 100) && (yaProve == 0)) {
                            sCodProve = o.getDataValue();
                            yaProve = 1;

                            if (oEdit.fsEntry.TipoSolicit == 9 || oEdit.fsEntry.TipoSolicit == 8) {
                                sIDCodProve = o.id;
                                if (o.idDataEntryDependent3) { //Forma Pago existe pero invisible
                                    sIdFPagoHidden = o.id;
                                };
                            };
                        };
                        //Determinar Forma pago, si es pedido negociado
                        if ((o.tipoGS == 101) && (yaForma == 0)) {
                            sIdFPago = o.id;
                            yaForma = 1;
                        };
                        //Por si te han puesto la org y centro en otro grupo
                        if ((o.tipoGS == 123) && (yaOrgCompras == 0)) {
                            sCodOrgCompras = o.getDataValue()
                            if (sCodOrgCompras == "") sCodOrgCompras = "-1"
                            sReadOrgCompras = (o.readOnly ? "1" : "0")
                            yaOrgCompras = 1;
                        };
                        //Por si te han puesto la org y centro en otro grupo
                        if ((o.tipoGS == 124) && (yaCentro == 0)) {
                            sCodCentro = o.getDataValue()
                            if (sCodCentro == "") sCodCentro = "-1"
                            sReadCentro = (o.readOnly ? "1" : "0")
                            yaCentro = 1;
                        };
                        //Si todo ya cargado , acabaste
                        if ((yaProve == 1) && (yaForma == 1) && (yaOrgCompras == 1) && (yaCentro == 1)) {
                            break;
                        }
                    }
                }
            }
        }
    };
    p = window.opener;
    if (oEdit.fsEntry.TipoSolicit == 5) {
        if (document.getElementById("hid_Proveedor")) {
            sCodProve = document.getElementById("hid_Proveedor").value
        } else {
            if (window.parent.location.toString().search("desglose.aspx") != -1) {
                sCodProve = p.document.getElementById("hid_Proveedor").value
            }
        }
    }
    if ((oEdit.fsEntry.TipoSolicit == 2) || (oEdit.fsEntry.TipoSolicit == 3)) {
        if (window.parent.location.toString().search("altacertificado.aspx") != -1) {
            var Params = window.parent.location.search.toString();

            var Inicio = Params.search("Proveedores=");
            var Fin = Params.search("&volver");
            var NCar = "Proveedores=".length;

            Params = Params.substr(Inicio + NCar, Fin - Inicio - NCar);

            arrAux = Params.split("'");

            if (arrAux[3] == null) {
                sCodProve = arrAux[1];
            }
        } else {
            if (document.getElementById("Proveedor")) {
                sCodProve = document.getElementById("Proveedor").value
            } else {
                if (window.parent.location.toString().search("desglose.aspx") != -1) {
                    sCodProve = p.document.getElementById("Proveedor").value
                }
            }
        }
    }

    //altura ventana
    if ((sCodOrgCompras == "") || (sCodCentro == "")) altura = 640;
    else altura = 690;

    //Si no hay unidad organizativa y s� centro de coste se asigna a la unidad organizativa las UONs correspondientes al centro de coste para abrir el buscador   
    if ((sCodUnidadOrganizativa == '' || sCodUnidadOrganizativa == '-1') && sCodCentroCoste != '') {
        var a = new Array();
        a = sCodCentroCoste.split("#");
        if (a.length == 1)
            sCodUnidadOrganizativa = a[0] + ' - ' + oCentroCoste.getValue().split(' - ')[1];     //agregar denominaci�n al final
        else if (a.length == 2)
            sCodUnidadOrganizativa = a[0] + ' - ' + a[1] + ' - ' + oCentroCoste.getValue().split(' - ')[1];     //agregar denominaci�n al final    
        else if (a.length == 3)
            sCodUnidadOrganizativa = a[0] + ' - ' + a[1] + ' - ' + a[2] + ' - ' + oCentroCoste.getValue().split(' - ')[1];     //agregar denominaci�n al final    
        else
            sCodUnidadOrganizativa = a[0] + ' - ' + a[1] + ' - ' + a[2] + ' - (' + a[3] + ') ' + oCentroCoste.getValue().split(' - ')[1];     //agregar denominaci�n al final   
        sReadUnidadOrganizativa = "1";
    }

    if (sCodUnidadOrganizativa !== '') {
        sCodUnidadOrganizativa = encodeURIComponent(sCodUnidadOrganizativa)
    }

    //Altura variable: 
    //  Otras caracteristicas-> 1 o 3 lineas
    //  Tabla Atributos-> 0..N
    var params = { sCodUnidadOrganizativa: sCodUnidadOrganizativa, sCodOrgCompras: sCodOrgCompras, sCodCentro: sCodCentro };
    $.when($.ajax({
        type: "POST",
        url: rutaFS + '_Common/BuscadorArticulos.aspx/DeterminarAturaAtrbsUon',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        var respuesta = msg.d;
        if ((respuesta[0] == "0") && ((sCodOrgCompras == "") || (sCodCentro == "") || (sCodOrgCompras == "-1") || (sCodCentro == "-1"))) {
            altura = altura + 20; //titulo
        } else {
            if (respuesta[0] == "1") {
                altura = altura + 30;
                if (parseInt(respuesta[1]) < 9) {
                    altura = altura + (25 * parseInt(respuesta[1]));
                } else {
                    altura = altura + (25 * 8);
                }
            } else {
                //No sale Ni titulo Ni Atrib
                altura = 620
            }
        }
        if (respuesta[2] == 1) {
            altura = altura + 50;
        }
    });

    //Tipo de Solicitud (1- solic compra, 2- Certif, ...,8-pedido expres,9- PedidoNegociado
    var iTipoSolicit = 1;
    var AniadirDesde = '';
    AniadirDesde = '&CodProve=' + sCodProve;

    if (oEdit.fsEntry.TipoSolicit == 9 || oEdit.fsEntry.TipoSolicit == 8) {
        AniadirDesde = AniadirDesde + '&sIDCodProve=' + sIDCodProve + "&Rellenos=" + Rellenos + "&IDFPago=" + sIdFPago + "&IDFPagoHidden=" + sIdFPagoHidden + param
    };

    CerrarDesplegables();
    var sMoneda = obtenerMonedaFormulario(oEdit);
    if (sMoneda != "") {
        var newWindow = window.open(rutaFS + "_common/BuscadorArticulos.aspx?mat=" + sMat + "&matro=" + sRead + "&Index=" + lIndex + "&TabContainer=" + sInit + "&ClientID=" + sClientID + "&CloneClientID=" + oEdit.id +
                                    "den&restric=" + (oEdit.fsEntry.Restric ? oEdit.fsEntry.Restric : "") + "&MatClientId=" + sMatId + "&InstanciaMoneda=" + sMoneda + "&CargarUltAdj=" + bCargarUltAdj +
                                    (sCodOrgCompras == '' ? '' : "&CodOrgCompras=" + (sCodOrgCompras == null ? '' : sCodOrgCompras) + "&OrgComprasRO=" + sReadOrgCompras) + (sCodCentro == '' ? '' : "&CodCentro=" + (sCodCentro == null ? '' : sCodCentro) +
                                    "&CentroRO=" + sReadCentro) + "&CargarCC=" + bCargarCC + "&concepto=" + concepto + "&almacenamiento=" + almacenamiento + "&recepcion=" + recepcion + AniadirDesde +
                                    (sCodUnidadOrganizativa == '' ? '' : "&CodUnidadOrganizativa=" + (sCodUnidadOrganizativa == null ? '' : sCodUnidadOrganizativa) + "&UnidadOrganizativaRO=" + sReadUnidadOrganizativa) +
                                    "&NivelSeleccion=" + nivelSeleccion + "&FamiliaMateriales=" + familiaMateriales + "&TipoSolicitud=" + oEdit.fsEntry.TipoSolicit + "&ProveSumiArt=" + sProveSumiArt, "_blank", "width=850,height=" + altura + ",status=yes,resizable=yes,scrollbars=yes,top=50,left=200");
        newWindow.focus();
    }
};
function ddNuevoCodArticulo_ValueChange(oEdit, oldValue, oEvent) {
    if (agregandoDesdeExcel) return;
    oEdit = oEdit[0];
    //Evitar el postback al pulsar enter
    $('#' + oEdit.id + '_t').on('keypress', function (event, args) {
        if (event.keyCode == 13) {
            $('#' + oEdit.id + '_t').parent().next().find('img').click();
            return false;
        }
    });
    oEdit.fsEntry.codigoArticulo = oEdit.fsEntry.getValue();
    var oMat = null;
    var oOrgCompras = null;
    var p = null;
    var requestExtra = "";
    VaciarCtrlsDependientes(oEdit);
    if (oEdit.fsEntry.getValue() == '') {
        oEdit.fsEntry.Concepto = "";
        oEdit.fsEntry.Almacenamiento = "";
        oEdit.fsEntry.Recepcion = "";
        //Para solicitudes negociadas, al vaciar el art�culo, adem�s de lo anterior, borramos los datos de precio y unidades
        if (oEdit.fsEntry.tipoGS == 104 || oEdit.fsEntry.tipoGS == 119) {
            if (oEdit.fsEntry.TipoSolicit == 9 || oEdit.fsEntry.TipoSolicit == 8) {
                //Unidad
                if (oEdit.fsEntry.idEntryUNI == null) idEntryUNI = ""
                else idEntryUNI = oEdit.fsEntry.idEntryUNI
                if (idEntryUNI != "") {
                    var oUni = fsGeneralEntry_getById(idEntryUNI);
                    if (oUni) {
                        oUni.setValue('');
                        oUni.setDataValue('');
                    }
                    oUni = null;
                    idEntryUNI = "";
                }
                //PRECIO si CARGAR_ULT_ADJ = 1
                if (oEdit.fsEntry.idEntryPREC != null) {
                    idEntryPrec = oEdit.fsEntry.idEntryPREC;
                    oPrecio = fsGeneralEntry_getById(idEntryPrec);
                    if ((oPrecio) && (oPrecio.cargarUltADJ == true)) {
                        oPrecio.setValue('');
                        oPrecio.setDataValue('');
                    }
                    oPrecio = null;
                    idEntryPrec = "";
                }
                //Aqui vamos a rellenar el IdEntryUniPedido del codigo de articulo pq si esta invisible se pierde
                if (oEdit.fsEntry.IdEntryUniPedido) {
                    if (oEdit.fsEntry.IdEntryUniPedido != '') {
                        oUnidadPedido = fsGeneralEntry_getById(oEdit.fsEntry.IdEntryUniPedido);
                        if (oUnidadPedido) {
                            oUnidadPedido.setDataValue("");
                            oUnidadPedido.setValue("");
                        }
                    }
                }
            }
        }
        return;
    }

    if (document.getElementById("bMensajePorMostrar")) document.getElementById("bMensajePorMostrar").value = "1"
    var idEditorInput = oEdit.id + '_t';
    var oEditorInput = document.getElementById(idEditorInput);
    if (oEditorInput) {
        sValor = trim(oEditorInput.value);
        oEditorInput.title = sValor;
    }

    idEntryDen = calcularIdDataEntryCelda(oEdit.id, oEdit.fsEntry.nColumna + 1, oEdit.fsEntry.nLinea, 0)
    IDoMat = calcularIdDataEntryCelda(oEdit.id, oEdit.fsEntry.nColumna - 1, oEdit.fsEntry.nLinea, 0)

    var oEntryMaterial;
    oEntryMaterial = getMaterialFormulario();

    if (IDoMat) {
        oMat = fsGeneralEntry_getById(IDoMat);
        if ((oMat) && (oMat.tipoGS == 103)) sMat = oMat.getDataValue();
    }
    if (oEntryMaterial && oEntryMaterial.NivelSeleccion != 0 && (sMat == "")) {
        //Si no hemos seleccionado un material a nivel de l�nea ...
        //si existe un campo mat fuera desglose con nivel de seleccion obligatorio se carga este material
        sMat = oEntryMaterial.dataValue;
        oMat = oEntryMaterial;
        //si no se ha seleccionado ningun material
        if (sMat || sMat == "") {
            nivelSeleccion = oEntryMaterial.NivelSeleccion;
        } else {
            show_MensajeMaterialObligatorio();
            return false;
        }
    } else {
        if (IDoMat) oMat = fsGeneralEntry_getById(IDoMat);
        else IDoMat = "";
        if ((oMat) && (oMat.tipoGS == 103)) sMat = oMat.getDataValue();
        else {
            sMat = oEdit.fsEntry.Dependent.value;
            IDoMat = ""
        }
        if (sMat == null) {
            sMat = ""
        }
    }

    if (sMat == "")
        if (oEdit.fsEntry.Restrict)
            sMat = oEdit.fsEntry.Restrict;

    if (document.getElementById("ddFavoritos") != undefined) {
        //DEMO Gestamp / 2017 / 66
        //Tienes opci�n a seleccionar articulo desde un panel "articulos favoritos" q no tienen pq ser todos del mismo material (lo indica un check del popup). Se supone q es correcto, es decir q NO hay pq darlo de alta nunca. 
        //El _common/BuscadorArticuloserver.aspx se encargara de los permisos.
        sMat = ""
    }

    var idEntryPrec = null;
    var idEntryProv = null;

    //Obtener  el id del DataEntry del PRECIO si CARGAR_ULT_ADJ = 1
    if (oEdit.fsEntry.idEntryPREC != null) {
        idEntryPrec = oEdit.fsEntry.idEntryPREC;
        oPrecio = fsGeneralEntry_getById(idEntryPrec);
        if ((oPrecio) && (oPrecio.cargarUltADJ == true)) idEntryPrec = oEdit.fsEntry.idEntryPREC;
        else idEntryPrec = "";
    }
    //Obtener  el id del DataEntry del PROVEEDOR si CARGAR_ULT_ADJ = 1
    if (oEdit.fsEntry.idEntryPROV != null) {
        idEntryProv = oEdit.fsEntry.idEntryPROV;
        oProveedor = fsGeneralEntry_getById(idEntryProv);
        if ((oProveedor) && (oProveedor.cargarUltADJ == true)) idEntryProv = oEdit.fsEntry.idEntryPROV;
        else idEntryProv = "";
    }
    if (idEntryPrec == null) idEntryPrec = "";
    if (idEntryProv == null) idEntryProv = "";
    if (oEdit.fsEntry.idEntryCANT == null) idEntryCANT = "";
    else idEntryCANT = oEdit.fsEntry.idEntryCANT;
    if (oEdit.fsEntry.idEntryUNI == null) idEntryUNI = "";
    else idEntryUNI = oEdit.fsEntry.idEntryUNI;
    var sIDCodUniPedido = "";
    if (oEdit.fsEntry.IdEntryUniPedido == null) sIDCodUniPedido = "";
    else {
        sIDCodUniPedido = oEdit.fsEntry.IdEntryUniPedido;
        requestExtra += "&sIDCodUniPedido=" + sIDCodUniPedido;
    }

    //Nos aseguramos de que tenemos la Org de compras y el centro.
    var sCodOrgCompras = "";
    var sCodCentro = "";
    var sCodCentroCoste = "";
    var yaOrgCompras = 0;
    var yaCentro = 0;
    var yaCentroCoste = 0;

    var idEntryOrg = null;
    if (oEdit.fsEntry.idDataEntryDependent) {
        idEntryOrg = oEdit.fsEntry.idDataEntryDependent
        oOrgCompras = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent)
        if (oOrgCompras) {
            sCodOrgCompras = oOrgCompras.getDataValue()
            requestExtra += "&CodOrgCompras=" + sCodOrgCompras;
        }
    } else if ((oEdit.fsEntry.OrgComprasDependent) && (oEdit.fsEntry.OrgComprasDependent.value)) {
        sCodOrgCompras = oEdit.fsEntry.OrgComprasDependent.value;
    } else {
        p = window.opener;
        if (p) if (p.sDataEntryOrgComprasFORM) { // Si antes que un desglose de pop-up hay un dataEntry ORGANIZACION COMPRAS
            oOrgCompras = p.fsGeneralEntry_getById(p.sDataEntryOrgComprasFORM)
            if (oOrgCompras) {
                idEntryOrg = oOrgCompras.id;
                sCodOrgCompras = oOrgCompras.getDataValue();
                requestExtra += "&CodOrgCompras=" + sCodOrgCompras;
            }
        }
    }
    if (idEntryOrg == null) idEntryOrg = "";

    var idEntryCentro = null;
    var oCentro;
    if ((oEdit.fsEntry.idDataEntryDependent2) && (yaCentro == 0)) {
        idEntryCentro = oEdit.fsEntry.idDataEntryDependent2
        oCentro = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent2)
        if (oCentro) {
            sCodCentro = oCentro.getDataValue()
            requestExtra += "&CodCentro=" + sCodCentro;
            yaCentro = 1;
        }
    } else if ((oEdit.fsEntry.CentroDependent) && (oEdit.fsEntry.CentroDependent.value) && (yaCentro == 0)) {
        sCodCentro = oEdit.fsEntry.CentroDependent.value
        requestExtra += "&CodCentro=" + sCodCentro;
        yaCentro = 1;
    } else {
        p = window.opener;
        if (p && (yaCentro == 0)) if (p.sDataEntryCentroFORM) { // Si antes que un desglose de pop-up hay un dataEntry CENTROS
            oCentro = p.fsGeneralEntry_getById(p.sDataEntryCentroFORM)
            if (oCentro) {
                idEntryCentro = p.sDataEntryCentroFORM;
                sCodCentro = oCentro.getDataValue()
                requestExtra += "&CodCentro=" + sCodCentro;
                yaCentro = 1;
            }
        }
    }
    if (idEntryCentro == null) idEntryCentro = ""

    var idEntryCentroCoste = '';
    var oCentroCoste;
    if (oEdit.fsEntry.ActivoSM) {
        if (oEdit.fsEntry.idDataEntryDependentCentroCoste) {
            idEntryCentroCoste = oEdit.fsEntry.idDataEntryDependentCentroCoste;
            var oCentroCoste = fsGeneralEntry_getById(idEntryCentroCoste)
            if (oCentroCoste) {
                sCodCentroCoste = oCentroCoste.getDataValue();
                yaCentroCoste = 1;
            }
        } else {
            p = window.opener;
            if (p) {
                if (yaCentroCoste = 0 && p.sDataEntryCentroCosteFORM) { // Si antes que un desglose de pop-up hay un dataEntry CENTRO
                    oCentroCoste = p.fsGeneralEntry_getById(p.sDataEntryCentroCosteFORM);
                    if (oCentroCoste) {
                        idEntryCentroCoste = p.sDataEntryCentroCosteFORM;
                        sCodCentroCoste = oCentroCoste.getDataValue();
                        yaCentroCoste = 1;
                    }
                }
            }
        }
    }

    //Proveedor que suministra el art�culo       
    var sProveSumiArt = '';
    if (oEdit.fsEntry.idEntryProveSumiArt) {
        var o = fsGeneralEntry_getById(oEdit.fsEntry.idEntryProveSumiArt);
        if (o.CargarProveSumiArt) {
            sProveSumiArt = o.getDataValue();
        }
    }

    //Obtener  el id del DataEntry de la cuenta contable
    var idEntryCC = null;
    if (oEdit.fsEntry.idEntryCC != null) {
        idEntryCC = oEdit.fsEntry.idEntryCC;
        oCC = fsGeneralEntry_getById(idEntryCC);
        if (oCC) idEntryCC = oEdit.fsEntry.idEntryCC;
        else idEntryCC = "";
    }
    if (idEntryCC == null) idEntryCC = ""

    var concepto = "";
    var almacenamiento = "";
    var recepcion = "";

    //Variable para el Tipo de Pedido
    var yaTipoPedido = 0;
    //Variables para decidir si es necesario pasar el proveedor y la unidad de pedido a BuscardorArticulosServer
    var sCodProve = "";
    var sIDCodProve = "";
    var yaProve = 0;

    if ((oEdit.fsEntry.TipoSolicit != 9) && (oEdit.fsEntry.TipoSolicit != 8)) yaProve = 1;
    if (oOrgCompras) {
        yaOrgCompras = 1;
        if (oCentro) yaCentro = 1;
    }
    if (oCentroCoste || !oEdit.fsEntry.ActivoSM) yaCentroCoste = 1;

    var sIdFPago = "";
    var sIdFPagoHidden = "";
    var yaForma = 0;
    var yaUons = 0;
    if (oEdit.fsEntry.TipoSolicit != 9) {
        yaForma = 1
        yaUons = 1
    }

    requestExtra += "&iTipoSolicit=" + oEdit.fsEntry.TipoSolicit;

    for (arrInput in arrInputs) {
        var o = fsGeneralEntry_getById(arrInputs[arrInput])
        if (o) {
            if ((o.tipoGS == 132) && (yaTipoPedido == 0)) {
                if (o.getDataValue() != '') {
                    concepto = o.Concepto;
                    almacenamiento = o.Almacenamiento;
                    recepcion = o.Recepcion;
                    yaTipoPedido = 1;
                }
            }
            //Por si te han puesto la org y centro en otro grupo
            if (!oOrgCompras) {
                //Solo si no lo has obtenido a traves de oEdit
                if ((o.tipoGS == 123) && (yaOrgCompras == 0)) {
                    sCodOrgCompras = o.getDataValue()
                    idEntryOrg = o.id;
                    if (sCodOrgCompras == "") sCodOrgCompras = "-1"
                    sReadOrgCompras = (o.readOnly ? "1" : "0")
                    yaOrgCompras = 1;
                    requestExtra += "&CodOrgCompras=" + sCodOrgCompras;
                }
                if ((o.tipoGS == 124) && (yaCentro == 0)) {
                    sCodCentro = o.getDataValue()
                    if (sCodCentro == "") sCodCentro = "-1"
                    sReadCentro = (o.readOnly ? "1" : "0")
                    yaCentro = 1;
                    requestExtra += "&CodCentro=" + sCodCentro;
                }
            }
            if (o.ActivoSM && !oCentroCoste && o.tipoGS == 129) {
                sCodCentroCoste = o.getDataValue();
                idEntryCentroCoste = o.id;
                yaCentroCoste = 1;
            }
            //Determinar proveedor. es necesario cuando el pedido es negociado/expres
            //idEntryProv guardar el id del proveedor cuando est� en el desglose pero no si est� en la cabecera y en el caso de pedidos negociados/expres, estar� en la cabecera
            if ((o.tipoGS == 100) && (yaProve == 0)) {
                sCodProve = o.getDataValue();
                yaProve = 1;
                requestExtra += "&sCodProve=" + sCodProve;
                if ((oEdit.fsEntry.TipoSolicit == 8) || (oEdit.fsEntry.TipoSolicit == 9)) {
                    sIDCodProve = o.id;
                    requestExtra += "&sIDCodProve=" + sIDCodProve;
                    if (o.idDataEntryDependent3) { //Forma Pago existe pero invisible
                        sIdFPagoHidden = o.id;
                        requestExtra += "&IDFPagoHidden=" + sIdFPagoHidden;
                    }
                }
            }
            //Determinar Forma pago, si es pedido negociado
            if ((o.tipoGS == 101) && (yaForma == 0)) {
                sIdFPago = o.id;
                yaForma = 1;
                requestExtra += "&IDFPago=" + sIdFPago;
            }
            //Determinar Forma pago, si es pedido negociado. Acceso SM y partida
            if (o.tipoGS == 130) {               
                yaUons = 1;
                sPartida = o.getDataValue();
                arrAux = sPartida.split("#");
                if (arrAux[2] == null) {
                    //00#212
                    sUon1 = arrAux[0];

                    requestExtra += '&Uon1=' + sUon1;
                } else {
                    if (arrAux[3] == null) {
                        //00#01#212|200
                        sUon1 = arrAux[0];
                        sUon2 = arrAux[1];

                        requestExtra += '&Uon1=' + sUon1 + '&Uon2=' + sUon2;
                    } else {
                        if (arrAux[4] == null) {
                            //00#01#01#212|200
                            sUon1 = arrAux[0];
                            sUon2 = arrAux[1];
                            sUon3 = arrAux[2];

                            requestExtra += '&Uon1=' + sUon1 + '&Uon2=' + sUon2 + '&Uon3=' + sUon3;
                        } else {
                            //00#01#01#01#212|200
                            sUon1 = arrAux[0];
                            sUon2 = arrAux[1];
                            sUon3 = arrAux[2];
                            sUon4 = arrAux[3];

                            requestExtra += '&Uon1=' + sUon1 + '&Uon2=' + sUon2 + '&Uon3=' + sUon3 + '&Uon4=' + sUon4;
                        }
                    }
                }
            }
            //Si tienes todos los valores que necesitas, ya acabaste
            if ((yaTipoPedido == 1) && (yaOrgCompras == 1) && (yaCentro == 1) && (yaProve == 1) && (yaForma == 1) && (yaUons == 1) && (yaCentroCoste == 1)) { // && (yaUniPedido == 1)) {
                break;
            }
        }
    }
    //Puede darse el caso de que desglose sea popup y que al ser una solicitud de tipo Pedido Negociado, el proveedor est� fuera del desglose
    //por lo que hay que buscarlo en el parent
    if ((yaProve == 0) || (yaOrgCompras == 0) || (yaCentro == 0)) {
        if (oEdit.fsEntry.TipoSolicit == 9 || oEdit.fsEntry.TipoSolicit == 8) {
            p = window.opener;
            if (p) {
                var arrInputsOpener = null;
                arrInputsOpener = p.arrInputs;
                if (arrInputsOpener) {
                    for (arrInput in arrInputsOpener) {
                        o = p.fsGeneralEntry_getById(arrInputsOpener[arrInput]);
                        if (o) {
                            //Determinar proveedor. es necesario cuando el pedido es negociado/expres
                            //idEntryProv guardar el id del proveedor cuando est� en el desglose pero no si est� en la cabecera y en el caso de pedidos negociados/expres, estar� en la cabecera
                            if ((o.tipoGS == 100)) {
                                sCodProve = o.getDataValue();
                                yaProve = 1;
                                requestExtra += "&sCodProve=" + sCodProve;
                                sIDCodProve = o.id;
                                requestExtra += "&sIDCodProve=" + sIDCodProve;
                                if (o.idDataEntryDependent3) { //Forma Pago existe pero invisible
                                    sIdFPagoHidden = o.id;
                                    requestExtra += "&IDFPagoHidden=" + sIdFPagoHidden;
                                }
                            }
                            //Determinar Forma pago, si es pedido negociado
                            if ((o.tipoGS == 101) && (yaForma == 0)) {
                                sIdFPago = o.id;
                                yaForma = 1;
                                requestExtra += "&IDFPago=" + sIdFPago;
                            }
                            //Por si te han puesto la org y centro en otro grupo
                            if ((o.tipoGS == 123) && (yaOrgCompras == 0)) {
                                idEntryOrg = o.id;
                                sCodOrgCompras = o.getDataValue()
                                if (sCodOrgCompras == "") sCodOrgCompras = "-1"
                                sReadOrgCompras = (o.readOnly ? "1" : "0")
                                yaOrgCompras = 1;
                                requestExtra += "&CodOrgCompras=" + sCodOrgCompras;
                            }
                            //Por si te han puesto la org y centro en otro grupo
                            if ((o.tipoGS == 124) && (yaCentro == 0)) {
                                sCodCentro = o.getDataValue()
                                if (sCodCentro == "") sCodCentro = "-1"
                                sReadCentro = (o.readOnly ? "1" : "0")
                                yaCentro = 1;
                                requestExtra += "&CodCentro=" + sCodCentro;
                            }
                            if (o.ActivoSM && o.tipoGS == 129) {
                                sCodCentroCoste = o.getDataValue();
                                yaCentroCoste = 1;
                            }
                            if ((yaProve == 1) && (yaForma == 1) && (yaOrgCompras == 1) && (yaCentro == 1) && (yaCentroCoste == 1)) {
                                break;
                            }
                        }
                    }
                }
                arrInputsOpener = null;
                o = null;
            }
        }
    }
    //Si se teclea el c�digo del articulo
    //Se comprueba si se viene de Contrato y si se ha elegido Proveedor
    //En ese caso, se realiza la b�squeda usando dicho proveedor que se le pasa como par�metro
    var sCodProve = "";
    if (oEdit.fsEntry.TipoSolicit == 5) {
        if (document.getElementById("hid_Proveedor")) {
            sCodProve = document.getElementById("hid_Proveedor").value
            requestExtra += "&CodProve=" + sCodProve;

        }
    };
    //si hay campo de Un. Organizativa/ Centro con valor se pasa para poder comprobar que el art�culo sea de esa UON
    var miUON = BuscarUonEnSolicitud(oEdit);

    //Si no hay unidad organizativa y s� centro de coste se asigna a la unidad organizativa las UONs correspondientes al centro de coste para abrir el buscador        
    if ((miUON == '' || miUON == '-1') && sCodCentroCoste != '') miUON = sCodCentroCoste.replace('#', ' - ');

    var sMoneda = obtenerMonedaFormulario(oEdit);
    if (sMoneda != "") {
        var newWindow = window.open("../_common/BuscadorArticuloserver.aspx?idMaterial=" + sMat + "&idArt=" + sValor + "&fsEntryArt=" + oEdit.id + "&fsEntryDen=" + idEntryDen + "&fsEntryMat=" + IDoMat + "&fsEntryPrec=" + idEntryPrec +
                                    "&fsEntryProv=" + idEntryProv + "&InstanciaMoneda=" + sMoneda + "&fsEntryCant=" + idEntryCANT + "&fsEntryUni=" + idEntryUNI + "&fsEntryOrg=" + idEntryOrg + "&fsEntryCentro=" + idEntryCentro +
                                    "&fsEntryCC=" + idEntryCC + "&concepto=" + concepto + "&almacenamiento=" + almacenamiento + "&recepcion=" + recepcion + requestExtra + "&miUON=" + miUON + "&ProveSumiArt=" + sProveSumiArt, "iframeWSServer", "resizable=yes")
        newWindow.focus();
    } else {
        oEdit.fsEntry.setValue('');
        oEdit.fsEntry.setDataValue('');
        document.getElementById(idEntryDen + "__tden").fsEntry.setValue('');
        document.getElementById(idEntryDen + "__tden").fsEntry.setDataValue('');
    }
};
function ddCentroCoste_ValueChange(oEdit, oldValue, oEvent) {
    sValor = trim(oEdit.getValue());
    oEdit.elem.title = sValor;

    borrarPartidaoActivoRelacionado(oEdit.fsEntry);
    var newWindow = window.open("../_common/BuscadorCentroCosteServer.aspx?idCC=" + sValor + "&fsEntryCC=" + oEdit.ID + "&VerUON=" + (oEdit.fsEntry.VerUON ? "1" : "0"), "iframeWSServer")
    newWindow.focus();
};
//Comprueba que el c�digo de centro de coste introducido es correcto.
function ddPartida_ValueChange(oEdit, oldValue, oEvent) {
    var sCC = "";
    var idCC = "";
    var sPRES5 = "";
    var sTieneCCRelacionado = "0"

    sValor = trim(oEdit.getValue());
    oEdit.elem.title = sValor;
    sPRES5 = oEdit.fsEntry.PRES5;

    if (oEdit.fsEntry.idEntryCC != null) {
        sTieneCCRelacionado = "1"
        o = fsGeneralEntry_getById(oEdit.fsEntry.idEntryCC)
        if (o) {
            if (o.tipoGS == 129) {
                sCC = o.getDataValue();
                idCC = o.id;
            }
        } else {
            if (oEdit.fsEntry.idCampoPadre) {
                var desglose = oEdit.fsEntry.id.substr(0, oEdit.fsEntry.id.search("_fsdsentry"))
                var index = oEdit.fsEntry.id.substr(oEdit.fsEntry.id.search("_fsdsentry_") + 11, oEdit.fsEntry.id.search(oEdit.fsEntry.idCampo) - oEdit.fsEntry.id.search("_fsdsentry_") - 12)

                oDSEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + oEdit.fsEntry.idEntryCC)
                if (oDSEntry) {
                    if (oDSEntry.tipoGS == 129) {
                        sCC = oDSEntry.getDataValue();
                        idCC = oDSEntry.id;
                    }
                } else {

                    ////////////////
                    for (i = 0; i < document.getElementById(desglose + "_tblDesgloseHidden").rows[0].cells.length; i++) {
                        s = document.getElementById(desglose + "_tblDesgloseHidden").rows[0].cells[i].innerHTML
                        re = /fsentry/

                        x = s.search(re)
                        y = s.search("__tbl")

                        id = s.substr(x + 7, y - x - 7)
                        oEntryCC = fsGeneralEntry_getById(desglose + "_fsentry" + id)

                        if (oEntryCC) {
                            if ((oEntryCC.tipoGS == 129) && (oEntryCC.campo_origen == oEdit.fsEntry.idEntryCC)) {
                                oDSEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + fsGeneralEntry_getById(oEntryCC.id).idCampo)
                                if (oDSEntry) {
                                    sCC = oDSEntry.getDataValue();
                                    idCC = oDSEntry.id;
                                }
                                break;
                            }
                        }
                    }
                }
            }
        };
        if (sCC == '') {
            for (arrInput in arrInputs) {
                var o = fsGeneralEntry_getById(arrInputs[arrInput])
                if (o) {
                    if ((o.tipoGS == 129) && ((oEdit.fsEntry.idEntryCC == o.campo_origen) || (oEdit.fsEntry.idEntryCC == o.idCampo))) {
                        sCC = o.getDataValue();
                        idCC = o.id;
                        break;
                    }
                }
            }
        }
    };

    borrarAnyoPartidaRelacionado(oEdit.fsEntry);
    var newWindow = window.open("../_common/BuscadorPartidaServer.aspx?CodPartida=" + sValor + "&fsEntry=" + oEdit.ID + "&Pres5=" + sPRES5 + "&CentroCoste=" + escape(sCC) + "&CCRelacionado=" + sTieneCCRelacionado + "&IDCentroCoste=" + idCC, "iframeWSServer")
    newWindow.focus();
};
//Comprueba que el c�digo de activo introducido es correcto.
function ddActivo_ValueChange(oEdit, oldValue, oEvent) {
    var sCC = "";
    sValor = trim(oEdit.getValue());
    oEdit.elem.title = sValor;
    if (oEdit.fsEntry.idEntryCC != null) {
        o = fsGeneralEntry_getById(oEdit.fsEntry.idEntryCC)
        if (o) {
            if (o.tipoGS == 129) sCC = o.getDataValue();
        } else {
            if (oEdit.fsEntry.idCampoPadre) {
                var desglose = oEdit.fsEntry.id.substr(0, oEdit.fsEntry.id.search("_fsdsentry"))
                var index = oEdit.fsEntry.id.substr(oEdit.fsEntry.id.search("_fsdsentry_") + 11, oEdit.fsEntry.id.search(oEdit.fsEntry.idCampo) - oEdit.fsEntry.id.search("_fsdsentry_") - 12)

                oDSEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + oEdit.fsEntry.idEntryCC)
                if (oDSEntry) {
                    if (oDSEntry.tipoGS == 129) sCC = oDSEntry.getDataValue()
                } else {

                    ////////////////
                    for (i = 0; i < document.getElementById(desglose + "_tblDesgloseHidden").rows[0].cells.length; i++) {
                        s = document.getElementById(desglose + "_tblDesgloseHidden").rows[0].cells[i].innerHTML
                        re = /fsentry/

                        x = s.search(re)
                        y = s.search("__tbl")

                        id = s.substr(x + 7, y - x - 7)
                        oEntryCC = fsGeneralEntry_getById(desglose + "_fsentry" + id)

                        if (oEntryCC) {
                            if ((oEntryCC.tipoGS == 129) && (oEntryCC.campo_origen == oEdit.fsEntry.idEntryCC)) {
                                oDSEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + fsGeneralEntry_getById(oEntryCC.id).idCampo)
                                if (oDSEntry) sCC = oDSEntry.getDataValue()

                                break;
                            }
                        }
                    }

                    ///////////////////////////

                }
            }
        };
        if (sCC == '') {
            for (arrInput in arrInputs) {
                var o = fsGeneralEntry_getById(arrInputs[arrInput])
                if (o) {
                    if ((o.tipoGS == 129) && ((oEdit.fsEntry.idEntryCC == o.campo_origen) || (oEdit.fsEntry.idEntryCC == o.idCampo))) {
                        sCC = o.getDataValue();
                        break;
                    }
                }
            }
        }
    };
    var newWindow = window.open("../_common/BuscadorActivoServer.aspx?codActivo=" + sValor + "&fsEntryId=" + oEdit.ID + "&idCentroCoste=" + escape(sCC), "iframeWSServer")
    newWindow.focus();
};
/*
''' <summary>
''' Abre la ventana de monedas para un dataentry importe repercutido
''' </summary>
''' <param name="relativeElement">this, es decir, el control desde el que se lanza la llamada a la funci�n</param>
''' <param name="oEditIDMon">ClienId del dataentry</param>
''' <param name="oEditIDBt">ClienId del link</param>        
''' <remarks>Llamada desde:javascript introducido para responder al onclick de todos los campos de tipo importe repercutido
''' ; Tiempo m�ximo:0</remarks>
*/
function ddMonedaopenDropDownEvent(relativeElement, oEditIDMon, oEditIDBt) {
    CerrarDesplegables();
    var posicion = [0, 0];
    posicion = showPosition(relativeElement, null, 200, 200, 10);
    var x = posicion[0];
    var y = posicion[1];

    mostrarFRAME("ddMoneda", x, y, 201, 201)

    var oEditMon
    oEditMon = fsGeneralEntry_getById(oEditIDMon)

    //ddMoneda.location = "../_common/LinkMonedas.aspx?FrameId=ddMoneda&EntryMoneda=" + oEditMon.id + "&EntryBoton=" + oEditIDBt
    if (document.getElementById("ddMoneda") != undefined) {
        document.getElementById("ddMoneda").src = "../_common/LinkMonedas.aspx?FrameId=ddMoneda&EntryMoneda=" + oEditMon.id + "&EntryBoton=" + oEditIDBt;
    };
};
// Cierra la ventana de monedas para un dataentry importe repercutido
function ddCerrarMoneda() {
    if (document.getElementById("ddMoneda")) {
        var oFrame = document.getElementById("ddMoneda");
        oFrame.parentNode.removeChild(oFrame);
    };
};
/*
''' <summary>
''' Abre la ventana de proveedores favoritos para un dataentry proveedor
''' </summary>
''' <param name="oEdit">Dataentry proveedor</param>
''' <param name="text">texto a buscar como parte de la denominaci�n del proveedor</param>
''' <param name="oEvent">Evento click</param>        
''' <remarks>Llamada desde:javascript introducido para responder al onclick de todos los campos de tipo proveedor favorito; Tiempo m�ximo:0</remarks>
*/
function ddProvFavopenDropDownEvent(oEdit, text, oEvent) {
    var x = 0;
    var y = 0;
    var posicion = showPosition(oEdit.Element, document.getElementById("ddFavoritos"), 200, 200, 5);
    x = posicion[0];
    y = posicion[1];
    mostrarFRAME("ddFavoritos", x, y, 200, 200)

    //Comprobar si existe Organizaci�n de Compras y Material relacionado con el Proveedor
    var oEntry
    oEntry = oEdit.fsEntry
    if (oEntry.idDataEntryDependent) {
        oEntry.dependentfield = oEntry.idDataEntryDependent
    }
    var sOrgCompras = ""
    if (oEntry.dependentfield) {
        var oOrgCompras = fsGeneralEntry_getById(oEntry.dependentfield)
        if (oOrgCompras) {
            sOrgCompras = oOrgCompras.getDataValue()
        }
    }
    var sMaterial = ""
    if (oEntry.idDataEntryDependent2) {
        var oMaterial = fsGeneralEntry_getById(oEntry.idDataEntryDependent2)
        if (oMaterial) {
            sMaterial = oMaterial.getDataValue()
        }
    };
    if (document.getElementById("ddFavoritos") != undefined) {
        document.getElementById("ddFavoritos").src = "../_common/provfavserver.aspx?FrameId=ddFavoritos&Entry=" + oEntry.id + "&OrgCompras=" + sOrgCompras + "&Material=" + sMaterial;
    }
};
/*
''' <summary>
''' Abre la ventana de presupuestos favoritos para un dataentry presupuesto
''' </summary>
''' <param name="oEdit">Dataentry presupuesto</param>
''' <param name="text">texto a buscar como parte de la denominaci�n del presupuesto</param>
''' <param name="oEvent">Evento click</param>        
''' <remarks>Llamada desde:javascript introducido para responder al onclick de todos los campos de tipo presupuesto favorito; Tiempo m�ximo:0</remarks>
*/
function ddPresupFavopenDropDownEvent(oEdit, text, oEvent) {
    CerrarDesplegables();
    var orgcomcod = "";
    var sIndex = "-1";
    if (oEdit.fsEntry.idCampoPadre) {
        var o = oEdit.fsEntry;
        var desglose = o.id.substr(0, o.id.search("_fsdsentry"));
        sIndex = o.id.substring((desglose + "_fsdsentry_").length, o.id.search("_" + o.idCampo))
    }
    if (oEdit.fsEntry.dependentfield) {
        if (sIndex == "-1") {
            if (fsGeneralEntry_getById(oEdit.fsEntry.dependentfield) != null) orgcomcod = fsGeneralEntry_getById(oEdit.fsEntry.dependentfield).getDataValue()
        } else {
            if (fsGeneralEntry_getById(oEdit.fsEntry.dependentfield.replace("fsentry", "fsdsentry_" + sIndex + "_")) != null) orgcomcod = fsGeneralEntry_getById(oEdit.fsEntry.dependentfield.replace("fsentry", "fsdsentry_" + sIndex + "_")).getDataValue()
        }
    }
    var relativeElement = oEdit.Element;
    var posicion = [0, 0];
    posicion = showPosition(relativeElement, null, 300, 200, 5);
    var x = posicion[0];
    var y = posicion[1];

    mostrarFRAME("ddFavoritos", x, y, 300, 200)

    var oEntry
    oEntry = oEdit.fsEntry;
    if (document.getElementById("ddFavoritos") != undefined) {
        document.getElementById("ddFavoritos").src = "../_common/presupfavserver.aspx?FrameId=ddFavoritos&Entry=" + oEntry.id + "&tipo=" + (oEntry.tipoGS - 109).toString() + "&orgcom=" + orgcomcod;
    };
};
// Cierra los desplegables de favoritos de proveedores y presupuestos
function ddCerrarFavoritos() {
    if (document.getElementById("ddFavoritos")) {
        var oFrame = document.getElementById("ddFavoritos")
        oFrame.parentNode.removeChild(oFrame);
    }
};
// Cierra la ventana de a�adir l�nea/eliminar l�nea en un desglose
function ddCerrarpopupDesglose() {
    if (document.getElementById("ddpopupDesglose")) {
        var oFrame = document.getElementById("ddpopupDesglose")
        oFrame.parentNode.removeChild(oFrame);
    }
};
// Cerrar desplegables
function CerrarDesplegables(evt) {
    try {
        ddCerrarpopupDesglose();
        ddCerrarFavoritos();
        ddCerrarMoneda();
        CloseDateCalendars(evt);

        return true
    } catch (e) { }
};
function show_mat(oEntry) {

    var oEntryMaterial;
    o = oEntry.fsEntry;
    CerrarDesplegables();
    var sCodProve = "";
    nivelSeleccion = 0;

    //Restricciones de material
    var sRestric = o.Restric;
    if (!o.isDesglose) {
        nivelSeleccion = o.NivelSeleccion;
    } else {
        //comprobar si hay un campo de material fuera del desglose
        oEntryMaterial = getMaterialFormulario();
        if (oEntryMaterial) {
            //si existe y tiene un nivel obligatorio, solo podemos seleccionar dicho nivel o descendientes
            if (oEntryMaterial.NivelSeleccion != 0) {
                sRestric = oEntryMaterial.dataValue;
                //si no tiene valor
                if (sRestric) {
                    nivelSeleccion = oEntryMaterial.NivelSeleccion;
                } else {
                    show_MensajeMaterialObligatorio();
                    return false;
                }
            }
        }
    }

    if (oEntry.fsEntry.TipoSolicit == 5) { //Comprueba si se viene de Contrato
        if (document.getElementById("hid_Proveedor")) { //Comprueba si hay hidden de Proveedor (si ya hay proveedor seleccionado)
            sCodProve = document.getElementById("hid_Proveedor").value //Se obtiene el valor del proveedor
        }
    }

    //Si se viene de contrato, en la querystring de materiales.aspx va incluido el par�metro de proveedor con su valor
    //Si se viene de otra p�gina, se realiza la b�squeda de materiales sin indicar proveedor
    var newWindow;
    newWindow = window.open("../_common/materiales.aspx?Valor=" + o.getDataValue() + "&IdControl=" + o.id + "&Mat=" + sRestric + "&readonly=" + (o.readOnly ? "1" : "0") + "&CodProve=" + sCodProve + "&NivelSeleccion=" + nivelSeleccion, "_blank", "width=550,height=550,status=yes,resizable=no,top=100,left=200");
    newWindow.focus();
};
function show_UnidadesOrganizativa(oEntry) {
    o = oEntry.fsEntry;
    CerrarDesplegables();
    var newWindow = window.open(rutaFS + "_Common/UnidadesOrganizativas.aspx?Valor=" + o.getDataValue() + "&IdControl=" + o.id, "_blank", "width=550,height=550,status=yes,resizable=no,top=100,left=200")
    newWindow.focus();
};
//rue 160309	
//Descripci�n:	Abre la ventana de presupuestos para un dataentry presupuesto
//Param. Entrada: 
//	otxt		nombre del dataentry
//	svalor		valor que teien asociado
//	oEvent	    Evento producido
//Param. Salida: -
//LLamada:	javascript introducido para responder al onclick de todos los campos de tipo presupuesto
function show_pres(oTxt, sValor, oEvent) {        
    o = oTxt.fsEntry
    var sValor = o.getDataValue()
    if (sValor == null) sValor = ""
    if (document.getElementById("Solicitud")) vSolicitud = document.getElementById("Solicitud").value
    else vSolicitud = ""

    if (document.getElementById("Instancia")) vInstancia = document.getElementById("Instancia").value
    else vInstancia = ""
    var dCantidad = ""
    var sTitulo = ""
    var codOrgCompras = ""
    var articulo = ""

    if (o.dependentfield) {
        oOrgCompras = fsGeneralEntry_getById(o.dependentfield)
        if (oOrgCompras != null) {
            codOrgCompras = oOrgCompras.getDataValue()
            if (oOrgCompras.dependentfield) {
                oArt = fsGeneralEntry_getById(oOrgCompras.dependentfield)
                if (oArt) articulo = oArt.codigoArticulo
            }
        }
    }

    //B�squeda en el desglose
    if (o.idCampoPadre) {
        var desglose = o.id.substr(0, o.id.search("_fsdsentry"))
        var index = o.id.substr(o.id.search("_fsdsentry_") + 11, o.id.search(o.idCampo) - o.id.search("_fsdsentry_") - 12)
        var idArticulo = null
        var idCantidad = null
        var idUnidad = null
        var iOrgCompras = null
        for (i = 0; i < document.getElementById(desglose + "_tblDesgloseHidden").rows[0].cells.length; i++) {
            s = document.getElementById(desglose + "_tblDesgloseHidden").rows[0].cells[i].innerHTML
            re = /fsentry/

            x = s.search(re)
            y = s.search("__tbl")

            id = s.substr(x + 7, y - x - 7)
            oEntry = fsGeneralEntry_getById(desglose + "_fsentry" + id)

            if (oEntry) {
                if (oEntry.tipoGS == 4) idCantidad = oEntry.id
                if ((oEntry.tipoGS == 104) || (oEntry.tipoGS == 119)) {
                    idArticulo = oEntry.id
                    idOrgCompras = oEntry.idDataEntryDependent
                }
                if (oEntry.tipoGS == 105) idUnidad = oEntry.id
                if (oEntry.tipoGS == 123) iOrgCompras = oEntry.id
            }
        }

        if (idCantidad != null && idArticulo != null && idUnidad != null) {
            oDSEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + fsGeneralEntry_getById(idArticulo).idCampo)
            if (oDSEntry) sTitulo = oDSEntry.getValue()

            oDSEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + fsGeneralEntry_getById(idCantidad).idCampo)
            if (oDSEntry) dCantidad = oDSEntry.getValue()
        }
        if (idArticulo != null) {
            oDSEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + fsGeneralEntry_getById(idArticulo).idCampo)
            if (oDSEntry) articulo = oDSEntry.getDataValue()
        }        
        if (iOrgCompras != null) {
            oDSEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + fsGeneralEntry_getById(iOrgCompras).idCampo)
            if (oDSEntry) codOrgCompras = oDSEntry.getDataValue();
        }
    }

    //Si no encuentro la organizaci�n de compras en el desglose la busco fuera de desglose
    if (iOrgCompras == null || codOrgCompras == null || codOrgCompras == "") {  
        var arOrgCompras = $.grep(arrInputs, function (elem) {
            var oInput = fsGeneralEntry_getById(elem);
            if (oInput.idCampoPadre == null && oInput.tipoGS == 123) return oInput.id;
        });
        if (arOrgCompras.length > 0) codOrgCompras = fsGeneralEntry_getById(arOrgCompras[0]).getDataValue();                             
    }    

    if (articulo == null) {
        articulo = "";
    }
    CerrarDesplegables();
    var newWindow = window.open(rutaFS + "_common/BuscadorPresupuestos.aspx?Valor=" + Var2Param(sValor) + "&IDPres=" + o.id + "&Campo=" + o.idCampo + "&Instancia=" + vInstancia.toString() + "&Solicitud=" + vSolicitud.toString() + "&Cantidad=" + dCantidad + "&Titulo=" + Var2Param(sTitulo) + "&OrgCompras=" + codOrgCompras + "&Articulo=" + articulo, "_blank", "width=840,height=660,status=yes,resizable=no,top=75,left=120")
    newWindow.focus();
};
function show_pres_desde_cabecera(IDPres, Campo, EsCabecera, Instancia, Solicitud) {
    var pertenece;
    var cadenaOrgCompras = "";
    var arrPres = new Array()
    if (EsCabecera != 0) {
        //PRES1 = 110
        //Pres2 = 111
        //Pres3 = 112
        //Pres4 = 113			
        switch (EsCabecera) {
            case '110':
                {
                    arrPres = arrPres1;
                    break;
                }
            case '111':
                {
                    arrPres = arrPres2;
                    break;
                }
            case '112':
                {
                    arrPres = arrPres3;
                    break;
                }
            case '113':
                {
                    arrPres = arrPres4;
                    break;
                }
        }

        for (i = 0; i < arrPres.length; i++) {
            pertenece = -1
            o = fsGeneralEntry_getById(arrPres[i])
            if (o != null) {
                pertenece = arrPres[i].indexOf(Campo)
                if (pertenece != -1) {
                    var desglose = o.id.substr(0, o.id.search("_fsdsentry"))
                    var index = o.id.substr(o.id.search("_fsdsentry_") + 11, o.id.search(o.idCampo) - o.id.search("_fsdsentry_") - 12)

                    if (index > 0) {
                        var iOrgCompras = null
                        var iPrimerArticulo = null
                        for (j = 0; j < document.getElementById(desglose + "_tblDesgloseHidden").rows[0].cells.length; j++) {
                            s = document.getElementById(desglose + "_tblDesgloseHidden").rows[0].cells[j].innerHTML
                            re = /fsentry/

                            x = s.search(re)
                            y = s.search("__tbl")

                            id = s.substr(x + 7, y - x - 7)
                            oEntry = fsGeneralEntry_getById(desglose + "_fsentry" + id)

                            if (oEntry) {
                                if (oEntry.tipoGS == 123) {
                                    iOrgCompras = oEntry.id
                                }
                                if ((oEntry.tipoGS == 104) || (oEntry.tipoGS == 119) || (iPrimerArticulo == null)) {
                                    iPrimerArticulo = oEntry.id
                                    //iOrgCompras = oEntry.idDataEntryDependent
                                }
                            }
                        };

                        var codOrgCompras="";
                        if (iOrgCompras != null) {
                            oDSEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + fsGeneralEntry_getById(iOrgCompras).idCampo);
                            codOrgCompras = oDSEntry.getDataValue();                                
                        }
                        if (codOrgCompras != "") {
                            cadenaOrgCompras = cadenaOrgCompras + "'" + codOrgCompras + "',";  
                        }
                        else {
                            //Si no encuentro la organizaci�n de compras en el desglose la busco fuera de desglose                            
                            var arOrgCompras = $.grep(arrInputs, function (elem) {
                                var oInput = fsGeneralEntry_getById(elem);
                                if (oInput.idCampoPadre == null && oInput.tipoGS == 123) return oInput.id;
                            });
                            if (arOrgCompras.length > 0) {
                                oDSEntry = fsGeneralEntry_getById(arOrgCompras[0]);
                                cadenaOrgCompras = cadenaOrgCompras + "'" + oDSEntry.getDataValue() + "',";   
                            }
                        }
                                                   
                        if (iPrimerArticulo != null) {
                            oDSEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + fsGeneralEntry_getById(iPrimerArticulo).idCampo)
                            iPrimerArticulo = oDSEntry.getDataValue()
                        }
                    }
                }
            }
        }
    };
    if (cadenaOrgCompras.length > 0) {
        cadenaOrgCompras = cadenaOrgCompras.substr(cadenaOrgCompras, cadenaOrgCompras.length - 1)
    }
    params = {
        IDPres: IDPres,
        Campo: Campo,
        EsCabecera: EsCabecera,
        Instancia: Instancia,
        Solicitud: Solicitud,
        Articulo: iPrimerArticulo,
        cadenaOrgComp: cadenaOrgCompras
    };
    var respuesta;
    $.when($.ajax({
        type: "POST",
        url: rutaPM + 'ConsultasPMWEB.asmx/Obtener_DatosPresupuesto',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        respuesta = msg.d;
        if (respuesta.Mensaje != "") {
            mostrarMensaje(respuesta.Mensaje)
        } else {
            var newWindow = window.open(rutaFS + "_common/BuscadorPresupuestos.aspx?IDPres=" + respuesta.Input + "&Campo=" + respuesta.Campo + "&EsCabecera=" + respuesta.EsCabecera + "&Instancia=" + respuesta.Instancia + "&Solicitud=" + respuesta.Solicitud + "&Articulo=" + respuesta.Articulo + "&OrgCompras=" + respuesta.OrgCompras, "_blank", "width=800,height=600,status=yes,resizable=no,top=75,left=120");
            newWindow.focus();
        }
    });
};
//Descripci�n:	Abre la ventana de presupuestos para un dataentry presupuesto de solo lectura
//Param. Entrada: 
//	otxt		nombre del dataentry
//	svalor		valor que tiene asociado
//	oEvent	    Evento producido
//Param. Salida: -
//LLamada:	javascript introducido para responder al onclick de todos los campos de tipo presupuesto
//Tiempo:	0	
function show_pres_ReadOnly(oTxt, sValor, oEvent) {
    o = oTxt.fsEntry
    var sValor = o.getDataValue()
    if (sValor == null) sValor = ""
    if (document.getElementById("Solicitud")) vSolicitud = document.getElementById("Solicitud").value
    else vSolicitud = ""

    if (document.getElementById("Instancia")) vInstancia = document.getElementById("Instancia").value
    else vInstancia = ""
    var dCantidad = ""
    var sTitulo = ""
    if (o.idCampoPadre) {
        var desglose = o.id.substr(0, o.id.search("_fsdsentry"))
        var index = o.id.substr(o.id.search("_fsdsentry_") + 11, o.id.search(o.idCampo) - o.id.search("_fsdsentry_") - 12)
        var idArticulo = null
        var idCantidad = null
        var idUnidad = null
        for (i = 0; i < document.getElementById(desglose + "_tblDesgloseHidden").rows[0].cells.length; i++) {
            s = document.getElementById(desglose + "_tblDesgloseHidden").rows[0].cells[i].innerHTML
            re = /fsentry/
            x = s.search(re)
            y = s.search("__tbl")

            id = s.substr(x + 7, y - x - 7)
            oEntry = fsGeneralEntry_getById(desglose + "_fsentry" + id)

            if (oEntry) {
                if (oEntry.tipoGS == 4) idCantidad = oEntry.id
                if (oEntry.tipoGS == 104) idArticulo = oEntry.id
                if (oEntry.tipoGS == 105) idUnidad = oEntry.id
            }
        }
        if (idCantidad != null && idArticulo != null && idUnidad != null) {

            oDSEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + fsGeneralEntry_getById(idArticulo).idCampo)
            sTitulo = oDSEntry.getValue()

            oDSEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + fsGeneralEntry_getById(idCantidad).idCampo)
            dCantidad = oDSEntry.getValue()
        }
    }
    CerrarDesplegables();
    var newWindow = window.open("../_common/presupuestosreadonly.aspx?Valor=" + Var2Param(sValor) + "&IDPres=" + o.id + "&Campo=" + o.idCampo + "&Instancia=" + vInstancia.toString() + "&Solicitud=" + vSolicitud.toString() + "&Cantidad=" + dCantidad + "&Titulo=" + Var2Param(sTitulo), "_blank", "width=550,height= 250,status=yes,resizable=yes,top=100,left=120")
    newWindow.focus();
};
/*
''' <summary>
''' Mostrar una pantalla de selecci�n de proveedores. Se hara en funci�n de la organizaci�n de compras de haberla.
'''	Puede estar pero oculta, entonces se usa arrGSOcultos para sacarla.
''' </summary>
''' <param name="oEntry">Entry de tipo proveedor que provoco esta llamada</param> 
''' <returns>Nada. Llama a _common/proveedores.aspx y esta es la devuelve lo seleccionado</returns>
''' <remarks>Llamada desde:GeneralEntry.vb ; Tiempo m�ximo:0 </remarks>
*/
function show_proves(oEntry) {
    var d = document.getElementById("ddProvFav");
    if (d) {
        eliminarFRAME()
    }
    oEntry = oEntry.fsEntry
    var oDepen
    var oEntryArt
    var visibilidad
    var IdOrgCompras
    var x
    var sCodArt = ""
    if (oEntry == null) return;
    bSinArticulo = false;

    if (oEntry.CargarProveSumiArt) {
        if (oEntry.idEntryART && oEntry.idEntryART != "") {
            oEntryArt = fsGeneralEntry_getById(oEntry.idEntryART);
            if (oEntryArt != null) {
                sCodArt = oEntryArt.getValue();
            } else {
                //El articulo es visible Y esta en desglose
                //El articulo es visible Y no permite escritura Y esta en campo
                var Encontrado
                Encontrado = 0
                for (arrInput in arrInputs) {
                    var o = fsGeneralEntry_getById(arrInputs[arrInput])
                    if (o) {
                        tipoGS = o.tipoGS
                        if (tipoGS == 119 || tipoGS == 104) {
                            //Desglose No Pop
                            if (o.nLinea == oEntry.nLinea && o.idCampoPadre == oEntry.idCampoPadre) {
                                sCodArt = o.getDataValue()
                                Encontrado = 1
                                break
                            } else {
                                //Campo
                                if (oEntry.idCampoPadre == "") {
                                    sCodArt = o.getDataValue()
                                    Encontrado = 1
                                    break
                                }
                            }
                        }
                    }
                }
                //Invisible, luego en arrGSOcultos
                if (Encontrado == 0) {
                    if (oEntry.id.search("fsentry") > -1) {
                        //No desglose
                        re = /_fsentry/
                        s = oEntry.id
                        y = s.search(re)
                        x = -1
                        for (i = 0; i < y; i++) {
                            if (s.charAt(i) == "_") {
                                x = i
                            }
                        }
                        s2 = s.substring(x + 1, y)

                        for (arrGSOculto in arrGSOcultos) {
                            sBuscada = arrGSOcultos[arrGSOculto].substring(0, 3 + y - x)
                            if ((sBuscada == "119_" + s2) || (sBuscada == "104_" + s2)) {
                                x = arrGSOcultos[arrGSOculto].search("__")
                                sCodArt = arrGSOcultos[arrGSOculto].substr(x + 2)
                                break
                            }
                        }
                    } else {
                        //Desglose
                        s2 = oEntry.idCampoPadre

                        re = /fsdsentry/
                        x1 = oEntry.id.search(re) + 10
                        y1 = oEntry.id.search(oEntry.idCampo) - 1
                        iLinea = oEntry.id.substring(x1, y1)
                        for (arrGSOculto in arrGSOcultos) {
                            sBuscada = arrGSOcultos[arrGSOculto].substring(0, s2.length + 4)
                            if ((sBuscada == "119_" + s2) || (sBuscada == "104_" + s2)) {
                                if (iLinea == arrGSOcultos[arrGSOculto].substr(arrGSOcultos[arrGSOculto].search("___") + 3)) {
                                    //sacar el valor
                                    x = arrGSOcultos[arrGSOculto].search("__")
                                    y = arrGSOcultos[arrGSOculto].search("___")
                                    sCodArt = arrGSOcultos[arrGSOculto].substring(x + 2, y)
                                    break
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    if (oEntry.idDataEntryDependent) {
        oEntry.dependentfield = oEntry.idDataEntryDependent
    }

    if (oEntry.dependentfield) {
        oDepen = fsGeneralEntry_getById(oEntry.dependentfield)

        if (oDepen) {
            if (oDepen.readOnly == true) {
                visibilidad = "readOnly"
            } else {
                visibilidad = "visible"
            }
            valorOrgCompras = oDepen.getDataValue()

            sDepenId = oDepen.id
        }
        else {
            oDepen = document.getElementById(oEntry.dependentfield + '__t');
            if (oDepen) {
                visibilidad = "readOnly";
                valorOrgCompras = oDepen.value;
                sDepenId = oEntry.dependentfield;
            }
        }
    } else //El dependiente no existe (por no haber campo orgCompras) o es no visible.
    {
        valorOrgCompras = "null"
        visibilidad = "null"
        sDepenId = "null"

        if (oEntry.id.search("fsentry") > -1) {
            //No desglose
            re = /_fsentry/
            s = oEntry.id
            y = s.search(re)
            x = -1
            for (i = 0; i < y; i++) {
                if (s.charAt(i) == "_") {
                    x = i
                }
            }
            s2 = s.substring(x + 1, y)

            for (arrGSOculto in arrGSOcultos) {
                sBuscada = arrGSOcultos[arrGSOculto].substring(0, 3 + y - x)
                if (sBuscada == "123_" + s2) //Es organizacion compras
                {
                    //sacar el valor

                    x = arrGSOcultos[arrGSOculto].search("__")
                    valor = arrGSOcultos[arrGSOculto].substr(x + 2)
                    sDepenId = "oculto"
                    valorOrgCompras = valor
                }
            }
        } else {
            //Desglose
            s2 = oEntry.idCampoPadre

            //Buscar el numero de Grupo (por si hay una organizacion de compras en el grupo fuera del desglose)
            re = s2 + "_fsdsentry"
            y1 = oEntry.id.search(re) - 1
            s3 = oEntry.id.substring(0, y1)
            x1 = s3.lastIndexOf("_")
            s4 = oEntry.id.substring(x1 + 1, y1)

            //Mirar si hay alguna organizaci�n de compra oculta en el grupo s4
            var bfueraDeDesglose = false
            if (s4 != "") {
                for (arrGSOculto in arrGSOcultos) {
                    sBuscada = arrGSOcultos[arrGSOculto].substring(0, s4.length + 4)
                    if (sBuscada == "123_" + s4) //Es organizacion compras
                    {
                        x = arrGSOcultos[arrGSOculto].search("__")
                        valorOrgCompras = arrGSOcultos[arrGSOculto].substr(x + 2)
                        sDepenId = "oculto"
                        bfueraDeDesglose = true
                    }
                }
            };
            if (bfueraDeDesglose == false) {
                //Buscar Tambien el numero de linea
                re = /fsdsentry/
                x1 = oEntry.id.search(re) + 10
                y1 = oEntry.id.search(oEntry.idCampo) - 1
                iLinea = oEntry.id.substring(x1, y1)

                var sPrimerOculto = ""
                var bEncontrado = false
                var sPrimerOculto = ""
                for (arrGSOculto in arrGSOcultos) {
                    sBuscada = arrGSOcultos[arrGSOculto].substring(0, s2.length + 4)
                    if (sBuscada == "123_" + s2) //Es organizacion compras
                    {
                        //Guardar el valor de la primera linea en caso de que sea necesario met�rselo
                        //a un campo de una linea a�adida
                        if ("1" == arrGSOcultos[arrGSOculto].substr(arrGSOcultos[arrGSOculto].search("___") + 3)) {
                            sPrimerOculto = arrGSOcultos[arrGSOculto]
                        }

                        if (iLinea == arrGSOcultos[arrGSOculto].substr(arrGSOcultos[arrGSOculto].search("___") + 3)) {
                            //sacar el valor
                            x = arrGSOcultos[arrGSOculto].search("__")
                            y = arrGSOcultos[arrGSOculto].search("___")
                            valor = arrGSOcultos[arrGSOculto].substring(x + 2, y)
                            sDepenId = "oculto"
                            valorOrgCompras = valor
                            bEncontrado = true
                        }

                    }
                }
                //Si bEncontrado = false se trata de alguna linea a�adida, luego se le da el valor de
                //la primera y se crea en el array de GSOcultos
                if (bEncontrado == false) {
                    if ((arrGSOcultos.length != 0) && (sPrimerOculto != "")) {
                        sNuevoOculto = sPrimerOculto.substring(0, sPrimerOculto.search("___") + 3) + iLinea
                        arrGSOcultos[arrGSOcultos.length] = sNuevoOculto

                        x = sNuevoOculto.search("__")
                        y = sNuevoOculto.search("___")
                        valor = sNuevoOculto.substring(x + 2, y)
                        sDepenId = "oculto"
                        valorOrgCompras = valor
                    }
                }
            } //Fin bFueraDeDesglose = False

        }
    }
    p = window.opener;
    if (p) {
        if (p.sDataEntryOrgComprasFORM) //Desglose no pop-up con organizaci�n de compras antes del desglose
        {
            //sDepenId = "oculto"
            //valorOrgCompras = valor	
            var oOrgCompras = p.fsGeneralEntry_getById(p.sDataEntryOrgComprasFORM)
            if (oOrgCompras) {
                if (valorOrgCompras == '') {
                    valorOrgCompras = oOrgCompras.getDataValue()
                }
                sDepenId = oOrgCompras.id
                if (oOrgCompras.readOnly == true) {
                    visibilidad = "readOnly"
                } else {
                    visibilidad = "visible"
                }
            }
        } else { //Comprobar si la organizaci�n de compras est� oculta
            if (p.sCodOrgComprasFORM) {
                sDepenId = "oculto"
                valorOrgCompras = p.sCodOrgComprasFORM
            }
        }
    }

    //Paso del valor del material relacionado como par�metro
    var material = ""
    if (oEntry.idDataEntryDependent2) {
        var oMaterial = fsGeneralEntry_getById(oEntry.idDataEntryDependent2)
        if (oMaterial) {
            material = oMaterial.getDataValue()
        }
    }

    CerrarDesplegables();

    var param = '';
    if (oEntry.independiente == "1") param = '&NoConf=1'

    if ((oEntry.TipoSolicit == 8) || (oEntry.TipoSolicit == 9)) {
        if (oEntry.TipoSolicit == 8) {
            param = param + '&desde2904=PedidoExpress';
        }
        if (oEntry.TipoSolicit == 9) {
            param = param + '&desde2904=PedidoNegociado';

            //Numero de articulos ya rellenos
            var Rellenos = 0;

            for (arrInput in arrInputs) {
                o = fsGeneralEntry_getById(arrInputs[arrInput]);
                if (o) {
                    if (o.tipo == 9 && o.tipoGS != 136) { //desglose
                        for (k = 1; k <= document.getElementById(o.id + "__numTotRows").value; k++) {
                            if (document.getElementById(o.id + "__" + k.toString() + "__Deleted")) {
                                for (arrCampoHijo in o.arrCamposHijos) {
                                    var oHijoSrc = document.getElementById(o.id + "__" + o.arrCamposHijos[arrCampoHijo]);
                                    if (oHijoSrc) {
                                        var oHijo = document.getElementById(o.id + "__" + k.toString() + "__" + o.arrCamposHijos[arrCampoHijo]);
                                        if (oHijo) {
                                            if (oHijoSrc.getAttribute("tipoGS") == 119) { //Art nuevo
                                                if (oHijo.value != "") {
                                                    Rellenos = 1;
                                                    break;
                                                }
                                            }
                                            if (oHijoSrc.getAttribute("tipoGS") == 104) { //Art viejo
                                                if (oHijo.value != "") {
                                                    Rellenos = 1;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (o.tipoGS == 119) { //Art nuevo
                            if (o.getDataValue() != "") {
                                Rellenos = 1;
                                break;
                            }
                        }
                        if (o.tipoGS == 104) { //Art viejo
                            if (o.getDataValue() != "") {
                                Rellenos = 1;
                                break;
                            }
                        }
                    }
                }
            }
            param = param + '&Rellenos=' + Rellenos;
        }

        for (arrInput in arrInputs) {
            var o = fsGeneralEntry_getById(arrInputs[arrInput])
            if (o) {
                if (o.tipoGS == 100) {
                    if (o.idDataEntryDependent3) { //Forma Pago existe pero invisible
                        param = param + '&IDFPagoHidden=' + o.id;
                    }
                }
                if (o.tipoGS == 101) {
                    param = param + '&IDFPago=' + o.id;
                }
                if (o.tipoGS == 130) {
                    sPartida = o.getDataValue();

                    arrAux = sPartida.split("#");

                    if (arrAux[2] == null) {
                        //00#212
                        sUon1 = arrAux[0]

                        param = param + '&Uon1=' + sUon1
                    } else {
                        if (arrAux[3] == null) {
                            //00#01#212|200
                            sUon1 = arrAux[0]
                            sUon2 = arrAux[1]

                            param = param + '&Uon1=' + sUon1 + '&Uon2=' + sUon2
                        } else {
                            if (arrAux[4] == null) {
                                //00#01#01#212|200
                                sUon1 = arrAux[0]
                                sUon2 = arrAux[1]
                                sUon3 = arrAux[2]

                                param = param + '&Uon1=' + sUon1 + '&Uon2=' + sUon2 + '&Uon3=' + sUon3
                            } else {
                                //00#01#01#01#212|200
                                sUon1 = arrAux[0]
                                sUon2 = arrAux[1]
                                sUon3 = arrAux[2]
                                sUon4 = arrAux[3]

                                param = param + '&Uon1=' + sUon1 + '&Uon2=' + sUon2 + '&Uon3=' + sUon3 + '&Uon4=' + sUon4
                            }
                        }
                    }
                }
            }
        }
    };
    var newWindow = window.open(rutaFS + "_common/BuscadorProveedores.aspx?PM=" + oEntry.PM + "&Valor=" + oEntry.getDataValue() + "&IDControl=" + oEntry.id + "&IDDepen=" + sDepenId + "&Campo=" + oEntry.idCampo + "&ValorOrgCompras=" + valorOrgCompras + "&Visibilidad=" + visibilidad + "&Mat=" + material + "&ComboContactos=" + oEntry.esProveedorParticipante + param + "&CodArt=" + sCodArt, "_blank", "width=835,height=635,status=yes,resizable=yes,top=0,left=150,scrollbars=yes")
    newWindow.focus();
};
//muestra los usuarios para que el usuario elija uno.
function show_personas(oEntry) {
    oEntry = oEntry.fsEntry;

    CerrarDesplegables();
    var newWindow = window.open(rutaFS + "_common/BuscadorUsuarios.aspx?desde=showPersonas&Valor=" + oEntry.getDataValue() + "&IDControl=" + oEntry.id + "&Campo=" + oEntry.idCampo, "_blank", "width=800,height=575,status=yes,resizable=no,top=200,left=200,scrollbars=yes")
    newWindow.focus();
};
//muestra los centros de coste para que el usuario elija uno.
function show_centros_coste(oEntry) {    
    oEntry = oEntry.fsEntry;    
    CerrarDesplegables();
    if (document.getElementById("hidEmpresa").value) {       
        var newWindow = window.open(rutaFS + "PM/CentrosCoste/centrosCoste.aspx?Valor=" + encodeURIComponent(oEntry.getDataValue()) + "&IDControl=" + oEntry.id + "&Campo=" + oEntry.idCampo + "&VerUON=" + (oEntry.VerUON ? "1" : "0") + "&VerBts=" + (oEntry.readOnly ? "0" : "1"  + "&IdEmpresa=" + document.getElementById("hidEmpresa").value), "_blank", "width=750,height=475,status=yes,resizable=no,top=200,left=200")
    }
    else {                
        var newWindow = window.open(rutaFS + "PM/CentrosCoste/centrosCoste.aspx?Valor=" + encodeURIComponent(oEntry.getDataValue()) + "&IDControl=" + oEntry.id + "&Campo=" + oEntry.idCampo + "&VerUON=" + (oEntry.VerUON ? "1" : "0") + "&VerBts=" + (oEntry.readOnly ? "0" : "1"), "_blank", "width=750,height=475,status=yes,resizable=no,top=200,left=200")
    }
   
    newWindow.focus();
};
//muestra las partidas presupuestarias para que el usuario elija una.
function show_partidas(oEntry) {
    var centro_coste = "";
    var centro_coste_den = "";
    var centro_coste_entry_id = ""
    oEntry = oEntry.fsEntry;
    if (oEntry.idEntryCC != null) {
        o = fsGeneralEntry_getById(oEntry.idEntryCC)
        if (o) {
            if (o.tipoGS == 129) {
                centro_coste_entry_id = o.id;
                centro_coste = o.getDataValue();
                centro_coste_den = o.getValue();
            }
        } else {
            if (oEntry.idCampoPadre) {
                var desglose = oEntry.id.substr(0, oEntry.id.search("_fsdsentry"))
                var index = oEntry.id.substr(oEntry.id.search("_fsdsentry_") + 11, oEntry.id.search(oEntry.idCampo) - oEntry.id.search("_fsdsentry_") - 12)

                oDSEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + oEntry.idEntryCC)
                if (oDSEntry) {
                    if (oDSEntry.tipoGS == 129) {
                        centro_coste_entry_id = oDSEntry.id;
                        centro_coste = oDSEntry.getDataValue();
                        centro_coste_den = oDSEntry.getValue();
                    }
                } else {
                    for (i = 0; i < document.getElementById(desglose + "_tblDesgloseHidden").rows[0].cells.length; i++) {
                        s = document.getElementById(desglose + "_tblDesgloseHidden").rows[0].cells[i].innerHTML
                        re = /fsentry/

                        x = s.search(re)
                        y = s.search("__tbl")

                        id = s.substr(x + 7, y - x - 7)
                        oEntryCC = fsGeneralEntry_getById(desglose + "_fsentry" + id)

                        if (oEntryCC) {
                            if ((oEntryCC.tipoGS == 129) && (oEntryCC.campo_origen == oEntry.idEntryCC)) {
                                oDSEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + fsGeneralEntry_getById(oEntryCC.id).idCampo)
                                if (oDSEntry) {
                                    centro_coste_entry_id = oDSEntry.id;
                                    centro_coste = oDSEntry.getDataValue();
                                    centro_coste_den = oDSEntry.getValue();
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }

        if (centro_coste_entry_id == '') {
            for (arrInput in arrInputs) {
                var o = fsGeneralEntry_getById(arrInputs[arrInput])
                if (o) {
                    if ((o.tipoGS == 129) && ((oEntry.idEntryCC == o.campo_origen) || (oEntry.idEntryCC == o.idCampo))) {
                        centro_coste_entry_id = o.id;
                        centro_coste = o.getDataValue();
                        centro_coste_den = o.getValue();
                        break;
                    }
                }
            }
        }
    };
    CerrarDesplegables();
    var newWindow = window.open(rutaFS + "PM/CentrosCoste/partidas.aspx?PRES5=" + oEntry.PRES5 + "&Valor=" + encodeURIComponent(oEntry.getDataValue()) + "&IDControl=" + oEntry.id + "&Campo=" + oEntry.idCampo + "&CCEntryId=" + centro_coste_entry_id + "&CentroCoste=" + encodeURIComponent(centro_coste) + "&CentroCosteDen=" + centro_coste_den + "&Titulo=" + oEntry.title, "_blank", "width=850,height=475,status=yes,resizable=no,top=200,left=100");
    newWindow.focus();
};
//muestra el buscador de activos
function show_activos(oEntry) {
    var uons = "";
    var centro_coste_entry_id = ""
    var centro_coste_den = "";
    oEntry = oEntry.fsEntry;

    if (oEntry.idEntryCC != null) {
        o = fsGeneralEntry_getById(oEntry.idEntryCC)
        if (o) {
            if (o.tipoGS == 129) {
                uons = o.getDataValue();
                centro_coste_entry_id = o.id;
                centro_coste_den = o.getValue();
            }
        } else {
            if (oEntry.idCampoPadre) {
                var desglose = oEntry.id.substr(0, oEntry.id.search("_fsdsentry"))
                var index = oEntry.id.substr(oEntry.id.search("_fsdsentry_") + 11, oEntry.id.search(oEntry.idCampo) - oEntry.id.search("_fsdsentry_") - 12)

                oDSEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + oEntry.idEntryCC)
                if (oDSEntry) {
                    if (oDSEntry.tipoGS == 129) {
                        uons = oDSEntry.getDataValue();
                        centro_coste_entry_id = oDSEntry.id;
                        centro_coste_den = oDSEntry.getValue();
                    }
                } else {
                    for (i = 0; i < document.getElementById(desglose + "_tblDesgloseHidden").rows[0].cells.length; i++) {
                        s = document.getElementById(desglose + "_tblDesgloseHidden").rows[0].cells[i].innerHTML
                        re = /fsentry/

                        x = s.search(re)
                        y = s.search("__tbl")

                        id = s.substr(x + 7, y - x - 7)
                        oEntryCC = fsGeneralEntry_getById(desglose + "_fsentry" + id)

                        if (oEntryCC) {
                            if ((oEntryCC.tipoGS == 129) && (oEntryCC.campo_origen == oEntry.idEntryCC)) {
                                oDSEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + fsGeneralEntry_getById(oEntryCC.id).idCampo)
                                if (oDSEntry) {
                                    uons = oDSEntry.getDataValue()
                                    centro_coste_entry_id = oDSEntry.id;
                                    centro_coste_den = oDSEntry.getValue();
                                }
                                break;
                            }
                        }
                    };
                }
            }
        };
        if (centro_coste_entry_id == '') {
            for (arrInput in arrInputs) {
                var o = fsGeneralEntry_getById(arrInputs[arrInput])
                if (o) {
                    if ((o.tipoGS == 129) && ((oEntry.idEntryCC == o.campo_origen) || (oEntry.idEntryCC == o.idCampo))) {
                        uons = o.getDataValue();
                        centro_coste_entry_id = o.id;
                        centro_coste_den = o.getValue();
                        break;
                    }
                }
            }
        }
    };
    CerrarDesplegables();
    var newWindow = window.open(rutaFS + "SM/Activos/BuscadorActivos.aspx?Valor=" + escape(oEntry.getDataValue()) + "&IDControl=" + oEntry.id + "&Campo=" + oEntry.idCampo + "&CCEntryId=" + centro_coste_entry_id + "&UONs=" + escape(uons) + "&CentroCosteDen=" + escape(centro_coste_den), "_blank", "width=850,height=600,status=yes,resizable=no,top=50,left=100")
    newWindow.focus();
};
function show_atached_files(oEntry) {
    oEntry = oEntry.fsEntry;
    if (document.getElementById("Solicitud")) vSolicitud = document.getElementById("Solicitud").value
    else vSolicitud = ""

    if (document.getElementById("Instancia")) vInstancia = document.getElementById("Instancia").value
    else vInstancia = ""

    if (document.getElementById("Favorito")) vFavorito = document.getElementById("Favorito").value
    else vFavorito = "0"

    var inputFiles = document.getElementById(oEntry.id + "__hAct")
    sValor = inputFiles.value
    inputFiles = document.getElementById(oEntry.id + "__hNew")
    sValor2 = inputFiles.value
    if (oEntry.id.search("fsdsentry") > 0) tipo = 3
    else tipo = 1;
    CerrarDesplegables();
    var newWindow = window.open("atachedfiles.aspx?Input=" + oEntry.id + "__hAct" + "&Valor=" + sValor + "&Valor2=" + sValor2 + "&Campo=" + oEntry.idCampo + "&tipo=" + tipo.toString() + "&Instancia=" + vInstancia.toString() + "&readOnly=" + (oEntry.readOnly ? "1" : "0") + "&Solicitud=" + vSolicitud.toString() + "&Favorito=" + vFavorito, "_blank", "width=750,height=330,status=yes,resizable=no,top=200,left=200")
    newWindow.focus();
};
/*
''' <summary>
''' Abre la ventana con los archivos adjuntos o si s�lo hay un archivo, abre dicho archivo directamente.
''' Si estas dando de alta una solicitud/ no conf... y se llama desde el enlace para abrir un fichero:
'''     -  de campo q viene desde el formulario hay q leer desde campo_adjun, eso lo indica el tipo a 1 
'''     -  de desglose q viene desde el formulario hay q leer desde linea_desglose_adjun, eso lo indica el tipo a 3 
'''     -  q has subido mientras das de alta hay q leer desde copia_adjun, eso lo indica el tipo a 2 
''' </summary>
''' <param name="oEntryId">el id del dataentry</param>     
''' <param name="idCampo">el id del campo</param>     
''' <param name="readOnly">si el campo es de solo lectura o no</param>     
''' <param name="button">si llama a esta funci�n desde el bot�n (1) o desde el enlace (0).</param>     
''' <remarks>Llamada desde: Al hacer click desde el bot�n o el enlace de un dataentry tipo Archivo; Tiempo m�ximo:0</remarks>
*/
function show_attached_files_button_hyperlink(oEntryId, idCampo, readOnly, button) {
    if (document.getElementById("Solicitud")) vSolicitud = document.getElementById("Solicitud").value
    else vSolicitud = ""

    if (document.getElementById("Instancia")) vInstancia = document.getElementById("Instancia").value
    else vInstancia = ""

    if (document.getElementById("Favorito")) vFavorito = document.getElementById("Favorito").value
    else vFavorito = "0"

    var inputFiles = document.getElementById(oEntryId + "__hAct")
    sValor = inputFiles.value
    inputFiles = document.getElementById(oEntryId + "__hNew")
    sValor2 = inputFiles.value

    if ((vInstancia == "") && (button == 0) && (sValor.search("xx") == -1) && (sValor2.search("xx") == -1)) {
        if (oEntryId.search("fsdsentry") > 0) tipo = 3;
        else tipo = 1;
    } else {
        if (oEntryId.search("fsdsentry") > 0) tipo = 3;
        else tipo = 1;
    }

    if ((button == 1) || (sValor.search("xx") > 0) || (sValor2.search("xx") > 0) || ((sValor != '') && (sValor2 != ''))) {
        CerrarDesplegables();
        var newWindow = window.open("atachedfiles.aspx?Input=" + oEntryId + "__hAct" + "&Valor=" + sValor + "&Valor2=" + sValor2 + "&Campo=" + idCampo + "&tipo=" + tipo.toString() + "&Instancia=" + vInstancia.toString() + "&readOnly=" + readOnly + "&Solicitud=" + vSolicitud.toString() + "&Favorito=" + vFavorito, "_blank", "width=750,height=330,status=yes,resizable=no,top=200,left=200")
        newWindow.focus();
    } else {
        if (sValor != '') valor = sValor;
        else {
            valor = sValor2;
            tipo = 2;
        }
        if ((tipo == 3) && (vFavorito == "1")) tipo = 2;
        if (valor != '') { //si no hay ficheros que no haga nada
            newWindow = window.open("../_common/attach.aspx?instancia=" + vInstancia + "&id=" + valor.toString() + "&tipo=" + tipo.toString(), "_blank", "width=600,height=275,status=no,resizable=yes,top=200,left=200");
            newWindow.focus();
        }
    }
};
var oWinDesglose;
/*
''' <summary>
''' Abre la ventana de desglose. El nombre del entry pasa de estar en el id a estar en el name
''' </summary>
''' <param name="oEntry">el dataentry padre del desglose</param>     
''' <returns>Nada</returns>
''' <remarks>Llamada desde:Desde todos los entry que son desglose popup; Tiempo m�ximo:0</remarks>
*/
function show_desglose(oEntry) {
    //var oWinDesglose;
    oWinDesglose = window.open("", "windesglose", "width=750,height=315,status=no,resizable=yes,top=200,left=200")
    oWinDesglose.focus();
    var vCalcular

    oEntry = oEntry.fsEntry
    oDiv = oEntry.Editor

    oFrm = document.forms["frmDesglose"]

    sInner = oDiv.innerHTML
    oDiv.innerHTML = ""

    sInner = replaceAll(sInner, ":", "_")

    oFrm.innerHTML = sInner

    if (oFrm.elements["nomDesglose"]) oFrm.elements["nomDesglose"].value = oEntry.name
    else oFrm.insertAdjacentHTML("beforeEnd", "<input id=nomDesglose type=hidden name =nomDesglose value=" + oEntry.name + ">")

    if (document.getElementById("Solicitud")) vSolicitud = document.getElementById("Solicitud").value
    else vSolicitud = ""

    //En QA se actualiza tras cada grabaci�n el hidden Version. Desglose.ascx usa los entrys de cuando se entra por primera vez
    //bien en DetalleCertificado bien en DetalleNoConformidad por lo q hay q mantener un hidden VersionInicialCargada para q 
    //desglose.ascx funcione correctamente.
    if (document.getElementById("VersionInicialCargada")) vVersionInicial = document.getElementById("VersionInicialCargada").value
    else {
        if (document.getElementById("Version")) vVersionInicial = document.getElementById("Version").value
        else vVersionInicial = ""
    }

    if (document.getElementById("Version")) vVersionBdOCombo = document.getElementById("Version").value
    else vVersionBdOCombo = ""

    if (document.getElementById("Proveedor")) vProveedor = document.getElementById("Proveedor").value
    else vProveedor = ""


    oInst = document.getElementById("Instancia")

    if (document.getElementById("NoConformidad")) vNoConformidad = document.getElementById("NoConformidad").value
    else vNoConformidad = ""

    if (document.getElementById("FuerzaDesgloseLectura")) vFuerzaDesgloseLectura = document.getElementById("FuerzaDesgloseLectura").value
    else vFuerzaDesgloseLectura = ""

    if (document.getElementById("BotonCalcular")) vCalcular = document.getElementById("BotonCalcular").value
    else vCalcular = 0

    if (document.getElementById("Favorito")) vFavorito = document.getElementById("Favorito").value
    else vFavorito = ""

    //COMPROBAR SI EL CAMPO ANTERIOR AL DESGLOSE ES DE TIPO ORGANIZACION DE COMPRAS
    //*****************************************************************************
    bCentroRelacionado = false;
    sCodOrgCompras = '';
    oFSEntryAnt = null;

    if (oEntry.orden > 1) {
        idEntryAnt = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna - 1, oEntry.nLinea, 0);
        if (idEntryAnt) {
            oFSEntryAnt = fsGeneralEntry_getById(idEntryAnt);

            if (oFSEntryAnt) if (oFSEntryAnt.tipoGS == 123) {
                bCentroRelacionado = true;
                sCodOrgCompras = oFSEntryAnt.getDataValue();
            }
        }
    }
    bObservador = 0;
    if (document.getElementById("Observadores")) if (document.getElementById("Observadores").value == "1") bObservador = 1;

    solicitudguardada = 0
    if (document.getElementById("NEW_Instancia")) if (document.getElementById("NEW_Instancia").value != "") solicitudguardada = 1

    if (oInst)
        oFrm.action = "desglose.aspx?Campo=" + oEntry.idCampo + "&Input=" + oEntry.id + "&Instancia=" + oInst.value + "&Version=" + vVersionInicial + "&Proveedor=" + vProveedor + "&SoloLectura=" + (oEntry.readOnly ? "1" : "0") + "&NoConformidad=" + vNoConformidad + "&CentroRelacionadoFORM=" + bCentroRelacionado + "&CodOrgComprasFORM=" + sCodOrgCompras + "&BotonCalcular=" + vCalcular + "&campo_origen=" + oEntry.campo_origen + "&Observador=" + bObservador + "&DesgloseLectura=" + vFuerzaDesgloseLectura + "&Favorito=" + vFavorito + "&VersionBd=" + vVersionBdOCombo + "&IdInstanciaImportar=" + $("#IdInstanciaImportar").val()
    else {
        oFrm.action = "desglose.aspx?Campo=" + oEntry.idCampo + "&Input=" + oEntry.id + "&SoloLectura=" + (oEntry.readOnly ? "1" : "0") + "&Solicitud=" + vSolicitud.toString() + "&CentroRelacionadoFORM=" + bCentroRelacionado + "&CodOrgComprasFORM=" + sCodOrgCompras + "&BotonCalcular=" + vCalcular + "&DesgloseLectura=" + vFuerzaDesgloseLectura + "&Solicitudguardada=" + solicitudguardada + "&Favorito=" + vFavorito + "&IdInstanciaImportar=" + $("#IdInstanciaImportar").val()
        var monedaInput = $.map(arrInputs, function (x) { if (fsGeneralEntry_getById(x) != null) { if (fsGeneralEntry_getById(x).tipoGS == 102 && !fsGeneralEntry_getById(x).basedesglose) return fsGeneralEntry_getById(x); } })[0];
        if ((monedaInput) && (monedaInput.dataValue != '') && (monedaInput.dataValue != null)) {
            oFrm.action = oFrm.action + "&MonedaSolicitud=" + monedaInput.dataValue
        }
    }
    oFrm.action = oFrm.action //para cuando entra desde GS

    CerrarDesplegables();
    oWinDesglose = window.open("", "windesglose", "width=750,height=315,status=no,resizable=yes,top=200,left=200")
    oWinDesglose.focus();
    oFrm.target = "windesglose"
    oFrm.submit()

    sInner = oFrm.innerHTML
    oFrm.innerHTML = ""
    oDiv.innerHTML = sInner
};
/*
''' <summary>
''' Abre la ventana del editor.
''' </summary>
''' <param name="oEntry">el dataentry del campo tipo editor</param>     
''' <returns>Nada</returns>
''' <remarks>Llamada desde:Desde todos los entry que son tipo editor; Tiempo m�ximo:0</remarks>
*/
function show_editor(titulo, id, readOnly) {
    var newWindow = window.open(rutaFS + "_common/Editor.aspx?titulo=" + escape(titulo) + "&ID=" + id + "&readOnly=" + readOnly, "Editor", "width=910,height=600,status=yes,resizable=yes,top=20,left=100,scrollbars=yes")
    newWindow.focus();
};

/*
''' <summary>
''' Abre la ventana del campo enlace.
''' </summary>
''' <param name="sUrl">url que se va a abrir en ventana nueva</param>     
''' <returns>Nada</returns>
''' <remarks>Llamada desde:Desde todos los entry que son tipo enlace; Tiempo m�ximo:0</remarks>
*/
function show_link(sUrl) {
    var newWindow = window.open(sUrl, "Enlace", "width=1300,height=600,status=yes,resizable=yes,top=20,left=100,scrollbars=yes")
    newWindow.focus();
};

/*
''' <summary>
''' Guarda el texto introducido en el editor y lo guarda en el dataentry
''' </summary>
''' <param name="IDControl">Id del dataentry tipo editor</param>     
''' <param name="texto">texto del editor</param>     
''' <returns>Nada</returns>
''' <remarks>Llamada desde:al pulsar el bot�n "guardar" del editor; Tiempo m�ximo:0</remarks>
*/
function save_editor_text(IDControl, texto) {
    o = fsGeneralEntry_getById(IDControl)
    o.setDataValue(texto)
};
/*
''' <summary>
''' Carga el proveedor recien seleccionado en BuscadorProvedores.aspx
''' </summary>
''' <param name="IDControl">html donde poner el codigo proveedor</param>
''' <param name="sProveCod">codigo proveedor</param>        
''' <param name="sProveDen">denom proveedor</param>
''' <param name="sContactos">Contactos proveedor</param>        
''' <param name="IDFPago">html donde poner el codigo forma pago</param>
''' <param name="IDFPagoHidden">html donde poner el codigo forma pago, es el id de proveedor pq la forma de pago esta oculta y se guardara en una proiedad del entry proveedor</param>
''' <param name="FPago">codigo forma pago</param>        
''' <param name="DenFPago">denom forma pago</param>
''' <remarks>Llamada desde: BuscadorProvedores.aspx ; Tiempo m�ximo: 0</remarks>*/
function prove_seleccionado(IDControl, sProveCod, sProveDen, sContactos, IDFPago, IDFPagoHidden, FPago, DenFPago) {
    for (i = 0; i < oGridsDesglose.length; i++) {
        if (!oGridsDesglose[i].IdEntryMaterial) continue
        sAux = oGridsDesglose[i].IdEntryMaterial
        re = /fsentry/
        sGridAux = sAux
        while (sGridAux.search(re) >= 0)
            sGridAux = sGridAux.replace(re, "")
        sGridAux = "_" + sGridAux + "_"

        if (IDControl.search(sAux) >= 0 || IDControl.search(sGridAux) > 0) {
            sAux = IDControl

            if (sAux.search("fsdsentry") >= 0) {
                sAux = sAux.substr(sAux.search("fsdsentry"), sAux.length)
                arrAux = sAux.split("_")
                lIndex = arrAux[1]
            } else {
                sAux = sAux.substr(sAux.search("fsentry"), sAux.length)
                lIndex = "-1"
            }

            break;
        }
    };
    o = fsGeneralEntry_getById(IDControl)
    o.setValue(sProveCod + ' - ' + sProveDen)
    o.setDataValue(sProveCod)
    o.Contactos = sContactos

    if ((IDFPago != '') && (IDFPago != null)) {
        o = fsGeneralEntry_getById(IDFPago)
        if (FPago == null) {
            o.setValue('')
            o.setDataValue('')
        } else {
            o.setValue(FPago + ' - ' + DenFPago)
            o.setDataValue(FPago)
        }
    }
    if ((IDFPagoHidden != '') && (IDFPagoHidden != null)) {
        o = fsGeneralEntry_getById(IDFPagoHidden);
        o.FPagoOculta = FPago;
    }

    BorrarProveedoresERP(o);
};
function prove_seleccionado3(IDControl, sProveCod, sProveDen, lConID, sConEmail) {

    //busca en oGridsDesglose la del participante
    for (i = 0; i < oGridsDesglose.length; i++) {
        if (!oGridsDesglose[i].IdEntryMaterial) continue
        sAux = oGridsDesglose[i].IdEntryMaterial
        re = /fsentry/
        sGridAux = sAux
        while (sGridAux.search(re) >= 0)
            sGridAux = sGridAux.replace(re, "")
        sGridAux = "_" + sGridAux + "_"

        if (IDControl.search(sAux) >= 0 || IDControl.search(sGridAux) > 0) {
            sAux = IDControl

            if (sAux.search("fsdsentry") >= 0) {
                sAux = sAux.substr(sAux.search("fsdsentry"), sAux.length)
                arrAux = sAux.split("_")
                lIndex = arrAux[1]
            } else {
                sAux = sAux.substr(sAux.search("fsentry"), sAux.length)
                lIndex = "-1"
            }

            break;
        }
    };
    o = fsGeneralEntry_getById(IDControl)
    o.setValue(sProveDen + ' (' + sConEmail + ')')
    o.setDataValue(sProveCod + '###' + lConID);
};
function usu_seleccionado(IDControl, sUsuCod, sUsuNom, sUsuEmail) {
    for (i = 0; i < oGridsDesglose.length; i++) {
        if (!oGridsDesglose[i].IdEntryMaterial) continue
        sAux = oGridsDesglose[i].IdEntryMaterial
        re = /fsentry/
        sGridAux = sAux
        while (sGridAux.search(re) >= 0)
            sGridAux = sGridAux.replace(re, "")
        sGridAux = "_" + sGridAux + "_"

        if (IDControl.search(sAux) >= 0 || IDControl.search(sGridAux) > 0) {
            sAux = IDControl

            if (sAux.search("fsdsentry") >= 0) {
                sAux = sAux.substr(sAux.search("fsdsentry"), sAux.length)
                arrAux = sAux.split("_")
                lIndex = arrAux[1]
            } else {
                sAux = sAux.substr(sAux.search("fsentry"), sAux.length)
                lIndex = "-1"
            }

            break;
        }
    };
    o = fsGeneralEntry_getById(IDControl)
    o.setValue(sUsuNom + ' (' + sUsuEmail + ')')
    o.setDataValue(sUsuCod)
};
function limpiarArt() { };
/*''' <summary>
''' Rellenar un entry con el material seleccionado
''' </summary>
''' <param name="IDControl">Id del entry donde va el material seleccionado</param>
''' <param name="sGMN1">Gmn1 de material seleccionado</param>        
''' <param name="sGMN2">Gmn2 de material seleccionado</param>
''' <param name="sGMN3">Gmn3 de material seleccionado</param>  
''' <param name="sGMN4">Gmn4 de material seleccionado</param>
''' <param name="iNivel">Nivel de material seleccionado</param>  
''' <param name="sDen">Denominaci�n del material</param>
''' <param name="desde">Vacio si viene de una pantalla q no sea Materiales.aspx eoc 'Materiales'</param>  
''' <remarks>Llamada desde: articulosserver.aspx/aceptar()  materiales.aspx/seleccionarMaterial()        
'''         validararticulo.aspx/articuloValidado(); Tiempo m�ximo:0</remarks>*/
function mat_seleccionado(IDControl, sGMN1, sGMN2, sGMN3, sGMN4, iNivel, sDen, desde, TodosNiveles) {
    var sCodArt = null;
    var idEntryCod = null;
    //Si el texto del nodo era, por ejemplo, C'a"ta >600mm. El oNode.getText() devuelve C'a"ta &gt;600mm. Lo q ha
    //de verse es > no &gt;
    sDen = replaceAll(sDen, "&gt;", ">")
    sDen = replaceAll(sDen, "&lt;", "<")
    var sDenAux = sDen
    if (IDControl == "") return
    for (i = 0; i < oGridsDesglose.length; i++) {
        if (!oGridsDesglose[i].IdEntryMaterial) continue
        sAux = oGridsDesglose[i].IdEntryMaterial
        re = /fsentry/
        sGridAux = sAux
        while (sGridAux.search(re) >= 0)
            sGridAux = sGridAux.replace(re, "")
        sGridAux = "_" + sGridAux + "_"
        if (IDControl.search(sAux) > 0 || IDControl.search(sGridAux) > 0) {
            sAux = IDControl

            if (sAux.search("fsdsentry") >= 0) {
                sAux = sAux.substr(sAux.search("fsdsentry"), sAux.length)
                arrAux = sAux.split("_")
                lIndex = arrAux[1]
            } else {
                sAux = sAux.substr(sAux.search("fsentry"), sAux.length)
                lIndex = "-1"
            }

            break;
        }
    }

    o = fsGeneralEntry_getById(IDControl)
    s = ""
    for (i = 0; i < ilGMN1 - sGMN1.length; i++)
        s += " "

    sMat = s + sGMN1

    if (TodosNiveles == 1) {
        sDenShort = sGMN1 + ' - '
    } else {
        sDenShort = sGMN1 + ' - ' + sDen
    }

    sDenAux += ' (' + sGMN1
    if (sGMN2) {
        s = ""
        for (i = 0; i < ilGMN2 - sGMN2.length; i++)
            s += " "
        sMat = sMat + s + sGMN2
        sDenAux += ' - ' + sGMN2
        if (TodosNiveles == 1) {
            sDenShort += sGMN2 + ' - '
        } else {
            sDenShort = sGMN2 + ' - ' + sDen
        }
    } 

    if (sGMN3) {
        s = ""
        for (i = 0; i < ilGMN3 - sGMN3.length; i++)
            s += " "
        sMat = sMat + s + sGMN3
        sDenAux += ' - ' + sGMN3
        if (TodosNiveles == 1) {
            sDenShort += sGMN3 + ' - '
        } else {
            sDenShort = sGMN3 + ' - ' + sDen
        }
    }
    if (sGMN4) {
        s = ""
        for (i = 0; i < ilGMN4 - sGMN4.length; i++)
            s += " "
        sMat = sMat + s + sGMN4
        sDenAux += ' - ' + sGMN4
        if (TodosNiveles == 1) {
            sDenShort += sGMN4 + ' - '
        } else {
            sDenShort = sGMN4 + ' - ' + sDen
        }

    }
    sDenAux += ')'

    if (TodosNiveles == 1) {
        sDenShort += sDen
    }

    if (desde == 'Materiales') if (o.getDataValue() != '') //anteriormente no hab�a material

        if (o.getDataValue() != sMat) {
            //si el campo material se encuentra en el desglose, confirmar� la eliminaci�n de los art�culos introducidos    
            if (!o.isDesglose && existeLineaDesgloseArticulos()) {
                var respuesta = confirm(sMensajeEliminArtMat);
                if (respuesta) {
                    Borra_Los_Articulos();
                } else {
                    return false;
                }
            }
            //Poner el NuevoCodArt en blanco
            var idEntryCod = calcularIdDataEntryCelda(o.id, o.nColumna + 1, o.nLinea, 0)
            if (idEntryCod) var oEntry = fsGeneralEntry_getById(idEntryCod)
            if ((oEntry != null) && (oEntry.tipoGS == 119)) {
                if (!oEntry.anyadir_art) {
                    sCodArt = oEntry.getDataValue()
                    oEntry.setValue("")
                    oEntry.title = ""

                    //Poner la Denominacion en Blanco
                    oEntry = null;
                    var idEntryDen = calcularIdDataEntryCelda(o.id, o.nColumna + 2, o.nLinea, 0)
                    if (idEntryDen) oEntry = fsGeneralEntry_getById(idEntryDen)
                    if ((oEntry != null) && (oEntry.tipoGS == 118)) {
                        if ((sCodArt != "") || (oEntry.getDataValue() == "")) {
                            oEntry.setValue("")
                            oEntry.title = ""
                        }
                    }
                }
            }

            //Mirar si tiene alg�n campo de lista como hijo y ponerlo en blanco 
            VaciarCampoListaCampoMaterial(o);
        };
    o.setDataValue(sMat)
    o.setValue(sDenShort)
    o.setToolTip(sDenAux)
    ComprobarComprador(sMat, "Mat");
};
function mostrarMensaje(men) {
    alert(men);
};
function pres_seleccionado(IDControl, sValor, sTexto, CabeceraDesglose, DesgloseAsociado, OrgCompras) {
    //DesgloseAsociado numero que me sirve para actualizar todos los dataentrys de presupuestos asociados
    //cuando es una cabecera la que abre la ventana de presupuestos
    var pertenece = -1
    var arrPres = new Array()

    if (CabeceraDesglose != 0) {
        //PRES1 = 110
        //Pres2 = 111
        //Pres3 = 112
        //Pres4 = 113			
        switch (CabeceraDesglose) {
            case '110':
                arrPres = arrPres1;
                break;
            case '111':
                arrPres = arrPres2;
                break;
            case '112':
                arrPres = arrPres3;
                break;
            case '113':
                arrPres = arrPres4;
                break;
        };
        for (i = 0; i < arrPres.length; i++) {
            pertenece = -1
            o = fsGeneralEntry_getById(arrPres[i])
            if (o != null) {
                pertenece = arrPres[i].indexOf(DesgloseAsociado)
                if (pertenece != -1) {
                    var desglose = o.id.substr(0, o.id.search("_fsdsentry"))
                    var index = o.id.substr(o.id.search("_fsdsentry_") + 11, o.id.search(o.idCampo) - o.id.search("_fsdsentry_") - 12)

                    if (index > 0) {
                        var iOrgCompras = null

                        for (j = 0; j < document.getElementById(desglose + "_tblDesgloseHidden").rows[0].cells.length; j++) {
                            s = document.getElementById(desglose + "_tblDesgloseHidden").rows[0].cells[j].innerHTML
                            re = /fsentry/

                            x = s.search(re)
                            y = s.search("__tbl")

                            id = s.substr(x + 7, y - x - 7)
                            oEntry = fsGeneralEntry_getById(desglose + "_fsentry" + id)

                            if (oEntry) {
                                if (oEntry.tipoGS == 123) {
                                    iOrgCompras = oEntry.id
                                }
                            }
                        }
                        if ((iOrgCompras != null) && (OrgCompras != "")) {
                            oDSEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + fsGeneralEntry_getById(iOrgCompras).idCampo)
                            iOrgCompras = oDSEntry.getDataValue()

                            if (iOrgCompras == OrgCompras) {
                                o.setDataValue(sValor)
                                o.setValue(sTexto)
                            }
                        } else {
                            o.setDataValue(sValor)
                            o.setValue(sTexto)
                        }
                    }
                }
            }
        }
    } else {
        o = fsGeneralEntry_getById(IDControl)
        if (o != null) {
            if (o.getValue(0) != sTexto) {
                if (document.getElementById(IDControl + "__t_t").className == "StringTachado") document.getElementById(IDControl + "__t_t").className = "TipoTextoMedio"
            }
            o.setDataValue(sValor)
            o.setValue(sTexto)
        }
    }
};
function CentroCoste_seleccionado(IDControl, sUON1, sUON2, sUON3, sUON4, sDen) {
    var sValor = '';

    //lo que se guarda en valor_text
    if (sUON1) sValor = sUON1;
    if (sUON2) sValor = sValor + '#' + sUON2;
    if (sUON3) sValor = sValor + '#' + sUON3;
    if (sUON4) sValor = sValor + '#' + sUON4;

    if (IDControl == "") return

    o = fsGeneralEntry_getById(IDControl)
    if (o) {
        o.Editor.elem.className = "TipoTextoMedio"
        o.setDataValue(sValor)
        o.setValue(sDen)
        borrarPartidaoActivoRelacionado(o)
    }
};
function Partida_seleccionada(IDControl, sUON1, sUON2, sUON3, sUON4, sContrato, sDen, IDCentroCoste, sDenCC) {
    var sValor = '';

    //lo que se guarda en valor_text
    if (sUON1) sValor = sUON1;
    if (sUON2) sValor = sValor + '#' + sUON2;
    if (sUON3) sValor = sValor + '#' + sUON3;
    if (sUON4) sValor = sValor + '#' + sUON4;
    if (sContrato) sValor = sValor + '#' + sContrato;

    if (IDControl == "") return;

    o = fsGeneralEntry_getById(IDControl);
    if (o) {
        o.Editor.elem.className = "TipoTextoMedio";
        o.setDataValue(sValor);
        o.setValue(sDen);
        borrarAnyoPartidaRelacionado(o)
    }

    if (IDCentroCoste != "") {
        o = fsGeneralEntry_getById(IDCentroCoste);
        if (o) {
            var separador = sValor.lastIndexOf('#');
            if (separador > 0) var sValor = sValor.substr(0, separador);

            o.Editor.elem.className = "TipoTextoMedio";
            o.setDataValue(sValor);
            o.setValue(sDenCC);
            borrarArticuloRelacionado(o);
        }
    }
};
function Activo_seleccionado(IDControl, sDataValue, sValue, IDCentroCoste, ValorCC, sUON) {
    if (IDControl == "") return;

    o = fsGeneralEntry_getById(IDControl);
    o.Editor.elem.className = "TipoTextoMedio"
    o.setDataValue(sDataValue);
    o.setValue(sValue);

    if (IDCentroCoste != "") {
        o = fsGeneralEntry_getById(IDCentroCoste)
        if (o) {
            var newWindow = window.open(rutaPM + "_common/BuscadorCentroCosteServer.aspx?idCC=" + (o.VerUON ? sUON : ValorCC) + "&fsEntryCC=" + o.id + "&VerUON=" + (o.VerUON ? "1" : "0") + "&sinColor=1", "iframeWSServer")
            newWindow.focus();
        }
    }
    return;
};
// Revisado por: blp. Fecha: 05/10/2011
//''' <summary>
//''' Establece en pantalla la uon seleccionada
//''' </summary>
//''' <param name="IDControl">Control UOn donde establecer lo seleccionado</param>
//''' <param name="sUON1">UON nivel 1</param>
//''' <param name="sUON2">UON nivel 2</param>
//''' <param name="sUON3">UON nivel 3</param>
//''' <param name="iNivel">nivel</param>
//''' <param name="sDen">Denominacion de UON</param>
//''' <remarks>Llamada desde: script\_common\UnidadesOrganizativas.aspx; Tiempo m�ximo:0,2</remarks>
function UON_seleccionado(IDControl, sUON1, sUON2, sUON3, iNivel, sDen) {
    var sDenAux = '';
    if (sUON1) {
        sDenAux = sDenAux + sUON1;
        if (sUON2) sDenAux = sDenAux + ' - ' + sUON2;
        if (sUON3) sDenAux = sDenAux + ' - ' + sUON3;
        if (sDen) sDenAux = sDenAux + ' - ' + sDen;
    } else if (sDen) sDenAux = sDen;

    if (IDControl == "") return

    if (document.getElementById(IDControl + "__t_t").className == "StringTachado") document.getElementById(IDControl + "__t_t").className = "TipoTextoMedio"

    for (i = 0; i < oGridsDesglose.length; i++) {
        if (!oGridsDesglose[i].IdEntryMaterial) continue
        sAux = oGridsDesglose[i].IdEntryMaterial
        re = /fsentry/
        sGridAux = sAux
        while (sGridAux.search(re) >= 0)
            sGridAux = sGridAux.replace(re, "")
        sGridAux = "_" + sGridAux + "_"
        if (IDControl.search(sAux) > 0 || IDControl.search(sGridAux) > 0) {
            sAux = IDControl

            if (sAux.search("fsdsentry") >= 0) {
                sAux = sAux.substr(sAux.search("fsdsentry"), sAux.length)
                arrAux = sAux.split("_")
                lIndex = arrAux[1]
            } else {
                sAux = sAux.substr(sAux.search("fsentry"), sAux.length)
                lIndex = "-1"
            }
            break;
        }
    };
    var o = fsGeneralEntry_getById(IDControl);
    o.setDataValue(sDenAux);
    o.setValue(sDenAux);
    if (o.dependentfield) {
        //DEPARTAMENTO
        var oEntrySig = fsGeneralEntry_getById(o.dependentfield);
        if ((oEntrySig != null) && (oEntrySig.tipoGS == 122)) {
            //Vaciamos de contenido el control dependiente
            vaciarDropDown(o.dependentfield);
        }
        oEntrySig = null;
    }
    if (o.idDataEntryDependent) {
        var oEntryArticulo = fsGeneralEntry_getById(o.idDataEntryDependent)
        if (oEntryArticulo != null) {
            oEntryArticulo.setValue("");
            oEntryArticulo.setDataValue("");
            if (oEntryArticulo.tipoGS == 119) {
                oEntryArticulo.UnidadOrganizativaDependent.value = sDenAux;
                //Borrar la Denominacion
                var idDataEntryDen = calcularIdDataEntryCelda(oEntryArticulo.id, oEntryArticulo.nColumna + 1, oEntryArticulo.nLinea, 0)
                if (idDataEntryDen) {
                    var oEntryDen = fsGeneralEntry_getById(idDataEntryDen)
                    if ((oEntryDen) && (oEntryDen.tipoGS == 118)) {
                        oEntryDen.setValue("");
                        oEntryDen.setDataValue("");
                        oEntryDen.UnidadOrganizativaDependent.value = sDenAux
                    };
                };
            } else {
                if (oEntryArticulo.tipoGS == 118) {
                    oEntryArticulo.codigoArticulo = "";
                    oEntryArticulo.UnidadOrganizativaDependent.value = sDenAux;
                    var idDataEntryCod = calcularIdDataEntryCelda(oEntryArticulo.id, oEntryArticulo.nColumna - 1, oEntryArticulo.nLinea, 0)
                    if (idDataEntryCod) {
                        var oEntryCod = fsGeneralEntry_getById(idDataEntryCod)
                        if ((oEntryCod) && (oEntryCod.tipoGS == 119)) {
                            oEntryCod.setValue("");
                            oEntryCod.setDataValue("");
                            oEntryCod.UnidadOrganizativaDependent.value = sDenAux;
                        };
                    };
                };
            };
        };
    };
    o = null;
    var lastIndex = sDenAux.lastIndexOf("-");
    var sUON = sDenAux.substring(0, lastIndex);
    ComprobarComprador(sUON, "UON");
};
//''' <summary>Borra del desglose la fila seleccionada
//''' </summary>
//''' <param name="desglose">id del desglose</param>
//''' <param name="index">UON nivel 1</param>
//''' <param name="idCampo">UON nivel 2</param>
//''' <remarks>Llamada desde: script\alta\desglose.ascx; Tiempo m�ximo:0,1</remarks>
function deleteRow(e, desglose, index, idCampo) {
    var filaOld;
    var campo;
    var bAlta = false;
    p = window.opener;
    if (p) {
        if (p.document.forms["frmAlta"] != undefined) bAlta = true;
    } else {
        if (document.forms["frmAlta"] != undefined) bAlta = true;
    }

    if (!bAlta) {
        //Hay que comprobar que no est� asociada a un �tem o l�nea de pedido
        var result = ComprobarLineasAsociadas(idCampo, index);
        if (respuesta != "") {
            mostrarMensaje(respuesta);
            return;
        }
    }
    for (var indice in htControlFilas) {
        filaOld = indice.substr(indice.lastIndexOf("_") + 1)
        campo = indice.substring(0, indice.indexOf("_"))
        if ((parseInt(filaOld) > index) && (campo == idCampo) && (htControlFilas[indice]) != 0) {
            htControlFilas[indice] = String(parseInt(htControlFilas[indice]) - 1)
        }

        if ((parseInt(filaOld) == index) && (campo == idCampo)) {
            htControlFilas[indice] = "0"
        }
    };
    for (arrInput in arrInputs) {
        if (arrInputs[arrInput].search(desglose + "_fsdsentry_" + index.toString() + "_") >= 0) {
            if (fsGeneralEntry_getById(arrInputs[arrInput]).tipoGS == 124) {
                if (fsGeneralEntry_getById(arrInputs[arrInput]).idDataEntryDependent2 != null) {
                    oOrgCompras = fsGeneralEntry_getById(fsGeneralEntry_getById(arrInputs[arrInput]).idDataEntryDependent2)
                    if (oOrgCompras.id.search("fsentry") > -1) // Org fuera del desglose
                    {
                        for (var indicefilas in htControlFilas) {
                            if (htControlFilas[indicefilas] != 0) {
                                if (oOrgCompras.idDataEntryDependent2.search("fsdsentry") >= 0) {
                                    sNew = oOrgCompras.idDataEntryDependent2.substr(0, oOrgCompras.idDataEntryDependent2.search("fsdsentry"))
                                    sNew2 = oOrgCompras.idDataEntryDependent2.substr(oOrgCompras.idDataEntryDependent2.search("fsdsentry"), oOrgCompras.idDataEntryDependent2.length)
                                    arrAux = sNew2.split("_")

                                    sCadenaFinal = sNew + arrAux[0] + "_" + htControlFilas[indicefilas] + "_" + arrAux[2]
                                    oOrgCompras.idDataEntryDependent2 = sCadenaFinal
                                    haylinea = 1
                                    break;
                                }

                            }
                        }

                        if (haylinea == 0) {
                            oOrgCompras.idDataEntryDependent2 = null
                        }
                    }
                }
            }

            arrInputs[arrInput] = "deleted"
        }

    };
    for (i = 0; i < arrCalculados.length; i++) {
        if (arrCalculados[i].search(desglose + "_fsdsentry_" + index.toString() + "_") >= 0) {
            arrCalculados[i] = "deleted"
        }
    }
    for (i = 0; i < arrObligatorios.length; i++) {
        if (arrObligatorios[i].search(desglose + "_fsdsentry_" + index.toString() + "_") >= 0) {
            arrObligatorios[i] = "deleted"
        }
    }

    var oTbl = e.parentNode.parentNode

    var oRow = e.parentNode

    oTbl.removeChild(oRow)

    if (document.getElementById(desglose + "_numRowsGrid")) {
        document.getElementById(desglose + "_numRowsGrid").value = parseFloat(document.getElementById(desglose + "_numRowsGrid").value) - 1;
    }

    //S�lo reenumero las lineas si estoy en el alta, si no mantengo los n�meros de l�nea
    if (bAlta) {
        //Tarea 3369: El Num.Linea q sea el real.
        var Fila = 1;
        var ultimaFila = 0;
        for (var indice in htControlFilas) {
            if (parseInt(htControlFilas[indice]) != "0") {
                for (arrInput in arrInputs) {
                    if (arrInputs[arrInput].search(desglose + "_fsdsentry_" + Fila.toString() + "_") >= 0) {
                        if (fsGeneralEntry_getById(arrInputs[arrInput]).tipoGS == 3000) {
                            fsGeneralEntry_getById(arrInputs[arrInput]).setValue(htControlFilas[indice]);
                        }
                    }
                }
            }
            Fila = Fila + 1;
        };
        //Tarea 3369
    }
};
function ComprobarLineasAsociadas(campoDesglose, linea) {
    params = { idDesglose: campoDesglose, numLinea: linea }
    $.when($.ajax({
        type: "POST",
        url: rutaPM + 'ConsultasPMWEB.asmx/Comprobar_Lineas',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        respuesta = msg.d;
    });
}
//  Revisado por: blp. Fecha: 06/10/2011
//jbg 310309	
//Descripci�n:	Comprueba los obligatorios antes de una grabaci�n
//		160309 Si un desglose no popup es obligatorio tiene q meter al menos una linea
//Param. Entrada: -
//Param. Salida: -
//LLamada:	alta/alta.aspx	alta/nwalta.aspx	certificados/altacertificados.aspx
//			noconformidad/altanoconformidad.aspx	noconformidad/detallenoconformidad.aspx
//			noconformidad/noconformidadrevisor.aspx		seguimiento/detallesolicitud.aspx
//			seguimiento/nwdetallesolicitud.aspx		workflow/gestioninstancia.aspx
//			workflow/nwgestioninstancia.aspx
//Tiempo:	0
function comprobarObligatorios() {
    if (typeof (arrObligatorios) != "undefined") {
        for (var i = 0; i < arrObligatorios.length; i++) {
            if (arrObligatorios[i] != "deleted") {
                oEntry = fsGeneralEntry_getById(arrObligatorios[i])
                if (oEntry) {
                    if ((oEntry.tipoGS != 115) && (oEntry.tipoGS != 116)) { //si es campo participante proveedor o persona no se mira. Ya se mirar� en la funci�n ComprobarParticipantes. 
                        //si est� en rojo es que no est� validado y se borra
                        if (oEntry.Editor != null) {
                            if (oEntry.Editor.elem != null) {
                                if (oEntry.Editor.elem.className == "TipoTextoMedioRojo") {
                                    oEntry.setDataValue('');
                                    oEntry.setValue('');
                                    oEntry.Editor.elem.className = "TipoTextoMedio";
                                }
                            }
                        }

                        if (oEntry.basedesglose == false) {
                            if (oEntry.tipo == 9 || oEntry.tipo == 13 || oEntry.tipo == 14) {
                                if (oEntry.tipo == 13) {
                                    sDesglose = replaceAll(arrObligatorios[i], "OBLIG_", "")
                                    //uwtGrupos__ctl0x_922_922_9339_numRowsGrid
                                    numRows = document.getElementById(sDesglose + "_numRowsGrid").value
                                    if (numRows == 0) return 'filas0'
                                } else {
                                    if (oEntry.tipoGS == 136) {
                                        if (sNumLineas == 0) return 'filas0'
                                    } else {
                                        if (oEntry.tipo == 14) {
                                            sDesglose = replaceAll(arrObligatorios[i], "_OBLDESG_", "")
                                            //uwtGrupos__ctl0x_922_922_9339_numRowsGrid
                                            numRows = document.getElementById(sDesglose + "_numRowsGrid").value
                                            if (numRows == 0) return 'filas0'

                                        } else {
                                            //uwtGrupos__ctl0x_922_fsentry9339__numRows
                                            numRows = document.getElementById(arrObligatorios[i] + "__numRows").value
                                            if (numRows == 0) return 'filas0'
                                        }
                                    }
                                }
                            } else {
                                if (oEntry.tipoGS == 100 || oEntry.tipoGS == 110 || oEntry.tipoGS == 111 || oEntry.tipoGS == 112 || oEntry.tipoGS == 113 || oEntry.tipoGS == 132 || oEntry.tipoGS == 149) v = oEntry.getDataValue()
                                else {
                                    if ((oEntry.tipoGS == 8) || (oEntry.tipo == 8)) {
                                        //v = oEntry.Editor.outerText
                                        var inputFiles;
                                        inputFiles = document.getElementById(oEntry.id + "__hNew")
                                        if (inputFiles.value != "") v = inputFiles.value
                                        else {
                                            inputFiles = document.getElementById(oEntry.id + "__hAct")
                                            v = inputFiles.value
                                        }
                                    } else {
                                        if ((oEntry.intro == 1) || (oEntry.tipo == 15)) v = oEntry.getDataValue()
                                        else {
                                            if (oEntry.intro == 2) v = oEntry.getValue()
                                            else {
                                                if (oEntry.tipoGS == 0 || oEntry.tipoGS == 1 || oEntry.tipoGS == 2 || oEntry.tipoGS == 126 || oEntry.tipoGS == 133 || oEntry.tipoGS == 134 || oEntry.tipoGS == 20 || oEntry.tipoGS == 23 || oEntry.tipoGS == 24 || oEntry.tipoGS == 25 || oEntry.tipoGS == 26 || oEntry.tipoGS == 30 || oEntry.tipoGS == 31 || oEntry.tipoGS == 32 || oEntry.tipoGS == 35 || oEntry.tipoGS == 38 || oEntry.tipoGS == 39 || oEntry.tipoGS == 1020) {
                                                    switch (oEntry.tipo) {
                                                        case 6:
                                                            //tipo Texto medio y Texto Largo
                                                        case 7:
                                                            v = oEntry.getDataValue()
                                                            break;
                                                        default:
                                                            v = oEntry.Editor.elem.value
                                                            break;
                                                    }
                                                } else {
                                                    if (oEntry.Editor.elem != null) {
                                                        v = oEntry.Editor.elem.value
                                                    } else { //Al menos en el caso: tipoGS=120 (NumSolicitERP) tipo=6 el fsGeneralEntry_Setup no mete Editor.elem
                                                        v = oEntry.Editor.value
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (v == null || v == "" || v == "null") if (oEntry.tipoGS == 104) {
                                    v = oEntry.EditorDen.elem.value
                                    if (v == null || v == "" || v == "null") if (oEntry.nLinea == 0) return oEntry.title.toString() + ' (' + oEntry.DenGrupo.toString() + ')'
                                    else return sFila + ' ' + oEntry.nLinea.toString() + ': ' + oEntry.title.toString() + ' (' + oEntry.DenDesgloseMat.toString() + ' - ' + oEntry.DenGrupo.toString() + ')'
                                } else if (oEntry.nLinea == 0) return oEntry.title.toString() + '(' + oEntry.DenGrupo.toString() + ')'
                                else return sFila + ' ' + oEntry.nLinea.toString() + ': ' + oEntry.title.toString() + ' (' + oEntry.DenDesgloseMat.toString() + ' - ' + oEntry.DenGrupo.toString() + ')'
                            }
                        }
                    }
                } else {
                    sDesglose = replaceAll(arrObligatorios[i], "OBLIG_", "")
                    if (document.getElementById(sDesglose + "_numRowsGrid")) {
                        //uwtGrupos__ctl0x_922_922_9339_numRowsGrid
                        numRows = document.getElementById(sDesglose + "_numRowsGrid").value
                        if (numRows == 0) return 'filas0'
                    } else {

                        sDesglose = replaceAll(arrObligatorios[i], "_OBLDESG_", "")
                        if (document.getElementById(sDesglose + "_numRowsGrid")) {
                            //uwtGrupos__ctl0x_922_922_9339_numRowsGrid
                            numRows = document.getElementById(sDesglose + "_numRowsGrid").value
                            if (numRows == 0) return 'filas0'
                        } else {
                            sDesglose = arrObligatorios[i]
                            sDesglose = sDesglose.split("__")
                            idCampo = sDesglose[sDesglose.length - 1]
                            sDesglose = replaceAll(arrObligatorios[i], "__" + idCampo, "")
                            var oEntryDesg = fsGeneralEntry_getById(sDesglose)
                            iNumRows = document.getElementById(sDesglose + "__numRows").value
                            iNumTotRows = document.getElementById(sDesglose + "__numTotRows").value
                            for (k = 1; k <= iNumTotRows; k++) {
                                if (document.getElementById(sDesglose + "__" + k.toString() + "__" + idCampo)) {
                                    v = document.getElementById(sDesglose + "__" + k.toString() + "__" + idCampo).value
                                    if (v == null || v == "" || v == "null") {
                                        if (document.getElementById(sDesglose + "__" + k.toString() + "__" + idCampo + "_hNew")) {
                                            v = document.getElementById(sDesglose + "__" + k.toString() + "__" + idCampo + "_hNew").value
                                            if (v == null || v == "" || v == "null") {
                                                return sFila + ' ' + k.toString() + ': ' + arrObligatoriosDen[i].toString() + ' (' + oEntryDesg.title.toString() + ' - ' + oEntryDesg.DenGrupo.toString() + ')'
                                            }
                                        } else return sFila + ' ' + k.toString() + ': ' + arrObligatoriosDen[i].toString() + ' (' + oEntryDesg.title.toString() + ' - ' + oEntryDesg.DenGrupo.toString() + ')'
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    };
    return "";
};
/*
''' <summary>
''' Comprueba si se han rellenado los campos de participantes
'''	''' </summary>
''' <returns>true o false dependiendo si estan rellenos</returns>
''' <remarks>Llamada desde:NWAlta,NwDetalleSolicitud,Nwgestioninstancia ; Tiempo m�ximo:0 </remarks>
*/
function comprobarParticipantes() {
    if (typeof (arrObligatorios) != "undefined") {
        for (var i = 0; i < arrObligatorios.length; i++) {
            if (arrObligatorios[i] != "deleted") {
                oEntry = fsGeneralEntry_getById(arrObligatorios[i])
                if (oEntry) {
                    if (oEntry.tipoGS == 115 || oEntry.tipoGS == 116) {
                        v = oEntry.getDataValue();
                        if (v == null || v == "") return oEntry.title;
                    }
                }
            }
        }
    }
    return '';
};
function ddPConPreInitDropDown(param) {
    //fsdsentry_1_1792###uwtGrupos__ctl0__ctl0###uwtGrupos__ctl0__ctl0_fsdsentry_1_1793
    var oGrid = new Object()
    var arrParam = param.split("###")
    oGrid.ID = arrParam[1]
    oGrid.IdEntryMaterial = arrParam[0] //esto es el id del campo rol
    oGrid.Editor = arrParam[2] //esto es el fsentry  del participante

    oGridsDesglose[oGridsDesglose.length] = oGrid

};
/*''' <summary>
''' Abre la ventana del buscador de proveedores para un dataentry ProveContacto
''' </summary>
''' <param name="oEdit">Dataentry</param>
''' <param name="text">texto a buscar como parte de la denominaci�n del ProveContacto</param>        
''' <param name="oEvent">Evento click</param>        
''' <remarks>Llamada desde: javascript introducido para responder al onclick de todos los campos de tipo ProveContacto ; Tiempo m�ximo: 0</remarks>*/
function ddPConopenDropDownEvent(oEdit, text, oEvent) {
    moEdit = oEdit
    motext = text
    moEvent = oEvent

    for (i = 0; i < oGridsDesglose.length; i++) {
        if (oGridsDesglose[i].Editor + "__t" == oEdit.ID) vGrid = oGridsDesglose[i]
    }
    sGridName = vGrid.ID

    sGridName += '_tblDesglose'

    sAux = oEdit.ID
    if (sAux.search("fsdsentry") >= 0) {
        sInit = sAux.substr(0, sAux.search("fsdsentry"))
        sAux = sAux.substr(sAux.search("fsdsentry"), sAux.length)
        arrAux = sAux.split("_")
        lIndex = arrAux[1]
    } else {
        sInit = sAux.substr(0, sAux.search("fsentry"))
        lIndex = "-1"
    }
    var oGrid = document.getElementById(sGridName)
    o = fsGeneralEntry_getById(sInit + vGrid.IdEntryMaterial)
    sRol = o.getDataValue()
    oEntry = fsGeneralEntry_getById(vGrid.Editor)
    if (document.getElementById("Instancia")) vInstancia = document.getElementById("Instancia").value
    else vInstancia = "";

    CerrarDesplegables();

    var newWindow = window.open(rutaFS + "_common/BuscadorProveedores.aspx?PM=true&ComboContactos=true&Rol=" + sRol + "&Instancia=" + vInstancia.toString() + "&IDControl=" + oEntry.id, "_blank", "width=835,height=635,status=yes,resizable=yes,top=0,left=150,scrollbars=yes")
    newWindow.focus();
};
/*
''' <summary>
''' A�ade un proveedor nuevo en alta certificados.
''' </summary>
''' <param name="sRoot">nombre entry del desglose</param>
''' <param name="idCampo">id de bbdd del desglose</param>
''' <param name="ProveCod">Cod de bbdd del proveedor</param>     
''' <param name="ProveDen">Descrip de bbdd del proveedor</param>
''' <param name="lIdContacto">id de bbdd del contacto del proveedor</param>     
''' <param name="sDenContacto">Descrip de bbdd del contacto del proveedor</param>     
''' <remarks>Llamada desde:certifproveedores.aspx; Tiempo m�ximo:0</remarks>
*/
function copiarFila(sRoot, idCampo, ProveCod, ProveDen, lIdContacto, sDenContacto) {
    bMensajePorMostrar = document.getElementById("bMensajePorMostrar")
    if (bMensajePorMostrar) if (bMensajePorMostrar.value == "1") {
        bMensajePorMostrar.value = "0";
        return;
    }
    bMensajePorMostrar = null;
    var s
    //uwtGrupos__ctl0__ctl0
    sClase = document.getElementById(sRoot + "_tblDesglose").rows[document.getElementById(sRoot + "_tblDesglose").rows.length - 1].cells[0].className
    if (sClase == "ugfilatablaDesglose") sClase = "ugfilaalttablaDesglose"
    else sClase = "ugfilatablaDesglose"

    oTR = document.getElementById(sRoot + "_tblDesglose").insertRow(-1)

    if (document.getElementById(sRoot + "_numRows")) {
        lIndex = parseFloat(document.getElementById(sRoot + "_numRows").value) + 1
        document.getElementById(sRoot + "_numRows").value = lIndex
    } else {
        lIndex = document.getElementById(sRoot + "_tblDesglose").rows.length - 1
        s = "<input type=hidden name=" + sRoot + "_numRows id=" + sRoot + "_numRows value=" + lIndex.toString() + ">"
        document.getElementById("divAlta").insertAdjacentHTML("beforeEnd", s)
    }

    if (document.getElementById(sRoot + "_numRowsGrid")) {
        lLineaGrid = parseFloat(document.getElementById(sRoot + "_numRowsGrid").value) + 1
        document.getElementById(sRoot + "_numRowsGrid").value = lLineaGrid
    } else {
        lLineaGrid = document.getElementById(sRoot + "_tblDesglose").rows.length - 1
        s = "<input type=hidden name=" + sRoot + "_numRowsGrid id=" + sRoot + "_numRowsGrid value=" + lLineaGrid.toString() + ">"
        document.getElementById("divAlta").insertAdjacentHTML("beforeEnd", s)
    }

    //Controlar las filas que se a�aden para optimizar MontarFormularioSubmit
    var ultimaFila = 0
    for (var indice in htControlFilas) {
        if (String(idCampo) == indice.substring(0, indice.indexOf("_"))) {
            if (parseInt(ultimaFila) < parseInt(htControlFilas[indice])) {
                ultimaFila = htControlFilas[indice]
            }
        }
    }

    htControlFilas[String(idCampo) + "_" + String(lIndex)] = String(parseInt(ultimaFila) + 1)
    //A continuacion se obtiene lPrimeraLinea, que es la fila de donde se copian los datos
    var lPrimeraLinea
    if (lLineaGrid == 1) {
        lPrimeraLinea = lIndex
        var js = "<input type=hidden name=" + sRoot + "_numPrimeraLinea id=" + sRoot + "_numPrimeraLinea value=" + lIndex.toString() + ">"
        document.forms[0].insertAdjacentHTML("beforeEnd", js)
        js = null;
    } else {

        if (document.getElementById(sRoot + "_numPrimeraLinea")) lPrimeraLinea = document.getElementById(sRoot + "_numPrimeraLinea").value
        else lPrimeraLinea = 1
    }

    lPrimeraLinea = parseInt(lPrimeraLinea.toString(), 10)

    //En este array se guardan en cada fila el nuevo data entry y del que se va acoger el valor (_1_)
    var arraynuevafila = new Array()
    var insertarnuevoelemento = 0
    for (var i = 0; i < document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells.length; i++) {
        arraynuevafila[i] = new Array()
        for (var j = 0; j < 2; j++)
            arraynuevafila[i][j] = ""
    }

    for (i = 0; i < document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells.length; i++) {
        s = document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells[i].innerHTML
        oTD = oTR.insertCell(-1)
        oTD.className = sClase
        if (document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells[i].id) oTD.id = document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells[i].id + '_' + lIndex;

        var entryActual
        var stemp = s
        var stemp2
        var tempinicio
        var tempfin

        if (stemp.search(' id="') > 0) { //En FireFox los ids vienen con comillas
            tempinicio = stemp.search(' id="')
            tempinicio = tempinicio + 5
        } else {
            tempinicio = stemp.search(" id=") //En IE los ids vienen sin comillas
            tempinicio = tempinicio + 4
        }
        stemp2 = stemp.substr(tempinicio, stemp.length - tempinicio)
        tempfin = stemp2.search("__tbl")
        entryActual = stemp2.substr(0, tempfin)

        re = /fsentry/
        while (s.search(re) >= 0) {
            s = s.replace(re, "fsdsentry_" + lIndex.toString() + "_")
        }

        //Para evitar generar los contenidos de los webdropdown con los mismos IDs y a fin de que podamos localizar esos datos con m�s facilidad, vamos a editar los IDs
        var webdropdownReplace = '';
        var finID = s.search(".0:mkr:Target");
        if (finID > -1) {
            var extraer = s.substr(0, finID);
            var inicioID = extraer.lastIndexOf("x:");
            if (inicioID > -1) {
                extraer = extraer.substr(inicioID + 2, finID)
                webdropdownReplace = extraer;
                extraer = null;
            }
            inicioID = null;
        }
        finID = null;
        if (webdropdownReplace != '') {
            var numAleatorio = Aleatorio(1, parseInt(webdropdownReplace))
            var re = new RegExp() // empty constructor
            re.compile(webdropdownReplace, 'gi')
            s = s.replace(re, parseInt(webdropdownReplace) + parseInt(numAleatorio))
            //Reemplazar la clase seleccionada para que no se seleccione nada por defecto al copiar l�nea vac�a
            re.compile('igdd_FullstepPMWebListItemSelected', 'gi')
            s = s.replace(re, parseInt(webdropdownReplace) + parseInt(numAleatorio))
            numAleatorio = null;
        }
        webdropdownReplace = null;

        se = /<SCRIPT language=javascript>/
        se2 = /<script language="javascript">/
        var arrHTML = new Array()
        var arrJS = new Array()

        if ((s.search(se) > 0) || (s.search(se2) > 0)) {
            while (s.search(se) > 0) {
                iComienzo = s.search(se) + 28
                se = /<\/SCRIPT>/
                iFin = s.search(se)
                var sEval = s.substr(iComienzo, iFin - iComienzo)
                s = s.substr(0, iComienzo - 28) + s.substr(iFin + 9)
                arrJS[arrJS.length] = sEval
                se = /<SCRIPT language=javascript>/
            }
            while (s.search(se2) > 0) {
                var iComienzo = s.search(se2) + 30
                se = /<\/script>/
                var iFin = s.search(se)
                var sEval = s.substr(iComienzo, iFin - iComienzo)
                s = s.substr(0, iComienzo - 30) + s.substr(iFin + 9)
                arrJS[arrJS.length] = sEval
                sEval = null;
                se2 = /<script language="javascript">/
            }
            ni = /##newIndex##/g
            if (s.search(ni) > 0) {
                oTD.align = "center"
            }
            s = replaceAll(s, "##newIndex##", lIndex.toString())

            oTD.insertAdjacentHTML("beforeEnd", s)

            for (k = 0; k < arrJS.length; k++) {
                eval(arrJS[k])
            }
        } else {
            ni = /##newIndex##/g
            if (s.search(ni) > 0) {
                oTD.align = "center"
            }
            s = replaceAll(s, "'##newIndex##'", lIndex.toString())
            oTD.insertAdjacentHTML("beforeEnd", s)
        }

        //Aqui es donde se introducen en el array los controles	
        stemp = s

        if (stemp.search(' id="') > 0) { //En FireFox los ids vienen con comillas
            tempinicio = stemp.search(' id="')
            tempinicio = tempinicio + 5
        } else {
            tempinicio = stemp.search(" id=") //En IE los ids vienen sin comillas
            tempinicio = tempinicio + 4
        }
        stemp2 = stemp.substr(tempinicio, stemp.length - tempinicio)
        tempfin = stemp2.search("__tbl")
        mientry = stemp2.substr(0, tempfin)

        a = new Array();
        var entry1;
        var numero
        a = mientry.split("_")
        var otmp;
        var o1tmp;
        if ((a.length == 7) || (a.length == 4) || (a.length == 9)) {
            entry1 = mientry
            if (a.length == 7) //cuando no es popup
                numero = a[5]
            else if (a.length == 4) //Cuando es popup
                numero = a[2]
            else if (a.length == 9) //Cuando el desglose no es una ventana emergente
                numero = a[7]

            numero = parseInt(numero.toString(), 10)

            var cadenaareemplazar = "_fsdsentry_" + numero.toString() + "_"
            entry1 = entry1.replace(cadenaareemplazar, "_fsdsentry_" + lPrimeraLinea.toString() + "_")

            arraynuevafila[insertarnuevoelemento][0] = mientry
            arraynuevafila[insertarnuevoelemento][1] = entry1

            ////////////////////////////////////
            oContr = fsGeneralEntry_getById(arraynuevafila[insertarnuevoelemento][1])
            while (!oContr && numero > lPrimeraLinea) {
                lPrimeraLinea = lPrimeraLinea + 1
                entry1 = entryControl.replace(cadenaareemplazar, "_fsdsentry_" + lPrimeraLinea.toString() + "_")
                arraynuevafila[insertarnuevoelemento][1] = entry1
                oContr = fsGeneralEntry_getById(arraynuevafila[insertarnuevoelemento][1])
            }
            cadenaareemplazar = null;

            var entryACopiar = entry1.replace("fsdsentry_" + lPrimeraLinea.toString() + "_", "fsentry")
            oContr = fsGeneralEntry_getById(entryACopiar)

            if (oContr) {
                if (oContr.CampoOrigenVinculado == null) {
                    if ((oContr.intro == 1) && (oContr.readOnly == false)) {
                        var param;
                        if (a.length == 9) param = a[0] + '$_' + a[2] + '$' + a[3] + '$' + a[4] + '_' + a[5] + '$' + a[6] + '_' + a[7] + '_' + a[8] + '$_t';
                        else if (a.length >= 6) param = a[0] + '$_' + a[2] + '$' + a[3] + '$' + a[4] + '_' + a[5] + '$' + a[6] + '$_t';
                        if (a[0] == "ucDesglose") param = a[0] + '$' + a[1] + '$_t';

                        var bClientBinding = true;

                        var valores;

                        Sys.Application.add_init(function () {
                            $create(Infragistics.Web.UI.WebDropDown, {
                                "clientbindingprops": [
                                    [
                                        [, 1, 120, , , , , , , , 1, , 1, , 2, 0, , , 0, , 300, 120, , "", , , , 6, , 1, , , , , , , , , , 0, , , 1, 0, , , 0, , , , 1], {
                                            "c": {
                                                "dropDownInputFocusClass": "igdd_FullstepPMWebValueDisplayFocus",
                                                "dropDownButtonPressedImageUrl": "/ig_res/Fullstep/images/igdd_DropDownButtonPressed.png",
                                                "_pi": "[\"/ig_res/Fullstep/images/ig_ajaxIndicator.gif\",,,\"ig_FullstepPMWebAjaxIndicator\",,1,,,,\"ig_FullstepPMWebAjaxIndicatorBlock\",,3,,3,\"Async post\"]",
                                                "pageCount": 0,
                                                "vse": 0,
                                                "textField": "Texto",
                                                "controlAreaHoverClass": "ig_FullstepPMWebHover igdd_FullstepPMWebControlHover",
                                                "controlAreaClass": "igdd_FullstepPMWebControlArea",
                                                "valueField": "Valor",
                                                "controlClass": "ig_FullstepPMWebControl igdd_FullstepPMWebControl",
                                                "dropDownInputHoverClass": "ig_FullstepPMWebHover igdd_FullstepPMWebValueDisplayHover",
                                                "dropDownItemDisabled": "igdd_FullstepPMWebListItemDisabled",
                                                "uid": mientry + "_t",
                                                "nullTextCssClass": "igdd_FullstepPMWebNullText",
                                                "controlAreaFocusClass": "igdd_FullstepPMWebControlFocus",
                                                "dropDownItemHover": "igdd_FullstepPMWebListItemHover",
                                                "dropDownButtonNormalImageUrl": "/ig_res/Fullstep/images/igdd_DropDownButton.png",
                                                "dropDownInputClass": "igdd_FullstepPMWebValueDisplay",
                                                "controlDisabledClass": "ig_FullstepPMWebDisabled igdd_FullstepPMWebControlDisabled",
                                                "dropDownButtonDisabledImageUrl": "/ig_res/Fullstep/images/igdd_DropDownButtonDisabled.png",
                                                "dropDownItemClass": "igdd_FullstepPMWebListItem",
                                                "pi": "[,,9,,1,1,,80,,,,3,,3,\"Async post\"]",
                                                "dropDownItemSelected": "igdd_FullstepPMWebListItemSelected",
                                                "ocs": 1,
                                                "dropDownButtonClass": "igdd_FullstepPMWebDropDownButton",
                                                "dropDownItemActiveClass": "igdd_FullstepPMWebListItemActive",
                                                "dropDownButtonHoverImageUrl": "/ig_res/Fullstep/images/igdd_DropDownButtonHover.png",
                                                "dropDownInputDisabledClass": "ig_FullstepPMWebDisabled igdd_FullstepPMWebValueDisplayDisabled"
                                            }
                                        }], , [{}],
                                    ["InputKeyDown:WebDropDown_KeyDown", "DropDownOpening:WebDropDown_DataCheck", "SelectionChanged:WebDropDown_SelectionChanging", "Blur:WebDropDown_Focus", "ValueChanged:WebDropDown_valueChanged"],
                                    []
                                ],
                                "id": mientry + "__t",
                                "name": "_t"
                            }, null, null, $get(mientry + "__t"));
                        });

                        Sys.Application.add_load(function () {
                            $find(mientry + "__t").behavior.set_enableMovingTargetWithSource(false)
                        })

                        param = null;
                        valores = null;
                    }
                }
            }
            entryACopiar = null;
            ////////////////////////////////////

            insertarnuevoelemento = insertarnuevoelemento + 1
        }

        numero = null;
    }
    s = "<input type=hidden name=" + sRoot + "_" + lIndex + "_Deleted id=" + sRoot + "_" + lIndex + "_Deleted value='no'>"
    oTD.insertAdjacentHTML("beforeEnd", s)

    var iNumCeldas //indica el n�mero de celdas de la tabla 
    iNumCeldas = document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells.length // si estamos a�adiendo un proveedor para emitirle un certificado est�n todas las celdas

    for (i = 0; i < iNumCeldas; i++) {

        re = /fsentry/

        s2 = eval("arrEntrys_" + idCampo + "[i]")
        if (!s2) continue;

        s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
        s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")

        if (s2.search(",114,") > 0) {
            s2 = s2.replace(re, "fsdsentryx" + lIndex.toString() + "x") //en el caso del contacto del proveedor, la grid del dropdown es �nica para cada fila
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_") //tambien es �nico el campo dependiente (dependentfield)
        }
        eval(s2)

        s2 = null;
    }

    //Esto ocurre al a�adir un proveedor al que emitir un certificado
    //A�ade los datos del proveedor:
    oEntry = fsGeneralEntry_getById(sRoot + "_fsdsentry_" + lIndex.toString() + "_PROVE")
    if (oEntry) {
        oEntry.setValue(ProveCod + ' - ' + ProveDen)
        oEntry.setDataValue(ProveCod)
    }

    //A�ade los datos del contacto:
    oEntry = fsGeneralEntry_getById(sRoot + "_fsdsentry_" + lIndex.toString() + "_CON")
    if (oEntry) {
        oEntry.setValue(sDenContacto)
        oEntry.setDataValue(lIdContacto)
    }

    //Se hace la asignacion de valores para la nueva fila   
    var o;
    var o1;

    for (i = 0; i < insertarnuevoelemento; i++) {
        o = fsGeneralEntry_getById(arraynuevafila[i][0])
        o1 = fsGeneralEntry_getById(arraynuevafila[i][1])
        if (o) {
            o.nColumna = i;
            o.nLinea = lLineaGrid;

            if (o.tipoGS == 114) {
                if (o.dependentfield && o.dependentfield != '') {
                    o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_")
                    o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                }
            }

            if (o.readOnly == false) {
                var sID = ''
                if (o.Editor) {
                    if (o.Editor.ID) {
                        sID = o.Editor.ID
                    } else {
                        sID = o.Editor.id
                    }
                    if (sID != '' && sID != undefined) {
                        CambiarSeleccion(sID, "BORRAR")
                    }
                }
                sID = null;
            }
        }
    }
};
function TieneMasDecimalesQueEsteUsuario(num, dec, separador) {
    //separador 1-->","   2-->"."
    var nTmp = num

    var sNum = nTmp.toString()
    var rd = /\./
    var pDec
    if (separador == 1) pDec = sNum.search(",")
    else pDec = sNum.search(rd)

    if (pDec == -1) return (0)

    sDec = sNum.substr(pDec + 1, sNum.length)
    if (sDec.length > dec) {
        return (1)
    } else {
        return (0)
    };
};
function redondeacalc(num, dec, separador) {
    var nTmp = num
    var sNum = nTmp.toString()
    var rd = /\./
    var pDec
    if (separador == 1) pDec = sNum.search(",")
    else pDec = sNum.search(rd)

    if (pDec == -1) return (num)
    sDec = sNum.substr(pDec + 1, sNum.length)
    dec = (dec < sDec.length) ? dec : sDec.length

    //nTmp= parseFloat(nTmp)
    //nTmp = nTmp.replace(vthousanfmt, '')
    nTmp = nTmp.replace(",", '.')

    for (var i = 0; i < dec; i++) {
        nTmp = nTmp * 10
    }

    nTmp = Math.round(nTmp)


    for (var i = 0; i < dec; i++)
        nTmp = nTmp / 10

    return (nTmp)
};
function num2strcalc(num, vdecimalfmt, vthousanfmt, vprecisionfmt, separador) {
    var j
    if (num == 0) {
        return ("0")
    }
    if (num == null) {
        return ("")
    }

    var sNum
    var sDec
    var sInt
    var bNegativo
    var i

    bNegativo = false
    var vNum = redondeacalc(num, vprecisionfmt, separador)
    sNum = vNum.toString()
    if (sNum.charAt(0) == "-") {
        bNegativo = true
        sNum = sNum.substr(1, sNum.length)
    }
    rd = /\./
    pDec = sNum.search(rd)

    if (pDec == -1) {
        sInt = sNum
        sDec = ""
    } else {
        sInt = sNum.substr(0, pDec)
        sDec = sNum.substr(pDec + 1, 20)
        if (sDec.substr(sDec.length - 4, 4) == "999" + sDec.substr(sDec.length - 1, 1)) {
            var lDigito = 10 - parseFloat(sDec.substr(sDec.length - 1, 1))
            var sSum = "0."
            for (j = 1; j < sDec.length; j++)
                sSum += "0"
            sSum += lDigito.toString()


            sNum = (parseFloat(sInt + "." + sDec) + parseFloat(sSum)).toString()
            pDec = sNum.search(rd)

            if (pDec == -1) {
                sInt = sNum
                sDec = ""
            } else {
                sInt = sNum.substr(0, pDec)
                sDec = sNum.substr(pDec + 1, vprecisionfmt)
            }
        }
    }

    if (sInt.length > 3) {
        newValor = ""
        s = 0
        //los posicionamos correctamente
        for (i = sInt.length - 1; i >= 0; i--) {
            newValor = sInt.charAt(i) + newValor
            s++
        }
        sInt = newValor
    }

    var redondeo = sDec

    sDec = sDec.substr(0, vprecisionfmt >= 12 ? 10 : vprecisionfmt)

    while (sDec.charAt(sDec.length - 1) == "0")
        sDec = sDec.substr(0, sDec.length - 1)

    if (sDec != "" && sDec != "000000000000".substr(0, vprecisionfmt))
        //sNum= sInt + vdecimalfmt + sDec
        sNum = sInt + "." + sDec
    else sNum = sInt
    if (bNegativo == true) sNum = "-" + sNum

    var vprec = vprecisionfmt
    while (sNum == "0" && num != 0) {
        vprec++;
        sNum = num2strcalc(num, vdecimalfmt, vthousanfmt, vprec, separador)
    };
    return (sNum)
};
/*
''' <summary>
''' Crear los input necesarios el calculo de los importes del formulario.
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: NWAlta.aspx     Alta.aspx   AltaCertificado.aspx    DetalleCertificado.aspx
'''     AltaNoConformidad.aspx    DetalleNoConformidad.aspx    NoConformidadRevisor.aspx
'''     NWDetalleSolicitud.aspx   gestiontrasladada.aspx    NWgestionInstancia.aspx; Tiempo m�ximo:1,0sg</remarks>
*/
function MontarFormularioCalculados() {
    var f = document.forms["frmAlta"]
    if (!f) f = document.forms["frmDetalle"]

    oFrm = document.getElementById("frmCalculados")
    oFrm.innerHTML = ""

    if (f.elements["Solicitud"]) {
        oFrm.insertAdjacentHTML("beforeEnd", "<INPUT id=CALC_Solicitud type=hidden NAME=CALC_Solicitud>")
        oFrm.elements["CALC_Solicitud"].value = f.elements["Solicitud"].value
    }
    if (f.elements["Instancia"]) {
        oFrm.insertAdjacentHTML("beforeEnd", "<INPUT id=CALC_Instancia type=hidden NAME=CALC_Instancia>")
        oFrm.elements["CALC_Instancia"].value = f.elements["Instancia"].value
    }
    if (f.elements["Version"]) {
        oFrm.insertAdjacentHTML("beforeEnd", "<INPUT id=CALC_NumVersion type=hidden NAME=CALC_NumVersion>")
        oFrm.elements["CALC_NumVersion"].value = f.elements["Version"].value
    }
    if (f.elements["SoloLectura"]) {
        oFrm.insertAdjacentHTML("beforeEnd", "<INPUT id=CALC_SoloLectura type=hidden NAME=CALC_SoloLectura>")
        oFrm.elements["CALC_SoloLectura"].value = f.elements["SoloLectura"].value
    }
    oFrm.insertAdjacentHTML("beforeEnd", "<input type=hidden name=CALC_txtNumDesgloses>")
    oFrm.elements["CALC_txtNumDesgloses"].value = arrDesgloses.length
    for (i = 0; f.elements["txtPre_" + i.toString()]; i++) {
        sGrupo = f.elements["txtPre_" + i.toString()].value
        sGrupo = sGrupo.split("_")
        sGrupo = sGrupo[sGrupo.length - 1]
        oFrm.insertAdjacentHTML("beforeEnd", "<INPUT id=CALC_txtPre_" + sGrupo + " type=hidden name=CALC_txtPre_" + sGrupo + ">")
        oFrm.elements["CALC_txtPre_" + sGrupo].value = f.elements["txtPre_" + i.toString()].value

    }

    if (typeof (arrDesgloses) != "undefined") {
        for (i = 0; i < arrDesgloses.length; i++) {
            sDesglose = replaceAll(arrDesgloses[i], "_tblDesglose", "")
            sNumRows = replaceAll(arrDesgloses[i], "_tblDesglose", "_numRows")
            oFrm.insertAdjacentHTML("beforeEnd", "<input type=hidden name=CALC_nomDesglose_" + i.toString() + " value ='" + arrDesgloses[i] + "'>")
            oFrm.insertAdjacentHTML("beforeEnd", "<input type=hidden name=CALC_" + arrDesgloses[i] + ">")
            oFrm.elements["CALC_" + arrDesgloses[i]].value = document.getElementById(sNumRows).value
        }
    }

    if (typeof (arrCalculados) != "undefined") {
        for (i = 0; i < arrCalculados.length; i++) {
            if (arrCalculados[i] != "deleted") {
                oEntry = fsGeneralEntry_getById(arrCalculados[i])
                if (oEntry) {
                    oFrm.insertAdjacentHTML("beforeEnd", "<input type=hidden name=CALC_" + arrCalculados[i] + "  id=CALC_" + arrCalculados[i] + ">")
                    if (oEntry.readOnly) {
                        vValor = oEntry.dataValue
                        if (vValor == null) vValor = ""
                        else {
                            if (oEntry.dataValue.toString().toString().indexOf(',') != -1) vValor = oEntry.dataValue.replace(",", ".")
                            else vValor = oEntry.dataValue
                        }
                        document.forms["frmCalculados"].elements["CALC_" + arrCalculados[i]].value = vValor

                    } else {
                        vValor = oEntry.dataValue
                        if (vValor == null) {
                            vValor = oEntry.getValue()
                            if (vValor == null) vValor = ""
                        } else {
                            if (TieneMasDecimalesQueEsteUsuario(oEntry.dataValue, vprecisionfmt, 1)) {
                                formateado = num2strcalc(oEntry.dataValue, vdecimalfmt, vthousanfmt, vprecisionfmt, 1)
                                formateado2 = redondeacalc(oEntry.dataValue, vprecisionfmt, 1)
                                if ((formateado == oEntry.getValue()) || (formateado2 == oEntry.getValue())) {
                                    if (oEntry.dataValue.toString().toString().indexOf(',') != -1) vValor = oEntry.dataValue.replace(",", ".")
                                    else vValor = oEntry.dataValue
                                } else {
                                    if (oEntry.TablaExterna > 0) vValor = oEntry.dataValue.replace(",", ".")
                                    else
                                        if (oEntry.intro == 1) vValor = oEntry.dataValue.replace(",", ".")
                                        else vValor = oEntry.getValue()
                                }
                            } else {
                                if (oEntry.TablaExterna > 0) vValor = oEntry.dataValue.replace(",", ".")
                                else
                                    if (oEntry.intro == 1) vValor = oEntry.dataValue.replace(",", ".")
                                    else vValor = oEntry.getValue()
                            }
                        }

                        document.forms["frmCalculados"].elements["CALC_" + arrCalculados[i]].value = vValor
                    }

                } else {
                    sDesglose = arrCalculados[i]
                    sDesglose = sDesglose.split("__")
                    idCampo = sDesglose[sDesglose.length - 1]

                    sDesglose = replaceAll(arrCalculados[i], "__" + idCampo, "")
                    iNumRows = document.getElementById(sDesglose + "__numRows").value
                    iNumTotRows = document.getElementById(sDesglose + "__numTotRows").value
                    if (!document.forms["frmCalculados"].elements["CALC_" + sDesglose + "__numRows"]) {
                        oFrm.insertAdjacentHTML("beforeEnd", "<input type=hidden name=CALC_" + sDesglose + "__numRows  id=CALC_" + sDesglose + "__numRows>")
                        document.forms["frmCalculados"].elements["CALC_" + sDesglose + "__numRows"].value = iNumRows
                    }
                    if (!document.forms["frmCalculados"].elements["CALC_" + sDesglose + "__numTotRows"]) {
                        oFrm.insertAdjacentHTML("beforeEnd", "<input type=hidden name=CALC_" + sDesglose + "__numTotRows  id=CALC_" + sDesglose + "__numTotRows>")
                        document.forms["frmCalculados"].elements["CALC_" + sDesglose + "__numTotRows"].value = iNumTotRows
                    }

                    for (k = 1; k <= iNumTotRows; k++) {
                        if (document.getElementById(sDesglose + "__" + k.toString() + "__Deleted")) if (document.getElementById(sDesglose + "__" + k.toString() + "__" + idCampo)) {
                            oFrm.insertAdjacentHTML("beforeEnd", "<input type=hidden name=CALC_" + sDesglose + "__" + k.toString() + "__" + idCampo + "  id=CALC_" + sDesglose + "__" + k.toString() + "__" + idCampo + ">")
                            vValor = document.getElementById(sDesglose + "__" + k.toString() + "__" + idCampo).value
                            if (vValor == null) {
                                vValor = ""
                            } else {
                                vValor1 = document.getElementById(sDesglose + "__" + k.toString() + "__" + idCampo).defaultValue

                                if (TieneMasDecimalesQueEsteUsuario(vValor1, vprecisionfmt, 2)) {
                                    formateado = num2strcalc(vValor1, vdecimalfmt, vthousanfmt, vprecisionfmt, 2)
                                    formateado2 = redondeacalc(vValor1, vprecisionfmt, 2)
                                    if ((formateado == vValor) || (formateado2 == vValor)) {
                                        vValor = vValor1
                                    }
                                }
                            }
                            document.forms["frmCalculados"].elements["CALC_" + sDesglose + "__" + k.toString() + "__" + idCampo].value = vValor
                        }
                    }
                }
            }
        }
    }

    oFrm.insertAdjacentHTML("beforeEnd", "<input type=hidden name=CALC_MONEDA id=CALC_MONEDA>")
    var monedaInput = $.map(arrInputs, function (x) { if (fsGeneralEntry_getById(x) != null) { if (fsGeneralEntry_getById(x).tipoGS == 102 && !fsGeneralEntry_getById(x).basedesglose) return fsGeneralEntry_getById(x); } })[0];
    if ((monedaInput) && (monedaInput.dataValue != '') && (monedaInput.dataValue != null)) { document.forms["frmCalculados"].elements["CALC_MONEDA"].value = monedaInput.getDataValue(); }

    return oFrm
};
/*
''' <summary>
''' Crear un input tipo hidden con el nombre pasado en el param sInput y el valor pasado.
''' </summary>
''' <param name="sInput">nombre del hidden que se crea</param>
''' <param name="valor">valor del hidden que se crea</param>   
''' <returns>Nada</returns>
''' <remarks>Llamada desde: MontarFormularioSubmit</remarks>
*/
function anyadirInput(sInput, valor) {
    if ((valor == "null") || (valor == null)) valor = ""

    var f = document.forms["frmSubmit"]
    if (!f.elements[sInput]) {
        f.insertAdjacentHTML("beforeEnd", "<INPUT type=hidden name=" + sInput + ">\n")
        f.elements[sInput].value = valor
    }
};
/*
''' <summary>
''' Crear los input necesarios para la grabaci�n en bbdd de solicitudes, no conformidades y certificados.
''' </summary>
''' <param name="guarda">Hay cambio de etapa o no. True no lo hay False lo hay</param>
''' <param name="nuevoWorkflow">Estas grabando PM � QA. True solicitud de compra False no conformidad � certificado</param>   
''' <param name="Mapper">Si se aplica el control Mapper de integraci�n</param>        
''' <param name="AbrirSelf">Abrir Guardar Instancia en self o en frame fraWSServer</param>   
''' <returns>Nada</returns>
''' <remarks>Llamada desde: NWAlta.aspx     Alta.aspx   AltaCertificado.aspx    DetalleCertificado.aspx
'''     AltaNoConformidad.aspx    DetalleNoConformidad.aspx    NoConformidadRevisor.aspx
'''     NWDetalleSolicitud.aspx   gestiontrasladada.aspx    NWgestionInstancia.aspx; Tiempo m�ximo:1,0sg</remarks>
*/
function MontarFormularioSubmit(guarda, nuevoWorkflow, Mapper, AbrirSelf) {
    var bAlta = false;
    p = window.opener;
    if (p) {
        if (p.document.forms["frmAlta"] != undefined) bAlta = true;
    } else {
        if (document.forms["frmAlta"] != undefined) bAlta = true;
    }
    if (!document.forms["frmSubmit"]) {
        if (AbrirSelf == true) document.body.insertAdjacentHTML("beforeEnd", "<form name=frmSubmit action=../_common/guardarinstancia.aspx target=_self id=frmSubmit method=post style='position:absolute;top:0;left:0'></form>");
        else document.body.insertAdjacentHTML("beforeEnd", "<form name=frmSubmit action=../_common/guardarinstancia.aspx target=fraWSServer id=frmSubmit method=post style='position:absolute;top:0;left:0'></form>");
    } else {
        var s = document.forms["frmSubmit"].innerHTML;
        var x = s.search('MOVER_');

        if (x == -1) document.forms["frmSubmit"].innerHTML = "";
        else {
            var i = 50;
            while ((x - i) < 0) {
                i = i - 1;
            }
            s = s.substr(x - i);
            var y = s.search('<INPUT');
            s = s.substr(y);
            s = replaceAll(s, "MOVER_", "MOVERLINEA_");
            document.forms["frmSubmit"].innerHTML = s;
        }
    }

    f = document.forms["frmDetalle"];
    if (!f) f = document.forms["frmAlta"];

    var Favorito = "";
    if (f.elements["Favorito"]) Favorito = f.elements["Favorito"].value;

    //IdFavorito
    if (f.elements["IdFavorito"]) anyadirInput("IdFavorito", f.elements["IdFavorito"].value);

    var InstanciaImportar = "0";
    if (f.elements["IdInstanciaImportar"]) {
        InstanciaImportar = f.elements["IdInstanciaImportar"].value;
        anyadirInput("IdInstanciaImportar", InstanciaImportar);
    }

    anyadirInput("GEN_MostrarAvisoMapper", true);
    //primero los ids generales de la instancia / version / certificado / no conformidad
    anyadirInput("GEN_AccionRol", "");
    anyadirInput("GEN_Rol", "");

    if (guarda == true || guarda == "true") anyadirInput("GEN_Guardar", 1);
    else anyadirInput("GEN_Guardar", 0);

    anyadirInput("GEN_Bloque", "");
    if (f.elements["Solicitud"]) anyadirInput("GEN_Solicitud", f.elements["Solicitud"].value);

    if (f.elements["Instancia"]) anyadirInput("GEN_Instancia", f.elements["Instancia"].value);
    else if (f.elements["NEW_Instancia"]) anyadirInput("GEN_Instancia", f.elements["NEW_Instancia"].value);

    if (f.elements["Certificado"]) anyadirInput("GEN_Certificado", f.elements["Certificado"].value);
    if (f.elements["Version"]) anyadirInput("GEN_Version", f.elements["Version"].value);
    if (f.elements["NoConformidad"]) anyadirInput("GEN_NoConformidad", f.elements["NoConformidad"].value);
    if (f.elements["Notificar"]) anyadirInput("GEN_Notificar", f.elements["Notificar"].value);

    anyadirInput("GEN_Enviar", "0");
    anyadirInput("GEN_Accion", "");

    //para los certificados y las no conformidades:
    anyadirInput("GEN_DespubDia", "");
    anyadirInput("GEN_DespubMes", "");
    anyadirInput("GEN_DespubAnyo", "");

    anyadirInput("GEN_FecApliPlanDia", "");
    anyadirInput("GEN_FecApliPlanMes", "");
    anyadirInput("GEN_FecApliPlanAnyo", "");
    
    anyadirInput("GEN_LimiteCumplimentacionDia", "");
    anyadirInput("GEN_LimiteCumplimentacionMes", "");
    anyadirInput("GEN_LimiteCumplimentacionAnyo", "");

    //Para las no conformidades
    anyadirInput("GEN_NoConfProve", "");
    anyadirInput("GEN_NoConfProveDEN", "");
    anyadirInput("GEN_NoConfConDEN", "");
    anyadirInput("GEN_NoConfRevisor", ""); //valor del combo revisor
    anyadirInput("GEN_NoConfComentAlta", "");
    anyadirInput("GEN_NoConfUnidadQa", "");
    anyadirInput("GEN_IdEscalacionProve", "");
    anyadirInput("GEN_NivelEscalacion", "");
    anyadirInput("GEN_EsNoconfEscalacion", "");
    anyadirInput("GEN_NoConfProveERP", "");
    anyadirInput("GEN_NoConfProveERPDen", "");

    //Notificados internos para no conformidad
    anyadirInput("GEN_NoConfContactosProveedor", "");
    anyadirInput("GEN_NoConfContactosInternos", "");
    anyadirInput("GEN_CambioRevisor", "");

    //Valor del combo de las versiones, para saber si se guarda con nueva version
    anyadirInput("GEN_NombreVersion", "");

    if (f.elements["hidVolver"]) anyadirInput("sVolver", f.elements["hidVolver"].value);

    //Mapper
    anyadirInput("PantallaMaper", "");
    anyadirInput("DeDonde", "");

    //Campos de contrato
    anyadirInput("ID_CONTRATO", "");
    anyadirInput("COD_CONTRATO", "");
    anyadirInput("PathContrato", "");
    anyadirInput("NombreContrato", "");
    anyadirInput("ProcesoContrato", "");
    anyadirInput("GruposContrato", "");
    anyadirInput("ItemsContrato", "");
    anyadirInput("GEN_EmpresaID", "");
    anyadirInput("GEN_PROVE", "");
    anyadirInput("GEN_ContactoID", "");
    anyadirInput("GEN_CodMoneda", "");
    anyadirInput("GEN_FechaIni", ""); // dd/mm/aaaa --> hay que separarlo y convertirlo en date
    anyadirInput("GEN_FechaFin", "");
    anyadirInput("GEN_Alerta", "");
    anyadirInput("GEN_PeriodoAlerta", "");
    anyadirInput("GEN_RepetirEmail", "");
    anyadirInput("GEN_PeriodoRepetirEmail", "");
    anyadirInput("GEN_AlertaImporteDesde", "");
    anyadirInput("GEN_AlertaImporteHasta", "");
    anyadirInput("GEN_Notificados", "");
    anyadirInput("GEN_Volver", "");

    if (nuevoWorkflow == true) {
        if (guarda == false) return;
    }
    //y luego todos los inputs
    if (typeof (arrInputs) != "undefined") {
        for (arrInput in arrInputs) {
            oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
            if (oEntry) {
                if (oEntry.tipoGS == 136) {
                    //DESGLOSE DE ACTIVIDAD
                    if (sIdProyecto != null) { //Si hay un campo Desglose de actividad
                        var grid = igtbl_getGridById(NomGridDesgloseAct);

                        var x = 0;
                        for (x = 0; x <= grid.Rows.length - 1; x++) {
                            var rowG = grid.Rows.getRow(x);
                            //Se comprueba el estado de la fila, si es nueva, modificada, eliminada.....
                            var Status = rowG.getCellFromKey("STATUS").getValue();
                            if (Status != '' && Status != null) {
                                var Num_Old = rowG.getCellFromKey("NUM_OLD").getValue();
                                var Num = rowG.getCellFromKey("NUM").getValue();
                                rowG.getCellFromKey("NUM_OLD").setValue(Num);
                                var IdTarea = rowG.getCellFromKey("TAREA").getValue();
                                if (IdTarea == null) IdTarea = '';
                                var codTarea = rowG.getCellFromKey("CODIGO").getValue();
                                var idEmp = rowG.getCellFromKey("EMP").getValue();
                                var CodContactos = rowG.getCellFromKey("CODCONTACTOS").getValue();
                                var CodProve = rowG.getCellFromKey("CODPROVE").getValue();
                                var CodUsuarios = rowG.getCellFromKey("CODUSUARIOS").getValue();
                                var costeTarea = rowG.getCellFromKey("COSTE_TARE").getValue();
                                var codCategoria = rowG.getCellFromKey("CATEGORIA").getValue();
                                var Hito = rowG.getCellFromKey("HITO").getValue();
                                var TareaDen = rowG.getCellFromKey("TAREADEN").getValue();
                                var HorasEst = rowG.getCellFromKey("HORAS_ASIG").getValue();
                                var GradoAvance = rowG.getCellFromKey("GRADO_AVANCE").getValue();
                                var estado = rowG.getCellFromKey("CODESTADO").getValue();
                                var fec_inicio = rowG.getCellFromKey("FECEST_INICIO").getValue();
                                if (fec_inicio == null) fec_inicio = '';
                                else fec_inicio = fec_inicio.format("MM/dd/yy").toString();
                                var fec_fin = rowG.getCellFromKey("FECEST_FIN").getValue();
                                if (fec_fin == null) fec_fin = '';
                                else fec_fin = fec_fin.format("MM/dd/yy").toString();
                                var Obs = rowG.getCellFromKey("OBS").getValue();

                                anyadirInput("DESGACT_" + Num + "_" + Status + "_" + Hito + "_" + sIdProyecto + "_" + estado + "_" + sNumFases + "_" + IdTarea + "_" + idEmp + "_" + Num_Old, codTarea);
                                anyadirInput("DESGACTCODUSU_" + Num + "_" + Status, CodUsuarios);
                                anyadirInput("DESGACTCODCON_" + Num + "_" + Status, CodContactos);
                                anyadirInput("DESGACTCODCAT_" + Num + "_" + Status, codCategoria);
                                anyadirInput("DESGACTCOSTE_" + Num + "_" + Status, costeTarea);
                                anyadirInput("DESGACTTAREADEN_" + Num + "_" + Status, TareaDen);
                                anyadirInput("DESGACTOBS_" + Num + "_" + Status, Obs);
                                anyadirInput("DESGACTPROVE_" + Num + "_" + Status, CodProve);
                                anyadirInput("DESGACTHORASGRADOAV_" + Num + "_" + HorasEst + "_" + GradoAvance + "_" + Status, "");
                                anyadirInput("DESGACTFECHAS_" + Num + "_" + fec_inicio + "_" + fec_fin + "_" + Status, "");
                                switch (sNumFases) {
                                    case 1:
                                        var IdNiv1 = rowG.getCellFromKey("ID_NIV4").getValue();
                                        if (IdNiv1 == null) IdNiv1 = 0;
                                        var CodNiv1 = rowG.getCellFromKey("COD_NIV4").getValue();
                                        var DenNiv1 = rowG.getCellFromKey("DEN_NIV4").getValue();

                                        while (CodNiv1.search(" ") >= 0) {
                                            CodNiv1 = CodNiv1.replace(" ", "#@");
                                        }

                                        anyadirInput("DESGACTNIV1_" + Num + "_" + Status + "_" + IdNiv1 + "$$$" + CodNiv1, DenNiv1);
                                        break;
                                    case 2:
                                        var IdNiv1 = rowG.getCellFromKey("ID_NIV3").getValue();
                                        if (IdNiv1 == null) IdNiv1 = 0
                                        var CodNiv1 = rowG.getCellFromKey("COD_NIV3").getValue();
                                        var DenNiv1 = rowG.getCellFromKey("DEN_NIV3").getValue();
                                        while (CodNiv1.search(" ") >= 0) {
                                            CodNiv1 = CodNiv1.replace(" ", "#@")
                                        }
                                        anyadirInput("DESGACTNIV1_" + Num + "_" + Status + "_" + IdNiv1 + "$$$" + CodNiv1, DenNiv1)


                                        var IdNiv2 = rowG.getCellFromKey("ID_NIV4").getValue();
                                        if (IdNiv2 == null) IdNiv2 = 0
                                        var CodNiv2 = rowG.getCellFromKey("COD_NIV4").getValue();
                                        var DenNiv2 = rowG.getCellFromKey("DEN_NIV4").getValue();
                                        while (CodNiv2.search(" ") >= 0) {
                                            CodNiv2 = CodNiv2.replace(" ", "#@")
                                        }

                                        anyadirInput("DESGACTNIV2_" + Num + "_" + Status + "_" + IdNiv2 + "$$$" + CodNiv2, DenNiv2)
                                        break;
                                    case 3:
                                        var IdNiv1 = rowG.getCellFromKey("ID_NIV2").getValue();
                                        if (IdNiv1 == null) IdNiv1 = 0
                                        var CodNiv1 = rowG.getCellFromKey("COD_NIV2").getValue();
                                        var DenNiv1 = rowG.getCellFromKey("DEN_NIV2").getValue();
                                        while (CodNiv1.search(" ") >= 0) {
                                            CodNiv1 = CodNiv1.replace(" ", "#@")
                                        }
                                        anyadirInput("DESGACTNIV1_" + Num + "_" + Status + "_" + IdNiv1 + "$$$" + CodNiv1, DenNiv1)


                                        var IdNiv2 = rowG.getCellFromKey("ID_NIV3").getValue();
                                        if (IdNiv2 == null) IdNiv2 = 0
                                        var CodNiv2 = rowG.getCellFromKey("COD_NIV3").getValue();
                                        var DenNiv2 = rowG.getCellFromKey("DEN_NIV3").getValue();
                                        while (CodNiv2.search(" ") >= 0) {
                                            CodNiv2 = CodNiv2.replace(" ", "#@")
                                        }
                                        anyadirInput("DESGACTNIV2_" + Num + "_" + Status + "_" + IdNiv2 + "$$$" + CodNiv2, DenNiv2)

                                        var IdNiv3 = rowG.getCellFromKey("ID_NIV4").getValue();
                                        if (IdNiv3 == null) IdNiv3 = 0
                                        var CodNiv3 = rowG.getCellFromKey("COD_NIV4").getValue();
                                        var DenNiv3 = rowG.getCellFromKey("DEN_NIV4").getValue();
                                        while (CodNiv3.search(" ") >= 0) {
                                            CodNiv3 = CodNiv3.replace(" ", "#@")
                                        }

                                        anyadirInput("DESGACTNIV3_" + Num + "_" + Status + "_" + IdNiv3 + "$$$" + CodNiv3, DenNiv3)
                                        break;
                                    case 4:
                                        var IdNiv1 = rowG.getCellFromKey("ID_NIV1").getValue();
                                        if (IdNiv1 == null) IdNiv1 = 0;
                                        var CodNiv1 = rowG.getCellFromKey("COD_NIV1").getValue();
                                        var DenNiv1 = rowG.getCellFromKey("DEN_NIV1").getValue();
                                        while (CodNiv1.search(" ") >= 0) {
                                            CodNiv1 = CodNiv1.replace(" ", "#@");
                                        }
                                        anyadirInput("DESGACTNIV1_" + Num + "_" + Status + "_" + IdNiv1 + "$$$" + CodNiv1, DenNiv1);


                                        var IdNiv2 = rowG.getCellFromKey("ID_NIV2").getValue();
                                        if (IdNiv2 == null) IdNiv2 = 0;
                                        var CodNiv2 = rowG.getCellFromKey("COD_NIV2").getValue();
                                        var DenNiv2 = rowG.getCellFromKey("DEN_NIV2").getValue();
                                        while (CodNiv2.search(" ") >= 0) {
                                            CodNiv2 = CodNiv2.replace(" ", "#@");
                                        }
                                        anyadirInput("DESGACTNIV2_" + Num + "_" + Status + "_" + IdNiv2 + "$$$" + CodNiv2, DenNiv2);

                                        var IdNiv3 = rowG.getCellFromKey("ID_NIV3").getValue();
                                        if (IdNiv3 == null) IdNiv3 = 0
                                        var CodNiv3 = rowG.getCellFromKey("COD_NIV3").getValue();
                                        var DenNiv3 = rowG.getCellFromKey("DEN_NIV3").getValue();
                                        while (CodNiv3.search(" ") >= 0) {
                                            CodNiv3 = CodNiv3.replace(" ", "#@");
                                        }
                                        anyadirInput("DESGACTNIV3_" + Num + "_" + Status + "_" + IdNiv3 + "$$$" + CodNiv3, DenNiv3);

                                        var IdNiv4 = rowG.getCellFromKey("ID_NIV4").getValue();
                                        if (IdNiv4 == null) IdNiv4 = 0;
                                        var CodNiv4 = rowG.getCellFromKey("COD_NIV4").getValue();
                                        var DenNiv4 = rowG.getCellFromKey("DEN_NIV4").getValue();
                                        while (CodNiv4.search(" ") >= 0) {
                                            CodNiv4 = CodNiv4.replace(" ", "#@");
                                        }

                                        anyadirInput("DESGACTNIV4_" + Num + "_" + Status + "_" + IdNiv4 + "$$$" + CodNiv4, DenNiv4);
                                        break;

                                } //End switch

                            } //End if Status
                        }
                    } //FIN DESGLOSE DE ACTIVIDAD
                }
            }
            //si est� en rojo es que no est� validado y se borra
            if (oEntry) {
                if (oEntry.Editor != null) {
                    if (oEntry.Editor.elem != null) {
                        if (oEntry.Editor.elem.className == "TipoTextoMedioRojo") {
                            oEntry.setDataValue('');
                            oEntry.setValue('');
                            oEntry.Editor.elem.className = "TipoTextoMedio";
                        }
                    }
                }
            }

            if (oEntry)
                if (oEntry.tipo != 16) {
                    if (oEntry.idCampoPadre) //Este es un input perteneciente a un desglose
                    {
                        re = /fsentry/
                        x = oEntry.id.search(re)
                        if (oEntry.id.search(re) >= 0) {
                            if ((oEntry.tipoGS != 2000) && (oEntry.tipoGS != 3000) && (oEntry.tipo != 16)) {
                                anyadirInput("DESG_" + oEntry.idCampoPadre + "_" + oEntry.id.substr(x + 7) + "_0_1", "")
                                anyadirInput("DESGCCDHNP_" + oEntry.idCampoPadre + "_" + oEntry.id.substr(x + 7) + "_0_1", oEntry.CampoDef_CampoODesgHijo)
                                anyadirInput("DESGCCDPNP_" + oEntry.idCampoPadre + "_" + oEntry.id.substr(x + 7) + "_0_1", oEntry.CampoDef_DesgPadre)
                            }
                            if (oEntry.tipoGS == 2000) { //Solicitud Vinculada
                                anyadirInput("DSOLVINC_" + oEntry.idCampoPadre + "_2000_0_1_" + oEntry.CampoDef_DesgPadre, "1")
                            }
                        }

                        re = /fsdsentry/
                        x = oEntry.id.search(re) + 10
                        y = oEntry.id.search(oEntry.idCampo) - 1

                        if (oEntry.id.search(re) >= 0) //y no es el input base del desglose
                        {
                            if (oEntry.tipoGS == 42) {
                                y = x + 1
                            }
                            iLinea = oEntry.id.substr(x, y - x)
                            iLineaOriginal = iLinea

                            if (bAlta) {
                                iLinea = parseInt(htControlFilas[String(oEntry.idCampoPadre) + "_" + String(iLineaOriginal)]);
                            }

                            //input de desglose formato: IdCampoPadre_IdCampoHijo_Linea
                            if ((oEntry.tipoGS != 2000) && (oEntry.tipoGS != 3000) && (oEntry.tipo != 16)) {
                                anyadirInput("DESGTIPOGS_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString(), oEntry.tipoGS)
                                if (oEntry.tipoGS == 42) {
                                    anyadirInput("DESGCCDHNP_" + oEntry.idCampoPadre + "_1000_" + iLinea + "_" + oEntry.idCampo + "_1", oEntry.CampoDef_CampoODesgHijo)
                                    anyadirInput("DESGCCDPNP_" + oEntry.idCampoPadre + "_1000_" + iLinea + "_" + oEntry.idCampo + "_1", oEntry.CampoDef_DesgPadre)

                                } else {
                                    anyadirInput("DESGCCDHNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString(), oEntry.CampoDef_CampoODesgHijo)
                                    anyadirInput("DESGCCDPNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString(), oEntry.CampoDef_DesgPadre)
                                }
                            } else {
                                if (oEntry.tipoGS == 2000) { //Solicitud Vinculada
                                    anyadirInput("DSOLVINC_" + oEntry.idCampoPadre + "_2000_" + iLinea + "_1_" + oEntry.CampoDef_DesgPadre, oEntry.Hidden.value)
                                }
                            }


                            if (Mapper == true) {
                                if (oEntry.calculado == true) { //identifica si es campo calculado 
                                    sInput = "DESGCALC_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                    anyadirInput(sInput, oEntry.calculado)
                                    sInput = "DESGCALCTIT_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                    anyadirInput(sInput, oEntry.title)
                                }

                                if ((oEntry.intro == 0 || oEntry.intro == 2) && oEntry.tipoGS == 0 && oEntry.IdAtrib != 0) { // atributos 
                                    sInput = "DESGAT_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                    anyadirInput(sInput, oEntry.IdAtrib)
                                    sInput = "DESGERP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                    anyadirInput(sInput, oEntry.ValErp)
                                }

                                if (oEntry.TablaExterna > 0) {
                                    sInput = "DESGTABLAEXT_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                    anyadirInput(sInput, oEntry.TablaExterna)
                                }
                            }
                            
                            //2 -- lista externa
                            if (oEntry.intro == 2) {
                                sInput = "DESG_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                anyadirInput(sInput, FormatearListaExterna(oEntry.getValue()))
                            }
                            if (oEntry.intro == 1 && (oEntry.tipo == 1 || oEntry.tipo == 2 || oEntry.tipo == 3 || oEntry.tipo == 4 || oEntry.tipo == 5 || oEntry.tipo == 6 || oEntry.tipo == 7) && (oEntry.tipoGS == 0 || oEntry.tipoGS == 46 || oEntry.tipoGS == 148 || oEntry.tipoGS == 150)) {
                                sInput = "DESG_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                if (oEntry.tipo == 4) {
                                    anyadirInput(sInput, oEntry.getDataValue()) //Se ha metido el tipo = 4 (boolean) y se recoge diferente que los dem�s
                                } else if ((oEntry.intro == 1) && (oEntry.tipo == 2)) { //combo numerico puede ser 1 o 1234,56
                                    vValor = oEntry.getValue()
                                    if (vValor != null) {
                                        if (vValor.toString().search(',')) {
                                            vValor = vValor.toString().replace(",", ".")
                                        }
                                    }
                                    anyadirInput(sInput, vValor)
                                }
                                else {
                                    anyadirInput(sInput, oEntry.getValue())
                                }
                                sInput = "DESGID_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                anyadirInput(sInput, oEntry.getDataValue())
                                //                                                                                                                          
                                if (Mapper == true) {
                                    if (oEntry.IdAtrib != 0) {
                                        sInput = "DESGAT_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                        anyadirInput(sInput, oEntry.IdAtrib)
                                        sInput = "DESGERP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                        anyadirInput(sInput, oEntry.ValErp)
                                    }
                                }
                            } else {

                                sInput = "DESG_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()

                                if (oEntry.tipo == 8) { //archivo
                                    anyadirInput(sInput, oEntry.getValue())
                                } else if (oEntry.tipoGS == 104) //CodArticulo Soliciatudes anteriores
                                {
                                    if (oEntry.dependentfield != null) {
                                        var oMat;
                                        oMat = fsGeneralEntry_getById(oEntry.dependentfield);
                                        if (!oMat) //el material est� oculto
                                        {
                                            sRe = "/fsdsentry_" + iLinea.toString() + "/"
                                            re = eval(sRe)
                                            r = oEntry.dependentfield.search(re) + sRe.length - 1
                                            s = oEntry.dependentfield.length - r
                                            idMat = oEntry.dependentfield.substr(r, s)

                                            anyadirInput("DESG_" + oEntry.idCampoPadre + "_" + idMat + "_" + iLinea + "_6", oEntry.Dependent.value)
                                            anyadirInput("DESGTIPOGS_" + oEntry.idCampoPadre + "_" + idMat + "_" + iLinea + "_6", oEntry.Dependent.tipoGS)
                                            anyadirInput("DESGCCDHNP_" + oEntry.idCampoPadre + "_" + idMat + "_" + iLinea + "_6", -1000)
                                            anyadirInput("DESGCCDPNP_" + oEntry.idCampoPadre + "_" + idMat + "_" + iLinea + "_6", oEntry.CampoDef_DesgPadre)
                                        }
                                    }

                                    //Comprobar si la Unidad esta oculta
                                    if (oEntry.idEntryUNI != null) {
                                        oUni = fsGeneralEntry_getById(oEntry.idEntryUNI)
                                        if (!oUni) { //La Unidad est� oculta				
                                            sRe = "/fsdsentry_" + iLineaOriginal.toString() + "/"
                                            re = eval(sRe)
                                            r = oEntry.idEntryUNI.search(re) + sRe.length - 1
                                            s = oEntry.idEntryUNI.length - r
                                            idUni = oEntry.idEntryUNI.substr(r, s)
                                            if (idUni != "") {
                                                anyadirInput(sInputUni = "DESG_" + oEntry.idCampoPadre + "_" + idUni + "_" + iLinea + "_6", oEntry.UnidadDependent.value)
                                                anyadirInput("DESGCCDHNP_" + oEntry.idCampoPadre + "_" + idUni + "_" + iLinea + "_6", -1000)
                                                anyadirInput("DESGCCDPNP_" + oEntry.idCampoPadre + "_" + idUni + "_" + iLinea + "_6", oEntry.CampoDef_DesgPadre)
                                            }
                                        }
                                    }
                                    //Comprobar si la Org Compras est� oculta
                                    if (oEntry.idDataEntryDependent != null) {
                                        var oOrgCompras = fsGeneralEntry_getById(oEntry.idDataEntryDependent)
                                        if (!oOrgCompras) {
                                            sRe = "/fsdsentry_" + iLineaOriginal.toString() + "/"
                                            re = eval(sRe)
                                            r = oEntry.idDataEntryDependent.search(re) + sRe.length - 1
                                            s = oEntry.idDataEntryDependent.length - r
                                            idOrgCompras = oEntry.idDataEntryDependent.substr(r, s)
                                            if (idOrgCompras != "") {
                                                anyadirInput("DESG_" + oEntry.idCampoPadre + "_" + idOrgCompras + "_" + iLinea + "_6", oEntry.OrgComprasDependent.value)
                                                anyadirInput("DESGCCDHNP_" + oEntry.idCampoPadre + "_" + idOrgCompras + "_" + iLinea + "_6", -1000)
                                                anyadirInput("DESGCCDPNP_" + oEntry.idCampoPadre + "_" + idOrgCompras + "_" + iLinea + "_6", oEntry.CampoDef_DesgPadre)
                                            }
                                        }
                                    }
                                    //Comprobar si el Centro est� oculto
                                    if (oEntry.idDataEntryDependent2 != null) {
                                        oCentro = fsGeneralEntry_getById(oEntry.idDataEntryDependent2)
                                        if (!oCentro) { //La Unidad est� oculta				
                                            sRe = "/fsdsentry_" + iLineaOriginal.toString() + "/"
                                            re = eval(sRe)
                                            r = oEntry.idDataEntryDependent2.search(re) + sRe.length - 1
                                            s = oEntry.idDataEntryDependent2.length - r
                                            idCentro = oEntry.idDataEntryDependent2.substr(r, s)
                                            if (idCentro != "") {
                                                anyadirInput("DESG_" + oEntry.idCampoPadre + "_" + idCentro + "_" + iLinea + "_6", oEntry.CentroDependent.value)
                                                anyadirInput("DESGCCDHNP_" + oEntry.idCampoPadre + "_" + idCentro + "_" + iLinea + "_6", -1000)
                                                anyadirInput("DESGCCDPNP_" + oEntry.idCampoPadre + "_" + idCentro + "_" + iLinea + "_6", oEntry.CampoDef_DesgPadre)
                                            }
                                        }
                                    }

                                    if (oEntry.getDataValue() == null || oEntry.getDataValue() == "") anyadirInput(sInput, oEntry.getValue().replace(" - ", ""))
                                    else anyadirInput(sInput, oEntry.getDataValue())
                                } else if (oEntry.tipoGS == 119) //Nuevo Codigo de Articulo
                                {
                                    if (oEntry.dependentfield != null) {
                                        oDen = null;
                                        idEntryDen = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna + 1, oEntry.nLinea, 0)
                                        if (idEntryDen) oDen = fsGeneralEntry_getById(idEntryDen)

                                        //si la denominaci�n no est� oculta, no hace nada con el material (ya se har� cuando se trate la denominaci�n)
                                        if ((!oDen) || ((oDen) && (oDen.tipoGS != 118))) //La denominacion esta oculta									
                                        {
                                            var oMat;
                                            oMat = fsGeneralEntry_getById(oEntry.dependentfield)
                                            if (!oMat) //el material est� oculto
                                            {
                                                sRe = "/fsdsentry_" + iLineaOriginal.toString() + "/"
                                                re = eval(sRe)
                                                x = oEntry.IdMaterialOculta.search(re) + sRe.length - 1
                                                y = oEntry.IdMaterialOculta.length - x
                                                idMat = oEntry.IdMaterialOculta.substr(x, y)

                                                anyadirInput("DESG_" + oEntry.idCampoPadre + "_" + idMat + "_" + iLinea + "_6", oEntry.Dependent.value)
                                                anyadirInput("DESGTIPOGS_" + oEntry.idCampoPadre + "_" + idMat + "_" + iLinea + "_6", oEntry.Dependent.tipoGS)
                                                anyadirInput("DESGCCDHNP_" + oEntry.idCampoPadre + "_" + idMat + "_" + iLinea + "_6", -1000)
                                                anyadirInput("DESGCCDPNP_" + oEntry.idCampoPadre + "_" + idMat + "_" + iLinea + "_6", oEntry.CampoDef_DesgPadre)
                                            }

                                            if (oEntry.tag != null) {
                                                sRe = "/fsdsentry_" + iLineaOriginal.toString() + "/"
                                                re = eval(sRe)
                                                x = oEntry.IdDenArticuloOculta.search(re) + sRe.length - 1
                                                y = oEntry.IdDenArticuloOculta.length - x
                                                idDen = oEntry.IdDenArticuloOculta.substr(x, y)

                                                anyadirInput("DESG_" + oEntry.idCampoPadre + "_" + idDen + "_" + iLinea + "_6", oEntry.tag)
                                                anyadirInput("DESGID_" + oEntry.idCampoPadre + "_" + idDen + "_" + iLinea + "_6", oEntry.tag)
                                                anyadirInput("DESGCCDHNP_" + oEntry.idCampoPadre + "_" + idDen + "_" + iLinea + "_6", -1000)
                                                anyadirInput("DESGCCDPNP_" + oEntry.idCampoPadre + "_" + idDen + "_" + iLinea + "_6", oEntry.CampoDef_DesgPadre)
                                            }
                                        }
                                    }
                                    //Comprobar si la Unidad esta oculta
                                    if (oEntry.idEntryUNI != null) {
                                        oUni = fsGeneralEntry_getById(oEntry.idEntryUNI)
                                        if (!oUni) { //La Unidad est� oculta				
                                            sRe = "/fsdsentry_" + iLineaOriginal.toString() + "/"
                                            re = eval(sRe)
                                            r = oEntry.idEntryUNI.search(re) + sRe.length - 1
                                            s = oEntry.idEntryUNI.length - r
                                            idUni = oEntry.idEntryUNI.substr(r, s)
                                            if (idUni != "") {
                                                anyadirInput("DESG_" + oEntry.idCampoPadre + "_" + idUni + "_" + iLinea + "_6", oEntry.UnidadDependent.value)
                                                anyadirInput("DESGCCDHNP_" + oEntry.idCampoPadre + "_" + idUni + "_" + iLinea + "_6", -1000)
                                                anyadirInput("DESGCCDPNP_" + oEntry.idCampoPadre + "_" + idUni + "_" + iLinea + "_6", oEntry.CampoDef_DesgPadre)
                                            }
                                        }
                                    }
                                    //Comprobar si la Org Compras est� oculta
                                    if (oEntry.idDataEntryDependent != null) {
                                        var oOrgCompras = fsGeneralEntry_getById(oEntry.idDataEntryDependent)
                                        if (!oOrgCompras) {
                                            sRe = "/fsdsentry_" + iLineaOriginal.toString() + "/"
                                            re = eval(sRe)
                                            r = oEntry.idDataEntryDependent.search(re) + sRe.length - 1
                                            s = oEntry.idDataEntryDependent.length - r
                                            idOrgCompras = oEntry.idDataEntryDependent.substr(r, s)
                                            if (idOrgCompras != "") {
                                                anyadirInput("DESG_" + oEntry.idCampoPadre + "_" + idOrgCompras + "_" + iLinea + "_6", oEntry.OrgComprasDependent.value)
                                                anyadirInput("DESGCCDHNP_" + oEntry.idCampoPadre + "_" + idOrgCompras + "_" + iLinea + "_6", -1000)
                                                anyadirInput("DESGCCDPNP_" + oEntry.idCampoPadre + "_" + idOrgCompras + "_" + iLinea + "_6", oEntry.CampoDef_DesgPadre)
                                            }
                                        }
                                    }
                                    //Comprobar si el Centro est� oculto
                                    if (oEntry.idDataEntryDependent2 != null) {
                                        oCentro = fsGeneralEntry_getById(oEntry.idDataEntryDependent2)
                                        if (!oCentro) { //La Unidad est� oculta				
                                            sRe = "/fsdsentry_" + iLineaOriginal.toString() + "/"
                                            re = eval(sRe)
                                            r = oEntry.idDataEntryDependent2.search(re) + sRe.length - 1
                                            s = oEntry.idDataEntryDependent2.length - r
                                            idCentro = oEntry.idDataEntryDependent2.substr(r, s)
                                            if (idCentro != "") {
                                                anyadirInput("DESG_" + oEntry.idCampoPadre + "_" + idCentro + "_" + iLinea + "_6", oEntry.CentroDependent.value)
                                                anyadirInput("DESGCCDHNP_" + oEntry.idCampoPadre + "_" + idCentro + "_" + iLinea + "_6", -1000)
                                                anyadirInput("DESGCCDPNP_" + oEntry.idCampoPadre + "_" + idCentro + "_" + iLinea + "_6", oEntry.CampoDef_DesgPadre)
                                            }
                                        }
                                    }

                                    if (oEntry.getDataValue() == null || oEntry.getDataValue() == "") anyadirInput(sInput, oEntry.getValue().replace(" - ", ""))
                                    else anyadirInput(sInput, oEntry.getDataValue())

                                    anyadirInput("DCONCEPTO_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_1", oEntry.Concepto)
                                    anyadirInput("DALMACENAMIENTO_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_1", oEntry.Almacenamiento)
                                    anyadirInput("DRECEPCION_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_1", oEntry.Recepcion)
                                } else if (oEntry.tipoGS == 110 || oEntry.tipoGS == 111 || oEntry.tipoGS == 112 || oEntry.tipoGS == 113) {
                                    sInput = "DESG_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_1"
                                    anyadirInput(sInput, oEntry.getDataValue())
                                } else if (oEntry.tipoGS == 42) { //Estado actual		
                                    sInput = "DESG_" + oEntry.idCampoPadre + "_" + 1000 + "_" + iLinea + "_" + oEntry.idCampo + "_1"
                                    anyadirInput(sInput, oEntry.getDataValue())
                                } else if (oEntry.tipoGS == 118) {
                                    oEntryCodArt = null;
                                    idEntryCod = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna - 1, oEntry.nLinea, 0)

                                    if (idEntryCod) oEntryCodArt = fsGeneralEntry_getById(idEntryCod)

                                    //Comprobar si el Material esta oculto
                                    if (oEntry.dependentfield != null) {
                                        var oMat;
                                        oMat = fsGeneralEntry_getById(oEntry.dependentfield)
                                        if (!oMat) //el material est� oculto
                                        {
                                            sRe = "/fsdsentry_" + iLineaOriginal.toString() + "/"
                                            re = eval(sRe)
                                            x = oEntry.IdMaterialOculta.search(re) + sRe.length - 1
                                            y = oEntry.IdMaterialOculta.length - x
                                            idMat = oEntry.IdMaterialOculta.substr(x, y)

                                            anyadirInput("DESG_" + oEntry.idCampoPadre + "_" + idMat + "_" + iLinea + "_6", oEntry.Dependent.value)
                                            anyadirInput("DESGCCDHNP_" + oEntry.idCampoPadre + "_" + idMat + "_" + iLinea + "_6", -1000)
                                            anyadirInput("DESGCCDPNP_" + oEntry.idCampoPadre + "_" + idMat + "_" + iLinea + "_6", oEntry.CampoDef_DesgPadre)
                                            anyadirInput("DESGTIPOGS_" + oEntry.idCampoPadre + "_" + idMat + "_" + iLinea + "_6", oEntry.Dependent.tipoGS)
                                        }
                                    }

                                    if ((!oEntryCodArt) || ((oEntryCodArt) && (oEntryCodArt.tipoGS != 119))) {
                                        sRe = "/fsdsentry_" + iLineaOriginal.toString() + "/"
                                        re = eval(sRe)
                                        x = oEntry.IdCodArticuloOculta.search(re) + sRe.length - 1
                                        y = oEntry.IdCodArticuloOculta.length - x
                                        idCod = oEntry.IdCodArticuloOculta.substr(x, y)

                                        if (oEntry.codigoArticulo) {
                                            anyadirInput("DESG_" + oEntry.idCampoPadre + "_" + idCod + "_" + iLinea + "_6", oEntry.codigoArticulo)
                                            anyadirInput("DESGCCDHNP_" + oEntry.idCampoPadre + "_" + idCod + "_" + iLinea + "_6", -1000)
                                            anyadirInput("DESGCCDPNP_" + oEntry.idCampoPadre + "_" + idCod + "_" + iLinea + "_6", oEntry.CampoDef_DesgPadre)
                                        } else {
                                            anyadirInput("DESG_" + oEntry.idCampoPadre + "_" + idCod + "_" + iLinea + "_6", "nulo")
                                            anyadirInput("DESGCCDHNP_" + oEntry.idCampoPadre + "_" + idCod + "_" + iLinea + "_6", -1000)
                                            anyadirInput("DESGCCDPNP_" + oEntry.idCampoPadre + "_" + idCod + "_" + iLinea + "_6", oEntry.CampoDef_DesgPadre)
                                        }

                                        //Comprobar si la Unidad esta oculta
                                        if (oEntry.idEntryUNI != null) {
                                            oUni = fsGeneralEntry_getById(oEntry.idEntryUNI)
                                            if (!oUni) { //La Unidad est� oculta				
                                                sRe = "/fsdsentry_" + iLineaOriginal.toString() + "/"
                                                re = eval(sRe)
                                                r = oEntry.idEntryUNI.search(re) + sRe.length - 1
                                                s = oEntry.idEntryUNI.length - r
                                                idUni = oEntry.idEntryUNI.substr(r, s)
                                                if (idUni != "") {
                                                    anyadirInput("DESG_" + oEntry.idCampoPadre + "_" + idUni + "_" + iLinea + "_6", oEntry.UnidadDependent.value)
                                                    anyadirInput("DESGCCDHNP_" + oEntry.idCampoPadre + "_" + idUni + "_" + iLinea + "_6", -1000)
                                                    anyadirInput("DESGCCDPNP_" + oEntry.idCampoPadre + "_" + idUni + "_" + iLinea + "_6", oEntry.CampoDef_DesgPadre)
                                                }
                                            }
                                        }
                                    }
                                    //Comprobar si el Centro est� oculto
                                    if (oEntry.idDataEntryDependent2 != null) {
                                        oCentro = fsGeneralEntry_getById(oEntry.idDataEntryDependent2)
                                        if (!oCentro) { //La Unidad est� oculta				
                                            sRe = "/fsdsentry_" + iLineaOriginal.toString() + "/"
                                            re = eval(sRe)
                                            r = oEntry.idDataEntryDependent2.search(re) + sRe.length - 1
                                            s = oEntry.idDataEntryDependent2.length - r
                                            idCentro = oEntry.idDataEntryDependent2.substr(r, s)
                                            if (idCentro != "") {
                                                sInputCentro = anyadirInput("DESG_" + oEntry.idCampoPadre + "_" + idCentro + "_" + iLinea + "_6", oEntry.CentroDependent.value)
                                                anyadirInput("DESGCCDHNP_" + oEntry.idCampoPadre + "_" + idCentro + "_" + iLinea + "_6", -1000)
                                                anyadirInput("DESGCCDPNP_" + oEntry.idCampoPadre + "_" + idCentro + "_" + iLinea + "_6", oEntry.CampoDef_DesgPadre)
                                            }
                                        }
                                    }
                                    anyadirInput(sInput, oEntry.getDataValue())

                                    anyadirInput("DCONCEPTO_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_1", oEntry.Concepto)
                                    anyadirInput("DALMACENAMIENTO_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_1", oEntry.Almacenamiento)
                                    anyadirInput("DRECEPCION_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_1", oEntry.Recepcion)
                                } else {
                                    if (oEntry.tipoGS == 130) { //Partida
                                        anyadirInput("DPRES5_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_1", oEntry.PRES5)
                                    }

                                    if (oEntry.tipoGS == 45) { //Importe Repercutido
                                        anyadirInput(sInput, oEntry.getDataValue())
                                        anyadirInput("DMONREPER_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_1", oEntry.MonRepercutido)
                                        anyadirInput("DEQREPER_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_1", oEntry.CambioRepercutido)
                                    } else {
                                        if (oEntry.readOnly) {
                                            vValor = oEntry.dataValue
                                            if (vValor == null) {
                                                vValor = oEntry.getDataValue()
                                                if (vValor == null) vValor = ""
                                            } else {
                                                if (oEntry.tipo == 2) {
                                                    if (oEntry.dataValue.toString().indexOf(',') != -1) vValor = oEntry.dataValue.replace(",", ".")
                                                    else vValor = oEntry.getDataValue()
                                                } else vValor = oEntry.getDataValue()
                                            }
                                        } else {
                                            vValor = oEntry.dataValue
                                            if (vValor == null) {
                                                vValor = oEntry.getDataValue()
                                                if (vValor == null) vValor = ""
                                            } else {
                                                if (oEntry.tipo == 2) {
                                                    if (TieneMasDecimalesQueEsteUsuario(oEntry.dataValue, vprecisionfmt, 1)) {
                                                        formateado = num2strcalc(oEntry.dataValue, vdecimalfmt, vthousanfmt, vprecisionfmt, 1)
                                                        formateado2 = redondeacalc(oEntry.dataValue, vprecisionfmt, 1)
                                                        if ((formateado == oEntry.getValue()) || (formateado2 == oEntry.getValue())) {
                                                            if (oEntry.dataValue.toString().indexOf(',') != -1) vValor = oEntry.dataValue.replace(",", ".")
                                                            else vValor = oEntry.dataValue
                                                        } else {
                                                            vValor = oEntry.getDataValue()
                                                        }
                                                    } else {
                                                        vValor = oEntry.getDataValue()
                                                    }

                                                    if (oEntry.TablaExterna > 0) {
                                                        vValor = vValor.toString().replace(",", ".")
                                                    }
                                                    if (oEntry.intro == 1) { //combo numerico puede ser 1 o 1234,56
                                                        if (vValor != null) {
                                                            if (vValor.toString().search(',')) {
                                                                vValor = vValor.toString().replace(",", ".")
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    vValor = oEntry.getDataValue()
                                                }
                                            }
                                        }
                                        if ((oEntry.tipoGS != 2000) && (oEntry.tipoGS != 3000) && (oEntry.tipo != 16)) {
                                            anyadirInput(sInput, vValor)
                                        }
                                    }
                                }
                            };
                            //hay que obtener la l�nea con la que estaba guardado en  bd
                            sPre = oEntry.id.substr(0, x - 10)
                            //Para cuando un desglose sea no emergente para que obtenga bien el n� de linea
                            //"uwtGrupos$_ctl0x$7690$7690_275575$_1_Linea" -->Para obtener bien la linea
                            //"uwtGrupos$_ctl0x$7690$7690$275575$_1_linea" -->Antes
                            sAux = sPre.split("_")
                            var sCampoHijo = null
                            if (sAux.length == 7) {
                                sCampoHijo = sAux[sAux.length - 2]
                                if (sCampoHijo) sPre = sPre.substr(0, sPre.length - (sCampoHijo.length + 2))
                            }
                            sPre = replaceAll(sPre, "__", "::")
                            sPre = replaceAll(sPre, "_", "$")
                            sPre = replaceAll(sPre, "::", "$_")
                            var iLineaOld
                            if (!sCampoHijo) {
                                if (f.elements[sPre + "_" + iLineaOriginal + "_Linea"]) iLineaOld = f.elements[sPre + "_" + iLineaOriginal + "_Linea"].value
                                else iLineaOld = null
                            } else {
                                if (f.elements[sPre + "_" + sCampoHijo + "$_" + iLineaOriginal + "_Linea"]) iLineaOld = f.elements[sPre + "_" + sCampoHijo + "$_" + iLineaOriginal + "_Linea"].value
                                else iLineaOld = null
                            }

                            if (iLineaOld != null && Favorito == "" && InstanciaImportar == "0") {
                                //input que dir� la l�nea de bd: IdCampoPadre_Linea
                                anyadirInput("LINDESG_" + oEntry.idCampoPadre + "_" + iLinea, iLineaOld);
                                anyadirInput("LINOLDILV_" + oEntry.idCampoPadre + "_" + iLinea, iLineaOld);
                                anyadirInput("LINILV_" + oEntry.idCampoPadre + "_" + iLinea, parseInt(htControlFilasVinc[String(oEntry.idCampoPadre) + "_" + String(iLineaOld)]));
                            }

                            if (oEntry.tipo == 8) {

                                var inputFiles = document.getElementById(oEntry.id + "__hNew")
                                files = inputFiles.value.split("xx")
                                for (file in files) {
                                    sInput = "ADJUNDESG_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea.toString() + "_2_" + file.toString()
                                    anyadirInput(sInput, files[file])
                                    if ((files[file].toString() != '') && ((files[file].toString() != 'null'))) {
                                        sInput = "ADJUNDESGCCDHNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea.toString() + "_2_" + files[file].toString()
                                        anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                        sInput = "ADJUNDESGCCDPNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea.toString() + "_2_" + files[file].toString()
                                        anyadirInput(sInput, oEntry.CampoDef_DesgPadre)
                                    }
                                }
                                inputFiles = document.getElementById(oEntry.id + "__hAct")
                                files = inputFiles.value.split("xx")
                                for (file in files) {
                                    sInput = "ADJUNDESG_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea.toString() + "_1_" + file.toString()
                                    anyadirInput(sInput, files[file])
                                    if ((files[file].toString() != '') && ((files[file].toString() != 'null'))) {
                                        sInput = "ADJUNDESGCCDHNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea.toString() + "_1_" + files[file].toString()
                                        anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                        sInput = "ADJUNDESGCCDPNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea.toString() + "_1_" + files[file].toString()
                                        anyadirInput(sInput, oEntry.CampoDef_DesgPadre)
                                    }
                                }
                            }
                        }
                    } else {
                        if (oEntry.tipo == 9 && oEntry.tipoGS != 136) //desglose
                        {
                            //Si el desglose no tiene lineas. No entra en el bucle. Luego no habra inputs -> Sin registros para tabla COPIA_DESGLOSE
                            var numfilasReal = 0;
                            var numfilas = f.elements[oEntry.id + "__numTotRows"].value;
                            for (r = 1; r <= numfilas; r++) {
                                if (f.elements[oEntry.id + "__" + r + "__Deleted"]) {
                                    numfilasReal++;
                                }
                            }
                            if (numfilasReal == 0) {
                                var CampoDef;
                                for (arrCampoHijo in oEntry.arrCamposHijos) {
                                    CampoDef = oEntry.arrCamposHijos_CampoDef[arrCampoHijo].split("xx")

                                    if (CampoDef[1] != -1) { //Si no es desglose vinculado
                                        anyadirInput("DESG_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_0_1", "")
                                        anyadirInput("DESGCCDPP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_0_1", oEntry.CampoDef_CampoODesgHijo)
                                        anyadirInput("DESGCCDHP_" + oEntry.idCampo + "_" + CampoDef[0] + "_0_1", CampoDef[1])
                                    } else {
                                        anyadirInput("DSOLVINC_" + oEntry.idCampo + "_2000_0_1_" + oEntry.CampoDef_CampoODesgHijo, "1")
                                    }
                                }
                            }
                            //----
                            var i
                            i = 0;
                            for (k = 1; k <= f.elements[oEntry.id + "__numRowsVinc"].value; k++) {
                                oLineaV = f.elements[oEntry.id + "__" + k.toString() + "__LineaVinc"];
                                if (oLineaV) {
                                    if ((oLineaV.value == "-1") || (oLineaV.value == "-2")) {
                                        oLineaV.value = "0";
                                    }
                                    oLineaVM = f.elements[oEntry.id + "__" + k.toString() + "__LineaVincMovible"]
                                    if (oLineaVM) {
                                        if (oLineaVM.value == "##Nueva##") {
                                            oLineaVM.value = "##Si##"
                                        }
                                    }
                                    if ((oLineaV.value != "0") && (oLineaV.value != "-4")) {
                                        i++;
                                        oLineaOV = f.elements[oEntry.id + "__" + i.toString() + "__LineaOldVinc"]
                                        oLineaOV.value = k.toString();

                                        sInput = "LINOLDILV_" + oEntry.idCampo + "_" + i.toString()
                                        if (oLineaOV) {
                                            anyadirInput(sInput, oLineaOV.value)
                                        } else {
                                            anyadirInput(sInput, "")
                                        }

                                        sInput = "LINILV_" + oEntry.idCampo + "_" + i.toString()
                                        anyadirInput(sInput, oLineaV.value)
                                    }
                                } else {
                                    sInput = "LINILV_" + oEntry.idCampo + "_" + k.toString()
                                    anyadirInput(sInput, "0")

                                    sInput = "LINOLDILV_" + oEntry.idCampo + "_" + k.toString()
                                    anyadirInput(sInput, "")
                                }


                            };
                            //----------
                            for (k = 1; k <= f.elements[oEntry.id + "__numTotRows"].value; k++) {
                                oNoDeleted = f.elements[oEntry.id + "__" + k.toString() + "__Deleted"]

                                oLinea = f.elements[oEntry.id + "__" + k.toString() + "__Linea"]
                                if (oLinea) {
                                    //input que dir� la l�nea de bd: IdCampoPadre_<LineaOld>_Linea
                                    sInput = "LINDESG_" + oEntry.idCampo + "_" + k.toString()
                                    anyadirInput(sInput, oLinea.value)
                                }

                                var visibleArt = false;
                                for (arrCampoHijo in oEntry.arrCamposHijos) {
                                    //encontrar el copia_campo_def
                                    var CampoDef;
                                    var CampoDefHijo;
                                    for (arrCampoHijoDef in oEntry.arrCamposHijos_CampoDef) {
                                        CampoDef = oEntry.arrCamposHijos_CampoDef[arrCampoHijoDef].split("xx")
                                        if (CampoDef[0] == oEntry.arrCamposHijos[arrCampoHijo]) {
                                            CampoDefHijo = CampoDef[1];
                                            break;
                                        }
                                    }
                                    //

                                    oHijo = f.elements[oEntry.id + "__" + k.toString() + "__" + oEntry.arrCamposHijos[arrCampoHijo]]
                                    if (oHijo) {
                                        //input de desglose formato: IdCampoPadre_IdCampoHijo_Linea
                                        oHijoSrc = f.elements[oEntry.id + "__" + oEntry.arrCamposHijos[arrCampoHijo]]
                                        if (oHijoSrc) {
                                            if (Mapper == true) {
                                                if (oHijoSrc.getAttribute("calculado") == "1") { //identifica si es campo calculado
                                                    sInput = "DESGCALC_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                    anyadirInput(sInput, oHijoSrc.getAttribute("calculado"))
                                                    sInput = "DESGCALCTIT_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                    anyadirInput(sInput, oHijoSrc.title)
                                                }

                                                if (oHijoSrc.getAttribute("IdAtrib") != 0) {
                                                    sInput = "DESGAT_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                    anyadirInput(sInput, oHijoSrc.getAttribute("IdAtrib"))
                                                    sInput = "DESGERP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                    anyadirInput(sInput, oHijoSrc.getAttribute("ValErp"))
                                                }

                                                if (oHijoSrc.getAttribute("TablaExterna") > 0) {
                                                    sInput = "DESGTABLAEXT_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                    anyadirInput(sInput, oHijoSrc.getAttribute("TablaExterna"))
                                                }
                                            }

                                            if ((oHijoSrc.getAttribute("tipoGS") != 2000) && (oHijoSrc.getAttribute("tipoGS") != 3000) && (oHijoSrc.getAttribute("tipo") != 16)) { //Solic Vinculada
                                                sInput = "DESGTIPOGS_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                anyadirInput(sInput, oHijoSrc.getAttribute("tipoGS"))
                                            }

                                            if (oHijoSrc.getAttribute("tipoGS") == 42) { //Estado actual	
                                                sInput = "DESGCCDHP_" + oEntry.idCampo + "_" + 1000 + "_" + k.toString() + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_1"
                                                anyadirInput(sInput, CampoDefHijo)
                                                sInput = "DESGCCDPP_" + oEntry.idCampo + "_" + 1000 + "_" + k.toString() + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_1"
                                                anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                            } else {
                                                sInput = "DESGCCDHP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                anyadirInput(sInput, CampoDefHijo)
                                                sInput = "DESGCCDPP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                            }

                                            if (oHijoSrc.getAttribute("intro") == 2) {
                                                sInput = "DESGID_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                anyadirInput(sInput, oHijo.value)
                                                sInput = "DESG_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                anyadirInput(sInput, oHijo.value)
                                            }

                                            if (oHijoSrc.getAttribute("intro") == 1 && (oHijoSrc.getAttribute("tipo") == 1 || oHijoSrc.getAttribute("tipo") == 3 || oHijoSrc.getAttribute("tipo") == 5 || oHijoSrc.getAttribute("tipo") == 6 || oHijoSrc.getAttribute("tipo") == 7) && (oHijoSrc.getAttribute("tipoGS") == 0 || oHijoSrc.getAttribute("tipoGS") == 46)) {
                                                sInput = "DESGID_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                anyadirInput(sInput, oHijo.value)

                                                sInput = "DESG_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                if (((oHijoSrc.getAttribute("tipo") == 1) || (oHijoSrc.getAttribute("tipo") == 5) || (oHijoSrc.getAttribute("tipo") == 6)) && (oHijoSrc.getAttribute("tipoGS") == 0)) {
                                                    oDescrComboPopUp = f.elements[oHijo.id + "_DescrComboPopUp"]
                                                    if (oDescrComboPopUp) anyadirInput(sInput, oDescrComboPopUp.value)
                                                    else //invisible
                                                        anyadirInput(sInput, oHijo.value)
                                                } else anyadirInput(sInput, oHijo.value)
                                            } else {
                                                sInput = "DESG_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()

                                                oAdjun = f.elements[oEntry.id + "__" + k.toString() + "__" + oEntry.arrCamposHijos[arrCampoHijo] + "_hNew"]
                                                if (oAdjun) {
                                                    oAdjunNombre = f.elements[oEntry.id + "__" + k.toString() + "__" + oEntry.arrCamposHijos[arrCampoHijo] + "_hNombre"]
                                                    if (oAdjunNombre) anyadirInput(sInput, oAdjunNombre.value)
                                                    else anyadirInput(sInput, "")
                                                    files = oHijo.value.split("xx")
                                                    for (file in files) {
                                                        if (Favorito == "1") //si es favorito estamos en el alta
                                                            sInput = "ADJUNDESG_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + i.toString() + "_2_" + file.toString()
                                                        else sInput = "ADJUNDESG_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + i.toString() + "_1_" + file.toString()
                                                        anyadirInput(sInput, files[file])

                                                        if ((files[file].toString() != '') && ((files[file].toString() != 'null'))) {
                                                            if (Favorito == "1") //si es favorito estamos en el alta
                                                                sInput = "ADJUNDESGCCDHP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + i.toString() + "_2_" + files[file].toString()
                                                            else sInput = "ADJUNDESGCCDHP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + i.toString() + "_1_" + files[file].toString()
                                                            anyadirInput(sInput, CampoDefHijo)

                                                            if (Favorito == "1") //si es favorito estamos en el alta
                                                                sInput = "ADJUNDESGCCDPP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + i.toString() + "_2_" + files[file].toString()
                                                            else sInput = "ADJUNDESGCCDPP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + i.toString() + "_1_" + files[file].toString()
                                                            anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                                        }
                                                    }
                                                    files = oAdjun.value.split("xx")
                                                    for (file in files) {
                                                        sInput = "ADJUNDESG_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_2_" + file.toString()
                                                        anyadirInput(sInput, files[file])

                                                        if ((files[file].toString() != '') && ((files[file].toString() != 'null'))) {
                                                            sInput = "ADJUNDESGCCDHP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_2_" + files[file].toString()
                                                            anyadirInput(sInput, CampoDefHijo)
                                                            sInput = "ADJUNDESGCCDPP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_2_" + files[file].toString()
                                                            anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                                        }
                                                    }
                                                } else if (oHijoSrc.getAttribute("tipoGS") == 110 || oHijoSrc.getAttribute("tipoGS") == 111 || oHijoSrc.getAttribute("tipoGS") == 112 || oHijoSrc.getAttribute("tipoGS") == 113) { //los presupuestos 
                                                    anyadirInput("DESG_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_1", oHijo.value)
                                                } else if (oHijoSrc.getAttribute("tipoGS") == 42) { //Estado actual	
                                                    sInput = "DESG_" + oEntry.idCampo + "_" + 1000 + "_" + k.toString() + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_1"
                                                    anyadirInput(sInput, oHijo.value)
                                                } else if (oHijoSrc.getAttribute("tipoGS") == 119) //Codigo de Articulo (Nuevas Solicitudes)
                                                {
                                                    anyadirInput(sInput, oHijo.value);
                                                    if ((oHijo.value != null) && (oHijo.value != "")) {
                                                        visibleArt = true;
                                                    }
                                                } else if (oHijoSrc.getAttribute("tipoGS") == 118) {
                                                    if ((oHijo.codigoArticulo != null) && (oHijo.codigoArticulo != "")) {
                                                        sInputCodArt = "DESG_" + oEntry.idCampo + "_" + (oEntry.arrCamposHijos[arrCampoHijo - 1]) + "_" + k.toString() + "_6"
                                                        var fSubmit = document.forms["frmSubmit"] //Hay que obtener el input del Articulo que se va a pasar,
                                                        if (!fSubmit.elements.item(sInputCodArt)) // anteriormente se ha pasado el articulo aun estando invisible.
                                                            anyadirInput(sInputCodArt, oHijo.codigoArticulo)
                                                        else fSubmit.elements.item(sInputCodArt).value = oHijo.codigoArticulo

                                                    } else if ((!oHijo.articuloGenerico) && (oHijo.DenArticuloModificado) && (!visibleArt)) {
                                                        sInputCodArt = "DESG_" + oEntry.idCampo + "_" + (oEntry.arrCamposHijos[arrCampoHijo - 1]) + "_" + k.toString() + "_6"
                                                        var fSubmit = document.forms["frmSubmit"] //Hay que obtener el input del Articulo que se va a pasar,
                                                        if (!fSubmit.elements.item(sInputCodArt)) // anteriormente se ha pasado el articulo aun estando invisible.
                                                            anyadirInput(sInputCodArt, "nulo")
                                                        else fSubmit.elements.item(sInputCodArt).value = "nulo"
                                                    }

                                                    anyadirInput(sInput, oHijo.value);
                                                    visibleArt = false;
                                                } else if (oHijoSrc.getAttribute("tipoGS") == 104) {
                                                    anyadirInput(sInput, oHijo.value)
                                                } else {
                                                    if (oHijoSrc.getAttribute("tipoGS") == 130) { //Partida
                                                        sInputPres5 = "DPRES5_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_1"
                                                        anyadirInput(sInputPres5, oHijo.PRES5)
                                                    }

                                                    if (oHijoSrc.getAttribute("tipoGS") == 45) { //Importe Repercutido
                                                        anyadirInput(sInput, oHijo.value)

                                                        oRepercutido = f.elements[oHijo.id + "_MonRepercutido"]
                                                        sInputMon = "DMONREPER_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_1"
                                                        anyadirInput(sInputMon, oRepercutido.value)

                                                        oRepercutido = f.elements[oHijo.id + "_CambioRepercutido"]
                                                        sInputEquiv = "DEQREPER_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_1"
                                                        anyadirInput(sInputEquiv, oRepercutido.value)
                                                    } else if (oHijoSrc.getAttribute("tipoGS") == 2000) { //Solic Vinculada
                                                        oSolicVinculada = f.elements[oHijo.id + "_IdSolicVinculada"]
                                                        sInputVinc = "DSOLVINC_" + oEntry.idCampo + "_2000_" + k.toString() + "_1_" + oEntry.CampoDef_CampoODesgHijo
                                                        anyadirInput(sInputVinc, oSolicVinculada.value)
                                                    } else {
                                                        anyadirInput(sInput, oHijo.value)
                                                        visibleArt = false;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } //for
                            //En LINILV_ debe ir la posicion en INSTANCIA_LINEAS_VINCULAR al entrar � en la anterior grabaci�n (tras un guardado)
                            //pq se hace updates en bbdd-> en versi�n 2 linea x era linea origen x, en versi�n 3 tras mover 2 filas y eliminar 1 fila
                            //sera linea origen x-3.
                            i = 0
                            for (k = 1; k <= f.elements[oEntry.id + "__numRowsVinc"].value; k++) {
                                oLineaV = f.elements[oEntry.id + "__" + k.toString() + "__LineaVinc"];
                                if (oLineaV) {
                                    if ((oLineaV.value != "0") && (oLineaV.value != "-4")) {
                                        i++;
                                        oLineaV.value = i.toString();
                                    }
                                }
                            }
                        } //desglose
                        else {
                            if (oEntry.tipo == 8) {

                                var inputFiles = document.getElementById(oEntry.id + "__hNew")
                                files = inputFiles.value.split("xx")
                                for (file in files) {
                                    sInput = "ADJUN_" + oEntry.idCampo + "_2_" + file.toString()
                                    anyadirInput(sInput, files[file])
                                    if ((files[file].toString() != '') && ((files[file].toString() != 'null'))) {
                                        sInput = "ADJUNCCD_" + oEntry.idCampo + "_2_" + files[file].toString()
                                        anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                    }
                                }
                                inputFiles = document.getElementById(oEntry.id + "__hAct")
                                files = inputFiles.value.split("xx")
                                for (file in files) {
                                    if (oEntry.tipoGS == 135) { //Archivo de contrato
                                        sInput = "ADJUN_" + oEntry.idCampo + "_5_" + file.toString()
                                        anyadirInput(sInput, files[file])
                                        if ((files[file].toString() != '') && ((files[file].toString() != 'null'))) {
                                            sInput = "ADJUNCCD_" + oEntry.idCampo + "_5_" + files[file].toString()
                                            anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                        }
                                    } else {
                                        sInput = "ADJUN_" + oEntry.idCampo + "_1_" + file.toString()
                                        anyadirInput(sInput, files[file])
                                        if ((files[file].toString() != '') && ((files[file].toString() != 'null'))) {
                                            sInput = "ADJUNCCD_" + oEntry.idCampo + "_1_" + files[file].toString()
                                            anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)

                                            comentFile = document.getElementById(oEntry.id + "__text" + files[file])
                                            if (comentFile) {
                                                if (comentFile.value != '') {
                                                    sInput = "ADJUNCOMENT_" + oEntry.idCampo + "_1_" + files[file].toString()
                                                    anyadirInput(sInput, comentFile.value)
                                                }
                                            }
                                        }
                                    }
                                }
                                sInput = "CAMPO_" + oEntry.idCampo + "_" + oEntry.tipo.toString()
                                anyadirInput(sInput, oEntry.getValue())

                                sInput = "CAMPOCCD_" + oEntry.idCampo + "_" + oEntry.tipo.toString()
                                anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                            } else if (oEntry.idCampo == "PROVE" || oEntry.idCampo == "SOLICIT" || oEntry.idCampo == "CON") //proveedores a los que emitir el certificado
                            {
                                //uwtGrupos__ctl0xx__ctl0_fsdsentry_2_CON
                                re = /fsdsentry/
                                x = oEntry.id.search(re) + 10
                                y = oEntry.id.search(oEntry.idCampo) - 1

                                if (oEntry.id.search(re) >= 0 && oEntry.idCampo != "SOLICIT") //y no es el input base del desglose
                                {
                                    iFila = oEntry.id.substr(x, y - x)
                                    sSolicit = oEntry.id.substr(0, y) + "_SOLICIT"
                                    oSolicit = fsGeneralEntry_getById(sSolicit)
                                    if (oSolicit) if (oSolicit.getValue() == 1) {
                                        sInput = oEntry.idCampo + "_" + iFila.toString()
                                        anyadirInput(sInput, oEntry.getDataValue())

                                    }
                                }
                            } else //campos simples
                            {
                                if (oEntry.idCampo != '') {
                                    sInput = "CAMPOSAVEVALUES_" + oEntry.idCampo
                                    anyadirInput(sInput, oEntry.RecordarValores.toString())
                                }

                                if (Mapper == true && oEntry.IdAtrib != 0) {
                                    sInput = "CAMPOGRUPO_" + oEntry.idCampo
                                    anyadirInput(sInput, oEntry.Grupo)
                                    sInput = "CAMPOORDEN_" + oEntry.idCampo
                                    anyadirInput(sInput, oEntry.orden)
                                    sInput = "CAMPOAT_" + oEntry.idCampo
                                    anyadirInput(sInput, oEntry.IdAtrib)
                                    sInput = "CAMPOERP_" + oEntry.idCampo
                                    anyadirInput(sInput, oEntry.ValErp)
                                }

                                //La condici�n oEntry.idCampo!="" se incluye para no incluir campos de tipo persona no pertenecientes al formulario (ej. pesta�a participantes)
                                if (!(oEntry.tipoGS == 116 || oEntry.tipoGS == 117) && (oEntry.idCampo != "")) { //Participantes y Rol                             
                                    sInput = "CAMPOTIPOGS_" + oEntry.idCampo;
                                    anyadirInput(sInput, oEntry.tipoGS);
                                    sInput = "CAMPOCCD_" + oEntry.idCampo;
                                    anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo);
                                }                                
                              
                                sInput = "CAMPO_" + oEntry.idCampo + "_" + oEntry.tipo.toString()

                                //2 -lista externa
                                if (oEntry.intro == 2) {
                                    anyadirInput(sInput, FormatearListaExterna(oEntry.getValue()))
                                    sInput = "CAMPOID_" + oEntry.idCampo + "_" + oEntry.tipo.toString()
                                    anyadirInput(sInput, oEntry.getDataValue())
                                }

                                if (oEntry.intro == 1 && (oEntry.tipo == 1 || oEntry.tipo == 3 || oEntry.tipo == 5 || oEntry.tipo == 6 || oEntry.tipo == 7) && (oEntry.tipoGS == 0 || oEntry.tipoGS == 46 || oEntry.tipoGS == 148 || oEntry.tipoGS == 150)) {
                                    anyadirInput(sInput, oEntry.getValue())
                                    sInput = "CAMPOID_" + oEntry.idCampo + "_" + oEntry.tipo.toString()
                                    anyadirInput(sInput, oEntry.getDataValue())
                                } else if (oEntry.tipoGS == 104) //CodArticulo Solicitudes anteriores
                                {
                                    if (oEntry.dependentfield != null) {
                                        oMat = fsGeneralEntry_getById(oEntry.dependentfield)
                                        if (!oMat) //el material est� oculto
                                        {
                                            re = /fsentry/
                                            r = oEntry.dependentfield.search(re) + 7
                                            s = oEntry.dependentfield.length - r
                                            idMat = oEntry.dependentfield.substr(r, s)

                                            anyadirInput("CAMPO_" + idMat + "_6", oEntry.Dependent.value);
                                            anyadirInput("CAMPOTIPOGS_" + idMat + "_6", oEntry.Dependent.tipoGS);
                                            anyadirInput("CAMPOCCD_" + idMat, -1000);
                                        }
                                    }
                                    if (oEntry.getDataValue() == null || oEntry.getDataValue() == "") {
                                        s = oEntry.getValue()
                                        s = s.replace(" - ", "")
                                        anyadirInput(sInput, s)
                                    } else anyadirInput(sInput, oEntry.getDataValue())

                                    if (Mapper == true) {
                                        sInput = "CAMPOGRUPO_" + oEntry.idCampo
                                        anyadirInput(sInput, oEntry.Grupo)
                                        sInput = "CAMPOORDEN_" + oEntry.idCampo
                                        anyadirInput(sInput, oEntry.orden)
                                    }
                                    //Si la UNIDAD esta oculta									
                                    if (oEntry.idEntryUNI != null) {
                                        oUni = fsGeneralEntry_getById(oEntry.idEntryUNI)
                                        if (!oUni) { //La Unidad est� oculta										
                                            re = /fsentry/
                                            r = oEntry.idEntryUNI.search(re) + 7
                                            s = oEntry.idEntryUNI.length - r
                                            idUni = oEntry.idEntryUNI.substr(r, s)
                                            if (idUni != "") {
                                                anyadirInput("CAMPO_" + idUni + "_6", oEntry.UnidadDependent.value);
                                                anyadirInput("CAMPOCCD_" + idUni, -1000);
                                            }
                                        }
                                    }
                                } else if (oEntry.tipoGS == 119) //CodArticulo Nuevas Solicitudes
                                {
                                    if (oEntry.dependentfield != null) {
                                        oMat = fsGeneralEntry_getById(oEntry.dependentfield)
                                        if (!oMat) //el material est� oculto
                                        {
                                            re = /fsentry/
                                            r = oEntry.dependentfield.search(re) + 7;
                                            s = oEntry.dependentfield.length - r;
                                            idMat = oEntry.dependentfield.substr(r, s);
                                            anyadirInput("CAMPO_" + idMat + "_6", oEntry.Dependent.value);
                                            anyadirInput("CAMPOTIPOGS_" + idMat, oEntry.Dependent.tipoGS);
                                            anyadirInput("CAMPOCCD_" + idMat, -1000);
                                            if (Mapper == true) {
                                                anyadirInput("CAMPOGRUPO_" + idMat, oEntry.Grupo);
                                                anyadirInput("CAMPOORDEN_" + idMat, oEntry.orden);
                                            }

                                        }
                                        oDen = null;
                                        idEntryDen = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna + 1, oEntry.nLinea, 0)
                                        if (idEntryDen) oDen = fsGeneralEntry_getById(idEntryDen)
                                        if ((!oDen) || ((oDen) && (oDen.tipoGS != 118)) && (oEntry.tag != null)) { //La denominacion esta oculta
                                            re = /fsentry/
                                            x = oEntry.dependentfield.search(re) + 7
                                            y = oEntry.dependentfield.length - x
                                            idDen = oEntry.dependentfield.substr(x, y)

                                            anyadirInput("CAMPO_" + idDen + "_6", oEntry.tag);
                                            anyadirInput("CAMPOCCD_" + idDen, -1000);
                                        }
                                    }
                                    //Si la UNIDAD esta oculta									
                                    if (oEntry.idEntryUNI != null) {
                                        oUni = fsGeneralEntry_getById(oEntry.idEntryUNI)
                                        if (!oUni) { //La Unidad est� oculta										
                                            re = /fsentry/
                                            r = oEntry.idEntryUNI.search(re) + 7
                                            s = oEntry.idEntryUNI.length - r
                                            idUni = oEntry.idEntryUNI.substr(r, s)
                                            if (idUni != '') {
                                                anyadirInput("CAMPO_" + idUni + "_6", oEntry.UnidadDependent.value);
                                                anyadirInput("CAMPOCCD_" + idUni, -1000);
                                            }
                                        }

                                    }
                                    if (oEntry.getDataValue() == null || oEntry.getDataValue() == "") {
                                        s = oEntry.getValue()
                                        s = s.replace(" - ", "")
                                        anyadirInput(sInput, s)
                                    } else anyadirInput(sInput, oEntry.getDataValue())

                                    if (Mapper == true) {
                                        anyadirInput("CAMPOGRUPO_" + oEntry.idCampo, oEntry.Grupo);
                                        anyadirInput("CAMPOORDEN_" + oEntry.idCampo, oEntry.orden);
                                    }

                                    anyadirInput("CONCEPTO_" + oEntry.idCampo, oEntry.Concepto);
                                    anyadirInput("ALMACENAMIENTO_" + oEntry.idCampo, oEntry.Almacenamiento);
                                    anyadirInput("RECEPCION_" + oEntry.idCampo, oEntry.Recepcion);
                                } else if (oEntry.tipoGS == 115 || oEntry.tipoGS == 116) //Participantes
                                {
                                    if (oEntry.intro == 1) {
                                        oRol = fsGeneralEntry_getById(oEntry.dependentfield)
                                        sRol = oRol.getDataValue()
                                        sInput = "PART_" + oEntry.tipoGS.toString() + "_" + sRol
                                        anyadirInput(sInput, oEntry.getDataValue())
                                    } else {
                                        sInput = "CAMPO_" + oEntry.idCampo + "_" + oEntry.tipo.toString()
                                        anyadirInput(sInput, oEntry.getDataValue())
                                        sInput = "CAMPOCCD_" + oEntry.idCampo
                                        anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                    }
                                } else if (oEntry.tipoGS == 117) //Rol
                                { } else if (oEntry.tipoGS == 110 || oEntry.tipoGS == 111 || oEntry.tipoGS == 112 || oEntry.tipoGS == 113) { //los presupuestos 
                                    anyadirInput("CAMPO_" + oEntry.idCampo + "_1", oEntry.getDataValue())
                                    if (oEntry.tipoGS == 113 && Mapper == true) {
                                        anyadirInput("CAMPOGRUPO_" + oEntry.idCampo, oEntry.Grupo);
                                        anyadirInput("CAMPOORDEN_" + oEntry.idCampo, oEntry.orden);
                                    }
                                } else if (oEntry.tipoGS == 118) //Denominacion Articulo									
                                {
                                    oEntryCodArt = null;
                                    idEntryCod = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna - 1, oEntry.nLinea, 0)
                                    if (idEntryCod) oEntryCodArt = fsGeneralEntry_getById(idEntryCod)
                                    if ((!oEntryCodArt) || ((oEntryCodArt) && (oEntryCodArt.tipoGS != 119))) {
                                        re = /fsentry/
                                        x = oEntry.IdCodArticuloOculta.search(re) + 7
                                        y = oEntry.IdCodArticuloOculta.length - x
                                        idCod = oEntry.IdCodArticuloOculta.substr(x, y)

                                        if ((oEntry.DenArticuloModificado) && (!oEntry.articuloGenerico)) {
                                            //Si el Codigo de Articulo esta oculto, se trata de un articulo NO Generico y se ha modificado la denominacion
                                            //eliminar el codigo de articulo
                                            sInputCodArt = "CAMPO_" + idCod + "_6"
                                            anyadirInput(sInputCodArt, null)
                                        } else if (oEntry.codigoArticulo) {
                                            sInputCodArt = "CAMPO_" + idCod + "_6"
                                            anyadirInput(sInputCodArt, oEntry.codigoArticulo)
                                        }

                                        anyadirInput("CAMPOCCD_" + idCod, -1000);
                                        oEntryMat = null;
                                        idEntryMat = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna - 2, oEntry.nLinea, 0)
                                        if (idEntryMat) oEntryMat = fsGeneralEntry_getById(idEntryMat)
                                        if ((!oEntryMat) || ((oEntryMat) && (oEntryMat.tipoGS != 103))) {
                                            re = /fsentry/
                                            x = oEntry.IdMaterialOculta.search(re) + 7
                                            y = oEntry.IdMaterialOculta.length - x
                                            idMat = oEntry.IdMaterialOculta.substr(x, y)

                                            //Si la Estructura del Material tambien esta oculta...
                                            anyadirInput("CAMPO_" + idMat + "_6", oEntry.Dependent.value);
                                            anyadirInput("CAMPOCCD_" + idMat, -1000);
                                        }
                                        //Si el Codigo de Articulo esta oculto, mirar Si la UNIDAD esta oculta									
                                        if (oEntry.idEntryUNI != null) {
                                            oUni = fsGeneralEntry_getById(oEntry.idEntryUNI)
                                            if (!oUni) { //La Unidad est� oculta										
                                                re = /fsentry/
                                                r = oEntry.idEntryUNI.search(re) + 7;
                                                s = oEntry.idEntryUNI.length - r;
                                                idUni = oEntry.idEntryUNI.substr(r, s);
                                                if (idUni != "") {
                                                    anyadirInput("CAMPO_" + idUni + "_6", oEntry.UnidadDependent.value)
                                                    anyadirInput("CAMPOCCD_" + idUni, -1000)
                                                }
                                            }
                                        }
                                    }
                                    anyadirInput(sInput, oEntry.getDataValue());

                                    anyadirInput("CONCEPTO_" + oEntry.idCampo, oEntry.Concepto);
                                    anyadirInput("ALMACENAMIENTO_" + oEntry.idCampo, oEntry.Almacenamiento);
                                    anyadirInput("RECEPCION_" + oEntry.idCampo, oEntry.Recepcion);
                                } else if (oEntry.tipoGS == 128) { // RefSolicitud 
                                    anyadirInput(sInput, oEntry.getDataValue());
                                    anyadirInput("GEN_IDREFSOL", oEntry.getDataValue());
                                    anyadirInput("GEN_AVISOBLOQUEO", oEntry.avisoBloqueoRefSOL);
                                } else if (oEntry.tipoGS == 123) { //Organizacion de compras
                                    if (oEntry.idDataEntryDependent2 != null) {
                                        oCentro = fsGeneralEntry_getById(oEntry.idDataEntryDependent2)
                                        if (!oCentro) //el material est� oculto
                                        {
                                            re = /fsentry/
                                            r = oEntry.idDataEntryDependent2.search(re) + 7;
                                            s = oEntry.idDataEntryDependent2.length - r;
                                            idCentro = oEntry.idDataEntryDependent2.substr(r, s);
                                            if (oEntry.CentroDependent) anyadirInput("CAMPO_" + idCentro + "_6", oEntry.CentroDependent.value)
                                        }
                                    }

                                    anyadirInput(sInput, oEntry.getDataValue());
                                } else {

                                    if (oEntry.tipoGS == 130) { //Partida
                                        anyadirInput("PRES5_" + oEntry.idCampo, oEntry.PRES5);
                                    }

                                    //los proveedores, los centros de coste, cantidad y precio unitario
                                    if (oEntry.tipoGS == 100 || oEntry.tipoGS == 129 || oEntry.tipoGS == 4 || oEntry.tipoGS == 9) {
                                        if (Mapper == true) {
                                            anyadirInput("CAMPO_" + oEntry.idCampo + "_" + oEntry.tipo.toString(), oEntry.getDataValue());
                                            anyadirInput("CAMPOGRUPO_" + oEntry.idCampo, oEntry.Grupo);
                                            anyadirInput("CAMPOORDEN_" + oEntry.idCampo, oEntry.orden);
                                        }

                                        if ((oEntry.tipoGS == 100) && (oEntry.esProveedorParticipante)) { //Si es un proveedor participante se han seleccionado el/los contacto/s
                                            if (oEntry.Contactos != '') {
                                                anyadirInput("CAMPOCONTACTOS_" + oEntry.idCampo, oEntry.Contactos);
                                            }
                                        }
                                    }

                                    if (oEntry.tipoGS == 132) { //Tipo de pedido
                                        anyadirInput("ALMACENAMIENTO_" + oEntry.idCampo, oEntry.Almacenamiento);
                                        anyadirInput("CONCEPTO_" + oEntry.idCampo, oEntry.Concepto);
                                    }

                                    if (oEntry.tipoGS == 45) { //Importe Repercutido
                                        anyadirInput(sInput, oEntry.getValue());
                                        anyadirInput("MONREPER_" + oEntry.idCampo, oEntry.MonRepercutido);
                                        anyadirInput("EQREPER_" + oEntry.idCampo, oEntry.CambioRepercutido);
                                    } else {
                                        sInput = "CAMPO_" + oEntry.idCampo + "_" + oEntry.tipo.toString()
                                        if (oEntry.readOnly) {
                                            vValor = oEntry.dataValue
                                            if (vValor == null) vValor = ""
                                            else {
                                                if (oEntry.tipo == 2) {
                                                    if (oEntry.dataValue.toString().indexOf(',') != -1) vValor = oEntry.dataValue.replace(",", ".")
                                                    else vValor = oEntry.getDataValue()
                                                } else vValor = oEntry.getDataValue()
                                            }

                                        } else {
                                            vValor = oEntry.dataValue
                                            if (vValor == null) {
                                                vValor = oEntry.getDataValue()
                                                if (vValor == null) vValor = ""
                                            } else {
                                                if (oEntry.tipo == 2) {
                                                    if (TieneMasDecimalesQueEsteUsuario(oEntry.dataValue, vprecisionfmt, 1)) {
                                                        formateado = num2strcalc(oEntry.dataValue, vdecimalfmt, vthousanfmt, vprecisionfmt, 1)
                                                        formateado2 = redondeacalc(oEntry.dataValue, vprecisionfmt, 1)
                                                        if ((formateado == oEntry.getValue()) || (formateado2 == oEntry.getValue())) {
                                                            if (oEntry.dataValue.toString().indexOf(',') != -1) vValor = oEntry.dataValue.replace(",", ".")
                                                            else vValor = oEntry.dataValue
                                                        } else {
                                                            vValor = oEntry.getDataValue()
                                                        }
                                                    }
                                                    else {
                                                        vValor = oEntry.getDataValue()
                                                    }

                                                    if (oEntry.intro == 1) { //combo numerico puede ser 1 o 1234,56
                                                        if (vValor != null) {
                                                            if (vValor.toString().search(',')) {
                                                                vValor = vValor.toString().replace(",", ".")
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    vValor = oEntry.getDataValue()
                                                }
                                            }
                                            if (oEntry.TablaExterna > 0) {
                                                if (Mapper == true) {
                                                    anyadirInput("CAMPOTABLAEXT_" + oEntry.idCampo, oEntry.TablaExterna)
                                                    anyadirInput("CAMPOGRUPO_" + oEntry.idCampo, oEntry.Grupo)
                                                    anyadirInput("CAMPOORDEN_" + oEntry.idCampo, oEntry.orden)
                                                }

                                                vValor = vValor.replace(",", ".")
                                            }
                                        }
                                        anyadirInput(sInput, vValor)
                                    }
                                }
                            }
                        }
                    }
            }
        }
    };
    //si es una no conformidad y ha solicitado acciones:
    if (typeof (arrAcciones) != "undefined") {
        for (arrAccion in arrAcciones) {
            oEntry = fsGeneralEntry_getById(arrAcciones[arrAccion])
            if (oEntry) {
                anyadirInput("ACCION_" + oEntry.idCampo + "_" + oEntry.tipo.toString() + "_" + oEntry.CampoDef_DesgPadre.toString(), oEntry.getDataValue())
            }
        }
    };
};
/*
''' <summary>
''' Determina si el texto de Aprobar o Rechazar en el idioma del usuario.
''' </summary>
''' <param name="desglose">desglose</param>
''' <param name="index">linea del desglose</param>        
''' <remarks>Llamada desde: jsAlta.js/aprobarAccion jsAlta.js/rechazarAccion; Tiempo m�ximo: 0</remarks>*/
function obtenerNombreAccion(desglose, index) {
    for (i = 0; i < document.getElementById(desglose + "_tblDesgloseHidden").rows[0].cells.length; i++) {
        s = document.getElementById(desglose + "_tblDesgloseHidden").rows[0].cells[i].innerHTML
        re = /fsentry/

        x = s.search(re)
        y = s.search("__tbl")

        id = s.substr(x + 7, y - x - 7)

        oEntry = fsGeneralEntry_getById(desglose + "_fsentry" + id)

        if (oEntry) {
            if (oEntry.tipoGS == 35) {
                oDSEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + id)
                sTitulo = oDSEntry.getValue()
                break;
            }
        }
    }
    return sTitulo
};
function aprobarAccion(e, desglose, index) {
    var arrDesglose = desglose.split("_")
    var idGrupo
    if (arrDesglose.length == 4) idGrupo = arrDesglose[arrDesglose.length - 1]
    else if (arrDesglose.length > 1) idGrupo = arrDesglose[arrDesglose.length - 2]
    else idGrupo = desglose

    if (idGrupo == desglose) idGrupo = ""
    oEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + idGrupo.toString() + "1010")
    if (oEntry.getDataValue() != 2 && oEntry.getDataValue() != 5) //si ya est� aprobada o finalizada y revisada no hace nada
    {
        var sTitulo = obtenerNombreAccion(desglose, index)
        if (sTitulo.length > 200) {
            sTitulo = sTitulo.substr(sTitulo, 200) + '...'
        }
        var newWindow = window.open("../noconformidad/aprobRechazoAccion.aspx?Aprobar=true&desglose=" + desglose + "&index=" + index + "&DenAccion=" + Var2Param(sTitulo), "_blank", "width=600,height=500,status=no,resizable=no,top=200,left=200,menubar=no")
        newWindow.focus();
    }
};
function rechazarAccion(e, desglose, index) {
    var arrDesglose = desglose.split("_")
    var idGrupo
    if (arrDesglose.length == 4) idGrupo = arrDesglose[arrDesglose.length - 1]
    else if (arrDesglose.length > 1) idGrupo = arrDesglose[arrDesglose.length - 2]
    else idGrupo = desglose

    if (idGrupo == desglose) idGrupo = ""

    oEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + idGrupo.toString() + "1010")
    if (oEntry.getDataValue() != 3) //si ya est� rechazada no hace nada
    {
        var sTitulo = obtenerNombreAccion(desglose, index)
        if (sTitulo.length > 200) {
            sTitulo = sTitulo.substr(sTitulo, 200) + '...'
        }        
        var newWindow = window.open("../noconformidad/aprobRechazoAccion.aspx?Aprobar=false&desglose=" + desglose + "&index=" + index + "&DenAccion=" + Var2Param(sTitulo), "_blank", "width=600,height=500,status=no,resizable=no,top=200,left=200,menubar=no")
        newWindow.focus();
    }
};
function gestionarCambioEstadoAccion(bAprobar, desglose, index, comentario) {
    var arrDesglose = desglose.split("_")
    if (arrDesglose.length == 4) idGrupo = arrDesglose[arrDesglose.length - 1]
    else if (arrDesglose.length > 1) idGrupo = arrDesglose[arrDesglose.length - 2]
    else idGrupo = desglose

    if (idGrupo == desglose) idGrupo = ""

    //A�ade el comentario:
    oEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + idGrupo.toString() + "1020")
    oEntry.setValue(comentario)
    oEntry.setDataValue(comentario)


    //Modifica el estado interno
    oEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + idGrupo.toString() + "1010")

    if (bAprobar == 1) {
        if (oEntry.getDataValue() == 1) //est� SinRevisar y pasa Aprobada
        {
            oEntry.setValue(arrEstadosInternos[2][1])
            oEntry.setDataValue(2)
        } else {
            if (oEntry.getDataValue() == 4) //est� FinalizadaSinRevisar y pasa a FinalizadaRevisada
            {
                oEntry.setValue(arrEstadosInternos[5][1])
                oEntry.setDataValue(5)
            } else {
                if (oEntry.getDataValue() == 3) //est� rechazada y pasa a Aprobada
                {
                    oEntry.setValue(arrEstadosInternos[2][1])
                    oEntry.setDataValue(2)
                }
            }
        }
        if (document.getElementById(desglose + "_cmdAprobarAccion_" + index.toString())) document.getElementById(desglose + "_cmdAprobarAccion_" + index.toString()).style.visibility = "hidden"
        if (document.getElementById(desglose + "_cmdRechazarAccion_" + index.toString())) document.getElementById(desglose + "_cmdRechazarAccion_" + index.toString()).style.visibility = "visible"
    } else {
        if (oEntry.getDataValue() == 5) //est� FinalizadaRevisada y pasa Rechazada
        {
            oEntry.setValue(arrEstadosInternos[3][1])
            oEntry.setDataValue(3)
        } else {
            if (oEntry.getDataValue() == 1) //est� SinRevisar y pasa a Rechazada
            {
                oEntry.setValue(arrEstadosInternos[3][1])
                oEntry.setDataValue(3)
            } else {
                if (oEntry.getDataValue() == 4) //est� FinalizadaSinRevisar y pasa a rechazada
                {
                    oEntry.setValue(arrEstadosInternos[3][1])
                    oEntry.setDataValue(3)
                } else {
                    if (oEntry.getDataValue() == 2) //est� aprobada y pasa a rechazada
                    {
                        oEntry.setValue(arrEstadosInternos[3][1])
                        oEntry.setDataValue(3)
                    }
                }
            }
        }
        if (document.getElementById(desglose + "_cmdAprobarAccion_" + index.toString())) document.getElementById(desglose + "_cmdAprobarAccion_" + index.toString()).style.visibility = "visible"
        if (document.getElementById(desglose + "_cmdRechazarAccion_" + index.toString())) document.getElementById(desglose + "_cmdRechazarAccion_" + index.toString()).style.visibility = "hidden"

    }
    //HA HABIDO UN CAMBIO DE ESTADO POR LO QUE HAY QUE NOTIFICAR
    document.getElementById("Notificar").value = 1
};
/*
''' <summary>
''' Validar fechas de suministro
'''	''' </summary>
''' <param name="oEdit">Campo</param> 
''' <param name="oldValue">Valor que tenia el entry anteriormente</param> 
''' <param name="oEvent"></param> 
''' <returns>cuando alguna de las fechas cambia</returns>
''' <remarks>Llamada desde:GeneralEntry.vb ; Tiempo m�ximo:0 </remarks>
*/
function ddFechasSuministro_ValueChange(oEdit, oldValue, oEvent) {
    if (oEdit.fsEntry.tipoGS == 6) { //El campo es INICIO DE SUMINISTRO
        //Mirar si la fecha de FIN de SUMINISTRO es menor a la de Inicio		
        if (oEdit.fsEntry.idDataEntryDependent) {
            oFSEntryFFIN = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent)
            if ((oFSEntryFFIN) && (oFSEntryFFIN.tipoGS == 7)) {
                if (oFSEntryFFIN.getValue()) if (oEdit.fsEntry.getValue() != null && oEdit.fsEntry.getValue() != "") if (oEdit.fsEntry.getValue() > oFSEntryFFIN.getValue()) {
                    //Cargar el ALERT de la BBDD						
                    alert(sMensajeFecha)
                    oEdit.fsEntry.setValue("")
                    oEdit.fsEntry.setDataValue("")
                    oEdit.lastText = "" //Un bug en ig_edit.js obliga a vaciar el lastTExt o de lo contrario no se lanzan eventos cuando es necesario
                    oEdit.setText("");
                    oEdit.setValue("");
                    oEdit.setDate("");
                    oEdit.old = null; //Vaciamos la fecha e impedimos que en ig_edit.js se le de valor de nuevo, para que se lance el evento valueChange siempre que es necesario
                    window.focus(); //Los cambios de LastText y old hacen que el evento spin del control (las flechas) puedan entrar en bucle. Hacemos un focus fuera del control para impedirlo.
                    return;
                }
            }
        }
    } else if (oEdit.fsEntry.tipoGS == 7) {
        //Mirar si la fecha de INICIO de SUMINISTRO es mayor a la de fin	
        if (oEdit.fsEntry.idDataEntryDependent) {
            oFSEntryFINI = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent)
            if ((oFSEntryFINI) && (oFSEntryFINI.tipoGS == 6)) if (oFSEntryFINI.getValue()) if (oEdit.fsEntry.getValue() != null && oEdit.fsEntry.getValue() != "") if (oFSEntryFINI.getValue() > oEdit.fsEntry.getValue()) {
                //Cargar el ALERT de la BBDD							
                alert(sMensajeFecha)
                oEdit.fsEntry.setValue("")
                oEdit.fsEntry.setDataValue("")
                oEdit.lastText = "" //Un bug en ig_edit.js obliga a vaciar el lastTExt o de lo contrario no se lanzan eventos cuando es necesario
                oEdit.setText("");
                oEdit.setValue("");
                oEdit.setDate("");
                oEdit.old = null; //Vaciamos la fecha e impedimos que en ig_edit.js se le de valor de nuevo, para que se lance el evento valueChange siempre que es necesario
                window.focus(); //Los cambios de LastText y old hacen que el evento spin del control (las flechas) puedan entrar en bucle. Hacemos un focus fuera del control para impedirlo.
                return;
            }
        }
    };
    ddFec_ValueChange(oEdit, oldValue, oEvent)
};
function fslimitar_den_art(oEdit, text, oEvent) {
    oEdit = oEdit[0];
    if (oEdit.fsEntry.articuloGenerico) {
        if (oEdit.fsEntry.getValue().length > oEdit.fsEntry.maxLength) {
            oEdit.fsEntry.setValue(oEdit.fsEntry.getValue().substr(oEdit.fsEntry.getValue(), oEdit.fsEntry.maxLength))
            oEdit.fsEntry.setDataValue(oEdit.fsEntry.getDataValue().substr(oEdit.fsEntry.getDataValue(), oEdit.fsEntry.maxLength))
        }
    } else if (!oEdit.fsEntry.articuloGenerico) {
        if (oEvent.keyCode == 9) return;
        if (oEvent.keyCode == 17) return;
    }
};
function DenArticulo_ValueChange(oEdit, text) {
    oEdit = oEdit[0];
    if (!oEdit.fsEntry.articuloGenerico) {
        idEntryCod = calcularIdDataEntryCelda(oEdit.fsEntry.id, oEdit.fsEntry.nColumna - 1, oEdit.fsEntry.nLinea, 0);
        if (idEntryCod) oFSEntryCod = fsGeneralEntry_getById(idEntryCod);
        if ((oFSEntryCod) && (oFSEntryCod.tipoGS == 119)) {
            if (!oFSEntryCod.getValue() == '') ddNuevoCodArticulo_ValueChange($('#' + idEntryCod + "__t"), oFSEntryCod.getValue());
        }
    }
};
function show_Solicitudes(oEdit, oldValue, oEvent) {
    CerrarDesplegables();
    var newWindow = window.open("../_common/solicitudesserver.aspx?idDataEntry=" + oEdit.fsEntry.id + "&idRefSol=" + oEdit.fsEntry.idRefSol + "&Moneda=" + oEdit.fsEntry.instanciaMoneda, "_blank", "width=670,height=500,top=50,left=200,status=yes,resizable=no");
    newWindow.focus();
};
function show_DetalleSolicitudPadre(oEdit, oldValue, oEvent) {
    var idSolPadre = oEdit.fsEntry.getDataValue();
    CerrarDesplegables();
    var newWindow = window.open("../_common/detallesolicitudpadre.aspx?idSolPadre=" + idSolPadre + "&Moneda=" + oEdit.fsEntry.instanciaMoneda, "_blank", "width=530,height=130,status=yes,resizable=no,top=200,left=200");
    newWindow.focus();
};
//muestra el detalle de la partida cuando el dataentry es de solo lectura
function show_detalle_partida(oEdit, oldValue, oEvent) {
    var pres5 = oEdit.fsEntry.PRES5;
    var texto = oEdit.fsEntry.getDataValue();
    var titulo = oEdit.fsEntry.title

    CerrarDesplegables();
    if (texto != '') {
        var newWindow = window.open("../_common/detallePartida.aspx?titulo=" + titulo + "&pres5=" + pres5 + "&texto=" + escape(texto), "_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250")
        newWindow.focus();
    }
};
function show_detalle_activo(oEdit, oldValue, oEvent) {
    var value = oEdit.fsEntry.getDataValue();

    CerrarDesplegables();
    if (value != '') {
        var newWindow = window.open(rutaFS + "SM/Activos/detalleactivo.aspx?&codActivo=" + escape(value), "_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");
        newWindow.focus();
    };
};
//muestra el detalle del tipo de pedido cuando el dataentry es de solo lectura
function show_detalle_tipo_pedido(oEdit, oldValue, oEvent) {
    var texto = oEdit.fsEntry.getDataValue();

    CerrarDesplegables();
    if (texto != '') {
        var newWindow = window.open(rutaFS + "_common/DetalleTipoPedido.aspx?cod=" + texto, "_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250")
        newWindow.focus();
    }
};
//se ejecuta cuando se ha modificado el contenido de la combo de tipo de pedido
//Par�metro: id de la combo
function wddTipoPedido_AfterSelectChange(webComboId) {
    // get the combo from the passed-in id
    var combo = igcmbo_getComboById(webComboId);
    // get the currently selected row in webcombo
    var index = combo.getSelectedIndex();
    // get grid reference from webcombo
    var grid = combo.getGrid();
    // get selected row in grid
    var row = grid.Rows.getRow(index);
    var idDataEntry = replaceAll(webComboId, "x", "_")
    idDataEntry = idDataEntry.replace("__t", "")

    if (row != null) {
        oEdit = fsGeneralEntry_getById(idDataEntry)
        if (oEdit) {
            oEdit.setDataValue(row.getCell(1).getValue());
            oEdit.Concepto = row.getCell(3).getValue();
            oEdit.Almacenamiento = row.getCell(4).getValue();
            oEdit.Recepcion = row.getCell(5).getValue();
        }

        var cadena = '\n';
        for (arrInput in arrInputs) {
            var o = fsGeneralEntry_getById(arrInputs[arrInput])
            if (o) {
                if (o.tipoGS == 119) {
                    if (o.getDataValue() != '') {
                        if (((row.getCell(3).getValue() == '0') && (o.Concepto == '1')) || ((row.getCell(3).getValue() == '1') && (o.Concepto == '0')) || ((row.getCell(4).getValue() == '0') && (o.Almacenamiento == '1')) || ((row.getCell(4).getValue() == '1') && (o.Almacenamiento == '0')) || ((row.getCell(4).getValue() == '1') && (o.Almacenamiento == '0')) || ((row.getCell(5).getValue() == '0') && (o.Recepcion == '1')) || ((row.getCell(5).getValue() == '1') && (o.Recepcion == '0'))) {
                            sDenArticulo = '';
                            if (o.idCampoPadre) { //est� en un desglose
                                oDen = null;
                                idEntryDen = calcularIdDataEntryCelda(o.id, o.nColumna + 1, o.nLinea, 0)
                                if (idEntryDen) oDen = fsGeneralEntry_getById(idEntryDen)
                                if (oDen) {
                                    sDenArticulo = ' - ' + oDen.getDataValue();
                                    oDen.setValue('');
                                    oDen.setDataValue('');
                                    oDen.codigoArticulo = '';
                                }
                            }

                            if (cadena.search('\n' + o.getDataValue() + sDenArticulo + '\n') == -1) cadena = cadena + o.getDataValue() + sDenArticulo + '\n'
                            o.setValue('');
                            o.setDataValue('');

                        }
                    }
                }
            }
        }

        if (cadena != '\n') alert(mensajeCambioTipoPedido + cadena)
    } else {
        oEdit = fsGeneralEntry_getById(idDataEntry)
        if (oEdit) {
            oEdit.setDataValue('');
            oEdit.Concepto = '';
            oEdit.Almacenamiento = '';
            oEdit.Recepcion = '';
        }
    }
};
/*  Revisado por: blp. Fecha: 03/10/2011
''' Si no hay elemento seleccionado, se quita el texto que haya en el cuado de texto del webdropdown para no confundir
''' sender: control que lanza el evento
''' e: argumentos del evento
''' Llamada desde: Evento blur de webdropdown. Max 0,1 seg
*/
function WebDropDown_Focus(sender, e) {
    var dropDownEntry = fsGeneralEntry_getById(sender._id);
    var dropDown = $find(sender._id);
    switch (dropDownEntry.tipoGS) {
        case 101:
            //Forma pago
        case 102:
            //Moneda
        case 105:
            //Unidad
        case 142:
            //Unidad de Pedido
        case 107:
            //Pais
        case 108:
            //Provincia
        case 109:
            //Destino
        case 114:
            //Contacto Proveedor
        case 122:
            //Departamento
        case 123:
            //Org. Compras
        case 124:
            //Centro
        case 125:
            //Almacen
        case 151:
            //Anyo partida
        case 143:
            //Proveedor ERP
            if (dropDown) {
                //Estos casos cargan sus datos desde ajax al desplegar el combo.
                //Por lo que puede que no hayan cargado a�n los registros y por eso no hay nada seleccionado.
                //En este caso miramos si tiene value
                var sValue = dropDownEntry.getDataValue();
                if (sValue == null || sValue == "") {
                    dropDown.set_currentValue('', true);
                }
                sValue = null;
            }
            break;
        default:
            if (dropDown) {
                if (((dropDownEntry.tipoGS == 0) && (dropDownEntry.tipo == 5 || dropDownEntry.tipo == 6)) || (dropDownEntry.idCampoPadreListaEnlazada != 0)) { //Si es un dropdown hijo de listas hacemos lo mismo que con Prov, Dpto, Centro y Almac�n
                    var sValue = dropDownEntry.getDataValue();
                    if (sValue == null || sValue == "") {
                        dropDown.set_currentValue('', true);
                    }
                    sValue = null;
                } else {
                    //Si no hay elemento seleccionado, se quita el texto que haya en el cuado de texto del webdropdown para no confundir
                    var item = dropDown.get_items().getItem(dropDown.get_selectedItemIndex());
                    if (item == null) {
                        dropDown.set_currentValue('', true);
                    }
                    item = null;
                }
            }
            break;
    }
    dropDownEntry = null;
    dropDown = null;
};
// Revisado por: blp. Fecha: 03/10/2011
//''' <summary>
//Gets the new collection of selected drop-down items
//Only one item will be in the collection when single selection is enabled      
//''' </summary>
//''' <param name="sender">Origen del evento.</param>
//''' <param name="e">los datos del evento.</param>    
//''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
function WebDropDown_SelectionChanging(sender, e) {
    var newSelection = e.getNewSelection();
    if (newSelection[0] != null) {
        var text = newSelection[0].get_text();
        var index = newSelection[0].get_value();

        var oldSelection = e.getOldSelection();
        var indexOld = '';

        if (oldSelection[0]) {
            indexOld = oldSelection[0].get_value();
        }
        var oEdit = fsGeneralEntry_getById(sender._id)
        if (oEdit) {
            //si hay valor por defecto y es la primera vez que se selecciona de la combo, e.getOldSelection est� vac�o. Por eso cojemos el valor del dataentry.
            if (oEdit.tipoGS == 102) {
                var oldCodMoneda = oEdit.getDataValue();
                var oldDenMoneda = oEdit.getValue();
                if (oldCodMoneda == "" || oldCodMoneda == null) {
                    oldCodMoneda = sMonedaActual;
                    oldDenMoneda = sDenMonedaActual;
                }
            }

            oEdit.setDataValue(index);
            oEdit.setValue(text)

            var bCambiado = (index != indexOld)

            oEdit.oldDataValue = indexOld;

            switch (oEdit.tipoGS) {
                case 114:
                    //Contacto Proveedor
                    break;
                case 102:
                    //Moneda 
                    var nuevoTexto;
                    if (oldCodMoneda != "" && oldCodMoneda != null) {
                        if (((oEdit.TipoSolicit == 9) || (oEdit.TipoSolicit == 8)) && existeLineaDesgloseArticulos() && existeLineaDesglosePrecioUnitarioUltimaAdjudicacion()) {
                            if (oldCodMoneda != oEdit.getDataValue()) { //caso en el que se ha limpiado y luego quiero volver a ponerlo como estaba antes
                                var mensajeReemplazado = sMensajeImposibleCambiarMoneda.replace('###', oEdit.getDataValue());
                                mensajeReemplazado = mensajeReemplazado.replace('@@@', oldCodMoneda);
                                alert(mensajeReemplazado);
                                oEdit.setDataValue(oldCodMoneda);
                                oEdit.setValue(oldDenMoneda)
                            }
                        } else {
                            aplicarCambioMonedaImportesUnitarios(oldCodMoneda, oEdit.getDataValue())
                            $.each($('[nombreMoneda]'), function () {
                                nuevoTexto = $(this).text().replace(oldCodMoneda, oEdit.getDataValue());
                                $(this).text(nuevoTexto);
                            });
                        }
                    } else {
                        $.each($('[nombreMoneda]'), function () {
                            nuevoTexto = $(this).text().replace('()', '(' + oEdit.getDataValue() + ')');
                            $(this).text(nuevoTexto);
                        });
                    }

                    sMonedaActual = oEdit.getDataValue();
                    sDenMonedaActual = oEdit.getValue();

                    break;
                case 107:
                    //Pa�s
                    if (oEdit.dependentfield != null) {
                        var ugtxtProvinciaID = oEdit.dependentfield;
                        //Vaciamos de contenido el control dependiente
                        vaciarDropDown(ugtxtProvinciaID);
                    }
                    break;
                case 109:
                    //Destino
                    if (oEdit.getValue() != null) document.getElementById("igtxt" + oEdit.Editor2.ID).style.visibility = "visible"
                    else document.getElementById("igtxt" + oEdit.Editor2.ID).style.visibility = "hidden"
                    if (oEdit.idDataEntryDependent3 != null) {
                        var ugtxtAlmacenID = oEdit.idDataEntryDependent3;
                        //Vaciamos de contenido el control dependiente
                        vaciarDropDown(ugtxtAlmacenID);
                        ugtxtAlmacenID = null;
                    }
                    break;
                case 122:
                    //departamento
                    //Pongo el estilo normal del departamento pq ha cambiado y no esta de baja
                    //MALLLLLLLLLLLdocument.getElementById(oEdit.id + "__t_t").className = "TipoTextoMedio"
                    break;
                case 123:
                    //Org Compras
                    try {
                        eval("sCodOrgComprasFORM" + oEdit.idCampo + "=" + "'" + oEdit.Hidden.value + "'");
                    } catch (err) { }

                    //Borrar el Proveedor, Articulo y la denominacion y El CENTRO
                    if (bCambiado) {
                        //Art�culo
                        if (oEdit.dependentfield != null) {
                            var oArticuloID = oEdit.dependentfield;
                            //Vaciamos de contenido el control dependiente
                            vaciarDropDown(oArticuloID);
                            oArticuloID = null;
                        }
                        //Borrar Proveedores, Proveedores ERP y atributos de lista externa                        
                        borrarDependientes(oEdit);
                        if (oEdit.idDataEntryDependent != null) {
                            var aOrgCompras = oEdit.id.split("_")
                            var aArt = oEdit.idDataEntryDependent.split("_")
                            if (aOrgCompras.length < aArt.length) {
                                BorrarArticulosCentrosEnDesglose(oEdit.idDataEntryDependent, oEdit.idDataEntryDependent2);
                            } else {
                                var oEntry = fsGeneralEntry_getById(oEdit.idDataEntryDependent)
                                if (oEntry != null) {
                                    oEntry.setValue("");
                                    oEntry.setDataValue("");
                                    if (oEntry.tipoGS == 119) {
                                        oEntry.OrgComprasDependent.value = index

                                        //Borrar la Denominacion
                                        var idDataEntryDen = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna + 1, oEntry.nLinea, 0)
                                        if (idDataEntryDen) {
                                            var oEntryDen = fsGeneralEntry_getById(idDataEntryDen)
                                            if ((oEntryDen) && (oEntryDen.tipoGS == 118)) {
                                                oEntryDen.setValue("");
                                                oEntryDen.setDataValue("");

                                                oEntryDen.OrgComprasDependent.value = index
                                            }
                                        }
                                    } else if (oEntry.tipoGS == 118) {
                                        oEntry.codigoArticulo = "";
                                        oEntry.OrgComprasDependent.value = index

                                        var idDataEntryCod = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna - 1, oEntry.nLinea, 0)
                                        if (idDataEntryCod) {
                                            var oEntryCod = fsGeneralEntry_getById(idDataEntryCod)
                                            if ((oEntryCod) && (oEntryCod.tipoGS == 119)) {
                                                oEntryCod.setValue("");
                                                oEntryCod.setDataValue("");

                                                oEntryCod.OrgComprasDependent.value = index
                                            }
                                        }
                                    }
                                }
                                //si es webdropdown, vacias de contenido el control en pantalla
                                vaciarDropDown(oEdit.idDataEntryDependent);
                            }
                            aOrgCompras = null;
                        }
                        //CENTRO
                        if (oEdit.idDataEntryDependent2 != null) {
                            var ugtxtCentroID = oEdit.idDataEntryDependent2;
                            //Vaciamos de contenido el control dependiente
                            vaciarDropDown(ugtxtCentroID);
                            ugtxtCentroID = null;

                            //Vaciar el control dependiente del control dependiente
                            var oCentro = fsGeneralEntry_getById(oEdit.idDataEntryDependent2)
                            if (oCentro) {
                                if (oCentro.Dependent) {
                                    oCentro.Dependent.value = index;
                                }
                                //Almac�n (depende de centro)
                                if (oCentro.idDataEntryDependent3 != null) {
                                    //Vaciamos de contenido el control dependiente
                                    vaciarDropDown(oCentro.idDataEntryDependent3);
                                }
                                oCentro = null;
                            }
                        }
                    }
                    break;
                case 124:
                    //Centro
                    if (bCambiado) {
                        //Si ha cambiado el Centro borrar el ARTICULO relacionado
                        if (oEdit.idDataEntryDependent != null) {
                            oCentro = oEdit.id.split("_")
                            aArt = oEdit.idDataEntryDependent.split("_")
                            if (oCentro.length < aArt.length) {
                                BorrarArticulosCentrosEnDesglose(oEdit.idDataEntryDependent, null);
                            } else {
                                oEntry = fsGeneralEntry_getById(oEdit.idDataEntryDependent)
                                if (oEntry != null) {
                                    var sCodOrgCompras = oEdit.Dependent.value;
                                    var sCodCentro = oEdit.Hidden.value;

                                    if (oEntry.tipoGS == 119) {
                                        idDataEntryDen = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna + 1, oEntry.nLinea, 0)
                                        oEntryDen = fsGeneralEntry_getById(idDataEntryDen)
                                        oEntry.CentroDependent.value = index
                                        if (oEntryDen != null) {
                                            if (oEntryDen.tipoGS == 118) { //S�lo si es tipo Den Art�culo
                                                oEntryDen.CentroDependent.value = index
                                                if ((oEntryDen.codigoArticulo != null) && (oEntryDen.codigoArticulo != "")) {
                                                    //Llamamos de nuevo al buscador para que nos diga si para el nuevo Centro existe o no el articulo que tenemos
                                                    //en caso negativo, borra el articulo y la denominaci�n
                                                    var newWindow = window.open("../_common/BuscadorArticuloserver.aspx?&idArt=" + oEntryDen.codigoArticulo + "&fsEntryArt=" + oEntry.id + "&fsEntryDen=" + idDataEntryDen + "&CodOrgCompras=" + sCodOrgCompras + "&CodCentro=" + sCodCentro + "&desde=CENTRO", "iframeWSServer", "resizable=yes")
                                                    newWindow.focus();
                                                }
                                            }
                                        }
                                    } else {
                                        if (oEntry.tipoGS == 118) {
                                            idDataEntryCod = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna - 1, oEntry.nLinea, 0)
                                            oEntry.CentroDependent.value = index
                                            oEntryCod = fsGeneralEntry_getById(idDataEntryCod)
                                            if (oEntryCod) oEntryCod.CentroDependent.value = index
                                            if ((oEntry.codigoArticulo != null) && (oEntry.codigoArticulo != "")) {
                                                //Llamamos de nuevo al buscador para que nos diga si para el nuevo Centro existe o no el articulo que tenemos
                                                //en caso negativo, borra el articulo y la denominaci�n
                                                newWindow = window.open("../_common/BuscadorArticuloserver.aspx?&idArt=" + oEntry.codigoArticulo + "&fsEntryArt=" + idDataEntryCod + "&fsEntryDen=" + oEntry.id + "&CodOrgCompras=" + sCodOrgCompras + "&CodCentro=" + sCodCentro + "&desde=CENTRO", "iframeWSServer", "resizable=yes")
                                                newWindow.focus();
                                            }
                                        }
                                    }
                                    sCodOrgCompras = null;
                                    sCodCentro = null;
                                }
                            }
                            oCentro = null;
                        }
                        var sCodOrgCompras = oEdit.Dependent.value;
                        var sCodCentro = oEdit.Hidden.value;
                        var params = { sCodOrgCompras: sCodOrgCompras, sCodCentro: sCodCentro };
                        $.when($.ajax({
                            type: "POST",
                            url: rutaFS + '_Common/BuscadorArticulos.aspx/DevolverUONOrgCompras',
                            contentType: "application/json; charset=utf-8",
                            data: JSON.stringify(params),
                            dataType: "json",
                            async: false
                        })).done(function (msg) {
                            var respuesta = msg.d;
                            result = respuesta[0] + "-" + respuesta[1] + "-" + respuesta[2];
                        });
                        ComprobarComprador(result, "OrgComp");

                        if (oEdit.idDataEntryDependent3 != null) {
                            var ugtxtAlmacenID = oEdit.idDataEntryDependent3;
                            //Vaciamos de contenido el control dependiente
                            vaciarDropDown(ugtxtAlmacenID);
                            ugtxtAlmacenID = null;
                        }
                    }
                    break;
                case 125:
                    //Almacen
                    break;
                case 132:
                    //Tipo Pedido
                    var Concepto = newSelection[0].get_element().childNodes[0].rows[0].cells[2].innerHTML
                    var Recepcion = newSelection[0].get_element().childNodes[0].rows[0].cells[3].innerHTML
                    var Almacenamiento = newSelection[0].get_element().childNodes[0].rows[0].cells[4].innerHTML

                    oEdit.Concepto = Concepto
                    oEdit.Almacenamiento = Almacenamiento
                    oEdit.Recepcion = Recepcion

                    var cadena = '\n';
                    for (var arrInput in arrInputs) {
                        var o = fsGeneralEntry_getById(arrInputs[arrInput])
                        if (o) {
                            if (o.tipoGS == 119) {
                                if (o.getDataValue() != '') {
                                    if (((Concepto == '0') && (o.Concepto == '1')) || ((Concepto == '1') && (o.Concepto == '0')) || ((Almacenamiento == '0') && (o.Almacenamiento == '1')) || ((Almacenamiento == '1') && (o.Almacenamiento == '0')) || ((Almacenamiento == '1') && (o.Almacenamiento == '0')) || ((Recepcion == '0') && (o.Recepcion == '1')) || ((Recepcion == '1') && (o.Recepcion == '0'))) {
                                        var sDenArticulo = '';
                                        if (o.idCampoPadre) { //est� en un desglose
                                            var oDen = null;
                                            var idEntryDen = calcularIdDataEntryCelda(o.id, o.nColumna + 1, o.nLinea, 0)
                                            if (idEntryDen) oDen = fsGeneralEntry_getById(idEntryDen)
                                            if (oDen) {
                                                sDenArticulo = ' - ' + oDen.getDataValue();
                                                oDen.setValue('');
                                                oDen.setDataValue('');
                                                oDen.codigoArticulo = '';
                                            }
                                            idEntryDen = null;
                                            oDen = null;
                                        }

                                        if (cadena.search('\n' + o.getDataValue() + sDenArticulo + '\n') == -1) cadena = cadena + o.getDataValue() + sDenArticulo + '\n'
                                        o.setValue('');
                                        o.setDataValue('');
                                    }
                                }
                            }
                        }
                        o = null;
                        sDenArticulo = null;
                    }
                    Concepto = null;
                    Recepcion = null;
                    Almacenamiento = null;
                    arrInput = null;

                    if (cadena != '\n') alert(mensajeCambioTipoPedido + cadena)
                    cadena = null;
                    break;
            }
            oEntry = null;
            idDataEntryDen = null;
            oEntryDen = null;
            idDataEntryCod = null;
            oEntryCod = null;
            bCambiado = null;
        }

        text = null;
        index = null;
        oEdit = null;
        oldSelection = null;
        indexOld = null;
    } else {
        //Vaciamos de contenido el control
        vaciarDropDown(sender._id);
    }
    newSelection = null;
};
// The client event �InputKeyDown� takes two parameters sender and e
// sender  is the object which is raising the event
// e is the DropDownControlEventArgs
function WebDropDown_InputKeyDown(sender, eventArgs) {
    oCombo = $find(sender._id)
    oEdit = fsGeneralEntry_getById(sender._id)
    sComboPadre = ''
    if (oCombo) {
        //Obtener el combo padre para que si al no tener ningun elemento seleccionado no puedas escribir (LAS LISTAS ENLAZADAS SE ENCUENTRAN DE MOMENTO EN EL MISMO GRUPO)
        if ((oEdit.idCampoPadreListaEnlazada) && (oEdit.idCampoPadreListaEnlazada > 0)) {
            arrAux = sender._id.split("_")

            if (arrAux[1] == '') sComboPadre = arrAux[0] + '__' + arrAux[2] + '_' + arrAux[3] + "_" + 'fsentry' + oEdit.Grupo + '__t'; //uwtGrupos_ctl01_578_fsentry28277__t"
            else sComboPadre = arrAux[0] + '_' + arrAux[1] + '_' + arrAux[2] + "_" + 'fsentry' + oEdit.Grupo + '__t'; //uwtGrupos_ctl01_578_fsentry28277__t"

        }
        if (sComboPadre != '') {
            comboPadre = $find(sComboPadre)
            if ((comboPadre) && (comboPadre.get_currentValue() == '')) eventArgs.set_cancel(true);
        }
    }
};
function WebDropDown_DropDownOpening(sender, e) {
    var dropDown = $find(sender._id);
    dropDown._elements.DropDown.style.width = (dropDown._elements.Input.clientWidth + dropDown._elements.Button.clientWidth).toString() + "px"
    dropDown._elements.DropDownContents.style.Width = (dropDown._elements.Input.clientWidth + dropDown._elements.Button.clientWidth).toString() + "px"
};
function Solicitud_Padre_seleccionado(idDataEntry, sId, sDen) {
    //Obtener el DataEntry de Ref.Solicitud
    oFSEntry = fsGeneralEntry_getById(idDataEntry);
    if (oFSEntry) {
        oFSEntry.setDataValue(sId);
        if (sDen != null) sValor = sId + " - " + sDen;
        else sValor = sId;
        oFSEntry.setValue(sValor);
    };
};
//''' <summary>
//Muestra la ventana con los datos de la tabla externa correspondiente
//''' </summary>
//''' <param name="oEdit">nombre del dataentry</param>
//''' <param name="oldValue">Valor del dataentry</param>    
//''' <param name="oEvent">Evento producido</param>    
//''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
var ventexterna = false;
function show_externa(oEdit, oldValue, oEvent) {
    ventexterna = true;
    var artcod = "";
    var sIndex = "-1";
    if (oEdit.fsEntry.idCampoPadre) {
        var o = oEdit.fsEntry;
        var desglose = o.id.substr(0, o.id.search("_fsdsentry"));
        sIndex = o.id.substring((desglose + "_fsdsentry_").length, o.id.search("_" + o.idCampo))
    }
    if (oEdit.fsEntry.dependentfield) {
        if (sIndex == "-1") artcod = fsGeneralEntry_getById(oEdit.fsEntry.dependentfield).getDataValue()
        else artcod = fsGeneralEntry_getById(oEdit.fsEntry.dependentfield.replace("fsentry", "fsdsentry_" + sIndex + "_")).getDataValue()
    }
    var vent = window.open(rutaFS + "_common/TablaExterna.aspx?tabla=" + oEdit.fsEntry.TablaExterna + "&valor=" + encodeURIComponent(oldValue) + "&idDataEntry=" + oEdit.fsEntry.id + "&art=" + artcod, "wtablaexterna", "width=700,height=450,status=yes,resizable=no,top=200,left=200");
    vent.focus();
};
function show_externa_ReadOnly(oEdit, oldValue, oEvent) {
    var newWindow = window.open("../_common/tablaexternareg.aspx?tabla=" + oEdit.fsEntry.TablaExterna + "&valor=" + oEdit.fsEntry.dataValue, "wtablaexterna", "width=700,height=275,status=yes,resizable=no,top=200,left=200");
    newWindow.focus();
};
function valor_original_externa(idDataEntry) {
    var oFSEntry = fsGeneralEntry_getById(idDataEntry);
    if (oFSEntry.tipo == 2 && oFSEntry.dataValue == null || oFSEntry.tipo == 3 && oFSEntry.dataValue == new Date('1/1/1900') || oFSEntry.getValue() == '') {
        oFSEntry.setValue('');
        oFSEntry.setDataValue('');
        oFSEntry.codigoArticulo = null;
    } else {
        var valorant = oFSEntry.dataValue;
        oFSEntry.setValue(oFSEntry.Hidden.value);
        oFSEntry.setDataValue(valorant);
    }
};
function valor_externa_seleccionado(idDataEntry, valor, texto, art) {
    var oFSEntry = fsGeneralEntry_getById(idDataEntry);
    if (document.getElementById("bMensajePorMostrar")) {
        if (document.getElementById("bMensajePorMostrar").value == "1") {
            document.getElementById("bMensajePorMostrar").value = "0";
        }
    }
    if (oFSEntry) {
        oFSEntry.setValue(texto);
        oFSEntry.setDataValue(valor);
        oFSEntry.codigoArticulo = ((art == '') ? null : art);
    }
};
function validar_externa(oEdit, oldValue, oEvent) {
    if (oEdit.fsEntry.getValue() != '' && !ventexterna) {
        var bvalidar = false;
        if (oEdit.fsEntry.tipo == 2 && oEdit.fsEntry.dataValue == null || oEdit.fsEntry.tipo == 3 && oEdit.fsEntry.dataValue == new Date('1/1/1900')) {
            if (oEdit.fsEntry.getValue() != '') bvalidar = true;
        } else {
            if (oEdit.fsEntry.getValue() != oEdit.fsEntry.Hidden.value) bvalidar = true;
        }
        if (bvalidar) {
            var artcod = "";
            var sIndex = "-1";
            if (oEdit.fsEntry.idCampoPadre) {
                var o = oEdit.fsEntry;
                var desglose = o.id.substr(0, o.id.search("_fsdsentry"));
                sIndex = o.id.substring((desglose + "_fsdsentry_").length, o.id.search("_" + o.idCampo))
            }
            if (oEdit.fsEntry.dependentfield) {
                if (sIndex == "-1") artcod = fsGeneralEntry_getById(oEdit.fsEntry.dependentfield).getDataValue()
                else artcod = fsGeneralEntry_getById(oEdit.fsEntry.dependentfield.replace("fsentry", "fsdsentry_" + sIndex + "_")).getDataValue()
            }
            if (document.getElementById("bMensajePorMostrar")) {
                document.getElementById("bMensajePorMostrar").value = "1"
            }
            var newWindow = window.open("../_common/tablaexternaserver.aspx?tabla=" + oEdit.fsEntry.TablaExterna + "&valor=" + oEdit.text + "&fsEntryID=" + oEdit.fsEntry.id + "&art=" + artcod, "iframeWSServer");
            newWindow.focus();
        }
    }
};
function errorval_externa(idDataEntry, mensaje) {
    alert(mensaje);
    if (document.getElementById("bMensajePorMostrar")) {
        if (document.getElementById("bMensajePorMostrar").value == "1") {
            document.getElementById("bMensajePorMostrar").value = "0";
        }
    }
    var oFSEntry = fsGeneralEntry_getById(idDataEntry);
    if (oFSEntry) {
        var valorant = oFSEntry.dataValue;
        oFSEntry.setValue(oFSEntry.Hidden.value);
        oFSEntry.setDataValue(valorant);
    }
};
function VaciarMat_NuevoCodArt_Den(oEdit, text, oEvent) {
    if (oEvent.event.keyCode == 8 || oEvent.event.keyCode == 46 || oEvent.event.keyCode == 32) {
        //Poner el NuevoCodArt en blanco
        var idEntryCod = calcularIdDataEntryCelda(oEdit.fsEntry.id, oEdit.fsEntry.nColumna + 1, oEdit.fsEntry.nLinea, 0)
        if (idEntryCod) var oEntry = fsGeneralEntry_getById(idEntryCod)
        if ((oEntry != null) && (oEntry.tipoGS == 119)) {
            oEntry.setValue("")
            oEntry.title = ""

            //Poner la Denominacion en Blanco
            oEntry = null;
            var idEntryDen = calcularIdDataEntryCelda(oEdit.fsEntry.id, oEdit.fsEntry.nColumna + 2, oEdit.fsEntry.nLinea, 0)
            if (idEntryDen) oEntry = fsGeneralEntry_getById(idEntryDen)
            if ((oEntry != null) && (oEntry.tipoGS == 118)) {
                oEntry.setValue("")
                oEntry.title = ""
            }
        }

        //Mirar si tiene alg�n campo de lista como hijo y ponerlo en blanco 
        VaciarCampoListaCampoMaterial(oEdit.fsEntry);

        fsVaciaCombo(oEdit, text, oEvent)
    }
};
function VaciarCampoListaCampoMaterial(oEntry) {
    var idEntryLista;
    var oLista;
    var p = window.opener;
    var arrAux = oEntry.id.split("_");
    if (arrAux[1] == '') idEntryLista = arrAux[0] + '__' + arrAux[2] + '_' + arrAux[3] + "_" + 'fsentry' + oEntry.idCampoHijaListaEnlazada + '__t'; //uwtGrupos_ctl01_578_fsentry28277__t";
    else idEntryLista = arrAux[0] + '_' + arrAux[1] + '_' + arrAux[2] + "_" + 'fsentry' + oEntry.idCampoHijaListaEnlazada + '__t'; //uwtGrupos_ctl01_578_fsentry28277__t";
    if (idEntryLista) oLista = fsGeneralEntry_getById(idEntryLista);
    if (!oLista && p) { //el padre puede que est� en p, el window.opener
        oLista = p.fsGeneralEntry_getById(sIdPadre)
    }
    if (!oLista) {
        //el padre puede que sea un campo de tipo material que est� en otro grupo
        for (arrInput in arrInputs) {
            var o = fsGeneralEntry_getById(arrInputs[arrInput])
            if (o) {
                if ((o.intro == 1) && (o.tipoGS == 0) && o.id.endsWith('fsentry' + oEntry.idCampoHijaListaEnlazada)) {
                    oLista = fsGeneralEntry_getById(arrInputs[arrInput] + '__t');
                }
            }
        }
    }
    if (oLista) {
        oLista.setValue("");
        oLista.title = "";
    }
}
/*  Revisado por: blp. Fecha: 06/10/2011
''' M�todo que vac�a los combos de art�culos y centro de cada l�nea de desglose cuando se le pasan los Id de los respectivos controles en la tabla oculta
''' idDataEntryInicialArticulo: Id del control de art�culo
''' idDataEntryInicialCentro: Id del control de centro
''' Llamada desde jsAlta.js
''' Tiempo m�x < 0,1 seg.
*/
function BorrarArticulosCentrosEnDesglose(idDataEntryInicialArticulo, idDataEntryInicialCentro) {
    if (idDataEntryInicialArticulo) var oEntryInicialArt = fsGeneralEntry_getById(idDataEntryInicialArticulo)
    if (idDataEntryInicialCentro) var oEntryInicialCen = fsGeneralEntry_getById(idDataEntryInicialCentro)

    arrAux = idDataEntryInicialArticulo.split("_")
    tabla = arrAux[0] + "__" + arrAux[2] + "_" + arrAux[3] + "_" + arrAux[3] + "_" + arrAux[5] + "_tblDesglose"
    filas = (document.getElementById(tabla).rows.length) - 1
    for (nLinea = 1; nLinea <= filas; nLinea++) {
        var idDataEntry = calcularIdDataEntryCelda(idDataEntryInicialArticulo, oEntryInicialArt.nColumna, nLinea, 0)
        if (oEntryInicialCen) var idDataEntryCentro = calcularIdDataEntryCelda(idDataEntryInicialCentro, oEntryInicialCen.nColumna, nLinea, 0)
        if (idDataEntry) { //Borrar Articulo
            var oEntry = fsGeneralEntry_getById(idDataEntry)
            if (oEntry != null) {
                oEntry.setValue("");
                oEntry.setDataValue("");
                oEntry.CentroDependent.value = ""
                if (oEntry.tipoGS == 119) {
                    //Borrar la Denominacion
                    var idDataEntryDen = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna + 1, oEntry.nLinea, 0)
                    if (idDataEntryDen) {
                        var oEntryDen = fsGeneralEntry_getById(idDataEntryDen)
                        if ((oEntryDen) && (oEntryDen.tipoGS == 118)) {
                            oEntryDen.setValue("");
                            oEntryDen.setDataValue("");
                            oEntryDen.CentroDependent.value = ""
                        }
                    }
                } else if (oEntry.tipoGS == 118) {
                    oEntry.codigoArticulo = "";
                    var idDataEntryCod = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna - 1, oEntry.nLinea, 0)
                    if (idDataEntryCod) {
                        var oEntryCod = fsGeneralEntry_getById(idDataEntryCod)
                        if ((oEntryCod) && (oEntryCod.tipoGS == 119)) {
                            oEntryCod.setValue("");
                            oEntryCod.setDataValue("");
                            oEntryCod.CentroDependent.value = "";
                        }
                    }
                }
            }
            if (idDataEntryCentro) {
                //Vaciamos de contenido el control
                vaciarDropDown(idDataEntryCentro);
            }
        }
    } //fin for
}; //fin function BorrarArticulosCentrosEnDesglose 
function fsVaciaComboCentros(oEdit, text, oEvent) {
    if (oEvent.event.keyCode == 8 || oEvent.event.keyCode == 46 || oEvent.event.keyCode == 32) {
        oEdit.setValue(null)
        oEdit.fsEntry.setValue(null)
        oEdit.fsEntry.setDataValue("")
        if (oEdit.fsEntry.idDataEntryDependent) {
            aCentro = oEdit.fsEntry.id.split("_")
            aArt = oEdit.fsEntry.idDataEntryDependent.split("_")
            if (aCentro.length < aArt.length) {
                BorrarArticulosCentrosEnDesglose(oEdit.fsEntry.idDataEntryDependent, null);
            } else {

                var oEntry = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent)
                if (oEntry != null) {
                    oEntry.setValue("");
                    oEntry.setDataValue("");
                    if (oEntry.tipoGS == 119) {
                        //Borrar la Denominacion
                        var idDataEntryDen = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna + 1, oEntry.nLinea, 0)
                        if (idDataEntryDen) {
                            var oEntryDen = fsGeneralEntry_getById(idDataEntryDen)
                            if ((oEntryDen) && (oEntryDen.tipoGS == 118)) {
                                oEntryDen.setValue("");
                                oEntryDen.setDataValue("");
                            }
                        }
                    } else if (oEntry.tipoGS == 118) {
                        oEntry.codigoArticulo = "";
                        var idDataEntryCod = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna - 1, oEntry.nLinea, 0)
                        if (idDataEntryCod) {
                            var oEntryCod = fsGeneralEntry_getById(idDataEntryCod)
                            if ((oEntryCod) && (oEntryCod.tipoGS == 119)) {
                                oEntryCod.setValue("");
                                oEntryCod.setDataValue("");
                            }
                        }
                    }
                }
            }
        }
    }
    ddcloseDropDownEvent(oEdit, text, oEvent)
    if (!(oEvent.event.keyCode == 16 || (oEvent.event.keyCode >= 35 && oEvent.event.keyCode <= 40) || oEvent.event.keyCode == 9)) oEvent.cancel = true
};
function ControlLongitudStringLargo(oCampo, longitud) {
    if (oCampo.value.length >= longitud) return false
    else return true
};
/* <summary>
''' Valida la longitud de un dataentry de tipo texto
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Todos los dataentry de tipo texto medio,largo y string ; Tiempo m�ximo: instantaneo</remarks>
*/
function validarLengthStringLargo(oCampo, longitud, msg) {
    msg = msg.replace("\\n", "\n")
    msg = msg.replace("\\n", "\n")
    if (oCampo.value.length > longitud) {
        alert(msg + longitud)
        return false
    } else return true
};
/* Valida el formato de un dataentry de tipo texto corto (sin recordar valores) */
function validarFormatoTextoCorto(oEdit, oldValue, oEvent) {
    var urlregex = new RegExp(oEdit.fsEntry.Formato, "i");
    if (urlregex.test(oEdit.fsEntry.getValue())) {
        oEdit.elem.className = "TipoTextoMedioVerde";
    } else {
        oEdit.elem.className = "TipoTextoMedioRojo";
        alert(oEdit.fsEntry.title + '. ' + oEdit.fsEntry.errMsgFormato);
    }
}
/* Valida el formato de un dataentry de tipo texto corto (con recordar valores) y texto medio y largo */
function validarFormato(oCampo, formato, msg) {
    var urlregex = new RegExp(formato, "i");
    if (urlregex.test(oCampo.value)) {
        oCampo.className = "TipoTextoMedioVerde";
    }
    else {
        oCampo.className = "TipoTextoMedioRojo";
        alert(msg);
    }
};
/*
Valida la longitud de un dataentry de tipo texto y si no cumple la condici�n, corta el texto.
Llamadad desde: las cajas de texto de los dataentry medio y largo.
*/
function validarLengthyCortarStringLargo(oCampo, longitud, msg) {
    var retorno = validarLengthStringLargo(oCampo, longitud, msg);
    if (!retorno) {
        oCampo.value = oCampo.value.substr(0, longitud);
    }
    return retorno;
};
/*  Revisado por: blp. Fecha: 05/10/2011
''' M�todo para vaciar el contenido del dropdown Almac�n
''' oCentro: control Centro
''' Llamada desde BuscadorArticulos.aspx
''' Tiempo m�x < 0,1 seg.
*/
function vaciarAlmacen(oCentro) {
    if (oCentro.orden > 1) {
        idEntryPost = calcularIdDataEntryCelda(oCentro.id, oCentro.nColumna + 1, oCentro.nLinea, 0);
        if (idEntryPost) {
            oFSEntryPost = fsGeneralEntry_getById(idEntryPost)
            if (oFSEntryPost) if (oFSEntryPost.tipoGS == 125) { // El DataEntry posterior es de tipo Almacen
                //Vaciamos de contenido el ALMAC�N
                vaciarDropDown(oCentro.idDataEntryDependent3);
            }
        }
    }
};
//<summary>M�todo para borrar el contenido de dataentrys dependientes (proveedores, proveedores ERP y atributos de lista externa) cuando ha cambiado la organizaci�n de compras</summary>
//<param name="Entry">id del dataentry de la organizaci�n de compras</param>
//<remarks>Llamada desde: WebDropDown_SelectionChanging</remarks>
function borrarDependientes(Entry) {
    f = document.forms["frmDetalle"];
    if (!f) f = document.forms["frmAlta"];

    for (arrInput in arrInputs) {
        var oAux = fsGeneralEntry_getById(arrInputs[arrInput]);
        if (oAux) {
            if (oAux.tipo == 9 && oAux.idDataEntryDependent == Entry.id) {
                //desglose despu�s de un campo de OrgCompras
                var i = 0;
                for (k = 1; k <= f.elements[oAux.id + "__numTotRows"].value; k++) {
                    for (arrCampoHijo in oAux.arrCamposHijos) {
                        oHijo = f.elements[oAux.id + "__" + k.toString() + "__" + oAux.arrCamposHijos[arrCampoHijo]];
                        if (oHijo) {
                            //input de desglose formato: IdCampoPadre_IdCampoHijo_Linea
                            oHijoSrc = f.elements[oAux.id + "__" + oAux.arrCamposHijos[arrCampoHijo]];
                            //si es proveedor
                            if (oHijoSrc.tipoGS == 100 ||   //proveedor
                                (oHijoSrc.tipoGS == 143 && (oHijoSrc.idDataEntryDependent == Entry.id || oHijoSrc.idEntryPROV == Entry.id)) ||  //proveedor ERP
                                (oHijoSrc.tipoGS == 143 && oHijoSrc.idDataEntryDependent == undefined) || //proveedor ERP
                                (oHijoSrc.intro == 2)   //atributo lista externa
                                ) {
                                oHijo.value = "";
                            }
                        }
                    }
                }
            } else {
                //si es proveedor
                if (((oAux.idDataEntryDependent == Entry.id) && (oAux.tipoGS == 100)) || //proveedor
                    ((oAux.idDataEntryDependent == Entry.id || oAux.idEntryPROV == Entry.id) && (oAux.tipoGS == 143)) || //proveedor ERP
                    (oAux.intro == 2 && oAux.idDataEntryDependent == Entry.id) //atributo lista externa
                    ) {
                    oAux.setValue("");
                    oAux.setDataValue("");
                }
            }
        }
    }
};
function borrarAnyoPartidaRelacionado(Entry) {
    f = document.forms["frmDetalle"];
    if (!f) f = document.forms["frmAlta"];
    for (arrInput in arrInputs) {
        var oAux = fsGeneralEntry_getById(arrInputs[arrInput]);
        var bComprobarBorrar = false;
        if (oAux) {
            if (Entry.nLinea > 0) { //se trata de un centro de coste en una l�nea de desglose  
                bComprobarBorrar = (Entry.nLinea == oAux.nLinea) &&  (oAux.tipoGS == 151 && oAux.IdEntryPartidaPlurianual && (oAux.IdEntryPartidaPlurianual == Entry.idCampo || oAux.IdEntryPartidaPlurianual == Entry.id || oAux.IdEntryPartidaPlurianual == Entry.campo_origen))
            } else {
                bComprobarBorrar = (oAux.tipoGS == 151 && oAux.IdEntryPartidaPlurianual && (oAux.IdEntryPartidaPlurianual == Entry.idCampo || oAux.IdEntryPartidaPlurianual == Entry.id || oAux.IdEntryPartidaPlurianual == Entry.campo_origen))
            }
            if (bComprobarBorrar && Entry.getValue() != "") {
                var bBorrar = false;
                if (oAux.tipoGS == 151) {
                    var AnyoPartida = comprobarAnyoPartidaUnica(Entry.PRES5, Entry.getDataValue());
                    if (AnyoPartida.length == 1) {
                        oAux.setValue(AnyoPartida[0].Valor);
                        oAux.setDataValue(AnyoPartida[0].Valor);
                    } else bBorrar = true;
                }            
                if (bBorrar) {
                    oAux.setValue("");
                    oAux.setDataValue("");
                }
            }
        }
    }
}

//borra el dataentry de partida o activo asociado a un centro de coste. Se le llama cuando se modifica o se
//borra el centro de coste.
//borra el art�culo y su denominaci�n si hay y no hay organizaci�n de compras o unidad organizativa
//Llamada desde: ddCentroCoste_ValueChange
function borrarPartidaoActivoRelacionado(Entry) {
    f = document.forms["frmDetalle"];
    if (!f) f = document.forms["frmAlta"];
    var bComprobarBorrarAnyoPartida = false;
    for (arrInput in arrInputs) {
        var oAux = fsGeneralEntry_getById(arrInputs[arrInput]);
        var bComprobarBorrar = false;
        if (oAux) {         
            if (Entry.nLinea > 0) { //se trata de un centro de coste en una l�nea de desglose                
                bComprobarBorrar = (Entry.nLinea == oAux.nLinea) && (((oAux.tipoGS == 130 || oAux.tipoGS == 131) && oAux.idEntryCC && (oAux.idEntryCC == Entry.idCampo || oAux.idEntryCC == Entry.id || oAux.idEntryCC == Entry.campo_origen)) ||
                    oAux.tipoGS == 118 || oAux.tipoGS == 119 || ((oAux.tipoGS == 151) && (oAux.IdEntryPartidaPlurianual !== undefined))); //partida o activo con centro de coste relacionado
            } else {
                bComprobarBorrar = (((oAux.tipoGS == 130 || oAux.tipoGS == 131) && oAux.idEntryCC && (oAux.idEntryCC == Entry.idCampo || oAux.idEntryCC == Entry.id || oAux.idEntryCC == Entry.campo_origen)) ||
                    oAux.tipoGS == 118 || oAux.tipoGS == 119 || ((oAux.tipoGS == 151) && (oAux.IdEntryPartidaPlurianual !== undefined))); //partida o activo con centro de coste relacionado
            }

            if (bComprobarBorrar && Entry.getValue() != "") {
                var bBorrar = false;

                //Miro antes de borrar si se ha escrito un centro de coste si hay una �nica partida asociada
                if (oAux.tipoGS == 130 || oAux.tipoGS == 131) {
                    var partida = comprobarPartidaUnica(oAux.PRES5, Entry.getDataValue());
                    if (partida != "") {
                        oAux.setValue(partida.Denominacion);
                        oAux.setDataValue(partida.Texto);
                    } else bBorrar = true;

                    oAux.Editor.elem.className = "TipoTextoMedio";

                    if ((oAux.tipoGS == 130) && ((bBorrar) || (oAux.getDataValue() == ''))) {
                        bComprobarBorrarAnyoPartida = true;
                    }
                } else {
                    if (oAux.tipoGS == 118 || oAux.tipoGS == 119) {
                        bBorrar = (!oAux.idDataEntryDependent && !oAux.idDataEntryDependent2 && !oAux.idDataEntryDependent3);
                        if (bBorrar) {
                            bBorrar = false;
                            var Articulo;
                            if (oAux.tipoGS == 118) {
                                Articulo = oAux.codigoArticulo;
                            } else {
                                Articulo = oAux.getDataValue();
                            }
                            if ((Articulo !== '') && (Articulo !== null)) {
                                params = {
                                    Articulo: Articulo,
                                    Centro: Entry.getDataValue()
                                };
                                $.when($.ajax({
                                    type: "POST",
                                    url: rutaFS + 'PM/CentrosCoste/Partidas.aspx/DeterminarSiEstaEnUon',
                                    contentType: "application/json; charset=utf-8",
                                    data: JSON.stringify(params),
                                    dataType: "json",
                                    async: false
                                })).done(function (msg) {
                                    var respuesta = msg.d;
                                    if (respuesta == "0") {
                                        bBorrar = true;
                                    }
                                });
                            }
                        }
                    } else {
                        if (oAux.tipoGS == 151) {
                            if (bComprobarBorrarAnyoPartida) {
                                bBorrar =true
                            }
                            if (oAux.EntryPartidaPlurianualCampo == 0)
                                bComprobarBorrarAnyoPartida = false
                        }
                    }
                }

                if (bBorrar) {
                    oAux.setValue("");
                    oAux.setDataValue("");
                }
            }
        }
    }
};
//borra el art�culo y su denominaci�n si hay y no hay organizaci�n de compras o unidad organizativa
//Llamada desde: Partida_seleccionada
function borrarArticuloRelacionado(Entry) {
    f = document.forms["frmDetalle"];
    if (!f) f = document.forms["frmAlta"];
    for (arrInput in arrInputs) {
        var oAux = fsGeneralEntry_getById(arrInputs[arrInput]);
        var bComprobarBorrar = false;
        if (oAux) {
            if (Entry.nLinea > 0) { //se trata de una partida en una l�nea de desglose                
                bComprobarBorrar = (Entry.nLinea == oAux.nLinea) && (oAux.tipoGS == 118 || oAux.tipoGS == 119);
            } else {
                bComprobarBorrar = (oAux.tipoGS == 118 || oAux.tipoGS == 119); //partida o activo con centro de coste relacionado
            }

            if (bComprobarBorrar && Entry.getValue() != "" && (!oAux.idDataEntryDependent && !oAux.idDataEntryDependent2 && !oAux.idDataEntryDependent3)) {
                var Articulo;
                if (oAux.tipoGS == 118) {
                    Articulo = oAux.codigoArticulo;
                } else {
                    Articulo = oAux.getDataValue();
                }
                if ((Articulo !== '') && (Articulo !== null)) {
                    params = {
                        Articulo: Articulo,
                        Centro: Entry.getDataValue()
                    };
                    $.when($.ajax({
                        type: "POST",
                        url: rutaFS + 'PM/CentrosCoste/Partidas.aspx/DeterminarSiEstaEnUon',
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(params),
                        dataType: "json",
                        async: false
                    })).done(function (msg) {
                        var respuesta = msg.d;
                        if (respuesta == "0") {
                            oAux.setValue("");
                            oAux.setDataValue("");
                        }
                    });
                }
            }
        }
    }
};
//INI FRAMES DESPLEGABLES
var gFRAMEid;
/*  
''' Muestra el iframe 
''' FRAMEid: id se le va a dar al iframe
''' x: coordenada donde empieza el iframe (LEFT)
''' y: coordenada donde empieza el iframe (TOP)
''' width: ancho
''' height: altura
''' Llamada desde: jsalta.js --> popupDesgloseClickEvent, ddMonedaopenDropDownEvent, ddProvFavopenDropDownEvent, ddPresupFavopenDropDownEvent
''' Tiempo m�x < 0,1 seg.
*/
function mostrarFRAME(FRAMEid, x, y, width, height) {
    var fr = document.getElementById(FRAMEid)
    if (fr) {
        fr.parentNode.removeChild(fr);
    }
    document.body.insertAdjacentHTML("beforeEnd", "<iframe id='" + FRAMEid + "' frameborder='no' style='Z-INDEX: 2000; LEFT:" + x + "px; WIDTH: " + width + "px; POSITION: absolute; TOP: " + y + "px; HEIGHT: " + height + "px;'   scrolling='no'></iframe>")

    if (window.addEventListener) {
        window.document.addEventListener("mousedown", eliminarFRAME, false)
    } else {
        if (window.attachEvent) {
            window.document.attachEvent("onmousedown", eliminarFRAME)
            window.document.attachEvent("onblur", eliminarFRAME)
        }
    };
    var d = document.getElementById(FRAMEid);
    d.frameBorder = false
    if (d.addEventListener) {
        d.addEventListener("mouseover", FRAMEover, false)
        d.addEventListener("mouseout", FRAMEout, false)
    } else {
        if (d.attachEvent) {
            d.attachEvent("onmouseover", FRAMEover)
            d.attachEvent("onmouseout", FRAMEout)
        }
    };
    gFRAMEid = FRAMEid
};
/*  
''' Oculta el iframe cargado en la variable global gFRAMEid
''' Llamada desde: jsalta.js --> mostrarFRAME
''' Tiempo m�x < 0,1 seg.
*/
function eliminarFRAME() {
    var d = document.getElementById(gFRAMEid);
    if (d) {
        d.parentNode.removeChild(d);
        if (window.document.removeEventListener) window.document.removeEventListener("mousedown", eliminarFRAME, false)
        else {
            if (window.document.detachEvent) window.document.detachEvent("onmousedown", eliminarFRAME)
        }
    }
};
/*  
''' Oculta el iframe cargado en la variable global gFRAMEid cuando se pasa por encima del frame
''' Llamada desde: jsalta.js --> mostrarFRAME
''' Tiempo m�x < 0,1 seg.
*/
function FRAMEover() {
    if (window.document.removeEventListener) window.document.removeEventListener("mousedown", eliminarFRAME, false)
    else {
        if (window.document.detachEvent) window.document.detachEvent("onmousedown", eliminarFRAME)
    }
};
/*  
''' Oculta el iframe cargado en la variable global gFRAMEid cuando se sale del frame
''' Llamada desde: jsalta.js --> mostrarFRAME
''' Tiempo m�x < 0,1 seg.
*/
function FRAMEout() {
    if (window.document.addEventListener) window.document.addEventListener("mousedown", eliminarFRAME, false)
    else {
        if (window.document.attachEvent) window.document.attachEvent("onmousedown", eliminarFRAME)
    }
};
/*FIN FRAMES DESPLEGABLES
''' <summary>
''' Crear los input necesarios para la grabaci�n en bbdd de una solicitud de compra favorita.
''' </summary>   
''' <returns>Nada</returns>
''' <remarks>Llamada desde: NWAlta.aspx     NWDetalleSolicitud.aspx; Tiempo m�ximo:0,1sg</remarks>
*/
function MontarFormularioFavoritos() {
    //Monta el formulario para luego hacer las insertar en las tablas de los favoritos
    //PM_SOLICITUDES_FAV , PM_CAMPO_FAV, PM_LINEA_DESGLOSE_FAV
    //Solo mandar los que estan visibles!!!!!!!!!!!!
    //***********************************************
    var bDetalle = true;
    var f = document.forms["frmSubmitFav"]
    var f2 = document.forms["frmDetalle"]
    if (!f2) {
        f2 = document.forms["frmAlta"]
        bDetalle = false;
    }

    var sDenFavorita = f.elements["denFavorita"].value
    if (bDetalle) var solicitud = f2.elements["SolicitudFav"].value
    else var solicitud = f2.elements["Solicitud"].value

    f.innerHTML = ""

    //A�adir la denominaci�n y la solicitud
    anyadirInputFav("denFavorita", sDenFavorita)
    anyadirInputFav("solicitud", solicitud)
    if (bDetalle) anyadirInputFav("Instancia", f2.elements["Instancia"].value)
    else anyadirInputFav("Instancia", 0)
    //y luego todos los inputs
    for (arrInput in arrInputs) {
        oEntry = fsGeneralEntry_getById(arrInputs[arrInput])

        if (oEntry) {
            if (oEntry.tipoGS != 3000) { //Excluyo el campo con el n�mero de linea, ya que no es un campo real de form_campo
                if (oEntry.idCampoPadre) //Este es un input perteneciente a un desglose
                {
                    re = /fsdsentry/
                    x = oEntry.id.search(re) + 10
                    y = oEntry.id.search(oEntry.idCampo) - 1

                    if (oEntry.id.search(re) >= 0) //y no es el input base del desglose
                    {
                        if (oEntry.tipoGS == 42) y = x + 1

                        iLinea = oEntry.id.substr(x, y - x)
                        iLineaOriginal = iLinea

                        iLinea = parseInt(htControlFilas[String(oEntry.idCampoPadre) + "_" + String(iLineaOriginal)])

                        //input de desglose formato: IdCampoPadre_IdCampoHijo_Linea

                        if (oEntry.intro == 1 && (oEntry.tipo == 1 || oEntry.tipo == 5 || oEntry.tipo == 6 || oEntry.tipo == 7) && (oEntry.tipoGS == 0 || oEntry.tipoGS == 46 || oEntry.tipoGS == 148 || oEntry.tipoGS == 150)) {
                            if (bDetalle) sInput = "DESG_" + oEntry.idCampoPadre_origen + "_" + oEntry.campo_origen + "_" + iLinea + "_" + oEntry.tipo.toString()
                            else sInput = "DESG_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                            anyadirInputFav(sInput, oEntry.getValue())

                            if (bDetalle) sInput = "DESGID_" + oEntry.idCampoPadre_origen + "_" + oEntry.campo_origen + "_" + iLinea + "_" + oEntry.tipo.toString()
                            else sInput = "DESGID_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()

                            anyadirInputFav(sInput, oEntry.getDataValue())
                        } else {
                            if (bDetalle) sInput = "DESG_" + oEntry.idCampoPadre_origen + "_" + oEntry.campo_origen + "_" + iLinea + "_" + oEntry.tipo.toString()
                            else sInput = "DESG_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()

                            if (oEntry.tipo == 8) //archivo
                                anyadirInputFav(sInput, oEntry.getValue())
                            else if ((oEntry.tipoGS == 104) || (oEntry.tipoGS == 119)) //CodArticulo Soliciatudes anteriores
                            {

                                if (oEntry.getDataValue() == null || oEntry.getDataValue() == "") {
                                    s = oEntry.getValue()
                                    s = s.replace(" - ", "")
                                    anyadirInputFav(sInput, s)
                                } else {
                                    anyadirInputFav(sInput, oEntry.getDataValue())
                                    //////////////////
                                    oMat = fsGeneralEntry_getById(oEntry.dependentfield)
                                    if (!oMat) //el material est� oculto
                                    {
                                        if (bDetalle) sInputMat = "DESG_" + oEntry.idCampoPadre_origen + "_" + (oEntry.campo_origen - 1) + "_" + iLinea + "_" + oEntry.tipo.toString()
                                        else sInputMat = "DESG_" + oEntry.idCampoPadre + "_" + oEntry.dependentfield.substring(oEntry.dependentfield.lastIndexOf("_") + 1) + "_" + iLinea + "_" + oEntry.tipo.toString()
                                        anyadirInputFav(sInputMat, oEntry.Dependent.value)

                                    }

                                    oUni = fsGeneralEntry_getById(oEntry.idEntryUNI)
                                    if (!oUni) { //La Unidad est� oculta

                                        if (bDetalle) sInputUni = "DESG_" + oEntry.idCampoPadre_origen + "_" + (oEntry.campo_origen + 4) + "_" + iLinea + "_" + oEntry.tipo.toString()
                                        else sInputUni = "DESG_" + oEntry.idCampoPadre + "_" + oEntry.idEntryUNI.substring(oEntry.idEntryUNI.lastIndexOf("_") + 1) + "_" + iLinea + "_" + oEntry.tipo.toString()
                                        anyadirInputFav(sInputUni, oEntry.UnidadDependent.value)
                                    }
                                    /////////////////
                                }
                            } else if (oEntry.tipoGS == 110 || oEntry.tipoGS == 111 || oEntry.tipoGS == 112 || oEntry.tipoGS == 113) {
                                if (bDetalle) sInput = "DESG_" + oEntry.idCampoPadre_origen + "_" + oEntry.campo_origen + "_" + iLinea + "_1"
                                else sInput = "DESG_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_1"
                                anyadirInputFav(sInput, oEntry.getDataValue())
                            } else if (oEntry.tipoGS == 42) { //Estado actual	
                                if (bDetalle) sInput = "DESG_" + oEntry.idCampoPadre_origen + "_" + 1000 + "_" + iLinea + "_" + oEntry.campo_origen + "_1"
                                else sInput = "DESG_" + oEntry.idCampoPadre + "_" + 1000 + "_" + iLinea + "_" + oEntry.idCampo + "_1"

                                anyadirInputFav(sInput, oEntry.getDataValue())
                            } else {

                                vValor = oEntry.dataValue
                                if (vValor == null) {
                                    vValor = oEntry.getDataValue()
                                    if (vValor == null) vValor = ""
                                } else {
                                    if (TieneMasDecimalesQueEsteUsuario(oEntry.dataValue, vprecisionfmt, 1)) {
                                        formateado = num2strcalc(oEntry.dataValue, vdecimalfmt, vthousanfmt, vprecisionfmt, 1)
                                        formateado2 = redondeacalc(oEntry.dataValue, vprecisionfmt, 1)
                                        if ((formateado == oEntry.getValue()) || (formateado2 == oEntry.getValue())) {
                                            if (oEntry.dataValue.toString().indexOf(',') != -1) vValor = oEntry.dataValue.replace(",", ".")
                                            else vValor = oEntry.dataValue
                                        } else vValor = oEntry.getDataValue()
                                    } else vValor = oEntry.getDataValue()
                                    //}
                                }
                                anyadirInputFav(sInput, vValor)
                            }
                        }
                        //hay que obtener la l�nea con la que estaba guardado en  bd				
                        sPre = oEntry.id.substr(0, x - 10)
                        sAux = sPre.split("_")
                        var sCampoHijo = null
                        if (sAux.length == 7) {
                            sCampoHijo = sAux[sAux.length - 2]
                            if (sCampoHijo) sPre = sPre.substr(0, sPre.length - (sCampoHijo.length + 2))
                        }
                        sPre = replaceAll(sPre, "__", "::")
                        sPre = replaceAll(sPre, "_", "$")
                        sPre = replaceAll(sPre, "::", "$_")
                        var iLineaOld
                        if (!sCampoHijo) {
                            if (f2.elements[sPre + "_" + iLinea + "_Linea"]) iLineaOld = f2.elements[sPre + "_" + iLinea + "_Linea"].value
                            else iLineaOld = null
                        } else {
                            if (f2.elements[sPre + "_" + sCampoHijo + "$_" + iLineaOriginal + "_Linea"]) iLineaOld = f2.elements[sPre + "_" + sCampoHijo + "$_" + iLineaOriginal + "_Linea"].value
                            else iLineaOld = null
                        }
                        //input que dir� la l�nea de bd: IdCampoPadre_Linea
                        if (bDetalle) sInput = "LINDESG_" + oEntry.idCampoPadre_origen + "_" + iLinea
                        else sInput = "LINDESG_" + oEntry.idCampoPadre + "_" + iLinea

                        anyadirInputFav(sInput, iLineaOld)

                        if (oEntry.tipo == 8) {

                            var inputFiles = document.getElementById(oEntry.id + "__hNew")
                            files = inputFiles.value.split("xx")
                            for (file in files) {
                                if (bDetalle) sInput = "ADJUNDESG_" + oEntry.idCampoPadre_origen + "_" + oEntry.campo_origen + "_" + iLinea.toString() + "_2_" + file.toString()
                                else sInput = "ADJUNDESG_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea.toString() + "_2_" + file.toString()
                                anyadirInputFav(sInput, files[file])

                            }
                            inputFiles = document.getElementById(oEntry.id + "__hAct")
                            files = inputFiles.value.split("xx")
                            for (file in files) {
                                if (bDetalle) //entra aqui cuando estamos en NWDetalleSolicitud.aspx y ya ha guardado un adjunto previamente en NWAlta.aspx. En este caso file contiene el id de copia_linea_desglose_adjun.
                                    sInput = "ADJUNDESG_" + oEntry.idCampoPadre_origen + "_" + oEntry.campo_origen + "_" + iLinea.toString() + "_0_" + file.toString()
                                else sInput = "ADJUNDESG_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea.toString() + "_1_" + file.toString()
                                anyadirInputFav(sInput, files[file])

                            }
                        }
                    }
                } else {
                    if (oEntry.tipo == 9 && oEntry.tipoGS != 136) //desglose
                    {
                        var i
                        i = 0
                        for (k = 1; k <= f2.elements[oEntry.id + "__numTotRows"].value; k++) {
                            oNoDeleted = f2.elements[oEntry.id + "__" + k.toString() + "__Deleted"]
                            if (oNoDeleted) i++

                            oLinea = f2.elements[oEntry.id + "__" + k.toString() + "__Linea"]
                            if (oLinea) {
                                //input que dir� la l�nea de bd: IdCampoPadre_<LineaOld>_Linea
                                if (bDetalle) sInput = "LINDESG_" + oEntry.campo_origen + "_" + i.toString()
                                else sInput = "LINDESG_" + oEntry.idCampo + "_" + i.toString()
                                anyadirInputFav(sInput, oLinea.value)
                            }
                            for (arrCampoHijo in oEntry.arrCamposHijos) {
                                oHijo = f2.elements[oEntry.id + "__" + k.toString() + "__" + oEntry.arrCamposHijos[arrCampoHijo]]
                                if (oHijo) {
                                    oHijoSrc = f2.elements[oEntry.id + "__" + oEntry.arrCamposHijos[arrCampoHijo]]
                                    if (oHijoSrc.getAttribute("intro") == 1 && (oHijoSrc.getAttribute("tipo") == 1 || oHijoSrc.getAttribute("tipo") == 5 || oHijoSrc.getAttribute("tipo") == 6 || oHijoSrc.getAttribute("tipo") == 7) && (oHijoSrc.getAttribute("tipoGS") == 0 || oHijoSrc.getAttribute("tipoGS") == 46)) {
                                        if (bDetalle) sInput = "DESGID_" + oEntry.campo_origen + "_" + oEntry.arrCamposHijos_origen[arrCampoHijo] + "_" + i.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                        else sInput = "DESGID_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + i.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                        anyadirInputFav(sInput, oHijo.value)
                                    } else {
                                        if (bDetalle) sInput = "DESG_" + oEntry.campo_origen + "_" + oEntry.arrCamposHijos_origen[arrCampoHijo] + "_" + i.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                        else sInput = "DESG_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + i.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()

                                        oAdjun = f2.elements[oEntry.id + "__" + k.toString() + "__" + oEntry.arrCamposHijos[arrCampoHijo] + "_hNew"]
                                        if (oAdjun) {
                                            oAdjunNombre = f2.elements[oEntry.id + "__" + k.toString() + "__" + oEntry.arrCamposHijos[arrCampoHijo] + "_hNombre"]
                                            if (oAdjunNombre) anyadirInputFav(sInput, oAdjunNombre.value)
                                            else anyadirInputFav(sInput, "")

                                            //sra: adjuntos desglose pop-up
                                            var inputFiles = f2.elements[oEntry.id + "__" + k.toString() + "__" + oEntry.arrCamposHijos[arrCampoHijo] + "_hNew"]
                                            files = inputFiles.value.split("xx")
                                            for (file in files) {
                                                if (bDetalle) sInput = "ADJUNDESG_" + oEntry.campo_origen + "_" + oEntry.arrCamposHijos_origen[arrCampoHijo] + "_" + i.toString() + "_2_" + file.toString()
                                                else sInput = "ADJUNDESG_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + i.toString() + "_2_" + file.toString()
                                                anyadirInputFav(sInput, files[file])
                                            }

                                            inputFiles = f2.elements[oEntry.id + "__" + k.toString() + "__" + oEntry.arrCamposHijos[arrCampoHijo]]
                                            files = inputFiles.value.split("xx")
                                            for (file in files) {
                                                if (bDetalle) //entra aqui cuando estamos en NWDetalleSolicitud.aspx y ya ha guardado un adjunto previamente en NWAlta.aspx. En este caso file contiene el id de copia_linea_desglose_adjun.
                                                    sInput = "ADJUNDESG_" + oEntry.campo_origen + "_" + oEntry.arrCamposHijos_origen[arrCampoHijo] + "_" + i.toString() + "_0_" + file.toString()
                                                else sInput = "ADJUNDESG_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + i.toString() + "_1_" + file.toString()
                                                anyadirInputFav(sInput, files[file])
                                            }

                                            //fin adjuntos										
                                        } else if (oHijoSrc.getAttribute("tipoGS") == 110 || oHijoSrc.getAttribute("tipoGS") == 111 || oHijoSrc.getAttribute("tipoGS") == 112 || oHijoSrc.getAttribute("tipoGS") == 113) {
                                            //los presupuestos 
                                            if (bDetalle) sInput = "DESG_" + oEntry.campo_origen + "_" + oEntry.arrCamposHijos_origen[arrCampoHijo] + "_" + i.toString() + "_1"
                                            else sInput = "DESG_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + i.toString() + "_1"

                                            anyadirInputFav(sInput, oHijo.value)
                                        } else if (oHijoSrc.getAttribute("tipoGS") == 42) { //Estado actual
                                            if (bDetalle) sInput = "DESG_" + oEntry.campo_origen + "_" + 1000 + "_" + i.toString() + "_" + oEntry.arrCamposHijos_origen[arrCampoHijo] + "_1"
                                            else sInput = "DESG_" + oEntry.idCampo + "_" + 1000 + "_" + i.toString() + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_1"

                                            anyadirInputFav(sInput, oHijo.value)
                                        } else anyadirInputFav(sInput, oHijo.value)
                                    }
                                }
                            }
                        }
                    } else {
                        if (oEntry.tipo == 8) {

                            var inputFiles = document.getElementById(oEntry.id + "__hNew")
                            files = inputFiles.value.split("xx")
                            for (file in files) {
                                if (bDetalle) sInput = "ADJUN_" + oEntry.campo_origen + "_2_" + file.toString()
                                else sInput = "ADJUN_" + oEntry.idCampo + "_2_" + file.toString()
                                anyadirInputFav(sInput, files[file])

                            }
                            inputFiles = document.getElementById(oEntry.id + "__hAct")
                            files = inputFiles.value.split("xx")
                            for (file in files) {
                                if (bDetalle) sInput = "ADJUN_" + oEntry.campo_origen + "_1_" + file.toString()
                                else sInput = "ADJUN_" + oEntry.idCampo + "_1_" + file.toString()
                                anyadirInputFav(sInput, files[file])

                            }
                            if (bDetalle) sInput = "CAMPO_" + oEntry.campo_origen + "_" + oEntry.tipo.toString()
                            else sInput = "CAMPO_" + oEntry.idCampo + "_" + oEntry.tipo.toString()

                            anyadirInputFav(sInput, oEntry.getValue())
                        } else if (oEntry.idCampo != "PROVE" && oEntry.idCampo != "SOLICIT" && oEntry.idCampo != "CON")
                            //campos simples
                        {
                            if (bDetalle) sInput = "CAMPO_" + oEntry.campo_origen + "_" + oEntry.tipo.toString()
                            else sInput = "CAMPO_" + oEntry.idCampo + "_" + oEntry.tipo.toString()

                            if (oEntry.intro == 1 && (oEntry.tipo == 1 || oEntry.tipo == 5 || oEntry.tipo == 6 || oEntry.tipo == 7) && (oEntry.tipoGS == 0 || oEntry.tipoGS == 46 || oEntry.tipoGS == 148 || oEntry.tipoGS == 150)) {

                                anyadirInputFav(sInput, oEntry.getValue())
                                if (bDetalle) sInput = "CAMPOID_" + oEntry.campo_origen + "_" + oEntry.tipo.toString()
                                else sInput = "CAMPOID_" + oEntry.idCampo + "_" + oEntry.tipo.toString()

                                anyadirInputFav(sInput, oEntry.getDataValue())
                            } else if ((oEntry.tipoGS == 104) || (oEntry.tipoGS == 119) || (oEntry.tipoGS == 118)) //CodArticulo Solicitudes anteriores
                            {
                                if (oEntry.getDataValue() == null || oEntry.getDataValue() == "") {
                                    s = oEntry.getValue()
                                    s = s.replace(" - ", "")
                                    anyadirInputFav(sInput, s)
                                } else {
                                    anyadirInputFav(sInput, oEntry.getDataValue())
                                    //////////////////
                                    oMat = fsGeneralEntry_getById(oEntry.dependentfield)
                                    if (!oMat) //el material est� oculto
                                    {
                                        if (oEntry.tipoGS == 119) {
                                            if (bDetalle) sInputMat = "CAMPOID_" + (oEntry.campo_origen - 1) + "_" + oEntry.tipo.toString()
                                            else sInputMat = "CAMPOID_" + (oEntry.idCampo - 1) + "_" + oEntry.tipo.toString()
                                        }
                                        if (oEntry.tipoGS == 118) {
                                            if (bDetalle) sInputMat = "CAMPOID_" + (oEntry.campo_origen - 2) + "_" + oEntry.tipo.toString()
                                            else sInputMat = "CAMPOID_" + (oEntry.idCampo - 2) + "_" + oEntry.tipo.toString()
                                        }
                                        anyadirInputFav(sInputMat, oEntry.Dependent.value)

                                    }
                                }
                            } else if (oEntry.tipoGS == 117) //Rol
                            {
                                //En MontarFormularioSubmit no hay nada.
                                sInput = "ROL_" + oEntry.getDataValue()
                                //anyadirInputFav(sInput,oEntry.getDataValue())
                            } else if (oEntry.tipoGS == 115 || oEntry.tipoGS == 116) //Participantes
                            {
                                if (oEntry.intro == 1) {
                                    oRol = fsGeneralEntry_getById(oEntry.dependentfield)
                                    sRol = oRol.getDataValue()
                                    sInput = "PART_" + oEntry.tipoGS.toString() + "_" + sRol
                                    anyadirInputFav(sInput, oEntry.getDataValue())
                                } else {
                                    if (bDetalle) sInput = "CAMPO_" + oEntry.campo_origen + "_" + oEntry.tipo.toString()
                                    else sInput = "CAMPO_" + oEntry.idCampo + "_" + oEntry.tipo.toString()

                                    anyadirInputFav(sInput, oEntry.getDataValue())
                                }
                            } else if (oEntry.tipoGS == 110 || oEntry.tipoGS == 111 || oEntry.tipoGS == 112 || oEntry.tipoGS == 113) {
                                //los presupuestos 
                                if (bDetalle) sInputDet = "CAMPO_" + oEntry.campo_origen + "_1"
                                else sInput = "CAMPO_" + oEntry.idCampo + "_1"
                                anyadirInputFav(sInput, oEntry.getDataValue())
                            } else {
                                vValor = oEntry.dataValue
                                if (vValor == null) {
                                    vValor = oEntry.getDataValue()
                                    if (vValor == null) vValor = ""
                                } else {
                                    if (TieneMasDecimalesQueEsteUsuario(oEntry.dataValue, vprecisionfmt, 1)) {
                                        formateado = num2strcalc(oEntry.dataValue, vdecimalfmt, vthousanfmt, vprecisionfmt, 1)
                                        formateado2 = redondeacalc(oEntry.dataValue, vprecisionfmt, 1)
                                        if ((formateado == oEntry.getValue()) || (formateado2 == oEntry.getValue())) {
                                            if (oEntry.dataValue.toString().indexOf(',') != -1) vValor = oEntry.dataValue.replace(",", ".")
                                            else vValor = oEntry.dataValue
                                        } else {
                                            vValor = oEntry.getDataValue()
                                        }
                                    } else {
                                        vValor = oEntry.getDataValue()
                                    }
                                    //}
                                }
                                anyadirInputFav(sInput, vValor)
                            }
                        }
                    }
                }
            }
        }
    }
};
/*
''' <summary>
''' Crear un input tipo hidden con el nombre pasado en el param sInput y el valor pasado.
''' </summary>
''' <param name="sInput">nombre del hidden que se crea</param>
''' <param name="valor">valor del hidden que se crea</param>   
''' <returns>Nada</returns>
''' <remarks>Llamada desde: MontarFormularioFavoritos</remarks>
*/
function anyadirInputFav(sInput, valor) {
    if ((valor == "null") || (valor == null)) valor = ""
    var f = document.forms["frmSubmitFav"]
    if (!f.elements[sInput]) {
        f.insertAdjacentHTML("beforeEnd", "<INPUT type=hidden name=" + sInput + ">\n")
        f.elements[sInput].value = valor
    };
};
/* cuando no hay dataentrys y los campos son de solo lectura
''' <summary>
''' Abre el popup para insertar el comentario   
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Todos los dataentry de tipo texto medio,largo y string ; Tiempo m�ximo: instantaneo</remarks>
*/
function AbrirPopUpTexto(titulo, textSourceId, bLargeSize, textoPopUp) {
    var texto;
    var hfTextSource = document.getElementById(textSourceId);
    if (textoPopUp != null) {
        texto = textoPopUp;
    } else {
        if (hfTextSource != null) {
            if (hfTextSource.value != null) {
                texto = hfTextSource.value;
            } else {
                texto = hfTextSource.innerText;
            }
        }
    }
    if (titulo == null) titulo = '';
    if (texto == null) texto = '';
    if (bLargeSize) {
        var MySize = "dialogHeight:650px;dialogWidth:770px"
        var MyArgs = new Array(titulo, texto, "600px", "770px");
    } else {
        var MySize = "dialogHeight:220px;dialogWidth:500px"
        var MyArgs = new Array(titulo, texto, "170px", "485px");
    }
    window.showModalDialog("../_common/popUpTexto.aspx", MyArgs, MySize);
};
function AbrirFicherosAdjuntos(sValor, idCampo, sInstancia, sTipo) {
    var newWindow = window.open("atachedfiles.aspx?Valor=" + sValor + "&Valor2=&Campo=" + idCampo + "&tipo=" + sTipo + "&Instancia=" + sInstancia + "&readOnly=1", "_blank", "width=750,height=330,status=yes,resizable=no,top=200,left=200")
    newWindow.focus();
};
function IrAAdjunto(urlcompleta) {
    var NombreArchivo = urlcompleta.split("&nombre=")[1].split("&datasize=")[0]
    var NombreArchivoCodificado = encodeURIComponent(NombreArchivo)
    var URLCodificada = urlcompleta.replace(NombreArchivo, NombreArchivoCodificado);
    var newWindow = window.open(URLCodificada, "_blank", "width=600,height=275,status=no,resizable=yes,top=200,left=200");
    newWindow.focus();
};
function AbrirPresupuestos(sValor, idCampo, sInstancia) {
    var newWindow = window.open("../_common/presupuestosreadonly.aspx?Valor=" + escape(sValor) + "&Campo=" + idCampo + "&Instancia=" + sInstancia, "_blank", "width=550,height= 250,status=yes,resizable=yes,top=100,left=120")
    newWindow.focus();
};
function AbrirMateriales(sValor) {
    var newWindow = window.open("../_common/materiales.aspx?Valor=" + sValor + "&readonly=1", "_blank", "width=550,height=550,status=yes,resizable=no,top=100,left=200")
    newWindow.focus();
};
function AbrirDesglose(idCampo, sInstancia, sVersion) {
    var newWindow = window.open("desglose.aspx?Campo=" + idCampo + "&Instancia=" + sInstancia + "&Version=" + sVersion + "&SoloLectura=1&BotonCalcular=1&Observador=1", "windesglose", "width=750,height=315,status=no,resizable=yes,top=200,left=200")
    newWindow.focus();
};
function AbrirTablaExterna(tabla, valor) {
    var newWindow = window.open("../_common/tablaexternareg.aspx?tabla=" + tabla + "&valor=" + valor, "wtablaexterna", "width=700,height=275,status=yes,resizable=no,top=200,left=200");
    newWindow.focus();
};
function AbrirDetallePartida(titulo, pres5, texto) {
    var newWindow = window.open("../_common/detallePartida.aspx?titulo=" + titulo + "&pres5=" + pres5 + "&texto=" + escape(texto), "_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250")
    newWindow.focus();
};
function AbrirDetalleActivo(valor) {
    var newWindow = window.open(rutaFS + "SM/Activos/detalleactivo.aspx?&codActivo=" + escape(valor), "_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");
    newWindow.focus();
};
function borrarPresupuestos(idFSEntryCod) {
    var e = null;
    var oArt = null;
    p = window.document;
    idFSEntryCod = idFSEntryCod.replace("__t", "");
    e = p.getElementById(idFSEntryCod + "__tbl")
    if (e) oArt = e.Object;
    if (oArt) {
        if (oArt.idCampoPadre) {
            var desglose = oArt.id.substr(0, oArt.id.search("_fsdsentry"))
            var index = oArt.id.substr(oArt.id.search("_fsdsentry_") + 11, oArt.id.search(oArt.idCampo) - oArt.id.search("_fsdsentry_") - 12)
            var oEntry = null;
            for (i = 0; i < p.getElementById(desglose + "_tblDesgloseHidden").rows[0].cells.length; i++) {
                s = p.getElementById(desglose + "_tblDesgloseHidden").rows[0].cells[i].innerHTML
                re = /fsentry/

                x = s.search(re)
                y = s.search("__tbl")

                id = s.substr(x + 7, y - x - 7)
                if (id != "") oEntry = fsGeneralEntry_getById(desglose + "_fsentry" + id)

                if (oEntry) {
                    if ((oEntry.tipoGS == 110) || (oEntry.tipoGS == 111) || (oEntry.tipoGS == 112) || (oEntry.tipoGS == 113)) {
                        var pres = oEntry.id
                        oDSEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + fsGeneralEntry_getById(pres).idCampo)

                        oDSEntry.setValue("");
                        oDSEntry.setDataValue("");
                    }
                }
            }
        }
    }
};
/*
''' <summary>
''' Validar fechas
'''	''' </summary>
''' <param name="oEdit">Campo</param> 
''' <param name="oldValue">Valor que tenia el entry anteriormente</param> 
''' <param name="oEvent"></param> 
''' <returns>cuando alguna de las fechas cambia</returns>
''' <remarks>Llamada desde:GeneralEntry.vb ; Tiempo m�ximo:0 </remarks>
*/
function ddFec_ValueChange(oEdit, oldValue, oEvent) {
    var a = new Array();
    var s;
    if (oEdit.fsEntry.getValue() != null) {
        s = String(oEdit.fsEntry.getValue());
        var dFecha = new Date(s);
        var Anyo = dFecha.getFullYear();
        if (Anyo < 1900 || Anyo > 2100) {
            alert(sMensajeFechaDefecto)
            oEdit.fsEntry.setDataValue("");
            oEdit.fsEntry.setValue("");
            oEdit.setValue("");
            oEdit.setText("");
            oEdit.setDate("");
            oEdit.old = null;
        }
    }
};
/*
''' <summary>
''' Validar fechas
'''	''' </summary>
''' <param name="oEdit">Campo</param> 
''' <param name="oldValue">Valor que tenia el entry anteriormente</param> 
''' <param name="oEvent"></param> 
''' <returns>cuando la fecha es anterior a la fecha del sistema</returns>
''' <remarks>Llamada desde:GeneralEntry.vb ; Tiempo m�ximo:0 </remarks>
*/
function ddFechaNoAnteriorAlSistema_ValueChange(oEdit, oldValue, oEvent) {
    if (oEdit.fsEntry.getValue() != null) {
        var dFechaActual = new Date();
        var s;
        s = oEdit.fsEntry.getValue()
        var dFecha = new Date(s)
        if ((dFechaActual.getYear() > dFecha.getYear()) || ((dFechaActual.getYear() == dFecha.getYear()) && (dFechaActual.getMonth() > dFecha.getMonth())) || ((dFechaActual.getYear() == dFecha.getYear()) && (dFechaActual.getMonth() == dFecha.getMonth()) && (dFechaActual.getDate() > dFecha.getDate()))) {
            alert(sFechaNoAnteriorAlSistema)
            oEdit.fsEntry.setDataValue("");
            oEdit.fsEntry.setValue("");
            oEdit.setValue("");
            oEdit.setText("");
            oEdit.setDate("");
            oEdit.old = null;
        }
    }
};
/*
''' <summary>
''' Determinar si todos los campos texto medio y texto largo cumplen con su longitud maxima
''' </summary>      
''' <returns>Si todos los campos texto medio y texto largo cumplen con su longitud maxima</returns>
''' <remarks>Llamada desde: noconformidades.aspx	certificados,aspx; Tiempo m�ximo:0</remarks>*/
function comprobarTextoLargo() {
    for (arrInput in arrInputs) {
        oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
        if (oEntry) {
            if (oEntry.intro == 0) {
                if (oEntry.tipo == 6 || oEntry.tipo == 7) {
                    if (parseInt(oEntry.maxLength) > 0) {
                        if (oEntry.getValue().length > parseInt(oEntry.maxLength)) {
                            var msg = oEntry.errMsg;
                            msg = msg.replace("\\n", "\n");
                            msg = msg.replace("\\n", "\n");
                            alert(oEntry.title + '. ' + msg + oEntry.maxLength);
                            return false;
                        }
                    }
                }
            }
        }
    };
    return true;
};
/*
''' <summary>
''' Funcion que abre la pantalla de los valores que se muestran en el autocompletado de los campos texto
''' </summary>      
''' <returns></returns>
''' <remarks>Llamada desde: GeneralEntry.vb; Tiempo m�ximo:0</remarks>*/
function AbrirListaValoresAutoCompletado(idCampo, denCampo) {
    var newWindow = window.open(rutaFS + "_common/listavalores.aspx?idCampo=" + idCampo + "&denCampo=" + denCampo, "_blank", "width=450,height=320,status=yes,resizable=no,top=200,left=250")
    newWindow.focus();
};
//Funci�n que limita el n�mero de decimales a los propios de cada unidad en los controles webNumericEdit
function limiteDecimalesTextChanged(oEdit, newText, oEvent) {
    var eventoPaste = false;
    var textoDecimales = newText;
    var avisoMaxDecimales = oEdit.fsEntry.errMsg
    var denUnidad = oEdit.fsEntry.UnidadOculta
    var numeroDecimales = oEdit.fsEntry.NumeroDeDecimales
    var separadorDecimales = oEdit.decimalSeparator

    if (numeroDecimales == null) numeroDecimales = 15
    oEdit.decimals = numeroDecimales

    if (oEvent) {
        if (oEvent.event) {
            if (oEvent.event.keyCode == 17) eventoPaste = true
        }
    };
    if (numeroDecimales >= 0 && separadorDecimales != null && textoDecimales != '') {
        if (textoDecimales.indexOf(separadorDecimales) > 0) {
            var textoEntero = textoDecimales.substr(0, textoDecimales.indexOf(separadorDecimales));
            textoDecimales = textoDecimales.slice(textoDecimales.indexOf(separadorDecimales) + 1)
            if (textoDecimales.length > numeroDecimales) {
                var old = oEdit.old;
                textoDecimales = textoDecimales.substr(0, numeroDecimales);
                oEdit.setText(textoEntero + separadorDecimales + textoDecimales);
                oEdit.lastText = oEdit.getText(); //Imprescindible modificar SIEMPRE el lastText para garantizar que ejecuta el evento TextChanged (si no, podr�a no modificarlo)
                oEdit.old = old;
                if (eventoPaste == true) {
                    alert(avisoMaxDecimales + ' ' + denUnidad + ':' + numeroDecimales);
                }
            } else {
                if (eventoPaste == true && textoDecimales.length == numeroDecimales) {
                    alert(avisoMaxDecimales + ' ' + denUnidad + ':' + numeroDecimales);
                }
            }
        } else {
            if (eventoPaste == true) {
                alert(avisoMaxDecimales + ' ' + denUnidad + ':' + numeroDecimales);
            }
        }
    }
};
//Funci�n que recoloca la posicion de la caja de valores del autocompleteextender en caso de scroll de la pagina 
function resetPosition(object, args) {
    var tb = object._element;
    var tbposition = findPositionWithScrolling2(tb);

    var xposition = tbposition[0];
    var yposition = tbposition[1] + 17; // 17 = textbox height + a few pixels spacing

    var ex = object._completionListElement;
    if (ex) {
        $common.setLocation(ex, new Sys.UI.Point(xposition, yposition));
    }
};
//Funci�n que retorna la posicion en la pagina del objeto
function findPositionWithScrolling2(oElement) {
    if (typeof (oElement.offsetParent) != 'undefined') {
        var originalElement = oElement;
        var oOffsetParent;
        //CALIDAD
        for (var posX = 0, posY = 0; oElement; oElement = oElement.parentElement) {
            if (oElement == oOffsetParent) {
                posX += oElement.offsetLeft;
                posY += oElement.offsetTop;
            }
            if (oElement != originalElement && oElement != document.body && oElement != document.documentElement) {
                posX -= oElement.scrollLeft;
                posY -= oElement.scrollTop;
            }
            if (oElement.offsetParent && oElement.offsetParent != '') oOffsetParent = oElement.offsetParent;
        }
        return [posX, posY];

    } else {
        return [oElement.x, oElement.y];
    }
};
var xmlHttp;
var mioEntryId;
//Funci�n que crea el objeto Ajax
function CreateXmlHttp() {
    // Probamos con IE
    try {
        // Funcionar� para JavaScript 5.0
        xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (oc) {
            xmlHttp = null;
        }
    };
    // Si no se trataba de un IE, probamos con esto
    if (!xmlHttp && typeof XMLHttpRequest != "undefined") {
        xmlHttp = new XMLHttpRequest();
    };
    return xmlHttp;
};
//Funci�n que envia la peticion Ajax
function envioInfo(oEntryID, Id, nom, com, datasize, comsalto, origen, IdContrato) {
    var entryId;
    entryId = oEntryID;
    while (entryId.indexOf('$') >= 0)
        entryId = entryId.replace("$", "_");
    oEntry = fsGeneralEntry_getById(entryId)
    mioEntryId = oEntryID;
    var instancia = document.getElementById(oEntryID + '__hInstancia').value;
    var tipo = document.getElementById(oEntryID + '__hTipo').value
    var adjunto = document.getElementById(oEntryID + '__hAct').value
    var adjuntoNew = document.getElementById(oEntryID + '__hNew').value
    var nombre = document.getElementById(oEntryID + '__hNombre').value
    if (nombre == '') {
        nombre = nom;
    } else {
        nombre = nombre + '; ' + nom;
    }
    document.getElementById(oEntryID + '__hNombre').value = nombre
    var idCampo = document.getElementById(oEntryID + '__hCampo').value

    if (adjuntoNew == '') {
        adjuntoNew = Id;
        document.getElementById(oEntryID + '__hNew').value = adjuntoNew;
    } else {
        adjuntoNew = adjuntoNew + 'xx' + Id;
        document.getElementById(oEntryID + '__hNew').value = adjuntoNew;
    };
    // 1.- Creamos el objeto xmlHttpRequest
    CreateXmlHttp();
    // 2.- Definimos la llamada para hacer un simple GET.
    var ajaxRequest = rutaPM + 'GestionaAjax.aspx?accion=1&EntryID=' + oEntryID + '&instancia=' + instancia + '&tipo=' + tipo + '&nombre=' + nombre + '&adjunto=' + adjunto + '&adjuntoNew=' + adjuntoNew + '&campo=' + idCampo + '&readOnly=' + oEntry.readOnly.toString() + '&origen=""&IdContrato=' + IdContrato.toString() + '&defecto=' + oEntry.DataValorDefecto
    // 3.- Marcar qu� funci�n manejar� la respuesta
    xmlHttp.onreadystatechange = recogeInfo;
    // 4.- Enviar
    xmlHttp.open("GET", ajaxRequest, true);
    xmlHttp.send("");
};
//Funci�n que envia la peticion Ajax desde la pagina de detalle de adjuntos 
function envioInfoAdj(oEntryID, Id, nom, com, datasize, comsalto, origen) {
    var entryId;
    entryId = oEntryID;
    p = window.opener
    oEntry = p.fsGeneralEntry_getById(entryId)
    mioEntryId = oEntryID;

    var instancia = p.document.getElementById(oEntryID + '__hInstancia').value;
    var tipo = p.document.getElementById(oEntryID + '__hTipo').value
    var adjunto = p.document.getElementById(oEntryID + '__hAct').value
    var adjuntoNew = p.document.getElementById(oEntryID + '__hNew').value
    var nombre = p.document.getElementById(oEntryID + '__hNombre').value
    if (nombre == '') {
        nombre = nom;
    } else {
        nombre = nombre + '; ' + nom;
    };
    var idCampo = p.document.getElementById(oEntryID + '__hCampo').value

    if (adjuntoNew == '') {
        adjuntoNew = Id;
    } else {
        adjuntoNew = adjuntoNew + 'xx' + Id;
    };
    // 1.- Creamos el objeto xmlHttpRequest
    CreateXmlHttp();
    // 2.- Definimos la llamada para hacer un simple GET.
    var ajaxRequest = rutaPM + 'GestionaAjax.aspx?accion=2&EntryID=' + oEntryID + '&instancia=' + instancia + '&tipo=' + tipo + '&nombre=' + nombre + '&adjunto=' + adjunto + '&adjuntoNew=' + adjuntoNew + '&campo=' + idCampo + '&readOnly=' + oEntry.readOnly.toString() + '&origen=' + origen.toString() + '&defecto=' + oEntry.DataValorDefecto;
    // 3.- Marcar qu� funci�n manejar� la respuesta
    xmlHttp.onreadystatechange = recogeInfo;
    // 4.- Enviar
    xmlHttp.open("GET", ajaxRequest, true);
    xmlHttp.send("");
};
//Funci�n que envia la peticion Ajax
function envioInfoAdjuntoContrato(oEntryID, Id, nom, com, datasize, comsalto, origen, IdContrato, PathAdjunto) {
    var entryId;
    entryId = oEntryID;
    while (entryId.indexOf('$') >= 0)
        entryId = entryId.replace("$", "_");
    oEntry = fsGeneralEntry_getById(entryId)
    mioEntryId = oEntryID;
    var instancia = document.getElementById(oEntryID + '__hInstancia').value;
    var tipo = document.getElementById(oEntryID + '__hTipo').value
    var adjunto = document.getElementById(oEntryID + '__hAct').value
    document.getElementById(oEntryID + '__hAct').value = ''
    var nombre = document.getElementById(oEntryID + '__hNombre').value
    if (nombre == '') {
        nombre = nom;
    } else {
        nombre = nombre + '; ' + nom;
    };
    document.getElementById(oEntryID + '__hNombre').value = nombre
    var Path_Contrato = document.getElementById('hid_pathContrato')
    if (Path_Contrato) {
        Path_Contrato.value = PathAdjunto
    };
    var Nom_Contrato = document.getElementById('hid_NombreContrato')
    if (Nom_Contrato) {
        Nom_Contrato.value = nom
    };
    var idCampo = document.getElementById(oEntryID + '__hCampo').value

    adjunto = Id;
    document.getElementById(oEntryID + '__hAct').value = adjunto;
    // 1.- Creamos el objeto xmlHttpRequest
    CreateXmlHttp();
    // 2.- Definimos la llamada para hacer un simple GET.
    var ajaxRequest = rutaPM + 'GestionaAjax.aspx?idArchivoContrato=' + Id + '&EsContrato=1&accion=1&EntryID=' + oEntryID + '&instancia=' + instancia + '&tipo=' + tipo + '&nombre=' + nombre + '&adjunto=' + adjunto + '&adjuntoNew=&campo=' + idCampo + '&readOnly=' + oEntry.readOnly.toString() + '&origen=""&IdContrato=' + IdContrato.toString() + '&defecto=' + oEntry.DataValorDefecto + '&datasize=' + datasize
    // 3.- Marcar qu� funci�n manejar� la respuesta
    xmlHttp.onreadystatechange = recogeInfo;
    // 4.- Enviar
    xmlHttp.open("GET", ajaxRequest, true);
    xmlHttp.send("");
};
//Funci�n que recoge la respuesta de la peticion Ajax
function recogeInfo() {
    if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
        var div;
        while (mioEntryId.indexOf('$') >= 0)
            mioEntryId = mioEntryId.replace("$", "_");
        div = document.getElementById(mioEntryId + "_divcontenedor")
        div.innerHTML = xmlHttp.responseText;

        //recojo los nombres de todos los adjuntos y los asigno al editor del Entry
        if (document.getElementById(mioEntryId + '__hNombre')) {
            hid_NomAdjuntos = document.getElementById(mioEntryId + "__hidNombresAdj").value
            document.getElementById(mioEntryId + '__hNombre').value = hid_NomAdjuntos
        }
    }
};
/*
''' <summary>
''' Funcion que abre la pantalla para adjuntar un fichero
''' </summary>    
''' <param name="instancia">instancia</param> 
''' <param name="adjuntos">adjuntos que tiene la solicitud</param> 
''' <param name="tipo">tipo del adjunto</param>  
''' <param name="entryId">Id del dataentry</param>  
''' <param name="Origen">origen desde donde se llama a la funcion</param>  
''' <returns></returns>
''' <remarks>Llamada desde: GeneralEntry.vb; Tiempo m�ximo:0</remarks>*/
function show_atach_file(instancia, adjuntos, tipo, entryId, origen, idContrato, nombreAdjunto) {
    var newWindow = window.open(rutaPM + "_common/attachfile.aspx?instancia=" + instancia + "&id=" + adjuntos.toString() + "&tipo=" + tipo.toString() + "&entryId=" + entryId.toString() + "&origen=" + origen.toString() + "&IdContrato=" + idContrato.toString() + "&nombre=" + escape(nombreAdjunto), "_blank", "width=600,height=275,status=no,resizable=yes,top=200,left=200")
    newWindow.focus();
};
function show_atach_file_Contrato(instancia, idAdjuntoContrato, entryId, origen, idContrato, nombreAdjunto) {
    var newWindow = window.open(rutaPM + "_common/attachfile.aspx?EsContrato=1&instancia=" + instancia + "&idContrato=" + idContrato.toString() + "&entryId=" + entryId.toString() + "&origen=" + origen.toString() + "&nombre=" + escape(nombreAdjunto) + "&idAdjuntoContrato=" + idAdjuntoContrato.toString(), "_blank", "width=600,height=275,status=no,resizable=yes,top=200,left=200")
    newWindow.focus();
};
/*
''' <summary>
''' Funcion que abre la pantalla de detalle para adjuntar un fichero o eliminar/sustituit los existentes
''' </summary>    
''' <param name="instancia">instancia</param> 
''' <param name="adjuntos">adjuntos que tiene la solicitud</param> 
''' <param name="tipo">tipo del adjunto</param>  
''' <param name="entryId">Id del dataentry</param>  
''' <param name="Origen">origen desde donde se llama a la funcion</param>  
''' <returns></returns>
''' <remarks>Llamada desde: GeneralEntry.vb; Tiempo m�ximo:0</remarks>*/
function show_atach_fileDesglose(instancia, adjuntos, adjuntosNew, tipo, entryId) {
    oInput = fsGeneralEntry_getById(entryId)
    var adjuntosAct = document.getElementById(entryId + '__hAct').value
    var adjuntosNew = document.getElementById(entryId + '__hNew').value
    var solicitud = document.getElementById('Solicitud');
    var idSolicitud
    if (solicitud) idSolicitud = solicitud.value
    else idSolicitud = 0
    var newWindow = window.open(rutaPM + "_common/atachedfilesDesglose.aspx?solicitud=" + idSolicitud.toString() + "&instancia=" + instancia + "&adjuntos=" + adjuntosAct + "&adjuntosNew=" + adjuntosNew.toString() + "&tipo=" + tipo.toString() + "&entryId=" + entryId.toString() + '&readOnly=' + oInput.readOnly.toString() + '&nombreCampo=' + encodeURI(oInput.title.toString()) + '&idCampo=' + oInput.idCampo.toString() + '&defecto=' + oInput.DataValorDefecto, "_blank", "width=800,height=300,status=yes,resizable=yes,top=200,left=250,scrollbars=yes") //"_blank","width=800,height=600,status=no,resizable=yes,top=200,left=200,scrollbars=yes")
    newWindow.focus();
};
/*
''' <summary>
''' Funcion que borra de la solicitud el adjunto indicado
''' </summary>    
''' <param name="idAdjunto">id del adjunto a eliminar</param> 
''' <param name="tipo">tipo del adjunto</param> 
''' <param name="instancia">instancia</param>  
''' <param name="entryId">Id del dataentry</param>  
''' <param name="Origen">origen desde donde se llama a la funcion</param>  
''' <returns></returns>
''' <remarks>Llamada desde: GeneralEntry.vb; Tiempo m�ximo:0</remarks>*/
function borrarAdjunto(idAdjunto, tipo, instancia, entryId, origen, idContrato) {
    mioEntryId = entryId;
    while (entryId.indexOf('$') >= 0)
        entryId = entryId.replace("$", "_");
    oInput = fsGeneralEntry_getById(entryId)
    oInput.removeFile(tipo, idAdjunto)

    if (typeof (idContrato) == 'undefined') idContrato = 0;

    var instancia = document.getElementById(entryId + '__hInstancia').value;
    var tipo = document.getElementById(entryId + '__hTipo').value
    var adjunto = document.getElementById(entryId + '__hAct').value
    var adjuntoNew = document.getElementById(entryId + '__hNew').value
    var nombre = document.getElementById(entryId + '__hNombre').value
    var idCampo = document.getElementById(entryId + '__hCampo').value

    // 1.- Creamos el objeto xmlHttpRequest
    CreateXmlHttp();

    // 2.- Definimos la llamada para hacer un simple GET.
    var ajaxRequest = rutaPM + 'GestionaAjax.aspx?accion=1&EntryID=' + mioEntryId + '&instancia=' + instancia + '&tipo=' + tipo + '&nombre=' + nombre + '&adjunto=' + adjunto + '&adjuntoNew=' + adjuntoNew + '&campo=' + idCampo + '&readOnly=' + oInput.readOnly.toString() + '&origen=' + origen.toString() + '&IdContrato=' + idContrato.toString() + '&defecto=' + oInput.DataValorDefecto;

    // 3.- Marcar qu� funci�n manejar� la respuesta
    xmlHttp.onreadystatechange = recogeInfo;

    // 4.- Enviar
    xmlHttp.open("GET", ajaxRequest, true);
    xmlHttp.send("");
};
/*
''' <summary>
''' Funcion que borra de la solicitud el adjunto indicado(de contrato)
''' </summary>    
''' <param name="idAdjunto">id del adjunto a eliminar</param> 
''' <param name="tipo">tipo del adjunto</param> 
''' <param name="instancia">instancia</param>  
''' <param name="entryId">Id del dataentry</param>  
''' <param name="Origen">origen desde donde se llama a la funcion</param>  
''' <returns></returns>
''' <remarks>Llamada desde: GeneralEntry.vb; Tiempo m�ximo:0</remarks>*/
function borrarAdjuntoContrato(idAdjunto, tipo, instancia, entryId, origen, idContrato) {
    mioEntryId = entryId;
    while (entryId.indexOf('$') >= 0)
        entryId = entryId.replace("$", "_");
    oInput = fsGeneralEntry_getById(entryId)
    oInput.removeFile(tipo, idAdjunto)

    if (typeof (idContrato) == 'undefined') idContrato = 0;

    //borramos el adjunto del contrato
    document.getElementById(entryId + '__hAct').value = ''

    var instancia = document.getElementById(entryId + '__hInstancia').value;
    var tipo = document.getElementById(entryId + '__hTipo').value
    var adjunto = document.getElementById(entryId + '__hAct').value
    var adjuntoNew = document.getElementById(entryId + '__hNew').value
    var nombre = document.getElementById(entryId + '__hNombre').value
    var idCampo = document.getElementById(entryId + '__hCampo').value

    //borramos el adjunto del contrato
    document.getElementById(entryId + '__hAct').value = ''
    //borramos el adjunto del contrato
    document.getElementById(entryId + '__hNew').value = ''

    // 1.- Creamos el objeto xmlHttpRequest
    CreateXmlHttp();

    // 2.- Definimos la llamada para hacer un simple GET.
    var ajaxRequest = rutaPM + 'GestionaAjax.aspx?Borrando=1&EsContrato=1&accion=1&EntryID=' + mioEntryId + '&instancia=' + instancia + '&tipo=' + tipo + '&nombre=' + nombre + '&adjunto=' + adjunto + '&adjuntoNew=' + adjuntoNew + '&campo=' + idCampo + '&readOnly=' + oInput.readOnly.toString() + '&origen=' + origen.toString() + '&IdContrato=' + idContrato.toString() + '&defecto=' + oInput.DataValorDefecto;

    // 3.- Marcar qu� funci�n manejar� la respuesta
    xmlHttp.onreadystatechange = recogeInfo;

    // 4.- Enviar
    xmlHttp.open("GET", ajaxRequest, true);
    xmlHttp.send("");
};
/*
''' <summary>
''' Funcion que borra de la solicitud el adjunto indicado llamado desde la pagina de detalle de adjuntos
''' </summary>    
''' <param name="sololectura">indica si se mostrara la pantalla en modo de solo lectura</param>  
''' <param name="entryId">Id del dataentry</param>  
''' <param name="Origen">origen desde donde se llama a la funcion</param>  
''' <returns></returns>
''' <remarks>Llamada desde: GeneralEntry.vb; Tiempo m�ximo:0</remarks>*/
function borrarAdjuntoDetalle(entryId, sololectura, origen) {
    mioEntryId = entryId;
    while (entryId.indexOf('$') >= 0)
        entryId = entryId.replace("$", "_");
    p = window.opener;
    oInput = p.fsGeneralEntry_getById(entryId)
    var p = window.opener
    var instancia = p.document.getElementById(entryId + '__hInstancia').value;
    var tipo = p.document.getElementById(entryId + '__hTipo').value
    var adjunto = p.document.getElementById(entryId + '__hAct').value
    var adjuntoNew = p.document.getElementById(entryId + '__hNew').value
    var nombre = p.document.getElementById(entryId + '__hNombre').value
    var idCampo = p.document.getElementById(entryId + '__hCampo').value

    // 1.- Creamos el objeto xmlHttpRequest
    CreateXmlHttp();

    // 2.- Definimos la llamada para hacer un simple GET.
    var ajaxRequest = rutaPM + 'GestionaAjax.aspx?accion=2&EntryID=' + entryId + '&instancia=' + instancia + '&tipo=' + tipo + '&nombre=' + nombre + '&adjunto=' + adjunto + '&adjuntoNew=' + adjuntoNew + '&campo=' + idCampo + '&readOnly=' + sololectura + '&origen=' + origen.toString() + '&defecto=' + oInput.DataValorDefecto;

    // 3.- Marcar qu� funci�n manejar� la respuesta
    xmlHttp.onreadystatechange = recogeInfo;

    // 4.- Enviar
    xmlHttp.open("GET", ajaxRequest, true);
    xmlHttp.send("");
};
/*
''' <summary>
''' Funcion que sustituye un adjunto
''' </summary>    
''' <param name="idadjunto">id del adjunto que sera sustituido</param>  
''' <param name="tipo">tipo del adjunto que sera sustituido</param>  
''' <param name="Origen">origen desde donde se llama a la funcion</param>  
''' <param name="entryId">id del DataEntry</param>  
''' <param name="instancia">instancia de la solicitud</param>  
''' <returns></returns>
''' <remarks>Llamada desde: GeneralEntry.vb; Tiempo m�ximo:0</remarks>*/
function sustituirAdjunto(idAdjunto, tipo, instancia, entryId, origen, nombreAdjunto) {
    mioEntryId = entryId;
    oInput = fsGeneralEntry_getById(entryId)
    oInput.removeFile(tipo, idAdjunto)

    show_atach_file(instancia, idAdjunto, tipo, mioEntryId, origen, 0, nombreAdjunto)
};
/*
''' <summary>
''' Funcion que sustituye un adjunto
''' </summary>    
''' <param name="idadjunto">id del adjunto que sera sustituido</param>  
''' <param name="tipo">tipo del adjunto que sera sustituido</param>  
''' <param name="Origen">origen desde donde se llama a la funcion</param>  
''' <param name="entryId">id del DataEntry</param>  
''' <param name="instancia">instancia de la solicitud</param>  
''' <returns></returns>
''' <remarks>Llamada desde: GeneralEntry.vb; Tiempo m�ximo:0</remarks>*/
function sustituirAdjuntoContrato(idAdjunto, tipo, instancia, entryId, origen, nombreAdjunto) {
    mioEntryId = entryId;
    oInput = fsGeneralEntry_getById(entryId)
    oInput.removeFile(tipo, idAdjunto)

    show_atach_file_Contrato(instancia, idAdjunto, mioEntryId, origen, 0, nombreAdjunto)
};
/*
''' <summary>
''' Funcion que sustituye un adjunto en la pantalla de detalle
''' </summary>    
''' <param name="idadjunto">id del adjunto que sera sustituido</param>  
''' <param name="tipo">tipo del adjunto que sera sustituido</param>  
''' <param name="Origen">origen desde donde se llama a la funcion</param>  
''' <param name="entryId">id del DataEntry</param>  
''' <param name="instancia">instancia de la solicitud</param>  
''' <returns></returns>
''' <remarks>Llamada desde: GeneralEntry.vb; Tiempo m�ximo:0</remarks>*/
function sustituirAdjuntoDetalle(idAdjunto, tipo, instancia, entryId, origen, nombreAdjunto) {
    mioEntryId = entryId;
    var p;
    p = window.opener;
    oInput = p.fsGeneralEntry_getById(entryId)
    oInput.removeFile(tipo, idAdjunto)

    show_atach_file(instancia, idAdjunto, tipo, mioEntryId, origen, 0, nombreAdjunto);
};
/*
''' <summary>
''' Funcion que llama a la pantalla que descarga el adjunto
''' </summary>    
''' <param name="idadjunto">id del adjunto a descargar</param>  
''' <param name="tipo">tipo del adjunto que sera sustituido</param>  
''' <param name="instancia">instancia de la solicitud</param>  
''' <returns></returns>
''' <remarks>Llamada desde: GeneralEntry.vb; Tiempo m�ximo:0</remarks>*/
function descargarAdjunto(idAdjunto, tipo, instancia, entryId) {
    var adjuntosNew
    var adjuntosAct
    var oadjuntosAct = document.getElementById(entryId + '__hAct')
    if (oadjuntosAct) adjuntosAct = oadjuntosAct.value
    else adjuntosAct = idAdjunto
    var oadjuntosNew = document.getElementById(entryId + '__hNew')
    if (oadjuntosNew) adjuntosNew = oadjuntosNew.value
    else adjuntosNew = ''
    if ((adjuntosNew != '' && adjuntosAct != '' && tipo == 3) || adjuntosAct.indexOf('xx') != -1 && tipo == 3 || adjuntosNew.indexOf('xx') != -1 && tipo == 3) {
        descargarAdjuntos(adjuntosAct, adjuntosNew, instancia, tipo, entryId)
    } else {
        var newWindow = window.open(rutaPM + "_common/attach.aspx?instancia=" + instancia + "&id=" + idAdjunto.toString() + "&tipo=" + tipo.toString(), "_blank", "width=600,height=275,status=no,resizable=yes,top=200,left=200")
        newWindow.focus();
    }
};
/*
''' <summary>
''' Funcion que llama a la pantalla que descarga un Zip con todos los adjuntos
''' </summary>    
''' <param name="idadjuntos">id de los adjuntos que estaban grabados en la solicitud</param>  
''' <param name="idadjuntosNew">id de los adjuntos que se han dado de alta</param>  
''' <param name="tipo">tipo del adjunto que sera sustituido</param>  
''' <param name="instancia">instancia de la solicitud</param>  
''' <returns></returns>
*/
function descargarAdjuntos(idAdjuntos, idAdjuntosNew, instancia, tipo, entryId) {
    var adjuntosAct = document.getElementById(entryId + '__hAct').value
    var adjuntosNew = document.getElementById(entryId + '__hNew').value

    if (adjuntosNew == '' && adjuntosAct.indexOf('xx') == -1) if (tipo.indexOf(',3')) descargarAdjunto(adjuntosAct, 3, instancia, entryId)
    else if (tipo.indexOf(',1')) descargarAdjunto(adjuntosAct, 1, instancia, entryId)
    else descargarAdjunto(adjuntosAct, tipo, instancia, entryId)
    else if (adjuntosAct == '' && adjuntosNew.indexOf('xx') == -1) {
        descargarAdjunto(adjuntosNew, tipo, instancia, entryId)
    } else {
        var newWindow = window.open(rutaPM + "_common/attach.aspx?adjuntosNew=" + idAdjuntosNew.toString() + "&adjuntos=" + idAdjuntos.toString() + "&tipo=" + encodeURI(tipo.toString()) + "&instancia=" + instancia.toString(), "_blank", "width=600,height=275,status=no,resizable=yes,top=200,left=200")
        newWindow.focus();
    }
};
/*
''' <summary>
''' Funcion que llama a la pantalla que descarga el adjunto de contrato
''' </summary>    
''' <param name="idContrato">idContrato</param>  
''' <param name="sNombreAdjunto">Nombre del adjunto</param>  
''' <remarks>Llamada desde: GeneralEntry.vb; Tiempo m�ximo:0</remarks>*/
function descargarAdjuntoContrato(instancia, idContrato, idArchivoContrato, sNombreAdjunto, datasize) {

    var newWindow = window.open(rutaPM + "_common/attach.aspx?instancia=" + instancia.toString() + "&idContrato=" + idContrato.toString() + "&idArchivoContrato=" + idArchivoContrato.toString() + "&datasize=" + datasize.toString() + "&NombreAdjunto=" + encodeURIComponent(sNombreAdjunto.toString()), "_blank", "width=600,height=275,status=no,resizable=yes,top=200,left=200")
    if (newWindow) newWindow.focus();
    else var timer = window.setTimeout(function () {
        if (newWindow) newWindow.focus();
    }, 400);
};
/*
''' <summary>
''' Funcion que realiza la peticion Ajax para eliminar todos los adjuntos
''' </summary>    
''' <param name="entryId">id del DataEntry</param>  
''' <returns></returns>
*/
function eliminarAdjuntos(entryId, bConContrato) {
    mioEntryId = entryId;
    document.getElementById(entryId + '__hAct').value = '';
    document.getElementById(entryId + '__hNew').value = '';
    var instancia = document.getElementById(entryId + '__hInstancia').value;
    var tipo = document.getElementById(entryId + '__hTipo').value
    if (bConContrato == 1) var adjunto = -1
    else var adjunto = document.getElementById(entryId + '__hAct').value

    var adjuntoNew = document.getElementById(entryId + '__hNew').value

    var nombre = document.getElementById(entryId + '__hNombre').value
    var idCampo = document.getElementById(entryId + '__hCampo').value
    oEntry = fsGeneralEntry_getById(entryId)
    // 1.- Creamos el objeto xmlHttpRequest
    CreateXmlHttp();
    // 2.- Definimos la llamada para hacer un simple GET.
    var ajaxRequest = rutaPM + 'GestionaAjax.aspx?accion=1&EntryID=' + entryId + '&instancia=' + instancia + '&tipo=' + tipo + '&nombre=' + nombre + '&adjunto=' + adjunto + '&adjuntoNew=' + adjuntoNew + '&campo=' + idCampo + '&readOnly=' + oEntry.readOnly.toString() + '&defecto=' + oEntry.DataValorDefecto;
    // 3.- Marcar qu� funci�n manejar� la respuesta
    xmlHttp.onreadystatechange = recogeInfo;
    // 4.- Enviar
    xmlHttp.open("GET", ajaxRequest, true);
    xmlHttp.send("");
};
/*
''' <summary>
''' Funcion que aumenta el tama�o de la caja de texto con los comentarios de los adjuntos
''' </summary>    
''' <param name="entryId">id del DataEntry</param>  
''' <param name="idAdjunto">id del adjunto</param> 
''' <returns></returns>
*/
function aumentarTamanoCajaTexto(entryId, idAdjunto, readOnly) {
    while (entryId.indexOf('$') >= 0)
        entryId = entryId.replace("$", "_");
    var cajaTexto;
    var hid_Texto;
    cajaTexto = document.getElementById(entryId + "__text" + idAdjunto)
    hid_Texto = document.getElementById(entryId + "__hidComent" + idAdjunto)

    cajaTexto.rows = 8
    if (readOnly == 1) cajaTexto.value = hid_Texto.value
    else cajaTexto.style.height = "200px"
    cajaTexto.style.Zindex = "99"
    cajaTexto.style.overflow = "scroll"
};
/*
''' <summary>
''' Funcion que aumenta el tama�o de la caja de texto con los comentarios de los adjuntos
''' </summary>    
''' <param name="entryId">id del DataEntry</param>  
''' <param name="idAdjunto">id del adjunto</param> 
''' <returns></returns>
*/
function reducirTamanoCajaTexto(entryId, idAdjunto, tipo, instancia, guardar, pos) {
    while (entryId.indexOf('$') >= 0)
        entryId = entryId.replace("$", "_");
    var cajaTexto;
    cajaTexto = document.getElementById(entryId + "__text" + idAdjunto)
    cajaTexto.style.Zindex = "99"
    if (pos > 0) cajaTexto.value = cajaTexto.value.substring(0, pos) + " ...."
    else cajaTexto.style.height = "33px"
    cajaTexto.rows = 2
    cajaTexto.style.overflow = "hidden"
    if (guardar == 1) {
        // 1.- Creamos el objeto xmlHttpRequest
        CreateXmlHttp();
        // 2.- Definimos la llamada para hacer un simple GET.
        var ajaxRequest = rutaPM + 'GestionaAjax.aspx?accion=3&tipo=' + tipo + '&instancia=' + instancia + '&adjunto=' + idAdjunto + '&coment=' + encodeURI(cajaTexto.value)
        //         // 3.- Marcar qu� funci�n manejar� la respuesta
        //         xmlHttp.onreadystatechange = recogeInfoAdjunto;
        // 4.- Enviar
        xmlHttp.open("GET", ajaxRequest, true);
        xmlHttp.send("");
    }

};
/*
''' <summary>
''' Funcion que aumenta el tama�o de la caja de texto con los comentarios de los adjuntos
''' </summary>    
''' <param name="entryId">id del DataEntry</param>  
''' <param name="idAdjunto">id del adjunto</param> 
''' <returns></returns>
*/
function reducirTamanoCajaTextoContrato(entryId, idAdjunto, tipo, instancia, guardar, pos, idArchivoContrato) {
    while (entryId.indexOf('$') >= 0)
        entryId = entryId.replace("$", "_");
    var cajaTexto;
    cajaTexto = document.getElementById(entryId + "__text" + idAdjunto)
    cajaTexto.style.Zindex = "99"
    if (pos > 0) cajaTexto.value = cajaTexto.value.substring(0, pos) + " ...."
    else cajaTexto.style.height = "33px"
    cajaTexto.rows = 2
    cajaTexto.style.overflow = "hidden"
    if (guardar == 1) {
        // 1.- Creamos el objeto xmlHttpRequest
        CreateXmlHttp();
        // 2.- Definimos la llamada para hacer un simple GET.
        var ajaxRequest = rutaPM + 'GestionaAjax.aspx?accion=3&tipo=5&instancia=' + instancia + '&adjunto=' + idAdjunto + '&idArchivoContrato=' + idArchivoContrato + '&coment=' + encodeURI(cajaTexto.value)
        //         // 3.- Marcar qu� funci�n manejar� la respuesta
        //         xmlHttp.onreadystatechange = recogeInfoAdjunto;
        // 4.- Enviar
        xmlHttp.open("GET", ajaxRequest, true);
        xmlHttp.send("");
    }
};
/*
''' <summary>
''' Funcion que aumenta el tama�o de la caja de texto del campo tipo medio o largo
''' </summary>    
''' <param name="entryId">id del DataEntry</param>  
''' <param name="numFilas">n�mero de filas que se va a aumentar</param> 
''' <returns></returns>
*/
function aumentarTamanoCampo(entryId, tipo, desglose) {
    while (entryId.indexOf('$') >= 0)
        entryId = entryId.replace("$", "_");

    var cajaTexto;
    cajaTexto = document.getElementById(entryId + "__t")

    if (tipo == 6) {
        cajaTexto.rows = 3;
        cajaTexto.style.height = "60px";
    } else {
        cajaTexto.rows = 8;
        cajaTexto.style.height = "120px";
    }

    cajaTexto.style.zIndex = "99";
    cajaTexto.style.overflow = "scroll";

    if (desglose == true) {
        var pos = $common.getLocation(cajaTexto);
        var posx = pos.x;
        var posy = pos.y

        cajaTexto.style.position = "absolute";
        cajaTexto.style.left = posx + "px";
        cajaTexto.style.top = posy + "px";
    }
};
/*
''' <summary>
''' Funcion que aumenta el tama�o de la caja de texto con los comentarios de los adjuntos
''' </summary>    
''' <param name="entryId">id del DataEntry</param>  
''' <param name="idAdjunto">id del adjunto</param> 
''' <returns></returns>
*/
function reducirTamanoCampo(entryId, desglose) {
    while (entryId.indexOf('$') >= 0)
        entryId = entryId.replace("$", "_");

    var cajaTexto;
    cajaTexto = document.getElementById(entryId + "__t");
    cajaTexto.style.zIndex = 10;
    cajaTexto.style.height = "15px";
    cajaTexto.rows = 1;
    cajaTexto.style.overflow = "hidden";

    if (desglose == true) {
        cajaTexto.style.position = "relative";
        cajaTexto.style.left = "";
        cajaTexto.style.top = "-5px";
    }
};
/* Descripcion: function que lleva a cabo el volvado de la informacion del buscador en el campo del desglose de actividad
Parametros entrada:
IDControl --> ID del dataentry del que hemos llamado al buscador de Asignacion de usuarios
sValor    --> Valores devueltos desde el buscador.
CodigoProveedor#DenominacionProveedor#CodMoneda/nombreUsuario_idContacto_nombreCategoria_idCategoria_CosteHora#... /n�Lineas a repetir
Llamada desde:AsignacionUsuarios.aspx
Tiempo ejecucion:0,2seg.  */
function DevolverSeleccionAsignacionUsuarios(IDControl, sValor) {
    if ((IDControl == "") || (sValor == "")) return
    arrAux = sValor.split("/")
    sProveedor = arrAux[0]
    sCadenaItems = arrAux[1]
    copiarLineas = arrAux[2]

    sResultado = "";
    o = fsGeneralEntry_getById(IDControl)
    if (o) {
        o.setDataValue(sValor)
        if (sCadenaItems != "") {
            arrAux2 = sCadenaItems.split("#")
            for (i = 0; i < arrAux2.length; i++) {
                if (arrAux2[i] != "") {
                    arrAux3 = arrAux2[i].split("_")
                    sNombre = arrAux3[0]
                    sResultado = sResultado + sNombre + ";"
                }
            }
        }
        o.setValue(sResultado)
    }
    //A continuacion se realiza el proceso de copiado en las lineas del desglose.
    if (copiarLineas != "") {
        posicionInicio = IDControl.search("fsdsentry")
        posicionFin = IDControl.length
        if (posicionInicio > 0) {
            CadenaFija = IDControl.substring(0, posicionInicio)
            CadenaSustituir = IDControl.substring(posicionInicio, IDControl.length)
            lineas = copiarLineas.split(",")

            arrCadenaSustituir = CadenaSustituir.split("_")
            for (i = 0; i < lineas.length; i++) {
                rangos = lineas[i].split("-")
                rangoInf = 0
                rangoSup = 0
                if (rangos.length == 1) { // unica linea
                    rangoInf = lineas[i]
                    rangoSup = lineas[i]
                } else { // Es un rango
                    rangoInf = rangos[0]
                    rangoSup = rangos[1]
                }
                for (k = rangoInf; k <= rangoSup; k++) {
                    nuevoEntry = CadenaFija + arrCadenaSustituir[0] + "_" + k + "_" + arrCadenaSustituir[2]
                    o = fsGeneralEntry_getById(nuevoEntry)
                    if (o) {
                        o.setDataValue(sValor)
                        if (sCadenaItems != "") {
                            arrAux2 = sCadenaItems.split("#")
                            sResultado = ""
                            for (x = 0; x < arrAux2.length; x++) {
                                if (arrAux2[x] != "") {
                                    arrAux3 = arrAux2[x].split("_")
                                    sNombre = arrAux3[0]
                                    sResultado = sResultado + sNombre + ";"
                                }
                            }

                        }
                        o.setValue(sResultado)
                    }
                }
            }
        }
    }
};
/*''' <summary>
''' Abre la ventana del detalle de una linea vinculada para un dataentry Desglose q este vinculado
''' </summary>
''' <param name="oEdit">Dataentry</param>
''' <param name="oldValue">Valor que tenia el entry anteriormente</param> 
''' <param name="oEvent">Evento click</param>        
''' <remarks>Llamada desde:javascript introducido para responder al onclick de todos los campos de tipo DesgloseVinculado; Tiempo m�ximo:0</remarks>*/
function show_detalle_DesgloseVinculado(oEdit, oldValue, oEvent) {
    var DatosLinea = oEdit.fsEntry.Hidden.value;
    var DatosSolic = oEdit.fsEntry.getValue();

    CerrarDesplegables();
    if (DatosLinea != '') if (DatosLinea.split("@").length == 3) {
        var newWindow = window.open(rutaFS + "_common/BuscadorSolicitudes.aspx?DatosLinea=" + DatosLinea + "&DatosSolic=" + DatosSolic, "_blank", "width=920,height=250,status=yes,resizable=no,top=100,left=100")
        newWindow.focus();
    } else {
        var newWindow = window.open(rutaFS + "_common/BuscadorSolicitudes.aspx?DatosLinea=" + DatosLinea + "&DatosSolic=" + DatosSolic, "_blank", "width=510,height=185,status=yes,resizable=no,top=100,left=100")
        newWindow.focus();
    }
};
/*''' <summary>
''' Abre la ventana del detalle de un destino para un dataentry de tipo destino
''' </summary>
''' <param name="oEdit">Dataentry</param>
''' <param name="oldValue">Valor que tenia el entry anteriormente</param> 
''' <param name="oEvent">Evento click</param>         
''' <remarks>Llamada desde:javascript introducido para responder al onclick de todos los campos de tipo Destino; Tiempo m�ximo:0</remarks>*/
function showDetalleDestino(oEdit, text, oEvent) {
    var valor;
    var popup = $('#PnlDestInfo');
    if (oEdit == null) {
        valor = text;
    } else {
        var oEntry = oEdit.fsEntry;
        valor = oEntry.getDataValue();
    };

    var params = { sID: valor }
    $.when($.ajax({
        type: "POST",
        url: rutaPM + 'ConsultasPMWEB.asmx/CargarDatosDestinos',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        var rDestino = msg.d[0];
        var rTextos = msg.d[1];
        $.when($.get(rutaPM + '_common/html/DetalleDestino.htm', function (detalleDestPopUp) {
            $('body').append(detalleDestPopUp);
        })).done(function () {
            //Poner textos de label
            $('#lblDestInfoDir').text(rTextos[0]);
            $('#lblDestInfoCP').text(rTextos[1]);
            $('#lblDestInfoPob').text(rTextos[2]);
            $('#lblDestInfoProv').text(rTextos[3]);
            $('#lblDestInfoPais').text(rTextos[4]);
            $('#lblDestInfoTfno').text(rTextos[5]);
            $('#lblDestInfoFAX').text(rTextos[6]);
            $('#lblDestInfoEmail').text(rTextos[7]);
            $('#lblTitPpalDen').text(rTextos[8]);
            $('#lblSubTit').text(rTextos[9]);
            //Poner valores del destino
            $('#lblDestInfoTituloCod').text(rDestino.Den);
            $('#txtDestInfoDir').text(rDestino.Direccion);
            $('#txtDestInfoCP').text(rDestino.CP);
            $('#txtDestInfoPob').text(rDestino.Pob);
            $('#txtDestInfoProv').text(rDestino.Provi);
            $('#txtDestInfoPais').text(rDestino.Pai);
            $('#txtDestInfoTfno').text(rDestino.Tfno);
            $('#txtDestInfoFAX').text(rDestino.FAX);
            $('#txtDestInfoEmail').text(rDestino.Email);
            $('#ImgDestInfoCerrar').live('click', function () {
                $('#PnlDestInfo').hide();
                $('#popupFondo').hide();
            });
            $('#popupFondo').css('height', $(document).height());
            $('#popupFondo').show();
            $('#PnlDestInfo').css('height', '280px');
            $('#PnlDestInfo').css('width', '300px');
            $('#PnlDestInfo').css('overflow', 'none');
            CentrarPopUp($('#PnlDestInfo'));
            $('#PnlDestInfo').show();
        });
    });
};
/*''' <summary>
''' Tarea 1959 : Control de Consumos
''' Si existe vinculaciones entre desgloses, antes de emitir hay q comprobar la cantidad de cada linea para q sobrepase la cantidad origen
''' </summary>  
''' <returns>si sobrepasa o no</returns>
''' <remarks>Llamada desde: NWAlta.aspx     NWGestionInstancia.aspx; Tiempo m�ximo:0,3</remarks>*/
function Validar_Cantidades_Vinculadas(Solicitud, Instancia) {
    var htDatosOrigen = new Array();
    var indexOrig;
    indexOrig = 0;

    var htDatosPadre = new Array();
    var indexPadre;
    var Existe;
    indexPadre = 0;

    var Cantidad = null;
    var htCantidad = new Array();

    for (arrInput in arrInputs) {
        oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
        if (oEntry) {
            if (oEntry.idCampoPadre) { //Este es un input perteneciente a un desglose                
                re = /fsdsentry/
                if (oEntry.id.search(re) >= 0) { //y no es el input base del desglose
                    if (oEntry.tipoGS == 2000) { //Solic vinculada
                        indexOrig++;
                        htDatosOrigen[indexOrig] = oEntry.Hidden.value;
                        htCantidad[indexOrig] = 0

                        Existe = 0;
                        for (i = 1; i <= indexPadre; i++) {
                            if (htDatosPadre[i] == oEntry.idCampoPadre) {
                                Existe = 1;
                                break;
                            }
                        }
                        if (Existe == 0) {
                            indexPadre++;
                            htDatosPadre[indexPadre] = oEntry.idCampoPadre;
                        }
                    } else {
                        if (oEntry.tipoGS == 4) { //Cantidad
                            Existe = 0;
                            for (i = 1; i <= indexPadre; i++) {
                                if (htDatosPadre[i] == oEntry.idCampoPadre) {
                                    Existe = 1;
                                    break;
                                }
                            }

                            if (Existe == 1) {
                                Cantidad = oEntry.getValue();

                                if ((Cantidad == null) || (Cantidad == "null")) {
                                    Cantidad = 0;
                                }

                                htCantidad[indexOrig] = Cantidad;
                            }
                        }
                    }
                }
            } else {
                if (oEntry.tipo == 9 && oEntry.tipoGS != 136) { //desglose
                    for (k = 1; k <= document.getElementById(oEntry.id + "__numTotRows").value; k++) {
                        if (document.getElementById(oEntry.id + "__" + k.toString() + "__Deleted")) {
                            for (arrCampoHijo in oEntry.arrCamposHijos) {
                                var oHijoSrc = document.getElementById(oEntry.id + "__" + oEntry.arrCamposHijos[arrCampoHijo]);
                                if (oHijoSrc) {
                                    if (oHijoSrc.tipoGS == 2000) {
                                        var oHijo = document.getElementById(oEntry.id + "__" + k.toString() + "__" + oEntry.arrCamposHijos[arrCampoHijo] + "_IdSolicVinculada");
                                        if (oHijo) {
                                            indexOrig++;
                                            htDatosOrigen[indexOrig] = oHijo.value;
                                            htCantidad[indexOrig] = 0

                                            Existe = 0;
                                            for (i = 1; i <= indexPadre; i++) {
                                                if (htDatosPadre[i] == oEntry.idCampo) {
                                                    Existe = 1;
                                                    break;
                                                }
                                            }
                                            if (Existe == 0) {
                                                indexPadre++;
                                                htDatosPadre[indexPadre] = oEntry.idCampo;
                                            }
                                        }
                                    } else {
                                        if (oHijoSrc.tipoGS == 4) {
                                            var oHijo = document.getElementById(oEntry.id + "__" + k.toString() + "__" + oEntry.arrCamposHijos[arrCampoHijo]);
                                            if (oHijo) {
                                                Existe = 0;
                                                for (i = 1; i <= indexPadre; i++) {
                                                    if (htDatosPadre[i] == oEntry.idCampo) {
                                                        Existe = 1;
                                                        break;
                                                    }
                                                }
                                                if (Existe == 1) {
                                                    Cantidad = oHijo.value;
                                                    if ((Cantidad == null) || (Cantidad == "null")) {
                                                        Cantidad = 0;
                                                    }
                                                    htCantidad[indexOrig] = Cantidad;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    };
    var Cual;
    for (i = 1; i < indexOrig; i++) {
        Cual = htDatosOrigen[i];

        if (Cual.split("@").length == 3) {
            for (j = i + 1; j <= indexOrig; j++) {
                if (Cual == htDatosOrigen[j]) {
                    htCantidad[i] = parseFloat(htCantidad[i]) + parseFloat(htCantidad[j]);
                    htDatosOrigen[j] = "";
                }
            }
        } else {
            htDatosOrigen[i] = "";
        }
    };
    for (i = 1; i <= indexOrig; i++) {
        if (htDatosOrigen[i] != "") {
            CreateXmlHttp();
            if (xmlHttp) {
                if (Solicitud == 0) var params = "InstanciaVinc=" + Instancia + "&DatosOrigen=" + htDatosOrigen[i] + "&Cantidad=" + htCantidad[i];
                else var params = "Solicitud=" + Solicitud + "&DatosOrigen=" + htDatosOrigen[i] + "&Cantidad=" + htCantidad[i];

                xmlHttp.open("POST", "../_common/controlarLineaVinculada.aspx", false);
                xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                xmlHttp.send(params);

                //Tras q se ejecute sincronamente controlarLineaVinculada.aspx controlamos sus resultados y obramos en 
                //consecuencia.                            
                var retorno;
                if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                    retorno = xmlHttp.responseText;
                    if (retorno == 'KO') { //no pasa la comprobaci�n
                        var newWindow = window.open(rutaFS + "_common/NoPasoControlarLineaVinculada.aspx?" + params, "_blank", "width=350,height=200,status=yes,resizable=no,top=200,left=300");
                        newWindow.focus();
                        return false;
                    }
                }
            }
        }
    };
    return true;
};
/*    
''' <summary>
''' Para desgloses vinculados mover linea significa lanzar el buscador de solictudes. Y el propio buscador se ocupara de a�adir lo q se 
''' necesita para hacer efectivo el cambio cuando se grabe y de borrar la linea movida del desglose.
''' </summary>
''' <param name="sRoot">nombre entry del desglose</param>
''' <param name="index">id de la fila seleccionada</param>        
''' <param name="idCampo">id de bbdd del desglose</param>
''' <param name="Solicitud">solicitud a la q mover la linea</param>
''' <param name="Instancia">instancia de la q se mueve la linea</param>
''' <param name="Celda">Celda, html, donde esta el bt mover/copiar/elim. A traves de �l se saca la fila y tabla html</param>
''' <param name="Frame">Frame donde esta el desglose a borrar</param>
''' <param name="PopUp">Indica si es Popup o no</param>  
''' <remarks>Llamada desde:javascript introducido en popupDesglose_Mover y popupDesglose_Delete_Mover para el LinkButton hypMover; Tiempo m�ximo:0</remarks>*/
function moverFilaSeleccionada(sRoot, index, idCampo, Solicitud, Instancia, Celda, Frame, PopUp) {
    var newWindow = window.open(rutaFS + "_common/BuscadorSolicitudes.aspx?sRoot=" + sRoot + "&IdCampo=" + idCampo + "&Instancia=" + Instancia + "&Index=" + index + "&Solicitud=" + Solicitud + "&Celda=" + Celda + "&Frame=" + Frame + "&PopUp=" + PopUp, "_blank", "width=910,height=500,status=yes,resizable=no,top=100,left=100")
    newWindow.focus();
};
/*''' <summary>
''' Mueve la linea indicada de la instancia actual a la instancia indicada
''' </summary>
''' <param name="IdCampo">De q desglose se va a mover la linea</param>        
''' <param name="sId">Id de la instancia a la q mueves</param>
''' <param name="index">fila q mueves</param>
''' <param name="PopUp">Indica si es Popup o no</param>
''' <param name="PrimerGuardado">Indica si ya se ha realizado algun guardado o no</param>    
''' <remarks>Llamada desde: PMWeb2008/BuscadorSolicitudes.aspx.vb; Tiempo m�ximo:0,1</remarks>*/
function MoverAInstancia(IdCampo, sId, Index, PopUp, PrimerGuardado) {
    if (PopUp == 0) {
        if (!document.forms["frmSubmit"]) {
            document.body.insertAdjacentHTML("beforeEnd", "<form name=frmSubmit action=../_common/guardarinstancia.aspx target=fraWSServer id=frmSubmit method=post style='position:absolute;top:0;left:0'></form>")
        }

        if (document.forms["frmDetalle"].elements.item("VinculacionesPrimerGuardar").value == 0) {
            sInput = "MOVER_" + IdCampo + "_" + Index + "_" + Index
            anyadirInput(sInput, sId)
        } else {
            //Problema: Mover la 2da fila, guardar y Mover la q era la 4ta fila. 
            //Tras el 1er guardado la fila 4 pasa a ser la 3 con lineaold 4. Como la creaci�n de los bts copiar/elim/mover 
            //es una unica vez resulta q el index en el 2do mover es 4 pero en bbdd INSTANCIA_LINEAS_VINCULAR tiene
            //un 3 en LINEAORIGEN (el guardado updata). Por lo q "MOVER_" + IdCampo + "_" + 4 no vale, debe ser "MOVER_" + IdCampo + "_" + 3.
            //A la vez necesito el 4 para la copia de la fila en la instancia "destino"
            if (typeof (arrInputs) != "undefined") {
                for (arrInput in arrInputs) {
                    oEntry = fsGeneralEntry_getById(arrInputs[arrInput])

                    if (oEntry) {
                        if (oEntry.idCampoPadre) //Este es un input perteneciente a un desglose
                        {
                            if (oEntry.idCampoPadre == IdCampo) {
                                var re = /fsdsentry/
                                var x = oEntry.id.search(re) + 10
                                var y = oEntry.id.search(oEntry.idCampo) - 1

                                if (oEntry.id.search(re) >= 0) //y no es el input base del desglose
                                {
                                    if (oEntry.tipoGS == 42) {
                                        y = x + 1
                                    }

                                    var iLinea = oEntry.id.substr(x, y - x)

                                    if (iLinea == Index) {
                                        var sPre = oEntry.id.substr(0, x - 10)

                                        var sAux = sPre.split("_")
                                        var sCampoHijo = null
                                        if (sAux.length == 7) {
                                            sCampoHijo = sAux[sAux.length - 2]
                                            if (sCampoHijo) sPre = sPre.substr(0, sPre.length - (sCampoHijo.length + 2))
                                        }

                                        sPre = replaceAll(sPre, "__", "::")
                                        sPre = replaceAll(sPre, "_", "$")
                                        sPre = replaceAll(sPre, "::", "$_")

                                        var iLineaOld
                                        if (!sCampoHijo) {
                                            if (document.forms["frmDetalle"].elements.item(sPre + "_" + iLinea + "_Linea")) iLineaOld = document.forms["frmDetalle"].elements.item(sPre + "_" + iLinea + "_Linea").value
                                            else iLineaOld = null
                                        } else {
                                            if (document.forms["frmDetalle"].elements.item(sPre + "_" + sCampoHijo + "$_" + iLinea + "_Linea")) iLineaOld = document.forms["frmDetalle"].elements.item(sPre + "_" + sCampoHijo + "$_" + iLinea + "_Linea").value
                                            else iLineaOld = null
                                        }

                                        iLinea = parseInt(htControlFilasVinc[String(oEntry.idCampoPadre) + "_" + String(iLinea)])

                                        sInput = "MOVER_" + IdCampo + "_" + iLinea + "_" + iLineaOld
                                        anyadirInput(sInput, sId)

                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    } else {
        p = window.opener

        if (!p.document.forms["frmSubmit"]) {
            p.document.body.insertAdjacentHTML("beforeEnd", "<form name=frmSubmit action=../_common/guardarinstancia.aspx target=fraWSServer id=frmSubmit method=post style='position:absolute;top:0;left:0'></form>")
        }

        var f = p.document.forms["frmSubmit"]

        if (p.document.forms["frmDetalle"].elements.item("VinculacionesPrimerGuardar").value == 0) {
            sInput = "MOVER_" + IdCampo + "_" + Index + "_" + Index

            if (!f.elements.item(sInput)) {
                s = "<INPUT type=hidden name=" + sInput + ">\n"
                f.insertAdjacentHTML("beforeEnd", s)
                f.elements.item(sInput).value = sId
            }
        } else {
            var sPre = document.getElementById("INPUTDESGLOSE").value
            var iMaxIndex = p.document.getElementById(sPre + "__numRowsVinc").value
            var i
            i = 0
            for (k = 1; k <= iMaxIndex; k++) {
                oLineaV = p.document.getElementById(sPre + "__" + k.toString() + "__LineaVinc");
                if (oLineaV) {
                    if ((oLineaV.value != "0") && (oLineaV.value != "-4")) {
                        i++;
                    }
                }

                if (i == Index) {
                    sInput = "MOVERPOPUP_" + IdCampo + "_" + i.toString() + "_" + k.toString()

                    if (!f.elements.item(sInput)) {
                        s = "<INPUT type=hidden name=" + sInput + ">\n"
                        f.insertAdjacentHTML("beforeEnd", s)
                        f.elements.item(sInput).value = sId
                    }

                    break;
                }
            }
        }
    }
};
/*''' <summary>
''' Tras un guardado con mover fila y/o eliminar fila se ha actualiza la tabla INSTANCIA_LINEAS_VINCULAR y la q era la
''' linea x ahora es la linea y. Las diferentes llamadas a popupDesgloseClickEvent se crean una vez al abrir el detalle
''' de la solicitud y por ello el index tras el guardado ha dejado de ser valido. Esta funci�n devuelve el numero de fila
''' en bbdd para la linea_old Index
''' </summary>
''' <param name="IdCampo">De q desglose se va a mover la linea</param>        
''' <param name="index">fila q mueves</param>
''' <param name="PopUp">Indica si es Popup o no</param>
''' <remarks>Llamada desde: PMWeb2008/BuscadorSolicitudes.aspx.vb; Tiempo m�ximo:0,1</remarks>*/
function DameLineaMoverAInstancia(IdCampo, Index, PopUp) {
    if (PopUp == 0) {
        //Problema: Mover la 2da fila, guardar y Mover la q era la 4ta fila. 
        //Tras el 1er guardado la fila 4 pasa a ser la 3 con lineaold 4. Como la creaci�n de los bts copiar/elim/mover 
        //es una unica vez resulta q el index en el 2do mover es 4 pero en bbdd INSTANCIA_LINEAS_VINCULAR tiene
        //un 3 en LINEAORIGEN (el guardado updata). Por lo q "MOVER_" + IdCampo + "_" + 4 no vale, debe ser "MOVER_" + IdCampo + "_" + 3.
        //A la vez necesito el 4 para la copia de la fila en la instancia "destino"
        if (parseInt(htControlFilasVinc[String(IdCampo) + "_" + String(Index)]) > 0) {
            return parseInt(htControlFilasVinc[String(IdCampo) + "_" + String(Index)]).toString();
        } else {
            var i;
            i = 0;
            for (k = 1; k <= Index; k++) {
                if (parseInt(htControlFilasVinc[String(IdCampo) + "_" + String(k)]) > 0) {
                    i++;
                } else {
                    if (parseInt(htControlFilasVinc[String(IdCampo) + "_" + String(k)]) < 0) {
                        i++;
                        htControlFilasVinc[String(IdCampo) + "_" + String(k)] = String(i);
                    }
                }
            }
            return i.toString();
        }
    } else {
        p = window.opener

        var iMaxIndex = p.document.getElementById(document.getElementById("INPUTDESGLOSE").value + "__numRowsVinc").value
        var i
        i = 0
        for (k = 1; k <= iMaxIndex; k++) {
            oLineaV = p.document.getElementById(document.getElementById("INPUTDESGLOSE").value + "__" + k.toString() + "__LineaVinc")
            if (oLineaV) {
                if ((oLineaV.value != "0") && (oLineaV.value != "-2") && (oLineaV.value != "-4")) {
                    i++;
                }
            }

            if (i == Index) {
                return p.document.getElementById(document.getElementById("INPUTDESGLOSE").value + "__" + k.toString() + "__LineaVinc").value.toString();
            }
        }
    }
    return Index.toString();
};
/*  Revisado por: blp. Fecha: 06/10/2011
''' Funci�n que hace recupera datos de servidor y los inserta en el control seleccionado
''' sUrl: ruta de la funcion a usar
''' sData: par�metros que la funcion requiere
''' sIDDestino: Id del control que se desea actualizar
''' sSelectedItemIndex: �ndice del registro a mostrar seleccionado cuando se despliega el combo. NO USAR NUNCA AL MISMO TIEMPO QUE sSelectedItemValue
''' sSelectedItemValue: Valor del registro a mostrar seleccionado cuando se despliega el combo. 
'''                     NO USAR NUNCA AL MISMO TIEMPO QUE sSelectedItemIndex
'''                     Usar NULL para evitar que se use
''' Si se usan a la vez sSelectedItemIndex y sSelectedItemValue, tiene prioridad el sSelectedItemValue
''' sTemplate: Nombre del campo de datos que se usa en el template para rellenar el combo a trav�s de json. */
function ajaxUpdate(sUrl, sData, sIDDestino, sSelectedItemIndex, sSelectedItemValue, tipoGS) {
    sData = '{"contextKey":"' + sData + "@@@" + tipoGS + '"}'
    $.ajax({
        type: "POST",
        url: sUrl,
        data: sData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            var dataSource;
            if (data.d != null) {
                dataSource = jQuery.parseJSON('[' + data.d + ']')
            } else { //No hay datos
                dataSource = jQuery.parseJSON('[{""Valor"":"""",""Texto"":""""}]')
            }
            var oControlDestino = $find(sIDDestino);
            oControlDestino.set_template('<li value=${Valor}>${Texto}</li>')
            oControlDestino.set_dataSource(dataSource);
            oControlDestino.dataBind();
            oControlDestino = null;
            //Si el control tiene valor seleccionado, lo seleccionamos
            if (sSelectedItemValue != null) {
                setTimeout('CambiarSeleccionValue("' + sIDDestino + '","' + sSelectedItemValue + '")', 200);
            }
        },
        failure: function () {
            alert("Error with AJAX callback");
        }
    });
};
/*  Revisado por: blp. Fecha: 06/10/2011
''' M�todo que selecciona un registro concreto en el control del que se pasa el ID.
''' sIDControl: Id del control (generalmente webdropdown) en el que se va a seleccionar un registro concreto
''' sSelectedItemIndex: �ndice del elemento a seleccionar. Si es Undefined no se selecciona ninguno
''' Llamada desde ajaxUpdate(). Tiempo m�ximo inferior a 0,1 seg.
*/
function CambiarSeleccion(sIDControl, sSelectedItemIndex) {
    var oControlDestino = $find(sIDControl);
    if (oControlDestino) {
        if (isNaN(parseInt(sSelectedItemIndex))) {
            DeseleccionarDesactivarSeleccionado(sIDControl, true, true)
            oControlDestino.set_selectedItemIndex(-1);
            oControlDestino.set_currentValue('', true)
        } else {
            if (parseInt(sSelectedItemIndex) == -1) {
                DeseleccionarDesactivarSeleccionado(sIDControl, true, true)
            }
            oControlDestino.selectItemByIndex(sSelectedItemIndex, true, true);
        }
        oControlDestino = null;
    }
};
/*  Revisado por: blp. Fecha: 06/10/2011
''' M�todo que selecciona un registro concreto en el control a partir del valor seleccionado.
''' sIDControl: Id del control (generalmente webdropdown) en el que se va a seleccionar un registro concreto
''' sSelectedItemValue: Valor del elemento a seleccionar. Si es Undefined no se selecciona ninguno
''' Llamada desde ajaxUpdate() y WebDropDown_DataCheck() en jsALta.js. Tiempo m�ximo inferior a 0,1 seg.
*/
function CambiarSeleccionValue(sIDControl, sSelectedItemValue) {
    var oControlDestino = $find(sIDControl);
    if (sSelectedItemValue == null || sSelectedItemValue == 'undefined' || sSelectedItemValue == '') {
        DeseleccionarDesactivarSeleccionado(sIDControl, true, true)
        oControlDestino.set_selectedItemIndex(-1);
    } else {
        if (oControlDestino.get_dataSource) {
            var datos = oControlDestino.get_dataSource();
            var k;
            for (k = 0; k < datos.length; ++k) {
                if (datos[k].Text == sSelectedItemValue) {
                    var sSelectedItemIndex = k;
                    oControlDestino.selectItemByIndex(sSelectedItemIndex, true, true);
                    oControlDestino.set_currentValue(sSelectedItemValue, true)
                    break;
                }
            }
        }
    }
};
/*  Revisado por: blp. Fecha: 28/12/2012
''' M�todo que cierra el ajaxIndicator (la imagen de "Cargando") del webdropdown
''' Hemos asociado el m�todo al evento Focus del webdropdown porque es el que m�s se aproxima al momento en que queremos ocultar el ajaxIndicator
''' senderID: Id del Control webdropdown a cargar
''' Llamada desde: jsAlta.js
''' Tiempo m�ximo inferior a 1 seg.*/
function WebDropDown_CloseAjaxIndicatorID(senderID) {
    var oDropdown = $find(senderID);
    //Ocultamos el ajaxIndicator
    oDropdown.get_ajaxIndicator().hide();
    oDropdown = null;
};
function WebDropDown_KeyDown(sender, e) {
    if ((e.get_browserEvent().keyCode == 18) || (e.get_browserEvent().keyCode == 8)) {
        var dropdown = $find(sender._id);
        //Vaciar texto
        if (dropdown) {
            if (dropdown.set_currentValue) dropdown.set_currentValue('', true);
            DeseleccionarDesactivarSeleccionado(sender._id, true, true)
            dropdown.set_selectedItemIndex(-1);
        }

        var dropdownEntry = fsGeneralEntry_getById(sender._id)
        if (dropdownEntry) {
            if ((dropdownEntry.tipoGS == 102) && (dropdownEntry.getDataValue() != "")) {
                sMonedaActual = dropdownEntry.getDataValue()
                sDenMonedaActual = dropdownEntry.getValue()
            }
            dropdownEntry.setValue("");
            dropdownEntry.setDataValue("");
            dropdownEntry = null;
        }
        return
    }

    if (e.get_browserEvent().keyCode != 9) {
        var datosDropDown = sender.get_dataSource()
        if (!datosDropDown || datosDropDown.length == 0) {
            var oDropdown = $find(sender._id);
            oDropdown.openDropDown();
        }
        WebDropDown_DataCheck(sender, e)
    }
};
/*  Revisado por: blp. Fecha: 06/10/2011
''' M�todo que se lanza en el evento de despliegue del panel de un webdropdown y carga y muestra sus datos
''' sender: Control webdropdown que lanza el evento
''' e: argumentos del evento
''' Llamada desde: el evento de despliegue del panel del webdropdown de asquellos webdropdown a los que se haya asociado este m�todo al event
''' Tiempo m�ximo inferior a 1 seg.*/
function WebDropDown_DataCheck(sender, e) {
    WebDropDown_DropDownOpening(sender, e)
    var Editor = sender._id
    var oSenderGeneralEntry = fsGeneralEntry_getById(Editor)
    var sMasterField = oSenderGeneralEntry.masterField
    var sidCampoListaEnlazada = oSenderGeneralEntry.idCampoListaEnlazada
    var sidCampoPadreListaEnlazada = oSenderGeneralEntry.idCampoPadreListaEnlazada
    var sURL;

    switch (oSenderGeneralEntry.tipoGS) {
        case 101:
            //Forma pago
        case 102:
            //Moneda
        case 105:
            //Unidad          
        case 142:
            //Unidad de Pedido
        case 107:
            //Pais
        case 108:
            //Provincia
        case 109:
            //Destino
        case 114:
            //Contacto Proveedor
        case 122:
            //Departamento
        case 123:
            //Organizaci�n de compras
        case 124:
            //Centro
        case 125:
            //Almac�n
        case 151:
            //A�o Partida   
        case 143:
            //Proveedor ERP
            sURL = rutaPM + 'ConsultasPMWEB.asmx/ObtenerDatos_DropDown'
            break;
        default:
            //las listas
            sURL = rutaPM + 'ConsultasPMWEB.asmx/Obtener_Lista'
            break;
    }
    //Miramos si el dropdown ya tiene datos
    var datosDropDown = sender.get_dataSource();
    //El combo no puede tener masterField y CampoPadreLista a la vez
    //por lo que vamos a unificar ambos elementos en un �nico valor para no duplicar el c�digo a la hora de recuperar los datos
    var sIdPadre = '';
    var sCampoListaEnlazada = '';
    if (sMasterField != '') {
        sIdPadre = getID(sMasterField, Editor);
    } else {
        if ((oSenderGeneralEntry.idCampoPadreListaEnlazada) && (oSenderGeneralEntry.idCampoPadreListaEnlazada > 0)) {
            var arrAux = sender._id.split("_")
            if (arrAux[1] == '') sIdPadre = arrAux[0] + '__' + arrAux[2] + '_' + arrAux[3] + "_" + 'fsentry' + oSenderGeneralEntry.idCampoPadreListaEnlazada + '__t'; //uwtGrupos_ctl01_578_fsentry28277__t"
            else sIdPadre = arrAux[0] + '_' + arrAux[1] + '_' + arrAux[2] + "_" + 'fsentry' + oSenderGeneralEntry.idCampoPadreListaEnlazada + '__t'; //uwtGrupos_ctl01_578_fsentry28277__t"
            arrAux = null;

            sCampoListaEnlazada = oSenderGeneralEntry.idCampoListaEnlazada + '#';
        }
    }
    var bDestino = false;
    //El id del padre puede no estar al mismo nivel que el campo hijo/dependiente, por eso hacemos una comprobaci�n m�s
    if (sIdPadre == '') {
        //El masterfield solo tiene el valor de control del que depende el entry cuando est�n al mismo nivel pero Almacen puede depender del centro aunque est� a un nivel superior. 
        //En ese caso, comprobaremos si existe.
        if (oSenderGeneralEntry.tipoGS == 125) { //  ALMACEN y obligaci�n de usar OrgCompras/Centro para filtrar
            var oEntry = null;
            if (oSenderGeneralEntry.UsarOrgCompras == true) {
                for (arrInput in arrInputs) {
                    oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
                    if (oEntry) if (oEntry.tipoGS == 124) { //Centro
                        sIdPadre = oEntry.id + '__t';
                        break;
                    }
                }
                var p = window.opener;
                if (sIdPadre == '' && p) {
                    for (arrInput in p.arrInputs) {
                        oEntry = p.fsGeneralEntry_getById(p.arrInputs[arrInput]);
                        if (oEntry) if (oEntry.tipoGS == 124) { //Centro
                            sIdPadre = oEntry.id + '__t';
                            break;
                        }
                    }
                }
            } else { //  ALMACEN si no hay usar OrgCompras/Centro se usar� destino para filtrar
                for (arrInput in arrInputs) {
                    oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
                    if (oEntry) if (oEntry.tipoGS == 109) { //Destino
                        sIdPadre = oEntry.id + '__t';
                        bDestino = true;
                        break;
                    }
                }
                var p = window.opener;
                if (sIdPadre == '' && p) {
                    for (arrInput in p.arrInputs) {
                        oEntry = p.fsGeneralEntry_getById(p.arrInputs[arrInput]);
                        if (oEntry) if (oEntry.tipoGS == 109) { //Destino
                            sIdPadre = oEntry.id + '__t';
                            bDestino = true;
                            break;
                        }
                    }
                }

            }
            oEntry = null;
        }
        //CENTRO: EL centro puede depender de una Organizaci�n de Compras que est� fuera del desglose o en otro grupo.
        if (oSenderGeneralEntry.tipoGS == 124) {
            var oEntry = null;
            for (arrInput in arrInputs) { //GRUPOS
                oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
                if (oEntry) if (oEntry.tipoGS == 123) { //Org Compras
                    sIdPadre = oEntry.id + '__t';
                    break;
                }
            }
            var p = window.opener; //DESGLOSE EN VENTANA EMERGENTE
            if (sIdPadre == '' && p) {
                for (arrInput in p.arrInputs) { //GRUPOS
                    oEntry = p.fsGeneralEntry_getById(p.arrInputs[arrInput]);
                    if (oEntry) if (oEntry.tipoGS == 123) { //Org Compras
                        sIdPadre = oEntry.id + '__t';
                        break;
                    }
                }
            }
            oEntry = null;
        }
    }
    if (sIdPadre != '') {
        var oPadre = fsGeneralEntry_getById(sIdPadre)
        if (!oPadre && p) { //el padre puede que est� en p, el window.opener
            oPadre = p.fsGeneralEntry_getById(sIdPadre)
        }
        if (!oPadre) {
            //el padre puede que sea un campo de tipo material que est� en otro grupo
            for (arrInput in arrInputs) {
                var o = fsGeneralEntry_getById(arrInputs[arrInput])
                if (o) {
                    if (o.tipoGS == 103 && arrInputs[arrInput].endsWith('fsentry' + oSenderGeneralEntry.idCampoPadreListaEnlazada)) oPadre = fsGeneralEntry_getById(arrInputs[arrInput] + '__t');
                }
            }
        }
        if (oPadre != null && oPadre != undefined) {
            if (oPadre.tipoGS == 103) {
                //En este caso se obtiene la lista de valores de otro m�todo distinto al de una lista con padre otra lista
                sURL = rutaPM + 'ConsultasPMWEB.asmx/Obtener_Lista_CampoPadre_Material';
            }
            var sPadreValue = oPadre.getDataValue();
            //Cuando hay listaEnlazada, el sPadreValue debe llevar no s�lo el valor seleccionado en el padre sino tb el id la lista y la instancia
            if (sCampoListaEnlazada != '') {
                if (oPadre.tipoGS == 103) {
                    sPadreValue = sCampoListaEnlazada + sPadreValue
                }
                else {
                    if (document.getElementById("Instancia")) {
                        var vInstancia = document.getElementById("Instancia").value
                    } else {
                        vInstancia = ""
                    }
                    sPadreValue = sCampoListaEnlazada + sPadreValue + "#" + vInstancia + "#0#0"
                    if (oPadre.tipo == 2) {
                        sPadreValue = sPadreValue + "#1"
                    } else {
                        sPadreValue = sPadreValue + "#0"
                    }
                    vInstancia = null;
                }
            }
            var sPadreValueOld = oPadre.oldDataValue;
            var PadreValueNotNullNotEmpty = (sPadreValue != '' && sPadreValue != null)
            var PadreValueComp = (sPadreValue != sPadreValueOld)
            if (bDestino || oPadre.tipoGS == 109) {
                sPadreValue = "@@DEST_" + sPadreValue;
            }
            //Llamamos al update de datos del webdropdown (ajaxUpdate) en tres casos
            //1. Si no tenemos datos en el webdropdown
            //2. Si existe la variable de datos, es decir, se han cargado previamente pero hay Cero resultados (datosDropDown.length)
            // y adem�s el control maestro tiene valor distinto de vac�o y de null (PadreValueNotNullNotEmpty) (este caso es ineficiente porque vamos a acceder a bdd siempre en aquellos campos que devuelven cero resultados pero nos sirve para los campos generados al copiar o a�adir filas, en los que no hay datos pero aparecen como vac�os de datos)
            //3. Si existe la variable de datos (tenemos datos)
            // y adem�s el control maestro tiene valor distinto de vac�o y de null (PadreValueNotNullNotEmpty)
            // y adem�s en control maestro ha cambiado de valor (PadreValueComp)
            if (!datosDropDown || (datosDropDown.length == 0 && PadreValueNotNullNotEmpty) || (datosDropDown && PadreValueNotNullNotEmpty && PadreValueComp)) {
                sender.get_ajaxIndicator().setRelativeContainer(sender.get_element());
                //Forzamos que se muestre el ajaxindicator
                sender.get_ajaxIndicator().show(sender)
                //Cargamos los datos en el dropdown
                ajaxUpdate(sURL, sPadreValue, Editor, '', null, oSenderGeneralEntry.tipoGS)
                oPadre.oldDataValue = sPadreValue;
                //Ocultamos el ajaxIndicator
                //No hay un evento adecuado para ocultarlo (ser�a lo ideal), lo que que usamos un setTimeOut
                setTimeout('WebDropDown_CloseAjaxIndicatorID("' + Editor + '")', 200);
            }
            sPadreValue = null;
            sPadreValueOld = null;
            PadreValueNotNullNotEmpty = null;
            PadreValueComp = null;
        }
        oPadre = null;
    } else {
        if (!datosDropDown || datosDropDown.length == 0 || oSenderGeneralEntry.tipoGS == 143 || oSenderGeneralEntry.tipoGS == 151) {
            if (sCampoListaEnlazada == '') { //Esta carga de datos es s�lo para ciertos casos de los campos que no son listas
                sender.get_ajaxIndicator().setRelativeContainer(sender.get_element());
                //Forzamos que se muestre el ajaxindicator
                sender.get_ajaxIndicator().show(sender)
                //Cargamos los datos en el dropdown
                if (oSenderGeneralEntry.tipoGS == 0) {
                    var vInstancia
                    if (document.getElementById("Instancia")) {
                        vInstancia = document.getElementById("Instancia").value
                        if ((oSenderGeneralEntry.idCampoListaEnlazada != 0) && (oSenderGeneralEntry.idCampoListaEnlazada != undefined)) {
                            sCampoValue = oSenderGeneralEntry.idCampoListaEnlazada + "##" + vInstancia
                        } else {
                            if ((oSenderGeneralEntry.CampoDef_CampoODesgHijo != 0) || (vInstancia != 0)) {
                                sCampoValue = oSenderGeneralEntry.CampoDef_CampoODesgHijo + "##" + vInstancia
                            }
                            else {//FSPMQA_DEVOLVER_VALORES_LISTA no hace nada sin un dato copia_campo_def.id o form_campo.id o enlazado
                                vInstancia = ""
                                sCampoValue = oSenderGeneralEntry.idCampo + "##" + vInstancia //form_campo.id
                            }
                        }
                    } else {
                        vInstancia = ""
                        sCampoValue = oSenderGeneralEntry.idCampo + "##" + vInstancia
                    }

                    if (oSenderGeneralEntry.IdAtrib == 0) {
                        sCampoValue = sCampoValue + "#0#0"
                    } else {
                        if (document.getElementById("Bloque")) {
                            sCampoValue = sCampoValue + "#" + document.getElementById("Bloque").value
                        } else {
                            sCampoValue = sCampoValue + "#0"
                        }

                        if (document.getElementById("Solicitud")) {
                            sCampoValue = sCampoValue + "#" + document.getElementById("Solicitud").value
                        } else {
                            sCampoValue = sCampoValue + "#0"
                        }
                    };
                    vInstancia = null;
                    ajaxUpdate(sURL, sCampoValue, Editor, '', null, oSenderGeneralEntry.tipoGS)
                } else {
                    switch (oSenderGeneralEntry.tipoGS) {
                        case 114:
                            var oPadre = fsGeneralEntry_getById(oSenderGeneralEntry.dependentfield)
                            sCampoValue = '';
                            if (oPadre) sCampoValue = oPadre.getDataValue();
                            ajaxUpdate(sURL, sCampoValue, Editor, '', null, oSenderGeneralEntry.tipoGS)
                            break;
                        case 143:
                            //Proveedor ERP
                            var sProveValue, sOrgComprasValue
                            var oOrgCompras
                            var oEntryProvErpId = oSenderGeneralEntry.id
                            if (oSenderGeneralEntry.idDataEntryDependent) oOrgCompras = fsGeneralEntry_getById(oSenderGeneralEntry.idDataEntryDependent)
                            //Comprobaremos si tenemos que cambiar el nombre del DataEntry del proveedor
                            if (oEntryProvErpId.indexOf("fsdsentry") != -1) {
                                var arrEntryProvErp = new Array();
                                arrEntryProvErp = oEntryProvErpId.split("_")
                                /*Si el Dataentry que llama a la funcion es un dataentry de desglose(Proveedor ERP), se comprobara
                                si los 2 campos de los que depende(Proveedor y Org.compras) vienen con ids de desglose, el proveedor siempre 
                                tendra que estar al mismo nivel que el Proveedor ERP, la organizacion de compras no*/
                                if (oSenderGeneralEntry.idEntryPROV.indexOf("fsdsentry") == -1) {
                                    //Si el campo proveedor no tiene el id del desglose habra que ponerselo, esto sucede cuando se a�ade una fila en blanco en un desglose
                                    oSenderGeneralEntry.idEntryPROV = oSenderGeneralEntry.idEntryPROV.replace("fsentry", "fsdsentry_" + arrEntryProvErp[arrEntryProvErp.length - 2])
                                }
                                if (oOrgCompras == null || oOrgCompras == undefined) {
                                    //Si no hemos recuperado el dataentry con el id que viene en idDataEntryDependent, cambiaremos el id poniendole el id de un campo de desglose
                                    //para ver si lo encontramos en el desglose
                                    if (oSenderGeneralEntry.idDataEntryDependent) {
                                        oSenderGeneralEntry.idDataEntryDependent = oSenderGeneralEntry.idDataEntryDependent.replace("fsentry", "fsdsentry_" + arrEntryProvErp[arrEntryProvErp.length - 2])
                                        oOrgCompras = fsGeneralEntry_getById(oSenderGeneralEntry.idDataEntryDependent)
                                    } else {
                                        var p = window.opener
                                        //Si no ha recogido el Entry de organizacion de compras sera que el campo proveedor ERP se encuentra en un desglose popup
                                        if (p.sDataEntryOrgComprasFORM) {
                                            oOrgCompras = p.fsGeneralEntry_getById(p.sDataEntryOrgComprasFORM)
                                        }
                                    }
                                }
                            }
                            var oProve = fsGeneralEntry_getById(oSenderGeneralEntry.idEntryPROV)

                            sProveValue = '';
                            sOrgComprasValue = '';
                            if (oProve) sProveValue = oProve.getDataValue();
                            if (oOrgCompras) sOrgComprasValue = oOrgCompras.getDataValue();
                            ajaxUpdate(sURL, sProveValue + '###' + sOrgComprasValue, Editor, '', null, oSenderGeneralEntry.tipoGS)
                            break;
                        case 151:                          
                            var oEntry = null;

                            for (arrInput in arrInputs) {
                                oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
                                if (oEntry) if (oEntry.id == oSenderGeneralEntry.IdEntryPartidaPlurianual) { 
                                    sIdPadre = oEntry.id + '__t';
                                    break;
                                }
                            }
                            var p = window.opener;
                            if (sIdPadre == '' && p) {
                                for (arrInput in p.arrInputs) {
                                    oEntry = p.fsGeneralEntry_getById(p.arrInputs[arrInput]);
                                    if (oEntry) if (oEntry.id == oSenderGeneralEntry.IdEntryPartidaPlurianual) {
                                        sIdPadre = oEntry.id + '__t';
                                        break;
                                    }
                                }
                            }
                            oEntry = null;

                            var oPadre = fsGeneralEntry_getById(sIdPadre)
                            sCampoPres0 = ' ';
                            sCampoValue = ' ';
                            if (oPadre) {
                                sCampoPres0 = oPadre.PRES5
                                sCampoValue = ' '+ oPadre.getDataValue();
                            }                          
                            ajaxUpdate(sURL, sCampoPres0 + "@@" + sCampoValue, Editor, '', null, oSenderGeneralEntry.tipoGS)
                            break;
                        default:
                            ajaxUpdate(sURL, '', Editor, '', null, oSenderGeneralEntry.tipoGS)
                    }
                }
                //Ocultamos el ajaxIndicator
                //No hay un evento adecuado para ocultarlo (ser�a lo ideal), lo que que usamos un setTimeOut
                setTimeout('WebDropDown_CloseAjaxIndicatorID("' + Editor + '")', 200);
            }
        }
    };

    //cuando haya datos, nos aseguramos de que, si el dropdown tiene un valor, est� realmente seleccionado
    var sSelectedDataValue = null;
    if (oSenderGeneralEntry.getDataValue() != null && oSenderGeneralEntry.getDataValue() != '' && oSenderGeneralEntry.getDataValue() != "undefined") {
        if (oSenderGeneralEntry.getValue() != null) {
            sSelectedDataValue = oSenderGeneralEntry.getValue();
        }
    } else {
        //cuando no hay entry porque el control es generado din�micamente desde javascript, controlamos si tiene algo en el cuadro de texto
        //y a partir de ese dato recuperamos el valor
        if (sender.get_currentValue() != null && sender.get_currentValue() != undefined) {
            sSelectedDataValue = sender.get_currentValue()
        }
    }
    if (sSelectedDataValue != null) {
        //Cuando acaba de haber una carga de datos desde ajax, esta funci�n s�lo funciona usando un timeout
        setTimeout('CambiarSeleccionValue("' + Editor + '","' + sSelectedDataValue + '")', 200);
    }
    Editor = null;
    oSenderGeneralEntry = null;
    sMasterField = null;
    sidCampoListaEnlazada = null;
    sidCampoPadreListaEnlazada = null;
    sURL = null;
    datosDropDown = null;
    sIdPadre = null;
    sCampoListaEnlazada = null;
    sSelectedDataValue = null;
};
/*  Revisado por: blp. Fecha: 06/10/2011
''' Funci�n que devuelve un ID editado a partir de un fragmento de ID que queremos insertar en un ID completo
''' Si no se cumplen los criterios para insertarlo, se devuelve el ID a insertar recibido
''' IDInsertar: Fragmento de ID a insertar
''' IDCompleto: Id completo enb el que se inserta el fragmento
''' Llamada desde: WebDropDown_DataCheck. M�ximo < 0,1 seg
*/
function getID(IdInsertar, IdCompleto) {
    var IDEditado;
    var sSearchingText = "fsentry"
    if (IdCompleto.search(sSearchingText) == -1) {
        sSearchingText = "fsdsentry"
    }
    if (IdCompleto.search(sSearchingText) > -1) {
        var sInit = IdCompleto.substr(0, IdCompleto.search(sSearchingText));
        var sEnd = IdCompleto.substr(IdCompleto.search(sSearchingText), IdCompleto.length);
        IDEditado = sInit + IdInsertar + '__t';
    }
    return IDEditado
};
/*  Revisado por: blp. Fecha: 06/10/2011
''' Funci�n que vac�a el contenido del dropdown, los elementos seleccionados y los valores guardados en el entry 
''' dropdownID: Id del control a vaciar
''' Llamada desde: jsAlta.js. M�ximo < 0,1 seg
*/
function vaciarDropDown(dropdownID) {
    var dropdown = $find(dropdownID + '__t');
    //Vaciar texto
    if (dropdown) {
        if (dropdown.set_currentValue) {
            dropdown.set_currentValue('', true);
            dropdown.set_selectedItemIndex(-1);
            var datos = dropdown.get_dataSource(); //As� se recuperan los datos cuando el control carga por jquery
            if (datos && datos.length > 0) { //Aqu� entra cuando el control tiene carga desde jquery (si no, oControl.get_dataSource() ser�a undefined)
                dropdown.set_dataSource(eval('[{"Valor":"","Texto":""}]'));
                dropdown.dataBind();
                datos = null;
            }
        }
        dropdown = null;
    }
    var dropdownEntry = fsGeneralEntry_getById(dropdownID)
    if (dropdownEntry) {
        dropdownEntry.setValue("");
        dropdownEntry.setDataValue("");
        dropdownEntry = null;
    }
};
/*  Revisado por: blp. Fecha: 06/10/2011
''' Busca el contenido del par�metro valorSeleccionado en los datos del webdropdown con ID idControlDestino. Si existe, lo selecciona 
''' dropdownID: Id del control en cuyos datos el que comprobar si existe el valorSeleccionado
''' valorSeleccionado: valor a buscar en el control y que se seleccionar� si se encuentra
''' Llamada desde: jsAlta.js. M�ximo < 0,1 seg
*/
function copiarSeleccionWebDropDown(idControlDestino, valorSeleccionado) {
    if (valorSeleccionado != '' && valorSeleccionado != 'undefined' && valorSeleccionado != null) {
        var oControl = $find(idControlDestino);
        if (oControl) {
            var datos = oControl.get_dataSource(); //As� se recuperan los datos cuando el control carga por jquery
            var k;
            if (datos) { //Aqu� entra cuando el control tiene carga desde jquery (si no, oControl.get_dataSource() ser�a undefined)
                if (datos.length > 0) {
                    for (k = 0; k < datos.length; ++k) {
                        if (datos[k].Text.substring(0, datos[k].Text.indexOf(" - ")) == valorSeleccionado) {
                            var iSelectedItemIndex = k;
                            CambiarSeleccion(oControl._id, iSelectedItemIndex);
                            break;
                        }
                    }
                } else {
                    var oControlEntry = fsGeneralEntry_getById(idControlDestino)
                    if (oControlEntry) {
                        var valorDefecto = oControlEntry.DataValorDefecto
                        if (valorSeleccionado == valorDefecto && oControlEntry.TextoValorDefecto != null) {
                            oControl.set_currentValue(oControlEntry.TextoValorDefecto, true)
                        }
                    }
                    oControlEntry = null;
                    valorDefecto = null;
                }
            } else { //cuando el control carga los datos desde servidor, o sea oControl.get_dataSource() es undefined
                datos = oControl.get_items(); //As� se recuperan los datos cuando el control carga desde servidor
                //Hay dos modos de recuperar los datos en esta situacion. controlaremos cu�ndo recuperarlos de una manera o de otra
                var modo = 1;
                var iLength = datos._items.length;
                if (datos.getLength() > datos._items.length) {
                    modo = 2;
                    iLength = datos.getLength();
                }
                if (datos && iLength > 0) {
                    for (var item = 0; item < iLength; item++) {
                        var dataValue = null;
                        var oCelda = null;
                        if (modo == 1) {
                            if (datos._items[item].get_element(0).children[0].rows != undefined) oCelda = datos._items[item].get_element(0).children[0].rows[0].cells[0]

                            if (oCelda == null) { //tipoGS=0
                                dataValue = datos._items[item].get_value();
                            } else {
                                if (oCelda.innerText != undefined) dataValue = oCelda.innerText;
                                else dataValue = oCelda.textContent;
                            }
                        } else {
                            if (datos.getItem(item).get_element(0).children[0].rows != undefined) oCelda = datos.getItem(item).get_element(0).children[0].rows[0].cells[0]

                            if (oCelda == null) { //tipoGS=0
                                dataValue = datos.getItem(item).get_value();
                            } else {
                                if (oCelda.innerText != undefined) dataValue = oCelda.innerText;
                                else dataValue = oCelda.textContent;
                            }
                        }
                        if (dataValue == valorSeleccionado) {
                            var iSelectedItemIndex = -1;
                            if (modo == 1) {
                                iSelectedItemIndex = datos._items[item].get_index();
                            } else {
                                iSelectedItemIndex = datos.getItem(item).get_index();
                            }
                            CambiarSeleccion(idControlDestino, iSelectedItemIndex);
                            break;
                        }
                    }
                }
            }
        }
    }
};
/*  Revisado por: blp. Fecha: 06/10/2011
''' Funci�n que devuelve un n�mero aleatorio dentro de un m�nimo y un m�ximo
''' Min: valor m�nimo
''' Max: valor m�ximo
''' Llamada desde: jsAlta.js. M�ximo < 0,1 seg
*/
function Aleatorio(Min, Max) {
    var random = parseInt(new String(Math.random()).split(".")[1].substring(0, new String(Max).length))
    while (random > Max && random < Min || random > Max && random > Min || random < Max && random < Min) {
        random = parseInt(new String(Math.random()).split(".")[1].substring(0, new String(Max).length))
    }
    return random
};
/*  Revisado por: blp. Fecha: 06/10/2011
''' Funci�n que devuelve la primera parte de un string en el que se busca el string textoBuscado.
''' Si no lo encuentra, devuelve el string completo
''' texto: texto en el que buscar el textoBuscado
''' textoBuscado: texto a bsucar
''' Llamada desde: webdropdown_datacheck. M�ximo < 0,1 seg
*/
function devolverCod(texto, textoBuscado) {
    sTexto = texto
    if (texto.indexOf(textoBuscado) > -1) {
        sTexto = texto.substr(0, texto.indexOf(textoBuscado))
    }
    return sTexto
};
/*
Revisado por: blp. Fecha: 03/10/2011
Funci�n que devuelve el alto o el ancho de la ventana
type: Valor del que depende que devuelva ancho o alto
Height: devuelve alto
Width: devuelve ancho
Llamada desde desglose.aspx --> resize()
*/
function getWindowSize(type) {
    var mySize = 0;
    if (type == "Height") {
        if (typeof (window.innerWidth) == 'number') {
            //Non-IE
            mySize = window.innerHeight;
        } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
            //IE 6+ in 'standards compliant mode'
            mySize = document.documentElement.clientHeight;
        } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
            //IE 4 compatible
            mySize = document.body.clientHeight;
        }
    } else {
        if (typeof (window.innerWidth) == 'number') {
            //Non-IE
            mySize = window.innerWidth;
        } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
            //IE 6+ in 'standards compliant mode'
            mySize = document.documentElement.clientWidth;
        } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
            //IE 4 compatible
            mySize = document.body.clientWidth;
        }
    }
    return mySize
};
/*  Revisado por: blp. Fecha: 03/10/2011
''' M�todo que se lanza en el evento de texto cambiado en el webdropdown
''' En �l, comprobamos si la medida tomada es vaciar el contenido. 
''' Si es as�, comprobamos si hay alg�n elemento seleccionado y lo deseleccionamos 
''' y vaciamos el valor del control entry
''' sender: Control webdropdown que lanza el evento
''' e: argumentos del evento
''' Llamada desde: el evento
''' Tiempo m�ximo inferior a 1 seg.*/
function WebDropDown_valueChanged(sender, e) {
    if (e.getNewValue() == "") {
        if (sender.get_selectedItem() != null && sender.get_selectedItem() != undefined) {
            DeseleccionarDesactivarSeleccionado(sender._id, true, true)
            sender.set_selectedItemIndex(-1);
        }
        var dropdownEntry = fsGeneralEntry_getById(sender._id)
        if (dropdownEntry) {
            dropdownEntry.setValue("");
            dropdownEntry.setDataValue("");
            dropdownEntry = null;
            var oEdit = fsGeneralEntry_getById(sender._id);
            switch (oEdit.tipoGS) {
                case 109:
                    //Destinos
                    document.getElementById("igtxt" + oEdit.Editor2.ID).style.visibility = "hidden";
                    break;

            }
        }
    }
};
/*  Revisado por: blp. Fecha: 03/10/2011
''' M�todo que deselecciona y/o desactiva el elemento seleccionado, si lo hay
''' de modo que CAMBIA EL CSS del elemento seleccionado, que es lo que perseguimos con esta funci�n
''' idControl: ID del control a tratar
''' deseleccionar: si es true, deseleccionaremos el item
''' desactivar: si es true, desactivaremos el item
''' Llamada desde: CambiarSeleccion, CambiarSeleccionValue, WebDropDown_valueChanged
''' Tiempo m�ximo inferior a 0,1 seg.*/
function DeseleccionarDesactivarSeleccionado(idControl, deseleccionar, desactivar) {
    var oControl = $find(idControl)
    if (oControl) {
        if (oControl.get_selectedItem() != null && oControl.get_selectedItem() != undefined) {
            if (deseleccionar) {
                oControl.get_selectedItem().unselect()
            }
            if (desactivar) {
                oControl.get_selectedItem().inactivate()
            }
        }
    }
};
/*  Revisado por: blp. Fecha: 03/10/2011
''' La funci�n devuelve un array con los datos del doctype
''' Llamada desde:popupDesgloseClickEvent
''' M�x 0,1 seg
*/
function detectDoctype() {
    var re = /\s+(X?HTML)\s+([\d\.]+)\s*([^\/]+)*\//gi;
    var res = false;
    /*==============================================
    Just check for internet explorer.
    ==============================================*/
    if (typeof document.namespaces != "undefined") {
        //CALIDAD
        res = document.all[0].nodeType == 8 ? re.test(document.all[0].nodeValue) : false;
    } else {
        res = document.doctype != null ? re.test(document.doctype.publicId) : false;
    }
    if (res) {
        res = new Object();
        res['xhtml'] = RegExp.$1;
        res['version'] = RegExp.$2;
        res['importance'] = RegExp.$3;
        return res;
    } else {
        return null;
    }
};
//Funcion que abrira desde el Campo "Desglose Vinculado" la instancia a la que se ha vinculado
//oEdit: WebTextEdit con el numero de instancia y descripcion
//idInstancia: id de la instancia
function AbrirInstanciaVinculada(oEdit, idInstancia) {
    if (oEdit.value != "") {
        var instancia = oEdit.value.split("-")
        if (instancia.length == 2) {
            if (idInstancia != 0) {
                var newWindow = window.open("../workflow/comprobaraprob.aspx?DesgloseVinculado=1&Instancia=" + instancia[0] + "&volver=../workflow/comprobaraprob.aspx?Instancia=" + idInstancia, "_self")
                newWindow.focus();
            } else {
                //Se acaba de guardar la instancia y el parametro idInstancia vendria a 0
                var idInstancia = $("[id$=NEW_Instancia]")[0].value
                if (idInstancia != "") {
                    newWindow = window.open("../workflow/comprobaraprob.aspx?DesgloseVinculado=1&Instancia=" + instancia[0] + "&volver=../workflow/comprobaraprob.aspx?Instancia=" + idInstancia, "_self")
                    newWindow.focus();
                } else {
                    newWindow = window.open("../workflow/comprobaraprob.aspx?DesgloseVinculado=1&Instancia=" + instancia[0] + "&volver=" + document.location, "_self")
                    newWindow.focus();
                }
            }
        }
    }
};
//Comprueba que el n�mero de factura introducido es correcto.
function ddFactura_ValueChange(oEdit, oldValue, oEvent) {
    var sCodProveedor = '';
    var sValor;
    sValor = trim(oEdit.getValue());
    oEdit.elem.title = sValor;

    if (oEdit.fsEntry.DependentField != null) {
        o = fsGeneralEntry_getById(oEdit.fsEntry.DependentField)
        if (o) sCodProveedor = o.getDataValue();
    };
    window.open("../_common/BuscadorFacturaServer.aspx?id=" + sValor + "&fsEntry=" + oEdit.ID + "&CodProveedor=" + sCodProveedor, "iframeWSServer")
};
function show_facturas(oEntry) {
    var sCodProveedor = '';
    var sDenProveedor = '';
    //si hay un dataentry proveedor hay que pasarlo al buscador
    oEntry = oEntry.fsEntry;

    if (oEntry.dependentfield != null) {
        o = fsGeneralEntry_getById(oEntry.dependentfield)
        if (o) {
            sCodProveedor = o.getDataValue();
            sDenProveedor = o.getValue();
            if (sDenProveedor.indexOf(' - ') > 0) sDenProveedor = sDenProveedor.substring(sDenProveedor.indexOf(' - ') + 3, sDenProveedor.legnth)
        }
    };
    CerrarDesplegables();
    var newWindow = window.open(rutaFS + "_common/BuscadorFacturas.aspx?Valor=" + escape(oEntry.getValue()) + "&IDControl=" + oEntry.id + "&CodProveedor=" + sCodProveedor + "&DenProveedor=" + encodeURI(sDenProveedor), "_blank", "width=950,height=475,status=yes,resizable=yes,scrollbars=yes,top=200,left=200");
    newWindow.focus();
};
function factura_seleccionada(IDControl, NumFactura, IdFactura) {
    var o;
    o = fsGeneralEntry_getById(IDControl)
    if (o) {
        o.setValue(NumFactura)
        o.setDataValue(NumFactura)
        o.Dependent.value = IdFactura
    }
};
//Muestra la pantalla de detalle de factura al pulsar desde el campo de Factura(CampoGS=138)
function show_detalle_factura(idEntry) {
    var oEntry = fsGeneralEntry_getById(idEntry)
    if (oEntry) {
        if (oEntry.Dependent.value != -1) {
            window.open(rutaFS + "PM/Facturas/DetalleFactura.aspx?ID=" + oEntry.Dependent.value + "&DesdePM=1", "Factura", "width=910,height=600,status=yes,resizable=yes,top=20,left=100,scrollbars=yes")
        }
    }
};
function factura_keydown(oEdit, text, oEvent) {
    if (oEvent.event.keyCode == 8 || oEvent.event.keyCode == 46 || oEvent.event.keyCode == 32) {
        if (oEdit.fsEntry) {
            var oEntry = oEdit.fsEntry
            if (oEntry) {
                oEntry.setValue('')
                oEntry.setDataValue('')
                oEntry.Dependent.value = -1
            }
        }
    }
};
function DecodeJSText(Entrada) {
    Entrada = replaceAll(Entrada, "&lt;", "<")
    Entrada = replaceAll(Entrada, "&gt;", ">")
    return Entrada
};
/*''' <summary>
''' En pedido negociado limpia los articulos por cambio de proveedor.
''' </summary>
''' <remarks>Llamada desde: BuscadorArticulos.aspx  Buscadorproveedores.aspx; Tiempo m�ximo: 0,2</remarks>*/
function Borra_Los_Articulos() {

    var NoRepetirPop = 0;
    borrar_Entrys(arrInputs);
    //Quien Borra puede ser un POPUP, en el padre hay q borrar los artic
    p = window.opener;
    if (p) {
        borrar_Entrys(p.arrInputs);
    }
};
function borrar_Entrys(arrInputs) {
    for (arrInput in arrInputs) {
        oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
        if (oEntry) {
            if (oEntry.idCampoPadre) //Este es un input perteneciente a un desglose
            {
                NoRepetirPop = oEntry.idCampoPadre;

                re = /fsdsentry/
                if (oEntry.id.search(re) >= 0) //y no es el input base del desglose
                {
                    if (oEntry.tipoGS == 119 || oEntry.tipoGS == 104 || oEntry.tipoGS == 118 || oEntry.tipoGS == 9 || oEntry.tipoGS == 105 || oEntry.tipoGS == 142 || oEntry.tipoGS == 103) { //Art nuevo//Art viejo////Precio Unitario//Unidad//Unidad de pedido//Material
                        oEntry.setValue("")
                        oEntry.setDataValue("")
                    }
                }
            } else {
                if (oEntry.tipo == 9 && oEntry.tipoGS != 136) { //desglose
                    for (k = 1; k <= document.getElementById(oEntry.id + "__numTotRows").value; k++) {
                        if (document.getElementById(oEntry.id + "__" + k.toString() + "__Deleted")) {
                            for (arrCampoHijo in oEntry.arrCamposHijos) {
                                var oHijoSrc = document.getElementById(oEntry.id + "__" + oEntry.arrCamposHijos[arrCampoHijo]);
                                if (oHijoSrc) {
                                    var oHijo = document.getElementById(oEntry.id + "__" + k.toString() + "__" + oEntry.arrCamposHijos[arrCampoHijo]);
                                    if (oHijo) {
                                        if (oHijoSrc.getAttribute("tipoGS") == 119 || oHijoSrc.getAttribute("tipoGS") == 104 || oHijoSrc.getAttribute("tipoGS") == 118 || oHijoSrc.getAttribute("tipoGS") == 9 || oHijoSrc.getAttribute("tipoGS") == 105 || oHijoSrc.getAttribute("tipoGS") == 142 || oHijoSrc.getAttribute("tipoGS") == 103) { //Art nuevo
                                            oHijo.value = "";
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else { //campo normal
                    if (oEntry.tipoGS == 119 || oEntry.tipoGS == 104 || oEntry.tipoGS == 118 || oEntry.tipoGS == 9 || oEntry.tipoGS == 105 || oEntry.tipoGS == 142 || oEntry.tipoGS == 103) { //Art nuevo//Art viejo////Precio Unitario//Unidad//Unidad de pedido//Material
                        oEntry.setValue("")
                        oEntry.setDataValue("")
                    }
                }
            }
        }
    }
}
/*Funci�n con la que recuperamos el alto y ancho de la pantalla, no de la p�gina, es decir, no tiene en cuenta el scroll, 
por lo que sirve para controlar que los paneles se muestren hacia arriba si la posici�n en la que vamos a mostrar el panel es cercana a alguno de los l�mites*/
function getWindowSize2() {
    var winW = 630,
       winH = 460;
    if (document.body && document.body.offsetWidth) {
        winW = document.body.offsetWidth;
        winH = document.body.offsetHeight;
    }
    if (document.compatMode == 'CSS1Compat' && document.documentElement && document.documentElement.offsetWidth) {
        winW = document.documentElement.offsetWidth;
        winH = document.documentElement.offsetHeight;
    }
    if (window.innerWidth && window.innerHeight) {
        winW = window.innerWidth;
        winH = window.innerHeight;
    }
    return [winW, winH];
};
/*
Mediante las funciones findPositionWithScrolling2, getWindowSize2 y getScroll, determinamos el punto en el que posicionar un panel de forma que no quede cortado en ning�n extremo
relativeElement: Elemento en relaci�n al cual vamos a calcular la posici�n en la que mostrar le shownElement
shownElement: Elemento a mostrar, lo usaremos para tener en cuenta sus dimensiones (a menos que nos pasen showElementWidth y showElementHeight)
showElementWidth: ancho del shownElement
showElementHeight: alto del shownElement
appart: Indicamos cu�nto queremos separar la posici�n que se calcula de la posici�n del relativeElement
*/
function showPosition(relativeElement, showElement, showElementWidth, showElementHeight, appart) {
    var position = [0, 0];
    var dimensions = [0, 0];
    var x = 0,
       y = 0;
    if (relativeElement) var position = findPositionWithScrolling2(relativeElement);
    if ((showElementWidth) && (showElementHeight) && (showElementWidth > 0) && (showElementHeight > 0)) {
        dimensions = [showElementWidth, showElementHeight]
    } else {
        if (showElement) dimensions = [showElement.offsetWidth, showElement.offsetHeight];
        if (dimensions[0] == 0 && dimensions[1] == 0) {
            var showElementX = parseInt(showElement.style.width);
            var showElementY = parseInt(showElement.style.height);
            dimensions = [showElementX, showElementY];
        }
    }
    var windowSize = getWindowSize2();
    var scrollPosition = getScroll();
    if (position[0] + dimensions[0] + appart > windowSize[0] + scrollPosition[0]) {
        if (position[0] - dimensions[0] - appart > 0) x = position[0] - dimensions[0] - appart;
        else x = 0;
    } else {
        x = position[0] + appart;
    }
    if (position[1] + dimensions[1] + appart > windowSize[1] + scrollPosition[1]) {
        if (position[1] - dimensions[1] - appart > 0) y = position[1] - dimensions[1] - appart;
        else y = 0;
    } else {
        y = position[1] + appart;
    }
    return [x, y];
};
/*Devuelve cu�nto scroll se ha efectuado, tanto en abscisas como en ordenadas*/
function getScroll() {
    if (window.pageYOffset != undefined) {
        return [pageXOffset, pageYOffset];
    } else {
        var sx, sy, d = document,
           r = d.documentElement,
           b = d.body;
        sx = r.scrollLeft || b.scrollLeft || 0;
        sy = r.scrollTop || b.scrollTop || 0;
        return [sx, sy];
    }
};
//
//<summary>Borra los art�culos de la solicitud de un pedido express correspondientes al proveedores pasado como par�metro</summary>
//<param name="codProv">C�digo del proveedor del cual hay que borrar los art�culos adjudicados</param>
//
function borraArticulosAdjudicadosProve(codProv) {
    var idLinea;
    var first;
    var lineas = new Array();
    var NoRepetirPop = 0;
    for (arrInput in arrInputs) {
        oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
        if (oEntry)
            if (oEntry.isDesglose) //Este es un input perteneciente a un desglose             
                if (oEntry.tipoGS == 119 || oEntry.tipoGS == 104) //Campos de articulos GS nuevo y viejo
                    comprobarAdjudicadoArticulo(oEntry.getValue(), codProv, oEntry.idLinea, oEntry.idCampoPadre);
                else  //campo normal borrado directo
                    if (oEntry.tipoGS == 119 || oEntry.tipoGS == 104) //Art nuevo
                        if (comprobarAdjudicadoArticulo(oEntry.getValue(), codProv)) {
                            oEntry.setValue("");
                            oEntry.setDataValue("");
                        }
    }
};
///<summary>elimina articulos de linea de desglose, de un determinado campo desglose<summary>
/// <param name="id" type="Number">identificador de la linea.</param>
///<param name="idCampoPadre">Identificador del padre del desglose</param>
function borrarLineaDesglose(id, idCampoPadre) {
    for (arrInput in arrInputs) {
        if (arrInputs[arrInput] != "deleted") {
            oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
            if (oEntry) {
                if (oEntry.isDesglose) //Este es un input perteneciente a un desglose
                {
                    if ((oEntry.idCampoPadre == idCampoPadre && oEntry.nLinea == id) && (oEntry.tipoGS == 119 || oEntry.tipoGS == 104)) {//Campos de articulos GS nuevo y viejo
                        idEntryDen = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna + 1, oEntry.nLinea, 0);
                        var oDenArt = fsGeneralEntry_getById(idEntryDen);
                        idEntryMat = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna - 1, oEntry.nLinea, 0);
                        var oMatArt = fsGeneralEntry_getById(idEntryMat);
                        oEntry.setValue("");
                        oEntry.setDataValue("");
                        oDenArt.setValue("");
                        oDenArt.setDataValue("");
                        oMatArt.setValue("");
                        oMatArt.setDataValue("");
                    }
                }
            } else {//campo normal borrado directo
                if (oEntry.tipoGS == 119 || oEntry.tipoGS == 104) {//Art nuevo
                    oEntry.setValue("");
                    oEntry.setDataValue("");
                }
            }
        }
    }
}
function getEntryTipoByLinea(tipoEntry, idLinea) {
    ///<summary>devuelve el entry de un determinado tipo que se encuentra en una linea dada</summary>
    for (arrInput in arrInputs) {
        oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
        if (oEntry) {
            if (oEntry.isDesglose) {
                if (oEntry.idLinea == idLinea && oEntry.idCampoPadre) {
                    if (oEntry.tipoGS == tipoEntry) {
                        return oEntry;
                    }
                }
            }
        }
    }
};
///<summary>Comprueba si un articulo determinado tiene adjudicaciones vigentes para un proveedor, si las tiene llama a la funci�n borrar linea</summary>
///<param name="oEntry" type="string">Entry con tipo articulo GS</param>
///<param name="codProv" type="string">cod. proveedor</param>
function comprobarAdjudicadoArticulo(codArt, codProv, idLinea, idCampoPadre) {
    params = {
        codArt: codArt,
        codPro: codProv
    };
    $.when($.ajax({
        type: "POST",
        url: rutaPM + 'alta/NWAlta.aspx/comprobarAdjudicadoArticulo',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        if (msg.d == '1') {
            borrarLineaDesglose(idLinea, idCampoPadre);
        }
    });
};

function BorrarProveedoresERP(Entry) {
    f = document.forms["frmDetalle"]
    if (!f) f = document.forms["frmAlta"]
    for (arrInput in arrInputs) {
        var oAux = fsGeneralEntry_getById(arrInputs[arrInput])
        if (oAux) {
            if (oAux.tipo == 9 && oAux.idDataEntryDependent == Entry.id) { //desglose despu�s de un campo de OrgCompras             
                var i
                i = 0
                for (k = 1; k <= f.elements[oAux.id + "__numTotRows"].value; k++) {
                    for (arrCampoHijo in oAux.arrCamposHijos) {
                        oHijo = f.elements[oAux.id + "__" + k.toString() + "__" + oAux.arrCamposHijos[arrCampoHijo]]
                        if (oHijo) {
                            //input de desglose formato: IdCampoPadre_IdCampoHijo_Linea
                            oHijoSrc = f.elements[oAux.id + "__" + oAux.arrCamposHijos[arrCampoHijo]]
                            if (oHijoSrc.tipoGS == 143 && (oHijoSrc.idDataEntryDependent == Entry.id || oHijoSrc.idEntryPROV == Entry.id)) {
                                oHijo.value = ""
                            } else {
                                if (oHijoSrc.tipoGS == 143 && oHijoSrc.idDataEntryDependent == undefined) {
                                    if (sDataEntryOrgComprasFORM == Entry.id) oHijo.value = ""
                                }
                            }
                        }
                    }
                }
            } else {
                if ((oAux.idDataEntryDependent == Entry.id || oAux.idEntryPROV == Entry.id) && (oAux.tipoGS == 143)) {
                    oAux.setValue("")
                    oAux.setDataValue("")
                }
            }
        }
    }
};

/*
''' <summary>
''' Llamar Servicio Externo
''' </summary>
''' <param name="ClientID">Id del entry q es un servicio</param>
''' <param name="Instancia">Instancia en la q estas</param>        
''' <param name="Peticionario">Instancia>0 entonces se trae de bbdd el Instancia.Peticionario Instancia=0 entonces usuario conectado</param>   
''' <remarks>Llamada desde: GeneralEntry.vb ; Tiempo m�ximo: 0</remarks>*/
function llamarServicioExternoPorId(ClientID, Instancia, Peticionario, UsuMask) {
    var oEntry = fsGeneralEntry_getById(ClientID)
    llamarServicioExterno(oEntry, Instancia, Peticionario, UsuMask)
};
/*
''' <summary>
''' Llamar Servicio Externo
''' </summary>
''' <param name="ClientID">Id del entry q es un servicio</param>
''' <param name="Instancia">Instancia en la q estas</param>        
''' <param name="Peticionario">Instancia>0 entonces se trae de bbdd el Instancia.Peticionario Instancia=0 entonces usuario conectado</param>   
''' <param name="UsuMask">el setvalue de las fechas no funciona si el formato q usas no es el del usuario</param>        
''' <remarks>Llamada desde: llamarServicioExternoPorId ; Tiempo m�ximo: 0</remarks>*/
function llamarServicioExterno(oEdit, Instancia, PeticionarioInstancia, UsuMask) {    
    var respuesta;
    var campoId, lineaDesglose;
    var servicio = oEdit.Servicio;
    var valor = oEdit.getDataValue();
    if (oEdit.idLinea == "") { //Campo de fuera de desglose
        lineaDesglose = "0";
    } else {
        lineaDesglose = oEdit.idLinea;
    }
    if (oEdit.campo_origen == 0) { //Alta de formulario
        campoId = oEdit.idCampo;
    } else {
        campoId = oEdit.campo_origen;
    }

    //OBTENEMOS LOS PARAMETROS
    //EmpresaID
    empresa = 0;
    if (typeof (empresaEntrada) != 'undefined' && document.getElementById(empresaEntrada)) empresa = (document.getElementById(empresaEntrada).value == '' ? 0 : document.getElementById(empresaEntrada).value);

    //CodProve
    proveedor = '';
    if (typeof (proveedorEntrada) != 'undefined' && document.getElementById(proveedorEntrada)) proveedor = document.getElementById(proveedorEntrada).value;

    //ContactoID
    contacto = 0;
    var wdd = $find("wddContactos");
    if (wdd && wdd.get_selectedItems().length == 1) contacto = wdd.get_selectedItems()[0].get_value();

    //Monedas
    moneda = "";
    var wdd = $find("wddMonedas");
    if (wdd && wdd.get_selectedItems().length == 1) moneda = wdd.get_selectedItems()[0].get_value();

    //Fechas Inicio - Fin Contrato
    fechaInicio = '';
    if (typeof (fechaInicioEntrada) != 'undefined' && fsGeneralEntry_getById(fechaInicioEntrada)) {
        fechaAux = fsGeneralEntry_getById(fechaInicioEntrada).getValue();
        fechaInicio = (fechaAux == null ? '' : new Date(fechaAux.getFullYear(), fechaAux.getMonth(), fechaAux.getDate()));
    }

    fechaExpiracion = '';
    if (typeof (fechaExpiracionEntrada) != 'undefined' && fsGeneralEntry_getById(fechaExpiracionEntrada)) {
        fechaAux = fsGeneralEntry_getById(fechaExpiracionEntrada).getValue();
        fechaExpiracion = (fechaAux == null ? '' : new Date(fechaAux.getFullYear(), fechaAux.getMonth(), fechaAux.getDate()));
    }

    importeDesde = 0;
    importeHasta = 0;
    if (typeof (importeDesdeEntrada) != 'undefined' && $find(importeDesdeEntrada)) importeDesde = $find(importeDesdeEntrada).get_value();
    if (typeof (importeHastaEntrada) != 'undefined' && $find(importeHastaEntrada)) importeHasta = $find(importeHastaEntrada).get_value();

    if (importeDesde == null) importeDesde = 0;
    if (importeHasta == null) importeHasta = 0;

    iDiasAlerta = 0;
    if (typeof (alertaEntrada) != 'undefined' && document.getElementById(alertaEntrada)) iDiasAlerta = document.getElementById(alertaEntrada).value;

    var wdd = $find("wddPeriodo")
    iPeriodo = 1
    if (wdd && wdd.get_selectedItems().length == 1) {
        iPeriodo = wdd.get_selectedItems()[0].get_value();

        if (iPeriodo > 1) {
            if (iPeriodo == 2)
                iDiasAlerta = iDiasAlerta * 7
            else
                iDiasAlerta = iDiasAlerta * 30
        }
    }

    iDiasEmail = 0;
    if (typeof (periodoEmail) != 'undefined' && document.getElementById(periodoEmail)) iDiasEmail = document.getElementById(periodoEmail).value;

    var wdd = $find("wddEmail")
    iPeriodo = 1
    if (wdd && wdd.get_selectedItems().length == 1) {
        iPeriodo = wdd.get_selectedItems()[0].get_value();

        if (iPeriodo > 1) {
            if (iPeriodo == 2)
                iDiasEmail = iDiasEmail * 7;
            else
                iDiasEmail = iDiasEmail * 30;
        }
    }

    //Saco la url y los parametros del servicio
    params = {
        servicio: servicio,
        campoId: campoId,
        Instancia: Instancia,
        Peticionario: PeticionarioInstancia,
        Empresa: empresa,
        Proveedor: proveedor,
        Contacto: contacto,
        Moneda: moneda,
        FechaInicio: fechaInicio,
        FechaExpiracion: fechaExpiracion,
        Alerta: iDiasAlerta,
        RepetirEmail: iDiasEmail,
        ImporteDesde: importeDesde,
        ImporteHasta: importeHasta
    }

    $.when($.ajax({
        type: "POST",
        url: rutaPM + 'ConsultasPMWEB.asmx/Obtener_Servicio',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        respuesta = msg.d;
    });

    var sUrl = respuesta.Url;
    var oEntrada = respuesta.ParamsEntrada;
    var oSalida = respuesta.ParamsSalida;
    var Peticionario = respuesta.Peticionario;
    var nombreResult;
    for (var key in oSalida) {
        if (oSalida[key].IndError == 1) {
            nombreResult = oSalida[key].Den;
        }
    }

    //El campo servicio puede estar en campo, desglose no popup y desglose popup. Si es popup los param entrada pueden estar fuera desglose.
    var EsPopUp;
    EsPopUp = 0;
    p = window.opener;
    if (p) EsPopUp = 1;

    //llamar al webservice con el valor del par�metro de entrada introducido y recojo la respuesta
    params = "{"
    $.each($.map(oEntrada, function (parametro) {
        return parametro
    }), function () {
        params += "'" + this.Den + "':'";
        if (this.Directo == 1) { //Directo
            switch (this.Tipo) {
                case 2: //numero
                    params += this.valorNum;
                    break;
                case 3: //fecha
                    arrAux = this.valorFec.split("-")
                    params += arrAux[0] + '-' + arrAux[1] + '-' + arrAux[2] + ' ' + arrAux[3] + ':' + arrAux[4] + ':' + arrAux[5] + '.000'
                    break;
                case 4: //boolean
                    if (this.valorBool == 1) params += true;
                    else params += false;
                    break;
                case 6: //texto
                    params += this.valorText;
                    break;
            }
        } else { //No Directo            
            switch (this.TipoCampo) {
                case 0:
                case 1: //0-1 campo Formulario
                    if (this.EsSubcampo == 0) {
                        //param entrada No en desglose Y Campo servicio S� en desglose -> buscar fuera.
                        lineaDesglose = "0";
                    } else {
                        lineaDesglose = oEdit.idLinea;
                    }

                    if (EsPopUp == 0 || this.EsSubcampo == 1) {
                        for (arrInput in arrInputs) {
                            oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
                            if (oEntry) {
                                Dato = DameDatoParamServicio(oEntry, lineaDesglose, this.Campo);
                                if (Dato != '###NODATO###') {
                                    params += Dato;
                                    break;
                                }
                            }
                        }
                    } else { //PopUp y param entrada es campo No de desglose.Buscar en padre.
                        for (arrInput in p.arrInputs) {
                            oEntry = p.fsGeneralEntry_getById(p.arrInputs[arrInput]);
                            if (oEntry) {
                                Dato = DameDatoParamServicio(oEntry, lineaDesglose, this.Campo);
                                if (Dato != '###NODATO###') {
                                    params += Dato;
                                    break;
                                }
                            }
                        }
                    }
                    break;
                case 2: //Peticionario
                    params += respuesta.ParamsCamposGenericos.Peticionario.valorText;
                    break;
                case 11: //Moneda
                    params += respuesta.ParamsCamposGenericos.Moneda.valorText;
                    break;
                case 12: //Proveedor
                    params += respuesta.ParamsCamposGenericos.Proveedor.valorText;
                    break;
                case 13: //Contacto
                    params += respuesta.ParamsCamposGenericos.Contacto.valorNum;
                    break;
                case 14: //Empresa
                    params += respuesta.ParamsCamposGenericos.Empresa.valorNum;
                    break;
                case 15: //Fecha de inicio
                    params += respuesta.ParamsCamposGenericos.FechaInicio.valorFec;
                    break;
                case 16: //Fecha de expiracion
                    params += respuesta.ParamsCamposGenericos.FechaExpiracion.valorFec;
                    break;
                case 17: //Mostrar alerta
                    params += respuesta.ParamsCamposGenericos.MostrarAlerta.valorNum;
                    break;
                case 18: //Enviar email
                    params += respuesta.ParamsCamposGenericos.RepetirMail.valorNum;
                    break;
                case 19: //Importe desde
                    params += respuesta.ParamsCamposGenericos.ImporteDesde.valorNum;
                    break;
                case 20: //Importe hasta
                    params += respuesta.ParamsCamposGenericos.ImporteHasta.valorNum;
                    break;
            }
        }
        params += "',";
    });

    params = params.substring(0, params.length - 1);
    params += "}";
    
    $.when($.ajax({
        type: "POST",
        url: sUrl,
        contentType: "application/json; charset=utf-8",
        data: params,
        dataType: "json",
        async: false
    })).done(function (msg) {
        respuesta = msg.d;
    });
    if (respuesta[nombreResult] != "") {
        alert(respuesta[nombreResult]);
        oEdit.Editor.elem.className = "TipoTextoMedioRojo";
        for (var key in oSalida) {
            if (oSalida[key].IndError == 0) {
                var nomParam = oSalida[key].Den;
                var IDCampo = oSalida[key].Campo;
                //vaciar los valores de salida en sus correspondientes campos
                for (arrInput in arrInputs) {
                    oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
                    if (oEntry) {
                        if (lineaDesglose == "0" || (lineaDesglose != "0" && oEntry.idLinea == lineaDesglose)) {
                            if (oEntry.campo_origen == 0) { //Alta de formulario
                                if (oEntry.idCampo == IDCampo.toString()) {
                                    oEntry.setValue("");
                                    oEntry.setDataValue("");
                                }
                            } else {
                                if (oEntry.campo_origen == IDCampo.toString()) {
                                    oEntry.setValue("");
                                    oEntry.setDataValue("");
                                }
                            }
                        }
                    }
                }
            }
        }
    } else {
        //Cojo los valores y los guardo en los campos correspondientes
        oEdit.Editor.elem.className = "TipoTextoMedioVerde";
        for (var key in oSalida) {
            if (oSalida[key].IndError == 0) {
                var nomParam = oSalida[key].Den;
                var IDCampo = oSalida[key].Campo;
                var salidaParam = respuesta[nomParam];
                //meter los valores de salida en sus correspondientes campos
                for (arrInput in arrInputs) {
                    oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
                    if (oEntry) {
                        if (lineaDesglose == "0" || (lineaDesglose != "0" && oEntry.idLinea == lineaDesglose)) {
                            if (oEntry.campo_origen == 0) { //Alta de formulario
                                if (oEntry.idCampo == IDCampo.toString()) {
                                    //segun tipo es una funcion u otra                                     
                                    if (oEntry.tipo == 4) {
                                        if ((salidaParam == "1") || (salidaParam == "true")) {
                                            oEntry.setValue(sTextoSi);
                                            oEntry.setDataValue('1');
                                        } else {
                                            oEntry.setValue(sTextoNo);
                                            oEntry.setDataValue('0');
                                        }
                                    } else {
                                        if (oEntry.tipo == 3) {
                                            if (salidaParam !== null) {
                                                if (salidaParam.toString().toLowerCase().indexOf("date") >= 0) {
                                                    var fecha = eval("new " + salidaParam.slice(1, -1));
                                                    salidaParam = fecha.format(UsuMask).toString();
                                                } else {
                                                    if (salidaParam.toString().toLowerCase().indexOf("t") >= 0) {
                                                        var Aux = salidaParam
                                                        var Anno = Aux.substr(0, 4)
                                                        Aux = Aux.substr(5, 100)

                                                        var Mes = Aux.substr(0, 2)
                                                        if (Mes.toString().toLowerCase().indexOf("-") >= 0) {
                                                            Mes = Aux.substr(0, 1);
                                                            Aux = '0' + Aux
                                                        }
                                                        Mes = Mes - 1
                                                        Aux = Aux.substr(3, 100)

                                                        var Dia = Aux.substr(0, 2);
                                                        if (Dia.toString().toLowerCase().indexOf("t") >= 0) {
                                                            Dia = Aux.substr(0, 1)
                                                        }
                                                        fecha = new Date(Anno, Mes, Dia)
                                                        salidaParam = fecha.format(UsuMask).toString();
                                                    }
                                                }
                                                oEntry.setValue(salidaParam);
                                            } else {
                                                oEntry.setValue('')
                                                oEntry.setDataValue('');
                                            }
                                        } else {
                                            oEntry.setValue(salidaParam);
                                            oEntry.setDataValue(salidaParam);
                                        }
                                    }
                                }
                            } else {
                                if (oEntry.campo_origen == IDCampo.toString()) {
                                    //segun tipo es una funcion u otra                                     
                                    if (oEntry.tipo == 4) {
                                        if ((salidaParam == "1") || (salidaParam == "true")) {
                                            oEntry.setValue(sTextoSi);
                                            oEntry.setDataValue('1');
                                        } else {
                                            oEntry.setValue(sTextoNo);
                                            oEntry.setDataValue('0');
                                        }
                                    } else {
                                        if (oEntry.tipo == 3) {
                                            if (salidaParam !== null) {
                                                if (salidaParam.toString().toLowerCase().indexOf("date") >= 0) {
                                                    var fecha = eval("new " + salidaParam.slice(1, -1));
                                                    salidaParam = fecha.format(UsuMask).toString();
                                                } else {
                                                    if (salidaParam.toString().toLowerCase().indexOf("t") >= 0) {
                                                        var Aux = salidaParam
                                                        var Anno = Aux.substr(0, 4)
                                                        Aux = Aux.substr(5, 100)

                                                        var Mes = Aux.substr(0, 2)
                                                        if (Mes.toString().toLowerCase().indexOf("-") >= 0) {
                                                            Mes = Aux.substr(0, 1);
                                                            Aux = '0' + Aux
                                                        }
                                                        Mes = Mes - 1
                                                        Aux = Aux.substr(3, 100)

                                                        var Dia = Aux.substr(0, 2);
                                                        if (Dia.toString().toLowerCase().indexOf("t") >= 0) {
                                                            Dia = Aux.substr(0, 1)
                                                        }
                                                        fecha = new Date(Anno, Mes, Dia)
                                                        salidaParam = fecha.format(UsuMask).toString();
                                                    }
                                                }
                                                oEntry.setValue(salidaParam);
                                            } else {
                                                oEntry.setValue('')
                                                oEntry.setDataValue('');
                                            }
                                        } else {
                                            oEntry.setValue(salidaParam);
                                            oEntry.setDataValue(salidaParam);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
};
//muestra los compradores para que el usuario elija uno
//LLamada:	javascript introducido para responder al onclick de todos los campos de tipo comprador
function show_compradores(oEntry) {
    oEdit = oEntry.fsEntry;
    var sUnidadOrganizativa = "";
    var sCodOrgCompras = "";
    var sCentro = "";
    var sMaterial = "";
    var yaOrgCompras = 0;
    var yaCentro = 0;

    //Recojo todos los valores de UO, OrgComp y Material
    for (arrInput in arrInputs) {
        var o = fsGeneralEntry_getById(arrInputs[arrInput])
        if (o) {
            if (o.tipo == 9) { //Para los popup(desglose)
                for (k = 1; k <= document.getElementById(o.id + "__numTotRows").value; k++) {
                    if (document.getElementById(o.id + "__" + k.toString() + "__Deleted")) {
                        for (arrCampoHijo in o.arrCamposHijos) {
                            var oHijoSrc = document.getElementById(o.id + "__" + o.arrCamposHijos[arrCampoHijo]);
                            if (oHijoSrc) {
                                var oHijo = document.getElementById(o.id + "__" + k.toString() + "__" + o.arrCamposHijos[arrCampoHijo]);
                                if (oHijo) {
                                    valor = oHijo.value
                                    tipoGS = oHijoSrc.getAttribute("tipoGS")
                                    if ((valor == "") || (valor == null)) {
                                        valor = "";
                                    }
                                    if (tipoGS == 121) {
                                        if (sUnidadOrganizativa.indexOf(valor + "_-_") == -1) {
                                            sUnidadOrganizativa = sUnidadOrganizativa + valor + "_-_";
                                            if (sUnidadOrganizativa == "_-_") { sUnidadOrganizativa = ""; }
                                        }
                                    }
                                    if ((tipoGS == 123) && (yaOrgCompras == 0)) {
                                        sCodOrgCompras = valor;
                                        if (sCodOrgCompras != "") {
                                            yaOrgCompras = 1;
                                        }
                                        if ((yaOrgCompras == 1) && (yaCentro == 1)) {
                                            var params = { sCodOrgCompras: sCodOrgCompras, sCodCentro: sCodCentro };
                                            $.when($.ajax({
                                                type: "POST",
                                                url: rutaFS + '_Common/BuscadorArticulos.aspx/DevolverUONOrgCompras',
                                                contentType: "application/json; charset=utf-8",
                                                data: JSON.stringify(params),
                                                dataType: "json",
                                                async: false
                                            })).done(function (msg) {
                                                var respuesta = msg.d;
                                                result = respuesta[0] + "-" + respuesta[1] + "-" + respuesta[2];
                                            });
                                            if (sUnidadOrganizativa.indexOf(result + "_-_") == -1) {
                                                sUnidadOrganizativa = sUnidadOrganizativa + result + "_-_";
                                                if (sUnidadOrganizativa == "_-_") { sUnidadOrganizativa = ""; }
                                            }
                                            //Los vuelvo a poner a 0 porque puede haber m�s
                                            yaCentro = 0;
                                            yaOrgCompras = 0;
                                        }
                                    }
                                    if ((tipoGS == 124) && (yaCentro == 0)) {
                                        sCentro = valor;
                                        if (sCentro != "") {
                                            yaCentro = 1;
                                        }
                                        //Si no ha encontrado campo OrgCompras puede existir pero estar oculto, lo cojo del idDataEntryDependent del centro
                                        if (yaOrgCompras == 0 && o.idDataEntryDependent) {
                                            oOrgCompras = fsGeneralEntry_getById(o.idDataEntryDependent);
                                            if (oOrgCompras) {
                                                sCodOrgCompras = oOrgCompras.OrgComprasDependent.value;
                                                if (sCodOrgCompras != "") {
                                                    yaOrgCompras = 1;
                                                }
                                            };
                                        }
                                        if ((yaOrgCompras == 1) && (yaCentro == 1)) {
                                            var params = { sCodOrgCompras: sCodOrgCompras, sCodCentro: sCodCentro };
                                            $.when($.ajax({
                                                type: "POST",
                                                url: rutaFS + '_Common/BuscadorArticulos.aspx/DevolverUONOrgCompras',
                                                contentType: "application/json; charset=utf-8",
                                                data: JSON.stringify(params),
                                                dataType: "json",
                                                async: false
                                            })).done(function (msg) {
                                                var respuesta = msg.d;
                                                result = respuesta[0] + "-" + respuesta[1] + "-" + respuesta[2];
                                            });
                                            if (sUnidadOrganizativa.indexOf(result + "_-_") == -1) {
                                                sUnidadOrganizativa = sUnidadOrganizativa + result + "_-_";
                                                if (sUnidadOrganizativa == "_-_") { sUnidadOrganizativa = ""; }
                                            }
                                            //Los vuelvo a poner a 0 porque puede haber m�s
                                            yaCentro = 0;
                                            yaOrgCompras = 0;
                                        }
                                    }
                                    if (tipoGS == 103) {
                                        if (sMaterial.indexOf(valor + "_-_") == -1) {
                                            sMaterial = sMaterial + valor + "_-_";
                                            if (sMaterial == "_-_") { sMaterial = ""; }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            } else {
                tipoGS = o.tipoGS
                valor = o.getDataValue()
                if ((valor == "") || (valor == null)) {
                    valor = "";
                }
                if (tipoGS == 121) {
                    if (sUnidadOrganizativa.indexOf(valor + "_-_") == -1) {
                        sUnidadOrganizativa = sUnidadOrganizativa + valor + "_-_";
                        if (sUnidadOrganizativa == "_-_") { sUnidadOrganizativa = ""; }
                    }
                }
                if ((tipoGS == 123) && (yaOrgCompras == 0)) {
                    sCodOrgCompras = valor;
                    if (sCodOrgCompras != "") {
                        yaOrgCompras = 1;
                    }
                    if ((yaOrgCompras == 1) && (yaCentro == 1)) {
                        var params = { sCodOrgCompras: sCodOrgCompras, sCodCentro: sCodCentro };
                        $.when($.ajax({
                            type: "POST",
                            url: rutaFS + '_Common/BuscadorArticulos.aspx/DevolverUONOrgCompras',
                            contentType: "application/json; charset=utf-8",
                            data: JSON.stringify(params),
                            dataType: "json",
                            async: false
                        })).done(function (msg) {
                            var respuesta = msg.d;
                            result = respuesta[0] + "-" + respuesta[1] + "-" + respuesta[2];
                        });
                        if (sUnidadOrganizativa.indexOf(result + "_-_") == -1) {
                            sUnidadOrganizativa = sUnidadOrganizativa + result + "_-_";
                            if (sUnidadOrganizativa == "_-_") { sUnidadOrganizativa = ""; }
                        }
                        //Los vuelvo a poner a 0 porque puede haber m�s
                        yaCentro = 0;
                        yaOrgCompras = 0;
                    }
                }
                if ((tipoGS == 124) && (yaCentro == 0)) {
                    sCentro = valor;
                    if (sCentro != "") {
                        yaCentro = 1;
                    }
                    //Si no ha encontrado campo OrgCompras puede existir pero estar oculto, lo cojo del idDataEntryDependent del centro
                    if (yaOrgCompras == 0 && o.idDataEntryDependent) {
                        oOrgCompras = fsGeneralEntry_getById(o.idDataEntryDependent);
                        if (oOrgCompras) {
                            sCodOrgCompras = oOrgCompras.OrgComprasDependent.value;
                            if (sCodOrgCompras != "") {
                                yaOrgCompras = 1;
                            }
                        };
                    }
                    if ((yaOrgCompras == 1) && (yaCentro == 1)) {
                        var params = { sCodOrgCompras: sCodOrgCompras, sCodCentro: sCentro };
                        $.when($.ajax({
                            type: "POST",
                            url: rutaFS + '_Common/BuscadorArticulos.aspx/DevolverUONOrgCompras',
                            contentType: "application/json; charset=utf-8",
                            data: JSON.stringify(params),
                            dataType: "json",
                            async: false
                        })).done(function (msg) {
                            var respuesta = msg.d;
                            result = respuesta[0] + "-" + respuesta[1] + "-" + respuesta[2];
                        });
                        if (sUnidadOrganizativa.indexOf(result + "_-_") == -1) {
                            sUnidadOrganizativa = sUnidadOrganizativa + result + "_-_";
                            if (sUnidadOrganizativa == "_-_") { sUnidadOrganizativa = ""; }
                        }
                        //Los vuelvo a poner a 0 porque puede haber m�s
                        yaCentro = 0;
                        yaOrgCompras = 0;
                    }
                }
                if (tipoGS == 103) {
                    if (sMaterial.indexOf(valor + "_-_") == -1) {
                        sMaterial = sMaterial + valor + "_-_";
                        if (sMaterial == "_-_") { sMaterial = ""; }
                    }
                }
            }
        }
    };
    CerrarDesplegables();
    var newWindow = window.open(rutaFS + "_common/BuscadorUsuarios.aspx?Valor=" + oEdit.getDataValue() + "&IDControl=" + oEdit.id + "&Campo=" + oEdit.idCampo + "&Comprador=1&sUO=" + sUnidadOrganizativa + "&sMat=" + sMaterial, "_blank", "width=800,height=575,status=yes,resizable=no,top=200,left=200,scrollbars=yes")
    newWindow.focus();
};
function Cantidad_ValueChange(oEdit, oldValue, oEvent) {
    var cant = 0;
    if (oEdit.fsEntry.TipoRecepcion == "1") {
        cant = oEdit.fsEntry.getValue();
        if (cant != 1) {
            oEdit.fsEntry.setValue('1');
            alert(smensajeRecep);
        }
    }
};
function showDetalleArticulo(oEdit, text, oEvent) {
    var valor;
    var oEntry = oEdit.fsEntry;
    if ((oEntry.tipoGS == 118) && (oEntry.isDesglose)) {
        idDataEntryCod = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna - 1, oEntry.nLinea, 0);
        var oCodArt = fsGeneralEntry_getById(idDataEntryCod);
        valor = oCodArt.getValue();
    } else {
        valor = oEntry.codigoArticulo;
    }
    var respuesta;
    var miUON = BuscarUonEnSolicitud(oEdit);
    var sUon = ["", "", ""];
    if (miUON != "") {
        var temp = miUON.split('-');
        for (i = 0; i < temp.length; i++) {
            sUon[i] = trim(temp[i]);
        }
    }
    params = { codArt: valor, UON1: sUon[0], UON2: sUon[1], UON3: sUon[2] }
    $.when($.ajax({
        type: "POST",
        url: rutaPM + 'ConsultasPMWEB.asmx/Detalle_Articulo',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(params),
        async: false
    })).done(function (msg) {
        respuesta = msg.d;
        $.when($.get(rutaPM + '_common/html/detalleArticulo.htm', function (detalleArtPopUp) {
            $('body').append(detalleArtPopUp);
        })).done(function () {
            MostrarPopUP(respuesta);
            $('#cellImagenCerrar').live('click', function () {
                $('#popupDetalleArticulo').hide();
                $('#popupFondo').hide();
            });
        });
    });
};
function MostrarPopUP(respuesta) {
    $('#popupFondo').css('height', $(document).height());
    $('#popupFondo').show();
    $.each(respuesta, function (key, value) {
        if (key == "numUon") {
            $(".tablaUON").remove()
            if (value == 0) {
                $('#popupDetalleArticulo').css('height', '200px');
                $('#popupDetalleArticulo').css('overflow', 'none');
            }
            for (i = 0; i < value; i++) {
                $('#tablasUON').append("<table id='tablaAtributos_" + i + "' cellpadding='4' cellspacing='1' class= 'tablaUON'><thead></thead><tbody></tbody></table>");
                $('#tablaAtributos_' + i + ' thead').append("<tr class='headerNoWrap headerTablaAtrib'></tr>");
                $('#tablaAtributos_' + i + ' thead tr').append("<th style='width: 50%;'><span id='lblAtributos" + i + "'></span></th><th class='Negrita'><span id='lblUON" + i + "' class='text'></span></th>");
            }
        } else if (key.substr(0, 5) == "tabla") {
            var num = key.substr(5, 1);
            if (value == "") {
                $("#tablaAtributos_" + num).hide();
                if (num == 0) {
                    $('#popupDetalleArticulo').css('height', '200px');
                    $('#popupDetalleArticulo').css('overflow', 'none');
                }
            } else {
                var table = $("#tablaAtributos_" + num);
                $("#tablaAtributos_" + num).show();
                $('#popupDetalleArticulo').css('height', '400px');
                table.find('tbody tr').remove();
                $('#tablaAtributos_' + num + ' > tbody:last').append(value);
            }
        } else {
            $('#' + key).html(value);
        }
    });
    if (document.getElementById("INPUTDESGLOSE")) {
        $('#popupDetalleArticulo').css('height', '225px');
        $('#popupDetalleArticulo').css('overflow', 'none');
    }
    $('#popupDetalleArticulo').show();
};
function BuscarUonEnSolicitud(oEdit) {
    sCodUnidadOrganizativa = "";
    sReadUnidadOrganizativa = "";
    var oUnidadOrganizativa;
    var result = "";
    var yaUnidadOrganizativa = 0;
    //Busca si ya hay UnidadOrganizativa seleccionada
    if (oEdit.fsEntry.idDataEntryDependent3) {
        oUnidadOrganizativa = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent3);
        if (oUnidadOrganizativa) {
            sCodUnidadOrganizativa = oUnidadOrganizativa.getDataValue();
            if ((sCodUnidadOrganizativa == "") || (sCodUnidadOrganizativa == null))
                sCodUnidadOrganizativa = "-1";
            else
                yaUnidadOrganizativa = 1;
            sReadUnidadOrganizativa = (oUnidadOrganizativa.readOnly ? "1" : "0");
        } else {
            if ((oEdit.fsEntry.UnidadOrganizativa) && (oEdit.fsEntry.UnidadOrganizativaDependent.value)) {
                oUnidadOrganizativa = fsGeneralEntry_getById(oEdit.fsEntry.UnidadOrganizativaDependent.value);
                sCodUnidadOrganizativa = oEdit.fsEntry.UnidadOrganizativaDependent.value;
                if ((sCodUnidadOrganizativa == "") || (sCodUnidadOrganizativa == null))
                    sCodUnidadOrganizativa = "-1";
                else
                    yaUnidadOrganizativa = 1;
                sReadUnidadOrganizativa = "1";
            };
        };
    } else {
        if ((oEdit.fsEntry.UnidadOrganizativaDependent) && (oEdit.fsEntry.UnidadOrganizativaDependent.value)) {
            oUnidadOrganizativa = fsGeneralEntry_getById(oEdit.fsEntry.UnidadOrganizativaDependent.value)
            sCodUnidadOrganizativa = oEdit.fsEntry.UnidadOrganizativaDependent.value
            if (sCodUnidadOrganizativa == "")
                sCodUnidadOrganizativa = "-1";
            else
                yaUnidadOrganizativa = 1;
            sReadUnidadOrganizativa = "1";
        } else {
            p = window.opener;
            if (p) {
                if (p.sDataEntryUnidadOrganizativaFORM) { // Si antes que un desglose de pop-up hay un dataEntry CENTRO
                    oUnidadOrganizativa = p.fsGeneralEntry_getById(p.sDataEntryUnidadOrganizativaFORM);
                    if (oUnidadOrganizativa) {
                        sCodUnidadOrganizativa = oUnidadOrganizativa.getDataValue();
                        if ((sCodUnidadOrganizativa == "") || (sCodUnidadOrganizativa == null))
                            sCodUnidadOrganizativa = "-1";
                        else
                            yaUnidadOrganizativa = 1;
                        sReadUnidadOrganizativa = (oUnidadOrganizativa.readOnly ? "1" : "0");
                    };
                } else {
                    if (p.sCodUnidadOrganizativaFORM) {
                        sCodUnidadOrganizativa = p.sCodUnidadOrganizativaFORM;
                        sReadUnidadOrganizativa = "1";
                    }
                };
            }
        }
    };
    if (yaUnidadOrganizativa == 1) {
        result = sCodUnidadOrganizativa;
    };
    //Si no he encontrado recorro todo
    if (yaUnidadOrganizativa == 0) {
        for (arrInput in arrInputs) {
            var o = fsGeneralEntry_getById(arrInputs[arrInput])
            if (o) {
                if (!oUnidadOrganizativa) {
                    //Por si te han puesto la uon en otro grupo
                    if ((o.tipoGS == 121) && (yaUnidadOrganizativa == 0)) {
                        sCodUnidadOrganizativa = o.getDataValue()
                        if ((sCodUnidadOrganizativa == "") || (sCodUnidadOrganizativa == null)) sCodUnidadOrganizativa = "-1"
                        sReadUnidadOrganizativa = (o.readOnly ? "1" : "0")
                        yaUnidadOrganizativa = 1;
                        result = sCodUnidadOrganizativa;
                    };
                };
                //Si todo ya cargado, acabaste
                if (yaUnidadOrganizativa == 1) {
                    break;
                };
            };
        };
    }
    if (result != "") {
        var lastIndex = result.lastIndexOf("-");
        var result = result.substring(0, lastIndex);
    }
    if (yaUnidadOrganizativa == 0) {
        //Si no hay Unidad Organizativa busco si hay OrgCompras seleccionado
        sCodOrgCompras = "";
        sReadOrgCompras = "";
        var yaOrgCompras = 0;
        var oOrgCompras;
        //Busca si ya hay OrgCompras
        if (oEdit.fsEntry.idDataEntryDependent) {
            oOrgCompras = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent);
            if (oOrgCompras) {
                sCodOrgCompras = oOrgCompras.getDataValue();
                if ((sCodOrgCompras == "") || (sCodOrgCompras == null))
                    sCodOrgCompras = "-1";
                else
                    yaOrgCompras = 1;
                sReadOrgCompras = (oOrgCompras.readOnly ? "1" : "0");
            } else if ((oEdit.fsEntry.OrgComprasDependent) && (oEdit.fsEntry.OrgComprasDependent.value)) {
                oOrgCompras = fsGeneralEntry_getById(oEdit.fsEntry.OrgComprasDependent.value);
                sCodOrgCompras = oEdit.fsEntry.OrgComprasDependent.value;
                if ((sCodOrgCompras == "") || (sCodOrgCompras == null))
                    sCodOrgCompras = "-1";
                else
                    yaOrgCompras = 1;
                sReadOrgCompras = "1";
            };
        } else {
            if ((oEdit.fsEntry.OrgComprasDependent) && (oEdit.fsEntry.OrgComprasDependent.value)) {
                oOrgCompras = fsGeneralEntry_getById(oEdit.fsEntry.OrgComprasDependent.value);
                sCodOrgCompras = oEdit.fsEntry.OrgComprasDependent.value;
                if ((sCodOrgCompras == "") || (sCodOrgCompras == null))
                    sCodOrgCompras = "-1";
                else
                    yaOrgCompras = 1;
                sReadOrgCompras = "1";
            } else {
                p = window.opener;
                if (p) {
                    if (p.sDataEntryOrgComprasFORM) { // Si antes que un desglose de pop-up hay un dataEntry ORGANIZACION COMPRAS
                        oOrgCompras = p.fsGeneralEntry_getById(p.sDataEntryOrgComprasFORM);
                        if (oOrgCompras) {
                            sCodOrgCompras = oOrgCompras.getDataValue();
                            if ((sCodOrgCompras == "") || (sCodOrgCompras == null))
                                sCodOrgCompras = "-1";
                            else
                                yaOrgCompras = 1;
                            sReadOrgCompras = (oOrgCompras.readOnly ? "1" : "0");
                        };
                    } else {
                        if (p.sCodOrgComprasFORM) {
                            sCodOrgCompras = p.sCodOrgComprasFORM;
                            sReadOrgCompras = "1";
                        };
                    };
                };
            };
        };
        sCodCentro = "";
        sReadCentro = "";
        var yaCentro = 0;
        var oCentro;
        //Busca si ya hay Centro de Coste
        if (oEdit.fsEntry.idDataEntryDependent2) {
            oCentro = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent2);
            if (oCentro) {
                sCodCentro = oCentro.getDataValue();
                if ((sCodCentro == "") || (sCodCentro == null))
                    sCodCentro = "-1";
                else
                    yaCentro = 1;
                sReadCentro = (oCentro.readOnly ? "1" : "0");
            } else {
                if ((oEdit.fsEntry.CentroDependent) && (oEdit.fsEntry.CentroDependent.value)) {
                    oCentro = fsGeneralEntry_getById(oEdit.fsEntry.CentroDependent.value);
                    sCodCentro = oEdit.fsEntry.CentroDependent.value;
                    if ((sCodCentro == "") || (sCodCentro == null))
                        sCodCentro = "-1";
                    else
                        yaCentro = 1;
                    sReadCentro = "1";
                };
            };
        } else {
            if ((oEdit.fsEntry.CentroDependent) && (oEdit.fsEntry.CentroDependent.value)) {
                oCentro = fsGeneralEntry_getById(oEdit.fsEntry.CentroDependent.value)
                sCodCentro = oEdit.fsEntry.CentroDependent.value
                if ((sCodCentro == "") || (sCodCentro == null))
                    sCodCentro = "-1";
                else
                    yaCentro = 1;
                sReadCentro = "1";
            } else {
                p = window.opener;
                if (p) {
                    if (p.sDataEntryCentroFORM) { // Si antes que un desglose de pop-up hay un dataEntry CENTRO
                        oCentro = p.fsGeneralEntry_getById(p.sDataEntryCentroFORM);
                        if (oCentro) {
                            sCodCentro = oCentro.getDataValue();
                            if ((sCodCentro == "") || (sCodCentro == null))
                                sCodCentro = "-1";
                            else
                                yaCentro = 1;
                            sReadCentro = (oCentro.readOnly ? "1" : "0");
                        };
                    } else {
                        if (p.sCodCentroFORM) {
                            sCodCentro = p.sCodCentroFORM;
                            sReadCentro = "1";
                        }
                    };
                }
            }
        };

        if (yaOrgCompras == 0) {
            for (arrInput in arrInputs) {
                var o = fsGeneralEntry_getById(arrInputs[arrInput])
                if (o) {
                    //Por si te han puesto la org y centro en otro grupo
                    if (!oOrgCompras) {
                        //Solo si no lo has obtenido a traves de oEdit
                        if ((o.tipoGS == 123) && (yaOrgCompras == 0)) {
                            sCodOrgCompras = o.getDataValue()
                            if ((sCodOrgCompras == "") || (sCodOrgCompras == null))
                                sCodOrgCompras = "-1";
                            else
                                yaOrgCompras = 1;
                            sReadOrgCompras = (o.readOnly ? "1" : "0")
                        }
                        if ((o.tipoGS == 124) && (yaCentro == 0)) {
                            sCodCentro = o.getDataValue()
                            if ((sCodCentro == "") || (sCodCentro == null))
                                sCodCentro = "-1";
                            else
                                yaCentro = 1;
                            sReadCentro = (o.readOnly ? "1" : "0")
                        }

                    };
                    //Si todo ya cargado , acabaste
                    if ((yaOrgCompras == 1) && (yaCentro == 1)) {
                        break;
                    };
                };
            };
        }

        if ((yaOrgCompras == 0) || (yaCentro == 0)) {
            //Si es pedido negociado/expres
            //Si es popup el arrInputs no contiene prove ni formapago ni org compras ni centro ni unidadorganizativa
            if ((oEdit.fsEntry.TipoSolicit == 9) || (oEdit.fsEntry.TipoSolicit == 8)) {
                p = window.opener;

                if (oEdit.fsEntry.TipoSolicit == 8) { //Express no carga ni prove ni formapago tras selecc art
                    //Solo pedidos Negociados admite lo de meter la org compras y centro en otro grupo.... En el resto esta como antes, lo q NO este en tu mismo grupo NO existe
                    yaOrgCompras = 1;
                    yaCentro = 1;
                }

                if (p) {
                    for (arrInput in p.arrInputs) {
                        var o = p.fsGeneralEntry_getById(p.arrInputs[arrInput])
                        if (o) {
                            //Por si te han puesto la org y centro en otro grupo
                            if ((o.tipoGS == 123) && (yaOrgCompras == 0)) {
                                sCodOrgCompras = o.getDataValue()
                                if ((sCodOrgCompras == "") || (sCodOrgCompras == null))
                                    sCodOrgCompras = "-1";
                                else
                                    yaOrgCompras = 1;
                                sReadOrgCompras = (o.readOnly ? "1" : "0")
                            };
                            //Por si te han puesto la org y centro en otro grupo
                            if ((o.tipoGS == 124) && (yaCentro == 0)) {
                                sCodCentro = o.getDataValue()
                                if ((sCodCentro == "") || (sCodCentro == null))
                                    sCodCentro = "-1";
                                else
                                    yaCentro = 1;
                                sReadCentro = (o.readOnly ? "1" : "0")
                            };
                            //Si todo ya cargado , acabaste
                            if ((yaOrgCompras == 1) && (yaCentro == 1)) {
                                break;
                            }
                        }
                    }
                }
            }
        };
        if ((yaOrgCompras == 1) && (yaCentro == 1)) {
            var params = { sCodOrgCompras: sCodOrgCompras, sCodCentro: sCodCentro };
            $.when($.ajax({
                type: "POST",
                url: rutaFS + '_Common/BuscadorArticulos.aspx/DevolverUONOrgCompras',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(params),
                dataType: "json",
                async: false
            })).done(function (msg) {
                var respuesta = msg.d;
                result = respuesta[0] + "-" + respuesta[1] + "-" + respuesta[2];
            });
        }
    }
    return result;
};
//Si hay campo comprador y tiene valor compruebo que sea v�lido para la UON/OrgCompras+Centro/Material seleccionado 
function ComprobarComprador(value, tipo) {
    var sComp = "";
    for (arrInput in arrInputs) {
        var o = fsGeneralEntry_getById(arrInputs[arrInput])
        if (o) {
            if (o.tipoGS == 144) {
                valor = o.getDataValue();
                sComp = valor;
                break;
            } else if (o.tipo == 9) { //Para los popup(desglose)
                for (k = 1; k <= document.getElementById(o.id + "__numTotRows").value; k++) {
                    if (document.getElementById(o.id + "__" + k.toString() + "__Deleted")) {
                        for (arrCampoHijo in o.arrCamposHijos) {
                            var oHijoSrc = document.getElementById(o.id + "__" + o.arrCamposHijos[arrCampoHijo]);
                            if (oHijoSrc) {
                                var oHijo = document.getElementById(o.id + "__" + k.toString() + "__" + o.arrCamposHijos[arrCampoHijo]);
                                if (oHijo) {
                                    valor = oHijo.value;
                                    tipoGS = oHijoSrc.getAttribute("tipoGS");
                                    if (tipoGS == 144) {
                                        sComp = valor;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    if (sComp == null) sComp = "";
    if (sComp == "") return;
    sValor = value + "_-_";
    //Compruebo que el comprador sea v�lido para la UON seleccionada 
    params = { comprador: sComp, valor: sValor, tipo: tipo }
    $.when($.ajax({
        type: "POST",
        url: rutaPM + 'ConsultasPMWEB.asmx/Comprobar_Comprador',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        respuesta = msg.d;
    });
    if (respuesta == "") {
        return;
    } else {
        mostrarMensaje(respuesta);
        //Borro el campo comprador porque no es v�lido
        o.setValue("");
        o.setDataValue("");
        o.title = ""
    }
};
//<summary>Busca el valor de un campo (primera coincidencia) a nivel de formulario</summary>
//<param name="tipoCampo">Tipo de campo GS a buscar</param>
function buscarCampoEnFormulario(tipoCampo) {
    var p = window;
    if (window.opener) p = window.opener;
    for (arrInput in p.arrInputs) {
        oEntry = p.fsGeneralEntry_getById(p.arrInputs[arrInput]);
        if (oEntry) {
            if (!oEntry.isDesglose) //Input perteneciente al formulario
            {
                if (!oEntry.basedesglose) { //Si isDesglose es false pero basedesglose es true es parte del desglose_hidden
                    if (oEntry.tipoGS == tipoCampo) {
                        return oEntry;
                    }
                }
            }
        }
    }
};
//<summary>Busca el valor de un campo al nivel que se pida, bien de Grupo o bien de Desglose</summary>
//<param name="tipoCampo">Tipo de campo GS a buscar</param>
function buscarCampoEnFormularioMismoNivel(tipoCampo, bDesglose) {
    for (arrInput in getWindow().arrInputs) {
        oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
        if (oEntry) {
            if (oEntry.isDesglose == bDesglose) //Input perteneciente al formulario
            {
                if (!oEntry.basedesglose) { //Si isDesglose es false pero basedesglose es true es parte del desglose_hidden
                    if (oEntry.tipoGS == tipoCampo) {
                        return oEntry;
                    }
                }
            }
        }
    }
};
function obtenerMonedaFormulario(oEdit) {
    //Busca en el formulario la moneda seleccionada si la hay, si no ha seleccionado muestra un mensaje.
    //Si el formulario no tiene el campo moneda lo toma de la instancia.
    var sMoneda = "";
    var oEntry = buscarCampoEnFormulario(102); //busca el campo de tipo moneda
    if (oEntry) {
        if (oEntry.getDataValue()) {
            sMoneda = oEntry.getDataValue();
            oEdit.fsEntry.MonSolicit = sMoneda;
            oEdit.fsEntry.instanciaMoneda = sMoneda;
        } else {
            alert(sMensajeMonedaObligatoria);
        }
    } else {
        sMoneda = oEdit.fsEntry.instanciaMoneda;
    }
    return sMoneda;
};
/* <summary>
''' Muestra una ventana popup con el detalle de total de adjudicaciones, de una l�nea determinada del desglose
''' </summary>
'''<parameters name="idInstancia">Id de la instancia de la que mostrar detalle</parameters>
'''<parameters name="idCampo">Id del campo que representa el total adjudicado/preadjudicado</parameters>
'''<parameters name="idLinea">Id de la l�nea del desglose de la que mostrar el detalle</parameters>
'''<parameters name="bPreadj">Indica si se muestran adjudicaciones o preadjudicaciones (true : preadjudicaciones, false: adjudicaciones) </parameters>
''' <returns></returns>
''' <remarks>Llamada desde: Todos los dataentry de tipo texto medio,largo y string ; Tiempo m�ximo: instantaneo</remarks>
*/
function showDetalleTotalLineaAdj(idInstancia, idCampo, idLinea, bPreadj) {
    var params = { lInstancia: idInstancia, lCampo: idCampo, lLinea: idLinea, bPreadj: bPreadj }
    $.when($.ajax({
        type: "POST",
        url: rutaPM + 'ConsultasPMWEB.asmx/Devolver_Adjudicaciones_Linea_Instancia',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        var textos = msg.d[0];
        var textosC = msg.d[1]
        var adjs = msg.d[2];
        $.get(rutaPM + '_common/html/detalleAdjudicacion.htm', function (detallePopUp) {
            $("#popupDetalleAdjudicacion").remove();
            $('body').append(detallePopUp);
            $('#totalLineaAdj').html(textos[0]);
            $('#lblSubTitulo').html(textos[1]);
            $("#tblAdjs").html("");

            //titulos
            var row = "";
            $.each(textosC, function () {
                row += "<th style='text-align:left'>" + this + "</th>";
            });

            $("#tblAdjs").append("<tr class='cabeceraDesglose'>" + row + "</tr>");
            //lineas
            $.each(adjs, function () {
                var row = "";
                var s = "";
                $.each(this, function (index, value) {
                    if (index == "Ultimo") { if (value == 0) { s = " class='cabeceraDesglose'" } }
                    row += "<td class='label " + index + "'>" + value + "</td>"
                });
                $("#tblAdjs").append("<tr" + s + ">" + row + "</tr>");
            });
            $('#tblAdjs').attr("border", "1");
            MostrarFondoPopUp();
            $("#popupDetalleAdjudicacion").show();
            $('#popupDetalleAdjudicacion').css('z-index', '10002');
            CentrarPopUp($('#popupDetalleAdjudicacion'));
            $('#cellImagenCerrar').live('click', function () {
                $('#popupDetalleAdjudicacion').hide();
                OcultarFondoPopUp();
            });
        });
    });
};
function MostrarFondoPopUp() {
    $('#popupFondo').css('height', $(document).height());
    $('#popupFondo').css('z-index', 10002);
    $('#popupFondo').show();
};
function OcultarFondoPopUp() {
    $('#popupFondo').hide();
    $('#popupFondo').css('z-index', 1001);
};
/* <summary>
''' Obtiene el entry del material a nivel de formulario del que depende el control actual. 
''' </summary>
'''<parameters name="oEdit">Entry actual</parameters>
''' <returns></returns>
*/
function getMaterialFormulario() {
    //si est� en el desglose
    if (window.opener) {
        if (window.opener.sDataEntryMaterialFORM)
            return window.opener.fsGeneralEntry_getById(window.opener.sDataEntryMaterialFORM);
    } else {
        if (sDataEntryMaterialFORM)
            return fsGeneralEntry_getById(sDataEntryMaterialFORM);
    }
};
function show_MensajeMaterialObligatorio() {
    p = window;
    if (window.opener) p = window.opener;
    alert(p.sMensajeMaterialObligatorio);
};
/**
* Obtiene la ventana correspondiente a la cabecera de la solicitud
**/
function getWindow() {
    p = window;
    if (window.opener) p = window.opener;
    return p;
};
function existeLineaDesgloseArticulos() {
    /*Comprueba si existe alguna linea del desglose con art�culos*/
    for (arrInput in arrInputs) {
        oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
        if (oEntry) {
            if (oEntry.isDesglose) //Input perteneciente al desglose
            {
                if (oEntry.tipoGS == 119 || oEntry.tipoGS == 104) {//Campos de articulos GS nuevo y viejo
                    if (oEntry.getValue() != '') return true;
                }
            }
            //desglose popup
            if (oEntry.arrCamposHijos) {
                for (arrCampoHijo in oEntry.arrCamposHijos) {
                    var oHijoSrc = document.getElementById(oEntry.id + "__" + oEntry.arrCamposHijos[arrCampoHijo]);
                    if (oHijoSrc) {
                        if (oHijoSrc.getAttribute("tipoGS") == 119 || oHijoSrc.getAttribute("tipoGS") == 104) {
                            for (k = 1; k <= document.getElementById(oEntry.id + "__numTotRows").value; k++) {
                                var oHijo = document.getElementById(oEntry.id + "__" + k.toString() + "__" + oEntry.arrCamposHijos[arrCampoHijo]);
                                if (oHijo.value != "") return true;
                            }
                        }
                    }
                }
            }
        }
    }
    return false;
};
/*Comprueba si existe alguna linea del desglose con precio unitario marcado para cargar la �ltima adjudicaci�n*/
function existeLineaDesglosePrecioUnitarioUltimaAdjudicacion() {
    for (arrInput in arrInputs) {
        oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
        if (oEntry) {
            if (oEntry.isDesglose) //Input perteneciente al desglose
            {
                if (oEntry.tipoGS == 9) { //Precio Unitario
                    if (oEntry.cargarUltADJ == true) return true;
                }
            }
            //desglose popup
            if (oEntry.arrCamposHijos) {
                for (arrCampoHijo in oEntry.arrCamposHijos) {
                    var oHijoSrc = document.getElementById(oEntry.id + "__" + oEntry.arrCamposHijos[arrCampoHijo]);
                    if (oHijoSrc) {
                        if (oHijoSrc.getAttribute("tipoGS") == 9) {
                            if (oHijoSrc.getAttribute("cargarUltADJ") == "1") return true;
                        }
                    }
                }
            }
        }
    }
    return false;
};
function comprobarAnyoPartidaUnica(pres0, pres5) {  
    var respuesta = "";
    params = { contextKey: pres0 + '@@' + pres5 + '@@@151' }
    $.when($.ajax({
        type: "POST",
        url: rutaPM + 'ConsultasPMWEB.asmx/ObtenerDatos_DropDown',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        if (msg.d != null) {
            respuesta = jQuery.parseJSON('[' + msg.d + ']')
        }
    });
    return respuesta;
};
function comprobarPartidaUnica(pres5, value) {
    var respuesta = "";
    params = { sPRES5: pres5, sCodCentroCoste: value }
    $.when($.ajax({
        type: "POST",
        url: rutaPM + 'ConsultasPMWEB.asmx/Obtener_PartidasCentro',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        respuesta = msg.d;
    });
    return respuesta;
};
function comprobarCentroUnico(verUON) {
    var respuesta = "";
    params = { bverUON: verUON }
    $.when($.ajax({
        type: "POST",
        url: rutaPM + 'ConsultasPMWEB.asmx/Obtener_CentroCoste',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        respuesta = msg.d;
    });
    return respuesta;
};
//Aplica el cambio de la nueva moneda seleccionada a los precios unitarios de los desgloses.
function aplicarCambioMonedaImportesUnitarios(oldCodMoneda, newCodMoneda) {
    var oldCambio = obtenerCambioMoneda(oldCodMoneda)
    var newCambio = obtenerCambioMoneda(newCodMoneda);
    var nuevoImporte = 0;

    for (arrInput in arrInputs) {
        oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
        if (oEntry) {
            if (oEntry.isDesglose) //Input perteneciente al desglose
            {
                if (oEntry.tipoGS == 9) {
                    if ((oEntry.getValue() != '') && (oEntry.getValue() != null)) {
                        nuevoImporte = oEntry.getValue() * parseFloat(newCambio.replace(",", ".")) / parseFloat(oldCambio.replace(",", "."))
                        oEntry.setValue(nuevoImporte)
                        oEntry.setDataValue(nuevoImporte)
                    }
                }
            }
            //desglose popup
            if (oEntry.arrCamposHijos) {
                for (arrCampoHijo in oEntry.arrCamposHijos) {
                    var oHijoSrc = document.getElementById(oEntry.id + "__" + oEntry.arrCamposHijos[arrCampoHijo]);
                    if (oHijoSrc) {
                        if (oHijoSrc.getAttribute("tipoGS") == 9) {
                            for (k = 1; k <= document.getElementById(oEntry.id + "__numTotRows").value; k++) {
                                var oHijo = document.getElementById(oEntry.id + "__" + k.toString() + "__" + oEntry.arrCamposHijos[arrCampoHijo]);
                                if ((oHijo.value != "") && (oHijo.value != null)) {
                                    oHijo.value = (oHijo.value * parseFloat(newCambio.replace(",", "."))) / parseFloat(oldCambio.replace(",", "."))
                                }
                            }
                        }
                    }
                }
            }
        }
    }
};
//Obtiene el cambio actual de una moneda
function obtenerCambioMoneda(codMoneda) {
    var respuesta = "";
    params = { sCodMoneda: codMoneda }
    $.when($.ajax({
        type: "POST",
        url: rutaPM + 'ConsultasPMWEB.asmx/Obtener_CambioMoneda',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        respuesta = msg.d;
    });
    return respuesta;
};
/*
''' <summary>
''' Comprobar si el entry q se poporciona es el campo de donde se quiere sacar el param de entrada
''' </summary>
''' <param name="oEntry">Entry donde busco</param>
''' <param name="lineaDesglose">Linea donde busco</param>        
''' <param name="CampoId">Id del Campo que busco</param>   
''' <remarks>Llamada desde: llamarServicioExterno ; Tiempo m�ximo: 0</remarks>*/
function DameDatoParamServicio(oEntry, lineaDesglose, CampoId) {
    var Dato;
    Dato = '###NODATO###'
    var encontrado = false;

    if (lineaDesglose == "0" || (lineaDesglose != "0" && oEntry.idLinea == lineaDesglose)) {        
        if (oEntry.campo_origen == 0) { //Alta de formulario
            if (oEntry.idCampo == CampoId) {
                if (oEntry.intro == 2) {
                    Dato = oEntry.getValue();
                }
                else {
                    Dato = oEntry.getDataValue();
                }
                encontrado = true;
            }
        } else
            if (oEntry.campo_origen == CampoId) {
                if (oEntry.intro == 2) {
                    Dato = oEntry.getValue();
                }
                else {
                    Dato = oEntry.getDataValue();
                }
                encontrado = true;
            }        

        if (encontrado == true) {
            //Las fechas llegan como 2017-8-25-10-54-57-920. Integraci�n necesita 2017-8-25 10:54:57.920
            //Las fechas pueden no estar rellenas. Integraci�n necesita fecha, mandas 0001-01-01 00:00:00.000
            if (oEntry.tipo == 3) {
                if (Dato == '') {
                    Dato = '0001-01-01 00:00:00.000';
                } else {
                    arrAux = Dato.split("-")
                    Dato = arrAux[0] + '-' + arrAux[1] + '-' + arrAux[2] + ' ' + arrAux[3] + ':' + arrAux[4] + ':' + arrAux[5] + '.' + arrAux[6]
                }
            } else {
                //Los si/no llegan como 1 o 0. Integraci�n necesita true/false
                //Los si/no sin rellenar. Integraci�n necesita true/false, mandas false.
                if (oEntry.tipo == 4) {
                    if (Dato == '') {
                        Dato = false
                    } else {
                        if (Dato == 1) {
                            Dato = true;
                        } else {
                            Dato = false;
                        }
                    }
                } else {
                    //Los numericos pueden no estar rellenos. Integraci�n necesita un numero, mandas 0.
                    if (oEntry.tipo == 2) {
                        if (Dato == null) {
                            Dato = 0
                        }
                    }
                }
            }
        }
    }

    return Dato
};
/*''' <summary>Establece las variables de contexto para el extender del c�digo de art�culo y denominaci�n de art�culo</summary>  
''' <remarks>Llamada desde:</remarks>*/
function SetContextKey(oEdit) {
    oEdit = oEdit[0];
    if (sContextKey == undefined || sContextKey == "") {
        var nivelSeleccion = 0;
        //uwtGruposxctl0xctl0xfsdsentryx1x1793xdd
        for (i = 0; i < oGridsDesglose.length; i++) {
            if (oGridsDesglose[i].Editor + "__t" == oEdit.id || oGridsDesglose[i].Editor + "__tden" == oEdit.id) vGrid = oGridsDesglose[i]
        }

        var sGridName = vGrid.ID + '_tblDesglose';
        var sAux = oEdit.id;
        if (sAux.search("fsdsentry") >= 0) {
            sInit = sAux.substr(0, sAux.search("fsdsentry"))
            sAux = sAux.substr(sAux.search("fsdsentry"), sAux.length)
            arrAux = sAux.split("_")
            lIndex = arrAux[1]
        } else {
            sInit = sAux.substr(0, sAux.search("fsentry"))
            lIndex = "-1"
        }

        var oGrid = document.getElementById(sGridName);
        o = fsGeneralEntry_getById(sInit + vGrid.IdEntryMaterial);
        var sMat = "";
        var sRead = "";
        var sMatId = "";
        //Material
        //Nivel de seleccion de material 
        var oEntryMaterial = getMaterialFormulario();
        if (oEntryMaterial && oEntryMaterial.NivelSeleccion != 0) nivelSeleccion = oEntryMaterial.NivelSeleccion
        //si hay un nivel obligatorio a nivel de cabecera y no hemos seleccionado un material de linea
        if (oEntryMaterial && oEntryMaterial.NivelSeleccion != 0 && !o.getDataValue()) {
            //Si no hemos seleccionado un material a nivel de l?nea ...
            //si existe un campo mat fuera desglose con nivel de seleccion obligatorio se carga este material
            sMat = oEntryMaterial.dataValue;
            //si no se ha seleccionado ningun material
            if (!sMat || sMat == "") {
                show_MensajeMaterialObligatorio();
                return false;
            }
            if (o) {
                sRead = (o.readOnly ? "1" : "0");
                sMatId = o.Id;
            }
        } else if (o) {
            sMat = o.getDataValue();
            sRead = (o.readOnly ? "1" : "0");
            sMatId = o.id;
        } else sMat = oEdit.fsEntry.Dependent.value;

        if (sMat == "")
            if (oEdit.fsEntry.Restrict)
                sMat = oEdit.fsEntry.Restrict;

        //Org. compras - centro
        sCodOrgCompras = "";
        var oOrgCompras;
        if (oEdit.fsEntry.idDataEntryDependent) {
            oOrgCompras = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent);
            if (oOrgCompras) {
                sCodOrgCompras = oOrgCompras.getDataValue();
            } else if ((oEdit.fsEntry.OrgComprasDependent) && (oEdit.fsEntry.OrgComprasDependent.value)) {
                oOrgCompras = fsGeneralEntry_getById(oEdit.fsEntry.OrgComprasDependent.value);
                sCodOrgCompras = oEdit.fsEntry.OrgComprasDependent.value;
            };
        } else {
            if ((oEdit.fsEntry.OrgComprasDependent) && (oEdit.fsEntry.OrgComprasDependent.value)) {
                oOrgCompras = fsGeneralEntry_getById(oEdit.fsEntry.OrgComprasDependent.value);
                sCodOrgCompras = oEdit.fsEntry.OrgComprasDependent.value;
            } else {
                p = window.opener;
                if (p) {
                    if (p.sDataEntryOrgComprasFORM) { // Si antes que un desglose de pop-up hay un dataEntry ORGANIZACION COMPRAS
                        oOrgCompras = p.fsGeneralEntry_getById(p.sDataEntryOrgComprasFORM);
                        if (oOrgCompras) {
                            sCodOrgCompras = oOrgCompras.getDataValue();
                        };
                    } else
                        if (p.sCodOrgComprasFORM)
                            sCodOrgCompras = p.sCodOrgComprasFORM;
                };
            };
        };
        sCodCentro = "";
        var oCentro;
        if (oEdit.fsEntry.idDataEntryDependent2) {
            oCentro = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent2);
            if (oCentro) {
                sCodCentro = oCentro.getDataValue();
            } else {
                if ((oEdit.fsEntry.CentroDependent) && (oEdit.fsEntry.CentroDependent.value)) {
                    oCentro = fsGeneralEntry_getById(oEdit.fsEntry.CentroDependent.value);
                    sCodCentro = oEdit.fsEntry.CentroDependent.value;
                };
            };
        } else {
            if ((oEdit.fsEntry.CentroDependent) && (oEdit.fsEntry.CentroDependent.value)) {
                oCentro = fsGeneralEntry_getById(oEdit.fsEntry.CentroDependent.value)
                sCodCentro = oEdit.fsEntry.CentroDependent.value
            } else {
                p = window.opener;
                if (p) {
                    if (p.sDataEntryCentroFORM) { // Si antes que un desglose de pop-up hay un dataEntry CENTRO
                        oCentro = p.fsGeneralEntry_getById(p.sDataEntryCentroFORM);
                        if (oCentro) {
                            sCodCentro = oCentro.getDataValue();
                        };
                    } else {
                        if (p.sCodCentroFORM)
                            sCodCentro = p.sCodCentroFORM;
                    };
                }
            }
        };

        //UON
        sCodUnidadOrganizativa = "";
        var oUnidadOrganizativa;
        if (oEdit.fsEntry.idDataEntryDependent3) {
            oUnidadOrganizativa = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent3);
            if (oUnidadOrganizativa) {
                sCodUnidadOrganizativa = oUnidadOrganizativa.getDataValue();
            } else {
                if ((oEdit.fsEntry.UnidadOrganizativa) && (oEdit.fsEntry.UnidadOrganizativaDependent.value)) {
                    oUnidadOrganizativa = fsGeneralEntry_getById(oEdit.fsEntry.UnidadOrganizativaDependent.value);
                    sCodUnidadOrganizativa = oEdit.fsEntry.UnidadOrganizativaDependent.value;
                };
            };
        } else {
            if ((oEdit.fsEntry.UnidadOrganizativaDependent) && (oEdit.fsEntry.UnidadOrganizativaDependent.value)) {
                oUnidadOrganizativa = fsGeneralEntry_getById(oEdit.fsEntry.UnidadOrganizativaDependent.value)
                sCodUnidadOrganizativa = oEdit.fsEntry.UnidadOrganizativaDependent.value
            } else {
                p = window.opener;
                if (p) {
                    if (p.sDataEntryUnidadOrganizativaFORM) { // Si antes que un desglose de pop-up hay un dataEntry CENTRO
                        oUnidadOrganizativa = p.fsGeneralEntry_getById(p.sDataEntryUnidadOrganizativaFORM);
                        if (oUnidadOrganizativa) {
                            sCodUnidadOrganizativa = oUnidadOrganizativa.getDataValue();
                        };
                    } else {
                        if (p.sCodUnidadOrganizativaFORM) {
                            sCodUnidadOrganizativa = p.sCodUnidadOrganizativaFORM;
                        }
                    };
                }
            }
        };

        //Centro coste
        var sCodCentroCoste = "";
        var oCentroCoste;
        if (oEdit.fsEntry.ActivoSM) {
            if (oEdit.fsEntry.idDataEntryDependentCentroCoste) {
                oCentroCoste = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependentCentroCoste);
                if (oCentroCoste) {
                    sCodCentroCoste = oCentroCoste.getDataValue();
                }
            } else {
                p = window.opener;
                if (p) {
                    if (p.sDataEntryCentroCosteFORM) { // Si antes que un desglose de pop-up hay un dataEntry CENTRO
                        oCentroCoste = p.fsGeneralEntry_getById(p.sDataEntryCentroCosteFORM);
                        if (oCentroCoste) {
                            sCodCentroCoste = oCentroCoste.getDataValue();
                        }
                    }
                }
            }
        }

        var yaOrgCompras = 0;
        var yaCentro = 0;
        var yaUnidadOrganizativa = 0;
        var yaCentroCoste = 0;
        if (oOrgCompras) yaOrgCompras = 1;
        //Asociado al artic de la linea desglose esta el centro: No buscar. La org compra puede no estar asociada pq esta fuera del desglose-> buscar org pero no centro.
        //Asociado al artic q es campo (no desglose) esta el centro: No buscar.
        if (oCentro) yaCentro = 1;
        if (oUnidadOrganizativa) yaUnidadOrganizativa = 1;
        var sCodProve = "";
        var sIDCodProve = "";
        var yaProve = 0;
        if ((oEdit.fsEntry.TipoSolicit != 9) && (oEdit.fsEntry.TipoSolicit != 8)) yaProve = 1;
        if (oCentroCoste || !oEdit.fsEntry.ActivoSM) yaCentroCoste = 1;

        for (arrInput in arrInputs) {
            var o = fsGeneralEntry_getById(arrInputs[arrInput])
            if (o) {
                //Por si te han puesto la org y centro en otro grupo
                if (!oOrgCompras) {
                    //Solo si no lo has obtenido a traves de oEdit
                    if ((o.tipoGS == 123) && (yaOrgCompras == 0)) {
                        sCodOrgCompras = o.getDataValue();
                        yaOrgCompras = 1;
                    }
                    if ((o.tipoGS == 124) && (yaCentro == 0)) {
                        sCodCentro = o.getDataValue();
                        yaCentro = 1;
                    }

                };
                if (!oUnidadOrganizativa) {
                    //Por si te han puesto la uon en otro grupo
                    if ((o.tipoGS == 121) && (yaUnidadOrganizativa == 0)) {
                        sCodUnidadOrganizativa = o.getDataValue();
                        if (sCodUnidadOrganizativa == null) sCodUnidadOrganizativa = "";
                        yaUnidadOrganizativa = 1;
                    };
                };
                if (o.ActivoSM && !oCentroCoste) {
                    //Por si te han puesto el centro de coste en otro grupo
                    if ((o.tipoGS == 129) && (yaCentroCoste == 0)) {
                        sCodCentroCoste = o.getDataValue();
                        if (sCodCentroCoste == null) sCodCentroCoste = "-1";
                        yaCentroCoste = 1;
                    };
                };

                //Determinar proveedor, si es pedido negociado/express
                if ((o.tipoGS == 100) && (yaProve == 0)) {
                    sCodProve = o.getDataValue();
                    yaProve = 1;

                    if (oEdit.fsEntry.TipoSolicit == 9 || oEdit.fsEntry.TipoSolicit == 8) sIDCodProve = o.id;
                }

                //Si todo ya cargado , acabaste
                if ((yaOrgCompras == 1) && (yaCentro == 1) && (yaProve == 1) && (yaUnidadOrganizativa == 1) && (yaCentroCoste == 1)) break;
            };
        };

        if ((yaProve == 0) || (yaOrgCompras == 0) || (yaCentro == 0)) {
            //Si es pedido negociado/expres
            //Si es popup el arrInputs no contiene prove ni formapago ni org compras ni centro ni unidadorganizativa
            if ((oEdit.fsEntry.TipoSolicit == 9) || (oEdit.fsEntry.TipoSolicit == 8)) {
                p = window.opener;

                if (oEdit.fsEntry.TipoSolicit == 8) { //Express no carga ni prove ni formapago tras selecc art                     
                    //Solo pedidos Negociados admite lo de meter la org compras y centro en otro grupo.... En el resto esta como antes, lo q NO este en tu mismo grupo NO existe
                    yaOrgCompras = 1;
                    yaCentro = 1;
                }

                if (p) {
                    for (arrInput in p.arrInputs) {
                        var o = p.fsGeneralEntry_getById(p.arrInputs[arrInput])
                        if (o) {
                            //Determinar proveedor, si es pedido negociado/express
                            if ((o.tipoGS == 100) && (yaProve == 0)) {
                                sCodProve = o.getDataValue();
                                yaProve = 1;

                                if (oEdit.fsEntry.TipoSolicit == 9 || oEdit.fsEntry.TipoSolicit == 8) sIDCodProve = o.id;
                            };
                            //Por si te han puesto la org y centro en otro grupo
                            if ((o.tipoGS == 123) && (yaOrgCompras == 0)) {
                                sCodOrgCompras = o.getDataValue();
                                yaOrgCompras = 1;
                            };
                            //Por si te han puesto la org y centro en otro grupo
                            if ((o.tipoGS == 124) && (yaCentro == 0)) {
                                sCodCentro = o.getDataValue();
                                yaCentro = 1;
                            };
                            //Si todo ya cargado , acabaste
                            if ((yaProve == 1) && (yaOrgCompras == 1) && (yaCentro == 1)) break;
                        }
                    }
                }
            }
        };
        p = window.opener;

        if (oEdit.fsEntry.TipoSolicit == 5) {
            if (document.getElementById("hid_Proveedor")) {
                sCodProve = document.getElementById("hid_Proveedor").value
            } else {
                if (window.parent.location.toString().search("desglose.aspx") != -1) {
                    sCodProve = p.document.getElementById("hid_Proveedor").value
                }
            }
        }
        if ((oEdit.fsEntry.TipoSolicit == 2) || (oEdit.fsEntry.TipoSolicit == 3)) {
            if (window.parent.location.toString().search("altacertificado.aspx") != -1) {
                var Params = window.parent.location.search.toString();

                var Inicio = Params.search("Proveedores=");
                var Fin = Params.search("&volver");
                var NCar = "Proveedores=".length;

                Params = Params.substr(Inicio + NCar, Fin - Inicio - NCar);
                arrAux = Params.split("'");
                if (arrAux[3] == null) sCodProve = arrAux[1];
            } else {
                if (document.getElementById("Proveedor")) {
                    sCodProve = document.getElementById("Proveedor").value
                } else {
                    if (window.parent.location.toString().search("desglose.aspx") != -1) {
                        sCodProve = p.document.getElementById("Proveedor").value
                    }
                }
            }
        }

        //Proveedor que suministra el art�culo
        var sCodProveSumiArt = "";
        if (oEdit.fsEntry.idEntryProveSumiArt) {
            var o = fsGeneralEntry_getById(oEdit.fsEntry.idEntryProveSumiArt);
            if (o.CargarProveSumiArt) { sCodProveSumiArt = o.getDataValue(); }
        }

        //Si no hay unidad organizativa y s� centro de coste se asigna a la unidad organizativa las UONs correspondientes al centro de coste para abrir el buscador            
        if ((sCodUnidadOrganizativa == '') && sCodCentroCoste != '') sCodUnidadOrganizativa = sCodCentroCoste.replace('#', ' - ');

        var bDen = "0";
        if (vGrid.Editor + "__tden" == oEdit.id) bDen = "1";
        sContextKey = bDen + "&" + sMat + "&" + (!sCodOrgCompras ? "" : sCodOrgCompras) + "&" + (!sCodCentro ? "" : sCodCentro) + "&" + sCodUnidadOrganizativa + "&" + sCodProve + "&" + oEdit.fsEntry.TipoSolicit.toString() + "&" + sCodProveSumiArt;
    }

    var oExtender = $find(oEdit.id + '_ext');
    oExtender.set_contextKey(sContextKey);
};
/*
''' <summary>Limpia las variables de contexto para el extender del c�digo de art�culo y denominaci�n de art�culo</summary>  
''' <remarks>Llamada desde:</remarks>*/
function ResetContextKey() {
    sContextKey = "";
};
/*
''' <summary>establece el ancho del extender</summary>  
''' <remarks>Llamada desde:</remarks>*/
function onCodListPopulated(sender, e) {
    //establecer el ancho del extender a autom�tico    
    var completionList = sender.get_completionList();
    completionList.style.width = 'auto';
    //La lista del extender se carga mediante pares text-value en el servicio
    //Al desplegar el extender se muestra el valor de text, aunque iternamente se mantiene una prop. _value que permite acceder al otro elemento del par.
    //El valor que se traslada al textbox es text, es decir, lo que se muestra
    //Como se se debe mostrar "c�digo - denominaci�n" si se da este valor a text ser� lo que se trslade al evento ddNuevoCodArticulo_ValueChange, dando lugar a una validaci�n incorrecta.
    //Si s�lo se mostrar "c�digo" dar�a lugar a una validaci�n correcta, pero mostrar s�lo los c�digos en el extender ser�a de poco valor al usuario 
    //Para que se muestre "c�digo - denominaci�n" sin cambiar el texto que ser� lo que se traslade al textbox se edita cada elemento del extender para a�adir un objeto de texto con la denominaci�n
    //Esto parece no modificar el texto (c�digo) que se manda al textbox y nos perminte mostrar lo que queremos (c�digo - denominaci�n)
    var Articulos = completionList.childNodes;
    for (var i = 0; i < Articulos.length; i++) {
        var t = document.createTextNode(" - " + Articulos[i]._value);
        Articulos[i].appendChild(t);
    }
};
function onDenExtenderItemSelected(sender, e) {
    if (e._text != null) {
        var oEdit = $('#' + sender._element.id.substring(0, sender._element.id.length - 2));
        oEdit[0].fsEntry.setValue(e._text);
        var IDEntryCod = calcularIdDataEntryCelda(oEdit[0].id, oEdit[0].fsEntry.nColumna - 1, oEdit[0].fsEntry.nLinea, 0);
        $('#' + IDEntryCod + "__t")[0].fsEntry.setValue(e._text);
        ddNuevoCodArticulo_ValueChange($('#' + IDEntryCod + "__t"), e._text);
    }
};
/*
''' <summary>establece el contextkey para el autocompleteextender de atributos de lista externa</summary>  
''' <remarks>Llamada desde:</remarks>*/
function SetAtribListaExternaContextKey(oEdit) {
    oEdit = oEdit[0];
    if (oEdit.fsEntry.readOnly == true) {
        return
    }
    if (sAtribsListaExtContextKey == undefined || sAtribsListaExtContextKey == "") ObtenerDatosAtribListaExterna(oEdit);
    //Org.compras + "&" + UON + "&" + IdAtrib
    sAtribsListaExtContextKey = OrgCompras + "&" + UON + "&" + idAtributoListaExterna;
    sAtribsListaExtContextKey = sAtribsListaExtContextKey + "&" + atributos + "&" + CodProveedor;
    var oExtender = $find(oEdit.id + '_ext');
    oExtender.set_contextKey(sAtribsListaExtContextKey);
};
/*
''' <summary>limpia el contextkey para el autocompleteextender de atributos de lista externa</summary>  
''' <remarks>Llamada desde:</remarks>*/
function ResetAtribListaExternaContextKey() {
    sAtribsListaExtContextKey = "";
};
var itemListaExternaSeleccionado = false;
function onListaExternaExtenderItemSelected(sender, e) {
    document.getElementById(sender._element.id).innerText = e._value;
    var oEdit = $('#' + sender._element.id.substring(0, sender._element.id.length - 2));
    var IDEntryCod = calcularIdDataEntryCelda(oEdit[0].id, oEdit[0].fsEntry.nColumna, oEdit[0].fsEntry.nLinea, 0);
    $('#' + IDEntryCod + "__t")[0].fsEntry.setValue(e._text + " - " + e._value);
    itemListaExternaSeleccionado = true;
};
/*
''' <summary>muestra el buscador de atributos de lista externa</summary>  
''' <remarks>Llamada desde:</remarks>*/
function MostrarBuscadorAtributosListaExterna(oEdit, text, denominacion, lblBuscador, lblCodigo, lblBuscar, lblLimpiar, lblDenominacion, sMensajeFaltaDatos) {    
    if (!popupListaExternaCargado) {
        popupListaExternaCargado = true;
        $.get(rutaPM + '_common/html/buscadorListaExterna.htm?version=' + version, function (buscadorPopUp) {
            buscadorPopUp = buscadorPopUp.replace(/src="/gi, 'src="' + rutaPM);
            $('body').append(buscadorPopUp);

            $('#popupBuscadorListaExterna span.RotuloGrande').text(lblBuscador + ' ' + denominacion);
            $('#lblCodigoListaExterna').text(lblCodigo + ': ');
            $('#lblDenominacion').text(lblDenominacion + ': ');
            $('#btnBuscar').text(lblBuscar);
            $('#btnLimpiar').text(lblLimpiar);
            $('#headerCodigo').text(lblCodigo);
            $('#headerDenominacion').text(lblDenominacion);

            $('#txtCodigo').val('');
            $('#txtDenominacion').val('');
            $('#grResultadosListaExterna').empty();

            if (ObtenerDatosAtribListaExterna(oEdit[0], sMensajeFaltaDatos)) {
                $('#popupFondo').css('height', $(document).height());
                $('#popupFondo').show();
                $('#popupBuscadorListaExterna').show();
                CentrarPopUp($('#popupBuscadorListaExterna'));
            }
        });
    }
    else {
        $('#popupBuscadorListaExterna span.RotuloGrande').text(lblBuscador + ' ' + denominacion);

        $('#txtCodigo').val('');
        $('#txtDenominacion').val('');
        $('#grResultadosListaExterna').empty();

        if (ObtenerDatosAtribListaExterna(oEdit[0], sMensajeFaltaDatos)) {
            $('#popupFondo').css('height', $(document).height());
            $('#popupFondo').show();
            $('#popupBuscadorListaExterna').show();
            CentrarPopUp($('#popupBuscadorListaExterna'));
        }
    }
};
function ComprobarAtribListaExterna(oEdit, sMsgNoValido) {
    if (!itemListaExternaSeleccionado && oEdit[0].fsEntry.getValue() != null && oEdit[0].fsEntry.getValue() != "") {
        ObtenerDatosAtribListaExterna(oEdit[0]);
        var param = JSON.stringify({ IdAtributoListaExterna: idAtributoListaExterna, codigoBuscado: oEdit[0].fsEntry.getValue(), denominacionBuscado: "", sCodOrgCompras: OrgCompras, sUON: UON, atributos: atributos, sCodProveedor: CodProveedor });
        $.when($.ajax({
            type: "POST",
            url: rutaPM + 'AutoCompletePMWEB.asmx/Obtener_Datos_ListaExterna',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: param,
            async: true
        })).done(function (msg) {
            var bEncontrado = false;
            $.each(msg.d, function () {
                var valor = oEdit[0].fsEntry.getValue();
                var index = valor.indexOf(" - ");
                if (index > -1) valor = valor.substr(0, index);
                if (this.id == valor) {
                    bEncontrado = true;
                    return;
                }
            });
            if (!bEncontrado) {
                alert(sMsgNoValido);
                oEdit[0].fsEntry.setValue("");
            }
        });
    }
    if (itemListaExternaSeleccionado) itemListaExternaSeleccionado = false;
};
/*
''' <summary>
''' Obtener los datos para la carga de un Atrib de Lista Externa
''' </summary>
''' <param name="oEdit">Entry atrib</param>
''' <param name="sMensajeFaltaDatos">Si se quiere comprobar q haya o no, datos suficientes para la busqueda de la lista (no comprobar <- no se pasa parametro).</param>
''' <returns>Si hay datos suficientes para la busqueda de la lista</returns>        
''' <remarks>Llamada desde: SetAtribListaExternaContextKey      MostrarBuscadorAtributosListaExterna        ComprobarAtribListaExterna; Tiempo m�ximo: 0</remarks>*/
function ObtenerDatosAtribListaExterna(oEdit, sMensajeFaltaDatos) {
    var sOrgCompras = "";
    var sUON = "";        

    idAtributoListaExterna = fsGeneralEntry_getById(oEdit.fsEntry.id).IdAtrib;
    oEditAtribListaExt = oEdit;

    //Primero se busca la Org. compras, si no se encuentra se busca en centro sm, si no si no se encuentra la UON                    
    if (oEdit.fsEntry.UsarOrgCompras) { sOrgCompras = ObtenerOrgCompras(oEdit) };
    if (sOrgCompras == "") {
        if (oEdit.fsEntry.ActivoSM) { sUON = ObtenerUONCentroSM(oEdit) };
        if (sUON == "") { sUON = ObtenerUON(oEdit) };
    }

    //Atributos y proveedor
    atributos = '';
    var yaProve = false;
    if (typeof (arrInputs) != "undefined") {
        //1ro Saber si es campo o desglose popup o desglose no popup
        var bEsCampo, lCampoPadre, iLineaSel, bPopUp, vValor;
        if (oEdit.fsEntry.idCampoPadre > 0) {
            bEsCampo = false;

            lCampoPadre = oEdit.fsEntry.idCampoPadre;

            //No es la linea q va a ir a bbdd. Es la linea en la q pulso lupa lista extena. Q si borras lineas puede ser la 7 pero para bbdd sera la 5. Esto debe sacar lo atributos de la 7.
            var a = new Array();
            a = oEdit.id.split("_");
            var IndiceLinea;
            if (a.length == 11) {
                IndiceLinea = 7; //"uwtGrupos__ctl0x_2538_2538_49267_fsdsentry_7_49270__t" Esto es un desglose NO popup
            } else {
                IndiceLinea = 2; //"ucDesglose_fsdsentry_6_49270__t"
            }
            iLineaSel = a[IndiceLinea];
            ///////////////////////////////////////////////////////

            re = /ucDesglose/
            if (oEdit.fsEntry.id.search(re) >= 0) {
                bPopUp = 1;
            } else {
                bPopUp = 0;
            }
        } else {
            bEsCampo = true;

            lCampoPadre = 0;
            iLineaSel = 0;
            bPopUp = 0;
        }       
        //2do recorrer y cargar lo necesario
        for (arrInput in arrInputs) {
            var o = fsGeneralEntry_getById(arrInputs[arrInput])
            if (o) {
                if ((o.IdAtrib !== '') && (o.IdAtrib > 0) && (o.IdAtrib !== idAtributoListaExterna)) {
                    //"uwtGrupos__ctl0x_2538_2538_49267_fsdsentry_7_49272"
                    //      Tendras atributos de campos y atributos de la linea.
                    //"ucDesglose_fsdsentry_4_49271"
                    //      Tendras atributos de la linea. Pq es desglose POPUP
                    a = o.id.split("_");
                    if ((o.idCampoPadre == null) || (!bEsCampo && (o.idCampoPadre == lCampoPadre) && (a[IndiceLinea] == iLineaSel))) {

                        if (o.intro == 2) {
                            vValor = o.getValue() //Otro lista externa. Especial: con getDataValue no sacas su valor.
                        } else {
                            vValor = o.getDataValue()
                        }

                        if (vValor == null) { //nulos, se quitan. 
                            vValor = '';
                        }

                        if ((o.tipo == 3) && (vValor !== '')) { //Fecha lleva hora, se quita
                            a = vValor.split("-");
                            vValor = a[0] + '-' + a[1] + '-' + a[2];
                        }

                        if (atributos == '')
                            atributos = o.IdAtrib + '@########@' + vValor
                        else {
                            atributos = atributos + "##########" + o.IdAtrib + '@########@' + vValor
                        }
                    }
                }
                
                if (o.tipoGS == TIPOCAMPOGS.PROVEEDOR) {
                    CodProveedor = o.getDataValue();
                    yaProve = true;
                }
                
            }
        }
        //3ro si era desglose popup. No tienes aun los atributos de campos.
        if (bPopUp) {
            var p = window.opener;
            for (arrInput in p.arrInputs) {
                var o = p.fsGeneralEntry_getById(p.arrInputs[arrInput]);
                if (o) {
                    if ((o.IdAtrib !== '') && (o.IdAtrib > 0) && (o.IdAtrib !== idAtributoListaExterna)) {
                        if (o.idCampoPadre == null) {
                            //Tendras atributos de campos
                            if (o.intro == 2) {
                                vValor = o.getValue()
                            } else {
                                vValor = o.getDataValue()
                            }

                            if (vValor == null) { //nulos, se quitan. 
                                vValor = '';
                            }

                            if ((o.tipo == 3) && (vValor !== '')) { //Fecha lleva hora, se quita
                                a = vValor.split("-");
                                vValor = a[0] + '-' + a[1] + '-' + a[2];
                            }

                            if (atributos == '')
                                atributos = o.IdAtrib + '@########@' + vValor
                            else {
                                atributos = atributos + "##########" + o.IdAtrib + '@########@' + vValor
                            }
                        }
                    }

                    if (!yaProve && o.tipoGS == TIPOCAMPOGS.PROVEEDOR) {
                        CodProveedor = o.getDataValue();
                        yaProve = true;
                    }
                }
            }
        }
    }
    
    if (sOrgCompras != "" || sUON != "") {
        OrgCompras = sOrgCompras;
        UON = sUON;
        return true;
    }
    else {
        if (sMensajeFaltaDatos != null) alert(sMensajeFaltaDatos);
        return false;
    }
};
function ObtenerOrgCompras(oEdit) {
    var sCodOrgCompras = "";
    var oOrgCompras;

    if (oEdit.fsEntry.idDataEntryDependent) {
        oOrgCompras = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent);
        if (oOrgCompras) {
            sCodOrgCompras = oOrgCompras.getDataValue();
        } else if ((oEdit.fsEntry.OrgComprasDependent) && (oEdit.fsEntry.OrgComprasDependent.value)) {
            oOrgCompras = fsGeneralEntry_getById(oEdit.fsEntry.OrgComprasDependent.value);
            sCodOrgCompras = oEdit.fsEntry.OrgComprasDependent.value;
        };
    } else {
        if ((oEdit.fsEntry.OrgComprasDependent) && (oEdit.fsEntry.OrgComprasDependent.value)) {
            oOrgCompras = fsGeneralEntry_getById(oEdit.fsEntry.OrgComprasDependent.value);
            sCodOrgCompras = oEdit.fsEntry.OrgComprasDependent.value;
        } else {
            p = window.opener;
            if (p) {
                if (p.sDataEntryOrgComprasFORM) { // Si antes que un desglose de pop-up hay un dataEntry ORGANIZACION COMPRAS
                    oOrgCompras = p.fsGeneralEntry_getById(p.sDataEntryOrgComprasFORM);
                    if (oOrgCompras) {
                        sCodOrgCompras = oOrgCompras.getDataValue();
                    };
                } else {
                    if (p.sCodOrgComprasFORM) {
                        sCodOrgCompras = p.sCodOrgComprasFORM;
                    };
                };
            };
        };
    };

    var yaOrgCompras = 0;
    if (sCodOrgCompras == "") {
        //Por si te han puesto la org en otro grupo                          
        for (arrInput in arrInputs) {
            var o = fsGeneralEntry_getById(arrInputs[arrInput])
            if (o) {
                if (!oOrgCompras) {
                    //Solo si no lo has obtenido a traves de oEdit
                    if ((o.tipoGS == 123) && (yaOrgCompras == 0)) {
                        sCodOrgCompras = o.getDataValue();
                        yaOrgCompras = 1;
                    }
                }

                //Si todo ya cargado 
                if (yaOrgCompras == 1) break;
            };
        };
    };

    if (yaOrgCompras == 0) {
        //Si es pedido negociado/expres
        //Si es popup el arrInputs no contiene org compras 
        if ((oEdit.fsEntry.TipoSolicit == 9) || (oEdit.fsEntry.TipoSolicit == 8)) {
            p = window.opener;

            if (oEdit.fsEntry.TipoSolicit == 8) {
                //Solo pedidos Negociados admite lo de meter la org compras en otro grupo.... En el resto esta como antes, lo q NO este en tu mismo grupo NO existe
                yaOrgCompras = 1;
            }

            if (p) {
                for (arrInput in p.arrInputs) {
                    var o = p.fsGeneralEntry_getById(p.arrInputs[arrInput])
                    if (o) {
                        //Por si te han puesto la org y centro en otro grupo
                        if ((o.tipoGS == 123) && (yaOrgCompras == 0)) {
                            sCodOrgCompras = o.getDataValue();
                            yaOrgCompras = 1;
                        };

                        //Si todo ya cargado
                        if (yaOrgCompras == 1) break;
                    }
                }
            }
        }
    };

    if (sCodOrgCompras == null) { sCodOrgCompras = ""; }
    return sCodOrgCompras;
};
function ObtenerUONCentroSM(oEdit) {
    //Se obtienen las UONs del centro SM

    var sCentroCoste = "";
    var oCentroCoste;

    if (oEdit.fsEntry.idDataEntryDependent2) {
        oCentroCoste = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent2);
        if (oCentroCoste) {
            sCentroCoste = oCentroCoste.getDataValue();
        }
    }

    var yaCentroCoste = 0;
    if (sCentroCoste == "") {
        for (arrInput in arrInputs) {
            var o = fsGeneralEntry_getById(arrInputs[arrInput])
            if (o) {
                //Por si te han puesto el Centro de Coste en otro grupo                
                if (!oCentroCoste) {
                    //Por si te han puesto el centro de coste en otro grupo
                    if ((o.tipoGS == 129) && (yaCentroCoste == 0)) {
                        sCentroCoste = o.getDataValue()
                        if (sCentroCoste == null) sCentroCoste = "";
                        yaCentroCoste = 1;
                    };
                };

                //Si todo ya cargado 
                if (yaCentroCoste == 1) break;
            };
        };
    };
    return sCentroCoste;
};
function ObtenerUON(oEdit) {
    var sCodUnidadOrganizativa = "";
    var oUnidadOrganizativa;

    if (oEdit.fsEntry.idDataEntryDependent3) {
        oUnidadOrganizativa = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent3);
        if (oUnidadOrganizativa) {
            sCodUnidadOrganizativa = oUnidadOrganizativa.getDataValue();
        } else {
            if ((oEdit.fsEntry.UnidadOrganizativa) && (oEdit.fsEntry.UnidadOrganizativaDependent.value)) {
                oUnidadOrganizativa = fsGeneralEntry_getById(oEdit.fsEntry.UnidadOrganizativaDependent.value);
                sCodUnidadOrganizativa = oEdit.fsEntry.UnidadOrganizativaDependent.value;
            };
        };
    } else {
        if ((oEdit.fsEntry.UnidadOrganizativaDependent) && (oEdit.fsEntry.UnidadOrganizativaDependent.value)) {
            oUnidadOrganizativa = fsGeneralEntry_getById(oEdit.fsEntry.UnidadOrganizativaDependent.value)
            sCodUnidadOrganizativa = oEdit.fsEntry.UnidadOrganizativaDependent.value
        } else {
            p = window.opener;
            if (p) {
                if (p.sDataEntryUnidadOrganizativaFORM) { // Si antes que un desglose de pop-up hay un dataEntry CENTRO
                    oUnidadOrganizativa = p.fsGeneralEntry_getById(p.sDataEntryUnidadOrganizativaFORM);
                    if (oUnidadOrganizativa) {
                        sCodUnidadOrganizativa = oUnidadOrganizativa.getDataValue();
                    };
                } else {
                    if (p.sCodUnidadOrganizativaFORM) {
                        sCodUnidadOrganizativa = p.sCodUnidadOrganizativaFORM;
                    }
                };
            }
        }
    };

    var yaUnidadOrganizativa = 0;
    if (sCodUnidadOrganizativa == "" || sCodUnidadOrganizativa == null) {
        for (arrInput in arrInputs) {
            var o = fsGeneralEntry_getById(arrInputs[arrInput])
            if (o) {
                //Por si te han puesto la UON en otro grupo                
                if (!oUnidadOrganizativa) {
                    //Por si te han puesto la uon en otro grupo
                    if ((o.tipoGS == 121) && (yaUnidadOrganizativa == 0)) {
                        sCodUnidadOrganizativa = o.getDataValue()
                        if (sCodUnidadOrganizativa == null) sCodUnidadOrganizativa = "";
                        yaUnidadOrganizativa = 1;
                    };
                };

                //Si todo ya cargado 
                if (yaUnidadOrganizativa == 1) break;
            };
        };
    };

    if (sCodUnidadOrganizativa == null) sCodUnidadOrganizativa = "";
    sCodUnidadOrganizativa = sCodUnidadOrganizativa.replace(/ - /g, "#");
    var index = sCodUnidadOrganizativa.lastIndexOf("#");
    sCodUnidadOrganizativa = sCodUnidadOrganizativa.substr(0, index);

    return sCodUnidadOrganizativa;
};
/*  Revisado por: ilg. Fecha: 19/02/2016
''' <summary>
''' A�ade una fila a un desglose para cada una cargada desde una excel. 
''' Para Qa, el estado interno de la fila nueva es 1 (sin revisar) y
''' deben ver los botones de aceptar y rechazar. Si la columna es readonly no se crea dataentry solo una label
''  con un boton tres puntos si tiene texto, si no tiene texto no debe ir el bot�n. 
''' </summary>
''' <param name="sRoot">nombre entry del desglose/param>
''' <param name="idCampo">id de bbdd del desglose</param>        
''' <param name="EsFavorita">Si se esta con favoritos o no</param>
''' <remarks>Llamada desde:javascript introducido para responder al onclick de todo boton 'a�adir fila' de desgloses no vinculados
''' ; Tiempo m�ximo:0</remarks>*/
var agregandoDesdeExcel = false;
function anyadirFilaDesgloseConDatos(sRoot, idCampo, EsFavorita, valoresLineaDesglose, MensajePorMostrar) {
    if (document.getElementById("bMensajePorMostrar") && document.getElementById("bMensajePorMostrar").value == "1" && MensajePorMostrar) {
        document.getElementById("bMensajePorMostrar").value = "0";
        return;
    };
    var bAlta = false;
    p = window.opener;
    if (p)
        if (p.document.forms["frmAlta"] != undefined) bAlta = true;
        else
            if (document.forms["frmAlta"] != undefined) bAlta = true;
    var re;
    var s;
    //uwtGrupos__ctl0__ctl0
    var lIndex;
    if (document.getElementById(sRoot + "_numRows")) {
        lIndex = parseFloat(document.getElementById(sRoot + "_numRows").value) + 1;
        document.getElementById(sRoot + "_numRows").value = lIndex;
    } else {
        lIndex = document.getElementById(sRoot + "_tblDesglose").rows.length - 1
        s = "<input type=hidden name=" + sRoot + "_numRows id=" + sRoot + "_numRows value=" + lIndex.toString() + ">";
        document.getElementById("divAlta").insertAdjacentHTML("beforeEnd", s);
    };
    var lLineaGrid;
    if (document.getElementById(sRoot + "_numRowsGrid")) {
        lLineaGrid = parseFloat(document.getElementById(sRoot + "_numRowsGrid").value) + 1;
        document.getElementById(sRoot + "_numRowsGrid").value = lLineaGrid;
        if ((!bAlta) || (bAlta && p)) lLineaGrid = lIndex;
    } else {
        lLineaGrid = document.getElementById(sRoot + "_tblDesglose").rows.length - 1;
        s = "<input type=hidden name=" + sRoot + "_numRowsGrid id=" + sRoot + "_numRowsGrid value=" + lLineaGrid.toString() + ">";
        document.getElementById("divAlta").insertAdjacentHTML("beforeEnd", s);
    };

    var Salta = 0;
    if (window.parent && window.parent.location.toString().search("altacertificado.aspx") != -1) Salta = 1;
    if (window.opener) {
        if (Salta == 0) {
            var oFrm = window.opener.document.forms["frmDesglose"];
            if (oFrm) {
                if (document.getElementById("INPUTDESGLOSE")) {
                    var oRowVinc = window.opener.document.getElementById(document.getElementById("INPUTDESGLOSE").value + "__numRowsVinc");
                    if (oRowVinc) oRowVinc.value = parseFloat(oRowVinc.value) + 1;
                    oRowVinc = null;
                };
            };
            oFrm = null;
        };
    };

    //Controlar las filas que se a�aden para optimizar MontarFormularioSubmit
    var ultimaFila = 0;
    for (var indice in htControlFilas) {
        if (String(idCampo) == indice.substring(0, indice.indexOf("_"))) {
            if (parseInt(ultimaFila) < parseInt(htControlFilas[indice])) ultimaFila = htControlFilas[indice];
        };
    };
    htControlFilas[String(idCampo) + "_" + String(lIndex)] = String(parseInt(ultimaFila) + 1);
    htControlFilasVinc[String(idCampo) + "_" + String(lIndex)] = String(0);
    ultimaFila = null;

    var sMovible;
    sMovible = String(idCampo) + "_" + String(lIndex);
    htControlArrVincMovible[sMovible] = "##No##";

    //A continuacion se obtiene lPrimeraLinea, que es la fila de donde se copian los datos
    var lPrimeraLinea;
    if (lLineaGrid == 1) {
        lPrimeraLinea = lIndex;
        var js = "<input type=hidden name=" + sRoot + "_numPrimeraLinea id=" + sRoot + "_numPrimeraLinea value=" + lIndex.toString() + ">";
        document.forms[0].insertAdjacentHTML("beforeEnd", js);
        js = null;
    } else {
        if (document.getElementById(sRoot + "_numPrimeraLinea")) lPrimeraLinea = document.getElementById(sRoot + "_numPrimeraLinea").value;
        else lPrimeraLinea = 1;
    };

    lPrimeraLinea = parseInt(lPrimeraLinea.toString(), 10);
    //En este array se guardan en cada fila el nuevo data entry y del que se va acoger el valor (_1_)
    var arraynuevafila = new Array();
    var arrayEntrysReadOnlyConDefecto = new Array();
    var insertarnuevoelemento = 0;
    var VecesEntre = 0;
    var filaOculta = document.getElementById(sRoot + "_tblDesgloseHidden");
    for (var i = 0, len = filaOculta.rows[0].cells.length; i < len; i++) {
        arraynuevafila[i] = new Array();
        arrayEntrysReadOnlyConDefecto[i] = "";
        arraynuevafila[i][0] = "";
        arraynuevafila[i][1] = "";
    };
    var oTR = document.getElementById(sRoot + "_tblDesglose").insertRow(-1);
    var iniFor;
    var tiemposFor = [];
    iniFor = new Date();
    for (var i = 0, len = filaOculta.rows[0].cells.length; i < len; i++) {
        s = filaOculta.rows[0].cells[i].innerHTML;
        var oTD = oTR.insertCell(-1);
        if (document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells[i].id)
            oTD.id = document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells[i].id + '_' + lIndex;

        var entryActual;
        if ($(s).attr('id').indexOf('__tbl') > 0) entryActual = $(s).attr('id').replace('__tbl', '');
        else entryActual = '';
        ///////////////////
        if ($('.trasparent', s).length > 0) {
            var den = '';
            var valor = '';
            var inicio = s.search(sRoot + "_fsentry");
            if (inicio > 1) {
                var cadena = s.substr(inicio + sRoot.length + 8);
                var fin = cadena.indexOf("_");
                cadena = cadena.substring(0, fin);
                if (document.getElementById(sRoot + "_fsdsentry_" + lPrimeraLinea + "_" + cadena)) {
                    etiqueta = document.getElementById(sRoot + "_fsdsentry_" + lPrimeraLinea + "_" + cadena).innerHTML;
                    inicio = etiqueta.indexOf("<SPAN>") + 6;
                    if (inicio < 6) inicio = etiqueta.indexOf("<span>") + 6;
                    if (inicio < 6) { //no existe <SPAN> sino <SPAN class="TipoArchivo">
                        inicio = etiqueta.indexOf("<SPAN");
                        fin = etiqueta.indexOf(">", inicio);
                        inicio = inicio + (fin - inicio) + 1; //etiqueta.indexOf("<SPAN ") + 24
                    };
                    fin = etiqueta.indexOf("</SPAN>");
                    if (fin < 0) fin = etiqueta.indexOf("</span>");
                    den = etiqueta.substring(inicio, fin);
                    etiqueta = null;
                    if (den == '&nbsp;') {
                        den = '';
                        s = s.replace("<IMG src='" + rutaPM + "_common/images/trespuntos.gif' align=middle border=0 unselectable=\"on\">", "");
                        s = s.replace("BACKGROUND-IMAGE: url(" + rutaPM + "_common/images/trespuntos.gif);", "display: none;");
                    };
                }
                if (document.getElementById(sRoot + "_" + lPrimeraLinea + "_" + cadena)) valor = document.getElementById(sRoot + "_" + lPrimeraLinea + "_" + cadena).value;
            };
            if (valor != '') {
                if (s.search("__h type=hidden") >= 0) s = s.replace("__h type=hidden", "__h type='hidden' value='" + valor + "'");
                if (s.search("__hAct type=hidden") >= 0) s = s.replace("__hAct type=hidden", "__hAct type='hidden' value='" + valor + "'"); //para tipo archivo                                        
                if (s.search("__hNew type=hidden") >= 0) s = s.replace("__hNew type=hidden", "__hNew type='hidden'");//para tipo archivo                                         
                if (s.search("__h\" type=\"hidden\"") >= 0) s = s.replace("__h\" type=\"hidden\"", "__h\" type=\"hidden\" value='" + valor + "'");
            };
            if (den != '') {
                if (s.search("</TEXTAREA") >= 0) s = s.replace("</TEXTAREA", den + "</TEXTAREA");
                else s = s.replace("readOnly", "readOnly value='" + den + "'");
                s = s.replace(/type=hidden/g, "type='hidden' value='" + den + "'");
                s = s.replace(/\[\"\"/g, '[\"' + den + '\"');
            };
        };
        den = null;
        valor = null;
        inicio = null;
        cadena = null;
        ///////////////////	 
        s = s.replace(/fsentry/g, "fsdsentry_" + lIndex.toString() + "_");
        //Para evitar generar los contenidos de los webdropdown con los mismos IDs y a fin de que podamos localizar esos datos con m�s facilidad, vamos a editar los IDs
        var webdropdownReplace = '';
        if ($('[data-ig$=\\.0\\:mkr\\:Target]', $(s)).length > 0) webdropdownReplace = $('[data-ig$=\\.0\\:mkr\\:Target]', $(s)).attr('data-ig').replace('x:', '').replace('.0:mkr:Target', '');
        if (webdropdownReplace != '') {
            var numAleatorio = Aleatorio(1, parseInt(webdropdownReplace));
            re = new RegExp(); // empty constructor
            re.compile(webdropdownReplace, 'gi');
            s = s.replace(re, parseInt(webdropdownReplace) + parseInt(numAleatorio));
            //Reemplazar la clase seleccionada para que no se seleccione nada por defecto al copiar l�nea vac�a
            re.compile('igdd_FullstepPMWebListItemSelected', 'gi');
            s = s.replace(re, parseInt(webdropdownReplace) + parseInt(numAleatorio));
            numAleatorio = null;
        };
        webdropdownReplace = null;
        var ni = /##newIndex##/g;
        if ($('style', s).length > 0) {
            var styleTag = $(s);
            styleTag.find('style').remove();
            s = styleTag.wrap("<div>").parent().html();
        };
        if ($('script', s).length > 0) {
            var scriptsControl = $('script', s);
            var scriptTag = $(s);
            scriptTag.find('script').remove();
            s = scriptTag.wrap("<div>").parent().html();

            if (s.search(ni) > 0) oTD.align = "center";
            s = s.replace(/##newIndex##/g, lIndex.toString());

            oTD.insertAdjacentHTML("beforeEnd", s);
            var numScripts = scriptsControl.length;
            for (var k = 0; k < numScripts; k++) { eval(scriptsControl[k].innerText); };
        } else {
            if (s.search(ni) > 0) oTD.align = "center";

            ni = "_cmdRechazarAccion";
            if (s.search(ni) > 0) s = s.replace("_cmdRechazarAccion", "_cmdRechazarAccion_" + lIndex.toString());

            ni = "_cmdAprobarAccion";
            if (s.search(ni) > 0) s = s.replace("_cmdAprobarAccion", "_cmdAprobarAccion_" + lIndex.toString());

            s = s.replace(/##newIndex##/g, lIndex.toString());
            re = /'##Si##'/
            if (s.search(re) > 0) htControlArrVincMovible[sMovible] = "##Nueva##";
            oTD.insertAdjacentHTML("beforeEnd", s);
        };
        iComienzo = null;
        iFin = null;
        arrJS = null;
        ni = null;

        var oContr = fsGeneralEntry_getById(entryActual);
        var campo;
        var a = new Array();
        a = entryActual.split("_");
        //Si es un desglose que esta en la propia pagina          

        if (a.length >= 6) {
            if (oContr.tipoGS == TIPOCAMPOGS.DENARTICULO) entryActual = a[0] + '__' + a[2] + '_' + a[3] + '_' + a[4] + '_' + a[5] + '_' + a[6] + '__tden';
            else entryActual = a[0] + '__' + a[2] + '_' + a[3] + '_' + a[4] + '_' + a[5] + '_' + a[6] + '__t';

            if (entryActual.search('fsentry') >= 0) entryActual = entryActual.replace(/fsentry/g, "fsdsentry_" + lIndex.toString() + "_");
            campo = a[6];
            campo = campo.replace('fsentry', '');
            if (oContr) {
                if (oContr.RecordarValores == true) {
                    if (oContr.campo_origen != 0) campo = oContr.campo_origen;
                    Sys.Application.add_init(function () {
                        $create(Sys.Extended.UI.AutoCompleteBehavior, {
                            "completionInterval": 500,
                            "completionListCssClass": "autoCompleteList",
                            "completionListItemCssClass": "autoCompleteListItem",
                            "contextKey": campo,
                            "delimiterCharacters": "",
                            "enableCaching": false,
                            "highlightedItemCssClass": "autoCompleteSelectedListItem",
                            "id": "autocompleteextender_" + lIndex.toString() + "_" + campo.toString(),
                            "minimumPrefixLength": 0,
                            "serviceMethod": "DevolverValores",
                            "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                            "useContextKey": true
                        }, {
                                "shown": resetPosition
                            }, null, $get(entryActual));
                    });
                }
                else if (oContr.tipoGS == TIPOCAMPOGS.DENARTICULO || oContr.tipoGS == TIPOCAMPOGS.NUEVOCODARTICULO) {
                    if (oContr.tipoGS == TIPOCAMPOGS.NUEVOCODARTICULO) {    //C�digo art�culo
                        Sys.Application.add_init(function () {
                            $create(Sys.Extended.UI.AutoCompleteBehavior, {
                                "completionInterval": 500,
                                "completionListCssClass": "autoCompleteList",
                                "completionListItemCssClass": "autoCompleteListItem",
                                "enabled": true,
                                "minimumPrefixLength": 3,
                                "delimiterCharacters": "",
                                "enableCaching": false,
                                "completionListHighlightedItemCssClass": "autoCompleteSelectedListItem",
                                "id": entryActual + "_ext",
                                "serviceMethod": "DevolverArticulos",
                                "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                                "useContextKey": true,
                                "completionSetCount": oContr.FilasAutoCompleteExtender,
                                "targetControlID": entryActual + "_t"
                            }, {
                                    "populated": onCodListPopulated,
                                    "shown": resetPosition
                                }, null, $get(entryActual + "_t"));
                        });
                    }
                    if (oContr.tipoGS == TIPOCAMPOGS.DENARTICULO) { //Denominaci�n art�culo
                        Sys.Application.add_init(function () {
                            $create(Sys.Extended.UI.AutoCompleteBehavior, {
                                "completionInterval": 500,
                                "completionListCssClass": "autoCompleteList",
                                "completionListItemCssClass": "autoCompleteListItem",
                                "enabled": true,
                                "minimumPrefixLength": 3,
                                "delimiterCharacters": "",
                                "enableCaching": false,
                                "completionListHighlightedItemCssClass": "autoCompleteSelectedListItem",
                                "id": entryActual + "_ext",
                                "serviceMethod": "DevolverArticulos",
                                "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                                "useContextKey": true,
                                "completionSetCount": oContr.FilasAutoCompleteExtender,
                                "targetControlID": entryActual + "_t"
                            }, {
                                    "itemSelected": onDenExtenderItemSelected,
                                    "populated": onCodListPopulated,
                                    "shown": resetPosition
                                }, null, $get(entryActual + "_t"));
                        });
                    }
                } else if (oContr.tipoGS == 0 && oContr.intro == 2) {    //Atributos de lista externa                    
                    Sys.Application.add_init(function () {
                        $create(Sys.Extended.UI.AutoCompleteBehavior, {
                            "completionInterval": 500,
                            "completionListCssClass": "autoCompleteList",
                            "completionListItemCssClass": "autoCompleteListItem",
                            "delimiterCharacters": "",
                            "enableCaching": false,
                            "highlightedItemCssClass": "autoCompleteSelectedListItem",
                            "id": entryActual + "_ext",
                            "minimumPrefixLength": 2,
                            "serviceMethod": "Obtener_Datos_ListaExterna_Extender",
                            "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                            "useContextKey": true,
                            "completionSetCount": oContr.FilasAutoCompleteExtender
                        }, {
                                "itemSelected": onListaExternaExtenderItemSelected,
                                "populated": onCodListPopulated,
                                "shown": resetPosition
                            }, null, $get(entryActual + "_t"));
                    });
                };
            };
        };
        //Si es un desglose que se abre en PopUp
        if (a[0] == "ucDesglose") {
            if (oContr.tipoGS == 118) entryActual = a[0] + '_' + a[1] + '__tden';
            else entryActual = a[0] + '_' + a[1] + '__t';

            entryActual = entryActual.replace(/fsentry/g, "fsdsentry_" + lIndex.toString() + "_");
            campo = a[1];
            campo = campo.replace('fsentry', '');
            if (oContr) {
                if (oContr.RecordarValores == true) {
                    if (oContr.campo_origen != 0) campo = oContr.campo_origen;

                    Sys.Application.add_init(function () {
                        $create(Sys.Extended.UI.AutoCompleteBehavior, {
                            "completionInterval": 500,
                            "completionListCssClass": "autoCompleteList",
                            "completionListItemCssClass": "autoCompleteListItem",
                            "contextKey": campo,
                            "delimiterCharacters": "",
                            "enableCaching": false,
                            "highlightedItemCssClass": "autoCompleteSelectedListItem",
                            "id": "autocompleteextender_" + lIndex.toString() + "_" + campo.toString(),
                            "minimumPrefixLength": 0,
                            "serviceMethod": "DevolverValores",
                            "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                            "useContextKey": true
                        }, {
                                "shown": resetPosition
                            }, null, $get(entryActual));
                    });
                }
                else if (oContr.tipoGS == TIPOCAMPOGS.DENARTICULO || oContr.tipoGS == TIPOCAMPOGS.NUEVOCODARTICULO) {
                    if (oContr.tipoGS == TIPOCAMPOGS.NUEVOCODARTICULO) {    //C�digo art�culo
                        Sys.Application.add_init(function () {
                            $create(Sys.Extended.UI.AutoCompleteBehavior, {
                                "completionInterval": 500,
                                "completionListCssClass": "autoCompleteList",
                                "completionListItemCssClass": "autoCompleteListItem",
                                "enabled": true,
                                "minimumPrefixLength": 3,
                                "delimiterCharacters": "",
                                "enableCaching": false,
                                "completionListHighlightedItemCssClass": "autoCompleteSelectedListItem",
                                "id": entryActual + "_ext",
                                "serviceMethod": "DevolverArticulos",
                                "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                                "useContextKey": true,
                                "completionSetCount": oContr.FilasAutoCompleteExtender,
                                "targetControlID": entryActual + "_t"
                            }, {
                                    "populated": onCodListPopulated,
                                    "shown": resetPosition
                                }, null, $get(entryActual + "_t"));
                        });
                    }
                    if (oContr.tipoGS == TIPOCAMPOGS.DENARTICULO) { //Denominaci�n art�culo
                        Sys.Application.add_init(function () {
                            $create(Sys.Extended.UI.AutoCompleteBehavior, {
                                "completionInterval": 500,
                                "completionListCssClass": "autoCompleteList",
                                "completionListItemCssClass": "autoCompleteListItem",
                                "enabled": true,
                                "minimumPrefixLength": 3,
                                "delimiterCharacters": "",
                                "enableCaching": false,
                                "completionListHighlightedItemCssClass": "autoCompleteSelectedListItem",
                                "id": entryActual + "_ext",
                                "serviceMethod": "DevolverArticulos",
                                "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                                "useContextKey": true,
                                "completionSetCount": oContr.FilasAutoCompleteExtender,
                                "targetControlID": entryActual + "_t"
                            }, {
                                    "itemSelected": onDenExtenderItemSelected,
                                    "populated": onCodListPopulated,
                                    "shown": resetPosition
                                }, null, $get(entryActual + "_t"));
                        });
                    }
                } else if (oContr.tipoGS == 0 && oContr.intro == 2) {    //Atributos de lista externa                    
                    Sys.Application.add_init(function () {
                        $create(Sys.Extended.UI.AutoCompleteBehavior, {
                            "completionInterval": 500,
                            "completionListCssClass": "autoCompleteList",
                            "completionListItemCssClass": "autoCompleteListItem",
                            "delimiterCharacters": "",
                            "enableCaching": false,
                            "highlightedItemCssClass": "autoCompleteSelectedListItem",
                            "id": entryActual + "_ext",
                            "minimumPrefixLength": 2,
                            "serviceMethod": "Obtener_Datos_ListaExterna_Extender",
                            "servicePath": rutaPM + "AutoCompletePMWEB.asmx",
                            "useContextKey": true,
                            "completionSetCount": oContr.FilasAutoCompleteExtender
                        }, {
                                "itemSelected": onListaExternaExtenderItemSelected,
                                "populated": onCodListPopulated,
                                "shown": resetPosition
                            }, null, $get(entryActual + "_t"));
                    });
                };
            };
        };

        campo = null;
        //Aqui es donde se introducen en el array los controles	
        stemp = s;
        var mientry;
        if (stemp.indexOf('__tbl') > 0) mientry = $(stemp).attr('id').replace('__tbl', '');
        else mientry = '';
        a = new Array();
        var entry1;
        var entryControl;
        var numero;
        a = mientry.split("_");
        if ((a.length == 7) || (a.length == 4) || (a.length == 9)) {
            entry1 = mientry;
            entryControl = mientry;
            if (a.length == 7) numero = a[5]; //cuando no es popup               
            else {
                if (a.length == 4) numero = a[2]; //Cuando es popup
                else if (a.length == 9) numero = a[7]; //Cuando el desglose no es una ventana emergente                
            };
            numero = parseInt(numero.toString(), 10);
            var cadenaareemplazar = "_fsdsentry_" + numero.toString() + "_";
            entry1 = entry1.replace(cadenaareemplazar, "_fsdsentry_" + lPrimeraLinea.toString() + "_");
            if (numero.toString() == lPrimeraLinea.toString()) entry1 = entry1.replace("fsdsentry_" + numero.toString() + "_", "fsentry");
            arraynuevafila[insertarnuevoelemento][0] = mientry;
            arraynuevafila[insertarnuevoelemento][1] = entry1;
            oContr = fsGeneralEntry_getById(arraynuevafila[insertarnuevoelemento][1]);
            while (!oContr && VecesEntre == 0 && numero > lPrimeraLinea) {
                lPrimeraLinea = lPrimeraLinea + 1;
                entry1 = entryControl.replace(cadenaareemplazar, "_fsdsentry_" + lPrimeraLinea.toString() + "_");
                arraynuevafila[insertarnuevoelemento][1] = entry1;
                oContr = fsGeneralEntry_getById(arraynuevafila[insertarnuevoelemento][1]);
            };
            cadenaareemplazar = null;
            var entryACopiar = entry1.replace("fsdsentry_" + lPrimeraLinea.toString() + "_", "fsentry");
            oContr = fsGeneralEntry_getById(entryACopiar);
            //Se agregan los dataentys que estan en ReadOnly y tienen valor por defecto para que en la asignacion de valores no se borren  
            if (oContr.PoseeDefecto == true) arrayEntrysReadOnlyConDefecto[insertarnuevoelemento] = entry1;
            else arrayEntrysReadOnlyConDefecto[insertarnuevoelemento] = "";
            if (oContr) {
                if (oContr.CampoOrigenVinculado == null) {
                    if ((oContr.tipoGS != 119) && (oContr.tipoGS != 118) && (oContr.tipoGS != 104) && (oContr.tipoGS != 127) && (oContr.tipoGS != 121) && (oContr.tipoGS != 116) && (oContr.intro == 1) && (oContr.readOnly == false)) {
                        var param;
                        if (a.length == 9) {
                            param = a[0] + '$_' + a[2] + '$' + a[3] + '$' + a[4] + '_' + a[5] + '$' + a[6] + '_' + a[7] + '_' + a[8] + '$_t';
                        } else {
                            if (a.length >= 6) param = a[0] + '$_' + a[2] + '$' + a[3] + '$' + a[4] + '_' + a[5] + '$' + a[6] + '$_t';
                        };
                        if (a[0] == "ucDesglose") param = a[0] + '$' + a[1] + '$_t';
                        var bClientBinding;
                        switch (oContr.tipoGS) {
                            case TIPOCAMPOGS.FORMAPAGO:
                            case TIPOCAMPOGS.MONEDA:
                            case TIPOCAMPOGS.UNIDAD:
                            case TIPOCAMPOGS.UNIDADPEDIDO:
                            case TIPOCAMPOGS.PAIS:
                            case TIPOCAMPOGS.PROVINCIA:
                            case TIPOCAMPOGS.DEST:
                            case TIPOCAMPOGS.DEPARTAMENTO:
                            case TIPOCAMPOGS.ORGANIZACIONCOMPRAS:
                            case TIPOCAMPOGS.CENTRO:
                            case TIPOCAMPOGS.ALMACEN:
                            case TIPOCAMPOGS.PROVEEDORERP:
                            case TIPOCAMPOGS.ANYOPARTIDA:
                                bClientBinding = true;
                                break;
                            default:
                                bClientBinding = false;
                                break;
                        };
                        if ((oContr.tipoGS == 0) && (oContr.tipo != SUBTIPO.NUMERO) && (oContr.tipo != SUBTIPO.FECHA) && (oContr.tipo != SUBTIPO.BOOLEAN)) bClientBinding = true;
                        var valores;
                        if (bClientBinding == false) {
                            var codigo = document.documentElement.innerHTML;
                            var posicion = codigo.search("Infragistics.Web.UI.WebDropDown");
                            if (posicion > 0) {
                                codigo = codigo.substring(posicion);
                                posicion = codigo.search(entryACopiar);
                                if (posicion > 0) {
                                    codigo = codigo.substring(posicion);
                                    posicion = codigo.search("{'0':");
                                    fin = codigo.search(']]}]');
                                    if ((posicion > 0) && (fin > 0)) valores = codigo.substring(posicion, fin + 3);
                                };
                            };
                            codigo = null;
                            posicion = null;
                        };
                        if (bClientBinding == true) {
                            Sys.Application.add_init(function () {
                                $create(Infragistics.Web.UI.WebDropDown, {
                                    "clientbindingprops": [
                                        [
                                            [, 1, 120, , , , , , , , 1, , 1, , 2, 0, , , 0, , 300, 120, , "", , , , 6, , 1, , , , , , , , , , 0, , , 1, 0, , , 0, , , , 1], {
                                                "c": {
                                                    "dropDownInputFocusClass": "igdd_FullstepPMWebValueDisplayFocus",
                                                    "dropDownButtonPressedImageUrl": "/ig_res/Fullstep/images/igdd_DropDownButtonPressed.png",
                                                    "_pi": "[\"/ig_res/Fullstep/images/ig_ajaxIndicator.gif\",,,\"ig_FullstepPMWebAjaxIndicator\",,1,,,,\"ig_FullstepPMWebAjaxIndicatorBlock\",,3,,3,\"Async post\"]",
                                                    "pageCount": 0,
                                                    "vse": 0,
                                                    "textField": "Texto",
                                                    "controlAreaHoverClass": "ig_FullstepPMWebHover igdd_FullstepPMWebControlHover",
                                                    "controlAreaClass": "igdd_FullstepPMWebControlArea",
                                                    "valueField": "Valor",
                                                    "controlClass": "ig_FullstepPMWebControl igdd_FullstepPMWebControl",
                                                    "dropDownInputHoverClass": "ig_FullstepPMWebHover igdd_FullstepPMWebValueDisplayHover",
                                                    "dropDownItemDisabled": "igdd_FullstepPMWebListItemDisabled",
                                                    "uid": mientry + "_t",
                                                    "nullTextCssClass": "igdd_FullstepPMWebNullText",
                                                    "controlAreaFocusClass": "igdd_FullstepPMWebControlFocus",
                                                    "dropDownItemHover": "igdd_FullstepPMWebListItemHover",
                                                    "dropDownButtonNormalImageUrl": "/ig_res/Fullstep/images/igdd_DropDownButton.png",
                                                    "dropDownInputClass": "igdd_FullstepPMWebValueDisplay",
                                                    "controlDisabledClass": "ig_FullstepPMWebDisabled igdd_FullstepPMWebControlDisabled",
                                                    "dropDownButtonDisabledImageUrl": "/ig_res/Fullstep/images/igdd_DropDownButtonDisabled.png",
                                                    "dropDownItemClass": "igdd_FullstepPMWebListItem",
                                                    "pi": "[,,9,,1,1,,80,,,,3,,3,\"Async post\"]",
                                                    "dropDownItemSelected": "igdd_FullstepPMWebListItemSelected",
                                                    "ocs": 1,
                                                    "dropDownButtonClass": "igdd_FullstepPMWebDropDownButton",
                                                    "dropDownItemActiveClass": "igdd_FullstepPMWebListItemActive",
                                                    "dropDownButtonHoverImageUrl": "/ig_res/Fullstep/images/igdd_DropDownButtonHover.png",
                                                    "dropDownInputDisabledClass": "ig_FullstepPMWebDisabled igdd_FullstepPMWebValueDisplayDisabled"
                                                }
                                            }], , [{}],
                                        ["InputKeyDown:WebDropDown_KeyDown", "DropDownOpening:WebDropDown_DataCheck", "SelectionChanged:WebDropDown_SelectionChanging", "Blur:WebDropDown_Focus", "ValueChanged:WebDropDown_valueChanged"],
                                        []
                                    ],
                                    "id": mientry + "__t",
                                    "name": "_t"
                                }, null, null, $get(mientry + "__t"));
                            });
                        } else {
                            Sys.Application.add_init(function () {
                                $create(Infragistics.Web.UI.WebDropDown, {
                                    "id": mientry + "__t",
                                    "name": "_t",
                                    "props": [
                                        [
                                            [, 1, 160, , , , , -1, , , 1, , 1, , 2, 0, , , 0, , , 0, , "", , , , 6, , 1, , , , , , , , 0, , 0, , -1, 0, 0, , 0, 0, , ,], {
                                                "c": {
                                                    "controlDisabledClass": "ig_FullstepPMWebDisabled igdd_FullstepPMWebControlDisabled",
                                                    "controlAreaClass": "igdd_FullstepPMWebControlArea",
                                                    "controlAreaHoverClass": "ig_FullstepPMWebHover igdd_FullstepPMWebControlHover",
                                                    "vse": 0,
                                                    "dropDownInputHoverClass": "ig_FullstepPMWebHover igdd_FullstepPMWebValueDisplayHover",
                                                    "dropDownItemHover": "igdd_FullstepPMWebListItemHover",
                                                    "dropDownButtonNormalImageUrl": "/ig_res/Fullstep/images/igdd_DropDownButton.png",
                                                    "pi": "[]",
                                                    "dropDownButtonHoverImageUrl": "/ig_res/Fullstep/images/igdd_DropDownButtonHover.png",
                                                    "dropDownButtonPressedImageUrl": "/ig_res/Fullstep/images/igdd_DropDownButtonPressed.png",
                                                    "dropDownInputClass": "igdd_FullstepPMWebValueDisplay",
                                                    "dropDownItemClass": "igdd_FullstepPMWebListItem",
                                                    "dropDownInputDisabledClass": "ig_FullstepPMWebDisabled igdd_FullstepPMWebValueDisplayDisabled",
                                                    "controlClass": "ig_FullstepPMWebControl igdd_FullstepPMWebControl",
                                                    "dropDownInputFocusClass": "igdd_FullstepPMWebValueDisplayFocus",
                                                    "dropDownButtonDisabledImageUrl": "/ig_res/Fullstep/images/igdd_DropDownButtonDisabled.png",
                                                    "pageCount": 4,
                                                    "controlAreaFocusClass": "igdd_FullstepPMWebControlFocus",
                                                    "dropDownItemDisabled": "igdd_FullstepPMWebListItemDisabled",
                                                    "ocs": 1,
                                                    "nullTextCssClass": "igdd_FullstepPMWebNullText",
                                                    "dropDownButtonClass": "igdd_FullstepPMWebDropDownButton",
                                                    "uid": param,
                                                    "dropDownItemSelected": "igdd_FullstepPMWebListItemSelected",
                                                    "dropDownItemActiveClass": "igdd_FullstepPMWebListItemActive"
                                                }
                                            }], , eval("[" + valores + "]"), ["DropDownOpening:WebDropDown_DropDownOpening", "SelectionChanged:WebDropDown_SelectionChanging", "Blur:WebDropDown_Focus", "ValueChanged:WebDropDown_valueChanged"]
                                    ]
                                }, null, null, $get(mientry + "__t"));
                            });
                        };
                        Sys.Application.add_load(function () {
                            $find(mientry + "__t").behavior.set_enableMovingTargetWithSource(false)
                        });
                        param = null;
                        valores = null;
                    };
                };
            };
            entryACopiar = null;
            //las no conf grabadas tienen cols 1020 y 1010 que me obligan
            VecesEntre = 1;
            insertarnuevoelemento = insertarnuevoelemento + 1
        };
        oContr = null;
        a = null;
        mientry = null;
        entry1 = null;
        entryControl = null;
        numero = null;
        tiemposFor.push((new Date() - iniFor) / 1000);
    };
    //alert(tiemposFor.join(' - '));
    s = "<input type=hidden name=" + sRoot + "_" + lIndex + "_Deleted id=" + sRoot + "_" + lIndex + "_Deleted value='no'>";
    oTD.insertAdjacentHTML("beforeEnd", s);
    oTD = null;
    entryActual = null;
    stemp = null;
    var iNumCeldas; //indica el n�mero de celdas de la tabla 
    var iCeldaIni = 1;

    iNumCeldas = document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells.length - 2; //si es una linea de desglose estan todas menos la �ltima que es el bot�n de eliminar
    for (i = 0; i < iNumCeldas; i++) {
        var s2 = eval("arrEntrys_" + idCampo + "[i]");
        if (!s2) continue;
        if (s2.search("arrCalculados") > 0) s2 = s2.replace(/fsentry/, "fsdsentry_" + lIndex.toString() + "_");
        if (s2.search("arrObligatorios") > 0) s2 = s2.replace(/fsentry/, "fsdsentry_" + lIndex.toString() + "_");
        if (s2.search("arrArticulos") > 0) {
            s2 = s2.replace(/fsentry/, "fsdsentry_" + lIndex.toString() + "_");
            if (!(s2.search("arrArticulos\[iNumArts][0]=\"\"") > 0)) s2 = s2.replace(/fsentry/, "fsdsentry_" + lIndex.toString() + "_");
        };
        if (s2.search("arrPres1") > 0) s2 = s2.replace(/fsentry/, "fsdsentry_" + lIndex.toString() + "_");
        if (s2.search("arrPres2") > 0) s2 = s2.replace(/fsentry/, "fsdsentry_" + lIndex.toString() + "_");
        if (s2.search("arrPres3") > 0) s2 = s2.replace(/fsentry/, "fsdsentry_" + lIndex.toString() + "_");
        if (s2.search("arrPres4") > 0) s2 = s2.replace(/fsentry/, "fsdsentry_" + lIndex.toString() + "_");
        s2 = s2.replace(/fsentry/, "fsdsentry_" + lIndex.toString() + "_");
        s2 = s2.replace(/fsentry/, "fsdsentry_" + lIndex.toString() + "_");
        if (s2.search(", 104,") > 0 || s2.search(",108,") > 0 || s2.search(", 114,") > 0) {
            s2 = s2.replace(/fsentry/, "fsdsentryx" + lIndex.toString() + "x"); //en el caso del art�culo,de la provincia y del contacto del proveedor, la grid del dropdown es �nica para cada fila
            s2 = s2.replace(/fsentry/, "fsdsentry_" + lIndex.toString() + "_"); //tambien es �nico el campo dependiente (dependentfield)
        };
        eval(s2);
        //------------------------------------------------------------------------------------------------------------			
        if (s2.search("arrObligatorios") > 0) {
            var oEntry = fsGeneralEntry_getById(arrObligatorios[arrObligatorios.length - 1]);
            oEntry.basedesglose = false;
        };
        s2 = null;
    };
    iNumCeldas = null;
    //Se hace la asignacion de valores para la nueva fila   --------------------------------------------------------
    var o, o1;
    var sTraerPrecio = 0
    for (i = 0; i < insertarnuevoelemento; i++) {
        sTraerPrecio = 0
        o = fsGeneralEntry_getById(arraynuevafila[i][0]);
        o1 = fsGeneralEntry_getById(arraynuevafila[i][1]);
        //  Columnas No Escritura � No Visible son labels no son entrys. fsGeneralEntry_getById de una label es null. null.PoseeDefecto es error         
        if (o) {
            o.nColumna = i + iCeldaIni;
            o.nLinea = lLineaGrid;
            if (o.errMsg) {
                o.errMsg = o.errMsg.replace('\n', '\\n');
                o.errMsg = o.errMsg.replace('\n', '\\n');
            };
            if (o1) {
                o.PoseeDefecto = o1.PoseeDefecto;
                o.TextoValorDefecto = o1.TextoValorDefecto;
                o.DataValorDefecto = o1.DataValorDefecto;
            };
            if ((o != null) && (o1 != null)) {
                if (o.intro == 2) {
                    if (typeof (valoresLineaDesglose[o.idCampo]) !== 'undefined') {
                        ObtenerDatosAtribListaExterna(o.Editor);
                        var listaExternaValue = o;
                        var param = JSON.stringify({ IdAtributoListaExterna: o.IdAtrib, codigoBuscado: valoresLineaDesglose[o.idCampo].Key, denominacionBuscado: "", sCodOrgCompras: OrgCompras, sUON: UON, atributos: atributos });
                        $.when($.ajax({
                            type: "POST",
                            url: rutaPM + 'AutoCompletePMWEB.asmx/Obtener_Datos_ListaExterna',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            data: param,
                            async: false
                        })).done(function (msg) {
                            var bEncontrado = false;
                            if (msg.d.length == 1) {
                                fsGeneralEntry_getById(listaExternaValue.Editor.fsEntry.id).setValue(msg.d[0].id + " - " + msg.d[0].name);
                                fsGeneralEntry_getById(listaExternaValue.Editor.fsEntry.id).setDataValue(msg.d[0].id);
                            }
                        });
                    }
                } else {
                    if (o.tipoGS == TIPOCAMPOGS.DENARTICULO) {
                        var idCodArt = calcularIdDataEntryCelda(o.id, o.nColumna - 1, o.nLinea, 0);
                        if (idCodArt) EntryCodArt = fsGeneralEntry_getById(idCodArt);

                        if ((EntryCodArt) && (EntryCodArt.tipoGS == TIPOCAMPOGS.NUEVOCODARTICULO)) {
                            if (!EntryCodArt.getValue() == '') {
                                var idCodMat = calcularIdDataEntryCelda(idCodArt, EntryCodArt.nColumna - 1, EntryCodArt.nLinea, 0);
                                if (idCodMat) EntryMat = fsGeneralEntry_getById(idCodMat);

                                //Obtener  el id del DataEntry del PRECIO si CARGAR_ULT_ADJ = 1
                                var idEntryPrec = "";
                                sMoneda = EntryCodArt.instanciaMoneda;
                                if (EntryCodArt.idEntryPREC != null) {
                                    idEntryPrec = EntryCodArt.idEntryPREC;
                                    oPrecio = fsGeneralEntry_getById(idEntryPrec);
                                    if ((oPrecio) && (oPrecio.cargarUltADJ == true)) {
                                        //Precio. De haber, q sea en moneda Instancia. Si no, lo q venga de bbdd (moneda central)
                                        var sMoneda = "";
                                        var oEntry = buscarCampoEnFormulario(102); //busca el campo de tipo moneda
                                        if (oEntry) {
                                            if (oEntry.getDataValue()) {
                                                sMoneda = oEntry.getDataValue();
                                                EntryCodArt.MonSolicit = sMoneda;
                                                EntryCodArt.instanciaMoneda = sMoneda;
                                            } else {
                                                //En teor�a no puede pasar este caso

                                                oEntry.setDataValue(EntryCodArt.instanciaMoneda)
                                                sMoneda = EntryCodArt.instanciaMoneda;
                                                EntryCodArt.MonSolicit = sMoneda;
                                            }
                                        } else {
                                            sMoneda = EntryCodArt.instanciaMoneda;
                                        }
                                    } else {
                                        idEntryPrec = "";
                                    }
                                }

                                var sTraerDen = 0
                                sTraerPrecio = 0
                                if (!idEntryPrec == "") sTraerPrecio = 1

                                if (o.readOnly && valoresLineaDesglose[o.idCampo] == undefined) {
                                    var params = { idArt: EntryCodArt.getValue(), fsEntryArt: idCodArt, fsEntryDen: o.id, fsEntryMat: idCodMat, fsEntryPrec: idEntryPrec, InstanciaMoneda: sMoneda };
                                    sTraerDen = 1;
                                } else {
                                    var params = { idArt: EntryCodArt.getValue(), fsEntryArt: idCodArt, fsEntryDen: "", fsEntryMat: idCodMat, fsEntryPrec: idEntryPrec, InstanciaMoneda: sMoneda };
                                }
                                if ((sTraerDen == 1) || (sTraerPrecio == 1)) {
                                    $.when($.ajax({
                                        type: "POST",
                                        url: rutaFS + 'PMWEB/_common/BuscadorArticuloserver.aspx/CargarDesdeExcel',
                                        contentType: "application/json; charset=utf-8",
                                        data: JSON.stringify(params),
                                        dataType: "json",
                                        async: false
                                    })).done(function (msg) {
                                        var respuesta = msg.d;

                                        resultDenArt = respuesta[0];
                                        resultPrecio = respuesta[2];

                                        resultGenerico = respuesta[1];
                                        if (resultGenerico == "false") {
                                            resultGenerico = false;
                                        } else {
                                            resultGenerico = true;
                                        }

                                        resultDenMat = respuesta[3];
                                        resultCodMat = respuesta[4];
                                        resultDenMatTip = respuesta[5];
                                    });
                                    EntryCodArt.articuloCodificado = true;
                                    EntryCodArt.articuloGenerico = resultGenerico;

                                    //DenArt
                                    if (sTraerDen == 1) {
                                        o.setValue(resultDenArt);
                                        o.setDataValue(resultDenArt);

                                        o.articuloGenerico = resultGenerico;
                                        o.codigoArticulo = EntryCodArt.getDataValue();
                                    }
                                    //Precio
                                    if ((sTraerPrecio == 1) && (!resultGenerico)) {
                                        oPrecio.setValue(resultPrecio)
                                        oPrecio.setDataValue(resultPrecio)
                                    }
                                    if (EntryMat.tipoGS == TIPOCAMPOGS.MATERIAL) {
                                        EntryMat.setValue(resultDenMat);
                                        EntryMat.setDataValue(resultCodMat);
                                        EntryMat.setToolTip(resultDenMatTip);
                                    }
                                }
                            }
                        }
                    }

                    if (o.readOnly && valoresLineaDesglose[o.idCampo] == undefined) {
                        if (o.tipoGS != TIPOCAMPOGS.DENARTICULO) {
                            o.setValue(o1.getValue());
                            o.setDataValue(o1.getDataValue());
                        }
                    } else {
                        //esto tiene que estar antes del setValue, si no, coge los decimales de las opciones del usuario
                        if (o.tipoGS == TIPOCAMPOGS.CANTIDAD) { //Cantidad
                            if (o1.NumeroDeDecimales == null) o.Editor.decimals = 15;
                            else o.Editor.decimals = o1.NumeroDeDecimales;
                        };
                        //campo de tipo texto medio o largo
                        // si no esta en blanco se le asigna el valor al campo de la linea copiada
                        if (valoresLineaDesglose[o.idCampo] !== undefined && valoresLineaDesglose[o.idCampo].Key != "") o.setValue(valoresLineaDesglose[o.idCampo].Value);
                        else {
                            if ((o.tipoGS != TIPOCAMPOGS.PRECIOUNITARIO) || (sTraerPrecio == 0) || (resultGenerico)) {
                                o.setValue(null);
                            }
                        }

                        if ((o.tipoGS == 0) && (o.intro == 1) && (o.readOnly == false)) {
                            var dropDown = $find(o.Editor.id);
                            if (dropDown && dropDown.set_currentValue) {
                                if (valoresLineaDesglose[o.idCampo] == undefined || valoresLineaDesglose[o.idCampo].Value == null) {
                                    dropDown.set_currentValue('', true);
                                } else {
                                    copiarSeleccionWebDropDown(o.Editor.id, valoresLineaDesglose[o.idCampo].Key);
                                    dropDown.set_currentValue(valoresLineaDesglose[o.idCampo].Value, true);
                                };
                            };
                            dropDown = null;
                        };
                        if ((o.tipoGS != 0) && (o.tipoGS != TIPOCAMPOGS.NUEVOCODARTICULO) && (o.tipoGS != TIPOCAMPOGS.DENARTICULO) &&
                            (o.tipoGS != TIPOCAMPOGS.CODARTICULO) && (o.tipoGS != TIPOCAMPOGS.IMPORTESOLICITUDESVINCULADAS) &&
                            (o.tipoGS != TIPOCAMPOGS.UNIDADORGANIZATIVA) && (o.tipoGS != TIPOCAMPOGS.PROVECONTACTO) && (o.intro == 1) && (!o.readOnly)) {
                            var dropDown = $find(o.Editor.id);
                            if (dropDown && dropDown.set_currentValue) {
                                if (valoresLineaDesglose[o.idCampo] == undefined || valoresLineaDesglose[o.idCampo].Value == null || valoresLineaDesglose[o.idCampo].Value == '') dropDown.set_currentValue('', true);
                                else {
                                    copiarSeleccionWebDropDown(o.Editor.id, valoresLineaDesglose[o.idCampo].Key)
                                    dropDown.set_currentValue(valoresLineaDesglose[o.idCampo].Value, true);
                                    if (o.tipoGS == 109 && valoresLineaDesglose[o.idCampo].Value != null) document.getElementById("igtxt" + o.Editor2.ID).style.visibility = "visible";
                                };
                            };
                            dropDown = null;
                        };
                        if (o.tipoGS == TIPOCAMPOGS.UNIDAD && o.idEntryCANT) o.idEntryCANT = o.idEntryCANT.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        if (o.tipoGS == TIPOCAMPOGS.CANTIDAD) {
                            o.NumeroDeDecimales = o1.NumeroDeDecimales;
                            o.UnidadOculta = o1.UnidadOculta;
                        };
                        if ((o.tipoGS == TIPOCAMPOGS.NUEVOCODARTICULO) || (o.tipoGS == TIPOCAMPOGS.DENARTICULO) || (o.tipoGS == TIPOCAMPOGS.CODARTICULO)) {
                            if (o.tipoGS == TIPOCAMPOGS.DENARTICULO) {
                                o.codigoArticulo = o1.codigoArticulo;
                                o.articuloGenerico = o1.articuloGenerico;
                                if (o.Dependent) o.Dependent.value = o1.Dependent.value;
                                if (o1.dependentfield) o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                            } else if ((o.tipoGS == TIPOCAMPOGS.NUEVOCODARTICULO) || (o.tipoGS == TIPOCAMPOGS.CODARTICULO)) {
                                if (o.Dependent) o.Dependent.value = o1.Dependent.value;
                                if (o.tipoGS == TIPOCAMPOGS.CODARTICULO && o1.dependentfield) o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                            };
                            if (o.idEntryPREC) o.idEntryPREC = o.idEntryPREC.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                            if (o.idEntryPROV) o.idEntryPROV = o.idEntryPROV.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                            if (o.idEntryCANT) o.idEntryCANT = o.idEntryCANT.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                            if (o.idEntryUNI) o.idEntryUNI = o.idEntryUNI.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                            if (o.IdEntryUniPedido) o.IdEntryUniPedido = o.IdEntryUniPedido.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                            if (o.idEntryCC) o.idEntryCC = o.idEntryCC.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                            if (o.idDataEntryDependent) o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                            if (o.idDataEntryDependent2) o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                            if (o.idDataEntryDependent3) o.idDataEntryDependent3 = o.idDataEntryDependent3.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                            if (o.idEntryProveSumiArt) o.idEntryProveSumiArt = o.idEntryProveSumiArt.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                            if (o.OrgComprasDependent) o.OrgComprasDependent.value = o1.OrgComprasDependent.value;
                            if (o.CentroDependent) o.CentroDependent.value = o1.CentroDependent.value;
                            if (o.UnidadDependent) o.UnidadDependent.value = o1.UnidadDependent.value;
                            if (o.IdMaterialOculta) o.IdMaterialOculta = o.IdMaterialOculta.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                            if (o.IdCodArticuloOculta) o.IdCodArticuloOculta = o.IdCodArticuloOculta.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        } else if ((o.tipoGS == TIPOCAMPOGS.PRECIOUNITARIO) || (o.tipoGS == TIPOCAMPOGS.PROVEEDOR)) { // Precio Unitario = 9 Y Proveedores = 100
                            o.cargarUltADJ = o1.cargarUltADJ;
                            if (o.tipoGS == TIPOCAMPOGS.PROVEEDOR) {
                                if (o.dependentfield) o.dependentfield = o.dependentfield.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                                if (o.idDataEntryDependent2) o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                            };
                        } else if ((o.tipoGS == TIPOCAMPOGS.INISUMINISTRO) || (o.tipoGS == TIPOCAMPOGS.FINSUMINISTRO)) {
                            if (o.idDataEntryDependent) o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        } else if (o.tipoGS == TIPOCAMPOGS.MATERIAL) {
                            if (o.idDataEntryDependent2) o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        if (o.tipoGS == TIPOCAMPOGS.ORGANIZACIONCOMPRAS) {
                            if (o.dependentfield) o.dependentfield = o.dependentfield.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                            if (o.idDataEntryDependent2) o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        if (o.tipoGS == TIPOCAMPOGS.PAIS) {
                            if (o.dependentfield) o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        if (o.tipoGS == TIPOCAMPOGS.CENTRO) {
                            if (o.idDataEntryDependent3) o.idDataEntryDependent3 = o.idDataEntryDependent3.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                        }
                        if (o.tipoGS == 151) {
                            if ((o.IdEntryPartidaPlurianual) && (o.EntryPartidaPlurianualCampo == 0))  o.IdEntryPartidaPlurianual = o.IdEntryPartidaPlurianual.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                        }
                        if ((o.tipoGS == TIPOCAMPOGS.ORGANIZACIONCOMPRAS) || (o.tipoGS == TIPOCAMPOGS.CENTRO)) {
                            if (o.idDataEntryDependent) o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                            if (valoresLineaDesglose[o.idCampo] == undefined) o.setDataValue('');
                            else o.setDataValue(valoresLineaDesglose[o.idCampo].Key);
                        } else if ((o.tipoGS == TIPOCAMPOGS.PRECIOUNITARIOADJ) || (o.tipoGS == TIPOCAMPOGS.CANTIDADADJ)) {
                            o.setValue('');
                            o.setDataValue('');
                        } else if ((o.tipoGS == TIPOCAMPOGS.PROVEEDORADJ)) {
                            o.setValue('');
                            o.setDataValue('');
                        } else if ((o.tipoGS == TIPOCAMPOGS.ESTADOINTERNONOCONF)) {
                            o.setValue(arrEstadosInternos[1][1]);
                            o.setDataValue(1);
                        } else if (o.tipoGS == 1020) {
                            o.setValue('');
                            o.setDataValue('');
                        } else if (o.tipoGS == TIPOCAMPOGS.IMPORTEREPERCUTIDO) {
                            if (o1) {
                                if (valoresLineaDesglose[o.idCampo] == undefined) {
                                    o.setValue('');
                                    o.setDataValue('');
                                } else {
                                    o.setValue(valoresLineaDesglose[o.idCampo].Value);
                                    o.setDataValue(valoresLineaDesglose[o.idCampo].Key);
                                }
                                o.MonRepercutido = o1.MonRepercutido;
                                o.CambioRepercutido = o1.CambioRepercutido;
                                if (o.readOnly == false) {
                                    var lnk = document.getElementById(o.dependentfield);
                                    lnk.innerHTML = o1.MonRepercutido;
                                    lnk = null;
                                };
                            };
                        } else if (o.tipoGS == 2000) {
                            if (valoresLineaDesglose[o.idCampo] == undefined) {
                                o.setValue('');
                                o.setDataValue('');
                            } else {
                                o.setValue(valoresLineaDesglose[o.idCampo].Value);
                                o.setDataValue(valoresLineaDesglose[o.idCampo].Key);
                            }
                        } else if (o.tipoGS == TIPOCAMPOGS.LINEA) {
                            o.setValue(o.nLinea);
                        } else if (o1.tipo !== SUBTIPO.ARCHIVO) {//No importamos los archivos  
                            if (valoresLineaDesglose[o.idCampo] == undefined) o.setDataValue('');
                            else o.setDataValue(valoresLineaDesglose[o.idCampo].Key);
                        };
                    };
                }
            } else { // else ---> if ((o != null)&&(o1 != null)) si no, se guarda en blanco
                if ((o != null) && (o1 == null) && (o.readOnly == true)) o.setDataValue(o.getDataValue());
            };
            if ((o.tipoGS == TIPOCAMPOGS.ARCHIVOESPECIFIC || o.tipo == SUBTIPO.ARCHIVO)) { //Fichero ADJUNTO               			
                var vaciar = new Array();
                vaciar[0] = "";
                vaciar[1] = "";
                o.setValue(''); //los de tipo fichero no se importan desde excel
                o.setDataValue(vaciar);

                vaciar = null;
            } else {
                switch (o.tipoGS) {
                    case TIPOCAMPOGS.CODARTICULO:
                    case TIPOCAMPOGS.DENARTICULO:
                        if (o1) {
                            if (o.codigoArticulo) o.codigoArticulo = o1.codigoArticulo;
                            if (o.articuloGenerico) o.articuloGenerico = o1.articuloGenerico;
                            if (o.Dependent) o.Dependent.value = o1.Dependent.value;
                        }
                    case TIPOCAMPOGS.NUEVOCODARTICULO:
                        if (o.dependentfield) o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                        if (o.idDataEntryDependent) {
                            if (o.idDataEntryDependent.search("fsentry_") < 0) { //La organizacion de compras de encuentra fuera del desglose
                                oEntry = fsGeneralEntry_getById(o.idDataEntryDependent)
                                if ((oEntry) && (oEntry.tipoGS == 123)) oEntry.idDataEntryDependent = arraynuevafila[i][0];
                            };
                            o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        if (o.idDataEntryDependent2) o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");

                        if (o.idDataEntryDependent3) {
                            if (o.idDataEntryDependent3.search("fsentry_") < 0) { //La organizacion de compras de encuentra fuera del desglose
                                oEntry = fsGeneralEntry_getById(o.idDataEntryDependent3);
                                if ((oEntry) && (oEntry.tipoGS == 121)) oEntry.idDataEntryDependent3 = arraynuevafila[i][0];
                            };
                            o.idDataEntryDependent3 = o.idDataEntryDependent3.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        if (o.idEntryPREC) o.idEntryPREC = o.idEntryPREC.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        if (o.idEntryPROV) o.idEntryPROV = o.idEntryPROV.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        if (o.idEntryCANT) o.idEntryCANT = o.idEntryCANT.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        if (o.idEntryUNI) o.idEntryUNI = o.idEntryUNI.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        if (o.IdEntryUniPedido) o.IdEntryUniPedido = o.IdEntryUniPedido.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        if (o.idEntryCC) o.idEntryCC = o.idEntryCC.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        if (o.IdMaterialOculta) o.IdMaterialOculta = o.IdMaterialOculta.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        if (o.IdCodArticuloOculta) o.IdCodArticuloOculta = o.IdCodArticuloOculta.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        if (o.idEntryProveSumiArt) o.idEntryProveSumiArt = o.idEntryProveSumiArt.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        if (o1) {
                            if (o.OrgComprasDependent) o.OrgComprasDependent.value = o1.OrgComprasDependent.value;
                            if (o.CentroDependent) o.CentroDependent.value = o1.CentroDependent.value;
                            if (o.UnidadDependent) o.UnidadDependent.value = o1.UnidadDependent.value;
                        }
                        break;
                    case TIPOCAMPOGS.INISUMINISTRO:
                    case TIPOCAMPOGS.FINSUMINISTRO:
                        if (o.idDataEntryDependent) o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        break;
                    case TIPOCAMPOGS.PROVEEDOR:
                        //Proveedores
                        if (o.dependentfield) o.dependentfield = o.dependentfield.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        if (o.idDataEntryDependent2) o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        break;
                    case TIPOCAMPOGS.MATERIAL:
                        if (o.idDataEntryDependent2) o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        break;
                    case TIPOCAMPOGS.ORGANIZACIONCOMPRAS:
                        if (o.dependentfield) o.dependentfield = o.dependentfield.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        if (o.idDataEntryDependent) o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        if (o.idDataEntryDependent2) o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        break;
                    case TIPOCAMPOGS.PAIS:
                        //pais
                        if (o.dependentfield) o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                        break;
                    case TIPOCAMPOGS.CENTRO:
                        if (o.idDataEntryDependent) {
                            o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                            var oEntryArt = fsGeneralEntry_getById(o.idDataEntryDependent);
                            if ((oEntryArt) && (oEntryArt.idDataEntryDependent)) {
                                if (oEntryArt.idDataEntryDependent.search("fsentry_") < 0) { //La organizacion de compras de encuentra fuera del desglose
                                    oEntry = fsGeneralEntry_getById(oEntryArt.idDataEntryDependent);
                                    if ((oEntry) && (oEntry.tipoGS == 123)) oEntry.idDataEntryDependent2 = arraynuevafila[i][0];
                                };
                            };
                        };
                        if (o.idDataEntryDependent3) o.idDataEntryDependent3 = o.idDataEntryDependent3.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                        break;
                    case TIPOCAMPOGS.IMPORTEREPERCUTIDO:
                        o.MonRepercutido = o.MonCentral;
                        o.CambioRepercutido = o.CambioCentral;
                        if (o.readOnly == false) {
                            var lnk = document.getElementById(o.dependentfield)
                            lnk.innerHTML = o.MonCentral;
                        };
                        break;
                    case TIPOCAMPOGS.PRES1:
                    case TIPOCAMPOGS.PRES2:
                    case TIPOCAMPOGS.PRES3:
                    case TIPOCAMPOGS.PRES4:
                        break;
                    case TIPOCAMPOGS.UNIDAD:
                        if (o.idEntryCANT) o.idEntryCANT = o.idEntryCANT.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        break;
                    case TIPOCAMPOGS.CANTIDAD:
                        if (o1) {
                            o.NumeroDeDecimales = o1.NumeroDeDecimales
                            o.UnidadOculta = o1.UnidadOculta
                        }
                        break;
                    case TIPOCAMPOGS.UNIDADORGANIZATIVA:
                        if (o.idDataEntryDependent) {
                            if (o.idDataEntryDependent.search("fsentry_") < 0) { //La organizacion de compras de encuentra fuera del desglose
                                oEntry = fsGeneralEntry_getById(o.idDataEntryDependent);
                                if ((oEntry) && (oEntry.tipoGS == 118)) oEntry.idDataEntryDependent = arraynuevafila[i][0];
                            };
                            o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        };
                        break;
                    case TIPOCAMPOGS.CENTROCOSTE:
                        if ((!o.PoseeDefecto) && (!o.dataValue)) {
                            var centro = comprobarCentroUnico(o.VerUON);
                            if (centro != "") {
                                o.PoseeDefecto = true;
                                o.TextoValorDefecto = centro.Denominacion;
                                o.DataValorDefecto = centro.Texto;
                            }
                        }
                        break;
                    case TIPOCAMPOGS.PARTIDA:
                        var codCCoste = "";
                        if ((!o.PoseeDefecto) && (!o.dataValue)) {
                            if (o.idEntryCC) {
                                var desglose = o.id.substr(0, o.id.search("_fsdsentry"))
                                var index = o.id.substr(o.id.search("_fsdsentry_") + 11, o.id.search(o.idCampo) - o.id.search("_fsdsentry_") - 12)
                                oAux = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + o.idEntryCC);
                                //codCCoste = oAux.DataValorDefecto;
                                if (oAux) {
                                    codCCoste = oAux.DataValorDefecto;
                                } else {
                                    for (arrInput in arrInputs) {
                                        oAux = fsGeneralEntry_getById(arrInputs[arrInput]);
                                        if (oAux) {
                                            if ((oAux.tipoGS == 129) && ((o.idEntryCC == oAux.campo_origen) || (o.idEntryCC == oAux.idCampo))) {
                                                if (oAux.idCampoPadre) { //Todas las filas desglose tendran el mismo DataValorDefecto. As� q para linea 4ta cojo el valordefecto de la 1ra.
                                                    codCCoste = oAux.DataValorDefecto;
                                                } else { //El centro coste esta fuera del desglose
                                                    codCCoste = oAux.getDataValue();
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                            };
                            partida = comprobarPartidaUnica(o.PRES5, codCCoste);
                            if (partida != "") {
                                o.PoseeDefecto = true;
                                o.TextoValorDefecto = partida.Denominacion;
                                o.DataValorDefecto = partida.Texto;
                            }
                        }
                        break;
                    case TIPOCAMPOGS.ANYOPARTIDA:
                        var codPartida0 = '';
                        var codPartida = ' ';
                        if ((!o.PoseeDefecto) && (!o.dataValue)) {
                            if (o.IdEntryPartidaPlurianual) {
                                oAux = fsGeneralEntry_getById(o.IdEntryPartidaPlurianual)
                                if (oAux) {
                                    codPartida0 = oAux.PRES5
                                    codPartida = ' ' + oAux.getDataValue();
                                }
                                var AnyoPartida = comprobarAnyoPartidaUnica(codPartida0, codPartida);
                                if (AnyoPartida.length == 1) {
                                    o.PoseeDefecto = true;
                                    o.TextoValorDefecto = AnyoPartida[0].Valor;
                                    o.DataValorDefecto = AnyoPartida[0].Valor;
                                }
                            }
                        }
                        break;
                };
                if (o.tipoGS == TIPOCAMPOGS.ESTADOINTERNONOCONF) {
                    o.setValue(arrEstadosInternos[1][1]);
                    o.setDataValue(1);
                };
                if (o.tipoGS == TIPOCAMPOGS.LINEA) o.setValue(o.nLinea);
                if (o.readOnly == false && !o.intro == 2) {
                    var sID = '';
                    if (o.Editor) {
                        if (o.Editor.ID) sID = o.Editor.ID;
                        else sID = o.Editor.id;

                        if (sID != '' && sID != undefined)
                            if (o.getDataValue() != '' && o.getDataValue() != null) copiarSeleccionWebDropDown(sID, o.getDataValue());
                            else CambiarSeleccion(sID, "BORRAR");
                    };
                    sID = null;
                };
            };
            if (o.masterField && o.masterField != '') o.masterField = o.masterField.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
            if (o.dependentfield && o.dependentfield != '') {
                //Se han usado tanto 'fsentry_' como con 'fsentry'. Ante la duda, controlamos ambos casos
                o.dependentfield = o.dependentfield.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
            };
        };
    };
    s = null;
    oTR = null;
    lIndex = null;
    lLineaGrid = null;
    sMovible = null;
    lPrimeraLinea = null;
    arraynuevafila = null;
    insertarnuevoelemento = null;
    VecesEntre = null;
    re = null;
    fin = null;
    Textos = null;
    iCeldaIni = null;
    o = null;
    o1 = null;
    oEntry = null;
};

/*''' <summary>
''' Integraci�n solo puede trabajar con listas externas bien formateadas. C�digo espacio gui�n espacio descripci�n.
''' A traves de cargar excel o escribiendo a mano se ha conseguido darle valor mal formato.
''' </summary>
''' <param name="vValor">lista externa/param>
''' <remarks>Llamada desde:MontarFormularioSubmit; Tiempo m�ximo:0</remarks>
*/
function FormatearListaExterna(vValor) {
    if (vValor !== "") {
        if ((vValor.search("-") >= 0) && (vValor.search(" - ") < 0)) {
            vValor = vValor.replace("-", " - ")
        }
        while ((vValor.search("  -") >= 0) || (vValor.search("-  ") >= 0)) {
            vValor = vValor.replace("  -", " -");
            vValor = vValor.replace("-  ", "- ");
        }
    }
    return vValor;
}
function show_companies(oEntry, text, event) {
    var newWindow = window.open(rutaFS + "_common/BuscadorEmpresas.aspx?IdControl=" + oEntry[0].id + "&opener=desdeSolicitud", "_blank", "width=835,height=635,status=yes,resizable=yes,top=0,left=150,scrollbars=yes")
    newWindow.focus();
};
function SeleccionarEmpresaSolicitud(idControl, idEmpresa, nombreEmpresa, nifEmpresa) {
    o = fsGeneralEntry_getById(idControl);
    o.setValue(nifEmpresa + ' - ' + nombreEmpresa);
    o.setDataValue(idEmpresa);
}
function onEmpresaItemSelected(sender, e) {
    if (e._text != null) {
        var oEdit = $('#' + sender._element.id.substring(0, sender._element.id.length - 2));
        oEdit[0].fsEntry.setValue(e._text);
        oEdit[0].fsEntry.setDataValue(e._value);
    }
}