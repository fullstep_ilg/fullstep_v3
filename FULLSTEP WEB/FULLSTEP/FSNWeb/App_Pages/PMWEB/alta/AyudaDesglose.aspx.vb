﻿Public Class AyudaDesglose
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.DesgloseControl

        lblTituloAyudaDesglose.Text = Textos(53)
        lblSubTituloAyudaDesglose.Text = Textos(54)
        lblInstruccionesAyudaDesglose.Text = Server.HtmlDecode(Textos(55))
    End Sub
End Class