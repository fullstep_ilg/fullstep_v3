Public Class desglose
    Inherits FSNPage

#Region " Web Form Designer Generated Code "
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    End Sub
    Protected WithEvents lblSubTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents INPUTDESGLOSE As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblTituloData As System.Web.UI.WebControls.Label
    Protected WithEvents cmdGuardar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cmdCalcular As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents Solicitud As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Favorito As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents IdInstanciaImportar As System.Web.UI.HtmlControls.HtmlInputHidden
    Private designerPlaceholderDeclaration As System.Object
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
#End Region
    ''' <summary>
    ''' Cargar un desglose en un popup
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">evento de sistema</param>        
    ''' <remarks>Llamada desde: alta/NWalta.aspx; Tiempo m�ximo:0,1</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim idCampo As Integer = Request("campo")
        Dim sInput As String = Request("Input")
        Dim oCampo As FSNServer.Campo
        Dim lSolicitud As Long

        INPUTDESGLOSE.Value = sInput
        If Request("Solicitud") <> Nothing Then lSolicitud = Request("Solicitud")

        Solicitud.Value = lSolicitud
        Favorito.Value = Request("Favorito")
        IdInstanciaImportar.Value = Request("IdInstanciaImportar")

        oCampo = FSNServer.Get_Object(GetType(FSNServer.Campo))
        oCampo.Id = idCampo
        oCampo.Load(Idioma, lSolicitud)

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Desglose

        lblTitulo.Text = Textos(0)
        lblTituloData.Text = oCampo.DenSolicitud(Idioma)

        If Request("BotonCalcular") <> Nothing Then
            If Request("BotonCalcular") = 1 Then
                cmdCalcular.Visible = True
            Else
                cmdCalcular.Visible = False
            End If
        End If

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ruta", "<script>var ruta = '" & ConfigurationManager.AppSettings("ruta") & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM2008", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' </script>")

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "version") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "version",
                "<script>var version = '" & System.Configuration.ConfigurationManager.AppSettings("version") & "';</script>")
        End If

        FindControl("frmAlta").Controls.Add(FSNWeb.CommonAlta.InsertarCalendario())

        Dim oucdesglose As desgloseControl
        oucdesglose = FindControl("ucdesglose")
        oucdesglose.Version = Request("Version")
        oucdesglose.PM = True
        oucdesglose.Titulo = oCampo.DenGrupo(Idioma) + " / " + oCampo.Den(Idioma)
        oucdesglose.MonedaSolicitud = Request("MonedaSolicitud")
        oucdesglose.TipoSolicitud = oCampo.TipoSolicitud

        cmdGuardar.Value = Textos(2)
        cmdGuardar.Attributes.Add("onclick", "return actualizarCampoDesgloseYCierre()")
        cmdCalcular.Value = Textos(3)
    End Sub
End Class