<%@ Page Language="vb" AutoEventWireup="false" Codebehind="solicitudPMWEB.aspx.vb" Inherits="Fullstep.FSNWeb.solicitudPMWEB"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
	</head>
	<script type="text/javascript">
			function Descargar(IdAdjunto) {
			    var newWindow = window.open("espec.aspx?id=" + IdAdjunto, "_blank", "width=600,height=275,status=no,resizable=yes,top=200,left=200,menubar=yes");			    
			}
	</script>	
	<body>
		<form id="Form1" method="post" runat="server">
			<br/>
			<asp:label id="lblTitulo" style="Z-INDEX: 104" runat="server" Width="100%" CssClass="subtitulo"></asp:label><br/>
			<br/>
			<asp:label id="lblDescripcion" style="Z-INDEX: 103" runat="server" Width="88px" CssClass="captionBlue"	Height="16px"></asp:label><br/>
			<asp:label id="txtDescripcion" runat="server" Width="100%" CssClass="parrafo bordeado" Height="56px"></asp:label><br/>
			<br>
			<asp:label id="lblArchivos" style="Z-INDEX: 101" runat="server" Width="192px" CssClass="captionBlue" Height="16px"></asp:label><br/>
			<table class="bordeado" id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
				<tr>
					<td>
                        <igtbl:ultrawebgrid id="uwgEspecs" style="Z-INDEX: 105" runat="server" Width="100%" Height="200px">
							<DisplayLayout RowHeightDefault="20px" Version="4.00" BorderCollapseDefault="Separate" Name="uwgEspecs" ReadOnly="LevelTwo">
								<FrameStyle Width="100%" Font-Size="8pt" Font-Names="Verdana" Height="200px"></FrameStyle>
								<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
									<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
								</FooterStyleDefault>
							</DisplayLayout>
							<Bands>
								<igtbl:UltraGridBand CellSpacing="1" CellPadding="3" RowSelectors="No">
									<HeaderStyle CssClass="Cabecera Texto12"></HeaderStyle>
									<RowAlternateStyle CssClass="ugfilaalttabla"></RowAlternateStyle>
									<Columns>
										<igtbl:UltraGridColumn HeaderText="Descargar" Key="DESCARGAR" BaseColumnName="">
											<Header Caption="Descargar"></Header>
										</igtbl:UltraGridColumn>
									</Columns>
									<RowStyle CssClass="ugfilatabla"></RowStyle>
								</igtbl:UltraGridBand>
							</Bands>
						</igtbl:ultrawebgrid>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
