﻿<%@ Register Assembly="Infragistics.WebUI.UltraWebTab.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>
<%@ Register Assembly="Infragistics.WebUI.WebDataInput.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.WebDataInput" TagPrefix="igtxt" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NWAlta.aspx.vb" Inherits="Fullstep.FSNWeb.NWAlta" EnableViewState="False" EnableEventValidation="false" %>
<%@ Register Assembly="Infragistics.WebUI.UltraWebNavigator.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebNavigator" TagPrefix="ignav" %>
<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
</head>
<script type="text/javascript">
    var wProgreso = null;

    //Muestra el contenido peligroso que ha efectuado un error al guardar.
    function ErrorValidacion(contenido) {
        alert(arrTextosML[2].replace("$$$", contenido));
        HabilitarBotones();
    };
    // Revisado por: blp. Fecha: 05/10/2011
    //Pone un ancho al desglose
    //Llamada desde: javascript de la página. Máx < 0,1 seg.
    function resize() {
        for (i = 0; i < arrDesgloses.length; i++) {
            var sDiv = arrDesgloses[i].replace("tblDesglose", "divDesglose")
            if (document.getElementById(sDiv))
                document.getElementById(sDiv).style.width = parseFloat(document.body.offsetWidth) - 95 + 'px';
            sDiv = null;
        }
    };
    function localEval(s) {
        eval(s)
    };

    //Muestra el div que indica que se está cargando la solicitud
    function MostrarEspera() {
        wProgreso = true;

        $("[id*='lnkBoton']").attr('disabled', 'disabled');

        document.getElementById("lblCamposObligatorios").style.display = "none"
        document.getElementById("divForm2").style.display = 'none';
        document.getElementById("divForm3").style.display = 'none';
        document.getElementById("igtabuwtGrupos").style.display = 'none';

        var i = 0;
        var bSalir = false;
        while (bSalir == false) {
            if (document.getElementById("uwtGrupos_div" + i)) {
                document.getElementById("uwtGrupos_div" + i).style.visibility = 'hidden';
                i = i + 1;
            }
            else {
                bSalir = true;
            }
        }
        document.getElementById("lblProgreso").value = document.forms["frmAlta"].elements["cadenaespera"].value;
        document.getElementById("divProgreso").style.display = 'inline';
    };

    //Estaba habilitando los botones antes de ocultar el progreso. Parecia q te dejaba dar a varios botones mientras estaba en progreso.
    function DarTiempoAOcultarProgreso() {
        $("[id*='lnkBoton']").removeAttr('disabled');
    }

    // Revisado por: blp. Fecha: 05/10/2011
    //Oculta el div que indica que se está cargando la solicitud
    //Llamada desde: javascript de la página. Máx < 0,1 seg.
    function OcultarEspera() {
        wProgreso = null;

        document.getElementById("divProgreso").style.display = 'none';
        document.getElementById("divForm2").style.display = '';
        document.getElementById("divForm3").style.display = 'inline';
        document.getElementById("igtabuwtGrupos").style.display = '';

        setTimeout('DarTiempoAOcultarProgreso()', 250);

        var i = 0;
        var bSalir = false;
        while (bSalir == false) {
            if (document.getElementById("uwtGrupos_div" + i)) {
                document.getElementById("uwtGrupos_div" + i).style.visibility = 'visible';
                i = i + 1;
            }
            else {
                bSalir = true;
            }
        }
    };
    /* Revisado por: blp. Fecha: 05/10/2011
    ''' <summary>
    ''' Monta el formulario para el posterior guardado. (guardarInstancia.aspx)
    ''' </summary>
    ''' Llamada desde: javascript de la página. Máx < 0,1 seg.*/
    function MontarSubmitGuardar() {
        MontarFormularioSubmit(true, true, false, false);

        var frmSubmitElements = document.forms["frmSubmit"].elements;
        frmSubmitElements["GEN_AccionRol"].value = 0;
        frmSubmitElements["GEN_Bloque"].value = document.forms["frmAlta"].elements["Bloque"].value;
        frmSubmitElements["GEN_Enviar"].value = 0;
        frmSubmitElements["GEN_Accion"].value = "guardarsolicitud";
        frmSubmitElements["GEN_Rol"].value = oRol_Id;
        frmSubmitElements["PantallaMaper"].value = false;
        frmSubmitElements["DeDonde"].value = "Guardar";

        var oFrm = MontarFormularioCalculados();
        var sInner = oFrm.innerHTML;
        oFrm.innerHTML = "";
        document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner);
        frmSubmitElements = null;
        oFrm = null;
        sInner = null;
        document.forms["frmSubmit"].submit();
    };
    /* Revisado por: blp. Fecha: 05/10/2011
    ''' <summary>
    ''' Comprueba las cantidades si hay vinculación y llama a MontarSubmitGuardar (fn guardado solicitud)
    ''' </summary>
    ''' <remarks>Llamada desde: cmdGuardar; Tiempo máximo: 0,2</remarks>*/
    function Guardar() {
        //si existe campo moneda, aunque la comprobación no sea obligatoria, debemos comprobarlo
        //o no se creará la instancia - Tarea 2918
        var oEntryMoneda = buscarCampoEnFormulario(102);
        if (oEntryMoneda) {
            if (!oEntryMoneda.getDataValue()) {
                alert(arrTextosML[0] + '\n' + oEntryMoneda.title.toString());
                return false;
            }
        }

        var frmAltaElements = document.forms["frmAlta"].elements;
        if (frmAltaElements["PantallaVinculaciones"].value == "True") {
            var instancia = document.getElementById("NEW_Instancia").value;
            var solicitud = 1;
            if (instancia > 0)
                solicitud = 0;
            if (Validar_Cantidades_Vinculadas(solicitud, instancia) == false) {
                return false
            }
            instancia = null;
            solicitud = null;
        }

        frmAltaElements = null;

        if (wProgreso == null) {
            wProgreso = true;
            MostrarEspera();
        }

        setTimeout("MontarSubmitGuardar()", 100)
        return false;
    };
    /* Revisado por: blp. Fecha: 05/10/2011
    Función que monta el formulario calculado y luego efectúa el submit 
    Llamada desde evento onClick del botón cmdCalcular. Máx < 1 seg.
    */
    function CalcularCamposCalculados() {
        var oFrm = MontarFormularioCalculados()
        oFrm.submit()
        oFrm = null;
        return false;
    };
    /* Revisado por: blp. Fecha: 05/10/2011
    ''' <summary>
    ''' Monta el formulario para el posterior cambio de etapa. (guardarInstancia.aspx)
    ''' </summary>
    ''' <param name="id">Id Accion</param>
    ''' <param name="bloque">Id Bloque</param>        
    ''' <param name="comp_obl">Si es necesario rellenar los campos obligatorios</param>
    ''' <param name="guarda">si hay que guardar o versión o no</param> 
    ''' <param name="HayMapper">Si es necesario hacer comprobaciones mapper en guardarInstancia.aspx</param>
    ''' <remarks>Llamada desde: javascript EjecutarAccion2; Tiempo máximo:0,2</remarks>*/
    function MontarSubmitAccion(id, bloque, comp_obl, guarda, HayMapper) {
        MontarFormularioSubmit(guarda, true, HayMapper, false)

        var frmSubmitElements = document.forms["frmSubmit"].elements;
        frmSubmitElements["GEN_AccionRol"].value = id;
        frmSubmitElements["GEN_Bloque"].value = bloque;
        frmSubmitElements["GEN_Rol"].value = oRol_Id;
        frmSubmitElements["PantallaMaper"].value = HayMapper;
        frmSubmitElements["DeDonde"].value = "Guardar";

        var oFrm = MontarFormularioCalculados()

        var sInner = oFrm.innerHTML
        oFrm.innerHTML = ""
        document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner)
        frmSubmitElements = null;
        oFrm = null;
        sInner = null;
        document.forms["frmSubmit"].submit()
    };
    //Descripción: Cuando se pulsa un botón para realizar una acción, se llama a esta función
    //Paramátros: id: id de la acción
    // bloque: id del bloque
    // comp_olb: si hay que comprobar los campos de obligatorios o no
    // guarda: si hay que guardar o versión o no
    // rechazo: indica si la accion implica un rechazo, ya sea temporal o definitivo
    function EjecutarAccion(id, bloque, comp_obl, guarda, rechazo) {
        setTimeout("EjecutarAccion2(" + id + "," + bloque + "," + comp_obl + "," + guarda + "," + rechazo + ")", 100);
    };
    //Revisado por: blp. Fecha: 05/10/2011
    //Descripción: Comprueba los campos obligatorios, si hay participantes o no, y llama a montarformulario
    //Paramátros: id: id de la acción
    // bloque: id del bloque
    // comp_olb: si hay que comprobar los campos de obligatorios o no
    // guarda: si hay que guardar o versión o no
    // rechazo: indica si la accion implica un rechazo, ya sea temporal o definitivo
    // LLamada desde: EjecutarAccion. Max < 1 seg
    function EjecutarAccion2(id, bloque, comp_obl, guarda, rechazo) {//maper=True
    	var respOblig;
    	if (comp_obl == true || comp_obl == "true") {

    		var bMensajePorMostrar = document.getElementById("bMensajePorMostrar")
    		if (bMensajePorMostrar)
    			if (bMensajePorMostrar.value == "1") {
    				bMensajePorMostrar.value = "0";
    				return false;
    			}
    		bMensajePorMostrar = null;
    		respOblig = comprobarObligatorios();
    		switch (respOblig) {
    			case "":  //no falta ningun campo obligatorio
    				break;
    			case "filas0": //no se han introducido filas en un desglose obligatorio
    				alert(arrTextosML[0]);
    				return false;
    				break;
    			default: //falta algun campo obligatorio
    				alert(arrTextosML[0] + '\n' + respOblig);
    				return false;
    				break;
    		}
    	} else {
    		//si existe campo moneda, aunque la comprobación no sea obligatoria, debemos comprobarlo
    		//o no se creará la instancia - Tarea 2918
    		var oEntryMoneda = buscarCampoEnFormulario(102);
    		if (oEntryMoneda) {
    			if (!oEntryMoneda.getDataValue()) {
    				alert(arrTextosML[0] + '\n' + oEntryMoneda.title.toString());
    				return false;
    			}
    		}
    	};

        if (rechazo == false) {
        	respOblig = comprobarParticipantes();
        	if (respOblig !== '') {
        		alert(arrTextosML[0] + '\n' + respOblig);
        		return false;
        	}
        };

        var frmAltaElements = document.forms["frmAlta"].elements;
        if (frmAltaElements["PantallaVinculaciones"].value == "True") {
            var instancia = document.getElementById("NEW_Instancia").value;
            var solicitud = 1;
            if (instancia > 0)
                solicitud = 0;
            if (Validar_Cantidades_Vinculadas(solicitud, instancia) == false) {
                return false
            }
            instancia = null;
            solicitud = null;
        }

        if (wProgreso == null) {
            wProgreso = true;
            MostrarEspera()
        }

        var HayMapper = false
        if (frmAltaElements["PantallaMaper"].value == "True")
            HayMapper = true

        setTimeout("MontarSubmitAccion(" + id + "," + bloque + "," + comp_obl + "," + guarda + "," + HayMapper + ")", 100)

        frmAltaElements = null;
        HayMapper = null;
    };
    function init() {
        resize();
    };
    //Habilita los botones disponibles en la solicitud y llama a ocultar espera	
    function HabilitarBotones() {
        if (arrObligatorios.length > 0)
            if (document.getElementById("lblCamposObligatorios"))
                document.getElementById("lblCamposObligatorios").style.display = "";
        OcultarEspera();
    };
    //Una vez guardada la solicitud se muestra el id que se le ha asignado
    function PonerIdSolicitud(instancia) {
        if (document.forms["frmAlta"].elements["NEW_Instancia"].value == "") {
            if (document.getElementById("lblCabecera"))
                document.getElementById("lblCabecera").innerHTML = instancia + ' - ' + document.getElementById("lblCabecera").innerHTML;
        };

        document.getElementById("NEW_Instancia").value = instancia;

        HabilitarBotones();
        wProgreso = null;
    };
    function EnviarFavoritos() {
        window.open("enviarFavoritos.aspx?TipoSolicitud=" + tipoSolicitud, "enviarFavoritos", "width=500,height=200,status=yes,resizable=no,top=200,left=300");
        return false;
    };
    function ImportarInstancia() {
        window.open('<%=ConfigurationManager.AppSettings("rutaFS")%>_common/BuscadorSolicitudes.aspx?Solicitud=' + $("#Solicitud").val() + '&Importar=1&TipoSolicitud=' + tipoSolicitud, '_blank', 'width=920,height=450,status=yes,resizable=no,scrollbars=yes,top=100,left=100');
		    return false;
		};
		//Monta el formulario frmSubmitFav para enviarlo a la pagina  guardarSOLFavorita
		// y ser almacenada en las tablas.
		function guardarFavoritos() {
		    MontarFormularioFavoritos();
		    window.document.forms["frmSubmitFav"].submit();
		};
		// Revisado por: blp. Fecha: 05/10/2011
		// Función que modifica el ancho mínimo de un control
		// Llamada en el evento InitializeTabs del control uwtGrupos. Máx < 1seg
		function initTab(webTab) {
		    var cp = document.getElementById(webTab.ID + '_cp');
		    cp.style.minHeight = '300px';
		    cp = null;
		};
		/*''' <summary>
	    ''' Evitar los enter q hacen q el 1er botón de FSPMPageHeader se ejecute. No se puede traducir en Tab en FF/Chrome. Pero si evitar el submit y q haga lo q debe.
	    ''' </summary>
	    ''' <param name="event">Pulsación</param>
	    ''' <remarks>Llamada desde: sistema; Tiempo máximo:0</remarks>*/
		function FFreplaceEnterKeyWithTab(event) {
		    if (!window.event)//FF
		        if ((event.target.type != 'textarea') && (event.which == 13))//13 is enter key
		            event.preventDefault();
		};
		/*''' <summary>
	    ''' Evitar los enter q hacen q el 1er botón de FSPMPageHeader se ejecute. Se traduce en Tab.
	    ''' </summary>
	    ''' <param name="event">Pulsación</param>
	    ''' <returns>True para Cualquier tecla q pulses excepto el enter</returns>
	    ''' <remarks>Llamada desde: sistema; Tiempo máximo:0</remarks>*/
		function replaceEnterKeyWithTab() {
		    if (window.event)//IE
		        if ((window.event.srcElement.type != 'textarea') && (window.event.keyCode == 13))//13 is enter key	            
		            window.event.keyCode = 9; //return 9 the tab key 
		};
		function mostrarMenuAcciones(event, i) {
		    var p = $get("lnkBotonAccion" + i.toString()).getBoundingClientRect();
		    igmenu_showMenu('uwPopUpAcciones', event, p.left, p.bottom);
		    return false;
		}		
</script>
<script language="javascript" type="text/javascript">
    var xmlHttp;
    /*''' <summary>
    ''' Crear el objeto para llamar con ajax a ComprobarEnProceso
    ''' </summary>
    ''' <remarks>Llamada desde: javascript ; Tiempo máximo: 0</remarks>*/
    function CreateXmlHttp() {
        // Probamos con IE
        try {
            // Funcionará para JavaScript 5.0
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (oc) {
                xmlHttp = null;
            }
        }

        // Si no se trataba de un IE, probamos con esto
        if (!xmlHttp && typeof XMLHttpRequest != "undefined") {
            xmlHttp = new XMLHttpRequest();
        }

        return xmlHttp;
    };
    //Creamos el objeto xmlHttpRequest
    CreateXmlHttp();
    /* Revisado por: blp. Fecha: 05/10/2011
    ''' <summary>
    ''' Hacer una validación a medida de cantidades antes de añadir lineas vinculadas
    ''' </summary>
    ''' <param name="sRoot">En q desglose, html, se van añadir lineas</param>
    ''' <param name="IdCampo">En q desglose, form_campo.id, se van añadir lineas</param>        
    ''' <param name="Row">Fila a añadir</param>
    ''' <remarks>Llamada desde: PMWeb2008/BuscadorSolicitudes.aspx.vb; Tiempo máximo:0,1</remarks>*/
    function VincularCopiarFilaVacia(sRoot, IdCampo, Row) {
        CreateXmlHttp();
        if (xmlHttp) {
            if (Row.getCellFromKey) {
                var params = "Instancia=" + Row.getCellFromKey("INSTANCIAORIGEN").getValue() + "&Desglose=" + Row.getCellFromKey("DESGLOSEORIGEN").getValue() + "&Linea=" + Row.getCellFromKey("LINEAORIGEN").getValue();
            } else {
                var params = "Instancia=" + Row.get_cellByColumnKey("INSTANCIAORIGEN").get_value() + "&Desglose=" + Row.get_cellByColumnKey("DESGLOSEORIGEN").get_value() + "&Linea=" + Row.get_cellByColumnKey("LINEAORIGEN").get_value();
            }

            xmlHttp.open("POST", "../_common/controlarLineaVinculada.aspx", false);
            xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            xmlHttp.send(params);

            //Tras q se ejecute sincronamente controlarLineaVinculada.aspx controlamos sus resultados y obramos en 
            //consecuencia.                            
            var retorno;
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                retorno = xmlHttp.responseText;
                if (retorno == 'OK')  //pasa la comprobación
                    copiarFilaVacia(sRoot, IdCampo, 0, Row);
                else
                    var newWindow = window.open("../_common/NoPasoControlarLineaVinculada.aspx?" + params, "_blank", "width=350,height=200,status=yes,resizable=no,top=200,left=300");
            }
            params = null;
            retorno = null;
        }
    };
    /*''' <summary>
    ''' Añadir instancia vinculadas
    ''' </summary>
    ''' <param name="sRoot">En q desglose, html, se van añadir lineas</param>
    ''' <param name="IdCampo">En q desglose, form_campo.id, se van añadir lineas</param>        
    ''' <param name="sId">Id de la instancia vinculada</param>
    ''' <param name="sDen">Descrip de la instancia vinculada</param> 
    ''' <remarks>Llamada desde: PMWeb2008/BuscadorSolicitudes.aspx.vb; Tiempo máximo:0,1</remarks>*/
    function SeleccionarSolicitud(sRoot, IdCampo, sId, sDen) {
        copiarFilaVacia(sRoot, IdCampo, 0, null, sId, sDen);
    };
    /*''' <summary>
    ''' Vuelve a la pagina anterior
    ''' </summary>
    ''' <remarks>Llamada desde: Cuando se pincha al enlace ("Volver"); Tiempo máximo:0,1</remarks>*/
    function HistoryHaciaAtras() {
        valor = document.getElementById("hid_DesdeInicio");
        if (valor) {
            if (valor.value == 1) {
                window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>Inicio.aspx", "_self");
            }
            else {                
                switch (Number(tipoSolicitud)) {
                    case 12:
                    case 13:
                    case 14:
                        window.open("<%=ConfigurationManager.AppSettings("rutaPM2008")%>Tareas/AltaSolicitudes.aspx?TipoSolicitud=" + tipoSolicitud, "_self");
                        break;
                    default:
                        window.open("<%=ConfigurationManager.AppSettings("rutaPM2008")%>Tareas/AltaSolicitudes.aspx", "_self");
                }
            }
        }
    };
</script>
<style type="text/css">
    html, body {
        height: 100%;
        width: 100%;
    }
</style>
<body onresize="resize()" onload="init()" onunload="if (wProgreso != null) OcultarEspera();">
    <form id="frmAlta" method="post" runat="server" onkeydown="replaceEnterKeyWithTab()" onkeypress="FFreplaceEnterKeyWithTab(event)">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <CompositeScript>
                <Scripts>
                    <asp:ScriptReference Name="ExtenderBase.BaseScripts.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Common.Common.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Compat.Timer.Timer.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Animation.Animations.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Animation.AnimationBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="PopupExtender.PopupBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="AutoComplete.AutoCompleteBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Path="js/AdjacentHTML.js" />
                    <asp:ScriptReference Path="../alta/js/jsAlta.js" />
                </Scripts>
            </CompositeScript>
        </asp:ScriptManager>
        <input id="bMensajePorMostrar" type="hidden" value="0" name="bMensajePorMostrar" />
        <iframe id="iframeWSServer" style="z-index: 106; left: 8px; visibility: hidden; position: absolute; top: 200px"
            name="iframeWSServer" src="../blank.htm"></iframe>
        
        <div style="width:100%; position:fixed; z-index:100; background-color:white;">
            <uc1:menu runat="server" ID="controlMenu" Seccion="Detalle solicitud"></uc1:menu>                
            <fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
        </div>
        <div id="divAcciones" runat="server">
            <ignav:UltraWebMenu ID="uwPopUpAcciones" runat="server" WebMenuTarget="PopupMenu" SubMenuImage="ig_menuTri.gif"
                ScrollImageTop="ig_menu_scrollup.gif" Cursor="Default" ScrollImageBottomDisabled="ig_menu_scrolldown_disabled.gif" ScrollImageTopDisabled="ig_menu_scrollup_disabled.gif"
                ScrollImageBottom="ig_menu_scrolldown.gif" AutoPostBack="false">
                <MenuClientSideEvents InitializeMenu="" ItemChecked="" ItemClick="" SubMenuDisplay="" ItemHover=""></MenuClientSideEvents>
                <DisabledStyle ForeColor="LightGray"></DisabledStyle>
                <HoverItemStyle Cursor="Hand"></HoverItemStyle>
                <IslandStyle Cursor="Default" BorderWidth="1px" Font-Size="8pt" Font-Names="MS Sans Serif" BorderStyle="Outset" ForeColor="Black" BackColor="LightGray"></IslandStyle>
                <ExpandEffects ShadowColor="LightGray"></ExpandEffects>
                <TopSelectedStyle Cursor="Default"></TopSelectedStyle>
                <SeparatorStyle BackgroundImage="ig_menuSep.gif"
                    CustomRules="background-repeat:repeat-x; "></SeparatorStyle>
                <Levels>
                    <ignav:Level Index="0"></ignav:Level>
                </Levels>
            </ignav:UltraWebMenu>
        </div>
        <div>
            <asp:Label ID="lblCamposObligatorios" Style="z-index: 102; display: none;" runat="server" CssClass="captionRed">Los campos marcados con (*) son de obligada cumplimentacion</asp:Label>
        </div>
        <div id="divProgreso" style="display: inline">
            <table id="tblProgreso" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
                <tr style="height: 50px">
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <asp:TextBox ID="lblProgreso" runat="server" BorderStyle="None" BorderWidth="0" CssClass="captionBlue"
                            Width="100%" Style="text-align: center">Su solicitud está siendo tramitada. Espere unos instantes...</asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <asp:Image ID="imgProgreso" runat="server" src="../_common/images/cursor-espera_grande.gif"></asp:Image>
                    </td>
                </tr>
            </table>
        </div>
        <div id="divForm2" style="display:none; padding-top:13em;">
            <table cellspacing="3" cellpadding="3" width="100%">
                <tr>
                    <td width="100%" colspan="4">
                        <igtab:UltraWebTab ID="uwtGrupos" Style="z-index: 101" runat="server" BorderStyle="Solid" BorderWidth="1px"
                            Width="100%" EnableViewState="false" ThreeDEffect="False" DummyTargetUrl=" " FixedLayout="True"
                            CustomRules="padding:10px;" DisplayMode="Scrollable">
                            <DefaultTabStyle Height="24px" CssClass="uwtDefaultTab">
                                <Padding Left="20px" Right="20px"></Padding>
                            </DefaultTabStyle>
                            <RoundedImage NormalImage="ig_tab_blueb2.gif" HoverImage="ig_tab_blueb1.gif" FillStyle="LeftMergedWithCenter"></RoundedImage>
                            <ClientSideEvents InitializeTabs="initTab" />
                        </igtab:UltraWebTab>
                        <script type="text/javascript">
                            i = 0;
                            var bSalir = false;
                            while (bSalir == false) {
                                if (document.getElementById("uwtGrupos_div" + i)) {
                                    document.getElementById("uwtGrupos_div" + i).style.visibility = 'hidden';
                                    i = i + 1;
                                }
                                else {
                                    bSalir = true;
                                }
                            }
                            bSalir = null;
                        </script>
                    </td>
                </tr>
            </table>
            <input id="Solicitud" type="hidden" name="Solicitud" runat="server" />
            <input id="txtEnviar" type="hidden" name="Enviar" runat="server" />
            <input id="Bloque" type="hidden" name="Bloque" runat="server" />
            <input id="NEW_Instancia" type="hidden" name="NEW_Instancia" runat="server" />
            <div id="divAlta" style="visibility: hidden"></div>
            <div id="divDropDowns" style="z-index: 101; visibility: hidden; position: absolute; top: 300px"></div>
            <div id="divCalculados" style="z-index: 107; visibility: hidden; position: absolute; top: 0px"></div>
            <input id="cadenaespera" type="hidden" name="cadenaespera" runat="server" />
            <input id="BotonCalcular" type="hidden" value="0" name="BotonCalcular" runat="server" />
            <input id="PantallaMaper" type="hidden" name="PantallaMaper" runat="server" />
            <input id="Favorito" type="hidden" name="Favorito" runat="server" />
            <input id="IdFavorito" type="hidden" name="IdFavorito" runat="server" />
            <input id="IdInstanciaImportar" type="hidden" name="IdInstanciaImportar" runat="server" />
            <input id="PantallaVinculaciones" type="hidden" name="PantallaVinculaciones" runat="server" />
            <input id="hid_DesdeInicio" type="hidden" runat="server" />
        </div>
    </form>
    <div id="divForm3" style="display: none">
        <form id="frmCalculados" name="frmCalculados" action="recalcularimportes.aspx" method="post" target="fraWSServer">
        </form>
        <form id="frmDesglose" name="frmDesglose" method="post" target="winDesglose" action="">
        </form>
    </div>
    <form id="frmSubmitFav" name="frmSubmitFav" action="guardarSOLFavorita.aspx"
        target="fraWSServer" method="post" style="left: 0px; position: absolute; top: 0px">
        <input type="hidden" id="denFavorita" name="denFavorita" runat="server" />
    </form>
    <script type="text/javascript">
        HabilitarBotones();
        var monedaInput = $.map(arrInputs, function (x) { if (fsGeneralEntry_getById(x) != null) { if (fsGeneralEntry_getById(x).tipoGS == 102 && !fsGeneralEntry_getById(x).basedesglose) return fsGeneralEntry_getById(x); } })[0];
        if ((monedaInput) && (monedaInput.dataValue != '') && (monedaInput.dataValue != null)) {
            var nuevoTexto;
            $.each($('[nombreMoneda]'), function () {
                nuevoTexto = $(this).text().replace('()', '(' + monedaInput.dataValue + ')');
                $(this).text(nuevoTexto);
            });
        }
    </script>
</body>
</html>
