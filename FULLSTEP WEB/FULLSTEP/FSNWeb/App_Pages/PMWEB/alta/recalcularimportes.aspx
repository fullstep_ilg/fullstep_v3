<%@ Page Language="vb" AutoEventWireup="false" Codebehind="recalcularimportes.aspx.vb" Inherits="Fullstep.FSNWeb.recalcularimportes"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	<script type="text/javascript">		
	/*
	Descripcion: Pone el valor calculado en el campo Importe del formulario (En paginas de contratos)
	parametros entrada:
	cadena: cadena con el importe + el valor
	importe: el importe (numerico)
	*/
	function ponerValorTotalCalculadoContrato(importeConFormato, importe) {
		p = window.parent.frames["fraWSMain"]
		if (p)
			p.ponerCalculados(importeConFormato, importe)
	}

	/*
	Descripcion: Pone el valor calculado en el campo Importe del formulario
	parametros entrada:
	cadena: cadena con el importe + el valor
	*/
	function ponerValorTotalCalculado(cadena) {
	    p = window.parent.frames["fraWSMain"]
		if (p.lblImporte)
			p.lblImporte.innerHTML = cadena
	}

	function ponerValorCampoCalculado(campo, valor, tipo) {
	    p = window.parent.frames["fraWSMain"]
		if (tipo == 1) {
			oEntry = p.fsGeneralEntry_getById(campo)
			if (oEntry) {
				oEntry.setValue(valor)
			}
		}
		else {
			if (p.document.getElementById(campo)) {
				p.document.getElementById(campo).value = valor
				if (p.oWinDesglose != null) {
					if (!p.oWinDesglose.closed) {
						var sAux
						sAux = campo
						sAux = sAux.split("__")
						sAux = "ucDesglose_fsdsentry_" + sAux[sAux.length - 2] + "_" + sAux[sAux.length - 1]
						oEntry = p.oWinDesglose.fsGeneralEntry_getById(sAux)
						if (oEntry) {
							oEntry.setValue(valor)
						}
					}
				}

			}

		}

	}	
	</script>	
	<body>
		<form id="Form1" method="post" runat="server">
		</form>
	</body>
</html>
