Imports PMWeb.modUtilidades

Public Class recalcularimportes
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' <summary>
    ''' Calcular los campos calculados de desgloses y campos
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">evento de sistema</param>           
    ''' <remarks>Llamada desde: alta/alta.aspx    alta/NWAlta.aspx  certificados\altacertificado.aspx   certificados\detalleCertificado.aspx
    ''' noconformidad\altaNoconformidad.aspx      noconformidad\detalleNoConformidad.aspx       noconformidad\noconformidadRevisor.aspx
    ''' seguimiento\NWDetalleSolicitud.aspx       workflow\gestiontrasladada.aspx     workflow\NWgestionInstancia.aspx; Tiempo m�ximo: 0,3</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oSolicitud As FSNServer.Solicitud
        Dim oInstancia As FSNServer.Instancia
        Dim lIndexGrupos As Integer = 0
        Dim oDSRow As DataRow
        Dim sPre As String
        Dim oDS As DataSet
        Dim oDSDesglosePadreVisible As DataSet
        Dim oDSRowDesglosePadreVisible As DataRow
        Dim lInstancia As Integer = Request("CALC_Instancia")
        Dim sDesde As String = Request("desde") 'En principio se informa desde altacontrato.aspx
        Dim bEsQA As Boolean

        If lInstancia = Nothing Then
            oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
            oSolicitud.ID = Request("CALC_Solicitud")
            oSolicitud.Load(Idioma)

            bEsQA = (oSolicitud.TipoSolicit = TiposDeDatos.TipoDeSolicitud.Certificado _
                    OrElse oSolicitud.TipoSolicit = TiposDeDatos.TipoDeSolicitud.NoConformidad)

            If bEsQA Then
                oDS = oSolicitud.Formulario.LoadCamposCalculados(oSolicitud.ID)
            Else
                oDS = oSolicitud.Formulario.LoadCamposCalculados(oSolicitud.ID, FSNUser.CodPersona)
            End If
        Else
            oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
            oInstancia.ID = Request("CALC_Instancia")
            oInstancia.Cargar(Idioma)
            oInstancia.Version = Request("CALC_NumVersion")

            bEsQA = (oInstancia.Solicitud.TipoSolicit = TiposDeDatos.TipoDeSolicitud.Certificado _
                OrElse oInstancia.Solicitud.TipoSolicit = TiposDeDatos.TipoDeSolicitud.NoConformidad)

            If bEsQA Then
                oDS = oInstancia.LoadCamposCalculados(FSNUser.CodPersona, , False)
            Else
                oDS = oInstancia.LoadCamposCalculados(FSNUser.CodPersona)
            End If
        End If

        Dim oDSDesglose As DataSet
        Dim oDSRowDesglose As DataRow
        Dim oDSRowDesgloseOculto As DataRow
        Dim sVariables() As String
        Dim sVariablesDesglose() As String
        Dim dValues() As Double
        Dim dValuesDesglose() As Double
        Dim dValuesTotalDesglose() As Double
        Dim dTotalDesglose As Double
        Dim i As Integer
        Dim iDesglose As Integer

        ReDim sVariables(oDS.Tables(0).Rows.Count - 1)
        ReDim dValues(oDS.Tables(0).Rows.Count - 1)

        Dim iTipoGS As TiposDeDatos.TipoCampoGS
        Dim iTipo As TiposDeDatos.TipoGeneral
        Dim sRequest As String
        Dim vValor As Object
        Dim iTipoGSDesglose As TiposDeDatos.TipoCampoGS
        Dim iTipoDesglose As TiposDeDatos.TipoGeneral
        Dim iEq As New USPExpress.USPExpression

        i = 0
        Dim noinsertar As Integer = 0
        Dim cadenatotal As String
        Dim iIndices = 0
        Dim bEstaInvisibleElDesglose As Boolean
        Dim bSoloLectura As Boolean = False

        If Request("CALC_SoloLectura") <> "" Then
            bSoloLectura = Request("CALC_SoloLectura")
        End If
        Dim moneda As String
        If Request("CALC_MONEDA") IsNot Nothing AndAlso Request("CALC_MONEDA") <> "" Then
            moneda = Request("CALC_MONEDA")
        ElseIf lInstancia = Nothing Then
            'NC y Certif en alta. Ni CALC_MONEDA ni oInstancia.Moneda
            'Pm en alta. Habra CALC_MONEDA.
            moneda = ""
        Else
            moneda = oInstancia.Moneda
        End If
        Dim totalCabeceraActualizado As Boolean = False
        For Each oDSRow In oDS.Tables(0).Rows
            bEstaInvisibleElDesglose = False

            sPre = Request("CALC_txtPre_" + oDSRow.Item("GRUPO").ToString())

            sVariables(i) = oDSRow.Item("ID_CALCULO")

            iTipoGS = DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS"))
            iTipo = oDSRow.Item("SUBTIPO")

            If lInstancia = Nothing Then
                sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ID").ToString()
            Else
                sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ID_CAMPO").ToString()
            End If

            'Condiciones para coger el valor de los campos tipo "CALC_":
            'Si estamos en el alta y el campo est� visible
            'Si no estamos en el alta y el campo es de escritura
            'En los dem�s casos se coge el valor de VALOR_NUM.
            If Not bSoloLectura AndAlso ((oDSRow.Item("ESCRITURA") = 1 AndAlso Not (lInstancia = Nothing)) OrElse (oDSRow.Item("VISIBLE") = 1 AndAlso lInstancia = Nothing)) Then
                vValor = Numero(VacioToNothing(Request(sRequest)), ".", ",")
            Else
                vValor = oDSRow.Item("VALOR_NUM")
            End If

            If oDSRow.Item("TIPO") = TipoCampoPredefinido.Calculado Then
                If Not IsDBNull(oDSRow.Item("ORIGEN_CALC_DESGLOSE")) Then
                    Dim oCampo As FSNServer.Campo
                    oCampo = FSNServer.Get_Object(GetType(FSNServer.Campo))

                    If lInstancia = Nothing Then
                        oCampo.Id = oDSRow.Item("ORIGEN_CALC_DESGLOSE")
                        oDSDesglose = oCampo.LoadCalculados(oSolicitud.ID, IIf(bEsQA, Nothing, FSNUser.CodPersona))
                        oDSDesglosePadreVisible = oSolicitud.Formulario.LoadDesglosePadreVisible(oSolicitud.ID, oCampo.Id, FSNUser.CodPersona)
                    Else
                        oCampo.Id = DBNullToInteger(oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE"))
                        oDSDesglose = oCampo.LoadInstCalculados(oInstancia.ID, FSNUser.CodPersona)
                        oDSDesglosePadreVisible = oInstancia.LoadDesglosePadreVisible(oCampo.Id, FSNUser.CodPersona)
                    End If

                    ReDim sVariablesDesglose(oDSDesglose.Tables(0).Rows.Count - 1)
                    ReDim dValuesDesglose(oDSDesglose.Tables(0).Rows.Count - 1)
                    ReDim dValuesTotalDesglose(oDSDesglose.Tables(0).Rows.Count - 1)

                    Dim iNumRows As Integer
                    Dim lineasReal As Integer()
                    Dim bPopUp As Boolean
                    Dim bDesgloseEmergente As Boolean

                    dTotalDesglose = 0
                    iNumRows = 0
                    bEstaInvisibleElDesglose = True
                    If oDSDesglosePadreVisible.Tables(0).Rows.Count > 0 Then
                        For Each oDSRowDesglosePadreVisible In oDSDesglosePadreVisible.Tables(0).Rows
                            If oDSRowDesglosePadreVisible.Item("VISIBLE") = 1 Then
                                bEstaInvisibleElDesglose = False
                                Exit For
                            End If
                        Next
                    End If

                    'bEstaInvisibleElDesglose = (oDSRow.Item("VISIBLE") = 0)  Lo quito para que aunque el calculado de fuera del desglose este invisible recaculle
                    If bEstaInvisibleElDesglose = False Then
                        sPre = Request("CALC_txtPre_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString())
                        iNumRows = Request("CALC_" + sPre + "_tblDesglose") 'Filas de desglose que salen en pesta�a
                        bPopUp = False
                        bDesgloseEmergente = False
                        If iNumRows = Nothing Then  'El desglose es de tipo popup
                            If lInstancia = Nothing Then
                                iNumRows = Request("CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString + "_tblDesglose")
                                bDesgloseEmergente = False
                                If iNumRows = Nothing Then
                                    bDesgloseEmergente = True
                                    iNumRows = Request("CALC_" + sPre + "_fsentry" + oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString + "__numRows")
                                End If
                            Else 'Desglose no Emergente "CALC_uwtGrupos__ctl0xx_5491_5491_133510_tblDesglose"
                                iNumRows = Request("CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "_tblDesglose")
                                Dim conta As Integer = 1
                                If iNumRows <> Nothing Then
                                    bDesgloseEmergente = False
                                    ReDim lineasReal(iNumRows)
                                    For numLinea As Integer = 1 To iNumRows
                                        For campoCalc As Integer = 0 To oDSDesglose.Tables(0).Rows.Count - 1
                                            If Not Request("CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "_fsdsentry_" + numLinea.ToString + "_" + oDSDesglose.Tables(0).Rows(campoCalc).Item("ID_CAMPO").ToString) Is Nothing Then
                                                lineasReal(conta) = numLinea
                                                Exit For
                                            ElseIf Not Request("CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "_fsentry" + oDSDesglose.Tables(0).Rows(campoCalc).Item("ID_CAMPO").ToString) Is Nothing Then
                                                lineasReal(conta) = numLinea
                                                Exit For
                                            End If
                                        Next
                                        conta += 1
                                    Next
                                Else
                                    bDesgloseEmergente = True
                                    iNumRows = Request("CALC_" + sPre + "_fsentry" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "__numTotRows")
                                    ReDim lineasReal(iNumRows)
                                    For numLinea As Integer = 1 To iNumRows
                                        For campoCalc As Integer = 0 To oDSDesglose.Tables(0).Rows.Count - 1
                                            If Not Request("CALC_" + sPre + "_fsentry" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "__" + numLinea.ToString + "__" + oDSDesglose.Tables(0).Rows(campoCalc).Item("ID_CAMPO").ToString) Is Nothing Then
                                                lineasReal(conta) = numLinea
                                                Exit For
                                            End If
                                        Next
                                        conta += 1
                                    Next
                                End If
                            End If
                            bPopUp = True
                        Else
                            If lInstancia <> Nothing Then
                                ReDim lineasReal(iNumRows)
                                Dim conta As Integer = 1
                                For numLinea As Integer = 1 To iNumRows
                                    For campoCalc As Integer = 0 To oDSDesglose.Tables(0).Rows.Count - 1
                                        If Not Request("CALC_" + sPre + "_fsdsentry_" + numLinea.ToString + "_" + oDSDesglose.Tables(0).Rows(campoCalc).Item("ID_CAMPO").ToString) Is Nothing Then
                                            lineasReal(conta) = numLinea
                                            Exit For
                                        End If
                                    Next
                                    conta += 1
                                Next
                            End If
                        End If
                    Else
                        iNumRows = 0
                        If oDSDesglose.Tables(1).Rows.Count > 0 Then
                            For Each oDSRowDesgloseOculto In oDSDesglose.Tables(1).Rows
                                If oDSRowDesgloseOculto.Item("Linea") > iNumRows Then
                                    iNumRows = oDSRowDesgloseOculto.Item("Linea")
                                    bEstaInvisibleElDesglose = True
                                End If
                            Next
                        End If
                    End If
                    For k As Integer = 1 To iNumRows
                        If lInstancia = Nothing OrElse lineasReal.Contains(k) Then
                            iDesglose = 0

                            For Each oDSRowDesglose In oDSDesglose.Tables(0).Rows
                                sVariablesDesglose(iDesglose) = oDSRowDesglose.Item("ID_CALCULO")

                                iTipoGSDesglose = DBNullToSomething(oDSRowDesglose.Item("TIPO_CAMPO_GS"))
                                iTipoDesglose = oDSRow.Item("SUBTIPO")
                                vValor = Nothing

                                'Condiciones para coger el valor de los campos tipo "CALC_":
                                'Si estamos en el alta y el campo est� visible
                                'Si no estamos en el alta y el campo es de escritura
                                'En los dem�s casos se coge el valor de VALOR_NUM.
                                If (iTipoGSDesglose = TiposDeDatos.TipoCampoGS.PrecioUnitario AndAlso oDSRowDesglose.Item("VISIBLE") = 1) _
                                    OrElse (Not bSoloLectura _
                                            AndAlso ((oDSRowDesglose.Item("ESCRITURA") = 1 AndAlso Not (lInstancia = Nothing)) OrElse (oDSRowDesglose.Item("VISIBLE") = 1 AndAlso lInstancia = Nothing)) _
                                            AndAlso Not bEstaInvisibleElDesglose) Then
                                    If lInstancia = Nothing Then
                                        If bPopUp Then
                                            If bDesgloseEmergente Then
                                                sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ORIGEN_CALC_DESGLOSE") & "__" & (k).ToString() & "__" & oDSRowDesglose.Item("ID")
                                            Else
                                                'CALC_uwtGrupos__ctl0xx_623_623_10549_fsdsentry_1_10551
                                                sRequest = "CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID").ToString
                                            End If
                                        Else
                                            'CALC_uwtGrupos__ctl0x_146_fsdsentry_1_2283
                                            sRequest = "CALC_" + sPre + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID").ToString()
                                        End If
                                    Else
                                        If bPopUp Then
                                            If bDesgloseEmergente Then
                                                sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE") & "__" & (k).ToString() & "__" & oDSRowDesglose.Item("ID_CAMPO")

                                            Else
                                                'CALC_uwtGrupos__ctl0xx_5491_5491_133510_fsdsentry_1_133486
                                                sRequest = "CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID_CAMPO").ToString
                                            End If
                                        Else
                                            'CALC_uwtGrupos__ctl0x_146_fsdsentry_1_2283
                                            sRequest = "CALC_" + sPre + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID_CAMPO").ToString()
                                        End If
                                    End If
                                    vValor = Numero(VacioToNothing(Request(sRequest)), ".", ",")
                                Else
                                    Dim keysDesglose(2) As DataColumn
                                    Dim findDesglose(2) As Object
                                    Dim oRowDesglose As DataRow

                                    keysDesglose(0) = oDSDesglose.Tables(1).Columns("LINEA")
                                    keysDesglose(1) = oDSDesglose.Tables(1).Columns("CAMPO_PADRE")
                                    keysDesglose(2) = oDSDesglose.Tables(1).Columns("CAMPO_HIJO")

                                    oDSDesglose.Tables(1).PrimaryKey = keysDesglose

                                    Dim iLinea As Integer
                                    iLinea = k

                                    findDesglose(0) = (k).ToString()
                                    If lInstancia = Nothing Then
                                        findDesglose(1) = oDSRow.Item("ORIGEN_CALC_DESGLOSE")
                                        findDesglose(2) = oDSRowDesglose.Item("ID").ToString()
                                    Else
                                        findDesglose(1) = oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE")
                                        findDesglose(2) = oDSRowDesglose.Item("ID_CAMPO").ToString()
                                    End If
                                    oRowDesglose = oDSDesglose.Tables(1).Rows.Find(findDesglose)
                                    While oRowDesglose Is Nothing And iLinea > 0
                                        iLinea -= 1
                                        findDesglose(0) = iLinea

                                        oRowDesglose = oDSDesglose.Tables(1).Rows.Find(findDesglose)
                                    End While

                                    If Not oRowDesglose Is Nothing Then
                                        vValor = DBNullToSomething(oRowDesglose.Item("VALOR_NUM"))
                                    End If
                                End If

                                If oDSRowDesglose.Item("TIPO") = TipoCampoPredefinido.Calculado Then
                                    dValuesDesglose(iDesglose) = 0
                                Else
                                    dValuesDesglose(iDesglose) = Numero(vValor)
                                End If

                                dValuesTotalDesglose(iDesglose) += dValuesDesglose(iDesglose)
                                iDesglose += 1
                            Next
                            iDesglose = 0

                            For Each oDSRowDesglose In oDSDesglose.Tables(0).Rows
                                If oDSRowDesglose.Item("TIPO") = TipoCampoPredefinido.Calculado Then
                                    Try

                                        iEq.Parse(oDSRowDesglose.Item("FORMULA"), sVariablesDesglose)
                                        dValuesDesglose(iDesglose) = iEq.Evaluate(dValuesDesglose)
                                        If lInstancia = Nothing Then
                                            If bPopUp Then
                                                If bDesgloseEmergente Then
                                                    Page.ClientScript.RegisterStartupScript(Me.GetType(),oDSRowDesglose.Item("ID").ToString() + "_" + iIndices.ToString(), "<script>ponerValorCampoCalculado('" + sPre & "_fsentry" & oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString & "__" & (k).ToString() & "__" & oDSRowDesglose.Item("ID").ToString() + "'," + JSNum(dValuesDesglose(iDesglose)) + ",2);</script>")
                                                Else
                                                    Page.ClientScript.RegisterStartupScript(Me.GetType(),oDSRowDesglose.Item("ID").ToString() + "_" + iIndices.ToString(), "<script>ponerValorCampoCalculado('" + sPre & "_" & oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString() & "_" & oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString & "_fsdsentry_" & (k).ToString() & "_" & oDSRowDesglose.Item("ID").ToString() + "'," + JSNum(dValuesDesglose(iDesglose)) + ",1);</script>")
                                                End If
                                            Else
                                                Page.ClientScript.RegisterStartupScript(Me.GetType(),oDSRowDesglose.Item("ID").ToString() + "_" + iIndices.ToString(), "<script>ponerValorCampoCalculado('" + sPre & "_fsdsentry_" & (k).ToString() & "_" & oDSRowDesglose.Item("ID").ToString() + "'," + JSNum(dValuesDesglose(iDesglose)) + ",1);</script>")
                                            End If
                                            iIndices += 1
                                        Else
                                            If bPopUp Then
                                                If bDesgloseEmergente Then
                                                    Page.ClientScript.RegisterStartupScript(Me.GetType(),oDSRowDesglose.Item("ID").ToString() + "_" + iIndices.ToString(), "<script>ponerValorCampoCalculado('" + sPre & "_fsentry" & oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString & "__" & (k).ToString() & "__" & oDSRowDesglose.Item("ID_CAMPO").ToString() + "'," + JSNum(dValuesDesglose(iDesglose)) + ",2);</script>")
                                                Else
                                                    Page.ClientScript.RegisterStartupScript(Me.GetType(),oDSRowDesglose.Item("ID").ToString() + "_" + iIndices.ToString(), "<script>ponerValorCampoCalculado('" + sPre & "_" & oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString() & "_" & oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString & "_fsdsentry_" & (k).ToString() & "_" & oDSRowDesglose.Item("ID_CAMPO").ToString() + "'," + JSNum(dValuesDesglose(iDesglose)) + ",1);</script>")
                                                End If
                                            Else
                                                Page.ClientScript.RegisterStartupScript(Me.GetType(),oDSRowDesglose.Item("ID").ToString() + "_" + iIndices.ToString(), "<script>ponerValorCampoCalculado('" + sPre & "_fsdsentry_" & (k).ToString() & "_" & oDSRowDesglose.Item("ID_CAMPO").ToString() + "'," + JSNum(dValuesDesglose(iDesglose)) + ",1);</script>")
                                            End If
                                            iIndices += 1
                                        End If
                                        dValuesTotalDesglose(iDesglose) += dValuesDesglose(iDesglose)

                                    Catch ex As USPExpress.ParseException
                                    Catch ex0 As Exception
                                    End Try
                                End If
                                iDesglose += 1
                            Next
                            ReDim Preserve sVariablesDesglose(oDSDesglose.Tables(0).Rows.Count)
                            ReDim Preserve dValuesDesglose(oDSDesglose.Tables(0).Rows.Count)

                            sVariablesDesglose(oDSDesglose.Tables(0).Rows.Count) = oDSRow.Item("FORMULA") + " * 1"

                            Try
                                iEq.Parse(oDSRow.Item("FORMULA"), sVariablesDesglose)

                                dValuesDesglose(oDSDesglose.Tables(0).Rows.Count) = iEq.Evaluate(dValuesDesglose)
                                dTotalDesglose += dValuesDesglose(oDSDesglose.Tables(0).Rows.Count)
                                ReDim sVariablesDesglose(oDSDesglose.Tables(0).Rows.Count - 1)
                                ReDim dValuesDesglose(oDSDesglose.Tables(0).Rows.Count - 1)
                            Catch ex As USPExpress.ParseException
                            Catch ex0 As Exception
                            End Try
                        End If
                    Next

                    sPre = Request("CALC_txtPre_" + oDSRow.Item("GRUPO").ToString())
                    If Not IsDBNull(oDSRow.Item("FORMULA")) And iNumRows > 0 Then
                        Try
                            dTotalDesglose = iEq.Evaluate(dValuesTotalDesglose)
                            dValues(i) = dTotalDesglose
                            If lInstancia = Nothing Then
                                Page.ClientScript.RegisterStartupScript(Me.GetType(),oDSRow.Item("ID").ToString(), "<script>ponerValorCampoCalculado('" + sPre & "_fsentry" & oDSRow.Item("ID").ToString() + "'," + JSNum(dValues(i)) + ",1);</script>")
                                If DBNullToSomething(oDSRow.Item("ES_IMPORTE_TOTAL")) = 1 AndAlso DBNullToSomething(oDSRow.Item("ES_SUBCAMPO")) = 0 Then
                                    If sDesde = "contratos" Then
                                        cadenatotal = "Importe" & ":" & FSNLibrary.FormatNumber(dValues(i), FSNUser.NumberFormat) & " "
                                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "PonerImporte1", "<script>ponerValorTotalCalculadoContrato('" + FSNLibrary.FormatNumber(dValues(i), FSNUser.NumberFormat) + "','" & dValues(i) & "');</script>")
                                    End If
                                End If
                            Else
                                Page.ClientScript.RegisterStartupScript(Me.GetType(),oDSRow.Item("ID").ToString(), "<script>ponerValorCampoCalculado('" + sPre & "_fsentry" & oDSRow.Item("ID_CAMPO").ToString() + "'," + JSNum(dValues(i)) + ",1);</script>")

                                If DBNullToSomething(oDSRow.Item("ES_IMPORTE_TOTAL")) = 1 AndAlso DBNullToSomething(oDSRow.Item("ES_SUBCAMPO")) = 0 Then
                                    If sDesde = "contratos" Then
                                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "PonerImporte1", "<script>ponerValorTotalCalculadoContrato('" + FSNLibrary.FormatNumber(dValues(i), FSNUser.NumberFormat) + "','" & dValues(i) & "');</script>")
                                    Else
                                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "PonerImporte1", "<script>ponerValorTotalCalculado('" + FSNLibrary.FormatNumber(dValues(i), FSNUser.NumberFormat) & " " & moneda + "');</script>")
                                        totalCabeceraActualizado = True
                                    End If
                                End If
                            End If
                        Catch ex As USPExpress.ParseException
                        Catch ex0 As Exception
                        End Try
                    End If
                Else
                    dValues(i) = 0
                End If
            Else
                If IsDBNull(vValor) Then
                    dValues(i) = 0
                Else
                    dValues(i) = Numero(vValor)
                End If
            End If
            i += 1
        Next
        i = 0

        For Each oDSRow In oDS.Tables(0).Rows
            sPre = Request("CALC_txtPre_" + oDSRow.Item("GRUPO").ToString())
            If oDSRow.Item("TIPO") = TipoCampoPredefinido.Calculado Then
                Try
                    iEq.Parse(oDSRow.Item("FORMULA"), sVariables)
                    dValues(i) = iEq.Evaluate(dValues)
                    oDSRow.Item("VALOR_NUM") = dValues(i)
                    If lInstancia = Nothing Then
                        Page.ClientScript.RegisterStartupScript(Me.GetType(),oDSRow.Item("ID").ToString(), "<script>ponerValorCampoCalculado('" + sPre & "_fsentry" & oDSRow.Item("ID").ToString() + "'," + JSNum(dValues(i)) + ",1);</script>")

                        If DBNullToSomething(oDSRow.Item("ES_IMPORTE_TOTAL")) = 1 AndAlso DBNullToSomething(oDSRow.Item("ES_SUBCAMPO")) = 0 Then
                            If sDesde = "contratos" Then
                                Page.ClientScript.RegisterStartupScript(Me.GetType(), "PonerImporte3", "<script>ponerValorTotalCalculadoContrato('" + FSNLibrary.FormatNumber(dValues(i), FSNUser.NumberFormat) + "','" & dValues(i) & "');</script>")
                            End If
                        End If
                    Else
                        Page.ClientScript.RegisterStartupScript(Me.GetType(),oDSRow.Item("ID").ToString(), "<script>ponerValorCampoCalculado('" + sPre & "_fsentry" & oDSRow.Item("ID_CAMPO").ToString() + "'," + JSNum(dValues(i)) + ",1);</script>")

                        If lInstancia <> Nothing Then
                            If DBNullToSomething(oDSRow.Item("ES_IMPORTE_TOTAL")) = 1 AndAlso DBNullToSomething(oDSRow.Item("ES_SUBCAMPO")) = 0 Then
                                If sDesde = "contratos" Then
                                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "PonerImporte3", "<script>ponerValorTotalCalculadoContrato('" + FSNLibrary.FormatNumber(dValues(i), FSNUser.NumberFormat) + "','" & dValues(i) & "');</script>")
                                Else
                                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "PonerImporte2", "<script>ponerValorTotalCalculado('" + FSNLibrary.FormatNumber(dValues(i), FSNUser.NumberFormat) & " " & moneda + "');</script>")
                                    totalCabeceraActualizado = True
                                End If
                            End If
                        End If
                    End If
                Catch ex As USPExpress.ParseException
                Catch ex0 As Exception
                End Try
            End If
            i += 1
        Next
        If Not totalCabeceraActualizado AndAlso lInstancia <> Nothing AndAlso sDesde <> "contratos" Then
            Page.ClientScript.RegisterStartupScript(Me.GetType(),"PonerImporte3", "<script>ponerValorTotalCalculado('" + FSNLibrary.FormatNumber(oInstancia.Importe, FSNUser.NumberFormat) & " " & moneda + "');</script>")
        End If
    End Sub

End Class

