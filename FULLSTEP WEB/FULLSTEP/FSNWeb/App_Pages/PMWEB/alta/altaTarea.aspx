﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="altaTarea.aspx.vb" Inherits="Fullstep.FSNWeb.altaTarea" %>
<%@ Register Assembly="Infragistics.WebUI.WebDataInput.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.WebDataInput" TagPrefix="igtxt" %>
<%@ Register TagPrefix="fsde" Namespace="Fullstep.DataEntry" Assembly="DataEntry" %>
<%@ Register TagPrefix="igtbl" Namespace="Infragistics.WebUI.UltraWebGrid" Assembly="Infragistics.WebUI.UltraWebGrid.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
	<title></title>
	<style type="text/css">
		.style1
		{
			width: 451px;
		}
		.igdd_DropDownListContainer
		{
			border: 1px solid #BBBBBB;
			background-color: #FFFFFF;
			width:300px;
		}
	   
	</style>  
</head>
<script language="javascript" type="text/javascript" >
	function CambiarNumLinea(numOld, numNew) {
		var oNomGrid = document.getElementById("<%=hid_NomGrid.ClientID%>")
		var oLinea = document.getElementById("<%=hid_linea_grid.ClientID%>")
		var mainWindow = window.opener;
		var grid = mainWindow.igtbl_getGridById(oNomGrid.value);
			 
		   
			 
		var x = 0;
		for (x = 0; x <= grid.Rows.length - 1; x++) {
			var row = grid.Rows.getRow(x);
			var celda = row.getCellFromKey("NUM");
			if (celda.getValue() == numNew) {
				celda.setValue(numOld)
			}
		}

	}

	//funcion que comprueba que se hayan completado los campos que son obligatorios para dar de alta o modificar una tarea
	function ComprobarObligatorios() {
		var oObligatorio = document.getElementById("<%=hid_Obligatorio.ClientID%>")
		var oNumFases = document.getElementById("<%=hid_NumFases.ClientID%>")

		var Num = igedit_getById("<%=txtNumLinea.ClientID%>").text
		if (Num) {
			Num = Num.replace(/^\s*|\s*$/g, ""); //es como la funcion TRIM
			if (Num == '')
				return false;
		} else
		return false
			 
			 
		var textDescTarea = document.getElementById("<%=txtDescTarea.ClientID%>").value
		textDescTarea = textDescTarea.replace(/^\s*|\s*$/g, "");
		if (textDescTarea == '')
			return false;

		var textCodTarea = document.getElementById("<%=txtCodTarea.ClientID%>").value
		textCodTarea = textCodTarea.replace(/^\s*|\s*$/g, "");
		if (textCodTarea == '')
			return false;

		var dteFechaInicio = fsGeneralEntry_getById('<%= dteFechaInicio.clientID %>')
		var dteFechaFin = fsGeneralEntry_getById("<%=dteFechaFin.ClientID%>")
		var chkHito = document.getElementById("<%=chkHito.ClientID%>")
		if (oObligatorio.value == 1) {

			if (dteFechaInicio.getValue() == 'Null' || dteFechaInicio.getValue() == '' || dteFechaInicio.getValue() == null)
				return false
				 
					
			if (chkHito.checked == false) {
			//Si no es hito se comprobara
				if (dteFechaFin.getValue() == 'Null' || dteFechaFin.getValue() == '' || dteFechaFin.getValue() == null)
					return false
				var txtHorasEst = igedit_getById("<%=txtHorasEst.ClientID%>")
				if (txtHorasEst) {
					if (txtHorasEst.value == null)
						return false;
				} else
					return false
			}
			 

	}
		 
			 
		switch (oNumFases.value) {
			case '1':
			{
				var textCodNiv1 = document.getElementById("<%=txtCodNivel1.ClientID%>").value
				textCodNiv1 = textCodNiv1.replace(/^\s*|\s*$/g, "");
				if (textCodNiv1 == '')
					return false;

				var textDenNiv1 = document.getElementById("<%=txtDescNivel1.ClientID%>").value
				textDenNiv1 = textDenNiv1.replace(/^\s*|\s*$/g, "");
				if (textDenNiv1 == '')
					return false;
						  
				break 
			}
			case '2':
				{
					var textCodNiv1 = document.getElementById("<%=txtCodNivel1.ClientID%>").value
					textCodNiv1 = textCodNiv1.replace(/^\s*|\s*$/g, "");
					if (textCodNiv1 == '')
						return false;

					var textDenNiv1 = document.getElementById("<%=txtDescNivel1.ClientID%>").value
					textDenNiv1 = textDenNiv1.replace(/^\s*|\s*$/g, "");
					if (textDenNiv1 == '')
						return false;

					var textCodNiv2 = document.getElementById("<%=txtCodNivel2.ClientID%>").value
					textCodNiv2 = textCodNiv2.replace(/^\s*|\s*$/g, "");
					if (textCodNiv2 == '')
						return false;

					var textDenNiv2 = document.getElementById("<%=txtDescNivel2.ClientID%>").value
					textDenNiv2 = textDenNiv2.replace(/^\s*|\s*$/g, "");
					if (textDenNiv2 == '')
						return false;
					break
			}
			case '3':
				{
					var textCodNiv1 = document.getElementById("<%=txtCodNivel1.ClientID%>").value
					textCodNiv1 = textCodNiv1.replace(/^\s*|\s*$/g, "");
					if (textCodNiv1 == '')
						return false;

					var textDenNiv1 = document.getElementById("<%=txtDescNivel1.ClientID%>").value
					textDenNiv1 = textDenNiv1.replace(/^\s*|\s*$/g, "");
					if (textDenNiv1 == '')
						return false;

					var textCodNiv2 = document.getElementById("<%=txtCodNivel2.ClientID%>").value
					textCodNiv2 = textCodNiv2.replace(/^\s*|\s*$/g, "");
					if (textCodNiv2 == '')
						return false;

					var textDenNiv2 = document.getElementById("<%=txtDescNivel2.ClientID%>").value
					textDenNiv2 = textDenNiv2.replace(/^\s*|\s*$/g, "");
					if (textDenNiv2 == '')
						return false;

					var textCodNiv3 = document.getElementById("<%=txtCodNivel3.ClientID%>").value
					textCodNiv3 = textCodNiv3.replace(/^\s*|\s*$/g, "");
					if (textCodNiv3 == '')
						return false;

					var textDenNiv3 = document.getElementById("<%=txtDescNivel3.ClientID%>").value
					textDenNiv3 = textDenNiv3.replace(/^\s*|\s*$/g, "");
					if (textDenNiv3 == '')
						return false;
					break

			}
			case '4':
				{
					var textCodNiv1 = document.getElementById("<%=txtCodNivel1.ClientID%>").value
					textCodNiv1 = textCodNiv1.replace(/^\s*|\s*$/g, "");
					if (textCodNiv1 == '')
						return false;

					var textDenNiv1 = document.getElementById("<%=txtDescNivel1.ClientID%>").value
					textDenNiv1 = textDenNiv1.replace(/^\s*|\s*$/g, "");
					if (textDenNiv1 == '')
						return false;

					var textCodNiv2 = document.getElementById("<%=txtCodNivel2.ClientID%>").value
					textCodNiv2 = textCodNiv2.replace(/^\s*|\s*$/g, "");
					if (textCodNiv2 == '')
						return false;

					var textDenNiv2 = document.getElementById("<%=txtDescNivel2.ClientID%>").value
					textDenNiv2 = textDenNiv2.replace(/^\s*|\s*$/g, "");
					if (textDenNiv2 == '')
						return false;

					var textCodNiv3 = document.getElementById("<%=txtCodNivel3.ClientID%>").value
					textCodNiv3 = textCodNiv3.replace(/^\s*|\s*$/g, "");
					if (textCodNiv3 == '')
						return false;

					var textDenNiv3 = document.getElementById("<%=txtDescNivel3.ClientID%>").value
					textDenNiv3 = textDenNiv3.replace(/^\s*|\s*$/g, "");
					if (textDenNiv3 == '')
						return false;

					var textCodNiv4 = document.getElementById("<%=txtCodNivel4.ClientID%>").value
					textCodNiv4 = textCodNiv4.replace(/^\s*|\s*$/g, "");
					if (textCodNiv4 == '')
						return false;

					var textDenNiv4 = document.getElementById("<%=txtDescNivel4.ClientID%>").value
					textDenNiv4 = textDenNiv4.replace(/^\s*|\s*$/g, "");
					if (textDenNiv4 == '')
						return false;
					break
			}
	}
	if (chkHito.checked == false ) {
		if (dteFechaFin.getValue() != null && dteFechaInicio.getValue() != null) {
				var fecI = dteFechaInicio.getValue()
				var fecF = dteFechaFin.getValue()
				if (fecI > fecF) return 1
			}
		}
		
	}

	//funcion que valida que se inserten las fases correctamente
	function Validar(numFases) {
	var textCodNivel1 = document.getElementById("<%=txtCodNivel1.ClientID%>")
	var textCodNivel2 = document.getElementById("<%=txtCodNivel2.ClientID%>")
	var textCodNivel3 = document.getElementById("<%=txtCodNivel3.ClientID%>")
	var textCodNivel4 = document.getElementById("<%=txtCodNivel4.ClientID%>")
	var textCodTarea = document.getElementById("<%=txtCodTarea.ClientID%>")
	var ArrLineas = window.opener.lineasDesglose.split(";");
	var NewTarea = '';
	switch (numFases) {
			case '1':
				{
				//Valido la Tarea
					for (z = 0; z <= ArrLineas.length - 1; z++) {
						if (ArrLineas[z].indexOf(textCodTarea.value) != -1 && ArrLineas[z].split("#")[1].length == textCodTarea.value.length) {
							var arrFases = ArrLineas[z].split("#");
							var Cod1 = arrFases[0]
							if (textCodNivel1.value!=Cod1)
							return 'error'
						}
					}

					//Valido que la tarea no exista
					NewTarea = textCodNivel1.value  + '#' + textCodTarea.value
					if (window.opener.lineasDesglose.indexOf(NewTarea) != -1)
						return 'existe'
						 
					if (window.opener.lineasDesglose == '')
						window.opener.lineasDesglose = textCodNivel1.value +  + textCodTarea.value
					else
						window.opener.lineasDesglose = window.opener.lineasDesglose + ";" + textCodNivel1.value + '#' + textCodTarea.value
					break
				}
			case '2':
				{
						 
					//Valido el Nivel2
					for (z = 0; z <= ArrLineas.length - 1; z++) {
						if (ArrLineas[z].indexOf(textCodNivel2.value) != -1 && ArrLineas[z].split("#")[1].length == textCodNivel2.value.length) {
							var arrFases = ArrLineas[z].split("#");
							var Cod1 = arrFases[0]
							if (textCodNivel1.value!=Cod1)
							return 'error'
						}
					}
					//Valido la Tarea
					for (z = 0; z <= ArrLineas.length - 1; z++) {
						if (ArrLineas[z].indexOf(textCodTarea.value) != -1 && ArrLineas[z].split("#")[2].length == textCodTarea.value.length) {
							var arrFases = ArrLineas[z].split("#");
							var Cod2 = arrFases[1]
							if (textCodNivel2.value!=Cod2)
							return 'error'
						}
					}

					//Valido que la tarea no exista
					NewTarea = textCodNivel1.value + '#' + textCodNivel2.value + '#' + textCodTarea.value
					if (window.opener.lineasDesglose.indexOf(NewTarea) != -1)
					return 'existe'
						 
					if (window.opener.lineasDesglose == '')
						window.opener.lineasDesglose = textCodNivel1.value + '#' + textCodNivel2.value + '#' + textCodTarea.value
					else
						window.opener.lineasDesglose = window.opener.lineasDesglose + ";" + textCodNivel1.value + '#' + textCodNivel2.value + '#' + textCodTarea.value
					break
				}
			case '3':
				{
					//Valido el Nivel2
					for (z = 0; z <= ArrLineas.length - 1; z++) {
						if (ArrLineas[z].indexOf(textCodNivel2.value) != -1 && ArrLineas[z].split("#")[1].length == textCodNivel2.value.length) {
							var arrFases = ArrLineas[z].split("#");
							var Cod1 = arrFases[0]
							if (textCodNivel1.value != Cod1)
								return 'error'
						}
					}
					//Valido el Nivel3
					for (z = 0; z <= ArrLineas.length - 1; z++) {
						if (ArrLineas[z].indexOf(textCodNivel3.value) != -1 && ArrLineas[z].split("#")[2].length == textCodNivel3.value.length) {
							var arrFases = ArrLineas[z].split("#");
							var Cod2 = arrFases[1]
							if (textCodNivel2.value != Cod2)
								return 'error'
						}
					}
					//Valido la Tarea
					for (z = 0; z <= ArrLineas.length - 1; z++) {
						if (ArrLineas[z].indexOf(textCodTarea.value) != -1 && ArrLineas[z].split("#")[3].length == textCodNivel4.value.length) {
							var arrFases = ArrLineas[z].split("#");
							var Cod3 = arrFases[2]
							if (textCodNivel3.value != Cod3)
								return 'error'
						}
					}

					//Valido que la tarea no exista
					NewTarea = textCodNivel1.value + '#' + textCodNivel2.value + '#' + textCodNivel3.value + '#' + textCodTarea.value
					if (window.opener.lineasDesglose.indexOf(NewTarea) != -1)
						return 'existe'

					if (window.opener.lineasDesglose == '')
						window.opener.lineasDesglose = textCodNivel1.value + '#' + textCodNivel2.value + '#' + textCodNivel3.value + '#' + textCodTarea.value
					else
						window.opener.lineasDesglose = window.opener.lineasDesglose + ";" + textCodNivel1.value + '#' + textCodNivel2.value + '#' + textCodNivel3.value + '#' + textCodTarea.value
					break
			}
		case '4':
			{

				//Valido el Nivel2
				for (z = 0; z <= ArrLineas.length - 1; z++) {
					if (ArrLineas[z].indexOf(textCodNivel2.value) != -1 && ArrLineas[z].split("#")[1].length == textCodNivel2.value.length) {
						var arrFases = ArrLineas[z].split("#");
						var Cod1 = arrFases[0]
						if (textCodNivel1.value != Cod1)
							return 'error'
					}
				}
				//Valido el Nivel3
				for (z = 0; z <= ArrLineas.length - 1; z++) {
					if (ArrLineas[z].indexOf(textCodNivel3.value) != -1 && ArrLineas[z].split("#")[2].length == textCodNivel3.value.length) {
						var arrFases = ArrLineas[z].split("#");
						var Cod2 = arrFases[1]
						if (textCodNivel2.value != Cod2)
							return 'error'
					}
				}
				//Valido el Nivel4
				for (z = 0; z <= ArrLineas.length - 1; z++) {
					if (ArrLineas[z].indexOf(textCodNivel4.value) != -1 && ArrLineas[z].split("#")[3].length == textCodNivel3.value.length) {
						var arrFases = ArrLineas[z].split("#");
						var Cod3 = arrFases[2]
						if (textCodNivel3.value != Cod3)
							return 'error'
					}
				}
				//Valido la Tarea
				for (z = 0; z <= ArrLineas.length - 1; z++) {
					if (ArrLineas[z].indexOf(textCodTarea.value) != -1 && ArrLineas[z].split("#")[4].length == textCodTarea.value.length) {
						var arrFases = ArrLineas[z].split("#");
						var Cod4 = arrFases[3]
						if (textCodNivel4.value != Cod4)
							return 'error'
					}
				}

				//Valido que la tarea no exista
				NewTarea = textCodNivel1.value + '#' + textCodNivel2.value + '#' + textCodNivel3.value + '#' + textCodNivel4.value + '#' + textCodTarea.value
				if (window.opener.lineasDesglose.indexOf(NewTarea) != -1)
					return 'existe'
						
				if (window.opener.lineasDesglose == '')
					window.opener.lineasDesglose = textCodNivel1.value + '#' + textCodNivel2.value + '#' + textCodNivel3.value + '#' + textCodNivel4.value + '#' + textCodTarea.value
				else
					window.opener.lineasDesglose = window.opener.lineasDesglose + ";" + textCodNivel1.value + '#' + textCodNivel2.value + '#' + textCodNivel3.value + '#' + textCodNivel4.value + '#' + textCodTarea.value
				break
			}
		}
		return ''
	}
		 
	//funcion que actualiza la variable LineasDesglose cuando se modifica una tarea
	function ActualizarLineasDesglose(numFases) {
		window.opener.lineasDesglose = ''
		var textCodNivel1 = document.getElementById("<%=txtCodNivel1.ClientID%>")
		var textCodNivel2 = document.getElementById("<%=txtCodNivel2.ClientID%>")
		var textCodNivel3 = document.getElementById("<%=txtCodNivel3.ClientID%>")
		var textCodNivel4 = document.getElementById("<%=txtCodNivel4.ClientID%>")
		var textCodTarea = document.getElementById("<%=txtCodTarea.ClientID%>")
		var oLinea = document.getElementById("<%=hid_linea_grid.ClientID%>")
		var oNomGrid = document.getElementById("<%=hid_NomGrid.ClientID%>")
		var mainWindow = window.opener;
		var grid = mainWindow.igtbl_getGridById(oNomGrid.value);



		var x = 0;
		for (x = 0; x <= grid.Rows.length - 1; x++) {
			var row = grid.Rows.getRow(x);
			var celda = row.getCellFromKey("STATUS");
			if ((celda.getValue() == 'A' || celda.getValue() == 'M') && x != parseInt(oLinea.value)) {
				switch (numFases) {
					case '1':
						{
							if (window.opener.lineasDesglose == '')
								window.opener.lineasDesglose = row.getCellFromKey("COD_NIV4").getValue() + '#' + row.getCellFromKey("CODIGO").getValue()
							else
								window.opener.lineasDesglose = window.opener.lineasDesglose + ";" + row.getCellFromKey("COD_NIV4").getValue() + '#' + row.getCellFromKey("CODIGO").getValue()
								
							break
						}
					case '2':
						{
							if (window.opener.lineasDesglose == '')
								window.opener.lineasDesglose = row.getCellFromKey("COD_NIV3").getValue() + '#' + row.getCellFromKey("COD_NIV4").getValue() + '#' + row.getCellFromKey("CODIGO").getValue()
							else
								window.opener.lineasDesglose = window.opener.lineasDesglose + ";" + row.getCellFromKey("COD_NIV3").getValue() + '#' + row.getCellFromKey("COD_NIV4").getValue() + '#' + row.getCellFromKey("CODIGO").getValue()
								 
							break
						}
					case '3':
						{
							if (window.opener.lineasDesglose == '')
								window.opener.lineasDesglose = row.getCellFromKey("COD_NIV2").getValue() + '#' + row.getCellFromKey("COD_NIV3").getValue() + '#' + row.getCellFromKey("COD_NIV4").getValue() + '#' + row.getCellFromKey("CODIGO").getValue()
							else
								window.opener.lineasDesglose = window.opener.lineasDesglose + ";" + row.getCellFromKey("COD_NIV2").getValue() + '#' + row.getCellFromKey("COD_NIV3").getValue() + '#' + row.getCellFromKey("COD_NIV4").getValue() + '#' + row.getCellFromKey("CODIGO").getValue()
							break
						}

					case '4':
						{
							if (window.opener.lineasDesglose == '')
								window.opener.lineasDesglose = row.getCellFromKey("COD_NIV1").getValue() + '#' + row.getCellFromKey("COD_NIV2").getValue() + '#' + row.getCellFromKey("COD_NIV3").getValue() + '#' + row.getCellFromKey("COD_NIV4").getValue() + '#' + row.getCellFromKey("CODIGO").getValue()
							else
								window.opener.lineasDesglose = window.opener.lineasDesglose + ";" + row.getCellFromKey("COD_NIV1").getValue() + '#' + row.getCellFromKey("COD_NIV2").getValue() + '#' + row.getCellFromKey("COD_NIV3").getValue() + '#' + row.getCellFromKey("COD_NIV4").getValue() + '#' + row.getCellFromKey("CODIGO").getValue()
							break
						}
				}
			}
		}
	}
		 
	//funcion que inserta o modifica la fila 
	function InsertRow() {

		var resp;
		resp=ComprobarObligatorios()
		if (resp == false) {
			alert(sCamposOblig)
			return false
		}else{
		if (resp == 1) {
			alert(sFechas)
			return false
		}
		}
		var oNumFases = document.getElementById("<%=hid_NumFases.ClientID%>")
		var hid_Edicion = document.getElementById("<%=hid_Edicion.ClientID%>")
			 
		if (parseInt(hid_Edicion.value) != 0) { //Es una edicion y se tiene que modificar la variable LineasDesglose para una validacion
			ActualizarLineasDesglose(oNumFases.value)
		}
			 
			 
		resp = Validar(oNumFases.value)
		if (resp == 'error') {
			alert(window.opener.sErrorFases)
			return false
		}
		if (resp == 'existe') {
			alert(window.opener.sTareaExiste)
			return false
		} 
			 
	//Recogemos los controles de la pantalla
		var oNomGrid = document.getElementById("<%=hid_NomGrid.ClientID%>")
			 
		var hid_NomFilas = document.getElementById("<%=hid_NomElimFilas.ClientID%>")
			 
		var textDescNivel1 = document.getElementById("<%=txtDescNivel1.ClientID%>")
		var textDescNivel2 = document.getElementById("<%=txtDescNivel2.ClientID%>")
		var textDescNivel3 = document.getElementById("<%=txtDescNivel3.ClientID%>")
		var textDescNivel4 = document.getElementById("<%=txtDescNivel4.ClientID%>")
		var textCodNivel1 = document.getElementById("<%=txtCodNivel1.ClientID%>")
		var textCodNivel2 = document.getElementById("<%=txtCodNivel2.ClientID%>")
		var textCodNivel3 = document.getElementById("<%=txtCodNivel3.ClientID%>")
		var textCodNivel4 = document.getElementById("<%=txtCodNivel4.ClientID%>")
		var textDescTarea = document.getElementById("<%=txtDescTarea.ClientID%>")
		var textCodTarea = document.getElementById("<%=txtCodTarea.ClientID%>")
		var dteFechaInicio = fsGeneralEntry_getById('<%= dteFechaInicio.clientID %>')
		var dteFechaFin = fsGeneralEntry_getById("<%=dteFechaFin.ClientID%>")
		var txtHorasEst = igedit_getById("<%=txtHorasEst.ClientID%>")
		var txtGradoAvance = igedit_getById("<%=txtGradoAvance.ClientID%>")
		var wddEstado = $find("<%=wddEstado.ClientID%>")
		var txtObs = document.getElementById("<%=txtObs.ClientID%>")
		var txtNum = igedit_getById("<%=txtNumLinea.ClientID%>")
		var chkHito = document.getElementById("<%=chkHito.ClientID%>")

		var mainWindow = window.opener;
		var oElimFilas = mainWindow.document.getElementById(hid_NomFilas.value)
		oElimFilas.style.visibility = "visible"
		var grid = mainWindow.igtbl_getGridById(oNomGrid.value);
		var numLineas = grid.Rows.length

		var Textos = sTextos
		var arrTextos = Textos.split('#')

		var HitoHTML = "<div><IMG ID='imgHito' border=0 alt=" + arrTextos[2] + " title=" + arrTextos[2] + " align=middle  src='" + sRutaPM + "alta/images/Hito.gif' /> </div>";

		//Cambio la configuracion de edicion del grid
		var currentSettings = grid.AllowAddNew;
		grid.AllowAddNew = 1;
		grid.AllowUpdate = 1;
			 
		if (parseInt(hid_Edicion.value) == 0) { //ALTA DE TAREA
			var EditarHTML = "<TABLE style='WIDTH: 100%; DISPLAY: inline' cellSpacing=0 cellPadding=0><TR><TD><IMG id=idEditar onclick=Editar(" + numLineas + ") border=0 alt=" + arrTextos[1] + " title=" + arrTextos[1] + " align=middle src='" + sRutaPM + "alta/images/Lapiz.png'></TD></TR></TBODY></TABLE>";

			var UsuariosHTML = "<TABLE style='WIDTH: 100%;' cellSpacing=0 cellPadding=0><TR><TD><SPAN style='WIDTH: 220px; heigth: 15px DISPLAY: inline-block; filter: alpha(opacity = 0) ; opacity: 0 ;font-size:8pt; font-family:Verdana; color: #494949 ' id=lblUsuarios>texto</SPAN> </TD><TD style='FONT-SIZE: 1pt' onclick=AbrirUsuarios(" + numLineas + ") class=fsstyentrydd vAlign=center width=12 align=middle><IMG id=btnUsuarios  border=0 alt=dAbrirUsuarios align=middle src='" + sRutaPM + "_common/images/trespuntos.gif'> </TD></TR></TABLE>";

			var EliminarHTML = "<div><IMG ID='imgEliminar' border=0 onclick=Eliminar(" + numLineas + ") alt=" + arrTextos[0] + " title=" + arrTextos[0] + " align=middle  src='" + sRutaPM + "alta/images/Eliminar.gif' /> </div>";

			//Recojo la nueva fila
			mainWindow.igtbl_addNew(oNomGrid.value, 0);
			var Newrow = grid.Rows.getRow(grid.Rows.length - 1)

			mainWindow.sNumLineas = mainWindow.sNumLineas + 1
				 
			var NewCelda = Newrow.getCellFromKey("EDITAR");
			var celdaHtml = NewCelda.getElement()
			celdaHtml.innerHTML = EditarHTML

			var NewCelda = Newrow.getCellFromKey("USUARIOS");
			var celdaHtml = NewCelda.getElement()
			celdaHtml.innerHTML = UsuariosHTML

			var NewCelda = Newrow.getCellFromKey("ELIMINAR");
			var celdaHtml = NewCelda.getElement()
			celdaHtml.innerHTML = EliminarHTML

			var NewCelda = Newrow.getCellFromKey("STATUS");
			NewCelda.setValue('A') //Indicamos que es un alta de tarea

			var NewCelda = Newrow.getCellFromKey("ID");
			NewCelda.setValue(window.opener.sIdProyecto)

			var NewCelda = Newrow.getCellFromKey("EMP");
			NewCelda.setValue(0)

			var CeldaNumOld = Newrow.getCellFromKey("NUM_OLD")
			CeldaNumOld.setValue(txtNum.value)

			if (txtObs.value != '')
				var ComentarioHTML = "<div><IMG ID='imgComentario' border=0 alt=" + arrTextos[3] + " title=" + arrTextos[3] + " align=middle onclick=AbrirComentarios(" + numLineas + ")  src='" + sRutaPM + "alta/images/SolicitudesPmSmall.gif' /> </div>";
			else
				var ComentarioHTML = "<div><IMG ID='imgComentario' border=0 alt=" + arrTextos[3] + " title=" + arrTextos[3] + " align=middle  width=0  src='" + sRutaPM + "alta/images/SolicitudesPmSmall.gif' /> </div>";

		} else {
		//Recojo la fila a editar
			var oLinea = document.getElementById("<%=hid_linea_grid.ClientID%>")
			var Newrow = grid.Rows.getRow(oLinea.value)

			var NewCelda = Newrow.getCellFromKey("STATUS");
			var status = NewCelda.getValue();
			if (status == 'null' || status == 'Null' || status == '' || status == null ) //Solamente pondremos el status M (Modificada) a una tarea existente, si se acaba de crear y luego se edita sera un ALTA
			NewCelda.setValue('M') //Indicamos que es una modificacion de una tarea existente

		if (txtObs.value != '')
			var ComentarioHTML = "<div><IMG ID='imgComentario' border=0 alt=" + arrTextos[3] + " title=" + arrTextos[3] + " align=middle onclick=AbrirComentarios(" + oLinea.value + ")  src='" + sRutaPM + "alta/images/SolicitudesPmSmall.gif' /> </div>";
		else
			var ComentarioHTML = "<div><IMG ID='imgComentario' border=0 alt=" + arrTextos[3] + " title=" + arrTextos[3] + " align=middle  width=0  src='" + sRutaPM + "alta/images/SolicitudesPmSmall.gif' /> </div>";
	}


		if (chkHito.checked) {
			var NewCelda = Newrow.getCellFromKey("COL_HITO");
			var celdaHtml = NewCelda.getElement()
			celdaHtml.innerHTML = HitoHTML
		}

		if (txtObs.value != '') {
			var NewCelda = Newrow.getCellFromKey("COMENTARIOS");
			var celdaHtml = NewCelda.getElement()
			celdaHtml.innerHTML = ComentarioHTML
		}
			 
		var CeldaCodTarea = Newrow.getCellFromKey("CODIGO")
		CeldaCodTarea.setValue(textCodTarea.value)

		var CeldaDen = Newrow.getCellFromKey("TAREADEN")
		CeldaDen.setValue(textDescTarea.value)

			 
		switch (oNumFases.value) {
			case '1':
				{
					var CeldaCodNiv4 = Newrow.getCellFromKey("COD_NIV4")
					if (CeldaCodNiv4)
						CeldaCodNiv4.setValue(textCodNivel1.value)

					var CeldaDenNiv4 = Newrow.getCellFromKey("DEN_NIV4")
					if (CeldaDenNiv4)
						CeldaDenNiv4.setValue(textDescNivel1.value)
					break
				}
			case '2':
				{
					var CeldaCodNiv4 = Newrow.getCellFromKey("COD_NIV4")
					if (CeldaCodNiv4)
						CeldaCodNiv4.setValue(textCodNivel2.value)

					var CeldaCodNiv3 = Newrow.getCellFromKey("COD_NIV3")
					if (CeldaCodNiv3)
						CeldaCodNiv3.setValue(textCodNivel1.value)


					var CeldaDenNiv4 = Newrow.getCellFromKey("DEN_NIV4")
					if (CeldaDenNiv4)
						CeldaDenNiv4.setValue(textDescNivel2.value)

					var CeldaDenNiv3 = Newrow.getCellFromKey("DEN_NIV3")
					if (CeldaDenNiv3)
						CeldaDenNiv3.setValue(textDescNivel1.value)

					break
				}
			case '3':
				{
					var CeldaCodNiv4 = Newrow.getCellFromKey("COD_NIV4")
					if (CeldaCodNiv4)
						CeldaCodNiv4.setValue(textCodNivel3.value)

					var CeldaCodNiv3 = Newrow.getCellFromKey("COD_NIV3")
					if (CeldaCodNiv3)
						CeldaCodNiv3.setValue(textCodNivel2.value)

					var CeldaCodNiv2 = Newrow.getCellFromKey("COD_NIV2")
					if (CeldaCodNiv2)
						CeldaCodNiv2.setValue(textCodNivel1.value)

					var CeldaDenNiv4 = Newrow.getCellFromKey("DEN_NIV4")
					if (CeldaDenNiv4)
						CeldaDenNiv4.setValue(textDescNivel3.value)
						 
					var CeldaDenNiv3 = Newrow.getCellFromKey("DEN_NIV3")
					if (CeldaDenNiv3)
						CeldaDenNiv3.setValue(textDescNivel2.value)

					var CeldaDenNiv2 = Newrow.getCellFromKey("DEN_NIV2")
					if (CeldaDenNiv2)
						CeldaDenNiv2.setValue(textDescNivel1.value)

					break
				}

			case '4':
				{
					var CeldaCodNiv4 = Newrow.getCellFromKey("COD_NIV4")
					if (CeldaCodNiv4)
						CeldaCodNiv4.setValue(textCodNivel4.value)

					var CeldaCodNiv3 = Newrow.getCellFromKey("COD_NIV3")
					if (CeldaCodNiv3)
						CeldaCodNiv3.setValue(textCodNivel3.value)

					var CeldaCodNiv2 = Newrow.getCellFromKey("COD_NIV2")
					if (CeldaCodNiv2)
						CeldaCodNiv2.setValue(textCodNivel2.value)

					var CeldaCodNiv1 = Newrow.getCellFromKey("COD_NIV1")
					if (CeldaCodNiv1)
						CeldaCodNiv1.setValue(textCodNivel1.value)


					var CeldaDenNiv4 = Newrow.getCellFromKey("DEN_NIV4")
					if (CeldaDenNiv4)
						CeldaDenNiv4.setValue(textDescNivel4.value)

					var CeldaDenNiv3 = Newrow.getCellFromKey("DEN_NIV3")
					if (CeldaDenNiv3)
						CeldaDenNiv3.setValue(textDescNivel3.value)

					var CeldaDenNiv2 = Newrow.getCellFromKey("DEN_NIV2")
					if (CeldaDenNiv2)
						CeldaDenNiv2.setValue(textDescNivel2.value)

					var CeldaDenNiv1 = Newrow.getCellFromKey("DEN_NIV1")
					if (CeldaDenNiv1)
						CeldaDenNiv1.setValue(textDescNivel1.value)
					break
				}
		}
			 


		var CeldaFecInicio = Newrow.getCellFromKey("FECEST_INICIO")
		if (dteFechaInicio.getValue() != null) {
			var fec_ini = dteFechaInicio.getValue()
			CeldaFecInicio.setValue(fec_ini.toString())
		} else
			CeldaFecInicio.setValue('')

		if (!(chkHito.checked)) { //Si no es Hito
			var CeldaFecFin = Newrow.getCellFromKey("FECEST_FIN")
			if (dteFechaFin.getValue() != null) {
				var fec_fin = dteFechaFin.getValue()
				CeldaFecFin.setValue(fec_fin.toString())
			} else
				CeldaFecFin.setValue('')

			var CeldaHorasAsig = Newrow.getCellFromKey("HORAS_ASIG")
			if (txtHorasEst.value == null || txtHorasEst.value == '' || txtHorasEst.value == 'Null')
			CeldaHorasAsig.setValue(0)
			else
			CeldaHorasAsig.setValue(txtHorasEst.value)


			var CeldaGradoAvance = Newrow.getCellFromKey("GRADO_AVANCE")
			if (txtGradoAvance.value == null || txtGradoAvance.value == '' || txtGradoAvance.value == 'Null')
			CeldaGradoAvance.setValue(0)
			else
				CeldaGradoAvance.setValue(txtGradoAvance.value)
		}
		else
		{
			var CeldaFecFin = Newrow.getCellFromKey("FECEST_FIN")
			var CeldaHorasAsig = Newrow.getCellFromKey("HORAS_ASIG")
			var CeldaGradoAvance = Newrow.getCellFromKey("GRADO_AVANCE")
				 
			CeldaFecFin.setValue('')
			CeldaHorasAsig.setValue(0)
			CeldaGradoAvance.setValue(0)
				 
		}
			 

		var CeldaEstado = Newrow.getCellFromKey("ESTADO")
		CeldaEstado.setValue(wddEstado.get_currentValue())

		var CeldaCodEstado = Newrow.getCellFromKey("CODESTADO")
		var item = wddEstado.get_selectedItem();
		CeldaCodEstado.setValue(item.get_value())

		var CeldaObs = Newrow.getCellFromKey("OBS")
		CeldaObs.setValue(txtObs.value)

		var CeldaHito = Newrow.getCellFromKey("HITO")
		if (chkHito.checked)
			CeldaHito.setValue("1")
		else
			CeldaHito.setValue("0")
			 
		var hid_linea = document.getElementById("<%=hid_Linea.ClientID%>")

		//Reorganizo el numero de lineas si se ha modificado por una ya existente
		if (hid_linea.value != txtNum.value)
			CambiarNumLinea(hid_linea.value, txtNum.value)
				 
		var CeldaNum = Newrow.getCellFromKey("NUM")
		CeldaNum.setValue(txtNum.value)

			 
		if (txtNum.value > window.opener.MaxFila)
			window.opener.MaxFila = parseInt(txtNum.value)
			 
		grid.AllowUpdate = 2;
		grid.AllowAddNew = currentSettings;

		window.close()
	}
</script>
<body style="background-color:#f7f6f6">
	<form id="form1" runat="server">
	<br />
	<asp:ScriptManager ID="scriptMng" runat="server"></asp:ScriptManager>
   
	<table id="tablaTarea" runat="server" width="700px" cellspacing="10">
	
	<tr>
		<td class="style1">
		<asp:Label CssClass="captionBlue" ID="lblNumLinea" runat="server" Text="DNumero Linea" ></asp:Label></td>
		<td>
            <igpck:WebNumericEditor ID="txtNumLinea" runat="server">
            </igpck:WebNumericEditor>
        </td>
		<td style="width:240px"></td>
		<td><asp:CheckBox ID="chkHito" runat="server" CssClass="captionBlue" Text="DHito" AutoPostBack="true"  /></td>
	</tr>
	<tr id="filaNiv1" runat="server" >
		<td class="style1"><asp:Label CssClass="captionBlue" ID="lblNivel1" runat="server" Text="DNivel1" ></asp:Label></td>
		<td><asp:TextBox ID="txtCodNivel1" runat="server" Width="150px" ></asp:TextBox></td>
		<td colspan="2"><asp:TextBox ID="txtDescNivel1" runat="server" Width="359px" ></asp:TextBox></td>
	</tr>
	<tr id="filaNiv2" runat="server">
		<td class="style1"><asp:Label CssClass="captionBlue" ID="lblNivel2" runat="server" Text="DNivel2" ></asp:Label></td>
		<td><asp:TextBox ID="txtCodNivel2" runat="server" Width="150px" ></asp:TextBox></td>
		<td colspan="2"><asp:TextBox ID="txtDescNivel2" runat="server" Width="359px" ></asp:TextBox></td>
	</tr>
	<tr id="filaNiv3" runat="server">
		<td class="style1"><asp:Label CssClass="captionBlue" ID="lblNivel3" runat="server" Text="DNivel3" ></asp:Label></td>
		<td><asp:TextBox ID="txtCodNivel3" runat="server" Width="150px" ></asp:TextBox></td>
		<td colspan="2"><asp:TextBox ID="txtDescNivel3" runat="server" Width="359px" ></asp:TextBox></td>
	</tr>
	<tr id="filaNiv4" runat="server">
		<td class="style1"><asp:Label CssClass="captionBlue" ID="lblNivel4" runat="server" Text="DNivel4" ></asp:Label></td>
		<td><asp:TextBox ID="txtCodNivel4" runat="server" Width="150px" ></asp:TextBox></td>
		<td colspan="2"><asp:TextBox ID="txtDescNivel4" runat="server" Width="359px" ></asp:TextBox></td>
	</tr>
	<tr id="filaTarea" runat="server">
		<td class="style1"><asp:Label CssClass="captionBlue" ID="lblTarea" runat="server" Text="DTarea" ></asp:Label></td>
		<td><asp:TextBox ID="txtCodTarea" runat="server" Width="150px" ></asp:TextBox></td>
		<td colspan="2"><asp:TextBox ID="txtDescTarea" runat="server" Width="359px" ></asp:TextBox></td>
	</tr>
	<tr id="filaFechas" runat="server">
		<td class="style1"><asp:Label CssClass="captionBlue" ID="lblFecInicio"  runat="server" Text="DFecha Inicio" ></asp:Label></td> 
		<td><fsde:GeneralEntry ID="dteFechaInicio" runat="server" Width="152px" Independiente="1"
					DropDownGridID="fsentryFecInicio" Tipo="TipoFecha" Tag="FecInicio">
					<InputStyle Width="130px" />
			   </fsde:GeneralEntry></td> 
		<td align="right"><asp:Label ID="lblFecFin" runat="server" Text="DFecha Fin" CssClass="captionBlue" ></asp:Label></td>
		<td id="CeldaFechaFin" runat="server" >
		<fsde:GeneralEntry ID="dteFechaFin" runat="server" Width="152px" Independiente="1"
					DropDownGridID="fsentryFecFin" Tipo="TipoFecha" Tag="FecFin">
					<InputStyle Width="130px" />
		</fsde:GeneralEntry></td>
	</tr>
	<tr id="filaHorayAvance" runat="server">
		<td class="style1"><asp:Label CssClass="captionBlue" ID="lblHorasEst" runat="server" Text="DHoras Est." ></asp:Label></td>
		<td><igpck:WebNumericEditor ID="txtHorasEst" runat="server" Width="152px" ></igpck:WebNumericEditor> </td>
		<td align="right"><asp:Label ID="lblGradoAvance" runat="server" Text="DGrado Avance"  CssClass="captionBlue"></asp:Label></td>
		<td><igpck:WebNumericEditor ID="txtGradoAvance" runat="server" Width="154px" ></igpck:WebNumericEditor></td>
	</tr>
	<tr id="filaEstado" runat="server">
		<td colspan="1" class="style1"><asp:Label CssClass="captionBlue" ID="lblEstado" runat="server" Text="DEstado" ></asp:Label></td>
		<td colspan="3"><ig:webdropdown runat="server" ID="wddEstado" BackColor="White"  
		EnableClosingDropDownOnSelect="true" EnableDropDownAsChild="true" Width="300px" DropDownContainerWidth="300px" DisplayMode="DropDownList">
		</ig:webdropdown></td>
	</tr>
	<tr id="filaObs" runat="server">
		<td colspan="1" class="style1"><asp:Label CssClass="captionBlue" ID="lblObs" runat="server" Text="DObservaciones" ></asp:Label></td>
		<td colspan="3"><asp:TextBox ID="txtObs" runat="server" Rows="4" 
				TextMode="MultiLine" Width="507px"></asp:TextBox></td>
	</tr>
	</table>
	<table width="700px">
	<tr>
		<td align="right" style="width:350px"><asp:Button style="min-width:70px" ID="btnAceptar" runat="server" OnClientClick="InsertRow();return false;" Text="DAceptar"  /></td> 
		<td align="left"  style="width:350px"><asp:Button style="min-width:70px" ID="btnCancelar" runat="server" Text="DCancelar" OnClientClick="window.close();return false;" /></td> 
	</tr>
	</table>
	<igtxt:WebDateTimeEdit ID="WebDateTimeEdit1" runat="server" Visible="false"></igtxt:WebDateTimeEdit>
	<asp:HiddenField ID="hid_NomGrid" runat="server"  />
	<asp:HiddenField ID="hid_linea" runat="server"  />
	<asp:HiddenField ID="hid_linea_grid" runat="server"  />
	<asp:HiddenField ID="hid_Edicion" runat="server"  />
	<asp:HiddenField ID="hid_Obligatorio" runat="server"  />
	<asp:HiddenField ID="hid_NumFases" runat="server"  />
	<asp:HiddenField ID="hid_NomElimFilas" runat="server" />
	</form>
</body>
</html>
