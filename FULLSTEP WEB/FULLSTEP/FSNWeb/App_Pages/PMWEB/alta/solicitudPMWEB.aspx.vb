Public Class solicitudPMWEB
    Inherits FSNPage

#Region " Web Form Designer Generated Code "
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    End Sub
    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents lblDescripcion As System.Web.UI.WebControls.Label
    Protected WithEvents lblArchivos As System.Web.UI.WebControls.Label
    Protected WithEvents txtDescripcion As System.Web.UI.WebControls.Label
    Protected WithEvents uwgEspecs As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
    Private designerPlaceholderDeclaration As System.Object
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        InitializeComponent()
    End Sub
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oSolicitud As FSNServer.Solicitud

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Solicitud

        lblDescripcion.Text = Textos(0)
        lblArchivos.Text = Textos(1)

        oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
        oSolicitud.ID = Request("Solicitud")

        oSolicitud.Load(Idioma)
        lblTitulo.Text = oSolicitud.Den(Idioma)
        txtDescripcion.Text = oSolicitud.Descr(Idioma)
        uwgEspecs.DataSource = oSolicitud.dsAdjuntos
        uwgEspecs.DataBind()
        uwgEspecs.Bands(0).Columns.FromKey("ID").Hidden = True
        uwgEspecs.Bands(0).Columns.FromKey("PER").Hidden = True
        uwgEspecs.Bands(0).Columns.FromKey("IDIOMA").Hidden = True
        uwgEspecs.Bands(0).Columns.FromKey("DESCARGAR").Move(11)
        uwgEspecs.Bands(0).Columns.FromKey("FECALTA").Hidden = True
        uwgEspecs.Bands(0).Columns.FromKey("NOMBRE").Hidden = True
        uwgEspecs.Bands(0).Columns.FromKey("FECALTA").Width = Unit.Percentage(10)
        uwgEspecs.Bands(0).Columns.FromKey("NOMBRE").Width = Unit.Percentage(25)
        uwgEspecs.Bands(0).Columns.FromKey("NOM").Width = Unit.Percentage(25)
        uwgEspecs.Bands(0).Columns.FromKey("DATASIZE").Width = Unit.Percentage(5)
        uwgEspecs.Bands(0).Columns.FromKey("COMENT").Width = Unit.Percentage(30)
        uwgEspecs.Bands(0).Columns.FromKey("DESCARGAR").Width = Unit.Percentage(5)
        uwgEspecs.Bands(0).Columns.FromKey("FECALTA").Header.Caption = Textos(6)
        uwgEspecs.Bands(0).Columns.FromKey("NOMBRE").Header.Caption = Textos(7)
        uwgEspecs.Bands(0).Columns.FromKey("NOM").Header.Caption = Textos(2)
        uwgEspecs.Bands(0).Columns.FromKey("DATASIZE").Header.Caption = Textos(4)
        uwgEspecs.Bands(0).Columns.FromKey("COMENT").Header.Caption = Textos(3)
        uwgEspecs.Bands(0).Columns.FromKey("DESCARGAR").Header.Caption = Textos(5)
    End Sub
    Private Sub uwgEspecs_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgEspecs.InitializeRow
        Dim oNumber As Double = e.Row.Cells.FromKey("DATASIZE").Value
        e.Row.Cells.FromKey("DATASIZE").Text = oNumber.ToString(FSNUser.NumberFormat)
		e.Row.Cells.FromKey("DESCARGAR").Value = "<img border=""0"" align=""center"" src=""" & ConfigurationManager.AppSettings("rutaFS") + "PMWEB/_common/images/descargarespecif.gif"" onclick=""Descargar(" & e.Row.Cells.FromKey("ID").Value & ")"">"
	End Sub
End Class