

Public Class guardarSOLFavorita_aspx
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' <summary>
    ''' Lleva a campo el almacenamiento de una solicitud favorita
    ''' </summary>
    ''' <param name="sender">propios del evento</param>
    ''' <param name="e">propios del evento</param>        
    ''' <remarks>Tiempo m�ximo:1,5</remarks>

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim oSolicitud As FSNServer.Solicitud
        oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
        Dim ds As DataSet

        Dim sDenFavorita As String = Request("denFavorita")
        Dim lSolicitud As Long = Request("solicitud")
        Dim instancia As Long
        If Request("Instancia") <> Nothing Then
            'Si viene de detalle de una solicitud vendra con id de la instancia
            instancia = Request("Instancia")
        End If

        ds = GenerarDataset(Request)

        CompletarFormulario(lSolicitud, ds, instancia)

        oSolicitud.EnviarFavoritos(lSolicitud, sDenFavorita, FSNUser.Cod, ds)

    End Sub


    ''' <summary>
    ''' Crea el dataset con los cmapos del formulario
    ''' </summary>
    ''' <param name="Request">Campos del formulario</param>
    ''' <returns>dataset con los campos del formulario</returns>
    ''' <remarks>Llamada desde:page_load; Tiempo m�ximo:1seg.</remarks>

    Private Function GenerarDataset(ByVal Request As System.Web.HttpRequest) As DataSet

        Dim oItem As System.Collections.Specialized.NameValueCollection
        Dim oUser As FSNServer.User = Session("FSN_User")
        Dim loop1 As Integer
        Dim arr1() As String
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")

        Dim DS As DataSet
        Dim dt As DataTable
        Dim oDTDesglose As DataTable
        Dim keys(0) As DataColumn
        Dim find(0) As Object
        Dim keysDesglose(2) As DataColumn
        Dim findDesglose(2) As Object
        Dim keysAcc(1) As DataColumn
        Dim findAcc(1) As Object

        DS = New DataSet

        dt = DS.Tables.Add("CAMPOS")

        dt.Columns.Add("CAMPO", System.Type.GetType("System.Int32"))
        dt.Columns.Add("VALOR_TEXT", System.Type.GetType("System.String"))
        dt.Columns.Add("VALOR_NUM", System.Type.GetType("System.Double"))
        dt.Columns.Add("VALOR_BOOL", System.Type.GetType("System.Int32"))
        dt.Columns.Add("VALOR_FEC", System.Type.GetType("System.DateTime"))
        dt.Columns.Add("ES_SUBCAMPO", System.Type.GetType("System.Int32"))

        keys(0) = dt.Columns("CAMPO")
        dt.PrimaryKey = keys

        'Adjuntos Campos
        Dim dtAdjun As DataTable = DS.Tables.Add("TEMPADJUN")

        dtAdjun.Columns.Add("CAMPO", System.Type.GetType("System.Int32"))
        dtAdjun.Columns.Add("ID", System.Type.GetType("System.Int32"))
        dtAdjun.Columns.Add("TIPO", System.Type.GetType("System.String"))

        'Lineas de desglose
        oDTDesglose = DS.Tables.Add("LINEAS_DESGLOSE")

        oDTDesglose.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
        oDTDesglose.Columns.Add("CAMPO_PADRE", System.Type.GetType("System.Int32"))
        oDTDesglose.Columns.Add("CAMPO_HIJO", System.Type.GetType("System.Int32"))
        oDTDesglose.Columns.Add("VALOR_TEXT", System.Type.GetType("System.String"))
        oDTDesglose.Columns.Add("VALOR_NUM", System.Type.GetType("System.Double"))
        oDTDesglose.Columns.Add("VALOR_BOOL", System.Type.GetType("System.Int32"))
        oDTDesglose.Columns.Add("VALOR_FEC", System.Type.GetType("System.DateTime"))
        oDTDesglose.Columns.Add("LINEA_OLD", System.Type.GetType("System.Int32"))
        oDTDesglose.Columns("LINEA_OLD").AllowDBNull = True

        keysDesglose(0) = oDTDesglose.Columns("LINEA")
        keysDesglose(1) = oDTDesglose.Columns("CAMPO_PADRE")
        keysDesglose(2) = oDTDesglose.Columns("CAMPO_HIJO")
        oDTDesglose.PrimaryKey = keysDesglose

        'Adjuntos desglose
        Dim dtDesgloseAdjun As DataTable = DS.Tables.Add("TEMPDESGLOSEADJUN")

        dtDesgloseAdjun.Columns.Add("CAMPO_PADRE", System.Type.GetType("System.Int32"))
        dtDesgloseAdjun.Columns.Add("CAMPO_HIJO", System.Type.GetType("System.Int32"))
        dtDesgloseAdjun.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
        dtDesgloseAdjun.Columns.Add("ID", System.Type.GetType("System.Int32"))
        dtDesgloseAdjun.Columns.Add("TIPO", System.Type.GetType("System.String"))
        dtDesgloseAdjun.Columns.Add("LINEA_OLD", System.Type.GetType("System.Int32"))
        dtDesgloseAdjun.Columns("LINEA_OLD").AllowDBNull = True

        'Participantes
        Dim dtParticipantes As DataTable = DS.Tables.Add("PARTICIPANTES")

        dtParticipantes.Columns.Add("ROL", System.Type.GetType("System.Int32"))
        dtParticipantes.Columns.Add("PER", System.Type.GetType("System.String"))
        dtParticipantes.Columns.Add("PROVE", System.Type.GetType("System.String"))

        Dim lInstancia As Long = Request("Instancia")
        '0 -> Nwalta.aspx no tiene este control.
        'Eoc-> NWDetalleSolicitud.aspx tiene este control. Y el id q llega en PART es de pm_copia_rol no de pm_rol.

        'Contactos
        Dim dtContactos As DataTable = DS.Tables.Add("CONTACTOS")

        dtContactos.Columns.Add("CON", System.Type.GetType("System.String"))

        Dim sRequest As String
        Dim oRequest As Object
        Dim dtNewRow As DataRow

        Dim iTipo As TiposDeDatos.TipoGeneral

        Dim sDenFavorita As String = ""

        oItem = Request.Form
        arr1 = oItem.AllKeys
        For loop1 = 0 To arr1.GetUpperBound(0)
            sRequest = arr1(loop1).ToString

            oRequest = sRequest.Split("_")

            If oRequest(0) = "CAMPO" Or oRequest(0) = "CAMPOID" Or oRequest(0) = "DESG" Or oRequest(0) = "DESGID" Or oRequest(0) = "PART" Then
                iTipo = oRequest(UBound(oRequest))
            End If


            Select Case oRequest(0)

                Case "CAMPO"
                    'CAMPO_17046_7
                    find(0) = oRequest(1)
                    dtNewRow = dt.Rows.Find(find)
                    If dtNewRow Is Nothing Then
                        dtNewRow = dt.NewRow
                    End If

                    dtNewRow.Item("CAMPO") = oRequest(1)

                    If Not IsNothing(Request(sRequest)) Then
                        Select Case iTipo
                            Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoArchivo, TiposDeDatos.TipoGeneral.TipoPresBajaLog, TiposDeDatos.TipoGeneral.TipoDepBajaLog, TiposDeDatos.TipoGeneral.TipoEditor
                                dtNewRow.Item("VALOR_TEXT") = strToDBNull(Request(sRequest))
                            Case TiposDeDatos.TipoGeneral.TipoNumerico
                                If Request(sRequest) = "" Then
                                    dtNewRow.Item("VALOR_NUM") = System.DBNull.Value
                                Else
                                    dtNewRow.Item("VALOR_NUM") = Numero(Request(sRequest), ".", ",")
                                End If
                            Case TiposDeDatos.TipoGeneral.TipoBoolean
                                If Request(sRequest) = "" Then
                                    dtNewRow.Item("VALOR_BOOL") = System.DBNull.Value
                                Else
                                    dtNewRow.Item("VALOR_BOOL") = Request(sRequest)
                                End If
                            Case TiposDeDatos.TipoGeneral.TipoFecha
                                If igDateToDate(Request(sRequest)) <> Nothing Then
                                    dtNewRow.Item("VALOR_FEC") = igDateToDate(Request(sRequest))
                                End If
                        End Select
                    End If
                    If dtNewRow.RowState = DataRowState.Detached Then
                        dt.Rows.Add(dtNewRow)
                    End If

                Case "CAMPOID"
                    'PARA LOS CAMPOS DE TIPO TEXTO Y INTRODUCCI�N LISTA HAY QUE METER EL INDICE DEL ELEMENTO SELECCIONADO EN VALOR_NUM
                    If iTipo = TiposDeDatos.TipoGeneral.TipoString Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoCorto Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoLargo Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio Then
                        find(0) = oRequest(1)
                        dtNewRow = dt.Rows.Find(find)
                        If dtNewRow Is Nothing Then
                            dtNewRow = dt.NewRow
                        End If

                        dtNewRow.Item("CAMPO") = oRequest(1)

                        Select Case iTipo
                            Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoArchivo
                                If Request(sRequest) = "" Then
                                    dtNewRow.Item("VALOR_NUM") = System.DBNull.Value
                                Else
                                    dtNewRow.Item("VALOR_NUM") = Numero(Request(sRequest), ".", ",")
                                End If
                        End Select
                    End If
                    If dtNewRow.RowState = DataRowState.Detached Then
                        dt.Rows.Add(dtNewRow)
                    End If

                Case "DESG"

                    'DESG_17048_17058_3_6

                    findDesglose(0) = oRequest(3)
                    findDesglose(1) = oRequest(1)
                    findDesglose(2) = oRequest(2)
                    dtNewRow = oDTDesglose.Rows.Find(findDesglose)
                    If dtNewRow Is Nothing Then
                        dtNewRow = oDTDesglose.NewRow
                    End If

                    dtNewRow.Item("LINEA") = oRequest(3)
                    dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
                    dtNewRow.Item("CAMPO_HIJO") = oRequest(2)
                    'dtNewRow.Item("LINEA_OLD") = oRequest(3)
                    If Request(sRequest) <> Nothing Then
                        Select Case iTipo
                            Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoArchivo, TiposDeDatos.TipoGeneral.TipoEditor
                                If Request(sRequest) = "nulo" Then
                                    dtNewRow.Item("VALOR_TEXT") = System.DBNull.Value
                                Else
                                    dtNewRow.Item("VALOR_TEXT") = Request(sRequest)
                                End If
                            Case TiposDeDatos.TipoGeneral.TipoNumerico
                                If Request(sRequest) = "" Then
                                    dtNewRow.Item("VALOR_NUM") = System.DBNull.Value
                                Else
                                    dtNewRow.Item("VALOR_NUM") = Numero(Request(sRequest), ".", ",")
                                End If
                            Case TiposDeDatos.TipoGeneral.TipoBoolean
                                If Request(sRequest) = "" Then
                                    dtNewRow.Item("VALOR_BOOL") = System.DBNull.Value
                                Else
                                    dtNewRow.Item("VALOR_BOOL") = Request(sRequest)
                                End If
                            Case TiposDeDatos.TipoGeneral.TipoFecha
                                If igDateToDate(Request(sRequest)) <> Nothing Then
                                    dtNewRow.Item("VALOR_FEC") = igDateToDate(Request(sRequest))
                                End If
                        End Select
                    End If
                    If dtNewRow.RowState = DataRowState.Detached Then
                        oDTDesglose.Rows.Add(dtNewRow)
                    End If


                Case "DESGID"
                    'DESGID_17048_17053_2_6:
                    If iTipo = TiposDeDatos.TipoGeneral.TipoString Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoCorto Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoLargo Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio Then
                        findDesglose(0) = oRequest(3)
                        findDesglose(1) = oRequest(1)
                        findDesglose(2) = oRequest(2)
                        dtNewRow = oDTDesglose.Rows.Find(findDesglose)
                        If dtNewRow Is Nothing Then
                            dtNewRow = oDTDesglose.NewRow
                        End If

                        dtNewRow.Item("LINEA") = oRequest(3)
                        dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
                        dtNewRow.Item("CAMPO_HIJO") = oRequest(2)
                        'dtNewRow.Item("LINEA_OLD") = oRequest(3)
                        Select Case iTipo
                            Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoArchivo
                                dtNewRow.Item("VALOR_NUM") = IIf(Request(sRequest) = "", System.DBNull.Value, Numero(Request(sRequest), ".", ","))
                        End Select
                        If dtNewRow.RowState = DataRowState.Detached Then
                            oDTDesglose.Rows.Add(dtNewRow)
                        End If

                    End If

                Case "ADJUN"
                    'ADJUN_17063_2
                    If Request(sRequest) <> Nothing Then
                        dtNewRow = dtAdjun.NewRow
                        dtNewRow.Item("CAMPO") = oRequest(1)
                        dtNewRow.Item("TIPO") = oRequest(2)
                        dtNewRow.Item("ID") = Request(sRequest)
                        dtAdjun.Rows.Add(dtNewRow)
                    End If

                Case "ADJUNDESG"
                    'ADJUNDESG_17064_17079_1_2
                    If Request(sRequest) <> Nothing Then

                        dtNewRow = dtDesgloseAdjun.NewRow
                        dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
                        dtNewRow.Item("CAMPO_HIJO") = oRequest(2)

                        dtNewRow.Item("LINEA") = oRequest(3)
                        dtNewRow.Item("TIPO") = oRequest(4)
                        dtNewRow.Item("ID") = Request(sRequest)
                        dtDesgloseAdjun.Rows.Add(dtNewRow)
                    End If

                    'Participantes
                Case "PART"
                    Dim dtNewRowCon As DataRow

                    dtNewRow = dtParticipantes.NewRow
                    dtNewRow.Item("ROL") = CInt(oRequest(2))

                    Dim iTipoGS As TiposDeDatos.TipoCampoGS = CInt(oRequest(1))
                    If iTipoGS = TiposDeDatos.TipoCampoGS.Persona Then
                        dtNewRow.Item("PER") = strToDBNull(Request(sRequest))
                        dtNewRow.Item("PROVE") = System.DBNull.Value
                    Else
                        Dim sAux As String = Request(sRequest)
                        Dim sProve As String
                        Dim sCon As String
                        If InStr(sAux, "###") > 0 Then
                            sProve = Left(sAux, InStr(sAux, "###") - 1)
                            sCon = Right(sAux, Len(sAux) - Len(sProve) - 3)
                        Else
                            sProve = ""
                            sCon = ""
                        End If
                        dtNewRow.Item("PER") = System.DBNull.Value
                        dtNewRow.Item("PROVE") = strToDBNull(sProve)

                        If sCon <> "" Then
                            Dim sContactos() As String
                            sContactos = Left(sCon, Len(sCon) - 1).Split("|")
                            For Each sContacto As String In sContactos
                                dtNewRowCon = dtContactos.NewRow
                                dtNewRowCon.Item("CON") = sContacto
                                dtContactos.Rows.Add(dtNewRowCon)
                            Next
                        End If
                    End If
                    dtParticipantes.Rows.Add(dtNewRow)
            End Select

        Next loop1


        'volvemos a recorrernos el request para actualizar las l�neas de desglose con la correspondiente l�nea en bd
        For loop1 = 0 To arr1.GetUpperBound(0)
            sRequest = arr1(loop1).ToString

            oRequest = sRequest.Split("_")

            Select Case oRequest(0)

                Case "LINDESG"
                    If Request(sRequest) <> Nothing Then
                        Dim oLineas As DataRow()

                        oLineas = oDTDesglose.Select("LINEA =" & oRequest(2).ToString() & " AND CAMPO_PADRE = " & oRequest(1).ToString())
                        For Each dtNewRow In oLineas
                            dtNewRow.Item("LINEA_OLD") = Numero(Request(sRequest))
                        Next

                        oLineas = dtDesgloseAdjun.Select("LINEA =" & oRequest(2).ToString() & " AND CAMPO_PADRE = " & oRequest(1).ToString())
                        For Each dtNewRow In oLineas
                            dtNewRow.Item("LINEA_OLD") = Numero(Request(sRequest))
                        Next

                    End If

            End Select
        Next

        Return DS

    End Function
    ''' <summary>
    ''' Completa el dataset que estamos usando con el formulario, con los campos ocultos del formulario
    ''' </summary>
    ''' <param name="lSolicitud">ID de la solicitud</param>
    ''' <param name="ds">Dataset con los campos del formulario</param>
    ''' <param name="idInstancia">id de la instancia si se llama a esta pagina desde el detalle de una solicitud</param>        
    ''' <remarks>Llamada desde:Form_load; Tiempo m�ximo:0,3</remarks>
    Private Sub CompletarFormulario(ByVal lSolicitud As Long, ByRef ds As DataSet, Optional ByVal idInstancia As Long = 0)
        Dim oSolicitud As FSNServer.Solicitud
        oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))

        Dim dsInvisibles As DataSet

        dsInvisibles = oSolicitud.CargarInvisibles(lSolicitud, True, FSNUser.CodPersona, idInstancia)

        Dim dRow As DataRow
        Dim dNewRow As DataRow
        Dim dRows() As DataRow
        Dim dRows2() As DataRow
        Dim dRowsAdjun() As DataRow
        Dim bNoAdjQaPop As Boolean

        ''
        Dim bPadreNoVisibleHijoVa As Boolean = False
        ''

        'METEMOS LOS CAMPOS SIMPLES QUE ESTUVIERAN OCULTOS JUNTO CON LOS VALORES
        For Each dRow In dsInvisibles.Tables(0).Rows
            dNewRow = ds.Tables("CAMPOS").NewRow
            dNewRow.Item("CAMPO") = dRow.Item("ID")
            dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")

            Try
                dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
                dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
            Catch ex As Exception

            End Try

            dNewRow.Item("ES_SUBCAMPO") = 0

            Try
                ds.Tables("CAMPOS").Rows.Add(dNewRow)
            Catch ex As Exception

            End Try
        Next

        'METEMOS LOS ADJUNTOS DE LOS CAMPOS SIMPLES OCULTOS
        If Not (ds.Tables("TEMPADJUN") Is Nothing) Then
            For Each dRow In dsInvisibles.Tables(5).Rows
                If ds.Tables("TEMPADJUN").Select("CAMPO=" + dRow.Item("CAMPO").ToString + " AND ID=" + dRow.Item("ID").ToString).Length = 0 Then
                    dNewRow = ds.Tables("TEMPADJUN").NewRow
                    dNewRow.Item("CAMPO") = dRow.Item("CAMPO")
                    dNewRow.Item("TIPO") = dRow.Item("TIPO")
                    dNewRow.Item("ID") = dRow.Item("ID")

                    Try
                        ds.Tables("TEMPADJUN").Rows.Add(dNewRow)
                    Catch ex As Exception
                    End Try
                End If
            Next
        End If

        'METEMOS LOS CAMPOS DE DESGLOSE QUE ESTUVIERAN OCULTOS
        For Each dRow In dsInvisibles.Tables(1).Rows
            If ds.Tables("CAMPOS").Select("CAMPO=" & dRow.Item("CAMPO_HIJO")).Length = 0 Then
                dNewRow = ds.Tables("CAMPOS").NewRow
                dNewRow.Item("CAMPO") = dRow.Item("CAMPO_HIJO")
                dNewRow.Item("ES_SUBCAMPO") = 1

                Try
                    ds.Tables("CAMPOS").Rows.Add(dNewRow)
                Catch ex As Exception

                End Try
            End If
        Next

        Dim iLinea As Integer
        For Each dRow In dsInvisibles.Tables(1).Rows
            'en este datatable vienen los visibles y los invisibles. 
            'Solo se deben a�adir los datos de los invisibles que tienen valor en el campo linea y aquellos que sean de solo lectura.
            If (dRow.Item("VISIBLE") = 0 Or dRow.Item("ESCRITURA") = 0) AndAlso Not IsDBNull(dRow.Item("LINEA")) Then
                ' Este IF comprueba que este campo invisible pertenece a un desglose con alg�n campo visible
                dRows = dsInvisibles.Tables(1).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND VISIBLE = 1")

                If dRows.Length > 0 Then
                    'Si hay alg�n campo visible en este desglose habr� que obtener que nuevo n�mero de l�nea tiene ahora

                    dRows = ds.Tables("LINEAS_DESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString)
                    If dRows.Length > 0 Then 'si fuera igual a 0 indicar�a que se ha borrado esa l�nea y por tanto no hay que guardarlo
                        iLinea = dRows(0).Item("LINEA")
                        'comprobamos que no exista ya que los desgloses de popup si que se cargan aunque est� invisible
                        If ds.Tables("LINEAS_DESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString).Length = 0 Then
                            dNewRow = ds.Tables("LINEAS_DESGLOSE").NewRow
                            dNewRow.Item("LINEA") = iLinea
                            dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                            dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                            dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                            If (IsDBNull(dRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
                                dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
                            End If
                            Try
                                dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                                dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
                                dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
                            Catch ex As Exception
                            End Try
                            dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
                            Try
                                'caso Presupuesto nivel 4: Campo Oculto que controla por otro lado el maper.
                                'Intenta meter algo que ya he metido. Solo desglose popup.
                                ds.Tables("LINEAS_DESGLOSE").Rows.Add(dNewRow)
                            Catch ex As Exception
                            End Try

                            If Not (ds.Tables("TEMPDESGLOSEADJUN") Is Nothing) Then
                                dRowsAdjun = dsInvisibles.Tables(3).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA = " + iLinea.ToString)
                                If (dRowsAdjun.Length > 0) Then
                                    For Each Adjun As DataRow In dRowsAdjun
                                        dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow

                                        dNewRow.Item("LINEA") = iLinea
                                        dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                                        dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")

                                        dNewRow.Item("ID") = Adjun.Item("ADJUN")
                                        dNewRow.Item("TIPO") = 1
                                        Try
                                            ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)
                                        Catch ex As Exception
                                        End Try
                                        dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
                                    Next
                                End If
                            End If
                        Else
                            'Para que no se cargue CodArticulo =119 en el caso de que el NuevoCodArt este OCULTO y la DENOMINACION visible
                            Dim sql, sql2 As String
                            sql = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND ((TIPO_CAMPO_GS= " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND VISIBLE = 0)"
                            sql = sql + " OR (TIPO_CAMPO_GS =  " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND VISIBLE = 1))"

                            sql2 = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND ((TIPO_CAMPO_GS= " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & "OR TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & ") AND VISIBLE = 1)"

                            If ((DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo) And (DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Centro)) _
                                 Or ((dsInvisibles.Tables(1).Select(sql).Length < 2) And (DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.NuevoCodArticulo)) _
                                 Or ((DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Centro) And (dsInvisibles.Tables(1).Select(sql2).Length = 0)) Then

                                dNewRow = ds.Tables("LINEAS_DESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString)(0)

                                'dNewRow.Item("VALOR_TEXT") tendr� el valor "nulo" cuando en el desglose se muestre solamente la denominacion el articulo no sea generico
                                'y introducimos una nueva denominacion
                                If IsDBNull(dNewRow.Item("VALOR_TEXT")) Then
                                    dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                                ElseIf dNewRow.Item("VALOR_TEXT") = "nulo" Then
                                    dNewRow.Item("VALOR_TEXT") = System.DBNull.Value
                                Else
                                    dNewRow.Item("VALOR_TEXT") = dNewRow.Item("VALOR_TEXT")
                                End If
                                If (IsDBNull(dNewRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
                                    dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
                                End If
                                Try
                                    dNewRow.Item("VALOR_NUM") = IIf(IsDBNull(dNewRow.Item("VALOR_NUM")), dRow.Item("VALOR_NUM"), dNewRow.Item("VALOR_NUM"))
                                    dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(IIf(IsDBNull(dNewRow.Item("VALOR_NUM")), dRow.Item("VALOR_NUM"), dNewRow.Item("VALOR_NUM"))), DBNullToSomething(IIf(IsDBNull(dNewRow.Item("VALOR_FEC")), dRow.Item("VALOR_FEC"), dNewRow.Item("VALOR_FEC"))))
                                Catch ex As Exception
                                End Try
                                Try
                                    dNewRow.Item("VALOR_FEC") = IIf(IsDBNull(dNewRow.Item("VALOR_FEC")), dRow.Item("VALOR_FEC"), dNewRow.Item("VALOR_FEC"))
                                    dNewRow.Item("VALOR_BOOL") = IIf(IsDBNull(dNewRow.Item("VALOR_BOOL")), dRow.Item("VALOR_BOOL"), dNewRow.Item("VALOR_BOOL"))
                                Catch ex As Exception
                                End Try

                            End If
                        End If

                    End If
                Else ' al no haber campos visibles de este desglose habr� que a�adirlo tal y como viene

                    dNewRow = ds.Tables("LINEAS_DESGLOSE").NewRow
                    dNewRow.Item("LINEA") = dRow.Item("LINEA")
                    dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                    dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                    dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                    If (IsDBNull(dRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
                        dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
                    End If

                    Try
                        dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                        dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
                        dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
                    Catch ex As Exception

                    End Try

                    dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")

                    Try
                        'caso Presupuesto nivel 4: Campo Oculto que controla por otro lado el maper.
                        'Intenta meter algo que ya he metido. Solo desglose popup.       
                        ds.Tables("LINEAS_DESGLOSE").Rows.Add(dNewRow)
                    Catch ex As Exception
                    End Try

                    If Not (ds.Tables("TEMPDESGLOSEADJUN") Is Nothing) Then
                        dRowsAdjun = dsInvisibles.Tables(3).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA = " + dRow.Item("LINEA").ToString)
                        If (dRowsAdjun.Length > 0) AndAlso (Not bNoAdjQaPop) Then
                            For Each Adjun As DataRow In dRowsAdjun
                                dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow

                                dNewRow.Item("LINEA") = dRow.Item("LINEA")
                                dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                                dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")

                                dNewRow.Item("ID") = Adjun.Item("ADJUN")
                                dNewRow.Item("TIPO") = 1
                                Try
                                    ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)
                                Catch ex As Exception
                                End Try
                                dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
                            Next
                        End If
                    End If
                End If
            End If
        Next

        For Each dRow In dsInvisibles.Tables(2).Rows
            'Si estaba entre los invisibles  el PADRE, ed, el DESGLOSE que mas da PM_CONF_CUMP_BLOQUE
            'del hijo, seguro que no esta por GenerarDataSet y habra que meterlo
            dRows = dsInvisibles.Tables(0).Select("ID=" + dRow.Item("CAMPO_PADRE").ToString)
            bPadreNoVisibleHijoVa = (dRows.Length > 0)
            ''
            If bPadreNoVisibleHijoVa Then
                dRows = ds.Tables("LINEAS_DESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString)
                If dRows.Length > 0 Then 'si fuera igual a 0 indicar�a que se ha borrado esa l�nea y por tanto no hay que guardarlo
                    iLinea = dRows(0).Item("LINEA")
                    'comprobamos que no exista ya que los desgloses de popup si que se cargan aunque est� invisible
                    If ds.Tables("LINEAS_DESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString).Length = 0 Then

                        dNewRow = ds.Tables("LINEAS_DESGLOSE").NewRow
                        dNewRow.Item("LINEA") = iLinea
                        dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                        dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                        dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                        If (IsDBNull(dRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
                            dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
                        End If
                        Try
                            dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                            dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
                            dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
                        Catch ex As Exception
                        End Try
                        dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
                        Try
                            'caso Presupuesto nivel 4: Campo Oculto que controla por otro lado el maper.
                            'Intenta meter algo que ya he metido. Solo desglose popup.
                            ds.Tables("LINEAS_DESGLOSE").Rows.Add(dNewRow)
                        Catch ex As Exception
                        End Try

                        If Not (ds.Tables("TEMPDESGLOSEADJUN") Is Nothing) Then
                            dRowsAdjun = dsInvisibles.Tables(4).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA = " + iLinea.ToString)
                            If (dRowsAdjun.Length > 0) Then
                                For Each Adjun As DataRow In dRowsAdjun
                                    dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow

                                    dNewRow.Item("LINEA") = iLinea
                                    dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                                    dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")

                                    dNewRow.Item("ID") = Adjun.Item("ADJUN")
                                    dNewRow.Item("TIPO") = 1
                                    Try
                                        ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)
                                    Catch ex As Exception
                                    End Try
                                    dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
                                Next
                            End If
                        End If
                    Else
                        'Para que no se cargue CodArticulo =119 en el caso de que el NuevoCodArt este OCULTO y la DENOMINACION visible
                        Dim sql, sql2 As String
                        sql = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString
                        sql = sql + " AND CAMPO_HIJO = " + dRow.Item("CAMPO_HIJO").ToString
                        sql = sql + " AND LINEA = " + iLinea.ToString
                        dRows = dsInvisibles.Tables(1).Select(sql)

                        sql = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND ((TIPO_CAMPO_GS= " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND VISIBLE = 0)"
                        sql = sql + " OR (TIPO_CAMPO_GS =  " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND VISIBLE = 1))"

                        sql2 = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND ((TIPO_CAMPO_GS= " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & "OR TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & ") AND VISIBLE = 1)"

                        If dRows.Length > 0 Then
                            If ((DBNullToSomething(dRows(0).Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo) And (DBNullToSomething(dRows(0).Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Centro)) _
                            Or ((dsInvisibles.Tables(1).Select(sql).Length < 2) And (DBNullToSomething(dRows(0).Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.NuevoCodArticulo)) _
                            Or ((DBNullToSomething(dRows(0).Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Centro) And (dsInvisibles.Tables(1).Select(sql2).Length = 0)) Then
                                dNewRow = ds.Tables("LINEAS_DESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString)(0)

                                'dNewRow.Item("VALOR_TEXT") tendr� el valor "nulo" cuando en el desglose se muestre solamente la denominacion el articulo no sea generico
                                'y introducimos una nueva denominacion
                                If IsDBNull(dNewRow.Item("VALOR_TEXT")) Then
                                    dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                                ElseIf dNewRow.Item("VALOR_TEXT") = "nulo" Then
                                    dNewRow.Item("VALOR_TEXT") = System.DBNull.Value
                                Else
                                    dNewRow.Item("VALOR_TEXT") = dNewRow.Item("VALOR_TEXT")
                                End If
                                If (IsDBNull(dNewRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
                                    dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
                                End If
                                Try
                                    dNewRow.Item("VALOR_NUM") = IIf(IsDBNull(dNewRow.Item("VALOR_NUM")), dRow.Item("VALOR_NUM"), dNewRow.Item("VALOR_NUM"))
                                    dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(IIf(IsDBNull(dNewRow.Item("VALOR_NUM")), dRow.Item("VALOR_NUM"), dNewRow.Item("VALOR_NUM"))), DBNullToSomething(IIf(IsDBNull(dNewRow.Item("VALOR_FEC")), dRow.Item("VALOR_FEC"), dNewRow.Item("VALOR_FEC"))))
                                Catch ex As Exception
                                End Try
                                Try
                                    dNewRow.Item("VALOR_FEC") = IIf(IsDBNull(dNewRow.Item("VALOR_FEC")), dRow.Item("VALOR_FEC"), dNewRow.Item("VALOR_FEC"))
                                    dNewRow.Item("VALOR_BOOL") = IIf(IsDBNull(dNewRow.Item("VALOR_BOOL")), dRow.Item("VALOR_BOOL"), dNewRow.Item("VALOR_BOOL"))
                                Catch ex As Exception
                                End Try


                            End If
                        End If
                    End If
                Else ' al no haber campos visibles de este desglose habr� que a�adirlo tal y como viene
                    dNewRow = ds.Tables("LINEAS_DESGLOSE").NewRow
                    dNewRow.Item("LINEA") = dRow.Item("LINEA")
                    dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                    dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                    dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                    If (IsDBNull(dRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
                        dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
                    End If

                    Try
                        dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                        dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
                        dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
                    Catch ex As Exception

                    End Try

                    dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")

                    Try
                        'caso Presupuesto nivel 4: Campo Oculto que controla por otro lado el maper.
                        'Intenta meter algo que ya he metido. Solo desglose popup.       
                        ds.Tables("LINEAS_DESGLOSE").Rows.Add(dNewRow)
                    Catch ex As Exception
                    End Try

                    If Not (ds.Tables("TEMPDESGLOSEADJUN") Is Nothing) Then
                        dRowsAdjun = dsInvisibles.Tables(4).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA = " + dRow.Item("LINEA").ToString)
                        If (dRowsAdjun.Length > 0) Then
                            For Each Adjun As DataRow In dRowsAdjun
                                dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow

                                dNewRow.Item("LINEA") = dRow.Item("LINEA")
                                dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                                dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")

                                dNewRow.Item("ID") = Adjun.Item("ADJUN")
                                dNewRow.Item("TIPO") = 1
                                Try
                                    ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)
                                Catch ex As Exception
                                End Try
                                dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
                            Next
                        End If
                    End If
                End If
            End If
        Next


        'A�adir las nuevas filas y copiar los campos ocultos
        Dim dDefaultRow As DataRow
        Dim lMaxLine As Long = 0
        Dim auxRow As DataRow
        Dim auxDataTable As New DataTable("GRUPOS")
        auxDataTable.Columns.Add("CAMPO_PADRE", System.Type.GetType("System.Int32"))
        auxDataTable.Columns.Add("LINEAS", System.Type.GetType("System.Int32"))

        dRows = ds.Tables("LINEAS_DESGLOSE").Select("LINEA_OLD IS NULL")


        For Each dRow In dRows
            If auxDataTable.Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString).Length > 0 Then
                auxRow = auxDataTable.Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString)(0)
            Else
                auxRow = auxDataTable.NewRow
                auxRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
            End If

            If DBNullToSomething(auxRow.Item("LINEAS")) < DBNullToSomething(dRow.Item("LINEA")) Then
                auxRow.Item("LINEAS") = dRow.Item("LINEA")
            End If

            If auxRow.RowState = DataRowState.Detached Then
                auxDataTable.Rows.Add(auxRow)
            End If
        Next

        For Each auxRow In auxDataTable.Rows
            For i As Long = 1 To auxRow.Item("LINEAS")
                If dsInvisibles.Tables(1).Select("LINEA=" + i.ToString()).Length > 0 Then
                    For Each dDefaultRow In dsInvisibles.Tables(1).Select("CAMPO_PADRE = " + auxRow.Item("CAMPO_PADRE").ToString + " AND VISIBLE = 0 AND LINEA = 1")
                        If ds.Tables("LINEAS_DESGLOSE").Select("LINEA=" + i.ToString + " AND CAMPO_PADRE = " + dDefaultRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO = " + dDefaultRow.Item("CAMPO_HIJO").ToString).Length = 0 Then
                            dNewRow = ds.Tables("LINEAS_DESGLOSE").NewRow
                            dNewRow.Item("LINEA") = i
                            dNewRow.Item("CAMPO_PADRE") = dDefaultRow.Item("CAMPO_PADRE").ToString
                            dNewRow.Item("CAMPO_HIJO") = dDefaultRow.Item("CAMPO_HIJO")
                            dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("VALOR_TEXT")
                            If (IsDBNull(dDefaultRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dDefaultRow.Item("LISTA_TEXT"))) Then
                                dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("LISTA_TEXT")
                            End If

                            Try
                                dNewRow.Item("VALOR_NUM") = dDefaultRow.Item("VALOR_NUM")
                                dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dDefaultRow.Item("VALOR_NUM")), DBNullToSomething(dDefaultRow.Item("VALOR_FEC")))
                                dNewRow.Item("VALOR_BOOL") = dDefaultRow.Item("VALOR_BOOL")
                            Catch ex As Exception

                            End Try
                            Try
                                'caso Presupuesto nivel 4: Campo Oculto que controla por otro lado el maper.
                                'Intenta meter algo que ya he metido. Solo desglose popup.       
                                ds.Tables("LINEAS_DESGLOSE").Rows.Add(dNewRow)
                            Catch ex As Exception
                            End Try
                        Else
                            dNewRow = ds.Tables("LINEAS_DESGLOSE").Select("LINEA=" + i.ToString + " AND CAMPO_PADRE = " + dDefaultRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO = " + dDefaultRow.Item("CAMPO_HIJO").ToString)(0)
                            If DBNullToSomething(dDefaultRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Material And DBNullToSomething(dDefaultRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Unidad Then
                                dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("VALOR_TEXT")
                            End If

                            If (IsDBNull(dDefaultRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dDefaultRow.Item("LISTA_TEXT"))) Then
                                dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("LISTA_TEXT")
                            End If

                            Try
                                dNewRow.Item("VALOR_NUM") = dDefaultRow.Item("VALOR_NUM")
                                dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dDefaultRow.Item("VALOR_NUM")), DBNullToSomething(dDefaultRow.Item("VALOR_FEC")))
                                dNewRow.Item("VALOR_BOOL") = dDefaultRow.Item("VALOR_BOOL")
                            Catch ex As Exception

                            End Try

                        End If

                    Next
                Else
                    For Each dDefaultRow In dsInvisibles.Tables(1).Select("CAMPO_PADRE = " + auxRow.Item("CAMPO_PADRE").ToString + " AND VISIBLE = 0")
                        If ds.Tables("LINEAS_DESGLOSE").Select("LINEA=" + i.ToString + " AND CAMPO_PADRE = " + dDefaultRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO = " + dDefaultRow.Item("CAMPO_HIJO").ToString & " AND LINEA_OLD IS NULL").Length = 0 Then
                            dNewRow = ds.Tables("LINEAS_DESGLOSE").NewRow
                            dNewRow.Item("LINEA") = i
                            dNewRow.Item("CAMPO_PADRE") = dDefaultRow.Item("CAMPO_PADRE").ToString
                            dNewRow.Item("CAMPO_HIJO") = dDefaultRow.Item("CAMPO_HIJO")
                            dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("VALOR_TEXT")
                            If (IsDBNull(dDefaultRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dDefaultRow.Item("LISTA_TEXT"))) Then
                                dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("LISTA_TEXT")
                            End If

                            Try
                                dNewRow.Item("VALOR_NUM") = dDefaultRow.Item("VALOR_NUM")
                                dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dDefaultRow.Item("VALOR_NUM")), DBNullToSomething(dDefaultRow.Item("VALOR_FEC")))
                                dNewRow.Item("VALOR_BOOL") = dDefaultRow.Item("VALOR_BOOL")
                            Catch ex As Exception

                            End Try

                            Try
                                ds.Tables("LINEAS_DESGLOSE").Rows.Add(dNewRow)
                            Catch ex As Exception

                            End Try


                        End If

                        'A�adir unicamente el adjunto
                        'Obtener el adjunto de la primero fila
                        If Not IsDBNull(dDefaultRow.Item("VALOR_TEXT")) Then
                            dRows2 = ds.Tables("TEMPDESGLOSEADJUN").Select("LINEA=1 AND CAMPO_PADRE = " + dDefaultRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO = " + dDefaultRow.Item("CAMPO_HIJO").ToString)

                            If dRows2.Length > 0 Then
                                'hay algun adjunto
                                dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow
                                dNewRow.Item("LINEA") = i
                                dNewRow.Item("CAMPO_PADRE") = dDefaultRow.Item("CAMPO_PADRE").ToString
                                dNewRow.Item("CAMPO_HIJO") = dDefaultRow.Item("CAMPO_HIJO")
                                dNewRow.Item("TIPO") = 1
                                dNewRow.Item("ID") = dRows2(0).Item("ID")
                                ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)

                            End If
                        End If

                    Next
                End If
            Next
        Next

    End Sub

End Class


