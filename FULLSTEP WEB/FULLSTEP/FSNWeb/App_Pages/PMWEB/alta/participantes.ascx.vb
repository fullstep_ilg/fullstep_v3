
    Public Class participantes
        Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents tblDesglose As System.Web.UI.WebControls.Table

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

    Private moTextos As DataTable
    Private msTabContainer As String
    Private msIdi As String
    Private mdsParticipantes As DataSet
    Private mbSoloProveedores As Boolean
    Private mlInstancia As Long

#Region "PROPIEDADES"
    Property Idi() As String
        Get
            Idi = msIdi
        End Get
        Set(ByVal Value As String)
            msIdi = Value
        End Set
    End Property
    Property TabContainer() As String
        Get
            TabContainer = msTabContainer
        End Get
        Set(ByVal Value As String)
            msTabContainer = Value
        End Set
    End Property
    Property Textos() As DataTable
        Get
            Textos = moTextos
        End Get
        Set(ByVal Value As DataTable)
            moTextos = Value
        End Set
    End Property
    Property dsParticipantes() As DataSet
        Get
            dsParticipantes = mdsParticipantes
        End Get
        Set(ByVal Value As DataSet)
            mdsParticipantes = Value
        End Set
    End Property
#End Region
    Public Sub New(Optional ByVal lId As Long = Nothing, Optional ByRef oDS As DataSet = Nothing, Optional ByVal sIdi As String = Nothing)
        If lId <> Nothing Then msIdi = sIdi
    End Sub
    ''' Revisado por: blp. Fecha: 05/10/2011
    ''' <summary>
    ''' Carga los input/entrys para un tab "Participantes" de una solicitud de compra
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">evento de sistema</param>  
    ''' <remarks>Llamada desde: NWAlta.aspx/Page_Load       NWDetalleSolicitud.aspx/Page_Load
    '''     NWgestionInstancia.aspx/Page_Load; Tiempo m�ximo: 0,1</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Page.IsPostBack Then Exit Sub

        Dim otblRow, oRowHeader As System.Web.UI.WebControls.TableRow
        Dim otblCell, oCellHeader As System.Web.UI.WebControls.TableCell
        Dim oLbl As System.Web.UI.HtmlControls.HtmlGenericControl
        Dim oDSRow As System.Data.DataRow
        Dim olabel As System.Web.UI.WebControls.Label
        Dim oFSEntry As DataEntry.GeneralEntry
        Dim sClase, sKeyRol As String
        Dim iFila, iCampo, iInicio, iFin As Integer
        Dim mipage As FSNPage
        sKeyRol = String.Empty
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "claveArrayEntrys1") Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "claveArrayEntrys1", "<script>var arrEntrys_1" + " = new Array()</script>")
        End If

        mipage = CType(Page, FSNPage)
        mipage.ModuloIdioma = TiposDeDatos.ModulosIdiomas.CertifProveedores

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "ClienteID") Then _
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "ClienteID", "<script>var vClientIDCertif ='" + Me.ClientID + "'</script>")

        'A�ade las cabeceras 
        oRowHeader = New System.Web.UI.WebControls.TableRow
        tblDesglose.Rows.Add(oRowHeader)

        oCellHeader = New System.Web.UI.WebControls.TableCell
        oCellHeader.ID = "ETA"
        oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
        oLbl.InnerHtml = mipage.Textos(5)
        oCellHeader.Controls.Add(oLbl)
        oCellHeader.Attributes("class") = "captionBlueUnderline"
        oRowHeader.Cells.Add(oCellHeader)

        oCellHeader = New System.Web.UI.WebControls.TableCell
        oCellHeader.ID = "ROL"
        oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
        oLbl.InnerHtml = mipage.Textos(6)
        oCellHeader.Controls.Add(oLbl)
        oCellHeader.Attributes("class") = "captionBlueUnderline"
        oRowHeader.Cells.Add(oCellHeader)

        oCellHeader = New System.Web.UI.WebControls.TableCell
        oCellHeader.ID = "PAR"
        oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
        oLbl.InnerHtml = mipage.Textos(7)
        oCellHeader.Controls.Add(oLbl)
        oCellHeader.Attributes("class") = "captionBlueUnderline"
        oRowHeader.Cells.Add(oCellHeader)

        Dim oHid As New System.Web.UI.HtmlControls.HtmlInputHidden
        oHid.ID = "numRows"
        oHid.Value = mdsParticipantes.Tables(0).Rows.Count
        Me.Controls.Add(oHid)

        'ahora va generando los participantes:
        sClase = "ugfilatabla"
        iInicio = 1
        iFin = 3
        iFila = 0
        otblRow = Nothing
        For Each oDSRow In mdsParticipantes.Tables(0).Rows
            iFila = iFila + 1
            For iCampo = iInicio To iFin
                Select Case iCampo
                    Case 1
                        'Nombre del bloque
                        otblRow = New System.Web.UI.WebControls.TableRow
                        otblCell = New System.Web.UI.WebControls.TableCell
                        otblCell.Width = Unit.Percentage(25)
                        olabel = New System.Web.UI.WebControls.Label
                        olabel.Text = "(*)" + oDSRow.Item("BDEN")
                        olabel.Style("font-weight") = "bold"
                        otblCell.Controls.Add(olabel)
                        otblCell.CssClass = sClase
                        otblRow.Cells.Add(otblCell)
                    Case 2
                        'Nombre del rol
                        otblCell = New System.Web.UI.WebControls.TableCell
                        otblCell.Width = Unit.Percentage(25)
                        oFSEntry = New DataEntry.GeneralEntry
                        oFSEntry.Width = Unit.Percentage(100)
                        oFSEntry.ID = "fsentryROL" + oDSRow.Item("ROL").ToString
                        oFSEntry.Tag = "ROL"
                        oFSEntry.ActivoSM = CType(Me.Page, FSNPage).Acceso.gbAccesoFSSM
                        oFSEntry.IdContenedor = Me.ClientID

                        Dim pag As FSNPage = Me.Page
                        oFSEntry.Title = Page.Title
                        oFSEntry.Intro = 0
                        oFSEntry.Idi = msIdi
                        oFSEntry.TabContainer = msTabContainer
                        oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.Rol
                        oFSEntry.ReadOnly = 1
                        oFSEntry.Obligatorio = 0
                        oFSEntry.Calculado = 0
                        oFSEntry.Intro = 0
                        oFSEntry.Tag = ""
                        oFSEntry.NumberFormat = mipage.FSNUser.NumberFormat
                        oFSEntry.DateFormat = mipage.FSNUser.DateFormat
                        oFSEntry.Tipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
                        oFSEntry.MaxLength = 800
                        oFSEntry.Valor = oDSRow.Item("ROL")
                        oFSEntry.Text = oDSRow.Item("RDEN")
                        oFSEntry.LabelStyle.CssClass = "CampoSoloLectura"
                        oFSEntry.InputStyle.CssClass = "trasparent"

                        otblCell.Controls.Add(oFSEntry)

                        otblCell.CssClass = sClase
                        otblRow.Cells.Add(otblCell)

                        otblRow.Height = Unit.Pixel(20)
                        sKeyRol = oFSEntry.ClientID
                        tblDesglose.Rows.Add(otblRow)
                    Case 3
                        otblCell = New System.Web.UI.WebControls.TableCell
                        otblCell.Width = Unit.Percentage(50)
                        oFSEntry = New DataEntry.GeneralEntry
                        oFSEntry.Width = Unit.Percentage(100)
                        oFSEntry.ID = "fsentry" + "PART" + iFila.ToString
                        oFSEntry.Tag = "PART"
                        oFSEntry.IdContenedor = Me.ClientID
                        oFSEntry.ActivoSM = CType(Me.Page, FSNPage).Acceso.gbAccesoFSSM

						oFSEntry.Title = oDSRow.Item("BDEN")
						oFSEntry.Idi = msIdi
                        oFSEntry.TabContainer = msTabContainer
                        If oDSRow.Item("TIPO") = 2 Then
                            oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.ProveContacto
                            oFSEntry.Intro = 1
                        Else
                            oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.Persona
                            oFSEntry.Intro = 1

                            Dim oParticipantes As FSNServer.Participantes
                            oParticipantes = mipage.FSNServer.Get_Object(GetType(FSNServer.Participantes))
                            oParticipantes.Rol = oDSRow.Item("ROL").ToString
                            oParticipantes.Instancia = Request("Instancia")
                            oParticipantes.LoadDataPersonas()

                            oFSEntry.Lista = oParticipantes.Data
                        End If
                        oFSEntry.ReadOnly = 0
                        oFSEntry.Obligatorio = 1
                        oFSEntry.Calculado = 0
                        oFSEntry.NumberFormat = mipage.FSNUser.NumberFormat
                        oFSEntry.DateFormat = mipage.FSNUser.DateFormat
                        oFSEntry.Tipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
                        oFSEntry.Tag = ""

                        If DBNullToSomething(oDSRow.Item("PARTNOM")) <> "  ()" Then
                            If oDSRow.Item("TIPO") = 2 Then
                                If Not IsDBNull(oDSRow.Item("PART")) Then
                                    oFSEntry.Valor = oDSRow.Item("PART") + "###" + DBNullToStr(oDSRow.Item("ID_CONTACTO"))
                                Else
                                    oFSEntry.Valor = DBNullToSomething(oDSRow.Item("PART"))
                                End If
                            Else
                                oFSEntry.Valor = DBNullToSomething(oDSRow.Item("PART"))
                            End If
                            oFSEntry.Text = DBNullToSomething(oDSRow.Item("PARTNOM"))
                        End If

                        If oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.ProveContacto Then 'proveedor
                            oFSEntry.InputStyle.CssClass = "TipoTextoMedio"
                        End If
                        oFSEntry.MaxLength = 800

                        oFSEntry.DependentField = sKeyRol
                        If Page.IsPostBack Then
                            If (Request.Form.AllKeys.Contains(oFSEntry.SubmitKey)) Then
                                oFSEntry.Valor = Request(oFSEntry.SubmitKey)
                            End If
                        End If
                        otblCell.Controls.Add(oFSEntry)
                        otblCell.CssClass = sClase
                        otblRow.Cells.Add(otblCell)
                        otblRow.Height = Unit.Pixel(20)

                        tblDesglose.Rows.Add(otblRow)
                End Select
            Next 'iCampo
        Next 'rows
    End Sub
    End Class

