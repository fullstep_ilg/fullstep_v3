
    Public Class rptParams
    Inherits FSNPage

        Private report As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        Private vEstructura(2) As String

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
        Protected WithEvents lblParam As System.Web.UI.WebControls.Label
        Protected WithEvents lblNoExiste As System.Web.UI.WebControls.Label
        Protected WithEvents tblParametros As System.Web.UI.WebControls.Table
        Protected WithEvents tblRptParams As System.Web.UI.HtmlControls.HtmlTable
        Protected WithEvents cmdObtener As System.Web.UI.HtmlControls.HtmlInputButton
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim otblRow As System.Web.UI.WebControls.TableRow
            Dim otblCell As System.Web.UI.webControls.TableCell
            Dim oRowHeader As System.Web.UI.webControls.TableRow
            Dim oCellHeader As System.Web.UI.WebControls.TableCell
            Dim oLabel As System.Web.UI.WebControls.Label
            Dim sTabContainer As String
            Dim oFSEntry As DataEntry.GeneralEntry
            Dim dsParametros As New DataSet
            Dim oRow(0)

            dsParametros.Tables.Add("Parametros")
            dsParametros.Tables("Parametros").Columns.Add("Valor1")
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
            Dim oDict As FSNServer.Dictionary = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))

        Dim sIdi As String = Session("FSN_User").Idioma.ToString()
        Dim oUser As FSNServer.User
        oUser = Session("FSN_User")

            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If

            If Not Page.IsPostBack Then
                oDict.LoadData(TiposDeDatos.ModulosIdiomas.Listados, sIdi)
                Dim oTextos As DataTable = oDict.Data.Tables(0)

                Me.lblTitulo.Text = oTextos.Rows(0).Item(1) & Request("ReportName")
                Me.lblParam.Text = oTextos.Rows(1).Item(1)
                'Cabeceras de los Parametros
                oRowHeader = New System.Web.UI.webControls.TableRow
                otblRow = New System.Web.UI.webControls.TableRow
                tblParametros.Rows.Add(oRowHeader)
                'Parámetro
                oCellHeader = New System.Web.UI.webControls.TableCell
                oCellHeader.ID = "Nombre"
                oCellHeader.Width = Unit.Percentage(70)
                oCellHeader.BorderStyle = BorderStyle.Solid
                oCellHeader.BorderWidth = Unit.Pixel(1)
                oLabel = New System.Web.UI.WebControls.Label
                oLabel.Text = oTextos.Rows(2).Item(1)

                oCellHeader.Controls.Add(oLabel)
                oRowHeader.Cells.Add(oCellHeader)

                'Valor
                oCellHeader = New System.Web.UI.webControls.TableCell
                oCellHeader.ID = "Valor"
                oCellHeader.Width = Unit.Percentage(30)
                oCellHeader.BorderStyle = BorderStyle.Solid
                oCellHeader.BorderWidth = Unit.Pixel(1)
                oLabel = New System.Web.UI.WebControls.Label
                oLabel.Text = oTextos.Rows(3).Item(1)
                oCellHeader.Controls.Add(oLabel)

                oRowHeader.HorizontalAlign = HorizontalAlign.Center
                oRowHeader.Attributes("Class") = "VisorHead"
                oRowHeader.Cells.Add(oCellHeader)

                Me.cmdObtener.value = oTextos.Rows(4).Item(1)
                Me.lblNoExiste.Text = oTextos.Rows(5).Item(1)
            End If

            ' Leemos los parámetros que tiene el listado

            Dim crParam As CrystalDecisions.shared.ParameterField
            Dim oSubreport As CrystalDecisions.CrystalReports.Engine.ReportDocument
            Dim sFileName As String

        sFileName = ConfigurationManager.AppSettings("rptFilesPathPMWEB") & "/" & Request("FileReport")

        Dim oFile As System.IO.File
            If oFile.Exists(sFileName) = False Then
                Me.tblRptParams.Visible = False
                Me.cmdObtener.Visible = False
                Me.lblNoExiste.Visible = True
                Exit Sub
            Else
                Me.lblNoExiste.Visible = False
            End If

            report.Load(sFileName, CrystalDecisions.[Shared].OpenReportMethod.OpenReportByTempCopy)

            'Obtiene los datos de conexión
            FSWSServer.get_PropiedadesConexion()
            report.SetDatabaseLogon(FSWSServer.DBLogin, FSWSServer.DBPassword, FSWSServer.DBServidor, FSWSServer.DBName)

            For Each oSubreport In report.Subreports
                oSubreport.SetDatabaseLogon(FSWSServer.DBLogin, FSWSServer.DBPassword, FSWSServer.DBServidor, FSWSServer.DBName)
            Next
            Dim cont As Integer
            cont = 0
            If Not Page.IsPostBack Then
                If report.ParameterFields.Count > 0 Then
                    For Each crParam In report.ParameterFields

                        If report.Subreports.Item(crParam.ReportName) Is Nothing Then
                            'Escribir el Nombre del parámetro
                            otblRow = New System.Web.UI.webControls.TableRow
                            otblCell = New System.Web.UI.webControls.TableCell

                            oLabel = New System.Web.UI.WebControls.Label
                            oLabel.Text = " " & crParam.PromptText
                            oLabel.Font.Name = "Verdana"
                            oLabel.Font.Size = System.Web.UI.WebControls.FontUnit.Point(8)

                            otblCell.Controls.Add(oLabel)
                            otblCell.BorderStyle = BorderStyle.Solid
                            otblCell.BorderWidth = Unit.Pixel(1)
                            otblCell.Attributes("Class") = "VisorFrame"
                            otblRow.Cells.Add(otblCell)

                            Dim crParameterFieldDefinitions As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinitions
                            crParameterFieldDefinitions = report.DataDefinition.ParameterFields()
                            sTabContainer = Me.tblParametros.ID
                            otblCell = New System.Web.UI.webControls.TableCell
                        oFSEntry = New DataEntry.GeneralEntry
                        oFSEntry.Title = Me.Page.Title

                            With oFSEntry
                                .Tag = "List_" & cont
                                .ID = "fsentry" & "_List_" & cont
                                .Calculado = False
                                .ReadOnly = False
                                .Obligatorio = False
                                .TipoGS = TiposDeDatos.TipoCampoGS.ListadosPersonalizados
                                .Tipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
                                .NumberFormat = oUser.NumberFormat
                                .DateFormat = oUser.DateFormat
                                .Idi = sIdi
                            End With
                        
                            If crParam.DefaultValues.Count > 0 Then
                                'Tiene valores Predefinidos
                                Dim j As Integer = 0
                                dsParametros.Reset()
                                dsParametros.Tables.Add("Parametros")
                                dsParametros.Tables("Parametros").Columns.Add("Valor1")
                                For j = 0 To crParam.DefaultValues.Count - 1
                                    oRow(0) = crParameterFieldDefinitions.Item(cont).DefaultValues(j).Description
                                    dsParametros.Tables("Parametros").Rows.Add(oRow)
                                Next
                                With oFSEntry
                                    .Intro = 1
                                    .TabContainer = sTabContainer
                                    .Lista = New DataSet
                                    .Lista.Merge(dsParametros)
                                    .Width = Unit.Percentage(100)
                                End With
                        Else
                            If Comprobar_estructura(crParam.Name) > 0 Then
                                'Buscar en la BBDD
                                ReDim Preserve vEstructura(2)
                                Dim oListados As FSNServer.Listados
                                oListados = FSWSServer.Get_Object(GetType(FSNServer.Listados))
                                dsParametros = oListados.CargarDatos_TablaIndefinida(vEstructura(0), vEstructura(1), vEstructura(2))
                                oListados = Nothing

                                With oFSEntry
                                    If dsParametros.Tables.Count > 0 Then
                                        .Intro = 1
                                        .Lista = New DataSet
                                        .Lista.Merge(dsParametros)
                                    Else 'Si no ha encontrado la tabla con los campos en la BBDD que nos muestre una caja de texto
                                        .Intro = 0
                                    End If
                                    .TabContainer = sTabContainer
                                    .Width = Unit.Percentage(100)

                                End With
                            Else
                                'El parametro es de tipo Texto                                   
                                With oFSEntry
                                    .Width = Unit.Pixel(250)
                                End With
                            End If
                            End If

                            otblCell.Controls.Add(oFSEntry)
                            otblCell.BorderStyle = BorderStyle.Solid
                            otblCell.BorderWidth = Unit.Pixel(1)
                            otblRow.Cells.Add(otblCell)
                            otblRow.Height = Unit.Pixel(20)

                            tblParametros.Rows.Add(otblRow)
                            cont = cont + 1
                        End If

                    Next
                Else
                    rptViewer.report = report
                    Response.Redirect("rptViewer.aspx")
                End If
            End If
            report.Close()
            report.Dispose()

            Page.ClientScript.RegisterStartupScript(Me.GetType(),"claveStartUpScript", "<script>EstablecernParametros(" & cont & ")</script>")
        End Sub


        Private Function Comprobar_estructura(ByVal sVal As String) As Byte
            vEstructura = Split(sVal, ".")
            Comprobar_estructura = UBound(vEstructura)
        End Function


    End Class


