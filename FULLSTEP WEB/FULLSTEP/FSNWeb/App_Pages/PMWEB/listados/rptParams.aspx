<%@ Page Language="vb" AutoEventWireup="false" Codebehind="rptParams.aspx.vb" Inherits="Fullstep.FSNWeb.rptParams" buffer="False"%>
<%@ Register TagPrefix="igtab" Namespace="Infragistics.WebUI.UltraWebTab" Assembly="Infragistics.WebUI.UltraWebTab.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<%@ Register TagPrefix="igtbl" Namespace="Infragistics.WebUI.UltraWebGrid" Assembly="Infragistics.WebUI.UltraWebGrid.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="True" name="vs_showGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	
	<script>
		var nParametros
		var oGridsDesglose
		
		oGridsDesglose = false		
		
		function ObtenerInforme() {				
			oFrm = MontarFormulario()
			oFrm.submit()			
		}
		
		function EstablecernParametros(nParam) {
			nParametros = nParam;
						
		}
		function MontarFormulario() {			
			
			document.body.insertAdjacentHTML("beforeEnd","<form name=frmObtenerInforme id=frmObtenerInforme action=ObtenerInforme.aspx method=post style='visibility:hidden;position:absolute;top:0;left:0'></form>")
		    oFrm = document.getElementById("frmObtenerInforme")
		    
		    sInput ="<INPUT id=FileReport type=hidden NAME=FileReport>"
		    oFrm.insertAdjacentHTML("beforeEnd",sInput)
		    oFrm.item("FileReport").value="<%=request("FileReport")%>"
			for (i=0;i<nParametros;i++) {
				oEntry = fsGeneralEntry_getById("fsentry_List_"+i)
				sInput ="<INPUT id=FSEntry_List_0 type=hidden NAME=FSEntry_List_"+i+">"
				oFrm.insertAdjacentHTML("beforeEnd",sInput)			
				oFrm.item("FSEntry_List_"+i).value=oEntry.getValue()				
			}
			return oFrm
		}
		
	</script>
	<body>
		<form id="Form1" method="post" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
			    <CompositeScript>
                    <Scripts>
                        <asp:ScriptReference Path="../alta/js/jsAlta.js" />
                    </Scripts>
                </CompositeScript>
			</asp:ScriptManager>
			<asp:label id="lblTitulo" style="Z-INDEX: 101; LEFT: 16px; POSITION: absolute; TOP: 40px" runat="server"
				CssClass="titulo" Height="3px" Width="100%">DListado personalizado:</asp:label><uc1:menu id="Menu1" runat="server"></uc1:menu>
			<TABLE id="tblRptParams" style="Z-INDEX: 102; LEFT: 40px; POSITION: absolute; TOP: 80px"
				height="70%" cellSpacing="1" cellPadding="1" width="80%" border="0" runat="server">
				<TR>
					<TD colSpan="2" height="20"><asp:label id="lblParam" runat="server" CssClass="fntLogin">DIntroduzca los siguientes parámetros para el listado:</asp:label></TD>
				</TR>
				<TR vAlign="top">
					<TD><asp:table id="tblParametros" runat="server" CellSpacing="0" CellPadding="0" Font-Names="Verdana"
							Font-Size="8pt"></asp:table></TD>
				</TR>
				<TR>
					<TD vAlign="bottom" align="center" colSpan="2" height="20" rowSpan="1"><input class="botonPMWEB" id="cmdObtener" onclick="return ObtenerInforme()" type="button" value="DObtener"
							name="cmdObtener" runat="server"></TD>
				</TR>
			</TABLE>
			<asp:label id="lblNoExiste" style="Z-INDEX: 103; LEFT: 40px; POSITION: absolute; TOP: 80px"
				runat="server" CssClass="SinDatos" Height="24px" Width="80%">lblNoExiste</asp:label></form>
	</body>
</html>
