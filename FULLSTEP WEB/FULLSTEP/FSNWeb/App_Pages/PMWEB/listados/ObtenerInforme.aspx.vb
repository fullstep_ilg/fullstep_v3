Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class ObtenerInforme
    Inherits FSNPage
    Private report As New CrystalDecisions.CrystalReports.Engine.ReportDocument

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents crViewer1 As CrystalDecisions.Web.CrystalReportViewer

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Or (Request("FileReport") = "ValePedido.rpt" Or Request("FileReport") = "PedidoExpress.rpt") Then
            Cache.Remove("ReportPM" & Me.Usuario.Cod)
            'Put user code to initialize the page here
            ' Pasamos los valores de los par�metros al listado
            Dim crParam As CrystalDecisions.Shared.ParameterField
            Dim iFila As Integer = 0
            Dim sFileName As String
            Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
            Dim oUser As FSNServer.User = Session("FSN_User")
            Dim sIdi As String = oUser.Idioma.ToString()
            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If

            Dim oDict As FSNServer.Dictionary = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))
            oDict.LoadData(TiposDeDatos.ModulosIdiomas.ImposibleAccion, sIdi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)
            Dim blnHayOblig As Boolean 'para comprobar si hay parametros obligatorios vacios

            sFileName = ConfigurationManager.AppSettings("rptFilesPathPMWEB") & "/" & Request("FileReport")

            Try

                If Dir(sFileName) <> "" Then
                    report.Load(sFileName, CrystalDecisions.[Shared].OpenReportMethod.OpenReportByTempCopy)

                    Dim lInstancia As Long
                    lInstancia = Request("Instancia")

                    'Obtiene los datos de conexi�n
                    FSWSServer.get_PropiedadesConexion()
                    report.SetDatabaseLogon(FSWSServer.DBLogin, FSWSServer.DBPassword, FSWSServer.DBServidor, FSWSServer.DBName)

                    If lInstancia <> 0 Then
                        For Each crParam In report.ParameterFields
                            If report.Subreports.Item(crParam.ReportName) Is Nothing Then
                                report.SetParameterValue(crParam.Name, CLng(lInstancia))
                            End If
                        Next

                        Dim crtableLogoninfos As New TableLogOnInfos
                        Dim crtableLogoninfo As New TableLogOnInfo
                        Dim myConnectionInfo As New ConnectionInfo
                        Dim CrTables As Tables
                        Dim CrTable As Table

                        With myConnectionInfo
                            .ServerName = FSWSServer.DBServidor
                            .DatabaseName = FSWSServer.DBName
                            .UserID = FSWSServer.DBLogin
                            .Password = FSWSServer.DBPassword
                        End With

                        'Recorremos las tablas del Report (los comandos est�n incluidos) 
                        'y les asignamos los par�metros de conexi�n
                        CrTables = report.Database.Tables
                        For Each CrTable In CrTables
                            crtableLogoninfo = CrTable.LogOnInfo
                            crtableLogoninfo.ConnectionInfo = myConnectionInfo
                            CrTable.ApplyLogOnInfo(crtableLogoninfo)
                        Next

                        ' Pasamos el listado, ya con los datos de conexi�n y par�metros al formulario con el visor.
                        crViewer1.ReportSource = report

                        ' Pasamos las propiedades de conexi�n al Visor
                        SetDBLogonForReport(myConnectionInfo)

                        crViewer1.Visible = True
                    Else
                        blnHayOblig = False
                        For Each crParam In report.ParameterFields
                            If report.Subreports.Item(crParam.ReportName) Is Nothing Then
                                If Left(crParam.PromptText, 3) = "(*)" Then
                                    'campos obligatorios
                                    If Request("fsentry_List_" & iFila) <> "" Then
                                        Select Case crParam.ParameterValueType
                                            Case CrystalDecisions.[Shared].ParameterValueKind.NumberParameter
                                                report.SetParameterValue(crParam.Name, CLng(Request("fsentry_List_" & iFila)))
                                            Case CrystalDecisions.[Shared].ParameterValueKind.DateParameter, CrystalDecisions.[Shared].ParameterValueKind.DateTimeParameter
                                                report.SetParameterValue(crParam.Name, CDate(Request("fsentry_List_" & iFila)))
                                            Case Else
                                                report.SetParameterValue(crParam.Name, Request("fsentry_List_" & iFila))
                                        End Select
                                    Else
                                        blnHayOblig = True
                                        Exit For
                                    End If
                                Else
                                    Select Case crParam.ParameterValueType
                                        Case CrystalDecisions.[Shared].ParameterValueKind.NumberParameter
                                            report.SetParameterValue(crParam.Name, CLng(IIf(Request("fsentry_List_" & iFila) <> "", Request("fsentry_List_" & iFila), 0)))
                                        Case CrystalDecisions.[Shared].ParameterValueKind.DateParameter, CrystalDecisions.[Shared].ParameterValueKind.DateTimeParameter
                                            report.SetParameterValue(crParam.Name, CDate(Request("fsentry_List_" & iFila)))
                                        Case Else
                                            report.SetParameterValue(crParam.Name, Request("fsentry_List_" & iFila))
                                    End Select
                                End If
                                iFila = iFila + 1
                            End If

                        Next
                        If blnHayOblig Then
                            'Si quedan parametros obligatorios sin rellenar
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alerta", "<script>alert('" & oTextos.Rows(40).Item(1) & "');</script>")
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Vuelve", "<script>history.go(-1);</script>")
                        Else
                            ' Pasamos el listado, ya con los datos de conexi�n y par�metros
                            ' al formulario con el visor.
                            rptViewer.report = report
                            Response.Redirect("rptViewer.aspx")
                        End If
                    End If
                Else
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alerta", "<script>alert('" & oTextos.Rows(39).Item(1) & "');</script>")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Cierra", "<script>window.close();</script>")
                End If

            Catch ex As Exception
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alerta", "<script>alert('" & oTextos.Rows(38).Item(1) & "');</script>")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Vuelve", "<script>history.go(-1);</script>")
                Throw ex
            End Try
            Me.InsertarEnCache("ReportPM" & Me.Usuario.Cod, report)
        Else
            report = Cache("ReportPM" & Me.Usuario.Cod)

            For Each crParam As CrystalDecisions.Shared.ParameterField In report.ParameterFields
                If report.Subreports.Item(crParam.ReportName) Is Nothing Then
                    report.SetParameterValue(crParam.Name.ToString, crParam.CurrentValues(0))
                End If
            Next
            Dim lInstancia As Long
            lInstancia = Request("Instancia")
            If lInstancia <> 0 Then
                crViewer1.ReportSource = report
            Else
                rptViewer.report = report
                Response.Redirect("rptViewer.aspx")
            End If
        End If
    End Sub

    ''' <summary>
    ''' Procedimiento que vincula el visor de Crystal con los par�metros de conexi�n
    ''' </summary>
    ''' <param name="myConnectionInfo">Informaci�n de conexi�n (Server, BBDD, User, Pwd)</param>
    ''' <remarks>
    ''' Llamada desde: cmdActualizar_Click de Report.aspx
    ''' Tiempo m�ximo: 0 seg
    ''' </remarks>
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo)

        Dim myTableLogOnInfos As TableLogOnInfos = crViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub

    Private Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Dim sName As String
        sName = report.FileName

        If InStr(1, sName, "ValePedido.rpt") >= 1 Or InStr(1, sName, "PedidoExpress.rpt") >= 1 Then
            report.Close()
            report.Dispose()
        End If
    End Sub
End Class

