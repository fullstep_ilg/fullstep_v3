
    Public Class publicadoOK
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblSolicitud As System.Web.UI.WebControls.Label
    Protected WithEvents lblSolicitudValue As System.Web.UI.WebControls.Label
    Protected WithEvents lblPeticionario As System.Web.UI.WebControls.Label
    Protected WithEvents lblPeticionarioValue As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecPub As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecPubValue As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecDespub As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecDespubValue As System.Web.UI.WebControls.Label
    Protected WithEvents lblProveedores As System.Web.UI.WebControls.Label
    Protected WithEvents lblProveedoresValue As System.Web.UI.WebControls.Label
    Protected WithEvents tblProv As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents FSNPageHeader As Fullstep.FSNWebControls.FSNPageHeader
    Protected WithEvents lblEnvioSatisfactorio As System.Web.UI.WebControls.Label
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' <summary>
    ''' Pantalla para informar de q ha ido bien la publicaci�n
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>Llamada desde: La propia p�gina, cada vez que se carga
    ''' Tiempo m�ximo:0,1</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim oInstancia As FSNServer.Instancia
        Dim oCertificado As FSNServer.Certificado

        Dim sIdi = FSNUser.Idioma.ToString()

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.CertifPublicadoOK

        'MONTAR CABECERA
        '===============
        FSNPageHeader.VisibleBotonVolver = True
        FSNPageHeader.TextoBotonVolver = Textos(6)
        FSNPageHeader.OnClientClickVolver = "window.open(""" & Replace(Request.QueryString("Volver"), "**", "&") & """,""_top"");return false;"

        FSNPageHeader.TituloCabecera = Textos(9)

        lblEnvioSatisfactorio.Text = Textos(7)

        Me.lblProveedores.Text = Textos(5)

        If Request("DesdeCertificado") <> "1" Then
            FSNPageHeader.VisibleBotonVolver = False
        End If

        'Carga los datos de la instancia:
        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))

        Dim listaInstanciasRequest As String = Request("Instancia")
        Dim listaInstancias As String() = Split(listaInstanciasRequest, "@")
        Dim listaInstanciasMostrar As String = Replace(listaInstanciasRequest, "@", ", ")

        oInstancia.ID = listaInstancias(0)
        oInstancia.Load(sIdi)

        oCertificado = FSNServer.Get_Object(GetType(FSNServer.Certificado))
        oCertificado.Instancia = oInstancia.ID
        oCertificado.CargarDatosGenerales()

        'Datos de la instancia:
        Me.lblSolicitud.Text = Textos(8) & " "
        Me.lblSolicitudValue.Text = listaInstanciasMostrar
        Me.lblPeticionario.Text = Textos(2) & " "
        Me.lblPeticionarioValue.Text = oInstancia.NombrePeticionario
        Me.lblFecPub.Text = Textos(3) & " "
        Me.lblFecPubValue.Text = FormatDate(oCertificado.FechaPub, FSNUser.DateFormat)
        If IsTime(oCertificado.FechaDesPub) Then
            Me.lblFecDespub.Text = ""
            Me.lblFecDespubValue.Text = ""
        Else
            Me.lblFecDespub.Text = Textos(4) & " "
            Me.lblFecDespubValue.Text = FormatDate(oCertificado.FechaDesPub, FSNUser.DateFormat)
        End If

        Dim oRow As DataRow
        Dim oCelda As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oFila As System.Web.UI.HtmlControls.HtmlTableRow
        For i As Integer = 0 To UBound(listaInstancias)
            If i > 0 Then
                oCertificado.Instancia = listaInstancias(i)
                oCertificado.CargarDatosGenerales()
            End If

            oRow = oCertificado.Proveedores.Rows(0)

            'Ahora a�ade el detalle de los items:
            oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = DBNullToSomething(oRow.Item("COD")) & " - " & DBNullToSomething(oRow.Item("DEN"))
            oFila.Cells.Add(oCelda)

            oFila.Attributes("class") = "captionNegritaNegro"

            Me.tblProv.Rows.Add(oFila)
        Next

        oInstancia = Nothing
        oCertificado = Nothing

    End Sub

    End Class

