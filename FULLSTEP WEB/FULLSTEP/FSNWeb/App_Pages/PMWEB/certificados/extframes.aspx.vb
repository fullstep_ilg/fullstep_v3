
    Public Class extframescertificados
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents fraWS As System.Web.UI.HtmlControls.HtmlGenericControl

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

    ''' <summary>
    ''' Pantalla para cargar el Visor de certificados
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>Llamada desde: La propia p�gina, cada vez que se carga
    ''' Tiempo m�ximo:0,1</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim str As String

        str = "<frame id=fraWSMain src='" & ConfigurationManager.AppSettings("rutaQA2008") & "Certificados/Certificados.aspx" & "'><frame name=fraWSServer id=fraWSServer src='../blank.htm' >"
        Me.fraWS.InnerHtml = str
    End Sub

    End Class

