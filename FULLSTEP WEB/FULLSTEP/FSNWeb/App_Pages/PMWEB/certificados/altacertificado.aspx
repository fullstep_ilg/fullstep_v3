﻿<%@ Register Assembly="Infragistics.WebUI.WebDataInput.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.WebDataInput" TagPrefix="igtxt" %>
<%@ Register Assembly="Infragistics.WebUI.UltraWebTab.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="altacertificado.aspx.vb" Inherits="Fullstep.FSNWeb.altacertificado" enableViewState="False"%>
<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head runat="server">
		<title>FULLSTEP Networks</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	
	<script type="text/javascript">		
		wProgreso = null;

		//Muestra el contenido peligroso que ha efectuado un error al guardar.
		function ErrorValidacion(contenido) {
			alert(arrTextosML[2].replace("$$$", contenido));
			OcultarEspera();
		}
		
		/*
		''' <summary>
		''' Redimensiona la anchura del desglose
		''' </summary>      
		''' <remarks>Llamada desde=init(); Tiempo máximo=0seg.</remarks>
		*/
		function resize() {
			for (i = 0; i < arrDesgloses.length; i++) {
				sDiv = arrDesgloses[i].replace("tblDesglose", "divDesglose")
				if (document.getElementById(sDiv))
					document.getElementById(sDiv).style.width = parseFloat(document.body.offsetWidth) - 70 + 'px';
			}
		}

		function localEval(s) {
		    eval(s);
		}
        
		function init() {
		    resize();
		    if (arrObligatorios.length > 0)
		        if (document.getElementById("lblCamposObligatorios"))
		            document.getElementById("lblCamposObligatorios").style.display = "";
		}

		//''' <summary>
		//''' Comprobar q todo es correcto (mirar <returns>) antes de lanzar la pantalla certifcalendario.aspx
		//''' q se encarga de grabar en bbdd el alta del certificado seleccionado.
		//''' </summary>     
		//''' <returns>Falso si no se ha rellenado los obligatorios � indicado proveedor � hay error en la
		//'''		pagina y salta en el momento de pulsar el botón cmdSolicitar (ejemplo: metes articulo
		//'''		no valido y sin salir del entry articulo pulsas cmdSolicitar)</returns>
		//''' <remarks>Llamada desde: cmdSolicitar ; Tiempo máximo:0</remarks>						
		function Enviar()
		{		        
			bMensajePorMostrar = document.getElementById("bMensajePorMostrar")
			if (bMensajePorMostrar)
				if (bMensajePorMostrar.value == "1") {
					bMensajePorMostrar.value = "0";
					return false; 		
				}
			var respOblig = comprobarObligatorios()
			switch (respOblig) {
				case "":  //no falta ningun campo obligatorio
					break;
				case "filas0": //no se han introducido filas en un desglose obligatorio
					alert(arrTextosML[1])
					return false
					break;
				default: //falta algun campo obligatorio
					alert(arrTextosML[1] + '\n' + respOblig)
					return false
					break;
			}
			if (comprobarTextoLargo() == false)
				return false
        if (ComprobarExistenProv() == false) {
            alert(arrTextosML[0])
            return false
        }
        else {
            var newWindow = window.open("certifcalendario.aspx?plazo=" + window.document.frmAlta.PlazoCumplimentacion.value, "_blank", "width=400px,height=360px,status=no,resizable=no,top=200,left=200,menubar=no");
            
        }
		return false
		}

		/*
		''' <summary>
		''' Tras elegir las fechas de despublicación y limite cumplimentación en certifcalendario.aspx, esta función realiza la grabación en bbdd
		''' </summary>
		''' <param name="DiaDespub">Dia de despublicación</param>
		''' <param name="MesDespub">Mes de despublicación</param>        
		''' <param name="AnyoDespub">Año de despublicación</param>
		''' <param name="Accion">q estas realizando solicitar/renovar</param>  
		''' <param name="DiaLimite">Dia de limite cumplimentación</param>
		''' <param name="MesLimite">Mes de limite cumplimentación</param>  
		''' <param name="AnyoLimite">Año de limite cumplimentación</param>                     
		''' <remarks>Llamada desde: PMWeb\script\certificados\certifcalendario.aspx; Tiempo máximo:0,3</remarks>*/
		function GenerarCertificado(DiaDespub, MesDespub, AnyoDespub, Accion, DiaLimite, MesLimite, AnyoLimite) {

			if (frmAlta.cmdSolicitar)
				frmAlta.cmdSolicitar.disabled = true;

			if (wProgreso == null) {
				wProgreso = true;
				MostrarEspera();
				//wProgreso=window.open("../_common/espera.aspx?cadena=" + frmAlta.cadenaespera.value ,"_blank","top=300,left=450,width=400,height=100,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")	 				   
			}

			MontarFormularioSubmit(false, false, false, true)
			document.forms["frmSubmit"].elements["GEN_Enviar"].value = 1
			document.forms["frmSubmit"].elements["GEN_Accion"].value = "enviarcertificados"
			document.forms["frmSubmit"].elements["GEN_DespubDia"].value = DiaDespub
			document.forms["frmSubmit"].elements["GEN_DespubMes"].value = MesDespub
			document.forms["frmSubmit"].elements["GEN_DespubAnyo"].value = AnyoDespub
			document.forms["frmSubmit"].elements["GEN_LimiteCumplimentacionDia"].value = DiaLimite
			document.forms["frmSubmit"].elements["GEN_LimiteCumplimentacionMes"].value = MesLimite
			document.forms["frmSubmit"].elements["GEN_LimiteCumplimentacionAnyo"].value = AnyoLimite
			document.forms["frmSubmit"].elements["GEN_Volver"].value = document.forms["frmAlta"].elements["Volver"].value //parametros del visor
			
		oFrm = MontarFormularioCalculados()
		sInner = oFrm.innerHTML
		oFrm.innerHTML = ""
		document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner)
		document.forms["frmSubmit"].submit()

		return
		}
		
		function CalcularCamposCalculados()
		{
		oFrm = MontarFormularioCalculados()
		oFrm.submit()
		}

		function HistoryHaciaAtras (num)
		{ 
			//Va hacia atras tantas paginas como indique el parametro num
			window.history.go(num)
		}
		/*
		Descripcion: Muestra el div de espera y oculta los tabs de grupos
		Llamada desde= GenerarCertificado
		Tiempo ejecucion:=0,1seg. 
		*/
		function MostrarEspera()
		{
		    wProgreso = true;

		    $("[id*='lnkBoton']").attr('disabled', 'disabled');

		document.getElementById("lblCamposObligatorios").style.display = "none"
		document.getElementById("divForm2").style.display = 'none';		  
		
		i=0;
		bSalir = false;
		while (bSalir == false)
			{
			if (document.getElementById("uwtGrupos_div" + i))
				{
				document.getElementById("uwtGrupos_div" + i).style.visibility='hidden';
				i = i+1;
				}
			else
				{
				bSalir = true;
				}
			}
		document.getElementById("lblProgreso").value = frmAlta.cadenaespera.value;
		document.getElementById("divProgreso").style.display='inline';				
		}

		//Estaba habilitando los botones antes de ocultar el progreso. Parecia q te dejaba dar a varios botones mientras estaba en progreso.
		function DarTiempoAOcultarProgreso() {
		    $("[id*='lnkBoton']").removeAttr('disabled');
		}

		/*
		Descripcion: Oculta el div de espera y muestra los tabs de grupos
		Llamada desde= onunload() // ErrorValidacion() 
		Tiempo ejecucion:=0,1seg. 
		*/
		function OcultarEspera()
		{
		wProgreso = null;
		
		document.getElementById("divProgreso").style.display='none';
		document.getElementById("divForm2").style.display = 'inline';

		setTimeout('DarTiempoAOcultarProgreso()', 250);
		
		i=0;
		bSalir = false;
		while (bSalir == false)
			{
			if (document.getElementById("uwtGrupos_div" + i))
				{
				document.getElementById("uwtGrupos_div" + i).style.visibility='visible';
				i = i+1;
				}
			else
				{
				bSalir = true;
				}
			}		
			
		}
		function initTab(webTab) {
			var cp = document.getElementById(webTab.ID + '_cp');
			cp.style.minHeight = '300px';
		}
	</script>	
	<body onresize="resize()" onunload="if (wProgreso != null) OcultarEspera();wProgreso=null;" onload="init()">
		<IFRAME id="iframeWSServer" style="Z-INDEX: 103; LEFT: 8px; VISIBILITY: hidden; POSITION: absolute; TOP: 200px"
			name="iframeWSServer" src="../blank.htm"></IFRAME>
		<form id="frmAlta" method="post" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <CompositeScript>
                <Scripts>
                    <asp:ScriptReference Name="ExtenderBase.BaseScripts.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Common.Common.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Compat.Timer.Timer.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Animation.Animations.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Animation.AnimationBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="PopupExtender.PopupBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="AutoComplete.AutoCompleteBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Path="../alta/js/AdjacentHTML.js" />
                    <asp:ScriptReference Path="../alta/js/jsAlta.js" />
                </Scripts>
            </CompositeScript>
        </asp:ScriptManager>		
	        
			<INPUT id="txtEnviar" style="Z-INDEX: 104; LEFT: 336px; POSITION: absolute; TOP: 116px"
				type="hidden" name="Enviar" runat="server"> <INPUT id="txtDespubAnyo" style="Z-INDEX: 104; LEFT: 336px; POSITION: absolute; TOP: 116px"
				type="hidden" name="txtDespubAnyo" runat="server"> <INPUT id="txtDespubMes" style="Z-INDEX: 104; LEFT: 336px; POSITION: absolute; TOP: 116px"
				type="hidden" name="txtDespubMes" runat="server"> <INPUT id="txtDespubDia" style="Z-INDEX: 104; LEFT: 336px; POSITION: absolute; TOP: 116px"
				type="hidden" name="txtDespubDia" runat="server"> <input type="hidden" id="cadenaespera" name="cadenaespera" RUNAT="SERVER">
			<INPUT id="Solicitud" type="hidden" name="Solicitud" runat="server">
			<INPUT id="Volver" type="hidden" name="Volver" runat="server" >
			<input id="PlazoCumplimentacion" type="hidden" name="PlazoCumplimentacion" runat="server" />
			<uc1:menu id="Menu1" runat="server"  OpcionMenu="Calidad" OpcionSubMenu="Certificados"></uc1:menu>
			<table id="Table1" height="10%" cellSpacing="1" cellPadding="1" width="100%" border="0">
				<tr>
					<fsn:FSNPageHeader runat="server" ID="FSNPageHeader" UrlImagenCabecera="./images/certificados.gif">  
					</fsn:FSNPageHeader>
				</tr>
				<tr>
					<td ><asp:label id="lblCamposObligatorios" style="Z-INDEX: 102; display:none;" runat="server" CssClass="captionRed">Los campos marcados con (*) son de obligada cumplimentacion</asp:label></td>
				</tr>
			</table>
			<div id="divProgreso" style="DISPLAY: none">
				<table id="tblProgreso" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
					<tr style="HEIGHT: 50px">
						<td>&nbsp;</td>
					</tr>
					<TR>
						<td align="center" width="100%"><asp:textbox id="lblProgreso" style="TEXT-ALIGN: center" runat="server" Width="100%" CssClass="captionBlue"
								BorderWidth="0" BorderStyle="None">Su solicitud está siendo tramitada. Espere unos instantes...</asp:textbox></td>
					</TR>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<TR>
						<td align="center" width="100%"><asp:image id="imgProgreso" runat="server" src="../_common/images/cursor-espera_grande.gif"></asp:image></td>
					</TR>
				</table>
			</div>
			<div id="divForm2" style="DISPLAY: inline">
				<table height="90%" cellSpacing="3" cellPadding="3" width="100%" border="0">
					<TR>
						<TD>
							<igtab:ultrawebtab id="uwtGrupos" runat="server" EnableViewState="False" DisplayMode="Scrollable" 
								BorderStyle="Solid" BorderWidth="1px" CustomRules="padding:10px;" FixedLayout="True" DummyTargetUrl=" "
								ThreeDEffect="False" Width="100%">
								<DefaultTabStyle Height="24px" CssClass="uwtDefaultTab">
									<Padding Left="20px" Right="20px"></Padding>
								</DefaultTabStyle>
								<RoundedImage NormalImage="ig_tab_blueb2.gif" HoverImage="ig_tab_blueb1.gif" FillStyle="LeftMergedWithCenter"></RoundedImage>
								<ClientSideEvents InitializeTabs="initTab" />
							</igtab:ultrawebtab>
						</TD>
					</TR>
				</table>
			</div>
			<div id="divDropDowns" style="Z-INDEX: 101; VISIBILITY: hidden; POSITION: absolute; TOP: 300px"></div>
			<div id="divAlta" style="VISIBILITY: hidden"></div>
			<div id="divCalculados" style="Z-INDEX: 1001; VISIBILITY: hidden; POSITION: absolute; TOP: 0px"></div>			
		</form>
		<form id="frmCalculados" name="frmCalculados" action="recalcularimportes.aspx" method="post"
			target="fraWSServer">
		</form>
		<form name="frmDesglose" id="frmDesglose" method="post" target="winDesglose">
		</form>
	</body>
</html>
