Public Class certifproveedores
    Inherits System.Web.UI.UserControl
#Region " Web Form Designer Generated Code "
    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblDesglose As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tblDesgloseHidden As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tblGeneral As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents divDesglose As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents numRows As System.Web.UI.HtmlControls.HtmlInputHidden
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
#End Region
    Private moTextos As DataTable
    Private msTabContainer As String
    Private msIdi As String
    Private mdsProveedores As DataSet
    Private mbSoloProveedores As Boolean
    Private Const AltaScriptKey As String = "AltaIncludeScript"
    Private Const AltaFileName As String = "js/jsAlta.js?v="
    Private Const IncludeScriptFormat As String = ControlChars.CrLf & "<script type=""{0}"" src=""{1}""></script>"
    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
    Property Idi() As String
        Get
            Idi = msIdi
        End Get
        Set(ByVal Value As String)
            msIdi = Value
        End Set
    End Property
    Property TabContainer() As String
        Get
            TabContainer = msTabContainer
        End Get
        Set(ByVal Value As String)
            msTabContainer = Value
        End Set
    End Property
    Property Textos() As DataTable
        Get
            Textos = moTextos
        End Get
        Set(ByVal Value As DataTable)
            moTextos = Value
        End Set
    End Property
    Property dsProveedores() As DataSet
        Get
            dsProveedores = mdsProveedores
        End Get
        Set(ByVal Value As DataSet)
            mdsProveedores = Value
        End Set
    End Property
    Property SoloProveedores() As Boolean
        Get
            SoloProveedores = mbSoloProveedores
        End Get
        Set(ByVal Value As Boolean)
            mbSoloProveedores = Value
        End Set
    End Property
    Public Sub New(Optional ByVal lId As Long = Nothing, Optional ByRef oDS As DataSet = Nothing, Optional ByVal sIdi As String = Nothing)
        If lId <> Nothing Then
            msIdi = sIdi
        End If
    End Sub
    ''' Revisado por: blp. Fecha: 05/10/2011
    ''' <summary>
    ''' Carga los input/entrys para un tab "Seleccione los proveedores" de Certificado
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">evento de sistema</param>  
    ''' <remarks>Llamada desde: altacertificado.aspx/Page_Load       NWDetalleSolicitud.aspx/Page_Load
    '''     aprobacion.aspx/Page_Load       gestiontrasladada.aspx/Page_Load ; Tiempo m�ximo: 0,1</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Page.IsPostBack Then
            Exit Sub
        End If
        Dim otblRow As System.Web.UI.HtmlControls.HtmlTableRow
        Dim otblCell As System.Web.UI.HtmlControls.HtmlTableCell
        Dim otblCellData As System.Web.UI.HtmlControls.HtmlTableCell
        Dim otblRowData As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oRowHeader As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oCellHeader As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oLbl As System.Web.UI.HtmlControls.HtmlGenericControl
        Dim oDSRow As System.Data.DataRow
        Dim olabel As System.Web.UI.WebControls.Label
        Dim oHyper As System.Web.UI.WebControls.HyperLink
        Dim oFSEntry As DataEntry.GeneralEntry
        Dim oFSDSEntry As DataEntry.GeneralEntry
        Dim sClase As String
        Dim i As Integer
        Dim iFila As Integer
        Dim iCampo As Integer
        Dim sKeyProve As String = String.Empty
        Dim iInicio As Integer
        Dim iFin As Integer

        If Not Page.ClientScript.IsClientScriptBlockRegistered("claveArrayEntrys1") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "claveArrayEntrys1", "<script>var arrEntrys_1" + " = new Array()</script>")
        End If

        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oUser As FSNServer.User

        oUser = Session("FSN_User")
        msIdi = oUser.Idioma.ToString()

        Dim oDict As FSNServer.Dictionary = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.CertifProveedores, msIdi)
        Dim oTextos As DataTable = oDict.Data.Tables(0)

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ClienteID", "<script>var vClientIDCertif ='" + Me.ClientID + "'</script>")

        If Not mbSoloProveedores Then
            'a�ade la label y el hyperlink de b�squeda de proveedores:
            otblRow = New System.Web.UI.HtmlControls.HtmlTableRow
            otblCell = New System.Web.UI.HtmlControls.HtmlTableCell
            otblCell.ColSpan = 3
            olabel = New System.Web.UI.WebControls.Label
            olabel.Text = oTextos.Rows(0).Item(1)
            olabel.CssClass = "captionBlue"
            otblCell.Controls.Add(olabel)
            otblRow.Cells.Add(otblCell)
            Me.tblDesglose.Rows.Add(otblRow)

            otblRow = New System.Web.UI.HtmlControls.HtmlTableRow
            otblCell = New System.Web.UI.HtmlControls.HtmlTableCell
            otblCell.ColSpan = 3
            otblCell.Align = "Right"
            oHyper = New System.Web.UI.WebControls.HyperLink
            oHyper.Text = oTextos.Rows(1).Item(1)
            oHyper.Attributes("onclick") = "BuscarProveedor()"
            oHyper.CssClass = "UnderlineGray"
            otblCell.Controls.Add(oHyper)
            otblRow.Cells.Add(otblCell)
            Me.tblDesglose.Rows.Add(otblRow)

            'A�ade las cabeceras de los proveedores:
            oRowHeader = New System.Web.UI.HtmlControls.HtmlTableRow
            otblRow = New System.Web.UI.HtmlControls.HtmlTableRow
            Me.tblDesglose.Rows.Add(oRowHeader)

            oCellHeader = New System.Web.UI.HtmlControls.HtmlTableCell
            oCellHeader.ID = "SOL"
            oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
            oLbl.InnerHtml = oTextos.Rows(2).Item(1)
            oCellHeader.Controls.Add(oLbl)
            oCellHeader.Attributes("class") = "captionBlueUnderline"
            oRowHeader.Cells.Add(oCellHeader)

            oCellHeader = New System.Web.UI.HtmlControls.HtmlTableCell
            oCellHeader.ID = "PROVE"
            oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
            oLbl.InnerHtml = oTextos.Rows(3).Item(1)
            oCellHeader.Controls.Add(oLbl)
            oCellHeader.Attributes("class") = "captionBlueUnderline"
            oRowHeader.Cells.Add(oCellHeader)

            oCellHeader = New System.Web.UI.HtmlControls.HtmlTableCell
            oCellHeader.ID = "CON"
            oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
            oLbl.InnerHtml = oTextos.Rows(4).Item(1)
            oCellHeader.Controls.Add(oLbl)
            oCellHeader.Attributes("class") = "captionBlueUnderline"
            oRowHeader.Cells.Add(oCellHeader)
        End If

        numRows.Value = mdsProveedores.Tables(0).Rows.Count

        'ahora va generando los proveedores:
        sClase = "ugfilatabla"
        If mbSoloProveedores = True Then
            otblRow = New System.Web.UI.HtmlControls.HtmlTableRow
            iInicio = 2
            iFin = 2
        Else
            iInicio = 1
            iFin = 3
        End If

        For iCampo = iInicio To iFin
            '***** ESTRUCTURA INTERNA PARA LA COPIA **********************
            otblCell = New System.Web.UI.HtmlControls.HtmlTableCell
            oFSEntry = New DataEntry.GeneralEntry
            Dim pag As FSNPage = Me.Page
            oFSEntry.Title = Me.Page.Title
            oFSEntry.ActivoSM = CType(Me.Page, FSNPage).Acceso.gbAccesoFSSM

            Select Case iCampo
                Case 1
                    oFSEntry.ID = "fsentry" + "SOLICIT"
                    oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.SinTipo
                    oFSEntry.Tipo = TiposDeDatos.TipoGeneral.TipoCheckBox
                    oFSEntry.Intro = 0
                    oFSEntry.Tag = "SOLICIT"
                    oFSEntry.ReadOnly = False
                    oFSEntry.Obligatorio = 0
                    oFSEntry.Width = Unit.Percentage(5)
                    oFSEntry.Valor = True
                Case 2
                    oFSEntry.ID = "fsentry" + "PROVE"
                    oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.Proveedor
                    oFSEntry.Tipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
                    oFSEntry.MaxLength = 800
                    oFSEntry.Intro = 0
                    oFSEntry.Tag = "PROVE"
                    oFSEntry.ReadOnly = True
                    oFSEntry.Obligatorio = 0
                    oFSEntry.InputStyle.CssClass = "trasparent"
                    oFSEntry.Width = Unit.Percentage(70)
                    sKeyProve = oFSEntry.ClientID
                Case 3
                    oFSEntry.ID = "fsentry" + "CON"
                    oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.Contacto
                    oFSEntry.Tipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
                    oFSEntry.Intro = 1
                    oFSEntry.Tag = "CON"
                    oFSEntry.ReadOnly = False
                    oFSEntry.Obligatorio = 0
                    oFSEntry.Width = Unit.Pixel(250)
                    oFSEntry.DependentField = sKeyProve
            End Select
            oFSEntry.IsGridEditor = False
            oFSEntry.IdContenedor = Me.ClientID
            oFSEntry.TabContainer = msTabContainer
            oFSEntry.BaseDesglose = True
            oFSEntry.Desglose = True
            oFSEntry.NumberFormat = oUser.NumberFormat
            oFSEntry.DateFormat = oUser.DateFormat
            oFSEntry.Idi = msIdi

            otblCell.Controls.Add(oFSEntry)
            otblRow.Cells.Add(otblCell)
            oFSEntry.UsarOrgCompras = CType(Me.Page, FSNPage).Acceso.gbUsar_OrgCompras
            Page.ClientScript.RegisterStartupScript(Me.GetType(), oFSEntry.ClientID, "<script>arrEntrys_1" + "[arrEntrys_1" + ".length] = '" + oFSEntry.InitScript + "';</script>")

            iFila = 1

            For Each oDSRow In mdsProveedores.Tables(0).Rows
                Try
                    otblRowData = Me.tblDesglose.Rows(iFila + 2)
                Catch ex As Exception
                    otblRowData = New System.Web.UI.HtmlControls.HtmlTableRow
                    Me.tblDesglose.Rows.Add(otblRowData)
                End Try

                oFSDSEntry = New DataEntry.GeneralEntry
                Select Case iCampo
                    Case 1
                        oFSDSEntry.ID = "fsdsentry_" + iFila.ToString + "_SOLICIT"
                        oFSDSEntry.TipoGS = TiposDeDatos.TipoCampoGS.SinTipo
                        oFSDSEntry.Tipo = TiposDeDatos.TipoGeneral.TipoCheckBox
                    Case 2
                        oFSDSEntry.ID = "fsdsentry_" + iFila.ToString + "_PROVE"
                        oFSDSEntry.TipoGS = TiposDeDatos.TipoCampoGS.Proveedor
                        oFSDSEntry.Tipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
                        oFSDSEntry.InputStyle.CssClass = "trasparent"
                    Case 3
                        oFSDSEntry.ID = "fsdsentry_" + iFila.ToString + "_CON"
                        oFSDSEntry.TipoGS = TiposDeDatos.TipoCampoGS.Contacto
                        oFSDSEntry.Tipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
                End Select

                oFSDSEntry.DropDownGridID = oFSEntry.ID
                oFSDSEntry.Width = oFSEntry.Width
                oFSDSEntry.Intro = oFSEntry.Intro
                oFSDSEntry.Tag = oFSEntry.Tag
                oFSDSEntry.Title = oFSEntry.Title
                oFSDSEntry.IsGridEditor = False
                oFSDSEntry.IdContenedor = oFSEntry.IdContenedor
                oFSDSEntry.TabContainer = oFSEntry.TabContainer
                oFSDSEntry.ReadOnly = oFSEntry.ReadOnly
                oFSDSEntry.Obligatorio = oFSEntry.Obligatorio
                oFSDSEntry.BaseDesglose = False
                oFSDSEntry.Maximo = oFSEntry.Maximo
                oFSDSEntry.Minimo = oFSEntry.Minimo
                oFSDSEntry.Lista = oFSEntry.Lista
                oFSDSEntry.InputStyle.CopyFrom(oFSEntry.InputStyle)
                oFSDSEntry.MaxLength = oFSEntry.MaxLength
                oFSDSEntry.PopUpStyle = oFSEntry.PopUpStyle
                oFSDSEntry.PUCaptionBoton = oFSEntry.PUCaptionBoton
                oFSDSEntry.ButtonStyle.CopyFrom(oFSEntry.ButtonStyle)
                oFSDSEntry.Restric = oFSEntry.Restric
                oFSDSEntry.Calculado = oFSEntry.Calculado
                oFSDSEntry.NumberFormat = oUser.NumberFormat
                oFSDSEntry.DateFormat = oUser.DateFormat
                oFSDSEntry.Valor = oFSEntry.Valor
                oFSDSEntry.Idi = oFSEntry.Idi
                oFSDSEntry.Text = oFSEntry.Text
                oFSDSEntry.DependentField = Replace(oFSEntry.DependentField, "fsentry", "fsdsentry_" + iFila.ToString + "_")

                otblCellData = New System.Web.UI.HtmlControls.HtmlTableCell
                otblCellData.Controls.Add(oFSDSEntry)
                otblCellData.Attributes("class") = sClase
                otblRowData.Cells.Add(otblCellData)

                iFila = iFila + 1
            Next  'fila
            Me.tblDesgloseHidden.Rows.Add(otblRow)
        Next  'iCampo


        ''''''''''''''''''''''''' Mete los valores ''''''''''''''''''''''''''''''''''''''
        iFila = 1

        For Each oDSRow In mdsProveedores.Tables(0).Rows
            For i = iInicio To iFin
                Select Case i
                    Case 1
                        oFSDSEntry = Me.tblDesglose.FindControl("fsdsentry_" + iFila.ToString + "_SOLICIT")
                    Case 2
                        oFSDSEntry = Me.tblDesglose.FindControl("fsdsentry_" + iFila.ToString + "_PROVE")
                    Case 3
                        oFSDSEntry = Me.tblDesglose.FindControl("fsdsentry_" + iFila.ToString + "_CON")
                End Select

                If Not oFSDSEntry Is Nothing Then
                    Select Case i
                        Case 1
                            oFSDSEntry.Valor = True
                        Case 2
                            oFSDSEntry.Valor = oDSRow.Item("COD")
                            oFSDSEntry.Text = oDSRow.Item("COD") & " - " & oDSRow.Item("DEN")
                        Case 3
                            Dim oContactos As FSNServer.Contactos
                            oContactos = FSWSServer.Get_Object(GetType(FSNServer.Contactos))

                            oContactos.Proveedor = oDSRow.Item("COD")
                            oContactos.LoadData()

                            oFSDSEntry.Lista = oContactos.Data
                            oFSDSEntry.Valor = oDSRow.Item("ID_CONTACTO")
                            oFSDSEntry.Text = DBNullToSomething(oDSRow.Item("NOM_CONTACTO"))

                            oFSEntry = Me.tblDesglose.FindControl("fsentryCON")
                            If oFSEntry.Lista Is Nothing Then oFSEntry.Lista = oFSDSEntry.Lista
                    End Select
                End If
            Next i
            iFila = iFila + 1
        Next

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim includeScript As String
        If Not Page.ClientScript.IsClientScriptBlockRegistered(AltaScriptKey) Then
            includeScript = String.Format(IncludeScriptFormat, "text/javascript", AltaFileName & ConfigurationManager.AppSettings("versionJs"))
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), AltaScriptKey, includeScript)
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered("claveArrayDesgloses") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "claveArrayDesgloses", "<script>var arrDesgloses = new Array()</script>")
        End If

        Page.ClientScript.RegisterStartupScript(Me.GetType(), Me.tblDesglose.ClientID, "<script>arrDesgloses[arrDesgloses.length] = '" + Me.tblDesglose.ClientID + "';</script>")

    End Sub
End Class