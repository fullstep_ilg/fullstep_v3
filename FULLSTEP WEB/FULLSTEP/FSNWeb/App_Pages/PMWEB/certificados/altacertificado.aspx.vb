
Public Class altacertificado
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents lblCamposObligatorios As System.Web.UI.WebControls.Label
    Protected WithEvents cmdSolicitar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents uwtGrupos As Infragistics.WebUI.UltraWebTab.UltraWebTab
    Protected WithEvents txtEnviar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Solicitud As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtFecDespub As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtDespubAnyo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtDespubMes As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtDespubDia As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblVolver As System.Web.UI.WebControls.Label
    Protected WithEvents cadenaespera As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Volver As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblProgreso As System.Web.UI.WebControls.TextBox
    Protected WithEvents imgProgreso As System.Web.UI.WebControls.Image
    Protected WithEvents tblProgreso As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents PlazoCumplimentacion As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents FSNPageHeader As Fullstep.FSNWebControls.FSNPageHeader

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private _oSolicitud As FSNServer.Solicitud
    ''' <summary>
    ''' Propiedad que carga la informacion de la solicitud
    ''' </summary>
    Protected ReadOnly Property oSolicitud() As FSNServer.Solicitud
        Get
            If _oSolicitud Is Nothing Then
                If Me.IsPostBack Then
                    _oSolicitud = CType(Cache("oSolicitud_" & FSNUser.Cod), FSNServer.Solicitud)
                Else
                    _oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
                    _oSolicitud.ID = Request("TipoSolicit")
                    _oSolicitud.Load(FSNUser.Idioma.ToString)
                    _oSolicitud.Formulario.Load(FSNUser.Idioma.ToString, _oSolicitud.ID, , , , , FSNUser.DateFmt)
                    Me.InsertarEnCache("oSolicitud_" & FSNUser.Cod, _oSolicitud)
                End If
            End If
            Return _oSolicitud
        End Get
    End Property

    Private _oProvedoresSeleccionados As DataSet
    ''' <summary>
    ''' Propiedad que carga la informacion de los  Provedores Seleccionados
    ''' </summary>
    Protected ReadOnly Property oProvedoresSeleccionados() As DataSet
        Get
            If _oProvedoresSeleccionados Is Nothing Then
                If Me.IsPostBack Then
                    _oProvedoresSeleccionados = CType(Cache("oProvedoresSeleccionados" & FSNUser.Cod), DataSet)
                Else
                    Dim Proveedores As FSNServer.Proveedores
                    Proveedores = FSNServer.Get_Object(GetType(FSNServer.Proveedores))

                    Dim sProveedores As String
                    sProveedores = Request("Proveedores")
                    Proveedores.DevolverProvedoresSeleccionados(sProveedores)

                    _oProvedoresSeleccionados = Proveedores.Data.Tables(0).DataSet
                    Me.InsertarEnCache("oProvedoresSeleccionados" & FSNUser.Cod, _oProvedoresSeleccionados)
                End If
            End If
            Return _oProvedoresSeleccionados
        End Get
    End Property

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
#End Region

    Public oFile As System.IO.StreamWriter

    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"

    ''' <summary>
    ''' Carga los datos para mostrarlos en la pagina de alta de un certificado
    ''' </summary>
    ''' <param name="sender">las del evento</param>
    ''' <param name="e">las del evento</param>        
    ''' <remarks>Llamada desde; Tiempo m�ximo=0,3seg.</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Page.IsPostBack Then
            CargarCamposGrupo()
            Exit Sub
        End If

        Dim sScript As String
        Dim sIdi As String = FSNUser.Idioma.ToString

        'Tema de la cadena espera
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Espera
        Me.cadenaespera.Value = Textos(1)

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.AltaCertificados

        Dim sClientTextVars As String
        sClientTextVars = ""
        sClientTextVars += "vdecimalfmt='" + FSNUser.DecimalFmt + "';"
        sClientTextVars += "vthousanfmt='" + FSNUser.ThousanFmt + "';"
        sClientTextVars += "vprecisionfmt='" + FSNUser.PrecisionFmt + "';"
        sClientTextVars = String.Format(IncludeScriptKeyFormat, "javascript", sClientTextVars)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsUser", sClientTextVars)

        Me.Volver.Value = Request.QueryString("Volver")

        'MONTAR LA CABECERA
        '==================
        FSNPageHeader.VisibleBotonVolver = True
        FSNPageHeader.VisibleBotonEmitir = True

        FSNPageHeader.TextoBotonVolver = Textos(5)
        FSNPageHeader.TextoBotonEmitir = Textos(1)

        FSNPageHeader.OnClientClickVolver = "window.open(""" & Replace(Request.QueryString("volver"), "**", "&") & """,""_top"");return false;"
        FSNPageHeader.OnClientClickEmitir = "return Enviar();"

        Solicitud.Value = oSolicitud.ID

        PlazoCumplimentacion.Value = oSolicitud.PlazoCumplimentacion

        FSNPageHeader.TituloCabecera = Textos(0) & " " & oSolicitud.Den(sIdi)
        lblCamposObligatorios.Text = Textos(8)

        Dim sClientTexts As String
        sClientTexts = ""
        sClientTexts += "var arrTextosML = new Array();"
        sClientTexts += "arrTextosML[0] = '" + JSText(Textos(2)) + "';"
        sClientTexts += "arrTextosML[1] = '" + JSText(Textos(4)) + "';"
        sClientTexts += "arrTextosML[2] = '" + JSText(Textos(6)) + "';"
        sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)

        CargarCamposGrupo()

        Dim ilGMN1 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN1
        Dim ilGMN2 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN2
        Dim ilGMN3 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN3
        Dim ilGMN4 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN4
        sScript = ""
        sScript += "var ilGMN1 = " + ilGMN1.ToString() + ";"
        sScript += "var ilGMN2 = " + ilGMN2.ToString() + ";"
        sScript += "var ilGMN3 = " + ilGMN3.ToString() + ";"
        sScript += "var ilGMN4 = " + ilGMN4.ToString() + ";"
        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "LongitudesCodigosKey", sScript)

        Dim includeScript As String

        If Not Page.ClientScript.IsClientScriptBlockRegistered("varFila") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "varFila", "<script>var sFila = '" & JSText(Textos(7)) & "' </script>")
        If Not Page.ClientScript.IsClientScriptIncludeRegistered("claveArrayDesgloses") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "claveArrayDesgloses", "<script>var arrDesgloses = new Array()</script>")
        End If
        'En jsAlta.js hay para las funciones de a�adir linea y copia linea
        '       $create(AjaxControlToolkit.AutoCompleteBehavior ...rutaPM...
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")
        'para el buscador de art�culos
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' </script>")

        FindControl("frmAlta").Controls.Add(FSNWeb.CommonAlta.InsertarCalendario())
    End Sub

    ''' Revisado por: blp. Fecha: 05/10/2011
    ''' <summary>
    ''' Genera el tab con los datos del formulario asociado a la solicitud
    ''' </summary>
    ''' <remarks>Llamada desde: page_load ;Tiempo maximo:0.3</remarks>
    Private Sub CargarCamposGrupo()
        Dim oGrupo As FSNServer.Grupo
        Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab
        Dim oucCampos As campos
        Dim oucDesglose As desgloseControl
        Dim oucProveedores As certifproveedores
        Dim oRow As DataRow
        Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden

        uwtGrupos.Tabs.Clear()
        AplicarEstilosTab(uwtGrupos)
        Dim lIndex As Long = 0

        For Each oGrupo In oSolicitud.Formulario.Grupos.Grupos
            oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
            Dim Font As String = ObtenerValorPropiedad(".uwtDefaultTab", "font-family")
            Dim Size As String = Replace(ObtenerValorPropiedad(".uwtDefaultTab", "font-size"), "pt", "")
            oTabItem.Text = AjustarAnchoTextoPixels(oGrupo.Den(FSNUser.Idioma.ToString), AnchoDeTab, IIf(Font = "", "verdana", Font), IIf(Size = "", 8, CInt(Size)), False)
            oTabItem.Key = lIndex

            uwtGrupos.Tabs.Add(oTabItem)

            oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden

            oInputHidden.ID = "txtPre_" + lIndex.ToString
            lIndex += 1

            If oGrupo.NumCampos <= 1 Then
                For Each oRow In oGrupo.DSCampos.Tables(0).Rows
                    If oRow.Item("VISIBLE") = 1 Then
                        Exit For
                    End If

                Next
                If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Desglose Or (DBNullToSomething(oRow.Item("SUBTIPO")) = TiposDeDatos.TipoGeneral.TipoDesglose And DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.DesgloseActividad) Then
                    If oRow.Item("VISIBLE") = 0 Then
                        uwtGrupos.Tabs.Remove(oTabItem)
                    Else
                        oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Auto
                        oTabItem.ContentPane.UserControlUrl = "..\alta\desglose.ascx"
                        oucDesglose = oTabItem.ContentPane.UserControl
                        oucDesglose.ID = oGrupo.Id.ToString
                        oucDesglose.Campo = oRow.Item("ID")
                        oucDesglose.TieneIdCampo = False
                        oucDesglose.Ayuda = DBNullToSomething(oRow.Item("AYUDA_" & Idioma))
                        oucDesglose.TabContainer = uwtGrupos.ClientID
                        oucDesglose.SoloLectura = (oRow.Item("ESCRITURA") = 0)
                        oucDesglose.Solicitud = oSolicitud.ID
                        oucDesglose.Titulo = DBNullToSomething(oRow.Item("DEN_" & Idioma))
                        oucDesglose.TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Certificado
                        oInputHidden.Value = oucDesglose.ClientID
                    End If
                Else
                    oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Auto
                    oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
                    oucCampos = oTabItem.ContentPane.UserControl
                    oucCampos.ID = oGrupo.Id.ToString
                    oucCampos.dsCampos = oGrupo.DSCampos
                    oucCampos.IdGrupo = oGrupo.Id
                    oucCampos.Idi = FSNUser.Idioma.ToString
                    oucCampos.TabContainer = uwtGrupos.ClientID
                    oucCampos.Solicitud = oSolicitud.ID
                    oucCampos.TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Certificado
                    oucCampos.PM = False
                    oucCampos.Formulario = oSolicitud.Formulario
                    oInputHidden.Value = oucCampos.ClientID
                End If
            Else
                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Auto
                oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
                oucCampos = oTabItem.ContentPane.UserControl
                oucCampos.ID = oGrupo.Id.ToString
                oucCampos.dsCampos = oGrupo.DSCampos
                oucCampos.IdGrupo = oGrupo.Id
                oucCampos.Idi = FSNUser.Idioma.ToString
                oucCampos.TabContainer = uwtGrupos.ClientID
                oucCampos.Solicitud = oSolicitud.ID
                oucCampos.TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Certificado
                oucCampos.PM = False
                oucCampos.Formulario = oSolicitud.Formulario
                oInputHidden.Value = oucCampos.ClientID
            End If

            Me.FindControl("frmAlta").Controls.Add(oInputHidden)
        Next

        'A�ade el tab de los proveedores
        oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
        oTabItem.Text = Textos(3)
        oTabItem.Key = "PROV_CERTIF"
        uwtGrupos.Tabs.Add(oTabItem)

        oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Auto
        oTabItem.ContentPane.UserControlUrl = "certifproveedores.ascx"
        oucProveedores = oTabItem.ContentPane.UserControl
        oucProveedores.Idi = FSNUser.Idioma.ToString
        oucProveedores.TabContainer = uwtGrupos.ClientID

        oucProveedores.dsProveedores = oProvedoresSeleccionados

        oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden
        oInputHidden.ID = "txtPre_PROV_CERTIF"
        oInputHidden.Value = oucProveedores.ClientID
        Me.FindControl("frmAlta").Controls.Add(oInputHidden)

        'Selecciona por defecto el tab de los proveedores:
        uwtGrupos.SelectedTabObject = oTabItem
    End Sub
End Class

