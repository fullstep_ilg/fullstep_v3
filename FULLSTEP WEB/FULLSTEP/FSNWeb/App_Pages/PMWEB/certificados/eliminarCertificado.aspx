<%@ Page Language="vb" AutoEventWireup="false" Codebehind="eliminarCertificado.aspx.vb" Inherits="Fullstep.FSNWeb.eliminarCertificado"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<script>		
	    /*''' <summary>
	    ''' Nos lleva a la pagina del detalle de certificado
	    ''' </summary>
	    ''' <param name="IdCertif">certificado a cargar</param>
	    ''' <remarks>Llamada desde: Page_Load; Tiempo m�ximo: 0</remarks>*/	
		function Refrescar(IdCertif) {
		    var FechaLocal
		    FechaLocal = new Date()
		    otz = FechaLocal.getTimezoneOffset()
		    
		    window.open("detallecertificado.aspx?Certificado=" + IdCertif + "&Otz=" + otz.toString(), "_parent") 
		}
		
		/*''' <summary>
        ''' Ha eliminado el certificado entero, va al visor
        ''' </summary>
        ''' <remarks>Llamada desde: page_load; Tiempo m�ximo:0</remarks>*/
		function IrAVisorCertif()
		{
		window.open("<%=System.Configuration.ConfigurationManager.AppSettings("rutaQA2008")%>certificados/certificados.aspx","_parent")
		}		
	</script>	
	<body>
		<form id="Form1" method="post" runat="server">
		</form>
	</body>
</html>
