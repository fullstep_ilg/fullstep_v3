<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="publicadoOK.aspx.vb" Inherits="Fullstep.FSNWeb.publicadoOK"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
         <asp:ScriptManager ID="sm" runat="server">
         </asp:ScriptManager>
		<uc1:menu id="Menu1" runat="server" OpcionMenu="Calidad" OpcionSubMenu="Certificados"></uc1:menu>
			<table id="Table1" style="POSITION: absolute; TOP: 140px" width="100%" border="0">
				<tr>
					<td>
					    <fsn:FSNPageHeader runat="server" ID="FSNPageHeader" UrlImagenCabecera="./images/certificados.gif">  
			            </fsn:FSNPageHeader>
					</td>
				</tr>
			</table>
			<table style="Z-INDEX: 101; POSITION: absolute; TOP: 190px; width:100%;" class="fondoCabecera">
			    <tr>
			        <td style="padding-left:10px;" colspan="2">
			            <asp:label id="lblEnvioSatisfactorio" runat="server" CssClass="captionBlue">Label</asp:label>
			        </td>
			    </tr>
				<tr>
					<td style="padding-left:10px;">
					    <asp:label id="lblSolicitud" runat="server" CssClass="captionBlue">Label</asp:label>
					    <asp:label id="lblSolicitudValue" runat="server" CssClass="captionNegritaNegro">Label</asp:label>
					</td>
					<td>
					    <asp:label id="lblPeticionario" runat="server" CssClass="captionBlue">Label</asp:label>
					    <asp:label id="lblPeticionarioValue" runat="server" CssClass="captionNegritaNegro">Label</asp:label>  
					</td>
				</tr>
				<tr>
					<td style="padding-left:10px;">
					    <asp:label id="lblFecPub" runat="server" CssClass="captionBlue">Label</asp:label>
					    <asp:label id="lblFecPubValue" runat="server" CssClass="captionNegritaNegro">Label</asp:label>
					</td>
					<td>
					    <asp:label id="lblFecDespub" runat="server" CssClass="captionBlue">Label</asp:label>
					    <asp:label id="lblFecDespubValue" runat="server" CssClass="captionNegritaNegro">Label</asp:label>
					    
					</td>
				</tr>
				<tr>
				    <td height="25px"></td>
				</tr>
				<tr>
					<td style="padding-left:10px; width:844px; vertical-align:bottom;" colspan="2">
					    <asp:label id="lblProveedores" runat="server" CssClass="captionBlueUnderline" Width="100%"></asp:label>
					</td>
				</tr>
				<tr>
					<td style="padding-left:10px; vertical-align:top; height:60%;" colspan="2">
						<table id="tblProv" width="100%" border="0" runat="server">
							<tr>
								<td></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
