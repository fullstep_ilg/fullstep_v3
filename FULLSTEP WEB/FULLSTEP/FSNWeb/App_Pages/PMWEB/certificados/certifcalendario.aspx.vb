Public Class certifcalendario
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents lblSubTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents lblCalendario As System.Web.UI.WebControls.Label
    Protected WithEvents cmdContinuar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cmdCancelar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents txtAccion As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tblProv As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblFechaLimiteCumplimentacion As System.Web.UI.WebControls.Label
    Protected WithEvents lblFechaDespublicacion As System.Web.UI.WebControls.Label
    Protected WithEvents dpFechaLimCumplim As Infragistics.Web.UI.EditorControls.WebDatePicker
    Protected WithEvents dpFechaDesPub As Infragistics.Web.UI.EditorControls.WebDatePicker
    Protected WithEvents imgbtnHelpLimiteCumplimentacion As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblAyudaLimiteCumplimentacion As System.Web.UI.WebControls.Label
    Protected WithEvents lblAyudaDespublicacion As System.Web.UI.WebControls.Label
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
"<script language=""{0}"">{1}</script>"

    ''' <summary>
    ''' Carga de la pantalla. Muestra un calendario para q elijas fecha de despublicación si tienes permiso
    ''' para acceder a ella. Si no tienes permiso te muestra una lista de proveedores para los q se solicita
    ''' o envia o renueva.
    ''' </summary>
    ''' <param name="sender">parametro de sistema</param>
    ''' <param name="e">parametro de sistema</param>            
    ''' <returns>Nada, es un evento de sistema</returns>
    ''' <remarks>Llamada desde: sistema; Tiempo máximo: 0</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.txtAccion.Value = Request("Accion")

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.CertifCalendario

        Dim sClientTexts As String
        sClientTexts = ""
        sClientTexts += "var arrUnidadesNegocio = {};var arrTextosML = new Array();"
        sClientTexts += "arrTextosML[0] = '" + JSText(Textos(16)) + " ';"  'Seleccione una unidad
        sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
        Page.ClientScript.RegisterStartupScript(Me.GetType(),"TextosMICliente", sClientTexts)

        lblAyudaLimiteCumplimentacion.Text = Textos(17)
        lblAyudaDespublicacion.Text = Textos(18)

        'Textos:
        Me.lblTitulo.Text = Textos(0)
        Me.lblSubTitulo.Text = Textos(11)

        Me.cmdContinuar.Value = Textos(1)
        Me.cmdCancelar.Value = Textos(2)


        lblFechaLimiteCumplimentacion.Text = Textos(13)

        With dpFechaLimCumplim
            .EditModeFormat = FSNUser.DateFormat.ShortDatePattern
            .DisplayModeFormat = FSNUser.DateFormat.ShortDatePattern
        End With

        With dpFechaDesPub
            .EditModeFormat = FSNUser.DateFormat.ShortDatePattern
            .DisplayModeFormat = FSNUser.DateFormat.ShortDatePattern
        End With

        dpFechaLimCumplim.Value = DateAdd(DateInterval.Day, CType(Request("Plazo"), Integer), Now.Date)

        dpFechaDesPub.Value = ""

        lblFechaDespublicacion.Text = Textos(15)
        
    End Sub

End Class

