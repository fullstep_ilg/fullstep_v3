<%@ Page Language="vb" AutoEventWireup="false" Codebehind="certifcalendario.aspx.vb" Inherits="Fullstep.FSNWeb.certifcalendario"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<style type="text/css"> 
			/* The hint to Hide and Show */
			.hint {
				display: none; 
				position: absolute;                
				left:150px;
				width: 220px;
				line-height:15px;                
				border: 1px solid #c93;
				text-align:justify;
				z-index:1002;                
				padding: 5px 5px;
				background: #ffc url(../_common/images/pointer.gif) no-repeat -10px 5px;
			}
			.hint2 {
				display: none; 
				position: absolute;                
				left:150px;
				width: 220px;
				line-height:15px;
				border: 1px solid #c93;
				text-align:justify;
				z-index:1002;
				padding: 5px 5px;
				background: #ffc url(../_common/images/pointer.gif) no-repeat -10px 5px;
			}
			 
			/* The pointer image is hadded by using another span */
			.hint-pointer 
			{
				display: none; 
				position: absolute;
				width: 10px;
				height: 19px;
				background: url(../_common/images/pointer.gif) left top no-repeat;
			}
			.hint-pointer2 
			{
				display: none; 
				position: absolute;
				width: 10px;
				height: 19px;
				background: url(../_common/images/pointer.gif) left top no-repeat;
			}
		</style>
	</head>	
	<script type="text/javascript">
		/*''' <summary>
		''' Graba la solicitud o renovaci�n de uno o mas certificados
		''' </summary>
		''' <remarks>Llamada desde:cmdContinuar/onclick ; Tiempo m�ximo: 0,1</remarks>*/		
		function RegresarACertificado() {
			var accion = document.forms["Form1"].elements["txtAccion"].value

			var dpFechaDespublicacion = $find("<%=dpFechaDesPub.ClientID%>");
			var dpFechaLimiteCumplimentacion = $find("<%=dpFechaLimCumplim.ClientID%>");

			sFecLimite = dpFechaLimiteCumplimentacion.get_value();

			if (sFecLimite != null) {
				var diaLimite = sFecLimite.getDate()
				var mesLimite = sFecLimite.getMonth() + 1
				var anyoLimite = sFecLimite.getFullYear()
			} else {
				alert(arrTextosML[0])
				return false
			}

			sFecDespub = dpFechaDespublicacion.get_value();

			if (sFecDespub != null) {
				var dia = sFecDespub.getDate()
				var mes = sFecDespub.getMonth() + 1
				var anyo = sFecDespub.getFullYear()
			} else {
				var dia = "null"
				var mes = "null"
				var anyo = "null"
			}


			var p = window.opener
			p.GenerarCertificado(dia, mes, anyo, accion, diaLimite, mesLimite, anyoLimite, false)

			window.close()
		}
		/*''' <summary>
		''' Mostrar la ayuda para el Limite de Cumplimentacion
		''' </summary>
		''' <remarks>Llamada desde: imgAyudaLimiteCumplimentacion/ onclick; Tiempo m�ximo:0</remarks>*/		    
		function MostrarAyudaLimiteCumplimentacion() {
			document.getElementById("sAyudaLimiteCumplimentacion").style.display = "inline";
			document.getElementById("iAyudaLimiteCumplimentacion").style.display = "inline";
			document.getElementById("iAyudaLimiteCumplimentacion").style.left = document.getElementById("sAyudaLimiteCumplimentacion").offsetLeft - 9
			document.getElementById("iAyudaLimiteCumplimentacion").style.top = document.getElementById("sAyudaLimiteCumplimentacion").offsetTop
		}
		/*''' <summary>
		''' Mostrar la ayuda para el Limite de Despublicacion
		''' </summary>
		''' <remarks>Llamada desde: imgAyudaDespublicacion/ onclick; Tiempo m�ximo:0</remarks>*/		    
		function MostrarAyudaDespublicacion() {
			document.getElementById("sAyudaDespublicacion").style.display = "inline";
			document.getElementById("iAyudaDespublicacion").style.display = "inline";
			document.getElementById("iAyudaDespublicacion").style.left = document.getElementById("sAyudaDespublicacion").offsetLeft - 9
			document.getElementById("iAyudaDespublicacion").style.top = document.getElementById("sAyudaDespublicacion").offsetTop + 52

		}
		/*''' <summary>
		''' Ocultar la ayuda para el Limite de Despublicacion y el Limite de Cumplimentacion
		''' </summary>
		''' <param name="event">evento de pantalla</param>
		''' <remarks>Llamada desde: biody/onclick; Tiempo m�ximo: 0</remarks>*/
		function OcultarAyuda(event) {
			try{
				var node = event.target;
				while (node.nodeType != node.ELEMENT_NODE)
					node = node.parentNode;
			}
			catch (e) {
				var node = event.srcElement;
			}
			if (node.id != "imgAyudaLimiteCumplimentacion") {
				document.getElementById("sAyudaLimiteCumplimentacion").style.display = "none";
				document.getElementById("iAyudaLimiteCumplimentacion").style.display = "none";
			}

			if (node.id != "imgAyudaDespublicacion") {
				document.getElementById("sAyudaDespublicacion").style.display = "none";
				document.getElementById("iAyudaDespublicacion").style.display = "none";
			}
		}		    
	</script>	
	<body  onclick="OcultarAyuda(event)" style="padding:0px;">
		<form id="Form1" method="post" runat="server">
		<asp:ScriptManager ID="ScriptManager1" runat="server">
		</asp:ScriptManager>            
				<div style="clear:both; float:left; z-index:100; height:20px; width:100%; background-color:#646464; padding-top:10px;">
					<asp:label id="lblTitulo" runat="server" CssClass="titulo" style="color:White; padding-left:90px;">DCertificados</asp:label>
				</div>
				<div style="clear:both; float:left; height:20px; width:100%; background-color:#424242;">
					<asp:label id="lblSubTitulo" runat="server" CssClass="captionBlueSmall" style="color:White; font-weight:bold; padding-left:90px;">DFecha de cumplimentaci�n</asp:label>
				</div> 
				<div style="clear:both; float:left; width:100%; margin-left:10px; padding-top:25px;">
					<asp:label id="lblFechaLimiteCumplimentacion" runat="server" CssClass="captionBlue">DIntroduzca la fecha l�mite de cumplimentaci�n</asp:label>	                
				</div>
				<div style="clear:both; float:left; width:100%; margin-left:10px;">
					<div style="float:left;">
                        <igpck:WebDatePicker runat="server" ID="dpFechaLimCumplim" NullText="" SkinID="Calendario" Width="120px"></igpck:WebDatePicker>
					</div>
					<div style="float:left; padding-top:3px; padding-left:10px;">
						<img id="imgAyudaLimiteCumplimentacion" src="../alta/images/help.gif" alt="" border="0"  onclick="MostrarAyudaLimiteCumplimentacion()"/>                        
					</div>
				</div>
				<div style="clear:both; float:left; width:100%; margin-left:10px; padding-top:5px; padding-bottom:5px;">
					<asp:label id="lblFechaDespublicacion" runat="server" CssClass="captionBlueSmall" style="color:Black; float:none">DIntroduzca la fecha de despublicaci�n</asp:label>
				</div>
				<div style="clear:both; float:left; width:100%; margin-left:10px;">
					<div style="float:left;">
                        <igpck:WebDatePicker runat="server" ID="dpFechaDesPub" NullText="" SkinID="Calendario" Width="120px"></igpck:WebDatePicker>
					</div>
					<div style="float:left; padding-top:3px; padding-left:10px;">
						<img id="imgAyudaDespublicacion" src="../alta/images/help.gif" alt="" border="0" onclick="MostrarAyudaDespublicacion()"/>                        
					</div>                    
				</div>
				<div style="clear:both; width:100%; padding-top:10px; text-align:center;">
					<input class="botonPMWEB" id="cmdContinuar" onclick="RegresarACertificado()" type="button" value="DContinuar"
						name="cmdContinuar" runat="server"/>&nbsp;&nbsp;
					<input class="botonPMWEB" id="cmdCancelar" onclick="window.close()" type="button" value="DCancelar"
						name="cmdCancelar" runat="server"/>
				</div>
				<div style="background-color:Transparent; color:Black; z-index:101; position:absolute; top: 0px; left: 10px;">
					<img src="images/calendar.png" alt="" />
				</div>	
				<div id="divAyuda" style="background-color:Transparent; z-index:1001; width:100%; position:absolute; top:90px; left:15px;">
					<span id="sAyudaLimiteCumplimentacion" class="hint">	                        
						<asp:Label runat="server" ID="lblAyudaLimiteCumplimentacion" style="display:inline; color:#000; text-align:justify;" CssClass="parrafo"></asp:Label>                        
					</span>
					<span id="iAyudaLimiteCumplimentacion" class="hint-pointer" style="z-index:1003;">&nbsp;</span>
					<span id="sAyudaDespublicacion" class="hint2">	                        
						<asp:Label runat="server" ID="lblAyudaDespublicacion" style="display:inline; color:#000; text-align:justify;" CssClass="parrafo"></asp:Label>                        
					</span>                    
					<span id="iAyudaDespublicacion" class="hint-pointer2" style="z-index:1003;">&nbsp;</span>
				</div>                		
				<input id="txtAccion" type="hidden" runat="server"/>
		</form>
	</body>
</html>

