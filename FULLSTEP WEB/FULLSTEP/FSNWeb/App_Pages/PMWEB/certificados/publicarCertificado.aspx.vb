
    Public Class publicarCertificado
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents Respuesta As System.Web.UI.HtmlControls.HtmlInputHidden

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
    "<script language=""{0}"">{1}</script>"

    ''' <summary>
    ''' Evento de sistema, publica � despublica un certificado � modifica las fechas del certificado, todo
    ''' en funci�n de los diferentes request q puede recibir.
    ''' </summary>
    ''' <param name="sender">parametro de sistema</param>
    ''' <param name="e">parametro de sistema</param>            
    ''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,2 </remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Response.Expires = -1
        Dim oCertificado As FSNServer.Certificado
        Dim bRefrescar As Boolean
        Dim bHuboGuardados As Boolean = IIf(Request("HuboGuardados") Is Nothing OrElse Request("HuboGuardados") = 0, False, True)

        Dim sIdi As String = FSNUser.Idioma.ToString
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.PublicarCertificado, sIdi)
        Dim oTextos As DataTable = oDict.Data.Tables(0)

        'oUser = Session("FSN_User")

        oCertificado = FSNServer.Get_Object(GetType(FSNServer.Certificado))
        oCertificado.ID = Request("Certificado")


        Dim sClientTexts As String
        sClientTexts = ""
        sClientTexts += "var arrTextosML = new Array();"
        sClientTexts += "arrTextosML[0] = '" + JSText(oTextos.Rows(0).Item(1)) + "';"
        sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
        Page.ClientScript.RegisterStartupScript(Me.GetType(),"TextosMICliente", sClientTexts)

        'Comprueba si para este certificado se han modificado datos en el portal y no se han enviado
        If Request("FecDespubAnyo") = Nothing AndAlso Request("FecLimCumplAnyo") = Nothing Then
            If Request("Pub") = 0 Then
                'If oCertificado.ExistenDatosPortalSinEnviar = True Then
                '    'Manda el alert y no hace nada
                '    Page.ClientScript.RegisterStartupScript(Me.GetType(),"claveStartUpScript", "<script>avisoNoSePuedeDespublicar('" & oTextos.Rows(1).Item(1) & "')</script>")
                'Else
                'Despublica 
                oCertificado.Publicada = False
                oCertificado.ModificarPublicacion(True)
                bRefrescar = True
                'End If
            Else
                'Publica
                oCertificado.Publicada = True
                oCertificado.ModificarPublicacion(True)
                bRefrescar = True
            End If

        Else
            If Not Request("FecDespubAnyo") Is Nothing Then
                'Modifica la fecha de despublicaci�n
                If Request("FecDespubAnyo") = "null" Then
                    oCertificado.FechaDesPub = Nothing
                Else
                    Dim D As Date
                    D = New Date(Request("FecDespubAnyo"), Request("FecDespubMes"), Request("FecDespubDia"))
                    oCertificado.FechaDesPub = D
                End If
            End If

            If Not Request("FecLimCumplAnyo") Is Nothing Then
                'Modifica la fecha l�mite de cumplimentaci�n
                If Request("FecLimCumplAnyo") = "null" Then
                    oCertificado.FechaLimCumpl = Nothing
                Else
                    Dim D As Date
                    D = New Date(Request("FecLimCumplAnyo"), Request("FecLimCumplMes"), Request("FecLimCumplDia"))
                    oCertificado.FechaLimCumpl = D
                End If
            End If

            oCertificado.ModificarPublicacion(False)
        End If

        If Request("Refrescar") = 1 And bRefrescar = True Then
            If Not bHuboGuardados Then
                Page.ClientScript.RegisterStartupScript(Me.GetType(),"claveStartUpScript", "<script>Refrescar(" & oCertificado.ID & ");</script>")
            Else
                Page.ClientScript.RegisterStartupScript(Me.GetType(),"claveStartUpScript", "<script>RefrescarBts(" & Request("Pub") & ");</script>")
            End If
        End If

        oCertificado = Nothing
    End Sub

    End Class

