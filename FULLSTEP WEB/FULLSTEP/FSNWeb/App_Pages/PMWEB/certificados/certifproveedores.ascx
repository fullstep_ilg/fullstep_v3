<%@ Control Language="vb" AutoEventWireup="false" Codebehind="certifproveedores.ascx.vb" Inherits="Fullstep.FSNWeb.certifproveedores" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" enableViewState="False"%>
<script type="text/javascript">
    /*''' <summary>
    ''' Mostrar una pantalla de selecci�n de proveedores.
    ''' </summary>
    ''' <remarks>Llamada desde: imgBuscarProveedor; Tiempo m�ximo: 0</remarks>*/
	function BuscarProveedor() {
	    var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS") & "_common/BuscadorProveedores.aspx?" %>Certif=1&PM=false" , "_blank", "width=835,height=635,status=yes,resizable=no,top=0,left=150,scrollbars=yes");        
	}
		
	function prove_seleccionado2(sCIF, sProveCod, sProveDen,lIdContacto,sDenContacto) {
		copiarFila(vClientIDCertif,'1',sProveCod,sProveDen,lIdContacto,sDenContacto)	
	}	
	function ComprobarExistenProv() {
		var bExiste
		bExiste=false
		
		lNumFilas = parseFloat(document.getElementById(vClientIDCertif + "_numRows").value)
		for (i=1;i<=lNumFilas;i++)
		  {
			oEntry=fsGeneralEntry_getById(vClientIDCertif + "_fsdsentry_" + i.toString() + "_SOLICIT")
			if (oEntry)
			{
				if (oEntry.getDataValue()==true) 
					bExiste=true
			}
		 }
		return bExiste
	}	
</script>
<table id="tblDesglose" width="100%" runat="server">
</table>
<div style="OVERFLOW: hidden; WIDTH: 0px; POSITION: absolute; TOP: 0px; HEIGHT: 0px">
	<table id="tblDesgloseHidden" style="VISIBILITY: hidden" runat="server">
	</table>
	<input id="numRows" runat="server" type="hidden" />
</div>
