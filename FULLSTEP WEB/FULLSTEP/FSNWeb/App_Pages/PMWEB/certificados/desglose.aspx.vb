
    Public Class certdesglose
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblSubTitulo As System.Web.UI.WebControls.Label
        Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
        Protected WithEvents INPUTDESGLOSE As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents lblTituloData As System.Web.UI.WebControls.Label
        Protected WithEvents cmdGuardar As System.Web.UI.WebControls.Button
        Protected WithEvents cmdCalcular As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents Instancia As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents Solicitud As System.Web.UI.HtmlControls.HtmlInputHidden

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

    ''' <summary>
    ''' Carga el desglose en la ventana pop-up.
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">evento de sistema</param>        
    ''' <remarks>LLamada desde: Al cargarse la p�gina; Tiempo m�ximo: 0 sg.</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim idCampo As Integer = Request("campo")
        Dim sInput As String = Request("Input")
        Dim lInstancia As Integer = Request("instancia")
        Dim lSolicitud As Integer
        INPUTDESGLOSE.Value = sInput
        Instancia.Value = lInstancia

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Desglose

        Dim oCampo As FSNServer.Campo
        oCampo = FSNServer.Get_Object(GetType(FSNServer.Campo))
        oCampo.Id = idCampo

        If Request("Solicitud") <> Nothing Then
            lSolicitud = Request("Solicitud")
            Me.Solicitud.Value = lSolicitud
            oCampo.Load(Idioma, lSolicitud)
        Else
            oCampo.LoadInst(lInstancia, Idioma)
        End If

        Me.lblTitulo.Text = Textos(0)
        Me.lblTituloData.Text = oCampo.DenSolicitud(Idioma)

        Dim bSoloLectura As Boolean = (Request("SoloLectura") = "1")

        Dim oucDesglose As desgloseControl = Me.FindControl("ucDesglose")
        With oucDesglose
            .SoloLectura = bSoloLectura
            .Version = Request("Version")
            .VersionCert = Request("VersionBd")
            .Titulo = oCampo.DenGrupo(Idioma) + " / " + oCampo.Den(Idioma)
            .TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Certificado
        End With

        'En jsAlta.js hay para las funciones de a�adir linea y copia linea
        '       $create(AjaxControlToolkit.AutoCompleteBehavior ...rutaPM...
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ruta", "<script>var ruta = '" & ConfigurationManager.AppSettings("ruta") & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")
        'para el buscador de art�culos
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' </script>")

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "version") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "version", _
                "<script>var version = '" & System.Configuration.ConfigurationManager.AppSettings("version") & "';</script>")
        End If

        Me.FindControl("frmAlta").Controls.Add(FSNWeb.CommonAlta.InsertarCalendario())

        cmdGuardar.Text = Textos(2)
        cmdGuardar.Attributes.Add("onclick", "return actualizarCampoDesgloseYCierre()")
        cmdCalcular.Value = Textos(3)
    End Sub
End Class