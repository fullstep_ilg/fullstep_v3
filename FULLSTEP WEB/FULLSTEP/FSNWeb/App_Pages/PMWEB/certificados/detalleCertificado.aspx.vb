
    Public Class detalleCertificado
    Inherits FSNPage

#Region " Web Form Designer Generated Code "
    Protected WithEvents uwtGrupos As Infragistics.WebUI.UltraWebTab.UltraWebTab
    Protected WithEvents lblLitProveedor As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitFecAlta As System.Web.UI.WebControls.Label
    Protected WithEvents lblProveedor As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecAlta As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecDespub As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecLimCumpl As System.Web.UI.WebControls.Label
    Protected WithEvents Instancia As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents chkPub As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblVersion As System.Web.UI.WebControls.Label
    Protected WithEvents Certificado As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblFechaDespub As System.Web.UI.WebControls.Label
    Protected WithEvents lblFechaLimCumpl As System.Web.UI.WebControls.Label
    Protected WithEvents lblPub As System.Web.UI.WebControls.Label
    Protected WithEvents txtEnviar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Version As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents VersionInicialCargada As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblAviso As System.Web.UI.WebControls.Label
    Protected WithEvents lblAvisoProveedorBaja As System.Web.UI.WebControls.Label
    Protected WithEvents lblAvisoProveedorNoContacto As System.Web.UI.WebControls.Label
    Protected WithEvents Proveedor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Email As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cadenaespera As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtfsFecDesPub As DataEntry.GeneralEntry
    Protected WithEvents txtfsFecLimCumpl As DataEntry.GeneralEntry
    Protected WithEvents FuerzaDesgloseLectura As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Tipo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents FecAlta As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents DenProveedor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblProgreso As System.Web.UI.WebControls.TextBox
    Protected WithEvents imgProgreso As System.Web.UI.WebControls.Image
    Protected WithEvents tblProgreso As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents indiceCombo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents diferenciaUTCServidor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblPublicado As System.Web.UI.WebControls.Label
    Protected WithEvents ComboL As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ComboV As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents FSNPageHeader As Fullstep.FSNWebControls.FSNPageHeader
    Protected WithEvents lblCheckBox As System.Web.UI.WebControls.Label
    Protected WithEvents wddVersion As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents imgProveedorBaja As System.Web.UI.WebControls.Image
    Protected WithEvents imgProveedorNoContacto As System.Web.UI.WebControls.Image
    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private _oInstancia As FSNServer.Instancia
    ''' <summary>
    ''' Propiedad que carga la informacion de la instancia
    ''' </summary>
    Protected ReadOnly Property oInstancia() As FSNServer.Instancia
        Get
            If _oInstancia Is Nothing Then
                If Me.IsPostBack Then
                    _oInstancia = CType(Cache("oInstancia_" & FSNUser.Cod), FSNServer.Instancia)
                Else
                    _oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))

                    If Request("version") <> Nothing Then
                        _oInstancia.ID = Request("Instancia")
                    Else
                        _oInstancia.ID = oCertificado.Instancia
                    End If

                    _oInstancia.Version = oCertificado.Version

                    If oCertificado.ProveedorBaja Then
                        _oInstancia.CargarCamposInstancia(FSNUser.Idioma.ToString, oCertificado.PeticionarioCod, oCertificado.ProveedorBaja)
                    ElseIf oCertificado.PeticionarioCod = FSNUser.CodPersona Or FSNUser.QACertifModifOtrosUsu = False Then
                        _oInstancia.CargarCamposInstancia(FSNUser.Idioma.ToString, FSNUser.CodPersona, oCertificado.Proveedor)
                    ElseIf oCertificado.PeticionarioCod = "" Then
                        'considere al usuario como peticionario. Para los certificados
                        'creados automaticamente, lo que implica peticionario nulo.
                        _oInstancia.CargarCamposInstancia(FSNUser.Idioma.ToString, FSNUser.CodPersona, oCertificado.Proveedor)
                    ElseIf FSNUser.QACertifModifOtrosUsu = True Then
                        'si tiene permisos de modificaci�n carga la configuraci�n del peticionario:
                        _oInstancia.CargarCamposInstancia(FSNUser.Idioma.ToString, oCertificado.PeticionarioCod, oCertificado.Proveedor)
                    End If

                    Me.InsertarEnCache("oInstancia_" & FSNUser.Cod, _oInstancia)
                End If
            End If
            Return _oInstancia
        End Get
    End Property

    Private _oCertificado As FSNServer.Certificado
    ''' <summary>
    ''' Propiedad que carga la informacion del Certificado
    ''' </summary>
    Protected ReadOnly Property oCertificado() As FSNServer.Certificado
        Get
            If _oCertificado Is Nothing Then
                If Me.IsPostBack Then
                    _oCertificado = CType(Cache("oCertificado_" & FSNUser.Cod), FSNServer.Certificado)
                Else
                    _oCertificado = FSNServer.Get_Object(GetType(FSNServer.Certificado))
                    _oCertificado.ID = Request("Certificado")
                    _oCertificado.Load(FSNUser.Idioma.ToString)

                    If Request("version") <> Nothing Then _oCertificado.Version = Request("version")

                    Me.InsertarEnCache("oCertificado_" & FSNUser.Cod, _oCertificado)
                End If
            End If
            Return _oCertificado
        End Get
    End Property

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"

    ''' <summary>
    ''' Evento de sistema, carga de pantalla con el detalle de un certificado. Dependiendo de si el usuario
    ''' sea peticionario, proveedor este de baja y sea certificado automatico mostrara unos botones/campos u
    ''' otros. Si hay una versi�n de tipo 3 (->portal guardado sin enviar) no deja grabar aunque se muestre
    ''' el bot�n guardar.
    ''' </summary>
    ''' <param name="sender">parametro de sistema</param>
    ''' <param name="e">parametro de sistema</param>            
    ''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0 </remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Response.Expires = -1
        If Page.IsPostBack Then
            CargarCamposGrupo()
            Exit Sub
        End If

        Dim iVersion As Long
        Dim sIdi As String = FSNUser.Idioma.ToString

        Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.DetalleCertificado

        'MONTAR CABECERA
        '===============
        FSNPageHeader.VisibleBotonMail = True
        FSNPageHeader.VisibleBotonImpExp = True
        FSNPageHeader.VisibleBotonEliminar = True
        FSNPageHeader.VisibleBotonGuardar = True
        If Request("cn") IsNot Nothing Then
            FSNPageHeader.VisibleBotonVolverColaboracion = True
            FSNPageHeader.OnClientClickVolverColaboracion = "HistoryHaciaAtras();return false;"
        Else
            FSNPageHeader.VisibleBotonVolver = True
        End If

        FSNPageHeader.TextoBotonMail = Textos(14)
        FSNPageHeader.TextoBotonImpExp = Textos(10)
        FSNPageHeader.TextoBotonEliminar = Textos(1)
        FSNPageHeader.TextoBotonGuardar = Textos(2)
        If Request("cn") IsNot Nothing Then
            FSNPageHeader.TextoBotonVolverColaboracion = Textos(34)
        Else
            FSNPageHeader.TextoBotonVolver = Textos(13)
            FSNPageHeader.OnClientClickVolver = "HistoryHaciaAtras();return false;"
        End If
        FSNPageHeader.TextoBotonCalcular = Textos(37)

        'Comprueba si hay que hacer visible el bot�n de calcular
        FSNPageHeader.VisibleBotonCalcular = False

        'SI EXISTE ALGUN CAMPO DE TIPO=3 (CALCULADO) HACEMOS VISIBLE EL BOTON DE CALCULAR
        FSNPageHeader.VisibleBotonCalcular = oInstancia.Grupos.Grupos.OfType(Of FSNServer.Grupo).Where(Function(x) x.DSCampos.Tables(0).Rows.OfType(Of DataRow).Where(Function(y) y("TIPO") = TipoCampoPredefinido.Calculado).Any).Any()
        If FSNPageHeader.VisibleBotonCalcular Then FSNPageHeader.OnClientClickCalcular = "return CalcularCamposCalculados()"

        FSNPageHeader.OnClientClickMail = "EnviarMail();return false;"
        FSNPageHeader.OnClientClickImpExp = "cmdImpExp_onclick();return false;"
        FSNPageHeader.OnClientClickEliminar = "EliminarVersion();return false;"
        FSNPageHeader.OnClientClickGuardar = "Guardar(); return false;"

        Dim sClientTextVars As String
        sClientTextVars = ""
        sClientTextVars += "vdecimalfmt='" + FSNUser.DecimalFmt + "';"
        sClientTextVars += "vthousanfmt='" + FSNUser.ThousanFmt + "';"
        sClientTextVars += "vprecisionfmt='" + FSNUser.PrecisionFmt + "';"
        sClientTextVars += "vDatefmt='" + FSNUser.DateFormat.ShortDatePattern + "';"
        sClientTextVars = String.Format(IncludeScriptKeyFormat, "javascript", sClientTextVars)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsUser", sClientTextVars)

        Dim sVolver As String
        If Request("volver") <> "" Then
            sVolver = Request("volver")
            sVolver = Replace(sVolver, "*", "&")
            Session("VolverdetalleCertificado") = sVolver
        End If

        If Request("version") <> Nothing Then
            If Request("Version") <> oCertificado.Version Or oCertificado.Instancia <> Request("Instancia") Then
                FSNPageHeader.VisibleBotonGuardar = False
            End If
        End If

        If FSNUser.AccesoCN Then
            If Not Page.ClientScript.IsClientScriptBlockRegistered("EntidadColaboracion") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "EntidadColaboracion",
                    "var TipoEntidadColaboracion='CE';" &
                    "var CodigoEntidadColaboracion ={identificador:" & oCertificado.Instancia &
                                                    ",texto:'" & JSText(oCertificado.TipoDen) & "'" &
                                                    ",proveedor:'" & JSText(oCertificado.ProveDen) & "'};", True)
            End If
        End If
        oInstancia.Version = oCertificado.Version

        Me.FuerzaDesgloseLectura.Value = "0"
        If oCertificado.ProveedorBaja Then
            Me.FuerzaDesgloseLectura.Value = "1"
        End If

        Me.Version.Value = oInstancia.Version
        Me.VersionInicialCargada.Value = oInstancia.Version

        Me.Instancia.Value = oInstancia.ID
        Me.Certificado.Value = oCertificado.ID
        Me.Proveedor.Value = oCertificado.Proveedor
        Me.DenProveedor.Value = oCertificado.ProveDen
        Me.Tipo.Value = oCertificado.TipoDen
        Me.FecAlta.Value = oCertificado.FechaPub

        'Obtener email
        Dim oProves As FSNServer.Proveedores
        oProves = FSNServer.Get_Object(GetType(FSNServer.Proveedores))
        oProves.CargarEmailProv(Proveedor.Value, , , True)
        If oProves.Data.Tables(0).Rows.Count = 0 Then
            Email.Value = ""
        Else
            Email.Value = DBNullToStr(oProves.Data.Tables(0).Rows(0)("EMAIL"))
        End If
        imgProveedorBaja.Visible = False
        lblAvisoProveedorBaja.Visible = False
        imgProveedorNoContacto.Visible = False
        lblAvisoProveedorNoContacto.Visible = False

        If oCertificado.ProveedorBaja Then
            imgProveedorBaja.Visible = True
            lblAvisoProveedorBaja.Visible = True
        ElseIf Email.Value = "" Then
            imgProveedorNoContacto.Visible = True
            lblAvisoProveedorNoContacto.Visible = True
        End If
        Me.chkPub.Attributes("onclick") = "Publicar()"

        'Textos de idiomas:
        Me.lblLitProveedor.Text = Textos(3)
        Me.lblLitFecAlta.Text = Textos(4)
        Me.lblFecDespub.Text = Textos(6)
        Me.lblFecLimCumpl.Text = Textos(21)
        Me.lblCheckBox.Text = Textos(5)
        Me.chkPub.Text = ""
        Me.lblVersion.Text = Textos(7)
        Me.lblAviso.Text = Textos(9)
        Me.lblAvisoProveedorNoContacto.Text = Textos(35)
        Me.lblAvisoProveedorBaja.Text = Textos(36)

        FSNPageHeader.TituloCabecera = oCertificado.TipoDen
        lblProveedor.Text = AjustarAnchoTextoPixels(oCertificado.Proveedor & " - " & oCertificado.ProveDen, 275, "Verdana", 12)
        lblProveedor.ToolTip = oCertificado.Proveedor & " - " & oCertificado.ProveDen
        lblFecAlta.Text = oCertificado.FechaPub

        sClientTextVars = ""
        sClientTextVars += "sAnyoAlta='" + Year(oCertificado.FechaPub).ToString + "';"
        sClientTextVars += "sMesAlta='" + Month(oCertificado.FechaPub).ToString + "';"
        sClientTextVars += "sDiaAlta='" + Day(oCertificado.FechaPub).ToString + "';"
        sClientTextVars += "sHorasAlta='" + Hour(oCertificado.FechaPub).ToString + "';"
        sClientTextVars += "sMinutosAlta='" + Minute(oCertificado.FechaPub).ToString + "';"
        sClientTextVars += "sSegundosAlta='" + Second(oCertificado.FechaPub).ToString + "';"
        sClientTextVars = String.Format(IncludeScriptKeyFormat, "javascript", sClientTextVars)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsFechaAlta", sClientTextVars)

        Me.txtfsFecLimCumpl.TipoGS = TiposDeDatos.TipoCampoGS.FechaLimCumplim
        Me.txtfsFecLimCumpl.DateFormat = FSNUser.DateFormat

        If IsTime(oCertificado.FechaLimCumpl) Then
            lblFechaLimCumpl.Text = ""
            Me.txtfsFecLimCumpl.Valor = ""
        Else
            lblFechaLimCumpl.Text = FormatDate(oCertificado.FechaLimCumpl, FSNUser.DateFormat)
            Me.txtfsFecLimCumpl.Valor = oCertificado.FechaLimCumpl
        End If

        Me.txtfsFecDesPub.TipoGS = TiposDeDatos.TipoCampoGS.FecDespublicacion
        Me.txtfsFecDesPub.DateFormat = FSNUser.DateFormat

        If IsTime(oCertificado.FechaDesPub) Then
            lblFechaDespub.Text = ""
            Me.txtfsFecDesPub.Valor = ""
        Else
            lblFechaDespub.Text = FormatDate(oCertificado.FechaDesPub, FSNUser.DateFormat)
            Me.txtfsFecDesPub.Valor = oCertificado.FechaDesPub
        End If

        Me.lblPub.Text = Textos(8)

        If oCertificado.Publicada = True Then
            Me.chkPub.Checked = True
            Me.lblPublicado.Text = Textos(22)
            lblAviso.Visible = True
        Else
            Me.chkPub.Checked = False
            Me.lblPublicado.Text = Textos(23)
            lblAviso.Visible = False
        End If

        'Si no es el peticionario s�lo podr� consultar los datos
        '29-11 Epb dice que considere al usuario como peticionario. Para los certificados
        'creados automaticamente, lo que implica peticionario nulo.
        If oCertificado.ProveedorBaja OrElse Email.Value = "" Then
            Me.lblPub.Visible = True
            Me.lblPublicado.Visible = True
            Me.lblCheckBox.Visible = False
            Me.chkPub.Visible = False
            If lblFechaDespub.Text = "" Then
                Me.lblFechaDespub.Visible = False
                Me.lblFecDespub.Visible = False
            Else
                Me.lblFechaDespub.Visible = True
            End If
            If lblFechaLimCumpl.Text = "" Then
                Me.lblFechaLimCumpl.Visible = False
                Me.lblFecLimCumpl.Visible = False
            Else
                Me.lblFechaLimCumpl.Visible = True
            End If
            Me.txtfsFecDesPub.Visible = False
            Me.txtfsFecLimCumpl.Visible = False
            FSNPageHeader.VisibleBotonGuardar = False
            FSNPageHeader.VisibleBotonEliminar = False
            lblAviso.Visible = False
        ElseIf oCertificado.PeticionarioCod = FSNUser.CodPersona _
        OrElse oCertificado.PeticionarioCod = "" Then
            Me.lblFechaDespub.Visible = False
            Me.txtfsFecDesPub.Visible = True
            Me.lblFechaLimCumpl.Visible = False
            Me.txtfsFecLimCumpl.Visible = True

            Me.lblPub.Visible = False
            Me.lblPublicado.Visible = False
            Me.chkPub.Visible = True
            Me.lblCheckBox.Visible = True
            If oCertificado.Publicada = True Then
                FSNPageHeader.VisibleBotonGuardar = False
            End If
        Else
            'si tiene permisos de mod. la publicaci�n de certificados de otros usuarios:
            If FSNUser.QACertifPubOtrosUsu = True Then
                Me.lblPub.Visible = False
                Me.lblPublicado.Visible = False
                Me.chkPub.Visible = True
                Me.lblCheckBox.Visible = True
                Me.lblFechaDespub.Visible = False
                Me.txtfsFecDesPub.Visible = True
                Me.lblFechaLimCumpl.Visible = False
                Me.txtfsFecLimCumpl.Visible = True
            Else
                Me.lblPub.Visible = True
                Me.lblPublicado.Visible = True
                Me.chkPub.Visible = False
                Me.lblCheckBox.Visible = False
                If lblFechaDespub.Text = "" Then
                    Me.lblFechaDespub.Visible = False
                    Me.lblFecDespub.Visible = False
                Else
                    Me.lblFechaDespub.Visible = True
                End If
                Me.txtfsFecDesPub.Visible = False

                Me.txtfsFecLimCumpl.Visible = False
            End If

            'si tiene permisos de mod. los datos de certificados de otros usuarios:
            If FSNUser.QACertifModifOtrosUsu = True Then
                If oCertificado.Publicada = True Then
                    FSNPageHeader.VisibleBotonGuardar = False
                End If
            Else
                FSNPageHeader.VisibleBotonEliminar = False
                FSNPageHeader.VisibleBotonGuardar = False
                lblAviso.Visible = False
            End If

        End If

        CargarCamposGrupo()

        ''''''Combo de versi�n: 
        oCertificado.LoadVersiones()

        Dim dt As New DataTable
        dt.Columns.Add("CLAVE", GetType(System.String))
        dt.Columns.Add("TEXTO", GetType(System.String))

        diferenciaUTCServidor.Value = oInstancia.DiferenciaUTCServidor

        indiceCombo.Value = -1

        If oCertificado.Data.Tables(0).Rows.Count = 0 Then
            Me.lblVersion.Attributes.Add("style", "visibility: hidden")
            Me.wddVersion.Attributes.Add("style", "visibility: hidden")
        End If

        Dim Index As Integer = 0
        Dim dFecha As Date
        Dim sQuien As String
        Dim DifUTC As Integer = oInstancia.DiferenciaUTCServidor
        Dim DifOtz As Integer = Request.QueryString("Otz")

        For Each row As DataRow In oCertificado.Data.Tables(0).Rows
            If row.Item("NUM_VERSION") = oCertificado.Version AndAlso row.Item("INSTANCIA") = Me.Instancia.Value Then
                indiceCombo.Value = Index
            Else
                Index = Index + 1
            End If

            dFecha = row.Item("FECHA_MODIF")
            dFecha = dFecha.AddMinutes(-1 * (DifUTC + DifOtz))

            sQuien = IIf(IsDBNull(row.Item("PERSONA")), IIf(IsDBNull(row.Item("USUPORT_NOM")), row.Item("PROVEEDOR"), row.Item("USUPORT_NOM")), row.Item("PERSONA"))

            Dim rowAdd As DataRow = dt.NewRow

            rowAdd.Item("CLAVE") = CStr(row.Item("INSTANCIA")) & "##"
            rowAdd.Item("CLAVE") = rowAdd.Item("CLAVE") & CStr(row.Item("NUM_VERSION"))

            rowAdd.Item("TEXTO") = FormatDate(dFecha, FSNUser.DateFormat, "g") & " " & sQuien

            dt.Rows.Add(rowAdd)
        Next


        wddVersion.DataSource = dt

        wddVersion.TextField = "TEXTO"
        wddVersion.ValueField = "CLAVE"

        wddVersion.DataBind()

        Me.wddVersion.SelectedItemIndex = indiceCombo.Value

        '---------------

        Dim sScript As String
        Dim ilGMN1 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN1
        Dim ilGMN2 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN2
        Dim ilGMN3 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN3
        Dim ilGMN4 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN4
        sScript = ""
        sScript += "var ilGMN1 = " + ilGMN1.ToString() + ";"
        sScript += "var ilGMN2 = " + ilGMN2.ToString() + ";"
        sScript += "var ilGMN3 = " + ilGMN3.ToString() + ";"
        sScript += "var ilGMN4 = " + ilGMN4.ToString() + ";"
        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "LongitudesCodigosKey", sScript)

        If Not Page.ClientScript.IsClientScriptIncludeRegistered("claveArrayDesgloses") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "claveArrayDesgloses", "<script>var arrDesgloses = new Array()</script>")
        End If

        'En jsAlta.js hay para las funciones de a�adir linea y copia linea
        '       $create(AjaxControlToolkit.AutoCompleteBehavior ...rutaPM...
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")
        'para el buscador de art�culos
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' </script>")

        Dim sClientTexts As String
        sClientTexts = ""
        sClientTexts += "var arrTextosML = new Array();"
        sClientTexts += "arrTextosML[0] = '" + JSText(Textos(11)) + "';"
        sClientTexts += "arrTextosML[1] = '" + JSText(Textos(12)) + "';"
        sClientTexts += "arrTextosML[2] = '" + JSText(Textos(20)) + "';"
        sClientTexts += "arrTextosML[3] = '" + JSText(Textos(24)) + "';" 'Error Ajax
        sClientTexts += "arrTextosML[4] = '" + JSText(Textos(25)) + "';" 'QA lo ha renovado
        sClientTexts += "arrTextosML[5] = '" + JSText(Textos(26)) + "';" 'en proceso
        sClientTexts += "arrTextosML[6] = '" + JSText(Textos(27)) + "';" 'Guardar.En este caso en vez de un alert mostraremos un confirm:		                
        'Si el usuario confirma la acci�n, antes de llamar a guardarInstancia procederemos a eliminar la versi�n de tipo 3
        sClientTexts += "arrTextosML[7] = '" + JSText(Textos(28)) + "';" 'Si no es misma versi�n en funci�n del tipo mostraremos un  
        'alert y el usuario no podr� guardar los cambios. Tipo Usu Qa
        sClientTexts += "arrTextosML[8] = '" + JSText(Textos(29)) + "';" 'Si no es misma versi�n en funci�n del tipo mostraremos un  
        'alert y el usuario no podr� guardar los cambios. Tipo Proveedor
        sClientTexts += "arrTextosML[9] = '" + JSText(Textos(30)) + "';" 'El proveedor tiene datos guardados sin 
        'enviar, �Est� seguro que desea despublicar el certificado antes de que el proveedor le env�e los 
        'cambios?
        sClientTexts += "arrTextosML[10] = '" + JSText(Textos(31)) + "';" 'Eliminar.En este caso en vez de un alert mostraremos un confirm:		                
        'Si el usuario confirma la acci�n, antes de llamar a guardarInstancia procederemos a eliminar la versi�n de tipo 3
        sClientTexts += "arrTextosML[11] = '" + JSText(Textos(32)) + "';" 'Cerrar.En este caso en vez de un alert mostraremos un confirm:		                
        'Si el usuario confirma la acci�n, antes de llamar a guardarInstancia procederemos a eliminar la versi�n de tipo 3
        sClientTexts += "arrTextosML[12] = '" + JSText(Textos(33)) + "';"
        sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)

        'Tema de la cadena espera
        Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.Espera

        Me.cadenaespera.Value = Textos(1)

    End Sub

    ''' Revisado por: blp. Fecha: 05/10/2011
    ''' <summary>
    ''' Genera el tab con los datos del formulario asociado a la solicitud
    ''' </summary>
    ''' <remarks>Llamada desde: page_load ;Tiempo maximo:0.3</remarks>
    Private Sub CargarCamposGrupo()
        Dim oGrupo As FSNServer.Grupo
        Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab
        Dim oucCampos As campos
        Dim oucDesglose As desgloseControl
        Dim oRow As DataRow
        Dim lIndex As Integer = 0
        Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden

        'Rellena los grupos y campos de la instancia:
        uwtGrupos.Tabs.Clear()
        AplicarEstilosTab(uwtGrupos)

        If Not oInstancia.Grupos.Grupos Is Nothing Then
            For Each oGrupo In oInstancia.Grupos.Grupos
                oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden

                oInputHidden.ID = "txtPre_" + lIndex.ToString
                oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
                Dim Font As String = ObtenerValorPropiedad(".uwtDefaultTab", "font-family")
                Dim Size As String = Replace(ObtenerValorPropiedad(".uwtDefaultTab", "font-size"), "pt", "")
                oTabItem.Text = AjustarAnchoTextoPixels(oGrupo.Den(FSNUser.Idioma.ToString), AnchoDeTab, IIf(Font = "", "verdana", Font), IIf(Size = "", 8, CInt(Size)), False)
                oTabItem.Key = lIndex.ToString
                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Hidden
                uwtGrupos.Tabs.Add(oTabItem)
                lIndex += 1

                If oGrupo.NumCampos <= 1 Then
                    For Each oRow In oGrupo.DSCampos.Tables(0).Rows
                        If oRow.Item("VISIBLE") = 1 Then
                            Exit For
                        End If
                    Next
                    If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Desglose Or (DBNullToSomething(oRow.Item("SUBTIPO")) = TiposDeDatos.TipoGeneral.TipoDesglose And DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.DesgloseActividad) Then
                        If oRow.Item("VISIBLE") = 0 Then
                            uwtGrupos.Tabs.Remove(oTabItem)
                        Else
                            oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Auto
                            oTabItem.ContentPane.UserControlUrl = "..\alta\desglose.ascx"
                            oucDesglose = oTabItem.ContentPane.UserControl
                            oucDesglose.ID = oGrupo.Id.ToString
                            oucDesglose.Campo = oRow.Item("ID_CAMPO")
                            oucDesglose.TieneIdCampo = True
                            oucDesglose.Ayuda = DBNullToSomething(oRow.Item("AYUDA_" & Idioma))
                            oucDesglose.TabContainer = uwtGrupos.ClientID
                            oucDesglose.Instancia = oInstancia.ID
                            oucDesglose.Version = oInstancia.Version
                            oucDesglose.SoloLectura = ((oRow.Item("ESCRITURA") = 0) OrElse (oCertificado.ProveedorBaja OrElse Email.Value = ""))
                            oucDesglose.FuerzaLectura = oCertificado.ProveedorBaja
                            oucDesglose.Proveedor = oCertificado.Proveedor
                            oucDesglose.Titulo = DBNullToSomething(oRow.Item("DEN_" & Idioma))
                            oucDesglose.TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Certificado
                            oucCampos.InstanciaMoneda = oInstancia.Moneda
                            oucCampos.TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Certificado
                            oInputHidden.Value = oucDesglose.ClientID
                        End If
                    Else
                        oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Auto
                        oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
                        oucCampos = oTabItem.ContentPane.UserControl
                        oucCampos.Instancia = oInstancia.ID
                        oucCampos.ID = oGrupo.Id.ToString
                        oucCampos.dsCampos = oGrupo.DSCampos
                        oucCampos.IdGrupo = oGrupo.Id
                        oucCampos.Idi = FSNUser.Idioma.ToString
                        oucCampos.TabContainer = uwtGrupos.ClientID
                        oucCampos.Version = oInstancia.Version
                        oucCampos.Proveedor = oCertificado.Proveedor
                        oucCampos.SoloLectura = (oCertificado.ProveedorBaja OrElse Email.Value = "")
                        oucCampos.FuerzaDesgloseLectura = oCertificado.ProveedorBaja
                        oucCampos.InstanciaMoneda = oInstancia.Moneda
                        oucCampos.TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Certificado
                        oucCampos.ObjInstancia = oInstancia
                        oucCampos.Formulario = oInstancia.Solicitud.Formulario
                        oInputHidden.Value = oucCampos.ClientID
                    End If
                Else
                    oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Auto
                    oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
                    oucCampos = oTabItem.ContentPane.UserControl
                    oucCampos.Instancia = oInstancia.ID
                    oucCampos.ID = oGrupo.Id.ToString
                    oucCampos.dsCampos = oGrupo.DSCampos
                    oucCampos.IdGrupo = oGrupo.Id
                    oucCampos.Idi = FSNUser.Idioma.ToString
                    oucCampos.TabContainer = uwtGrupos.ClientID
                    oucCampos.TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Certificado
                    oucCampos.Version = oInstancia.Version
                    oucCampos.Proveedor = oCertificado.Proveedor
                    oucCampos.SoloLectura = (oCertificado.ProveedorBaja OrElse Email.Value = "")
                    oucCampos.FuerzaDesgloseLectura = oCertificado.ProveedorBaja
                    oucCampos.ObjInstancia = oInstancia
                    oucCampos.Formulario = oInstancia.Solicitud.Formulario
                    oInputHidden.Value = oucCampos.ClientID
                End If
                Me.FindControl("frmDetalle").Controls.Add(oInputHidden)
            Next
        End If
        Me.FindControl("frmDetalle").Controls.Add(FSNWeb.CommonAlta.InsertarCalendario())
    End Sub

End Class

