Public Class eliminarCertificado
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"

    ''' <summary>
    ''' Elimina la versi�n del certificado
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>Llamada desde: La propia p�gina, cada vez que se carga
    ''' Tiempo m�ximo:0,3</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Elimina la versi�n del certificado
        Dim oCertificado As FSNServer.Certificado
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oUser As FSNServer.User = Session("FSN_User")

        oCertificado = FSWSServer.Get_Object(GetType(FSNServer.Certificado))
        oCertificado.ID = Request("Certificado")
        oCertificado.Instancia = Request("Instancia")
        oCertificado.Version = Request("Version")

        oCertificado.EliminarVersionesCertificado(oUser.CodPersona)
        If oCertificado.ID = 0 Then
            'Ha eliminado el certificado entero, va al visor
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>IrAVisorCertif();</script>")
        Else
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>Refrescar(" & oCertificado.ID & ")</script>")
        End If
        oCertificado = Nothing
    End Sub
End Class