﻿<%@ Register TagPrefix="fsde" Namespace="Fullstep.DataEntry" Assembly="DataEntry" %>
<%@ Register Assembly="Infragistics.WebUI.WebDateChooser.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.WebSchedule" TagPrefix="igsch" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="detalleCertificado.aspx.vb" Inherits="Fullstep.FSNWeb.detalleCertificado"%>
<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<%@ Register Assembly="Infragistics.WebUI.UltraWebTab.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head runat="server">
		<title>detalleCertificado</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
    
	<script type="text/javascript"><!--
		//Muestra el contenido peligroso que ha efectuado un error al guardar.
		function ErrorValidacion(contenido) {
			alert(arrTextosML[12].replace("$$$", contenido));
			OcultarEspera();
		}
			
			
		// 1.- Creamos el objeto xmlHttpRequest
		CreateXmlHttp();
		var bEnProceso = false;

		function CreateXmlHttp() {
			// Probamos con IE
			try {
				// Funcionará para JavaScript 5.0
				xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e) {
				try {
					xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (oc) {
					xmlHttp = null;
				}
			}
			// Si no se trataba de un IE, probamos con esto
			if (!xmlHttp && typeof XMLHttpRequest != "undefined") {
				xmlHttp = new XMLHttpRequest();
			}
			return xmlHttp;
		}
			
		wProgreso = null;

		/*''' <summary>
		''' Elimina una versión del certificado
		''' </summary>
		''' <remarks>Llamada desde: cmdEliminar; Tiempo máximo: 0,2</remarks>*/
		function EliminarVersion() {
			if(document.getElementById("lnkBotonEliminar"))
			    if (document.getElementById("lnkBotonEliminar").disabled)
					return false
					
			if (confirm(arrTextosML[0]) == true) {
			    CreateXmlHttp();
				if (xmlHttp) {		            		            
					var params = "Certificado=" + document.forms["frmDetalle"].elements["Certificado"].value + "&Instancia=" + document.forms["frmDetalle"].elements["Instancia"].value + "&Version=" + document.forms["frmDetalle"].elements["Version"].value;
																		
					xmlHttp.open("POST", "../_common/controlarVersionQA.aspx", false);
					xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
					xmlHttp.send(params);

					resultadoProcesarAccionesQA(2);
				}
				else {
					alert(arrTextosML[3]);
				}
			}
		}

		//''' <summary>
		//''' Grabar el certificado. Si hay una versión de tipo 3 (->portal guardado sin enviar) no deja grabar.
		//''' </summary>
		//''' <returns>Nada</returns>
		//''' <remarks>Llamada desde: bt cmdGuardar; Tiempo máximo:0</remarks>	
		function Guardar() {
			if (comprobarTextoLargo() == false)
				return false
					
			if (document.getElementById("lnkBotonGuardar")) {
			    if (document.getElementById("lnkBotonGuardar").disabled)
					return false;
			}
			
            if (document.getElementById("lnkBotonMail")) document.getElementById("lnkBotonMail").disabled = true;
            if (document.getElementById("lnkBotonImpExp")) document.getElementById("lnkBotonImpExp").disabled = true;
            if (document.getElementById("lnkBotonEmitir")) document.getElementById("lnkBotonEmitir").disabled = true;
            if (document.getElementById("lnkBotonGuardar")) document.getElementById("lnkBotonGuardar").disabled = true;
            if (document.getElementById("lnkBotonEliminar")) document.getElementById("lnkBotonEliminar").disabled = true;
            if (document.getElementById("lnkBotonVolver")) document.getElementById("lnkBotonVolver").disabled = true;
            if (document.getElementById("chkPub")) document.getElementById("chkPub").style.visibility = 'hidden';
            
			if (wProgreso == null) {
				wProgreso = true;
				MostrarEspera();
			}

			CreateXmlHttp();
			if (xmlHttp) {                
				var params = "Certificado=" + document.forms["frmDetalle"].elements["Certificado"].value + "&Instancia=" + document.forms["frmDetalle"].elements["Instancia"].value + "&Version=" + document.forms["frmDetalle"].elements["Version"].value;

				xmlHttp.open("POST", "../_common/controlarVersionQA.aspx", false);
				xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
				xmlHttp.send(params);

				resultadoProcesarAccionesQA(1);
			}
			else {
				alert(arrTextosML[3]);

				HabilitarBotones()
				wProgreso = null;
			}		        
		}

		/*''' <summary>
		''' Tras q se ejecute sincronamente controlarVersionQA.aspx controlamos sus resultados y obramos en 
		''' consecuencia.
		''' </summary>
		''' <param name="Quien">1 guardando 2 eliminando</param>        
		''' <remarks>Llamada desde: EliminarVersion     Guardar     Cerrar; Tiempo máximo: 0</remarks>*/
		function resultadoProcesarAccionesQA( Quien) {
			var retorno;
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
				retorno = xmlHttp.responseText;

				if (retorno == 1) { //en proceso
					alert(arrTextosML[5])
					HabilitarBotones()
					wProgreso = null;
					return
				}
				else
					if (retorno.search("Version") > -1) { //Si no es misma version, y la última versión es de tipo 3, quiere decir
						//que el proveedor tiene una versión guardada sin enviar.
						if (Quien == 1)
							Mensaje = 6
						else
							if (Quien == 2)
							Mensaje = 10
						else
							if (Quien == 3)
							Mensaje = 11
						if (confirm(arrTextosML[Mensaje]) == true) {//En este caso en vez de un alert mostraremos un confirm:		                
							//Si el usuario confirma la acción, antes de llamar a guardarInstancia procederemos a eliminar la versión de tipo 3
							retorno = retorno.substring(retorno.search("n") + 1)

							var params = "Certificado=" + document.forms["frmDetalle"].elements["Certificado"].value + "&Instancia=" + document.forms["frmDetalle"].elements["Instancia"].value + "&Version=" + retorno;

							xmlHttp.open("POST", "../_common/EliminarVersionProveedor.aspx", false);
							xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
							xmlHttp.send(params);

							var params = "Certificado=" + document.forms["frmDetalle"].elements["Certificado"].value + "&Instancia=" + document.forms["frmDetalle"].elements["Instancia"].value + "&Version=" + document.forms["frmDetalle"].elements["Version"].value;

							xmlHttp.open("POST", "../_common/controlarVersionQA.aspx", false);
							xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
							xmlHttp.send(params);

							resultadoProcesarAccionesQA( Quien)   
								
							return
						}
						else {
							HabilitarBotones()
							wProgreso = null;
						}

						return
				}
				else
					if (retorno == 2) {//Si no está en proceso comprobamos el certificado sigue siendo el 
					//activo para ese proveedor, ya que se ha podido renovar mientras lo teníamos editado. 
					//Si no es el activo mostraremos el alert.
						alert(arrTextosML[4])
						HabilitarBotones()
						wProgreso = null;
						return
					}
					else                    
						if (retorno == 3) {//Si no es misma versión en función del tipo mostraremos un  
						//alert y el usuario no podrá guardar los cambios. Tipo Usu Qa
							alert(arrTextosML[7])
							HabilitarBotones()
							wProgreso = null;
							return
						}
						else
							if (retorno == 4) {//Si no es misma versión en función del tipo mostraremos un  
							//alert y el usuario no podrá guardar los cambios. Tipo Proveedor
								alert(arrTextosML[8])
								HabilitarBotones()
								wProgreso = null;
								return
							}
			}

			if (Quien == 1) {
				MontarFormularioSubmit()
				document.forms["frmSubmit"].elements["GEN_Enviar"].value = 0
				document.forms["frmSubmit"].elements["GEN_Accion"].value = "guardarcertificado"
				oFrm = MontarFormularioCalculados()
				sInner = oFrm.innerHTML
				oFrm.innerHTML = ""
				document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner)
				document.forms["frmSubmit"].submit()

				return
			}
			else
			    if (Quien == 2) {
			        if (document.getElementById("chkPub")) {
			            document.getElementById("chkPub").style.visibility = 'visible';
			        }
					window.open("eliminarCertificado.aspx?Certificado=" + document.forms["frmDetalle"].elements["Certificado"].value + "&Instancia=" + document.forms["frmDetalle"].elements["Instancia"].value + "&Version=" + document.forms["frmDetalle"].elements["Version"].value, "iframeWSServer")                        
			}
		}

		var HuboGuardados = 0;

		/*
		''' <summary>
		''' Publicar un certificado
		''' </summary>
		''' <returns>Falso si no se puede publicar</returns>
		''' <remarks>Llamada desde: atributo onclick del check; Tiempo máximo:0</remarks>
		*/
		function Publicar() {
		    if (bEnProceso) setTimeout(Publicar(), 2000);		    
		    else {
		        if (document.getElementById("lnkBotonVolver").disabled) setTimeout(Publicar(), 2000);		        
		        else {		            
		            var iPub
		            if (document.forms["frmDetalle"].elements["chkPub"].checked == true) {                        
		                iPub = 1
		                //si va a publicar el certificado comprueba la fecha de despublicacíon:
		                var today = new Date()
		                dateChooser = fsGeneralEntry_getById("txtfsFecDesPub")
		                var today2 = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0)

		                if (today2 > dateChooser.getValue() && dateChooser.getValue() != null) {
		                    alert(arrTextosML[1])
		                    document.forms["frmDetalle"].elements["chkPub"].checked = false
		                    return false
		                }

		                //
		                if (document.getElementById("lnkBotonVolver")) document.getElementById("lnkBotonVolver").disabled = true;
		                
		                //para publicar no hace falta comprobar si está en proceso o no
		                window.open("publicarCertificado.aspx?HuboGuardados=" + HuboGuardados + "&Certificado=" + document.forms["frmDetalle"].elements["Certificado"].value + "&Pub=" + iPub + "&Refrescar=1", "iframeWSServer")
                        
                        //
		                HuboGuardados = 0;
		            }
		            else {
		                iPub = 0
		                document.forms["frmDetalle"].elements["chkPub"].checked = true //por si no deja despublicar y así sigue marcado
		                var params = "Certificado=" + document.forms["frmDetalle"].elements["Certificado"].value + "&Instancia=" + document.forms["frmDetalle"].elements["Instancia"].value + "&Version=" + document.forms["frmDetalle"].elements["Version"].value;

		                xmlHttp.open("POST", "../_common/controlarVersionQA.aspx", false);
		                xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
		                xmlHttp.send(params);

		                if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
		                    retorno = xmlHttp.responseText;
		                    if (retorno == 1) alert(arrTextosML[5]) //en proceso		                        
		                    else {
		                        if (retorno.search("Version") > -1) { //Si no es misma version, y la última versión es de tipo 3, quiere decir
		                            //que el proveedor tiene una versión guardada sin enviar.
		                            if (confirm(arrTextosML[9]) == true) {//El proveedor tiene datos guardados sin enviar, ¿Está seguro que desea
		                                //despublicar el certificado antes de que el proveedor le envíe los cambios?
		                                window.open("../certificados/publicarCertificado.aspx?HuboGuardados=" + HuboGuardados + "&Certificado=" + document.forms["frmDetalle"].elements["Certificado"].value + "&Pub=0&Refrescar=1", "iframeWSServer")
		                            }
		                        } else window.open("../certificados/publicarCertificado.aspx?HuboGuardados=" + HuboGuardados + "&Certificado=" + document.forms["frmDetalle"].elements["Certificado"].value + "&Pub=0&Refrescar=1", "iframeWSServer")
		                    }
		                }
		            }
		        }
		    }
			return false;						
		}

        //Lo q había antes de mostrar/ocultar el bt de Guardar en función de si has salvado datos o no, NO FUNCIONABA. Con esto funciona.
		function BotoneraGuardar(Mostrar) {
		    if (Mostrar == 1) FSNHeader_MostrarBoton('BotonGuardar');
		    else FSNHeader_OcultarBoton('BotonGuardar'); //Guardar

		}

		/*''' <summary>
		''' Dar un mensaje de q no puedes publicar pq esta en proceso o proceder a publicar
		''' </summary>
		''' <param name="Instancia">Instancia</param>
		''' <param name="clave">clave de la celda cliqueada</param>        
		''' <param name="enProceso">Instancia en Proceso o no</param>
		''' <param name="Certificado">Certificado</param>     
		''' <param name="fecAlta">fec Alta Certificado</param>
		''' <param name="Proveedor">Proveedor Certificado</param>     
		''' <param name="Tipo">Tipo Certificado</param>                                    
		''' <remarks>Llamada desde: comprobarEnProcesoServer.aspx; Tiempo máximo:0</remarks>*/
		function comprobadoEnProceso(Instancia, clave, enProceso, Certificado, fecAlta, Proveedor, Tipo) {
		    if (enProceso != 0) alert(arrTextosML[5]); //En PROCESO
		    else window.open("../certificados/publicarCertificado.aspx?Certificado=" + Certificado + "&Pub=0&Refrescar=1", "iframeWSServer");			
		}

		function PublicarCertificado(tipo) {
			bEnProceso = true;
				
			if (tipo == "FechaDespublicacion") {
				oFSEntry = fsGeneralEntry_getById("txtfsFecDesPub")
				if (oFSEntry) {
					if (oFSEntry.getValue()) {
						var DespubMes = oFSEntry.getValue().getMonth() + 1
						var DespubDia = oFSEntry.getValue().getDate()
						var DespubAnyo = oFSEntry.getValue().getFullYear()
					} else {
						var DespubMes = null
						var DespubDia = null
						var DespubAnyo = null
					}
					CreateXmlHttp();
					if (xmlHttp) {
						var params = "Certificado=" + document.forms["frmDetalle"].elements["Certificado"].value + "&FecDespubAnyo=" + DespubAnyo + "&FecDespubMes=" + DespubMes + "&FecDespubDia=" + DespubDia + "&Refrescar=1";
						xmlHttp.onreadystatechange = resultadoProcesarAcciones;
						xmlHttp.open("POST", "publicarCertificado.aspx", true);
						xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
						xmlHttp.send(params);
					}
				}
			}
			if (tipo == "FechaLimCumplimentacion") {
				oFSEntry = fsGeneralEntry_getById("txtfsFecLimCumpl")
				if (oFSEntry) {
					if (oFSEntry.getValue()) {
						var LimCumplMes = oFSEntry.getValue().getMonth() + 1
						var LimCumplDia = oFSEntry.getValue().getDate()
						var LimCumplAnyo = oFSEntry.getValue().getFullYear()
					} else {
						var LimCumplMes = null
						var LimCumplDia = null
						var LimCumplAnyo = null
					}
					CreateXmlHttp();
					if (xmlHttp) {
						var params = "Certificado=" + document.forms["frmDetalle"].elements["Certificado"].value + "&FecLimCumplAnyo=" + LimCumplAnyo + "&FecLimCumplMes=" + LimCumplMes + "&FecLimCumplDia=" + LimCumplDia + "&Refrescar=1";
						xmlHttp.onreadystatechange = resultadoProcesarAcciones;
						xmlHttp.open("POST", "publicarCertificado.aspx", true);
						xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
						xmlHttp.send(params);
					}
				}
			}				
			bEnProceso = false;
		}

        //Despues de cada webmethod Asincrono, hacer el codigo de dentro del if.
		function resultadoProcesarAcciones() {
			var retorno;
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                ////retorno = xmlHttp.responseText;
                ////if (retorno != '') {
                ////    alert("Se ha producido un error al " + retorno + " tareas.");
                ////    window.location.href("detalleCertificado.aspx");
                ////}

                //El window.location.href NO va bien. 
			    //Ocultar bt/cambiar el check SÍ. 

			    //Pero hay 4 posibles llamadas a publicarCertificado. 3 de ellas a traves de xmlHttp. 2 son asincronas y resulta q solo son modificacion en bbdd.
			}
		}
			
		function ddFecLimCumplimentacion_ValueChange(oEdit, oldValue, oEvent) {
		    if (bEnProceso) setTimeout(ddFecLimCumplimentacion_ValueChange(oEdit, oldValue, oEvent), 2000);
		    else PublicarCertificado('FechaLimCumplimentacion');			
			return false;
		}

		function ddFecDespublicacion_ValueChange(oEdit, oldValue, oEvent) {
		    if (bEnProceso) setTimeout(ddFecDespublicacion_ValueChange(oEdit, oldValue, oEvent), 2000);
		    else PublicarCertificado('FechaDespublicacion');			
			return false;
        }

		/*''' Revisado por: Jbg. Fecha: 26/10/2011
		''' <summary>
		''' Abrir una pantalla para enviar emails a una serie de proveedores
		''' </summary>
		''' <remarks>Llamada desde: FSNPageHeader.OnClientClickMail ; Tiempo máximo: 0</remarks>*/
		function EnviarMail() {
			if (document.getElementById("lnkBotonMail"))
			    if (document.getElementById("lnkBotonMail").disabled)
					return false

            var newWindow = window.open("../_common/MailProveedores.aspx?Instancia=" + document.forms["frmDetalle"].elements["Instancia"].value + "&Origen=2&Proveedores=" + document.forms["frmDetalle"].Email.value, "_blank", "width=710,height=360,status=yes,resizable=no,top=180,left=200");            
        }

		/*''' <summary>
		''' Tras un guardado no se hace ni submit ni se va a otra pantalla. Eso implica q haya datos q no estan 
		'''	actualizados en pantalla pero si en bbdd. Esta función habilita los botones y oculta la ventana de espera.
		'''	Por ultimo actualiza el combo de versiones.
		''' </summary>
		''' <param name="sCadena">Persona de nueva versión para el combo de versiones</param>
		''' <param name="version">nueva versión para el combo de versiones</param>	
		''' <param name="TiempoCert">Año, mes, dia, hora, minuto y segundos de la nueva version separados por #. Y diferencia horaria con el servidor.</param>				    	
		''' <remarks>Llamada desde: GuardarInstancia.aspx; Tiempo máximo:0,1</remarks>*/
		function HabilitarBotones(sCadena, version, TiempoCert) {
			if (document.getElementById("lnkBotonMail")) document.getElementById("lnkBotonMail").disabled = false;
			if (document.getElementById("lnkBotonImpExp")) document.getElementById("lnkBotonImpExp").disabled = false;
			if (document.getElementById("lnkBotonGuardar")) document.getElementById("lnkBotonGuardar").disabled = false;
			if (document.getElementById("lnkBotonEliminar")) document.getElementById("lnkBotonEliminar").disabled = false;
			if (document.getElementById("lnkBotonVolver")) document.getElementById("lnkBotonVolver").disabled = false; 
            if (document.getElementById("chkPub"))  document.getElementById("chkPub").style.visibility = 'visible';
					
			OcultarEspera()
			
			if ((sCadena != '') && (sCadena != undefined)) {
				addItem(sCadena, version, TiempoCert);
					
				if (document.getElementById("lblVersion")) document.getElementById("lblVersion").style.visibility = 'visible';

				var combo = $find('<%=wddVersion.ClientID %>');
				combo.set_visible(true);

				document.getElementById("Version").value = version

				HuboGuardados = 1;
			}
		}
		/*''' <summary>
		''' Tras un guardado no se hace ni submit ni se va a otra pantalla. Eso implica q haya datos q no estan 
		'''	actualizados en pantalla pero si en bbdd. Esta funciÃƒÂ³n habilita los botones y oculta la ventana de espera.
		'''	Por ultimo actualiza el combo de versiones.
		''' </summary>
		''' <param name="sCadena">Persona de nueva versiÃƒÂ³n para el combo de versiones</param>
		''' <param name="version">nueva versiÃƒÂ³npara el combo de versiones</param>	
		''' <param name="TiempoNoConf">AÃƒÂ±o, mes, dia, hora, minuto y segundos de la nueva version separados por #. Y diferencia horaria con el servidor.</param>				    	
		''' <remarks>Llamada desde: activarVentana; Tiempo mÃƒÆ’Ã‚Â¡ximo:0,1</remarks>*/
		function addItem(sCadena, version, TiempoCert) {
			var combo = $find('<%=wddVersion.ClientID %>');
			var item = combo.get_items().createItem();
			var sText;
			var Horario = TiempoCert.split("#");
			var FechaLocal;

			FechaLocal = new Date();
			otz = FechaLocal.getTimezoneOffset();
			diferencia = parseInt(Horario[6]) + otz;

			FechaLocal = new Date(Horario[0], Horario[1] - 1, Horario[2], Horario[3], Horario[4] - diferencia, Horario[5]);
			valorTempVersion = myDatetoStr(FechaLocal, vDatefmt, true);

			sText = valorTempVersion + ' ' + sCadena;

			item.set_text(sText);

			var sValue = document.forms["frmDetalle"].elements["Instancia"].value + "##" + version;

			item.set_value(sValue);

			bEstasAnnadiendo=1;

			combo.get_items().add(item)
			combo.set_activeItem(item, true)
		}

		/*''' <summary>
		''' Evento que salta tras aÃƒÂ±adir una version en el combo.
		''' </summary>
		''' <param name="sender">componente input</param>
		''' <param name="eventArgs">evento</param>    
		''' <remarks>Llamada desde=evento del dropdown; Tiempo maximo=0seg.</remarks>*/
		function wddVersion_ItemAdded(sender, eventArgs) {
			sender.selectItemByIndex(sender.get_items().getLength() - 1, true, true)
		}


		/*''' <summary>
		''' Evento que salta tras elegir una version en el combo. Carga los datos de la noConformidad en esa version
		''' </summary>
		''' <param name="sender">componente input</param>
		''' <param name="eventArgs">evento</param>    
		''' <remarks>Llamada desde=evento del dropdown; Tiempo maximo=0seg.</remarks>*/
		function wddVersion_selectedIndexChanged(sender, eventArgs) {
			var aValue = sender.get_selectedItems()[0].get_value().split("##");
			var iInstancia = aValue[0];
			var iVersion = aValue[1];

			var FechaLocal
			FechaLocal = new Date()
			otz = FechaLocal.getTimezoneOffset()

			window.open("detallecertificado.aspx?Instancia=" + iInstancia.toString() + "&Certificado=" + document.forms["frmDetalle"].elements["Certificado"].value + "&Version=" + iVersion.toString() + "&Otz=" + otz.toString(), "_self")
		}
		var bEstasAnnadiendo;
		/*''' <summary>
		''' Evento que salta al abrirse el combo. Tras aÃƒÂ±adir una version en el combo este se abre automaticamente, esta funciÃƒÂ³n lo cierra para 
		''' evitar q se quede abierto tras el guardado obligando al usuario a cerrarlo ÃƒÂ©l.
		''' </summary>
		''' <param name="sender">componente input</param>
		''' <param name="eventArgs">evento</param>    
		''' <remarks>Llamada desde=evento del dropdown; Tiempo maximo=0seg.</remarks>*/	        
		function wddVersion_DropDownOpening(sender, eventArgs) {
			if (bEstasAnnadiendo == 1) {
				bEstasAnnadiendo = 0;
				eventArgs.set_cancel(true);
			}
		}
		/*
		''' <summary>
		''' Esta funcion convierte un objeto javascript Date en una cadena con una fecha
		''' en el formato especificado en vdatefmt
		''' </summary>
		''' <param name="fec">Fecha a convertir</param>
		''' <param name="vdatefmt">formato del usuario</param>        
		''' <param name="bConHora">Si la fecha tiene tb las horas/param>        
		''' <returns>Devuelve cadena con la fecha en el formato del usuario</returns>
		''' <remarks>Llamada desde=cargarComboVersiones/ HabilitarBotones; Tiempo máximo=0</remarks>
		*/
		function myDatetoStr(fec, vdatefmt, bConHora) {
			var vDia
			var vMes
			var str

			if (fec == null) return ("")
			
			re = /dd/i
			vDia = "00".substr(0, 2 - fec.getDate().toString().length) + fec.getDate().toString()
			str = vdatefmt.replace(re, vDia)

			re = /mm/i

			vMes = "00".substr(0, 2 - (fec.getMonth() + 1).toString().length) + (fec.getMonth() + 1).toString()
			str = str.replace(re, vMes)

			re = /yyyy/i

			str = str.replace(re, fec.getFullYear())
			if (bConHora)
				str += (" " + "00".substr(0, 2 - (fec.getHours()).toString().length) + fec.getHours() + ":" + "00".substr(0, 2 - (fec.getMinutes()).toString().length) + fec.getMinutes())
			return (str)
		}
		/*
		''' <summary>
		''' Carga la combo de versiones y la fecha de alta con el horario y el formato de fecha del usuario
		''' </summary>		    
		''' <remarks>Llamada desde=onLoad del body; Tiempo máximo=0seg.</remarks>
		*/
		function cargarComboVersiones() {
			var FechaLocal
			FechaLocal = new Date()
			otz = FechaLocal.getTimezoneOffset()

			var diferencia = parseFloat(window.document.getElementById("<%=diferenciaUTCServidor.ClientID%>").value) + otz

			//Actualizar la fecha de alta en la hora del usuario SIN!!! el formato
			fechaAlta = window.document.getElementById("<%=lblFecAlta.ClientID%>")

			if (fechaAlta) {
				valorTempFechaAlta = new Date(fechaAlta.innerHTML)
				FechaLocal = new Date(sAnyoAlta, sMesAlta - 1, sDiaAlta, sHorasAlta, sMinutosAlta - diferencia, sSegundosAlta)
				//vDatefmt se crea en el vb page_loal
				valorTempFechaAlta = myDatetoStr(FechaLocal, vDatefmt, true)
				fechaAlta.innerHTML = valorTempFechaAlta
			}

		} //fin del function cargarComboVersiones
        
	    //Estaba habilitando los botones antes de ocultar el progreso. Parecia q te dejaba dar a varios botones mientras estaba en progreso.
	    function DarTiempoAOcultarProgreso() {
	        $("[id*='lnkBoton']").removeAttr('disabled');
	    }

   
		/*Descripcion: Oculta los divs que indican que se esta realizando un proceso y Muestra los grupos.
		Llamada desde:=HabilitarBotones // ErrorValidacion()
		Tiempo ejecucion:0,1seg.*/	
		function OcultarEspera() {
			wProgreso = null;

			document.getElementById("divProgreso").style.display = 'none';
			document.getElementById("divForm2").style.display = 'inline';
			
			setTimeout('DarTiempoAOcultarProgreso()', 250);

			i = 0;
			bSalir = false;
			while (bSalir == false) {
				if (document.getElementById("uwtGrupos_div" + i)) {
					document.getElementById("uwtGrupos_div" + i).style.visibility = 'visible';
					i = i + 1;
				}
				else {
					bSalir = true;
				}
			}

		}
		/*
		Descripcion: Muestra el div de espera y oculta los tabs de grupos
		Llamada desde= Guardar()
		Tiempo ejecucion:=0,1seg. 
		*/
		function MostrarEspera() {
		    wProgreso = true;	
            
		    $("[id*='lnkBoton']").attr('disabled', 'disabled');

			document.getElementById("divForm2").style.display = 'none';

			i = 0;
			bSalir = false;
			while (bSalir == false) {
				if (document.getElementById("uwtGrupos_div" + i)) {
					document.getElementById("uwtGrupos_div" + i).style.visibility = 'hidden';
					i = i + 1;
				}
				else {
					bSalir = true;
				}
			}
			document.getElementById("lblProgreso").value = document.getElementById("cadenaespera").value;
			document.getElementById("divProgreso").style.display = 'inline';
        }
        /*
        ''' <summary>
        ''' Calcular campos desglose Calculados 
        ''' </summary>        
        ''' <remarks>Llamada desde: Boton Calcular ; Tiempo máximo: 0,2</remarks>
        */
        function CalcularCamposCalculados() {
            oFrm = MontarFormularioCalculados()
            oFrm.submit()
            return false;
        }
--></script>
	<script language="javascript" id="clientEventHandlersJS">
		<!--
		/*Descripcion:=Llama a la pagina para exportacion de datos
		Llamada desde:Option de menu "Impr./Exp."
		Tiempo ejecucion:0seg.*/   
		function cmdImpExp_onclick() {
			if (document.getElementById("lnkBotonImpExp"))
				if (document.getElementById("lnkBotonImpExp").disabled)
					return false
			
			window.open('../seguimiento/impexp_sel.aspx?Instancia='+document.getElementById("Instancia").value+'&Certificado='+document.forms["frmDetalle"].elements["Certificado"].value+'&Version='+document.forms["frmDetalle"].elements["Version"].value + '&TipoImpExp=2','_new','fullscreen=no,height=115,width=315,location=no,menubar=no,resizable=no,scrollbars=no,status=yes,titlebar=yes,toolbar=no,left=200,top=200');
		}

		/*''' <summary>
		''' Redimensionar los desgloses de la pantalla.
		''' </summary>   
		''' <remarks>Llamada desde: onResize; Tiempo máximo: 0</remarks>*/
		function resize() {
			if (arrDesgloses)
				for (i=0;i<arrDesgloses.length;i++) {					
					sDiv = arrDesgloses[i].replace("tblDesglose","divDesglose")
					if (document.getElementById(sDiv))
						document.getElementById(sDiv).style.width=parseFloat(document.body.offsetWidth) - 70 + 'px';
				}
		}		
		/*''' <summary>
		''' Evalua un codigo html
		''' </summary>   
		''' <remarks>Llamada desde: moneasserver.aspx; Tiempo máximo: 0</remarks>*/
		function localEval(s) {
		    eval(s);
		}		
		function init() {
			resize();
			cargarComboVersiones();
		}
//-->
		/*''' <summary>
		''' Vuelve a la pagina anterior
		''' </summary>
		''' <remarks>Llamada desde: Cuando se pincha al enlace ("Volver"); Tiempo maximo:0,1</remarks>*/	
		function HistoryHaciaAtras () {         
			if (document.getElementById("lnkBotonVolver"))
				if (document.getElementById("lnkBotonVolver").disabled)
					return false	

			if ("<%=Session("VolverdetalleCertificado")%>" == "")			
				window.history.go(-1)			
			else
				window.open("<%=Session("VolverdetalleCertificado")%>","_top")	
		}
		function initTab(webTab) {
		   var cp = document.getElementById(webTab.ID + '_cp');
		   cp.style.minHeight = '300px';
		}
	</script>
	<body onresize="resize()" onload="init()" onunload="if (wProgreso != null) wProgreso.close();">
		<form id="frmDetalle" method="post" runat="server">
			<asp:ScriptManager ID="ScriptManager1" runat="server">
				<CompositeScript>
                    <Scripts>
                        <asp:ScriptReference Name="ExtenderBase.BaseScripts.js" Assembly="AjaxControlToolkit" />
                        <asp:ScriptReference Name="Common.Common.js" Assembly="AjaxControlToolkit" />
                        <asp:ScriptReference Name="Compat.Timer.Timer.js" Assembly="AjaxControlToolkit" />
                        <asp:ScriptReference Name="Animation.Animations.js" Assembly="AjaxControlToolkit" />
                        <asp:ScriptReference Name="Animation.AnimationBehavior.js" Assembly="AjaxControlToolkit" />
                        <asp:ScriptReference Name="PopupExtender.PopupBehavior.js" Assembly="AjaxControlToolkit" />
                        <asp:ScriptReference Name="AutoComplete.AutoCompleteBehavior.js" Assembly="AjaxControlToolkit" />
                        <asp:ScriptReference Path="../alta/js/AdjacentHTML.js" />
                        <asp:ScriptReference Path="../alta/js/jsAlta.js" />
                    </Scripts>
                </CompositeScript>
			</asp:ScriptManager>           	
            
			<iframe id="iframeWSServer" style="Z-INDEX: 103; LEFT: 8px; VISIBILITY: hidden; POSITION: absolute; TOP: 200px"
				name="iframeWSServer" src="../blank.htm"></iframe>
			<uc1:menu id="Menu1" runat="server" OpcionMenu="Calidad" OpcionSubMenu="Certificados"></uc1:menu>
			<table id="Table1" style="height:15%; width:100%; padding-bottom:15px;" cellspacing="0" cellpadding="1" border="0">
				<tr>
					<td colspan="7">
						<fsn:FSNPageHeader runat="server" ID="FSNPageHeader" UrlImagenCabecera="./images/certificados.gif">
						</fsn:FSNPageHeader>
					</td>				    
				</tr>
				<tr>
					<td colspan="5" style="padding-left:5px; padding-bottom:5px;">
						<asp:label id="lblAviso" runat="server" CssClass="fntRequired" Width="100%"></asp:label>
					</td>
				</tr>
                <tr>
					<td colspan="5" style="padding-left:5px; padding-bottom:5px;">
                        <asp:Image runat="server" ID="imgProveedorBaja" style="vertical-align:middle;" ImageUrl="~/App_Pages/PMWEB/images/alert-rojo-mini.png" />
						<asp:label id="lblAvisoProveedorBaja" runat="server" CssClass="fntRequired"></asp:label>
					</td>
				</tr>
                <tr>
					<td colspan="5" style="padding-left:5px; padding-bottom:5px;">
                        <asp:Image runat="server" ID="imgProveedorNoContacto" style="vertical-align:middle;" ImageUrl="~/App_Pages/PMWEB/images/alert-rojo-mini.png" />
						<asp:label id="lblAvisoProveedorNoContacto" runat="server" CssClass="fntRequired"></asp:label>
					</td>
				</tr>
				<tr>
					<td colspan="7" style="padding-top:5px; padding-bottom:5px;" class="fondoCabecera">
						<table  id="tblCabecera" style="width:100%;table-layout:fixed;" border="0">
							<tr>
								<td style="width:10%">
									<asp:label id="lblLitProveedor" runat="server" CssClass="captionBlue" Width="100%">DProveedor</asp:label>
								</td>
								<td style="width:275px;">
									<asp:label id="lblProveedor" runat="server" CssClass="label" style="width:100%; text-overflow: ellipsis; overflow: hidden; white-space: nowrap;">Label</asp:label>
								</td>				
								<td style="width:23%">
									<asp:label id="lblFecLimCumpl" runat="server" CssClass="captionBlue" Width="100%">dFecha límite cumplimentación:</asp:label>
								</td>        
								<td style="width:12%;">
									<asp:label id="lblFechaLimCumpl" runat="server" CssClass="label">Label</asp:label>
									<fsde:generalentry id="txtfsFecLimCumpl" runat="server" Width="113px" DropDownGridID="txtfsFecLimCumpl"
										Tipo="TipoFecha" Tag="txtfsFecLimCumpl" Independiente="1">
										<InputStyle CssClass="TipoFecha"></InputStyle>
									</fsde:generalentry>
								</td>
								<td id="ComboL" runat="server" style="width:6%;">
									<asp:label id="lblVersion" runat="server" CssClass="captionBlue">DVersión:</asp:label>
								</td>
								<td id="ComboV" runat="server">
									<ig:WebDropDown ID ="wddVersion" runat="server" EnableViewState="true" Height="22px" Width="250px" 
										EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true" 
										EnableClosingDropDownOnBlur="true" CurrentValue="">
										<ClientEvents SelectionChanged="wddVersion_selectedIndexChanged" ItemAdded="wddVersion_ItemAdded" DropDownOpening="wddVersion_DropDownOpening"/>
									</ig:WebDropDown>
								</td>
							</tr>
							<tr>							    
								<td>
									<asp:label id="lblLitFecAlta" runat="server" CssClass="captionBlue" Width="100%">DFecha de alta</asp:label>
								</td>
								<td>
									<asp:label id="lblFecAlta" runat="server" CssClass="label" Width="100%">Label</asp:label>
								</td>                                
								<td style="width:25%">
									<asp:label id="lblFecDespub" runat="server" CssClass="captionBlue" Width="100%">dFecha de despublicación:</asp:label>						                        
								</td>
								<td>
									<asp:label id="lblFechaDespub" runat="server" CssClass="label">Label</asp:label>
									<fsde:generalentry id="txtfsFecDesPub" runat="server" Width="113px" DropDownGridID="txtfsFecDesPub"
										Tipo="TipoFecha" Tag="txtfsFecDesPub" Independiente="1">
										<InputStyle CssClass="TipoFecha"></InputStyle>
									</fsde:generalentry>
								</td>	
								<td style="text-align:left;" colspan="2">
									<asp:label id="lblPub" runat="server" CssClass="captionBlue">Label</asp:label>
									<asp:label id="lblPublicado" runat="server" CssClass="label">Label</asp:label>
									<asp:Label ID="lblCheckBox" runat="server" CssClass="captionBlue"></asp:Label>
									<asp:checkbox id="chkPub" runat="server" CssClass="captionBlue"></asp:checkbox>
								</td>							
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<div id="divProgreso" style="DISPLAY: none">
				<table id="tblProgreso" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
					<tr style="HEIGHT: 50px">
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center" width="100%"><asp:textbox id="lblProgreso" style="TEXT-ALIGN: center" runat="server" CssClass="captionBlue"
								Width="100%" BorderWidth="0" BorderStyle="None">Su solicitud está siendo tramitada. Espere unos instantes...</asp:textbox></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center" width="100%"><asp:image id="imgProgreso" runat="server" src="../_common/images/cursor-espera_grande.gif"></asp:image></td>
					</tr>
				</table>
			</div>
			<div id="divForm2" style="display: inline;">
				<table cellspacing="0" cellpadding="0" border="0" style="height:85%; width:100%;">
					<tr style="padding-top:10px;">
						<td valign="middle">
							<igtab:UltraWebTab ID="uwtGrupos" runat="server" Width="100%" BorderWidth="1px" BorderStyle="Solid"
								DisplayMode="Scrollable" CustomRules="padding:10px;" FixedLayout="True"
								DummyTargetUrl=" " ThreeDEffect="False" EnableViewState="false">
								<DefaultTabStyle Height="24px" CssClass="uwtDefaultTab">
									<Padding Left="20px" Right="20px"></Padding>
								</DefaultTabStyle>
								<RoundedImage NormalImage="ig_tab_blueb2.gif" HoverImage="ig_tab_blueb1.gif" FillStyle="LeftMergedWithCenter">
								</RoundedImage>
								<ClientSideEvents InitializeTabs="initTab" />
							</igtab:UltraWebTab>
						</td>
					</tr>
				</table>
				<input id="Instancia" type="hidden" size="16" name="Instancia" runat="server"/> 
                <input id="FuerzaDesgloseLectura" type="hidden" size="16" name="FuerzaDesgloseLectura" runat="server"/> 
                <input id="Certificado" type="hidden" size="16" name="Certificado" runat="server"/> 
                <input id="txtEnviar" type="hidden" name="Enviar" runat="server"/> 
                <input id="Version"  type="hidden" name="Version" runat="server"/>
                <input id="VersionInicialCargada" type="hidden" name="VersionInicialCargada" runat="server"/>
                <input id="Proveedor" type="hidden" name="Proveedor" runat="server"/>
                <input id="Email" type="hidden" name="Email" runat="server"/>
                <input id="FecAlta" type="hidden" name="FecAlta" runat="server"/>
                <input id="Tipo" type="hidden" name="Tipo" runat="server"/>
                <input id="DenProveedor" type="hidden" name="DenProveedor" runat="server"/>
				<div id="divCalculados" style="z-index: 109; visibility: hidden; position: absolute; top: 0px"></div>
				<div id="divAlta" style="visibility: hidden"></div>
				<div id="divDropDowns" style="z-index: 101; visibility: hidden; position: absolute; top: 300px"></div>
				<input id="cadenaespera" type="hidden" name="cadenaespera" RUNAT="SERVER"/>
				<input id="indiceCombo" type="hidden" name="indiceCombo" runat="server" />
				<input type="hidden" runat="server" id="diferenciaUTCServidor" name="diferenciaUTCServidor" />
            </div>
		</form>
        <div id="divForm3" style="DISPLAY: none">
		    <form id="frmCalculados" name="frmCalculados" action="../alta/recalcularimportes.aspx" method="post" target="fraWSServer"></form>
		    <form id="frmDesglose" name="frmDesglose" method="post" target="winDesglose"></form>
        </div>		
	</body>
</html>
