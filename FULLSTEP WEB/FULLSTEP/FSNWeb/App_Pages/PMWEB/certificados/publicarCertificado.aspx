<%@ Page Language="vb" AutoEventWireup="false" Codebehind="publicarCertificado.aspx.vb" Inherits="Fullstep.FSNWeb.publicarCertificado"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<script>
		function comprobarRespuestas()
		{
		if (confirm(arrTextosML[0])==true)
			{
			var theform;
			if (window.navigator.appName.toLowerCase().indexOf("microsoft") > -1) {
				theform = document.Form1;
			}
			else {
				theform = document.forms["Form1"];
			}
			theform.submit();
			
			}
		}
		
		function avisoNoSePuedeDespublicar(mensaje)
		{
			alert(mensaje);
		}

		/*''' <summary>
		''' Recargar el detalle tras la publicaci�n � despublicaci�n. Realmente la recarga depende de
		''' si hubo guardados pq si cambias algo, guardas y publicas puede recargarte con datos viejos pq no
		''' le ha dado tiempo a la bbdd. Por eso si hubo guardados es 1 no es recargar sino ocultar/mostrar 
		''' el bt de guardar. Este es para recargar.
		''' </summary>
		''' <param name="IdCertif">Id del Certificado</param>
		''' <remarks>Llamada desde:Page_load; Tiempo m�ximo:0</remarks>*/
		function Refrescar(IdCertif) {
			var FechaLocal
			FechaLocal = new Date()
			otz = FechaLocal.getTimezoneOffset()

			window.parent.location.href = "detallecertificado.aspx?Certificado=" + IdCertif + "&Otz=" + otz.toString()
		}

		/*''' <summary>
		''' Recargar el detalle tras la publicaci�n � despublicaci�n. Realmente la recarga depende de
		''' si hubo guardados pq si cambias algo, guardas y publicas puede recargarte con datos viejos pq no
		''' le ha dado tiempo a la bbdd. Por eso si hubo guardados es 1 no es recargar sino ocultar/mostrar 
		''' el bt de guardar. Este oculta/muestra el bt.
		''' </summary>   
		''' <param name="Pub">1 publicaci�n � 0 despublicaci�n</param> 
		''' <remarks>Llamada desde:Page_load; Tiempo m�ximo:0</remarks>*/
		function RefrescarBts(Pub) {
			if (Pub == 1) {	    
			    window.parent.BotoneraGuardar(0);
			} else {
                window.parent.BotoneraGuardar(1);
				window.parent.document.forms["frmDetalle"].elements["chkPub"].checked = false;
            }

            if (window.parent.document.getElementById("lnkBotonVolver")) window.parent.document.getElementById("lnkBotonVolver").disabled = false;            
		}		
	</script>	
	<body>
		<form id="Form1" method="post" runat="server">
		</form>
	</body>
</html>
