<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="imposibleaccion.aspx.vb" Inherits="Fullstep.FSNWeb.imposibleaccion" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
</head>
<body>
    <p>
        <form id="Form1" method="post" runat="server">
            <asp:ScriptManager ID="sm" runat="server">
            </asp:ScriptManager>
            <table id="Table1" style="z-index: 102; left: 8px; position: absolute; top: 150px" height="50%"
                cellspacing="1" cellpadding="1" width="100%" border="0">
                <tr height="20%">
                    <td width="80%" colspan="2"><asp:Label ID="lblTitulo" runat="server" Width="100%" CssClass="TituloDarkBlue"></asp:Label></td>
                    <td width="20%"><asp:Label ID="lblFecha" runat="server" Width="100%" CssClass="fntLogin"></asp:Label></td>
                    <td></td>
                </tr>
                <tr height="40%">
                    <td colspan="3">
                        <table id="tblDatos" cellspacing="1" cellpadding="1" width="80%" border="0">
                            <tr>
                                <td><asp:Label ID="lblDescr" runat="server" CssClass="fntLogin"></asp:Label></td>
                                <td><asp:Label ID="lblDescrBD" runat="server" CssClass="fntLogin"></asp:Label></td>
                                <td><asp:Label ID="lblImporte" runat="server" CssClass="fntLogin"></asp:Label></td>
                            </tr>
                            <tr>
                                <td><asp:Label ID="lblId" runat="server" CssClass="fntLogin"></asp:Label></td>
                                <td><asp:Label ID="lblIdBD" runat="server" CssClass="caption"></asp:Label></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><asp:Label ID="lblPet" runat="server" CssClass="fntLogin"></asp:Label></td>
                                <td><asp:Label ID="lblPetBD" runat="server" CssClass="fntLogin"></asp:Label></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <hr width="100%" color="gainsboro" noshade size="4">
                                    &nbsp;
                                </td>
                                <td colspan="2">
                                    <hr width="100%" color="gainsboro" noshade size="4">
                                    &nbsp;
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr height="60%">
                    <td valign="middle" colspan="3">
                        <asp:Label ID="lblMensaje" runat="server" Width="100%" CssClass="SinDatos"></asp:Label><asp:Label ID="lblMensajeMapper" runat="server" Width="100%" CssClass="SinDatos"></asp:Label>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            <uc1:menu ID="Menu1" runat="server"></uc1:menu>
        </form>
    </p>
</body>
</html>
