Public Class flowDiagramLaunch
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Session("DpiX") = "" Then

            Response.Redirect("detectscreen.aspx?Instancia=" & Request("Instancia") & "&Codigo=" & Request("Codigo") & "&NumFactura=" & Request("NumFactura"))

        Else

            CDiagWebBloque.tppX = 1440 / Session("DpiX")
            CDiagWebBloque.tppY = 1440 / Session("DpiY")
            CDiagWebEnlace.tppX = CDiagWebBloque.tppX
            CDiagWebEnlace.tppY = CDiagWebBloque.tppY

            Response.Redirect("flowDiagram.aspx?Instancia=" & Request("Instancia") & "&Codigo=" & Request("Codigo") & "&NumFactura=" & Request("NumFactura"))

        End If

    End Sub

End Class
