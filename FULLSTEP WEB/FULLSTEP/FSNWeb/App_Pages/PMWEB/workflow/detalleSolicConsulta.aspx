<%@ Register TagPrefix="ignav" Namespace="Infragistics.WebUI.UltraWebNavigator" Assembly="Infragistics.WebUI.UltraWebNavigator.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Register TagPrefix="igtab" Namespace="Infragistics.WebUI.UltraWebTab" Assembly="Infragistics.WebUI.UltraWebTab.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="detalleSolicConsulta.aspx.vb" Inherits="Fullstep.FSNWeb.detalleSolicConsulta" Trace="false" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
	<title></title>
	<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
	<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
	<meta content="JavaScript" name="vs_defaultClientScript" />
	<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
</head>
<script type="text/javascript" language="javascript" id="clientEventHandlersJS">
	var xmlHttp;

	//Funci�n que crea el objeto Ajax
	function CreateXmlHttp() {
		// Probamos con IE
		try {
			// Funcionar� para JavaScript 5.0
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e) {
			try {
				xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (oc) {
				xmlHttp = null;
			}
		}

		// Si no se trataba de un IE, probamos con esto
		if (!xmlHttp && typeof XMLHttpRequest != "undefined") {
			xmlHttp = new XMLHttpRequest();
		}

		return xmlHttp;
	}


	//Guarda el comentario de la aprobaci�n/rechazo
	function GuardarComentario() {
		if (document.getElementById("txtComentario") == '[object]' && document.getElementById("hid_ComentarioGuardado").value != "1") {
			CreateXmlHttp();
			if (xmlHttp) {
				var idInstancia = document.getElementById("Instancia")
				var idBloque = document.getElementById("hid_bloque")
				var txtComentario = document.getElementById("txtComentario")

				var params = "accion=11&instancia=" + idInstancia.value.toString() + "&coment=" + encodeURI(txtComentario.value.toString())

				xmlHttp.open("POST", rutaPM + "GestionaAjax.aspx", false);
				xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
				xmlHttp.send(params);
			}
		}
	}

	function ConformarBorde() {
		var children = $(document.getElementById('uwtGrupos__ctl1__tbl1')).children();

		for (var i = 0; i < children.length; i++) {
			if (children[i] instanceof HTMLTableElement) {
				children[i].setAttribute("border", "1");
				children[i].setAttribute("class", "bordeDesglose");
				break;
			}
		}
	}

	//Esta funcion marca que al aprobar o rechazar el comentario ya se ha guardado y no es
	//necesario guardarlo en el Unload de la pagina
	function Comentario_Guardado() {
		document.getElementById("hid_ComentarioGuardado").value = "1";
	}

	//funcion que Aprobara o rechazara la instancia
	function AprobaroRechazarInstancia(sAccion) {
		CreateXmlHttp();
		if (xmlHttp) {
			var id_accion
			if (sAccion == "A")
				//Si se aprueba 
				id_accion = document.getElementById("hid_aprobar")
			else
				//Si se rechaza
				id_accion = document.getElementById("hid_rechazar")

			var idInstancia = document.getElementById("Instancia")
			var idBloque = document.getElementById("hid_bloque")
			var txtComentario = document.getElementById("txtComentario")

			var params = "accion=10&idaccion=" + id_accion.value.toString() + "&instancia=" + idInstancia.value.toString() + "&bloque=" + idBloque.value.toString() + "&coment=" + encodeURI(txtComentario.value.toString())

			xmlHttp.open("POST", rutaPM + "GestionaAjax.aspx", false);
			xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
			xmlHttp.send(params);

			//cierra la ventana y oculta la fila de la instancia que se aprueba o rechaza en el visor de solicitudes
			self.close();
			window.opener.Cargar_Solicitudes();
		}
	}
	//Llama al popup para poder imprimir/exportar la solicitud
	function cmdImpExp_onclick() {
		window.open('../seguimiento/impexp_sel.aspx?Instancia=' + document.getElementById("Instancia").value + '&TipoImpExp=1' + '&Observadores=' + document.getElementById("hid_Observador").value, '_new', 'fullscreen=no,height=115,width=315,location=no,menubar=no,resizable=no,scrollbars=no,status=yes,titlebar=yes,toolbar=no,left=200,top=200');
	}

	function AbrirPopUpTexto(titulo, textSourceId, bLargeSize) {
		var texto;
		var hfTextSource = document.getElementById(textSourceId);

		if (hfTextSource != null) texto = hfTextSource.value;

		if (titulo == null) titulo = '';
		if (texto == null) texto = '';

		var MySize, MyArgs;
		if (bLargeSize) {
			MySize = "height=650,width=770";
			MyArgs = "titulo="+titulo+"&texto="+texto+"&height=600&=770";
		} else {
			MySize = "height=220,width=500";
			MyArgs = "titulo="+titulo+"&texto="+texto+"&height=170&=485";
		}
		window.open("../_common/popUpTexto.aspx?" + MyArgs,"_blank",MySize);
	}

	function AbrirEditor(titulo, campoId) {
		window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>_common/Editor.aspx?ID=" + campoId + "&readOnly=1&titulo=" + escape(titulo), "Editor", "width=910,height=600,status=yes,resizable=yes,top=20,left=100,scrollbars=yes")
	}

	function AbrirEnlace(sUrl) {
		window.open(sUrl, "Enlace", "width=910,height=600,status=yes,resizable=yes,top=20,left=100,scrollbars=yes")
	}

	function AbrirDetallePartida(titulo, pres5, texto) {
		var newWindow = window.open("../_common/detallePartida.aspx?titulo=" + titulo + "&pres5=" + pres5 + "&texto=" + escape(texto), "_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");
		newWindow.focus();
	}

	function AbrirDetalleActivo(valor) {
		var newWindow = window.open('<%=ConfigurationManager.AppSettings("rutaSM")%>Activos/detalleactivo.aspx?codActivo=' + escape(valor), '_blank', 'width=450,height=220,status=yes,resizable=no,top=200,left=250');
		newWindow.focus();
	}

	function AbrirDetalleTipoPedido(texto) {
		var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>_common/DetalleTipoPedido.aspx?cod=" + texto, "_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");
		newWindow.focus();
	}

	function AbrirFicherosAdjuntos(sValor, idCampo, sInstancia, sTipo) {
		var newWindow = window.open("atachedfiles.aspx?Valor=" + sValor + "&Valor2=&Campo=" + idCampo + "&tipo=" + sTipo + "&Instancia=" + sInstancia + "&readOnly=1", "_blank", "width=750,height=330,status=yes,resizable=no,top=200,left=200");
		newWindow.focus();
	}

	function IrAAdjunto(urlcompleta) {
		var NombreArchivo = urlcompleta.split("&nombre=")[1].split("&datasize=")[0]
		var NombreArchivoCodificado = encodeURIComponent(NombreArchivo)
		var URLCodificada = urlcompleta.replace(NombreArchivo, NombreArchivoCodificado)

		var newWindow = window.open(URLCodificada, "_blank", "width=600,height=275,status=no,resizable=yes,top=200,left=200");
		newWindow.focus();
	}

	//valida que la longitud del input no sea superior al l�mite establecido en el par�metro l
	function validarLength(e, l) {
		if (e.value.length > l)
			return false
		else
			return true
	}

	//Cuenta el n� de caracteres introducidos en un input
	function textCounter(e, l) {
		if (e.value.length > l)
			e.value = e.value.substring(0, l);
	}

</script>
<body onunload="GuardarComentario()" onload="ConformarBorde()">
	<form id="frmDetalle" method="post" runat="server">
		<asp:ScriptManager ID="sm1" runat="server">
			<CompositeScript>
				<Scripts>
					<asp:ScriptReference Path="../../../js/jquery/jquery.min.js" />
				</Scripts>
			</CompositeScript>
		</asp:ScriptManager>
		<input id="Instancia" type="hidden" name="Instancia" runat="server" />
		<input id="hid_bloque" type="hidden" runat="server" />
		<input id="hid_aprobar" type="hidden" runat="server" />
		<input id="hid_rechazar" type="hidden" runat="server" />
		<input id="hid_ComentarioGuardado" type="hidden" runat="server" />
		<input id="hid_Observador" type="hidden" runat="server" />
		<input type="hidden" id="AR" runat="server" value="" />

		<div style="padding: 0.5em; font-size: 0px;">
			<div style="line-height: 2em; display: inline-block; width: 70%;">
				<img alt="" src="images/SolicitudesPMSmall.jpg" style="vertical-align: middle;" />
				<asp:Label ID="lblSolicit" runat="server" CssClass="Titulo" Style="margin-left: 1em; display: inline; vertical-align: middle;"></asp:Label>
				<asp:Label ID="lblIdBD" runat="server" CssClass="Titulo" Style="margin-left: 0.2em; display: inline; vertical-align: middle;"></asp:Label>
			</div>
			<div style="line-height: 2em; display: inline-block; width: 30%;">
				<asp:Label ID="lblImporte" runat="server" CssClass="Titulo" Style="vertical-align: middle;"></asp:Label>
			</div>
		</div>
		<div class="fondoCabecera" style="padding: 0.5em;">
			<div>
				<asp:Label ID="lblTipo" runat="server" CssClass="captionBlue" Style="display: inline-block; width: 8em; margin-left: 1em; vertical-align: middle;"></asp:Label>
				<asp:Label ID="lblTipoBD" runat="server" CssClass="captionDarkBlue" Style="display: inline-block; width: 20em; margin-left: 1em; white-space: normal; vertical-align: middle;"></asp:Label>
				<asp:Label ID="lblEstado" runat="server" CssClass="captionBlue" Style="display: inline-block; width: 11em; margin-left: 1em; white-space: normal; vertical-align: middle;"></asp:Label>
				<asp:Label ID="lblEstadoBD" runat="server" CssClass="captionDarkBlue" Style="display: inline-block; width: 10em; margin-left: 1em; white-space: normal; vertical-align: middle;"></asp:Label>
				<input class="botonPMWEB" id="cmdAprobar" style="font-weight: bold; width: 100px" onclick="Comentario_Guardado(); AprobaroRechazarInstancia('A');" type="button"
					name="cmdAprobar" runat="server" />
				<input class="botonPMWEB" id="cmdRechazar" style="font-weight: bold; width: 100px" onclick="Comentario_Guardado(); AprobaroRechazarInstancia('R');" type="button"
					name="cmdRechazar" runat="server" />
				<input class="botonPMWEB" id="cmdImpExp" style="width: 100px" onclick="return cmdImpExp_onclick()"
					type="button" name="cmdImpExp" runat="server" />
			</div>
			<div>
				<asp:Label ID="lblPeticionario" runat="server" CssClass="captionBlue" Style="display: inline-block; width: 8em; margin-left: 1em; vertical-align: middle;"></asp:Label>
				<asp:Label ID="lblPeticionarioBD" runat="server" CssClass="captionDarkBlue" Style="display: inline-block; width: 20em; margin-left: 1em; white-space: normal; vertical-align: middle;"></asp:Label>
				<asp:Label ID="lblFecha" runat="server" CssClass="captionBlue" Style="display: inline-block; width: 11em; margin-left: 1em; white-space: normal; vertical-align: middle;"></asp:Label>
				<asp:Label ID="lblFechaBD" runat="server" CssClass="captionDarkBlue" Style="display: inline-block; width: 10em; margin-left: 1em; white-space: normal; vertical-align: middle;"></asp:Label>
			</div>
		</div>

		<asp:PlaceHolder ID="phComentario" Visible="False" runat="server">
			<table id="coment" cellspacing="5" cellpadding="1" width="100%" border="0" height="15%">
				<tr height="25%">
					<td valign="middle" colspan="8" height="100%">
						<textarea id="txtComentario" style="width: 100%; height: 100px" name="txtComent" runat="server" onkeydown="textCounter(this,500)" onkeyup="textCounter(this,500)" onchange="return validarLength(this,500)"></textarea></td>
				</tr>
			</table>
		</asp:PlaceHolder>

		<div id="divForm2">
			<igtab:UltraWebTab ID="uwtGrupos" runat="server" Width="100%" BorderWidth="1px" BorderStyle="Solid"
				CustomRules="padding:10px;" FixedLayout="False" DummyTargetUrl=" " ThreeDEffect="False" DisplayMode="Scrollable" Height="100%">
				<DefaultTabStyle Height="24px" CssClass="uwtDefaultTab">
					<Padding Left="20px" Right="20px"></Padding>
				</DefaultTabStyle>
				<RoundedImage NormalImage="ig_tab_blueb2.gif" HoverImage="ig_tab_blueb1.gif" FillStyle="LeftMergedWithCenter"></RoundedImage>
				<Tabs></Tabs>
			</igtab:UltraWebTab>
		</div>
		<div style="text-align: center; margin: 1em;">
			<input class="botonPMWEB" id="cmdCerrar" onclick="window.close()" type="button" name="cmdCerrar" runat="server" />
		</div>
	</form>
</body>
</html>
