Public Class aprobacionPMWEB
    Inherits FSNPage

    Protected WithEvents uwtGrupos As Infragistics.WebUI.UltraWebTab.UltraWebTab
    Protected WithEvents lblMensaje1 As System.Web.UI.WebControls.Label
    Protected WithEvents lblMensaje2 As System.Web.UI.WebControls.Label
    Protected WithEvents cmdSi As System.Web.UI.WebControls.Button
    Protected WithEvents cmdNo As System.Web.UI.WebControls.Button
    Protected WithEvents HyperDetalle As System.Web.UI.WebControls.HyperLink
    Protected WithEvents txtIdTipo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents hRolActual As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Instancia As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents FSNPageHeader As Global.Fullstep.FSNWebControls.FSNPageHeader

    Protected WithEvents lblLitImporte As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitCreadoPor As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitEstado As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitFecAlta As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitTipo As System.Web.UI.WebControls.Label

    Protected WithEvents lblIDInstanciayEstado As System.Web.UI.WebControls.Label
    Protected WithEvents lblImporte As System.Web.UI.WebControls.Label
    Protected WithEvents lblCreadoPor As System.Web.UI.WebControls.Label
    Protected WithEvents lblEstado As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecAlta As System.Web.UI.WebControls.Label
    Protected WithEvents lblTipo As System.Web.UI.WebControls.Label

    Protected WithEvents imgInfCreadoPor As Global.System.Web.UI.WebControls.Image
    Protected WithEvents imgInfTipo As Global.System.Web.UI.WebControls.Image

    Protected WithEvents FSNPanelDatosPeticionario As Global.Fullstep.FSNWebControls.FSNPanelInfo
    Protected WithEvents FSNPanelDatosProveedor As Global.Fullstep.FSNWebControls.FSNPanelInfo
    Protected WithEvents Menu1 As MenuControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private _oInstancia As FSNServer.Instancia
    Protected ReadOnly Property oInstancia() As FSNServer.Instancia
        Get
            If _oInstancia Is Nothing Then
                If Me.IsPostBack Then
                    _oInstancia = CType(Cache("oInstancia" & FSNUser.Cod), FSNServer.Instancia)
                Else
                    _oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                    _oInstancia.ID = Request("Instancia")

                    'Carga los datos de la instancia:
                    _oInstancia.Load(Idioma, True)
                    _oInstancia.CargarCamposInstancia(Idioma, FSNUser.CodPersona, , True)
                    _oInstancia.DevolverEtapaActual(Idioma, FSNUser.CodPersona) 'Se necesita la etapa y el rol actual

                    Me.InsertarEnCache("oInstancia" & FSNUser.Cod, _oInstancia)
                End If
            End If
            Return _oInstancia
        End Get
    End Property

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Const IncludeScriptFormat As String = ControlChars.CrLf &
  "<script type=""{0}"" src=""{1}""></script>"
    Dim tipoSolicitud As Integer

    ''' <summary>
    ''' Descripci�n: Carga el detalle de la solicitud y unos botones "Si" y "No" preguntando si quiere
    ''' gestionar o no esta solicitud.   
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Tiempo m�ximo: 1 sg  </remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Page.IsPostBack Then
            CargarCamposGrupo()
            Exit Sub
        End If

        Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab
        Dim iTipoGS As TiposDeDatos.TipoCampoGS
        Dim iTipo As TiposDeDatos.TipoGeneral
        Dim oEntry As DataEntry.GeneralEntry
        Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden

        tipoSolicitud = oInstancia.Solicitud.TipoSolicit
        If Not ClientScript.IsStartupScriptRegistered("tipoSolicitud") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "tipoSolicitud", "<script>var tipoSolicitud = '" & tipoSolicitud & "';</script>")
        End If
        Select Case tipoSolicitud
            Case TiposDeDatos.TipoDeSolicitud.Factura
                Menu1.OpcionMenu = "Facturacion"
                Menu1.OpcionSubMenu = "Seguimiento"
            Case TiposDeDatos.TipoDeSolicitud.Encuesta
                Menu1.OpcionMenu = "Calidad"
                Menu1.OpcionSubMenu = "Encuestas"
            Case TiposDeDatos.TipoDeSolicitud.SolicitudQA
                Menu1.OpcionMenu = "Calidad"
                Menu1.OpcionSubMenu = "SolicitudesQA"
            Case Else
                Menu1.OpcionMenu = "Procesos"
                Menu1.OpcionSubMenu = "Seguimiento"
        End Select
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Aprobacion
        ConfigurarCabeceraMenu()

        If oInstancia.Grupos Is Nothing Then
            'El Usuario no ha participado en la solicitud.            
            Select Case tipoSolicitud
                Case TiposDeDatos.TipoDeSolicitud.Factura, TiposDeDatos.TipoDeSolicitud.Encuesta, TiposDeDatos.TipoDeSolicitud.SolicitudQA
                    Response.Redirect("UsuarioNoParticipante.aspx?TipoSolicitud=" & tipoSolicitud)
                Case Else
                    Response.Redirect("UsuarioNoParticipante.aspx")
            End Select
        End If

        'Textos de idiomas:
        Me.lblLitTipo.Text = Textos(1)
        Me.lblLitEstado.Text = Textos(45) & ":"
        Me.lblLitCreadoPor.Text = Textos(53) & ":"
        Me.lblLitFecAlta.Text = Textos(54) & ":"
        Me.lblMensaje1.Text = Textos(5)
        Me.lblMensaje2.Text = Textos(6)
        Me.cmdSi.Text = Textos(7)
        Me.cmdNo.Text = Textos(8)
        Me.HyperDetalle.Text = Textos(9)

        If FSNUser.AccesoCN Then
            If Not Page.ClientScript.IsClientScriptBlockRegistered("EntidadColaboracion") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "EntidadColaboracion",
                    "var TipoEntidadColaboracion='SC'; var CodigoEntidadColaboracion ={identificador:" & oInstancia.ID &
                    ",texto:'" & JSText(oInstancia.ID & " - " & oInstancia.Solicitud.Den(Idioma)) & "'};", True)
            End If
        End If

        'Datos de la instancia:
        If Not oInstancia.CampoImporte Is Nothing Then
            Me.lblLitImporte.Text = oInstancia.CampoImporte & ":"
            Me.lblImporte.Text = FSNLibrary.FormatNumber(oInstancia.Importe, FSNUser.NumberFormat) & " " & oInstancia.Moneda
        Else
            If oInstancia.Solicitud.Tipo = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras Then
                Me.lblLitImporte.Text = Textos(0)
                Me.lblImporte.Text = FSNLibrary.FormatNumber(oInstancia.Importe, FSNUser.NumberFormat) & " " & oInstancia.Moneda  'Importe:
            Else
                Me.lblLitImporte.Visible = False
                Me.lblImporte.Visible = False
            End If
        End If

        Dim sEtapaActual As String = Nothing
        Select Case oInstancia.Estado
            Case TipoEstadoSolic.Pendiente, TipoEstadoSolic.EnCurso
                sEtapaActual = Textos(36)
            Case TipoEstadoSolic.Guardada
                sEtapaActual = Textos(10)
            Case TipoEstadoSolic.SCPendiente, TipoEstadoSolic.SCAprobada
                sEtapaActual = Textos(12)
            Case TipoEstadoSolic.SCRechazada
                sEtapaActual = Textos(13)
            Case TipoEstadoSolic.SCAnulada
                sEtapaActual = Textos(14)
            Case TipoEstadoSolic.SCCerrada
                sEtapaActual = Textos(15)
        End Select
        lblIDInstanciayEstado.Text = oInstancia.ID & " (" & sEtapaActual & ")"

        'hiperv�nculo para el detalle del workflow
        If oInstancia.VerDetalleFlujo = True Then
            Me.HyperDetalle.NavigateUrl = "../seguimiento/NWhistoricoestados.aspx?Instancia=" + CStr(oInstancia.ID)
        Else
            Me.HyperDetalle.Visible = False
        End If

        If oInstancia.VerDetallePersona = False Then
            Me.lblLitCreadoPor.Visible = False
            Me.lblCreadoPor.Visible = False
            Me.imgInfCreadoPor.Visible = False
        Else
            If oInstancia.PeticionarioProve = "" Then
                imgInfCreadoPor.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosPeticionario.AnimationClientID & "', event, '" & FSNPanelDatosPeticionario.DynamicPopulateClientID & "', '" & oInstancia.Peticionario & "'); return false;")
            Else
                imgInfCreadoPor.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosProveedor.AnimationClientID & "', event, '" & FSNPanelDatosProveedor.DynamicPopulateClientID & "', '" & oInstancia.PeticionarioProve & "'); return false;")
            End If
        End If

        imgInfTipo.Attributes.Add("onclick", "DetalleTipoSolicitud()")

        Me.lblEstado.Text = oInstancia.DenEtapaActual
        Me.lblTipo.Text = oInstancia.Solicitud.Codigo + "-" + oInstancia.Solicitud.Den(Idioma)
        If oInstancia.PeticionarioProve = "" Then
            lblCreadoPor.Text = oInstancia.NombrePeticionario
        Else
            lblCreadoPor.Text = oInstancia.PeticionarioProve + " (" + oInstancia.NombrePeticionario + ")"
        End If
        Me.lblFecAlta.Text = FormatDate(oInstancia.FechaAlta, FSNUser.DateFormat)
        Me.txtIdTipo.Value = oInstancia.Solicitud.ID
        Me.hRolActual.Value = oInstancia.RolActual
        Me.Instancia.Value = oInstancia.ID

        CargarCamposGrupo()

        Me.FindControl("frmDetalle").Controls.Add(FSNWeb.CommonAlta.InsertarCalendario())

        RegistrarScripts()

        Me.cmdNo.OnClientClick = "cmdNo_click();return false;"
    End Sub

    Private Sub cmdSi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSi.Click
        '********************************************************************************************************
        '*** Autor: sra                                                                                     *****
        '*** Descripci�n: Comprueba que la solicitud no est� asignada a otro usuario, asigna la solicitud   *****
        '*** al usuario actual y le redirige a la solicitud para que pueda gestionarla.                     *****
        '*** Llamada desde: Se lanza al hacer click sobre el WebControl cmdSi          						*****                    
        '*** Tiempo m�ximo: 0 sg  						                                                    *****                    
        '********************************************************************************************************
        Dim oRol As FSNServer.Rol

        oRol = FSNServer.Get_Object(GetType(FSNServer.Rol))
        oRol.Id = hRolActual.Value
        oRol.IdInstancia = CType(Request("Instancia"), Long)
        oRol.Load()
        If oRol.Persona = Nothing Then
            oRol.AsignarRol(FSNUser.CodPersona)
            Dim volver As String = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx"
            If tipoSolicitud = TiposDeDatos.TipoDeSolicitud.Factura Then volver += "&TipoSolicitud=13"
            Response.Redirect("NWgestioninstancia.aspx?Instancia=" & CStr(Request("Instancia")) & "&volver=" & volver)
        Else
            'LLama a la ventana que indica que la solicitud ya se est� gestionando por otro usuario:
            If tipoSolicitud = TiposDeDatos.TipoDeSolicitud.Factura Then
                Response.Redirect("instanciaasignada.aspx?Instancia=" & Request("Instancia") & "&TipoSolicitud=13")
            Else
                Response.Redirect("instanciaasignada.aspx?Instancia=" & Request("Instancia"))
            End If
        End If
    End Sub

    ''' <summary>
    ''' Carga los Campos de los grupos de la solicitud
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarCamposGrupo()
        Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim lIndex As Integer = 0
        Dim oucCampos As campos
        Dim oucDesglose As desgloseControl
        Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab
        Dim oGrupo As FSNServer.Grupo
        Dim oRow As DataRow

        'Rellena el tab :
        uwtGrupos.Tabs.Clear()
        AplicarEstilosTab(uwtGrupos)

        If Not oInstancia.Grupos.Grupos Is Nothing Then
            For Each oGrupo In oInstancia.Grupos.Grupos
                oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden

                oInputHidden.ID = "txtPre_" + lIndex.ToString

                oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
                Dim Font As String = ObtenerValorPropiedad(".uwtDefaultTab", "font-family")
                Dim Size As String = Replace(ObtenerValorPropiedad(".uwtDefaultTab", "font-size"), "pt", "")
                oTabItem.Text = AjustarAnchoTextoPixels(oGrupo.Den(FSNUser.Idioma.ToString), AnchoDeTab, IIf(Font = "", "verdana", Font), IIf(Size = "", 8, CInt(Size)), False)
                oTabItem.Key = lIndex.ToString
                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                uwtGrupos.Tabs.Add(oTabItem)
                lIndex += 1

                If oGrupo.DSCampos.Tables.Count > 0 Then
                    If oGrupo.NumCampos <= 1 Then
                        For Each oRow In oGrupo.DSCampos.Tables(0).Rows
                            If oRow.Item("VISIBLE") = 1 Then
                                Exit For
                            End If
                        Next

                        If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Desglose Or (DBNullToSomething(oRow.Item("SUBTIPO")) = TiposDeDatos.TipoGeneral.TipoDesglose And DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.DesgloseActividad) Then
                            If oRow.Item("VISIBLE") = 0 Then
                                uwtGrupos.Tabs.Remove(oTabItem)
                            Else
                                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                                oTabItem.ContentPane.UserControlUrl = "..\alta\desglose.ascx"
                                oucDesglose = oTabItem.ContentPane.UserControl
                                oucDesglose.ID = oGrupo.Id.ToString
                                oucDesglose.Campo = oRow.Item("ID_CAMPO")
                                oucDesglose.TabContainer = uwtGrupos.ClientID
                                oucDesglose.Instancia = oInstancia.ID
                                oucDesglose.Version = oInstancia.Version
                                oucDesglose.SoloLectura = True
                                oucDesglose.Titulo = DBNullToSomething(oRow.Item("DEN_" & Idioma))
                                oucDesglose.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                                oInputHidden.Value = oucDesglose.ClientID
                            End If
                        Else
                            oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                            oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
                            oucCampos = oTabItem.ContentPane.UserControl
                            oucCampos.Instancia = oInstancia.ID
                            oucCampos.ID = oGrupo.Id.ToString
                            oucCampos.dsCampos = oGrupo.DSCampos
                            oucCampos.IdGrupo = oGrupo.Id
                            oucCampos.Idi = FSNUser.Idioma.ToString
                            oucCampos.TabContainer = uwtGrupos.ClientID
                            oucCampos.Version = oInstancia.Version
                            oucCampos.SoloLectura = True
                            oucCampos.InstanciaMoneda = oInstancia.Moneda
                            oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                            oInputHidden.Value = oucCampos.ClientID
                            oucCampos.Formulario = oInstancia.Solicitud.Formulario
                            oucCampos.ObjInstancia = oInstancia
                        End If

                    Else
                        oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                        oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
                        oucCampos = oTabItem.ContentPane.UserControl
                        oucCampos.Instancia = oInstancia.ID
                        oucCampos.ID = oGrupo.Id.ToString
                        oucCampos.dsCampos = oGrupo.DSCampos
                        oucCampos.IdGrupo = oGrupo.Id
                        oucCampos.Idi = FSNUser.Idioma.ToString
                        oucCampos.TabContainer = uwtGrupos.ClientID
                        oucCampos.Version = oInstancia.Version
                        oucCampos.SoloLectura = True
                        oucCampos.InstanciaMoneda = oInstancia.Moneda
                        oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                        oInputHidden.Value = oucCampos.ClientID
                        oucCampos.Formulario = oInstancia.Solicitud.Formulario
                        oucCampos.ObjInstancia = oInstancia
                    End If
                End If
                Me.FindControl("frmDetalle").Controls.Add(oInputHidden)
            Next
        End If
    End Sub

    ''' <summary>
    ''' Registra los script necesarios para la pantalla
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub RegistrarScripts()
        If Not Page.ClientScript.IsClientScriptIncludeRegistered("claveArrayDesgloses") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "claveArrayDesgloses", "<script>var arrDesgloses = new Array()</script>")
        End If

    End Sub


    Private Sub ConfigurarCabeceraMenu()
        FSNPageHeader.TituloCabecera = If(oInstancia.Den(Idioma) <> "", oInstancia.Den(Idioma), oInstancia.Solicitud.Den(Idioma))
        FSNPageHeader.UrlImagenCabecera = "images/SolicitudesPMSmall.jpg"

        FSNPageHeader.TextoBotonVolver = Textos(31)
        FSNPageHeader.VisibleBotonVolver = True
        FSNPageHeader.OnClientClickVolver = "cmdNo_click(); return false;"
    End Sub
End Class

