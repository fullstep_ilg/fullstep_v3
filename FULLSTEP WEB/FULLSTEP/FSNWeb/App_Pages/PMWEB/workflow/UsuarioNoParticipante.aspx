<%@ Page Language="vb" AutoEventWireup="false" Codebehind="UsuarioNoParticipante.aspx.vb" Inherits="Fullstep.FSNWeb.UsuarioNoParticipante" %>
<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
            <asp:ScriptManager ID="sm" runat="server"></asp:ScriptManager>
			<asp:Label id="lblMensaje" style="Z-INDEX: 101; LEFT: 16px; POSITION: absolute; TOP: 152px"
				runat="server" Width="100%" CssClass="SinDatos"></asp:Label>
			<uc1:menu id="Menu1" runat="server"></uc1:menu>
		</form>
	</body>
</html>
