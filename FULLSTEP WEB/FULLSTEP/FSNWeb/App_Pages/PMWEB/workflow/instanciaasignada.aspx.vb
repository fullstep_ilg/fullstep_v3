
    Public Class instanciaasignada
    Inherits FSNPage

#Region " Web Form Designer Generated Code "
    Protected WithEvents HyperDetalle As System.Web.UI.WebControls.HyperLink
    Protected WithEvents lblMensaje1 As System.Web.UI.WebControls.Label
    Protected WithEvents lblMensaje2 As System.Web.UI.WebControls.Label
    Protected WithEvents uwtGrupos As Infragistics.WebUI.UltraWebTab.UltraWebTab
    Protected WithEvents txtIdTipo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Version As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents FSNPageHeader As Global.Fullstep.FSNWebControls.FSNPageHeader

    Protected WithEvents lblLitImporte As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitCreadoPor As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitEstado As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitFecAlta As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitTipo As System.Web.UI.WebControls.Label

    Protected WithEvents lblIDInstanciayEstado As System.Web.UI.WebControls.Label
    Protected WithEvents lblImporte As System.Web.UI.WebControls.Label
    Protected WithEvents lblCreadoPor As System.Web.UI.WebControls.Label
    Protected WithEvents lblEstado As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecAlta As System.Web.UI.WebControls.Label
    Protected WithEvents lblTipo As System.Web.UI.WebControls.Label

    Protected WithEvents imgInfCreadoPor As Global.System.Web.UI.WebControls.Image
    Protected WithEvents imgInfTipo As Global.System.Web.UI.WebControls.Image

    Protected WithEvents FSNPanelDatosPeticionario As Global.Fullstep.FSNWebControls.FSNPanelInfo
    Protected WithEvents FSNPanelDatosProveedor As Global.Fullstep.FSNWebControls.FSNPanelInfo
    Protected WithEvents Menu1 As MenuControl

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private _oInstancia As FSNServer.Instancia
    Protected ReadOnly Property oInstancia() As FSNServer.Instancia
        Get
            If _oInstancia Is Nothing Then
                If Me.IsPostBack Then
                    _oInstancia = CType(Cache("oInstancia" & FSNUser.Cod), FSNServer.Instancia)
                Else
                    _oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                    _oInstancia.ID = Request("Instancia")

                    'Carga los datos de la instancia:
                    _oInstancia.Cargar(Idioma)
                    _oInstancia.CargarCamposInstancia(Idioma, FSNUser.CodPersona, , True, True)
                    _oInstancia.DevolverEtapaActual(Idioma)

                    Me.InsertarEnCache("oInstancia" & FSNUser.Cod, _oInstancia)
                End If
            End If
            Return _oInstancia
        End Get
    End Property

    Private _bSoloLectura As Boolean = False
    Protected Property bSoloLectura() As Boolean
        Get
            ViewState("bSoloLectura") = _bSoloLectura
            Return ViewState("bSoloLectura")
        End Get
        Set(ByVal value As Boolean)
            _bSoloLectura = value
        End Set
    End Property

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Const IncludeScriptFormat As String = ControlChars.CrLf & _
"<script type=""{0}"" src=""{1}""></script>"

    ''' <summary>
    ''' Carga la pagina instancia asignada
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">evento de sistema</param>           
    ''' <remarks>Tiempo máximo: 0,3</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim iTipoGS As TiposDeDatos.TipoCampoGS
        Dim iTipo As TiposDeDatos.TipoGeneral
        Dim oEntry As DataEntry.GeneralEntry
        Dim oUser As FSNServer.User
        Dim tipoSolicitud As Integer = 0

        If Request("TipoSolicitud") <> Nothing Then tipoSolicitud = CInt(Request("TipoSolicitud"))
        If Not ClientScript.IsStartupScriptRegistered("tipoSolicitud") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "tipoSolicitud", "<script>var tipoSolicitud = '" & tipoSolicitud & "';</script>")
        End If
        Select Case tipoSolicitud
            Case TiposDeDatos.TipoDeSolicitud.Factura
                Menu1.OpcionMenu = "Facturacion"
                Menu1.OpcionSubMenu = "Seguimiento"
            Case TiposDeDatos.TipoDeSolicitud.Encuesta
                Menu1.OpcionMenu = "Calidad"
                Menu1.OpcionSubMenu = "Encuestas"
            Case TiposDeDatos.TipoDeSolicitud.SolicitudQA
                Menu1.OpcionMenu = "Calidad"
                Menu1.OpcionSubMenu = "SolicitudesQA"
            Case Else
                Menu1.OpcionMenu = "Procesos"
                Menu1.OpcionSubMenu = "Seguimiento"
        End Select

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Aprobacion
        ConfigurarCabeceraMenu()

        Me.Version.Value = oInstancia.Version

        'Rellena el tab :
        If Page.IsPostBack Then
            CargarCamposGrupo()

            For Each oGrupo In oInstancia.Grupos.Grupos
                For Each oRow In oGrupo.DSCampos.Tables(0).Rows
                    iTipo = oRow.Item("SUBTIPO")
                    iTipoGS = DBNullToSomething(oRow.Item("TIPO_CAMPO_GS"))
                    oEntry = Me.uwtGrupos.FindControl("fsentry" + oRow.Item("ID").ToString())
                Next
            Next
        Else
            uwtGrupos.Tabs.Clear()
        End If

        AplicarEstilosTab(uwtGrupos)

        'Textos de idiomas:
        Me.lblLitTipo.Text = Textos(1)
        Me.lblLitCreadoPor.Text = Textos(53) & ":"
        Me.lblFecAlta.Text = Textos(54) & ":"
        Me.HyperDetalle.Text = Textos(9)
        Me.lblMensaje1.Text = Textos(16)


        oUser = FSNServer.Get_Object(GetType(FSNServer.User))
        oUser.LoadUserData(oInstancia.AprobadorActual)
        Me.lblMensaje2.Text = Textos(17) & " " & DBNullToSomething(oUser.Nombre)

        'Datos de la instancia:
        If Not oInstancia.CampoImporte Is Nothing Then
            Me.lblLitImporte.Text = oInstancia.CampoImporte & ":"
            Me.lblImporte.Text = FSNLibrary.FormatNumber(oInstancia.Importe, FSNUser.NumberFormat) & " " & oInstancia.Moneda
        Else
            If oInstancia.Solicitud.Tipo = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras Then
                Me.lblLitImporte.Text = Textos(0)
                Me.lblImporte.Text = FSNLibrary.FormatNumber(oInstancia.Importe, FSNUser.NumberFormat) & " " & oInstancia.Moneda  'Importe:
            Else
                Me.lblLitImporte.Visible = False
                Me.lblImporte.Visible = False
            End If
        End If

        Dim sEtapaActual As String = Nothing
        Select Case oInstancia.Estado
            Case TipoEstadoSolic.Pendiente, TipoEstadoSolic.EnCurso
                sEtapaActual = Textos(36)
            Case TipoEstadoSolic.Guardada
                sEtapaActual = Textos(10)
            Case TipoEstadoSolic.SCPendiente, TipoEstadoSolic.SCAprobada
                sEtapaActual = Textos(12)
            Case TipoEstadoSolic.SCRechazada
                sEtapaActual = Textos(13)
            Case TipoEstadoSolic.SCAnulada
                sEtapaActual = Textos(14)
            Case TipoEstadoSolic.SCCerrada
                sEtapaActual = Textos(15)
        End Select
        lblIDInstanciayEstado.Text = oInstancia.ID & " (" & sEtapaActual & ")"

        'hipervínculo para el detalle del workflow
        If oInstancia.IdWorkflow = Nothing Then  'Si no hay workflow no muestra el hipervínculo
            Me.HyperDetalle.Visible = False
        Else
            Me.HyperDetalle.NavigateUrl = "../seguimiento/NWhistoricoestados.aspx?Instancia=" + CStr(oInstancia.ID)
        End If

        Me.lblEstado.Text = oInstancia.DenEtapaActual
        Me.lblTipo.Text = oInstancia.Solicitud.Codigo + " - " + oInstancia.Solicitud.Den(Idioma)
        If oInstancia.PeticionarioProve = "" Then
            lblCreadoPor.Text = oInstancia.NombrePeticionario
        Else
            lblCreadoPor.Text = oInstancia.PeticionarioProve + " (" + oInstancia.NombrePeticionario + ")"
        End If
        Me.lblFecAlta.Text = FormatDate(oInstancia.FechaAlta, FSNUser.DateFormat)
        Me.txtIdTipo.Value = oInstancia.Solicitud.ID

        If oInstancia.PeticionarioProve = "" Then
            imgInfCreadoPor.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosPeticionario.AnimationClientID & "', event, '" & FSNPanelDatosPeticionario.DynamicPopulateClientID & "', '" & oInstancia.Peticionario & "'); return false;")
        Else
            imgInfCreadoPor.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosProveedor.AnimationClientID & "', event, '" & FSNPanelDatosProveedor.DynamicPopulateClientID & "', '" & oInstancia.PeticionarioProve & "'); return false;")
        End If
        imgInfTipo.Attributes.Add("onclick", "DetalleTipoSolicitud()")

        CargarCamposGrupo()

        Me.FindControl("frmDetalle").Controls.Add(FSNWeb.CommonAlta.InsertarCalendario())

        If Not Page.ClientScript.IsClientScriptBlockRegistered("claveArrayDesgloses") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "claveArrayDesgloses", "<script>var arrDesgloses = new Array()</script>")
        End If
    End Sub

    ''' <summary>
    ''' Carga los Campos de los grupos de la solicitud
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarCamposGrupo()
        Dim lIndex As Integer = 0
        Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab
        Dim oucCampos As campos
        Dim oucDesglose As desgloseControl
        Dim oRow As DataRow

        Dim oGrupo As FSNServer.Grupo

        bSoloLectura = True

        If Not oInstancia.Grupos.Grupos Is Nothing Then
            For Each oGrupo In oInstancia.Grupos.Grupos
                oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden

                oInputHidden.ID = "txtPre_" + lIndex.ToString
                oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
                Dim Font As String = ObtenerValorPropiedad(".uwtDefaultTab", "font-family")
                Dim Size As String = Replace(ObtenerValorPropiedad(".uwtDefaultTab", "font-size"), "pt", "")
                oTabItem.Text = AjustarAnchoTextoPixels(oGrupo.Den(Idioma), AnchoDeTab, IIf(Font = "", "verdana", Font), IIf(Size = "", 8, CInt(Size)), False)
                oTabItem.Key = lIndex.ToString
                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                uwtGrupos.Tabs.Add(oTabItem)
                lIndex += 1

                If oGrupo.DSCampos.Tables.Count > 0 Then
                    If oGrupo.NumCampos <= 1 Then
                        For Each oRow In oGrupo.DSCampos.Tables(0).Rows
                            If oRow.Item("VISIBLE") = 1 Then
                                Exit For
                            End If

                        Next
                        If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Desglose Or (DBNullToSomething(oRow.Item("SUBTIPO")) = TiposDeDatos.TipoGeneral.TipoDesglose And DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.DesgloseActividad) Then
                            If oRow.Item("VISIBLE") = 0 Then
                                uwtGrupos.Tabs.Remove(oTabItem)
                            Else
                                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                                oTabItem.ContentPane.UserControlUrl = "..\alta\desglose.ascx"
                                oucDesglose = oTabItem.ContentPane.UserControl
                                oucDesglose.ID = oGrupo.Id.ToString
                                oucDesglose.Campo = oRow.Item("ID_CAMPO")
                                oucDesglose.TabContainer = uwtGrupos.ClientID
                                oucDesglose.Instancia = oInstancia.ID
                                oucDesglose.Version = oInstancia.Version
                                oucDesglose.SoloLectura = bSoloLectura Or (oRow.Item("ESCRITURA") = 0)
                                oucDesglose.Titulo = DBNullToSomething(oRow.Item("DEN_" & Idioma))
                                oucDesglose.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                                oInputHidden.Value = oucDesglose.ClientID
                            End If
                        Else
                            oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                            oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
                            oucCampos = oTabItem.ContentPane.UserControl
                            oucCampos.Instancia = oInstancia.ID
                            oucCampos.ID = oGrupo.Id.ToString
                            oucCampos.dsCampos = oGrupo.DSCampos
                            oucCampos.IdGrupo = oGrupo.Id
                            oucCampos.Idi = Idioma
                            oucCampos.TabContainer = uwtGrupos.ClientID
                            oucCampos.Version = oInstancia.Version
                            oucCampos.SoloLectura = bSoloLectura
                            oucCampos.InstanciaMoneda = oInstancia.Moneda
                            oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                            oucCampos.Formulario = oInstancia.Solicitud.Formulario
                            oucCampos.ObjInstancia = oInstancia
                            oInputHidden.Value = oucCampos.ClientID
                        End If

                    Else
                        oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                        oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
                        oucCampos = oTabItem.ContentPane.UserControl
                        oucCampos.Instancia = oInstancia.ID
                        oucCampos.ID = oGrupo.Id.ToString
                        oucCampos.dsCampos = oGrupo.DSCampos
                        oucCampos.IdGrupo = oGrupo.Id
                        oucCampos.Idi = Idioma
                        oucCampos.TabContainer = uwtGrupos.ClientID
                        oucCampos.Version = oInstancia.Version
                        oucCampos.SoloLectura = bSoloLectura
                        oucCampos.InstanciaMoneda = oInstancia.Moneda
                        oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                        oucCampos.Formulario = oInstancia.Solicitud.Formulario
                        oucCampos.ObjInstancia = oInstancia
                        oInputHidden.Value = oucCampos.ClientID
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub ConfigurarCabeceraMenu()
        FSNPageHeader.TituloCabecera = If(oInstancia.Den(Idioma) <> "", oInstancia.Den(Idioma), oInstancia.Solicitud.Den(Idioma))
        FSNPageHeader.UrlImagenCabecera = "images/SolicitudesPMSmall.jpg"

        FSNPageHeader.TextoBotonVolver = Textos(31)
        FSNPageHeader.VisibleBotonVolver = True
        FSNPageHeader.OnClientClickVolver = "Volver(); return false;"
    End Sub
End Class

