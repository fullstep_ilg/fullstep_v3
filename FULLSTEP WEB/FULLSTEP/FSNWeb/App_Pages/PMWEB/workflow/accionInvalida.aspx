<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="accionInvalida.aspx.vb" Inherits="Fullstep.FSNWeb.accionInvalida" %>
<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
</head>
<script type="text/javascript">
    function BuscarPeticionario() {
        var Cod = document.forms("Form1").item("txtPeticionario").value

        var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/detallepersona.aspx?CodPersona=" + Cod.toString(), "_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");
			newWindow.focus();
        }
</script>
<body>
    <form id="Form1" method="post" runat="server">
        <asp:ScriptManager ID="sm" runat="server"></asp:ScriptManager>
        <input id="txtPeticionario" style="z-index: 102; left: 8px; position: absolute; top: 8px" type="hidden" name="txtPeticionario" runat="server">
        <uc1:menu ID="Menu1" runat="server"></uc1:menu>
        <table id="tblGeneral" style="z-index: 101; left: 16px; position: absolute; top: 140px"
            cellspacing="1" cellpadding="1" width="90%" border="0" runat="server" height="85%">
            <tr>
                <td width="80%"><asp:Label ID="lblTitulo" runat="server" Width="100%" CssClass="Titulo">DEmisi�n de solicitud tipo XXX</asp:Label></td>
            </tr>
            <tr>
                <td>
                    <table id="tblCabecera" cellspacing="1" cellpadding="1" width="100%" border="0">
                        <tr height="40px">
                            <td width="7%"><asp:Label ID="lblLitId" runat="server" CssClass="captionBlue" Width="100%">DIdentificador de la solicitud:</asp:Label></td>
                            <td width="20%"><asp:Label ID="lblId" runat="server" CssClass="captionDarkBlue" Width="100%">Label</asp:Label></td>
                            <td width="5%"></td>
                            <td width="5%"><asp:Label ID="lblFecha" runat="server" CssClass="captionBlue" Width="100%">lblFecha</asp:Label></td>
                            <td width="10%"><asp:Label ID="lblFechaBD" runat="server" CssClass="captionDarkBlue" Width="100%">lblFechaBD</asp:Label></td>
                        </tr>
                        <tr height="40px">
                            <td><asp:Label ID="lblLitPeticionario" runat="server" CssClass="captionBlue" Width="100%">DPeticionario:</asp:Label></td>
                            <td><asp:Label ID="lblPeticionario" runat="server" CssClass="captionDarkBlue" Width="100%">Label</asp:Label></td>
                            <td>
                                <a id="cmdBuscarPet" class="aPMWeb" style="cursor: hand" onclick="javascript:BuscarPeticionario()" name="cmdBuscarPet">
                                <img src="../alta/images/help.gif" border="0"></a>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr height="60%">
                <td valign="top"><asp:Label ID="lblMensaje" runat="server" Width="100%" CssClass="SinDatos">lblMensaje</asp:Label></td>
            </tr>
        </table>
    </form>
</body>
</html>
