
    Public Class gestiontrasladada
    Inherits FSNPage

#Region " Web Form Designer Generated Code "
    Protected WithEvents HyperDetalle As System.Web.UI.WebControls.HyperLink
    Protected WithEvents txtIdTipo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents uwtGrupos As Infragistics.WebUI.UltraWebTab.UltraWebTab
    Protected WithEvents txtComent As System.Web.UI.WebControls.TextBox
    Protected WithEvents Instancia As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtEnviar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Version As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Bloque As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cadenaespera As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents BotonCalcular As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents hidVolver As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divForm1 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divForm2 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divForm3 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divProgreso As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblProgreso As System.Web.UI.WebControls.TextBox
    Protected WithEvents imgProgreso As System.Web.UI.WebControls.Image
    Protected WithEvents tblProgreso As System.Web.UI.HtmlControls.HtmlTable
    
    Protected WithEvents FSNPageHeader As Global.Fullstep.FSNWebControls.FSNPageHeader

    Protected WithEvents lblLitImporte As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitCreadoPor As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitEstado As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitFecAlta As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitTipo As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitTrasladadaPor As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitFechaTraslado As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitFechaDevolucion As System.Web.UI.WebControls.Label

    Protected WithEvents lblIDInstanciayEstado As System.Web.UI.WebControls.Label
    Protected WithEvents lblImporte As System.Web.UI.WebControls.Label
    Protected WithEvents lblCreadoPor As System.Web.UI.WebControls.Label
    Protected WithEvents lblEstado As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecAlta As System.Web.UI.WebControls.Label
    Protected WithEvents lblTipo As System.Web.UI.WebControls.Label
    Protected WithEvents lblTrasladadaPor As System.Web.UI.WebControls.Label
    Protected WithEvents lblFechaTraslado As System.Web.UI.WebControls.Label
    Protected WithEvents lblFechaDevolucion As System.Web.UI.WebControls.Label

    Protected WithEvents imgInfCreadoPor As Global.System.Web.UI.WebControls.Image
    Protected WithEvents imgInfTrasladadaPor As Global.System.Web.UI.WebControls.Image
    Protected WithEvents imgInfTipo As Global.System.Web.UI.WebControls.Image

    Protected WithEvents FSNPanelDatosPeticionario As Global.Fullstep.FSNWebControls.FSNPanelInfo
    Protected WithEvents FSNPanelDatosTrasladadaPor As Global.Fullstep.FSNWebControls.FSNPanelInfo
    Protected WithEvents FSNPanelDatosProveedor As Global.Fullstep.FSNWebControls.FSNPanelInfo
    Protected WithEvents mnuMenu As MenuControl

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private _oInstancia As FSNServer.Instancia
    Protected ReadOnly Property oInstancia() As FSNServer.Instancia
        Get
            If _oInstancia Is Nothing Then
                If Me.IsPostBack Then
                    _oInstancia = CType(Cache("oInstancia" & FSNUser.Cod), FSNServer.Instancia)
                Else
                    _oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                    _oInstancia.ID = Request("Instancia")

                    'Carga los datos de la instancia:
                    _oInstancia.Load(Idioma, True)
                    _oInstancia.CargarCamposInstancia(Idioma, FSNUser.CodPersona, , True, , FSNUser.DateFmt)
                    _oInstanciaGrupos = _oInstancia.Grupos

                    Me.InsertarEnCache("oInstancia" & FSNUser.Cod, _oInstancia)
                    Me.InsertarEnCache("oInstanciaGrupos" & FSNUser.Cod, _oInstanciaGrupos)
                End If
            End If
            Return _oInstancia
        End Get
    End Property

    Private _oInstanciaGrupos As FSNServer.Grupos
    Protected ReadOnly Property oInstanciaGrupos() As FSNServer.Grupos
        Get
            _oInstanciaGrupos = CType(Cache("oInstanciaGrupos" & FSNUser.Cod), FSNServer.Grupos)
            If _oInstanciaGrupos Is Nothing Then
                _oInstancia.CargarCamposInstancia(Idioma, FSNUser.CodPersona, , True)
                _oInstanciaGrupos = _oInstancia.Grupos
            End If
            Return _oInstanciaGrupos
        End Get
    End Property

    Private _bSoloLectura As Boolean = False
    Protected Property bSoloLectura() As Boolean
        Get
            ViewState("bSoloLectura") = _bSoloLectura
            Return ViewState("bSoloLectura")
        End Get
        Set(ByVal value As Boolean)
            _bSoloLectura = value
        End Set
    End Property

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
"<script language=""{0}"">{1}</script>"
    ''' <summary>
    '''Muestra la pagina de que la instancia ha sido trasladada
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">evento de sistema</param>           
    ''' <remarks>Tiempo m�ximo: 0,3</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Response.Expires = -1

        CargarTextoEspera()

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.GestionTraslado

        If Page.IsPostBack Then
            CargarCamposGrupo()
            Exit Sub
        End If

        Dim tipoSolicitud As Integer = 0

        If Request("TipoSolicitud") <> Nothing Then tipoSolicitud = CInt(Request("TipoSolicitud"))
        Select Case tipoSolicitud
            Case TiposDeDatos.TipoDeSolicitud.Factura
                mnuMenu.OpcionMenu = "Facturacion"
                mnuMenu.OpcionSubMenu = "Seguimiento"
            Case TiposDeDatos.TipoDeSolicitud.Encuesta
                mnuMenu.OpcionMenu = "Calidad"
                mnuMenu.OpcionSubMenu = "Encuestas"
            Case TiposDeDatos.TipoDeSolicitud.SolicitudQA
                mnuMenu.OpcionMenu = "Calidad"
                mnuMenu.OpcionSubMenu = "SolicitudesQA"
            Case Else
                mnuMenu.OpcionMenu = "Procesos"
                mnuMenu.OpcionSubMenu = "Seguimiento"
        End Select

        oInstancia.DevolverEtapaActualTraslado(Idioma)
        Bloque.Value = oInstancia.Etapa
        Version.Value = oInstancia.Version
        Instancia.Value = oInstancia.ID

        ConfigurarCabeceraMenu() 'tiene que estar cargada oInstancia

        If Not ClientScript.IsStartupScriptRegistered("oRol_Id") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "oRol_Id", "var oRol_Id=" & oInstancia.RolActual & ";", True)

        'Textos de idiomas:
        lblLitImporte.Text = Textos(0)
        lblLitCreadoPor.Text = Textos(27) & ":"
        lblLitEstado.Text = Textos(22) & ":"
        lblLitFecAlta.Text = Textos(28) & ":"
        lblLitTipo.Text = Textos(1)
        lblLitFechaTraslado.Text = Textos(8)
        HyperDetalle.Text = Textos(5)

        If FSNUser.AccesoCN Then
            If Not Page.ClientScript.IsClientScriptBlockRegistered("EntidadColaboracion") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "EntidadColaboracion", _
                    "var TipoEntidadColaboracion='SC'; var CodigoEntidadColaboracion ={identificador:" & oInstancia.ID & _
                    ",texto:'" & JSText(oInstancia.ID & " - " & oInstancia.Solicitud.Den(Idioma)) & "'};", True)
            End If
        End If

        If oInstancia.DestinatarioEst = FSNUser.CodPersona OrElse oInstancia.EsSustitutodeDestinatario(FSNUser.CodPersona, oInstancia.DestinatarioEst) Then
            'Le han trasladado la solicitud y la tiene que cumplimentar:
            oInstancia.DevolverDatosTraslado(FSNUser.CodPersona)
            lblLitTrasladadaPor.Text = Textos(7)
            If oInstancia.FecLimiteTraslado = Nothing Then
                lblLitFechaDevolucion.Visible = False
                lblFechaDevolucion.Visible = False
            Else
                lblLitFechaDevolucion.Text = Textos(11)
                lblFechaDevolucion.Text = FormatDate(oInstancia.FecLimiteTraslado, FSNUser.DateFormat)
            End If
            lblTrasladadaPor.Text = oInstancia.NombrePersonaEst
        Else
            'Es una solicitud trasladada a otra persona, no podr� hacer nada:
            oInstancia.DevolverDatosTraslado(FSNUser.CodPersona, True)
            lblLitTrasladadaPor.Text = Textos(6)
            lblTrasladadaPor.Text = oInstancia.NombreDestinatario
            If oInstancia.FecLimiteTraslado = Nothing Then
                lblLitFechaDevolucion.Visible = False
                lblFechaDevolucion.Visible = False
            Else
                lblLitFechaDevolucion.Text = Textos(9)
                lblFechaDevolucion.Text = FormatDate(oInstancia.FecLimiteTraslado, FSNUser.DateFormat)
            End If
            bSoloLectura = True
        End If

        'hiperv�nculo para el detalle del workflow
        If oInstancia.IdWorkflow = Nothing Then  'Si no hay workflow no muestra el hiperv�nculo
            HyperDetalle.Visible = False
        Else
            If oInstancia.VerDetalleFlujo = True Then
                HyperDetalle.NavigateUrl = "../seguimiento/NWhistoricoestados.aspx?Instancia=" + CStr(oInstancia.ID)
                HyperDetalle.Visible = True
            Else
                HyperDetalle.Visible = False
            End If
        End If

        'Al ser un traslado el poder ver dos datos de las personas depende del rol del que traslada
        If oInstancia.VerDetallePersona = False Then
            imgInfCreadoPor.Visible = False
            lblLitCreadoPor.Visible = False
            lblCreadoPor.Visible = False
            imgInfTrasladadaPor.Visible = False
        Else
            If oInstancia.PeticionarioProve = "" Then
                imgInfCreadoPor.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosPeticionario.AnimationClientID & "', event, '" & FSNPanelDatosPeticionario.DynamicPopulateClientID & "', '" & oInstancia.Peticionario & "'); return false;")
            Else
                imgInfCreadoPor.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosProveedor.AnimationClientID & "', event, '" & FSNPanelDatosProveedor.DynamicPopulateClientID & "', '" & oInstancia.PeticionarioProve & "'); return false;")
            End If
            imgInfTrasladadaPor.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosTrasladadaPor.AnimationClientID & "', event, '" & FSNPanelDatosTrasladadaPor.DynamicPopulateClientID & "', '" & If(oInstancia.DestinatarioEst = Usuario.CodPersona OrElse oInstancia.EsSustitutodeDestinatario(Usuario.CodPersona, oInstancia.DestinatarioEst), oInstancia.PersonaEst, oInstancia.DestinatarioEst) & "'); return false;")
        End If

        'Datos de la instancia:
        lblIDInstanciayEstado.Text = oInstancia.ID & " (" & Textos(10) & ")"
        lblImporte.Text = FSNLibrary.FormatNumber(oInstancia.Importe, Usuario.NumberFormat) & " " & oInstancia.Moneda
        lblEstado.Text = oInstancia.DenEtapaActual
        lblTipo.Text = oInstancia.Solicitud.Codigo + " - " + oInstancia.Solicitud.Den(Idioma)
        If oInstancia.PeticionarioProve = "" Then
            lblCreadoPor.Text = oInstancia.NombrePeticionario
        Else
            lblCreadoPor.Text = oInstancia.PeticionarioProve + " (" + oInstancia.NombrePeticionario + ")"
        End If
        lblFecAlta.Text = FormatDate(oInstancia.FechaAlta, Usuario.DateFormat)
        txtIdTipo.Value = oInstancia.Solicitud.ID
        lblFechaTraslado.Text = FormatDate(oInstancia.FechaEstado, Usuario.DateFormat)
        txtComent.Text = oInstancia.ComentarioEstado

        imgInfTipo.Attributes.Add("onclick", "DetalleTipoSolicitud()")

        CargarCamposGrupo()

        FindControl("frmDetalle").Controls.Add(FSNWeb.CommonAlta.InsertarCalendario())

        RegistrarScripts()
    End Sub

    ''' <summary>
    ''' Carga los Campos de los grupos de la solicitud
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarCamposGrupo()
        Dim oRow As DataRow
        Dim lIndex As Integer = 0
        Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oucCampos As campos
        Dim oucDesglose As desgloseControl

        Dim oGrupo As FSNServer.Grupo
        Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab

        'Rellena el tab :
        uwtGrupos.Tabs.Clear()
        AplicarEstilosTab(uwtGrupos)

        If Not oInstanciaGrupos.Grupos Is Nothing Then
            For Each oGrupo In oInstanciaGrupos.Grupos
                oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden

                oInputHidden.ID = "txtPre_" + lIndex.ToString
                oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
                Dim Font As String = ObtenerValorPropiedad(".uwtDefaultTab", "font-family")
                Dim Size As String = Replace(ObtenerValorPropiedad(".uwtDefaultTab", "font-size"), "pt", "")
                oTabItem.Text = AjustarAnchoTextoPixels(oGrupo.Den(Idioma), AnchoDeTab, IIf(Font = "", "verdana", Font), IIf(Size = "", 8, CInt(Size)), False)
                oTabItem.Key = lIndex.ToString
                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Hidden
                uwtGrupos.Tabs.Add(oTabItem)
                lIndex += 1

                If oGrupo.DSCampos.Tables.Count > 0 Then
                    If oGrupo.NumCampos <= 1 Then
                        For Each oRow In oGrupo.DSCampos.Tables(0).Rows
                            If oRow.Item("VISIBLE") = 1 Then
                                Exit For
                            End If
                        Next
                        If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Desglose Or (DBNullToSomething(oRow.Item("SUBTIPO")) = TiposDeDatos.TipoGeneral.TipoDesglose And DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.DesgloseActividad) Then
                            If oRow.Item("VISIBLE") = 0 Then
                                uwtGrupos.Tabs.Remove(oTabItem)
                            Else
                                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                                oTabItem.ContentPane.UserControlUrl = "..\alta\desglose.ascx"
                                oucDesglose = oTabItem.ContentPane.UserControl
                                oucDesglose.ID = oGrupo.Id.ToString
                                oucDesglose.Campo = oRow.Item("ID_CAMPO")
                                oucDesglose.TabContainer = uwtGrupos.ClientID
                                oucDesglose.Instancia = oInstancia.ID
                                oucDesglose.Version = oInstancia.Version
                                oucDesglose.SoloLectura = bSoloLectura Or (oRow.Item("ESCRITURA") = 0)
                                oucDesglose.PM = True
                                oucDesglose.Titulo = DBNullToSomething(oRow.Item("DEN_" & Idioma))
                                oucDesglose.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                                oInputHidden.Value = oucDesglose.ClientID
                            End If
                        Else
                            oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                            oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
                            oucCampos = oTabItem.ContentPane.UserControl
                            oucCampos.Instancia = oInstancia.ID
                            oucCampos.ID = oGrupo.Id.ToString
                            oucCampos.dsCampos = oGrupo.DSCampos
                            oucCampos.IdGrupo = oGrupo.Id
                            oucCampos.Idi = Idioma
                            oucCampos.TabContainer = uwtGrupos.ClientID
                            oucCampos.Version = oInstancia.Version
                            oucCampos.SoloLectura = bSoloLectura
                            oucCampos.PM = True
                            oucCampos.Formulario = oInstancia.Solicitud.Formulario
                            oucCampos.ObjInstancia = oInstancia
                            oucCampos.InstanciaMoneda = oInstancia.Moneda
                            oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                            oInputHidden.Value = oucCampos.ClientID
                        End If

                    Else
                        oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                        oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
                        oucCampos = oTabItem.ContentPane.UserControl
                        oucCampos.Instancia = oInstancia.ID
                        oucCampos.ID = oGrupo.Id.ToString
                        oucCampos.dsCampos = oGrupo.DSCampos
                        oucCampos.IdGrupo = oGrupo.Id
                        oucCampos.Idi = Idioma
                        oucCampos.TabContainer = uwtGrupos.ClientID
                        oucCampos.Version = oInstancia.Version
                        oucCampos.SoloLectura = bSoloLectura
                        oucCampos.PM = True
                        oucCampos.Formulario = oInstancia.Solicitud.Formulario
                        oucCampos.ObjInstancia = oInstancia
                        oucCampos.InstanciaMoneda = oInstancia.Moneda
                        oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                        oInputHidden.Value = oucCampos.ClientID
                    End If
                End If
                Me.FindControl("frmDetalle").Controls.Add(oInputHidden)

            Next
        End If
    End Sub

    ''' <summary>
    ''' Registra los script necesarios para la pantalla
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub RegistrarScripts()
        Dim sClientTextVars As String
        sClientTextVars = ""
        sClientTextVars += "vdecimalfmt='" + Usuario.DecimalFmt + "';"
        sClientTextVars += "vthousanfmt='" + Usuario.ThousanFmt + "';"
        sClientTextVars += "vprecisionfmt='" + Usuario.PrecisionFmt + "';"
        sClientTextVars = String.Format(IncludeScriptKeyFormat, "javascript", sClientTextVars)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsUser", sClientTextVars)

        Dim sClientTexts As String
        sClientTexts = ""
        sClientTexts += "var arrTextosML = new Array();"
        sClientTexts += "arrTextosML[0] = '" + JSText(Textos(26)) + "';"
        sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeCambioTipoPedido", "<script>var mensajeCambioTipoPedido = '" & JSText(Textos(25)) & "' </script>")

        If Not Page.ClientScript.IsClientScriptBlockRegistered("claveArrayDesgloses") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "claveArrayDesgloses", "<script>var arrDesgloses = new Array()</script>")
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered("volver") Then
            hidVolver.Value = Replace(Request("Volver"), "*", "&")
            Dim includeScript As String
            includeScript = "function Volver() {" & vbCrLf
            If Request("volver") <> "" Then
                includeScript = includeScript & "window.open('" & Replace(Request("Volver"), "*", "&") & "','_top');" & vbCrLf
            Else
                includeScript = includeScript & "window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?volver=1','_top');" & vbCrLf
            End If
            includeScript = includeScript & "return false;" & vbCrLf
            includeScript = includeScript & "}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "volver", includeScript, True)
        End If
    End Sub

    ''' <summary>
    ''' Obtiene los textos de la pantalla de espera
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarTextoEspera()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Espera

        cadenaespera.Value = Textos(1)
        lblProgreso.Text = Textos(2)
    End Sub

    ''' Revisado por: sra. Fecha: 24/01/2012
    ''' <summary>
    ''' Configura la cabecera, muestra en el menu las posibles acciones
    ''' Activamos los botones que pueden aparecer en la pantalla
    ''' </summary>
    ''' <remarks>Llamada desde:=Page_load; Tiempo m�ximo:=0,1seg.</remarks>
    Private Sub ConfigurarCabeceraMenu()
        FSNPageHeader.TituloCabecera = If(oInstancia.Den(Idioma) <> "", oInstancia.Den(Idioma), oInstancia.Solicitud.Den(Idioma))
        FSNPageHeader.UrlImagenCabecera = "images/SolicitudesPMSmall.jpg"

        FSNPageHeader.TextoBotonGuardar = Textos(12)
        FSNPageHeader.TextoBotonDevolver = Textos(13)
        FSNPageHeader.TextoBotonTrasladar = Textos(14)
        FSNPageHeader.TextoBotonCalcular = Textos(21)
        FSNPageHeader.TextoBotonImpExp = Textos(20)
        FSNPageHeader.TextoBotonVolver = Textos(24)

        FSNPageHeader.OnClientClickVolver = "return Volver()"
        FSNPageHeader.OnClientClickImpExp = "return cmdImpExp_onclick()"

        FSNPageHeader.VisibleBotonImpExp = True
        FSNPageHeader.VisibleBotonVolver = True

        If oInstancia.DestinatarioEst = Usuario.CodPersona Or oInstancia.EsSustitutodeDestinatario(Usuario.CodPersona, oInstancia.DestinatarioEst) Then
            If FSNUser.PMPermitirTraslados AndAlso oInstancia.PermitirTraslados Then
                FSNPageHeader.OnClientClickTrasladar = "return Trasladar()"
                FSNPageHeader.VisibleBotonTrasladar = True
            End If

            FSNPageHeader.OnClientClickDevolver = "return Devolver()"
            FSNPageHeader.OnClientClickGuardar = "return Guardar()"

            FSNPageHeader.VisibleBotonGuardar = True
            FSNPageHeader.VisibleBotonDevolver = True
        End If

        'SI EXISTE ALGUN CAMPO DE TIPO=3 (CALCULADO) HACEMOS VISIBLE EL BOTON DE CALCULAR
        FSNPageHeader.VisibleBotonCalcular = oInstanciaGrupos.Grupos.OfType(Of FSNServer.Grupo).Where(Function(x) x.DSCampos.Tables(0).Rows.OfType(Of DataRow).Where(Function(y) y("TIPO") = TipoCampoPredefinido.Calculado).Any).Any()
        If FSNPageHeader.VisibleBotonCalcular Then
            BotonCalcular.Value = 1
            FSNPageHeader.OnClientClickCalcular = "return CalcularCamposCalculados()"
        End If
    End Sub
End Class

