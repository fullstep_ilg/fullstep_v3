Public Class UsuarioNoParticipante
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    Protected WithEvents Menu1 As MenuControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' <summary>
    ''' Muestra el mensaje de que el usuario no ha participado en el flujo de la solicitud,
    ''' y por lo tanto, no se muestra su detalle
    ''' </summary>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim tipoSolicitud As Integer = 0

        If Request.QueryString("TipoSolicitud") IsNot Nothing Then tipoSolicitud = CInt(Request.QueryString("TipoSolicitud"))

        Select Case tipoSolicitud
            Case TiposDeDatos.TipoDeSolicitud.Factura
                Menu1.OpcionMenu = "Facturacion"
                Menu1.OpcionSubMenu = "Seguimiento"
            Case TiposDeDatos.TipoDeSolicitud.Encuesta
                Menu1.OpcionMenu = "Calidad"
                Menu1.OpcionSubMenu = "Encuestas"
            Case TiposDeDatos.TipoDeSolicitud.SolicitudQA
                Menu1.OpcionMenu = "Calidad"
                Menu1.OpcionSubMenu = "SolicitudesQA"
            Case Else
                Menu1.OpcionMenu = "Procesos"
                Menu1.OpcionSubMenu = "Seguimiento"
        End Select

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.UsuarioNoParticipante
        Me.lblMensaje.Text = Textos(0)
    End Sub

End Class
