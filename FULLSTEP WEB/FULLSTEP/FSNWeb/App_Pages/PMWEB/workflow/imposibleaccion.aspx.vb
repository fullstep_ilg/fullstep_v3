
    Public Class imposibleaccion
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
    Protected WithEvents Form1 As System.Web.UI.HtmlControls.HtmlForm
    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecha As System.Web.UI.WebControls.Label
    Protected WithEvents lblDescr As System.Web.UI.WebControls.Label
    Protected WithEvents lblDescrBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblImporte As System.Web.UI.WebControls.Label
    Protected WithEvents lblId As System.Web.UI.WebControls.Label
    Protected WithEvents lblIdBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblPet As System.Web.UI.WebControls.Label
    Protected WithEvents lblPetBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    Protected WithEvents lblMensajeMapper As System.Web.UI.WebControls.Label
    Protected WithEvents Menu1 As MenuControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oInstancia As FSNServer.Instancia
        Dim tipoSolicitud As Integer = 0

        If Request("TipoSolicitud") <> Nothing Then tipoSolicitud = CInt(Request("TipoSolicitud"))
        Select Case tipoSolicitud
            Case TiposDeDatos.TipoDeSolicitud.Factura
                Menu1.OpcionMenu = "Facturacion"
                Menu1.OpcionSubMenu = "Seguimiento"
            Case TiposDeDatos.TipoDeSolicitud.Encuesta
                Menu1.OpcionMenu = "Calidad"
                Menu1.OpcionSubMenu = "Encuestas"
            Case TiposDeDatos.TipoDeSolicitud.SolicitudQA
                Menu1.OpcionMenu = "Calidad"
                Menu1.OpcionSubMenu = "SolicitudesQA"
            Case Else
                Menu1.OpcionMenu = "Procesos"
                Menu1.OpcionSubMenu = "Seguimiento"
        End Select

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ImposibleAccion

        'Textos de idiomas:
        Me.lblDescr.Text = Textos(1)
        Me.lblId.Text = Textos(2)
        Me.lblPet.Text = Textos(3)

        'Carga los datos de la instancia:
        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        oInstancia.ID = Request("Instancia")
        oInstancia.Load(Idioma, True)

        'Datos de la instancia:
        Select Case Request("Accion")
            Case 1, 2
                Me.lblTitulo.Text = Textos(7)  'Rechazo de solicitud
            Case 3, 4
                Me.lblTitulo.Text = Textos(11)  'Traslado de solicitud
            Case 5, 6
                Me.lblTitulo.Text = Textos(13)  'Devoluci�n de solicitud
            Case 7, 8
                Me.lblTitulo.Text = Textos(15)  'Aprobaci�n de solicitud
            Case 9, 10
                Me.lblTitulo.Text = Textos(18) 'Anulaci�n de solicitud
            Case 11
                Me.lblTitulo.Text = Textos(21)  'Emisi�n de solicitud
            Case 12
                Me.lblTitulo.Text = Textos(25)  'Reemisi�n de solicitud
            Case 13
                Me.lblTitulo.Text = Textos(27) 'Guardar solicitud
            Case 14
                Me.lblTitulo.Text = Textos(29) 'Emisi�n de no conformidad
            Case 15
                Me.lblTitulo.Text = Textos(31) 'Guardar no conformidad
        End Select

        If Not oInstancia.Solicitud Is Nothing Then
            Me.lblTitulo.Text = lblTitulo.Text & " " & oInstancia.Solicitud.Codigo + "-" + oInstancia.Solicitud.Den(Idioma)
        End If

        Me.lblFecha.Text = Textos(0) & " " & FormatDate(oInstancia.FechaAlta, FSNUser.DateFormat)
        If Not oInstancia.CampoImporte Is Nothing Then
            Me.lblImporte.Text = oInstancia.CampoImporte & ":" & FSNLibrary.FormatNumber(oInstancia.Importe, FSNUser.NumberFormat) & " " & oInstancia.Moneda
        Else
            If Not oInstancia.Solicitud Is Nothing Then
                If oInstancia.Solicitud.Tipo = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras Then
                    Me.lblImporte.Text = Textos(4) & " " & FSNLibrary.FormatNumber(oInstancia.Importe, FSNUser.NumberFormat) & " " & oInstancia.Moneda  'Importe
                Else
                    Me.lblImporte.Visible = False
                End If
            End If
        End If
        Me.lblIdBD.Text = oInstancia.ID
        Me.lblPetBD.Text = oInstancia.NombrePeticionario
        Me.lblDescrBD.Text = oInstancia.Den(Idioma)


        Select Case Request("Accion")
            Case 1  'Ha intentando rechazar pero ya hab�a sido anulada
                Me.lblMensaje.Text = Textos(8) & "&nbsp;" & Textos(6)
                lblMensaje.Text = lblMensaje.Text & "&nbsp;" & Textos(5) & "&nbsp;" & oInstancia.NombrePeticionario
                lblMensaje.Text = lblMensaje.Text & "&nbsp;&nbsp;&nbsp;" & Textos(0) & "&nbsp;" & FormatDate(Now, FSNUser.DateFormat)

            Case 2 'Ha intentando rechazar pero ya hab�a sido rechazada
                Me.lblMensaje.Text = Textos(8) & "&nbsp;" & Textos(9)

            Case 3
                Me.lblMensaje.Text = Textos(12) & "&nbsp;" & Textos(6)
                lblMensaje.Text = lblMensaje.Text & "&nbsp;" & Textos(5) & "&nbsp;" & oInstancia.NombrePeticionario
                lblMensaje.Text = lblMensaje.Text & "&nbsp;&nbsp;&nbsp;" & Textos(0) & "&nbsp;" & FormatDate(Now, FSNUser.DateFormat)

            Case 4
                Me.lblMensaje.Text = Textos(12) & "&nbsp;" & Textos(9)
                If oInstancia.Trasladada = True Then
                    lblMensaje.Text = lblMensaje.Text & Textos(10) & "&nbsp;" & oInstancia.NombreDestinatario
                Else
                    If oInstancia.AprobadorActual = Nothing Then
                        lblMensaje.Text = lblMensaje.Text & Textos(10) & "&nbsp;" & oInstancia.UnidadYDep
                    Else
                        lblMensaje.Text = lblMensaje.Text & Textos(10) & "&nbsp;" & oInstancia.NombreAprobadorActual
                    End If
                End If

            Case 5
                Me.lblMensaje.Text = Textos(14) & "&nbsp;" & Textos(6)
                lblMensaje.Text = lblMensaje.Text & "&nbsp;" & Textos(5) & "&nbsp;" & oInstancia.NombrePeticionario
                lblMensaje.Text = lblMensaje.Text & "&nbsp;&nbsp;&nbsp;" & Textos(0) & "&nbsp;" & FormatDate(Now, FSNUser.DateFormat)

            Case 6
                Me.lblMensaje.Text = Textos(14) & "&nbsp;" & Textos(9)
                If oInstancia.Trasladada = True Then
                    lblMensaje.Text = lblMensaje.Text & Textos(10) & "&nbsp;" & oInstancia.NombreDestinatario
                Else
                    If oInstancia.AprobadorActual = Nothing Then
                        lblMensaje.Text = lblMensaje.Text & Textos(10) & "&nbsp;" & oInstancia.UnidadYDep
                    Else
                        lblMensaje.Text = lblMensaje.Text & Textos(10) & "&nbsp;" & oInstancia.NombreAprobadorActual
                    End If
                End If

            Case 7
                Me.lblMensaje.Text = Textos(16) & "&nbsp;" & Textos(6)
                lblMensaje.Text = lblMensaje.Text & "&nbsp;" & Textos(5) & "&nbsp;" & oInstancia.NombrePeticionario
                lblMensaje.Text = lblMensaje.Text & "&nbsp;&nbsp;&nbsp;" & Textos(0) & "&nbsp;" & FormatDate(Now, FSNUser.DateFormat)

            Case 8
                If Request("Compras") = 1 Then
                    Me.lblMensaje.Text = Textos(16) & "&nbsp;" & Textos(17)
                Else
                    Me.lblMensaje.Text = Textos(16) & "&nbsp;" & Textos(9)
                    If oInstancia.AprobadorActual = Nothing Then
                        lblMensaje.Text = lblMensaje.Text & Textos(10) & "&nbsp;" & oInstancia.UnidadYDep
                    Else
                        lblMensaje.Text = lblMensaje.Text & Textos(10) & "&nbsp;" & oInstancia.NombreAprobadorActual
                    End If
                End If

            Case 9
                Me.lblMensaje.Text = Textos(19) & "&nbsp;" & Textos(6)
                lblMensaje.Text = lblMensaje.Text & "&nbsp;" & Textos(5) & "&nbsp;" & oInstancia.NombrePeticionario
                lblMensaje.Text = lblMensaje.Text & "&nbsp;&nbsp;&nbsp;" & Textos(0) & "&nbsp;" & FormatDate(Now, FSNUser.DateFormat)

            Case 10
                Me.lblMensaje.Text = Textos(19) & "&nbsp;" & Textos(20)

            Case 11
                Me.lblMensaje.Text = Textos(22)

            Case 12
                Me.lblMensaje.Text = Textos(26)

            Case 13
                Me.lblMensaje.Text = Textos(28)  'La solicitud no ha podido ser guardada.
            Case 14
                Me.lblMensaje.Text = Textos(30)   'La no conformidad no ha podido ser emitida.

            Case 15
                Me.lblMensaje.Text = Textos(32)  'La no conformidad no ha podido ser guardada.
        End Select


        oInstancia = Nothing
    End Sub

    End Class

