Public Class avisosprecondiciones
    Inherits FSNPage
    Private m_sMensaje As String
    Private m_sBloqueante As String


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents uwgAvisos As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
    Protected WithEvents tblOpciones As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' <summary>
    ''' Carga de la pagina
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: guardarinstancia.aspx.</remarks>    
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oItem As System.Collections.Specialized.NameValueCollection
        Dim loop1 As Integer
        Dim arr1() As String
        Dim sRequest As String
        Dim arrAviso() As String

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.AvisosPrecondiciones

        lblTitulo.Text = Textos(0)
        m_sMensaje = Textos(3)
        m_sBloqueante = Textos(4)

        Dim dtAvisos As DataTable = New DataTable
        Dim newRow As DataRow

        dtAvisos.Columns.Add("TIPO", System.Type.GetType("System.Int32"))
        dtAvisos.Columns.Add("TIPO_DEN", System.Type.GetType("System.Int32"))
        dtAvisos.Columns.Add("ID", System.Type.GetType("System.String"))
        dtAvisos.Columns.Add("MENSAJE", System.Type.GetType("System.String"))

        oItem = Request.Form
        arr1 = oItem.AllKeys
        For loop1 = 0 To arr1.GetUpperBound(0)
            sRequest = arr1(loop1).ToString

            arrAviso = oItem(sRequest).Split("|")

            newRow = dtAvisos.NewRow
            newRow.Item("TIPO") = arrAviso(0)
            newRow.Item("ID") = arrAviso(1)
            newRow.Item("MENSAJE") = arrAviso(2)

            dtAvisos.Rows.Add(newRow)
        Next

        uwgAvisos.DataSource = dtAvisos
        uwgAvisos.DataBind()

        'Idiomas en las caption de la grid: 
        uwgAvisos.Bands(0).Columns.FromKey("TIPO_DEN").Header.Caption = Textos(1)
        uwgAvisos.Bands(0).Columns.FromKey("ID").Header.Caption = Textos(2)
        uwgAvisos.Bands(0).Columns.FromKey("MENSAJE").Header.Caption = Textos(3)
    End Sub

    ''' <summary>
    ''' Se oculta la columna Tipo, se establecen los porcentajes de las columnas 
    ''' y la columna Mensaje puede ocupar m�s de una l�nea (si el texto es muy largo).
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: Al cargarse la grid. Tiempo m�ximo: 0 sg.</remarks>
    Private Sub uwgAvisos_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles uwgAvisos.InitializeLayout
        With e.Layout.Bands(0)
            'Oculta las columnas:
            .Columns.FromKey("TIPO").Hidden = True

            .Columns.FromKey("TIPO_DEN").Width = Unit.Percentage(15)
            .Columns.FromKey("ID").Width = Unit.Percentage(10)
            .Columns.FromKey("MENSAJE").Width = Unit.Percentage(75)
            .Columns.FromKey("MENSAJE").CellStyle.Wrap = True
        End With
    End Sub

    ''' <summary>
    ''' Inicializa las lieas del grid
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: Al cargarse la grid. Tiempo m�ximo: 0 sg.</remarks>    
    Private Sub uwgAvisos_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgAvisos.InitializeRow
        If e.Row.Cells.FromKey("TIPO").Value = 2 Then
            e.Row.Cells.FromKey("TIPO_DEN").Value = m_sBloqueante
            e.Row.Style.CssClass = "ugfilatablaRoja" 'La fila se ver� en rojo
        Else
            e.Row.Cells.FromKey("TIPO_DEN").Value = m_sMensaje
            e.Row.Style.CssClass = "ugfilatablaHist" 'La fila se ver� en azul
        End If

    End Sub
End Class

