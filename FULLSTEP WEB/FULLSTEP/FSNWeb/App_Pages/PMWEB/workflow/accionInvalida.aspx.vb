
Public Class accionInvalida
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitId As System.Web.UI.WebControls.Label
    Protected WithEvents lblId As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecha As System.Web.UI.WebControls.Label
    Protected WithEvents lblFechaBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitPeticionario As System.Web.UI.WebControls.Label
    Protected WithEvents lblPeticionario As System.Web.UI.WebControls.Label
    Protected WithEvents tblGeneral As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    Protected WithEvents txtPeticionario As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Menu1 As MenuControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oInstancia As FSNServer.Instancia
        Dim tipoSolicitud As Integer = 0

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.AccionInvalida

        'Textos de idiomas:
        Me.lblLitId.Text = Textos(0)
        Me.lblFecha.Text = Textos(1)
        Me.lblLitPeticionario.Text = Textos(2)
        Me.lblMensaje.Text = Textos(3)

        'Carga los datos de la instancia:
        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        oInstancia.ID = Request("Instancia")
        oInstancia.Cargar(Idioma)

        tipoSolicitud = oInstancia.Solicitud.TipoSolicit
        Select Case tipoSolicitud
            Case TiposDeDatos.TipoDeSolicitud.Factura
                Menu1.OpcionMenu = "Facturacion"
                Menu1.OpcionSubMenu = "Seguimiento"
            Case TiposDeDatos.TipoDeSolicitud.Encuesta
                Menu1.OpcionMenu = "Calidad"
                Menu1.OpcionSubMenu = "Encuestas"
            Case TiposDeDatos.TipoDeSolicitud.SolicitudQA
                Menu1.OpcionMenu = "Calidad"
                Menu1.OpcionSubMenu = "SolicitudesQA"
            Case Else
                Menu1.OpcionMenu = "Procesos"
                Menu1.OpcionSubMenu = "Seguimiento"
        End Select

        'Datos de la instancia:
        Me.lblFechaBD.Text = FormatDate(oInstancia.FechaAlta, FSNUser.DateFormat)
        Me.lblId.Text = oInstancia.ID
        Me.lblPeticionario.Text = oInstancia.NombrePeticionario
        Me.txtPeticionario.Value = oInstancia.Peticionario

        'Carga la acci�n:
        Dim oAccion As FSNServer.Accion
        oAccion = FSNServer.Get_Object(GetType(FSNServer.Accion))
        oAccion.Id = Request("Accion")
        oAccion.CargarAccion(Idioma)

        Me.lblTitulo.Text = Textos(4) & " " & oAccion.Den

        oInstancia = Nothing
    End Sub
End Class

