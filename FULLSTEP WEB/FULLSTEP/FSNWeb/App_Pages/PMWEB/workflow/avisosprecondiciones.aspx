<%@ Register TagPrefix="igtbl" Namespace="Infragistics.WebUI.UltraWebGrid" Assembly="Infragistics.WebUI.UltraWebGrid.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="avisosprecondiciones.aspx.vb" Inherits="Fullstep.FSNWeb.avisosprecondiciones"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	<script language="javascript">
		function resizeOuterTo(w,h) 
		{
			if (parseInt(navigator.appVersion)>3) 
			{
				if (navigator.appName=="Netscape") 
				{
					top.outerWidth=w;
					top.outerHeight=h;
				}
				else top.resizeTo(w,h);
			}
		}
	</script>	
	<body onload="resizeOuterTo(600,400);">
		<form id="Form1" method="post" runat="server">
			<TABLE id="tblOpciones" style="Z-INDEX: 101; LEFT: 10px; POSITION: absolute; TOP: 12px"
				cellSpacing="1" cellPadding="1" width="95%" border="0" runat="server">
				<TR>
					<TD vAlign="middle" colSpan="2"><asp:label id="lblTitulo" runat="server" CssClass="captionBlue">Avisos de precondiciones de acci�n</asp:label></TD>
				</TR>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<igtbl:ultrawebgrid id="uwgAvisos" runat="server" Width="100%">
							<DisplayLayout RowHeightDefault="20px" Version="4.00" BorderCollapseDefault="Separate" RowSelectorsDefault="No"
								Name="uwgAvisos">
								<AddNewBox>
									<Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">

<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
</BorderDetails>

									</Style>
								</AddNewBox>
								<Pager>
									<Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">

<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
</BorderDetails>

									</Style>
								</Pager>
								<HeaderStyleDefault BorderStyle="Solid" CssClass="VisorHead">
									<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
								</HeaderStyleDefault>
								<FrameStyle Width="100%" Font-Size="8pt" Font-Names="Verdana" BorderStyle="None" CssClass="VisorFrame"></FrameStyle>
								<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
									<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
								</FooterStyleDefault>
								<ActivationObject BorderStyle="None" BorderWidth="" BorderColor=""></ActivationObject>
								<FixedHeaderStyleDefault CssClass="VisorHead"></FixedHeaderStyleDefault>
								<EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
								<SelectedRowStyleDefault BorderStyle="None"></SelectedRowStyleDefault>
								<RowStyleDefault BorderWidth="1px" BorderStyle="Solid" CssClass="VisorRow">
									<Padding Left="3px"></Padding>
									<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
								</RowStyleDefault>
							</DisplayLayout>
							<Bands>
								<igtbl:UltraGridBand></igtbl:UltraGridBand>
							</Bands>
						</igtbl:ultrawebgrid>
					</td>
				</tr>
			</TABLE>
		</form>
	</body>
</html>
