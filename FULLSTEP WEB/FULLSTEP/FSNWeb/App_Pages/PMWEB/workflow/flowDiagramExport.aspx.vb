Imports Syncfusion.Windows.Forms.diagram

Public Class flowDiagramExport
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dwc As Syncfusion.Web.UI.WebControls.Diagram.DiagramWebControl
    Protected WithEvents cmdBMP As System.Web.UI.WebControls.Button
    Protected WithEvents cmdPDF As System.Web.UI.WebControls.Button
    Protected WithEvents Instancia As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cmdGIF As System.Web.UI.WebControls.Button
    Protected WithEvents xmlPres As System.Web.UI.WebControls.Xml

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' <summary>
    ''' Carga los bloques y los enlaces del flujo para realizar el diagrama
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: Al cargarse la p�gina; Tiempo m�ximo: 1 sg</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Instancia.Value = Request("Instancia")

        If Not Page.IsPostBack() Then
            Exit Sub
        End If

        ' En primer lugar ubicaremos los bloques

        Dim oDiagBloques As FSNServer.DiagBloques

        oDiagBloques = FSNServer.Get_Object(GetType(FSNServer.DiagBloques))

        Dim oDiagWebBloques() As CDiagWebBloque
        Dim iIndice As Integer = -1
        Dim oColor As System.Drawing.Color

        oDiagBloques.LoadData(Me.Instancia.Value, Idioma)

        Dim oRow As DataRow

        For Each oRow In oDiagBloques.Data.Tables(0).Rows

            If Not IsDBNull(oRow.Item("ESTADO")) Then
                If oRow.Item("ESTADO") = 1 Then
                    oColor = System.Drawing.Color.FromArgb(255, 192, 128)
                Else
                    If oRow.Item("ESTADO") = 2 Then
                        oColor = System.Drawing.Color.LightGreen
                    Else
                        oColor = System.Drawing.Color.White
                    End If
                End If
            Else
                oColor = System.Drawing.Color.White
            End If

            iIndice = iIndice + 1

            ' Adaptaci�n tama�o diagrama

            If oRow.Item("TOP") / CDiagWebBloque.tppY + oRow.Item("HEIGHT") / CDiagWebBloque.tppY >= dwc.Model.Height Then

                dwc.Model.Height = oRow.Item("TOP") / CDiagWebBloque.tppY + oRow.Item("HEIGHT") / CDiagWebBloque.tppY + 100

            End If

            If oRow.Item("LEFT") / CDiagWebBloque.tppX + oRow.Item("WIDTH") / CDiagWebBloque.tppX >= dwc.Model.Width Then

                dwc.Model.Width = oRow.Item("LEFT") / CDiagWebBloque.tppX + oRow.Item("WIDTH") / CDiagWebBloque.tppX + 100

            End If

            ReDim Preserve oDiagWebBloques(iIndice)

            oDiagWebBloques(iIndice) = New CDiagWebBloque(oRow.Item("ID"), oRow.Item("LEFT"), oRow.Item("TOP"), oRow.Item("WIDTH"), oRow.Item("HEIGHT"), oRow.Item("DEN"), oColor)

            dwc.Model.AppendChild(oDiagWebBloques(iIndice))

        Next

        ' Ahora ubicamos los enlaces

        Dim i As Long
        Dim iDestino As Long

        Dim oRowEp As DataRow
        Dim Extrapoints() As Drawing.PointF
        Dim iExtrapoints As Integer
        Dim oDashStyle As Drawing.Drawing2D.DashStyle
        Dim oLineColor As System.Drawing.Color

        For i = 0 To oDiagWebBloques.Length - 1

            Dim oDiagEnlaces As FSNServer.DiagEnlaces

            oDiagEnlaces = FSNServer.Get_Object(GetType(FSNServer.DiagEnlaces))

            oDiagEnlaces.LoadData(oDiagWebBloques(i).Id, Idioma)

            For Each oRow In oDiagEnlaces.Data.Tables(0).Rows

                ' Localizar nodo destino
                iDestino = LocalizarBloqueDestino(oRow.Item("BLOQUE_DESTINO"), oDiagWebBloques)

                ' Preparar array con puntos intermedios
                Dim dsExtrapoints As New DataSet
                dsExtrapoints.Merge(oRow.GetChildRows("REL_ENLACE_EXTRAPOINTS"))

                iExtrapoints = -1

                If dsExtrapoints.Tables.Count > 0 Then

                    For Each oRowEp In dsExtrapoints.Tables(0).Rows

                        iExtrapoints = iExtrapoints + 1
                        ReDim Preserve Extrapoints(iExtrapoints)

                        Extrapoints(iExtrapoints) = New Drawing.PointF(oRowEp.Item("X"), oRowEp.Item("Y"))

                    Next

                End If

                If oRow.Item("FORMULA").ToString <> "" Then 'Condicional
                    oDashStyle = Drawing.Drawing2D.DashStyle.Dash
                Else
                    oDashStyle = Drawing.Drawing2D.DashStyle.Solid
                End If

                If CInt(oRow.Item("TIPO")) = 2 Then 'Rechazo
                    oLineColor = System.Drawing.Color.Red
                Else
                    oLineColor = System.Drawing.Color.DarkGray
                End If

                ' Crear enlace

                Dim oDiagWebEnlace As CDiagWebEnlace

                If iExtrapoints >= 0 Then
                    oDiagWebEnlace = New CDiagWebEnlace(oDiagWebBloques(i), oDiagWebBloques(iDestino), oRow.Item("DEN"), oDashStyle, oLineColor, Extrapoints)
                Else
                    oDiagWebEnlace = New CDiagWebEnlace(oDiagWebBloques(i), oDiagWebBloques(iDestino), oRow.Item("DEN"), oDashStyle, oLineColor)
                End If

                dwc.Model.AppendChild(oDiagWebEnlace)

                oDiagWebEnlace = Nothing

                dsExtrapoints = Nothing


            Next

            oDiagEnlaces = Nothing

        Next

    End Sub
    Private Function LocalizarBloqueDestino(ByVal lId As Long, ByVal oDiagWebBloques() As CDiagWebBloque) As Long

        ' B�squeda del destino (burbuja)

        Dim c As Long = oDiagWebBloques.Length
        Dim istart, iend As Long

        LocalizarBloqueDestino = -1

        If c = 0 Then
            Exit Function
        End If

        If c = 1 Then
            If oDiagWebBloques(0).Id = lId Then
                LocalizarBloqueDestino = 0
                Exit Function
            Else
                Exit Function
            End If
        End If

        istart = 0
        iend = c - 1

        While LocalizarBloqueDestino = -1

            If oDiagWebBloques((istart + iend) \ 2).Id = lId Then

                LocalizarBloqueDestino = (istart + iend) \ 2
                Exit Function

            Else

                If oDiagWebBloques((istart + iend) \ 2).Id < lId Then
                    istart = ((istart + iend) \ 2) + 1
                Else
                    iend = ((istart + iend) \ 2) - 1
                End If

                If istart < 0 Or iend > (c - 1) Then

                    LocalizarBloqueDestino = -1
                    Exit Function

                End If

            End If

        End While

    End Function

    Private Sub cmdBMP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBMP.Click

        Dim bmp As Drawing.Bitmap = Me.dwc.View.ExportDiagramAsImage()

        Dim sPath As String
        Dim sBytes As Long

        sPath = ConfigurationManager.AppSettings("temp") + "\" + GenerateRandomPath()

        Dim oFolder As System.IO.Directory

        If Not oFolder.Exists(sPath) Then
            oFolder.CreateDirectory(sPath)
        End If

        sPath += "\" + CStr(Me.Instancia.Value) + ".bmp"

        bmp.Save(sPath, System.Drawing.Imaging.ImageFormat.Bmp)

        Dim ofile As New System.IO.FileInfo(sPath)

        sBytes = ofile.Length

        Dim arrPath() As String = sPath.Split("\")
        Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "_Common/download.aspx?path=" + arrPath(UBound(arrPath) - 1) + "&nombre=" + Server.UrlEncode(arrPath(UBound(arrPath))) + "&datasize=" + CStr(sBytes))

    End Sub

    Private Sub cmdPDF_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPDF.Click

        ' Guardamos la imagen en GIF

        Dim bmp As Drawing.Bitmap = Me.dwc.View.ExportDiagramAsImage()

        Dim sPathBase As String
        Dim sPathGIF As String
        Dim sPathHTML As String
        Dim sPathPDF As String
        Dim sBytes As Long

        sPathBase = ConfigurationManager.AppSettings("temp") + "\" + GenerateRandomPath()

        Dim oFolder As System.IO.Directory

        If Not oFolder.Exists(sPathBase) Then
            oFolder.CreateDirectory(sPathBase)
        End If

        sPathGIF = sPathBase + "\" + CStr(Me.Instancia.Value) + ".gif"

        bmp.Save(sPathGIF, System.Drawing.Imaging.ImageFormat.Gif)
        bmp.Dispose()
        bmp = Nothing


        ' Escribimos un HTML con la imagen

        Dim oStringWriter As New System.IO.StringWriter

        Dim oHtmlTextWriter As New System.Web.UI.HtmlTextWriter(oStringWriter)

        oHtmlTextWriter.WriteLine("<HTML>")
        oHtmlTextWriter.WriteLine("<HEAD>")
        oHtmlTextWriter.WriteLine("<meta http-equiv=""Content-Type"" content=""text/html; charset=windows-1252"">")
        oHtmlTextWriter.WriteLine("</HEAD>")
        oHtmlTextWriter.WriteLine("<BODY>")

        oHtmlTextWriter.WriteLine("<IMG SRC=""" & CStr(Me.Instancia.Value) & ".gif" & """>")

        oHtmlTextWriter.WriteLine("</BODY>")
        oHtmlTextWriter.WriteLine("</HTML>")

        ' Guardamos el HTML en un archivo

        sPathHTML = sPathBase + "\" + CStr(Me.Instancia.Value) + ".html"
        sPathPDF = sPathBase + "\" + CStr(Me.Instancia.Value) + ".pdf"

        Dim oStreamWriter As New System.IO.StreamWriter(sPathHTML, False, System.Text.Encoding.UTF8)
        oStreamWriter.Write(oStringWriter.ToString)
        oStreamWriter.Close()

        ''''''' Pasamos el HTML a PDF
        ConvertHTMLToPDF(sPathHTML, sPathPDF)

        ''''''' Ponemos el PDF a descargar
        Dim ofile As New System.IO.FileInfo(sPathPDF)
        sBytes = ofile.Length
        Dim arrPath2() As String = sPathPDF.Split("\")
        Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "_Common/download.aspx?path=" + arrPath2(UBound(arrPath2) - 1) + "&nombre=" + Server.UrlEncode(arrPath2(UBound(arrPath2))) + "&datasize=" + CStr(sBytes))
    End Sub

    Private Sub cmdGIF_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGIF.Click

        Dim bmp As Drawing.Bitmap = Me.dwc.View.ExportDiagramAsImage()

        Dim sPath As String
        Dim sBytes As Long

        sPath = ConfigurationManager.AppSettings("temp") + "\" + GenerateRandomPath()

        Dim oFolder As System.IO.Directory

        If Not oFolder.Exists(sPath) Then
            oFolder.CreateDirectory(sPath)
        End If

        sPath += "\" + CStr(Me.Instancia.Value) + ".gif"

        bmp.Save(sPath, System.Drawing.Imaging.ImageFormat.Gif)

        Dim ofile As New System.IO.FileInfo(sPath)

        sBytes = ofile.Length

        Dim arrPath() As String = sPath.Split("\")
        Response.Redirect("../_common/download.aspx?path=" + arrPath(UBound(arrPath) - 1) + "&nombre=" + Server.UrlEncode(arrPath(UBound(arrPath))) + "&datasize=" + CStr(sBytes))

    End Sub

End Class


