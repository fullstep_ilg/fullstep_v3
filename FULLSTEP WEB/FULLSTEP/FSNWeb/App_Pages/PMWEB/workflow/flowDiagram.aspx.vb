Imports Syncfusion.Windows.Forms.Diagram


Public Class flowDiagram

    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dwc As Syncfusion.Web.UI.WebControls.Diagram.DiagramWebControl
    Protected WithEvents lblBDFecha As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecha As System.Web.UI.WebControls.Label
    Protected WithEvents lblBDPeticionario As System.Web.UI.WebControls.Label
    Protected WithEvents lblPeticionario As System.Web.UI.WebControls.Label
    Protected WithEvents lblBDId As System.Web.UI.WebControls.Label
    Protected WithEvents lblId As System.Web.UI.WebControls.Label
    Protected WithEvents lblBDTipo As System.Web.UI.WebControls.Label
    Protected WithEvents lblTipo As System.Web.UI.WebControls.Label
    Protected WithEvents Instancia As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Version As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtPeticionario As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtPath As System.Web.UI.WebControls.TextBox
    Protected WithEvents cmdImpExp As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cmdBuscarPet As System.Web.UI.HtmlControls.HtmlAnchor

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    ''' <summary>
    ''' Carga los bloques y los enlaces del flujo para realizar el diagrama
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: Al cargarse la p�gina; Tiempo m�ximo: 1 sg</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim sCodContrato As String = String.Empty
        Dim sNumFactura As String = String.Empty
        ' Poner textos
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.flowDiagram

        If Request("Codigo") <> Nothing Then sCodContrato = Request("Codigo")
        If Request("NumFactura") <> Nothing Then sNumFactura = Request("NumFactura")

        Me.lblTipo.Text = Textos(1)
        Me.lblPeticionario.Text = Textos(2)
        Me.lblFecha.Text = Textos(3)
        Me.cmdImpExp.Value = Textos(4)

        ' Datos de la instancia
        Dim oInstancia As FSNServer.Instancia
        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        oInstancia.ID = Request("Instancia")
        oInstancia.Cargar(Idioma)

        Me.Instancia.Value = oInstancia.ID
        Me.Version.Value = oInstancia.Version
        Me.lblBDFecha.Text = FormatDate(oInstancia.FechaAlta, FSNUser.DateFormat)
        Me.lblBDPeticionario.Text = oInstancia.NombrePeticionario
        Me.lblBDTipo.Text = oInstancia.Solicitud.Codigo + "-" + oInstancia.Solicitud.Den(Idioma)

        If sCodContrato <> String.Empty Then
            Me.lblId.Text = Textos(5)
            Me.lblBDId.Text = sCodContrato
        ElseIf sNumFactura <> String.Empty Then
            Me.lblId.Text = Textos(6) & ":"
            Me.lblBDId.Text = sNumFactura
        Else
            Me.lblId.Text = Textos(0)
            Me.lblBDId.Text = oInstancia.ID
        End If
        Me.txtPeticionario.Value = oInstancia.Peticionario

        Dim dsPermisosRol As DataSet
        dsPermisosRol = FSNUser.VerDetallesPersona(oInstancia.ID)
        If dsPermisosRol.Tables.Count > 0 Then
            If dsPermisosRol.Tables(0).Rows(0).Item("VER_DETALLE_PER") = 0 Then
                Me.cmdBuscarPet.Visible = False
                Me.lblBDPeticionario.Visible = False
                Me.lblPeticionario.Visible = False
            End If
        End If

        ' En primer lugar ubicaremos los bloques
        Dim oDiagBloques As FSNServer.DiagBloques
        oDiagBloques = FSNServer.Get_Object(GetType(FSNServer.DiagBloques))

        Dim oDiagWebBloques() As CDiagWebBloque
        Dim iIndice As Integer = -1
        Dim oColor As System.Drawing.Color

        oDiagBloques.LoadData(oInstancia.ID, Idioma)

        For Each oRow As DataRow In oDiagBloques.Data.Tables(0).Rows
            If Not IsDBNull(oRow.Item("ESTADO")) Then
                If oRow.Item("ESTADO") = 1 Then
                    oColor = System.Drawing.Color.FromArgb(255, 192, 128)
                Else
                    If oRow.Item("ESTADO") = 2 Or oRow.Item("ESTADO") = 3 Then
                        oColor = System.Drawing.Color.LightGreen
                    Else
                        oColor = System.Drawing.Color.White
                    End If
                End If
            Else
                oColor = System.Drawing.Color.White
            End If

            iIndice = iIndice + 1

            ' Adaptaci�n tama�o diagrama
            If oRow.Item("TOP") / CDiagWebBloque.tppY + oRow.Item("HEIGHT") / CDiagWebBloque.tppY >= dwc.Model.Height Then
                dwc.Model.Height = oRow.Item("TOP") / CDiagWebBloque.tppY + oRow.Item("HEIGHT") / CDiagWebBloque.tppY + 100
            End If

            If oRow.Item("LEFT") / CDiagWebBloque.tppX + oRow.Item("WIDTH") / CDiagWebBloque.tppX >= dwc.Model.Width Then
                dwc.Model.Width = oRow.Item("LEFT") / CDiagWebBloque.tppX + oRow.Item("WIDTH") / CDiagWebBloque.tppX + 100
            End If

            ReDim Preserve oDiagWebBloques(iIndice)
            oDiagWebBloques(iIndice) = New CDiagWebBloque(oRow.Item("ID"), oRow.Item("LEFT"), oRow.Item("TOP"), oRow.Item("WIDTH"), oRow.Item("HEIGHT"), DBNullToStr(oRow.Item("DEN")), oColor)
            dwc.Model.AppendChild(oDiagWebBloques(iIndice))
        Next

        ' Ahora ubicamos los enlaces

        Dim i As Long
        Dim iDestino As Long
        Dim oRowEp As DataRow
        Dim Extrapoints() As Drawing.PointF
        Dim iExtrapoints As Integer
        Dim oDashStyle As Drawing.Drawing2D.DashStyle
        Dim oLineColor As System.Drawing.Color

        For i = 0 To oDiagWebBloques.Length - 1

            Dim oDiagEnlaces As FSNServer.DiagEnlaces
            oDiagEnlaces = FSNServer.Get_Object(GetType(FSNServer.DiagEnlaces))
            oDiagEnlaces.LoadData(oDiagWebBloques(i).Id, Idioma)

            For Each oRow As DataRow In oDiagEnlaces.Data.Tables(0).Rows

                ' Localizar nodo destino
                iDestino = LocalizarBloqueDestino(oRow.Item("BLOQUE_DESTINO"), oDiagWebBloques)

                ' Preparar array con puntos intermedios
                Dim dsExtrapoints As New DataSet
                dsExtrapoints.Merge(oRow.GetChildRows("REL_ENLACE_EXTRAPOINTS"))

                iExtrapoints = -1

                If dsExtrapoints.Tables.Count > 0 Then
                    For Each oRowEp In dsExtrapoints.Tables(0).Rows
                        iExtrapoints = iExtrapoints + 1
                        ReDim Preserve Extrapoints(iExtrapoints)

                        Extrapoints(iExtrapoints) = New Drawing.PointF(oRowEp.Item("X"), oRowEp.Item("Y"))
                    Next
                End If

                If oRow.Item("FORMULA").ToString <> "" Then 'Condicional
                    oDashStyle = Drawing.Drawing2D.DashStyle.Dash
                Else
                    oDashStyle = Drawing.Drawing2D.DashStyle.Solid
                End If

                If CInt(oRow.Item("TIPO")) = 2 Then 'Rechazo
                    oLineColor = System.Drawing.Color.Red
                ElseIf CInt(DBNullToInteger(oRow.Item("LLAMADA_EXTERNA"))) > 0 Then 'realiza una llamada externa
                    oLineColor = System.Drawing.Color.Blue
                Else
                    oLineColor = System.Drawing.Color.DarkGray
                End If

                ' Crear enlace
                Dim oDiagWebEnlace As CDiagWebEnlace
                If iExtrapoints >= 0 Then
                    oDiagWebEnlace = New CDiagWebEnlace(oDiagWebBloques(i), oDiagWebBloques(iDestino), DBNullToStr(oRow.Item("DEN")), oDashStyle, oLineColor, Extrapoints)
                Else
                    oDiagWebEnlace = New CDiagWebEnlace(oDiagWebBloques(i), oDiagWebBloques(iDestino), DBNullToStr(oRow.Item("DEN")), oDashStyle, oLineColor)
                End If

                dwc.Model.AppendChild(oDiagWebEnlace)

                oDiagWebEnlace = Nothing
                dsExtrapoints = Nothing
            Next
            oDiagEnlaces = Nothing
        Next

    End Sub

    Private Function LocalizarBloqueDestino(ByVal lId As Long, ByVal oDiagWebBloques() As CDiagWebBloque) As Long

        ' B�squeda del destino (burbuja)

        Dim c As Long = oDiagWebBloques.Length
        Dim istart, iend As Long

        LocalizarBloqueDestino = -1

        If c = 0 Then
            Exit Function
        End If

        If c = 1 Then
            If oDiagWebBloques(0).Id = lId Then
                LocalizarBloqueDestino = 0
                Exit Function
            Else
                Exit Function
            End If
        End If

        istart = 0
        iend = c - 1

        While LocalizarBloqueDestino = -1
            If oDiagWebBloques((istart + iend) \ 2).Id = lId Then

                LocalizarBloqueDestino = (istart + iend) \ 2
                Exit Function

            Else

                If oDiagWebBloques((istart + iend) \ 2).Id < lId Then
                    istart = ((istart + iend) \ 2) + 1
                Else
                    iend = ((istart + iend) \ 2) - 1
                End If

                If istart < 0 Or iend > (c - 1) Then

                    LocalizarBloqueDestino = -1
                    Exit Function

                End If
            End If
        End While
    End Function

End Class


