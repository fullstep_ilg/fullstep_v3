﻿<%@ Register Assembly="Infragistics.WebUI.UltraWebTab.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="gestiontrasladada.aspx.vb" Inherits="Fullstep.FSNWeb.gestiontrasladada" EnableViewState="False" %>
<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>gestiontrasladada</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
</head>

<script type="text/javascript">
    wProgreso = null;
    //Muestra el contenido peligroso que ha efectuado un error al guardar.
    function ErrorValidacion(contenido) {
        alert(arrTextosML[0].replace("$$$", contenido));
        OcultarEspera();
    }
    //Le pone un ancho al desglose
    function resize() {
        for (i = 0; i < arrDesgloses.length; i++) {
            sDiv = arrDesgloses[i].replace("tblDesglose", "divDesglose");
            if (document.getElementById(sDiv));
            document.getElementById(sDiv).style.width = parseFloat(document.body.offsetWidth) - 95 + 'px';
        }
    }
    function localEval(s) {
        eval(s);
    }
    function DetalleTipoSolicitud() {
        var newWindow = window.open("../alta/solicitudPMWEB.aspx?Solicitud=" + document.forms["frmDetalle"].elements["txtIdTipo"].value, "_blank", "width=700,height=450,status=no,resizable=no,top=100,left=100");
        newWindow.focus();
    }
    function MostrarEspera() {
        wProgreso = true;

        $("[id*='lnkBoton']").attr('disabled', 'disabled');

        document.getElementById("divForm2").style.display = 'none';
        document.getElementById("divForm3").style.display = 'none';
        document.getElementById("igtabuwtGrupos").style.display = 'none';

        var i = 0;
        var bSalir = false;
        while (bSalir == false) {
            if (document.getElementById("uwtGrupos_div" + i)) {
                document.getElementById("uwtGrupos_div" + i).style.visibility = 'hidden';
                i = i + 1;
            } else
                bSalir = true;
        }
        document.getElementById("lblProgreso").value = document.forms["frmDetalle"].elements["cadenaespera"].value;
        document.getElementById("divProgreso").style.display = 'inline';
    }

    //Estaba habilitando los botones antes de ocultar el progreso. Parecia q te dejaba dar a varios botones mientras estaba en progreso.
    function DarTiempoAOcultarProgreso() {
        $("[id*='lnkBoton']").removeAttr('disabled');
    }

    function OcultarEspera() {
        wProgreso = null;

        document.getElementById("divProgreso").style.display = 'none';
        document.getElementById("divForm2").style.display = 'inline';
        document.getElementById("divForm3").style.display = 'inline';
        document.getElementById("igtabuwtGrupos").style.display = '';

        setTimeout('DarTiempoAOcultarProgreso()', 250);

        i = 0;
        bSalir = false;
        while (bSalir == false) {
            if (document.getElementById("uwtGrupos_div" + i)) {
                document.getElementById("uwtGrupos_div" + i).style.visibility = 'visible';
                i = i + 1;
            } else
                bSalir = true;
        }
        return;
    }
    function MontarSubmitGuardar() {
        MontarFormularioSubmit(true, true, false, false);

        var frmSubmitElements = document.forms["frmSubmit"].elements;
        frmSubmitElements["GEN_AccionRol"].value = 0;
        frmSubmitElements["GEN_Bloque"].value = document.forms["frmDetalle"].elements["Bloque"].value;
        frmSubmitElements["GEN_Rol"].value = oRol_Id;
        frmSubmitElements["GEN_Enviar"].value = 0;
        frmSubmitElements["GEN_Accion"].value = "guardarsolicitud";
        frmSubmitElements["PantallaMaper"].value = 0;

        oFrm = MontarFormularioCalculados();
        sInner = oFrm.innerHTML;
        oFrm.innerHTML = "";
        document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner);
        document.forms["frmSubmit"].submit();
    }
    function Guardar() {
        MostrarEspera();
        setTimeout("MontarSubmitGuardar()", 100);
        return false;
    }
    function HabilitarBotones() {
        OcultarEspera();
    }
    function MontarSubmitTrasladar() {
        MontarFormularioSubmit(true, true, false, false)

        var frmSubmitElements = document.forms["frmSubmit"].elements;
        frmSubmitElements["GEN_Enviar"].value = 0;
        frmSubmitElements["GEN_Accion"].value = "trasladadatrasladarinstancia";
        frmSubmitElements["GEN_AccionRol"].value = 0;
        frmSubmitElements["GEN_Bloque"].value = document.forms["frmDetalle"].elements["Bloque"].value;
        frmSubmitElements["GEN_Rol"].value = oRol_Id;
        frmSubmitElements["PantallaMaper"].value = 0;

        oFrm = MontarFormularioCalculados()
        sInner = oFrm.innerHTML
        oFrm.innerHTML = ""
        document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner)
        document.forms["frmSubmit"].submit()
    }
    function Trasladar() {
        MostrarEspera();
        setTimeout("MontarSubmitTrasladar()", 100);
        return false;
    }
    function MontarSubmitDevolver() {
        MontarFormularioSubmit(true, true, false, false);

        var frmSubmitElements = document.forms["frmSubmit"].elements;
        frmSubmitElements["GEN_AccionRol"].value = 0;
        frmSubmitElements["GEN_Bloque"].value = document.forms["frmDetalle"].elements["Bloque"].value;
        frmSubmitElements["GEN_Rol"].value = oRol_Id;
        frmSubmitElements["GEN_Enviar"].value = 0;
        frmSubmitElements["GEN_Accion"].value = "trasladadadevolverinstancia";
        frmSubmitElements["PantallaMaper"].value = 0;

        oFrm = MontarFormularioCalculados();
        sInner = oFrm.innerHTML;
        oFrm.innerHTML = "";
        document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner);
        document.forms["frmSubmit"].submit();
    }
    function Devolver() {
        MostrarEspera();
        setTimeout("MontarSubmitDevolver()", 100);
        return false;
    }
    //Función que se ejecuta cuando se pulsa el botón Calcular y realiza el cálculo sobre los campos calculados.
    function CalcularCamposCalculados() {
        oFrm = MontarFormularioCalculados();
        oFrm.submit();
        return false;
    }
    function init() {
        resize();
    }
    function initTab(webTab) {
        var cp = document.getElementById(webTab.ID + '_cp');
        cp.style.minHeight = '300px';
    }
    function cmdImpExp_onclick() {
        window.open('../seguimiento/impexp_sel.aspx?Instancia=' + document.getElementById("Instancia").value + '&TipoImpExp=1', '_new', 'fullscreen=no,height=115,width=315,location=no,menubar=no,resizable=no,scrollbars=no,status=yes,titlebar=yes,toolbar=no,left=200,top=200');
        return false;
    }
</script>
<body onresize="resize()" onload="init()" onunload="if (wProgreso != null) wProgreso=null;">
    <form id="frmDetalle" method="post" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <CompositeScript>
                <Scripts>
                    <asp:ScriptReference Name="ExtenderBase.BaseScripts.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Common.Common.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Compat.Timer.Timer.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Animation.Animations.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Animation.AnimationBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="PopupExtender.PopupBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="AutoComplete.AutoCompleteBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Path="../alta/js/AdjacentHTML.js" />
                    <asp:ScriptReference Path="../alta/js/jsAlta.js" />
                </Scripts>
            </CompositeScript>
        </asp:ScriptManager>
        <iframe id="iframeWSServer" style="z-index: 109; left: 8px; visibility: hidden; position: absolute; top: 200px" name="iframeWSServer" src="../blank.htm"></iframe>
        <input id="Bloque" type="hidden" name="Bloque" runat="server" />
        <input id="Instancia" type="hidden" name="Instancia" runat="server" />
        <input id="txtEnviar" type="hidden" name="Enviar" runat="server" />
        <input id="Version" type="hidden" name="Version" runat="server" />
        <input id="txtIdTipo" type="hidden" name="txtIdTipo" runat="server" />
        <input id="hidVolver" type="hidden" runat="server" />
        <input id="cadenaespera" type="hidden" name="cadenaespera" runat="server" />
        <input id="BotonCalcular" type="hidden" value="0" name="BotonCalcular" runat="server" />
        
        <div style="width:100%; position:fixed; z-index:100; background-color:white;">
            <uc1:menu runat="server" ID="mnuMenu" Seccion="Detalle solicitud"></uc1:menu>                
            <fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
        </div> 
        <!------------------------------------------------->
        <div style="padding-left:15px; padding-bottom:15px; padding-top:14em;">
            <asp:Panel ID="pnlCabecera" runat="server" BackColor="#f5f5f5" Font-Names="Arial" Width="99%">
                <table style="height: 15%; width: 100%; padding-bottom: 15px; padding-left: 5px" cellspacing="0" cellpadding="1" border="0">
                    <tr>
                        <td style="padding-top: 5px; padding-bottom: 5px;" class="fondoCabecera">
                            <table style="width: 100%; table-layout: fixed; padding-left: 10px" border="0">
                                <tr>
                                    <td colspan="2"><asp:Label ID="lblIDInstanciayEstado" runat="server" CssClass="label" Font-Bold="true"></asp:Label></td>
                                    <td><asp:Label ID="lblLitImporte" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
                                    <td><asp:Label ID="lblImporte" runat="server" CssClass="label" Font-Bold="true"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td><asp:Label ID="lblLitCreadoPor" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblCreadoPor" runat="server" CssClass="label"></asp:Label>
                                        <asp:Image ID="imgInfCreadoPor" ImageUrl="images/info.gif" runat="server" Style="cursor: pointer" />
                                    </td>
                                    <td><asp:Label ID="lblLitEstado" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
                                    <td><asp:Label ID="lblEstado" runat="server" CssClass="label"></asp:Label></td>
                                    <td><asp:HyperLink ID="HyperDetalle" runat="server" CssClass="CaptionLink"></asp:HyperLink></td>
                                </tr>
                                <tr>
                                    <td><asp:Label ID="lblLitFecAlta" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
                                    <td><asp:Label ID="lblFecAlta" runat="server" CssClass="label"></asp:Label></td>
                                    <td><asp:Label ID="lblLitTipo" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
                                    <td colspan="3">
                                        <asp:Label ID="lblTipo" runat="server" CssClass="label"></asp:Label>
                                        <asp:Image ID="imgInfTipo" ImageUrl="images/info.gif" runat="server" Style="cursor: pointer" />
                                    </td>
                                </tr>
                                <tr>
                                    <td><asp:Label ID="lblLitTrasladadaPor" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblTrasladadaPor" runat="server" CssClass="label"></asp:Label>
                                        <asp:Image ID="imgInfTrasladadaPor" ImageUrl="images/info.gif" runat="server" Style="cursor: pointer" />
                                    </td>
                                    <td><asp:Label ID="lblLitFechaTraslado" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
                                    <td><asp:Label ID="lblFechaTraslado" runat="server" CssClass="label"></asp:Label></td>
                                    <td><asp:Label ID="lblLitFechaDevolucion" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
                                    <td><asp:Label ID="lblFechaDevolucion" runat="server" CssClass="label"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>

        <ajx:DropShadowExtender TrackPosition="true" ID="DropShadowExtender1" runat="server" Opacity="0.5" Width="3" TargetControlID="pnlCabecera" Rounded="true">
        </ajx:DropShadowExtender>
        <!---------------------------------------------------->
        <table id="coment" cellspacing="5" cellpadding="1" width="100%" border="0" height="15%">
            <tr height="25%">
                <td valign="middle" colspan="8" height="100%">
                    <asp:TextBox ID="txtComent" runat="server" Width="100%" Height="100%" ReadOnly="True" TextMode="MultiLine"
                        BackColor="#E0E0E0"></asp:TextBox></td>
            </tr>
        </table>
        <div id="divProgreso" style="display: inline">
            <table id="tblProgreso" cellspacing="2" cellpadding="1" width="100%" border="0" runat="server">
                <tr style="height: 50px">
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <asp:TextBox ID="lblProgreso" runat="server" BorderStyle="None" BorderWidth="0" CssClass="captionBlue"
                            Width="100%" Style="text-align: center">Su solicitud est� siendo tramitada. Espere unos instantes...</asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <asp:Image ID="imgProgreso" runat="server" src="../_common/images/cursor-espera_grande.gif"></asp:Image>
                    </td>
                </tr>
            </table>
        </div>
        <div id="divForm2" style="display: none">
            <table cellspacing="3" cellpadding="1" width="100%" border="0">
                <tr>
                    <td width="100%" colspan="4">
                        <igtab:UltraWebTab ID="uwtGrupos" runat="server" Width="100%" BorderStyle="Solid" BorderWidth="1px"
                            CustomRules="padding:10px;" FixedLayout="True" DummyTargetUrl=" "
                            ThreeDEffect="False" DisplayMode="Scrollable" EnableViewState="false">
                            <DefaultTabStyle Height="24px" CssClass="uwtDefaultTab">
                                <Padding Left="20px" Right="20px"></Padding>
                            </DefaultTabStyle>
                            <ClientSideEvents InitializeTabs="initTab" />
                            <RoundedImage NormalImage="ig_tab_blueb2.gif" HoverImage="ig_tab_blueb1.gif" FillStyle="LeftMergedWithCenter"></RoundedImage>
                        </igtab:UltraWebTab>
                        <script type="text/javascript">
                            var i = 0;
                            var bSalir = false;
                            while (bSalir == false) {
                                if (document.getElementById("uwtGrupos_div" + i)) {
                                    document.getElementById("uwtGrupos_div" + i).style.visibility = 'hidden';
                                    i = i + 1;
                                }
                                else {
                                    bSalir = true;
                                }
                            }
                        </script>
                    </td>
                </tr>
            </table>
            <div id="divDropDowns" style="z-index: 103; visibility: hidden; position: absolute; top: 300px"></div>
            <div id="divCalculados" style="z-index: 110; visibility: hidden; position: absolute; top: 0px"></div>
            <div id="divAlta" style="visibility: hidden"></div>
        </div>
        <fsn:FSNPanelInfo ID="FSNPanelDatosPeticionario" runat="server" ServicePath="~/App_Pages/PMWEB/ConsultasPMWEB.asmx" ServiceMethod="Obtener_DatosPersona" TipoDetalle="2"></fsn:FSNPanelInfo>
        <fsn:FSNPanelInfo ID="FSNPanelDatosTrasladadaPor" runat="server" ServicePath="~/App_Pages/PMWEB/ConsultasPMWEB.asmx" ServiceMethod="Obtener_DatosPersona" TipoDetalle="3"></fsn:FSNPanelInfo>
        <fsn:FSNPanelInfo ID="FSNPanelDatosProveedor" runat="server" ServicePath="~/App_Pages/PMWEB/ConsultasPMWEB.asmx" ServiceMethod="Obtener_DatosProveedor" TipoDetalle="1"></fsn:FSNPanelInfo>

    </form>
    <div id="divForm3" style="display: none">
        <form id="frmCalculados" name="frmCalculados" action="../alta/recalcularimportes.aspx" method="post" target="fraWSServer">
        </form>
        <form id="frmDesglose" name="frmDesglose" method="post" target="winDesglose">
        </form>
    </div>
    <script type="text/javascript">HabilitarBotones();</script>
</body>
</html>