﻿<%@ Register Assembly="Infragistics.WebUI.UltraWebNavigator.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebNavigator" TagPrefix="ignav" %>
<%@ Register Assembly="Infragistics.WebUI.UltraWebTab.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>
<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NWgestionInstancia.aspx.vb" Inherits="Fullstep.FSNWeb.NWgestionInstancia" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>NWgestionInstancia</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
</head>

<script type="text/javascript">
    wProgreso = null;
    var idAccionEnEjecucion = null;
    var MemJustificativaCreada = '0';
    var temporizador0 = false;
    var temporizador1 = false;
    var temporizador2 = false;
    var temporizador3 = false;
    var temporizador4 = false;
    var temporizador5 = false;

    //Muestra el contenido peligroso que ha efectuado un error al guardar.
    function ErrorValidacion(contenido) {
        alert(arrTextosML[2].replace("$$$", contenido));
        OcultarEspera()
    }
    //Le pone un ancho al desglose
    function resize() {
        for (i = 0; i < arrDesgloses.length; i++) {
            sDiv = arrDesgloses[i].replace("tblDesglose", "divDesglose")
            if (document.getElementById(sDiv))
                document.getElementById(sDiv).style.width = parseFloat(document.body.offsetWidth) - 95 + 'px';
        }
    }
    function localEval(s) {
        eval(s)
    }
    //Muestra el detalle del tipo de solicitud
    function DetalleTipoSolicitud() {
        var IdSolicitud = document.forms["frmDetalle"].elements["txtIdTipo"].value;
        var newWindow = window.open("../alta/solicitudPMWEB.aspx?Solicitud=" + IdSolicitud, "_blank", "width=700,height=450,status=no,resizable=no,top=100,left=100");
        newWindow.focus();
        return false;
    }
    //Muestra el div que indica que se está cargando la solicitud
    function MostrarEspera() {
        wProgreso = true;

        $("[id*='lnkBoton']").attr('disabled', 'disabled');

        document.getElementById("lblCamposObligatorios").style.display = "none"
        document.getElementById("divForm2").style.display = 'none';
        document.getElementById("divForm3").style.display = 'none';
        document.getElementById("igtabuwtGrupos").style.display = 'none';

        var i = 0;
        var bSalir = false;
        while (bSalir == false) {
            if (document.getElementById("uwtGrupos_div" + i)) {
                document.getElementById("uwtGrupos_div" + i).style.visibility = 'hidden';
                i = i + 1;
            } else {
                bSalir = true;
            }
        }
        document.getElementById("lblProgreso").value = document.forms["frmDetalle"].elements["cadenaespera"].value;
        document.getElementById("divProgreso").style.display = 'inline';
    }

    //Estaba habilitando los botones antes de ocultar el progreso. Parecia q te dejaba dar a varios botones mientras estaba en progreso.
    function DarTiempoAOcultarProgreso() {
        $("[id*='lnkBoton']").removeAttr('disabled');
    }

    //Oculta el div que indica que se está cargando la solicitud
    function OcultarEspera() {
        wProgreso = null;

        document.getElementById("divProgreso").style.display = 'none';
        document.getElementById("divForm2").style.display = 'inline';
        document.getElementById("divForm3").style.display = 'inline';
        document.getElementById("igtabuwtGrupos").style.display = '';

        setTimeout('DarTiempoAOcultarProgreso()', 250);

        var i = 0;
        var bSalir = false;
        while (bSalir == false) {
            if (document.getElementById("uwtGrupos_div" + i)) {
                document.getElementById("uwtGrupos_div" + i).style.visibility = 'visible';
                i = i + 1;
            } else {
                bSalir = true;
            }
        }
        return
    }
    //Realizar el submit cuando se pulsa el botón de Trasladar la solicitud
    function MontarSubmitTrasladar(HayMapper) {
        MontarFormularioSubmit(true, true, HayMapper, false)

        var frmSubmitElements = document.forms["frmSubmit"].elements;

        frmSubmitElements["GEN_AccionRol"].value = 0;
        frmSubmitElements["GEN_Bloque"].value = document.forms["frmDetalle"].elements["Bloque"].value;
        frmSubmitElements["GEN_Rol"].value = oRol_Id;
        frmSubmitElements["GEN_Enviar"].value = 0;
        frmSubmitElements["GEN_Accion"].value = "trasladarinstancia";
        frmSubmitElements["PantallaMaper"].value = HayMapper;
        frmSubmitElements["DeDonde"].value = 'Gestion';

        oFrm = MontarFormularioCalculados();
        sInner = oFrm.innerHTML;
        oFrm.innerHTML = "";
        document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner);
        document.forms["frmSubmit"].submit();
        return false;
    }
    //Deshabilita los botones de la página y llama a montarSubmitTrasladar para realizar el traslado de la solicitud
    function Trasladar() {
        if (wProgreso == null) {
            wProgreso = true;
            MostrarEspera();
        }

        var HayMapper = false;
        if (document.forms["frmDetalle"].elements["PantallaMaper"].value == "True")
            HayMapper = true;

        setTimeout("MontarSubmitTrasladar(" + HayMapper + ")", 100)
        return false;
    }
    function LanzarListado(NombreArchivo, Instancia) {
        var newWindow = window.open("../listados/ObtenerInforme.aspx?FileReport=" + NombreArchivo + "&Instancia=" + Instancia, "_blank", "width=800,height=600,status=no,resizable=yes,top=100,left=100, ScrollBars=yes")
        newWindow.focus();
        return false;
    }
    /*''' <summary>
    ''' Monta el formulario para el posterior guardado. (guardarInstancia.aspx)
    ''' </summary>
    ''' <param name="id">Id Accion</param>
    ''' <param name="bloque">Id Bloque</param>        
    ''' <param name="comp_obl">Si es necesario rellenar los campos obligatorios</param>
    ''' <param name="guarda">si hay que guardar o versi�n o no</param> 
    ''' <param name="HayMapper">Si es necesario hacer comprobaciones mapper en guardarInstancia.aspx</param>
    ''' <remarks>Llamada desde: javascript EjecutarAccion2; Tiempo m�ximo:0,2</remarks>*/
    function MontarSubmitAccion(id, bloque, comp_obl, guarda, HayMapper) {
        idAccionEnEjecucion = id
        MontarFormularioSubmit(guarda, true, HayMapper, false)

        var frmDetalleElements = document.forms["frmDetalle"].elements;
        var frmSubmitElements = document.forms["frmSubmit"].elements;

        frmSubmitElements["GEN_AccionRol"].value = id;
        frmSubmitElements["GEN_Bloque"].value = bloque;
        frmSubmitElements["GEN_Rol"].value = oRol_Id;
        frmSubmitElements["PantallaMaper"].value = HayMapper
        frmSubmitElements["DeDonde"].value = 'Gestion'

        oFrm = MontarFormularioCalculados()
        sInner = oFrm.innerHTML
        oFrm.innerHTML = ""
        document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner)

        if (frmDetalleElements["VinculacionesIdGuardar"].value == id) {
            if (frmDetalleElements["VinculacionesPrimerGuardar"].value != "0") {
                for (var indice in htControlFilasVinc) {
                    if (htControlFilas[indice] == "0") {
                        htControlFilasVinc[indice] = "0";
                    }
                }
            }
        }

        document.forms["frmSubmit"].submit()

        if (frmDetalleElements["VinculacionesIdGuardar"].value == id) {
            var i;
            var campo;
            for (var iddesglose in htControlArrVinc) {
                i = 0;

                if (frmDetalleElements["VinculacionesPrimerGuardar"].value == "0") {
                    for (var indice in htControlFilasVinc) {
                        campo = indice.substring(0, indice.indexOf("_"))

                        if (campo == iddesglose) {
                            if (htControlFilas[indice] == "0") {
                                htControlFilasVinc[indice] = "0";
                                i++;
                            } else {
                                htControlFilasVinc[indice] = String(parseInt(htControlFilasVinc[indice]) - i);
                            }
                        }
                    }
                } else {
                    for (var indice in htControlFilasVinc) {
                        campo = indice.substring(0, indice.indexOf("_"))

                        if (campo == iddesglose) {
                            if (htControlFilasVinc[indice] != "0") {
                                i++;
                                htControlFilasVinc[indice] = String(i);
                            }
                        }
                    }
                }
            }
            frmDetalleElements["VinculacionesPrimerGuardar"].value = 1;
        }

        for (var indice in htControlArrVincMovible) {
            if (htControlArrVincMovible[indice] == "##Nueva##") {
                htControlArrVincMovible[indice] = "##Si##";
            }
        }
    }
    //Descripci�n: Cuando se pulsa un bot�n para realizar una acci�n, se llama a esta funci�n
    //Param�tros: id: id de la acci�n
    // bloque: id del bloque
    // comp_olb: si hay que comprobar los campos de obligatorios o no
    // guarda: si hay que guardar o versi�n o no
    // rechazo: indica si la accion implica un rechazo, ya sea temporal o definitivo
    function EjecutarAccion(id, bloque, comp_obl, guarda, bloq, rechazo) {
        setTimeout("EjecutarAccion2(" + id + "," + bloque + "," + comp_obl + "," + guarda + "," + bloq + "," + rechazo + ")", 100);
    }
    //Descripci�n: Comprueba los campos obligatorios, si hay participantes o no, y llama a montarformulario
    //Param�tros: id: id de la acci�n
    // bloque: id del bloque
    // comp_olb: si hay que comprobar los campos de obligatorios o no
    // guarda: si hay que guardar o versi�n o no
    // rechazo: indica si la accion implica un rechazo, ya sea temporal o definitivo
    // LLamada desde: EjecutarAccion
    function EjecutarAccion2(id, bloque, comp_obl, guarda, bloq, rechazo) {
    	var respOblig;
        if (bloq == 2) {
            alert(arrTextosML[3])
            return false
        }
        
        if (comp_obl == true || comp_obl == "true") {
            bMensajePorMostrar = document.getElementById("bMensajePorMostrar")
            if (bMensajePorMostrar)
                if (bMensajePorMostrar.value == "1") {
                    bMensajePorMostrar.value = "0";
                    return false;
                }

            respOblig = comprobarObligatorios();
            switch (respOblig) {
                case "":  //no falta ningun campo obligatorio
                    break;
                case "filas0": //no se han introducido filas en un desglose obligatorio
                	alert(arrTextosML[0]);
                	return false;
                    break;
                default: //falta algun campo obligatorio
                	alert(arrTextosML[0] + '\n' + respOblig);
                	return false;
                    break;
            }
        }

        if (rechazo == false) {
        	respOblig = comprobarParticipantes();
        	if (respOblig !== '') {
        		alert(arrTextosML[0] + '\n' + respOblig);
        		return false;
        	};
        };

        var frmDetalleElements = document.forms["frmDetalle"].elements;
        if (frmDetalleElements["PantallaVinculaciones"].value == "True") {
            if (Validar_Cantidades_Vinculadas(0, document.getElementById("Instancia").value) == false) {
                return false
            }
        }

        if (wProgreso == null) {
            wProgreso = true;
            MostrarEspera();
        }

        HayMapper = false;
        if (frmDetalleElements["PantallaMaper"].value == "True")
            HayMapper = true;

        setTimeout("MontarSubmitAccion(" + id + "," + bloque + "," + comp_obl + "," + guarda + "," + HayMapper + ")", 100);
    }
    function init() {
        resize();
    }
    //Habilita los botones disponibles en la solicitud y llama a ocultar espera	
    function HabilitarBotones() {
        var retardo = 11000;
        var x = 0;
        if (sAccion_MemoriaJust == idAccionEnEjecucion) {
            MemJustificativaCreada = '0';
            if (temporizador0) {
                clearTimeout(temporizador0); temporizador0 = false;
                clearTimeout(temporizador1); temporizador1 = false;
                clearTimeout(temporizador2); temporizador2 = false;
                clearTimeout(temporizador3); temporizador3 = false;
                clearTimeout(temporizador4); temporizador4 = false;
                clearTimeout(temporizador5); temporizador5 = false;
            }
            for (x = 0; x <= 5; x++) {
                if (x == 0)
                    temporizador0 = setTimeout('ComprobarCreacionMemJustificativa()', retardo);
                if (x == 1)
                    temporizador1 = setTimeout('ComprobarCreacionMemJustificativa()', retardo);
                if (x == 2)
                    temporizador2 = setTimeout('ComprobarCreacionMemJustificativa()', retardo);
                if (x == 3)
                    temporizador3 = setTimeout('ComprobarCreacionMemJustificativa()', retardo);
                if (x == 4)
                    temporizador4 = setTimeout('ComprobarCreacionMemJustificativa()', retardo);
                if (x == 5)
                    temporizador5 = setTimeout('ComprobarCreacionMemJustificativa()', retardo);

                retardo = retardo + 8000;
            }
        } else {
            if (arrObligatorios.length > 0)
                if (document.getElementById("lblCamposObligatorios"))
                    document.getElementById("lblCamposObligatorios").style.display = ""
            OcultarEspera();
        }
    }
    function ComprobarCreacionMemJustificativa() {
        if (MemJustificativaCreada == '1')
            return;
        CreateXmlHttp();

        if (xmlHttp) {
            var params = "&accion=7"

            xmlHttp.open("POST", rutaPM + "GestionaAjax.aspx", false);
            xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            xmlHttp.send(params);


            var retorno;
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                retorno = xmlHttp.responseText;
                if (retorno == '1' && MemJustificativaCreada == '0') {
                    MemJustificativaCreada = '1';
                    HabilitarBotonesMemoriaJustificativa()
                }
            }
        }
    }
    //funcion que habilita los botones con un delay cuando se ha pulsado la opcion de descargar la memoria justificativa
    function HabilitarBotonesMemoriaJustificativa() {
        var inputMemJustificativa;

        OcultarEspera();

        var newWindow = window.open(rutaFS + "_Common/download.aspx?MemoriaJustificativa=1&path=&nombre=MemoriaJustificativa.pdf&datasize=", "_blank", "width=600,height=275,status=no,resizable=yes,top=200,left=200,menubar=no")
        newWindow.focus();
        for (arrInput in arrInputs) {
            if (arrInputs[arrInput].indexOf(Campo_MemoriaJust) != -1) {
                inputMemJustificativa = arrInputs[arrInput]
                break;
            }
        }
        CreateXmlHttp();

        if (xmlHttp) {
            var params = "MemJustificativa=1&accion=1&EntryID=" + inputMemJustificativa + '&instancia=' + idInstancia + '&tipo=1'

            xmlHttp.open("POST", rutaPM + "GestionaAjax.aspx", false);
            xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            xmlHttp.send(params);

            var retorno;
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                retorno = xmlHttp.responseText;
                var div;
                div = document.getElementById(inputMemJustificativa + "_divcontenedor")
                div.innerHTML = xmlHttp.responseText;
                var hid_MemJustif = document.getElementById("__hidIdMemJustif")
                document.getElementById(inputMemJustificativa + '__hNew').value = hid_MemJustif.value;
            }
        }
    }
    function CalcularCamposCalculados() {
        oFrm = MontarFormularioCalculados()
        oFrm.submit()
        return false;
    }
    function initTab(webTab) {
        var cp = document.getElementById(webTab.ID + '_cp');
        cp.style.minHeight = '300px';
    }
    function mostrarMenuListados(event) {
        var p = $get("lnkBotonListados").getBoundingClientRect();
        igmenu_showMenu('uwPopUpListados', event, p.left, p.bottom);
        return false;
    }
    function mostrarMenuAcciones(event, i) {
        var p = $get("lnkBotonAccion" + i.toString()).getBoundingClientRect();
        igmenu_showMenu('uwPopUpAcciones', event, p.left, p.bottom);
        return false;
    }
</script>
<script language="javascript" type="text/javascript">
    var xmlHttp;
    /*''' <summary>
    ''' Crear el objeto para llamar con ajax a ComprobarEnProceso
    ''' </summary>
    ''' <remarks>Llamada desde: javascript ; Tiempo m�ximo: 0</remarks>*/
    function CreateXmlHttp() {
        // Probamos con IE
        try {
            // Funcionar� para JavaScript 5.0
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (oc) {
                xmlHttp = null;
            }
        }

        // Si no se trataba de un IE, probamos con esto
        if (!xmlHttp && typeof XMLHttpRequest != "undefined") {
            xmlHttp = new XMLHttpRequest();
        }

        return xmlHttp;
    }
    //Creamos el objeto xmlHttpRequest
    CreateXmlHttp();

    /*''' <summary>
    ''' Hacer una validaci�n a medida de cantidades antes de a�adir lineas vinculadas
    ''' </summary>
    ''' <param name="sRoot">En q desglose, html, se van a�adir lineas</param>
    ''' <param name="IdCampo">En q desglose, form_campo.id, se van a�adir lineas</param>        
    ''' <param name="Row">Fila a a�adir</param>
    ''' <remarks>Llamada desde: PMWeb2008/BuscadorSolicitudes.aspx.vb; Tiempo m�ximo:0,1</remarks>*/
    function VincularCopiarFilaVacia(sRoot, IdCampo, Row) {
        CreateXmlHttp();
        if (xmlHttp) {
            if (Row.getCellFromKey) {
                var params = "Instancia=" + Row.getCellFromKey("INSTANCIAORIGEN").getValue() + "&Desglose=" + Row.getCellFromKey("DESGLOSEORIGEN").getValue() + "&Linea=" + Row.getCellFromKey("LINEAORIGEN").getValue();
            } else {
                var params = "Instancia=" + Row.get_cellByColumnKey("INSTANCIAORIGEN").get_value() + "&Desglose=" + Row.get_cellByColumnKey("DESGLOSEORIGEN").get_value() + "&Linea=" + Row.get_cellByColumnKey("LINEAORIGEN").get_value();
            }

            xmlHttp.open("POST", "../_common/controlarLineaVinculada.aspx", false);
            xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            xmlHttp.send(params);

            //Tras q se ejecute sincronamente controlarLineaVinculada.aspx controlamos sus resultados y obramos en 
            //consecuencia.                            
            var retorno;
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                retorno = xmlHttp.responseText;
                if (retorno == 'OK') { //pasa la comprobaci�n
                    copiarFilaVacia(sRoot, IdCampo, 0, Row)
                }
                else {
                    var newWindow = window.open("../_common/NoPasoControlarLineaVinculada.aspx?" + params, "_blank", "width=350,height=200,status=yes,resizable=no,top=200,left=300")
                    newWindow.focus();
                }
            }
        }
    }
    /*''' <summary>
    ''' A�adir instancia vinculadas
    ''' </summary>
    ''' <param name="sRoot">En q desglose, html, se van a�adir lineas</param>
    ''' <param name="IdCampo">En q desglose, form_campo.id, se van a�adir lineas</param>        
    ''' <param name="sId">Id de la instancia vinculada</param>
    ''' <param name="sDen">Descrip de la instancia vinculada</param> 
    ''' <remarks>Llamada desde: PMWeb2008/BuscadorSolicitudes.aspx.vb; Tiempo m�ximo:0,1</remarks>*/
    function SeleccionarSolicitud(sRoot, IdCampo, sId, sDen) {
        copiarFilaVacia(sRoot, IdCampo, 0, null, sId, sDen);
    }
    /*''' <summary>
    ''' Mueve la linea indicada de la instancia actual a la instancia indicada
    ''' </summary>
    ''' <param name="sRoot">nombre entry del desglose</param>	    
    ''' <param name="IdCampo">De q desglose se va a mover la linea</param>        
    ''' <param name="sId">Id de la instancia a la q mueves</param>
    ''' <param name="index">fila q mueves</param>
    ''' <param name="ObjCelda">tabla html donde esta el bt mover/copiar/elim.</param>        
    ''' <param name="Celda">Celda, html, donde esta el bt mover/copiar/elim. A traves de �l se saca la fila y tabla html</param>
    ''' <param name="Frame">Frame donde esta el desglose a borrar</param>          
    ''' <remarks>Llamada desde: PMWeb2008/BuscadorSolicitudes.aspx.vb; Tiempo m�ximo:0,1</remarks>*/
    function VincularMoverAInstancia(sRoot, IdCampo, sId, Index, ObjCelda, Celda, Frame) {
        CreateXmlHttp();
        if (xmlHttp) {
            IndexCtrl = Index
            if (document.forms["frmDetalle"].elements["VinculacionesPrimerGuardar"].value == 1)
                IndexCtrl = DameLineaMoverAInstancia(IdCampo, Index, 0)

            var params = "Mover=1&Instancia=" + document.getElementById("Instancia").value + "&Desglose=" + IdCampo + "&Linea=" + IndexCtrl;

            xmlHttp.open("POST", "../_common/controlarLineaVinculada.aspx", false);
            xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            xmlHttp.send(params);

            //Tras q se ejecute sincronamente controlarLineaVinculada.aspx controlamos sus resultados y obramos en 
            //consecuencia.                            
            var retorno;
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                retorno = xmlHttp.responseText;
                if (retorno == 'OK') { //pasa la comprobaci�n	                    
                    MoverAInstancia(IdCampo, sId, Index, 0);

                    var p = window.parent
                    deleteRow(ObjCelda, sRoot, Index, IdCampo)
                    var oFrame = document.getElementById(Frame)
                    oFrame.parentNode.removeChild(oFrame);
                } else
                    if (retorno == 'PANT') {//existen lineas vinculadas pedir confirmacion
                        var newWindow = window.open('../_common/controlarLineaVinculadaMover.aspx?IdCampo=' + IdCampo + '&sId=' + sId + '&Index=' + Index + '&Celda=' + Celda + '&sRoot=' + sRoot + '&Frame=' + Frame + '&PopUp=0', "_blank", "width=350,height=105,status=yes,resizable=no,top=200,left=300")
                        newWindow.focus();
                    }
            }
        }
    }
    /*''' <summary>
    ''' Evitar los enter q hacen q el 1er botón de FSPMPageHeader se ejecute. Evita el submit.
    ''' </summary>
    ''' <param name="event">Pulsación</param>
    ''' <remarks>Llamada desde: sistema; Tiempo máximo:0</remarks>*/
    function onEnterDoNothing(event) {
        if ((event.target.type != 'textarea') && (event.which == 13)) {//13 is enter key        
            event.preventDefault();
        }
    }
</script>
<script type="text/javascript" language="javascript" id="clientEventHandlersJS">
    //Llama al popup para poder imprimir/exportar la solicitud
    function cmdImpExp_onclick() {
        var newWindow = window.open('../seguimiento/impexp_sel.aspx?Instancia=' + document.getElementById("Instancia").value + '&TipoImpExp=1', '_new', 'fullscreen=no,height=115,width=315,location=no,menubar=no,resizable=no,scrollbars=no,status=yes,titlebar=yes,toolbar=no,left=200,top=200');
        newWindow.focus();
        return false;
    }
</script>
<body onresize="resize()" onunload="if (wProgreso != null) OcultarEspera();wProgreso=null;"
    onload="init()">
    <form id="frmDetalle" method="post" runat="server" onkeydown="onEnterDoNothing(event)" onkeypress="onEnterDoNothing(event)">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <CompositeScript>
                <Scripts>
                    <asp:ScriptReference Name="ExtenderBase.BaseScripts.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Common.Common.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Compat.Timer.Timer.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Animation.Animations.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Animation.AnimationBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="PopupExtender.PopupBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="AutoComplete.AutoCompleteBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Path="../alta/js/AdjacentHTML.js" />
                    <asp:ScriptReference Path="../alta/js/jsAlta.js" />
                </Scripts>
            </CompositeScript>
        </asp:ScriptManager>

        <iframe id="iframeWSServer" style="z-index: 108; left: 8px; visibility: hidden; position: absolute; top: 200px"
            name="iframeWSServer" src="../blank.htm"></iframe>

        <input type="hidden" id="bMensajePorMostrar" name="bMensajePorMostrar" value="0" />
        <input id="Rol" type="hidden" name="Rol" runat="server" />
        <input id="Instancia" type="hidden" name="Instancia" runat="server" />
        <input id="txtEnviar" type="hidden" name="Enviar" runat="server" />
        <input id="Version" type="hidden" name="Version" runat="server" />
        <input id="hidVolver" type="hidden" runat="server" />
        
        <div style="width:100%; position:fixed; z-index:100; background-color:white;">
            <uc1:menu runat="server" ID="Menu1" Seccion="Detalle solicitud"></uc1:menu>                
            <fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
        </div>
        <!------------------------------------------------->
        <div style="padding-left: 15px; padding-bottom: 15px; padding-top:14em;">
            <asp:Panel ID="pnlCabecera" runat="server" BackColor="#f5f5f5" Font-Names="Arial" Width="99%">
                <table style="height: 15%; width: 100%; padding-bottom: 15px; padding-left: 5px" cellspacing="0" cellpadding="1" border="0">
                    <tr>
                        <td style="padding-top: 5px; padding-bottom: 5px;" class="fondoCabecera">
                            <table style="width: 100%; table-layout: fixed; padding-left: 10px" border="0">
                                <tr>
                                    <td colspan="2"><asp:Label ID="lblIDInstanciayEstado" runat="server" CssClass="label" Font-Bold="true"></asp:Label></td>
                                    <td><asp:Label ID="lblLitImporte" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
                                    <td><asp:Label ID="lblImporte" runat="server" CssClass="label" Font-Bold="true"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td><asp:Label ID="lblLitCreadoPor" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblCreadoPor" runat="server" CssClass="label"></asp:Label>
                                        <asp:Image ID="imgInfCreadoPor" ImageUrl="images/info.gif" runat="server" Style="cursor: pointer" />
                                    </td>
                                    <td><asp:Label ID="lblLitEstado" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
                                    <td><asp:Label ID="lblEstado" runat="server" CssClass="label"></asp:Label></td>
                                    <td><asp:HyperLink ID="HyperDetalle" runat="server" CssClass="CaptionLink"></asp:HyperLink></td>
                                </tr>
                                <tr>
                                    <td><asp:Label ID="lblLitFecAlta" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
                                    <td><asp:Label ID="lblFecAlta" runat="server" CssClass="label"></asp:Label></td>
                                    <td><asp:Label ID="lblLitTipo" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblTipo" runat="server" CssClass="label"></asp:Label>
                                        <asp:Image ID="imgInfTipo" ImageUrl="images/info.gif" runat="server" Style="cursor: pointer" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <asp:Label ID="lblCamposObligatorios" Style="z-index: 102; display: none;" runat="server" CssClass="captionRed">Los campos marcados con (*) son de obligada cumplimentacion</asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>

        <ajx:DropShadowExtender TrackPosition="true" ID="DropShadowExtender1" runat="server" Opacity="0.5" Width="3" TargetControlID="pnlCabecera" Rounded="true">
        </ajx:DropShadowExtender>
        <!---------------------------------------------------->
        <div id="divProgreso" style="display: inline">
            <table id="tblProgreso" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
                <tr style="height: 50px">
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <asp:TextBox ID="lblProgreso" runat="server" BorderStyle="None" BorderWidth="0" CssClass="captionBlue"
                            Width="100%" Style="text-align: center">Su solicitud est� siendo tramitada. Espere unos instantes...</asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <asp:Image ID="imgProgreso" runat="server" src="../_common/images/cursor-espera_grande.gif"></asp:Image>
                    </td>
                </tr>
            </table>
        </div>
        <div id="divForm2" style="display: none">
            <table cellspacing="3" cellpadding="1" width="100%" border="0">
                <tr>
                    <td width="100%" colspan="4">
                        <igtab:UltraWebTab ID="uwtGrupos" runat="server" Width="100%" DisplayMode="Scrollable"
                            ThreeDEffect="False" DummyTargetUrl=" " FixedLayout="True" CustomRules="padding:10px;" BorderWidth="1px"
                            BorderStyle="Solid" EnableViewState="false">
                            <DefaultTabStyle Height="24px" CssClass="uwtDefaultTab">
                                <Padding Left="20px" Right="20px"></Padding>
                            </DefaultTabStyle>
                            <RoundedImage NormalImage="ig_tab_blueb2.gif" HoverImage="ig_tab_blueb1.gif" FillStyle="LeftMergedWithCenter"></RoundedImage>
                            <ClientSideEvents InitializeTabs="initTab" />
                        </igtab:UltraWebTab>
                        <script type="text/javascript">
                            var i = 0;
                            var bSalir = false;
                            while (bSalir == false) {
                                if (document.getElementById("uwtGrupos_div" + i)) {
                                    document.getElementById("uwtGrupos_div" + i).style.visibility = 'hidden';
                                    i = i + 1;
                                }
                                else {
                                    bSalir = true;
                                }
                            }
                        </script>
                    </td>
                </tr>
            </table>
            <input id="txtIdTipo" type="hidden" name="txtIdTipo" runat="server" />
            <input id="Bloque" type="hidden" name="Bloque" runat="server" />
            <div id="divDropDowns" style="z-index: 104; visibility: hidden; position: absolute; top: 300px" name="divDropDowns"></div>
            <div id="divCalculados" style="z-index: 109; visibility: hidden; position: absolute; top: 0px" name="divCalculados"></div>
            <div id="divAlta" style="visibility: hidden"></div>
            <div id="divAcciones" runat="server">
                <ignav:UltraWebMenu ID="uwPopUpAcciones" runat="server" ScrollImageBottom="ig_menu_scrolldown.gif" ScrollImageTopDisabled="ig_menu_scrollup_disabled.gif"
                    ScrollImageBottomDisabled="ig_menu_scrolldown_disabled.gif" Cursor="Default" ScrollImageTop="ig_menu_scrollup.gif"
                    SubMenuImage="ig_menuTri.gif" WebMenuTarget="PopupMenu">
                    <ItemStyle CssClass="ugMenuItem"></ItemStyle>
                    <DisabledStyle ForeColor="LightGray"></DisabledStyle>
                    <HoverItemStyle Cursor="Hand" CssClass="ugMenuItemHover"></HoverItemStyle>
                    <IslandStyle Cursor="Default" BorderWidth="1px" Font-Size="8pt" Font-Names="MS Sans Serif" BorderStyle="Outset"
                        ForeColor="Black" BackColor="LightGray">
                    </IslandStyle>
                    <ExpandEffects ShadowColor="LightGray"></ExpandEffects>
                    <TopSelectedStyle Cursor="Default"></TopSelectedStyle>
                    <SeparatorStyle BackgroundImage="ig_menuSep.gif" CssClass="SeparatorClass" CustomRules="background-repeat:repeat-x; "></SeparatorStyle>
                    <Levels>
                        <ignav:Level Index="0"></ignav:Level>
                    </Levels>
                </ignav:UltraWebMenu>
            </div>
            <div id="divListados" runat="server">
                <ignav:UltraWebMenu ID="uwPopUpListados" runat="server" ScrollImageBottom="ig_menu_scrolldown.gif" ScrollImageTopDisabled="ig_menu_scrollup_disabled.gif"
                    ScrollImageBottomDisabled="ig_menu_scrolldown_disabled.gif" Cursor="Default" ScrollImageTop="ig_menu_scrollup.gif"
                    SubMenuImage="ig_menuTri.gif" WebMenuTarget="PopupMenu">
                    <ItemStyle CssClass="ugMenuItem"></ItemStyle>
                    <DisabledStyle ForeColor="LightGray"></DisabledStyle>
                    <HoverItemStyle Cursor="Hand" CssClass="ugMenuItemHover"></HoverItemStyle>
                    <IslandStyle Cursor="Default" BorderWidth="1px" Font-Size="8pt" Font-Names="MS Sans Serif" BorderStyle="Outset"
                        ForeColor="Black" BackColor="LightGray">
                    </IslandStyle>
                    <ExpandEffects ShadowColor="LightGray"></ExpandEffects>
                    <TopSelectedStyle Cursor="Default"></TopSelectedStyle>
                    <SeparatorStyle BackgroundImage="ig_menuSep.gif" CssClass="SeparatorClass" CustomRules="background-repeat:repeat-x; "></SeparatorStyle>
                    <Levels>
                        <ignav:Level Index="0"></ignav:Level>
                    </Levels>
                </ignav:UltraWebMenu>
            </div>
            <input type="hidden" id="cadenaespera" name="cadenaespera" runat="SERVER" />
            <input id="BotonCalcular" type="hidden" value="0" name="BotonCalcular" runat="server" />
            <input id="PantallaMaper" type="hidden" name="PantallaMaper" runat="SERVER" />
            <input id="PantallaVinculaciones" type="hidden" name="PantallaVinculaciones" runat="SERVER" />
            <input id="VinculacionesIdGuardar" type="hidden" name="VinculacionesIdGuardar" runat="SERVER" />
            <input id="VinculacionesPrimerGuardar" type="hidden" name="VinculacionesPrimerGuardar" runat="SERVER" />
        </div>

        <fsn:FSNPanelInfo ID="FSNPanelDatosPeticionario" runat="server" ServicePath="~/App_Pages/PMWEB/ConsultasPMWEB.asmx" ServiceMethod="Obtener_DatosPersona" TipoDetalle="2"></fsn:FSNPanelInfo>
        <fsn:FSNPanelInfo ID="FSNPanelDatosProveedor" runat="server" ServicePath="~/App_Pages/PMWEB/ConsultasPMWEB.asmx" ServiceMethod="Obtener_DatosProveedor" TipoDetalle="1"></fsn:FSNPanelInfo>
    </form>
    <div id="divForm3" style="display: none">
        <form id="frmCalculados" name="frmCalculados" action="../alta/recalcularimportes.aspx"
            method="post" target="fraWSServer">
        </form>
        <form id="frmDesglose" name="frmDesglose" method="post" target="winDesglose">
        </form>
    </div>
    <script type="text/javascript">HabilitarBotones();</script>
</body>
</html>
