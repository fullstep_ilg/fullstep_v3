<%@ Page Language="vb" AutoEventWireup="false" Codebehind="flowDiagram.aspx.vb" Inherits="Fullstep.FSNWeb.flowDiagram" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	<script language="javascript" id="clientEventHandlersJS">
		function window_onload() {
	
		}

		//Muestra el detalle del peticionario o del aprobador actual
		function BuscarPeticionario(tipo)
		{
			if (tipo==1) //es el peticionario
				{
					var Cod=document.forms["frmDetalle"].elements["txtPeticionario"].value   
				}
			else
				{
					if (tipo==2) //es el aprobador actual
						{
							var Cod=document.forms["frmDetalle"].elements["txtAprobador"].value   
						}	
				}
	
			if (Cod!=null && Cod!="")
	
				{
	                var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/detallepersona.aspx?CodPersona=" + Cod.toString(),"_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");
	                newWindow.focus();
				}
		}

		//Muestra la ventana donde el usuario puede elegir si exportar a BMP,GIF o PDF
		function cmdImpExp_onclick() {
			window.open('flowDiagramExport.aspx?Instancia='+document.forms["frmDetalle"].elements["Instancia"].value,'_new','fullscreen=no,height=200,width=500,location=no,menubar=no,resizable=no,scrollbars=no,status=yes,titlebar=yes,toolbar=no,left=200,top=200');
		}


		//Redimensiona el div que contiene el flujo
		function ResizeDiv()
		{
			document.getElementById("divFlujo").style.top = (parseInt(document.getElementById("Table2").style.top) + parseInt(document.getElementById("Table2").height) + 20) + ' px'
			document.getElementById("divFlujo").style.height = (parseInt(document.body.clientHeight) - parseInt(document.getElementById("divFlujo").style.top)) + ' px'
		}

		window.onresize = ResizeDiv;
	</script>	
	<body language="javascript" onload="ResizeDiv(); return window_onload()">
		<form id="frmDetalle" name="frmDetalle" method="post" runat="server">
			<div id="divFlujo" style="LEFT: 8px; OVERFLOW: scroll; WIDTH: 100%; POSITION: absolute; TOP: 104px; HEIGHT: 100%">
				<cc1:diagramwebcontrol id="dwc" runat="server" Height="352px" Width="688px" SaveToSessionState="False"
					EnableViewState="False"></cc1:diagramwebcontrol>
			</div>
			<INPUT id="txtPeticionario" style="Z-INDEX: 104; LEFT: 504px; WIDTH: 40px; POSITION: absolute; TOP: 168px; HEIGHT: 22px"
				type="hidden" size="1" name="txtIdTipo" runat="server"><INPUT id="Version" style="Z-INDEX: 103; LEFT: 504px; WIDTH: 40px; POSITION: absolute; TOP: 136px; HEIGHT: 22px"
				type="hidden" size="1" name="Version" runat="server"><INPUT id="Instancia" style="Z-INDEX: 102; LEFT: 504px; WIDTH: 40px; POSITION: absolute; TOP: 104px; HEIGHT: 22px"
				type="hidden" size="1" name="Instancia" runat="server">
			<TABLE class="cabeceraSolicitud" id="Table2" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 70px"
				height="94" cellSpacing="3" cellPadding="3" width="100%" border="0">
				<TR>
					<TD style="WIDTH: 92px; HEIGHT: 2px"><asp:label id="lblId" runat="server" Width="100%" CssClass="captionBlue"></asp:label></TD>
					<TD style="WIDTH: 394px; HEIGHT: 2px"><asp:label id="lblBDId" runat="server" Width="100%" CssClass="captionDarkBlue"></asp:label></TD>
					<TD style="WIDTH: 25px; HEIGHT: 2px"><a id="cmdDetalleSolic" class="aPMWeb" style="CURSOR: hand" onclick="javascript:DetalleTipoSolicitud()"
							name="cmdDetalleSolic"></a></TD>
					<TD style="WIDTH: 140px; HEIGHT: 2px"><asp:label id="lblFecha" runat="server" Width="100%" CssClass="captionBlue"></asp:label></TD>
					<TD style="WIDTH: 202px; HEIGHT: 2px"><asp:label id="lblBDFecha" runat="server" Width="100%" CssClass="captionDarkBlue"></asp:label></TD>
					<TD style="WIDTH: 86px; HEIGHT: 2px"></TD>
					<TD style="HEIGHT: 2px"></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 92px; HEIGHT: 12px"><asp:label id="lblTipo" runat="server" Width="100%" CssClass="captionBlue"></asp:label></TD>
					<TD style="WIDTH: 394px; HEIGHT: 12px"><asp:label id="lblBDTipo" runat="server" Width="100%" CssClass="captionDarkBlue"></asp:label></TD>
					<TD></TD>
					<TD style="WIDTH: 140px; HEIGHT: 12px"></TD>
					<TD style="WIDTH: 202px; HEIGHT: 12px"></TD>
					<TD style="WIDTH: 86px; HEIGHT: 12px"></TD>
					<TD style="HEIGHT: 12px"></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 92px"><asp:label id="lblPeticionario" runat="server" Width="100%" CssClass="captionBlue"></asp:label></TD>
					<TD style="WIDTH: 394px"><asp:label id="lblBDPeticionario" runat="server" Width="100%" CssClass="captionDarkBlue"></asp:label></TD>
					<TD style="WIDTH: 25px"><a id="cmdBuscarPet" runat="server" class="aPMWeb" style="CURSOR: hand" onclick="javascript:BuscarPeticionario(1)"
							name="cmdBuscarPet"><IMG src="images/help.gif" border="0"></a><a id="cmdBuscarAprob" class="aPMWeb" style="CURSOR: hand" onclick="javascript:BuscarPeticionario(2)"
							name="cmdBuscarPet"></a></TD>
					<TD align="right" colSpan="2"></TD>
					<TD style="WIDTH: 86px"></TD>
					<TD><INPUT id="cmdImpExp" type="button" value="Impr./Exp." runat="server" name="cmdImpExp"
							class="botonPMWEB" language="javascript" onclick="return cmdImpExp_onclick()"></TD>
				</TR>
			</TABLE>
			<asp:TextBox id="txtPath" style="Z-INDEX: 106; LEFT: 504px; POSITION: absolute; TOP: 208px" runat="server"
				Width="28px" Visible="False"></asp:TextBox>
		</form>
	</body>
</html>
