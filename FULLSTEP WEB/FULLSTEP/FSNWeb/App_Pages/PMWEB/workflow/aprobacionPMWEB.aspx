<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="aprobacionPMWEB.aspx.vb" Inherits="Fullstep.FSNWeb.aprobacionPMWEB" EnableViewState="False" %>
<%@ Register TagPrefix="igtab" Namespace="Infragistics.WebUI.UltraWebTab" Assembly="Infragistics.WebUI.UltraWebTab.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
</head>

<script type="text/javascript">
    //Le pone un ancho al desglose
    function resize() {
        for (i = 0; i < arrDesgloses.length; i++) {
            sDiv = arrDesgloses[i].replace("tblDesglose", "divDesglose")
            if (document.getElementById(sDiv))
                document.getElementById(sDiv).style.width = parseFloat(document.body.offsetWidth) - 95 + 'px';
        }
    }

    //Muestra el detalle del tipo de solicitud
    function DetalleTipoSolicitud() {
        var IdSolicitud = document.forms["frmDetalle"].elements["txtIdTipo"].value
        var newWindow = window.open("../alta/solicitudPMWEB.aspx?Solicitud=" + IdSolicitud, "_blank", "width=700,height=450,status=no,resizable=no,top=100,left=100");
        newWindow.focus();
    }
    function initTab(webTab) {
        var cp = document.getElementById(webTab.ID + '_cp');
        cp.style.minHeight = '300px';
    }
    /*''' <summary>
    ''' Volver al visor sin asignar aprobador
    ''' </summary>
    ''' <remarks>Llamada desde: cmdNo; Tiempo m�ximo:0</remarks>*/
    function cmdNo_click() {
        if (tipoSolicitud == 13) {
            window.open("<%=ConfigurationManager.AppSettings("rutaPM2008")%>Tareas/VisorSolicitudes.aspx?TipoVisor=5", "_top");
        } else {
            window.open("<%=ConfigurationManager.AppSettings("rutaPM2008")%>Tareas/VisorSolicitudes.aspx", "_top");
        }
    }
</script>
<body onresize="resize()" onload="resize()">
    <form id="frmDetalle" method="post" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <CompositeScript>
                <Scripts>
                    <asp:ScriptReference Path="../alta/js/AdjacentHTML.js" />
                    <asp:ScriptReference Path="../alta/js/jsAlta.js" />
                </Scripts>
            </CompositeScript>
        </asp:ScriptManager>
        
        <div style="width:100%; position:fixed; z-index:100; background-color:white;">
            <uc1:menu runat="server" ID="Menu1" Seccion="Detalle solicitud"></uc1:menu>                
            <fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
        </div>
        <!------------------------------------------------->
        <div style="padding-left: 15px; padding-top:14em;">
            <asp:Panel ID="pnlCabecera" runat="server" BackColor="#f5f5f5" Font-Names="Arial"
                Width="95%">
                <table style="height: 15%; width: 100%; padding-bottom: 15px; padding-left: 5px" cellspacing="0" cellpadding="1" border="0">
                    <tr>
                        <td style="padding-top: 5px; padding-bottom: 5px;" class="fondoCabecera">
                            <table style="width: 100%; table-layout: fixed; padding-left: 10px" border="0">
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lblIDInstanciayEstado" runat="server" CssClass="label" Font-Bold="true"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblLitImporte" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblImporte" runat="server" CssClass="label" Font-Bold="true"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblLitCreadoPor" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblCreadoPor" runat="server" CssClass="label"></asp:Label>
                                        <asp:Image ID="imgInfCreadoPor" ImageUrl="images/info.gif" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblLitEstado" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblEstado" runat="server" CssClass="label"></asp:Label></td>
                                    <td>
                                        <asp:HyperLink ID="HyperDetalle" runat="server" CssClass="CaptionLink"></asp:HyperLink></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblLitFecAlta" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblFecAlta" runat="server" CssClass="label"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblLitTipo" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblTipo" runat="server" CssClass="label"></asp:Label>
                                        <asp:Image ID="imgInfTipo" ImageUrl="images/info.gif" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
        <ajx:DropShadowExtender TrackPosition="true" ID="DropShadowExtender1" runat="server"
            Opacity="0.5" Width="3" TargetControlID="pnlCabecera" Rounded="true">
        </ajx:DropShadowExtender>
        <!---------------------------------------------------->
        <table class="popupCN" id="Table1" style="top: 0; left: 16; width: 80%; margin-top: 0.5em; margin-bottom: 0.5em; margin-left: 10%;" cellspacing="0" cellpadding="0">
            <tr>
                <td align="center" width="100%" colspan="5">
                    <asp:Label ID="lblMensaje1" runat="server" Width="100%" CssClass="Titulo"></asp:Label></td>
            </tr>
            <tr>
                <td align="center" width="100%" colspan="5">
                    <asp:Label ID="lblMensaje2" runat="server" Width="100%" CssClass="Titulo"></asp:Label></td>
            </tr>
            <tr>
                <td align="center" width="100%" colspan="5">
                    <table id="tblBotones" cellspacing="1" cellpadding="1" width="200" border="0">
                        <tr>
                            <td>
                                <asp:Button ID="cmdSi" runat="server" CssClass="botonPMWEB"></asp:Button></td>
                            <td>
                                <asp:Button ID="cmdNo" runat="server" CssClass="botonPMWEB"></asp:Button></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <input id="txtIdTipo" type="hidden" name="txtIdTipo" runat="server" />
        <input type="hidden" id="hRolActual" name="hRolActual" runat="server" />
        <input type="hidden" id="Instancia" name="Instancia" runat="server" />
        <igtab:UltraWebTab ID="uwtGrupos" Style="padding-left: 15px; width: 100%;" runat="server"
            DisplayMode="Scrollable" ThreeDEffect="False"
            DummyTargetUrl=" " FixedLayout="True" CustomRules="padding:10px;" BorderWidth="1px"
            BorderStyle="Solid" EnableViewState="false">
            <DefaultTabStyle Height="24px" CssClass="uwtDefaultTab">
                <Padding Left="20px" Right="20px"></Padding>
            </DefaultTabStyle>
            <RoundedImage NormalImage="ig_tab_blueb2.gif" HoverImage="ig_tab_blueb1.gif" FillStyle="LeftMergedWithCenter"></RoundedImage>
            <ClientSideEvents InitializeTabs="initTab" />
        </igtab:UltraWebTab>
        <fsn:FSNPanelInfo ID="FSNPanelDatosPeticionario" runat="server" ServicePath="~/App_Pages/PMWEB/ConsultasPMWEB.asmx" ServiceMethod="Obtener_DatosPersona" TipoDetalle="2"></fsn:FSNPanelInfo>
        <fsn:FSNPanelInfo ID="FSNPanelDatosProveedor" runat="server" ServicePath="~/App_Pages/PMWEB/ConsultasPMWEB.asmx" ServiceMethod="Obtener_DatosProveedor" TipoDetalle="1"></fsn:FSNPanelInfo>
    </form>
</body>
</html>
