
Public Class comprobaraprob
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '********************************************************************************************************
        '*** Autor: sra (31/03/2009)                                                                        *****
        '*** Descripci�n: Comprueba que la solicitud est� asignada a una persona (tanto si es observador    *****
        '*** como no) para llevarle al detalle de la solicitud o llevarle a aprobacion.aspx para            *****
        '*** autoasignarse dicha solicitud.                                                                 *****
        '*** - Si la persona que entra es un observador o la solicitud est� finalizada, y no es aprobador,  *****
        '***   te lleva a NWDetalleSolicitud.aspx.                                                          *****
        '*** - Si la solicitud est� trasladada te lleva a gestiontrasladada.aspx.                           *****
        '*** - En caso contrario, te lleva a detalle NWgestioninstancia.aspx para gestionar dicha solicitud.*****
        '*** Llamada desde: PMWeb2008/Tareas/VisorSolicitudes.aspx y Frames.aspx (cuando se accede desde el *****
        '*** link de los emails)                                                      						*****                    
        '*** Tiempo m�ximo: 1 sg  						                                                    *****                    
        '********************************************************************************************************
        Dim oInstancia As FSNServer.Instancia

        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        oInstancia.ID = Request("Instancia")
        oInstancia.Cargar(Idioma)
        oInstancia.DevolverEtapaActual(Idioma, Usuario.CodPersona, IIf(Request("Bloque") <> Nothing, CLng(Request("Bloque")), 0))
        Dim direccion As String = IIf(Request("volver") <> "", Request("volver"), ConfigurationManager.AppSettings("rutaPM2008") & "tareas/VisorSolicitudes.aspx") &
            IIf(Request("cn") IsNot Nothing, "&cn=1", "")
        Dim sTipoSolicitud As String = String.Empty

        Select Case oInstancia.Solicitud.TipoSolicit
            Case TiposDeDatos.TipoDeSolicitud.Encuesta
                direccion &= "&TipoVisor=" & FSNLibrary.TipoVisor.Encuestas
                sTipoSolicitud = "&TipoSolicitud=" & TiposDeDatos.TipoDeSolicitud.Encuesta
            Case TiposDeDatos.TipoDeSolicitud.Factura
                direccion &= "&TipoVisor=" & FSNLibrary.TipoVisor.Facturas
                sTipoSolicitud = "&TipoSolicitud=" & TiposDeDatos.TipoDeSolicitud.Factura
            Case TiposDeDatos.TipoDeSolicitud.SolicitudQA
                direccion &= "&TipoVisor=" & FSNLibrary.TipoVisor.SolicitudesQA
                sTipoSolicitud = "&TipoSolicitud=" & TiposDeDatos.TipoDeSolicitud.SolicitudQA
        End Select

        Dim sURL As String = String.Empty
		If oInstancia.InstanciaBloqueTrasladada Then
			sURL = ConfigurationManager.AppSettings("rutaPM") & "workflow/gestiontrasladada.aspx?Instancia=" & CStr(oInstancia.ID) & "&Volver=" & direccion & sTipoSolicitud
		ElseIf oInstancia.ComprobarEsObservador(Usuario.CodPersona) AndAlso oInstancia.Etapa = 0 AndAlso oInstancia.AprobadorActual <> Usuario.CodPersona Then 'es observador pero no aprobador
			sURL = ConfigurationManager.AppSettings("rutaPM") & "seguimiento/NWDetalleSolicitud.aspx?Instancia=" & CStr(oInstancia.ID) & "&ComboParticipante=3&Volver=" & direccion & sTipoSolicitud
		ElseIf String.IsNullOrEmpty(oInstancia.AprobadorActual) AndAlso Not oInstancia.Etapa = 0 Then
			sURL = ConfigurationManager.AppSettings("rutaPM") & "workflow/aprobacionPMWEB.aspx?Instancia=" & CStr(oInstancia.ID) & sTipoSolicitud
		ElseIf oInstancia.Etapa = 0 OrElse oInstancia.Estado >= TipoEstadoSolic.SCPendiente Then 'solicitud finalizada
			sURL = ConfigurationManager.AppSettings("rutaPM") & "seguimiento/NWDetalleSolicitud.aspx?Instancia=" & CStr(oInstancia.ID) & "&Volver=" & direccion & sTipoSolicitud
        ElseIf oInstancia.Estado = TipoEstadoSolic.Guardada Or oInstancia.Estado = TipoEstadoSolic.Rechazada Then 'solicitud guardada o rechazada
            sURL = ConfigurationManager.AppSettings("rutaPM") & "seguimiento/NWDetalleSolicitud.aspx?Instancia=" & CStr(oInstancia.ID) & "&Volver=" & direccion & sTipoSolicitud
        Else
            Dim sParametros As String = "Instancia=" & CStr(oInstancia.ID)
            If Not Request("TipoAcceso") = Nothing Then _
                sParametros = sParametros & "&TipoAcceso=" & CInt(Request("TipoAcceso"))

            If Not Request("Bloque") = Nothing Then _
                sParametros = sParametros & "&Bloque=" & Request("Bloque") & "&Rol=" & oInstancia.RolActual

            sParametros = sParametros & "&volver=" & direccion
            sURL = "NWgestioninstancia.aspx?" & sParametros & sTipoSolicitud
        End If

        Response.Redirect(sURL)

        oInstancia = Nothing
    End Sub
End Class

