Public Class detalleSolicConsulta
	Inherits FSNPage

#Region " Web Form Designer Generated Code "

	'This call is required by the Web Form Designer.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub

	Protected WithEvents lblSolicit As System.Web.UI.WebControls.Label
	Protected WithEvents lblEstado As System.Web.UI.WebControls.Label
	Protected WithEvents lblEstadoBD As System.Web.UI.WebControls.Label
	Protected WithEvents lblImporte As System.Web.UI.WebControls.Label
	Protected WithEvents lblTipo As System.Web.UI.WebControls.Label
	Protected WithEvents lblTipoBD As System.Web.UI.WebControls.Label
	Protected WithEvents lblIdBD As System.Web.UI.WebControls.Label
	Protected WithEvents lblPeticionario As System.Web.UI.WebControls.Label
	Protected WithEvents lblPeticionarioBD As System.Web.UI.WebControls.Label
	Protected WithEvents lblFecha As System.Web.UI.WebControls.Label
	Protected WithEvents lblFechaBD As System.Web.UI.WebControls.Label
	Protected WithEvents cmdImpExp As System.Web.UI.HtmlControls.HtmlInputButton
	Protected WithEvents cmdAprobar As System.Web.UI.HtmlControls.HtmlInputButton
	Protected WithEvents cmdRechazar As System.Web.UI.HtmlControls.HtmlInputButton
	Protected WithEvents Instancia As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents AR As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents hid_ComentarioGuardado As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents hid_aprobar As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents hid_rechazar As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents hid_bloque As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents hid_Observador As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents uwtGrupos As Infragistics.WebUI.UltraWebTab.UltraWebTab
	Protected WithEvents cmdCerrar As System.Web.UI.HtmlControls.HtmlInputButton
	Protected WithEvents tblTablaTab As System.Web.UI.WebControls.Table
	Protected WithEvents txtComentario As System.Web.UI.HtmlControls.HtmlTextArea
	Protected WithEvents phComentario As System.Web.UI.WebControls.PlaceHolder
	Protected WithEvents frmDetalle As System.Web.UI.HtmlControls.HtmlForm

	'NOTE: The following placeholder declaration is required by the Web Form Designer.
	'Do not delete or move it.
	Private designerPlaceholderDeclaration As System.Object

	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
		'CODEGEN: This method call is required by the Web Form Designer
		'Do not modify it using the code editor.
		InitializeComponent()
	End Sub

#End Region
	Private Const Imagelocation As String = "../_common/images/3puntos.gif"
	Private Const ClaseFilaTabla As String = "ugfilatabla"
	Private oInstancia As FSNServer.Instancia
	Private oGrupo As FSNServer.Grupo
	Private oTabItem As Infragistics.WebUI.UltraWebTab.Tab

	Dim otblRow As System.Web.UI.WebControls.TableRow
	Dim otblCell As System.Web.UI.WebControls.TableCell
	Dim sAdjun As String
	Dim idAdjun As String
    ''' <summary>
    ''' Carga el detalle de la solicitud con todos los datos como etiquetas. No puede modificar ning�n dato,
    ''' s�lo aprobar, rechazar (metiendo o no un comentario) e imp/exp.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Tiempo m�ximo: Depende de la solicitud</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Response.Expires = -1

        If Page.IsPostBack Then Exit Sub

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Aprobacion

        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        oInstancia.ID = Request("Instancia")
        oInstancia.Cargar(Idioma)
        oInstancia.CargarCamposInstancia(Idioma, Usuario.CodPersona, , True, If(Request("EsObservador") Is Nothing, 1, Request("EsObservador")))

        'campo hidden para Imprimir/Exportar:
        Instancia.Value = Request("Instancia")

        'hiddens
        hid_bloque.Value = Request("bloque")
        hid_aprobar.Value = Request("aprobar")
        hid_rechazar.Value = Request("rechazar")
        hid_Observador.Value = If(Request("EsObservador") Is Nothing, 1, Request("EsObservador"))

        'Textos de idiomas:
        If oInstancia.Solicitud.Tipo = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras Then
            lblSolicit.Text = Textos(46)
        Else
            lblSolicit.Text = oInstancia.Solicitud.DenTipo
        End If
        lblEstado.Text = Textos(45) & ":"

        'Importe
        If oInstancia.Solicitud.Tipo <> TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras Then
            lblImporte.Visible = False
        Else
            lblImporte.Text = Textos(0) & " " & FSNLibrary.FormatNumber(oInstancia.Importe, Usuario.NumberFormat) & " " & oInstancia.Moneda
        End If

        If Not String.IsNullOrEmpty(Request("Certificado")) Then
            Dim oCertif As FSNServer.Certificado = FSNServer.Get_Object(GetType(FSNServer.Certificado))
            oCertif.ID = Request("Certificado")
            oCertif.Load(Idioma)
            lblEstadoBD.Text = DenEstadoCertif(oCertif)
            If oCertif.FechaCumpl <> Date.MinValue Then lblFechaBD.Text = oCertif.FechaCumpl
        Else
            If Not Request("Estado") Is Nothing AndAlso Request("Estado") <> "" Then
                lblEstadoBD.Text = Request("Estado")
            Else
                lblEstadoBD.Text = oInstancia.CargarEstapaActual(Idioma)
            End If
            lblFechaBD.Text = FormatDate(oInstancia.FechaAlta, Usuario.DateFormat)
        End If

        lblTipo.Text = Textos(1)
        lblTipoBD.Text = oInstancia.Solicitud.Codigo + "-" + oInstancia.Solicitud.Den(Idioma)
        lblIdBD.Text = oInstancia.ID
        lblPeticionario.Text = Textos(3)
        lblPeticionarioBD.Text = oInstancia.NombrePeticionario
        lblFecha.Text = Textos(4)

        'Botones y comentario:
        cmdAprobar.Value = Textos(19)
        cmdRechazar.Value = Textos(21)
        cmdImpExp.Value = Textos(28)
        cmdCerrar.Value = Textos(44)

        cmdAprobar.Visible = False
        cmdRechazar.Visible = False
        txtComentario.Value = Session("COMENT" & Request("Instancia"))
        phComentario.Visible = False
        If CLng(Request.QueryString("Aprobar")) <> 0 Then
            cmdAprobar.Visible = True
            phComentario.Visible = True
        End If
        If CLng(Request.QueryString("Rechazar")) <> 0 Then
            cmdRechazar.Visible = True
            phComentario.Visible = True
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutaPM") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM",
                "<script>var rutaPM = '" & System.Configuration.ConfigurationManager.AppSettings("rutaPM") & "';</script>")
        End If

        'Grupos:
        uwtGrupos.Tabs.Clear()
        AplicarEstilosTab(uwtGrupos)
        CargarGrupos()
    End Sub
    Private Function DenEstadoCertif(ByRef oCertif As FSNServer.Certificado) As String
        Dim iModAnt As Integer = ModuloIdioma
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorCertificados
        Dim sDen As String = Textos(oCertif.Id_Estado_Texto - 1)
        ModuloIdioma = iModAnt

        Return sDen
    End Function

    ''' <summary>
    ''' Carga los campos en el detalle de la solicitud
    ''' </summary>
    Private Sub CargarCamposTabla()
        Dim oDSRow As System.Data.DataRow
        Dim lIndex As Integer = 1
        Dim oLbl As System.Web.UI.HtmlControls.HtmlGenericControl
        Dim oImgPopUp As System.Web.UI.WebControls.HyperLink

        For Each oTabItem In uwtGrupos.Tabs
            oGrupo = oInstancia.Grupos.Grupos.Item(lIndex)

            tblTablaTab = New Table
            tblTablaTab.ID = "_tbl" & oTabItem.Key
            tblTablaTab.Width = Unit.Percentage(100)
            tblTablaTab.CellPadding = 2

            If oGrupo.DSCampos.Tables.Count > 0 Then
                For Each oDSRow In oGrupo.DSCampos.Tables(0).Rows
                    If oDSRow.Item("VISIBLE") = 1 Then
                        '------------------------------
                        'Nombre del campo
                        '------------------------------
                        otblRow = New System.Web.UI.WebControls.TableRow
                        otblCell = New System.Web.UI.WebControls.TableCell
                        otblCell.Width = Unit.Percentage(45)

                        If oDSRow.Item("SUBTIPO") <> TiposDeDatos.TipoGeneral.TipoDesglose Then
                            oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
                            oLbl.InnerHtml = DBNullToSomething(oDSRow.Item("DEN_" & Idioma))

                            otblCell.Controls.Add(oLbl)
                            otblCell.CssClass = ClaseFilaTabla
                            otblRow.Cells.Add(otblCell)
                            tblTablaTab.Rows.Add(otblRow)

                            oDSRow.Item("DEN_" & Idioma) = oDSRow.Item("DEN_" & Idioma).ToString.Replace("'", "\'")
                            oDSRow.Item("DEN_" & Idioma) = oDSRow.Item("DEN_" & Idioma).ToString.Replace("""", "\""")
                        End If
                        '------------------------------
                        'Valor del campo
                        '------------------------------
                        otblCell = New System.Web.UI.WebControls.TableCell
                        otblCell.Width = Unit.Percentage(53)

                        If oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoDesglose And DBNullToInteger(oDSRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.DesgloseActividad Then
                            CargarDesglose(oDSRow.Item("ID"), oDSRow.Item("DEN_" & Idioma))
                        ElseIf oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoDesglose And DBNullToInteger(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.DesgloseActividad Then
                            Dim oucDesgloseActividad As desgloseAct = LoadControl("..\alta\desgloseAct.ascx")
                            oucDesgloseActividad.IdProyecto = DBNullToInteger(oDSRow.Item("VALOR_NUM"))
                            oucDesgloseActividad.Obligatorio = DBNullToInteger(oDSRow.Item("OBLIGATORIO"))
                            oucDesgloseActividad.Titulo = DBNullToSomething(oDSRow.Item("DEN_" & Idioma))
                            oucDesgloseActividad.SoloLectura = True
                            If Not oDSRow.Table.Columns("id_campo") Is Nothing Then
                                oucDesgloseActividad.ID = Me.ID + "_" + oDSRow.Item("ID_CAMPO").ToString
                                oucDesgloseActividad.Campo = oDSRow.Item("ID_CAMPO")
                            Else
                                oucDesgloseActividad.ID = Me.ID + "_" + oDSRow.Item("ID").ToString
                                oucDesgloseActividad.Campo = oDSRow.Item("ID")
                            End If
                            otblCell = New System.Web.UI.WebControls.TableCell
                            otblCell.CssClass = ClaseFilaTabla
                            otblCell.ColumnSpan = 3
                            otblCell.Controls.Add(oucDesgloseActividad)
                            otblRow.Cells.Add(otblCell)
                        ElseIf oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoEditor Then
                            If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
                                Dim hfTextSource As HtmlInputHidden
                                Dim textSourceId As String = String.Format("{0}_{1}", oTabItem.ContentPane.ClientID, oDSRow.Item("ID"))

                                hfTextSource = New HtmlInputHidden
                                hfTextSource.ID = oDSRow.Item("ID")
                                hfTextSource.Value = Replace(oDSRow.Item("VALOR_TEXT"), "'", "\'")

                                Dim oAnchor As New HtmlAnchor
                                oAnchor.InnerText = Textos(50)
                                oAnchor.Attributes("onclick") = "AbrirEditor('" & oDSRow.Item("DEN_" & Idioma) & "','" & textSourceId & "')"

                                otblCell = New System.Web.UI.WebControls.TableCell
                                otblCell.CssClass = ClaseFilaTabla
                                otblCell.Controls.Add(hfTextSource)
                                otblCell.Controls.Add(oAnchor)
                                otblRow.Cells.Add(otblCell)
                            Else
                                oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
                                oLbl.InnerHtml = "&nbsp;"
                                otblCell = New System.Web.UI.WebControls.TableCell
                                otblCell.ColumnSpan = 2
                                otblCell.CssClass = ClaseFilaTabla
                                otblCell.Controls.Add(oLbl)
                                otblRow.Cells.Add(otblCell)
                            End If
                        ElseIf oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoEnlace Then
                            If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
                                Dim oAnchor As New HtmlAnchor
                                oAnchor.InnerText = oDSRow.Item("VALOR_TEXT")
                                oAnchor.Attributes("onclick") = "AbrirEnlace('" & Replace(oDSRow.Item("VALOR_TEXT_DEN"), "'", "\'") & "')"

                                otblCell = New System.Web.UI.WebControls.TableCell
                                otblCell.CssClass = ClaseFilaTabla
                                otblCell.Controls.Add(oAnchor)
                                otblRow.Cells.Add(otblCell)
                            Else
                                oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
                                oLbl.InnerHtml = "&nbsp;"
                                otblCell = New System.Web.UI.WebControls.TableCell
                                otblCell.ColumnSpan = 2
                                otblCell.CssClass = ClaseFilaTabla
                                otblCell.Controls.Add(oLbl)
                                otblRow.Cells.Add(otblCell)
                            End If
                        ElseIf (oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoMedio Or oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo) AndAlso
                            (IsDBNull(oDSRow.Item("TIPO_CAMPO_GS")) OrElse (oDSRow.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.Activo And oDSRow.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.Partida _
                            And oDSRow.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.TipoPedido And oDSRow.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.Material)) Then
                            'De tipo Texto medio o largo y que no sean algunos campos de Tipo GS en concreto

                            Dim oDivTexto As New HtmlControls.HtmlGenericControl("DIV")
                            oDivTexto.ID = "_t"

                            oDivTexto.Style.Add("width", "600px")
                            oDivTexto.Style.Add("display", "block")
                            oDivTexto.Style.Add("overflow", "hidden")
                            oDivTexto.Style.Add("text-overflow", "ellipsis")
                            oDivTexto.Style.Add("white-space", "nowrap")
                            Dim hfTextSource As HtmlInputHidden
                            If Not oDSRow.Item("VALOR_TEXT") Is Nothing Then
                                hfTextSource = New HtmlInputHidden
                                hfTextSource.ID = oDSRow.Item("ID")
                                hfTextSource.Value = Replace(DBNullToStr(oDSRow.Item("VALOR_TEXT")), "'", "\'")
                                oDivTexto.InnerText = DBNullToStr(oDSRow.Item("VALOR_TEXT"))
                            End If
                            Dim textSourceId As String = String.Format("{0}_{1}", oTabItem.ContentPane.ClientID, oDSRow.Item("ID"))
                            If DBNullToStr(oDSRow.Item("VALOR_TEXT")) <> String.Empty Then
                                oDivTexto.Attributes("onclick") = "javascript:AbrirPopUpTexto('" & Replace(oDSRow.Item("DEN_" & Idioma), "'", "\'") & "','" & textSourceId & "'," & IIf(oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo, "true", "false") & ")"
                            End If
                            If Not hfTextSource Is Nothing Then otblCell.Controls.Add(hfTextSource)

                            otblCell.Controls.Add(oDivTexto)

                            otblCell.CssClass = ClaseFilaTabla
                            otblRow.Cells.Add(otblCell)
                        Else
                            oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
                            oLbl.InnerHtml = EscribirValorCampo(oDSRow)

                            If oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoNumerico AndAlso (IsDBNull(oDSRow.Item("TIPO_CAMPO_GS")) OrElse oDSRow.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.Almacen) AndAlso oDSRow.Item("TIPO") <> "6" Then
                                otblCell.Controls.Add(CargarImporteEnTabla(oLbl))
                            Else
                                otblCell.Controls.Add(oLbl)
                            End If

                            otblCell.CssClass = ClaseFilaTabla
                            otblRow.Cells.Add(otblCell)

                            'POPUPS con la descripci�n completa de textos medio y largos o archivos adjuntos o partidas presupuestarias
                            If (oDSRow.Item("TIPO") <> "6" AndAlso
                                ((IsDBNull(oDSRow.Item("TIPO_CAMPO_GS")) AndAlso oDSRow.Item("INTRO") = "0") OrElse DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Partida OrElse DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Activo OrElse DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.TipoPedido) AndAlso Not IsDBNull(oDSRow.Item("VALOR_TEXT"))) Then
                                oImgPopUp = New System.Web.UI.WebControls.HyperLink
                                oImgPopUp.ImageUrl = Imagelocation

                                Dim hfTextSource As HtmlInputHidden
                                If oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoArchivo Then
                                    oImgPopUp.NavigateUrl = "javascript:AbrirFicherosAdjuntos('" & idAdjun & "','" & oDSRow.Item("ID_CAMPO") & "','" & oInstancia.ID & "',1)"
                                ElseIf DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Partida Then
                                    oImgPopUp.NavigateUrl = "javascript:AbrirDetallePartida('" & oDSRow.Item("DEN_" & Idioma) & "','" & oDSRow.Item("PRES5") & "','" & Replace(oDSRow.Item("VALOR_TEXT"), "'", "\'") & "')"
                                ElseIf DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Activo Then
                                    oImgPopUp.NavigateUrl = "javascript:AbrirDetalleActivo('" & oDSRow.Item("VALOR_TEXT") & "')"
                                ElseIf DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.TipoPedido Then
                                    oImgPopUp.NavigateUrl = "javascript:AbrirDetalleTipoPedido('" & oDSRow.Item("VALOR_TEXT") & "')"
                                Else
                                    hfTextSource = New HtmlInputHidden
                                    hfTextSource.ID = oDSRow.Item("ID")
                                    hfTextSource.Value = Replace(oDSRow.Item("VALOR_TEXT"), "'", "\'")

                                    Dim textSourceId As String = String.Format("{0}_{1}", oTabItem.ContentPane.ClientID, oDSRow.Item("ID"))
                                    oImgPopUp.NavigateUrl = "javascript:AbrirPopUpTexto('" & Replace(oDSRow.Item("DEN_" & Idioma), "'", "\'") & "','" & textSourceId & "'," & IIf(oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo, "true", "false") & ")"
                                End If
                                otblCell = New System.Web.UI.WebControls.TableCell
                                otblCell.Width = Unit.Percentage(2)
                                otblCell.CssClass = ClaseFilaTabla
                                If Not hfTextSource Is Nothing Then
                                    otblCell.Controls.Add(hfTextSource)
                                End If
                                otblCell.Controls.Add(oImgPopUp)
                                otblRow.Cells.Add(otblCell)
                            Else
                                otblCell.ColumnSpan = 2
                            End If
                        End If
                        tblTablaTab.Rows.Add(otblRow)
                    End If
                Next
                oTabItem.ContentPane.Children.Add(Me.tblTablaTab)
            End If
            lIndex += 1
        Next
    End Sub
    ''' <summary>
    ''' Carga los grupos de la solicitud como pestanas del objeto UltraWebTab
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load(). Tiempo m�ximo: 1 sg.</remarks>
    Private Sub CargarGrupos()
        Dim lIndex As Integer = 0
        If Not oInstancia.Grupos.Grupos Is Nothing Then
            For Each oGrupo In oInstancia.Grupos.Grupos
                oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
                Dim Font As String = ObtenerValorPropiedad(".uwtDefaultTab", "font-family")
                Dim Size As String = Replace(ObtenerValorPropiedad(".uwtDefaultTab", "font-size"), "pt", "")
                oTabItem.Text = AjustarAnchoTextoPixels(oGrupo.Den(Idioma), AnchoDeTab, IIf(Font = "", "verdana", Font), IIf(Size = "", 8, CInt(Size)), False)
                oTabItem.Key = lIndex.ToString
                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible

                uwtGrupos.Tabs.Add(oTabItem)
                lIndex += 1
            Next
        End If
    End Sub
    ''' <summary>
    ''' Carga los campos del desglose en el detalle de la solicitud
    ''' </summary>
    ''' <param name="CampoID">Id en bbdd del campo padre</param>
    ''' <remarks>Llamada desde: detalleSolicConsulta/CargarCamposTabla; Tiempo m�ximo: Puede ser > 2seg dependiendo de las lineas de los desglose </remarks>
    Private Sub CargarDesglose(ByVal CampoID As Long, ByVal sTitulo As String)
        Dim oDS As DataSet
        Dim oRow As DataRow
        Dim oRowDesglose As DataRow
        Dim CampoTipoTextoMedioOLargo As Boolean = False
        Dim oCampo As FSNServer.Campo
        Dim tblDesglose As System.Web.UI.HtmlControls.HtmlTable
        Dim oRowTablaDesglose As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oCellTablaDesglose As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oLbl As System.Web.UI.HtmlControls.HtmlGenericControl
        Dim oDivDesglose As System.Web.UI.HtmlControls.HtmlGenericControl
        Dim oImgPopUp As System.Web.UI.WebControls.HyperLink
        Dim nTotalLineasDesglose As Integer
        Dim nLineaDesglose As Integer = 1
        Dim findTheseVals(2) As Object
        Dim sAdjun As String = ""
        Dim idAdjun As String = ""

        oCampo = FSNServer.Get_Object(GetType(FSNServer.Campo))
        oCampo.Id = CampoID
        oCampo.LoadInstVinculado()

        Dim bSacarNumLinea As Boolean = (oInstancia.Solicitud.TipoSolicit = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras _
                 OrElse oInstancia.Solicitud.TipoSolicit = TiposDeDatos.TipoDeSolicitud.PedidoExpress _
                 OrElse oInstancia.Solicitud.TipoSolicit = TiposDeDatos.TipoDeSolicitud.PedidoNegociado)

        oDS = oCampo.LoadInstDesglose(Idioma, oInstancia.ID, Usuario.CodPersona, oInstancia.Version, , True, Request("EsObservador"), True, , Usuario.DateFmt, , , bSacarNumLinea)

        tblDesglose = New System.Web.UI.HtmlControls.HtmlTable
        tblDesglose.Style.Add("width ", "100%")
        '------------------------------
        'TITULO del desglose
        '------------------------------
        oRowTablaDesglose = New System.Web.UI.HtmlControls.HtmlTableRow
        oCellTablaDesglose = New System.Web.UI.HtmlControls.HtmlTableCell
        oCellTablaDesglose.InnerText = sTitulo
        oCellTablaDesglose.ColSpan = 200
        oCellTablaDesglose.Attributes("class") = "cabeceraTituloDesglose"
        oRowTablaDesglose.Cells.Add(oCellTablaDesglose)
        tblDesglose.Rows.Add(oRowTablaDesglose)

        '------------------------------
        'CABECERA del desglose
        '------------------------------
        oRowTablaDesglose = New System.Web.UI.HtmlControls.HtmlTableRow
        For Each oRow In oDS.Tables(0).Rows
            If oRow.Item("VISIBLE") = 1 Then
                oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
                If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.IdsFicticios.DesgloseVinculado Then
                    oLbl.InnerHtml = Textos(49)
                ElseIf DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.IdsFicticios.NumLinea Then
                    oLbl.InnerHtml = Textos(56)
                Else
                    oLbl.InnerHtml = DBNullToSomething(oRow.Item("DEN_" & Idioma))
                End If

                oCellTablaDesglose = New System.Web.UI.HtmlControls.HtmlTableCell
                oCellTablaDesglose.ID = "COL_" + oRow.Item("ID").ToString
                oCellTablaDesglose.Controls.Add(oLbl)
                oCellTablaDesglose.Attributes("class") = "cabeceraDesglose"

                'oRow.Item("TIPO") = "6" tabla externa
                If {TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoArchivo}.Contains(oRow.Item("SUBTIPO")) _
                        AndAlso Not oRow.Item("TIPO") = "6" _
                        AndAlso (IsDBNull(oRow.Item("TIPO_CAMPO_GS")) AndAlso oRow.Item("INTRO") = 0) _
                        OrElse {TiposDeDatos.TipoCampoGS.DescrBreve, TiposDeDatos.TipoCampoGS.DescrDetallada,
                                TiposDeDatos.TipoCampoGS.Partida, TiposDeDatos.TipoCampoGS.Activo}.Contains(DBNullToSomething(oRow.Item("TIPO_CAMPO_GS"))) Then
                    oCellTablaDesglose.ColSpan = 2
                End If

                oRowTablaDesglose.Cells.Add(oCellTablaDesglose)
            End If
        Next
        tblDesglose.Rows.Add(oRowTablaDesglose)

        '------------------------------
        'DETALLE del desglose
        '------------------------------
        Dim distinctCounts As IEnumerable(Of Int32)
        Dim lineasReal As Integer() = Nothing
        Dim iMaxIndex As Integer = 0
        If oDS.Tables("LINEAS").Rows.Count > 0 Then
            iMaxIndex = oDS.Tables("LINEAS").Select("", "LINEA DESC")(0).Item("LINEA")
            distinctCounts = From row In oDS.Tables("LINEAS")
                             Select row.Field(Of Int32)("LINEA")
                             Distinct
            nTotalLineasDesglose = distinctCounts.Count
            ReDim lineasReal(nTotalLineasDesglose - 1)
            For contador As Integer = 0 To nTotalLineasDesglose - 1
                lineasReal(contador) = distinctCounts(contador)
            Next
        End If

        Do While nLineaDesglose <= iMaxIndex
            If lineasReal.Contains(nLineaDesglose) Then
                oRowTablaDesglose = New System.Web.UI.HtmlControls.HtmlTableRow
                For Each oRow In oDS.Tables(0).Rows
                    CampoTipoTextoMedioOLargo = False
                    If oRow.Item("VISIBLE") = 1 Then
                        idAdjun = ""
                        sAdjun = ""

                        findTheseVals(0) = nLineaDesglose
                        findTheseVals(1) = oCampo.IdCopiaCampo
                        findTheseVals(2) = oRow.Item("ID")
                        oRowDesglose = oDS.Tables(2).Rows.Find(findTheseVals)

                        oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")

                        oCellTablaDesglose = New System.Web.UI.HtmlControls.HtmlTableCell
                        oCellTablaDesglose.Attributes("class") = IIf(nLineaDesglose Mod 2 = 0, "ugfilaalttablaDesglose", "ugfilatablaDesglose")
                        oCellTablaDesglose.NoWrap = True
                        If oRowDesglose Is Nothing Then
                            If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.IdsFicticios.NumLinea Then
                                oLbl.InnerHtml = nLineaDesglose
                                oCellTablaDesglose.Width = "60px"
                                oCellTablaDesglose.Align = "right"
                            Else
                                oLbl.InnerHtml = "&nbsp;"
                            End If
                            'A�adimos la etiqueta a la celda
                            oCellTablaDesglose.Controls.Add(oLbl)
                            'A�adimos la celda a la fila
                            oRowTablaDesglose.Cells.Add(oCellTablaDesglose)
                        Else
                            '- Si es una TABLA EXTERNA ya sea de tipo string, num�rico o fecha se obtiene la denominaci�n de VALOR_TEXT_DEN
                            '- Si es un CAMPO DEL SISTEMA tb se obtiene la denominaci�n de VALOR_TEXT_DEN
                            If oRow.Item("TIPO") = "6" Then
                                If Not IsDBNull(oRowDesglose.Item("VALOR_TEXT_DEN")) Then
                                    oLbl.InnerHtml = DevolverTablaExterna(oRowDesglose.Item("VALOR_TEXT_DEN"))
                                End If
                            ElseIf (Not IsDBNull(oRowDesglose.Item("TIPO_CAMPO_GS")) AndAlso (oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.ProveedorAdj OrElse oRowDesglose.Item("TIPO_CAMPO_GS") >= TiposDeDatos.TipoCampoGS.Proveedor) AndAlso oRowDesglose.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.DenArticulo AndAlso oRowDesglose.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo AndAlso oRowDesglose.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.CodArticulo) Then
                                If Not IsDBNull(oRowDesglose.Item("VALOR_TEXT_DEN")) Then
                                    If Not IsDBNull(oRowDesglose.Item("TIPO_CAMPO_GS")) AndAlso (oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.PRES1 OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres2 OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres3 OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres4) Then
                                        oLbl.InnerHtml = DevolverPresupuestosConPorcentajes(oRowDesglose.Item("VALOR_TEXT"), oRowDesglose.Item("VALOR_TEXT_DEN"))
                                    Else
                                        oLbl.InnerHtml = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oRowDesglose.Item("VALOR_TEXT_DEN"), Idioma)
                                    End If
                                    If oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.ProveedorAdj OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Proveedor OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.OrganizacionCompras OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Centro Then
                                        oLbl.InnerHtml = oRowDesglose.Item("VALOR_TEXT") & " - " & oLbl.InnerHtml
                                    ElseIf oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Material Then
                                        oLbl.InnerHtml = DevolverMaterialConCodigoUltimoNivel(oRowDesglose.Item("VALOR_TEXT"), oLbl.InnerHtml)
                                    ElseIf oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.CentroCoste Then
                                        If InStrRev(oRowDesglose.Item("VALOR_TEXT"), "#") > 0 Then
                                            oLbl.InnerHtml = Right(oRowDesglose.Item("VALOR_TEXT"), Len(oRowDesglose.Item("VALOR_TEXT")) - InStrRev(oRowDesglose.Item("VALOR_TEXT"), "#")) & " - " & oLbl.InnerHtml
                                        Else
                                            oLbl.InnerHtml = oRowDesglose.Item("VALOR_TEXT") & " - " & oLbl.InnerHtml
                                        End If
                                    ElseIf oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Partida Then
                                        If InStrRev(oRowDesglose.Item("VALOR_TEXT"), "#") > 0 Then
                                            If InStrRev(oRowDesglose.Item("VALOR_TEXT"), "|") > 0 Then 'si no est� en el nivel 1, si no m�s abajo
                                                oLbl.InnerHtml = Right(oRowDesglose.Item("VALOR_TEXT"), Len(oRowDesglose.Item("VALOR_TEXT")) - InStrRev(oRowDesglose.Item("VALOR_TEXT"), "|")) & " - " & oLbl.InnerHtml
                                            Else
                                                oLbl.InnerHtml = Right(oRowDesglose.Item("VALOR_TEXT"), Len(oRowDesglose.Item("VALOR_TEXT")) - InStrRev(oRowDesglose.Item("VALOR_TEXT"), "#")) & " - " & oLbl.InnerHtml
                                            End If
                                        Else
                                            oLbl.InnerHtml = oRowDesglose.Item("VALOR_TEXT") & " - " & oLbl.InnerHtml
                                        End If
                                    ElseIf oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Activo Then
                                        oLbl.InnerHtml = oRowDesglose.Item("VALOR_TEXT") & " - " & oLbl.InnerHtml
                                    End If
                                End If
                            End If
                            If oLbl.InnerHtml = "" Then 'por si el campo DEN est� vac�o. Todav�a no se guardan los datos en este campo.
                                Select Case oRow.Item("SUBTIPO")
                                    Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoTextoLargo
                                        If (oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoMedio Or oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo) _
                                            AndAlso (IsDBNull(oRow.Item("TIPO_CAMPO_GS")) _
                                            OrElse {TiposDeDatos.TipoCampoGS.DescrBreve, TiposDeDatos.TipoCampoGS.DescrDetallada}.Contains(oRow.Item("TIPO_CAMPO_GS"))) Then
                                            'De tipo Texto medio o largo 
                                            CampoTipoTextoMedioOLargo = True
                                            Dim oDivTexto As New HtmlControls.HtmlGenericControl("DIV")
                                            oDivTexto.ID = "_t"

                                            oDivTexto.Style.Add("width", "200px")

                                            oDivTexto.Style.Add("display", "block")
                                            oDivTexto.Style.Add("overflow", "hidden")
                                            oDivTexto.Style.Add("text-overflow", "ellipsis")
                                            oDivTexto.Style.Add("white-space", "nowrap")

                                            Dim hfTextSource As HtmlInputHidden = Nothing
                                            If Not oRowDesglose.Item("VALOR_TEXT") Is Nothing Then
                                                hfTextSource = New HtmlInputHidden
                                                hfTextSource.ID = oRow.Item("ID")
                                                hfTextSource.Value = Replace(DBNullToStr(oRowDesglose.Item("VALOR_TEXT")), "'", "\'")
                                                oDivTexto.InnerText = DBNullToStr(oRowDesglose.Item("VALOR_TEXT"))
                                            End If
                                            Dim textSourceId As String = String.Format("{0}_{1}", oTabItem.ContentPane.ClientID, oRow.Item("ID"))
                                            If DBNullToStr(oRowDesglose.Item("VALOR_TEXT")) <> String.Empty Then
                                                oDivTexto.Attributes("onclick") = "javascript:AbrirPopUpTexto('" & Replace(oRow.Item("DEN_" & Idioma), "'", "\'") & "','" & textSourceId & "'," & IIf(oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo, "true", "false") & ")"
                                            End If
                                            If Not hfTextSource Is Nothing Then
                                                oCellTablaDesglose.Controls.Add(hfTextSource)
                                            End If

                                            oCellTablaDesglose.Controls.Add(oDivTexto)
                                        Else
                                            oLbl.InnerHtml = DBNullToSomething(oRowDesglose.Item("VALOR_TEXT"))

                                            If oRow.Item("INTRO") = "1" Then
                                                Dim oRowLista() As DataRow
                                                Dim Lista = New DataSet
                                                Lista.Merge(oRow.GetChildRows("REL_CAMPO_LISTA"))
                                                If Lista.Tables.Count > 0 Then
                                                    If DBNullToSomething(oRowDesglose.Item("VALOR_NUM").ToString) <> Nothing Then
                                                        oRowLista = Lista.Tables(0).Select("ID = " + oRowDesglose.Item("CAMPO_HIJO").ToString + " and ORDEN=" + oRowDesglose.Item("VALOR_NUM").ToString)
                                                    Else
                                                        oRowLista = Lista.Tables(0).Select("ID = " + oRowDesglose.Item("CAMPO_HIJO").ToString + " and VALOR_TEXT_" & Idioma & " =" + StrToSQLNULL(oRowDesglose.Item("VALOR_TEXT").ToString))
                                                    End If
                                                    If oRowLista.Length > 0 Then
                                                        oLbl.InnerHtml = oRowLista(0).Item("VALOR_TEXT_" & Idioma)
                                                    End If
                                                End If
                                            End If
                                            If (oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoMedio OrElse oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo) AndAlso Len(oLbl.InnerHtml) > 80 Then
                                                oLbl.InnerHtml = Mid(oLbl.InnerHtml, 1, 80) & "..."
                                            End If
                                        End If
                                    Case TiposDeDatos.TipoGeneral.TipoNumerico
                                        oCellTablaDesglose.Align = "right"
                                        If Not IsDBNull(oRowDesglose.Item("VALOR_NUM")) Then
                                            oLbl.InnerHtml = QuitarDecimalSiTodoCeros(FSNLibrary.FormatNumber(oRowDesglose.Item("VALOR_NUM"), Usuario.NumberFormat))
                                        End If
                                    Case TiposDeDatos.TipoGeneral.TipoFecha
                                        If IsDBNull(oRowDesglose.Item("VALOR_FEC")) Then
                                            oLbl.InnerHtml = "&nbsp;"
                                        Else
                                            oLbl.InnerHtml = FormatDate(DevolverFechaRelativaA(DBNullToSomething(oRowDesglose.Item("VALOR_NUM")), DBNullToSomething(oRowDesglose.Item("VALOR_FEC"))), Usuario.DateFormat)
                                        End If
                                    Case TiposDeDatos.TipoGeneral.TipoBoolean
                                        If Not IsDBNull(oRowDesglose.Item("VALOR_BOOL")) Then
                                            oLbl.InnerHtml = IIf(oRowDesglose.Item("VALOR_BOOL") = 1, Textos(7), Textos(8))
                                        End If
                                    Case TiposDeDatos.TipoGeneral.TipoArchivo
                                        oLbl.Attributes("class") = "TipoArchivo"

                                        Dim oDSRowAdjun As DataRow
                                        For Each oDSRowAdjun In oRowDesglose.GetChildRows("REL_LINEA_ADJUNTO")
                                            idAdjun += oDSRowAdjun.Item("ID").ToString() + "xx"
                                            sAdjun += oDSRowAdjun.Item("NOM") + " (" + FSNLibrary.FormatNumber((DBNullToSomething(oDSRowAdjun.Item("DATASIZE")) / 1024), Usuario.NumberFormat) + " Kb.), "
                                        Next
                                        If sAdjun <> "" Then
                                            sAdjun = sAdjun.Substring(0, sAdjun.Length - 2)
                                            idAdjun = idAdjun.Substring(0, idAdjun.Length - 2)

                                            Dim enlace As String
                                            If oRowDesglose.GetChildRows("REL_LINEA_ADJUNTO").Length > 1 Then
                                                enlace = "<a class='aPMWeb' href=""javascript:AbrirFicherosAdjuntos('" & idAdjun & "','" & oRow.Item("ID") & "','" & oInstancia.ID & "',3)"">" & sAdjun & "</a>"
                                            Else
                                                enlace = "<a class='aPMWeb' href=""javascript:IrAAdjunto('" & ObtenerPathAdjunto(idAdjun, 3) & "')"">" & sAdjun & "</a>"
                                            End If

                                            oLbl.InnerHtml = enlace
                                        End If
                                    Case TiposDeDatos.TipoGeneral.TipoEditor
                                        If Not IsDBNull(oRowDesglose.Item("VALOR_TEXT")) Then
                                            Dim hfTextSource As HtmlInputHidden
                                            Dim textSourceId As String = String.Format("{0}_{1}", oTabItem.ContentPane.ClientID, oRow.Item("ID"))

                                            hfTextSource = New HtmlInputHidden
                                            hfTextSource.ID = oRow.Item("ID")
                                            hfTextSource.Value = Replace(oRowDesglose.Item("VALOR_TEXT"), "'", "\'")

                                            Dim oAnchor As New HtmlAnchor
                                            oAnchor.InnerText = Textos(50)
                                            oAnchor.Attributes("onclick") = "AbrirEditor('" & oRow.Item("DEN_" & Idioma) & "','" & textSourceId & "')"

                                            otblCell = New System.Web.UI.WebControls.TableCell
                                            otblCell.CssClass = ClaseFilaTabla
                                            otblCell.Controls.Add(hfTextSource)
                                            oCellTablaDesglose.Controls.Add(hfTextSource)
                                            oCellTablaDesglose.Controls.Add(oAnchor)
                                        End If
                                    Case TiposDeDatos.TipoGeneral.TipoEnlace
                                        If Not IsDBNull(oRowDesglose.Item("VALOR_TEXT")) Then
                                            Dim oAnchor As New HtmlAnchor
                                            oAnchor.InnerText = oRow.Item("VALOR_TEXT")
                                            oAnchor.Attributes("onclick") = "AbrirEnlace('" & Replace(oRow.Item("VALOR_TEXT_DEN"), "'", "\'") & "')"

                                            otblCell = New System.Web.UI.WebControls.TableCell
                                            otblCell.CssClass = ClaseFilaTabla
                                            oCellTablaDesglose.Controls.Add(oAnchor)
                                        End If
                                End Select
                            End If
                            If (oRow.Item("SUBTIPO") <> TiposDeDatos.TipoGeneral.TipoEditor AndAlso oRow.Item("SUBTIPO") <> TiposDeDatos.TipoGeneral.TipoEnlace) _
                                    OrElse IsDBNull(oRowDesglose.Item("VALOR_TEXT")) Then
                                If oLbl.InnerHtml = "" Then oLbl.InnerHtml = "&nbsp;"
                                'A�adimos la etiqueta a la celda
                                If Not CampoTipoTextoMedioOLargo Then oCellTablaDesglose.Controls.Add(oLbl)
                            End If

                            'A�adimos la celda a la fila
                            oRowTablaDesglose.Cells.Add(oCellTablaDesglose)

                            If Not oRow.Item("TIPO") = "6" _
                                        AndAlso {TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoArchivo}.Contains(oRow.Item("SUBTIPO")) _
                                        AndAlso CampoTipoTextoMedioOLargo _
                                        AndAlso (IsDBNull(oRow.Item("TIPO_CAMPO_GS")) AndAlso oRow.Item("INTRO") = 0) _
                                        OrElse DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.DescrBreve _
                                        OrElse DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.DescrDetallada _
                                        OrElse DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Partida _
                                        OrElse DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Activo Then
                                oCellTablaDesglose = New System.Web.UI.HtmlControls.HtmlTableCell
                                oCellTablaDesglose.Attributes("class") = IIf(nLineaDesglose Mod 2 = 0, "ugfilaalttablaDesglose", "ugfilatablaDesglose")
                                'A�adimos el bot�n para abrir el popup SOLO si hay archivos
                                If sAdjun <> "" OrElse Not IsDBNull(oRowDesglose.Item("VALOR_TEXT")) Then
                                    oImgPopUp = New System.Web.UI.WebControls.HyperLink
                                    oImgPopUp.ImageUrl = Imagelocation
                                    If oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoArchivo Then
                                        oImgPopUp.NavigateUrl = "javascript:AbrirFicherosAdjuntos('" & idAdjun & "','" & oRow.Item("ID") & "','" & oInstancia.ID & "',3)"
                                    ElseIf DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Partida Then
                                        oImgPopUp.NavigateUrl = "javascript:AbrirDetallePartida('" & oRow.Item("DEN_" & Idioma) & "','" & oRow.Item("PRES5") & "','" & oRowDesglose.Item("VALOR_TEXT") & "')"
                                    ElseIf DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Activo Then
                                        oImgPopUp.NavigateUrl = "javascript:AbrirDetalleActivo('" & oRowDesglose.Item("VALOR_TEXT") & "')"
                                    Else
                                        Dim hfTextSource As New HtmlInputHidden
                                        hfTextSource.ID = nLineaDesglose.ToString & "_" & DBNullToStr(oRow.Item("ID"))
                                        hfTextSource.Value = Replace(oRowDesglose.Item("VALOR_TEXT"), "'", "\'")
                                        oCellTablaDesglose.Controls.Add(hfTextSource)

                                        Dim textSourceId As String = String.Format("{0}_{1}", oTabItem.ContentPane.ClientID, nLineaDesglose.ToString & "_" & DBNullToStr(oRow.Item("ID")))
                                        oImgPopUp.NavigateUrl = "javascript:AbrirPopUpTexto('" & Replace(oRow.Item("DEN_" & Idioma), "'", "\'") & "','" & textSourceId & "'," & IIf(oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo, "true", "false") & ")"
                                    End If
                                    oCellTablaDesglose.Controls.Add(oImgPopUp)
                                Else
                                    oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
                                    oLbl.InnerHtml = "&nbsp;"
                                    oCellTablaDesglose.Controls.Add(oLbl)
                                End If
                                'A�adimos la celda a la fila
                                oRowTablaDesglose.Cells.Add(oCellTablaDesglose)
                            End If
                        End If
                    End If
                Next
                'A�adimos la fila a la tabla
                tblDesglose.Rows.Add(oRowTablaDesglose)
            End If
            nLineaDesglose = nLineaDesglose + 1
        Loop

        otblCell.Controls.Add(tblDesglose)
        otblCell.ColumnSpan = 3
        otblRow.Cells.Add(otblCell)
    End Sub
    ''' <summary>
    ''' Escribe la denominaci�n del campo en el formato en el que se le muestra al usuario
    ''' </summary>
    ''' <param name="oDSRow">Datarow con los datos relacionados con el campo a tratar</param>
    ''' <returns>Devuelve un string con la denominaci�n en el formato correcto y en el idioma del usuario (en el caso de campos multiusuario)</returns>
    ''' <remarks>Llamada desde: CargarCamposTabla; Tiempo m�ximo: 0,1 sg.</remarks>
    Private Function EscribirValorCampo(ByVal oDSRow As DataRow) As String
        Dim sValor As String
        '- Si es una TABLA EXTERNA ya sea de tipo string, num�rico o fecha se obtiene la denominaci�n de VALOR_TEXT_DEN
        '- Si es un CAMPO DEL SISTEMA tb se obtiene la denominaci�n de VALOR_TEXT_DEN
        If oDSRow.Item("TIPO") = "6" Then
            If Not IsDBNull(oDSRow.Item("VALOR_TEXT_DEN")) Then
                sValor = DevolverTablaExterna(oDSRow.Item("VALOR_TEXT_DEN"))
            End If
        ElseIf (Not IsDBNull(oDSRow.Item("TIPO_CAMPO_GS")) AndAlso (oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.ProveedorAdj OrElse oDSRow.Item("TIPO_CAMPO_GS") >= TiposDeDatos.TipoCampoGS.Proveedor) AndAlso oDSRow.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.DenArticulo AndAlso oDSRow.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo AndAlso oDSRow.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.CodArticulo) Then
            If Not IsDBNull(oDSRow.Item("VALOR_TEXT_DEN")) Then
                If Not IsDBNull(oDSRow.Item("TIPO_CAMPO_GS")) AndAlso (oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.PRES1 OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres2 OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres3 OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres4) Then
                    sValor = DevolverPresupuestosConPorcentajes(oDSRow.Item("VALOR_TEXT"), oDSRow.Item("VALOR_TEXT_DEN"))
                Else
                    sValor = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oDSRow.Item("VALOR_TEXT_DEN"), Idioma)
                End If
                If oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.ProveedorAdj OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Proveedor OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.OrganizacionCompras OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Centro Then
                    sValor = oDSRow.Item("VALOR_TEXT") & " - " & sValor
                ElseIf oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Material Then
                    sValor = DevolverMaterialConCodigoUltimoNivel(oDSRow.Item("VALOR_TEXT"), sValor)
                ElseIf oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.CentroCoste Then
                    If InStrRev(oDSRow.Item("VALOR_TEXT"), "#") > 0 Then
                        sValor = Right(oDSRow.Item("VALOR_TEXT"), Len(oDSRow.Item("VALOR_TEXT")) - InStrRev(oDSRow.Item("VALOR_TEXT"), "#")) & " - " & sValor
                    Else
                        sValor = oDSRow.Item("VALOR_TEXT") & " - " & sValor
                    End If
                ElseIf oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Partida Then
                    If InStrRev(oDSRow.Item("VALOR_TEXT"), "#") > 0 Then
                        If InStrRev(oDSRow.Item("VALOR_TEXT"), "|") > 0 Then 'si no est� en el nivel 1, si no m�s abajo
                            sValor = Right(oDSRow.Item("VALOR_TEXT"), Len(oDSRow.Item("VALOR_TEXT")) - InStrRev(oDSRow.Item("VALOR_TEXT"), "|")) & " - " & sValor
                        Else
                            sValor = Right(oDSRow.Item("VALOR_TEXT"), Len(oDSRow.Item("VALOR_TEXT")) - InStrRev(oDSRow.Item("VALOR_TEXT"), "#")) & " - " & sValor
                        End If
                    Else
                        sValor = oDSRow.Item("VALOR_TEXT") & " - " & sValor
                    End If
                ElseIf oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Activo Then
                    sValor = oDSRow.Item("VALOR_TEXT") & " - " & sValor
                ElseIf oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.TipoPedido Then
                    sValor = oDSRow.Item("VALOR_TEXT") & " - " & sValor
                End If
            End If
        End If
        If sValor = "" Then 'por si el campo DEN est� vac�o. Todav�a no se guardan los datos en este campo.
            Select Case oDSRow.Item("SUBTIPO")
                Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoTextoLargo
                    sValor = DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
                    If (oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoMedio OrElse oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo) AndAlso Len(sValor) > 80 Then
                        sValor = Mid(sValor, 1, 80) & "..."
                    End If
                Case TiposDeDatos.TipoGeneral.TipoNumerico
                    If Not IsDBNull(oDSRow.Item("VALOR_NUM")) Then
                        sValor = QuitarDecimalSiTodoCeros(FSNLibrary.FormatNumber(oDSRow.Item("VALOR_NUM"), Usuario.NumberFormat))
                    End If
                Case TiposDeDatos.TipoGeneral.TipoFecha
                    If IsDBNull(oDSRow.Item("VALOR_FEC")) Then
                        sValor = "&nbsp;"
                    Else
                        sValor = FormatDate(DevolverFechaRelativaA(DBNullToSomething(oDSRow.Item("VALOR_NUM")), DBNullToSomething(oDSRow.Item("VALOR_FEC"))), Usuario.DateFormat)
                    End If
                Case TiposDeDatos.TipoGeneral.TipoBoolean
                    If Not IsDBNull(oDSRow.Item("VALOR_BOOL")) Then
                        sValor = IIf(oDSRow.Item("VALOR_BOOL") = 1, Textos(7), Textos(8))
                    End If
                Case TiposDeDatos.TipoGeneral.TipoArchivo
                    sAdjun = ""
                    idAdjun = ""
                    Dim oDSRowAdjun As System.Data.DataRow
                    For Each oDSRowAdjun In oDSRow.GetChildRows("REL_CAMPO_ADJUN")
                        idAdjun += oDSRowAdjun.Item("ID").ToString() + "xx"
                        sAdjun += oDSRowAdjun.Item("NOM") + " (" + FSNLibrary.FormatNumber((oDSRowAdjun.Item("DATASIZE") / 1024), Usuario.NumberFormat) + " Kb.), "
                    Next
                    If sAdjun <> "" Then
                        sAdjun = sAdjun.Substring(0, sAdjun.Length - 2)
                        idAdjun = idAdjun.Substring(0, idAdjun.Length - 2)
                    End If
                    If Len(sAdjun) > 90 Then
                        sValor = Mid(sAdjun, 1, 90) & "..."
                    Else
                        sValor = sAdjun
                    End If
                    If sAdjun <> "" Then
                        Dim enlace As String
                        If oDSRow.GetChildRows("REL_CAMPO_ADJUN").Length > 1 Then
                            enlace = "<a class='aPMWeb' href=""javascript:AbrirFicherosAdjuntos('" & idAdjun & "','" & oDSRow.Item("ID_CAMPO") & "','" & oInstancia.ID & "',1)"">" & sValor & "</a>"
                        Else
                            enlace = "<a class='aPMWeb' href=""javascript:IrAAdjunto('" & ObtenerPathAdjunto(idAdjun) & "')"">" & sValor & "</a>"
                        End If

                        sValor = enlace
                    End If
            End Select
        End If
        If Trim(sValor) = "" Then sValor = "&nbsp;"

        EscribirValorCampo = sValor
    End Function
    ''' <summary>
    ''' Para que los importes salgan alineados a la derecha.
    ''' </summary>
    ''' <param name="oLbl">Control etiqueta</param>
    ''' <returns>Devuelve un HtmlTable</returns>
    ''' <remarks>LLamada desde: CargarCamposTabla; Tiempo m�ximo: 0,1 sg.</remarks>
    Private Function CargarImporteEnTabla(ByVal oLbl As HtmlGenericControl) As HtmlTable
        Dim Tbl As System.Web.UI.HtmlControls.HtmlTable
        Dim Row As System.Web.UI.HtmlControls.HtmlTableRow
        Dim Cell As System.Web.UI.HtmlControls.HtmlTableCell

        Row = New System.Web.UI.HtmlControls.HtmlTableRow
        Cell = New System.Web.UI.HtmlControls.HtmlTableCell
        Cell.Controls.Add(oLbl)
        Cell.Align = "right"
        Cell.Attributes("class") = "CampoSoloLectura"
        Row.Cells.Add(Cell)

        Tbl = New System.Web.UI.HtmlControls.HtmlTable
        Tbl.Align = "left"
        Tbl.Width = "100px"
        Tbl.CellPadding = 0
        Tbl.CellSpacing = 0
        Tbl.Rows.Add(Row)

        CargarImporteEnTabla = Tbl
    End Function
    ''' <summary>
    ''' Quita los decimales si el n� es xx,00 para mostrarlo como un n� entero.
    ''' </summary>
    ''' <param name="sValor">Cadena con el n�</param>
    ''' <returns>N� modificado</returns>
    ''' <remarks>Llamada desde: EscribirValorCampo; Tiempo m�ximo: 0 sg.</remarks>
    Private Function QuitarDecimalSiTodoCeros(ByVal sValor As String) As String
        Dim decimales As String = ""
        If Usuario.PrecisionFmt > 0 AndAlso Right(sValor, Usuario.PrecisionFmt + 1) = Usuario.DecimalFmt & decimales.PadRight(Usuario.PrecisionFmt, "0") Then
            sValor = Left(sValor, Len(sValor) - Usuario.PrecisionFmt - 1)
        End If
        QuitarDecimalSiTodoCeros = sValor
    End Function
    Private Function DevolverMaterialConCodigoUltimoNivel(ByVal sValorText As String, ByVal sValorTextDen As String) As String
        Dim sMat As String = DBNullToSomething(sValorText)
        Dim arrMat(4) As String
        Dim lLongCod As Integer
        Dim i As Integer

        For i = 1 To 4
            arrMat(i) = ""
        Next i

        i = 1
        While Trim(sMat) <> ""
            Select Case i
                Case 1
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                Case 2
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                Case 3
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                Case 4
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN4
            End Select
            arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
            sMat = Mid(sMat, lLongCod + 1)
            i = i + 1
        End While

        If Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles Then
            If arrMat(4) <> "" Then
                Return arrMat(1) + " - " + arrMat(2) + " - " + arrMat(3) + " - " + arrMat(4) + " - " + sValorTextDen
            ElseIf arrMat(3) <> "" Then
                Return arrMat(1) + " - " + arrMat(2) + " - " + arrMat(3) + " - " + sValorTextDen
            ElseIf arrMat(2) <> "" Then
                Return arrMat(1) + " - " + arrMat(2) + " - " + sValorTextDen
            ElseIf arrMat(1) <> "" Then
                Return arrMat(1) + " - " + sValorTextDen
            End If
        Else
            If arrMat(4) <> "" Then
                Return arrMat(4) + " - " + sValorTextDen
            ElseIf arrMat(3) <> "" Then
                Return arrMat(3) + " - " + sValorTextDen
            ElseIf arrMat(2) <> "" Then
                Return arrMat(2) + " - " + sValorTextDen
            ElseIf arrMat(1) <> "" Then
                Return arrMat(1) + " - " + sValorTextDen
            End If
        End If
    End Function
    Private Function DevolverPresupuestosConPorcentajes(ByVal sValorText As String, ByVal sValorTextDen As String) As String
        Dim arrPresupuestos() As String = sValorText.Split("#")
        Dim arrDenominaciones() As String = sValorTextDen.ToString().Split("#")
        Dim oPresup As String
        Dim iContadorPres As Integer
        Dim arrPresup(2) As String
        Dim dPorcent As Double
        Dim sValor As String

        iContadorPres = 0
        For Each oPresup In arrPresupuestos
            iContadorPres = iContadorPres + 1
            arrPresup = oPresup.Split("_")
            'Los presupuestos siempre llevan el porcentaje como 0.xx salvo cuando es 100% que llevan 1
            dPorcent = Numero(arrPresup(2), ".", "")
            sValor = arrDenominaciones(iContadorPres - 1) & " (" & dPorcent.ToString("0.00%", Usuario.NumberFormat) & "); " & sValor
        Next
        DevolverPresupuestosConPorcentajes = sValor
    End Function
    ''' <summary>
    ''' Devuelve el valor del campo tipo tabla externa extra�do del campo VALOR_TEXT_DEN. En funci�n del tipo (s: string, n: num�rico, d: fecha)
    ''' </summary>
    ''' <param name="sValorTextDen">Contenido del campo VALOR_TEXT_DEN</param>
    ''' <returns>El contenido en el formato correcto.</returns>
    ''' <remarks>LLamada desde: Page_Load; Tiempo m�ximo: 0,01 sg</remarks>
    Private Function DevolverTablaExterna(ByVal sValorTextDen As String) As String
        Dim sValor As String
        Dim sTxt As String = sValorTextDen.ToString()
        Dim sTxt2 As String
        If sTxt.IndexOf("]#[") > 0 Then
            sTxt2 = sTxt.Substring(2, sTxt.IndexOf("]#[") - 2)
        Else
            sTxt2 = sTxt.Substring(2, sTxt.Length - 3)
        End If
        Select Case sTxt.Substring(0, 1)
            Case "s"
                sValor = sTxt2
            Case "n"
                sValor = Fullstep.FSNLibrary.FormatNumber(Numero(sTxt2, "."), Usuario.NumberFormat)
            Case "d"
                sValor = CType(sTxt2, Date).ToString("d", Usuario.DateFormat)
        End Select
        If sTxt.IndexOf("]#[") > 0 Then
            sValor = sValor & " "
            sTxt2 = sTxt.Substring(sTxt.IndexOf("]#[") + 3, sTxt.Length - sTxt.IndexOf("]#[") - 5)
            Select Case sTxt.Substring(sTxt.Length - 1, 1)
                Case "s"
                    sValor = sValor & sTxt2
                Case "n"
                    sValor = sValor & Fullstep.FSNLibrary.FormatNumber(Numero(sTxt2, "."), Usuario.NumberFormat)
                Case "d"
                    sValor = sValor & CType(sTxt2, Date).ToString("d", Usuario.DateFormat)
            End Select
        End If
        DevolverTablaExterna = sValor
    End Function
    ''' <summary>
    ''' Guarda el archivo a disco y llama a download.aspx pasandole el path para mostrarlo o descargarlo
    ''' </summary>
    ''' <param name="Adjuntoid">id del archivo</param>
    ''' <param name="Tipo">Tipo (1: si es un campo normal, 2: el archivo es nuevo, 3: es un campo de un desglose)</param>
    ''' <returns>El enlace para que se abra el archivo con su nombre, path y tama�o.</returns>
    ''' <remarks></remarks>
    Private Function ObtenerPathAdjunto(ByVal Adjuntoid As Long, Optional ByVal Tipo As Integer = 1) As String
        Dim sPath As String
        Dim cAdjunto As FSNServer.Adjunto

        cAdjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))
        cAdjunto.Id = Adjuntoid
        cAdjunto.LoadInstFromRequest(Tipo)
        If cAdjunto.dataSize = -1000 Then
            cAdjunto.LoadFromRequest(Tipo)
            sPath = cAdjunto.SaveAdjunToDisk(Tipo)
        Else
            sPath = cAdjunto.SaveAdjunToDisk(Tipo, True)
        End If

        Dim arrPath() As String = sPath.Split("\")
        ObtenerPathAdjunto = ConfigurationManager.AppSettings("rutaFS") & "_Common/download.aspx?path=" + arrPath(UBound(arrPath) - 1) + "&nombre=" + arrPath(UBound(arrPath)) + "&datasize=" + cAdjunto.dataSize.ToString
    End Function
	Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
		If Not IsPostBack() Then
			CargarCamposTabla()
		End If
	End Sub
End Class
