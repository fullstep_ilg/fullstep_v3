<%@ Page Language="vb" AutoEventWireup="false" Codebehind="detectscreen.aspx.vb" Inherits="Fullstep.FSNWeb.detectscreen" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<script id="clientEventHandlersJS" language="javascript">
		function window_onload() {
			if ('deviceXDPI' in screen) {
				DpiX = screen.deviceXDPI;
				DpiY = screen.deviceYDPI;
			} else {
				DpiX = 96;
				DpiY = 96;
			}
top.location.href = "detectscreen.aspx?action=set&DpiX=" + DpiX + "&DpiY=" + DpiY + "&Instancia=" + document.getElementById("Instancia").value + "&Codigo=" + document.getElementById("CodContrato").value + "&NumFactura=" + document.getElementById("hidNumFactura").value
			
		}
	</script>	
	<body language="javascript" onload="return window_onload()">
		<form id="frmDetect" method="post" runat="server">
			<INPUT id="Instancia" style="Z-INDEX: 101; LEFT: 16px; POSITION: absolute; TOP: 16px" type="hidden"
				name="Instancia" runat="server">
			<INPUT id="CodContrato" style="Z-INDEX: 101; LEFT: 16px; POSITION: absolute; TOP: 36px" type="hidden"
				name="CodContrato" runat="server">	
            <input id="hidNumFactura"  type="hidden"
				name="hidNumFactura" runat="server"/>					
		</form>
	</body>
</html>
