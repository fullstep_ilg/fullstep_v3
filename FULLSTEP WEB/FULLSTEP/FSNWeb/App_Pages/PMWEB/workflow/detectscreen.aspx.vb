Public Class detectscreen
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Instancia As System.Web.UI.HtmlControls.HtmlInputHidden
	
    Protected WithEvents CodContrato As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents hidNumFactura As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Instancia.Value = Request("Instancia")
        CodContrato.Value = Request("Codigo")
        hidNumFactura.Value = Request("NumFactura")
        If Not (Request.QueryString("action") Is Nothing) Then
            Session("DpiX") = Request.QueryString("DpiX")
            Session("DpiY") = Request.QueryString("DpiY")
            Response.Redirect("flowDiagramLaunch.aspx?Instancia=" & Request("Instancia") & "&Codigo=" & Request("Codigo") & "&NumFactura=" & Request("NumFactura"))
        End If
    End Sub

End Class
