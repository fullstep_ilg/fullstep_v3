﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization
Imports Fullstep.FSNServer

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class AutoCompletePMWEB
    Inherits System.Web.Services.WebService

    ''' <summary>
    ''' Funcion que devuelve los valores de autocompletado
    ''' </summary>
    ''' <param name="prefixText">texto ya introducido en la caja de texto</param>
    ''' <param name="count">nº de palabras que se devuelven</param>
    ''' <param name="contextKey">nº de campo</param>
    ''' <returns>retorna los valores para autocompletar el texto ya introducido en la caja de texto</returns>
    ''' <remarks>Llamada desde:GeneralEntry.vb</remarks>
    <System.Web.Services.WebMethod(True)>
    Public Function DevolverValores(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        Dim oCampo As FSNServer.Campo = FSNServer.Get_Object(GetType(FSNServer.Campo))
        Dim MiDataSet As New DataSet
        Dim sbusqueda

        MiDataSet = oCampo.DevolverValoresCampo(contextKey, FSNUser.CodPersona)

        sbusqueda = From Datos In MiDataSet.Tables(0)
                    Let w = UCase(DBNullToStr("VALOR_TEXT"))
                    Where Datos.Field(Of String)("VALOR_TEXT").ToUpper Like "" & strToSQLLIKE(prefixText, True) & "*"
                    Select Datos.Item("VALOR_TEXT") Distinct.Take(count).ToArray()


        Dim resul() As String
        ReDim resul(UBound(sbusqueda))
        For i As Integer = 0 To UBound(sbusqueda)
            resul(i) = sbusqueda(i).ToString
        Next
        Return resul


    End Function

    <System.Web.Services.WebMethod(True)>
    Public Function GetEmpresas(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        Dim oEmpresas As FSNServer.Empresas = FSNServer.Get_Object(GetType(FSNServer.Empresas))
        Dim MiDataTable As New DataTable
        Dim arrayPalabras As String()
        Dim sbusqueda

        If InStr(contextKey, "**") = 0 Then 'alta de contrato
            Dim ProcesoContrato() As String = Split(contextKey, "_")
            MiDataTable = oEmpresas.ObtenerEmpresas(FSNUser.Idioma, , , , , , , , , , , , ProcesoContrato(0), ProcesoContrato(1), ProcesoContrato(2), FSNUser.Pyme)
        Else 'detalle de contrato
            Dim Campos() As String = Split(contextKey, "**")
            If UBound(Campos) = 1 Then
                MiDataTable = oEmpresas.ObtenerEmpresas(FSNUser.Idioma, , , , , , , , CLng(Campos(0)), CLng(Campos(1)), , , , , , FSNUser.Pyme)
            Else
                MiDataTable = oEmpresas.ObtenerEmpresas(FSNUser.Idioma, , , , , , , , CLng(Campos(0)), CLng(Campos(1)), CLng(Campos(2)), CLng(Campos(3)), , , , FSNUser.Pyme)
            End If
        End If

        arrayPalabras = Split(prefixText)

        For i = 1 To arrayPalabras.Length - 1 'desde la segunda palabra
            arrayPalabras(i) = " " & arrayPalabras(i)
        Next

        sbusqueda = From Datos In MiDataTable
                    Let w = UCase(DBNullToStr("NIF"))
                    Where Datos.Field(Of String)("NIF").ToUpper Like "" & strToSQLLIKE(prefixText, True) & "*" Or Datos.Field(Of String)("DEN").ToUpper Like "*" & strToSQLLIKE(prefixText, True) & "*"
                    Select Datos Distinct.Take(count).ToArray()

        Dim resul() As String
        Dim l As Integer = 0
        ReDim resul(UBound(sbusqueda))
        For Each empresa In sbusqueda
            resul(l) = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(empresa(1) & "-" & empresa(2), empresa(0))
            l = l + 1
        Next
        Return resul
    End Function

    ''' <summary>Funcion que devuelve los valores de autocompletado de artículos</summary>
    ''' <param name="prefixText">texto ya introducido en la caja de texto</param>
    ''' <param name="count">nº de palabras que se devuelven</param>
    ''' <param name="contextKey">otros parámetros</param>
    ''' <returns>devuelve un array con los artículos que cumplen los filtros</returns>
    ''' <remarks>Llamada desde:GeneralEntry.vb</remarks>
    <System.Web.Services.WebMethod(True)>
    Public Function DevolverArticulos(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        Dim oArts As FSNServer.Articulos = FSNServer.Get_Object(GetType(FSNServer.Articulos))

        Dim sCod As String
        Dim sDen As String
        Dim sMat As String
        Dim sOrgCompras As String
        Dim sCentro As String
        Dim UON1 As String
        Dim UON2 As String
        Dim UON3 As String
        Dim sProve As String
        Dim sProveSumiArt As String
        Dim iTipoSolicit As Integer

        'Obtener los filtros del contextKey
        'sMat + "&" + sCodOrgCompras + "&" + sCodCentro + "&" + sCodUnidadOrganizativa + "&" + sCodProve + "&"  + TipoSolicit + "&" + sCodProveSumiArt
        Dim arContextKey() As String = contextKey.Split("&")
        'Filtro por código o denominación
        If arContextKey(0) = "0" Then
            sCod = prefixText
        Else
            sDen = prefixText
        End If
        'Material
        If arContextKey(1) <> "" Then
            Dim iLongCod As Integer
            Dim material(3) As String
            sMat = arContextKey(1)

            Dim i As Integer = 0
            While sMat.Trim(sMat) <> ""
                Select Case i
                    Case 0
                        iLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                    Case 1
                        iLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                    Case 2
                        iLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                    Case 3
                        iLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN4
                End Select

                If sMat.Length > 0 Then
                    material(i) = sMat.Substring(0, iLongCod).Trim
                    sMat = sMat.Substring(iLongCod)
                End If

                i += 1
            End While

            oArts.GMN1 = material(0)
            oArts.GMN2 = material(1)
            oArts.GMN3 = material(2)
            oArts.GMN4 = material(3)
        End If
        'Org. compras-centro
        If FSNServer.TipoAcceso.gbUsar_OrgCompras Then
            If arContextKey(2) <> "" Then sOrgCompras = arContextKey(2)
            If arContextKey(3) <> "" Then sCentro = arContextKey(3)
        End If
        'UON
        If arContextKey(4) <> "" Then
            Dim arUON() As String = Split(arContextKey(4), " - ")
            Select Case arUON.GetUpperBound(0)
                Case 1
                    UON1 = arUON(0)
                Case 2
                    UON1 = arUON(0)
                    UON2 = arUON(1)
                Case 3
                    UON1 = arUON(0)
                    UON2 = arUON(1)
                    UON3 = arUON(2)
            End Select
        End If
        'Proveedor
        If arContextKey(5) <> "" Then sProve = arContextKey(5)
        'Tipo de solicitud
        If arContextKey(6) <> "" Then iTipoSolicit = CType(arContextKey(6), Integer)
        'Proveedor suministrador del artículo
        If arContextKey(7) <> "" Then sProveSumiArt = arContextKey(7)

        If iTipoSolicit = TiposDeDatos.TipoDeSolicitud.Certificado Then
            oArts.LoadData(FSNUser.Cod, False, False, , , , , , , FSNUser.Idioma, , , , sOrgCompras, sCentro, UsarOrgCompras:=FSNServer.TipoAcceso.gbUsar_OrgCompras, TopAutoComplete:=count, sCodContiene:=sCod, sDenContiene:=sDen,
                           CodProveedorSumiArt:=sProveSumiArt, AccesoFSEP:=FSNServer.TipoAcceso.gbAccesoFSEP)
        ElseIf iTipoSolicit = TiposDeDatos.TipoDeSolicitud.NoConformidad Then
            oArts.LoadData(FSNUser.Cod, False, False, , , , , , , FSNUser.Idioma, , , , sOrgCompras, sCentro, UsarOrgCompras:=FSNServer.TipoAcceso.gbUsar_OrgCompras, TopAutoComplete:=count, sCodContiene:=sCod, sDenContiene:=sDen,
                           CodProveedorSumiArt:=sProveSumiArt, AccesoFSEP:=FSNServer.TipoAcceso.gbAccesoFSEP)
        ElseIf iTipoSolicit <> TiposDeDatos.TipoDeSolicitud.PedidoExpress AndAlso iTipoSolicit <> TiposDeDatos.TipoDeSolicitud.PedidoNegociado Then
            oArts.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil, , , , , , , FSNUser.Idioma, , , , sOrgCompras, sCentro, CodProveedor:=sProve,
                           UsarOrgCompras:=FSNServer.TipoAcceso.gbUsar_OrgCompras, RestringirMaterialUsuario:=AccionesDeSeguridad.PMRestringirMaterialAsignadoUsuario, UON1:=UON1, UON2:=UON2, UON3:=UON3, TopAutoComplete:=count,
                           sCodContiene:=sCod, sDenContiene:=sDen, CodProveedorSumiArt:=sProveSumiArt, AccesoFSEP:=FSNServer.TipoAcceso.gbAccesoFSEP)
        Else
            Dim iFiltrarNegociados As Integer
            Dim bCargarUltAdj As Boolean
            If iTipoSolicit = TiposDeDatos.TipoDeSolicitud.PedidoNegociado Then
                bCargarUltAdj = True
                iFiltrarNegociados = 1 'FILTRA POR ARTÍCULOS NEGOCIADOS                

                oArts.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil, , , , , , , FSNUser.Idioma, , , bCargarUltAdj, sOrgCompras, sCentro, CodProveedor:=sProve,
                       Negociados:=iFiltrarNegociados, RestringirPedidosArticulosUONUsuario:=FSNUser.RestringirPedidosArticulosUONUsuario, RestringirPedidosArticulosUONPerfil:=FSNUser.RestringirPedidosArticulosUONPerfil,
                       UsarOrgCompras:=FSNServer.TipoAcceso.gbUsar_OrgCompras, AccesoFSSM:=FSNServer.TipoAcceso.gbAccesoFSSM, RestringirMaterialUsuario:=AccionesDeSeguridad.PMRestringirMaterialAsignadoUsuario,
                       UON1:=UON1, UON2:=UON2, UON3:=UON3, TopAutoComplete:=count, sCodContiene:=sCod, sDenContiene:=sDen, CodProveedorSumiArt:=sProveSumiArt, AccesoFSEP:=FSNServer.TipoAcceso.gbAccesoFSEP)
            ElseIf iTipoSolicit = TiposDeDatos.TipoDeSolicitud.PedidoExpress Then
                bCargarUltAdj = False
                If FSNServer.TipoAcceso.gbArticulosNegociadosPedExpress Then
                    bCargarUltAdj = True
                    iFiltrarNegociados = 3 'FILTRA POR ARTÍCULOS NEGOCIADOS O NO
                Else
                    iFiltrarNegociados = 2 'FILTRA ARTICULOS NO NEGOCIADOS
                End If

                oArts.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil, , , , , , , FSNUser.Idioma, , , bCargarUltAdj, sOrgCompras, sCentro, CodProveedor:=sProve,
                                Negociados:=iFiltrarNegociados, UsarOrgCompras:=FSNServer.TipoAcceso.gbUsar_OrgCompras, RestringirMaterialUsuario:=AccionesDeSeguridad.PMRestringirMaterialAsignadoUsuario, TopAutoComplete:=count, sCodContiene:=sCod,
                                sDenContiene:=sDen, CodProveedorSumiArt:=sProveSumiArt, AccesoFSEP:=FSNServer.TipoAcceso.gbAccesoFSEP)
            End If
        End If


        Dim Arts As New List(Of String)
        If Not oArts.Data Is Nothing AndAlso oArts.Data.Tables.Count > 0 AndAlso oArts.Data.Tables(0).Rows.Count > 0 Then
            For Each oRow As DataRow In oArts.Data.Tables(0).Rows
                Arts.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(oRow("COD"), oRow("DEN")))
            Next
        End If

        Return Arts.ToArray()
    End Function

    ''' <summary>Funcion que devuelve los valores de autocompletado de los atributos de lista externa.</summary>
    ''' <param name="Empresa">Empresa</param>
    ''' <param name="IdAtributoListaExterna">Is atributo</param>
    ''' <param name="atributos">atributos</param>
    ''' <param name="codigoBuscado">código buscado</param>
    ''' <param name="denominacionBuscado">den. buscada</param>
    ''' <param name="buscador">buscador</param>
    ''' <returns>devuelve una lista con los valores posibles</returns>
    ''' <remarks>Llamada desde: GeneralEntry.vb</remarks>
    <System.Web.Services.WebMethod(True)>
    Public Function Obtener_Datos_ListaExterna_Extender(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        'Obtener los filtros del contextKey
        'Org. compras + "&" + UON + "&" + IdAtrib + "&" + todos los atribs (id @########@ valor) de la linea en curso y todos los atribs (id @########@ valor) no de desglose separados por ########## + "&" + CodProveedor
        'Org. compras + "&" + UON + "&" + IdAtrib + "&" + todos los atribs (id @########@ valor) no de desglose separados por ########## + "&" + CodProveedor
        Dim arContextKey() As String = contextKey.Split("&")

        Dim listaExterna As List(Of itemListaExterna) = Obtener_Datos_ListaExterna(CType(arContextKey(2), Integer), prefixText, Nothing, arContextKey(0), arContextKey(1), arContextKey(3), arContextKey(4))
        Dim oAtribs As New List(Of String)
        For Each oItem As itemListaExterna In listaExterna
            oAtribs.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(oItem.id, oItem.name))
        Next

        Return oAtribs.ToArray
    End Function

    ''' <summary>Funcion que devuelve los valores de autocompletado de los atributos de lista externa.</summary>
    ''' <param name="IdAtributoListaExterna">Is atributo</param>
    ''' <param name="codigoBuscado">código buscado</param>
    ''' <param name="denominacionBuscado">den. buscada</param>
    ''' <param name="sCodOrgCompras">La organización de compras o la UON</param>
    ''' <param name="atributos">atributos</param>
    ''' <param name="sCodProveedor">Código del proveedor</param>
    ''' <returns>devuelve una lista con los valores posibles</returns>
    ''' <remarks>Llamada desde: GeneralEntry.vb</remarks>
    <System.Web.Services.WebMethod(True)>
    Public Function Obtener_Datos_ListaExterna(ByVal IdAtributoListaExterna As Integer, ByVal codigoBuscado As String, ByVal denominacionBuscado As String, ByVal sCodOrgCompras As String, ByVal sUON As String, ByVal atributos As Object,
                                               ByVal sCodProveedor As String) As IList
        '************** EXISTE UNA FUNCION SIMILAR EN FSNWEB, EN EmisionPedido.aspx *****************
        Try
            Dim serializer As New JavaScriptSerializer()
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oEP_ValidacionesIntegracion As EP_ValidacionesIntegracion = FSNServer.Get_Object(GetType(EP_ValidacionesIntegracion))

            Dim sEmisor As String

            'Se crea la lista con el diccionario de atributos
            Dim lAtributosAux(,) As String
            If Not (atributos = "") Then
                Dim lAtributos As String() = Split(atributos, "##########")

                ReDim lAtributosAux(UBound(lAtributos), 1)

                Dim lValor As String()
                For i As Integer = 0 To UBound(lAtributos)
                    lValor = Split(lAtributos(i), "@########@")

                    lAtributosAux(i, 0) = lValor(0)
                    lAtributosAux(i, 1) = lValor(1)
                Next
            End If
            'FIN Se crea la lista con el diccionario de atributos

            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            sEmisor = FSNUser.CodPersona

            Dim lResultados(,) As String
            lResultados = oEP_ValidacionesIntegracion.EvalListaExterna(0, IdAtributoListaExterna, lAtributosAux, sEmisor, codigoBuscado, denominacionBuscado, sCodOrgCompras, sUON, , sCodProveedor)

            Dim listaExterna As New List(Of itemListaExterna)
            Dim iListaExterna As itemListaExterna

            If Not lResultados Is Nothing Then
                For i As Integer = 0 To UBound(lResultados, 1)
                    iListaExterna = New itemListaExterna

                    iListaExterna.id = lResultados(i, 0)
                    iListaExterna.name = lResultados(i, 1)

                    listaExterna.Add(iListaExterna)
                Next
            End If

            Return listaExterna
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>Funcion que devuelve los valores de autocompletado de artículos</summary>
    ''' <param name="prefixText">texto ya introducido en la caja de texto</param>
    ''' <param name="count">nº de palabras que se devuelven</param>
    ''' <param name="contextKey">otros parámetros</param>
    ''' <returns>devuelve un array con los artículos que cumplen los filtros</returns>
    ''' <remarks>Llamada desde:GeneralEntry.vb</remarks>
    <System.Web.Services.WebMethod(True)>
    Public Function DevolverEmpresas(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        Dim oEmpresas As FSNServer.Empresas = FSNServer.Get_Object(GetType(FSNServer.Empresas))

        Dim dtEmpresas As DataTable = oEmpresas.ObtenerEmpresas(FSNUser.Idioma.ToString, UsuCod:=FSNUser.Cod, AutoCompletar:=prefixText,
                                                                RestriccionEmpresaUsuario:=FSNUser.PMRestringirEmpresasUSU,
                                                                RestriccionEmpresaPerfilUsuario:=FSNUser.PMRestringirEmpresasPerfilUSU)
        Dim lEmpresas As New List(Of String)
        If Not dtEmpresas Is Nothing AndAlso dtEmpresas.Rows.Count > 0 Then
            For Each oRow As DataRow In dtEmpresas.Rows
                lEmpresas.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(oRow("NIF") & " - " & oRow("DEN"), oRow("ID")))
            Next
        End If

        Return lEmpresas.ToArray()
    End Function

#Region "Clases privadas"
    Private Class itemListaExterna
        Private _id As String
        Public Property id() As String
            Get
                Return _id
            End Get
            Set(ByVal value As String)
                _id = value
            End Set
        End Property
        Private _name As String
        Public Property name() As String
            Get
                Return _name
            End Get
            Set(ByVal value As String)
                _name = value
            End Set
        End Property
    End Class
#End Region

End Class