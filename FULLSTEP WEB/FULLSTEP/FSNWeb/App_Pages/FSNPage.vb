﻿Imports System.Data
Imports Fullstep.FSNLibrary
Imports Fullstep.FSNServer

Public Class FSNPage
    Inherits System.Web.UI.Page
    Implements IFSNPage

    Private _FSNDict As FSNServer.Dictionary
    Private _FSNUser As FSNServer.User
    Private _FSNAcceso As FSNLibrary.TiposDeDatos.ParametrosGenerales
    Private _FSNServer As FSNServer.Root
    Private _Idioma As String
    Private _ModuloIdioma As FSNLibrary.TiposDeDatos.ModulosIdiomas
    Private _FSNServerLogin As FSNServer.Root

    Public ReadOnly Property FSNUser() As FSNServer.User
        Get
            If _FSNUser Is Nothing Then
                Try
                    _FSNUser = Session("FSN_User")
                Catch
                    ''evitar cuelgue cuando aun no tenemos sesion, por ejemplo en una pagina publica
                End Try
            End If

            Return _FSNUser
        End Get
    End Property
    Public ReadOnly Property Usuario() As FSNServer.User Implements IFSNPage.Usuario
        Get
            Return CType(Me.FSNUser, FSNServer.User)
        End Get
    End Property
    Public ReadOnly Property Acceso() As FSNLibrary.TiposDeDatos.ParametrosGenerales Implements IFSNPage.Acceso
        Get
            Return Me.FSNServer.TipoAcceso
        End Get
    End Property
    Public ReadOnly Property FSNServer() As FSNServer.Root Implements IFSNPage.FSNServer
        Get
            If _FSNServer Is Nothing Then
                _FSNServer = Session("FSN_Server")
            End If
            Return _FSNServer
        End Get
    End Property
    Public ReadOnly Property FSNServerLogin() As FSNServer.Root
        Get
            If _FSNServerLogin Is Nothing Then
                _FSNServerLogin = Session("FSN_ServerLoginAccesoExterno")
                If _FSNServerLogin Is Nothing Then
                    _FSNServerLogin = New FSNServer.Root
                    Session("FSN_ServerLoginAccesoExterno") = _FSNServerLogin
                End If
            End If
            Return _FSNServerLogin
        End Get
    End Property
    Public ReadOnly Property Idioma() As String Implements IFSNPage.Idioma
        Get
            If _Idioma Is Nothing Then
                If Not FSNUser Is Nothing Then
                    _Idioma = FSNUser.Idioma
                End If
                If _Idioma Is Nothing Then
                    Try
                        If Not Request.QueryString("Idioma") Is Nothing Then
                            _Idioma = Request.QueryString("Idioma")
                        End If
                    Catch
                        'solucionar error "Request is not available in this context"
                    End Try
                End If
                If _Idioma Is Nothing Then
                    _Idioma = System.Configuration.ConfigurationManager.AppSettings("idioma")
                End If
            End If
            Return _Idioma
        End Get
    End Property
    Public Property ModuloIdioma() As FSNLibrary.TiposDeDatos.ModulosIdiomas Implements IFSNPage.ModuloIdioma
        Get
            Return _ModuloIdioma
        End Get
        Set(ByVal Value As FSNLibrary.TiposDeDatos.ModulosIdiomas)
            _ModuloIdioma = Value
        End Set
    End Property
    Public ReadOnly Property Textos(ByVal iTexto As Integer) As String Implements IFSNPage.Textos
        Get
            If HttpContext.Current.Cache("Textos_" & Me.Idioma & "_" & _ModuloIdioma) Is Nothing Then
                If _FSNDict Is Nothing Then
                    If Me.FSNServer Is Nothing Then
                        _FSNDict = New FSNServer.Dictionary()
                    Else
                        _FSNDict = Me.FSNServer.Get_Object(GetType(FSNServer.Dictionary))
                    End If
                End If
                _FSNDict.LoadData(_ModuloIdioma, Me.Idioma)
                Me.InsertarEnCache("Textos_" & Me.Idioma & "_" & _ModuloIdioma, _FSNDict.Data)
            End If
            Return HttpContext.Current.Cache("Textos_" & Me.Idioma & "_" & _ModuloIdioma).Tables(0).Rows(iTexto).Item(1)
        End Get
    End Property
    ''' <summary>
    ''' Devuelve el texto correspondiente al mÃ³dulo actual , con el id del registro
    ''' A diferencia de Textos que utiliza la posiciÃ³n relativa de 0 a nÂº registros - 1
    ''' </summary>
    ''' <param name="iTexto"></param>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property TextosById(ByVal iTexto As Integer) As String
        Get
            Return Me.TextosModuloCompleto.Select("id=" & iTexto)(0)(1).ToString
        End Get
    End Property
    Public ReadOnly Property TextosModuloCompleto As DataTable
        Get
            If HttpContext.Current.Cache("Textos_" & Me.Idioma & "_" & _ModuloIdioma) Is Nothing Then
                Dim FSNDict As FSNServer.Dictionary
                If Me.FSNServer Is Nothing Then
                    FSNDict = New FSNServer.Dictionary()
                    FSNDict.LoadData(_ModuloIdioma, Me.Idioma)
                Else
                    FSNDict = Me.FSNServer.Get_Object(GetType(FSNServer.Dictionary))
                    FSNDict.LoadData(_ModuloIdioma, Me.Idioma)
                End If
                Me.InsertarEnCache("Textos_" & Me.Idioma & "_" & _ModuloIdioma, FSNDict.Data)
            End If
            Return HttpContext.Current.Cache("Textos_" & Me.Idioma & "_" & _ModuloIdioma).Tables(0)
        End Get
    End Property
    Public WriteOnly Property Seccion() As String
        Set(ByVal value As String)
            Session("Seccion") = value
        End Set
    End Property
    Public ReadOnly Property ScriptMgr() As System.Web.UI.ScriptManager Implements IFSNPage.ScriptMgr
        Get
            Return ScriptManager.GetCurrent(Me.Page)
        End Get
    End Property
    ''' <summary>
    ''' Realiza el login si el usuario no está autenticado
    ''' </summary>
    ''' <param name="sSessionID">ID de la sesión</param>
    Public Sub Login(ByVal sSessionID As String)
        Dim webService As New FSNSessionService.SessionService
        Dim Username As String = webService.GetSessionData(sSessionID, "Usuario")
        'Si finaliza la Session del SessionService
        If Username Is Nothing Then
            Response.Redirect(ConfigurationManager.AppSettings("ruta") & "default.aspx")
            Response.End()
        End If
        'Si No hay Session o la Session que hay es de otro usuario distinto al del SessionService
        If FSNUser Is Nothing OrElse Username <> FSNUser.Cod Then
            Session.Clear()
        End If
        Session("sSession") = sSessionID
        If FSNUser Is Nothing OrElse Username <> FSNUser.Cod Then
            _FSNServer = New FSNServer.Root
            Dim Password As String = String.Empty
            Dim idDBLogin As String
            Dim idDBSecundaria As String

            'Dim oUser As FSNServer.User
            Select Case _FSNServer.TipoDeAutenticacion
                Case TiposDeAutenticacion.Windows, TiposDeAutenticacion.LDAP
                    _FSNUser = _FSNServer.Login(Username)
                Case Else
                    Password = Server.UrlDecode(webService.GetSessionData(sSessionID, "Pwd"))
                    idDBLogin = webService.GetSessionData(sSessionID, "idDBLogin")
                    idDBSecundaria = webService.GetSessionData(sSessionID, "idDBSecundaria")
                    'Establecemos las BD de login y BD secundaria de Server
                    If idDBLogin > 0 Then
                        _FSNServer.BDLogin = _FSNServer.obtenerBaseDatosSecundaria(idDBLogin)
                        If idDBLogin <> idDBSecundaria Then
                            _FSNServer.BDSecundaria = _FSNServer.obtenerBaseDatosSecundaria(idDBSecundaria)
                        End If
                    End If

                    If _FSNServer.TipoDeAccesoLCX = True Then
                        _FSNUser = _FSNServer.Login(Username, True)
                        _FSNUser.PerfilLCX = webService.GetSessionData(sSessionID, "Perfil")
                        _FSNUser.EmpresaLCX = strToInt(webService.GetSessionData(sSessionID, "Empresa"))
                        If (webService.GetSessionData(sSessionID, "CentroCoste")) Is Nothing Then
                            _FSNUser.CentroLCX = "" 'en caso de que sea nothing lo convertimos a string
                        Else
                            _FSNUser.CentroLCX = DBNullToStr(webService.GetSessionData(sSessionID, "CentroCoste"))
                        End If
                    Else
                        _FSNUser = _FSNServer.Login(Username, Password, False)
                    End If

            End Select

            If _FSNUser.ResultadoLogin <> TipoResultadoLogin.LoginOk Then
                Response.Redirect(ConfigurationManager.AppSettings("ruta") & "default.aspx")
                Response.End()
            Else
                _FSNUser.AccesoCN = FSNServer.TipoAcceso.gbAccesoFSCN
                _FSNUser.LoadUserData(Username)
                If _FSNUser.AccesoPM OrElse _FSNUser.AccesoQA OrElse _FSNUser.AccesoEP OrElse _FSNUser.AccesoSM OrElse _FSNUser.AccesoGS OrElse _FSNUser.AccesoFACT Then
                    If _FSNUser.AccesoQA Then
                        If Application("Nivel_UnQa") Is Nothing Then
                            _FSNServer.Load_UnQa()
                            Application("Nivel_UnQa") = FSNServer.NivelesUnQa
                        End If
                    End If
                    Dim sIdioma As String = _FSNUser.Idioma
                    If _FSNUser.Idioma = "" Then
                        _FSNUser.Idioma = ConfigurationManager.AppSettings("Idioma")
                    End If
                    _FSNServer.Load_LongitudesDeCodigos()
                    Session("FSN_User") = _FSNUser
                    Session("FSN_Server") = _FSNServer

                Else
                    Response.Redirect(ConfigurationManager.AppSettings("ruta") & "default.aspx")
                End If

                HttpContext.Current.User = Session("FSN_User")

                If _FSNUser.AccesoPM OrElse _FSNUser.AccesoFACT Then
                    Dim FSSMPargen As FSNServer.PargensSMs = FSNServer.Get_Object(GetType(FSNServer.PargensSMs))
                    FSSMPargen.CargarConfiguracionSM("", Idioma, True) 'no hace falta pasar el usuario. En PMWeb solo se usa la configuraciÃ³n de las partidas independiente del usuario.
                    Session("FSSMPargen") = FSSMPargen
                End If
            End If
        Else
            If Not (Session("CargarUserDataFSNWeb")) Is Nothing AndAlso Session("CargarUserDataFSNWeb") Then
                _FSNUser.LoadUserData(Username)
                Session("CargarUserDataFSNWeb") = False
            End If
        End If

    End Sub

	''' <summary>Realiza el login de un usuario sin pwd para acceso externo</summary>
	''' <param name="UserName">Cod. usuario</param>
	''' <param name="Fullstep40_SessionID">Id Sesión de la 4.0</param>
	''' <remarks>Llamada desde: Page_PreInit</remarks>
	Public Sub LoginServidorExterno(ByVal UserName As String, ByVal Fullstep40_SessionID As String,
									Optional ByVal sSessionID As String = Nothing, Optional soloInicializarUserRoot As Boolean = False)
		Dim oFullste40Seg As New Fullstep40_Servicios_Seguridad.SeguridadClient
		If oFullste40Seg.comprobarSesion(Fullstep40_SessionID) Then
			If FSNUser Is Nothing OrElse (Not String.IsNullOrEmpty(UserName.ToString) AndAlso UserName <> FSNUser.Cod) Then
				_FSNServer = New FSNServer.Root
				_FSNUser = _FSNServer.Login(UserName, True)

				If soloInicializarUserRoot Then
					Session("FSN_User") = _FSNUser
					Session("FSN_Server") = _FSNServer
				Else
					If _FSNUser.ResultadoLogin <> TipoResultadoLogin.LoginOk Then
						Response.End()
					Else
						If _FSNServer.TipoDeAccesoLCX = True AndAlso Not IsNothing(sSessionID) AndAlso sSessionID.Trim.Length > 0 Then
							Dim webService As New FSNSessionService.SessionService

							_FSNUser = _FSNServer.Login(UserName, True)
							_FSNUser.PerfilLCX = webService.GetSessionData(sSessionID, "Perfil")
							_FSNUser.EmpresaLCX = strToInt(webService.GetSessionData(sSessionID, "Empresa"))
							If (webService.GetSessionData(sSessionID, "CentroCoste")) Is Nothing Then
								_FSNUser.CentroLCX = "" 'en caso de que sea nothing lo convertimos a string
							Else
								_FSNUser.CentroLCX = DBNullToStr(webService.GetSessionData(sSessionID, "CentroCoste"))
							End If
						Else
							_FSNUser.AccesoCN = FSNServer.TipoAcceso.gbAccesoFSCN
							_FSNUser.LoadUserData(UserName)
							If _FSNUser.AccesoPM OrElse _FSNUser.AccesoQA OrElse _FSNUser.AccesoEP OrElse _FSNUser.AccesoSM OrElse _FSNUser.AccesoGS OrElse _FSNUser.AccesoFACT Then
								If _FSNUser.AccesoQA Then
									If Page IsNot Nothing AndAlso Application("Nivel_UnQa") Is Nothing Then
										_FSNServer.Load_UnQa()
										Application("Nivel_UnQa") = FSNServer.NivelesUnQa
									End If
								End If
								Dim sIdioma As String = _FSNUser.Idioma
								If _FSNUser.Idioma = "" Then
									_FSNUser.Idioma = ConfigurationManager.AppSettings("Idioma")
								End If
								_FSNServer.Load_LongitudesDeCodigos()
								Session("FSN_User") = _FSNUser
								Session("FSN_Server") = _FSNServer
							Else
								Response.End()
							End If
						End If
						HttpContext.Current.User = Session("FSN_User")

						If _FSNUser.AccesoPM OrElse _FSNUser.AccesoFACT Then
							Dim FSSMPargen As FSNServer.PargensSMs = FSNServer.Get_Object(GetType(FSNServer.PargensSMs))
							FSSMPargen.CargarConfiguracionSM("", Idioma, True) 'no hace falta pasar el usuario. En PMWeb solo se usa la configuraciÃ³n de las partidas independiente del usuario.
							Session("FSSMPargen") = FSSMPargen
						End If
					End If
				End If
			End If
		End If
	End Sub

	'''Revisado por:
	''' <summary>
	''' Comprueba si el usuario contiene el identity asignado mediante la Cookie de autenticacion.
	''' </summary>
	''' <remarks>Llamada desde el evento Page_PreInit. Maximo 1 msg.</remarks>
	Private Sub ComprobarTicketAutenticacion()

        If HttpContext.Current.User.Identity.Name = "" Then
            If Not IsNothing(Session("FSN_User")) AndAlso
                Not IsNothing(Session("FSN_Server")) AndAlso
                Session("FSN_Server").TipoDeAutenticacion <> TiposDeAutenticacion.Windows AndAlso
                Session("FSN_Server").TipoDeAutenticacion <> TiposDeAutenticacion.LDAP Then
                'A falta de más investigación esto es necesario para los WebPart y FSAL
                ' Crear cookie de autenticaciÃƒÂ³n de usuario
                Dim ticket As FormsAuthenticationTicket = New FormsAuthenticationTicket(1,
                    FSNUser.Cod,
                    DateTime.Now,
                    DateTime.Now.AddMinutes(Session.Timeout * 2),
                    False,
                    FSNUser.Cod,
                    FormsAuthentication.FormsCookiePath)
                ' Encrypt the ticket.
                Dim encTicket As String = FormsAuthentication.Encrypt(ticket)
                ' Create the cookie.
                Response.Cookies.Add(New HttpCookie(FormsAuthentication.FormsCookieName, encTicket))
                'Marcarla como segura si estas con https
                If InStr(ConfigurationManager.AppSettings("rutaFS"), "https:") > 0 Then
                    Dim cookie As HttpCookie
                    cookie = Request.Cookies(FormsAuthentication.FormsCookieName)
                    cookie.Secure = True
                    Response.SetCookie(cookie)
                End If
                'Asignamos al HttpContext.Current.User el Identity que la Cookie de autenticación ya asigna una vez que haya cambiado el HttpContext.
                'Se asigna aquí, ya que en la carga de páginas con WebParts a veces todavía no ha cambiado el HttpContext
                'y no se ha recuperado el Identity desde la Cookie de autenticación, lo que provoca un error de autenticación en los WebPart.
                HttpContext.Current.User = New System.Security.Principal.GenericPrincipal(New System.Security.Principal.GenericIdentity(Session("FSN_User").Cod), Nothing)
            End If
        End If

    End Sub
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = System.Configuration.ConfigurationManager.AppSettings.Item("TituloVentanas_" & Idioma)
        If FSNServerLogin.TipoDeAutenticacion = TiposDeAutenticacion.ServidorExterno AndAlso Master IsNot Nothing Then
            If TypeOf Master Is Fullstep.FSNWeb.Menu Then
                Master.Master.FindControl("Cabecera").Visible = False
                Master.Master.FindControl("Pestanias").Visible = False
            ElseIf TypeOf Master Is Fullstep.FSNWeb.CabCatalogo Then
                Master.Master.Master.FindControl("Cabecera").Visible = False
                Master.Master.Master.FindControl("Pestanias").Visible = False
            End If
        End If
    End Sub
    Protected Overrides Sub InitializeCulture()
        MyBase.InitializeCulture()
        If Not IsNothing(Me.FSNUser) Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo(Me.FSNUser.RefCultural)
            Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat = Me.FSNUser.DateFormat
            Threading.Thread.CurrentThread.CurrentCulture.NumberFormat = Me.FSNUser.NumberFormat

            Threading.Thread.CurrentThread.CurrentUICulture = Threading.Thread.CurrentThread.CurrentCulture
        End If
    End Sub
    ''' <summary>
    ''' Inicializa la sesión si recibe el parámetro SessionId. 
    ''' Si no hay sesión iniciada redirige a la página de Login.
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
    ''' <remarks>Se produce al principio de la inicialización de la página.</remarks>
    Private Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Not Request("desdeGS") Is Nothing And Not String.IsNullOrEmpty(Request("desdeGS")) Then Session("desdeGS") = "1"

        If Not TypeOf Master Is Fullstep.FSNWeb.Login And Not TypeOf Master Is Fullstep.FSNWeb.CabeceraLoginMail And Not TypeOf Me Is Fullstep.FSNWeb.PedidoAprobadoMail Then
            If (Not IsPostBack AndAlso Request("SessionId") IsNot Nothing AndAlso Session("desdeGS") = "1") Then
                Login(Request("SessionId"))
            ElseIf FSNServerLogin.TipoDeAutenticacion = TiposDeAutenticacion.ServidorExterno AndAlso Request("UserName") IsNot Nothing AndAlso Request("CodigoSesion") IsNot Nothing Then


                LoginServidorExterno(Request("UserName"), Request("CodigoSesion"), Request("SessionId"))
                'AKI
            ElseIf (Usuario Is Nothing) AndAlso (Not IsPostBack AndAlso Request("SessionId") IsNot Nothing AndAlso Session("desdeGS") Is Nothing) Then
                'Creo q solo entrara aqui en el caso del Gs. 
                'Has ido a Visor contratos, has seleccionado Detalle Contrato y te vas a una pagina popup como BuscadorProveedores. Se hace aqui pq en el detalle contrato los campos no son fijos, puedes tener N buscadores (usuario, Empresa, Centro, presup 1/2/3/4, material, ...)
                'Las paginas popup son otra session, otra unica session. Los N buscadores popup comparten HttpCurrentSession.
                Login(Request("SessionId"))
            End If
            ComprobarTicketAutenticacion()
            If Usuario Is Nothing OrElse Usuario.ResultadoLogin <> TipoResultadoLogin.LoginOk Then
                If FSNServerLogin.TipoDeAutenticacion = TiposDeAutenticacion.ServidorExterno Then
                    Response.End()
                Else
                    Response.Redirect(ConfigurationManager.AppSettings("ruta") & "default.aspx")
                End If
            End If
        End If
    End Sub
    ''' Revisado por: blp. Fecha: 29/01/2013
    ''' <summary>
    ''' Registra el script con las funciones para usar con paneles de búsqueda construidos con jQuery
    ''' </summary>
    ''' <param name="origen">Origen desde el que se solicita el listado de partidas y centros</param>
    ''' <remarks>Llamada desde el evento. Maximo 0,1 seg.</remarks>
    Public Function CargarBuscadoresJQuery(ByVal origen As TiposDeDatos.Aplicaciones) As HtmlGenericControl
        ''jquery se carga en Cabecera.Master, lo cual hace que en la primera carga, no este disponible 
        ''si usamos el scriptmanager o el me.page.clientScript para registrar cualquier llamada a sus utilidades
        ''Por eso usamos el ScriptMgr con un scriptReference
        Dim scriptReference As ScriptReference
        scriptReference = New System.Web.UI.ScriptReference("../js/BuscadoresJQuery.js")
        ScriptMgr.CompositeScript.Scripts.Add(scriptReference)
        Dim sScript As New StringBuilder
        'Agregamos el script de filtrado por jquery
        'MIRAR EL MODO DE AGREGARLO UNA SOLA VEZ
        agregarfastLiveFilter(sScript)

        Dim oScript As New HtmlGenericControl("script")
        oScript.InnerHtml = sScript.ToString
        Dim MenuMaster As MasterPage = Me.Master
        Dim CabeceraMaster As MasterPage = MenuMaster.Master
        If origen = TiposDeDatos.Aplicaciones.EP Then
            CabeceraMaster.Controls.Add(oScript)
            Return New HtmlGenericControl("EMPTY")
        Else
            'Actualmente, cuando el origen no es EP, solo se usa esta función desde el control de usuario de PM WucBusquedaAvanzadaContratos.
            'Al ser un control de usuario, no es posible modificar cabeceramaster porque se encuentra bloqueada la modificación enel momento que se crea el control en la página donde se usa (VisorContratos.aspx). 
            'La solución es agregar el script luego, desde visorContratos. Es lo que se hace en este Else:
            Return oScript
        End If
    End Function
    ''' Revisado por: blp. Fecha: 25/01/2013
    ''' <summary>
    ''' registramos los scripts que llenaran de contenido el panel de búsqueda
    ''' También pondrá a solo lectura el control de los filtros de búsqueda donde se escribirá el valor seleccionado en el panel de búsqueda
    ''' </summary>
    ''' <param name="tipoControl">Tipo de Control que cargamos</param>
    ''' <param name="FiltroClientID">Id del control de filtro de datos</param>
    ''' <param name="listadoClientID">Id del control con el listado de datos</param>
    ''' <param name="origen">Origen desde el que se solicita el listado de partidas y centros</param>
    ''' <remarks>Llamada desde el evento. Maximo 0,1 seg.</remarks>
    Public Function CargarScriptBusquedaJQuery(ByVal tipoControl As TiposDeDatos.ControlesBusquedaJQuery, ByVal listadoClientID As String, ByVal FiltroClientID As String, ByVal origen As TiposDeDatos.Aplicaciones) As HtmlGenericControl
        Dim sScript As New StringBuilder
        sScript.Append(vbCrLf)
        sScript.Append("$(window).ready(function(){")
        sScript.Append("    cargarDatos('" & tipoControl & "', '" & listadoClientID & "', '" & FiltroClientID & "', '" & If(origen = TiposDeDatos.Aplicaciones.PM, TiposDeDatos.AplicacionesEnNumero.PM, TiposDeDatos.AplicacionesEnNumero.EP) & "');")
        sScript.Append(vbCrLf)
        sScript.Append("});")
        sScript.Append(vbCrLf)

        Dim oScript As New HtmlGenericControl("script")
        oScript.InnerHtml = sScript.ToString
        Dim MenuMaster As MasterPage = Me.Master
        Dim CabeceraMaster As MasterPage = MenuMaster.Master
        If origen = TiposDeDatos.Aplicaciones.EP Then
            CabeceraMaster.Controls.Add(oScript)
            Return New HtmlGenericControl("EMPTY")
        Else
            'Actualmente, cuando el origen no es EP, solo se usa esta función desde el control de usuario de PM WucBusquedaAvanzadaContratos.
            'Al ser un control de usuario, no es posible modificar cabeceramaster porque se encuentra bloqueada la modificación enel momento que se crea el control en la página donde se usa (VisorContratos.aspx). 
            'La solución es agregar el script luego, desde visorContratos. Es lo que se hace en este Else:
            Return oScript
        End If
    End Function
    ''' Revisado por: blp. Fecha: 25/01/2013
    ''' <summary>
    ''' registramos los scripts que llenaran de contenido el panel de búsqueda
    ''' También pondrá a solo lectura el control de los filtros de búsqueda donde se escribirá el valor seleccionado en el panel de búsqueda
    ''' </summary>
    ''' <param name="tipoControl">Tipo de Control que cargamos</param>
    ''' <param name="FiltroClientID">Id del control de filtro de datos</param>
    ''' <param name="listadoClientID">Id del control con el listado de datos</param>
    ''' <param name="origen">Origen desde el que se solicita el listado de partidas y centros</param>
    ''' <remarks>Llamada desde el evento. Maximo 0,1 seg.</remarks>
    Public Function CargarScriptBusquedaJQueryEMP(ByVal tipoControl As TiposDeDatos.ControlesBusquedaJQuery, ByVal listadoClientID As String, ByVal FiltroClientID As String, ByVal origen As TiposDeDatos.Aplicaciones, ByVal VerUON As Boolean) As HtmlGenericControl
        Dim sScript As New StringBuilder
        sScript.Append(vbCrLf)
        sScript.Append("$(window).ready(function(){")
        sScript.Append("    cargarDatosEMP('" & tipoControl & "', '" & listadoClientID & "', '" & FiltroClientID & "', '" & If(origen = TiposDeDatos.Aplicaciones.PM, TiposDeDatos.AplicacionesEnNumero.PM, TiposDeDatos.AplicacionesEnNumero.EP) & "', '" & VerUON & "');")
        sScript.Append(vbCrLf)
        sScript.Append("});")
        sScript.Append(vbCrLf)

        Dim oScript As New HtmlGenericControl("script")
        oScript.InnerHtml = sScript.ToString
        Dim MenuMaster As MasterPage = Me.Master
        Dim CabeceraMaster As MasterPage = MenuMaster.Master
        If origen = TiposDeDatos.Aplicaciones.EP Then
            CabeceraMaster.Controls.Add(oScript)
            Return New HtmlGenericControl("EMPTY")
        Else
            'Actualmente, cuando el origen no es EP, solo se usa esta función desde el control de usuario de PM WucBusquedaAvanzadaContratos.
            'Al ser un control de usuario, no es posible modificar cabeceramaster porque se encuentra bloqueada la modificación enel momento que se crea el control en la página donde se usa (VisorContratos.aspx). 
            'La solución es agregar el script luego, desde visorContratos. Es lo que se hace en este Else:
            Return oScript
        End If
    End Function
    ''' Revisado por: blp. Fecha: 25/01/2013
    ''' <summary>
    ''' registramos los scripts que llenaran de contenido el panel de búsqueda
    ''' También pondrá a solo lectura el control de los filtros de búsqueda donde se escribirá el valor seleccionado en el panel de búsqueda
    ''' </summary>
    ''' <param name="tipoControl">Tipo de Control que cargamos</param>
    ''' <param name="FiltroClientID">Id del control de filtro de datos</param>
    ''' <param name="listadoClientID">Id del control con el listado de datos</param>
    ''' <param name="origen">Origen desde el que se solicita el listado de partidas y centros</param>
    ''' <remarks>Llamada desde el evento. Maximo 0,1 seg.</remarks>
    Public Function CargarScriptBusquedaJQueryEP(ByVal tipoControl As TiposDeDatos.ControlesBusquedaJQuery, ByVal listadoClientID As String, ByVal FiltroClientID As String, ByVal origen As TiposDeDatos.Aplicaciones, ByVal sIdCodCentro As String, ByVal CboBoxEmpresa As String) As HtmlGenericControl
        Dim sScript As New StringBuilder
        sScript.Append(vbCrLf)
        sScript.Append("$(window).ready(function(){")
        sScript.Append("    cargarDatosEP('" & tipoControl & "', '" & listadoClientID & "', '" & FiltroClientID & "', '" & If(origen = TiposDeDatos.Aplicaciones.PM, TiposDeDatos.AplicacionesEnNumero.PM, TiposDeDatos.AplicacionesEnNumero.EP) & "','" & sIdCodCentro & "','" & CboBoxEmpresa & "');")
        'sScript.Append("    cargarDatos('" & tipoControl & "', '" & listadoClientID & "', '" & FiltroClientID & "', '" & If(origen = TiposDeDatos.Aplicaciones.PM, TiposDeDatos.AplicacionesEnNumero.PM, TiposDeDatos.AplicacionesEnNumero.EP) & "');")
        sScript.Append(vbCrLf)
        sScript.Append("});")
        sScript.Append(vbCrLf)

        Dim oScript As New HtmlGenericControl("script")
        oScript.InnerHtml = sScript.ToString
        Dim MenuMaster As MasterPage = Me.Master
        Dim CabeceraMaster As MasterPage = MenuMaster.Master
        If origen = TiposDeDatos.Aplicaciones.EP Then
            CabeceraMaster.Controls.Add(oScript)
            Return New HtmlGenericControl("EMPTY")
        Else
            'Actualmente, cuando el origen no es EP, solo se usa esta función desde el control de usuario de PM WucBusquedaAvanzadaContratos.
            'Al ser un control de usuario, no es posible modificar cabeceramaster porque se encuentra bloqueada la modificación enel momento que se crea el control en la página donde se usa (VisorContratos.aspx). 
            'La solución es agregar el script luego, desde visorContratos. Es lo que se hace en este Else:
            Return oScript
        End If
    End Function
    '''Revisado por: blp. Fecha: 07/02/2013
    ''' <summary>
    ''' Anyadimos a un control StringBuilder el js de la funcionalidad JQuery fastLiveFilter
    ''' </summary>
    ''' <param name="sScript">StringBuilder al que anyadir el js</param>
    ''' <remarks>Llamada desde CargarScriptPartidasCentros. Maximo 0,1 seg.</remarks>
    Private Sub agregarfastLiveFilter(ByRef sScript As StringBuilder)
        sScript.Append("/**")
        sScript.Append(vbCrLf)
        sScript.Append("* fastLiveFilter jQuery plugin 1.0.3")
        sScript.Append(vbCrLf)
        sScript.Append("* ")
        sScript.Append(vbCrLf)
        sScript.Append("* Copyright (c) 2011, Anthony Bush")
        sScript.Append(vbCrLf)
        sScript.Append("* License: <http://www.opensource.org/licenses/bsd-license.php>")
        sScript.Append(vbCrLf)
        sScript.Append("* Project Website: http://anthonybush.com/projects/jquery_fast_live_filter/")
        sScript.Append(vbCrLf)
        sScript.Append("**/")
        sScript.Append(vbCrLf)
        sScript.Append("")
        sScript.Append(vbCrLf)
        sScript.Append("jQuery.fn.fastLiveFilter = function (list, options) {")
        sScript.Append(vbCrLf)
        sScript.Append("    // Options: input, list, timeout, callback")
        sScript.Append(vbCrLf)
        sScript.Append("    options = options || {};")
        sScript.Append(vbCrLf)
        sScript.Append("    list = jQuery(list);")
        sScript.Append(vbCrLf)
        sScript.Append("    var input = this;")
        sScript.Append(vbCrLf)
        sScript.Append("    var timeout = options.timeout || 0;")
        sScript.Append(vbCrLf)
        sScript.Append("    var callback = options.callback || function () { };")
        sScript.Append(vbCrLf)
        sScript.Append("")
        sScript.Append(vbCrLf)
        sScript.Append("    var keyTimeout;")
        sScript.Append(vbCrLf)
        sScript.Append("")
        sScript.Append(vbCrLf)
        sScript.Append("    // NOTE: because we cache lis & len here, users would need to re-init the plugin")
        sScript.Append(vbCrLf)
        sScript.Append("    // if they modify the list in the DOM later.  This doesn't give us that much speed")
        sScript.Append(vbCrLf)
        sScript.Append("    // boost, so perhaps it's not worth putting it here.")
        sScript.Append(vbCrLf)
        sScript.Append("    var lis = list.children();")
        sScript.Append(vbCrLf)
        sScript.Append("    var len = lis.length;")
        sScript.Append(vbCrLf)
        sScript.Append("    var oldDisplay = len > 0 ? lis[0].style.display : ""block"";")
        sScript.Append(vbCrLf)
        sScript.Append("    callback(len); // do a one-time callback on initialization to make sure everything's in sync")
        sScript.Append(vbCrLf)
        sScript.Append("")
        sScript.Append(vbCrLf)
        sScript.Append("    input.change(function () {")
        sScript.Append(vbCrLf)
        sScript.Append("        // var startTime = new Date().getTime();")
        sScript.Append(vbCrLf)
        sScript.Append("        var filter = input.val().toLowerCase();")
        sScript.Append(vbCrLf)
        sScript.Append("        var li;")
        sScript.Append(vbCrLf)
        sScript.Append("        var numShown = 0;")
        sScript.Append(vbCrLf)
        sScript.Append("        for (var i = 0; i < len; i++) {")
        sScript.Append(vbCrLf)
        sScript.Append("            li = lis[i];")
        sScript.Append(vbCrLf)
        sScript.Append("            if ((li.textContent || li.innerText || """").toLowerCase().indexOf(filter) >= 0) {")
        sScript.Append(vbCrLf)
        sScript.Append("                if (li.style.display == ""none"") {")
        sScript.Append(vbCrLf)
        sScript.Append("                    li.style.display = oldDisplay;")
        sScript.Append(vbCrLf)
        sScript.Append("                }")
        sScript.Append(vbCrLf)
        sScript.Append("                numShown++;")
        sScript.Append(vbCrLf)
        sScript.Append("            } else {")
        sScript.Append(vbCrLf)
        sScript.Append("                if (li.style.display != ""none"") {")
        sScript.Append(vbCrLf)
        sScript.Append("                    li.style.display = ""none"";")
        sScript.Append(vbCrLf)
        sScript.Append("                }")
        sScript.Append(vbCrLf)
        sScript.Append("            }")
        sScript.Append(vbCrLf)
        sScript.Append("        }")
        sScript.Append(vbCrLf)
        sScript.Append("        callback(numShown);")
        sScript.Append(vbCrLf)
        sScript.Append("        // var endTime = new Date().getTime();")
        sScript.Append(vbCrLf)
        sScript.Append("        // console.log('Search for ' + filter + ' took: ' + (endTime - startTime) + ' (' + numShown + ' results)');")
        sScript.Append(vbCrLf)
        sScript.Append("        return false;")
        sScript.Append(vbCrLf)
        sScript.Append("    }).keydown(function () {")
        sScript.Append(vbCrLf)
        sScript.Append("        // TODO: one point of improvement could be in here: currently the change event is")
        sScript.Append(vbCrLf)
        sScript.Append("        // invoked even if a change does not occur (e.g. by pressing a modifier key or")
        sScript.Append(vbCrLf)
        sScript.Append("        // something)")
        sScript.Append(vbCrLf)
        sScript.Append("        clearTimeout(keyTimeout);")
        sScript.Append(vbCrLf)
        sScript.Append("        keyTimeout = setTimeout(function () { input.change(); }, timeout);")
        sScript.Append(vbCrLf)
        sScript.Append("    });")
        sScript.Append(vbCrLf)
        sScript.Append("    return this; // maintain jQuery chainability")
        sScript.Append(vbCrLf)
        sScript.Append("}")
        sScript.Append(vbCrLf)
    End Sub
    ''' Revisado por: blp. Fecha: 26/08/2013
    ''' <summary>
    ''' Registra script. Es útil cuando en un control de usuario se quiere registrar un script y hacerlo directamente desde el control da un error.
    ''' </summary>
    ''' <param name="Script">Script a registrar</param>
    ''' <param name="ScriptKey">Nombre del script a registrar</param>
    ''' <remarks>Llamada desde wucBusquedaAvanzadoContratos. Max. 0,1 seg</remarks>
    Public Sub RegistrarScript(ByVal Script As HtmlGenericControl, ByVal ScriptKey As String)
        If Script IsNot Nothing AndAlso Script.TagName = "script" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), ScriptKey, Script.InnerHtml, True)
        End If
    End Sub
    '''Revisado por: blp. Fecha: 28/08/2013
    ''' <summary>
    ''' Función que crea una estructura de tabla de String Genérico (como el tipo tabla de usuario TBCADENATEXTO de sql server)
    ''' </summary>
    ''' <returns>DataTable vacío</returns>
    ''' <remarks>Llamada desde wucBusquedaAvanzadaContratos.ascx.vb. Máx. 0,1 seg.</remarks>
    Public Function CrearTBCADENATEXTO() As DataTable
        Dim dt As DataTable = New DataTable("TBCADENATEXTO")
        dt.Columns.Add("CADENA", GetType(System.String))
        Return dt
    End Function
    '''Revisado por: blp. Fecha: 28/08/2013
    ''' <summary>
    ''' Carga en una tabla las cadenas de texto pasados por parámetro
    ''' </summary>
    ''' <param name="sStrings">Cadena con las cadenas de texto</param>
    ''' <param name="Separador">String separador</param>
    ''' <returns>Una tabla con las cadenas de texto</returns>
    ''' <remarks>
    ''' Llamada desde: Seguimiento.aspx.vb
    ''' Tiempo máximo: 0,1 sec.
    ''' </remarks>
    Public Function CargarTablaStrings(ByVal sStrings As String, ByVal Separador As String) As DataTable
        Dim dtStrings As DataTable = CrearTBCADENATEXTO()
        Dim arrayStrings As String() = Split(sStrings, Separador)
        For Each sString As Object In arrayStrings
            dtStrings.Rows.Add({sString})
        Next
        Return (dtStrings)
    End Function

    ''' <summary>Inserta un objeto en la caché con un tiempo de expiración que se obtiene del web.config</summary>
    ''' <param name="key">clave</param>
    ''' <param name="value">objeto a insertar</param>
    ''' <param name="priority">prioridad</param>
    Public Sub InsertarEnCache(ByVal key As String, ByVal value As Object, Optional ByVal priority As CacheItemPriority = CacheItemPriority.Default)
        HttpContext.Current.Cache.Insert(key, value, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0), priority, Nothing)
    End Sub
    Public Sub AplicarEstilosTab(ByRef oTab As Infragistics.WebUI.UltraWebTab.UltraWebTab)
        oTab.ImageDirectory = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Theme & "/images/"
        oTab.BorderColor = System.Drawing.Color.FromName(ObtenerValorPropiedad("#uwtDefaultTab", "border-color"))
    End Sub
    Public Function ObtenerValorPropiedad(ByVal sBloque As String, ByVal sProp As String) As String
        Dim sPath As String
        sPath = HttpContext.Current.Server.MapPath("~/App_Themes/" & Theme)
        Dim oFile As New System.IO.StreamReader(sPath & "\" & Theme & ".css")
        Dim sStyle As String = oFile.ReadToEnd()

        oFile.Close()
        oFile = Nothing
        Dim lIndex As Long
        Dim sAux As String

        lIndex = sStyle.IndexOf(sBloque)

        sAux = sStyle.Substring(lIndex, sStyle.Length - lIndex)
        lIndex = sAux.IndexOf("}")

        sAux = sAux.Substring(0, lIndex)

        lIndex = sAux.IndexOf(sProp)
        sAux = sAux.Substring(lIndex, sAux.Length - lIndex)
        lIndex = sAux.IndexOf(";")
        sAux = sAux.Substring(sAux.IndexOf(":") + 1, lIndex - sAux.IndexOf(":") - 1)
        Return sAux
    End Function

End Class