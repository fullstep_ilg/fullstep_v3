<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master"
    Inherits="Fullstep.FSNWeb.Inicio" CodeBehind="Inicio.aspx.vb" Async="true" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE10"/>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH4" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <aspf:WebPartManager ID="WebPartManager1" runat="server">
            </aspf:WebPartManager>
            <fsn:FSNButton ID="btnCatalogo" runat="server" Text="DA�adir m�s informaci�n&gt;&gt;"
                Alineacion="Left"></fsn:FSNButton>
            <asp:Panel ID="pnlEditorZone" CssClass="EditorZone" runat="server" Style="position: relative;
                width: 680px;" BorderStyle="None" ScrollBars="Auto">
                <asp:EditorZone Width="97%" ID="EditorWebParts" runat="server" ErrorText="DSe ha producido un error al aplicar los cambios."
                    InstructionText="DModifique las propiedades del elemento y pulse Aceptar." PartChromeType="None">
                    <HeaderCloseVerb Description="DCerrar el editor." Text="DCerrar" />
                    <CancelVerb Description="DCancelar los cambios." Text="DCancelar" />
                    <OKVerb Description="DAplicar los cambios y cerrar el editor." Text="DAceptar" />
                </asp:EditorZone>
            </asp:Panel>
            <input id="btnOculto" type="button" value="button" runat="server" style="display: none" />
            <ajx:ModalPopupExtender ID="pnlEditorZone_ModalPopupExtender" runat="server" DynamicServicePath=""
                Enabled="True" PopupControlID="pnlEditorZone" TargetControlID="btnOculto" BackgroundCssClass="modalBackground"
                DropShadow="True" RepositionMode="RepositionOnWindowScroll" Drag="true" PopupDragHandleControlID="EditorWebParts">
            </ajx:ModalPopupExtender>
            <fsn:FSNCatalogZone ID="MiCatalogo" runat="server" HeaderText="" HeaderStyle-CssClass="Rotulo"
                Width="95%" CloseVerb-Visible="False" AddVerb-Visible="false" SelectTargetZoneText="">
                <CatalogTemplate>
                    <ajx:Accordion ID="AccorCatalogo" runat="server" DataSource='<%# Container.CatalogParts %>'
                        OnItemDataBound="AccorCatalogo_ItemDataBound">
                        <HeaderTemplate>
                            <asp:Label ID="lblcabecera" runat="server" CssClass="RotuloGrande" />
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:HiddenField ID="hddCatalogID" runat="server" Value='<%#Eval("ID") %>'></asp:HiddenField>
                            <asp:DataList ID="lstWebParts" runat="server" RepeatColumns="2" RepeatDirection="Vertical"
                                RepeatLayout="Table" Width="100%" OnItemDataBound="lstWebParts_ItemDataBound">
                                <ItemTemplate>
                                    <table width="90%" border="0">
                                        <tr>
                                            <td rowspan="2" width="25%" align="center">
                                                <asp:Image ID="Image1" runat="server" ImageUrl='<%# "~/images/" & Eval("ID") & ".gif" %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("Title") %>' CssClass="Rotulo"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblDescripcion" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hddID" runat="server" Value='<%#Eval("ID") %>'></asp:HiddenField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <fsn:FSNButton ID="Button1" runat="server" OnClick="fsnButtonAnadir_Click" CommandArgument='<%#Eval("ID") %>'></fsn:FSNButton>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </ContentTemplate>
                    </ajx:Accordion>
                </CatalogTemplate>
                <ZoneTemplate>
                    <asp:DeclarativeCatalogPart ID="DeclarativeCatalogPart1" runat="server" ChromeType="None">
                        <WebPartsTemplate>
                            <fsn:FSNInfoSoporte ID="InfoSoporte" runat="server" />
                            <fsn:FSNAyuda ID="FSNAyuda" runat="server" />
                            <fsn:FSEPWebPartPedidos ID="PedidosCurso" runat="server" AuthorizationFilter="EPAprovisionador" />
                            <fsn:FSEPWebPartCesta ID="CestaVirtual" runat="server" AuthorizationFilter="EPAprovisionador" />
                            <fsn:FSEPWebPartPedidosFav ID="PedidosFavoritos" runat="server" AuthorizationFilter="EPAprovisionador" />
                            <fsn:FSEPWebPartBusqArticulos ID="FSEPWebPartBusqArticulos" runat="server" AuthorizationFilter="EPAprovisionador" />
                            <fsn:FSPMWebPartAltaSolicitudes ID="FSPMWebPartAltaSolicitudes" runat="server" AuthorizationFilter="PM" />
                            <fsn:FSPMWebPartSolicitudesPendientes ID="FSPMWebPartSolicitudesPendientes" runat="server" AuthorizationFilter="PM" />
                            <fsn:FSPMWebPartBusquedaSolicitudes ID="FSPMWebPartBusquedaSolicitudes" runat="server" AuthorizationFilter="PM" />
                            <fsn:FSPMWebPartMonitorizacionSolicitudes ID="FSPMWebPartMonitorizacionSolicitudes" runat="server" AuthorizationFilter="PM" />
                            <fsn:FSPMWebPartFiltrosDisponibles ID="FSPMWebPartFiltrosDisponibles" runat ="server" AuthorizationFilter="PM" />
                            <fsn:FSQAWebPartNoConfAbiertas ID="FSQAWebPartNoConfAbiertas" runat="server" CatalogIconImageUrl="~/images/thumbFSEPWebPartBusqArt.gif" AuthorizationFilter="QANoConformidades" />
                            <fsn:FSQAWebPartAltaNoConformidades ID="FSQAWebPartAltaNoConformidades" runat="server" CatalogIconImageUrl="~/images/thumbFSEPWebPartBusqArt.gif" AuthorizationFilter="QANoConformidades" />
                            <fsn:FSQAWebPartNoConformidadesPend ID="FSQAWebPartNoConformidadesPend" runat="server" CatalogIconImageUrl="~/images/thumbFSEPWebPartBusqArt.gif" AuthorizationFilter="QANoConformidades" />
                            <fsn:FSQAWebPartCertificados ID="FSQAWebPartCertificados" runat="server" CatalogIconImageUrl="~/images/thumbFSEPWebPartBusqArt.gif" AuthorizationFilter="QACertificados" />
                            <fsn:FSQAWebPartPanelCalidad ID="FSQAWebPartPanelCalidad" runat="server" CatalogIconImageUrl="~/images/thumbFSEPWebPartBusqArt.gif" AuthorizationFilter="QAPuntuacion" />
                            <fsn:FSPMWebPartOtrasURLs ID="FSPMWebPartOtrasURLs" CatalogIconImageUrl="~/images/FSPMWebPartOtrasURLs.gif" runat="server"></fsn:FSPMWebPartOtrasURLs>
                            <fsn:FSPMWebPartFacturas ID="FSPMWebPartFacturas" CatalogIconImageUrl="~/images/Img_Ico_Catalogo.gif" runat="server" AuthorizationFilter="IMAutofacturas"></fsn:FSPMWebPartFacturas>
                            <fsn:FSPMWebPartAltaSolicitudes ID="FSPMWebPartAltaFacturas" runat="server" TipoSolicitud="13" AuthorizationFilter="IMFacturas" />
                            <fsn:FSPMWebPartBusquedaSolicitudes ID="FSPMWebPartBusquedaFacturas" runat="server" TipoSolicitud="13" AuthorizationFilter="IMFacturas" />
                            <fsn:FSPMWebPartSolicitudesPendientes ID="FSPMWebPartFacturasPendientes" runat="server" TipoSolicitud="13" AuthorizationFilter="IMFacturas" />
                        </WebPartsTemplate>
                    </asp:DeclarativeCatalogPart>
                </ZoneTemplate>
            </fsn:FSNCatalogZone>
            <br />
            <table cellpadding="0" cellspacing="4" width="100%">
                <tr>
                    <td valign="top" width="33%" style="min-width: 200px;">
                        <aspf:WebPartZone ID="ZoneIzquierda" runat="server">
                            <RestoreVerb Description="DRestaurar '{0}'" />
                            <DeleteVerb Description="DQuitar '{0}'" />
                            <MinimizeVerb Description="DMinimizar '{0}'" />
                        </aspf:WebPartZone>
                    </td>
                    <td valign="top" width="33%" style="min-width: 200px">
                        <aspf:WebPartZone ID="ZoneCentro" runat="server">
                            <RestoreVerb Description="DRestaurar '{0}'" />
                            <DeleteVerb Description="DQuitar '{0}'" />
                            <MinimizeVerb Description="DMinimizar '{0}'" />
                        </aspf:WebPartZone>
                    </td>
                    <td valign="top" width="33%" style="min-width: 200px">
                        <aspf:WebPartZone ID="ZoneDerecha" runat="server">
                            <RestoreVerb Description="DRestaurar '{0}'" />
                            <DeleteVerb Description="DQuitar '{0}'" />
                            <MinimizeVerb Description="DMinimizar '{0}'" />
                        </aspf:WebPartZone>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript" language="javascript">
        var ModalProgress = '<%= ModalProgress.ClientID %>';
    </script>
    
    <ajx:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress" BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
    
    <fsn:FSNPanelInfo ID="FSNPanelDatosUsuario" runat="server" ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_DatosPersona" TipoDetalle="3" />
    <fsn:FSNPanelInfo ID="FSNPanelDatosPersona" runat="server" ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_DatosPersona" TipoDetalle="2" />
    <fsn:FSNPanelInfo ID="FSNPanelDatosPersonaQA" runat="server" ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_DatosPersona" TipoDetalle="2" />
    <fsn:FSNPanelInfo ID="FSNPanelDatosProveedor" runat="server" ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_DatosProveedor" TipoDetalle="1" />
    <fsn:FSNPanelInfo ID="FSNPanelDatosProveedorQA" runat="server" ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_DatosProveedorQA" TipoDetalle="1" />
    <fsn:FSNPanelInfo ID="FSNPanelEnProceso" runat="server" ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_TextoEnProceso" TipoDetalle="4" 
        ImagenPopUp="~/App_Themes/<%=Me.Page.Theme%>/images/Icono_Error_Amarillo_40x40.gif" Height="120px" Width="370px" />
</asp:Content>
