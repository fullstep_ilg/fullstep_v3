﻿<%@ Page  Debug="true" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master" CodeBehind="QlikApps.aspx.vb" Inherits="Fullstep.FSNWeb.QlikApps"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=9"/>
    <link type="text/css" href="css/ig.ui.chart.igtheme.transparent.css" rel="stylesheet" />
    <link type="text/css" href="css/jquery-ui.css" rel="stylesheet" />

    <link href="css/infragistics.theme.css" rel="stylesheet" />
	<link href="css/infragistics.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">

    <script type="text/javascript">
        var SelectedRows = [];
        var SelectedRowCount = 0;

        $(document).keypress(function (e) {
            if (e.which == 13) {
                if ($('#pnlQlikApps').is(':visible')) {
                    var focused = document.activeElement;
                    if (!focused || focused == document.body)
                        focused = null;
                    else if (document.querySelector)
                        focused = document.querySelector(":focus");
                    if (focused.id == $('[id$=txtUrl]').attr('id')) {
                        $('[id$=txtPuertoRestAPI]').focus();
                    }
                }
                return false;
            }
        });

        $(document).ready(function () {

            $('[id$=ImgBtnFirst]').live('click', function () { FirstPage(); });
            $('[id$=ImgBtnPrev]').live('click', function () { PrevPage(); });
            $('[id$=ImgBtnNext]').live('click', function () { NextPage(); });
            $('[id$=ImgBtnLast]').live('click', function () { LastPage(); });

            $('[id$=txtUrl]').on('focusin', function () { $('[id$=txtUrl]').on('focusout', function () { MostrarModal('ctl00_ctl00_ModalProgress'); setTimeout(ComprobarServidorQlikCorrecto, 500); }); });
            $('[id$=lstIdApp]').on('change', function () { MostrarModal('ctl00_ctl00_ModalProgress'); setTimeout(OnSeleccionarAplicacion, 500); });

            $('[id$=imgCerrar]').live('click', function () {
                CancelarQlikApp();
                return false;
            });

            $('[id$=imgCerrarAccesoQlik]').live('click', function () {
                CancelarAccesoQlik();
                return false;
            });

        });

        function IndexChanged(){
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var btnPager = $('[id$=btnPager]').attr('id');
            var selectedIndex = dropdownlist.selectedIndex;
            __doPostBack(btnPager,dropdownlist.selectedIndex);
        }

        function FirstPage(){
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var btnPager = $('[id$=btnPager]').attr('id');
            dropdownlist.options[0].selected = true;
            __doPostBack(btnPager,0);
        }

        function PrevPage(){
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var btnPager = $('[id$=btnPager]').attr('id');
            var selectedIndex = dropdownlist.selectedIndex;
            if(selectedIndex-1>=0){
                dropdownlist.options[selectedIndex-1].selected = true;
                __doPostBack(btnPager,dropdownlist.selectedIndex);
            }
        }

        function NextPage(){
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var btnPager = $('[id$=btnPager]').attr('id');
            var selectedIndex = dropdownlist.selectedIndex;
            if(selectedIndex+1<=dropdownlist.length-1){
                dropdownlist.options[selectedIndex+1].selected = true;
                __doPostBack(btnPager,dropdownlist.selectedIndex);
            }                 
        }

        function LastPage() {
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var btnPager = $('[id$=btnPager]').attr('id');
            dropdownlist.options[dropdownlist.length - 1].selected = true;
            __doPostBack(btnPager, dropdownlist.length - 1);
        }

        function whgQlikApps_Sorting(sender, e) {
            var sortSentence = '';
            switch (e.get_column().get_key()) {
                default:
                    sortSentence = e.get_column().get_key() + " " + (e.get_sortDirection() == 1 ? "ASC" : "DESC");
                    break;
            }
            $('[id$=hOrderedColumn]').val(sortSentence);
        }

        function whgQlikApps_Filtering(sender, e) {
            var gridFiltered = false;
            var filteredColumns, filteredCondition, filteredValue;
            $('[id$=hFilteredColumns]').val('');
            $('[id$=hFilteredCondition]').val('');
            $('[id$=hFilteredValue]').val('');
            for (i = 0; i < e.get_columnFilters().length; i++) {
                if (e.get_columnFilters()[i].get_condition().get_rule() != 0) {
                    gridFiltered = true;
                    filteredColumns = $('[id$=hFilteredColumns]').val();
                    filteredCondition = $('[id$=hFilteredCondition]').val();
                    filteredValue = $('[id$=hFilteredValue]').val();
                    $('[id$=hFilteredColumns]').val((filteredColumns == '' ? e.get_columnFilters()[i].get_columnKey() : filteredColumns + "#" + e.get_columnFilters()[i].get_columnKey()));
                    $('[id$=hFilteredCondition]').val((filteredCondition == '' ? e.get_columnFilters()[i].get_condition().get_rule() : filteredCondition + "#" + e.get_columnFilters()[i].get_condition().get_rule()));
                    $('[id$=hFilteredValue]').val((filteredValue == '' ? e.get_columnFilters()[i].get_condition().get_value() : filteredValue + "#" + e.get_columnFilters()[i].get_condition().get_value()));
                }
            }
            if (gridFiltered) {
                $('[id$=hFiltered]').val('1');
            }
            else {
                $('[id$=hFiltered]').val('0');
            }
        }

        function AceptarNuevaQlikApp() {
            if (ComprobarCamposQlikApp()) {
                if (ComprobarCamposNumericosQlikApp()) {
                    GuardarQlikApp();
                }
            }
        }

        function AceptarConfigurarQlikApp() {
            if (ComprobarCamposQlikApp()) {
                if (ComprobarCamposNumericosQlikApp()) {
                    GuardarQlikApp();
                }
            }
        }

        function CancelarQlikApp() {
            LimpiarModalQlikApp();
            $('#popupFondo').hide();
        }

        function LimpiarModalQlikApp() {
            $('[id$=hId]').val("");
            $('[id$=txtNombre]').val("");
            $('[id$=chkActiva]').attr('checked', false);
            $('[id$=txtUrl]').val("");
            $('[id$=hUrl]').val("");
            $('[id$=txtPuertoRestAPI]').val("");
            $('[id$=lstIdApp]').empty();
            $('[id$=lstIdSheet]').empty();
            $('#pnlQlikApps').hide();
        }

        function NuevaQlikApp() {
            MostrarQlikAppVacia();

            $('[id$=lblNuevaQlikApp]').text(TextosQlikApps[16]);            //Nueva App Qlik

            $('[id$=btnAceptarNueva]').css("display", "inherit");
            $('[id$=btnAceptarConfigurar]').css("display", "none");

            var punto = window.center({ width: $('#pnlQlikApps').width(), height: $('#pnlQlikApps').height() });
            $('#pnlQlikApps').css('top', punto.y + 'px');
            $('#pnlQlikApps').css('left', punto.x + 'px');
            $('#pnlQlikApps').show();
        }

        function MostrarQlikAppVacia() {
            $('#popupFondo').css('height', $(document).height());
            $('#popupFondo').show();

            $('[id$=lblNombre]').text(TextosQlikApps[18]);                  //Nombre: 
            $('[id$=lblActiva]').text(TextosQlikApps[19]);                  //Activa: 
            $('[id$=lblServidorQlik]').text(TextosQlikApps[20]);            //Servidor de Qlik
            $('[id$=lblUrl]').text(TextosQlikApps[21]);                     //Url: 
            $('[id$=lblPuertoRestAPI]').text(TextosQlikApps[24]);           //Puerto REST API: 
            $('[id$=lblQlikAppSheet]').text(TextosQlikApps[25]);            //Aplicacion Qlik
            $('[id$=lblIdApp]').text(TextosQlikApps[26]);                   //Aplicacion:
            $('[id$=lblIdSheet]').text(TextosQlikApps[27]);                 //Hoja inicial:
            $('[id$=btnAceptarNueva]').text(TextosQlikApps[28]);            //Aceptar
            $('[id$=btnAceptarConfigurar]').text(TextosQlikApps[28]);       //Aceptar
            $('[id$=btnCancelar]').text(TextosQlikApps[29]);                //Cancelar

            $('[id$=txtNombre]').attr('title', TextosQlikApps[31].replace(/#/g, '\n'));          //Tooltip Nombre
            $('[id$=txtUrl]').attr('title', TextosQlikApps[32].replace(/#/g, '\n'));             //Tooltip Url
            $('[id$=txtPuertoRestAPI]').attr('title', TextosQlikApps[35].replace(/#/g, '\n'));   //Tooltip Puerto de la REST API
            $('[id$=lstIdApp]').attr('title', TextosQlikApps[36].replace(/#/g, '\n'));           //Tooltip Aplicacion
            $('[id$=lstIdSheet]').attr('title', TextosQlikApps[37].replace(/#/g, '\n'));         //Tooltip Hoja inicial

            $('[id$=hId]').val("");
            $('[id$=txtNombre]').val("");
            $('[id$=chkActiva]').attr('checked', false);
            $('[id$=txtUrl]').val("");
            $('[id$=hUrl]').val("");
            $('[id$=txtPuertoRestAPI]').val("");
            $('[id$=lstIdApp]').empty();
            $('[id$=lstIdSheet]').empty();
        }

        function ComprobarCamposQlikApp() {
            var nombre = $('[id$=txtNombre]').val();
            var url = $('[id$=txtUrl]').val();
            var puertoRestAPI = $('[id$=txtPuertoRestAPI]').val();
            var idApp = $('[id$=lstIdApp]').val();

            var mensajeError = TextosQlikApps[30] + '\n\n';         //Los siguiente campos son obligatorios: 

            if (nombre == "") {
                mensajeError += '- ' + TextosQlikApps[8] + '\n';    //Nombre de la aplicacion 
            }
            if (url == "") {
                mensajeError += '- ' + TextosQlikApps[9] + '\n';    //Url del servidor de Qlik 
            }
            if (puertoRestAPI == "") {
                mensajeError += '- ' + TextosQlikApps[12] + '\n';   //Puerto de la REST API para la autenticacion
            }
            if ((!idApp) || (idApp == "")) {
                mensajeError += '- ' + TextosQlikApps[10] + '\n';   //Aplicacion Qlik 
            }

            if (mensajeError != TextosQlikApps[30] + '\n\n') {
                alert(mensajeError);
                return false;
            } else {
                return true;
            }
        }

        function ComprobarCamposNumericosQlikApp() {
            var puertoRestAPI = $('[id$=txtPuertoRestAPI]').val();

            var mensajeError = TextosQlikApps[38] + '\n\n';         //Los siguientes campos son de tipo numerico: 

            if (isNaN(puertoRestAPI)) {
                mensajeError += '- ' + TextosQlikApps[12] + '\n';   //Puerto de la REST API para la autenticacion
            }

            if (mensajeError != TextosQlikApps[38] + '\n\n') {
                alert(mensajeError);
                return false;
            } else {
                return true;
            }
        }

        function ComprobarServidorQlikCorrecto(origenLlamada) {
            var correcto = false;
            var modalFormatoUrl = false;
            var sUrlOld = $('[id$=hUrl]').val();
            var sUrl = $('[id$=txtUrl]').val();
            if (sUrl != sUrlOld) {
                $('[id$=hUrl]').val(sUrl);
                $('[id$=txtPuertoRestAPI]').val("");
                $('[id$=lstIdApp]').empty();
                $('[id$=lstIdSheet]').empty();
                if (sUrl != "") {
                    if (isUrlValid(sUrl)) {
                        var to = rutaFS + 'BI/QlikApps.aspx/ComprobarServidorQlikCorrecto';
                        $.when($.ajax({
                            type: 'POST',
                            url: to,
                            data: JSON.stringify({ sUrl: sUrl }),
                            contentType: 'application/json;charset=utf-8',
                            dataType: 'json',
                            async: false
                        })).done(function (msg) {
                            if (msg.d == "error") {
                                correcto = false;
                            } else {
                                if ($('[id$=txtPuertoRestAPI]').val() == "") {
                                    $('[id$=txtPuertoRestAPI]').val(msg.d);
                                }
                                correcto = true;
                            }
                        });
                        if (correcto) {
                            var to = rutaFS + 'BI/QlikApps.aspx/ObtenerAplicacionesQlik';
                            var sUrl = $('[id$=txtUrl]').val();
                            $.when($.ajax({
                                type: 'POST',
                                url: to,
                                data: JSON.stringify({ sUrl: sUrl }),
                                contentType: 'application/json;charset=utf-8',
                                dataType: 'json',
                                async: false
                            })).done(function (msg) {
                                $('[id$=lstIdApp]').append($('<option>', {
                                    value: "",
                                    text: ""
                                }));
                                $(msg.d).each(function (index, item) {
                                    $('[id$=lstIdApp]').append($('<option>', {
                                        value: item.id,
                                        text: item.name
                                    }));
                                });
                            });
                        }
                    }
                    else {
                        alert(TextosQlikApps[39].replace(/#/g, '\n'));      //El formato de la URL no es correcto...
                        modalFormatoUrl = true
                        correcto = false
                    }
                }
                else {
                    correcto = true
                }
            }
            else {
                correcto = true
            }

            $('[id$=txtUrl]').off('focusout');

            if (origenLlamada != "cargaDatosQlikApp") {
                OcultarModal('ctl00_ctl00_ModalProgress');
            }

            if ((!correcto) && (!modalFormatoUrl)) {
                alert(TextosQlikApps[40].replace(/#/g, '\n'));        //No se ha podido conectar al servidor de Qlik Sense. Compruebe que la URL es correcta o la correcta instalacion del certificado en el servidor
            }

            return correcto;
        }

        function isUrlValid(url) {
            return /^(https|http):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
        }

        function MostrarModal(idModal) {
            var modalprog = $find(idModal);
            if (modalprog) modalprog.show();
        }

        function OcultarModal(idModal) {
            var modalprog = $find(idModal);
            if (modalprog) modalprog.hide();
        }

        function OnSeleccionarAplicacion(origenLlamada) {
            $('[id$=lstIdSheet]').empty();
            if ($('[id$=lstIdSheet]').children('option').length == 0) {
                var to = rutaFS + 'BI/QlikApps.aspx/ObtenerHojasQlik';
                var sUrl = $('[id$=txtUrl]').val();
                var sIdApp = $('[id$=lstIdApp]').val();
                if ((sUrl != "") && (sIdApp != "")) {
                    $.when($.ajax({
                        type: 'POST',
                        url: to,
                        data: JSON.stringify({ sUrl: sUrl, sIdApp: sIdApp }),
                        contentType: 'application/json;charset=utf-8',
                        dataType: 'json',
                        async: false
                    })).done(function (msg) {
                        $('[id$=lstIdSheet]').append($('<option>', {
                            value: "",
                            text: ""
                        }));
                        $(msg.d).each(function (index, item) {
                            $('[id$=lstIdSheet]').append($('<option>', {
                                value: item.id,
                                text: item.name
                            }));
                        });
                    });
                }
            }
            if (origenLlamada != "cargaDatosQlikApp") {
                OcultarModal('ctl00_ctl00_ModalProgress');
            }
        }

        function GuardarQlikApp() {
            var to = rutaFS + 'BI/QlikApps.aspx/GuardarQlikApp';
            var lId = $('[id$=hId]').val();
            var sNombre = $('[id$=txtNombre]').val();
            var bActiva = $('[id$=chkActiva]').is(':checked');
            var sUrl = $('[id$=txtUrl]').val();
            var lPuertoRestAPI = $('[id$=txtPuertoRestAPI]').val();
            var sIdApp = $('[id$=lstIdApp]').val();
            var sApp = $('[id$=lstIdApp] option:selected').text();
            var sIdSheet = $('[id$=lstIdSheet]').val();
            var sSheet = $('[id$=lstIdSheet] option:selected').text();

            $.when($.ajax({
                type: 'POST',
                url: to,
                data: JSON.stringify({ lId: lId, sNombre: sNombre, bActiva: bActiva, sUrl: sUrl, lPuertoRestAPI: lPuertoRestAPI, sIdApp: sIdApp, sApp: sApp, sIdSheet: sIdSheet, sSheet: sSheet }),
                contentType: 'application/json;charset=utf-8',
                dataType: 'json',
                async: true
            })).done(function (msg) {
                var btnUpdateGrid = $('[id$=btnUpdateGrid]').attr('id');
                __doPostBack(btnUpdateGrid, '');
                LimpiarModalQlikApp();
                $('#popupFondo').hide();
            });
        }

        function ComprobarUnaFilaSeleccionada() {
            var grid = $find($('[id$=whgQlikApps]').attr('id')).get_gridView();
            var selectedRows = grid.get_behaviors().get_selection().get_selectedRows();
            if (selectedRows.get_length() != 1) {
                alert(TextosQlikApps[41]);                     //Se debe seleccionar una fila
                return false;
            }
            return true;
        }

        function ConfigurarQlikApp() {
            if (ComprobarUnaFilaSeleccionada()) {
                MostrarQlikAppVacia();
                MostrarModal('ctl00_ctl00_ModalProgress');
                setTimeout(ObtenerDatosQlikApp, 500);
            }
        }

        function ObtenerDatosQlikApp() {
            var to = rutaFS + 'BI/QlikApps.aspx/ObtenerQlikApp';
            var grid = $find($('[id$=whgQlikApps]').attr('id')).get_gridView();
            var selectedRows = grid.get_behaviors().get_selection().get_selectedRows();
            var lId = selectedRows.getItem(0).get_cellByColumnKey("ID").get_value();
            /*Obtenemos en formato string todos los datos que mostraremos en la pantalla*/
            $.when($.ajax({
                type: 'POST',
                url: to,
                data: JSON.stringify({ lId: lId }),
                contentType: 'application/json;',
                async: false
            })).done(function (msg) {
                $('[id$=lblNuevaQlikApp]').text(TextosQlikApps[17]);            //Configurar App Qlik
                var oQlikApp = msg.d;
                $('[id$=hId]').val(oQlikApp.Id);
                $('[id$=txtNombre]').val(oQlikApp.Nombre);
                if (oQlikApp.Activa) {
                    $('[id$=chkActiva]').attr('checked', 'checked');
                } else {
                    $('[id$=chkActiva]').attr('checked', false);
                }
                $('[id$=txtUrl]').val(oQlikApp.Url);
                if (ComprobarServidorQlikCorrecto("cargaDatosQlikApp")) {
                    $('[id$=lstIdApp]').find('option').each(function () {
                        if ($(this).val() == oQlikApp.IdApp) {
                            $(this).attr('selected', 'selected');
                        }
                    });
                    OnSeleccionarAplicacion("cargaDatosQlikApp");
                    $('[id$=lstIdSheet]').find('option').each(function () {
                        if ($(this).val() == oQlikApp.IdSheet) {
                            $(this).attr('selected', 'selected');
                        }
                    });
                }
                $('[id$=btnAceptarNueva]').css("display", "none");
                $('[id$=btnAceptarConfigurar]').css("display", "inherit");
                var punto = window.center({ width: $('#pnlQlikApps').width(), height: $('#pnlQlikApps').height() });
                $('#pnlQlikApps').css('top', punto.y + 'px');
                $('#pnlQlikApps').css('left', punto.x + 'px');
                $('#pnlQlikApps').show();
            });
            OcultarModal('ctl00_ctl00_ModalProgress');
        }

        function EliminarQlikApp() {
            if (ComprobarUnaFilaSeleccionada()) {
                var statusConfirm = confirm(TextosQlikApps[42]);   //¿Desea eliminar la App Qlik seleccionada?
                if (statusConfirm) {
                    EliminarApp();
                }
            }
        }

        function EliminarApp() {
            var to = rutaFS + 'BI/QlikApps.aspx/EliminarQlikApp';
            var grid = $find($('[id$=whgQlikApps]').attr('id')).get_gridView();
            var selectedRows = grid.get_behaviors().get_selection().get_selectedRows();
            var lId = selectedRows.getItem(0).get_cellByColumnKey("ID").get_value();
            $.when($.ajax({
                type: 'POST',
                url: to,
                data: JSON.stringify({ lId: lId }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                async: true
            }).done(function (msg) {
                grid._behaviors._selection.__clearAll();
                var btnUpdateGrid = $('[id$=btnUpdateGrid]').attr('id');
                __doPostBack(btnUpdateGrid, '');
            }));
        }

        function AccesoAQlik() {
            MostrarModal('ctl00_ctl00_ModalProgress');
            setTimeout(CargarVentanaAccesoAQlik, 500);
        }

        function CargarVentanaAccesoAQlik() {
            $('#popupFondo').css('height', $(document).height());
            $('#popupFondo').show();

            $('[id$=lblAccesoQlik]').text(TextosQlikApps[5]);               //Acceso a Qlik
            $('[id$=lblPerfilesFSN]').text(TextosQlikApps[43]);             //Perfil
            $('[id$=lblUsuariosQlik]').text(TextosQlikApps[44]);            //Usuario de Qlik
            $('[id$=btnAceptarAccesoQlik]').text(TextosQlikApps[45]);       //Guardar
            $('[id$=btnCancelarAccesoQlik]').text(TextosQlikApps[46]);      //Cerrar

            $('#tablePerfilesUsuariosQlik tbody').find('tr:gt(0)').remove();
            CargarPerfilesUsuariosQlikAsignados("Carga");
            CargarPerfilesUsuariosQlikNuevo("Carga");

            var punto = window.center({ width: $('#pnlAccesoQlik').width(), height: $('#pnlAccesoQlik').height() });
            $('#pnlAccesoQlik').css('top', punto.y + 'px');
            $('#pnlAccesoQlik').css('left', punto.x + 'px');
            $('#pnlAccesoQlik').show();

            OcultarModal('ctl00_ctl00_ModalProgress');
        }

        function RecargarVentanaAccesoAQlik() {
            $('#tablePerfilesUsuariosQlik tbody').find('tr:gt(0)').remove();
            CargarPerfilesUsuariosQlikAsignados("Recarga");
            CargarPerfilesUsuariosQlikNuevo("Recarga");

            OcultarModal('ctl00_ctl00_ModalProgress');
        }

        function CargarPerfilesUsuariosQlikAsignados(origenLlamada) {
            var oPerfUsuQlik;
            var oServidoresQlik;
            var to = rutaFS + 'BI/QlikApps.aspx/ObtenerPerfilesUsuariosQlikAsignados';
            $.when($.ajax({
                type: 'POST',
                url: to,
                data: JSON.stringify(),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                async: false
            }).done(function (msg) {
                oPerfUsuQlik = msg.d;
            }));
            to = rutaFS + 'BI/QlikApps.aspx/ObtenerServidoresQlikConfigurados';
            $.when($.ajax({
                type: 'POST',
                url: to,
                data: JSON.stringify(),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                async: false
            }).done(function (msg) {
                oServidoresQlik = msg.d;
            }));
            if ((oPerfUsuQlik != null) && (oServidoresQlik != null)) {
                for (var i = 0; i < oPerfUsuQlik.length; i++) {
                    var lineaTabla = '';
                    lineaTabla += '<tr>';
                    lineaTabla += '<td width="45%" height="60px" style="border-bottom: 1px solid #CCCCCC; overflow:hidden;">';
                    lineaTabla += '<div style="clear:both; float:left; max-width:' + (origenLlamada === "Carga" ? Math.floor((($('#pnlAccesoQlik').width() * $('#divPerfilesUsuariosQlik').width() / 100) * $('#colPerfilesFSN').width() / 100) - 5) : $('#colPerfilesFSN').width()) + 'px; min-width:' + (origenLlamada === "Carga" ? Math.floor((($('#pnlAccesoQlik').width() * $('#divPerfilesUsuariosQlik').width() / 100) * $('#colPerfilesFSN').width() / 100) - 5) : $('#colPerfilesFSN').width()) + 'px;">';
                    lineaTabla += '<span id="lblPerfil_' + oPerfUsuQlik[i].ID + '" style="font-weight:bold; margin-left:5px;">' + oPerfUsuQlik[i].COD + ' - ' + oPerfUsuQlik[i].DEN + '</span>';
                    lineaTabla += '</div>';
                    lineaTabla += '</td>';
                    lineaTabla += '<td width="55%" height="60px" style="border-bottom: 1px solid #CCCCCC; overflow:hidden;">';
                    lineaTabla += '<div style="clear:both; float:left; max-width:' + (origenLlamada === "Carga" ? Math.floor((($('#pnlAccesoQlik').width() * $('#divPerfilesUsuariosQlik').width() / 100) * $('#colUsuariosQlik').width() / 100)) : $('#colUsuariosQlik').width()) + 'px; min-width:' + (origenLlamada === "Carga" ? Math.floor((($('#pnlAccesoQlik').width() * $('#divPerfilesUsuariosQlik').width() / 100) * $('#colUsuariosQlik').width() / 100)) : $('#colUsuariosQlik').width()) + 'px;">';
                    lineaTabla += '<span id="lblServidor_' + oPerfUsuQlik[i].ID + '" style="float:left; margin-left:5px; margin-top:5px; max-width:18%; min-width:18%;">' + TextosQlikApps[47] + '</span>';   //Servidor:
                    lineaTabla += '<select id="selServidor_' + oPerfUsuQlik[i].ID + '" style="float:left; margin-left:5px; margin-top:2px; max-width:78%; min-width:78%;" onchange="OnSeleccionarServidorUsuarios($(&#039;#selServidor_' + oPerfUsuQlik[i].ID + '&#039;), $(&#039;#selUsuario_' + oPerfUsuQlik[i].ID + '&#039;), &#039;CambioSelect&#039;)">';
                    lineaTabla += '<option value=""></option>';
                    for (var j = 0; j < oServidoresQlik.length; j++) {
                        lineaTabla += '<option value="' + oServidoresQlik[j].URL + '"' + (oServidoresQlik[j].URL === oPerfUsuQlik[i].URL_QLIK ? ' selected' : '') + '>' + oServidoresQlik[j].URL + '</option>';
                    }
                    lineaTabla += '</select>';
                    lineaTabla += '</div>';
                    lineaTabla += '<div style="clear:both; float:left; max-width:' + (origenLlamada === "Carga" ? Math.floor((($('#pnlAccesoQlik').width() * $('#divPerfilesUsuariosQlik').width() / 100) * $('#colUsuariosQlik').width() / 100)) : $('#colUsuariosQlik').width()) + 'px; min-width:' + (origenLlamada === "Carga" ? Math.floor((($('#pnlAccesoQlik').width() * $('#divPerfilesUsuariosQlik').width() / 100) * $('#colUsuariosQlik').width() / 100)) : $('#colUsuariosQlik').width()) + 'px;">';
                    lineaTabla += '<span id="lblUsuario_' + oPerfUsuQlik[i].ID + '" style="float:left; margin-left:5px; margin-top:5px; max-width:18%; min-width:18%;">' + TextosQlikApps[48] + '</span>';   //Usuario:
                    lineaTabla += '<select id="selUsuario_' + oPerfUsuQlik[i].ID + '" style="float:left; margin-left:5px; margin-top:2px; max-width:78%; min-width:78%;">';
                    lineaTabla += '</select>';
                    lineaTabla += '</div>';
                    lineaTabla += '</td>';
                    lineaTabla += '</tr>';
                    $('#tablePerfilesUsuariosQlik tbody').append(lineaTabla);
                    selServidor_X = '#selServidor_' + oPerfUsuQlik[i].ID;
                    selUsuario_X = '#selUsuario_' + oPerfUsuQlik[i].ID;
                    OnSeleccionarServidorUsuarios($(selServidor_X), $(selUsuario_X), 'Carga');
                    $(selUsuario_X + ' select').val(oPerfUsuQlik[i].USUARIO_QLIK);
                    $(selUsuario_X).val(oPerfUsuQlik[i].USUARIO_QLIK).change();
                }
            }
        }

        function CargarPerfilesUsuariosQlikNuevo(origenLlamada) {
            var oPerfilesFSN;
            var oServidoresQlik;
            var to = rutaFS + 'BI/QlikApps.aspx/ObtenerPerfilesFSNNoAsignados';
            $.when($.ajax({
                type: 'POST',
                url: to,
                data: JSON.stringify(),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                async: false
            }).done(function (msg) {
                oPerfilesFSN = msg.d;
            }));
            to = rutaFS + 'BI/QlikApps.aspx/ObtenerServidoresQlikConfigurados';
            $.when($.ajax({
                type: 'POST',
                url: to,
                data: JSON.stringify(),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                async: false
            }).done(function (msg) {
                oServidoresQlik = msg.d;
            }));
            var lineaTabla = '';
            lineaTabla += '<tr>';
            lineaTabla += '<td width="45%" height="60px" style="overflow:hidden;">';
            lineaTabla += '<div style="clear:both; float:left; max-width:' + (origenLlamada === "Carga" ? Math.floor((($('#pnlAccesoQlik').width() * $('#divPerfilesUsuariosQlik').width() / 100) * $('#colPerfilesFSN').width() / 100) - 5) : $('#colPerfilesFSN').width()) + 'px; min-width:' + (origenLlamada === "Carga" ? Math.floor((($('#pnlAccesoQlik').width() * $('#divPerfilesUsuariosQlik').width() / 100) * $('#colPerfilesFSN').width() / 100) - 5) : $('#colPerfilesFSN').width()) + 'px;">';
            lineaTabla += '<select id="selPerfil_Nuevo" style="float:left; margin-left:5px; margin-top:2px; max-width:95%; min-width:95%;">';
            lineaTabla += '<option value=""></option>';
            for (var i = 0; i < oPerfilesFSN.length; i++) {
                lineaTabla += '<option value="' + oPerfilesFSN[i].ID + '">' + oPerfilesFSN[i].COD + ' - ' + oPerfilesFSN[i].DEN + '</option>';
            }
            lineaTabla += '</div>';
            lineaTabla += '</td>';
            lineaTabla += '<td width="55%" height="60px" style="overflow:hidden;">';
            lineaTabla += '<div style="clear:both; float:left; max-width:' + (origenLlamada === "Carga" ? Math.floor((($('#pnlAccesoQlik').width() * $('#divPerfilesUsuariosQlik').width() / 100) * $('#colUsuariosQlik').width() / 100)) : $('#colUsuariosQlik').width()) + 'px; min-width:' + (origenLlamada === "Carga" ? Math.floor((($('#pnlAccesoQlik').width() * $('#divPerfilesUsuariosQlik').width() / 100) * $('#colUsuariosQlik').width() / 100)) : $('#colUsuariosQlik').width()) + 'px;">';
            lineaTabla += '<span id="lblServidor_Nuevo" style="float:left; margin-left:5px; margin-top:5px; max-width:18%; min-width:18%;">' + TextosQlikApps[47] + '</span>';   //Servidor:
            lineaTabla += '<select id="selServidor_Nuevo" style="float:left; margin-left:5px; margin-top:2px; max-width:78%; min-width:78%;" onchange="OnSeleccionarServidorUsuarios($(&#039;#selServidor_Nuevo&#039;), $(&#039;#selUsuario_Nuevo&#039;), &#039;CambioSelect&#039;)">';
            lineaTabla += '<option value=""></option>';
            for (var i = 0; i < oServidoresQlik.length; i++) {
                lineaTabla += '<option value="' + oServidoresQlik[i].URL + '">' + oServidoresQlik[i].URL + '</option>';
            }
            lineaTabla += '</select>';
            lineaTabla += '</div>';
            lineaTabla += '<div style="clear:both; float:left; max-width:' + (origenLlamada === "Carga" ? Math.floor((($('#pnlAccesoQlik').width() * $('#divPerfilesUsuariosQlik').width() / 100) * $('#colUsuariosQlik').width() / 100)) : $('#colUsuariosQlik').width()) + 'px; min-width:' + (origenLlamada === "Carga" ? Math.floor((($('#pnlAccesoQlik').width() * $('#divPerfilesUsuariosQlik').width() / 100) * $('#colUsuariosQlik').width() / 100)) : $('#colUsuariosQlik').width()) + 'px;">';
            lineaTabla += '<span id="lblUsuario_Nuevo" style="float:left; margin-left:5px; margin-top:5px; max-width:18%; min-width:18%;">' + TextosQlikApps[48] + '</span>';   //Usuario:
            lineaTabla += '<select id="selUsuario_Nuevo" style="float:left; margin-left:5px; margin-top:2px; max-width:78%; min-width:78%;">';
            lineaTabla += '</select>';
            lineaTabla += '</div>';
            lineaTabla += '</td>';
            lineaTabla += '</tr>';
            $('#tablePerfilesUsuariosQlik tbody').append(lineaTabla);
        }

        function OnSeleccionarServidorUsuarios(selServidor_X, selUsuario_X, origenLlamada) {
            if (origenLlamada === "CambioSelect") {
                MostrarModal('ctl00_ctl00_ModalProgress');
                setTimeout(function () {
                    CargarUsuariosServidorQlik(selServidor_X, selUsuario_X);
                }, 500);
            }
            else {
                CargarUsuariosServidorQlik(selServidor_X, selUsuario_X);
            }
        }

        function CargarUsuariosServidorQlik(selServidor_X, selUsuario_X) {
            selUsuario_X.empty();
            if (selUsuario_X.children('option').length == 0) {
                var oUsuariosQlik;
                var to = rutaFS + 'BI/QlikApps.aspx/ObtenerUsuariosQlik';
                var sUrl = selServidor_X.val();
                $.when($.ajax({
                    type: 'POST',
                    url: to,
                    data: JSON.stringify({ sUrl: sUrl }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    async: false
                }).done(function (msg) {
                    oUsuariosQlik = msg.d;
                }));
                selUsuario_X.append($('<option>', {
                    value: "",
                    text: ""
                }));
                $(oUsuariosQlik).each(function (index, item) {
                    selUsuario_X.append($('<option>', {
                        value: item.id,
                        text: item.userDirectory + '\\' + item.userId
                    }));
                });
            }
            OcultarModal('ctl00_ctl00_ModalProgress');
        }

        function AceptarAccesoQlik() {
            MostrarModal('ctl00_ctl00_ModalProgress');
            setTimeout(GuardarAccesoQlik, 500);
        }

        function CancelarAccesoQlik() {
            $('#pnlAccesoQlik').hide();
            $('#popupFondo').hide();
        }

        function GuardarAccesoQlik() {
            var datosTabla = [];
            $('#tablePerfilesUsuariosQlik > tbody  > tr').each(function (iFila, fila) {
                //La primera fila es la cabecera de la tabla
                if (iFila > 0) {
                    var perfilFSN="";
                    var servidorQlik="";
                    var usuarioQlik = "";
                    var useridQlik = "";
                    $(fila).find('td').each(function (jColumna, elemento) {
                        switch(jColumna) {
                            case 0:
                                if ($(elemento).find('span').length == 1) {
                                    perfilFSN = $(elemento).find('span').attr('id').split('lblPerfil_')[1];
                                }
                                else if ($(elemento).find('select').length == 1) {
                                    perfilFSN = $(elemento).find('select').val();
                                }
                                break;
                            case 1:
                                if ($(elemento).find('select').length == 2) {
                                    servidorQlik = $($(elemento).find('select')[0]).val();
                                    usuarioQlik = $($(elemento).find('select')[1]).val();
                                    useridQlik = $($(elemento).find('select')[1]).children(':selected').text();
                                }
                                break;
                        }
                    });
                    datosTabla.push({ "ID": iFila, "PERF": perfilFSN, "URL_QLIK": servidorQlik, "USUARIO_QLIK": usuarioQlik, "USERID_QLIK": useridQlik });
                }
            });
            var to = rutaFS + 'BI/QlikApps.aspx/GuardarPerfilesUsuariosQlik';
            $.when($.ajax({
                type: 'POST',
                url: to,
                data: JSON.stringify({ lstPerfUsuQlik: datosTabla }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                async: false
            }).done(function (msg) {
                var iResultado = msg.d;
                if (iResultado == -1) {
                    alert(TextosQlikApps[49] + '\n\n' + TextosQlikApps[50]);    //No ha sido posible guardar las asignaciones: Se ha producido un error en la base de datos.
                }
                else if (iResultado == -2) {
                    alert(TextosQlikApps[49] + '\n\n' + TextosQlikApps[51]);    //No ha sido posible guardar las asignaciones: Se ha producido un error al intentar guardar los cambios.
                }
            }));
            RecargarVentanaAccesoAQlik();
        }
    </script>

    <div style="clear:both; float:left; width:100%;">
		    <fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
    </div>

    <asp:UpdatePanel runat="server" ID="upPager" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:HiddenField runat="server" ID="hFiltered" Value="0" />
            <asp:HiddenField runat="server" ID="hFilteredColumns" Value="" />
            <asp:HiddenField runat="server" ID="hFilteredCondition" Value="" />
            <asp:HiddenField runat="server" ID="hFilteredValue" Value="" />
            <asp:HiddenField runat="server" ID="hOrderedColumn" Value="" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Button runat="server" ID="btnPager" style="display:none;" />

    <div style="clear:both; float:left; width:100%;">
        <asp:Button runat="server" ID="btnUpdateGrid" style="display:none;"></asp:Button> 
		<asp:UpdatePanel ID="updGridQlikApps" ChildrenAsTriggers="false" runat="server" UpdateMode="Conditional">						
			<Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnPager" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnUpdateGrid" EventName="Click" />
			</Triggers>
			<ContentTemplate>
				<ig:WebHierarchicalDataGrid runat="server" ID="whgQlikApps" AutoGenerateBands="false"
					AutoGenerateColumns="false" Width="100%" EnableAjax="true" 
					EnableAjaxViewState="true" EnableDataViewState="true">
                    <GroupingSettings EnableColumnGrouping="True" GroupAreaVisibility="Visible" ></GroupingSettings>
					<Behaviors>
                        <ig:Activation Enabled="true"/>
						<ig:Filtering Alignment="Top" Enabled="true" Visibility="Visible" AnimationEnabled="true">
                            <FilteringClientEvents DataFiltering="whgQlikApps_Filtering" />
                        </ig:Filtering>                 
						<ig:RowSelectors Enabled="true" EnableInheritance="true">
						</ig:RowSelectors>
						<ig:Selection Enabled="true" RowSelectType="Multiple" CellSelectType="None" ColumnSelectType="None">
						</ig:Selection>
						<ig:Sorting Enabled="true" SortingMode="Single" SortingClientEvents-ColumnSorting="whgQlikApps_Sorting"></ig:Sorting>
						<ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
						<ig:ColumnMoving Enabled="false"></ig:ColumnMoving>								
						<ig:Paging Enabled="true" PagerAppearance="Top">
							<PagerTemplate>
								<div class="CabeceraBotones" style="clear:both; float:left; width:100%;"> 
									<div style="float:right; margin:5px 5px 0px 0px;">
                                        <asp:Panel runat="server" ID="pnlNuevaQlikApp" onclick="NuevaQlikApp()" class="botonDerechaCabecera" style="line-height:22px; float:left;">
                                            <asp:Image runat="server" ID="imgNuevaQlikApp" CssClass="imagenCabecera" />
											<asp:Label runat="server" ID="lnkNuevaQlikApp" CssClass="fntLogin2" style="margin-left:3px" Text="dNuevaQlikApp"></asp:Label>
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="pnlConfigurarQlikApp" onclick="ConfigurarQlikApp()" class="botonDerechaCabecera" style="line-height:22px; float:left;">           
											<asp:Image runat="server" ID="imgConfigurarQlikApp" CssClass="imagenCabecera"/>
											<asp:Label runat="server" ID="lnkConfigurarQlikApp" CssClass="fntLogin2" style="margin-left:3px" Text="dConfigurarQlikApp"></asp:Label>           
										</asp:Panel>
                                        <asp:Panel runat="server" ID="pnlEliminarQlikApp" onclick="EliminarQlikApp()" class="botonDerechaCabecera" style="line-height:22px; float:left;">										       
											<asp:Image runat="server" ID="imgEliminarQlikApp" CssClass="imagenCabecera" />
											<asp:Label runat="server" ID="lnkEliminarQlikApp" CssClass="fntLogin2" style="margin-left:3px" Text="dEliminarQlikApp"></asp:Label>           
										</asp:Panel>
										<asp:Panel runat="server" ID="pnlAccesoAQlik" onclick="AccesoAQlik()" class="botonDerechaCabecera" style="line-height:22px; float:left;">            
											<asp:Image runat="server" ID="imgAccesoAQlik" CssClass="imagenCabecera" />
											<asp:Label runat="server" ID="lblAccesoAQlik" CssClass="fntLogin2" style="margin-left:3px" Text="dAccesoAQlik"></asp:Label>           
										</asp:Panel>
									</div>
									<div style="float:left; padding:3px 0px 2px 0px; margin-left:10px;">
										<div style="clear: both; float: left; margin-right: 5px;">
                                            <asp:Image ID="ImgBtnFirst" runat="server" OnClientClick="FirstPage()" style="margin-right:2px;" />
                                            <asp:Image ID="ImgBtnPrev" runat="server" OnClientClick="PrevPage()" />
                                        </div>
                                        <div style="float: left; margin-right:5px;">
                                            <asp:Label ID="lblPage" runat="server" Text="dPage" Style="margin-right: 3px;"></asp:Label>
                                            <asp:DropDownList ID="PagerPageList" runat="server" Style="margin-right: 3px;" onchange="return IndexChanged()" />
                                            <asp:Label ID="lblOF" runat="server" Text="dOf" Style="margin-right: 3px;"></asp:Label>
                                            <asp:Label ID="lblCount" runat="server" />
                                        </div>
                                        <div style="float: left;">
                                            <asp:Image ID="ImgBtnNext" runat="server" OnClientClick="NextPage()" style="margin-right:2px;" />
                                            <asp:Image ID="ImgBtnLast" runat="server" OnClientClick="LastPage()" />
                                        </div>
									</div>								
								</div>						
							</PagerTemplate>
						</ig:Paging>						
					</Behaviors>
				</ig:WebHierarchicalDataGrid>                
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>

    <div id="pnlQlikApps" class="Rectangulo" style="display:none; position:absolute; z-index:1002; min-width:920px; min-height:280px">        
        <div style="position:relative">
            <div id="divCabeceraQlikApps" class="PageHeaderBackground" style="position:relative; z-index:1; float:left; height:25px; line-height:25px; background-color:#AB0020">			
			    <span id="lblNuevaQlikApp" class="Texto14 TextoClaro Negrita" style="float:left; margin-left:10px"></span>
                <input type="image" src="~/images/Bt_Cerrar.png" id="imgCerrar" runat="server" SkinID="Cerrar" style="border:0px; width:25px; float:right; cursor:pointer"/>
		    </div>	
            <div style="clear:both; position:relative; width:100%; float:left; margin-top:20px; margin-left:30px">
                <input type="hidden" id="hId" value="" />
                <div style="float:left; margin-left:12px">
                    <span id="lblNombre" class="Texto12 Negrita" runat="server"></span>
                    <input id="txtNombre" runat="server" style="margin-left:12px; width:470px;" maxlength="100"/>
                </div>
                <div style="float:left; margin-left:218px">
                    <span ID="lblActiva" class="Texto12 Negrita" runat="server"></span>
                    <input id="chkActiva" type="checkbox" runat="server" style="margin-left:12px"/>
                </div>
            </div>
            <div id="divServidorQlik" class="dContenedor" style="clear:both; float:left; margin-top:10px; margin-bottom:10px; margin-left:30px; padding-bottom:10px; width:94%; background-color:transparent">                                  
                <span id="lblServidorQlik" style="float:left; margin-top:10px; margin-left: 10px" class="Texto14 Negrita"></span>
                <div style="clear:both; float:left; margin-top:10px; margin-left:12px">
                    <span id="lblUrl" class="Texto12 Negrita" runat="server"></span>
                    <input id="txtUrl" runat="server" style="margin-left:10px; width:500px" maxlength="200"/>
                    <input type="hidden" id="hUrl" value="" />
                </div>
                <div style="float:left; margin-top:10px; margin-left:38px">
                    <span id="lblPuertoRestAPI" class="Texto12 Negrita" runat="server"></span>
                    <input id="txtPuertoRestAPI" runat="server" style="margin-left:10px; width:135px" maxlength="10"/>
                </div>              
            </div>
            <div id="divQlikAppSheet" class="dContenedor" style="clear:both; float:left; margin-top:10px; margin-bottom:10px; margin-left:30px; padding-bottom:10px; width:94%; background-color:transparent">                                 
                <span id="lblQlikAppSheet" style="float:left; margin-top:10px; margin-left:10px" class="Texto14 Negrita"></span>
                <div style="clear:both; float:left; margin-top:10px; margin-left:12px">
                    <span id="lblIdApp" class="Texto12 Negrita" runat="server" ></span>
                    <select id="lstIdApp" runat="server" style="margin-left:10px; width:320px">
                    </select>
                </div>
                <div style="float:left; margin-top:10px; margin-left:38px">
                    <span id="lblIdSheet" class="Texto12 Negrita" runat="server"></span>
                    <select id="lstIdSheet" runat="server" style="margin-left:10px; width:315px">
                    </select>
                </div>
            </div> 
            <div style="clear:both; float:left; margin-left:400px; margin-top:10px; padding-bottom:10px; width:150px; background-color:transparent">
                <fsn:FSNButton ID="btnAceptarNueva" runat="server" Alineacion="Left" OnClientClick="AceptarNuevaQlikApp(); return false;" ></fsn:FSNButton>   
                <fsn:FSNButton ID="btnAceptarConfigurar" runat="server" Alineacion="Left" OnClientClick="AceptarConfigurarQlikApp(); return false;" style="display:none" ></fsn:FSNButton>   
                <fsn:FSNButton ID="btnCancelar" runat="server" Alineacion="Left" OnClientClick="CancelarQlikApp(); return false;" style="margin-left:10px"></fsn:FSNButton>
            </div>
        </div>
    </div>

    <div id="pnlAccesoQlik" class="Rectangulo" style="display:none; position:absolute; z-index:1002; min-width:920px; max-width:800px;">        
        <div style="position:relative">
            <div id="divCabeceraAccesoQlik" class="PageHeaderBackground" style="position:relative; z-index:1; float:left; height:25px; line-height:25px; background-color:#AB0020">			
			    <span id="lblAccesoQlik" class="Texto14 TextoClaro Negrita" style="float:left; margin-left:10px"></span>
                <input type="image" src="~/images/Bt_Cerrar.png" id="imgCerrarAccesoQlik" runat="server" SkinID="Cerrar" style="border:0px; width:25px; float:right; cursor:pointer"/>
		    </div>	
            <div id="divPerfilesUsuariosQlik" class="dContenedor" style="clear:both; float:left; margin-top:10px; margin-bottom:10px; margin-left:10px; margin-right:10px; padding-bottom:10px; width:97.5%; background-color:transparent; align-items:center;">
                <table id="tablePerfilesUsuariosQlik" style="table-layout:fixed; overflow: hidden; border-collapse:collapse; min-width:100%; max-width:100%;">
                    <tr>
                        <td id="colPerfilesFSN" width="45%" height="20px" style="border-bottom: 1px solid #CCCCCC;">
                            <span id="lblPerfilesFSN" style="float:left; margin-left:5px;" class="Texto14 Negrita"></span>
                        </td>
                        <td id="colUsuariosQlik" width="55%" height="20px" style="border-bottom: 1px solid #CCCCCC;">
                            <span id="lblUsuariosQlik" style="float:left; margin-left:5px;" class="Texto14 Negrita"></span>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="clear:both; float:left; margin-left:400px; margin-top:10px; padding-bottom:10px; width:150px; background-color:transparent">
                <fsn:FSNButton ID="btnAceptarAccesoQlik" runat="server" Alineacion="Left" OnClientClick="AceptarAccesoQlik(); return false;" ></fsn:FSNButton>    
                <fsn:FSNButton ID="btnCancelarAccesoQlik" runat="server" Alineacion="Left" OnClientClick="CancelarAccesoQlik(); return false;" style="margin-left:10px"></fsn:FSNButton>
            </div>
        </div>
    </div>

</asp:Content>


