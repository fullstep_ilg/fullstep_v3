﻿Imports Fullstep.FSNLibrary.TiposDeDatos
Imports System.Web.Services

Public Class BI
    Inherits FSNPage

    Public NoHayCubos As String
    'Public CubosDisponibles As String
    Public CuboRecuperado As Boolean = False



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim mMaster = CType(MyBase.Master, FSNWeb.Menu)
        mMaster.Seleccionar("Informes", "BusinessInteligence")
        ModuloIdioma = ModulosIdiomas.Menu
        NoHayCubos = Textos(66)
        ModuloIdioma = ModulosIdiomas.Cubos
        'CubosDisponibles = Textos(33)                       'Cubos Disponibles: "    

        Dim listaCubos As Fullstep.FSNServer.CubosDenominacion = FSNServer.Get_Object(GetType(Fullstep.FSNServer.CubosDenominacion))
        listaCubos.ObtenerCubosPermisosUsu(Usuario.Cod, Usuario.IdiomaCod, Usuario.UON1, Usuario.UON2, Usuario.UON3)
        Session("DsCubos") = listaCubos
        Dim cubos As Fullstep.FSNServer.Cubos = FSNServer.Get_Object(GetType(Fullstep.FSNServer.Cubos))
        If Request.Form("cubo") = 1 Or Request.Form("cubo") Is Nothing Then
            For i = 1 To listaCubos.CubosDen.Count

                Dim lblID As Label = New Label()
                Dim lblVersion As Label = New Label()
                Dim lblFecAct As Label = New Label()
                lblFecAct.CssClass = "Texto12 Negrita"
                lblVersion.CssClass = "Texto12 Negrita"
                lblID.Visible = False
                lblVersion.Style.Add("margin-bottom", "20px")
                lblVersion.Style.Add("margin-top", "10px")
                lblFecAct.Style.Add("margin-bottom", "20px")
                lblVersion.Style.Add("margin-left", "20px")
                lblFecAct.Style.Add("margin-left", "20px")
                Dim j As Integer = listaCubos.CubosDen(i).Id
                lblID.Text = j
                Dim ds As DataSet = cubos.Get_Cubo(j)
                Dim bd As String = ds.Tables(0).Rows(0).Item(9)
                Dim version As String = "Version" & " " & bd.Substring(bd.Length - 3).Replace("_", ".")
                lblVersion.Text = version
                Dim fecha As String = GetCubeProcessTime(ds.Tables(0).Rows(0).Item(5), ds.Tables(0).Rows(0).Item(9), ds.Tables(0).Rows(0).Item(10), ds.Tables(0).Rows(0).Item(8), ds.Tables(0).Rows(0).Item(6), Encrypter.EncriptarSQLtod("", ds.Tables(0).Rows(0).Item(7), False))
                lblFecAct.Text = fecha
                Dim pnlCubo As Panel = New Panel()
                pnlCubo.Width = 225
                pnlCubo.Height = 225
                Dim imageName As String = ds.Tables(0).Rows(0).Item(6)
                Dim image() As String = imageName.Split("_")
                Dim imgColor As Image = New Image()
                imgColor.ImageUrl = ConfigurationManager.AppSettings("rutaFS") + "BI/images/" + image(1) + "_color.png"
                Dim img As Image = New Image()
                img.ImageUrl = ConfigurationManager.AppSettings("rutaFS") + "BI/images/" + image(1) + ".png"
                img.AlternateText = listaCubos.CubosDen(i).Den
                img.Style.Add("margin-top", "35px")
                Dim nav As String = Request.ServerVariables("HTTP_USER_AGENT")
                If InStr(nav, "MSIE") Then
                    img.Style.Add("margin-bottom", "20px")
                ElseIf InStr(nav, "Mozilla") Then
                    img.Style.Add("margin-bottom", "18px")
                Else
                    img.Style.Add("margin-bottom", "20px")
                End If
                imgColor.Width = pnlCubo.Width
                imgColor.Height = 22
                imgColor.Style.Add("margin-left", "-" & img.Width.ToString())
                pnlCubo.BorderColor = Drawing.Color.Black
                pnlCubo.BorderWidth = 1
                pnlCubo.BorderColor = Drawing.Color.Black
                pnlCubo.Style.Add("margin-bottom", "50px")
                pnlCubo.Style.Add("cursor", "pointer")

                pnlCubo.Style.Add("float", "left")
                pnlCubo.Style.Add("margin-top", "40px")
                pnlCubo.Style.Add("margin-left", "40px")
                pnlCubo.Style.Add("margin-right", "40px")
                pnlCubo.Style.Add("margin-bottom", "40px")


                lblFecAct.Width = pnlCubo.Width
                lblVersion.Width = pnlCubo.Width
                pnlCubo.Attributes.Add("onclick", "AceptarClick(" & lblID.Text & ")")

                pnlCubo.Controls.Add(lblID)
                pnlCubo.Controls.Add(lblVersion)
                pnlCubo.Controls.Add(lblFecAct)
                pnlCubo.Controls.Add(img)
                pnlCubo.Controls.Add(imgColor)

                divPermisosCubos.Controls.Add(pnlCubo)
            Next
        Else
            Session("idCubo") = Request.Form("cubo")
        End If
    End Sub

    Public Function GetCubeProcessTime(ByVal serverName As String, ByVal databaseName As String, ByVal cubeName As String, ByVal domain As String, ByVal user As String, ByVal password As String) As String

        Dim fecha As String = String.Empty
        Dim server As New Microsoft.AnalysisServices.Server()
        Dim server2 As New Microsoft.AnalysisServices.Server()
        Dim database As Microsoft.AnalysisServices.Database
        Dim cube As Microsoft.AnalysisServices.Cube
        Try
            ' Connect to the SSAS server

            server.Connect("Data Source=" & serverName & "; User Id=" & domain & "\" & user & ";Password=" & password & ";")
            ' Get the Database
            database = server.Databases.GetByName(databaseName)
            ' Get the Cube
            cube = database.Cubes.GetByName(cubeName)
            ' Get processDate
            fecha = cube.LastProcessed.ToString("yyyy-MM-dd HH:mm:ss")
            Return fecha
            Exit Function
        Catch ex As Exception
            'Si el servicio está en la mismo servidor que el SSAS, y el servicio está en modo anónimo
            'Utilizara las credenciales del Usuario anónimo de IIS para tratar de conectarse al SSAS

        End Try

        Try
            ' Connect to the SSAS server
            server2.Connect("Data Source=" & Replace(LCase(serverName), "/olap/", "/_openolap/") & "; User Id=" & domain & "\" & user & ";Password=" & password & ";")
            ' Get the Database
            database = server2.Databases.GetByName(databaseName)
            ' Get the Cube
            cube = database.Cubes.GetByName(cubeName)
            ' Get processDate
            fecha = cube.LastProcessed.ToString("yyyy-MM-dd HH:mm:ss")
            Return fecha
            Exit Function
        Catch exception As Exception

            'Dim objWriter As New System.IO.StreamWriter(System.AppDomain.CurrentDomain.BaseDirectory() & "log.txt")
            'objWriter.Write(exception.InnerException.ToString)
            'objWriter.Write(serverName & "," & databaseName & "," & cubeName & "," & domain & "," & user & "," & password)

            'objWriter.Close()

            ' Uups
            fecha = "Not Available"
        End Try
        Return fecha
    End Function

End Class