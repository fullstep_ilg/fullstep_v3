﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master" CodeBehind="VisorActividad.aspx.vb" Inherits="Fullstep.FSNWeb.VisorActividad" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<meta http-equiv="X-UA-Compatible" content="IE=9"/>
	<link type="text/css" href="css/ig.ui.chart.igtheme.transparent.css" rel="stylesheet" />
	<link type="text/css" href="css/jquery-ui.css" rel="stylesheet" />

	<link href="css/infragistics.theme.css" rel="stylesheet" />
	<link href="css/infragistics.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server"> 
	
	<script id="tooltipTemplate1" type="text/x-jquery-tmpl">
		<div id="tooltip1" class="ui-widget-content ui-corner-all">
			<span id="tooltipValueSerie">&nbsp&nbsp&nbspFSNWeb</span><br/>
			<span id="tooltipValueTime">Time&nbsp: ${item.Label}</span><br/>
			<span id="tooltipValue">Value: ${item.Value1}</span>          
		</div>
	</script>

	<script id="tooltipTemplate2" type="text/x-jquery-tmpl">
		<div id="tooltip2" class="shadow ui-widget-content ui-corner-all">
			<span id="tooltipValueSerie">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspGS</span><br/>
			<span id="tooltipValueTime">Time&nbsp: ${item.Label}</span><br/>
			<span id="tooltipValue2">Value: ${item.Value2}</span>
		</div>
	</script>  

	<script id="tooltipTemplate3" type="text/x-jquery-tmpl">
		<div id="tooltip3" class="shadow ui-widget-content ui-corner-all">
			<span id="tooltipValueSerie">&nbsp&nbsp&nbspPORTAL</span><br/>
			<span id="tooltipValueTime">Time&nbsp: ${item.Label}</span><br/>
			<span id="tooltipValue3">Value: ${item.Value3}</span>
		</div>
	</script>  

	<script id="tooltipTemplate4" type="text/x-jquery-tmpl">
		<div id="tooltip4" class="shadow ui-widget-content ui-corner-all">
			<span id="tooltipValueSerie">FSNWeb(AVG)</span><br/>
			<span id="tooltipValueTime">Time&nbsp: ${item.Label}</span><br/>
			<span id="tooltipValue4">Value: ${item.Value4}</span>
		</div>
	</script>  

	<script id="tooltipTemplate5" type="text/x-jquery-tmpl">
		<div id="tooltip5" class="shadow ui-widget-content ui-corner-all">
			<span id="tooltipValueSerie">GS(AVG)</span><br/>
			<span id="tooltipValueTime">Time&nbsp: ${item.Label}</span><br/>
			<span id="tooltipValue5">Value: ${item.Value5}</span>
		</div>
	</script>  

	<script id="tooltipTemplate6" type="text/x-jquery-tmpl">
		<div id="tooltip6" class="shadow ui-widget-content ui-corner-all">
			<span id="tooltipValueSerie">PORTAL(AVG)</span><br/>
			<span id="tooltipValueTime">Time&nbsp: ${item.Label}</span><br/>
			<span id="tooltipValue6">Value: ${item.Value6}</span>
		</div>
	</script>  

	<script id="tooltipTemplate7" type="text/x-jquery-tmpl">
		<div id="tooltip7" class="shadow ui-widget-content ui-corner-all">
			<span id="tooltipValueSerie">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspALL</span><br/>
			<span id="tooltipValueTime">Time&nbsp: ${item.Label}</span><br/>
			<span id="tooltipValue7">Value: ${item.Value7}</span>
		</div>
	</script>  

	<script id="tooltipTemplate8" type="text/x-jquery-tmpl">
		<div id="tooltip8" class="shadow ui-widget-content ui-corner-all">
			<span id="tooltipValueSerie">ALL(AVG)</span><br/>
			<span id="tooltipValueTime">Time&nbsp: ${item.Label}</span><br/>
			<span id="tooltipValue8">Value: ${item.Value8}</span>
		</div>
	</script>  

	<script id="tooltipTemplateRecentActivity1" type="text/x-jquery-tmpl">
		<div id="tooltipRecentActivity1" class="shadow ui-widget-content ui-corner-all">
			<span id="tooltipValueSerie">EVENTOS</span><br/>
			<span id="tooltipValueTime">Time&nbsp: ${item.LabelRecentActivity}</span><br/>
			<span id="tooltipValueRecentActivity1">Value: ${item.ValueRecentActivity1}</span>
		</div>
	</script>  

	<script id="tooltipTemplateRecentActivity2" type="text/x-jquery-tmpl">
		<div id="tooltipRecentActivity2" class="shadow ui-widget-content ui-corner-all">
			<span id="tooltipValueSerie">AVG(EVENTOS)</span><br/>
			<span id="tooltipValueTime">Time&nbsp: ${item.LabelRecentActivity}</span><br/>
			<span id="tooltipValueRecentActivity2">Value: ${item.ValueRecentActivity2}</span>
		</div>
	</script>  
	<style>
	.oculto
	{
		display: none !important;
	}
	</style>
	<script id="errorGrid" type="text/javascript">

	    var informacionToolTip = null;

	    function OcultarCargando() {
	        var modalprog = $find(ModalProgress);
	        if (modalprog) modalprog.hide();
	    }

	    //Muestra el panel con el detalle del error
	    function ErrorInfoShow(idError, sender) {
	        //$('td div#errorInfo').remove()
	        if (informacionToolTip != null) {
	            $("#errorInfo").html(informacionToolTip);
	        }

	        $("#errorInfo").removeClass("oculto");
	        $("#errorInfo").css("position", "absolute");
	        $("#errorInfo").css("left", $(sender).position().left);
	        $("#errorInfo").css("top", $(sender).position().top);



	        $('body').append($("#errorInfo"));
	        //debugger;
	        //pongo invisible los spans que estan visibles dentro del div errorInfo

	        //var m = $("#errorInfo");
	        //m.children("span").css("display","none");
	        //pongo visible el span que necesito
	        $('span', $("#errorInfo")).css("display", "none");
	        $('#data' + idError).css("display", "block");
	    }
	    //Cierra el panel de detalle del error
	    function ErrorInfoClose(sender) {
	        $('#errorInfo').addClass('oculto');

	        return false;
	    }
	    //iconos para las distintas excepciones y errores dependiendo del idError
	    function IconoError(idError) {
	        switch (idError) {
	            case 1: return "0007"; break;
	            case 2: return "0009"; break;
	            case 3: return "0008"; break;
	            case 4: return "0010"; break;
	            case 5: return "0011"; break;
	            default: return "0009";
	        }
	    }

	    //grid de Errores
	    function ComprobarGridErrores(fechaInicio, fechaFin, entorno, pyme) {
	        $(function () {
	            var timeoffset = new Date().getTimezoneOffset();
	            var dataError = [];
	            var datosError = [];
	            var to = rutaFS + 'BI/VisorActividad.aspx/ErroresActividad';
	            $.when($.ajax({
	                type: 'POST',
	                url: to,
	                contentType: 'application/json;charset=utf-8',
	                data: JSON.stringify({ "fechaInicio": fechaInicio, "fechaFin": fechaFin, "entorno": entorno, "pyme": pyme, "timeoffset": timeoffset }),
	                dataType: 'json',
	                async: false
	            })).done(function (msg) {
	                datosError = null;
	                datosError = msg.d;
	                if (datosError != null) {
	                    for (var i = 0; i < datosError.length; i++) {
	                        dataTemp = "<span id='data" + datosError[i].id + "' style='display:none; margin-left:5px'>" + datosError[i].data.toString() + "</span>";
	                        $('[id$=errorInfo]').append(dataTemp);
	                        cellTemp = "<img width='15' height='15' src='images/" + IconoError(datosError[i].idError) + ".png'/>";
	                        info = "<div style='cursor:pointer; color:blue' onclick='ErrorInfoShow(" + datosError[i].id + ", this);return false'> View Info</div>";
	                        dataError.push({ IdError: datosError[i].idError, DateTimes: stringToDate(datosError[i].fecAct), Error: datosError[i].exFullName.toString(), Today: datosError[i].rank, Id_error: datosError[i].idError, Info: info, url: cellTemp });
	                    }
	                }


	                $("#gridError").igGrid({
	                    autoGenerateColumns: false,
	                    dataSource: dataError, //JSON Array defined above
	                    features: [{
	                        name: 'Sorting',
	                        type: "local",
	                        columnSorted: fixRowHeight
	                    }],
	                    columns: [
							{ headerText: " ", key: "url", dataType: "string", width: "4%" },
							{ headerText: TextosVisorActividad[49], key: "DateTimes", dataType: "dateTime", formatter: function (val) { return dateToString(val); }, width: "20%" },
							{ headerText: TextosVisorActividad[54], key: "Error", dataType: "string", width: "45%" },
							{ headerText: TextosVisorActividad[55], key: "Today", dataType: "double", formatter: function (val) { return val.toString().replace(".", ","); }, width: "8%" },
							{ headerText: TextosVisorActividad[65], key: "Id_error", dataType: "double", formatter: function (val) { return val.toString().replace(".", ","); }, width: "8%" },
							{ headerText: TextosVisorActividad[53], key: "Info", dataType: "string", width: "15%" },
						],
	                    dataRendered: function (evt, ui) {
	                        ui.owner.element.find("tr td:nth-child(4)").css("text-align", "right");
	                        ui.owner.element.find("tr td:nth-child(5)").css("text-align", "right");
	                        ui.owner.element.find("tr td").css("height", "100%");
	                        ui.owner.element.find("tr td").css("word-wrap", "break-word");
	                    }
	                });
	            });
	        });
	    }
	</script>

	<style type="text/css">
		.tooltip-options
		{
			padding: 10px;
			background-color: #EBEBEB;
			width:100px;
		}
		.tooltip-grid-descr
		{
			overflow: hidden;
			height: 135px;
			width:100px;
		}
		
		#gridEvent_tooltips_container
		{
			min-height: auto;
		}
		
	 </style>

	 <script id="eventGrid" type="text/javascript">
	     //grid de eventos
	     function ComprobarGridEventos(fechaInicio, fechaFin, entorno, pyme, tipoEvento, rango) {
	         $(function () {
	             var dataEventos = [];
	             var datosEventos = [];
	             var timeoffset = new Date().getTimezoneOffset();
	             var to = rutaFS + 'BI/VisorActividad.aspx/EventosActividad';
	             $.when($.ajax({
	                 type: 'POST',
	                 url: to,
	                 contentType: 'application/json;charset=utf-8',
	                 data: JSON.stringify({ "fechaInicio": fechaInicio, "fechaFin": fechaFin, "entorno": entorno, "pyme": pyme, "tipoEvento": tipoEvento, "rango": rango, "timeoffset": timeoffset }),
	                 dataType: 'json',
	                 async: false
	             })).done(function (msg) {
	                 datosEventos = null;
	                 datosEventos = msg.d;
	                 if (datosEventos != null) {
	                     for (var i = 0; i < datosEventos.length; i++) {
	                         dataEventos.push({ DateTimes: stringToDate(datosEventos[i].fechaEvento), Event: datosEventos[i].tipoEvento, NumEvento: parseFloat(datosEventos[i].numEvento.replace(",", ".")), Media: parseFloat(datosEventos[i].mediaEvento.replace(",", ".")), Info: datosEventos[i].data });
	                     }
	                 }


	                 $("#gridEvent").igGrid({
	                     autoGenerateColumns: false,
	                     dataSource: dataEventos, //JSON Array defined above 
	                     features: [
						 {
						     name: "Tooltips",
						     columnSettings: [
									{ columnKey: "Info", allowTooltips: true }
								],
						     tooltipShowing: function (e, args) {
						         var message =
									"<div class='event-args'>" +
										"<span class='tooltip-italic'>" + args.value + "</span></div>";
						         showEvent(message);
						     }
						 },
						 {
						     name: 'Sorting',
						     type: "local",
						     columnSorted: fixRowHeight
						 }],
	                     columns: [
								{ headerText: TextosVisorActividad[49], key: "DateTimes", dataType: "dateTime", formatter: function (val) { return dateToString(val); }, width: "20%" },
								{ headerText: TextosVisorActividad[50], key: "Event", dataType: "string", width: "30%" },
								{ headerText: TextosVisorActividad[51], key: "NumEvento", dataType: "double", formatter: function (val) { return val.toString().replace(".", ","); }, width: "10%" },
	                     /**** Se oculta la columna de la media en el grid. En caso de volver a mostrarla, hay que ajustar de nuevo el ancho de las columnas. ****/
	                     /**** Se comenta el codigo anterior por si en un futuro se quiere recuperar el calculo y muestra de medias. ****/
	                     //{ headerText: TextosVisorActividad[52], key: "Media", dataType: "double", formatter: function (val) { return val.toString().replace(".", ","); }, width: "14%" },
                                {headerText: TextosVisorActividad[52], key: "Media", dataType: "double", formatter: function (val) { return val.toString().replace(".", ","); }, width: "0%", hidden: true },
	                     /**** Fin ocultacion de medias. ****/
								{headerText: TextosVisorActividad[53], key: "Info", dataType: "string", width: "40%" },
							],
	                     dataRendered: function (evt, ui) {
	                         ui.owner.element.find("tr td:nth-child(3)").css("text-align", "right");
	                         ui.owner.element.find("tr td:nth-child(4)").css("text-align", "right");
	                         ui.owner.element.find("tr td").css("height", "100%");
	                         ui.owner.element.find("tr td").css("word-wrap", "break-word");
	                     }

	                 });
	             });
	         });
	     }

	     function showEvent(message) {
	         $("#eventsInfo").empty();
	         $("#eventsInfo").html($("#eventsInfo").html() + message).
							prop({ scrollTop: $("#eventsInfo").prop("scrollHeight") });
	     }

	     function fixRowHeight(evt, ui) {
	         ui.owner.element.find("tr td").css("height", "100%");
	         ui.owner.element.find("tr td").css("word-wrap", "break-word");
	     }

	     //Funcion para convertir a tipo fecha una cadena proveniente de la BBDD
	     function stringToDate(sFecha) {
	         var sFechaArr = new Array();
	         var fecha;
	         var fechaArr = new Array();
	         var anyo;
	         var mes;
	         var dia;
	         var tiempo;
	         var horaArr = new Array();
	         var horas;
	         var minutos;
	         var segundos;

	         sFechaArr = sFecha.split(' ');
	         fecha = sFechaArr[0];
	         tiempo = sFechaArr[1];

	         //Recogemos el año, mes, dia dependiendo del formato de fecha del usuario
	         switch (UsuMask) {
	             case 'dd' + UsuMaskSeparator + 'MM' + UsuMaskSeparator + 'yyyy':
	                 fechaArr = fecha.split(UsuMaskSeparator);
	                 anyo = fechaArr[2];
	                 mes = fechaArr[1] - 1; // Cuidado, el indice de meses en JS comienza en 0!
	                 dia = fechaArr[0];
	                 break;
	             case 'MM' + UsuMaskSeparator + 'dd' + UsuMaskSeparator + 'yyyy':
	                 fechaArr = fecha.split(UsuMaskSeparator);
	                 anyo = fechaArr[2];
	                 mes = fechaArr[0] - 1; // Cuidado, el indice de meses en JS comienza en 0!
	                 dia = fechaArr[1];
	                 break;
	             case 'yyyy' + UsuMaskSeparator + 'MM' + UsuMaskSeparator + 'dd':
	                 fechaArr = fecha.split(UsuMaskSeparator);
	                 anyo = fechaArr[0];
	                 mes = fechaArr[1] - 1; // Cuidado, el indice de meses en JS comienza en 0!
	                 dia = fechaArr[2];
	                 break;
	             default:
	                 fechaArr = fecha.split(UsuMaskSeparator);
	                 anyo = fechaArr[2];
	                 mes = fechaArr[1] - 1; // Cuidado, el indice de meses en JS comienza en 0!
	                 dia = fechaArr[0];
	         }

	         if (sFechaArr.length < 2) {
	             horas = 0;
	             minutos = 0;
	             segundos = 0;
	         }
	         else {
	             horaArr = tiempo.split(':');
	             horas = horaArr[0];
	             minutos = horaArr[1];
	             segundos = horaArr[2];
	         }

	         return new Date(anyo, mes, dia, horas, minutos, segundos, 0);
	     }

	     function addZero(x, n) {
	         while (x.toString().length < n) {
	             x = '0' + x;
	         }
	         return x;
	     }

	     //Funcion para convertir una fecha a una cadena con el formato del usuario
	     function dateToString(dFecha) {
	         var yyyy, MM, dd, HH, mm, ss, ms;
	         var sFechaFormateada;

	         yyyy = dFecha.getFullYear();
	         MM = addZero(dFecha.getMonth() + 1, 2); // Cuidado, el indice de meses en JS comienza en 0!
	         dd = addZero(dFecha.getDate(), 2);
	         HH = addZero(dFecha.getHours(), 2);
	         mm = addZero(dFecha.getMinutes(), 2);
	         ss = addZero(dFecha.getSeconds(), 2);

	         //Recogemos el año, mes, dia dependiendo del formato de fecha del usuario
	         switch (UsuMask) {
	             case 'dd' + UsuMaskSeparator + 'MM' + UsuMaskSeparator + 'yyyy':
	                 sFechaFormateada = dd + UsuMaskSeparator + MM + UsuMaskSeparator + yyyy;
	                 break;
	             case 'MM' + UsuMaskSeparator + 'dd' + UsuMaskSeparator + 'yyyy':
	                 sFechaFormateada = MM + UsuMaskSeparator + dd + UsuMaskSeparator + yyyy;
	                 break;
	             case 'yyyy' + UsuMaskSeparator + 'MM' + UsuMaskSeparator + 'dd':
	                 sFechaFormateada = yyyy + UsuMaskSeparator + MM + UsuMaskSeparator + dd;
	                 break;
	             default:
	                 sFechaFormateada = dd + UsuMaskSeparator + MM + UsuMaskSeparator + yyyy;
	         }

	         return sFechaFormateada + ' ' + HH + ':' + mm + ':' + ss;
	     }

	</script>
	
	 <script id="Pymes" type="text/javascript">
	     //grid de eventos
	     function CargarComboPymes(entorno) {
	         $(function () {
	             var datosPymes = [];
	             var to = rutaFS + 'BI/VisorActividad.aspx/CargarPymes';
	             $.when($.ajax({
	                 type: 'POST',
	                 url: to,
	                 contentType: 'application/json;charset=utf-8',
	                 data: JSON.stringify({ "entorno": entorno }),
	                 dataType: 'json',
	                 async: false
	             })).done(function (msg) {
	                 datosPymes = null;
	                 datosPymes = msg.d;
	                 var html = "";
	                 if (datosPymes != null) {
	                     for (var i = 0; i < datosPymes.length; i++) {
	                         html += "<option value=\"";
	                         html += datosPymes[i].Cod + "\">";
	                         html += datosPymes[i].Denominacion + "</option>";
	                     }
	                     $("#ddlEnviromentPymes").empty().append(html);
	                 }

	             });
	         });
	     }
	</script>
	
	<script type="text/javascript">
	    $(function () {
	        // combo de eleccion del tipo de evento de la grafica actividad reciente
	        $('[id$=ddlTipoEvento]').change(function () {
	            var tipoEvento = $('[id$=ddlTipoEvento]').val();
	            ComprobarUsuariosActividadReciente(fi, ff, ent, ran, tipoEvento, pym);
	            ComprobarGridEventos(fi, ff, ent, pym, tipoEvento, ran);
	            $('[id$=ddlTipoEventoGrid]').val($('[id$=ddlTipoEvento]').val());
	            if ($('[id$=ddlTipoEventoGrid]').val() == "452") {
	                $("#gridEvent_tooltips_container").css("width", "600");
	            }
	        });
	        // combo de eleccion del tipo de evento del grid de eventos
	        $('[id$=ddlTipoEventoGrid]').change(function () {
	            var tipoEvento = $('[id$=ddlTipoEventoGrid]').val();
	            if (tipoEvento == TextosVisorActividad[34]) {
	                tipoEvento = ""
	                ComprobarGridEventos(fi, ff, ent, pym, tipoEvento, ran);
	            } else {
	                ComprobarUsuariosActividadReciente(fi, ff, ent, ran, tipoEvento, pym);
	                ComprobarGridEventos(fi, ff, ent, pym, tipoEvento, ran);
	                $('[id$=ddlTipoEvento]').val($('[id$=ddlTipoEventoGrid]').val());
	            }
	            if ($('[id$=ddlTipoEventoGrid]').val() == "452") {
	                $('[id$=gridEvent_tooltips_container]').css("width", "600px");
	            }
	        });
	    });

		
	</script>
	<script type="text/javascript">
	    function CrearGraficaActividadReciente(valor) {
	        $(function () {
	            var currData, currDataSource, doGeneration;
	            doGeneration2 = function () {
	                if (datos2 != null && datos2 != null) {
	                    $('[id$=chart2]').css("display", "block");
	                    $('[id$=gridRecentActivity]').css("display", "block");
	                    $('[id$=divNoData2]').remove();
	                    data = [];
	                    if (valor == 1) { datos2.sort(OrdenarPorHoraActividad); }
	                    for (var i = 0; i < datos2.length; i++) {
	                        if (datos2[i].numUsuariosActividad == -2) {
	                            datos2[i].numUsuariosActividad = null;
	                        }
	                        if (datos2[i].avgUsuariosActividad == -2) {
	                            datos2[i].avgUsuariosActividad = null;
	                        }
	                        data[i] = { LabelRecentActivity: datos2[i].horaAccesoActividad.toString(), ValueRecentActivity1: datos2[i].numUsuariosActividad, ValueRecentActivity2: datos2[i].avgUsuariosActividad };
	                    }
	                    currDataSource = null;
	                    currData = null;
	                    currData = data;
	                    currDataSource = new $.ig.DataSource({ dataSource: null });
	                    currDataSource = new $.ig.DataSource({ dataSource: currData });
	                }
	            }
	            doGeneration2();
	            $("#chart2").igDataChart({
	                width: "77%",
	                height: "250px",
	                leftMargin: 10,
	                topMargin: 10,
	                dataSource: currDataSource,
	                axes: [{
	                    name: "xAxis",
	                    type: "categoryX",
	                    label: "LabelRecentActivity",
	                    interval: 1,
	                    labelAngle: 65,
	                    labelExtent: 30
	                },
					{
					    name: "yAxis",
					    type: "numericY",
					    minimumValue: 0
					}],
	                series: [{
	                    name: "seriesRecentActivity",
	                    type: "line",
	                    xAxis: "xAxis",
	                    yAxis: "yAxis",
	                    valueMemberPath: "ValueRecentActivity1",
	                    showTooltip: true,
	                    tooltipTemplate: "tooltipTemplateRecentActivity1",
	                    thickness: 3,
	                    horizontalZoomable: true,
	                    verticalZoomable: true
	                    /**** Se oculta la representacion de las medias en el grafico. ****/
	                    /**** Se comenta el codigo anterior por si en un futuro se quiere recuperar el calculo y muestra de medias. ****/
	                    //					}, {
	                    //						name: "seriesRecentActivity2",
	                    //						type: "line",
	                    //						xAxis: "xAxis",
	                    //						yAxis: "yAxis",
	                    //						valueMemberPath: "ValueRecentActivity2",
	                    //						showTooltip: true,
	                    //						tooltipTemplate: "tooltipTemplateRecentActivity2",
	                    //						thickness: 1,
	                    //						horizontalZoomable: true,
	                    //						verticalZoomable: true
	                    /**** Fin ocultacion de medias. ****/
	                }],

	                horizontalZoomable: true,
	                verticalZoomable: true,
	                windowResponse: "immediate"
	            });
	        });

	        $(function () {
	            var avgActividad = 0;
	            var maxActividad = 0;
	            var minActividad = 1000;
	            var totalActividad = 0;

	            if (datos2 != null) {
	                for (var i = 0; i < datos2.length; i++) {
	                    /**** Se cambia la logica de calculo de la media. Hay que comentar/borrar esta linea en caso de querer volver a la anterior logica. ****/
	                    /**** Se comenta el codigo anterior por si en un futuro se quiere recuperar el calculo y muestra de medias. ****/
	                    var sinNuloNumUsuariosActividad = (datos2[i].numUsuariosActividad == null ? 0 : datos2[i].numUsuariosActividad);
	                    avgActividad += sinNuloNumUsuariosActividad;
	                    /**** Fin ocultacion de medias. ****/
	                    totalActividad += sinNuloNumUsuariosActividad;
	                    if (sinNuloNumUsuariosActividad > maxActividad) {
	                        maxActividad = sinNuloNumUsuariosActividad;
	                    }
	                    if (sinNuloNumUsuariosActividad < minActividad) {
	                        minActividad = sinNuloNumUsuariosActividad;
	                    }
	                    /**** Se cambia la logica de calculo de la media. Hay que descomentar esta linea en caso de querer volver a la anterior logica. ****/
	                    /**** Se comenta el codigo anterior por si en un futuro se quiere recuperar el calculo y muestra de medias. ****/
	                    //						avgActividad = datos2[i].avgUsuariosActividad;
	                    /**** Fin ocultacion de medias. ****/
	                }
	                /**** Se cambia la logica de calculo de la media. Hay que comentar/borrar esta linea en caso de querer volver a la anterior logica. ****/
	                /**** Se comenta el codigo anterior por si en un futuro se quiere recuperar el calculo y muestra de medias. ****/
	                avgActividad = Math.floor(avgActividad / datos2.length * 100) / 100;
	                /**** Fin ocultacion de medias. ****/
	                $('#gridRecentActivity').empty();
	                $('#gridRecentActivity').append('<table id="tableRecentActivity">');
	                $('#gridRecentActivity').append('<tbody><tr>');
	                $('#gridRecentActivity').append('<td style="font-size:10px" class="tabla-con-border-fondo-oscuro" align="center">&nbspTOTAL&nbsp</td> <td style="font-size:8px" class="tabla-con-border" align="right">&nbsp' + totalActividad + '&nbsp</td> </tr>');
	                $('#gridRecentActivity').append('<tr>');
	                $('#gridRecentActivity').append('<td style="font-size:10px" class="tabla-con-border-fondo-oscuro-abajo" align="center">&nbspAVG&nbsp</td> <td style="font-size:8px" class="tabla-con-border-abajo" align="right">&nbsp' + Math.floor((avgActividad) * 100) / 100 + '&nbsp</td> </tr></tbody>');
	                $('#gridRecentActivity').append('</table>');
	            } else {
	                $('[id$=chart2]').css("display", "none");
	                $('[id$=panelMedioDcha]').append("<div id='divNoData2' style='height:100px; margin-top:120px; margin-left:150px'><span id='lblNoData2'></span></div>");
	                $('[id$=lblNoData2]').text(TextosVisorActividad[33]);
	                $('[id$=gridRecentActivity]').css("display", "none");
	            }
	            /************* DSL Aumentar margen respecto al valor máximo ******************************/
	            $("#chart2").igDataChart("option", "axes", [{ name: "yAxis", type: "numericY", maximumValue: maxActividad + 1}]);
	            /*****************************************************************************************/
	        });
	    }

	    function CrearGraficaActividadRecienteSinMedias(valor, rango) {
	        $(function () {
	            var currData, currDataSource, doGeneration;
	            doGeneration2 = function () {
	                if (datos2 != null && datos2 != null) {
	                    $('[id$=chart2]').css("display", "block");

	                    /************** DSL: Ocultar medias **************************/
	                    //$('[id$=gridRecentActivity]').css("display", "block");
	                    $('[id$=gridRecentActivity]').css("display", "none");
	                    /************** DSL: Ocultar medias **************************/

	                    $('[id$=divNoData2]').remove();
	                    data = [];
	                    if (valor == 1) { datos2.sort(OrdenarPorHoraActividad); }
	                    //                        if (rango == "ANYO") { datos2.sort(OrdenarMesesAnyo); }
	                    for (var i = 0; i < datos2.length; i++) {
	                        if (datos2[i].numUsuariosActividad == -2) {
	                            datos2[i].numUsuariosActividad = null;
	                        }
	                        data[i] = { LabelRecentActivity: datos2[i].horaAccesoActividad, ValueRecentActivity1: datos2[i].numUsuariosActividad };
	                    }
	                    currDataSource = null;
	                    currData = null;
	                    currData = data;
	                    currDataSource = new $.ig.DataSource({ dataSource: null });
	                    currDataSource = new $.ig.DataSource({ dataSource: currData });
	                }
	            }
	            doGeneration2();
	            $("#chart2").igDataChart({
	                width: "77%",
	                leftMargin: 10,
	                topMargin: 10,
	                height: "250px",
	                dataSource: currDataSource,
	                axes: [{
	                    name: "xAxis",
	                    type: "categoryX",
	                    label: "LabelRecentActivity",
	                    interval: 1,
	                    labelAngle: 65,
	                    labelExtent: 30
	                },
					{
					    name: "yAxis",
					    type: "numericY",
					    minimumValue: 0
					}],
	                series: [{
	                    name: "seriesRecentActivity",
	                    type: "line",
	                    xAxis: "xAxis",
	                    yAxis: "yAxis",
	                    valueMemberPath: "ValueRecentActivity1",
	                    showTooltip: true,
	                    tooltipTemplate: "tooltipTemplateRecentActivity1",
	                    thickness: 3,
	                    horizontalZoomable: true,
	                    verticalZoomable: true
	                }],

	                horizontalZoomable: true,
	                verticalZoomable: true,
	                windowResponse: "immediate"
	            });
	        });

	        $(function () {
	            var avgActividad = 0;
	            //                var avgGS = 0;
	            //                var avgPortal = 0;
	            var maxActividad = 0;
	            //                var maxGS = 0;
	            //                var maxPortal = 0;
	            var minActividad = 1000;
	            //                var minGS = 1000;
	            //                var minPortal = 1000;
	            var totalActividad = 0;
	            if (datos2 != null) {
	                for (var i = 0; i < datos2.length; i++) {

	                    var sinNuloNumUsuariosActividad = (datos2[i].numUsuariosActividad == null ? 0 : datos2[i].numUsuariosActividad);
	                    var sinNuloAvgUsuariosActividad = (datos2[i].avgUsuariosActividad == null ? 0 : datos2[i].avgUsuariosActividad);

	                    avgActividad += sinNuloNumUsuariosActividad;
	                    //                        avgGS += datos2[i].numUsuariosGS;
	                    //                        avgPortal += datos2[i].numUsuariosPortal;
	                    totalActividad += sinNuloNumUsuariosActividad;

	                    if (sinNuloNumUsuariosActividad > maxActividad) {
	                        maxActividad = sinNuloNumUsuariosActividad;
	                    }
	                    //                        if (datos2[i].numUsuariosGS > maxGS) {
	                    //                            maxGS = datos2[i].numUsuariosGS;
	                    //                        }
	                    //                        if (datos2[i].numUsuariosPortal > maxPortal) {
	                    //                            maxPortal = datos2[i].numUsuariosPortal;
	                    //                        }

	                    if (sinNuloNumUsuariosActividad < minActividad) {
	                        minActividad = sinNuloNumUsuariosActividad;
	                    }
	                    //                        if (datos2[i].numUsuariosGS < minGS) {
	                    //                            minGS = datos2[i].numUsuariosGS;
	                    //                        }
	                    //                        if (datos2[i].numUsuariosPortal < minPortal) {
	                    //                            minPortal = datos2[i].numUsuariosPortal;
	                    //                        }
	                    avgActividad = sinNuloAvgUsuariosActividad;
	                }

	                //                    avgGS = Math.floor(avgGS / datos2.length * 100) / 100;
	                //                    avgPortal = Math.floor(avgPortal / datos2.length * 100) / 100;
	                $('#gridRecentActivity').empty();
	                $('#gridRecentActivity').append('<table id="tableRecentActivity">');
	                $('#gridRecentActivity').append('<tbody><tr>');
	                $('#gridRecentActivity').append('<td style="font-size:10px" class="tabla-con-border-fondo-oscuro" align="center">&nbspTOTAL&nbsp</td> <td style="font-size:8px" class="tabla-con-border" align="right">&nbsp' + totalActividad + '&nbsp</td> </tr>');
	                $('#gridRecentActivity').append('<tr>');
	                $('#gridRecentActivity').append('<td style="font-size:10px" class="tabla-con-border-fondo-oscuro-abajo" align="center">&nbspAVG&nbsp</td> <td style="font-size:8px" class="tabla-con-border-abajo" align="right">&nbsp' + Math.floor((avgActividad) * 100) / 100 + '&nbsp</td> </tr></tbody>');
	                $('#gridRecentActivity').append('</table>');
	            } else {
	                $('[id$=chart2]').css("display", "none");
	                $('[id$=panelMedioDcha]').append("<div id='divNoData2' style='height:100px; margin-top:120px; margin-left:150px'><span id='lblNoData2'></span></div>");
	                $('[id$=lblNoData2]').text(TextosVisorActividad[33]);
	                $('[id$=gridRecentActivity]').css("display", "none");
	            }
	            /************* DSL Aumentar margen respecto al valor máximo ******************************/
	            $("#chart2").igDataChart("option", "axes", [{ name: "yAxis", type: "numericY", maximumValue: maxActividad + 1}]);
	            /*****************************************************************************************/
	        });
	    }
	</script>

	<script type="text/javascript">
	    //var datos;
	    var data = [];
	    var fi;
	    var ff;
	    var ran;
	    var ent;
	    var pym;

	    //Comprueba los usuarios que se han conectado a las distintas aplicaciones en un rango de tiempo y devuelve los datos para crear la gráfica
	    function ComprobarUsuariosConectadosRango(fechaInicio, fechaFin, rango, entorno, pyme, valor) {
	        var timeoffset = new Date().getTimezoneOffset();
	        var to = rutaFS + 'BI/VisorActividad.aspx/UsuariosConectadosPorRango';

	        $.when($.ajax({
	            type: 'POST',
	            url: to,
	            data: JSON.stringify({ fechaInicio: fechaInicio, fechaFin: fechaFin, rango: rango, entorno: entorno, pyme: pyme, timeoffset: timeoffset }),
	            contentType: 'application/json;charset=utf-8',
	            dataType: 'json',
	            async: false
	        })).done(function (msg) {
	            datos = null;
	            datos = msg.d;
	            if (rango == "HORA-NOW") {
	                if ($('#chkAverages').prop('checked')) {
	                    CrearGraficaUsuariosEnLinea("NOW", valor, timeoffset)
	                } else {
	                    CrearGraficaUsuariosEnLineaSinMedias("NOW", valor, timeoffset)
	                }
	            } else {
	                if ($('#chkAverages').prop('checked')) {
	                    CrearGraficaUsuariosEnLinea("", valor, timeoffset)
	                } else {
	                    CrearGraficaUsuariosEnLineaSinMedias("", valor, timeoffset)
	                }
	            }
	        });
	    }

	    function ComprobarUsuariosActividadReciente(fechaInicio, fechaFin, entorno, rango, tipoEvento, pyme, valor) {
	        var to = rutaFS + 'BI/VisorActividad.aspx/UsuariosConectadosPorRangoRecentActivity';
	        fi = fechaInicio;
	        ff = fechaFin;
	        ran = rango;
	        ent = entorno;
	        pym = pyme;
	        var timeoffset = new Date().getTimezoneOffset();
	        $.when($.ajax({
	            type: 'POST',
	            url: to,
	            data: JSON.stringify({ fechaInicio: fechaInicio, fechaFin: fechaFin, entorno: entorno, rango: rango, tipoEvento: tipoEvento, pyme: pyme, timeoffset: timeoffset }),
	            contentType: 'application/json;charset=utf-8',
	            dataType: 'json',
	            async: false
	        })).done(function (msg) {
	            //datos2 = null;
	            //datos2 = msg.d;
	            datos2 = [];
	            $(msg.d).each(function (index, item) {
	                datos2.push(msg.d[index]);
	            });
	            //************* DSL: Expediente Pruebas 319009/2015/285 ******************************//
	            //for (var j = 1; j < datos2.length; j++) {
	            //datos2[j].numUsuariosActividad = datos2[j - 1].numUsuariosActividad + datos2[j].numUsuariosActividad;
	            //}
	            //************* DSL: Expediente Pruebas 319009/2015/285 ******************************//

	            if ($('#chkAveragesRecentActivity').prop('checked')) {
	                CrearGraficaActividadReciente(valor);
	            } else {
	                CrearGraficaActividadRecienteSinMedias(valor, ran);
	            }

	        });
	    }
	</script>

	<style type="text/css">
		#chart
		{
			position: relative;
			float: left;
			margin-right: 10px;
		}        
		.selectionOptions
		{
			margin-bottom: 10px;   
		}
	</style>

	<script type="text/javascript">
	    //declarar variables para que podamos acceder a ellas en cualquier momento
	    var fecDesde, fecHasta, Rango, Texto;

	    $(function () {
	        $("#tabs").tabs();
	        $("#dpFechaIni").datepicker();
	        $("#dpFechaFin").datepicker();
	        //Seleccion del dia en el datepicker (Today) y creacion de la gráfica respecto a ese día.
	        $("#dpSelectDay").datepicker({
	            onSelect: function (dateText) {
	                OcultarBotonesSeleccion();
	                OcultarTextosBotonesSeleccion();
	                //$('[id$=lblHoy]').text(FormatoFecha(dateText));
	                $('[id$=lblHoy]').text(dateText);
	                $('[id$=lblHoy]').css("font-size", "10px");
	                var fechaInicio = $(this).datepicker('getDate');
	                var fechaFin = $(this).datepicker('getDate');
	                fechaInicio.setHours(0, 0, 0, 0);
	                fechaFin.setHours(23, 59, 59, 999);
	                $('[id$=divCargando]').css("display", "block").css("width", "100px").css("margin", "18% auto");
	                $('[id$=divTabContainer]').css("display", "none");
	                setTimeout(function () { SelectDay(dateText, fechaInicio, fechaFin); }, 200);
	            }
	        });
	    });
	    function SelectDay(dateText, fechaInicio, fechaFin) {

	        if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
	            var entorno = $('[id$=lblCodEntorno]').text();
	        } else {
	            var entorno = $('[id$=ddlEnviroment]').val();
	        }
	        var tipoEvento = $('[id$=ddlTipoEvento]').val();
	        var tipoEvento2 = $('[id$=ddlTipoEventoGrid]').val();
	        if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
	            var pyme = $('[id$=ddlEnviromentPymes]').val();
	        } else {
	            var pyme = null;
	        }
	        ComprobarUsuariosConectadosRango(fechaInicio, fechaFin, "DIA", entorno, pyme, 1);
	        ComprobarUsuariosActividadReciente(fechaInicio, fechaFin, entorno, "DIA", tipoEvento, pyme, 1);
	        ComprobarGridEventos(fechaInicio, fechaFin, entorno, pyme, tipoEvento2, "DIA");
	        ComprobarGridErrores(fechaInicio, fechaFin, entorno, pyme);

	        $("#chart").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	        $("#chart2").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	        $('[id$=divHoy]').css("background-color", "#D3D3D3");
	        $('[id$=divAhora]').css("background-color", "white");
	        $('[id$=divEstaSemana]').css("background-color", "white");
	        $('[id$=divEsteMes]').css("background-color", "white");
	        $('[id$=divEsteAnyo]').css("background-color", "white");
	        fecDesde = fechaInicio;
	        fecHasta = fechaFin;
	        Rango = "DIA";
	        Texto = dateText;
	        $('[id$=divCargando]').css("display", "none");
	        $('[id$=divTabContainer]').css("display", "block");
	    }

	    //Evento que se produce cuando se selecciona un dia del combo del mes+anyo
	    $(function () {
	        var mes = -1;
	        var anyo = (new Date).getFullYear();
	        var months = [TextosVisorActividad[0], TextosVisorActividad[1], TextosVisorActividad[2], TextosVisorActividad[3], TextosVisorActividad[4], TextosVisorActividad[5], TextosVisorActividad[6], TextosVisorActividad[7], TextosVisorActividad[8], TextosVisorActividad[9], TextosVisorActividad[10], TextosVisorActividad[11]];
	        $('#ddlSelectMonth').change(function () {
	            mes = parseInt($("#ddlSelectMonth").val());
	            anyo = parseInt($("#ddlSelectYear").val()) + 2003;
	            if (mes != -1) {
	                $('[id$=divCargando]').css("display", "block").css("width", "100px").css("margin", "18% auto");
	                $('[id$=divTabContainer]').css("display", "none");
	                setTimeout(function () { SelectMonth(mes, months, anyo); }, 200);
	            }
	        });
	        function SelectMonth(mes, months, anyo) {
	            var fechaIni = new Date(parseInt(anyo), mes, parseInt("01"));
	            fechaIni.setHours(0, 0, 0, 0);
	            var fechaFin = new Date(parseInt(anyo), mes + 1, parseInt("01"));
	            fechaFin.setDate(fechaFin.getDate() - 1);
	            fechaFin.setHours(23, 59, 59, 999);
	            OcultarBotonesSeleccion();
	            OcultarTextosBotonesSeleccion();
	            if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
	                var entorno = $('[id$=lblCodEntorno]').text();
	            } else {
	                var entorno = $('[id$=ddlEnviroment]').val();
	            }
	            if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
	                var pyme = $('[id$=ddlEnviromentPymes]').val();
	            } else {
	                var pyme = null;
	            }
	            ComprobarUsuariosConectadosRango(fechaIni, fechaFin, "MES", entorno, pyme, 0);
	            var tipoEvento = $('[id$=ddlTipoEvento]').val();
	            var tipoEvento2 = $('[id$=ddlTipoEventoGrid]').val();
	            ComprobarUsuariosActividadReciente(fechaIni, fechaFin, entorno, "MES", tipoEvento, pyme, 0);
	            if (anyo.length == 1) {
	                anyo = "190" + anyo;
	            }
	            $('[id$=lblEsteMes]').text(months[mes] + "-" + anyo);
	            $('[id$=lblEsteMes]').css("font-size", "10px");
	            ComprobarGridEventos(fechaIni, fechaFin, entorno, pyme, tipoEvento2, "MES");
	            ComprobarGridErrores(fechaIni, fechaFin, entorno, pyme);
	            $("#chart").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	            $("#chart2").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	            $('[id$=divHoy]').css("background-color", "white");
	            $('[id$=divAhora]').css("background-color", "white");
	            $('[id$=divEstaSemana]').css("background-color", "white");
	            $('[id$=divEsteMes]').css("background-color", "#D3D3D3");
	            $('[id$=divEsteAnyo]').css("background-color", "white");
	            $('[id$=ddlSelectMonth]').val('-1');
	            fecDesde = fechaIni;
	            fecHasta = fechaFin;
	            Rango = "MES";
	            Texto = months[mes] + "-" + anyo;

	            $('[id$=divCargando]').css("display", "none");
	            $('[id$=divTabContainer]').css("display", "block");
	        }
	        $('#ddlSelectYear').change(function () {
	            anyo = parseInt(this.value) + 2003;
	        });

	        //Evento que se produce cuando se selecciona un anyo del combo de Este Anyo
	        $('#ddlSelectYearAnyo').change(function () {
	            if (anyo != 0) {
	                anyo = parseInt(this.value) + 2003;
	                $('[id$=divCargando]').css("display", "block").css("width", "100px").css("margin", "18% auto");
	                $('[id$=divTabContainer]').css("display", "none");
	                setTimeout(function () { SelectYearAnyo(anyo); }, 200);
	            }
	        });

	        function SelectYearAnyo(anyo) {
	            var fechaIni = new Date(parseInt(anyo), 00, 01);
	            fechaIni.setHours(0, 0, 0, 0);
	            var fechaFin = new Date(parseInt(anyo), 11, 31);
	            fechaFin.setHours(23, 59, 59, 999);
	            OcultarBotonesSeleccion();
	            OcultarTextosBotonesSeleccion();
	            if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
	                var entorno = $('[id$=lblCodEntorno]').text();
	            } else {
	                var entorno = $('[id$=ddlEnviroment]').val();
	            }
	            if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
	                var pyme = $('[id$=ddlEnviromentPymes]').val();
	            } else {
	                var pyme = null;
	            }
	            ComprobarUsuariosConectadosRango(fechaIni, fechaFin, "ANYO", entorno, pyme, 0);
	            var tipoEvento = $('[id$=ddlTipoEvento]').val();
	            var tipoEvento2 = $('[id$=ddlTipoEventoGrid]').val();
	            ComprobarUsuariosActividadReciente(fechaIni, fechaFin, entorno, "ANYO", tipoEvento, pyme, 0);
	            $('[id$=lblEsteAnyo]').text(anyo);
	            $('[id$=lblEsteAnyo]').css("font-size", "10px");
	            anyo = null;
	            ComprobarGridEventos(fechaIni, fechaFin, entorno, pyme, tipoEvento2, "ANYO");
	            ComprobarGridErrores(fechaIni, fechaFin, entorno, pyme);
	            $("#chart").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	            $("#chart2").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	            //marcar como seleccionado
	            $('[id$=divHoy]').css("background-color", "white");
	            $('[id$=divAhora]').css("background-color", "white");
	            $('[id$=divEstaSemana]').css("background-color", "white");
	            $('[id$=divEsteMes]').css("background-color", "white");
	            $('[id$=divEsteAnyo]').css("background-color", "#D3D3D3");
	            $('[id$=ddlSelectYearAnyo]').val('-1');
	            fecDesde = fechaIni;
	            fecHasta = fechaFin;
	            Rango = "ANYO";
	            Texto = anyo;

	            $('[id$=divCargando]').css("display", "none");
	            $('[id$=divTabContainer]').css("display", "block");
	        }
	    });

	    $(function () {
	        $('[id$=ddlEnviroment]').change(function () {
	            if ($('[id$=ddlEnviroment]').val() == "PYME")
	            //if ($('[id$=ddlEnviroment] option:selected').text() == "Pymes") 
	            {
	                if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
	                    var entorno = $('[id$=lblCodEntorno]').text();
	                } else {
	                    var entorno = $('[id$=ddlEnviroment]').val();
	                }
	                //                  var entorno = $('[id$=ddlEnviroment] option:selected').text();
	                CargarComboPymes(entorno);

	                $('[id$=lblEnviromentPymes]').css("display", "block");
	                $('[id$=ddlEnviromentPymes]').css("display", "block");
	            } else {
	                $('[id$=lblEnviromentPymes]').css("display", "none");
	                $('[id$=ddlEnviromentPymes]').css("display", "none");
	            }
	        });
	    });

	    esNow = "";
	    esNowRecentActivity = "";

	    function OrdenarPorHoraAcceso(elementoA, elementoB) {
	        var horaA = elementoA.horaAcceso.toLowerCase();
	        var horaB = elementoB.horaAcceso.toLowerCase();
	        if (horaA.length <= 4) {
	            horaA = "0" + horaA;
	        }
	        if (horaB.length <= 4) {
	            horaB = "0" + horaB;
	        }
	        return ((horaA < horaB) ? -1 : ((horaA > horaB) ? 1 : 0));
	    }

	    function OrdenarPorHoraActividad(elementoA, elementoB) {
	        var horaA = elementoA.horaAccesoActividad.toLowerCase();
	        var horaB = elementoB.horaAccesoActividad.toLowerCase();
	        if (horaA.length <= 4) {
	            horaA = "0" + horaA;
	        }
	        if (horaB.length <= 4) {
	            horaB = "0" + horaB;
	        }
	        return ((horaA < horaB) ? -1 : ((horaA > horaB) ? 1 : 0));
	    }


	    function CrearGraficaUsuariosEnLineaSinMedias(tipoGrafica, valor) {
	        esNow = tipoGrafica;
	        $(function () {
	            var currData, currDataSource, doGeneration;
	            doGeneration = function () {
	                if (datos != null) {
	                    $('[id$=chart]').css("display", "block");

	                    /******** DSL Ocultar tabla con medias *****************/
	                    //$('[id$=gridOnlineUsers]').css("display", "block");
	                    $('[id$=gridOnlineUsers]').css("display", "none");
	                    /******** DSL Ocultar tabla con medias *****************/

	                    $('[id$=legend]').css("display", "block");
	                    $('[id$=divNoData]').remove();
	                    data = [];
	                    if (valor == 1) { datos.sort(OrdenarPorHoraAcceso); }
	                    for (var i = 0; i < datos.length; i++) {
	                        //Ordenar el array de horas las horas vienen en el data[]
	                        var allNumUsuarios = datos[i].numUsuariosWeb + datos[i].numUsuariosGS + datos[i].numUsuariosPortal;
	                        if (datos[i].numUsuariosWeb == -2) {
	                            datos[i].numUsuariosWeb = null;
	                            allNumUsuarios = null;
	                        }
	                        if (datos[i].numUsuariosGS == -2) {
	                            datos[i].numUsuariosGS = null;
	                            allNumUsuarios = null;
	                        }
	                        if (datos[i].numUsuariosPortal == -2) {
	                            datos[i].numUsuariosPortal = null;
	                            allNumUsuarios = null;
	                        }
	                        data[i] = { Label: datos[i].horaAcceso.toString(), Value1: datos[i].numUsuariosWeb, Value2: datos[i].numUsuariosGS, Value3: datos[i].numUsuariosPortal, Value7: allNumUsuarios };
	                    }
	                    currDataSource = null;
	                    currData = data;
	                    currDataSource = new $.ig.DataSource({ dataSource: null });
	                    currDataSource = new $.ig.DataSource({ dataSource: currData });
	                } else {
	                    $('[id$=chart]').css("display", "none");
	                    $('[id$=panelMedioIzda]').append("<div id='divNoData' style='height:100px; margin-top:130px; margin-left:150px'><span id='lblNoData'></span></div>");
	                    $('[id$=lblNoData]').text(TextosVisorActividad[33]);
	                    $('[id$=gridOnlineUsers]').css("display", "none");
	                    $('[id$=legend]').css("display", "none");
	                }
	            }
	            doGeneration();
	            $("#chart").igDataChart({
	                //                     width: "80%",    
	                width: "60%",
	                height: "265px",
	                leftMargin: 10,
	                topMargin: 10,
	                dataSource: currDataSource,
	                axes: [{
	                    name: "xAxis",
	                    interval: 1,
	                    type: "categoryX",
	                    label: "Label",
	                    labelAngle: 65,
	                    labelExtent: 30
	                },
				{
				    name: "yAxis",
				    type: "numericY"
				}],
	                //crea la leyenda que aparece a la derecha del grafico
	                series: [{
	                    name: "series1",
	                    title: "FULLSTEP WEB",
	                    type: "line",
	                    xAxis: "xAxis",
	                    yAxis: "yAxis",
	                    valueMemberPath: "Value1",
	                    showTooltip: true,
	                    tooltipTemplate: "tooltipTemplate1",
	                    thickness: 4,
	                    legend: {
	                        element: "legend"
	                    },
	                    horizontalZoomable: true,
	                    verticalZoomable: true
	                }, {
	                    name: "series2",
	                    title: "FULLSTEP GS",
	                    type: "line",
	                    xAxis: "xAxis",
	                    yAxis: "yAxis",
	                    valueMemberPath: "Value2",
	                    showTooltip: true,
	                    tooltipTemplate: "tooltipTemplate2",
	                    thickness: 3,
	                    legend: {
	                        element: "legend"
	                    },
	                    horizontalZoomable: true,
	                    verticalZoomable: true
	                }, {
	                    name: "series3",
	                    title: "FULLSTEP PORTAL",
	                    type: "line",
	                    xAxis: "xAxis",
	                    yAxis: "yAxis",
	                    valueMemberPath: "Value3",
	                    showTooltip: true,
	                    tooltipTemplate: "tooltipTemplate3",
	                    thickness: 2,
	                    legend: {
	                        element: "legend"
	                    },
	                    horizontalZoomable: true,
	                    verticalZoomable: true
	                }, {
	                    name: "series7",
	                    title: "ALL",
	                    type: "line",
	                    xAxis: "xAxis",
	                    yAxis: "yAxis",
	                    valueMemberPath: "Value7",
	                    showTooltip: true,
	                    tooltipTemplate: "tooltipTemplate7",
	                    thickness: 1,
	                    legend: {
	                        element: "legend"
	                    },
	                    horizontalZoomable: true,
	                    verticalZoomable: true
	                }],
	                horizontalZoomable: true,
	                verticalZoomable: true,
	                windowResponse: "immediate"
	            });
	        });

	        //Carga la tabla con los datos que se van a mostrar en la gráfica
	        $(function () {
	            var avgWeb = 0;
	            var avgGS = 0;
	            var avgPortal = 0;
	            var avgAll = 0;

	            var maxWeb = 0;
	            var maxGS = 0;
	            var maxPortal = 0;
	            var maxAll = 0;

	            var minWeb = 1000;
	            var minGS = 1000;
	            var minPortal = 1000;
	            var minAll = 1000;

	            var totalWeb = 0;
	            var totalGS = 0;
	            var totalPortal = 0;
	            var totalAll = 0;

	            if (datos != null) {
	                for (var i = 0; i < datos.length; i++) {

	                    var sinNuloNumUsuariosWeb = (datos[i].numUsuariosWeb == null ? 0 : datos[i].numUsuariosWeb);
	                    var sinNuloNumUsuariosGS = (datos[i].numUsuariosGS == null ? 0 : datos[i].numUsuariosGS);
	                    var sinNuloNumUsuariosPortal = (datos[i].numUsuariosPortal == null ? 0 : datos[i].numUsuariosPortal);

	                    avgWeb += sinNuloNumUsuariosWeb;
	                    avgGS += sinNuloNumUsuariosGS;
	                    avgPortal += sinNuloNumUsuariosPortal;
	                    avgAll += sinNuloNumUsuariosWeb + sinNuloNumUsuariosGS + sinNuloNumUsuariosPortal;
	                    totalGS = datos[i].totalUsuariosGS;
	                    totalWeb = datos[i].totalUsuariosWeb;
	                    totalPortal = datos[i].totalUsuariosPortal;
	                    totalAll = datos[i].totalUsuariosGS + datos[i].totalUsuariosWeb + datos[i].totalUsuariosPortal;

	                    if (sinNuloNumUsuariosWeb > maxWeb) {
	                        maxWeb = sinNuloNumUsuariosWeb;
	                    }
	                    if (sinNuloNumUsuariosGS > maxGS) {
	                        maxGS = sinNuloNumUsuariosGS;
	                    }
	                    if (sinNuloNumUsuariosPortal > maxPortal) {
	                        maxPortal = sinNuloNumUsuariosPortal;
	                    }
	                    if ((sinNuloNumUsuariosWeb + sinNuloNumUsuariosGS + sinNuloNumUsuariosPortal) > maxAll) {
	                        maxAll = sinNuloNumUsuariosWeb + sinNuloNumUsuariosGS + sinNuloNumUsuariosPortal;
	                    }
	                    if (sinNuloNumUsuariosWeb < minWeb) {
	                        minWeb = sinNuloNumUsuariosWeb;
	                    }
	                    if (sinNuloNumUsuariosGS < minGS) {
	                        minGS = sinNuloNumUsuariosGS;
	                    }
	                    if (sinNuloNumUsuariosPortal < minPortal) {
	                        minPortal = sinNuloNumUsuariosPortal;
	                    }
	                    if ((sinNuloNumUsuariosWeb + sinNuloNumUsuariosGS + sinNuloNumUsuariosPortal) < minAll) {
	                        minAll = sinNuloNumUsuariosWeb + sinNuloNumUsuariosGS + sinNuloNumUsuariosPortal;
	                    }

	                }

	                avgWeb = Math.floor(avgWeb / datos.length * 100) / 100;
	                avgGS = Math.floor(avgGS / datos.length * 100) / 100;
	                avgPortal = Math.floor(avgPortal / datos.length * 100) / 100;
	                avgAll = Math.floor(avgAll / datos.length * 100) / 100;

	                if (tipoGrafica == "NOW") {
	                    nowWeb = datos[datos.length - 1].numUsuariosWeb;
	                    nowGS = datos[datos.length - 1].numUsuariosGS;
	                    nowPortal = datos[datos.length - 1].numUsuariosPortal;
	                    nowAll = nowWeb + nowGS + nowPortal;

	                    $('#gridOnlineUsers').empty();
	                    $('#gridOnlineUsers').append('<table id="tableOnlineUsers">');
	                    $('#gridOnlineUsers').append('<thead><tr>');
	                    $('#gridOnlineUsers').append('<th style="height:17px; font-size:8px; border-right:1px solid black" align="center"></th>');
	                    $('#gridOnlineUsers').append('<th style="font-size:8px; vertical-align:middle" class="tabla-con-border-azul-fondo-oscuro" align="center">&nbsp&nbspPortal&nbsp&nbsp</th>');
	                    $('#gridOnlineUsers').append('<th style="font-size:8px; vertical-align:middle" class="tabla-con-border-naranja-fondo-oscuro" align="center">&nbsp&nbspWeb&nbsp&nbsp</th>');
	                    $('#gridOnlineUsers').append('<th style="font-size:8px; vertical-align:middle" class="tabla-con-border-verde-fondo-oscuro" align="center">&nbsp&nbspGS&nbsp&nbsp</th>');
	                    $('#gridOnlineUsers').append('<th style="font-size:8px; vertical-align:middle" class="tabla-con-border-rojo-fondo-oscuro" align="center">&nbsp&nbspAll&nbsp&nbsp</th>');
	                    $('#gridOnlineUsers').append('</tr></thead>');
	                    $('#gridOnlineUsers').append('<tbody><tr>');
	                    $('#gridOnlineUsers').append('<td style="font-size:8px; vertical-align:middle" class="tabla-con-border-fondo-oscuro" align="center">NOW </td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-azul" align="right"> ' + nowPortal + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-naranja" align="right">' + nowWeb + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-verde" align="right">' + nowGS + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-rojo" align="right">' + nowAll + '&nbsp</td> </tr>');
	                    $('#gridOnlineUsers').append('<tr>');
	                    $('#gridOnlineUsers').append('<td style="font-size:8px; vertical-align:middle" class="tabla-con-border-fondo-oscuro" align="center">MAX </td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-azul" align="right"> ' + maxPortal + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-naranja" align="right">' + maxWeb + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-verde" align="right">' + maxGS + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-rojo" align="right">' + maxAll + '&nbsp</td> </tr>');
	                    $('#gridOnlineUsers').append('<tr>');
	                    $('#gridOnlineUsers').append('<td style="font-size:8px; vertical-align:middle" class="tabla-con-border-fondo-oscuro" align="center">MIN </td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-azul" align="right"> ' + minPortal + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-naranja" align="right">' + minWeb + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-verde" align="right">' + minGS + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-rojo" align="right">' + minAll + '&nbsp</td> </tr>');
	                    $('#gridOnlineUsers').append('<tr>');
	                    $('#gridOnlineUsers').append('<td style="font-size:8px; vertical-align:middle" class="tabla-con-border-fondo-oscuro" align="center">AVG </td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-azul" align="right">' + avgPortal + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-naranja" align="right">' + avgWeb + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-verde" align="right">' + avgGS + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-rojo" align="right">' + avgAll + '&nbsp</td> </tr>');
	                    $('#gridOnlineUsers').append('<tr>');
	                    $('#gridOnlineUsers').append('<td style="font-size:8px; vertical-align:middle" class="tabla-con-border-fondo-oscuro-abajo" align="center">TOTAL </td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-azul-abajo" align="right">' + totalPortal + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-naranja-abajo" align="right">' + totalWeb + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-verde-abajo" align="right">' + totalGS + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-rojo-abajo" align="right">' + totalAll + '&nbsp</td> </tr></tbody>');
	                    $('#gridOnlineUsers').append('</table>');
	                } else {
	                    $('#gridOnlineUsers').empty();
	                    $('#gridOnlineUsers').append('<table id="tableOnlineUsers">');
	                    $('#gridOnlineUsers').append('<thead><tr>');
	                    $('#gridOnlineUsers').append('<th style="height:17px; font-size:8px; border-right: 1px solid black" align="center"></th>');
	                    $('#gridOnlineUsers').append('<th style="font-size:8px; vertical-align:middle" class="tabla-con-border-azul-fondo-oscuro" align="center">&nbsp&nbspPortal&nbsp&nbsp</th>');
	                    $('#gridOnlineUsers').append('<th style="font-size:8px; vertical-align:middle" class="tabla-con-border-naranja-fondo-oscuro" align="center">&nbsp&nbspWeb&nbsp&nbsp</th>');
	                    $('#gridOnlineUsers').append('<th style="font-size:8px; vertical-align:middle" class="tabla-con-border-verde-fondo-oscuro" align="center">&nbsp&nbspGS&nbsp&nbsp</th>');
	                    $('#gridOnlineUsers').append('<th style="font-size:8px; vertical-align:middle" class="tabla-con-border-rojo-fondo-oscuro" align="center">&nbsp&nbspAll&nbsp&nbsp</th>');
	                    $('#gridOnlineUsers').append('</tr></thead>');
	                    $('#gridOnlineUsers').append('<tbody><tr>');
	                    $('#gridOnlineUsers').append('<td style="font-size:8px; vertical-align:middle" class="tabla-con-border-fondo-oscuro" align="center">AVG </td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-azul" align="right">' + avgPortal + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-naranja" align="right">' + avgWeb + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-verde" align="right">' + avgGS + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-rojo" align="right">' + avgAll + '&nbsp</td></tr>');
	                    $('#gridOnlineUsers').append('<tr>');
	                    $('#gridOnlineUsers').append('<td style="font-size:8px; vertical-align:middle" class="tabla-con-border-fondo-oscuro" align="center">MAX </td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-azul" align="right">' + maxPortal + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-naranja" align="right">' + maxWeb + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-verde" align="right">' + maxGS + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-rojo" align="right">' + maxAll + '&nbsp</td> </tr>');
	                    $('#gridOnlineUsers').append('<tr>');
	                    $('#gridOnlineUsers').append('<td style="font-size:8px; vertical-align:middle" class="tabla-con-border-fondo-oscuro" align="center">MIN </td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-azul" align="right">' + minPortal + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-naranja" align="right">' + minWeb + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-verde" align="right">' + minGS + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-rojo" align="right">' + minAll + '&nbsp</td> </tr>');
	                    $('#gridOnlineUsers').append('<tr>');
	                    $('#gridOnlineUsers').append('<td style="font-size:8px; vertical-align:middle" class="tabla-con-border-fondo-oscuro-abajo" align="center">TOTAL </td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-azul-abajo" align="right">' + totalPortal + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-naranja-abajo" align="right">' + totalWeb + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-verde-abajo" align="right">' + totalGS + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-rojo-abajo" align="right">' + totalAll + '&nbsp</td> </tr></tbody>');
	                    $('#gridOnlineUsers').append('</table>');
	                }

	                /************* DSL Aumentar margen respecto al valor máximo ******************************/
	                $("#chart").igDataChart("option", "axes", [{ name: "yAxis", type: "numericY", maximumValue: maxAll + 1}]);
	                /*****************************************************************************************/
	            }
	        });
	    }



	    //Crea la grafica de accesos de usuarios
	    function CrearGraficaUsuariosEnLinea(tipoGrafica, valor, timeoffset) {
	        esNow = tipoGrafica;
	        $(function () {
	            var currData, currDataSource, doGeneration;
	            doGeneration = function () {
	                if (datos != null) {
	                    $('[id$=chart]').css("display", "block");
	                    $('[id$=gridOnlineUsers]').css("display", "block");
	                    $('[id$=legend]').css("display", "block");
	                    $('[id$=divNoData]').remove();
	                    data = [];
	                    if (valor == 1) { datos.sort(OrdenarPorHoraAcceso); }
	                    for (var i = 0; i < datos.length; i++) {
	                        var allNumUsuarios = datos[i].numUsuariosWeb + datos[i].numUsuariosGS + datos[i].numUsuariosPortal;
	                        var allAvgUsuarios = datos[i].avgUsuariosWeb + datos[i].avgUsuariosGS + datos[i].avgUsuariosPortal;
	                        if (datos[i].numUsuariosWeb == -2) {
	                            datos[i].numUsuariosWeb = null;
	                            allNumUsuarios = null;
	                        }
	                        if (datos[i].numUsuariosGS == -2) {
	                            datos[i].numUsuariosGS = null;
	                            allNumUsuarios = null;
	                        }
	                        if (datos[i].numUsuariosPortal == -2) {
	                            datos[i].numUsuariosPortal = null;
	                            allNumUsuarios = null;
	                        }
	                        if (datos[i].avgUsuariosWeb == -2) {
	                            datos[i].avgUsuariosWeb = null;
	                            allAvgUsuarios = null;
	                        }
	                        if (datos[i].avgUsuariosGS == -2) {
	                            datos[i].avgUsuariosGS = null;
	                            allAvgUsuarios = null;
	                        }
	                        if (datos[i].avgUsuariosPortal == -2) {
	                            datos[i].avgUsuariosPortal = null;
	                            allAvgUsuarios = null;
	                        }
	                        data[i] = { Label: datos[i].horaAcceso.toString(), Value1: datos[i].numUsuariosWeb, Value2: datos[i].numUsuariosGS, Value3: datos[i].numUsuariosPortal, Value4: datos[i].avgUsuariosWeb, Value5: datos[i].avgUsuariosGS, Value6: datos[i].avgUsuariosPortal, Value7: allNumUsuarios, Value8: allAvgUsuarios };
	                    }
	                    currDataSource = null;
	                    currData = data;
	                    currDataSource = new $.ig.DataSource({ dataSource: null });
	                    currDataSource = new $.ig.DataSource({ dataSource: currData });
	                } else {
	                    $('[id$=chart]').css("display", "none");
	                    $('[id$=panelMedioIzda]').append("<div id='divNoData' style='height:100px; margin-top:130px; margin-left:150px'><span id='lblNoData'></span></div>");
	                    $('[id$=lblNoData]').text(TextosVisorActividad[33]);
	                    $('[id$=gridOnlineUsers]').css("display", "none");
	                    $('[id$=legend]').css("display", "none");
	                }
	            }
	            doGeneration();
	            $("#chart").igDataChart({
	                //width: "80%", 
	                width: "60%",
	                height: "265px",
	                leftMargin: 10,
	                topMargin: 10,
	                dataSource: currDataSource,
	                axes: [{
	                    name: "xAxis",
	                    interval: 1,
	                    type: "categoryX",
	                    label: "Label",
	                    labelAngle: 65,
	                    labelExtent: 30
	                },
				{
				    name: "yAxis",
				    type: "numericY"
				}],
	                series: [{
	                    name: "series1",
	                    title: "FULLSTEP WEB",
	                    type: "line",
	                    xAxis: "xAxis",
	                    yAxis: "yAxis",
	                    valueMemberPath: "Value1",
	                    showTooltip: true,
	                    tooltipTemplate: "tooltipTemplate1",
	                    thickness: 3,
	                    legend: {
	                        element: "legend"
	                    },
	                    horizontalZoomable: true,
	                    verticalZoomable: true
	                }, {
	                    name: "series2",
	                    title: "FULLSTEP GS",
	                    type: "line",
	                    xAxis: "xAxis",
	                    yAxis: "yAxis",
	                    valueMemberPath: "Value2",
	                    showTooltip: true,
	                    tooltipTemplate: "tooltipTemplate2",
	                    thickness: 3,
	                    legend: {
	                        element: "legend"
	                    },
	                    horizontalZoomable: true,
	                    verticalZoomable: true
	                }, {
	                    name: "series3",
	                    title: "FULLSTEP PORTAL",
	                    type: "line",
	                    xAxis: "xAxis",
	                    yAxis: "yAxis",
	                    valueMemberPath: "Value3",
	                    showTooltip: true,
	                    tooltipTemplate: "tooltipTemplate3",
	                    thickness: 3,
	                    legend: {
	                        element: "legend"
	                    },
	                    horizontalZoomable: true,
	                    verticalZoomable: true
	                    /**** Se oculta la representacion de las medias en el grafico. ****/
	                    /**** Se comenta el codigo anterior por si en un futuro se quiere recuperar el calculo y muestra de medias. ****/
	                    //					}, {
	                    //						name: "series4",
	                    //						title: "AVG FULLSTEP WEB",
	                    //						type: "line",
	                    //						xAxis: "xAxis",
	                    //						yAxis: "yAxis",
	                    //						valueMemberPath: "Value4",
	                    //						showTooltip: true,
	                    //						tooltipTemplate: "tooltipTemplate4",
	                    //						thickness: 1,
	                    //						horizontalZoomable: true,
	                    //						verticalZoomable: true
	                    //					}, {
	                    //						name: "series5",
	                    //						title: "AVG FULLSTEP GS",
	                    //						type: "line",
	                    //						xAxis: "xAxis",
	                    //						yAxis: "yAxis",
	                    //						valueMemberPath: "Value5",
	                    //						showTooltip: true,
	                    //						tooltipTemplate: "tooltipTemplate5",
	                    //						thickness: 1,
	                    //						horizontalZoomable: true,
	                    //						verticalZoomable: true
	                    //					}, {
	                    //						name: "series6",
	                    //						title: "AVG FULLSTEP PORTAL",
	                    //						type: "line",
	                    //						xAxis: "xAxis",
	                    //						yAxis: "yAxis",
	                    //						valueMemberPath: "Value6",
	                    //						showTooltip: true,
	                    //						tooltipTemplate: "tooltipTemplate6",
	                    //						thickness: 1,
	                    //						horizontalZoomable: true,
	                    //						verticalZoomable: true
	                    /**** Fin ocultacion de medias. ****/
	                }, {
	                    name: "series7",
	                    title: "ALL",
	                    type: "line",
	                    xAxis: "xAxis",
	                    yAxis: "yAxis",
	                    valueMemberPath: "Value7",
	                    showTooltip: true,
	                    tooltipTemplate: "tooltipTemplate7",
	                    thickness: 3,
	                    legend: {
	                        element: "legend"
	                    },
	                    horizontalZoomable: true,
	                    verticalZoomable: true
	                    /**** Se oculta la representacion del total de medias en el grafico. ****/
	                    /**** Se comenta el codigo anterior por si en un futuro se quiere recuperar el calculo y muestra de medias. ****/
	                    //					}, {
	                    //						name: "series8",
	                    //						title: "AVG ALL",
	                    //						type: "line",
	                    //						xAxis: "xAxis",
	                    //						yAxis: "yAxis",
	                    //						valueMemberPath: "Value8",
	                    //						showTooltip: true,
	                    //						tooltipTemplate: "tooltipTemplate8",
	                    //						thickness: 1,
	                    //						horizontalZoomable: true,
	                    //						verticalZoomable: true
	                    /**** Fin ocultacion de medias. ****/
	                }],

	                horizontalZoomable: true,
	                verticalZoomable: true,
	                windowResponse: "immediate"
	            });
	        });

	        //Carga la tabla con los datos que se van a mostrar en la gráfica
	        $(function () {
	            var avgWeb = 0;
	            var avgGS = 0;
	            var avgPortal = 0;
	            var avgAll = 0;

	            var maxWeb = 0;
	            var maxGS = 0;
	            var maxPortal = 0;
	            var maxAll = 0;

	            var minWeb = 1000;
	            var minGS = 1000;
	            var minPortal = 1000;
	            var minAll = 1000;

	            var totalWeb = 0;
	            var totalGS = 0;
	            var totalPortal = 0;
	            var totalAll = 0;
	            if (datos != null) {
	                for (var i = 0; i < datos.length; i++) {

	                    var sinNuloNumUsuariosWeb = (datos[i].numUsuariosWeb == null ? 0 : datos[i].numUsuariosWeb);
	                    var sinNuloNumUsuariosGS = (datos[i].numUsuariosGS == null ? 0 : datos[i].numUsuariosGS);
	                    var sinNuloNumUsuariosPortal = (datos[i].numUsuariosPortal == null ? 0 : datos[i].numUsuariosPortal);

	                    avgWeb += sinNuloNumUsuariosWeb;
	                    avgGS += sinNuloNumUsuariosGS;
	                    avgPortal += sinNuloNumUsuariosPortal;
	                    avgAll += sinNuloNumUsuariosWeb + sinNuloNumUsuariosGS + sinNuloNumUsuariosPortal;
	                    totalGS = datos[i].totalUsuariosGS;
	                    totalWeb = datos[i].totalUsuariosWeb;
	                    totalPortal = datos[i].totalUsuariosPortal;
	                    totalAll = datos[i].totalUsuariosGS + datos[i].totalUsuariosWeb + datos[i].totalUsuariosPortal;

	                    if (sinNuloNumUsuariosWeb > maxWeb) {
	                        maxWeb = sinNuloNumUsuariosWeb;
	                    }
	                    if (sinNuloNumUsuariosGS > maxGS) {
	                        maxGS = sinNuloNumUsuariosGS;
	                    }
	                    if (sinNuloNumUsuariosPortal > maxPortal) {
	                        maxPortal = sinNuloNumUsuariosPortal;
	                    }
	                    if ((sinNuloNumUsuariosWeb + sinNuloNumUsuariosGS + sinNuloNumUsuariosPortal) > maxAll) {
	                        maxAll = sinNuloNumUsuariosWeb + sinNuloNumUsuariosGS + sinNuloNumUsuariosPortal;
	                    }
	                    if (sinNuloNumUsuariosWeb < minWeb) {
	                        minWeb = sinNuloNumUsuariosWeb;
	                    }
	                    if (sinNuloNumUsuariosGS < minGS) {
	                        minGS = sinNuloNumUsuariosGS;
	                    }
	                    if (sinNuloNumUsuariosPortal < minPortal) {
	                        minPortal = sinNuloNumUsuariosPortal;
	                    }
	                    if ((sinNuloNumUsuariosWeb + sinNuloNumUsuariosGS + sinNuloNumUsuariosPortal) < minAll) {
	                        minAll = sinNuloNumUsuariosWeb + sinNuloNumUsuariosGS + sinNuloNumUsuariosPortal;
	                    }

	                }

	                avgWeb = Math.floor(avgWeb / datos.length * 100) / 100;
	                avgGS = Math.floor(avgGS / datos.length * 100) / 100;
	                avgPortal = Math.floor(avgPortal / datos.length * 100) / 100;
	                avgAll = Math.floor(avgAll / datos.length * 100) / 100;

	                if (tipoGrafica == "NOW") {
	                    nowWeb = datos[datos.length - 1].numUsuariosWeb;
	                    nowGS = datos[datos.length - 1].numUsuariosGS;
	                    nowPortal = datos[datos.length - 1].numUsuariosPortal;
	                    nowAll = nowWeb + nowGS + nowPortal;

	                    $('#gridOnlineUsers').empty();
	                    $('#gridOnlineUsers').append('<table id="tableOnlineUsers"  >');
	                    $('#gridOnlineUsers').append('<thead><tr>');
	                    $('#gridOnlineUsers').append('<th style="height:17px; font-size:8px; border-right: 1px solid black" align="center"></th>');
	                    $('#gridOnlineUsers').append('<th style="font-size:8px; vertical-align:middle" class="tabla-con-border-azul-fondo-oscuro" align="center">&nbsp&nbspPortal&nbsp&nbsp</th>');
	                    $('#gridOnlineUsers').append('<th style="font-size:8px; vertical-align:middle" class="tabla-con-border-naranja-fondo-oscuro" align="center">&nbsp&nbspWeb&nbsp&nbsp</th>');
	                    $('#gridOnlineUsers').append('<th style="font-size:8px; vertical-align:middle" class="tabla-con-border-verde-fondo-oscuro" align="center">&nbsp&nbspGS&nbsp&nbsp</th>');
	                    $('#gridOnlineUsers').append('<th style="font-size:8px; vertical-align:middle" class="tabla-con-border-rojo-fondo-oscuro" align="center">&nbsp&nbspAll&nbsp&nbsp</th>');
	                    $('#gridOnlineUsers').append('</tr></thead>');
	                    $('#gridOnlineUsers').append('<tbody><tr>');
	                    $('#gridOnlineUsers').append('<td style="font-size:8px; vertical-align:middle" class="tabla-con-border-fondo-oscuro" align="center">NOW </td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-azul" align="right"> ' + nowPortal + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-naranja" align="right">' + nowWeb + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-verde" align="right">' + nowGS + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-rojo" align="right">' + nowAll + '&nbsp</td> </tr>');
	                    $('#gridOnlineUsers').append('<tr>');
	                    $('#gridOnlineUsers').append('<td style="font-size:8px; vertical-align:middle" class="tabla-con-border-fondo-oscuro" align="center">MAX </td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-azul" align="right"> ' + maxPortal + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-naranja" align="right">' + maxWeb + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-verde" align="right">' + maxGS + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-rojo" align="right">' + maxAll + '&nbsp</td> </tr>');
	                    $('#gridOnlineUsers').append('<tr>');
	                    $('#gridOnlineUsers').append('<td style="font-size:8px; vertical-align:middle" class="tabla-con-border-fondo-oscuro" align="center">MIN </td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-azul" align="right"> ' + minPortal + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-naranja" align="right">' + minWeb + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-verde" align="right">' + minGS + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-rojo" align="right">' + minAll + '&nbsp</td> </tr>');
	                    $('#gridOnlineUsers').append('<tr>');
	                    $('#gridOnlineUsers').append('<td style="font-size:8px; vertical-align:middle" class="tabla-con-border-fondo-oscuro" align="center">AVG </td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-azul" align="right">' + avgPortal + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-naranja" align="right">' + avgWeb + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-verde" align="right">' + avgGS + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-rojo" align="right">' + avgAll + '&nbsp</td></tr>');
	                    $('#gridOnlineUsers').append('<tr>');
	                    $('#gridOnlineUsers').append('<td style="font-size:8px; vertical-align:middle" class="tabla-con-border-fondo-oscuro-abajo" align="center">TOTAL </td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-azul-abajo" align="right">' + totalPortal + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-naranja-abajo" align="right">' + totalWeb + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-verde-abajo" align="right">' + totalGS + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-rojo-abajo" align="right">' + totalAll + '&nbsp</td></tr></tbody>');
	                    $('#gridOnlineUsers').append('</table>');

	                } else {
	                    $('#gridOnlineUsers').empty();
	                    $('#gridOnlineUsers').append('<table id="tableOnlineUsers">');
	                    $('#gridOnlineUsers').append('<thead><tr>');
	                    $('#gridOnlineUsers').append('<th style="height:17px; font-size:8px; border-right: 1px solid black" align="center"></th>');
	                    $('#gridOnlineUsers').append('<th style="font-size:8px; vertical-align:middle" class="tabla-con-border-azul-fondo-oscuro" align="center">&nbsp&nbspPortal&nbsp&nbsp</th>');
	                    $('#gridOnlineUsers').append('<th style="font-size:8px; vertical-align:middle" class="tabla-con-border-naranja-fondo-oscuro" align="center">&nbsp&nbspWeb&nbsp&nbsp</th>');
	                    $('#gridOnlineUsers').append('<th style="font-size:8px; vertical-align:middle" class="tabla-con-border-verde-fondo-oscuro" align="center">&nbsp&nbspGS&nbsp&nbsp</th>');
	                    $('#gridOnlineUsers').append('<th style="font-size:8px; vertical-align:middle" class="tabla-con-border-rojo-fondo-oscuro" align="center">&nbsp&nbspAll&nbsp&nbsp</th>');
	                    $('#gridOnlineUsers').append('</tr></thead>');
	                    $('#gridOnlineUsers').append('<tbody><tr>');
	                    $('#gridOnlineUsers').append('<td style="font-size:8px; vertical-align:middle" class="tabla-con-border-fondo-oscuro" align="center">AVG </td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-azul" align="right">' + avgPortal + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-naranja" align="right">' + avgWeb + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-verde" align="right">' + avgGS + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-rojo" align="right">' + avgAll + '&nbsp</td> </tr>');
	                    $('#gridOnlineUsers').append('<tr>');
	                    $('#gridOnlineUsers').append('<td style="font-size:8px; vertical-align:middle" class="tabla-con-border-fondo-oscuro" align="center">MAX </td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-azul" align="right">' + maxPortal + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-naranja" align="right">' + maxWeb + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-verde" align="right">' + maxGS + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-rojo" align="right">' + maxAll + '&nbsp</td> </tr>');
	                    $('#gridOnlineUsers').append('<tr>');
	                    $('#gridOnlineUsers').append('<td style="font-size:8px; vertical-align:middle" class="tabla-con-border-fondo-oscuro" align="center">MIN </td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-azul" align="right">' + minPortal + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-naranja" align="right">' + minWeb + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-verde" align="right">' + minGS + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-rojo" align="right">' + minAll + '&nbsp</td> </tr>');
	                    $('#gridOnlineUsers').append('<tr>');
	                    $('#gridOnlineUsers').append('<td style="font-size:8px; vertical-align:middle" class="tabla-con-border-fondo-oscuro-abajo" align="center">TOTAL </td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-azul-abajo" align="right">' + totalPortal + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-naranja-abajo" align="right">' + totalWeb + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-verde-abajo" align="right">' + totalGS + '&nbsp</td> <td style="font-size:8px; vertical-align:middle" class="tabla-con-border-rojo-abajo" align="right">' + totalAll + '&nbsp</td></tr></tbody>');
	                    $('#gridOnlineUsers').append('</table>');
	                }

	                /************* DSL Aumentar margen respecto al valor máximo ******************************/
	                $("#chart").igDataChart("option", "axes", [{ name: "yAxis", type: "numericY", maximumValue: maxAll + 1}]);
	                /*****************************************************************************************/
	            }
	        });
	    }

	    var idioma;

	    //formatea la fecha en base a la seleccion de las opciones del usuario
	    function FormatoFecha() {
	        var fecha2 = "";
	        var formato = "";
	        $(function () {
	            //Recuperamos el formato de la fecha de la aplicacion
	            var to = rutaFS + 'BI/VisorActividad.aspx/FormatoFecha';
	            $.when($.ajax({
	                type: 'POST',
	                url: to,
	                contentType: 'application/json;charset=utf-8',
	                dataType: 'json',
	                async: false
	            })).done(function (msg) {
	                formato = null;
	                formato = msg.d;

	                if (formato.toUpperCase() == "DD/MM/YYYY" || formato.toUpperCase() == "DD/MM/YY") {
	                    $('#dpSelectDay').datepicker('option', { dateFormat: 'dd/mm/yy' });
	                    $('#dpSelectWeek').datepicker('option', { dateFormat: 'dd/mm/yy' });

	                } else if (formato.toUpperCase() == "MM/DD/YYYY" || formato.toUpperCase() == "MM/DD/YY") {
	                    $('#dpSelectDay').datepicker('option', { dateFormat: 'mm/dd/yy' });
	                    $('#dpSelectWeek').datepicker('option', { dateFormat: 'mm/dd/yy' });

	                } else if (formato.toUpperCase() == "DD-MM-YYYY" || formato.toUpperCase() == "DD-MM-YY") {
	                    $('#dpSelectDay').datepicker('option', { dateFormat: 'dd-mm-yy' });
	                    $('#dpSelectWeek').datepicker('option', { dateFormat: 'dd-mm-yy' });

	                } else if (formato.toUpperCase() == "MM-DD-YYYY" || formato.toUpperCase() == "MM-DD-YY") {
	                    $('#dpSelectDay').datepicker('option', { dateFormat: 'mm-dd-yy' });
	                    $('#dpSelectWeek').datepicker('option', { dateFormat: 'mm-dd-yy' });

	                } else if (formato.toUpperCase() == "DD.MM.YYYY" || formato.toUpperCase() == "DD.MM.YY") {
	                    $('#dpSelectDay').datepicker('option', { dateFormat: 'dd.mm.yy' });
	                    $('#dpSelectWeek').datepicker('option', { dateFormat: 'dd.mm.yy' });

	                } else if (formato.toUpperCase() == "MM.DD.YYYY" || formato.toUpperCase() == "MM.DD.YY") {
	                    $('#dpSelectDay').datepicker('option', { dateFormat: 'mm.dd.yy' });
	                    $('#dpSelectWeek').datepicker('option', { dateFormat: 'mm.dd.yy' });
	                }
	            });
	        })
	        //return fecha2;
	    }
	    $(document).ready(function () {
	        //formateamos los datepicker con el formato de las fechas del usuario
	        FormatoFecha();

	        //Recuperamos el entorno en el que estamos (el primer registro de la tabla FSAL_ENTORNO)
	        var primerEntorno = "";
	        var to = rutaFS + 'BI/VisorActividad.aspx/RecuperarEntorno';
	        $.when($.ajax({
	            type: 'POST',
	            url: to,
	            contentType: 'application/json;charset=utf-8',
	            dataType: 'json',
	            async: false
	        })).done(function (msg) {
	            primerEntorno = null;
	            primerEntorno = msg.d;
	        });

	        //Textos y estilos 
	        $('[id$=divCargando]').css("display", "none");
	        $('[id$=divTabContainer]').css("display", "block");
	        $('[id$=lblActivityDashboard]').text(TextosVisorActividad[12]);
	        $('[id$=lblPerformanceDashboard]').text(TextosVisorActividad[13]);
	        $('[id$=lblStatisticalInfo]').text(TextosVisorActividad[14]);
	        $('[id$=lblEnviroment]').text(TextosVisorActividad[15]);
	        $('[id$=lblEnviroment]').css({ 'font-weight': 'bold' });
	        $('[id$=lblFechaDesde]').text(TextosVisorActividad[16]);
	        $('[id$=lblFechaHasta]').text(TextosVisorActividad[17]);
	        $('[id$=lblAhora]').text(TextosVisorActividad[18]);
	        $('[id$=lblHoy]').text(TextosVisorActividad[19]);
	        $('[id$=lblEstaSemana]').text(TextosVisorActividad[20]);
	        $('[id$=lblEsteMes]').text(TextosVisorActividad[21]);
	        $('[id$=lblEsteAnyo]').text(TextosVisorActividad[22]);
	        $('[id$=lblOnlineUsers]').text(TextosVisorActividad[23]);
	        $('[id$=lblOnlineUsers]').css({ 'font-weight': 'bold' });
	        $('[id$=lblRecentActivity]').text(TextosVisorActividad[24]);
	        $('[id$=lblRecentActivity]').css({ 'font-weight': 'bold' });
	        $('[id$=lblEventos]').text(TextosVisorActividad[25]);
	        $('[id$=lblEventos]').css({ 'font-weight': 'bold' });
	        $('[id$=lblErrores]').text(TextosVisorActividad[26]);
	        $('[id$=lblErrores]').css({ 'font-weight': 'bold' });
	        $('[id$=lblIncludeAverages]').text(TextosVisorActividad[32]);
	        $('[id$=lblIncludeAverages]').css({ 'font-size': '10' });
	        /**** Marcamos y ocultamos los checkbox de medias para cargar las tablas, pero estas medias se omiten y ocultan en graficos y grids. ****/
	        /**** Se comenta el codigo anterior por si en un futuro se quiere recuperar el calculo y muestra de medias. ****/
	        //$('[id$=chkAverages]').attr('checked', false);
	        $('[id$=chkAverages]').attr('checked', true);
	        $('[id$=chkAveragesRecentActivity]').attr('checked', true);
	        $('[id$=chkAverages]').css("display", "none");
	        $('[id$=chkAveragesRecentActivity]').css("display", "none");
	        $('[id$=lblIncludeAverages]').css("display", "none");
	        $('[id$=lblIncludeAveragesRecentActivity]').css("display", "none");
	        /**** Fin ocultacion de medias. ****/
	        $('[id$=lblIncludeAveragesRecentActivity]').text(TextosVisorActividad[32]);
	        $('[id$=lblEnviromentPymes]').text(TextosVisorActividad[48]);
	        $('[id$=lblcargando]').text(TextosVisorActividad[64]);
	        $('[id$=lblEnviromentPymes]').css({ 'font-weight': 'bold' });
	        $('[id$=lblEnviromentPymes]').css("display", "none");
	        $('[id$=ddlEnviromentPymes]').css("display", "none");

	        //fechas de inicio y fin
	        var fechaDesde = new Date();
	        var mins = fechaDesde.getMinutes();
	        while (mins % 5 != 0) {
	            mins -= 1;
	        }
	        fechaDesde.setHours(fechaDesde.getHours() - 1, mins, 0);
	        var fechaHasta = new Date();
	        fechaHasta.setHours(fechaDesde.getHours() + 1, mins, 0);

	        //Cargo las gráficas
	        ComprobarUsuariosConectadosRango(fechaDesde, fechaHasta, "HORA-NOW", primerEntorno, null, 1);
	        //CrearGraficaUsuariosEnLineaSinMedias("NOW");

	        ComprobarUsuariosActividadReciente(fechaDesde, fechaHasta, primerEntorno, "HORA", 1, null, 1);  //El tipoEvento que se pasa inicialmente es '1' -> 'Nueva solicitud de compra'
	        //CrearGraficaActividadRecienteSinMedias("");

	        //Combo de los meses
	        var months = [TextosVisorActividad[0], TextosVisorActividad[1], TextosVisorActividad[2], TextosVisorActividad[3], TextosVisorActividad[4], TextosVisorActividad[5], TextosVisorActividad[6], TextosVisorActividad[7], TextosVisorActividad[8], TextosVisorActividad[9], TextosVisorActividad[10], TextosVisorActividad[11]];
	        $.each(months, function (val, text) {
	            $('#ddlSelectMonth').append($('<option></option>').val(val).html(text))
	        });
	        $('#ddlSelectMonth').prepend($('<option value="-1" selected></option>'));

	        var d = new Date();

	        //Combo de los anyos
	        for (var i = 2003; i <= fechaDesde.getFullYear(); i++) {
	            $('#ddlSelectYear').append($('<option></option>').val(i - 2003).html(i));
	            $('#ddlSelectYearAnyo').append($('<option></option>').val(i - 2003).html(i));
	        }
	        //para que se quede seleccionado el año en el que estamos en la combo
	        var y = d.getFullYear();
	        $("#ddlSelectYear").find("option:contains('" + y + "')").each(function () {
	            if ($(this).text() == y) {
	                $(this).attr("selected", "selected");
	            }
	        });
	        $('#ddlSelectYearAnyo').prepend($('<option value="-1" selected></option>'));

	        //Cargo los grids
	        ComprobarGridEventos(fechaDesde, fechaHasta, primerEntorno, null, "", "HORA");
	        ComprobarGridErrores(fechaDesde, fechaHasta, primerEntorno, null);

	        OcultarBotonesSeleccion();
	        OcultarTextosBotonesSeleccion();

	        //Marco Ahora como opción por defecto
	        $('[id$=divHoy]').css("background-color", "white");
	        $('[id$=divAhora]').css("background-color", "#D3D3D3");
	        $('[id$=divEstaSemana]').css("background-color", "white");
	        $('[id$=divEsteMes]').css("background-color", "white");
	        $('[id$=divEsteAnyo]').css("background-color", "white");
	        fecDesde = fechaDesde;
	        fecHasta = fechaHasta;
	        Rango = "HORA-NOW";
	        Texto = TextosVisorActividad[18];
	    });

	    function OcultarBotonesSeleccion() {
	        $('[id$=lblNow]').css("display", "none");
	        $('[id$=lblSelectHour]').css("display", "none");
	        $('[id$=divSelectHour]').css("display", "none");
	        $('[id$=lblToday]').css("display", "none");
	        $('[id$=lblSelectDay]').css("display", "none");
	        $('[id$=dpSelectDay]').css("display", "none");
	        $('[id$=lblThisWeek]').css("display", "none");
	        $('[id$=lblSelectWeek]').css("display", "none");
	        $('[id$=dpSelectWeek]').css("display", "none");
	        $('[id$=lblThisMonth]').css("display", "none");
	        $('[id$=lblSelectMonth]').css("display", "none");
	        $('[id$=ddlSelectMonth]').css("display", "none");
	        $('[id$=ddlSelectYear]').css("display", "none");
	        $('[id$=divSeleccionMes]').css("display", "none");
	        $('[id$=lblThisYear]').css("display", "none");
	        $('[id$=lblSelectYear]').css("display", "none");
	        $('[id$=divSeleccionAnyo]').css("display", "none");
	        $('[id$=ddlSelectYearAnyo]').css("display", "none");

	    }

	    function OcultarTextosBotonesSeleccion() {
	        $('[id$=lblAhora]').text(TextosVisorActividad[18]);
	        $('[id$=lblAhora]').css("font-size", "10px");
	        $('[id$=lblHoy]').text(TextosVisorActividad[19]);
	        $('[id$=lblHoy]').css("font-size", "10px");
	        $('[id$=lblEstaSemana]').text(TextosVisorActividad[20]);
	        $('[id$=lblEstaSemana]').css("font-size", "10px");
	        $('[id$=lblEsteMes]').text(TextosVisorActividad[21]);
	        $('[id$=lblEsteMes]').css("font-size", "10px");
	        $('[id$=lblEsteAnyo]').text(TextosVisorActividad[22]);
	        $('[id$=lblEsteAnyo]').css("font-size", "10px");
	        $('[id$=divAhora]').css("background-color", "white");
	        $('[id$=divHoy]').css("background-color", "white");
	        $('[id$=divEstaSemana]').css("background-color", "white");
	        $('[id$=divEsteMes]').css("background-color", "white");
	        $('[id$=divEsteAnyo]').css("background-color", "white");
	    }

	    //Click del boton Ahora
	    function btnNow_Click() {
	        //mostrar ventana cargando
	        $('[id$=divCargando]').css("display", "block").css("width", "100px").css("margin", "18% auto");
	        $('[id$=divTabContainer]').css("display", "none");
	        setTimeout(Now_Click, 200);
	    }
	    function Now_Click() {
	        var fechaDesde = new Date();
	        var mins = fechaDesde.getMinutes();
	        while (mins % 5 != 0) {
	            mins -= 1;
	        }
	        fechaDesde.setHours(fechaDesde.getHours() - 1, mins, fechaDesde.getSeconds());
	        var fechaHasta = new Date();
	        fechaHasta.setHours(fechaHasta.getHours(), mins, fechaHasta.getSeconds());
	        if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
	            var entorno = $('[id$=lblCodEntorno]').text();
	        } else {
	            var entorno = $('[id$=ddlEnviroment]').val();
	        }
	        var tipoEvento = $('[id$=ddlTipoEvento]').val();
	        var tipoEvento2 = $('[id$=ddlTipoEventoGrid]').val();
	        if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
	            var pyme = $('[id$=ddlEnviromentPymes]').val();
	        } else {
	            var pyme = null;
	        }
	        ComprobarUsuariosActividadReciente(fechaDesde, fechaHasta, entorno, "HORA", tipoEvento, pyme, 1);
	        ComprobarUsuariosConectadosRango(fechaDesde, fechaHasta, "HORA-NOW", entorno, pyme, 1);
	        OcultarBotonesSeleccion();
	        OcultarTextosBotonesSeleccion();
	        ComprobarGridEventos(fechaDesde, fechaHasta, entorno, pyme, tipoEvento2, "HORA");
	        ComprobarGridErrores(fechaDesde, fechaHasta, entorno, pyme);
	        $("#chart").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	        $("#chart2").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	        $('[id$=divAhora]').css("background-color", "white");
	        $('[id$=divHoy]').css("background-color", "white");
	        $('[id$=divEstaSemana]').css("background-color", "white");
	        $('[id$=divEsteMes]').css("background-color", "white");
	        $('[id$=divEsteAnyo]').css("background-color", "white");
	        //marcar como seleccionado
	        $('[id$=divHoy]').css("background-color", "white");
	        $('[id$=divAhora]').css("background-color", "#D3D3D3");
	        $('[id$=divEstaSemana]').css("background-color", "white");
	        $('[id$=divEsteMes]').css("background-color", "white");
	        $('[id$=divEsteAnyo]').css("background-color", "white");
	        fecDesde = fechaDesde;
	        fecHasta = fechaHasta;
	        Rango = "HORA-NOW";
	        Texto = TextosVisorActividad[18];

	        $('[id$=divCargando]').css("display", "none");
	        $('[id$=divTabContainer]').css("display", "block");
	    }
	    function btnAhora_Click() {
	        OcultarBotonesSeleccion();
	        $('[id$=lblNow]').text(TextosVisorActividad[18]);
	        $('[id$=lblNow]').css("display", "inherit");
	        $('[id$=lblSelectHour]').text(TextosVisorActividad[31]);
	        $('[id$=lblSelectHour]').css("display", "inherit");
	        $('[id$=divSelectHour]').css("display", "inherit");
	    }

	    //Click del boton hoy
	    function btnToday_Click() {
	        //mostrar ventana cargando
	        $('[id$=divCargando]').css("display", "block").css("width", "100px").css("margin", "18% auto");
	        $('[id$=divTabContainer]').css("display", "none");
	        setTimeout(Today_Click, 200);
	    }
	    function Today_Click() {
	        var fechaDesde = new Date();
	        fechaDesde.setHours(0, 0, 0, 0);
	        var fechaHasta = new Date();
	        fechaHasta.setHours(23, 59, 59, 999);
	        if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
	            var entorno = $('[id$=lblCodEntorno]').text();
	        } else {
	            var entorno = $('[id$=ddlEnviroment]').val();
	        }
	        if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
	            var pyme = $('[id$=ddlEnviromentPymes]').val();
	        } else {
	            var pyme = null;
	        }
	        ComprobarUsuariosConectadosRango(fechaDesde, fechaHasta, "DIA", entorno, pyme, 1);
	        var tipoEvento = $('[id$=ddlTipoEvento]').val();
	        var tipoEvento2 = $('[id$=ddlTipoEventoGrid]').val();
	        ComprobarUsuariosActividadReciente(fechaDesde, fechaHasta, entorno, "DIA", tipoEvento, pyme, 1);
	        OcultarBotonesSeleccion();
	        OcultarTextosBotonesSeleccion();
	        ComprobarGridEventos(fechaDesde, fechaHasta, entorno, pyme, tipoEvento2, "DIA");
	        ComprobarGridErrores(fechaDesde, fechaHasta, entorno, pyme);
	        $("#chart").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	        $("#chart2").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	        //marcar como seleccionado
	        $('[id$=divHoy]').css("background-color", "#D3D3D3");
	        $('[id$=divAhora]').css("background-color", "white");
	        $('[id$=divEstaSemana]').css("background-color", "white");
	        $('[id$=divEsteMes]').css("background-color", "white");
	        $('[id$=divEsteAnyo]').css("background-color", "white");
	        fecDesde = fechaDesde;
	        fecHasta = fechaHasta;
	        Rango = "DIA";
	        Texto = TextosVisorActividad[19];

	        $('[id$=divCargando]').css("display", "none");
	        $('[id$=divTabContainer]').css("display", "block");
	    }
	    function btnHoy_Click() {
	        OcultarBotonesSeleccion();
	        $('[id$=lblToday]').text(TextosVisorActividad[19]);
	        $('[id$=lblToday]').css("display", "inherit");
	        $('[id$=lblSelectDay]').text(TextosVisorActividad[27]);
	        $('[id$=lblSelectDay]').css("display", "inherit");
	        $('[id$=dpSelectDay]').css("display", "inherit");
	    }

	    //Click del boton Esta semana
	    function btnThisWeek_Click() {
	        //mostrar ventana cargando
	        $('[id$=divCargando]').css("display", "block").css("width", "100px").css("margin", "18% auto");
	        $('[id$=divTabContainer]').css("display", "none");
	        setTimeout(ThisWeek_Click, 200);
	    }
	    function ThisWeek_Click() {
	        var fechaDesde;
	        var fechaHasta;
	        var currentDate = new Date();
	        if (currentDate.getDay() == 0) {
	            fechaDesde = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - currentDate.getDay() - 6);
	            fechaHasta = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - currentDate.getDay());
	        } else {
	            fechaDesde = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - currentDate.getDay() + 1);
	            fechaHasta = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - currentDate.getDay() + 7);
	        }
	        fechaDesde.setHours(0, 0, 0, 0);
	        fechaHasta.setHours(23, 59, 59, 999);
	        if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
	            var entorno = $('[id$=lblCodEntorno]').text();
	        } else {
	            var entorno = $('[id$=ddlEnviroment]').val();
	        }
	        if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
	            var pyme = $('[id$=ddlEnviromentPymes]').val();
	        } else {
	            var pyme = null;
	        }
	        ComprobarUsuariosConectadosRango(fechaDesde, fechaHasta, "SEMANA", entorno, pyme, 0);
	        var tipoEvento = $('[id$=ddlTipoEvento]').val();
	        var tipoEvento2 = $('[id$=ddlTipoEventoGrid]').val();
	        ComprobarUsuariosActividadReciente(fechaDesde, fechaHasta, entorno, "SEMANA", tipoEvento, pyme, 0);
	        ComprobarGridEventos(fechaDesde, fechaHasta, entorno, pyme, tipoEvento2, "SEMANA");
	        ComprobarGridErrores(fechaDesde, fechaHasta, entorno, pyme);
	        $("#chart").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	        $("#chart2").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	        OcultarBotonesSeleccion();
	        OcultarTextosBotonesSeleccion();
	        //marcar como seleccionado
	        $('[id$=divHoy]').css("background-color", "white");
	        $('[id$=divAhora]').css("background-color", "white");
	        $('[id$=divEstaSemana]').css("background-color", "#D3D3D3");
	        $('[id$=divEsteMes]').css("background-color", "white");
	        $('[id$=divEsteAnyo]').css("background-color", "white");
	        fecDesde = fechaDesde;
	        fecHasta = fechaHasta;
	        Rango = "SEMANA";
	        Texto = TextosVisorActividad[20];
	        $('[id$=divCargando]').css("display", "none");
	        $('[id$=divTabContainer]').css("display", "block");
	    }
	    function btnEstaSemana_Click() {
	        OcultarBotonesSeleccion();
	        $('[id$=lblThisWeek]').text(TextosVisorActividad[20]);
	        $('[id$=lblThisWeek]').css("display", "inherit");
	        $('[id$=lblSelectWeek]').text(TextosVisorActividad[28]);
	        $('[id$=lblSelectWeek]').css("display", "inherit");
	        $('[id$=dpSelectWeek]').css("display", "inherit");
	    }

	    //Click del boton Este mes
	    function btnThisMonth_Click() {
	        //mostrar ventana cargando
	        $('[id$=divCargando]').css("display", "block").css("width", "100px").css("margin", "18% auto");
	        $('[id$=divTabContainer]').css("display", "none");
	        setTimeout(ThisMonth_Click, 500);

	    }
	    function ThisMonth_Click() {
	        var currentDate = new Date();
	        var firstDayOfMonth = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
	        var lastDayOfMonth = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0, 23, 59, 59);
	        if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
	            //var entorno = $('[id$=lblCodEntorno]').text();
	            var entorno = $('[id$=lblCodEntorno]').text();
	        } else {
	            var entorno = $('[id$=ddlEnviroment]').val();
	        }
	        if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
	            var pyme = $('[id$=ddlEnviromentPymes]').val();
	        } else {
	            var pyme = null;
	        }
	        ComprobarUsuariosConectadosRango(firstDayOfMonth, lastDayOfMonth, "MES", entorno, pyme, 0);
	        var tipoEvento = $('[id$=ddlTipoEvento]').val();
	        var tipoEvento2 = $('[id$=ddlTipoEventoGrid]').val();
	        ComprobarUsuariosActividadReciente(firstDayOfMonth, lastDayOfMonth, entorno, "MES", tipoEvento, pyme, 0);
	        ComprobarGridEventos(firstDayOfMonth, lastDayOfMonth, entorno, pyme, tipoEvento2, "MES");
	        ComprobarGridErrores(firstDayOfMonth, lastDayOfMonth, entorno, pyme);
	        OcultarBotonesSeleccion();
	        OcultarTextosBotonesSeleccion();
	        $("#chart").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	        $("#chart2").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	        //marcar como seleccionado
	        $('[id$=divHoy]').css("background-color", "white");
	        $('[id$=divAhora]').css("background-color", "white");
	        $('[id$=divEstaSemana]').css("background-color", "white");
	        $('[id$=divEsteMes]').css("background-color", "#D3D3D3");
	        $('[id$=divEsteAnyo]').css("background-color", "white");
	        fecDesde = firstDayOfMonth;
	        fecHasta = lastDayOfMonth;
	        Rango = "MES";
	        Texto = TextosVisorActividad[21];
	        $('[id$=divCargando]').css("display", "none");
	        $('[id$=divTabContainer]').css("display", "block");
	    }
	    function btnEsteMes_Click() {
	        OcultarBotonesSeleccion();
	        $('[id$=lblThisMonth]').text(TextosVisorActividad[21]);
	        $('[id$=lblSelectMonth]').text(TextosVisorActividad[29]);
	        $('[id$=lblThisMonth]').css("display", "inherit");
	        $('[id$=lblSelectMonth]').css("display", "inherit");
	        $('[id$=pnlSelectMonth]').css("display", "inherit");
	        $('[id$=divSeleccionMes]').css("display", "inherit");
	        $('[id$=ddlSelectMonth]').css("display", "inherit");
	        $('[id$=ddlSelectYear]').css("display", "inherit");
	    }

	    //Click del boton Este año
	    function btnThisYear_Click() {
	        //mostrar ventana cargando
	        $('[id$=divCargando]').css("display", "block").css("width", "100px").css("margin", "18% auto");
	        $('[id$=divTabContainer]').css("display", "none");
	        setTimeout(ThisYear_Click, 1000);
	    }
	    function ThisYear_Click() {
	        var currentDate = new Date();
	        if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
	            var entorno = $('[id$=lblCodEntorno]').text();
	        } else {
	            var entorno = $('[id$=ddlEnviroment]').val();
	        }
	        if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
	            var pyme = $('[id$=ddlEnviromentPymes]').val();
	        } else {
	            var pyme = null;
	        }
	        var fechaDesde = new Date(currentDate.getFullYear(), 0, 1);
	        var fechaHasta = new Date(currentDate.getFullYear(), 11, 31);
	        fechaHasta.setHours(23, 59, 59, 999);
	        ComprobarUsuariosConectadosRango(fechaDesde, fechaHasta, "ANYO", entorno, pyme, 0);
	        var tipoEvento = $('[id$=ddlTipoEvento]').val();
	        var tipoEvento2 = $('[id$=ddlTipoEventoGrid]').val();
	        ComprobarUsuariosActividadReciente(fechaDesde, fechaHasta, entorno, "ANYO", tipoEvento, pyme, 0);
	        ComprobarGridEventos(fechaDesde, fechaHasta, entorno, pyme, tipoEvento2, "ANYO");
	        ComprobarGridErrores(fechaDesde, fechaHasta, entorno, pyme);
	        OcultarBotonesSeleccion();
	        OcultarTextosBotonesSeleccion();
	        $("#chart").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	        $("#chart2").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	        //marcar como seleccionado
	        $('[id$=divHoy]').css("background-color", "white");
	        $('[id$=divAhora]').css("background-color", "white");
	        $('[id$=divEstaSemana]').css("background-color", "white");
	        $('[id$=divEsteMes]').css("background-color", "white");
	        $('[id$=divEsteAnyo]').css("background-color", "#D3D3D3");
	        fecDesde = fechaDesde;
	        fecHasta = fechaHasta;
	        Rango = "ANYO";
	        Texto = TextosVisorActividad[22];
	        $('[id$=divCargando]').css("display", "none");
	        $('[id$=divTabContainer]').css("display", "block");
	    }
	    function btnEsteAnyo_Click() {
	        OcultarBotonesSeleccion();
	        $('[id$=lblThisYear]').text(TextosVisorActividad[22]);
	        $('[id$=lblSelectYear]').text(TextosVisorActividad[30]);
	        $('[id$=lblThisYear]').css("display", "inherit");
	        $('[id$=lblSelectYear]').css("display", "inherit");
	        $('[id$=divSeleccionAnyo]').css("display", "inherit");
	        $('[id$=ddlSelectYearAnyo]').css("display", "inherit");
	    }

	    //Clicks de las distintas horas
	    function Horas(h1, h2) {
	        //mostrar ventana cargando
	        $('[id$=divCargando]').css("display", "block").css("width", "100px").css("margin", "18% auto");
	        $('[id$=divTabContainer]').css("display", "none");
	        setTimeout(function () { CalculoHoras(h1, h2); }, 200);
	    }
	    function CalculoHoras(h1, h2) {
	        var d = new Date();
	        d.setHours(h1, 0, 0, 0);
	        var d2 = new Date();
	        d2.setHours(h2, 0, 0, 0);
	        if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
	            var entorno = $('[id$=lblCodEntorno]').text();
	        } else {
	            var entorno = $('[id$=ddlEnviroment]').val();
	        }
	        if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
	            var pyme = $('[id$=ddlEnviromentPymes]').val();
	        } else {
	            var pyme = null;
	        }
	        ComprobarUsuariosConectadosRango(d, d2, "HORA", entorno, pyme, 1);
	        var tipoEvento = $('[id$=ddlTipoEvento]').val();
	        var tipoEvento2 = $('[id$=ddlTipoEventoGrid]').val();
	        ComprobarUsuariosActividadReciente(d, d2, entorno, "HORA", tipoEvento, pyme, 1);
	        ComprobarGridEventos(d, d2, entorno, pyme, tipoEvento2, "HORA");
	        ComprobarGridErrores(d, d2, entorno, pyme);
	        OcultarBotonesSeleccion();
	        OcultarTextosBotonesSeleccion();
	        $('[id$=lblAhora]').text(h1 + ":00 " + h2 + ":00"); //para darle el valor de la hora al boton
	        $('[id$=lblAhora]').css("font-size", "10px");
	        $("#chart").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	        $("#chart2").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	        //marcar como seleccionado
	        $('[id$=divHoy]').css("background-color", "white");
	        $('[id$=divAhora]').css("background-color", "#D3D3D3");
	        $('[id$=divEstaSemana]').css("background-color", "white");
	        $('[id$=divEsteMes]').css("background-color", "white");
	        $('[id$=divEsteAnyo]').css("background-color", "white");
	        fecDesde = d;
	        fecHasta = d2;
	        Rango = "HORA";
	        Texto = h1 + ":00 " + h2 + ":00";
	        $('[id$=divCargando]').css("display", "none");
	        $('[id$=divTabContainer]').css("display", "block");
	    }

	    //Muestra el calendario para seleccionar una semana.
	    $(function () {
	        var startDate;
	        var endDate;

	        var selectCurrentWeek = function () {
	            window.setTimeout(function () {
	                $('.week-picker').find('.ui-datepicker-current-day a').addClass('ui-state-active')
	            }, 1);
	        }
	        $('.week-picker').datepicker({
	            showOtherMonths: true,
	            selectOtherMonths: true,
	            onSelect: function (dateText, inst) {
	                var date = $(this).datepicker('getDate');
	                if (date.getDay() == 0) {
	                    startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() - 6);
	                    endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
	                } else {
	                    startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 1);
	                    endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 7);
	                }
	                var dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
	                $('#startDate').text($.datepicker.formatDate(dateFormat, startDate, inst.settings));
	                $('#endDate').text($.datepicker.formatDate(dateFormat, endDate, inst.settings));
	                selectCurrentWeek();

	                var fechaDesde = $.datepicker.formatDate(dateFormat, startDate, inst.settings);
	                var fechaHasta = $.datepicker.formatDate(dateFormat, endDate, inst.settings);
	                //mostrar ventana cargando
	                $('[id$=divCargando]').css("display", "block").css("width", "100px").css("margin", "18% auto");
	                $('[id$=divTabContainer]').css("display", "none");
	                setTimeout(function () { Week(startDate, endDate, fechaDesde, fechaHasta); }, 200);
	            },
	            beforeShowDay: function (date) {
	                var cssClass = '';
	                if (date >= startDate && date <= endDate)
	                    cssClass = 'ui-datepicker-current-day';
	                return [true, cssClass];
	            },
	            onChangeMonthYear: function (year, month, inst) {
	                selectCurrentWeek();
	            }
	        });

	        $('.week-picker .ui-datepicker-calendar tr').live('mousemove', function () { $(this).find('td a').addClass('ui-state-hover'); });
	        $('.week-picker .ui-datepicker-calendar tr').live('mouseleave', function () { $(this).find('td a').removeClass('ui-state-hover'); });

	        function Week(startDate, endDate, fechaDesde, fechaHasta) {
	            if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
	                var entorno = $('[id$=lblCodEntorno]').text();
	            } else {
	                var entorno = $('[id$=ddlEnviroment]').val();
	            }
	            if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
	                var pyme = $('[id$=ddlEnviromentPymes]').val();
	            } else {
	                var pyme = null;
	            }
	            ComprobarUsuariosConectadosRango(startDate, endDate, "SEMANA", entorno, pyme, 0)
	            var tipoEvento = $('[id$=ddlTipoEvento]').val();
	            var tipoEvento2 = $('[id$=ddlTipoEventoGrid]').val();
	            ComprobarUsuariosActividadReciente(startDate, endDate, entorno, "SEMANA", tipoEvento, pyme, 0);
	            ComprobarGridEventos(startDate, endDate, entorno, pyme, tipoEvento2, "SEMANA");
	            ComprobarGridErrores(startDate, endDate, entorno, pyme);
	            OcultarBotonesSeleccion();
	            OcultarTextosBotonesSeleccion();
	            $('[id$=lblEstaSemana]').text(fechaDesde + " - " + fechaHasta);
	            //$('[id$=lblEstaSemana]').text(FormatoFecha(fechaDesde) + " - " + FormatoFecha(fechaHasta));
	            $('[id$=lblEstaSemana]').css("font-size", "10px");
	            $("#chart").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	            $("#chart2").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	            //marcar como seleccionado
	            $('[id$=divHoy]').css("background-color", "white");
	            $('[id$=divAhora]').css("background-color", "white");
	            $('[id$=divEstaSemana]').css("background-color", "#D3D3D3");
	            $('[id$=divEsteMes]').css("background-color", "white");
	            $('[id$=divEsteAnyo]').css("background-color", "white");
	            fecDesde = startDate;
	            fecHasta = endDate;
	            Rango = "SEMANA";
	            Texto = fechaDesde + " - " + fechaHasta;

	            $('[id$=divCargando]').css("display", "none");
	            $('[id$=divTabContainer]').css("display", "block");
	        }
	    });

	    $(function () {
	        $("#chkAverages").change(function () {
	            if ($('#chkAverages').prop('checked')) {

	                CrearGraficaUsuariosEnLinea(esNow)
	            } else {

	                CrearGraficaUsuariosEnLineaSinMedias(esNow)
	            }
	            if (ran == "HORA-NOW") {
	                $("#chart").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	            } else if (ran == "HORA") {
	                $("#chart").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	            } else if (ran == "DIA") {
	                $("#chart").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	            } else if (ran == "SEMANA") {
	                $("#chart").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	            } else if (ran == "MES") {
	                $("#chart").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	            } else if (ran == "ANYO") {
	                $("#chart").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	            }
	        });
	    });

	    $(function () {
	        $("#chkAveragesRecentActivity").change(function () {
	            if ($('#chkAveragesRecentActivity').prop('checked')) {

	                CrearGraficaActividadReciente(esNowRecentActivity)
	            } else {

	                CrearGraficaActividadRecienteSinMedias(esNowRecentActivity, "")
	            }
	            if (ran == "HORA-NOW") {
	                $("#chart2").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	            } else if (ran == "HORA") {
	                $("#chart2").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	            } else if (ran == "DIA") {
	                $("#chart2").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	            } else if (ran == "SEMANA") {
	                $("#chart2").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	            } else if (ran == "MES") {
	                $("#chart2").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	            } else if (ran == "ANYO") {
	                $("#chart2").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	            }
	        });
	    });

	    $(function () {
	        $("#lblSelectDay").click(function () {
	            $("#dpSelectDay").focus();
	        });
	    });

	    $(function () {
	        $("#lblSelectWeek").click(function () {
	            $("#dpSelectWeek").focus();
	        });
	    });

	    $('body').click(function (event) {
	        var Ocultar1 = $(event.target).parents('[id^=divAhora]').length == 1;
	        var Ocultar2 = $(event.target).parents('[id^=divHoy]').length == 1;
	        var Ocultar3 = $(event.target).parents('[id^=divEstaSemana]').length == 1;
	        var Ocultar4 = $(event.target).parents('[id^=divEsteMes]').length == 1;
	        var Ocultar5 = $(event.target).parents('[id^=divEsteAnyo]').length == 1;
	        var Ocultar6 = $(event.target).parents('[id^=lblToday]').length == 1;
	        var Ocultar7 = $(event.target).parents('[id^=lblSelectDay]').length == 1;
	        var Ocultar8 = $(event.target).parents('[id^=dpSelectDay]').length == 1;
	        var Ocultar9 = $(event.target).parents('[id^=tdThisDay]').length == 1;
	        var Ocultar10 = $(event.target).parents('[id^=tdThisWeek]').length == 1;
	        var Ocultar11 = $(event.target).parents('[id^=tdThisMonth]').length == 1;
	        var Ocultar12 = $(event.target).parents('[id^=tdThisYear]').length == 1;

	        if (!((Ocultar1) || (Ocultar2) || (Ocultar3) || (Ocultar4) || (Ocultar5) || (Ocultar6) || (Ocultar7) || (Ocultar8) || (Ocultar9) || (Ocultar10) || (Ocultar11) || (Ocultar12))) {
	            OcultarBotonesSeleccion();
	        }
	    });

	    //funcion que se ejecuta en el momento en el que se cambia el valor del contenido del combo de entorno
	    $(function () {
	        $('#<%= ddlEnviroment.ClientID %>').change(function () {
	            //debugger;
	            //mostrar ventana cargando
	            $('[id$=divCargando]').css("display", "block").css("width", "100px").css("margin", "18% auto");
	            $('[id$=divTabContainer]').css("display", "none");
	            setTimeout(CambioEntorno, 200);
	        });
	    });

	    function CambioEntorno() {
	        if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
	            var entorno = $('[id$=lblCodEntorno]').text();
	        } else {
	            var entorno = $('[id$=ddlEnviroment]').val();
	        }
	        if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
	            var pyme = $('[id$=ddlEnviromentPymes]').val();
	        } else {
	            var pyme = null;
	        }
	        if ((Rango == "HORA-NOW") || (Rango == "HORA") || (Rango == "DIA")) {
	            ComprobarUsuariosConectadosRango(fecDesde, fecHasta, Rango, entorno, pyme, 1);
	        } else {
	            ComprobarUsuariosConectadosRango(fecDesde, fecHasta, Rango, entorno, pyme, 0);
	        }
	        var tipoEvento = $('[id$=ddlTipoEvento]').val();
	        var tipoEvento2 = $('[id$=ddlTipoEventoGrid]').val();
	        if ((Rango == "HORA-NOW") || (Rango == "HORA") || (Rango == "DIA")) {
	            ComprobarUsuariosActividadReciente(fecDesde, fecHasta, entorno, Rango, tipoEvento, pyme, 1);
	        } else {
	            ComprobarUsuariosActividadReciente(fecDesde, fecHasta, entorno, Rango, tipoEvento, pyme, 0);
	        }
	        OcultarBotonesSeleccion();
	        OcultarTextosBotonesSeleccion();
	        ComprobarGridEventos(fecDesde, fecHasta, entorno, pyme, tipoEvento2, Rango);
	        ComprobarGridErrores(fecDesde, fecHasta, entorno, pyme);
	        $("#chart").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
	        $("#chart2").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);

	        if (Rango == "DIA") {
	            $('[id$=lblHoy]').text(Texto);
	            $('[id$=divHoy]').css("background-color", "#D3D3D3");
	            $('[id$=divAhora]').css("background-color", "white");
	            $('[id$=divEstaSemana]').css("background-color", "white");
	            $('[id$=divEsteMes]').css("background-color", "white");
	            $('[id$=divEsteAnyo]').css("background-color", "white");
	        } else if ((Rango == "HORA") || (Rango == "HORA-NOW")) {
	            $('[id$=lblAhora]').text(Texto);
	            $('[id$=divHoy]').css("background-color", "white");
	            $('[id$=divAhora]').css("background-color", "#D3D3D3");
	            $('[id$=divEstaSemana]').css("background-color", "white");
	            $('[id$=divEsteMes]').css("background-color", "white");
	            $('[id$=divEsteAnyo]').css("background-color", "white");
	        } else if (Rango == "SEMANA") {
	            $('[id$=lblEstaSemana]').text(Texto);
	            $('[id$=divHoy]').css("background-color", "white");
	            $('[id$=divAhora]').css("background-color", "white");
	            $('[id$=divEstaSemana]').css("background-color", "#D3D3D3");
	            $('[id$=divEsteMes]').css("background-color", "white");
	            $('[id$=divEsteAnyo]').css("background-color", "white");
	        } else if (Rango == "MES") {
	            $('[id$=lblEsteMes]').text(Texto);
	            $('[id$=divHoy]').css("background-color", "white");
	            $('[id$=divAhora]').css("background-color", "white");
	            $('[id$=divEstaSemana]').css("background-color", "#white");
	            $('[id$=divEsteMes]').css("background-color", "#D3D3D3");
	            $('[id$=divEsteAnyo]').css("background-color", "white");
	        } else if (Rango == "ANYO") {
	            $('[id$=lblEsteAnyo]').text(Texto);
	            $('[id$=divHoy]').css("background-color", "white");
	            $('[id$=divAhora]').css("background-color", "white");
	            $('[id$=divEstaSemana]').css("background-color", "white");
	            $('[id$=divEsteMes]').css("background-color", "white");
	            $('[id$=divEsteAnyo]').css("background-color", "#D3D3D3");
	        }
	        $('[id$=divCargando]').css("display", "none");
	        $('[id$=divTabContainer]').css("display", "block");
	    }

	</script>  
	<div id="divCargando" style="width:100%">
		<asp:panel runat="server" id="PanelLoad" HorizontalAlign="Center" style="text-align:center">
			<img id="imgCarga0" runat="server" alt="Loading... Please wait!!" 
			src="images/cargando.gif" /><br /><br />
			<asp:label ID="lblcargando" runat="server" style="margin-left:15px"></asp:label>
			&nbsp;
		</asp:panel>
	</div>
	<div id="divTabContainer" style="display:none">
			<%--<div id="tabs">
				<ul>
					<li><a href="#tabs-1">
						<span id="lblActivityDashboard" class="Texto12" ></span>
					</a></li>
					<li><a href="#tabs-2">
						<span id="lblPerformanceDashboard" class="Texto12"></span>
					</a></li>
					<li><a href="#tabs-3">
						<span id="lblStatisticalInfo" class="Texto12"></span>
					</a></li>
				</ul>    --%>  
				 
						
				<div id="tabs-1" style="width:96%; height:800px; margin-left:25px">
				<div id="panelSuperiorFondo" style="height:60px; border:1px black solid; background-color:#D3D3D3; min-width:940px">
				<div id="panelSuperior" style="clear:both; height:47px; border: 1px black solid; background-color:White; margin-top:5px; margin-bottom:5px; margin-left:5px; margin-right:5px; min-width:915px">
						<div id="divEnviroment" style="float:left; width:27%; margin-left:5px; margin-top:13px; vertical-align:middle">   
							<img src="images/0005.png" alt="ENVIROMENT" style="float:left; margin-top:-5px"/>
							<span id="lblEnviroment" style="font-size:12px; float:left; margin-left:5px"></span>
							<asp:DropDownList ID="ddlEnviroment" runat="server" style="font-size:12px; float:left; margin-left:5px" enabled="true"/>
							<asp:label ID="lblEntorno" runat="server" style="font-size:12px; float:left; margin-left:5px"></asp:label>
							<asp:label ID="lblCodEntorno" runat="server" style="display:none"></asp:label>
						</div>

						<div id="divEnviromentPymes" style="float:left; width:70%; margin-left:5px; vertical-align:middle">
							<span id="lblEnviromentPymes" style="font-size:12px; float:left;  margin-top:13px"></span>&nbsp
							<select id="ddlEnviromentPymes" style="font-size:12px; float:left; margin-left:5px;  margin-top:13px;"></select>
						<table style="float:left; margin-top:3px; margin-left:20px" cellspacing="0" cellpadding="0">

							<tr>
								<td>
									<table id="divAhora" cellspacing="0" cellpadding="2" border="0" style="border:solid 1pt #AAAAAA; cursor:pointer" onclick="btnNow_Click();" >
										<tr>
											<td align="center" valign="middle">
												<img src="Images/va_Now.png" alt="NOW"/>
											</td>
										</tr>
										<tr>
											<td  align="center" nowrap="nowrap" valign="middle">
												<div id="lblAhora" style="font-size:8px ;font:tahoma"/>
											</td>
										</tr>
									</table>                                 
								</td>
								<td>
									<table id="divAhora2"  cellpadding="2" cellspacing="0"> 
										<tr>
											<td align="left" style="border:solid 1pt #AAAAAA; height:31.5px; margin-left:-10px;" onclick="btnAhora_Click(); return false;" >
												<img src="Images/va_desplegable.png" alt="" />
											</td>
										</tr>
									</table>
								</td>

								<td width="30"></td>
								<td>
									<table id="divHoy" cellspacing="0" cellpadding="2" border="0" style="border:solid 1pt #AAAAAA; cursor:pointer" onclick="btnToday_Click();" >
										<tr>
											<td align="center" valign="middle">
												<img src="Images/va_Today.png" alt="TODAY"/>
											</td>                                            
										</tr>
										<tr>
											<td  align="center" nowrap="nowrap" valign="middle" width="30">
												<div id="lblHoy" style="font-size:8px ;font:tahoma"/>
											</td>
										</tr>
									</table> 
								</td>
								<td>
									<table id="divHoy2"  cellpadding="2" cellspacing="0"> 
										<tr>
											<td align="left" style="border:solid 1pt #AAAAAA; height:31.5px; margin-left:-10px;" onclick="btnHoy_Click(); return false;" >
												<img src="Images/va_desplegable.png" alt="" />
											</td>
										</tr>
									</table>
								</td>
								<td width="30"></td>
								<td>
									 <table id="divEstaSemana" cellspacing="0" cellpadding="2" border="0" style="border:solid 1pt #AAAAAA; cursor:pointer" onclick="btnThisWeek_Click();">
										<tr>
											<td align="center" valign="middle">
												<img src="Images/va_Semana.png" alt="WEEK"/>
											</td>
										</tr>
										<tr>
											<td  align="center" nowrap="nowrap" valign="middle">
												<div id="lblEstaSemana" style="font-size:8px ;font:tahoma"/>
											</td>
										</tr>
									 </table> 
								</td>
								<td>
									<table id="divEstaSemana2"  cellpadding="2" cellspacing="0"> 
										<tr>
											<td align="left" style="border:solid 1pt #AAAAAA; height:31.5px; margin-left:-10px;" onclick="btnEstaSemana_Click(); return false;" >
												<img src="Images/va_desplegable.png" alt="" />
											</td>
										</tr>
									</table>
								</td>

								<td width="30"></td>
								<td>
									 <table id="divEsteMes" cellspacing="0" cellpadding="2" border="0" style="border:solid 1pt #AAAAAA; cursor:pointer" onclick="btnThisMonth_Click();" >
										<tr>
											<td align="center" valign="middle">
												<img src="Images/va_mes.png" alt="MONTH"/>
											</td>
										</tr>
										<tr>
											<td  align="center" nowrap="nowrap" valign="middle">
												<div id="lblEsteMes" style="font-size:8px ;font:tahoma"/>
											</td>
										</tr>
									 </table> 
								</td>
								<td>
									<table id="divEsteMes2"  cellpadding="2" cellspacing="0"> 
										<tr>
											<td align="left" style="border:solid 1pt #AAAAAA; height:31.5px; margin-left:-10px;" onclick="btnEsteMes_Click(); return false;" >
												<img src="Images/va_desplegable.png" alt="" />
											</td>
										</tr>
									</table>
								</td>


								<td width="30"></td>
								<td>
									 <table id="divEsteAnyo" cellspacing="0" cellpadding="2" border="0" style="border:solid 1pt #AAAAAA; cursor:pointer" onclick="btnThisYear_Click();" >
										<tr>
											<td align="center" valign="middle">
												<img src="Images/va_Year.png" alt="YEAR" style="width:20px"/>
											</td>
										</tr>
										<tr>
											<td  align="center" nowrap="nowrap" valign="middle">
												<div id="lblEsteAnyo" style="font-size:8px ;font:tahoma"/>
											</td>
										</tr>
									 </table> 
								</td>
								<td>
									<table id="divEsteAnyo2"  cellpadding="2" cellspacing="0"> 
										<tr>
											<td align="left" style="border:solid 1pt #AAAAAA; height:31.5px; margin-left:-10px;" onclick="btnEsteAnyo_Click(); return false;">
												<img src="Images/va_desplegable.png" alt="" />
											</td>
										</tr>
									</table>
								</td>

							</tr>
							
							<tr style="margin-top:5px">
								<td>    
									<span id="lblNow" style="display:none; border:1px solid black; width:91px; position: absolute; background-color:White; font-size:10px; text-align:center; margin-top:-3px; z-index:5; cursor:pointer" onclick="btnNow_Click(); return false"></span><br />
									<span id="lblSelectHour" style="display:none; border:1px solid black; z-index:2; width:91px; position:absolute; background-color: White; font-size:10px;margin-top:-5px; z-index:5"></span>
									<div id="divSelectHour" style="width:90px; height:200px; overflow-y: scroll; padding-top:5px; display:none; margin-top: -5px; margin-left:93px; z-index:5; background-color:White; position:absolute; cursor:pointer">
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('00','01');"> 00:00-01:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('01','02');"> 01:00-02:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('02','03');"> 02:00-03:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('03','04');"> 03:00-04:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('04','05');"> 04:00-05:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('05','06');"> 05:00-06:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('06','07');"> 06:00-07:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('07','08');"> 07:00-08:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('08','09');"> 08:00-09:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('09','10');"> 09:00-10:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('10','11');"> 10:00-11:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('11','12');"> 11:00-12:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('12','13');"> 12:00-13:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('13','14');"> 13:00-14:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('14','15');"> 14:00-15:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('15','16');"> 15:00-16:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('16','17');"> 16:00-17:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('17','18');"> 17:00-18:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('18','19');"> 18:00-19:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('19','20');"> 19:00-20:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('20','21');"> 20:00-21:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('21','22');"> 21:00-22:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('22','23');"> 22:00-23:00 </span>
												<span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('23','00');"> 23:00-00:00 </span>    
									</div>                                
								</td>
								<td width="10px"></td>
								<td width="30px"></td>
								<%--<td style="width:30px; margin-left:30px"></td>--%>
								<td id="tdThisDay">
										<span id="lblSelectDay" style="display:none; border: 1px solid black; width:90px; position:absolute; background-color:White; font-size:10px; margin-top:10px; z-index:5; cursor:pointer"></span>
										<span id="lblToday" style="display:none; border: 1px solid black; width:90px; position:absolute; background-color: White; font-size:10px; margin-top:-3px; z-index:5; cursor:pointer" onclick="btnToday_Click();return false"></span><br />
										<input id="dpSelectDay" class="day-picker" type="text" style="display:none; width:90px; margin-top:-5px; margin-left:92px; position:absolute; cursor:pointer; font-size:10px; z-index:5"/>
						
								</td> 
								<td width="10px"></td>
								<td width="30px"></td>
								<%--<td style="width:30p<%--x; margin-left:30px"></td>--%>
								<td id="tdThisWeek">
										<span id="lblThisWeek" style="display:none; border: 1px solid black; width:105px; position:absolute; background-color:White; font-size:10px; margin-top:-3px; z-index:5; cursor:pointer" onclick="btnThisWeek_Click(); return false"></span><br />
										<span id="lblSelectWeek" style="display:none; border:1px solid black; width:105px; position:absolute; background-color:White; font-size:10px;margin-top:-4px; z-index:5; cursor:pointer"></span>       
										<input id="dpSelectWeek" class="week-picker" type="text" style="display:none; width:90px; margin-top:-4px; margin-left:107px; position:absolute; cursor:pointer; font-size:10px; z-index:5"/>                       
								</td>
								<td width="10px"></td>
								<td width="30px"></td>
								<%--<td style="width:30px; margin-left:30px"></td>--%>
								<td id="tdThisMonth">
										<span id="lblThisMonth" style="display:none; border: 1px solid black; width:90px; position:absolute; background-color:White; font-size:10px; z-index:5; margin-top:-3px; cursor:pointer" onclick="btnThisMonth_Click(); return false"></span><br />
										<span id="lblSelectMonth" style="display:none; border: 1px solid black; width:90px; position:absolute; background-color:White; font-size:10px; margin-top:-4px; z-index:5"></span>
										<div id="divSeleccionMes" style="display:none; margin-top:-4px; margin-left:201px; position:absolute; z-index:5">
											<select id="ddlSelectYear" style="display:none; width:80px; cursor:pointer; margin-left:-109px; font-size:10px"></select>
											<select id="ddlSelectMonth" style="display:none; width:80px; cursor: pointer; margin-left:-109px; font-size:10px"></select>
										</div>
								</td>
								<td width="10px"></td>
								<td width="30px"></td>
								<%--<td style="width:30px; margin-left:30px"></td>--%>
								<td id="tdThisYear">
										<span id="lblThisYear" style="display:none; background-color:White; border: 1px solid black; width:90px; position:absolute; font-size:10px; z-index:5; margin-top:-3px; text-align:left; cursor:pointer" onclick="btnThisYear_Click(); return false"></span><br />
										<span id="lblSelectYear" style="display:none; background-color:White; border: 1px solid black; width:90px; position:absolute; font-size:10px; margin-top:-4px; z-index:5; text-align:left"></span> 
										<div id="divSeleccionAnyo" style="display:none; margin-left:211.5px; margin-top:-4px; position:absolute; z-index:5">
											<select id="ddlSelectYearAnyo" style="display:none; cursor:pointer; margin-left:-119px; font-size:10px"></select>
										</div>  
								</td>
							</tr>
						</table>   
						</div>                                         
					</div>
					</div>
				   
					<div id="panelMedio" style="height:350px"  onclick="OcultarBotonesSeleccion(); return true;">
						<div id="panelMedioFondoIzda" style="background-color:#D3D3D3; border:1px black solid; margin-top:10px; width:49%; float:left">
							<div id="panelMedioIzdaCabecera" style="background-color:White; border:1px black solid; height:30px; margin-top:5px; margin-bottom:5px; margin-left:5px; margin-right:5px; position:relative; z-index:3">
								<img id="imgOnlineUsers" style="margin-top:3px; float:left; margin-left:5px" alt="Online Users" src="images/0003.png"/>
								<div id="divlblOnlineUsers" style="margin-top:7px; margin-left:5px; float:left">
									<span id="lblOnlineUsers"></span>
								</div>
							</div>
							<div id="panelMedioIzda" style="background-color:White; border:1px black solid; height:290px">
								<div id="average" style="float:left;margin-top:10px;width:100%; z-index:5">
									<input id="chkAverages" type="checkbox"/>
									<span id="lblIncludeAverages" style="font-size:12px"></span>
								</div>
								<div id="chart" style="float:left; margin-top:5px; min-height:160px; margin-left:5px; width:60%" ></div>
								<div id="legend" style="width: 33%; min-height:60px; float:right; display:block; margin-top:5px; margin-right:10px"></div> 
								<div id="gridOnlineUsers" style="width:33%; float:right; display:block; margin-top:30px; margin-right:10px"></div>
							</div> 
						</div>

						<div id="panelMedioFondoDcha" style="background-color:#D3D3D3; border:1px black solid; margin-top:10px; width:49%; float:right">
							<div id="panelMedioDchaCabecera" style="background-color:White; border:1px black solid; height:30px; margin-top:5px; margin-bottom:5px; margin-left:5px;  margin-right:5px">
								<img id="imgRecentActivity" style="margin-top:3px; float:left; margin-left:5px" alt="Recent Activity" src="images/0004.png" />
								<div id="divlblRecentActivity" style="margin-top:7px; margin-left:5px; float:left">
									<span id="lblRecentActivity"></span>
								</div>
							</div>
							<div id="panelMedioDcha" style="background-color:White; border:1px black solid; height:290px; ">
								<div id="divOffersReceived" style="width:98%; margin-left:10px; margin-top:8px; margin-bottom:-5px">
									<asp:DropDownList id="ddlTipoEvento" runat="server" style="width:260px; font-size:12px"></asp:DropDownList>
									<input id="chkAveragesRecentActivity" type="checkbox"   style="margin-left:10px" />
									<span id="lblIncludeAveragesRecentActivity" style="font-size:12px"></span>
								</div>
								<div id="chart2" style="float:left; margin-top:10px; min-height:160px; margin-left:5px; width:77%" ></div>
								<div id="gridRecentActivity" style="float:right; margin-top:10px; margin-right:10px; width:15%; min-height:160px"></div>
							</div> 
						</div>

					</div>
					<div id="panelInferior" style="height:380px" onclick="OcultarBotonesSeleccion(); return false;">
						<div id="panelInferiorFondoIzda"  style="background-color:#D3D3D3; border:1px black solid; margin-top:10px; width:49%; float:left">
							<div id="panelInferiorIzdaCabecera" style="background-color:White; border:1px black solid; height:30px; margin-top:5px; margin-bottom:5px; margin-left:5px;  margin-right:5px">
								<img id="imgEventos" style="margin-top:3px; float:left; margin-left:5px" alt="Events" src="images/0006.png"/>
								<div id="divlblEventos" style="margin-top:7px; margin-left:5px; float:left">
									<span id="lblEventos"></span>
								</div>
							</div>
							<div id="panelInferiorIzda" style="border:1px black solid; height:310px">
								 <div id="divTipoEventos" style="float:left; height:28px; width:100%; background-color:White">
									<asp:DropDownList id="ddlTipoEventoGrid" runat="server" style="font-size:12px; margin-left:10px; margin-top:8px"></asp:DropDownList>
								 </div>
								 <div id="divEventos" style="overflow-y:scroll; height:282px; margin-top:28px; overflow-x:hidden; position:relative;background-color:White"} >    
									<table id="gridEvent" style="width:100%"></table>                                            
								</div>                                
							</div> 
						</div>
						<div id="panelInferiorFondoDcha" style="background-color:#D3D3D3; border:1px black solid; margin-top:10px; width:49%; float:right">
							 <div id="panelInferiorDchaCabecera" style="background-color:White; border:1px black solid; height:30px; margin-top:5px; margin-bottom:5px; margin-left:5px;  margin-right:5px">
								<img id="imgErrores" style="margin-top:3px; float:left; margin-left:5px" alt="Errors" src="images/0002.png"/>
								<div id="divlblErrores" style="margin-top:7px; margin-left:5px; float:left">
									<span id="lblErrores"></span>
								</div>
							</div>
							 <div id="panelInferiorDcha" style="border:1px black solid; height:310px">
								<div id="divTipoErrores" style="float:left; height:28px; width:100%;background-color:White">
								</div>
								<div id="divErrores" style="overflow-y:scroll; height:282px; margin-top:28px; background-color:White">
									<table id="gridError" style="width:100%"></table>
								</div> 
								<div id="errorInfo" class="controlPanelEvent oculto" data-scroll="true" style="margin-left:-22%; position:absolute; width:20%; padding: 5px 10px; background-color: #FFFFCC; border:1px solid black; z-index:10">
									<img src="images/cerrar.png" alt="X" style="float:right; width:15px; height:15px; border-top:1px solid black; border-right: 1px solid black; border-left: 1px solid black; margin-top:-20px; margin-right:-11px; background-color:White; cursor:pointer" onclick="ErrorInfoClose(this); return false"/>
								</div>
							</div>
						</div>
				   </div>
				</div>  
			   <%-- <div id="tabs-2">                        
				</div>
				<div id="tabs-3">
				</div>
			 </div>--%>
		</div>
</asp:Content>