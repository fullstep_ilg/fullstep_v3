﻿<%@ Page  Debug="true" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master" CodeBehind="VisorQlik.aspx.vb" Inherits="Fullstep.FSNWeb.VisorQlik"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--Comentamos esta cabecera porque si no, en IE no podemos embeber la app en el iframe por error con el require.js de Qlik--%>
    <%--<meta http-equiv="X-UA-Compatible" content="IE=9"/>--%>

    <link rel="stylesheet" href="css/jquery-ui.css" />

    <link href="css/infragistics.theme.css" rel="stylesheet" />
	<link href="css/infragistics.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server" style="height:auto">

    <script type="text/javascript">

        function MostrarAvisoEnPanelApps(Aviso) {
            $('[id$=pnlQlikApps]').html('<div id="divViewer" style="clear:both; float:left; width:100%; padding-top:15%; display:table;"><span style="display:table-cell; text-align:center; vertical-align:middle; font-size:20px;">' + Aviso + '</span></div>');
            $('[id$=pnlQlikViewer]').hide();
            $('[id$=pnlQlikApps]').show();
            $('[id$=Contenido]').height(($('[id$=pnlQlikApps]').height() + 30));
        }

        function CargarQlikAppEnPanelVisor(IdFSNQlikApp) {
            var to = rutaFS + 'BI/VisorQlik.aspx/CargarQlikAppEnPanelVisor';
            $.when($.ajax({
                type: 'POST',
                url: to,
                data: JSON.stringify({ IdFSNQlikApp: IdFSNQlikApp }),
                contentType: 'application/json;charset=utf-8',
                dataType: 'json',
                async: false
            })).done(function (msg) {
                if (msg.d.length == 2) {
                    switch(msg.d[0]['resultado']) {
                        case 'success':
                            $('[id$=iframeQlikApp]').attr('src', msg.d[1]['cadena']);
                            $('[id$=pnlQlikViewer]').show();
                            $('[id$=pnlQlikApps]').hide();
                            $('[id$=Contenido]').height(($('[id$=pnlQlikViewer]').height() + 30));
                            break;
                        case 'error':
                            MostrarAvisoEnPanelApps(TextosVisorQlik[parseInt(msg.d[1]['cadena'])]);
                            break;
                        default:
                            $('[id$=pnlQlikViewer]').hide();
                            $('[id$=pnlQlikApps]').hide();
                    }
                }
            });
        }

    </script>

    <div style="clear:both; float:left; width:100%;">
        <fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
    </div>

    <asp:Panel id="pnlQlikViewer" ClientIDMode="Static" runat="server" style="clear:both; float:left; width:100%; overflow-y:auto; padding-bottom:5px; text-align:center;">
        <iframe id="iframeQlikApp" src="" style="width:100%; height:100%; overflow-y:auto; border:0; min-height:800px; margin:auto;"></iframe>
    </asp:Panel>
    <asp:Panel id="pnlQlikApps" ClientIDMode="Static" runat="server" style="clear:both; float:left; width:100%; overflow-y:auto; padding-bottom:5px; text-align:center;">
    </asp:Panel>

</asp:Content>


