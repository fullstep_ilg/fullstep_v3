﻿Imports Fullstep.FSNServer
Imports Infragistics.Web.UI.GridControls
Imports Infragistics.Web.UI.GridControls.WebExcelExporter
Imports Infragistics.Documents.Reports.Report
Imports System.Web.Script.Serialization
Imports System.Net

Public Class Cubos
    Inherits FSNPage

    Private _pagerControl As wucPagerControl
    Private _pageNumber As Integer = 0
    Private _dsCubos As DataSet
    Private _dsDenominacionCubos As DataSet

    Friend mDBServer As Root
    Friend Function DBServer() As Root
        If mDBServer Is Nothing Then
            mDBServer = New Root
        End If
        Return mDBServer
    End Function
    ''' <summary>
    ''' Devuelve los cubos
    ''' </summary>
    ''' <returns>Dataset con los cubos</returns>
    ''' <remarks>Llamada desde: CargarCubos, Tiempo maximo: 0,2</remarks>
    Protected ReadOnly Property Cubos() As DataSet
        Get
            Dim bOrdenacionEnBDVacio As Boolean = False
            If CType(hFiltered.Value, Boolean) Then
                whgCubos.GridView.Behaviors.Paging.PageIndex = _pageNumber
                Dim dsAuxiliar As New DataSet
                dsAuxiliar = CType(Cache("dsCubos_" & FSNUser.Cod), DataSet).Copy
                _dsCubos = New DataSet()
                _dsCubos.Tables.Add(dsAuxiliar.DefaultViewManager.DataViewSettings("CUBOS").Table.DefaultView.ToTable.Copy)


                'Cargar La ordenacion del Filtro (por BBDD) 
                'o la de por defecto (por cookie o por "fecha de solicitud de más a menos recientes" sino hay cooki)
                Dim sOrdenacion As String = "FEC_SOLICITUD DESC"
                If hOrdered.Value = "" Then
                    If Session("BIFiltroCubo") = "" Then
                        Dim cookie As HttpCookie
                        cookie = Request.Cookies("ORDENACION_BI_CUBOS")
                        If Not cookie Is Nothing Then
                            sOrdenacion = cookie.Value
                            bOrdenacionEnBDVacio = (sOrdenacion = "")
                        End If
                    End If
                    If InStr(sOrdenacion, "ID") = 0 Then sOrdenacion = IIf(sOrdenacion = "", "ID ASC", sOrdenacion & ",ID ASC")
                    hOrdered.Value = sOrdenacion
                Else
                    sOrdenacion = hOrdered.Value
                    bOrdenacionEnBDVacio = False
                End If

                _pageNumber = IIf(HttpContext.Current.Session("QApageNumberCubos") = "", 0, HttpContext.Current.Session("QApageNumberCubos"))

                Dim _pageSize As Integer = System.Configuration.ConfigurationManager.AppSettings("PageSize")


            End If



            whgCubos.GridView.Behaviors.Paging.PageIndex = _pageNumber
            Dim listaCubos As Fullstep.FSNServer.Cubos = FSNServer.Get_Object(GetType(Fullstep.FSNServer.Cubos))
            listaCubos.FullLoad(FSNUser.IdiomaCod)
            _dsCubos = listaCubos.data
            Dim PKCol(0) As DataColumn
            PKCol(0) = _dsCubos.Tables(0).Columns("ID")
            _dsCubos.Tables(0).PrimaryKey = PKCol
            _dsCubos.Tables(0).TableName = "CUBOS"
            Dim sortedColumns() As String = Split(hOrdered.Value, ",")
            Dim groupedColumns() As String = Split(Session("ColsGroupBy" & Session("BIFiltroCubo")), "#")

            Dim sItemSinOrder As String
            Dim sGrupoSinOrder As String
            Dim bNoOrdenar As Boolean

            For Each item As String In sortedColumns
                If Not item = "" Then
                    sItemSinOrder = Trim(Replace(Replace(UCase(item), " ASC", ""), " DESC", ""))

                    bNoOrdenar = False
                    'Comprueba q las columnas por las q ordenar no esten en las columnas a agrupar o el SortedColumns.Add fallara
                    For Each sGrupoConOrder As String In groupedColumns
                        sGrupoSinOrder = Trim(Replace(Replace(UCase(sGrupoConOrder), " ASC", ""), " DESC", ""))
                        If sGrupoSinOrder = sItemSinOrder Then
                            bNoOrdenar = True
                            Exit For
                        End If
                    Next

                    If Not bNoOrdenar Then
                        If Not bOrdenacionEnBDVacio Then 'Si hay datos creo q siempre esta bien
                            whgCubos.GridView.Behaviors.Sorting.SortedColumns.Add(Split(item, " ")(0), _
                               IIf(Split(item, " ")(1) = "DESC", Infragistics.Web.UI.SortDirection.Descending, Infragistics.Web.UI.SortDirection.Ascending))
                        ElseIf Split(item, " ")(0) = "ID" Then
                            whgCubos.GridView.Behaviors.Sorting.SortedColumns.Add(Split(item, " ")(0), _
                                IIf(Split(item, " ")(1) = "DESC", Infragistics.Web.UI.SortDirection.Descending, Infragistics.Web.UI.SortDirection.Ascending))
                        End If
                    End If
                End If
            Next

            For Each item As String In groupedColumns
                If Not item = "" Then whgCubos.GroupingSettings.GroupedColumns.Add(Split(item, " ")(0), _
                    IIf(Split(item, " ")(1) = "DESC", Infragistics.Web.UI.GridControls.GroupingSortDirection.Descending, Infragistics.Web.UI.GridControls.GroupingSortDirection.Ascending))
            Next

            'para q el hOrdered quede fijado
            upPager.Update()

            Return _dsCubos
        End Get
    End Property
    ''' <summary>
    ''' Al iniciar la pagina establece EnableScriptGlobalization
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub Cert_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ScriptMgr.EnableScriptGlobalization = True
    End Sub
#Region "Inicio"
    ''' <summary>
    ''' Cargar la pagina
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0,2</remarks>
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorCertificados
        CargarIdiomas()
        Dim FSNServer As FSNServer.Root
        If Not IsPostBack Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaQA", "<script>var rutaTheme = '" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "'</script>")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaQA", "<script>var rutaQA = '" & ConfigurationManager.AppSettings("rutaQA") & "'</script>")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaQA2008", "<script>var rutaQA2008 = '" & ConfigurationManager.AppSettings("rutaQA2008") & "'</script>")

            CType(whgCubos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgNuevoCubo"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/nuevo.png"
            CType(whgCubos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgConfigurarCubo"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/comparar.png"
            CType(whgCubos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgEliminarCubo"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/eliminar.png"


            'Session("arrDatosGeneralesCubo") = Nothing

            Seccion = "Cubos"
            Dim mMaster = CType(Me.Master, FSNWeb.Menu)
            mMaster.Seleccionar("Informes", "Cubos")
            Dim mMasterTotal As FSNWeb.Cabecera = CType(mMaster.Master, FSNWeb.Cabecera)
            Dim scriptReference As ScriptReference
            scriptReference = New ScriptReference
            scriptReference.Path = ConfigurationManager.AppSettings("ruta") & "js/jsUpdateProgress.js"
            CType(mMasterTotal.FindControl("ScriptManager1"), ScriptManager).Scripts.Add(scriptReference)

            whgCubos.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize")
            whgCubos.GridView.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize")

            FSNServer = HttpContext.Current.Session("FSN_Server")
            Dim oCubos As FSNServer.Cubos = FSNServer.Get_Object(GetType(FSNServer.Cubos))
            oCubos.FullLoad(FSNUser.IdiomaCod)
            ddlCopiarPermisos.DataSource = oCubos.data
            ddlCopiarPermisos.DataTextField = "DEN"
            ddlCopiarPermisos.DataValueField = "ID"
            ddlCopiarPermisos.DataBind()
        Else
            Dim RecargarGrid As Boolean = True
            Select Case Request("__EVENTTARGET")
                Case btnPager.ClientID
                    _pageNumber = CType(Request("__EVENTARGUMENT"), Integer)
                    Session("QApageNumberCubos") = CType(Request("__EVENTARGUMENT"), String)
                Case btnOrder.ClientID
                    Exit Sub
                Case btnRecargaCache.ClientID
                    RecargarCache()
                    Exit Sub
            End Select

            If RecargarGrid Then CargarCubos()
            Select Case Request("__EVENTTARGET")
                Case Else
                    updGridCubos.Update()
            End Select


        End If

        FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/Cubo.png"

        CType(whgCubos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgPermisos"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/comparar.png"

        Dim ModalProgressClientID As String = CType(Master.Master.FindControl("ModalProgress"), AjaxControlToolkit.ModalPopupExtender).ClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalClientID", "<script>var ModalProgress = '" & ModalProgressClientID & "' </script>")
        FSNServer = HttpContext.Current.Session("FSN_Server")
        'Cargo la denominacion de los cubos en los distintos idiomas( para el FSNTextBoxMultiIdioma
        Dim oCubosDenominacion As FSNServer.CubosDenominacion = FSNServer.Get_Object(GetType(FSNServer.CubosDenominacion))
        oCubosDenominacion.EmptyLoad()
        With txtNombre
            .itemList = oCubosDenominacion.Denominaciones
            .IdiomaSeleccionado = FSNUser.IdiomaCod
        End With
        If Request("__EVENTTARGET") = btnUpdateGrid.ClientID Then
            CargarCubos()
        End If

    End Sub
    ''' <summary>
    ''' Carga los textos con el idioma de la pagina
    ''' </summary>
    Private Sub CargarIdiomas()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Cubos

        FSNPageHeader.TituloCabecera = Textos(0) 'Cubos

        whgCubos.GroupingSettings.EmptyGroupAreaText = Textos(1) 'Desplace una columna aquí para agrupar por ese concepto

        CType(whgCubos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lnkNuevoCubo"), Label).Text = Textos(2) 'Nuevo Cubo
        CType(whgCubos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lnkConfigurarCubo"), Label).Text = Textos(3) 'Configurar Cubo
        CType(whgCubos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lnkEliminarCubo"), Label).Text = Textos(4) 'Eliminar Cubo

        CType(whgCubos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPermisos"), Label).Text = Textos(5) 'Permisos

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Notificaciones
        'CType(whgCubos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPage"), Label).Text = Textos(46)
        'CType(whgCubos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblOF"), Label).Text = Textos(48)

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Cubos
        Dim sVariableJavascriptTextosCubos As String = ""
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosCubos") Then
            sVariableJavascriptTextosCubos = "var TextosCubos = new Array();"
            For i As Integer = 0 To 49
                sVariableJavascriptTextosCubos &= "TextosCubos[" & i & "]='" & JSText(Textos(i)) & "';"
            Next
        End If
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosCubos", sVariableJavascriptTextosCubos, True)
    End Sub
    ''' <summary>
    ''' Recarga la cache con los cubos de la BBDD.
    ''' </summary>
    Private Sub RecargarCache()
        Dim _pageSize As Integer = System.Configuration.ConfigurationManager.AppSettings("PageSize")
        CargarCubos()
        Dim PKCol(0) As DataColumn
        PKCol(0) = _dsCubos.Tables(0).Columns("ID")
        _dsCubos.Tables(0).PrimaryKey = PKCol
        _dsCubos.Tables(0).TableName = "CUBOS"
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorCertificados
        _dsCubos.Relations.Clear()
        Me.InsertarEnCache("dsCubos_" & FSNUser.Cod, _dsCubos, CacheItemPriority.BelowNormal)
    End Sub
    ''' <summary>
    ''' Antes de dibujar la pagina carga los cubos de BBDD
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub Cubos_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            CargarCubos()
        End If
    End Sub
#End Region
#Region "Carga de datos"
    ''' <summary>
    ''' Carga el grid con los cubos 
    ''' </summary>
    Private Sub CargarCubos()
        With whgCubos
            .Rows.Clear()
            .DataSource = Cubos
            .GridView.DataSource = .DataSource
            CreacionEdicionColumnasGrid()
            .DataMember = "CUBOS"
            .DataKeyFields = "ID"
            .DataBind()
        End With
        updGridCubos.Update()
    End Sub
    ''' <summary>
    ''' Crea las columnas del grid
    ''' </summary>
    Private Sub CreacionEdicionColumnasGrid()
        whgCubos.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize")
        whgCubos.GridView.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize")

        ObtenerColumnasGrid(whgCubos.UniqueID)
        CrearColumnasGrid(whgCubos.UniqueID)
    End Sub
    ''' <summary>
    ''' Inicializa las columnas del grid con los campos generales
    ''' </summary>
    Private Sub ObtenerColumnasGrid(ByVal gridId As String)
        Dim grid As WebHierarchicalDataGrid = CType(Me.FindControl(gridId), WebHierarchicalDataGrid)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorCertificados

        'NOMBRE DE LAS COLUMNAS DE LA GRID!!!
        Dim camposGrid As New List(Of Infragistics.Web.UI.GridControls.BoundDataField)
        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
        Dim campoCheckGrid As Infragistics.Web.UI.GridControls.BoundCheckBoxField

        campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
        With campoGrid
            .Key = "ID"
            .DataFieldName = "ID"
            .Header.CssClass = "headerNoWrap"
            .Width = Unit.Pixel(100)
            .Hidden = True
            .CssClass = "itemSeleccionable SinSalto"
        End With
        camposGrid.Add(campoGrid)

        campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
        With campoGrid
            .Key = "DEN"
            .DataFieldName = "DEN"
            .Header.CssClass = "headerNoWrap"
            .Width = Unit.Pixel(100)
            .Hidden = False
            .CssClass = "itemSeleccionable SinSalto"
        End With
        camposGrid.Add(campoGrid)

        campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
        With campoGrid
            .Key = "SVC_URL"
            .DataFieldName = "SVC_URL"
            .Header.CssClass = "headerNoWrap"
            .Header.Text = Textos(19)
            .Header.Tooltip = Textos(19)
            .Width = Unit.Pixel(300)
            .Hidden = False
            .CssClass = "itemSeleccionable SinSalto"
        End With
        camposGrid.Add(campoGrid)

        campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
        With campoGrid
            .Key = "SSAS_SERVER"
            .DataFieldName = "SSAS_SERVER"
            .Header.CssClass = "headerNoWrap"
            .Header.Text = Textos(22)
            .Header.Tooltip = Textos(22)
            .Width = Unit.Pixel(100)
            .Hidden = False
            .CssClass = "itemSeleccionable SinSalto"
        End With
        camposGrid.Add(campoGrid)

        campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
        With campoGrid
            .Key = "SSAS_BD"
            .DataFieldName = "SSAS_BD"
            .Header.CssClass = "headerNoWrap"
            .Header.Text = Textos(26)
            .Header.Tooltip = Textos(26)
            .Width = Unit.Pixel(400)
            .Hidden = False
            .CssClass = "itemSeleccionable SinSalto"
        End With
        camposGrid.Add(campoGrid)

        campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
        With campoGrid
            .Key = "SSAS_CUBE"
            .DataFieldName = "SSAS_CUBE"
            .Header.CssClass = "headerNoWrap"
            .Header.Text = Textos(27)
            .Header.Tooltip = Textos(27)
            .Width = Unit.Pixel(100)
            .Hidden = False
            .CssClass = "itemSeleccionable SinSalto"
        End With
        camposGrid.Add(campoGrid)

        campoCheckGrid = New Infragistics.Web.UI.GridControls.BoundCheckBoxField(True)
        With campoCheckGrid
            .Key = "ACTIVO"
            .DataFieldName = "ACTIVO"
            .Header.CssClass = "headerNoWrap"
            .Header.Text = Textos(28)
            .Header.Tooltip = Textos(28)
            .Width = Unit.Pixel(2)
            .Hidden = False
            .CssClass = "itemSeleccionable SinSalto"
        End With
        camposGrid.Add(campoCheckGrid)

        Session("arrDatosGeneralesCubos") = camposGrid

    End Sub
    ''' <summary>
    ''' Crea las columnas del grid con los campos devueltos de los cubos
    ''' </summary>
    Private Sub CrearColumnasGrid(ByVal gridId As String)
        Dim grid As WebHierarchicalDataGrid = CType(Me.FindControl(gridId), WebHierarchicalDataGrid)
        Dim campoGridBoundTemplate As Infragistics.Web.UI.GridControls.BoundDataField
        campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
        Dim campoGridCheckBoundTemplate As Infragistics.Web.UI.GridControls.BoundCheckBoxField
        campoGridCheckBoundTemplate = New Infragistics.Web.UI.GridControls.BoundCheckBoxField


        For Each field As Infragistics.Web.UI.GridControls.BoundDataField In CType(Session("arrDatosGeneralesCubos"), List(Of Infragistics.Web.UI.GridControls.BoundDataField))
            Select Case field.Key
                Case "ID"
                    campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
                    With campoGridBoundTemplate
                        .Key = "ID"
                        .DataFieldName = "ID"
                        .Header.Text = "Id del cubo"
                        .Hidden = True
                    End With
                    If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.Columns.Add(campoGridBoundTemplate)
                        'AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
                    Else
                        grid.Columns(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                    If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.GridView.Columns.Add(campoGridBoundTemplate)
                        'AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
                    Else
                        grid.GridView.Columns.Item(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                Case "DEN"
                    campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
                    With campoGridBoundTemplate
                        .Key = "DEN"
                        .DataFieldName = "DEN"
                        .Header.Text = "Nombre"
                        .Hidden = False
                    End With
                    If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.Columns.Add(campoGridBoundTemplate)
                        'AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
                    Else
                        grid.Columns(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                    If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.GridView.Columns.Add(campoGridBoundTemplate)
                        'AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
                    Else
                        grid.GridView.Columns.Item(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                Case "SVC_URL"
                    campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
                    With campoGridBoundTemplate
                        .Key = "SVC_URL"
                        .DataFieldName = "SVC_URL"
                        .Header.Text = "Url Servicio de datos"
                        .Header.Tooltip = ""
                        .Hidden = False
                    End With
                    If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.Columns.Add(campoGridBoundTemplate)
                        'AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
                    Else
                        grid.Columns(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                    If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.GridView.Columns.Add(campoGridBoundTemplate)
                        'AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
                    Else
                        grid.GridView.Columns.Item(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                Case "SSAS_SERVER"
                    campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
                    With campoGridBoundTemplate
                        .Key = "SSAS_SERVER"
                        .DataFieldName = "SSAS_SERVER"
                        .Header.Text = "Url Servicio SSAS"
                        .Header.Tooltip = ""
                        .Hidden = False
                    End With
                    If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.Columns.Add(campoGridBoundTemplate)
                        'AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
                    End If
                    If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.GridView.Columns.Add(campoGridBoundTemplate)
                        'AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
                    End If
                Case "SSAS_BD"
                    campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
                    With campoGridBoundTemplate
                        .Key = "SSAS_BD"
                        .DataFieldName = "SSAS_BD"
                        .Header.Text = "Base de datos"
                        .Header.Tooltip = ""
                        .Hidden = False
                    End With
                    If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.Columns.Add(campoGridBoundTemplate)
                        'AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
                    End If
                    If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.GridView.Columns.Add(campoGridBoundTemplate)
                        'AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
                    End If

                Case "SSAS_CUBE"
                    campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
                    With campoGridBoundTemplate
                        .Key = "SSAS_CUBE"
                        .DataFieldName = "SSAS_CUBE"
                        .Header.Text = "Cubo"
                        .Header.Tooltip = ""
                        .Hidden = False
                    End With
                    If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.Columns.Add(campoGridBoundTemplate)
                        'AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
                    End If
                    If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.GridView.Columns.Add(campoGridBoundTemplate)
                        'AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
                    End If

                Case "ACTIVO"
                    campoGridCheckBoundTemplate = New Infragistics.Web.UI.GridControls.BoundCheckBoxField
                    With campoGridCheckBoundTemplate
                        .Key = "ACTIVO"
                        .DataFieldName = "ACTIVO"
                        .Header.Text = "ACTIVO"
                        .Header.Tooltip = ""
                        .Hidden = False
                    End With
                    campoGridCheckBoundTemplate.CheckBox.CheckedImageUrl = ConfigurationManager.AppSettings("ruta") & "images\baja.gif"
                    campoGridCheckBoundTemplate.CssClass = "itemCenterAligment"
                    If grid.Columns(campoGridCheckBoundTemplate.Key) Is Nothing Then
                        grid.Columns.Add(campoGridCheckBoundTemplate)
                        ' AnyadirTextoAColumnaNoFiltrada(gridId, campoGridCheckBoundTemplate.Key, False, False)
                    End If
                    If grid.GridView.Columns.Item(campoGridCheckBoundTemplate.Key) Is Nothing Then
                        grid.GridView.Columns.Add(campoGridCheckBoundTemplate)
                        'AnyadirTextoAColumnaNoFiltrada(gridId, campoGridCheckBoundTemplate.Key, False, False)
                    End If
            End Select
        Next

    End Sub
#End Region
#Region "Eventos Grid"
    ''' <summary>
    ''' Tras traer datos (Bound) actualiza el Paginador
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub whgCubos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles whgCubos.DataBound
        'Dim _pageCount As Integer
        'Dim _rows As Integer
        'Dim dt As DataTable = CType(whgCubos.DataSource, DataSet).Tables(0)
        'If CType(hFiltered.Value, Boolean) Then
        '    _rows = dt.Rows.Count
        '    'Else
        '    Dim Tab As FSNWebControls.FSNTab
        '    For index As Integer = 0 To dlFiltrosConfigurados.Items.Count - 1
        '        Tab = CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab)
        '        If Tab.Selected Then
        '            _rows = CType(Tab.Attributes("NumCertificados"), Integer)
        '        End If
        '    Next
        'End If
        '_rows = dt.Rows.Count
        '_pageCount = _rows \ System.Configuration.ConfigurationManager.AppSettings("PageSize")
        '_pageCount = _pageCount + (IIf(_rows Mod System.Configuration.ConfigurationManager.AppSettings("PageSize") = 0, 0, 1))
        'Paginador(_pageCount)
        'updGridCubos.Update()
    End Sub
    ''' <summary>
    ''' Tras ordenar actualiza el grid
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub whgCubos_ColumnSorted(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.SortingEventArgs) Handles whgCubos.ColumnSorted
        Dim sName As String
        Dim sortCriteria As String = String.Empty
        Dim sortDirection As String = String.Empty
        Dim bdatabind As Boolean = False
        Dim sColsGroupBy As String = "ColsGroupBy" & Session("BIFiltroCubo")

        If e.SortedColumns.Count > 0 Then
            e.SortedColumns.Remove(CType(sender, Infragistics.Web.UI.GridControls.ContainerGrid).Behaviors.Sorting.SortedColumns.Item("ID"))
            e.SortedColumns.Add(CType(sender, Infragistics.Web.UI.GridControls.ContainerGrid).Columns.Item("ID"), Infragistics.Web.UI.SortDirection.Ascending)
            For Each sortColumn As Infragistics.Web.UI.GridControls.SortedColumnInfo In e.SortedColumns
                sName = sortColumn.ColumnKey
                sortDirection = IIf(sortColumn.SortDirection = Infragistics.Web.UI.SortDirection.Descending, " DESC", " ASC")
                sortCriteria = IIf(sortCriteria Is String.Empty, sName & sortDirection, sortCriteria & "," & sName & sortDirection)
            Next
        End If
        If Session("BIFiltroCubo") = "" Then
            'Lo almacenamos en cookie
            Dim cookie As HttpCookie
            cookie = Request.Cookies("ORDENACION_BI_CUBOS")
            If cookie Is Nothing Then
                cookie = New HttpCookie("ORDENACION_BI_CUBOS")
            End If
            cookie.Value = sortCriteria
            cookie.Expires = Date.MaxValue
            Response.AppendCookie(cookie)
            'Else
            '    'Lo Almacenamos en BBDD
            '    Dim cFiltro As FiltroQA
            '    cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroQA))
            '    cFiltro.IDFiltroUsuario = Session("BIFiltroCubo")
            '    cFiltro.TipoSol = TiposDeDatos.TipoDeSolicitud.Certificado
            '    cFiltro.Ordenacion = sortCriteria
            '    cFiltro.GuardarOrdenacion()
            '    cFiltro = Nothing
        End If
        hOrdered.Value = sortCriteria
        updGridCubos.Update()
    End Sub
    ''' <summary>
    ''' Tras filtrarse actualiza el grid
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub whgCubos_DataFiltered(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.FilteredEventArgs) Handles whgCubos.DataFiltered
        updGridCubos.Update()
    End Sub
    ''' <summary>
    ''' Tras agrupar actualiza el grid
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub whgCubos_GroupedColumnsChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.GroupedColumnsChangedEventArgs) Handles whgCubos.GroupedColumnsChanged
        Dim sColsGroupBy As String = "ColsGroupBy" & Session("BIFiltroCubo")
        Dim groupedColumnList As String = String.Empty
        For Each column As Infragistics.Web.UI.GridControls.GroupedColumn In e.GroupedColumns
            groupedColumnList = IIf(groupedColumnList Is String.Empty, String.Empty, "#")
            groupedColumnList &= column.ColumnKey & IIf(column.SortDirection = Infragistics.Web.UI.GridControls.GroupingSortDirection.Ascending, " ASC", " DESC")
        Next
        Session(sColsGroupBy) = groupedColumnList
        updGridCubos.Update()
    End Sub
#End Region
#Region "Page Methods"
    ''' <summary>
    ''' Inserta un nuevo Cubo en la BBDD
    ''' </summary>
    ''' <param name="denominaciones">Denominaciones del cubo</param>
    ''' <param name="activo">Cubo activo o no</param>
    ''' <param name="url">Url del cubo</param>
    ''' <param name="usuario">Usuario del cubo</param>
    ''' <param name="password">Contraseña del cubo</param>
    ''' <param name="dominio">Dominio del cubo</param>
    ''' <param name="url2">Url SSAS del cubo</param>
    ''' <param name="usuario2">Usuario SSAS del cubo</param>
    ''' <param name="password2">Contraseña SSAS del cubo</param>
    ''' <param name="dominio2">Dominio SSAS del cubo</param>
    ''' <param name="BD">BD SSAS del cubo</param>
    ''' <param name="cubo">Cubo SSAS</param>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function InsertarCubo(ByVal denominaciones As Dictionary(Of String, String), ByVal url As String, ByVal usuario As String, ByVal password As String, ByVal dominio As String, ByVal url2 As String, ByVal usuario2 As String, ByVal password2 As String, ByVal dominio2 As String, ByVal BD As String, ByVal cubo As String, ByVal activo As String, ByVal availableMeasures As String, ByVal defaultMeasures As String) As String
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")

        Dim oCubos As FSNServer.Cubos = FSNServer.Get_Object(GetType(FSNServer.Cubos))
        Dim oCubosDenominacion As FSNServer.CubosDenominacion = FSNServer.Get_Object(GetType(FSNServer.CubosDenominacion))
        Dim contador As Integer = 0
        Dim key1 As String = ""
        Dim key2 As String = ""
        Dim ValueCorrecto As String = ""
        Try
            If Not (defaultMeasures = "") Then
                defaultMeasures = defaultMeasures.Remove(defaultMeasures.LastIndexOf("|"), 1)
            End If
            If Not (availableMeasures = "") Then
                availableMeasures = availableMeasures.Remove(availableMeasures.LastIndexOf("|"), 1)
            End If
            Dim idCubo = oCubos.Cubos_Insert(url, usuario, password, dominio, url2, usuario2, password2, dominio2, BD, cubo, activo, availableMeasures, defaultMeasures)
            For Each item As KeyValuePair(Of String, String) In denominaciones
                If item.Value = " " Then
                    contador += 1
                    If (key1 = "") Then
                        key1 = item.Key
                    Else
                        key2 = item.Key
                    End If
                Else
                    ValueCorrecto = item.Value
                End If
            Next
            If (contador = 1) Then
                denominaciones.Item(key1) = ValueCorrecto
            ElseIf (contador = 2) Then
                denominaciones.Item(key1) = ValueCorrecto
                denominaciones.Item(key2) = ValueCorrecto
            End If
            If (contador = 3) Then
                Return "sinDenominaciones"
                'Mensaje error Hay que insertar al menos una denominacion'
            Else
                oCubosDenominacion.CubosDenominacion_Insert(idCubo, denominaciones)
            End If
            Return "success"
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Elimina un Cubo de la BBDD
    ''' </summary>
    ''' <param name="idCubo">id Cubo</param>
    <System.Web.Services.WebMethod(True), _
  System.Web.Script.Services.ScriptMethod()> _
    Public Shared Sub EliminarCubo(ByVal idCubo As Integer)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oCubos As FSNServer.Cubos = FSNServer.Get_Object(GetType(FSNServer.Cubos))
        Dim oCubosDenominacion As FSNServer.CubosDenominacion = FSNServer.Get_Object(GetType(FSNServer.CubosDenominacion))
        Try
            oCubosDenominacion.CubosDenominacion_Delete(idCubo)
            oCubos.Cubos_Delete(idCubo)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Inserta un nuevo Cubo en la BBDD
    ''' </summary>
    ''' <param name="denominaciones">Denominaciones del cubo</param>
    ''' <param name="activo">Cubo activo o no</param>
    ''' <param name="url">Url del cubo</param>
    ''' <param name="usuario">Usuario del cubo</param>
    ''' <param name="password">Contraseña del cubo</param>
    ''' <param name="dominio">Dominio del cubo</param>
    ''' <param name="url2">Url SSAS del cubo</param>
    ''' <param name="usuario2">Usuario SSAS del cubo</param>
    ''' <param name="password2">Contraseña SSAS del cubo</param>
    ''' <param name="dominio2">Dominio SSAS del cubo</param>
    ''' <param name="BD">BD SSAS del cubo</param>
    ''' <param name="cubo">Cubo SSAS</param>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function ConfigurarCubo(ByVal denominaciones As Dictionary(Of String, String), ByVal url As String, ByVal usuario As String, ByVal password As String, ByVal dominio As String, ByVal url2 As String, ByVal usuario2 As String, ByVal password2 As String, ByVal dominio2 As String, ByVal BD As String, ByVal cubo As String, ByVal activo As String, ByVal availableMeasures As String, ByVal defaultMeasures As String, ByVal idCubo As String) As String
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oCubos As FSNServer.Cubos = FSNServer.Get_Object(GetType(FSNServer.Cubos))
        Dim oCubosDenominacion As FSNServer.CubosDenominacion = FSNServer.Get_Object(GetType(FSNServer.CubosDenominacion))
        Dim contador As Integer = 0
        Dim key1 As String = ""
        Dim key2 As String = ""
        Dim ValueCorrecto As String = ""

        Try
            If Not (defaultMeasures = "") Then
                defaultMeasures = defaultMeasures.Remove(defaultMeasures.LastIndexOf("|"), 1)
            End If
            If Not (availableMeasures = "") Then
                availableMeasures = availableMeasures.Remove(availableMeasures.LastIndexOf("|"), 1)
            End If

            oCubos.Cubos_Update(idCubo, url, usuario, password, dominio, url2, usuario2, password2, dominio2, BD, cubo, activo, availableMeasures, defaultMeasures)
            For Each item As KeyValuePair(Of String, String) In denominaciones
                If item.Value = " " Then
                    contador += 1
                    If (key1 = "") Then
                        key1 = item.Key
                    Else
                        key2 = item.Key
                    End If
                Else
                    ValueCorrecto = item.Value
                End If
            Next
            If (contador = 1) Then
                denominaciones.Item(key1) = ValueCorrecto
            ElseIf (contador = 2) Then
                denominaciones.Item(key1) = ValueCorrecto
                denominaciones.Item(key2) = ValueCorrecto
            End If
            If (contador = 3) Then
                Return "sinDenominaciones"
                'Mensaje error Hay que insertar al menos una denominacion'
            Else
                oCubosDenominacion.CubosDenominacion_Update(idCubo, denominaciones)
            End If
            'oCubosDenominacion.CubosDenominacion_Delete(idCubo)
            'oCubos.Cubos_Delete(idCubo)
            Return "success"
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Mandamos los datos guardados en cache en formato jSon, y seleccionaremos y cargaremos los datos en la parte de cliente
    ''' Lo serializamos en .net y lo pasamos en formato string para luego parsearlo en cliente.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function Obtener_Datos_Cubo(ByVal idCubo As String) As String
        Try
            Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim serializer As New JavaScriptSerializer()
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oCubos As FSNServer.Cubos = FSNServer.Get_Object(GetType(FSNServer.Cubos))
            Dim oCubo As FSNServer.Cubo = FSNServer.Get_Object(GetType(FSNServer.Cubo))
            Dim dataTable As DataTable = oCubos.Get_Cubo(idCubo).Tables(0)
            oCubo.Id = dataTable.Rows(0).Item("ID")
            oCubo.Svc_Url = dataTable.Rows(0).Item("SVC_URL")
            oCubo.Svc_Usu = dataTable.Rows(0).Item("SVC_USU")
            oCubo.Svc_Pwd = Encrypter.EncriptarSQLtod("", dataTable.Rows(0).Item("SVC_PWD"), False)
            oCubo.Svc_Dom = dataTable.Rows(0).Item("SVC_DOM")
            oCubo.Ssas_Server = dataTable.Rows(0).Item("SSAS_SERVER")
            oCubo.Ssas_Usu = dataTable.Rows(0).Item("SSAS_USU")
            oCubo.Ssas_Pwd = Encrypter.EncriptarSQLtod("", dataTable.Rows(0).Item("SSAS_PWD"), False)
            oCubo.Ssas_Dom = dataTable.Rows(0).Item("SSAS_DOM")
            oCubo.Ssas_BD = dataTable.Rows(0).Item("SSAS_BD")
            oCubo.Ssas_Cube = dataTable.Rows(0).Item("SSAS_CUBE")
            oCubo.Available_Measures = dataTable.Rows(0).Item("AVAILABLE_MEASURES")
            oCubo.Default_Measures = dataTable.Rows(0).Item("DEFAULT_MEASURES")
            oCubo.Mon = dataTable.Rows(0).Item("MON")
            oCubo.Cli = dataTable.Rows(0).Item("CLI")
            oCubo.Activo = dataTable.Rows(0).Item("ACTIVO")
            Return (serializer.Serialize(DirectCast(oCubo, Object)).ToString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Mandamos los datos guardados en cache en formato jSon, y seleccionaremos y cargaremos los datos en la parte de cliente
    ''' Lo serializamos en .net y lo pasamos en formato string para luego parsearlo en cliente.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function Obtener_Datos_Cubo_Den(ByVal idCubo As String) As Object
        Try
            Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim serializer As New JavaScriptSerializer()
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oCubosDenominacion As FSNServer.CubosDenominacion = FSNServer.Get_Object(GetType(FSNServer.CubosDenominacion))
            Dim oCubosDen As Dictionary(Of String, String) = oCubosDenominacion.Load(idCubo)
            Return serializer.Serialize(DirectCast(oCubosDen, Object)).ToString

        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
#Region "Permisos"
    'Permisos
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function UON_Permiso_Cubos() As List(Of FSNServer.cn_fsTreeViewItem)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim obi_Data As FSNServer.bi_Data
        obi_Data = FSNServer.Get_Object(GetType(FSNServer.bi_Data))

        Dim BI_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim oUONs As New List(Of FSNServer.cn_fsTreeViewItem)
        With BI_Usuario
            oUONs = obi_Data.Obtener_UONsPara(.Idioma)
        End With

        Return oUONs
    End Function
    <System.Web.Services.WebMethod(True), _
   System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function GuardarPermisos(ByVal idUsuario As Object, ByVal idCubo As String) As String
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim obi_Data As FSNServer.bi_Data
        obi_Data = FSNServer.Get_Object(GetType(FSNServer.bi_Data))
        For Each idU As String In idUsuario
            Dim subs() As String = idU.Split("-")
            Dim idUsuSubs As String = subs(subs.Length - 1)
            Dim uon2Subs As String = ""
            Dim uon3Subs As String = ""
            Dim uon1Subs As String = ""
            If (idUsuSubs = "null") Then
                obi_Data.GuardarPermisosCuboTodosUON(idCubo)
            Else
                If (subs.Length > 1) Then
                    uon1Subs = subs(1)
                End If
                If (subs.Length > 2) Then
                    uon2Subs = subs(2)
                End If
                If (subs.Length > 3) Then
                    uon3Subs = subs(3)
                End If

                Dim x() As String = idUsuSubs.Split("_")
                Dim idUsu As String = ""
                If ((Not x Is Nothing) And x.Contains("USU")) Then
                    If (x(x.Length - 2) = "adm") Then
                        idUsu = x(x.Length - 2) + "_" + x(x.Length - 1)
                    Else
                        idUsu = x(x.Length - 1)
                    End If
                End If

                Dim y() As String = uon1Subs.Split("_")
                Dim uon1 As String = ""
                If ((Not y Is Nothing) And y.Contains("UON1")) Then
                    uon1 = y(y.Length - 1)
                End If

                Dim z() As String = uon2Subs.Split("_")
                Dim uon2 As String = ""
                If ((Not z Is Nothing) And z.Contains("UON2")) Then
                    uon2 = z(z.Length - 1)
                End If

                Dim w() As String = uon2Subs.Split("_")
                Dim uon3 As String = ""
                If ((Not w Is Nothing) And w.Contains("UON3")) Then
                    uon3 = w(w.Length - 1)
                End If
                If (Not idUsu = "") Or (Not uon1 = "" Or Not uon2 = "" Or Not uon3 = "") Then
                    obi_Data.GuardarPermisosCubo(idCubo, idUsu, uon1, uon2, uon3)
                End If
            End If
        Next
        Return True
    End Function
    <System.Web.Services.WebMethod(True), _
  System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function ObtenerPermisosUON(ByVal idCubo As String) As String()
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim obi_Data As FSNServer.bi_Data
        obi_Data = FSNServer.Get_Object(GetType(FSNServer.bi_Data))
        Dim Usuarios() As String = obi_Data.ObtenerPermisosUON(idCubo)

        Return Usuarios
    End Function
    <System.Web.Services.WebMethod(True), _
System.Web.Script.Services.ScriptMethod()> _
    Public Shared Sub BorrarPermiso(ByVal id As String)
        Dim x() As String = id.Split("_")
        Dim idUsu As String = ""
        If ((Not x Is Nothing) And x.Contains("USU")) Then
            idUsu = x(3)
        End If
        Dim idCubo As String = ""
        If ((Not x Is Nothing) And x.Contains("CUBO")) Then
            idCubo = x(1)
        End If
        Dim uon1 As String = ""
        If ((Not x Is Nothing) And x.Contains("UON1")) Then
            uon1 = x(3)
        End If
        Dim uon2 As String = ""
        If ((Not x Is Nothing) And x.Contains("UON2")) Then
            uon1 = x(5)
        End If
        Dim uon3 As String = ""
        If ((Not x Is Nothing) And x.Contains("UON3")) Then
            uon3 = x(7)
        End If
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim obi_Data As FSNServer.bi_Data
        obi_Data = FSNServer.Get_Object(GetType(FSNServer.bi_Data))
        If (idUsu = "") Then
            obi_Data.EliminarPermisoUON(idCubo, uon1, uon2, uon3)
        Else
            obi_Data.EliminarPermisoUsu(idCubo, idUsu)
        End If
    End Sub
    ''' <summary>
    ''' Comprueba que se pueda acceder al cubo 
    ''' </summary>
    ''' <param name="url2">Url SSAS del cubo</param>
    ''' <param name="usuario2">Usuario SSAS del cubo</param>
    ''' <param name="password2">Contraseña SSAS del cubo</param>
    ''' <param name="dominio2">Dominio SSAS del cubo</param>
    ''' <param name="BD">BD SSAS del cubo</param>
    ''' <param name="cubo">Cubo SSAS</param>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function ComprobarCuboCorrecto(ByVal url2 As String, ByVal usuario2 As String, ByVal password2 As String, ByVal dominio2 As String, ByVal BD As String, ByVal cubo As String) As String
        Dim server As New Microsoft.AnalysisServices.Server()
        Dim server2 As New Microsoft.AnalysisServices.Server()
        Dim database As Microsoft.AnalysisServices.Database
        Dim cube As Microsoft.AnalysisServices.Cube
        Try
            ' Connect to the SSAS server
            server.Connect("Data Source=" & url2 & "; User Id=" & dominio2 & "\" & usuario2 & ";Password=" & password2 & ";")
            ' Get the Database
            database = server.Databases.GetByName(BD)
            ' Get the Cube
            cube = database.Cubes.GetByName(cubo)
            Return "success"
        Catch ex As Exception

        End Try
        Try
            ' Connect to the SSAS server
            server2.Connect("Data Source=" & Replace(LCase(url2), "/olap/", "/_openolap/") & "; User Id=" & dominio2 & "\" & usuario2 & ";Password=" & password2 & ";")
            ' Get the Database
            database = server2.Databases.GetByName(BD)
            ' Get the Cube
            cube = database.Cubes.GetByName(cubo)
            ' Get processDate
        Catch ex As Exception
            Return ex.Message
        End Try
        Return "success"
    End Function
    <System.Web.Services.WebMethod(True), _
   System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function isSiteOnline(url As [String]) As String
        Try
            Dim result As [Boolean] = True
            Dim httpReq As HttpWebRequest = DirectCast(WebRequest.Create(url), HttpWebRequest)
            httpReq.AllowAutoRedirect = False
            Dim httpRes As HttpWebResponse = DirectCast(httpReq.GetResponse(), HttpWebResponse)
            If httpRes.StatusCode <> HttpStatusCode.OK Then
                result = False
            End If
            httpRes.Close()
            If (result = True) Then
                Return "success"
            End If
        Catch ex As Exception
            Return ex.Message
        End Try
        Return "success"
    End Function
    <System.Web.Services.WebMethod(True), _
  System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function CopiarPermisos(idCuboACopiar As Integer, idCubo As Integer) As String
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim obi_Data As FSNServer.bi_Data
            obi_Data = FSNServer.Get_Object(GetType(FSNServer.bi_Data))
            obi_Data.CopiarTodosPermisos(idCuboACopiar, idCubo)
            Return "success"
        Catch ex As Exception
            Return ex.Message
        End Try
        Return "success"
    End Function
#End Region
End Class