﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master" CodeBehind="VisorRendimiento.aspx.vb" Inherits="Fullstep.FSNWeb.VisorRendimiento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=9"/>
    <link type="text/css" href="css/ig.ui.chart.igtheme.transparent.css" rel="stylesheet" />
    <link type="text/css" href="css/jquery-ui.css" rel="stylesheet" />

    <link href="css/infragistics.theme.css" rel="stylesheet" />
    <link href="css/infragistics.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server"> 

    <script id="tooltipTemplateMaxTAcceso" type="text/x-jquery-tmpl">
        <div id="tooltipTemplateMaxTAcceso" class="shadow ui-widget-content ui-corner-all">
            <span id="tooltipValueSerie">ACCESS MAX TIME</span><br/>
            <span id="tooltipValueTime">Time&nbsp: ${item.LabelRecentActivityKPI}</span><br/>
            <span id="tooltipTemplateMaxTAccesoKPI">Value: ${item.ValueMaxTAcceso} ms</span>
        </div>
    </script>

    <script id="tooltipTemplateMinTAcceso" type="text/x-jquery-tmpl">
        <div id="tooltipTemplateMinTAcceso" class="shadow ui-widget-content ui-corner-all">
            <span id="tooltipValueSerie">ACCESS MIN TIME</span><br/>
            <span id="tooltipValueTime">Time&nbsp: ${item.LabelRecentActivityKPI}</span><br/>
            <span id="tooltipTemplateMinTAccesoKPI">Value: ${item.ValueMinTAcceso} ms</span>
        </div>
    </script>

    <script id="tooltipTemplateAvgTAcceso" type="text/x-jquery-tmpl">
        <div id="tooltipTemplateAvgTAcceso" class="shadow ui-widget-content ui-corner-all">
            <span id="tooltipValueSerie">ACCESS AVG TIME</span><br/>
            <span id="tooltipValueTime">Time&nbsp: ${item.LabelRecentActivityKPI}</span><br/>
            <span id="tooltipTemplateAvgTAccesoKPI">Value: ${item.ValueAvgTAcceso} ms</span>
        </div>
    </script>

    <script id="tooltipTemplateTarget" type="text/x-jquery-tmpl">
        <div id="tooltipTemplateTarget" class="shadow ui-widget-content ui-corner-all">
            <span id="tooltipValueSerie">ACCESS TIME TARGET</span><br/>
            <span id="tooltipValueTime">Time&nbsp: ${item.LabelRecentActivityKPI}</span><br/>
            <span id="tooltipTemplateTargetKPI">Value: ${item.ValueTarget} ms</span>
        </div>
    </script>

    <script id="tooltipTemplatePerformanceEvolution" type="text/x-jquery-tmpl">
        <div id="tooltipTemplatePerformanceEvolution" class="shadow ui-widget-content ui-corner-all">
            <span id="tooltipValueSerie">PERFORMANCE</span><br/>
            <span id="tooltipValueTime">Time&nbsp: ${item.LabelPerformanceEvolution}</span><br/>
            <span id="tooltipTemplatePerformanceEvolution">Value: ${item.ValuePerformanceEvolution} %</span>
        </div>
    </script>

    <script id="tooltipTemplateValueAVG" type="text/x-jquery-tmpl">
        <div id="tooltipTemplateValueAVG" class="shadow ui-widget-content ui-corner-all">
            <span id="tooltipValueSerie">PERFORMANCE AVG</span><br/>
            <span id="tooltipValueTime">Time&nbsp: ${item.LabelPerformanceEvolution}</span><br/>
            <span id="tooltipTemplateValueAVG">Value: ${item.ValueAVG} %</span>
        </div>
    </script>

    <style>
    .oculto
    {
        display: none !important;
    }
    </style>

    <style type="text/css">
        .tooltip-options
        {
            padding: 10px;
            background-color: #EBEBEB;
            width:100px;
        }
        .tooltip-grid-descr
        {
            overflow: hidden;
            height: 135px;
            width:100px;
        }
        
        #gridEvent_tooltips_container
        {
            min-height: auto;
        }
        
     </style>

     <script id="Pymes" type="text/javascript">
         function CargarComboPymes(entorno) {
             $(function () {
                 var datosPymes = [];
                 var to = rutaFS + 'BI/VisorRendimiento.aspx/CargarPymes';
                 $.when($.ajax({
                     type: 'POST',
                     url: to,
                     contentType: 'application/json;charset=utf-8',
                     data: JSON.stringify({ "entorno": entorno }),
                     dataType: 'json',
                     async: false
                 })).done(function (msg) {
                     datosPymes = null;
                     datosPymes = msg.d;
                     var html = "";
                     if (datosPymes != null) {
                         for (var i = 0; i < datosPymes.length; i++) {
                             html += "<option value=\"";
                             html += datosPymes[i].Cod + "\">";
                             html += datosPymes[i].Denominacion + "</option>";
                         }
                         $("#ddlEnviromentPymes").empty().append(html);
                     }
                 });
             });
         }
    </script>

    <script type="text/javascript">
        //var datos;
        var data = [];
        var fi;
        var ff;
        var ran;
        var ent;
        var pym;
    </script>

    <style type="text/css">
        #chart
        {
            position: relative;
            float: left;
            margin-right: 10px;
        }        
        .selectionOptions
        {
            margin-bottom: 10px;   
        }
    </style>

   <script type="text/javascript">
       //declarar variables para que podamos acceder a ellas en cualquier momento
       var fecDesde, fecHasta, Rango, Texto;

       $(function () {
           $("#tabs").tabs();
           $("#dpFechaIni").datepicker();
           $("#dpFechaFin").datepicker();
           //Seleccion del dia en el datepicker (Today) y creacion de la gráfica respecto a ese día.
           $("#dpSelectDay").datepicker({
               onSelect: function (dateText) {
                   OcultarBotonesSeleccion();
                   OcultarTextosBotonesSeleccion();
                   //$('[id$=lblHoy]').text(FormatoFecha(dateText));
                   $('[id$=lblHoy]').text(dateText);
                   $('[id$=lblHoy]').css("font-size", "10px");
                   var fechaInicio = $(this).datepicker('getDate');
                   var fechaFin = $(this).datepicker('getDate');
                   fechaInicio.setHours(0, 0, 0, 0);
                   fechaFin.setHours(23, 59, 59, 000);
                   $('[id$=divCargando]').css("display", "block").css("width", "100px").css("margin", "18% auto");
                   $('[id$=divTabContainer]').css("display", "none");
                   setTimeout(function () { SelectDay(dateText, fechaInicio, fechaFin); }, 200);
               }
           });
       });
       function SelectDay(dateText, fechaInicio, fechaFin) {
           if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
               var entorno = $('[id$=lblCodEntorno]').text();
           } else {
               var entorno = $('[id$=ddlEnviroment]').val();
           }

           if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
               var pyme = $('[id$=ddlEnviromentPymes]').val();
           } else {
               var pyme = null;
           }

           var kpi = $('[id$=ddlRecentActivity2]').val();
           CrearRadialGaugeMedia(fechaInicio, fechaFin, entorno, pyme, "DIA");
           CrearGraficaPerformanceEvolution(fechaInicio, fechaFin, entorno, pyme, "DIA");
           $("#divPerformanceEvolution").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30 }]);
           CrearGraficaRecentActivityKPI(fechaInicio, fechaFin, entorno, pyme, "DIA", kpi);
           $("#divRecentActivityKPI").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
           ComprobarGridKPIs(fechaInicio, fechaFin, entorno, pyme, "DIA");
           ComprobarGridLastDurationTimes(fechaInicio, fechaFin, entorno, pyme, "DIA", kpi);

           $('[id$=divHoy]').css("background-color", "#D3D3D3");
           $('[id$=divAhora]').css("background-color", "white");
           $('[id$=divEstaSemana]').css("background-color", "white");
           $('[id$=divEsteMes]').css("background-color", "white");
           $('[id$=divEsteAnyo]').css("background-color", "white");
           fecDesde = fechaInicio;
           fecHasta = fechaFin;
           Rango = "DIA";
           Texto = dateText;
           $('[id$=divCargando]').css("display", "none");
           $('[id$=divTabContainer]').css("display", "block");
       }

       //Evento que se produce cuando se selecciona un dia del combo del mes+anyo
       $(function () {
           var mes = -1;
           var anyo = (new Date).getFullYear();
           var months = [TextosVisorActividad[0], TextosVisorActividad[1], TextosVisorActividad[2], TextosVisorActividad[3], TextosVisorActividad[4], TextosVisorActividad[5], TextosVisorActividad[6], TextosVisorActividad[7], TextosVisorActividad[8], TextosVisorActividad[9], TextosVisorActividad[10], TextosVisorActividad[11]];
           $('#ddlSelectMonth').change(function () {
               mes = parseInt($("#ddlSelectMonth").val());
               anyo = parseInt($("#ddlSelectYear").val()) + 2003;
               if (mes != -1) {
                   $('[id$=divCargando]').css("display", "block").css("width", "100px").css("margin", "18% auto");
                   $('[id$=divTabContainer]').css("display", "none");
                   setTimeout(function () { SelectMonth(mes, months, anyo); }, 200);
               }
           });
           function SelectMonth(mes, months, anyo) {
               var fechaIni = new Date(parseInt(anyo), mes, parseInt("01"));
               fechaIni.setHours(0, 0, 0, 0);
               var fechaFin = new Date(parseInt(anyo), mes + 1, parseInt("01"));
               fechaFin.setDate(fechaFin.getDate() - 1);
               fechaFin.setHours(23, 59, 59, 000);
               OcultarBotonesSeleccion();
               OcultarTextosBotonesSeleccion();
               if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
                   var entorno = $('[id$=lblCodEntorno]').text();
               } else {
                   var entorno = $('[id$=ddlEnviroment]').val();
               }
               if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
                   var pyme = $('[id$=ddlEnviromentPymes]').val();
               } else {
                   var pyme = null;
               }

               if (anyo.length == 1) {
                   anyo = "190" + anyo;
               }
               $('[id$=lblEsteMes]').text(months[mes] + "-" + anyo);
               $('[id$=lblEsteMes]').css("font-size", "10px");

               var kpi = $('[id$=ddlRecentActivity2]').val();
               CrearRadialGaugeMedia(fechaIni, fechaFin, entorno, pyme, "MES");
               CrearGraficaPerformanceEvolution(fechaIni, fechaFin, entorno, pyme, "MES");
               $("#divPerformanceEvolution").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30 }]);
               CrearGraficaRecentActivityKPI(fechaIni, fechaFin, entorno, pyme, "MES", kpi)
               $("#divRecentActivityKPI").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
               ComprobarGridKPIs(fechaIni, fechaFin, entorno, pyme, "MES");
               ComprobarGridLastDurationTimes(fechaIni, fechaFin, entorno, pyme, "MES", kpi);

               $('[id$=divHoy]').css("background-color", "white");
               $('[id$=divAhora]').css("background-color", "white");
               $('[id$=divEstaSemana]').css("background-color", "white");
               $('[id$=divEsteMes]').css("background-color", "#D3D3D3");
               $('[id$=divEsteAnyo]').css("background-color", "white");
               $('[id$=ddlSelectMonth]').val('-1');
               fecDesde = fechaIni;
               fecHasta = fechaFin;
               Rango = "MES";
               Texto = months[mes] + "-" + anyo;

               $('[id$=divCargando]').css("display", "none");
               $('[id$=divTabContainer]').css("display", "block");
           }
           $('#ddlSelectYear').change(function () {
               anyo = parseInt(this.value) + 2003;
           });

           //Evento que se produce cuando se selecciona un anyo del combo de Este Anyo
           $('#ddlSelectYearAnyo').change(function () {
               if (anyo != 0) {
                   anyo = parseInt(this.value) + 2003;
                   $('[id$=divCargando]').css("display", "block").css("width", "100px").css("margin", "18% auto");
                   $('[id$=divTabContainer]').css("display", "none");
                   setTimeout(function () { SelectYearAnyo(anyo); }, 200);
               }
           });

           function SelectYearAnyo(anyo) {
               var fechaIni = new Date(parseInt(anyo), 00, 01);
               fechaIni.setHours(0, 0, 0, 0);
               var fechaFin = new Date(parseInt(anyo), 11, 31);
               fechaFin.setHours(23, 59, 59, 000);
               OcultarBotonesSeleccion();
               OcultarTextosBotonesSeleccion();
               if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
                   var entorno = $('[id$=lblCodEntorno]').text();
               } else {
                   var entorno = $('[id$=ddlEnviroment]').val();
               }
               if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
                   var pyme = $('[id$=ddlEnviromentPymes]').val();
               } else {
                   var pyme = null;
               }

               $('[id$=lblEsteAnyo]').text(anyo);
               $('[id$=lblEsteAnyo]').css("font-size", "10px");
               anyo = null;

               var kpi = $('[id$=ddlRecentActivity2]').val();
               CrearRadialGaugeMedia(fechaIni, fechaFin, entorno, pyme, "ANYO");
               CrearGraficaPerformanceEvolution(fechaIni, fechaFin, entorno, pyme, "ANYO");
               $("#divPerformanceEvolution").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30 }]);
               CrearGraficaRecentActivityKPI(fechaIni, fechaFin, entorno, pyme, "ANYO", kpi);
               $("#divRecentActivityKPI").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
               ComprobarGridKPIs(fechaIni, fechaFin, entorno, pyme, "ANYO");
               ComprobarGridLastDurationTimes(fechaIni, fechaFin, entorno, pyme, "ANYO", kpi);

               //marcar como seleccionado
               $('[id$=divHoy]').css("background-color", "white");
               $('[id$=divAhora]').css("background-color", "white");
               $('[id$=divEstaSemana]').css("background-color", "white");
               $('[id$=divEsteMes]').css("background-color", "white");
               $('[id$=divEsteAnyo]').css("background-color", "#D3D3D3");
               $('[id$=ddlSelectYearAnyo]').val('-1');
               fecDesde = fechaIni;
               fecHasta = fechaFin;
               Rango = "ANYO";
               Texto = anyo;

               $('[id$=divCargando]').css("display", "none");
               $('[id$=divTabContainer]').css("display", "block");
           }
       });

       $(function () {
           $('[id$=ddlEnviroment]').change(function () {
               if ($('[id$=ddlEnviroment]').val() == "PYME")
               //if ($('[id$=ddlEnviroment] option:selected').text() == "Pymes") 
               {
                   if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
                       var entorno = $('[id$=lblCodEntorno]').text();
                   } else {
                       var entorno = $('[id$=ddlEnviroment]').val();
                   }
                   //                  var entorno = $('[id$=ddlEnviroment] option:selected').text();
                   CargarComboPymes(entorno);

                   $('[id$=lblEnviromentPymes]').css("display", "block");
                   $('[id$=ddlEnviromentPymes]').css("display", "block");
               } else {
                   $('[id$=lblEnviromentPymes]').css("display", "none");
                   $('[id$=ddlEnviromentPymes]').css("display", "none");
               }
           });
       });

       esNow = "";
       esNowRecentActivity = "";

       function OrdenarPorHoraAcceso(elementoA, elementoB) {
           var horaA = elementoA.horaAcceso.toLowerCase();
           var horaB = elementoB.horaAcceso.toLowerCase();
           if (horaA.length <= 4) {
               horaA = "0" + horaA;
           }
           if (horaB.length <= 4) {
               horaB = "0" + horaB;
           }
           return ((horaA < horaB) ? -1 : ((horaA > horaB) ? 1 : 0));
       }

       function OrdenarPorHoraActividad(elementoA, elementoB) {
           var horaA = elementoA.horaAccesoActividad.toLowerCase();
           var horaB = elementoB.horaAccesoActividad.toLowerCase();
           if (horaA.length <= 4) {
               horaA = "0" + horaA;
           }
           if (horaB.length <= 4) {
               horaB = "0" + horaB;
           }
           return ((horaA < horaB) ? -1 : ((horaA > horaB) ? 1 : 0));
       }

       var idioma;

       //formatea la fecha en base a la seleccion de las opciones del usuario
       function FormatoFecha() {
           var fecha2 = "";
           var formato = "";
           $(function () {
               //Recuperamos el formato de la fecha de la aplicacion
               var to = rutaFS + 'BI/VisorRendimiento.aspx/FormatoFecha';
               $.when($.ajax({
                   type: 'POST',
                   url: to,
                   contentType: 'application/json;charset=utf-8',
                   dataType: 'json',
                   async: false
               })).done(function (msg) {
                   formato = null;
                   formato = msg.d;

                   if (formato.toUpperCase() == "DD/MM/YYYY" || formato.toUpperCase() == "DD/MM/YY") {
                       $('#dpSelectDay').datepicker('option', { dateFormat: 'dd/mm/yy' });
                       $('#dpSelectWeek').datepicker('option', { dateFormat: 'dd/mm/yy' });

                   } else if (formato.toUpperCase() == "MM/DD/YYYY" || formato.toUpperCase() == "MM/DD/YY") {
                       $('#dpSelectDay').datepicker('option', { dateFormat: 'mm/dd/yy' });
                       $('#dpSelectWeek').datepicker('option', { dateFormat: 'mm/dd/yy' });

                   } else if (formato.toUpperCase() == "DD-MM-YYYY" || formato.toUpperCase() == "DD-MM-YY") {
                       $('#dpSelectDay').datepicker('option', { dateFormat: 'dd-mm-yy' });
                       $('#dpSelectWeek').datepicker('option', { dateFormat: 'dd-mm-yy' });

                   } else if (formato.toUpperCase() == "MM-DD-YYYY" || formato.toUpperCase() == "MM-DD-YY") {
                       $('#dpSelectDay').datepicker('option', { dateFormat: 'mm-dd-yy' });
                       $('#dpSelectWeek').datepicker('option', { dateFormat: 'mm-dd-yy' });

                   } else if (formato.toUpperCase() == "DD.MM.YYYY" || formato.toUpperCase() == "DD.MM.YY") {
                       $('#dpSelectDay').datepicker('option', { dateFormat: 'dd.mm.yy' });
                       $('#dpSelectWeek').datepicker('option', { dateFormat: 'dd.mm.yy' });

                   } else if (formato.toUpperCase() == "MM.DD.YYYY" || formato.toUpperCase() == "MM.DD.YY") {
                       $('#dpSelectDay').datepicker('option', { dateFormat: 'mm.dd.yy' });
                       $('#dpSelectWeek').datepicker('option', { dateFormat: 'mm.dd.yy' });
                   }
               });
           })
           //return fecha2;
       }

       $(document).ready(function () {
           //formateamos los datepicker con el formato de las fechas del usuario
           FormatoFecha();

           //Recuperamos el entorno en el que estamos (el primer registro de la tabla FSAL_ENTORNO)
           var primerEntorno = "";
           var to = rutaFS + 'BI/VisorRendimiento.aspx/RecuperarEntorno';
           $.when($.ajax({
               type: 'POST',
               url: to,
               contentType: 'application/json;charset=utf-8',
               dataType: 'json',
               async: false
           })).done(function (msg) {
               primerEntorno = null;
               primerEntorno = msg.d;
           });

           //Textos y estilos 
           $('[id$=divCargando]').css("display", "none");
           $('[id$=divTabContainer]').css("display", "block");
           $('[id$=lblActivityDashboard]').text(TextosVisorActividad[12]);
           $('[id$=lblPerformanceDashboard]').text(TextosVisorActividad[13]);
           $('[id$=lblStatisticalInfo]').text(TextosVisorActividad[14]);
           $('[id$=lblEnviroment]').text(TextosVisorActividad[15]);
           $('[id$=lblEnviroment]').css({ 'font-weight': 'bold' });
           $('[id$=lblFechaDesde]').text(TextosVisorActividad[16]);
           $('[id$=lblFechaHasta]').text(TextosVisorActividad[17]);
           $('[id$=lblAhora]').text(TextosVisorActividad[18]);
           $('[id$=lblHoy]').text(TextosVisorActividad[19]);
           $('[id$=lblEstaSemana]').text(TextosVisorActividad[20]);
           $('[id$=lblEsteMes]').text(TextosVisorActividad[21]);
           $('[id$=lblEsteAnyo]').text(TextosVisorActividad[22]);
           $('[id$=lblOnlineUsers]').text(TextosVisorActividad[23]);
           $('[id$=lblOnlineUsers]').css({ 'font-weight': 'bold' });
           $('[id$=lblRecentActivity]').text(TextosVisorActividad[24]);
           $('[id$=lblRecentActivity]').css({ 'font-weight': 'bold' });
           $('[id$=lblEventos]').text(TextosVisorActividad[25]);
           $('[id$=lblEventos]').css({ 'font-weight': 'bold' });
           $('[id$=lblErrores]').text(TextosVisorActividad[26]);
           $('[id$=lblErrores]').css({ 'font-weight': 'bold' });
           $('[id$=lblIncludeAverages]').text(TextosVisorActividad[32]);
           $('[id$=lblIncludeAverages]').css({ 'font-size': '10' });
           $('[id$=chkAverages]').attr('checked', false);
           $('[id$=lblIncludeAveragesRecentActivity]').text(TextosVisorActividad[32]);
           $('[id$=lblEnviromentPymes]').text(TextosVisorActividad[48]);
           $('[id$=lblcargando]').text(TextosVisorActividad[64]);
           $('[id$=lblEnviromentPymes]').css({ 'font-weight': 'bold' });
           $('[id$=lblEnviromentPymes]').css("display", "none");
           $('[id$=ddlEnviromentPymes]').css("display", "none");

           $('[id$=lblOverallPerformance]').text(TextosVisorActividad[56]);
           $('[id$=lblOverallPerformance]').css({ 'font-weight': 'bold' });
           $('[id$=lblRecentActivity2]').text(TextosVisorActividad[57]);
           $('[id$=lblRecentActivity2]').css({ 'font-weight': 'bold' });
           $('[id$=lblKPI]').text(TextosVisorActividad[58]);
           $('[id$=lblKPI]').css({ 'font-weight': 'bold' });
           $('[id$=lblLastDurationTimes]').text(TextosVisorActividad[59]);
           $('[id$=lblLastDurationTimes]').css({ 'font-weight': 'bold' });

           //fechas de inicio y fin
           var fechaDesde = new Date();
           var mins = fechaDesde.getMinutes();
           while (mins % 5 != 0) {
               mins -= 1;
           }
           fechaDesde.setHours(fechaDesde.getHours() - 1, mins, 0);
           var fechaHasta = new Date();
           fechaHasta.setHours(fechaDesde.getHours() + 1, mins, 0);

           //Combo de los meses
           var months = [TextosVisorActividad[0], TextosVisorActividad[1], TextosVisorActividad[2], TextosVisorActividad[3], TextosVisorActividad[4], TextosVisorActividad[5], TextosVisorActividad[6], TextosVisorActividad[7], TextosVisorActividad[8], TextosVisorActividad[9], TextosVisorActividad[10], TextosVisorActividad[11]];
           $.each(months, function (val, text) {
               $('#ddlSelectMonth').append($('<option></option>').val(val).html(text))
           });
           $('#ddlSelectMonth').prepend($('<option value="-1" selected></option>'));

           var d = new Date();
           //Combo de los anyos
           for (var i = 2003; i <= fechaDesde.getFullYear(); i++) {
               $('#ddlSelectYear').append($('<option></option>').val(i - 2003).html(i));
               $('#ddlSelectYearAnyo').append($('<option></option>').val(i - 2003).html(i));
           }
           //para que se quede seleccionado el año en el que estamos en la combo
           var y = d.getFullYear();
           $("#ddlSelectYear").find("option:contains('" + y + "')").each(function () {
               if ($(this).text() == y) {
                   $(this).attr("selected", "selected");
               }
           });
           $('#ddlSelectYearAnyo').prepend($('<option value="-1" selected></option>'));

           if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
               var entorno = $('[id$=lblCodEntorno]').text();
           } else {
               var entorno = $('[id$=ddlEnviroment]').val();
           }

           if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
               var pyme = $('[id$=ddlEnviromentPymes]').val();
           } else {
               var pyme = null;
           }

           //Cargo los grids y las gráficas    
           CrearRadialGaugeMedia(fechaDesde, fechaHasta, entorno, pyme, "HORA");
           CrearGraficaPerformanceEvolution(fechaDesde, fechaHasta, entorno, pyme, "HORA");
           $("#divPerformanceEvolution").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
           CrearGraficaRecentActivityKPI(fechaDesde, fechaHasta, entorno, pyme, "HORA", 1);
           $("#divRecentActivityKPI").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
           ComprobarGridKPIs(fechaDesde, fechaHasta, entorno, pyme, "HORA");
           ComprobarGridLastDurationTimes(fechaDesde, fechaHasta, entorno, pyme, "HORA", 1);
           OcultarBotonesSeleccion();
           OcultarTextosBotonesSeleccion();

           //Marco Ahora como opción por defecto
           $('[id$=divHoy]').css("background-color", "white");
           $('[id$=divAhora]').css("background-color", "#D3D3D3");
           $('[id$=divEstaSemana]').css("background-color", "white");
           $('[id$=divEsteMes]').css("background-color", "white");
           $('[id$=divEsteAnyo]').css("background-color", "white");
           fecDesde = fechaDesde;
           fecHasta = fechaHasta;
           Rango = "HORA";
           Texto = TextosVisorActividad[18];
       });

       function OcultarBotonesSeleccion() {
           $('[id$=lblNow]').css("display", "none");
           $('[id$=lblSelectHour]').css("display", "none");
           $('[id$=divSelectHour]').css("display", "none");
           $('[id$=lblToday]').css("display", "none");
           $('[id$=lblSelectDay]').css("display", "none");
           $('[id$=dpSelectDay]').css("display", "none");
           $('[id$=lblThisWeek]').css("display", "none");
           $('[id$=lblSelectWeek]').css("display", "none");
           $('[id$=dpSelectWeek]').css("display", "none");
           $('[id$=lblThisMonth]').css("display", "none");
           $('[id$=lblSelectMonth]').css("display", "none");
           $('[id$=ddlSelectMonth]').css("display", "none");
           $('[id$=ddlSelectYear]').css("display", "none");
           $('[id$=divSeleccionMes]').css("display", "none");
           $('[id$=lblThisYear]').css("display", "none");
           $('[id$=lblSelectYear]').css("display", "none");
           $('[id$=divSeleccionAnyo]').css("display", "none");
           $('[id$=ddlSelectYearAnyo]').css("display", "none");
       }

       function OcultarTextosBotonesSeleccion() {
           $('[id$=lblAhora]').text(TextosVisorActividad[18]);
           $('[id$=lblAhora]').css("font-size", "10px");
           $('[id$=lblHoy]').text(TextosVisorActividad[19]);
           $('[id$=lblHoy]').css("font-size", "10px");
           $('[id$=lblEstaSemana]').text(TextosVisorActividad[20]);
           $('[id$=lblEstaSemana]').css("font-size", "10px");
           $('[id$=lblEsteMes]').text(TextosVisorActividad[21]);
           $('[id$=lblEsteMes]').css("font-size", "10px");
           $('[id$=lblEsteAnyo]').text(TextosVisorActividad[22]);
           $('[id$=lblEsteAnyo]').css("font-size", "10px");
           $('[id$=divAhora]').css("background-color", "white");
           $('[id$=divHoy]').css("background-color", "white");
           $('[id$=divEstaSemana]').css("background-color", "white");
           $('[id$=divEsteMes]').css("background-color", "white");
           $('[id$=divEsteAnyo]').css("background-color", "white");
       }

       //Click del boton Ahora
       function btnNow_Click() {
           //mostrar ventana cargando
           $('[id$=divCargando]').css("display", "block").css("width", "100px").css("margin", "18% auto");
           $('[id$=divTabContainer]').css("display", "none");
           setTimeout(Now_Click, 200);
       }
       function Now_Click() {
           var fechaDesde = new Date();
           var mins = fechaDesde.getMinutes();
           while (mins % 5 != 0) {
               mins -= 1;
           }
           fechaDesde.setHours(fechaDesde.getHours() - 1, mins, fechaDesde.getSeconds());
           var fechaHasta = new Date();
           fechaHasta.setHours(fechaHasta.getHours(), mins, fechaHasta.getSeconds());
           if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
               var entorno = $('[id$=lblCodEntorno]').text();
           } else {
               var entorno = $('[id$=ddlEnviroment]').val();
           }

           if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
               var pyme = $('[id$=ddlEnviromentPymes]').val();
           } else {
               var pyme = null;
           }

           var kpi = $('[id$=ddlRecentActivity2]').val();
           CrearRadialGaugeMedia(fechaDesde, fechaHasta, entorno, pyme, "HORA");
           CrearGraficaPerformanceEvolution(fechaDesde, fechaHasta, entorno, pyme, "HORA");
           $("#divPerformanceEvolution").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30 }]);
           CrearGraficaRecentActivityKPI(fechaDesde, fechaHasta, entorno, pyme, "HORA", kpi);
           $("#divRecentActivityKPI").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30 }]);
           OcultarBotonesSeleccion();
           OcultarTextosBotonesSeleccion();
           ComprobarGridKPIs(fechaDesde, fechaHasta, entorno, pyme, "HORA");
           ComprobarGridLastDurationTimes(fechaDesde, fechaHasta, entorno, pyme, "HORA", kpi);

           $('[id$=divAhora]').css("background-color", "white");
           $('[id$=divHoy]').css("background-color", "white");
           $('[id$=divEstaSemana]').css("background-color", "white");
           $('[id$=divEsteMes]').css("background-color", "white");
           $('[id$=divEsteAnyo]').css("background-color", "white");

           //marcar como seleccionado
           $('[id$=divHoy]').css("background-color", "white");
           $('[id$=divAhora]').css("background-color", "#D3D3D3");
           $('[id$=divEstaSemana]').css("background-color", "white");
           $('[id$=divEsteMes]').css("background-color", "white");
           $('[id$=divEsteAnyo]').css("background-color", "white");
           fecDesde = fechaDesde;
           fecHasta = fechaHasta;
           Rango = "HORA";
           Texto = TextosVisorActividad[18];

           $('[id$=divCargando]').css("display", "none");
           $('[id$=divTabContainer]').css("display", "block");
       }

       function btnAhora_Click() {
           OcultarBotonesSeleccion();
           $('[id$=lblNow]').text(TextosVisorActividad[18]);
           $('[id$=lblNow]').css("display", "inherit");
           $('[id$=lblSelectHour]').text(TextosVisorActividad[31]);
           $('[id$=lblSelectHour]').css("display", "inherit");
           $('[id$=divSelectHour]').css("display", "inherit");
       }

       //Click del boton hoy
       function btnToday_Click() {
           //mostrar ventana cargando
           $('[id$=divCargando]').css("display", "block").css("width", "100px").css("margin", "18% auto");
           $('[id$=divTabContainer]').css("display", "none");
           setTimeout(Today_Click, 200);
       }

       function Today_Click() {
           var fechaDesde = new Date();
           fechaDesde.setHours(0, 0, 0, 0);
           var fechaHasta = new Date();
           fechaHasta.setHours(23, 59, 59, 999);
           if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
               var entorno = $('[id$=lblCodEntorno]').text();
           } else {
               var entorno = $('[id$=ddlEnviroment]').val();
           }
           if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
               var pyme = $('[id$=ddlEnviromentPymes]').val();
           } else {
               var pyme = null;
           }

           var kpi = $('[id$=ddlRecentActivity2]').val();
           CrearRadialGaugeMedia(fechaDesde, fechaHasta, entorno, pyme, "DIA");
           CrearGraficaPerformanceEvolution(fechaDesde, fechaHasta, entorno, pyme, "DIA");
           $("#divPerformanceEvolution").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30 }]);
           CrearGraficaRecentActivityKPI(fechaDesde, fechaHasta, entorno, pyme, "DIA", kpi);
           $("#divRecentActivityKPI").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30 }]);
           OcultarBotonesSeleccion();
           OcultarTextosBotonesSeleccion();
           ComprobarGridKPIs(fechaDesde, fechaHasta, entorno, pyme, "DIA");
           ComprobarGridLastDurationTimes(fechaDesde, fechaHasta, entorno, pyme, "DIA", kpi);

           //marcar como seleccionado
           $('[id$=divHoy]').css("background-color", "#D3D3D3");
           $('[id$=divAhora]').css("background-color", "white");
           $('[id$=divEstaSemana]').css("background-color", "white");
           $('[id$=divEsteMes]').css("background-color", "white");
           $('[id$=divEsteAnyo]').css("background-color", "white");
           fecDesde = fechaDesde;
           fecHasta = fechaHasta;
           Rango = "DIA";
           Texto = TextosVisorActividad[19];

           $('[id$=divCargando]').css("display", "none");
           $('[id$=divTabContainer]').css("display", "block");
       }

       function btnHoy_Click() {
           OcultarBotonesSeleccion();
           $('[id$=lblToday]').text(TextosVisorActividad[19]);
           $('[id$=lblToday]').css("display", "inherit");
           $('[id$=lblSelectDay]').text(TextosVisorActividad[27]);
           $('[id$=lblSelectDay]').css("display", "inherit");
           $('[id$=dpSelectDay]').css("display", "inherit");
       }

       function getMonday(d) {
           var day = d.getDay(),
                diff = d.getDate() - day + (day == 0 ? -6 : 0);
           return new Date(d.setDate(diff));
       }

       //Click del boton Esta semana
       function btnThisWeek_Click() {
           //mostrar ventana cargando
           $('[id$=divCargando]').css("display", "block").css("width", "100px").css("margin", "18% auto");
           $('[id$=divTabContainer]').css("display", "none");
           setTimeout(ThisWeek_Click, 200);
       }

       function ThisWeek_Click() {
           var fechaDesde;
           var fechaHasta;
           var currentDate = new Date();
           if (currentDate.getDay() == 0) {
               fechaDesde = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - currentDate.getDay() - 6);
               fechaHasta = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - currentDate.getDay());
           } else {
               fechaDesde = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - currentDate.getDay() + 1);
               fechaHasta = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - currentDate.getDay() + 7);
           }
           fechaDesde.setHours(0, 0, 0, 0);
           fechaHasta.setHours(23, 59, 59, 999);
           if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
               var entorno = $('[id$=lblCodEntorno]').text();
           } else {
               var entorno = $('[id$=ddlEnviroment]').val();
           }
           if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
               var pyme = $('[id$=ddlEnviromentPymes]').val();
           } else {
               var pyme = null;
           }

           var kpi = $('[id$=ddlRecentActivity2]').val();
           CrearRadialGaugeMedia(fechaDesde, fechaHasta, entorno, pyme, "SEMANA");
           CrearGraficaPerformanceEvolution(fechaDesde, fechaHasta, entorno, pyme, "SEMANA");
           $("#divPerformanceEvolution").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30 }]);
           CrearGraficaRecentActivityKPI(fechaDesde, fechaHasta, entorno, pyme, "SEMANA", kpi);
           $("#divRecentActivityKPI").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30 }]);
           ComprobarGridKPIs(fechaDesde, fechaHasta, entorno, pyme, "SEMANA");
           ComprobarGridLastDurationTimes(fechaDesde, fechaHasta, entorno, pyme, "SEMANA", kpi);

           OcultarBotonesSeleccion();
           OcultarTextosBotonesSeleccion();
           //marcar como seleccionado
           $('[id$=divHoy]').css("background-color", "white");
           $('[id$=divAhora]').css("background-color", "white");
           $('[id$=divEstaSemana]').css("background-color", "#D3D3D3");
           $('[id$=divEsteMes]').css("background-color", "white");
           $('[id$=divEsteAnyo]').css("background-color", "white");
           fecDesde = fechaDesde;
           fecHasta = fechaHasta;
           Rango = "SEMANA";
           Texto = TextosVisorActividad[20];
           $('[id$=divCargando]').css("display", "none");
           $('[id$=divTabContainer]').css("display", "block");
       }
       function btnEstaSemana_Click() {
           OcultarBotonesSeleccion();
           $('[id$=lblThisWeek]').text(TextosVisorActividad[20]);
           $('[id$=lblThisWeek]').css("display", "inherit");
           $('[id$=lblSelectWeek]').text(TextosVisorActividad[28]);
           $('[id$=lblSelectWeek]').css("display", "inherit");
           $('[id$=dpSelectWeek]').css("display", "inherit");
       }

       //Click del boton Este mes
       function btnThisMonth_Click() {
           //mostrar ventana cargando
           $('[id$=divCargando]').css("display", "block").css("width", "100px").css("margin", "18% auto");
           $('[id$=divTabContainer]').css("display", "none");
           setTimeout(ThisMonth_Click, 500);

       }
       function ThisMonth_Click() {
           var currentDate = new Date();
           var firstDayOfMonth = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
           var lastDayOfMonth = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0, 23, 59, 59);
           if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
               var entorno = $('[id$=lblCodEntorno]').text();
           } else {
               var entorno = $('[id$=ddlEnviroment]').val();
           }
           if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
               var pyme = $('[id$=ddlEnviromentPymes]').val();
           } else {
               var pyme = null;
           }

           var kpi = $('[id$=ddlRecentActivity2]').val();
           CrearRadialGaugeMedia(firstDayOfMonth, lastDayOfMonth, entorno, pyme, "MES");
           CrearGraficaPerformanceEvolution(firstDayOfMonth, lastDayOfMonth, entorno, pyme, "MES");
           $("#divPerformanceEvolution").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30 }]);
           CrearGraficaRecentActivityKPI(firstDayOfMonth, lastDayOfMonth, entorno, pyme, "MES", kpi);
           $("#divRecentActivityKPI").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30 }]);
           ComprobarGridKPIs(firstDayOfMonth, lastDayOfMonth, entorno, pyme, "MES");
           ComprobarGridLastDurationTimes(firstDayOfMonth, lastDayOfMonth, entorno, pyme, "MES", kpi);

           OcultarBotonesSeleccion();
           OcultarTextosBotonesSeleccion();

           //marcar como seleccionado
           $('[id$=divHoy]').css("background-color", "white");
           $('[id$=divAhora]').css("background-color", "white");
           $('[id$=divEstaSemana]').css("background-color", "white");
           $('[id$=divEsteMes]').css("background-color", "#D3D3D3");
           $('[id$=divEsteAnyo]').css("background-color", "white");
           fecDesde = firstDayOfMonth;
           fecHasta = lastDayOfMonth;
           Rango = "MES";
           Texto = TextosVisorActividad[21];
           $('[id$=divCargando]').css("display", "none");
           $('[id$=divTabContainer]').css("display", "block");
       }
       function btnEsteMes_Click() {
           OcultarBotonesSeleccion();
           $('[id$=lblThisMonth]').text(TextosVisorActividad[21]);
           $('[id$=lblSelectMonth]').text(TextosVisorActividad[29]);
           $('[id$=lblThisMonth]').css("display", "inherit");
           $('[id$=lblSelectMonth]').css("display", "inherit");
           $('[id$=pnlSelectMonth]').css("display", "inherit");
           $('[id$=divSeleccionMes]').css("display", "inherit");
           $('[id$=ddlSelectMonth]').css("display", "inherit");
           $('[id$=ddlSelectYear]').css("display", "inherit");
       }

       //Click del boton Este año
       function btnThisYear_Click() {
           //mostrar ventana cargando
           $('[id$=divCargando]').css("display", "block").css("width", "100px").css("margin", "18% auto");
           $('[id$=divTabContainer]').css("display", "none");
           setTimeout(ThisYear_Click, 1000);
       }
       function ThisYear_Click() {
           var currentDate = new Date();
           if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
               var entorno = $('[id$=lblCodEntorno]').text();
           } else {
               var entorno = $('[id$=ddlEnviroment]').val();
           }
           if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
               var pyme = $('[id$=ddlEnviromentPymes]').val();
           } else {
               var pyme = null;
           }
           var fechaDesde = new Date(currentDate.getFullYear(), 0, 1);
           var fechaHasta = new Date(currentDate.getFullYear(), 11, 31);
           fechaHasta.setHours(23, 59, 59, 000);

           var kpi = $('[id$=ddlRecentActivity2]').val();
           CrearRadialGaugeMedia(fechaDesde, fechaHasta, entorno, pyme, "ANYO");
           CrearGraficaPerformanceEvolution(fechaDesde, fechaHasta, entorno, pyme, "ANYO");
           $("#divPerformanceEvolution").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30 }]);
           CrearGraficaRecentActivityKPI(fechaDesde, fechaHasta, entorno, pyme, "ANYO", kpi);
           $("#divRecentActivityKPI").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30 }]);
           ComprobarGridKPIs(fechaDesde, fechaHasta, entorno, pyme, "ANYO");
           ComprobarGridLastDurationTimes(fechaDesde, fechaHasta, entorno, pyme, "ANYO", kpi);
           OcultarBotonesSeleccion();
           OcultarTextosBotonesSeleccion();

           //marcar como seleccionado
           $('[id$=divHoy]').css("background-color", "white");
           $('[id$=divAhora]').css("background-color", "white");
           $('[id$=divEstaSemana]').css("background-color", "white");
           $('[id$=divEsteMes]').css("background-color", "white");
           $('[id$=divEsteAnyo]').css("background-color", "#D3D3D3");
           fecDesde = fechaDesde;
           fecHasta = fechaHasta;
           Rango = "ANYO";
           Texto = TextosVisorActividad[22];
           $('[id$=divCargando]').css("display", "none");
           $('[id$=divTabContainer]').css("display", "block");
       }
       function btnEsteAnyo_Click() {
           OcultarBotonesSeleccion();
           $('[id$=lblThisYear]').text(TextosVisorActividad[22]);
           $('[id$=lblSelectYear]').text(TextosVisorActividad[30]);
           $('[id$=lblThisYear]').css("display", "inherit");
           $('[id$=lblSelectYear]').css("display", "inherit");
           $('[id$=divSeleccionAnyo]').css("display", "inherit");
           $('[id$=ddlSelectYearAnyo]').css("display", "inherit");
       }

       //Clicks de las distintas horas
       function Horas(h1, h2) {
           //mostrar ventana cargando
           $('[id$=divCargando]').css("display", "block").css("width", "100px").css("margin", "18% auto");
           $('[id$=divTabContainer]').css("display", "none");
           setTimeout(function () { CalculoHoras(h1, h2); }, 200);
       }

       function CalculoHoras(h1, h2) {
           var d = new Date();
           d.setHours(h1, 0, 0, 0);
           var d2 = new Date();
           d2.setHours(h2, 0, 0, 0);
           if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
               var entorno = $('[id$=lblCodEntorno]').text();
           } else {
               var entorno = $('[id$=ddlEnviroment]').val();
           }
           if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
               var pyme = $('[id$=ddlEnviromentPymes]').val();
           } else {
               var pyme = null;
           }

           var kpi = $('[id$=ddlRecentActivity2]').val();
           CrearRadialGaugeMedia(d, d2, entorno, pyme, "HORA");
           CrearGraficaPerformanceEvolution(d, d2, entorno, pyme, "HORA");
           $("#divPerformanceEvolution").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30 }]);
           CrearGraficaRecentActivityKPI(d, d2, entorno, pyme, "HORA", kpi);
           $("#divRecentActivityKPI").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
           ComprobarGridKPIs(d, d2, entorno, pyme, "HORA");
           ComprobarGridLastDurationTimes(d, d2, entorno, pyme, "HORA", kpi);

           OcultarBotonesSeleccion();
           OcultarTextosBotonesSeleccion();
           $('[id$=lblAhora]').text(h1 + ":00 " + h2 + ":00"); //para darle el valor de la hora al boton
           $('[id$=lblAhora]').css("font-size", "10px");

           //marcar como seleccionado
           $('[id$=divHoy]').css("background-color", "white");
           $('[id$=divAhora]').css("background-color", "#D3D3D3");
           $('[id$=divEstaSemana]').css("background-color", "white");
           $('[id$=divEsteMes]').css("background-color", "white");
           $('[id$=divEsteAnyo]').css("background-color", "white");
           fecDesde = d;
           fecHasta = d2;
           Rango = "HORA";
           Texto = h1 + ":00 " + h2 + ":00";
           $('[id$=divCargando]').css("display", "none");
           $('[id$=divTabContainer]').css("display", "block");
       }

       //Muestra el calendario para seleccionar una semana.
       $(function () {
           var startDate;
           var endDate;

           var selectCurrentWeek = function () {
               window.setTimeout(function () {
                   $('.week-picker').find('.ui-datepicker-current-day a').addClass('ui-state-active')
               }, 1);
           }
           $('.week-picker').datepicker({
               showOtherMonths: true,
               selectOtherMonths: true,
               onSelect: function (dateText, inst) {
                   var date = $(this).datepicker('getDate');
                   if (date.getDay() == 0) {
                       startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() - 6);
                       endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
                   } else {
                       startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 1);
                       endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 7);
                   }
                   var dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
                   $('#startDate').text($.datepicker.formatDate(dateFormat, startDate, inst.settings));
                   $('#endDate').text($.datepicker.formatDate(dateFormat, endDate, inst.settings));
                   selectCurrentWeek();

                   var fechaDesde = $.datepicker.formatDate(dateFormat, startDate, inst.settings);
                   var fechaHasta = $.datepicker.formatDate(dateFormat, endDate, inst.settings);
                   //mostrar ventana cargando
                   $('[id$=divCargando]').css("display", "block").css("width", "100px").css("margin", "18% auto");
                   $('[id$=divTabContainer]').css("display", "none");
                   setTimeout(function () { Week(startDate, endDate, fechaDesde, fechaHasta); }, 200);
               },
               beforeShowDay: function (date) {
                   var cssClass = '';
                   if (date >= startDate && date <= endDate)
                       cssClass = 'ui-datepicker-current-day';
                   return [true, cssClass];
               },
               onChangeMonthYear: function (year, month, inst) {
                   selectCurrentWeek();
               }
           });

           $('.week-picker .ui-datepicker-calendar tr').live('mousemove', function () { $(this).find('td a').addClass('ui-state-hover'); });
           $('.week-picker .ui-datepicker-calendar tr').live('mouseleave', function () { $(this).find('td a').removeClass('ui-state-hover'); });

           function Week(startDate, endDate, fechaDesde, fechaHasta) {
               if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
                   var entorno = $('[id$=lblCodEntorno]').text();
               } else {
                   var entorno = $('[id$=ddlEnviroment]').val();
               }
               if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
                   var pyme = $('[id$=ddlEnviromentPymes]').val();
               } else {
                   var pyme = null;
               }

               var kpi = $('[id$=ddlRecentActivity2]').val();
               CrearRadialGaugeMedia(startDate, endDate, entorno, pyme, "SEMANA");
               CrearGraficaPerformanceEvolution(startDate, endDate, entorno, pyme, "SEMANA");
               $("#divPerformanceEvolution").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30 }]);
               CrearGraficaRecentActivityKPI(startDate, endDate, entorno, pyme, "SEMANA", kpi);
               $("#divRecentActivityKPI").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30 }]);
               ComprobarGridKPIs(startDate, endDate, entorno, pyme, "SEMANA");
               ComprobarGridLastDurationTimes(startDate, endDate, entorno, pyme, "SEMANA", kpi);

               OcultarBotonesSeleccion();
               OcultarTextosBotonesSeleccion();
               $('[id$=lblEstaSemana]').text(fechaDesde + " - " + fechaHasta);
               //$('[id$=lblEstaSemana]').text(FormatoFecha(fechaDesde) + " - " + FormatoFecha(fechaHasta));
               $('[id$=lblEstaSemana]').css("font-size", "10px");

               //marcar como seleccionado
               $('[id$=divHoy]').css("background-color", "white");
               $('[id$=divAhora]').css("background-color", "white");
               $('[id$=divEstaSemana]').css("background-color", "#D3D3D3");
               $('[id$=divEsteMes]').css("background-color", "white");
               $('[id$=divEsteAnyo]').css("background-color", "white");
               fecDesde = startDate;
               fecHasta = endDate;
               Rango = "SEMANA";
               Texto = fechaDesde + " - " + fechaHasta;

               $('[id$=divCargando]').css("display", "none");
               $('[id$=divTabContainer]').css("display", "block");
           }
       });

       $(function () {
           $("#lblSelectDay").click(function () {
               $("#dpSelectDay").focus();
           });
       });

       $(function () {
           $("#lblSelectWeek").click(function () {
               $("#dpSelectWeek").focus();
           });
       });

       $('body').click(function (event) {
           var Ocultar1 = $(event.target).parents('[id^=divAhora]').length == 1;
           var Ocultar2 = $(event.target).parents('[id^=divHoy]').length == 1;
           var Ocultar3 = $(event.target).parents('[id^=divEstaSemana]').length == 1;
           var Ocultar4 = $(event.target).parents('[id^=divEsteMes]').length == 1;
           var Ocultar5 = $(event.target).parents('[id^=divEsteAnyo]').length == 1;
           var Ocultar6 = $(event.target).parents('[id^=lblToday]').length == 1;
           var Ocultar7 = $(event.target).parents('[id^=lblSelectDay]').length == 1;
           var Ocultar8 = $(event.target).parents('[id^=dpSelectDay]').length == 1;
           var Ocultar9 = $(event.target).parents('[id^=tdThisDay]').length == 1;
           var Ocultar10 = $(event.target).parents('[id^=tdThisWeek]').length == 1;
           var Ocultar11 = $(event.target).parents('[id^=tdThisMonth]').length == 1;
           var Ocultar12 = $(event.target).parents('[id^=tdThisYear]').length == 1;

           if (!((Ocultar1) || (Ocultar2) || (Ocultar3) || (Ocultar4) || (Ocultar5) || (Ocultar6) || (Ocultar7) || (Ocultar8) || (Ocultar9) || (Ocultar10) || (Ocultar11) || (Ocultar12))) {
               OcultarBotonesSeleccion();
           }
       });

       //funcion que se ejecuta en el momento en el que se cambia el valor del contenido del combo de entorno
       $(function () {
           $('#<%= ddlEnviroment.ClientID %>').change(function () {
               //debugger;
               //mostrar ventana cargando
               $('[id$=divCargando]').css("display", "block").css("width", "100px").css("margin", "18% auto");
               $('[id$=divTabContainer]').css("display", "none");
               setTimeout(CambioEntorno, 200);
           });
       });

       function CambioEntorno() {
           if ($('[id$=lblEntorno]').css('visibility') == 'visible') {
               var entorno = $('[id$=lblCodEntorno]').text();
           } else {
               var entorno = $('[id$=ddlEnviroment]').val();
           }
           if ($('[id$=ddlEnviromentPymes]').css("display") != "none") {
               var pyme = $('[id$=ddlEnviromentPymes]').val();
           } else {
               var pyme = null;
           }

           var kpi = $('[id$=ddlRecentActivity2]').val();
           CrearRadialGaugeMedia(fecDesde, fecHasta, entorno, pyme, Rango);
           CrearGraficaPerformanceEvolution(fecDesde, fecHasta, entorno, pyme, Rango);
           $("#divPerformanceEvolution").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30 }]);
           CrearGraficaRecentActivityKPI(fecDesde, fecHasta, entorno, pyme, Rango, kpi);
           $("#divRecentActivityKPI").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
           ComprobarGridKPIs(fecDesde, fecHasta, entorno, pyme, Rango);
           ComprobarGridLastDurationTimes(fecDesde, fecHasta, entorno, pyme, Rango, kpi);
           OcultarBotonesSeleccion();
           OcultarTextosBotonesSeleccion();


           if (Rango == "DIA") {
               $('[id$=lblHoy]').text(Texto);
               $('[id$=divHoy]').css("background-color", "#D3D3D3");
               $('[id$=divAhora]').css("background-color", "white");
               $('[id$=divEstaSemana]').css("background-color", "white");
               $('[id$=divEsteMes]').css("background-color", "white");
               $('[id$=divEsteAnyo]').css("background-color", "white");
           } else if (Rango == "HORA") {
               $('[id$=lblAhora]').text(Texto);
               $('[id$=divHoy]').css("background-color", "white");
               $('[id$=divAhora]').css("background-color", "#D3D3D3");
               $('[id$=divEstaSemana]').css("background-color", "white");
               $('[id$=divEsteMes]').css("background-color", "white");
               $('[id$=divEsteAnyo]').css("background-color", "white");
           } else if (Rango == "SEMANA") {
               $('[id$=lblEstaSemana]').text(Texto);
               $('[id$=divHoy]').css("background-color", "white");
               $('[id$=divAhora]').css("background-color", "white");
               $('[id$=divEstaSemana]').css("background-color", "#D3D3D3");
               $('[id$=divEsteMes]').css("background-color", "white");
               $('[id$=divEsteAnyo]').css("background-color", "white");
           } else if (Rango == "MES") {
               $('[id$=lblEsteMes]').text(Texto);
               $('[id$=divHoy]').css("background-color", "white");
               $('[id$=divAhora]').css("background-color", "white");
               $('[id$=divEstaSemana]').css("background-color", "white");
               $('[id$=divEsteMes]').css("background-color", "#D3D3D3");
               $('[id$=divEsteAnyo]').css("background-color", "white");
           } else if (Rango == "ANYO") {
               $('[id$=lblEsteAnyo]').text(Texto);
               $('[id$=divHoy]').css("background-color", "white");
               $('[id$=divAhora]').css("background-color", "white");
               $('[id$=divEstaSemana]').css("background-color", "white");
               $('[id$=divEsteMes]').css("background-color", "white");
               $('[id$=divEsteAnyo]').css("background-color", "#D3D3D3");
           }
           $('[id$=divCargando]').css("display", "none");
           $('[id$=divTabContainer]').css("display", "block");
       }

    </script>  

    <script type="text/javascript">
        //Codigos de los colores que evaluan el rendimiento
        var codigoColorRojo = "rgba(211, 64, 75, 1)";
        var codigoColorAmarillo = "rgba(253, 189, 72, 1)";
        var codigoColorVerde = "rgba(0, 128, 0, 1)";
        var mediaRendimiento = 0.0;

        function CrearRadialGaugeMedia(fechaInicio, fechaFin, entorno, pyme, rango) {
            var currDataSource;
            var currData;
            var datos;
            var data;
            var timeoffset = new Date().getTimezoneOffset();
            var to = rutaFS + 'BI/VisorRendimiento.aspx/CargarRadialGaugeMedia';
            $.when($.ajax({
                type: 'POST',
                url: to,
                data: JSON.stringify({ fechaInicio: fechaInicio, fechaFin: fechaFin, rango: rango, entorno: entorno, pyme: pyme, timeoffset: timeoffset }),
                contentType: 'application/json;charset=utf-8',
                dataType: 'json',
                async: false
            })).done(function (msg) {
                mediaRendimiento = 0.0;
                datos = null;
                datos = msg.d;
                data = [];
                if (datos != null && datos.length > 0) {
                    for (var i = 0; i < datos.length; i++) {
                        data[i] = { LabelRadialGaude: datos[i].radialGaugeValue.toString() };
                        mediaRendimiento = datos[i].radialGaugeValue;
                    }
                }
                currDataSource = null;
                currData = null;
                currData = data;
                currDataSource = new $.ig.DataSource({ dataSource: null });
                currDataSource = new $.ig.DataSource({ dataSource: currData });
            });
            $("#divPM").remove();
            $("#lblPM").remove();
            $("#divRadialGauge").css("display", "inherit");
            if (datos != null && datos.length > 0) {
                CrearRadialGauge(datos[0].radialGaugeValue);
            }
        };

        function CrearGraficaPerformanceEvolution(fechaInicio, fechaFin, entorno, pyme, rango) {
            var colorAVG;
            if (mediaRendimiento > 100) {
                colorAVG = codigoColorVerde;
            } else if (mediaRendimiento > 60) {
                colorAVG = codigoColorAmarillo;
            } else {
                colorAVG = codigoColorRojo;
            }
            var currDataSource;
            var currData;
            var datos;
            var data;
            var timeoffset = new Date().getTimezoneOffset();
            var to = rutaFS + 'BI/VisorRendimiento.aspx/CargarPerformanceEvolution';
            $.when($.ajax({
                type: 'POST',
                url: to,
                data: JSON.stringify({ fechaInicio: fechaInicio, fechaFin: fechaFin, rango: rango, entorno: entorno, pyme: pyme, timeoffset: timeoffset }),
                contentType: 'application/json;charset=utf-8',
                dataType: 'json',
                async: false
            })).done(function (msg) {
                datos = null;
                datos = msg.d;
                data = [];
                if (datos != null && datos.length > 0) {
                    for (var i = 0; i < datos.length; i++) {
                        if (datos[i].rendimientoKPI == -1) {
                            if (i > 0) {
                                datos[i].rendimientoKPI = datos[i - 1].rendimientoKPI;
                            }
                            else {
                                datos[i].rendimientoKPI = 100;
                            }
                        }
                        if (datos[i].rendimientoKPI == -2) {
                            datos[i].rendimientoKPI = null;
                        }
                        data[i] = { LabelPerformanceEvolution: datos[i].fechaHoraKPI.toString(), ValuePerformanceEvolution: datos[i].rendimientoKPI, ValueAVG: mediaRendimiento };
                    }
                }
                currDataSource = null;
                currData = null;
                currData = data;
                currDataSource = new $.ig.DataSource({ dataSource: null });
                currDataSource = new $.ig.DataSource({ dataSource: currData });
            });
            $("#divPM").remove();
            $("#lblPM").remove();
            $("#divRadialGauge").css("display", "inherit");
            $("#divPerformanceEvolution").css("display", "inherit");
            $("#divPerformanceEvolution").igDataChart({
                width: "50%",
                height: "270px",
                leftMargin: 10,
                topMargin: 10,
                dataSource: currDataSource,
                axes: [{
                    name: "xAxis",
                    type: "categoryX",
                    label: "LabelPerformanceEvolution",
                    interval: 1,
                    labelAngle: 65,
                    labelExtent: 30
                },
                {
                    name: "yAxis",
                    type: "numericY",
                    crossingAxis: "xAxis",
                    crossingValue: "0",
                    maximumValue: 200,
                    minimumValue: 0

                }],
                series: [{
                    name: "seriesPerformanceEvolution",
                    type: "line",
                    xAxis: "xAxis",
                    yAxis: "yAxis",
                    minimumValue: 0,
                    maximumValue: 200,
                    valueMemberPath: "ValuePerformanceEvolution",
                    showTooltip: true,
                    tooltipTemplate: "tooltipTemplatePerformanceEvolution",
                    thickness: 3,
                    horizontalZoomable: true,
                    verticalZoomable: true,
                    brush: "#2380a8"
                },
                {
                    name: "seriesValueAVG",
                    type: "line",
                    xAxis: "xAxis",
                    yAxis: "yAxis",
                    valueMemberPath: "ValueAVG",
                    showTooltip: true,
                    tooltipTemplate: "tooltipTemplateValueAVG",
                    thickness: 2,
                    horizontalZoomable: true,
                    verticalZoomable: true,
                    brush: colorAVG
                }],

                horizontalZoomable: true,
                verticalZoomable: true,
                windowResponse: "immediate",

            });

            //                if (datos == null) {
            //                    $("#divPerformanceEvolution").css("display", "none");
            //                    //$("#panelMedioIzda2").append("<div id='divPM' style='height:130px'></div><span id='lblPM' style='font-size:20px; margin-left:35%'>NO DATA TO SHOW</span>");
            //                    $("#panelMedioIzda2").append("<div id='pmd' style='font-size:20px; height:130px; margin-top:130px; margin-left:35%'><span id='lblNoData'></span></div>");
            //                    $('[id$=lblNoData]').text(TextosVisorActividad[33]);
            //                    $("#divRadialGauge").css("display", "none");
            //                    $("#lblRadialGauge").text("");
            //                }
            //                //$("#divPerformanceEvolution").igDataChart("option", "overviewPlusDetailPaneVisibility", "visible");
        };
    </script>

    <script type="text/javascript">
        function CrearRadialGauge(needleValue) {
            $(function CrearRadialGauge() {
                $("#divRadialGauge").igRadialGauge({
                    height: "270px",
                    width: "47%",
                    font: "normal normal 14px/normal 'Arial'",
                    labelExtent: 0.79,
                    interval: 10,
                    labelInterval: 20,
                    transitionDuration: 1500,
                    value: 60,
                    centerX: 0.5,
                    centerY: 0.5,
                    minimumValue: 0,
                    maximumValue: 200,
                    backingOutline: "#2380a8",
                    backingStrokeThickness: 10,
                    backingOuterExtent: 0.93,
                    backingInnerExtent: 0.2,
                    scaleBrush: "E5E5E5",
                    scaleStartAngle: 132.5,
                    scaleEndAngle: 62.5,
                    scaleStartExtent: 0.5,
                    scaleEndExtent: 0.79,
                    scaleOversweep: 0,
                    needleStartExtent: 0,
                    needleEndExtent: 0.7,
                    tickStartExtent: 0.62,
                    tickEndExtent: 0.69,
                    tickBrush: "black",
                    minorTickBrush: "black",
                    minorTickStartExtent: 0.67,
                    minorTickEndExtent: 0.69,
                    fontBrush: "black",
                    needleShape: "triangle",
                    needleBrush: "black",
                    needleOutline: "black",
                    needleStartExtent: -0.2,
                    needleEndExtent: 0.7,
                    needleBaseFeatureExtent: 0.06,
                    needleBaseFeatureWidthRatio: 0.13,
                    needlePointFeatureExtent: 0.5,
                    needlePointFeatureWidthRatio: 0.06,
                    needlePivotWidthRatio: 0.12,
                    needlePivotInnerWidthRatio: 0.05,
                    needlePivotShape: "circleWithHole",
                    scaleSweepDirection: "clockwise",
                    ranges: [
                        {
                            name: "range1",
                            startValue: 0,
                            endValue: 60,
                            outerStartExtent: .5,
                            outerEndExtent: .57,
                            innerStartExtent: 0.5,
                            innerEndExtent: 0.5,
                            strokeThickness: 1.0,
                            outline: "#BF0000",
                            brush: codigoColorRojo
                        },
                        {
                            name: "range2",
                            startValue: 61,
                            endValue: 100,
                            outerStartExtent: .57,
                            outerEndExtent: .61,
                            innerStartExtent: 0.5,
                            innerEndExtent: 0.5,
                            strokeThickness: 0,
                            outline: "#E85600",
                            brush: codigoColorAmarillo
                        },
                        {
                            name: "range3",
                            startValue: 101,
                            endValue: 200,
                            outerStartExtent: .61,
                            outerEndExtent: .65,
                            innerStartExtent: 0.5,
                            innerEndExtent: 0.5,
                            strokeThickness: 0,
                            outline: "green",
                            brush: codigoColorVerde
                        }
                    ]
                });
                $("#divRadialGauge").igRadialGauge("option", "value", needleValue);
                if (needleValue > 100) {
                    $("#divRadialGauge").igRadialGauge("option", "needleOutline", codigoColorVerde);
                } else if (needleValue > 60) {
                    $("#divRadialGauge").igRadialGauge("option", "needleOutline", codigoColorAmarillo);
                } else {
                    $("#divRadialGauge").igRadialGauge("option", "needleOutline", codigoColorRojo);
                }
                $("#lblRadialGauge").text(((Math.round(needleValue * 100, 2)) / 100).toString() + '%');
            });
        }
    </script>

    <script type="text/javascript">
        var fecIni;
        var fecFin;
        var ent;
        var pym;
        var rang;
        var kp;

        function CrearGraficaRecentActivityKPI(fechaInicio, fechaFin, entorno, pyme, rango, kpi) {
            fecIni = fechaInicio;
            fecFin = fechaFin;
            ent = entorno;
            pym = pyme;
            rang = rango;
            kp = kpi;
            var timeoffset = new Date().getTimezoneOffset();
            var currDataSource;
            var currData;
            var datos;
            var data;
            var to = rutaFS + 'BI/VisorRendimiento.aspx/CargarRecentActivityKPI';
            $.when($.ajax({
                type: 'POST',
                url: to,
                data: JSON.stringify({ fechaInicio: fechaInicio, fechaFin: fechaFin, rango: rango, entorno: entorno, pyme: pyme, kpi: kpi, timeoffset: timeoffset }),
                contentType: 'application/json;charset=utf-8',
                dataType: 'json',
                async: false
            })).done(function (msg) {
                datos = null;
                datos = msg.d;
                data = [];
                if (datos != null) {
                    for (var i = 0; i < datos.length; i++) {
                        if (datos[i].tiempoMaxKPI == -2) {
                            datos[i].tiempoMaxKPI = null;
                        }
                        if (datos[i].tiempoMinKPI == -2) {
                            datos[i].tiempoMinKPI = null;
                        }
                        if (datos[i].tiempoAvgKPI == -2) {
                            datos[i].tiempoAvgKPI = null;
                        }
                        data[i] = { LabelRecentActivityKPI: datos[i].fechaHoraKPI, ValueMaxTAcceso: datos[i].tiempoMaxKPI, ValueMinTAcceso: datos[i].tiempoMinKPI, ValueAvgTAcceso: datos[i].tiempoAvgKPI, ValueTarget: datos[i].targetKPI };
                    }
                }
                currDataSource = null;
                currData = null;
                currData = data;
                currDataSource = new $.ig.DataSource({ dataSource: null });
                currDataSource = new $.ig.DataSource({ dataSource: currData });
                var arrayJson;
                arrayJson = null;
                arrayJson = currData;

            });
            $("#divRecentActivityKPI").css("display", "block");
            $('[id$=legend]').css("display", "block");
            $("#lblpmd").remove();
            $("#pmd").remove();
            $("#divRecentActivityKPI").igDataChart({
                width: "76%",
                height: "240px",
                leftMargin: 10,
                topMargin: 10,
                dataSource: currDataSource,
                axes: [{
                    name: "xAxis",
                    type: "categoryX",
                    label: "LabelRecentActivityKPI",
                    interval: 1, //intervalo entre lineas
                    labelAngle: 65,
                    labelExtent: 30
                },
                {
                    name: "yAxis",
                    type: "numericY",
                    crossingAxis: "xAxis",
                    crossingValue: "0"
                }],
                series: [{
                    name: "series1",
                    title: TextosVisorActividad[66].toUpperCase(),
                    type: "line",
                    xAxis: "xAxis",
                    yAxis: "yAxis",
                    valueMemberPath: "ValueMaxTAcceso",
                    showTooltip: true,
                    tooltipTemplate: "tooltipTemplateMaxTAcceso",
                    thickness: 3,
                    legend: {
                        element: "legend"
                    },
                    horizontalZoomable: true,
                    verticalZoomable: true,
                    brush: codigoColorRojo
                }, {
                    name: "series2",
                    title: TextosVisorActividad[61].toUpperCase(),
                    type: "line",
                    xAxis: "xAxis",
                    yAxis: "yAxis",
                    valueMemberPath: "ValueAvgTAcceso",
                    showTooltip: true,
                    tooltipTemplate: "tooltipTemplateAvgTAcceso",
                    thickness: 3,
                    legend: {
                        element: "legend"
                    },
                    horizontalZoomable: true,
                    verticalZoomable: true,
                    brush: codigoColorAmarillo
                }, {
                    name: "series3",
                    title: TextosVisorActividad[67].toUpperCase(),
                    type: "line",
                    xAxis: "xAxis",
                    yAxis: "yAxis",
                    valueMemberPath: "ValueMinTAcceso",
                    showTooltip: true,
                    tooltipTemplate: "tooltipTemplateMinTAcceso",
                    thickness: 3,
                    legend: {
                        element: "legend"
                    },
                    horizontalZoomable: true,
                    verticalZoomable: true,
                    brush: codigoColorVerde
                }, {
                    name: "series4",
                    title: TextosVisorActividad[62].toUpperCase(),
                    type: "line",
                    xAxis: "xAxis",
                    yAxis: "yAxis",
                    valueMemberPath: "ValueTarget",
                    showTooltip: true,
                    tooltipTemplate: "tooltipTemplateTarget",
                    thickness: 2,
                    legend: {
                        element: "legend"
                    },
                    horizontalZoomable: true,
                    verticalZoomable: true,
                    brush: "#2380a8"
                }],

                horizontalZoomable: true,
                verticalZoomable: true,
                windowResponse: "immediate"
            });
            if (datos == null) {
                $("#divRecentActivityKPI").css("display", "none");
                $('[id$=legend]').css("display", "none");
                //$("#panelMedioDcha2").append("<div id='pmd' style='height:112px'></div><span id='lblpmd'style='font-size:20px; margin-left:35%'>NO DATA TO SHOW</span>");
                $("#panelMedioDcha2").append("<div id='pmd' style='font-size:20px; height:112px; margin-top:130px; margin-left:35%'><span id='lblNoData'></span></div>");
                $('[id$=lblNoData]').text(TextosVisorActividad[33]);
            }
            //$("#divRecentActivityKPI").igDataChart("option", "overviewPlusDetailPaneVisibility", "visible");
        };
    </script>

    <script type="text/javascript">
        //grid de kpis
        function ComprobarGridKPIs(fechaInicio, fechaFin, entorno, pyme, rango) {
            $(function () {
                var dataKPI = [];
                var datosKPI = [];
                var timeoffset = new Date().getTimezoneOffset();
                var denominacionHeader = (TextosVisorActividad[58].length == 4) ? TextosVisorActividad[58].slice(0, -1) : TextosVisorActividad[58];
                var to = rutaFS + 'BI/VisorRendimiento.aspx/CargarListaKPIsRango';
                $.when($.ajax({
                    type: 'POST',
                    url: to,
                    contentType: 'application/json;charset=utf-8',
                    data: JSON.stringify({ "fechaInicio": fechaInicio, "fechaFin": fechaFin, "entorno": entorno, "pyme": pyme, "rango": rango, "timeoffset": timeoffset }),
                    dataType: 'json',
                    async: false
                })).done(function (msg) {
                    datosKPI = null;
                    datosKPI = msg.d;
                    if (datosKPI != null) {
                        for (var i = 0; i < datosKPI.length; i++) {
                            if (datosKPI[i].rendimientoKPI < 0) {
                                datosKPI[i].rendimientoKPI = 0;
                            }
                            dataKPI.push({ Denominacion: datosKPI[i].denKPI, numAccesos: parseInt(datosKPI[i].numAccesosKPI), maxTime: parseFloat(datosKPI[i].tiempoMaxKPI), avgTime: parseFloat(datosKPI[i].tiempoAvgKPI), minTime: parseFloat(datosKPI[i].tiempoMinKPI), target: parseFloat(datosKPI[i].targetKPI), porcentaje: parseFloat(datosKPI[i].rendimientoKPI) });
                        }
                    }
                    $("#divGridKPI").igGrid({
                        autoGenerateColumns: true,
                        width: "100%",
                        height: "100%",
                        dataSource: dataKPI, //JSON Array defined above 
                        features: [
						 {
						     name: "Tooltips",
						     columnSettings: [
									{ columnKey: "Denominacion", allowTooltips: true }
								],
						     tooltipShowing: function (e, args) {
						         var message =
									"<div class='event-args'>" +
										"<span class='tooltip-italic'>" + args.value + "</span></div>";
						         showEvent(message);
						     }
						 },
						 {
						     name: 'Sorting',
						     type: "local",
						     columnSorted: fixRowHeight
						 }],
                        columns: [
                                { headerText: denominacionHeader, key: "Denominacion", dataType: "string", width: "31%" },
                                { headerText: TextosVisorActividad[60], key: "numAccesos", dataType: "integer", width: "10%" },
                                { headerText: TextosVisorActividad[66], key: "maxTime", dataType: "double", formatter: function (val) { return val.toString().replace(".", ","); }, width: "13%" },
                                { headerText: TextosVisorActividad[61], key: "avgTime", dataType: "double", formatter: function (val) { return val.toString().replace(".", ","); }, width: "13%" },
                                { headerText: TextosVisorActividad[67], key: "minTime", dataType: "double", formatter: function (val) { return val.toString().replace(".", ","); }, width: "13%" },
                                { headerText: TextosVisorActividad[62], key: "target", dataType: "double", formatter: function (val) { return val.toString().replace(".", ","); }, width: "10%" },
                                { headerText: "%", key: "porcentaje", dataType: "double", formatter: function (val) { return drawPerformance(val); }, width: "10%" }
                        ],
                        dataRendered: function (evt, ui) {
                            ui.owner.element.find("tr td:nth-child(2)").css("text-align", "right");
                            ui.owner.element.find("tr td:nth-child(3)").css("text-align", "right");
                            ui.owner.element.find("tr td:nth-child(4)").css("text-align", "right");
                            ui.owner.element.find("tr td:nth-child(5)").css("text-align", "right");
                            ui.owner.element.find("tr td:nth-child(6)").css("text-align", "right");
                            ui.owner.element.find("tr td").css("height", "100%");
                            ui.owner.element.find("tr td").css("word-wrap", "break-word");
                        }
                    });
                });
            });
        }

        function fixRowHeight(evt, ui) {
            ui.owner.element.find("tr td").css("height", "100%");
            ui.owner.element.find("tr td").css("word-wrap", "break-word");
        }

        function drawPerformance(val) {
            if (val < 60) {
                dataTemp = "<div id='divPorcentaje'> <span id='lblporcentaje' style='float:left; margin-left:5px; margin-bottom:-12px; width:100%; position:relative; z-index:5'>" + (Math.floor(val * 100) / 100).toString().replace(".", ",") + " %</span><img id='imgPorcentaje' src='images/red.png' style='width:" + val / 2 + "px; height:15px; float:left; position:relative; z-index:3'/> </div>";
            } else if ((val > 59) && (val < 100)) {
                dataTemp = "<div id='divPorcentaje'> <span id='lblporcentaje' style='float:left; margin-left:5px; margin-bottom:-12px; width:100%; position:relative; z-index:5'>" + (Math.floor(val * 100) / 100).toString().replace(".", ",") + " %</span><img id='imgPorcentaje' src='images/orange.png' style='width:" + val / 2 + "px; height:15px; float:left; position:relative; z-index:3'/> </div>";
            } else {
                dataTemp = "<div id='divPorcentaje'> <span id='lblporcentaje' style='float:left; margin-left:5px; margin-bottom:-12px; width:100%; position:relative; z-index:5'>" + (Math.floor(val * 100) / 100).toString().replace(".", ",") + " %</span><img id='imgPorcentaje' src='images/green.png' style='width:" + val / 2 + "px; height:15px; float:left; position:relative; z-index:3'/> </div>";
            }
            return dataTemp;
        }                
    </script>

    <script type="text/javascript">
        $(function () {
            $('[id$=ddlRecentActivity2]').change(function () {
                var kpi = $('[id$=ddlRecentActivity2]').val();
                CrearGraficaRecentActivityKPI(fecIni, fecFin, ent, pym, rang, kpi);
                $("#divRecentActivityKPI").igDataChart("option", "axes", [{ name: "xAxis", "labelAngle": 65, "labelExtent": 30}]);
                ComprobarGridLastDurationTimes(fecIni, fecFin, ent, pym, rang, kpi);
            });
        });
    </script>

    <script type="text/javascript">
        //Grid Last Duration Times
        function ComprobarGridLastDurationTimes(fechaInicio, fechaFin, entorno, pyme, rango, kpi) {
            $(function () {
                var dataLastDurationTimes = [];
                var datosLastDurationTimes = [];
                var timeoffset = new Date().getTimezoneOffset();
                var to = rutaFS + 'BI/VisorRendimiento.aspx/CargarListaPorRangoKPI';
                $.when($.ajax({
                    type: 'POST',
                    url: to,
                    contentType: 'application/json;charset=utf-8',
                    data: JSON.stringify({ "fechaInicio": fechaInicio, "fechaFin": fechaFin, "entorno": entorno, "pyme": pyme, "rango": rango, "kpi": kpi, "timeoffset": timeoffset }),
                    dataType: 'json',
                    async: false
                })).done(function (msg) {
                    datosLastDurationTimes = null;
                    datosLastDurationTimes = msg.d;
                    if (datosLastDurationTimes != null) {
                        if (datosLastDurationTimes[0].usuAccesoKPI === null) {
                            $("#divGridLastDurationTimes2").css("display", "none");
                            $("#divGridLastDurationTimes1").css("display", "block");
                            $('[id$=lblLastDurationTimes]').text($('[id$=ddlRecentActivity2] option:selected').text());
                            $('[id$=lblLastDurationTimes]').css({ 'font-weight': 'bold' });
                            $('[id$=imgLastDurationTimes]').attr('src', 'images/0004.png');
                            for (var i = 0; i < datosLastDurationTimes.length; i++) {
                                if (datosLastDurationTimes[i].rendimientoKPI < 0) {
                                    datosLastDurationTimes[i].rendimientoKPI = 0;
                                }
                                dataLastDurationTimes.push({ fechaHora: datosLastDurationTimes[i].fechaHoraKPI.toString(), numAccesos: parseInt(datosLastDurationTimes[i].numAccesosKPI), maxTime: parseFloat(datosLastDurationTimes[i].tiempoMaxKPI), avgTime: parseFloat(datosLastDurationTimes[i].tiempoAvgKPI), minTime: parseFloat(datosLastDurationTimes[i].tiempoMinKPI), target: parseFloat(datosLastDurationTimes[i].targetKPI), porcentaje: parseFloat(datosLastDurationTimes[i].rendimientoKPI) });
                            }
                            $("#divGridLastDurationTimes1").igGrid({
                                autoGenerateColumns: true,
                                width: "100%",
                                height: "100%",
                                dataSource: dataLastDurationTimes, //JSON Array defined above 
                                features: [
						         {
						             name: 'Sorting',
						             type: "local",
						             columnSorted: fixRowHeight
						         }],
                                columns: [
                                        { headerText: TextosVisorActividad[49], key: "fechaHora", dataType: "string", formatter: function (val) { var textoArr = val.toString().split('|'); return (textoArr.length < 2) ? textoArr[0] : textoArr[1]; }, width: "15%" },
                                        { headerText: TextosVisorActividad[60], key: "numAccesos", dataType: "integer", width: "13%" },
                                        { headerText: TextosVisorActividad[66], key: "maxTime", dataType: "double", formatter: function (val) { return val.toString().replace(".", ","); }, width: "15%" },
                                        { headerText: TextosVisorActividad[61], key: "avgTime", dataType: "double", formatter: function (val) { return val.toString().replace(".", ","); }, width: "15%" },
                                        { headerText: TextosVisorActividad[67], key: "minTime", dataType: "double", formatter: function (val) { return val.toString().replace(".", ","); }, width: "15%" },
                                        { headerText: TextosVisorActividad[62], key: "target", dataType: "double", formatter: function (val) { return val.toString().replace(".", ","); }, width: "12%" },
                                        { headerText: "%", key: "porcentaje", dataType: "double", formatter: function (val) { return drawPerformance(val); }, width: "15%" }
                                ],
                                dataRendered: function (evt, ui) {
                                    ui.owner.element.find("tr td:nth-child(2)").css("text-align", "right");
                                    ui.owner.element.find("tr td:nth-child(3)").css("text-align", "right");
                                    ui.owner.element.find("tr td:nth-child(4)").css("text-align", "right");
                                    ui.owner.element.find("tr td:nth-child(5)").css("text-align", "right");
                                    ui.owner.element.find("tr td:nth-child(6)").css("text-align", "right");
                                    ui.owner.element.find("tr td").css("height", "100%");
                                    ui.owner.element.find("tr td").css("word-wrap", "break-word");
                                }
                            });
                        } else {
                            $("#divGridLastDurationTimes1").css("display", "none");
                            $("#divGridLastDurationTimes2").css("display", "block");
                            $('[id$=lblLastDurationTimes]').text(TextosVisorActividad[59] + ' - ' + $('[id$=ddlRecentActivity2] option:selected').text());
                            $('[id$=lblLastDurationTimes]').css({ 'font-weight': 'bold' });
                            $('[id$=imgLastDurationTimes]').attr('src', 'images/0003.png');
                            for (var i = 0; i < datosLastDurationTimes.length; i++) {
                                if (datosLastDurationTimes[i].rendimientoKPI < 0) {
                                    datosLastDurationTimes[i].rendimientoKPI = 0;
                                }
                                dataLastDurationTimes.push({ fechaHora: datosLastDurationTimes[i].fechaHoraKPI.toString(), usuAcceso: datosLastDurationTimes[i].usuAccesoKPI.toString(), uonAcceso: datosLastDurationTimes[i].uonAccesoKPI.toString(), tiempoAcceso: parseFloat(datosLastDurationTimes[i].tiempoAccesoKPI), target: parseFloat(datosLastDurationTimes[i].targetKPI), porcentaje: parseFloat(datosLastDurationTimes[i].rendimientoKPI) });
                            }
                            $("#divGridLastDurationTimes2").igGrid({
                                autoGenerateColumns: true,
                                width: "100%",
                                height: "100%",
                                dataSource: dataLastDurationTimes, //JSON Array defined above 
                                features: [
						         {
						             name: 'Sorting',
						             type: "local",
						             columnSorted: fixRowHeight
						         }],
                                columns: [
                                        { headerText: TextosVisorActividad[49], key: "fechaHora", dataType: "string", formatter: function (val) { var textoArr = val.toString().split('|'); return (textoArr.length < 2) ? textoArr[0] : textoArr[1]; }, width: "15%" },
                                        { headerText: TextosVisorActividad[68], key: "usuAcceso", dataType: "string", width: "18%" },
                                        { headerText: TextosVisorActividad[69], key: "uonAcceso", dataType: "string", width: "25%" },
                                        { headerText: TextosVisorActividad[63], key: "tiempoAcceso", dataType: "double", formatter: function (val) { return val.toString().replace(".", ","); }, width: "15%" },
                                        { headerText: TextosVisorActividad[62], key: "target", dataType: "double", formatter: function (val) { return val.toString().replace(".", ","); }, width: "12%" },
                                        { headerText: "%", key: "porcentaje", dataType: "double", formatter: function (val) { return drawPerformance(val); }, width: "15%" }
                                ],
                                dataRendered: function (evt, ui) {
                                    ui.owner.element.find("tr td:nth-child(4)").css("text-align", "right");
                                    ui.owner.element.find("tr td:nth-child(5)").css("text-align", "right");
                                    ui.owner.element.find("tr td").css("height", "100%");
                                    ui.owner.element.find("tr td").css("word-wrap", "break-word");
                                }
                            });
                        }
                    } else {
                        $("#divGridLastDurationTimes2").css("display", "none");
                        $("#divGridLastDurationTimes1").css("display", "block");
                        $('[id$=lblLastDurationTimes]').text($('[id$=ddlRecentActivity2] option:selected').text());
                        $('[id$=lblLastDurationTimes]').css({ 'font-weight': 'bold' });
                        $('[id$=imgLastDurationTimes]').attr('src', 'images/0004.png');
                        $("#divGridLastDurationTimes1").igGrid({
                            autoGenerateColumns: true,
                            width: "100%",
                            height: "100%",
                            dataSource: dataLastDurationTimes, //JSON Array defined above 
                            features: [
						     {
						         name: 'Sorting',
						         type: "local",
						         columnSorted: fixRowHeight
						     }],
                            columns: [
                                    { headerText: TextosVisorActividad[49], key: "fechaHora", dataType: "string", formatter: function (val) { var textoArr = val.toString().split('|'); return (textoArr.length < 2) ? textoArr[0] : textoArr[1]; }, width: "15%" },
                                    { headerText: TextosVisorActividad[60], key: "numAccesos", dataType: "integer", width: "13%" },
                                    { headerText: TextosVisorActividad[66], key: "maxTime", dataType: "double", formatter: function (val) { return val.toString().replace(".", ","); }, width: "15%" },
                                    { headerText: TextosVisorActividad[61], key: "avgTime", dataType: "double", formatter: function (val) { return val.toString().replace(".", ","); }, width: "15%" },
                                    { headerText: TextosVisorActividad[67], key: "minTime", dataType: "double", formatter: function (val) { return val.toString().replace(".", ","); }, width: "15%" },
                                    { headerText: TextosVisorActividad[62], key: "target", dataType: "double", formatter: function (val) { return val.toString().replace(".", ","); }, width: "12%" },
                                    { headerText: "%", key: "porcentaje", dataType: "double", formatter: function (val) { return drawPerformance(val); }, width: "15%" }
                            ],
                            dataRendered: function (evt, ui) {
                                ui.owner.element.find("tr td:nth-child(2)").css("text-align", "right");
                                ui.owner.element.find("tr td:nth-child(3)").css("text-align", "right");
                                ui.owner.element.find("tr td:nth-child(4)").css("text-align", "right");
                                ui.owner.element.find("tr td:nth-child(5)").css("text-align", "right");
                                ui.owner.element.find("tr td:nth-child(6)").css("text-align", "right");
                                ui.owner.element.find("tr td").css("height", "100%");
                                ui.owner.element.find("tr td").css("word-wrap", "break-word");
                            }
                        });
                    }
                });
            });
        }

    </script>

     

    <div id="divCargando" style="width:100%">
        <asp:panel runat="server" id="PanelLoad" HorizontalAlign="Center" style="text-align:center">
            <img id="imgCarga0" runat="server" alt="Loading... Please wait!!" 
            src="images/cargando.gif" /><br /><br />
            <asp:label ID="lblcargando" runat="server" style="margin-left:15px"></asp:label>
            &nbsp;
        </asp:panel>
    </div>
    <div id="divTabContainer" style="display:none">
         <div id="Div1" style="width:96%; height:800px; margin-left:25px">
                <div id="panelSuperiorFondo" style="height:60px; border:1px black solid; background-color:#D3D3D3; min-width:940px">
                    <div id="panelSuperior" style="clear:both; height:47px; border: 1px black solid; background-color:White; margin-top:5px; margin-bottom:5px; margin-left:5px; margin-right:5px; min-width:915px">
                        <div id="divEnviroment" style="float:left; width:27%; margin-left:5px; margin-top:13px; vertical-align:middle">   
                            <img src="images/0005.png" alt="ENVIROMENT" style="float:left; margin-top:-5px"/>
                            <span id="lblEnviroment" style="font-size:12px; float:left; margin-left:5px"></span>
                            <asp:DropDownList ID="ddlEnviroment" runat="server" style="font-size:12px; float:left; margin-left:5px" enabled="true"/>
                            <asp:label ID="lblEntorno" runat="server" style="font-size:12px; float:left; margin-left:5px"></asp:label>
                            <asp:label ID="lblCodEntorno" runat="server" style="display:none"></asp:label>
                        </div>

                        <div id="divEnviromentPymes" style="float:left; width:70%; margin-left:5px; vertical-align:middle">
                            <span id="lblEnviromentPymes" style="font-size:12px; float:left;  margin-top:13px"></span>&nbsp
                            <select id="ddlEnviromentPymes" style="font-size:12px; float:left; margin-left:5px;  margin-top:13px;"></select>
                            <table style="float:left; margin-top:3px; margin-left:20px" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <table id="divAhora" cellspacing="0" cellpadding="2" border="0" style="border:solid 1pt #AAAAAA; cursor:pointer" onclick="btnNow_Click();" >
                                            <tr>
                                                <td align="center" valign="middle">
	                                                <img src="Images/va_Now.png" alt="NOW"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td  align="center" nowrap="nowrap" valign="middle">
                                                    <div id="lblAhora" style="font-size:8px ;font:tahoma"/>
                                                </td>
                                            </tr>
                                        </table>                                 
                                    </td>
                                    <td>
                                        <table id="divAhora2"  cellpadding="2" cellspacing="0"> 
                                            <tr>
                                                <td align="left" style="border:solid 1pt #AAAAAA; height:31.5px; margin-left:-10px;" onclick="btnAhora_Click(); return false;" >
	                                                <img src="Images/va_desplegable.png" alt="" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                    <td width="30"></td>
                                    <td>
                                        <table id="divHoy" cellspacing="0" cellpadding="2" border="0" style="border:solid 1pt #AAAAAA; cursor:pointer" onclick="btnToday_Click();" >
                                            <tr>
                                                <td align="center" valign="middle">
	                                                <img src="Images/va_Today.png" alt="TODAY"/>
                                                </td>                                            
                                            </tr>
                                            <tr>
                                                <td  align="center" nowrap="nowrap" valign="middle" width="30">
                                                    <div id="lblHoy" style="font-size:8px ;font:tahoma"/>
                                                </td>
                                            </tr>
                                        </table> 
                                    </td>
                                    <td>
                                        <table id="divHoy2"  cellpadding="2" cellspacing="0"> 
                                            <tr>
                                                <td align="left" style="border:solid 1pt #AAAAAA; height:31.5px; margin-left:-10px;" onclick="btnHoy_Click(); return false;" >
	                                                <img src="Images/va_desplegable.png" alt="" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="30"></td>
                                    <td>
                                            <table id="divEstaSemana" cellspacing="0" cellpadding="2" border="0" style="border:solid 1pt #AAAAAA; cursor:pointer" onclick="btnThisWeek_Click();">
                                            <tr>
                                                <td align="center" valign="middle">
	                                                <img src="Images/va_Semana.png" alt="WEEK"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td  align="center" nowrap="nowrap" valign="middle">
                                                    <div id="lblEstaSemana" style="font-size:8px ;font:tahoma"/>
                                                </td>
                                            </tr>
                                            </table> 
                                    </td>
                                    <td>
                                        <table id="divEstaSemana2"  cellpadding="2" cellspacing="0"> 
                                            <tr>
                                                <td align="left" style="border:solid 1pt #AAAAAA; height:31.5px; margin-left:-10px;" onclick="btnEstaSemana_Click(); return false;" >
	                                                <img src="Images/va_desplegable.png" alt="" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                    <td width="30"></td>
                                    <td>
                                            <table id="divEsteMes" cellspacing="0" cellpadding="2" border="0" style="border:solid 1pt #AAAAAA; cursor:pointer" onclick="btnThisMonth_Click();" >
                                            <tr>
                                                <td align="center" valign="middle">
	                                                <img src="Images/va_mes.png" alt="MONTH"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td  align="center" nowrap="nowrap" valign="middle">
                                                    <div id="lblEsteMes" style="font-size:8px ;font:tahoma"/>
                                                </td>
                                            </tr>
                                            </table> 
                                    </td>
                                    <td>
                                        <table id="divEsteMes2"  cellpadding="2" cellspacing="0"> 
                                            <tr>
                                                <td align="left" style="border:solid 1pt #AAAAAA; height:31.5px; margin-left:-10px;" onclick="btnEsteMes_Click(); return false;" >
	                                                <img src="Images/va_desplegable.png" alt="" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>


                                    <td width="30"></td>
                                    <td>
                                            <table id="divEsteAnyo" cellspacing="0" cellpadding="2" border="0" style="border:solid 1pt #AAAAAA; cursor:pointer" onclick="btnThisYear_Click();" >
                                            <tr>
                                                <td align="center" valign="middle">
	                                                <img src="Images/va_Year.png" alt="YEAR" style="width:20px"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td  align="center" nowrap="nowrap" valign="middle">
                                                    <div id="lblEsteAnyo" style="font-size:8px ;font:tahoma"/>
                                                </td>
                                            </tr>
                                            </table> 
                                    </td>
                                    <td>
                                        <table id="divEsteAnyo2"  cellpadding="2" cellspacing="0"> 
                                            <tr>
                                                <td align="left" style="border:solid 1pt #AAAAAA; height:31.5px; margin-left:-10px;" onclick="btnEsteAnyo_Click(); return false;">
	                                                <img src="Images/va_desplegable.png" alt="" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>
                            
                                <tr style="margin-top:5px">
                                    <td>    
                                        <span id="lblNow" style="display:none; border:1px solid black; width:91px; position: absolute; background-color:White; font-size:10px; text-align:center; margin-top:-3px; z-index:5; cursor:pointer" onclick="btnNow_Click(); return false"></span><br />
                                        <span id="lblSelectHour" style="display:none; border:1px solid black; z-index:2; width:91px; position:absolute; background-color: White; font-size:10px;margin-top:-5px; z-index:5"></span>
                                        <div id="divSelectHour" style="width:90px; height:200px; overflow-y: scroll; padding-top:5px; display:none; margin-top: -5px; margin-left:93px; z-index:5; background-color:White; position:absolute; cursor:pointer">
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('00','01');"> 00:00-01:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('01','02');"> 01:00-02:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('02','03');"> 02:00-03:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('03','04');"> 03:00-04:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('04','05');"> 04:00-05:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('05','06');"> 05:00-06:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('06','07');"> 06:00-07:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('07','08');"> 07:00-08:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('08','09');"> 08:00-09:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('09','10');"> 09:00-10:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('10','11');"> 10:00-11:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('11','12');"> 11:00-12:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('12','13');"> 12:00-13:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('13','14');"> 13:00-14:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('14','15');"> 14:00-15:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('15','16');"> 15:00-16:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('16','17');"> 16:00-17:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('17','18');"> 17:00-18:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('18','19');"> 18:00-19:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('19','20');"> 19:00-20:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('20','21');"> 20:00-21:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('21','22');"> 21:00-22:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('22','23');"> 22:00-23:00 </span>
                                            <span style="display:block; border:1px solid black; cursor:pointer; font-size:10px; padding-bottom: 2px; padding-top: 2px" onclick="Horas('23','00');"> 23:00-00:00 </span>    
                                        </div>                                
                                    </td>
                                    <td width="10px"></td>
                                    <td width="30px"></td>
                                    <%--<td style="width:30px; margin-left:30px"></td>--%>
                                    <td id="tdThisDay">
                                        <span id="lblSelectDay" style="display:none; border: 1px solid black; width:90px; position:absolute; background-color:White; font-size:10px; margin-top:10px; z-index:5; cursor:pointer"></span>
                                        <span id="lblToday" style="display:none; border: 1px solid black; width:90px; position:absolute; background-color: White; font-size:10px; margin-top:-3px; z-index:5; cursor:pointer" onclick="btnToday_Click();return false"></span><br />
                                        <input id="dpSelectDay" class="day-picker" type="text" style="display:none; width:90px; margin-top:-5px; margin-left:92px; position:absolute; cursor:pointer; font-size:10px; z-index:5"/>
                                    </td> 
                                    <td width="10px"></td>
                                    <td width="30px"></td>
                                    <%--<td style="width:30p<%--x; margin-left:30px"></td>--%>
                                    <td id="tdThisWeek">
                                        <span id="lblThisWeek" style="display:none; border: 1px solid black; width:105px; position:absolute; background-color:White; font-size:10px; margin-top:-3px; z-index:5; cursor:pointer" onclick="btnThisWeek_Click(); return false"></span><br />
                                        <span id="lblSelectWeek" style="display:none; border:1px solid black; width:105px; position:absolute; background-color:White; font-size:10px;margin-top:-4px; z-index:5; cursor:pointer"></span>       
                                        <input id="dpSelectWeek" class="week-picker" type="text" style="display:none; width:90px; margin-top:-4px; margin-left:107px; position:absolute; cursor:pointer; font-size:10px; z-index:5"/>                       
                                    </td>
                                    <td width="10px"></td>
                                    <td width="30px"></td>
                                    <%--<td style="width:30px; margin-left:30px"></td>--%>
                                    <td id="tdThisMonth">
                                        <span id="lblThisMonth" style="display:none; border: 1px solid black; width:90px; position:absolute; background-color:White; font-size:10px; z-index:5; margin-top:-3px; cursor:pointer" onclick="btnThisMonth_Click(); return false"></span><br />
                                        <span id="lblSelectMonth" style="display:none; border: 1px solid black; width:90px; position:absolute; background-color:White; font-size:10px; margin-top:-4px; z-index:5"></span>
                                        <div id="divSeleccionMes" style="display:none; margin-top:-4px; margin-left:201px; position:absolute; z-index:5">
                                            <select id="ddlSelectYear" style="display:none; width:80px; cursor:pointer; margin-left:-109px; font-size:10px"></select>
                                            <select id="ddlSelectMonth" style="display:none; width:80px; cursor: pointer; margin-left:-109px; font-size:10px"></select>
                                        </div>
                                    </td>
                                    <td width="10px"></td>
                                    <td width="30px"></td>
                                    <%--<td style="width:30px; margin-left:30px"></td>--%>
                                    <td id="tdThisYear">
                                        <span id="lblThisYear" style="display:none; background-color:White; border: 1px solid black; width:90px; position:absolute; font-size:10px; z-index:5; margin-top:-3px; text-align:left; cursor:pointer" onclick="btnThisYear_Click(); return false"></span><br />
                                        <span id="lblSelectYear" style="display:none; background-color:White; border: 1px solid black; width:90px; position:absolute; font-size:10px; margin-top:-4px; z-index:5; text-align:left"></span> 
                                        <div id="divSeleccionAnyo" style="display:none; margin-left:211.5px; margin-top:-4px; position:absolute; z-index:5">
                                            <select id="ddlSelectYearAnyo" style="display:none; cursor:pointer; margin-left:-119px; font-size:10px"></select>
                                        </div>  
                                    </td>
                                </tr>
                            </table>   
                        </div>                                         
                    </div>
                </div>
     
            <div id="tabs-2" style="width:100%">    
                <div id="PanelMedio2" style="height:350px"  onclick="OcultarBotonesSeleccion(); return true;">
                    <div id="panelMedioFondoIzda2" style="background-color:#D3D3D3; border:1px black solid; margin-top:10px; width:49%; float:left">
                        <div id="panelMedioIzdaCabecera2" style="background-color:White; border:1px black solid; height:30px; margin-top:5px; margin-bottom:5px; margin-left:5px; margin-right:5px; position:relative; z-index:3">
                            <img id="imgOverallPerformance" style="margin-top:3px; float:left; margin-left:5px" alt="Overall performance" src="images/0003.png"/>
                            <div id="divOverallPerformance" style="margin-top:7px; margin-left:5px; float:left">
                                <span id="lblOverallPerformance"></span>
                            </div>
                        </div>
                        <div id="panelMedioIzda2" style="background-color:White; border:1px black solid; height:290px">
                            <div id="divRadialGauge" style="width:33%; height:270px; margin-left:5px; margin-top:10px; border: 1px solid black; border-radius:5px; background-color:white; float:left">
                                <span id="lblRadialGauge" style="width:100%; margin-top:210px; text-align: center; position:absolute; font-weight:bold; font-size:16px; z-index:5" >60%</span>
                            </div>                               
                            <div id="divPerformanceEvolution" style="width:57%; float:right; margin-top:10px; margin-right:6px">                                    
                            </div>
                        </div> 
                    </div>
                    <div id="panelMedioFondoDcha2" style="background-color:#D3D3D3; border:1px black solid; margin-top:10px; width:49%; float:right">
                        <div id="panelMedioDchaCabecera2" style="background-color:White; border:1px black solid; height:30px; margin-top:5px; margin-bottom:5px; margin-left:5px; margin-right:5px">
                            <img id="imgRecentActivity2" style="margin-top:3px; float:left; margin-left:5px" alt="Recent Activity" src="images/0004.png"/>
                            <div id="divRecentActivity2" style="margin-top:7px; margin-left:5px; float:left">
                                <span id="lblRecentActivity2"></span>
                            </div>
                        </div>
                        <div id="panelMedioDcha2" style="background-color:White; border:1px black solid; height:290px; width:100% ">
                            <asp:DropDownList id="ddlRecentActivity2" runat="server" style="width:35%; font-size:12px; margin-left:10px; margin-top:8px; margin-bottom:-5px;"></asp:DropDownList>                              
                            <div id="divRecentActivityKPI" style="width:76%; float:left; margin-top:2%; margin-left:10px; margin-bottom:auto"></div>
                            <div id="legend" style="width:20%; min-height:60px; float:right; display:block; margin-top:2%; margin-right:10px; margin-bottom:auto""></div>
                        </div> 
                    </div>
                </div>
                <div id="PanelInferior2" style="height:380px" onclick="OcultarBotonesSeleccion(); return false;">
                    <div id="PanelInferiorFondoIzda2"  style="background-color:#D3D3D3; border:1px black solid; margin-top:10px; width:49%; float:left">
                        <div id="panelInferiorIzdaCabecera2" style="background-color:White; border:1px black solid; height:30px; margin-top:5px; margin-bottom:5px; margin-left:5px; margin-right:5px">
                            <img id="imgKPI" style="margin-top:3px; float:left; margin-left:5px" alt="KPIs" src="images/0006.png"/>
                            <div id="divKPI" style="margin-top:7px; margin-left:5px; float:left">
                                <span id="lblKPI"></span>
                            </div>
                        </div>
                        <div id="panelInferiorIzda2" style="border:1px black solid; height:310px; background-color:White">
                            <div id="divGridKPI" style="width:100%; float:left; height:308px"> 
                            </div>
                        </div> 
                    </div>
                    <div id="PanelInferiorFondoDcha2" style="background-color:#D3D3D3; border:1px black solid; margin-top:10px; width:49%; float:right">
                        <div id="panelInferiorDchaCabecera2" style="background-color:White; border:1px black solid; height:30px; margin-top:5px; margin-bottom:5px; margin-left:5px; margin-right:5px">
                            <img id="imgLastDurationTimes" style="margin-top:3px; float:left; margin-left:5px" alt="Last Duration Times" src="images/0003.png"/>
                            <div id="divLastDurationTimes" style="margin-top:7px; margin-left:5px; float:left">
                                <span id="lblLastDurationTimes"></span>
                            </div>
                        </div>
                        <div id="panelInferiorDcha2" style="border:1px black solid; height:310px; background-color:White">
                            <div id="divGridLastDurationTimes1" style="width:100%; float:left; height:308px">
                            </div>
                            <div id="divGridLastDurationTimes2" style="width:100%; float:left; height:308px">
                            </div>
                        </div>
                    </div>
                </div>                    
            </div>
        </div>
    </div>              
</asp:Content>
