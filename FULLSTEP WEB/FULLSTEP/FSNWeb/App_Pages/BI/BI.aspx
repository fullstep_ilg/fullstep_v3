﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master" CodeBehind="BI.aspx.vb" Inherits="Fullstep.FSNWeb.BI" %>

<%@ Import Namespace="Fullstep.FSNLibrary" %>

<%@ Import Namespace="System.IO" %>

<%@ Import Namespace="System.Data" %>

<asp:Content ID="ContHeadBI" ContentPlaceHolderID="head" runat="server">

<!-- INTEGRAR EL MÓDULO DE BI -->
    <script type="text/javascript" src="Silverlight.js"></script>
    <script type="text/javascript">
        function onSilverlightError(sender, args) {
            var appSource = "";
            if (sender != null && sender != 0) {
                appSource = sender.getHost().Source;
            }
            var errorType = args.ErrorType;
            var iErrorCode = args.ErrorCode;
            if (errorType == "ImageError" || errorType == "MediaError") {
                return;
            }
            var errMsg = "Unhandled Error in Silverlight Application " + appSource + "\n";
            errMsg += "Code: " + iErrorCode + "    \n";
            errMsg += "Category: " + errorType + "       \n";
            errMsg += "Message: " + args.ErrorMessage + "     \n";
            if (errorType == "ParserError") {
                errMsg += "File: " + args.xamlFile + "     \n";
                errMsg += "Line: " + args.lineNumber + "     \n";
                errMsg += "Position: " + args.charPosition + "     \n";
            }
            else if (errorType == "RuntimeError") {
                if (args.lineNumber != 0) {
                    errMsg += "Line: " + args.lineNumber + "     \n";
                    errMsg += "Position: " + args.charPosition + "     \n";
                }
                errMsg += "MethodName: " + args.methodName + "     \n";
            }
            throw new Error(errMsg);
        }

        function crearObjetoXmlHttpRequest()
        {
            if(window.XMLHttpRequest) {
                xmlobj = new XMLHttpRequest();
            }
            else
            {
            try {
                xmlobj = new ActiveXObject('Microsoft.XMLHTTP');
                }
            catch (e) {}
            }
            return xmlobj;
        }
        function AceptarClick(idCubo) 
        {
            document.getElementById('FormCubo').cubo.value = idCubo;
            document.getElementById('FormCubo').submit(); return true;
       }
       
    </script>
    <style type="text/css">
    html, body {
	    height: 100%;
	    overflow: auto;
    }
    body {
	    padding: 0;
	    margin: 0;
    }
    #silverlightControlHost {
	    height: 100%;
	    text-align:center;
    }
    </style>	
<!-- FIN INTEGRAR EL MÓDULO DE BI -->	

</asp:Content>
<asp:Content ID="ContBI" ContentPlaceHolderID="CPH4" runat="server">
    
    <%
    
        'Se solicitan los cubos disponibles a la plataforma   
    Dim listaCubos As Fullstep.FSNServer.CubosDenominacion = FSNServer.Get_Object(GetType(Fullstep.FSNServer.CubosDenominacion))
        listaCubos = Session("DsCubos")
        Session.Remove("DsCubos")
    'Si no existen cubos escribimos un mensaje en pantalla
    If (listaCubos.CubosDen Is Nothing) Then
        Dim Msg As String
        Msg = "<div align=""center"" style=""padding-top: 50px; font-size: 20px"">" & NoHayCubos & "</div>"
        Response.Write(Msg)
    Else
            Dim cubos As Fullstep.FSNServer.Cubos = FSNServer.Get_Object(GetType(Fullstep.FSNServer.Cubos))
            Dim cubo As Fullstep.FSNServer.Cubo = FSNServer.Get_Object(GetType(Fullstep.FSNServer.Cubo))
            If (listaCubos.CubosDen.Count = 1) Then
               
                Dim i As Integer = listaCubos.CubosDen(1).Id
                Dim ds As DataSet = cubos.Get_Cubo(i)
                Dim j As Integer = 0
                cubo.Id = ds.Tables(0).Rows(j).Item("ID")
                cubo.Den = listaCubos.CubosDen(1).Den
                cubo.Svc_Usu = ds.Tables(0).Rows(j).Item("SVC_USU")
                cubo.Svc_Url = ds.Tables(0).Rows(j).Item("SVC_URL")
                cubo.Svc_Pwd = ds.Tables(0).Rows(j).Item("SVC_PWD")
                cubo.Svc_Dom = ds.Tables(0).Rows(j).Item("SVC_DOM")
                cubo.Ssas_Server = ds.Tables(0).Rows(j).Item("SSAS_SERVER")
                cubo.Ssas_Usu = ds.Tables(0).Rows(j).Item("SSAS_USU")
                cubo.Ssas_Pwd = ds.Tables(0).Rows(j).Item("SSAS_PWD")
                cubo.Ssas_Dom = ds.Tables(0).Rows(j).Item("SSAS_DOM")
                cubo.Ssas_BD = ds.Tables(0).Rows(j).Item("SSAS_BD")
                cubo.Ssas_Cube = ds.Tables(0).Rows(j).Item("SSAS_CUBE")
                cubo.Activo = ds.Tables(0).Rows(j).Item("ACTIVO")
                cubo.Available_Measures = ds.Tables(0).Rows(j).Item("AVAILABLE_MEASURES")
                cubo.Default_Measures = ds.Tables(0).Rows(j).Item("DEFAULT_MEASURES")
                cubo.Extended_Dashboard = ds.Tables(0).Rows(j).Item("EXTENDED_DASHBOARD")
                cubo.Mon = ds.Tables(0).Rows(j).Item("MON")
                cubo.Cli = ds.Tables(0).Rows(j).Item("CLI")
            End If
        If (listaCubos.CubosDen.Count = 1) Or (Not Session("idCubo") Is Nothing) Then
                Dim ds As DataSet = cubos.Get_Cubo(Session("idCubo"))
			If(ds.Tables(0).Rows.Count > 0) Then
                Session.Remove("idCubo")
                Dim j As Integer = 0
                cubo.Id = ds.Tables(0).Rows(j).Item("ID")
                cubo.Svc_Usu = ds.Tables(0).Rows(j).Item("SVC_USU")
                cubo.Svc_Url = ds.Tables(0).Rows(j).Item("SVC_URL")
                cubo.Svc_Pwd = ds.Tables(0).Rows(j).Item("SVC_PWD")
                cubo.Svc_Dom = ds.Tables(0).Rows(j).Item("SVC_DOM")
                cubo.Ssas_Server = ds.Tables(0).Rows(j).Item("SSAS_SERVER")
                cubo.Ssas_Usu = ds.Tables(0).Rows(j).Item("SSAS_USU")
                cubo.Ssas_Pwd = ds.Tables(0).Rows(j).Item("SSAS_PWD")
                cubo.Ssas_Dom = ds.Tables(0).Rows(j).Item("SSAS_DOM")
                cubo.Ssas_BD = ds.Tables(0).Rows(j).Item("SSAS_BD")
                cubo.Ssas_Cube = ds.Tables(0).Rows(j).Item("SSAS_CUBE")
                cubo.Activo = ds.Tables(0).Rows(j).Item("ACTIVO")
                cubo.Available_Measures = ds.Tables(0).Rows(j).Item("AVAILABLE_MEASURES")
                cubo.Default_Measures = ds.Tables(0).Rows(j).Item("DEFAULT_MEASURES")
                cubo.Extended_Dashboard = ds.Tables(0).Rows(j).Item("EXTENDED_DASHBOARD")
                cubo.Mon = ds.Tables(0).Rows(j).Item("MON")
                cubo.Cli = ds.Tables(0).Rows(j).Item("CLI")
			End If
%>
   
   <!-- OBJETO SILVERLIGHT -->
    <div id="silverlightControlHost" style=" z-index:-1;">
         <!--<object data="data:application/x-silverlight-2," type="application/x-silverlight-2" width="100%" height="100%" >
		  <param name="source" value="ClientBin/Cube-FS-Ver01.xap"/>
		  <param name="onError" value="onSilverlightError" />
		  <param name="background" value="white" />
		  <param name="minRuntimeVersion" value="4.0.50826.0" />
		  <param name="autoUpgrade" value="true" />
		  
            <param name="initParams" value="CLI=FICOSA,VER=2.1,IDIOMA=SPA,USER=DPD,MON=USD,EXTENDED_DASHBOARD=0,DEFAULT_MEASURES=Importe adjudicación,AVAILABLE_MEASURES=Presupuesto abierto|Presupuesto consumido|Importe adjudicación|Ahorro adjudicación|Importe pedido|Importe recepción|Ahorro recepción,SS=mBoC4ExvGgzSFAHjXhFGpqU/qMxkamHI3o1S8pEwOaZ3vbBGyAHJnZN3jDjpALNnw0B4AHPctpykX5JUtqVRSuEfN+LQhpn0hN2DRTviWqkpUGJjscxt17KPvF+/j2qcidC3h0O3Q9WeN6nYXoTAkkV6uaVKk5TaEBkO3W6fUfrH+tNFrEFnHF2Euk2aR2bp2qr+IAO/iVKe/uxL8BIMk8qy+kWGF8cTJUo4v5fyMRw1PzmQAEP51U8v3nnMrQSM"/>

		  <a href="http://go.microsoft.com/fwlink/?LinkID=149156&v=4.0.50826.0" style="text-decoration:none">
 			  <img src="http://go.microsoft.com/fwlink/?LinkId=161376" alt="Get Microsoft Silverlight" style="border-style:none"/>
		  </a>
	    </object>-->

        <object id="SLOBJECT" data="data:application/x-silverlight-2," type="application/x-silverlight-2" width="100%" height="100%">
  		  <%If File.Exists(Server.MapPath(".") & "/ClientBin/" & cubo.Ssas_Dom & ".xap") Then%>
          <param name="source" value="ClientBin/<%=cubo.Ssas_Dom%>.xap"/>
          <%Else%> No existe el xap <%End If%>
		  <param name="onError" value="onSilverlightError" />
		  <param name="background" value="white" />
          <param name="windowless" value="true"/>
		  <param name="minRuntimeVersion" value="4.0.50826.0" />
		  <param name="autoUpgrade" value="true" />
          
          
 <%     
     Dim IDIOMA As String = FSNUser.Idioma
     Dim version As String = cubo.Ssas_BD.Substring(cubo.Ssas_BD.Length - 3)
     Dim cliente() As String = cubo.Ssas_Usu.Split("_")
     Dim CLI As String = cliente(1)
     Dim VER As String = version.Replace("_", ".")
                             
        'Parametrización de campos   
        'Dim cubo As Fullstep.FSNServer.Cubo '= listaCubos.Cubos.Item(1)

        Dim USER As String = FSNUser.Cod
        Dim SS As String
        Dim MON As String = cubo.Mon
    
        'Medidas disponibles
     Dim AVAILABLE_MEASURES As String = cubo.Available_Measures  '"Presupuesto abierto|Presupuesto consumido|Importe adjudicación|Ahorro adjudicación|Importe pedido|Importe recepción|Ahorro recepción"
    
        'Medidas por defecto
     Dim DEFAULT_MEASURES As String = cubo.Default_Measures  '"Importe adjudicación"
    
    
                             
        'En la versión 31900_8 sólo se utiliza 1 cubo por instalación.    
        'Obtener los datos, serializarlos y encriptarlos en la variable SS
    
        'SS = "SVC_URL|SVC_USU|SVC_PWD|SVC_DOM|SSAS_SERVER|SSAS_USU|SSAS_PWD|SSAS_DOM|SSAS_BD|SSAS_CUBE"
     SS = cubo.Svc_Url + "|" + cubo.Svc_Usu + "|" + Encrypter.EncriptarSQLtod("", cubo.Svc_Pwd, False) + "|" + cubo.Svc_Dom + "|" + cubo.Ssas_Server + "|" + cubo.Ssas_Usu + "|" + Encrypter.EncriptarSQLtod("", cubo.Ssas_Pwd, False) + "|" + cubo.Ssas_Dom + "|" + cubo.Ssas_BD + "|" + cubo.Ssas_Cube
    

        Dim aes As New FSNCrypt.FSNAES ' New Cube_FS_Ver01.Web.FSNAES
        SS = aes.EncryptStringToString(Date.Now.Date, USER, SS)
        Dim EXTENDED_DASHBOARD As Long
        If (cubo.Extended_Dashboard) Then
            EXTENDED_DASHBOARD = 1
        Else
            EXTENDED_DASHBOARD = 0
        End If
        Dim InitParams As String
        'Parámmetros de inicio del objeto Silverlight    
        InitParams = "<param name=""initParams"" value=""CLI=" & CLI & _
        ",VER=" & VER & _
        ",MON=" & MON & _
        ",IDIOMA=" & IDIOMA & _
        ",USER=" & USER & _
        ",EXTENDED_DASHBOARD=" & EXTENDED_DASHBOARD & _
        ",DEFAULT_MEASURES=" & DEFAULT_MEASURES & _
        ",AVAILABLE_MEASURES=" & AVAILABLE_MEASURES & _
        ",SS=" & SS & _
        """/>"
     Response.Write(InitParams)
     'FSNAceptar.Visible = False
     CuboRecuperado = False
     Session.Remove("DSCubos")
 
 End If
%>
    
     <div id="divPermisosCubos" runat="server" style="margin-bottom:20px; width:100%; float:left">       
    </div>	
     <form id="frmCubos" action="BI.aspx" method="post">
            <input id="sCubo"  type="hidden"/>
        </form>	
	</object>

	<script type="text/javascript">
        // Se ajusta el tamaño del objeto silverlight al alto de la pantalla respetando la cabecera y las pestañas
	    function adjustSL() 
        {
	        document.getElementById("SLOBJECT").height = document.body.clientHeight - document.getElementById("ctl00_ctl00_Cabecera").clientHeight - document.getElementById("ctl00_ctl00_Pestanias").clientHeight - 10;
	    }		
	</script>
<!-- Iframe necesario para la compatibilidad con exploradores -->
<iframe id="_sl_historyFrame" style="visibility:hidden;height:0px;width:0px;border:0px"></iframe>
</div>


<%

End If
    
%>


<form  name="FormCubo" id="FormCubo" method="post" action="BI.aspx">
<input name="cubo" id="cubo"  type="hidden" value="1" />

</form>




</asp:Content>
