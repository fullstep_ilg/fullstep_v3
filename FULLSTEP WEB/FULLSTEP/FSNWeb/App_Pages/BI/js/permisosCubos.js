﻿var idUsuario=[];
function Control_Panel_ParaUON(IdNuevaSeleccion, Master) {
    switch (IdNuevaSeleccion) {
        case 'divParaUON' + Master:
            EstablecerTextos('ParaUON');
            var to = rutaFS + 'BI/Cubos.aspx/UON_Permiso_Cubos';
            $.when($.ajax({
                type: 'POST',
                url: to,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                async: false
            })).done(function (msg) {
                var data = msg.d;
                $('#tvParaUONTree' + Master).tree({
                    data: data,
                    autoOpen: true,
                    selectable: false,
                    onOpenParentsDone: function () { setScrollSelected('tvParaUONTree', 250); }
                });
            });
    };

    $('#tvParaUONTree' + Master + ' .treeTipo3').live('click', function () {

        if ($('#divListaPara' + Master + ' #' + $(this).attr("id").replace('tvParaUONTree' + Master, 'itemParaUON')).length == 0) {
            $('#divListaPara' + Master).prepend('<div class="ItemPara" id="' + $(this).attr('id').replace('tvParaUONTree' + Master, 'itemParaUON') + '">' + $(this).text() + '</div>');
        }
        if (jQuery.inArray($(this).attr('id'), idUsuario) == -1) {
            idUsuario.push($(this).attr('id'));
        }
        var id = $(this).attr('id')
        //Control_Panel_Para('divParaUON', '');

    });

    $('#txtParaUON' + Master).live(($.browser.opera ? 'keypress' : 'keydown'), function (event) {
        
        var code;
        if (event.keyCode) code = event.keyCode;
        else if (event.which) code = event.which;

        switch (code) {
            case KEY.RETURN:
                var selectedItem = $('#tvParaUONTree' + Master + ' span.treeSeleccionable.selected');
                if (selectedItem.length != 0) {
                    if ($('#divListaPara' + Master + ' #' + $(selectedItem).attr("id").replace('tvParaUONTree' + Master, 'itemParaUON')).length == 0) {
                        $('#divListaPara' + Master).prepend('<div class="ItemPara" id="' + $(selectedItem).attr('id').replace('tvParaUONTree' + Master, 'itemParaUON') + '">' + $(selectedItem).text() + '</div>');
                    }
                }
                $('#txtParaUON' + Master).val('');
                $('#tvParaUONTree' + Master + ' span.treeSeleccionable.selected').removeClass('selected');
                event.preventDefault();
                (this).focus();
                return false;
                break;
            default:
                EncontrarItemTreeView('tvParaUONTree' + Master, 'txtParaUON' + Master, event);
                this.focus();
                break;
        }
    });

    $('[id^=divListaPara]').live('focusout', function () {
        $('.ItemParaSeleccionado').removeClass("ItemParaSeleccionado");
    });

    $('[id^=divListaPara]').live(($.browser.opera ? 'keypress' : 'keydown'), function (event) {
        var code;
        if (event.keyCode) code = event.keyCode;
        else if (event.which) code = event.which;

        if (code == KEY.DEL || code == KEY.BACKSPACE) {
            if ($('.ItemParaSeleccionado').length > 0) {
                $('.ItemParaSeleccionado').remove();
                idUsuario.pop($('.ItemParaSeleccionado').attr('id'));
            }
        }        
        return false;
    });
    $('.ItemNuevoMensajePara').live('click', function () {
        var Master = ($(this).attr('id').indexOf('Master') >= 0 ? 'Master' : '');
        var IdNuevaSeleccion = $(this).attr('id');
        Control_Panel_Para(IdNuevaSeleccion, Master);
    });

    $('.ItemPara').live('click', function () {
        $('.ItemParaSeleccionado').removeClass('ItemParaSeleccionado');
        $(this).addClass('ItemParaSeleccionado');
    });   

    $('[id^=divListaPara]').live('click', function () {       
        var Master = ($(this).attr('id').indexOf('Master') >= 0 ? 'Master' : '');
        $('#divCuboOpcionesPara' + Master).show();
        $('#divParaUON' + Master).addClass('ItemNuevoMensajeParaSeleccionado');
        $('#divParaUONPanel' + Master).show();
        Control_Panel_Para('divParaUON' + Master, Master);
    });
    $('[id^=divParaUONPanel]').live('click', function () {

    });
    $('[id^=divPermisos]').live('click', function () {
        $('#divCuboOpcionesPara' + Master).show();
        $('#divParaUONPanel' + Master).show();
    });
    $('[id^=divCabeceraPermiso]').live('click', function () {
        $('#divCuboOpcionesPara' + Master).show();
        $('#divParaUONPanel' + Master).show();
    });
    $('[id^=divMensajePermiso]').live('click', function () {
        $('#divCuboOpcionesPara' + Master).show();
        $('#divParaUONPanel' + Master).show();
    });
    $('[id^=divAgregarPermiso]').live('click', function () {
        $('#divCuboOpcionesPara' + Master).show();
        $('#divParaUONPanel' + Master).show();
    });
    $('[id^=divParaUONPanel]').live('click', function () {
        $('#divCuboOpcionesPara' + Master).show();
        $('#divParaUONPanel' + Master).show();
    });
    $('[id^=divContenedor]').live('click', function () {
        $('#divCuboOpcionesPara' + Master).show();
        $('#divParaUONPanel' + Master).show();
    });
    $('[id^=divFondo]').live('click', function () {
        $('#divCuboOpcionesPara' + Master).show();
        $('#divParaUONPanel' + Master).show();
    });
    $('[id^=Contenido]').live('click', function () {
        $('#divCuboOpcionesPara' + Master).show();
        $('#divParaUONPanel' + Master).show();
    });   
}

function Control_Panel_Para(IdNuevaSeleccion, Master) {
    $('#' + IdNuevaSeleccion).addClass('ItemNuevoMensajeParaSeleccionado');
    $('#' + IdNuevaSeleccion.replace('Master', '') + 'Panel' + Master).show();
}

function EstablecerTextos(tipo) {
    switch (tipo) {
        case 'ParaUON':
            $('[id^=lblParaUON]').text(TextosCubos[30]);
            break;
    }
}

