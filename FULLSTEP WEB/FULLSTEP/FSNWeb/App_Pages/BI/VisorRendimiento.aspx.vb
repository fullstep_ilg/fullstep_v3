﻿Imports System.Web.Script.Serialization
Imports Fullstep.FSNLibrary.TiposDeDatos

Public Class VisorRendimiento
    Inherits FSNPage

    'Carga el combo de los entornos
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Miramos si el usuario tiene permiso para acceder a este apartado si no puede acceder lo mandamos a la página de inicio
        Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
        Dim oUser As FSNServer.User = pag.Usuario
        If (Not oUser.AccesoFSAL) Or (Not oUser.ALVisorActividad) Then
            Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "Inicio.aspx")
        End If

        'Seleccionar el menu
        Dim mMaster = CType(MyBase.Master, FSNWeb.Menu)
        mMaster.Seleccionar("Informes", "Visor Rendimiento")

        'Ventana de Cargando
        Dim ModalProgressClientID As String = CType(Master.Master.FindControl("ModalProgress"), AjaxControlToolkit.ModalPopupExtender).ClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalClientID", "<script>var ModalProgress = '" & ModalProgressClientID & "' </script>")

        'Carga del combo de entornos
        Dim listaEntornos As Fullstep.FSNServer.Entornos = FSNServer.Get_Object(GetType(Fullstep.FSNServer.Entornos))
        listaEntornos.Load(FSNUser.IdiomaCod)
        If (listaEntornos.data.Tables(0).Rows.Count = 1) Then
            lblEntorno.Visible = True
            ddlEnviroment.Visible = False
            lblEntorno.Text = listaEntornos.data.Tables(0).Rows(0).Item("DEN")
            lblCodEntorno.Text = listaEntornos.data.Tables(0).Rows(0).Item("COD")
        Else
            lblEntorno.Visible = False
            ddlEnviroment.Visible = True
            ddlEnviroment.DataSource = listaEntornos.data
            ddlEnviroment.DataValueField = "COD"
            ddlEnviroment.DataTextField = "DEN"
            ddlEnviroment.DataBind()
        End If

        'Cargo la variable TextosVisorActividad para los textos
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorActividad
        Dim sVariableJavascriptTextosVisorActividad As String = ""
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosVisorActividad") Then
            sVariableJavascriptTextosVisorActividad = "var TextosVisorActividad = new Array();"
            For i As Integer = 0 To 69
                sVariableJavascriptTextosVisorActividad &= "TextosVisorActividad[" & i & "]='" & JSText(Textos(i)) & "';"
            Next
        End If
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosCubos", sVariableJavascriptTextosVisorActividad, True)

        'Carga del combo KPIs del Recent Activity
        Dim listaKPIs As Fullstep.FSNServer.AccesosRendimiento = FSNServer.Get_Object(GetType(Fullstep.FSNServer.AccesosRendimiento))
        ddlRecentActivity2.DataSource = listaKPIs.KPILoad(FSNUser.IdiomaCod)
        ddlRecentActivity2.DataValueField = "ID"
        ddlRecentActivity2.DataTextField = "DEN_KPI"
        ddlRecentActivity2.DataBind()
        ddlRecentActivity2.SelectedValue = 1

    End Sub

#Region "PageMethods"
    'Devuelve el primer entorno encontrado en la tabla FSAL_ENTORNOS
    <System.Web.Services.WebMethod(True), _
   System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function RecuperarEntorno()
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim Acceso As FSNServer.Accesos = FSNServer.Get_Object(GetType(Fullstep.FSNServer.Accesos))
        Dim entorno As String = Acceso.RecuperarEntorno()
        Return entorno
    End Function


    <System.Web.Services.WebMethod(True), _
      System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function IdiomaUsuario()
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Return FSNUser.Idioma
    End Function

    'Devuelve el formato de la fecha elegida
    <System.Web.Services.WebMethod(True), _
   System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function FormatoFecha()
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Return FSNUser.DateFormat.ShortDatePattern.ToString()
    End Function

    'Carga del combo de pymes para cuando seleccionen "pymes" en el combo de entornos
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function CargarPymes(entorno As String)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim listaPymes As Fullstep.FSNServer.Pymes = FSNServer.Get_Object(GetType(Fullstep.FSNServer.Pymes))
        listaPymes.Load(entorno, Usuario.Idioma)
        Return listaPymes.Pymes
    End Function

    'Carga los accesos dependiendo el periodo de tiempo 
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function CargarRadialGaugeMedia(fechaInicio As DateTime, fechaFin As DateTime, rango As String, entorno As String, pyme As String, timeoffset As Double)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim listaAccesos As FSNServer.AccesosRendimiento = FSNServer.Get_Object(GetType(Fullstep.FSNServer.AccesosRendimiento))
        If (rango = "HORA") Then
            fechaInicio = New DateTime(fechaInicio.Year, fechaInicio.Month, fechaInicio.Day, fechaInicio.Hour, fechaInicio.Minute, 0)
            fechaFin = New DateTime(fechaFin.Year, fechaFin.Month, fechaFin.Day, fechaFin.Hour, fechaFin.Minute, 0)
        Else
            fechaInicio = New DateTime(fechaInicio.Year, fechaInicio.Month, fechaInicio.Day, fechaInicio.Hour, fechaInicio.Minute, fechaInicio.Second)
            fechaFin = New DateTime(fechaFin.Year, fechaFin.Month, fechaFin.Day, fechaFin.Hour, fechaFin.Minute, fechaFin.Second)
        End If
        If (rango = "HORA" Or rango = "DIA") Then
            fechaInicio = DateAdd(DateInterval.Minute, timeoffset, fechaInicio)
            fechaFin = DateAdd(DateInterval.Minute, timeoffset, fechaFin)
        End If
        listaAccesos.LoadPorRangoRadialGauge(fechaInicio, fechaFin, entorno, pyme, rango)
        Return listaAccesos.AccesosRendimiento
    End Function

    'Carga los accesos dependiendo el periodo de tiempo 
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function CargarPerformanceEvolution(fechaInicio As DateTime, fechaFin As DateTime, rango As String, entorno As String, pyme As String, timeoffset As Double)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim listaAccesos As FSNServer.AccesosRendimiento = FSNServer.Get_Object(GetType(Fullstep.FSNServer.AccesosRendimiento))
        Dim dFechaInicioOrig As DateTime = fechaInicio
        If (rango = "HORA") Then
            fechaInicio = New DateTime(fechaInicio.Year, fechaInicio.Month, fechaInicio.Day, fechaInicio.Hour, fechaInicio.Minute, 0)
            fechaFin = New DateTime(fechaFin.Year, fechaFin.Month, fechaFin.Day, fechaFin.Hour, fechaFin.Minute, 0)
        Else
            fechaInicio = New DateTime(fechaInicio.Year, fechaInicio.Month, fechaInicio.Day, fechaInicio.Hour, fechaInicio.Minute, fechaInicio.Second)
            fechaFin = New DateTime(fechaFin.Year, fechaFin.Month, fechaFin.Day, fechaFin.Hour, fechaFin.Minute, fechaFin.Second)
        End If
        If (rango = "HORA" Or rango = "DIA") Then
            fechaInicio = DateAdd(DateInterval.Minute, timeoffset, fechaInicio)
            fechaFin = DateAdd(DateInterval.Minute, timeoffset, fechaFin)
        End If
        listaAccesos.LoadPorRango(fechaInicio, fechaFin, entorno, pyme, rango)
        If Not listaAccesos.AccesosRendimiento Is Nothing And (rango = "HORA" Or rango = "DIA") Then
            For Each item In listaAccesos.AccesosRendimiento
                item.fechaHoraKPI = DateAdd(DateInterval.Minute, -timeoffset, item.fechaHoraKPI).ToShortTimeString()
            Next
        End If
        If Not listaAccesos.AccesosRendimiento Is Nothing Then
            For i As Integer = 1 To listaAccesos.AccesosRendimiento.Count
                Dim dFechaHoraKPI As DateTime
                Select Case rango
                    Case "HORA", "DIA"
                        dFechaHoraKPI = New DateTime(dFechaInicioOrig.Year, dFechaInicioOrig.Month, dFechaInicioOrig.Day, CDate(listaAccesos.AccesosRendimiento(i).fechaHoraKPI).Hour, CDate(listaAccesos.AccesosRendimiento(i).fechaHoraKPI).Minute, 0)
                    Case "SEMANA", "MES"
                        dFechaHoraKPI = dFechaInicioOrig.AddDays((i - 1))
                    Case "ANYO"
                        dFechaHoraKPI = New DateTime(fechaInicio.Year, i, 1, 0, 0, 0)
                End Select
                If dFechaHoraKPI > Now() Then
                    listaAccesos.AccesosRendimiento(i).rendimientoKPI = -2
                End If
            Next i
        End If
        Return listaAccesos.AccesosRendimiento
    End Function

    'Carga los accesos dependiendo el periodo de tiempo y el indicador de rendimiento que haya sido seleccionado en el combo 
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function CargarRecentActivityKPI(fechaInicio As DateTime, fechaFin As DateTime, rango As String, entorno As String, pyme As String, kpi As Integer, timeoffset As Double)
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim listaAccesos As FSNServer.AccesosRendimiento = FSNServer.Get_Object(GetType(Fullstep.FSNServer.AccesosRendimiento))
        Dim dFechaInicioOrig As DateTime = fechaInicio
        If (rango = "HORA") Then
            fechaInicio = New DateTime(fechaInicio.Year, fechaInicio.Month, fechaInicio.Day, fechaInicio.Hour, fechaInicio.Minute, 0)
            fechaFin = New DateTime(fechaFin.Year, fechaFin.Month, fechaFin.Day, fechaFin.Hour, fechaFin.Minute, 0)
        Else
            fechaInicio = New DateTime(fechaInicio.Year, fechaInicio.Month, fechaInicio.Day, fechaInicio.Hour, fechaInicio.Minute, fechaInicio.Second)
            fechaFin = New DateTime(fechaFin.Year, fechaFin.Month, fechaFin.Day, fechaFin.Hour, fechaFin.Minute, fechaFin.Second)
        End If
        If (rango = "HORA" Or rango = "DIA") Then
            fechaInicio = DateAdd(DateInterval.Minute, timeoffset, fechaInicio)
            fechaFin = DateAdd(DateInterval.Minute, timeoffset, fechaFin)
        End If
        listaAccesos.LoadPorRangoKPI(fechaInicio, fechaFin, entorno, pyme, rango, kpi, FSNUser.IdiomaCod)
        If Not listaAccesos.AccesosRendimiento Is Nothing And (rango = "HORA" Or rango = "DIA") Then
            For Each item In listaAccesos.AccesosRendimiento
                item.fechaHoraKPI = DateAdd(DateInterval.Minute, -timeoffset, item.fechaHoraKPI).ToShortTimeString()
            Next
        End If
        If Not listaAccesos.AccesosRendimiento Is Nothing Then
            For i As Integer = 1 To listaAccesos.AccesosRendimiento.Count
                Dim dFechaHoraKPI As DateTime
                Select Case rango
                    Case "HORA", "DIA"
                        dFechaHoraKPI = New DateTime(dFechaInicioOrig.Year, dFechaInicioOrig.Month, dFechaInicioOrig.Day, CDate(listaAccesos.AccesosRendimiento(i).fechaHoraKPI).Hour, CDate(listaAccesos.AccesosRendimiento(i).fechaHoraKPI).Minute, 0)
                    Case "SEMANA", "MES"
                        dFechaHoraKPI = dFechaInicioOrig.AddDays((i - 1))
                    Case "ANYO"
                        dFechaHoraKPI = New DateTime(fechaInicio.Year, i, 1, 0, 0, 0)
                End Select
                If dFechaHoraKPI > Now() Then
                    listaAccesos.AccesosRendimiento(i).tiempoMaxKPI = -2
                    listaAccesos.AccesosRendimiento(i).tiempoMinKPI = -2
                    listaAccesos.AccesosRendimiento(i).tiempoAvgKPI = -2
                End If
            Next i
        End If
        Return listaAccesos.AccesosRendimiento
    End Function

    'Carga los accesos dependiendo el periodo de tiempo y el indicador de rendimiento que haya sido seleccionado en el combo 
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function CargarListaKPIsRango(fechaInicio As DateTime, fechaFin As DateTime, entorno As String, pyme As String, rango As String, timeoffset As Double)
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim listaAccesos As FSNServer.AccesosRendimiento = FSNServer.Get_Object(GetType(Fullstep.FSNServer.AccesosRendimiento))
        If (rango = "HORA") Then
            fechaInicio = New DateTime(fechaInicio.Year, fechaInicio.Month, fechaInicio.Day, fechaInicio.Hour, fechaInicio.Minute, 0)
            fechaFin = New DateTime(fechaFin.Year, fechaFin.Month, fechaFin.Day, fechaFin.Hour, fechaFin.Minute, 0)
        Else
            fechaInicio = New DateTime(fechaInicio.Year, fechaInicio.Month, fechaInicio.Day, fechaInicio.Hour, fechaInicio.Minute, fechaInicio.Second)
            fechaFin = New DateTime(fechaFin.Year, fechaFin.Month, fechaFin.Day, fechaFin.Hour, fechaFin.Minute, fechaFin.Second)
        End If
        If (rango = "HORA" Or rango = "DIA") Then
            fechaInicio = DateAdd(DateInterval.Minute, timeoffset, fechaInicio)
            fechaFin = DateAdd(DateInterval.Minute, timeoffset, fechaFin)
        End If
        listaAccesos.LoadListaKPIsPorRango(fechaInicio, fechaFin, entorno, pyme, rango, FSNUser.IdiomaCod)
        If Not listaAccesos.AccesosRendimiento Is Nothing And (rango = "HORA" Or rango = "DIA") Then
            For Each item In listaAccesos.AccesosRendimiento
                item.fechaHoraKPI = DateAdd(DateInterval.Minute, -timeoffset, item.fechaHoraKPI).ToShortTimeString()
            Next
        End If
        Return listaAccesos.AccesosRendimiento
    End Function

    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function CargarListaPorRangoKPI(fechaInicio As DateTime, fechaFin As DateTime, entorno As String, pyme As String, rango As String, kpi As Integer, timeoffset As Double)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim listaAccesos As FSNServer.AccesosRendimiento = FSNServer.Get_Object(GetType(Fullstep.FSNServer.AccesosRendimiento))
        If (rango = "HORA") Then
            fechaInicio = New DateTime(fechaInicio.Year, fechaInicio.Month, fechaInicio.Day, fechaInicio.Hour, fechaInicio.Minute, 0)
            fechaFin = New DateTime(fechaFin.Year, fechaFin.Month, fechaFin.Day, fechaFin.Hour, fechaFin.Minute, 0)
        Else
            fechaInicio = New DateTime(fechaInicio.Year, fechaInicio.Month, fechaInicio.Day, fechaInicio.Hour, fechaInicio.Minute, fechaInicio.Second)
            fechaFin = New DateTime(fechaFin.Year, fechaFin.Month, fechaFin.Day, fechaFin.Hour, fechaFin.Minute, fechaFin.Second)
        End If
        If (rango = "HORA" Or rango = "DIA") Then
            fechaInicio = DateAdd(DateInterval.Minute, timeoffset, fechaInicio)
            fechaFin = DateAdd(DateInterval.Minute, timeoffset, fechaFin)
        End If
        If (rango = "HORA" Or rango = "DIA") AndAlso listaAccesos.ExistenRegistrosKPI(fechaInicio, fechaFin, entorno, pyme, rango, kpi) Then
            listaAccesos.LoadAccesosPorRangoKPI(fechaInicio, fechaFin, entorno, pyme, rango, kpi)
        Else
            listaAccesos.LoadListaPorRangoKPI(fechaInicio, fechaFin, entorno, pyme, rango, kpi, FSNUser.IdiomaCod)
        End If
        If Not listaAccesos.AccesosRendimiento Is Nothing And (rango = "HORA" Or rango = "DIA") Then
            For Each item In listaAccesos.AccesosRendimiento
                item.fechaHoraKPI = DateAdd(DateInterval.Minute, -timeoffset, item.fechaHoraKPI).ToShortTimeString()
            Next
        End If
        If Not listaAccesos.AccesosRendimiento Is Nothing Then
            Dim i As Integer = 1
            For Each item In listaAccesos.AccesosRendimiento
                item.fechaHoraKPI = Format(i, "000") & "|" & item.fechaHoraKPI
                i += 1
            Next
        End If
        Return listaAccesos.AccesosRendimiento
    End Function

#End Region

End Class