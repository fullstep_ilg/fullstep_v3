﻿Imports Fullstep.FSNLibrary.TiposDeDatos
Imports Fullstep.FSNServer
Imports Infragistics.Web.UI.GridControls
Imports System.Web.Script.Serialization
Imports System.Net

Public Class QlikApps
    Inherits FSNPage

    Private _dsQlikApps As DataSet
    Private _bOrdenarColumnaUngroup As Boolean = False
    Private _sNombreColumnaUngroup As String = ""
    Private _sOrdenColumnaUngroup As String = ""

    Friend mDBServer As Root
    Friend Function DBServer() As Root
        If mDBServer Is Nothing Then
            mDBServer = New Root
        End If
        Return mDBServer
    End Function

    ''' <summary>
    ''' Devuelve las QlikApps de la tabla QLIK_APPS
    ''' </summary>
    ''' <returns>Dataset con las QlikApps</returns>
    ''' <remarks>Llamada desde: CargarQlikApps, Tiempo maximo: 0,2</remarks>
    Protected ReadOnly Property QlikApps() As DataSet

        Get

            Dim listaQlikApps As Fullstep.FSNServer.FSNQlikApps = FSNServer.Get_Object(GetType(Fullstep.FSNServer.FSNQlikApps))
            listaQlikApps.ObtenerFSNQlikApps(False)
            _dsQlikApps = listaQlikApps.Data
            Dim PKCol(0) As DataColumn
            PKCol(0) = _dsQlikApps.Tables(0).Columns("ID")
            _dsQlikApps.Tables(0).PrimaryKey = PKCol
            _dsQlikApps.Tables(0).TableName = "QLIK_APPS"

            Return _dsQlikApps

        End Get

    End Property

#Region "Inicio"

    ''' <summary>
    ''' Al iniciar la pagina establece EnableScriptGlobalization
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub QlikApps_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ScriptMgr.EnableScriptGlobalization = True
    End Sub

    ''' <summary>
    ''' Cargar la pagina
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0,2</remarks>
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Miramos si el usuario tiene permiso para acceder a este apartado si no puede acceder lo mandamos a la página de inicio
        Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
        Dim oUser As FSNServer.User = pag.Usuario
        If (Not oUser.AccesoBI) Or (Not oUser.BIConfigurarQlikApps) Then
            Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "Inicio.aspx")
        End If

        Seccion = "QlikApps"

        'Seleccionar el menu
        Dim mMaster = CType(MyBase.Master, FSNWeb.Menu)
        mMaster.Seleccionar("Informes", "QlikApps")

        Dim mMasterTotal As FSNWeb.Cabecera = CType(mMaster.Master, FSNWeb.Cabecera)
        Dim scriptReference As ScriptReference
        scriptReference = New ScriptReference
        scriptReference.Path = ConfigurationManager.AppSettings("ruta") & "js/jsUpdateProgress.js"
        CType(mMasterTotal.FindControl("ScriptManager1"), ScriptManager).Scripts.Add(scriptReference)

        'Ventana de Cargando
        Dim ModalProgressClientID As String = CType(Master.Master.FindControl("ModalProgress"), AjaxControlToolkit.ModalPopupExtender).ClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalClientID", "<script>var ModalProgress = '" & ModalProgressClientID & "' </script>")

        'Cargo los diferentes textos
        CargarIdiomas()

        If Not IsPostBack Then

            FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/Qlik.png"

            CType(whgQlikApps.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgNuevaQlikApp"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/nuevo.png"
            CType(whgQlikApps.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgConfigurarQlikApp"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/comparar.png"
            CType(whgQlikApps.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgEliminarQlikApp"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/Eliminar.png"
            CType(whgQlikApps.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgAccesoAQlik"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/GruposCN.png"

            Session("PageNumberQlikApps") = "0"

        Else

            Select Case Request("__EVENTTARGET")
                Case btnPager.ClientID
                    Session("PageNumberQlikApps") = CType(Request("__EVENTARGUMENT"), String)
            End Select

            'If Request("__EVENTTARGET") = btnUpdateGrid.ClientID Then
            '    CargarQlikApps()
            'End If
            CargarQlikApps()

        End If

    End Sub

    ''' <summary>
    ''' Carga los textos con el idioma de la pagina
    ''' </summary>
    Private Sub CargarIdiomas()

        '' Modulo de idiomas de Qlik Apps
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.QlikApps

        FSNPageHeader.TituloCabecera = Textos(0) 'Qlik Apps

        whgQlikApps.GroupingSettings.EmptyGroupAreaText = Textos(1) 'Desplace una columna aquí para agrupar por ese concepto

        CType(whgQlikApps.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lnkNuevaQlikApp"), Label).Text = Textos(2) 'Nueva App
        CType(whgQlikApps.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lnkConfigurarQlikApp"), Label).Text = Textos(3) 'Configurar App
        CType(whgQlikApps.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lnkEliminarQlikApp"), Label).Text = Textos(4) 'Eliminar App
        CType(whgQlikApps.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblAccesoAQlik"), Label).Text = Textos(5) 'Acceso a Qlik

        CType(whgQlikApps.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPage"), Label).Text = Textos(6) 'Pagina
        CType(whgQlikApps.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblOF"), Label).Text = Textos(7) 'de

        'Cargar la variable TextosQlikApps para los textos en JS
        Dim sVariableJavascriptTextosQlikApps As String = ""

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosQlikApps") Then
            sVariableJavascriptTextosQlikApps = "var TextosQlikApps = new Array();"
            For i As Integer = 0 To 51 '' TODO: Cambiar el inicio y el fin del for en funcion del numero de textos
                sVariableJavascriptTextosQlikApps &= "TextosQlikApps[" & i & "]='" & JSText(Textos(i)) & "';"
            Next
        End If

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosQlikApps", sVariableJavascriptTextosQlikApps, True)

    End Sub

    ''' <summary>
    ''' Antes de dibujar la pagina carga las QlikApps de BBDD
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub QlikApps_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            CargarQlikApps()
        End If
    End Sub

#End Region

#Region "Carga de datos"

    ''' <summary>
    ''' Carga el grid con las apps
    ''' </summary>
    Private Sub CargarQlikApps()
        With whgQlikApps
            .Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize")
            .GridView.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize")
            .Rows.Clear()
            .DataSource = QlikApps
            .GridView.DataSource = .DataSource
            CreacionEdicionColumnasGrid()
            .DataMember = "QLIK_APPS"
            .DataKeyFields = "ID"
            .DataBind()
        End With
        updGridQlikApps.Update()
    End Sub

    ''' <summary>
    ''' Crea las columnas del grid
    ''' </summary>
    Private Sub CreacionEdicionColumnasGrid()

        ObtenerColumnasGrid(whgQlikApps.UniqueID)
        CrearColumnasGrid(whgQlikApps.UniqueID)

    End Sub

    ''' <summary>
    ''' Inicializa las columnas del grid con los campos generales
    ''' </summary>
    Private Sub ObtenerColumnasGrid(ByVal gridId As String)
        Dim grid As WebHierarchicalDataGrid = CType(Me.FindControl(gridId), WebHierarchicalDataGrid)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.QlikApps

        'NOMBRE DE LAS COLUMNAS DE LA GRID!!!
        Dim camposGrid As New List(Of Infragistics.Web.UI.GridControls.BoundDataField)
        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
        Dim campoCheckGrid As Infragistics.Web.UI.GridControls.BoundCheckBoxField

        campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
        With campoGrid
            .Key = "ID"
            .DataFieldName = "ID"
            .Header.CssClass = "headerNoWrap"
            .Hidden = True
            .CssClass = "itemSeleccionable SinSalto"
        End With
        camposGrid.Add(campoGrid)

        campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
        With campoGrid
            .Key = "NOMBRE"
            .DataFieldName = "NOMBRE"
            .Header.CssClass = "headerNoWrap"
            .Header.Text = Textos(8)
            .Header.Tooltip = Textos(8)
            .Hidden = False
            .CssClass = "itemSeleccionable SinSalto"
        End With
        camposGrid.Add(campoGrid)

        campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
        With campoGrid
            .Key = "URL"
            .DataFieldName = "URL"
            .Header.CssClass = "headerNoWrap"
            .Header.Text = Textos(9)
            .Header.Tooltip = Textos(9)
            .Hidden = False
            .CssClass = "itemSeleccionable SinSalto"
        End With
        camposGrid.Add(campoGrid)

        campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
        With campoGrid
            .Key = "ID_APP"
            .DataFieldName = "ID_APP"
            .Header.CssClass = "headerNoWrap"
            .Hidden = True
            .CssClass = "itemSeleccionable SinSalto"
        End With
        camposGrid.Add(campoGrid)

        campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
        With campoGrid
            .Key = "APP"
            .DataFieldName = "APP"
            .Header.CssClass = "headerNoWrap"
            .Header.Text = Textos(10)
            .Header.Tooltip = Textos(10)
            .Hidden = False
            .CssClass = "itemSeleccionable SinSalto"
        End With
        camposGrid.Add(campoGrid)

        campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
        With campoGrid
            .Key = "ID_SHEET"
            .DataFieldName = "ID_SHEET"
            .Header.CssClass = "headerNoWrap"
            .Hidden = True
            .CssClass = "itemSeleccionable SinSalto"
        End With
        camposGrid.Add(campoGrid)

        campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
        With campoGrid
            .Key = "SHEET"
            .DataFieldName = "SHEET"
            .Header.CssClass = "headerNoWrap"
            .Header.Text = Textos(11)
            .Header.Tooltip = Textos(11)
            .Hidden = False
            .CssClass = "itemSeleccionable SinSalto"
        End With
        camposGrid.Add(campoGrid)

        campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
        With campoGrid
            .Key = "REST_API_PORT"
            .DataFieldName = "REST_API_PORT"
            .Header.CssClass = "headerNoWrap"
            .Header.Text = Textos(12)
            .Header.Tooltip = Textos(12)
            .Hidden = False
            .CssClass = "itemSeleccionable SinSalto"
        End With
        camposGrid.Add(campoGrid)

        campoCheckGrid = New Infragistics.Web.UI.GridControls.BoundCheckBoxField(True)
        With campoCheckGrid
            .Key = "ACTIVA"
            .DataFieldName = "ACTIVA"
            .Header.CssClass = "headerNoWrap"
            .Header.Text = Textos(15)
            .Header.Tooltip = Textos(15)
            .Hidden = False
            .CssClass = "itemSeleccionable SinSalto"
        End With
        camposGrid.Add(campoCheckGrid)

        Session("arrDatosGeneralesQlikApps") = camposGrid

    End Sub

    ''' <summary>
    ''' Crea las columnas del grid con los campos devueltos de las QlikApps
    ''' </summary>
    Private Sub CrearColumnasGrid(ByVal gridId As String)
        Dim grid As WebHierarchicalDataGrid = CType(Me.FindControl(gridId), WebHierarchicalDataGrid)
        Dim campoGridBoundTemplate As Infragistics.Web.UI.GridControls.BoundDataField
        campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
        Dim campoGridCheckBoundTemplate As Infragistics.Web.UI.GridControls.BoundCheckBoxField
        campoGridCheckBoundTemplate = New Infragistics.Web.UI.GridControls.BoundCheckBoxField

        ModuloIdioma = ModulosIdiomas.QlikApps

        For Each field As Infragistics.Web.UI.GridControls.BoundDataField In CType(Session("arrDatosGeneralesQlikApps"), List(Of Infragistics.Web.UI.GridControls.BoundDataField))
            Select Case field.Key
                Case "ID"
                    campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
                    With campoGridBoundTemplate
                        .Key = "ID"
                        .DataFieldName = "ID"
                        .Header.Text = "Id de la Qlik App"
                        .Hidden = True
                        .Width = Unit.Percentage(0)
                    End With
                    If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.Columns.Add(campoGridBoundTemplate)
                    Else
                        grid.Columns(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                    If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.GridView.Columns.Add(campoGridBoundTemplate)
                    Else
                        grid.GridView.Columns.Item(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                Case "NOMBRE"
                    campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
                    With campoGridBoundTemplate
                        .Key = "NOMBRE"
                        .DataFieldName = "NOMBRE"
                        .Header.Text = Textos(8)
                        .Header.Tooltip = Textos(8)
                        .Hidden = False
                        .Width = Unit.Percentage(22)
                    End With
                    If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.Columns.Add(campoGridBoundTemplate)
                    Else
                        grid.Columns(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                    If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.GridView.Columns.Add(campoGridBoundTemplate)
                    Else
                        grid.GridView.Columns.Item(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                Case "URL"
                    campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
                    With campoGridBoundTemplate
                        .Key = "URL"
                        .DataFieldName = "URL"
                        .Header.Text = Textos(9)
                        .Header.Tooltip = Textos(9)
                        .Hidden = False
                        .Width = Unit.Percentage(25)
                    End With
                    If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.Columns.Add(campoGridBoundTemplate)
                    Else
                        grid.Columns(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                    If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.GridView.Columns.Add(campoGridBoundTemplate)
                    Else
                        grid.GridView.Columns.Item(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                Case "ID_APP"
                    campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
                    With campoGridBoundTemplate
                        .Key = "ID_APP"
                        .DataFieldName = "ID_APP"
                        .Header.Text = "Id de la aplicacion Qlik App"
                        .Hidden = True
                        .Width = Unit.Percentage(0)
                    End With
                    If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.Columns.Add(campoGridBoundTemplate)
                    Else
                        grid.Columns(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                    If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.GridView.Columns.Add(campoGridBoundTemplate)
                    Else
                        grid.GridView.Columns.Item(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                Case "APP"
                    campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
                    With campoGridBoundTemplate
                        .Key = "APP"
                        .DataFieldName = "APP"
                        .Header.Text = Textos(10)
                        .Header.Tooltip = Textos(10)
                        .Hidden = False
                        .Width = Unit.Percentage(20)
                    End With
                    If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.Columns.Add(campoGridBoundTemplate)
                    Else
                        grid.Columns(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                    If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.GridView.Columns.Add(campoGridBoundTemplate)
                    Else
                        grid.GridView.Columns.Item(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                Case "ID_SHEET"
                    campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
                    With campoGridBoundTemplate
                        .Key = "ID_SHEET"
                        .DataFieldName = "ID_SHEET"
                        .Header.Text = "Id de la hoja de aplicacion Qlik App"
                        .Hidden = True
                        .Width = Unit.Percentage(0)
                    End With
                    If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.Columns.Add(campoGridBoundTemplate)
                    Else
                        grid.Columns(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                    If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.GridView.Columns.Add(campoGridBoundTemplate)
                    Else
                        grid.GridView.Columns.Item(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                Case "SHEET"
                    campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
                    With campoGridBoundTemplate
                        .Key = "SHEET"
                        .DataFieldName = "SHEET"
                        .Header.Text = Textos(11)
                        .Header.Tooltip = Textos(11)
                        .Hidden = False
                        .Width = Unit.Percentage(15)
                    End With
                    If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.Columns.Add(campoGridBoundTemplate)
                    Else
                        grid.Columns(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                    If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.GridView.Columns.Add(campoGridBoundTemplate)
                    Else
                        grid.GridView.Columns.Item(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                Case "REST_API_PORT"
                    campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
                    With campoGridBoundTemplate
                        .Key = "REST_API_PORT"
                        .DataFieldName = "REST_API_PORT"
                        .Header.Text = Textos(12)
                        .Header.Tooltip = Textos(12)
                        .Hidden = False
                        .Width = Unit.Percentage(10)
                    End With
                    If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.Columns.Add(campoGridBoundTemplate)
                    Else
                        grid.Columns(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                    If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.GridView.Columns.Add(campoGridBoundTemplate)
                    Else
                        grid.GridView.Columns.Item(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                Case "ACTIVA"
                    campoGridCheckBoundTemplate = New Infragistics.Web.UI.GridControls.BoundCheckBoxField
                    With campoGridCheckBoundTemplate
                        .Key = "ACTIVA"
                        .DataFieldName = "ACTIVA"
                        .Header.Text = Textos(15)
                        .Header.Tooltip = Textos(15)
                        .Hidden = False
                        .Width = Unit.Percentage(8)
                    End With
                    campoGridCheckBoundTemplate.CheckBox.CheckedImageUrl = ConfigurationManager.AppSettings("ruta") & "images\baja.gif"
                    campoGridCheckBoundTemplate.CssClass = "itemCenterAligment"
                    If grid.Columns(campoGridCheckBoundTemplate.Key) Is Nothing Then
                        grid.Columns.Add(campoGridCheckBoundTemplate)
                    Else
                        grid.Columns(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
                    If grid.GridView.Columns.Item(campoGridCheckBoundTemplate.Key) Is Nothing Then
                        grid.GridView.Columns.Add(campoGridCheckBoundTemplate)
                    Else
                        grid.GridView.Columns.Item(campoGridBoundTemplate.Key).Hidden = field.Hidden
                    End If
            End Select
        Next

    End Sub

#End Region

#Region "Eventos Grid"

    ''' <summary>
    ''' Tras traer datos (Bound) actualiza el Paginador
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub whgQlikApps_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles whgQlikApps.DataBound
        Dim _pageNumber = IIf(IsNumeric(Session("PageNumberQlikApps")), CInt(Session("PageNumberQlikApps")), 0)
        Dim _pageCount As Integer
        Dim _rows As Integer
        Dim dt As DataTable = CType(whgQlikApps.DataSource, DataSet).Tables(0)

        ''Calculamos el numero de filas que se muestran
        If CType(hFiltered.Value, Boolean) Then
            Dim saColumnas As String() = hFilteredColumns.Value.Split("#")
            Dim saCondiciones As String() = hFilteredCondition.Value.Split("#")
            Dim saValores As String() = hFilteredValue.Value.Split("#")
            Dim iColumnasFiltradas As Integer = Math.Min(Math.Min(saColumnas.Length, saCondiciones.Length), saValores.Length)
            Dim sFiltroCompleto As String = "1=1"

            For i As Integer = 0 To (iColumnasFiltradas - 1)
                Select Case saColumnas(i)
                    Case "REST_API_PORT" ''Columnas de tipo numerico
                        Select Case saCondiciones(i)
                            Case "1" ''Equals
                                sFiltroCompleto &= " AND " & saColumnas(i) & "=" & saValores(i)
                            Case "2" ''Does not equal
                                sFiltroCompleto &= " AND " & saColumnas(i) & "<>" & saValores(i)
                            Case "3" ''Greater than
                                sFiltroCompleto &= " AND " & saColumnas(i) & ">" & saValores(i)
                            Case "4" ''Greater than or Equal to
                                sFiltroCompleto &= " AND " & saColumnas(i) & ">=" & saValores(i)
                            Case "5" ''Less than
                                sFiltroCompleto &= " AND " & saColumnas(i) & "<" & saValores(i)
                            Case "6" ''Less than or Equal to
                                sFiltroCompleto &= " AND " & saColumnas(i) & "<=" & saValores(i)
                            Case "7" ''Is null
                                sFiltroCompleto &= " AND " & saColumnas(i) & " IS NULL"
                            Case "8" ''Is not null
                                sFiltroCompleto &= " AND " & saColumnas(i) & " IS NOT NULL"
                        End Select
                    Case "ACTIVA" ''Columnas de tipo CheckBox
                        Select Case saCondiciones(i)
                            Case "1" ''Checked
                                sFiltroCompleto &= " AND " & saColumnas(i) & "=TRUE"
                            Case "2" ''Unchecked
                                sFiltroCompleto &= " AND " & saColumnas(i) & "=FALSE"
                            Case "3" ''Partial
                                sFiltroCompleto &= " AND " & saColumnas(i) & " IS NULL"
                            Case "4" ''Is not null
                                sFiltroCompleto &= " AND " & saColumnas(i) & " IS NOT NULL"
                        End Select
                    Case Else ''Columnas de tipo String
                        Select Case saCondiciones(i)
                            Case "1" ''Equals
                                sFiltroCompleto &= " AND " & saColumnas(i) & "='" & saValores(i) & "'"
                            Case "2" ''Does not equal
                                sFiltroCompleto &= " AND " & saColumnas(i) & "<>'" & saValores(i) & "'"
                            Case "3" ''Begins with
                                sFiltroCompleto &= " AND " & saColumnas(i) & " LIKE '" & saValores(i) & "%'"
                            Case "4" ''Ends with
                                sFiltroCompleto &= " AND " & saColumnas(i) & " LIKE '%" & saValores(i) & "'"
                            Case "5" ''Contains
                                sFiltroCompleto &= " AND " & saColumnas(i) & " LIKE '%" & saValores(i) & "%'"
                            Case "6" ''Does not contain
                                sFiltroCompleto &= " AND " & saColumnas(i) & " NOT LIKE '%" & saValores(i) & "%'"
                            Case "7" ''Is null
                                sFiltroCompleto &= " AND " & saColumnas(i) & " IS NULL"
                            Case "8" ''Is not null
                                sFiltroCompleto &= " AND " & saColumnas(i) & " IS NOT NULL"
                        End Select
                End Select
            Next i
            _rows = dt.Select(sFiltroCompleto).Length
        Else
            _rows = dt.Rows.Count
        End If

        ''Calculamos en numero de paginas que se muestran
        _pageCount = _rows \ System.Configuration.ConfigurationManager.AppSettings("PageSize")
        _pageCount = _pageCount + (IIf((_rows Mod System.Configuration.ConfigurationManager.AppSettings("PageSize") = 0) AndAlso (_rows > 0), 0, 1))

        If (_pageNumber + 1) > _pageCount Then
            Session("PageNumberQlikApps") = "0"
            _pageNumber = 0
        End If

        ''Gestion del paginador
        Paginador(_pageCount, (_pageNumber + 1))

        ''Indicarmos la pagina actual al Grid
        whgQlikApps.GridView.Behaviors.Paging.PageIndex = _pageNumber

        updGridQlikApps.Update()
    End Sub

    ''' <summary>
    ''' Tras ordenar actualiza el grid
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub whgQlikApps_ColumnSorted(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.SortingEventArgs) Handles whgQlikApps.ColumnSorted
        Dim _pageNumber = IIf(IsNumeric(Session("PageNumberQlikApps")), CInt(Session("PageNumberQlikApps")), 0)

        ''Indicarmos la pagina actual al Grid
        whgQlikApps.GridView.Behaviors.Paging.PageIndex = _pageNumber

        updGridQlikApps.Update()
    End Sub

    ''' <summary>
    ''' Tras filtrar actualiza el grid
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub whgQlikApps_DataFiltered(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.FilteredEventArgs) Handles whgQlikApps.DataFiltered
        Dim _pageNumber = IIf(IsNumeric(Session("PageNumberQlikApps")), CInt(Session("PageNumberQlikApps")), 0)

        ''Indicarmos la pagina actual al Grid
        whgQlikApps.GridView.Behaviors.Paging.PageIndex = _pageNumber

        updGridQlikApps.Update()
    End Sub

    ''' <summary>
    ''' Tras agrupar actualiza el grid
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub whgQlikApps_GroupedColumnsChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.GroupedColumnsChangedEventArgs) Handles whgQlikApps.GroupedColumnsChanged
        Dim _pageNumber = IIf(IsNumeric(Session("PageNumberQlikApps")), CInt(Session("PageNumberQlikApps")), 0)

        ''Indicarmos la pagina actual al Grid
        whgQlikApps.GridView.Behaviors.Paging.PageIndex = _pageNumber

        ''En caso de que hayamos sacado una columna del grupo y no exista ordenación de columna fuera del grupo,
        ''o existe ordenación previa en las columnas justo por ella misma,
        ''llevamos la ordenación que tiene en el grupo al nivel de columnas.
        If (_bOrdenarColumnaUngroup) AndAlso (_sNombreColumnaUngroup <> "") AndAlso (_sOrdenColumnaUngroup <> "") Then
            whgQlikApps.GridView.Behaviors.Sorting.SortedColumns.Clear()
            whgQlikApps.GridView.Behaviors.Sorting.SortedColumns.Add(_sNombreColumnaUngroup, IIf(_sOrdenColumnaUngroup = "DESC", Infragistics.Web.UI.SortDirection.Descending, Infragistics.Web.UI.SortDirection.Ascending))
        End If

        updGridQlikApps.Update()
    End Sub

    Private Sub whgQlikApps_GroupedColumnsChanging(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.GroupedColumnsChangingEventArgs) Handles whgQlikApps.GroupedColumnsChanging

        If e.Action = GroupByChangeAction.Group Then
            ''En caso de que metamos una columna al grupo y esta tenga ya ordenacion a nivle de columnas,
            ''llevamos la ordenación que tiene al grupo.
            If (hOrderedColumn.Value.Contains(e.EffectedColumns(0).ColumnKey)) AndAlso (Split(hOrderedColumn.Value, " ").Length = 2) Then
                e.EffectedColumns(0).SortDirection = IIf(Split(hOrderedColumn.Value, " ")(1) = "DESC", GroupingSortDirection.Descending, GroupingSortDirection.Ascending)
            End If
        ElseIf e.Action = GroupByChangeAction.Ungroup Then
            ''En caso de que hayamos sacado una columna del grupo y no exista ordenación de columna fuera del grupo,
            ''o existe ordenación previa en las columnas justo por ella misma,
            ''llevamos la ordenación que tiene en el grupo al nivel de columnas.
            If ((hOrderedColumn.Value = "") Or (hOrderedColumn.Value.Contains(e.EffectedColumns(0).ColumnKey)) AndAlso Split(hOrderedColumn.Value, " ").Length = 2) Then
                _bOrdenarColumnaUngroup = True
                _sNombreColumnaUngroup = e.EffectedColumns(0).ColumnKey
                _sOrdenColumnaUngroup = IIf(e.EffectedColumns(0).SortDirection = GroupingSortDirection.Descending, "DESC", "ASC")
            End If
        End If

    End Sub

#End Region

#Region "Metodos Grid"

    ''' <summary>
    ''' Gestiona el Paginador
    ''' </summary>
    ''' <param name="_pageCount">numero de paginas</param>
    ''' <param name="_pageNumber">pagina actual del Grid</param>     
    ''' <remarks>Llamada desde: Evento DataBound del Grid, Tiempo maximo: 0</remarks>
    Private Sub Paginador(ByVal _pageCount As Integer, ByVal _pageNumber As Integer)
        Dim pagerList As DropDownList = DirectCast(whgQlikApps.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("PagerPageList"), DropDownList)
        pagerList.Items.Clear()
        If _pageCount = 0 Then pagerList.Items.Add("")
        For i As Integer = 1 To _pageCount
            pagerList.Items.Add(i.ToString())
        Next
        CType(whgQlikApps.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblCount"), Label).Text = _pageCount
        If _pageCount > 0 Then pagerList.SelectedIndex = _pageNumber - 1
        Dim Desactivado As Boolean = (_pageNumber = 1)
        With CType(whgQlikApps.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        With CType(whgQlikApps.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        Desactivado = (_pageCount = 1 OrElse _pageNumber = _pageCount)
        With CType(whgQlikApps.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        With CType(whgQlikApps.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
    End Sub

#End Region

#Region "Page Methods"

    ''' <summary>
    ''' Comprueba la URL del servidor de Qlik Sense
    ''' </summary>
    ''' <param name="sUrl">Url del servidor Qlik Sense</param>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function ComprobarServidorQlikCorrecto(ByVal sUrl As String) As String
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oQlikSenseServer As Fullstep.FSNServer.QlikSenseServer = FSNServer.Get_Object(GetType(Fullstep.FSNServer.QlikSenseServer))

        Try
            oQlikSenseServer.UrlServidorQlik = sUrl

            If oQlikSenseServer.ExisteRepositorioQlik() Then
                Return oQlikSenseServer.RestAPIPort.ToString()
            Else
                Return "error"
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    ''' <summary>
    ''' Obtiene las aplicaciones del repositorio de Qlik Sense
    ''' </summary>
    ''' <param name="sUrl">Url del servidor Qlik Sense</param>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function ObtenerAplicacionesQlik(ByVal sUrl As String)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oQlikSenseServer As Fullstep.FSNServer.QlikSenseServer = FSNServer.Get_Object(GetType(Fullstep.FSNServer.QlikSenseServer))
        Dim oFSNQlikConfig As Fullstep.FSNServer.FSNQlikConfig = FSNServer.Get_Object(GetType(Fullstep.FSNServer.FSNQlikConfig))
        Dim oQlikApps As New List(Of QlikSenseServer.QlikApp)

        Try
            oQlikSenseServer.UrlServidorQlik = sUrl

            If oQlikSenseServer.ExisteRepositorioQlik() Then
                Dim sCodigoCliente As String = oFSNQlikConfig.ObtenerCodigoCliente()
                ''Si el codigo de cliente es "FSN" no filtramos por Stream y concatenamos el nombre del Stream al de la aplicacion
                ''El codigo de cliente "FSN" pertenece a Fullstep en Corporate
                If (sCodigoCliente <> "") And (sCodigoCliente = "FSN") Then
                    oQlikApps = oQlikSenseServer.ObtenerAplicacionesQlik()
                    For Each oQlikApp As QlikSenseServer.QlikApp In oQlikApps
                        oQlikApp.name = oQlikApp.stream.name & "\" & oQlikApp.name
                    Next
                Else
                    oQlikApps = oQlikSenseServer.ObtenerAplicacionesStreamQlik(sCodigoCliente)
                End If
                oQlikApps.Sort(Function(x, y) x.name.CompareTo(y.name))
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return oQlikApps
    End Function

    ''' <summary>
    ''' Obtiene las hojas del repositorio de Qlik Sense
    ''' </summary>
    ''' <param name="sUrl">Url del servidor Qlik Sense</param>
    ''' <param name="sIdApp">Id de la App de Qlik Sense</param>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function ObtenerHojasQlik(ByVal sUrl As String, ByVal sIdApp As String)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oQlikSenseServer As Fullstep.FSNServer.QlikSenseServer = FSNServer.Get_Object(GetType(Fullstep.FSNServer.QlikSenseServer))
        Dim oQlikAppSheets As New List(Of QlikSenseServer.QlikAppObject)

        Try
            oQlikSenseServer.UrlServidorQlik = sUrl

            If oQlikSenseServer.ExisteRepositorioQlik() Then
                oQlikAppSheets = oQlikSenseServer.ObtenerObjetosQlik(sIdApp, "sheet")
                oQlikAppSheets.Sort(Function(x, y) x.name.CompareTo(y.name))
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return oQlikAppSheets
    End Function

    ''' <summary>
    ''' Inserta o modifica una Qlik App en/de la tabla QLIK_APPS
    ''' </summary>
    ''' <param name="lId">Identificador de la QlikApp en la tabla QLIK_APPS</param>
    ''' <param name="sNombre">Nombre de la QlikApp en la tabla QLIK_APPS</param>
    ''' <param name="bActiva">Esta o no activa la QlikApp en la plataforma</param>
    ''' <param name="sUrl">Url del servidor Qlik Sense</param>
    ''' <param name="lPuertoRestAPI">Puerto de la API de autenticacion Qlik Sense</param>
    ''' <param name="sIdApp">Id de la aplicacion de Qlik Sense</param>
    ''' <param name="sApp">Nombre de la aplicacion Qlik Sense</param>
    ''' <param name="sIdSheet">Id de la hoja de aplicacion Qlik Sense</param>
    ''' <param name="sSheet">Nombre de la hoja de aplicacion Qlik Sense</param>
    ''' <returns>Identificador del registro creado o modificado</returns>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function GuardarQlikApp(ByVal lId As String, ByVal sNombre As String, ByVal bActiva As String, ByVal sUrl As String, ByVal lPuertoRestAPI As String, ByVal sIdApp As String, ByVal sApp As String, ByVal sIdSheet As String, ByVal sSheet As String)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oQlikApps As Fullstep.FSNServer.FSNQlikApps = FSNServer.Get_Object(GetType(Fullstep.FSNServer.FSNQlikApps))
        Dim oQlikApp As Fullstep.FSNServer.FSNQlikApp = FSNServer.Get_Object(GetType(Fullstep.FSNServer.FSNQlikApp))
        Dim lResultadoGuardado As Long = 0

        Try
            If (Not String.IsNullOrEmpty(lId)) AndAlso (IsNumeric(lId)) Then
                oQlikApp.Id = CLng(lId)
            Else
                oQlikApp.Id = 0
            End If
            oQlikApp.Nombre = sNombre
            oQlikApp.Activa = CBool(bActiva)
            oQlikApp.Url = sUrl
            If (Not String.IsNullOrEmpty(lPuertoRestAPI)) AndAlso (IsNumeric(lPuertoRestAPI)) Then
                oQlikApp.RestAPIPort = CLng(lPuertoRestAPI)
            Else
                oQlikApp.RestAPIPort = 0
            End If
            oQlikApp.IdApp = sIdApp
            oQlikApp.App = sApp
            oQlikApp.IdSheet = sIdSheet
            oQlikApp.Sheet = sSheet

            oQlikApps.FSNQlikApp = oQlikApp

            lResultadoGuardado = oQlikApps.GuardarFSNQlikApp()
        Catch ex As Exception
            Throw ex
        End Try

        Return lResultadoGuardado
    End Function

    ''' <summary>
    ''' Obtiene la Qlik App indicada de la tabla QLIK_APPS
    ''' </summary>
    ''' <param name="lId">Identificador de la QlikApp en la tabla QLIK_APPS</param>
    ''' <returns>La Qlik App de la tabla QLIK_APPS indicada</returns>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function ObtenerQlikApp(ByVal lId As String)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oQlikApps As Fullstep.FSNServer.FSNQlikApps = FSNServer.Get_Object(GetType(Fullstep.FSNServer.FSNQlikApps))
        Dim oQlikApp As Fullstep.FSNServer.FSNQlikApp = FSNServer.Get_Object(GetType(Fullstep.FSNServer.FSNQlikApp))

        Try

            If (Not String.IsNullOrEmpty(lId)) AndAlso (IsNumeric(lId)) Then
                oQlikApps.ObtenerFSNQlikApp(lId)
                oQlikApp = oQlikApps.FSNQlikApp
            End If

        Catch ex As Exception
            Throw ex
        End Try

        Return oQlikApp
    End Function

    ''' <summary>
    ''' Elimina la Qlik App indicada de la tabla QLIK_APPS
    ''' </summary>
    ''' <param name="lId">Identificador de la QlikApp en la tabla QLIK_APPS</param>
    ''' <returns>Identificador del registro eliminado</returns>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function EliminarQlikApp(ByVal lId As String)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oQlikApps As Fullstep.FSNServer.FSNQlikApps = FSNServer.Get_Object(GetType(Fullstep.FSNServer.FSNQlikApps))
        Dim lResultadoEliminado As Long = 0

        Try

            If (Not String.IsNullOrEmpty(lId)) AndAlso (IsNumeric(lId)) Then
                lResultadoEliminado = oQlikApps.EliminarFSNQlikApp(lId)
            End If

        Catch ex As Exception
            Throw ex
        End Try

        Return lResultadoEliminado
    End Function

    ''' <summary>
    ''' Obtiene los registros de perfiles de la plataforma y usuarios Qlik asignados desde la tabla QLIK_PERF_USUQLIK
    ''' </summary>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function ObtenerPerfilesUsuariosQlikAsignados()
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim oFSNQlikConfig As Fullstep.FSNServer.FSNQlikConfig = FSNServer.Get_Object(GetType(Fullstep.FSNServer.FSNQlikConfig))
        Dim dsPerfUsuQlik As New DataSet
        Dim lstPerfUsuQlik As New List(Of Dictionary(Of String, Object))

        Try
            dsPerfUsuQlik = oFSNQlikConfig.ObtenerPerfilesUsuariosQlikAsignados(FSNUser.IdiomaCod)
            If dsPerfUsuQlik.Tables.Count = 1 Then
                For Each drPerfUsuQlik As DataRow In dsPerfUsuQlik.Tables(0).Rows
                    Dim dicRowPerfUsuQlik As New Dictionary(Of String, Object)
                    For Each dcPerfUsuQlik As DataColumn In dsPerfUsuQlik.Tables(0).Columns
                        dicRowPerfUsuQlik.Add(dcPerfUsuQlik.ColumnName, drPerfUsuQlik(dcPerfUsuQlik.ColumnName))
                    Next
                    lstPerfUsuQlik.Add(dicRowPerfUsuQlik)
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return lstPerfUsuQlik
    End Function

    ''' <summary>
    ''' Obtiene los servidores Qlik configurados en la tabla QLIK_APPS
    ''' </summary>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function ObtenerServidoresQlikConfigurados()
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim oFSNQlikConfig As Fullstep.FSNServer.FSNQlikConfig = FSNServer.Get_Object(GetType(Fullstep.FSNServer.FSNQlikConfig))
        Dim dsServidoresQlik As New DataSet
        Dim lstServidoresQlik As New List(Of Dictionary(Of String, Object))

        Try
            dsServidoresQlik = oFSNQlikConfig.ObtenerServidoresQlikConfigurados()
            If dsServidoresQlik.Tables.Count = 1 Then
                For Each drServidoresQlik As DataRow In dsServidoresQlik.Tables(0).Rows
                    Dim dicRowServidoresQlik As New Dictionary(Of String, Object)
                    For Each dcServidoresQlik As DataColumn In dsServidoresQlik.Tables(0).Columns
                        dicRowServidoresQlik.Add(dcServidoresQlik.ColumnName, drServidoresQlik(dcServidoresQlik.ColumnName))
                    Next
                    lstServidoresQlik.Add(dicRowServidoresQlik)
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return lstServidoresQlik
    End Function

    ''' <summary>
    ''' Obtiene los usuarios de Qlik disponibles en el servidor Qlik Sense indicado
    ''' </summary>
    ''' <param name="sUrl">Url del servidor Qlik Sense</param>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function ObtenerUsuariosQlik(ByVal sUrl As String)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim oQlikSenseServer As Fullstep.FSNServer.QlikSenseServer = FSNServer.Get_Object(GetType(Fullstep.FSNServer.QlikSenseServer))
        Dim oFSNQlikConfig As Fullstep.FSNServer.FSNQlikConfig = FSNServer.Get_Object(GetType(Fullstep.FSNServer.FSNQlikConfig))
        Dim oQlikUsers As New List(Of QlikSenseServer.QlikUser)
        Dim oQlikUsersTemp As New List(Of QlikSenseServer.QlikUser)

        Try
            oQlikSenseServer.UrlServidorQlik = sUrl

            If oQlikSenseServer.ExisteRepositorioQlik() Then
                Dim sCodigoCliente As String = oFSNQlikConfig.ObtenerCodigoCliente()
                ''Obtenemos todos los usuarios disponibles
                oQlikUsers = oQlikSenseServer.ObtenerUsuariosQlik()
                oQlikUsersTemp.AddRange(oQlikUsers)
                ''Una vez obtenidos todos los usuarios, eliminamos de la lista los usuarios :
                ''1. que no tengan permiso de lectura sobre el Stream que corresponde al cliente
                ''2. o que no tengan permiso de lectura sobre ningun Stream para el caso de Fullstep "FSN"
                For Each oQlikUser As QlikSenseServer.QlikUser In oQlikUsers
                    ''Si el codigo de cliente es "FSN" no filtramos por Stream
                    ''El codigo de cliente "FSN" pertenece a Fullstep en Corporate
                    If (sCodigoCliente <> "") And (sCodigoCliente = "FSN") Then
                        Dim oQlikStreams As New List(Of QlikSenseServer.QlikStream)
                        oQlikStreams = oQlikSenseServer.ObtenerStreamsAccionUsuarioQlik("Read", oQlikUser.id)
                        If oQlikStreams.Count = 0 Then
                            oQlikUsersTemp.Remove(oQlikUser)
                        End If
                    Else
                        Dim oQlikStreams As New List(Of QlikSenseServer.QlikStream)
                        oQlikStreams = oQlikSenseServer.ObtenerStreamsAccionUsuarioQlik("Read", oQlikUser.id)
                        If Not oQlikStreams.Exists(Function(oQlikStream) oQlikStream.name = sCodigoCliente) Then
                            oQlikUsersTemp.Remove(oQlikUser)
                        End If
                    End If
                Next
                oQlikUsers = oQlikUsersTemp
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return oQlikUsers.OrderBy(Function(x) x.userDirectory).ThenBy(Function(x) x.userId)
    End Function

    ''' <summary>
    ''' Obtiene los perfiles de la plataforma que todavia no tienen asignado un usuario Qlik
    ''' </summary>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function ObtenerPerfilesFSNNoAsignados()
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim oFSNQlikConfig As Fullstep.FSNServer.FSNQlikConfig = FSNServer.Get_Object(GetType(Fullstep.FSNServer.FSNQlikConfig))
        Dim dsPerfilesFSN As New DataSet
        Dim lstPerfilesFSN As New List(Of Dictionary(Of String, Object))

        Try
            dsPerfilesFSN = oFSNQlikConfig.ObtenerPerfilesFSNNoAsignados(FSNUser.IdiomaCod)
            If dsPerfilesFSN.Tables.Count = 1 Then
                For Each drPerfilesFSN As DataRow In dsPerfilesFSN.Tables(0).Rows
                    Dim dicRowPerfilesFSN As New Dictionary(Of String, Object)
                    For Each dcPerfilesFSN As DataColumn In dsPerfilesFSN.Tables(0).Columns
                        dicRowPerfilesFSN.Add(dcPerfilesFSN.ColumnName, drPerfilesFSN(dcPerfilesFSN.ColumnName))
                    Next
                    lstPerfilesFSN.Add(dicRowPerfilesFSN)
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return lstPerfilesFSN
    End Function

    ''' <summary>
    ''' Guarda las asignaciones de perfiles de la plataforma y usuarios de Qlik en la tabla QLIK_PERF_USUQLIK
    ''' </summary>
    ''' <param name="lstPerfUsuQlik">Objeto con los datos de las asignaciones configuradas en la ventana de "Acceso a Qlik"</param>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function GuardarPerfilesUsuariosQlik(ByVal lstPerfUsuQlik As List(Of Dictionary(Of String, String)))
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oFSNQlikConfig As Fullstep.FSNServer.FSNQlikConfig = FSNServer.Get_Object(GetType(Fullstep.FSNServer.FSNQlikConfig))
        Dim lstPerfUsuQlikTemp As New List(Of Dictionary(Of String, String))
        Dim dtPerfUsuQlik As New DataTable
        Dim iResultadoGuardado As Integer

        Try
            lstPerfUsuQlikTemp.AddRange(lstPerfUsuQlik)
            For Each dicPerfUsuQlik As Dictionary(Of String, String) In lstPerfUsuQlik
                If (dicPerfUsuQlik.Values.Contains(Nothing)) Or (dicPerfUsuQlik.Values.Contains(String.Empty)) Then
                    lstPerfUsuQlikTemp.Remove(dicPerfUsuQlik)
                End If
            Next
            ''Siempre tendremos al menos un elemento en los datos de la tabla de la ventana "Acceso a Qlik", correspondiente a la fila de "Nuevo"
            For Each sKey In lstPerfUsuQlik(0).Keys
                dtPerfUsuQlik.Columns.Add(sKey)
            Next
            For Each dicPerfUsuQlik As Dictionary(Of String, String) In lstPerfUsuQlikTemp
                Dim drPerfUsuQlik As DataRow = dtPerfUsuQlik.NewRow()
                For Each dcPerfUsuQlik As DataColumn In dtPerfUsuQlik.Columns
                    drPerfUsuQlik(dcPerfUsuQlik.ColumnName) = dicPerfUsuQlik.Item(dcPerfUsuQlik.ColumnName)
                Next
                dtPerfUsuQlik.Rows.Add(drPerfUsuQlik)
            Next
            iResultadoGuardado = oFSNQlikConfig.GuardarPerfilesUsuariosQlik(dtPerfUsuQlik)
        Catch ex As Exception
            Throw ex
            iResultadoGuardado = -2
        End Try

        Return iResultadoGuardado
    End Function

#End Region

End Class
