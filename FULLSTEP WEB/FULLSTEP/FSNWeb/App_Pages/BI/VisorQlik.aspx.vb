﻿Imports Fullstep.FSNLibrary.TiposDeDatos
Imports Fullstep.FSNServer
Imports System.Web.Script.Serialization
Imports System.Net

Public Class VisorQlik
    Inherits FSNPage

    Friend mDBServer As Root
    Friend Function DBServer() As Root
        If mDBServer Is Nothing Then
            mDBServer = New Root
        End If
        Return mDBServer
    End Function


#Region "Inicio"

    ''' <summary>
    ''' Al iniciar la pagina establece EnableScriptGlobalization
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub VisorQlik_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ScriptMgr.EnableScriptGlobalization = True
    End Sub

    ''' <summary>
    ''' Cargar la pagina
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0,2</remarks>
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Miramos si el usuario tiene permiso para acceder a este apartado si no puede acceder lo mandamos a la página de inicio
        Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
        Dim oUser As FSNServer.User = pag.Usuario
        If Not oUser.AccesoBI Then
            Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "Inicio.aspx")
        End If

        Seccion = "VisorQlik"

        'Seleccionar el menu
        Dim mMaster = CType(MyBase.Master, FSNWeb.Menu)
        mMaster.Seleccionar("Informes", "QlikViewer")

        Dim mMasterTotal As FSNWeb.Cabecera = CType(mMaster.Master, FSNWeb.Cabecera)
        Dim scriptReference As ScriptReference
        scriptReference = New ScriptReference
        scriptReference.Path = ConfigurationManager.AppSettings("ruta") & "js/jsUpdateProgress.js"
        CType(mMasterTotal.FindControl("ScriptManager1"), ScriptManager).Scripts.Add(scriptReference)

        'Ventana de Cargando
        Dim ModalProgressClientID As String = CType(Master.Master.FindControl("ModalProgress"), AjaxControlToolkit.ModalPopupExtender).ClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalClientID", "<script>var ModalProgress = '" & ModalProgressClientID & "' </script>")

        'Cargo los diferentes textos
        CargarIdiomas()

        FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/Qlik.png"


        If Not IsPostBack Then
            ''Acciones NO PostBack

            ''Obtenemos el usuario de Qlik asociado al perfil del usuario FSN que accede al visor y el codigo del cliente
            Dim oFSNQlikConfig As Fullstep.FSNServer.FSNQlikConfig = FSNServer.Get_Object(GetType(Fullstep.FSNServer.FSNQlikConfig))
            Dim dicIdUrlUsuarioQlik As Dictionary(Of String, String) = oFSNQlikConfig.ObtenerUsuarioQlik(oUser.Cod)
            Dim sIdUsuarioQlik As String = dicIdUrlUsuarioQlik.Keys(0).ToString()
            Dim sUrlUsuarioQlik As String = dicIdUrlUsuarioQlik.Values(0).ToString()
            Dim sCodigoCliente As String = oFSNQlikConfig.ObtenerCodigoCliente()

            ''Obtenemos el servidor Qlik Sense al que pertenece el usuario de Qlik asociado, el objeto usuario Qlik asociado y nos autenticaremos
            Dim oQlikSenseServer As Fullstep.FSNServer.QlikSenseServer = FSNServer.Get_Object(GetType(Fullstep.FSNServer.QlikSenseServer))
            Dim oQlikUser As New Fullstep.FSNServer.QlikSenseServer.QlikUser
            Dim oQlikTicket As New Fullstep.FSNServer.QlikSenseServer.QlikTicket.TicketResponse

            ''Obtenemos las Qlik Apps de la tabla QLIK_APPS y realizamos el filtrado
            Dim oFSNQlikApps As Fullstep.FSNServer.FSNQlikApps = FSNServer.Get_Object(GetType(Fullstep.FSNServer.FSNQlikApps))
            Dim oListaFSNQlikApps As New List(Of Fullstep.FSNServer.FSNQlikApp)
            Dim oQlikApps As New List(Of Fullstep.FSNServer.QlikSenseServer.QlikApp)

            ''Si el perfil tiene asociado un usuario de Qlik y este tiene una Url de servidor Qlik Sense
            If (sIdUsuarioQlik <> "") AndAlso (sUrlUsuarioQlik <> "") Then
                ''Primero, obtenemos el objeto servidor de Qlik Sense del usuario de Qlik asociado
                oQlikSenseServer.UrlServidorQlik = sUrlUsuarioQlik
                ''Segundo, si existe el servidor y podemos autenticarnos...
                If (oQlikSenseServer.ExisteRepositorioQlik) Then
                    If (Not String.IsNullOrEmpty(oQlikSenseServer.UrlProxyServiceAPI)) AndAlso
                    (oQlikSenseServer.UrlProxyServiceAPI <> "") Then
                        ''obtenemos el objeto usuario Qlik
                        oQlikUser = oQlikSenseServer.ObtenerUsuarioQlik(sIdUsuarioQlik)
                        If Not IsNothing(oQlikUser) Then
                            ''nos autenticamos en el servidor Qlik Sense con este usuario Qlik
                            oQlikTicket = oQlikSenseServer.ObtenerTicketQlik(oQlikUser.userDirectory, oQlikUser.userId)
                        End If
                        ''obtenemos las aplicaciones con permiso de lectura para el usuario, desde el repositorio correspondiente de la Qlik App configurada
                        oQlikApps = oQlikSenseServer.ObtenerAplicacionesAccionUsuarioQlik("Read", sIdUsuarioQlik)
                        ''aplicamos el filtro de obtener solo las Qlik Apps activas
                        oFSNQlikApps.ObtenerFSNQlikApps(True)
                        ''y solo añadimos a la lista definitiva las Qlik Apps que sean del usuario
                        For Each oFSNQlikApp As Fullstep.FSNServer.FSNQlikApp In oFSNQlikApps.FSNQlikApps
                            ''Para eso, si la Url de la Qlik App configurada es la misma que la del servidor del usuario Qlik asociado...
                            If oFSNQlikApp.Url = sUrlUsuarioQlik Then
                                ''añadimos la Qlik App configurada que estamos evaluando a la lista definitiva, mirando si esta en el listado de aplicaciones con permiso de lectura para el usuario Qlik asociado
                                Dim bExisteAplicacion As Boolean = False
                                If oQlikApps.Exists(Function(oQlikApp) oQlikApp.id = oFSNQlikApp.IdApp) Then
                                    bExisteAplicacion = True
                                Else
                                    '' Miramos por si acaso si se ha duplicado la aplicación para eliminar y despublicarla del stream y luego publicar la duplicada con los cambios y el mismo nombre.
                                    '' Tendría otro id, por eso no la encontramos en la primera búsqueda.
                                    Dim sNombreAppPosibleDuplicada = ""
                                    If (oFSNQlikApp.App.Contains("\")) AndAlso (oFSNQlikApp.App.Split("\").Length = 2) Then
                                        sNombreAppPosibleDuplicada = oFSNQlikApp.App.Split("\")(1)
                                    Else
                                        sNombreAppPosibleDuplicada = oFSNQlikApp.App
                                    End If
                                    If oQlikApps.Exists(Function(oQlikApp) oQlikApp.name = sNombreAppPosibleDuplicada) Then
                                        bExisteAplicacion = True
                                        Dim iIndiceAppDuplicada = oQlikApps.FindIndex(Function(EvaluatedQlikApp) EvaluatedQlikApp.name = sNombreAppPosibleDuplicada)
                                        oFSNQlikApp.IdApp = oQlikApps.ElementAt(iIndiceAppDuplicada).id
                                        '' Miramos por si acaso si ha cambiado el id de la hoja de la aplicación, al eliminar y despublicarla del stream y luego publicar la duplicada con los cambios y el mismo nombre.
                                        '' Tendría otro id.
                                        Dim oQlikAppObjects As New List(Of Fullstep.FSNServer.QlikSenseServer.QlikAppObject)
                                        oQlikAppObjects = oQlikSenseServer.ObtenerObjetosQlik(oFSNQlikApp.IdApp, "sheet", True)
                                        If oQlikAppObjects.Exists(Function(oQlikAppObject) oQlikAppObject.name = oFSNQlikApp.Sheet) Then
                                            Dim iIndiceHojaDuplicada = oQlikAppObjects.FindIndex(Function(EvaluatedQlikAppObject) EvaluatedQlikAppObject.name = oFSNQlikApp.Sheet)
                                            oFSNQlikApp.IdSheet = oQlikAppObjects.ElementAt(iIndiceHojaDuplicada).id
                                        End If
                                        '' Si hemos actualizado algún id (aplicación u hoja) debido a cambios en el servidor Qlik Sense, actualizamos estos id en BBDD
                                        oFSNQlikApps.FSNQlikApp = oFSNQlikApp
                                        oFSNQlikApps.GuardarFSNQlikApp()
                                    End If
                                End If
                                If bExisteAplicacion Then
                                    ''Ademas de esto, antes de añadir la Qlik App configurada que estamos evaluando, se comprueba que el nombre de su Stream sea igual que el codigo del cliente 
                                    ''(a no ser que el codigo sea "FSN" que en ese caso no se filtran por Stream)
                                    If (sCodigoCliente <> "") And (sCodigoCliente = "FSN") Then
                                        oListaFSNQlikApps.Add(oFSNQlikApp)
                                    Else
                                        Dim iIndexExistente As Integer = oQlikApps.FindIndex(Function(EvaluatedQlikApp) EvaluatedQlikApp.id = oFSNQlikApp.IdApp)
                                        Dim oQlikApp As Fullstep.FSNServer.QlikSenseServer.QlikApp = oQlikApps.ElementAt(iIndexExistente)
                                        If oQlikApp.stream.name = sCodigoCliente Then
                                            oListaFSNQlikApps.Add(oFSNQlikApp)
                                        End If
                                    End If
                                End If
                            End If
                        Next
                    End If
                End If
            End If

            Dim sJSScript As String
            sJSScript = "$(document).ready(function(){" & vbCrLf &
                        "   $('.PageHeaderBackground').css('cursor','pointer');" & vbCrLf &
                        "   $('.PageHeaderBackground').on('click',function(){TempCargando(); window.location = '" & ConfigurationManager.AppSettings("rutaFS") & "BI/VisorQlik.aspx" & "';});" & vbCrLf &
                        "   $('[id$=pnlQlikViewer]').hide();" & vbCrLf &
                        "   $('[id$=pnlQlikApps]').hide();" & vbCrLf
            ''Si no existen aplicaciones que podamos mostrar, o no hay un usuario de Qlik asociado al perfil, hacemos visible el panel de las apps e indicamos esto con un mensaje
            If oListaFSNQlikApps.Count = 0 Then
                If sIdUsuarioQlik = "" Then
                    sJSScript &= "   MostrarAvisoEnPanelApps(TextosVisorQlik[7]);" & vbCrLf     ''No hay asociado ningun usuario de Qlik Sense a su perfil
                Else
                    If (Not oQlikSenseServer.ExisteRepositorioQlik) Then
                        sJSScript &= "   MostrarAvisoEnPanelApps(TextosVisorQlik[3]);" & vbCrLf     ''No se puede acceder al servidor de Qlik Sense
                    Else
                        If (String.IsNullOrEmpty(oQlikSenseServer.UrlProxyServiceAPI)) Then
                            sJSScript &= "   MostrarAvisoEnPanelApps(TextosVisorQlik[4]);" & vbCrLf     ''No se puede acceder a la autenticacion del servidor Qlik Sense
                        Else
                            If (String.IsNullOrEmpty(oQlikTicket.Ticket)) Then
                                sJSScript &= "   MostrarAvisoEnPanelApps(TextosVisorQlik[6]);" & vbCrLf     ''No se puede realizar la autenticación en el servidor Qlik Sense
                            Else
                                sJSScript &= "   MostrarAvisoEnPanelApps(TextosVisorQlik[1]);" & vbCrLf     ''No existen aplicaciones disponibles
                            End If
                        End If
                    End If
                End If
            End If
            ''Si hay una app disponible y esta todavia existe y esta accesible en el servidor Qlik Sense, la mostramos en el panel visor
            If oListaFSNQlikApps.Count = 1 Then
                sJSScript &= "   CargarQlikAppEnPanelVisor(" & oListaFSNQlikApps(0).Id & ");" & vbCrLf
            End If
            'Si hay varias apps disponibles y estas todavia existen y estan accesibles en el servidor Qlik Sense, las mostramos en el panel de apps
            If oListaFSNQlikApps.Count > 1 Then
                Dim sInnerHTML As String = ""
                sInnerHTML &= "<table border=" & Chr(34) & "0px" & Chr(34) & "style=" & Chr(34) & "table-layout:fixed; width:100%;" & Chr(34) & ">"
                sInnerHTML &= "<tr>"
                For i As Integer = 0 To (oListaFSNQlikApps.Count - 1)
                    Dim sUrlImagenQlikApp As String = ""
                    Dim sNombreFSNQlikApp As String = ""
                    Dim sNombreQlikApp As String = ""
                    If (Not IsNothing(oListaFSNQlikApps(i))) AndAlso (Not String.IsNullOrEmpty(oListaFSNQlikApps(i).Url)) Then
                        If (oQlikSenseServer.ExisteRepositorioQlik) Then
                            If (Not String.IsNullOrEmpty(oQlikSenseServer.UrlProxyServiceAPI)) AndAlso
                            (oQlikSenseServer.UrlProxyServiceAPI <> "") Then
                                If (Not String.IsNullOrEmpty(oListaFSNQlikApps(i).IdApp)) AndAlso (oQlikSenseServer.ExisteAplicacionQlik(oListaFSNQlikApps(i).IdApp)) Then
                                    Dim oQlikApp As Fullstep.FSNServer.QlikSenseServer.QlikApp = oQlikSenseServer.ObtenerAplicacionQlik(oListaFSNQlikApps(i).IdApp)
                                    If (Not IsNothing(oQlikApp)) AndAlso (oQlikApp.published) Then
                                        If (Not String.IsNullOrEmpty(oQlikTicket.Ticket)) AndAlso (oQlikTicket.Ticket <> "") Then
                                            If oQlikApp.thumbnail <> "" Then
                                                '' Obtenemos de nuevo un QlikTicket para el usuario Qlik Sense configurado, para poder descargar correctamente las imágenes miniatura
                                                '' Se creará una sesión en el servidor por cada imágen descargada, pero el posterior acceso a las aplicaciones o nueva carga de imágenes,
                                                '' no creará nuevas sesiones en el servidor Qlik Sense
                                                oQlikTicket = oQlikSenseServer.ObtenerTicketQlik(oQlikUser.userDirectory, oQlikUser.userId)
                                                sUrlImagenQlikApp = oQlikSenseServer.UrlServidorQlik & oQlikApp.thumbnail & "?QlikTicket=" & oQlikTicket.Ticket
                                            End If
                                            sNombreFSNQlikApp = oListaFSNQlikApps(i).Nombre
                                            sNombreQlikApp = oQlikApp.name
                                        Else
                                            sNombreFSNQlikApp = Textos(6)   ''No se puede realizar la autenticación en el servidor Qlik Sense
                                        End If
                                    Else
                                        sNombreFSNQlikApp = Textos(5)   ''La aplicacion ya no esta disponible
                                    End If
                                Else
                                    sNombreFSNQlikApp = Textos(5)   ''La aplicacion ya no esta disponible
                                End If
                            Else
                                sNombreFSNQlikApp = Textos(4)   ''No se puede acceder a la autenticacion del servidor Qlik Sense
                            End If
                        Else
                            sNombreFSNQlikApp = Textos(3)   ''No se puede acceder al servidor de Qlik Sense
                        End If
                    Else
                        sNombreFSNQlikApp = Textos(2)   ''La Qlik App a la que intenta acceder no esta configurada
                    End If
                    If sUrlImagenQlikApp = "" Then
                        sUrlImagenQlikApp = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/Qlik_Ic_AppBig.png"
                    End If
                    sInnerHTML &= "<td style=" & Chr(34) & "max-width:31%; min-width:31%; width:31%; max-height:300px; padding:1%; cursor:pointer; display:inline-block;" & Chr(34) & " title=" & Chr(34) & sNombreQlikApp & Chr(34) & " onClick=" & Chr(34) & "return CargarQlikAppEnPanelVisor(" & oListaFSNQlikApps(i).Id & ");" & Chr(34) & ">"
                    sInnerHTML &= "<table border=" & Chr(34) & "0px" & Chr(34) & " style=" & Chr(34) & "width:100%;" & Chr(34) & ">"
                    sInnerHTML &= "<tr style=" & Chr(34) & "height:80%" & Chr(34) & "><td style=" & Chr(34) & "width:100%; height:30%; vertical-align:middle; text-align:center;" & Chr(34) & "><img src=" & Chr(34) & sUrlImagenQlikApp & Chr(34) & " style=" & Chr(34) & "width:400px; height:260px;" & Chr(34) & "></td></tr>"
                    sInnerHTML &= "<tr style=" & Chr(34) & "height:20%" & Chr(34) & "><td style=" & Chr(34) & "width:100%; height:100%; vertical-align:middle; text-align:center;" & Chr(34) & "><span style=" & Chr(34) & "font-size:16px; font-weight:bold;" & Chr(34) & ">" & sNombreFSNQlikApp & "</span></td></tr>"
                    sInnerHTML &= "</table>"
                    'sInnerHTML &= "<img src=" & Chr(34) & sUrlImagenQlikApp & Chr(34) & " style=" & Chr(34) & "width:100%; height:100%;" & Chr(34) & ">"
                    If ((i + 1) Mod 3 = 0) Then
                        sInnerHTML &= "</tr>"
                        sInnerHTML &= "<tr>"
                    End If
                Next i
                sInnerHTML &= "</tr>"
                sInnerHTML &= "</table>"
                sJSScript &= "   $('[id$=pnlQlikApps]').html('" & sInnerHTML & "');" & vbCrLf
                sJSScript &= "   $('[id$=pnlQlikApps]').show();" & vbCrLf
                sJSScript &= "   $('[id$=Contenido]').height(($('[id$=pnlQlikApps]').height() + 30));" & vbCrLf
            End If
            sJSScript &= "});" & vbCrLf & vbCrLf
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "JSInicio", sJSScript, True)
        Else
            ''Acciones PostBack
        End If

    End Sub

    ''' <summary>
    ''' Carga los textos con el idioma de la pagina
    ''' </summary>
    Private Sub CargarIdiomas()

        '' Modulo de idiomas del visor de Qlik
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorQlik

        FSNPageHeader.TituloCabecera = Textos(0) 'Qlik Viewer

        'Cargar la variable TextosVisorQlik para los textos en JS
        Dim sVariableJavascriptTextosVisorQlik As String = ""
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosVisorQlik") Then
            sVariableJavascriptTextosVisorQlik = "var TextosVisorQlik = new Array();"
            For i As Integer = 0 To 7 '' TODO: Cambiar el inicio y el fin del for en funcion del numero de textos
                sVariableJavascriptTextosVisorQlik &= "TextosVisorQlik[" & i & "]='" & JSText(Textos(i)) & "';"
            Next
        End If
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosVisorQlik", sVariableJavascriptTextosVisorQlik, True)

    End Sub

#End Region

#Region "Page Methods"

    ''' <summary>
    ''' Carga la Qlik App indicada en el panel visor, si esta todavia existe y esta accesible en el servidor Qlik Sense
    ''' </summary>
    ''' <param name="IdFSNQlikApp">Identificador de la Qlik App en la tabla QLIK_APPS</param>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function CargarQlikAppEnPanelVisor(ByVal IdFSNQlikApp As String)
        ''Comprobamos si la Qlik App almacenada en la tabla QLIK_APPS existe y esta accesible en el servidor Qlik Sense
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim oFSNQlikConfig As Fullstep.FSNServer.FSNQlikConfig = FSNServer.Get_Object(GetType(Fullstep.FSNServer.FSNQlikConfig))
        Dim oFSNQlikApps As Fullstep.FSNServer.FSNQlikApps = FSNServer.Get_Object(GetType(Fullstep.FSNServer.FSNQlikApps))
        Dim jsSerializer As New JavaScriptSerializer()
        Dim dicResultado As New Dictionary(Of String, String)
        Dim dicCadena As New Dictionary(Of String, String)
        Dim lstResultadoDevuelto As New List(Of Object)
        Dim sIdUsuarioQlik As String = oFSNQlikConfig.ObtenerUsuarioQlik(FSNUser.Cod).Keys(0).ToString()

        Try
            If (Not String.IsNullOrEmpty(IdFSNQlikApp)) AndAlso (IsNumeric(IdFSNQlikApp)) Then
                oFSNQlikApps.ObtenerFSNQlikApp(IdFSNQlikApp)
                If (Not IsNothing(oFSNQlikApps.FSNQlikApp)) AndAlso (Not String.IsNullOrEmpty(oFSNQlikApps.FSNQlikApp.Url)) Then
                    Dim oQlikSenseServer As Fullstep.FSNServer.QlikSenseServer = FSNServer.Get_Object(GetType(Fullstep.FSNServer.QlikSenseServer))
                    oQlikSenseServer.UrlServidorQlik = oFSNQlikApps.FSNQlikApp.Url
                    If (oQlikSenseServer.ExisteRepositorioQlik) Then
                        If (Not String.IsNullOrEmpty(oQlikSenseServer.UrlProxyServiceAPI)) AndAlso _
                        (oQlikSenseServer.UrlProxyServiceAPI <> "") Then
                            If (oQlikSenseServer.ExisteAplicacionQlik(oFSNQlikApps.FSNQlikApp.IdApp)) Then
                                Dim sUrlIFrame As String
                                sUrlIFrame = IIf(oFSNQlikApps.FSNQlikApp.Url.EndsWith("/"), oFSNQlikApps.FSNQlikApp.Url, oFSNQlikApps.FSNQlikApp.Url & "/") & "sense/app/" & oFSNQlikApps.FSNQlikApp.IdApp
                                If (Not String.IsNullOrEmpty(oFSNQlikApps.FSNQlikApp.IdSheet)) AndAlso (oFSNQlikApps.FSNQlikApp.IdSheet <> "") Then
                                    Dim oQlikAppSheet As Fullstep.FSNServer.QlikSenseServer.QlikAppObject = oQlikSenseServer.ObtenerObjetoQlik(oFSNQlikApps.FSNQlikApp.IdSheet)
                                    If (Not IsNothing(oQlikAppSheet)) AndAlso (Not String.IsNullOrEmpty(oQlikAppSheet.engineObjectId)) AndAlso (oQlikAppSheet.engineObjectId <> "") Then
                                        sUrlIFrame &= "/sheet/" & oQlikAppSheet.engineObjectId & "/state/analysis"
                                    End If
                                End If
                                sUrlIFrame &= "/language/" & FSNUser.Idioma_CodigoUniversal & "/select/idioma/" & FSNUser.Idioma_CodigoUniversal & "/"
                                Dim oQlikUser As Fullstep.FSNServer.QlikSenseServer.QlikUser = oQlikSenseServer.ObtenerUsuarioQlik(sIdUsuarioQlik)
                                Dim oQlikTicket As Fullstep.FSNServer.QlikSenseServer.QlikTicket.TicketResponse = oQlikSenseServer.ObtenerTicketQlik(oQlikUser.userDirectory, oQlikUser.userId)
                                If (Not IsNothing(oQlikTicket)) AndAlso (Not String.IsNullOrEmpty(oQlikTicket.Ticket)) AndAlso (oQlikTicket.Ticket <> "") Then
                                    sUrlIFrame &= "?QlikTicket=" & oQlikTicket.Ticket
                                    dicResultado.Add("resultado", "success")
                                    dicCadena.Add("cadena", sUrlIFrame)
                                    lstResultadoDevuelto.Add(dicResultado)
                                    lstResultadoDevuelto.Add(dicCadena)
                                    Return lstResultadoDevuelto
                                Else
                                    dicResultado.Add("resultado", "error")
                                    dicCadena.Add("cadena", 6)              ''No se puede realizar la autenticación en el servidor Qlik Sense
                                    lstResultadoDevuelto.Add(dicResultado)
                                    lstResultadoDevuelto.Add(dicCadena)
                                    Return lstResultadoDevuelto
                                End If
                            Else
                                dicResultado.Add("resultado", "error")
                                dicCadena.Add("cadena", 5)              ''La aplicacion ya no esta disponible
                                lstResultadoDevuelto.Add(dicResultado)
                                lstResultadoDevuelto.Add(dicCadena)
                                Return lstResultadoDevuelto
                            End If
                        Else
                            dicResultado.Add("resultado", "error")
                            dicCadena.Add("cadena", 4)              ''No se puede acceder a la autenticacion del servidor Qlik Sense
                            lstResultadoDevuelto.Add(dicResultado)
                            lstResultadoDevuelto.Add(dicCadena)
                            Return lstResultadoDevuelto
                        End If
                    Else
                        dicResultado.Add("resultado", "error")
                        dicCadena.Add("cadena", 3)              ''No se puede acceder al servidor de Qlik Sense
                        lstResultadoDevuelto.Add(dicResultado)
                        lstResultadoDevuelto.Add(dicCadena)
                        Return lstResultadoDevuelto
                    End If
                Else
                    dicResultado.Add("resultado", "error")
                    dicCadena.Add("cadena", 2)              ''La Qlik App a la que intenta acceder no esta configurada
                    lstResultadoDevuelto.Add(dicResultado)
                    lstResultadoDevuelto.Add(dicCadena)
                    Return lstResultadoDevuelto
                End If
            Else
                dicResultado.Add("resultado", "error")
                dicCadena.Add("cadena", 2)              ''La Qlik App a la que intenta acceder no esta configurada
                lstResultadoDevuelto.Add(dicResultado)
                lstResultadoDevuelto.Add(dicCadena)
                Return lstResultadoDevuelto
            End If
        Catch ex As Exception
            Throw ex
        End Try

        dicResultado.Add("resultado", "exception")
        dicCadena.Add("cadena", "")
        lstResultadoDevuelto.Add(dicResultado)
        lstResultadoDevuelto.Add(dicCadena)
        Return lstResultadoDevuelto

    End Function

#End Region

End Class