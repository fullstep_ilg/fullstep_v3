﻿Imports System.Web.Script.Serialization
Imports Fullstep.FSNLibrary.TiposDeDatos

Public Class VisorActividad
    Inherits FSNPage

    'Carga el combo de los entornos
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Miramos si el usuario tiene permiso para acceder a este apartado si no puede acceder lo mandamos a la página de inicio
        Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
        Dim oUser As FSNServer.User = pag.Usuario
        If (Not oUser.AccesoFSAL) Or (Not oUser.ALVisorActividad) Then
            Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "Inicio.aspx")
        End If

        'Seleccionar el menu
        Dim mMaster = CType(MyBase.Master, FSNWeb.Menu)
        mMaster.Seleccionar("Informes", "Visor Actividad")

        'Ventana de Cargando
        Dim ModalProgressClientID As String = CType(Master.Master.FindControl("ModalProgress"), AjaxControlToolkit.ModalPopupExtender).ClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalClientID", "<script>var ModalProgress = '" & ModalProgressClientID & "' </script>")

        'Carga del combo de entornos
        Dim listaEntornos As Fullstep.FSNServer.Entornos = FSNServer.Get_Object(GetType(Fullstep.FSNServer.Entornos))
        listaEntornos.Load(FSNUser.IdiomaCod)
        If (listaEntornos.data.Tables(0).Rows.Count = 1) Then
            lblEntorno.Visible = True
            ddlEnviroment.Visible = False
            lblEntorno.Text = listaEntornos.data.Tables(0).Rows(0).Item("DEN")
            lblCodEntorno.Text = listaEntornos.data.Tables(0).Rows(0).Item("COD")
        Else
            lblEntorno.Visible = False
            ddlEnviroment.Visible = True
            ddlEnviroment.DataSource = listaEntornos.data
            ddlEnviroment.DataValueField = "COD"
            ddlEnviroment.DataTextField = "DEN"
            ddlEnviroment.DataBind()
        End If


        'Cargo la variable TextosVisorActividad para los textos
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorActividad
        Dim sVariableJavascriptTextosVisorActividad As String = ""
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosVisorActividad") Then
            sVariableJavascriptTextosVisorActividad = "var TextosVisorActividad = new Array();"
            For i As Integer = 0 To 65
                sVariableJavascriptTextosVisorActividad &= "TextosVisorActividad[" & i & "]='" & JSText(Textos(i)) & "';"
            Next
        End If
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosCubos", sVariableJavascriptTextosVisorActividad, True)

        'Carga del combo tipo de evento
        Dim listaEventos As Fullstep.FSNServer.TipoEventoVisorActividades = FSNServer.Get_Object(GetType(Fullstep.FSNServer.TipoEventoVisorActividades))
        listaEventos.TipoEventoLoad(FSNUser.IdiomaCod)
        ddlTipoEvento.DataSource = listaEventos.data
        ddlTipoEvento.DataValueField = "ID"
        ddlTipoEvento.DataTextField = "DEN"
        ddlTipoEvento.DataBind()
        ddlTipoEvento.SelectedValue = 1 ''El tipoEvento que se pasa inicialmente es '1' -> 'Nueva solicitud de compra'


        ddlTipoEventoGrid.DataSource = listaEventos.data
        ddlTipoEventoGrid.DataValueField = "ID"
        ddlTipoEventoGrid.DataTextField = "DEN"
        ddlTipoEventoGrid.DataBind()
        ddlTipoEventoGrid.Items.Add(Textos(34))
        ddlTipoEventoGrid.SelectedValue = Textos(34)    ''El evento de la lista que se pasa inicialmente es 'TODOS'/'ALL'

    End Sub

#Region "PageMethods"


    <System.Web.Services.WebMethod(True), _
   System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function IdiomaUsuario()
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Return FSNUser.Idioma
    End Function

    'Devuelve el formato de la fecha elegida
    <System.Web.Services.WebMethod(True), _
   System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function FormatoFecha()
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Return FSNUser.DateFormat.ShortDatePattern.ToString()
    End Function

    'Devuelve el primer entorno encontrado en la tabla FSAL_ENTORNOS
    <System.Web.Services.WebMethod(True), _
   System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function RecuperarEntorno()
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim Acceso As FSNServer.Accesos = FSNServer.Get_Object(GetType(Fullstep.FSNServer.Accesos))
        Dim entorno As String = Acceso.RecuperarEntorno()
        Return entorno
    End Function


    'Devuelve los usuarios conectados en un rango de tiempo
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function UsuariosConectadosPorRango(ByVal fechaInicio As DateTime, ByVal fechaFin As DateTime, ByVal rango As String, ByVal entorno As String, ByVal pyme As String, ByVal timeoffset As Double)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim listaAccesos As FSNServer.Accesos = FSNServer.Get_Object(GetType(Fullstep.FSNServer.Accesos))
        Dim fechaDesde, fechaHasta As DateTime
        Dim dFechaInicioOrig As DateTime = fechaInicio

        If (rango = "HORA" Or rango = "HORA-NOW") Then
            fechaDesde = New DateTime(fechaInicio.Year, fechaInicio.Month, fechaInicio.Day, fechaInicio.Hour, fechaInicio.Minute, 0)
            fechaHasta = New DateTime(fechaFin.Year, fechaFin.Month, fechaFin.Day, fechaFin.Hour, fechaFin.Minute, 0)
        Else
            fechaDesde = New DateTime(fechaInicio.Year, fechaInicio.Month, fechaInicio.Day, fechaInicio.Hour, fechaInicio.Minute, fechaInicio.Second)
            fechaHasta = New DateTime(fechaFin.Year, fechaFin.Month, fechaFin.Day, fechaFin.Hour, fechaFin.Minute, fechaFin.Second)
        End If

        If (rango = "HORA" Or rango = "HORA-NOW" Or rango = "DIA") Then
            fechaDesde = DateAdd(DateInterval.Minute, timeoffset, fechaDesde)
            fechaHasta = DateAdd(DateInterval.Minute, timeoffset, fechaHasta)
        End If
        listaAccesos.LoadPorRango(fechaDesde, fechaHasta, rango, entorno, pyme)
        If Not listaAccesos.Accesos Is Nothing And (rango = "HORA" Or rango = "HORA-NOW" Or rango = "DIA") Then
            For Each item In listaAccesos.Accesos
                item.horaAcceso = DateAdd(DateInterval.Minute, -timeoffset, item.horaAcceso).ToShortTimeString()
                If (rango = "DIA") AndAlso (fechaInicio.Day = Now().Day) Then
                    '**************** DSL: No mostrar sesiones abiertas de GS pasada la hora actual ******************************************
                    Dim laHora As String() = item.horaAcceso.split(":")
                    If DatePart(DateInterval.Hour, Now()) < CInt(laHora(0)) Then 'horas posteriores a la actual --> poner a 0 GS
                        item.numUsuariosGS = 0
                    End If
                    '*************************************************************************************************************************
                End If
            Next
        End If
        If Not listaAccesos.Accesos Is Nothing Then
            For i As Integer = 1 To listaAccesos.Accesos.Count
                Dim dHoraAcceso As DateTime
                Select Case rango
                    Case "HORA-NOW", "HORA", "DIA"
                        dHoraAcceso = New DateTime(dFechaInicioOrig.Year, dFechaInicioOrig.Month, dFechaInicioOrig.Day, CDate(listaAccesos.Accesos(i).horaAcceso).Hour, CDate(listaAccesos.Accesos(i).horaAcceso).Minute, 0)
                    Case "SEMANA", "MES"
                        dHoraAcceso = dFechaInicioOrig.AddDays((i - 1))
                    Case "ANYO"
                        dHoraAcceso = New DateTime(fechaInicio.Year, i, 1, 0, 0, 0)
                End Select
                If dHoraAcceso > Now() Then
                    listaAccesos.Accesos(i).numUsuariosPortal = -2
                    listaAccesos.Accesos(i).numUsuariosWeb = -2
                    listaAccesos.Accesos(i).numUsuariosGS = -2
                    listaAccesos.Accesos(i).avgUsuariosPortal = -2
                    listaAccesos.Accesos(i).avgUsuariosWeb = -2
                    listaAccesos.Accesos(i).avgUsuariosGS = -2
                End If
            Next i
        End If
        Return listaAccesos.Accesos
    End Function

    'Devuelve los usuarios con actividad reciente entre dichas fechas, para ese entorno y con el tipo de evento especificado
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function UsuariosConectadosPorRangoRecentActivity(ByVal fechaInicio As DateTime, ByVal fechaFin As DateTime, ByVal entorno As String, ByVal rango As String, ByVal tipoEvento As String, ByVal pyme As String, ByVal timeoffset As Double)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim listaAccesos As FSNServer.Accesos = FSNServer.Get_Object(GetType(Fullstep.FSNServer.Accesos))
        Dim fechaDesde, fechaHasta As DateTime
        Dim dFechaInicioOrig As DateTime = fechaInicio

        If (rango = "HORA" Or rango = "HORA-NOW") Then
            fechaDesde = New DateTime(fechaInicio.Year, fechaInicio.Month, fechaInicio.Day, fechaInicio.Hour, fechaInicio.Minute, 0)
            fechaHasta = New DateTime(fechaFin.Year, fechaFin.Month, fechaFin.Day, fechaFin.Hour, fechaFin.Minute, 0)
        Else
            fechaDesde = New DateTime(fechaInicio.Year, fechaInicio.Month, fechaInicio.Day, fechaInicio.Hour, fechaInicio.Minute, fechaInicio.Second)
            fechaHasta = New DateTime(fechaFin.Year, fechaFin.Month, fechaFin.Day, fechaFin.Hour, fechaFin.Minute, fechaFin.Second)
        End If

        If (rango = "HORA" Or rango = "HORA-NOW" Or rango = "DIA") Then
            fechaDesde = DateAdd(DateInterval.Minute, timeoffset, fechaDesde)
            fechaHasta = DateAdd(DateInterval.Minute, timeoffset, fechaHasta)
        End If
        listaAccesos.LoadPorRangoRecentActivity(fechaDesde, fechaHasta, entorno, rango, tipoEvento, pyme)
        If (Not listaAccesos.Accesos Is Nothing And (rango = "HORA" Or rango = "HORA-NOW" Or rango = "DIA")) Then
            For Each item In listaAccesos.Accesos
                item.horaAccesoActividad = DateAdd(DateInterval.Minute, -timeoffset, item.horaAccesoActividad).ToShortTimeString()
            Next
        End If
        If Not listaAccesos.Accesos Is Nothing Then
            For i As Integer = 1 To listaAccesos.Accesos.Count
                Dim dHoraAccesoActividad As DateTime
                Select Case rango
                    Case "HORA-NOW", "HORA", "DIA"
                        dHoraAccesoActividad = New DateTime(dFechaInicioOrig.Year, dFechaInicioOrig.Month, dFechaInicioOrig.Day, CDate(listaAccesos.Accesos(i).horaAccesoActividad).Hour, CDate(listaAccesos.Accesos(i).horaAccesoActividad).Minute, 0)
                    Case "SEMANA", "MES"
                        dHoraAccesoActividad = dFechaInicioOrig.AddDays((i - 1))
                    Case "ANYO"
                        dHoraAccesoActividad = New DateTime(fechaInicio.Year, i, 1, 0, 0, 0)
                End Select
                If dHoraAccesoActividad > Now() Then
                    listaAccesos.Accesos(i).numUsuariosActividad = -2
                    listaAccesos.Accesos(i).avgUsuariosActividad = -2
                End If
            Next i
        End If
        Return listaAccesos.Accesos
    End Function


    'Devuelve los datos relativos a los errores que sirven para cargar el grid de errores
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function ErroresActividad(fechaInicio As DateTime, fechaFin As DateTime, entorno As String, pyme As String, ByVal timeoffset As Double)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim listaErrores As FSNServer.ErroresVisorActividades = FSNServer.Get_Object(GetType(Fullstep.FSNServer.ErroresVisorActividades))
        fechaInicio = DateAdd(DateInterval.Minute, timeoffset, fechaInicio)
        fechaFin = DateAdd(DateInterval.Minute, timeoffset, fechaFin)
        listaErrores.Load(fechaInicio, fechaFin, entorno, pyme)
        If Not listaErrores.Errores Is Nothing Then
            For Each item In listaErrores.Errores
                item.fecAct = DateAdd(DateInterval.Minute, -timeoffset, item.fecAct)
                item.fecAct = item.fecAct.ToString()
            Next
        End If
        Return listaErrores.Errores
    End Function


    'Devuelve los datos relativos a los eventos que sirven para cargar el grid de eventos
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function EventosActividad(fechaInicio As DateTime, fechaFin As DateTime, entorno As String, pyme As String, tipoEvento As String, rango As String, ByVal timeoffset As Double)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim listaEventos As FSNServer.EventosVisorActividades = FSNServer.Get_Object(GetType(Fullstep.FSNServer.EventosVisorActividades))
        Dim fechaDesde, fechaHasta As DateTime

        If (rango = "HORA" Or rango = "HORA-NOW") Then
            fechaDesde = New DateTime(fechaInicio.Year, fechaInicio.Month, fechaInicio.Day, fechaInicio.Hour, fechaInicio.Minute, 0)
            fechaHasta = New DateTime(fechaFin.Year, fechaFin.Month, fechaFin.Day, fechaFin.Hour, fechaFin.Minute, 0)
        Else
            fechaDesde = New DateTime(fechaInicio.Year, fechaInicio.Month, fechaInicio.Day, fechaInicio.Hour, fechaInicio.Minute, fechaInicio.Second)
            fechaHasta = New DateTime(fechaFin.Year, fechaFin.Month, fechaFin.Day, fechaFin.Hour, fechaFin.Minute, fechaFin.Second)
        End If

        If (rango = "HORA" Or rango = "HORA-NOW" Or rango = "DIA") Then
            fechaDesde = DateAdd(DateInterval.Minute, timeoffset, fechaDesde)
            fechaHasta = DateAdd(DateInterval.Minute, timeoffset, fechaHasta)
        End If
        listaEventos.Load(fechaDesde, fechaHasta, entorno, pyme, tipoEvento, rango)
        If (Not listaEventos.Eventos Is Nothing And (rango = "HORA" Or rango = "HORA-NOW" Or rango = "DIA")) Then
            For Each item In listaEventos.Eventos
                item.fechaEvento = DateAdd(DateInterval.Minute, -timeoffset, item.fechaEvento).ToString()
            Next
        End If
        Return listaEventos.Eventos
    End Function

    'Carga del combo de pymes para cuando seleccionen "pymes" en el combo de entornos
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function CargarPymes(entorno As String)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim listaPymes As Fullstep.FSNServer.Pymes = FSNServer.Get_Object(GetType(Fullstep.FSNServer.Pymes))
        listaPymes.Load(entorno, Usuario.Idioma)
        Return listaPymes.Pymes
    End Function

#End Region

End Class