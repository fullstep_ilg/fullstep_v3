﻿<%@ Page  Debug="true" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master" CodeBehind="Cubos.aspx.vb" Inherits="Fullstep.FSNWeb.Cubos"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
<link rel="stylesheet" href="css/jquery-ui.css" />

 <script type="text/javascript">
     $(function () {
         $("#tabs").tabs();
     });
</script>
<script type="text/javascript">
		var SelectedRows=[];
		var SelectedRowCount=0;
		$(document).ready(function () {

		    $('[id$=ImgBtnFirst]').live('click', function () { FirstPage(); });
		    $('[id$=ImgBtnPrev]').live('click', function () { PrevPage(); });
		    $('[id$=ImgBtnNext]').live('click', function () { NextPage(); });
		    $('[id$=ImgBtnLast]').live('click', function () { LastPage(); });

		    $.get(rutaFS + 'BI/html/permisosCubos.htm', function (paraOpciones) {
		        paraOpciones = paraOpciones.replace(/src="/gi, 'src="' + ruta);
		        $('#divPermisos').append(paraOpciones);
		    })
		    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(LoadCache);
		    $('[id$=imgCerrar]').live('click', function () {
		        CancelarNuevoCubo();
		        return false;
		    });
		    $('[id$=imgCerrarPermisos]').live('click', function () {
		        CancelarPermisoModalClick();
		        return false;
		    });
            //Comprobacion de cuando se pueden checkear los checkbox de las medidas disponibles y predeterminadas.
		    $('[id$=chkPresupuestoAbierto2]').live('click', function () {
		        if ($('[id$=chkPresupuestoAbierto]').is(':checked')) {
		            if ($('[id$=chkPresupuestoAbierto2]').is(':checked')) {
		                $('[id$=chkPresupuestoAbierto2]').attr('checked', false);
		            } else {
		                $('[id$=chkPresupuestoAbierto2]').attr('checked', true);
		            }
		        } else {
		            $('[id$=chkPresupuestoAbierto2]').attr('checked', false);
		        }
		    });
		    $('[id$=chkPresupuestoAbierto2]').click( function () {
		        if ($('[id$=chkPresupuestoAbierto]').is(':checked')) {
		            if ($('[id$=chkPresupuestoAbierto2]').is(':checked')) {
		                $('[id$=chkPresupuestoAbierto2]').attr('checked', false);
		            } else {
		                $('[id$=chkPresupuestoAbierto2]').attr('checked', true);
		            }
		        } else {
		            $('[id$=chkPresupuestoAbierto2]').attr('checked', false);
		        }
		    });
		    $('[id$=chkPresupuestoConsumido2]').live('click', function () {
		        if ($('[id$=chkPresupuestoConsumido]').is(':checked')) {
		            if ($('[id$=chkPresupuestoConsumido2]').is(':checked')) {
		                $('[id$=chkPresupuestoConsumido2]').attr('checked', false);
		            } else {
		                $('[id$=chkPresupuestoConsumido2]').attr('checked', true);
		            }
		        } else {
		            $('[id$=chkPresupuestoConsumido2]').attr('checked', false);
		        }
		    });
		    $('[id$=chkPresupuestoConsumido2]').click(function () {
		        if ($('[id$=chkPresupuestoConsumido]').is(':checked')) {
		            if ($('[id$=chkPresupuestoConsumido2]').is(':checked')) {
		                $('[id$=chkPresupuestoConsumido2]').attr('checked', false);
		            } else {
		                $('[id$=chkPresupuestoConsumido2]').attr('checked', true);
		            }
		        } else {
		            $('[id$=chkPresupuestoConsumido2]').attr('checked', false);
		        }
		    });
		    $('[id$=chkImporteAdjudicacion2]').live('click', function () {
		        if ($('[id$=chkImporteAdjudicacion]').is(':checked')) {
		            if ($('[id$=chkImporteAdjudicacion2]').is(':checked')) {
		                $('[id$=chkImporteAdjudicacion2]').attr('checked', false);
		            } else {
		                $('[id$=chkImporteAdjudicacion2]').attr('checked', true);
		            }
		        } else {
		            $('[id$=chkImporteAdjudicacion2]').attr('checked', false);
		        }
		    });
		    $('[id$=chkImporteAdjudicacion2]').click(function () {
		        if ($('[id$=chkImporteAdjudicacion]').is(':checked')) {
		            if ($('[id$=chkImporteAdjudicacion2]').is(':checked')) {
		                $('[id$=chkImporteAdjudicacion2]').attr('checked', false);
		            } else {
		                $('[id$=chkImporteAdjudicacion2]').attr('checked', true);
		            }
		        } else {
		            $('[id$=chkImporteAdjudicacion2]').attr('checked', false);
		        }
		    });
		    $('[id$=chkAhorroAdjudicacion2]').live('click', function () {
		        if ($('[id$=chkAhorroAdjudicacion]').is(':checked')) {
		            if ($('[id$=chkAhorroAdjudicacion2]').is(':checked')) {
		                $('[id$=chkAhorroAdjudicacion2]').attr('checked', false);
		            } else {
		                $('[id$=chkAhorroAdjudicacion2]').attr('checked', true);
		            }
		        } else {
		            $('[id$=chkAhorroAdjudicacion2]').attr('checked', false);
		        }
		    });
		    $('[id$=chkAhorroAdjudicacion2]').click(function () {
		        if ($('[id$=chkAhorroAdjudicacion]').is(':checked')) {
		            if ($('[id$=chkAhorroAdjudicacion2]').is(':checked')) {
		                $('[id$=chkAhorroAdjudicacion2]').attr('checked', false);
		            } else {
		                $('[id$=chkAhorroAdjudicacion2]').attr('checked', true);
		            }
		        } else {
		            $('[id$=chkAhorroAdjudicacion2]').attr('checked', false);
		        }
		    });
		    $('[id$=chkImportePedido2]').live('click', function () {
		        if ($('[id$=chkImportePedido]').is(':checked')) {
		            if ($('[id$=chkImportePedido2]').is(':checked')) {
		                $('[id$=chkImportePedido2]').attr('checked', false);
		            } else {
		                $('[id$=chkImportePedido2]').attr('checked', true);
		            }
		        } else {
		            $('[id$=chkImportePedido2]').attr('checked', false);
		        }
		    });
		    $('[id$=chkImportePedido2]').click(function () {
		        if ($('[id$=chkImportePedido]').is(':checked')) {
		            if ($('[id$=chkImportePedido2]').is(':checked')) {
		                $('[id$=chkImportePedido2]').attr('checked', false);
		            } else {
		                $('[id$=chkImportePedido2]').attr('checked', true);
		            }
		        } else {
		            $('[id$=chkImportePedido2]').attr('checked', false);
		        }
		    });
		    $('[id$=chkImporteRecepcion2]').live('click', function () {
		        if ($('[id$=chkImporteRecepcion]').is(':checked')) {
		            if ($('[id$=chkImporteRecepcion2]').is(':checked')) {
		                $('[id$=chkImporteRecepcion2]').attr('checked', false);
		            } else {
		                $('[id$=chkImporteRecepcion2]').attr('checked', true);
		            }
		        } else {
		            $('[id$=chkImporteRecepcion2]').attr('checked', false);
		        }
		    });
		    $('[id$=chkImporteRecepcion2]').click(function () {
		        if ($('[id$=chkImporteRecepcion]').is(':checked')) {
		            if ($('[id$=chkImporteRecepcion2]').is(':checked')) {
		                $('[id$=chkImporteRecepcion2]').attr('checked', false);
		            } else {
		                $('[id$=chkImporteRecepcion2]').attr('checked', true);
		            }
		        } else {
		            $('[id$=chkImporteRecepcion2]').attr('checked', false);
		        }
		    });
		    $('[id$=chkAhorroRecepcion2]').live('click', function () {
		        if ($('[id$=chkAhorroRecepcion]').is(':checked')) {
		            if ($('[id$=chkAhorroRecepcion2]').is(':checked')) {
		                $('[id$=chkAhorroRecepcion2]').attr('checked', false);
		            } else {
		                $('[id$=chkAhorroRecepcion2]').attr('checked', true);
		            }
		        } else {
		            $('[id$=chkAhorroRecepcion2]').attr('checked', false);
		        }
		    });
		    $('[id$=chkAhorroRecepcion2]').click(function () {
		        if ($('[id$=chkAhorroRecepcion]').is(':checked')) {
		            if ($('[id$=chkAhorroRecepcion2]').is(':checked')) {
		                $('[id$=chkAhorroRecepcion2]').attr('checked', false);
		            } else {
		                $('[id$=chkAhorroRecepcion2]').attr('checked', true);
		            }
		        } else {
		            $('[id$=chkAhorroRecepcion2]').attr('checked', false);
		        }
		    });
		    $('[id$=chkCopiarPermisos]').live('click', function () {
		        if ($('[id$=chkCopiarPermisos]').is(':checked')) {
		            $('[id$=ddlCopiarPermisos]').css('disabled', false);
		            
		        } else {
		            $('[id$=ddlCopiarPermisos]').prop('disabled', true);
		           
		        }
		    });
		    $('[id$=chkCopiarPermisos]').click(function () {
		        if ($('[id$=chkCopiarPermisos]').is(':checked')) {
		            $('[id$=ddlCopiarPermisos]').prop('disabled', false);
		            
		        } else {
		            $('[id$=ddlCopiarPermisos]').prop('disabled', true);
		          
		        }
		    });  

		});
		
        function LoadCache(sender, args) {
            if ($('[id$=hCacheLoaded]').val() == 0) {
                var btnRecargaCache = $('[id$=btnRecargaCache]').attr('id');
                __doPostBack(btnRecargaCache, '');
            }
        }
       
        function IndexChanged(){
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var btnPager = $('[id$=btnPager]').attr('id');
            var selectedIndex = dropdownlist.selectedIndex;
            __doPostBack(btnPager,dropdownlist.selectedIndex);
        }
        function FirstPage(){
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var btnPager = $('[id$=btnPager]').attr('id');
            dropdownlist.options[0].selected = true;
            __doPostBack(btnPager,0);
        }
        function PrevPage(){
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var btnPager = $('[id$=btnPager]').attr('id');
            var selectedIndex = dropdownlist.selectedIndex;
            if(selectedIndex-1>=0){
                dropdownlist.options[selectedIndex-1].selected = true;
                __doPostBack(btnPager,dropdownlist.selectedIndex);
            }
        }
        function NextPage(){
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var btnPager = $('[id$=btnPager]').attr('id');
            var selectedIndex = dropdownlist.selectedIndex;
            if(selectedIndex+1<=dropdownlist.length-1){
                dropdownlist.options[selectedIndex+1].selected = true;
                __doPostBack(btnPager,dropdownlist.selectedIndex);
            }                 
        }
        function LastPage(){
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var btnPager = $('[id$=btnPager]').attr('id');
            dropdownlist.options[dropdownlist.length-1].selected = true;
            __doPostBack(btnPager,dropdownlist.length-1);
        }
        function MostrarCuboVacio() {
            $('#popupFondo').css('height', $(document).height());
            $('#popupFondo').show();
            $('[id$=lblNuevoCubo]').text(TextosCubos[2]);                   //Nuevo Cubo
            $('[id$=lblNombre]').text(TextosCubos[6]);                      //Nombre: 
            $('[id$=lblActivo]').text(TextosCubos[7]);                      //Activo: 
            $('[id$=lblUrl]').text(TextosCubos[8]);                         //Url: 
            $('[id$=lblUsuario]').text(TextosCubos[9]);                     //Usuario: 
            $('[id$=lblPassword]').text(TextosCubos[10]);                   //Password: 
            $('[id$=lblDominio]').text(TextosCubos[11]);                    //Dominio: 
            $('[id$=lblUrl2]').text(TextosCubos[8]);                        //Url: 
            $('[id$=lblUsuario2]').text(TextosCubos[9]);                    //Usuario: 
            $('[id$=lblPassword2]').text(TextosCubos[10]);                  //Password: 
            $('[id$=lblDominio2]').text(TextosCubos[11]);                   //Dominio: 
            $('[id$=lblBD]').text(TextosCubos[12]);                         //BD: 
            $('[id$=lblCubo]').text(TextosCubos[13]);                       //Cubo: 
            $('[id$=lblServicioDatos]').text(TextosCubos[14]);              //Servicio de Datos
            $('[id$=lblAnalysisService]').text(TextosCubos[15]);            //Analysis Service

            $('[id$=lblMedidasDisponibles]').text(TextosCubos[16]);         //Medidas Disponibles
            $('[id$=lblPresupuestoAbierto]').text(TextosCubos[17]);         //Presupuesto abierto
            $('[id$=lblPresupuestoConsumido]').text(TextosCubos[18]);       //Presupuesto consumido
            $('[id$=lblImporteAdjudicacion]').text(TextosCubos[19]);        //Importe adjudicacion
            $('[id$=lblAhorroAdjudicacion]').text(TextosCubos[20]);         //Ahorro adjudicacion
            $('[id$=lblImportePedido]').text(TextosCubos[21]);              //Importe pedido
            $('[id$=lblImporteRecepcion]').text(TextosCubos[22]);           //Importe recepcion
            $('[id$=lblAhorroRecepcion]').text(TextosCubos[23]);            //Ahorro recepcion

            $('[id$=lblMedidasPredeterminadas]').text(TextosCubos[29]);     //Medidas Predeterminadas
            $('[id$=lblPresupuestoAbierto2]').text(TextosCubos[17]);        //Presupuesto abierto
            $('[id$=lblPresupuestoConsumido2]').text(TextosCubos[18]);      //Presupuesto consumido
            $('[id$=lblImporteAdjudicacion2]').text(TextosCubos[19]);       //Importe adjudicacion  
            $('[id$=lblAhorroAdjudicacion2]').text(TextosCubos[20]);        //Ahorro adjudicacion
            $('[id$=lblImportePedido2]').text(TextosCubos[21]);             //Importe pedido
            $('[id$=lblImporteRecepcion2]').text(TextosCubos[22]);          //Importe recepcion
            $('[id$=lblAhorroRecepcion2]').text(TextosCubos[23]);           //Ahorro recepcion

        }
        function NuevoCubo() {
            MostrarCuboVacio();

            $('[id$=chkPresupuestoAbierto]').attr('checked', 'checked');
            $('[id$=chkPresupuestoConsumido]').attr('checked', 'checked');
            $('[id$=chkImporteAdjudicacion]').attr('checked', 'checked');
            $('[id$=chkAhorroAdjudicacion]').attr('checked', 'checked');
            $('[id$=chkImportePedido]').attr('checked', 'checked');
            $('[id$=chkImporteRecepcion]').attr('checked', 'checked');
            $('[id$=chkAhorroRecepcion]').attr('checked', 'checked');

            $('[id$=chkPresupuestoAbierto2]').attr('checked', 'checked');
            $('[id$=chkPresupuestoConsumido2]').attr('checked', false);
            $('[id$=chkImporteAdjudicacion2]').attr('checked', false);
            $('[id$=chkAhorroAdjudicacion2]').attr('checked', false);
            $('[id$=chkImportePedido2]').attr('checked', false);
            $('[id$=chkImporteRecepcion2]').attr('checked', false);
            $('[id$=chkAhorroRecepcion2]').attr('checked', false);
            $("input[type='password']").val('');
            $('[id$=btnAceptar]').css("display", "inherit");
            $('[id$=btnAceptarConfigurar]').css("display","none");
            var punto = window.center({ width: $('#pnlCubos').width(), height: $('#pnlCubos').height() });
            $('#pnlCubos').css('top', punto.y + 'px');
            $('#pnlCubos').css('left', punto.x + 'px');
            $('#pnlCubos').show();
           
        }
        function InsertarCubo() {
            if (ComprobarDenominacionInsertada()) {
                AceptarNuevoCubo()
            }
        }
        function ComprobarDenominacionInsertada() {
            var correcto = false;
            var denominaciones = $('#txtNombre').textBoxMultiIdioma('getItems');

            if (!(denominaciones["ENG"] == " " & denominaciones["SPA"] == " " & denominaciones["GER"] == " " & denominaciones["FRA"] == " ")) {
                correcto = true;
                return correcto;
            } else {
                alert(TextosCubos[24]);                                     //Se debe insertar al menos la denominación en un idioma
                correct = false;
                return correcto;
            }
        }
        function ComprobarCampos() {
            var denominaciones = $('#txtNombre').textBoxMultiIdioma('getItems');
            var url = $('[id$=txtUrl]').val();
            var usuario = $('[id$=txtUsuario]').val();
            var password = $('[id$=txtPassword]').val();
            var dominio = $('[id$=txtDominio]').val();
            var url2 = $('[id$=txtUrl2]').val();
            var usuario2 = $('[id$=txtUsuario2]').val();
            var password2 = $('[id$=txtPassword2]').val();
            var dominio2 = $('[id$=txtDominio2]').val();
            var BD = $('[id$=txtBD]').val();
            var cubo = $('[id$=txtCubo]').val();
            var mensajeError=TextosCubos[34]        //Se deben rellenar los siguiente campos: 
            if (denominaciones["ENG"] == " " && denominaciones["SPA"] == " " && denominaciones["GER"] == " " && denominaciones["FRA"] == " ")
            {
                mensajeError+=TextosCubos[35]       //Nombre del cubo, 
            }
            if (url =="") 
            {
                mensajeError+=TextosCubos[36]       //Url del Servicio Datos, 
            }
            if(usuario == "")
            {
                mensajeError += TextosCubos[37]     //Usuario del Servicio Datos, 
            }
            if(password == "")
            {
                mensajeError += TextosCubos[38]     //Password del Servicio Datos, 
            }
            if(dominio == "")
            {
                mensajeError += TextosCubos[39]     //Dominio del Servicio Datos, 
            }
            if(url2=="")
            {
                mensajeError+= TextosCubos[40]      //URL del Analysis Service, 
            }
            if(usuario2=="")
            {
                mensajeError += TextosCubos[41]     //Usuario del Analysis Service, 
            }
            if(password2=="")
            {
                mensajeError += TextosCubos[42]     //Password del Analysis Service, 
            }
            if(dominio2=="")
            {
                mensajeError += TextosCubos[43]     //Dominio del Analysis Service, 
            }
            if(BD=="")
            {
                mensajeError += TextosCubos[44]     //BD del Analysis Service, 
            }
            if(cubo=="")
            {
                mensajeError += TextosCubos[45]     //Cubo del Analysis Service, 
            }
            if (mensajeError != TextosCubos[34])
            {
                var mensaje=mensajeError.substring(0, mensajeError.length-2);
                alert(mensaje);
                return false;
            }else
            {
                return true;
            }
        }
        function ComprobarCuboCorrecto(url2, usuario2, password2, dominio2, BD, cubo) 
        {
            var to = rutaFS + 'BI/Cubos.aspx/ComprobarCuboCorrecto';
            var confirmacion=false;
            $.when($.ajax({
                type: 'POST',
                url: to,
                data: JSON.stringify({ url2: url2, usuario2: usuario2, password2: password2, dominio2: dominio2, BD: BD, cubo: cubo }),
                contentType: 'application/json;charset=utf-8',
                dataType: 'json',
                async: false
            })).done(function (msg) {
                if (msg.d != "success") {
                    confirmacion = confirm(TextosCubos[46]);        // No se ha podido conectar al cubo ¿Desea mantener el cubo a pesar que no esté disponible?                  
                } else {
                    confirmacion = true;
                }
            });
            return confirmacion;
        }

        function ComprobarServicioCorrecto(url) {
            var to = rutaFS + 'BI/Cubos.aspx/isSiteOnline';
            var confirmacion = false;
            $.when($.ajax({
                type: 'POST',
                url: to,
                data: JSON.stringify({ url: url}),
                contentType: 'application/json;charset=utf-8',
                dataType: 'json',
                async: false
            })).done(function (msg) {
                if (msg.d != "success") {
                    confirmacion = confirm(TextosCubos[47]);                         // El servicio al que intenta acceder no está activo. Desea mantener el cubo a pesar de que el servicio no esté activo?
                } else {
                    confirmacion = true;
                }
            });
            return confirmacion;
        }

        function AceptarNuevoCubo() 
        {
            var denominaciones = $('#txtNombre').textBoxMultiIdioma('getItems'); 
            var url =$.trim($('[id$=txtUrl]').val());
            var usuario =$.trim( $('[id$=txtUsuario]').val());
            var password =$.trim( $('[id$=txtPassword]').val());
            var dominio =$.trim( $('[id$=txtDominio]').val());
            var url2 =$.trim( $('[id$=txtUrl2]').val());
            var usuario2 =$.trim( $('[id$=txtUsuario2]').val());
            var password2 =$.trim( $('[id$=txtPassword2]').val());
            var dominio2 =$.trim( $('[id$=txtDominio2]').val());
            var BD =$.trim( $('[id$=txtBD]').val());
            var cubo =$.trim( $('[id$=txtCubo]').val());
            var activo =$('[id$=chkActivo]').is(":checked");
            if (ComprobarCampos()) {
                if (ComprobarCuboCorrecto(url2, usuario2, password2, dominio2, BD, cubo)) {
                    if (ComprobarServicioCorrecto(url)) {
                        var presupuestoAbierto = $('[id$=chkPresupuestoAbierto]').is(":checked");
                        var presupuestoConsumido = $('[id$=chkPresupuestoConsumido]').is(":checked");
                        var importeAdjudicacion = $('[id$=chkImporteAdjudicacion]').is(":checked");
                        var ahorroAdjudicacion = $('[id$=chkAhorroAdjudicacion]').is(":checked");
                        var importePedido = $('[id$=chkImportePedido]').is(":checked");
                        var importeRecepcion = $('[id$=chkImporteRecepcion]').is(":checked");
                        var ahorroRecepcion = $('[id$=chkAhorroRecepcion]').is(":checked");
                        var presupuestoAbierto2 = $('[id$=chkPresupuestoAbierto2]').is(":checked");
                        var presupuestoConsumido2 = $('[id$=chkPresupuestoConsumido2]').is(":checked");
                        var importeAdjudicacion2 = $('[id$=chkImporteAdjudicacion2]').is(":checked");
                        var ahorroAdjudicacion2 = $('[id$=chkAhorroAdjudicacion2]').is(":checked");
                        var importePedido2 = $('[id$=chkImportePedido2]').is(":checked");
                        var importeRecepcion2 = $('[id$=chkImporteRecepcion2]').is(":checked");
                        var ahorroRecepcion2 = $('[id$=chkAhorroRecepcion2]').is(":checked");

                        var availableMeasures = "";
                        if (presupuestoAbierto) { availableMeasures = availableMeasures + TextosCubos[17] + "|" }           //Presupuesto abierto
                        if (presupuestoConsumido) { availableMeasures = availableMeasures + TextosCubos[18] + "|" }         //Presupuesto consumido
                        if (importeAdjudicacion) { availableMeasures = availableMeasures + TextosCubos[19] + "|" }          //Importe adjudicación
                        if (ahorroAdjudicacion) { availableMeasures = availableMeasures + TextosCubos[20] + "|" }           //Ahorro adjudicación
                        if (importePedido) { availableMeasures = availableMeasures + TextosCubos[21] + "|" }                //Importe pedido
                        if (importeRecepcion) { availableMeasures = availableMeasures + TextosCubos[22] + "|" }             //Importe recepción
                        if (ahorroRecepcion) { availableMeasures = availableMeasures + TextosCubos[23] + "|" }              //Ahorro recepción

                        var defaultMeasures = "";
                        if (presupuestoAbierto2) { defaultMeasures = defaultMeasures + TextosCubos[17] + "|" }              //Presupuesto abierto
                        if (presupuestoConsumido2) { defaultMeasures = defaultMeasures + TextosCubos[18] + "|" }            //Presupuesto consumido
                        if (importeAdjudicacion2) { defaultMeasures = defaultMeasures + TextosCubos[19] + "|" }             //Importe adjudicación
                        if (ahorroAdjudicacion2) { defaultMeasures = defaultMeasures + TextosCubos[20] + "|" }              //Ahorro adjudicación
                        if (importePedido2) { defaultMeasures = defaultMeasures + TextosCubos[21] + "|" }                   //Importe pedido
                        if (importeRecepcion2) { defaultMeasures = defaultMeasures + TextosCubos[22] + "|" }                //Importe recepción
                        if (ahorroRecepcion2) { defaultMeasures = defaultMeasures + TextosCubos[23] + "|" }                 //Ahorro recepción

                        var to = rutaFS + 'BI/Cubos.aspx/InsertarCubo';
                        $.when($.ajax({
                            type: 'POST',
                            url: to,
                            data: JSON.stringify({ denominaciones: denominaciones, url: url, usuario: usuario, password: password, dominio: dominio, url2: url2, usuario2: usuario2, password2: password2, dominio2: dominio2, BD: BD, cubo: cubo, activo: activo, availableMeasures: availableMeasures, defaultMeasures: defaultMeasures }),
                            contentType: 'application/json;charset=utf-8',
                            dataType: 'json',
                            async: true
                        })).done(function (msg) {
                            var btnUpdateGrid = $('[id$=btnUpdateGrid]').attr('id');
                            __doPostBack(btnUpdateGrid, '');
                            LimpiarModal();
                            $('#popupFondo').hide();
                        });
                    }                 
                }
            }
        }
        function LimpiarModal() {
            $('[id$=chkActivo]').attr('checked', false);
            $('[id$=txtUrl]').val(" ");
            $('[id$=txtUsuario]').val(" ");
            $('[id$=txtPassword]').val(" ");
            $('[id$=txtDominio]').val(" ");
            $('[id$=txtUrl2]').val(" ");
            $('[id$=txtUsuario2]').val(" ");
            $('[id$=txtPassword2]').val(" ");
            $('[id$=txtDominio2]').val(" ");
            $('[id$=txtBD]').val(" ");
            $('[id$=txtCubo]').val(" ");
            $('[id$=chkPresupuestoAbierto2]').attr('checked', false);
            $('[id$=chkPresupuestoConsumido2]').attr('checked', false);
            $('[id$=chkImporteAdjudicacion2]').attr('checked', false);
            $('[id$=chkAhorroAdjudicacion2]').attr('checked', false);
            $('[id$=chkImportePedido2]').attr('checked', false);
            $('[id$=chkImporteRecepcion2]').attr('checked', false);
            $('[id$=chkAhorroRecepcion2]').attr('checked', false);
            var denominaciones = $('#txtNombre').textBoxMultiIdioma('getItems');
            denominaciones["ENG"] = ""
            denominaciones["SPA"] = ""
            denominaciones["GER"] = ""
            denominaciones["FRA"] = ""
            $('#txtNombre').textBoxMultiIdioma('setItems', denominaciones);
            $('#pnlCubos').hide();
        }
        function CancelarNuevoCubo() 
        {
            LimpiarModal();
            $('#popupFondo').hide();
        }
        function ComprobarUnaFilaSeleccionada() {
                var grid = $find($('[id$=whgCubos]').attr('id')).get_gridView();
                var selectedRows = grid.get_behaviors().get_selection().get_selectedRows();
                if (selectedRows.get_length() != 1) {
                    alert(TextosCubos[25]);                 //se debe seleccionar una fila
                    return false;
                }
                return true;
            }
       function EliminarClick() {
            ComprobarUnaFilaSeleccionada()
            var statusConfirm = confirm(TextosCubos[26]);   //¿Desea eliminar el cubo seleccionado?
            if(statusConfirm){
                EliminarCubo();
            }
        }
        function EliminarCubo() {
            var grid = $find($('[id$=whgCubos]').attr('id')).get_gridView();
            var selectedRows = grid.get_behaviors().get_selection().get_selectedRows();
            var idCubo = selectedRows.getItem(0).get_cellByColumnKey("ID").get_value();
            var url = rutaFS + 'BI/Cubos.aspx/EliminarCubo';
            $.when($.ajax({
                type: 'POST',
                url: url,
                data: JSON.stringify({ idCubo: idCubo }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                async: true
            }).done(function (msg) {
                $('[id$=btnAceptar]').css("display", "");
                $('[id$=btnAceptarConfigurar]').css("display", "none");
                LimpiarModal();
                $('#popupFondo').hide();
                var btnUpdateGrid = $('[id$=btnUpdateGrid]').attr('id');
                __doPostBack(btnUpdateGrid, '');
            }));
                       
        }
        function ObtenerDatosCubo() {
            var grid = $find($('[id$=whgCubos]').attr('id')).get_gridView();
            var selectedRows = grid.get_behaviors().get_selection().get_selectedRows();
            if (selectedRows.get_length() != 1) {
                alert(TextosCubos[25]);                     //se debe seleccionar una fila
                return false;
            }
            var idCubo = selectedRows.getItem(0).get_cellByColumnKey("ID").get_value();
            /*Obtenemos en formato string todos los datos que mostraremos en la pantalla*/
            $.when($.ajax({
                type: 'POST',
                url: rutaFS + 'BI/Cubos.aspx/Obtener_Datos_Cubo',
                data: JSON.stringify({ idCubo: idCubo }),
                contentType: 'application/json;',
                async: true
            })).done(function (msg) {
                MostrarCuboVacio();
                $('[id$=lblNuevoCubo]').text(TextosCubos[3]);    //Configurar Cubo
                /*Parseamos el string a JSON para facilitar el manejo de datos*/
                oCubo = $.parseJSON(msg.d);
                $('[id$=txtUrl]').val(oCubo.Svc_Url);
                $('[id$=txtUsuario]').val(oCubo.Svc_Usu);
                $('[id$=txtPassword]').val(oCubo.Svc_Pwd);
                $('[id$=txtDominio]').val(oCubo.Svc_Dom);
                $('[id$=txtUrl2]').val(oCubo.Ssas_Server);
                $('[id$=txtUsuario2]').val(oCubo.Ssas_Usu);
                $('[id$=txtPassword2]').val(oCubo.Ssas_Pwd);
                $('[id$=txtDominio2]').val(oCubo.Ssas_Dom);
                $('[id$=txtBD]').val(oCubo.Ssas_BD);
                $('[id$=txtCubo]').val(oCubo.Ssas_Cube);
                if (oCubo.Activo) {
                    $('[id$=chkActivo]').attr('checked', 'checked');
                } else {
                    $('[id$=chkActivo]').attr('checked', false);
                }
                var s = oCubo.Available_Measures.split("|");

                $('[id$=chkPresupuestoAbierto]').attr('checked', false);
                $('[id$=chkPresupuestoConsumido]').attr('checked', false);
                $('[id$=chkImporteAdjudicacion]').attr('checked', false);
                $('[id$=chkAhorroAdjudicacion]').attr('checked', false);
                $('[id$=chkImportePedido]').attr('checked', false);
                $('[id$=chkImporteRecepcion]').attr('checked', false);
                $('[id$=chkAhorroRecepcion]').attr('checked', false);
                $('[id$=chkPresupuestoAbierto2]').attr('checked', false);
                $('[id$=chkPresupuestoConsumido2]').attr('checked', false);
                $('[id$=chkImporteAdjudicacion2]').attr('checked', false);
                $('[id$=chkAhorroAdjudicacion2]').attr('checked', false);
                $('[id$=chkImportePedido2]').attr('checked', false);
                $('[id$=chkImporteRecepcion2]').attr('checked', false);
                $('[id$=chkAhorroRecepcion2]').attr('checked', false);

                for (var i = 0; i < s.length; i++) {
                    if (s[i] == (TextosCubos[17])) {                //Presupuesto abierto
                        $('[id$=chkPresupuestoAbierto]').attr('checked', 'checked');
                    }
                    if (s[i] == (TextosCubos[18])) {                //Presupuesto consumido
                        $('[id$=chkPresupuestoConsumido]').attr('checked', 'checked');
                    }
                    if (s[i] == (TextosCubos[19])) {                //Importe adjudicación
                        $('[id$=chkImporteAdjudicacion]').attr('checked', 'checked');
                    }
                    if (s[i] == (TextosCubos[20])) {                //Ahorro adjudicación
                        $('[id$=chkAhorroAdjudicacion]').attr('checked', 'checked');
                    }
                    if (s[i] == (TextosCubos[21])) {                //Importe pedido
                        $('[id$=chkImportePedido]').attr('checked', 'checked');
                    }
                    if (s[i] == (TextosCubos[22])) {                //Importe recepción
                        $('[id$=chkImporteRecepcion]').attr('checked', 'checked');
                    }
                    if (s[i] == (TextosCubos[23])) {                //Ahorro recepción
                        $('[id$=chkAhorroRecepcion]').attr('checked', 'checked');
                    }
                }
                s = oCubo.Default_Measures.split("|");
                for (var i = 0; i < s.length; i++) {
                    if (s[i] == (TextosCubos[17])) {                //Presupuesto abierto
                        $('[id$=chkPresupuestoAbierto2]').attr('checked', 'checked');
                    }
                    if (s[i] == (TextosCubos[18])) {                //Presupuesto consumido
                        $('[id$=chkPresupuestoConsumido2]').attr('checked', 'checked');
                    }
                    if (s[i] == (TextosCubos[19])) {                //Importe adjudicación
                        $('[id$=chkImporteAdjudicacion2]').attr('checked', 'checked');
                    }
                    if (s[i] == (TextosCubos[20])) {                //Ahorro adjudicación
                        $('[id$=chkAhorroAdjudicacion2]').attr('checked', 'checked');
                    }
                    if (s[i] == (TextosCubos[21])) {                //Importe pedido
                        $('[id$=chkImportePedido2]').attr('checked', 'checked');
                    }
                    if (s[i] == (TextosCubos[22])) {                //Importe recepción
                        $('[id$=chkImporteRecepcion2]').attr('checked', 'checked');
                    }
                    if (s[i] == (TextosCubos[23])) {                //Ahorro recepción
                        $('[id$=chkAhorroRecepcion2]').attr('checked', 'checked');
                    }
                }
                //Obtenemos los datos de la denominacion del cubo para mostrar por pantalla
                $.when($.ajax({
                    type: 'POST',
                    url: rutaFS + 'BI/Cubos.aspx/Obtener_Datos_Cubo_Den',
                    data: JSON.stringify({ idCubo: idCubo }),
                    contentType: 'application/json;',
                    async: true
                })).done(function (msg) {
                    oCuboDenoninacion = $.parseJSON(msg.d);
                    $('#txtNombre').textBoxMultiIdioma('setItems', oCuboDenoninacion);
                });
                $('[id$=btnAceptar]').css("display", "none");
                $('[id$=btnAceptarConfigurar]').css("display", "inherit");
                var punto = window.center({ width: $('#pnlCubos').width(), height: $('#pnlCubos').height() });
                $('#pnlCubos').css('top', punto.y + 'px');
                $('#pnlCubos').css('left', punto.x + 'px');
                $('#pnlCubos').show();
            });
        }
        function AceptarConfigurarCubo() {
            var correcto = false;
            correcto=ComprobarUnaFilaSeleccionada();
            if (correcto){
            correcto=ComprobarDenominacionInsertada();
            }
            if (correcto) {
                ConfigurarCubo();                    
            }
        }
        function ConfigurarCubo() {
            if (ComprobarCampos()) {
               
                var denominaciones = $('#txtNombre').textBoxMultiIdioma('getItems');
                var url =$.trim( $('[id$=txtUrl]').val());
                var usuario =$.trim( $('[id$=txtUsuario]').val());
                var password =$.trim( $('[id$=txtPassword]').val());
                var dominio =$.trim($('[id$=txtDominio]').val());
                var url2 = $.trim($('[id$=txtUrl2]').val());
                var usuario2 =$.trim( $('[id$=txtUsuario2]').val());
                var password2 =$.trim( $('[id$=txtPassword2]').val());
                var dominio2 =$.trim( $('[id$=txtDominio2]').val());
                var BD =$.trim( $('[id$=txtBD]').val());
                var cubo =$.trim( $('[id$=txtCubo]').val());
                var activo =$('[id$=chkActivo]').is(":checked");

                var presupuestoAbierto = $('[id$=chkPresupuestoAbierto]').is(":checked");
                var presupuestoConsumido = $('[id$=chkPresupuestoConsumido]').is(":checked");
                var importeAdjudicacion = $('[id$=chkImporteAdjudicacion]').is(":checked");
                var ahorroAdjudicacion = $('[id$=chkAhorroAdjudicacion]').is(":checked");
                var importePedido = $('[id$=chkImportePedido]').is(":checked");
                var importeRecepcion = $('[id$=chkImporteRecepcion]').is(":checked");
                var ahorroRecepcion = $('[id$=chkAhorroRecepcion]').is(":checked");
                var presupuestoAbierto2 = $('[id$=chkPresupuestoAbierto2]').is(":checked");
                var presupuestoConsumido2 = $('[id$=chkPresupuestoConsumido2]').is(":checked");
                var importeAdjudicacion2 = $('[id$=chkImporteAdjudicacion2]').is(":checked");
                var ahorroAdjudicacion2 = $('[id$=chkAhorroAdjudicacion2]').is(":checked");
                var importePedido2 = $('[id$=chkImportePedido2]').is(":checked");
                var importeRecepcion2 = $('[id$=chkImporteRecepcion2]').is(":checked");
                var ahorroRecepcion2 = $('[id$=chkAhorroRecepcion2]').is(":checked");

                var availableMeasures = "";
                if (presupuestoAbierto) { availableMeasures = availableMeasures + TextosCubos[17] + "|" }           //Presupuesto abierto
                if (presupuestoConsumido) { availableMeasures = availableMeasures + TextosCubos[18] + "|" }         //Presupuesto consumido
                if (importeAdjudicacion) { availableMeasures = availableMeasures + TextosCubos[19] + "|" }          //Importe adjudicación
                if (ahorroAdjudicacion) { availableMeasures = availableMeasures + TextosCubos[20] + "|" }           //Ahorro adjudicación
                if (importePedido) { availableMeasures = availableMeasures + TextosCubos[21] + "|" }                //Importe pedido
                if (importeRecepcion) { availableMeasures = availableMeasures + TextosCubos[22] + "|" }             //Importe recepción
                if (ahorroRecepcion) { availableMeasures = availableMeasures + TextosCubos[23] + "|" }              //Ahorro recepción

                var defaultMeasures = "";
                if (presupuestoAbierto2) { defaultMeasures = defaultMeasures + TextosCubos[17] + "|" }              //Presupuesto abierto
                if (presupuestoConsumido2) { defaultMeasures = defaultMeasures + TextosCubos[18] + "|" }            //Presupuesto consumido
                if (importeAdjudicacion2) { defaultMeasures = defaultMeasures + TextosCubos[19] + "|" }             //Importe adjudicación
                if (ahorroAdjudicacion2) { defaultMeasures = defaultMeasures + TextosCubos[20] + "|" }              //Ahorro adjudicación
                if (importePedido2) { defaultMeasures = defaultMeasures + TextosCubos[21] + "|" }                   //Importe pedido
                if (importeRecepcion2) { defaultMeasures = defaultMeasures + TextosCubos[22] + "|" }                //Importe recepción
                if (ahorroRecepcion2) { defaultMeasures = defaultMeasures + TextosCubos[23] + "|" }                 //Ahorro recepción

                var grid = $find($('[id$=whgCubos]').attr('id')).get_gridView();
                var selectedRows = grid.get_behaviors().get_selection().get_selectedRows();
                var idCubo = selectedRows.getItem(0).get_cellByColumnKey("ID").get_value();
                if (ComprobarCuboCorrecto(url2, usuario2, password2, dominio2, BD, cubo)) {
                    if (ComprobarServicioCorrecto(url)) {
                        var to = rutaFS + 'BI/Cubos.aspx/ConfigurarCubo';
                        $.when($.ajax({
                            type: 'POST',
                            url: to,
                            data: JSON.stringify({ denominaciones: denominaciones, url: url, usuario: usuario, password: password, dominio: dominio, url2: url2, usuario2: usuario2, password2: password2, dominio2: dominio2, BD: BD, cubo: cubo, activo: activo, availableMeasures: availableMeasures, defaultMeasures: defaultMeasures, idCubo: idCubo }),
                            contentType: 'application/json;charset=utf-8',
                            dataType: 'json',
                            async: true
                        })).done(function (msg) {
                            LimpiarModal();
                            $('#popupFondo').hide();
                            var btnUpdateGrid = $('[id$=btnUpdateGrid]').attr('id');
                            __doPostBack(btnUpdateGrid, '');
                        });
                    }
                }
            }
        }

//......................Funciones Permisos...................................................................................
        function PermisosClick() {
            if (ComprobarUnaFilaSeleccionada()) {
                var grid = $find($('[id$=whgCubos]').attr('id')).get_gridView();
                var selectedRows = grid.get_behaviors().get_selection().get_selectedRows();
                var nombreCubo = selectedRows.getItem(0).get_cellByColumnKey("DEN").get_value();
                var idCubo = selectedRows.getItem(0).get_cellByColumnKey("ID").get_value();             
                var punto = window.center({ width: $('#pnlPermisos').width(), height: $('#pnlPermisos').height() });
                $('#pnlPermisos').css('top', punto.y + 'px');
                $('#pnlPermisos').css('left', punto.x + 'px');
                $('#popupFondo').css('height', $(document).height());
                $('#popupFondo').show();
				$('[id$=pnlPermisos]').css('visibility', 'visible');
                $('#pnlPermisos').show();
                $('[id$=lblCabeceraPermisos]').text(TextosCubos[32]+" "+nombreCubo);    //Permiso de acceso al cubo
                $('[id$=lblMensajePermisos]').text(TextosCubos[27]);                    //Acceso habilitado para los siguientes Usuarios/Unidades Organizativas: 
                $('[id$=lblAgregarPermisos]').text(TextosCubos[28]);                    //Agregar permisos:
                $('#divListaPara').empty()
                var to = rutaFS + 'BI/Cubos.aspx/ObtenerPermisosUON';
                $.when($.ajax({
                    type: 'POST',
                    url: to,
                    data: JSON.stringify({ idCubo: idCubo }),
                    contentType: 'application/json;charset=utf-8',
                    dataType: 'json',
                    async: true
                })).done(function (msg) {
                    var data = msg.d;
                    $('[id$=divUsuarios]').empty()
                    for (var i = 0; i < data.length - 1; i++) {
                        var id = data[i].split("||")
                        $('[id$=divUsuarios]').append('<div id="PermisoSeleccionado_' + id[0] + '" style="border-bottom-color: #EEEEEE; border-bottom-width: 0.5pt; border-bottom-style: solid; padding-top:3pt; padding-bottom:3pt"><span id="' + id[0] + '" class="Texto12" style="margin-left:10px; margin-top:10px">' + id[1] + '</span> <asp:Button id="' + id[0] + '" style="float:right; cursor:Pointer; margin-right:10px" onclick="BorrarSeleccionado(id);return false">x</asp:Button> </div>');
                    }
                    Control_Panel_ParaUON('divParaUON', '');
                    $('#divParaUONPanel').show();
                    $('#divCuboOpcionesPara').show();
                    $('[id$=lblCopiarPermisos]').text(TextosCubos[48]);         //Copiar permisos
                    $('[id$=lblCopiarPermisos2]').text(TextosCubos[49]);        //Copiar permisos del cubo :
                    $('[id$=chkCopiarPermisos]').attr('checked', false);
                    $('[id$=ddlCopiarPermisos]').prop('disabled', true);
                });
            }
        }

        /*Se ha seleccionado nuevos permisos. Tras grabar, limpia la caja de texto, el nodo seleccionado, cierra el popup y desmarca chkCopiarPermisos*/
        function AceptarPermisoModalClick() {
            var grid = $find($('[id$=whgCubos]').attr('id')).get_gridView();
            var selectedRows = grid.get_behaviors().get_selection().get_selectedRows();
            var idCubo = selectedRows.getItem(0).get_cellByColumnKey("ID").get_value();
            if($('[id$=chkCopiarPermisos]').is(':checked')) {
                var idCuboACopiar = $('[id$=ddlCopiarPermisos]').val();
                var to = rutaFS + 'BI/Cubos.aspx/CopiarPermisos';
                $.when($.ajax({
                    type: 'POST',
                    url: to,
                    data: JSON.stringify({ idCuboACopiar: idCuboACopiar, idCubo: idCubo }),
                    contentType: 'application/json;charset=utf-8',
                    dataType: 'json',
                    async: true
                })).done(function (msg) {
                    $('#txtParaUON').val('');
                    $('#tvParaUONTree span.treeSeleccionable.selected').removeClass('selected');
                    $('#popupFondo').hide();
                    $('#pnlPermisos').hide();
                    $('[id$=chkCopiarPermisos]').attr('checked', false);
                    $('[id$=ddlCopiarPermisos]').prop('disabled', true);
                });
            }else
            {
                var to= rutaFS + 'BI/Cubos.aspx/GuardarPermisos';
                $.when($.ajax({
                    type: 'POST',
                    url: to,
                    data: JSON.stringify({ idUsuario: idUsuario, idCubo: idCubo }),
                    contentType: 'application/json;charset=utf-8',
                    dataType: 'json',
                    async: true
                })).done(function (msg) {
                    $('#txtParaUON').val('');
                    $('#tvParaUONTree span.treeSeleccionable.selected').removeClass('selected');
                    $('#popupFondo').hide();
                    $('#pnlPermisos').hide();              
                });
            }
        }

        /*No se selecciona ningun permiso. Limpia la caja de texto, el nodo seleccionado y cierra el popup*/
        function CancelarPermisoModalClick() {
            $('#txtParaUON').val('');
            $('#tvParaUONTree span.treeSeleccionable.selected').removeClass('selected');
            $('#popupFondo').hide();
            $('#pnlPermisos').hide();
        }
        function BorrarSeleccionado(id) {
             var grid = $find($('[id$=whgCubos]').attr('id')).get_gridView();
             var selectedRows = grid.get_behaviors().get_selection().get_selectedRows();
             var idCubo = selectedRows.getItem(0).get_cellByColumnKey("ID").get_value();
             var to = rutaFS + 'BI/Cubos.aspx/BorrarPermiso';
             $.when($.ajax({
                 type: 'POST',
                 url: to,
                 data: JSON.stringify({ id: id }),
                 contentType: 'application/json;charset=utf-8',
                 dataType: 'json',
                 async: true
             })).done(function (msg) {
				if (id.indexOf('+') > 0) {
                     id = id.split('+')[0];
                 }
                 $('[id^=PermisoSeleccionado_' + id + ']').remove();
                 idUsuario.pop(id);
             });
        }
       
       
</script>
<div style="clear:both; float:left; width:100%;">
		<fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
</div>
    <asp:Button runat="server" ID="btnOrder" style="display:none;" />
    <asp:Button runat="server" ID="btnFilter" style="display:none;" />
    <asp:Button runat="server" ID="btnRecargaCache" style="display:none;" />    
    <asp:UpdatePanel runat="server" ID="upPager" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnOrder" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnFilter" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:HiddenField runat="server" ID="hFiltered" Value="0" />
            <asp:HiddenField runat="server" ID="hFilteredColumns" Value="" />
            <asp:HiddenField runat="server" ID="hFilteredCondition" Value="" />
            <asp:HiddenField runat="server" ID="hFilteredValue" Value="" />
            <asp:HiddenField runat="server" ID="hOrdered" Value="" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Button runat="server" ID="btnPager" style="display:none;" />	
    <ig:WebHierarchicalDataGrid runat="server" ID="whgCubosExportacion" 
                    Visible="false" AutoGenerateBands="false"
					AutoGenerateColumns="false" Width="100%" EnableAjax="true" 
					EnableAjaxViewState="false" EnableDataViewState="false">
                    <Behaviors>
                        <ig:Filtering Enabled="true"></ig:Filtering>
                        <ig:Sorting Enabled="true" SortingMode="Multi"></ig:Sorting>
                        <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
                    </Behaviors>
    </ig:WebHierarchicalDataGrid>
	<div style="clear:both; float:left; width:100%;">
        <asp:Button runat="server" ID="btnUpdateGrid" style="display:none;"></asp:Button> 
		<asp:UpdatePanel ID="updGridCubos" ChildrenAsTriggers="false" runat="server" UpdateMode="Conditional">						
			<Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnPager" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnUpdateGrid" EventName="Click" />
			</Triggers>
			<ContentTemplate>
				<ig:WebHierarchicalDataGrid runat="server" ID="whgCubos" AutoGenerateBands="false"
					AutoGenerateColumns="false" Width="100%" EnableAjax="true" 
					EnableAjaxViewState="false" EnableDataViewState="false">
					<GroupingSettings EnableColumnGrouping="True" GroupAreaVisibility="Visible"></GroupingSettings>
					<Behaviors>
                        <ig:Activation Enabled="true"/>
						<ig:Filtering Alignment="Top" Enabled="true" Visibility="Visible" AnimationEnabled="false">
                        </ig:Filtering>                 
						<ig:RowSelectors Enabled="true" EnableInheritance="true">
						</ig:RowSelectors>
						<ig:Selection Enabled="true" RowSelectType="Multiple" CellSelectType="None" ColumnSelectType="None">
						</ig:Selection>
						<ig:Sorting Enabled="true" SortingMode="Multi" ></ig:Sorting>
						<ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
						<ig:ColumnMoving Enabled="false"></ig:ColumnMoving>								
						<ig:Paging Enabled="true" PagerAppearance="Top">
							<PagerTemplate>
								<div class="CabeceraBotones" style="clear:both; float:left; width:100%;"> 
									<div style="float:right; margin:5px 5px 0px 0px;">
                                        <asp:Panel runat="server" ID="pnlNuevoCubo" onclick="NuevoCubo()" class="botonDerechaCabecera" style="line-height:22px; float:left;">
                                            <asp:Image runat="server" ID="imgNuevoCubo" CssClass="imagenCabecera" />
											<asp:Label runat="server" ID="lnkNuevoCubo" CssClass="fntLogin2" style="margin-left:3px" Text="dNuevoCubo"></asp:Label>
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="pnlConfigurarCubo" onclick="ObtenerDatosCubo()" class="botonDerechaCabecera" style="line-height:22px; float:left;">           
											<asp:Image runat="server" ID="imgConfigurarCubo" CssClass="imagenCabecera"/>
											<asp:Label runat="server" ID="lnkConfigurarCubo" CssClass="fntLogin2" style="margin-left:3px" Text="dConfigurarCubo"></asp:Label>           
										</asp:Panel>
                                        <asp:Panel runat="server" ID="pnlEliminarCubo" onclick="EliminarClick()" class="botonDerechaCabecera" style="line-height:22px; float:left;">										       
											<asp:Image runat="server" ID="imgEliminarCubo" CssClass="imagenCabecera" />
											<asp:Label runat="server" ID="lnkEliminarCubo" CssClass="fntLogin2" style="margin-left:3px" Text="dEliminarCubo"></asp:Label>           
										</asp:Panel>
										<asp:Panel runat="server" ID="pnlPermisosCubo" onclick="PermisosClick()" class="botonDerechaCabecera" style="line-height:22px; float:left;">            
											<asp:Image runat="server" ID="imgPermisos" CssClass="imagenCabecera" />
											<asp:Label runat="server" ID="lblPermisos" CssClass="fntLogin2" style="margin-left:3px" Text="dPermisos"></asp:Label>           
										</asp:Panel>
									</div>
									<%-- <div style="float:left; padding:3px 0px 2px 0px; margin-left:10px;">
										<div style="clear: both; float: left; margin-right: 5px;">
                                            <asp:Image ID="ImgBtnFirst" runat="server" OnClientClick="FirstPage()" style="margin-right:2px;" />
                                            <asp:Image ID="ImgBtnPrev" runat="server" OnClientClick="PrevPage()" />
                                        </div>
                                        <div style="float: left; margin-right:5px;">
                                            <asp:Label ID="lblPage" runat="server" Text="dPage" Style="margin-right: 3px;"></asp:Label>
                                            <asp:DropDownList ID="PagerPageList" runat="server" Style="margin-right: 3px;" onchange="return IndexChanged()" />
                                            <asp:Label ID="lblOF" runat="server" Text="dOf" Style="margin-right: 3px;"></asp:Label>
                                            <asp:Label ID="lblCount" runat="server" />
                                        </div>
                                        <div style="float: left;">
                                            <asp:Image ID="ImgBtnNext" runat="server" OnClientClick="NextPage()" style="margin-right:2px;" />
                                            <asp:Image ID="ImgBtnLast" runat="server" OnClientClick="LastPage()" />
                                        </div>
									</div>--%>									
								</div>						
							</PagerTemplate>
						</ig:Paging>						
					</Behaviors>
				</ig:WebHierarchicalDataGrid>                
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>
    <div id="pnlCubos" class="Rectangulo" style="display:none; width:50%; position:absolute; z-index:1002; min-width:700px; min-height:590px">        
        <div style="position:relative">
            <div id="divCabeceraCubos" class="PageHeaderBackground" style="position:relative; z-index:1; float:left; height:25px; line-height:25px; background-color:#AB0020">			
			    <span id="lblNuevoCubo" class="Texto14 TextoClaro Negrita" style="margin-left:10px"></span>
                <input type="image" src="~/images/Bt_Cerrar.png" id="imgCerrar" runat="server" SkinID="Cerrar" style="border:0px; width:25px; float:right; cursor:pointer"/>
		    </div>	
            <div style="clear:both; position:relative; width:100%; float:left; margin-top:10px">
                <div style="float:left; margin-left:30px">
                    <span id="lblNombre" class="Texto12 Negrita" runat="server" style="width:80px; margin-left:12px"></span>
                    <fsn:FSNTextBoxMultiIdioma ID="txtNombre" runat="server" Width="425px" style="float:right; margin-left:12px"></fsn:FSNTextBoxMultiIdioma>
                </div>
                <div style="float:left; margin-left:62px" >
                    <span ID="lblActivo" class="Texto12 Negrita" runat="server"></span>
                    <input id="chkActivo" type="checkbox" runat="server" style="margin-left:12px"/>
                </div>
            </div>
            <div id="divServicioDatos" class="dContenedor" style="clear:both; float:left; margin-top:10px; margin-bottom:10px; margin-left:30px; padding-bottom:10px; width:90%; background-color:transparent">                                  
                    <span id="lblServicioDatos" style="margin-top: -10px; margin-left: 20px" class="Texto14 Negrita"></span>
                    <div style="float:left; margin-top:10px">
                        <span id="lblUrl" class="Texto12 Negrita" runat="server" style="margin-left:40px;"></span>
                        <input id="txtUrl" runat="server" style="margin-left:10px; width:520px" />
                    </div>
                    <div style="clear:both; float:left; margin-top:10px">
                        <span id="lblUsuario" class="Texto12 Negrita" runat="server" style="margin-left:12px"></span>
                        <input id="txtUsuario" runat="server" style="margin-left:10px; width:100px" />
                    </div>
                    <div style="float:left; margin-top:10px">
                        <span id="lblPassword" class="Texto12 Negrita" runat="server" style="margin-left:36px"></span>
                        <input id="txtPassword" type="password" runat="server" style="margin-left:10px; width:100px" />
                    </div>
                    <div style="float:left; margin-top:10px">
                        <span id="lblDominio" class="Texto12 Negrita" runat="server" style="margin-left:36px"></span>
                        <input id="txtDominio" runat="server" style="margin-left:10px; width:100px" />
                    </div>
            </div>
            <div id="divAnalysisService" class="dContenedor" style="clear:both; float:left; margin-top:10px; margin-bottom:10px; margin-left:30px; padding-bottom:10px; width:90%; background-color:transparent">                                 
                    <span id="lblAnalysisService" style="margin-top: -10px; margin-left:20px" class="Texto14 Negrita"></span>
                    <div style="float:left; margin-top:10px; margin-left:40px">
                        <span id="lblUrl2" class="Texto12 Negrita" runat="server" ></span>
                        <input id="txtUrl2" runat="server" style="margin-left:10px; width:520px" />
                    </div>
                    <div style="clear:both; float:left; margin-top:10px; margin-left:12px">
                        <span id="lblUsuario2" class="Texto12 Negrita" runat="server"></span>
                        <input id="txtUsuario2" runat="server" style="width:100px; margin-left:10px" />
                    </div>
                    <div style="float:left; margin-top:10px; margin-left:36px">
                        <span id="lblPassword2" class="Texto12 Negrita" runat="server"></span>
                        <input id="txtPassword2" runat="server" type="password" style="width:100px; margin-left:10px" />
                    </div>
                    <div style="float:left; margin-top:10px; margin-left:36px">
                        <span id="lblDominio2" class="Texto12 Negrita" runat="server"></span>
                        <input id="txtDominio2" runat="server" style="margin-left:10px; width:100px" />
                    </div>
                    <div style="clear:both; float:left; margin-top:10px; margin-left:40px">
                        <span id="lblBD" class="Texto12 Negrita" runat="server"></span>
                        <input id="txtBD" runat="server" style="margin-left:10px; width:100px" />
                    </div>
                    <div style="float:left; margin-top:10px; margin-left:61px">
                        <span id="lblCubo" class="Texto12 Negrita" runat="server"></span>
                        <input id="txtCubo" runat="server" style="margin-left:10px; width:100px" />
                    </div>

            </div> 
            <div id="divMedidasDisponibles" class="dContenedor" style="float:left; margin-top:10px; margin-bottom:10px; margin-left:30px; padding-bottom:10px; width:40%; background-color:transparent; min-height:220px">                                 
                    <span id="lblMedidasDisponibles" style="margin-top: -10px; margin-left:20px" class="Texto14 Negrita"></span>
                    <div style="float:left; margin-left:10px; margin-top:10px" >
                        <input id="chkPresupuestoAbierto" type="checkbox" runat="server" style="margin-left:10px"/>
                        <span ID="lblPresupuestoAbierto" class="Texto12 Negrita" runat="server" style="margin-left:10px" ></span>
                    </div>
                   <div style="float:left; margin-left:10px; margin-top:10px" >
                        <input id="chkPresupuestoConsumido" type="checkbox" runat="server" style="margin-left:10px"/>
                        <span ID="lblPresupuestoConsumido" class="Texto12 Negrita" runat="server" style="margin-left:10px" ></span>
                    </div>
                    <div style="float:left; margin-left:10px; margin-top:10px" >
                        <input id="chkImporteAdjudicacion" type="checkbox" runat="server" style="margin-left:10px"/>
                        <span ID="lblImporteAdjudicacion" class="Texto12 Negrita" runat="server" style="margin-left:10px" ></span>
                    </div>
                    <div style="float:left; margin-left:10px; margin-top:10px" >
                        <input id="chkAhorroAdjudicacion" type="checkbox" runat="server" style="margin-left:10px"/>
                        <span ID="lblAhorroAdjudicacion" class="Texto12 Negrita" runat="server" style="margin-left:10px" ></span>
                    </div>
                    <div style="float:left; margin-left:10px; margin-top:10px" >
                        <input id="chkImportePedido" type="checkbox" runat="server" style="margin-left:10px"/>
                        <span ID="lblImportePedido" class="Texto12 Negrita" runat="server" style="margin-left:10px" ></span>
                    </div>
                    <div style="float:left; margin-left:10px; margin-top:10px" >
                        <input id="chkImporteRecepcion" type="checkbox" runat="server" style="margin-left:10px"/>
                        <span ID="lblImporteRecepcion" class="Texto12 Negrita" runat="server" style="margin-left:10px" ></span>
                    </div>
                    <div style="float:left; margin-left:10px; margin-top:10px" >
                        <input id="chkAhorroRecepcion" type="checkbox" runat="server" style="margin-left:10px"/>
                        <span ID="lblAhorroRecepcion" class="Texto12 Negrita" runat="server" style="margin-left:10px" ></span>
                    </div>
            </div>
            <div id="divMedidasPredeterminadas" class="dContenedor" style="float:right; margin-top:10px; margin-bottom:10px; margin-right:37px; padding-bottom:10px; width:40%;background-color:transparent; min-height:220px">                                 
                    <span id="lblMedidasPredeterminadas" style="margin-top: -10px; margin-left:20px" class="Texto14 Negrita"></span>
                    <div style="float:left; margin-left:10px; margin-top:10px" >
                        <input id="chkPresupuestoAbierto2" type="checkbox" runat="server" style="margin-left:10px"/>
                        <span ID="lblPresupuestoAbierto2" class="Texto12 Negrita" runat="server" style="margin-left:10px" ></span>
                    </div>
                   <div style="float:left; margin-left:10px; margin-top:10px" >
                        <input id="chkPresupuestoConsumido2" type="checkbox" runat="server" style="margin-left:10px"/>
                        <span ID="lblPresupuestoConsumido2" class="Texto12 Negrita" runat="server" style="margin-left:10px" ></span>
                    </div>
                    <div style="float:left; margin-left:10px; margin-top:10px" >
                        <input id="chkImporteAdjudicacion2" type="checkbox" runat="server" style="margin-left:10px"/>
                        <span ID="lblImporteAdjudicacion2" class="Texto12 Negrita" runat="server" style="margin-left:10px" ></span>
                    </div>
                    <div style="float:left; margin-left:10px; margin-top:10px" >
                        <input id="chkAhorroAdjudicacion2" type="checkbox" runat="server" style="margin-left:10px"/>
                        <span ID="lblAhorroAdjudicacion2" class="Texto12 Negrita" runat="server" style="margin-left:10px" ></span>
                    </div>
                    <div style="float:left; margin-left:10px; margin-top:10px" >
                        <input id="chkImportePedido2" type="checkbox" runat="server" style="margin-left:10px"/>
                        <span ID="lblImportePedido2" class="Texto12 Negrita" runat="server" style="margin-left:10px" ></span>
                    </div>
                     <div style="float:left; margin-left:10px; margin-top:10px" >
                        <input id="chkImporteRecepcion2" type="checkbox" runat="server" style="margin-left:10px"/>
                        <span ID="lblImporteRecepcion2" class="Texto12 Negrita" runat="server" style="margin-left:10px" ></span>
                    </div>
                    <div style="float:left; margin-left:10px; margin-top:10px" >
                        <input id="chkAhorroRecepcion2" type="checkbox" runat="server" style="margin-left:10px"/>
                        <span ID="lblAhorroRecepcion2" class="Texto12 Negrita" runat="server" style="margin-left:10px" ></span>
                    </div>
            </div>
                    <div style="clear:both; margin-left:280px; margin-top: 40px; margin-bottom:40px; width:150px; background-color:transparent">
                        <fsn:FSNButton ID="btnAceptar" runat="server" Text="Aceptar" Alineacion="Left" OnClientClick="InsertarCubo(); return false;" ></fsn:FSNButton>   
                        <fsn:FSNButton ID="btnAceptarConfigurar" runat="server" Text="Aceptar" Alineacion="Left" OnClientClick="AceptarConfigurarCubo(); return false;" style="display:none" ></fsn:FSNButton>   
                        <fsn:FSNButton ID="btnCancelar" runat="server" Text="Cancelar" Alineacion="Left" OnClientClick="CancelarNuevoCubo(); return false;" style="margin-left:10px"></fsn:FSNButton>
                    </div>
            </div>
</div>
 <div id="pnlPermisos" class="popupControl" style="display:none; position:absolute; vertical-align:middle; z-index:1002; width:50%; min-width:600px; min-height:390px; background-color:White; border-color:#F1F1F1">        
    <div id="divFondo" style="margin-bottom:20px; background-color:White">
        <div id="divCabeceraPermisos" class="Cabecera" style="position:relative; z-index:1; float:left; height:25px; line-height:25px; width:100%; background-color:#AB0020">			
			        <span id="lblCabeceraPermisos" class="Texto14 TextoClaro Negrita" style="margin-left:10px"></span>
                    <asp:Image ImageUrl="~/images/Bt_Cerrar.png" id="imgCerrarPermisos" runat="server" SkinID="Cerrar" style="border:0px; width:25px; float:right; cursor:pointer"  />
        </div>	
        <br /><br /><br />
        <div id="divTabContainer" style="margin-left:10px; margin-right:10px">
            <div id="tabs" >
                <ul>
                    <li><a href="#tabs-1">
                        <span id="lblAgregarPermisos" class="Texto12" ></span>
                    </a></li>
                    <li><a href="#tabs-2">
                        <span id="lblCopiarPermisos" class="Texto12" style="margin-left:10px"></span>
                    </a></li>
                </ul>
                <div id="tabs-1" style="height:420px">  
                         <div id="divMensajePermisos" style="clear:both; position:relative; width:100%; float:left; margin-top:10px; margin-left:10px">
                            <span id="lblMensajePermisos" class="Texto12 Negrita" ></span>
                         </div>    
                        <div id="divUsuarios" class="divContenedor" style="clear:both; float:left; width:95%; margin-left:10px; min-height:18px; max-height:46px; overflow-y:auto;"></div>         
                        <div id="divPermisos" style="clear:both; position:relative; width:95%; float:left; margin-top:10px; margin-left:10px; min-height:300px"></div>                
                </div>
                <div id="tabs-2">
                        <input type="checkbox" id="chkCopiarPermisos" style="margin-left:10px"/>
                        <span id="lblCopiarPermisos2" class="Texto12" ></span>
                        <asp:DropDownList runat="server" id="ddlCopiarPermisos" style="float:right; margin-right:50px"></asp:DropDownList>
                </div>
             </div>
        </div>
    </div>
    <div style="clear:both; margin-left:40%; margin-bottom:40px; margin-top:20px">
            <fsn:FSNButton ID="btnAceptarPermisos" runat="server" Text="Aceptar" Alineacion="Left" OnClientClick="AceptarPermisoModalClick();return false;" style="margin-bottom:10px" ></fsn:FSNButton>   
            <fsn:FSNButton ID="btnCancelarPermisos" runat="server" Text="Cancelar" Alineacion="Left" OnClientClick="CancelarPermisoModalClick();return false;" style="margin-left:10px; margin-bottom:10px"></fsn:FSNButton>
    </div>
    
</div>
</asp:Content>


