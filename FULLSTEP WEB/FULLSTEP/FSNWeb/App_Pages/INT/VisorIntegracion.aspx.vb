﻿Imports System.IO
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports System.Web.Script.Serialization
Imports Fullstep.FSNServer
Imports Infragistics.Web.UI
Imports Infragistics.Web.UI.GridControls
Imports Infragistics.Documents.Reports
Imports Infragistics.Documents.Reports.Report

Public Class VisorIntegracion
    Inherits FSNPage

    Private _pagerControl As wucPagerControl
    Private _dsVisor As DataSet
    Private _dsHistorico As DataSet

    ''' <summary>
    ''' Dataset con los datos del visor
    ''' </summary>
    Protected ReadOnly Property DatasetVisor(Optional ByVal RecargarDatos As Boolean = False) As DataSet
        Get
            If Not IsPostBack Then
                RecargarDatos = True
            Else
                If Request("__EVENTTARGET") = btnBuscarGeneral.UniqueID Then
                    RecargarDatos = True
                End If
                '''si he pulsado el botón de integración correcta fuerzo la actualización del grid
                If Request("__EVENTTARGET") = btnOk.UniqueID Then
                    RecargarDatos = True
                End If
            End If

            If RecargarDatos Then
                ObtenerDatosVisor()
            End If

            Dim dtVisorInt As DataSet = CType(Cache("oDsVisorInt_" & FSNUser.Cod), DataSet).Copy
            If Not IsNothing(dtVisorInt) Then dtVisorInt.Tables(0).TableName = "."
            Return dtVisorInt
        End Get
    End Property
#Region "Inicio"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ScriptMgr.EnableScriptGlobalization = True

        _pagerControl = CType(whdgVisorInt.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("CustomerPager"), wucPagerControl)
        AddHandler _pagerControl.PageChanged, AddressOf currentPageControl_PageChanged
    End Sub
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = ModulosIdiomas.VisorIntegracion
        CargarTextos()
        CargaImagenes()
        CargarHistorico()

        If Not IsPostBack Then

            Dim Master = CType(Me.Master, FSNWeb.Menu)
            Master.Seleccionar("Integracion", "")

            ''Comprueba si tiene que mostrar el moda popup de alerta de parada del servicio de integracion
            'El tab de integración mostrará un icono y un tooltip en caso de parada del servicio de integración
            Dim dtFechaParada As DateTime
            Dim oInt As FSNServer.VisorInt = FSNServer.Get_Object(GetType(FSNServer.VisorInt))
            If oInt.HayParadaServicioInt(dtFechaParada) Then
                If Not IsNothing(dtFechaParada) Then
                    lblHoraParada.Text = lblHoraParada.Text & Format(dtFechaParada, FSNUser.DateFormat.ShortDatePattern) & " " & Format(dtFechaParada, FSNUser.DateFormat.ShortTimePattern)
                End If
                mpeAlertaParada.Show()
            End If

            CargarERPs()

            CargarEntidades(ddERP.SelectedValue)

            InicializacionControles()

            'Nº filas
            Me.whdgVisorInt.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PaginacionINT")
            Me.whdgVisorInt.GridView.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PaginacionINT")

            ''Colapsa el buscador avanzado
            Me.CollapsiblePanelExtender1.Collapsed = True

            ''Busqueda inicial: Carga por defecto los datos desde la fecha actual
            dteDesde.Value = Now().Date

            CargaGrid()
        Else
            CargaGrid()
            Select Case Request("__EVENTTARGET")
                Case "ExportarExcel"
                    ExportarExcel()
                Case "ExportarPDF"
                    ExportarPDF()
            End Select
        End If

        Dim ModalProgress As String = CType(Master.Master.FindControl("ModalProgress"), AjaxControlToolkit.ModalPopupExtender).ClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalClientID", "<script>var ModalProgress = '" & ModalProgress & "' </script>")

        Dim ErrorAnimationClientID As String = FSNPanelErrorInt.AnimationClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ErrorAnimationClientID", "<script>var ErrorAnimationClientID = '" & ErrorAnimationClientID & "' </script>")

        Dim ErrorDynamicPopulateClienteID As String = FSNPanelErrorInt.DynamicPopulateClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ErrorDynamicPopulateClienteID", "<script>var ErrorDynamicPopulateClienteID = '" & ErrorDynamicPopulateClienteID & "' </script>")
    End Sub
    ''' Revisado por: ngo. Fecha: 02/03/2012
    ''' <summary>
    ''' Carga de los elementos con el idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo=0,2</remarks>
    Private Sub CargarTextos()
        FSNPageHeader.TituloCabecera = Textos(0)
        ' ''FSNPageHeader.TextoBotonINTHistorico = Textos(76)
        ' '''lnkHistoricoParadas.InnerText = Textos(76)
        ' '' btnHistoricoParadas.Value = Textos(76)
        lblERP.Text = Textos(1) & ":" 'ERP
        lblEntidad.Text = Textos(2) & ":" 'Entidad
        lblProceso.Text = Textos(3) 'Proceso
        lblPedido.Text = Textos(4) & ":" 'Pedido
        lblCodigo.Text = Textos(5) & ":" 'Código
        lblCodERP.Text = Textos(6) & ":" 'CódigoERP
        lblIdentificador.Text = Textos(7) & ":" 'Identificador
        lblAccion.Text = Textos(8) & ":" 'Acción
        lblEstado.Text = Textos(9) & ":" 'Estado
        lblSentido.Text = Textos(10) & ":" 'Sentido
        lblFechaDesde.Text = Textos(11) & ":" 'Fecha desde
        lblFechaHasta.Text = Textos(12) & ":" 'Fecha hasta
        btnBuscarGeneral.Text = Textos(18)
        btnLimpiarBusquedaAvanzada.Text = Textos(19)  'Limpiar
        lblBusquedaAvanzada.Text = Textos(20) 'Seleccione los criterios de búsqueda avanzados
        CType(whdgVisorInt.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblExcel"), Label).Text = Textos(21)
        CType(whdgVisorInt.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPDF"), Label).Text = Textos(22)

        lblParada.Text = Textos(73)
        lblHoraParada.Text = Textos(74)
        lblConsulteDpto.Text = Textos(75)

        FSNPanelErrorInt.Titulo = Textos(80)

        Me.lblAlertaRelanzar.Text = JSText(Me.Textos(82))
        Me.lblNuevoMovimiento.Text = JSText(Me.Textos(83))
        Me.btnAceptarRelanzar.Value = Me.Textos(84)
        Me.btnCancelarRelanzar.Value = Textos(85)
        Me.btnOk.Value = Me.Textos(84)

        'Me.btnCerrarHistorico.Text = Textos(86)

        'Me.schTabs.Tabs(0).Text = Textos(87)
        'Me.schTabs.Tabs(1).Text = Textos(88)
        'Me.schTabs.Tabs(2).Text = Textos(89)
        'Me.schTabs.Tabs(3).Text = Textos(90)

    End Sub
    ''' <summary>
    ''' Carga las imagenes correspondiente en los controles
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0seg.</remarks>
    Private Sub CargaImagenes()
        CType(whdgVisorInt.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgPDF"), Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/PDF.png"
        CType(whdgVisorInt.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgExcel"), Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/Excel.png"
        imgAlertaInt.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/alert-rojo.png"
        '' '' ''lnkHistoricoParadas.InnerText = "<img src='~/images/alert-rojo.png' style='text-align:center;   '/>" & lnkHistoricoParadas.InnerText
        '' '' ''' lnkParadas.Text = "<img src='~/images/alert-rojo.png' style='text-align:center;   '/>" & lnkHistoricoParadas.InnerText

        FSNPanelErrorInt.ImagenPopUp = "~/App_Themes/" & Page.Theme & "/images/alert-rojo.png"
    End Sub
    Private Sub InicializacionControles()
        ''Oculta los paneles
        pProceso.Visible = False
        pPedido.Visible = False
        pCodigo.Visible = False
        pCodigoERP.Visible = False
        pRecep.Visible = False

        ddAccion.Items.AddRange(AccionesInt.ToArray)
        ddAccion.DataBind()

        ddEstado.Items.AddRange(EstadosInt.ToArray)
        ddEstado.DataBind()

        ddSentido.Items.AddRange(SentidosInt.ToArray)

        'Formato calendario (fecha y hora)        
        dteDesde.NullText = ""
        dteHasta.NullText = ""

        hrDesde.DisplayModeFormat = Me.Usuario.DateFormat.ShortTimePattern
        hrDesde.EditModeFormat = Me.Usuario.DateFormat.ShortTimePattern
        hrDesde.DataMode = EditorControls.DateDataMode.Date
        hrDesde.Buttons.SpinButtonsDisplay = EditorControls.ButtonDisplay.OnRight
        hrDesde.Buttons.SpinOnArrowKeys = True

        hrHasta.DisplayModeFormat = Me.Usuario.DateFormat.ShortTimePattern
        hrHasta.EditModeFormat = Me.Usuario.DateFormat.ShortTimePattern
        hrHasta.DataMode = EditorControls.DateDataMode.Date
        hrHasta.Buttons.SpinButtonsDisplay = EditorControls.ButtonDisplay.OnRight
        hrHasta.Buttons.SpinOnArrowKeys = True
    End Sub
#End Region
#Region "Carga de datos buscador"
    ''' <summary>
    ''' Carga la combo con los nombres de los ERPs
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,1seg.</remarks>
    Private Sub CargarERPs()
        Dim oFSServer As FSNServer.Root = Session("FSN_Server")
        Dim oInt As FSNServer.VisorInt = oFSServer.Get_Object(GetType(FSNServer.VisorInt))
        Dim lista As List(Of ListItem) = From Datos In oInt.DevuelveERPs(FSNUser.Pyme).Tables(0)
                                         Order By Datos.Item("COD")
                                         Select New ListItem(Datos.Item("DEN"), Datos.Item("COD")) Distinct.ToList()
        ddERP.DataSource = lista
        ddERP.DataBind()

        If lista.Count = 1 Then
            ddERP.Visible = False
            lblERP.Text = lblERP.Text + " " & ddERP.SelectedItem.Text
        End If
        oInt = Nothing
        oFSServer = Nothing

        oInt = Nothing
        oFSServer = Nothing
    End Sub
    ''' <summary>
    ''' Carga la combo con los nombres de las entidades
    ''' </summary>
    ''' <param name="codERP">Código del ERP para el que se van a cargar las entidades integradas</param>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,1seg.</remarks>
    Private Sub CargarEntidades(Optional ByVal codERP As String = "")
        Dim oFSServer As FSNServer.Root = Session("FSN_Server")
        Dim oInt As FSNServer.VisorInt = oFSServer.Get_Object(GetType(FSNServer.VisorInt))
        Dim dsEntidades As DataSet
        Dim listaDeEntidades As New List(Of ListItem)
        'Devuelve el dataset con los IDs de las entidades (TABLA) con integración activada
        dsEntidades = oInt.DevuelveEntidades(codERP)

        'Crea el elemento lista con los nombres y los IDs de las entidades integradas
        listaDeEntidades.Insert(0, New ListItem(String.Empty, String.Empty))
        For Each oItem In dsEntidades.Tables(0).Rows
            Select Case oItem(0)
                Case TablasIntegracion.Mon
                    listaDeEntidades.Add(New ListItem(Textos(34), oItem(0)))
                Case TablasIntegracion.Pai
                    listaDeEntidades.Add(New ListItem(Textos(35), oItem(0)))
                Case TablasIntegracion.Provi
                    listaDeEntidades.Add(New ListItem(Textos(36), oItem(0)))
                Case TablasIntegracion.Pag
                    listaDeEntidades.Add(New ListItem(Textos(37), oItem(0)))
                Case TablasIntegracion.Dest
                    listaDeEntidades.Add(New ListItem(Textos(38), oItem(0)))
                Case TablasIntegracion.Uni
                    listaDeEntidades.Add(New ListItem(Textos(39), oItem(0)))
                Case TablasIntegracion.art4
                    listaDeEntidades.Add(New ListItem(Textos(40), oItem(0)))
                Case TablasIntegracion.Prove
                    listaDeEntidades.Add(New ListItem(Textos(41), oItem(0)))
                Case TablasIntegracion.Adj
                    listaDeEntidades.Add(New ListItem(Textos(42), oItem(0)))
                Case TablasIntegracion.con
                    listaDeEntidades.Add(New ListItem(Textos(43), oItem(0)))
                Case TablasIntegracion.prove_ERP
                    listaDeEntidades.Add(New ListItem(Textos(44), oItem(0)))
                Case TablasIntegracion.PED_Aprov
                    listaDeEntidades.Add(New ListItem(Textos(45), oItem(0)))
                Case TablasIntegracion.Rec_Aprov
                    listaDeEntidades.Add(New ListItem(Textos(47), oItem(0)))
                Case TablasIntegracion.PED_directo
                    listaDeEntidades.Add(New ListItem(Textos(46), oItem(0)))
                Case TablasIntegracion.Rec_Directo
                    listaDeEntidades.Add(New ListItem(Textos(48), oItem(0)))
                Case TablasIntegracion.Materiales
                    listaDeEntidades.Add(New ListItem(Textos(49), oItem(0)))
                Case TablasIntegracion.PresArt
                    listaDeEntidades.Add(New ListItem(Textos(50), oItem(0)))
                Case TablasIntegracion.VARCAL
                    listaDeEntidades.Add(New ListItem(Textos(51), oItem(0)))
                Case TablasIntegracion.PM
                    listaDeEntidades.Add(New ListItem(Textos(52), oItem(0)))
                Case TablasIntegracion.Presupuestos1
                    listaDeEntidades.Add(New ListItem(Textos(53), oItem(0)))
                Case TablasIntegracion.Presupuestos2
                    listaDeEntidades.Add(New ListItem(Textos(54), oItem(0)))
                Case TablasIntegracion.Presupuestos3
                    listaDeEntidades.Add(New ListItem(Textos(55), oItem(0)))
                Case TablasIntegracion.Presupuestos4
                    listaDeEntidades.Add(New ListItem(Textos(56), oItem(0)))
                Case TablasIntegracion.Proceso
                    listaDeEntidades.Add(New ListItem(Textos(57), oItem(0)))
                Case TablasIntegracion.ViaPag
                    listaDeEntidades.Add(New ListItem(Textos(59), oItem(0)))
                Case TablasIntegracion.PPM
                    listaDeEntidades.Add(New ListItem(Textos(60), oItem(0)))
                Case TablasIntegracion.TasasServicio
                    listaDeEntidades.Add(New ListItem(Textos(61), oItem(0)))
                Case TablasIntegracion.CargosProveedor
                    listaDeEntidades.Add(New ListItem(Textos(62), oItem(0)))
                Case TablasIntegracion.UnidadesOrganizativas
                    listaDeEntidades.Add(New ListItem(Textos(63), oItem(0)))
                Case TablasIntegracion.Usuarios
                    listaDeEntidades.Add(New ListItem(Textos(64), oItem(0)))
                Case TablasIntegracion.Activos
                    listaDeEntidades.Add(New ListItem(Textos(65), oItem(0)))
                Case TablasIntegracion.Facturas
                    listaDeEntidades.Add(New ListItem(Textos(66), oItem(0)))
                Case TablasIntegracion.Pagos
                    listaDeEntidades.Add(New ListItem(Textos(67), oItem(0)))
                Case TablasIntegracion.PRES5
                    listaDeEntidades.Add(New ListItem(Textos(68), oItem(0)))
                Case TablasIntegracion.CentroCoste
                    listaDeEntidades.Add(New ListItem(Textos(69), oItem(0)))
                Case TablasIntegracion.Empresa
                    listaDeEntidades.Add(New ListItem(Textos(70), oItem(0)))
                Case TablasIntegracion.ProvePortal
                    listaDeEntidades.Add(New ListItem(Textos(100), oItem(0)))
                Case TablasIntegracion.Certificado
                    listaDeEntidades.Add(New ListItem(Textos(101), oItem(0)))
                Case TablasIntegracion.ProveArticulo
                    listaDeEntidades.Add(New ListItem(Textos(102), oItem(0)))
            End Select
        Next


        ddEntidad.Items.Clear()
        ddEntidad.Items.AddRange(listaDeEntidades.ToArray)
        ddEntidad.DataBind()
        oInt = Nothing
        oFSServer = Nothing
    End Sub
    ''' <summary>
    ''' Carga los años de pedidos
    ''' </summary>
    ''' <remarks>Llamada desde: ddEntidad_SelectedIndexChanged</remarks>
    Private Sub CargarAnioPedido()
        Dim iAnyoActual As Integer
        Dim iInd As Integer
        Dim oFSServer As FSNServer.Root = Session("FSN_Server")
        Dim oInt As FSNServer.VisorInt = oFSServer.Get_Object(GetType(FSNServer.VisorInt))
        Dim oDsPedRecep As DataSet

        iAnyoActual = Year(Now)
        ddAnyoPed.Items.Clear()

        oDsPedRecep = oInt.DevuelveAnyo()

        Dim listaAnyo As List(Of ListItem) = From datos In oDsPedRecep.Tables(0) Order By datos.Item("ANYO") Descending
                                             Select New ListItem(datos.Item("ANYO"), datos.Item("ANYO")) Distinct.ToList()
        listaAnyo.Insert(0, New ListItem(String.Empty, String.Empty))

        ddAnyoPed.DataSource = listaAnyo
        ddAnyoPed.DataBind()

        ddAnyoPed.SelectedIndex = ddAnyoPed.Items.IndexOf(ddAnyoPed.Items.FindByText(iAnyoActual.ToString))

        oInt = Nothing
        oDsPedRecep = Nothing
    End Sub

    ''' <summary>
    ''' Vuelca los procesos existentes en la tabla de LOG a las combos de procesos
    ''' </summary>
    ''' <param name="ERP">Código del ERP seleccionado</param>
    ''' <param name="iEntidad">Identificador de la entidad seleccionada</param> 
    ''' <remarks></remarks>
    Private Sub CargarTodosProcesos(ByVal ERP As String, ByVal iEntidad As Integer)
        Dim oFSServer As FSNServer.Root = Session("FSN_Server")
        Dim oInt As FSNServer.VisorInt = oFSServer.Get_Object(GetType(FSNServer.VisorInt))
        Dim oDsProce As DataSet

        oDsProce = oInt.DevuelveProcesos(ERP, iEntidad)
        'Año
        Dim listaAnyo As List(Of ListItem) = From datos In oDsProce.Tables(0) Order By datos.Item("ANYO") Descending
                                             Select New ListItem(datos.Item("ANYO"), datos.Item("ANYO")) Distinct.ToList()
        listaAnyo.Insert(0, New ListItem(String.Empty, String.Empty))
        'Gmn1
        Dim listaGmn As List(Of ListItem) = From datos In oDsProce.Tables(0) Order By datos.Item("GMN1")
                                            Select New ListItem(datos.Item("GMN1"), datos.Item("GMN1")) Distinct.ToList()
        listaGmn.Insert(0, New ListItem(String.Empty, String.Empty))
        'Proce
        Dim listaProce As List(Of ListItem) = From datos In oDsProce.Tables(0) Order By datos.Item("COD")
                                              Select New ListItem(datos.Item("COD"), datos.Item("COD")) Distinct.ToList()
        listaProce.Insert(0, New ListItem(String.Empty, String.Empty))

        ddAnyoProce.DataSource = listaAnyo
        ddAnyoProce.DataBind()

        ddGmnProce.DataSource = listaGmn
        ddGmnProce.DataBind()

        ddCodProce.DataSource = listaProce
        ddCodProce.DataBind()

        oInt = Nothing
        oDsProce = Nothing
    End Sub

    ''' <summary>
    ''' Carga en las combos de procesos los procesos existentes en las tablas de log con los filtros indicados
    ''' </summary>
    ''' <param name="FiltroOrigen">Identificador para determinar desde qué combo se ha provocado el evento (anyo, gmn1 o proce) </param>
    ''' <remarks></remarks>
    Private Sub CargarProcesos(ByVal FiltroOrigen As Short)
        Dim anyo As Short = 0
        Dim gmn1 As String = ""
        Dim Proce As Integer = 0
        Dim anyoCarga As Short = 0
        Dim gmn1Carga As String = ""
        Dim ProceCarga As Integer = 0

        Dim oFSServer As FSNServer.Root = Session("FSN_Server")
        Dim oInt As FSNServer.VisorInt = oFSServer.Get_Object(GetType(FSNServer.VisorInt))
        Dim oDsProce As DataSet

        'Recupera los valores seleccionados
        If Not IsNothing(ddAnyoProce.SelectedItem) Then
            If ddAnyoProce.SelectedItem.Value <> "" Then anyo = ddAnyoProce.SelectedValue
        End If
        If Not IsNothing(ddGmnProce.SelectedItem) Then
            If ddGmnProce.SelectedItem.Value <> "" Then gmn1 = ddGmnProce.SelectedValue
        End If
        If Not IsNothing(ddCodProce.SelectedItem) Then
            If ddCodProce.SelectedItem.Value <> "" Then Proce = ddCodProce.SelectedValue
        End If

        'Carga los procesos filtrando por el valor seleccionado
        Select Case FiltroOrigen
            Case 1
                anyoCarga = anyo
            Case 2
                gmn1Carga = gmn1
            Case 3
                ProceCarga = Proce
        End Select
        oDsProce = oInt.DevuelveProcesos(ddERP.SelectedValue, ddEntidad.SelectedValue, anyoCarga, gmn1Carga, ProceCarga)

        'Año
        Dim listaAnyo As List(Of ListItem) = From datos In oDsProce.Tables(0) Order By datos.Item("ANYO") Descending
                                             Select New ListItem(datos.Item("ANYO"), datos.Item("ANYO")) Distinct.ToList()
        listaAnyo.Insert(0, New ListItem(String.Empty, String.Empty))

        'Gmn1
        Dim listaGmn As List(Of ListItem) = From datos In oDsProce.Tables(0) Order By datos.Item("GMN1")
                                            Select New ListItem(datos.Item("GMN1"), datos.Item("GMN1")) Distinct.ToList()
        listaGmn.Insert(0, New ListItem(String.Empty, String.Empty))

        'Proce
        Dim listaProce As List(Of ListItem) = From datos In oDsProce.Tables(0) Order By datos.Item("COD")
                                              Select New ListItem(datos.Item("COD"), datos.Item("COD")) Distinct.ToList()
        listaProce.Insert(0, New ListItem(String.Empty, String.Empty))

        Select Case FiltroOrigen
            Case 1
                ddGmnProce.Items.Clear()
                ddGmnProce.DataSource = listaGmn
                ddGmnProce.DataBind()
                If gmn1 <> "" Then
                    If Not IsNothing(ddGmnProce.Items.FindByText(gmn1)) Then
                        ddGmnProce.SelectedIndex = ddGmnProce.Items.IndexOf(ddGmnProce.Items.FindByText(gmn1))
                        'Para los procesos, filtro por los de este gmn1
                        listaProce.Clear()
                        listaProce = From datos In oDsProce.Tables(0).Select("GMN1=" & gmn1) Order By datos.Item("COD")
                                     Select New ListItem(datos.Item("COD"), datos.Item("COD")) Distinct.ToList()
                        listaProce.Insert(0, New ListItem(String.Empty, String.Empty))
                    End If
                End If
                ddCodProce.Items.Clear()
                ddCodProce.DataSource = listaProce
                ddCodProce.DataBind()
                If Proce <> 0 Then
                    If Not IsNothing(ddCodProce.Items.FindByText(Proce)) Then
                        ddCodProce.SelectedIndex = ddCodProce.Items.IndexOf(ddCodProce.Items.FindByText(Proce))
                    End If
                End If

            Case 2
                If anyo <> 0 Then
                    ddAnyoProce.Items.Clear()
                    ddAnyoProce.DataSource = listaAnyo
                    ddAnyoProce.DataBind()
                    If Not IsNothing(ddAnyoProce.Items.FindByText(anyo)) Then
                        ddAnyoProce.SelectedIndex = ddAnyoProce.Items.IndexOf(ddAnyoProce.Items.FindByText(anyo))
                        'Para los procesos, filtro por los de este AÑO
                        listaProce.Clear()
                        listaProce = From datos In oDsProce.Tables(0).Select("ANYO=" & anyo) Order By datos.Item("COD")
                                     Select New ListItem(datos.Item("COD"), datos.Item("COD")) Distinct.ToList()
                        listaProce.Insert(0, New ListItem(String.Empty, String.Empty))
                    End If
                End If

                ddCodProce.Items.Clear()
                ddCodProce.DataSource = listaProce
                ddCodProce.DataBind()
                If Proce <> 0 Then
                    If Not IsNothing(ddCodProce.Items.FindByText(Proce)) Then
                        ddCodProce.SelectedIndex = ddCodProce.Items.IndexOf(ddCodProce.Items.FindByText(Proce))
                    End If
                End If

            Case 3
                ddAnyoProce.Items.Clear()
                ddAnyoProce.DataSource = listaAnyo
                ddAnyoProce.DataBind()
                If anyo <> 0 Then
                    If Not IsNothing(ddAnyoProce.Items.FindByText(anyo)) Then
                        ddAnyoProce.SelectedIndex = ddAnyoProce.Items.IndexOf(ddAnyoProce.Items.FindByText(anyo))
                        'Para los procesos, filtro por los de este AÑO
                        listaGmn.Clear()
                        listaGmn = From datos In oDsProce.Tables(0).Select("ANYO=" & anyo) Order By datos.Item("GMN1")
                                   Select New ListItem(datos.Item("GMN1"), datos.Item("GMN1")) Distinct.ToList()
                        listaGmn.Insert(0, New ListItem(String.Empty, String.Empty))
                    End If
                End If

                ddGmnProce.Items.Clear()
                ddGmnProce.DataSource = listaProce
                ddGmnProce.DataBind()
                If Proce <> 0 Then
                    If Not IsNothing(ddGmnProce.Items.FindByText(gmn1)) Then
                        ddGmnProce.SelectedIndex = ddGmnProce.Items.IndexOf(ddGmnProce.Items.FindByText(gmn1))
                    End If
                End If
        End Select
    End Sub
#End Region
#Region "Eventos buscador"
    ''' <summary>
    ''' Cuando se modifica el ERP seleccionado, se refresca la combo de entidades con las entidades con integración activada para ese erp
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: Evento; Tiempo maximo: 0,1</remarks>
    Private Sub ddERP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddERP.SelectedIndexChanged
        ddEntidad.ClearSelection()
        'Se ocultan los criterios de selección
        pCodigo.Visible = False
        pCodigoERP.Visible = False
        pProceso.Visible = False
        pPedido.Visible = False
        pRecep.Visible = False

        If Not IsNothing(ddERP.SelectedItem) Then
            CargarEntidades(ddERP.SelectedItem.Value)
        End If
    End Sub
    ' ''' <summary>
    ' ''' Cuando se modifica la entidad seleccionada, se muestran unos u otros criterios de búsqueda
    ' ''' </summary>
    ' ''' <param name="sender"></param>
    ' ''' <param name="e"></param>
    ' ''' <remarks>Llamada desde: Evento; Tiempo maximo: 0,1</remarks>
    Private Sub ddEntidad_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddEntidad.SelectedIndexChanged
        'Se ocultan todos
        pCodigo.Visible = False
        pCodigoERP.Visible = False
        pProceso.Visible = False
        pPedido.Visible = False
        pRecep.Visible = False
        txtCodigo.Visible = False
        txtCodigoMask.Visible = False
        'Se limpian los controles
        ddAnyoPed.Items.Clear()
        txtNumPed.Text = ""
        txtNumOrden.Text = ""
        txtCodERP.Text = ""
        txtCodigoMask.Text = ""
        txtCodigo.Text = ""
        ddAnyoProce.Items.Clear()
        ddGmnProce.Items.Clear()
        ddCodProce.Items.Clear()

        txtAlbaran.Text = ""
        txtRecepERP.Text = ""

        If ddEntidad.SelectedItem.Value = "" Then
            ddEntidad.SelectedItem.Value = "0"
        End If
        Select Case CInt(ddEntidad.SelectedItem.Value)
            Case TablasIntegracion.art4, TablasIntegracion.Prove, TablasIntegracion.prove_ERP, TablasIntegracion.Activos, TablasIntegracion.Facturas
                pCodigo.Visible = True
                pCodigoERP.Visible = True
                txtCodigo.Visible = True
            Case TablasIntegracion.PM
                pCodigo.Visible = True
                pCodigoERP.Visible = True
                CargarMascara(TablasIntegracion.PM)
            Case TablasIntegracion.PED_Aprov, TablasIntegracion.PED_directo
                pPedido.Visible = True
                pCodigoERP.Visible = True
                CargarAnioPedido()
                txtCodigo.Visible = True
            Case TablasIntegracion.Rec_Directo, TablasIntegracion.Rec_Aprov
                pPedido.Visible = True
                pCodigoERP.Visible = True
                pRecep.Visible = True
                CargarAnioPedido()
                txtCodigo.Visible = True
            Case TablasIntegracion.Adj, TablasIntegracion.Proceso
                pProceso.Visible = True
                CargarTodosProcesos(ddERP.SelectedValue, ddEntidad.SelectedItem.Value)
                txtCodigo.Visible = True
            Case TablasIntegracion.Materiales
                pCodigo.Visible = True
                CargarMascara(TablasIntegracion.Materiales)
            Case TablasIntegracion.ProvePortal, TablasIntegracion.Certificado
                pCodigo.Visible = True
                txtCodigo.Visible = True
            Case 0, TablasIntegracion.PPM, TablasIntegracion.TasasServicio, TablasIntegracion.CargosProveedor, TablasIntegracion.ProveArticulo
                ''En estos casos no se muestra ninguna opción de filtrado adicional
            Case Else
                pCodigo.Visible = True
                txtCodigo.Visible = True
        End Select
    End Sub
    ''' <summary>
    ''' Carga la máscara para el código de la entidad Estructura de Materiales
    ''' </summary>
    ''' <param name="entidad">Entidad de integración actual.</param>
    ''' <remarks>Llamada desde: ddEntidad_SelectedIndexChanged; Tiempo maximo: 0,1
    '''          Revisado por aam (19/09/2013)</remarks>
    Private Sub CargarMascara(Optional ByRef entidad As Integer = -1)
        Dim oFSServer As FSNServer.Root = Session("FSN_Server")
        Dim oInt As FSNServer.VisorInt = oFSServer.Get_Object(GetType(FSNServer.VisorInt))
        Dim oDsLongGMN As DataSet
        Dim dr As DataRow
        Dim longGmn(4) As Object
        Dim i, o As Integer
        txtCodigoMask.Visible = True
        txtCodigoMask.PromptChar = " "

        If entidad = TablasIntegracion.Materiales Then
            i = 0
            oDsLongGMN = oInt.DevuelveLongitudGMN()
            For Each dr In oDsLongGMN.Tables(0).Rows
                longGmn(i) = dr(0).ToString
                i = i + 1
            Next
            For o = 0 To 3
                For i = 1 To longGmn(o)
                    txtCodigoMask.InputMask = txtCodigoMask.InputMask + "a"
                Next
                If o < 3 Then
                    txtCodigoMask.InputMask = txtCodigoMask.InputMask + "-"
                End If
            Next
            txtCodigoMask.DataMode = EditorControls.MaskDataMode.RawTextWithRequiredPrompts
        ElseIf entidad = TablasIntegracion.PM Then
            For i = 0 To 50
                txtCodigoMask.InputMask = txtCodigoMask.InputMask + "#"
            Next
        Else
            'Quitamos la máscara por defecto y ponemos otra muy permisiva.
            For i = 0 To 50
                txtCodigoMask.InputMask = txtCodigoMask.InputMask + "&"
            Next
        End If
        txtCodigoMask.DisplayMode = EditorControls.MaskDisplayMode.BlankWhenEmpty
    End Sub
    ''' <summary>
    ''' Al cambiar el año del proceso, recarga todos los GMN1 y códigos de proceso relacionados
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ddAnyoProce_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddAnyoProce.SelectedIndexChanged
        If Not ddAnyoProce.SelectedItem Is Nothing Then
            CargarProcesos(1)
        End If

    End Sub
    ''' <summary>
    ''' Evento que salta cuando se cambia el valor del combo de GMN1 de proceso. Carga todos los procesos asociados al GMN1 del proceso
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ddGmnProce_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddGmnProce.SelectedIndexChanged
        If Not ddGmnProce.SelectedItem Is Nothing Then
            CargarProcesos(2)

        End If
    End Sub
    Private Sub ddCodProce_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddCodProce.SelectedIndexChanged
        If Not ddCodProce.SelectedItem Is Nothing Then
            If ddCodProce.SelectedValue <> ddCodProce.SelectedItem.Text Then
                ddCodProce.ClearSelection()
                ddCodProce.SelectedValue = ""
            End If
        End If

        If Not ddCodProce.Items.FindByValue(e.ToString) Is Nothing Then
            Dim oItem As System.Web.UI.WebControls.ListItem = ddCodProce.Items.FindByValue(e.ToString)
            oItem.Selected = True
        Else
            Exit Sub
        End If
        Dim valor As String = ddCodProce.Items(ddCodProce.SelectedIndex).Value
        ddCodProce.SelectedValue = ddCodProce.Items(ddCodProce.SelectedIndex).Text
    End Sub
    ''' <summary>
    ''' Recarga la grid filtrando por los criterios de búsqueda y búsqueda avanzada
    ''' </summary>
    ''' <param name="sender">control</param>
    ''' <param name="e">evento de sistema</param>  
    ''' <remarks>Llamada desde: sistema; Tiempo máximo:0 </remarks>
    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarGeneral.Click
        CargaGrid()
        Me.whdgVisorInt.GridView.Behaviors.Paging.PageIndex = 0
        _pagerControl.SetCurrentPageNumber(0)
    End Sub
    Private Sub btnOk_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.ServerClick
        UpGridVisor.Update()
    End Sub
#End Region
#Region "Carga de datos BusquedaAvanzada"
    ''' <summary>
    ''' Propiedad que devuelve una lista de listitems con todos los sentidos 
    ''' </summary>
    Private ReadOnly Property SentidosInt() As List(Of ListItem)
        Get
            Dim _listaDeSentidos As New List(Of ListItem)
            _listaDeSentidos.Insert(0, New ListItem(String.Empty, String.Empty))
            _listaDeSentidos.Add(New ListItem(Textos(31), VisorInt.SentidoINT.Entrada))
            _listaDeSentidos.Add(New ListItem(Textos(32), VisorInt.SentidoINT.Salida))
            _listaDeSentidos.Add(New ListItem(Textos(33), VisorInt.SentidoINT.EntradaSalida))

            Return _listaDeSentidos
        End Get
    End Property
    ''' <summary>
    ''' Propiedad que devuelve una lista de listitems con todos las estados 
    ''' </summary>
    Private ReadOnly Property EstadosInt() As List(Of ListItem)
        Get
            Dim _listaDeEstados As New List(Of ListItem)
            _listaDeEstados.Insert(0, New ListItem(String.Empty, String.Empty))
            _listaDeEstados.Add(New ListItem(Textos(23), VisorInt.EstadoINT.Pendiente))
            _listaDeEstados.Add(New ListItem(Textos(24), VisorInt.EstadoINT.Enviado))
            _listaDeEstados.Add(New ListItem(Textos(25), VisorInt.EstadoINT.Correcto))
            _listaDeEstados.Add(New ListItem(Textos(26), VisorInt.EstadoINT.ErrorInt))
            _listaDeEstados.Add(New ListItem(Textos(104), VisorInt.EstadoINT.NoAplica))
            Return _listaDeEstados
        End Get
    End Property
    ''' <summary>
    ''' Propiedad que devuelve una lista de listitems con todos las acciones 
    ''' </summary>
    Private ReadOnly Property AccionesInt() As List(Of ListItem)
        Get
            Dim _listaDeAcciones As New List(Of ListItem)
            _listaDeAcciones.Insert(0, New ListItem(String.Empty, String.Empty))
            _listaDeAcciones.Add(New ListItem(Textos(27), "I"))
            _listaDeAcciones.Add(New ListItem(Textos(28), "U"))
            _listaDeAcciones.Add(New ListItem(Textos(29), "D"))
            _listaDeAcciones.Add(New ListItem(Textos(30), "4"))
            Return _listaDeAcciones
        End Get
    End Property
    ''' <summary>
    ''' Propiedad que devuelve una lista de listitems con todas las entidades 
    ''' </summary>
    Private ReadOnly Property EntidadesInt() As List(Of ListItem)
        Get
            Dim _listaDeEntidades As New List(Of ListItem)
            _listaDeEntidades.Add(New ListItem(Textos(35), TablasIntegracion.Mon))
            _listaDeEntidades.Add(New ListItem(Textos(36), TablasIntegracion.Pai))
            _listaDeEntidades.Add(New ListItem(Textos(37), TablasIntegracion.Provi))
            _listaDeEntidades.Add(New ListItem(Textos(38), TablasIntegracion.Pag))
            _listaDeEntidades.Add(New ListItem(Textos(39), TablasIntegracion.Dest))
            _listaDeEntidades.Add(New ListItem(Textos(40), TablasIntegracion.Uni))
            _listaDeEntidades.Add(New ListItem(Textos(41), TablasIntegracion.art4))
            _listaDeEntidades.Add(New ListItem(Textos(42), TablasIntegracion.Prove))
            _listaDeEntidades.Add(New ListItem(Textos(43), TablasIntegracion.Adj))
            _listaDeEntidades.Add(New ListItem(Textos(44), TablasIntegracion.con))
            _listaDeEntidades.Add(New ListItem(Textos(45), TablasIntegracion.prove_ERP))
            _listaDeEntidades.Add(New ListItem(Textos(46), TablasIntegracion.PED_Aprov))
            _listaDeEntidades.Add(New ListItem(Textos(47), TablasIntegracion.PED_directo))
            _listaDeEntidades.Add(New ListItem(Textos(48), TablasIntegracion.Rec_Aprov))
            _listaDeEntidades.Add(New ListItem(Textos(49), TablasIntegracion.Rec_Directo))
            _listaDeEntidades.Add(New ListItem(Textos(50), TablasIntegracion.Materiales))
            _listaDeEntidades.Add(New ListItem(Textos(51), TablasIntegracion.PresArt))
            _listaDeEntidades.Add(New ListItem(Textos(52), TablasIntegracion.VARCAL))
            _listaDeEntidades.Add(New ListItem(Textos(53), TablasIntegracion.PM))
            _listaDeEntidades.Add(New ListItem(Textos(54), TablasIntegracion.Presupuestos1))
            _listaDeEntidades.Add(New ListItem(Textos(55), TablasIntegracion.Presupuestos2))
            _listaDeEntidades.Add(New ListItem(Textos(56), TablasIntegracion.Presupuestos3))
            _listaDeEntidades.Add(New ListItem(Textos(57), TablasIntegracion.Presupuestos4))
            _listaDeEntidades.Add(New ListItem(Textos(58), TablasIntegracion.Proceso))
            _listaDeEntidades.Add(New ListItem(Textos(60), TablasIntegracion.ViaPag))
            _listaDeEntidades.Add(New ListItem(Textos(61), TablasIntegracion.PPM))
            _listaDeEntidades.Add(New ListItem(Textos(62), TablasIntegracion.TasasServicio))
            _listaDeEntidades.Add(New ListItem(Textos(63), TablasIntegracion.CargosProveedor))
            _listaDeEntidades.Add(New ListItem(Textos(64), TablasIntegracion.UnidadesOrganizativas))
            _listaDeEntidades.Add(New ListItem(Textos(65), TablasIntegracion.Usuarios))
            _listaDeEntidades.Add(New ListItem(Textos(66), TablasIntegracion.Activos))
            _listaDeEntidades.Add(New ListItem(Textos(67), TablasIntegracion.Facturas))
            _listaDeEntidades.Add(New ListItem(Textos(68), TablasIntegracion.Pagos))
            _listaDeEntidades.Add(New ListItem(Textos(69), TablasIntegracion.PRES5))
            _listaDeEntidades.Add(New ListItem(Textos(70), TablasIntegracion.CentroCoste))
            _listaDeEntidades.Add(New ListItem(Textos(71), TablasIntegracion.Empresa))
            _listaDeEntidades.Add(New ListItem(Textos(101), TablasIntegracion.ProvePortal))
            _listaDeEntidades.Add(New ListItem(Textos(102), TablasIntegracion.Certificado))
            _listaDeEntidades.Add(New ListItem(Textos(103), TablasIntegracion.ProveArticulo))
            Return _listaDeEntidades
        End Get
    End Property
#End Region
#Region "Eventos BusquedaAvanzada"
    Private Sub btnLimpiarBusquedaAvanzada_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiarBusquedaAvanzada.Click
        txtIdentificador.Text = ""
        ddEstado.ClearSelection()
        ddSentido.ClearSelection()
        ddAccion.ClearSelection()
        dteDesde.Value = Nothing
        dteHasta.Value = Nothing
        hrDesde.Value = Nothing
        hrHasta.Value = Nothing
    End Sub
#End Region
#Region "Carga Datos Visor"
    ''' <summary>
    ''' Carga el grid 
    ''' </summary>
    ''' <remarks>Llamada desde: Page_load      btnBuscar_Click; Tiempo maximo: 0,2</remarks>
    Private Sub ObtenerDatosVisor()

        Dim oInt As FSNServer.VisorInt = FSNServer.Get_Object(GetType(FSNServer.VisorInt))

        'Obtener filtros
        'Criterios búsqueda General
        Dim sERP As String
        Dim iEntidad As Integer
        Dim sCod As String
        Dim sCodERP As String
        Dim sAlbaranERP As String
        Dim iAnioPedido As Integer
        Dim iNumPedido As Integer
        Dim iNumOrden As Integer
        Dim iAnioProce As Integer
        Dim sGMN As String
        Dim iCodProce As Integer
        'Criterios búsqueda Avanzados
        Dim iIdentificador As Integer
        Dim sAccion As String
        Dim iEstado As Integer
        Dim iSentido As Integer
        Dim dFechaDesde As Date
        Dim dFechaHasta As Date
        Dim idioma As String

        'Recoge los criterios de búsqueda seleccionados
        If ddERP.SelectedItem IsNot Nothing Then
            sERP = IIf(ddERP.SelectedItem.Value <> String.Empty, ddERP.SelectedItem.Value, "")
        Else
            sERP = ""
        End If
        If ddEntidad.SelectedItem IsNot Nothing Then
            iEntidad = IIf(ddEntidad.SelectedItem.Value <> String.Empty, ddEntidad.SelectedItem.Value, -1)
        Else
            iEntidad = 0
        End If
        If (Not txtCodigo.Visible) Then
            sCod = IIf(txtCodigoMask.Text IsNot Nothing AndAlso txtCodigoMask.Text <> String.Empty, txtCodigoMask.Text, "")
        Else
            sCod = IIf(txtCodigo.Text IsNot Nothing AndAlso txtCodigo.Text <> String.Empty, txtCodigo.Text, "")
        End If
        sCod = Replace(sCod, " ", "")
        If sCod = "" Then  'para las recepciones
            sCod = IIf(txtAlbaran.Text IsNot Nothing AndAlso txtAlbaran.Text <> String.Empty, txtAlbaran.Text, "")
        End If
        sCodERP = IIf(txtCodERP.Text IsNot Nothing AndAlso txtCodERP.Text <> String.Empty, txtCodERP.Text, "")
        If ddAnyoPed.SelectedItem IsNot Nothing Then
            iAnioPedido = IIf(ddAnyoPed.SelectedItem.Value <> String.Empty, ddAnyoPed.SelectedItem.Value, -1)
        Else
            iAnioPedido = -1
        End If
        iNumPedido = IIf(txtNumPed.Text IsNot Nothing AndAlso txtNumPed.Text <> String.Empty, txtNumPed.Text, -1)
        iNumOrden = IIf(txtNumOrden.Text IsNot Nothing AndAlso txtNumOrden.Text <> String.Empty, txtNumOrden.Text, -1)
        If ddAnyoProce.SelectedItem IsNot Nothing Then
            iAnioProce = IIf(ddAnyoProce.SelectedItem.Text <> String.Empty, ddAnyoProce.SelectedItem.Text, -1)
        Else
            iAnioProce = -1
        End If
        If ddGmnProce.SelectedItem IsNot Nothing Then
            sGMN = IIf(ddGmnProce.SelectedItem.Text <> String.Empty, ddGmnProce.SelectedItem.Text, "")
        Else
            sGMN = ""
        End If
        If ddCodProce.SelectedItem IsNot Nothing Then
            iCodProce = IIf(ddCodProce.SelectedItem.Text <> String.Empty, ddCodProce.SelectedItem.Text, -1)
        Else
            iCodProce = -1
        End If
        iIdentificador = IIf(txtIdentificador IsNot Nothing AndAlso txtIdentificador.Text <> String.Empty, txtIdentificador.Text, -1)
        If ddAccion.SelectedItem IsNot Nothing Then
            sAccion = IIf(ddAccion.SelectedItem.Value <> String.Empty, ddAccion.SelectedItem.Value, "")
        Else
            sAccion = ""
        End If
        If ddEstado.SelectedItem IsNot Nothing Then
            iEstado = IIf(ddEstado.SelectedItem.Value <> String.Empty, ddEstado.SelectedItem.Value, -1)
        Else
            iEstado = -1
        End If
        If ddSentido.SelectedItem IsNot Nothing Then
            iSentido = IIf(ddSentido.SelectedItem.Value <> String.Empty, ddSentido.SelectedItem.Value, -1)
        Else
            iSentido = -1
        End If
        dFechaDesde = Nothing
        dFechaDesde = IIf(dteDesde.Value Is Nothing Or dteDesde.Value Is DBNull.Value, Nothing, dteDesde.Value)
        If hrDesde.Value = Nothing Or hrDesde.Value Is DBNull.Value Then
        Else
            dFechaDesde = DateAdd(DateInterval.Hour, Hour(hrDesde.Value), dFechaDesde)
            dFechaDesde = DateAdd(DateInterval.Minute, Minute(hrDesde.Value), dFechaDesde)
        End If

        'dFechaDesde = DateToBDDate(dFechaDesde)

        dFechaHasta = Nothing
        dFechaHasta = IIf(dteHasta.Value Is Nothing Or dteHasta.Value Is DBNull.Value, Nothing, dteHasta.Value)
        If hrHasta.Value = Nothing Or hrHasta.Value Is DBNull.Value Then
        Else
            dFechaHasta = DateAdd(DateInterval.Hour, Hour(hrHasta.Value), dFechaHasta)
            dFechaHasta = DateAdd(DateInterval.Minute, Minute(hrHasta.Value), dFechaHasta)
        End If

        'dFechaHasta = DateToBDDate(dFechaHasta)
        sAlbaranERP = IIf(txtRecepERP.Text IsNot Nothing AndAlso txtRecepERP.Text <> String.Empty, txtRecepERP.Text, "")

        'Idioma del usuario
        If String.IsNullOrEmpty(FSNUser.Idioma.ToString()) Then
            idioma = "SPA"
        Else
            idioma = FSNUser.Idioma.ToString()
        End If

        Dim oDatasetVisor As DataSet
        oDatasetVisor = oInt.CargaVisor(sERP, iEntidad, iAnioProce, sGMN, iCodProce, sCod, sCodERP, sAlbaranERP, iAnioPedido, iNumPedido, iNumOrden, iIdentificador, sAccion, iEstado, iSentido, dFechaDesde, dFechaHasta, FSNUser.DateFormat.ShortDatePattern, idioma)

        Me.InsertarEnCache("oDsVisorInt_" & FSNUser.Cod, oDatasetVisor, CacheItemPriority.BelowNormal)
    End Sub
    Private Sub CargaGrid()
        whdgVisorInt.Rows.Clear()
        whdgVisorInt.GridView.ClearDataSource()
        whdgVisorInt.DataSource = DatasetVisor
        whdgVisorInt.GridView.DataSource = whdgVisorInt.DataSource


        'Obtener coincidencias de tipo destino en tipo wcg, si no hay ninguna ocultamos la columna de relanzamiento
        Dim dtTemp As DataRow()
        dtTemp = whdgVisorInt.DataSource.Tables(0).Select("RELANZAR= 1")

        Dim IndexRelanzar As Integer = whdgVisorInt.Columns.FromKey("RELANZAR").Index

        If dtTemp.Count = 0 Or Not Me.FSNUser.INTRestriccion_RelanzarIntegracion Then
            Me.whdgVisorInt.Columns(IndexRelanzar).Hidden = True
            Me.whdgVisorInt.GridView.Columns(IndexRelanzar).Hidden = True
        Else
            Me.whdgVisorInt.Columns(IndexRelanzar).Hidden = False
            Me.whdgVisorInt.GridView.Columns(IndexRelanzar).Hidden = False
        End If
        Me.whdgVisorInt.GridView.Columns("TEXTO").Hidden = True
        'Añadir campos al webdatagrid
        ConfiguracionGridVisor()

        whdgVisorInt.DataMember = ""

        whdgVisorInt.DataBind()

        whdgVisorInt.GridView.DataMember = ""
        whdgVisorInt.GridView.DataBind()

        UpGridVisor.Update()
    End Sub
    Private Sub CargarHistorico()
        If Session("_dsHistorico") Is Nothing Then
            _dsHistorico = WebScheduleProviderBinding()
            Session("_dsHistorico") = _dsHistorico
            'Session("ActiveDayUtc") = schInfo.ActiveDayUtc
        Else
            _dsHistorico = Session("_dsHistorico")
            'schInfo.ActiveDayUtc = Session("ActiveDayUtc")
        End If

        'wschData.ActivityDataSource = _dsHistorico
        'wschData.ResourceDataSource = _dsHistorico
        'wschData.VarianceDataSource = _dsHistorico

        'schDayView.WebScheduleInfo = schInfo
        'sch5DaysView.WebScheduleInfo = schInfo
        'schMonthView.WebScheduleInfo = schInfo
        'schCalendarView.WebScheduleInfo = schInfo
    End Sub
    ' ''' <summary>
    ' ''' </summary>
    ' ''' <remarks></remarks>
    Private Sub ConfiguracionGridVisor()
        Me.ConfigurarColumna("ERP", Textos(1))
        Me.ConfigurarColumna("ENTIDAD", Textos(2))
        Me.ConfigurarColumna("IDENTIF", Textos(7))
        Me.ConfigurarColumna("ESTADO", Textos(9))
        Me.ConfigurarColumna("FECENV", Textos(13))
        Me.ConfigurarColumna("FECREC", Textos(14))
        Me.ConfigurarColumna("ACCION", Textos(8))
        Me.ConfigurarColumna("COD", Textos(5))
        Me.ConfigurarColumna("COD_ERP", Textos(6))
        Me.ConfigurarColumna("USU", Textos(15))
        Me.ConfigurarColumna("INFO", Textos(79))
        Me.ConfigurarColumna("NOM_FICHERO", Textos(16))
        Me.ConfigurarColumna("NOM_RECIBO", Textos(103))
        Me.ConfigurarColumna("ORIGEN", Textos(10)) 'El campo es origen pero el texto de la cabecera es Sentido (tarea 3022)
        Me.ConfigurarColumna("RELANZAR", Me.Textos(81))
        Me.ConfigurarColumna("TEXTO", Me.Textos(80))
        Me.whdgVisorInt.GroupingSettings.EmptyGroupAreaText = Textos(77)
    End Sub
    ' ''' Revisado por: ngo. Fecha: 17/05/2012
    ' ''' <summary>
    ' ''' Función que añade los textos a las cabeceras de las columnas y a los filtros
    ' ''' </summary>
    ' ''' <param name="fieldName">Nombre del campo de la grid</param>
    ' ''' <param name="headerText">Título de la columna</param>
    ' ''' <param name="headerTooltip">Tooltip de la columna</param>
    ' ''' <remarks>Llamada desde ConfiguracionGridVisor . Máx. 0,1 seg.</remarks>
    Private Sub ConfigurarColumna(ByVal fieldName As String, ByVal headerText As String, Optional ByVal headerTooltip As String = "")
        'Configurar el grid
        Dim fieldSetting As New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        Dim groupSetting As New Infragistics.Web.UI.GridControls.ColumnGroupingSetting

        If Not whdgVisorInt.GridView.Columns.Item(fieldName) Is Nothing Then
            With whdgVisorInt.GridView.Columns.Item(fieldName)
                .Header.Text = headerText
                If headerTooltip = String.Empty Then headerTooltip = headerText
                .Header.Tooltip = headerTooltip
            End With
            If whdgVisorInt.Columns.Item(fieldName) IsNot Nothing Then
                whdgVisorInt.Columns.Item(fieldName).Header.Text = headerText
                If headerTooltip = String.Empty Then headerTooltip = headerText
                whdgVisorInt.Columns.Item(fieldName).Header.Tooltip = headerTooltip
                If fieldName = "FECENV" Or fieldName = "FECREC" Then
                    If fieldName = "FECENV" Then
                        groupSetting.ColumnKey = "FECENV"
                    ElseIf fieldName = "FECREC" Then
                        groupSetting.ColumnKey = "FECREC"
                    End If
                    groupSetting.GroupComparer = New DateGroupComparer
                    whdgVisorInt.GroupingSettings.ColumnSettings.Add(groupSetting)
                End If
            End If
            fieldSetting.ColumnKey = fieldName
            fieldSetting.BeforeFilterAppliedAltText = Textos(78) 'Filtro no aplicado
            Me.whdgVisorInt.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
            Me.whdgVisorInt.GridView.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        End If
    End Sub
    Public Class DateGroupComparer
        Inherits GroupEqualityComparer(Of Object)

        Public Overrides Function Equals(x As Object, y As Object) As Boolean
            If IsDBNull(x) And IsDBNull(y) Then
                Return True
            ElseIf IsDBNull(x) Or IsDBNull(y) Then
                Return False
            ElseIf x.Date = y.Date Then
                Return True
            Else
                Return False
            End If
        End Function
        Public Overrides Function GroupName(value As Object) As String
            If IsDBNull(value) Then
                Return " "
            Else
                Return value.Date
            End If
        End Function
    End Class
#End Region
#Region "EventosVisor"
    ''' <summary>
    ''' Ajusta los valores de página y número de página
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Administrar_Paginador()
        If (_pagerControl.MaxPage <> whdgVisorInt.GridView.Behaviors.Paging.PageCount) Then
            _pagerControl.SetupPageList(whdgVisorInt.GridView.Behaviors.Paging.PageCount)
        End If
        If Not Request("__EVENTTARGET") = _pagerControl.FindControl("PagerPageList").UniqueID Then
            _pagerControl.SetCurrentPageNumber(whdgVisorInt.GridView.Behaviors.Paging.PageIndex)
        End If
    End Sub
    ''' <summary>
    ''' Tras la carga del grid se "sincroniza" con el Custom Pager
    ''' </summary>
    ''' <param name="sender">control</param>
    ''' <param name="e">evento de sistema</param>  
    ''' <remarks>Llamada desde: sistema; Tiempo máximo:0 </remarks>
    Private Sub whdgVisorInt_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles whdgVisorInt.DataBound
        Administrar_Paginador()
        UpGridVisor.Update()
    End Sub
    ''' <summary>
    ''' Tras la carga del grid se "sincroniza" con el Custom Pager
    ''' </summary>
    ''' <param name="sender">control</param>
    ''' <param name="e">evento de sistema</param>  
    ''' <remarks>Llamada desde: sistema; Tiempo mÃ¡ximo:0 </remarks>
    Private Sub whdgVisorInt_DataFiltered(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.FilteredEventArgs) Handles whdgVisorInt.DataFiltered
        Administrar_Paginador()
        UpGridVisor.Update()
    End Sub
    ''' <summary>
    ''' Tras la carga del grid se "sincroniza" con el Custom Pager
    ''' </summary>
    ''' <param name="sender">control</param>
    ''' <param name="e">evento de sistema</param>  
    ''' <remarks>Llamada desde: sistema; Tiempo mÃ¡ximo:0 </remarks>
    Private Sub whdgVisorInt_GroupedColumnsChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.GroupedColumnsChangedEventArgs) Handles whdgVisorInt.GroupedColumnsChanged
        Administrar_Paginador()
        UpGridVisor.Update()
    End Sub
    Private Sub whdgVisorInt_GroupedColumnsChanging(sender As Object, e As Infragistics.Web.UI.GridControls.GroupedColumnsChangingEventArgs) Handles whdgVisorInt.GroupedColumnsChanging
    End Sub
    ''' <summary>
    ''' An extension method that replaces the first occurance of a specified string.
    ''' </summary>
    ''' <param name="str"></param>
    ''' <param name="searchTxt"></param>
    ''' <param name="replaceTxt"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ReplaceFirst(ByVal str As String, searchTxt As String, replaceTxt As String) As String
        Dim pos As Integer = str.IndexOf(searchTxt)

        If pos < 0 Then
            Return str
        End If

        Return str.Substring(0, pos) + replaceTxt + str.Substring(pos + searchTxt.Length)
    End Function
    Private Sub whdgVisorInt_GroupedRowInitialized(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.GroupedRowEventArgs) Handles whdgVisorInt.GroupedRowInitialized

        If IsDBNull(e.GroupedRow.Value) Then
            e.GroupedRow.Text = " "
        End If

        If e.GroupedRow.ColumnGroupedBy.Key = "ACCION" Then
            Select Case e.GroupedRow.Value
                Case "I"  ''Alta
                    e.GroupedRow.Text = e.GroupedRow.Text.Replace("I", Textos(27))
                Case "U"  ''ModificaciÃ³n
                    e.GroupedRow.Text = e.GroupedRow.Text.Replace("U", Textos(28))
                Case "R"  ''ModificaciÃ³n
                    e.GroupedRow.Text = e.GroupedRow.Text.Replace("R", Textos(28))
                Case "D" ''Borrado
                    e.GroupedRow.Text = e.GroupedRow.Text.Replace("D", Textos(29))
                Case "4" ''ReubicaciÃ³n
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, "4", Textos(30))
            End Select
        ElseIf e.GroupedRow.ColumnGroupedBy.Key = "SENTIDO" Then
            Select Case e.GroupedRow.Value
                Case "1"  ''Salida
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, "1", Textos(32))
                Case "2"  ''Entrada
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, "2", Textos(31))
                Case "3"  ''Entrada y salida
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, "3", Textos(33))
            End Select
        ElseIf e.GroupedRow.ColumnGroupedBy.Key = "ENTIDAD" Then
            Select Case e.GroupedRow.Value
                Case TablasIntegracion.Mon
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.Mon, Textos(34))
                Case TablasIntegracion.Pai
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.Pai, Textos(35))
                Case TablasIntegracion.Provi
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.Provi, Textos(36))
                Case TablasIntegracion.Pag
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.Pag, Textos(37))
                Case TablasIntegracion.Dest
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.Dest, Textos(38))
                Case TablasIntegracion.Uni
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.Uni, Textos(39))
                Case TablasIntegracion.art4
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.art4, Textos(40))
                Case TablasIntegracion.Prove
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.Prove, Textos(41))
                Case TablasIntegracion.Adj
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.Adj, Textos(42))
                Case TablasIntegracion.con
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.con, Textos(43))
                Case TablasIntegracion.prove_ERP
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.prove_ERP, Textos(44))
                Case TablasIntegracion.PED_Aprov
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.PED_Aprov, Textos(45))
                Case TablasIntegracion.Rec_Aprov
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.Rec_Aprov, Textos(47))
                Case TablasIntegracion.PED_directo
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.PED_directo, Textos(46))
                Case TablasIntegracion.Rec_Directo
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.Rec_Directo, Textos(48))
                Case TablasIntegracion.Materiales
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.Materiales, Textos(49))
                Case TablasIntegracion.PresArt
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.PresArt, Textos(50))
                Case TablasIntegracion.VARCAL
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.VARCAL, Textos(51))
                Case TablasIntegracion.PM
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.PM, Textos(52))
                Case TablasIntegracion.Presupuestos1
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.Presupuestos1, Textos(53))
                Case TablasIntegracion.Presupuestos2
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.Presupuestos2, Textos(54))
                Case TablasIntegracion.Presupuestos3
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.Presupuestos3, Textos(55))
                Case TablasIntegracion.Presupuestos4
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.Presupuestos4, Textos(56))
                Case TablasIntegracion.Proceso
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.Proceso, Textos(57))
                Case TablasIntegracion.ViaPag
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.ViaPag, Textos(59))
                Case TablasIntegracion.PPM
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.PPM, Textos(60))
                Case TablasIntegracion.TasasServicio
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.TasasServicio, Textos(61))
                Case TablasIntegracion.CargosProveedor
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.CargosProveedor, Textos(62))
                Case TablasIntegracion.UnidadesOrganizativas
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.UnidadesOrganizativas, Textos(63))
                Case TablasIntegracion.Usuarios
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.Usuarios, Textos(64))
                Case TablasIntegracion.Activos
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.Activos, Textos(65))
                Case TablasIntegracion.Facturas
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.Facturas, Textos(66))
                Case TablasIntegracion.Pagos
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.Pagos, Textos(67))
                Case TablasIntegracion.PRES5
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.PRES5, Textos(68))
                Case TablasIntegracion.CentroCoste
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.CentroCoste, Textos(69))
                Case TablasIntegracion.Empresa
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.Empresa, Textos(70))
                Case TablasIntegracion.ProvePortal
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.ProvePortal, Textos(100))
                Case TablasIntegracion.Certificado
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.Certificado, Textos(101))
                Case TablasIntegracion.ProveArticulo
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, TablasIntegracion.ProveArticulo, Textos(102))
            End Select
        ElseIf e.GroupedRow.ColumnGroupedBy.Key = "ESTADO" Then
            Select Case e.GroupedRow.Value
                Case EstadoIntegracion.ParcialmenteIntegrado
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, EstadoIntegracion.ParcialmenteIntegrado, Textos(23))
                Case EstadoIntegracion.EsperaAcuseRecibo
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, EstadoIntegracion.EsperaAcuseRecibo, Textos(24))
                Case EstadoIntegracion.ConError
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, EstadoIntegracion.ConError, Textos(26))
                Case EstadoIntegracion.TotalmenteIntegrado
                    e.GroupedRow.Text = ReplaceFirst(e.GroupedRow.Text, EstadoIntegracion.TotalmenteIntegrado, Textos(25))
            End Select
        End If
    End Sub
    ''' <summary>
    ''' Asigna los textos a las defierentes acciones y entidades, y, en el caso del estado de integración,
    ''' asigna el texto, la imagen y el formato correspondiente
    ''' </summary>
    ''' <param name="sender">control</param>
    ''' <param name="e">evento de sistema</param>  
    ''' <remarks>Llamada desde: sistema; Tiempo maximo:0.1 </remarks>
    Private Sub wdgVisorInt_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgVisorInt.InitializeRow
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim UrlImage As String = ""
        Dim Index As Integer = sender.Columns.FromKey("ESTADO").Index
        Dim IndexIdentificador As Integer = sender.columns.fromkey("IDENTIF").Index
        Dim IndexAcc As Integer = sender.Columns.FromKey("ACCION").Index
        Dim IndexEntidad As Integer = sender.Columns.FromKey("ENTIDAD").Index
        Dim IndexSentido As Integer = sender.Columns.FromKey("ORIGEN").Index
        Dim IndexRelanzar As Integer = sender.Columns.FromKey("RELANZAR").Index
        Dim indexFechaEnvio As Integer = sender.columns.fromkey("FECENV").Index
        Dim indexFechaRecepcion As Integer = sender.columns.fromkey("FECREC").Index

        Dim sTexto As String = "", sTextoAccion As String = "", sTextoEntidad As String = "", sTextoSentido As String = "", sTextoOrigen As String = ""

        'formateo de columna Relanzar
        e.Row.Items.Item(IndexRelanzar).Text = ""
        'comprobar si el usario tiene permisos
        If Me.FSNUser.INTRestriccion_RelanzarIntegracion() Then
            Dim mostrar As Boolean
            e.Row.Items.Item(IndexRelanzar).Text = ""
            If e.Row.Items.Item(IndexRelanzar).Value = "1" Then
                mostrar = True
                If e.Row.Items(Index).Value = "1" Then 'si ya enviado
                    'Si ha sido enviado comprobar que el tiempo del ultimo envío no sea menor del configurado
                    ' en webconfig.
                    Dim fechaEnvio As Date = e.Row.Items(indexFechaEnvio).Value
                    Dim ahora As Date = Now
                    If ahora.Subtract(fechaEnvio).TotalMinutes <= System.Configuration.ConfigurationManager.AppSettings.Item("minRelanzarINT") Then
                        mostrar = False
                    End If
                End If
            End If
            If mostrar Then
                Dim oIntegracion As Integracion = FSNServer.Get_Object(GetType(FSNServer.Integracion))
                Dim iCodERP As Integer = oIntegracion.getErpFromDen(e.Row.Items(sender.columns.fromKey("ERP").Index).Value)

                e.Row.Items.Item(IndexRelanzar).Text = "<input type='button' class='botonRedondeado' value='" & Me.Textos(81) & "' onclick='javascript:confirmaRelanzar(" & e.Row.Items(IndexIdentificador).Value & "," & e.Row.Items(IndexEntidad).Value & ", " & iCodERP & ")'/>"
            End If
        Else
            Me.whdgVisorInt.Columns("RELANZAR").Hidden = True
        End If

        'Formatea la columna ACCION
        Select Case e.Row.Items.Item(IndexAcc).Value
            Case "I"
                sTextoAccion = Textos(27)
            Case "U"
                sTextoAccion = Textos(28)
            Case "R"
                sTextoAccion = Textos(28)
            Case "D"
                sTextoAccion = Textos(29)
            Case "4"
                sTextoAccion = Textos(30)
        End Select
        e.Row.Items.Item(IndexAcc).Text = sTextoAccion

        'Formatea la columna ENTIDAD
        Select Case e.Row.Items.Item(IndexEntidad).Value
            Case TablasIntegracion.Mon
                sTextoEntidad = Textos(34)
            Case TablasIntegracion.Pai
                sTextoEntidad = Textos(35)
            Case TablasIntegracion.Provi
                sTextoEntidad = Textos(36)
            Case TablasIntegracion.Pag
                sTextoEntidad = Textos(37)
            Case TablasIntegracion.Dest
                sTextoEntidad = Textos(38)
            Case TablasIntegracion.Uni
                sTextoEntidad = Textos(39)
            Case TablasIntegracion.art4
                sTextoEntidad = Textos(40)
            Case TablasIntegracion.Prove
                sTextoEntidad = Textos(41)
            Case TablasIntegracion.Adj
                sTextoEntidad = Textos(42)
            Case TablasIntegracion.con
                sTextoEntidad = Textos(43)
            Case TablasIntegracion.prove_ERP
                sTextoEntidad = Textos(44)
            Case TablasIntegracion.PED_Aprov
                sTextoEntidad = Textos(45)
            Case TablasIntegracion.Rec_Aprov
                sTextoEntidad = Textos(47)
            Case TablasIntegracion.PED_directo
                sTextoEntidad = Textos(46)
            Case TablasIntegracion.Rec_Directo
                sTextoEntidad = Textos(48)
            Case TablasIntegracion.Materiales
                sTextoEntidad = Textos(49)
            Case TablasIntegracion.PresArt
                sTextoEntidad = Textos(50)
            Case TablasIntegracion.VARCAL
                sTextoEntidad = Textos(51)
            Case TablasIntegracion.PM
                sTextoEntidad = Textos(52)
            Case TablasIntegracion.Presupuestos1
                sTextoEntidad = Textos(53)
            Case TablasIntegracion.Presupuestos2
                sTextoEntidad = Textos(54)
            Case TablasIntegracion.Presupuestos3
                sTextoEntidad = Textos(55)
            Case TablasIntegracion.Presupuestos4
                sTextoEntidad = Textos(56)
            Case TablasIntegracion.Proceso
                sTextoEntidad = Textos(57)
            Case TablasIntegracion.ViaPag
                sTextoEntidad = Textos(59)
            Case TablasIntegracion.PPM
                sTextoEntidad = Textos(60)
            Case TablasIntegracion.TasasServicio
                sTextoEntidad = Textos(61)
            Case TablasIntegracion.CargosProveedor
                sTextoEntidad = Textos(62)
            Case TablasIntegracion.UnidadesOrganizativas
                sTextoEntidad = Textos(63)
            Case TablasIntegracion.Usuarios
                sTextoEntidad = Textos(64)
            Case TablasIntegracion.Activos
                sTextoEntidad = Textos(65)
            Case TablasIntegracion.Facturas
                sTextoEntidad = Textos(66)
            Case TablasIntegracion.Pagos
                sTextoEntidad = Textos(67)
            Case TablasIntegracion.PRES5
                sTextoEntidad = Textos(68)
            Case TablasIntegracion.CentroCoste
                sTextoEntidad = Textos(69)
            Case TablasIntegracion.Empresa
                sTextoEntidad = Textos(70)
            Case TablasIntegracion.ProvePortal
                sTextoEntidad = Textos(100)
            Case TablasIntegracion.Certificado
                sTextoEntidad = Textos(101)
            Case TablasIntegracion.ProveArticulo
                sTextoEntidad = Textos(102)
        End Select
        e.Row.Items.Item(IndexEntidad).Text = sTextoEntidad

        'Formatea la columna ESTADO
        'En caso de que se le llame desde la exportación a excel o pdf, en el texto de cada celda incluye únicamente el dato (sin aplicar formatos)
        Select Case e.Row.Items.Item(Index).Value
            Case "0"
                If Request("__EVENTTARGET") = "ExportarExcel" OrElse Request("__EVENTTARGET") = "ExportarPDF" Then
                    e.Row.Items.Item(Index).Text = Textos(23)
                Else
                    UrlImage = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/Ico_Pendiente.gif"
                    e.Row.Items.Item(Index).CssClass = "EstadoIntPendiente"
                    e.Row.Items.Item(Index).Text = "<img src='" & UrlImage & "' style='text-align:center;   '/>" & Textos(23)
                End If
            Case "1"
                If Request("__EVENTTARGET") = "ExportarExcel" OrElse Request("__EVENTTARGET") = "ExportarPDF" Then
                    e.Row.Items.Item(Index).Text = Textos(24)
                Else
                    UrlImage = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/Ico_Opcional.gif"
                    e.Row.Items.Item(Index).CssClass = "EstadoIntEnviado"
                    e.Row.Items.Item(Index).Text = "<img src='" & UrlImage & "' style='text-align:center;   '/>" & Textos(24)
                End If
            Case "2"
                If Request("__EVENTTARGET") = "ExportarExcel" OrElse Request("__EVENTTARGET") = "ExportarPDF" Then
                    e.Row.Items.Item(Index).Text = Textos(104)
                Else
                    UrlImage = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/Ico_Opcional.gif"
                    e.Row.Items.Item(Index).CssClass = "EstadoIntEnviado"
                    e.Row.Items.Item(Index).Text = "<img src='" & UrlImage & "' style='text-align:center;   '/>" & Textos(104)
                End If
            Case "3"
                If Request("__EVENTTARGET") = "ExportarExcel" OrElse Request("__EVENTTARGET") = "ExportarPDF" Then
                    e.Row.Items.Item(Index).Text = Textos(26)
                Else
                    UrlImage = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/Ico_Expirado.gif"
                    e.Row.Items.Item(Index).CssClass = "EstadoIntError"
                    e.Row.Items.Item(Index).Text = "<img src='" & UrlImage & "' style='text-align:center;   '/>" & Textos(26)
                End If
            Case "4"
                If Request("__EVENTTARGET") = "ExportarExcel" OrElse Request("__EVENTTARGET") = "ExportarPDF" Then
                    e.Row.Items.Item(Index).Text = Textos(25)
                Else
                    UrlImage = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/Ico_Vigente.gif"
                    e.Row.Items.Item(Index).CssClass = "EstadoIntOK"
                    e.Row.Items.Item(Index).Text = "<img src='" & UrlImage & "' style='text-align:center;   '/>" & Textos(25)
                End If
        End Select

        'Formatea la columna origen (etiquetada como "Destino")
        Select Case e.Row.Items.Item(IndexSentido).Value
            Case 0, 2
                sTextoOrigen = Textos(32)
            Case 3
                sTextoOrigen = Textos(31)
        End Select
        e.Row.Items.Item(IndexSentido).Text = sTextoOrigen

        'For Each item As Object In whdgVisorInt.GridView.Columns
        '    If item.Type = System.Type.GetType("System.DateTime") Then
        '        gridCellIndex = whdgVisorInt.GridView.Columns.FromKey(item.Key).Index
        '        CType(Items(gridCellIndex).Column, Infragistics.Web.UI.GridControls.BoundDataField).DataFormatString = "{0:" & FSNUser.DateFormat.ShortDatePattern & "}"
        '    End If
        'Next

        'Formatea la columna FECHA ENVIO
        If e.Row.Items.Item(indexFechaEnvio).Value.ToString() <> "" Then
            e.Row.Items.Item(indexFechaEnvio).Text = Format(e.Row.Items.Item(indexFechaEnvio).Value, FSNUser.DateFormat.ShortDatePattern) & " " & Format(e.Row.Items.Item(indexFechaEnvio).Value, FSNUser.DateFormat.ShortTimePattern)
        End If

        'Formatea la columna FECHA RECEPCION
        If e.Row.Items.Item(indexFechaRecepcion).Value.ToString() <> "" Then
            e.Row.Items.Item(indexFechaRecepcion).Text = Format(e.Row.Items.Item(indexFechaRecepcion).Value, FSNUser.DateFormat.ShortDatePattern) & " " & Format(e.Row.Items.Item(indexFechaRecepcion).Value, FSNUser.DateFormat.ShortTimePattern)
        End If

    End Sub
#End Region
#Region "Paginador"
    ''' <summary>
    ''' Cuando con la paginacion customizada cambia de pagina, el grid debe actualizarse a la pagina indicada.
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>    
    ''' <remarks>Llamada desde: wucPagercOntrol.ascx; Tiempo máximo:0,1seg.</remarks>
    Private Sub currentPageControl_PageChanged(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        whdgVisorInt.GridView.Behaviors.Paging.PageIndex = e.PageNumber
        UpGridVisor.Update()
    End Sub
#End Region
#Region "Exportacion"
    ''' <summary>
    ''' Exporta a Excel
    ''' </summary>
    ''' <remarks>Llamada desde: Page_load; Tiempo máximo:0,1</remarks>
    Private Sub ExportarExcel()
        Dim fileName As String = HttpUtility.UrlEncode(FSNPageHeader.TituloCabecera)
        fileName = fileName.Replace("+", "%20")

        With wdgExcelExporter
            .DownloadName = fileName
            .DataExportMode = DataExportMode.AllDataInDataSource
            Me.whdgVisorInt.GridView.Columns("TEXTO").Hidden = False
            .Export(whdgVisorInt)
            Me.whdgVisorInt.GridView.Columns("TEXTO").Hidden = True
        End With
    End Sub
    Private Sub wdgExcelExporter_Exported(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.ExcelExportedEventArgs) Handles wdgExcelExporter.Exported
        e.Worksheet.Name = Textos(0)
    End Sub
    ''' <summary>
    ''' Formatea los valores de las columnas para que exporte los textos (y no los identificadores de la acción, entidad, etc). 
    ''' Aplica a las fechas el formato del usuario.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub wdgExcelExporter_GridRecordItemExported(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.GridRecordItemExportedEventArgs) Handles wdgExcelExporter.GridRecordItemExported
        ''Formatear las columnas 
        Select Case e.GridCell.Column.Key
            Case "ACCION"
                e.WorksheetCell.Value = e.GridCell.Text
            Case "ENTIDAD"
                e.WorksheetCell.Value = e.GridCell.Text
            Case "ESTADO"
                e.WorksheetCell.Value = e.GridCell.Text
            Case "ORIGEN" 'EL campo es Origen pero la etiqueta a mostrar es destino
                e.WorksheetCell.Value = e.GridCell.Text
            Case Else
                Select Case e.GridCell.Column.Type
                    Case System.Type.GetType("System.DateTime")
                        e.WorksheetCell.CellFormat.FormatString = FSNUser.DateFormat.ShortDatePattern
                    Case Else
                End Select
        End Select
    End Sub
    ''' <summary>
    ''' Exporta a PDF
    ''' </summary>
    ''' <remarks>Llamada desde: Page_load; Tiempo máximo:0,1</remarks>
    Private Sub ExportarPDF()
        Dim fileName As String = HttpUtility.UrlEncode(FSNPageHeader.TituloCabecera)
        fileName = fileName.Replace("+", "%20")
        wdgPDFExporter.DownloadName = fileName

        With wdgPDFExporter
            .EnableStylesExport = True

            .DataExportMode = DataExportMode.AllDataInDataSource
            .TargetPaperOrientation = PageOrientation.Landscape
            .TargetPaperSize = PageSizes.GetPageSize("A4")
            .Margins = PageMargins.GetPageMargins("Narrow")
            Me.whdgVisorInt.GridView.Columns("TEXTO").Hidden = False
            .Export(whdgVisorInt)
            Me.whdgVisorInt.GridView.Columns("TEXTO").Hidden = True
        End With
    End Sub
    ''' <summary>
    ''' Aplica el formato del usuario a las fechas exportadas
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub wdgPDFExporter_GridRecordItemExporting(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.DocumentGridRecordItemExportingEventArgs) Handles wdgPDFExporter.GridRecordItemExporting

        Select Case e.GridCell.Column.Type
            Case System.Type.GetType("System.DateTime")
                If IsDBNull(e.ExportValue) Then
                    e.ExportValue = ""
                Else
                    e.ExportValue = FormatDate(e.ExportValue, FSNUser.DateFormat)
                End If

        End Select
    End Sub
#End Region
    ''' <summary>
    ''' Devuelve un array de dos posiciones idLogGeneral,idLogEntidad correspondientes a los registros insertados
    ''' </summary>
    ''' <param name="idLog">id de log general a relanzar</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function relanzarProveedor(ByVal idLog As Integer) As Integer()
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oIntegracion As Integracion = FSNServer.Get_Object(GetType(FSNServer.Integracion))
            Return oIntegracion.relanzarIntegracion(idLog, FSNUser.Cod)
        Catch e As Exception
            Throw e
        End Try
    End Function
    ''' <summary>
    ''' Llamada a server para realizar función de exportación del registro de integración.
    ''' </summary>
    ''' <param name="idEntidad">id del registro insertado en la entidad relanzada</param>
    ''' <param name="idlog">id del registro insertado en el log general del relanzamiento</param>
    ''' <param name="iErp">CÃ³digo del erp sobre el que se realiza la integraciÃ³n</param>
    ''' <remarks>llamada desde VisorIntegracion.aspx</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Sub exportar(ByVal idEntidad As Integer, ByVal idLog As Integer, ByVal iErp As Integer)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim oIntegracion As Integracion = FSNServer.Get_Object(GetType(FSNServer.Integracion))
        oIntegracion.LlamarFSISReemisiones(idEntidad, idLog, iErp)
    End Sub
    Private Function WebScheduleProviderBinding() As DataSet
        'Dim fechaUltimaParada As Infragistics.WebUI.Shared.SmartDate
        Dim ds As DataSet
        Dim dsParadas As DataSet
        Dim iParadaTime As Integer
        ds = New DataSet

        'Appointments
        Dim appointmentsTable As New DataTable("Appointments")
        appointmentsTable.Columns.Add("ID", GetType(Integer))
        appointmentsTable.Columns.Add("StartDateTimeUtc", GetType(DateTime))
        appointmentsTable.Columns.Add("Duration", GetType(Integer))
        appointmentsTable.Columns.Add("Subject", GetType(String))
        appointmentsTable.Columns.Add("AllDayEvent", GetType(Boolean))
        appointmentsTable.Columns.Add("Location", GetType(String))
        appointmentsTable.Columns.Add("ActivityDescription", GetType(String))
        appointmentsTable.Columns.Add("Status", GetType(Integer))
        appointmentsTable.Columns.Add("EnableReminders", GetType(Boolean))
        appointmentsTable.Columns.Add("EnableReminderResolved", GetType(Boolean))
        appointmentsTable.Columns.Add("ShowTimeAs", GetType(Integer))
        appointmentsTable.Columns.Add("Importance", GetType(Integer))
        appointmentsTable.Columns.Add("VarianceID", GetType(String))
        appointmentsTable.Columns.Add("RecurrencePattern", GetType(String))
        appointmentsTable.Columns.Add("RecurrenceID", GetType(Integer))
        appointmentsTable.Columns.Add("OriginalStartDateTimeUtc", GetType(DateTime))
        appointmentsTable.Columns.Add("ResourceKey", GetType(String))

        'Cargamos nuestros datos
        dsParadas = ObtenerParadas()
        Dim bPrimeraParada As Boolean = True
        For Each row As DataRow In dsParadas.Tables(0).Rows
            If Not IsDBNull(row("FECHA_INI")) Then
                If bPrimeraParada And (Not IsPostBack) Then
                    bPrimeraParada = False
                    Dim fechaTemp As Date = row("FECHA_INI")
                    'fechaUltimaParada = New Infragistics.WebUI.Shared.SmartDate(fechaTemp)
                    'schInfo.ActiveDayUtc = fechaUltimaParada.ToUniversalTime()
                End If
                'Configuramos, si podemos, el tiempo de parada
                If Not IsDBNull(row("FECHA_FIN")) Then
                    Dim fechaTempIni As DateTime = row("FECHA_INI")
                    Dim fechaTempFin As DateTime = row("FECHA_FIN")
                    iParadaTime = (fechaTempFin - fechaTempIni).Ticks / (TimeSpan.TicksPerMillisecond * 1000)
                Else
                    'Por defecto ponemos un valor
                    iParadaTime = 3600
                End If
                'App. Data
                appointmentsTable.Rows.Add(CreateAppointment(row("ID"), row("FECHA_INI"), iParadaTime, Textos(91), DBNullToStr(row("MOTIVO")), "Descripción de la interrupción de parada", False))
            End If
        Next

        dsParadas.Clear()
        dsParadas.Dispose()
        dsParadas = Nothing

        'Resources
        Dim resourcesTable As New DataTable("Resources")
        resourcesTable.Columns.Add("ID", GetType(Integer))
        resourcesTable.Columns.Add("ResourceName", GetType(String))
        resourcesTable.Columns.Add("ResourceDescription", GetType(String))
        resourcesTable.Columns.Add("EmailAddress", GetType(String))

        resourcesTable.Rows.Add(New Object() {-999, "Unassigned", "Unassigned Resource", "unassigned@unassigned"})

        ds.Tables.Add(appointmentsTable)
        ds.Tables.Add(resourcesTable)

        Return ds
    End Function
    Private Function ObtenerParadas() As DataSet
        Dim oInt As FSNServer.VisorInt = FSNServer.Get_Object(GetType(FSNServer.VisorInt))
        Dim ds As DataSet
        ds = oInt.ObtenerHistoricoParadasINT()
        Return ds
    End Function
    Protected Function CreateAppointment(ByVal id As Integer, ByVal startDateTime As DateTime, ByVal appointmentDuration As Integer, ByVal appName As String, ByVal location As String, ByVal appDescription As String,
    ByVal allDayEvent As Boolean) As Object()
        Return New Object() {id, startDateTime.ToUniversalTime(), appointmentDuration, appName, allDayEvent, location,
                appDescription, 1, False, False, 1, 1, Guid.NewGuid().ToString(),
        "", Nothing, Nothing, "-999"}
    End Function
    Protected Sub btnCerrarHistorico_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session.Remove("_dsHistorico")
        Session.Remove("ActiveDayUtc")
    End Sub
End Class