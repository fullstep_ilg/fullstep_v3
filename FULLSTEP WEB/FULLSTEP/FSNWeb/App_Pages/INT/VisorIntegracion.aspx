﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master" CodeBehind="VisorIntegracion.aspx.vb" Inherits="Fullstep.FSNWeb.VisorIntegracion" %>

<asp:Content ID="VisorIntegracionHead" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="~/App_Pages/BI/css/jquery-ui.css" />
</asp:Content>
<asp:Content ID="VisorIntegracion" ContentPlaceHolderID="CPH4" runat="server">
 <script type="text/javascript" language="javascript">
     var gIdLog;
     var gIdEntidad;
     var gERP;

     $(document).ready(function(){
        $("#pnlRelanzar").hide();
        $("#pnlNuevoMovimiento").hide();
		
		
     });
	 
	 
	 
     /*''' Revisado por: ngo; Fecha: 12/03/2012
     ''' <summary>
     ''' Provoca el postback para hacer la exportación a excel
     ''' </summary>
     ''' <remarks>Llamada desde: Icono de exportar a Excel; Tiempo máximo:0,5</remarks>
     */
     function ExportarExcel() {
         __doPostBack('ExportarExcel', '')
     }
     /*''' Revisado por: ngo; Fecha: 12/03/2012
     ''' <summary>
     ''' Provoca el postback para hacer la exportación a pdf
     ''' </summary>
     ''' <remarks>Llamada desde: Icono de exportar a pdf; Tiempo máximo:0,5</remarks>
     */
     function ExportarPDF() {
         __doPostBack('ExportarPDF', '')
     }


     /*
     ''' <summary>
     ''' Responder al evento click en una celda del grid. Si es el estado, muestra el tooltip con el mensaje de error
     ''' </summary>
     ''' <param name="sender"></param>
     ''' <param name="e"></param>        
     ''' <remarks>Llamada desde: sistema (al hacer click en una celda); Tiempo máximo:0</remarks>*/
     function whdgVisorInt_CellSelectionChanged(sender, e, event) {
         var Key = e.getSelectedCells().getItem(0).get_column().get_key();
         var Estado = e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("ESTADO").get_value();
         var Id = e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("IDENTIF").get_value();
         //elimina la seleccion sobre la celda para si se vuelve a pulsar salte el evento, (si siguiera seleccionada no saltaría de nuevo al hacer click)
         e.getSelectedCells().remove(e.getSelectedCells().getItem(0));
         if (Key == "ESTADO" && Estado == "3") {
             FSNMostrarPanel(ErrorAnimationClientID, event, ErrorDynamicPopulateClienteID, Id);
         }

     }
     /*
     '''<summary>
     '''Muestra la ventana de confirmar relanzamiento y configura el id del registro a relanzar
     '''</summary>
     '''<param name="id">identificador del registro de log a relanzar</param>*/
     function confirmaRelanzar(idLog, idEntidad, codERP) {
         gIdLog = idLog;
         gIdEntidad = idEntidad;
         gErp = codERP;
         CentrarPopUp($("#pnlRelanzar"));
         $('#popupFondo').css('height', $(document).height());
         $('#popupFondo').show();
         $("#pnlRelanzar").show();
         var texto=$("[id$=lblAlertaRelanzar]").html();
         texto=texto.replace('###', idLog);
         $("[id$=lblAlertaRelanzar]").html(texto);
         return false;
     }

     function cerrarNuevoMovimiento() {
         $("#pnlNuevoMovimiento").hide();
         $('#popupFondo').hide();
         return false;
     }

     function cancelarRelanzar() {
         $("#pnlRelanzar").hide();
         $('#popupFondo').hide();
         var texto = $("[id$=lblAlertaRelanzar]").html();
         texto = texto.replace(gIdLog, "###");
         $("[id$=lblAlertaRelanzar]").html(texto);
     }
     /**
     /*Relanza la integración del registro seleccionado
     **/
     function relanzar() {
         var newId;
         //ocultar popup 
         var texto = $("[id$=lblAlertaRelanzar]").text();
         texto = texto.replace(gIdLog, "###");
         $("[id$=lblAlertaRelanzar]").text(texto);
         $("#pnlRelanzar").hide();
         //mostrar ventana cargando
         MostrarCargando();

         params = { idLog: gIdLog};
         $.when($.ajax({
             type: "POST",
             url: rutaFS + 'INT/VisorIntegracion.aspx/relanzarProveedor',
             contentType: "application/json; charset=utf-8",
             data: JSON.stringify(params),
             dataType: "json",
             async: false
         })).done(function (msg) {
             //Realizar llamada asíncrona a FsnLibraryCom
             params = { idEntidad: gIdEntidad, idLog: msg.d[1], iErp: gErp };
             $.ajax({
                 type: "POST",
                 url: rutaFS + 'INT/VisorIntegracion.aspx/exportar',
                 contentType: "application/json; charset=utf-8",
                 data: JSON.stringify(params),
                 dataType: "json",
                 async: true
             });
             OcultarCargando();
             //tras el envío mostrar ventana de confirmacion
             CentrarPopUp($("#pnlNuevoMovimiento"));
             $('#popupFondo').show();
             $("#pnlNuevoMovimiento").show();
             var texto = $("[id$=lblNuevoMovimiento]").html();
             texto = texto.replace('###', gIdLog);
             texto = texto.replace('@@@', msg.d[0]); 
             $("[id$=lblNuevoMovimiento]").html(texto);
         });
         return false;
     }

     function OcultarCargando() {
         var modalprog = $find(ModalProgress);
         if (modalprog) modalprog.hide();
     }
     
    </script>
    <ig:WebExcelExporter ID="wdgExcelExporter" runat="server" ExportMode="Download"></ig:WebExcelExporter>
    <ig:WebDocumentExporter ID="wdgPDFExporter" runat="server" ExportMode="Download"></ig:WebDocumentExporter>
    <fsn:FSNPanelInfo ID="FSNPanelErrorInt" runat="server" 	ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" 
        ServiceMethod="CargarErrorIntegracion" TipoDetalle="0" />
    <!--Cabecera-->
   <div id="divCabeceraVisor" style="clear:both; position:relative; float:left; width:100%;">
		<fsn:FSNPageHeader ID="FSNPageHeader" runat="server" UrlImagenCabecera="~/images/VisorIntegracion.png"/>        
	</div>	   
   <!--Busqueda general-->
    <asp:UpdatePanel ID="upERP" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        <div style="clear: both; float: left; width: 100%;">
            <div style=" width: 45%; float: left; margin-top: 10px;">
                <div style="float: left; margin-right: 15px; margin-top: 10px;">
                    <asp:Label ID="lblERP" runat="server" Text="DERP:" CssClass="etiqueta"
                        Style="margin-left: 10px; margin-right: 5px; font-weight: bold;"></asp:Label>
                    <asp:DropDownList CssClass="desplegable" ID="ddERP" DataTextField="Text" DataValueField="Value"
                        runat="server" AutoPostBack="true" >
                    </asp:DropDownList>
                </div>
                <div style="float: left; margin-right: 15px; margin-top: 10px;">
                    <asp:Label ID="lblEntidad" runat="server" Text="DEntidad:" CssClass="etiqueta"
                        Style="margin-left: 15px; margin-right: 5px; font-weight: bold;"></asp:Label>
                    <asp:DropDownList CssClass="desplegable" ID="ddEntidad" DataTextField="Text" DataValueField="Value"
                        runat="server" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
            <div style="  width: 55%; float: left; margin-top: 10px;">
                <!--Código-->
                <div id="pFiltradoCodigo" style="float: left; margin-left: 10px; margin-top: 10px;">
                    <asp:Panel id="pCodigo" runat="server">
                        <asp:Label ID="lblCodigo" runat="server" Text="DCodigo:" Style="margin-right: 5px;
                            font-weight: bold;"></asp:Label>
                        <igpck:WebMaskEditor ID="txtCodigoMask" runat="server"></igpck:WebMaskEditor>
                        <asp:TextBox ID="txtCodigo" runat="server" Style="margin-right: 5px;" Width="150px"></asp:TextBox>
                    </asp:Panel> 
                </div>
                <!--Pedidos y Recepciones-->
                <div id="pFiltradoPedido" style="float: left;">
                     <asp:Panel ID="pRecep"  runat="server" visible="false" style="margin-top: 10px ">   
                        <asp:Label ID="lblAlbaran" runat="server" Text="Delivery Note:" Style="margin-right: 5px;
                            font-weight: bold;" ></asp:Label>
                        <asp:TextBox ID="txtAlbaran" runat="server" Style="margin-right: 5px;" Width="150px"></asp:TextBox>
                        <asp:Label ID="lblRecepERP" runat="server" Text="ERP delivery note:" Style="margin-right: 5px;
                            font-weight: bold;"></asp:Label>
                        <asp:TextBox ID="txtRecepERP" runat="server" Style="margin-right: 5px;" Width="150px"></asp:TextBox>
                    </asp:Panel>  
                    <asp:Panel ID="pPedido"  runat="server" visible="false" style="margin-top: 10px ">   
                        <asp:Label ID="lblPedido" runat="server" Text="DPedido:" Style="margin-right: 5px;
                            font-weight: bold;" ></asp:Label>
                        <asp:DropDownList CssClass="desplegable" ID="ddAnyoPed" DataTextField="Text" DataValueField="Value"
                            runat="server" Width="60px" Style="margin-right: 5px;">
                        </asp:DropDownList>
                        <asp:TextBox ID="txtNumPed" runat="server" Style="margin-right: 5px;" Width="100px"></asp:TextBox>
                        <asp:TextBox ID="txtNumOrden" runat="server" Style="margin-right: 5px;" Width="100px"></asp:TextBox>
                     </asp:Panel>  
                </div>
                
                <!--Proceso-->
                <div id="pFiltradoProceso" style="float: left;  margin-left: 10px; margin-top: 10px;">
                    <asp:Panel ID="pProceso" runat="server" visible="false">  
                        <div style="float: left;">
                            <asp:Label ID="lblProceso" runat="server"  Text="DProceso:" Style="margin-right: 5px; font-weight: bold;"></asp:Label>
                        </div>
                        <div style="float: left; margin-left: 10px;">
                            <asp:DropDownList ID="ddAnyoProce"  runat="server" CssClass="desplegable" AutoPostBack="true"  DataTextField="Text" DataValueField="Value" Width="100px" ></asp:DropDownList>
                        </div>
                        <div style="float: left; margin-left: 10px;">
                            <asp:DropDownList ID="ddGmnProce" runat="server" CssClass="desplegable"  AutoPostBack="true" DataTextField="Text" DataValueField="Value"  Width="100px"></asp:DropDownList>
                        </div>
                        <div style="float: left; margin-left: 10px;">
                              <asp:DropDownList ID="ddCodProce" runat="server" CssClass="desplegable"  AutoPostBack="true" DataTextField="Text" DataValueField="Value" Width="100px"></asp:DropDownList>
                        </div>
                    </asp:Panel>
                </div>

                <!--CódigoERP-->
                <div id="pFiltradoCodigoERP" style="float: left; margin-left: 10px; margin-top: 10px;">
                    <asp:Panel ID="pCodigoERP" runat="server" visible="false">  
                        <asp:Label ID="lblCodERP" runat="server" Style="margin-right: 5px; font-weight: bold;">DCodERP:</asp:Label>
                        <asp:TextBox ID="txtCodERP" runat="server"></asp:TextBox>
                    </asp:Panel> 
                </div>
                <!--Buscar-->
                <div id="divBotonBuscar" style="float: right; margin-left: 20px; margin-top: 10px;">
                    <fsn:FSNButton id="btnBuscarGeneral" runat="server" text="DBuscar"></fsn:FSNButton>
                </div>
                <!--Panel nuevo movimiento>
                <!-- popup, metido en dentro de update panel para que no haga postback-->
                 <div id="pnlNuevoMovimiento"  class="popupCN" style="margin:0 auto;position:absolute;z-index:1555;display:none;padding:1.5em;">
                    <div style="float:left;width: 100%; text-align:right; position:absolute;top:1em;right:1em;vertical-align:top">
		                    <asp:ImageButton ID="ImageButton2" runat="server" SkinID="Cerrar" 
                                style="float:right; vertical-align:top;" ImageUrl="~/Images/Cerrar.png" 
                                Height="26px" Width="23px" OnClientClick="cerrarNuevoMovimiento();" /> 
                    </div>
                    <p style="margin:1em auto;clear:both;">
                        <asp:Label id='lblNuevoMovimiento' runat="server">DMovimento ### relanzado.<br />Nuevo Identificador : @@@</asp:Label>
                    </p>
                    <div style="text-align:center;">
                        <input type="button" id="btnOk" runat="server" value="DAceptar" class="botonRedondeado" onclick="cerrarNuevoMovimiento();"/>
                    </div>
                </div>
            </div>
        </div>
        </ContentTemplate>
    </asp:UpdatePanel>

   <!--Busqueda avanzada-->
    <div style="clear: both; float: left; width: 100%; margin-top: 10px;">
        <asp:Panel ID="pnlCabeceraParametros" runat="server" Width="100%">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td valign="middle" align="left" style="padding-top: 5px">
                    <asp:Panel runat="server" ID="pnlBusquedaAvanzada" Style="cursor: pointer" Font-Bold="True"
                        Height="20px" Width="100%" CssClass="PanelCabecera">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="padding-top: 2px">
                                    <asp:Image ID="imgExpandir" runat="server" SkinID="Expandir" />
                                </td>
                                <td style="vertical-align: top; padding-top: 2px">
                                    <asp:Label ID="lblBusquedaAvanzada" runat="server" Text="DSeleccione los criterios de búsqueda avanzada"
                                        Font-Bold="True" ForeColor="White"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        </asp:Panel>
    </div>
    <div style="clear: both; float: left; width: 100%; margin-top: 10px;">
        <asp:Panel ID="pnlParametros" runat="server" CssClass="Rectangulo" Width="99%">
            <asp:UpdatePanel ID="upBusquedaAvanzada" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <!--Inicio UCBusquedaAvanzada-->
                <asp:Panel ID="PnlFiltrosAvanzados" runat="server" Width="100%">
                    <table width="100%" style="table-layout: fixed" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 80px; padding-top: 5px; padding-left: 15px;">
                                <asp:Label ID="lblIdentificador" runat="server" Text="dIdentificador:" Font-Bold="true"></asp:Label>
                            </td>
                            <td style="width: 150px; padding-top: 5px;">
                                <asp:TextBox ID="txtIdentificador" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td style="width: 50px; padding-top: 5px; padding-left: 20px;">
                                <asp:Label ID="lblEstado" runat="server" Text="dEstado" Font-Bold="true"></asp:Label>
                            </td>
                            <td style="width: 200px; padding-top: 5px; padding-left: 5px;">
                                <asp:DropDownList  ID="ddEstado" DataTextField="Text" DataValueField="Value"
                                         runat="server" Width="200px"></asp:DropDownList>
                            </td>
                            <td style="width: 80px; padding-top: 5px; padding-left: 20px;">
                                <asp:Label ID="lblFechaDesde" runat="server" Text="dFechaDesde" Font-Bold="true"  Visible="true"></asp:Label>
                            </td>
                            <td style="width: 120px; padding-top: 5px; padding-left: 5px;">
                                <igpck:WebDatePicker ID="dteDesde" Width="100px" Height = "15px" runat="server" SkinID="Calendario"></igpck:WebDatePicker>
                            </td>
                            <td style="width: 100px; padding-top: 5px; padding-left: 5px;">
                                <igpck:WebDatePicker ID="hrDesde" Buttons-CustomButtonDisplay="None" Width="75px" Height = "15px"  runat="server" SkinID="SpinButtons"></igpck:WebDatePicker>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 80px; padding-top: 5px; padding-bottom: 5px; padding-left: 15px">
                                <asp:Label ID="lblAccion" runat="server" Text="dAccion:" Font-Bold="true"></asp:Label>
                            </td>
                            <td style="width: 150px; padding-top: 5px; padding-bottom: 5px;">
                                <asp:DropDownList CssClass="normal" ID="ddAccion" DataTextField="Text" DataValueField="Value"
                                    runat="server" Width="150px"></asp:DropDownList>
                            </td>
                            <td style="width: 80px; padding-top: 5px; padding-bottom: 5px; padding-left: 20px;">
                                <asp:Label ID="lblSentido" runat="server" Text="dSentido:" Font-Bold="true"></asp:Label>
                            </td>
                            <td style="width: 200px; padding-top: 5px; padding-bottom: 5px; padding-left: 5px;">
                                <div style="background-color: White; width: 200px">
                                    <asp:DropDownList ID="ddSentido" DataTextField="Text" DataValueField="Value"
                                        runat="server" Width="200px"></asp:DropDownList>
                                </div>
                            </td>
                            <td style="width: 80px; padding-top: 5px; padding-left: 20px;">
                                <asp:Label ID="lblFechaHasta" runat="server" Text="dFechahasta" Font-Bold="true"
                                    Visible="true"></asp:Label>
                            </td>
                            <td style="width: 120px; padding-top: 5px; padding-left: 5px;">
                                <igpck:WebDatePicker ID="dteHasta" Width="100px" Height = "15px" runat="server" SkinID="Calendario"></igpck:WebDatePicker>
                            </td>
                            <td style="width: 100px; padding-top: 5px; padding-left: 5px;">
                                <igpck:WebDatePicker ID="hrHasta" Buttons-CustomButtonDisplay="None"  Width="75px" Height = "15px" runat="server" SkinID="SpinButtons"></igpck:WebDatePicker>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <!--Fin UCBusquedaAvanzada-->
                <table width="80%" border="0" cellpadding="4" cellspacing="0">
                    <tr>
                        <td style="width: 60px;"  align="center">
                            <fsn:FSNButton ID="btnLimpiarBusquedaAvanzada" runat="server" Text="DLimpiar" Alineacion="Right" ></fsn:FSNButton>
                        </td>
                        <td colspan="6">
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        </asp:Panel>
    </div>
    <ajx:CollapsiblePanelExtender id="CollapsiblePanelExtender1" runat="server" CollapseControlID="pnlCabeceraParametros"
        ExpandControlID="pnlCabeceraParametros" ImageControlID="imgExpandir" SuppressPostBack="true"
        TargetControlID="pnlParametros" Collapsed="true" SkinID="FondoRojo">
    </ajx:CollapsiblePanelExtender>
    
    <!--Grid Visor Integracion-->
    <div style="clear: both; float: left; width: 100%; margin-top: 10px;">
    <asp:UpdatePanel ID="UpGridVisor" ChildrenAsTriggers="false"
        runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnBuscarGeneral" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <ig:WebHierarchicalDataGrid  ID="whdgVisorInt" runat="server"  Width="99.8%" Height="510px" ShowHeader = "true"
                AutoGenerateColumns="false" DataKeyFields = "IDENTIF" BorderWidth ="1px" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="false" >
                <Columns>
                    <ig:BoundDataField Key="ERP" DataFieldName="ERP"></ig:BoundDataField>
                    <ig:BoundDataField Key="ENTIDAD" DataFieldName="ENTIDAD" ></ig:BoundDataField>
                    <ig:BoundDataField Key="IDENTIF" DataFieldName="IDENTIF" ></ig:BoundDataField>
                    <ig:BoundDataField Key="ESTADO" DataFieldName="ESTADO" ></ig:BoundDataField>
                    <ig:BoundDataField Key="TEXTO" DataFieldName="TEXTO" ></ig:BoundDataField>
                    <ig:BoundDataField Key="FECENV" DataFieldName="FECENV" CssClass="itemCenterAligment"></ig:BoundDataField>
                    <ig:BoundDataField Key="FECREC" DataFieldName="FECREC" CssClass="itemCenterAligment"></ig:BoundDataField>
                    <ig:BoundDataField Key="ACCION" DataFieldName="ACCION" ></ig:BoundDataField>
                    <ig:BoundDataField Key="COD" DataFieldName="COD"></ig:BoundDataField>
                    <ig:BoundDataField Key="COD_ERP" DataFieldName="COD_ERP"></ig:BoundDataField>
                    <ig:BoundDataField Key="INFO" DataFieldName="INFO"></ig:BoundDataField>
                    <ig:BoundDataField Key="USU" DataFieldName="USU"></ig:BoundDataField>
                    <ig:BoundDataField Key="NOM_FICHERO" DataFieldName="NOM_FICHERO"></ig:BoundDataField>
                    <ig:BoundDataField Key="NOM_RECIBO" DataFieldName="NOM_RECIBO"></ig:BoundDataField>
                    <ig:BoundDataField Key="ORIGEN" DataFieldName="ORIGEN"></ig:BoundDataField>
                    <ig:BoundDataField Key="RELANZAR" DataFieldName="RELANZAR"></ig:BoundDataField> 
                </Columns> 
                <Behaviors>
                    <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
                    <ig:Filtering Alignment="Top" Enabled="true" Visibility="Visible" AnimationEnabled="false">
                    </ig:Filtering>
                    <ig:Paging Enabled="true" PagerAppearance="Top" >
                        <PagerTemplate>
                            <div class="CabeceraBotones" style="clear:both; float:left; width:100%;">  
                                <div style="float:left; padding:3px 0px 2px 0px;">
                                    <fsn:wucPagerControl ID="CustomerPager" runat="server" />
                                </div>   
                                <div style="float: right; margin:5px 5px 0px 0px;">
                                    <div onclick="ExportarExcel()" class="botonDerechaCabecera" style="float: left;">
                                        <asp:Image runat="server" ID="imgExcel" CssClass="imagenCabecera" />
                                        <asp:Label runat="server" ID="lblExcel" CssClass="fntLogin2" Text="Excel"></asp:Label>
                                    </div>
                                    <div onclick="ExportarPDF()" class="botonDerechaCabecera" style="float: left; margin-left: 15px;">
                                        <asp:Image runat="server" ID="imgPDF" CssClass="imagenCabecera" />
                                        <asp:Label runat="server" ID="lblPDF" CssClass="fntLogin2" Text="PDF"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </PagerTemplate>
                    </ig:Paging>
                    <ig:VirtualScrolling Enabled="false"></ig:VirtualScrolling>                           
                    <ig:Sorting Enabled="true" SortingMode="Multi"></ig:Sorting>
                    <ig:ColumnMoving Enabled="true"></ig:ColumnMoving>
                    <ig:Selection Enabled="true" CellClickAction="Cell" CellSelectType="Single" SelectedCellCssClass="EstadoIntPendiente">
                        <SelectionClientEvents CellSelectionChanged ="whdgVisorInt_CellSelectionChanged"/>
                    </ig:Selection>
                </Behaviors>
                <GroupingSettings EnableColumnGrouping="True" GroupAreaVisibility="Visible" />
            </ig:WebHierarchicalDataGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
    </div>
    
    <!--Panel relanzar integracion-->
    <div id="pnlRelanzar" class="popupCN" style="margin:0 auto;position:absolute;z-index:1555;display:none;border:1px;padding:1.5em;">
        <div style="text-align:right;position:absolute;top:1em;right:1em;">
		        <img ID="ImageButton1" style="vertical-align:top;" src="../../Images/Cerrar.png" 
                    height="26px" width="23px" onclick ="cancelarRelanzar();" />
        </div>
        <div>
            <p style="margin:1em auto;"><asp:Label id='lblAlertaRelanzar' runat="server">D¿Desea relanzar la integación<br /> correspondiente al movimiento ###?</asp:Label></p>
        </div>
        <div style="text-align:center">
            <input type="button" id="btnAceptarRelanzar" runat="server" value="DAceptar" class="botonRedondeado" style="margin-right:2em" onclick="relanzar();"/>
            <input type="button" id="btnCancelarRelanzar"  runat="server" value="DCancelar" class="botonRedondeado" style="margin-left:2em;" onclick="cancelarRelanzar();" />    
        </div>    
    </div>
        
    <!-- Panel Cargando... -->
    
    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
		<div style="position: relative; top: 30%; text-align: center;">
		    <asp:Image ID="ImgProgress" runat="server" SkinId="ImgProgress" />
            <asp:Label ID="LblProcesando" runat="server" Text="..."></asp:Label>	
		</div>
	</asp:Panel>
	<ajx:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
		BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" /> 

      <!-- Panel Alerta Parada Integracion -->
    <input id="btnOcultoParada" type="button" value="button" runat="server" style="display: none" />
    <asp:Panel ID="pnlAlertaParada" runat="server" BackColor="White" BorderColor="DimGray"
        BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left" Style="display: none;
        min-width: 450px; padding: 2px">
        <asp:UpdatePanel ID="updpnlPopupAlertaParada" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="clear: both;float:left ; width: 100%; text-align:right; vertical-align:top ">  
                    <asp:ImageButton ID="ImgBtnCerrarpnlAlertaParada" runat="server" ImageUrl="~/images/Bt_Cerrar.png" />
                </div>
                <div style="clear: both; float: left; width: 100%;">
                    <div style="clear: both; float: left;  margin-left: 15px; vertical-align:middle;">
                        <asp:Image runat="server" ID="imgAlertaInt" ImageAlign="Middle" />
                    </div>
                    <div style="float:left; margin-left: 20px; margin-top: 10px; text-align:left">
                        <asp:Label ID="lblParada" runat="server" CssClass="RotuloGrande">DServicio de integración interrumpido!</asp:Label>
                    </div>
                </div>
                <div style="clear: both; float: left; margin-left: 75px; margin-top: 10px; text-align:center; ">
                    <asp:Label ID="lblHoraParada" runat="server" CssClass="Rotulo">DÚltimaIntegración:</asp:Label>
                </div>
                <div style="clear: both; float: left; margin-left: 75px; margin-top: 10px; margin-bottom:30px; text-align:center;">
                    <asp:Label ID="lblConsulteDpto" runat="server"  style="text-align:center" CssClass="EtiquetaPeque">DConsulte con el departamento técnico:</asp:Label>
                </div>
            </div>
        </ContentTemplate> 
        </asp:UpdatePanel> 
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpeAlertaParada" runat="server" BackgroundCssClass="modalBackground" PopupControlID="pnlAlertaParada"
        TargetControlID="btnOcultoParada" CancelControlID="ImgBtnCerrarpnlAlertaParada" RepositionMode="None">
    </ajx:ModalPopupExtender>
    
		
</asp:Content>

