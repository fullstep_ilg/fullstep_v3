﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BuscadorPartidas.aspx.vb" Inherits="Fullstep.FSNWeb.BuscadorPartidas" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
    <script language="javascript" type="text/javascript">

        //////////////////////////////////////////////////
        function seleccionarPartida() {

            var oTree = $find("Partidas_wdtPartidas");

            var oNode = oTree.get_selectedNodes()[0]
            if (oNode == null)
                return false

            //Tag: 1 = centro de coste
            //     2 = partida presupuestaria imputable
            //     3 = partida presupuestaria no imputable    
            if ((oNode.get_target() == null) || (oNode.get_target() == 1) || (oNode.get_target() == 3)) {
                return false
            }
            
            var sDen = oNode.get_text();
            var sTag = oNode.get_target();
            if (sTag != 4)
                sDen = sDen.substring(0, sDen.lastIndexOf('('));

            var sUON = new Array()

            var sUON0 = '';
            var sUON1 = '';
            var sUON2 = '';
            var sUON3 = '';
            var sUON4 = '';
            var sUON5 = '';
            var sUON6 = '';
            var sUON7 = '';
            var sUON8 = '';
            var sDenCC = '';
            var iNivel = -1

            while (oNode) {
                iNivel++;
                if (oNode.get_target() == 3)
                    sUON[iNivel] = '';
                else
                    sUON[iNivel] = oNode.get_key()

                if ((oNode.get_target() == 1) && (sDenCC == ''))
                    sDenCC = oNode.get_text(); ; //cogemos la denominación del cc

                oNode = oNode.get_parentNode()
            }

            var j = 0;
            for (i = iNivel; i > 0; i--) {
                eval("sUON" + i.toString() + "=sUON[" + j.toString() + "]");
                j++;
            }

            if (iNivel > 5) {
                if (sUON6 != '') {
                    sUON5 = sUON6
                }
                if (sUON7 != '') {
                    sUON5 = sUON7
                }
                if (sUON8 != '') {
                    sUON5 = sUON8
                }
            }
            var sIdControlPartida = document.getElementById("idControlPartida")
            var sIdControlPartidaDen = document.getElementById("idControlPartidaDen")
            var sIdControlCentroCoste = document.getElementById("idControlCentroCoste")
            var sIdControlCentroCosteDen = document.getElementById("idControlCentroCosteDen")

            window.opener.Partida_seleccionada(sIdControlPartida.value, sIdControlPartidaDen.value, sUON1, sUON2, sUON3, sUON4, sUON5, sDen, sIdControlCentroCoste.value, sIdControlCentroCosteDen.value, sDenCC)
            window.close()

        }

        function AbrirBuscadorCentros() {
            var newWindow = window.open("BuscadorCentrosCoste.aspx?VerUON=1", "_blank", "width=750,height=475,status=yes,resizable=no,scrollbars=yes,top=200,left=200");
            newWindow.focus();
        }

        function CentroCoste_seleccionado(idControl, idHidControl, sUON1, sUON2, sUON3, sUON4, sDen) {
            var sValor = '';

            //lo que se guarda en valor_text
            if (sUON1)
                sValor = sUON1;
            if (sUON2)
                sValor = sValor + '#' + sUON2;
            if (sUON3)
                sValor = sValor + '#' + sUON3;
            if (sUON4)
                sValor = sValor + '#' + sUON4;

            txtCentroCostes = document.getElementById("idControlCentroCoste")
            txtCentroCostes.value = sDen
            hidCentroCostes = document.getElementById("idControlCentroCosteDen")
            hidCentroCostes.value = sValor

            txtCentro = document.getElementById("Partidas_txtCentro")
            txtCentro.value = sDen
            hCentroCod = document.getElementById("Partidas_hCentroCod")
            hCentroCod.value = sValor
        }

</script>
</head>
<body style="background-color: #FFFFFF; margin-top: 0px; margin-left: 0px; margin-right: 0px">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scm1" runat="server"></asp:ScriptManager>
    <div>
        <fspm:BusquedaPartidas id="Partidas" runat="server"></fspm:BusquedaPartidas>
        <input type="hidden" id="idControlPartida" runat="server" />
        <input type="hidden" id="idControlPartidaDen" runat="server" />
        <input type="hidden" id="idControlCentroCoste" runat="server" />
        <input type="hidden" id="idControlCentroCosteDen" runat="server" />
    </div>
    </form>
</body>
</html>
