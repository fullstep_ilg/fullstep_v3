﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="errores.aspx.vb" Inherits="Fullstep.FSNWeb.errores" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
    	<script language="javascript" type="text/javascript">
<!--

/*''' <summary>
''' Cierra la pagina de error
''' </summary>
''' <remarks>Llamada desde: linkVolver; Tiempo máximo: 0</remarks>*/
function fVolverCerrar() {
	if(window.document.referrer == ''){
		self.close();
	}
    else {
        if (window.opener) {
            self.close();    
        }
        else {
            window.location = urlVolver;
        }
	}
}
function ponerLabelVolver() {    
    if (document.getElementById('tbVolver')) {
        if(window.document.referrer == ''){
            document.all.hlInicio.innerHTML=lCerrar;
        }
        else{
            document.all.hlInicio.innerHTML=lInicio;
        }
    }		
}
//-->
</script>
</head>
<body onload="ponerLabelVolver();" style="background-color:White">
    <form id="form1" runat="server">
			<br />
			<br />
			<asp:Label CssClass="TituloErrorUsuario" id="lblTitulo" runat="server" Width="80%"></asp:Label>
			<hr color="lightgrey" />
			<br />
			<ul class="TituloErrorUsuario" style="LIST-STYLE-POSITION: outside; LIST-STYLE-TYPE: square; POSITION: static">
				<li>
					<asp:Label cssClass="SubTituloErrorUsuario" id="lblSubTitulo1" runat="server" Font-Size="10"></asp:Label>
					<br />
					<br />
					<a href="http://www.fullstep.com/FSGE" class="SubTituloErrorUsuario" style="FONT-SIZE: 10pt">
						<b><asp:literal Runat="server" ID="lEnlaceFSGE"></asp:literal></b></a><br />
					<br /></li>
				<li>
					<asp:Label cssClass="SubTituloErrorUsuario" id="lblSubTitulo2" runat="server" Font-Size="10"></asp:Label>
					<br /> 
					<br />
					<table cellpadding="0">
						<tr>
							<td width="306" height="79"><img src="<%=ConfigurationManager.AppSettings("ruta")%>images/helpdesk.jpg" /></td>
							<td>
								<table class="SubTituloErrorUsuario" bgcolor="whitesmoke">
									<tr>
										<td><asp:literal Runat="server" ID="lPorTelefono"></asp:literal></td>
									</tr>
									<tr>
										<td><asp:literal Runat="server" ID="lViaEmail"></asp:literal></td>
									</tr>
									<tr>
										<td><asp:literal Runat="server" ID="lHorario1"></asp:literal></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<br />
					<br />
					
					<table bgcolor="gray" cellspacing="1" border="0" height="70">
						<tr>
							<td bgcolor="whitesmoke">
								<table border="0" cellpadding="7" cellspacing="4">
									<tr>
										<td><asp:Label CssClass="SubTituloErrorUsuario" ID="lblIdentificadorError" Runat="server" Font-Size="12"
												Font-Bold="True"></asp:Label></td>
										<td><asp:Label CssClass="TituloSinDatos" ID="lblIdError" Runat="server"></asp:Label></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<br /><br />
				<table id="tbVolver" cellpadding="0" cellspacing="4" runat="server">
                    <tr>
                        <td valign="middle" width="20"><img src="<%=ConfigurationManager.AppSettings("ruta")%>images/volver_bullet_cuad_b.gif" border="0" /></td>
				        <td width="55"><asp:HyperLink  cssclass ="SubTituloErrorUsuario" Font-Size="10" Font-Bold="True" Runat="server" ID="hlInicio"></asp:HyperLink></td>
                    </tr>
				</table>
			</li>
		</ul>
    </form>
</body>
</html>
