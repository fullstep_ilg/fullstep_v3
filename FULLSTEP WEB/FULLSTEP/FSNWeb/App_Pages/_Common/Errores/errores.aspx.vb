﻿Public Partial Class errores
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not FSNServer Is Nothing Then
                Dim oError As FSNServer.Errores = FSNServer.Get_Object(GetType(FSNServer.Errores))
                Dim sPagina As String = Request("aspxerrorpath")
                Dim sUsuario As String = FSNUser.Cod
                Dim sv_Http_User_Agent As String = Request.ServerVariables("HTTP_USER_AGENT")

                If sPagina = Nothing Then
                    sPagina = Request.Path
                End If
                Dim tmpExcepcion As Exception
                tmpExcepcion = Server.GetLastError

                If Not tmpExcepcion Is Nothing Then
                    Dim sSv_Query_String As String = tmpExcepcion.Data("QueryString")
                    tmpExcepcion = tmpExcepcion.GetBaseException
                    oError.Create(sPagina, sUsuario, tmpExcepcion.GetType().FullName, tmpExcepcion.Message, tmpExcepcion.StackTrace, sSv_Query_String, sv_Http_User_Agent)
                Else
                    oError.Create(sPagina, sUsuario, String.Empty, String.Empty, String.Empty, String.Empty, sv_Http_User_Agent)
                End If

                Dim sIdi As String = FSNUser.Idioma
                If sIdi = Nothing Then sIdi = ConfigurationManager.AppSettings("idioma")

                Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
                Dim oTextos As DataTable
                oDict.LoadData(TiposDeDatos.ModulosIdiomas.PaginaError, sIdi)
                oTextos = oDict.Data.Tables(0)

                lblTitulo.Text = oTextos.Rows(0).Item(1)
                lblSubTitulo1.Text = oTextos.Rows(1).Item(1)
                lEnlaceFSGE.Text = oTextos.Rows(2).Item(1)
                lblSubTitulo2.Text = oTextos.Rows(3).Item(1)
                lPorTelefono.Text = oTextos.Rows(4).Item(1)
                lViaEmail.Text = oTextos.Rows(5).Item(1)
                lHorario1.Text = oTextos.Rows(6).Item(1)
                lblIdentificadorError.Text = oTextos.Rows(7).Item(1)
                lblIdError.Text = oError.ID

                If FSNServerLogin.TipoDeAutenticacion <> TiposDeAutenticacion.ServidorExterno Then
                    hlInicio.Text = oTextos.Rows(8).Item(1)
                    hlInicio.NavigateUrl = "javascript:fVolverCerrar();"

                    ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sVolver", "var urlVolver=""" & ConfigurationManager.AppSettings("rutaFS") & "inicio.aspx" & """;lInicio=""" & oTextos.Rows(8).Item(1) & """;lCerrar=""" & oTextos.Rows(9).Item(1) & """", True)
                Else
                    tbVolver.Visible = False
                End If
            Else
                Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "inicio.aspx")
            End If
        Catch ex As Exception
        End Try
    End Sub
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Sub SendException(ByVal Exception As Object)
        HttpContext.Current.Session("AjaxException") = Exception
    End Sub
End Class