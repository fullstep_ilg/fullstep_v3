﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BuscadorProveedores.aspx.vb" Inherits="Fullstep.FSNWeb.BuscadorProveedores"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
    <style type="text/css">
        html{height:100%; margin-bottom:1px;}
        .itemBold {font-weight:bold;}
    </style>    
</head>
<body style="background-color: #FFFFFF; margin-top: 0px; margin-left: 0px; margin-right: 0px">

    <script type="text/javascript">
        var isPedidoExpressNegociable;
        /*'''funciones para la PAGINACIÓN*/
        /*''' <summary>
        ''' Selecciono una pagina en concreto
        ''' </summary>
        ''' <remarks>Llamada desde: PagerPageList.onchange ; Tiempo máximo: 0</remarks>*/
         function IndexChanged(){
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var grid = $find("<%= wdgDatos.ClientID %>");
            var parentGrid = grid;
            var newValue = dropdownlist.selectedIndex;
            parentGrid.get_behaviors().get_paging().set_pageIndex(newValue);
        }
        function FirstPage(){
            var grid = $find("<%= wdgDatos.ClientID %>");
            var parentGrid = grid;
            parentGrid.get_behaviors().get_paging().set_pageIndex(0);
        }
        function PrevPage(){
            var grid = $find("<%= wdgDatos.ClientID %>");
            var parentGrid = grid;
            var dropdownlist = $('[id$=PagerPageList]')[0];
            if (parentGrid.get_behaviors().get_paging().get_pageIndex() > 0) {
               parentGrid.get_behaviors().get_paging().set_pageIndex(parentGrid.get_behaviors().get_paging().get_pageIndex() - 1);
            }
        }
        function NextPage(){
            var grid = $find("<%= wdgDatos.ClientID %>");
            var parentGrid =grid;
            var dropdownlist = $('[id$=PagerPageList]')[0];
            if (parentGrid.get_behaviors().get_paging().get_pageIndex() < parentGrid.get_behaviors().get_paging().get_pageCount() - 1) {
               parentGrid.get_behaviors().get_paging().set_pageIndex(parentGrid.get_behaviors().get_paging().get_pageIndex() + 1);
            }                 
        }
        function LastPage(){
            var grid = $find("<%= wdgDatos.ClientID %>");
            var parentGrid = grid;
            parentGrid.get_behaviors().get_paging().set_pageIndex(parentGrid.get_behaviors().get_paging().get_pageCount()-1);
        }
        function AdministrarPaginador(){
            var grid = $find("<%= wdgDatos.ClientID %>");
            var parentGrid = grid;
            $('[id$=lblCount]').text(parentGrid.get_behaviors().get_paging().get_pageCount());
            if(parentGrid.get_behaviors().get_paging().get_pageIndex()==0){
                $('[id$=ImgBtnFirst]').attr('disabled','disabled');
                $('[id$=ImgBtnFirst]').attr('src',rutaTheme + '/images/primero_desactivado.gif');
                $('[id$=ImgBtnPrev]').attr('disabled','disabled');
                $('[id$=ImgBtnPrev]').attr('src',rutaTheme + '/images/anterior_desactivado.gif');
            }else{
                $('[id$=ImgBtnFirst]').removeAttr('disabled');
                $('[id$=ImgBtnFirst]').attr('src',rutaTheme + '/images/primero.gif');
                $('[id$=ImgBtnPrev]').removeAttr('disabled');
                $('[id$=ImgBtnPrev]').attr('src',rutaTheme + '/images/anterior.gif');
            }
            if(parentGrid.get_behaviors().get_paging().get_pageIndex()==parentGrid.get_behaviors().get_paging().get_pageCount()-1){
                $('[id$=ImgBtnNext]').attr('disabled','disabled');
                $('[id$=ImgBtnNext]').attr('src',rutaTheme + '/images/siguiente_desactivado.gif');
                $('[id$=ImgBtnLast]').attr('disabled','disabled');
                $('[id$=ImgBtnLast]').attr('src',rutaTheme + '/images/ultimo_desactivado.gif');
            }else{
                $('[id$=ImgBtnNext]').removeAttr('disabled');
                $('[id$=ImgBtnNext]').attr('src',rutaTheme + '/images/siguiente.gif');
                $('[id$=ImgBtnLast]').removeAttr('disabled');
                $('[id$=ImgBtnLast]').attr('src',rutaTheme + '/images/ultimo.gif');
            }
        }
        function wdgDatos_PageIndexChanged(a,b,c,d){
            var grid = $find("<%= wdgDatos.ClientID %>");
            var parentGrid =grid;
            var dropdownlist = $('[id$=PagerPageList]')[0];

            dropdownlist.options[parentGrid.get_behaviors().get_paging().get_pageIndex()].selected = true;
            AdministrarPaginador(); 
        }
        /*''' <summary>
        ''' Reestablece los filtros sin tocar el grid
        ''' </summary> 
        ''' <param name="sender">parametro sistema</param>
        ''' <remarks>Llamada desde: btnLimpiar ; Tiempo máximo: 0 </remarks>*/
        function Limpiar(sender) {
            document.getElementById("<%=txtIdentificador.ClientID%>").value = "";
            document.getElementById("<%=txtDenominacion.ClientID%>").value = "";
            document.getElementById("<%=txtCif.ClientID%>").value = "";            
            if (document.getElementById("<%=chkProveCriticos.ClientID%>")!= null) document.getElementById("<%=chkProveCriticos.ClientID%>").checked=false;

            if (document.getElementById("txtMaterial") != null){
                document.getElementById("<%=txtMaterial.ClientID%>").value = "";
                document.getElementById("<%=hidMaterial.ClientID%>").value = "";
            }
            
            var dropdown = null;
            dropdown = $find("<%= ugtxtOrganizacionCompras.clientID %>");
            if (dropdown != null) {           
                dropdown.set_currentValue(DenOrgInicial, true);
                document.getElementById("<%=hidOrg.ClientID%>").value = DenOrgInicial;
                
                var grid = $find('<%= ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg").clientID %>');
                var selectedRows = grid.get_behaviors().get_selection().get_selectedRows();
                var row = selectedRows.getItem(0);
                
                selectedRows.remove(row);
                
                row = grid.get_rows().get_row(OrgInicial);
                selectedRows.add(row);
            }          
        }
        /*''' <summary>
        ''' Devuelve a MailProveedores.aspx una selección de contactos.
        ''' </summary>
        ''' <param name="sender">parametro sistema</param>       
        ''' <remarks>Llamada desde: btnAceptar ; Tiempo máximo: 0</remarks>*/
        function Aceptar(sender) { 		
			var grid = null;
            grid = $find("<%=wdgDatos.clientID %>");
            if(grid){
                var ListaContacto = "";

                //Si no se ha seleccionado nada saca un mensaje:
                if (grid.get_behaviors().get_selection().get_selectedRows().get_length() == 0){
                    alert(textoAlertMail);
			        return false;
                }

                for(i=0; i< grid.get_behaviors().get_selection().get_selectedRows().get_length(); i++){          
                    row=grid.get_behaviors().get_selection().get_selectedRows().getItem(i);    
                    
                    if (row){
                        Contacto = row.get_cellByColumnKey("EMAIL");
                        if (Contacto.get_text() != ""){
                            ListaContacto=ListaContacto + Contacto.get_text() + ";";   
                        }       
                    }
                }
             
                window.close();
                
                if (document.getElementById("EsBtPara").value == 1){
		            var p = window.opener;
		            p.MasMail('Para', ListaContacto + document.getElementById("HiddenLista").value);                
                } else {
				    if (document.getElementById("EsBtCC").value ==1){
				        var p = window.opener;
				        p.MasMail('CC', ListaContacto + document.getElementById("HiddenLista").value);
				    } else {
					    if (document.getElementById("EsBtCCO").value ==1){
					        var p = window.opener;
					        p.MasMail('CCO', ListaContacto + document.getElementById("HiddenLista").value);
					    }	
				    }
		        }                 
                return true;
            }
            return false;
        }

        /*''' <summary>
        ''' Cierra sin hacer nada
        ''' </summary>
        ''' <param name="sender">parametro sistema</param>       
        ''' <remarks>Llamada desde: btnCancelar ; Tiempo máximo: 0</remarks>*/    
        function Cancelar(sender){
            window.close();
        }
                
        /*
        ''' <summary>
        ''' Devuelve a la pantalla q llamo a BusquedaProvedores el proveedor seleccionado y cierra esta pantalla.
        ''' </summary>
        ''' <param name="sender">parametro de sistema</param>
        ''' <param name="e">parametro de sistema</param>            
        ''' <remarks>Llamada desde: Sistema ;Tiempo máximo: 0</remarks>
        */
        function wdgDatos_CellSelectionChanged(sender, e) {
            var Key = e.getSelectedCells().getItem(0).get_column().get_key();
            var sProveCod = e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("COD").get_value();
            var sProveDen = e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("DEN").get_value();
            var sCIF = e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("NIF").get_value();
            var bEncontrado = false;
            var sIdsContactos = '',sEmailsContactos = '';
            
            //si además estamos en un pedido express con articulos negociados permitidos tenemos que borrar los 
            //artículos adjudicados al anterior proveedor
            if (isPedidoExpressNegociable) {
                p = window.opener;
                var sProveedor; //proveedor actual
                oProveedor = p.fsGeneralEntry_getById(document.getElementById("IDCONTROL").value);
                sProveedor = oProveedor.getDataValue();
                p.borraArticulosAdjudicadosProve(sProveedor);
                p.BorrarProveedoresERP(oProveedor);
            }
            //Reemplazamos el nuevo proveedor en buscador articulos


            if (Key == "SEL"){
                //nada q hacer
            }
            else{
                if (Key == "FAVORITO") //Añadir o Eliminar Favorito
                {
                    var sAccion = ""
                    var favorito = e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("FAVORITO").get_value();
                    if (favorito == TextoNo) {
                        sAccion="Insertar"
                        e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("FAVORITO").set_value("")
                        e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("FAVORITO").get_element().innerHTML='<IMG style="TEXT-ALIGN: center" src="<%=System.Configuration.ConfigurationManager.AppSettings("ruta")%>App_themes/<%=Page.Theme%>/images/favoritos_si.gif">'
                    }
                    else {
                        sAccion="Eliminar" 
                        e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("FAVORITO").set_value(TextoNo)
                        e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("FAVORITO").get_element().innerHTML='<IMG style="TEXT-ALIGN: center" src="<%=System.Configuration.ConfigurationManager.AppSettings("ruta")%>App_themes/<%=Page.Theme%>/images/favoritos_no.gif">'
                    }    
                    params = { codProve: sProveCod, accion: sAccion };
                    $.ajax({
                        type: "POST",
                        url: rutaFS + '_Common/App_Services/Consultas.asmx/CambiarProveFavoritosBD',
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(params),
                        dataType: "json",
                        async: true
                    });                      
                    e.getSelectedCells().clear()      
                }
                else {                    
                    if (document.getElementById("desde").value == "BuscadorArt") {
                        p = window.opener;
                        p.document.getElementById(document.getElementById("HiddenFieldID").value).value = sProveCod;
                        p.document.getElementById(document.getElementById("IDCONTROL").value).focus()
                        p.document.getElementById(document.getElementById("IDCONTROL").value).value = sProveCod + ' - ' + sProveDen;   

                        window.close();

                        return
                    }
                    if (document.getElementById("<%=chkAnyadirProvFav.ClientID%>")) {
                        if (document.getElementById("<%=chkAnyadirProvFav.ClientID%>").checked == true) //Anyadir el proveedor a favoritos
                        {
                            var favorito = e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("FAVORITO").get_value();
                            if (favorito == TextoNo) {
                                sAccion = "InsertarAut"
                                params = { codProve: sProveCod, accion: sAccion };
                                $.ajax({
                                    type: "POST",
                                    url: rutaFS + '_Common/App_Services/Consultas.asmx/CambiarProveFavoritosBD',
                                    contentType: "application/json; charset=utf-8",
                                    data: JSON.stringify(params),
                                    dataType: "json",
                                    async: true
                                });  
                            }
                        }
                    }
                    if (e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("ID_CONTACTO")) {
                        var lIdContacto = e.getSelectedCells().getItem(0).get_row().get_cell(3).get_value();
                        var sDenContacto = e.getSelectedCells().getItem(0).get_row().get_cell(4).get_value();
                    }

                    if (document.getElementById("IDCONTROL").value == "") {
                        if (e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("ID_CONTACTO"))
                            window.opener.prove_seleccionado2(sCIF, sProveCod, sProveDen, lIdContacto, sDenContacto)
                        else
                            window.opener.prove_seleccionado2(sCIF, sProveCod, sProveDen)
                    }
                    else {
                        if ((document.getElementById("desde").value == "RecepcionesEP") || (document.getElementById("desde").value == "SeguimientoEP")) {
                            p = window.opener;
                            p.document.getElementById(document.getElementById("HiddenFieldID").value).value = sProveCod;
                            p.document.getElementById(document.getElementById("IDCONTROL").value).focus()
                            p.document.getElementById(document.getElementById("IDCONTROL").value).value = sProveCod + ' - ' + sProveDen;    
                        }else{
                            if (document.getElementById("desde").value == "WebPart") {
                            p = window.opener;
                            var oLista = p.document.getElementById(document.getElementById("IDCONTROL").value);
                            var listaCodProves = p.document.getElementById(document.getElementById("codProves").value);
                            var listaDenProves = p.document.getElementById(document.getElementById("denProves").value);
                            bEncontrado = false;
                            for (i = 0; i < oLista.options.length; i++) {
                                elem = oLista.options[i]
                                if (elem) {
                                    if (elem.value == sProveCod)
                                        bEncontrado = true;
                                }
                            }
                            if (bEncontrado) {
                                alert(textoAlert);
                            } else {
                                var opt = p.document.createElement("option");
                                opt.text = sProveDen;
                                opt.value = sProveCod;
                                oLista.options.add(opt);
                                if (listaCodProves.value != '') {
                                    listaCodProves.value = listaCodProves.value + '#';
                                    listaDenProves.value = listaDenProves.value + '#';

                                }
                                listaCodProves.value = listaCodProves.value + sProveCod;
                                listaDenProves.value = listaDenProves.value + sProveDen;

                                p.document.getElementById(document.getElementById("tipoProve").value).checked = true
                            }
                        }
                        else {
                            if (document.getElementById("desde").value == "altaNoConformidad") {
                                p = window.opener;
                                p.document.getElementById("Proveedor").value = sProveCod;
                                p.document.getElementById(document.getElementById("IDCONTROL").value).value = sProveDen;
                                p.__doPostBack(document.getElementById("IDCONTROL").value, '');
                            }
                            else {
                                if (document.getElementById("desde").value == "TextBoxAndHiddenField") {
                                    p = window.opener;
                                    p.document.getElementById(document.getElementById("HiddenFieldID").value).value = sProveCod;
                                    p.document.getElementById(document.getElementById("IDCONTROL").value).value = sProveDen;
                                    p.__doPostBack(document.getElementById("IDCONTROL").value, '');
                                }
                                else {
                                    if (document.getElementById("desde").value == "VisorCert") {
                                        p = window.opener;
                                        p.document.getElementById(document.getElementById("IDCONTROLHID").value).value = sProveCod;
                                        p.document.getElementById(document.getElementById("IDCONTROL").value).value = sProveDen;
                                    }
                                    else {
                                        if (document.getElementById("desde").value == "ObjSuelos") {
                                            window.opener.prove_seleccionado(sProveCod, sProveDen)
                                        }
                                        else {
                                            if (e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("CONTACTO") != null) {
                                                var ComboContacto = $find(e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("CONTACTO").get_element().childNodes[1].id)
                                                for (j = 0; j < ComboContacto.get_selectedItems().length; j++) {
                                                    sIdsContactos = sIdsContactos + ComboContacto.get_selectedItems()[j].get_value() + "|";
                                                    sEmailsContactos = sEmailsContactos + ComboContacto.get_selectedItems()[j].get_text().substring(ComboContacto.get_selectedItems()[j].get_text().lastIndexOf('(') + 1, ComboContacto.get_selectedItems()[j].get_text().lastIndexOf(')')) + ", ";
                                                }
                                                if (sEmailsContactos.length > 0)
                                                    sEmailsContactos = sEmailsContactos.substring(0, sEmailsContactos.length - 2)
                                            }

                                            if ((document.getElementById("IDCONTROL").value.search("PART") != -1) || (document.getElementById("IDCONTROL").value.search("ugtxtRoles") != -1)) {
                                                if (document.getElementById("FilaGrid").value == "")
                                                    window.opener.prove_seleccionado3(document.getElementById("IDCONTROL").value, sProveCod, sProveDen, sIdsContactos, sEmailsContactos)
                                                else
                                                    window.opener.prove_seleccionado3(document.getElementById("FilaGrid").value, sProveCod, sProveDen, sIdsContactos, sEmailsContactos)
                                            } else {
                                                if (document.getElementById("desde").value == "VisorSol") {
                                                    //igual q el autocomplete cod guion den
                                                    p = window.opener;
                                                    p.document.getElementById(document.getElementById("IDCONTROLHID").value).value = sProveCod;
                                                    p.document.getElementById(document.getElementById("IDCONTROL").value).value = sProveCod + '-' + sProveDen;
                                                }
                                                else {
                                                    var Esdesde2904 = 0;
                                                    if (document.getElementById("desde2904")) {
                                                        if (document.getElementById("desde2904").value != "") {
                                                            var sPag = e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("PAG").get_value();
                                                            var sDenPag = e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("DENPAG").get_value();

                                                            if (document.getElementById("desde2904").value == "PedidoExpress") {
                                                                Esdesde2904 = 1;
                                                            }
                                                            if (document.getElementById("desde2904").value == "PedidoNegociado") {
                                                                Esdesde2904 = 2;
                                                            }
                                                        }
                                                    }

                                                    if (Esdesde2904 == 1) {
                                                        window.opener.prove_seleccionado(document.getElementById("IDCONTROL").value, sProveCod, sProveDen, sIdsContactos, document.getElementById("IDFPago").value, document.getElementById("IDFPagoHidden").value, sPag, sDenPag);
                                                    } else {
                                                        if (Esdesde2904 == 2) {
                                                            if (document.getElementById("hProveedorIniNegociado").value != "") {
                                                                if (document.getElementById("hProveedorIniNegociado").value != sProveCod) {
                                                                    if (document.getElementById("Rellenos").value > 0) {
                                                                        confirm(mensajeNegociadoNoAcepCambioProve)
                                                                        return
                                                                    }
                                                                }
                                                            }
                                                            window.opener.prove_seleccionado(document.getElementById("IDCONTROL").value, sProveCod, sProveDen, sIdsContactos, document.getElementById("IDFPago").value, document.getElementById("IDFPagoHidden").value, sPag, sDenPag);
                                                        } else {
                                                            window.opener.prove_seleccionado(document.getElementById("IDCONTROL").value, sProveCod, sProveDen, sIdsContactos, null, null, null, null);
                                                        }
                                                    }
                                                }
                                            }
                                        }                                        
                                    }
                                }
                            }
                        }
                        }
                        
                    }
                    if (!bEncontrado)
                        window.close();
                }
            }
        }


        /*''' <summary>
        ''' Establece el texto visible del dropdown tras una selección en el webdatagrid de organizaciones
        ''' </summary>
        ''' <param name="sender">is the object which is raising the event</param>
        ''' <param name="e">s the RowSelectionChangedEventArgs</param>        
        ''' <remarks>Llamada desde: ugtxtOrganizacionCompras ; Tiempo máximo: 0 </remarks>*/
        function WebDataGrid_RowSelectionChanged(sender, e) {

            //Gets the selected rows collection of the WebDataGrid
            var selectedRows = e.getSelectedRows();
            //Gets the row that is selected from the selected rows collection
            var row = selectedRows.getItem(0);
            //Gets the second cell object in the row
            //In this case it is ProductName cell 
            var cell = row.get_cell(1);
            //Gets the text in the cell
            var text = cell.get_text();
            
            //Gets reference to the WebDropDown
            var dropdown = null;
            dropdown = $find("<%= ugtxtOrganizacionCompras.clientID %>");
            if (dropdown != null) {
                //Sets the text of the value display to the product name of the selected row
                dropdown.set_currentValue(text, true);

                dropdown.closeDropDown();
                
                document.getElementById("hidOrg").value = text;                
            }
        }               
</script>
    <form id="frmBuscadorProves"  name="frmBuscadorProves" method="post" runat="server" defaultbutton="btnBuscar">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
        </asp:ScriptManager>        
        <input runat="server" id="desde" name="desde" type="hidden" />
        <input runat="server" id="desde2904" name="desde2904" type="hidden" />
        <input id="Rellenos" type="hidden" name="Rellenos" runat="server" />
        <input runat="server" id="IDFPago" name="IDFPago" type="hidden" />
        <input runat="server" id="IDFPagoHidden" name="IDFPagoHidden" type="hidden" />
        <input runat="server" id="hProveedorIniNegociado" name="hProveedorIniNegociado" type="hidden" />
        <input runat="server" id="EsBtPara" name="desde" type="hidden" />
        <input runat="server" id="EsBtCC" name="desde" type="hidden" />
        <input runat="server" id="EsBtCCO" name="desde" type="hidden" />
        <input runat="server" id="HiddenLista" name="HiddenLista" type="hidden" />
        <input type="hidden" id="codProves" name="codProves2" runat="server" />
        <input type="hidden" id="denProves" name="denProves" runat="server" />
        <input type="hidden" id="tipoProve" name="tipoProve" runat="server" />  
        <input type="hidden" id="FilaGrid" name="tipoProve" runat="server" />      
        <asp:Panel ID="pnlTitulo" runat="server" Width="100%">
            <table width="100%" cellpadding="0" border="0">
                <tr>
                    <td>
                        <fsn:FSNPageHeader ID="FSNPageHeader" runat="server">
                        </fsn:FSNPageHeader>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="pnlFiltros" runat="server" Width="100%">
            <table id="Table1" width="100%" style="table-layout: fixed; padding-top: 4px; padding-bottom: 4px" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 65px; padding-left: 5px;">
                        <asp:Label ID="lblIdentificador" runat="server" Text="dCodigo:"></asp:Label>
                    </td>
                    <td style="width: 150px">
                        <asp:TextBox ID="txtIdentificador" runat="server" Width="140px"></asp:TextBox>
                    </td>
                    <td style="width: 105px;">
                        <asp:Label ID="lblDenominacion" runat="server" Text="dDenominacion:"></asp:Label>
                    </td>
                    <td style="width: 300px">
                        <asp:TextBox ID="txtDenominacion" runat="server" Width="290px"></asp:TextBox>
                    </td>
                    <td style="width: 45px;">
                        <asp:Label ID="lblCif" runat="server" Text="dCIF:"></asp:Label>
                    </td>
                    <td style="width: 150px">
                        <asp:TextBox ID="txtCif" runat="server" Width="144px"></asp:TextBox>
                    </td>                                        
                </tr>
                <tr id="Linea1" runat="server">
                    <td colspan="4" style="width: 640px;">                    
                    <div style="clear:both;">
                        <div style="width:100%; float:left; margin-top:4px;">
                            <div id="divlblMaterial" runat="server" style="width: 65px; padding-left: 5px; float:left;">
                                <asp:Label ID="lblMaterial" runat="server" Text="dMaterial:"></asp:Label>
                            </div>
                            <div id="divlblArticulo" runat="server" style="width: 65px; padding-left: 5px; float:left;">
                                <asp:Label ID="lblArticulo" runat="server" Text="dArtículo:"></asp:Label>
                            </div>
                            <div id="divMaterial" runat="server" style="width: 148px; float:left;">
                                <table style="height:20px; width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="width:100%;">
                                            <asp:TextBox ID="txtMaterial" runat="server" Width="130px" BorderWidth="1px"></asp:TextBox>
                                        </td>
                                        <td style="padding-left:2px;padding-right:0px">
                                            <asp:ImageButton id="imgMaterialLupa" runat="server" SkinID="BuscadorLupa" /> 
                                        </td>
                                    </tr>                                                                                      
                                </table>
                                <asp:HiddenField ID="hidMaterial" runat="server" />  
                            </div>
                            <div id="divArticulo" runat="server" style="width: 148px; float:left;">
                                <table style="height:20px; width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="width:100%;">
                                            <asp:Label ID="txtArticulo" runat="server" Width="148px"></asp:Label>
                                        </td>
                                    </tr>                                                                                      
                                </table>
                            </div>
                            <div id="divLblOrg" runat="server" style="width: 101px; float:left; margin-left: 6px;">
                                <asp:Label ID="lblOrganizacionCompras" runat="server" CssClass="parrafo">Org Compras</asp:Label>
                            </div>
                            <div id="divWddOrg" runat="server" style="width: 298px; float:left;">
                                <ig:WebDropDown ID="ugtxtOrganizacionCompras" 
                                    runat="server" Width="294px" EnableClosingDropDownOnBlur="true" EnableClosingDropDownOnSelect="true"
                                    DropDownContainerWidth="293px">
                                    <Items>
                                    <ig:DropDownItem>
                                    </ig:DropDownItem>
                                </Items>
                                    <ItemTemplate>
                                    <ig:WebDataGrid ID="ugtxtOrganizacionCompras_wdg" runat="server"
                                        AutoGenerateColumns="false" Width="100%" ShowHeader="false" 
                                        OnRowSelectionChanged ="ugtxtOrganizacionCompras_SelectedRowChanged">
                                        <Columns>
                                            <ig:BoundDataField DataFieldName="COD" Key="COD" Width="20%">
                                            </ig:BoundDataField>
                                            <ig:BoundDataField DataFieldName="DEN" Key="DEN" Width="80%">
                                            </ig:BoundDataField>
                                        </Columns>
                                        <Behaviors>
                                            <ig:Selection RowSelectType="Single" Enabled="True" CellClickAction="Row">
                                                <SelectionClientEvents RowSelectionChanged="WebDataGrid_RowSelectionChanged" />
                                            </ig:Selection>
                                        </Behaviors>
                                    </ig:WebDataGrid>
                                </ItemTemplate>
                                </ig:WebDropDown>                                    
						        <asp:label id="lblOrganizacionComprasReadOnly" runat="server" CssClass="Etiqueta"></asp:label>
                                <asp:HiddenField ID="hidOrg" runat="server" /> 
                            </div>
                        </div>
                    </div>
                    </td>
                    <td colspan="2">
                        <asp:CheckBox ID="chkSoloFavoritos" runat="server" CssClass="parrafo" Checked="false"></asp:CheckBox>
                        <asp:ImageButton id="imgSoloFavoritos" runat="server" SkinID="CabFavorito" /> 
                        <asp:Label ID="lblSoloFavoritos" runat="server" Text="dMostrar solo favoritos"></asp:Label>
                    </td>
                </tr>
                <tr id="Linea2" runat="server">
                    <td colspan="6" style="padding-left: 5px;">
                        <asp:CheckBox ID="chkAnyadirProvFav" runat="server"
                        Text="Añadir automáticamente los proveedores seleccionados a la lista de favoritos"></asp:CheckBox>
                    </td>
                </tr>
                <tr id="Linea3" runat="server" visible="false">
                    <td colspan="6" style="width: 640px; padding-top:5px; padding-bottom :5px;">                        
                        <div style="clear:both;width:50%; float:left; margin-top:4px;">
                            <div style="overflow:auto; padding-left: 5px; float:left;">
                                <asp:Label ID="lblMaterialQA" runat="server" Text="dMaterial QA:"></asp:Label>
                            </div>
                            <div style="overflow:auto; padding-left: 5px; float:left;">
                                <asp:Label ID="lblMaterialQADen" runat="server" Text="" CssClass="itemBold"></asp:Label>                                
                            </div>
                        </div>
                        <div style="width:50%; margin-top:4px;float:left;">
                            <div style="overflow:auto; float:left;">
                                <asp:Label ID="lblUNQA" runat="server" Text="dUnidad de Negocio:"></asp:Label>
                            </div>
                            <div style="overflow:auto; padding-left: 5px; float:left;">
                                <asp:Label ID="lblUNQADen" runat="server" Text="" CssClass="itemBold"></asp:Label>
                            </div>
                        </div> 
                        <div style="clear:both;width:100%; float:left; margin-top:4px; padding-top:5px; padding-bottom :5px;">                            
                            <input id="chkProveCriticos" type="checkbox" runat="server" />  
                            <asp:Label ID="lblProvesCriticos" runat="server" Text="dProveedoresCriticos"></asp:Label>                          
                        </div>                       
                    </td> 
                </tr> 
                <tr>
                    <td colspan="6" style="padding-left: 5px;">
                        <table style="width:100%;">
                            <tr>
                                <td>
                                    <fsn:FSNButton ID="btnBuscar" runat="server" Text="DBuscar" Alineacion="Left">                    
                                        &nbsp;&nbsp;&nbsp;&nbsp;           
                                    </fsn:FSNButton>
                                </td>
                                <td>
                                    <fsn:FSNButton ID="btnLimpiar" runat="server" Text="DLimpiar" OnClientClick="Limpiar();return false;" Alineacion="Left">                    
                                        &nbsp;&nbsp;&nbsp;&nbsp;                    
                                    </fsn:FSNButton>
                                </td>
                                <td style="width:690px; margin-left:5px;">                                    
                                    <asp:Label ID="lblPulse" runat="server" Text="d(Pulse sobre un proveedor para seleccionarlo)" CssClass="ColorAzul"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        
        <asp:Panel ID="pnlGrid" runat="server" Width="99%">
            <div class="BusqProve">
                <ig:WebDataGrid  ID="wdgDatos" runat="server" Width="100%" ShowHeader="true" AutoGenerateColumns="false" EnableViewState="true">
                    <Behaviors>                        
                        <ig:Selection  RowSelectType="Single" Enabled="True" CellClickAction="Cell" >
                            <SelectionClientEvents CellSelectionChanged="wdgDatos_CellSelectionChanged"/>
                        </ig:Selection>                                
                        <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true" AnimationEnabled="true">
                            <FilteringClientEvents DataFiltered="AdministrarPaginador" />
                        </ig:Filtering> 
                        <ig:Sorting Enabled="true" SortingMode="Multi"></ig:Sorting> 
                        <ig:RowSelectors Enabled="false" RowNumbering="false"></ig:RowSelectors>
                        <ig:Paging Enabled="true" PagerAppearance="Top">
                            <PagingClientEvents PageIndexChanged="wdgDatos_PageIndexChanged" />
                            <PagerTemplate>
							<div class="CabeceraBotones" style="clear:both; float:left; width:100%;"> 
							<div style="float:left; padding:3px 0px 2px 0px; margin-left:10px;">
								<div style="clear: both; float: left; margin-right: 5px;">
                                    <asp:Image ID="ImgBtnFirst" runat="server" OnClientClick="FirstPage()" style="margin-right:2px;" />
                                    <asp:Image ID="ImgBtnPrev" runat="server" OnClientClick="PrevPage()" />
                                </div>
                                <div style="float: left; margin-right:5px;">
                                    <asp:Label ID="lblPage" runat="server" Text="dPage" Style="margin-right: 3px;"></asp:Label>
                                    <asp:DropDownList ID="PagerPageList" runat="server" Style="margin-right: 3px;" onchange="return IndexChanged()" />
                                    <asp:Label ID="lblOF" runat="server" Text="dOf" Style="margin-right: 3px;"></asp:Label>
                                    <asp:Label ID="lblCount" runat="server" />
                                </div>
                                <div style="float: left;">
                                    <asp:Image ID="ImgBtnNext" runat="server" OnClientClick="NextPage()" style="margin-right:2px;" />
                                    <asp:Image ID="ImgBtnLast" runat="server" OnClientClick="LastPage()" />
                                </div>
							</div>   
                            </div>  
                            </PagerTemplate>
                        </ig:Paging>
                        <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
                        <ig:Activation Enabled="true"></ig:Activation>
                    </Behaviors>                
                </ig:WebDataGrid>
            </div>
        </asp:Panel>    
                          
        <asp:Panel ID="btnMail" runat="server">
            <table style="width:100%;">
                <tr>
                    <td style="width:50%;">
                        <fsn:FSNButton ID="btnAceptar" runat="server" Text="DAceptar" OnClientClick="return Aceptar();" >                    
                        &nbsp;&nbsp;&nbsp;&nbsp;                    
                        </fsn:FSNButton>
                    </td>
                    <td>
                        <fsn:FSNButton ID="btnCancelar" runat="server" Text="DCancelar" Alineacion="Left" OnClientClick="return Cancelar();">                    
                        &nbsp;&nbsp;&nbsp;&nbsp;                    
                        </fsn:FSNButton>                        
                    </td>
                </tr>
            </table>
        </asp:Panel> 
        

        <input type="hidden" id="HiddenFieldID" name="HiddenFieldID" runat="server" />        
        <input id="USAR_ORGCOMPRAS" style="z-index: 107; left: 0px; position: absolute; top: 0px"
            type="hidden" name="USAR_ORGCOMPRAS" runat="server"/>   
        <input id="VISIBILIDAD" style="z-index: 104; left: 0px; position: absolute; top: 0px"
            type="hidden" name="VISIBILIDAD" runat="server"/>  
        <input id="IDDEPEN" style="z-index: 106; left: 0px; position: absolute; top: 0px"
            type="hidden" name="IDDEPEN" runat="server"/>        
        <input id="DENORGCOMPRAS" style="z-index: 105; left: 0px; position: absolute; top: 0px"
            type="hidden" name="DENORGCOMPRAS" runat="server"/>                
        <input id="IDCONTROL" style="z-index: 103; left: 0px; position: absolute; top: 0px"
            type="hidden" name="IDCAMPO" runat="server"/>
        <input id="IDCONTROLHID" style="z-index: 103; left: 0px; position: absolute; top: 0px"
            type="hidden" name="IDCAMPO" runat="server"/>                                           

		<script type="text/javascript">

			//''' <summary>
			//''' Pregunta si desea borrar los artículos de la pantalla q llamo a esta.
			//''' </summary>
			//''' <remarks>Llamada desde: Aceptar() ; Tiempo máximo: 0,2</remarks>
			function Confirm(msg) {
			    $get('divConfirmContent').innerHTML = msg;
			    popUpShowed = $find('Confirm');
			    popUpShowed.show();
			    popUpShowed._backgroundElement.style.zIndex += 10;
			    popUpShowed._foregroundElement.style.zIndex += 10;
			}

			window.confirm = Confirm;

			//''' <summary>
			//''' Responde no a la pregunta si desea borrar los artículos de la pantalla q llamo a esta.
			//''' </summary>
			//''' <remarks>Llamada desde: mpeConfirm.OnOkScript ; Tiempo máximo: 0,2</remarks>
			function PreguntaNo() {
			    var grid = null;
                grid = $find("<%=wdgDatos.clientID %>");
                if (grid) {
                    grid.get_behaviors().get_selection().get_selectedCells().remove(grid.get_behaviors().get_selection().get_selectedCells().getItem(0))
                }
			}

			//''' <summary>
			//''' Responde sí a la pregunta si desea borrar los artículos de la pantalla q llamo a esta.
			//''' </summary>
			//''' <remarks>Llamada desde: mpeConfirm.OnOkScript ; Tiempo máximo: 0,2</remarks>
			function PreguntaSi() {			        
			    var grid = null;
                grid = $find("<%=wdgDatos.clientID %>");
                if (grid) {
                    row = grid.get_behaviors().get_selection().get_selectedCells().getItem(0).get_row();
                    if (row) {

                        p = window.opener
                        p.Borra_Los_Articulos()

                        var sProveCod = row.get_cellByColumnKey("COD").get_value();
                        var sProveDen = row.get_cellByColumnKey("DEN").get_value();
                        var sPag = row.get_cellByColumnKey("PAG").get_value();
                        var sDenPag = row.get_cellByColumnKey("DENPAG").get_value();

                        p.prove_seleccionado(document.getElementById("IDCONTROL").value, sProveCod, sProveDen, null, document.getElementById("IDFPago").value, document.getElementById("IDFPagoHidden").value, sPag, sDenPag);

                        window.close();
                    }
                }
			}
		</script>

        <!-- Pregunta de validación -->
		<asp:Button ID="btnConfirm" runat="server" Style="display: none" />
        <ajx:ModalPopupExtender ID="mpeConfirm" runat="server" Enabled="True" PopupControlID="panConfirm" DropShadow="true" TargetControlID="btnConfirm" 
        OkControlID="btnAceptarConfirm" CancelControlID="btnCancelConfirm" BehaviorID="Confirm" BackgroundCssClass="modalBackground" 
        OnOkScript="PreguntaSi();popUpShowed = null;" OnCancelScript="PreguntaNo();popUpShowed = null"/>
        <asp:Panel ID="panConfirm" runat="server" Style="display: none" Width="270px" Height="85px" CssClass="modalPopup">
             <table>
             <tr>
                <td height="5px" colspan="3"></td>
            </tr>
             <tr>
                <td width="5px"></td>
                <td><asp:Image ID="Image3" runat="server" SkinId="ErrorGrande" /></td>
                <td><div id="divConfirmContent"></div></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="2" align="center">
                    <table>
                    <tr>
                        <td><fsn:FSNButton ID="btnAceptarConfirm" runat="server" Text="OK" Alineacion="Left"></fsn:FSNButton></td>
                        <td><fsn:FSNButton ID="btnCancelConfirm" runat="server" Text="Cancel" Alineacion="Left"></fsn:FSNButton></td>
                    </tr>
                    </table>
                </td>
            </tr>
            </table>
        </asp:Panel>

    </form>  
  </body>
</html>
