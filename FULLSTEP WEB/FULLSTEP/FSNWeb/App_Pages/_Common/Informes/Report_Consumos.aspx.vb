﻿Imports CrystalDecisions.CrystalReports.Engine
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports CrystalDecisions.Shared
Imports Infragistics.WebUI.WebSchedule

Partial Public Class Report_Consumos
    Inherits FSNPage

    Private Report As New ReportDocument
    Private vEstructura(2) As String

    ''' <summary>
    ''' Evento de carga inicial de la página, en el que cargaremos los textos y los controles correspondientes a los parámetros del report solicitado.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Tiempo máximo: Depende del nº y tipo de los parámetros configurados en el report (si han de acceder a BBDD, etc...)</remarks>
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        Master.Seleccionar("Informes", "Informes")

        ModuloIdioma = ModulosIdiomas.Listados

        cmdObtener.Text = Textos(6) ' Actualizar
        lblNoExiste.Text = Textos(5) ' Imposible mostrar el listado: Ha sido eliminado de su ubicación física.
        lblParametros.Text = Textos(7) ' Parámetros
        lblTitulo.Text = Request("ReportName")


        If IsPostBack And Not IsNothing(Cache("MiReportConsumos")) Then
            'Si estamos haciendo postback y tenemos el report en la caché lo recuperamos
            Report = Cache("MiReportConsumos")
            Me.lblNoExiste.Visible = False
        Else
            'Si no, eliminamos el report de la caché 
            Cache.Remove("MiReportConsumos")

            Dim sFileName As String

            sFileName = ConfigurationManager.AppSettings("rptFilesPath") & "/" & Request("FileReport")

            If Not IO.File.Exists(sFileName) Then
                'Si no existe el listado en esa ubicación sacamos el mensaje
                Me.tblRptParams.Visible = False
                Me.cmdObtener.Visible = False
                Me.lblNoExiste.Visible = True
                Exit Sub
            Else
                Me.lblNoExiste.Visible = False
            End If

            'Cargamos el report
            Report.Load(sFileName, OpenReportMethod.OpenReportByTempCopy)

            PonerTextosReport()

        End If

        CargarParametrosEnPagina()

        If IsPostBack And Not IsNothing(Cache("MiReportConsumos")) Then
            'Si estamos haciendo postback y tenemos el report en la caché (esto es, cuando
            'el usuario está paginando el report), se lo pasamos al visor
            CrystalReportViewer1.ReportSource = Report
        End If
    End Sub

    ''' <summary>
    ''' Procedimiento que cargará un control por cada parámetro que haya en el report, con sus valores correspondientes.
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: El evento Page_Init de Report.aspx;
    ''' Tiempo máximo: Depende del nº y tipo de los parámetros configurados en el report (si han de acceder a BBDD, etc...)
    ''' </remarks>
    Sub CargarParametrosEnPagina()

        Dim otblRow As System.Web.UI.WebControls.TableRow
        Dim otblCell As System.Web.UI.WebControls.TableCell
        Dim oLabel As System.Web.UI.WebControls.Label

        If Not IsPostBack Then

            'APROVISIONADOR
            Me.lblAprov.Text = Textos(9) 'Aprovisionador:
            If FSNUser.EPTipo = 2 Or FSNUser.EPTipo = 3 Then
                Dim dsAprov As DataSet
                Dim oRow As DataRow
                Dim bYaEnLista As Boolean
                Dim oCAprovisionadores As FSNServer.CAprovisionadores = Me.FSNServer.Get_Object(GetType(FSNServer.CAprovisionadores))
                dsAprov = oCAprovisionadores.DevolverAprovisionadoresParaAprobador(FSNUser.CodPersona)

                If dsAprov.Tables(0).Rows.Count > 0 Then
                    For Each oRow In dsAprov.Tables(0).Rows
                        Me.ddlAprov.Items.Add(oRow("COD") & " - " & oRow("DEN"))
                        If oRow("COD") = FSNUser.CodPersona Then
                            bYaEnLista = True
                        End If
                    Next
                End If

                If Not bYaEnLista Then
                    Me.ddlAprov.Items.Add(FSNUser.CodPersona & " - " & FSNUser.Nombre)
                End If
                trAprov.Visible = True
            Else
                trAprov.Visible = False
            End If

        End If

        'FECHA DESDE
        Me.lblDesde.Text = Textos(10) 'Desde fecha:
        Me.dteDesde.Value = DateAdd(DateInterval.Month, -1, Now)

        'FECHA HASTA
        Me.lblHasta.Text = Textos(11) 'Hasta fecha:
        Me.dteHasta.Value = Now

        ' Leemos los parámetros que tiene el listado
        Dim cont As Integer = 0
        Dim crParam As ParameterField

        If Report.ParameterFields.Count > 0 Then
            'Se instancia la Fila que va a albergar todos los parámetros
            otblRow = New System.Web.UI.WebControls.TableRow
            'Se instancia la Celda que va a albergar todos los parámetros
            otblCell = New System.Web.UI.WebControls.TableCell

            'Creamos el dataset que vamos a usar para recoger los valores por defecto que puedan tener los parámetros
            Dim dsParametros As New DataSet
            Dim oRow(0)
            For Each crParam In Report.ParameterFields
                If Report.Subreports.Item(crParam.ReportName) Is Nothing And _
                crParam.Name <> "APROBADOR" And _
                crParam.Name <> "APROVISIONADOR" And _
                crParam.Name <> "DESDEFECHA" And _
                crParam.Name <> "HASTAFECHA" Then

                    'Se instancia la Fila que va a albergar a éste parámetro
                    otblRow = New System.Web.UI.WebControls.TableRow
                    'Se instancia la Celda que va a albergar a la label con el título de este parámetro
                    otblCell = New System.Web.UI.WebControls.TableCell

                    'Se instancia la Etiqueta que tendrá el nombre del parámetro
                    oLabel = New System.Web.UI.WebControls.Label
                    'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
                    'ANTES
                    'If crParam.Name = "FILTRO" Then 'Si es el parámetro FILTRO pondremos el texto de BBDD
                    '    oLabel.Text = " " & Textos(23)
                    'Else 'Si es un parámetro no controlado (por nuevo) entonces sacamos su texto del que tenga puesto en el report
                    '    oLabel.Text = " " & crParam.PromptText
                    'End If
                    'AHORA
                    'Vamos a controlar si lo que recibimos es un número. Si no lo es, actuamos como hasta ahora pero si lo es, significa que
                    'hay que recuperar ese número mediante la función Textos PERO, IMPORTANTE, el número recibido va a ser el que corresponde en base de datos al
                    'texto, por lo que hay q restarle uno para recuperarlo del array
                    Dim numTexto As Integer = 0
                    Try
                        numTexto = CInt(crParam.PromptText)
                    Catch ex As Exception
                        'ignoramos el catch, ya lo controlamos con el numTexto=0
                    End Try
                    If numTexto <> 0 Then
                        oLabel.Text = " " & Textos(numTexto - 1)
                    Else
                        If crParam.Name = "FILTRO" Then 'Si es el parámetro FILTRO pondremos el texto de BBDD
                            oLabel.Text = " " & Textos(23)
                        Else 'Si es un parámetro no controlado (por nuevo) entonces sacamos su texto del que tenga puesto en el report
                            oLabel.Text = " " & crParam.PromptText
                        End If
                    End If
                    'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

                    oLabel.Font.Name = "Verdana"
                    oLabel.Font.Size = System.Web.UI.WebControls.FontUnit.Point(8)

                    otblCell.Controls.Add(oLabel)

                    otblRow.Cells.Add(otblCell)

                    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
                    crParameterFieldDefinitions = Report.DataDefinition.ParameterFields()

                    'Se instancia la Celda que va a albergar al control con el contenido de este parámetro
                    otblCell = New System.Web.UI.WebControls.TableCell

                    If crParam.DefaultValues.Count > 0 Then
                        'Tiene valores Predefinidos

                        Dim j As Integer = 0
                        dsParametros.Reset()
                        dsParametros.Tables.Add("Parametros")
                        dsParametros.Tables("Parametros").Columns.Add("Valor1")
                        For j = 0 To crParam.DefaultValues.Count - 1
                            'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
                            'ANTES
                            'oRow(0) = DirectCast(crParameterFieldDefinitions.Item(cont).DefaultValues(j), ParameterDiscreteValue).Value & " - " & crParameterFieldDefinitions.Item(cont).DefaultValues(j).Description
                            'AHORA
                            'Vamos a controlar si lo que recibimos es un número. Si no lo es, actuamos como hasta ahora pero si lo es, significa que
                            'hay que recuperar ese número mediante la función Textos PERO, IMPORTANTE, el número recibido va a ser el que corresponde en base de datos al
                            'texto, por lo que hay q restarle uno para recuperarlo del array
                            numTexto = 0
                            Try
                                numTexto = CInt(crParameterFieldDefinitions.Item(cont).DefaultValues(j).Description)
                            Catch ex As Exception
                                'ignoramos el catch, ya lo controlamos con el numTexto=0
                            End Try
                            oRow(0) = DirectCast(crParameterFieldDefinitions.Item(cont).DefaultValues(j), ParameterDiscreteValue).Value
                            If numTexto <> 0 Then
                                oRow(0) = oRow(0) & " - " & Textos(numTexto - 1)
                            Else
                                oRow(0) = oRow(0) & " - " & crParameterFieldDefinitions.Item(cont).DefaultValues(j).Description
                            End If
                            'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

                            dsParametros.Tables("Parametros").Rows.Add(oRow)
                        Next

                        Dim oLista As New System.Web.UI.WebControls.DropDownList
                        With oLista
                            .ID = crParam.Name
                            .EnableViewState = True
                            .DataSource = dsParametros
                            .DataValueField = "Valor1"
                            .DataTextField = "Valor1"
                            .DataBind()
                        End With
                        otblCell.Controls.Add(oLista)
                    Else
                        'El parametro es de tipo Texto 
                        Dim oTexto As New System.Web.UI.WebControls.TextBox
                        With oTexto
                            .EnableViewState = True
                            .ID = crParam.Name
                            .Width = Unit.Pixel(250)
                        End With
                        otblCell.Controls.Add(oTexto)
                    End If

                    'Insertamos el RequiredFieldValidator ya que los parámetros de este formulario van a ser obligatorios
                    Dim oReq As New RequiredFieldValidator
                    With oReq
                        .Display = ValidatorDisplay.Dynamic
                        .ErrorMessage = Textos(8) 'Parámetro obligatorio
                        .ControlToValidate = crParam.Name
                        .EnableClientScript = False
                    End With
                    otblCell.Controls.Add(oReq)

                    otblRow.Cells.Add(otblCell)
                    otblRow.Height = Unit.Pixel(20)

                    tblParametros.Rows.Add(otblRow)
                End If

                cont = cont + 1
            Next
        End If
    End Sub

    ''' <summary>
    ''' Evento que se lanza al pulsar sobre el botón Actualizar, en el que pasaremos al informe los valores seleccionados por el usuario para cada parámetro, los parámetros de conexión a la BBDD y mostraremos el informe 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' Tiempo máximo: 0 seg
    ''' </remarks>
    Private Sub cmdObtener_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdObtener.Click
        If Page.IsValid Then
            ' Pasamos los valores de los parámetros al listado
            Dim crParam As ParameterField
            Dim sFileName As String

            'Plegamos el panel de parámetros
            CollapsiblePanelExtender1.Collapsed = True
            CollapsiblePanelExtender1.ClientState = "true"

            Try
                sFileName = ConfigurationManager.AppSettings("rptFilesPath") & "/" & Request("FileReport")

                If Dir(sFileName) <> "" Then
                    Report.Load(sFileName, OpenReportMethod.OpenReportByTempCopy)

                    Dim lInstancia As Long
                    lInstancia = Request("Instancia")

                    If lInstancia <> 0 Then
                        For Each crParam In Report.ParameterFields
                            If Report.Subreports.Item(crParam.ReportName) Is Nothing Then
                                Report.SetParameterValue(crParam.Name, CLng(lInstancia))
                            End If
                        Next
                    Else
                        Dim strAprov As String
                        If FSNUser.EPTipo = 2 Or FSNUser.EPTipo = 3 Then
                            strAprov = CType(tblParametros.FindControl("ddlAprov"), DropDownList).SelectedValue
                            strAprov = Trim(Mid(strAprov, 1, InStr(strAprov, " - ")))
                        Else
                            strAprov = FSNUser.CodPersona
                        End If

                        For Each crParam In Report.ParameterFields
                            Select Case crParam.Name
                                Case "APROVISIONADOR"
                                    Report.SetParameterValue(crParam.Name, strAprov)

                                Case "APROBADOR"
                                    If FSNUser.EPTipo = 2 Or FSNUser.EPTipo = 3 Then
                                        If strAprov <> FSNUser.CodPersona Then
                                            Report.SetParameterValue(crParam.Name, FSNUser.CodPersona)
                                        Else
                                            Report.SetParameterValue(crParam.Name, "")
                                        End If
                                    Else
                                        Report.SetParameterValue(crParam.Name, "")
                                    End If
                                Case "DESDEFECHA"
                                    Report.SetParameterValue(crParam.Name, CType(tblParametros.FindControl("dteDesde"), Infragistics.Web.UI.EditorControls.WebDatePicker).Value)
                                Case "HASTAFECHA"
                                    Report.SetParameterValue(crParam.Name, CType(tblParametros.FindControl("dteHasta"), Infragistics.Web.UI.EditorControls.WebDatePicker).Value)
                                Case "FILTRO"
                                    Dim sFiltro As String
                                    sFiltro = CType(tblParametros.FindControl(crParam.Name), DropDownList).SelectedValue
                                    sFiltro = Trim(Mid(sFiltro, 1, InStr(sFiltro, " - ")))

                                    Report.SetParameterValue(crParam.Name, CLng(sFiltro))
                                Case Else
                                    If Report.Subreports.Item(crParam.ReportName) Is Nothing Then
                                        If crParam.DefaultValues.Count > 0 Then
                                            Select Case crParam.ParameterValueType
                                                Case ParameterValueKind.NumberParameter
                                                    Report.SetParameterValue(crParam.Name, CLng(IIf(CType(tblParametros.FindControl(crParam.Name), DropDownList).SelectedValue <> "", CType(tblParametros.FindControl(crParam.Name), DropDownList).SelectedValue, 0)))
                                                Case ParameterValueKind.DateParameter, ParameterValueKind.DateTimeParameter
                                                    Report.SetParameterValue(crParam.Name, CDate(CType(tblParametros.FindControl(crParam.Name), DropDownList).SelectedValue))
                                                Case Else
                                                    Report.SetParameterValue(crParam.Name, CType(tblParametros.FindControl(crParam.Name), DropDownList).SelectedValue)
                                            End Select
                                        Else
                                            Select Case crParam.ParameterValueType
                                                Case ParameterValueKind.NumberParameter
                                                    Report.SetParameterValue(crParam.Name, CLng(IIf(CType(tblParametros.FindControl(crParam.Name), TextBox).Text <> "", CType(tblParametros.FindControl(crParam.Name), TextBox).Text, 0)))
                                                Case ParameterValueKind.DateParameter, ParameterValueKind.DateTimeParameter
                                                    Report.SetParameterValue(crParam.Name, CDate(CType(tblParametros.FindControl(crParam.Name), TextBox).Text))
                                                Case Else
                                                    Report.SetParameterValue(crParam.Name, CType(tblParametros.FindControl(crParam.Name), TextBox).Text)
                                            End Select
                                        End If
                                    End If
                            End Select
                        Next
                    End If

                    Dim crtableLogoninfos As New TableLogOnInfos
                    Dim crtableLogoninfo As New TableLogOnInfo
                    Dim myConnectionInfo As New ConnectionInfo
                    Dim CrTables As Tables
                    Dim CrTable As Table

                    'Obtenemos las propiedades de conexión...
                    FSNServer.get_PropiedadesConexion()

                    With myConnectionInfo
                        .ServerName = FSNServer.DBServidor
                        .DatabaseName = FSNServer.DBName
                        .UserID = FSNServer.DBLogin
                        .Password = FSNServer.DBPassword
                    End With

                    'Recorremos las tablas del Report (los comandos están incluidos) 
                    'y les asignamos los parámetros de conexión
                    CrTables = Report.Database.Tables
                    For Each CrTable In CrTables
                        crtableLogoninfo = CrTable.LogOnInfo
                        crtableLogoninfo.ConnectionInfo = myConnectionInfo
                        CrTable.ApplyLogOnInfo(crtableLogoninfo)
                    Next

                    ' Pasamos el listado, ya con los datos de conexión y parámetros al formulario con el visor.
                    CrystalReportViewer1.ReportSource = Report

                    ' Pasamos las propiedades de conexión al Visor
                    SetDBLogonForReport(myConnectionInfo)

                    CrystalReportViewer1.Visible = True
                End If

                'Introducimos el objeto Report en la caché, para mantener así los parámetros entre postbacks                
                Me.InsertarEnCache("MiReportConsumos", Report)
            Catch ex As Exception

            End Try
        Else
            'Si no pasamos las validaciones de parámetros requeridos no mostramos el visor
            CrystalReportViewer1.Visible = False
        End If
    End Sub

    ''' <summary>
    ''' Procedimiento que vincula el visor de Crystal con los parámetros de conexión
    ''' </summary>
    ''' <param name="myConnectionInfo">Información de conexión (Server, BBDD, User, Pwd)</param>
    ''' <remarks>
    ''' Llamada desde: cmdActualizar_Click de Report.aspx
    ''' Tiempo máximo: 0 seg
    ''' </remarks>
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub

    ''' <summary>
    ''' Pone los textos al report
    ''' </summary>
    ''' <remarks>Llamada desde la carga inical de la pagina
    ''' Tiempo maximo 0 sec</remarks>
    Private Sub PonerTextosReport()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Listados

        For Each campo As CrystalDecisions.CrystalReports.Engine.FormulaFieldDefinition In Report.DataDefinition.FormulaFields
            Select Case campo.Name
                Case "txtArticulo"
                    Report.DataDefinition.FormulaFields("txtArticulo").Text = "'" & Textos(32) & "'"
                Case "txtProveedor"
                    Report.DataDefinition.FormulaFields("txtProveedor").Text = "'" & Textos(33) & "'"
                Case "txtCantidad"
                    Report.DataDefinition.FormulaFields("txtCantidad").Text = "'" & Textos(34) & "'"
                Case "txtUnidad"
                    Report.DataDefinition.FormulaFields("txtUnidad").Text = "'" & Textos(35) & "'"
                Case "txtImporte"
                    Report.DataDefinition.FormulaFields("txtImporte").Text = "'" & Textos(36) & "'"
                Case "txtFecha"
                    Report.DataDefinition.FormulaFields("txtFecha").Text = "'" & Textos(37) & "'"
                Case "txtImporteTotal"
                    Report.DataDefinition.FormulaFields("txtImporteTotal").Text = "'" & Textos(38) & "'"
                Case "txtMoneda"
                    Report.DataDefinition.FormulaFields("txtMoneda").Text = "'" & Textos(39) & "'"
                Case "txtDestino"
                    Report.DataDefinition.FormulaFields("txtDestino").Text = "'" & Textos(40) & "'"
            End Select
        Next
    End Sub
End Class