<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master" CodeBehind="Report.aspx.vb" Inherits="Fullstep.FSNWeb.Report" %>

<%@ MasterType VirtualPath="~/App_Master/Menu.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 125px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
    <table cellpadding="10" cellspacing="0" border="0" width="100%">
    <tr>
        <td>
            <img alt="" src="../../../images/cabinformes.jpg" />
            <asp:Label ID="lblTitulo" runat="server" Text="DListado" CssClass="Rotulo" Font-Size="14px" Height="40px"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:UpdatePanel ID="upPnlError" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" EnableViewState="false">
            <ContentTemplate>
                <asp:Panel ID="pnlError" runat="server" Visible="false" EnableViewState="false">
                    <table cellpadding="0" cellspacing="0" border="0" class="CajaError" style="height: 60px;min-height: 100px;min-width: 450px;">
                        <tr>
                            <td valign="top" align="left" nowrap="nowrap">
                                <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Panel runat="server" ID="pnlCabeceraInformes" SkinID="PanelColapsable">
                &nbsp; &nbsp;
                <asp:Image ID="imgExpandir" runat="server" ImageUrl="../../../images/contraer.gif" />
                &nbsp; &nbsp;
                <asp:Label ID="lblParametros" runat="server" Text="DPar�metros:" Font-Bold="True" ForeColor="White"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="pnlParametros" runat="server" CssClass="Rectangulo">
                <table id="tblRptParams" width="80%" border="0" runat="server">
		            <tr valign="top">
			            <td>
                            <asp:table id="tblParametros" runat="server" Font-Names="Verdana" Font-Size="8pt"></asp:table>
                        </td>
		            </tr>
                    <tr>
		                <td></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
		            <tr>
			            <td valign="bottom" height="20" style="text-align: right;">
                            <fsn:FSNButton ID="cmdObtener" runat="server" Text="DObtener"></fsn:FSNButton>
                        </td>
		            </tr>
	            </table>
            </asp:Panel>

            <asp:Panel ID="pnlTiemposMedios" runat="server">
                <table width="80%" border="0">
                    <tr ID="pnlClasificacion" runat="server" valign="top" class="Rectangulo" style="border:solid 0px #fafafa;" clientidmode="Static">
                        <td class="style1" style="vertical-align: middle;width:10%;">
                            <asp:Label ID="lblClasificacion" runat="server" Text="dClasificacion:" style="text-align: left;"></asp:Label>
                        </td>
                        <td colspan="4">
                            <div id="divClasificacion">
			                    <div id="divSelClasificacion" style="clear:both; width:18px; height:18px; text-align:center; cursor:pointer;border-bottom-width:0px;" class="Bordear">
				                    <img id="imgSelCategoria" class="Image16" src="../../../images/Categoria.png" style="padding:2px;" />
			                    </div>
			                    <div id="divCategoria" class="Bordear" style="width:75%; margin-bottom:-1px;">
				                    <asp:RequiredFieldValidator ID="valReportSelCategoria" ControlToValidate="txtReportSelCategoria" runat="server" EnableClientScript="false" Display="None" EnableViewState="false"></asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtReportSelCategoria" style="width:99%;padding-left:1px; border:none;" runat="server" onkeyup="EncontrarItemTreeViewReport('tvClasificaciones','txtReportSelCategoria')"></asp:TextBox>
                                    <asp:HiddenField id="idSelCategoria" runat="server" />
                                    <asp:HiddenField id="idSelCategoriaVisible" runat="server" />
    			                </div>
	    		                <div id="divClasificaciones" class="divContenedor" style="display:none; max-height:200px; overflow-y:auto;" onclick="SeleccionarClasificacion()">
				                    <div id="tvClasificaciones"></div>
			                    </div>
		                    </div>
                        </td>
                    </tr>

                    <tr ID="pnlSolicitud1" runat="server"  valign="top" class="Rectangulo" style="border:solid 0px #fafafa;">
			            <td colspan="5">
                            <asp:table id="tblSolicitud" runat="server" Font-Names="Verdana" Font-Size="8pt"></asp:table>
                        </td>
		            </tr>
                    <tr ID="pnlSolicitud2" runat="server"  valign="top" class="Rectangulo" style="border:solid 0px #fafafa;">
                        <td>
                            <asp:Label ID="LblSolicitud" runat="server" Text="dSolicitud:"></asp:Label>
                        </td>
                        <td>
							<ig:WebDropDown ID="wddSolicitud" runat="server" Width="240px" EnableClosingDropDownOnSelect="true" EnableClientRendering="true" 
                                CurrentValue="" ValueField = "Valor" TextField ="Texto" SelectionChanged="wddSolicitud_SelectionChanging()" 
                                ClientEvents-SelectionChanged="wddSolicitud_SelectionChanging">
                            </ig:WebDropDown>
                            <asp:HiddenField ID="idTipoSolicitud" runat="server"/>
                            <asp:RequiredFieldValidator ID="valTipoSolicitud" ControlToValidate="wddSolicitud" runat="server" EnableClientScript="false" Display="None" EnableViewState="false"></asp:RequiredFieldValidator> 
					    </td> 
                        <td colspan="3"></td>
                    </tr>

                    <tr ID="pnlProveedor1" runat="server" valign="top" class="Rectangulo" style="border:solid 0px #fafafa;">
                        <td colspan="5">
                            <asp:table id="tblProveedor" runat="server" Font-Names="Verdana" Font-Size="8pt"></asp:table>
                        </td>
                    </tr>
                    <tr ID="pnlProveedor2" runat="server" valign="top" class="Rectangulo" style="border:solid 0px #fafafa;">
                        <td>
                            <asp:Label ID="lblProveedor" runat="server" Text="dProveedor:"></asp:Label>
                        </td>
                        <td>
                            <table style="background-color: White; width: 150px; border: solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtProveedor" runat="server" Width="190px" BorderWidth="0px" BackColor="White" Height="16px" onchange="cambioProve()"></asp:TextBox>
                                        <ajx:AutoCompleteExtender ID="txtProveedor_AutoCompleteExtender" runat="server" CompletionSetCount="10"
                                            DelimiterCharacters="" Enabled="True" MinimumPrefixLength="1" ServiceMethod="GetProveedores"
                                            ServicePath="~/App_Pages/_Common/App_Services/AutoComplete.asmx" TargetControlID="txtProveedor" 
                                            EnableCaching="False" OnClientItemSelected="selected_Proveedor" >
                                        </ajx:AutoCompleteExtender>
                                    </td>
                                    <td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px">
                                        <asp:ImageButton ID="imgProveedorLupa" runat="server" SkinID="BuscadorLupa" />
                                        <asp:HiddenField ID="hidProveedor" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td colspan="3"></td>
                    </tr>

	    	        <tr ID="pnlTiemposMediosPorCarpeta1" runat="server" valign="top" class="Rectangulo" style="border:solid 0px #fafafa;">
		    	        <td colspan="5" width="60%">
                            <asp:table id="tblTiemposMediosPorCarpeta" runat="server" Font-Names="Verdana" Font-Size="8pt"></asp:table>
                        </td>
    		        </tr>
                    <tr ID="pnlTiemposMediosPorCarpeta2" runat="server" valign="top" class="Rectangulo" style="border:solid 0px #fafafa;">
						<td style="width:10%;">
                            <asp:Label ID="lblDesde" runat="server" Text="dDesde:"></asp:Label>
                        </td>
						<td style="width:15%;"> 
							<igpck:WebDatePicker ID="WDFecDesde" runat="server" SkinID="Calendario" Width="168px"></igpck:WebDatePicker>
						</td>                       
                        <td style="width:10%;">
                            <asp:Label ID="lblHasta" runat="server" Text="dHasta:"></asp:Label>
                        </td>
						<td style="width:15%;">
							<igpck:WebDatePicker ID="WDFecHasta" runat="server" SkinID="Calendario" Width="168px"></igpck:WebDatePicker>
						</td>
                        <td></td>
                    </tr>
                    <tr ID="pnlTiemposMediosPorCarpeta3" runat="server" valign="top" class="Rectangulo" style="border:solid 0px #fafafa;">
                        <td colspan="2">
                            <asp:CheckBox ID="chkMostrarDetalleSolicitudes" runat="server" />
                        </td>
                        <td colspan="3"></td>
                    </tr>
                    <tr ID="pnlTiemposMediosPorCarpeta4" runat="server" valign="top" class="Rectangulo" style="border:solid 0px #fafafa;">
    			        <td valign="bottom" height="20" style="text-align: left;" colspan="5">
                            <fsn:FSNButton ID="cmdObtenerTMCarpeta" runat="server" Text="DObtener" Alineacion="Left" ></fsn:FSNButton>
                        </td>
		            </tr>

		            <tr ID="pnlTiemposMediosPorIdentificador1" runat="server" valign="top" class="Rectangulo" style="border:solid 0px #fafafa;">
			            <td colspan="5">
                            <asp:table id="tblTiemposMediosPorIdentificador" runat="server" Font-Names="Verdana" Font-Size="8pt"></asp:table>
                        </td>
		            </tr>
                    <tr ID="pnlTiemposMediosPorIdentificador2" runat="server" valign="top" class="Rectangulo" style="border:solid 0px #fafafa;">
						<td>
                            <asp:Label ID="lblIdentificador" runat="server" Text="dIdentificador:"></asp:Label>
                        </td>
						<td>
							<input id="NumIdentificador" runat="server" type="text" class="CajaTexto" style="width: 250px"/>
						</td>
                        <td colspan="3"></td>
                    </tr>
                    <tr ID="pnlTiemposMediosPorIdentificador3" runat="server" valign="top" class="Rectangulo" style="border:solid 0px #fafafa;">          
			            <td valign="bottom" height="20" style="text-align: left;" colspan="5">
                            <fsn:FSNButton ID="cmdObtenerTMIdentificador" runat="server" Text="DObtener" Alineacion="Left" ></fsn:FSNButton>
                        </td>
		            </tr>
                </table>
            </asp:Panel>

            <asp:Label id="lblNoExiste" runat="server" Height="24px" Visible="False"></asp:Label>
            <ajx:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" 
                CollapseControlID="pnlCabeceraInformes" CollapsedImage="../../../images/expandir.gif" 
                ExpandControlID="pnlCabeceraInformes" ExpandedImage="../../../images/contraer.gif" 
                ImageControlID="imgExpandir" SuppressPostBack="True" 
                TargetControlID="pnlParametros">
            </ajx:CollapsiblePanelExtender>
            <ajx:CollapsiblePanelExtender ID="CollapsiblePanelExtenderTiempoMedio" runat="server" 
                CollapseControlID="pnlCabeceraInformes" CollapsedImage="../../../images/expandir.gif" 
                ExpandControlID="pnlCabeceraInformes" ExpandedImage="../../../images/contraer.gif" 
                ImageControlID="imgExpandir" SuppressPostBack="True" 
                TargetControlID="pnlTiemposMedios">
            </ajx:CollapsiblePanelExtender>
            <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="True" HasCrystalLogo="False" 
                ToolPanelView="None" HasToggleGroupTreeButton="False" />
        </td>
    </tr>
</table>
<script type="text/javascript">
    var arbol
    arbol=document.getElementById("pnlClasificacion");
    if (arbol)
    {
        $(document).ready(function () {
            $.ajax({
                type: "POST",
                url: rutaFS + '_Common/Informes/Report.aspx/Cargar_CarpetasSolicitudes',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false
            }).done(function (msg) {
                var data = msg.d;
                $(tvClasificaciones).tree({
                    data: data,
                    autoOpen: true,
                    selectable: true
                });
            });
        });
        //Funciones para controlar la visibilidad del panel de Categor�as. Cu�ndo se expande y se contrae.
        $('[id^=divSelClasificacion]').click(function (event) {
            if ($("id$=idSelCategoriaVisible").val() == 1) {
                $('#divClasificaciones').hide();
                $("id$=idSelCategoriaVisible").val(0);
            }
            else {
                $('#divClasificaciones').show();
                $("id$=idSelCategoriaVisible").val(1);
            }

        });
        $('[id$=txtReportSelCategoria]').focus(function (event) {
            $('#divClasificaciones').show();
            $('[id$=idSelCategoriaVisible]').val(1)
        });

    }
    function SeleccionarClasificacion()
    {
        var selectedCategoria = $('#tvClasificaciones .selected');
        var txtCategoria = $('[id$=txtReportSelCategoria]');
        var textotxtCategoria = txtCategoria.val();
        txtCategoria.attr("itemValue", selectedCategoria.attr("itemValue"));
        var texto = selectedCategoria.text();
        var idCategoria;
        idCategoria = 0;
        var idSelCategoria = $('[id$=idSelCategoria]');
        
        idSelCategoria.val(selectedCategoria.attr("itemValue"));

        while ($('#' + selectedCategoria.attr("parentId")).length > 0) {
            if ((idCategoria) == 0)
            {
            idSelCategoria.val(selectedCategoria.attr("itemValue"));
            idCategoria=1;
            }
            selectedCategoria = $('#' + selectedCategoria.attr("parentId"));
            texto = selectedCategoria.text() + ">" + texto;
        }
        txtCategoria.val(texto);
        if (texto != "") {
            //Ocultamos el mensaje de error
            var pnlError = $("#pnlError");
            if (pnlError) {
                pnlError.hide();
            }
        }
        if (texto != textotxtCategoria) {
            $('#divClasificaciones').hide();
            //variable que utilizo para cerrar arbol una vez seleccionada una carpeta
            $('[id$=idSelCategoriaVisible]').val(0);
            var wdd
            wdd = document.getElementById("<%=wddSolicitud.ClientID%>")
            if (wdd) {
                CargarWddSolicitud();
            }
        }

        //CargarWddSolicitud();
    }
    //cargar webdropdown
    function CargarWddSolicitud() {
        var wdd;
        wdd = document.getElementById("<%=wddSolicitud.ClientID%>");
        var txt;
        var id;
        txt = $('[id$=txtReportSelCategoria]').val();
        id = $("[id$=idSelCategoria]").val();
            
        if (wdd) {
                params = { IdClasificacion: id, txtClasificacion: txt };
                $(document).ready(function () {
                    $.when($.ajax({
                        type: "POST",
                        url: rutaFS + '_Common/Informes/Report.aspx/Cargar_TiposSolicitudes',
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(params),
                        dataType: "json",
                        async: false
                    })).done(function (data) {
                        var dataSource;
                        if (data.d != null) {
                            dataSource = jQuery.parseJSON('[' + data.d + ']')
                        }
                        else {//No hay datos
                            dataSource = jQuery.parseJSON('[{""Valor"":"""",""Texto"":""""}]')
                        }
                        var wddSolicitud = $find("<%=wddSolicitud.ClientID%>");

                        wddSolicitud.set_template('<li value=${Valor}>${Texto}</li>')
                        wddSolicitud.set_dataSource(dataSource);
                        wddSolicitud.dataBind();
                        
                    });
                });
        }
    }
    // Revisado por: mpf. Fecha: 07/05/2013
    //''' <summary>
    //Gets the new collection of selected drop-down items
    //Only one item will be in the collection when single selection is enabled      
    //''' </summary>
    //''' <param name="sender">Origen del evento.</param>
    //''' <param name="e">los datos del evento.</param>    
    //''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
    function wddSolicitud_SelectionChanging(sender, e) {
        var newSelection = e.getNewSelection();
        if (newSelection[0] != null) {
            var text = newSelection[0].get_text();
            var index = newSelection[0].get_value();
            var idTipoSolicitud = $('[id$=idTipoSolicitud]');
            idTipoSolicitud.val(index);
        }
    }

    //Recoge el ID del proveedor seleccionado con el autocompletar
    function selected_Proveedor(sender, e) {
        oIdProveedor = document.getElementById('<%= hidProveedor.clientID %>');
        if (oIdProveedor)
            oIdProveedor.value = e._value;
    }

    function cambioProve() {
        //si borramos el proveedor que hab�a...
//        if ($("[id$=txtProveedor]").val() == "") {
//            document.getElementById("<%=hidProveedor.ClientID%>").value = "";
//            document.getElementById("<%=txtProveedor.ClientID%>").value = "";
//        }
    }
    function prove_seleccionado2(sCIF, sProveCod, sProveDen) {
        document.getElementById("<%=hidProveedor.ClientID%>").value = sProveCod
        document.getElementById("<%=txtProveedor.ClientID%>").value = sProveDen
    }
    //Resalta el primer registro del treeview que coincide con el texto escrito en txtReportSelCategoria
    function EncontrarItemTreeViewReport(treeName, textBox) {
        var selectedItem;
        $('#' + treeName).scrollTop(0);
        selectedItem = $('#' + treeName + ' .selected');
        if (selectedItem.length != 0) {
            selectedItem.removeClass('selected');
        };
        $.each($('#' + treeName + ' span.treeSeleccionable'), function () {
            var text = $('[id$=' + textBox).val().toLowerCase();
            var itemText = $(this).text().toLowerCase()
            if (text != '' && itemText.indexOf(text) >= 0) {
                $(this).addClass('selected');
                selectedItem = $(this);
                $('#' + treeName).tree('openParents', selectedItem);
                $("#divClasificaciones").scrollTop($(this)[0].offsetTop-100);
                return false;
            }
        });
        $('#' + treeName).scrollTop(-10);
    }
</script>
</asp:Content>
