﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master" Inherits="Fullstep.FSNWeb.Informes" 
    Codebehind="Informes.aspx.vb" %>
<%@ MasterType VirtualPath="~/App_Master/Menu.Master" %>
<asp:Content ID="contInformes" ContentPlaceHolderID="CPH4" Runat="Server">
    <div style="padding-left:30px; padding-right:50px">
    <table cellpadding="10" cellspacing="0" border="0">
    <tr><td><img alt="" src="~/images/cabinformes.jpg" runat=server /></td>
        <td><label id="lblTitulo" runat="server" class="RotuloGrande" Height="40px"></label>
        </td></tr>
        </table>
    <div>
    <asp:Repeater ID="rptInformesPM" runat="server">
    <HeaderTemplate><div><asp:Label ID="lblPM" class="Rotulo" runat="server"></asp:Label></div><hr class="hr_informes" /></HeaderTemplate>
    <ItemTemplate><div style="padding-bottom: 10px; padding-top: 10px"><img src="~/images/informe.gif" hspace="5" runat=server  /> <asp:HyperLink runat="server" id="hlEnlace" CssClass="Etiqueta"></asp:HyperLink></div></ItemTemplate>
    <FooterTemplate><div style="padding-bottom: 10px; padding-top: 10px"></div></FooterTemplate>
    </asp:Repeater>
    <asp:Repeater ID="rptInformesQA" runat="server">
    <HeaderTemplate><div><asp:Label ID="lblQA" class="Rotulo" runat="server"></asp:Label></div><hr class="hr_informes"/></HeaderTemplate>
    <ItemTemplate><div style="padding-bottom: 10px; padding-top: 10px"><img src="~/images/informe.gif" runat=server hspace="5" /> <asp:HyperLink runat="server" id="hlEnlace" CssClass="Etiqueta"></asp:HyperLink></div></ItemTemplate>
    <FooterTemplate><div style="padding-bottom: 10px; padding-top: 10px"></div></FooterTemplate>
    </asp:Repeater>
    <asp:Repeater ID="rptInformesEP" runat="server">
    <HeaderTemplate><div><asp:Label ID="lblEP" class="Rotulo" runat="server"></asp:Label></div><hr class="hr_informes"/>
                    <div style="padding-bottom: 10px; padding-top: 10px"><img src="~/images/informe.gif" runat=server hspace="5" /> <asp:HyperLink runat="server" id="hlInfCat" CssClass="Etiqueta"></asp:HyperLink></div>
                    <div style="padding-bottom: 10px; padding-top: 10px"><img src="~/images/informe.gif" runat=server hspace="5" /> <asp:HyperLink runat="server" id="hlInfProve" CssClass="Etiqueta"></asp:HyperLink></div>
    </HeaderTemplate>
    <ItemTemplate><div style="padding-bottom: 10px; padding-top: 10px"><img src="~/images/informe.gif" runat=server hspace="5" /> <asp:HyperLink runat="server" id="hlEnlace" CssClass="Etiqueta"></asp:HyperLink></div></ItemTemplate>
    <FooterTemplate><div style="padding-bottom: 10px; padding-top: 10px"></div></FooterTemplate>
    </asp:Repeater>
    <asp:Repeater ID="rptInformesTiemposMedios" runat="server">
    <HeaderTemplate><div><asp:Label ID="lblTiemposMedios" class="Rotulo" runat="server"></asp:Label></div><hr class="hr_informes" /></HeaderTemplate>
    <ItemTemplate><div style="padding-bottom: 10px; padding-top: 10px"><img id="Img1" src="~/images/informe.gif" hspace="5" runat=server  /> <asp:HyperLink runat="server" id="hlEnlace" CssClass="Etiqueta"></asp:HyperLink></div></ItemTemplate>
    <FooterTemplate><div style="padding-bottom: 10px; padding-top: 10px"></div></FooterTemplate>
    </asp:Repeater>
    </div>
    </div>
</asp:Content>

