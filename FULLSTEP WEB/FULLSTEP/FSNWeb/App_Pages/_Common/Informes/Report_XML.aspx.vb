﻿Imports Fullstep.FSNLibrary.TiposDeDatos
Imports Infragistics.WebUI.WebSchedule

Partial Public Class Report_XML
    Inherits FSNPage
    Private PanelMaterialMostrado As Boolean = False
    Private cXMLS As Fullstep.FSNServer.cXMLs
    Private m_desdeGS As Boolean = False


    ''' <summary>
    ''' Evento preinit en el que configuramos la apariencia de algunos controles
    ''' </summary>
    ''' <param name="sender">La propia página, al cargarse</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>>Llamada desde: Automática; Tiempo máximo:0</remarks>
    Private Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Not String.IsNullOrEmpty(Request.QueryString("desdeGS")) Then
            m_desdeGS = True
        End If
        If m_desdeGS Then
            Me.MasterPageFile = "~/App_Master/EnBlanco.Master"
        Else
            Me.MasterPageFile = "~/App_Master/Menu.master"
        End If
    End Sub

    ''' <summary>
    ''' Evento de carga de la página, se carga el módulo de idioma, se registran los controles y se carga el archivo XML para dibujar los filtros.
    ''' </summary>
    ''' <param name="sender">La propia página, al cargarse</param>
    ''' <param name="e">Los àrgumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automática, cada vez que se recarga la página
    ''' Tiempo máximo: Depende de los parámetros y las consultas, pero actualmente menos de 1 segundo.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim MiArchivoReport As String = ""

        If m_desdeGS = False Then Master.Seleccionar("Informes", "Informes")

        ModuloIdioma = ModulosIdiomas.Listados
        CargarTextos()
        If Not ViewState("PanelMaterialMostrado") Is Nothing Then
            PanelMaterialMostrado = ViewState("PanelMaterialMostrado")
        End If
        If Not ViewState("cXMLS") Is Nothing Then
            cXMLS = ViewState("cXMLS")
        End If
        If ViewState("TextoCategoria") <> "" Then
            litCategoria.Text = ViewState("TextoCategoria")
            litCategoria.Visible = True
            LnkBtnBorrarTextoCategoria.Visible = True
        End If
        If ViewState("TextoMaterial") <> "" Then
            LblTextoMateriales.Text = ViewState("TextoMaterial")
            LblTextoMateriales.Visible = True
            LnkBtnBorrarTextoMateriales.Visible = True
        End If
        If Not String.IsNullOrEmpty(Request.QueryString("FileReport")) Then
            MiArchivoReport = Request("FileReport").Replace(System.IO.Path.GetExtension(Request("FileReport")), ".xml")
            If Not MiArchivoReport.Contains(ConfigurationManager.AppSettings("rptFilesPath")) Then
                Dim dirrpt As String = Trim(ConfigurationManager.AppSettings("rptFilesPath"))
                If Right(dirrpt, 1) = "/" Or Right(dirrpt, 1) = "\" Then dirrpt = Left(dirrpt, dirrpt.Length - 1)
                MiArchivoReport = dirrpt & "\" & MiArchivoReport
            End If
        End If
        If Not IO.File.Exists(MiArchivoReport) Then
            'Si no existe el listado en esa ubicación sacamos el mensaje
            PanelParametros.Visible = False
            BtnCrearInforme.Visible = False
            pnlCabeceraInformes.Visible = False
            lblNoExiste.Visible = True
            Exit Sub
        Else
            lblNoExiste.Visible = False
            Call CargarParametrosEnPagina(MiArchivoReport, Request.QueryString("FileReport"))
            RegistrarControles()
        End If
        tvwCategorias.ExpandImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "mas.gif")
        tvwCategorias.CollapseImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "menos.gif")
        tvwMateriales.ExpandImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "mas.gif")
        tvwMateriales.CollapseImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "menos.gif")

        If Not Me.IsPostBack Then
            If m_desdeGS = False Then
                Me.BtnAceptarGenerarInforme.OnClientClick = "var newWindow = window.open('EjecutarSSIS.aspx' ,'_blank','height=300,width=300,menubar=no,scrollbars=no,resizable=no,toolbar=no,addressbar=no');return false;"
            Else
                Me.BtnAceptarGenerarInforme.OnClientClick = "var newWindow = window.open('EjecutarSSIS.aspx?desdeGS=1" & "' ,'_blank','height=300,width=300,menubar=no,scrollbars=no,resizable=no,toolbar=no,addressbar=no');return false;"
            End If
        End If
    End Sub

    ''' <summary>
    ''' Procedimiento que carga los textos de los controles de la página
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: El page_load
    ''' Tiempo máximo: 0 seg</remarks>
    Public Sub CargarTextos()
        BtnAceptarGenerarInforme.Text = Textos(20)
        BtnCancelarGenerarInforme.Text = Textos(21)
        LblConfirmGenerarListado.Text = Textos(22)
        LblTitMateriales.Text = Textos(12)
        ImgBtnCerrarPanelMateriales.ToolTip = Textos(15)
        lblTitulo.Text = Textos(0) & " " & Request("ReportName")
        tvwMateriales.ExpandImageToolTip = Textos(13) & "{0}"
        tvwMateriales.CollapseImageToolTip = Textos(14) & "{0}"
        lblParametros.Text = Textos(1) & ":"
        TextoCategoria.Text = Textos(16)
        LnkBtnBorrarTextoCategoria.ToolTip = Textos(19)
        LblCatalogoArbol.Text = Textos(17)
        ImgBtnCerrarPanelArbol.ToolTip = Textos(15)
        tvwCategorias.ExpandImageToolTip = Textos(13) & "{0}"
        tvwCategorias.CollapseImageToolTip = Textos(14) & "{0}"
        LitMateriales.Text = Textos(18)
        LnkBtnBorrarTextoMateriales.ToolTip = Textos(19)
        BtnCrearInforme.Text = Textos(4)
        lblNoExiste.Text = Textos(5)
    End Sub

    ''' <summary>
    ''' Procedimiento que registra los controles de la página para que sus eventos se ejecuten como asíncronos
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: El page_load
    ''' Tiempo máximo: 0 seg</remarks>
    Public Sub RegistrarControles()
        ScriptMgr.RegisterAsyncPostBackControl(tvwMateriales)
        ScriptMgr.RegisterAsyncPostBackControl(ImgBtnCerrarPanelMateriales)
        If Not TablaParametros.FindControl("ImgBtnBorrarTextoCategorias") Is Nothing Then
            ScriptMgr.RegisterAsyncPostBackControl(TablaParametros.FindControl("ImgBtnBorrarTextoCategorias"))
        End If
        If Not TablaParametros.FindControl("ImgBtnAbrirArbolCategorias") Is Nothing Then
            ScriptMgr.RegisterAsyncPostBackControl(TablaParametros.FindControl("ImgBtnAbrirArbolCategorias"))
        End If
        If Not TablaParametros.FindControl("ImgBtnBorrarTextoMateriales") Is Nothing Then
            ScriptMgr.RegisterAsyncPostBackControl(TablaParametros.FindControl("ImgBtnBorrarTextoMateriales"))
        End If
        If Not TablaParametros.FindControl("ImgBtnAbrirArbolMateriales") Is Nothing Then
            ScriptMgr.RegisterAsyncPostBackControl(TablaParametros.FindControl("ImgBtnAbrirArbolMateriales"))
        End If
        ScriptMgr.RegisterAsyncPostBackControl(BtnMateriales)
        ScriptMgr.RegisterAsyncPostBackControl(BtnCategoria)
        ScriptMgr.RegisterAsyncPostBackControl(tvwCategorias)
        ScriptMgr.RegisterAsyncPostBackControl(tvwMateriales)
        ScriptMgr.RegisterAsyncPostBackControl(BtnCrearInforme)
        ScriptMgr.RegisterAsyncPostBackControl(BtnAceptarGenerarInforme)
        ScriptMgr.RegisterAsyncPostBackControl(BtnCancelarGenerarInforme)
    End Sub

    ''' <summary>
    ''' Procedimiento que lee el archivo XML y carga los parámetros necesarios en la página para ponerlos como filtros
    ''' </summary>
    ''' <param name="RutaArchivoReport">Ruta fisica donde se encuentra el archivo a cargar</param>
    ''' <remarks>
    ''' Llamada desde: El page_load
    ''' Tiempo máximo: Depende de los procedimientos a ejecutar pero alrededor de 0,5 seg</remarks>
    Public Sub CargarParametrosEnPagina(ByVal RutaArchivoReport As String, ByVal sRPTName As String)

        Dim ContarComoCelda As Boolean = True
        Dim ContadorFilas As Integer = 0
        If cXMLS Is Nothing Then
            cXMLS = FSNServer.Get_Object(GetType(Fullstep.FSNServer.cXMLs))
            cXMLS.LeerXML(RutaArchivoReport)
            cXMLS.RPTName = sRPTName
        End If
        Dim Fila As New TableRow
        Dim Celda As New TableCell
        Dim LabelNombreParametro As New Label
        Dim ContadorCelda As Integer = 0
        Dim tableHtml As New Table
        Dim FilaTablaHtml As New TableRow
        Dim CeldaTablaHtml As New TableCell
volver:
        Dim ContFilasTablaParametros As Integer = 0
        For Each FilaTablaPArametros As TableRow In TablaParametros.Rows
            If FilaTablaPArametros.ID <> "FilaCatyMat" Then
                TablaParametros.Rows.RemoveAt(ContFilasTablaParametros)
                GoTo volver
            End If
        Next
        For Each Parametro As Fullstep.FSNServer.cXML In cXMLS
            If Parametro.TipoParametro = Fullstep.FSNServer.cXML.TipoDeParametro.ParametroForm Then
                tableHtml = New Table
                FilaTablaHtml = New TableRow
                CeldaTablaHtml = New TableCell
                LabelNombreParametro = New Label
                Celda = New TableCell
                If Not Parametro.TipoDato = Fullstep.FSNServer.cXML.TipoDeDato.ArbolCategorias AndAlso Not Parametro.TipoDato = Fullstep.FSNServer.cXML.TipoDeDato.EstructuraMateriales Then
                    LabelNombreParametro.Text = Parametro.TextoAMostrar & " "
                    LabelNombreParametro.CssClass = "Normal"
                    CeldaTablaHtml.Controls.Add(LabelNombreParametro)
                    CeldaTablaHtml.Width = Unit.Pixel(160)
                    FilaTablaHtml.Cells.Add(CeldaTablaHtml)
                    tableHtml.Rows.Add(FilaTablaHtml)
                End If
                CeldaTablaHtml = New TableCell
                If Parametro.TipoDato = Fullstep.FSNServer.cXML.TipoDeDato.ArbolCategorias Then
                    CeldaCategorias.Visible = True
                    FilaCatyMat.Visible = True
                    ContarComoCelda = False
                    PanelArbol.Visible = True
                    mpePanelArbol.Enabled = True
                    CargarArbolCategorias()
                ElseIf Parametro.TipoDato = Fullstep.FSNServer.cXML.TipoDeDato.ArticulosCatalogo Then
                    Dim TbArticulos As New TextBox
                    TbArticulos.ID = Parametro.NombreParametro
                    TbArticulos.Width = Unit.Pixel(200)
                    Dim tbWatermark As New AjaxControlToolkit.TextBoxWatermarkExtender
                    tbWatermark.TargetControlID = TbArticulos.ID
                    tbWatermark.WatermarkText = "Introduzca codigo de artículo"
                    tbWatermark.ID = "tbwe" & TbArticulos.ID
                    CeldaTablaHtml.Controls.Add(TbArticulos)
                    CeldaTablaHtml.Controls.Add(tbWatermark)
                    FilaTablaHtml.Cells.Add(CeldaTablaHtml)
                    Celda.Controls.Add(tableHtml)
                ElseIf Parametro.TipoDato = Fullstep.FSNServer.cXML.TipoDeDato.EstructuraMateriales Then
                    CeldaMateriales.Visible = True
                    FilaCatyMat.Visible = True
                    ContarComoCelda = False
                    PanelMateriales.Visible = True
                    mpePanelMateriales.Enabled = True
                    CargarArbolMateriales()
                    PanelMaterialMostrado = True
                    UpArbolMateriales.Update()
                    ViewState("PanelMaterialMostrado") = PanelMaterialMostrado
                Else
                    If Parametro.TieneRangoValores AndAlso Not Parametro.ValoresPorDefecto Is Nothing AndAlso Parametro.ValoresPorDefecto.Count > 0 Then
                        Dim CmbBox As New DropDownList
                        CmbBox.ID = Parametro.NombreParametro
                        Dim ContadorItems As Integer = 0
                        If Parametro.Requerido = False Then
                            CmbBox.Items.Add("")
                            CmbBox.Items(0).Value = Nothing
                            ContadorItems = 1
                        End If
                        For Each ValorDefecto As Fullstep.FSNServer.cXML.ValoresDefecto In Parametro.ValoresPorDefecto
                            CmbBox.Items.Add(ValorDefecto.Texto)
                            CmbBox.Items(ContadorItems).Value = ValorDefecto.Valor
                            ContadorItems = ContadorItems + 1
                        Next
                        Dim contadorDatos As Integer = 0
                        If Not Parametro.ValorPorDefecto Is Nothing AndAlso Not Parametro.ValorPorDefecto = "Null" Then
                            For Each Dato In CmbBox.Items
                                If Dato.ToString.ToLower = Parametro.ValorPorDefecto.ToString.ToLower Then
                                    CmbBox.Items(contadorDatos).Selected = True
                                    Exit For
                                Else
                                    contadorDatos = contadorDatos + 1
                                End If
                            Next
                        End If
                        If Parametro.Requerido = False AndAlso (Not Parametro.ValorPorDefecto = "" AndAlso Not Parametro.ValorPorDefecto Is Nothing) Then
                            For i As Integer = 0 To CmbBox.Items.Count - 1
                                CmbBox.Items(i).Selected = False
                            Next
                            CmbBox.Items(0).Selected = True
                        End If
                        CeldaTablaHtml.Controls.Add(CmbBox)
                        FilaTablaHtml.Cells.Add(CeldaTablaHtml)
                        Celda.Controls.Add(tableHtml)
                    ElseIf Parametro.TieneRangoValores AndAlso Parametro.OrigenDatos.Tipo = Fullstep.FSNServer.cXML.TipoOrigenDatos.Stored Then
                        Dim NombreStored As String
                        Dim DsetResul As New DataSet
                        NombreStored = Parametro.OrigenDatos.Nombre

                        Dim oXMLStored As Fullstep.FSNServer.cXML = FSNServer.Get_Object(GetType(Fullstep.FSNServer.cXML))
                        Dim ParametrosStored() As ParametrosOrigenDeDatos = Nothing
                        Dim ContadorParametrosStored As Integer = 0
                        If Not Parametro.OrigenDatos.Parametros Is Nothing AndAlso Not Parametro.OrigenDatos.Parametros.Count = 0 Then
                            Dim ValorDato As String
                            For Each ParamStored As FSNLibrary.TiposDeDatos.ParametrosOrigenDeDatos In Parametro.OrigenDatos.Parametros
                                ReDim Preserve ParametrosStored(ContadorParametrosStored)
                                Select Case ParamStored.ValorDato.ToLower
                                    Case "fsnuser.codpersona"
                                        ValorDato = FSNUser.CodPersona
                                    Case "fsnuser.idioma"
                                        ValorDato = FSNUser.Idioma
                                    Case "fsnuser.cod"
                                        ValorDato = FSNUser.Cod
                                    Case "year(now)"
                                        ValorDato = Year(Now)
                                    Case "date(now)"
                                        ValorDato = Date.Now
                                    Case Else
                                        ValorDato = ParamStored.ValorDato
                                End Select
                                ParametrosStored(ContadorParametrosStored) = ParamStored
                                ParametrosStored(ContadorParametrosStored).ValorDato = ValorDato
                                ContadorParametrosStored = ContadorParametrosStored + 1
                            Next
                        End If
                        DsetResul = oXMLStored.EjecutaStoredProcedure(NombreStored, ParametrosStored)
                        Dim CmbBox As New DropDownList
                        CmbBox.ID = Parametro.NombreParametro
                        Dim ContadorItems As Integer = 0
                        If Parametro.Requerido = False Then
                            CmbBox.Items.Add("")
                            CmbBox.Items(0).Value = Nothing
                            ContadorItems = 1
                        End If
                        If Not DsetResul.Tables(0).Rows.Count = 0 Then
                            For Each FilaRow As DataRow In DsetResul.Tables(0).Rows
                                If Not IsDBNull(FilaRow.Item(Parametro.OrigenDatos.CampoAMostrar)) Then
                                    CmbBox.Items.Add(FilaRow.Item(Parametro.OrigenDatos.CampoAMostrar))
                                    CmbBox.Items(ContadorItems).Value = FilaRow.Item(Parametro.OrigenDatos.CampoValor)
                                    ContadorItems = ContadorItems + 1
                                End If
                            Next
                            If Parametro.Requerido = False AndAlso (Not Parametro.ValorPorDefecto = "" AndAlso Not Parametro.ValorPorDefecto Is Nothing) Then
                                For i As Integer = 0 To CmbBox.Items.Count - 1
                                    CmbBox.Items(i).Selected = False
                                Next
                                CmbBox.Items(0).Selected = True
                            End If
                        Else
                            If CmbBox.Items.Count > 0 Then
                                CmbBox.Items(0).Selected = True
                            End If
                        End If

                        CmbBox.Width = New Unit(200)
                        CeldaTablaHtml.Controls.Add(CmbBox)
                        BindTooltip(CmbBox)

                        FilaTablaHtml.Cells.Add(CeldaTablaHtml)
                        Celda.Controls.Add(tableHtml)
                    Else
                        Select Case Parametro.TipoDato
                            Case Fullstep.FSNServer.cXML.TipoDeDato.Booleano
                                Dim ChkBx As New CheckBox
                                ChkBx.ID = Parametro.NombreParametro
                                If Not Parametro.ValorPorDefecto Is Nothing AndAlso Not Parametro.ValorPorDefecto = "Null" Then
                                    If Parametro.ValorPorDefecto = True Then
                                        ChkBx.Checked = True
                                    Else
                                        ChkBx.Checked = False
                                    End If
                                End If
                                CeldaTablaHtml.Controls.Add(ChkBx)
                                FilaTablaHtml.Cells.Add(CeldaTablaHtml)
                                Celda.Controls.Add(tableHtml)
                            Case Fullstep.FSNServer.cXML.TipoDeDato.Entero
                                Dim WNumEdit As New TextBox
                                WNumEdit.EnableViewState = False
                                WNumEdit.CssClass = "Normal"
                                WNumEdit.Width = Unit.Pixel(60)
                                WNumEdit.ID = Parametro.NombreParametro
                                Dim Filter As New AjaxControlToolkit.FilteredTextBoxExtender
                                Filter.FilterType = AjaxControlToolkit.FilterTypes.Numbers
                                Filter.FilterMode = AjaxControlToolkit.FilterModes.ValidChars
                                Filter.FilterInterval = 1
                                Filter.ID = "Filter_" & Parametro.NombreParametro
                                Filter.TargetControlID = Parametro.NombreParametro
                                If Parametro.Requerido Then
                                    WNumEdit.Text = 0
                                Else
                                    WNumEdit.Text = ""
                                End If
                                WNumEdit.Style.Add("align", "right")
                                CeldaTablaHtml.Controls.Add(WNumEdit)
                                CeldaTablaHtml.Controls.Add(Filter)
                                FilaTablaHtml.Cells.Add(CeldaTablaHtml)
                                Celda.Controls.Add(tableHtml)
                            Case Fullstep.FSNServer.cXML.TipoDeDato.Fecha
                                Dim WDatChoos As New Infragistics.Web.UI.EditorControls.WebDatePicker
                                WDatChoos.NullText = ""
                                WDatChoos.SkinID = "Calendario"
                                WDatChoos.ID = Parametro.NombreParametro
                                WDatChoos.DisplayModeFormat = FSNUser.DateFormat.ShortDatePattern  'DateFormat.Short
                                WDatChoos.HorizontalAlign = HorizontalAlign.Right
                                WDatChoos.CssClass = "Etiqueta"
                                WDatChoos.EnableViewState = False
                                If Not Parametro.Requerido Then
                                    WDatChoos.Nullable = True
                                Else
                                    WDatChoos.Nullable = False
                                End If
                                WDatChoos.BorderColor = Drawing.Color.SteelBlue
                                WDatChoos.BorderStyle = BorderStyle.Solid
                                WDatChoos.BorderWidth = "1"
                                CeldaTablaHtml.Controls.Add(WDatChoos)
                                FilaTablaHtml.Cells.Add(CeldaTablaHtml)
                                Celda.Controls.Add(tableHtml)
                            Case Fullstep.FSNServer.cXML.TipoDeDato.Real
                                Dim WNumEdit As New Infragistics.Web.UI.EditorControls.WebNumericEditor
                                WNumEdit.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Float
                                WNumEdit.EnableViewState = False
                                WNumEdit.CssClass = "Normal"
                                WNumEdit.Width = Unit.Pixel(100)
                                WNumEdit.NullText = ""
                                WNumEdit.ID = Parametro.NombreParametro
                                If Parametro.Requerido Then
                                    WNumEdit.Text = 0
                                Else
                                    WNumEdit.Text = ""
                                End If
                                If Not Parametro.ValorMinimo Is Nothing AndAlso Not Parametro.ValorMinimo = "Null" Then
                                    WNumEdit.MinValue = CDbl(Parametro.ValorMinimo)
                                    If Parametro.Requerido Then
                                        WNumEdit.Text = Parametro.ValorMinimo
                                    End If
                                End If
                                If Not Parametro.ValorMaximo Is Nothing AndAlso Not Parametro.ValorMaximo = "Null" Then
                                    WNumEdit.MaxValue = CDbl(Parametro.ValorMaximo)
                                End If
                                CeldaTablaHtml.Controls.Add(WNumEdit)
                                FilaTablaHtml.Cells.Add(CeldaTablaHtml)
                                Celda.Controls.Add(tableHtml)
                            Case Fullstep.FSNServer.cXML.TipoDeDato.Texto
                                Dim TxBx As New TextBox
                                If Parametro.ValorPorDefecto <> "" AndAlso Parametro.ValorPorDefecto <> "Null" Then
                                    TxBx.Text = Parametro.ValorPorDefecto
                                End If
                                TxBx.ID = Parametro.NombreParametro
                                TxBx.CssClass = "Etiqueta"
                                TxBx.EnableViewState = False
                                CeldaTablaHtml.Controls.Add(TxBx)
                                FilaTablaHtml.Cells.Add(CeldaTablaHtml)
                                Celda.Controls.Add(tableHtml)
                        End Select
                    End If
                End If
                If ContarComoCelda Then
                    Fila.Cells.Add(Celda)
                    ContadorCelda = ContadorCelda + 1
                    If ContadorCelda = 2 Then
                        TablaParametros.Rows.AddAt(ContadorFilas, Fila)
                        ContadorFilas = ContadorFilas + 1
                        ContadorCelda = 0
                        Fila = New TableRow
                    End If
                End If
                ContarComoCelda = True
            End If
        Next
        If Not ContadorCelda = 0 Then
            TablaParametros.Rows.AddAt(ContadorFilas, Fila)
        End If
        ViewState("cXMLS") = cXMLS
    End Sub

    Private Sub BindTooltip(ByVal ddl As DropDownList)
        For Each item As ListItem In ddl.Items
            item.Attributes.Add("title", item.Text)
        Next
    End Sub

    Private Sub ComboSelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        If Not CType(sender, DropDownList).SelectedItem Is Nothing Then
            CType(sender, DropDownList).ToolTip = CType(sender, DropDownList).SelectedItem.Text
        Else
            CType(sender, DropDownList).ToolTip = String.Empty
        End If
    End Sub

    ''' <summary>
    ''' Evento que se genera cuando se pincha el botón etiquetado con "Generar Informe", abriendo una nueva página que ejecutara el paquete SSIS
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento, en este caso el botón</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: La propia página, cada vez que se clica el botón de "Generar informe"
    ''' Tiempo máximo: Es una llamada asíncrona, pero el paquete puede tardar varios segundos en ejecutarse y dejar descargar el informe</remarks>
    Protected Sub BtnCrearInforme_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnCrearInforme.Click
        Dim CadenaPasar As String() = {}
        Dim ContadorParametros As Integer = 0
        For Each parametro As Fullstep.FSNServer.cXML In cXMLS
            If parametro.TipoParametro = Fullstep.FSNServer.cXML.TipoDeParametro.ParametroForm Then
                ReDim Preserve CadenaPasar(ContadorParametros)
                If Not parametro.TieneRangoValores Then
                    Select Case parametro.TipoDato
                        Case Fullstep.FSNServer.cXML.TipoDeDato.Booleano
                            If Not TablaParametros.FindControl(parametro.NombreParametro) Is Nothing Then
                                CadenaPasar(ContadorParametros) = IIf(CType(TablaParametros.FindControl(parametro.NombreParametro), CheckBox).Checked, 1, 0)
                            End If
                        Case Fullstep.FSNServer.cXML.TipoDeDato.Entero
                            If Not TablaParametros.FindControl(parametro.NombreParametro) Is Nothing Then
                                CadenaPasar(ContadorParametros) = IIf(CType(TablaParametros.FindControl(parametro.NombreParametro), TextBox).Text = "", "NULL", CType(TablaParametros.FindControl(parametro.NombreParametro), TextBox).Text)
                            End If
                        Case Fullstep.FSNServer.cXML.TipoDeDato.Fecha
                            If Not TablaParametros.FindControl(parametro.NombreParametro) Is Nothing Then
                                CadenaPasar(ContadorParametros) = IIf(CType(TablaParametros.FindControl(parametro.NombreParametro), Infragistics.Web.UI.EditorControls.WebDatePicker).Value Is DBNull.Value, "NULL", String.Format("{0:dd/MM/yyyy}", CType(TablaParametros.FindControl(parametro.NombreParametro), Infragistics.Web.UI.EditorControls.WebDatePicker).Value))
                            End If
                        Case Fullstep.FSNServer.cXML.TipoDeDato.Real
                            If Not TablaParametros.FindControl(parametro.NombreParametro) Is Nothing Then
                                CadenaPasar(ContadorParametros) = IIf(CType(TablaParametros.FindControl(parametro.NombreParametro), Infragistics.Web.UI.EditorControls.WebNumericEditor).Text = "", "NULL", CType(TablaParametros.FindControl(parametro.NombreParametro), Infragistics.Web.UI.EditorControls.WebNumericEditor).Text)
                            End If
                        Case Fullstep.FSNServer.cXML.TipoDeDato.Texto
                            'Se comprueba que no se meta SQLINJECTION
                            Dim param As String = modUtilidades.strToSQLLIKE(CType(TablaParametros.FindControl(parametro.NombreParametro), TextBox).Text)
                            If Not TablaParametros.FindControl(parametro.NombreParametro) Is Nothing Then
                                CadenaPasar(ContadorParametros) = IIf(param = "", "NULL", param)
                            End If
                    End Select
                Else
                    If parametro.TipoDato = Fullstep.FSNServer.cXML.TipoDeDato.ArbolCategorias OrElse parametro.TipoDato = Fullstep.FSNServer.cXML.TipoDeDato.ArticulosCatalogo OrElse parametro.TipoDato = Fullstep.FSNServer.cXML.TipoDeDato.EstructuraMateriales Then
                        If parametro.TipoDato = Fullstep.FSNServer.cXML.TipoDeDato.ArbolCategorias Then
                            If litCategoria.Visible = False Then
                                CadenaPasar(ContadorParametros) = "NULL"
                                ContadorParametros = ContadorParametros + 1
                                ReDim Preserve CadenaPasar(ContadorParametros)
                                CadenaPasar(ContadorParametros) = "NULL"
                                ContadorParametros = ContadorParametros + 1
                                ReDim Preserve CadenaPasar(ContadorParametros)
                                CadenaPasar(ContadorParametros) = "NULL"
                                ContadorParametros = ContadorParametros + 1
                                ReDim Preserve CadenaPasar(ContadorParametros)
                                CadenaPasar(ContadorParametros) = "NULL"
                                ContadorParametros = ContadorParametros + 1
                                ReDim Preserve CadenaPasar(ContadorParametros)
                                CadenaPasar(ContadorParametros) = "NULL"
                            Else
                                Dim Categorias As String() = {}
                                For Each fila As DataRow In TodasCategorias.Tables(0).Rows
                                    If fila.Item("Den") = litCategoria.Text Then
                                        Categorias = Split(DBNullToStr(fila.Item("Id")), "_", 5, CompareMethod.Text)
                                        Exit For
                                    End If
                                Next
                                If Categorias Is Nothing OrElse Categorias.Count = 0 Then
                                    CadenaPasar(ContadorParametros) = "NULL"
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = "NULL"
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = "NULL"
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = "NULL"
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = "NULL"
                                ElseIf Categorias.Count = 1 Then
                                    CadenaPasar(ContadorParametros) = Categorias(0)
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = "NULL"
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = "NULL"
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = "NULL"
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = "NULL"
                                ElseIf Categorias.Count = 2 Then
                                    CadenaPasar(ContadorParametros) = Categorias(0)
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = Categorias(1)
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = "NULL"
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = "NULL"
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = "NULL"
                                ElseIf Categorias.Count = 3 Then
                                    CadenaPasar(ContadorParametros) = Categorias(0)
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = Categorias(1)
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = Categorias(2)
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = "NULL"
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = "NULL"
                                ElseIf Categorias.Count = 4 Then
                                    CadenaPasar(ContadorParametros) = Categorias(0)
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = Categorias(1)
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = Categorias(2)
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = Categorias(3)
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = "NULL"
                                Else
                                    CadenaPasar(ContadorParametros) = Categorias(0)
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = Categorias(1)
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = Categorias(2)
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = Categorias(3)
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = Categorias(4)
                                End If
                            End If
                        ElseIf parametro.TipoDato = Fullstep.FSNServer.cXML.TipoDeDato.ArticulosCatalogo Then
                            If Not TablaParametros.FindControl(parametro.NombreParametro) Is Nothing Then
                                'Se comprueba que no se meta SQLINJECTION
                                Dim param As String = modUtilidades.strToSQLLIKE(CType(TablaParametros.FindControl(parametro.NombreParametro), TextBox).Text)
                                If param = "" Then
                                    CadenaPasar(ContadorParametros) = "NULL"
                                Else
                                    CadenaPasar(ContadorParametros) = param
                                End If
                            End If
                        Else
                            If LblTextoMateriales.Visible = False Then
                                CadenaPasar(ContadorParametros) = "NULL"
                                ContadorParametros = ContadorParametros + 1
                                ReDim Preserve CadenaPasar(ContadorParametros)
                                CadenaPasar(ContadorParametros) = "NULL"
                                ContadorParametros = ContadorParametros + 1
                                ReDim Preserve CadenaPasar(ContadorParametros)
                                CadenaPasar(ContadorParametros) = "NULL"
                                ContadorParametros = ContadorParametros + 1
                                ReDim Preserve CadenaPasar(ContadorParametros)
                                CadenaPasar(ContadorParametros) = "NULL"
                            Else
                                Dim GMNs As String() = {}
                                For Each fila As DataRow In TodosMateriales.Tables(0).Rows
                                    If fila.Item("Den_SPa") = LblTextoMateriales.Text Then
                                        GMNs = Split(fila.Item("Cod"), "_", 4, CompareMethod.Text)
                                        Exit For
                                    End If
                                Next
                                If GMNs Is Nothing OrElse GMNs.Count = 0 Then
                                    CadenaPasar(ContadorParametros) = "NULL"
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = "NULL"
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = "NULL"
                                    ContadorParametros = ContadorParametros + 1
                                    ReDim Preserve CadenaPasar(ContadorParametros)
                                    CadenaPasar(ContadorParametros) = "NULL"
                                Else
                                    Select Case GMNs.Count
                                        Case 1
                                            CadenaPasar(ContadorParametros) = GMNs(0)
                                            ContadorParametros = ContadorParametros + 1
                                            ReDim Preserve CadenaPasar(ContadorParametros)
                                            CadenaPasar(ContadorParametros) = "NULL"
                                            ContadorParametros = ContadorParametros + 1
                                            ReDim Preserve CadenaPasar(ContadorParametros)
                                            CadenaPasar(ContadorParametros) = "NULL"
                                            ContadorParametros = ContadorParametros + 1
                                            ReDim Preserve CadenaPasar(ContadorParametros)
                                            CadenaPasar(ContadorParametros) = "NULL"
                                        Case 2
                                            CadenaPasar(ContadorParametros) = GMNs(0)
                                            ContadorParametros = ContadorParametros + 1
                                            ReDim Preserve CadenaPasar(ContadorParametros)
                                            CadenaPasar(ContadorParametros) = GMNs(1)
                                            ContadorParametros = ContadorParametros + 1
                                            ReDim Preserve CadenaPasar(ContadorParametros)
                                            CadenaPasar(ContadorParametros) = "NULL"
                                            ContadorParametros = ContadorParametros + 1
                                            ReDim Preserve CadenaPasar(ContadorParametros)
                                            CadenaPasar(ContadorParametros) = "NULL"
                                        Case 3
                                            CadenaPasar(ContadorParametros) = GMNs(0)
                                            ContadorParametros = ContadorParametros + 1
                                            ReDim Preserve CadenaPasar(ContadorParametros)
                                            CadenaPasar(ContadorParametros) = GMNs(1)
                                            ContadorParametros = ContadorParametros + 1
                                            ReDim Preserve CadenaPasar(ContadorParametros)
                                            CadenaPasar(ContadorParametros) = GMNs(2)
                                            ContadorParametros = ContadorParametros + 1
                                            ReDim Preserve CadenaPasar(ContadorParametros)
                                            CadenaPasar(ContadorParametros) = "NULL"
                                        Case 4
                                            CadenaPasar(ContadorParametros) = GMNs(0)
                                            ContadorParametros = ContadorParametros + 1
                                            ReDim Preserve CadenaPasar(ContadorParametros)
                                            CadenaPasar(ContadorParametros) = GMNs(1)
                                            ContadorParametros = ContadorParametros + 1
                                            ReDim Preserve CadenaPasar(ContadorParametros)
                                            CadenaPasar(ContadorParametros) = GMNs(2)
                                            ContadorParametros = ContadorParametros + 1
                                            ReDim Preserve CadenaPasar(ContadorParametros)
                                            CadenaPasar(ContadorParametros) = GMNs(3)
                                    End Select
                                End If
                            End If
                        End If
                    Else
                        If Not TablaParametros.FindControl(parametro.NombreParametro) Is Nothing Then
                            CadenaPasar(ContadorParametros) = IIf(CType(TablaParametros.FindControl(parametro.NombreParametro), DropDownList).Text = "", "NULL", CType(TablaParametros.FindControl(parametro.NombreParametro), DropDownList).Text)
                        End If
                    End If
                End If
                ContadorParametros = ContadorParametros + 1
            End If
        Next
        Me.InsertarEnCache("CadenaParametros" & FSNUser.CodPersona, CadenaPasar)
        Me.InsertarEnCache("ObjetoXML" & FSNUser.CodPersona, cXMLS)
        mpeConfGenerarInforme.Show()
    End Sub

    ''' <summary>
    ''' Procedimiento en el que cargaremos los valores para el arbol de categorías
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: El evento Load de la página
    ''' Tiempo máximo: 0 sec.</remarks>
    Private Sub CargarArbolCategorias()
        Dim DsetCategoriasPedidos As New DataSet
        Dim oCArticulos As FSNServer.cArticulos = Me.FSNServer.Get_Object(GetType(FSNServer.cArticulos))
        DsetCategoriasPedidos = oCArticulos.DevolverCategoriasPedidos
        Dim query = From Datos In TodasCategorias.Tables(0) _
            Let w = IIf(Datos.Item("ID").ToString().IndexOf("_") < 0, Datos.Item("ID").ToString().Length, Datos.Item("ID").ToString().IndexOf("_")) _
            Join Articulos In DsetCategoriasPedidos.Tables(0) On _
                Articulos.Item("CAT1").ToString() Equals Datos.Item("ID").ToString().Substring(0, w) _
            Where Datos.Item("ID") = Articulos.Item("CAT1").ToString() _
                Or Datos.Item("ID") = Articulos.Item("CAT1") & "_" & Articulos.Item("CAT2") _
                Or Datos.Item("ID") = Articulos.Item("CAT1") & "_" & Articulos.Item("CAT2") & "_" & Articulos.Item("CAT3") _
                Or Datos.Item("ID") = Articulos.Item("CAT1") & "_" & Articulos.Item("CAT2") & "_" & Articulos.Item("CAT3") & "_" & Articulos.Item("CAT4") _
                Or Datos.Item("ID") = Articulos.Item("CAT1") & "_" & Articulos.Item("CAT2") & "_" & Articulos.Item("CAT3") & "_" & Articulos.Item("CAT4") & "_" & Articulos.Item("CAT5") _
            Order By Datos.Item("DEN") _
            Select Datos Distinct
        '  Dim query = From Datos In TodasCategorias.Tables(0) _
        '      Let w = IIf(Datos.Item("ID").ToString().IndexOf("_") < 0, Datos.Item("ID").ToString().Length, Datos.Item("ID").ToString().IndexOf("_")) _
        'Order By Datos.Item("DEN") _
        '      Select Datos Distinct
        Dim ds As DataSet = New DataSet
        If query.Count > 0 Then
            ds.Tables.Add(query.CopyToDataTable())
        Else
            Dim dt As DataTable = TodasCategorias.Tables(0).Copy()
            dt.Clear()
            ds.Tables.Add(dt)
        End If
        tvwCategorias.DataSource = New HierarchicalDataSet(ds, "ID", "PADRE")
        tvwCategorias.DataBind()

    End Sub

    ''' <summary>
    ''' Procedimiento que carga el arbol de materiales, en caso de que se encuentre entre los filtros del informe.
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: El procedimiento CargarParametrosEnPagina
    ''' Tiempo máximo: Unos 0,3 seg</remarks>
    Private Sub CargarArbolMateriales()
        If Not PanelMaterialMostrado Then
            
            Dim cMaterial As FSNServer.cMaterial = FSNServer.Get_Object(GetType(FSNServer.cMaterial))
            Dim Ds As New DataSet
            Ds = TodosMateriales()
            Dim Tnode As New TreeNode
            Dim NodoHijo As New TreeNode
            tvwMateriales.Nodes.Clear()
            For Each fila As DataRow In Ds.Tables(0).Rows
                If fila.Item("Padre") Is Nothing OrElse IsDBNull(fila.Item("Padre")) Then
                    Tnode = New TreeNode
                    Tnode.Text = fila.Item("Den_Spa")
                    Tnode.Value = fila.Item("cod")
                    For Each otrafila As DataRow In Ds.Tables(0).Rows
                        If DBNullToStr(otrafila.Item("Padre")) = fila.Item("Cod") Then
                            Tnode.PopulateOnDemand = True
                        End If
                    Next

                    'NodoHijo = New TreeNode
                    'NodoHijo.Value = ""
                    'NodoHijo.Text = ""
                    'Tnode.ChildNodes.Add(NodoHijo)
                    tvwMateriales.Nodes.Add(Tnode)
                End If
            Next
            'If tvwMateriales.DataSource Is Nothing Then
            '    tvwMateriales.DataSource = New HierarchicalDataSet(Ds, "Cod", "Padre")
            '    tvwMateriales.DataBind()
            'End If
            PanelMaterialMostrado = True
            ViewState("PanelMaterialMostrado") = PanelMaterialMostrado
        End If
    End Sub

    ''' <summary>
    ''' Evento que se genera cuando se clica la imagen de cerrar del panel de materiales
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento, en este caso el ImageButton</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: La propia página, cada vez que se clica la imagen de cerrar el panel del arbol de materiales
    ''' Tiempo máximo: 0 seg</remarks>
    Protected Sub ImgBtnCerrarPanelMateriales_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        PanelMaterialMostrado = False
        mpePanelMateriales.Hide()
        ViewState("PanelMaterialMostrado") = PanelMaterialMostrado
    End Sub

    ''' <summary>
    ''' Propiedad que devuelve un dataset jerárquico con todos los materiales, su código y denominación
    ''' </summary>
    ''' <value>Valor que se guarda en la caché durante 10 minutos desde su acceso</value>
    ''' <returns>Un dataset jerárquico</returns>
    ''' <remarks>
    ''' Llamada desde:El procedimiento CargarArbolMateriales y BtnCrearInforme_Click
    ''' Tiempo máximo: Varía entre 0 seg(si esta en la caché) y 0,5 seg(si tiene que ejecutar la consulta)</remarks>
    Private ReadOnly Property TodosMateriales() As DataSet
        Get
            Dim ds As DataSet
            If HttpContext.Current.Cache("TodosMateriales") Is Nothing Then
                Dim oCMaterial As FSNServer.cMaterial = Me.FSNServer.Get_Object(GetType(FSNServer.cMaterial))
                ds = oCMaterial.DevolverTodosMateriales
                Me.InsertarEnCache("TodosMateriales", ds)
            Else
                ds = HttpContext.Current.Cache("TodosMateriales")
            End If
            Dim key() As DataColumn = {ds.Tables(0).Columns("Cod")}
            ds.Tables(0).PrimaryKey = key
            Return ds
        End Get
    End Property

    ''' <summary>
    ''' Propiedad que devuelve un dataset jerárquico con todas las categorias, su código y descripción
    ''' </summary>
    ''' <value>Valor que se guarda en la caché durante 10 minutos desde su acceso</value>
    ''' <returns>Un dataset jerárquico, con las categorias del catálogo</returns>
    ''' <remarks>
    ''' Llamada desde:El procedimiento CargarArbolCategorias y BtnCrearInforme_Click
    ''' Tiempo máximo: Varía entre 0 seg(si esta en la caché) y 0,5 seg(si tiene que ejecutar la consulta)</remarks>
    Private ReadOnly Property TodasCategorias() As DataSet
        Get
            Dim ds As DataSet
            If HttpContext.Current.Cache("TodasCategoriasReport") Is Nothing Then
                Dim oCCategorias As FSNServer.CCategorias = FSNServer.Get_Object(GetType(FSNServer.CCategorias))
                ds = oCCategorias.CargarArbolCategorias("")
                Me.InsertarEnCache("TodasCategoriasReport", ds)
            Else
                ds = HttpContext.Current.Cache("TodasCategoriasReport")
            End If
            Dim key() As DataColumn = {ds.Tables(0).Columns("ID")}
            ds.Tables(0).PrimaryKey = key
            Return ds
        End Get
    End Property

    ''' <summary>
    ''' Propiedad que devuelve un dataset jerárquico con todos los artículos, así como sus datos
    ''' </summary>
    ''' <value>Valor que se guarda en la caché durante 10 minutos desde su acceso</value>
    ''' <returns>Un dataset con los datos de los artículos del catálogo</returns>
    ''' <remarks>
    ''' Llamada desde:El evento tvwCategorias_SelectedNodeChanged,ImgBtnBorrarTextoCategorias_Click y BtnCrearInforme_Click
    ''' Tiempo máximo: Varía entre 0 seg(si esta en la caché) y 0,5 seg(si tiene que ejecutar la consulta)</remarks>
    Public ReadOnly Property DsetArticulos() As DataSet
        Get
            Dim dsArticulos As DataSet
            If HttpContext.Current.Cache("DsetArticulos_" & Me.Usuario.CodPersona) Is Nothing Then
                Dim oArticulos As FSNServer.cArticulos = FSNServer.Get_Object(GetType(FSNServer.cArticulos))
                dsArticulos = oArticulos.DevolverArticulosCatalogo(0, "", "", System.DBNull.Value, Nothing, Nothing, Nothing, Nothing, Nothing, System.DBNull.Value, FSNUser.CodPersona, 1, "", String.Empty, String.Empty, "")
                Me.InsertarEnCache("DsetArticulos_" & Me.Usuario.CodPersona, dsArticulos)
            Else
                dsArticulos = CType(HttpContext.Current.Cache("DsetArticulos_" & Me.Usuario.CodPersona), DataSet)
            End If
            Return dsArticulos
        End Get
    End Property

    ''' <summary>
    ''' Evento que se lanza cuando se clica la imagen del arbol de categorias en la zona del "Arbol de Categorias", abriendo el panel con el arbol para que el usuario seleccione una rama
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento, en este caso el ImageButton</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: La propia página, cada vez que se clica el ImageButton con el árbol de categorias
    ''' Tiempo máximo: 0 seg</remarks>
    Protected Sub BtnCategoria_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnCategoria.Click
        'CargarArbolCategorias()
        mpePanelArbol.Show()
    End Sub

    ''' <summary>
    ''' Evento que se lanza al clicar el ImageButton al lado del TextBox del Arbol de Categorias, con un dibujo de una goma; borrando el texto que se encuentre en el TextBox
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento, en este caso el ImageButton</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: La propia página, cada vez que se clica en el ImageButton
    ''' Tiempo máximo: 0 seg</remarks>
    Protected Sub ImgBtnBorrarTextoCategorias_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        CType(TablaParametros.FindControl("tbArbolCategorias"), TextBox).ReadOnly = False
        CType(TablaParametros.FindControl("tbArbolCategorias"), TextBox).Enabled = True
        CType(TablaParametros.FindControl("tbArbolCategorias"), TextBox).Text = ""
        CType(TablaParametros.FindControl("tbArbolCategorias"), TextBox).ReadOnly = True
        CType(TablaParametros.FindControl("tbArbolCategorias"), TextBox).Enabled = False
        CType(TablaParametros.FindControl("cbArticulos"), DropDownList).Items.Clear()
        CType(TablaParametros.FindControl("upZonaCategorias"), UpdatePanel).Update()
    End Sub

    ''' <summary>
    ''' Evento que se lanza cuando se clica la imagen del arbol de categorias en la zona de la "Estructura de materiales", abriendo el panel con el arbol jerárquico de materiales para que el usuario seleccione una rama
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento, en este caso el ImageButton</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: La propia página, cada vez que se clica el ImageButton con el árbol de Materiales
    ''' Tiempo máximo: 0 seg</remarks>
    Protected Sub BtnMateriales_CLick(ByVal sender As Object, ByVal e As EventArgs) Handles BtnMateriales.Click
        CargarArbolMateriales()
        PanelMaterialMostrado = True
        tvwMateriales.CollapseAll()
        UpArbolMateriales.Update()
        mpePanelMateriales.Show()
        ViewState("PanelMaterialMostrado") = PanelMaterialMostrado
    End Sub

    ''' <summary>
    ''' Evento que se lanza al clicar el ImageButton al lado del TextBox del Arbol de Materiales, con un dibujo de una goma; borrando el texto que se encuentre en el TextBox de la estructura de materiales
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento, en este caso el ImageButton</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: La propia página, cada vez que se clica en el ImageButton con la goma de la zona de Estructura de Materiales
    ''' Tiempo máximo: 0 seg</remarks>
    Protected Sub ImgBtnBorrarTextoMateriales_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        CType(TablaParametros.FindControl("tbArbolMateriales"), TextBox).ReadOnly = False
        CType(TablaParametros.FindControl("tbArbolMateriales"), TextBox).Enabled = True
        CType(TablaParametros.FindControl("tbArbolMateriales"), TextBox).Text = ""
        CType(TablaParametros.FindControl("tbArbolMateriales"), TextBox).ReadOnly = True
        CType(TablaParametros.FindControl("tbArbolMateriales"), TextBox).Enabled = False
        ViewState("TextoMaterial") = ""
        CType(TablaParametros.FindControl("upZonaMateriales"), UpdatePanel).Update()
    End Sub

    ''' <summary>
    ''' Evento que se lanza cada vez que se clica un nodo del árbol de Categorias, escribiendo esa rama en el TextBox etiquetado con Categoria del Catálogo
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento, en este caso el Treeview de categorias</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: La propia página, cada vez que se clica en un nodo del árbol de categorias.
    ''' Tiempo máximo: 0,1 seg</remarks>
    Protected Sub tvwCategorias_SelectedNodeChanged(ByVal sender As Object, ByVal e As EventArgs) Handles tvwCategorias.SelectedNodeChanged
        Dim StrNodo As String
        StrNodo = CType(sender, TreeView).SelectedNode.Text
        litCategoria.Visible = True
        litCategoria.Text = StrNodo
        LnkBtnBorrarTextoCategoria.Visible = True
        ViewState("TextoCategoria") = StrNodo
        upCategorias.Update()
        mpePanelArbol.Hide()
    End Sub

    ''' <summary>
    ''' Evento que se lanza cada vez que se clica un nodo del árbol de Materiales, escribiendo esa rama en el TextBox etiquetado con Estructura de materiales
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento, en este caso el Treeview de Estructura de materiales</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: La propia página, cada vez que se clica en un nodo del árbol de Estructura de Materiales.
    ''' Tiempo máximo: 0,1 seg</remarks>
    Protected Sub tvwMateriales_SelectedNodeChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim StrNodo As String
        StrNodo = CType(sender, TreeView).SelectedNode.Text
        LblTextoMateriales.Visible = True
        LblTextoMateriales.Text = StrNodo
        LnkBtnBorrarTextoMateriales.Visible = True
        upMateriales.Update()
        ViewState("TextoMaterial") = StrNodo
        mpePanelMateriales.Hide()
    End Sub

    ''' <summary>
    ''' Evento que se lanza cada vez que un usuario clica en la imagen de expandir un nodo, cargando los nodos hijos de ese nodo, si no tendría nodos hijos, no aparecería el "+" para expandirlo
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento, en este caso el Treeview</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: La propia página, cada vez que se clica el "+" para expandir un nodo
    ''' Tiempo máximo: 0.2 seg</remarks>
    Protected Sub tvwMateriales_TreeNodePopulate(ByVal sender As Object, ByVal e As TreeNodeEventArgs) Handles tvwMateriales.TreeNodePopulate
        Dim IdNodo As String
        IdNodo = e.Node.Value
        Dim ds As DataSet
        ds = TodosMateriales
        Dim Nivel As Integer
        Dim ArrayCods As String() = {}
        If IdNodo.Contains("_") Then
            ArrayCods = Split(IdNodo, "_", 4)
            Nivel = ArrayCods.Count
        Else
            Nivel = 1
        End If
        For Each nodo As TreeNode In tvwMateriales.Nodes
            If Nivel = 1 Then
                If nodo.Value = IdNodo Then
                    Dim Tnode As TreeNode
                    For Each fila As DataRow In ds.Tables(0).Rows
                        If DBNullToStr(fila.Item("Padre")) = IdNodo Then
                            Tnode = New TreeNode
                            Tnode.Text = fila.Item("Den_Spa")
                            Tnode.Value = fila.Item("Cod")
                            If Not Nivel >= 4 Then
                                Tnode.PopulateOnDemand = True
                            End If
                            nodo.ChildNodes.Add(Tnode)
                        End If
                    Next
                    Exit For
                End If
            ElseIf Nivel = 2 Then
                If nodo.Value = ArrayCods(0) Then
                    For Each NodoHijo As TreeNode In nodo.ChildNodes
                        If NodoHijo.Value = IdNodo Then
                            Dim Tnode As TreeNode
                            For Each fila As DataRow In ds.Tables(0).Rows
                                If DBNullToStr(fila.Item("Padre")) = IdNodo Then
                                    Tnode = New TreeNode
                                    Tnode.Text = fila.Item("Den_Spa")
                                    Tnode.Value = fila.Item("Cod")
                                    If Not Nivel >= 4 Then
                                        Tnode.PopulateOnDemand = True
                                    End If
                                    NodoHijo.ChildNodes.Add(Tnode)
                                End If
                            Next
                        End If
                    Next
                End If
            ElseIf Nivel = 3 Then
                If nodo.Value = ArrayCods(0) Then
                    For Each NodoHijo As TreeNode In nodo.ChildNodes
                        If NodoHijo.Value = ArrayCods(0) & "_" & ArrayCods(1) Then
                            For Each NodoHijoHijo As TreeNode In NodoHijo.ChildNodes
                                If NodoHijoHijo.Value = IdNodo Then
                                    Dim Tnode As TreeNode
                                    For Each fila As DataRow In ds.Tables(0).Rows
                                        If DBNullToStr(fila.Item("Padre")) = IdNodo Then
                                            Tnode = New TreeNode
                                            Tnode.Text = fila.Item("Den_Spa")
                                            Tnode.Value = fila.Item("Cod")
                                            Tnode.PopulateOnDemand = False
                                            NodoHijoHijo.ChildNodes.Add(Tnode)
                                        End If
                                    Next
                                End If
                            Next
                        End If
                    Next
                End If
            Else
            End If
        Next
        UpArbolMateriales.Update()
    End Sub

    ''' <summary>
    ''' Evento que se lanza al pinchar con el ratón en la ( X ) que aparece cuando se elige una categoria del arbol
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento, en este caso el LinkButton que contiene como texto ( X )</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: La propia página, cada vez que se clica el LinkButton al lado de la categoria elegida
    ''' Tiempo máximo: 0 seg</remarks>
    Protected Sub LnkBtnBorrarTextoCategoria_Click(ByVal sender As Object, ByVal e As EventArgs) Handles LnkBtnBorrarTextoCategoria.Click
        CType(upCategorias.FindControl("litCategoria"), Literal).Text = ""
        CType(upCategorias.FindControl("litCategoria"), Literal).Visible = False
        CType(sender, LinkButton).Visible = False
        upCategorias.Update()
    End Sub

    ''' <summary>
    ''' Evento que se lanza al pinchar con el ratón en la ( X ) que aparece cuando se elige un material del arbol de materiales
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento, en este caso el LinkButton que contiene como texto ( X )</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: La propia página, cada vez que se clica el LinkButton al lado del material elegido
    ''' Tiempo máximo: 0 seg</remarks>
    Protected Sub LnkBtnBorrarTextoMateriales_Click(ByVal sender As Object, ByVal e As EventArgs) Handles LnkBtnBorrarTextoMateriales.Click
        CType(upMateriales.FindControl("LblTextoMateriales"), Literal).Text = ""
        CType(upMateriales.FindControl("LblTextoMateriales"), Literal).Visible = False
        CType(sender, LinkButton).Visible = False
        ViewState("TextoMaterial") = ""
        upMateriales.Update()
    End Sub

    ''' <summary>
    ''' Evento que se genera al clicar el botón de cancelar al pedir la confirmación de generar el informe
    ''' </summary>
    ''' <param name="sender">El propio boton</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automática
    ''' Tiempo máximo: 0 seg</remarks>
    Protected Sub BtnCancelarGenerarInforme_CLick(ByVal sender As Object, ByVal e As EventArgs) Handles BtnCancelarGenerarInforme.Click
        mpeConfGenerarInforme.Hide()
    End Sub


End Class