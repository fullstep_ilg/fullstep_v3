<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master"
    CodeBehind="Report_Consumos.aspx.vb" Inherits="Fullstep.FSNWeb.Report_Consumos" %>

<%@ MasterType VirtualPath="~/App_Master/Menu.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
    <table cellpadding="10" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <img alt="" src="~/images/cabinformes.jpg" runat="server" />
                <asp:Label ID="lblTitulo" runat="server" Text="DListado" CssClass="Rotulo" Font-Size="14px" Height="40px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlCabeceraInformes" runat="server" SkinID="PanelColapsable">
                    &nbsp; &nbsp;
                    <asp:Image ID="imgExpandir" runat="server" ImageUrl="~/images/contraer.gif" />
                    &nbsp;&nbsp;
                    <asp:Label ID="lblParametros" runat="server" Text="DParámetros:" Font-Bold="True" ForeColor="White"></asp:Label>
                </asp:Panel>
                <asp:Panel ID="pnlParametros" runat="server" CssClass="Rectangulo">
                    <table id="tblRptParams" width="80%" border="0" runat="server">
                        <tr valign="top">
                            <td>
                                <asp:Table ID="tblParametros" runat="server" Font-Names="Verdana" Font-Size="8pt">
                                    <asp:TableRow ID="trAprov">
                                        <asp:TableCell>
                                            <asp:Label ID="lblAprov" runat="server" Text="DAprovisionador"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:DropDownList ID="ddlAprov" runat="server"></asp:DropDownList>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Label ID="lblDesde" runat="server" Text="DFecha Desde"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <igpck:WebDatePicker ID="dteDesde" runat="server" SkinID="Calendario"></igpck:WebDatePicker>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Label ID="lblHasta" runat="server" Text="DFecha Hasta"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <igpck:WebDatePicker ID="dteHasta" runat="server" SkinID="Calendario"></igpck:WebDatePicker>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom" height="20" style="text-align: right;">
                                <fsn:FSNButton ID="cmdObtener" runat="server" Text="DObtener"></fsn:FSNButton>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Label ID="lblNoExiste" runat="server" Height="24px" Visible="False"></asp:Label>
                <ajx:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" CollapseControlID="pnlCabeceraInformes"
                    CollapsedImage="~/images/expandir.gif" ExpandControlID="pnlCabeceraInformes"
                    ExpandedImage="~/images/contraer.gif" ImageControlID="imgExpandir" SuppressPostBack="True"
                    TargetControlID="pnlParametros">
                </ajx:CollapsiblePanelExtender>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="True"
                            HasCrystalLogo="False" Width="10px" ToolPanelView="None" HasToggleGroupTreeButton="False" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="CrystalReportViewer1" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
