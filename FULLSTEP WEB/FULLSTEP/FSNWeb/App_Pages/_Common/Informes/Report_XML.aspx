﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master"
    CodeBehind="Report_XML.aspx.vb" Inherits="Fullstep.FSNWeb.Report_XML" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
    <!-- Panel Confirmacion Generar Listado -->
    <asp:Panel runat="server" BackColor="White" BorderColor="DimGray" ID="pnlConfGenerarInforme"
        BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Left" Style="display: none;
        min-width: 150px; padding: 2px">
        <asp:Table runat="server" ID="TConfirmacionGenLis">
            <asp:TableRow>
                <asp:TableCell ColumnSpan="3" HorizontalAlign="Center">
                    <asp:Image ID="imgInfo" runat="server" ImageUrl="~/images/icono_info.gif" ImageAlign="Left" />
                    &nbsp;&nbsp;
                    <asp:Label ID="LblConfirmGenerarListado" runat="server" CssClass="normal" Text="DSe va a proceder con la generación del informe.¿Continuar?"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Right" Width="40%">
                    <fsn:FSNButton CssClass="Normal" ID="BtnAceptarGenerarInforme" runat="server" Alineacion="Right"
                        Text="DAceptar">
                    </fsn:FSNButton>
                </asp:TableCell>
                <asp:TableCell Width="20%">
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Left" Width="40%">
                    <fsn:FSNButton CssClass="Normal" ID="BtnCancelarGenerarInforme" runat="server" Alineacion="Left"
                        Text="DCancelar">
                    </fsn:FSNButton>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </asp:Panel>
    <button id="BtnOcGenerarInforme" runat="server" style="display: none">
    </button>
    <ajx:ModalPopupExtender ID="mpeConfGenerarInforme" runat="server" PopupControlID="pnlConfGenerarInforme"
        TargetControlID="BtnOcGenerarInforme" CancelControlID="BtnAceptarGenerarInforme">
    </ajx:ModalPopupExtender>
    <!-- Panel Categorías -->
    <ajx:CollapsiblePanelExtender ID="cpeBusqueda" runat="server" CollapseControlID="pnlCabeceraInformes"
        CollapsedImage="~/images/expandir.gif" ExpandControlID="pnlCabeceraInformes"
        ExpandedImage="~/images/contraer.gif" ImageControlID="imgExpandir" SuppressPostBack="True"
        TargetControlID="PanelParametros" Collapsed="False">
    </ajx:CollapsiblePanelExtender>
    <asp:Panel ID="PanelArbol" runat="server" BackColor="White" BorderColor="DimGray"
        BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Left" Style="display: none;
        min-width: 350px; padding: 2px" onmouseout="FSNCerrarPanelesTemp()" onmouseover="FSNNoCerrarPaneles()">
        <table width="350">
            <tr>
                <td align="left">
                    <asp:Image ID="ImgCatalogoArbol" runat="server" SkinID="Categorias" />
                    &nbsp; &nbsp;
                    <asp:Label ID="LblCatalogoArbol" runat="server" CssClass="Rotulo" Text="Arbol de categorias"> </asp:Label>
                </td>
                <td align="right">
                    <asp:ImageButton runat="server" ID="ImgBtnCerrarPanelArbol" SkinID="Cerrar" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:TreeView ID="tvwCategorias" CssClass="Normal" runat="server" CollapseImageToolTip="Colapsar {0}"
                        EnableViewState="false" ExpandImageToolTip="Expandir {0}" ImageSet="Custom" ExpandDepth="0"
                        OnSelectedNodeChanged="tvwCategorias_SelectedNodeChanged">
                        <DataBindings>
                            <asp:TreeNodeBinding DataMember="System.Data.DataRowView" TextField="DEN" ValueField="ID" />
                        </DataBindings>
                    </asp:TreeView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpePanelArbol" runat="server" PopupControlID="PanelArbol"
        TargetControlID="btnOculto" CancelControlID="ImgBtnCerrarPanelArbol" PopupDragHandleControlID="CeldaCategorias"
        RepositionMode="none" Enabled="false">
    </ajx:ModalPopupExtender>
    <input id="btnOculto" type="button" value="button" runat="server" style="display: none" />
    <!-- Panel Materiales -->
    <asp:Panel ID="PanelMateriales" runat="server" BackColor="White" BorderColor="DimGray"
        BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Left" Style="display: none;
        min-width: 350px; padding: 2px" onmouseout="FSNCerrarPanelesTemp()" onmouseover="FSNNoCerrarPaneles()">
        <table width="350">
            <tr>
                <td align="left">
                    <asp:Image ID="ImgMateriales" runat="server" SkinID="Materiales" />
                    &nbsp; &nbsp;
                    <asp:Label ID="LblTitMateriales" runat="server" CssClass="Rotulo" Text="Estructura de Materiales"> </asp:Label>
                </td>
                <td align="right">
                    <asp:ImageButton runat="server" ID="ImgBtnCerrarPanelMateriales" SkinID="Cerrar"
                        OnClick="ImgBtnCerrarPanelMateriales_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:UpdatePanel ID="UpArbolMateriales" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <asp:TreeView ID="tvwMateriales" CssClass="Normal" runat="server" PopulateNodesFromClient="true"
                                EnableClientScript="false" CollapseImageToolTip="Colapsar {0}" ExpandImageToolTip="Expandir {0}"
                                ImageSet="Custom" ExpandDepth="0" OnSelectedNodeChanged="tvwMateriales_SelectedNodeChanged"
                                OnTreeNodePopulate="tvwMateriales_TreeNodePopulate">                                
                            </asp:TreeView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpePanelMateriales" runat="server" PopupControlID="PanelMateriales"
        TargetControlID="BtnOcMat" CancelControlID="BtnCerrarMateriales" RepositionMode="none"
        PopupDragHandleControlID="CeldaCategorias" Enabled="false">        
    </ajx:ModalPopupExtender>
    <input id="BtnOcMat" type="button" value="button" runat="server" style="display: none" />
    <asp:Button ID="BtnCerrarMateriales" runat="server" Style="display: none" />
    <asp:Image ID="ImgCabInforme" runat="server" ImageUrl="~/images/cabinformes.jpg" />
    <asp:Label ID="lblTitulo" runat="server" Text="Listado" CssClass="RotuloGrande"></asp:Label>
    <asp:Panel ID="pnlCabeceraInformes" runat="server" SkinID="PanelColapsable">
        <table cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td valign="middle">
                    <asp:Image ID="imgExpandir" runat="server" ImageUrl="~/images/contraer.gif" />
                </td>
                <td valign="middle">
                    <asp:Label ID="lblParametros" runat="server" Text="Parámetros:" Font-Bold="True"
                        ForeColor="White"> </asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <asp:Label ID="lblNoExiste" runat="server" Visible="False"></asp:Label>
    <asp:Panel ID="PanelParametros" runat="server" Width="100%" DefaultButton="BtnCrearInforme"
        Height="300px">
        <table>
            <tr>
                <td>
                    <asp:Table ID="TablaParametros" runat="server" Width="100%" HorizontalAlign="Right">
                        <asp:TableRow runat="server" ID="FilaCatyMat" Visible="false">
                            <asp:TableCell ID="CeldaCategorias" runat="server" Visible="false">
                                <asp:UpdatePanel ID="upCategorias" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <table>
                                            <tr>
                                                <td align="left">
                                                    <fsn:FSNLinkInfo ID="BtnCategoria" runat="server" CssClass="Rotulo" Style="text-decoration: none;
                                                        text-align: left" PanelInfo="mpePanelArbol">
                                                        <asp:Literal ID="TextoCategoria" runat="server" Text="Categoria:"></asp:Literal>&nbsp;
                                                        <asp:Image ID="imgDespCategoria" runat="server" SkinID="desplegar" />
                                                    </fsn:FSNLinkInfo>
                                                    &nbsp; &nbsp;
                                                </td>
                                                <td align="right" colspan="2">
                                                    <asp:Literal ID="litCategoria" runat="server" Visible="False">
                                                    </asp:Literal>
                                                    <asp:LinkButton ID="LnkBtnBorrarTextoCategoria" runat="server" Visible="False" Text="(x)"
                                                        CssClass="Normal" Style="text-decoration: none; text-align: right">
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:TableCell>
                            <asp:TableCell runat="server" ID="CeldaMateriales" Visible="false">
                                <asp:UpdatePanel ID="upMateriales" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <table>
                                            <tr>
                                                <td align="left">
                                                    <fsn:FSNLinkInfo ID="BtnMateriales" runat="server" CssClass="Rotulo" Style="text-decoration: none;
                                                        text-align: left" PanelInfo="mpePanelMateriales">
                                                        <asp:Literal ID="LitMateriales" runat="server" Text="Materiales:"></asp:Literal>&nbsp;
                                                        <asp:Image ID="imgDespMateriales" runat="server" SkinID="desplegar" />
                                                    </fsn:FSNLinkInfo>
                                                    &nbsp; &nbsp;
                                                </td>
                                                <td align="right" colspan="2">
                                                    <asp:Literal ID="LblTextoMateriales" runat="server" Visible="False">
                                                    </asp:Literal>
                                                    <asp:LinkButton ID="LnkBtnBorrarTextoMateriales" runat="server" Visible="False" Text="(x)"
                                                        CssClass="Normal" Style="text-decoration: none; text-align: right" OnClick="LnkBtnBorrarTextoMateriales_Click">
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td>
                    <fsn:FSNButton ID="BtnCrearInforme" runat="server" Alineacion="Left" Text="Generar Informe"
                        CssClass="Rotulo" OnClick="BtnCrearInforme_Click"></fsn:FSNButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
