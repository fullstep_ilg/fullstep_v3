﻿Imports Fullstep.FSNLibrary.TiposDeDatos
Imports Fullstep.FSNServer
Partial Class Informes
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("Seccion") = "Listados"

        Master.Seleccionar("Informes", "Informes")

        ModuloIdioma = ModulosIdiomas.Menu
        lblTitulo.InnerText = Textos(25)

        'Que haga la select de las 3 aplicaciones, así siempre devuelve el mismo nº de tables.
        Dim oListados As Listados = FSNServer.Get_Object(GetType(FSNServer.Listados))
        oListados.LoadListadosPersonalizados(FSNUser.Cod, True, True, True)

        'Informes de PM
        If Acceso.gsAccesoFSPM <> TipoAccesoFSPM.SinAcceso And FSNUser.AccesoPM Then
            If oListados.Data.Tables(0).Rows.Count > 0 Then 'hay informes PM
                rptInformesPM.DataSource = oListados.Data.Tables(0)
                rptInformesPM.DataBind()
            End If
        End If
        'Informes de QA
        If Acceso.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso And FSNUser.AccesoQA Then
            If oListados.Data.Tables(1).Rows.Count > 0 Then 'hay informes QA
                rptInformesQA.DataSource = oListados.Data.Tables(1)
                rptInformesQA.DataBind()
            End If
        End If
        'Informes de EP
        If Acceso.gbAccesoFSEP And FSNUser.AccesoEP Then 'Siempre hay al menos 2
            rptInformesEP.DataSource = oListados.Data.Tables(2)
            rptInformesEP.DataBind()
        End If
       
    End Sub

    ''' <summary>
    ''' Escribe la cabecera de los informes de PM y escribe el nombre y el enlace por cada informe.
    ''' </summary>
    ''' <remarks>Se lanza cuando se han cargado los datos en el Repeater pero todavía no se ha mostrado en pantalla.</remarks>
    Private Sub rptInformesPM_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptInformesPM.ItemDataBound
        If e.Item.ItemType = ListItemType.Header Then
            CType(e.Item.Controls(0).FindControl("lblPM"), Label).Text = Textos(8)
        End If

        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) AndAlso CInt(IIf(IsDBNull(CType(e.Item.DataItem, System.Data.DataRowView).Row.Item("Filtro")), 0, CType(e.Item.DataItem, System.Data.DataRowView).Row.Item("Filtro"))) <> 0 Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim hlEnlace As HyperLink = CType(e.Item.FindControl("hlEnlace"), HyperLink)
                hlEnlace.Text = e.Item.DataItem(1)
                hlEnlace.NavigateUrl = "Report.aspx?FileReport=" & e.Item.DataItem(2) & "&ReportName=" & e.Item.DataItem(1) & "&Filtro=" & e.Item.DataItem(3)
            End If
        Else
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim hlEnlace As HyperLink = CType(e.Item.FindControl("hlEnlace"), HyperLink)
                hlEnlace.Text = e.Item.DataItem(1)
                hlEnlace.NavigateUrl = "Report.aspx?FileReport=" & e.Item.DataItem(2) & "&ReportName=" & e.Item.DataItem(1)
                If (New String() {".xls", ".xlsm"}).Contains(System.IO.Path.GetExtension(CType(e.Item.DataItem, DataRowView).Item(2))) Then
                    hlEnlace.NavigateUrl = "Report_XML.aspx?FileReport=" & e.Item.DataItem(2) & "&ReportName=" & e.Item.DataItem(1)
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' Escribe la cabecera de los informes de QA y escribe el nombre y el enlace por cada informe.
    ''' </summary>
    ''' <remarks>Se lanza cuando se han cargado los datos en el Repeater pero todavía no se ha mostrado en pantalla.</remarks>
    Private Sub rptInformesQA_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptInformesQA.ItemDataBound
        If e.Item.ItemType = ListItemType.Header Then
            CType(e.Item.Controls(0).FindControl("lblQA"), Label).Text = Textos(9)
        End If
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim hlEnlace As HyperLink = CType(e.Item.FindControl("hlEnlace"), HyperLink)
            hlEnlace.Text = e.Item.DataItem(1)
            hlEnlace.NavigateUrl = "Report.aspx?FileReport=" & e.Item.DataItem(2) & "&ReportName=" & e.Item.DataItem(1)
            If (New String() {".xls", ".xlsm"}).Contains(System.IO.Path.GetExtension(CType(e.Item.DataItem, DataRowView).Item(2))) Then
                hlEnlace.NavigateUrl = "Report_XML.aspx?FileReport=" & e.Item.DataItem(2) & "&ReportName=" & e.Item.DataItem(1)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Escribe la cabecera de los informes de EP y escribe el nombre y el enlace por cada informe.
    ''' </summary>
    ''' <remarks>Se lanza cuando se han cargado los datos en el Repeater pero todavía no se ha mostrado en pantalla.</remarks>
    Private Sub rptInformesEP_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptInformesEP.ItemDataBound
        If e.Item.ItemType = ListItemType.Header Then
            CType(e.Item.Controls(0).FindControl("lblEP"), Label).Text = Textos(21)
            CType(e.Item.Controls(0).FindControl("hlInfCat"), HyperLink).Text = Textos(26)
            CType(e.Item.Controls(0).FindControl("hlInfCat"), HyperLink).NavigateUrl = "Report_Consumos.aspx?FileReport=ConsumosCategorias.rpt&ReportName=" & Server.UrlEncode(Textos(26))
            CType(e.Item.Controls(0).FindControl("hlInfProve"), HyperLink).Text = Textos(27)
            CType(e.Item.Controls(0).FindControl("hlInfProve"), HyperLink).NavigateUrl = "Report_Consumos.aspx?FileReport=ConsumosProveedores.rpt&ReportName=" & Server.UrlEncode(Textos(27))
        End If
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim hlEnlace As HyperLink = CType(e.Item.FindControl("hlEnlace"), HyperLink)
            hlEnlace.Text = e.Item.DataItem(1)
            hlEnlace.NavigateUrl = "Report.aspx?FileReport=" & e.Item.DataItem(2) & "&ReportName=" & e.Item.DataItem(1)
            If (New String() {".xls", ".xlsm"}).Contains(System.IO.Path.GetExtension(CType(e.Item.DataItem, DataRowView).Item(2))) Then
                hlEnlace.NavigateUrl = "Report_XML.aspx?FileReport=" & e.Item.DataItem(2) & "&ReportName=" & e.Item.DataItem(1)
            End If
        End If
    End Sub

End Class
