﻿Imports CrystalDecisions.CrystalReports.Engine
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports CrystalDecisions.Shared
Imports System.Web.Script.Serialization
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web


Partial Public Class Report
    Inherits FSNPage

    Private Report As New ReportDocument
    Private vEstructura(2) As String

    ''' <summary>
    ''' Evento de carga inicial de la página, en el que cargaremos los textos y los controles correspondientes a los parámetros del report solicitado.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Tiempo máximo: Depende del nº y tipo de los parámetros configurados en el report (si han de acceder a BBDD, etc...)</remarks>
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Master.Seleccionar("Informes", "Informes")

        ModuloIdioma = ModulosIdiomas.Listados

        cmdObtener.Text = Textos(6) ' Actualizar
        lblNoExiste.Text = Textos(5) ' Imposible mostrar el listado: Ha sido eliminado de su ubicación física.
        lblParametros.Text = Textos(7) ' Parámetros
        lblTitulo.Text = Request("ReportName")

        If IsPostBack And Not IsNothing(Cache("MiReport")) Then
            'Si estamos haciendo postback y tenemos el report en la caché lo recuperamos
            Report = Cache("MiReport")
            Me.lblNoExiste.Visible = False
        Else
            'Si no, eliminamos el report de la caché 
            Cache.Remove("MiReport")

            Dim sFileName As String

            sFileName = ConfigurationManager.AppSettings("rptFilesPath") & Request("FileReport")

            If Not IO.File.Exists(sFileName) Then
                'Si no existe el listado en esa ubicación sacamos el mensaje
                Me.tblRptParams.Visible = False
                Me.cmdObtener.Visible = False
                Me.lblNoExiste.Visible = True
                pnlClasificacion.Visible = False
                pnlSolicitud1.Visible = False
                pnlSolicitud2.Visible = False
                pnlProveedor1.Visible = False
                pnlProveedor2.Visible = False
                pnlTiemposMediosPorCarpeta1.Visible = False
                pnlTiemposMediosPorCarpeta2.Visible = False
                pnlTiemposMediosPorCarpeta3.Visible = False
                pnlTiemposMediosPorCarpeta4.Visible = False
                pnlTiemposMediosPorIdentificador1.Visible = False
                pnlTiemposMediosPorIdentificador2.Visible = False
                pnlTiemposMediosPorIdentificador3.Visible = False
                Exit Sub
            Else
                Me.lblNoExiste.Visible = False
            End If

            'Cargamos el report
            Report.Load(sFileName, OpenReportMethod.OpenReportByTempCopy)

        End If

        CargarParametrosEnPagina()
      
        If IsPostBack And Not IsNothing(Cache("MiReport")) Then
            'Si estamos haciendo postback y tenemos el report en la caché (esto es, cuando
            'el usuario está paginando el report), se lo pasamos al visor
            CrystalReportViewer1.ReportSource = Report
        End If

    End Sub

    ''' <summary>
    ''' Procedimiento que cargará un control por cada parámetro que haya en el report, con sus valores correspondientes.
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: El evento Page_Init de Report.aspx;
    ''' Tiempo máximo: Depende del nº y tipo de los parámetros configurados en el report (si han de acceder a BBDD, etc...)
    ''' </remarks>
    Sub CargarParametrosEnPagina()

        pnlClasificacion.Visible = False
        pnlSolicitud1.Visible = False
        pnlSolicitud2.Visible = False
        pnlProveedor1.Visible = False
        pnlProveedor2.Visible = False
        pnlTiemposMediosPorCarpeta1.Visible = False
        pnlTiemposMediosPorCarpeta2.Visible = False
        pnlTiemposMediosPorCarpeta3.Visible = False
        pnlTiemposMediosPorCarpeta4.Visible = False
        pnlTiemposMediosPorIdentificador1.Visible = False
        pnlTiemposMediosPorIdentificador2.Visible = False
        pnlTiemposMediosPorIdentificador3.Visible = False

        If Not (Request("Filtro") Is Nothing) AndAlso (DBNullToInteger(Request("Filtro")) >= 1 And DBNullToInteger(Request("Filtro")) <= 5) Then
            CargarParametrostiemposMedios(Request("Filtro"))
            Exit Sub
        End If

        Dim otblRow As System.Web.UI.WebControls.TableRow
        Dim otblCell As System.Web.UI.WebControls.TableCell

        Dim oLabel As System.Web.UI.WebControls.Label

        ' Leemos los parámetros que tiene el listado
        Dim cont As Integer = 0
        Dim crParam As ParameterField

        If Report.ParameterFields.Count > 0 Then


            'Creamos el dataset que vamos a usar para recoger los valores por defecto que puedan tener los parámetros
            Dim dsParametros As New DataSet
            Dim oRow(0)

            For Each crParam In Report.ParameterFields

                If Report.Subreports.Item(crParam.ReportName) Is Nothing Then

                    'Se instancia la Fila que va a albergar a éste parámetro
                    otblRow = New System.Web.UI.WebControls.TableRow
                    'Se instancia la Celda que va a albergar a la label con el título de este parámetro
                    otblCell = New System.Web.UI.WebControls.TableCell

                    'Se instancia la Etiqueta que tendrá el nombre del parámetro
                    oLabel = New System.Web.UI.WebControls.Label
                    oLabel.Text = " " & crParam.PromptText
                    oLabel.Font.Name = "Verdana"
                    oLabel.Font.Size = System.Web.UI.WebControls.FontUnit.Point(8)

                    otblCell.Controls.Add(oLabel)


                    otblRow.Cells.Add(otblCell)

                    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
                    crParameterFieldDefinitions = Report.DataDefinition.ParameterFields()

                    'Se instancia la Celda que va a albergar al control con el contenido de este parámetro
                    otblCell = New System.Web.UI.WebControls.TableCell

                    If crParam.DefaultValues.Count > 0 Then
                        'Tiene valores Predefinidos

                        Dim j As Integer = 0
                        dsParametros.Reset()
                        dsParametros.Tables.Add("Parametros")
                        dsParametros.Tables("Parametros").Columns.Add("Valor1")
                        For j = 0 To crParam.DefaultValues.Count - 1
                            oRow(0) = crParameterFieldDefinitions.Item(cont).DefaultValues(j).Description
                            dsParametros.Tables("Parametros").Rows.Add(oRow)
                        Next

                        Dim oLista As New System.Web.UI.WebControls.DropDownList

                        With oLista
                            .ID = crParam.Name
                            .EnableViewState = True
                            '.Width = Unit.Percentage(100)
                            .DataSource = dsParametros
                            .DataValueField = "Valor1"
                            .DataTextField = "Valor1"
                            .DataBind()
                        End With

                        otblCell.Controls.Add(oLista)

                    Else
                        'Comprobamos si debemos acceder a BBDD o no para este parámetro
                        If Comprobar_estructura(crParam.Name) > 0 Then
                            'Buscar en la BBDD
                            ReDim Preserve vEstructura(2)
                            Dim oListados As Fullstep.FSNServer.Listados
                            'Traemos los datos requeridos por el parámetro de la BBDD
                            oListados = FSNServer.Get_Object(GetType(FSNServer.Listados))
                            dsParametros = oListados.CargarDatos_TablaIndefinida(vEstructura(0), vEstructura(1), vEstructura(2))
                            oListados = Nothing

                            Dim oLista As New System.Web.UI.WebControls.DropDownList

                            With oLista
                                .ID = crParam.Name
                                .EnableViewState = True
                                .DataSource = dsParametros
                                .DataValueField = vEstructura(1)
                                If vEstructura(2) = "" Or vEstructura Is Nothing Then
                                    .DataTextField = vEstructura(1)
                                Else
                                    .DataTextField = vEstructura(2)
                                End If
                                .DataBind()
                            End With

                            otblCell.Controls.Add(oLista)
                        Else
                            'El parametro es de tipo Fecha 
                            If crParam.ParameterValueType = ParameterValueKind.DateParameter Then
                                Dim oFecha As New Infragistics.Web.UI.EditorControls.WebDatePicker

                                With oFecha
                                    .EnableViewState = True
                                    .ID = crParam.Name
                                    .Width = Unit.Pixel(250)
                                    .NullText = ""
                                    .SkinID = "Calendario"
                                End With
                                otblCell.Controls.Add(oFecha)
                            Else

                                'El parametro es de tipo Texto 
                                Dim oTexto As New System.Web.UI.WebControls.TextBox

                                With oTexto
                                    .EnableViewState = True
                                    .ID = crParam.Name
                                    .Width = Unit.Pixel(250)
                                End With
                                otblCell.Controls.Add(oTexto)
                            End If

                        End If
                    End If

                    'Si el nombre del parámetro comienza por (*) insertamos un control RequiredFieldValidator,
                    'que controle que el usuario no lo deja vacío
                    If Left(crParam.PromptText, 3) = "(*)" Then
                        'Insertamos el RequiredFieldValidator
                        Dim oReq As New RequiredFieldValidator
                        With oReq
                            .Display = ValidatorDisplay.Dynamic
                            .ErrorMessage = Textos(8)
                            .ControlToValidate = crParam.Name
                            .EnableClientScript = False
                        End With
                        otblCell.Controls.Add(oReq)
                    End If

                    otblRow.Cells.Add(otblCell)
                    otblRow.Height = Unit.Pixel(20)

                    tblParametros.Rows.Add(otblRow)

                    cont = cont + 1

                End If

            Next

        End If
    End Sub

    ''' <summary>
    ''' Comprueba si el nombre del parámetro contiene ".",  lo que querría decir que debemos acceder a BBDD
    ''' </summary>
    ''' <param name="sVal">Nombre del parámetro</param>
    ''' <returns>el número de posiciones separadas por puntos</returns>
    ''' <remarks>
    ''' Llamada desde: el procedimiento CargarParametrosEnPágina
    ''' Tiempo máximo: 0 seg</remarks>
    Private Function Comprobar_estructura(ByVal sVal As String) As Byte
        vEstructura = Split(sVal, ".")
        Comprobar_estructura = UBound(vEstructura)
    End Function


    ''' <summary>
    ''' Evento que se lanza al pulsar sobre el botón Actualizar, en el que pasaremos al informe los valores seleccionados por el usuario para cada parámetro, los parámetros de conexión a la BBDD y mostraremos el informe 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' Tiempo máximo: 0 seg
    ''' </remarks>
    Private Sub cmdObtener_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdObtener.Click, cmdObtenerTMCarpeta.Click, cmdObtenerTMIdentificador.Click
        If pnlClasificacion.Visible = False Then
            valReportSelCategoria.IsValid = True
            lblError.Text = ""
            pnlError.Visible = False
            upPnlError.Update()
        End If

        'la carpeta de categorias se valida para : tiempos medio por carpeta (tipo1), tiempos medios por tiposolicitud(2)
        'tiempos medios negociados por tiposolicitud (4)
        If Request("Filtro") = "1" Or Request("Filtro") = "2" Or Request("Filtro") = "4" Then
            'Validar que hay contenido en el campo txtReportSelCategoria
            If txtReportSelCategoria IsNot Nothing Then
                If (txtReportSelCategoria.Text = "") Then
                    valReportSelCategoria.IsValid = False
                    lblError.Text = Textos(49) 'Debe seleccionar una carpeta de Clasificación para continuar
                    pnlError.Visible = True
                    upPnlError.Update()
                Else
                    valReportSelCategoria.IsValid = True
                    lblError.Text = ""
                    pnlError.Visible = False
                    upPnlError.Update()
                End If
            End If
        End If
        'El tipo de solicitud se valida para : tiempos medios (por tiposol) y tiemposmed negociados por tiposol (4)
        If Request("Filtro") = "2" Or Request("Filtro") = "4" Then
            If Me.idTipoSolicitud.Value = "" Then
                valTipoSolicitud.IsValid = False
                If lblError.Text <> "" Then lblError.Text &= "<br />"
                lblError.Text &= Textos(50)

                pnlError.Visible = True
                upPnlError.Update()
            End If
        End If
        If Page.IsValid Then
            ' Pasamos los valores de los parámetros al listado
            Dim crParam As ParameterField
            Dim iFila As Integer = 0
            Dim sFileName As String

            Try

                'Plegamos el panel de parámetros
                CollapsiblePanelExtender1.Collapsed = True
                CollapsiblePanelExtender1.ClientState = "true"

                sFileName = ConfigurationManager.AppSettings("rptFilesPath") & Request("FileReport")

                If Dir(sFileName) <> "" Then

                    Report.Load(sFileName, OpenReportMethod.OpenReportByTempCopy)

                    If Request("Filtro") = 1 Or Request("Filtro") = 2 Or Request("Filtro") = 4 Then
                        'INICIO TRATAMIENTO DE 1.1 Listado de tiempos medios por carpeta
                        Dim strCategoria As String
                        Dim idCategoria As Integer
                        strCategoria = txtReportSelCategoria.Text
                        If Not strCategoria Is Nothing Then
                            idCategoria = idSelCategoria.Value
                            Dim inivel As Integer
                            Dim arrLetras() As String
                            arrLetras = Split(strCategoria, ">")
                            inivel = UBound(arrLetras) + 1

                            If inivel = 1 Then
                                Report.SetParameterValue("Proceso", CInt(idCategoria))
                                Report.SetParameterValue("Solicitud", 0)
                                Report.SetParameterValue("Flujo", 0)
                            Else
                                If inivel = 2 Then
                                    Dim ds As DataSet

                                    Dim oSolicitudes As FSNServer.Solicitudes
                                    oSolicitudes = FSNServer.Get_Object(GetType(FSNServer.Solicitudes))
                                    ds = oSolicitudes.Obtener_NivelesCarpetasSolicitudes(CInt(idCategoria), inivel)

                                    If Not ds Is Nothing Then
                                        If ds.Tables.Count > 0 Then
                                            If ds.Tables(0) IsNot Nothing Then
                                                If ds.Tables(0).Rows.Count >= 1 Then

                                                    Report.SetParameterValue("Solicitud", ds.Tables(0).Rows(0).Item("CSN1"))
                                                    Report.SetParameterValue("Proceso", CInt(idCategoria))
                                                    Report.SetParameterValue("Flujo", 0)

                                                End If
                                            End If
                                        End If
                                    End If
                                Else
                                    If inivel = 3 Then
                                        Dim ds As DataSet

                                        Dim oSolicitudes As FSNServer.Solicitudes
                                        oSolicitudes = FSNServer.Get_Object(GetType(FSNServer.Solicitudes))
                                        ds = oSolicitudes.Obtener_NivelesCarpetasSolicitudes(CInt(idCategoria), inivel)

                                        If Not ds Is Nothing Then
                                            If ds.Tables.Count > 0 Then
                                                If ds.Tables(0) IsNot Nothing Then
                                                    If ds.Tables(0).Rows.Count >= 1 Then

                                                        Report.SetParameterValue("Solicitud", ds.Tables(0).Rows(0).Item("CSN1"))
                                                        Report.SetParameterValue("Proceso", ds.Tables(0).Rows(0).Item("CSN2"))
                                                        Report.SetParameterValue("Flujo", CInt(idCategoria))

                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If

                        If WDFecDesde.Text <> "" Then
                            Report.SetParameterValue("Desde", WDFecDesde.Value)
                        Else
                            Report.SetParameterValue("Desde", "")
                        End If
                        If WDFecHasta.Text <> "" Then
                            Report.SetParameterValue("Hasta", WDFecHasta.Value)
                        Else
                            Report.SetParameterValue("Hasta", "")
                        End If
                        'FIN TRATAMIENTO DE 1.1 Listado de tiempos medios por carpeta
                        If Request("Filtro") = 2 Or Request("Filtro") = 4 Then
                            Report.SetParameterValue("TipoSolicitud", idTipoSolicitud.Value)
                            If chkMostrarDetalleSolicitudes.Checked = True Then
                                Report.SetParameterValue("Detalle", "S") 'Checkeado 
                            Else
                                Report.SetParameterValue("Detalle", "") 'Sin Checkear
                            End If

                            If Request("Filtro") = 4 Then
                                Report.SetParameterValue("Proveedor", Me.hidProveedor.Value)
                            End If

                        End If
                    Else
                        If Request("Filtro") = 3 Or Request("Filtro") = 5 Then

                            Report.SetParameterValue("NumSolicitud", NumIdentificador.Value)
                            Report.SetParameterValue("Solicitud", 0)
                            Report.SetParameterValue("Proceso", 0)
                            Report.SetParameterValue("TipoSolicitud", "")
                            Report.SetParameterValue("Flujo", 0)
                            If Request("Filtro") = 5 Then
                                '1.5	Listado tiempos medios de negociación (por identificador de solicitud)
                                Report.SetParameterValue("Proveedor", "")
                            End If
                            If Request("Filtro") = 3 Then
                                '1.3	Listado de tiempos medios por identificador
                                Report.SetParameterValue("Desde", "")
                                Report.SetParameterValue("Hasta", "")
                            End If

                        Else

                            Dim lInstancia As Long
                            lInstancia = Request("Instancia")

                            If lInstancia <> 0 Then
                                For Each crParam In Report.ParameterFields
                                    If Report.Subreports.Item(crParam.ReportName) Is Nothing Then
                                        Report.SetParameterValue(crParam.Name, CLng(lInstancia))
                                    End If
                                Next

                            Else
                                For Each crParam In Report.ParameterFields

                                    If Report.Subreports.Item(crParam.ReportName) Is Nothing Then
                                        If crParam.DefaultValues.Count > 0 Or Comprobar_estructura(crParam.Name) > 0 Then
                                            Select Case crParam.ParameterValueType
                                                Case ParameterValueKind.NumberParameter
                                                    Report.SetParameterValue(crParam.Name, CLng(IIf(CType(tblParametros.FindControl(crParam.Name), DropDownList).SelectedValue <> "", CType(tblParametros.FindControl(crParam.Name), DropDownList).SelectedValue, 0)))
                                                Case ParameterValueKind.DateParameter, ParameterValueKind.DateTimeParameter
                                                    Report.SetParameterValue(crParam.Name, CDate(CType(tblParametros.FindControl(crParam.Name), DropDownList).SelectedValue))
                                                Case Else
                                                    Report.SetParameterValue(crParam.Name, CType(tblParametros.FindControl(crParam.Name), DropDownList).SelectedValue)
                                            End Select
                                        Else
                                            Select Case crParam.ParameterValueType
                                                Case ParameterValueKind.NumberParameter
                                                    Report.SetParameterValue(crParam.Name, CLng(IIf(CType(tblParametros.FindControl(crParam.Name), TextBox).Text <> "", CType(tblParametros.FindControl(crParam.Name), TextBox).Text, 0)))
                                                Case ParameterValueKind.DateParameter
                                                    Report.SetParameterValue(crParam.Name, CDate(CType(tblParametros.FindControl(crParam.Name), Infragistics.Web.UI.EditorControls.WebDatePicker).Text))
                                                Case ParameterValueKind.DateTimeParameter
                                                    Report.SetParameterValue(crParam.Name, CDate(CType(tblParametros.FindControl(crParam.Name), TextBox).Text))
                                                Case Else
                                                    Report.SetParameterValue(crParam.Name, CType(tblParametros.FindControl(crParam.Name), TextBox).Text)
                                            End Select
                                        End If

                                        iFila = iFila + 1
                                    End If

                                Next

                            End If
                        End If
                    End If

                    Dim crtableLogoninfos As New TableLogOnInfos
                    Dim crtableLogoninfo As New TableLogOnInfo
                    Dim myConnectionInfo As New ConnectionInfo
                    Dim CrTables As Tables
                    Dim CrTable As Table
                    Dim CrSubReports As Subreports
                    Dim CrSubReport As ReportDocument

                    'Obtenemos las propiedades de conexión...
                    FSNServer.get_PropiedadesConexion()

                    With myConnectionInfo
                        .ServerName = FSNServer.DBServidor
                        .DatabaseName = FSNServer.DBName
                        .UserID = FSNServer.DBLogin
                        .Password = FSNServer.DBPassword
                    End With

                    'Recorremos las subreports del Report
                    'y les asignamos los parametros de conexion a sus tablas
                    'Es FUNDAMENTAL asignar los parametros de conexion a los subreports antes que al report
                    CrSubReports = Report.Subreports
                    For Each CrSubReport In CrSubReports
                        CrTables = CrSubReport.Database.Tables
                        For Each CrTable In CrTables
                            crtableLogoninfo = CrTable.LogOnInfo
                            crtableLogoninfo.ConnectionInfo = myConnectionInfo
                            CrTable.ApplyLogOnInfo(crtableLogoninfo)
                        Next
                    Next

                    'Recorremos las tablas del Report (los comandos están incluidos) 
                    'y les asignamos los parámetros de conexión
                    CrTables = Report.Database.Tables
                    For Each CrTable In CrTables
                        crtableLogoninfo = CrTable.LogOnInfo
                        crtableLogoninfo.ConnectionInfo = myConnectionInfo
                        CrTable.ApplyLogOnInfo(crtableLogoninfo)
                    Next

                    ' Pasamos el listado, ya con los datos de conexión y parámetros al formulario con el visor.
                    CrystalReportViewer1.ReportSource = Report

                    ' Pasamos las propiedades de conexión al Visor
                    SetDBLogonForReport(myConnectionInfo)

                    CrystalReportViewer1.Visible = True

                End If

                'Introducimos el objeto Report en la caché, para mantener así los parámetros entre postbacks                
                Me.InsertarEnCache("MiReport", Report)

                'Plegamos el panel de parámetros
                CollapsiblePanelExtender1.Collapsed = True
                CollapsiblePanelExtender1.ClientState = "true"

            Catch ex As Exception


            End Try


        Else
            'Si no pasamos las validaciones de parámetros requeridos no mostramos el visor
            CrystalReportViewer1.Visible = False
        End If
    End Sub
    ''' <summary>
    ''' Procedimiento que vincula el visor de Crystal con los parámetros de conexión
    ''' </summary>
    ''' <param name="myConnectionInfo">Información de conexión (Server, BBDD, User, Pwd)</param>
    ''' <remarks>
    ''' Llamada desde: cmdActualizar_Click de Report.aspx
    ''' Tiempo máximo: 0 seg
    ''' </remarks>
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo)

        Dim myTableLogOnInfos As TableLogOnInfos = CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub

    ''' <summary>
    ''' Procedimiento que cargará un control por cada parámetro que haya en el report, con sus valores correspondientes.
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: El evento Page_Init de Report.aspx;
    ''' Tiempo máximo: Depende del nº y tipo de los parámetros configurados en el report (si han de acceder a BBDD, etc...)
    ''' </remarks>
    Sub CargarParametrostiemposMedios(ByVal idFiltro As Integer)
        
        pnlClasificacion.Visible = True
        pnlParametros.Visible = False
        pnlSolicitud1.Visible = False
        pnlSolicitud2.Visible = False
        pnlProveedor1.Visible = False
        pnlProveedor2.Visible = False
        pnlTiemposMediosPorCarpeta1.Visible = False
        pnlTiemposMediosPorCarpeta2.Visible = False
        pnlTiemposMediosPorCarpeta3.Visible = False
        pnlTiemposMediosPorCarpeta4.Visible = False
        pnlTiemposMediosPorIdentificador1.Visible = False
        pnlTiemposMediosPorIdentificador2.Visible = False
        pnlTiemposMediosPorIdentificador3.Visible = False

        lblParametros.Text = Textos(47) '"Introduzca los siguientes criterios para el listado:"
        cmdObtener.Text = Textos(4) '"Obtener"
        cmdObtenerTMCarpeta.Text = Textos(4) '"Obtener"
        cmdObtenerTMIdentificador.Text = Textos(4) '"Obtener"

        Select idFiltro

            Case 1
                pnlTiemposMediosPorCarpeta1.Visible = True
                pnlTiemposMediosPorCarpeta2.Visible = True
                pnlTiemposMediosPorCarpeta3.Visible = True
                pnlTiemposMediosPorCarpeta4.Visible = True
                chkMostrarDetalleSolicitudes.Visible = False
                chkMostrarDetalleSolicitudes.Visible = True
                lblDesde.Text = "" & Textos(41) '"Fecha Desde: "
                lblHasta.Text = "" & Textos(42) '"Fecha Hasta: "
                lblClasificacion.Text = "" & Textos(48) '"Clasificación: "
                chkMostrarDetalleSolicitudes.Visible = False
            Case 2
                pnlSolicitud1.Visible = True
                pnlSolicitud2.Visible = True
                pnlTiemposMediosPorCarpeta1.Visible = True
                pnlTiemposMediosPorCarpeta2.Visible = True
                pnlTiemposMediosPorCarpeta3.Visible = True
                pnlTiemposMediosPorCarpeta4.Visible = True
                pnlTiemposMediosPorIdentificador1.Visible = False
                pnlTiemposMediosPorIdentificador2.Visible = False
                pnlTiemposMediosPorIdentificador3.Visible = False

                chkMostrarDetalleSolicitudes.Visible = True
                lblDesde.Text = "" & Textos(41) '"Fecha Desde: "
                lblHasta.Text = "" & Textos(42) '"Fecha Hasta: "
                lblClasificacion.Text = "" & Textos(48) '"Clasificación: "
                chkMostrarDetalleSolicitudes.Text = "" & Textos(43) '"Mostrar el detalle de todas las solicitudes "
                LblSolicitud.Text = "" & Textos(44) '"Solicitud: "
            Case 3, 5
                pnlClasificacion.Visible = False
                pnlTiemposMediosPorIdentificador1.Visible = True
                pnlTiemposMediosPorIdentificador2.Visible = True
                pnlTiemposMediosPorIdentificador3.Visible = True

                pnlParametros.Visible = False
                pnlTiemposMediosPorCarpeta1.Visible = False
                pnlTiemposMediosPorCarpeta2.Visible = False
                pnlTiemposMediosPorCarpeta3.Visible = False
                pnlTiemposMediosPorCarpeta4.Visible = False
                lblIdentificador.Text = " " & Textos(45) '"Identificador: "
            Case 4
                pnlSolicitud1.Visible = True
                pnlSolicitud2.Visible = True
                pnlProveedor1.Visible = True
                pnlProveedor2.Visible = True
                pnlTiemposMediosPorCarpeta1.Visible = True
                pnlTiemposMediosPorCarpeta2.Visible = True
                pnlTiemposMediosPorCarpeta3.Visible = True
                pnlTiemposMediosPorCarpeta4.Visible = True
                pnlTiemposMediosPorIdentificador1.Visible = False
                pnlTiemposMediosPorIdentificador2.Visible = False
                pnlTiemposMediosPorIdentificador3.Visible = False
                chkMostrarDetalleSolicitudes.Visible = True
                InicializacionImgProveedorLupa()
                lblDesde.Text = "" & Textos(41) '"Fecha Desde: "
                lblHasta.Text = "" & Textos(42) '"Fecha Hasta: "
                lblClasificacion.Text = "" & Textos(48) '"Clasificación: "
                chkMostrarDetalleSolicitudes.Text = "" & Textos(43) '"Mostrar el detalle de todas las solicitudes "
                lblProveedor.Text = "" & Textos(46) '"Proveedor: "
                LblSolicitud.Text = "" & Textos(44) '"Solicitud: "
        End Select
    End Sub

    Private Sub InicializacionImgProveedorLupa()
        imgProveedorLupa.OnClientClick = "var newWindow = window.open('" & ConfigurationManager.AppSettings("RutaFS") & "_common/BuscadorProveedores.aspx?Notificacion=1', '_blank', 'width=835,height=635,status=yes,resizable=no,top=0,left=150,scrollbars=yes');return false;"
    End Sub
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Shared Function Cargar_CarpetasSolicitudes() As List(Of FSNServer.cn_fsTreeViewItem)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oSolicitudes As FSNServer.Solicitudes
        oSolicitudes = FSNServer.Get_Object(GetType(FSNServer.Solicitudes))

        Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim oCategorias As New List(Of FSNServer.cn_fsTreeViewItem)
        With CN_Usuario
            oCategorias = oSolicitudes.Obtener_CarpetasSolicitudes()
        End With

        Return oCategorias
    End Function

    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Shared Function Cargar_TiposSolicitudes(ByVal IdClasificacion As Integer, ByVal txtClasificacion As String) As String
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oSolicitudes As FSNServer.Solicitudes
        Dim sResultado As String = String.Empty
        oSolicitudes = FSNServer.Get_Object(GetType(FSNServer.Solicitudes))

        Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim dsTiposSolicitudes As DataSet
        dsTiposSolicitudes = oSolicitudes.Obtener_TiposSolicitudes(IdClasificacion, txtClasificacion)

        For Each oRow As DataRow In dsTiposSolicitudes.Tables(0).Rows
            If oRow("COD") Is System.DBNull.Value AndAlso oRow("DEN") Is System.DBNull.Value AndAlso dsTiposSolicitudes.Tables(0).Rows.Count = 1 Then
                sResultado = sResultado & "{""Valor"":"""",""Texto"":""""},"
            Else
                If Not (oRow("COD") Is System.DBNull.Value AndAlso oRow("DEN") Is System.DBNull.Value) Then
                    sResultado = sResultado & "{""Valor"":""" & HttpUtility.JavaScriptStringEncode(oRow("COD"))
                End If
                sResultado = sResultado & """,""Texto"":""" & HttpUtility.JavaScriptStringEncode(oRow("COD")) & " - " & HttpUtility.JavaScriptStringEncode(oRow("DEN" & "_" & CStr(CN_Usuario.Idioma)))
                sResultado = sResultado & """},"
            End If
        Next

        If Len(sResultado) > 0 Then
            sResultado = Left(sResultado, Len(sResultado) - 1)
        End If
        Return sResultado
    End Function
End Class