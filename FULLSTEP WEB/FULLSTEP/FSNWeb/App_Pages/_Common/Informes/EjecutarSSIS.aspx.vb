﻿Imports Microsoft.SqlServer.Dts.Runtime

Partial Public Class EjecutarSSIS
    Inherits FSNPage

    ''' <summary>
    ''' Evento que se ejecuta cada vez que se carga la página, en este caso llama al paquete SSIS, le pasamos la contraseña encriptada en el web.config y introducimos valores para las variables del paquete, para despues ejecutarlo.
    ''' </summary>
    ''' <param name="sender">El objeto que genera el evento, en este caso la propia página</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: La propia página, cada vez que se carga
    ''' Tiempo máximo: Depende del paquete SSIS, pueden ser varios segundos.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim pck As New Package
        Dim app As New Application
        Dim pckResult As New DTSExecResult
        Dim PckLocation As String
        Dim tipoDescarga As String = ""
        Dim sPath As String = Nothing
        If CType(Cache("ObjetoXML" & FSNUser.CodPersona), FSNServer.cXMLs) Is Nothing Then
            Div1.Visible = True
            lblcargando.Visible = False
            imgCarga0.Visible = False
            Exit Sub
        End If
        Dim dirrpt As String = Trim(ConfigurationManager.AppSettings("rptFilesPath"))
        If Right(dirrpt, 1) = "/" Or Right(dirrpt, 1) = "\" Then dirrpt = Left(dirrpt, dirrpt.Length - 1)
        Dim dirtemp As String = Trim(ConfigurationManager.AppSettings("temp"))
        If Right(dirtemp, 1) = "/" Or Right(dirtemp, 1) = "\" Then dirtemp = Left(dirtemp, dirtemp.Length - 1)
        PckLocation = dirrpt & "\" & CType(Cache("ObjetoXML" & FSNUser.CodPersona), FSNServer.cXMLs).PathPaqueteSSIS

        If Not String.IsNullOrEmpty(Request.QueryString("desdeGS")) Then
            app.UpdatePackage = False
        End If

        pck = app.LoadPackage(PckLocation, Nothing)

        Dim oXMLs As FSNServer.cXMLs = FSNServer.Get_Object(GetType(FSNServer.cXMLs))
        oXMLs = Cache("ObjetoXML" & FSNUser.CodPersona)
        Dim ValoresParametros As String()
        ValoresParametros = Cache("CadenaParametros" & FSNUser.CodPersona)
        Dim ContadorParametros As Integer = 0
        '        Dim ContadorParametrosConfig As Integer = 0  ' Contador de parámetros internos de configuración
        For Each parametro As FSNServer.cXML In oXMLs
            If parametro.TipoParametro = Fullstep.FSNServer.cXML.TipoDeParametro.ParametroForm Then
                Select Case parametro.TipoDato
                    Case Fullstep.FSNServer.cXML.TipoDeDato.ArbolCategorias
                        If ValoresParametros(ContadorParametros) = "NULL" Then
                            pck.Variables("CAT1").Value = 0
                        Else
                            pck.Variables("CAT1").Value = CInt(ValoresParametros(ContadorParametros))
                        End If
                        ContadorParametros = ContadorParametros + 1
                        If ValoresParametros(ContadorParametros) = "NULL" Then
                            pck.Variables("CAT2").Value = 0
                        Else
                            pck.Variables("CAT2").Value = CInt(ValoresParametros(ContadorParametros))
                        End If
                        ContadorParametros = ContadorParametros + 1
                        If ValoresParametros(ContadorParametros) = "NULL" Then
                            pck.Variables("CAT3").Value = 0
                        Else
                            pck.Variables("CAT3").Value = CInt(ValoresParametros(ContadorParametros))
                        End If
                        ContadorParametros = ContadorParametros + 1
                        If ValoresParametros(ContadorParametros) = "NULL" Then
                            pck.Variables("CAT4").Value = 0
                        Else
                            pck.Variables("CAT4").Value = CInt(ValoresParametros(ContadorParametros))
                        End If
                        ContadorParametros = ContadorParametros + 1
                        If ValoresParametros(ContadorParametros) = "NULL" Then
                            pck.Variables("CAT5").Value = 0
                        Else
                            pck.Variables("CAT5").Value = CInt(ValoresParametros(ContadorParametros))
                        End If
                    Case Fullstep.FSNServer.cXML.TipoDeDato.ArticulosCatalogo
                        pck.Variables("CODART").Value = ValoresParametros(ContadorParametros)
                    Case Fullstep.FSNServer.cXML.TipoDeDato.Booleano
                        pck.Variables(parametro.NombreParametro.ToUpper).Value = CByte(ValoresParametros(ContadorParametros))
                    Case Fullstep.FSNServer.cXML.TipoDeDato.Entero
                        If parametro.NombreParametro.ToUpper = "ESTPEDIDO" Then
                            If ValoresParametros(ContadorParametros) = "NULL" Then
                                pck.Variables(parametro.NombreParametro.ToUpper).Value = 9999
                            Else
                                pck.Variables(parametro.NombreParametro.ToUpper).Value = CInt(ValoresParametros(ContadorParametros))
                            End If
                        Else
                            If ValoresParametros(ContadorParametros) = "NULL" Then
                                pck.Variables(parametro.NombreParametro.ToUpper).Value = 0
                            Else
                                pck.Variables(parametro.NombreParametro.ToUpper).Value = CInt(ValoresParametros(ContadorParametros))
                            End If
                        End If
                    Case Fullstep.FSNServer.cXML.TipoDeDato.EstructuraMateriales
                        pck.Variables("GMN1").Value = ValoresParametros(ContadorParametros)
                        ContadorParametros = ContadorParametros + 1
                        pck.Variables("GMN2").Value = ValoresParametros(ContadorParametros)
                        ContadorParametros = ContadorParametros + 1
                        pck.Variables("GMN3").Value = ValoresParametros(ContadorParametros)
                        ContadorParametros = ContadorParametros + 1
                        pck.Variables("GMN4").Value = ValoresParametros(ContadorParametros)
                    Case Fullstep.FSNServer.cXML.TipoDeDato.Fecha
                        pck.Variables(parametro.NombreParametro.ToUpper).Value = CStr(Trim(Left(ValoresParametros(ContadorParametros), 10)))
                    Case Fullstep.FSNServer.cXML.TipoDeDato.Real
                        If ValoresParametros(ContadorParametros) = "NULL" Then
                            pck.Variables(parametro.NombreParametro.ToUpper).Value = 0
                        Else
                            pck.Variables(parametro.NombreParametro.ToUpper).Value = CDbl(ValoresParametros(ContadorParametros))
                        End If
                    Case Fullstep.FSNServer.cXML.TipoDeDato.Texto
                        pck.Variables(parametro.NombreParametro.ToUpper).Value = ValoresParametros(ContadorParametros)
                        'Ubicación compresor WinZip
                    Case Fullstep.FSNServer.cXML.TipoDeDato.PathWinzip
                        pck.Variables(parametro.NombreParametro.ToUpper).Value = CStr(ValoresParametros(ContadorParametros))
                End Select
                ContadorParametros = ContadorParametros + 1
            Else
                Select Case parametro.NombreParametroConfig
                    Case "PATHWINZIP"
                        pck.Variables(parametro.NombreParametroConfig.ToUpper).Value = CStr(parametro.ValorParametroConfig)
                    Case "TIPODESCARGA"
                        tipoDescarga = CStr(parametro.ValorParametroConfig)
                End Select
            End If
        Next
        pck.Variables("PATHARCHIVOORIGINAL").Value = dirrpt & "\" & oXMLs.RPTName

        Dim stempfile As String
        If tipoDescarga <> "ZIP" Then
            stempfile = dirtemp & "\" & FSNUser.CodPersona & "_" & Day(Date.Today) & Month(Date.Today) & Year(Date.Today) & "_" & oXMLs.RPTName
            If IO.File.Exists(stempfile) Then
                Dim i As Integer = 1
                stempfile = dirtemp & "\" & FSNUser.CodPersona & "_" & Day(Date.Today) & Month(Date.Today) & Year(Date.Today) & i & "_" & oXMLs.RPTName
                Do While IO.File.Exists(stempfile)
                    i = i + 1
                    stempfile = dirtemp & "\" & FSNUser.CodPersona & "_" & Day(Date.Today) & Month(Date.Today) & Year(Date.Today) & i & "_" & oXMLs.RPTName
                Loop
            End If
            pck.Variables("PATHARCHIVOCOPIAR").Value = stempfile
        Else
            stempfile = FSNUser.CodPersona & "_" & Day(Date.Today) & Month(Date.Today) & Year(Date.Today) & "_" & oXMLs.RPTName
            If IO.File.Exists(stempfile) Then
                Dim i As Integer = 1
                stempfile = FSNUser.CodPersona & "_" & Day(Date.Today) & Month(Date.Today) & Year(Date.Today) & i & "_" & oXMLs.RPTName
                Do While IO.File.Exists(stempfile)
                    i = i + 1
                    stempfile = FSNUser.CodPersona & "_" & Day(Date.Today) & Month(Date.Today) & Year(Date.Today) & i & "_" & oXMLs.RPTName
                Loop
            End If

            Dim sCarpeta As String

            sCarpeta = modUtilidades.GenerateRandomPath()
            sPath = dirtemp & "\" & sCarpeta
            If Not IO.Directory.Exists(sPath) Then
                IO.Directory.CreateDirectory(sPath)
            End If
            pck.Variables("PATHARCHIVOCOPIAR").Value = sPath & "\" & stempfile
        End If
        pck.OfflineMode = False
        'Dim ConnectionStringExcel As String
        'ConnectionStringExcel = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & pck.Variables("PATHARCHIVOCOPIAR").Value & ";Extended Properties=" & """EXCEL 8.0;HDR=YES""" & ";"
        If pck.Connections.Contains("ConexionAExcel") Then _
            pck.Connections("ConexionAExcel").ConnectionString = oXMLs.DevolverPropiedadesconexionArchivoExcel(pck.Variables("PATHARCHIVOCOPIAR").Value)
        Dim ConnectionStringBBDD As String
        ConnectionStringBBDD = oXMLs.DevolverPropiedadesConexionAPaqueteSSIS
        'pck.Connections("ConexionABBDD").ConnectionString = pck.Connections("ConexionABBDD").ConnectionString & "password=10152025;"
        If pck.Connections.Contains("ConexionABBDD") Then _
            pck.Connections("ConexionABBDD").ConnectionString = ConnectionStringBBDD
        'If pck.Connections.Contains("ConexionFicheroPlano") Then _
        '    pck.Connections("ConexionFicheroPlano").ConnectionString = dirtemp & "\" & Replace(oXMLs.PathPaqueteSSIS, ".dtsx", ".txt", 1, 1, CompareMethod.Text)
        'pck.EnableConfigurations = False
        pckResult = pck.Execute
        If pckResult = DTSExecResult.Success Then
            Dim Archivo As Byte()
            Select Case tipoDescarga
                Case "ZIP"
                    'Eliminar archivo Excel origen
                    If IO.File.Exists(pck.Variables("PATHARCHIVOCOPIAR").Value) Then IO.File.Delete(pck.Variables("PATHARCHIVOCOPIAR").Value)

                    pck.Variables("PATHARCHIVOCOPIAR").Value = pck.Variables("PATHARCHIVOCOPIAR").Value.ToString.Replace(System.IO.Path.GetExtension(pck.Variables("PATHARCHIVOCOPIAR").Value.ToString), ".zip")
                    Comprimir(sPath, "*" & System.IO.Path.GetExtension(pck.Variables("PATHARCHIVOCOPIAR").Value.ToString), pck.Variables("PATHARCHIVOCOPIAR").Value, False, False)
                    If IO.File.Exists(pck.Variables("PATHARCHIVOCOPIAR").Value) Then
                        Response.ClearHeaders()
                        Response.ClearContent()
                        Archivo = IO.File.ReadAllBytes(pck.Variables("PATHARCHIVOCOPIAR").Value)
                        Dim NombreArchivo As String()
                        NombreArchivo = Split(pck.Variables("PATHARCHIVOCOPIAR").Value, "\", , CompareMethod.Text)
                        Response.Clear()
                        Response.ContentType = "application/octet-stream"
                        Response.AddHeader("content-disposition", "attachment; filename=""" & NombreArchivo(NombreArchivo.Count - 1) & """")
                        Response.BinaryWrite(Archivo)
                        'Borrado de los ficheros del directorio y del propio directorio
                        IO.Directory.Delete(sPath, True)
                        Response.End()
                    Else
                        Dim ex As New FSNException("Error en informe SSIS: No existe el fichero ZIP")
                        ex.Number = FSNException.TipoError.NoExisteArchivo
                        Throw ex
                    End If
                Case Else
                    If IO.File.Exists(pck.Variables("PATHARCHIVOCOPIAR").Value) Then
                        Response.ClearHeaders()
                        Response.ClearContent()
                        Archivo = IO.File.ReadAllBytes(pck.Variables("PATHARCHIVOCOPIAR").Value)
                        Dim NombreArchivo As String()
                        NombreArchivo = Split(pck.Variables("PATHARCHIVOCOPIAR").Value, "\", , CompareMethod.Text)
                        Response.Clear()
                        Response.ContentType = "application/octet-stream"
                        Response.AddHeader("content-disposition", "attachment; filename=""" & NombreArchivo(NombreArchivo.Count - 1) & """")
                        Response.BinaryWrite(Archivo)
                        IO.File.Delete(pck.Variables("PATHARCHIVOCOPIAR").Value)
                        Response.End()
                    Else
                        Dim ex As New FSNException("Error en informe SSIS: No existe el fichero Excel")
                        ex.Number = FSNException.TipoError.NoExisteArchivo
                        Throw ex
                    End If
            End Select
        Else
            Dim errMsg As String = String.Empty
            For Each dtserr As DtsError In pck.Errors
                errMsg = errMsg & "Source: " & dtserr.Source &
                    ". SubComponent: " & IIf(String.IsNullOrEmpty(dtserr.SubComponent), String.Empty, dtserr.SubComponent) &
                    ". Description:" & dtserr.Description & vbCrLf
            Next
            Dim ex As New FSNException(errMsg)
            ex.Number = FSNException.TipoError.ErrorEnPaqueteDTS
            Throw ex
        End If

        pck.Dispose()
        pck = Nothing
        app = Nothing
        pckResult = Nothing
    End Sub
    Protected Sub BtnDescarga_OnClick(ByVal sender As Object, ByVal e As EventArgs) Handles BtnDescarga.Click
        Dim PAth As String
        PAth = Session("PathArchivo")
        Dim Archivo As Byte()
        Session("PathArchivo") = PAth
        Archivo = IO.File.ReadAllBytes(PAth)
        Dim NombreArchivo As String()
        NombreArchivo = Split(PAth, "\", , CompareMethod.Text)
        Response.ContentType = "application/octet-stream"
        Response.AddHeader("content-disposition", "attachment; filename=""" & NombreArchivo(NombreArchivo.Count - 1) & """")
        Response.BinaryWrite(Archivo)
        IO.File.Delete(PAth)
        Response.End()
    End Sub
End Class