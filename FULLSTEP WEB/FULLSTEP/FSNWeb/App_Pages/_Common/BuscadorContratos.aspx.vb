﻿Imports Infragistics.Web.UI
Imports Infragistics.Web.UI.GridControls.WebDataGrid

Partial Public Class BuscadorContratos
    Inherits FSNPage
#Region "Propertys"
    ''' <summary>
    ''' Obtener el nombre con el q se ha guardado en cache el dataset de Solicitudes
    ''' </summary>
    ''' <returns>Nombre con el q se ha guardado en cache el dataset de Solicitudes</returns>
    ''' <remarks>Llamada desde: Page_Load    wdgSolicitudes_InitializeDataSource          BuscarSolicitudes
    ''' ; Tiempo maximo:0</remarks>
    Private ReadOnly Property DataSourceCacheContratos() As String
        Get
            Return "Contratos_" + Usuario.Cod.ToString()
        End Get
    End Property
    ''' <summary>
    ''' Obtener el nombre con el q se ha guardado en cache el dataset de Lineas
    ''' </summary>
    ''' <value></value>
    ''' <returns>Nombre con el q se ha guardado en cache el dataset de Lineas</returns>
    ''' <remarks>Llamada desde: Page_Load    wdgLineas_InitializeDataSource         wdgLineas_InitializeLayout      wdgLineas_InitializeRow
    ''' BuscarLineas ; Tiempo maximo:0</remarks>
    Private ReadOnly Property DataSourceCacheLineas() As String
        Get
            Return "Lineas_" + Usuario.Cod.ToString()
        End Get
    End Property
#End Region
#Region "Carga Inicial"

    Private Enum enumTextoBuscador
        Busqueda = 0
        Cancelar
        Estado
        Codigo
        Tipo
        Peticionario
        Empresa
        Buscar
        Aceptar
        Articulo
        TipoFiltro
        Estado_Cualquier
        Estado_Guardado
        Estado_EnCurso
        Estado_Vigente
        Estado_ProximoExpirar
        Estado_Expirado
        Estado_Rechazado
        Estado_Anulado
        FechaAlta
        FechaInicio
        FechaExpiracion
        ProveedorCodigo
        ProveedorDen
        MsgSeleccionarContrato
        Proveedor
    End Enum





    ''' <summary>
    ''' Cargar la pagina. 
    ''' Si se proporciona "DatosLinea": Sirve para ver el detalle de la Instancia/Linea origen de la vinculación
    ''' Eoc: Sirve para Buscar y devolver Instancias (vinculación a nivel de solicitud) ó Lineas (vinculación a nivel de desglose origen)
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>    
    ''' <remarks>Llamada desde:sistema; Tiempo máximo:0</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'CAMBIAR
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.BuscadorContratos

        btnAceptar1.Attributes.Add("onclick", "Importar();")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Solicitud", "<script>var lSolicitud = '" & Request.QueryString("Solicitud") & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "sMensaje", "<script>var sMensajeSolicitud = '" & JSText(Textos(enumTextoBuscador.MsgSeleccionarContrato)) & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "frames.aspx?pagina='</script>")

        imgProveedorLupa.OnClientClick = "var newWindow = window.open('" & ConfigurationManager.AppSettings("RutaFS") & "_common/BuscadorProveedores.aspx?desdeGS=&SessionID=" & Request.QueryString("SessionID") & "&PM=true&Contr=true', '_blank', 'width=835,height=635,status=yes,resizable=no,top=0,left=150,scrollbars=yes');return false;"
        imgEmpresaLupa.Attributes.Add("onClick", "javascript:var newWindow = window.open('" & ConfigurationManager.AppSettings("rutaFS") & "_common/BuscadorEmpresas.aspx','_blank','width=800,height=600, location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =no');newWindow.focus();return false;")
        txtEmpresa.Attributes.Add("onkeydown", "javascript:return eliminarEmpresa(event)")
        txtProveedor.Attributes.Add("onkeydown", "javascript:return eliminarProveedor(event)")

        If Not IsPostBack Then
            HttpContext.Current.Cache.Remove(DataSourceCacheContratos)
            HttpContext.Current.Cache.Remove(DataSourceCacheLineas)

            FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/seguimiento.gif"

            CargarTextos()
            CargarGridSolicitudes()

            CargarEstados()
            Dim bSoloSolic As Boolean = CargarTipos()

            InicializacionControles()

        End If

        If (Request("DatosLinea") Is Nothing) Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalProgress", "<script>var ModalProgress = '" & ModalProgress.ClientID & "';</script>")
        End If

        actualizargrid()

    End Sub
    ''' <summary>
    ''' Establece los textos de los controles de acuerdo con el idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load; Tiempo máximo:0</remarks> 
    Private Sub CargarTextos()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.BuscadorContratos

        FSNPageHeader.TituloCabecera = Textos(enumTextoBuscador.Busqueda)
        btnCancelar.Text = Textos(enumTextoBuscador.Cancelar)

        lblEstado.Text = String.Format("{0}:", Textos(enumTextoBuscador.Estado))
        lblCodContrato.Text = String.Format("{0}:", Textos(enumTextoBuscador.Codigo))
        lblTipoSolicitud.Text = String.Format("{0}:", Textos(enumTextoBuscador.Tipo))
        lblFechaDesde.Text = String.Format("{0}:", Textos(enumTextoBuscador.FechaInicio))
        lblFechaHasta.Text = String.Format("{0}:", Textos(enumTextoBuscador.FechaExpiracion))
        lblArticulo.Text = String.Format("{0}:", Textos(enumTextoBuscador.Articulo))
        lblPeticionario.Text = String.Format("{0}:", Textos(enumTextoBuscador.Peticionario))
        lblEmpresa.Text = String.Format("{0}:", Textos(enumTextoBuscador.Empresa))
        lblProveedor.Text = String.Format("{0}:", Textos(enumTextoBuscador.Proveedor))

        btnBuscar.Text = Textos(enumTextoBuscador.Buscar)
        lblFiltrarPor.Text = String.Format("{0}:", Textos(enumTextoBuscador.TipoFiltro))
        btnAceptar1.Text = Textos(enumTextoBuscador.Aceptar)
    End Sub
    ''' <summary>
    ''' Inicializa los componentes y funciones javascript de los elementos
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load; Tiempo máximo:0</remarks> 
    Private Sub InicializacionControles()
        optFiltrar1.Checked = True
        ''imgPeticionarioLupa.Attributes.Add("onClick", "javascript:var newWindow = window.open('" & ConfigurationManager.AppSettings("rutaFS") & "_Common/BuscadorUsuarios.aspx?&idControl=" & txtPeticionario.ID & "', '_blank', 'width=850,height=630,status=yes,resizable=no,top=200,left=200');newWindow.focus();")
        dteFechaDesde.NullText = String.Empty
        dteFechaDesde.Value = Today.AddYears(-1)
        dteFechaHasta.NullText = String.Empty

        lblDenPeticionario.Visible = True
        lblDenPeticionario.Text = FSNUser.Nombre
        hfPeticionario.Value = FSNUser.CodPersona

        If FSNServer.TipoAcceso.gbAccesoLCX AndAlso Not Trim(LCase(FSNUser.PerfilLCX)).Equals(Trim(LCase("auditor"))) AndAlso Not Trim(LCase(FSNUser.PerfilLCX)).Equals(Trim(LCase("compras"))) Then
            Dim oEmpresa As FSNServer.Empresa
            oEmpresa = FSNServer.Get_Object(GetType(FSNServer.Empresa))
            oEmpresa.ID = FSNUser.EmpresaLCX
            oEmpresa.Load(FSNUser.Idioma)
            lblEmpresaLCX.Text = oEmpresa.Den
            PanelTableEmpresa.Visible = False
            imgEmpresaLupa.Visible = False
            hidEmpresa.Value = oEmpresa.ID ' añadido para el detalle
        Else 'Empresas  Como AccesoLCX es false cargo empresa como antiguamente
            lblEmpresaLCX.Visible = False
        End If

    End Sub
    ''' <summary>
    ''' Carga la lista de estados de proceso
    ''' </summary>
    ''' <remarks>Llamada desde=Page_Load; Tiempo máximo=0seg.</remarks>
    Private Sub CargarEstados()
        '(ninguno)

        AñadirElementoEstado(-1, Textos(enumTextoBuscador.Estado_Cualquier))
        AñadirElementoEstado(TiposDeDatos.EstadosVisorContratos.Guardado, Me.Textos(enumTextoBuscador.Estado_Guardado))
        AñadirElementoEstado(TiposDeDatos.EstadosVisorContratos.En_Curso_De_Aprobacion, Me.Textos(enumTextoBuscador.Estado_EnCurso))
        AñadirElementoEstado(TiposDeDatos.EstadosVisorContratos.Vigentes, Me.Textos(enumTextoBuscador.Estado_Vigente))
        AñadirElementoEstado(TiposDeDatos.EstadosVisorContratos.Proximo_a_Expirar, Me.Textos(enumTextoBuscador.Estado_ProximoExpirar))
        AñadirElementoEstado(TiposDeDatos.EstadosVisorContratos.Expirados, Me.Textos(enumTextoBuscador.Estado_Expirado))
        AñadirElementoEstado(TiposDeDatos.EstadosVisorContratos.Rechazados, Me.Textos(enumTextoBuscador.Estado_Rechazado))
        AñadirElementoEstado(TiposDeDatos.EstadosVisorContratos.Anulados, Me.Textos(enumTextoBuscador.Estado_Anulado))

        wddEstado.CurrentValue = Textos(enumTextoBuscador.Estado_Cualquier)
        wddEstado.SelectedValue = -1
    End Sub
    ''' <summary>
    ''' Carga un elemento de la lista de estados de proceso
    ''' </summary>
    ''' <param name="value">Id del elemento</param>
    ''' <param name="text">Texto del elemento</param>
    ''' <remarks>Llamada desde=CargarEstados; Tiempo máximo=0seg.</remarks>
    Private Sub AñadirElementoEstado(ByVal value As Nullable(Of Integer), ByVal text As String)
        Dim oItem As New ListControls.DropDownItem
        oItem.Value = value
        oItem.Text = text
        wddEstado.Items.Add(oItem)
    End Sub
    ''' <summary>
    ''' Carga el combo de Tipos de solictudes e indica si hay alguna vinculación a nivel de desglose origen
    ''' </summary>
    ''' <returns>Si hay alguna vinculación a nivel de desglose origen</returns>
    ''' <remarks>Llamada desde:Page_Load; Tiempo máximo:0</remarks> 
    Private Function CargarTipos() As Boolean
        Dim idCampo As Long
        If IsNumeric(Request.QueryString("IdCampo")) Then idCampo = CInt(Request.QueryString("IdCampo"))

        Dim lInstancia As Long
        If IsNumeric(Request.QueryString("Instancia")) Then lInstancia = CInt(Request.QueryString("Instancia"))

        Dim s As FSNServer.Solicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
        s.ID = Request("Solicitud")
            s.Load(Me.Idioma)

        lblDenTipoSolicitud.Text = s.Den(Me.Idioma)
        hfTipoSolicitud.Value = s.ID

        Return True

    End Function
#End Region
#Region "wdgSolicitudes"
    Private Sub actualizargrid()
        wdgSolicitudes.Rows.Clear()
        wdgSolicitudes.Columns.Clear()
        wdgSolicitudes.DataSource = BuscarSolicitudes()
        CrearCol_Solicitudes()
        wdgSolicitudes.DataBind()
        Conf_GridSolicitudes()
    End Sub
    ''' <summary>
    ''' Carga la grid con los datos
    ''' </summary>
    ''' <remarks>Llamada desde=sistema;Tiempo máximo=0,3seg.</remarks>
    Private Sub CargarGridSolicitudes()
        wdgSolicitudes.Rows.Clear()
        wdgSolicitudes.Columns.Clear()

        Dim dataSource As DataTable
        If Not CType(HttpContext.Current.Cache.Item(DataSourceCacheContratos), DataTable) Is Nothing Then
            dataSource = CType(HttpContext.Current.Cache.Item(DataSourceCacheContratos), DataTable)
        Else
            dataSource = SolicitudesLayout()
        End If
        wdgSolicitudes.DataSource = dataSource
        CrearCol_Solicitudes()
        wdgSolicitudes.DataBind()
        Conf_GridSolicitudes()
    End Sub

    Private Sub CrearCol_Solicitudes()
        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
        Dim nombre As String

        For i As Integer = 0 To wdgSolicitudes.DataSource.Columns.Count - 1
            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            nombre = wdgSolicitudes.DataSource.Columns(i).ToString
            With campoGrid
                .Key = nombre
                .DataFieldName = nombre
                .Header.Text = nombre
            End With
            wdgSolicitudes.Columns.Add(campoGrid)
        Next
    End Sub
    Private Sub AnyadirColumnaU(ByVal fieldname As String, ByVal grid As GridControls.WebDataGrid)
        Dim unboundField As New Infragistics.Web.UI.GridControls.UnboundField(True)
        unboundField.Key = fieldname
        grid.Columns.Add(unboundField)
    End Sub
    ''' <summary>
    ''' Se ejecuta una vez, al cargarse la grid
    ''' </summary>
    ''' <remarks>Llamada desde: Al cargarse la grid; Tiempo máximo: 0 sg.</remarks>
    Private Sub Conf_GridSolicitudes()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.BuscadorContratos

        wdgSolicitudes.Height = Unit.Pixel(240)

        wdgSolicitudes.Columns("CODIGO").Header.Text = Textos(enumTextoBuscador.Codigo)
        wdgSolicitudes.Columns("FECHA_ALTA").Header.Text = Textos(enumTextoBuscador.FechaAlta)
        wdgSolicitudes.Columns("FECHA_INICIO").Header.Text = Textos(enumTextoBuscador.FechaInicio)
        wdgSolicitudes.Columns("FECHA_EXP").Header.Text = Textos(enumTextoBuscador.FechaExpiracion)
        wdgSolicitudes.Columns("CODPRO").Header.Text = Textos(enumTextoBuscador.ProveedorCodigo)
        wdgSolicitudes.Columns("PROVEEDOR").Header.Text = Textos(enumTextoBuscador.ProveedorDen)
        wdgSolicitudes.Columns("EMPRESA").Header.Text = Textos(enumTextoBuscador.Empresa)
        wdgSolicitudes.Columns("EST_DEN").Header.Text = Textos(enumTextoBuscador.Estado)
        wdgSolicitudes.Columns("PETICIONARIO").Header.Text = Textos(enumTextoBuscador.Peticionario)
        wdgSolicitudes.Columns("TIPO_CONTRATO").Header.Text = Textos(enumTextoBuscador.Tipo)

        wdgSolicitudes.Columns("CODIGO").Width = Unit.Pixel(100)

        wdgSolicitudes.Columns("FECHA_ALTA").FormatValue(FSNUser.DateFormat.ShortDatePattern)
        wdgSolicitudes.Columns("FECHA_INICIO").FormatValue(FSNUser.DateFormat.ShortDatePattern)
        wdgSolicitudes.Columns("FECHA_EXP").FormatValue(FSNUser.DateFormat.ShortDatePattern)

        wdgSolicitudes.Columns("CODESTADO").Hidden = True
        wdgSolicitudes.Columns("ID_CONTRATO").Hidden = True
        wdgSolicitudes.Columns("CODEMP").Hidden = True
        wdgSolicitudes.Columns("CODPET").Hidden = True
        wdgSolicitudes.Columns("ID").Hidden = True
    End Sub
    '''' <summary>
    '''' Se ejecuta cada vez que se inicializa una fila del grid
    '''' </summary>
    '''' <param name="sender">del evento</param>
    '''' <param name="e">del evento</param>     
    '''' <remarks>LLamada desde: Al cargar la grid con datos; Tiempo máximo: 0 sg</remarks>
    'Private Sub wdgSolicitudes_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles wdgSolicitudes.InitializeRow
    '    'e.Row.Items.FindItemByKey("IMPORTE").Text = modUtilidades.FormatNumber(e.Row.Items.FindItemByKey("IMPORTE").Value, FSNUser.NumberFormat)
    'End Sub
    ''' <summary>
    ''' Inicializa el dataset de Solicitudes
    ''' </summary>
    ''' <returns>dataset de Solicitude</returns>
    ''' <remarks>Llamada desde: wdgSolicitudes_InitializeDataSource; Tiempo maximo: 0</remarks>
    Private Function SolicitudesLayout() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("ID", GetType(Integer))
        dt.Columns.Add("ID_CONTRATO", GetType(Integer))
        dt.Columns.Add("CODIGO", GetType(String))
        dt.Columns.Add("FECHA_ALTA", GetType(Date))
        dt.Columns.Add("FECHA_INICIO", GetType(Date))
        dt.Columns.Add("FECHA_EXP", GetType(Date))
        dt.Columns.Add("CODPRO", GetType(Double))
        dt.Columns.Add("PROVEEDOR", GetType(String))
        dt.Columns.Add("CODEMP", GetType(String))
        dt.Columns.Add("EMPRESA", GetType(Integer))
        dt.Columns.Add("EST_DEN", GetType(String))
        dt.Columns.Add("CODPET", GetType(Integer))
        dt.Columns.Add("PETICIONARIO", GetType(String))
        dt.Columns.Add("TIPO_CONTRATO", GetType(Integer))
        dt.Columns.Add("CODESTADO", GetType(Integer))
        Return dt
    End Function
    ''' <summary>
    ''' Esta función carga el datatable de solicitudes con las instancias de la solicitud/es origen. Luego lo mete en cache.
    ''' </summary>
    ''' <returns>datatable de solicitudes</returns>
    ''' <remarks>Llamada desde:btnBuscar_Click; Tiempo maximo:0,3</remarks>
    Private Function BuscarSolicitudes() As DataTable


        Dim ltipo As Long
        ltipo = hfTipoSolicitud.Value
        Dim speticionario As String = hfPeticionario.Value

        Dim lempresa As Nullable(Of Long)
        Dim sproveedor As String = String.Empty
        Dim sCodigo As String = String.Empty
        Dim sarticulo As String = String.Empty
        Dim iestado As Nullable(Of Integer)

        If Not IsNothing(hidEmpresa.Value) AndAlso hidEmpresa.Value.Trim.Length > 0 Then lempresa = hidEmpresa.Value
        If Not IsNothing(hidProveedor.Value) AndAlso hidProveedor.Value.Trim.Length > 0 Then sproveedor = hidProveedor.Value.Trim
        If Not IsNothing(txtArticulo.Text) AndAlso txtArticulo.Text.Trim.Length > 0 Then sarticulo = txtArticulo.Text.Trim
        If Not IsNothing(txtCodContrato.Text) AndAlso txtCodContrato.Text.Trim.Length > 0 Then sCodigo = txtCodContrato.Text.Trim

        If IsNumeric(wddEstado.SelectedValue) AndAlso wddEstado.SelectedValue <> -1 Then
            iestado = CInt(wddEstado.SelectedValue)
        End If
        Dim dfechaDesde As Nullable(Of Date)
        If dteFechaDesde.Value = New Date Then
            dfechaDesde = Nothing
        Else
            dfechaDesde = dteFechaDesde.Value
        End If
        Dim dfechaHasta As Nullable(Of Date)
        If dteFechaHasta.Value = New Date Then
            dfechaHasta = Nothing
        Else
            dfechaHasta = dteFechaHasta.Value
        End If

        Dim C As FSNServer.FiltroContrato = FSNServer.Get_Object(GetType(FSNServer.FiltroContrato))
        Dim result As DataTable = C.GetContratosBuscador(Usuario.Idioma, sCodigo, speticionario,
                                                         ltipo, sarticulo, sproveedor, lempresa, iestado, dfechaDesde, dfechaHasta)

        result = InsertarColumnaTextoEstado(result)
        InsertarEnCache(DataSourceCacheContratos, result)

        Return result
    End Function
    ''' <summary>
    ''' Actualiza en el datatable de solicitudes la denominación del estado
    ''' </summary>
    ''' <param name="dt">datatable de solicitudes</param>
    ''' <returns>datatable de solicitudes con la denominación del estado actualizada</returns>
    ''' <remarks>Llamada desde:BuscarSolicitudes; Tiempo maximo:0</remarks>
    Private Function InsertarColumnaTextoEstado(ByVal dt As DataTable) As DataTable
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.BuscadorContratos
        Dim columnName As String = "EST_DEN"
        If Not dt Is Nothing Then
            dt.Columns.Add(columnName, GetType(String))
            For Each row As DataRow In dt.Rows
                Select Case row("CODESTADO")
                    Case TiposDeDatos.EstadosVisorContratos.Guardado
                        row(columnName) = Textos(enumTextoBuscador.Estado_Guardado)
                    Case TiposDeDatos.EstadosVisorContratos.En_Curso_De_Aprobacion
                        row(columnName) = Textos(enumTextoBuscador.Estado_EnCurso)
                    Case TiposDeDatos.EstadosVisorContratos.Vigentes
                        row(columnName) = Textos(enumTextoBuscador.Estado_Vigente)
                    Case TiposDeDatos.EstadosVisorContratos.Proximo_a_Expirar
                        row(columnName) = Textos(enumTextoBuscador.Estado_ProximoExpirar)
                    Case TiposDeDatos.EstadosVisorContratos.Expirados
                        row(columnName) = Textos(enumTextoBuscador.Estado_Expirado)
                    Case TiposDeDatos.EstadosVisorContratos.Rechazados
                        row(columnName) = Textos(enumTextoBuscador.Estado_Rechazado)
                    Case TiposDeDatos.EstadosVisorContratos.Anulados
                        row(columnName) = Textos(enumTextoBuscador.Estado_Anulado)
                    Case Else
                        row(columnName) = String.Empty
                End Select
            Next
        End If
        Return dt
    End Function
#End Region
#Region "Botonera"
    ''' <summary>
    ''' Carga el grid de solicitudes con las instancias de la solicitud/es origen
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>  
    ''' <remarks>Llamada desde:sistema; Tiempo máximo:0,3</remarks>
    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        actualizargrid()
    End Sub

#End Region
End Class