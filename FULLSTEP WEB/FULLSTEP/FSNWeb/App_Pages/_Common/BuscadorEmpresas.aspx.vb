﻿Partial Public Class BuscadorEmpresas
    Inherits FSNPage

    ''' <summary>
    ''' Cargar la pagina. En el postback usa los filtros indicados para buscar empresas.
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>        
    ''' <remarks>Llamada desde:sistema; Tiempo máximo:0</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CargarComboPaises()
        End If
        CargarIdiomas()
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalProgress", "<script>var ModalProgress = '" & ModalProgress.ClientID & "' </script>")
    End Sub


    ''' <summary>
    ''' Introduce el texto
    ''' </summary>
    ''' <remarks>Llamada desde:=Page_load</remarks>
    Private Sub CargarIdiomas()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.BusquedaEmpresas
        BusquedaEmpresas.Textos = TextosModuloCompleto
    End Sub

    ''' <summary>
    ''' Pone el texto correspondiente en la etiqueta lblProcesando.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: El prerender de la etiqueta. Tiempo máximo: 0sg.</remarks>
    Protected Sub LblProcesando_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, Label).Text = Textos(46)
    End Sub


    ''' <summary>
    ''' Proceso que carga el combo de Tipo con los tipos de las solicitudes..
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,1seg.</remarks>
    Private Sub CargarComboPaises()
        Dim oPaises As FSNServer.Paises
        oPaises = FSNServer.Get_Object(GetType(FSNServer.Paises))

        oPaises.LoadData(Idioma)

        BusquedaEmpresas.Paises = oPaises.Data.Tables(0)

        oPaises = Nothing
    End Sub

    ''' <summary>
    ''' Carga la propiedad del usercontrol con las provincias de un determinado pais
    ''' </summary>
    ''' <param name="sCodPais">Codigo del pais</param>
    ''' <remarks>Llamada desde=wddSubtipo_ItemsRequested; Tiempo máximo=0seg.</remarks>
    Private Sub CargarProvincias(ByVal sCodPais As String) Handles BusquedaEmpresas.eventProvinciasItemRequested
        Dim oProvincias As FSNServer.Provincias
        oProvincias = FSNServer.Get_Object(GetType(FSNServer.Provincias))

        oProvincias.Pais = sCodPais
        oProvincias.LoadData(Idioma)

        BusquedaEmpresas.Provincias = oProvincias.Data.Tables(0)
        BusquedaEmpresas.CargarComboProvincias()

        oProvincias = Nothing
    End Sub

    ''' <summary>
    ''' Carga las empresas con las opciones de filtro de busqueda y se mete en cache
    ''' </summary>
    ''' <remarks>Llamada desde:DevolverEmpresas()  ; Tiempo máximo:1seg.</remarks>
    Private Sub CargarEmpresas() Handles BusquedaEmpresas.eventBtnBuscarClick
        Dim oEmpresas As FSNServer.Empresas
        oEmpresas = FSNServer.Get_Object(GetType(FSNServer.Empresas))

        Dim dt As DataTable
        If Request.QueryString("desdeSolicitud") Is Nothing Then
            dt = oEmpresas.ObtenerEmpresas(Idioma)
        Else
            dt = oEmpresas.ObtenerEmpresas(Idioma, UsuCod:=FSNUser.Cod, RestriccionEmpresaUsuario:=FSNUser.PMRestringirEmpresasUSU, RestriccionEmpresaPerfilUsuario:=FSNUser.PMRestringirEmpresasPerfilUSU)
        End If

        BusquedaEmpresas.Empresas = dt
        BusquedaEmpresas.CargarEmpresas()

        oEmpresas = Nothing
    End Sub
End Class