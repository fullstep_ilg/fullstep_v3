﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BuscadorProcesos.aspx.vb"
    Inherits="Fullstep.FSNWeb.BuscadorProcesos" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
</head>
<script type="text/javascript">
    //Recoge el ID del proveedor seleccionado con el autocompletar
    function selected_Proveedor(sender, e) {
        oIdProveedor = document.getElementById('<%= hfProveedor.clientID %>');
        if (oIdProveedor)
            oIdProveedor.value = e._value;
    }

    /*  Descripcion: Elimina el contenido del textbox proveedor, al pulsar eliminar. En caso contrario deja escribir
    Parametros entrada:
    elem:la caja de texto
    event:Evento pulsado.
    Llamada desde: Evento que salta al pulsar la tecla de <-, suprimir
    Tiempo ejecucion:=0seg. */
    function ControlProveedor(elem, event) {
        //tanto si escribe algo como si pulsa la tecla de <- o suprimir, limpiamos lo que hay en el hidden
        oIdProveedor = document.getElementById('<%= hfProveedor.clientID %>');
        if (oIdProveedor)
            oIdProveedor.value = "";
        if (event.keyCode == 8 || event.keyCode == 46) {  //quitamos el espacio || event.keyCode == 32) {
            elem.value = ""
            return false;
        } else
            return true;
    }
    function AceptarSel() {
        var p = window.opener;
        if (p) {
            var grid = $find("<%= whdgProcesos.ClientID %>");
            var row = grid.get_gridView().get_behaviors().get_selection().get_selectedRows(0);
            if (!row) {
                alert(""+ Textos(32) +"");
                return;
            }
            cell = row.getItem(0).get_cellByColumnKey("ANYO");
            sANYO = cell.get_text();
            cell = row.getItem(0).get_cellByColumnKey("GMN1");
            sGMN1 = cell.get_text();
            cell = row.getItem(0).get_cellByColumnKey("COD");
            sCOD = cell.get_text();
            cell = row.getItem(0).get_cellByColumnKey("DEN");
            sDEN = cell.get_text();
            if (sGMN1 != null) {
                p.SeleccionarProceso(sANYO, sGMN1, sCOD, sDEN);
                window.close();
            }
            else {
                alert("" + Textos(32) + "");
            }
        }
    }
    function devolverDatosBuscArtic() {
        sCodArticuloID = document.getElementById('<%= hfArticulo.ClientID %>').id;
        sDenArticuloID = document.getElementById('<%= txtArticulo.clientID %>').id;
        sMaterial = document.getElementById('<%= hfMaterial.clientID %>');
        sMaterialBD = sMaterial.id;
        sMaterialDen = document.getElementById('<%= txtMaterial.clientID %>').id;
        var sMat;
        hMaterialVal = sMaterial.value;
        params = { hMaterialVal: hMaterialVal };
        $.when($.ajax({
            type: "POST",
            url: rutaFS + '_Common/App_Services/Consultas.asmx/Obtener_sMat',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(params),
            dataType: "json",
            async: false
        })).done(function (msg) {
            sMat = msg.d;
            var newWindow = window.open(rutaFS + "_common/BuscadorArticulos.aspx?desde=WebPartPanelCalidad&IdMaterialBD=" + sMaterialBD + "&IdMaterialDen=" + sMaterialDen + "&IdControlCodArt=" + sCodArticuloID + "&IdControlDenArt=" + sDenArticuloID + "&mat=" + sMat, "_blank", "width=850,height=630,status=yes,resizable=no,top=150,left=150");
            newWindow.focus();
        });
    }
    
 </script>
<body style="margin-top: 0px; margin-left: 0px; margin-right: 0px">
<script type="text/javascript">
    /*Funciones para paginacion*/

    function AdministrarPaginador() {
        var grid = $find("<%= whdgProcesos.ClientID %>");
        var parentGrid = grid;
        $('[id$=lblCount]').text(parentGrid.get_gridView().get_behaviors().get_paging().get_pageCount());
        if (parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() == 0) {
            $('[id$=ImgBtnFirst]').attr('disabled', 'disabled');
            $('[id$=ImgBtnFirst]').attr('src', rutaTheme + '/images/primero_desactivado.gif');
            $('[id$=ImgBtnPrev]').attr('disabled', 'disabled');
            $('[id$=ImgBtnPrev]').attr('src', rutaTheme + '/images/anterior_desactivado.gif');
        } else {
            $('[id$=ImgBtnFirst]').removeAttr('disabled');
            $('[id$=ImgBtnFirst]').attr('src', rutaTheme + '/images/primero.gif');
            $('[id$=ImgBtnPrev]').removeAttr('disabled');
            $('[id$=ImgBtnPrev]').attr('src', rutaTheme + '/images/anterior.gif');
        }
        if (parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() == parentGrid.get_gridView().get_behaviors().get_paging().get_pageCount() - 1) {
            $('[id$=ImgBtnNext]').attr('disabled', 'disabled');
            $('[id$=ImgBtnNext]').attr('src', rutaTheme + '/images/siguiente_desactivado.gif');
            $('[id$=ImgBtnLast]').attr('disabled', 'disabled');
            $('[id$=ImgBtnLast]').attr('src', rutaTheme + '/images/ultimo_desactivado.gif');
        } else {
            $('[id$=ImgBtnNext]').removeAttr('disabled');
            $('[id$=ImgBtnNext]').attr('src', rutaTheme + '/images/siguiente.gif');
            $('[id$=ImgBtnLast]').removeAttr('disabled');
            $('[id$=ImgBtnLast]').attr('src', rutaTheme + '/images/ultimo.gif');
        }
    }
    function whdgProcesos_PageIndexChanged(a, b, c, d) {
        var grid = $find("<%= whdgProcesos.ClientID %>");
        var parentGrid = grid;
        var dropdownlist = $('[id$=PagerPageList]')[0];

        dropdownlist.options[parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex()].selected = true;
        AdministrarPaginador();
    }

    function IndexChanged() {

        var dropdownlist = $('[id$=PagerPageList]')[0];
        var grid = $find("<%= whdgProcesos.ClientID %>");
        var parentGrid = grid.get_gridView();
        var newValue = dropdownlist.selectedIndex;
        parentGrid.get_behaviors().get_paging().set_pageIndex(newValue);
        AdministrarPaginador();
    }
    function FirstPage() {
        var grid = $find("<%= whdgProcesos.ClientID %>");
        var parentGrid = grid;
        parentGrid.get_gridView().get_behaviors().get_paging().set_pageIndex(0);
    }
    function PrevPage() {
        var grid = $find("<%= whdgProcesos.ClientID %>");
        var parentGrid = grid;
        var dropdownlist = $('[id$=PagerPageList]')[0];
        if (parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() > 0) {
            parentGrid.get_gridView().get_behaviors().get_paging().set_pageIndex(parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() - 1);
        }
    }
    function NextPage() {
        var grid = $find("<%= whdgProcesos.ClientID %>");
        var parentGrid = grid;
        var dropdownlist = $('[id$=PagerPageList]')[0];
        if (parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() < parentGrid.get_gridView().get_behaviors().get_paging().get_pageCount() - 1) {
            parentGrid.get_gridView().get_behaviors().get_paging().set_pageIndex(parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() + 1);
        }
    }
    function LastPage() {
        var grid = $find("<%= whdgProcesos.ClientID %>");
        var parentGrid = grid;
        parentGrid.get_gridView().get_behaviors().get_paging().set_pageIndex(parentGrid.get_gridView().get_behaviors().get_paging().get_pageCount() - 1);
    }
</script>
    <form id="frmBuscadorProcesos" style="background-color: #FFFFFF" name="frmBuscadorProcesos" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        <CompositeScript>
            <Scripts>
                <asp:ScriptReference Path="~/js/jsUpdateProgress.js" />
            </Scripts>
        </CompositeScript>
    </asp:ScriptManager>
    <asp:Panel ID="pnlParametros" runat="server" Width="100%">
        <table width="100%" cellpadding="0" border="0">
            <tr>
                <td>
                    <fsn:FSNPageHeader ID="FSNPageHeader" runat="server">
                    </fsn:FSNPageHeader>
                </td>
            </tr>
        </table>
        <table width="100%" style="table-layout: fixed" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td valign="top" style="width: 50%; padding-top: 5px; padding-left: 5px;">
                    <table width="100%" style="table-layout: fixed ; border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 110px; padding-top: 5px; padding-left: 5px">
                                <asp:Label ID="lblMaterial" runat="server" Text="dMaterial:" Font-Bold="true"></asp:Label>
                            </td>
                            <td colspan="3" style="width: 275px; padding-top: 5px;">
                                <table style="background-color: White; width: 250px; height: 20px; border: solid 1px #BBBBBB"
                                    cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtMaterial" runat="server" Width="250px" BorderWidth="0px"></asp:TextBox>
                                        </td>
                                        <td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px">
                                            <asp:ImageButton ID="imgMaterialLupa" runat="server" SkinID="BuscadorLupa" />
                                        </td>
                                    </tr>
                                    <asp:HiddenField ID="hfMaterial" runat="server" />
                                </table>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 110px; padding-top: 5px; padding-left: 5px">
                                <asp:Label ID="lblArticulo" runat="server" Text="dArticulo:" Font-Bold="true"></asp:Label>
                            </td>
                            <td colspan="3" style="width: 275px; padding-top: 5px;">
                                <table style="background-color: White; width: 250px; height: 20px; border: solid 1px #BBBBBB"
                                    cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtArticulo" runat="server" Width="250px" BorderWidth="0px"></asp:TextBox>
                                        </td>
                                        <td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px">
                                            <asp:ImageButton ID="imgArticuloLupa" runat="server" SkinID="BuscadorLupa" />
                                        </td>
                                    </tr>
                                    <asp:HiddenField ID="hfArticulo" runat="server" />
                                </table>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 110px; padding-top: 5px; padding-left: 5px">
                                <asp:Label ID="lblProveedor" runat="server" Text="dProveedor:" Font-Bold="true"></asp:Label>
                            </td>
                            <td colspan="3" style="width: 275px; padding-top: 5px;">
                                <table style="background-color: White; width: 250px; height: 20px; border: solid 1px #BBBBBB"
                                    cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtProveedor" runat="server" Width="250px" BorderWidth="0px"></asp:TextBox>
                                        </td>
                                        <ajx:AutoCompleteExtender ID="txtProveedor_AutoCompleteExtender" runat="server" CompletionSetCount="10"
                                            DelimiterCharacters="" Enabled="True" MinimumPrefixLength="1" ServiceMethod="GetProveedores"
                                            ServicePath="App_Services/AutoComplete.asmx" TargetControlID="txtProveedor" EnableCaching="False" OnClientItemSelected="selected_Proveedor">
                                        </ajx:AutoCompleteExtender>
                                        <td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px">
                                            <asp:ImageButton ID="imgProveedorLupa" runat="server" SkinID="BuscadorLupa" />
                                        </td>
                                    </tr>
                                    <asp:HiddenField ID="hfProveedor" runat="server" />
                                </table>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 110px; padding-top: 5px; padding-left: 5px">
                                <asp:Label ID="lblEstado" runat="server" Text="dEstado:" Font-Bold="true"></asp:Label>
                            </td>
                            <td colspan="3" style="width: 275px; padding-top: 5px;">
                                <ig:WebDropDown ID="wddEstado" runat="server" Width="270px" DropDownAnimationDuration="1"
                                    DropDownContainerWidth="270" EnableDropDownAsChild="false" MultipleSelectionType="Checkbox"
                                    EnableMultipleSelection="true" EnableClosingDropDownOnSelect="false" BackColor="White">
                                </ig:WebDropDown>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 110px; padding-top: 5px; padding-left: 5px">
                                <asp:Label ID="lblMoneda" runat="server" Text="dMoneda:" Font-Bold="true"></asp:Label>
                            </td>
                            <td style="width: 130px; padding-top: 5px;">
                                <ig:WebDropDown ID="wddMoneda" runat="server" Width="90px" DropDownContainerWidth="250px"
                                    DropDownAnimationDuration="1" EnableDropDownAsChild="False" BackColor="White">
                                    <ItemTemplate>
                                        <table style="width: 100%" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td style="width: 50px;">
                                                    <%#DataBinder.Eval(Container.DataItem, "COD")%>
                                                </td>
                                                <td style="width: 200px; padding-left: 10px;">
                                                    <%#DataBinder.Eval(Container.DataItem, "DEN")%>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </ig:WebDropDown>
                            </td>
                            <td style="width: 90px; padding-top: 5px; padding-left: 25px">
                                <asp:Label ID="lblSolicitud" runat="server" Text="dSolicitud:" Font-Bold="true"></asp:Label>
                            </td>
                            <td style="width: 80px; padding-top: 5px; padding-left: 2px">
                                <asp:TextBox ID="txtSolicitud" runat="server" Width="80px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 50%; padding-left: 20px">
                    <table width="100%" style="table-layout: fixed" border="0" cellpadding="0" cellspacing="5">
                        <tr>
                            <td>
                                <fieldset id="fsPresupuesto" runat="server">
                                    <legend style="vertical-align: top">
                                        <asp:Label ID="lblfsPresupuesto" runat="server" Text="Presupuesto" Font-Bold="true" ForeColor="Black">
                                        </asp:Label>
                                    </legend>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="center">
                                                <asp:Label ID="lblPresupuestoDesde" runat="server" Text="dDesde:" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td align="left">
                                                <igpck:WebNumericEditor ID="wnePresupuestoDesde" runat="server" Width="90px">
                                                </igpck:WebNumericEditor>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblPresupuestoHasta" runat="server" Text="dHasta:" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td align="left">
                                                <igpck:WebNumericEditor ID="wnePresupuestoHasta" runat="server" Width="90px">
                                                </igpck:WebNumericEditor>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <fieldset id="fsFechaApertura" runat="server">
                                    <legend style="vertical-align: top">
                                        <asp:Label ID="lblfsFechaApertura" runat="server" Text="Fecha apertura" Font-Bold="true" ForeColor="Black">
                                        </asp:Label>
                                    </legend>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="center">
                                                <asp:Label ID="lblFechaDesde" runat="server" Text="dDesde:" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td align="left">
                                                <igpck:WebDatePicker runat="server" ID="dteFechaDesde" Width="90px" dropdowncalendarID="calendarioPicker" SkinID="Calendario"></igpck:WebDatePicker>
                                                <igpck:WebMonthCalendar runat="server" ID="calendarioPicker"></igpck:WebMonthCalendar>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblFechaHasta" runat="server" Text="dHasta:" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td align="left">
                                                <igpck:WebDatePicker runat="server" ID="dteFechaHasta" Width="90px" dropdowncalendarID="calendarioPicker" SkinID="Calendario"></igpck:WebDatePicker>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td>
                                <asp:RadioButtonList ID="rblOpcionesAdjudicacion" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" Text="dAdjudicacion directa"></asp:ListItem>
                                    <asp:ListItem Text="dReunion" style="padding-left: 50px"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td style="width: 95%">
                </td>
                <td>
                    <fsn:FSNButton ID="btnBuscar" Text="DBuscar" runat="server">                    
                    &nbsp;&nbsp;&nbsp;&nbsp;                    
                    </fsn:FSNButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlGridProcesos" runat="server" Width="100%">
        <!--Filtro -->
        <asp:Panel ID="pnlSinPaginacion" runat="server" Visible="True" Width="100%">
            <div style="border: 1px solid #CCCCCC;">
                <table border="0" width="100%">
                    <tr>
                        <td>
                            <asp:Label ID="lblFiltrarPor" runat="server" Text="DFiltrar Por:" Font-Bold="true"></asp:Label>
                            <asp:RadioButton ID="optFiltrar1" GroupName="optFiltrarPor" runat="server" AutoPostBack="true" />
                            <asp:ImageButton ID="imgFiltrar1" SkinID="Filtro_Cabecera" runat="server" />
                            <asp:RadioButton ID="optFiltrar2" GroupName="optFiltrarPor" runat="server" AutoPostBack="true" />
                            <asp:ImageButton ID="imgFiltrar2" SkinID="Filtro_Columna" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <!--Grid Empresas -->
        <asp:UpdatePanel ID="UpdatePanelProcesos" runat="server" UpdateMode="conditional">
            <ContentTemplate>
                <table border="0" width="100%" style="margin-left: 5px; margin-right: 5px;" cellspacing="0">
                    <tr>
                        <td style="height: 5px">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" rowspan="2">
                            <div id="divProcesos">
                                <ig:WebHierarchicalDataGrid ID="whdgProcesos" runat="server" Width="100%" Height="500px" SkinID="Visor" Browser="Xml"
                                 AutoGenerateBands="false" AutoGenerateColumns="false" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="true">
                                    <Behaviors>
                                        <ig:Selection  RowSelectType="Single" Enabled="True" CellClickAction="Row" ></ig:Selection> 
                                        <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true"  AnimationEnabled="true">
                                            <FilteringClientEvents DataFiltered="AdministrarPaginador" /> 
                                        </ig:Filtering> 
                                        <ig:RowSelectors Enabled="false" RowNumbering="false"></ig:RowSelectors>
                                        <ig:Paging Enabled="true" PagerAppearance="Top" PageSize="20">
                                            <PagingClientEvents PageIndexChanged="whdgProcesos_PageIndexChanged" />
                                           <PagerTemplate>
								            <div class="CabeceraBotones" style="clear:both; float:left; width:100%;"> 
									        <div style="float:left; padding:3px 0px 2px 0px; margin-left:10px;">
										        <div style="clear: both; float: left; margin-right: 5px;">
                                                    <asp:Image ID="ImgBtnFirst" runat="server" OnClientClick="FirstPage()" style="margin-right:2px;" />
                                                    <asp:Image ID="ImgBtnPrev" runat="server" OnClientClick="PrevPage()" />
                                                </div>
                                                <div style="float: left; margin-right:5px;">
                                                    <asp:Label ID="lblPage" runat="server" Text="dPage" Style="margin-right: 3px;"></asp:Label>
                                                    <asp:DropDownList ID="PagerPageList" runat="server" onchange="return IndexChanged()" Style="margin-right: 3px;" />
                                                    <asp:Label ID="lblOF" runat="server" Text="dOf" Style="margin-right: 3px;"></asp:Label>
                                                    <asp:Label ID="lblCount" runat="server" />
                                                </div>
                                                <div style="float: left;">
                                                    <asp:Image ID="ImgBtnNext" runat="server" OnClientClick="NextPage()" style="margin-right:2px;" />
                                                    <asp:Image ID="ImgBtnLast" runat="server" OnClientClick="LastPage()" />
                                                </div>
									        </div>   
                                            </div>  
                                            </PagerTemplate>
                                        </ig:Paging>
                                        <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
                                    </Behaviors>                                    
                                </ig:WebHierarchicalDataGrid>
                            </div>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnBuscar" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:Panel ID="pnlComandos" runat="server" Visible="True" Width="100%">
            <table width="100%">
                <tr>
                    <td align="right" width="50%">
                        <fsn:FSNButton ID="btnAceptar" runat="server" Text="DAceptar" Alineacion="Right"
                            OnClientClick="AceptarSel()"></fsn:FSNButton>
                            <!--OnClientClick="aceptar();return false;"-->
                    </td>
                    <td align="left" width="50%">
                        <fsn:FSNButton ID="btnCancelar" runat="server" Text="DCancelar" Alineacion="Left"
                            OnClientClick="window.close();return false;"></fsn:FSNButton>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </asp:Panel>

   

    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
        <div style="position: relative; top: 30%; text-align: center;">
            <asp:Image ID="ImgProgress" runat="server" SkinID="ImgProgress" />
            <asp:Label ID="LblProcesando" runat="server" Text="..."></asp:Label>
        </div>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
    <asp:HiddenField runat="server" ID="hfAnyo" />
    <asp:HiddenField runat="server" ID="hfCommodity" />
    <asp:HiddenField runat="server" ID="hfCodProceso" />
    <asp:HiddenField runat="server" ID="hfDenProceso" />
    </form>
</body>
</html>
