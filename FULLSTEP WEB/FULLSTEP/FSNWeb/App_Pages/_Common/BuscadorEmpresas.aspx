﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BuscadorEmpresas.aspx.vb" Inherits="Fullstep.FSNWeb.BuscadorEmpresas" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
    <title></title>
	<script>
	    function Alert(msg) {
	        $get('divAlertContent').innerHTML = msg;
	        popUpShowed = $find('Alert');
	        popUpShowed.show();
	        popUpShowed._backgroundElement.style.zIndex += 10;
	        popUpShowed._foregroundElement.style.zIndex += 10;
	    }

	    window.alert = Alert;           

	    /*
	    Descripcion: Funcion que inicializa la caja de texto del combo Subtipo a ""
	    Parametros entrada:=
	    elem:=componente input
	    event:=evento
	    Llamada desde: Al cambiar el valor del combo tipo
	    Tiempo ejecucion:= 0seg.    
    
	    */
	    function ValueChanged_wddTipo_CambioTipo(elem, event) {
	        comboSubtipo = document.getElementById("x:1129115704.2:mkr:Input")
	        if (comboSubtipo)
	            comboSubtipo.value = "";
	    }


	    function InputKeyDown_wwdSubtipo_Inicializar(elem, event) {
	        comboSubtipo = document.getElementById("x:1129115704.2:mkr:Input")
	        if (comboSubtipo)
	            comboSubtipo.value = "";
	    }    
	</script>		
</head>

<body style="background-color: #EEEEEE;margin-top: 0px; margin-left: 0px; margin-right:0px">
    <form id="frmEmpresas" name="frmEmpresas" method="post" runat="server">
    <asp:ScriptManager runat="server" ID="scriptManager1">
        <CompositeScript>
            <Scripts>
                <asp:ScriptReference Path="~/js/jsUpdateProgress.js" />
            </Scripts>
        </CompositeScript>
    </asp:ScriptManager>
    <div>
        <fspm:BusquedaEmpresas id="BusquedaEmpresas" runat="server"></fspm:BusquedaEmpresas>
        <input type="hidden" id="idControl" runat="server" />
        <input type="hidden" id="idHidControl" runat="server" />
    </div>

    <!-- Cargando -->
        
        <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
			<div style="position: relative; top: 30%; text-align: center;">
				<asp:Image ID="ImgProgress" runat="server" SkinId="ImgProgress" />
                <asp:Label ID="LblProcesando" runat="server" Text="..."></asp:Label>	
			</div>
		</asp:Panel>
		<ajx:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
			BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
    </form>
</body>
</html>
