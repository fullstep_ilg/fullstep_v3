﻿Imports Infragistics.Web.UI
Imports Infragistics.Web.UI.GridControls.WebDataGrid

Partial Public Class BuscadorSolicitudes
    Inherits FSNPage
#Region "Propertys"
    ''' <summary>
    ''' Obtener el nombre con el q se ha guardado en cache el dataset de Solicitudes
    ''' </summary>
    ''' <returns>Nombre con el q se ha guardado en cache el dataset de Solicitudes</returns>
    ''' <remarks>Llamada desde: Page_Load    wdgSolicitudes_InitializeDataSource          BuscarSolicitudes
    ''' ; Tiempo maximo:0</remarks>
    Private ReadOnly Property DataSourceCacheSolicitudes() As String
        Get
            Return "Solicitudes_" + Usuario.Cod.ToString()
        End Get
    End Property
    ''' <summary>
    ''' Obtener el nombre con el q se ha guardado en cache el dataset de Lineas
    ''' </summary>
    ''' <value></value>
    ''' <returns>Nombre con el q se ha guardado en cache el dataset de Lineas</returns>
    ''' <remarks>Llamada desde: Page_Load    wdgLineas_InitializeDataSource         wdgLineas_InitializeLayout      wdgLineas_InitializeRow
    ''' BuscarLineas ; Tiempo maximo:0</remarks>
    Private ReadOnly Property DataSourceCacheLineas() As String
        Get
            Return "Lineas_" + Usuario.Cod.ToString()
        End Get
    End Property
#End Region
#Region "Carga Inicial"
    ''' <summary>
    ''' Cargar la pagina. 
    ''' Si se proporciona "DatosLinea": Sirve para ver el detalle de la Instancia/Linea origen de la vinculación
    ''' Eoc: Sirve para Buscar y devolver Instancias (vinculación a nivel de solicitud) ó Lineas (vinculación a nivel de desglose origen)
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>    
    ''' <remarks>Llamada desde:sistema; Tiempo máximo:0</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.BuscadorSolicitudes

        If esImportacion Then
            btnAceptar1.Attributes.Add("onclick", "Importar();")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Solicitud", "<script>var lSolicitud = '" & Request.QueryString("Solicitud") & "' </script>")
        Else
            btnSiguiente.Attributes.Add("onclick", "return Siguiente('" & Request.QueryString("sRoot") & "','" & Request.QueryString("IdCampo") & "');")
            If Request("Index") Is Nothing Then
                btnAceptar1.Attributes.Add("onclick", "Siguiente('" & Request.QueryString("sRoot") & "','" & Request.QueryString("IdCampo") & "');return false;")
            Else
                btnAceptar1.Attributes.Add("onclick", "Mover('" & Request.QueryString("sRoot") & "','" & Request.QueryString("IdCampo") & "','" & Request.QueryString("Index") & "','" & Request.QueryString("Celda") & "','" & Request.QueryString("Frame") & "','" & Request.QueryString("PopUp") & "');return false;")
            End If
        End If

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "sMensaje", "<script>var sMensajeSolicitud = '" & If(esTipoFactura, JSText(Textos(53)), JSText(Textos(42))) & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "sMensajeUn", "<script>var sMensajeUnTipo = '" & JSText(Textos(51)) & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "msgFechaObligatoria", "<script>var msgFechaObligatoria = '" & JSText(Textos(52)) & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "frames.aspx?pagina='</script>")

        If Not IsPostBack Then
            HttpContext.Current.Cache.Remove(DataSourceCacheSolicitudes)
            HttpContext.Current.Cache.Remove(DataSourceCacheLineas)

            FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/seguimiento.gif"

            CargarTextos()
            CargarGridSolicitudes()

            If (Request("DatosLinea") Is Nothing) Then 'Buscador para añadir o mover
                CargarEstados()
                Dim bSoloSolic As Boolean = CargarTipos()

                InicializacionControles()

                If bSoloSolic Then
                    btnSiguiente.Visible = False
                    btnAtras.Visible = False
                    btnAceptar1.Visible = True
                    btnAceptar2.Visible = False
                Else
                    btnSiguiente.Visible = True
                    btnAtras.Visible = False
                    btnAceptar1.Visible = False
                    btnAceptar2.Visible = False
                End If

                imgPeticionarioLupa.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Img_Ico_Lupa.gif"
            Else 'Detalle
                Dim Spli() As String = Request("DatosLinea").ToString.Split("@")
                pnlStep1.Visible = False
                pnlStep2.Visible = True
                btnSiguiente.Visible = False
                btnAtras.Visible = False
                btnAceptar1.Visible = False
                btnAceptar2.Visible = False
                pnlSinPaginacion2.Visible = False

                If UBound(Spli) > 0 Then
                    lblInfo1.Visible = False
                    BuscarLineas(CLng(Spli(0)), "", CLng(Spli(1)), , CInt(Spli(2)))
                    CargarGridLineas()
                Else
                    lblInfo1.Visible = True
                    wdgLineas.Visible = False
                    BuscarDatosInstancia(Spli(0))
                End If

                UpdatePanelLineas.Update()

                Dim sDatosSolic As String = Request("DatosSolic")
                sDatosSolic = Replace(sDatosSolic, "," & Request("DatosLinea"), "")
                lblDescSolicitud.Text = sDatosSolic
                lblDescSolicitud.ToolTip = lblDescSolicitud.Text
            End If
        End If

        If (Request("DatosLinea") Is Nothing) Then
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "mensaje_validacion") Then
                Dim scriptText = "function mensaje_validacion() {alert(""" + If(esTipoFactura, Textos(53), Textos(42)) + """);}"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensaje_validacion", scriptText, True)
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "Aceptar2") Then
                Dim scriptText = "function aceptarLineas() {" & vbNewLine &
                                "   p = window.opener;" & vbNewLine &
                                "   if (p) {" & vbNewLine &
                                "       _grid = $find(""wdgLineas"");" & vbNewLine &
                                "       Nlin=0;" & vbNewLine &
                                "       for(i=0; i<_grid.get_behaviors().get_selection().get_selectedRows().get_length(); i++) {" & vbNewLine &
                                "           Nlin=Nlin+1;" & vbNewLine &
                                "           selectedRow = _grid.get_behaviors().get_selection().get_selectedRows().getItem(i);" & vbNewLine &
                                "           p.VincularCopiarFilaVacia('" & Request.QueryString("sRoot") & "','" & Request.QueryString("IdCampo") & "',selectedRow);" & vbNewLine &
                                "       }" & vbNewLine &
                                "       if (Nlin==0){" & vbNewLine &
                                "           alert(""" + Textos(44) + """);" & vbNewLine &
                                "       }" & vbNewLine &
                                "       else{" & vbNewLine &
                                "           window.close();" & vbNewLine &
                                "       }" & vbNewLine &
                                "   }" & vbNewLine &
                                "}" & vbNewLine
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Aceptar2", scriptText, True)
            End If

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalProgress", "<script>var ModalProgress = '" & ModalProgress.ClientID & "';</script>")
        End If

        If esImportacion Then
            actualizargrid()
        End If
    End Sub
    ''' <summary>
    ''' Establece los textos de los controles de acuerdo con el idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load; Tiempo máximo:0</remarks> 
    Private Sub CargarTextos()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.BuscadorSolicitudes

        If (Request("DatosLinea") Is Nothing) Then
            FSNPageHeader.TituloCabecera = If(esTipoFactura, Textos(55), Textos(0))
            btnCancelar.Text = Textos(20)
        Else
            FSNPageHeader.TituloCabecera = Textos(47)
            btnCancelar.Text = Textos(48)
        End If
        lblIdentificador.Text = String.Format("{0}:", Textos(1))
        lblEstado.Text = String.Format("{0}:", Textos(2))
        lblTipoSolicitud.Text = String.Format("{0}:", If(esTipoFactura, Textos(54), Textos(3)))
        lblFechaAltaDesde.Text = String.Format("{0}:", Textos(4))
        lblFechaAltaHasta.Text = String.Format("{0}:", Textos(24))
        lblArticulo.Text = String.Format("{0}:", Textos(5))
        lblDenominacion.Text = String.Format("{0}:", Textos(6))
        lblPeticionario.Text = String.Format("{0}:", Textos(7))
        lblImporteDesde.Text = String.Format("{0}:", Textos(8))
        lblImporteHasta.Text = String.Format("{0}:", Textos(9))
        btnBuscar.Text = Textos(10)
        lblFiltrarPor.Text = String.Format("{0}:", Textos(11))
        lblFiltrarPor_2.Text = lblFiltrarPor.Text
        btnSiguiente.Text = Textos(19)
        btnAtras.Text = String.Format("<- {0}", Textos(22))
        btnAceptar1.Text = Textos(23)
        btnAceptar2.Text = Textos(23)
        lblInfo1.Text = If(esTipoFactura, Textos(56), Textos(43))
    End Sub
    ''' <summary>
    ''' Inicializa los componentes y funciones javascript de los elementos
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load; Tiempo máximo:0</remarks> 
    Private Sub InicializacionControles()
        optFiltrar1.Checked = True
        optFiltrar1_2.Checked = True

        imgPeticionarioLupa.Attributes.Add("onClick", "javascript:var newWindow = window.open('" & ConfigurationManager.AppSettings("rutaFS") & "_Common/BuscadorUsuarios.aspx?&idControl=" & txtPeticionario.ID & "', '_blank', 'width=850,height=630,status=yes,resizable=no,top=200,left=200');newWindow.focus();")
        dteFechaAltaDesde.NullText = ""
        dteFechaAltaDesde.Value = Today.AddYears(-1)
        dteFechaAltaHasta.NullText = ""

        If esImportacion Then
            tblPeticionario.Visible = False
            lblDenPeticionario.Visible = True
            lblDenPeticionario.Text = FSNUser.Nombre
            hfPeticionario.Value = FSNUser.CodPersona
        Else
            lblDenPeticionario.Visible = False
        End If
    End Sub
    ''' <summary>
    ''' Carga la lista de estados de proceso
    ''' </summary>
    ''' <remarks>Llamada desde=Page_Load; Tiempo máximo=0seg.</remarks>
    Private Sub CargarEstados()
        '(ninguno)
        AñadirElementoEstado(-1, Textos(32))
        'Pendientes
        AñadirElementoEstado(TipoEstadoSolic.Pendiente, Me.Textos(25))
        'Guardadas
        AñadirElementoEstado(TipoEstadoSolic.Guardada, Me.Textos(26))
        'En curso
        AñadirElementoEstado(TipoEstadoSolic.EnCurso, Me.Textos(27))
        'Rechazadas
        AñadirElementoEstado(TipoEstadoSolic.Rechazada, Me.Textos(28))
        'Anuladas
        AñadirElementoEstado(TipoEstadoSolic.Anulada, Me.Textos(29))
        'Finalizadas
        'NOTA:en presentacion solo se va a utilizar el valor TipoEstadoSolic.Enviada para englobar todos los estados que se consideran 
        'como("Finalizada"), despues en la consulta a la BBDD se incluiran, con el valor 1, los valores de enumerado 7,100,101,102 y 103
        AñadirElementoEstado(TipoEstadoSolic.Enviada, Me.Textos(30))
        'Cerradas
        AñadirElementoEstado(TipoEstadoSolic.SCCerrada, Me.Textos(31))

        wddEstado.CurrentValue = Textos(32)
        wddEstado.SelectedValue = -1
    End Sub
    ''' <summary>
    ''' Carga un elemento de la lista de estados de proceso
    ''' </summary>
    ''' <param name="value">Id del elemento</param>
    ''' <param name="text">Texto del elemento</param>
    ''' <remarks>Llamada desde=CargarEstados; Tiempo máximo=0seg.</remarks>
    Private Sub AñadirElementoEstado(ByVal value As Nullable(Of Integer), ByVal text As String)
        Dim oItem As New ListControls.DropDownItem
        oItem.Value = value
        oItem.Text = text
        wddEstado.Items.Add(oItem)
    End Sub
    ''' <summary>
    ''' Carga el combo de Tipos de solictudes e indica si hay alguna vinculación a nivel de desglose origen
    ''' </summary>
    ''' <returns>Si hay alguna vinculación a nivel de desglose origen</returns>
    ''' <remarks>Llamada desde:Page_Load; Tiempo máximo:0</remarks> 
    Private Function CargarTipos() As Boolean
        Dim idCampo As Long
        If IsNumeric(Request.QueryString("IdCampo")) Then idCampo = CInt(Request.QueryString("IdCampo"))

        Dim lInstancia As Long
        If IsNumeric(Request.QueryString("Instancia")) Then lInstancia = CInt(Request.QueryString("Instancia"))

        If Request("Index") Is Nothing And Request("Importar") Is Nothing Then
            Dim s As FSNServer.Solicitudes = FSNServer.Get_Object(GetType(FSNServer.Solicitudes))
            Dim tiposSolicitud As DataSet = s.LoadTiposSolicitudes(Usuario.Idioma, lInstancia, idCampo)

            If tiposSolicitud.Tables(0).Rows.Count > 1 Then
                'combo visible
                lblDenTipoSolicitud.Visible = False

                If Not tiposSolicitud Is Nothing Then
                    Dim emptyRow As DataRow = tiposSolicitud.Tables(0).NewRow()
                    emptyRow.ItemArray = New Object() {Nothing, Textos(33)}
                    tiposSolicitud.Tables(0).Rows.InsertAt(emptyRow, 0)
                End If

                wddTipoSolicitud.TextField = "DEN"
                wddTipoSolicitud.ValueField = "ID"
                wddTipoSolicitud.DataSource = tiposSolicitud
                wddTipoSolicitud.DataBind()

                wddTipoSolicitud.CurrentValue = Textos(33)
                wddTipoSolicitud.SelectedValue = Nothing
            Else 'Label visible
                lblDenTipoSolicitud.Visible = True
                wddTipoSolicitud.Visible = False

                If tiposSolicitud.Tables(0).Rows.Count = 0 Then
                    lblDenTipoSolicitud.Text = ""
                    hfTipoSolicitud.Value = 0
                Else
                    lblDenTipoSolicitud.Text = tiposSolicitud.Tables(0).Rows(0)("DEN").ToString
                    hfTipoSolicitud.Value = tiposSolicitud.Tables(0).Rows(0)("ID").ToString
                End If
            End If

            For Each Row As DataRow In tiposSolicitud.Tables(1).Rows
                If Row.Item("CAMPO_ORIGEN") > 0 Then
                    Return False
                End If
            Next

            Return True
        Else
            lblDenTipoSolicitud.Visible = True
            wddTipoSolicitud.Visible = False

            Dim s As FSNServer.Solicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
            s.ID = Request("Solicitud")
            s.Load(Me.Idioma)

            lblDenTipoSolicitud.Text = s.Den(Me.Idioma)
            hfTipoSolicitud.Value = s.ID

            Return True
        End If
    End Function
    ''' <summary>
    ''' Cunado se ve el detalle de una linea vinculada a nivel de solicitud sin desglose, se debe ver Descr Instancia, fecha alta Instancia
    ''' y Peticionario Instancia
    ''' </summary>
    ''' <param name="lInstancia">Instancia Origen</param>
    ''' <remarks>Llamada desde: Page_load; tiempo maximo:0</remarks>
    Private Sub BuscarDatosInstancia(ByVal lInstancia As Long)
        Dim oInstan As FSNServer.Instancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        oInstan.ID = lInstancia

        oInstan.Load(FSNUser.Idioma, True)

        lblInfo1.Text = String.Format("{0}:", Textos(49)) & "&nbsp;" & modUtilidades.FormatDate(oInstan.FechaAlta, FSNUser.DateFormat)
        lblInfo1.Text = lblInfo1.Text & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & String.Format("{0}:", Textos(50)) & " " & oInstan.NombrePeticionario
    End Sub
#End Region
#Region "wdgSolicitudes"
    Private Sub actualizargrid()
        wdgSolicitudes.Rows.Clear()
        wdgSolicitudes.Columns.Clear()
        wdgSolicitudes.DataSource = BuscarSolicitudes()
        CrearCol_Solicitudes()
        wdgSolicitudes.DataBind()
        Conf_GridSolicitudes()
    End Sub
    ''' <summary>
    ''' Carga la grid con los datos
    ''' </summary>
    ''' <remarks>Llamada desde=sistema;Tiempo máximo=0,3seg.</remarks>
    Private Sub CargarGridSolicitudes()
        wdgSolicitudes.Rows.Clear()
        wdgSolicitudes.Columns.Clear()

        Dim dataSource As DataTable
        If Not CType(HttpContext.Current.Cache.Item(DataSourceCacheSolicitudes), DataTable) Is Nothing Then
            dataSource = CType(HttpContext.Current.Cache.Item(DataSourceCacheSolicitudes), DataTable)
        Else
            dataSource = SolicitudesLayout()
        End If
        wdgSolicitudes.DataSource = dataSource
        CrearCol_Solicitudes()
        wdgSolicitudes.DataBind()
        Conf_GridSolicitudes()
    End Sub
    Private Sub CrearCol_Solicitudes()
        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
        Dim nombre As String

        For i As Integer = 0 To wdgSolicitudes.DataSource.Columns.Count - 1
            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            nombre = wdgSolicitudes.DataSource.Columns(i).ToString
            If (nombre <> "IMPORTE") Then
                With campoGrid
                    .Key = nombre
                    .DataFieldName = nombre
                    .Header.Text = nombre
                End With
                wdgSolicitudes.Columns.Add(campoGrid)
            Else
                AnyadirColumnaU(nombre, wdgSolicitudes)
            End If
        Next
    End Sub
    Private Sub AnyadirColumnaU(ByVal fieldname As String, ByVal grid As GridControls.WebDataGrid)
        Dim unboundField As New Infragistics.Web.UI.GridControls.UnboundField(True)
        unboundField.Key = fieldname
        grid.Columns.Add(unboundField)
    End Sub
    ''' <summary>
    ''' Se ejecuta una vez, al cargarse la grid
    ''' </summary>
    ''' <remarks>Llamada desde: Al cargarse la grid; Tiempo máximo: 0 sg.</remarks>
    Private Sub Conf_GridSolicitudes()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.BuscadorSolicitudes

        wdgSolicitudes.Height = Unit.Pixel(240)

        wdgSolicitudes.Columns("ID").Header.Text = Textos(12)
        wdgSolicitudes.Columns("DEN").Header.Text = Textos(13)
        wdgSolicitudes.Columns("FECHAALTA").Header.Text = Textos(14)
        wdgSolicitudes.Columns("IMPORTE").Header.Text = Textos(15)
        wdgSolicitudes.Columns("PET_DEN").Header.Text = Textos(16)
        wdgSolicitudes.Columns("TIPO_DEN").Header.Text = If(esTipoFactura, Textos(54), Textos(17))
        wdgSolicitudes.Columns("EST_DEN").Header.Text = Textos(18)

        wdgSolicitudes.Columns("ID").Width = Unit.Pixel(100)
        wdgSolicitudes.Columns("DEN").Width = Unit.Pixel(175)
        wdgSolicitudes.Columns("FECHAALTA").Width = Unit.Pixel(100)
        wdgSolicitudes.Columns("IMPORTE").Width = Unit.Pixel(100)
        wdgSolicitudes.Columns("PET_DEN").Width = Unit.Pixel(145)
        wdgSolicitudes.Columns("TIPO_DEN").Width = Unit.Pixel(150)
        wdgSolicitudes.Columns("EST_DEN").Width = Unit.Pixel(100)

        wdgSolicitudes.Columns("FECHAALTA").FormatValue(FSNUser.DateFormat.ShortDatePattern)

        wdgSolicitudes.Columns("PET").Hidden = True
        wdgSolicitudes.Columns("TIPO").Hidden = True
        wdgSolicitudes.Columns("EST").Hidden = True
        If (Request("Index") Is Nothing And Not esImportacion) Then
            wdgSolicitudes.Columns("IDDESGLOSEORIGEN").Hidden = True
            wdgSolicitudes.Columns("IDVINCULADO").Hidden = True
        End If
    End Sub
    ''' <summary>
    ''' Se ejecuta cada vez que se inicializa una fila del grid
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>LLamada desde: Al cargar la grid con datos; Tiempo máximo: 0 sg</remarks>
    Private Sub wdgSolicitudes_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles wdgSolicitudes.InitializeRow
        e.Row.Items.FindItemByKey("IMPORTE").Text = modUtilidades.FormatNumber(e.Row.Items.FindItemByKey("IMPORTE").Value, FSNUser.NumberFormat)
    End Sub
    ''' <summary>
    ''' Inicializa el dataset de Solicitudes
    ''' </summary>
    ''' <returns>dataset de Solicitude</returns>
    ''' <remarks>Llamada desde: wdgSolicitudes_InitializeDataSource; Tiempo maximo: 0</remarks>
    Private Function SolicitudesLayout() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("ID", GetType(Integer))
        dt.Columns.Add("DEN", GetType(String))
        dt.Columns.Add("FECHAALTA", GetType(Date))
        dt.Columns.Add("IMPORTE", GetType(Double))
        dt.Columns.Add("PET", GetType(String))
        dt.Columns.Add("PET_DEN", GetType(String))
        dt.Columns.Add("TIPO", GetType(Integer))
        dt.Columns.Add("TIPO_DEN", GetType(String))
        dt.Columns.Add("EST", GetType(Integer))
        dt.Columns.Add("EST_DEN", GetType(String))
        dt.Columns.Add("IDDESGLOSEORIGEN", GetType(Integer))
        dt.Columns.Add("IDVINCULADO", GetType(Integer))
        Return dt
    End Function
    ''' <summary>
    ''' Esta función carga el datatable de solicitudes con las instancias de la solicitud/es origen. Luego lo mete en cache.
    ''' </summary>
    ''' <returns>datatable de solicitudes</returns>
    ''' <remarks>Llamada desde:btnBuscar_Click; Tiempo maximo:0,3</remarks>
    Private Function BuscarSolicitudes() As DataTable
        Dim idCampo As Long
        If IsNumeric(Request.QueryString("IdCampo")) Then idCampo = CInt(Request.QueryString("IdCampo"))

        Dim lInstancia As Long
        If IsNumeric(Request.QueryString("Instancia")) Then lInstancia = CLng(Request.QueryString("Instancia"))

        Dim identificador As String = txtIdentificador.Text
        Dim estado As Nullable(Of Integer)
        If IsNumeric(wddEstado.SelectedValue) AndAlso wddEstado.SelectedValue <> -1 Then
            estado = CInt(wddEstado.SelectedValue)
        End If

        Dim denominacion As String = txtDenominacion.Text
        Dim tipo As Nullable(Of Long)
        If wddTipoSolicitud.Visible Then
            If IsNumeric(wddTipoSolicitud.SelectedValue) Then
                tipo = CInt(wddTipoSolicitud.SelectedValue)
            End If
        Else
            If hfTipoSolicitud.Value = 0 Then
            Else
                tipo = hfTipoSolicitud.Value
            End If
        End If

        Dim peticionario As String = hfPeticionario.Value

        Dim fechaAltaDesde As Nullable(Of Date)
        If dteFechaAltaDesde.Value = New Date Then
            fechaAltaDesde = Nothing
        Else
            fechaAltaDesde = dteFechaAltaDesde.Value
        End If

        Dim fechaAltaHasta As Nullable(Of Date)
        If dteFechaAltaHasta.Value = New Date Then
            fechaAltaHasta = Nothing
        Else
            fechaAltaHasta = dteFechaAltaHasta.Value
        End If

        Dim importeDesde As Nullable(Of Double)
        If Not Double.IsNaN(wneImporteDesde.Value) Then
            importeDesde = wneImporteDesde.Value
        End If

        Dim importeHasta As Nullable(Of Double)
        If Not Double.IsNaN(wneImporteHasta.Value) Then
            importeHasta = wneImporteHasta.Value
        End If

        Dim articulo As String = txtArticulo.Text

        Dim InstanciaMover As Nullable(Of Long)
        If Not (Request("Index") Is Nothing) Then
            If IsNumeric(Request.QueryString("Instancia")) Then
                InstanciaMover = CLng(Request.QueryString("Instancia"))
            End If
        End If

        Dim s As FSNServer.Solicitudes = FSNServer.Get_Object(GetType(FSNServer.Solicitudes))
        Dim result As DataTable = s.GetSolicitudesBuscador(lInstancia, idCampo, Usuario.Idioma, identificador, denominacion, fechaAltaDesde, fechaAltaHasta,
                                                           importeDesde, importeHasta, peticionario, tipo, estado, articulo, InstanciaMover)
        result = InsertarColumnaTextoEstado(result)
        InsertarEnCache(DataSourceCacheSolicitudes, result)

        Return result
    End Function
    ''' <summary>
    ''' Actualiza en el datatable de solicitudes la denominación del estado
    ''' </summary>
    ''' <param name="dt">datatable de solicitudes</param>
    ''' <returns>datatable de solicitudes con la denominación del estado actualizada</returns>
    ''' <remarks>Llamada desde:BuscarSolicitudes; Tiempo maximo:0</remarks>
    Private Function InsertarColumnaTextoEstado(ByVal dt As DataTable) As DataTable
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.BuscadorSolicitudes
        Dim columnName As String = "EST_DEN"
        If Not dt Is Nothing Then
            dt.Columns.Add(columnName, GetType(String))
            For Each row As DataRow In dt.Rows
                Select Case row("EST")
                    Case TipoEstadoSolic.Pendiente
                        row(columnName) = Textos(35)
                    Case TipoEstadoSolic.Guardada
                        row(columnName) = Textos(36)
                    Case TipoEstadoSolic.EnCurso
                        row(columnName) = Textos(37)
                    Case TipoEstadoSolic.Rechazada
                        row(columnName) = Textos(38)
                    Case TipoEstadoSolic.Anulada
                        row(columnName) = Textos(39)
                    Case TipoEstadoSolic.Enviada,
                    TipoEstadoSolic.Aprobada,
                    TipoEstadoSolic.SCPendiente,
                    TipoEstadoSolic.SCAprobada,
                    TipoEstadoSolic.SCRechazada,
                    TipoEstadoSolic.SCAnulada
                        row(columnName) = Textos(40)
                    Case TipoEstadoSolic.SCCerrada
                        row(columnName) = Textos(41)
                End Select
            Next
        End If
        Return dt
    End Function
    ''' <summary>
    ''' Pone el texto correspondiente en la etiqueta lblProcesando.
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>      
    ''' <remarks>Llamada desde: El prerender de la etiqueta. Tiempo máximo: 0sg.</remarks>
    Protected Sub LblProcesando_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, Label).Text = Textos(34)
    End Sub
#End Region
#Region "Botonera"
    ''' <summary>
    ''' Carga el grid de solicitudes con las instancias de la solicitud/es origen
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>  
    ''' <remarks>Llamada desde:sistema; Tiempo máximo:0,3</remarks>
    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        actualizargrid()
    End Sub
    ''' <summary>
    ''' Carga el grid de lineas con la/s linea/s de la instancia origen
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>  
    ''' <remarks>Llamada desde:sistema; Tiempo máximo:0,3</remarks>
    Private Sub btnSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Dim mostrarMensaje As Boolean = False
        Dim rows As GridControls.SelectedRowCollection = wdgSolicitudes.Behaviors.Selection.SelectedRows

        If rows.Count > 0 Then
            If IsNumeric(rows(0).Items.FindItemByKey("ID").Value) Then
                pnlStep1.Visible = False
                pnlStep2.Visible = True
                btnSiguiente.Visible = False
                btnAtras.Visible = True
                btnAceptar2.Visible = True

                BuscarLineas(rows(0).Items.FindItemByKey("ID").Value _
                             , rows(0).Items.FindItemByKey("DEN").Value _
                             , rows(0).Items.FindItemByKey("IDDESGLOSEORIGEN").Value _
                             , rows(0).Items.FindItemByKey("IDVINCULADO").Value)

                lblDescSolicitud.Text = CStr(rows(0).Items.FindItemByKey("ID").Value) _
                    & " - " & CStr(rows(0).Items.FindItemByKey("DEN").Value)
                lblDescSolicitud.ToolTip = Me.lblDescSolicitud.Text

                CargarGridLineas()
            Else
                mostrarMensaje = True
            End If
        Else
            mostrarMensaje = True
        End If

        If mostrarMensaje Then
            If (Not ClientScript.IsStartupScriptRegistered("validacion")) Then
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "validacion", "mensaje_validacion();", True)
            End If
        End If
    End Sub
    ''' <summary>
    ''' Mostrar el panel de "buscador solicitudes"
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>  
    ''' <remarks>Llamada desde:sistema; Tiempo máximo:0</remarks>
    Private Sub btnAtras_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAtras.Click
        pnlStep1.Visible = True
        pnlStep2.Visible = False
        btnSiguiente.Visible = True
        btnAtras.Visible = False
        btnAceptar2.Visible = False
    End Sub
    Public ReadOnly Property esImportacion
        Get
            Return (Not Request("Importar") Is Nothing AndAlso Request("Importar") = 1)
        End Get
    End Property
    Public ReadOnly Property esTipoFactura
        Get
            Return (Not Request("TipoSolicitud") Is Nothing AndAlso CInt(Request("TipoSolicitud")) = TiposDeDatos.TipoDeSolicitud.Factura)
        End Get
    End Property

#End Region
#Region "wdgLineas"
    ''' <summary>
    ''' Carga la grid con los datos
    ''' </summary>       
    ''' <remarks>Llamada desde=page_load    btnSiguiente_click;Tiempo máximo=0,3seg.</remarks>
    Private Sub CargarGridLineas()
        wdgLineas.Rows.Clear()
        wdgLineas.Columns.Clear()

        Dim dataSource As DataTable
        If Not CType(HttpContext.Current.Cache.Item(DataSourceCacheLineas), DataSet) Is Nothing Then
            dataSource = CType(HttpContext.Current.Cache.Item(DataSourceCacheLineas), DataSet).Tables(1)
        Else
            dataSource = LineasLayout()
        End If
        wdgLineas.DataSource = dataSource
        CrearCol_Lineas()
        wdgLineas.DataBind()
        Conf_GridLineas()
    End Sub
    Private Sub CrearCol_Lineas()
        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
        Dim nombre As String

        For i As Integer = 0 To wdgLineas.DataSource.Columns.Count - 1
            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            nombre = wdgLineas.DataSource.Columns(i).ToString
            If ((nombre <> "VALOR_5159") Or (nombre <> "VALORMOSTRAR_5162") Or (nombre <> "VALORMOSTRAR_5163") Or (nombre <> "VALORMOSTRAR_781")) Then
                With campoGrid
                    .Key = nombre
                    .DataFieldName = nombre
                    .Header.Text = nombre
                End With
                wdgLineas.Columns.Add(campoGrid)
            Else
                AnyadirColumnaU(nombre, wdgLineas)
            End If
        Next
    End Sub
    ''' <summary>
    ''' Inicializa el dataset de Lineas
    ''' </summary>
    ''' <returns>dataset de Lineas</returns>
    ''' <remarks>Llamada desde: wdgLineas_InitializeDataSource; Tiempo maximo: 0</remarks>
    Private Function LineasLayout() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("LINEA", GetType(Integer))
        Return dt
    End Function
    ''' <summary>
    ''' Se ejecuta una vez, al cargarse la grid
    ''' </summary>
    ''' <remarks>Llamada desde: Al cargarse la grid; Tiempo máximo: 0 sg.</remarks>
    Private Sub Conf_GridLineas()
        Dim dataEstructura As DataTable
        If Not CType(HttpContext.Current.Cache.Item(DataSourceCacheLineas), DataSet) Is Nothing Then
            dataEstructura = CType(HttpContext.Current.Cache.Item(DataSourceCacheLineas), DataSet).Tables(0)
        Else
            Exit Sub
        End If

        If (Request("DatosLinea") Is Nothing) Then wdgLineas.Height = Unit.Pixel(320)

        Dim dRows() As DataRow
        For Each field As Infragistics.Web.UI.GridControls.GridField In wdgLineas.Columns
            Dim Spli() As String = field.Key.ToString.Split("_") 'VALOR_CampoOrigen
            field.CssClass = "headerNoWrap"
            If Not (InStr(field.Key, "VALORMOSTRAR_") > 0 OrElse InStr(field.Key, "VALOR_") > 0) Then
                field.Hidden = True
            Else
                dRows = dataEstructura.Select("CAMPO_ORIGEN=" + Spli(1))

                If InStr(field.Key, "VALORMOSTRAR_") > 0 Then
                    field.Hidden = False
                    field.Header.Text = dRows(0)("DEN")
                    field.Width = Unit.Pixel(100)
                ElseIf (InStr(field.Key, "VALOR_") > 0) AndAlso (dRows(0)("VISIBLE") = 1) Then
                    field.Hidden = False
                    field.Header.Text = dRows(0)("DEN")
                    If dRows(0)("TIPO") = 3 Then
                        field.Header.Text = "<img src='" & ConfigurationManager.AppSettings("ruta") & "/images/sumatorio.gif' />"
                    End If

                    field.Width = Unit.Pixel(100)
                Else
                    field.Hidden = True
                End If
            End If
        Next
    End Sub
    ''' <summary>
    ''' Se ejecuta cada vez que se inicializa una fila del grid
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>LLamada desde: Al cargar la grid con datos; Tiempo máximo: 0 sg</remarks>
    Private Sub wdgLineas_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles wdgLineas.InitializeRow
        Dim dataEstructura As DataTable
        If Not CType(HttpContext.Current.Cache.Item(DataSourceCacheLineas), DataSet) Is Nothing Then
            dataEstructura = CType(HttpContext.Current.Cache.Item(DataSourceCacheLineas), DataSet).Tables(0)
        Else
            Exit Sub
        End If

        Dim dRows() As DataRow
        For i = 0 To wdgLineas.Columns.Count - 1
            Dim Spli() As String = e.Row.Items.Item(i).Column.Key.ToString.Split("_")

            If Not IsNothing(e.Row.Items.FindItemByKey(e.Row.Items.Item(i).Column.Key).Value) Then
                If (Spli.Length > 1) Then
                    dRows = dataEstructura.Select("CAMPO_ORIGEN=" + Spli(1))
                    If (Not IsDBNull(e.Row.Items.FindItemByKey(e.Row.Items.Item(i).Column.Key).Value)) Then
                        If (dRows(0)("SUBTIPO") = CStr(TiposDeDatos.TipoGeneral.TipoNumerico)) AndAlso Spli(0) = "VALORMOSTRAR" Then
                            e.Row.Items.FindItemByKey(e.Row.Items.Item(i).Column.Key).Text = modUtilidades.FormatNumber(e.Row.Items.FindItemByKey(e.Row.Items.Item(i).Column.Key).Value, FSNUser.NumberFormat)
                        ElseIf (dRows(0)("SUBTIPO") = CStr(TiposDeDatos.TipoGeneral.TipoFecha)) Then
                            e.Row.Items.FindItemByKey(e.Row.Items.Item(i).Column.Key).Text = modUtilidades.FormatDate(e.Row.Items.FindItemByKey(e.Row.Items.Item(i).Column.Key).Value, FSNUser.DateFormat)
                        End If
                    End If
                End If
            End If
        Next
    End Sub
    ''' <summary>
    ''' Esta función carga el dataset de lineas con las lineas del desglose origen. Luego lo mete en cache.
    ''' </summary>
    ''' <param name="lIdInstancia">Instancia Origen</param>
    ''' <param name="sTituloInstancia">Descripción de Instancia Origen</param>
    ''' <param name="lIdDesgloseOrigen">Desglose Origen</param>
    ''' <param name="lIdVinculado">Id de la vinculación (DESGLOSE_VINCULADO.ID)</param>   
    ''' <param name="iLinea">Linea Origen. Solo se usa para sacar el detalle de la linea oriegen de una vinculación.</param>
    ''' <returns>dataset de lineas</returns>
    ''' <remarks>Llamada desde: btnSiguiente_Click; Tiempo máximo=0,3seg.</remarks>
    Private Function BuscarLineas(ByVal lIdInstancia As Long, ByVal sTituloInstancia As String, ByVal lIdDesgloseOrigen As Long,
                Optional ByVal lIdVinculado As Long = 0, Optional ByVal iLinea As Integer = 0) As DataSet
        Dim oInstan As FSNServer.Instancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        oInstan.ID = lIdInstancia

        Dim ds As DataSet
        If iLinea = 0 Then
            ds = oInstan.GetLineasAVincular(lIdDesgloseOrigen, FSNUser.Idioma, lIdVinculado)
        Else
            ds = oInstan.GetLineaVinculada(lIdDesgloseOrigen, iLinea, FSNUser.Idioma)
        End If

        Dim dsLineas As DataSet = ConstruirTableLineas(ds)
        RellenarTableLineas(lIdInstancia, sTituloInstancia, lIdDesgloseOrigen, ds, dsLineas, iLinea)

        InsertarEnCache(DataSourceCacheLineas, dsLineas)

        Return dsLineas
    End Function
    ''' <summary>
    ''' Esta función carga las columnas del dataset de lineas. Las columnas son dinamicas pq son las del desglose origen en el momento
    ''' q se dio de alta la instancia origen.
    ''' </summary>
    ''' <param name="ds">dataset con los datos del desglose origen</param>
    ''' <returns>dataset de lineas</returns>
    ''' <remarks>Llamada desde: BuscarLineas; Tiempo máximo=0,1seg.</remarks>
    Private Function ConstruirTableLineas(ByVal ds As DataSet) As DataSet
        Dim dsLineas As New DataSet
        Dim dtDens As New DataTable
        dtDens.Columns.Add("CAMPO_ORIGEN", System.Type.GetType("System.Int32"))
        dtDens.Columns.Add("VISIBLE", System.Type.GetType("System.Int16"))
        dtDens.Columns.Add("DEN", System.Type.GetType("System.String"))
        dtDens.Columns.Add("SUBTIPO", System.Type.GetType("System.Int16"))
        dtDens.Columns.Add("INTRO", System.Type.GetType("System.Int16"))
        dtDens.Columns.Add("TIPO_CAMPO_GS", System.Type.GetType("System.Int16"))
        dtDens.Columns.Add("TIPO", System.Type.GetType("System.Int16"))

        Dim dtColumnas As New DataTable
        dtColumnas.Columns.Add("DENSOL", System.Type.GetType("System.String"))
        dtColumnas.Columns.Add("INSTANCIAORIGEN", System.Type.GetType("System.Int32"))
        dtColumnas.Columns.Add("DESGLOSEORIGEN", System.Type.GetType("System.Int32"))
        dtColumnas.Columns.Add("LINEAORIGEN", System.Type.GetType("System.Int32"))

        Dim sColName As String
        Dim lIdCol As String
        Dim sColumnasValorYDescripcion As String = ds.Tables(4).Rows(0).ItemArray(0)
        For Each row As DataRow In ds.Tables(0).Rows
            If Not DBNullToSomething(row.Item("CAMPO_HIJO_VINCULADO")) Is Nothing Then
                lIdCol = CLng(row.Item("CAMPO_HIJO_VINCULADO").ToString)
            Else
                lIdCol = CLng(row.Item("CAMPO_ORIGEN").ToString)
            End If

            sColName = "VALOR_" & lIdCol.ToString

            If row.Item("INTRO").ToString = "0" Then
                Select Case row.Item("SUBTIPO").ToString
                    Case TiposDeDatos.TipoGeneral.TipoBoolean
                        dtColumnas.Columns.Add(sColName, System.Type.GetType("System.Int16"))
                    Case TiposDeDatos.TipoGeneral.TipoFecha
                        'dtColumnas.Columns.Add(sColName, System.Type.GetType("System.DateTime"))
                        dtColumnas.Columns.Add(sColName, System.Type.GetType("System.String"))
                    Case TiposDeDatos.TipoGeneral.TipoNumerico
                        dtColumnas.Columns.Add(sColName, System.Type.GetType("System.Double"))
                    Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto _
                    , TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio
                        dtColumnas.Columns.Add(sColName, System.Type.GetType("System.String"))
                    Case TiposDeDatos.TipoGeneral.SinTipo
                        dtColumnas.Columns.Add(sColName, System.Type.GetType("System.String"))
                    Case TiposDeDatos.TipoGeneral.TipoArchivo
                        dtColumnas.Columns.Add(sColName, System.Type.GetType("System.String"))
                End Select
            Else
                If row.Item("SUBTIPO").ToString = TiposDeDatos.TipoGeneral.TipoString _
                OrElse row.Item("SUBTIPO").ToString = TiposDeDatos.TipoGeneral.TipoTextoCorto _
                OrElse row.Item("SUBTIPO").ToString = TiposDeDatos.TipoGeneral.TipoTextoMedio Then
                    dtColumnas.Columns.Add(sColName, System.Type.GetType("System.Int16"))

                    sColName = "VALORMOSTRAR_" & lIdCol
                    dtColumnas.Columns.Add(sColName, System.Type.GetType("System.String"))
                ElseIf row.Item("SUBTIPO").ToString = TiposDeDatos.TipoGeneral.TipoNumerico Then
                    dtColumnas.Columns.Add(sColName, System.Type.GetType("System.Double"))
                Else
                    dtColumnas.Columns.Add(sColName, System.Type.GetType("System.String"))
                End If
            End If

            If (InStr(sColumnasValorYDescripcion, "," & row.Item("TIPO_CAMPO_GS") & ",")) _
            OrElse (row.Item("TIPO") = 6) _
            OrElse (row.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoNumerico) _
            OrElse (row.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoBoolean) _
            OrElse (row.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoArchivo) Then
                sColName = "VALORMOSTRAR_" & lIdCol
                dtColumnas.Columns.Add(sColName, System.Type.GetType("System.String"))
            End If
        Next

        dsLineas.Tables.Add(dtDens)
        dsLineas.Tables.Add(dtColumnas)

        Return dsLineas
    End Function
    ''' <summary>
    ''' Esta función carga las datos del dataset de lineas. 
    ''' Las columnas son dinamicas pq son las del desglose origen en el momento q se dio de alta la instancia origen.
    ''' </summary>
    ''' <param name="lIdInstancia">Instancia Origen</param>
    ''' <param name="sTituloInstancia">Descripción de Instancia Origen</param>
    ''' <param name="dsBd">dataset con los datos del desglose origen</param>
    ''' <param name="dsLineas">ByRef. dataset de lineas</param>
    ''' <remarks>Llamada desde: BuscarLineas; Tiempo máximo=0,2seg.</remarks>
    Private Sub RellenarTableLineas(ByVal lIdInstancia As Long, ByVal sTituloInstancia As String, ByVal lIdDesgloseOrigen As Long, ByVal dsBd As DataSet, ByRef dsLineas As DataSet, Optional ByVal iLineaDetalle As Integer = 0)
        Dim dtNewRowDens As DataRow
        Dim dtNewRow As DataRow = Nothing
        Dim dRows() As DataRow
        Dim bHayDatos As Boolean = False
        Dim sColName As String
        Dim lIdCol As String
        Dim i As Integer
        Dim sDenominacion As String = Nothing
        Dim sIdi As String = Me.Idioma
        Dim iNivel As Integer
        Dim lIdPresup As Integer
        Dim dPorcent As Decimal
        Dim arrPresupuestos() As String
        Dim oPresup As String
        Dim arrPresup(2) As String
        Dim iContadorPres As Integer
        Dim oPres1 As FSNServer.PresProyectosNivel1
        Dim oPres2 As FSNServer.PresContablesNivel1
        Dim oPres3 As FSNServer.PresConceptos3Nivel1
        Dim oPres4 As FSNServer.PresConceptos4Nivel1
        Dim oDSPres As DataSet = Nothing
        Dim oProve As FSNServer.Proveedor
        Dim sCodPais As String = Nothing
        Dim sColumnasValorYDescripcion As String = dsBd.Tables(4).Rows(0).ItemArray(0)
        For Each rowEstructura As DataRow In dsBd.Tables(0).Rows
            dtNewRowDens = dsLineas.Tables(0).NewRow

            If Not DBNullToSomething(rowEstructura.Item("CAMPO_HIJO_VINCULADO")) Is Nothing Then
                dtNewRowDens.Item("CAMPO_ORIGEN") = rowEstructura.Item("CAMPO_HIJO_VINCULADO")
            Else
                dtNewRowDens.Item("CAMPO_ORIGEN") = rowEstructura.Item("CAMPO_ORIGEN")
            End If
            dtNewRowDens.Item("VISIBLE") = Not (InStr(sColumnasValorYDescripcion, "," & rowEstructura.Item("TIPO_CAMPO_GS") & ",") > 0)
            dtNewRowDens.Item("VISIBLE") = dtNewRowDens.Item("VISIBLE") AndAlso Not (rowEstructura.Item("TIPO") = 6)
            dtNewRowDens.Item("VISIBLE") = dtNewRowDens.Item("VISIBLE") AndAlso Not (rowEstructura.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoNumerico)
            dtNewRowDens.Item("VISIBLE") = dtNewRowDens.Item("VISIBLE") AndAlso Not (rowEstructura.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoBoolean)
            dtNewRowDens.Item("VISIBLE") = dtNewRowDens.Item("VISIBLE") AndAlso Not (rowEstructura.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoArchivo)
            dtNewRowDens.Item("VISIBLE") = dtNewRowDens.Item("VISIBLE") _
                AndAlso Not ((rowEstructura.Item("INTRO").ToString() = "1") AndAlso
                    ((rowEstructura.Item("SUBTIPO").ToString = TiposDeDatos.TipoGeneral.TipoString) _
                    OrElse (rowEstructura.Item("SUBTIPO").ToString = TiposDeDatos.TipoGeneral.TipoTextoCorto) _
                    OrElse (rowEstructura.Item("SUBTIPO").ToString = TiposDeDatos.TipoGeneral.TipoTextoMedio)))
            dtNewRowDens.Item("DEN") = rowEstructura.Item("DEN")
            dtNewRowDens.Item("SUBTIPO") = rowEstructura.Item("SUBTIPO")
            dtNewRowDens.Item("INTRO") = rowEstructura.Item("INTRO")
            dtNewRowDens.Item("TIPO_CAMPO_GS") = rowEstructura.Item("TIPO_CAMPO_GS")
            dtNewRowDens.Item("TIPO") = rowEstructura.Item("TIPO")

            dsLineas.Tables(0).Rows.Add(dtNewRowDens)
        Next

        Dim iNumLineas = dsBd.Tables(3).Rows(0).Item("LINEAS")
        If iLineaDetalle = 0 Then iLineaDetalle = 1

        For iLinea As Integer = iLineaDetalle To iNumLineas
            bHayDatos = False

            For Each rowEstructura As DataRow In dsBd.Tables(0).Rows
                dRows = dsBd.Tables(1).Select("LINEA= " + iLinea.ToString + " AND CAMPO_HIJO=" + rowEstructura.Item("CAMPO_HIJO").ToString)

                For Each rowDato As DataRow In dRows
                    If Not bHayDatos Then 'Por si hay huecos en las lineas
                        dtNewRow = dsLineas.Tables(1).NewRow

                        dtNewRow.Item("DENSOL") = CStr(lIdInstancia) & " - " & sTituloInstancia

                        dtNewRow.Item("INSTANCIAORIGEN") = lIdInstancia
                        dtNewRow.Item("DESGLOSEORIGEN") = lIdDesgloseOrigen
                        dtNewRow.Item("LINEAORIGEN") = iLinea

                        bHayDatos = True
                    End If

                    If Not DBNullToSomething(rowEstructura.Item("CAMPO_HIJO_VINCULADO")) Is Nothing Then
                        lIdCol = CLng(rowEstructura.Item("CAMPO_HIJO_VINCULADO").ToString)
                    Else
                        lIdCol = CLng(rowEstructura.Item("CAMPO_ORIGEN").ToString)
                    End If
                    sColName = "VALOR_" & lIdCol.ToString

                    If rowEstructura.Item("INTRO").ToString = "0" Then
                        Select Case rowEstructura.Item("SUBTIPO").ToString
                            Case TiposDeDatos.TipoGeneral.TipoBoolean
                                dtNewRow.Item(sColName) = rowDato.Item("VALOR_BOOL")
                            Case TiposDeDatos.TipoGeneral.TipoFecha
                                dtNewRow.Item(sColName) = rowDato.Item("VALOR_FEC")
                            Case TiposDeDatos.TipoGeneral.TipoNumerico
                                dtNewRow.Item(sColName) = rowDato.Item("VALOR_NUM")
                            Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto _
                            , TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio
                                dtNewRow.Item(sColName) = rowDato.Item("VALOR_TEXT")
                            Case TiposDeDatos.TipoGeneral.SinTipo
                                dtNewRow.Item(sColName) = rowDato.Item("VALOR_TEXT")
                            Case TiposDeDatos.TipoGeneral.TipoArchivo
                                dtNewRow.Item(sColName) = rowDato.Item("VALOR_TEXT")
                        End Select
                    Else
                        If rowEstructura.Item("SUBTIPO").ToString = TiposDeDatos.TipoGeneral.TipoString _
                        OrElse rowEstructura.Item("SUBTIPO").ToString = TiposDeDatos.TipoGeneral.TipoTextoCorto _
                        OrElse rowEstructura.Item("SUBTIPO").ToString = TiposDeDatos.TipoGeneral.TipoTextoMedio Then
                            dtNewRow.Item(sColName) = rowDato.Item("VALOR_NUM")

                            sColName = "VALORMOSTRAR_" & lIdCol.ToString
                            dtNewRow.Item(sColName) = rowDato.Item("VALOR_TEXT")
                        ElseIf rowEstructura.Item("SUBTIPO").ToString = TiposDeDatos.TipoGeneral.TipoNumerico Then
                            dtNewRow.Item(sColName) = rowDato.Item("VALOR_NUM")
                        Else
                            dtNewRow.Item(sColName) = rowDato.Item("VALOR_FEC")
                        End If
                    End If

                    If (InStr(sColumnasValorYDescripcion, "," & rowEstructura.Item("TIPO_CAMPO_GS") & ",")) Then
                        sColName = "VALORMOSTRAR_" & lIdCol.ToString

                        If IsDBNull(rowDato.Item("VALOR_TEXT")) Then
                            dtNewRow.Item(sColName) = ""
                        Else
                            Select Case rowEstructura.Item("TIPO_CAMPO_GS")
                                Case TiposDeDatos.TipoCampoGS.Material
                                    Dim sMat As String = DBNullToStr(rowDato.Item("VALOR_TEXT"))
                                    Dim lLongCod As Integer
                                    Dim arrMat(4) As String
                                    Dim oGMN1s As FSNServer.GruposMatNivel1
                                    Dim oGMN1 As FSNServer.GrupoMatNivel1
                                    Dim oGMN2 As FSNServer.GrupoMatNivel2
                                    Dim oGMN3 As FSNServer.GrupoMatNivel3

                                    For i = 1 To 4
                                        arrMat(i) = ""
                                    Next i
                                    i = 1
                                    While Trim(sMat) <> ""
                                        Select Case i
                                            Case 1
                                                lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                                            Case 2
                                                lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                                            Case 3
                                                lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                                            Case 4
                                                lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN4
                                        End Select
                                        arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
                                        sMat = Mid(sMat, lLongCod + 1)
                                        i = i + 1
                                    End While

                                    If arrMat(4) <> "" Then
                                        If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                            sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(rowDato.Item("VALOR_TEXT_DEN"), sIdi)
                                        Else
                                            oGMN3 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel3))
                                            oGMN3.GMN1Cod = arrMat(1)
                                            oGMN3.GMN2Cod = arrMat(2)
                                            oGMN3.Cod = arrMat(3)
                                            oGMN3.CargarTodosLosGruposMatDesde(1, sIdi, arrMat(4), , True)
                                            sDenominacion = oGMN3.GruposMatNivel4.Item(arrMat(4)).Den
                                            oGMN3 = Nothing
                                        End If
                                        dtNewRow.Item(sColName) = arrMat(4) + " - " + sDenominacion
                                    ElseIf arrMat(3) <> "" Then
                                        If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                            sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(rowDato.Item("VALOR_TEXT_DEN"), sIdi)
                                        Else
                                            oGMN2 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel2))
                                            oGMN2.GMN1Cod = arrMat(1)
                                            oGMN2.Cod = arrMat(2)
                                            oGMN2.CargarTodosLosGruposMatDesde(1, sIdi, arrMat(3), , True)
                                            sDenominacion = oGMN2.GruposMatNivel3.Item(arrMat(3)).Den
                                            oGMN2 = Nothing
                                        End If
                                        dtNewRow.Item(sColName) = arrMat(3) + " - " + sDenominacion
                                    ElseIf arrMat(2) <> "" Then
                                        If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                            sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(rowDato.Item("VALOR_TEXT_DEN"), sIdi)
                                        Else
                                            oGMN1 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel1))
                                            oGMN1.Cod = arrMat(1)
                                            oGMN1.CargarTodosLosGruposMatDesde(1, sIdi, arrMat(2), , True)
                                            sDenominacion = oGMN1.GruposMatNivel2.Item(arrMat(2)).Den
                                            oGMN1 = Nothing
                                        End If
                                        dtNewRow.Item(sColName) = arrMat(2) + " - " + sDenominacion
                                    ElseIf arrMat(1) <> "" Then
                                        If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                            sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(rowDato.Item("VALOR_TEXT_DEN"), sIdi)
                                        Else
                                            oGMN1s = FSNServer.Get_Object(GetType(FSNServer.GruposMatNivel1))
                                            oGMN1s.CargarTodosLosGruposMatDesde(1, sIdi, arrMat(1), , , True)
                                            sDenominacion = oGMN1s.Item(arrMat(1)).Den
                                            oGMN1 = Nothing
                                        End If
                                        dtNewRow.Item(sColName) = arrMat(1) + " - " + sDenominacion
                                    End If
                                Case TiposDeDatos.TipoCampoGS.Unidad
                                    If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                        sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(rowDato.Item("VALOR_TEXT_DEN"), sIdi)
                                    Else
                                        Dim oUnis As FSNServer.Unidades
                                        oUnis = FSNServer.Get_Object(GetType(FSNServer.Unidades))
                                        oUnis.LoadData(sIdi, rowDato.Item("VALOR_TEXT"), , False)
                                        sDenominacion = oUnis.Data.Tables(0).Rows(0).Item("DEN")
                                    End If
                                    dtNewRow.Item(sColName) = sDenominacion
                                Case TiposDeDatos.TipoCampoGS.Dest
                                    If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                        sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(rowDato.Item("VALOR_TEXT_DEN"), sIdi)
                                    Else
                                        Dim oDests As FSNServer.Destinos
                                        oDests = FSNServer.Get_Object(GetType(FSNServer.Destinos))
                                        oDests.LoadData(sIdi, FSNUser.CodPersona, rowDato.Item("VALOR_TEXT"))
                                        sDenominacion = oDests.Data.Tables(0).Rows(0).Item("DEN")
                                    End If
                                    dtNewRow.Item(sColName) = sDenominacion
                                Case TiposDeDatos.TipoCampoGS.FormaPago
                                    If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                        sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(rowDato.Item("VALOR_TEXT_DEN"), sIdi)
                                    Else
                                        Dim oPags As FSNServer.FormasPago
                                        oPags = FSNServer.Get_Object(GetType(FSNServer.FormasPago))
                                        oPags.LoadData(sIdi, rowDato.Item("VALOR_TEXT"))
                                        sDenominacion = oPags.Data.Tables(0).Rows(0).Item("DEN")
                                    End If
                                    dtNewRow.Item(sColName) = sDenominacion
                                Case TiposDeDatos.TipoCampoGS.Proveedor
                                    If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                        sDenominacion = rowDato.Item("VALOR_TEXT_DEN")
                                    Else
                                        oProve = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
                                        oProve.Cod = rowDato.Item("VALOR_TEXT")
                                        oProve.Load(sIdi)
                                        sDenominacion = oProve.Den.ToString.Replace("'", "")
                                    End If
                                    dtNewRow.Item(sColName) = rowDato.Item("VALOR_TEXT") + " - " + sDenominacion.Replace("'", "")
                                Case TiposDeDatos.TipoCampoGS.ProveedorAdj
                                    If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                        sDenominacion = rowDato.Item("VALOR_TEXT_DEN")
                                    Else
                                        oProve = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
                                        oProve.Cod = rowDato.Item("VALOR_TEXT")
                                        oProve.Load(sIdi)
                                        sDenominacion = oProve.Den.ToString.Replace("'", "")
                                    End If
                                    dtNewRow.Item(sColName) = rowDato.Item("VALOR_TEXT") + " - " + sDenominacion.Replace("'", "")
                                Case TiposDeDatos.TipoCampoGS.PRES1, TiposDeDatos.TipoCampoGS.Pres2, TiposDeDatos.TipoCampoGS.Pres3, TiposDeDatos.TipoCampoGS.Pres4
                                    Dim arrDenominaciones() As String = Nothing
                                    If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                        arrDenominaciones = rowDato.Item("VALOR_TEXT_DEN").ToString().Split("#")
                                    End If
                                    sDenominacion = ""
                                    iContadorPres = 0
                                    arrPresupuestos = rowDato.Item("VALOR_TEXT").split("#")
                                    For Each oPresup In arrPresupuestos
                                        iContadorPres = iContadorPres + 1
                                        arrPresup = oPresup.Split("_")
                                        iNivel = arrPresup(0)
                                        lIdPresup = arrPresup(1)
                                        'Los presupuestos siempre llevan el porcentaje como 0.xx salvo cuando es 100% que llevan 1
                                        dPorcent = Numero(arrPresup(2), ".", "")
                                        If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                            sDenominacion = arrDenominaciones(iContadorPres - 1) & " (" & dPorcent.ToString("0.00%", FSNUser.NumberFormat) & "); " & sDenominacion
                                        Else
                                            Select Case rowEstructura.Item("TIPO_CAMPO_GS")
                                                Case TiposDeDatos.TipoCampoGS.PRES1
                                                    oPres1 = FSNServer.Get_Object(GetType(FSNServer.PresProyectosNivel1))
                                                    Select Case iNivel
                                                        Case 1
                                                            oPres1.LoadData(lIdPresup)
                                                        Case 2
                                                            oPres1.LoadData(Nothing, lIdPresup)
                                                        Case 3
                                                            oPres1.LoadData(Nothing, Nothing, lIdPresup)
                                                        Case 4
                                                            oPres1.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                                                    End Select
                                                    oDSPres = oPres1.Data
                                                    oPres1 = Nothing
                                                Case TiposDeDatos.TipoCampoGS.Pres2
                                                    oPres2 = FSNServer.Get_Object(GetType(FSNServer.PresContablesNivel1))
                                                    Select Case iNivel
                                                        Case 1
                                                            oPres2.LoadData(lIdPresup)
                                                        Case 2
                                                            oPres2.LoadData(Nothing, lIdPresup)
                                                        Case 3
                                                            oPres2.LoadData(Nothing, Nothing, lIdPresup)
                                                        Case 4
                                                            oPres2.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                                                    End Select
                                                    oDSPres = oPres2.Data
                                                    oPres2 = Nothing
                                                Case TiposDeDatos.TipoCampoGS.Pres3
                                                    oPres3 = FSNServer.Get_Object(GetType(FSNServer.PresConceptos3Nivel1))
                                                    Select Case iNivel
                                                        Case 1
                                                            oPres3.LoadData(lIdPresup)
                                                        Case 2
                                                            oPres3.LoadData(Nothing, lIdPresup)
                                                        Case 3
                                                            oPres3.LoadData(Nothing, Nothing, lIdPresup)
                                                        Case 4
                                                            oPres3.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                                                    End Select
                                                    oDSPres = oPres3.Data
                                                    oPres3 = Nothing
                                                Case TiposDeDatos.TipoCampoGS.Pres4
                                                    oPres4 = FSNServer.Get_Object(GetType(FSNServer.PresConceptos4Nivel1))
                                                    Select Case iNivel
                                                        Case 1
                                                            oPres4.LoadData(lIdPresup)
                                                        Case 2
                                                            oPres4.LoadData(Nothing, lIdPresup)
                                                        Case 3
                                                            oPres4.LoadData(Nothing, Nothing, lIdPresup)
                                                        Case 4
                                                            oPres4.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                                                    End Select
                                                    oDSPres = oPres4.Data
                                                    oPres4 = Nothing
                                            End Select

                                            If Not oDSPres Is Nothing Then
                                                If oDSPres.Tables(0).Rows.Count > 0 Then
                                                    With oDSPres.Tables(0).Rows(0)
                                                        Dim sDenPresup As String = Nothing
                                                        For i = 4 To 1 Step -1
                                                            'PRESi:
                                                            If Not IsDBNull(.Item("PRES" & i)) Then
                                                                sDenPresup = " - " & .Item("PRES" & i).ToString
                                                            End If
                                                        Next
                                                        If Not oDSPres.Tables(0).Columns("ANYO") Is Nothing Then
                                                            sDenPresup = .Item("ANYO").ToString & sDenPresup
                                                        End If
                                                        If sDenominacion <> "" Then sDenominacion = "#" & sDenominacion
                                                        sDenominacion = sDenPresup & sDenominacion
                                                    End With
                                                End If
                                            End If
                                        End If
                                    Next
                                    dtNewRow.Item(sColName) = sDenominacion
                                Case TiposDeDatos.TipoCampoGS.Moneda
                                    If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                        sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(rowDato.Item("VALOR_TEXT_DEN"), sIdi)
                                    Else
                                        Dim oMons As FSNServer.Monedas
                                        oMons = FSNServer.Get_Object(GetType(FSNServer.Monedas))
                                        oMons.LoadData(sIdi, rowDato.Item("VALOR_TEXT"))
                                        sDenominacion = oMons.Data.Tables(0).Rows(0).Item("DEN")
                                    End If
                                    dtNewRow.Item(sColName) = sDenominacion
                                Case TiposDeDatos.TipoCampoGS.Pais
                                    If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                        sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(rowDato.Item("VALOR_TEXT_DEN"), sIdi)
                                    Else
                                        Dim oPaises As FSNServer.Paises
                                        oPaises = FSNServer.Get_Object(GetType(FSNServer.Paises))
                                        oPaises.LoadData(sIdi, rowDato.Item("VALOR_TEXT"))
                                        sDenominacion = oPaises.Data.Tables(0).Rows(0).Item("DEN")
                                    End If
                                    dtNewRow.Item(sColName) = sDenominacion
                                    sCodPais = rowDato.Item("VALOR_TEXT")
                                Case TiposDeDatos.TipoCampoGS.Provincia
                                    If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                        sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(rowDato.Item("VALOR_TEXT_DEN"), sIdi)
                                    Else
                                        Dim oProvis As FSNServer.Provincias
                                        oProvis = FSNServer.Get_Object(GetType(FSNServer.Provincias))
                                        oProvis.Pais = sCodPais
                                        oProvis.LoadData(sIdi, rowDato.Item("VALOR_TEXT"))
                                        sDenominacion = oProvis.Data.Tables(0).Rows(0).Item("DEN")
                                    End If
                                    dtNewRow.Item(sColName) = sDenominacion
                                Case TiposDeDatos.TipoCampoGS.Persona
                                    If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                        sDenominacion = DBNullToStr(rowDato.Item("VALOR_TEXT_DEN"))
                                    Else
                                        Dim oPersona As FSNServer.Persona = FSNServer.Get_Object(GetType(FSNServer.Persona))
                                        oPersona.LoadData(rowDato.Item("VALOR_TEXT").ToString)
                                        If Not oPersona.Codigo Is Nothing Then
                                            sDenominacion = oPersona.Denominacion(True)
                                        Else
                                            sDenominacion = ""
                                        End If
                                    End If
                                    dtNewRow.Item(sColName) = sDenominacion
                                Case TiposDeDatos.TipoCampoGS.UnidadOrganizativa
                                    dtNewRow.Item(sColName) = rowDato.Item("VALOR_TEXT")
                                Case TiposDeDatos.TipoCampoGS.Departamento
                                    If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                        dtNewRow.Item(sColName) = rowDato.Item("VALOR_TEXT") & " - " & rowDato.Item("VALOR_TEXT_DEN")
                                    Else
                                        Dim oDepartamento As FSNServer.Departamento
                                        oDepartamento = FSNServer.Get_Object(GetType(FSNServer.Departamento))
                                        oDepartamento.Cod = rowDato.Item("VALOR_TEXT")
                                        oDepartamento.LoadData()
                                        If oDepartamento.Data.Tables(0).Rows.Count > 0 Then
                                            sDenominacion = oDepartamento.Data.Tables(0).Rows(0).Item("DEN").ToString
                                            dtNewRow.Item(sColName) = oDepartamento.Data.Tables(0).Rows(0).Item("COD").ToString & " - " & sDenominacion
                                        End If
                                    End If
                                Case TiposDeDatos.TipoCampoGS.OrganizacionCompras
                                    If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                        sDenominacion = rowDato.Item("VALOR_TEXT_DEN")
                                    Else
                                        Dim oOrganizacionCompras As FSNServer.OrganizacionCompras
                                        oOrganizacionCompras = FSNServer.Get_Object(GetType(FSNServer.OrganizacionCompras))
                                        oOrganizacionCompras.Cod = rowDato.Item("VALOR_TEXT")
                                        oOrganizacionCompras.LoadData()
                                        If oOrganizacionCompras.Data.Tables(0).Rows.Count > 0 Then sDenominacion = oOrganizacionCompras.Data.Tables(0).Rows(0).Item("DEN").ToString
                                        oOrganizacionCompras = Nothing
                                    End If
                                    dtNewRow.Item(sColName) = rowDato.Item("VALOR_TEXT") & " - " & sDenominacion
                                Case TiposDeDatos.TipoCampoGS.Centro
                                    If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                        sDenominacion = rowDato.Item("VALOR_TEXT_DEN")
                                    Else
                                        Dim oCentro As FSNServer.Centro
                                        oCentro = FSNServer.Get_Object(GetType(FSNServer.Centro))
                                        oCentro.Cod = rowDato.Item("VALOR_TEXT")
                                        oCentro.LoadData()
                                        If oCentro.Data.Tables(0).Rows.Count > 0 Then sDenominacion = oCentro.Data.Tables(0).Rows(0).Item("DEN").ToString
                                        oCentro = Nothing
                                    End If
                                    dtNewRow.Item(sColName) = rowDato.Item("VALOR_TEXT") & " - " & sDenominacion
                                Case TiposDeDatos.TipoCampoGS.Almacen
                                    If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                        sDenominacion = rowDato.Item("VALOR_TEXT_DEN")
                                    Else
                                        Dim oAlmacen As FSNServer.Almacen
                                        oAlmacen = FSNServer.Get_Object(GetType(FSNServer.Almacen))
                                        oAlmacen.Id = rowDato.Item("VALOR_NUM")
                                        oAlmacen.LoadData()
                                        If oAlmacen.Data.Tables(0).Rows.Count > 0 Then
                                            sDenominacion = oAlmacen.Data.Tables(0).Rows(0).Item("COD") & " - " & oAlmacen.Data.Tables(0).Rows(0).Item("DEN").ToString
                                        End If
                                        oAlmacen = Nothing
                                    End If
                                    dtNewRow.Item(sColName) = sDenominacion
                                Case TiposDeDatos.TipoCampoGS.Empresa
                                    If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                        sDenominacion = rowDato.Item("VALOR_TEXT_DEN")
                                    Else
                                        Dim oEmpresa As FSNServer.Empresa
                                        oEmpresa = FSNServer.Get_Object(GetType(FSNServer.Empresa))
                                        oEmpresa.ID = rowDato.Item("VALOR_NUM")
                                        oEmpresa.Load(sIdi)
                                        If Not String.IsNullOrEmpty(oEmpresa.NIF) Then sDenominacion = oEmpresa.NIF & " - " & oEmpresa.Den
                                        oEmpresa = Nothing
                                    End If
                                    dtNewRow.Item(sColName) = sDenominacion
                                Case TiposDeDatos.TipoCampoGS.CentroCoste
                                    Dim sCentroCoste As String
                                    Dim bVerUON As Boolean = False

                                    If InStrRev(rowDato.Item("VALOR_TEXT"), "#") > 0 Then
                                        sCentroCoste = Right(rowDato.Item("VALOR_TEXT"), Len(rowDato.Item("VALOR_TEXT")) - InStrRev(rowDato.Item("VALOR_TEXT"), "#"))
                                    Else
                                        sCentroCoste = rowDato.Item("VALOR_TEXT")
                                    End If
                                    If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                        sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(rowDato.Item("VALOR_TEXT_DEN"), sIdi)
                                        sDenominacion = sCentroCoste & " - " & sDenominacion
                                    Else 'ya nos devuelve el codigo - denominación
                                        Dim oCentrosCoste As FSNServer.CentrosCoste
                                        oCentrosCoste = FSNServer.Get_Object(GetType(FSNServer.CentrosCoste))
                                        If Not IsDBNull(rowEstructura.Item("VER_UON")) Then
                                            bVerUON = rowEstructura.Item("VER_UON")
                                        End If
                                        oCentrosCoste.BuscarCentrosCoste(FSNUser.Cod, sIdi, sCentroCoste, bVerUON)
                                        If oCentrosCoste.Data.Tables(0).Rows.Count > 0 Then
                                            sDenominacion = oCentrosCoste.Data.Tables(0).Rows(0).Item("DEN").ToString
                                        End If
                                        oCentrosCoste = Nothing
                                    End If
                                    dtNewRow.Item(sColName) = sDenominacion
                                Case TiposDeDatos.TipoCampoGS.Partida
                                    If InStrRev(rowDato.Item("VALOR_TEXT"), "#") > 0 Then
                                        If InStrRev(rowDato.Item("VALOR_TEXT"), "|") > 0 Then 'si no está en el nivel 1, si no más abajo
                                            sDenominacion = Right(rowDato.Item("VALOR_TEXT"), Len(rowDato.Item("VALOR_TEXT")) - InStrRev(rowDato.Item("VALOR_TEXT"), "|"))
                                        Else
                                            sDenominacion = Right(rowDato.Item("VALOR_TEXT"), Len(rowDato.Item("VALOR_TEXT")) - InStrRev(rowDato.Item("VALOR_TEXT"), "#"))
                                        End If
                                    Else
                                        sDenominacion = rowDato.Item("VALOR_TEXT")
                                    End If
                                    If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                        sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(rowDato.Item("VALOR_TEXT_DEN"), sIdi)
                                        sDenominacion = sDenominacion & " - " & sDenominacion
                                    Else
                                        'ya nos devuelve el codigo - denominación
                                        Dim oPartida As FSNServer.Partida
                                        oPartida = FSNServer.Get_Object(GetType(FSNServer.Partida))
                                        oPartida.BuscarPartida(FSNUser.Cod, sIdi, rowEstructura.Item("PRES5"), sDenominacion)
                                        If oPartida.Data.Tables(0).Rows.Count > 0 Then
                                            sDenominacion = oPartida.Data.Tables(0).Rows(0).Item("DEN").ToString
                                        End If
                                        oPartida = Nothing
                                    End If
                                    dtNewRow.Item(sColName) = sDenominacion
                                Case TiposDeDatos.TipoCampoGS.Activo
                                    If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                        sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(rowDato.Item("VALOR_TEXT_DEN"), sIdi)
                                        sDenominacion = rowDato.Item("VALOR_TEXT") & " - " & sDenominacion
                                    Else
                                        Dim SMServer As FSNServer.Root = Session("FSN_Server")
                                        If Not SMServer Is Nothing Then
                                            Dim oActivo As FSNServer.Activo
                                            oActivo = FSNServer.Get_Object(GetType(FSNServer.Activo))
                                            oActivo.BuscarActivo(sIdi, FSNUser.Cod, rowDato.Item("VALOR_TEXT"))
                                            If oActivo.Data.Tables(0).Rows.Count > 0 Then
                                                sDenominacion = oActivo.Data.Tables(0).Rows(0).Item("DEN").ToString
                                                sDenominacion = rowDato.Item("VALOR_TEXT") & " - " & sDenominacion
                                            End If
                                        End If
                                    End If
                                    dtNewRow.Item(sColName) = sDenominacion
                            End Select
                        End If
                    End If

                    If rowEstructura.Item("TIPO") = 6 Then
                        sColName = "VALORMOSTRAR_" & lIdCol.ToString

                        If IsDBNull(rowDato.Item("VALOR_TEXT")) Then
                            dtNewRow.Item(sColName) = ""
                        Else
                            sDenominacion = ""
                            If rowDato.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(rowDato.Item("VALOR_TEXT_DEN")) Then
                                sDenominacion = DevolverTablaExterna(rowDato.Item("VALOR_TEXT_DEN"))
                            Else
                                Dim oTablaExterna As FSNServer.TablaExterna = FSNServer.Get_Object(GetType(FSNServer.TablaExterna))
                                oTablaExterna.LoadDefTabla(rowDato.Item("TABLA_EXTERNA"))
                                If Not rowDato.Item("VALOR_TEXT") Is Nothing Then
                                    Dim oRowReg As DataRow = oTablaExterna.BuscarRegistro(rowDato.Item("VALOR_TEXT"), , True)
                                    sDenominacion = oTablaExterna.DescripcionReg(oRowReg, sIdi, FSNUser.DateFormat, FSNUser.NumberFormat)
                                End If
                            End If
                            dtNewRow.Item(sColName) = sDenominacion
                        End If
                    End If

                    If rowEstructura.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoNumerico Then
                        sColName = "VALORMOSTRAR_" & lIdCol.ToString
                        dtNewRow.Item(sColName) = rowDato.Item("VALOR_NUM")
                    End If

                    If rowEstructura.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoBoolean Then
                        sColName = "VALORMOSTRAR_" & lIdCol.ToString

                        If Not IsDBNull(rowDato.Item("VALOR_BOOL")) Then
                            dtNewRow.Item(sColName) = IIf(rowDato.Item("VALOR_BOOL") = 1, Textos(45), Textos(46))
                        Else
                            dtNewRow.Item(sColName) = ""
                        End If
                    End If

                    If rowEstructura.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoArchivo Then
                        sColName = "VALORMOSTRAR_" & lIdCol.ToString

                        Dim idAdjunForm As String = ""
                        Dim sAdjun As String = ""
                        Dim idAdjun As String = ""

                        For Each oDSRowAdjun In rowDato.GetChildRows("REL_LINEA_ADJUNTO")
                            idAdjun += oDSRowAdjun.Item("ID").ToString() + "xx"
                            sAdjun += oDSRowAdjun.Item("NOM") + " (" + FSNLibrary.FormatNumber((DBNullToSomething(oDSRowAdjun.Item("DATASIZE")) / 1024), FSNUser.NumberFormat) + " Kb.), "
                        Next
                        If sAdjun <> "" Then
                            sAdjun = sAdjun.Substring(0, sAdjun.Length - 2)
                            idAdjun = idAdjun.Substring(0, idAdjun.Length - 2)
                        End If

                        dtNewRow.Item(sColName) = sAdjun
                        sColName = "VALOR_" & lIdCol.ToString
                        dtNewRow.Item(sColName) = idAdjun
                    End If
                Next
            Next
            If bHayDatos Then dsLineas.Tables(1).Rows.Add(dtNewRow)
        Next
    End Sub
    ''' <summary>
    ''' Sacar la Descripción de tabla externa
    ''' </summary>
    ''' <param name="sValorTextDen">Codigo de tabla externa</param>
    ''' <returns>Descripción de tabla externa</returns>
    ''' <remarks>Llamada desde: RellenarTableLineas; Tiempo máximo=0,2seg.</remarks>
    Private Function DevolverTablaExterna(ByVal sValorTextDen As String) As String
        Dim sValor As String = Nothing
        Dim sTxt As String = sValorTextDen.ToString()
        Dim sTxt2 As String
        If sTxt.IndexOf("]#[") > 0 Then
            sTxt2 = sTxt.Substring(2, sTxt.IndexOf("]#[") - 2)
        Else
            sTxt2 = sTxt.Substring(2, sTxt.Length - 3)
        End If
        Select Case sTxt.Substring(0, 1)
            Case "s"
                sValor = sTxt2
            Case "n"
                sValor = CType(sTxt2, Double).ToString("N" & IIf((CType(sTxt2, Double) Mod 1) > 0, "", "0"), FSNUser.NumberFormat)
            Case "d"
                sValor = CType(sTxt2, Date).ToString("d", FSNUser.DateFormat)
        End Select
        If sTxt.IndexOf("]#[") > 0 Then
            sValor = sValor & " "
            sTxt2 = sTxt.Substring(sTxt.IndexOf("]#[") + 3, sTxt.Length - sTxt.IndexOf("]#[") - 5)
            Select Case sTxt.Substring(sTxt.Length - 1, 1)
                Case "s"
                    sValor = sValor & sTxt2
                Case "n"
                    sValor = CType(sTxt2, Double).ToString("N" & IIf((CType(sTxt2, Double) Mod 1) > 0, "", "0"), FSNUser.NumberFormat)
                Case "d"
                    sValor = CType(sTxt2, Date).ToString("d", FSNUser.DateFormat)
            End Select
        End If
        DevolverTablaExterna = sValor
    End Function
#End Region
End Class