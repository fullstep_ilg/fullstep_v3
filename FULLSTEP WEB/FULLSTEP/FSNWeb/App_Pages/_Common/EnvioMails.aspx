﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EnvioMails.aspx.vb" Inherits="Fullstep.FSNWeb.EnvioMails" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
</head>
<body style="width: 100%; height:100%; padding: 0px; margin-top: 10px; margin-left: 0px; background:#FFFFFF;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <CompositeScript>
                <Scripts>                
                    <asp:ScriptReference Path="../../js/jquery/jquery.min.js" />
                </Scripts>
            </CompositeScript> 
        </asp:ScriptManager>        
        <div>
            <div class="PageHeaderBackground">
                <img id="imgCabecera" style="max-height:30px; max-width:32px;" runat="server"/>
                <span id="lblCabecera" title="DNotificaciÃ³n" class="LabelTituloCabeceraCustomControl" style="left: 60px; width: 79.85%; position: absolute;" runat="server">DNotificaciÃ³n</span>
                <a id="lnkBotonMail" class="CabeceraBotonCustomControl" runat="server" onclick="EnviarMail();">DEnviar mail</a>
            </div>
        </div>
        <div style="margin:1em;">
            <input id="Instancia" runat="server" value="0" type="hidden" name="Instancia"/>
            <input id="Prove" runat="server" value="0" type="hidden" name="Prove"/>
            <input id="isHTML" runat="server" value="0" type="hidden" name="isHTML"/>
            <input id="TipoEmail" runat="server" value="0" type="hidden" name="TipoEmail"/>
            <input id="EntidadEmail" runat="server" value="0" type="hidden" name="EntidadEmail"/>
            <asp:Label runat="server" ID="lblPara" CssClass="Rotulo" style="display:inline-block; width:4em;">DPara:</asp:Label>
            <asp:TextBox runat="server" ID="txtPara" style="width:700px;"></asp:TextBox>
        </div>
        <div style="margin:0em 1em;">
            <asp:Label runat="server" ID="lblAsunto" CssClass="Rotulo" style="display:inline-block; width:4em;">DAsunto:</asp:Label>
            <asp:TextBox runat="server" ID="txtAsunto" style="width:700px;"></asp:TextBox>
        </div>
        <div style="margin:1em; height:75vh;">
            <ajxEditor:Editor ID="txtEmailHtml" runat="server" style="width:100%; height:100%;"/>
        </div>

        <script type="text/javascript">
            function EnviarMail() {                                       
                $.when($.ajax({
                    type: "POST",
                    url: rutaFS + '/_Common/EnvioMails.aspx/EnviarMail',
                    data: JSON.stringify({'Asunto': $get('txtAsunto').value,'Para': $get('txtPara').value,'Cuerpo': $get('txtEmailHtml').control.get_content(),'isHTML': $get('isHTML').value,'CodProve': $get('Prove').value,'Instancia':
                        $get('Instancia').value, 'TipoEmail': $get('TipoEmail').value, 'EntidadEmail': $get('EntidadEmail').value
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false
                })).done(function (msg) {
                    window.close();
                });
            }
        </script>
    </form>
</body>
</html>
