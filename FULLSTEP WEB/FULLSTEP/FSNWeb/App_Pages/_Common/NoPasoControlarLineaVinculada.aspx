﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NoPasoControlarLineaVinculada.aspx.vb" Inherits="Fullstep.FSNWeb.NoPasoControlarLineaVinculada" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
</head>
<body style="background-color: #FFFFFF;margin-top: 0px; margin-left: 0px" onload="window.focus();">
	<form id="Form1" runat="server">
	
	    <table>
	    <tr>
	        <td valign="top">
		        <asp:Image id="Image1" runat="server" SkinId="ErrorGrande" ></asp:Image>
		    </td>
		    <td>
		        <div style="max-height:170px;overflow:auto">
		        <asp:Label id="LblCadena" runat="server"></asp:Label>
		        </div>
            </td>			    
        </tr>			    		    
        </table>
         <table width="60%">
            <tr>
                <td >
                    <fsn:FSNButton ID="btnCerrar" runat="server" Text="OK"  OnClientClick="window.close();return false;" Alineacion="Right"></fsn:FSNButton>		        
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table> 
    
	</form>
</body>
</html>
