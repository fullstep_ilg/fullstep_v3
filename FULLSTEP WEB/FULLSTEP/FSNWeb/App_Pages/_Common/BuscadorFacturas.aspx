﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BuscadorFacturas.aspx.vb" Inherits="Fullstep.FSNWeb.BuscadorFacturas" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
    
</head>
<body>
    <form id="frmBuscadorFacturas" name="frmBuscadorFacturas" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
        <CompositeScript>
            <Scripts>
                <asp:ScriptReference Path="~/js/jsUpdateProgress.js" />
            </Scripts>
        </CompositeScript>
    </asp:ScriptManager>   
    <asp:Panel ID="pnlTitulo" runat="server" Width="100%">
            <table width="100%" cellpadding="0" border="0">
                <tr>
                    <td>
                        <fsn:FSNPageHeader ID="FSNPageHeader" runat="server">
                        </fsn:FSNPageHeader>
                    </td>
                </tr>
            </table>
    </asp:Panel>
    <asp:UpdatePanel ID="upFiltros" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
    <asp:Panel ID="pnlFiltros" runat="server" Width="100%">
    <table id="Table1" width="100%" style="padding-top: 4px; padding-bottom: 4px" border="0" 
            cellpadding="0" cellspacing="0">
                <tr><td>
                        <asp:Label ID="lblProveedor" runat="server" Text="dProveedor:" CssClass="Etiqueta"></asp:Label>
                    </td>
                    <td><table style="background-color: White; width: 150px; border: solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
                             <tr><td><asp:TextBox ID="txtProveedor" runat="server" Width="240px" BorderWidth="0px" BackColor="White" Height="16px"></asp:TextBox>
                                     <ajx:AutoCompleteExtender ID="txtProveedor_AutoCompleteExtender" runat="server" CompletionSetCount="10"
                                          DelimiterCharacters="" Enabled="True" MinimumPrefixLength="1" ServiceMethod="GetProveedores"
                                          ServicePath="/App_Pages/_Common/App_Services/AutoComplete.asmx" TargetControlID="txtProveedor" EnableCaching="False" OnClientItemSelected="Proveedor_selected">
                                     </ajx:AutoCompleteExtender>
                                  </td>
                                  <td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px">
                                    <asp:ImageButton ID="imgProveedorLupa" runat="server" SkinID="BuscadorLupa" />
                                  </td>
                             </tr>
                             <asp:HiddenField ID="hidProveedor" runat="server" />
                         </table>
                    </td>
                    <td>
                        <asp:Label ID="lblEstado" runat="server" Text="dEstado:" CssClass="Etiqueta"></asp:Label>
                    </td>
                    <td><ig:WebDropDown ID="wddEstado" runat="server" Width="240px" EnableClosingDropDownOnSelect="true" EnableClientRendering="false" CurrentValue="">                    
                        </ig:WebDropDown>
                    </td>
                </tr>
                <tr><td>
                        <asp:Label ID="lblEmpresa" runat="server" Text="dEmpresa:" CssClass="Etiqueta"></asp:Label>
                    </td>
                    <td>
                        <table style="background-color:White;width:150px;border:solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtEmpresa" runat="server" Width="240px" BorderWidth="0px"></asp:TextBox>
                                    <ajx:AutoCompleteExtender ID="txtEmpresa_AutoCompleteExtender" 
                                     runat="server" CompletionSetCount="10" DelimiterCharacters="" Enabled="True"
                                     MinimumPrefixLength="1" ServiceMethod="GetEmpresas" ServicePath="/App_Pages/_Common/App_Services/AutoComplete.asmx"
                                     TargetControlID="txtEmpresa" EnableCaching="False" OnClientItemSelected="Empresa_selected"  ></ajx:AutoCompleteExtender >
                                </td>
                                <td style="border-left:solid 1px #BBBBBB;padding-left:2px;padding-right:0px">
                                    <asp:ImageButton id="imgEmpresaLupa" runat="server" SkinID="BuscadorLupa" /> 
                                </td>
                            </tr>                        
                            <asp:HiddenField ID="hidEmpresa" runat="server" />
                        </table>
                        </td>
                    <td>
                        <asp:Label ID="lblPedido" runat="server" Text="dPedido:" CssClass="Etiqueta"></asp:Label>
                    </td>
                    <td><div style="float:left">
                        <div style="display:table-cell;padding-right:5px"><ig:WebDropDown ID="ddlAnyo" runat="server" AutoPostBack="false" EnableMultipleSelection="false"
                                                                    EnableClosingDropDownOnSelect="true" BackColor="White" Width="70px" EnableCustomValues="false"
                                                                    DropDownContainerWidth="80" EnableDropDownAsChild="false" EnableCustomValueSelection="false"
                                                                     EnableMarkingMatchedText="true" AutoSelectOnMatch="true">
                                                                </ig:WebDropDown>
                        </div>
                        <div style="display:table-cell;padding-right:5px;vertical-align:middle"><asp:TextBox CssClass="CajaTexto" ID="txtNumCesta" runat="server" onchange="validarsolonum(this)" onkeypress="return validarkeynum()" onpaste="validarpastenum()" Columns="9"></asp:TextBox>
                                                    <ajx:TextBoxWatermarkExtender WatermarkCssClass="waterMark" ID="txtwmeNumCesta" runat="server" TargetControlID="txtNumCesta" WatermarkText="DNº de Cesta"></ajx:TextBoxWatermarkExtender></div>
                        <div style="display:table-cell;padding-right:5px;vertical-align:middle"><asp:TextBox CssClass="CajaTexto" ID="txtNumPedido" runat="server" onchange="validarsolonum(this)" onkeypress="return validarkeynum()" onpaste="validarpastenum()" Columns="9"></asp:TextBox>
                                                    <ajx:TextBoxWatermarkExtender WatermarkCssClass="waterMark" ID="txtwmeNumPedido" runat="server" TargetControlID="txtNumPedido" WatermarkText="DNº de Pedido"></ajx:TextBoxWatermarkExtender></div>
                    </div></td>
                </tr>
                <tr><td><asp:Label ID="lblArticulo" runat="server" Text="DArticulo:" CssClass="Etiqueta"></asp:Label></td>
                    <td><table style="background-color:White;width:150px;border:solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
                        <tr><td>
                <asp:TextBox ID="txtArticulo" runat="server" Width="240px" BorderWidth="0px"></asp:TextBox>
                <ajx:AutoCompleteExtender ID="txtArticulo_AutoCompleteExtender" 
                    runat="server" CompletionSetCount="10" DelimiterCharacters="" Enabled="True"
                    MinimumPrefixLength="1" ServiceMethod="GetArticulos" ServicePath="/App_Pages/_Common/App_Services/AutoComplete.asmx" OnClientItemSelected="Articulo_selected"
                    TargetControlID="txtArticulo" EnableCaching="False" ></ajx:AutoCompleteExtender >
             </td>
                        <td style="border-left:solid 1px #BBBBBB;padding-left:2px;padding-right:0px">
                        <asp:ImageButton id="imgArticuloLupa" runat="server" SkinID="BuscadorLupa" /> 
                        </td>
                        </tr>                        
                        <asp:HiddenField ID="hidArticulo" runat="server" />
                        </table>
                    </td>
                    <td><asp:Label ID="lblNumeroAlbaran" runat="server" Text="DNumero Albaran:" CssClass="Etiqueta"></asp:Label></td>
                    <td><asp:TextBox ID="txtNumeroAlbaran" runat="server" Width="235px"></asp:TextBox></td>
                </tr>
                <tr><td><asp:Label ID="lblFechaFactura" runat="server" Text="DFecha factura (desde-hasta):" CssClass="Etiqueta"></asp:Label></td>
                <td><div style="float:left">
                        <div style="display:table-cell;padding-right:5px"><igpck:WebDatePicker ID="dteFechaFacturaDesde" runat="server" Width="100px"  SkinID="Calendario"></igpck:WebDatePicker></div>
                        <div style="display:table-cell;padding-right:5px;vertical-align:middle">-</div>
                        <div style="display:table-cell;padding-right:5px"><igpck:WebDatePicker ID="dteFechaFacturaHasta" runat="server" Width="100px"  SkinID="Calendario"></igpck:WebDatePicker></div>
                    </div>
                </td>
                <td id="CeldaLblPedERP" runat="server"><asp:Label ID="lblPedidoERP" runat="server" Text="DRef. en factura:" CssClass="Etiqueta"></asp:Label></td>
                <td id="CeldaPedERP" runat="server"><asp:TextBox ID="txtPedidoERP" runat="server" Width="235px"></asp:TextBox></td>
                </tr>
                <tr><td><asp:Label ID="lblFechaContabilizacion" runat="server" Text="DFecha Contabilizacion (desde-hasta):" CssClass="Etiqueta"></asp:Label></td>
                    <td><div style="float:left">
                            <div style="display:table-cell;padding-right:5px"><igpck:WebDatePicker ID="dteContabilizacionDesde" runat="server" Width="100px"  SkinID="Calendario"></igpck:WebDatePicker></div>
                            <div style="display:table-cell;padding-right:5px;vertical-align:middle">-</div>
                            <div style="display:table-cell;padding-right:5px"><igpck:WebDatePicker ID="dteContabilizacionHasta" runat="server" Width="100px"  SkinID="Calendario"></igpck:WebDatePicker></div>
                        </div>
                    </td>
                    <td id="CeldaLblNumFacturaERP" runat="server"><asp:Label ID="lblNumeroFacturaSAP" runat="server" Text="DNum Factura SAP:" CssClass="Etiqueta"></asp:Label></td>
                    <td id="CeldaNumFacturaERP" runat="server"><asp:TextBox ID="txtNumeroFacturaSAP" runat="server" Width="235px"></asp:TextBox></td>
                </tr>
                <tr><td><asp:Label ID="lblImporte" runat="server" Text="DImporte (desde-hasta):" CssClass="Etiqueta"></asp:Label></td>
                    <td><div style="float:left">
                            <div style="display:table-cell;padding-right:5px"><igpck:WebNumericEditor ID="wneImporteDesde" runat="server" BorderStyle="Solid" BorderColor="#BBBBBB" Width="96px"></igpck:WebNumericEditor></div>
                            <div style="display:table-cell;padding-right:5px;vertical-align:middle">-</div>
                            <div style="display:table-cell;padding-right:5px"><igpck:WebNumericEditor id="wneImporteHasta" runat="server" BorderStyle="Solid" BorderColor="#BBBBBB" Width="96px"></igpck:WebNumericEditor></div>
                        </div>
                    </td>
                    <td id="CeldaLblGestor" runat="server"><asp:Label ID="lblGestor" runat="server" Text="DGestor:" CssClass="Etiqueta"></asp:Label></td>
                    <td id="CeldaGestor" runat="server">
                        <table style="background-color:White;width:150px;border:solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
                        <tr><td>
                                <asp:TextBox ID="txtGestor" runat="server" Width="240px" BorderWidth="0px"></asp:TextBox>
                             </td>
                             <td style="border-left:solid 1px #BBBBBB;padding-left:2px;padding-right:0px">
                                  <asp:ImageButton id="imgGestorLupa" runat="server" SkinID="BuscadorLupa" /> 
                             </td>
                        </tr>                        
                        <asp:HiddenField ID="hid_Gestor" runat="server" />
                        </table>
                    </td>
                </tr>
                <tr valign="top"><td id="CeldaLblCentroCoste" runat="server"><asp:Label ID="lblCentroCoste" runat="server" Text="DCentroCoste:" CssClass="Etiqueta"></asp:Label></td>
                <td id="CeldaCentroCoste" runat ="server">
                    <table style="background-color:White;width:150px;border:solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
                    <tr><td>
                            <asp:TextBox ID="txtCentroCoste" runat="server" Width="240px" BorderWidth="0px" onchange="CentroCosteChange(this)"></asp:TextBox>
                         </td>
                         <td style="border-left:solid 1px #BBBBBB;padding-left:2px;padding-right:0px">
                              <asp:ImageButton id="imgCentroCosteLupa" runat="server" SkinID="BuscadorLupa" /> 
                         </td>
                    </tr>                        
                    <asp:HiddenField ID="hid_CentroCostes" runat="server" />
                    </table>
                </td>
                <td id="CeldaPartidas" runat="server" colspan="2">
                <asp:DataList ID="dlPartidas" runat="server" Width="100%" RepeatDirection="Vertical" >
                <ItemTemplate>
                    <table style="width:100%">
                        <tr>
                            <td style="width:50%"><asp:Label ID="lblPartida" runat="server" Text="DPartida:" CssClass="Etiqueta"></asp:Label></td>
                            <td style="width:50%">
                                <table style="background-color:White;width:150px;border:solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
                                <tr><td >
                                        <asp:TextBox ID="txtPartida" runat="server" Width="240px" BorderWidth="0px"></asp:TextBox>
                                        </td>
                                        <td style="border-left:solid 1px #BBBBBB;padding-left:2px;padding-right:0px;">
                                            <asp:ImageButton id="imgPartidaLupa" runat="server" SkinID="BuscadorLupa" /> 
                                        </td>
                                </tr>                        
                                <asp:HiddenField ID="hid_Partida" runat="server" />
                                <asp:HiddenField ID="hid_Pres5" runat="server" />
                                </table>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                </asp:DataList> 
        
            </td>
                </tr>
                <tr>
                    <td colspan="4" style="padding-left: 5px;">
                        <table style="width:100%;">
                            <tr>
                                <td>
                                    <fsn:FSNButton ID="btnBuscar" runat="server" Text="DBuscar" Alineacion="Left">                    
                                        &nbsp;&nbsp;&nbsp;&nbsp;           
                                    </fsn:FSNButton>
                                </td>
                                <td>
                                    <fsn:FSNButton ID="btnLimpiar" runat="server" Text="DLimpiar" Alineacion="Left">                    
                                        &nbsp;&nbsp;&nbsp;&nbsp;                    
                                    </fsn:FSNButton>
                                </td>
                                <td style="width:690px; margin-left:5px;">                                    
                                    <asp:Label ID="lblPulse" runat="server" CssClass="ColorAzul"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
    </table>
    </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdDatos" ChildrenAsTriggers="false" runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="click" />
            </Triggers>
            <ContentTemplate>
            <asp:Panel ID="pnlGrid" runat="server" Width="99%">
                <div class="BusqFacturas">
                        <ig:WebDataGrid  ID="wdgDatos" runat="server" Width="100%" ShowHeader="true" AutoGenerateColumns="false" EnableViewState="true">
                        <Columns>
                            <ig:BoundDataField Key="ID" DataFieldName="ID" Hidden="true"></ig:BoundDataField>
                            <ig:BoundDataField Key="NUM" DataFieldName="NUM"></ig:BoundDataField>
                            <ig:BoundDataField Key="EMPRESA" DataFieldName="EMPRESA" DataFormatString="{0:d}"></ig:BoundDataField>
                            <ig:BoundDataField Key="PROVEEDOR" DataFieldName="COD_PROVE"></ig:BoundDataField>
                            <ig:BoundDataField Key="NOMBRE_PROVEEDOR" DataFieldName="DEN_PROVE"></ig:BoundDataField>
                            <ig:BoundDataField Key="FECHA" DataFieldName="FECHA" DataFormatString="{0:d}"></ig:BoundDataField>
                            <ig:BoundDataField Key="FEC_CONTA" DataFieldName="FEC_CONTA" DataFormatString="{0:d}"></ig:BoundDataField>
                            <ig:BoundDataField Key="NUM_ERP" DataFieldName="NUM_ERP"></ig:BoundDataField>
                            <ig:BoundDataField Key="IMPORTE" DataFieldName="IMPORTE"></ig:BoundDataField>
                        </Columns>
                            <Behaviors>                        
                                <ig:Selection  RowSelectType="Single" Enabled="True" CellClickAction="Cell" >
                                    <SelectionClientEvents CellSelectionChanged="wdgDatos_CellSelectionChanged"/>
                                </ig:Selection>                                
                                <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true" AnimationEnabled="true"></ig:Filtering> 
                                <ig:Sorting Enabled="true" SortingMode="Multi"></ig:Sorting> 
                                <ig:RowSelectors Enabled="false" RowNumbering="false"></ig:RowSelectors>
                                <ig:Paging Enabled="true" PagerAppearance="Top">
                                    <PagerTemplate>
                                        <fsn:wucPagerControl ID="CustomerPager" runat="server" />
                                    </PagerTemplate>
                                </ig:Paging>
                                <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
                                <ig:Activation Enabled="true"></ig:Activation>
                            </Behaviors>                
                        </ig:WebDataGrid>
                    </div>
            </asp:Panel>
            </ContentTemplate>
   </asp:UpdatePanel>
   <script type="text/javascript" language="javascript">
        var ModalProgress = '<%= ModalProgress.ClientID %>';
   </script>
   

        <ajx:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
            BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
        <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress" Style="display: none">
            <div style="position: relative; top: 30%; text-align: center;">
                <asp:Image ID="ImgProgress" runat="server" SkinID="ImgProgress" />
                <asp:Label ID="LblProcesando" runat="server" ForeColor="Black" Text="Cargando ..."></asp:Label>
            </div>
        </asp:Panel>      

    </form>
</body>
</html>

<script runat="server">
    ''' <summary>
    ''' Controlar el cross-site scripting (XSS)
    ''' </summary>
    ''' <param name="Request">objeto Request de la pantalla a controlar</param>       
    ''' <returns>0 hay XSS / 1 no hay XSS</returns>
    ''' <remarks>Llamada desde: Toda pantalla en q queramos controlar el cross-site; Tiempo máximo:0,1</remarks>
    Function CtrlPosibleXSS(strRequest As String) As String
        Dim intRespuesta
        Dim i
        Dim ABuscar(99)
        Dim j
	
        strRequest = QuitaEspacios(Trim(UCase(strRequest)))
        strRequest = QuitaAsciiHex(strRequest)

        intRespuesta = 1
	
        If strRequest = "" Then
        Else
		
            ABuscar(1) = "javascript"
            ABuscar(2) = "vbscript"
            ABuscar(3) = "expression"
            ABuscar(4) = "applet"
            ABuscar(5) = "meta"
            ABuscar(6) = "xml"
            ABuscar(7) = "blink"
            ABuscar(8) = "link"
            ABuscar(9) = "style"
            ABuscar(10) = "script"
            ABuscar(11) = "embed"
            ABuscar(12) = "object"
            ABuscar(13) = "iframe"
            ABuscar(14) = "frame"
            ABuscar(15) = "frameset"
            ABuscar(16) = "ilayer"
            ABuscar(17) = "layer"
            ABuscar(18) = "bgsound"
            ABuscar(19) = "title"
            ABuscar(20) = "base"
            ABuscar(21) = "onabort"
            ABuscar(22) = "onactivate"
            ABuscar(23) = "onafterprint"
            ABuscar(24) = "onafterupdate"
            ABuscar(25) = "onbeforeactivate"
            ABuscar(26) = "onbeforecopy"
            ABuscar(27) = "onbeforecut"
            ABuscar(28) = "onbeforedeactivate"
            ABuscar(29) = "onbeforeeditfocus"
            ABuscar(30) = "onbeforepaste"
            ABuscar(31) = "onbeforeprint"
            ABuscar(32) = "onbeforeunload"
            ABuscar(33) = "onbeforeupdate"
            ABuscar(34) = "onblur"
            ABuscar(35) = "onbounce"
            ABuscar(36) = "oncellchange"
            ABuscar(37) = "onchange"
            ABuscar(38) = "onclick"
            ABuscar(39) = "oncontextmenu"
            ABuscar(40) = "oncontrolselect"
            ABuscar(41) = "oncopy"
            ABuscar(42) = "oncut"
            ABuscar(43) = "ondataavailable"
            ABuscar(44) = "ondatasetchanged"
            ABuscar(45) = "ondatasetcomplete"
            ABuscar(46) = "ondblclick"
            ABuscar(47) = "ondeactivate"
            ABuscar(48) = "ondrag"
            ABuscar(49) = "ondragend"
            ABuscar(50) = "ondragenter"
            ABuscar(51) = "ondragleave"
            ABuscar(52) = "ondragover"
            ABuscar(53) = "ondragstart"
            ABuscar(54) = "ondrop"
            ABuscar(55) = "onerror"
            ABuscar(56) = "onerrorupdate"
            ABuscar(57) = "onfilterchange"
            ABuscar(58) = "onfinish"
            ABuscar(59) = "onfocus"
            ABuscar(60) = "onfocusin"
            ABuscar(61) = "onfocusout"
            ABuscar(62) = "onhelp"
            ABuscar(63) = "onkeydown"
            ABuscar(64) = "onkeypress"
            ABuscar(65) = "onkeyup"
            ABuscar(66) = "onlayoutcomplete"
            ABuscar(67) = "onload"
            ABuscar(68) = "onlosecapture"
            ABuscar(69) = "onmousedown"
            ABuscar(70) = "onmouseenter"
            ABuscar(71) = "onmouseleave"
            ABuscar(72) = "onmousemove"
            ABuscar(73) = "onmouseout"
            ABuscar(74) = "onmouseover"
            ABuscar(75) = "onmouseup"
            ABuscar(76) = "onmousewheel"
            ABuscar(77) = "onmove"
            ABuscar(78) = "onmoveend"
            ABuscar(79) = "onmovestart"
            ABuscar(80) = "onpaste"
            ABuscar(81) = "onpropertychange"
            ABuscar(82) = "onreadystatechange"
            ABuscar(83) = "onreset"
            ABuscar(84) = "onresize"
            ABuscar(85) = "onresizeend"
            ABuscar(86) = "onresizestart"
            ABuscar(87) = "onrowenter"
            ABuscar(88) = "onrowexit"
            ABuscar(89) = "onrowsdelete"
            ABuscar(90) = "onrowsinserted"
            ABuscar(91) = "onscroll"
            ABuscar(92) = "onselect"
            ABuscar(93) = "onselectionchange"
            ABuscar(94) = "onselectstart"
            ABuscar(95) = "onstart"
            ABuscar(96) = "onstop"
            ABuscar(97) = "onsubmit"
            ABuscar(98) = "onunload"
            ABuscar(99) = "alert"
									
            For i = 1 To 20
                If CtrlPosibleEntrada(strRequest, UCase("<" & ABuscar(i))) = 0 Then
                    strRequest = Replace(strRequest, UCase("<" & ABuscar(i)), "")
                End If
            Next

            If intRespuesta = 1 Then
                For i = 21 To 99
                    If CtrlPosibleEntrada(strRequest, UCase(ABuscar(i))) = 0 Then
                        strRequest = Replace(strRequest, UCase(ABuscar(i)), "")
                    End If
                Next
            End If
        End If
        
        Return strRequest
    End Function
    
    ''' <summary>
    ''' Controlar el cross-site scripting (XSS) contra una entrada de la lista negra javascript
    ''' </summary>
    ''' <param name="strRequest">string a controlar</param>      
    ''' <param name="strABuscar">string lista negra</param>   
    ''' <returns>0 hay XSS / 1 no hay XSS</returns>
    ''' <remarks>Llamada desde: CtrlPosible; Tiempo máximo:0,1</remarks>
    Function CtrlPosibleEntrada(strRequest, strABuscar) As Int16
        Dim j
        Dim k
        Dim i
	
        Dim car_ABuscar
        Dim car_strRequest
        Dim carSiguiente_strRequest
	
        Dim esBueno
        Dim bEsEvento ' Me dice si el string de la lista negra es un evento
	
        bEsEvento = False
	
        car_ABuscar = Mid(strABuscar, 1, 1)

        If car_ABuscar <> "<" Then
            bEsEvento = True
        End If
		
        For j = 1 To Len(strRequest)
            car_strRequest = Mid(strRequest, j, 1)
			
            If car_ABuscar = car_strRequest Then
		
                i = j + 1
                esBueno = False
			
                For k = 2 To Len(strABuscar)
                    If Mid(strRequest, i, 1) = Mid(strABuscar, k, 1) Then
                        i = i + 1
                    Else
                        esBueno = True
                        Exit For
                    End If
                Next
			
                If esBueno = False Then
                    If bEsEvento Then ' Si se trata de un evento se calcula el carácter siguiente al de la entrada y se da ésta por válida si el carácter es letra o número
                        carSiguiente_strRequest = Mid(strRequest, i, 1)
                        ''If RegExpTest("[0-9A-ZÀ-Ü]", carSiguiente_strRequest) Then
                        If IsNumeric(carSiguiente_strRequest) _
                        OrElse (Asc(LCase(carSiguiente_strRequest)) >= Asc("a") AndAlso Asc(LCase(carSiguiente_strRequest)) <= Asc("z")) _
                        OrElse (Asc(LCase(carSiguiente_strRequest)) >= Asc("à") AndAlso Asc(LCase(carSiguiente_strRequest)) <= Asc("ü")) Then
                            CtrlPosibleEntrada = 1
                            Exit Function
                        End If
                    End If
                    CtrlPosibleEntrada = 0
                    Exit Function
                End If
            End If
        Next
	
        CtrlPosibleEntrada = 1
	
    End Function
    
    ''' <summary>
    ''' Trasformar en strRequest los caracteres q me vinieran en decimal o hexadecimal en codigo ascii
    ''' </summary>
    ''' <param name="strRequest">string a controlar</param>          
    ''' <returns>strRequest con los caracteres convertidos a ascii</returns>
    ''' <remarks>Llamada desde:CtrlPosible; Tiempo máximo:0</remarks>
    Function QuitaAsciiHex(strRequest As String) As String
        Dim i
        Dim j
        Dim k
        Dim NumCar
        Dim sAux
        Dim car
        Dim asci3
        Dim asci4
        Dim c_asci3
        Dim c_asci4
        Dim encontrado

        sAux = ""
        NumCar = Len(strRequest)
        i = 1
	
        While i < NumCar + 1
            car = Mid(strRequest, i, 1)
            asci3 = Mid(strRequest, i, 3)
            asci4 = Mid(strRequest, i, 4)
	
            If car = "%" Or car = "\" Then
                encontrado = False
			
                'mayusculas
                k = 41
						
                For j = 65 To 90
                    c_asci3 = "%" & CStr(j)
				
                    Select Case j
                        Case 74
                            c_asci4 = "\X4A"
                        Case 75
                            c_asci4 = "\X4B"
                        Case 76
                            c_asci4 = "\X4C"
                        Case 77
                            c_asci4 = "\X4D"
                        Case 78
                            c_asci4 = "\X4E"
                        Case 79
                            c_asci4 = "\X4F"
                        Case 90
                            c_asci4 = "\X5A"
                        Case Else
                            c_asci4 = "\X" & CStr(k)
                            k = k + 1
                    End Select

                    If asci3 = c_asci3 Then
                        sAux = sAux & Chr(j)
					
                        i = i + 2
                        encontrado = True
					
                        Exit For
                    ElseIf asci4 = c_asci4 Then
                        sAux = sAux & Chr(j)
					
                        i = i + 3
                        encontrado = True
					
                        Exit For
                    End If
                Next
			
                'minusculas
                k = 61
                For j = 97 To 122
                    c_asci3 = "%" & CStr(j)
				
                    Select Case j
                        Case 106
                            c_asci4 = "\X6A"
                        Case 107
                            c_asci4 = "\X6B"
                        Case 108
                            c_asci4 = "\X6C"
                        Case 109
                            c_asci4 = "\X6D"
                        Case 110
                            c_asci4 = "\X6E"
                        Case 111
                            c_asci4 = "\X6F"
                        Case 122
                            c_asci4 = "\X7A"
                        Case Else
                            c_asci4 = "\X" & CStr(k)
                            k = k + 1
                    End Select

                    If asci3 = c_asci3 Then
                        sAux = sAux & UCase(Chr(j))
					
                        i = i + 2
                        encontrado = True
					
                        Exit For
                    ElseIf asci4 = c_asci4 Then
                        sAux = sAux & UCase(Chr(j))
					
                        i = i + 3
                        encontrado = True
					
                        Exit For
                    End If
                Next
			
                If encontrado = False Then
                    sAux = sAux & car
                End If
            Else
                sAux = sAux & car
            End If
		
            i = i + 1
        End While
	
        QuitaAsciiHex = sAux
    End Function

    ''' <summary>
    ''' Trasformar el strRequest quitandole los espacios
    ''' </summary>
    ''' <param name="strRequest">string a controlar</param>          
    ''' <returns>strRequest sin espacios</returns>
    ''' <remarks>Llamada desde:CtrlPosible; Tiempo máximo:0</remarks>
    Function QuitaEspacios(strRequest As String) As String
        Dim sAux
        Dim i
        Dim NumCar
        Dim car
        Dim asci3
        Dim asci4

        sAux = ""
        NumCar = Len(strRequest)
        i = 1
	
        While i < NumCar + 1
            car = Mid(strRequest, i, 1)
            asci3 = Mid(strRequest, i, 3)
            asci4 = Mid(strRequest, i, 4)
	
            If car = " " Then
                i = i + 1
            ElseIf asci3 = "%32" Then
                i = i + 3
            ElseIf asci4 = "\X20" Then
                i = i + 4
            Else
                sAux = sAux & car
			
                i = i + 1
            End If
        End While
	
        QuitaEspacios = sAux
    End Function
</script>

<script type="text/javascript">
    /*<%--
    ''' <summary>
    ''' Function que comprueba el valor del control pasado. Si es numérico lo devuelve, si no, no.
    ''' </summary>
    ''' txt: control que lanza el evento
    ''' <remarks>Llamada desde evento onchange de controles textbox; Tiempo máximo:0,1seg.</remarks>--%>*/
    function validarsolonum(txt) {
        var str = '';
        for (var i = 0; i < txt.value.length; i++) {
            if (txt.value.charCodeAt(i) >= 48 && txt.value.charCodeAt(i) <= 57)
                str += txt.value.charAt(i);
        }
        txt.value = str;
    }

/*<%--
    ''' <summary>
    ''' Function que comprueba que el valor tecleado es numérico. Si no lo es, cancela el evento.
    ''' </summary>
    ''' <remarks>Llamada desde evento onKeyPress de controles textbox; Tiempo máximo:0,1seg.</remarks>--%>*/
    function validarkeynum() {
        if (event.keyCode < 48 || event.keyCode > 57) {
            event.returnValue = false;
        }
    }

    /*<%--
    ''' <summary>
    ''' Function que comprueba que los valore pegados son numéricos. Si no lo son, impide el pagado
    ''' </summary>
    ''' <remarks>Llamada desde evento onpaste de controles textbox; Tiempo máximo:0,1seg.</remarks>--%>*/
    function validarpastenum() {
        var texto = window.clipboardData.getData('Text');
        var str = '';
        for (var i = 0; i < texto.length; i++) {
            if (texto.charCodeAt(i) >= 48 && texto.charCodeAt(i) <= 57)
                str += texto.charAt(i);
        }
        if (texto != str) event.returnValue = false;
    }

    //Recoge el ID de la empresa seleccionada con el autocompletar
    function Articulo_selected(sender, e) {
        oHid_Articulo = document.getElementById('<%= hidArticulo.ClientID %>');
        if (oHid_Articulo)
            oHid_Articulo.value = e._value;
    }

    //Recoge el ID de la empresa seleccionada con el autocompletar
    function Empresa_selected(sender, e) {
        oIdEmpresa = document.getElementById('<%= hidEmpresa.ClientID %>')
        if (oIdEmpresa)
            oIdEmpresa.value = e._value;
    }

    //Recoge el ID del proveedor seleccionado con el autocompletar
    function Proveedor_selected(sender, e) {
        oIdProveedor = document.getElementById('<%= hidProveedor.ClientID %>')
        if (oIdProveedor)
            oIdProveedor.value = e._value;
    }

     /*
        ''' <summary>
        ''' Devuelve a la pantalla q llamó al buscador de facturas, la factura seleccionada (num e id) y cierra esta pantalla.
        ''' </summary>
        ''' <param name="sender">parametro de sistema</param>
        ''' <param name="e">parametro de sistema</param>            
        ''' <remarks>Llamada desde: Sistema ;Tiempo máximo: 0</remarks>
        */
        function wdgDatos_CellSelectionChanged(sender, e) {
            var Key = e.getSelectedCells().getItem(0).get_column().get_key();
            var sID = e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("ID").get_value();
            var sNumFactura = e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("NUM").get_value();
            var str= "<%=CtrlPosibleXSS(Request("IDControl"))%>";

            window.opener.factura_seleccionada(str, sNumFactura, sID)
            window.close();
        }
    

     //Funcion que abre el buscador de partidas
    function AbrirPartidas(rutaPartidas, idTxtCentroCoste, idHidCentroCoste, idTxtPartida, idHidPartida, Partida, CentroCoste, CentroCosteDen, PartidaDen) {
        var txtCentroCoste = document.getElementById(idTxtCentroCoste)
        var hidCentroCoste = document.getElementById(idHidCentroCoste)
        var hidPartida = document.getElementById(idHidPartida)

        window.open(rutaPartidas + "?idTxtCentroCoste=" + idTxtCentroCoste + "&idHidCentroCoste=" + idHidCentroCoste + "&idTxtPartida=" + idTxtPartida + "&idHidPartida=" + idHidPartida + "&Partida=" + Partida + "&CentroCoste=" + escape(hidCentroCoste.value) + "&CentroCosteDen=" + txtCentroCoste.value + "&PartidaDen=" + PartidaDen, "_blank", "width=850,height=630,status=yes,resizable=no,top=200,left=200")
    }

    function Partida_seleccionada(IdControlPartida, IdControlPartidaDen, sUON1, sUON2, sUON3, sUON4, sContrato, sDen, IdControlCentroCosteDen, IdControlCentroCoste, sDenCC) {
        var sValor = '';

        //lo que se guarda en valor_text
        if (sUON1)
            sValor = sUON1;
        if (sUON2)
            sValor = sValor + '#' + sUON2;
        if (sUON3)
            sValor = sValor + '#' + sUON3;
        if (sUON4)
            sValor = sValor + '#' + sUON4;
        if (sContrato)
            sValor = sValor + '#' + sContrato;

        if (IdControlPartida == "")
            return

        txtPartida = document.getElementById(IdControlPartidaDen)
        txtPartida.value = sDen
        hidPartida = document.getElementById(IdControlPartida)
        hidPartida.value = sValor


        if (IdControlCentroCoste != "") {
            var separador = sValor.lastIndexOf('#');
            if (separador > 0)
                var sValor = sValor.substr(0, separador);

            txtCentroCostes = document.getElementById(IdControlCentroCosteDen)
            txtCentroCostes.value = sDenCC
            hidCentroCostes = document.getElementById(IdControlCentroCoste)
            hidCentroCostes.value = sValor
            
        }
    }
    
    function CentroCoste_seleccionado(idControl,idHidControl,sUON1, sUON2, sUON3, sUON4, sDen) {
        var sValor = '';

        //lo que se guarda en valor_text
        if (sUON1)
            sValor = sUON1;
        if (sUON2)
            sValor = sValor + '#' + sUON2;
        if (sUON3)
            sValor = sValor + '#' + sUON3;
        if (sUON4)
            sValor = sValor + '#' + sUON4;

        txtCentroCostes = document.getElementById(idControl)
        txtCentroCostes.value=sDen
        hidCentroCostes = document.getElementById(idHidControl)
        hidCentroCostes.value = sValor

    }

    function usu_seleccionado(sUsuCod, sUsuNom, sUsuEmail) {
        var txtGestor = document.getElementById('<%= txtGestor.ClientID %>')
        var hidGestor = document.getElementById('<%= hid_Gestor.ClientID %>')
        txtGestor.value = sUsuNom + ' (' + sUsuEmail + ')'
        hidGestor.value = sUsuCod
    }
    </script>
