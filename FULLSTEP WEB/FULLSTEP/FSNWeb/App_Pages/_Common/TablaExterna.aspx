﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TablaExterna.aspx.vb" Inherits="Fullstep.FSNWeb.TablaExterna" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
	<title></title>
	<script type="text/javascript">
		function cerrarvent(){
			try{
				window.opener.wext=null;
			}
			catch(e){
			}
		}

		if (window.addEventListener) {
			window.document.addEventListener("close", cerrarvent, false)
		} else {
			if (window.attachEvent) 
				window.attachEvent("onclose", cerrarvent);
		}
		</script>
        <style type="text/css">
            tbody>tr>td..SelectedRow
            {
                color:White;
                background-color: #696969;
            }
        </style>
</head>
<body style="background-color: #FFFFFF; margin-top: 0px; margin-left: 0px; margin-right: 0px">

<script type="text/javascript">
    /*Funciones para paginacion*/

    function AdministrarPaginador() {
        var grid = $find("<%= whdgTabla.ClientID %>");
        var parentGrid = grid;
        $('[id$=lblCount]').text(parentGrid.get_gridView().get_behaviors().get_paging().get_pageCount());
        if (parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() == 0) {
            $('[id$=ImgBtnFirst]').attr('disabled', 'disabled');
            $('[id$=ImgBtnFirst]').attr('src', rutaTheme + '/images/primero_desactivado.gif');
            $('[id$=ImgBtnPrev]').attr('disabled', 'disabled');
            $('[id$=ImgBtnPrev]').attr('src', rutaTheme + '/images/anterior_desactivado.gif');
        } else {
            $('[id$=ImgBtnFirst]').removeAttr('disabled');
            $('[id$=ImgBtnFirst]').attr('src', rutaTheme + '/images/primero.gif');
            $('[id$=ImgBtnPrev]').removeAttr('disabled');
            $('[id$=ImgBtnPrev]').attr('src', rutaTheme + '/images/anterior.gif');
        }
        if (parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() == parentGrid.get_gridView().get_behaviors().get_paging().get_pageCount() - 1) {
            $('[id$=ImgBtnNext]').attr('disabled', 'disabled');
            $('[id$=ImgBtnNext]').attr('src', rutaTheme + '/images/siguiente_desactivado.gif');
            $('[id$=ImgBtnLast]').attr('disabled', 'disabled');
            $('[id$=ImgBtnLast]').attr('src', rutaTheme + '/images/ultimo_desactivado.gif');
        } else {
            $('[id$=ImgBtnNext]').removeAttr('disabled');
            $('[id$=ImgBtnNext]').attr('src', rutaTheme + '/images/siguiente.gif');
            $('[id$=ImgBtnLast]').removeAttr('disabled');
            $('[id$=ImgBtnLast]').attr('src', rutaTheme + '/images/ultimo.gif');
        }
    }
    function whdgTabla_PageIndexChanged(a, b, c, d) {
        var grid = $find("<%= whdgTabla.ClientID %>");
        var parentGrid = grid;
        var dropdownlist = $('[id$=PagerPageList]')[0];

        dropdownlist.options[parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex()].selected = true;
        AdministrarPaginador();
    }

    function IndexChanged() {
        
        var dropdownlist = $('[id$=PagerPageList]')[0];
        var grid = $find("<%= whdgTabla.ClientID %>");
        var parentGrid = grid.get_gridView();
        var newValue = dropdownlist.selectedIndex;
        parentGrid.get_behaviors().get_paging().set_pageIndex(newValue);
        AdministrarPaginador();
    }
    function FirstPage() {
        var grid = $find("<%= whdgTabla.ClientID %>");
        var parentGrid = grid;
        parentGrid.get_gridView().get_behaviors().get_paging().set_pageIndex(0);
    }
    function PrevPage() {
        var grid = $find("<%= whdgTabla.ClientID %>");
        var parentGrid = grid;
        var dropdownlist = $('[id$=PagerPageList]')[0];
        if (parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() > 0) {
            parentGrid.get_gridView().get_behaviors().get_paging().set_pageIndex(parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() - 1);
        }
    }
    function NextPage() {
        var grid = $find("<%= whdgTabla.ClientID %>");
        var parentGrid = grid;
        var dropdownlist = $('[id$=PagerPageList]')[0];
        if (parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() < parentGrid.get_gridView().get_behaviors().get_paging().get_pageCount() - 1) {
            parentGrid.get_gridView().get_behaviors().get_paging().set_pageIndex(parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() + 1);
        }
    }
    function LastPage() {
        var grid = $find("<%= whdgTabla.ClientID %>");
        var parentGrid = grid;
        parentGrid.get_gridView().get_behaviors().get_paging().set_pageIndex(parentGrid.get_gridView().get_behaviors().get_paging().get_pageCount() - 1);
    }
</script>
	<form id="Form1" method="post" runat="server">
        <asp:ScriptManager ID="t_externa" runat="server">
            <CompositeScript>
                <Scripts>
                    <asp:ScriptReference Path="~/js/jquery/jquery.min.js" />
                </Scripts>
            </CompositeScript>
        </asp:ScriptManager>
			<br>
			<asp:Label id="lblBuscar" runat="server" CssClass="parrafo">PrimaryKey</asp:Label>
			&nbsp;<asp:textbox id="txtBuscar" runat="server"></asp:textbox>
			&nbsp;<asp:button id="cmdBuscar" runat="server" Text="nBuscar" CssClass="boton"></asp:button><br>
			<br>
			<table cellSpacing="0" cellPadding="0" border="0">
				<tr>
					<td rowSpan="2">
                        <ig:WebHierarchicalDataGrid id="whdgTabla" runat="server" Height="330px" Width="660px"
                    AutoGenerateBands="false" AutoGenerateColumns="false" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="true">
					        <Behaviors>
                                <ig:Selection  RowSelectType="Single" Enabled="True" CellClickAction="Row"></ig:Selection> 
						        <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
						        <ig:ColumnMoving Enabled="true"></ig:ColumnMoving>
                                <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true"  AnimationEnabled="true">
                                     <FilteringClientEvents DataFiltered="AdministrarPaginador" /> 
                                </ig:Filtering>
                                <ig:Paging Enabled="true" PagerAppearance="Top" PageSize="20">
                                    <PagingClientEvents PageIndexChanged="whdgTabla_PageIndexChanged" />
                                   <PagerTemplate>
								    <div class="CabeceraBotones" style="clear:both; float:left; width:100%;"> 
									    <div style="float:left; padding:3px 0px 2px 0px; margin-left:10px;">
										    <div style="clear: both; float: left; margin-right: 5px;">
                                                <asp:Image ID="ImgBtnFirst" runat="server" OnClientClick="FirstPage()" style="margin-right:2px;" />
                                                <asp:Image ID="ImgBtnPrev" runat="server" OnClientClick="PrevPage()" />
                                            </div>
                                            <div style="float: left; margin-right:5px;">
                                                <asp:Label ID="lblPage" runat="server" Text="dPage" Style="margin-right: 3px;"></asp:Label>
                                                <asp:DropDownList ID="PagerPageList" runat="server" onchange="return IndexChanged()" Style="margin-right: 3px;" />
                                                <asp:Label ID="lblOF" runat="server" Text="dOf" Style="margin-right: 3px;"></asp:Label>
                                                <asp:Label ID="lblCount" runat="server" />
                                            </div>
                                            <div style="float: left;">
                                                <asp:Image ID="ImgBtnNext" runat="server" OnClientClick="NextPage()" style="margin-right:2px;" />
                                                <asp:Image ID="ImgBtnLast" runat="server" OnClientClick="LastPage()" />
                                            </div>
									    </div>   
                                    </div>  
                                    </PagerTemplate>
                                </ig:Paging>		
                            </Behaviors>
					    </ig:WebHierarchicalDataGrid>
                    </td>					
				</tr>
			</table>
			<br>
			<div align="center"><asp:button id="cmdAceptar" runat="server" Text="nAceptar" CssClass="boton"></asp:button></div>
			<input id="hddDataEntry" type="hidden" name="hddDataEntry" runat="server"> <input id="hddFilaIni" type="hidden" name="hddFilaIni" runat="server">
			<input id="hddOrden" type="hidden" name="hddOrden" runat="server">
		</form>
</body>
</html>
