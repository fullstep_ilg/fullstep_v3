﻿Imports Infragistics.Web.UI.GridControls
Imports Infragistics.Web.UI.ListControls
Imports System.Linq

Partial Public Class BuscadorUsuarios
    Inherits FSNPage

#Region "Constantes"
    Private Const UO_VALUE_CELL = 0
    Private Const UO_TEXT_CELL = 4
#End Region
#Region "Variables privadas"
    Private m_sCualquiera As String
    Private m_sUsuario As String
    Private m_lRol As Integer
    Private m_lInstancia As Integer
    Private m_sUO As String
    Private m_UOIndex As Integer?
    Private m_Departamento As String
    Private m_sMaterial As String
    Private m_Remitente As String
    Private m_Comprador As String
    Private m_Equipo As String
    Protected WithEvents ugtxtUO_wdg As Infragistics.Web.UI.GridControls.WebDataGrid
#End Region
#Region "Inicialización"
    ''' <summary>
    ''' Carga la pantalla
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Objeto con los datos del evento</param>
    ''' <remarks>Llamada desde: guardarinstancia.aspx       NWrealizarAccion.aspx      jsAlta.js     altaContrato.aspx   
    ''' DetalleContrato.aspx  altaNoconformidad.aspx   detalleNoConformidad.aspx    BuscadorSolicitudes.aspx; Tiempo máximo:0 </remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Usuarios
        FSNPageHeader.WidthImagenCabecera = "40px"
        FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/usuario_buscar.gif"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaTheme", "<script>var rutaTheme = '" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "'</script>")

        'Obtenemos el grid de la plantilla del combo
        ugtxtUO_wdg = ugtxtUO.Items(0).FindControl("ugtxtUO_wdg")

        'Obtenemos los parámetros
        If Request("desde") <> Nothing Then
            Desde.Value = Request.QueryString("desde")
            m_Remitente = Desde.Value
        End If
        If Request("IdControl") <> Nothing Then IdControl.Value = Request("IdControl")
        If Request("IdHidControl") <> Nothing Then IdHidControl.Value = Request("idHidControl")
        If Request("Rol") <> Nothing Then
            Rol.Value = CInt(Request("Rol"))
            m_lRol = Rol.Value
        End If
        'OJO, esto es el índice de la UO dentro del combo
        If Request("UO") <> Nothing Then m_UOIndex = IIf(Request("UO") = String.Empty Or Request("UO") = "undefined", 0, CInt(Request("UO")))
        If Request("DEP") <> Nothing Then m_Departamento = Request("DEP")
        If Request("Instancia") <> Nothing Then m_lInstancia = CInt(Request("Instancia"))
        If Request("IdFilaGrid") <> Nothing Then FilaGrid.Value = Request("idFilaGrid")
        If Request("sUO") <> Nothing Then m_sUO = Request("sUO")
        If Request("sMat") <> Nothing Then m_sMaterial = Request("sMat")
        If Request("Nombre") <> Nothing Then ugtxtNombre.Text = Request("Nombre")
        If Request("Ape") <> Nothing Then ugtxtApe.Text = Request("Ape")
        If Request("Comprador") <> Nothing Then m_Comprador = Request("Comprador")
        If Request("EQP") <> Nothing Then m_Equipo = Request("EQP")
        If Request("Multiple") <> Nothing Then Multiple.Value = Request("Multiple")
        ' En estos casos la selección es múltiple
        If Not m_Remitente = Nothing Then
            If InStr(m_Remitente, "AltaContratos") >= 0 Or InStr(m_Remitente, "QA") >= 0 Then
                Multiple.Value = "1"
            End If
        End If

        If Desde.Value = "showPersonas" Then
            Multiple.Value = ""

            If FSNUser.PMRestringirPersonasDepartamentoUsuario Then 'la restricción de seguridad "Restringir a personas del departamento del usuario"
                ugtxtUO.Enabled = False
                ugtxtDEP.Enabled = False
            ElseIf FSNUser.PMRestringirPersonasUOUsuario Then 'la restricción de seguridad "Restringir a personas de la unidad organizativa del usuario"
                ugtxtUO.Enabled = False
            End If
        End If

        'Cargamos los textos de la página
        CargarTextos()

        'Carga los datos seleccionados:        
        If Not Page.IsPostBack Then
            Cache.Remove("oUsuBusqPrv_" & FSNUser.Cod)

            ' Variaciones según el origen de la página
            If m_Remitente = "QA" Then
                Me.lblPulsar.Visible = False
                If FSNUser.FSQARestNotificadosInternosUO Then ugtxtUO.Enabled = False
                If FSNUser.FSQARestNotificadosInternosDep Then ugtxtDEP.Enabled = False
            End If

            ' Establecemos las imagenes y eventos de los controles de paginación
            EstablecerPaginacion()

            'Cargamos el combo de las unidades organizativas
            CargarUnidadesOrganizativas()

            'En caso de que venga por parámetro un índice de UO, seleccionamos la UO correspondiente
            If m_UOIndex <> Nothing Then
                UO_SeleccionPorIndice(m_UOIndex)
            End If

            'Cargamos el combo de departamentos
            CargarDepartamentos()

            'En caso de que venga por parámetro un departamento, lo seleccionamos
            If (m_Departamento <> Nothing) Then
                ugtxtDEP.SelectedValue = m_Departamento
            End If

            If m_Comprador = "1" Then
                'Carga la combo de equipos
                CargarEquipos()

                'Si viene por parámetro un equipo de compra, lo seleccionamos
                If Not String.IsNullOrWhiteSpace(m_Equipo) <> Nothing Then
                    ugtxtEqp.SelectedValue = m_Equipo
                End If
            End If

            ' Si la selección es simple ocultamos el botón de aceptar
            If String.IsNullOrEmpty(Multiple.Value) Then
                cmdAceptar.Visible = False
            End If

            'Comprobamos si los parámetros del filtro tienen valor para saber si debemos buscar al inicio
            If Len(m_UOIndex & m_Departamento & ugtxtNombre.Text & ugtxtApe.Text & m_Equipo & m_sMaterial & m_sUO) > 0 Then
                Buscar()
            End If
        Else
            'Cargamos los datos de la caché en el grid
            CargarGrid()
        End If
    End Sub
    ''' <summary>
    ''' Establece los textos de los controles de acuerdo con el idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load; Tiempo máximo:0</remarks> 
    Private Sub CargarTextos()
        '-- Cabecera
        If m_Comprador = "1" Then
            FSNPageHeader.TituloCabecera = Textos(16)
        ElseIf m_Remitente = "QA" Then
            FSNPageHeader.TituloCabecera = Textos(13)
        ElseIf IdControl.Value <> Nothing AndAlso Not m_Remitente = "VisorSol" Then
            FSNPageHeader.TituloCabecera = Textos(12)
        ElseIf FilaGrid.Value <> Nothing OrElse m_Remitente = "VisorSol" Then
            FSNPageHeader.TituloCabecera = Textos(11)
        Else
            FSNPageHeader.TituloCabecera = Textos(0)
        End If

        'Filtros
        lblUO.Text = Textos(1) + ":"
        lblNombre.Text = Textos(2) + ":"
        lblApe.Text = Textos(3) + ":"
        lblDep.Text = Textos(4) + ":"
        lblCodigo.Text = Textos(23) + ":"
        lblEqp.Text = Textos(17) + ":"

        m_sCualquiera = Textos(8)
        cmdBuscar.Text = Textos(5)
        lblResult.Text = Textos(6)

        If m_Comprador = "1" Then
            lblPulsar.Text = Textos(18)
            lblEqp.Visible = True
            ugtxtEqp.Visible = True
            m_sUsuario = Textos(19)
        Else
            lblPulsar.Text = Textos(7)
            m_sUsuario = Textos(9)
        End If

        cmdAceptar.Text = Textos(14)

        'Paginación
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Notificaciones
        CType(whdgUsuarios.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPage"), Label).Text = Textos(46)
        CType(whdgUsuarios.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblOF"), Label).Text = Textos(48)
        'Vuelvo a cargar el modulo de usuarios
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Usuarios
    End Sub
#End Region
#Region "Carga de Datos"
    ''' <summary>
    ''' Carga el combo de unidades organizativas
    ''' </summary>    
    Private Sub CargarUnidadesOrganizativas()
        If Desde.Value = "CompartirEscenario" Then
            Dim dsUONsCompartir As DataSet

            Dim cEscenarios As FSNServer.Escenarios
            cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
            With FSNUser
                dsUONsCompartir = cEscenarios.FSN_Obtener_UONsCompartir(.Cod, .Idioma, .UON1, .UON2, .UON3, .Dep,
                                    .PMRestringirCrearEscenariosUsuariosUONUsu, .PMRestringirCrearEscenariosUsuariosDEPUsu, .PMRestringirCrearEscenariosUsuariosUONsPerfilUsu)
                InsertarEnCache("dsUONsCompartir" & FSNUser.Cod, dsUONsCompartir)
                Dim oUonsTodos = dsUONsCompartir.Tables(0).Rows.OfType(Of DataRow).Where(Function(x) _
                                            x("ESDEPARTAMENTO") = 0 AndAlso (x("PERMISO") = 1 OrElse FSNUser.UON1 Is Nothing)
                                            ).Select(Function(x) New With {Key .ID = ",,", .COD_UON1 = "", .COD_UON2 = "", .COD_UON3 = "", .DEN = x("DEN").ToString}).ToList()
                If dsUONsCompartir.Tables.Count > 1 Then
                    oUonsTodos = oUonsTodos.Union(dsUONsCompartir.Tables(1).Rows.OfType(Of DataRow).Where(Function(x) _
                                            x("ESDEPARTAMENTO") = 0 AndAlso (x("PERMISO") = 1 OrElse (x("COD") = FSNUser.UON1 And FSNUser.UON2 Is Nothing))
                                    ).Select(Function(x) New With {Key .ID = x("COD").ToString & ",,", .COD_UON1 = x("COD").ToString, .COD_UON2 = "", .COD_UON3 = "", .DEN = x("COD").ToString & " - " & x("DEN").ToString}).ToList()).ToList()
                    If dsUONsCompartir.Tables.Count > 2 Then
                        oUonsTodos = oUonsTodos.Union(dsUONsCompartir.Tables(2).Rows.OfType(Of DataRow).Where(Function(x) _
                                            x("ESDEPARTAMENTO") = 0 AndAlso (x("PERMISO") = 1 OrElse (x("UON1") = FSNUser.UON1 AndAlso x("COD") = FSNUser.UON2 AndAlso FSNUser.UON3 Is Nothing))
                                    ).Select(Function(x) New With {Key .ID = x("UON1").ToString & "," & x("COD").ToString & ",", .COD_UON1 = x("UON1").ToString, .COD_UON2 = x("COD").ToString, .COD_UON3 = "",
                                                                   .DEN = x("UON1").ToString & " - " & x("COD").ToString & " - " & x("DEN").ToString}).ToList()).ToList()
                        If dsUONsCompartir.Tables.Count > 3 Then
                            oUonsTodos = oUonsTodos.Union(dsUONsCompartir.Tables(3).Rows.OfType(Of DataRow).Where(Function(x) _
                                            x("ESDEPARTAMENTO") = 0 AndAlso (x("PERMISO") = 1 OrElse (x("UON1") = FSNUser.UON2 AndAlso x("UON2") = FSNUser.UON2 AndAlso x("COD") = FSNUser.UON3))
                                    ).Select(Function(x) New With {Key .ID = x("UON1").ToString & "," & x("UON2").ToString & "," & x("COD").ToString, .COD_UON1 = x("UON1").ToString, .COD_UON2 = x("UON2").ToString, .COD_UON3 = x("COD").ToString,
                                                                   .DEN = x("UON1").ToString & " - " & x("UON2").ToString & " - " & x("COD").ToString & " - " & x("DEN").ToString}).ToList()).ToList()
                        End If
                    End If
                End If
                ugtxtUO_wdg.DataSource = oUonsTodos
                ugtxtUO_wdg.DataBind()

                ' Establecemos el texto de la opción "Cualquiera"
                If (ugtxtUO_wdg.Rows.Count > 0 AndAlso CType(ugtxtUO_wdg.Rows(0).DataItem, Object).ID = "0") Then
                    CType(ugtxtUO_wdg.Rows(0).DataItem, Object).ID = m_sCualquiera
                End If
            End With
        Else
            Dim oUnidadesUO As FSNServer.UnidadesOrg = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))
            'Cargamos los datos de BB.DD
            If m_Remitente = "QA" Then
                oUnidadesUO.CargarUnidadesOrganizativasQA(FSNUser.Cod, FSNUser.FSQARestNotificadosInternosUO, Idioma)
            ElseIf FilaGrid.Value <> Nothing Then
                oUnidadesUO.CargarUnidadesOrganizativas(Idioma)
            ElseIf m_Remitente = "showPersonas" Then
                'la restricción de seguridad "Restringir a personas del departamento del usuario"
                'la restricción de seguridad "Restringir a personas de la unidad organizativa del usuario"
                'Ambas restricciones bloquean el combo de Organización y deben ser la uo de la persona.
                oUnidadesUO.CargarUnidadesOrganizativas(Idioma, FSNUser.Cod, FSNUser.PMRestringirPersonasUOUsuario OrElse FSNUser.PMRestringirPersonasDepartamentoUsuario)
            Else
                oUnidadesUO.CargarUnidadesOrganizativas(Idioma, FSNUser.Cod, FSNUser.PMRestringirPersonasUOUsuario)
            End If

            'Añadimos una columna al resultado para establecer el campo de valor 
            UO_CrearColumnaID(oUnidadesUO.Data.Tables(0))

            'Enlazamos los datos al grid
            ugtxtUO_wdg.DataSource = oUnidadesUO.Data
            ugtxtUO_wdg.DataBind()
            oUnidadesUO = Nothing

            ' Establecemos el texto de la opción "Cualquiera"
            If (ugtxtUO_wdg.Rows.Count > 0 AndAlso ugtxtUO_wdg.Rows(0).DataItem(ugtxtUO.ValueField) = "0") Then
                ugtxtUO_wdg.Rows(0).DataItem(ugtxtUO.TextField) = m_sCualquiera
            End If
        End If

        'Seleccionamos la primera opción
        UO_SeleccionPorIndice(0)
    End Sub
    ''' <summary>
    ''' Carga los departamentos en función de la unidad organizativa seleccionada
    ''' </summary>   
    ''' <remarks>LLamada desde: Page_Load; Tiempo máximo: 0,2 sg</remarks>
    Private Sub CargarDepartamentos()
        Dim oDepartamentos As FSNServer.Departamentos
        Dim iNivel As Integer
        Dim UON1, UON2, UON3 As String
        UON1 = String.Empty : UON2 = String.Empty : UON3 = String.Empty
        'Obtenemos el valor de la UO
        Dim sUONs = UO_Seleccion.Key
        If sUONs = "0" Then
            iNivel = 0
        Else
            Dim arrUONs = Split(sUONs, ",")
            'Calculamos el nivel en base a los ids de UONs
            iNivel = arrUONs.Select(Function(s, i) New With {i, s}).Where(Function(t) t.s <> String.Empty).Select(Function(t) t.i).DefaultIfEmpty(-1).Max() + 1

            'Rellenamos las variables de los ids
            Try
                UON1 = If(String.IsNullOrEmpty(arrUONs(0)), Nothing, arrUONs(0))
                UON2 = If(String.IsNullOrEmpty(arrUONs(1)), Nothing, arrUONs(1))
                UON3 = If(String.IsNullOrEmpty(arrUONs(2)), Nothing, arrUONs(2))
            Catch
            End Try
        End If

        If Desde.Value = "CompartirEscenario" Then
            Dim dsUONsCompartir As DataSet = CType(Cache("dsUONsCompartir" & FSNUser.Cod), DataSet).Copy
            Dim oDepTodos = dsUONsCompartir.Tables(0).Rows.OfType(Of DataRow).Select(Function(x) New With {Key .COD = "", .DEN = "", .COD_DEN = ""}).ToList()
            If dsUONsCompartir.Tables.Count > 1 Then
                oDepTodos = oDepTodos.Union(dsUONsCompartir.Tables(1).Rows.OfType(Of DataRow).Where(Function(x) _
                                        x("ESDEPARTAMENTO") = 1 AndAlso x("PERMISO") = 1 AndAlso (UON1 Is Nothing)
                                ).Select(Function(x) New With {Key .COD = x("COD").ToString, .DEN = x("DEN").ToString, .COD_DEN = x("COD").ToString & " - " & x("DEN").ToString}).ToList()).ToList()
                If dsUONsCompartir.Tables.Count > 2 Then
                    oDepTodos = oDepTodos.Union(dsUONsCompartir.Tables(2).Rows.OfType(Of DataRow).Where(Function(x) _
                                        x("ESDEPARTAMENTO") = 1 AndAlso x("PERMISO") = 1 AndAlso x("UON1") = UON1 AndAlso UON2 Is Nothing AndAlso UON3 Is Nothing
                                ).Select(Function(x) New With {Key .COD = x("COD").ToString, .DEN = x("DEN").ToString, .COD_DEN = x("COD").ToString & " - " & x("DEN").ToString}).ToList()).ToList()
                    If dsUONsCompartir.Tables.Count > 3 Then
                        oDepTodos = oDepTodos.Union(dsUONsCompartir.Tables(3).Rows.OfType(Of DataRow).Where(Function(x) _
                                        x("ESDEPARTAMENTO") = 1 AndAlso x("PERMISO") = 1 AndAlso x("UON1") = UON1 AndAlso x("UON2") = UON2 AndAlso UON3 Is Nothing
                                ).Select(Function(x) New With {Key .COD = x("COD").ToString, .DEN = x("DEN").ToString, .COD_DEN = x("COD").ToString & " - " & x("DEN").ToString}).ToList()).ToList()
                        If dsUONsCompartir.Tables.Count > 4 Then
                            oDepTodos = oDepTodos.Union(dsUONsCompartir.Tables(4).Rows.OfType(Of DataRow).Where(Function(x) _
                                        x("ESDEPARTAMENTO") = 1 AndAlso x("PERMISO") = 1 AndAlso x("UON1") = UON1 AndAlso x("UON2") = UON2 AndAlso x("UON3") = UON3
                                ).Select(Function(x) New With {Key .COD = x("COD").ToString, .DEN = x("DEN").ToString, .COD_DEN = x("COD").ToString & " - " & x("DEN").ToString}).ToList()).ToList()
                        End If
                    End If
                End If
            End If
            'Vinculamos los datos al combo
            ugtxtDEP.DataSource = oDepTodos
            ugtxtDEP.DataBind()

            'Establecemos el texto de la opción cualquiera
            If ugtxtDEP.Items.Count > 0 AndAlso String.IsNullOrEmpty(ugtxtDEP.Items(0).Value) Then
                ugtxtDEP.Items(0).Text = m_sCualquiera
            End If

            'Si el usuario tiene la restricción a sólo personas de su departamento, se omite la opción cualquiera
            If FSNUser.PMRestringirCrearEscenariosUsuariosDEPUsu AndAlso FSNUser.PMRestringirCrearEscenariosUsuariosUONUsu AndAlso Not FSNUser.PMRestringirCrearEscenariosUsuariosUONsPerfilUsu Then
                ugtxtDEP.Items(0).Visible = False
                m_Departamento = ugtxtDEP.Items(1).Value
            End If
            'Si el usuario tiene restricción de Departamento pero no de UO, y no ha seleccionado su propia UO, vaciamos las opciones de Departamentos
            If FSNUser.PMRestringirCrearEscenariosUsuariosDEPUsu AndAlso Not FSNUser.PMRestringirCrearEscenariosUsuariosUONsPerfilUsu AndAlso Not FSNUser.PMRestringirCrearEscenariosUsuariosUONUsu Then
                ugtxtDEP.Items.Clear()
            End If
        Else
            'Cargamos los datos de BB.DD.
            oDepartamentos = FSNServer.Get_Object(GetType(FSNServer.Departamentos))
            If m_Remitente = "QA" Then
                oDepartamentos.LoadDataQA(iNivel, UON1, UON2, UON3, FSNUser.Cod, FSNUser.FSQARestNotificadosInternosDep)
            Else
                oDepartamentos.LoadData(iNivel, UON1, UON2, UON3, FSNUser.Cod, RestricDEPTraslado:=FSNUser.PMRestringirPersonasDepartamentoUsuario)
            End If

            'Vinculamos los datos al combo
            ugtxtDEP.DataSource = oDepartamentos.Data
            ugtxtDEP.DataBind()
            oDepartamentos = Nothing

            'Establecemos el texto de la opción cualquiera
            If ugtxtDEP.Items.Count > 0 AndAlso String.IsNullOrEmpty(ugtxtDEP.Items(0).Value) Then
                ugtxtDEP.Items(0).Text = m_sCualquiera
            End If

            'Si el usuario tiene la restricción a sólo personas de su departamento, se omite la opción cualquiera
            If (m_Remitente = "QA" And FSNUser.FSQARestNotificadosInternosDep) OrElse FSNUser.PMRestringirPersonasDepartamentoUsuario Then
                ugtxtDEP.Items(0).Visible = False
            End If
            'Si el usuario tiene restricción de Departamento pero no de UO, y no ha seleccionado su propia UO, vaciamos las opciones de Departamentos
            If m_Remitente <> "QA" And FSNUser.PMRestringirPersonasDepartamentoUsuario And Not FSNUser.PMRestringirPersonasUOUsuario And (FSNUser.UON1 <> UON1 Or FSNUser.UON2 <> UON2 Or FSNUser.UON3 <> UON3) Then
                ugtxtDEP.Items.Clear()
            End If
        End If

        'Seleccionamos el primer valor visible
        For Each item As DropDownItem In ugtxtDEP.Items
            If item.Visible Then
                ugtxtDEP.SelectedValue = item.Value
                Exit For
            End If
        Next
    End Sub
    ''' <summary>
    ''' Carga los Equipos de Compra
    ''' </summary>    
    Private Sub CargarEquipos()
        Dim oEquipos As FSNServer.Equipos

        'Cargamos los datos de BB.DD.
        oEquipos = FSNServer.Get_Object(GetType(FSNServer.Equipos))
        oEquipos.Load()
        ugtxtEqp.DataSource = oEquipos.Data
        ugtxtEqp.DataBind()
        oEquipos = Nothing

        'Establecemos el texto de la opción cualquiera
        ugtxtEqp.Items(0).Text = m_sCualquiera

        'Seleccionamos el valor por defecto       
        ugtxtEqp.SelectedItemIndex = 0
    End Sub
#End Region
#Region "Búsqueda"
    ''' <summary>
    ''' Carga en el grid los usuarios que cumplen las condiciones señaladas en los combo superiores
    ''' dependiendo de las restricciones que tiene el usuario que lanza la busqueda
    ''' </summary>
    ''' <remarks></remarks>    
    Private Sub Buscar()
        Dim sUONs As String = UO_Seleccion.Key
        Dim sUON1 As String = Nothing
        Dim sUON2 As String = Nothing
        Dim sUON3 As String = Nothing
        Dim sDEP As String = If(String.IsNullOrWhiteSpace(ugtxtDEP.SelectedValue), Nothing, ugtxtDEP.SelectedValue)
        Dim sNombre As String = IIf(String.IsNullOrWhiteSpace(ugtxtNombre.Value), Nothing, Trim(ugtxtNombre.Value))
        Dim sApellidos As String = IIf(String.IsNullOrWhiteSpace(ugtxtApe.Value), Nothing, Trim(ugtxtApe.Value))
        Dim sCodigo As String = IIf(String.IsNullOrWhiteSpace(ugtxtCodigo.Value), Nothing, Trim(ugtxtCodigo.Value))
        Dim datosPersona As DataSet
        Dim sDataKeyFields As String = "COD"

        'Rellenamos las variables de los ids de UONs
        If sUONs <> "0" Then
            Dim arrUONs = Split(sUONs, ",")
            Try
                sUON1 = If(String.IsNullOrEmpty(arrUONs(0)), Nothing, arrUONs(0))
                sUON2 = If(String.IsNullOrEmpty(arrUONs(1)), Nothing, arrUONs(1))
                sUON3 = If(String.IsNullOrEmpty(arrUONs(2)), Nothing, arrUONs(2))
            Catch
            End Try
        End If

        If Desde.Value = "CompartirEscenario" Then
            Dim cEscenarios As FSNServer.Escenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
            With FSNUser
                datosPersona = cEscenarios.FSN_Obtener_USUCompartir(.Cod, .Idioma, sCodigo, sNombre, sApellidos, Nothing, .UON1, .UON2, .UON3, .Dep,
                                        .PMRestringirCrearEscenariosUsuariosUONUsu, .PMRestringirCrearEscenariosUsuariosDEPUsu, .PMRestringirCrearEscenariosUsuariosUONsPerfilUsu,
                                        sUON1, sUON2, sUON3, sDEP)
            End With
        Else
            'Obtenemos los datos de BB.DD.. La consulta varía dependiendo de algunos parámetros
            If m_lRol <> Nothing Then
                Dim oPersonas As FSNServer.Participantes = FSNServer.Get_Object(GetType(FSNServer.Participantes))
                oPersonas.Instancia = m_lInstancia
                oPersonas.Rol = m_lRol
                sDataKeyFields = "PARTCOD"
                oPersonas.LoadDataPersonas(sCod:=sCodigo, sNom:=sNombre, sApe:=sApellidos, sUON1:=sUON1, sUON2:=sUON2, sUON3:=sUON3, sDep:=sDEP)
                datosPersona = oPersonas.Data
            Else
                Dim oPersonas As FSNServer.Personas = FSNServer.Get_Object(GetType(FSNServer.Personas))
                If m_Comprador = "1" Then
                    ' Si el valor seleccionado de "Equipos de Compra" es "" (Cualquiera) significa que se filtran todos los usuarios que tengan equipo (IS NOT NULL)
                    oPersonas.LoadData(sCod:=sCodigo, sNom:=sNombre, sApe:=sApellidos, sUON1:=sUON1, sUON2:=sUON2, sUON3:=sUON3, sDEP:=sDEP, bPM:=True, sEqp:=ugtxtEqp.SelectedValue, sUO:=m_sUO, sMaterial:=m_sMaterial)
                ElseIf m_Remitente = "AltaContratos" Then
                    oPersonas.LoadData(sCod:=sCodigo, sNom:=sNombre, sApe:=sApellidos, sUON1:=sUON1, sUON2:=sUON2, sUON3:=sUON3, sDEP:=sDEP, bCM:=True)
                ElseIf m_Remitente = "PM" Then 'Mira que tenga usuario con acceso a PM
                    oPersonas.LoadData(sCod:=sCodigo, sNom:=sNombre, sApe:=sApellidos, sUON1:=sUON1, sUON2:=sUON2, sUON3:=sUON3, sDEP:=sDEP, bPM:=True)
                Else 'no mira si tiene usuario, para QA, show_personas, IM
                    oPersonas.LoadData(sCod:=sCodigo, sNom:=sNombre, sApe:=sApellidos, sUON1:=sUON1, sUON2:=sUON2, sUON3:=sUON3, sDEP:=sDEP)
                End If
                datosPersona = oPersonas.Data
            End If
        End If

        'Añadimos una columna calculada para mostrar Nombre y Apellidos del USUARIO
        With datosPersona.Tables(0)
            .Columns.Add(New DataColumn("USUARIO", Type.GetType("System.String"), "ISNULL(NOM, '') + ' ' + ISNULL(APE, '')"))
        End With

        'Limpiamos el grid de resultados
        whdgUsuarios.GridView.Rows.Clear()
        For i = whdgUsuarios.GridView.Columns.Count - 1 To 1 Step -1
            whdgUsuarios.GridView.Columns.RemoveAt(i)
        Next
        whdgUsuarios.GridView.ClearDataSource()
        'Vinculamos los datos al grid
        whdgUsuarios.DataSource = datosPersona
        whdgUsuarios.GridView.DataSource = whdgUsuarios.DataSource
        whdgUsuarios.GridView.DataMember = "Table"
        whdgUsuarios.GridView.DataKeyFields = sDataKeyFields
        'Creamos las columnas del grid
        CrearColumnas()
        whdgUsuarios.DataBind()
        'Formateamos el grid
        ConfigurarGrid()

        'Insertamos el resultado en la caché        
        Me.InsertarEnCache("oUsuBusqPrv_" & FSNUser.Cod, datosPersona)
    End Sub
    ''' <summary>
    ''' Carga el grid con los datos de caché
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarGrid()
        'Si no hay datos en la caché, salimos
        If Cache("oUsuBusqPrv_" & FSNUser.Cod) Is Nothing Then Exit Sub

        'Limpiamos el grid de resultados
        whdgUsuarios.GridView.Rows.Clear()
        For i = whdgUsuarios.GridView.Columns.Count - 1 To 1 Step -1
            whdgUsuarios.GridView.Columns.RemoveAt(i)
        Next
        whdgUsuarios.GridView.ClearDataSource()
        'Vinculamos los datos de caché
        whdgUsuarios.DataSource = CType(Cache("oUsuBusqPrv_" & FSNUser.Cod), DataSet)
        whdgUsuarios.GridView.DataSource = whdgUsuarios.DataSource
        whdgUsuarios.GridView.DataMember = "Table"
        If m_lRol = Nothing Then
            whdgUsuarios.GridView.DataKeyFields = "COD"
        Else
            whdgUsuarios.GridView.DataKeyFields = "PARTCOD"
        End If
        CrearColumnas()
        whdgUsuarios.DataBind()
        ConfigurarGrid()
    End Sub
#End Region
#Region "Eventos"
    ''' <summary>
    ''' Tras la carga del grid se "sincroniza" con el Custom Pager
    ''' </summary>
    ''' <param name="sender">control</param>
    ''' <param name="e">evento de sistema</param>  
    ''' <remarks>Llamada desde: sistema; Tiempo mÃ¡ximo:0 </remarks>
    Private Sub whdgUsuarios_DataBound(sender As Object, e As System.EventArgs) Handles whdgUsuarios.DataBound
        ' Refrescamos los controles de paginación
        Dim pagerList As DropDownList = DirectCast(whdgUsuarios.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("PagerPageList"), DropDownList)
        pagerList.Items.Clear()
        For i As Integer = 1 To whdgUsuarios.GridView.Behaviors.Paging.PageCount
            pagerList.Items.Add(i.ToString())
        Next
        CType(whdgUsuarios.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblCount"), Label).Text = whdgUsuarios.GridView.Behaviors.Paging.PageCount
        Dim SoloUnaPagina As Boolean = (whdgUsuarios.GridView.Behaviors.Paging.PageCount = 1)
        With CType(whdgUsuarios.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
            .Enabled = Not SoloUnaPagina
        End With
        With CType(whdgUsuarios.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
            .Enabled = Not SoloUnaPagina
        End With
    End Sub
    Private Sub cmdBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBuscar.Click
        Buscar()
    End Sub
    Public Sub ugtxtUO_wdg_RowSelectionChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.SelectedRowEventArgs) Handles ugtxtUO_wdg.RowSelectionChanged
        CargarDepartamentos()
    End Sub
#End Region
#Region "Métodos auxiliares"
    ''' <summary>
    ''' Crea una columna ID para establecer el valor seleccionado el combo de unidades organizativas
    ''' </summary>
    ''' <param name="dt">DataTable en el que se creará la columna</param>    
    Private Sub UO_CrearColumnaID(dt As DataTable)
        'Añadimos la columna
        dt.Columns.Add(New DataColumn("ID", System.Type.GetType("System.String")))

        'Rellenamos los valores de la columna
        For Each row As DataRow In dt.Rows
            If IsDBNull(row("DEN")) Then
                row("ID") = "0"
            Else
                row("ID") = row("COD_UON1") + "," + row("COD_UON2") + "," + row("COD_UON3")
            End If
        Next
    End Sub
    ''' <summary>
    ''' Obtiene o establece el valor seleccionado del WebDropDown de Unidades Organizativas
    ''' </summary>
    ''' <param name="value">Valor a establecer</param>
    ''' <returns>Valor del WebDropDown</returns>    
    Private Function UO_Seleccion(Optional ByVal value As String = Nothing) As KeyValuePair(Of String, String)
        Dim text As String = String.Empty
        If Not value Is Nothing Then
            'Establecemos el valor
            Dim row As GridRecord = ugtxtUO_wdg.Rows.OfType(Of GridRecord).SingleOrDefault(Function(x) x.Items(UO_VALUE_CELL).Text.Equals(value))

            If (Not row Is Nothing) Then
                ugtxtUO_wdg.Behaviors.Selection.SelectedRows.Clear()
                ugtxtUO_wdg.Behaviors.Selection.SelectedRows.Add(row)
                ugtxtUO.CurrentValue = row.Items(UO_TEXT_CELL).Text
            End If
        End If

        'Devolvemos el valor
        If ugtxtUO_wdg.Behaviors.Selection.SelectedRows.Count > 0 Then
            value = ugtxtUO_wdg.Behaviors.Selection.SelectedRows(0).Items(UO_VALUE_CELL).Text
            text = ugtxtUO_wdg.Behaviors.Selection.SelectedRows(0).Items(UO_TEXT_CELL).Text
        Else
            value = Nothing
        End If

        Return New KeyValuePair(Of String, String)(value, text)
    End Function
    ''' <summary>
    ''' Establece la UO seleccionada en base a un índice de la lista
    ''' </summary>
    ''' <param name="idx">Índice a seleccionar</param>    
    Private Sub UO_SeleccionPorIndice(idx As Integer)
        'Establecemos el valor
        Dim row As GridRecord
        Try
            row = ugtxtUO_wdg.Rows(idx)

            If (Not row Is Nothing) Then
                ugtxtUO_wdg.Behaviors.Selection.SelectedRows.Clear()
                ugtxtUO_wdg.Behaviors.Selection.SelectedRows.Add(row)
                ugtxtUO.CurrentValue = row.Items(UO_TEXT_CELL).Text
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Establece las imagenes y eventos de los controles de paginación
    ''' </summary>    
    Private Sub EstablecerPaginacion()
        With CType(whdgUsuarios.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero_desactivado.gif"
            .Attributes.Add("onClick", "return FirstPage();")
            .Enabled = False
        End With
        With CType(whdgUsuarios.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior_desactivado.gif"
            .Attributes.Add("onClick", "return PrevPage();")
            .Enabled = False
        End With
        Dim SoloUnaPagina As Boolean = True
        With CType(whdgUsuarios.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
            .Attributes.Add("onClick", "return NextPage();")
            .Enabled = Not SoloUnaPagina
        End With
        With CType(whdgUsuarios.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
            .Attributes.Add("onClick", "return LastPage();")
            .Enabled = Not SoloUnaPagina
        End With
    End Sub
    ''' <summary>
    ''' Crear Columnas en grid
    ''' </summary>
    ''' <remarks>Llamada desde: Buscar / CargarGrid ; Tiempo máximo:0</remarks>
    Private Sub CrearColumnas()
        Dim nombre As String

        For i = 0 To whdgUsuarios.DataSource.Tables(0).Columns.Count - 1
            nombre = whdgUsuarios.DataSource.Tables(0).Columns(i).ColumnName

            whdgUsuarios.GridView.Columns.Add(New BoundDataField(True) With {
                                              .Key = nombre,
                                              .DataFieldName = nombre,
                                              .VisibleIndex = i + 1,
                                              .Hidden = True
                                          })
        Next
    End Sub
    ''' <summary>
    ''' Configura Columnas en grid. Visibilidad, ancho y textos
    ''' </summary>
    ''' <remarks>Llamada desde: Buscar / CargarGrid ; Tiempo máximo:0</remarks>
    Private Sub ConfigurarGrid()
        Dim sColumnasVisibles As String
        ' Definimos qué campos se deben ver
        With whdgUsuarios.GridView
            If m_lRol = Nothing Then
                sColumnasVisibles = "[SEL][COD][NOM_DEP][USUARIO]"
                If m_Comprador = "1" Then
                    sColumnasVisibles &= "[EQUIPO_DEN]"
                    .Columns("EQUIPO_DEN").Header.Text = Textos(17)
                End If

                ' Establecemos los textos de las Cabeceras
                .Columns("SEL").Header.Text = "SEL"
                .Columns("COD").Header.Text = Textos(23)
                .Columns("NOM_DEP").Header.Text = Textos(4)
                .Columns("USUARIO").Header.Text = m_sUsuario

                ' Establecemos las anchuras de los campos
                .Columns("COD").Width = Unit.Percentage(10)
                .Columns("NOM_DEP").Width = Unit.Percentage(30)
            Else
                sColumnasVisibles = "[SEL][PARTCOD][NOM_DEP][USUARIO]"
                ' Establecemos los textos de las Cabeceras
                .Columns("SEL").Header.Text = "SEL"
                .Columns("PARTCOD").Header.Text = Textos(23)
                .Columns("NOM_DEP").Header.Text = Textos(4)
                .Columns("USUARIO").Header.Text = m_sUsuario

                ' Establecemos las anchuras de los campos
                .Columns("PARTCOD").Width = Unit.Percentage(10)
                .Columns("NOM_DEP").Width = Unit.Percentage(30)
            End If

            ' Mostramos las columnas necesarias
            For Each c As GridField In .Columns
                If sColumnasVisibles.Contains("[" & c.Key & "]") Then
                    c.Hidden = False
                End If
            Next

            ' Si la selección es simple ocultamos la columna de seleccion
            If String.IsNullOrEmpty(Multiple.Value) Then
                .Columns("SEL").Hidden = True
            End If
        End With
    End Sub
#End Region
End Class