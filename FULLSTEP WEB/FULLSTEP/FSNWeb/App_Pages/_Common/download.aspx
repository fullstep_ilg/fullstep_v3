<%@ Page Language="vb" AutoEventWireup="false" Codebehind="download.aspx.vb" Inherits="Fullstep.FSNWeb.download"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<frameset runat="server" id="fraWS" rows="0,*" frameborder=no >
		<frame id="fraWSDownloadHidden" scrolling="yes" src="<% = ConfigurationManager.AppSettings("rutaFS").tostring + "_common/downloadhidden.aspx?" + Request.QueryString.tostring %>" marginheight="0" marginwidth="0" frameborder="0"   framespacing="0">
		<frame id="fraWSDownloadVisible" scrolling="auto" src="<% = ConfigurationManager.AppSettings("rutaFS").tostring + "_common/downloadvisible.aspx?" + Request.QueryString.tostring %>" marginwidth="0" marginheight="0" frameborder="0" framespacing="0" height="100%">
	</frameset>
</html>
