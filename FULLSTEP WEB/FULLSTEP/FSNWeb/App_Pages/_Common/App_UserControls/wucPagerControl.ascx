﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="wucPagerControl.ascx.vb" Inherits="Fullstep.FSNWeb.wucPagerControl" %>
<div style="clear: both; float: left; margin-right: 5px;">
    <asp:ImageButton ID="ImgBtnFirst" runat="server" CommandArgument="First" CommandName="Page" style="margin-right:2px;" />
    <asp:ImageButton ID="ImgBtnPrev" runat="server" CommandArgument="Prev" CommandName="Page" />
</div>
<div style="float: left; margin-right:5px;">
    <asp:Label ID="lblPage" runat="server" Text="dPage" Style="margin-right: 3px;"></asp:Label>
    <asp:DropDownList ID="PagerPageList" runat="server" AutoPostBack="true" Style="margin-right: 3px;" />
    <asp:Label ID="lblOF" runat="server" Text="dOf" Style="margin-right: 3px;"></asp:Label>
    <asp:Label ID="lblCount" runat="server" />
</div>
<div style="float: left;">
    <asp:ImageButton ID="ImgBtnNext" runat="server" CommandArgument="Next" CommandName="Page" style="margin-right:2px;" />
    <asp:ImageButton ID="ImgBtnLast" runat="server" CommandArgument="Last" CommandName="Page" />
</div>
