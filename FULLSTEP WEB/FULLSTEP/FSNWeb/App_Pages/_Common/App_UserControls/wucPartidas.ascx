﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="wucPartidas.ascx.vb" Inherits="FSNUserControls.wucPartidas" %>
<script language="javascript" type="text/javascript">
    function FindClicked(txtbxID, treeID) {

        // Get the TextBox that has the Regular Expression
        var txtbx = document.getElementById(txtbxID);

        // Set Regular Expression variable
        var regex = txtbx.value;

        if (regex == '')
            return false

        var tree = $find(treeID);
        var nodeCollection = tree.getNodes();

        clearSelectedNodes(tree)

        for (i = 0; i <= nodeCollection.get_length() - 1; i++) {
            var node = tree.getNodes(i).getNode(0)
            if (SearchInTree(node, regex) == true)
                break;
        }
        txtbx.selectionStart = txtbx.value.length;
        txtbx.selectionEnd = txtbx.value.length;
    }

    function SearchInTree(node, regex) {
        var text = node.get_text()
        for (var i = 0; i <= node.getItems().get_length() - 1; i++) {
            var childNode = node.get_childNode(i)
            if (childNode.get_text().search(regex) != -1) {
                childNode.set_selected(true)
                expandNodeParents(childNode)
                childNode.get_element().scrollIntoView()
                return true;
            } else {
                if (childNode.hasChildren() == true)
                    SearchInTree(childNode, regex)
            }
        }
        return false
    }

    //funcion que desselecciona los nodos seleccionados
    function clearSelectedNodes(tree) {
        var selectedNodes = tree.get_selectedNodes()
        for (i = 0; i <= selectedNodes.length - 1; i++) {
            selectedNodes[i].set_selected(false)
        }
    }
    //funcion que expande los nodos padres del nodo 
    function expandNodeParents(node) {
        var parentNodeCollection = new Array();
        var parentNode = node.get_parentNode()
        parentNodeCollection[0] = parentNode
        var i = 1
        while (parentNode.get_parentNode() != null) {
            parentNode = parentNode.get_parentNode()
            parentNodeCollection[i] = parentNode
            i+=1
        }
        for (x = parentNodeCollection.length - 1; x >= 0; x--) {
            parentNodeCollection[x].set_expanded(true)
        }
    }

</script>
<div>
    <asp:Panel ID="pnlTitulo" runat="server" Width="100%">
            <table width="100%" cellpadding="0" border="0">
                <tr>
                    <td>
                        <fsn:FSNPageHeader ID="FSNPageHeader" runat="server">
                        </fsn:FSNPageHeader>
                    </td>
                </tr>
            </table>
        </asp:Panel>


    <table cellspacing="1" cellpadding="3" width="100%" border="0">
    <tr><td><asp:Label ID="lblBuscar" runat="server" CssClass="Etiqueta"></asp:Label></td>
        <td colspan="4"><asp:TextBox ID="txtBuscar" runat="server" Width="350px"></asp:TextBox>&nbsp;&nbsp;<asp:ImageButton ID="imgSiguiente" runat="server" style="visibility:hidden" />
        </td>
        </tr>
    <tr><td><asp:Label ID="lblCentro" runat="server" CssClass="Etiqueta"></asp:Label></td>
        <td colspan="3"><asp:TextBox ID="txtCentro" runat="server" Width="350px" ReadOnly="true"></asp:TextBox>&nbsp;&nbsp;<asp:ImageButton ID="imgBuscarCentro" runat="server" OnClientClick="AbrirBuscadorCentros();return false;" />
                        <asp:Label ID="lblCentroCosteDen" runat="server" CssClass="Etiqueta"></asp:Label>
                        <input type="hidden" name="hCentroCod" id="hCentroCod" runat="server" /></td>
        <td><fsn:FSNButton ID="btnBuscar" runat="server" Alineacion="Left"></fsn:FSNButton></td>
        </tr>
    <tr><td><asp:Label ID="lblFechaInicioDesde" runat="server" CssClass="Etiqueta"></asp:Label></td>
        <td><igpck:WebDatePicker ID="dteFechaInicioDesde" runat="server" Width="100px"  SkinID="Calendario"></igpck:WebDatePicker></td>
        <td nowrap><asp:Label ID="lblFechaHastaFin" runat="server" CssClass="Etiqueta"></asp:Label></td>
        <td><igpck:WebDatePicker ID="dteFechaHastaFin" runat="server" Width="100px"  SkinID="Calendario"></igpck:WebDatePicker></td>
        <td nowrap><asp:CheckBox ID="chkPartidasNoVigentes" runat="server" CssClass="Etiqueta" /></td>
        </tr>
        
    <tr><td colspan="5">
            <asp:UpdatePanel ID="upAlerta" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
            <ContentTemplate>
            <div id="dCamposFiltro" class="dContenedor" style="width:95%">
            <ig:WebDataTree ID="wdtPartidas" runat="server" Height="400px" BorderStyle="Solid" BorderColor="Black">
                    <NodeSettings SelectedCssClass="NodeSelected" />
            </ig:WebDataTree>
			</div>
			 </ContentTemplate>
			 <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
            </Triggers>
            </asp:UpdatePanel>
			</td></tr>
    </table>
    <table cellspacing="1" cellpadding="3" width="100%" border="0">
         <tr>
            <td align="right" style="width:50%"><fsn:FSNButton ID="btnSeleccionar" runat="server" Alineacion="Right" OnClientClick="return seleccionarPartida();"></fsn:FSNButton></td>
		    <td align="left" style="width:50%"><fsn:FSNButton ID="btnCancelar" runat="server" Alineacion="Left" OnClientClick="window.close()"></fsn:FSNButton></td>
	    </tr>
    </table>   
    </div>