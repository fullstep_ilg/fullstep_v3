﻿Imports System
Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Public Class PageChangedEventArgs
    Inherits EventArgs

    ''' <summary>
    ''' Creación del control
    ''' </summary>
    ''' <param name="pageNumber">Numero de pagina</param>
    ''' <remarks>Llamada desde: Class wucPagerControl como EventHandler ; Tiempo máximo: 0</remarks>
    Public Sub New(ByVal pageNumber As Integer)
        Me._pageNumber = pageNumber
    End Sub

    Private _pageNumber As Integer

    ''' <summary>
    ''' Devuelve el numero de pagina
    ''' </summary>
    ''' <remarks>Llamada desde: Notificaciones.aspx ; Tiempo máximo: 0</remarks>
    Public ReadOnly Property PageNumber() As Integer
        Get
            Return Me._pageNumber
        End Get
    End Property
End Class

Partial Public Class wucPagerControl
    Inherits System.Web.UI.UserControl

    Private pag As FSNPage
    Private _PagNumber As Integer

    Public Event PageChanged As EventHandler(Of PageChangedEventArgs)

    ''' <summary>
    ''' Carga la pagina
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Llamada desde: Sistema; Tiempo máximo:0,5seg.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            pag = CType(Me.Page, FSNPage)

            pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.Notificaciones
            Me.lblPage.Text = pag.Textos(46)
            Me.lblOF.Text = pag.Textos(48)

            _PagNumber = 1
        Else
            _PagNumber = PagerPageList.SelectedIndex
        End If
    End Sub

    ''' <summary>
    ''' Los cambios en el combo de paginas deben reflejarse en el grid
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>  
    ''' <remarks>Llamada desde: Sistema ; Tiempo máximo: 0</remarks>
    Private Sub PagerPageList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PagerPageList.SelectedIndexChanged
        _PagNumber = PagerPageList.SelectedIndex

        RaiseEvent PageChanged(Me, New PageChangedEventArgs(PagerPageList.SelectedIndex))
    End Sub

    ''' <summary>
    ''' El cambio en el numero de pagina debe reflejarse en el grid
    ''' </summary>
    ''' <param name="pageNumber">numero de pagina</param>
    ''' <remarks>Llamada desde: Notificaciones.aspx ; Tiempo máximo: 0</remarks>
    Public Sub SetCurrentPageNumber(ByVal pageNumber As Integer)
        If pageNumber <= Me.PagerPageList.Items.Count - 1 Then
            Me.PagerPageList.SelectedIndex = pageNumber
        End If
    End Sub

    ''' <summary>
    ''' Carga del combo de paginas (de 1 a numberOfPages)
    ''' </summary>
    ''' <param name="numberOfPages">Numero  de pagina</param>
    ''' <remarks>Llamada desde: Notificaciones.aspx ; Tiempo máximo: 0</remarks>
    Public Sub SetupPageList(ByVal numberOfPages As Integer)
        Dim pageNumber As Integer = 1

        Me.PagerPageList.Items.Clear()

        While pageNumber <= numberOfPages
            Dim lstItem As New ListItem(pageNumber.ToString())
            Me.PagerPageList.Items.Add(lstItem)
            System.Math.Max(System.Threading.Interlocked.Increment(pageNumber), pageNumber - 1)
        End While

        Me.lblCount.Text = numberOfPages.ToString()
    End Sub

    ''' <summary>
    ''' Se solicita la primera pagina
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>  
    ''' <remarks>Llamada desde: Sistema ; Tiempo máximo: 0</remarks>
    Private Sub ImgBtnFirst_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnFirst.Click
        _PagNumber = 0

        Me.PagerPageList.SelectedIndex = _PagNumber

        RaiseEvent PageChanged(Me, New PageChangedEventArgs(PagerPageList.SelectedIndex))
    End Sub

    ''' <summary>
    ''' Dependiendo de la pagina en q estes el boton estara accesible o no
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>  
    ''' <remarks>Llamada desde: Sistema ; Tiempo máximo: 0</remarks>
    Private Sub ImgBtnFirst_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles ImgBtnFirst.PreRender
        If Not Me.PagerPageList.SelectedIndex = 0 Then
            Me.ImgBtnFirst.Enabled = True
            Me.ImgBtnFirst.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero.gif"
        Else
            Me.ImgBtnFirst.Enabled = False
            Me.ImgBtnFirst.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero_desactivado.gif"
        End If
    End Sub

    ''' <summary>
    ''' Se solicita la pagina previa
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>  
    ''' <remarks></remarks>
    Private Sub ImgBtnPrev_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnPrev.Click
        RaiseEvent PageChanged(Me, New PageChangedEventArgs(PagerPageList.SelectedIndex))

        SetCurrentPageNumber(_PagNumber - 1)
    End Sub

    ''' <summary>
    ''' Dependiendo de la pagina en q estes el boton estara accesible o no
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>  
    ''' <remarks>Llamada desde: Sistema ; Tiempo máximo: 0</remarks>
    Private Sub ImgBtnPrev_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles ImgBtnPrev.PreRender
        If Not Me.PagerPageList.SelectedIndex = 0 Then
            Me.ImgBtnPrev.Enabled = True
            Me.ImgBtnPrev.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior.gif"
        Else
            Me.ImgBtnPrev.Enabled = False
            Me.ImgBtnPrev.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior_desactivado.gif"
        End If
    End Sub

    ''' <summary>
    ''' Se solicita la pagina siguiente
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>  
    ''' <remarks>Llamada desde: Sistema ; Tiempo máximo: 0</remarks>
    Private Sub ImgBtnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnNext.Click
        RaiseEvent PageChanged(Me, New PageChangedEventArgs(PagerPageList.SelectedIndex))

        SetCurrentPageNumber(_PagNumber + 1)
    End Sub

    ''' <summary>
    ''' Dependiendo de la pagina en q estes el boton estara accesible o no
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>  
    ''' <remarks>Llamada desde: Sistema ; Tiempo máximo: 0</remarks>
    Private Sub ImgBtnNext_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles ImgBtnNext.PreRender
        If Not Me.PagerPageList.SelectedIndex = (MaxPage - 1) Then
            Me.ImgBtnNext.Enabled = True
            Me.ImgBtnNext.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente.gif"
        Else
            Me.ImgBtnNext.Enabled = False
            Me.ImgBtnNext.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente_desactivado.gif"
        End If
    End Sub

    ''' <summary>
    ''' Se solicita la pagina ultima
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>  
    ''' <remarks>Llamada desde: Sistema ; Tiempo máximo: 0</remarks>
    Private Sub ImgBtnLast_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnLast.Click

        _PagNumber = MaxPage - 1

        Me.PagerPageList.SelectedIndex = _PagNumber

        RaiseEvent PageChanged(Me, New PageChangedEventArgs(PagerPageList.SelectedIndex))
    End Sub

    ''' <summary>
    ''' Dependiendo de la pagina en q estes el boton estara accesible o no
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>  
    ''' <remarks>Llamada desde: Sistema ; Tiempo máximo: 0</remarks>
    Private Sub ImgBtnLast_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles ImgBtnLast.PreRender
        If Not Me.PagerPageList.SelectedIndex = (MaxPage - 1) Then
            Me.ImgBtnLast.Enabled = True
            Me.ImgBtnLast.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo.gif"
        Else
            Me.ImgBtnLast.Enabled = False
            Me.ImgBtnLast.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo_desactivado.gif"
        End If
    End Sub

    ''' <summary>
    ''' Devuelve el numero maximo de paginas
    ''' </summary>
    ''' <remarks>Llamada desde: Notificaciones.aspx ; Tiempo máximo: 0</remarks>
    Public ReadOnly Property MaxPage() As Integer
        Get
            Return Me.PagerPageList.Items.Count
        End Get
    End Property
End Class

