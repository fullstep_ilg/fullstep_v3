﻿Public Class BuscadorFacturas
    Inherits FSNPage

    Private _pagerControl As wucPagerControl

    ''' <summary>
    ''' Carga la pagina
    ''' Para q sea posible el multilingÃ¼ismo en los filtros y la paginacion customizada.
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Llamada desde: Sistema; Tiempo mÃ¡ximo:1seg.</remarks>
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.ScriptMgr.EnableScriptGlobalization = True

        _pagerControl = TryCast(Me.wdgDatos.Behaviors.Paging.PagerTemplateContainerTop.FindControl("CustomerPager"), wucPagerControl)
        AddHandler _pagerControl.PageChanged, AddressOf currentPageControl_PageChanged
    End Sub

    ''' <summary>
    ''' Cuando con la paginacion customizada cambia de pagina, el grid debe actualizarse a la pagina indicada.
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>    
    ''' <remarks>Llamada desde: wucPagercOntrol.ascx; Tiempo mÃ¡ximo:0,1seg.</remarks>
    Private Sub currentPageControl_PageChanged(ByVal sender As Object, ByVal e As PageChangedEventArgs)

        Me.wdgDatos.Behaviors.Paging.PageIndex = e.PageNumber

        UpdDatos.Update()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorFacturas
        WriteScripts()
        InicializacionControles()

        If Not IsPostBack Then

            FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/facturaPeq.png"
            FSNPageHeader.TituloCabecera = Textos(29)

            CargarIdiomasControles()
            ConfiguracionGrid()

            'Si no hay acceso al modulo SM no se cargan las partidas, ni gestor, ni centro de coste
            If Acceso.gbAccesoFSSM Then
                CargarControlesPartidas()
            Else
                CeldaCentroCoste.Visible = False
                CeldaLblCentroCoste.Visible = False
                CeldaLblGestor.Visible = False
                CeldaGestor.Visible = False
                CeldaPartidas.Visible = False
            End If

            If Acceso.gbOblCodPedDir Then
                CeldaPedERP.Visible = True
                CeldaLblPedERP.Visible = True
            Else
                CeldaPedERP.Visible = False
                CeldaLblPedERP.Visible = False
            End If

            'Si no hay integracion en facturas no se muestra el campo Factura Erp
            If Session("HayIntegracionFacturas") Then
                CeldaNumFacturaERP.Visible = True
                CeldaLblNumFacturaERP.Visible = True
            Else
                CeldaNumFacturaERP.Visible = False
                CeldaLblNumFacturaERP.Visible = False
            End If

            CargarAnyos()

            CargarEstados()
        End If

        If Not IsPostBack Then
            Me.wdgDatos.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize")
            Me.btnBuscar_Click(sender, e)
        Else
            'Todas las acciones sobre el grid provocan postback y todas necesitan de databind. 
            'Existen eventos para wdgDatos_ColumnSorted y wdgDatos_DataFiltered
            CargarGrid()
        End If
    End Sub


    ''' <summary>
    ''' Inicializa los componentes y funciones javascript de los elementos
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0seg.</remarks>
    Private Sub InicializacionControles()

        ddlAnyo.CurrentValue = ""
        dteFechaFacturaDesde.NullText = ""
        dteFechaFacturaHasta.NullText = ""
        dteContabilizacionDesde.NullText = ""
        dteContabilizacionHasta.NullText = ""

        If Not IsPostBack AndAlso Request("CodProveedor") <> Nothing Then
            hidProveedor.Value = Request("CodProveedor")
            txtProveedor.Text = Request("DenProveedor")
        End If
    End Sub

    ''' <summary>
    ''' Método que recorre de manera recursiva el control recibido y vacía las selecciones de todos los controles
    ''' </summary>
    ''' <param name="oControlBase">Control a recorrer</param>
    ''' <remarks>Llamada desde btnLimpiar_Click</remarks>
    Private Sub VaciarControlesRecursivo(ByVal oControlBase As Object)
        For Each oControl In oControlBase.Controls
            Select Case oControl.GetType().ToString
                Case GetType(DropDownList).ToString
                    If CType(oControl, DropDownList).SelectedItem IsNot Nothing Then _
                        CType(oControl, DropDownList).SelectedItem.Selected = False
                Case GetType(TextBox).ToString
                    CType(oControl, TextBox).Text = String.Empty
                Case GetType(Infragistics.Web.UI.EditorControls.WebDatePicker).ToString
                    CType(oControl, Infragistics.Web.UI.EditorControls.WebDatePicker).Value = Nothing
                Case GetType(CheckBox).ToString
                    CType(oControl, CheckBox).Checked = False
                Case GetType(Infragistics.Web.UI.EditorControls.WebNumericEditor).ToString
                    CType(oControl, Infragistics.Web.UI.EditorControls.WebNumericEditor).Value = Nothing
                Case GetType(System.Web.UI.WebControls.HiddenField).ToString
                    'No tiene que borrar los hidden donde se indica el codigo de cada raiz de partida presupuestaria
                    If Not CType(oControl, System.Web.UI.WebControls.HiddenField).ClientID.Contains("hid_Pres5") Then
                        CType(oControl, System.Web.UI.WebControls.HiddenField).Value = String.Empty
                    End If
                Case Else
                    If oControl.Controls IsNot Nothing AndAlso oControl.Controls.Count > 0 Then
                        VaciarControlesRecursivo(oControl)
                    End If
            End Select
        Next
    End Sub

    ''' <summary>
    ''' Carga los controles con el idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde:=Page_load; Tiempo máximo:=0seg.</remarks>
    Private Sub CargarIdiomasControles()
        lblProveedor.Text = String.Format("{0}:", Textos(1))
        lblEstado.Text = String.Format("{0}:", Textos(7))
        lblEmpresa.Text = String.Format("{0}:", Textos(16))
        lblPedido.Text = String.Format("{0}:", Textos(10))
        lblArticulo.Text = String.Format("{0}:", Textos(13))
        lblNumeroAlbaran.Text = String.Format("{0}:", Textos(8))
        lblFechaFactura.Text = String.Format("{0}:", Textos(6))
        lblPedidoERP.Text = String.Format("{0}:", Textos(11))
        lblFechaContabilizacion.Text = String.Format("{0}:", Textos(9))
        lblNumeroFacturaSAP.Text = String.Format("{0}:", Textos(14))
        lblImporte.Text = String.Format("{0}:", Textos(12))
        lblGestor.Text = String.Format("{0}:", Textos(27))
        lblCentroCoste.Text = String.Format("{0}:", Textos(28))

        txtwmeNumCesta.WatermarkText = Textos(51)
        txtwmeNumPedido.WatermarkText = Textos(52)

        Me.btnBuscar.Text = Textos(4)
        Me.btnLimpiar.Text = Textos(18)

        Me.lblPulse.Text = Textos(47)
    End Sub

    ''' <summary>
    ''' Inicializa javascript de los elementos
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0seg.</remarks>
    Private Sub WriteScripts()

        'Funcion que sirve para eliminar el Gestor
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarGestor") Then
            Dim sScript As String = "function eliminarGestor(event) {" & vbCrLf & _
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf & _
                    "  " & vbCrLf & _
            " } else { " & vbCrLf & _
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                    " campoGestor = document.getElementById('" & txtGestor.ClientID & "')" & vbCrLf & _
                    " if (campoGestor) { campoGestor.value = ''; } " & vbCrLf & _
                    " campoHiddenGestor = document.getElementById('" & hid_Gestor.ClientID & "')" & vbCrLf & _
                    " if (campoHiddenGestor) { campoHiddenGestor.value = ''; } " & vbCrLf & _
            " } " & vbCrLf & _
            "  " & vbCrLf & _
            " } }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarGestor", sScript, True)
        End If

        'Funcion que sirve para eliminar el Centro de coste
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarCentroCoste") Then
            Dim sScript As String = "function eliminarCentroCoste(event) {" & vbCrLf & _
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf & _
                    "  " & vbCrLf & _
            " } else { " & vbCrLf & _
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                    " campoCentroCoste = document.getElementById('" & txtCentroCoste.ClientID & "')" & vbCrLf & _
                    " if (campoCentroCoste) { campoCentroCoste.value = ''; } " & vbCrLf & _
                    " campoHiddenCentroCoste = document.getElementById('" & hid_CentroCostes.ClientID & "')" & vbCrLf & _
                    " if (campoHiddenCentroCoste) { campoHiddenCentroCoste.value = ''; } " & vbCrLf & _
            " } " & vbCrLf & _
            "  " & vbCrLf & _
            " } }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarCentroCoste", sScript, True)
        End If

        'Funcion que sirve para eliminar el articulo relacionado
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarArticulo") Then
            Dim sScript As String = "function eliminarArticulo(event) {" & vbCrLf & _
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf & _
                    "  " & vbCrLf & _
            " } else { " & vbCrLf & _
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                    " campoArticulo = document.getElementById('" & txtArticulo.ClientID & "')" & vbCrLf & _
                    " if (campoArticulo) { campoArticulo.value = ''; } " & vbCrLf & _
                    " campoHiddenArticulo = document.getElementById('" & hidArticulo.ClientID & "')" & vbCrLf & _
                    " if (campoHiddenArticulo) { campoHiddenArticulo.value = ''; } " & vbCrLf & _
             " } " & vbCrLf & _
            "  " & vbCrLf & _
            " } }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarArticulo", sScript, True)
        End If

        'Funcion que sirve para eliminar el proveedor
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarProveedor") Then
            Dim sScript As String = "function eliminarProveedor(event) {" & vbCrLf & _
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf & _
                    "  " & vbCrLf & _
            " } else { " & vbCrLf & _
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                    " campoProveedor = document.getElementById('" & txtProveedor.ClientID & "')" & vbCrLf & _
                    " if (campoProveedor) { campoProveedor.value = ''; } " & vbCrLf & _
                    " campoHiddenProveedor = document.getElementById('" & hidProveedor.ClientID & "')" & vbCrLf & _
                    " if (campoHiddenProveedor) { campoHiddenProveedor.value = ''; } " & vbCrLf & _
            " } " & vbCrLf & _
            "  " & vbCrLf & _
            " } }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarProveedor", sScript, True)
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered("prove_seleccionado") Then
            Dim sScript As String = " function prove_seleccionado2(sCIF, sProveCod, sProveDen) {" & vbCrLf & _
                    " document.getElementById('" & hidProveedor.ClientID & "').value = sProveCod " & vbCrLf & _
                    " document.getElementById('" & txtProveedor.ClientID & "').value = sProveDen " & vbCrLf & _
            " } " & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "prove_seleccionado", sScript, True)
        End If

        'Funcion que sirve para eliminar la empresa relacionada
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarEmpresa") Then
            Dim sScript As String = "function eliminarEmpresa(event) {" & vbCrLf & _
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf & _
                    "  " & vbCrLf & _
            " } else { " & vbCrLf & _
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                    " campoEmpresa = document.getElementById('" & txtEmpresa.ClientID & "')" & vbCrLf & _
                    " if (campoEmpresa) { campoEmpresa.value = ''; } " & vbCrLf & _
                    " campoHiddenEmpresa = document.getElementById('" & hidEmpresa.ClientID & "')" & vbCrLf & _
                    " if (campoHiddenEmpresa) { campoHiddenEmpresa.value = ''; } " & vbCrLf & _
            " } " & vbCrLf & _
            "  " & vbCrLf & _
            " } }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarEmpresa", sScript, True)
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered("seleccionarEmpresa") Then
            Dim sScript As String = "function SeleccionarEmpresa(idEmpresa, nombreEmpresa, nifEmpresa) {" & vbCrLf & _
                    "oIdEmpresa = document.getElementById('" & hidEmpresa.ClientID & "');" & vbCrLf & _
                    " if (oIdEmpresa) { oIdEmpresa.value = idEmpresa; } " & vbCrLf & _
                    "otxtEmpresa = document.getElementById('" & txtEmpresa.ClientID & "');" & vbCrLf & _
                    " if (otxtEmpresa) { otxtEmpresa.value = nombreEmpresa; } " & vbCrLf & _
            " } " & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "seleccionarEmpresa", sScript, True)
        End If

        imgProveedorLupa.OnClientClick = "var newWindow = window.open('" & ConfigurationManager.AppSettings("RutaFS") & "_Common/BuscadorProveedores.aspx?PM=true&Contr=true', '_blank', 'width=830,height=530,status=yes,resizable=no,top=150,left=150');newWindow.focus();return false;"
        imgEmpresaLupa.Attributes.Add("onClick", "javascript:var newWindow = window.open('" & ConfigurationManager.AppSettings("rutaFS") & "_common/BuscadorEmpresas.aspx','_blank','width=800,height=600, location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =no');newWindow.focus();return false;")
        imgCentroCosteLupa.Attributes.Add("onclick", "javascript:var newWindow = window.open('" & ConfigurationManager.AppSettings("rutaFS") & "_Common/BuscadorCentrosCoste.aspx?idControl=" & txtCentroCoste.ClientID & "&idHidControl=" & hid_CentroCostes.ClientID & "', '_blank', 'width=850,height=630,status=yes,resizable=no,top=200,left=200');newWindow.focus();return false;")
        imgGestorLupa.Attributes.Add("onclick", "javascript:var newWindow = window.open('" & ConfigurationManager.AppSettings("rutaFS") & "_Common/BuscadorUsuarios.aspx?Desde=IM&idControl=" & txtGestor.ClientID & "&idHidControl=" & hid_Gestor.ClientID & "', '_blank', 'width=850,height=630,status=yes,resizable=no,top=200,left=200');newWindow.focus();return false;")
        imgArticuloLupa.Attributes.Add("onclick", "javascript:var newWindow = window.open('" & ConfigurationManager.AppSettings("rutaFS") & "_common/BuscadorArticulos.aspx?desde=Visor&ClientId=" & txtArticulo.ClientID & "&idHidControl=" & hidArticulo.ClientID & "&Origen=VisorFacturas', '_blank', 'width=850,height=660,status=yes,resizable=no,top=50,left=200');newWindow.focus();return false;")

        txtCentroCoste.Attributes.Add("onkeydown", "javascript:return eliminarCentroCoste(event)")
        txtGestor.Attributes.Add("onkeydown", "javascript:return eliminarGestor(event)")
        txtArticulo.Attributes.Add("onkeydown", "javascript:return eliminarArticulo(event)")
        txtProveedor.Attributes.Add("onkeydown", "javascript:return eliminarProveedor(event)")
        txtEmpresa.Attributes.Add("onkeydown", "javascript:return eliminarEmpresa(event)")
    End Sub


    ''' <summary>
    ''' Enlaza la fuente de datos con el datalist de Partidas presupuestarias
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarControlesPartidas()
        dlPartidas.DataSource = FSNWeb.modFacturas.DevolverPartidas(Idioma)
        dlPartidas.DataBind()
    End Sub

    ''' <summary>
    ''' Carga los años en el combo de años de pedido. Desde el actual menos 10 hasta el actual más 10.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarAnyos()
        Dim iAnyoActual As Integer
        Dim iInd As Integer

        Dim ddItem As Infragistics.Web.UI.ListControls.DropDownItem
        iAnyoActual = Year(Now)
        ddlAnyo.Items.Clear()
        For iInd = iAnyoActual - 10 To iAnyoActual + 10
            ddItem = New Infragistics.Web.UI.ListControls.DropDownItem
            ddItem.Value = iInd
            ddItem.Text = iInd.ToString
            ddlAnyo.Items.Add(ddItem)
        Next
        ddlAnyo.SelectedItemIndex = ddlAnyo.Items.IndexOf(ddlAnyo.Items.FindItemByValue(iAnyoActual.ToString))
    End Sub

    ''' <summary>
    ''' Carga los distintos tipos de estados
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarEstados()
        wddEstado.Items.Clear()

        Dim oItem As Infragistics.Web.UI.ListControls.DropDownItem
        wddEstado.Items.Clear()
        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = -1
        oItem.Text = String.Empty
        wddEstado.Items.Add(oItem)
        Dim dtEstadosFacturas As DataTable
        dtEstadosFacturas = FSNWeb.modFacturas.DevolverEstados(Idioma).Tables(0)
        For Each drRow As DataRow In dtEstadosFacturas.Rows
            oItem = New Infragistics.Web.UI.ListControls.DropDownItem
            oItem.Value = DBNullToInteger(drRow("ID"))
            oItem.Text = drRow("DEN")
            wddEstado.Items.Add(oItem)
        Next

        wddEstado.EnableAutoFiltering = Infragistics.Web.UI.ListControls.AutoFiltering.Client
        wddEstado.AutoFilterQueryType = Infragistics.Web.UI.ListControls.AutoFilterQueryTypes.Contains
        wddEstado.AutoFilterSortOrder = Infragistics.Web.UI.ListControls.DropDownAutoFilterSortOrder.Ascending
    End Sub


    ''' <summary>
    ''' Buscar proveedores segun los filtros dados
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>      
    ''' <remarks>Llamada desde: sistema; Tiempo máximo: 0,1</remarks>
    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.wdgDatos.DataSource = Facturas(True)
        Me.wdgDatos.DataBind()
        Me.wdgDatos.Behaviors.Paging.PageIndex = 0

        Me.UpdDatos.Update()
    End Sub

    ''' <summary>
    ''' Carga la grid con los proveedores
    ''' </summary>
    ''' <remarks>Llamada desde: page_load; Tiempo mÃƒÂ¡ximo:0,3</remarks>
    Private Sub CargarGrid()
        wdgDatos.DataSource = Facturas(False)
        wdgDatos.DataBind()

        Me.UpdDatos.Update()
    End Sub

    ''' <summary>Devuelve un dataset con las facturas</summary>
    ''' <param name="bRecargar">Indica si hay que acceder a base de datos para recargar los datos</param>
    ''' <returns>Dataset con las facturas</returns>
    Private Function Facturas(ByVal bRecargar As Boolean) As DataSet
        Dim oData As DataSet

        If bRecargar Or Cache("oFacturasBusqueda_" & FSNUser.Cod) Is Nothing Then
            Dim oFacturas As FSNServer.Facturas = FSNServer.Get_Object(GetType(FSNServer.Facturas))
            oData = oFacturas.BuscadorFacturas(Idioma, Me.hidProveedor.Value, If(hidEmpresa.Value = "", 0, CLng(hidEmpresa.Value)), Me.dteFechaFacturaDesde.Value, Me.dteFechaFacturaHasta.Value, Me.dteContabilizacionDesde.Value, Me.dteContabilizacionHasta.Value, Me.wneImporteDesde.Value, Me.wneImporteHasta.Value, Me.hid_CentroCostes.Value, Nothing, If(Me.wddEstado.SelectedValue <> Nothing, Me.wddEstado.SelectedValue, 0), Me.ddlAnyo.SelectedValue, If(Me.txtNumCesta.Text <> Nothing, strToInt(txtNumCesta.Text), 0), If(Me.txtNumPedido.Text <> Nothing, strToInt(txtNumPedido.Text), 0), Me.hidArticulo.Value, Me.txtNumeroAlbaran.Text, Me.txtPedidoERP.Text, Me.txtNumeroFacturaSAP.Text, Me.hid_Gestor.Value)
            Me.InsertarEnCache("oFacturasBusqueda_" & FSNUser.Cod, oData)
        Else
            oData = CType(Cache("oFacturasBusqueda_" & FSNUser.Cod), DataSet)
        End If

        Return oData
    End Function

    ''' <summary>
    ''' Tras la carga del grid se "sincroniza" con el Custom Pager
    ''' </summary>
    ''' <param name="sender">control</param>
    ''' <param name="e">evento de sistema</param>  
    ''' <remarks>Llamada desde: sistema; Tiempo mÃ¡ximo:0 </remarks>
    Private Sub wdgDatos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles wdgDatos.DataBound
        If (_pagerControl.MaxPage <> Me.wdgDatos.Behaviors.Paging.PageCount) Then
            _pagerControl.SetupPageList(Me.wdgDatos.Behaviors.Paging.PageCount)
        End If

        If Request("__EVENTTARGET") = Replace(Me._pagerControl.Controls(6).ClientID.ToString, "_", "$") Then
        Else
            _pagerControl.SetCurrentPageNumber(wdgDatos.Behaviors.Paging.PageIndex)
        End If
    End Sub

    Private Sub ConfiguracionGrid()
        'Configurar el grid
        If wdgDatos.Columns.Item("NUM") IsNot Nothing Then
            wdgDatos.Columns.Item("NUM").Header.Text = Textos(30)
        End If
        If wdgDatos.Columns.Item("EMPRESA") IsNot Nothing Then
            wdgDatos.Columns.Item("EMPRESA").Header.Text = Textos(16)
        End If
        If wdgDatos.Columns.Item("PROVEEDOR") IsNot Nothing Then
            wdgDatos.Columns.Item("PROVEEDOR").Header.Text = Textos(1)
        End If
        If wdgDatos.Columns.Item("NOMBRE_PROVEEDOR") IsNot Nothing Then
            wdgDatos.Columns.Item("NOMBRE_PROVEEDOR").Header.Text = Textos(31)
        End If
        If wdgDatos.Columns.Item("FECHA") IsNot Nothing Then
            wdgDatos.Columns.Item("FECHA").Header.Text = Textos(32)
        End If
        If wdgDatos.Columns.Item("FEC_CONTA") IsNot Nothing Then
            wdgDatos.Columns.Item("FEC_CONTA").Header.Text = Textos(33)
        End If
        If wdgDatos.Columns.Item("NUM_ERP") IsNot Nothing Then
            wdgDatos.Columns.Item("NUM_ERP").Header.Text = Textos(14)
        End If
        If wdgDatos.Columns.Item("IMPORTE") IsNot Nothing Then
            wdgDatos.Columns.Item("IMPORTE").Header.Text = Textos(34)
        End If
    End Sub

    ''' <summary>
    ''' Evento que salta por cada partida de la instalación
    ''' </summary>
    ''' <param name="sender">datalist de partidas</param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub dlPartidas_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlPartidas.ItemDataBound
        Dim fila As DataRow = DirectCast(e.Item.DataItem, DataRowView).Row
        Dim txtPartida As TextBox
        With e.Item
            DirectCast(.FindControl("lblPartida"), Label).Text = fila("DEN") & ":"
            Dim sHiddenControlID As String = DirectCast(.FindControl("hid_Partida"), HiddenField).ClientID
            Dim sPartida As String = fila("PRES5")
            DirectCast(.FindControl("hid_Pres5"), HiddenField).Value = sPartida
            txtPartida = DirectCast(.FindControl("txtPartida"), TextBox)
            Dim sTextControlID As String = txtPartida.ClientID
            DirectCast(.FindControl("imgPartidaLupa"), ImageButton).Attributes.Add("onclick", "javascript:AbrirPartidas('" & ConfigurationManager.AppSettings("rutaFS") & "_Common/BuscadorPartidas.aspx" & "','" & Me.txtCentroCoste.ClientID & "','" & hid_CentroCostes.ClientID & "','" & sTextControlID & "','" & sHiddenControlID & "','" & sPartida & "','" & Me.hid_CentroCostes.Value & "','" & Me.txtCentroCoste.Text & "'); return false;")

            'Escribo los scripts de javascript para estos campos del datalist

            'Funcion que sirve para eliminar el Gestor
            If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarPartida_" & sPartida) Then
                Dim sScript As String = "function eliminarPartida_" & sPartida & "(event) {" & vbCrLf & _
                " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf & _
                        "  " & vbCrLf & _
                " } else { " & vbCrLf & _
                    " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                        " campoPartida = document.getElementById('" & sTextControlID & "')" & vbCrLf & _
                        " if (campoPartida) { campoPartida.value = ''; } " & vbCrLf & _
                        " campoHiddenPartida = document.getElementById('" & sHiddenControlID & "')" & vbCrLf & _
                        " if (campoHiddenPartida) { campoHiddenPartida.value = ''; } " & vbCrLf & _
                " } " & vbCrLf & _
                "  " & vbCrLf & _
                " } }" & vbCrLf
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarPartida_" & sPartida, sScript, True)

                txtPartida.Attributes.Add("onkeydown", "javascript:return eliminarPartida_" & sPartida & "(event)")
            End If
        End With
    End Sub

    ''' <summary>
    ''' Evento que se lanza al hacer click en el botón de limpiar
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: al hacer click en el botón de limpiar.</remarks>
    Private Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        VaciarControlesRecursivo(pnlFiltros)
        wddEstado.SelectedItemIndex = -1
        wddEstado.CurrentValue = Nothing
        upFiltros.Update()
    End Sub
End Class