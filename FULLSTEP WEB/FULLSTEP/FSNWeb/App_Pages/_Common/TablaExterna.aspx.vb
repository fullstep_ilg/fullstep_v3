﻿Imports Infragistics.Web.UI.GridControls

Partial Public Class TablaExterna
    Inherits FSNPage

    Private _mlId As Long
    Private _msArt As String
    Private _moTablaExterna As FSNServer.TablaExterna
    Public _msTitulo As String
    Private Const cdAnchoCaracter As Decimal = 7.5
    Private ciAnchoGrid As Integer = 680
    Private Const ciAnchoGridScroll As Integer = 663
    Private Const ciNumFilasScroll As Integer = 17

    ''' <summary>
    ''' Carga la página con los datos de la tabla externa cargados
    ''' </summary>
    ''' <remarks>LLamada desde: Al cargar la página; Tiempo máx: 0,3 sg.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaTheme", "<script>var rutaTheme = '" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "'</script>")
        _mlId = CLng(Request.QueryString("tabla"))
        If Request.QueryString("art").ToString() <> "" Then _
            _msArt = Request.QueryString("art").ToString()

        _moTablaExterna = FSNServer.Get_Object(GetType(FSNServer.TablaExterna))
        _moTablaExterna.LoadDefTabla(_mlId, _msArt, True)

        Dim sFiltro As String = Nothing
        If cmdBuscar.CommandArgument <> "" Then
            sFiltro = "CONVERT(varchar," & _moTablaExterna.PrimaryKey & ") LIKE '" & Replace(cmdBuscar.CommandArgument, "'", "''") & "%'"
        End If
        _moTablaExterna.LoadData(_mlId, _msArt, True, Nothing, Nothing, Nothing, sFiltro)

        If Not Page.IsPostBack() Then
            With CType(whdgTabla.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero_desactivado.gif"
                .Attributes.Add("onClick", "return FirstPage();")
                .Enabled = False
            End With
            With CType(whdgTabla.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior_desactivado.gif"
                .Attributes.Add("onClick", "return PrevPage();")
                .Enabled = False
            End With
            Dim SoloUnaPagina As Boolean = True
            With CType(whdgTabla.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
                .Attributes.Add("onClick", "return NextPage();")
                .Enabled = Not SoloUnaPagina
            End With
            With CType(whdgTabla.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
                .Attributes.Add("onClick", "return LastPage();")
                .Enabled = Not SoloUnaPagina
            End With
        End If
        hddFilaIni.Value = 0
        _msTitulo = _moTablaExterna.Nombre
        If Page.IsPostBack Then
            CargarGrid()
        End If
        If Not IsPostBack() Then
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.TablaExterna
            cmdAceptar.Text = Textos(0)
            cmdBuscar.Text = Textos(5)
            For Each oRow As System.Data.DataRow In _moTablaExterna.DefTabla.Tables(0).Rows
                If UCase(_moTablaExterna.PrimaryKey) = UCase(oRow.Item("CAMPO")) Then _
                    lblBuscar.Text = oRow.Item("NOMBRE")
            Next

            Me.hddDataEntry.Value = Request.QueryString("idDataEntry")

            Me.whdgTabla.DataSource = _moTablaExterna.Data
            CrearColumnas()
            Me.whdgTabla.DataBind()
            FormatGrid()
            SeleccionarValor()
            Me.InsertarEnCache("oTabExt_" & FSNUser.Cod, _moTablaExterna.Data)
        End If
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Notificaciones
        CType(whdgTabla.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPage"), Label).Text = Textos(46)
        CType(whdgTabla.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblOF"), Label).Text = Textos(48)
        'Vuelvo a cargar el modulo de TablaExterna
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.TablaExterna

    End Sub

    Private Sub CrearColumnas()

        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
        whdgTabla.Rows.Clear()
        whdgTabla.Columns.Clear()
        whdgTabla.GridView.Columns.Clear()
        For Each oRow As System.Data.DataRow In _moTablaExterna.DefTabla.Tables(0).Rows
            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = oRow.Item("CAMPO")
                .DataFieldName = oRow.Item("CAMPO")
                .Header.Text = oRow.Item("CAMPO")
            End With
            Me.whdgTabla.Columns.Add(campoGrid)
            Me.whdgTabla.GridView.Columns.Add(campoGrid)
        Next
    End Sub



    ''' <summary>
    ''' Establece el formato de la grid donde se muestran los datos, formato de los campos numéricos,...
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load, ActualizarGrid</remarks>
    Private Sub FormatGrid()
        Dim i As Integer
        Dim z As Integer
        Dim iAnchoGrid As Integer = 0
        Dim oGrid As WebHierarchicalDataGrid = Me.whdgTabla
        Dim iMaxAncho As Integer
        ciAnchoGrid = 680
        oGrid.Width = Unit.Pixel(ciAnchoGrid + 2)
        iMaxAncho = IIf(_moTablaExterna.DefTabla.Tables(1).Rows(0).Item("NUMFILAS") > ciNumFilasScroll, ciAnchoGridScroll, ciAnchoGrid)
        For Each oRow As System.Data.DataRow In _moTablaExterna.DefTabla.Tables(0).Rows
            For i = 0 To oGrid.Columns.Count - 1
                If UCase(oGrid.Columns(i).Key) = UCase(oRow.Item("CAMPO")) Then Exit For
            Next
            With oGrid.Columns(i)
                If UCase(.Header.Text) = UCase(_moTablaExterna.ArtKey) Then
                    .Hidden = True
                Else
                    .Header.Text = oRow.Item("NOMBRE")
                    .Width = Unit.Pixel(CInt(oRow.Item("LONGITUD_CAMPO")))
                    If oRow.Item("TIPOCAMPO") = "FECHA" Then
                        .FormatValue(FSNUser.DateFormat.ShortDatePattern)
                        .Width = Unit.Pixel(CInt(oRow.Item("LONGITUD_CAMPO") * cdAnchoCaracter))
                    Else
                        .Width = Unit.Pixel(CInt(oRow.Item("LONGITUD_NOMBRE") * cdAnchoCaracter))
                    End If
                    iAnchoGrid += .Width.Value
                End If
            End With
        Next

        If iAnchoGrid > iMaxAncho Then GoTo Ajustar
Ajustar:
        For z = 0 To oGrid.Columns.Count - 1
            If Not oGrid.Columns(z).Hidden Then
                oGrid.Columns(z).Width = Unit.Pixel(CInt(oGrid.Columns(z).Width.Value * iMaxAncho / iAnchoGrid))
            End If
        Next

    End Sub

    ''' <summary>
    ''' Carga el valor seleccionado en la grid en la página que ha abierto este popup
    ''' </summary>
    ''' <remarks>Llamada desde: Al hacer clcik sobre el botón aceptar; Tiempo máximo: 0,1 sg</remarks>
    Private Sub cmdAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        Dim sValor As String = ""
        Dim sScript As String = ""
        Dim sMostrar As String = ""
        Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & _
            "<script language=""{0}"">{1}</script>"

        Dim rows As SelectedRowCollection = whdgTabla.GridView.Behaviors.Selection.SelectedRows
        For i As Integer = 0 To rows.Count - 1
            For j As Integer = 0 To whdgTabla.Columns.Count - 1
                If UCase(whdgTabla.Columns(j).Key) = UCase(_moTablaExterna.PrimaryKey) Then
                    sValor = rows.Item(i).Items(j).Value.ToString()
                    sMostrar = _moTablaExterna.DescripcionReg(rows.Item(i).Items(j).Value, Idioma, FSNUser.DateFormat, FSNUser.NumberFormat)
                    Exit For
                End If
            Next
        Next

        sScript = "window.opener.valor_externa_seleccionado(document.forms[0].elements[""hddDataEntry""].value, '" & JSText(sValor) & "', '" & JSText(sMostrar) & "', '" & JSText(IIf(_msArt Is Nothing, "", _msArt)) & "');" & ControlChars.CrLf _
                & "window.close();"
        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "ValorTablaExterna", sScript)
    End Sub

    ''' <summary>
    ''' Carga el valor seleccionado en la grid en la página que ha abierto este popup
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load; Tiempo máximo: 0,1 sg</remarks>
    Private Sub SeleccionarValor()
        Dim sValorAnt As String = Trim(Request.QueryString("valor").ToString())
        Dim oRowReg As DataRow = _moTablaExterna.BuscarRegistro(sValorAnt, _msArt, False)
        Dim oValorKey As Object = Nothing
        Dim iFilaSel As Integer = 0
        Dim sScript As String = Nothing
        Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & _
            "<script language=""{0}"">{1}</script>"

        If Not oRowReg Is Nothing Then oValorKey = oRowReg.Item(_moTablaExterna.PrimaryKey)

        If Not oValorKey Is Nothing Then
            Dim iColClave As Integer
            For i = 0 To whdgTabla.Columns.Count - 1
                If UCase(whdgTabla.Columns(i).Key) = UCase(_moTablaExterna.PrimaryKey) Then
                    iColClave = whdgTabla.Columns(i).Index
                    Exit For
                End If
            Next           
            For Each oRow As GridRecord In whdgTabla.Rows
                oRow.CssClass = ""
                If oRow.Items.Item(iColClave).Value = oValorKey Then
                    oRow.CssClass = "SelectedRow"
                    iFilaSel = oRow.Index
                End If
            Next
        End If

        If iFilaSel >= ciNumFilasScroll Then
            sScript = "function scrollsel(){" & ControlChars.CrLf & _
                "igtbl_getGridById('" & whdgTabla.ClientID & "').scrElem.scrollTop=" & _
                (iFilaSel - ciNumFilasScroll) * 20 & ";" & ControlChars.CrLf & _
                "}" & ControlChars.CrLf
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "scrollsel", sScript)
            sScript = "window.attachEvent('onload',scrollsel);" & ControlChars.CrLf
        End If
        sScript = sScript & "window.opener.valor_original_externa(document.forms[0].elements[""hddDataEntry""].value);"
        sScript = sScript & ControlChars.CrLf & "window.opener.ventexterna=false;"
        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "ValorTablaExternaAct", sScript)
    End Sub

    ''' <summary>
    ''' Realiza la búsqueda en base de datos con los parámetros de búsqueda establecidos
    ''' </summary>
    ''' <remarks>Llamada desde: Al hacer click sobre el botón buscar; Tiempo máx: 0,2 sg.</remarks>
    Private Sub cmdBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscar.Click
        cmdBuscar.CommandArgument = txtBuscar.Text
        _moTablaExterna.LoadData(_mlId, _msArt, True, Nothing, Nothing, hddOrden.Value, "CONVERT(varchar," & _moTablaExterna.PrimaryKey & ") LIKE '" & Replace(cmdBuscar.CommandArgument, "'", "''") & "%'")
        Me.InsertarEnCache("oTabExt_" & FSNUser.Cod, _moTablaExterna.Data)
        ActualizarGrid()
    End Sub

    ''' <summary>
    ''' Actualiza la tabla haciendo una consulta a base de datos
    ''' </summary>
    ''' <remarks>Llamada desde: uwgTabla_Sortcolumn, cmdBuscar_Click, imgArriba_Click, imgAbajo_Click; Tiempo másx: 0,2 sg</remarks>
    Private Sub ActualizarGrid()
        Dim sFiltro As String = String.Empty
        If cmdBuscar.CommandArgument <> "" Then _
            sFiltro = "CONVERT(varchar," & _moTablaExterna.PrimaryKey & ") LIKE '" & Replace(cmdBuscar.CommandArgument, "'", "''") & "%'"
        _moTablaExterna.LoadData(_mlId, _msArt, True, 100, hddFilaIni.Value, hddOrden.Value, sFiltro)
        Dim oGrid As WebHierarchicalDataGrid
        oGrid = Me.whdgTabla
        oGrid.DataSource = _moTablaExterna.Data
        CrearColumnas()
        oGrid.DataBind()
        FormatGrid()
    End Sub

    ''' <summary>
    ''' Tras la carga del grid se "sincroniza" con el Custom Pager
    ''' </summary>
    ''' <param name="sender">control</param>
    ''' <param name="e">evento de sistema</param>  
    ''' <remarks>Llamada desde: sistema; Tiempo mÃ¡ximo:0 </remarks>
    Private Sub whdgTabla_DataBound(sender As Object, e As System.EventArgs) Handles whdgTabla.DataBound
        Dim pagerList As DropDownList = DirectCast(whdgTabla.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("PagerPageList"), DropDownList)
        pagerList.Items.Clear()
        For i As Integer = 1 To whdgTabla.GridView.Behaviors.Paging.PageCount
            pagerList.Items.Add(i.ToString())
        Next
        CType(whdgTabla.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblCount"), Label).Text = whdgTabla.GridView.Behaviors.Paging.PageCount
        Dim SoloUnaPagina As Boolean = (whdgTabla.GridView.Behaviors.Paging.PageCount = 1)
        With CType(whdgTabla.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
            .Enabled = Not SoloUnaPagina
        End With
        With CType(whdgTabla.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
            .Enabled = Not SoloUnaPagina
        End With
    End Sub
    Private Sub CargarGrid()
        whdgTabla.Rows.Clear()
        whdgTabla.Columns.Clear()
        whdgTabla.GridView.Columns.Clear()

        whdgTabla.DataSource = CType(Cache("oTabExt_" & FSNUser.Cod), DataSet)
        CrearColumnas()
        whdgTabla.DataBind()
        FormatGrid()
        SeleccionarValor()
    End Sub
End Class