﻿Imports Infragistics.Web.UI

Partial Public Class BuscadorProcesos
    Inherits FSNPage

    Private ReadOnly Property DataSourceCacheKey() As String
        Get
            Return "Procesos_" + Usuario.Cod.ToString()
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.BusquedaContratos
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaTheme", "<script>var rutaTheme = '" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "'</script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "'</script>")
        If Not IsPostBack Then
            HttpContext.Current.Cache.Remove(DataSourceCacheKey)

            FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/buscadorprocesos.gif"

            CargarTextos()
            CargarEstados()

            optFiltrar1.Checked = True

            InicializacionControles()

            'Valores de filtro que pueden venir de la pagina que abre el buscador
            If Not String.IsNullOrEmpty(Request("Anyo")) Then
                hfAnyo.Value = Request("Anyo")
            End If
            If Not String.IsNullOrEmpty(Request("Commodity")) Then
                hfCommodity.Value = Request("Commodity")
            End If
            If Not String.IsNullOrEmpty(Request("CodProceso")) Then
                hfCodProceso.Value = Request("CodProceso")
            End If
            If Not String.IsNullOrEmpty(Request("DenProceso")) Then
                hfDenProceso.Value = Request("DenProceso")
            End If
        End If
        If Not Page.IsPostBack Then
            With CType(whdgProcesos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero_desactivado.gif"
                .Attributes.Add("onClick", "return FirstPage();")
                .Enabled = False
            End With
            With CType(whdgProcesos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior_desactivado.gif"
                .Attributes.Add("onClick", "return PrevPage();")
                .Enabled = False
            End With
            Dim SoloUnaPagina As Boolean = True
            With CType(whdgProcesos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
                .Attributes.Add("onClick", "return NextPage();")
                .Enabled = Not SoloUnaPagina
            End With
            With CType(whdgProcesos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
                .Attributes.Add("onClick", "return LastPage();")
                .Enabled = Not SoloUnaPagina
            End With
        End If
        'whdgProcesos.DisplayLayout.FilterOptionsDefault.FilterUIType = IIf(optFiltrar1.Checked = True, FilterUIType.HeaderIcons, FilterUIType.FilterRow)

        CargarMonedas()

        imgMaterialLupa.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Img_Ico_Lupa.gif"
        imgArticuloLupa.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Img_Ico_Lupa.gif"
        imgProveedorLupa.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Img_Ico_Lupa.gif"

        'Funcion que sirve para eliminar el material y el articulo relacionado
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarMaterial") Then
            Dim sScript As String = "function eliminarMaterial(event) {" & vbCrLf & _
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf & _
                    " return true; " & vbCrLf & _
            " } else { " & vbCrLf & _
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                    " campo = document.getElementById('" & txtMaterial.ClientID & "')" & vbCrLf & _
                    " if (campo) { campo.value = ''; } " & vbCrLf & _
                    " campoHidden = document.getElementById('" & hfMaterial.ClientID & "')" & vbCrLf & _
                    " if (campoHidden) { campoHidden.value = ''; } " & vbCrLf & _
                    " campoArticulo = document.getElementById('" & txtArticulo.ClientID & "')" & vbCrLf & _
                    " if (campoArticulo) { campoArticulo.value = ''; } " & vbCrLf & _
                    " campoHiddenArticulo = document.getElementById('" & hfArticulo.ClientID & "')" & vbCrLf & _
                    " if (campoHiddenArticulo) { campoHiddenArticulo.value = ''; } " & vbCrLf & _
            " } " & vbCrLf & _
            " return false; " & vbCrLf & _
            " } }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarMaterial", sScript, True)
        End If

        'Funcion que sirve para eliminar el articulo relacionado
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarArticulo") Then
            Dim sScript As String = "function eliminarArticulo(event) {" & vbCrLf & _
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf & _
                    " return true; " & vbCrLf & _
            " } else { " & vbCrLf & _
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                    " campoArticulo = document.getElementById('" & txtArticulo.ClientID & "')" & vbCrLf & _
                    " if (campoArticulo) { campoArticulo.value = ''; } " & vbCrLf & _
                    " campoHiddenArticulo = document.getElementById('" & hfArticulo.ClientID & "')" & vbCrLf & _
                    " if (campoHiddenArticulo) { campoHiddenArticulo.value = ''; } " & vbCrLf & _
            " } " & vbCrLf & _
            " return false; " & vbCrLf & _
            " } }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarArticulo", sScript, True)
        End If

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalProgress", "<script>var ModalProgress = '" & ModalProgress.ClientID & "' </script>")

        'Cargo los datos
        If Not Page.IsPostBack Then
            Dim ds As New DataSet
            whdgProcesos.Rows.Clear()
            whdgProcesos.Columns.Clear()
            whdgProcesos.GridView.Columns.Clear()
            Dim dataSource As DataTable
            If Not CType(HttpContext.Current.Cache.Item(DataSourceCacheKey), DataTable) Is Nothing Then
                dataSource = CType(HttpContext.Current.Cache.Item(DataSourceCacheKey), DataTable)
            Else
                dataSource = CrearTablaVacia()
            End If
            ds.Tables.Add(dataSource)
            whdgProcesos.DataSource = ds
            CrearColumnasTab(dataSource)
            whdgProcesos.DataBind()
            ConfigurarGrid()
        End If
    End Sub

    ''' <summary>
    ''' Establece los textos de los controles de acuerdo con el idioma del usuario
    ''' </summary>
    Private Sub CargarTextos()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.BusquedaContratos

        FSNPageHeader.TituloCabecera = Textos(0)
        lblMaterial.Text = String.Format("{0}:", Textos(1))
        lblArticulo.Text = String.Format("{0}:", Textos(2))
        lblProveedor.Text = String.Format("{0}:", Textos(3))
        lblEstado.Text = String.Format("{0}:", Textos(4))
        lblMoneda.Text = String.Format("{0}:", Textos(5))
        lblSolicitud.Text = String.Format("{0}:", Textos(6))
        lblfsPresupuesto.Text = Textos(7)
        lblPresupuestoDesde.Text = String.Format("{0}:", Textos(8))
        lblPresupuestoHasta.Text = String.Format("{0}:", Textos(9))
        lblfsFechaApertura.Text = Textos(10)
        lblFechaDesde.Text = String.Format("{0}:", Textos(8))
        lblFechaHasta.Text = String.Format("{0}:", Textos(9))
        rblOpcionesAdjudicacion.Items(0).Text = Textos(11)
        rblOpcionesAdjudicacion.Items(1).Text = Textos(12)
        btnBuscar.Text = Textos(13)
        lblFiltrarPor.Text = String.Format("{0}:", Textos(29))
        btnAceptar.Text = Textos(30)
        btnCancelar.Text = Textos(31)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Notificaciones
        CType(whdgProcesos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPage"), Label).Text = Textos(46)
        CType(whdgProcesos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblOF"), Label).Text = Textos(48)
        'Vuelvo a cargar el modulo de BusquedaContratos
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.BusquedaContratos
    End Sub

    ''' <summary>
    ''' Carga la lista de estados de proceso
    ''' </summary>
    ''' <remarks>Llamada desde=Page_Load; Tiempo máximo=0seg.</remarks>
    Private Sub CargarEstados()
        'Pendiente de validar apertura
        AñadirElementoEstado(TipoEstadoProceso.ConItemsSinValidar, Me.Textos(19))
        'Pendiente de asignar proveedores
        AñadirElementoEstado(TipoEstadoProceso.Conproveasignados, Me.Textos(20))
        'Pendiente de enviar peticiones
        AñadirElementoEstado(TipoEstadoProceso.conasignacionvalida, Me.Textos(21))
        'En recepción de ofertas 
        'conofertas
        AñadirElementoEstado(TipoEstadoProceso.conpeticiones, Me.Textos(22))
        'Pendiente de comunicar objetivos
        'ConObjetivosSinNotificarYPreadjudicado
        AñadirElementoEstado(TipoEstadoProceso.ConObjetivosSinNotificar, Me.Textos(23))
        'Pendiente de adjudicar
        AñadirElementoEstado(TipoEstadoProceso.PreadjYConObjNotificados, Me.Textos(24))
        'Parcialmente cerrado
        AñadirElementoEstado(TipoEstadoProceso.ParcialmenteCerrado, Me.Textos(25))
        'Pendiente de notificar adjudicaciones
        AñadirElementoEstado(TipoEstadoProceso.conadjudicaciones, Me.Textos(26))
        'Adjudicado y notificado
        AñadirElementoEstado(TipoEstadoProceso.ConAdjudicacionesNotificadas, Me.Textos(27))
        'Anulado
        AñadirElementoEstado(TipoEstadoProceso.Cerrado, Me.Textos(28))

        wddEstado.CurrentValue = ""
        wddEstado.SelectedValue = Nothing
    End Sub

    Private Sub AñadirElementoEstado(ByVal value As Nullable(Of Integer), ByVal text As String)
        Dim oItem As New ListControls.DropDownItem
        oItem.Value = value
        oItem.Text = text
        wddEstado.Items.Add(oItem)
    End Sub

    Private Function BuscarProcesos() As DataTable
        'Filtros externos
        Dim anyo As Nullable(Of Integer)
        If IsNumeric(hfAnyo.Value) Then
            anyo = CInt(hfAnyo.Value)
        End If
        Dim commodity As String = hfCommodity.Value
        Dim codigoProceso As Nullable(Of Integer)
        If IsNumeric(hfCodProceso.Value) Then
            codigoProceso = CInt(hfCodProceso.Value)
        End If
        Dim denominacionProceso As String = hfDenProceso.Value

        'Filtros del buscador
        Dim gmn1 As String = Nothing
        Dim gmn2 As String = Nothing
        Dim gmn3 As String = Nothing
        Dim gmn4 As String = Nothing
        If Not String.IsNullOrEmpty(hfMaterial.Value) Then
            Dim gmns() As String = Split(hfMaterial.Value, "-")
            If gmns.Length = 1 Then
                gmn1 = gmns(0).Trim()
            ElseIf gmns.Length = 2 Then
                gmn1 = gmns(0).Trim()
                gmn2 = gmns(1).Trim()
            ElseIf gmns.Length = 3 Then
                gmn1 = gmns(0).Trim()
                gmn2 = gmns(1).Trim()
                gmn3 = gmns(2).Trim()
            ElseIf gmns.Length = 4 Then
                gmn1 = gmns(0).Trim()
                gmn2 = gmns(1).Trim()
                gmn3 = gmns(2).Trim()
                gmn4 = gmns(3).Trim()
            End If
        Else
            If FSNUser.Pyme > 0 Then
                Dim oGruposMaterial As FSNServer.GruposMaterial
                oGruposMaterial = FSNServer.Get_Object(GetType(FSNServer.GruposMaterial))
                gmn1 = oGruposMaterial.DevolverGMN1_PYME(FSNUser.Pyme)
            End If
        End If


        Dim art As String = hfArticulo.Value

        Dim prove As String = hfProveedor.Value

        Dim estados(-1) As Integer
        If wddEstado.SelectedItems.Count > 0 Then
            ReDim estados(wddEstado.SelectedItems.Count - 1)
        End If
        For i As Integer = 0 To wddEstado.SelectedItems.Count - 1
            estados(i) = CInt(wddEstado.SelectedItems(i).Value)
        Next

        Dim mon As String = wddMoneda.SelectedValue

        Dim solicitud As String = txtSolicitud.Text

        Dim presupuestoDesde As Nullable(Of Double)
        If Not Double.IsNaN(wnePresupuestoDesde.Value) Then
            presupuestoDesde = wnePresupuestoDesde.Value
        End If

        Dim presupuestoHasta As Nullable(Of Double)
        If Not Double.IsNaN(wnePresupuestoHasta.Value) Then
            presupuestoHasta = wnePresupuestoHasta.Value
        End If

        Dim fechaDesde As Nullable(Of Date)
        If IsDate(dteFechaDesde.Value) Then
            fechaDesde = dteFechaDesde.Value
        End If

        Dim fechaHasta As Nullable(Of Date)
        If IsDate(dteFechaHasta.Value) Then
            fechaHasta = dteFechaHasta.Value
        End If

        Dim adjudicacionDirecta As Boolean = rblOpcionesAdjudicacion.Items(0).Selected

        Dim p As FSNServer.Proceso = FSNServer.Get_Object(GetType(FSNServer.Proceso))
        Dim result As DataTable = p.BuscarProcesos(Usuario.CodPersona, anyo, commodity, codigoProceso, denominacionProceso, _
                                                   gmn1, gmn2, gmn3, gmn4, art, prove, estados, mon, solicitud, _
                                                   presupuestoDesde, presupuestoHasta, fechaDesde, fechaHasta, _
                                                   adjudicacionDirecta)

        result = InsertarColumnaTextoEstado(result)

        Me.InsertarEnCache(DataSourceCacheKey, result)

        Return result
    End Function

    Private Function InsertarColumnaTextoEstado(ByVal dt As DataTable) As DataTable
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.BusquedaContratos
        If Not dt Is Nothing Then
            dt.Columns.Add("EST_TEXT", GetType(String))
            For Each row As DataRow In dt.Rows
                Select Case row("EST")
                    Case TipoEstadoProceso.ConItemsSinValidar
                        row("EST_TEXT") = Textos(19)
                    Case TipoEstadoProceso.Conproveasignados
                        row("EST_TEXT") = Textos(20)
                    Case TipoEstadoProceso.conasignacionvalida
                        row("EST_TEXT") = Textos(21)
                    Case TipoEstadoProceso.conpeticiones
                        row("EST_TEXT") = Textos(22)
                    Case TipoEstadoProceso.ConObjetivosSinNotificar
                        row("EST_TEXT") = Textos(23)
                    Case TipoEstadoProceso.PreadjYConObjNotificados
                        row("EST_TEXT") = Textos(24)
                    Case TipoEstadoProceso.ParcialmenteCerrado
                        row("EST_TEXT") = Textos(25)
                    Case TipoEstadoProceso.conadjudicaciones
                        row("EST_TEXT") = Textos(26)
                    Case TipoEstadoProceso.ConAdjudicacionesNotificadas
                        row("EST_TEXT") = Textos(27)
                    Case TipoEstadoProceso.Cerrado
                        row("EST_TEXT") = Textos(28)
                End Select
            Next
        End If
        Return dt
    End Function

    Private Sub CargarMonedas()
        Dim currentSelectedItem As ListControls.DropDownItem = wddMoneda.SelectedItem

        Dim monedas As DataSet
        Dim identificadorCache As String = String.Format("{0}_{1}_{2}", "Monedas", Me.Usuario.CodPersona, Me.Usuario.Idioma.ToString())
        If Not HttpContext.Current.Cache(identificadorCache) Is Nothing Then
            monedas = CType(HttpContext.Current.Cache(identificadorCache), DataSet)
        Else
            Dim oMonedas As FSNServer.Monedas
            oMonedas = Me.FSNServer.Get_Object(GetType(FSNServer.Monedas))
            oMonedas.LoadData(FSNUser.Idioma.ToString())
            monedas = oMonedas.Data()
            If Not monedas Is Nothing Then Me.InsertarEnCache(identificadorCache, monedas)
        End If
        If Not monedas Is Nothing AndAlso monedas.Tables.Count > 0 Then
            wddMoneda.ValueField = "COD"
            wddMoneda.TextField = "COD"
            wddMoneda.DataSource = monedas.Tables(0)
            wddMoneda.DataBind()
        End If

        If currentSelectedItem Is Nothing Then
            wddMoneda.CurrentValue = ""
            wddMoneda.SelectedValue = Nothing
        Else
            wddMoneda.SelectedValue = currentSelectedItem.Value
        End If
    End Sub

    ''' <summary>
    ''' Inicializa los componentes y funciones javascript de los elementos
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0seg.</remarks>
    Private Sub InicializacionControles()

        imgMaterialLupa.Attributes.Add("onClick", "javascript:var newWindow = window.open('" & ConfigurationManager.AppSettings("rutaPM") & "_common/materiales.aspx?desde=WebPart&IdControl=" & txtMaterial.ID & "&MaterialGS_BBDD=" & hfMaterial.ID & "','_blank', 'width=550,height=550,status=yes,resizable=no,top=100,left=200');newWindow.focus();return false;")
        txtMaterial.Attributes.Add("onkeydown", "javascript:return eliminarMaterial(event)")

        txtArticulo.Attributes.Add("onkeydown", "javascript:return eliminarArticulo(event)")

        txtProveedor.Attributes.Add("onKeyDown", "return ControlProveedor(this, event);")
        imgProveedorLupa.OnClientClick = "var newWindow = window.open('" & ConfigurationManager.AppSettings("RutaFS") & "_common/BuscadorProveedores.aspx?PM=true&desde=TextBoxAndHiddenField&IDCONTROL=" & txtProveedor.ID & "&HiddenFieldID=" & hfProveedor.ID & "', '_blank', 'width=835,height=635,status=yes,resizable=no,top=0,left=150,scrollbars=yes');newWindow.focus();return false;"

        imgArticuloLupa.OnClientClick = "javascript: devolverDatosBuscArtic();"

        dteFechaDesde.NullText = ""

        dteFechaHasta.NullText = ""


    End Sub

    Private Function CrearTablaVacia() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("ANYO", GetType(Integer))
        dt.Columns.Add("GMN1", GetType(Integer))
        dt.Columns.Add("COD", GetType(Integer))
        dt.Columns.Add("DEN", GetType(Integer))
        dt.Columns.Add("EST", GetType(Integer))
        dt.Columns.Add("EST_TEXT", GetType(Integer))
        Return dt
    End Function
    Private Sub CrearColumnasTab(ByVal datasource As DataTable)
        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
        Dim nombre As String

        For i As Integer = 0 To datasource.Columns.Count - 1
            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            nombre = datasource.Columns(i).ToString
            With campoGrid
                .Key = nombre
                .DataFieldName = nombre
                .Header.Text = nombre
            End With
            whdgProcesos.Columns.Add(campoGrid)
            whdgProcesos.GridView.Columns.Add(campoGrid)
        Next
    End Sub

    Private Sub CrearColumnas()
        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
        Dim nombre As String

        For i As Integer = 0 To whdgProcesos.DataSource.Tables(0).Columns.Count - 1
            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            nombre = whdgProcesos.DataSource.Tables(0).Columns(i).ToString
            With campoGrid
                .Key = nombre
                .DataFieldName = nombre
                .Header.Text = nombre
            End With
            whdgProcesos.Columns.Add(campoGrid)
            whdgProcesos.GridView.Columns.Add(campoGrid)
        Next
    End Sub
    Private Sub ConfigurarGrid()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.BusquedaContratos

        whdgProcesos.Height = Unit.Pixel(225)

        whdgProcesos.Columns("ANYO").Header.Text = Textos(14)
        whdgProcesos.Columns("GMN1").Header.Text = Textos(15)
        whdgProcesos.Columns("COD").Header.Text = Textos(16)
        whdgProcesos.Columns("DEN").Header.Text = Textos(17)
        whdgProcesos.Columns("EST_TEXT").Header.Text = Textos(18)

        whdgProcesos.Columns("ANYO").Width = Unit.Pixel(50)
        whdgProcesos.Columns("GMN1").Width = Unit.Pixel(100)
        whdgProcesos.Columns("COD").Width = Unit.Pixel(100)
        whdgProcesos.Columns("DEN").Width = Unit.Pixel(250)
        whdgProcesos.Columns("EST_TEXT").Width = Unit.Pixel(250)

        whdgProcesos.Columns("EST").Hidden = True

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        whdgProcesos.Rows.Clear()
        whdgProcesos.Columns.Clear()
        whdgProcesos.GridView.Columns.Clear()
        Dim datasource As DataTable
        Dim ds As New DataSet
        datasource = BuscarProcesos()
        ds.Tables.Add(datasource)
        whdgProcesos.DataSource = ds
        CrearColumnas()
        whdgProcesos.DataBind()
        ConfigurarGrid()
    End Sub

    ''' <summary>
    ''' Pone el texto correspondiente en la etiqueta lblProcesando.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: El prerender de la etiqueta. Tiempo máximo: 0sg.</remarks>
    Protected Sub LblProcesando_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, Label).Text = Textos(46)
    End Sub

    Private Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Not String.IsNullOrEmpty(Session("desdeGS")) Then
            ConfigurarSkin()
        End If
    End Sub

    ''' <summary>
    ''' Configura el Skin de algunos controles si el visor se muestra desde GS para que tengan la apariencia correcta
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ConfigurarSkin()
        Me.frmBuscadorProcesos.Style.Item("background-color") = "#DCDCDC"

        Me.FSNPageHeader.AspectoGS = True

        Me.btnBuscar.SkinID = "GS"
        Me.btnAceptar.SkinID = "GS"
        Me.btnCancelar.SkinID = "GS"

        Me.imgProveedorLupa.SkinID = "BuscadorLupa_GS"
        Me.imgMaterialLupa.SkinID = "BuscadorLupa_GS"
        Me.imgArticuloLupa.SkinID = "BuscadorLupa_GS"

    End Sub

    Private Sub whdgProcesos_DataBound(sender As Object, e As System.EventArgs) Handles whdgProcesos.DataBound
        Dim pagerList As DropDownList = DirectCast(whdgProcesos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("PagerPageList"), DropDownList)
        pagerList.Items.Clear()
        For i As Integer = 1 To whdgProcesos.GridView.Behaviors.Paging.PageCount
            pagerList.Items.Add(i.ToString())
        Next
        CType(whdgProcesos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblCount"), Label).Text = whdgProcesos.GridView.Behaviors.Paging.PageCount
        Dim SoloUnaPagina As Boolean = (whdgProcesos.GridView.Behaviors.Paging.PageCount = 1)
        With CType(whdgProcesos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
            .Enabled = Not SoloUnaPagina
        End With
        With CType(whdgProcesos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
            .Enabled = Not SoloUnaPagina
        End With
    End Sub

End Class