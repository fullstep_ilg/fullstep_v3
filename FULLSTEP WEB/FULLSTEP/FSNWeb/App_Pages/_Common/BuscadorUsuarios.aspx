﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BuscadorUsuarios.aspx.vb" Inherits="Fullstep.FSNWeb.BuscadorUsuarios" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>    
    <style type="text/css">
        #whdgUsuarios_main
        {
            height:250px !important; 
        }
    </style>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
</head>
<body style="background-color: #FFFFFF; margin-top: 0px; margin-left: 0px; margin-right: 0px">
     <!-- Eventos de controles -->
    <script type="text/javascript">
        function init() {
            window.focus();
        };

        function ugtxtUO_wdg_RowSelectionChanged(sender, e) {
            //Mostramos el texto seleccionado del grid en el combo
            var ddUO = $find('<%= ugtxtUO.ClientID %>');
            var displayText = e.getSelectedRows().getItem(0).get_cellByColumnKey("DEN").get_text();
            ddUO.set_currentValue(displayText, true);
            ddUO._elements["Input"].focus();
            ddUO.closeDropDown();
        }

        function whdgUsuarios_RowSelectionChanged(sender, eventArgs) {
            //Si la selección es múltiple, no hacemos nada
            if ($('#Multiple').val()) return;

            var rows = eventArgs.getSelectedRows();
            var selectedRow = rows.getItem(0);
            //Marcamos el checkbox de la fila
            $('input[id$=cbSEL]', selectedRow.get_element()).prop("checked", true);

            //Pulsamos el boton aceptar
            cmdAceptar_Click();
        }
        
        function cmdAceptar_Click() {
            var oGrid = $find("<%= whdgUsuarios.ClientID %>");
            var oRows = oGrid.get_gridView().get_rows();
            var seleccion = [];

            var rol = $('#Rol').val();
            //Recorremos las filas comprobando si está seleccionado el checkbox
            for (var i = 0; i < oRows.get_length(); i++) {
                var row = oRows.get_row(i);
                if ($('input[id$=cbSEL]', row.get_element()).prop("checked")) {
                    if (rol == "") {
                        var obj = {
                            id: row.get_cellByColumnKey("COD").get_value(),
                            name: row.get_cellByColumnKey("USUARIO").get_value(),
                            nombre: row.get_cellByColumnKey("NOM").get_value(),
                            apellido: row.get_cellByColumnKey("APE").get_value(),
                            email: row.get_cellByColumnKey("EMAIL").get_value(),
                            UON1: row.get_cellByColumnKey("UON1").get_value(),
                            UON2: row.get_cellByColumnKey("UON2").get_value(),
                            UON3: row.get_cellByColumnKey("UON3").get_value(),
                            departamento: row.get_cellByColumnKey("DEP").get_value()
                        };
                    }
                    else {
                        var obj = {
                            id: row.get_cellByColumnKey("PARTCOD").get_value(),
                            name: row.get_cellByColumnKey("PARTNOM").get_value(),
                        };
                    }
                    seleccion.push(obj);
                }
            }

            //Si no hay seleccionados no hacemos nada
            if (seleccion.length == 0) return false;
            
            //Dependiendo desde donde se llame hacemos una cosa diferente
            //(Intentar que la forma de llamar sea la de la opción "TokenInput")
            if (rol == "") {
                switch ($('#Desde').val()) {
                    case 'IM':
                        window.opener.usu_seleccionado(seleccion[0].id, seleccion[0].name, seleccion[0].email);
                        break;
                    case 'AltaContratos':
                    case 'contratos':
                        window.opener.usu_seleccionado(seleccion[0].id, seleccion[0].name, seleccion[0].email);
                        break;
                    case 'QA':
                        window.opener.AgregarNotificadosInternos($.map(seleccion, function (x) { return x.id + "#" + x.name + "#" + x.email }).join("###") + "###");
                        break;
                    case 'TokenInput':
                    case 'CompartirEscenario':
                        //Llamamos al evento del botón con los items seleccionados
                        $('#' + $("#IdControl").val(), window.opener.$(window.opener.document)).trigger('addItems', [seleccion]);
                        break;
                    default:
                        var filaGrid = $("#FilaGrid").val();
                        if (filaGrid == "") {
                            var idControl = $("#IdControl").val();
                            if (idControl == "") {
                                window.opener.SelectedPostBack(seleccion[0].UON1 + ";" + seleccion[0].UON2 + ";" + seleccion[0].UON3 + ";", seleccion[0].departamento, seleccion[0].nombre, seleccion[0].apellido, seleccion[0].id);
                            }
                            else {
                                window.opener.usu_seleccionado(idControl, seleccion[0].id, seleccion[0].name, seleccion[0].email);
                            }
                        } else {
                            //se llama desde NWrealizarAccion
                            window.opener.usu_seleccionado(seleccion[0].id, seleccion[0].name, filaGrid);
                        };
                        break;
                }
            } else {                
                window.opener.usu_seleccionado(seleccion[0].id, seleccion[0].name, rol);
            }
            //Cerramos la ventana
            window.close();
        }
    </script>
    <!--Paginación -->
    <script type="text/javascript">
        function AdministrarPaginador() {
            var grid = $find("<%= whdgUsuarios.ClientID %>");
            var parentGrid = grid;
            $('[id$=lblCount]').text(parentGrid.get_gridView().get_behaviors().get_paging().get_pageCount());
            if (parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() == 0) {
                $('[id$=ImgBtnFirst]').attr('disabled', 'disabled');
                $('[id$=ImgBtnFirst]').attr('src', rutaTheme + '/images/primero_desactivado.gif');
                $('[id$=ImgBtnPrev]').attr('disabled', 'disabled');
                $('[id$=ImgBtnPrev]').attr('src', rutaTheme + '/images/anterior_desactivado.gif');
            } else {
                $('[id$=ImgBtnFirst]').removeAttr('disabled');
                $('[id$=ImgBtnFirst]').attr('src', rutaTheme + '/images/primero.gif');
                $('[id$=ImgBtnPrev]').removeAttr('disabled');
                $('[id$=ImgBtnPrev]').attr('src', rutaTheme + '/images/anterior.gif');
            }
            if (parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() == parentGrid.get_gridView().get_behaviors().get_paging().get_pageCount() - 1) {
                $('[id$=ImgBtnNext]').attr('disabled', 'disabled');
                $('[id$=ImgBtnNext]').attr('src', rutaTheme + '/images/siguiente_desactivado.gif');
                $('[id$=ImgBtnLast]').attr('disabled', 'disabled');
                $('[id$=ImgBtnLast]').attr('src', rutaTheme + '/images/ultimo_desactivado.gif');
            } else {
                $('[id$=ImgBtnNext]').removeAttr('disabled');
                $('[id$=ImgBtnNext]').attr('src', rutaTheme + '/images/siguiente.gif');
                $('[id$=ImgBtnLast]').removeAttr('disabled');
                $('[id$=ImgBtnLast]').attr('src', rutaTheme + '/images/ultimo.gif');
            }
        }

        function whdgUsuarios_PageIndexChanged(a, b, c, d) {
            var grid = $find("<%= whdgUsuarios.ClientID %>");
            var parentGrid = grid;
            var dropdownlist = $('[id$=PagerPageList]')[0];

            dropdownlist.options[parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex()].selected = true;
            AdministrarPaginador();
        }

        function IndexChanged() {
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var grid = $find("<%= whdgUsuarios.ClientID %>");
            var parentGrid = grid.get_gridView();
            var newValue = dropdownlist.selectedIndex;
            parentGrid.get_behaviors().get_paging().set_pageIndex(newValue);
            AdministrarPaginador();
        }

        function FirstPage() {
            var grid = $find("<%= whdgUsuarios.ClientID %>");
            var parentGrid = grid;
            parentGrid.get_gridView().get_behaviors().get_paging().set_pageIndex(0);
        }

        function PrevPage() {
            var grid = $find("<%= whdgUsuarios.ClientID %>");
            var parentGrid = grid;
            var dropdownlist = $('[id$=PagerPageList]')[0];
            if (parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() > 0) {
                parentGrid.get_gridView().get_behaviors().get_paging().set_pageIndex(parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() - 1);
            }
        }

        function NextPage() {
            var grid = $find("<%= whdgUsuarios.ClientID %>");
            var parentGrid = grid;
            var dropdownlist = $('[id$=PagerPageList]')[0];
            if (parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() < parentGrid.get_gridView().get_behaviors().get_paging().get_pageCount() - 1) {
                parentGrid.get_gridView().get_behaviors().get_paging().set_pageIndex(parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() + 1);
            }

        }
        function LastPage() {
            var grid = $find("<%= whdgUsuarios.ClientID %>");
            var parentGrid = grid;
            parentGrid.get_gridView().get_behaviors().get_paging().set_pageIndex(parentGrid.get_gridView().get_behaviors().get_paging().get_pageCount() - 1);
        }
    </script>   
    <form id="Form1" method="post" runat="server">
        <asp:ScriptManager ID="scrMng_usu" runat="server"></asp:ScriptManager>
        <input id="Desde" type="hidden" name="Desde" runat="server"/>
        <input id="IdControl" type="hidden" name="IdControl" runat="server"/>
        <input id="IdHidControl" type="hidden" name="IdHidControl" runat="server" />
        <input id="Rol" type="hidden" name="Rol" runat="server" />
        <input id="FilaGrid" type="hidden"	name="FilaGrid" runat="server"/>
        <input id="Multiple" type="hidden" name="Multiple" runat="server" />

        <asp:Panel ID="pnlTitulo" runat="server" Width="100%">
            <table width="100%" cellpadding="0" border="0">
            <tr>
                <td>
                    <fsn:FSNPageHeader ID="FSNPageHeader" runat="server">
                    </fsn:FSNPageHeader>
                </td>
            </tr>
            </table>
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="updFiltro" ChildrenAsTriggers="True" UpdateMode="Always" RenderMode="Inline">
            <ContentTemplate>
                <table id="pnlFiltro" style="z-index: 106; width: 98%; margin:5px 5px 0px 5px;" cellspacing="0" cellpadding="2" align="center" border="0">
			    <tr>
				    <td>
					    <asp:Label id="lblUO" runat="server" CssClass="Etiqueta" Width="100%"></asp:Label>
                    </td>
				    <td>
                        <ig:WebDropDown ID="ugtxtUO" runat="server" Width="200px" EnableClosingDropDownOnBlur="true" EnableClosingDropDownOnSelect="true" 
                            DropDownContainerWidth="345" ValueField="ID" TextField="DEN" DisplayMode="ReadOnlyList">
                            <Items>
                                <ig:DropDownItem>
                                </ig:DropDownItem>
                            </Items>                        
                            <ItemTemplate>
                                <ig:WebDataGrid ID="ugtxtUO_wdg" runat="server" AutoGenerateColumns="false" Width="100%" ShowHeader="false" 
                                    EnableAjaxViewState="true" EnableDataViewState="true" EnableAjax="False">
                                    <Columns>
                                        <ig:BoundDataField DataFieldName="ID" Key="ID" Hidden="true" />                                    
                                        <ig:BoundDataField DataFieldName="COD_UON1" Key="COD_UON1" Hidden="true" />                                           
                                        <ig:BoundDataField DataFieldName="COD_UON2" Key="COD_UON2" Hidden="true" />                                           
                                        <ig:BoundDataField DataFieldName="COD_UON3" Key="COD_UON3" Hidden="true" />                                    
                                        <ig:BoundDataField DataFieldName="DEN" Key="DEN" Width="100%" />                                    
                                    </Columns>
                                    <Behaviors>
                                        <ig:Selection RowSelectType="Single" Enabled="True" CellClickAction="Row">
                                            <AutoPostBackFlags RowSelectionChanged="true" />
                                            <SelectionClientEvents RowSelectionChanged="ugtxtUO_wdg_RowSelectionChanged" />
                                        </ig:Selection>
                                        <ig:RowSelectors Enabled="false" />
                                    </Behaviors>                                
                                </ig:WebDataGrid>
                            </ItemTemplate>                        
                        </ig:WebDropDown>
				    </td>
                    <td>
					    <asp:Label id="lblDep" runat="server" CssClass="Etiqueta" Width="100%"></asp:Label>
                    </td>
                    <td>					
					    <ig:WebDropDown ID="ugtxtDEP" runat="server" Width="200px" EnableClosingDropDownOnSelect="true"
						    DropDownContainerHeight="200px" DropDownContainerWidth="" DropDownAnimationType="EaseOut" 
						    EnablePaging="False" PageSize="12" ValueField="COD" TextField="DEN">
					    </ig:WebDropDown>                    
				    </td>
                </tr>
			    <tr>
				    <td>
					    <asp:Label id="lblNombre" runat="server" CssClass="Etiqueta" Width="100%"></asp:Label>
                    </td>
				    <td>
					    <igpck:WebTextEditor id="ugtxtNombre" runat="server" Width="196px"></igpck:WebTextEditor>
                    </td>
				    <td>
					    <asp:Label id="lblApe" runat="server" Width="100%" CssClass="Etiqueta"></asp:Label>
                    </td>
				    <td>
					    <igpck:WebTextEditor id="ugtxtApe" runat="server" Width="196px"></igpck:WebTextEditor>
                    </td>
			    </tr>
                <tr>
                    <td>
					    <asp:Label id="lblCodigo" runat="server" CssClass="Etiqueta" Width="100%"></asp:Label>
                    </td>
				    <td>
					    <igpck:WebTextEditor id="ugtxtCodigo" runat="server" Width="196px"></igpck:WebTextEditor>
                    </td>
                    <td>
					    <asp:Label id="lblEqp" runat="server" CssClass="Etiqueta" Width="100%" Visible="false"></asp:Label>
                    </td>
				    <td>
                        <ig:WebDropDown ID="ugtxtEqp" runat="server" Width="200px" EnableClosingDropDownOnBlur="true" EnableClosingDropDownOnSelect="true" 
                            DropDownContainerWidth="200px" DropDownAnimationType="EaseOut" EnablePaging="False" CurrentValue="" PageSize="12" Visible="false" 
                            ValueField="COD" TextField="DEN">					    
                        </ig:WebDropDown>
                    </td>
			    </tr>                
		        </table>                
            </ContentTemplate>
        </asp:UpdatePanel>
        <div style="width:100%; height:auto; text-align:center; margin-top:2em">
            <asp:Button runat="server" ID="cmdBuscar" CssClass="boton" style="width: 100px; margin-top:2px;" />
        </div>
		<asp:Label id="lblResult" style="margin-left:1em; z-index: 101;" runat="server" CssClass="Etiqueta" Width="528px">Resultado de la búsqueda:</asp:Label>			
		<hr style="z-index:102; width:99%; height:4px; color:#dcdcdc;" noshade="noshade" size="4"/>
		<asp:Label id="lblPulsar" style="margin-left:1em; Z-INDEX: 103;" runat="server" CssClass="Etiqueta" Width="672px">Pulse sobre el proveedor para seleccionarlo o haga una nueva búsqueda</asp:Label>
        <asp:Button runat="server" ID="btnPager" style="display:none;" />
        <div id="divContenedor" style="width:100%; height:auto">
            <asp:UpdatePanel ID="updBusqueda" runat="server" UpdateMode="Conditional" >
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="cmdBuscar" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnPager" EventName="Click" />
            </Triggers>
            <ContentTemplate >
			    <table border="0" width="98%" style="margin-left:5px;margin-right:5px;" cellspacing="0">
            	<tr>
					<td colspan="3" rowspan="2" width="90%">
                        <div id="divUsu">
                        <ig:WebHierarchicalDataGrid id="whdgUsuarios" style="z-index: 104;" runat="server"  Width="100%"
                            AutoGenerateBands="false" AutoGenerateColumns="false" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="true">
                            <Behaviors>
                                <ig:Selection RowSelectType="Single" Enabled="True" CellClickAction="Row" >
                                    <SelectionClientEvents RowSelectionChanged="whdgUsuarios_RowSelectionChanged" />
                                </ig:Selection> 
                                <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true"  AnimationEnabled="true">
                                    <FilteringClientEvents DataFiltered="AdministrarPaginador" /> 
                                </ig:Filtering> 
                                <ig:RowSelectors Enabled="false" RowNumbering="false"></ig:RowSelectors>
                                <ig:Paging Enabled="true" PagerAppearance="Top">
                                    <PagingClientEvents PageIndexChanged="whdgUsuarios_PageIndexChanged" />
                                    <PagerTemplate>
								    <div class="CabeceraBotones" style="clear:both; float:left; width:100%;"> 
									<div style="float:left; padding:3px 0px 2px 0px; margin-left:10px;">
										<div style="clear: both; float: left; margin-right: 5px;">
                                            <asp:Image ID="ImgBtnFirst" runat="server" OnClientClick="FirstPage()" style="margin-right:2px;" />
                                            <asp:Image ID="ImgBtnPrev" runat="server" OnClientClick="PrevPage()" />
                                        </div>
                                        <div style="float: left; margin-right:5px;">
                                            <asp:Label ID="lblPage" runat="server" Text="dPage" Style="margin-right: 3px;"></asp:Label>
                                            <asp:DropDownList ID="PagerPageList" runat="server" onchange="return IndexChanged()" Style="margin-right: 3px;" />
                                            <asp:Label ID="lblOF" runat="server" Text="dOf" Style="margin-right: 3px;"></asp:Label>
                                            <asp:Label ID="lblCount" runat="server" />
                                        </div>
                                        <div style="float: left;">
                                            <asp:Image ID="ImgBtnNext" runat="server" OnClientClick="NextPage()" style="margin-right:2px;" />
                                            <asp:Image ID="ImgBtnLast" runat="server" OnClientClick="LastPage()" />
                                        </div>
									</div>   
                                    </div>  
                                    </PagerTemplate>
                                </ig:Paging>
                                <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
                            </Behaviors>
                            <Columns>
                                <ig:TemplateDataField Key="SEL" VisibleIndex="0" Width="5%">
                                    <ItemTemplate>
                                        <div><asp:CheckBox ID="cbSEL" runat="server" OnClientClick="" /></div>
                                    </ItemTemplate>
					            </ig:TemplateDataField>
                            </Columns>
			            </ig:WebHierarchicalDataGrid>
                        </div>
            		</td>
				</tr>
			    </table>
            </ContentTemplate>
            </asp:UpdatePanel>            
        </div>
		<div style="width:100%; height:auto; text-align:center; margin-top:2em">
            <asp:Button runat="server" ID="cmdAceptar" Text="DAceptar" OnClientClick="cmdAceptar_Click();return false;" width="88" CssClass="boton" style="z-index: 104;"/>
        </div>		
	</form>
</body>
</html>
