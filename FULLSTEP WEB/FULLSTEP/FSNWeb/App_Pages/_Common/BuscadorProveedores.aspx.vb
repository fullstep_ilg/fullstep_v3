﻿Partial Public Class BuscadorProveedores
    Inherits FSNPage

    Dim sGMN1 As String
    Dim sGMN2 As String
    Dim sGMN3 As String
    Dim sGMN4 As String

    Dim lInstancia As Long = 0
    Dim lRol As Long = 0
    Dim sMensajeSelMat As String
    Private bArticulosNegociadosExpress As Boolean
#Region "Carga Inicial"
    ''' <summary>
    ''' Carga la pagina
    ''' Para q sea posible el multilingÃ¼ismo en los filtros y la paginacion customizada.
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Llamada desde: Sistema; Tiempo mÃ¡ximo:1seg.</remarks>
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.ScriptMgr.EnableScriptGlobalization = True
    End Sub
    ''' <summary>
    ''' Carga la pagina
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Llamada desde: Sistema; Tiempo máximo:1seg.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sBrowser As String = HttpContext.Current.Request.Browser.Browser
        Dim Version As Integer = HttpContext.Current.Request.Browser.MajorVersion
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Proveedores
        desde.Value = Request("desde") 'Parametro que se le pasa desde el WebPartPanelCalidad
        desde2904.Value = Request("desde2904") 'Parametro que se le pasa desde el jsalta
        bArticulosNegociadosExpress = Me.FSNServer.TipoAcceso.gbArticulosNegociadosPedExpress And desde2904.Value = "PedidoExpress"

        If bArticulosNegociadosExpress Then
            'Articulos negociados
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "expressNegociable", "<script>isPedidoExpressNegociable = true;</script>")
        Else
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "expressNegociable", "<script>isPedidoExpressNegociable = false;</script>")
        End If

        FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/proveedores_buscar.gif"
        CargarTextos()
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaTheme", "<script>var rutaTheme = '" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "'</script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "'</script>")
        If Not Page.IsPostBack Then
            If EstasEnPM Then
                divArticulo.Visible = False
                divlblArticulo.Visible = False
                InicializacionControles()
            End If
            With CType(wdgDatos.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero_desactivado.gif"
                .Attributes.Add("onClick", "return FirstPage();")
                .Enabled = False
            End With
            With CType(wdgDatos.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior_desactivado.gif"
                .Attributes.Add("onClick", "return PrevPage();")
                .Enabled = False
            End With
            Dim SoloUnaPagina As Boolean = True
            With CType(wdgDatos.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
                .Attributes.Add("onClick", "return NextPage();")
                .Enabled = Not SoloUnaPagina
            End With
            With CType(wdgDatos.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
                .Attributes.Add("onClick", "return LastPage();")
                .Enabled = Not SoloUnaPagina
            End With
        End If

        desde.Value = Request("desde")
        desde2904.Value = Request("desde2904")
        Rellenos.Value = If(IsNothing(Request("Rellenos")), 0, Request("Rellenos"))
        codProves.Value = Request("codProves")
        denProves.Value = Request("denProves")
        FilaGrid.Value = Request("idFilaGrid")

        EsBtPara.Value = 0
        EsBtCC.Value = 0
        EsBtCCO.Value = 0
        If Request("Ctrl") <> Nothing Then
            Select Case Request("Ctrl")
                Case "Para"
                    EsBtPara.Value = 1
                Case "CC"
                    EsBtCC.Value = 1
                Case "CCO"
                    EsBtCCO.Value = 1
            End Select
        End If
        HiddenLista.Value = ""
        If Request("Lista") <> Nothing Then
            HiddenLista.Value = Request("Lista")
        End If

        If desde.Value = "WebPart" Then
            tipoProve.Value = Request("IdControlTipoProveedor")
        End If
        If desde.Value = "BuscadorArt" Then
            chkAnyadirProvFav.Style("display") = "none"
        End If

        If EstasEnPM Then
            If Request("Rol") <> "" Then lRol = Request("Rol")
            If Request("Instancia") <> "" Then lInstancia = Request("Instancia")

            Dim sScript As String

            Dim ilGMN1 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN1
            Dim ilGMN2 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN2
            Dim ilGMN3 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN3
            Dim ilGMN4 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN4
            sScript = ""
            sScript += "var ilGMN1 = " + ilGMN1.ToString() + ";"
            sScript += "var ilGMN2 = " + ilGMN2.ToString() + ";"
            sScript += "var ilGMN3 = " + ilGMN3.ToString() + ";"
            sScript += "var ilGMN4 = " + ilGMN4.ToString() + ";"
            sScript += "var TextoNo = '" + Textos(27) + "';"

            Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & _
                 "<script language=""{0}"">{1}</script>"
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "LongitudesCodigosKey", sScript)

            Dim arrMat(4) As String
            Dim lLongCod As Integer
            Dim i As Integer
            For i = 1 To 4
                arrMat(i) = ""
            Next i

            Dim sMat As String = Request("Mat")
            Dim sMatOrig As String = ""
            If Page.IsPostBack Then
                sMat = hidMaterial.Value
                sMatOrig = hidMaterial.Value

                If sMat <> "" Then
                    Dim ArrM As String() = sMat.Split("-")

                    For i = 1 To UBound(ArrM) + 1
                        arrMat(i) = Trim(ArrM(i - 1))
                    Next i
                End If
            Else
                i = 1
                While Trim(sMat) <> ""
                    Select Case i
                        Case 1
                            lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                        Case 2
                            lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                        Case 3
                            lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                        Case 4
                            lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN4
                    End Select

                    'Llega sin guiones desde, por ejemplo NWAltaSolic, y si buscas en esta misma pantalla lo recibe con guiones
                    sMatOrig = sMatOrig + Mid(sMat, 1, lLongCod)
                    If i < 4 Then
                        sMatOrig = sMatOrig + "-"
                    End If

                    arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
                    sMat = Mid(sMat, lLongCod + 1)
                    i = i + 1
                End While
            End If

            sGMN1 = arrMat(1)
            sGMN2 = arrMat(2)
            sGMN3 = arrMat(3)
            sGMN4 = arrMat(4)

            If sGMN1 = "" Then
                sGMN1 = arrMat(1)
            End If
            If sGMN2 = "" Then
                sGMN2 = arrMat(2)
            End If
            If sGMN3 = "" Then
                sGMN3 = arrMat(3)
            End If
            If sGMN4 = "" Then
                sGMN4 = arrMat(4)
            End If

            Dim oGMN1s As FSNServer.GruposMatNivel1
            Dim oGMN1 As FSNServer.GrupoMatNivel1
            Dim oGMN2 As FSNServer.GrupoMatNivel2
            Dim oGMN3 As FSNServer.GrupoMatNivel3
            Dim sValor As String

            If sGMN4 <> "" Then
                oGMN3 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel3))
                oGMN3.GMN1Cod = sGMN1
                oGMN3.GMN2Cod = sGMN2
                oGMN3.Cod = sGMN3
                oGMN3.CargarTodosLosGruposMatDesde(1, Idioma, sGMN4, , True)
                sValor = sGMN4 & " - " & oGMN3.GruposMatNivel4.Item(sGMN4).Den
                oGMN3 = Nothing

            ElseIf sGMN3 <> "" Then
                oGMN2 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel2))
                oGMN2.GMN1Cod = sGMN1
                oGMN2.Cod = sGMN2
                oGMN2.CargarTodosLosGruposMatDesde(1, Idioma, sGMN3, , True)
                sValor = sGMN3 & " - " & oGMN2.GruposMatNivel3.Item(sGMN3).Den
                oGMN2 = Nothing

            ElseIf sGMN2 <> "" Then
                oGMN1 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel1))
                oGMN1.Cod = sGMN1
                oGMN1.CargarTodosLosGruposMatDesde(1, Idioma, sGMN2, , True)
                sValor = sGMN2 & " - " & oGMN1.GruposMatNivel2.Item(sGMN2).Den
                oGMN1 = Nothing

            ElseIf sGMN1 <> "" Then
                oGMN1s = FSNServer.Get_Object(GetType(FSNServer.GruposMatNivel1))
                oGMN1s.CargarTodosLosGruposMatDesde(1, Idioma, sGMN1, , , True)
                sValor = sGMN1 & " - " & oGMN1s.Item(sGMN1).Den
                oGMN1s = Nothing
            End If

            hidMaterial.Value = DBNullToSomething(sMatOrig)
            txtMaterial.Text = sValor
        Else
            Linea1.Visible = False
            Linea2.Visible = False
            If EstasEnQA_ObjSuelos Then
                Linea3.Visible = True
                lblMaterialQADen.Text = Request("MQADen")
                lblUNQADen.Text = Request("UNQADen")
            End If
        End If

        If desde.Value = "WebPart" Then
            Dim sScript As String

            sScript = ""
            sScript += "var textoAlert ='" + Textos(20) + "';"

            Dim IncludeScriptKeyFormat As String = ControlChars.CrLf &
                 "<script language=""{0}"">{1}</script>"
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextoAlertProveedorRepetido", sScript)
        End If

        Me.USAR_ORGCOMPRAS.Value = False
        If EstasEnPM Then
            Me.USAR_ORGCOMPRAS.Value = Acceso.gbUsar_OrgCompras
        End If

        If Page.IsPostBack AndAlso MostrarContactos Then
            DirectCast(Me.wdgDatos.Columns.FromKey("CONTACTO"), Infragistics.Web.UI.GridControls.TemplateDataField).ItemTemplate = New DropDownTemplate
        End If

        If Not Page.IsPostBack Then   'Inicialización del Grid
            Me.CreateColumns()

            Dim oDS = New DataSet
            Dim oTable As DataTable = oDS.Tables.Add("TEMP")
            oTable.Columns.Add("COD", System.Type.GetType("System.String"))
            oTable.Columns.Add("DEN", System.Type.GetType("System.String"))
            oTable.Columns.Add("NIF", System.Type.GetType("System.String"))

            Me.wdgDatos.DataSource = oDS

            Me.wdgDatos.DataBind()
        End If

        Me.VISIBILIDAD.Value = Request("Visibilidad")

        Me.IDDEPEN.Value = Request("IDDepen")

        If Not IsPostBack Then
            Session("IdOrgCompras_BuscadorProveedores") = Request("ValorOrgCompras")
        End If

        If Request("IdControl") = Nothing Then
            If Page.IsPostBack = False Then
                If Request("CIF") <> Nothing Or Request("COD") <> Nothing Or Request("DEN") <> Nothing Then
                    Me.txtCif.Text = Request("CIF")
                    Me.txtIdentificador.Text = Request("COD")
                    Me.txtDenominacion.Text = Request("DEN")
                    btnBuscar_Click(sender, e)
                End If
            End If
        Else
            Me.IDCONTROL.Value = Request("IdControl")
        End If

        Me.IDFPago.Value = IIf(IsNothing(Request("IDFPago")), "", Request("IDFPago"))
        Me.IDFPagoHidden.Value = IIf(IsNothing(Request("IDFPagoHidden")), "", Request("IDFPagoHidden"))

        Me.HiddenFieldID.Value = Request("HiddenFieldID")
        If Not (Request("IdControlHid") = Nothing) Then
            Me.IDCONTROLHID.Value = Request("IdControlHid")
        End If

        'En caso de que PARGEN_INTERNO.USAR_COMPRAS y VISIBILIDAD 
        Me.lblOrganizacionComprasReadOnly.Visible = False
        If Me.USAR_ORGCOMPRAS.Value = True And EstasEnPM Then
            Dim oOrganizacionesCompras As FSNServer.OrganizacionesCompras = FSNServer.Get_Object(GetType(FSNServer.OrganizacionesCompras))
            oOrganizacionesCompras.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil)

            Dim ugtxtOrganizacionCompras_wdg As Infragistics.Web.UI.GridControls.WebDataGrid = ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg")
            If oOrganizacionesCompras.Data.Tables(0).Rows.Count > 0 Then
                ugtxtOrganizacionCompras_wdg.DataSource = oOrganizacionesCompras.Data.Tables(0)

                ugtxtOrganizacionCompras_wdg.DataBind()

                Me.ugtxtOrganizacionCompras.ValueField = "COD"
                Me.ugtxtOrganizacionCompras.TextField = "DEN"
                If Me.VISIBILIDAD.Value = "visible" Then
                    Me.ugtxtOrganizacionCompras.Visible = True
                    Me.lblOrganizacionComprasReadOnly.Visible = False
                ElseIf Me.VISIBILIDAD.Value = "readOnly" Then
                    Me.ugtxtOrganizacionCompras.Visible = False
                    Me.lblOrganizacionComprasReadOnly.Visible = True
                ElseIf Me.VISIBILIDAD.Value = "null" And Me.IDDEPEN.Value = "oculto" Then
                    Me.ugtxtOrganizacionCompras.Visible = False
                    Me.lblOrganizacionComprasReadOnly.Visible = True
                End If
                If Not IsPostBack Then
                    Me.DENORGCOMPRAS.Value = ""

                    Session("IndexOrgCompras_BuscadorProveedores") = 0

                    If Session("IdOrgCompras_BuscadorProveedores") <> "" AndAlso Session("IdOrgCompras_BuscadorProveedores") <> "null" Then
                        For Each oRow_ As Infragistics.Web.UI.GridControls.GridRecord In ugtxtOrganizacionCompras_wdg.Rows
                            If (oRow_.Items(0).Value = Session("IdOrgCompras_BuscadorProveedores")) Then
                                ugtxtOrganizacionCompras_wdg.Behaviors.Selection.SelectedRows.Add(oRow_)
                                ugtxtOrganizacionCompras.Items(0).Text = oRow_.Items(1).Value
                                ugtxtOrganizacionCompras.Items(0).Value = oRow_.Items(0).Value

                                Me.lblOrganizacionComprasReadOnly.Text = oRow_.Items(1).Value
                                Me.DENORGCOMPRAS.Value = oRow_.Items(1).Value

                                Exit For
                            Else
                                Session("IndexOrgCompras_BuscadorProveedores") = Session("IndexOrgCompras_BuscadorProveedores") + 1
                            End If
                        Next
                    Else
                        If ugtxtOrganizacionCompras.Items.Count > 0 Then
                            Dim oRow_ As Infragistics.Web.UI.GridControls.GridRecord = ugtxtOrganizacionCompras_wdg.Rows(0)

                            ugtxtOrganizacionCompras_wdg.Behaviors.Selection.SelectedRows.Add(oRow_)
                            ugtxtOrganizacionCompras.Items(0).Text = oRow_.Items(1).Value
                            ugtxtOrganizacionCompras.Items(0).Value = oRow_.Items(0).Value
                            'Si no tiene OrgCompras y es de sólo lectura no tiene que poner ningún valor al label
                            'Seleccionar el primer valor sólo tiene sentido en este caso para un combo
                            If lblOrganizacionComprasReadOnly.Visible = False Then
                                Me.lblOrganizacionComprasReadOnly.Text = oRow_.Items(1).Value
                                Me.DENORGCOMPRAS.Value = oRow_.Items(1).Value
                                Session("IdOrgCompras_BuscadorProveedores") = oRow_.Items(0).Value
                            End If
                        End If
                    End If
                Else
                    If Me.hidOrg.Value <> "" Then
                        If Me.hidOrg.Value <> ugtxtOrganizacionCompras.CurrentValue Then
                            For Each oRow_ As Infragistics.Web.UI.GridControls.GridRecord In ugtxtOrganizacionCompras_wdg.Rows
                                If (oRow_.Items(1).Value = Me.hidOrg.Value) Then
                                    Session("IdOrgCompras_BuscadorProveedores") = oRow_.Items(0).Value
                                    Exit For
                                Else
                                    Session("IndexOrgCompras_BuscadorProveedores") = Session("IndexOrgCompras_BuscadorProveedores") + 1
                                End If
                            Next
                        End If
                    End If
                End If

                Dim sScript As String

                sScript = ""
                sScript += "var DenOrgInicial ='" & Me.DENORGCOMPRAS.Value & "';"
                sScript += "var OrgInicial ='" & Session("IndexOrgCompras_BuscadorProveedores") & "';"

                Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
                sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextoOrgInicial", sScript)

                oOrganizacionesCompras = Nothing
            End If
        Else
            Me.divLblOrg.Visible = False
            Me.divWddOrg.Visible = False

            Me.divMaterial.Style.Item("width") = "555px"
            Me.txtMaterial.Style.Item("width") = "535px"

        End If

        If Not IsPostBack Then
            btnMail.Visible = False

            Me.chkAnyadirProvFav.Checked = FSNUser.PMAnyadirProvFav

            Me.wdgDatos.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize")

            If EstasEnPM Then
                If Not (Request("CodArt") = Nothing) AndAlso Request("CodArt") <> "" Then
                    txtArticulo.Text = Request("CodArt")
                    divMaterial.Visible = False
                    divlblMaterial.Visible = False
                    divlblArticulo.Visible = True
                    divArticulo.Visible = True
                    Me.chkSoloFavoritos.Checked = False
                Else
                    Me.chkSoloFavoritos.Checked = True
                End If

                Me.btnBuscar_Click(sender, e)
                Me.chkSoloFavoritos.Checked = False
            ElseIf EstasEnQA_Mail Then
                btnMail.Visible = True

                Dim sScript As String

                sScript = ""
                sScript += "var textoAlertMail ='" + Textos(25) + "';"

                Dim IncludeScriptKeyFormat As String = ControlChars.CrLf &
                     "<script language=""{0}"">{1}</script>"
                sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextoAlertMail", sScript)

                btnBuscar_Click(sender, e)

                Me.wdgDatos.Behaviors.Selection.CellClickAction = Infragistics.Web.UI.GridControls.CellClickAction.Row
                Me.wdgDatos.Behaviors.Selection.CellSelectType = Infragistics.Web.UI.GridControls.SelectType.None
                Me.wdgDatos.Behaviors.Selection.ColumnSelectType = Infragistics.Web.UI.GridControls.SelectType.None
                Me.wdgDatos.Behaviors.Selection.RowSelectType = Infragistics.Web.UI.GridControls.SelectType.Multiple
                Me.wdgDatos.Behaviors.Selection.EnableCrossPageSelection = True
            End If
        Else 'If Request("__EVENTARGUMENT") = "," Then
            'Todas las acciones sobre el grid provocan postback y todas necesitan de databind. 
            'Existen eventos para wdgDatos_ColumnSorted y wdgDatos_DataFiltered

            CargarGrid()
        End If
        hProveedorIniNegociado.Value = Request("Valor")
        If EstasEnPM Then
            'Funcion que sirve para eliminar el material 
            If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarMaterial") Then
                Dim sScript As String = "function eliminarMaterial(event) {" & vbCrLf & _
                " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf & _
                        " return true; " & vbCrLf & _
                " } else { " & vbCrLf & _
                    " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                        " campo = document.getElementById('" & txtMaterial.ClientID & "')" & vbCrLf & _
                        " if (campo) { campo.value = ''; } " & vbCrLf & _
                        " campoHidden = document.getElementById('" & hidMaterial.ClientID & "')" & vbCrLf & _
                        " if (campoHidden) { campoHidden.value = ''; } " & vbCrLf & _
                " } " & vbCrLf & _
                " return false; " & vbCrLf & _
                " } }" & vbCrLf
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarMaterial", sScript, True)
            End If 'En caso de que Haya código de artículo se oculta el material y se muestra el label del artículo  
            
        End If


    End Sub

    ''' <summary>
    ''' Establece los textos de los controles de acuerdo con el idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load; Tiempo máximo:0</remarks> 
    Private Sub CargarTextos()
        FSNPageHeader.TituloCabecera = Textos(1)

        lblIdentificador.Text = Textos(8)
        lblDenominacion.Text = Textos(9)
        lblCif.Text = Textos(10)

        lblOrganizacionCompras.Text = Textos(15)

        lblArticulo.Text = Textos(32) & ":"
        lblMaterial.Text = Textos(17)
        chkAnyadirProvFav.Text = Textos(18)
        sMensajeSelMat = Textos(19)
        lblSoloFavoritos.Text = Textos(22)

        lblMaterialQA.Text = Textos(33) & ":"
        lblUNQA.Text = Textos(34) & ":"
        lblProvesCriticos.Text = Textos(35)

        btnBuscar.Text = Textos(7)
        btnLimpiar.Text = Textos(21)

        If EstasEnQA_Mail Then
            lblPulse.Text = Textos(28)
        Else
            lblPulse.Text = Textos(6)
        End If

        btnAceptar.Text = Textos(23)
        btnCancelar.Text = Textos(24)

        btnAceptarConfirm.Text = Textos(26)
        btnCancelConfirm.Text = Textos(27)

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeNegociadoNoAcepCambioProve", "<script>var mensajeNegociadoNoAcepCambioProve = '" & JSText(Textos(31)) & "' </script>")

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Notificaciones
        CType(wdgDatos.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPage"), Label).Text = Textos(46)
        CType(wdgDatos.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblOF"), Label).Text = Textos(48)
        'Vuelvo a cargar el modulo de proveedores
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Proveedores
    End Sub

    ''' <summary>
    ''' Inicializa los componentes y funciones javascript de los elementos
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0seg.</remarks>
    Private Sub InicializacionControles()
        imgSoloFavoritos.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/favoritos_si.gif"
        imgMaterialLupa.Attributes.Add("onClick", "javascript:window.open('" & ConfigurationManager.AppSettings("rutaPM") & "_common/materiales.aspx?desde=WebPart&IdControl=" & txtMaterial.ID & "&MaterialGS_BBDD=" & hidMaterial.ID & "','_blank', 'width=550,height=550,status=yes,resizable=no,top=100,left=200'); return false;")
        txtMaterial.Attributes.Add("onkeydown", "javascript:return eliminarMaterial(event)")
    End Sub
#End Region
#Region "Propertys"
    ''' <summary>
    ''' Determina si se buscan proveedores para una pantalla de QA
    ''' </summary>
    ''' <returns>Si es pantalla de QA o no</returns>
    ''' <remarks>Llamada desde: CreateColumns; Tiempo máximo:0seg.</remarks>
    Private ReadOnly Property EstasEnQA() As Boolean
        Get
            Return (Request.QueryString("PM") = "false") AndAlso (Request.QueryString("desde") <> "Mail")
        End Get
    End Property

    ''' <summary>
    ''' Determina si se buscan proveedores para una pantalla de PM
    ''' </summary>
    ''' <returns>Si es pantalla de PM o no</returns>
    ''' <remarks>Llamada desde: CreateColumns       wdgDatos_InitializeRow      btnBuscar_Click       Page_Load; Tiempo máximo:0seg.</remarks>
    Private ReadOnly Property EstasEnPM() As Boolean
        Get
            Dim bContrato As Boolean = False
            If Request("Contr") <> Nothing Then bContrato = True

            Return (Request.QueryString("PM") = "true") AndAlso Not bContrato AndAlso (Request.QueryString("desde") <> "VisorSol")
        End Get
    End Property

    ''' <summary>
    ''' Determina si se buscan contactos para la pantalla de mail de los visores QA
    ''' </summary>
    ''' <returns>Si es pantalla de mail o no</returns>
    ''' <remarks>Llamada desde: CreateColumns       btnBuscar_Click       Page_Load; Tiempo mÃ¡ximo:0seg.</remarks>
    Private ReadOnly Property EstasEnVisorSolicitudes() As Boolean
        Get
            Return (Request.QueryString("desde") = "VisorSol")
        End Get
    End Property

    ''' <summary>
    ''' Determina si hay que mostrar o no la combo con los contactos
    ''' </summary>
    ''' <returns>Si es hay que mostrar o no la combo con los contactos</returns>
    Private ReadOnly Property MostrarContactos() As Boolean
        Get
            Return (Request.QueryString("ComboContactos") = "true")
        End Get
    End Property

    ''' <summary>
    ''' Determina si se buscan contactos para la pantalla de mail de los visores QA
    ''' </summary>
    ''' <returns>Si es pantalla de mail o no</returns>
    ''' <remarks>Llamada desde: CreateColumns       btnBuscar_Click       Page_Load; Tiempo máximo:0seg.</remarks>
    Private ReadOnly Property EstasEnQA_Mail() As Boolean
        Get
            Return (Request.QueryString("PM") = "false") AndAlso (Request.QueryString("desde") = "Mail")
        End Get
    End Property

    ''' <summary>Determina si se buscan proveedores desde la página de objetivos y suelos</summary>
    ''' <returns>Si es la página o no</returns>
    ''' <remarks>Llamada desde: Page_load</remarks>
    Private ReadOnly Property EstasEnQA_ObjSuelos As Boolean
        Get
            Return (Request.QueryString("PM") = "false") AndAlso (Request.QueryString("desde") = "ObjSuelos")
        End Get
    End Property

    ''' <summary>
    ''' Determina si se buscan proveedores para una pantalla de Contratos
    ''' </summary>
    ''' <returns>Si es pantalla de Contratos o no</returns>
    ''' <remarks>Llamada desde: CreateColumns       wdgDatos_InitializeRow      btnBuscar_Click       Page_Load; Tiempo máximo:0seg.</remarks>
    Private ReadOnly Property EstasEnContrato() As Boolean
        Get
            Return (Request("Contr") <> Nothing)
        End Get
    End Property

    ''' <summary>
    ''' Determina si se buscan proveedores para una pantalla de notificaciones
    ''' </summary>
    ''' <returns>Si es pantalla de notificaciones o no</returns>
    ''' <remarks>Llamada desde: CreateColumns       wdgDatos_InitializeRow      btnBuscar_Click       Page_Load; Tiempo máximo:0seg.</remarks>
    Private ReadOnly Property EstasEnNotificaciones() As Boolean
        Get
            Return (Request("Notificacion") <> Nothing)
        End Get
    End Property

    ''' <summary>
    ''' Determina si se buscan proveedores para una pantalla de Pedido Express/negociado
    ''' </summary>
    ''' <returns>Si es pantalla de notificaciones o no</returns>
    ''' <remarks>Llamada desde: CreateColumns       wdgDatos_InitializeRow      btnBuscar_Click       Page_Load; Tiempo máximo:0seg.</remarks>
    Private ReadOnly Property EstasEnPedidoNegociadoOExpress() As Boolean
        Get
            Return (Request("desde2904") <> Nothing)
        End Get
    End Property
#End Region
#Region "wdgDatos"

    ''' <summary>
    ''' Crea las columnas en función de q se este buscando
    ''' </summary>
    ''' <remarks>Llamada desde: Page_load; Tiempo máximo:0seg.</remarks>
    Private Sub CreateColumns()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Proveedores

        With Me.wdgDatos

            If EstasEnQA Then
                Me.AddColumn("COD", Textos(8), 20)
                Me.AddColumn("DEN", Textos(9), 55)
                Me.AddColumn("NIF", Textos(10), 25)

                Me.AddColumn("ID_CONTACTO", "", 0, True)
                Me.AddColumn("NOM_CONTACTO", "", 0, True)
                Me.AddColumn("REAL", "", 0, True)
                Me.AddColumn("BAJA", "", 0, True)
                Me.AddColumn("PROVISIONAL", "", 0, True)

            ElseIf EstasEnQA_Mail Then

                Me.AddColumn("COD", Textos(8), 10)
                Me.AddColumn("DEN", Textos(9), 40)
                Me.AddColumn("NIF", Textos(10), 10)
                Me.AddColumn("ID_CONTACTO", "", 0, True)
                Me.AddColumn("NOM_CONTACTO", Textos(11), 40)
                Me.AddColumn("EMAIL", "", 0, True)
                Me.AddColumn("REAL", "", 0, True)
                Me.AddColumn("BAJA", "", 0, True)
                Me.AddColumn("PROVISIONAL", "", 0, True)

            ElseIf EstasEnPedidoNegociadoOExpress Then

                Me.AddColumn("FAVORITO", "", 5)
                Me.AddColumn("COD", Textos(8), 20)
                Me.AddColumn("DEN", Textos(9), IIf(MostrarContactos, 30, 50))
                Me.AddColumn("NIF", Textos(10), IIf(MostrarContactos, 10, 20))
                If MostrarContactos Then
                    Me.AddColumn("CONTACTO", Textos(11), 30, , True)
                End If
                Me.AddColumn("PAG", "", 0, True)
                Me.AddColumn("DENPAG", "", 0, True)

            ElseIf EstasEnPM Then

                Me.AddColumn("FAVORITO", "", 5)
                Me.AddColumn("COD", Textos(8), 20)
                Me.AddColumn("DEN", Textos(9), IIf(MostrarContactos, 30, 50))
                Me.AddColumn("NIF", Textos(10), IIf(MostrarContactos, 10, 20))
                If MostrarContactos Then
                    Me.AddColumn("CONTACTO", Textos(11), 30, , True)
                End If

            Else 'contrato / notificaciones

                Me.AddColumn("COD", Textos(8), 20)
                Me.AddColumn("DEN", Textos(9), 55)
                Me.AddColumn("NIF", Textos(10), 25)

            End If
        End With
    End Sub

    ''' <summary>
    ''' Crea una columna
    ''' </summary>
    ''' <param name="fieldName">Key de la columna</param>
    ''' <param name="headerText">Texto de cabecera de la columna</param>
    ''' <param name="Width">Ancho de la columna</param>
    ''' <param name="Invisible">Visibilidad de la columna</param>
    ''' <param name="Template">Si es una columna template o bound</param>
    ''' <remarks>Llamada desde: CreateColumns; Tiempo máximo:0seg.</remarks>
    Private Sub AddColumn(ByVal fieldName As String, ByVal headerText As String, ByVal Width As Integer, Optional ByVal Invisible As Boolean = False, Optional ByVal Template As Boolean = False)
        If Template Then
            Dim field As New Infragistics.Web.UI.GridControls.TemplateDataField

            field.ItemTemplate = New DropDownTemplate

            field.Key = fieldName
            field.Header.Text = headerText
            field.Width = Unit.Percentage(Width)

            Me.wdgDatos.Columns.Add(field)

            Dim fieldOrder As New Infragistics.Web.UI.GridControls.SortingColumnSetting
            fieldOrder.ColumnKey = fieldName
            Me.wdgDatos.Behaviors.Sorting.ColumnSettings.Add(fieldOrder)

            Me.wdgDatos.Behaviors.Sorting.ColumnSettings.Item(fieldName).Sortable = False

        Else
            Dim field As New Infragistics.Web.UI.GridControls.BoundDataField(True)

            field.Key = fieldName
            field.DataFieldName = fieldName
            field.Header.Text = headerText
            field.Width = Unit.Percentage(Width)
            field.Hidden = Invisible

            field.CssClass = "ListItemLink SinSalto"

            Me.wdgDatos.Columns.Add(field)
        End If

        If Template OrElse (Not Invisible) Then
            Dim fieldSetting As New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
            fieldSetting.ColumnKey = fieldName
            Me.wdgDatos.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)

            Me.wdgDatos.Behaviors.Filtering.ColumnSettings.Item(fieldName).BeforeFilterAppliedAltText = Textos(29)
        End If
    End Sub

    ''' <summary>
    ''' Tras la carga del grid se "sincroniza" con el Custom Pager
    ''' </summary>
    ''' <param name="sender">control</param>
    ''' <param name="e">evento de sistema</param>  
    ''' <remarks>Llamada desde: sistema; Tiempo mÃ¡ximo:0 </remarks>
    Private Sub wdgDatos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles wdgDatos.DataBound
        Dim pagerList As DropDownList = DirectCast(wdgDatos.Behaviors.Paging.PagerTemplateContainerTop.FindControl("PagerPageList"), DropDownList)
        pagerList.Items.Clear()
        For i As Integer = 1 To wdgDatos.Behaviors.Paging.PageCount
            pagerList.Items.Add(i.ToString())
        Next
        CType(wdgDatos.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblCount"), Label).Text = wdgDatos.Behaviors.Paging.PageCount
        Dim SoloUnaPagina As Boolean = (wdgDatos.Behaviors.Paging.PageCount = 1)
        With CType(wdgDatos.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
            .Enabled = Not SoloUnaPagina
        End With
        With CType(wdgDatos.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
            .Enabled = Not SoloUnaPagina
        End With

        'Caso de Mail. Mantener visualmente la seleccion
        If EstasEnQA_Mail Then
            Me.wdgDatos.Behaviors.Selection.SelectedRows.Clear()
        End If
    End Sub

    ''' <summary>
    ''' Inicializa una linea del grid poniendo los iconos
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema; Tiempo máximo:0,1seg.</remarks>
    Private Sub wdgDatos_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles wdgDatos.InitializeRow
        If EstasEnPM Then
            If e.Row.Items.Item(0).Value = Textos(27) Then
                e.Row.Items.Item(0).Text = "<img src='" & ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/favoritos_no.gif' style='text-align:center;'/>"
            Else
                e.Row.Items.Item(0).Text = "<img src='" & ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/favoritos_si.gif' style='text-align:center;'/>"
            End If

            If MostrarContactos Then
                Dim cboContacto As Infragistics.Web.UI.ListControls.WebDropDown = CType(e.Row.Items.Item(sender.Columns.FromKey("CONTACTO").Index).FindControl("cboContacto"), Infragistics.Web.UI.ListControls.WebDropDown)
                If Not cboContacto Is Nothing Then
                    Dim ProveedorCod As String = e.Row.Items.Item(sender.Columns.FromKey("COD").Index).Value
                    Dim dtContactos As DataTable

                    If HttpContext.Current.Cache("Contactos_" & FSNUser.Cod & "_" & ProveedorCod) Is Nothing Then
                        Dim oProveedor As FSNServer.Proveedor

                        oProveedor = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
                        oProveedor.Cod = ProveedorCod
                        oProveedor.CargarContactos(True)
                        dtContactos = oProveedor.Contactos.Tables(0)
                        If dtContactos IsNot Nothing Then Me.InsertarEnCache("Contactos_" & FSNUser.Cod & "_" & ProveedorCod, dtContactos)
                    Else
                        dtContactos = HttpContext.Current.Cache("Contactos_" & FSNUser.Cod & "_" & ProveedorCod)
                    End If

                    cboContacto.ValueField = "ID"
                    cboContacto.TextField = "CONTACTO_CON_EMAIL"

                    cboContacto.DataSource = dtContactos
                    cboContacto.DataBind()
                End If
            End If

        ElseIf Not EstasEnContrato AndAlso Not EstasEnNotificaciones AndAlso Not EstasEnVisorSolicitudes Then 'estas en qa Ã³ qa mail
            Dim Potencial As String = e.Row.Items.Item(sender.Columns.FromKey("REAL").Index).Value
            Dim Baja As String = e.Row.Items.Item(sender.Columns.FromKey("BAJA").Index).Value

            Dim sFile As String = "tipocalidad_" & Potencial & "_" & Baja
            If Acceso.gbPotAValProvisionalSelProve AndAlso Potencial = "2" AndAlso e.Row.Items.Item(sender.Columns.FromKey("PROVISIONAL").Index).Value = 1 Then sFile &= "_" & e.Row.Items.Item(sender.Columns.FromKey("PROVISIONAL").Index).Value.ToString

            Dim sTexto As String = "&nbsp;" & e.Row.Items.Item(sender.Columns.FromKey("DEN").Index).Value
            Dim Index As Integer = sender.Columns.FromKey("DEN").Index
            Dim UrlImage As String = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/" & sFile & ".gif"
            e.Row.Items.Item(Index).Text = "<img src='" & UrlImage & "' style='text-align:center;'/>"
            e.Row.Items.Item(Index).Text = e.Row.Items.Item(Index).Text & sTexto
        End If

        e.Row.Items.Item(sender.Columns.FromKey("COD").Index).Tooltip = e.Row.Items.Item(sender.Columns.FromKey("COD").Index).Text
        e.Row.Items.Item(sender.Columns.FromKey("DEN").Index).Tooltip = e.Row.Items.Item(sender.Columns.FromKey("DEN").Index).Text
        e.Row.Items.Item(sender.Columns.FromKey("NIF").Index).Tooltip = e.Row.Items.Item(sender.Columns.FromKey("NIF").Index).Text
        If EstasEnQA_Mail Then
            e.Row.Items.Item(sender.Columns.FromKey("NOM_CONTACTO").Index).Tooltip = e.Row.Items.Item(sender.Columns.FromKey("NOM_CONTACTO").Index).Text
        End If
    End Sub
#End Region
#Region "btnBuscar"

    ''' <summary>
    ''' Buscar proveedores segun los filtros dados
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>      
    ''' <remarks>Llamada desde: sistema; Tiempo máximo: 0,1</remarks>
    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        With wdgDatos
            .Rows.Clear()
            .Behaviors.Paging.PageIndex = 0
            .DataSource = Proveedores(True)
            .DataBind()
        End With
    End Sub

    ''' <summary>
    ''' Carga la grid con los proveedores
    ''' </summary>
    ''' <remarks>Llamada desde: page_load; Tiempo mÃƒÂ¡ximo:0,3</remarks>
    Private Sub CargarGrid()
        wdgDatos.DataSource = Proveedores(False)
        wdgDatos.DataBind()
    End Sub

    ''' <summary>Devuelve los datos de los proveedores resultado de la búsqueda</summary>
    ''' <param name="bRecargar">indica si hay que recargar de base de datos</param>
    ''' <returns>dataset con los datos de los proveedores</returns>
    Private Function Proveedores(ByVal bRecargar As Boolean) As DataSet
        Dim dsDatos As DataSet

        If bRecargar Or Cache("oProvesBusqPrv_" & FSNUser.Cod) Is Nothing Then
            Dim sCodArt As String
            Dim oProves As FSNServer.Proveedores
            oProves = FSNServer.Get_Object(GetType(FSNServer.Proveedores))
            sCodArt = Request("CodArt")
            If Request("Certif") = 1 Then
                oProves.Carga_Proveedores_QA(sCod:=txtIdentificador.Text, sDen:=txtDenominacion.Text, sNif:=txtCif.Text, sUser:=FSNUser.Cod, bUsaLike:=True,
                                             bDesdeProveedores:=True, RestricProvMat:=FSNUser.QARestProvMaterial, RestricProvEqp:=FSNUser.QARestProvEquipo, RestricProvCon:=FSNUser.QARestProvContacto)
            ElseIf Request("NoConf") = 1 Then
                oProves.Carga_Proveedores_QA(sCod:=txtIdentificador.Text, sDen:=txtDenominacion.Text, sNif:=txtCif.Text, sUser:=FSNUser.Cod, bUsaLike:=True,
                                            bDesdeProveedores:=True, bFuerzaBaja:=True, RestricProvMat:=FSNUser.QARestProvMaterial, RestricProvEqp:=FSNUser.QARestProvEquipo, RestricProvCon:=FSNUser.QARestProvContacto)
            ElseIf desde.Value = "VisorCert" Then
                oProves.Carga_Proveedores_QA(sCod:=txtIdentificador.Text, sDen:=txtDenominacion.Text, sNif:=txtCif.Text, sUser:=FSNUser.Cod, bUsaLike:=True,
                                            bDesdeProveedores:=True, bFuerzaBaja:=CBool(Request("Baja")), iTipo:=DBNullToInteger(Request("Tipo")),
                                            RestricProvMat:=FSNUser.QARestProvMaterial, RestricProvEqp:=FSNUser.QARestProvEquipo, RestricProvCon:=FSNUser.QARestProvContacto)
            ElseIf EstasEnQA_Mail Then
                oProves.CargarEmailProv(txtIdentificador.Text, txtDenominacion.Text, txtCif.Text,, True) 'En el original iExacto era true -> no usaba like para el codigo

                wdgDatos.Behaviors.RowSelectors.Enabled = True
                wdgDatos.Behaviors.RowSelectors.RowNumbering = False
            ElseIf EstasEnQA_ObjSuelos Then
                Dim iIdMatQA As Integer = CType(Request("MQAID"), Integer)
                Dim iIdUNQA As Integer = CType(Request("UNQAID"), Integer)

                oProves.CargarProvObjSuelos(FSNUser.Cod, iIdMatQA, iIdUNQA, txtIdentificador.Text, txtDenominacion.Text, txtCif.Text,, chkProveCriticos.Checked)
            ElseIf EstasEnQA Then 'En un certif o no conf un campo entry Proveedor
                oProves.Carga_Proveedores_QA(sCod:=txtIdentificador.Text, sDen:=txtDenominacion.Text, sNif:=txtCif.Text, sUser:=FSNUser.Cod, bUsaLike:=True, bDesdeProveedores:=True,
                                             RestricProvMat:=FSNUser.QARestProvMaterial, RestricProvEqp:=FSNUser.QARestProvEquipo, RestricProvCon:=FSNUser.QARestProvContacto)
            ElseIf EstasEnPM Then
                Dim gParametrosGenerales As TiposDeDatos.ParametrosGenerales
                Dim sUon1 As String = ""
                Dim sUon2 As String = ""
                Dim sUon3 As String = ""
                Dim sUon4 As String = ""
                If gParametrosGenerales.gbAccesoFSSM Then
                    sUon1 = Request("Uon1")
                    sUon2 = Request("Uon2")
                    sUon3 = Request("Uon3")
                    sUon4 = Request("Uon4")
                End If

                Dim CargaFpago As Boolean = EstasEnPedidoNegociadoOExpress
                CargaFpago = CargaFpago AndAlso ((Me.IDFPago.Value <> "") OrElse (Me.IDFPagoHidden.Value <> ""))

                'En caso de que PARGEN_INTERNO.USAR_COMPRAS
                If Me.USAR_ORGCOMPRAS.Value Then
                    Dim sCodOrgCompras As String = Session("IdOrgCompras_BuscadorProveedores")

                    If sCodOrgCompras <> Nothing Then
                        oProves.LoadData(Idioma, txtIdentificador.Text, txtDenominacion.Text, txtCif.Text, FSNUser.Cod, sCodOrgCompras, sGMN1, sGMN2, sGMN3, sGMN4, chkSoloFavoritos.Checked, lRol, IIf(MostrarContactos, True, False), PMRestricProveMat:=(FSNUser.PMRestricProveMat Or FSNUser.FSPMRestrMatUsu), CargaFormPago:=CargaFpago, UsarOrgCompras:=True, AccesoFSSM:=gParametrosGenerales.gbAccesoFSSM, sUon1:=sUon1, sUon2:=sUon2, sUon3:=sUon3, sUon4:=sUon4, sCodArt:=sCodArt)
                    Else
                        oProves.LoadData(Idioma, txtIdentificador.Text, txtDenominacion.Text, txtCif.Text, FSNUser.Cod, Nothing, sGMN1, sGMN2, sGMN3, sGMN4, chkSoloFavoritos.Checked, lRol, IIf(MostrarContactos, True, False), PMRestricProveMat:=(FSNUser.PMRestricProveMat Or FSNUser.FSPMRestrMatUsu), CargaFormPago:=CargaFpago, UsarOrgCompras:=True, AccesoFSSM:=gParametrosGenerales.gbAccesoFSSM, sUon1:=sUon1, sUon2:=sUon2, sUon3:=sUon3, sUon4:=sUon4, sCodArt:=sCodArt)
                    End If
                Else
                    oProves.LoadData(Idioma, txtIdentificador.Text, txtDenominacion.Text, txtCif.Text, FSNUser.Cod, , sGMN1, sGMN2, sGMN3, sGMN4, chkSoloFavoritos.Checked, lRol, IIf(MostrarContactos, True, False), PMRestricProveMat:=(FSNUser.PMRestricProveMat Or FSNUser.FSPMRestrMatUsu), CargaFormPago:=CargaFpago, UsarOrgCompras:=False, AccesoFSSM:=gParametrosGenerales.gbAccesoFSSM, sUon1:=sUon1, sUon2:=sUon2, sUon3:=sUon3, sUon4:=sUon4, sCodArt:=sCodArt)
                End If
            Else 'webpart o contrato o  Notificaciones
                oProves.LoadData(Idioma, txtIdentificador.Text, txtDenominacion.Text, txtCif.Text, FSNUser.Cod, PMRestricProveMat:=(FSNUser.PMRestricProveMat Or FSNUser.FSPMRestrMatUsu), sCodArt:=sCodArt)
            End If
            dsDatos = oProves.Data

            Me.InsertarEnCache("oProvesBusqPrv_" & FSNUser.Cod, oProves.Data)
        Else
            dsDatos = CType(Cache("oProvesBusqPrv_" & FSNUser.Cod), DataSet)
        End If

        Return dsDatos
    End Function

    ''' <summary>
    ''' Carga la grid con los proveedores tras pulsar en un icono de favorito
    ''' </summary>
    ''' <remarks>Llamada desde: page_load; Tiempo mÃƒÆ’Ã‚Â¡ximo:0,3</remarks>
    Private Sub CambioyCargarGrid(ByVal ProveCod As String)
        Dim oProvesData As DataSet = CType(Cache("oProvesBusqPrv_" & FSNUser.Cod), DataSet)
        Dim Tabla As DataTable = oProvesData.Tables(0)
        Dim Row As DataRow = Tabla.Select("COD='" & ProveCod & "'")(0)

        If Row("FAVORITO") = Textos(27) Then
            Row("FAVORITO") = Textos(26)
        Else
            Row("FAVORITO") = Textos(27)
        End If

        Me.InsertarEnCache("oProvesBusqPrv_" & FSNUser.Cod, oProvesData)
        wdgDatos.DataSource = CType(Cache("oProvesBusqPrv_" & FSNUser.Cod), DataSet)
        wdgDatos.DataBind()

    End Sub
#End Region
#Region "Solo PM"
    ''' <summary>
    ''' Añadir a Proveedores favoritos lo seleccionado
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>      
    ''' <remarks>Llamada desde: sistema; Tiempo máximo: 0,1</remarks>
    Private Sub chkAnyadirProvFav_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAnyadirProvFav.CheckedChanged
        FSNUser.Actualizar_AnyadirProvFav(Me.chkAnyadirProvFav.Checked)
        FSNUser.PMAnyadirProvFav = Me.chkAnyadirProvFav.Checked
    End Sub

    ''' <summary>
    ''' Al dar a btn buscar esto se ejecuta antes y carga con q organización estamos trabajando.
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>      
    ''' <remarks>Llamada desde: sistema; Tiempo máximo: 0,1</remarks>
    Public Sub ugtxtOrganizacionCompras_SelectedRowChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.SelectedRowEventArgs)
        Session("IdOrgCompras_BuscadorProveedores") = DirectCast(ugtxtOrganizacionCompras.Controls(0).Controls(0).Controls(1), Infragistics.Web.UI.GridControls.WebDataGrid).Behaviors.Selection.SelectedRows(0).Items(0).Value
    End Sub
#End Region
#Region "Template columna DROPDOWN"
    Private Class DropDownTemplate
        Implements ITemplate

        ''' <summary>
        ''' Rellena un Item del webdatagrid 
        ''' </summary>
        ''' <param name="container">Item del webdatagrid a rellenar</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Public Sub InstantiateIn(ByVal container As System.Web.UI.Control) Implements System.Web.UI.ITemplate.InstantiateIn
            Dim wdd As New Infragistics.Web.UI.ListControls.WebDropDown

            wdd.MultipleSelectionType = Infragistics.Web.UI.ListControls.DropDownMultipleSelectionType.Checkbox
            wdd.Width = Unit.Percentage(100)
            wdd.EnableMultipleSelection = True
            wdd.DisplayMode = Infragistics.Web.UI.ListControls.DropDownDisplayMode.DropDownList
            wdd.EnableClosingDropDownOnSelect = False
            wdd.DropDownContainerHeight = Unit.Pixel(80)
            wdd.EnableRenderingAnchors = False
            wdd.EnableViewState = True
            wdd.ID = "cboContacto"

            container.Controls.Add(wdd)
        End Sub
    End Class
#End Region
End Class
