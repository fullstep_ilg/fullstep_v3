﻿Public Partial Class UltimasAdjudicaciones
    Inherits FSNPage

    ''' <summary>
    ''' Carga en una grid la información de las últimas adjudicaciones de un artículo determinado.
    ''' </summary>
    ''' <remarks>LLamada desde: Al cargarse la página; Tiempo máximo: 0,2 sg</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sCod As String = Request("sCod")
        Dim sInstanciaMoneda As String = Request("sInstanciaMoneda")
        Dim Proveedor As String = Request("CodProveedor")
        Dim OrgCompra As String = Request("CodOrgCompra")
        Dim gParametrosGenerales As TiposDeDatos.ParametrosGenerales

        IDFPago.Value = IIf(IsNothing(Request("IDFPago")), "", Request("IDFPago"))
        IDFPagoHidden.Value = IIf(IsNothing(Request("IDFPagoHidden")), "", Request("IDFPagoHidden"))

        Dim bCargaFPago As Boolean = False
        Dim sUon1 As String = ""
        Dim sUon2 As String = ""
        Dim sUon3 As String = ""
        Dim sUon4 As String = ""
        If (IDFPago.Value <> "") OrElse (IDFPagoHidden.Value <> "") Then
            bCargaFPago = True

            If gParametrosGenerales.gbAccesoFSSM Then
                sUon1 = Request("Uon1")
                sUon2 = Request("Uon2")
                sUon3 = Request("Uon3")
                sUon4 = Request("Uon4")
            End If
        End If

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.UltimasAdjudicaciones

        lbltitulo.Text = Textos(1) 'Seleccione la adjudicación

        Dim oArticulos As FSNServer.Articulos
        oArticulos = FSNServer.Get_Object(GetType(FSNServer.Articulos))
        wdgUltimasAdjudicaciones.Rows.Clear()
        wdgUltimasAdjudicaciones.Columns.Clear()
        wdgUltimasAdjudicaciones.DataSource = oArticulos.CargarUltimasAdjudicaciones(sCod, FSNUser.PrecisionFmt, sInstanciaMoneda, Idioma _
            , Proveedor, OrgCompra, bCargaFPago, Acceso.gbUsar_OrgCompras, gParametrosGenerales.gbAccesoFSSM, sUon1, sUon2, sUon3, sUon4)
        CrearColumnas()
        wdgUltimasAdjudicaciones.DataBind()
        ConfigurarGrid()

        With wdgUltimasAdjudicaciones
            .Columns("CODPROVE").Header.Text = Textos(3)   '"Proveedor"
            .Columns("CODPROVE").Width = Unit.Pixel(100)
            .Columns("DESCR").Header.Text = Textos(4)      '"Denominación proveedor"
            .Columns("DESCR").Width = Unit.Pixel(250)
            .Columns("PREC").Header.Text = Textos(5)       '"Precio"
            .Columns("PREC").Width = Unit.Pixel(50)
            .Columns("MONEDA").Header.Text = Textos(9)       '"Moneda"
            .Columns("MONEDA").Width = Unit.Pixel(50)
            .Columns("CANT").Header.Text = Textos(6)       '"Cantidad"
            .Columns("CANT").Width = Unit.Pixel(50)
            .Columns("UNI").Header.Text = Textos(2) 'Unidad
            .Columns("UNI").Width = Unit.Pixel(50)
            .Columns("PORCEN").Header.Text = FSNUser.NumberFormat.PercentSymbol '"%"
            .Columns("PORCEN").Width = Unit.Pixel(70)
            .Columns("FECINI").Header.Text = Textos(7)  '"F.inicio"
            .Columns("FECINI").Width = Unit.Pixel(75)
            .Columns("FECFIN").Header.Text = Textos(8)     '"F.fin"
            .Columns("FECFIN").Width = Unit.Pixel(75)
            .Columns("PROCESO").Header.Text = Textos(10) 'Proceso
            .Columns("PROCESO").Width = Unit.Pixel(115)
        End With
    End Sub
    ''' <summary>
    ''' Formatea el precio de cada fila al formato del usuario y pone % al valor de la columna porcentaje
    ''' </summary>
    ''' <remarks>LLamada desde: Al cargarse cada fila de la grid; Tiempo máximo: 0 sg</remarks>
    Private Sub wdgUltimasAdjudicaciones_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles wdgUltimasAdjudicaciones.InitializeRow
        e.Row.Items.FindItemByKey("PREC").Text = Replace(e.Row.Items.FindItemByKey("PREC").Value, ".", ",")
        e.Row.Items.FindItemByKey("PREC").Text = FSNLibrary.FormatNumber(e.Row.Items.FindItemByKey("PREC").Text, FSNUser.NumberFormat)
        e.Row.Items.FindItemByKey("PORCEN").Text = e.Row.Items.FindItemByKey("PORCEN").Value & "%"
        e.Row.Items.FindItemByKey("PROCESO").Tooltip = e.Row.Items.FindItemByKey("PROCESO").Value
        e.Row.Items.FindItemByKey("MONEDA").Tooltip = e.Row.Items.FindItemByKey("MONDEN").Value
        e.Row.Items.FindItemByKey("UNI").Tooltip = e.Row.Items.FindItemByKey("UNIDADES").Value
    End Sub
    Private Sub CrearColumnas()
        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
        Dim nombre As String

        For i As Integer = 0 To wdgUltimasAdjudicaciones.DataSource.Tables(0).Columns.Count - 1
            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            nombre = wdgUltimasAdjudicaciones.DataSource.Tables(0).Columns(i).ToString
            With campoGrid
                .Key = nombre
                .DataFieldName = nombre
                .Header.Text = nombre
                .Header.CssClass = "headerNoWrap"
                .CssClass = "SinSalto"
            End With
            wdgUltimasAdjudicaciones.Columns.Add(campoGrid)
        Next
    End Sub
    ''' <summary>
    ''' Pone el formato a las columnas de fechas y oculta la columna del numero de decimales que permite la unidad
    ''' </summary>
    ''' <remarks>LLamada desde: Al cargarse la grid. Tiempo máximo: 0 sg</remarks>
    Private Sub ConfigurarGrid()
        'formatos:
        wdgUltimasAdjudicaciones.Columns("FECINI").FormatValue(FSNUser.DateFormat.ShortDatePattern)
        wdgUltimasAdjudicaciones.Columns("FECFIN").FormatValue(FSNUser.DateFormat.ShortDatePattern)
        'columnas ocultas
        wdgUltimasAdjudicaciones.Columns("NUMDEC").Hidden = True
        wdgUltimasAdjudicaciones.Columns("UNIDADES").Hidden = True
        wdgUltimasAdjudicaciones.Columns("MONDEN").Hidden = True

        If (Me.IDFPago.Value <> "") OrElse (Me.IDFPagoHidden.Value <> "") Then
            wdgUltimasAdjudicaciones.Columns("PAG").Hidden = True
            wdgUltimasAdjudicaciones.Columns("DENPAG").Hidden = True
        End If
    End Sub
End Class