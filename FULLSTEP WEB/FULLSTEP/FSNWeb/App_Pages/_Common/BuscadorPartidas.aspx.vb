﻿Public Class BuscadorPartidas
    Inherits FSNPage

    Private sPartida As String
    Private sCentroCoste As String
    Private sCentroCosteDen As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sCentroCoste = Request("CentroCoste")
        sCentroCosteDen = Request("CentroCosteDen")
        Partidas.CentroCoste = sCentroCoste
        Partidas.CentroCosteDen = sCentroCosteDen
        Partidas.urlImagenBuscarCentro = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/lupa.gif"
        Partidas.urlImagenCentroCosteSmall = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/centrocoste_small.JPG"
        Partidas.urlImagenCentroCoste = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/centrocoste.JPG"
        Partidas.urlImagenCandado = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/candado.gif"

        Me.Partidas.Culture = New System.Globalization.CultureInfo(Me.FSNUser.RefCultural)
        idControlPartida.Value = Request("idHidPartida")
        idControlPartidaDen.Value = Request("idTxtPartida")
        idControlCentroCoste.Value = Request("idTxtCentroCoste")
        idControlCentroCosteDen.Value = Request("idHidCentroCoste")

        sPartida = Request("Partida")
        If Not IsPostBack Then
            CargarPartidas()
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.Partidas
            Partidas.Textos = TextosModuloCompleto
        End If
    End Sub

    ''' <summary>
    ''' Pasa al userControl de partidas los datos de las partidas
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarPartidas()
        Dim oPres5 As FSNServer.PRES5

        oPres5 = FSNServer.Get_Object(GetType(FSNServer.PRES5))
        Partidas.DatosTreeView = oPres5.Partidas_LoadDataEnIM(sPartida, FSNUser.Cod, Me.Idioma, Partidas.Plurianual, Partidas.CentroCoste, Partidas.NoVigentes, Partidas.FechaInicio, Partidas.FechaHasta)

    End Sub

    Private Sub BuscarPartidas() Handles Partidas.eventBuscarPartidas
        CargarPartidas()
    End Sub

End Class