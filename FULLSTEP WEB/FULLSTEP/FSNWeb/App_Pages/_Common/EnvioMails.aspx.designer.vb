﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class EnvioMails
    
    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm
    
    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager
    
    '''<summary>
    '''imgCabecera control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgCabecera As Global.System.Web.UI.HtmlControls.HtmlImage
    
    '''<summary>
    '''lblCabecera control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCabecera As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''lnkBotonMail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkBotonMail As Global.System.Web.UI.HtmlControls.HtmlAnchor
    
    '''<summary>
    '''Instancia control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Instancia As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''Prove control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Prove As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''isHTML control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents isHTML As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''TipoEmail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TipoEmail As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''EntidadEmail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents EntidadEmail As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''lblPara control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPara As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtPara control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPara As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''lblAsunto control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAsunto As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtAsunto control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAsunto As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtEmailHtml control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEmailHtml As Global.AjaxControlToolkit.HTMLEditor.Editor
End Class
