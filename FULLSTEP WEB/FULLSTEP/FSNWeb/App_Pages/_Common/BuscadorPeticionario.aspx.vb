﻿Public Class BuscadorPeticionario
    Inherits FSNPage

    Private pag As FSNPage
    Private Sub BuscadorPeticionario_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        If pag Is Nothing Then pag = CType(Me.Page, FSNPage)
        pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorContratos

        If Not Page.IsPostBack Then
            'Si dejasen de cargarse los peticionarios por jQuery, habría que eliminar esta la llamada a CargarScriptBuscadoresJQuery del Init y descomentar la de CargarCombosPartidas
            CargarScriptBuscadoresJQuery()
            CargarPanelPeticionarios()
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CargarIdiomasControles()
        txtCodElementID.Value = Request("IdControlCodArt")
        txtDenElementID.Value = Request("IdControlDenArt")
    End Sub
    ''' <summary>
    ''' Carga los controles con el idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde:=Page_load; Tiempo máximo:=0seg.</remarks>
    Private Sub CargarIdiomasControles()
        pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorContratos
        lblPeticionarios.Text = pag.Textos(100) 'Búsqueda de Peticionario
        lblFiltroPeticionarios.Text = pag.Textos(33) & ":" 'Peticionario:
        minCharPeticionarios.Text = pag.Textos(101) '(Mín. 4 caracteres)
    End Sub
    ''' Revisado por: blp. Fecha: 29/08/2013
    ''' <summary>
    ''' Registra el script con las funciones para usar con paneles de búsqueda construidos con jQuery
    ''' </summary>
    ''' <remarks>LLamada desde el Page_Load; Tiempo máximo: 0,5 sg</remarks>
    Private Sub CargarScriptBuscadoresJQuery()
        Dim oScript As HtmlGenericControl
        oScript = pag.CargarBuscadoresJQuery(TiposDeDatos.Aplicaciones.PM)
        'Cuando todo se hace en CargarScriptPeticionarios, se devuelve un script "EMPTY" y no hay nada más que hacer. Si no, hay que insertar en la página el script q se recibe
        If oScript IsNot Nothing AndAlso oScript.TagName = "script" Then
            'pag.ScriptPeticionarios = oScript
            pag.RegistrarScript(oScript, "ScriptBuscadoresJQuery")
        End If
    End Sub
    ''' Revisado por: blp. Fecha: 29/08/2013
    ''' <summary>
    ''' Carga los peticionarios en el panel de peticionarios. 
    ''' </summary>
    ''' <remarks>LLamada desde el Page_Load; Tiempo máximo: 0,5 sg</remarks>
    Private Sub CargarPanelPeticionarios()
        'Cargar el filtrado cada vez que escribe
        Dim sScript As String = "cargarDatosPeticionario('" & TiposDeDatos.ControlesBusquedaJQuery.Peticionario & "', '" & listadoPeticionarios.ClientID & "', '" & FiltroPeticionarios.ClientID & "', '" & TiposDeDatos.AplicacionesEnNumero.PM & "');"
        'imgPanelPeticionarios.Attributes.Add("onclick", "abrirPanelPeticionarios('" & mpePeticionarios.ClientID & "', '" & FiltroPeticionarios.ClientID & "', event, '" & txtPeticionario.ClientID & "', '" & txtPeticionario_Hidden.ClientID & "', false, '" & listadoPeticionarios.ClientID & "')")
        'Vaciamos el contenido de FiltroPeticionarios para asegurarnos de que al cambiar su contenido no mezclamos en la función de js CargarDatos textos de dos búsquedas distintas
        'txtPeticionario.Attributes.Add("onKeyDown", "limpiarPanelPeticionarios('" & FiltroPeticionarios.ClientID & "','" & listadoPeticionarios.ClientID & "'); abrirPanelPeticionarios('" & mpePeticionarios.ClientID & "','" & FiltroPeticionarios.ClientID & "', event, '" & txtPeticionario.ClientID & "', '" & txtPeticionario_Hidden.ClientID & "', true,'" & listadoPeticionarios.ClientID & "');")
        FiltroPeticionarios.Attributes.Add("onkeyup", sScript)
        FiltroPeticionarios.Attributes.Add("autocomplete", "off") 'Esto impide la función autocompletar de FF. Si se usase, se controlaría mal la carga de peticionarios.
    End Sub
End Class