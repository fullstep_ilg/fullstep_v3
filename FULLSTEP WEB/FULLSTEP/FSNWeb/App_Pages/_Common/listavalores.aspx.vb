﻿
Partial Public Class listavalores
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sIdi As String = FSNUser.Idioma.ToString()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ListaValores

        Me.lblTitulo.Text = HttpUtility.UrlDecode(Request.QueryString("denCampo"))
        Me.lblSubtitulo.Text = Textos(0)
        CrearTabla()

    End Sub
    Private Sub CrearTabla()
        Dim sCampo As String
        Dim sPer As String
        Dim oCampo As FSNServer.Campo
        Dim ds As New DataSet
        Dim tbl As New System.Web.UI.WebControls.Table
        Dim otblRow As System.Web.UI.WebControls.TableRow
        Dim otblCell As System.Web.UI.WebControls.TableCell
        Dim oLabel As System.Web.UI.WebControls.Label
        Dim oImgButton As System.Web.UI.WebControls.ImageButton
        Dim sClase As String

        oCampo = Me.FSNServer.Get_Object(GetType(FSNServer.Campo))
        sCampo = Request.QueryString("idCampo")

        sPer = Me.FSNUser.CodPersona

        ds = oCampo.DevolverValoresCampo(sCampo, sPer)
        tbl = Page.FindControl("tblContenido")
        sClase = "ugfilatabla"
        tbl.Rows.Clear()
        For Each dr As DataRow In ds.Tables(0).Rows
            otblRow = New System.Web.UI.WebControls.TableRow
            otblCell = New System.Web.UI.WebControls.TableCell
            otblCell.Width = Unit.Percentage(75)
            oLabel = New System.Web.UI.WebControls.Label
            oLabel.Text = DBNullToStr(dr.Item("VALOR_TEXT"))
            oLabel.ID = "lbl" & DBNullToStr(dr.Item("ID")) & "#" & DBNullToStr(dr.Item("CAMPO"))
            otblCell.Controls.Add(oLabel)
            otblCell.CssClass = sClase
            otblRow.Cells.Add(otblCell)
            otblCell = New System.Web.UI.WebControls.TableCell
            otblCell.Width = Unit.Percentage(22)
            oImgButton = New System.Web.UI.WebControls.ImageButton
            oImgButton.AlternateText = "Eliminar"
            oImgButton.ImageUrl = ConfigurationManager.AppSettings("ruta") & "images/eliminar.gif"
            oImgButton.ID = DBNullToStr(dr.Item("ID")) & "#" & DBNullToStr(dr.Item("CAMPO"))
            AddHandler oImgButton.Command, AddressOf EliminarValorCampo
            otblCell.Controls.Add(oImgButton)
            otblCell.CssClass = sClase
            otblRow.Cells.Add(otblCell)
            tbl.Rows.Add(otblRow)
        Next
    End Sub
    Private Sub EliminarValorCampo(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim oCampo As FSNServer.Campo
        Dim id As String = CType(sender, WebControls.ImageButton).ID
        Dim sPer As String = Me.FSNUser.CodPersona
        Dim Campos() As String
        Campos = id.Split("#")
        oCampo = Me.FSNServer.Get_Object(GetType(FSNServer.Campo))
        oCampo.EliminarValoresCampo(Campos(0), Campos(1), sPer)
        CrearTabla()
    End Sub
End Class