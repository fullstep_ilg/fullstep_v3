﻿Imports Infragistics.WebUI.UltraWebGrid
Partial Public Class PresuFavoritoServer
    Inherits FSNPage

    ''' <summary>
    ''' Carga la página con los datos de la tabla externa cargados
    ''' </summary>
    ''' <remarks>LLamada desde: Al cargar la página; Tiempo máx: 0,3 sg.</remarks>
     Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
       
        Dim oPresupFavoritos As FSNServer.PresupuestosFavoritos

        Dim sScript As String
        oPresupFavoritos = FSNServer.Get_Object(GetType(FSNServer.PresupuestosFavoritos))
        Select Case Request("accion")
            Case "Eliminar"
                oPresupFavoritos.Eliminar(FSNUser.Cod, Request("Tipo"), Request("PresupId"))
            Case "Insertar"
                Dim lNuevoId As Long
                lNuevoId = oPresupFavoritos.Insertar(FSNUser.Cod, Request("Tipo"), Request("PresupId"), Request("Nivel"))
                If Not IsNothing(lNuevoId) Then
                    sScript = "<script>window.parent.AnyadirFavGrid(" & lNuevoId & ");</script>"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NuevoFavorito", sScript)
                End If
            Case "Automatico"
                FSNUser.Actualizar_AnyadirPresFav(Request("Tipo"), Request("Activar") = "true")
        End Select
    End Sub
End Class