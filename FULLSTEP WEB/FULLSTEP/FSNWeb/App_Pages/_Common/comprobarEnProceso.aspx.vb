﻿Partial Public Class comprobarEnProceso
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sInstancia As String = Request.Form("ID")
        Dim iEnProceso As Integer
        Dim cInstancia As FSNServer.Instancia

        cInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        cInstancia.ID = CLng(sInstancia)
        iEnProceso = cInstancia.ComprobarEnProceso()

        Response.Write(iEnProceso)
    End Sub

End Class