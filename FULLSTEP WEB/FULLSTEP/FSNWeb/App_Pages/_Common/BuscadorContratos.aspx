﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BuscadorContratos.aspx.vb"
    Inherits="Fullstep.FSNWeb.BuscadorContratos" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>      
</head>
<body style="background-color: #FFFFFF; margin-top: 0px; margin-left: 0px; margin-right: 0px">
    <form id="frmBuscadorContratos" name="frmBuscadorContratos" method="post" runat="server" defaultbutton="btnBuscar">
    
    <script type="text/javascript">
         function Importar() {
            grid = $find("wdgSolicitudes");
            row = grid.get_behaviors().get_selection().get_selectedRows(0);
            if (!row) {
                alert(sMensajeSolicitud);
                return false;
            }
            cell = row.getItem(0).get_cellByColumnKey("ID");
            sID = cell.get_value();
            window.opener.open(rutaPM + 'contratos/altaContrato.aspx?IdInstanciaImportar=' + sID + '*Solicitud=' + lSolicitud, "_self");
            window.close();
        }   


        
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        <CompositeScript>
            <Scripts>
                <asp:ScriptReference Path="../../js/jsUpdateProgress.js" />
            </Scripts>
        </CompositeScript>
    </asp:ScriptManager>
    <asp:Panel ID="pnlTitulo" runat="server" Width="100%">
        <table width="100%" cellpadding="0" border="0">
            <tr>
                <td>
                    <fsn:FSNPageHeader ID="FSNPageHeader" runat="server">
                    </fsn:FSNPageHeader>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlStep1" runat="server" Width="100%" Height="100%">
        <asp:Panel ID="pnlParametros" runat="server" Width="100%">
            <table width="100%" style="padding-top: 4px; padding-bottom: 4px"
                border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 20%; padding-left: 5px; padding-top: 5px;">
                        <asp:Label ID="lblCodContrato" runat="server" Text="dTipo solicitud:" Font-Bold="true"></asp:Label>
                    </td>
                    <td  colspan="3">
                        <asp:TextBox ID="txtCodContrato" runat="server" Width="300px" MaxLength="50"></asp:TextBox>         
                    </td>
                </tr>
                <tr>
                    <td style="width: 15%; padding-left: 5px; padding-top: 5px;">
                        <asp:Label ID="lblTipoSolicitud" runat="server" Text="dTipo solicitud:" Font-Bold="true"></asp:Label>
                    </td>
                    <td style="width: 35%; padding-top: 5px;padding-top: 5px;" colspan="3">
                        <asp:Label ID="lblDenTipoSolicitud" runat="server" Text="dSolicitud de proyecto" Width="100%"></asp:Label>
                        <asp:HiddenField ID="hfTipoSolicitud" runat="server" />
                    </td>
                    <td style="width: 10%;padding-top: 5px;padding-top: 5px;">
                        <asp:Label ID="lblPeticionario" runat="server" Text="dPeticionario:" Font-Bold="true"></asp:Label>
                    </td>
                    <td style="width: 35%; padding-top: 5px;padding-top: 5px;" >
                        <asp:Label ID="lblDenPeticionario" runat="server" Visible="false" Width="290px"></asp:Label>
                        <asp:HiddenField ID="hfPeticionario" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 5px;">
                        <asp:Label ID="lblFechaDesde" runat="server" Text="dFecha alta desde:" Font-Bold="true"></asp:Label>
                    </td>
                    <td style="padding-top:5px;">       
                        <igpck:WebDatePicker runat="server" ID="dteFechaDesde" Width="100px" SkinID="Calendario"></igpck:WebDatePicker>
                        </td>
                    <td style="white-space:nowrap;">
                        <asp:Label ID="lblFechaHasta" runat="server" Text="dFecha hasta:" Font-Bold="true"></asp:Label>
                    </td>
                    <td style="padding-top:5px;">
                        <igpck:WebDatePicker runat="server" ID="dteFechaHasta" Width="100px" SkinID="Calendario"></igpck:WebDatePicker>
                          </td>  
                    <td style="padding-top:5px;">
                        <asp:Label ID="lblEstado" runat="server" Text="dEstado:" Font-Bold="true"></asp:Label>
                    </td>
                    <td style="padding-top:5px;">
                        <ig:WebDropDown ID="wddEstado" runat="server" Width="148px" DropDownContainerWidth="148"
                            DropDownAnimationDuration="1" EnableDropDownAsChild="false" EnableCustomValues="false">
                        </ig:WebDropDown>
                    </td>
                </tr>
                <tr>
                     <td style="padding-top:5px; padding-left: 5px;">
                        <asp:Label ID="lblProveedor" runat="server" Text="DProveedor" CssClass="Etiqueta"></asp:Label>
                    </td>
                     <td style="padding-top: 5px;" colspan="3">
                        <table style="background-color: White; width: 300px; border: solid 1px #BBBBBB" cellpadding="0"
                            cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtProveedor" runat="server" Width="220px" BorderWidth="0px" BackColor="White"
                                        Height="16px" ReadOnly="true" ></asp:TextBox>
                                </td>
                                <td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px">
                                    <asp:ImageButton ID="imgProveedorLupa" runat="server" SkinID="BuscadorLupa" />
                                </td>
                            </tr>
                            <asp:HiddenField ID="hidProveedor" runat="server" />
                            <asp:HiddenField ID="hidProveedorCIF" runat="server" />
                        </table>
                    </td>
                    <td style="padding-top: 5px;">
                        <asp:Label ID="lblEmpresa" runat="server" Text="Empresa" CssClass="Etiqueta"></asp:Label>
                    </td>
                     <td style="padding-top: 5px;">
                        <asp:Label ID="lblEmpresaLCX" runat="server" CssClass="label" Style="width: 280px; overflow: hidden; white-space: nowrap;"></asp:Label>
                        <asp:Panel ID="PanelTableEmpresa" runat="server" Width="100%" cellpadding="0"  cellspacing="0">
                            <table  style="background-color: White; width:300px; border: solid 1px #BBBBBB" cellpadding="0"
                                cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtEmpresa" runat="server" Width="220px" BorderWidth="0px" BackColor="White"
                                            Height="16px" ReadOnly="true" ></asp:TextBox>
                                    </td>
                                    <td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px">
                                        <asp:ImageButton ID="imgEmpresaLupa" runat="server" SkinID="BuscadorLupa" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:HiddenField ID="hidEmpresa" runat="server" />

                    </td>

                </tr>
                <tr>
                    <td style="padding-left: 5px;">
                        <asp:Label ID="lblArticulo" runat="server" Text="dArticulo:" Font-Bold="true"></asp:Label>
                    </td>
                    <td style="padding-top:5px;" colspan="3">
                        <asp:TextBox ID="txtArticulo" runat="server" Width="300px"></asp:TextBox>
                        <ajx:AutoCompleteExtender ID="txtArticulo_AutoCompleteExtender" 
                         runat="server" CompletionSetCount="10" Enabled="True" DelimiterCharacters=""
                         MinimumPrefixLength="3" ServiceMethod="GetArticulos" ServicePath="App_Services/AutoComplete.asmx"
                         TargetControlID="txtArticulo" EnableCaching="False"></ajx:AutoCompleteExtender >                        
                    </td>
                    <td></td>
                    <td style="padding-right:20px;padding-top: 5px;" align="right" >
                        <fsn:FSNButton ID="btnBuscar" Text="DBuscar" runat="server" >                    
                            &nbsp;&nbsp;&nbsp;&nbsp;                    
                        </fsn:FSNButton>   
                    </td>
                </tr>
            </table>

        </asp:Panel>
        <asp:Panel ID="pnlGridSolicitudes" runat="server" Width="100%">
            <!--Filtro -->
            <asp:Panel ID="pnlSinPaginacion" runat="server" Visible="True" Width="98%">
                <div style="border-top: 1px solid #CCCCCC;">
                    <table border="0" width="100%">
                        <tr>
                            <td>
                                <asp:Label ID="lblFiltrarPor" runat="server" Text="DFiltrar Por:" Font-Bold="true"></asp:Label>
                                <asp:RadioButton ID="optFiltrar1" GroupName="optFiltrarPor" runat="server" AutoPostBack="true" />
                                <asp:ImageButton ID="imgFiltrar1" SkinID="Filtro_Cabecera" runat="server" />
                                <asp:RadioButton ID="optFiltrar2" GroupName="optFiltrarPor" runat="server" AutoPostBack="true" />
                                <asp:ImageButton ID="imgFiltrar2" SkinID="Filtro_Columna" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <!--Grid Solicitudes -->
            <asp:UpdatePanel ID="UpdatePanelSolicitudes" runat="server" UpdateMode="conditional" ChildrenAsTriggers="false">
                <ContentTemplate>
                    <table border="0" width="98%" style="margin-left: 5px; margin-right: 5px;" cellspacing="0">
                        <tr>
                            <td style="height: 5px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="divSolicitudes">
                                    <ig:WebDataGrid ID="wdgSolicitudes" runat="server" Width="100%" Height="200px" SkinID="Visor" Browser="Xml"
                                    AutoGenerateBands="false" AutoGenerateColumns="false" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="true">
                                        <Behaviors>
                                            <ig:Selection  RowSelectType="Single" Enabled="True" CellClickAction="Row" ></ig:Selection> 
                                            <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true"  AnimationEnabled="true"> 
                                            </ig:Filtering> 
                                            <ig:RowSelectors Enabled="false" RowNumbering="false"></ig:RowSelectors>
                                            <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
                                        </Behaviors>
                                    </ig:WebDataGrid>
                                </div>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlComandos" runat="server" Visible="True" Width="100%">
        <table width="60%">
            <tr>
                <td align="center" style="width:100%">
                </td>
                <td align="center" style="width:4%">    
                    <fsn:FSNButton ID="btnAceptar1" runat="server" Text="DAceptar"></fsn:FSNButton>        
                </td>
                <td align="left" style="width:100%">                    
                    <fsn:FSNButton ID="btnCancelar" runat="server" Text="DCancelar"
                        OnClientClick="window.close();return false;"></fsn:FSNButton>   
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
        <div style="position: relative; top: 30%; text-align: center;">
            <asp:Image ID="ImgProgress" runat="server" SkinID="ImgProgress" />
            <asp:Label ID="LblProcesando" runat="server"></asp:Label>
        </div>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
    </form>
      
    <script type="text/javascript">
             function prove_seleccionado2(sCIF, sProveCod, sProveDen) {
            document.getElementById("<%=hidProveedorCIF.ClientID%>").value = sCIF;
            document.getElementById("<%=hidProveedor.ClientID%>").value = sProveCod;
            document.getElementById("<%=txtProveedor.ClientID%>").value = sProveDen;
        }

        function eliminarEmpresa(event) {
            if ((event.keyCode >= 35) && (event.keyCode <= 40)) {

            } else {
                if ((event.keyCode == 8) || (event.keyCode == 46)) {
                    document.getElementById('<%= hidEmpresa.ClientID %>').value = '';
                    document.getElementById('<%= txtEmpresa.ClientID %>').value = '';
                }
            }
        }
        function eliminarProveedor(event) {
            if ((event.keyCode >= 35) && (event.keyCode <= 40)) {

            } else {
                if ((event.keyCode == 8) || (event.keyCode == 46)) {
                    document.getElementById("<%=hidProveedorCIF.ClientID%>").value = '';
                    document.getElementById("<%=hidProveedor.ClientID%>").value = '';
                    document.getElementById("<%=txtProveedor.ClientID%>").value = '';
                }
            }
        }


        function SeleccionarEmpresa(idEmpresa, nombreEmpresa, nifEmpresa) {
                oIdEmpresa = document.getElementById('<%= hidEmpresa.ClientID %>');
		    if (oIdEmpresa)
		        oIdEmpresa.value = idEmpresa;

		    otxtEmpresa = document.getElementById('<%=txtEmpresa.ClientID %>');
			if (otxtEmpresa)
			    otxtEmpresa.value = nombreEmpresa;
        }
        
    </script>
</body>
</html>
