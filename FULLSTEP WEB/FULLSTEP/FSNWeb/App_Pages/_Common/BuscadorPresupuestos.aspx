﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BuscadorPresupuestos.aspx.vb" Inherits="Fullstep.FSNWeb.BuscadorPresupuestos" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

</head>
<body onload="form_load()" style="background-color: #FFFFFF; margin-top: 0px; margin-left: 0px; margin-right: 0px">
    <script type="text/javascript">		
	    var actualizarcombo = 0;
	    var NuevoFavorito;
		
	    function VerAyuda(){		    
		    var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/detalledenominacion.aspx","_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");
		    newWindow.focus();
	    };

	    function Aceptar(){				
		    var oAsigPorcent = $("#txtAsignadoPorcent")
		    if (oAsigPorcent) {
			    var AsigPorcent = oAsigPorcent.val();					
			    if ((AsigPorcent < 100) && (arrDistribuciones.length > 0))
				    alert(cadenaerror100);
									
			    var sValor = ""
			    var sTexto = ""
			    var bNoAceptoNivelMinimo = false;
			    for (var i in arrDistribuciones) {
				    oDistr = arrDistribuciones[i]
				    if (oDistr.anyo>0)
					    sTexto += oDistr.anyo.toString() + " - " 
				    switch(oDistr.nivel) {
					    case 1:
						    sTexto += oDistr.pres1
						    break; 
					    case 2:
						    sTexto += oDistr.pres1 + " - " + oDistr.pres2
						    break; 
					    case 3:
						    sTexto += oDistr.pres1 + " - " + oDistr.pres2 + " - " + oDistr.pres3
						    break; 
					    case 4:
						    sTexto += oDistr.pres1 + " - " + oDistr.pres2 + " - " + oDistr.pres3 + " - " + oDistr.pres4
						    break; 
				    }
				    sTexto += " (" + num2str(oDistr.porcent * 100,".",",",2) + separadorDecimal +"00%); "
				    sValor += oDistr.nivel.toString() + "_" + oDistr.idpres.toString() + "_" + num2str(oDistr.porcent,".",",",2).toString() + "#"
			        //Nivel Minimo
				    if (NivelMinimo>0){
				        if (NivelMinimo>oDistr.nivel){
				            bNoAceptoNivelMinimo = true;
				        }
				    }
			    }

			    if (bNoAceptoNivelMinimo){
			        alert(cadenaNivelMinimo);
			        return;
			    }		        
				
			    sValor = sValor.substr(0,sValor.length-1)
				
			    window.opener.pres_seleccionado(document.forms["frmPresupuestos"].elements["IDCONTROL"].value, sValor, sTexto, document.forms["frmPresupuestos"].elements["EsCabeceraDesglose"].value, <%=Request("Campo")%>, document.forms["frmPresupuestos"].elements["OrgCompras"].value);
			    window.close();				
		    } else window.close();
	    }
		
        function Buscar() {	
		    var oDrop_;

		    if ($find("ugtxtunidadorganizativa") != null) {
			    oDrop_ = $find("ugtxtunidadorganizativa")
			    var oItem = oDrop_.get_selectedItem()
                if (oItem!=null) {
				    var sValue = oItem.get_value()
				    var sArray = sValue.split('@@')
				    document.forms["frmPresupuestos"].elements["UON1"].value=sArray[0]
				    if(sArray[1]==null){sArray[1]=''}
				    document.forms["frmPresupuestos"].elements["UON2"].value=sArray[1]
				    if(sArray[2]==null){sArray[2]=''}
				    document.forms["frmPresupuestos"].elements["UON3"].value=sArray[2]
                }
		    }
			
		    if ((document.forms["frmPresupuestos"].elements["TIPOPRESUPUESTO"].value == 1 ) || (document.forms["frmPresupuestos"].elements["TIPOPRESUPUESTO"].value == 2 ))
			    document.forms["frmPresupuestos"].elements["Anyo"].value=$find("ugtxtAnyo").get_value(); //prespuestos anuales
		    else document.forms["frmPresupuestos"].elements["Anyo"].value=0;
			
		    var bUO =true;
			
		    if($find("ugtxtunidadorganizativa") != null) {
                if(oDrop_.get_selectedItem()!=null) {
			        sUO =oDrop_.get_currentValue()
				    if (sUO=="") bUO=false
                }
		    }   
			  			
		    if (bUO==false && (document.forms["frmPresupuestos"].elements["txtCodigo"].value=="") && (document.forms["frmPresupuestos"].elements["txtDenominacion"].value==""))
			    alert(cadenaerrorfiltrado)
		    else {	
			    if ((document.forms["frmPresupuestos"].elements["TIPOPRESUPUESTO"].value == 1 ) || (document.forms["frmPresupuestos"].elements["TIPOPRESUPUESTO"].value == 2 )) {
				    if ($find("ugtxtAnyo").get_value()!= null) {
					    RellenarValor()				
					    document.forms["frmPresupuestos"].submit()
				    }
			    } else {
				    RellenarValor()				
				    document.forms["frmPresupuestos"].submit()				
			    }
		    }			
	    }
	
        function RellenarValor() {
	        var f = document.forms["frmPresupuestos"]
	        var sValor=""
	        for (var i in arrDistribuciones) {
			    oDistr = arrDistribuciones[i];					
			    sValor += oDistr.nivel.toString() + "_" + oDistr.idpres.toString() + "_" + num2str(oDistr.porcent,".",",",2).toString() + "#";
		    }
	        sValor = sValor.substr(0,sValor.length-1);
	        f.elements["Valor"].value=sValor;
        }
	
        var dPorcentDistribuido;
        var arrDistribuciones = new Array();
	
        function Distribucion(uon1, uon2, uon3, anyo, pres1, pres2, pres3, pres4, porcent, nivel,idpres,denuon,denpres,nodeId,indexNode,bajalog) {
            if (uon1!="") this.uon1 = uon1;
            else this.uon1 = null;
		
            if (uon2!="") this.uon2 = uon2;
            else this.uon2 = null;

            if (uon3!="") this.uon3 = uon3;
            else this.uon3 = null;

            this.anyo = anyo;
	
            if (pres1!="") this.pres1 = pres1;
            else this.pres1 = null;

            if (pres2!="") this.pres2 = pres2;
            else this.pres2 = null;

            if (pres3!="") this.pres3 = pres3;
            else this.pres3 = null;

            if (pres4!="") this.pres4 = pres4;
            else this.pres4 = null;

            this.porcent = porcent;
            this.nivel = nivel;
            this.idpres = idpres;
            this.denuon = denuon;
            this.denpres = denpres;

            if (nodeId!=null && nodeId!="") this.nodeId = nodeId;
            else this.nodeId = null;

            if (indexNode!=null && indexNode!="") this.indexNode = indexNode;
            else this.indexNode = null;

            if (bajalog!=null && bajalog=='True') this.bajalog = true;
            else this.bajalog = false;
        }		
	
        var PorcentageSelected="";
        var ValorAsociadoSelected="";
        var DenominacionSelected="";
    
        function Quitar() {
            if (ValorAsociadoSelected != "") {
                var f = document.forms["frmPresupuestos"];
                var temp = 	ValorAsociadoSelected.split("_");

                for (var i in arrDistribuciones) {
		            oDistr = arrDistribuciones[i]
		            if (oDistr.idpres == temp[1] && oDistr.nivel==temp[0]) {
			            //Elimina del array
			            arrDistribuciones.splice(i,1)
			            break
			        }
	            }
				
                //Quita el presupuesto asignado de las grids de asignaciones.
                oGrid = $find("wdgAsignaciones");	
                for (i=0;i<oGrid.get_rows().get_length();i++) {
	                oRow=oGrid.get_rows().get_row(i)
	                var column = oGrid.get_columns().get_columnFromKey("VALORASOCIADO");
	                if (oRow.get_cellByColumn(column).get_value()==ValorAsociadoSelected) {	
		                oGrid.get_rows().remove(oRow,false);	
		                break
		            }
                }	
	
                //Actualiza los porcentajes
                CargarPorcentage();
	
                //Cuando quita un presupuesto de la grid se oculta la grid "peque"
                RestaurarGrids();
            }
        }		

        function Aniadir() {         
            //Añade el nodo seleccionado a la grid de asignaciones
            var f = document.forms["frmPresupuestos"];
            var oTree=$find("wdtPresupuestos2");
            var oNode=oTree.get_selectedNodes()[0];

            if (oNode!=null) { //Si no es el nodo raíz podemos asignar        
                if (oNode.get_parentNode()!=null) { //Si no es el nodo raíz y el valor es > 0 podemos asignar        
                    if ($("#txtPorcent").val() > 0) {  
	                    //Obtenemos todos los datos necesarios para insertar:
	                    var dPorcent=$("#txtPorcent").val()
	                    var sDenPresAsig=""
	                    var sDenPres=oNode.get_text()

	                    if ((document.forms["frmPresupuestos"].elements["TIPOPRESUPUESTO"].value == 1) || (document.forms["frmPresupuestos"].elements["TIPOPRESUPUESTO"].value ==2)) {
		                    sDenPresAsig+= "(" + $find("ugtxtAnyo").get_text() + ") ";	
		                    iAnyo=$find("ugtxtAnyo").get_value();
	                    } else	iAnyo=0;	                

	                    while (oNode.get_parentNode()!=null) {
		                    oNode = oNode.get_parentNode()
		                    if (oNode.get_key()!="") {	
			                    sDenPresAsig+= oNode.get_valueString().split("__")[1] + "-"
		                    }
	                    }	
		
	                    sDenPresAsig+= sDenPres;
	                    oNode=oTree.get_selectedNodes()[0];
	                    var nodeId=oNode.Id;
	                    var idPresup = oNode.get_valueString().split("__")[0];
	                    var oarrNode = null;
	                    var arrPres = new Array();
	                    while (oNode.get_parentNode()!=null) {
		                    arrPres[arrPres.length] = oNode.get_valueString().split("__")[1];
		                    oNode = oNode.get_parentNode();
	                    }
	                    var i = arrPres.length - 1;
	                    var sPres1 = null;
	                    var sPres2 = null;
	                    var sPres3 = null;
	                    var sPres4 = null;
		
	                    sPres1 = arrPres[i];
	                    iNivel = 1;
	                    i--;
	                    if (i>=0) {
		                    sPres2 = arrPres[i]
		                    iNivel = 2
		                    i--
		                    if (i>=0) {
			                    sPres3 = arrPres[i]
			                    iNivel = 3
			                    i--
			                    if (i>=0) {
				                    sPres4 = arrPres[i]
				                    iNivel = 4
				                    i--
				                }
			                }
		                }
		
	                    //Obtiene los datos de la unidad organizativa
	                    sUon1 = f.elements["UON1"].value;
	                    if (sUon1=="" || sUon1=="#") sUon1=null;

	                    sUon2 = f.elements["UON2"].value;
	                    if (sUon2=="" || sUon2=="#") sUon2=null;

	                    sUon3 = f.elements["UON3"].value;
	                    if (sUon3=="" || sUon3=="#") sUon3=null;
		
	                    if ($find("ugtxtunidadorganizativa") != null) {
		                    var oDrop_ = $find("ugtxtunidadorganizativa");
		                    var oItem = oDrop_.get_selectedItem();
		                    sDenUon = oItem.get_text();
		                    if (sDenUon == null ) { //Es vacio por lo que 		                	
			                    sDenUon = document.forms["frmPresupuestos"].elements["DENUON"].value;
		                    }
	                    } else {
		                    sDenUon = document.forms["frmPresupuestos"].elements["DENUON"].value;
	                    }
		  
	                    //*********************Asigna en el array ***************************************
	                    //NOTA1: Comprueba que el presupuesto no esté ya asignado en el array, si es así no hace nada.
	                    var bYaExiste =false
	                    if (parseInt(dPorcent)<100) {
		                    for (var i in arrDistribuciones) {
			                    oDistr = arrDistribuciones[i];		
			                    if (oDistr.idpres == idPresup && oDistr.nivel==iNivel) {
				                    bYaExiste=true;
				                    break;
			                    }
		                    }
	                    }
	                    if (bYaExiste==false) {
		                    //NOTA2: Si ya había presupuestos asignados al 100% y se va a añadir otro al 100% quitamos del array los anteriores:
		                    var dPorcentDistribuido=0;
		                    for (var i in arrDistribuciones) {
			                    oDistr = arrDistribuciones[i];
			                    dPorcentDistribuido = dPorcentDistribuido + parseInt(num2str(oDistr.porcent * 100,".",",",2),10);		
		                    }
		
		                    if (dPorcentDistribuido==100) { 
                                //Elimina del array de asignados y de las 2 grids
			                    for (i=arrDistribuciones.length-1;i>=0;i--) {
				                    arrDistribuciones.splice(i,1);
			                    }
				
			                    oGrid = $find("wdgAsignaciones");
			                    for (i=oGrid.get_rows().get_length()-1;i>=0;i--) {
				                    oRow=oGrid.get_rows().get_row(i);
				                    oGrid.get_rows().remove(oRow,true);	
			                    }
		                    }
			
		                    //asigna el nuevo presupuesto:
		                    arrDistribuciones[arrDistribuciones.length] = new Distribucion(sUon1,sUon2,sUon3,iAnyo,sPres1,sPres2,sPres3,sPres4,parseInt(dPorcent)/100,iNivel,idPresup,sDenUon,sDenPres,nodeId,oarrNode);			
		                    //Inserta en las grids:
                            if (document.getElementById("AdmiteMultiplesPresupuestos").value == "1") {
                                var oNewRow = new Array(dPorcent, sDenPresAsig,iNivel + "_" + idPresup + "_" + dPorcent);
                                oGrid = $find("wdgAsignaciones");                       
                                oGrid.get_rows().add(oNewRow);
                            }            

		                    //Actualiza los porcentajes
		                    CargarPorcentage();
			
		                    //Cuando añade un presupuesto de la grid se oculta la grid "peque"
		                    RestaurarGrids();

                            if (document.getElementById("AdmiteMultiplesPresupuestos").value == "0"){
                                EscribirPresAsignado();
                            }

		                    //Si está seleccionado el añadir automáticamente a favoritos lo añade			
		                    if (document.forms['frmPresupuestos'].elements['chkFavAutomaticos'].checked) setTimeout("AnyadirFavoritos()",500);
	                    }
                    }
                }
            }
        }

        function wdgPresupuestos_CellSelectionChanged(sender,e) {
            var cell = e.getSelectedCells().getItem(0);
            var columnClicked = cell.get_column();
            grid = $find("wdgPresupuestosFavoritos");
            var gridRows = grid.get_rows();

            if (columnClicked.get_key() == "ELIMINAR") {
                gridRows.remove(cell.get_row());
            } else {
	            if ($("#txtPorcent").val()>0){
                    var idPresup = cell.get_row().get_cellByColumnKey("PRESID").get_value();
		            var iNivel = cell.get_row().get_cellByColumnKey("NIVEL").get_value();
		            // Comprueba que el presupuesto no esté ya asignado en el array, si es así no hace nada.
		            for (var i in arrDistribuciones) {
			            oDistr=arrDistribuciones[i];
			            if (oDistr.idpres==idPresup && oDistr.nivel==iNivel) return false;
		            }
			
		            // Si ya había presupuestos asignados al 100% no hace nada
		            var dPorcentDistribuido=0;
		            for (var i in arrDistribuciones) {
			            oDistr = arrDistribuciones[i];
			            dPorcentDistribuido = dPorcentDistribuido + parseInt(num2str(oDistr.porcent * 100,".",",",2),10);		
		            }
		
		            if ((dPorcentDistribuido==100) && (document.getElementById("AdmiteMultiplesPresupuestos").value == "1")) return false;

		            if ((dPorcentDistribuido==100) && (document.getElementById("AdmiteMultiplesPresupuestos").value == "0")) {
			            var oAsigPorcent = $("#txtAsignadoPorcent");
			            oAsigPorcent.val(0);
		
			            var oPendPorcent = $("#txtPendientePorcent");
			            oPendPorcent.val(100);
				
			            for (var i in arrDistribuciones) {	
					            arrDistribuciones.splice(i,1)
			            }
		            }
			
		            //asigna el nuevo presupuesto
		            var sUon1  = cell.get_row().get_cellByColumnKey("UON1").get_value();
                    var sUon2  = cell.get_row().get_cellByColumnKey("UON2").get_value();
                    var sUon3  = cell.get_row().get_cellByColumnKey("UON3").get_value();
		            var bAnyo=(document.forms["frmPresupuestos"].elements["TIPOPRESUPUESTO"].value==1 || document.forms["frmPresupuestos"].elements["TIPOPRESUPUESTO"].value==2);
		            var iAnyo=(bAnyo)?cell.get_row().get_cellByColumnKey("ANYO").get_value():0;
		            var sPres1 = cell.get_row().get_cellByColumnKey("PRES1").get_value();
                    var sPres2 = cell.get_row().get_cellByColumnKey("PRES2").get_value();
                    var sPres3 = cell.get_row().get_cellByColumnKey("PRES3").get_value();
                    var sPres4 = cell.get_row().get_cellByColumnKey("PRES4").get_value();
		            var dPorcent = $("#txtPorcent").val();
		            var sDenUon = cell.get_row().get_cellByColumnKey("UONDEN").get_value();
		            var sDenPres=cell.get_row().get_cell(2+iNivel).get_value() + ' - ' + cell.get_row().get_cellByColumnKey("DEN").get_value();
		            var sDenPresAsig=cell.get_row().get_cell((bAnyo)?13:12).get_value();
		            arrDistribuciones[arrDistribuciones.length]=new Distribucion(sUon1,sUon2,sUon3,iAnyo,sPres1,sPres2,sPres3,sPres4,parseInt(dPorcent)/100,iNivel,idPresup,sDenUon,sDenPres,null,null);
		            // Inserta en las grids:
                    var oNewRow = new Array(dPorcent, sDenPresAsig,iNivel + "_" + idPresup + "_" + dPorcent);
                    oGrid = $find("wdgAsignaciones");            
                    oGrid.get_rows().add(oNewRow);
			
		            if (document.getElementById("AdmiteMultiplesPresupuestos").value == "0") {
			            MostrarDetallePresAsignado()
		            }
			
		            //Actualiza los porcentajes
		            CargarPorcentage();
			
		            //Cuando añade un presupuesto de la grid se oculta la grid "peque"
		            RestaurarGrids()
	            }
            }
        }

        function Favorito(favid,nivel,presid,pres1,pres2,pres3,pres4,uon1,uon2,uon3,den,uonden,anyo,presupden) {
            this.favid=favid;
            this.nivel=nivel;
            this.presid=presid;
            this.pres1=pres1;
            this.pres2=pres2;
            this.pres3=pres3;
            this.pres4=pres4;
            this.uon1=uon1;
            this.uon2=uon2;
            this.uon3=uon3;
            this.den=den;
            this.uonden=uonden;
            this.anyo=anyo;
            this.presupden=presupden;
        }

        function SacarDatosPresupuesto() {
            var f=document.forms["frmPresupuestos"];
            var oTree=$find("wdtPresupuestos2");
            var oNode=oTree.get_selectedNodes()[0];

            if (oNode==null) return;
            if (oNode.get_parentNode()==null) return;
            var idPresup=oNode.get_key().split("__")[0];
            var arrPres=new Array();
            while (oNode.get_parentNode()!=null) {
	            arrPres[arrPres.length]=oNode.get_key().split("__")[1];
	            oNode=oNode.get_parentNode()
            }
            oNode=oTree.get_selectedNodes()[0];
            var iNivel=arrPres.length;
            oGridFav =$find("wdgPresupuestosFavoritos");
            var yaEnFavoritos= false;
            for (var i=0; i<oGridFav.get_rows().get_length(); i++) {
	            var row=oGridFav.get_rows().get_row(i);
	            if (row.get_cellByColumnKey("NIVEL").get_value()==iNivel && row.get_cellByColumnKey("PRESID").get_value()==idPresup) yaEnFavoritos=true;
            }
	
            var sPres2=null;
            var sPres3=null;
            var sPres4=null;
            var sPres1=arrPres[iNivel-1];
            if (iNivel>1) sPres2=arrPres[iNivel-2];
            if (iNivel>2) sPres3=arrPres[iNivel-3];
            if (iNivel>3) sPres4=arrPres[iNivel-4];
	
            //Obtiene los datos de la unidad organizativa
            var sUon1 = f.elements["UON1"].value;
            if (sUon1=="" || sUon1=="#") sUon1=null;
            var sUon2 = f.elements["UON2"].value;
            if (sUon2=="" || sUon2=="#") sUon2=null;
            var sUon3 = f.elements["UON3"].value;
            if (sUon3=="" || sUon3=="#") sUon3=null;
	
            var sDenUon="";
            if ($find("ugtxtunidadorganizativa") != null) {
	            var oDrop_ = $find("ugtxtunidadorganizativa")
	            var oItem = oDrop_.get_selectedItem()
	            sDenUon = oItem.get_text()
	            if (sDenUon == null ) {			
		            sDenUon = document.forms["frmPresupuestos"].elements["DENUON"].value;
	            }
            } else sDenUon = document.forms["frmPresupuestos"].elements["DENUON"].value;
	
            var sDenPresAsig="";
            var sDenPres=oNode.get_text();
            var iAnyo=0;
            if ((document.forms["frmPresupuestos"].elements["TIPOPRESUPUESTO"].value == 1) || (document.forms["frmPresupuestos"].elements["TIPOPRESUPUESTO"].value ==2)) {
	            sDenPresAsig+= "(" + $find("ugtxtAnyo").get_text() + ") ";
	            iAnyo=$find("ugtxtAnyo").get_value();
            }
            while (oNode.get_parentNode()!=null) {
	            oNode = oNode.get_parentNode();
	            if (oNode.get_key()!="") {	
		            sDenPresAsig+= oNode.get_key().split("__")[1] + "-";
	            }
            }
            sDenPresAsig+= sDenPres	;	
            i=sDenPres.search(" - ");
            sDenPres=sDenPres.substr(i+3);
            var result = new Array;
            result["iNivel"] = iNivel;
            result["idPresup"] = idPresup;
            result["sPres1"] = sPres1;
            result["sPres2"] = sPres2;
            result["sPres3"] = sPres3;
            result["sPres4"] = sPres4;
            result["sUon1"] = sUon1;
            result["sUon2"] = sUon2;
            result["sUon3"] = sUon3;
            result["sDenPres"] = sDenPres;
            result["sDenUon"] = sDenUon;
            result["iAnyo"] = iAnyo;
            result["sDenPresAsig"] = sDenPresAsig;
            result["yaEnFavoritos"] = yaEnFavoritos;
            return result;
        }

        function EscribirPresAsignado() {
            var res= SacarDatosPresupuesto();
            var sDenPresAsig= res["sDenPresAsig"];
            var sDenUon= res["sDenUon"];
            var iAnyo= res["iAnyo"];
            if(iAnyo!=0) sDenPresAsig = "(" + iAnyo + ") " + sDenPresAsig;
            document.getElementById("DetDenPresAsignado").innerHTML = sDenPresAsig;
            document.getElementById("DetUonPresAsignado").innerHTML = sDenUon;
        }

        function AnyadirFavoritos(){		
            var res= SacarDatosPresupuesto();
            var iNivel= res["iNivel"];
            var idPresup= res["idPresup"];
            var sPres1= res["sPres1"];
            var sPres2= res["sPres2"];
            var sPres3= res["sPres3"];
            var sPres4= res["sPres4"];
            var sUon1= res["sUon1"];
            var sUon2= res["sUon2"];
            var sUon3= res["sUon3"];
            var sDenPres= res["sDenPres"];
            var sDenUon= res["sDenUon"];
            var iAnyo= res["iAnyo"];
            var sDenPresAsig= res["sDenPresAsig"];
 
            if (res["yaEnFavoritos"]) return false;
    
            NuevoFavorito=new Favorito(null,iNivel,idPresup,sPres1,sPres2,sPres3,sPres4,sUon1,sUon2,sUon3,sDenPres,sDenUon,iAnyo,sDenPresAsig);
            window.open("PresuFavoritoServer.aspx?accion=Insertar&Tipo=" + document.forms["frmPresupuestos"].elements["TIPOPRESUPUESTO"].value + "&Nivel=" + iNivel + "&PresupId=" + idPresup,"iframeWSServer", "");
        }

        function AnyadirFavGrid(lNuevoId){
            NuevoFavorito.favid=lNuevoId;
            oGrid =$find("wdgPresupuestosFavoritos");
            var i=12;   
            var oNewRow = new Array(NuevoFavorito.favid, NuevoFavorito.nivel, NuevoFavorito.presid, NuevoFavorito.pres1,
                            NuevoFavorito.pres2, NuevoFavorito.pres3, NuevoFavorito.pres4, NuevoFavorito.uon1, NuevoFavorito.uon2,
                            NuevoFavorito.uon3, NuevoFavorito.den, NuevoFavorito.uonden);
            if (document.forms["frmPresupuestos"].elements["TIPOPRESUPUESTO"].value==1 || document.forms["frmPresupuestos"].elements["TIPOPRESUPUESTO"].value==2) {
                oNewRow.push(NuevoFavorito.anyo)
	            i++;
            }   
            oNewRow.push(NuevoFavorito.presupden);
            i++;
            oGrid.get_rows().add(oNewRow);			   
        }

        function FavoritosAutomaticos(activar){
            window.open("PresuFavoritoServer.aspx?accion=Automatico&Tipo=" + document.forms["frmPresupuestos"].elements["TIPOPRESUPUESTO"].value + "&Activar=" + activar,"iframeWSServer","");
        }

        function Cambiar() {
            if (ValorAsociadoSelected != "") {
                var dPorcent=$find("ugtxtPorcentDetalle").get_value()
		
                if (dPorcent > 0) { //Cuando quiere cambiar a 0 el valor lo hemos de quitar
        	        //Actualiza el array:
	                var temp = 	ValorAsociadoSelected.split("_")
	                for (var i in arrDistribuciones) {
			            oDistr = arrDistribuciones[i]
			            if (oDistr.idpres == temp[1] && oDistr.nivel==temp[0]) {
			                //Actualiza el array
			                oDistr.porcent=parseInt(dPorcent)/100
			                break
			            }
		            }
			
	                //Actualiza las grids:
	                var sNewValorAsociado=""
	                sNewValorAsociado=temp[0] + "_" + temp[1] + "_" + dPorcent
		
	                //var oRow=oGrid.getActiveRow()	
	                //Ahora lo cambia tb en la grid de asignaciones
	                oGrid =$find("wdgAsignaciones");		
	                for (i=0;i<oGrid.get_rows().get_length();i++) {
		                oRow=oGrid.get_rows().get_row(i);
		                if (oRow.get_cell(2).get_value()==ValorAsociadoSelected) {	
			                oRow.get_cell(0).set_value(dPorcent)
			                oRow.get_cell(2).set_value(sNewValorAsociado)
			                break
		                }
	                }	
			
	                PorcentageSelected=dPorcent;
		
	                //Ahora actualiza los porcentajes
	                CargarPorcentage();
		
	                //Porcentaje de la caja de asignación de detalle:
	                var f = document.forms["frmPresupuestos"]
	                var txtasignado=f.elements["txtAsignadoPorcent"].value
	                var txtpendiente=f.elements["txtPendientePorcent"].value					
	                var maximo = parseInt(PorcentageSelected,10) + parseInt(txtpendiente,10);
	                $find("ugtxtPorcentDetalle").set_maxValue(maximo)										
	                $find("ugtxtPorcentDetalle").set_minValue (0)
	                $find("ugtxtPorcentDetalle").set_value(PorcentageSelected)	
				
	                //Cuando cambia un presupuesto de la grid se oculta la grid "peque"
	                RestaurarGrids();			
	                ValorAsociadoSelected = "";	
                }		
            }
        }

        function MostrarDetalle() {
            var temp = 	ValorAsociadoSelected.split("_");
		    var textden="";
		    var textuon = "";
		    var f = document.forms["frmPresupuestos"];
			
		    for (var i in arrDistribuciones) {
			    oDistr = arrDistribuciones[i];
				
			    if (oDistr.idpres == temp[1] && oDistr.nivel==temp[0]) {
				    textuon = cadenaunidadorganizativa + ": "
				    if (oDistr.uon1 != null)					
					    textuon +=  oDistr.uon1					
				    if (oDistr.uon2 != null)
					    textuon += "-" + oDistr.uon2
				    if (oDistr.uon3 != null)
					    textuon += "-" + oDistr.uon3
				    if (oDistr.denuon != null) {					
					    if (oDistr.uon1 != null) textuon += "-";						  
					    textuon += oDistr.denuon						
				    }
										
				    var txtasignado=f.elements["txtAsignadoPorcent"].value
				    var txtpendiente=f.elements["txtPendientePorcent"].value
				    var porcenasignadoaotros = txtasignado - PorcentageSelected					
				    var maximo = parseInt(PorcentageSelected,10) + parseInt(txtpendiente,10);
				    $find("ugtxtPorcentDetalle").set_maxValue(maximo)										
				    $find("ugtxtPorcentDetalle").set_minValue(0)
				    $find("ugtxtPorcentDetalle").set_value(PorcentageSelected)	
				    $find("ugtxtPorcentDetalle").set_readOnly (oDistr.bajalog)	
					
				    document.getElementById("DetDen").innerHTML = DenominacionSelected
				    document.getElementById("DetUon").innerHTML = textuon
				    break;	
			    }				
		    }
        }	
		
        function wdgAsignaciones_RowSelectionChanged(sender, e) {			
            oGrid = $find("wdgAsignaciones");
            var oRow=e.getSelectedRows().getItem(0)		
            if (!oRow) return
            document.getElementById("layergrid").style.height="150px"										

            PorcentageSelected = oRow.get_cell(0).get_value()
            DenominacionSelected= oRow.get_cell(1).get_value()	
            ValorAsociadoSelected = oRow.get_cell(2).get_value()
			
            document.getElementById("tabladetalle").style.display="inline";
            document.getElementById("cont_tabladetalle").style.display="block";
			
            MostrarDetalle();			
        }

        function MensajeSinElementos() {
            alert(cadenaerrorsinpresupuestos)
        }

        function AlertaMultiplesUONS(sText) {
            alert(sText);
        }

        function ugtxtPorcent_ValueChange(oEdit, oldValue, oEvent) {
            var oPendPorcent;
            var permitidos="0123456789";
            var valorporcentage = $("#txtPorcent").val();
	
            if (valorporcentage != null) {
	            var permitido = 0;		 
	            for (i=0; i<= valorporcentage.length-1; i++) {
		            permitido = 0;
			
		            for (j=0; j<= permitidos.length-1; j++) {
			            if (valorporcentage.charAt(i) == permitidos.charAt(j)) {
				            permitido = 1
				            break;
			            }
		            }
			
		            if (permitido == 0) {
			            oPendPorcent = $("#txtPendientePorcent").val()			
			            if (oPendPorcent == 0) $("#txtPorcent").val(100)
			            else  $("#txtPorcent").val(oPendPorcent)
			            break;
		            }
	            }
            } else {
			    oPendPorcent = $("#txtPendientePorcent").val()			
			    if (oPendPorcent == 0) $("#txtPorcent").val(100)
			    else $("#txtPorcent").val(oPendPorcent)
            }
        }

        function IsNumeric(valor) { 
            var log=valor.length; var sw="S"; 
            for (x=0; x<log; x++)  { 
	            v1=valor.substr(x,1); 
	            v2 = parseInt(v1); 
	            if (isNaN(v2)) sw= "N";		        
            } 
            if (sw=="S") return true;
            else  return false;         
        } 
    		
        function CargarPorcentage() {
            dPorcentDistribuido = 0;
            document.getElementById("Botones").style.display="inline";

            for (var i in arrDistribuciones) {
	            oDistr = arrDistribuciones[i]
	            dPorcentDistribuido = dPorcentDistribuido + parseInt(num2str(oDistr.porcent * 100,".",",",2),10);		
            }

            var oAsigPorcent = $("#txtAsignadoPorcent")
            if (oAsigPorcent) oAsigPorcent.val(dPorcentDistribuido);
		
            var oPendPorcent = $("#txtPendientePorcent")
            if (oPendPorcent) oPendPorcent.val(100-dPorcentDistribuido);

            var oPorcent = $("#txtPorcent")

            if (oPorcent) {
	            if (dPorcentDistribuido < 100) {
		            oPorcent.val(100-dPorcentDistribuido);
	            } else {
    	            oPorcent.val(100);	
	            }		
            }
        }

        function RestaurarGrids() {
            //Cuando quita un presupuesto de la grid se oculta la grid "peque"
            if (document.getElementById("AdmiteMultiplesPresupuestos").value == "1") {
	            document.getElementById("layergrid").style.display="inline"	
	            document.getElementById("tabladetalle").style.display="none";
                document.getElementById("cont_tabladetalle").style.display="none";
            }
	
            //Quita las filas seleccionadas de las 2 grids
            var oGrid = $find("wdgAsignaciones");
            var selection = oGrid.get_behaviors().get_selection();
            var rows = selection.get_selectedRows();
            rows.remove(rows.getItem(0));
        }

        function CambiarValorCombo(valor) {
            actualizarcombo = parseInt(valor,10);
        }

        function ActualizarCombo() {
            oDrop = $find("ugtxtunidadorganizativa")
            oDrop.selectItemByIndex(actualizarcombo, true, true)
        }

        function ugtxtAnyo_ValueChange(oEdit, oldValue, oEvent) {		
            var valoranyo = $find("ugtxtAnyo").get_value()	
            var dateVar = new Date()
            var anyo = dateVar.getFullYear() 
				
            if (valoranyo != null && valoranyo !=0) {
	            if ((valoranyo < 1990) || (valoranyo >  anyo))
		            $find("ugtxtAnyo").set_value(anyo)
            } else $find("ugtxtAnyo").set_value(anyo)
        }

        function ugtxtPorcentDetalle_ValueChange(oEdit, oldValue, oEvent) {
            var valorporcentage = $find("ugtxtPorcentDetalle").get_value()
            if (valorporcentage == null) $find("ugtxtPorcentDetalle").set_value(oldValue)
        }

        function MostrarDetallePresAsignado() {		
            oGrid = $find("wdgAsignaciones");
            oRow = oGrid.get_rows().get_row(0)

            if (!oRow) return
				
            oCell = oRow.get_cellByColumnKey("DESCRIPCION")	
            DenominacionSelected= oCell.get_value()		
            oCell = oRow.get_cellByColumnKey("VALORASOCIADO")
            ValorAsociadoSelected = oCell.get_value()
	
            var temp = 	ValorAsociadoSelected.split("_")
            var textuon = ""
				
            for (var i in arrDistribuciones) {
		        oDistr = arrDistribuciones[i]
			
		        if (oDistr.idpres == temp[1] && oDistr.nivel==temp[0]) {
			        textuon = cadenaunidadorganizativa + ": "
			        if (oDistr.uon1 != null)					
				        textuon +=  oDistr.uon1					
			        if (oDistr.uon2 != null)
				        textuon += "-" + oDistr.uon2
			        if (oDistr.uon3 != null)
				        textuon += "-" + oDistr.uon3
			        if (oDistr.denuon != null) {					
				        if (oDistr.uon1 != null) 
					        textuon += "-"
				        textuon += oDistr.denuon						
			        }
			
			        document.getElementById("DetDenPresAsignado").innerHTML = DenominacionSelected
			        document.getElementById("DetUonPresAsignado").innerHTML = textuon
		        }
            }
        }
		
        function wdtPresupuestos2_AfterNodeSelectionChange(treeId, nodeId){	    
            Aniadir()
            MostrarDetallePresAsignado();
        }

        function checkCombo(){
            if (actualizarcombo != 0) ActualizarCombo();
        }
        function form_load(){
            Sys.Application.add_load(checkCombo);
            CargarPorcentage();
        }

    </script>
    <style>
        .NodeSelected
            {
                background-color:Navy;
                color: white;
                padding-bottom:3px !important;
                padding-top:3px !important;
            }
    </style>
    <form id="frmPresupuestos" method="post" runat="server">
        <asp:ScriptManager ID="scrMng_presAsig" runat="server">
            <CompositeScript>
                <Scripts>
                    <asp:ScriptReference Path="../../js/formatos.js" />
                    <asp:ScriptReference Path="../../js/jquery/jquery.min.js" />
                </Scripts>
            </CompositeScript>
        </asp:ScriptManager>
		<table cellspacing="5" cellpadding="0" width="800px" border="0">
			<tr>
				<td valign="top">
					<table cellspacing="4" cellpadding="0" width="100%" border="0">
						<tr>
							<td width="130"></td>
							<td width="170"></td>
							<td width="100"></td>
						</tr>
						<tr style="vertical-align:top; height:30;">
							<td colspan="3">
                                <table>
                                    <tr>
                                        <td><img id="imgCabecera" runat="server" alt="" /></td>
                                        <td><asp:label id="lbltitulo" runat="server" CssClass="RotuloGrande" Width="350px">Busqueda de partidas contables</asp:label></td>
                                    </tr>
                                </table>
                            </td>
						</tr>
						<tr>
							<td><asp:label id="lblUnidadOrganizativa" runat="server" CssClass="Rotulo">Unidad Organizativa:</asp:label></td>
							<td colspan="2">
	                            <ig:WebDropDown ID="ugtxtunidadorganizativa" runat="server" Width="256px"
	                                DropDownContainerHeight="150px" DropDownContainerWidth="256px" DropDownAnimationType="EaseOut" 
	                                EnablePaging="False" PageSize="12" CurrentValue="">
                                </ig:WebDropDown>
                                <asp:label id="lblUorganizativaRestringida" runat="server"  CssClass="captionBlue" Width="256px"></asp:label>
							</td>
						</tr>
						<tr>
							<td><asp:Label id="lblAnyo" runat="server" CssClass="Rotulo">Año:</asp:Label></td>
							<td colspan="2">
                                <igpck:WebNumericEditor id="ugtxtAnyo" runat="server" MinValue="1990" MaxValue="2100">
                                    <ClientEvents ValueChanged ="ugtxtAnyo_ValueChange" />
                                    <Buttons SpinButtonsDisplay ="OnRight" ></Buttons>
								</igpck:WebNumericEditor>
							</td>
						</tr>
						<tr>
							<td><asp:Label id="lblCodigo" runat="server" CssClass="Rotulo" Height="16px">Código:</asp:Label></td>
							<td colspan="2"><asp:TextBox id="txtCodigo" runat="server" Width="120px" Height="20px"></asp:TextBox></td>
						</tr>
						<tr>
							<td><asp:Label id="lblDenominacion" runat="server" CssClass="Rotulo">Denominación:</asp:Label></td>
							<td nowrap>
                                <asp:TextBox id="txtDenominacion" runat="server" Width="152px" Height="20px"></asp:TextBox>
                                <a id="cmdBuscarPet" style="CURSOR: hand" onclick="javascript:VerAyuda()" name="cmdBuscarPet">&nbsp;
									<img id="imgAyuda" runat="server" border="0">
                                </a>
                            </td>
							<td align="center"><input id="cmdBuscar" type="button" value="Buscar" name="cmdBuscar" runat="server" class="boton" onclick="Buscar()"/></td>
						</tr>
					</table>
					<table cellspacing="3" cellpadding="0" width="100%" border="0">
						<tr>
							<td colspan="2"><asp:Label id="lblResultado" runat="server" CssClass="Rotulo" Width="200px" Visible="False">Resultado de la busqueda:</asp:Label></td>
						</tr>
						<tr>
							<td colspan="2" align="left" >
								<hr style="background-color: gainsboro; height: 1px;" />
							</td>
						</tr>
						<tr>
							<td colspan="2">
                                <asp:Label id="lblSeleccionaArbol" runat="server" CssClass="Rotulo10" Width="90%" Height="22px" Visible="False"></asp:Label>
                            </td>
						</tr>
						<tr>
							<td>
                                <ig:WebDataTree ID="wdtPresupuestos2" style="BACKGROUND-COLOR: #e1edff" runat="server" CssClass="igTree" SelectionType="Single" 
									Width="99%" Height="264px" Visible="False">
                                        <NodeSettings SelectedCssClass="NodeSelected" />
                                        <DataBindings>
                                            <ig:DataTreeNodeBinding DataMember="Table" TextField="CDEN"  />
                                            <ig:DataTreeNodeBinding DataMember="Table1" TextField="CDEN" ValueField="IDCOD" KeyField="IDCOD" />
                                            <ig:DataTreeNodeBinding DataMember="Table2" TextField="CDEN" ValueField="IDCOD" KeyField="IDCOD" />
                                            <ig:DataTreeNodeBinding DataMember="Table3" TextField="CDEN" ValueField="IDCOD" KeyField="IDCOD" />
                                            <ig:DataTreeNodeBinding DataMember="Table4" TextField="CDEN" ValueField="IDCOD" KeyField="IDCOD" />
                                        </DataBindings>
                                </ig:WebDataTree> 
                            </td>
							<td align="center">
								<table>
                                    <tr>
                                        <td>
                                            <igpck:WebNumericEditor id="txtPorcent" style="display: " runat="server" Width="69px" Visible="False" Nullable="False">
                                                <ClientEvents ValueChanged="ugtxtPorcent_ValueChange" />
										        <Buttons SpinButtonsDisplay ="OnRight" ></Buttons>
									        </igpck:WebNumericEditor>
                                        </td>
									    <td>
                                            <asp:Label id="lblPorcen1" runat="server" Width="8px" Height="18px" Visible="False" cssClass="captionBlue">%</asp:Label>
                                        </td>
								    </tr>
                                </table>								    
								<br />
								<div id="Botones" align="center">
									<asp:Panel ID="pnlBotonesAniadirQuitar" Runat="server">
										<table cellspacing="4" cellpadding="0" align="center" border="0">
											<tr>
												<td align="center"><img id="imganiadir" style="CURSOR: hand" onclick="javascript:Aniadir()" runat="server" alt="" name="imganiadir"></td>
											</tr>
											<tr>
												<td align="center"><img id="imgquitar" style="CURSOR: hand" onclick="javascript:Quitar()" runat="server" alt="" name="imgquitar"></td>
											</tr>
										</table>
									</asp:Panel>
									<table cellspacing="4" cellpadding="0" align="center" border="0">
										<tr>
											<td align="center"><img id="imgfavoritos" style="CURSOR: hand" onclick="javascript:AnyadirFavoritos()" runat="server" alt="" name="imgfavoritos"></td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
					</table>
				</td>
				<td valign="top">
					<table cellspacing="3" cellpadding="0" align="right" border="0">
						<tr>
							<td colspan="2">
                                <ig:WebDataGrid id="wdgPresupuestosFavoritos" runat="server" Height="140px" Width="100%"
                                    AutoGenerateColumns="False" ShowFooter="false" BackColor="White" HeaderCaptionCssClass="parrafo" BorderStyle="Solid" BorderWidth="1px">
									<Behaviors>
                                        <ig:Selection  RowSelectType="Single" Enabled="True" CellClickAction="Cell" SelectedCellCssClass="SelectedRow">
                                            <SelectionClientEvents CellSelectionChanged="wdgPresupuestos_CellSelectionChanged" />
                                        </ig:Selection>    
                                        <ig:EditingCore AutoCRUD="false">
                                            <Behaviors>
                                                <ig:RowDeleting />
                                                <ig:RowAdding></ig:RowAdding>
                                                <ig:CellEditing></ig:CellEditing>
                                            </Behaviors>
                                        </ig:EditingCore>
                                    </Behaviors>										
								</ig:WebDataGrid>
                                <br/>
								<asp:CheckBox id="chkFavAutomaticos" runat="server" onclick="FavoritosAutomaticos(this.checked);" CssClass="Rotulo10"/>
                            </td>
						</tr>
						<tr>
							<td width="120"><asp:Label id="lblAsignado" runat="server" CssClass="captionBlue" Visible="False">Asignado:</asp:Label></td>
							<td width="280">
                                <igpck:WebNumericEditor id="txtAsignadoPorcent" runat="server" Width="56px" Height="20px" Visible="False" ReadOnly="True"></igpck:WebNumericEditor>&nbsp;
                                <asp:Label id="Label1" runat="server" CssClass="captionBlue" Height="18" Visible="False">%</asp:Label>
                            </td>
						</tr>
						<tr>
							<td><asp:Label id="lblPendiente" runat="server" CssClass="captionBlue" Visible="False">Pendiente:</asp:Label></td>
							<td>
                                <igpck:WebNumericEditor id="txtPendientePorcent" runat="server" Width="56px" Height="20px" Visible="False" ReadOnly="True"></igpck:WebNumericEditor>&nbsp;
                                <asp:Label id="Label3" runat="server" CssClass="captionBlue" Height="18" Visible="False">%</asp:Label>
                            </td>
						</tr>
						<tr>
							<td colspan="2">
                                <asp:Panel ID="layergrid" Runat="server">
                                    <asp:UpdatePanel ID="up_Asignaciones" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                                        <ContentTemplate>
                                            <ig:WebDataGrid id="wdgAsignaciones" runat="server" Width="100%" Height="150px" Visible="False"
                                                AutoGenerateColumns="False" ShowFooter="false" BackColor="White" HeaderCaptionCssClass="parrafo" BorderStyle="Solid" BorderWidth="1px" DataKeyFields="VALORASOCIADO">
                                                <Behaviors>
                                                    <ig:Selection  RowSelectType="Single" Enabled="True" CellClickAction="Row" SelectedCellCssClass="SelectedRow">
                                                        <SelectionClientEvents RowSelectionChanged="wdgAsignaciones_RowSelectionChanged" />
                                                    </ig:Selection>    
                                                    <ig:EditingCore AutoCRUD="false">
                                                        <Behaviors>
                                                            <ig:RowDeleting />
                                                            <ig:RowAdding></ig:RowAdding>
                                                            <ig:CellEditing></ig:CellEditing>
                                                        </Behaviors>
                                                    </ig:EditingCore>
                                                </Behaviors>
										    </ig:WebDataGrid>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </asp:Panel>
							</td>
						</tr>
						<tr>
							<td colspan="2">
                                <div id="cont_tabladetalle" style="border: 1px solid silver; padding-bottom:10px; display:none;">
									<div id="tabladetalle" style="DISPLAY: none; WIDTH: 100%; HEIGHT: 107px"><br>
										<table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
											<tr>
												<td align="left" colspan="2">
													<div class="captionBlackSmall" id="DetDen" style="overflow-y: auto; height: 30px"></div>
													<div class="captionBlackSmall" id="DetUon" style="overflow-y: auto; height: 30px"></div>
												</td>
											</tr>
											<tr>
												<td>
												    <table>
                                                        <tr>
                                                            <td>
                                                                <igpck:webnumericeditor id="ugtxtPorcentDetalle" style="display: " runat="server" Width="80px" Height="19">
														            <ClientEvents ValueChanged="ugtxtPorcentDetalle_ValueChange"></ClientEvents>
														            <Buttons SpinButtonsDisplay ="OnRight" ></Buttons>
													            </igpck:webnumericeditor>
                                                            </td>
													        <td><asp:label id="lblPorcen2" runat="server" Height="18px" Visible="False" cssClass="captionBlue">%</asp:label></td>
                                                        </tr>
                                                    </table>
												</td>
												<td><input class="boton" id="cmdCambiar" onclick="Cambiar()" type="button" value="Cambiar"	name="cmdCambiar" runat="server"/></td>
											</tr>
										</table>
									</div>
                                </div>
							</td>
						</tr>                            
						<tr>
							<td colspan="2">
								<asp:Panel ID="tabladetallePresAsignado" Runat="server" style="BORDER-RIGHT: silver 1px solid; BORDER-TOP: silver 1px solid; DISPLAY: none; BORDER-LEFT: silver 1px solid; BORDER-BOTTOM: silver 1px solid"
									Height="177"> <br />
									<table cellspacing="0" cellpadding="0" width="80%" align="center" border="0">
										<tr>
											<td align="left">
												<asp:Label class="captionBlackSmall" id="DetDenPresAsignado" style="OVERFLOW-Y: auto" runat="server" Height="30" name="DetDenPresAsignado"></asp:Label>
                                                <br/>
												<asp:Label class="captionBlackSmall" id="DetUonPresAsignado" style="OVERFLOW-Y: auto" runat="server" Height="30" name="DetUonPresAsignado"></asp:Label>
                                            </td>
										</tr>
									</table> 
								</asp:Panel>
							</td>
						</tr>
					</table>
				</td>
			</tr>
                
			<tr height="50">
				<td align="center" valign="middle" colspan="2">
                    <input class="boton" id="cmdAceptar" onclick="Aceptar()" type="button" value="Aceptar" name="cmdAceptar" runat="server"/>
                    <input class="boton" id="cmdCancelar" onclick="window.close()" type="button" value="Cancelar" name="cmdCancelar" runat="server"/>
				</td>
			</tr>
		</table>
		<input id="TIPO" type="hidden" name="TIPO" runat="server"/> 
        <input id="IDCONTROL" type="hidden" name="IDCONTROL" runat="server"/>
		<input id="txtunidadorganizativa" type="hidden" name="txtunidadorganizativa" runat="server"/>
		<input id="UON1" type="hidden" name="UON1" runat="server"/> 
        <input id="UON2" type="hidden" name="UON2" runat="server"/>
		<input id="UON3" type="hidden" name="UON3" runat="server"/> 
        <input id="DENUON" type="hidden" name="DENUON" runat="server"/>
		<input id="Valor" type="hidden" name="Valor" runat="server"/> 
        <input id="Anyo" type="hidden" value="0" name="Anyo" runat="server"/>
		<input id="TIPOPRESUPUESTO" type="hidden" value="0" name="TIPOPRESUPUESTO" runat="server"/>
		<input id="USUARIORESTRINGIDO" type="hidden" value="0" name="USUARIORESTRINGIDO" runat="server"/>
		<input id="EsCabeceraDesglose" type="hidden" value="0" name="EsCabeceraDesglose" runat="server"/>
		<input id="OrgCompras" type="hidden" name="OrgCompras" runat="server"/>
		<input id="AdmiteMultiplesPresupuestos" type="hidden" name="AdmiteMultiplesPresupuestos" runat="server" value="1"/> 
		<IFRAME id="iframeWSServer" style="BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; POSITION: absolute; BORDER-BOTTOM-STYLE: none"
			name="iframeWSServer" src="../blank.htm" width="1" height="1" frameborder="no" marginheight="0" marginwidth="0">
		</IFRAME>
	</form>
</body>
<script type="text/javascript">
    Sys.Application.add_load(checkCombo);
    CargarPorcentage()
</script>
</html>
