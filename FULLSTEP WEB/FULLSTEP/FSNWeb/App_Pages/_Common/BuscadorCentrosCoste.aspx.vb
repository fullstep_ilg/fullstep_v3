﻿Public Class BuscadorCentrosCoste
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        BusquedaCentrosCoste.urlImagenCerrar = "~/App_Themes/" & Me.Page.Theme & "/images/Cerrar.gif"
        BusquedaCentrosCoste.urlImagenCentrosCoste = "~/Images/centrocoste.JPG" '"/images/Calendario.png"
        BusquedaCentrosCoste.urlImagenRaiz = "~/Images/World.gif"
        BusquedaCentrosCoste.urlImagenNodo = "~/Images/carpetaNaranja.gif"
        idHidControl.Value = Request("idHidControl")
        idControl.Value = Request("idControl")
        If Not IsPostBack Then
            CargarDatosCentrosCoste()
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.CentrosCoste
            BusquedaCentrosCoste.Textos = TextosModuloCompleto
        End If
    End Sub


    ''' <summary>
    ''' Pasa al userControl de centros de coste los datos de los centros de coste
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarDatosCentrosCoste()
        Dim oCentros_SM As FSNServer.Centros_SM
        oCentros_SM = FSNServer.Get_Object(GetType(FSNServer.Centros_SM))
        BusquedaCentrosCoste.DatosTreeView = oCentros_SM.LoadDataEnIM(Me.FSNUser.Cod, Me.FSNUser.IdiomaCod)
    End Sub
End Class