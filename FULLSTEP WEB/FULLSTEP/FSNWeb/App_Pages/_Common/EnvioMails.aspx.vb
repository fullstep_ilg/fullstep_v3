﻿Imports Fullstep
Imports Fullstep.FSNLibrary

Partial Public Class EnvioMails
    Inherits FSNPage

    ''' Revisado por: Jbg; Fecha: 24/10/2011
    ''' <summary>
    ''' Carga la pagina
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Llamada desde: Sistema; Tiempo máximo:1seg.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EnvioMails

            If Not Page.ClientScript.IsClientScriptBlockRegistered("rutaFS") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' ;</script>")

            imgCabecera.Src = "../../App_Themes/" & Page.Theme.ToString & "/images/cab_enviar_email.gif"
            lblCabecera.InnerText = Textos(3)
            lblCabecera.Attributes.Add("title", Textos(3))
            lnkBotonMail.InnerText = Textos(2)
            lnkBotonMail.Style.Add("background-image", "url(../../App_Themes/" & Page.Theme.ToString & "/images/enviar_email_color.gif" & ")")

            lblPara.Text = Textos(0)
            lblAsunto.Text = Textos(1)

            If Request.QueryString("IdMail") <> 0 Then
                Dim oEmail As FSNServer.Email
                oEmail = FSNServer.Get_Object(GetType(FSNServer.Email))
                oEmail.Load_Registro_Email(Request.QueryString("IdMail"), FSNUser.Cod)

                Dim PuedeVer As Boolean = True
                Select Case oEmail.EntidadEMail
                    Case EntidadNotificacion.Calificaciones, EntidadNotificacion.Certificado, EntidadNotificacion.NoConformidad, EntidadNotificacion.EscalacionProveedores, EntidadNotificacion.Encuesta, EntidadNotificacion.SolicitudQA
                        PuedeVer = FSNUser.AccesoNotificacionesQA
                    Case EntidadNotificacion.ProcesoCompra, EntidadNotificacion.PedDirecto
                        PuedeVer = FSNUser.AccesoNotificacionesGS
                    Case EntidadNotificacion.Factura
                        PuedeVer = FSNUser.AccesoNotificacionesIM
                    Case EntidadNotificacion.Contrato
                        PuedeVer = FSNUser.AccesoNotificacionesPM
                    Case EntidadNotificacion.Orden, EntidadNotificacion.Recepcion
                        PuedeVer = FSNUser.AccesoNotificacionesEP
                    Case EntidadNotificacion.Solicitud
                        PuedeVer = FSNUser.AccesoNotificacionesGS Or FSNUser.AccesoNotificacionesPM
                    Case Else
                        PuedeVer = False
                End Select

                If Not PuedeVer Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Cierra", "<script>window.close();</script>")
                Else
                    Instancia.Value = oEmail.Instancia
                    Prove.Value = oEmail.Prove
                    isHTML.Value = oEmail.IsHTML

                    txtPara.Text = oEmail.Para

                    TipoEmail.Value = oEmail.TipoEMail
                    EntidadEmail.Value = oEmail.EntidadEMail

                    txtEmailHtml.ActiveMode = AjaxControlToolkit.HTMLEditor.ActiveModeType.Preview
                    txtEmailHtml.Content = oEmail.Cuerpo
                    txtAsunto.Text = oEmail.Asunto
                End If
            End If
        End If
    End Sub

    <System.Web.Services.WebMethodAttribute(),
    System.Web.Script.Services.ScriptMethodAttribute()>
    Public Shared Sub EnviarMail(ByVal Asunto As String, ByVal Para As String, ByVal Cuerpo As String, ByVal isHTML As Boolean, ByVal CodProve As String, ByVal Instancia As Long, ByVal TipoEmail As Integer, ByVal EntidadEmail As Integer)
        Dim oEmail As FSNServer.Email
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        oEmail = FSNServer.Get_Object(GetType(FSNServer.Email))

        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        oEmail.EnvioMail(FSNUser.Email, Asunto, Para, Cuerpo, isHTML, CodProve, Instancia, TipoEmail, EntidadEmail, FSNUser.Idioma)
    End Sub
End Class