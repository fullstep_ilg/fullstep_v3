﻿Public Class downloadfile
    Inherits FSNPage
    ''' <summary>
    ''' Carga la pantalla. Si el path pasado no es valido, no se descarga de nada
    ''' </summary>
    ''' <param name="sender">pantalla</param>
    ''' <param name="e">parametro de sistema</param>    
    ''' <remarks>Llamada desde: CN\js\cn_mensajes_ui.js       Reuniones\js\VisorPlantillasReunion_init.js        _common\downloadhidden.aspx; Tiempo máximo: 0,2</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sFichero As String
        Dim filename As String
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim byteBuffer() As Byte
        If Request.QueryString("e") Is Nothing Then
            Dim ocnMensajes As FSNServer.cnMensajes
            ocnMensajes = FSNServer.Get_Object(GetType(FSNServer.cnMensajes))
            filename = ocnMensajes.Obtener_Nombre_Adjunto(Request.QueryString("f"))
            byteBuffer = ocnMensajes.CN_Mensajes_GetAdjuntoMensaje(Request.QueryString("f"))
        Else
            sFichero = Server.UrlDecode(Request.QueryString("f"))
            filename = Split(Replace(sFichero, ConfigurationManager.AppSettings("temp") & "\", ""), "\")(1)
            Dim input As System.IO.FileStream = New System.IO.FileStream(sFichero, System.IO.FileMode.Open, System.IO.FileAccess.Read)
            Dim buffer(input.Length - 1) As Byte
            input.Read(buffer, 0, buffer.Length)
            input.Close()
            byteBuffer = buffer
        End If
        If byteBuffer IsNot Nothing Then
            Response.ClearContent()
            Response.ClearHeaders()
            Response.Clear()
            Call Me.Response.AddHeader("Content-Type", "application/octet-stream")
            Call Me.Response.AddHeader("Content-Disposition", "attachment; filename=""" & filename & """")
            Call Me.Response.BinaryWrite(byteBuffer)
            Call Me.Response.End()
        End If
    End Sub
End Class