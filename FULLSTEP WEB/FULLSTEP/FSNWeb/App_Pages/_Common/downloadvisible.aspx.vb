
Public Class downloadvisible
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents txtSize As System.Web.UI.WebControls.Label
    Protected WithEvents lblInstruc1 As System.Web.UI.WebControls.Label
    Protected WithEvents lblInstruc2 As System.Web.UI.WebControls.Label
    Protected WithEvents lnkDownload1 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents lnkDownload2 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents lblFichero As System.Web.UI.WebControls.Label
    Protected WithEvents txtFichero As System.Web.UI.WebControls.Label
    Protected WithEvents lblSize As System.Web.UI.WebControls.Label
    Protected WithEvents pnlDescarga As System.Web.UI.WebControls.Panel
    Protected WithEvents imgAlerta As System.Web.UI.WebControls.Image
    Protected WithEvents pnlNODescarga As System.Web.UI.WebControls.Panel
    Protected WithEvents lblTituloNoDescarga As System.Web.UI.WebControls.Label
    Protected WithEvents btnCerrar As Global.System.Web.UI.WebControls.Button
    Protected WithEvents LblTituloDescargaArchivo As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.DownloadFile

        If Request("descargable") Is Nothing OrElse Request("descargable") <> 0 Then
            If Server.UrlDecode(Request("nombre")) = "MemoriaJustificativa.pdf" And Not Request("MemoriaJustificativa") Is Nothing Then
                Me.txtFichero.Text = "MemoriaJustificativa.pdf"
                Me.lblTitulo.Text = Textos(0)
                Me.lblFichero.Text = Textos(1)
                Me.lblSize.Text = Textos(2)
                Me.lblInstruc1.Text = Textos(3)
                Me.lblInstruc2.Text = Textos(4)
            Else
                Me.txtFichero.Text = Server.UrlDecode(Request("nombre"))
                Me.lblTitulo.Text = Textos(0)
                Me.lblFichero.Text = Textos(1)
                Me.lblSize.Text = Textos(2)
                Me.lblInstruc1.Text = Textos(3)
                Me.lblInstruc2.Text = Textos(4)
                Me.txtSize.Text = FSNLibrary.FormatNumber(CDbl(Request("datasize")) / 1024, Usuario.NumberFormat) & " kb"
                Me.lnkDownload1.ImageUrl = ConfigurationManager.AppSettings("ruta") & "images/download.gif"
                Me.lnkDownload2.ImageUrl = ConfigurationManager.AppSettings("ruta") & "images/download.gif"
                Me.lnkDownload1.NavigateUrl = ConfigurationManager.AppSettings("ruta") + "/download/" + Request("path") + "/" + Server.UrlDecode(Request("nombre"))
                Me.lnkDownload2.NavigateUrl = ConfigurationManager.AppSettings("ruta") + "/download/" + Request("path") + "/" + Server.UrlDecode(Request("nombre"))
            End If
        Else
            Me.pnlDescarga.Visible = False
            Me.pnlNODescarga.Visible = True
            Me.imgAlerta.Visible = True
            Me.imgAlerta.ImageUrl = "images/Img_Ico_Alerta.gif"
            Me.lblTituloNoDescarga.Text = Textos(6) '"�No tiene permisos para descargar los ficheros adjuntos al contrato!"
            Me.btnCerrar.Text = Textos(7) ' "CERRAR"
            Me.LblTituloDescargaArchivo.Text = Textos(5) '"Descarga Archivo Contrato"
        End If
    End Sub

    Private Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Server.UrlDecode(Request("nombre")) = "MemoriaJustificativa.pdf" And Not Request("MemoriaJustificativa") Is Nothing Then
            Dim sPathMemoriaJustificativa As String = ""
            If Not Session("MemoriaJustificativa") = Nothing Then
                sPathMemoriaJustificativa = Session("MemoriaJustificativa").ToString
                Dim sFicheroMemoriaJustificativa As String = sPathMemoriaJustificativa.Substring(sPathMemoriaJustificativa.IndexOf("\") + 1, sPathMemoriaJustificativa.Length - sPathMemoriaJustificativa.IndexOf("\") - 1)
            End If
            Me.txtFichero.Text = "MemoriaJustificativa.pdf"
            Me.txtSize.Text = FSNLibrary.FormatNumber(CDbl(Session("MemoriaJustificativa_DataSize").ToString), Usuario.NumberFormat) & " kb"
            Me.lnkDownload1.NavigateUrl = ConfigurationManager.AppSettings("rutaPM") + "/download/" + sPathMemoriaJustificativa
            Me.lnkDownload2.NavigateUrl = ConfigurationManager.AppSettings("rutaPM") + "/download/" + sPathMemoriaJustificativa
            Session("MemoriaJustificativa_DataSize") = Nothing
            Session("MemoriaJustificativa") = Nothing
        End If
    End Sub

End Class

