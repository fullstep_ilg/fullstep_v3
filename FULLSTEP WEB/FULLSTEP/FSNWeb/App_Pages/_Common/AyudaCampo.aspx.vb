﻿Public Partial Class AyudaCampo
    Inherits FSNPage

    ''' <summary>
    ''' Carga la pagina
    ''' </summary>
    ''' <param name="sender">pagina</param>
    ''' <param name="e">evento</param>
    ''' <remarks>Llamada desde: Sistema; Tiempo máximo:0</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim idCampo As Integer = CInt(Request("Campo"))
        Dim lInstancia As Long
        Dim lSolicitud As Long
        Dim oCampo As FSNServer.Campo

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.AyudaCampo

        If Request("Instancia") <> Nothing Then lInstancia = Request("Instancia")
        If Request("Solicitud") <> Nothing Then lSolicitud = Request("Solicitud")

        oCampo = FSNServer.Get_Object(GetType(FSNServer.Campo))
        oCampo.Id = idCampo
        If lInstancia > 0 Then
            oCampo.LoadInst(lInstancia, Idioma)
        Else
            oCampo.Load(Idioma, lSolicitud)
        End If

        Me.lblTitulo.Text = Textos(0) & " " & oCampo.DenSolicitud(Idioma)
        Me.lblSubtitulo.Text = Textos(1) & " " & oCampo.DenGrupo(Idioma) & " - " & Textos(2) & " " & oCampo.Den(Idioma)
        Me.lblAyuda.Text = VB2HTML(oCampo.Ayuda(Idioma))

        If oCampo.Formula <> Nothing Then
            Dim oDS As DataSet

            If lInstancia = Nothing Then
                Dim oSolicitud As FSNServer.Solicitud
                oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
                oSolicitud.ID = oCampo.IdSolicitud
                oSolicitud.Load(Idioma)
                If oCampo.OrigenCalculo = Nothing And oCampo.CampoPadre = Nothing Then
                    If oSolicitud.TipoSolicit = TiposDeDatos.TipoDeSolicitud.Certificado Or oSolicitud.TipoSolicit = TiposDeDatos.TipoDeSolicitud.NoConformidad Then
                        oDS = oSolicitud.Formulario.LoadCamposCalculados(oSolicitud.ID)
                    Else
                        oDS = oSolicitud.Formulario.LoadCamposCalculados(oSolicitud.ID, FSNUser.CodPersona)
                    End If
                Else
                    Dim oCampoDesg As FSNServer.Campo
                    oCampoDesg = FSNServer.Get_Object(GetType(FSNServer.Campo))
                    If oCampo.CampoPadre <> Nothing Then
                        oCampoDesg.Id = oCampo.CampoPadre
                    Else
                        oCampoDesg.Id = oCampo.OrigenCalculo
                    End If
                    oDS = oCampoDesg.LoadCalculados(oSolicitud.ID, FSNUser.CodPersona)
                End If
            Else
                Dim oInstancia As FSNServer.Instancia
                oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                oInstancia.ID = lInstancia
                oInstancia.Load(Idioma)

                If oCampo.OrigenCalculo = Nothing And oCampo.CampoPadre = Nothing Then
                    If oInstancia.Solicitud.TipoSolicit = TiposDeDatos.TipoDeSolicitud.Certificado Or oInstancia.Solicitud.TipoSolicit = TiposDeDatos.TipoDeSolicitud.NoConformidad Then
                        oDS = oInstancia.LoadCamposCalculados(bWorkflow:=False)
                    Else
                        oDS = oInstancia.LoadCamposCalculados(sUsuario:=FSNUser.CodPersona)
                    End If
                Else
                    Dim oCampoDesg As FSNServer.Campo
                    oCampoDesg = FSNServer.Get_Object(GetType(FSNServer.Campo))
                    If oCampo.CampoPadre <> Nothing Then
                        oCampoDesg.Id = oCampo.CampoPadre
                    Else
                        oCampoDesg.Id = oCampo.OrigenCalculo
                    End If
                    oDS = oCampoDesg.LoadInstCalculados(lInstancia, FSNUser.CodPersona)
                End If
                oInstancia = Nothing
            End If

            Dim sFormulaAux As String = oCampo.Formula
            Dim sFormula As String = ""
            Dim FaltaCerrar As Boolean = False
            Dim PosCar As Integer = 0

            For Each car As Char In sFormulaAux
                If car = " " Then 'Quita blancos y Mete ##delante y detras de cada variable
                ElseIf car = "Y" OrElse car = "X" Then 'Y->desglose X->campo
                    'Ejemplo operadores:+-*/%^<>=!&|ANDORNOT~()ABSSENCOSTAN
                    Try
                        If Not IsNumeric(sFormulaAux.Chars(PosCar + 1)) Then
                            'no debería pasar No hay operadores q incluyan x ó y , por lo menos a 16/10/2014
                            sFormula = sFormula & car
                        Else
                            sFormula = sFormula & "##" & car
                            FaltaCerrar = True
                        End If
                    Catch ex As Exception
                        'x ó y es el ultimo caracter del string
                        sFormula = sFormula & car
                    End Try
                ElseIf FaltaCerrar And Not IsNumeric(car) Then
                    sFormula = sFormula & "##" & car
                    FaltaCerrar = False
                Else
                    sFormula = sFormula & car
                End If
                PosCar = PosCar + 1
            Next
            If FaltaCerrar Then sFormula = sFormula & "##"

            For Each oRow As DataRow In oDS.Tables(0).Rows
                sFormula = ReplaceExacto(sFormula, oRow.Item("ID_CALCULO"), " " + oRow.Item("DEN_" + Idioma) + " ")
            Next

            Me.hrTop.Visible = True
            Me.hrBottom.Visible = True
            Me.lblFormulaCalculo.Visible = True
            Me.lblFormula.Visible = True
            Me.lblFormulaCalculo.Text = Textos(3)
            Me.lblFormula.Text = sFormula
        Else
            Me.lblFormulaCalculo.Visible = False
            Me.lblFormula.Visible = False
        End If

    End Sub

    ''' <summary>
    ''' Reemplaza en la formula la variable indicada por el texto indicado, de formar parte de la formula.
    ''' </summary>
    ''' <param name="sFormula">formula</param>
    ''' <param name="Variable">variable</param>
    ''' <param name="Texto">texto</param>
    ''' <returns>formula donde se ha reemplazado la variable indicada por el texto indicado, de formar parte de la formula.</returns>
    ''' <remarks>Llamada desde: Form_Load; Tiempo máximo:0</remarks>
    Private Function ReplaceExacto(ByVal sFormula As String, ByVal Variable As String, ByVal Texto As String) As String
        Dim PosCar As Integer
        Dim Resul As String = ""

        Dim sFormulaAux As String
        Dim carAux As Char
        Dim i As Integer
        Dim bHaIdoBien As Boolean

        If InStr(sFormula, Variable) > 0 Then
            PosCar = -1
            For Each car As Char In sFormula
                PosCar = PosCar + 1

                If car = Variable.Chars(0) Then
                    sFormulaAux = Right(sFormula, Len(sFormula) - PosCar)
                    i = 0
                    bHaIdoBien = True

                    For Each car2 As Char In Variable
                        carAux = sFormulaAux.Chars(i)

                        If carAux = car2 Then
                        Else
                            bHaIdoBien = False
                            Exit For
                        End If

                        i = i + 1
                    Next

                    If bHaIdoBien Then
                        Try
                            carAux = sFormulaAux.Chars(i)
                        Catch ex As Exception
                            'Ok: Caso Y21*100/Y9 cuando variable es Y9

                            sFormula = Replace(sFormula, Variable, " " + Texto + " ")
                            ReplaceExacto = sFormula

                            Exit Function
                        End Try

                        If Not IsNumeric(carAux) Then 'Ejemplo:+-*/%^<>=!&|ANDORNOT~()ABSSENCOSTAN
                            sFormula = Replace(sFormula, "##" & Variable & "##", " " + Texto + " ")
                            ReplaceExacto = sFormula
                            Exit Function
                        End If
                    End If
                End If

                Resul = Resul & car
            Next
        Else
            Resul = sFormula
        End If

        ReplaceExacto = Resul
    End Function
End Class