﻿Public Class UnidadesOrganizativas
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oUser As FSNServer.User = Session("FSN_User")
        Dim sIdi As String = oUser.Idioma.ToString()

        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        'Textos de la página:
        Dim oDict As FSNServer.Dictionary = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.UnidadesOrganizativas, sIdi)
        Dim oTextos As DataTable = oDict.Data.Tables(0)

        cmdAceptar.Value = oTextos.Rows(1).Item(1)
        cmdCancelar.Value = oTextos.Rows(2).Item(1)

        IDCONTROL.Value = Request("IdControl")
        IDCONTROLVALUE.Value = Request("IdControlValue")
        desde.Value = Request("desde")

        Dim oUnidadesOrg As FSNServer.UnidadesOrg
        oUnidadesOrg = FSWSServer.Get_Object(GetType(FSNServer.UnidadesOrg))
        oUnidadesOrg.CargarTodasUnidadesOrganizativas(FSNUser.Cod, sIdi, _
                FSNUser.PMRestriccionUONSolicitudAUONUsuario, _
                FSNUser.PMRestriccionUONSolicitudAUONPerfil)

        With wdtUnidadesOrganizativas
            .DataSource = oUnidadesOrg.Data
            .DataBind()
        End With

        Dim sValor As String = Request("Valor")
        Dim sUnidadOrg As String() = Split(sValor, " - ")
        Dim sUON1 As String = String.Empty
        Dim sUON2 As String = String.Empty
        Dim sUON3 As String = String.Empty
        Dim sDen As String = String.Empty
        Dim iNivel As Integer = UBound(sUnidadOrg)
        Select Case iNivel
            Case 0
                sDen = sValor
            Case 1
                sUON1 = sUnidadOrg(0)
            Case 2
                sUON1 = sUnidadOrg(0)
                sUON2 = sUnidadOrg(1)
            Case 3
                sUON1 = sUnidadOrg(0)
                sUON2 = sUnidadOrg(1)
                sUON3 = sUnidadOrg(2)
        End Select

        For Each node As Infragistics.Web.UI.NavigationControls.DataTreeNode In wdtUnidadesOrganizativas.AllNodes
            node.Expanded = Not node.Enabled
            If node.Level = iNivel Then
                Select Case iNivel
                    Case 1
                        If node.Key = sUON1 Then
                            node.Expanded = True
                            node.Selected = True
                        End If
                    Case 2
                        If node.ParentNode.Key = sUON1 AndAlso node.Key = sUON2 Then
                            node.ParentNode.Expanded = True
                            node.Expanded = True
                            node.Selected = True
                        End If
                    Case 3
                        If node.ParentNode.ParentNode.Key = sUON1 AndAlso node.ParentNode.Key = sUON2 AndAlso node.Key = sUON3 Then
                            node.ParentNode.ParentNode.Expanded = True
                            node.ParentNode.Expanded = True
                            node.Expanded = True
                            node.Selected = True
                        End If
                End Select
            End If
        Next

        Dim sClientTexts As String
        sClientTexts = ""
        sClientTexts += "var arrTextosML = new Array();"
        sClientTexts += "arrTextosML[0] = '" + JSText(oTextos.Rows(3).Item(1)) + "';"
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts, True)

        oTextos = Nothing
        oUnidadesOrg = Nothing
    End Sub

    Private Sub wdtUnidadesOrganizativas_NodeBound(sender As Object, e As Infragistics.Web.UI.NavigationControls.DataTreeNodeEventArgs) Handles wdtUnidadesOrganizativas.NodeBound
        e.Node.Enabled = CType(e.Node.DataItem.Item("ACTIVA"), Boolean)
        Select Case e.Node.Level
            Case 0
                e.Node.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "Images/world.gif"
            Case 1
                e.Node.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "Images/carpetaAzulOscuro.gif"
            Case 2
                e.Node.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "Images/carpetaCommodityTxiki.gif"
            Case Else
                e.Node.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "Images/carpetagris.gif"
        End Select
    End Sub
End Class