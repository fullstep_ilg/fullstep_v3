﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization

<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Error_Handler
    Inherits System.Web.Services.WebService
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Guardar_Error(ByVal App_Error As Object) As Object
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oError As FSNServer.Errores = FSNServer.Get_Object(GetType(FSNServer.Errores))
            Dim sv_Http_User_Agent As String = HttpContext.Current.Request.ServerVariables("HTTP_USER_AGENT")
            Dim sUrlReferrer As String = HttpContext.Current.Request.UrlReferrer.ToString
            Dim serializer As New JavaScriptSerializer()
            oError.Create(sUrlReferrer, Usuario.Cod, App_Error("ExceptionType"), App_Error("Message"), App_Error("StackTrace"), String.Empty, sv_Http_User_Agent)

            Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
            Dim oTextos As DataTable
            oDict.LoadData(TiposDeDatos.ModulosIdiomas.PaginaError, Usuario.Idioma)
            oTextos = oDict.Data.Tables(0)

            Dim textos As New Dictionary(Of String, String)
            textos("lblTituloError") = oTextos.Rows(0).Item(1)
            textos("lblSubTitulo1") = oTextos.Rows(1).Item(1)
            textos("lEnlaceFSGE") = oTextos.Rows(2).Item(1)
            textos("lblSubTitulo2") = oTextos.Rows(3).Item(1)
            textos("lblTelefono") = oTextos.Rows(4).Item(1)
            textos("lblMail") = oTextos.Rows(5).Item(1)
            textos("lblHorario") = oTextos.Rows(6).Item(1)
            textos("lblIdentificadorError") = oTextos.Rows(7).Item(1)
            textos("lblIdError") = oError.ID
            textos("lblAceptarErrorAlert") = oTextos.Rows(10).Item(1)

            Return textos
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class