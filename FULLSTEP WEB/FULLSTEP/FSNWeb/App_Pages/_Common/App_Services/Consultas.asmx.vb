﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web
Imports System.Text
Imports System.Runtime.Serialization.Json
Imports System.IO
Imports System.Web.Script.Services

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<System.Web.Script.Services.ScriptService()> _
<ToolboxItem(False)> _
Public Class Consultas
    Inherits System.Web.Services.WebService
#Region "Persona"
    ''' <summary>
    ''' Función que se encarga de rellenar los datos de detalle de una persona
    ''' </summary>
    ''' <param name="contextKey">Código de persona</param>
    ''' <returns>Un String con los datos de la persona en formato html</returns>
    <System.Web.Services.WebMethod(True)>
    Public Function Obtener_DatosPersona(ByVal contextKey As System.String) As System.String
        Dim oFSUsuario As FSNServer.User = Session("FSN_USER")
        Dim oFSServer As FSNServer.Root = Session("FSN_Server")
        Dim oPer As FSNServer.Personas
        Dim oUsu As FSNServer.User

        oPer = oFSServer.Get_Object(GetType(FSNServer.Personas))

        oPer.LoadData(contextKey)

        Dim oDict As FSNServer.Dictionary = oFSServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.DetallePersona, oFSUsuario.Idioma)
        Dim sr As New StringBuilder()
        If oPer.Data.Tables.Count > 0 AndAlso oPer.Data.Tables(0).Rows.Count > 0 Then
            With oPer.Data.Tables(0).Rows(0)
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(0).Item(1) & ":</b> " & .Item("COD") & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(1).Item(1) & ":</b> " & .Item("NOM") & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(2).Item(1) & ":</b> " & .Item("APE") & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(3).Item(1) & ":</b> " & .Item("CAR") & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(4).Item(1) & ":</b> " &
                            "<a class=""Normal"" href=""callto:" & .Item("TFNO") & """>" & .Item("TFNO") & "</a><br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(5).Item(1) & ":</b> " & .Item("FAX") & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(6).Item(1) & ":</b> " &
                          "<a class=""Normal"" href=""mailto:" & .Item("EMAIL") & """>" & .Item("EMAIL") & "</a><br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(7).Item(1) & ":</b> " & .Item("NOM_DEP") & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(8).Item(1) & ":</b> " & .Item("NOM_UO") & "<br/>")
            End With
        Else
            'Si no hemos obtenido los datos de la persona, probamos a cargar el objeto User por si lo que nos hubiesen pasado seria el codigo de usuario
            oUsu = oFSServer.Get_Object(GetType(FSNServer.User))
            oUsu.LoadUserData(contextKey)
            If oUsu.CodPersona <> "" Then
                'Una vez con el codigo de la persona cargamos sus datos
                oPer.LoadData(oUsu.CodPersona)
                With oPer.Data.Tables(0).Rows(0)
                    sr.Append("<b>" & oDict.Data.Tables(0).Rows(0).Item(1) & ":</b> " & .Item("COD") & "<br/>")
                    sr.Append("<b>" & oDict.Data.Tables(0).Rows(1).Item(1) & ":</b> " & .Item("NOM") & "<br/>")
                    sr.Append("<b>" & oDict.Data.Tables(0).Rows(2).Item(1) & ":</b> " & .Item("APE") & "<br/>")
                    sr.Append("<b>" & oDict.Data.Tables(0).Rows(3).Item(1) & ":</b> " & .Item("CAR") & "<br/>")
                    sr.Append("<b>" & oDict.Data.Tables(0).Rows(4).Item(1) & ":</b> " &
                                "<a class=""Normal"" href=""callto:" & .Item("TFNO") & """>" & .Item("TFNO") & "</a><br/>")
                    sr.Append("<b>" & oDict.Data.Tables(0).Rows(5).Item(1) & ":</b> " & .Item("FAX") & "<br/>")
                    sr.Append("<b>" & oDict.Data.Tables(0).Rows(6).Item(1) & ":</b> " &
                              "<a class=""Normal"" href=""mailto:" & .Item("EMAIL") & """>" & .Item("EMAIL") & "</a><br/>")
                    sr.Append("<b>" & oDict.Data.Tables(0).Rows(7).Item(1) & ":</b> " & .Item("NOM_DEP") & "<br/>")
                    sr.Append("<b>" & oDict.Data.Tables(0).Rows(8).Item(1) & ":</b> " & .Item("NOM_UO") & "<br/>")
                End With
            End If
        End If
        Return sr.ToString()
    End Function
#End Region
#Region "Proveedor"
    ''' <summary>
    ''' Función que se encarga de rellenar los datos de detalle de un proveedor
    ''' </summary>
    ''' <param name="contextKey">Código de proveedor</param>
    ''' <returns>Un String con los datos del proveedor en formato html</returns>
    <System.Web.Services.WebMethod(True)>
    Public Function Obtener_DatosProveedor(ByVal contextKey As System.String) As System.String
        Dim oFSUsuario As FSNServer.User = Session("FSN_USER")
        Dim oFSServer As FSNServer.Root = Session("FSN_Server")
        Dim oProve As FSNServer.Proveedor

        oProve = oFSServer.Get_Object(GetType(FSNServer.Proveedor))
        oProve.Cod = contextKey
        oProve.Load(oFSUsuario.Idioma)

        Dim oDict As FSNServer.Dictionary = oFSServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.DetalleProveedor, oFSUsuario.Idioma)
        Dim sr As New StringBuilder()

        sr.Append("<b>" & oDict.Data.Tables(0).Rows(0).Item(1) & ":</b> " & oProve.Cod & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(1).Item(1) & ":</b> " & oProve.Den & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(2).Item(1) & ":</b> " & oProve.NIF & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(3).Item(1) & ":</b> " & oProve.Dir & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(4).Item(1) & ":</b> " & oProve.CP & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(5).Item(1) & ":</b> " & oProve.Pob & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(6).Item(1) & ":</b> " & oProve.PaiDen & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(7).Item(1) & ":</b> " & oProve.ProviDen & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(8).Item(1) & ":</b> " & oProve.MonCod & " - " & DBNullToSomething(oProve.MonDen) & "<br/>")

        Return sr.ToString()
    End Function
#End Region
#Region "ProveedorQA"
    ''' <summary>
    ''' Función que se encarga de rellenar los datos de detalle de un proveedor
    ''' </summary>
    ''' <param name="contextKey">Código de proveedor</param>
    ''' <returns>Un String con los datos del proveedor en formato html</returns>
    <System.Web.Services.WebMethod(True)>
    Public Function Obtener_DatosProveedorQA(ByVal contextKey As System.String) As System.String
        Dim oFSUsuario As FSNServer.User = Session("FSN_USER")
        Dim oFSServer As FSNServer.Root = Session("FSN_Server")
        Dim oProve As FSNServer.Proveedor

        oProve = oFSServer.Get_Object(GetType(FSNServer.Proveedor))
        oProve.Cod = contextKey
        oProve.LoadDatosQA(oFSUsuario.Idioma)

        Dim oDict As FSNServer.Dictionary = oFSServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.DetalleProveedor, oFSUsuario.Idioma)
        Dim sr As New StringBuilder()

        sr.Append("<b>" & oDict.Data.Tables(0).Rows(0).Item(1) & ":</b> " & oProve.Cod & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(1).Item(1) & ":</b> " & oProve.Den & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(2).Item(1) & ":</b> " & oProve.NIF & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(3).Item(1) & ":</b> " & oProve.Dir & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(4).Item(1) & ":</b> " & oProve.CP & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(5).Item(1) & ":</b> " & oProve.Pob & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(6).Item(1) & ":</b> " & oProve.PaiDen & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(7).Item(1) & ":</b> " & oProve.ProviDen & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(8).Item(1) & ":</b> " & oProve.MonCod & " - " & DBNullToSomething(oProve.MonDen) & "<br/>")

        sr.Append("<b>" & oDict.Data.Tables(0).Rows(11).Item(1) & ": </b>" &
                          "<a class=""Normal"" href=""mailto:" & oProve.Email & """>" & oProve.Email & "</a><br/>")

        Return sr.ToString()
    End Function
#End Region
#Region "Comentarios de NoConformidad"
    ''' <summary>
    ''' Función que se encarga de rellenar el comentario de una no conformidad
    ''' </summary>
    ''' <param name="contextKey">String que contiene 2 parametros (Instancia#iComentarios)
    ''' iComentarios (int): Celda de comentarios que hemos pulsado 
    '''                            (0 imagen comentarios, mostrar todos)
    '''                            (1) Comentario alta
    '''                            (2) Comentario cierre
    '''                            (3) Comentario revisor    
    ''' lInstancia (long): Id de la instancia</param> 
    ''' <returns>Un String con los datos de una no conformidad en formato html</returns>
    <System.Web.Services.WebMethod(True)> _
    Public Function Obtener_ComentarioQA(ByVal contextKey As System.String) As System.String

        Dim oFSUsuario As FSNServer.User = Session("FSN_USER")
        Dim oFSServer As FSNServer.Root = Session("FSN_Server")
        Dim oDict As FSNServer.Dictionary = oFSServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.VisorNoConformidades, oFSUsuario.Idioma)
        Dim arrTextosComentarios(8) As String
        arrTextosComentarios(0) = oDict.Data.Tables(0).Rows(34).Item(1) '"Comentario apertura"
        arrTextosComentarios(1) = oDict.Data.Tables(0).Rows(44).Item(1) '"Usuario"
        arrTextosComentarios(2) = oDict.Data.Tables(0).Rows(35).Item(1) '"Comentario cierre"
        arrTextosComentarios(3) = oDict.Data.Tables(0).Rows(36).Item(1) '"Comentario revisor"
        arrTextosComentarios(4) = oDict.Data.Tables(0).Rows(26).Item(1) '"Revisor"
        arrTextosComentarios(5) = oDict.Data.Tables(0).Rows(43).Item(1) '"Fecha"
        arrTextosComentarios(6) = oDict.Data.Tables(0).Rows(64).Item(1) '"Informacion Detallada"
        arrTextosComentarios(7) = oDict.Data.Tables(0).Rows(65).Item(1) '"Cerrar detalle"
        arrTextosComentarios(8) = oDict.Data.Tables(0).Rows(68).Item(1) '"Comentario anulación"

        Dim oNoConformidad As FSNServer.NoConformidad
        Dim iComentarios As Integer
        Dim lInstancia As Long
        Dim arrparametros As String()
        arrparametros = Split(contextKey, "#")
        iComentarios = strToInt(arrparametros(0))
        lInstancia = strToInt(arrparametros(1))

        Dim mi_array As New ArrayList
        Dim arr_Elementos As ArrayList
        Dim bComentario As Boolean = False
        Dim ds As DataSet = Nothing
        Dim oRow As DataRow
        oNoConformidad = oFSServer.Get_Object(GetType(FSNServer.NoConformidad))
        ds = oNoConformidad.CargarComentarios(lInstancia, iComentarios)
        oNoConformidad = Nothing
        For Each oRow In ds.Tables(0).Rows
            arr_Elementos = New ArrayList
            bComentario = True
            If DBNullToDbl(oRow.Item("TIPO")) = 1 OrElse DBNullToDbl(oRow.Item("TIPO")) = 0 Then
                'Comentario Apertura
                arr_Elementos.Add(arrTextosComentarios(0) & ":") '("Comentario apertura:")
                arr_Elementos.Add(oRow.Item("COMENT"))
                arr_Elementos.Add(FormatDate(oRow.Item("FECHA"), oFSUsuario.DateFormat))
                arr_Elementos.Add(arrTextosComentarios(1) & ": ") '("Usuario:")
            End If

            'Comentario Cierre
            If DBNullToDbl(oRow.Item("TIPO")) = 2 Then
                arr_Elementos.Add(arrTextosComentarios(2) & ":") '("Comentario cierre:")
                arr_Elementos.Add(oRow.Item("COMENT"))
                arr_Elementos.Add(FormatDate(oRow.Item("FECHA"), oFSUsuario.DateFormat))
                arr_Elementos.Add(arrTextosComentarios(1) & ": ") '("Usuario:")
            End If
            'Comentario Revisor
            If DBNullToDbl(oRow.Item("TIPO")) = 3 Then
                arr_Elementos = New ArrayList
                arr_Elementos.Add(arrTextosComentarios(3) & ":") '("Comentario revisor:")
                arr_Elementos.Add(oRow.Item("COMENT"))
                arr_Elementos.Add(FormatDate(oRow.Item("FECHA"), oFSUsuario.DateFormat))
                arr_Elementos.Add(arrTextosComentarios(4) & ": ") '("Revisor:")
            End If
            'Comentario Anulacion
            If DBNullToDbl(oRow.Item("TIPO")) = 4 Then
                arr_Elementos = New ArrayList
                arr_Elementos.Add(arrTextosComentarios(8) & ":") '("Comentario anulación:")
                arr_Elementos.Add(oRow.Item("COMENT"))
                arr_Elementos.Add(FormatDate(oRow.Item("FECHA"), oFSUsuario.DateFormat))
                arr_Elementos.Add(arrTextosComentarios(1) & ": ") '("Usuario:")
            End If
            arr_Elementos.Add(oRow.Item("NOMBRE"))
            mi_array.Add(arr_Elementos)
        Next

        Dim sr As New StringBuilder()
        sr.Append("<table id=""tblcomentarios"" border=""0"" cellpadding=""0"" cellspacing=""0"" style="" width: 100%; background-color: white; padding-left: 10px; padding-right: 10px"">")
        For i = 0 To mi_array.Count - 1
            If i > 0 Then
                sr.Append("<tr><td style=""padding-top:5px; border-top: 1px dotted #868686;""><span class=""Rotulo""><b>" & mi_array(i)(0) & "</b></span>") 'Comentario apertura:
            Else
                sr.Append("<tr><td style=""padding-top:5px;""><span class=""Rotulo""><b>" & mi_array(i)(0) & "</b></span>") 'Comentario apertura:
            End If
            sr.Append("</td></tr>")
            sr.Append("<tr><td style=""padding-top:5px;""><span>" & mi_array(i)(1) & "</span>") 'oNoConformidad.ComentAlta
            sr.Append("</td></tr>")
            sr.Append("<tr><td style=""padding-top:5px;""><span class=""Rotulo""><b>" & arrTextosComentarios(5) & ": </b></span><span>" & mi_array(i)(2) & "</span>") 'Fecha: FormatDate(oNoConformidad.FechaComentAlta, FSNUser.DateFormat)
            sr.Append("</td></tr>")
            sr.Append("<tr><td style=""padding-top:5px; padding-bottom:10px;""><span class=""Rotulo""><b>" & mi_array(i)(3) & "</b></span><span>" & mi_array(i)(4) & "</span>") 'Usuario: oNoConformidad.NombreComentAlta
            sr.Append("</td></tr>")
        Next
        sr.Append("</table>")
        Return sr.ToString()
    End Function
#End Region
#Region "Peticionarios"
    ''' Revisado por:blp. Fecha: 30/08/2013
    ''' <summary>
    ''' Devuelve los peticionarios del usuario
    ''' </summary>
    ''' <param name="Filtro">texto para filtrar los peticionarios a devolver</param>
    ''' <returns>String con las observaciones</returns>
    ''' <remarks>Llamadas desde Seguimiento.aspx. Máx. 0,1 seg.</remarks>
    <System.Web.Services.WebMethod(BufferResponse:=True, enableSession:=True)>
    Public Function DevolverPeticionarios(ByVal Filtro As String) As String
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        If oUsuario Is Nothing Then Return String.Empty

        Dim dsPeticionarios As DataSet = Nothing
        Dim lstPeticionarios As New List(Of Peticionario)
        Dim cRol As FSNServer.Rol = oServer.Get_Object(GetType(FSNServer.Rol))
        Dim sCacheKey As String = String.Empty

        'Se recuperan TODOS LOS DATOS de peticionarios y se guardan en caché. Sobre esa caché se filtra.
        sCacheKey = "DsetPeticionariosPM" & "_" & oUsuario.CodPersona.ToString() & "_" & oUsuario.Idioma.ToString()
        If HttpContext.Current.Cache(sCacheKey) Is Nothing Then
            dsPeticionarios = cRol.CargarPeticionariosWebService(oUsuario.CodPersona) ', Filtro)
            'Vamos a crear una estructura de Peticionario para poder serializar los valores a json
            If dsPeticionarios IsNot Nothing AndAlso dsPeticionarios.Tables(0) IsNot Nothing AndAlso dsPeticionarios.Tables(0).Rows.Count > 0 Then
                'Recorremos las Peticionarios
                For Each Peticionario As DataRow In dsPeticionarios.Tables(0).Rows
                    Dim oPeticionario As New Peticionario
                    oPeticionario.COD = null2str(Peticionario("COD"))
                    oPeticionario.DEN = null2str(Peticionario("DEN"))
                    lstPeticionarios.Add(oPeticionario)
                Next
                HttpContext.Current.Cache.Insert(sCacheKey, lstPeticionarios, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            Else
                Return String.Empty
            End If
        Else
            lstPeticionarios = HttpContext.Current.Cache(sCacheKey)
        End If

        'Filtrado
        If Filtro.Length > 0 Then
            'Dim arrPeticionarios As Peticionario()
            Dim query = From petic As Peticionario In lstPeticionarios
                        Where UCase(petic.COD).Contains(UCase(Filtro)) OrElse UCase(petic.DEN).Contains(UCase(Filtro))
                        Select petic
            'lstPeticionarios = arrPeticionarios.ToList
            lstPeticionarios = query.ToList
        End If

        Dim stream As New MemoryStream()
        Dim serializer As New DataContractJsonSerializer(GetType(List(Of Peticionario)))
        serializer.WriteObject(stream, lstPeticionarios)
        stream.Position = 0
        Dim streamReader As New StreamReader(stream)
        Return streamReader.ReadToEnd()
    End Function
    <Serializable()>
    Public Structure Peticionario
        Public COD As String
        Public DEN As String
    End Structure
#End Region
#Region "Partidas"
    ''' Revisado por:blp. Fecha: 25/01/2013
    ''' <summary>
    ''' Devuelve las partidas de de todos los árboles presupuestarios que tiene el usuario en los pedidos que puede ver
    ''' </summary>
    ''' <param name="datos">Origen de la consulta y filtro a aplicar</param>
    ''' <returns>String con las observaciones</returns>
    ''' <remarks>Llamadas desde Seguimiento.aspx. Máx. 0,1 seg.</remarks>
    <System.Web.Services.WebMethod(BufferResponse:=True, enableSession:=True)>
    Public Function DevolverPartidasPedidos(ByVal datos As String) As String
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim ardatos() As String = datos.Split({"@@@"}, StringSplitOptions.None)
        Dim origen As String = ardatos(0)
        Dim filtro As String = ardatos(1)
        Dim filtroBase = ""
        If filtro.Length >= 4 Then filtroBase = filtro.Substring(0, 4)
        Dim dsPartidas As DataSet = Nothing
        Dim lstArbolesPartidasPedido As New List(Of ArbolPartidasPedido)
        Dim lstArbolesPartidasPedidoCache As New List(Of ArbolPartidasPedido)
        If oUsuario Is Nothing Then Return String.Empty
        Dim oPRES5 As FSNServer.PRES5 = oServer.Get_Object(GetType(FSNServer.PRES5))
        Dim sCacheKey As String = String.Empty

        If origen = TiposDeDatos.Aplicaciones.EP Then
            sCacheKey = "DsetPartidasEP_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString
        Else
            sCacheKey = "DsetPartidasPM_" & oUsuario.Idioma.ToString() & "_" & filtroBase
        End If

        If HttpContext.Current.Cache(sCacheKey) Is Nothing Then
            If origen = TiposDeDatos.Aplicaciones.EP Then
                dsPartidas = oPRES5.DevolverPartidasPedidos(oUsuario.CodPersona, oUsuario.Idioma, oUsuario.PermisoVerPedidosCCImputables)
            Else
                dsPartidas = oPRES5.DevolverPartidasPM(oUsuario.Idioma, filtroBase)
            End If
            'Vamos a crear una estructura de Partida Presupuestaria para poder serializar los valores a json
            If dsPartidas IsNot Nothing AndAlso dsPartidas.Tables(0) IsNot Nothing AndAlso dsPartidas.Tables(0).Rows.Count > 0 Then
                Dim query = From partidas In dsPartidas.Tables(0)
                            Select partidas("PRES0") Distinct
                For Each oPRES0 In query
                    Dim oArbolPartidasPedido As New ArbolPartidasPedido
                    oArbolPartidasPedido.PRES0 = oPRES0
                    Dim lPartidasPedido As New List(Of PartidasPedido)
                    oArbolPartidasPedido.PRESN = lPartidasPedido
                    lstArbolesPartidasPedidoCache.Add(oArbolPartidasPedido)
                Next
                'Recorremos las partidas
                For Each Partida As DataRow In dsPartidas.Tables(0).Rows
                    Dim oPartidasPedido As New PartidasPedido
                    If origen = TiposDeDatos.Aplicaciones.EP Then
                        'el dataset recibido de EP devuelve en el dato PRESID los PRES1, 2, etc separados por @@, por lo que ponemos @@ aquí tb
                        oPartidasPedido.PRESID = Partida("PRES0") & "@@" & Partida("PRESID")
                    Else
                        'el dataset recibido de PM devuelve en el dato PRESID los PRES1, 2, etc separados por #, por lo que ponemos # aquí tb
                        oPartidasPedido.PRESID = Partida("PRES0") & "#" & Partida("PRESID")
                    End If
                    oPartidasPedido.PRESDEN = null2str(Partida("PRESDEN"))

                    Dim sPRES0 As String = Partida("PRES0")
                    lstArbolesPartidasPedidoCache.Find(Function(arbolPartidas As ArbolPartidasPedido) arbolPartidas.PRES0 = sPRES0).PRESN.Add(oPartidasPedido)
                Next
                HttpContext.Current.Cache.Insert(sCacheKey, lstArbolesPartidasPedidoCache, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            Else
                Return String.Empty
            End If
        Else
            lstArbolesPartidasPedidoCache = HttpContext.Current.Cache(sCacheKey)
        End If
        'Filtro en PM
        If origen = TiposDeDatos.Aplicaciones.PM AndAlso filtro <> filtroBase Then
            For Each arbolpartidas As ArbolPartidasPedido In lstArbolesPartidasPedidoCache
                Dim oArbol As New ArbolPartidasPedido
                Dim sPRES0 As String = arbolpartidas.PRES0.ToUpper
                oArbol.PRES0 = arbolpartidas.PRES0
                Dim oPRESN As New List(Of PartidasPedido)
                oArbol.PRESN = oPRESN
                lstArbolesPartidasPedido.Add(oArbol)
                For Each pres In arbolpartidas.PRESN.ToList
                    Dim oPres As New PartidasPedido
                    oPres.PRESDEN = pres.PRESDEN
                    oPres.PRESID = pres.PRESID
                    If (oPres.PRESID.ToUpper & " " & oPres.PRESDEN.ToUpper).Contains(filtro.ToUpper) Then
                        lstArbolesPartidasPedido.Find(Function(arbolPar As ArbolPartidasPedido) arbolPar.PRES0 = sPRES0).PRESN.Add(oPres)
                    End If
                Next
            Next
        Else
            lstArbolesPartidasPedido = lstArbolesPartidasPedidoCache
        End If

        Dim stream As New MemoryStream()
        Dim serializer As New DataContractJsonSerializer(GetType(List(Of ArbolPartidasPedido)))
        serializer.WriteObject(stream, lstArbolesPartidasPedido)
        stream.Position = 0
        Dim streamReader As New StreamReader(stream)
        Return streamReader.ReadToEnd()
    End Function
    ''' Revisado por:blp. Fecha: 25/01/2013
    ''' <summary>
    ''' Devuelve las partidas de de todos los árboles presupuestarios que tiene el usuario en los pedidos que puede ver
    ''' </summary>
    ''' <param name="datos">Origen de la consulta y filtro a aplicar</param>
    ''' <returns>String con las observaciones</returns>
    ''' <remarks>Llamadas desde Seguimiento.aspx. Máx. 0,1 seg.</remarks>
    <System.Web.Services.WebMethod(BufferResponse:=True, enableSession:=True)>
    Public Function DevolverPartidasEP(ByVal datos As String) As String
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim ardatos() As String = datos.Split({"@@@"}, StringSplitOptions.None)
        Dim origen As String = ardatos(0)
        Dim centro As String = ""

        If (ardatos(1) <> Nothing) Then
            Dim arCentro() As String = ardatos(1).Split({"@@"}, StringSplitOptions.None)
            If (arCentro(0) <> Nothing) Then
                centro = arCentro(0)
            End If
        End If

        Dim empresa As Integer = Nothing
        If (ardatos(2) <> Nothing) Then
            empresa = ardatos(2)
        End If
        Dim Pres5 As String = Nothing
        If (ardatos(3) <> Nothing) Then
            Pres5 = ardatos(3)
        End If
        Dim Pagina As Integer = 0
        Dim Regi As Integer = 0
        If (ardatos(4) <> Nothing) Then
            Pagina = ardatos(4)
        End If
        Dim sTextoFiltro As String = ""
        If (ardatos(5) <> Nothing) Then
            sTextoFiltro = ardatos(5)
        End If
        Dim pageSize As Integer = 50
        If Pagina = 0 Then
            Regi = 0
        Else
            Regi = pageSize + (pageSize * (Pagina - 1)) + 1
        End If

        Dim dsPartidas As DataSet = Nothing
        Dim lstArbolesPartidasPedido As New List(Of ArbolPartidasPedidoEP)
        Dim lstArbolesPartidasPedidoCache As New List(Of ArbolPartidasPedidoEP)
        If oUsuario Is Nothing Then Return String.Empty
        Dim oPRES5 As FSNServer.PRES5 = oServer.Get_Object(GetType(FSNServer.PRES5))
        Dim sCacheKey As String = String.Empty

        If origen = TiposDeDatos.Aplicaciones.EP Then
            sCacheKey = "DsetPartidasEP_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString & "_" & centro & "_" & sTextoFiltro
        End If

        Dim numPags As Integer = 0
        Dim devNumPartidas As Integer = pageSize
        Dim totalPartidas As Integer = 0
        If origen = TiposDeDatos.Aplicaciones.EP And HttpContext.Current.Cache(sCacheKey) Is Nothing Then
            dsPartidas = oPRES5.DevolverPartidasEP(oUsuario.CodPersona, oUsuario.Idioma, Pres5, oUsuario.PedidosOtrasEmp, centro, empresa, sTextoFiltro)
            totalPartidas = dsPartidas.Tables(0).Rows.Count
            Regi = 0
        Else
            lstArbolesPartidasPedidoCache = HttpContext.Current.Cache(sCacheKey)
            totalPartidas = lstArbolesPartidasPedidoCache.Item(0).PRESN.Count
        End If
        numPags = Math.Ceiling(totalPartidas / pageSize)
        If (totalPartidas - Regi) < pageSize Then
            devNumPartidas = totalPartidas - Regi
        End If

        'Vamos a crear una estructura de Partida Presupuestaria para poder serializar los valores a json
        If dsPartidas IsNot Nothing AndAlso dsPartidas.Tables(0) IsNot Nothing AndAlso dsPartidas.Tables(0).Rows.Count > 0 Then
            If HttpContext.Current.Cache(sCacheKey) Is Nothing Then
                Dim query = From partidas In dsPartidas.Tables(0)
                            Select partidas("PRES0") Distinct
                For Each oPRES0 In query
                    Dim oArbolPartidasPedido As New ArbolPartidasPedidoEP
                    oArbolPartidasPedido.PRES0 = oPRES0
                    Dim lPartidasPedido As New List(Of PartidasPedidoEP)
                    oArbolPartidasPedido.PRESN = lPartidasPedido
                    lstArbolesPartidasPedidoCache.Add(oArbolPartidasPedido)
                Next

                'Recorremos las partidas
                For Each Partida As DataRow In dsPartidas.Tables(0).Rows
                    Dim oPartidasPedido As New PartidasPedidoEP
                    If origen = TiposDeDatos.Aplicaciones.EP Then
                        'el dataset recibido de EP devuelve en el dato PRESID los PRES1, 2, etc separados por @@, por lo que ponemos @@ aquí tb
                        oPartidasPedido.PRESID = Partida("PRES5_IMP") & "||" & Partida("PRES0") & "@@" & Partida("PRESID")
                    Else
                        'el dataset recibido de PM devuelve en el dato PRESID los PRES1, 2, etc separados por #, por lo que ponemos # aquí tb
                        oPartidasPedido.PRESID = Partida("PRES5_IMP") & "||" & Partida("PRES0") & "#" & Partida("PRESID")
                    End If
                    oPartidasPedido.PRESDEN = null2str(Partida("PRESDEN"))
                    If Not IsDBNull(Partida("FECINI")) Then oPartidasPedido.FECINI = CDate(Partida("FECINI"))
                    If Not IsDBNull(Partida("FECFIN")) Then oPartidasPedido.FECFIN = CDate(Partida("FECFIN"))
                    Dim sCodCentro As String = ""
                    If Not IsDBNull(Partida("UON4")) Then
                        oPartidasPedido.CCCOD = Partida("UON1") & "@@" & Partida("UON2") & "@@" & Partida("UON3") & "@@" & Partida("UON4")
                        sCodCentro = Partida("UON4")
                    ElseIf Not IsDBNull(Partida("UON3")) Then
                        oPartidasPedido.CCCOD = Partida("UON1") & "@@" & Partida("UON2") & "@@" & Partida("UON3")
                        sCodCentro = Partida("UON3")
                    ElseIf Not IsDBNull(Partida("UON2")) Then
                        oPartidasPedido.CCCOD = Partida("UON1") & "@@" & Partida("UON2")
                        sCodCentro = Partida("UON2")
                    ElseIf Not IsDBNull(Partida("UON1")) Then
                        oPartidasPedido.CCCOD = Partida("UON1")
                        sCodCentro = Partida("UON1")
                    Else
                        oPartidasPedido.CCCOD = String.Empty
                    End If
                    oPartidasPedido.CCDEN = sCodCentro & " - " & null2str(Partida("CENTRO_SM_DEN"))
                    oPartidasPedido.CCCOD = null2str(Partida("CENTRO_SM")) & "@@" & oPartidasPedido.CCCOD
                    oPartidasPedido.PluriAnual = DBNullToInteger(Partida("PLURIANUAL"))
                    Dim sPRES0 As String = Partida("PRES0")
                    lstArbolesPartidasPedidoCache.Find(Function(arbolPartidas As ArbolPartidasPedidoEP) arbolPartidas.PRES0 = sPRES0).PRESN.Add(oPartidasPedido)
                Next
                HttpContext.Current.Cache.Insert(sCacheKey, lstArbolesPartidasPedidoCache, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            Else
                Return String.Empty
            End If
        Else
            lstArbolesPartidasPedidoCache = HttpContext.Current.Cache(sCacheKey)
        End If
        Dim oArbolAux As New ArbolPartidasPedidoEP
        If lstArbolesPartidasPedidoCache Is Nothing Then
            oArbolAux.PRES0 = Nothing
            oArbolAux.PRESN = Nothing
        Else
            oArbolAux.PRES0 = lstArbolesPartidasPedidoCache.Item(0).PRES0
            oArbolAux.PRESN = lstArbolesPartidasPedidoCache.Item(0).PRESN.GetRange(Regi, devNumPartidas)
        End If
        Dim lstAux As New List(Of ArbolPartidasPedidoEP)
        lstAux.Add(oArbolAux)
        lstArbolesPartidasPedido = lstAux
        Dim oArbolPagsAux As New ArbolPartidasPedidoNumPags
        oArbolPagsAux.Arbol = lstAux
        oArbolPagsAux.numPags = numPags
        Dim stream As New MemoryStream()

        Dim serializer As New DataContractJsonSerializer(GetType(ArbolPartidasPedidoNumPags))
        serializer.WriteObject(stream, oArbolPagsAux)
        stream.Position = 0
        Dim streamReader As New StreamReader(stream)
        Return streamReader.ReadToEnd()
    End Function
    <System.Web.Services.WebMethod(BufferResponse:=True, EnableSession:=True)>
    Public Function DevolverPartidasEPCompl(ByVal datos As String) As String
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim ardatos() As String = datos.Split({"@@@"}, StringSplitOptions.None)
        Dim origen As String = ardatos(0)
        Dim centro As String = ""

        If (ardatos(1) <> Nothing) Then
            Dim arCentro() As String = ardatos(1).Split({"@@"}, StringSplitOptions.None)
            If (arCentro(0) <> Nothing) Then
                centro = arCentro(0)
            End If
        End If
        Dim empresa As Integer = Nothing
        If (ardatos(2) <> Nothing) Then
            empresa = ardatos(2)
        End If
        Dim sTextoFiltro As String = ""
        If (ardatos(3) <> Nothing) Then
            sTextoFiltro = ardatos(3)
        End If

        Dim dsPartidas As DataSet = Nothing
        Dim lstArbolesPartidasCompl As New List(Of ArbolPartidasPedidoEPCompl)
        If oUsuario Is Nothing Then Return String.Empty
        Dim oPRES5 As FSNServer.PRES5 = oServer.Get_Object(GetType(FSNServer.PRES5))
        Dim sCacheKey As String = String.Empty

        If origen = TiposDeDatos.Aplicaciones.EP Then
            sCacheKey = "DsetPartidasEPCompl_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString & "_" & centro & "_" & sTextoFiltro
        End If

        If origen = TiposDeDatos.Aplicaciones.EP And HttpContext.Current.Cache(sCacheKey) Is Nothing Then
            dsPartidas = oPRES5.DevolverPartidasEP(oUsuario.CodPersona, oUsuario.Idioma, Nothing, oUsuario.PedidosOtrasEmp, centro, empresa, sTextoFiltro)
        Else
            lstArbolesPartidasCompl = HttpContext.Current.Cache(sCacheKey)
        End If

        'Vamos a crear una estructura de Partida Presupuestaria para poder serializar los valores a json
        If dsPartidas IsNot Nothing AndAlso dsPartidas.Tables(0) IsNot Nothing AndAlso dsPartidas.Tables(0).Rows.Count > 0 Then
            If HttpContext.Current.Cache(sCacheKey) Is Nothing Then
                'Recorremos las partidas
                For Each Partida As DataRow In dsPartidas.Tables(0).Rows
                    Dim oPartidasPedido As New PartidasPedidoEP
                    oPartidasPedido.PRESDEN = null2str(Partida("PRESDEN"))
                    If Not IsDBNull(Partida("FECINI")) Then oPartidasPedido.FECINI = CDate(Partida("FECINI"))
                    If Not IsDBNull(Partida("FECFIN")) Then oPartidasPedido.FECFIN = CDate(Partida("FECFIN"))
                    Dim sCodCentro As String = ""
                    If Not IsDBNull(Partida("UON4")) Then
                        oPartidasPedido.CCCOD = Partida("UON1") & "@@" & Partida("UON2") & "@@" & Partida("UON3") & "@@" & Partida("UON4")
                        sCodCentro = Partida("UON4")
                    ElseIf Not IsDBNull(Partida("UON3")) Then
                        oPartidasPedido.CCCOD = Partida("UON1") & "@@" & Partida("UON2") & "@@" & Partida("UON3")
                        sCodCentro = Partida("UON3")
                    ElseIf Not IsDBNull(Partida("UON2")) Then
                        oPartidasPedido.CCCOD = Partida("UON1") & "@@" & Partida("UON2")
                        sCodCentro = Partida("UON2")
                    ElseIf Not IsDBNull(Partida("UON1")) Then
                        oPartidasPedido.CCCOD = Partida("UON1")
                        sCodCentro = Partida("UON1")
                    Else
                        oPartidasPedido.CCCOD = String.Empty
                    End If
                    oPartidasPedido.CCDEN = sCodCentro & " - " & null2str(Partida("CENTRO_SM_DEN"))
                    oPartidasPedido.CCCOD = null2str(Partida("CENTRO_SM")) & "@@" & oPartidasPedido.CCCOD
                    Dim oPartidasCompl As New ArbolPartidasPedidoEPCompl
                    Dim PRESIDCods() As String = Partida("PRESID").Split({"@@"}, StringSplitOptions.None)
                    oPartidasCompl.PresHidden = Partida("PRES0") & "@@" & Partida("PRESID")
                    If PRESIDCods.Length > 1 Then
                        oPartidasCompl.PresValor = PRESIDCods(0) & " - " & PRESIDCods(1) & " - " & oPartidasPedido.PRESDEN & " - (" & oPartidasPedido.FECINI & " - " & oPartidasPedido.FECFIN & ")"
                    ElseIf PRESIDCods.Length = 1 Then
                        oPartidasCompl.PresValor = PRESIDCods(0) & " - " & oPartidasPedido.PRESDEN & " - (" & oPartidasPedido.FECINI & " - " & oPartidasPedido.FECFIN & ")"
                    End If
                    oPartidasCompl.CtroCod = oPartidasPedido.CCCOD
                    oPartidasCompl.CtroDen = oPartidasPedido.CCDEN
                    lstArbolesPartidasCompl.Add(oPartidasCompl)
                Next
                HttpContext.Current.Cache.Insert(sCacheKey, lstArbolesPartidasCompl, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            Else
                Return String.Empty
            End If
        Else
            lstArbolesPartidasCompl = HttpContext.Current.Cache(sCacheKey)
        End If

        Dim stream As New MemoryStream()
        Dim serializer As New DataContractJsonSerializer(GetType(List(Of ArbolPartidasPedidoEPCompl)))
        If lstArbolesPartidasCompl Is Nothing Then
            serializer.WriteObject(stream, lstArbolesPartidasCompl)
        Else
            serializer.WriteObject(stream, lstArbolesPartidasCompl.GetRange(0, IIf(lstArbolesPartidasCompl.Count > 20, 20, lstArbolesPartidasCompl.Count)))
        End If
        stream.Position = 0
        Dim streamReader As New StreamReader(stream)
        Return streamReader.ReadToEnd()
    End Function
    ''' Revisado por:blp. Fecha: 02/12/2013
    ''' <summary>
    ''' Devuelve la lista de arboles presupuestarios que tiene el usuario
    ''' </summary>
    ''' <returns>String con las observaciones</returns>
    ''' <remarks>Llamadas desde Seguimiento.aspx. Max. 0,1 seg.</remarks>
    <System.Web.Services.WebMethod(BufferResponse:=True, EnableSession:=True)>
    Public Function DevolverPartidasPedidosInicial() As String
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim dtPartidas As DataTable = Nothing
        Dim lstArbolesPartidasPedido As New List(Of ArbolPartidasPedido)
        If oUsuario Is Nothing Then Return String.Empty
        Dim oPRES5 As FSNServer.PRES5 = oServer.Get_Object(GetType(FSNServer.PRES5))
        Dim sCacheKey As String = String.Empty

        sCacheKey = "DsetPartidasPres0_" & oUsuario.Idioma.ToString()

        If HttpContext.Current.Cache(sCacheKey) Is Nothing Then
            dtPartidas = oPRES5.DevolverPartidasPres0PM(oUsuario.Idioma)
            'Vamos a crear una estructura de Partida Presupuestaria para poder serializar los valores a json
            If dtPartidas IsNot Nothing AndAlso dtPartidas.Rows.Count > 0 Then
                Dim query = From partidas In dtPartidas
                            Select partidas("PRES0") Distinct
                For Each oPRES0 In query
                    Dim oArbolPartidasPedido As New ArbolPartidasPedido
                    oArbolPartidasPedido.PRES0 = oPRES0
                    Dim lPartidasPedido As New List(Of PartidasPedido)
                    oArbolPartidasPedido.PRESN = lPartidasPedido
                    lstArbolesPartidasPedido.Add(oArbolPartidasPedido)
                Next
                HttpContext.Current.Cache.Insert(sCacheKey, lstArbolesPartidasPedido, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            Else
                Return String.Empty
            End If
        Else
            lstArbolesPartidasPedido = HttpContext.Current.Cache(sCacheKey)
        End If

        Dim stream As New MemoryStream()
        Dim serializer As New DataContractJsonSerializer(GetType(List(Of ArbolPartidasPedido)))
        serializer.WriteObject(stream, lstArbolesPartidasPedido)
        stream.Position = 0
        Dim streamReader As New StreamReader(stream)
        Return streamReader.ReadToEnd()
    End Function
    <Serializable()>
    Public Structure ArbolPartidasPedido
        Public PRES0 As String
        Public PRESN As List(Of PartidasPedido) 'Las partidas que corresponden a cada PRES0
    End Structure
    <Serializable()>
    Public Structure ArbolPartidasPedidoEP
        Public PRES0 As String
        Public PRESN As List(Of PartidasPedidoEP) 'Las partidas que corresponden a cada PRES0
    End Structure
    <Serializable()>
    Public Structure ArbolPartidasPedidoNumPags
        Public Arbol As List(Of ArbolPartidasPedidoEP)
        Public numPags As Integer
    End Structure
    <Serializable()>
    Public Structure ArbolPartidasPedidoEPCompl
        Public PresValor As String
        Public PresHidden As String
        Public CtroCod As String
        Public CtroDen As String
    End Structure
    <Serializable()>
    Public Structure PartidasPedido
        Public PRESID As String
        Public PRESDEN As String
        'Public PRESPMID As String 'Estructura que consta de los valores uon1#uon2#uon3#uon4#presN. Se guarda en la tabla COPIA_CAMPO, INSTANCIA_DESGLOSE_MATERIAL, PM_CONTR_FILTRO_USU_BUSQUEDA. Solo se usa en Contratos de PM
    End Structure
    <Serializable()>
    Public Structure PartidasPedidoEP
        Public PRESID As String
        Public PRESDEN As String
        Public FECINI As Nullable(Of DateTime)
        Public FECFIN As Nullable(Of DateTime)
        Public CCDEN As String
        Public CCCOD As String
        Public PluriAnual As Integer
        'Public PRESPMID As String 'Estructura que consta de los valores uon1#uon2#uon3#uon4#presN. Se guarda en la tabla COPIA_CAMPO, INSTANCIA_DESGLOSE_MATERIAL, PM_CONTR_FILTRO_USU_BUSQUEDA. Solo se usa en Contratos de PM
    End Structure
#Region "Autocompletar del buscador de partidas"
    ''' <summary>
    ''' Funcion que obtiene las partidas presupuestarias y centros de coste para el autocompletar
    ''' </summary>
    ''' <param name="prefixText">Texto escrito</param>
    ''' <param name="count">Cantidad de resultados a devolver</param>
    ''' <returns>Un array con las partidas o centros que contengan el texto escrito</returns>
    ''' <remarks>
    ''' Llamada desde: Ninguna
    ''' Tiempo maximo: 0,2 seg</remarks>
    <System.Web.Services.WebMethod(True)>
    Public Function GetPartidasYCentros(ByVal prefixText As String, ByVal count As Integer) As String()
        Dim oFSUsuario As FSNServer.User = Session("FSN_USER")
        Dim oFSNServer As FSNServer.Root = Session("FSN_Server")
        Dim oPres5 As FSNServer.PRES5
        Dim MiDataSet As New DataSet
        Dim arrayPalabras As String()
        Dim sbusqueda

        oPres5 = oFSNServer.Get_Object(GetType(FSNServer.PRES5))
        MiDataSet = oPres5.LoadAutoCompletar(Session("PRES5"), oFSUsuario.CodPersona, oFSUsuario.Idioma, Session("CentroCoste"))

        arrayPalabras = Split(prefixText)

        For i = 1 To arrayPalabras.Length - 1 'desde la segunda palabra
            arrayPalabras(i) = " " & arrayPalabras(i)
        Next

        sbusqueda = From Datos In MiDataSet.Tables(0)
                    Let w = UCase(DBNullToStr(Datos.Item(0)))
                    Where arrayPalabras.All(Function(palabra) w Like "*" & strToSQLLIKE(palabra, True) & "*")
                    Select Datos.Item("DEN") Distinct.Take(count).ToArray()
        Dim resul() As String
        ReDim resul(UBound(sbusqueda))
        For i As Integer = 0 To UBound(sbusqueda)
            resul(i) = sbusqueda(i).ToString
        Next
        Return resul
    End Function
#End Region
#End Region
#Region "Centros"
    ''' Revisado por:blp. Fecha: 25/01/2013
    ''' <summary>
    ''' Devuelve los centros que tiene el usuario en los pedidos que puede ver
    ''' </summary>
    ''' <param name="origen">Origen de la consulta</param>
    ''' <returns>String con las observaciones</returns>
    ''' <remarks>Llamadas desde Seguimiento.aspx. Máx. 0,1 seg.</remarks>
    <System.Web.Services.WebMethod(BufferResponse:=True, EnableSession:=True)>
    Public Function DevolverCentrosPedidos(ByVal origen As String) As String
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim sCacheKey As String = String.Empty
        Dim lCentrosPedido As New List(Of CentrosPedido)
        Dim oCentro_SM As FSNServer.Centro_SM = oServer.Get_Object(GetType(FSNServer.Centro_SM))
        Dim dsCentros As New DataSet

        If oUsuario Is Nothing Then Return String.Empty

        If origen = TiposDeDatos.Aplicaciones.EP Then
            sCacheKey = "DsetCentrosEP_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString()
        Else
            sCacheKey = "DsetCentrosPM_" & oUsuario.Idioma.ToString()
        End If

        If HttpContext.Current.Cache(sCacheKey) Is Nothing Then
            If origen = TiposDeDatos.Aplicaciones.EP Then
                dsCentros = oCentro_SM.DevolverCentrosPedidos(oUsuario.CodPersona, oUsuario.Idioma, oUsuario.PermisoVerPedidosCCImputables)
            Else
                dsCentros = oCentro_SM.DevolverCentrosPM(oUsuario.Idioma)
            End If
            'Vamos a crear una estructura llamada CentrosPedido para poder serializar los valores a json
            If dsCentros IsNot Nothing AndAlso dsCentros.Tables(0) IsNot Nothing AndAlso dsCentros.Tables(0).Rows.Count > 0 Then
                Dim oCentrosPedido As New CentrosPedido
                'Recorremos los centros
                For Each Centro As DataRow In dsCentros.Tables(0).Rows
                    oCentrosPedido = New CentrosPedido
                    oCentrosPedido.CCCOD = Centro("CCCOD")
                    oCentrosPedido.CCUON = Centro("CCUON")
                    oCentrosPedido.CCDEN = Centro("CCDEN")
                    oCentrosPedido.CCPM = ""
                    If origen = TiposDeDatos.Aplicaciones.PM Then
                        oCentrosPedido.CCPM = Centro("CCPM")
                    End If
                    lCentrosPedido.Add(oCentrosPedido)
                Next
                HttpContext.Current.Cache.Insert(sCacheKey, lCentrosPedido, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            Else
                Return String.Empty
            End If
        Else
            lCentrosPedido = HttpContext.Current.Cache(sCacheKey)
        End If
        Dim stream As New MemoryStream()
        Dim serializer As New DataContractJsonSerializer(GetType(List(Of CentrosPedido)))
        serializer.WriteObject(stream, lCentrosPedido)
        stream.Position = 0
        Dim streamReader As New StreamReader(stream)
        Return streamReader.ReadToEnd()
    End Function
    <System.Web.Services.WebMethod(BufferResponse:=True, EnableSession:=True)>
    Public Function DevolverCentrosPedidosEP(ByVal origen As String, ByVal verUON As Boolean, ByVal Empresa As String) As String
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim sCacheKey As String = String.Empty
        Dim lCentrosPedido As New List(Of CentrosPedido)
        Dim oCentro_SM As FSNServer.Centro_SM = oServer.Get_Object(GetType(FSNServer.Centro_SM))
        Dim oCentros_SM As FSNServer.Centros_SM = oServer.Get_Object(GetType(FSNServer.Centros_SM))
        Dim dsCentros As New DataSet

        If oUsuario Is Nothing Then Return String.Empty

        sCacheKey = "DsetCentrosEP_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString() & "_" & Empresa.ToString()

        If HttpContext.Current.Cache(sCacheKey) Is Nothing Then
            dsCentros = oCentros_SM.LoadDataEP(oUsuario.CodPersona, oUsuario.Idioma, Empresa, verUON)
            'Vamos a crear una estructura llamada CentrosPedido para poder serializar los valores a json
            If dsCentros IsNot Nothing AndAlso dsCentros.Tables(1) IsNot Nothing AndAlso dsCentros.Tables(1).Rows.Count > 0 Then
                Dim oCentrosPedido As New CentrosPedido
                'Recorremos los centros
                For Each Centro As DataRow In dsCentros.Tables(1).Rows
                    oCentrosPedido = New CentrosPedido
                    oCentrosPedido.CCCOD = Centro("CCCOD")
                    oCentrosPedido.CCUON = Centro("CCUON")
                    oCentrosPedido.CCDEN = Centro("CCDEN")
                    oCentrosPedido.CCPM = Centro("COALUON")
                    oCentrosPedido.Empresa = DBNullToStr(Centro("EMPRESA"))
                    lCentrosPedido.Add(oCentrosPedido)
                Next
                HttpContext.Current.Cache.Insert(sCacheKey, lCentrosPedido, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            Else
                Return String.Empty
            End If
        Else
            lCentrosPedido = HttpContext.Current.Cache(sCacheKey)
        End If
        Dim stream As New MemoryStream()
        Dim serializer As New DataContractJsonSerializer(GetType(List(Of CentrosPedido)))
        serializer.WriteObject(stream, lCentrosPedido)
        stream.Position = 0
        Dim streamReader As New StreamReader(stream)
        Return streamReader.ReadToEnd()
    End Function
    <System.Web.Services.WebMethod(BufferResponse:=True, EnableSession:=True)>
    Public Function DevolverCentrosPedidosEPCompl(ByVal origen As String, ByVal verUON As Boolean, ByVal Empresa As String, ByVal textoFiltro As String) As String
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim sCacheKey As String = String.Empty
        Dim lCentrosPedido As New List(Of CentrosPedidoCompl)
        Dim oCentros_SM As FSNServer.Centros_SM = oServer.Get_Object(GetType(FSNServer.Centros_SM))
        Dim dsCentros As New DataSet

        If oUsuario Is Nothing Then Return String.Empty

        sCacheKey = "DsetCentrosEPCompl_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString() & "_" & Empresa.ToString() & "_" & textoFiltro.ToString()

        If HttpContext.Current.Cache(sCacheKey) Is Nothing Then

            dsCentros = oCentros_SM.LoadDataEP(oUsuario.CodPersona, oUsuario.Idioma, Empresa, verUON, , textoFiltro)
            'Vamos a crear una estructura llamada CentrosPedido para poder serializar los valores a json
            If dsCentros IsNot Nothing AndAlso dsCentros.Tables(1) IsNot Nothing AndAlso dsCentros.Tables(1).Rows.Count > 0 Then
                Dim oCentrosPedido As New CentrosPedidoCompl
                'Recorremos los centros
                For Each Centro As DataRow In dsCentros.Tables(1).Rows
                    oCentrosPedido = New CentrosPedidoCompl
                    oCentrosPedido.CCHidden = Centro("CCCOD") & "@@" & Centro("COALUON")
                    oCentrosPedido.CCValor = Centro("CCUON") & " - " & Centro("CCDEN")
                    lCentrosPedido.Add(oCentrosPedido)
                Next
                HttpContext.Current.Cache.Insert(sCacheKey, lCentrosPedido, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            Else
                Return String.Empty
            End If
        Else
            lCentrosPedido = HttpContext.Current.Cache(sCacheKey)
        End If
        Dim stream As New MemoryStream()
        Dim serializer As New DataContractJsonSerializer(GetType(List(Of CentrosPedidoCompl)))
        serializer.WriteObject(stream, lCentrosPedido)
        stream.Position = 0
        Dim streamReader As New StreamReader(stream)
        Return streamReader.ReadToEnd()
    End Function
    <Serializable()>
    Public Structure CentrosPedido
        Public CCCOD As String
        Public CCUON As String
        Public CCDEN As String
        Public Empresa As String
        ''' <summary>
        ''' Centro de coste para usar en PM al construir el listado de centros en el visor de contratos
        ''' </summary>
        Public CCPM As String
    End Structure
    <Serializable()>
    Public Structure CentrosPedidoCompl
        Public CCValor As String
        Public CCHidden As String
    End Structure
#End Region
#Region "En proceso"
    <System.Web.Services.WebMethod(True)>
    Public Function ComprobarEnProceso(ByVal contextKey As System.String) As System.String
        Dim oFSServer As FSNServer.Root = Session("FSN_Server")
        Dim oInstancia As FSNServer.Instancia
        oInstancia = oFSServer.Get_Object(GetType(FSNServer.Instancia))
        oInstancia.ID = contextKey
        Return oInstancia.ComprobarEnProceso()
    End Function
    ''' <summary>
    ''' Función que se encarga de devolver el texto correspondiente al tipo
    ''' </summary>
    ''' <param name="contextKey">Tipo: 1:Solicitud, 2: Certificado, 3: NC, 5: Contrato</param>
    ''' <returns>Un String con el texto correspondiente indicando que está en proceso</returns>
    <System.Web.Services.WebMethod(True)>
    Public Function Obtener_TextoEnProceso(ByVal contextKey As Integer) As System.String
        Dim oFSUsuario As FSNServer.User = Session("FSN_USER")
        Dim oFSServer As FSNServer.Root = Session("FSN_Server")

        Dim oDict As FSNServer.Dictionary = oFSServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.PanelInfo, oFSUsuario.Idioma)
        Dim sr As New StringBuilder()

        Dim sTexto As String = ""
        Select Case contextKey
            Case FSNLibrary.TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras, FSNLibrary.TiposDeDatos.TipoDeSolicitud.PedidoExpress, FSNLibrary.TiposDeDatos.TipoDeSolicitud.PedidoNegociado
                sTexto = oDict.Data.Tables(0).Rows(8).Item(1) 'La solicitud está siendo tramitada en estos momentos. En breves instantes estará disponible para su gestión.
            Case FSNLibrary.TiposDeDatos.TipoDeSolicitud.Certificado
                sTexto = oDict.Data.Tables(0).Rows(7).Item(1)
            Case FSNLibrary.TiposDeDatos.TipoDeSolicitud.NoConformidad
                sTexto = oDict.Data.Tables(0).Rows(6).Item(1)
            Case FSNLibrary.TiposDeDatos.TipoDeSolicitud.Contrato
                sTexto = oDict.Data.Tables(0).Rows(9).Item(1)
            Case FSNLibrary.TiposDeDatos.TipoDeSolicitud.Autofactura
                sTexto = oDict.Data.Tables(0).Rows(10).Item(1)
        End Select
        sr.Append("&nbsp;&nbsp;&nbsp;" & sTexto)

        Return sr.ToString()
    End Function
#End Region
#Region "Master Page"
    ''' <summary>
    ''' Crea un html con la información para QS
    ''' </summary>
    ''' <param name="Idioma">Idioma</param>
    ''' <param name="Usuario">Usuario conectados</param>
    ''' <returns>html con la información para QS</returns>
    ''' <remarks>Llamada desde: MasterPageWSJS.js/CallService; Tiempo maximo: 0</remarks>
    <WebMethod()>
    Public Function DameInfoQS(ByVal Idioma As String, ByVal Usuario As String) As String
        Dim FSNDict As FSNServer.Dictionary
        FSNDict = New FSNServer.Dictionary()
        FSNDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.Menu, Idioma)
        Dim Textos As Data.DataSet = FSNDict.Data

        DameInfoQS = "<BR>"
        DameInfoQS = DameInfoQS & "<BR>"
        DameInfoQS = DameInfoQS & "<U>" & Textos.Tables(0).Rows(34).Item(1).ToString & "</U><BR>"

        Dim AssemblyInfo As System.Reflection.Assembly
        Dim AssemblyFileVersionAttribute As System.Reflection.AssemblyFileVersionAttribute

        AssemblyInfo = System.Reflection.Assembly.GetExecutingAssembly
        AssemblyFileVersionAttribute = AssemblyInfo.GetCustomAttributes(GetType(System.Reflection.AssemblyFileVersionAttribute), False)(0)
        Dim sVersion As String = AssemblyFileVersionAttribute.Version.ToString

        DameInfoQS = DameInfoQS & Textos.Tables(0).Rows(35).Item(1).ToString & "&nbsp;" & sVersion & "<BR>"

        Dim oFSNServer As FSNServer.Root = New FSNServer.Root
        oFSNServer.get_PropiedadesConexion()

        DameInfoQS = DameInfoQS & Textos.Tables(0).Rows(36).Item(1).ToString & "&nbsp;" & oFSNServer.DBServidor & "<BR>"
        DameInfoQS = DameInfoQS & Textos.Tables(0).Rows(37).Item(1).ToString & "&nbsp;" & oFSNServer.DBName & "<BR>"

        If Usuario = "" Then
            DameInfoQS = DameInfoQS & Textos.Tables(0).Rows(38).Item(1).ToString & "&nbsp;" & Textos.Tables(0).Rows(39).Item(1).ToString
        Else
            DameInfoQS = DameInfoQS & Textos.Tables(0).Rows(38).Item(1).ToString & "&nbsp;" & Usuario
        End If

    End Function
    ''' <summary>
    ''' Funcion que sirve para desconectar. Borra el SessionService
    ''' </summary>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True)>
    Public Sub Desconectar()
        Session.Abandon()
        Session.Clear()
    End Sub
#End Region
#Region "Empresa"
    ''' <summary>
    ''' Función que se encarga de rellenar los datos de detalle de una empresa
    ''' </summary>
    ''' <param name="contextKey">ID empresa</param>
    ''' <returns>Un String con los datos de la empresa en formato html</returns>
    <System.Web.Services.WebMethod(True)>
    Public Function Obtener_DatosEmpresa(ByVal contextKey As System.String) As System.String
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        Dim oEmpresa As FSNServer.Empresa

        oEmpresa = FSNServer.Get_Object(GetType(FSNServer.Empresa))
        oEmpresa.ID = contextKey
        oEmpresa.Load(FSNUser.Idioma)

        Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.DetalleEmpresa, FSNUser.Idioma)
        Dim sr As New StringBuilder()

        sr.Append("<table>")
        sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(0).Item(1) & ":</b></td><td>" & oEmpresa.Den & "</td></tr>")
        sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(1).Item(1) & ":</b></td><td>" & oEmpresa.NIF & "</td></tr>")
        sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(2).Item(1) & ":</b></td><td>" & oEmpresa.Dir & "</td></tr>")
        sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(3).Item(1) & ":</b></td><td>" & oEmpresa.CP & "</td></tr>")
        sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(4).Item(1) & ":</b></td><td>" & oEmpresa.Poblacion & "</td></tr>")
        sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(5).Item(1) & ":</b></td><td>" & oEmpresa.Pais & "</td></tr>")
        sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(6).Item(1) & ":</b></td><td>" & oEmpresa.Provincia & "</td></tr>")
        sr.Append("</table>")

        Return sr.ToString()
    End Function
#End Region
#Region "Detalle de pedido"
    ''' <summary>
    ''' Función que se encarga de rellenar los datos de detalle de un pedido
    ''' </summary>
    ''' <param name="contextKey">ID de pedido</param>
    ''' <returns>Un String con los datos de un pedido en formato html</returns>
    <System.Web.Services.WebMethod(True)>
    Public Function Obtener_DatosDetallePedido(ByVal contextKey As System.String) As System.String
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        Dim sParam() As String
        sParam = Split(contextKey, "#")

        Dim oPedido As FSNServer.Pedido
        oPedido = FSNServer.Get_Object(GetType(FSNServer.Pedido))
        oPedido.ID = sParam(0)
        oPedido.LoadDetalle(FSNUser.Idioma)

        Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.DetallePedido, FSNUser.Idioma)

        Dim sDenEstado As String = String.Empty
        Select Case oPedido.Estado
            Case TipoEstadoOrdenEntrega.PendienteDeAprobacion
                sDenEstado = oDict.Data.Tables(0).Rows(5).Item(1)
            Case TipoEstadoOrdenEntrega.DenegadoParcialAprob
                sDenEstado = oDict.Data.Tables(0).Rows(6).Item(1)
            Case TipoEstadoOrdenEntrega.EmitidoAlProveedor
                sDenEstado = oDict.Data.Tables(0).Rows(7).Item(1)
            Case TipoEstadoOrdenEntrega.AceptadoPorProveedor
                sDenEstado = oDict.Data.Tables(0).Rows(8).Item(1)
            Case TipoEstadoOrdenEntrega.EnCamino
                sDenEstado = oDict.Data.Tables(0).Rows(9).Item(1)
            Case TipoEstadoOrdenEntrega.EnRecepcion
                sDenEstado = oDict.Data.Tables(0).Rows(10).Item(1)
            Case TipoEstadoOrdenEntrega.RecibidoYCerrado
                sDenEstado = oDict.Data.Tables(0).Rows(11).Item(1)
            Case TipoEstadoOrdenEntrega.Anulado
                sDenEstado = oDict.Data.Tables(0).Rows(12).Item(1)
            Case TipoEstadoOrdenEntrega.RechazadoPorProveedor
                sDenEstado = oDict.Data.Tables(0).Rows(13).Item(1)
            Case TipoEstadoOrdenEntrega.DenegadoTotalAprobador
                sDenEstado = oDict.Data.Tables(0).Rows(14).Item(1)
        End Select

        Dim sr As New StringBuilder()
        sr.Append("<table cellpadding=3>")
        sr.Append("<tr><td colspan=5 align=center><span class=RotuloGrande>" & oPedido.NumPedidoCompleto & "</span></td></tr>")
        sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(18).Item(1) & "</b></td><td>" & oPedido.Peticionario & "</td></tr>")
        sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(37).Item(1) & ":</b></td><td>" & FormatDate(oPedido.Fecha, FSNUser.DateFormat) & "</td></tr>")
        sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(38).Item(1) & ":</b></td><td>" & sDenEstado & "</td></tr>")
        sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(39).Item(1) & ":</b></td><td>" & oPedido.DenTipoPedido & "</td></tr>")
        sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(40).Item(1) & ":</b></td><td>" & oPedido.NumPedErp & "</td></tr>")
        sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(41).Item(1) & ":</b></td><td>" & sParam(1).ToString.PadLeft(3, "0") & "</td></tr>")
        sr.Append("</table>")

        Return sr.ToString()
    End Function
#End Region
#Region "Detalle de pagos de una factura"
    ''' <summary>
    ''' Función que se encarga de rellenar los datos de detalle de un albarán
    ''' </summary>
    ''' <param name="contextKey">Albarán</param>
    ''' <returns>Un String con los datos de un albarán en formato html</returns>
    <System.Web.Services.WebMethod(True)>
    Public Function Obtener_DatosDetallepagos(ByVal contextKey As Long) As System.String
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        Dim oPagos As FSNServer.CPagos = FSNServer.Get_Object(GetType(FSNServer.CPagos))
        oPagos = oPagos.CargarPagos(CType(FSNUser.Idioma, FSNLibrary.Idioma), , contextKey)

        Dim sr As New StringBuilder()
        sr.Append("<center><table width=80% border=0 cellspacing=0 cellpadding=5>")
        sr.Append("<tr bgcolor=#cccccc><th width=30% align=left>Número pago</th><th width=20% align=left>Fecha emisión</th><th width=50% align=left>Estado</th></tr>")

        For Each pago As FSNServer.CPago In oPagos
            sr.Append("<tr>")
            sr.Append("<td align=left>" & pago.NumPago & "</td>")
            sr.Append("<td align=left>" & FormatDate(pago.Fecha, FSNUser.DateFormat) & "</td>")
            sr.Append("<td align=left>" & pago.EstadoDen & "</td>")
            sr.Append("</tr>")
        Next
        sr.Append("</table></center>")
        Return sr.ToString()
    End Function
#End Region
#Region "Motivo anulación de una factura"
    ''' <summary>
    ''' Función que se encarga de rellenar los datos de detalle de un albarán
    ''' </summary>
    ''' <param name="contextKey">Albarán</param>
    ''' <returns>Un String con los datos de un albarán en formato html</returns>
    <System.Web.Services.WebMethod(True)>
    Public Function Obtener_MotivoAnulacion(ByVal contextKey As Long) As System.String
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        Dim oFactura As FSNServer.Factura = FSNServer.Get_Object(GetType(FSNServer.Factura))
        oFactura.ID = contextKey
        Return oFactura.DevolverMotivoAnulacion()
    End Function
#End Region
#Region "Monitorizar"
    ''' <summary>
    ''' Procedimiento que graba la monitorización o no de una instancia
    ''' </summary>
    ''' <param name="lInstancia">Identificador del contacto a modificar</param>
    ''' <remarks>Llamada desde: 2008/puntuacion/Contactos/wdgDatos_CellSelectionChanged; Tiempo maximo: 0,1 seg</remarks>
    <System.Web.Services.WebMethod(True)>
    Public Sub Monitorizar(ByVal lInstancia As Long, ByVal iSeg As Short)
        Dim oFSServer As FSNServer.Root = Session("FSN_Server")
        Dim cInstancia As FSNServer.Instancia

        cInstancia = oFSServer.Get_Object(GetType(FSNServer.Instancia))
        cInstancia.ID = lInstancia
        cInstancia.ActualizarMonitorizacion(iSeg, Session("FSN_User").CodPersona)
    End Sub
#End Region
#Region "Integracion"
    ''' <summary>
    ''' FunciOn que se encarga de rellenar los datos de detalle de una persona
    ''' </summary>
    ''' <param name="contextKey">COdigo de persona</param>
    ''' <returns>Un String con los datos de la persona en formato html</returns>
    <System.Web.Services.WebMethod(True)>
    Public Function CargarErrorIntegracion(ByVal contextKey As System.String) As System.String
        Dim oFSUsuario As FSNServer.User = Session("FSN_USER")
        Dim oFSServer As FSNServer.Root = Session("FSN_Server")
        Dim oInt As FSNServer.VisorInt

        oInt = oFSServer.Get_Object(GetType(FSNServer.VisorInt))
        Dim dsDatosError As DataSet = oInt.CargarErrorIntegracion(contextKey)

        Dim sr As New StringBuilder()
        With dsDatosError.Tables(0).Rows(0)
            sr.Append("<div style=""float:left;padding-left:10px; vertical-align:middle;"">")
            sr.Append("<b>" & .Item("TEXTO") & "</b><br/><br/>")
            sr.Append(.Item("ARG1") & "<br/><br/>")
            sr.Append(.Item("ARG2"))
            sr.Append("</div>")
        End With

        Return sr.ToString()
    End Function
#End Region
#Region "Proveedores Favoritos"
    ''' <summary>
    ''' Modificar en bbdd los proveedores favoritos
    ''' </summary>
    <Services.WebMethod(EnableSession:=True)>
    <Script.Services.ScriptMethod()>
    Public Sub CambiarProveFavoritosBD(ByVal codProve As String, ByVal accion As String)
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oUser As FSNServer.User = Session("FSN_User")
        Dim oProvesFavoritos As FSNServer.ProveedoresFavoritos

        oProvesFavoritos = FSWSServer.Get_Object(GetType(FSNServer.ProveedoresFavoritos))
        If accion = "Insertar" Or accion = "InsertarAut" Then
            oProvesFavoritos.Eliminar(oUser.Cod, codProve) 'antes eliminamos, por si ya existe
            oProvesFavoritos.Insertar(oUser.Cod, codProve)
        End If
        If accion = "Eliminar" Then
            oProvesFavoritos.Eliminar(oUser.Cod, codProve)
        End If
    End Sub
#End Region
#Region "Buscador Articulos"
    <WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function Obtener_sMat(ByVal hMaterialVal As String) As String
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim material() As String
        Dim sMat As String = ""

        If hMaterialVal <> "" Then
            material = Split(hMaterialVal, "-")
            Dim i As Byte
            Dim lLongCod As Long
            i = 1
            For i = 1 To material.Length
                Select Case i
                    Case 1
                        lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN1
                    Case 2
                        lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN2
                    Case 3
                        lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN3
                    Case 4
                        lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN4
                End Select
                sMat = sMat + Space(lLongCod - Len(material(i - 1))) + material(i - 1)
            Next
        End If
        Return sMat
    End Function
#End Region
#Region "Artículos Favoritos"
	''' <summary>
	''' Modificar en bbdd los Artículos favoritos
	''' </summary>
	<Services.WebMethod(EnableSession:=True)>
    <Script.Services.ScriptMethod()>
    Public Sub CambiarArticuloFavoritosBD(ByVal codArt As String, ByVal accion As String)
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oUser As FSNServer.User = Session("FSN_User")
        Dim oArticuloFavoritos As FSNServer.ArticulosFavoritos
        oArticuloFavoritos = FSWSServer.Get_Object(GetType(FSNServer.ArticulosFavoritos))
        If accion = "Insertar" Or accion = "InsertarAut" Then
            oArticuloFavoritos.Insertar(oUser.Cod, codArt)
        End If
        If accion = "Eliminar" Then
            oArticuloFavoritos.Eliminar(oUser.Cod, codArt)
        End If
    End Sub
#End Region
#Region "Usuario 4"
	''' <summary>
	''' Función que actualiza los parametros de usuario en la BBDD con los datos introducidos por el usuario
	''' </summary>
	''' <param name="sThousanFmt">Formato de miles</param>
	''' <param name="sDecimalFmt">Formato decimal</param>
	''' <param name="iPrecisionFmt">Precision decimal</param>
	''' <param name="sDateFmt">Formato de fechas</param>
	''' <param name="sIdioma">Idioma de la aplicación</param>
	''' <remarks></remarks>
	<Services.WebMethod(EnableSession:=True)>
	<Script.Services.ScriptMethod()>
	Public Sub UpdateUserData(ByVal sIdioma As String, ByVal sThousanFmt As String, ByVal sDecimalFmt As String,
							 ByVal iPrecisionFmt As Integer, ByVal sDateFmt As String, sUserName As String, sCodigoSesion As String)
		Dim FSNServer As FSNServer.Root = Session("FSN_Server")
		Dim FSNUser As FSNServer.User = Session("FSN_User")
		Try
			If FSNServer Is Nothing AndAlso FSNUser Is Nothing Then
				Dim oFSNPage As FSNPage = New FSNPage()
				oFSNPage.LoginServidorExterno(sUserName, sCodigoSesion, soloInicializarUserRoot:=True)
				FSNServer = Session("FSN_Server")
				FSNUser = Session("FSN_User")
			End If

			If Not IsNothing(FSNServer) AndAlso Not IsNothing(FSNUser) Then
				FSNUser.UpdateUserData(sThousanFmt, sDecimalFmt, iPrecisionFmt, sDateFmt, FSNUser.TipoEmail, sIdioma)
			End If

			Desconectar()
		Catch ex As Exception
			Dim oError As FSNServer.Errores = FSNServer.Get_Object(GetType(FSNServer.Errores))
			oError.Create("_Common\Consultas.aspx\UpdateUserData", sUserName, ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
		End Try
	End Sub
#End Region
End Class