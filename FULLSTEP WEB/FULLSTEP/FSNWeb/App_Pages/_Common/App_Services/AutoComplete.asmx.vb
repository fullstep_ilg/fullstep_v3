﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class AutoComplete
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(True)>
    Public Function GetArticulos(ByVal prefixText As String, ByVal count As Integer) As String()
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        Dim cArticulos As FSNServer.Articulos = FSNServer.Get_Object(GetType(FSNServer.Articulos))
        Dim MiDataSet As New DataSet
        Dim arrayPalabras As String()
        Dim sbusqueda

        MiDataSet = cArticulos.DevolverCodigosYDenominaciones(FSNUser.CodPersona)
        arrayPalabras = Split(prefixText)

        For i = 1 To arrayPalabras.Length - 1 'desde la segunda palabra
            arrayPalabras(i) = " " & arrayPalabras(i)
        Next

        sbusqueda = From Datos In MiDataSet.Tables(0)
                    Let w = UCase(DBNullToStr(Datos.Item(0)))
                    Where arrayPalabras.All(Function(palabra) w Like "*" & strToSQLLIKE(palabra, True) & "*")
                    Select Datos.Item("ARTICULO") Distinct.Take(count).ToArray()


        Dim resul() As String
        ReDim resul(UBound(sbusqueda))
        For i As Integer = 0 To UBound(sbusqueda)
            resul(i) = sbusqueda(i).ToString
        Next
        Return resul
    End Function
    <System.Web.Services.WebMethod(True)>
    Public Function GetArticulosContratos(ByVal prefixText As String, ByVal count As Integer) As String()
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        Dim cArticulos As FSNServer.Articulos = FSNServer.Get_Object(GetType(FSNServer.Articulos))
        Dim MiDataSet As New DataSet
        Dim arrayPalabras As String()
        Dim sbusqueda

        MiDataSet = cArticulos.DevolverCodigosYDenominaciones(FSNUser.CodPersona, TiposDeDatos.TipoDeSolicitud.Contrato)
        arrayPalabras = Split(prefixText)

        For i = 1 To arrayPalabras.Length - 1 'desde la segunda palabra
            arrayPalabras(i) = " " & arrayPalabras(i)
        Next

        sbusqueda = From Datos In MiDataSet.Tables(0)
                    Let w = UCase(DBNullToStr(Datos.Item(0)))
                    Where arrayPalabras.All(Function(palabra) w Like "*" & strToSQLLIKE(palabra, True) & "*")
                    Select Datos.Item("ARTICULO") Distinct.Take(count).ToArray()
        Dim resul() As String
        ReDim resul(UBound(sbusqueda))
        For i As Integer = 0 To UBound(sbusqueda)
            resul(i) = sbusqueda(i).ToString
        Next
        Return resul
    End Function
    <System.Web.Services.WebMethod(True)>
    Public Function GetProveedores(ByVal prefixText As String, ByVal count As Integer) As String()
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        Dim cProveedores As FSNServer.Proveedores = FSNServer.Get_Object(GetType(FSNServer.Proveedores))
        Dim MiDataSet As New DataSet
        Dim arrayPalabras As String()
        Dim sbusqueda

        MiDataSet = cProveedores.CargarProveedores
        arrayPalabras = Split(prefixText)

        For i = 1 To arrayPalabras.Length - 1 'desde la segunda palabra
            arrayPalabras(i) = " " & arrayPalabras(i)
        Next

        sbusqueda = From Datos In MiDataSet.Tables(0)
                    Let w = UCase(DBNullToStr("COD"))
                    Where Datos.Field(Of String)("COD").ToUpper Like "" & strToSQLLIKE(prefixText, True) & "*" Or Datos.Field(Of String)("DEN").ToUpper Like "" & strToSQLLIKE(prefixText, True) & "*"
                    Select Datos Distinct.Take(count).ToArray()

        Dim resul() As String
        Dim l As Integer = 0
        ReDim resul(UBound(sbusqueda))
        For Each proveedor In sbusqueda
            resul(l) = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(proveedor(0) & "-" & proveedor(1), proveedor(0))
            l = l + 1
        Next

        Return resul
    End Function
    <System.Web.Services.WebMethod(True)>
    Public Function GetProveedoresObjSuelos(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        Dim oProves As FSNServer.Proveedores = FSNServer.Get_Object(GetType(FSNServer.Proveedores))

        'Obtener los filtros del contextKey        
        'sUsuCod + "&" + iIdMatQA + "&" + iIdUNQA 
        Dim arContextKey() As String = contextKey.Split("&")

        oProves.CargarProvObjSuelos(arContextKey(0), CType(arContextKey(1), Integer), CType(arContextKey(2), Integer), prefixText, , , count)

        Dim resul(oProves.Data.Tables(0).DefaultView.Count - 1) As String
        Dim i As Integer = 0
        For Each oRow As DataRowView In oProves.Data.Tables(0).DefaultView
            resul(i) = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(oRow("COD") & "-" & oRow("DEN"), oRow("COD"))
            i += +1
        Next

        Return resul
    End Function
    <System.Web.Services.WebMethod(True)>
    Public Function GetEmpresas(ByVal prefixText As String, ByVal count As Integer) As String()
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        Dim oEmpresas As FSNServer.Empresas = FSNServer.Get_Object(GetType(FSNServer.Empresas))
        Dim MiDataSet As New DataSet
        Dim arrayPalabras As String()
        Dim sbusqueda

        MiDataSet = oEmpresas.CargarEmpresas(FSNUser.Idioma, FSNUser.Pyme)
        arrayPalabras = Split(prefixText)

        For i = 1 To arrayPalabras.Length - 1 'desde la segunda palabra
            arrayPalabras(i) = " " & arrayPalabras(i)
        Next

        sbusqueda = From Datos In MiDataSet.Tables(0)
                    Let w = UCase(DBNullToStr("NIF"))
                    Where Datos.Field(Of String)("NIF").ToUpper Like "" & strToSQLLIKE(prefixText, True) & "*" Or Datos.Field(Of String)("DEN").ToUpper Like "" & strToSQLLIKE(prefixText, True) & "*"
                    Select Datos Distinct.Take(count).ToArray()

        Dim resul() As String
        ReDim resul(UBound(sbusqueda))
        Dim l As Integer = 0
        For Each entidad In sbusqueda
            resul(l) = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(entidad(1) & "-" & entidad(2), entidad(0))
            l = l + 1
        Next
        Return resul
    End Function
    <System.Web.Services.WebMethod(True)>
    Public Function GetDescripciones(ByVal prefixText As String, ByVal count As Integer) As String()
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        Dim cInstancias As FSNServer.Instancias = FSNServer.Get_Object(GetType(FSNServer.Instancias))
        Dim MiDataSet As New DataSet
        Dim arrayPalabras As String()
        Dim sbusqueda

        MiDataSet = cInstancias.DevolverDescripciones(FSNUser.CodPersona)
        arrayPalabras = Split(prefixText)

        For i = 1 To arrayPalabras.Length - 1 'desde la segunda palabra
            arrayPalabras(i) = " " & arrayPalabras(i)
        Next

        sbusqueda = From Datos In MiDataSet.Tables(0)
                    Let w = UCase(DBNullToStr(Datos.Item(0)))
                    Where arrayPalabras.All(Function(palabra) w Like "*" & strToSQLLIKE(palabra, True) & "*")
                    Select Datos.Item("DESCRIPCION") Distinct.Take(count).ToArray()
        Dim resul() As String
        ReDim resul(UBound(sbusqueda))
        For i As Integer = 0 To UBound(sbusqueda)
            resul(i) = sbusqueda(i).ToString
        Next
        Return resul
    End Function
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Saca "Instancia - Titulo" de certificados o no conformidades para el autocomplete de Entidad del visor de notificaciones 
    ''' </summary>
    ''' <param name="prefixText">Primeros caracteres de los titulos a buscar</param>
    ''' <param name="count">Numero de valores a mostrar</param>
    ''' <param name="contextKey">certificados o no conformidades</param>
    ''' <remarks>Llamada desde: Notificaciones.aspx ; Tiempo máximo: 0,1</remarks>
    <System.Web.Services.WebMethod(True)>
    Public Function GetTitulosQA_Cod(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String)
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        Dim cInstancias As FSNServer.Instancias = FSNServer.Get_Object(GetType(FSNServer.Instancias))
        Dim MiDataSet As New DataSet
        Dim arrayPalabras As String()
        Dim sbusqueda

        MiDataSet = cInstancias.DevolverTitulosQA(FSNUser.Cod, FSNUser.Idioma, contextKey,
                        FSNUser.QARestCertifUsu, FSNUser.QARestCertifUO, FSNUser.QARestCertifDep,
                        FSNUser.QARestProvMaterial, FSNUser.QARestProvEquipo, FSNUser.QARestProvContacto)
        arrayPalabras = Split(prefixText)

        For i = 1 To arrayPalabras.Length - 1 'desde la segunda palabra
            arrayPalabras(i) = " " & arrayPalabras(i)
        Next

        sbusqueda = From Datos In MiDataSet.Tables(0)
                    Let w = UCase(DBNullToStr("INSTANCIA"))
                    Where Datos.Field(Of String)("INSTANCIA").ToUpper Like "" & strToSQLLIKE(prefixText, True) & "*" Or Datos.Field(Of String)("EXT_TITULO").ToUpper Like "" & strToSQLLIKE(prefixText, True) & "*"
                    Select Datos Distinct.Take(count).ToArray()

        Dim resul() As String
        ReDim resul(UBound(sbusqueda))
        Dim l As Integer = 0
        For Each entidad In sbusqueda
            resul(l) = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(entidad(0) & "-" & entidad(1), entidad(0))
            l = l + 1
        Next
        Return resul
    End Function
    ''' Revisado por: jim. Fecha: 07/03/2014
    ''' <summary>
    ''' Saca Identificador entidad - Titulo de solicitudes de cun determinado tipo entidad
    ''' </summary>
    ''' <param name="prefixText">Primeros caracteres de los titulos a buscar</param>
    ''' <param name="count">Numero de valores a mostrar</param>
    ''' <remarks>Llamada desde: Notificaciones.aspx ; Tiempo máximo: 0,1</remarks>
    <System.Web.Services.WebMethod(True)>
    Public Function GetNotificaciones(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String)
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        Dim cInstancia As FSNServer.Instancias = FSNServer.Get_Object(GetType(FSNServer.Instancias))
        Dim MiDataSet As New DataSet
        Dim arrayPalabras As String()
        Dim sbusqueda

        MiDataSet = cInstancia.DevolverInstanciasNotificacion(FSNUser.Idioma, contextKey)
        arrayPalabras = Split(prefixText)

        For i = 1 To arrayPalabras.Length - 1 'desde la segunda palabra
            arrayPalabras(i) = " " & arrayPalabras(i)
        Next

        sbusqueda = From Datos In MiDataSet.Tables(0)
                    Let w = UCase(DBNullToStr("ID"))
                    Where Datos.Field(Of String)("ID").ToUpper Like "" & strToSQLLIKE(prefixText, True) & "*" _
                            Or Datos.Field(Of String)("ID").ToUpper Like "" & strToSQLLIKE(prefixText, True) & "*"
                    Select Datos Distinct.Take(count).ToArray()

        Dim resul() As String
        ReDim resul(UBound(sbusqueda))
        Dim l As Integer = 0
        For Each entidad In sbusqueda
            resul(l) = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(entidad(2), entidad(0))
            l = l + 1
        Next
        Return resul
    End Function
    <System.Web.Services.WebMethod(True)>
    Public Function GetProveedoresPanelEscalacion(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String)
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        Dim cProveedores As FSNServer.Proveedores = FSNServer.Get_Object(GetType(FSNServer.Proveedores))
        Dim MiDataSet As New DataSet
        Dim arrayPalabras As String()
        Dim sbusqueda
        'Utilizamos el stored FSQA_PROVEEDORES_CERTIF que da los proveedores de QA (deberia llamarse de otra forma)
        cProveedores.Carga_Proveedores_QA(sCod:=prefixText, sUser:=FSNUser.Cod, bUsaLike:=True, bOrdenacionDenominacion:=True,
                                                      RestricProvMat:=FSNUser.QARestProvMaterial, RestricProvEqp:=FSNUser.QARestProvEquipo, RestricProvCon:=FSNUser.QARestProvContacto)
        MiDataSet = cProveedores.Data
        arrayPalabras = Split(prefixText)

        For i = 1 To arrayPalabras.Length - 1 'desde la segunda palabra
            arrayPalabras(i) = " " & arrayPalabras(i)
        Next

        sbusqueda = From Datos In MiDataSet.Tables(0)
                    Let w = UCase(DBNullToStr("COD"))
                    Where Datos.Field(Of String)("COD").ToUpper Like "" & strToSQLLIKE(prefixText, True) & "*" Or Datos.Field(Of String)("DEN").ToUpper Like "" & strToSQLLIKE(prefixText, True) & "*"
                    Select Datos Distinct.Take(count).ToArray()

        Dim resul() As String
        Dim l As Integer = 0
        ReDim resul(UBound(sbusqueda))
        For Each proveedor In sbusqueda
            resul(l) = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(proveedor(0) & "-" & proveedor(1), proveedor(0))
            l = l + 1
        Next

        Return resul
    End Function
End Class