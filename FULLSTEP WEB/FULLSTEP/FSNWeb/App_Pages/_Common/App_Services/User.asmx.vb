﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class User
    Inherits System.Web.Services.WebService
    ''' <summary>
    ''' Devuelve el id de la sesión del webservice
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_SessionID() As String
        Return HttpContext.Current.Session("sSession")
    End Function
    ''' <summary>
    ''' Devuelve el nombre completo del usuario
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_NombreCompleto_Usuario() As String
        Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Return CN_Usuario.Nombre
    End Function
    ''' <summary>
    ''' Devuelve el idioma del usuario
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_Idioma_Usuario() As String
        Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim language As String
        Select Case CN_Usuario.Idioma
            Case "SPA"
                language = "es"
            Case "ENG"
                language = "en"
            Case "GER"
                language = "de"
            Case "FRA"
                language = "fr"
            Case Else
                language = "es"
        End Select
        Return language
    End Function
    ''' <summary>
    ''' Devuelve los accesos del usuario a los diferentes productos
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_Accesos_Usuario() As List(Of FSNServer.cn_fsItem)
        Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim oAcessosUsuario As New List(Of FSNServer.cn_fsItem)
        Dim iAccesoUsuario As New FSNServer.cn_fsItem
        With iAccesoUsuario
            .value = "EP"
            .text = CN_Usuario.AccesoEP
        End With
        oAcessosUsuario.Add(iAccesoUsuario)
        iAccesoUsuario = New FSNServer.cn_fsItem
        With iAccesoUsuario
            .value = "GS"
            .text = CN_Usuario.AccesoGS
        End With
        oAcessosUsuario.Add(iAccesoUsuario)
        iAccesoUsuario = New FSNServer.cn_fsItem
        With iAccesoUsuario
            .value = "PM"
            .text = CN_Usuario.AccesoPM
        End With
        oAcessosUsuario.Add(iAccesoUsuario)
        iAccesoUsuario = New FSNServer.cn_fsItem
        With iAccesoUsuario
            .value = "QA"
            .text = CN_Usuario.AccesoQA
        End With
        oAcessosUsuario.Add(iAccesoUsuario)
        iAccesoUsuario = New FSNServer.cn_fsItem
        With iAccesoUsuario
            .value = "SM"
            .text = CN_Usuario.AccesoSM
        End With
        oAcessosUsuario.Add(iAccesoUsuario)
        iAccesoUsuario = New FSNServer.cn_fsItem
        With iAccesoUsuario
            .value = "CN"
            .text = CN_Usuario.AccesoCN
        End With
        oAcessosUsuario.Add(iAccesoUsuario)
        Return oAcessosUsuario
    End Function
    ''' <summary>
    ''' Devuelve si tiene o no permiso para administrar grupos
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_PermisoUsuario_AdministrarGrupos() As Boolean
        Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Return CN_Usuario.CNPermisoAdministrarGrupos
    End Function
    ''' <summary>
    ''' Devuelve si tiene o no permiso para administrar categorias
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_PermisoUsuario_AdministrarCategorias() As Boolean
        Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Return CN_Usuario.CNPermisoAdministrarCategorias
    End Function
    ''' <summary>
    ''' Devuelve si tiene o no permiso para crear mensajes
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_PermisoUsuario_CrearMensajes() As Boolean
        Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Return (CN_Usuario.AccesoCN AndAlso CN_Usuario.CNPermisoCrearMensajes)
    End Function
    ''' <summary>
    ''' Devuelve si tiene o no permiso para crear mensajes urgentes
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_PermisoUsuario_CrearMensajesUrgentes() As Boolean
        Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Return (CN_Usuario.AccesoCN AndAlso CN_Usuario.CNPermisoCrearMensajesUrgentes)
    End Function
    ''' <summary>
    ''' Devuelve si tiene o no acceso al módulo de CN
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_PermisoUsuario_CN() As Boolean
        Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Return CN_Usuario.AccesoCN
    End Function
    ''' <summary>
    ''' Devuelve el usuario conectado
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Get_Logged_User() As FSNServer.User
        Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Return CN_Usuario
    End Function
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Date_UsuShortPattern(ByVal dateValue As DateTime) As String
        Dim Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Return dateValue.ToString(Usuario.DateFormat.ShortDatePattern)
    End Function
End Class