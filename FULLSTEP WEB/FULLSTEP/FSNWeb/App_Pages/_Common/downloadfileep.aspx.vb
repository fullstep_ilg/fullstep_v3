Imports System.Data.SqlClient
Imports System.Data.SqlTypes
Imports System.IO
Imports System.Security.Principal

Public Class downloadfileep
    Inherits FSNPage

#Region "Variables Suplantacion"

    Private Const LOGON32_LOGON_INTERACTIVE As Integer = 2
    Private Const LOGON32_PROVIDER_DEFAULT As Integer = 0

    Declare Function LogonUserA Lib "advapi32.dll" (ByVal lpszUsername As String,
                            ByVal lpszDomain As String,
                            ByVal lpszPassword As String,
                            ByVal dwLogonType As Integer,
                            ByVal dwLogonProvider As Integer,
                            ByRef phToken As IntPtr) As Integer

    Declare Auto Function DuplicateToken Lib "advapi32.dll" (
                            ByVal ExistingTokenHandle As IntPtr,
                            ByVal ImpersonationLevel As Integer,
                            ByRef DuplicateTokenHandle As IntPtr) As Integer

    Declare Auto Function RevertToSelf Lib "advapi32.dll" () As Long
    Declare Auto Function CloseHandle Lib "kernel32.dll" (ByVal handle As IntPtr) As Long

#End Region


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    '<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    'End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    'Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
    '    'CODEGEN: This method call is required by the Web Form Designer
    '    'Do not modify it using the code editor.
    '    InitializeComponent()
    'End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Request("idContrato") <> "" Then
            TratarAdjuntoContrato()
        Else
            TratarAdjunto()
        End If
    End Sub


    Private Sub TratarAdjunto()
        Dim oFileStream As System.IO.FileStream
        Dim sFichero As String = Server.UrlDecode(Request("src")) & Server.UrlDecode(Request("nombre"))

        If Not System.IO.File.Exists(sFichero) Then
            sFichero = Server.UrlDecode(Request("src")) & Server.UrlEncode(Request("nombre"))

            If Not System.IO.File.Exists(sFichero) Then
                sFichero = Server.UrlDecode(Request("src")) & Request("nombre")
            End If
        End If

        oFileStream = New System.IO.FileStream(sFichero, IO.FileMode.Open)

        Dim byteBuffer() As Byte

        ReDim byteBuffer(oFileStream.Length - 1)

        Call oFileStream.Read(byteBuffer, 0, Int32.Parse(oFileStream.Length.ToString()))

        oFileStream.Close()

        MostrarAdjunto(byteBuffer)

    End Sub
    Public Sub TratarAdjuntoContrato()
        Dim oAdjunto As FSNServer.Adjunto
        Dim oEmpresa As FSNServer.Empresa
        Dim centroCoste As Boolean
        Dim inst As Integer
        Dim cont As Integer
        Dim susuario As String
        oAdjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))
        oEmpresa = FSNServer.Get_Object(GetType(FSNServer.Empresa))
        Dim permiso As Integer '1 permiso descarga 0 sin permiso descarga
        Dim bDescargar As Boolean
        Dim sPath As String
        Dim oFolder As System.IO.Directory


        sPath = Request("src")
        If Not oFolder.Exists(sPath) Then
            oFolder.CreateDirectory(sPath)
        End If

        sPath = sPath & "\" & Server.UrlDecode(Request("nombre"))

        inst = Request("Instancia")
        cont = Request("idContrato")
        susuario = FSNUser.Cod

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Adjuntos

        If FSNServer.TipoAcceso.gbAccesoLCX Then
            'ya no tengo que comprobar la descarga porque ya lo he hecho en el attach
            If Request("descargable") = 1 Then
                bDescargar = True
            Else
                bDescargar = False
            End If
        Else
            bDescargar = True
        End If

        If bDescargar = True Then
            Dim impersonationContext As WindowsImpersonationContext

            FSNServer.Impersonate()

            impersonationContext = RealizarSuplantacion(FSNServer.ImpersonateLogin, FSNServer.ImpersonatePassword, FSNServer.ImpersonateDominio)

            Dim buffer As Byte()

            buffer = oAdjunto.LeerContratoAdjunto(Request("idContrato"), Request("idArchivoContrato"))

            MostrarAdjunto(buffer)

            If Not impersonationContext Is Nothing Then
                impersonationContext.Undo()
            End If

        End If

        oAdjunto = Nothing

    End Sub
    ''' <summary>
    ''' descargara el fichero
    ''' </summary>
    ''' <param name="buffer">fichero</param>
    ''' <remarks>Llamada desde: TratarAdjunto ; Tiempo m�ximo: 0,2</remarks>
    Private Sub MostrarAdjunto(ByVal buffer As Byte())
        Call Me.Response.AddHeader("Content-Type", "application/octet-stream")
        Call Me.Response.AddHeader("Content-Disposition", "attachment;filename=""" & Request("fname") & """")
        Call Me.Response.AddHeader("Content-Length", buffer.Length.ToString())
        Call Me.Response.BinaryWrite(buffer)
        Call Me.Response.End()
    End Sub
End Class