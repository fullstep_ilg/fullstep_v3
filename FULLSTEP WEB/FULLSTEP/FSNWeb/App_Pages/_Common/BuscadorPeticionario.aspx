﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" EnableEventValidation="false" MasterPageFile="~/App_Master/EnBlanco.Master" CodeBehind="BuscadorPeticionario.aspx.vb" Inherits="Fullstep.FSNWeb.BuscadorPeticionario"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
    <asp:Panel ID="pnlPeticionarios" runat="server" BackColor="White" BorderColor="DimGray"
        BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Left" Style="padding: 2px">
        <table width="550">
            <tr>
                <td align="left" style="line-height: 30px;">
                    <img src="../../Images/usuario_buscar.gif" style="padding-left: 10px; vertical-align: middle" />
                    <asp:Label ID="lblPeticionarios" runat="server" CssClass="RotuloGrande" Style="padding-left: 10px;">
                    </asp:Label>
                </td>
                <td align="right" valign="top">
                    <asp:ImageButton runat="server" ID="ImgBtnCerrarPanelPeticionarios" ImageUrl="~/images/Bt_Cerrar.png"
                        Style="display:none; vertical-align: top;" OnClientClick="ocultarPanelModal('mpePeticionarios')" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblFiltroPeticionarios" runat="server" CssClass="Rotulo" />
                    <input class="Normal" type="text" id="FiltroPeticionarios" runat="server" style="width: 320px;" />
                    <asp:Label ID="minCharPeticionarios" runat="server"></asp:Label>
                    <div id="listadoPeticionarios" runat="server" style="height: 400px; overflow: auto;
                        border-top: solid 1px #909090; clear: both; margin-top: 10px;">
                    </div>
                </td>
            </tr>
        </table>
        <asp:HiddenField runat="server" ID="txtCodElementID" />
        <asp:HiddenField runat="server" ID="txtDenElementID" />
    </asp:Panel>
</asp:Content>
