﻿Public Partial Class NoPasoControlarLineaVinculada
    Inherits FSNPage

    ''' <summary>
    ''' Cargar la pagina. Muestra pq no pasa el control de Consumos de la tarea 1959
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>    
    ''' <remarks>Llamada desde:sistema; Tiempo máximo:0</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim oInstancia As FSNServer.Instancia
        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))

        Dim lDesgloseOrig As Long
        Dim lLineaOrig As Integer

        Dim sControlar As String = ""

        If Not (Request("DatosOrigen") Is Nothing) Then
            Dim sDatosOrigen As String = Request("DatosOrigen")

            Dim spli() As String = sDatosOrigen.Split("@")

            oInstancia.ID = spli(0)
            lDesgloseOrig = spli(1)
            lLineaOrig = spli(2)


            If Not (Request("InstanciaVinc") Is Nothing) Then
                sControlar = oInstancia.ComprobarCantidadesVincular(True, lDesgloseOrig, lLineaOrig, Me.Idioma, FSNUser.DateFmt, Request("InstanciaVinc"), Numero(Request("Cantidad"), "."))
            Else
                sControlar = oInstancia.ComprobarCantidadesVincular(True, lDesgloseOrig, lLineaOrig, Me.Idioma, FSNUser.DateFmt)
            End If

        Else
            oInstancia.ID = Request("Instancia")
            lDesgloseOrig = Request("Desglose")
            lLineaOrig = Request("Linea")

            sControlar = oInstancia.ComprobarCantidadesVincular(True, lDesgloseOrig, lLineaOrig, Me.Idioma, FSNUser.DateFmt)
        End If

        Dim sMensaje As String = ""


        Dim sCantMax As String = sControlar.Substring(0, InStr(sControlar, "@") - 1)
        sControlar = sControlar.Substring(InStr(sControlar, "@"))

        Dim sCantidades As String = ""
        Dim sCantidadesFmtUser As String = ""
        While InStr(sControlar, "|") > 0
            sCantidades = sControlar.Substring(InStr(sControlar, "|"), InStr(sControlar, "#") - InStr(sControlar, "|") - 1)
            sCantidadesFmtUser = FSNLibrary.FormatNumber(CDbl(sCantidades), FSNUser.NumberFormat)

            sControlar = Replace(sControlar, "|" & sCantidades & "#", " (" & sCantidadesFmtUser & ")@")
        End While

        Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.NoPasoControlarLineaVinculada

        If Not (Request("DatosOrigen") Is Nothing) Then
            sMensaje = Textos(3) & "<br>" 'imposible ...
            sMensaje = sMensaje & Replace(Textos(4), "#", FSNLibrary.FormatNumber(CDbl(sCantMax), FSNUser.NumberFormat)) & "<br>" 'se ha alcanzado ...
            sMensaje = sMensaje & "<br>"
            sMensaje = sMensaje & Replace(sControlar, "@", "<br>") 'Lo q manda ControlarLineaVinculada.aspx
            sMensaje = sMensaje & "<br>"
        Else
            sMensaje = Textos(0) & "<br>" 'imposible ...
            sMensaje = sMensaje & Replace(Textos(1), "#", FSNLibrary.FormatNumber(CDbl(sCantMax), FSNUser.NumberFormat)) & "<br>" 'se ha alcanzado ...
            sMensaje = sMensaje & Textos(2) & "<br>" 'en las ..
            sMensaje = sMensaje & "<br>"
            sMensaje = sMensaje & Replace(sControlar, "@", "<br>") 'Lo q manda ControlarLineaVinculada.aspx
            sMensaje = sMensaje & "<br>"
        End If

        Me.LblCadena.Text = sMensaje

    End Sub

End Class