﻿Partial Public Class BuscadorArticulos
    Inherits FSNPage

    Private arrConcepto As String()
    Private arrAlmacenamiento As String()
    Private arrRecepcion As String()
    Private bCargarUltAdj As Boolean
    '''indica si el buscador debe mostrar también artículos previamente negociados con adjudicaciones vigentes
    Private bArticulosNegociadosExpress As Boolean
    ''' <summary>
    ''' Cargar la pagina. En el postback usa los filtros indicados para buscar artículos.
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>        
    ''' <remarks>Llamada desde:sistema; Tiempo máximo:0</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sInstanciaMoneda As String = Request("InstanciaMoneda")
        desde.Value = Request("desde") 'Parametro que se le pasa desde el WebPartPanelCalidad
        EsCampoGeneral.Value = If(String.IsNullOrEmpty(Request.QueryString("EsCampoGeneral")), "0", If(Request.QueryString("EsCampoGeneral"), "1", "0"))
        'Tipo de Solicitud
        Dim iTipoSolicit As Integer = IIf(String.IsNullOrEmpty(Request("TipoSolicitud")), -1, CType(Request("TipoSolicitud"), Integer))
        hTipoSolicitud.Value = iTipoSolicit 'Parametro que se le pasa desde el jsalta

        bArticulosNegociadosExpress = FSNServer.TipoAcceso.gbArticulosNegociadosPedExpress And hTipoSolicitud.Value = TiposDeDatos.TipoDeSolicitud.PedidoExpress

        If bArticulosNegociadosExpress Then
            'Articulos negociados
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "PedidoExpressNegociable", "<script>isPedidoExpressNegociable = true;</script>")
        Else
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "PedidoExpressNegociable", "<script>isPedidoExpressNegociable = false;</script>")
        End If

        'Nivel Seleccion familia de materiales
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NivelSeleccion", "<script>var NivelSeleccion = " & If(String.IsNullOrEmpty(Request("NivelSeleccion")), 0, Request("NivelSeleccion")) & ";</script>")
        'Familia de materiales 
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "FamiliaMateriales", "<script>var FamiliaMateriales = '" & Request("FamiliaMateriales") & "';</script>")

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TodosNiveles", "<script>var TodosNiveles = " & IIf(Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles, 1, 0) & ";</script>")

        Rellenos.Value = If(IsNothing(Request("Rellenos")), 0, Request("Rellenos"))
        'controles webPart Panel Calidad
        idControlMatBD.Value = Request("IdMaterialBD")
        idControlMatDen.Value = Request("IdMaterialDen")
        idControlArtCod.Value = Request("IdControlCodArt")
        idControlArtDen.Value = Request("IdControlDenArt")
        idHidControl.Value = Request("idHidControl")
        hTipoCampoGS.Value = Request("TipoCampoGS")
        ClientId.Value = Request("ClientId")
        Dim sProveSumiArt As String = Request("ProveSumiArt")

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaTheme", "<script>var rutaTheme = '" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "'</script>")

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.BusquedaArticulos
        If sInstanciaMoneda = "" Then sInstanciaMoneda = "EUR"
        If Request("CargarUltAdj") <> "" Then bCargarUltAdj = Request("CargarUltAdj")        

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Notificaciones
        CType(wdgArticulos.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPage"), Label).Text = Textos(46)
        CType(wdgArticulos.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblOF"), Label).Text = Textos(48)
        'Vuelvo a cargar el modulo de BusquedaArticulos
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.BusquedaArticulos
        lbltitulo.Text = Textos(15)
        lblCod.Text = Textos(2)
        lblCodigoAtributoTablaUON.Text = Textos(2)
        lblDen.Text = Textos(3)
        lblDenomAtributoTablaUON.Text = Textos(3)
        lblMaterial.Text = Textos(1)
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextoBotonBuscar", "var TextoBuscar='" & JSText(Textos(5)) & "';", True)
        btnAceptar.Text = Textos(6)
        btnCancelar.Text = Textos(39)
        btnAceptarAlert.Text = Textos(6)
        btnAceptarConfirm.Text = Textos(12)
        btnCancelConfirm.Text = Textos(13)
        sMensaje1.Value = Textos(14)

        ReDim arrConcepto(2)
        arrConcepto(0) = Textos(32)
        arrConcepto(1) = Textos(33)
        arrConcepto(2) = Textos(20)

        ReDim arrAlmacenamiento(2)
        arrAlmacenamiento(0) = Textos(34)
        arrAlmacenamiento(1) = Textos(35)
        arrAlmacenamiento(2) = Textos(36)

        ReDim arrRecepcion(2)
        arrRecepcion(0) = Textos(37)
        arrRecepcion(1) = Textos(35)
        arrRecepcion(2) = Textos(36)

        lblOtrasCaracteristicas.Text = Textos(17)
        lblArticulosEncontrados.Text = Textos(38)
        lblBuscPorAtributos.Text = Textos(58)
        lblValorAtributoTablaUON.Text = Textos(59)
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "Textos") Then
            Dim sVariableJavascriptTextosBoolean As String = "var TextosBoolean = new Array();"
            sVariableJavascriptTextosBoolean &= "TextosBoolean[0]='" & JSText(Textos(13)) & "';"
            sVariableJavascriptTextosBoolean &= "TextosBoolean[1]='" & JSText(Textos(12)) & "';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Textos", sVariableJavascriptTextosBoolean, True)
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosScript") Then
            Dim sVariableJavascriptTextosJScript As String = "var TextosJScript = new Array();"
            sVariableJavascriptTextosJScript &= "TextosJScript[1]='" & JSText(Textos(60)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[2]='" & FSNUser.RefCultural & "';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosScript", sVariableJavascriptTextosJScript, True)
        End If
        chkGasto.Text = Textos(18)
        chkInversion.Text = Textos(19)
        chkGastoInversion.Text = Textos(20)
        chkNoAlmacenable.Text = Textos(21)
        chkAlmacenable.Text = Textos(22)
        chkAlmacenamientoOpcional.Text = Textos(23)
        chkRecepcionObligatoria.Text = Textos(24)
        chkNoRecepcionable.Text = Textos(25)
        chkRecepcionOpcional.Text = Textos(26)
        chkGenerico.Text = Textos(27)
        chkNoGenerico.Text = Textos(28)
        chkGenerico2.Text = Textos(27)
        chkNoGenerico2.Text = Textos(28)

        chkCatalogados.Text = Textos(63)
        chkCatalogados2.Text = Textos(63)
        chkNoCatalogados.Text = Textos(64)
        chkNoCatalogados2.Text = Textos(64)

        lblSoloFavoritos.Text = Textos(65)
        lblSoloFavoritos2.Text = Textos(65)
        LblAnyadirAutoFav.Text = Textos(66)

        imgBuscarMaterial.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/lupa.gif"

        lblProveedor.Text = Textos(50)
        chkVerNoProveedor.Text = Textos(51)
        imgProveedor.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/lupa.gif"
        imgBuscarUnidadOrganizativa.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/lupa.gif"

        Dim oGrid As Infragistics.Web.UI.GridControls.WebDataGrid
        oGrid = Me.wdgArticulos
        oGrid.Rows.Clear()
        oGrid.Columns.Clear()
        If Not Page.IsPostBack Then
            imgSoloFavoritos.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/favoritos_si.gif"
            imgSoloFavoritos2.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/favoritos_si.gif"
            chkAnyadirAutoFav.Checked = FSNUser.PMAnyadirArticuloFav
            CargarArticulos()
        Else
            If Request("__EVENTTARGET") = "btnBuscar" Then
                CargarArticulos()
            Else
                If Cache("oArticulos_" & FSNUser.Cod) Is Nothing Then
                    CargarArticulos()
                Else
                    CargarDatos()
                End If
            End If
        End If

        txtProveedor.Width = Unit.Pixel(750)
        txtUnidadOrganizativa.Width = Unit.Pixel(750)
        Dim b As Boolean = False
        If hTipoSolicitud.Value = TiposDeDatos.TipoDeSolicitud.PedidoNegociado Then
            divFijoProveedor.Visible = False
            divChkProveedor.Visible = False
            divProveSumiArt.Visible = False
            b = True
        ElseIf hTipoSolicitud.Value = TiposDeDatos.TipoDeSolicitud.PedidoExpress Then
            txtProveedor.Width = Unit.Pixel(500)
            txtUnidadOrganizativa.Width = Unit.Pixel(500)
            divFijoProveedor.Visible = False
            divProveSumiArt.Visible = False
            b = True
        ElseIf desde.Value = "WebPartPanelCalidad" Then
            Linea1.Visible = False
            divFijoProveedor.Visible = False
            divChkProveedor.Visible = False
            divCajaProveedor.Visible = False
            divProveSumiArt.Visible = False
        ElseIf hTipoSolicitud.Value = TiposDeDatos.TipoDeSolicitud.Certificado OrElse hTipoSolicitud.Value = TiposDeDatos.TipoDeSolicitud.NoConformidad Then
            divCajaProveedor.Visible = False
            divProveSumiArt.Visible = False
        ElseIf desde.Value = "VisorContratos" OrElse hTipoSolicitud.Value = TiposDeDatos.TipoDeSolicitud.Contrato Then
            'Desde Contrato no interesa que aparezcan otras opciones distintas al proveedor que se ha elegido
            divCajaProveedor.Visible = False
            chkVerNoProveedor.Checked = False
            divChkProveedor.Visible = False
            divProveSumiArt.Visible = False
        Else 'SolicitudDeCompras, Resto PM y WucBusquedaNC (Busqueda NC + Filtro NC)
            If sProveSumiArt = String.Empty Then
                divFijoProveedor.Visible = False
                divChkProveedor.Visible = False
                divProveSumiArt.Visible = False
            Else
                divCajaProveedor.Visible = False
                divFijoProveedor.Visible = False
                divChkProveedor.Visible = False
            End If

            If desde.Value <> "WucBusquedaNC" AndAlso Not {TiposDeDatos.TipoDeSolicitud.Certificado, TiposDeDatos.TipoDeSolicitud.NoConformidad,
                    TiposDeDatos.TipoDeSolicitud.Contrato, TiposDeDatos.TipoDeSolicitud.Autofactura, TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoCatalogo,
                    TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoContraAbierto}.Contains(hTipoSolicitud.Value) Then
                b = True
            End If
        End If
        chkSoloFavoritos.Visible = b
        lblSoloFavoritos.Visible = b
        imgSoloFavoritos.Visible = b
        chkSoloFavoritos2.Visible = b
        lblSoloFavoritos2.Visible = b
        imgSoloFavoritos2.Visible = b
        chkAnyadirAutoFav.Visible = b
        LblAnyadirAutoFav.Visible = b

        Dim sScript As String
        Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"

        sScript = String.Format(IncludeScriptKeyFormat, "javascript", "var sNombreGrid='" + Replace(Me.wdgArticulos.ClientID, "_", "x") + "'")
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "ArticulosLlamadaKey", sScript)

        sScript = ""
        sScript += "var ilGMN1 = " + FSNServer.LongitudesDeCodigos.giLongCodGMN1.ToString() + ";"
        sScript += "var ilGMN2 = " + FSNServer.LongitudesDeCodigos.giLongCodGMN2.ToString() + ";"
        sScript += "var ilGMN3 = " + FSNServer.LongitudesDeCodigos.giLongCodGMN3.ToString() + ";"
        sScript += "var ilGMN4 = " + FSNServer.LongitudesDeCodigos.giLongCodGMN4.ToString() + ";"
        sScript += "var sInstanciaMoneda = '" + sInstanciaMoneda.ToString() + "';"

        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "LongitudesCodigosKey", sScript)

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "'; </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RutaFS", "<script>var RutaFS = '" & ConfigurationManager.AppSettings("RutaFS") & "'; </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ruta", "<script>var ruta = '" & ConfigurationManager.AppSettings("ruta") & "'; </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeConceptoGasto", "<script>var mensajeConceptoGasto = '" & JSText(Textos(40)) & "'; </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeConceptoInversion", "<script>var mensajeConceptoInversion = '" & JSText(Textos(41)) & "'; </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeNoAlmacenable", "<script>var mensajeNoAlmacenable = '" & JSText(Textos(42)) & "'; </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeAlmacenamientoObligatorio", "<script>var mensajeAlmacenamientoObligatorio = '" & JSText(Textos(43)) & "'; </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeNoRecepcionable", "<script>var mensajeNoRecepcionable = '" & JSText(Textos(44)) & "'; </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeRecepcionObligatoria", "<script>var mensajeRecepcionObligatoria = '" & JSText(Textos(45)) & "'; </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeNegociadoNoAcepCambioProve", "<script>var mensajeNegociadoNoAcepCambioProve = '" & JSText(Textos(53)) & "'; </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeNegociadoNoAcepCambioMoneda", "<script>var mensajeNegociadoNoAcepCambioMoneda = '" & JSText(Textos(54)) & "'; </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeNegociadoNoAcepGenerico", "<script>var mensajeNegociadoNoAcepGenerico = '" & JSText(Textos(55)) & "'; </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeExpressCambioProveedor", "<script>var mensajeExpressCambioProveedor = '" & JSText(Textos(56)) & "'; </script>")

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalProgress", "<script>var ModalProgress = '" & ModalProgress.ClientID & "'; </script>")


        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "NumberFormatUsu") Then
            With FSNUser
                Dim sVariablesJavascript As String = "var UsuNumberDecimalSeparator = '" & .NumberFormat.NumberDecimalSeparator & "';"
                sVariablesJavascript = sVariablesJavascript & "var UsuNumberGroupSeparator='" & .NumberFormat.NumberGroupSeparator & "';"
                sVariablesJavascript = sVariablesJavascript & "var UsuNumberNumDecimals='" & .NumberFormat.NumberDecimalDigits & "';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NumberFormatUsu", sVariablesJavascript, True)
            End With
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "DateFormatUsu") Then
            With Me.Usuario
                Dim sVariablesJavascript As String = "var UsuMask = '" & .DateFormat.ShortDatePattern & "';"
                sVariablesJavascript &= "var UsuMaskSeparator='" & .DateFormat.DateSeparator & "';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "DateFormatUsu", sVariablesJavascript, True)
            End With
        End If

        Me.ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery.min.js"))
        Me.ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery.ui.min.js"))
        Me.ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery.tmpl.min.js"))
        Me.ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery-migrate.min.js"))
        Me.ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.ui.fsCombo.js"))
        Me.ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.ui.datepicker." & Me.Usuario.RefCultural & ".js"))
        Me.ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jsUpdateProgress.js"))

    End Sub
    ''' <summary>
    ''' Carga la combo de centros, al seleccionar una organización de compras
    ''' </summary>
    ''' <remarks>Llamada desde: Al cambiar el objeto ugtxtOrganizacionCompras; Tiempo máximo: 0 sg.</remarks>
    Private Sub ugtxtOrganizacionCompras_SelectedRowChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownSelectionChangedEventArgs) Handles ugtxtOrganizacionCompras.SelectionChanged
        Dim oCentros As FSNServer.Centros
        oCentros = FSNServer.Get_Object(GetType(FSNServer.Centros))
        oCentros.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil, ugtxtOrganizacionCompras.SelectedValue)

        Dim ugtxtCentros_wdg As Infragistics.Web.UI.GridControls.WebDataGrid = ugtxtCentros.Controls(0).Controls(0).FindControl("ugtxtCentros_wdg")
        ugtxtCentros_wdg.DataSource = oCentros.Data
        ugtxtCentros_wdg.DataBind()
        ugtxtCentros.TextField = "DEN"
        ugtxtCentros.ValueField = "COD"

        ugtxtCentros.ClientEvents.ValueChanging = "ugtxtCentros_ValueChanging"

        oCentros = Nothing
    End Sub
    Private Sub CrearColumnas()
        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
        Dim nombre As String

        For i As Integer = 0 To wdgArticulos.DataSource.Tables(0).Columns.Count - 1
            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            nombre = wdgArticulos.DataSource.Tables(0).Columns(i).ToString
            With campoGrid
                .Key = nombre
                .DataFieldName = nombre
                .Header.Text = nombre
            End With
            wdgArticulos.Columns.Add(campoGrid)
        Next
    End Sub
    ''' <summary>
    ''' Se ejecuta una vez, al cargarse la grid
    ''' </summary>
    ''' <remarks>Llamada desde: Al cargarse la grid; Tiempo máximo: 0 sg.</remarks>
    Private Sub ConfigurarGrid()
        Dim indexInsert As Integer
        Dim bUsar_OrgCompras As Boolean = Not (Request("CodOrgCompras") Is Nothing)
        Dim oGrid As Infragistics.Web.UI.GridControls.WebDataGrid

        oGrid = Me.wdgArticulos
        If hTipoSolicitud.Value >= 0 AndAlso hTipoSolicitud.Value <> TiposDeDatos.TipoDeSolicitud.Certificado AndAlso
               hTipoSolicitud.Value <> TiposDeDatos.TipoDeSolicitud.NoConformidad AndAlso
               hTipoSolicitud.Value <> TiposDeDatos.TipoDeSolicitud.Contrato AndAlso
               hTipoSolicitud.Value <> TiposDeDatos.TipoDeSolicitud.Autofactura AndAlso
               hTipoSolicitud.Value <> TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoCatalogo AndAlso
               hTipoSolicitud.Value <> TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoContraAbierto Then
            wdgArticulos.Columns("FAVORITO").Width = Unit.Pixel(25)
            wdgArticulos.Columns("FAVORITO").Header.Text = ""
        Else
            wdgArticulos.Columns("FAVORITO").Hidden = True
        End If
        If hTipoSolicitud.Value = TiposDeDatos.TipoDeSolicitud.PedidoNegociado Or bArticulosNegociadosExpress Then
            With wdgArticulos
                indexInsert = .Columns("PREC_VIGENTE").Index + 1
                AnyadirColumnaU("BTN")
                .Columns("BTN").VisibleIndex = indexInsert
                .Columns("BTN").Width = Unit.Pixel(25)
            End With
        ElseIf FSNUser.FSPMVisualizar_Ult_ADJ And bCargarUltAdj Then
            With wdgArticulos
                indexInsert = .Columns("PREC_ULT_ADJ").Index + 1
                AnyadirColumnaU("BTN")
                .Columns("BTN").VisibleIndex = indexInsert
                .Columns("BTN").Width = Unit.Pixel(25)
            End With
        End If

        indexInsert = wdgArticulos.Columns("CONCEPTO").Index
        AnyadirColumnaU("GENERICO2")

        If Me.hMostrarCaracteristicas.Value Then
            AnyadirColumnaU("CONCEPTO_DEN")
            AnyadirColumnaU("ALMACENAR_DEN")
            AnyadirColumnaU("RECEPCIONAR_DEN")
        End If

        If Usuario.AccesoEP And (Usuario.EPTipo = TipoAccesoFSEP.SoloAprovisionador Or Usuario.EPTipo = TipoAccesoFSEP.SoloAprobador Or Usuario.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador) Then
            AnyadirColumnaU("CATALOGADO")
        End If
        oGrid.Columns("COD").Header.Text = Textos(2)
        oGrid.Columns("DEN").Header.Text = Textos(3)
        If Me.hMostrarCaracteristicas.Value Then
            oGrid.Columns("CONCEPTO_DEN").Header.Text = Textos(29)
            oGrid.Columns("ALMACENAR_DEN").Header.Text = Textos(30)
            oGrid.Columns("RECEPCIONAR_DEN").Header.Text = Textos(31)
        End If
        oGrid.Columns("GENERICO2").Header.Text = Textos(11)

        oGrid.Columns("GMN1").Hidden = True
        oGrid.Columns("GMN2").Hidden = True
        oGrid.Columns("GMN3").Hidden = True
        oGrid.Columns("GMN4").Hidden = True
        oGrid.Columns("DENGMN").Hidden = True
        oGrid.Columns("UNI").Hidden = True
        oGrid.Columns("DENUNI").Hidden = True
        oGrid.Columns("NUMDEC").Hidden = True
        oGrid.Columns("PRES5_NIV0").Hidden = True
        oGrid.Columns("PRES5_NIV1").Hidden = True
        oGrid.Columns("PRES5_NIV2").Hidden = True
        oGrid.Columns("PRES5_NIV3").Hidden = True
        oGrid.Columns("PRES5_NIV4").Hidden = True
        oGrid.Columns("GENERICO").Hidden = True
        oGrid.Columns("TIPORECEPCION").Hidden = True

        oGrid.Columns("CONCEPTO").Hidden = True
        oGrid.Columns("ALMACENAR").Hidden = True
        oGrid.Columns("RECEPCIONAR").Hidden = True
        oGrid.Columns("PERMISO_EP").Hidden = True
        oGrid.Columns("ESTA_CATALOGADO").Hidden = True
        oGrid.Columns("TIENE_PERMISO").Hidden = True

        oGrid.Columns("COD").Width = Unit.Pixel(150)
        oGrid.Columns("DEN").Width = Unit.Pixel(650)

        If FSNServer.TipoAcceso.gbArticulosGenericos = False Then
            oGrid.Columns("GENERICO2").Hidden = True
        Else
            oGrid.Columns("GENERICO2").Hidden = False
            oGrid.Columns("GENERICO2").Width = Unit.Pixel(70)
            oGrid.Columns("DEN").Width = oGrid.Columns("DEN").Width.Value - oGrid.Columns("GENERICO2").Width.Value
        End If
        If Usuario.AccesoEP And (Usuario.EPTipo = TipoAccesoFSEP.SoloAprovisionador Or Usuario.EPTipo = TipoAccesoFSEP.SoloAprobador Or Usuario.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador) Then
            oGrid.Columns("CATALOGADO").Header.Text = Textos(49)
            oGrid.Columns("CATALOGADO").Width = Unit.Pixel(100)
            oGrid.Columns("DEN").Width = oGrid.Columns("DEN").Width.Value - 100
        End If
        If CBool(Me.hMostrarCaracteristicas.Value) Then
            oGrid.Columns("DEN").Width = oGrid.Columns("DEN").Width.Value - 100
        End If
        If FSNUser.FSPMVisualizar_Ult_ADJ And bCargarUltAdj Then
            oGrid.Columns("DEN").Width = oGrid.Columns("DEN").Width.Value - 140
        End If

        If bUsar_OrgCompras Then
            oGrid.Columns("CODORGCOMPRAS").Hidden = True
            oGrid.Columns("DENORGCOMPRAS").Hidden = True
            oGrid.Columns("CODCENTRO").Hidden = True
            oGrid.Columns("DENCENTRO").Hidden = True
        End If

        If Me.hMostrarCaracteristicas.Value Then
            oGrid.Columns("CONCEPTO_DEN").Width = Unit.Pixel(120)
            oGrid.Columns("ALMACENAR_DEN").Width = Unit.Pixel(120)
            oGrid.Columns("RECEPCIONAR_DEN").Width = Unit.Pixel(120)
        End If

        oGrid.Columns("PREC_VIGENTE").Hidden = True
        If hTipoSolicitud.Value = TiposDeDatos.TipoDeSolicitud.PedidoNegociado Or bArticulosNegociadosExpress Then
            oGrid.Columns("PREC_ULT_ADJ").Hidden = True
            oGrid.Columns("PREC_VIGENTE").Hidden = False
            oGrid.Columns("PREC_VIGENTE").Header.Text = Textos(52)
            oGrid.Columns("PREC_VIGENTE").Width = Unit.Pixel(100)

            oGrid.Columns("UNI_ULT_ADJ").Hidden = True
            oGrid.Columns("DENUNI_ULT_ADJ").Hidden = True
            oGrid.Columns("MON_ULT_ADJ").Hidden = True
            oGrid.Columns("DENMON_ULT_ADJ").Hidden = True
            If (Me.IDFPago.Value <> "") OrElse (Me.IDFPagoHidden.Value <> "") Then
                oGrid.Columns("PAG_ULT_ADJ").Hidden = True
                oGrid.Columns("DENPAG_ULT_ADJ").Hidden = True
            End If

        ElseIf FSNUser.FSPMVisualizar_Ult_ADJ And bCargarUltAdj Then
            oGrid.Columns("PREC_ULT_ADJ").Header.Text = Textos(7) 'Ult.adjudicacion
            oGrid.Columns("PREC_ULT_ADJ").Width = Unit.Pixel(100)
        ElseIf oGrid.Columns("PREC_ULT_ADJ") IsNot Nothing Then
            oGrid.Columns("PREC_ULT_ADJ").Hidden = True
        End If

        If (bCargarUltAdj Or FSNUser.FSPMVisualizar_Ult_ADJ Or bArticulosNegociadosExpress Or hTipoSolicitud.Value = TiposDeDatos.TipoDeSolicitud.PedidoNegociado) Then
            oGrid.Columns("CODPROV_ULT_ADJ").Hidden = True
            oGrid.Columns("DENPROV_ULT_ADJ").Hidden = True
        End If
    End Sub

    ''' <summary>
    ''' Se ejecuta cada vez que se inicializa una fila del grid
    ''' </summary>
    ''' <remarks>LLamada desde: Al cargar la grid con datos; Tiempo máximo: 0 sg</remarks>
    Private Sub wdgArticulos_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles wdgArticulos.InitializeRow
        If e.Row.Items.Item(0).Value = 0 Then
            e.Row.Items.Item(0).Text = "<img name='nofavorito' src='" & ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/favoritos_no.gif' style='text-align:center;'/>" &
                "<img name='favorito' src='" & ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/favoritos_si.gif' style='display:none; text-align:center;'/>"
        Else
            e.Row.Items.Item(0).Text = "<img name='nofavorito' src='" & ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/favoritos_no.gif' style='display:none; text-align:center;'/>" &
                "<img name='favorito' src='" & ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/favoritos_si.gif' style='text-align:center;'/>"
        End If
        If (FSNUser.FSPMVisualizar_Ult_ADJ And bCargarUltAdj) OrElse (hTipoSolicitud.Value = TiposDeDatos.TipoDeSolicitud.PedidoNegociado) Or Me.bArticulosNegociadosExpress Then
            If (Not (e.Row.Items.FindItemByKey("GENERICO").Text = "1")) AndAlso
                Not ((e.Row.Items.FindItemByKey("PREC_ULT_ADJ").Value) Is Nothing Or IsDBNull(e.Row.Items.FindItemByKey("PREC_ULT_ADJ").Value)) Then

                e.Row.Items.FindItemByKey("BTN").Text = "<img src=""" & ConfigurationManager.AppSettings("ruta") & "images/3puntos.gif"" border=""0"">"
            End If
        End If

        If bCargarUltAdj Then
            If hTipoSolicitud.Value = TiposDeDatos.TipoDeSolicitud.PedidoNegociado Or bArticulosNegociadosExpress Then
                If e.Row.Items.FindItemByKey("GENERICO").Text = "1" Then
                    e.Row.Items.FindItemByKey("PREC_VIGENTE").Text = ""
                ElseIf Not e.Row.Items.FindItemByKey("PREC_ULT_ADJ").Value Is Nothing AndAlso Not IsDBNull(e.Row.Items.FindItemByKey("PREC_ULT_ADJ").Value) Then
                    e.Row.Items.FindItemByKey("PREC_VIGENTE").Text = FSNLibrary.FormatNumber(e.Row.Items.FindItemByKey("PREC_ULT_ADJ").Value, FSNUser.NumberFormat) & " " & e.Row.Items.FindItemByKey("MON_ULT_ADJ").Value
                    e.Row.Items.FindItemByKey("PREC_VIGENTE").Text = e.Row.Items.FindItemByKey("PREC_VIGENTE").Text & "/" & e.Row.Items.FindItemByKey("UNI_ULT_ADJ").Value
                End If
            End If

            If e.Row.Items.FindItemByKey("GENERICO").Text = "1" Then
                e.Row.Items.FindItemByKey("PREC_ULT_ADJ").Text = ""
            ElseIf Not e.Row.Items.FindItemByKey("PREC_ULT_ADJ").Value Is Nothing Then
                If Not IsDBNull(e.Row.Items.FindItemByKey("PREC_ULT_ADJ").Value) Then
                    e.Row.Items.FindItemByKey("PREC_ULT_ADJ").Text = FSNLibrary.FormatNumber(e.Row.Items.FindItemByKey("PREC_ULT_ADJ").Value, FSNUser.NumberFormat)
                End If
            End If

        End If

        If e.Row.Items.FindItemByKey("GENERICO").Text = "1" Then
            e.Row.Items.FindItemByKey("GENERICO2").Text = "<img src='" & ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/aprobado.gif' style='text-align:center;'/>"
        Else
            e.Row.Items.FindItemByKey("GENERICO2").Text = ""
        End If

        If Me.hMostrarCaracteristicas.Value Then
            e.Row.Items.FindItemByKey("CONCEPTO_DEN").Text = arrConcepto(e.Row.Items.FindItemByKey("CONCEPTO").Text)
            e.Row.Items.FindItemByKey("ALMACENAR_DEN").Text = arrAlmacenamiento(e.Row.Items.FindItemByKey("ALMACENAR").Text)
            e.Row.Items.FindItemByKey("RECEPCIONAR_DEN").Text = arrRecepcion(e.Row.Items.FindItemByKey("RECEPCIONAR").Text)
        End If
        Dim mostrarIconoAmarillo As Boolean = False
        If Usuario.AccesoEP And (Usuario.EPTipo = TipoAccesoFSEP.SoloAprovisionador Or Usuario.EPTipo = TipoAccesoFSEP.SoloAprobador Or Usuario.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador) Then
            If String.IsNullOrEmpty(e.Row.Items.FindItemByKey("ESTA_CATALOGADO").Text) Then
                e.Row.Items.FindItemByKey("CATALOGADO").Text = ""
            ElseIf DBNullToDbl(e.Row.Items.FindItemByKey("ESTA_CATALOGADO").Text) > 0 Then 'Indica si el articulo esta en CATALOG_LIN
                If Not String.IsNullOrEmpty(e.Row.Items.FindItemByKey("PERMISO_EP").Text) Then
                    If DBNullToDbl(e.Row.Items.FindItemByKey("PERMISO_EP").Text) > 0 Then 'PERMISO_EP: indica si es aprovisionador de ese articulo
                        If DBNullToDbl(e.Row.Items.FindItemByKey("TIENE_PERMISO").Text) > 0 Then 'TIENE_PERMISO: Indica si aplicando las restricciones(EPRestringirPedidosArticulosUONUsuario y/o EPRestringirPedidosArticulosUONPerfil) tiene permiso para emitir pedido de este articulo, o sin ninguna restriccion
                            e.Row.Items.FindItemByKey("CATALOGADO").Text = "<img src='" & ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/aprobado.gif' style='text-align:center;'/>"
                        Else
                            mostrarIconoAmarillo = True
                        End If
                    End If
                Else
                    mostrarIconoAmarillo = True
                End If
            Else
                mostrarIconoAmarillo = True
            End If
        End If
        If mostrarIconoAmarillo Then
            e.Row.Items.FindItemByKey("CATALOGADO").Tooltip = Textos(48)
            e.Row.Items.FindItemByKey("CATALOGADO").Text = "<img src='" & ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Icono_Error_amarillo.gif' style='text-align:center;'/>"
        End If
    End Sub
    Private Sub wdgArticulos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles wdgArticulos.DataBound
        Dim pagerList As DropDownList = DirectCast(wdgArticulos.Behaviors.Paging.PagerTemplateContainerTop.FindControl("PagerPageList"), DropDownList)
        pagerList.Items.Clear()
        For i As Integer = 1 To wdgArticulos.Behaviors.Paging.PageCount
            pagerList.Items.Add(i.ToString())
        Next
        CType(wdgArticulos.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblCount"), Label).Text = wdgArticulos.Behaviors.Paging.PageCount
        Dim SoloUnaPagina As Boolean = (wdgArticulos.Behaviors.Paging.PageCount = 1)
        With CType(wdgArticulos.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
            .Enabled = Not SoloUnaPagina
        End With
        With CType(wdgArticulos.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
            .Enabled = Not SoloUnaPagina
        End With
    End Sub
    ''' <summary>
    ''' Pone el texto correspondiente en la etiqueta lblProcesando.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: El prerender de la etiqueta. Tiempo máximo: 0sg.</remarks>
    Protected Sub LblProcesando_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, Label).Text = Textos(46)
    End Sub
    ''' <summary>
    ''' Si existe más de un tipo de pedido, devuelve true, si no, false.
    ''' </summary>
    ''' <returns>Devuelve true/false dependiendo de si hay o no más de un tipo de pedido</returns>
    ''' <remarks>Llamada desde: Page_Load; Tiempo máximo: 0 sg</remarks>
    Private Function ExisteMasDeUnTipoDePedido() As Boolean
        Dim oTiposPedido As FSNServer.TiposPedido
        Dim ds As DataSet

        oTiposPedido = FSNServer.Get_Object(GetType(FSNServer.TiposPedido))
        ds = oTiposPedido.LoadData(Idioma)
        If ds.Tables(0).Rows.Count > 1 Then
            ExisteMasDeUnTipoDePedido = True
        Else
            ExisteMasDeUnTipoDePedido = False
        End If
    End Function
    ''' <summary>
    ''' Evento del control webdropdown lanzado desde javascript con la funcion loadItems
    ''' </summary>
    ''' <param name="sender">control webdropdown</param>
    ''' <param name="e">parámetros del evento</param>
    ''' <remarks></remarks>
    Private Sub ugtxtCentros_ItemsRequested(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownItemsRequestedEventArgs) Handles ugtxtCentros.ItemsRequested
        Dim oCentros As FSNServer.Centros
        oCentros = FSNServer.Get_Object(GetType(FSNServer.Centros))
        oCentros.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil, e.Value)

        ugtxtCentros.Items(0).Text = String.Empty
        ugtxtCentros.Items(0).Value = String.Empty

        Dim ugtxtCentros_wdg As Infragistics.Web.UI.GridControls.WebDataGrid = ugtxtCentros.Controls(0).Controls(0).FindControl("ugtxtCentros_wdg")
        ugtxtCentros_wdg.DataSource = oCentros.Data
        ugtxtCentros_wdg.DataBind()
        AddHandler ugtxtCentros_wdg.PreRender, AddressOf ugtxtCentros_wdg_onPreRender
        ugtxtCentros.TextField = "DEN"
        ugtxtCentros.ValueField = "COD"

        ugtxtCentros.ClientEvents.ValueChanging = "ugtxtCentros_ValueChanging"

        oCentros = Nothing
    End Sub
    ''' <summary>
    ''' Creamos un prerender para deseleccionar el valor seleccionado en el webdatagrid ugtxtCentros_wdg cuando se cambie la selección en ugtxtOrganizacionCompras
    ''' </summary>
    ''' <param name="sender">control que lanza el evento (ugtxtCentros_wdg)</param>
    ''' <param name="e">parámetros del evento</param>
    ''' <remarks>Llamada desde Buscador articulos.aspx.vb. Tiempo máximo inferior a 0,2 seg</remarks>
    Private Sub ugtxtCentros_wdg_onPreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ugtxtCentros_wdg As Infragistics.Web.UI.GridControls.WebDataGrid = ugtxtCentros.Controls(0).Controls(0).FindControl("ugtxtCentros_wdg")
        ugtxtCentros_wdg.Behaviors.Selection.SelectedRows.Clear()
    End Sub
    Private Sub AnyadirColumnaU(ByVal fieldname As String)
        Dim unboundField As New Infragistics.Web.UI.GridControls.UnboundField(True)

        unboundField.Key = fieldname
        wdgArticulos.Columns.Add(unboundField)
    End Sub
    Private Sub AnyadirColumnaB(ByVal fieldname As String)
        Dim boundField As New Infragistics.Web.UI.GridControls.BoundDataField(True)

        boundField.Key = fieldname
        boundField.DataFieldName = fieldname
        boundField.Header.Text = fieldname
        wdgArticulos.Columns.Add(boundField)
    End Sub
    ''' <summary>
    ''' Carga el grid. 
    ''' Por la incidencia Pruebas 9 /2014 /341 debes darle un minimo de parametros de busqueda (sin nada 812000 registros en Fer909). 
    ''' Actualmente solo busca si das: CodArt y/o DenArt y/o Uon1 y/o Gmn y/o OrgCompra
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load</remarks>
    Private Sub CargarArticulos()
        Dim i As Integer
        Dim lLongCod As Integer
        Dim sGMN1 As String
        Dim sGMN2 As String
        Dim sGMN3 As String
        Dim sGMN4 As String
        Dim sMat As String = IIf(String.IsNullOrEmpty(Request("mat")), Request("FamiliaMateriales"), Request("mat"))
        Dim iGenericos As Integer
        Dim bUsar_OrgCompras As Boolean = Not (Request("CodOrgCompras") Is Nothing)
        Dim sCodOrgCompras As String = Nothing
        Dim OrgComprasRO, CentroRO, UnidadOrganizativaRO As Boolean
        Dim sCodCentro As String = Nothing
        Dim sCodUnidadOrganizativa As String = Nothing
        Dim bMatRO As Boolean = (Request("matro") = "1")
        Dim sRestric As String = Request("restric")

        Dim bMostrarCaracteristicas As Boolean
        Dim sInstanciaMoneda As String = Request("InstanciaMoneda")
        If bUsar_OrgCompras Then
            lblOrgCompras.Text = Textos(8)
            lblCentro.Text = Textos(9)
        End If
        lblUnidadOrganizativa.Text = Textos(57)
        If desde.Value = "WucBusquedaNC" AndAlso sMat <> "" Then
            Dim material() As String
            Dim Aux As String = sMat
            sMat = ""
            material = Split(Aux, "-")
            i = 1
            For i = 1 To UBound(material) + 1
                Select Case i
                    Case 1
                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                    Case 2
                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                    Case 3
                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                    Case 4
                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN4
                End Select
                sMat = sMat + Space(lLongCod - Len(material(i - 1))) + material(i - 1)
            Next
        End If

        If Not IsPostBack() Then
            'si hay tipo de pedido seleccionado en la solicitud
            hConcepto.Value = Request("concepto")
            hAlmacenamiento.Value = Request("almacenamiento")
            hRecepcion.Value = Request("recepcion")

            If ExisteMasDeUnTipoDePedido() Then
                bMostrarCaracteristicas = True
                pnlCaracteristicasCompleto.Visible = True
                pnlCaracteristicasSimple.Visible = False
                If Usuario.AccesoEP And (Usuario.EPTipo = TipoAccesoFSEP.SoloAprovisionador Or Usuario.EPTipo = TipoAccesoFSEP.SoloAprobador Or Usuario.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador) Then
                    chkCatalogados.Visible = True
                    chkCatalogados2.Visible = False
                    chkNoCatalogados.Visible = True
                    chkNoCatalogados2.Visible = False

                    chkCatalogados.Checked = True
                    chkNoCatalogados.Checked = True
                Else
                    chkCatalogados.Visible = False
                    chkCatalogados2.Visible = False
                    chkNoCatalogados.Visible = False
                    chkNoCatalogados2.Visible = False
                End If

                'Concepto
                If hConcepto.Value = "0" Then
                    chkGasto.Checked = True
                ElseIf hConcepto.Value = "1" Then
                    chkInversion.Checked = True
                ElseIf hConcepto.Value = "2" Then
                    chkGasto.Checked = True
                    chkInversion.Checked = True
                    chkGastoInversion.Checked = True
                End If

                'Almacenamiento
                If hAlmacenamiento.Value = "0" Then
                    chkNoAlmacenable.Checked = True
                    chkAlmacenamientoOpcional.Checked = True
                ElseIf hAlmacenamiento.Value = "1" Then
                    chkAlmacenable.Checked = True
                    chkAlmacenamientoOpcional.Checked = True
                ElseIf hAlmacenamiento.Value = "2" Then
                    chkNoAlmacenable.Checked = True
                    chkAlmacenable.Checked = True
                    chkAlmacenamientoOpcional.Checked = True
                End If

                'Recepcion
                If hRecepcion.Value = "0" Then
                    chkNoRecepcionable.Checked = True
                    chkRecepcionOpcional.Checked = True
                ElseIf hRecepcion.Value = "1" Then
                    chkRecepcionObligatoria.Checked = True
                    chkRecepcionOpcional.Checked = True
                ElseIf hRecepcion.Value = "2" Then
                    chkNoRecepcionable.Checked = True
                    chkRecepcionObligatoria.Checked = True
                    chkRecepcionOpcional.Checked = True
                End If

                'Genericos
                If FSNServer.TipoAcceso.gbArticulosGenericos = False Then
                    chkGenerico.Visible = False
                    chkNoGenerico.Visible = False
                Else
                    chkGenerico.Checked = True
                    chkGenerico.Visible = True
                    chkNoGenerico.Checked = True
                    chkNoGenerico.Visible = True
                End If
            Else
                bMostrarCaracteristicas = False
                pnlCaracteristicasCompleto.Visible = False
                pnlCaracteristicasSimple.Visible = True
                If Usuario.AccesoEP And (Usuario.EPTipo = TipoAccesoFSEP.SoloAprovisionador Or Usuario.EPTipo = TipoAccesoFSEP.SoloAprobador Or Usuario.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador) Then
                    chkCatalogados.Visible = False
                    chkCatalogados2.Visible = True
                    chkNoCatalogados.Visible = False
                    chkNoCatalogados2.Visible = True

                    chkCatalogados2.Checked = True
                    chkNoCatalogados2.Checked = True
                Else
                    chkCatalogados.Visible = False
                    chkCatalogados2.Visible = False
                    chkNoCatalogados.Visible = False
                    chkNoCatalogados2.Visible = False
                End If
                'Genericos
                If FSNServer.TipoAcceso.gbArticulosGenericos = False Then
                    chkGenerico2.Visible = False
                    chkNoGenerico2.Visible = False
                Else
                    chkGenerico2.Visible = True
                    chkGenerico2.Checked = True
                    chkNoGenerico2.Visible = True
                    chkNoGenerico2.Checked = True
                End If
            End If

            hMostrarCaracteristicas.Value = IIf(bMostrarCaracteristicas, 1, 0)
        End If

        If sInstanciaMoneda = "" Then sInstanciaMoneda = "EUR"
        If Request("CargarUltAdj") <> "" Then bCargarUltAdj = Request("CargarUltAdj")
        Dim iTipoSolicit As Integer = IIf(String.IsNullOrEmpty(Request("TipoSolicitud")), -1, CType(Request("TipoSolicitud"), Integer))

        txtMatClientID.Value = Request("MatClientId")
        txtArtClientID.Value = Request("ClientId")

        If Not IsPostBack() Then
            hProveedorIniNegociado.Value = Request("CodProve")
            IDCodProve.Value = Request("sIDCodProve")
            lblProveedorFijo.Text = " - "
            Dim CodProve As String = Request("CodProve")

            If CodProve <> "" Then
                hProveedor.Value = CodProve
                Dim oProve As FSNServer.Proveedor
                oProve = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
                oProve.Cod = CodProve
                oProve.Load(Idioma)
                txtProveedor.Text = oProve.Cod & " - " & oProve.Den
                lblProveedorFijo.Text = txtProveedor.Text
                If oProve.Den = "" Then
                    hProveedor.Value = ""
                End If
                oProve = Nothing
            End If

            Dim sProveSumiArt As String = Request("ProveSumiArt")
            If sProveSumiArt <> "" Then
                hProveedorSumiArt.Value = sProveSumiArt
                Dim oProve As FSNServer.Proveedor
                oProve = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
                oProve.Cod = sProveSumiArt
                oProve.Load(Idioma)
                lblProveSumiArt.Text = oProve.Cod & " - " & oProve.Den
                If oProve.Den = "" Then hProveedorSumiArt.Value = ""
            End If
        End If

        If Not Page.IsPostBack Then
            With CType(wdgArticulos.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero_desactivado.gif"
                .Attributes.Add("onClick", "return FirstPage();")
                .Enabled = False
            End With
            With CType(wdgArticulos.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior_desactivado.gif"
                .Attributes.Add("onClick", "return PrevPage();")
                .Enabled = False
            End With
            Dim SoloUnaPagina As Boolean = True
            With CType(wdgArticulos.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
                .Attributes.Add("onClick", "return NextPage();")
                .Enabled = Not SoloUnaPagina
            End With
            With CType(wdgArticulos.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
                .Attributes.Add("onClick", "return LastPage();")
                .Enabled = Not SoloUnaPagina
            End With
        End If

        If bUsar_OrgCompras Then
            If Request("txtCodOrgCompras") <> "" Then
                sCodOrgCompras = Request("txtCodOrgCompras")
                OrgComprasRO = "0"
            ElseIf Request("CodOrgCompras") IsNot Nothing Then  'Valor Inicial
                sCodOrgCompras = IIf(Request("CodOrgCompras") = "-1", "", Request("CodOrgCompras"))
                OrgComprasRO = Request("OrgComprasRO") = "1"
            End If

            If Request("txtCodCentro") <> "" Then
                sCodCentro = Request("txtCodCentro")
                CentroRO = "0"
            ElseIf Request("CodCentro") IsNot Nothing Then
                sCodCentro = IIf(Request("CodCentro") = "-1", "", Request("CodCentro"))
                CentroRO = Request("CentroRO") = "1"
            End If
        Else
            If Not Request("txtUnidadOrganizativa") = "" Then
                sCodUnidadOrganizativa = Request("txtUnidadOrganizativa")
                UnidadOrganizativaRO = "0"
            ElseIf Request("CodUnidadOrganizativa") IsNot Nothing AndAlso Not Request("CodUnidadOrganizativa") = String.Empty Then
                sCodUnidadOrganizativa = IIf(Request("CodUnidadOrganizativa") = "-1", "", Request("CodUnidadOrganizativa"))
                UnidadOrganizativaRO = Request("UnidadOrganizativaRO") = "1"
            End If

        End If

        Dim sCod As String
        Dim sDen As String
        sCod = Request("cod")
        sDen = Request("den")
        If Page.IsPostBack Then
            sMat = If(String.IsNullOrEmpty(Request("hMat")) AndAlso Not String.IsNullOrEmpty(Request("FamiliaMateriales")), Request("FamiliaMateriales"), Request("hMat"))
            sCod = Request("txtCod")
            sDen = Request("txtDen")
        End If
        hMat.Value = sMat
        txtCod.Text = sCod
        txtDen.Text = sDen

        Dim oArts As FSNServer.Articulos
        Dim arrMat(4) As String
        Dim arrRestric(4) As String

        For i = 1 To 4
            arrMat(i) = ""
            arrRestric(4) = ""
        Next i

        i = 1
        While Trim(sMat) <> ""
            Select Case i
                Case 1
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                Case 2
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                Case 3
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                Case 4
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN4
            End Select
            arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
            sMat = Mid(sMat, lLongCod + 1)
            i = i + 1
        End While
        i = 1
        While Trim(sRestric) <> ""
            Select Case i
                Case 1
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                Case 2
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                Case 3
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                Case 4
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN4
            End Select
            arrRestric(i) = Trim(Mid(sRestric, 1, lLongCod))
            sRestric = Mid(sRestric, lLongCod + 1)
            i = i + 1
        End While


        sGMN1 = arrRestric(1)
        sGMN2 = arrRestric(2)
        sGMN3 = arrRestric(3)
        sGMN4 = arrRestric(4)

        If sGMN1 = "" Then
            sGMN1 = arrMat(1)
        End If
        If sGMN2 = "" Then
            sGMN2 = arrMat(2)
        End If
        If sGMN3 = "" Then
            sGMN3 = arrMat(3)
        End If
        If sGMN4 = "" Then
            sGMN4 = arrMat(4)
        End If

        If Not IsPostBack() Then
            Dim oGMN1s As FSNServer.GruposMatNivel1
            Dim oGMN1 As FSNServer.GrupoMatNivel1
            Dim oGMN2 As FSNServer.GrupoMatNivel2
            Dim oGMN3 As FSNServer.GrupoMatNivel3
            Dim sValorTexto As String = ""
            Dim sValorTooltip As String = ""

            If sGMN4 <> "" Then
                oGMN3 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel3))
                oGMN3.GMN1Cod = sGMN1
                oGMN3.GMN2Cod = sGMN2
                oGMN3.Cod = sGMN3
                oGMN3.CargarTodosLosGruposMatDesde(1, Idioma, sGMN4, , True)
                If (Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles) Then
                    sValorTexto = sGMN1 & " - " & sGMN2 & " - " & sGMN3 & " - " & sGMN4 & " - " & oGMN3.GruposMatNivel4.Item(sGMN4).Den
                Else
                    sValorTexto = sGMN4 & " - " & oGMN3.GruposMatNivel4.Item(sGMN4).Den
                End If
                sValorTooltip = oGMN3.GruposMatNivel4.Item(sGMN4).Den & " (" & sGMN1 & " - " & sGMN2 & " - " & sGMN3 & " - " & sGMN4 & ")"
                oGMN3 = Nothing

            ElseIf sGMN3 <> "" Then
                oGMN2 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel2))
                oGMN2.GMN1Cod = sGMN1
                oGMN2.Cod = sGMN2
                oGMN2.CargarTodosLosGruposMatDesde(1, Idioma, sGMN3, , True)
                If (Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles) Then
                    sValorTexto = sGMN1 & " - " & sGMN2 & " - " & sGMN3 & " - " & oGMN2.GruposMatNivel3.Item(sGMN3).Den
                Else
                    sValorTexto = sGMN3 & " - " & oGMN2.GruposMatNivel3.Item(sGMN3).Den
                End If
                sValorTooltip = oGMN2.GruposMatNivel3.Item(sGMN3).Den & "(" & sGMN1 & " - " & sGMN2 & " - " & sGMN3 & ")"
                oGMN2 = Nothing

            ElseIf sGMN2 <> "" Then
                oGMN1 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel1))
                oGMN1.Cod = sGMN1
                oGMN1.CargarTodosLosGruposMatDesde(1, Idioma, sGMN2, , True)
                If (Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles) Then
                    sValorTexto = sGMN1 & " - " & sGMN2 & " - " & oGMN1.GruposMatNivel2.Item(sGMN2).Den
                Else
                    sValorTexto = sGMN2 & " - " & oGMN1.GruposMatNivel2.Item(sGMN2).Den
                End If
                sValorTooltip = oGMN1.GruposMatNivel2.Item(sGMN2).Den & "(" & sGMN1 & " - " & sGMN2 & ")"
                oGMN1 = Nothing

            ElseIf sGMN1 <> "" Then
                oGMN1s = FSNServer.Get_Object(GetType(FSNServer.GruposMatNivel1))
                oGMN1s.CargarTodosLosGruposMatDesde(1, Idioma, sGMN1, , , True)
                sValorTexto = sGMN1 & " - " & oGMN1s.Item(sGMN1).Den
                sValorTooltip = oGMN1s.Item(sGMN1).Den & "(" & sGMN1 & ")"
                oGMN1s = Nothing
            End If

                txtMaterial.Text = DBNullToSomething(sValorTexto)
            txtMaterial.ToolTip = Server.HtmlEncode(sValorTooltip)

        End If

        If bUsar_OrgCompras Then
            lblUnidadOrganizativa.Visible = False
            pnlUnidadesOrganizativas.Visible = False
            lblUnidadOrganizativaSeleccionada.Visible = False
            If OrgComprasRO = True Then
                ugtxtOrganizacionCompras.Visible = False
                Dim oOrganizacionCompras As FSNServer.OrganizacionCompras
                oOrganizacionCompras = FSNServer.Get_Object(GetType(FSNServer.OrganizacionCompras))
                oOrganizacionCompras.Cod = sCodOrgCompras
                oOrganizacionCompras.LoadData()
                If oOrganizacionCompras.Data.Tables(0).Rows.Count > 0 Then
                    lblOrgComprasNombre.Text = oOrganizacionCompras.Data.Tables(0).Rows(0).Item(0) & " - " & oOrganizacionCompras.Data.Tables(0).Rows(0).Item(1)
                Else
                    lblOrgComprasNombre.Text = ""
                End If
                oOrganizacionCompras = Nothing

            Else
                lblOrgComprasNombre.Visible = False
                'Carga el combo de ORGANIZACION DE COMPRAS
                Dim oOrganizacionesCompras As FSNServer.OrganizacionesCompras
                oOrganizacionesCompras = FSNServer.Get_Object(GetType(FSNServer.OrganizacionesCompras))
                oOrganizacionesCompras.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil)

                Dim ugtxtOrganizacionCompras_wdg As Infragistics.Web.UI.GridControls.WebDataGrid = ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg")
                ugtxtOrganizacionCompras_wdg.DataSource = oOrganizacionesCompras.Data
                ugtxtOrganizacionCompras_wdg.DataBind()

                If Not Page.IsPostBack And sCodOrgCompras <> "-1" Then
                    For Each oRow_ As Infragistics.Web.UI.GridControls.GridRecord In ugtxtOrganizacionCompras_wdg.Rows
                        If (oRow_.Items(0).Value = sCodOrgCompras) Then
                            ugtxtOrganizacionCompras_wdg.Behaviors.Selection.SelectedRows.Add(oRow_)
                            ugtxtOrganizacionCompras.Items(0).Text = oRow_.Items(1).Value
                            ugtxtOrganizacionCompras.Items(0).Value = oRow_.Items(0).Value
                            Exit For
                        End If
                    Next
                End If
                ugtxtOrganizacionCompras.TextField = "DEN"
                ugtxtOrganizacionCompras.ValueField = "COD"

                oOrganizacionesCompras = Nothing
            End If
            If CentroRO = True Then
                ugtxtCentros.Visible = False
                Dim oCentro As FSNServer.Centro
                oCentro = FSNServer.Get_Object(GetType(FSNServer.Centro))
                oCentro.Cod = sCodCentro
                oCentro.LoadData()
                If oCentro.Data.Tables(0).Rows.Count > 0 Then
                    lblCentroNombre.Text = oCentro.Data.Tables(0).Rows(0).Item(0) & " - " & oCentro.Data.Tables(0).Rows(0).Item(1)
                Else
                    lblCentroNombre.Visible = False
                End If
                oCentro = Nothing
            Else
                lblCentroNombre.Visible = False
                'Carga el combo de CENTROS
                Dim oCentros As FSNServer.Centros
                oCentros = FSNServer.Get_Object(GetType(FSNServer.Centros))
                If sCodCentro <> "" And sCodCentro <> "-1" And sCodOrgCompras = "-1" Then
                    oCentros.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil)
                Else
                    oCentros.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil, sCodOrgCompras:=sCodOrgCompras)
                End If

                Dim ugtxtCentros_wdg As Infragistics.Web.UI.GridControls.WebDataGrid = ugtxtCentros.Controls(0).Controls(0).FindControl("ugtxtCentros_wdg")
                ugtxtCentros_wdg.DataSource = oCentros.Data
                ugtxtCentros_wdg.DataBind()

                If Not Page.IsPostBack And sCodCentro <> "-1" Then
                    For Each oRow_ As Infragistics.Web.UI.GridControls.GridRecord In ugtxtCentros_wdg.Rows
                        If (oRow_.Items(0).Value = sCodCentro) Then
                            ugtxtCentros_wdg.Behaviors.Selection.SelectedRows.Add(oRow_)
                            ugtxtCentros.Items(0).Text = oRow_.Items(1).Value
                            ugtxtCentros.Items(0).Value = oRow_.Items(0).Value
                            Exit For
                        End If
                    Next
                End If
                ugtxtCentros.TextField = "DEN"
                ugtxtCentros.ValueField = "COD"

                ugtxtCentros.ClientEvents.ValueChanging = "ugtxtCentros_ValueChanging"

                oCentros = Nothing
            End If
        Else
            ugtxtOrganizacionCompras.Visible = False
            lblOrgCompras.Visible = False
            lblOrgComprasNombre.Visible = False
            ugtxtCentros.Visible = False
            lblCentro.Visible = False
            lblCentroNombre.Visible = False

            If Request("CodUnidadOrganizativa") IsNot Nothing Then
                If UnidadOrganizativaRO Then
                    pnlUnidadesOrganizativas.Visible = False
                    lblUnidadOrganizativaSeleccionada.Text = sCodUnidadOrganizativa
                    lblUnidadOrganizativaSeleccionada.Visible = True
                Else
                    lblUnidadOrganizativaSeleccionada.Visible = False
                    'Carga el combo de CENTROS
                    Dim oUnidadesOrganizativas As FSNServer.UnidadesOrg
                    oUnidadesOrganizativas = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))
                    oUnidadesOrganizativas.CargarTodasUnidadesOrganizativas(FSNUser.Cod, Idioma,
                                                FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil)
                    With wdtUnidadesOrganizativas
                        .DataSource = oUnidadesOrganizativas.Data
                        .DataBind()
                    End With
                    txtUnidadOrganizativa.Text = sCodUnidadOrganizativa
                    Dim sValor As String = sCodUnidadOrganizativa
                    Dim sUnidadOrg As String() = Split(sValor, " - ")
                    Dim sUON1 As String = String.Empty
                    Dim sUON2 As String = String.Empty
                    Dim sUON3 As String = String.Empty
                    Dim sDenUON As String = String.Empty
                    Dim iNivel As Integer = UBound(sUnidadOrg)
                    Select Case iNivel
                        Case 1
                            sUON1 = sUnidadOrg(0)
                        Case 2
                            sUON1 = sUnidadOrg(0)
                            sUON2 = sUnidadOrg(1)
                        Case 3
                            sUON1 = sUnidadOrg(0)
                            sUON2 = sUnidadOrg(1)
                            sUON3 = sUnidadOrg(2)
                    End Select

                    For Each node As Infragistics.Web.UI.NavigationControls.DataTreeNode In wdtUnidadesOrganizativas.AllNodes
                        If node.Level = 3 Then
                            If sCodUnidadOrganizativa = node.ParentNode.ParentNode.Key & " - " & node.ParentNode.Key & " - " & node.Key Then
                                sCodUnidadOrganizativa = sCodUnidadOrganizativa & " - " & node.Text
                                txtUnidadOrganizativa.Text = sCodUnidadOrganizativa

                                iNivel = 3
                                sUON2 = node.ParentNode.Key
                                sUON3 = node.Key

                                Exit For
                            End If
                        ElseIf node.Level = 2 Then
                            If sCodUnidadOrganizativa = node.ParentNode.Key & " - " & node.Key Then
                                sCodUnidadOrganizativa = sCodUnidadOrganizativa & " - " & node.Text
                                txtUnidadOrganizativa.Text = sCodUnidadOrganizativa

                                iNivel = 2
                                sUON2 = node.Key

                                Exit For
                            End If
                        ElseIf node.Level = 1 Then
                            If sCodUnidadOrganizativa = node.Key Then
                                sCodUnidadOrganizativa = sCodUnidadOrganizativa & " - " & node.Text
                                txtUnidadOrganizativa.Text = sCodUnidadOrganizativa

                                iNivel = 1
                                sUON1 = node.Key

                                Exit For
                            End If
                        End If
                    Next

                    For Each node As Infragistics.Web.UI.NavigationControls.DataTreeNode In wdtUnidadesOrganizativas.AllNodes
                        node.Expanded = Not node.Enabled
                        If node.Level = iNivel Then
                            Select Case iNivel
                                Case 1
                                    If node.Key = sUON1 Then
                                        node.Expanded = True
                                        node.Selected = True
                                    End If
                                Case 2
                                    If node.ParentNode.Key = sUON1 AndAlso node.Key = sUON2 Then
                                        node.ParentNode.Expanded = True
                                        node.Expanded = True
                                        node.Selected = True
                                    End If
                                Case 3
                                    If node.ParentNode.ParentNode.Key = sUON1 AndAlso node.ParentNode.Key = sUON2 AndAlso node.Key = sUON3 Then
                                        node.ParentNode.ParentNode.Expanded = True
                                        node.ParentNode.Expanded = True
                                        node.Expanded = True
                                        node.Selected = True
                                    End If
                            End Select
                        End If
                    Next
                    oUnidadesOrganizativas = Nothing
                End If
            Else
                pnlUnidadesOrganizativas.Visible = False
                lblUnidadOrganizativa.Visible = False
                lblUnidadOrganizativaSeleccionada.Visible = False
            End If
        End If

        oArts = FSNServer.Get_Object(GetType(FSNServer.Articulos))
        oArts.GMN1 = sGMN1
        oArts.GMN2 = sGMN2
        oArts.GMN3 = sGMN3
        oArts.GMN4 = sGMN4

        If Not Page.IsPostBack Or (bUsar_OrgCompras And (Request("txtCodOrgCompras") <> "" Or Request("txtCodCentro") <> "")) Or Not bUsar_OrgCompras Or (bUsar_OrgCompras And (OrgComprasRO = "1" Or CentroRO = "1")) Then
            txtCodOrgCompras.Value = ""
            txtCodCentro.Value = ""

            '-- Checkboxes de genéricos
            iGenericos = 2
            If hMostrarCaracteristicas.Value Then
                If chkGenerico.Checked AndAlso Not chkNoGenerico.Checked Then
                    iGenericos = 0
                ElseIf Not chkGenerico.Checked AndAlso chkNoGenerico.Checked Then
                    iGenericos = 1
                End If
            Else
                If chkGenerico2.Checked AndAlso Not chkNoGenerico2.Checked Then
                    iGenericos = 0
                ElseIf Not chkGenerico2.Checked AndAlso chkNoGenerico2.Checked Then
                    iGenericos = 1
                End If
            End If

            Dim bCatalogados As Nullable(Of Boolean) = Nothing  'NULL: los 2 o ninguno chequeado, False: no catalogados, True: catalogados
            If Usuario.AccesoEP And (Usuario.EPTipo = TipoAccesoFSEP.SoloAprovisionador Or Usuario.EPTipo = TipoAccesoFSEP.SoloAprobador Or Usuario.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador) Then
                If ExisteMasDeUnTipoDePedido() Then
                    If Not chkCatalogados.Checked.Equals(chkNoCatalogados.Checked) Then bCatalogados = chkCatalogados.Checked
                Else
                    If Not chkCatalogados2.Checked.Equals(chkNoCatalogados2.Checked) Then bCatalogados = chkCatalogados2.Checked
                End If
            End If

            Dim Negociados As Integer = 0
            Dim gParametrosGenerales As TiposDeDatos.ParametrosGenerales
            Dim sUon1 As String = ""
            Dim sUon2 As String = ""
            Dim sUon3 As String = ""
            Dim sUon4 As String = ""
            Dim bCargaFpago As Boolean = False

            IDFPago.Value = IIf(IsNothing(Request("IDFPago")), "", Request("IDFPago"))
            IDFPagoHidden.Value = IIf(IsNothing(Request("IDFPagoHidden")), "", Request("IDFPagoHidden"))

            If hTipoSolicitud.Value = TiposDeDatos.TipoDeSolicitud.PedidoNegociado Then
                Negociados = 1

                If gParametrosGenerales.gbAccesoFSSM Then
                    sUon1 = Request("Uon1")
                    sUon2 = Request("Uon2")
                    sUon3 = Request("Uon3")
                    sUon4 = Request("Uon4")
                End If

                bCargaFpago = ((IDFPago.Value <> "") OrElse (IDFPagoHidden.Value <> ""))

            ElseIf hTipoSolicitud.Value = TiposDeDatos.TipoDeSolicitud.PedidoExpress Then
                If bArticulosNegociadosExpress Then
                    Negociados = 3

                    bCargaFpago = ((IDFPago.Value <> "") OrElse (IDFPagoHidden.Value <> ""))
                Else
                    Negociados = 2
                End If

            End If
            Dim CodProveedor As String = ""
            If Not chkVerNoProveedor.Checked Then
                CodProveedor = hProveedor.Value
            End If
            Dim CodProveedorSumiArt As String = hProveedorSumiArt.Value

            Dim RestringirMaterialAsignadoUsuario As Integer = 0
            If hTipoSolicitud.Value <> TiposDeDatos.TipoDeSolicitud.NoConformidad AndAlso hTipoSolicitud.Value <> TiposDeDatos.TipoDeSolicitud.Certificado _
                AndAlso desde.Value <> "WebPartPanelCalidad" AndAlso desde.Value <> "WucBusquedaNC" Then
                RestringirMaterialAsignadoUsuario = AccionesDeSeguridad.PMRestringirMaterialAsignadoUsuario
            End If
            Dim UON1 As String = String.Empty
            Dim UON2 As String = String.Empty
            Dim UON3 As String = String.Empty
            Dim cambioUON As Integer = 1
            If Not sCodUnidadOrganizativa = String.Empty Then
                Select Case UBound(Split(sCodUnidadOrganizativa, " - "))
                    Case 1
                        UON1 = Split(sCodUnidadOrganizativa, " - ")(0)
                    Case 2
                        UON1 = Split(sCodUnidadOrganizativa, " - ")(0)
                        UON2 = Split(sCodUnidadOrganizativa, " - ")(1)
                    Case 3
                        UON1 = Split(sCodUnidadOrganizativa, " - ")(0)
                        UON2 = Split(sCodUnidadOrganizativa, " - ")(1)
                        UON3 = Split(sCodUnidadOrganizativa, " - ")(2)
                End Select
                If (Not (CType(Cache("oTblAtributos_" & FSNUser.Cod), DataTable)) Is Nothing) Then
                    cambioUON = 0
                End If
                'Si al abrir la página ya hay UON seleccionada cargo sus atributos y valores por defecto para hacer la primera búsqueda
                If Not Page.IsPostBack Then
                    Dim misAtributos As List(Of Fullstep.FSNServer.UnidadesOrg.Atributo)
                    misAtributos = CargarAtributosUON(UON1, UON2, UON3)
                    GuardarObjetoAtributosUON(misAtributos, UON1, UON2, UON3)
                    'Le digo que ha habido cambio para que genere el objeto gAtributosUON con el que luego crea la tabla 
                    cambioUON = 1
                End If
                'Cojo los valores de los atributos por UON
                CargarAtributos(UON1, UON2, UON3, cambioUON)
            End If
            If Not sCodOrgCompras = String.Empty Then
                Dim miArray() As String = {"", "", ""}
                miArray = DevolverUONOrgCompras(sCodOrgCompras, sCodCentro)
                If (Not (CType(Cache("oTblAtributos_" & FSNUser.Cod), DataTable)) Is Nothing) Then
                    cambioUON = 0
                End If

                'Si al abrir la página ya hay OrgCompras seleccionada cargo sus atributos y valores por defecto para hacer la primera búsqueda
                If Not Page.IsPostBack Then
                    Dim misAtributos As List(Of Fullstep.FSNServer.UnidadesOrg.Atributo)
                    misAtributos = CargarAtributosUON(miArray(0), miArray(1), miArray(2))
                    GuardarObjetoAtributosUON(misAtributos, miArray(0), miArray(1), miArray(2))
                    'Le digo que ha habido cambio para que genere el objeto gAtributosUON con el que luego crea la tabla 
                    cambioUON = 1
                End If
                'Cojo los valores de los atributos por UON
                CargarAtributos(miArray(0), miArray(1), miArray(2), cambioUON)
            End If

            Dim bRestricUONSolUsuUON As Boolean = IIf(iTipoSolicit <> TiposDeDatos.TipoDeSolicitud.Certificado And iTipoSolicit <> TiposDeDatos.TipoDeSolicitud.NoConformidad, FSNUser.PMRestriccionUONSolicitudAUONUsuario, False)
            Dim bRestricUONSolPerfUON As Boolean = IIf(iTipoSolicit <> TiposDeDatos.TipoDeSolicitud.Certificado And iTipoSolicit <> TiposDeDatos.TipoDeSolicitud.NoConformidad, FSNUser.PMRestriccionUONSolicitudAUONPerfil, False)
            Dim iFav As Integer = 0
            If chkSoloFavoritos.Visible Then
                If chkSoloFavoritos.Checked Then iFav = 1
            ElseIf chkSoloFavoritos2.Visible Then
                If chkSoloFavoritos2.Checked Then iFav = 1
            End If


            If Not ((sCod Is Nothing Or sCod Is String.Empty) And (sDen Is Nothing Or sDen Is String.Empty) _
            And sUon1 Is String.Empty And UON1 Is String.Empty And oArts.GMN1 Is String.Empty And (sCodOrgCompras Is String.Empty Or sCodOrgCompras Is Nothing)) Then
                oArts.LoadData(FSNUser.Cod, bRestricUONSolUsuUON, bRestricUONSolPerfUON, sCod:=sCod, sDen:=sDen, sPer:=FSNUser.CodPersona, sIdioma:=Idioma, sMoneda:=sInstanciaMoneda,
                bCargarUltAdj:=(bCargarUltAdj Or FSNUser.FSPMVisualizar_Ult_ADJ), iGenericos:=iGenericos, sCodOrgCompras:=sCodOrgCompras,
                sCodCentro:=sCodCentro, bCargarCC:=False, bGasto:=chkGasto.Checked,
                bInversion:=chkInversion.Checked, bGastoInversion:=chkGastoInversion.Checked, bNoAlmacenable:=chkNoAlmacenable.Checked,
                bAlmacenable:=chkAlmacenable.Checked, bAlmacenOpcional:=chkAlmacenamientoOpcional.Checked, bRecepcionOblig:=chkRecepcionObligatoria.Checked,
                bNoRecepcionable:=chkNoRecepcionable.Checked, bRecepcionOpcional:=chkRecepcionOpcional.Checked, bSoloCatalogados:=bCatalogados,
                Negociados:=Negociados, CodProveedor:=CodProveedor, CargaFormPago:=bCargaFpago, UsarOrgCompras:=Acceso.gbUsar_OrgCompras,
                AccesoFSSM:=gParametrosGenerales.gbAccesoFSSM, sUon1:=sUon1, sUon2:=sUon2, sUon3:=sUon3, sUon4:=sUon4,
                RestringirMaterialUsuario:=RestringirMaterialAsignadoUsuario, RestringirPedidosArticulosUONUsuario:=Usuario.RestringirPedidosArticulosUONUsuario,
                RestringirPedidosArticulosUONPerfil:=Usuario.RestringirPedidosArticulosUONPerfil,
                UON1:=IIf(UON1 Is String.Empty, Nothing, UON1), UON2:=IIf(UON2 Is String.Empty, Nothing, UON2), UON3:=IIf(UON3 Is String.Empty, Nothing, UON3),
                AtribUON:=CType(Cache("oTblAtributos_" & FSNUser.Cod), DataTable), CodProveedorSumiArt:=CodProveedorSumiArt, iFav:=iFav, AccesoFSEP:=FSNServer.TipoAcceso.gbAccesoFSEP)
            End If

            Dim oGrid As Infragistics.Web.UI.GridControls.WebDataGrid
            oGrid = wdgArticulos
            oGrid.Rows.Clear()
            oGrid.Columns.Clear()
            If Not oArts.Data Is Nothing Then
                oGrid.DataSource = oArts.Data
            Else
                Dim oDS = New DataSet
                Dim oTable As DataTable = oDS.Tables.Add("TEMP")

                With oTable.Columns
                    .Add("FAVORITO", System.Type.GetType("System.Int32"))
                    .Add("COD", System.Type.GetType("System.String"))
                    .Add("DEN", System.Type.GetType("System.String"))
                    .Add("GENERICO", System.Type.GetType("System.Int32"))
                    .Add("CONCEPTO", System.Type.GetType("System.Int32"))
                    .Add("ALMACENAR", System.Type.GetType("System.Int32"))
                    .Add("RECEPCIONAR", System.Type.GetType("System.Int32"))
                    .Add("GMN1", System.Type.GetType("System.String"))
                    .Add("GMN2", System.Type.GetType("System.String"))
                    .Add("GMN3", System.Type.GetType("System.String"))
                    .Add("GMN4", System.Type.GetType("System.String"))
                    .Add("UNI", System.Type.GetType("System.String"))
                    .Add("DENGMN", System.Type.GetType("System.String"))
                    .Add("DENUNI", System.Type.GetType("System.String"))
                    .Add("NUMDEC", System.Type.GetType("System.String"))
                    .Add("PRES5_NIV0", System.Type.GetType("System.String"))
                    .Add("PRES5_NIV1", System.Type.GetType("System.String"))
                    .Add("PRES5_NIV2", System.Type.GetType("System.String"))
                    .Add("PRES5_NIV3", System.Type.GetType("System.String"))
                    .Add("PRES5_NIV4", System.Type.GetType("System.String"))
                    .Add("PERMISO_EP", System.Type.GetType("System.String"))
                    .Add("ESTA_CATALOGADO", System.Type.GetType("System.String"))
                    .Add("PREC_VIGENTE", System.Type.GetType("System.String"))
                    .Add("TIPORECEPCION", System.Type.GetType("System.Int32"))
                    .Add("TIENE_PERMISO", System.Type.GetType("System.Int32"))
                    .Add("PREC_ULT_ADJ", System.Type.GetType("System.Double"))
                    .Add("CODPROV_ULT_ADJ", System.Type.GetType("System.String"))
                    .Add("DENPROV_ULT_ADJ", System.Type.GetType("System.String"))
                    .Add("CODORGCOMPRAS", System.Type.GetType("System.String"))
                    .Add("DENORGCOMPRAS", System.Type.GetType("System.String"))
                    .Add("CODCENTRO", System.Type.GetType("System.String"))
                    .Add("DENCENTRO", System.Type.GetType("System.String"))
                    .Add("UNI_ULT_ADJ", System.Type.GetType("System.String"))
                    .Add("DENUNI_ULT_ADJ", System.Type.GetType("System.String"))
                    .Add("MON_ULT_ADJ", System.Type.GetType("System.String"))
                    .Add("DENMON_ULT_ADJ", System.Type.GetType("System.String"))
                    .Add("PAG_ULT_ADJ", System.Type.GetType("System.String"))
                    .Add("DENPAG_ULT_ADJ", System.Type.GetType("System.String"))
                End With

                oGrid.DataSource = oDS
                End If
                CrearColumnas()
                ConfigurarGrid()
                oGrid.DataBind()
                InsertarEnCache("oArticulos_" & FSNUser.Cod, oGrid.DataSource)
            End If
    End Sub

    Private Sub CargarDatos()
        wdgArticulos.DataSource = CType(Cache("oArticulos_" & FSNUser.Cod), DataSet)
        CrearColumnas()
        ConfigurarGrid()
        wdgArticulos.DataBind()
        Dim bUsar_OrgCompras As Boolean = Not (Request("CodOrgCompras") Is Nothing)
        Dim cambioUON As Integer = 1
        If (bUsar_OrgCompras And (Request("txtCodOrgCompras") <> "" Or Request("txtCodCentro") <> "")) Or (Not bUsar_OrgCompras Or (Request("txtUnidadOrganizativa") <> "")) Then
            If Cache("oTblAtributos_" & FSNUser.Cod) Is Nothing Then
                cambioUON = 0
                If bUsar_OrgCompras Then
                    Dim miArray() As String = {"", "", ""}
                    miArray = DevolverUONOrgCompras(Request("txtCodOrgCompras"), Request("txtCodCentro"))
                    CargarAtributos(miArray(0), miArray(1), miArray(2), cambioUON)
                Else
                    Dim UON1 As String = String.Empty
                    Dim UON2 As String = String.Empty
                    Dim UON3 As String = String.Empty
                    Dim sCodUO As String = Request("txtUnidadOrganizativa")
                    Select Case UBound(Split(sCodUO, " - "))
                        Case 1
                            UON1 = Split(sCodUO, " - ")(0)
                        Case 2
                            UON1 = Split(sCodUO, " - ")(0)
                            UON2 = Split(sCodUO, " - ")(1)
                        Case 3
                            UON1 = Split(sCodUO, " - ")(0)
                            UON2 = Split(sCodUO, " - ")(1)
                            UON3 = Split(sCodUO, " - ")(2)
                    End Select
                    CargarAtributos(UON1, UON2, UON3, cambioUON)
                End If
            End If
        End If
    End Sub

    'Recojo los atributos de la UON seleccionada y sus valores por defecto
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function DevolverUONOrgCompras(ByVal sCodOrgCompras As String, ByVal sCodCentro As String) As Object
        Try
            Dim oUONData As FSNServer.UnidadesOrg
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")

            oUONData = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))
            oUONData.CargarUonDeOrgCompras(sCodOrgCompras, sCodCentro)
            Dim miArray() As String = {"", "", ""}
            If oUONData.Data.Tables(0).Rows.Count > 0 Then
                If DBNullToSomething(oUONData.Data.Tables(0).Rows(0).Item("UON1")) <> Nothing Then
                    miArray(0) = oUONData.Data.Tables(0).Rows(0).Item("UON1")
                End If
                If DBNullToSomething(oUONData.Data.Tables(0).Rows(0).Item("UON2")) <> Nothing Then
                    miArray(1) = oUONData.Data.Tables(0).Rows(0).Item("UON2")
                End If
                If DBNullToSomething(oUONData.Data.Tables(0).Rows(0).Item("UON3")) <> Nothing Then
                    miArray(2) = oUONData.Data.Tables(0).Rows(0).Item("UON3")
                End If
            End If
            Return miArray
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Recojo los atributos de la UON seleccionada y sus valores por defecto
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function CargarAtributosUON(ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String) As Object
        Try
            Dim oUONData As FSNServer.UnidadesOrg
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")

            oUONData = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))
            Dim oAtributos As List(Of Fullstep.FSNServer.UnidadesOrg.Atributo)
            oAtributos = oUONData.CargarAtributosDeUnidadOrganizativa(LTrim(UON1), LTrim(UON2), LTrim(UON3))
            Return oAtributos
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ' Meter en una tabla los atributos de UON
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Sub GuardarObjetoAtributosUON(ByVal objAtributosUON As Object, ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oUONData As FSNServer.UnidadesOrg
            oUONData = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))
            Dim oAtributos As List(Of Fullstep.FSNServer.UnidadesOrg.Atributo)
            Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer()
            oAtributos = serializer.Deserialize(Of List(Of Fullstep.FSNServer.UnidadesOrg.Atributo))(serializer.Serialize(DirectCast(objAtributosUON, Object)).ToString)

            Dim Contexto As System.Web.HttpContext
            Contexto = System.Web.HttpContext.Current
            'Meto los datos a una tabla con la estructura de ART4_UON_ATRIB
            Dim tablaAtribUON As New DataTable
            Dim rAtrib As DataRow
            tablaAtribUON.Columns.Add("UON1", GetType(String))
            tablaAtribUON.Columns.Add("UON2", GetType(String))
            tablaAtribUON.Columns.Add("UON3", GetType(String))
            tablaAtribUON.Columns.Add("ATRIB", GetType(Integer))
            tablaAtribUON.Columns.Add("VALOR_NUM", GetType(Double))
            tablaAtribUON.Columns.Add("VALOR_TEXT", GetType(String))
            tablaAtribUON.Columns.Add("VALOR_FEC", GetType(DateTime))
            tablaAtribUON.Columns.Add("VALOR_BOOL", GetType(Byte))

            For Each oAtributo As Fullstep.FSNServer.UnidadesOrg.Atributo In oAtributos
                rAtrib = tablaAtribUON.NewRow
                With rAtrib
                    .Item("ATRIB") = oAtributo.Id
                    .Item("UON1") = UON1
                    .Item("UON2") = UON2
                    .Item("UON3") = UON3
                    Select Case oAtributo.Tipo
                        Case 1
                            If oAtributo.Valor = "" Then
                                .Item("VALOR_TEXT") = DBNull.Value
                            Else
                                .Item("VALOR_TEXT") = oAtributo.Valor
                            End If
                        Case 2
                            If DBNullToStr(oAtributo.Valor) = "" Then
                                .Item("VALOR_NUM") = DBNull.Value
                            Else
                                .Item("VALOR_NUM") = oAtributo.Valor
                            End If
                        Case 3
                            If DBNullToStr(oAtributo.Valor) = "" Then
                                .Item("VALOR_FEC") = DBNull.Value
                            Else
                                .Item("VALOR_FEC") = oAtributo.Valor
                            End If
                        Case 4
                            If oAtributo.Valor = "" Then
                                .Item("VALOR_BOOL") = DBNull.Value
                            Else
                                .Item("VALOR_BOOL") = oAtributo.Valor
                            End If
                    End Select
                End With
                If Not (rAtrib.IsNull("VALOR_TEXT") And rAtrib.IsNull("VALOR_NUM") And rAtrib.IsNull("VALOR_BOOL") And rAtrib.IsNull("VALOR_FEC")) Then
                    'Si todos los VALOR_xx son dbnull no hacer el add a la tabla
                    tablaAtribUON.Rows.Add(rAtrib)
                End If
            Next
            Contexto.Cache.Insert("oTblAtributos_" & FSNUser.Cod, tablaAtribUON, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub wdtUnidadesOrganizativas_NodeBound(sender As Object, e As Infragistics.Web.UI.NavigationControls.DataTreeNodeEventArgs) Handles wdtUnidadesOrganizativas.NodeBound
        e.Node.Enabled = CType(e.Node.DataItem.Item("ACTIVA"), Boolean)
        Select Case e.Node.Level
            Case 0
                e.Node.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "Images/world.gif"
            Case 1
                e.Node.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "Images/carpetaAzulOscuro.gif"
            Case 2
                e.Node.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "Images/carpetaCommodityTxiki.gif"
            Case Else
                e.Node.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "Images/carpetagris.gif"
        End Select
    End Sub

    Private Sub CargarAtributos(sUON1, sUON2, sUON3, cambioUON)
        Dim sScript As String
        sScript = "$(document).ready(function(){"
        sScript += vbCrLf
        sScript += "CargarTablaAtributos('" & sUON1 & "','" & sUON2 & "','" & sUON3 & "'," & cambioUON & ") "
        sScript += vbCrLf
        sScript += "});"
        If Not Me.ClientScript.IsStartupScriptRegistered("jQuery") Then _
            ScriptManager.RegisterStartupScript(upAtributosUON, Me.GetType, "jQuery", sScript, True)
    End Sub

    Private Sub GuardarAtributosUON(sUON1, sUON2, sUON3)
        Dim sScript As String
        sScript = "$(document).ready(function(){"
        sScript += vbCrLf
        sScript += "GuardarDatosAtribUON('" & sUON1 & "','" & sUON2 & "','" & sUON3 & "') "
        sScript += vbCrLf
        sScript += "});"
        If Not Me.ClientScript.IsStartupScriptRegistered("GuardarAtribUON") Then _
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "GuardarAtribUON", sScript, True)

    End Sub

    ''' <summary>
    '''Determinar para conocer la altura aproximada 
    '''1) si van a salir atributos de uon y cuantos atributos de uon saldrian
    '''2) q panel de "otras caracteristicas" va a salir
    ''' </summary>
    ''' <param name="sCodUnidadOrganizativa"></param>
    ''' <param name="sCodOrgCompras"></param>
    ''' <param name="sCodCentro"></param>
    ''' <returns>Array {Si hay atributos, Cuantos atributos hay, Si sale panel "caracteristicas" grande}</returns>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function DeterminarAturaAtrbsUon(sCodUnidadOrganizativa, sCodOrgCompras, sCodCentro) As Object
        Try
            Dim miArray() As String = {"0", "0", "0"}

            Dim sUON1 As String = String.Empty
            Dim sUON2 As String = String.Empty
            Dim sUON3 As String = String.Empty

            Dim misAtributos As List(Of Fullstep.FSNServer.UnidadesOrg.Atributo)

            If Not sCodUnidadOrganizativa = String.Empty Then
                Select Case UBound(Split(sCodUnidadOrganizativa, " - "))
                    Case 1
                        sUON1 = Split(sCodUnidadOrganizativa, " - ")(0)
                    Case 2
                        sUON1 = Split(sCodUnidadOrganizativa, " - ")(0)
                        sUON2 = Split(sCodUnidadOrganizativa, " - ")(1)
                    Case 3
                        sUON1 = Split(sCodUnidadOrganizativa, " - ")(0)
                        sUON2 = Split(sCodUnidadOrganizativa, " - ")(1)
                        sUON3 = Split(sCodUnidadOrganizativa, " - ")(2)
                End Select
                misAtributos = CargarAtributosUON(sUON1, sUON2, sUON3)

                miArray(0) = IIf(misAtributos.Count > 0, "1", "0")
                miArray(1) = IIf(misAtributos.Count > 0, misAtributos.Count, "0")
            ElseIf Not sCodOrgCompras = String.Empty Then
                Dim UonArray() As String = {"", "", ""}
                UonArray = DevolverUONOrgCompras(sCodOrgCompras, sCodCentro)

                misAtributos = CargarAtributosUON(UonArray(0), UonArray(1), UonArray(2))

                miArray(0) = IIf(misAtributos.Count > 0, "1", "0")
                miArray(1) = IIf(misAtributos.Count > 0, misAtributos.Count, "0")
            End If

            Dim oTiposPedido As FSNServer.TiposPedido
            Dim ds As DataSet
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")

            oTiposPedido = FSNServer.Get_Object(GetType(FSNServer.TiposPedido))
            ds = oTiposPedido.LoadData(System.Configuration.ConfigurationManager.AppSettings("idioma"))

            miArray(2) = IIf(ds.Tables(0).Rows.Count > 1, "1", "0")
            Return miArray
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub chkAnyadirAutoFav_CheckedChanged(sender As Object, e As EventArgs) Handles chkAnyadirAutoFav.CheckedChanged
        FSNUser.Actualizar_AnyadirArticuloFav(chkAnyadirAutoFav.Checked)
        FSNUser.PMAnyadirArticuloFav = chkAnyadirAutoFav.Checked
    End Sub
End Class
