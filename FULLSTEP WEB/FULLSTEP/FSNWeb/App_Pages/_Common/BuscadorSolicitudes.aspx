﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BuscadorSolicitudes.aspx.vb"
    Inherits="Fullstep.FSNWeb.BuscadorSolicitudes" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>      
</head>
<body style="background-color: #FFFFFF; margin-top: 0px; margin-left: 0px; margin-right: 0px">
    <form id="frmBuscadorSolicitudes" name="frmBuscadorSolicitudes" method="post" runat="server">
    
    <script type="text/javascript">
        /*''' <summary>
        ''' Para solo admitir numeros en el campo txtIdentificado
        ''' </summary>
        ''' <param name="key">Tecla pulsada</param>
        ''' <remarks>Llamada desde: sistema; Tiempo máximo:0</remarks>*/
        function txtIdentificador_KeyPress(key) {
            if (!((event.keyCode >= 48) && (event.keyCode <= 57)))
                event.keyCode = 0;
        }
        /*''' <summary>
        ''' Para seleccionar un Peticionario
        ''' </summary>
        ''' <param name="IDCONTROL">Peticionario ctrl q muestra la denominacion</param>
        ''' <param name="codigo">Peticionario codigo</param>
        ''' <param name="denominacion">Peticionario denominacion</param>
        ''' <param name="email">Peticionario email</param>
        ''' <remarks>Llamada desde: PMWeb\script\_common\usuarios.aspx; Tiempo máximo:0</remarks>*/
        function usu_seleccionado(IDCONTROL, codigo, denominacion, email) {
            document.frmBuscadorSolicitudes.hfPeticionario.value = codigo;
            textbox = document.getElementById(IDCONTROL);
            if (textbox != null)
                textbox.value = denominacion;
        }
        /*''' <summary>
        ''' Añade la solicitud y cierra esta pantalla, si se ha seleccionado una vinculación a nivel de solicitud 
        ''' eoc devuelve true para q salte el btnSiguiente onclientclick
        ''' </summary>
        ''' <param name="sRoot">nombre entry del desglose</param>     
        ''' <param name="idCampo">id de bbdd del desglose</param>    
        ''' <returns>False si no se ha seleccionado nada
        '''     False si se ha seleccionado una vinculación a nivel de solicitud
        '''     True si se ha seleccionado una vinculación a nivel de desglose
        ''' </returns>
        ''' <remarks>Llamada desde: btnSiguiente/onclick    btnAceptar1/onclick; Tiempo máximo:0,5</remarks>*/
        function Siguiente(sRoot, IdCampo) {
            p = window.opener
            if (p) {
                grid = $find("wdgSolicitudes");
                row = grid.get_behaviors().get_selection().get_selectedRows(0);
                if (!row) {
                    alert(sMensajeSolicitud);
                    return false;
                }

                NLinDesglose = 0;
                NLinSolic = 0;

                for (i = 0; i < grid.get_behaviors().get_selection().get_selectedRows().get_length(); i++) {
                    selectedRow = grid.get_behaviors().get_selection().get_selectedRows();

                    cell = selectedRow.getItem(i).get_cellByColumnKey("IDDESGLOSEORIGEN");
                    sDesgl = cell.get_value();
                    if (sDesgl == 0) {
                        NLinSolic++;
                    }
                    else {
                        NLinDesglose++;
                    }

                    if ((NLinSolic > 0) && (NLinDesglose > 0)) {//Controlar Multiselección: Solo solicitudes sin desglose vinculado 
                        alert(sMensajeUnTipo);
                        return false;
                    }

                    if (NLinDesglose > 1) {//Solo una solicitud con desglose vinculado para ver grid de lineas
                        alert(sMensajeSolicitud);
                        return false;
                    }
                }

                if (NLinSolic > 0) {
                    for (i = 0; i < grid.get_behaviors().get_selection().get_selectedRows().get_length(); i++) {
                        selectedRow = grid.get_behaviors().get_selection().get_selectedRows();

                        cell = selectedRow.getItem(i).get_cellByColumnKey("ID");
                        sID = cell.get_value();
                        cell = selectedRow.getItem(i).get_cellByColumnKey("DEN");
                        sDEN = cell.get_value();

                        p.SeleccionarSolicitud(sRoot, IdCampo, sID, sDEN);
                    }
                    window.close();
                    return false;
                }
            }
            return true;
        }
        /*''' <summary>
        ''' Mover una linea de una instancia a otra
        ''' </summary>  
        ''' <param name="sRoot">nombre entry del desglose</param>        
        ''' <param name="idCampo">id de bbdd del desglose</param>          
        ''' <param name="index">fila q mueves</param>
        ''' <param name="Celda">Celda, html, donde esta el bt mover/copiar/elim. A traves de él se saca la fila y tabla html</param>
        ''' <param name="Frame">Frame donde esta el desglose a borrar</param>        
        ''' <param name="PopUp">Indica si es Popup o no</param>        
        ''' <returns>False si no se ha seleccionado nada</returns>
        ''' <remarks>Llamada desde: btnAceptar1/onclick; Tiempo máximo:0,1</remarks>*/
        function Mover(sRoot, IdCampo, Index, Celda, Frame, PopUp) {
            p = window.opener
            if (p) {
                grid = $find("wdgSolicitudes");
                row = grid.get_behaviors().get_selection().get_selectedRows(0);
                if (!row) {
                    alert(sMensajeSolicitud);
                    return false;
                }
                cell = row.getItem(0).get_cellByColumnKey("ID");
                sID = cell.get_value();

                p.VincularMoverAInstancia(sRoot, IdCampo, sID, Index, p.document.getElementById(Celda), Celda, Frame);

                window.close();
            }
        }
        function Importar() {
            grid = $find("wdgSolicitudes");
            row = grid.get_behaviors().get_selection().get_selectedRows(0);
            if (!row) {
                alert(sMensajeSolicitud);
                return false;
            }
            cell = row.getItem(0).get_cellByColumnKey("ID");
            sID = cell.get_value();
            window.opener.open(rutaPM + 'alta/NWAlta.aspx?IdInstanciaImportar=' + sID + '*Solicitud=' + lSolicitud, "_self");
            window.close();
        }        
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        <CompositeScript>
            <Scripts>
                <asp:ScriptReference Path="../../js/jsUpdateProgress.js" />
            </Scripts>
        </CompositeScript>
    </asp:ScriptManager>
    <asp:Panel ID="pnlTitulo" runat="server" Width="100%">
        <table width="100%" cellpadding="0" border="0">
            <tr>
                <td>
                    <fsn:FSNPageHeader ID="FSNPageHeader" runat="server">
                    </fsn:FSNPageHeader>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlStep1" runat="server" Width="100%" Height="100%">
        <asp:Panel ID="pnlParametros" runat="server" Width="100%">
            <table width="100%" style="table-layout: fixed; padding-top: 4px; padding-bottom: 4px"
                border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 105px; padding-left: 5px;">
                        <asp:Label ID="lblIdentificador" runat="server" Text="dIdentificador:" Font-Bold="true"></asp:Label>
                    </td>
                    <td style="width: 110px">
                        <asp:TextBox ID="txtIdentificador" runat="server" Width="85px" onKeyPress="txtIdentificador_KeyPress(window.event.keyCode);"></asp:TextBox>
                    </td>
                    <td style="width: 215px">
                        <table>
                            <tr>
                                <td style="width: 50px; padding-left: 5px;">
                                    <asp:Label ID="lblEstado" runat="server" Text="dEstado:" Font-Bold="true"></asp:Label>
                                </td>
                                <td style="padding-left: 3px;">
                                    <ig:WebDropDown ID="wddEstado" runat="server" Width="148px" DropDownContainerWidth="148"
                                        DropDownAnimationDuration="1" EnableDropDownAsChild="false" EnableCustomValues="false">
                                    </ig:WebDropDown>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 30px;"></td>
                    <td style="width: 95px; padding-left: 5px;">
                        <asp:Label ID="lblDenominacion" runat="server" Text="dDenominacion:" Font-Bold="true"></asp:Label>
                    </td>
                    <td style="padding-left:2px">
                        <asp:TextBox ID="txtDenominacion" runat="server" Width="307px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 105px; padding-left: 5px; padding-top: 5px;">
                        <asp:Label ID="lblTipoSolicitud" runat="server" Text="dTipo solicitud:" Font-Bold="true"></asp:Label>
                    </td>
                    <td style="width: 325px; padding-top: 5px;" colspan="2">
                        <ig:WebDropDown ID="wddTipoSolicitud" runat="server" Width="320px" DropDownAnimationDuration="1"
                            DropDownContainerWidth="320" EnableDropDownAsChild="false" EnableCustomValues="false">
                        </ig:WebDropDown>
                        <asp:Label ID="lblDenTipoSolicitud" runat="server" Text="dSolicitud de proyecto" Visible="false" Width="310px"></asp:Label>
                        <asp:HiddenField ID="hfTipoSolicitud" runat="server" />
                    </td>
                    <td style="width: 30px;"></td>
                    <td style="width: 95px; padding-left: 5px;">
                        <asp:Label ID="lblPeticionario" runat="server" Text="dPeticionario:" Font-Bold="true"></asp:Label>
                    </td>
                    <td style="width: 305px; padding-left:2px">
                        <table id="tblPeticionario" runat="server" style="background-color: White; width: 305px; height: 20px; border: solid 1px #BBBBBB"
                            cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtPeticionario" runat="server" Width="290px" BorderWidth="0px"></asp:TextBox>
                                </td>
                                <td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px">
                                    <asp:ImageButton ID="imgPeticionarioLupa" runat="server" SkinID="BuscadorLupa" />
                                </td>
                            </tr>
                        </table>
                        <asp:Label ID="lblDenPeticionario" runat="server" Visible="false" Width="290px"></asp:Label>
                        <asp:HiddenField ID="hfPeticionario" runat="server" />
                    </td>
                </tr>
                <tr>                    
                    <td style="width: 105px; padding-left: 5px;">
                        <asp:Label ID="lblFechaAltaDesde" runat="server" Text="dFecha alta desde:" Font-Bold="true"></asp:Label>
                    </td>
                    <td style="width: 110px; padding-top:5px;">             
                        <igpck:WebDatePicker runat="server" ID="dteFechaAltaDesde" Width="100px" SkinID="Calendario"></igpck:WebDatePicker>
                    </td>
                    <td style="width: 215px">
                        <table>
                            <tr>
                                <td style="width: 105px; padding-left: 5px; white-space:nowrap;">
                                    <asp:Label ID="lblFechaAltaHasta" runat="server" Text="dFecha alta hasta:" Font-Bold="true"></asp:Label>
                                </td>
                                <td style="padding-left: 5px; padding-top:5px;">
                                    <igpck:WebDatePicker runat="server" ID="dteFechaAltaHasta" Width="100px" SkinID="Calendario"></igpck:WebDatePicker>
                                </td>
                            </tr>
                        </table>                                
                    </td>
                    <td style="width: 30px;"></td>
                    <td style="width: 95px; padding-left: 5px;">
                        <asp:Label ID="lblImporteDesde" runat="server" Text="dImporte desde:" Font-Bold="true"></asp:Label>
                    </td>
                    <td style="width: 305px">
                        <table>
                            <tr>
                                <td style="width: 100px">
                                    <igpck:WebNumericEditor ID="wneImporteDesde" runat="server" Width="95px"></igpck:WebNumericEditor>
                                </td>
                                <td style="width: 90px; padding-right: 5px; text-align:right;">
                                    <asp:Label ID="lblImporteHasta" runat="server" Text="dImporte hasta:" Width="90px"
                                        Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    <igpck:WebNumericEditor ID="wneImporteHasta" runat="server" Width="100px"></igpck:WebNumericEditor>
                                </td>
                            </tr>
                        </table>
                    </td>                                
                </tr>
                <tr>
                    <td style="width: 105px; padding-left: 5px;">
                        <asp:Label ID="lblArticulo" runat="server" Text="dArticulo:" Font-Bold="true"></asp:Label>
                    </td>
                    <td style="padding-top:5px;" colspan="4">
                        <asp:TextBox ID="txtArticulo" runat="server" Width="320px"></asp:TextBox>
                        <ajx:AutoCompleteExtender ID="txtArticulo_AutoCompleteExtender" 
                         runat="server" CompletionSetCount="10" Enabled="True" DelimiterCharacters=""
                         MinimumPrefixLength="3" ServiceMethod="GetArticulos" ServicePath="App_Services/AutoComplete.asmx"
                         TargetControlID="txtArticulo" EnableCaching="False"></ajx:AutoCompleteExtender >                        
                    </td>
                    <td style="width: 305px">
                        <table>
                            <tr>
                                <td style="padding-left:255px">
                                    <fsn:FSNButton ID="btnBuscar" Text="DBuscar" runat="server" >                    
                                        &nbsp;&nbsp;&nbsp;&nbsp;                    
                                    </fsn:FSNButton>                                
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="pnlGridSolicitudes" runat="server" Width="100%">
            <!--Filtro -->
            <asp:Panel ID="pnlSinPaginacion" runat="server" Visible="True" Width="98%">
                <div style="border-top: 1px solid #CCCCCC;">
                    <table border="0" width="100%">
                        <tr>
                            <td>
                                <asp:Label ID="lblFiltrarPor" runat="server" Text="DFiltrar Por:" Font-Bold="true"></asp:Label>
                                <asp:RadioButton ID="optFiltrar1" GroupName="optFiltrarPor" runat="server" AutoPostBack="true" />
                                <asp:ImageButton ID="imgFiltrar1" SkinID="Filtro_Cabecera" runat="server" />
                                <asp:RadioButton ID="optFiltrar2" GroupName="optFiltrarPor" runat="server" AutoPostBack="true" />
                                <asp:ImageButton ID="imgFiltrar2" SkinID="Filtro_Columna" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <!--Grid Solicitudes -->
            <asp:UpdatePanel ID="UpdatePanelSolicitudes" runat="server" UpdateMode="conditional" ChildrenAsTriggers="false">
                <ContentTemplate>
                    <table border="0" width="98%" style="margin-left: 5px; margin-right: 5px;" cellspacing="0">
                        <tr>
                            <td style="height: 5px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="divSolicitudes">
                                    <ig:WebDataGrid ID="wdgSolicitudes" runat="server" Width="100%" Height="200px" SkinID="Visor" Browser="Xml"
                                    AutoGenerateBands="false" AutoGenerateColumns="false" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="true">
                                        <Behaviors>
                                            <ig:Selection  RowSelectType="Single" Enabled="True" CellClickAction="Row" ></ig:Selection> 
                                            <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true"  AnimationEnabled="true"> 
                                            </ig:Filtering> 
                                            <ig:RowSelectors Enabled="false" RowNumbering="false"></ig:RowSelectors>
                                            <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
                                        </Behaviors>
                                    </ig:WebDataGrid>
                                </div>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </asp:Panel>
    <!-- Panel del grid de lineas de solicitud -->
    <asp:Panel ID="pnlStep2" runat="server" Width="100%" Height="100%" Visible="false">
        <asp:Panel ID="pnlInfo" runat="server" Width="100%">
            <table border="0" width="100%" style="table-layout:fixed;">
                <tr>
                    <td style="text-overflow:ellipsis; white-space:nowrap; overflow:hidden;">
                        <asp:Label ID="lblDescSolicitud" runat="server" Text="[Descripcion de la solicitud]"
                            Font-Bold="true" cssClass="RotuloGrande" ToolTip="[Descripcion de la solicitud]"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblInfo1" runat="server" Text="DSeleccione las lineas que desee añadir a su solicitud"
                            Font-Bold="true"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="pnlGridLineas" runat="server" Width="100%">
            <!--Filtro -->
            <asp:Panel ID="pnlSinPaginacion2" runat="server" Visible="True" Width="98%">
                <div style="border-top: 1px solid #CCCCCC;">
                    <table border="0" width="100%">
                        <tr>
                            <td>
                                <asp:Label ID="lblFiltrarPor_2" runat="server" Text="DFiltrar Por:" Font-Bold="true"></asp:Label>
                                <asp:RadioButton ID="optFiltrar1_2" GroupName="optFiltrarPor" runat="server" AutoPostBack="true" />
                                <asp:ImageButton ID="imgFiltrar1_2" SkinID="Filtro_Cabecera" runat="server" />
                                <asp:RadioButton ID="optFiltrar2_2" GroupName="optFiltrarPor" runat="server" AutoPostBack="true" />
                                <asp:ImageButton ID="imgFiltrar2_2" SkinID="Filtro_Columna" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <!--Grid Lineas -->
            <asp:UpdatePanel ID="UpdatePanelLineas" runat="server" UpdateMode="conditional">
                <ContentTemplate>
                    <table border="0" width="98%" style="margin-left: 5px; margin-right: 5px;" cellspacing="0">
                        <tr>
                            <td style="height: 5px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="divLineas">
                                    <ig:WebDataGrid ID="wdgLineas" runat="server" Width="100%" Height="200px" SkinID="Visor" Browser="Xml"
                                    AutoGenerateBands="false" AutoGenerateColumns="false" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="true">
                                        <Behaviors>
                                            <ig:Selection  RowSelectType="Multiple" Enabled="True" CellClickAction="Row" ></ig:Selection> 
                                            <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true"  AnimationEnabled="true"> 
                                            </ig:Filtering> 
                                            <ig:RowSelectors Enabled="false" RowNumbering="false"></ig:RowSelectors>
                                            <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>                                           
                                        </Behaviors>
                                    </ig:WebDataGrid>
                                </div>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlComandos" runat="server" Visible="True" Width="100%">
        <table width="60%">
            <tr>
                <td align="center" style="width:100%">
                    <fsn:FSNButton ID="btnAtras" runat="server" Text="<-DAtras" Alineacion="Right"></fsn:FSNButton>
                </td>
                <td align="center" style="width:4%">    
                                             
                    <fsn:FSNButton ID="btnSiguiente" runat="server" Text="DSiguiente"></fsn:FSNButton>
                    <fsn:FSNButton ID="btnAceptar1" runat="server" Text="DAceptar"></fsn:FSNButton>              
                    <fsn:FSNButton ID="btnAceptar2" runat="server" Text="DAceptar"
                        OnClientClick="aceptarLineas();return false;"></fsn:FSNButton>                                                    
                </td>
                <td align="left" style="width:100%">                    
                    <fsn:FSNButton ID="btnCancelar" runat="server" Text="DCancelar"
                        OnClientClick="window.close();return false;"></fsn:FSNButton>   
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
        <div style="position: relative; top: 30%; text-align: center;">
            <asp:Image ID="ImgProgress" runat="server" SkinID="ImgProgress" />
            <asp:Label ID="LblProcesando" runat="server"></asp:Label>
        </div>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
    </form>
</body>
</html>
