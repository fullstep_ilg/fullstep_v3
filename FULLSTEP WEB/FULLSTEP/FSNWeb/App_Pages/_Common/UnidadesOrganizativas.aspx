﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" EnableEventValidation="false" MasterPageFile="~/App_Master/EnBlanco.Master" CodeBehind="UnidadesOrganizativas.aspx.vb" Inherits="Fullstep.FSNWeb.UnidadesOrganizativas"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $('body').addClass('fondoMasterEnBlanco');
            $('[id$=wdtUnidadesOrganizativas]').parent('div').css('max-height', $(window).height() * 0.85);
        });
        var currentTree;
        function initTree(tree) {
            currentTree = tree;
        };
        function seleccionarUnidadOrg() {            
            var selNode = currentTree.get_selectedNodes();
            if (selNode.length == 0) {
                alert(arrTextosML[0]);
                return false;
            };
            var sDen = currentTree.get_selectedNodes()[0].get_text();
            var oNode = currentTree.get_selectedNodes()[0];
            var iNivel = oNode.get_level();
            var sUON1, sUON2, sUON3;
            switch(iNivel){
                case 1:
                    sUON1 = oNode.get_valueString();
                    break;
                case 2:
                    sUON1 = oNode.get_parentNode().get_valueString();
                    sUON2 = oNode.get_valueString();
                    break;
                case 3:
                    sUON1 = oNode.get_parentNode().get_parentNode().get_valueString();
                    sUON2 = oNode.get_parentNode().get_valueString();
                    sUON3 = oNode.get_valueString();
                    break;
            };
            if ($('[id$=desde]').val() == 'VisorSol') window.opener.UON_seleccionado($('[id$=IDCONTROL]').val(), sUON1, sUON2, sUON3, iNivel, sDen, $('[id$=IDCONTROLVALUE]').val());
            else window.opener.UON_seleccionado($('[id$=IDCONTROL]').val(), sUON1, sUON2, sUON3, iNivel, sDen);
            window.close();
        };
    </script>
    <div style="overflow:auto; ">
        <ig:WebDataTree runat="server" ID="wdtUnidadesOrganizativas" SelectionType="Single" 
            InitialExpandDepth="4" InitialDataBindDepth="4">
            <ClientEvents Initialize="initTree" />
            <NodeSettings  SelectedCssClass="Seleccionable"/>
            <DataBindings>
                <ig:DataTreeNodeBinding DataMember="NIV0" TextField="DEN" KeyField="COD" ValueField="COD" />
                <ig:DataTreeNodeBinding DataMember="NIV1" TextField="DEN" KeyField="COD" ValueField="COD" />
                <ig:DataTreeNodeBinding DataMember="NIV2" TextField="DEN" KeyField="COD" ValueField="COD" />
                <ig:DataTreeNodeBinding DataMember="NIV3" TextField="DEN" KeyField="COD" ValueField="COD" />
            </DataBindings>
        </ig:WebDataTree>        
    </div>
    <div class="SeparadorSup" style="position:absolute; bottom:0.5em; width:100%; min-height:10%; padding-top:0.5em;">
        <div style="display:inline-block; width:49%; text-align:center;">
            <input class="boton" id="cmdAceptar" onclick="seleccionarUnidadOrg()" type="button" name="cmdAceptar" runat="server"/>
        </div>
        <div style="display:inline-block; width:49%; text-align:center;">
            <input class="boton" id="cmdCancelar" onclick="window.close()" type="button" name="cmdCancelar" runat="server"/>
        </div>
    </div>
    <input runat="server" id="IDCAMPO" type="hidden" name="IDCAMPO" />
    <input runat="server" id="IDCONTROL" type="hidden" name="IDCAMPO" />
    <input runat="server" id="IDCONTROLVALUE" type="hidden" name="IDCONTROLVALUE" />
    <input runat="server" id="desde" type="hidden" name="desde" />
</asp:Content>
