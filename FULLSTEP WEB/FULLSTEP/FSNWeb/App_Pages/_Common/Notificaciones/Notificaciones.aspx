﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.Master" 
	CodeBehind="Notificaciones.aspx.vb" Inherits="Fullstep.FSNWeb.Notificaciones" EnableEventValidation="false"%>

<asp:Content ID="ContentHead" ContentPlaceHolderID="head" runat="server">
	<script type="text/javascript" language="javascript">
		/*''' Revisado por: Jbg; Fecha: 27/10/2011
		''' <summary>
		''' Se oculta los criterios aplicados
		''' </summary>
		''' <param name="sender">parametro sistema</param>
		''' <remarks>Llamada desde: btnBuscar ; Tiempo máximo: 0 </remarks>*/
		function Buscar(sender) {
			if (document.getElementById("<%=pnlCabeceraCriterios.ClientID%>") != null) {
				document.getElementById("<%=pnlCabeceraCriterios.ClientID%>").style.display = "none"
				document.getElementById("<%=pnlCriterios.ClientID%>").style.display = "none"

				document.getElementById("<%=TablaEnBlanco.ClientID%>").style.display = "none"
			}
            MostrarCargando();
		}
        
        function OcultarCargando() {
            var modalprog = $find(ModalProgress);
            if (modalprog) modalprog.hide();
        }
		/*''' Revisado por: Jbg; Fecha: 27/10/2011
		''' <summary>
		''' Reestablece los filtros sin tocar el grid
		''' </summary>
		''' <param name="sender">parametro sistema</param>
		''' <remarks>Llamada desde: btnLimpiar ; Tiempo máximo: 0 </remarks>*/
		function Limpiar(sender) {
			var dropdown = null;
            
			document.getElementById("<%=txtProveedor.ClientID%>").value = "";
			document.getElementById("<%=hidProveedor.ClientID%>").value = "";

			dropdown = $find("<%=wddTipoEntidad.clientID %>");
			if (dropdown != null) {
				dropdown._elements.Input.value = ""

				dropdown.selectItem(dropdown.get_items().getItem(0), true, true);
				dropdown.closeDropDown();
				dropdown.dispose;
			}

			dropdown = $find("<%=wddTipoEmail.clientID %>");
			if (dropdown != null) {
				dropdown._elements.Input.value = ""

				dropdown.selectItem(dropdown.get_items().getItem(0), true, true);
				dropdown.closeDropDown();
				dropdown.dispose;
			}

			dropdown = $find("<%=wddEstado.clientID %>");
			if (dropdown != null) {
				dropdown._elements.Input.value = ""

				dropdown.selectItem(dropdown.get_items().getItem(0), true, true);
				dropdown.closeDropDown();
				dropdown.dispose;
			}

			var DataPicker = null;
			DataPicker = $find("<%=WDFecDesde.clientID%>");
			if (DataPicker != null) {
				DataPicker.set_value("", true);
			}

			DataPicker = $find("<%=WDFecHasta.clientID%>");
			if (DataPicker != null) {
				DataPicker.set_value("", true);
			}

            if (document.getElementById("<%=txtEntidadCod.ClientID%>")!=null){
			    document.getElementById("<%=txtEntidadCod.ClientID%>").value = "";
			    document.getElementById("<%=hidEntidad.ClientID%>").value = "";

			    document.getElementById("<%=txtEntidadCod.ClientID%>").disabled = true;
			    document.getElementById("<%=txtEntidadCod.ClientID%>").style.backgroundColor = "LightGray";
            }

            dropdown = $find("<%=ddlAnyo.clientID %>");
			if (dropdown != null) {
				dropdown._elements.Input.value = ""
                dropdown.selectItem(dropdown.get_items().getItem(0), true, true);
				dropdown.closeDropDown();
				dropdown.dispose;
			}

            dropdown = $find("<%=ddlCommodity.clientID %>");
			if (dropdown != null) {
				dropdown._elements.Input.value = ""
                dropdown.selectItem(dropdown.get_items().getItem(0), true, true);
				dropdown.closeDropDown();
				dropdown.dispose;
			}

            dropdown = $find("<%=ddlCodigo.clientID %>");
			if (dropdown != null) {
				dropdown._elements.Input.value = "";
                dropdown.selectItem(dropdown.get_items().getItem(0), true, true);
				dropdown.dispose;
			}

            dropdown = $find("<%=ddlDenominacion.clientID %>");
			if (dropdown != null) {
				dropdown._elements.Input.value = ""
                dropdown.selectItem(dropdown.get_items().getItem(0), true, true);
				dropdown.dispose;
			}

            $("id$=lgnProceso").hide();
		}
		
		/*''' Revisado por: Jbg; Fecha: 27/10/2011
		''' <summary>
		''' Exporta a Excel
		''' </summary>
		''' <remarks>Llamada desde: onclick del div q contiene al link excel y a la img excel; Tiempo máximo:0,5</remarks>
		*/
		function ExportarExcel() {
			__doPostBack('ExportarExcel', '')
		}
		/*''' Revisado por: Jbg; Fecha: 27/10/2011
		''' <summary>
		''' Exporta a Pdf
		''' </summary>
		''' <remarks>Llamada desde: onclick del div q contiene al link pdf y a la img pdf; Tiempo máximo:0,5</remarks>
		*/
		function ExportarPDF() {
			__doPostBack('ExportarPDF', '')
		}

		/*''' Revisado por: Jbg; Fecha: 07/11/2011
		''' <summary>
		''' Agrega los proveedorews buscados
		''' </summary>
		''' <param name="Proveedores">Proveedores</param>
		''' <remarks>Llamada desde=BuscadorProveedores.aspx --> wdgDatos_CellSelectionChanged; Tiempo maximo=0seg.</remarks>*/
		function prove_seleccionado2(sCIF, sProveCod, sProveDen) {
			document.getElementById("<%=hidProveedor.ClientID%>").value = sProveCod
			document.getElementById("<%=txtProveedor.ClientID%>").value = sProveCod + '-' + sProveDen
		}

		/*''' Revisado por: Jbg; Fecha: 27/10/2011
		''' <summary>
		''' Evento que salta al abrir el tipo de entidad.
		''' </summary>
		''' <param name="sender">componente input</param>
		''' <param name="eventArgs">evento</param>    
		''' <remarks>Llamada desde=evento del dropdown; Tiempo maximo=0seg.</remarks>*/
		function CancelDropDownOpening(sender, args) {
			if (sender.__isDeleting == true && sender.__isButtonClick == false) {
				args.set_cancel(true);
			}
		}

		/*''' Revisado por: Jbg; Fecha: 27/10/2011
		Descripcion: Funcion que inicializa la caja de texto del combo Tipo de Email a ""
		Parametros entrada:=
		elem:=componente input
		event:=evento
		Llamada desde: Al cambiar el valor del combo tipo
		Tiempo ejecucion:= 0seg.*/
		function ValueChanged_wddTipoEntidad(elem, event) {
            MostrarCargando();
			comboCodigo = $find("<%=wddTipoEmail.ClientID%>")
			if (comboCodigo)
				comboCodigo._elements.Input.value = ""
		}
		/*''' Revisado por: Jbg; Fecha: 27/10/2011
		''' <summary>
		''' Recoge la instancia seleccionada con el autocompletar
		''' </summary>
		''' <param name="sender">componente input</param>
		''' <param name="e">evento</param>        
		''' <remarks>Llamada desde: AutoCompleter/OnClientItemSelected; Tiempo máximo:0</remarks>
		*/
		function selected_Entidad(sender, e) {
			oIdProveedor = document.getElementById('<%= hidEntidad.clientID %>');
			if (oIdProveedor)
				oIdProveedor.value = e._value;
		}

		/*''' Revisado por: Jbg; Fecha: 27/10/2011
		''' <summary>
		''' Recoge el ID del proveedor seleccionado con el autocompletar
		''' </summary>
		''' <param name="sender">componente input</param>
		''' <param name="e">evento</param>        
		''' <remarks>Llamada desde: AutoCompleter/OnClientItemSelected; Tiempo máximo:0</remarks>
		*/
		function selected_Proveedor(sender, e) {
			oIdProveedor = document.getElementById('<%= hidProveedor.clientID %>');
			if (oIdProveedor)
				oIdProveedor.value = e._value;
		} 
		function wdgDatos_CellClick(sender, e) {
			var IdMail=e.getNewSelectedCells().getCell(0).get_row().get_cellByColumnKey("ID").get_value();
			e.set_cancel(true);
			var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>_Common/EnvioMails.aspx?IdMail=" + IdMail + "&Volver=Notificaciones", "_blank", "width=800,height=700,status=no,resizable=no,top=200,left=200,menubar=no");
            
		}
	</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
	<ig:WebExcelExporter ID="wdgExcelExporter" runat="server"></ig:WebExcelExporter>
	<ig:WebDocumentExporter ID="wdgPDFExporter" runat="server" ExportMode="Download"></ig:WebDocumentExporter>

	
	<fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>		

	<asp:Panel ID="pnlCabeceraParametros" runat="server" width="99.8%" style="clear:both;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="PanelCabecera">
			<tr style="cursor: pointer; font-weight: bold; height: 15px; width: 100%;">
				<td valign="middle" align="left" style="padding-top: 5px">
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td style="padding-top: 2px">
								<asp:Image ID="imgExpandirParametros" runat="server" SkinID="Expandir" />
							</td>
							<td style="vertical-align: top; padding-top: 2px;">
								<asp:Label ID="lblBusquedaParametros" runat="server" Text="DSeleccione los criterios de búsqueda generales"
									Font-Bold="True" ForeColor="White"></asp:Label>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</asp:Panel>

	<asp:Panel ID="pnlParametros" runat="server" CssClass="Rectangulo">
		<table width="100%" border="0" cellpadding="4" cellspacing="0">
			<tr>
				<td>
					<asp:UpdatePanel ID="upBusqueda" runat="server" UpdateMode="Conditional">
						<ContentTemplate>                                
						<table id="Table1" style="HEIGHT: 32px" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<tr>
								<td >
                                    <asp:Label ID="lblProveedor" runat="server" Text="dProveedor:"></asp:Label>
                                </td>
								<td >
									<table style="background-color: White; width: 150px; border: solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
										<tr>
											<td>
												<asp:UpdatePanel ID="updProveedor" runat="server" UpdateMode="Conditional">
													<ContentTemplate>
														<asp:TextBox ID="txtProveedor" runat="server" Width="150px" BorderWidth="0px" BackColor="White"
															Height="16px"></asp:TextBox>
														<ajx:AutoCompleteExtender ID="txtProveedor_AutoCompleteExtender" runat="server" CompletionSetCount="10"
															DelimiterCharacters="" Enabled="True" MinimumPrefixLength="1" ServiceMethod="GetProveedores"
															ServicePath="../App_Services/AutoComplete.asmx" TargetControlID="txtProveedor" EnableCaching="False" OnClientItemSelected="selected_Proveedor">
														</ajx:AutoCompleteExtender>
														<asp:HiddenField ID="hidProveedor" runat="server" />
													</ContentTemplate>
												</asp:UpdatePanel>
											</td>
											<td style="vertical-align:middle;">               
												<asp:ImageButton ID="imgProveedorLupa" runat="server" SkinID="BuscadorLupa" />                                                                                                                                                        
											</td>              
										</tr>
									</table>
								</td>
								<td ><asp:Label ID="lblTipoEntidad" runat="server" Text="dTipoEntidad:"></asp:Label></td>
								<td >
									<ig:WebDropDown ID ="wddTipoEntidad" runat="server"   
										EnableClosingDropDownOnSelect="true" DropDownAnimationDuration="1" DropDownContainerWidth="200"
										EnableDropDownAsChild="false" Height="22px" AutoPostBack="true"
										EnableMultipleSelection="false"  EnableClosingDropDownOnBlur="true"
										ClientEvents-DropDownOpening="CancelDropDownOpening" CurrentValue="">
										<ClientEvents ValueChanged="ValueChanged_wddTipoEntidad" />
									</ig:WebDropDown>
								</td>
								<td ><asp:Label ID="lblTipoEmail" runat="server" Text="dTipoEmail:"></asp:Label></td>
								<td >
									<ig:WebDropDown ID ="wddTipoEmail" runat="server" Height="22px" width="150px"
										EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true" 
										EnableClosingDropDownOnBlur="true" CurrentValue="">
									</ig:WebDropDown>
								</td>
								<td ><asp:Label ID="lblEstado" runat="server" Text="dProveedor:"></asp:Label></td>
								<td >
									<ig:WebDropDown ID ="wddEstado" runat="server" Height="22px"
										EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true" 
										EnableClosingDropDownOnBlur="true" CurrentValue=""
										DropDownContainerWidth="150">
									</ig:WebDropDown>
								</td>
							</tr>
							<tr>            
								<td rowspan="2"><asp:Label ID="lblDesde" runat="server" Text="dDesde:"></asp:Label></td>
								<td rowspan="2">
									<igpck:WebDatePicker ID="WDFecDesde" runat="server" SkinID="Calendario" >
									</igpck:WebDatePicker>
								</td>
								<td rowspan="2"><asp:Label ID="lblHasta" runat="server" Text="dHasta:"></asp:Label></td>
								<td rowspan="2">
									<igpck:WebDatePicker ID="WDFecHasta" runat="server" SkinID="Calendario" >
									</igpck:WebDatePicker>
								</td>
                                <span id="spResto" runat="server">
                                    <td >
                                        <asp:Label ID="lblEntidad" runat="server" Text="dEntidad:" ></asp:Label>
                                    </td>
								    <td colspan="3" >
									    <asp:textbox id="txtEntidadCod" runat="server" Enabled="false" BackColor="LightGray" width="100%"></asp:textbox>
									    <ajx:AutoCompleteExtender ID="txtEntidadCod_AutoCompleteExtender" 
										    runat="server" CompletionSetCount="10" DelimiterCharacters="" Enabled="True"
										    MinimumPrefixLength="3" ServiceMethod="GetNotificaciones" ServicePath="../App_Services/AutoComplete.asmx"
										    TargetControlID="txtEntidadCod" UseContextKey ="true" EnableCaching="False" 
										    OnClientItemSelected="selected_Entidad"></ajx:AutoCompleteExtender>
									    <asp:HiddenField ID="hidEntidad" runat="server" />
								    </td>
                                </span>
                            </tr>
                            <tr >
                                <span id="spProceso" runat="server">                                
                                    <td colspan="4"  >
                                    <fieldset style="vertical-align:middle;">  
                                    <legend id="lgnProceso" runat="server" >DProceso</legend>
                                        <table>
                                        <td>
                                            <asp:Label ID="lblAnyo" runat="server" Text="DAÃ±o:" ></asp:Label>
                                        <td>
                                            <ig:WebDropDown ID="ddlAnyo" runat="server" AutoPostBack="true" EnableMultipleSelection="false" style="display:inline-block;width:50px;"
                                                EnableClosingDropDownOnSelect="true" BackColor="White" EnableCustomValues="false"
                                                DropDownContainerWidth="80" EnableDropDownAsChild="false" EnableCustomValueSelection="false"
                                                    EnableMarkingMatchedText="true" AutoSelectOnMatch="true" ClientEvents-DropDownOpening="CancelDropDownOpening">
                                            </ig:WebDropDown>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCommodity" runat="server" Text="DCommodity:" style="vertical-align:middle;padding-bottom: 1.5em;"></asp:Label>
                                        </td>
                                        <td>
                                            <ig:WebDropDown ID="ddlCommodity" runat="server"  BackColor="White" style="display:inline-block;width:150px;"
                                                EnableClosingDropDownOnSelect="true" DropDownAnimationDuration="1" DropDownContainerWidth="300"
                                                EnableDropDownAsChild="false" Height="22px"  AutoPostBack="true" ClientEvents-DropDownOpening="CancelDropDownOpening">
                                            </ig:WebDropDown>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCodigo" runat="server" Text="DCodigo:" style="vertical-align:middle;padding-bottom: 1.5em;"></asp:Label>
                                        </td>
                                        <td>
                                            <ig:WebDropDown ID="ddlCodigo" runat="server"  BackColor="White" EnableClosingDropDownOnSelect="true" style="display:inline-block;width:50px"
                                                DropDownAnimationDuration="0" DropDownContainerWidth="80" EnableDropDownAsChild="false" EnableAutoCompleteFirstMatch="false"
                                                AutoPostBack="true" EnableCustomValues="false" EnableMarkingMatchedText="false" EnableCustomValueSelection="false" EnableMultipleSelection="false" ClientEvents-DropDownOpening="CancelDropDownOpening" AutoSelectOnMatch="false" CurrentValue="">
                                            </ig:WebDropDown>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDenominacion" runat="server" Text="DDenominacion:" style="vertical-align:middle;padding-bottom: 1.5em;"></asp:Label>
                                        </td>
                                        <td>
                                            <ig:WebDropDown ID="ddlDenominacion" runat="server"  BackColor="White" style="display:inline-block;width:145px;"
                                                EnableClosingDropDownOnSelect="true" DropDownAnimationDuration="1" DropDownContainerWidth="145px"
                                                EnableDropDownAsChild="false" AutoPostBack="true" Height="22px" EnableMultipleSelection="false" CurrentValue="">
                                            </ig:WebDropDown>
                                        </td>
                                        </table>
                                    </fieldset>
                                    </td>
                                
                                </span>
                            </tr>			   
						</table>
						</ContentTemplate>
					</asp:UpdatePanel>
				</td>
			</tr>
			<tr>
				<td>
					<table width="1%">
						<tr>
							<td>
								<fsn:FSNButton ID="btnBuscar" runat="server" Text="DBuscar" OnClientClick="Buscar();return true;"></fsn:FSNButton>
							</td>
							<td>
								<fsn:FSNButton ID="btnLimpiar" runat="server" Text="DLimpiar" OnClientClick="Limpiar();return false;"></fsn:FSNButton>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</asp:Panel>

	<ajx:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" CollapseControlID="pnlCabeceraParametros"
	ExpandControlID="pnlCabeceraParametros" ImageControlID="imgExpandirParametros" SuppressPostBack="true"
	TargetControlID="pnlParametros" Collapsed="true" SkinID="FondoRojo">
	</ajx:CollapsiblePanelExtender>

	<table id="TablaEnBlanco" runat="server" style="display:none; line-height:5px;">
		<tr>
			<td>
				&nbsp;
			</td>
		</tr>
	</table>

	<asp:Panel ID="pnlCabeceraCriterios" runat="server" Visible="false" width="99.8%">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="PanelCabecera">
			<tr style="cursor: pointer; font-weight: bold; height: 15px; width: 100%;">
				<td valign="middle" align="left" style="padding-top: 5px">
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td style="padding-top: 2px">
								<asp:Image ID="imgExpandirCriterios" runat="server" SkinID="Expandir" />
							</td>
							<td style="vertical-align: top; padding-top: 2px">
								<asp:Label ID="lblBusquedaCriterios" runat="server" Text="DCiterios de busqueda aplicados"
									Font-Bold="True" ForeColor="White"></asp:Label>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</asp:Panel>

	<asp:Panel ID="pnlCriterios" runat="server" CssClass="Rectangulo"  Visible="false">
		<table width="100%" border="0" cellpadding="4" cellspacing="0">
			<tr>
				<td>
						<table id="Table2" style="HEIGHT: 32px" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<tr id="Linea1" runat="server">
								<td>
									<asp:Label ID="lblProveedorCriterios" runat="server" Text="dProveedor:" Font-Bold="true"></asp:Label>
									&nbsp;
									<asp:Label ID="lblProveedorCriteriosValor" runat="server" Text="dProveedor:"></asp:Label>
								</td>
							</tr>
							<tr id="Linea2" runat="server">
								<td>
									<asp:Label ID="lblTipoEntidadCriterios" runat="server" Text="dProveedor:" Font-Bold="true"></asp:Label>
									&nbsp;
									<asp:Label ID="lblTipoEntidadCriteriosValor" runat="server" Text="dProveedor:"></asp:Label>
								</td>
							</tr>
							<tr id="Linea3" runat="server">
								<td>
									<asp:Label ID="lblTipoEmailCriterios" runat="server" Text="dProveedor:" Font-Bold="true"></asp:Label>
									&nbsp;
									<asp:Label ID="lblTipoEmailCriteriosValor" runat="server" Text="dProveedor:"></asp:Label>
								</td>
							</tr>
							<tr id="Linea4" runat="server">
								<td>
									<asp:Label ID="lblDesdeCriterios" runat="server" Text="dProveedor:" Font-Bold="true"></asp:Label>
									&nbsp;
									<asp:Label ID="lblDesdeCriteriosValor" runat="server" Text="dProveedor:"></asp:Label>
								</td>
							</tr>                                 
						</table>
				</td>
			</tr>
		</table>
	</asp:Panel>

	<ajx:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server" CollapseControlID="pnlCabeceraCriterios"
	ExpandControlID="pnlCabeceraCriterios" ImageControlID="imgExpandirCriterios" SuppressPostBack="true"
	TargetControlID="pnlCriterios" Collapsed="true" SkinID="FondoRojo">
	</ajx:CollapsiblePanelExtender>

  

	<asp:UpdatePanel ID="UpdDatos" ChildrenAsTriggers="false" EnableViewState="true" runat="server" UpdateMode="Conditional">
	<Triggers>
		<asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
	</Triggers>
	<ContentTemplate>
		<ig:WebHierarchicalDataGrid ID="wdgDatos" runat="server" Width="99.8%" ShowHeader="true" 
		AutoGenerateColumns="false" DataKeyFields="ID" BorderWidth="1px">
			<Columns>
				<ig:BoundDataField Key="ID" DataFieldName="ID" Hidden="true"></ig:BoundDataField>
				<ig:BoundDataField Key="ENTIDAD_EMAIL" DataFieldName="ENTIDAD_EMAIL" Hidden="true"></ig:BoundDataField>
				<ig:BoundDataField Key="ENTIDAD" DataFieldName="ENTIDAD" Width="11%" CssClass="SinSalto"></ig:BoundDataField>
				<ig:BoundDataField Key="INSTANCIA" DataFieldName="INSTANCIA" Width="4%" CssClass="SinSalto"></ig:BoundDataField>
				<ig:BoundDataField Key="PROVE" DataFieldName="PROVE" Width="9%" CssClass="SinSalto"></ig:BoundDataField>
				<ig:BoundDataField Key="DENPROVE" DataFieldName="DENPROVE" Width="11%" CssClass="SinSalto"></ig:BoundDataField>
				<ig:BoundDataField Key="SUBJECT" DataFieldName="SUBJECT" Width="18%" CssClass="SinSalto"></ig:BoundDataField>
				<ig:BoundDataField Key="DIR_RESPUESTA" DataFieldName="DIR_RESPUESTA" Width="18%" CssClass="SinSalto"></ig:BoundDataField>
				<ig:BoundDataField Key="PARA" DataFieldName="PARA" Width="18%" CssClass="SinSalto"></ig:BoundDataField>
				<ig:BoundDataField Key="FECHA" DataFieldName="FECHA" Width="6%" CssClass="SinSalto"></ig:BoundDataField>
				<ig:BoundDataField Key="KO" DataFieldName="KO" Hidden="true"></ig:BoundDataField>
				<ig:BoundDataField Key="ENVIADO" DataFieldName="ENVIADO" Width="5%"></ig:BoundDataField>
			</Columns>
			<Behaviors>
				<ig:Selection Enabled="true" RowSelectType="Single" CellSelectType="Single" ColumnSelectType="None">
                    <SelectionClientEvents CellSelectionChanging="wdgDatos_CellClick" />
                </ig:Selection>
				<ig:Filtering Alignment="Top" Enabled="true" Visibility="Visible">
					<ColumnSettings>
						<ig:ColumnFilteringSetting ColumnKey="ENVIADO" Enabled="false" />
					</ColumnSettings>
				</ig:Filtering>
				<ig:Sorting Enabled="true" SortingMode="Multi"></ig:Sorting>
				<ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
				<ig:ColumnMoving Enabled="true"></ig:ColumnMoving>
				<ig:Paging Enabled="true" PagerAppearance="Top">
					<PagerTemplate>						
	                    <div class="CabeceraBotones" style="clear:both; float:left; width:100%;">    
                            <div style="float:left; padding:3px 0px 2px 0px;">    
                                <fsn:wucPagerControl ID="CustomerPager" runat="server" />
                            </div>
		                    <div style="float:right; margin:5px 5px 0px 0px;">
			                    <div onclick="ExportarExcel()" class="botonDerechaCabecera" style="float:left;">            
				                    <asp:Image runat="server" ID="imgExcel" CssClass="imagenCabecera" />
				                    <asp:Label runat="server" ID="lblExcel" CssClass="fntLogin2" style="margin-left:3px;" Text="Excel"></asp:Label>           
			                    </div>
			                    <div onclick="ExportarPDF()" class="botonDerechaCabecera" style="float:left;">            
				                    <asp:Image runat="server" ID="imgPDF" CssClass="imagenCabecera" />
				                    <asp:Label runat="server" ID="lblPDF" CssClass="fntLogin2" style="margin-left:3px;" Text="PDF"></asp:Label>           
			                    </div>
		                    </div>
	                    </div>
					</PagerTemplate>
				</ig:Paging>
			</Behaviors>
			<GroupingSettings EnableColumnGrouping="True"></GroupingSettings>
		</ig:WebHierarchicalDataGrid>        
	</ContentTemplate>
	</asp:UpdatePanel>

	<script type="text/javascript" language="javascript">
		var ModalProgress = '<%= ModalProgress.ClientID %>';
	</script>

	

	<ajx:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
		BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
</asp:Content>
