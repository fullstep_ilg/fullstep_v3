﻿Imports Infragistics.Web.UI
Imports Infragistics.Web.UI.GridControls
Imports Infragistics.Documents.Reports
Imports Infragistics.Documents.Reports.Report
Imports Fullstep.FSNServer

Public Class Notificaciones
    Inherits FSNPage

    Private _pagerControl As wucPagerControl
    Private m_desdePanelFicha As Boolean = False
#Region "Carga Inicial"
    ''' <summary>
    ''' Evento preinit en el que configuramos la apariencia de la master
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Llamada desde: Sistema; Tiempo máximo:0seg.</remarks>
    Private Sub Notificaciones_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Not IsNothing(Request.QueryString("desdePanelFicha")) AndAlso (Request.QueryString("desdePanelFicha").ToString = "1") Then
            m_desdePanelFicha = True
        End If
        If m_desdePanelFicha Then
            MasterPageFile = "~/App_Master/EnBlanco.Master"
        Else
            MasterPageFile = "~/App_Master/Menu.Master"
        End If
    End Sub
    ''' <summary>
    ''' Carga la pagina. 
    ''' Para q sea posible el multilingüismo en los filtros y la paginacion customizada.
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Llamada desde: Sistema; Tiempo máximo:0,5seg.</remarks>
    Private Sub Notificaciones_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ScriptMgr.EnableScriptGlobalization = True

        _pagerControl = TryCast(Me.wdgDatos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("CustomerPager"), wucPagerControl)
        AddHandler _pagerControl.PageChanged, AddressOf currentPageControl_PageChanged
    End Sub
    ''' <summary>
    ''' Cuando con la paginacion customizada cambia de pagina, el grid debe actualizarse a la pagina indicada.
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>    
    ''' <remarks>Llamada desde: wucPagercOntrol.ascx; Tiempo máximo:0,1seg.</remarks>
    Private Sub currentPageControl_PageChanged(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        wdgDatos.GridView.Behaviors.Paging.PageIndex = e.PageNumber

        UpdDatos.Update()
    End Sub
    ''' <summary>
    ''' Carga la pagina
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Llamada desde: Sistema; Tiempo máximo:0,5seg.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Notificaciones

        CargarTextos()

        Dim ModalProgressClientID As String
        If m_desdePanelFicha Then
            ModalProgressClientID = CType(Master.FindControl("ModalProgress"), AjaxControlToolkit.ModalPopupExtender).ClientID
        Else
            ModalProgressClientID = CType(Master.Master.FindControl("ModalProgress"), AjaxControlToolkit.ModalPopupExtender).ClientID
        End If

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalClientID", "<script>var ModalProgress = '" & ModalProgressClientID & "' </script>")

        'Mostrar buscador en función del tipo de entidad
        If strToInt(wddTipoEntidad.SelectedValue) = EntidadNotificacion.ProcesoCompra Then
            spProceso.Visible = True
            spResto.Visible = False
        Else
            spProceso.Visible = False
            ddlCodigo.SelectedValue = ""
            ddlCommodity.SelectedValue = ""
            ddlCodigo.SelectedValue = ""
            ddlDenominacion.SelectedValue = ""
            ddlAnyo.SelectedValue = ""
            spResto.Visible = True
        End If

        If Not Page.IsPostBack Then
            CargarComboTipoEntidad()
            CargarComboEstado()

            If m_desdePanelFicha Then
                CargarTEmail(EntidadNotificacion.Calificaciones)
            Else
                CargarTEmail(0)
            End If

            CargaGrid(True)
            InicializacionControles()
            WriteScripts()
            CargaImagenes()

            wdgDatos.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize")
            wdgDatos.GridView.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize")

            If Not m_desdePanelFicha Then
                Seccion = "Notificaciones"
                Dim mMaster = CType(Me.Master, FSNWeb.Menu)
                mMaster.Seleccionar("Notificaciones", "Notificaciones")

                CollapsiblePanelExtender1.Collapsed = False
            Else
                pnlCabeceraCriterios.Visible = True
                pnlCriterios.Visible = True
                wdgDatos.Height = Unit.Pixel(515)
                CollapsiblePanelExtender2.Collapsed = False
                TablaEnBlanco.Style.Item("display") = ""
            End If
        Else
            If Request("__EVENTTARGET") = btnBuscar.UniqueID Then
                wdgDatos.Rows.Clear()
                ModalProgress.Show()
                CargaGrid(True)

                wdgDatos.GridView.Behaviors.Paging.PageIndex = 0
                _pagerControl.SetCurrentPageNumber(0)
            Else
                CargaGrid(False)

                Select Case Request("__EVENTTARGET")
                    Case "ExportarExcel"
                        ExportarExcel()
                    Case "ExportarPDF"
                        ExportarPDF()
                End Select
            End If
        End If
        ModalProgress.Hide()
    End Sub
    ''' <summary>
    ''' Carga el grid
    ''' </summary>
    ''' <param name="bCargaCompleta">Si se usa la cache o no</param>
    ''' <remarks>Llamada desde: Page_load      btnBuscar_Click; Tiempo maxiomo: 0,2</remarks>
    Private Sub CargaGrid(ByVal bCargaCompleta As Boolean)
        If bCargaCompleta Then
            Dim mail As FSNServer.Email = FSNServer.Get_Object(GetType(FSNServer.Email))
            Dim sProve As String = ""
            Dim AnnoEnCurso As Integer = Now.Year
            Dim sDesde As String = DateToBDDate(New Date(AnnoEnCurso, 1, 1))
            Dim sProves As String
            Dim TipoEntidad As String = ""
            Dim TipoEmail As Integer
            Dim Estado As Integer
            Dim sFechaDesde As String
            Dim sFechaHasta As String
            Dim Entidad As Long
            Dim iAnyo As Integer
            Dim sGmn1 As String = Nothing
            Dim iProce As Integer
            If Not Page.IsPostBack Then
                If Not m_desdePanelFicha Then
                    sProve = "'&&&'" 'Para q nada mas entrar no saque nada
                    sDesde = Nothing
                ElseIf Request.QueryString("Proveedores").ToString = "" Then
                    sProve = "" 'Todos

                    wddTipoEntidad.CurrentValue = Textos(24)
                    wddTipoEntidad.SelectedValue = EntidadNotificacion.Calificaciones

                    wddTipoEmail.CurrentValue = Textos(27)
                    wddTipoEmail.SelectedValue = TipoNotificacion.CambioCalificaciónTotalProveedor

                    WDFecDesde.Value = New Date(AnnoEnCurso, 1, 1)
                Else
                    sProve = Request.QueryString("Proveedores").ToString 'se proporciono uno o mas proves

                    wddTipoEntidad.CurrentValue = Textos(24)
                    wddTipoEntidad.SelectedValue = EntidadNotificacion.Calificaciones

                    wddTipoEmail.CurrentValue = Textos(27)
                    wddTipoEmail.SelectedValue = TipoNotificacion.CambioCalificaciónTotalProveedor

                    If InStr(Request.QueryString("Proveedores").ToString, "','") = 0 Then
                        Dim Aux As String = sProve
                        Aux = Left(Aux, Len(Aux) - 1)
                        hidProveedor.Value = Right(Aux, Len(Aux) - 1)
                        txtProveedor.Text = Request.QueryString("DenProveedores").ToString
                    End If

                    WDFecDesde.Value = New Date(AnnoEnCurso, 1, 1)
                End If

                sProves = sProve
                TipoEntidad = EntidadNotificacion.Calificaciones

                TipoEmail = TipoNotificacion.CambioCalificaciónTotalProveedor
                Estado = -1
                sFechaDesde = sDesde
                sFechaHasta = Nothing
                Entidad = 0
            Else
                If hidProveedor.Value <> "" Then
                    sProve = "'" & hidProveedor.Value & "'"
                End If

                Dim sTipoEntidad As String
                sTipoEntidad = 0
                If wddTipoEntidad.SelectedItemIndex > 0 AndAlso wddTipoEntidad.SelectedValue > -1 Then
                    sTipoEntidad = CInt(wddTipoEntidad.SelectedValue)
                Else
                    sTipoEntidad = String.Join(",", FSNUser.getEntidadesNotificacion.ToArray)
                End If

                Dim iTipoEmail As Integer
                iTipoEmail = -1
                'Salta dos veces 
                '   SelectedItemIndex es correcto siempre
                '   la primera el SelectedItem no tiene nada la segunda lo clava?????
                If (wddTipoEmail.SelectedItemIndex > 0) AndAlso (wddTipoEmail.SelectedValue <> "") Then
                    iTipoEmail = CInt(wddTipoEmail.SelectedValue)
                End If

                Dim iEstado As Integer
                iEstado = -1
                If wddEstado.SelectedItemIndex > 0 Then
                    iEstado = CInt(wddEstado.SelectedItemIndex - 1)
                End If

                sDesde = Nothing
                If WDFecDesde.Value = Nothing Or IsTime(WDFecDesde.Value) Then
                Else
                    sDesde = DateToBDDate(WDFecDesde.Value)
                End If

                Dim sHasta As String = Nothing
                If WDFecHasta.Value = Nothing Or IsTime(WDFecHasta.Value) Then
                Else
                    sHasta = DateToBDDate(WDFecHasta.Value)
                End If

                Dim lEntidad As Long
                lEntidad = 0
                If Me.hidEntidad.Value <> "" Then
                    lEntidad = CLng(Me.hidEntidad.Value)
                End If

                sProves = sProve
                TipoEntidad = sTipoEntidad
                TipoEmail = iTipoEmail
                Estado = iEstado
                sFechaDesde = sDesde
                sFechaHasta = sHasta
                Entidad = lEntidad

                If wddTipoEntidad.SelectedValue <> "" AndAlso wddTipoEntidad.SelectedValue = EntidadNotificacion.ProcesoCompra Then
                    If ddlAnyo.SelectedValue <> "" Then
                        iAnyo = CInt(ddlAnyo.SelectedValue)
                    End If
                    sGmn1 = ddlCommodity.SelectedValue
                    If ddlCodigo.SelectedItems.Count <> 0 AndAlso ddlCodigo.SelectedValue <> "" Then
                        iProce = CInt(ddlCodigo.SelectedValue)
                    End If
                End If
            End If
            wdgDatos.GridView.ClearDataSource()

            Dim oDS As DataSet = mail.Load_Registro_Email_Visor(sProves, TipoEntidad, TipoEmail, Estado, sFechaDesde, sFechaHasta, Entidad, Idioma, iAnyo, sGmn1, iProce, ConfigurationManager.AppSettings("LimiteNotificacionesCargadas"))
            wdgDatos.DataSource = oDS
            wdgDatos.GridView.DataSource = oDS
            wdgDatos.DataBind()

            InsertarEnCache("oDsNotificaciones" & FSNUser.Cod, oDS, CacheItemPriority.BelowNormal)
        Else
            wdgDatos.DataSource = CType(Cache("oDsNotificaciones" & FSNUser.Cod), DataSet)
            wdgDatos.DataBind()
        End If
        UpdDatos.Update()
    End Sub
    ''' <summary>
    ''' Establece los textos de los controles de acuerdo con el idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load; Tiempo máximo:0</remarks> 
    Private Sub CargarTextos()
        FSNPageHeader.TituloCabecera = Textos(0)

        lblBusquedaParametros.Text = Textos(1)
        lblProveedor.Text = Textos(2)
        lblTipoEntidad.Text = Textos(3)
        lblTipoEmail.Text = Textos(4)
        lblEstado.Text = Textos(5)
        lblDesde.Text = Textos(6)
        lblHasta.Text = Textos(7)
        lblEntidad.Text = Textos(8)

        btnBuscar.Text = Textos(9)
        btnLimpiar.Text = Textos(10)

        lblBusquedaCriterios.Text = Textos(11)

        lblProveedorCriterios.Text = Textos(2)
        lblTipoEntidadCriterios.Text = Textos(3)
        lblTipoEmailCriterios.Text = Textos(4)
        lblDesdeCriterios.Text = Textos(6)

        If m_desdePanelFicha Then

            If Request.QueryString("Proveedores").ToString = "" Then
                lblProveedorCriteriosValor.Text = Textos(47)
            Else
                lblProveedorCriteriosValor.Text = Request.QueryString("DenProveedores").ToString
            End If

            lblTipoEntidadCriteriosValor.Text = Textos(24)
            lblTipoEmailCriteriosValor.Text = Textos(27)
            lblDesdeCriteriosValor.Text = modUtilidades.FormatDate(New Date(Now.Year, 1, 1), FSNUser.DateFormat)

        End If

        CType(wdgDatos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblExcel"), Label).Text = Textos(12)
        CType(wdgDatos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPDF"), Label).Text = Textos(13)

        wdgDatos.GroupingSettings.EmptyGroupAreaText = Textos(14)

        Dim field As Infragistics.Web.UI.GridControls.BoundDataField
        Dim fieldSetting As Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        field = wdgDatos.Columns.FromKey("ENTIDAD")
        field.Header.Text = Textos(15)
        fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldSetting.ColumnKey = field.Key
        wdgDatos.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        wdgDatos.Behaviors.Filtering.ColumnSettings.Item(field.Key).BeforeFilterAppliedAltText = Textos(58)

        field = wdgDatos.Columns.FromKey("INSTANCIA")
        field.Header.Text = Textos(16)
        fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldSetting.ColumnKey = field.Key
        wdgDatos.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        wdgDatos.Behaviors.Filtering.ColumnSettings.Item(field.Key).BeforeFilterAppliedAltText = Textos(58)

        field = wdgDatos.Columns.FromKey("PROVE")
        field.Header.Text = Textos(17)
        fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldSetting.ColumnKey = field.Key
        wdgDatos.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        wdgDatos.Behaviors.Filtering.ColumnSettings.Item(field.Key).BeforeFilterAppliedAltText = Textos(58)

        field = wdgDatos.Columns.FromKey("DENPROVE")
        field.Header.Text = Textos(18)
        fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldSetting.ColumnKey = field.Key
        wdgDatos.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        wdgDatos.Behaviors.Filtering.ColumnSettings.Item(field.Key).BeforeFilterAppliedAltText = Textos(58)

        field = wdgDatos.Columns.FromKey("SUBJECT")
        field.Header.Text = Textos(19)
        fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldSetting.ColumnKey = field.Key
        wdgDatos.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        wdgDatos.Behaviors.Filtering.ColumnSettings.Item(field.Key).BeforeFilterAppliedAltText = Textos(58)

        field = wdgDatos.Columns.FromKey("DIR_RESPUESTA")
        field.Header.Text = Textos(20)
        fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldSetting.ColumnKey = field.Key
        wdgDatos.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        wdgDatos.Behaviors.Filtering.ColumnSettings.Item(field.Key).BeforeFilterAppliedAltText = Textos(58)

        field = wdgDatos.Columns.FromKey("PARA")
        field.Header.Text = Textos(21)
        fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldSetting.ColumnKey = field.Key
        wdgDatos.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        wdgDatos.Behaviors.Filtering.ColumnSettings.Item(field.Key).BeforeFilterAppliedAltText = Textos(58)

        field = wdgDatos.Columns.FromKey("FECHA")
        field.Header.Text = Textos(22)
        fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldSetting.ColumnKey = field.Key
        wdgDatos.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        wdgDatos.Behaviors.Filtering.ColumnSettings.Item(field.Key).BeforeFilterAppliedAltText = Textos(58)

        field = wdgDatos.Columns.FromKey("ENVIADO")
        field.Header.Text = Textos(23)
        fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldSetting.ColumnKey = field.Key
        wdgDatos.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        wdgDatos.Behaviors.Filtering.ColumnSettings.Item(field.Key).BeforeFilterAppliedAltText = Textos(58)

        Dim fieldGridView As Infragistics.Web.UI.GridControls.BoundDataField
        fieldGridView = wdgDatos.GridView.Columns.FromKey("ENTIDAD")
        fieldGridView.Header.Text = Textos(15)
        fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldSetting.ColumnKey = field.Key
        wdgDatos.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        wdgDatos.Behaviors.Filtering.ColumnSettings.Item(field.Key).BeforeFilterAppliedAltText = Textos(58)

        fieldGridView = wdgDatos.GridView.Columns.FromKey("INSTANCIA")
        fieldGridView.Header.Text = Textos(16)
        fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldSetting.ColumnKey = field.Key
        wdgDatos.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        wdgDatos.Behaviors.Filtering.ColumnSettings.Item(field.Key).BeforeFilterAppliedAltText = Textos(58)

        fieldGridView = wdgDatos.GridView.Columns.FromKey("PROVE")
        fieldGridView.Header.Text = Textos(17)
        fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldSetting.ColumnKey = field.Key
        wdgDatos.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        wdgDatos.Behaviors.Filtering.ColumnSettings.Item(field.Key).BeforeFilterAppliedAltText = Textos(58)

        fieldGridView = wdgDatos.GridView.Columns.FromKey("DENPROVE")
        fieldGridView.Header.Text = Textos(18)
        fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldSetting.ColumnKey = field.Key
        wdgDatos.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        wdgDatos.Behaviors.Filtering.ColumnSettings.Item(field.Key).BeforeFilterAppliedAltText = Textos(58)

        fieldGridView = wdgDatos.GridView.Columns.FromKey("SUBJECT")
        fieldGridView.Header.Text = Textos(19)
        fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldSetting.ColumnKey = field.Key
        wdgDatos.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        wdgDatos.Behaviors.Filtering.ColumnSettings.Item(field.Key).BeforeFilterAppliedAltText = Textos(58)

        fieldGridView = wdgDatos.GridView.Columns.FromKey("DIR_RESPUESTA")
        fieldGridView.Header.Text = Textos(20)
        fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldSetting.ColumnKey = field.Key
        wdgDatos.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        wdgDatos.Behaviors.Filtering.ColumnSettings.Item(field.Key).BeforeFilterAppliedAltText = Textos(58)

        fieldGridView = wdgDatos.GridView.Columns.FromKey("PARA")
        fieldGridView.Header.Text = Textos(21)
        fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldSetting.ColumnKey = field.Key
        wdgDatos.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        wdgDatos.Behaviors.Filtering.ColumnSettings.Item(field.Key).BeforeFilterAppliedAltText = Textos(58)

        fieldGridView = wdgDatos.GridView.Columns.FromKey("FECHA")
        fieldGridView.Header.Text = Textos(22)
        fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldSetting.ColumnKey = field.Key
        wdgDatos.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        wdgDatos.Behaviors.Filtering.ColumnSettings.Item(field.Key).BeforeFilterAppliedAltText = Textos(58)

        fieldGridView = wdgDatos.GridView.Columns.FromKey("ENVIADO")
        fieldGridView.Header.Text = Textos(23)
        fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldSetting.ColumnKey = field.Key
        wdgDatos.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        wdgDatos.Behaviors.Filtering.ColumnSettings.Item(field.Key).BeforeFilterAppliedAltText = Textos(58)

        lblAnyo.Text = Textos(99)
        lblCommodity.Text = Textos(100)
        lblCodigo.Text = Textos(101)
        lblDenominacion.Text = Textos(102)
        lgnProceso.InnerText = Textos(103)
    End Sub
    ''' <summary>
    ''' Carga el combo de TipoEntidad
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,1seg.</remarks>
    Private Sub CargarComboTipoEntidad()
        'TipoEntidad
        AñadirElementoTipoEntidad(-1, "")
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EntidadesNotificacion
        For Each tipo As EntidadNotificacion In FSNUser.getEntidadesNotificacion
            'NOTA: Cada vez que añada una entidad en tipo EntidadNotificacion nueva, debo actualizar el módulo de idiomas EntidadesNotificacion=185
            'introduciendo su id y sus denominaciones en diferentes idiomas, para que el combo lo cargue automáticamente
            AñadirElementoTipoEntidad(tipo, TextosById(tipo))
        Next
        AñadirElementoTipoEntidad(EntidadNotificacion.NoDefinido, TextosById(EntidadNotificacion.NoDefinido))
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Notificaciones

        wddTipoEntidad.CurrentValue = ""
        wddTipoEntidad.SelectedValue = Nothing
    End Sub
    ''' <summary>
    ''' Carga el combo de Estado
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,1seg.</remarks>
    Private Sub CargarComboEstado()
        'Estado
        AñadirElementoEstado(-1, "")
        AñadirElementoEstado(0, Textos(42))
        AñadirElementoEstado(1, Textos(43))
    End Sub
    ''' <summary>
    ''' Carga un registro en el combo de Tipo de Entidad
    ''' </summary>
    ''' <param name="value">Valor del combo</param>
    ''' <param name="text">Texto del combo</param>
    ''' <remarks>Llamada desde:CargarComboTipoEntidad; Tiempo máximo:0seg.</remarks>
    Private Sub AñadirElementoTipoEntidad(ByVal value As Nullable(Of Integer), ByVal text As String)
        Dim oItem As New ListControls.DropDownItem
        oItem.Value = value
        oItem.Text = text
        wddTipoEntidad.Items.Add(oItem)
    End Sub
    ''' <summary>
    ''' Carga un registro en el combo de Estado
    ''' </summary>
    ''' <param name="value">Valor del combo</param>
    ''' <param name="text">Texto del combo</param>
    ''' <remarks>Llamada desde:CargarComboEstado; Tiempo máximo:0seg.</remarks>
    Private Sub AñadirElementoEstado(ByVal value As Nullable(Of Integer), ByVal text As String)
        Dim oItem As New ListControls.DropDownItem
        oItem.Value = value
        oItem.Text = text
        wddEstado.Items.Add(oItem)
    End Sub
    ''' <summary>
    ''' Inicializa los componentes y funciones javascript de los elementos
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0seg.</remarks>
    Private Sub InicializacionControles()
        imgProveedorLupa.OnClientClick = "var newWindow = window.open('" & ConfigurationManager.AppSettings("RutaFS") & "_common/BuscadorProveedores.aspx?Notificacion=1', '_blank', 'width=835,height=635,status=yes,resizable=no,top=0,left=150,scrollbars=yes');return false;"

        Me.txtEntidadCod.Attributes.Add("onkeydown", "javascript:return eliminarEntidad(event)")
        Me.txtProveedor.Attributes.Add("onkeydown", "javascript:return eliminarProveedor(event)")
    End Sub
    ''' <summary>
    ''' Inicializa javascript de los elementos
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0seg.</remarks>
    Private Sub WriteScripts()
        'Funcion que sirve para eliminar la Entidad
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarEntidad") Then
            Dim sScript As String = "function eliminarEntidad(event) {" & vbCrLf &
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf &
                    "  " & vbCrLf &
            " } else { " & vbCrLf &
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf &
                    " campoEntidad = document.getElementById('" & txtEntidadCod.ClientID & "')" & vbCrLf &
                    " if (campoEntidad) { campoEntidad.value = ''; } " & vbCrLf &
                    " campoHiddenEntidad = document.getElementById('" & hidEntidad.ClientID & "')" & vbCrLf &
                    " if (campoHiddenEntidad) { campoHiddenEntidad.value = ''; } " & vbCrLf &
            " } " & vbCrLf &
            "  " & vbCrLf &
            " } }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarEntidad", sScript, True)
        End If

        'Funcion que sirve para eliminar la proveedor
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarProveedor") Then
            Dim sScript As String = "function eliminarProveedor(event) {" & vbCrLf &
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf &
                    "  " & vbCrLf &
            " } else { " & vbCrLf &
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf &
                    " campoProveedor = document.getElementById('" & txtProveedor.ClientID & "')" & vbCrLf &
                    " if (campoProveedor) { campoProveedor.value = ''; } " & vbCrLf &
                    " campoHiddenProveedor = document.getElementById('" & hidProveedor.ClientID & "')" & vbCrLf &
                    " if (campoHiddenProveedor) { campoHiddenProveedor.value = ''; } " & vbCrLf &
            " } " & vbCrLf &
            "  " & vbCrLf &
            " } }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarProveedor", sScript, True)
        End If
    End Sub

    ''' <summary>
    ''' Carga las imagenes correspondiente en los controles
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0seg.</remarks>
    Private Sub CargaImagenes()
        FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/Notificaciones.png"

        CType(wdgDatos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgExcel"), Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/Excel.png"
        CType(wdgDatos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgPDF"), Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/PDF.png"

        imgProveedorLupa.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Img_Ico_Lupa.gif"
    End Sub
#End Region
#Region "Combo dependiente"
    ''' <summary>
    ''' Evento que salta cuando se cambia la seleccion del combo
    ''' </summary>
    ''' <param name="sender">control</param>
    ''' <param name="e">evento de sistema</param>  
    ''' <remarks>Llamada desde: sistema; Tiempo máximo:0 </remarks>
    Private Sub wddTipoEntidad_SelectionChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownSelectionChangedEventArgs) Handles wddTipoEntidad.SelectionChanged
        Dim Entidad As Integer

        If wddTipoEntidad.SelectedValue <> "" Then
            Entidad = wddTipoEntidad.SelectedValue
        Else
            Entidad = 0
        End If
        CargarTipoEmail(Entidad)
        If CInt(Entidad) = EntidadNotificacion.Calificaciones Or CInt(Entidad) = EntidadNotificacion.ControlPresupuestario Then
            spProceso.Visible = False
            spResto.Visible = True
            txtEntidadCod.Enabled = False
            txtEntidadCod.BackColor = Drawing.Color.LightGray
        ElseIf Entidad = EntidadNotificacion.ProcesoCompra Then
            spProceso.Visible = True
            spResto.Visible = False
            CargarAnyos()
            CargarCommodities()
        Else
            txtEntidadCod.Text = ""
            hidEntidad.Value = ""
            spProceso.Visible = False
            spResto.Visible = True
            txtEntidadCod.Enabled = True
            txtEntidadCod.BackColor = Drawing.Color.White

            txtEntidadCod_AutoCompleteExtender.ContextKey = Entidad

        End If

        upBusqueda.Update()
    End Sub
    ''' <summary>
    ''' Procedimiento que carga los tipos de email cuando hay cambio de seleccion en tipo de entidad
    ''' </summary>
    ''' <param name="Entidad">Tipo de entidad</param>
    ''' <remarks>Llamada desde: wddTipoEntidad_SelectionChanged; Tiempo máximo:0 </remarks>
    Private Sub CargarTipoEmail(ByVal Entidad As Integer)
        If Entidad <> "0" Then
            wddTipoEmail.Items.Clear()
            CargarTEmail(Entidad)
        Else
            wddTipoEmail.Items.Clear()
        End If

        wddTipoEmail.CurrentValue = ""
        wddTipoEmail.SelectedItemIndex = 0
    End Sub
    ''' <summary>
    ''' Procedimiento que carga los tipos de email según la entidad
    ''' </summary>
    ''' <param name="Entidad">tipo de entidad</param> 
    ''' <remarks>Llamada desde: CargarTipoEmail; Tiempo máximo:0 </remarks>
    Private Sub CargarTEmail(ByVal Entidad As Integer)
        AnyadirElementoTipoEmail(0, "")

        Select Case CInt(Entidad)
            Case EntidadNotificacion.Calificaciones
                AnyadirElementoTipoEmail(TipoNotificacion.CambioCalificaciónTotalProveedor, Textos(27))
                AnyadirElementoTipoEmail(TipoNotificacion.ErroresCalculoPuntuaciones, Textos(56)) 'Aviso de incidencia en el cálculo de puntuaciones
                AnyadirElementoTipoEmail(TipoNotificacion.AvisoPasoProveQAaReal, Textos(134))    'Aviso de conversión de un proveedor potencial en real
            Case EntidadNotificacion.Certificado
                AnyadirElementoTipoEmail(TipoNotificacion.CertificadoEmitido, Textos(28)) 'Solicitud de certificado
                AnyadirElementoTipoEmail(TipoNotificacion.CertificadoRenovado, Textos(29)) 'Renovación de certificado
                AnyadirElementoTipoEmail(TipoNotificacion.CertificadoEnviado, Textos(30)) 'Envío de certificado
                AnyadirElementoTipoEmail(TipoNotificacion.ProxExpiracionCertif, Textos(31)) 'Aviso de proximidad de expiración de certificado
                AnyadirElementoTipoEmail(TipoNotificacion.ExpiracionCertif, Textos(32)) 'Aviso de expiración de certificado
                AnyadirElementoTipoEmail(TipoNotificacion.BorradoPortal_Certif, Textos(33)) 'Aviso de eliminación de datos guardados sin enviar
                AnyadirElementoTipoEmail(TipoNotificacion.EnvioCertificado, Textos(57)) 'Certificado enviado
                AnyadirElementoTipoEmail(TipoNotificacion.EmailNoAutomatico, Textos(34)) 'Emails no automáticos
                AnyadirElementoTipoEmail(TipoNotificacion.CertificadosRevisados, Textos(120)) 'Certificados Revisados
            Case EntidadNotificacion.NoConformidad
                AnyadirElementoTipoEmail(TipoNotificacion.NoConformidadEmitida, Textos(35)) '	36	Emisión de no conformidad    'NoConformidadEmitidaContactos = 2055 'NoConformidadEmitidaRevisor = 2056
                AnyadirElementoTipoEmail(TipoNotificacion.EnvioNoConformidad, Textos(49)) 'Envío de una no conformidad (2073)
                AnyadirElementoTipoEmail(TipoNotificacion.ModifEstadosAccionesNoConf, Textos(36)) '	37	Modificación de estados de las acciones	 'ModifEstadosAccionesNoConfContactos = 2058
                AnyadirElementoTipoEmail(TipoNotificacion.BorradoPortal_NC, Textos(41)) '	42	Aviso de eliminación de datos guardados sin enviar	
                AnyadirElementoTipoEmail(TipoNotificacion.CierrePositivoRevisor, Textos(50)) 'Cierre positivo pendiente de revisar (2069)
                AnyadirElementoTipoEmail(TipoNotificacion.CierreNegativoRevisor, Textos(51)) 'Cierre negativo pendiente de revisar (2070)
                AnyadirElementoTipoEmail(TipoNotificacion.AprobacionCierrePosRevisor, Textos(52)) 'Aprobación del revisor al cierre  positivo (2065)
                AnyadirElementoTipoEmail(TipoNotificacion.AprobacionCierreNegRevisor, Textos(53)) 'Aprobación del revisor al cierre negativo (2066)
                AnyadirElementoTipoEmail(TipoNotificacion.RechazoCierrePosRevisor, Textos(54)) 'Rechazo del revisor al cierre positivo (2067)
                AnyadirElementoTipoEmail(TipoNotificacion.RechazoCierreNegRevisor, Textos(55)) 'Rechazo del revisor al cierre negativo (2068)
                AnyadirElementoTipoEmail(TipoNotificacion.CierrePositivo, Textos(37)) '	38	Cierre eficaz de no conformidad 'CierrePositivoContactos = 2060
                AnyadirElementoTipoEmail(TipoNotificacion.CierreNegativo, Textos(38)) '	39	Cierre no eficaz de no conformidad	'CierreNegativoContactos = 2062
                AnyadirElementoTipoEmail(TipoNotificacion.ReaperturaNoConf, Textos(39)) '	40	Reapertura de no conformidad 'ReaperturaNoConfContactos = 2064
                AnyadirElementoTipoEmail(TipoNotificacion.NoConformidadAnuladaProveedor, Textos(40)) '	41	Anulación de no conformidad	'NoConformidadAnuladaContactos = 2052 'NoConformidadAnuladaRevisor = 2053
                AnyadirElementoTipoEmail(TipoNotificacion.EmailNoAutomatico, Textos(34)) 'Emails no automáticos
                AnyadirElementoTipoEmail(TipoNotificacion.RealizarAccion, Textos(84))
                AnyadirElementoTipoEmail(TipoNotificacion.CambioEtapa, Textos(85))
            Case EntidadNotificacion.Orden
                AnyadirElementoTipoEmail(TipoNotificacion.NotificadoresTipo1, Textos(63)) 'Notificación de pedido (límite de adjudicación superado)
                AnyadirElementoTipoEmail(TipoNotificacion.NotificadoresTipo2, Textos(64)) 'Notificación de pedio (límite de importe de pedido superado)
                AnyadirElementoTipoEmail(TipoNotificacion.OrdenDenegadaParcialmente, Textos(65))
                AnyadirElementoTipoEmail(TipoNotificacion.OrdenDenegadaTotalmente, Textos(66))
                AnyadirElementoTipoEmail(TipoNotificacion.EmisionAUsuario, Textos(69))
                AnyadirElementoTipoEmail(TipoNotificacion.OrdenAProveedor, Textos(70))
                AnyadirElementoTipoEmail(TipoNotificacion.AprobadoresTipo1, Textos(71))
                AnyadirElementoTipoEmail(TipoNotificacion.AprobadoresTipo2, Textos(72))
                AnyadirElementoTipoEmail(TipoNotificacion.EmisionNotificados, Textos(75))
                AnyadirElementoTipoEmail(TipoNotificacion.AnulacionDeOrdenAProveedor, Textos(76))
                AnyadirElementoTipoEmail(TipoNotificacion.ErrorEnAprobacion, Textos(77))
                AnyadirElementoTipoEmail(TipoNotificacion.ErrorEnRechazo, Textos(78))
                AnyadirElementoTipoEmail(TipoNotificacion.OrdenAAprobador, Textos(79))
                AnyadirElementoTipoEmail(TipoNotificacion.RealizarAccion, Textos(84))
                AnyadirElementoTipoEmail(TipoNotificacion.CambioEtapa, Textos(85))
                AnyadirElementoTipoEmail(TipoNotificacion.OrdenBorrar, Textos(135))
                AnyadirElementoTipoEmail(TipoNotificacion.OrdenDeshacerBorrado, Textos(136))
                AnyadirElementoTipoEmail(TipoNotificacion.LineaBorrar, Textos(137))


            Case EntidadNotificacion.Recepcion
                AnyadirElementoTipoEmail(TipoNotificacion.RecepcionIncorrectaAprov, Textos(74))
                AnyadirElementoTipoEmail(TipoNotificacion.RecepcionCorrectaAprov, Textos(73))
            Case EntidadNotificacion.Solicitud, EntidadNotificacion.Encuesta, EntidadNotificacion.Factura, EntidadNotificacion.SolicitudQA
                AnyadirElementoTipoEmail(TipoNotificacion.Devolucion, Textos(80))
                AnyadirElementoTipoEmail(TipoNotificacion.Traslado, Textos(81))
                AnyadirElementoTipoEmail(TipoNotificacion.TrasladoAProveedor, Textos(82))
                AnyadirElementoTipoEmail(TipoNotificacion.DevolucionPortal, Textos(83))
                AnyadirElementoTipoEmail(TipoNotificacion.RealizarAccion, Textos(84))
                AnyadirElementoTipoEmail(TipoNotificacion.CambioEtapa, Textos(85))
                AnyadirElementoTipoEmail(TipoNotificacion.RealizarAccionProveedor, Textos(86))
                AnyadirElementoTipoEmail(TipoNotificacion.CambioEtapaProveedor, Textos(87))
                AnyadirElementoTipoEmail(TipoNotificacion.RealizarAccionPortal, Textos(88))
                AnyadirElementoTipoEmail(TipoNotificacion.CambioEtapaPortal, Textos(89))
                AnyadirElementoTipoEmail(TipoNotificacion.ErrorValidacion, Textos(90))
                AnyadirElementoTipoEmail(TipoNotificacion.AsignacionSolicitudCompra, Textos(91))
                AnyadirElementoTipoEmail(TipoNotificacion.AprobacionSolicitudPet, Textos(92))
                AnyadirElementoTipoEmail(TipoNotificacion.RechazoSolicitudPet, Textos(93))
                AnyadirElementoTipoEmail(TipoNotificacion.AnulacionSolicitudPet, Textos(94))
                AnyadirElementoTipoEmail(TipoNotificacion.CierreSolicitudPet, Textos(95))
            Case EntidadNotificacion.Contrato
                AnyadirElementoTipoEmail(TipoNotificacion.Devolucion, Textos(80))
                AnyadirElementoTipoEmail(TipoNotificacion.Traslado, Textos(81))
                AnyadirElementoTipoEmail(TipoNotificacion.TrasladoAProveedor, Textos(82))
                AnyadirElementoTipoEmail(TipoNotificacion.DevolucionPortal, Textos(83))
                AnyadirElementoTipoEmail(TipoNotificacion.RealizarAccion, Textos(84))
                AnyadirElementoTipoEmail(TipoNotificacion.CambioEtapa, Textos(85))
                AnyadirElementoTipoEmail(TipoNotificacion.RealizarAccionProveedor, Textos(86))
                AnyadirElementoTipoEmail(TipoNotificacion.CambioEtapaProveedor, Textos(87))
                AnyadirElementoTipoEmail(TipoNotificacion.RealizarAccionPortal, Textos(88))
                AnyadirElementoTipoEmail(TipoNotificacion.CambioEtapaPortal, Textos(89))
                AnyadirElementoTipoEmail(TipoNotificacion.ErrorValidacion, Textos(90))
                AnyadirElementoTipoEmail(TipoNotificacion.ContratoProximoExpirar, Textos(116))
            Case EntidadNotificacion.AutoFactura
                AnyadirElementoTipoEmail(TipoNotificacion.Devolucion, Textos(80))
                AnyadirElementoTipoEmail(TipoNotificacion.Traslado, Textos(81))
                AnyadirElementoTipoEmail(TipoNotificacion.TrasladoAProveedor, Textos(82))
                AnyadirElementoTipoEmail(TipoNotificacion.DevolucionPortal, Textos(83))
                AnyadirElementoTipoEmail(TipoNotificacion.RealizarAccion, Textos(84))
                AnyadirElementoTipoEmail(TipoNotificacion.CambioEtapa, Textos(85))
                AnyadirElementoTipoEmail(TipoNotificacion.RealizarAccionProveedor, Textos(86))
                AnyadirElementoTipoEmail(TipoNotificacion.CambioEtapaProveedor, Textos(87))
                AnyadirElementoTipoEmail(TipoNotificacion.RealizarAccionPortal, Textos(88))
                AnyadirElementoTipoEmail(TipoNotificacion.CambioEtapaPortal, Textos(89))
                AnyadirElementoTipoEmail(TipoNotificacion.ErrorValidacion, Textos(90))
            Case EntidadNotificacion.ProcesoCompra
                AnyadirElementoTipoEmail(TipoNotificacion.PetOferta, Textos(97))
                AnyadirElementoTipoEmail(TipoNotificacion.PetOfertaWeb, Textos(98))
                AnyadirElementoTipoEmail(TipoNotificacion.PetOfertaSubasta, Textos(112))
                AnyadirElementoTipoEmail(TipoNotificacion.ComObjetivos, Textos(104))
                AnyadirElementoTipoEmail(TipoNotificacion.ComObjetivosWeb, Textos(105))
                AnyadirElementoTipoEmail(TipoNotificacion.AdjudicacionYOrdenCompra, Textos(106))
                AnyadirElementoTipoEmail(TipoNotificacion.ProveNoAdjudicado, Textos(107))
                AnyadirElementoTipoEmail(TipoNotificacion.ProximaDespublicacion, Textos(108))
                AnyadirElementoTipoEmail(TipoNotificacion.AvisoFinSubasta, Textos(113))
                AnyadirElementoTipoEmail(TipoNotificacion.InicioSubasta, Textos(114))
                AnyadirElementoTipoEmail(TipoNotificacion.ProximidadInicioSubasta, Textos(115))
                AnyadirElementoTipoEmail(TipoNotificacion.ConvocatoriaReunion, Textos(118))
                AnyadirElementoTipoEmail(TipoNotificacion.AvisoInvitacionProceso, Textos(121))
                AnyadirElementoTipoEmail(TipoNotificacion.AvisoExclusionProceso, Textos(122))
                AnyadirElementoTipoEmail(TipoNotificacion.AvisoPreadjProceso, Textos(123))
                AnyadirElementoTipoEmail(TipoNotificacion.AvisoCierreProceso, Textos(124))
                AnyadirElementoTipoEmail(TipoNotificacion.AvisoAnulacionProceso, Textos(133))
            Case EntidadNotificacion.PedDirecto
                AnyadirElementoTipoEmail(TipoNotificacion.PedEmision, Textos(111))
                AnyadirElementoTipoEmail(TipoNotificacion.NotifPedidoReceptor, Textos(110))
                AnyadirElementoTipoEmail(TipoNotificacion.PedAnulacionMail, Textos(109))
                AnyadirElementoTipoEmail(TipoNotificacion.PedRecepKo, Textos(67))
                AnyadirElementoTipoEmail(TipoNotificacion.PedRecepOk, Textos(68))
                AnyadirElementoTipoEmail(TipoNotificacion.PetDatosExtMail, Textos(116))
                AnyadirElementoTipoEmail(TipoNotificacion.PedAceptacion, Textos(117))
                AnyadirElementoTipoEmail(TipoNotificacion.PedNotificacionProve, Textos(119))
            Case EntidadNotificacion.ControlPresupuestario
                AnyadirElementoTipoEmail(TipoNotificacion.SMDisponibleNegativo, Textos(127))
            Case EntidadNotificacion.EscalacionProveedores
                AnyadirElementoTipoEmail(TipoNotificacion.AvisoEscalacionPendienteAprobar, Textos(129))
                AnyadirElementoTipoEmail(TipoNotificacion.AvisoEscalacionProveedor, Textos(130))
                AnyadirElementoTipoEmail(TipoNotificacion.AvisoEscalacionPendienteValidarCierreEficaz, Textos(131))
                AnyadirElementoTipoEmail(TipoNotificacion.AvisoEscalacionSinCerrarXMeses, Textos(132))
            Case Else

        End Select

        AnyadirElementoTipoEmail(TipoNotificacion.NoDefinido, Me.Textos(128))
    End Sub
    ''' <summary>
    ''' Carga un registro en el combo de Tipo de Email
    ''' </summary>
    ''' <param name="value">Valor del combo</param>
    ''' <param name="text">Texto del combo</param>
    ''' <remarks>Llamada desde: CargarTEmail; Tiempo maximo:0</remarks>
    Private Sub AnyadirElementoTipoEmail(ByVal value As Nullable(Of Integer), ByVal text As String)
        Dim oItem As New ListControls.DropDownItem
        oItem.Value = value
        oItem.Text = text
        wddTipoEmail.Items.Add(oItem)
    End Sub
#End Region
#Region "Actualiza el webdropdown para q filtros lo coja"
    ''' <summary>
    ''' Actualiza el webdropdown
    ''' </summary>
    ''' <param name="sender">control</param>
    ''' <param name="e">evento de sistema</param>  
    ''' <remarks>Llamada desde: sistema; Tiempo máximo:0 </remarks>
    Private Sub wddEstado_ValueChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownValueChangedEventArgs) Handles wddEstado.ValueChanged
        upBusqueda.Update()
    End Sub
    ''' <summary>
    ''' Actualiza el webdropdown
    ''' </summary>
    ''' <param name="sender">control</param>
    ''' <param name="e">evento de sistema</param>  
    ''' <remarks>Llamada desde: sistema; Tiempo máximo:0 </remarks>
    Private Sub wddTipoEmail_ValueChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownValueChangedEventArgs) Handles wddTipoEmail.ValueChanged
        upBusqueda.Update()
    End Sub
#End Region
#Region "Grid Notificaciones"
    ''' <summary>
    ''' Tras la carga del grid se "sincroniza" con el Custom Pager
    ''' </summary>
    ''' <param name="sender">control</param>
    ''' <param name="e">evento de sistema</param>  
    ''' <remarks>Llamada desde: sistema; Tiempo máximo:0 </remarks>
    Private Sub wdgDatos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles wdgDatos.DataBound
        If (_pagerControl.MaxPage <> Me.wdgDatos.GridView.Behaviors.Paging.PageCount) Then
            _pagerControl.SetupPageList(Me.wdgDatos.GridView.Behaviors.Paging.PageCount)
        End If
        If Request("__EVENTTARGET") <> Replace(_pagerControl.PagerPageList.ClientID.ToString, "_", "$") Then
            _pagerControl.SetCurrentPageNumber(wdgDatos.GridView.Behaviors.Paging.PageIndex)
        End If
    End Sub
    ''' <summary>
    ''' Tras la carga del grid se "sincroniza" con el Custom Pager
    ''' </summary>
    ''' <param name="sender">control</param>
    ''' <param name="e">evento de sistema</param>  
    ''' <remarks>Llamada desde: sistema; Tiempo mÃ¡ximo:0 </remarks>
    Private Sub wdgDatos_DataFiltered(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.FilteredEventArgs) Handles wdgDatos.DataFiltered
        If (_pagerControl.MaxPage <> Me.wdgDatos.GridView.Behaviors.Paging.PageCount) Then
            _pagerControl.SetupPageList(Me.wdgDatos.GridView.Behaviors.Paging.PageCount)
        End If
        If Request("__EVENTTARGET") <> Replace(_pagerControl.PagerPageList.ClientID.ToString, "_", "$") Then
            _pagerControl.SetCurrentPageNumber(wdgDatos.GridView.Behaviors.Paging.PageIndex)
        End If
    End Sub
    ''' <summary>
    ''' Tras la carga del grid se "sincroniza" con el Custom Pager
    ''' </summary>
    ''' <param name="sender">control</param>
    ''' <param name="e">evento de sistema</param>  
    ''' <remarks>Llamada desde: sistema; Tiempo mÃ¡ximo:0 </remarks>
    Private Sub wdgDatos_GroupedColumnsChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.GroupedColumnsChangedEventArgs) Handles wdgDatos.GroupedColumnsChanged
        If (_pagerControl.MaxPage <> Me.wdgDatos.GridView.Behaviors.Paging.PageCount) Then
            _pagerControl.SetupPageList(Me.wdgDatos.GridView.Behaviors.Paging.PageCount)
        End If

        If Request("__EVENTTARGET") = Replace(Me._pagerControl.Controls(6).ClientID.ToString, "_", "$") Then
        Else
            _pagerControl.SetCurrentPageNumber(wdgDatos.GridView.Behaviors.Paging.PageIndex)
        End If
    End Sub
    ''' <summary>
    ''' Inicializa el grid
    ''' </summary>
    ''' <param name="sender">control</param>
    ''' <param name="e">evento de sistema</param>  
    ''' <remarks>Llamada desde: sistema; Tiempo máximo:0 </remarks>
    Private Sub wdgDatos_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles wdgDatos.InitializeRow
        Dim Index As Integer = sender.Columns.FromKey("ENTIDAD").Index
        Dim IndexId As Integer = sender.Columns.FromKey("ENTIDAD_EMAIL").Index
        Dim IndexEstado As Integer = sender.Columns.FromKey("KO").Index
        Dim IndexEnviado As Integer = sender.Columns.FromKey("ENVIADO").index
        Dim UrlImage As String = Nothing
        Dim gridCellIndex As Integer

        If Request("__EVENTTARGET") = "ExportarExcel" OrElse Request("__EVENTTARGET") = "ExportarPDF" Then
            'viene en el idioma desde bbdd
            e.Row.Items.Item(Index).Text = e.Row.Items.Item(Index).Value
            e.Row.Items.Item(IndexEnviado).Text = e.Row.Items.Item(IndexEnviado).Value
        Else
            If Not IsDBNull(e.Row.Items.Item(IndexId).Value) Then
                If e.Row.Items.Item(IndexId).Value = EntidadNotificacion.Calificaciones Then
                    UrlImage = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Calificaciones15.png"
                ElseIf e.Row.Items.Item(IndexId).Value = EntidadNotificacion.Certificado Then
                    UrlImage = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Certificado15.png"
                Else
                    UrlImage = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/NoConformidad15.png"
                End If
            End If

            Dim sTexto As String = "&nbsp;" & e.Row.Items.Item(Index).Value
            e.Row.Items.Item(Index).Text = "<img src='" & UrlImage & "' style='text-align:center;'/>"
            e.Row.Items.Item(Index).Text = e.Row.Items.Item(Index).Text & sTexto

            If (e.Row.Items.Item(IndexEstado).Value = 0) Then
                UrlImage = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/aprobado.gif"
            Else
                UrlImage = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Img_Ico_Alerta_Peq.gif"
            End If

            e.Row.Items.Item(IndexEnviado).Text = "<img src='" & UrlImage & "' style='text-align:center;'/>"
            End If

            e.Row.CssClass = "rowWebHierarchical"

            For Each item As Object In wdgDatos.GridView.Columns
                If item.Type = System.Type.GetType("System.DateTime") Then
                    gridCellIndex = wdgDatos.GridView.Columns.FromKey(item.Key).Index
                    CType(e.Row.Items(gridCellIndex).Column, Infragistics.Web.UI.GridControls.BoundDataField).DataFormatString = "{0:" & FSNUser.DateFormat.ShortDatePattern & "}"
                End If
            Next
    End Sub
#End Region
#Region "Botonera"
    ''' <summary>
    ''' Exporta a Excel
    ''' </summary>
    ''' <remarks>Llamada desde: Page_load; Tiempo máximo:0,1</remarks>
    Private Sub ExportarExcel()
        Dim fileName As String = HttpUtility.UrlEncode(FSNPageHeader.TituloCabecera)
        fileName = fileName.Replace("+", "%20")

        With wdgExcelExporter
            .DownloadName = fileName
            .DataExportMode = DataExportMode.AllDataInDataSource
            .WorkbookFormat = Infragistics.Documents.Excel.WorkbookFormat.Excel2007
            .Export(wdgDatos)
        End With
    End Sub
    ''' <summary>
    ''' Exporta a PDF
    ''' </summary>
    ''' <remarks>Llamada desde: Page_load; Tiempo máximo:0,1</remarks>
    Private Sub ExportarPDF()
        Dim fileName As String = HttpUtility.UrlEncode(FSNPageHeader.TituloCabecera)
        fileName = fileName.Replace("+", "%20")
        wdgPDFExporter.DownloadName = fileName

        With wdgPDFExporter
            .EnableStylesExport = True

            .DataExportMode = DataExportMode.AllDataInDataSource
            .TargetPaperOrientation = PageOrientation.Landscape

            .Margins = PageMargins.GetPageMargins("Narrow")
            .TargetPaperSize = PageSizes.GetPageSize("A4")

            .Export(wdgDatos)
        End With
    End Sub
    ''' <summary>
    ''' Devuelve los procesos con los filtros indicados
    ''' </summary>
    ''' <param name="Anyo">Año del proceso</param>
    ''' <param name="Commodity">Commodity del proceso</param>
    ''' <remarks></remarks>
    Private Sub CargarProcesos(ByVal Anyo As Short, ByVal Commodity As String)
        ddlDenominacion.Items.Clear()
        ddlCodigo.Items.Clear()
        ddlDenominacion.SelectedValue = ""
        ddlCodigo.SelectedValue = ""
        Dim oProceso As FSNServer.Procesos
        oProceso = FSNServer.Get_Object(GetType(FSNServer.Procesos))
        If Commodity <> "" Then
            oProceso.CargarProcesos(Idioma, Anyo, Commodity)
            Dim oItem As Infragistics.Web.UI.ListControls.DropDownItem
            ddlCodigo.Items.Clear()
            oItem = New Infragistics.Web.UI.ListControls.DropDownItem("", "")
            ddlCodigo.Items.Add(oItem)
            For Each oRow As DataRow In oProceso.Data.Tables(0).Rows
                oItem = New Infragistics.Web.UI.ListControls.DropDownItem
                oItem.Value = DBNullToStr(oRow.Item("COD"))
                oItem.Text = DBNullToStr(oRow.Item("COD"))
                ddlCodigo.Items.Add(oItem)
            Next
        End If
        oProceso = Nothing
    End Sub
    ''' <summary>
    ''' Carga los años en el combo de años del proceso. Desde el actual menos 10 hasta el actual más 10.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarAnyos()
        Dim iAnyoActual As Integer
        Dim iInd As Integer
        Dim ddItem As Infragistics.Web.UI.ListControls.DropDownItem
        iAnyoActual = Year(Now)
        ddlAnyo.Items.Clear()
        ddItem = New Infragistics.Web.UI.ListControls.DropDownItem("", 0)
        ddlAnyo.Items.Add(ddItem)
        For iInd = iAnyoActual - 10 To iAnyoActual + 10
            ddItem = New Infragistics.Web.UI.ListControls.DropDownItem
            ddItem.Value = iInd
            ddItem.Text = iInd.ToString
            ddlAnyo.Items.Add(ddItem)
        Next
        ddlAnyo.SelectedItemIndex = ddlAnyo.Items.IndexOf(ddlAnyo.Items.FindItemByValue(iAnyoActual.ToString))
    End Sub
    ''' <summary>
    ''' Carga los materiales de nivel 1
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarCommodities()
        Dim oCommodities As FSNServer.GrupoMatNivel1
        oCommodities = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel1))
        If FSNUser.Pyme > 0 Then
            Dim oGruposMaterial As FSNServer.GruposMaterial
            oGruposMaterial = FSNServer.Get_Object(GetType(FSNServer.GruposMaterial))
            oCommodities.LoadData(oGruposMaterial.DevolverGMN1_PYME(FSNUser.Pyme), , , , FSNUser.Idioma)
        Else
            oCommodities.LoadData(, , , , FSNUser.Idioma)
        End If
        Dim oItem As Infragistics.Web.UI.ListControls.DropDownItem
        ddlCommodity.Items.Clear()
        oItem = New Infragistics.Web.UI.ListControls.DropDownItem("", "")
        ddlCommodity.Items.Add(oItem)
        For Each oRow As DataRow In oCommodities.Data.Tables(0).Rows
            oItem = New Infragistics.Web.UI.ListControls.DropDownItem
            oItem.Value = DBNullToStr(oRow.Item("COD"))
            If IsDBNull(oRow.Item("COD")) Then
                oItem.Text = ""
            Else
                oItem.Text = DBNullToStr(oRow.Item("DEN" & "_" & Idioma))
            End If
            ddlCommodity.Items.Add(oItem)
        Next
        oCommodities = Nothing
    End Sub
    ''' <summary>
    ''' Procedimiento que carga los procesos en los combos cuando hay cambio de seleccion en uno de ellos
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarProcesos()
        Dim Commodity As String = ""
        Dim Anyo As Short
        If ddlCommodity.SelectedValue <> "" Then
            Commodity = ddlCommodity.SelectedValue
        End If

        If IsNumeric(ddlAnyo.SelectedValue) Then
            Anyo = ddlAnyo.SelectedValue
        End If

        If Commodity <> "" Then
            CargarProcesos(Anyo, Commodity)
        Else
            ddlCodigo.Items.Clear()
            ddlDenominacion.Items.Clear()
        End If
        ddlCodigo.CurrentValue = ""
        ddlDenominacion.CurrentValue = ""
    End Sub
    ''' <summary>
    ''' Carga el proceso seleccionado desde el buscador de procesos en los combos
    ''' </summary>
    ''' <param name="sAnyo">Año del proceso</param>
    ''' <param name="sGMN1">Commodity del proceso</param>
    ''' <param name="Codigo">Codigo del proceso</param>
    ''' <remarks></remarks>
    Private Sub CargarProcesoEnDropDowns(ByVal sAnyo As String, ByVal sGMN1 As String, ByVal Codigo As String)
        'deseleccionamos los combos pues si habria algun proceso seleccionado
        If sAnyo = "" Or sGMN1 = "" Then
            If ddlAnyo.SelectedItems.Count > 0 Then
                For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlAnyo.SelectedItems
                    ddi.Selected = False
                Next
            End If
            If ddlCommodity.SelectedItems.Count > 0 Then
                For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlCommodity.SelectedItems
                    ddi.Selected = False
                Next
            End If
            ddlCommodity.CurrentValue = ""
            If ddlCodigo.SelectedItems.Count > 0 Then
                For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlCodigo.SelectedItems
                    ddi.Selected = False
                Next
            End If
            ddlCodigo.CurrentValue = ""
            If ddlDenominacion.SelectedItems.Count > 0 Then
                For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlDenominacion.SelectedItems
                    ddi.Selected = False
                Next
            End If
            ddlDenominacion.CurrentValue = ""
        Else
            For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlAnyo.Items
                If ddi.Value = sAnyo Then
                    ddi.Selected = True
                    ddlAnyo.CurrentValue = ddi.Text
                    Exit For
                End If
            Next
            For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlCommodity.Items
                If ddi.Value = sGMN1 Then
                    ddi.Selected = True
                    ddlCommodity.CurrentValue = ddi.Text
                    Exit For
                End If
            Next
            If Codigo <> "" Then
                CargarProcesos(sAnyo, sGMN1)
                For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlCodigo.Items
                    If ddi.Value = Codigo Then
                        ddi.Selected = True
                        ddlCodigo.CurrentValue = ddi.Text
                        Exit For
                    End If
                Next
                For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlDenominacion.Items
                    If ddi.Value = Codigo Then
                        ddi.Selected = True
                        ddlDenominacion.CurrentValue = ddi.Text
                        Exit For
                    End If
                Next
            End If
        End If
    End Sub
    ''' <summary>
    ''' Evento que salta cuando se cambia el valor del combo del anyo de proceso
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ddlAnyo_ValueChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownValueChangedEventArgs) Handles ddlAnyo.ValueChanged
        If Not ddlAnyo.Items.FindItemByValue(e.NewValue) Is Nothing Then
            Dim oItem As Infragistics.Web.UI.ListControls.DropDownItem = ddlAnyo.Items.FindItemByValue(e.NewValue)
            ddlAnyo.ClearSelection()
            oItem.Selected = True
            ddlAnyo.CurrentValue = oItem.Text
            ddlCommodity.ClearSelection()
            CargarProcesos()
        Else
            If e.NewValue.ToString.Length < e.OldValue.ToString.Length Then
                'Esta borrando el anyo
                ddlAnyo.ClearSelection()
                ddlAnyo.CurrentValue = ""
                ddlCommodity.ClearSelection()
                CargarProcesos()
            End If
        End If
    End Sub
    Private Sub ddlDenominacion_SelectionChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownSelectionChangedEventArgs) Handles ddlDenominacion.SelectionChanged
        Dim valor As String = ddlDenominacion.Items(ddlDenominacion.ActiveItemIndex).Value
        Dim oItem As Infragistics.Web.UI.ListControls.DropDownItem
        ddlCodigo.ClearSelection()
        For Each oItem In ddlCodigo.Items
            If oItem.Value = valor Then
                oItem.Selected = True
                ddlCodigo.CurrentValue = oItem.Text
                Exit For
            End If
        Next
    End Sub
    ''' <summary>
    ''' Evento que salta cuando se cambia la seleccion del combo
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ddlCommodity_SelectionChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownSelectionChangedEventArgs) Handles ddlCommodity.SelectionChanged
        CargarProcesos()
    End Sub
    ''' <summary>
    ''' Evento que salta cuando se cambia el valor del combo de commodity de proceso
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ddlCommodity_ValueChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownValueChangedEventArgs) Handles ddlCommodity.ValueChanged
        If Not ddlCommodity.SelectedItem Is Nothing Then
            If ddlCommodity.CurrentValue <> ddlCommodity.SelectedItem.Text Then
                ddlCommodity.ClearSelection()
                ddlCommodity.CurrentValue = ""
                CargarProcesos()
            End If
        End If
    End Sub
    ''' <summary>
    ''' Evento que salta cuando se cambia el valor del combo de codigo de proceso
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ddlCodigo_ValueChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownValueChangedEventArgs) Handles ddlCodigo.ValueChanged
        If Not ddlCodigo.SelectedItem Is Nothing Then
            If ddlCodigo.CurrentValue <> ddlCodigo.SelectedItem.Text Then
                ddlCodigo.ClearSelection()
                ddlCodigo.CurrentValue = ""
                ddlDenominacion.ClearSelection()
                ddlDenominacion.CurrentValue = ""
            End If
        End If
        If Not ddlCodigo.Items.FindItemByValue(e.NewValue) Is Nothing Then
            Dim oItem As Infragistics.Web.UI.ListControls.DropDownItem = ddlCodigo.Items.FindItemByValue(e.NewValue)
            oItem.Selected = True
        Else
            Exit Sub
        End If
        Dim valor As String = ddlCodigo.Items(ddlCodigo.SelectedItemIndex).Value 'ddlCodigo.SelectedItem.Value.ToString
        ddlCodigo.CurrentValue = ddlCodigo.Items(ddlCodigo.SelectedItemIndex).Text
        ddlDenominacion.ClearSelection()
        For Each oItem In ddlDenominacion.Items
            If oItem.Value = valor Then
                oItem.Selected = True
                ddlDenominacion.CurrentValue = oItem.Text
                Exit For
            End If
        Next
    End Sub
#End Region
End Class