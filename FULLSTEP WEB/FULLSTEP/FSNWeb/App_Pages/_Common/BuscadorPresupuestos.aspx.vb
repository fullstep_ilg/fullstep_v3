﻿Public Partial Class BuscadorPresupuestos
    Inherits FSNPage

#Region "Controles"
    Protected WithEvents lblUnidadOrganizativa As System.Web.UI.WebControls.Label
    Protected WithEvents ugtxtunidadorganizativa As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents lblAnyo As System.Web.UI.WebControls.Label
    Protected WithEvents ugtxtAnyo As Infragistics.Web.UI.EditorControls.WebNumericEditor
    Protected WithEvents cmdBuscar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents lblCodigo As System.Web.UI.WebControls.Label
    Protected WithEvents txtCodigo As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblDenominacion As System.Web.UI.WebControls.Label
    Protected WithEvents txtDenominacion As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblResultado As System.Web.UI.WebControls.Label
    Protected WithEvents lblSeleccionaArbol As System.Web.UI.WebControls.Label
    Protected WithEvents txtPorcent As Infragistics.Web.UI.EditorControls.WebNumericEditor
    Protected WithEvents lbltitulo As System.Web.UI.WebControls.Label
    Protected WithEvents lblUorganizativaRestringida As System.Web.UI.WebControls.Label
    Protected WithEvents lblAsignado As System.Web.UI.WebControls.Label
    Protected WithEvents txtAsignadoPorcent As Infragistics.Web.UI.EditorControls.WebNumericEditor
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents lblPendiente As System.Web.UI.WebControls.Label
    Protected WithEvents txtPendientePorcent As Infragistics.Web.UI.EditorControls.WebNumericEditor
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents lblPorcen1 As System.Web.UI.WebControls.Label
    Protected WithEvents ugtxtPorcentDetalle As Infragistics.Web.UI.EditorControls.WebNumericEditor
    Protected WithEvents lblPorcen2 As System.Web.UI.WebControls.Label
    Protected WithEvents cmdAceptar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cmdCancelar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cmdCambiar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents TIPO As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents IDCONTROL As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtunidadorganizativa As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents UON1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents UON2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents UON3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Valor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Anyo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents TIPOPRESUPUESTO As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents USUARIORESTRINGIDO As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents DENUON As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents EsCabeceraDesglose As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Table1 As System.Web.UI.WebControls.Table
    Protected WithEvents chkFavAutomaticos As System.Web.UI.WebControls.CheckBox

    Protected WithEvents pnlBotonesAniadirQuitar As System.Web.UI.WebControls.Panel
    Protected WithEvents tabladetallePresAsignado As System.Web.UI.WebControls.Panel
    Protected WithEvents layergrid As System.Web.UI.WebControls.Panel
    Protected WithEvents DetDenPresAsignado As System.Web.UI.WebControls.Label
    Protected WithEvents DetUonPresAsignado As System.Web.UI.WebControls.Label
    Protected WithEvents AdmiteMultiplesPresupuestos As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents OrgCompras As System.Web.UI.HtmlControls.HtmlInputHidden
#End Region
    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
        "<script language=""{0}"">{1}</script>"

    Private bMostrarArbolyGrid As Boolean = False
    Private cadenaerrormultiplesUO1 As String
    Private cadenaerrormultiplesUO2 As String
    Private iTipo As Integer ' tipo de prespuesto
    Private dtNewRow As DataRow
    Private oDGElement As DataTable
    Private dsTable As DataSet

    ''' <summary>
    ''' Carga la página del buscador de presupuestos
    ''' </summary>
    ''' <remarks>Llamada desde: Al cargar la página; Tiempo máx: 0, 3 sg</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim lInstancia As Long
        Dim lSolicitud As Long
        Dim mcadenapresupuestos As String 'cadena que guarda la descripcion del tipo de presupuesto
        Dim mcadenapresupuestosplural As String

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Presupuestos

        Dim cadenaunidadorganizativa As String = Textos(17)
        Dim cadenaerror100 As String = Textos(26)
        Dim cadenaerrorfiltrado As String = Textos(27)
        Dim cadenaerrorsinpresupuestos As String = Textos(28)
        cadenaerrormultiplesUO1 = Textos(29)
        cadenaerrormultiplesUO2 = Textos(30)

        Dim sScriptErrores As String
        sScriptErrores = "var cadenaunidadorganizativa = '" + JSText(cadenaunidadorganizativa) + "';"
        sScriptErrores += "var cadenaerror100 = '" + JSText(cadenaerror100) + "';"
        sScriptErrores += "var cadenaerrorfiltrado = '" + JSText(cadenaerrorfiltrado) + "';"
        sScriptErrores += "var cadenaerrorsinpresupuestos = '" + JSText(cadenaerrorsinpresupuestos) + "';"
        sScriptErrores = String.Format(IncludeScriptKeyFormat, "javascript", sScriptErrores)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "scripterrores", sScriptErrores)

        Me.lblUnidadOrganizativa.Text = Textos(17) & ":"
        Me.lblAnyo.Text = Textos(7)
        Me.lblAsignado.Text = Textos(11)
        Me.cmdBuscar.Value = Textos(18)
        Me.lblCodigo.Text = Textos(19)
        Me.lblDenominacion.Text = Textos(20)
        Me.lblPendiente.Text = Textos(12)
        Me.lblResultado.Text = Textos(21)
        Me.lblSeleccionaArbol.Text = Textos(22)
        Me.cmdCambiar.Value = Textos(23)
        Me.cmdAceptar.Value = Textos(2)
        Me.cmdCancelar.Value = Textos(3)

        imgCabecera.Src = ConfigurationManager.AppSettings("ruta") & "images/centrodecostebuscar.jpg"
        imganiadir.Src = ConfigurationManager.AppSettings("ruta") & "images/aniadir.gif"""
        imgquitar.Src = ConfigurationManager.AppSettings("ruta") & "images/quitar.gif"
        imgfavoritos.Src = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/favoritos_add.gif"
        imgAyuda.Src = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/help.gif"

        Dim oCampo As FSNServer.Campo
        Dim oUnidades As FSNServer.UnidadesOrg
        Dim oUnidadesCadenaPresupuesto As FSNServer.UnidadesOrg 'La utilizo para obtener la cadena de BD del tipo de presupuesto seleccionado

        If Request("iTipo") Is Nothing Then
            Dim idCampo As Integer = CInt(Request("Campo"))

            If Request("Solicitud") <> Nothing Then lSolicitud = Request("Solicitud")
            If Request("Instancia") <> Nothing Then lInstancia = Request("Instancia")

            oCampo = FSNServer.Get_Object(GetType(FSNServer.Campo))
            oCampo.Id = idCampo

            If lInstancia > 0 Then
                oCampo.LoadInst(lInstancia, Idioma)
            Else
                oCampo.Load(Idioma, lSolicitud)
            End If

            Select Case oCampo.IdTipoCampoGS
                Case TiposDeDatos.TipoCampoGS.PRES1
                    iTipo = 1
                Case TiposDeDatos.TipoCampoGS.Pres2
                    iTipo = 2
                Case TiposDeDatos.TipoCampoGS.Pres3
                    iTipo = 3
                Case TiposDeDatos.TipoCampoGS.Pres4
                    iTipo = 4
            End Select
        Else
            iTipo = CType(Request("iTipo"), Integer)
        End If

        Dim sScriptNivelMinimo As String = ""
        With FSNServer.TipoAcceso
            Select Case iTipo
                Case 1
                    sScriptNivelMinimo = "var cadenaNivelMinimo = '" + JSText(Replace(Textos(35), "##", .gbPres1Denominacion(Idioma.ToString))) + " " + CStr(.giPres1Nivel) + "';"
                    sScriptNivelMinimo += "var NivelMinimo = " + CStr(.giPres1Nivel) + ";"
                Case 2
                    sScriptNivelMinimo = "var cadenaNivelMinimo = '" + JSText(Replace(Textos(35), "##", .gbPres2Denominacion(Idioma.ToString))) + " " + CStr(.giPres2Nivel) + "';"
                    sScriptNivelMinimo += "var NivelMinimo = " + CStr(.giPres2Nivel) + ";"
                Case 3
                    sScriptNivelMinimo = "var cadenaNivelMinimo = '" + JSText(Replace(Textos(35), "##", .gbPres3Denominacion(Idioma.ToString))) + " " + CStr(.giPres3Nivel) + "';"
                    sScriptNivelMinimo += "var NivelMinimo = " + CStr(.giPres3Nivel) + ";"
                Case 4
                    sScriptNivelMinimo = "var cadenaNivelMinimo = '" + JSText(Replace(Textos(35), "##", .gbPres4Denominacion(Idioma.ToString))) + " " + CStr(.giPres4Nivel) + "';"
                    sScriptNivelMinimo += "var NivelMinimo = " + CStr(.giPres4Nivel) + ";"
            End Select
        End With
        sScriptNivelMinimo = String.Format(IncludeScriptKeyFormat, "javascript", sScriptNivelMinimo)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "scriptNivelMinimo", sScriptNivelMinimo)

        'Como ya se el tipo del presupuesto voy a obtener la cadena asociada a dicho tipo de presupuesto
        oUnidadesCadenaPresupuesto = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))
        oUnidadesCadenaPresupuesto.CargarCadenaPresupuesto(iTipo, Idioma)
        mcadenapresupuestos = oUnidadesCadenaPresupuesto.Data.Tables(0).Rows(0).Item(0)
        mcadenapresupuestosplural = oUnidadesCadenaPresupuesto.Data.Tables(0).Rows(0).Item(1)

        mcadenapresupuestos = Textos(24) + " " + mcadenapresupuestos

        Me.lbltitulo.Text = mcadenapresupuestos
        Me.chkFavAutomaticos.Text = Textos(32) & " " & mcadenapresupuestosplural & " " & Textos(33)
        chkFavAutomaticos.Checked = FSNUser.PMAnyadirPresFav(iTipo)
        Dim anyo As Integer = 0
        Me.TIPOPRESUPUESTO.Value = iTipo 'Al campo oculto de mi formulario le pongo el tipo del presupuesto

        Dim _NumberFormat As System.Globalization.NumberFormatInfo
        _NumberFormat = FSNUser.NumberFormat
        _NumberFormat.NumberGroupSeparator = ""

        Dim sScriptSeparador As String
        sScriptSeparador = "var separadorDecimal = '" + _NumberFormat.NumberDecimalSeparator + "';"
        sScriptSeparador = String.Format(IncludeScriptKeyFormat, "javascript", sScriptSeparador)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "scriptSeparador", sScriptSeparador)

        ''Los presupuestos de tipo 1 y 2 son anuales por lo tanto tienen campo de año
        If (iTipo = 1) Or (iTipo = 2) Then
            Me.ugtxtAnyo.Visible = True
            Me.lblAnyo.Visible = True
            Dim rightNow As DateTime = DateTime.Now
            Dim s As String

            s = rightNow.ToString()
            anyo = rightNow.Year()

            Me.ugtxtAnyo.NullText = anyo
            'Me.ugtxtAnyo = _NumberFormat

            If (Request.Form("Anyo") <> Nothing) Then
                Me.ugtxtAnyo.Value = Request.Form("Anyo")
                anyo = Int32.Parse(Request.Form("Anyo").ToString)
            Else
                Me.ugtxtAnyo.Value = Numero(anyo, "", ".")
            End If
        Else
            'Aqui se deberian de ocultar
            Me.ugtxtAnyo.Visible = False
            Me.lblAnyo.Visible = False
            Me.ugtxtAnyo.ValueText = 0
            anyo = 0
        End If

        Dim idControl = Request("IDPres")
        Me.IDCONTROL.Value = idControl

        Dim sUon1 As String
        Dim sUon2 As String
        Dim sUon3 As String
        Dim Valor As String 'Campo que recoge los presupuestos asignados.

        Dim sScript As String = ""
        Dim DS As DataSet

        Dim oDGFavoritos As DataTable
        DS = New DataSet
        Dim pKey(0) As DataColumn
        oDGElement = DS.Tables.Add("TEMP")
        oDGElement.Columns.Add("PORCENTAGE", System.Type.GetType("System.Int32"))
        oDGElement.Columns.Add("DESCRIPCION", System.Type.GetType("System.String"))
        oDGElement.Columns.Add("VALORASOCIADO", System.Type.GetType("System.String"))
        pKey(0) = oDGElement.Columns("VALORASOCIADO")
        oDGElement.PrimaryKey = pKey

        oDGFavoritos = DS.Tables.Add("FAVORITOS")
        oDGFavoritos.Columns.Add("DESCRIPCION", System.Type.GetType("System.String"))
        oDGFavoritos.Columns.Add("VALORASOCIADO", System.Type.GetType("System.String"))


        If Page.IsPostBack = True Then
            Valor = Request.Form("Valor")
            Me.EsCabeceraDesglose.Value = Request.Form("EsCabeceraDesglose")
        Else
            Valor = Request("Valor")
            If (Request("EsCabecera") <> Nothing) Then
                Me.EsCabeceraDesglose.Value = Request("EsCabecera")
            End If
        End If

        Dim DescripcionCompleta As String = Nothing
        If Valor <> "" Then
            Dim arrPresupuestos() As String = Valor.Split("#")
            Dim arrPresup As String
            Dim arrPresupuesto() As String
            Dim iNivel As Integer
            Dim lIdPresup As Integer
            Dim dPorcent As Double
            Dim iAnyo As Integer
            Dim sPres1 As String
            Dim sPres2 As String
            Dim sPres3 As String
            Dim sPres4 As String

            Dim sDenUon As String
            Dim sDenPres As String

            Dim bBajaLog As Boolean

            Dim oPres1 As FSNServer.PresProyectosNivel1
            Dim oPres2 As FSNServer.PresContablesNivel1
            Dim oPres3 As FSNServer.PresConceptos3Nivel1
            Dim oPres4 As FSNServer.PresConceptos4Nivel1
            Dim oDS As DataSet

            Valor = ""

            Dim valorasociado As String
            Dim primerelemento As Boolean = True

            For Each arrPresup In arrPresupuestos
                arrPresupuesto = arrPresup.Split("_")

                sUon1 = Nothing
                sUon2 = Nothing
                sUon3 = Nothing
                sDenUon = Nothing
                sPres1 = Nothing
                sPres2 = Nothing
                sPres3 = Nothing
                sPres4 = Nothing
                sDenPres = Nothing
                iAnyo = Nothing
                DescripcionCompleta = Nothing

                iNivel = arrPresupuesto(0)
                lIdPresup = arrPresupuesto(1)
                dPorcent = Numero(arrPresupuesto(2), ".", ",")

                Dim idTipoCampoGS As Integer

                If Request("iTipo") Is Nothing Then
                    Select Case oCampo.IdTipoCampoGS
                        Case TiposDeDatos.TipoCampoGS.PRES1
                            iTipo = 1
                        Case TiposDeDatos.TipoCampoGS.Pres2
                            iTipo = 2
                        Case TiposDeDatos.TipoCampoGS.Pres3
                            iTipo = 3
                        Case Else
                            iTipo = 4
                    End Select
                    idTipoCampoGS = oCampo.IdTipoCampoGS
                Else
                    iTipo = Request("iTipo")
                End If

                'A partir del id del presupuesto y el nivel obtenemos los códigos de unidad organizativa y de presupuesto en el dataset DS
                Select Case iTipo
                    Case  1
                        oPres1 = FSNServer.Get_Object(GetType(FSNServer.PresProyectosNivel1))
                        Select Case iNivel
                            Case 1
                                oPres1.LoadData(lIdPresup)
                            Case 2
                                oPres1.LoadData(Nothing, lIdPresup)
                            Case 3
                                oPres1.LoadData(Nothing, Nothing, lIdPresup)
                            Case 4
                                oPres1.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                        End Select
                        oDS = oPres1.Data
                        iAnyo = oDS.Tables(0).Rows(0).Item("ANYO")
                        oPres1 = Nothing
                    Case  2
                        oPres2 = FSNServer.Get_Object(GetType(FSNServer.PresContablesNivel1))
                        Select Case iNivel
                            Case 1
                                oPres2.LoadData(lIdPresup)
                            Case 2
                                oPres2.LoadData(Nothing, lIdPresup)
                            Case 3
                                oPres2.LoadData(Nothing, Nothing, lIdPresup)
                            Case 4
                                oPres2.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                        End Select
                        oDS = oPres2.Data
                        iAnyo = oDS.Tables(0).Rows(0).Item("ANYO")
                        oPres2 = Nothing
                    Case 3
                        oPres3 = FSNServer.Get_Object(GetType(FSNServer.PresConceptos3Nivel1))
                        Select Case iNivel
                            Case 1
                                oPres3.LoadData(lIdPresup)
                            Case 2
                                oPres3.LoadData(Nothing, lIdPresup)
                            Case 3
                                oPres3.LoadData(Nothing, Nothing, lIdPresup)
                            Case 4
                                oPres3.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                        End Select
                        oDS = oPres3.Data
                        oPres3 = Nothing
                    Case Else
                        oPres4 = FSNServer.Get_Object(GetType(FSNServer.PresConceptos4Nivel1))
                        Select Case iNivel
                            Case 1
                                oPres4.LoadData(lIdPresup)
                            Case 2
                                oPres4.LoadData(Nothing, lIdPresup)
                            Case 3
                                oPres4.LoadData(Nothing, Nothing, lIdPresup)
                            Case 4
                                oPres4.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                        End Select
                        oDS = oPres4.Data
                        oPres4 = Nothing
                End Select

                If Not oDS Is Nothing Then
                    With oDS.Tables(0).Rows(0)
                        If (iTipo = 1) Or (iTipo = 2) Then
                            DescripcionCompleta = "(" & .Item("ANYO") & ") "
                        End If

                        If Not IsDBNull(.Item("UON1")) Then
                            sUon1 = .Item("UON1")
                            If Not IsDBNull(.Item("UON2")) Then
                                sUon2 = .Item("UON2")
                                If Not IsDBNull(.Item("UON3")) Then
                                    sUon3 = .Item("UON3")
                                End If
                            End If
                        End If
                        sDenPres = DBNullToSomething(.Item("DEN"))
                        sDenUon = DBNullToSomething(.Item("UONDEN"))
                        If Not IsDBNull(.Item("PRES4")) Then
                            sPres4 = .Item("PRES4")
                            sPres3 = .Item("PRES3")
                            sPres2 = .Item("PRES2")
                            sPres1 = .Item("PRES1")
                            DescripcionCompleta += sPres1 + "-" + sPres2 + "-" + sPres3 + "-" + sPres4
                        ElseIf Not IsDBNull(.Item("PRES3")) Then
                            sPres3 = .Item("PRES3")
                            sPres2 = .Item("PRES2")
                            sPres1 = .Item("PRES1")
                            DescripcionCompleta += sPres1 + "-" + sPres2 + "-" + sPres3
                        ElseIf Not IsDBNull(.Item("PRES2")) Then
                            sPres2 = .Item("PRES2")
                            sPres1 = .Item("PRES1")
                            DescripcionCompleta += sPres1 + "-" + sPres2
                        ElseIf Not IsDBNull(.Item("PRES1")) Then
                            sPres1 = .Item("PRES1")
                            DescripcionCompleta += sPres1
                        End If

                        bBajaLog = DBNullToSomething(.Item("BAJALOG"))

                        oDS = Nothing
                    End With
                    Dim bSinPermisos As Boolean
                    bSinPermisos = False

                    sScript += "arrDistribuciones[arrDistribuciones.length]= new Distribucion('" + JSText(sUon1) + "','" + JSText(sUon2) + "','" + JSText(sUon3) + "'," + JSNum(iAnyo) + ",'" + JSText(sPres1) + "','" + JSText(sPres2) + "','" + JSText(sPres3) + "','" + JSText(sPres4) + "'," + JSNum(dPorcent) + "," + JSNum(iNivel) + "," + JSNum(lIdPresup) + ",'" + JSText(sDenUon) + "','" + JSText(sDenPres) + "','','','" + JSText(bBajaLog) + "');"
                    If primerelemento Then
                        Valor += arrPresup
                        Me.Valor.Value = arrPresup
                        primerelemento = False
                    Else
                        Valor += "#" + arrPresup
                        Me.Valor.Value += "#" + arrPresup
                    End If

                    dtNewRow = oDGElement.NewRow
                    dtNewRow.Item("PORCENTAGE") = dPorcent * 100
                    DescripcionCompleta += "-" + sDenPres

                    dtNewRow.Item("DESCRIPCION") = DescripcionCompleta
                    valorasociado = arrPresupuesto(0) + "_" + arrPresupuesto(1) + "_" + arrPresupuesto(2)
                    dtNewRow.Item("VALORASOCIADO") = valorasociado
                    oDGElement.Rows.Add(dtNewRow)
                End If
            Next

            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "distribucionPresupuestos", sScript)

            If Valor <> Nothing Then
                Valor = Left(Valor, Valor.Length - 1)
            End If
        Else
            Me.wdgAsignaciones.Rows.Clear()
        End If

        If Not IsPostBack Then
            'Genero y cargo el GRID wdgAsignaciones
            LimpiarGrid(wdgAsignaciones)
            'Me.wdgAsignaciones.DataSource = DS
            Me.wdgAsignaciones.DataSource = oDGElement
            CrearColumnasfromTable(wdgAsignaciones, oDGElement)
            Me.wdgAsignaciones.DataBind()
            Session("DTPress") = oDGElement
            ConfigurarGridAsig(wdgAsignaciones)
            Me.wdgAsignaciones.Columns("DESCRIPCION").Header.Text = mcadenapresupuestos + Textos(25)

            'Genero y cargo el GRID wdgPresupuestosFavoritos
            LimpiarGrid(wdgPresupuestosFavoritos)
            Dim oPresupFavoritos As FSNServer.PresupuestosFavoritos
            oPresupFavoritos = FSNServer.Get_Object(GetType(FSNServer.PresupuestosFavoritos))
            oPresupFavoritos.LoadData(FSNUser.Cod, iTipo)
            Me.wdgPresupuestosFavoritos.DataSource = oPresupFavoritos.Data
            Session("DSFav") = oPresupFavoritos.Data
            Me.wdgPresupuestosFavoritos.DataMember = "Table"
            Me.wdgPresupuestosFavoritos.DataKeyFields = "FAVID"
            CrearColumnasfromDataset(wdgPresupuestosFavoritos)
            AnyadirColumnaU(wdgPresupuestosFavoritos, "ELIMINAR")
            Me.wdgPresupuestosFavoritos.DataBind()
            ConfigurarGridFav()
        Else
            'wdgAsignaciones
            LimpiarGrid(wdgAsignaciones)
            Me.wdgAsignaciones.DataSource = Session("DTPress")
            CrearColumnasfromTable(wdgAsignaciones, oDGElement)
            Me.wdgAsignaciones.DataBind()
            ConfigurarGridAsig(wdgAsignaciones)
            'wdgPresupuestosFavoritos
            LimpiarGrid(wdgPresupuestosFavoritos)
            Dim oPresupFavoritos As FSNServer.PresupuestosFavoritos
            oPresupFavoritos = FSNServer.Get_Object(GetType(FSNServer.PresupuestosFavoritos))
            oPresupFavoritos.LoadData(FSNUser.Cod, iTipo)
            Me.wdgPresupuestosFavoritos.DataSource = oPresupFavoritos.Data
            Session("DSFav") = oPresupFavoritos.Data
            Me.wdgPresupuestosFavoritos.DataMember = "Table"
            Me.wdgPresupuestosFavoritos.DataKeyFields = "FAVID"
            CrearColumnasfromDataset(wdgPresupuestosFavoritos)
            AnyadirColumnaU(wdgPresupuestosFavoritos, "ELIMINAR")
            Me.wdgPresupuestosFavoritos.DataBind()
            ConfigurarGridFav()
        End If

        If iTipo = 1 Or iTipo = 2 Then Me.wdgPresupuestosFavoritos.Columns.FromKey("ANYO").Hidden = True
        Me.wdgPresupuestosFavoritos.Columns("PRESUPDEN").Header.Text = "<img src=" & ConfigurationManager.AppSettings("ruta") & "images/centrodecosteFavoritos.jpg hspace=5 />" & mcadenapresupuestosplural & " " & Textos(31)

        If Not Page.IsPostBack Then
            If Acceso.gbUsarART4_UON AndAlso Acceso.gbOblAsignarPresArt AndAlso Request("Articulo") <> Nothing Then
                Me.OrgCompras.Value = Request("OrgCompras")
                oUnidades = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))
                oUnidades.CargarUnidadconArticulo(Request("Articulo"))

                Dim txtUO As String
                If oUnidades.Data.Tables(0).Rows.Count = 1 Then
                    If DBNullToSomething(oUnidades.Data.Tables(0).Rows(0).Item("UON1")) <> Nothing Then
                        txtUO = oUnidades.Data.Tables(0).Rows(0).Item("UON1")
                        If DBNullToSomething(oUnidades.Data.Tables(0).Rows(0).Item("UON2")) <> Nothing Then
                            txtUO += "-" + oUnidades.Data.Tables(0).Rows(0).Item("UON2")
                            If DBNullToSomething(oUnidades.Data.Tables(0).Rows(0).Item("UON3")) <> Nothing Then
                                txtUO += "-" + oUnidades.Data.Tables(0).Rows(0).Item("UON3")
                            End If
                        End If
                        txtUO += "-" + oUnidades.Data.Tables(0).Rows(0).Item("DENOMINACION")
                    End If
                    lblUorganizativaRestringida.Text = txtUO
                    lblUorganizativaRestringida.Visible = True
                    ugtxtunidadorganizativa.Visible = False

                ElseIf oUnidades.Data.Tables(0).Rows.Count > 1 Then
                    lblUorganizativaRestringida.Visible = False
                    ugtxtunidadorganizativa.Visible = True

                    Me.ugtxtunidadorganizativa.TextField = "DEN"
                    Me.ugtxtunidadorganizativa.ValueField = "COD"
                    Me.ugtxtunidadorganizativa.DataSource = From datos In oUnidades.Data.Tables(0) _
                                                             Select New With {.DEN = datos.Item("den"), .COD = datos.Item(0) & "@@" & datos.Item(1) & "@@" & datos.Item(2)}
                    Me.ugtxtunidadorganizativa.DataBind()

                    Me.ugtxtunidadorganizativa.SelectedItemIndex = 0

                End If

                If oUnidades.Data.Tables(0).Rows.Count > 0 Then
                    If (DBNullToSomething(oUnidades.Data.Tables(0).Rows(0).Item("UON1")) <> Nothing) Then
                        CargarMisPresupuestos(iTipo, anyo, oUnidades.Data.Tables(0).Rows(0).Item("UON1"), DBNullToSomething(oUnidades.Data.Tables(0).Rows(0).Item("UON2")), DBNullToSomething(oUnidades.Data.Tables(0).Rows(0).Item("UON3")), Idioma, "", "", True)
                    End If

                    Me.UON1.Value = DBNullToSomething(oUnidades.Data.Tables(0).Rows(0).Item("UON1"))
                    Me.UON2.Value = DBNullToSomething(oUnidades.Data.Tables(0).Rows(0).Item("UON2"))
                    Me.UON3.Value = DBNullToSomething(oUnidades.Data.Tables(0).Rows(0).Item("UON3"))
                    If (oUnidades.Data.Tables(0).Rows(0).Item("DENOMINACION") <> "") Then
                        Me.DENUON.Value = oUnidades.Data.Tables(0).Rows(0).Item("DENOMINACION")
                    End If
                End If
            Else
                oUnidades = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))

                Dim EsPedidoAbierto, RestricPresUORama, RestricPresUO, RestricPresPerfUO As Boolean
                If Request.QueryString("EsPedidoAbierto") IsNot Nothing Then
                    EsPedidoAbierto = True
                    Select Case iTipo
                        Case 1
                            RestricPresUORama = FSNUser.EPPermitirSeleccionarPresupuestosConcepto1UONsPorEncimaUsu
                            RestricPresUO = FSNUser.EPPermitirSeleccionarPresupuestosConcepto1UONUsu
                            RestricPresPerfUO = FSNUser.EPPermitirSeleccionarPresupuestosConcepto1UONsPerfil
                        Case 2
                            RestricPresUORama = FSNUser.EPPermitirSeleccionarPresupuestosConcepto2UONsPorEncimaUsu
                            RestricPresUO = FSNUser.EPPermitirSeleccionarPresupuestosConcepto2UONUsu
                            RestricPresPerfUO = FSNUser.EPPermitirSeleccionarPresupuestosConcepto2UONsPerfil
                        Case 3
                            RestricPresUORama = FSNUser.EPPermitirSeleccionarPresupuestosConcepto3UONsPorEncimaUsu
                            RestricPresUO = FSNUser.EPPermitirSeleccionarPresupuestosConcepto3UONUsu
                            RestricPresPerfUO = FSNUser.EPPermitirSeleccionarPresupuestosConcepto3UONsPerfil
                        Case Else
                            RestricPresUORama = FSNUser.EPPermitirSeleccionarPresupuestosConcepto4UONsPorEncimaUsu
                            RestricPresUO = FSNUser.EPPermitirSeleccionarPresupuestosConcepto4UONUsu
                            RestricPresPerfUO = FSNUser.EPPermitirSeleccionarPresupuestosConcepto4UONsPerfil
                    End Select
                Else
                    RestricPresUO = FSNUser.PMPresUO
                End If

                If Not RestricPresUO Then
                    'Se carga la combo
                    lblUorganizativaRestringida.Visible = False
                    ugtxtunidadorganizativa.Visible = True

                    oUnidades.CargarUnidadesConPresupuestos(iTipo, RestricPresUO, anyo, Idioma, FSNUser.CodPersona, FSNUser.UON1, FSNUser.UON2, FSNUser.UON3, EsPedidoAbierto, RestricPresUORama, RestricPresPerfUO)

                    Me.ugtxtunidadorganizativa.TextField = "DEN"
                    Me.ugtxtunidadorganizativa.ValueField = "COD"
                    Me.ugtxtunidadorganizativa.DataSource = From datos In oUnidades.Data.Tables(0) _
                                                             Select New With {.DEN = datos.Item("den"), .COD = datos.Item(0) & "@@" & datos.Item(1) & "@@" & datos.Item(2)}
                    Me.ugtxtunidadorganizativa.DataBind()

                    Dim tempuseruon1 As String = "#"
                    Dim tempuseruon2 As String = "#"
                    Dim tempuseruon3 As String = "#"

                    If FSNUser.UON1 <> Nothing Then tempuseruon1 = FSNUser.UON1
                    If FSNUser.UON2 <> Nothing Then tempuseruon2 = FSNUser.UON2
                    If FSNUser.UON3 <> Nothing Then tempuseruon3 = FSNUser.UON3

                    For Each oItem As Infragistics.Web.UI.ListControls.DropDownItem In Me.ugtxtunidadorganizativa.Items
                        Dim oCod As String() = Split(oItem.Value, "@@")
                        If oCod.Length = 3 Then
                            If (oCod(0) = tempuseruon1) And (oCod(1) = tempuseruon2) And (oCod(2) = tempuseruon3) Then
                                ugtxtunidadorganizativa.SelectedItemIndex = oItem.Index
                                Exit For
                            End If
                        End If
                    Next

                    If (ugtxtunidadorganizativa.SelectedItemIndex = -1 AndAlso Not EsPedidoAbierto) Then
                        'Nuevo para el tema de cargar las UO superiores en caso de que la UO del usuario no tenga presupuestos
                        tempuseruon1 = ""
                        tempuseruon2 = ""
                        tempuseruon3 = ""
                        Dim oRowUON As System.Data.DataRow
                        Dim bSeleccionado As Boolean = False
                        Dim iIndex As Integer = 0
                        If (FSNUser.UON3 <> Nothing) Then
                            For Each oRowUON In oUnidades.Data.Tables(0).Rows
                                If ("" & oRowUON.ItemArray(0) = FSNUser.UON1) And ("" & oRowUON.ItemArray(1) = FSNUser.UON2) And ("" & oRowUON.ItemArray(2) = FSNUser.UON3) Then
                                    tempuseruon1 = oRowUON.ItemArray(0)
                                    tempuseruon2 = oRowUON.ItemArray(1)
                                    tempuseruon3 = oRowUON.ItemArray(2)
                                    ugtxtunidadorganizativa.SelectedItemIndex = iIndex
                                    bSeleccionado = True
                                    Exit For
                                End If
                                iIndex = iIndex + 1
                            Next
                        End If

                        If bSeleccionado = False Then
                            iIndex = 0
                            If (FSNUser.UON2 <> Nothing) Then
                                For Each oRowUON In oUnidades.Data.Tables(0).Rows
                                    If ("" & oRowUON.ItemArray(0) = FSNUser.UON1) And ("" & oRowUON.ItemArray(1) = FSNUser.UON2) And ("" & oRowUON.ItemArray(2) = "#") Then
                                        tempuseruon1 = oRowUON.ItemArray(0)
                                        tempuseruon2 = oRowUON.ItemArray(1)
                                        tempuseruon3 = ""
                                        ugtxtunidadorganizativa.SelectedItemIndex = iIndex
                                        bSeleccionado = True
                                        Exit For
                                    End If
                                    iIndex = iIndex + 1
                                Next
                            End If
                        End If

                        If bSeleccionado = False Then
                            iIndex = 0
                            If (FSNUser.UON1 <> Nothing) Then
                                For Each oRowUON In oUnidades.Data.Tables(0).Rows
                                    If ("" & oRowUON.ItemArray(0) = FSNUser.UON1) And ("" & oRowUON.ItemArray(1) = "#") And ("" & oRowUON.ItemArray(2) = "#") Then
                                        tempuseruon1 = oRowUON.ItemArray(0)
                                        tempuseruon2 = ""
                                        tempuseruon3 = ""
                                        ugtxtunidadorganizativa.SelectedItemIndex = iIndex
                                        Exit For
                                    End If
                                    iIndex = iIndex + 1
                                Next
                            End If
                        End If

                        If (FSNUser.UON1 <> Nothing) Then
                            CargarMisPresupuestos(iTipo, anyo, tempuseruon1, tempuseruon2, tempuseruon3, Idioma, "", "")
                        End If
                        Me.UON1.Value = tempuseruon1
                        Me.UON2.Value = tempuseruon2
                        Me.UON3.Value = tempuseruon3

                    Else
                        'Nuevo para el tema de cargar las UO superiores en caso de que la UO del usuario no tenga presupuestos
                        'Carga de el arbol y de la grid si es necesaria
                        Me.UON1.Value = FSNUser.UON1
                        Me.UON2.Value = FSNUser.UON2
                        Me.UON3.Value = FSNUser.UON3
                        If (Me.UON1.Value <> Nothing) Then
                            Me.DENUON.Value = Me.UON1.Value + "-"
                        Else
                            Me.UON1.Value = "#"
                        End If
                        If (Me.UON2.Value <> Nothing) Then
                            Me.DENUON.Value += Me.UON2.Value + "-"
                        Else
                            Me.UON2.Value = "#"
                        End If
                        If (Me.UON3.Value <> Nothing) Then
                            Me.DENUON.Value += Me.UON3.Value + "-"
                        Else
                            Me.UON3.Value = "#"
                        End If
                        If oUnidades.Data.Tables(0).Rows.Count > 0 Then
                            For i = 0 To oUnidades.Data.Tables(0).Rows.Count - 1
                                If (Me.UON1.Value = DBNullToSomething(oUnidades.Data.Tables(0).Rows(i).Item("UON1"))) Then
                                    If (Me.UON2.Value = DBNullToSomething(oUnidades.Data.Tables(0).Rows(i).Item("UON2"))) Then
                                        If (Me.UON3.Value = DBNullToSomething(oUnidades.Data.Tables(0).Rows(i).Item("UON3"))) Then
                                            Me.DENUON.Value += oUnidades.Data.Tables(0).Rows(i).Item("DENOMINACION")
                                        End If
                                    End If
                                End If
                            Next
                        End If
                        'Vuelvo a dejarlo como estaba
                        Me.UON1.Value = FSNUser.UON1
                        Me.UON2.Value = FSNUser.UON2
                        Me.UON3.Value = FSNUser.UON3
                        If (FSNUser.UON1 <> Nothing) Then
                            CargarMisPresupuestos(iTipo, anyo, FSNUser.UON1, FSNUser.UON2, FSNUser.UON3, Idioma, "", "")
                        End If
                    End If
                Else
                    'Es una etiqueta
                    Me.USUARIORESTRINGIDO.Value = 1
                    lblUorganizativaRestringida.Visible = True
                    ugtxtunidadorganizativa.Visible = False

                    oUnidades.CargarUnidadesConPresupuestos(iTipo, RestricPresUO, anyo, Idioma, FSNUser.CodPersona, FSNUser.UON1, FSNUser.UON2, FSNUser.UON3, EsPedidoAbierto, RestricPresUORama, RestricPresPerfUO)
                    Dim txtUORestringida As String = ""
                    If (FSNUser.UON1 <> Nothing) Then
                        txtUORestringida += FSNUser.UON1
                        If (FSNUser.UON2 <> Nothing) Then
                            txtUORestringida += "-" + FSNUser.UON2
                            If (FSNUser.UON3 <> Nothing) Then
                                txtUORestringida += "-" + FSNUser.UON3
                            End If
                        End If

                        If oUnidades.Data.Tables(0).Rows.Count > 0 Then
                            txtUORestringida += "-" + oUnidades.Data.Tables(0).Rows(0).Item("DENOMINACION")
                        End If
                    End If

                    'If Not EsPedidoAbierto Then
                    'Tema para cargar presupuestos de unidades padre en caso de que no tenga presupuestos la unidad del 
                    'usuario restringido
                    Dim restringidouon1 As String = ""
                    Dim restringidouon2 As String = ""
                    Dim restringidouon3 As String = ""
                    Dim denominacionrestringido As String = ""
                    Dim oRow As System.Data.DataRow
                    Dim bSeleccionado As Boolean = False

                    If (FSNUser.UON3 <> Nothing) Then
                        For Each oRow In oUnidades.Data.Tables(1).Rows
                            If ("" & oRow.ItemArray(0) = FSNUser.UON1) And ("" & oRow.ItemArray(1) = FSNUser.UON2) And ("" & oRow.ItemArray(2) = FSNUser.UON3) Then
                                restringidouon1 = oRow.ItemArray(0)
                                restringidouon2 = oRow.ItemArray(1)
                                restringidouon3 = oRow.ItemArray(2)
                                denominacionrestringido = oRow.ItemArray(3)
                                bSeleccionado = True
                                Exit For
                            End If
                        Next
                    End If

                    If bSeleccionado = False Then
                        If (FSNUser.UON2 <> Nothing) Then
                            For Each oRow In oUnidades.Data.Tables(1).Rows
                                If ("" & oRow.ItemArray(0) = FSNUser.UON1) And ("" & oRow.ItemArray(1) = FSNUser.UON2) And ("" & oRow.ItemArray(2) = "#") Then
                                    restringidouon1 = oRow.ItemArray(0)
                                    restringidouon2 = oRow.ItemArray(1)
                                    restringidouon3 = ""
                                    denominacionrestringido = oRow.ItemArray(3)
                                    bSeleccionado = True
                                    Exit For
                                End If
                            Next
                        End If
                    End If

                    If bSeleccionado = False Then
                        If (FSNUser.UON1 <> Nothing) Then
                            For Each oRow In oUnidades.Data.Tables(1).Rows
                                If ("" & oRow.ItemArray(0) = FSNUser.UON1) And ("" & oRow.ItemArray(1) = "#") And ("" & oRow.ItemArray(2) = "#") Then
                                    restringidouon1 = oRow.ItemArray(0)
                                    restringidouon2 = ""
                                    restringidouon3 = ""
                                    denominacionrestringido = oRow.ItemArray(3)
                                    Exit For
                                End If
                            Next
                        End If
                    End If

                    lblUorganizativaRestringida.Text = txtUORestringida
                    If (FSNUser.UON1 <> Nothing) Then
                        CargarMisPresupuestos(iTipo, anyo, restringidouon1, restringidouon2, restringidouon3, Idioma, "", "")
                    End If
                    Me.UON1.Value = restringidouon1
                    Me.UON2.Value = restringidouon2
                    Me.UON3.Value = restringidouon3
                    If (denominacionrestringido <> "") Then
                        Me.DENUON.Value = denominacionrestringido
                    End If
                End If
            End If
        Else
            Dim codigo As String
            codigo = Request.Form("txtCodigo")
            Dim currLoc1 As Integer

            Dim StringLength1 As Integer
            Dim tmpChar1 As String
            'Reemplazo los * por % para la busqueda por denominacion
            StringLength1 = Len(codigo)
            For currLoc1 = 1 To StringLength1
                tmpChar1 = Mid(codigo, currLoc1, 1)
                If InStr("*", tmpChar1) Then
                    Mid(codigo, currLoc1, 1) = "%"
                End If
            Next

            Dim denominacion As String
            denominacion = Request.Form("txtDenominacion")

            Dim currLoc As Integer
            Dim StringLength As Integer
            Dim tmpChar As String
            'Reemplazo los * por % para la busqueda por denominacion
            StringLength = Len(denominacion)
            For currLoc = 1 To StringLength
                tmpChar = Mid(denominacion, currLoc, 1)
                If InStr("*", tmpChar) Then
                    Mid(denominacion, currLoc, 1) = "%"
                End If
            Next

            If FSNUser.PMPresUO = True Then
                CargarMisPresupuestos(iTipo, anyo, Me.UON1.Value, Me.UON2.Value, Me.UON3.Value, Idioma, codigo, denominacion)
            Else
                CargarMisPresupuestos(iTipo, anyo, Me.UON1.Value, Me.UON2.Value, Me.UON3.Value, Idioma, codigo, denominacion)
            End If
        End If

        Dim bAdmiteMultiplesPresupuestos As Boolean = True
        If Request("OrgCompras") <> "" Then
            Dim oOrganizacionCompras As FSNServer.OrganizacionCompras
            oOrganizacionCompras = FSNServer.Get_Object(GetType(FSNServer.OrganizacionCompras))
            oOrganizacionCompras.Cod = Request("OrgCompras")
            bAdmiteMultiplesPresupuestos = oOrganizacionCompras.AdmiteMultiplesPresupuestos()
        End If

        If Not bAdmiteMultiplesPresupuestos Then
            Me.pnlBotonesAniadirQuitar.Visible = False
            Me.txtPorcent.Style.Add("display", "none")
            Me.lblPorcen1.Visible = False
            wdtPresupuestos2.ClientEvents.NodeClick = "wdtPresupuestos2_AfterNodeSelectionChange"
            Me.tabladetallePresAsignado.Style.Add("display", "''")
            Me.layergrid.Style.Add("display", "none")
            Me.AdmiteMultiplesPresupuestos.Value = "0"

            If Me.EsCabeceraDesglose.Value = "0" And DescripcionCompleta <> "" Then
                Me.DetDenPresAsignado.Text = DescripcionCompleta
                Me.DetUonPresAsignado.Text = cadenaunidadorganizativa & ": " & Me.DENUON.Value
            End If
        End If
    End Sub

    ''' <summary>
    ''' Devuelve los presupuestos de las unidades organizativas con los filtros pasados.
    ''' </summary>
    ''' <param name="iTipo">Tipo de presupuesto</param>
    ''' <param name="iAnyo">Año</param>
    ''' <param name="sUon1">Cod de la unidad organizativa de nivel 1</param>
    ''' <param name="sUon2">Cod de la unidad organizativa de nivel 2</param>
    ''' <param name="sUon3">Cod de la unidad organizativa de nivel 3</param>
    ''' <param name="sIdi">Idioma</param>
    ''' <param name="Codigo">Código</param>
    ''' <param name="Denominacion">Denominación</param>
    ''' <param name="bSubiendoNivel">True/False</param>
    ''' <remarks>Llamada desde: Page_Load(); Tiempo máximo: 0,3 sg.</remarks>
    Private Sub CargarMisPresupuestos(ByVal iTipo As Integer, ByVal iAnyo As Integer, ByVal sUon1 As String, ByVal sUon2 As String, ByVal sUon3 As String, ByVal sIdi As String, ByVal Codigo As String, ByVal Denominacion As String, Optional ByVal bSubiendoNivel As Boolean = False)
        Dim oUnidades As FSNServer.UnidadesOrg
        Dim bEsUONbase As Boolean = False
        Dim iEsUONVacia As Integer = 0

        oUnidades = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))

        If sUon1 = "null" And sUon2 = "null" And sUon3 = "null" Then
            iEsUONVacia = 1
        ElseIf sUon1 = "" And sUon2 = "" And sUon3 = "" Then
            iEsUONVacia = 1
        End If

        If sUon1 = "#" Or sUon1 = "null" Then sUon1 = ""
        If sUon2 = "#" Or sUon2 = "null" Then sUon2 = ""
        If sUon3 = "#" Or sUon3 = "null" Then sUon3 = ""

        oUnidades.CargarPresupuestos(iTipo, iEsUONVacia, iAnyo, sUon1, sUon2, sUon3, sIdi, Codigo, Denominacion)

        If bSubiendoNivel AndAlso oUnidades.Data.Tables(0).DefaultView.Count < 2 Then
            If sUon3 <> "" Then
                sUon3 = ""
            ElseIf sUon2 <> "" Then
                sUon2 = ""
            End If

            oUnidades.CargarPresupuestos(iTipo, iEsUONVacia, iAnyo, sUon1, sUon2, sUon3, sIdi, Codigo, Denominacion)
        End If

        Dim iTablas As Integer
        For iTablas = 0 To oUnidades.Data.Tables.Count() - 1
            If (oUnidades.Data.Tables(iTablas).Rows.Count > 0) Then
                bMostrarArbolyGrid = True
            End If
        Next

        If (sUon1 = "") And (sUon2 = "") And (sUon3 = "") Then
            Dim ArrayUON As String()
            ReDim ArrayUON(0)
            Dim ArrayDENUON As String()
            ReDim ArrayDENUON(0)
            Dim UONBase As Boolean
            UONBase = False
            Dim iElemento As Integer
            Dim iElementoArray As Integer
            Dim sTemp As String = ""
            Dim sTempUON1 As String = ""
            Dim sTempUON2 As String = ""
            Dim sTempUON3 As String = ""
            Dim sTempDEN As String = ""
            Dim bEstaenelArray As Boolean = False

            For iTablas = 1 To oUnidades.Data.Tables.Count() - 1
                If (oUnidades.Data.Tables(iTablas).Rows.Count > 0) Then
                    For iElemento = 0 To oUnidades.Data.Tables(iTablas).Rows.Count - 1
                        sTemp = ""
                        sTempDEN = ""
                        sTempUON1 = "#"
                        sTempUON2 = "#"
                        sTempUON3 = "#"
                        If (DBNullToSomething(oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("UON1")) <> Nothing) Then
                            sTemp = oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("UON1")
                            sTempUON1 = oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("UON1")
                            If (DBNullToSomething(oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("UON2")) <> Nothing) Then
                                sTemp += "-" + oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("UON2")
                                sTempUON2 = oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("UON2")
                                If (DBNullToSomething(oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("UON3")) <> "") Then
                                    sTemp += "-" + oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("UON3")
                                    sTempUON3 = oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("UON3")
                                End If
                            End If
                        End If

                        If (DBNullToSomething(oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("DEN_UON")) <> Nothing) Then
                            If (sTemp = "") Then
                                sTempDEN += oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("DEN_UON")
                            Else
                                sTempDEN += "-" + oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("DEN_UON")
                            End If
                        End If

                        For iElementoArray = 0 To ArrayUON.Length - 1
                            If (ArrayUON.GetValue(iElementoArray) = sTemp) Then
                                bEstaenelArray = True
                                Exit For
                            End If
                        Next

                        If (bEstaenelArray = False Or (sTemp = "" And UONBase = False)) Then
                            If (sTemp = "") Then
                                UONBase = True
                            End If

                            ArrayUON.SetValue(sTemp, ArrayUON.Length - 1)
                            ReDim Preserve ArrayUON(ArrayUON.Length)

                            ArrayDENUON.SetValue(sTempDEN, ArrayDENUON.Length - 1)

                            ReDim Preserve ArrayDENUON(ArrayDENUON.Length)

                        End If

                        bEstaenelArray = False
                        sTemp = ""
                    Next

                End If
            Next

            Dim sText As String = ""
            If Not (ArrayUON.Length > 2 And iEsUONVacia = 1) Then 'Comparo contra 2 pq tengo reservada una linea de mas
                Me.txtPorcent.Visible = True
                Me.wdgAsignaciones.Visible = True
                Me.lblSeleccionaArbol.Visible = True
                Me.wdtPresupuestos2.Visible = True
                Me.lblAsignado.Visible = True
                Me.txtAsignadoPorcent.Visible = True
                Me.lblResultado.Visible = True
                Me.lblPendiente.Visible = True
                Me.txtPendientePorcent.Visible = True
                Me.Label1.Visible = True
                Me.Label3.Visible = True
                Me.lblPorcen1.Visible = True
                Me.lblPorcen2.Visible = True

                Me.wdtPresupuestos2.DataSource = oUnidades.Data
                Me.wdtPresupuestos2.DataBind()

                For Each node As Infragistics.Web.UI.NavigationControls.DataTreeNode In wdtPresupuestos2.AllNodes
                    node.Expanded = True
                Next

                Me.DENUON.Value = sTempDEN
                If (sTempUON1 = "#") Then
                    Me.UON1.Value = ""
                Else
                    Me.UON1.Value = sTempUON1
                End If
                If (sTempUON1 = "#") Then
                    Me.UON2.Value = ""
                Else
                    Me.UON2.Value = sTempUON2
                End If
                If (sTempUON1 = "#") Then
                    Me.UON3.Value = ""
                Else
                    Me.UON3.Value = sTempUON3
                End If

                If (ArrayUON.Length <= 2) Then 'Cuando la UO no es la base la longitud es 2 y cuando es la base la longitud es 1
                    'hay una unica UO y la vamos a poner seleccionada en el combo
                    For Each oItem As Infragistics.Web.UI.ListControls.DropDownItem In Me.ugtxtunidadorganizativa.Items
                        Dim oCod As String() = Split(oItem.Value, "@@")
                        If oCod.Length = 3 Then
                            If (oCod(0) = sTempUON1) And (oCod(1) = sTempUON2) And (oCod(2) = sTempUON3) Then
                                Dim sScriptActualizarIndexCombo As String
                                sScriptActualizarIndexCombo = " CambiarValorCombo ( " + oItem.Index.ToString() + "); "
                                sScriptActualizarIndexCombo = String.Format(IncludeScriptKeyFormat, "javascript", sScriptActualizarIndexCombo)
                                Page.ClientScript.RegisterStartupScript(Me.GetType(), "scriptactualizarindexcombo", sScriptActualizarIndexCombo)
                                Exit For
                            End If
                        End If
                    Next
                End If
            End If
        Else
            Dim Valor As String 'Campo importante
            If (Request.Form("Tipo") <> Nothing) Then
                Valor = Request.Form("Valor")
            Else
                Valor = Request("Valor")
            End If

            If (bMostrarArbolyGrid) Then
                Me.txtPorcent.Visible = True
                Me.wdgAsignaciones.Visible = True
                Me.lblSeleccionaArbol.Visible = True
                Me.wdtPresupuestos2.Visible = True

                Me.lblAsignado.Visible = True
                Me.txtAsignadoPorcent.Visible = True
                Me.lblResultado.Visible = True
                Me.lblPendiente.Visible = True
                Me.txtPendientePorcent.Visible = True
                Me.Label1.Visible = True
                Me.Label3.Visible = True
                Me.lblPorcen1.Visible = True
                Me.lblPorcen2.Visible = True

                Me.wdtPresupuestos2.DataSource = oUnidades.Data

                Me.wdtPresupuestos2.DataBind()

                For Each node As Infragistics.Web.UI.NavigationControls.DataTreeNode In wdtPresupuestos2.AllNodes
                    If node.Nodes.Count > 0 Then
                        node.Expanded = True
                    End If
                Next
            Else
                'No hay que mostrar elementos en el arbol
                If (Valor = "") Then
                    Me.txtPorcent.Visible = False
                    Me.wdgAsignaciones.Visible = False
                    Me.lblSeleccionaArbol.Visible = False
                    Me.wdtPresupuestos2.Visible = False

                    Me.lblAsignado.Visible = False
                    Me.txtAsignadoPorcent.Visible = False
                    Me.lblResultado.Visible = False
                    Me.lblPendiente.Visible = False
                    Me.txtPendientePorcent.Visible = False
                    Me.Label1.Visible = False
                    Me.Label3.Visible = False
                    Me.lblPorcen1.Visible = False
                    Me.lblPorcen2.Visible = False
                Else
                    Me.txtPorcent.Visible = True
                    Me.wdgAsignaciones.Visible = True
                    Me.lblSeleccionaArbol.Visible = True
                    Me.wdtPresupuestos2.Visible = True
                    Me.lblAsignado.Visible = True
                    Me.txtAsignadoPorcent.Visible = True
                    Me.lblResultado.Visible = True
                    Me.lblPendiente.Visible = True
                    Me.txtPendientePorcent.Visible = True
                    Me.Label1.Visible = True
                    Me.Label3.Visible = True
                    Me.lblPorcen1.Visible = True
                    Me.lblPorcen2.Visible = True
                End If

                Dim sScript As String
                sScript = "document.getElementById('Botones').style.display='inline';MensajeSinElementos()"
                sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "cargarporcentage", sScript)
            End If
        End If

        'Si sólo existe un presupuesto en el árbol lo seleccionamos
        Dim oNode As Infragistics.Web.UI.NavigationControls.DataTreeNode
        If wdtPresupuestos2.Nodes.Count = 0 OrElse wdtPresupuestos2.Nodes(0) Is Nothing Then
            Exit Sub
        Else
            oNode = Me.wdtPresupuestos2.Nodes(0) ' Nodo raíz
        End If

        If oNode.Nodes.Count = 1 Then 'si sólo hay una rama de nivel 1
            If oNode.Nodes.Count = 0 Then 'Si no tiene ramas de nivel 2 selecciona la rama de nivel 1
                wdtPresupuestos2.SelectedNodes.Add(oNode.Nodes(0))
            ElseIf oNode.Nodes(0).Nodes.Count = 1 Then 'tiene una rama de nivel 2
                If oNode.Nodes(0).Nodes(0).Nodes.Count = 0 Then ' selecciona la rama de nivel 2   
                    wdtPresupuestos2.SelectedNodes.Add(oNode.Nodes(0).Nodes(0))
                ElseIf oNode.Nodes(0).Nodes(0).Nodes.Count = 1 Then ' tiene una rama de nivel 3
                    If oNode.Nodes(0).Nodes(0).Nodes(0).Nodes.Count = 0 Then ' selecciona la rama de nivel 3
                        wdtPresupuestos2.SelectedNodes.Add(oNode.Nodes(0).Nodes(0).Nodes(0))
                    ElseIf oNode.Nodes(0).Nodes(0).Nodes(0).Nodes.Count = 1 Then ' tiene una rama de nivel 4
                        wdtPresupuestos2.SelectedNodes.Add(oNode.Nodes(0).Nodes(0).Nodes(0).Nodes(0)) 'Seleccina la rama de nivel 4
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub LimpiarGrid(ByVal grid As Infragistics.Web.UI.GridControls.WebDataGrid)
        grid.Rows.Clear()
        grid.Columns.Clear()
    End Sub

    Private Sub CrearColumnasfromTable(ByVal grid As Infragistics.Web.UI.GridControls.WebDataGrid, ByVal datasource As DataTable)
        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
        Dim nombre As String

        For i As Integer = 0 To datasource.Columns.Count - 1
            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            nombre = datasource.Columns(i).ToString
            With campoGrid
                .Key = nombre
                .DataFieldName = nombre
                .Header.Text = nombre
            End With
            grid.Columns.Add(campoGrid)
        Next
    End Sub

    Private Sub CrearColumnasfromDataset(ByVal grid As Infragistics.Web.UI.GridControls.WebDataGrid)
        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
        Dim nombre As String

        For i As Integer = 0 To wdgPresupuestosFavoritos.DataSource.Tables(0).Columns.Count - 1
            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            nombre = wdgPresupuestosFavoritos.DataSource.Tables(0).Columns(i).ToString
            With campoGrid
                .Key = nombre
                .DataFieldName = nombre
                .Header.Text = nombre
            End With
            grid.Columns.Add(campoGrid)
        Next
    End Sub

    Private Sub AnyadirColumnaU(ByVal grid As Infragistics.Web.UI.GridControls.WebDataGrid, ByVal fieldname As String)
        Dim unboundField As New Infragistics.Web.UI.GridControls.UnboundField(True)

        unboundField.Key = fieldname
        grid.Columns.Add(unboundField)
    End Sub

    Private Sub ConfigurarGridAsig(ByVal grid As Infragistics.Web.UI.GridControls.WebDataGrid)

        grid.Columns(0).Width = Unit.Percentage(10)
        grid.Columns(1).Width = Unit.Percentage(90)
        grid.Columns(2).Hidden = True
        grid.Columns("PORCENTAGE").Header.Text = "%"
    End Sub

    Private Sub ConfigurarGridFav()
        Me.wdgAsignaciones.Columns("PORCENTAGE").Header.Text = "%"
        Me.wdgPresupuestosFavoritos.Columns.FromKey("FAVID").Hidden = True
        Me.wdgPresupuestosFavoritos.Columns.FromKey("NIVEL").Hidden = True
        Me.wdgPresupuestosFavoritos.Columns.FromKey("PRESID").Hidden = True
        Me.wdgPresupuestosFavoritos.Columns.FromKey("PRES1").Hidden = True
        Me.wdgPresupuestosFavoritos.Columns.FromKey("PRES2").Hidden = True
        Me.wdgPresupuestosFavoritos.Columns.FromKey("PRES3").Hidden = True
        Me.wdgPresupuestosFavoritos.Columns.FromKey("PRES4").Hidden = True
        Me.wdgPresupuestosFavoritos.Columns.FromKey("UON1").Hidden = True
        Me.wdgPresupuestosFavoritos.Columns.FromKey("UON2").Hidden = True
        Me.wdgPresupuestosFavoritos.Columns.FromKey("UON3").Hidden = True
        Me.wdgPresupuestosFavoritos.Columns.FromKey("DEN").Hidden = True
        Me.wdgPresupuestosFavoritos.Columns.FromKey("UONDEN").Hidden = True
        Me.wdgPresupuestosFavoritos.Columns("PRESUPDEN").Width = Unit.Percentage(80)
        Me.wdgPresupuestosFavoritos.Columns("ELIMINAR").Width = Unit.Percentage(20)
    End Sub

    ''' <summary>
    ''' Pone en la celda eliminar, la imagen de suprimir por cada fila.
    ''' </summary>
    ''' <remarks>LLamada desde: al cargarse las filas de la grid; Tiempo máx: 0,1 sg.</remarks>
    Private Sub wdgPresupuestosFavoritos_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles wdgPresupuestosFavoritos.InitializeRow

        e.Row.Items.FindItemByKey("ELIMINAR").Text = "<img src='" & ConfigurationManager.AppSettings("ruta") & "/images/eliminarNegro.gif' style='text-align:center;'/>"

    End Sub
    ''' <summary>
    ''' evento que salta cuando se añade una fila al grid de asignaciones
    ''' </summary>
    ''' <param name="sender">grid de asignaciones</param>
    ''' <param name="e">valores de la nueva fila</param>
    ''' <remarks></remarks>
    Private Sub wdgAsignaciones_RowAdding(sender As Object, e As Infragistics.Web.UI.GridControls.RowAddingEventArgs) Handles wdgAsignaciones.RowAdding
        oDGElement = Session("DTPress")

        dtNewRow = oDGElement.Rows.Find(e.Values.Item("VALORASOCIADO"))

        If (dtNewRow Is Nothing) Then
            dtNewRow = oDGElement.NewRow
            dtNewRow.Item("PORCENTAGE") = e.Values.Item("PORCENTAGE")
            dtNewRow.Item("DESCRIPCION") = e.Values.Item("DESCRIPCION")
            dtNewRow.Item("VALORASOCIADO") = e.Values.Item("VALORASOCIADO")
            oDGElement.Rows.Add(dtNewRow)
        End If

        Session("DTPress") = oDGElement
        LimpiarGrid(wdgAsignaciones)
        Me.wdgAsignaciones.DataSource = oDGElement
        CrearColumnasfromTable(wdgAsignaciones, oDGElement)
        Me.wdgAsignaciones.DataBind()
        ConfigurarGridAsig(wdgAsignaciones)
    End Sub

    ''' <summary>
    ''' evento que salta cuando se elimina una fila al grid de asignaciones
    ''' </summary>
    ''' <param name="sender">grid de asignaciones</param>
    ''' <param name="e">>valores de la fila a eliminar</param>
    ''' <remarks></remarks>
    Private Sub wdgAsignaciones_RowsDeleting(sender As Object, e As Infragistics.Web.UI.GridControls.RowDeletingEventArgs) Handles wdgAsignaciones.RowsDeleting
        oDGElement = Session("DTPress")
        If Not oDGElement.Rows.Find(e.Row.DataKey) Is Nothing Then
            oDGElement.Rows.Remove(oDGElement.Rows.Find(e.Row.DataKey))
        End If
        Session("DTPress") = oDGElement
        LimpiarGrid(wdgAsignaciones)
        Me.wdgAsignaciones.DataSource = oDGElement
        CrearColumnasfromTable(wdgAsignaciones, oDGElement)
        Me.wdgAsignaciones.DataBind()
        ConfigurarGridAsig(wdgAsignaciones)
    End Sub

    ''' <summary>
    ''' evento que salta cuando se actualiza alguna celda del grid de asignaciones
    ''' </summary>
    ''' <param name="sender">grid de asignaciones</param>
    ''' <param name="e">valores de la fila</param>
    ''' <remarks></remarks>
    Private Sub wdgAsignaciones_RowUpdating(sender As Object, e As Infragistics.Web.UI.GridControls.RowUpdatingEventArgs) Handles wdgAsignaciones.RowUpdating
        oDGElement = Session("DTPress")
        dtNewRow = oDGElement.Rows.Find(e.Row.DataKey)
        dtNewRow.Item("PORCENTAGE") = e.Values.Item("PORCENTAGE")
        dtNewRow.Item("DESCRIPCION") = e.Values.Item("DESCRIPCION")
        dtNewRow.Item("VALORASOCIADO") = e.Values.Item("VALORASOCIADO")
        Session("DTPress") = oDGElement
        LimpiarGrid(wdgAsignaciones)
        Me.wdgAsignaciones.DataSource = oDGElement
        CrearColumnasfromTable(wdgAsignaciones, oDGElement)
        Me.wdgAsignaciones.DataBind()
        ConfigurarGridAsig(wdgAsignaciones)
    End Sub

    Private Sub wdgPresupuestosFavoritos_RowsDeleting(sender As Object, e As Infragistics.Web.UI.GridControls.RowDeletingEventArgs) Handles wdgPresupuestosFavoritos.RowsDeleting
        dsTable = Session("DSFav")
        Dim pKey(0) As DataColumn
        pKey(0) = dsTable.Tables(0).Columns("FAVID")
        dsTable.Tables(0).PrimaryKey = pKey
        Dim clave As String = e.Row.DataKey(0).ToString
        If Not dsTable.Tables(0).Rows.Find(e.Row.DataKey) Is Nothing Then
            dsTable.Tables(0).Rows.Remove(dsTable.Tables(0).Rows.Find(e.Row.DataKey))
            Dim oPresupFavoritos As FSNServer.PresupuestosFavoritos
            oPresupFavoritos = FSNServer.Get_Object(GetType(FSNServer.PresupuestosFavoritos))
            oPresupFavoritos.Eliminar(FSNUser.Cod, TIPOPRESUPUESTO.Value, Long.Parse(clave))
        End If
        Session("DSFav") = dsTable
        LimpiarGrid(wdgPresupuestosFavoritos)
        Me.wdgPresupuestosFavoritos.DataSource = dsTable
        Me.wdgPresupuestosFavoritos.DataMember = "Table"
        Me.wdgPresupuestosFavoritos.DataKeyFields = "FAVID"
        CrearColumnasfromDataset(wdgPresupuestosFavoritos)
        AnyadirColumnaU(wdgPresupuestosFavoritos, "ELIMINAR")
        Me.wdgPresupuestosFavoritos.DataBind()
        ConfigurarGridFav()

        Dim oUnidadesCadenaPresupuesto As FSNServer.UnidadesOrg 'La utilizo para obtener la cadena de BD del tipo de presupuesto seleccionado
        Dim mcadenapresupuestos As String 'cadena que guarda la descripcion del tipo de presupuesto
        Dim mcadenapresupuestosplural As String
        'Como ya se el tipo del presupuesto voy a obtener la cadena asociada a dicho tipo de presupuesto
        oUnidadesCadenaPresupuesto = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))
        oUnidadesCadenaPresupuesto.CargarCadenaPresupuesto(iTipo, Idioma)
        mcadenapresupuestos = oUnidadesCadenaPresupuesto.Data.Tables(0).Rows(0).Item(0)
        mcadenapresupuestosplural = oUnidadesCadenaPresupuesto.Data.Tables(0).Rows(0).Item(1)
        If iTipo = 1 Or iTipo = 2 Then Me.wdgPresupuestosFavoritos.Columns.FromKey("ANYO").Hidden = True
        Me.wdgPresupuestosFavoritos.Columns("PRESUPDEN").Header.Text = "<img src=" & ConfigurationManager.AppSettings("ruta") & "images/centrodecosteFavoritos.jpg hspace=5 />" & mcadenapresupuestosplural & " " & Textos(31)
    End Sub


    Private Sub wdtPresupuestos2_NodeBound(sender As Object, e As Infragistics.Web.UI.NavigationControls.DataTreeNodeEventArgs) Handles wdtPresupuestos2.NodeBound
        Select Case e.Node.Level
            Case 0
                e.Node.ImageUrl = ConfigurationManager.AppSettings("ruta") & "images/RaizPresCon.gif"
            Case Else
                e.Node.ImageUrl = ConfigurationManager.AppSettings("ruta") & "images/SINEWAVE.gif"
        End Select
    End Sub
End Class
