﻿Public Partial Class ComprobarEnProcesoQA
    Inherits FSNPage

    ''' <summary>
    ''' Carga de pantalla. Realmente lo q hace es q por ajax sincrono se desea saber si se puede o no 
    ''' grabar lo q esta editando el usuario en DetalleXXX.
    ''' </summary>
    ''' <param name="sender">pantalla</param>
    ''' <param name="e">parametro de sistema</param>     
    ''' <remarks>Llamada desde: DetalleNoConformidad DetalleCertificado DetalleContrato DetalleSolicitud; Tiempo máximo: 0,1</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim oInstancia As FSNServer.Instancia

        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        oInstancia.ID = Request.QueryString("Instancia")

        Response.Clear()
        Response.ContentType = "text/plain"
        Response.Write(oInstancia.ComprobarEnProceso())
        Response.End()

    End Sub

End Class