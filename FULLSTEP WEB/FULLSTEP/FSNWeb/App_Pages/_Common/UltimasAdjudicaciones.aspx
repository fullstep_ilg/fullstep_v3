﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UltimasAdjudicaciones.aspx.vb"
    Inherits="Fullstep.FSNWeb.UltimasAdjudicaciones" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>" lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        //''' <summary>
        //''' Se devuelve la adjudicacion seleccionada
        //''' </summary>
        //''' <remarks>Llamada desde: Al hacer click sobre la grid ; Tiempo máximo: 0,2</remarks>
        function wdgUltimasAdjudicaciones_RowSelectionChanged(sender, e) {
            oGrid = $find("wdgUltimasAdjudicaciones");
            var oRow = e.getSelectedRows().getItem(0);

            oCell = oRow.get_cellByColumnKey("PREC")
            lPrecio = oCell.get_value()

            oCell = oRow.get_cellByColumnKey("CODPROVE")
            sCODPROVUltADJ = oCell.get_value()
            oCell = oRow.get_cellByColumnKey("DESCR")
            sDENPROVUltADJ = oCell.get_value()

            oCell = oRow.get_cellByColumnKey("UNI")	//Codigo de la unidad	
            sCodUNI = oCell.get_value()
            oCell = oRow.get_cellByColumnKey("UNIDADES")	//Denominación de la unidad	
            sDenUNI = oCell.get_value()
            oCell = oRow.get_cellByColumnKey("NUMDEC")	//Nº de decimales de la unidad	
            sNumDecUNI = oCell.get_value()
            oCell = oRow.get_cellByColumnKey("MONEDA")	//Moneda	
            sMoneda = oCell.get_value()
            oCell = oRow.get_cellByColumnKey("MONDEN")	//Moneda	
            sDenMoneda = oCell.get_value()

            sFPago = "";
            sDenFPago = "";
            if ((document.getElementById("IDFPago").value != "") || (document.getElementById("IDFPagoHidden").value != "")) {
                oCell = oRow.get_cellByColumnKey("PAG");	//Forma pago
                sFPago = oCell.get_value();

                oCell = oRow.get_cellByColumnKey("DENPAG");	//Forma pago
                sDenFPago = oCell.get_value();
            };

            p = window.opener
            p.PonerUltADJ(lPrecio, sCODPROVUltADJ, sDENPROVUltADJ, sCodUNI, sDenUNI, sNumDecUNI, sMoneda, sDenMoneda, sFPago, sDenFPago)

            window.close();
        };
    </script>
</head>
<body style="background-color: #FFFFFF; margin-top: 0px; margin-left: 5px; margin-right: 0px">
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScUltAdjs" runat="server"></asp:ScriptManager>
    <input id="IDCONTROL" type="hidden" name="IDCONTROL" runat="server" />
    <input id="IDFPago" type="hidden" name="IDFPago" runat="server" />
    <input id="IDFPagoHidden" type="hidden" name="IDFPagoHidden" runat="server" />
    <p><asp:Label ID="lbltitulo" CssClass="Etiqueta" runat="server"></asp:Label></p>
    <ig:WebDataGrid ID="wdgUltimasAdjudicaciones" runat="server" Height="250px" SkinID="ConfiguracionFiltros"
        Width="100%" AutoGenerateBands="false" AutoGenerateColumns="false" EnableAjax="true"
        EnableAjaxViewState="false" EnableDataViewState="true">
        <Behaviors>
            <ig:Selection RowSelectType="Single" Enabled="True" CellClickAction="Row">
                <SelectionClientEvents RowSelectionChanged="wdgUltimasAdjudicaciones_RowSelectionChanged" />
            </ig:Selection>
            <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true" AnimationEnabled="true"></ig:Filtering>
            <ig:RowSelectors Enabled="false" RowNumbering="false"></ig:RowSelectors>
            <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
        </Behaviors>
    </ig:WebDataGrid>
    </form>
</body>
</html>