<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="downloadvisible.aspx.vb"
    Inherits="Fullstep.FSNWeb.downloadvisible" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <asp:Panel ID="pnlDescarga" runat="server">
        <asp:Label ID="lblTitulo" runat="server" Height="24px" Width="100%" CssClass="subtitulo">DOWNLOADING FILE...</asp:Label>
        <table id="Table1" cellspacing="1" cellpadding="1" width="100%">
            <tr>
                <td style="font-size: 1px; height: 1px" class="linea1" colspan="2" height="1">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 150px">
                    <asp:Label ID="lblFichero" runat="server" CssClass="parrafo">File name:</asp:Label>
                </td>
                <td>
                    <asp:Label ID="txtFichero" runat="server" CssClass="parrafo">NombreDelFichero.EXT</asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblSize" runat="server" CssClass="parrafo">File size:</asp:Label>
                </td>
                <td>
                    <asp:Label ID="txtSize" runat="server" CssClass="parrafo">1.000 Kb.</asp:Label>
                </td>
            </tr>
            <tr>
                <td style="font-size: 1px; height: 1px" class="linea2" colspan="2" height="1">
                    &nbsp;
                </td>
            </tr>
        </table>
        <br />
        <asp:Label ID="lblInstruc1" runat="server" CssClass="parrafo">Download should begin in a few seconds. If the download does not start automatically, or you have any problems, click on the following link to re-start download:</asp:Label><asp:HyperLink
            ID="lnkDownload1" runat="server" Target="_blank">HyperLink</asp:HyperLink><br>
        <br />
        <asp:Label ID="lblInstruc2" runat="server" CssClass="parrafo">If you wish to save the file to disk, instead of opening the file, right click on the following link and select Save Target as...</asp:Label>
        <asp:HyperLink ID="lnkDownload2" runat="server" Target="_blank">HyperLink</asp:HyperLink>
    </asp:Panel>
    <asp:Panel ID="pnlNODescarga" runat="server" Visible="false" CssClass='button'>
        <br>
        <table id="Table3" cellspacing="1" cellpadding="1" width="100%">
            <tr>
                <td width="90">
                </td>
                <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="LblTituloDescargaArchivo" runat="server"
                        CssClass="tituloDescarga"></asp:Label>
                </td>
            </tr>
        </table>
        <br>
        <br>
        <br>
        <table id="Table2" cellspacing="1" cellpadding="1" width="100%">
            <tr>
                <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Image ID="imgAlerta" runat="server"
                        SkinID="Alerta" />
                </td>
                <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblTituloNoDescarga" runat="server" CssClass="subtituloDescarga"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <br />
                    <br />
                    <br />
                    &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button
                        ID="btnCerrar" runat="server" CssClass="boton" OnClientClick="window.parent.close(); return false;" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    </form>
</body>
</html>
