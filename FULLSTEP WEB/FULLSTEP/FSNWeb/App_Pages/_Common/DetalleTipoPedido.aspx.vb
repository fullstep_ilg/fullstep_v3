﻿Public Partial Class DetalleTipoPedido
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim oTipoPedido As FSNServer.TipoPedido
        Dim ds As DataSet
        Dim arrConcepto As String()
        Dim arrAlmacenamiento As String()
        Dim arrRecepcion As String()

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.TipoPedido

        Me.lblTitulo.Text = Textos(0)
        Me.lblSubtitulo.Text = Textos(1)
        Me.lblCod.Text = Textos(2)
        Me.lblDenominacion.Text = Textos(3)
        Me.lblConcepto.Text = Textos(4)
        Me.lblAlmacen.Text = Textos(5)
        Me.lblRecepcion.Text = Textos(6)

        ReDim arrConcepto(2)
        arrConcepto(0) = Textos(7)
        arrConcepto(1) = Textos(8)
        arrConcepto(2) = Textos(9)

        ReDim arrAlmacenamiento(2)
        arrAlmacenamiento(0) = Textos(10)
        arrAlmacenamiento(1) = Textos(11)
        arrAlmacenamiento(2) = Textos(12)

        ReDim arrRecepcion(2)
        arrRecepcion(0) = Textos(13)
        arrRecepcion(1) = Textos(11)
        arrRecepcion(2) = Textos(12)

        'carga los datos del tipo de pedido:
        oTipoPedido = FSNServer.Get_Object(GetType(FSNServer.TipoPedido))
        ds = oTipoPedido.BuscarDetalleTipoPedido(Request("cod"), Idioma)

        If ds.Tables(0).Rows.Count > 0 Then
            lblCodBD.Text = ds.Tables(0).Rows(0).Item("COD")
            lblDenominacionBD.Text = DBNullToSomething(ds.Tables(0).Rows(0).Item("DEN"))
            lblConceptoBD.Text = arrConcepto(ds.Tables(0).Rows(0).Item("CONCEPTO"))
            lblAlmacenBD.Text = arrAlmacenamiento(ds.Tables(0).Rows(0).Item("ALMACENAR"))
            lblRecepcionBD.Text = arrRecepcion(ds.Tables(0).Rows(0).Item("RECEPCIONAR"))
        End If

        ds = Nothing
        oTipoPedido = Nothing
    End Sub

End Class