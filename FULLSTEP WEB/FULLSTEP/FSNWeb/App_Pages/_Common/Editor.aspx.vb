﻿Public Partial Class Editor
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Le añadimos el CKFinder
        If Request("readOnly") <> "1" Then
            Dim FileBrowser1 As New CKFinder.FileBrowser
            FileBrowser1.BasePath = ConfigurationManager.AppSettings("rutaPM2008") & "/ckfinder/"
            FileBrowser1.SetupCKEditor(CKEditor1)
        End If

        If Request("COPIA_CAMPO_ID") = "" Then 'para que no haga el postback al pulsar en guardar
            form1.Action = "javascript:handleCKEditorPost();void(0);"
        End If

        If Not IsPostBack Then
            CargarValorCampoEnEditor()
        Else 'solo hace postback cuando se está modificando una solicitud desde GS            
            Me.InsertarEnCache("Editor_" & Request("COPIA_CAMPO_ID") & "_" & FSNUser.CodPersona, CKEditor1.Text)
        End If

        RegistrarScripts()
    End Sub

    ''' <summary>
    ''' Carga el valor del campo editor. Se utiliza desde el visor de contratos, no conformidades, y desde GS
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load(); Tiempo máximo: 0,2 sg.</remarks>
    Sub CargarValorCampoEnEditor()
        Dim oValor As Object = DBNull.Value
        If Request("COPIA_CAMPO_ID") <> "" OrElse Request("FORM_CAMPO") <> "" OrElse Request("CAMPO_ORIGEN") <> "" Then
            Dim dsCampo As DataSet
            If Request("COPIA_CAMPO_ID") <> "" Then
                Dim sTextoCache As String
                sTextoCache = CType(Cache("Editor_" & Request("COPIA_CAMPO_ID") & "_" & FSNUser.CodPersona), String)
                If sTextoCache = Nothing Then
                    Dim oInstancia As FSNServer.Instancia
                    oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                    dsCampo = oInstancia.cargarCopiaCampo(Request("COPIA_CAMPO_ID"))
                    oInstancia = Nothing
                Else
                    CKEditor1.Text = sTextoCache
                End If
            ElseIf Request("CAMPO_ORIGEN") <> "" Then
                Dim oInstancia As FSNServer.Instancia
                oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                dsCampo = oInstancia.cargarCampoOrigen(Request("ID"), Request("CAMPO_ORIGEN"))
                oInstancia = Nothing
            ElseIf Request("FORM_CAMPO") <> "" Then
                Dim oSolicitud As FSNServer.Solicitud
                oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
                dsCampo = oSolicitud.cargarCampo(Request("FORM_CAMPO"))
                oSolicitud = Nothing
            End If
            If Not dsCampo Is Nothing AndAlso dsCampo.Tables(0).Rows.Count > 0 AndAlso Not IsDBNull(dsCampo.Tables(0).Rows(0).Item("VALOR_TEXT")) Then
                oValor = Server.UrlDecode(dsCampo.Tables(0).Rows(0).Item("VALOR_TEXT"))
            End If
            dsCampo = Nothing
        ElseIf Request("CAMPO_HIJO") <> Nothing AndAlso Request("LINEA") <> Nothing Then
            Dim oSolicitud As FSNServer.Solicitud
            oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
            oValor = oSolicitud.cargarCampoDesglose(Request("CAMPO_HIJO"), Request("LINEA"))
            oSolicitud = Nothing
        End If
        If Not IsDBNull(oValor) Then
            If Request("readOnly") <> "1" Then
                CKEditor1.Text = Server.UrlDecode(oValor)
            Else
                texto.InnerHtml = Server.UrlDecode(oValor)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Registra en el aspx las funciones javascript para la configuración del editor, la carga del texto y su guardado
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load(); Tiempo máximo: 0,1 sg.</remarks>
    Sub RegistrarScripts()
        Dim sScript As String
        If Request("readOnly") <> "1" Then
            sScript = "function handleCKEditorPost() {" & vbCrLf
            If Request("desdeGS") <> "1" Then
                sScript = sScript & "p = window.opener" & vbCrLf
                sScript = sScript & "p.save_editor_text('" & Request("ID") & "', escape(CKEDITOR.instances.CKEditor1.getData()))" & vbCrLf
            End If
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.Editor
            sScript = sScript & "alert('" & JSText(Textos(0)) & "')" & vbCrLf & "}" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "handleCKEditorPost", sScript, True)
        End If

        If Request("readOnly") <> "1" Then
            Dim sIdioma As String = "en_US"
            If FSNUser.Idioma.ToString = "SPA" Then
                sIdioma = "es_ES"
            ElseIf FSNUser.Idioma.ToString = "GER" Then
                sIdioma = "de_DE"
            ElseIf FSNUser.Idioma.ToString = "FRA" Then
                sIdioma = "fr_FR"
            End If

            sScript = "function ConfigurarIdioma() {" & vbCrLf & _
                "CKEDITOR.config.defaultLanguage ='" & sIdioma & "';" & vbCrLf & _
                "CKEDITOR.config.language ='" & sIdioma & "';" & vbCrLf & _
                "CKEDITOR.config.scayt_sLang ='" & sIdioma & "';" & vbCrLf & _
                "CKEDITOR.config.scayt_defLan = '" & sIdioma & "';" & vbCrLf & _
                "setTimeout(""ConfigurarSCAYT('" & sIdioma & "')"",2000)" & vbCrLf & _
                "}" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ConfigurarIdioma", sScript, True)
        Else
            sScript = "function ConfigurarIdioma() {" & vbCrLf & _
                "}" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ConfigurarIdioma", sScript, True)
        End If

        If Request("desdeGS") <> "1" Then
            sScript = "function CargarTexto() {" & vbCrLf & _
                "var valor;" & vbCrLf & _
                "e = window.opener.document.getElementById('" & Request("ID") & "__tbl');" & vbCrLf & _
                "if (e) {" & vbCrLf & _
                    "o = e.Object;" & vbCrLf & _
                    "valor = unescape(o.getDataValue());" & vbCrLf & _
                "} else {" & vbCrLf & _
                    "e = window.opener.document.getElementById('" & Request("ID") & "');" & vbCrLf & _
                    "if (e) " & vbCrLf & _
                        "valor = unescape(e.value);" & vbCrLf & _
                "}" & vbCrLf & vbCrLf & _
                "if (e) {" & vbCrLf
            If Request("readOnly") <> "1" Then
                sScript = sScript & "CKEditor1 = document.getElementById('" & CKEditor1.ClientID & "');" & vbCrLf & _
                    "CKEditor1.value = valor;" & vbCrLf & _
                    "if ((CKEditor1.value == null) || (CKEditor1.value == 'null'))" & vbCrLf & _
                        "CKEditor1.value = '';" & vbCrLf
            Else
                sScript = sScript & "document.getElementById('" & texto.ClientID & "').innerHTML = valor;" & vbCrLf
            End If
            sScript = sScript & "}" & vbCrLf
            sScript = sScript & "}" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "CargarTexto", sScript, True)
        Else
            sScript = "function CargarTexto() {" & vbCrLf & _
                "}" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "CargarTexto", sScript, True)
        End If
    End Sub
End Class