﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DetalleTipoPedido.aspx.vb" Inherits="Fullstep.FSNWeb.DetalleTipoPedido" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
    <title></title>
</head>
<body style="background-color: #FFFFFF;margin-top: 0px; margin-left: 0px" onload="window.focus();">
    <form id="form1" runat="server">
    <div style='width:100%;z-index:10000; position:absolute;'>
    <div style='width:100%; height:35px;' class='CapaTituloPanelInfo'>&nbsp;</div>
    <div style='width:100%; height:25px;' class='CapaSubTituloPanelInfo'>&nbsp;</div></div>
    
    <table cellspacing="0" cellpadding="2" width="100%" border="0" style="z-index:10002; position:absolute;">
	<tr style="height:35px;">
	    <td width="1%" style="background-color: Transparent;" rowspan="2"><asp:Image ID="imgCalendar" runat="server" SkinId="Calendario" /></td>
	    <td align="left"><asp:label id="lblTitulo" runat="server" CssClass="TituloPopUpPanelInfo"></asp:label></td>
	    <td id="cellImagenCerrar" align="right" valign="top" style="width:1%"><asp:ImageButton ID="imgCerrar" runat="server" SkinID="Cerrar" OnClientClick="javascript:self.close();return false;" /></td>
    </tr>	    
	<tr style="height:25px;">
	   <td align="left" valign="top"><asp:label id="lblSubtitulo" runat="server" CssClass="SubTituloPopUpPanelInfo"></asp:label></td>
	   <td></td>
	</tr>
	<tr><td colspan="3">
	<table width="100%">
	<TR><TD nowrap><asp:Label id="lblCod" runat="server" CssClass="Etiqueta"></asp:Label></TD>
		<TD><asp:Label id="lblCodBD" runat="server"></asp:Label></TD>
	</TR>
	<TR><TD nowrap><asp:Label id="lblDenominacion" runat="server" CssClass="Etiqueta"></asp:Label></TD>
		<TD><asp:Label id="lblDenominacionBD" runat="server"></asp:Label></TD>
    </TR>
	<TR><TD nowrap><asp:Label id="lblConcepto" runat="server" CssClass="Etiqueta"></asp:Label></TD>
		<TD><asp:Label id="lblConceptoBD" runat="server"></asp:Label></TD>
	</TR>
	<TR><TD nowrap><asp:Label id="lblAlmacen" runat="server" CssClass="Etiqueta"></asp:Label></TD>
		<TD><asp:Label id="lblAlmacenBD" runat="server"></asp:Label></TD>
	</TR>
	<TR><TD nowrap><asp:Label id="lblRecepcion" runat="server" CssClass="Etiqueta"></asp:Label></TD>
		<TD><asp:Label id="lblRecepcionBD" runat="server"></asp:Label></TD>
	</TR>
	</table>
	</td></tr>
	</table>
    </form>
</body>
</html>

