﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BuscadorArticulos.aspx.vb"
    Inherits="Fullstep.FSNWeb.BuscadorArticulos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>"
lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
    <style type="text/css">
        html{height:100%; margin-bottom:1px;}
    </style>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
 </head>   
 <body style="background-color: #FFFFFF; margin-top: 0px; margin-left: 0px; margin-right: 0px">

    <form id="frmArticulos" name="frmArticulos" method="post" runat="server" onkeypress="return Form_KeyPres(event);">
    <asp:ScriptManager ID="ScriptManager1" runat="server">

    </asp:ScriptManager>
    <input id="txtArtClientID" type="hidden" name="txtArtClientID" runat="server" />
    <input id="txtMatClientID" type="hidden" name="txtMatClientID" runat="server" />
    <input id="lPrecio" type="hidden" name="lPrecio" />
    <input id="sCodProv" type="hidden" name="sCodProv" />
    <input id="sDenProv" type="hidden" name="sDenProv" />
    <input id="txtCodOrgCompras" type="hidden" name="txtCodOrgCompras" runat="server" />
    <input id="txtCodCentro" type="hidden" name="txtCodCentro" runat="server" />
    <input id="sCodUni" type="hidden" name="sCodUni" />
    <input id="sDenUni" type="hidden" name="sDenUni" />
    <input id="sNumDecUni" type="hidden" name="sNumDecUni" />
    <input id="sMensaje1" type="hidden" name="sMensaje1" runat="server" />
    <input id="txtCentroCto" type="hidden" name="txtCentroCto" runat="server" />
    <input id="desde" type="hidden" name="desde" runat="server" />
    <input id="EsCampoGeneral" type="hidden" name="desde" runat="server" />
    <input id="idControlMatBD" type="hidden" name="idControlMatBD" runat="server" />
    <input id="idControlMatDen" type="hidden" name="idControlMatDen" runat="server" />
    <input id="idControlArtCod" type="hidden" name="idControlArtCod" runat="server" />
    <input id="idControlArtDen" type="hidden" name="idControlArtDen" runat="server" />
    <input id="hRestric" type="hidden" name="hRestric" runat="server" />
    <input id="hMat" type="hidden" name="hMat" runat="server" />
    <input id="hConcepto" type="hidden" name="hConcepto" runat="server" />
    <input id="hAlmacenamiento" type="hidden" name="hAlmacenamiento" runat="server" />
    <input id="hRecepcion" type="hidden" name="hRecepcion" runat="server" />
    <input id="hMostrarCaracteristicas" type="hidden" name="hMostrarCaracteristicas" runat="server" />
    <input id="idHidControl" type="hidden" name="idHidControl" runat="server" />
    <input id="ClientId" type="hidden" name="ClientId" runat="server" />
    <input id="hTipoSolicitud" type="hidden" name="hTipoSolicitud" runat="server" />
    <input id="hProveedor" type="hidden" name="hProveedor" runat="server" />
    <input id="hProveedorSumiArt" type="hidden" name="hProveedorSumiArt" runat="server" />
    <input id="IDCodProve" type="hidden" name="IDCodProve" runat="server" />
    <input id="hProveedorIniNegociado" type="hidden" name="hProveedorIniNegociado" runat="server" />
    <input id="Rellenos" type="hidden" name="Rellenos" runat="server" />
    <input id="IDFPago" type="hidden" name="IDFPago" runat="server" />
    <input id="IDFPagoHidden" type="hidden" name="IDFPagoHidden" runat="server" />
    <input id="hExpressNegocidado" type="hidden" name="hExpressNegociado" runat="server" value="0" />
    <input id="hUnidadOrganizativa" type="hidden" name="hUnidadOrganizativa" runat="server" />
    <input id="hTipoCampoGS" type="hidden" name="hTipoCampoGS" runat="server" />
    <table border="0" width="100%" cellspacing="0">
        <tr class="ColorFondoTab">
            <td width="5%">
                <asp:Image ID="Image1" runat="server" SkinID="ArticuloBuscar" />
            </td>
            <td align="left">
                <asp:Label ID="lbltitulo" runat="server" CssClass="RotuloGrande"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="height: 5px">
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" style="margin-left: 5px; margin-right: 5px;
        border-color: #eeeeee; border-spacing: 2px; border-style: solid; width: 99%;">
        <tr>
            <td>
                <table id="tblBusqueda" border="0" width="100%">
                    <tr>
                        <td colspan="4">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblUnidadOrganizativa" runat="server" CssClass="Etiqueta" Text="dUnidad Organizativa"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Panel runat="server" ID="pnlUnidadesOrganizativas">
                                            <asp:TextBox ID="txtUnidadOrganizativa" runat="server" ReadOnly="true" onkeydown="LimpiarUON();return false;"></asp:TextBox>
                                            <asp:Image ID="imgBuscarUnidadOrganizativa" runat="server" style="cursor:pointer;" />
                                            <div style="display:none;">
                                                <ig:WebDataTree runat="server" ID="wdtUnidadesOrganizativas" SelectionType="Single" 
                                                    InitialExpandDepth="4" InitialDataBindDepth="4">
                                                    <ClientEvents Initialize="initTree" SelectionChanged="seleccionarUnidadOrg" />
                                                    <NodeSettings  SelectedCssClass="Seleccionable"/>
                                                    <DataBindings>
                                                        <ig:DataTreeNodeBinding DataMember="NIV0" TextField="DEN" KeyField="COD" ValueField="COD" />
                                                        <ig:DataTreeNodeBinding DataMember="NIV1" TextField="DEN" KeyField="COD" ValueField="COD" />
                                                        <ig:DataTreeNodeBinding DataMember="NIV2" TextField="DEN" KeyField="COD" ValueField="COD" />
                                                        <ig:DataTreeNodeBinding DataMember="NIV3" TextField="DEN" KeyField="COD" ValueField="COD" />
                                                    </DataBindings>
                                                </ig:WebDataTree>
                                            </div>                                            
                                        </asp:Panel>
                                        <asp:Label runat="server" ID="lblUnidadOrganizativaSeleccionada" Text="dPrueba"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="375px">
                                        <asp:Label ID="lblOrgCompras" runat="server" CssClass="Etiqueta"></asp:Label>
                                    </td>
                                    <td width="375px">
                                        <asp:Label ID="lblCentro" runat="server" CssClass="Etiqueta"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="375px">
                                        <ig:WebDropDown ID="ugtxtOrganizacionCompras" runat="server" Width="98%" EnableClosingDropDownOnBlur="true"
                                            EnableClosingDropDownOnSelect="true" DropDownContainerWidth="367px">
                                            <items>
                                            <ig:DropDownItem>
                                            </ig:DropDownItem>
                                        </items>
                                            <itemtemplate>
                                            <ig:WebDataGrid ID="ugtxtOrganizacionCompras_wdg" runat="server"
                                                AutoGenerateColumns="false" Width="100%" ShowHeader="false">
                                                <Columns>
                                                    <ig:BoundDataField DataFieldName="COD" Key="COD" Width="20%">
                                                    </ig:BoundDataField>
                                                    <ig:BoundDataField DataFieldName="DEN" Key="DEN" Width="80%">
                                                    </ig:BoundDataField>
                                                </Columns>
                                                <Behaviors>
                                                    <ig:Selection RowSelectType="Single" Enabled="True" CellClickAction="Row">
                                                        <SelectionClientEvents RowSelectionChanged="WebDataGrid_RowSelectionChanged" />
                                                    </ig:Selection>
                                                </Behaviors>
                                            </ig:WebDataGrid>
                                        </itemtemplate>
                                        </ig:WebDropDown>
                                        <asp:Label ID="lblOrgComprasNombre" runat="server" CssClass="Etiqueta"></asp:Label>
                                    </td>
                                    <td width="375px">
                                        <asp:UpdatePanel ID="updCentros" runat="server" UpdateMode="conditional">
                                            <ContentTemplate>
                                                <ig:WebDropDown ID="ugtxtCentros" runat="server" Width="98%" EnableClosingDropDownOnBlur="true"
                                                    EnableClosingDropDownOnSelect="true" DropDownContainerWidth="367px">
                                                    <items>
                                                        <ig:DropDownItem></ig:DropDownItem>
                                                    </items>
                                                    <itemtemplate>
                                                        <ig:WebDataGrid ID="ugtxtCentros_wdg" runat="server"
                                                            AutoGenerateColumns="false" Width="100%" ShowHeader="false">
                                                            <Columns>
                                                                <ig:BoundDataField DataFieldName="COD" Key="COD" Width="20%">
                                                                </ig:BoundDataField>
                                                                <ig:BoundDataField DataFieldName="DEN" Key="DEN" Width="80%">
                                                                </ig:BoundDataField>
                                                            </Columns>
                                                            <Behaviors>
                                                                <ig:Selection RowSelectType="Single" Enabled="True" CellClickAction="Row">
                                                                    <SelectionClientEvents RowSelectionChanged="WebDataGrid_RowSelectionChanged" />
                                                                </ig:Selection>
                                                            </Behaviors>
                                                        </ig:WebDataGrid>
                                                    </itemtemplate>
                                                </ig:WebDropDown>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <asp:Label ID="lblCentroNombre" runat="server" CssClass="Etiqueta"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="Linea1" runat="server">
                        <td colspan="2">
                            <asp:Label ID="lblProveedor" runat="server" CssClass="Etiqueta"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div style="clear: both;">
                                <div style="width: 100%; float: left;">
                                    <div id="divCajaProveedor" runat="server" style="float: left;">
                                        <asp:TextBox ID="txtProveedor" runat="server" ReadOnly="true" onkeydown="LimpiarProveedor();return false;"></asp:TextBox>
                                        &nbsp;<asp:ImageButton ID="imgProveedor" runat="server" OnClientClick="AbrirProveedor();" />
                                    </div>
                                    <div id="divFijoProveedor" runat="server" style="float: left;">
                                        <asp:Label ID="lblProveedorFijo" runat="server" Width="500px" CssClass="Etiqueta"></asp:Label>
                                    </div>
                                    <div id="divChkProveedor" runat="server" style="float: left;">
                                        &nbsp;&nbsp;<asp:CheckBox ID="chkVerNoProveedor" runat="server" />
                                    </div>
                                    <div id="divProveSumiArt" runat="server" style="float: left;">
                                        <asp:Label ID="lblProveSumiArt" runat="server" Width="500px" CssClass="Etiqueta"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 21px">
                            <asp:Label ID="lblMaterial" runat="server" CssClass="Etiqueta"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:TextBox ID="txtMaterial" runat="server" ReadOnly="true" Width="750px" onkeydown="LimpiarMaterial();return false;">
                            </asp:TextBox>&nbsp;&nbsp;<asp:ImageButton ID="imgBuscarMaterial" runat="server"
                                OnClientClick="AbrirMateriales();return false;" />
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 21px">
                            <asp:Label ID="lblCod" runat="server" CssClass="Etiqueta"></asp:Label>
                        </td>
                        <td style="height: 21px">
                            <asp:Label ID="lblDen" runat="server" CssClass="Etiqueta"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtCod" runat="server" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtDen" runat="server" Width="500px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%" border="0">
                                <tr>
                                    <td colspan="4" style="height: 21px">
                                        <asp:Label ID="lblOtrasCaracteristicas" runat="server" CssClass="Etiqueta"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="pnlCaracteristicasCompleto" runat="server">
                                            <table width="100%">
                                                <tr>
                                                    <td style="vertical-align: 0px;">
                                                        <table>
                                                            <tr><td><asp:CheckBox ID="chkGasto" runat="server" /></td></tr>
                                                            <tr><td><asp:CheckBox ID="chkInversion" runat="server" /></td></tr>
                                                            <tr><td><asp:CheckBox ID="chkGastoInversion" runat="server" /></td></tr>
                                                        </table>
                                                    </td>
                                                    <td style="vertical-align: 0px;">
                                                        <table>
                                                            <tr><td><asp:CheckBox ID="chkNoAlmacenable" runat="server" /></td></tr>
                                                            <tr><td><asp:CheckBox ID="chkAlmacenable" runat="server" /></td></tr>
                                                            <tr><td><asp:CheckBox ID="chkAlmacenamientoOpcional" runat="server" /></td></tr>
                                                        </table>
                                                    </td>
                                                    <td style="vertical-align: 0px;">
                                                        <table>
                                                            <tr><td><asp:CheckBox ID="chkRecepcionObligatoria" runat="server" /></td></tr>
                                                            <tr><td><asp:CheckBox ID="chkNoRecepcionable" runat="server" /></td></tr>
                                                            <tr><td><asp:CheckBox ID="chkRecepcionOpcional" runat="server" /></td></tr>
                                                        </table>
                                                    </td>
                                                    <td style="vertical-align: 0px;">
                                                        <table>
                                                            <tr><td><asp:CheckBox ID="chkGenerico" runat="server" /></td></tr>
                                                            <tr><td><asp:CheckBox ID="chkNoGenerico" runat="server" /> </td></tr> 
                                                            <tr>
                                                                <td>
                                                                <asp:CheckBox ID="chkSoloFavoritos" runat="server" CssClass="parrafo" Checked="false"></asp:CheckBox>
                                                                <asp:ImageButton id="imgSoloFavoritos" runat="server" SkinID="CabFavorito" />
                                                                <asp:Label ID="lblSoloFavoritos" runat="server" Text="dMostrar solo favoritos"></asp:Label>        
                                                                </td>
                                                            </tr>                                                        
                                                        </table>
                                                    </td>
                                                    <td style="vertical-align: 0px;">
                                                        <table>
                                                            <tr><td><asp:CheckBox ID="chkCatalogados" runat="server" /></td></tr>
                                                            <tr><td><asp:CheckBox ID="chkNoCatalogados" runat="server" /></td></tr>
                                                        </table>
                                                    </td>
                                                    
                                                </tr>                                         
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlCaracteristicasSimple" runat="server">
                                            <table width="100%">
                                                <tr>
                                                    <td><asp:CheckBox ID="chkGenerico2" runat="server" /></td>
                                                    <td><asp:CheckBox ID="chkNoGenerico2" runat="server" /></td>
                                                </tr>
                                                <tr>
                                                    <td><asp:CheckBox ID="chkCatalogados2" runat="server" /></td>
                                                    <td><asp:CheckBox ID="chkNoCatalogados2" runat="server" /></td>
                                                </tr>    
                                                <tr>
                                                    <td>
                                                    <asp:CheckBox ID="chkSoloFavoritos2" runat="server" CssClass="parrafo" Checked="false"></asp:CheckBox>
                                                    <asp:ImageButton id="imgSoloFavoritos2" runat="server" SkinID="CabFavorito" />
                                                    <asp:Label ID="lblSoloFavoritos2" runat="server" Text="dMostrar solo favoritos"></asp:Label>        
                                                    </td>
                                                </tr>                   
                                            </table>
                                        </asp:Panel>
                                    </td>
                                    <td>                                       
                                        <input type="button" id="btnBuscar" runat="server" value="pBuscar" class="botonRedondeado" onclick="buscar()" style="float:right;"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%" border="0">
                                <tr>
                                    <td colspan="3" class="fila_cabecera" style="text-align: left;">
                                        &nbsp;&nbsp;<asp:Label ID="lblBuscPorAtributos" runat="server" CssClass="Etiqueta"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:UpdatePanel id="upAtributosUON" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="wdtUnidadesOrganizativas" EventName="SelectionChanged" />
                                            </Triggers>
                                            <ContentTemplate>
                                                <div id="divBuscAtrib" style="border: 1px solid #c5c7cb;">
                                                    <table id="tblAtributosUON" style="border: 1px solid #E8E8E8; width:100%; table-layout:fixed;border-collapse:separate" >
                                                        <tr style="background-color:#cccccc; height:20px">
                                                            <td style="width:1%; height:20px; display:none;"><asp:Label ID="lblIdAtributoTablaGrupo" runat="server" Text="DId"></asp:Label></td>
                                                            <td style="width:23%; height:20px"><asp:Label ID="lblCodigoAtributoTablaUON" runat="server" Text="DCodigo"></asp:Label></td>
                                                            <td style="width:40%; height:20px"><asp:Label ID="lblDenomAtributoTablaUON" runat="server" Text="DDenominacion"></asp:Label></td>
                                                            <td style="width:1%; height:20px; display:none;"><asp:Label ID="lblTipoAtributoTablaUON" runat="server" Text="DTipo"></asp:Label></td>
                                                            <td style="width:1%; height:20px; display:none;"><asp:Label ID="lblIntroAtributoTablaUON" runat="server" Text="DIntro"></asp:Label></td>
                                                            <td style="width:37%; height:20px"><asp:Label ID="lblValorAtributoTablaUON" runat="server" Text="DValor"></asp:Label></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanelArticulos" runat="server" UpdateMode="conditional">
        <ContentTemplate>
            <input id="txtDesdeFila" type="hidden" name="txtDesdeFila" runat="server" />
            <input id="txtNumFilas" type="hidden" name="txtNumFilas" runat="server" />
            <table border="0" style="margin-left: 5px; margin-right: 5px;" cellspacing="0" width="99%">
                <tr>
                    <td colspan="3" style="height: 5px;">
                    </td>
                </tr>
                <!--style="background-color: #eeeeee; height:20px;"-->
                <tr>
                    <td colspan="3" class="fila_cabecera" style="text-align: left;">
                        &nbsp;&nbsp;<asp:Label ID="lblArticulosEncontrados" runat="server" CssClass="Etiqueta"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <div id="divArt" style="border: 1px solid #c5c7cb;">
                            <ig:WebDataGrid id="wdgArticulos" runat="server" Width="100%" Height="215px" AutoGenerateBands="false"
                                AutoGenerateColumns="false" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="true">
                                <behaviors>
                                    <ig:Selection  RowSelectType="Single" Enabled="True" CellClickAction="Cell">
                                        <SelectionClientEvents CellSelectionChanged="wdgArticulos_Selection_CellSelectionChanged" 
                                                               CellSelectionChanging="wdgArticulos_Selection_CellSelectionChanging" />
                                    </ig:Selection> 
                            
                                <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true"  AnimationEnabled="true">
                                    <FilteringClientEvents DataFiltered="AdministrarPaginador" /> 
                                </ig:Filtering> 
                                <ig:RowSelectors Enabled="false" RowNumbering="false"></ig:RowSelectors>
                                <ig:Paging Enabled="true" PagerAppearance="Top" PageSize="20">
                                    <PagingClientEvents PageIndexChanged="wdgArticulos_PageIndexChanged" />
                                    <PagerTemplate>
								    <div class="CabeceraBotones" style="clear:both; float:left; width:100%;"> 
									<div style="float:left; padding:3px 0px 2px 0px; margin-left:10px;">
										<div style="clear: both; float: left; margin-right: 5px;">
                                            <asp:Image ID="ImgBtnFirst" runat="server" OnClientClick="FirstPage()" style="margin-right:2px;" />
                                            <asp:Image ID="ImgBtnPrev" runat="server" OnClientClick="PrevPage()" />
                                        </div>
                                        <div style="float: left; margin-right:5px;">
                                            <asp:Label ID="lblPage" runat="server" Text="dPage" Style="margin-right: 3px;"></asp:Label>
                                            <asp:DropDownList ID="PagerPageList" runat="server" onchange="return IndexChanged()" Style="margin-right: 3px;" />
                                            <asp:Label ID="lblOF" runat="server" Text="dOf" Style="margin-right: 3px;"></asp:Label>
                                            <asp:Label ID="lblCount" runat="server" />
                                        </div>
                                        <div style="float: left;">
                                            <asp:Image ID="ImgBtnNext" runat="server" OnClientClick="NextPage()" style="margin-right:2px;" />
                                            <asp:Image ID="ImgBtnLast" runat="server" OnClientClick="LastPage()" />
                                        </div>
									</div>   
                                    </div>  
                                    </PagerTemplate>
                                </ig:Paging>
                                <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
                            </behaviors>
                                <clientevents click="grid_CellClick" />
                            </ig:WebDataGrid>
                        </div>
                    </td>
                </tr>
                <br />
                <tr>
                    <td colspan="4">
                        <table width="100%">
                            <tr>
                                <td align="right" width="50%">
                                    <fsn:FSNButton ID="btnAceptar" runat="server" Text="Aceptar" Alineacion="Right" OnClientClick="aceptar();return false;"></fsn:FSNButton>
                                </td>
                                <td align="left" width="50%">
                                    <fsn:FSNButton ID="btnCancelar" runat="server" Text="Cancelar" Alineacion="Left"
                                        OnClientClick="window.close();return false;"></fsn:FSNButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <asp:CheckBox ID="chkAnyadirAutoFav" runat="server" Checked="false" AutoPostBack ="true"></asp:CheckBox>
                                <asp:Label ID="LblAnyadirAutoFav" runat="server" Text="dAñadir automáticamente los artículos seleccionados a la lista de favoritos" type="hidden"></asp:Label>        
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnBuscar" />
        </Triggers>
    </asp:UpdatePanel>
    <script id="tmplCombo" type="text/x-jquery-tmpl">
        <tr style="vertical-align:middle">
            <td style="background-color:#F0F0F0; height:21px; display:none; vertical-align:middle"><span>${Id}</span></td>
            <td style="background-color:#F0F0F0; height:21px; vertical-align:middle"><span>${Codigo}</span></td>
            <td style="background-color:#F0F0F0; height:21px; vertical-align:middle"><span>${Denominacion}</span></td>
            <td style="background-color:#F0F0F0; height:21px; display:none; vertical-align:middle"><span>${Tipo}</span></td>
            <td style="background-color:#F0F0F0; height:21px; display:none; vertical-align:middle"><span>${Intro}</span></td>
            <td style="height:20px;background-color:#FFFFFF; vertical-align:middle">
                <div style="clear:both;float:left; height:21px; width:100%">
                    <div id='cboAtrib_${Id}' style=" float:left;height:21px; width: 100%;">
                    </div>
                </div>
            </td>
        </tr>
    </script>

    <script id="tmplCampoTexto" type="text/x-jquery-tmpl">
        <tr style="vertical-align:middle">
            <td style="background-color:#F0F0F0; height:20px; display:none; vertical-align:middle"><span>${Id}</span></td>
            <td style="background-color:#F0F0F0; height:20px; vertical-align:middle"><span>${Codigo}</span></td>
            <td style="background-color:#F0F0F0; height:20px; vertical-align:middle"><span>${Denominacion}</span></td>
            <td style="background-color:#F0F0F0; height:20px; display:none; vertical-align:middle"><span>${Tipo}</span></td>
            <td style="background-color:#F0F0F0; height:20px; display:none; vertical-align:middle"><span>${Intro}</span></td>
            <td style="height:20px;background-color:#FFFFFF; vertical-align:middle">
                <textarea id='inputTextAtrib_${Id}' rows=1 style="width: 97%; border:none">${Valor}</textarea>
            </td>
        </tr>
    </script>
    
    <script id="tmplCampoNumerico" type="text/x-jquery-tmpl">
        <tr style="vertical-align:middle">
            <td style="background-color:#F0F0F0; height:20px; display:none; vertical-align:middle"><span>${Id}</span></td>
            <td style="background-color:#F0F0F0; height:20px; vertical-align:middle"><span>${Codigo}</span></td>
            <td style="background-color:#F0F0F0; height:20px; vertical-align:middle"><span>${Denominacion}</span></td>
            <td style="background-color:#F0F0F0; height:20px; display:none; vertical-align:middle"><span>${Tipo}</span></td>
            <td style="background-color:#F0F0F0; height:20px; display:none; vertical-align:middle"><span>${Intro}</span></td>
            <td style="height:20px;background-color:#FFFFFF; vertical-align:middle">
                <input id='inputNumAtrib_${Id}' type="text" onBlur="validaFloat(this);"  value='${formatNumber($data.Valor)}' style="width: 97%; border:none; text-align:right"/>
            </td>
        </tr>
    </script>

    <script id="tmplCampoFecha" type="text/x-jquery-tmpl">
        <tr style="vertical-align:middle">
            <td style="background-color:#F0F0F0; height:20px; display:none; vertical-align:middle"><span>${Id}</span></td>
            <td style="background-color:#F0F0F0; height:20px; vertical-align:middle"><span>${Codigo}</span></td>
            <td style="background-color:#F0F0F0; height:20px; vertical-align:middle"><span>${Denominacion}</span></td>
            <td style="background-color:#F0F0F0; height:20px; display:none; vertical-align:middle"><span>${Tipo}</span></td>
            <td style="background-color:#F0F0F0; height:20px; display:none; vertical-align:middle"><span>${Intro}</span></td>
            <td style="height:20px;background-color:#FFFFFF; vertical-align:middle">
               <input type="text" id='inputFechaAtrib_${Id}' onBlur="validaDate(this);" class="Texto12 TextoOscuro" style="border:none; width:91%; text-align:center;"/>	
            </td>
        </tr>
    </script>
    <script type="text/javascript">
        $(document).ready(function () {            
            $('#btnBuscar').prop('value',TextoBuscar);
        });

        function Alert(msg) {
            $get('divAlertContent').innerHTML = msg;
            popUpShowed = $find("Alert");
            popUpShowed.show();
            popUpShowed._backgroundElement.style.zIndex += 10;
            popUpShowed._foregroundElement.style.zIndex += 10;
        }

        window.alert = Alert;

        //''' <summary>
        //''' Pregunta si desea borrar los artículos de la pantalla q llamo a esta.
        //''' </summary>
        //''' <remarks>Llamada desde: Aceptar() ; Tiempo máximo: 0,2</remarks>
        function Confirm(msg) {
            $get('divConfirmContent').innerHTML = msg;
            popUpShowed = $find('Confirm');
            popUpShowed.show();
            popUpShowed._backgroundElement.style.zIndex += 10;
            popUpShowed._foregroundElement.style.zIndex += 10;
        }

        function confirm2(msg) {
            $get('#panCambiarProve #div1').innerHTML = msg;
            popUpShowed = $find('Confirm2');
            popUpShowed.show();
            popUpShowed._backgroundElement.style.zIndex += 10;
            popUpShowed._foregroundElement.style.zIndex += 10;
        }

        window.confirm = Confirm;

        //''' <summary>
        //''' Responde sí a la pregunta si desea borrar los artículos de la pantalla q llamo a esta.
        //''' </summary>
        //''' <remarks>Llamada desde: mpeConfirm.OnOkScript ; Tiempo máximo: 0,2</remarks>
        function PreguntaSi() {
            document.getElementById("hProveedorIniNegociado").value = "";

            p = window.opener
            p.Borra_Los_Articulos()

            aceptar();
        }
        
    </script>
    <!-- Alertas de validación -->
    <asp:Button ID="btnAlert" runat="server" Style="display: none" />
    <ajx:ModalPopupExtender ID="mpeAlert" runat="server" Enabled="True" PopupControlID="panAlert"
        DropShadow="true" TargetControlID="btnAlert" OkControlID="btnAceptarAlert" BehaviorID="Alert"
        BackgroundCssClass="modalBackground" OnOkScript="popUpShowed = null" />
    <asp:Panel ID="panAlert" runat="server" Style="display: none" Width="370px" Height="100px"
        CssClass="modalPopup">
        <table>
            <tr>
                <td height="5px" colspan="3">
                </td>
            </tr>
            <tr>
                <td width="5px">
                </td>
                <td>
                    <asp:Image ID="Image2" runat="server" SkinID="ErrorGrande" />
                </td>
                <td>
                    <div id="divAlertContent">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="2" align="center">
                    <table>
                        <tr>
                            <td>
                                <fsn:FSNButton ID="btnAceptarAlert" runat="server" Text="OK" Alineacion="Left"></fsn:FSNButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <!-- Pregunta de validación -->
    <asp:Button ID="btnConfirm" runat="server" Style="display: none" />
    <ajx:ModalPopupExtender ID="mpeConfirm" runat="server" Enabled="True" PopupControlID="panConfirm"
        DropShadow="true" TargetControlID="btnConfirm" OkControlID="btnAceptarConfirm"
        CancelControlID="btnCancelConfirm" BehaviorID="Confirm" BackgroundCssClass="modalBackground"
        OnOkScript="PreguntaSi();popUpShowed = null;" OnCancelScript="popUpShowed = null" />
    <asp:Panel ID="panConfirm" runat="server" Style="display: none" Width="340px" Height="120px"
        CssClass="modalPopup">
        <table>
            <tr>
                <td height="5px" colspan="3">
                </td>
            </tr>
            <tr>
                <td width="5px">
                </td>
                <td>
                    <asp:Image ID="Image3" runat="server" SkinID="ErrorGrande" />
                </td>
                <td>
                    <div id="divConfirmContent">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="2" align="center">
                    <table>
                        <tr>
                            <td>
                                <fsn:FSNButton ID="btnAceptarConfirm" runat="server" Text="OK" Alineacion="Left"></fsn:FSNButton>
                            </td>
                            <td>
                                <fsn:FSNButton ID="btnCancelConfirm" runat="server" Text="Cancel" Alineacion="Left"></fsn:FSNButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <!--Cambio proveedor-->
    <!-- Pregunta de validación -->
    <asp:Button ID="Button1" runat="server" Style="display: none" />
    <ajx:ModalPopupExtender ID="mpeConfirm2" runat="server" Enabled="True" PopupControlID="panCambiarProve"
        DropShadow="true" TargetControlID="btnConfirm" OkControlID="btnAceptarConfirm"
        CancelControlID="btnCancelConfirm" BehaviorID="Confirm2" BackgroundCssClass="modalBackground"
        OnOkScript="BorrarArticulosOtroProve();popUpShowed = null;" OnCancelScript="popUpShowed = null" />
    <script type="text/javascript">

        function BorrarArticulosOtroProve() {
            //''' Elimina los artículos con adjudicaciones vigentes que no sean del nuevo proveedor seleccionado
            //''' Esta acción se realiza únicamente si pedido=express y parametro mostrar negociados activado.
            if (sProveedor != "") {
                p.borraArticulosAdjudicadosProve(sProveedor);
                //Reemplazamos el nuevo proveedor en buscador articulos

                sProveedor = document.getElementById("hProveedor").value;
                document.getElementById("hProveedorIniNegociado").value = sProveedor;
                aceptar();
            }
        }
    </script>
    <asp:Panel ID="panCambiarProve" runat="server" Style="width: 450px; margin: 0 auto;
        position: absolute; z-index: 1555; display: none; border: 1px; padding: 2em;"
        Width="340px" Height="120px" class="popupCN">
        <table>
            <tr>
                <td height="5px" colspan="3">
                </td>
            </tr>
            <tr>
                <td width="5px">
                </td>
                <td>
                    <asp:Image ID="Image4" runat="server" SkinID="ErrorGrande" />
                </td>
                <td>
                    <div id="div1">
                        DDHa seleccionado un artículo adjudicado al proveedor ###.<br />
                        El resto de artículos seleccionados en esta solicitud están adjudicados a otro proveedor.Se
                        borrarán estos artículos.¿Desea continuar?.
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="2" align="center">
                    <table>
                        <tr>
                            <td>
                                <fsn:FSNButton ID="FSNButton1" runat="server" Text="OK" Alineacion="Left" OnClientClick='BorrarArticulosOtroProve();'></fsn:FSNButton>
                            </td>
                            <td>
                                <fsn:FSNButton ID="FSNButton2" runat="server" Text="Cancel" Alineacion="Left"></fsn:FSNButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <!-- Cargando -->
    
    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
        <div style="position: relative; top: 30%; text-align: center;">
            <asp:Image ID="ImgProgress" runat="server" SkinID="ImgProgress" />
            <asp:Label ID="LblProcesando" runat="server" Text="..." OnPreRender="LblProcesando_PreRender"></asp:Label>
        </div>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
    </form>
    <script type="text/javascript">
        // The client event ‘RowSelectionChanged’ takes two parameters sender and e
        // sender  is the object which is raising the event
        // e is the RowSelectionChangedEventArgs
        var gAtributosUON;

        function WebDataGrid_RowSelectionChanged(sender, e) {

            //Gets the selected rows collection of the WebDataGrid
            var selectedRows = e.getSelectedRows();
            //Gets the row that is selected from the selected rows collection
            var row = selectedRows.getItem(0);
            //Gets the second cell object in the row
            //In this case it is ProductName cell 
            var cell = row.get_cell(1);
            //Gets the text in the cell
            var text = cell.get_text();

            //Gets reference to the WebDropDown
            var dropdown = null;
            if (sender._id.indexOf('<%= ugtxtOrganizacionCompras.ClientID %>', 0) != -1) {
                dropdown = $find("<%= ugtxtOrganizacionCompras.ClientID %>");
            }
            else if (sender._id.indexOf('<%= ugtxtCentros.ClientID %>', 0) != -1) {
                dropdown = $find("<%= ugtxtCentros.clientID %>");
            }
            if (dropdown != null) {
                //Sets the text of the value display to the product name of the selected row
                dropdown.set_currentValue(text, true);

                dropdown.closeDropDown()

                var CodCentro = "";
                var CodOrgCompras = "";
                if (sender._id.indexOf('<%= ugtxtOrganizacionCompras.clientID %>', 0) != -1) {
                    var CodCell = row.get_cell(0);
                    //Gets the text in the cell
                    var Cod = CodCell.get_text();
                    ugtxtOrganizacionComprasSelectionChanged(Cod);
                }
                if (sender._id.indexOf('<%= ugtxtCentros.clientID %>', 0) != -1) {
                    var CodCell = row.get_cell(0);
                    //Gets the text in the cell
                    CodCentro = CodCell.get_text();                    
                }
                if ($find('<%= ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg").clientID %>') != null) {
                    if ($find('<%= ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg").clientID %>').get_behaviors().get_selection().get_selectedRows(0).getItem(0) != null) {
                        CodOrgCompras = $find('<%= ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg").clientID %>').get_behaviors().get_selection().get_selectedRows(0).getItem(0).get_cell(0).get_text();
                    }
                } else if ($('#lblOrgComprasNombre')) {
                    var sOrgCompL = $('#lblOrgComprasNombre').html();
                    var num = sOrgCompL.indexOf(" - ");
                    CodOrgCompras = sOrgCompL.substring(0, num);
                }

                //Saco la UON para la OrgCompras + Centro
                var params = { sCodOrgCompras: CodOrgCompras, sCodCentro: CodCentro };
                $.when($.ajax({
                    type: "POST",
                    url: RutaFS + '_Common/BuscadorArticulos.aspx/DevolverUONOrgCompras',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(params),
                    dataType: "json",
                    async: false
                })).done(function (msg) {
                    var respuesta = msg.d;
                    CargarTablaAtributos(respuesta[0], respuesta[1], respuesta[2], 1);
                });                
            }
        }

        function ugtxtOrganizacionComprasValueChanging(sender, e) {

            e.set_cancel(true);

        }

        // The client event ‘ValueChanging' takes two parameters sender and e
        // sender  is the object which is raising the event
        // e is the DropDownEditEventArgs
        function ugtxtCentros_ValueChanging(sender, e) {

            e.set_cancel(true);

        }

        //''' <summary>
        //''' Limpia centro y proveedor
        //''' </summary>
        //''' <remarks>Llamada desde: WebDataGrid_RowSelectionChanged ; Tiempo máximo: 0,2</remarks>
        function ugtxtOrganizacionComprasSelectionChanged(cod) {
            var ugtxtCentros = $find("<%= ugtxtCentros.clientID %>");
            //Vaciar texto
            ugtxtCentros.set_currentValue('', true);
            //Cargar nuevos datos
            ugtxtCentros.loadItems(cod);

            if (document.getElementById("txtProveedor")) {
                document.getElementById("hProveedor").value = "";
                document.getElementById("txtProveedor").value = ""; 
            }

        }

        //''' <summary>
        //''' Limpiar el Proveedor
        //''' </summary>
        //''' <remarks>Llamada desde: imgProveedor/onclientclick ; Tiempo máximo: 0,2</remarks>
        function AbrirProveedor() {
            var ValorOrgCompras = '';
            buscar();
            try {
                if ($find('<%= ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg").clientID %>') != null) {
                    if ($find('<%= ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg").clientID %>').get_behaviors().get_selection().get_selectedRows(0).getItem(0) != null) {
                        ValorOrgCompras = $find('<%= ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg").clientID %>').get_behaviors().get_selection().get_selectedRows(0).getItem(0).get_cell(0).get_text();
                    }
                } else if ($('#lblOrgComprasNombre')) {
                    var sOrgCompL = $('#lblOrgComprasNombre').html();
                    var num = sOrgCompL.indexOf(" - ");
                    ValorOrgCompras = sOrgCompL.substring(0, num);
                }
                ValorOrgCompras = '&ValorOrgCompras=' + ValorOrgCompras;

            } catch (err) { }

            var newWindow = window.open(RutaFS + "_common/BuscadorProveedores.aspx?PM=true&desde=BuscadorArt&IDCONTROL=txtProveedor&HiddenFieldID=hProveedor" + ValorOrgCompras, "_blank", "width=830,height=530,status=yes,resizable=no,top=150,left=150");
            newWindow.focus();
            return false;
        }

        /*
        ''' <summary>
        ''' Muestra la pantalla de selección de Adjudicaciones
        ''' </summary>
        ''' <param name="sCod">Articulo</param>        
        ''' <param name="sInstanciaMoneda">Moneda Instancia</param>       
        ''' <remarks>Llamada desde: uwgArticulos_CellClickHandler ; Tiempo máximo: 0</remarks>*/
        function MostrarUltimasAdjucaciones(sCod, sInstanciaMoneda) {
            var CodProveedor = '';
            var ValorOrgCompras = '';
            var IDFPago = '';
            var IDFPagoHidden = '';

            sOrigenLlamada = ""
            if (document.forms["frmArticulos"].elements["hTipoSolicitud"]) {
                sOrigenLlamada = document.forms["frmArticulos"].elements["hTipoSolicitud"].value;
                if (sOrigenLlamada == 9) {  //PedidoNegociado
                    if (document.getElementById("hProveedor")) {
                        CodProveedor = "&CodProveedor=" + document.getElementById("hProveedor").value;
                    }

                    try {
                        if ($find('<%= ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg").clientID %>') != null) {
                            if ($find('<%= ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg").clientID %>').get_behaviors().get_selection().get_selectedRows(0).getItem(0) != null) {
                                ValorOrgCompras = $find('<%= ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg").clientID %>').get_behaviors().get_selection().get_selectedRows(0).getItem(0).get_cell(0).get_text();
                            }
                        } else if ($('#lblOrgComprasNombre')) {
                            var sOrgCompL = $('#lblOrgComprasNombre').html();
                            var num = sOrgCompL.indexOf(" - ");
                            ValorOrgCompras = sOrgCompL.substring(0, num);
                        }
                        ValorOrgCompras = '&CodOrgCompra=' + ValorOrgCompras;
                    } catch (err) { }

                    IDFPago = "&IDFPago=" + document.getElementById("IDFPago").value;
                    IDFPagoHidden = "&IDFPagoHidden=" + document.getElementById("IDFPagoHidden").value;
                }
            }

            var newWindow = window.open("UltimasAdjudicaciones.aspx?sCod=" + sCod + "&sInstanciaMoneda=" + sInstanciaMoneda + CodProveedor + ValorOrgCompras + IDFPago + IDFPagoHidden, "_blank", "width=900,height=300,status=yes,resizable=no,top=200,left=100")
            newWindow.focus();
        }

        function buscar() {          
            document.getElementById("txtDesdeFila").value = 0

            try {
                if ($find('<%= ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg").clientID %>') != null) {
                    if ($find('<%= ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg").clientID %>').get_behaviors().get_selection().get_selectedRows(0).getItem(0) != null) {
                        var oWdg_Value = $find('<%= ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg").clientID %>').get_behaviors().get_selection().get_selectedRows(0).getItem(0).get_cell(0).get_text();
                    }
                    else {
                        oWdg_Value = '';
                    }
                } else if ($('#lblOrgComprasNombre')) {
                    var sOrgCompL = $('#lblOrgComprasNombre').html();
                    var num = sOrgCompL.indexOf(" - ");
                    oWdg_Value = sOrgCompL.substring(0, num);
                }
                document.forms["frmArticulos"].elements["txtCodOrgCompras"].value = oWdg_Value;
            } catch (err) { }
            try {
                if ($find('<%= ugtxtCentros.Controls(0).Controls(0).FindControl("ugtxtCentros_wdg").clientID %>') != null) {
                    if ($find('<%= ugtxtCentros.Controls(0).Controls(0).FindControl("ugtxtCentros_wdg").clientID %>').get_behaviors().get_selection().get_selectedRows(0).getItem(0) != null) {
                        oWdg_Value = $find('<%= ugtxtCentros.Controls(0).Controls(0).FindControl("ugtxtCentros_wdg").clientID %>').get_behaviors().get_selection().get_selectedRows(0).getItem(0).get_cell(0).get_text();
                    }
                    else {
                        oWdg_Value = '';
                    }
                } else if ($('#lblCentroNombre')) {
                    sOrgCompL = $('#lblCentroNombre').html();
                    num = sOrgCompL.indexOf(" - ");
                    oWdg_Value = sOrgCompL.substring(0, num);
                }
                document.forms["frmArticulos"].elements["txtCodCentro"].value = oWdg_Value;

                var CodCentro = document.forms["frmArticulos"].elements["txtCodCentro"].value;
                var CodOrgCompras = document.forms["frmArticulos"].elements["txtCodOrgCompras"].value;
                //Saco la UON para la OrgCompras + Centro
                var params = { sCodOrgCompras: CodOrgCompras, sCodCentro: CodCentro };
                $.when($.ajax({
                    type: "POST",
                    url: RutaFS + '_Common/BuscadorArticulos.aspx/DevolverUONOrgCompras',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(params),
                    dataType: "json",
                    async: false
                })).done(function (msg) {
                    var respuesta = msg.d;
                    GuardarDatosAtribUON(respuesta[0], respuesta[1], respuesta[2]);
                });

            } catch (err) { }

            try {
                var UnOrg
                if (document.forms["frmArticulos"].elements["txtUnidadOrganizativa"]) {
                    UnOrg = document.forms["frmArticulos"].elements["txtUnidadOrganizativa"].value;
                } else if ($('#lblUnidadOrganizativaSeleccionada')) {
                    UnOrg = $('#lblUnidadOrganizativaSeleccionada').html();
                }
                var sUon = new Array();
                var Result = new Array();
                for (j = 1; j <= 3; j++) {
                    sUon[j] = "";
                }
                Result = UnOrg.split(" - ");
                for (i = 0; i < Result.length - 1; i++) {
                    sUon[i + 1] = Result[i];
                }
                GuardarDatosAtribUON(sUon[1], sUon[2], sUon[3]);
            } catch (err) { };

            var btnBuscar = $('[id$=btnBuscar]').attr('id');
            __doPostBack(btnBuscar, '');
        }

        //-------------------------------//
        //-------Tabla de atributos------//
        //-------------------------------//

        function CargarTablaAtributos(sUON1, sUON2, sUON3, cambioUON) {

            if (sUON3 == undefined || sUON3 == null)
                sUON3 = ""
            if (sUON2 == undefined || sUON2 == null)
                sUON2 = ""
            if (sUON1 == undefined || sUON1 == null)
                sUON1 = ""
            var params = { UON1: sUON1, UON2: sUON2, UON3: sUON3 };
            $.when($.ajax({
                type: "POST",
                url: RutaFS + '_Common/BuscadorArticulos.aspx/CargarAtributosUON',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(params),
                dataType: "json",
                async: false
            })).done(function (msg) {
                var oAtributos = msg.d;
                if (cambioUON == 1) {
                    gAtributosUON = oAtributos;
                    localStorage.setItem("gAtribsValor", JSON.stringify(gAtributosUON));
                }
                gAtributosUON = JSON.parse(localStorage.getItem("gAtribsValor"));
                if (gAtributosUON.length == 0) { //Si no hay atributos ocultamos el Div de Atributos    
                    $('[id$=divBuscAtrib]').hide()
                    $('[id$=lblBuscPorAtributos]').hide()
                } else {
                    $('[id$=divBuscAtrib]').show()
                    $('[id$=lblBuscPorAtributos]').show()
                }
                //Se eliminan las filas si las hubiese de la tabla(menos la cabecera)
                var table = $("#tblAtributosUON")
                table.find('tr:not(:first)').remove();
                $(gAtributosUON).each(function () {
                    var item = this;
                    if (typeof this.Intro == 'string') {
                        this.intro = strToNum(this.Intro)
                    }
                    if (typeof this.Tipo == 'string') {
                        this.Tipo = strToNum(this.Tipo)
                    }
                    switch (this.Intro) {
                        case 1:
                            //Opcion Lista
                            {
                                $("#tmplCombo").tmpl(this).appendTo("#tblAtributosUON");

                                var items = "{"
                                //vamos componiendo con los valores de la combo el objeto Json
                                for (i = 0; i <= item.Valores.length - 1; i++) {
                                    items += '"' + i + '": { "value":"' + item.Valores[i].Orden + '", "text": "' + item.Valores[i].Valor + '" }, '
                                }
                                items = items.substr(0, items.length - 2) //quitamos la ultima coma que sobra
                                items += "}"

                                var optionsPermiso = { valueField: "value", textField: "text", adjustWidthToSpace: true, dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: false, data: $.parseJSON(items) };
                                $('#cboAtrib_' + item.Id).fsCombo(optionsPermiso);
                                $('#cboAtrib_' + item.Id).fsCombo('selectValue', item.Orden);

                                break
                            }
                        default:
                            {
                                switch (this.Tipo) {
                                    case 1: //Texto
                                        $("#tmplCampoTexto").tmpl(this).appendTo("#tblAtributosUON");
                                        break
                                    case 2: //Numerico
                                        $("#tmplCampoNumerico").tmpl(this).appendTo("#tblAtributosUON");
                                        break
                                    case 3: //Fecha
                                        $("#tmplCampoFecha").tmpl(this).appendTo("#tblAtributosUON");
                                        var item = this;
                                        var idioma = TextosJScript[2];
                                        $('#inputFechaAtrib_' + item.Id).datepicker({
                                            showOn: 'both',
                                            buttonImage: ruta + 'images/colorcalendar.png',
                                            buttonImageOnly: true,
                                            buttonText: '',
                                            dateFormat: UsuMask.replace('MM', 'mm').replace('yyyy', 'yy'),
                                            showAnim: 'slideDown'
                                        });
                                        $.datepicker.setDefaults($.datepicker.regional[idioma]);
                                        if (item.Valor != null && item.Valor != undefined) {
                                            if (item.Valor.toString().indexOf('Date') != -1) //Viene de BD
                                                $('#inputFechaAtrib_' + item.Id).datepicker('setDate', eval("new " + item.Valor.slice(1, -1)));
                                            else
                                                $('#inputFechaAtrib_' + item.Id).datepicker('setDate', eval("new Date('" + item.Valor + "')"));
                                        }
                                        break
                                    case 4: //Boolean
                                        $("#tmplCombo").tmpl(this).appendTo("#tblAtributosUON");
                                        var item = this;
                                        var items = "{"
                                        //vamos componiendo con los valores de la combo el objeto Json
                                        for (i = 1; i >= 0; i--) {
                                            items += '"' + i + '": { "value":"' + i + '", "text": "' + TextosBoolean[i] + '" }, '
                                        }
                                        items = items.substr(0, items.length - 2) //quitamos la ultima coma que sobra
                                        items += "}"

                                        var optionsPermiso = { valueField: "value", textField: "text", adjustWidthToSpace: true, dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: false, data: $.parseJSON(items) };
                                        $('#cboAtrib_' + item.Id).fsCombo(optionsPermiso);
                                        $('#cboAtrib_' + item.Id).fsCombo('selectValue', item.Valor);

                                        break
                                }
                            }
                    }
                })
            });
        }

        function strToNum(strNum) {
            if (strNum == undefined || strNum == null || strNum === '')
                strNum = '0'

            while (strNum.indexOf(UsuNumberGroupSeparator) >= 0) {
                strNum = strNum.replace(UsuNumberGroupSeparator, "")
            }
            strNum = strNum.replace(UsuNumberDecimalSeparator, ".")
            return parseFloat(strNum)
        }

        //Funcion que valida si es un numero valido y si lo es lo formatea
        function validaFloat(sender, old) {
            gModificadaDistribucionPanelProv = true
            var numero = sender.value;
            var exp
            if (UsuNumberDecimalSeparator == ',') {
                exp = /^([0-9])*[.]?([0-9])*[,]?[0-9]*$/
            } else {
                exp = /^([0-9])*[,]?([0-9])*[.]?[0-9]*$/ ///^([0-9])*[.]?[0-9]*$/
            }

            if (!exp.test(numero)) {
                alert(TextosJScript[1].toString().replace("XXXXX", numero));
                sender.value = ''
                gBoolValidaFloat = false
                return false;
            } else {
                if (numero != "") {
                    numero = strToNum(numero)
                    numero = formatNumber(numero)
                    sender.value = numero
                }
                gBoolValidaFloat = true
                return true
            }
        }
        function formatNumber(num) {
            var result;
            if (num == undefined || num == null || num === '') {
                result = "";
            } else {
                num = parseFloat(num)
                result = num.toFixed(UsuNumberNumDecimals);
                result = addSeparadorMiles(result.toString())
                var result1 = result.substring(0, result.length - parseInt(UsuNumberNumDecimals) - 1)
                var result2 = result.substring(result.length - parseInt(UsuNumberNumDecimals), result.length)
                result = result1 + UsuNumberDecimalSeparator + result2
            }
            return result
        }

        function addSeparadorMiles(nStr) {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + UsuNumberGroupSeparator + '$2');
            }
            var retorno = x1 + x2;
            return retorno
        }
        //Funcion que valida la fecha que se introduce en los atributos
        //param=sender:Input de la fecha
        function validaDate(sender) {
            var caracterSeparador;
            var anyo;
            var mes;
            var dia;
            var valor = sender.value;
            var fechaArr = new Array();
            //Comprobamos que la fecha introducida tiene el separador correcto
            caracterSeparador = (valor.indexOf(UsuMaskSeparator) > -1 ? 1 : 0)
            if (caracterSeparador == 1) {
                fechaArr = valor.split(UsuMaskSeparator);
                //Recogemos el año, mes, dia dependiendo del formato de fecha del usuario
                switch (UsuMask) {
                    case 'dd' + UsuMaskSeparator + 'MM' + UsuMaskSeparator + 'yyyy':
                        anyo = fechaArr[2];
                        mes = fechaArr[1];
                        dia = fechaArr[0];
                        break;
                    case 'MM' + UsuMaskSeparator + 'dd' + UsuMaskSeparator + 'yyyy':
                        anyo = fechaArr[2];
                        mes = fechaArr[0];
                        dia = fechaArr[1];
                        break;
                    case 'yyyy' + UsuMaskSeparator + 'MM' + UsuMaskSeparator + 'dd':
                        anyo = fechaArr[0];
                        mes = fechaArr[1];
                        dia = fechaArr[2];
                        break;
                    default:
                        //No es un formato valido
                        sender.value = ''
                        return false;
                }
            } else {
                //No es un formato valido
                sender.value = ''
                return false;
            }

            try {
                //Creamos el objeto fecha y comprobamos que el año,mes, dia del objeto creado es el mismo que lo introducido en el input
                var newDate = new Date(anyo, mes - 1, dia);
                if (!newDate || newDate.getFullYear() == anyo && newDate.getMonth() == mes - 1 && newDate.getDate() == dia) {
                    return true;
                } else {
                    sender.value = ''
                    return false;
                }
            }
            catch (err) {
                sender.value = ''
                return false;
            }

        }

        function GuardarDatosAtribUON(sUON1, sUON2, sUON3) {
            //Guardamos los atributos de UON
            var tblAtributosUON = $("[id$=tblAtributosUON]")
            var x = 0;
            gAtributosUON = JSON.parse(localStorage.getItem("gAtribsValor"));
            gAtributosUON.splice(0, gAtributosUON.length);
            var body = tblAtributosUON.children()
            body.children().each(function (index) {
                var Id, Cod, Den, Tipo, Intro, Valor, Orden;
                //recorremos la tabla de atributos de UON
                $(this).children().each(function (index2) {
                    if (x > 0) {
                        switch (index2) {
                            case 0: //Id del atributo
                                Id = $(this).text();
                                break
                            case 1: //Codigo del atributo
                                Cod = $(this).text();
                                break
                            case 2: //Denominacion del atributo
                                Den = $(this).text();
                                break
                            case 3: //Tipo de dato
                                Tipo = $(this).text();
                                break
                            case 4: //Si es lista o no
                                Intro = $(this).text();
                                break
                            case 5: //Valor
                                Orden = -1 //Inicializo el orden, solo si es lista entrara a recoger el orden del valor seleccionado
                                switch (Intro) {
                                    case '1':
                                        Valor = $(this).find('.fsCombo').fsCombo('getSelectedText');
                                        Orden = $(this).find('.fsCombo').fsCombo('getSelectedValue');
                                        if (Orden === '')
                                            Orden = -1 //Si no se selecciona nada, se le asigna -1 por ser un campo numerico
                                        break
                                    default:
                                        switch (Tipo) {
                                            case '1': //Texto
                                                Valor = $(this).find('textarea').val()
                                                break
                                            case '2': //Numerico
                                                Valor = $(this).find('INPUT').val()
                                                if (Valor != "") {
                                                    Valor = strToNum(Valor)
                                                }
                                                break
                                            case '3': //Fecha
                                                Valor = $('#inputFechaAtrib_' + Id).datepicker('getDate');
                                                if (Valor != null) {
                                                    //Valor = Valor.toString()
                                                }
                                                break
                                            case '4': //Boolean
                                                Valor = $(this).find('.fsCombo').fsCombo('getSelectedValue');
                                                if (Valor == undefined)
                                                //Si no hay nada seleccionado en la lista
                                                    Valor = ''
                                                break
                                        }
                                        break
                                }
                        }
                    }
                })
                if (x > 0) { //Para evitar la cabecera de la tabla
                    Atributo = { Id: Id, Tipo: Tipo, Codigo: Cod, Denominacion: Den, Valor: Valor, Intro: Intro }
                    gAtributosUON.push(Atributo)
                }
                x += 1
            })
            localStorage.setItem("gAtribsValor", JSON.stringify(gAtributosUON));
            GuardarObjetoAtributos(sUON1, sUON2, sUON3)
        }
        function GuardarObjetoAtributos(sUON1, sUON2, sUON3) {
            var params = { objAtributosUON: gAtributosUON, UON1: sUON1, UON2: sUON2, UON3: sUON3 };
            $.when($.ajax({
                type: "POST",
                url: RutaFS + '_Common/BuscadorArticulos.aspx/GuardarObjetoAtributosUON',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(params),
                dataType: "json",
                async: false
            }));
        }
    </script>

    <script type="text/javascript">
        var p, pp;
        var isPedidoExpressNegociable;
        //var sProveedor = "";
        function aceptar() {
            //rue 310309	chkcatalogados
            //Descripción:	Añade los datos del item seleccionado	
            //Param. Salida: -
            //LLamada:	Desde el boton aceptar de esta pagina
            //Tiempo:	0
            //Si el usuario
            p = window.opener;
            pp = p.opener;
            sDataEntryOrgComprasFORM = null; sDataEntryCentroFORM = null; sCodOrgComprasOculto = ''; sCodCentroOculto = '';
            sDataEntryUnidadOrganizativaFORM = null;
            if (pp) {
                sDataEntryOrgComprasFORM = pp.sDataEntryOrgComprasFORM;
                sCodOrgComprasOculto = pp.sCodOrgComprasFORM;
                sDataEntryCentroFORM = pp.sDataEntryCentroFORM;
                sCodCentroOculto = pp.sCodCentroFORM;
                sDataEntryUnidadOrganizativaFORM = pp.sDataEntryUnidadOrganizativaFORM;
            }

            oGrid = $find("wdgArticulos");
            oRow = oGrid.get_behaviors().get_selection().get_selectedRows();
            if (oGrid.get_behaviors().get_selection().get_selectedRows().get_length() == 0)
                return
            oCell = oRow.getItem(0).get_cellByColumnKey("GMN1");
            sGMN1 = oCell.get_value()
            oCell = oRow.getItem(0).get_cellByColumnKey("GMN2")
            sGMN2 = oCell.get_value()
            oCell = oRow.getItem(0).get_cellByColumnKey("GMN3")
            sGMN3 = oCell.get_value()
            oCell = oRow.getItem(0).get_cellByColumnKey("GMN4")
            sGMN4 = oCell.get_value()
            oCell = oRow.getItem(0).get_cellByColumnKey("COD")
            sCod = oCell.get_value()
            oCell = oRow.getItem(0).get_cellByColumnKey("DEN")
            sDen = oCell.get_value()
            oCell = oRow.getItem(0).get_cellByColumnKey("DENGMN")
            sGMNDen = oCell.get_value()
            oCell = oRow.getItem(0).get_cellByColumnKey("GENERICO")
            bArticuloGenerico = oCell.get_value()
            oCell = oRow.getItem(0).get_cellByColumnKey("UNI")
            sUnidades = oCell.get_value()
            oCell = oRow.getItem(0).get_cellByColumnKey("DENUNI")
            sDenUnidad = oCell.get_value()
            oCell = oRow.getItem(0).get_cellByColumnKey("NUMDEC")
            sNumeroDecimales = oCell.get_value()
            if (sNumeroDecimales == "") {
                sNumeroDecimales = null;
            }
            oCell = oRow.getItem(0).get_cellByColumnKey("CONCEPTO")
            sConcepto = oCell.get_value()
            oCell = oRow.getItem(0).get_cellByColumnKey("ALMACENAR")
            sAlmacenamiento = oCell.get_value()
            oCell = oRow.getItem(0).get_cellByColumnKey("RECEPCIONAR")
            sRecepcion = oCell.get_value()
            oCell = oRow.getItem(0).get_cellByColumnKey("TIPORECEPCION")
            sTipoRecepcion = oCell.get_value()
            oCell = oRow.getItem(0).get_cellByColumnKey("FAVORITO")
            sFavorito = oCell.get_value()
            //Validaciones Concepto
            if (document.getElementById("hConcepto").value != '') {
                if ((document.getElementById("hConcepto").value == '1') && (sConcepto == '0')) {
                    alert(mensajeConceptoGasto);
                    return false;
                }
                if ((document.getElementById("hConcepto").value == '0') && (sConcepto == '1')) {
                    alert(mensajeConceptoInversion);
                    return false;
                }
            }
            //Validaciones Almacenamiento
            if (document.getElementById("hAlmacenamiento").value != '') {
                if ((document.getElementById("hAlmacenamiento").value == '1') && (sAlmacenamiento == '0')) {
                    alert(mensajeNoAlmacenable);
                    return false;
                }

                if ((document.getElementById("hAlmacenamiento").value == '0') && (sAlmacenamiento == '1')) {
                    alert(mensajeAlmacenamientoObligatorio);
                    return false;
                }
            }

            //Validaciones Recepción
            if (document.getElementById("hRecepcion").value != '') {
                if ((document.getElementById("hRecepcion").value == '1') && (sRecepcion == '0')) {
                    alert(mensajeNoRecepcionable);
                    return false;
                }

                if ((document.getElementById("hRecepcion").value == '0') && (sRecepcion == '1')) {
                    alert(mensajeRecepcionObligatoria);
                    return false;
                }
            }


            sdesde = ""
            if (document.forms["frmArticulos"].elements["desde"])
                sdesde = document.forms["frmArticulos"].elements["desde"].value;
            
            sOrigenLlamada = ""
            if (document.forms["frmArticulos"].elements["hTipoSolicitud"]) {
                sOrigenLlamada = document.forms["frmArticulos"].elements["hTipoSolicitud"].value;
                if (sOrigenLlamada == 9 || isPedidoExpressNegociable) { //9=PedidoNegociado
                    if ((oRow.getItem(0).get_cellByColumnKey("PREC_ULT_ADJ").get_value() !== null) && (oRow.getItem(0).get_cellByColumnKey("PREC_ULT_ADJ").get_value() != "")) {
                        oCell = oRow.getItem(0).get_cellByColumnKey("PREC_ULT_ADJ");
                        lPrecioUltADJ = oCell.get_value();
                        oCell = oRow.getItem(0).get_cellByColumnKey("UNI_ULT_ADJ");
                        sUnidades = oCell.get_value();
                        oCell = oRow.getItem(0).get_cellByColumnKey("DENUNI_ULT_ADJ")
                        sDenUnidad = oCell.get_value()
                        oCell = oRow.getItem(0).get_cellByColumnKey("CODPROV_ULT_ADJ")
                        sCODPROVUltADJ = oCell.get_value()
                        oCell = oRow.getItem(0).get_cellByColumnKey("DENPROV_ULT_ADJ")
                        sDENPROVUltADJ = oCell.get_value()
                        oCell = oRow.getItem(0).get_cellByColumnKey("MON_ULT_ADJ")
                        sMonPROVUltADJ = oCell.get_value()
                        oCell = oRow.getItem(0).get_cellByColumnKey("DENMON_ULT_ADJ")
                        sDenMonPROVUltADJ = oCell.get_value()
                        if ((document.getElementById("IDFPago").value != "") || (document.getElementById("IDFPagoHidden").value != "")) {
                            oCell = oRow.getItem(0).get_cellByColumnKey("PAG_ULT_ADJ");
                            sFPagoPROVUltADJ = oCell.get_value();
                            oCell = oRow.getItem(0).get_cellByColumnKey("DENPAG_ULT_ADJ");
                            sDenFPagoPROVUltADJ = oCell.get_value();
                        };
                    } else {
                        lPrecioUltADJ = null;
                        //                        sUnidades = null;
                        //                        sDenUnidad = null;
                        sCODPROVUltADJ = null;
                        sDENPROVUltADJ = null;
                        sMonPROVUltADJ = null;
                        sDenMonPROVUltADJ = null;
                        sFPagoPROVUltADJ = null;
                        sDenFPagoPROVUltADJ = null;
                    };
                };
            }
            //Validaciones Pedido negociado (o pedido express con parámetro mostrar negociados)
            if (sOrigenLlamada == 9 || isPedidoExpressNegociable) { //9=PedidoNegociado
                oArt = p.fsGeneralEntry_getById(document.forms["frmArticulos"].elements["txtArtClientID"].value)

                if (bArticuloGenerico && !isPedidoExpressNegociable) {
                    alert(mensajeNegociadoNoAcepGenerico);
                    return false;
                } else {
                    if (sMonPROVUltADJ != null && sMonPROVUltADJ != "" && oArt.MonSolicit != sMonPROVUltADJ) {
                        var mensaje = mensajeNegociadoNoAcepCambioMoneda;
                        if (oArt.MonSolicit) {
                            mensaje = mensaje.replace("###", "(" + oArt.MonSolicit + ")");
                        } else {
                            mensaje = mensaje.replace("###", "");
                        }
                        mensaje = mensaje.replace("@@@", sMonPROVUltADJ);
                        alert(mensaje);
                        return false;
                    }
                    //Cambio de proveedor
                    if (document.getElementById("IDCodProve")) {
                        if (sOrigenLlamada == 9) {  //PedidoNegociado
                            if (document.getElementById("hProveedorIniNegociado").value != "") {
                                if (document.getElementById("hProveedorIniNegociado").value != sCODPROVUltADJ) {
                                    if (document.getElementById("Rellenos").value > 0) {
                                        var mensaje = mensajeNegociadoNoAcepCambioProve
                                        mensaje = mensaje.replace("###", sCODPROVUltADJ)
                                        confirm(mensaje)
                                        return false;
                                    }
                                }
                            }
                        }

                        //si proveedor del formulario origen no estaba vacÃ­o o si era diferente al actual 
                        //borramos (previa confirmaciÃ³n) los artÃ­culos adjudicados a otro proveedor.
                        //esto aplicado sÃ³lo si tenemos activada la visualizaciÃ³n de artÃ­culos negociados en pedidos express
                        if (isPedidoExpressNegociable) {
                            sProveedor = document.getElementById("hProveedorIniNegociado").value;

                            if (document.getElementById("Rellenos").value > 0 && isPedidoExpressNegociable && sProveedor != null && sProveedor != document.getElementById("hProveedor").value) {

                                if (p.numLineasDesglose() > 1) {
                                    var mensaje = mensajeExpressCambioProveedor;
                                    //mostrar mensaje advertencia
                                    mensaje = mensaje.replace("###", sProveedor);
                                    confirm2(mensaje);
                                    return false;
                                } else {
                                    //si es un cambio de articulo, cambiamos el proveedor directamente sin confirmar
                                    sProveedor = document.getElementById("hProveedor").value;
                                    document.getElementById("hProveedorIniNegociado").value = sProveedor;
                                    aceptar();
                                }
                            }
                        }
                    }
                }
            };
            //Agregar el artculo a favoritos en el caso de que esté marcado el check
            if (document.getElementById("chkAnyadirAutoFav")){
                if (document.getElementById("chkAnyadirAutoFav").checked != '0') {
                    if (sFavorito == 0) {
                        var sAccion = "Insertar"
                        var sArtCod = oRow.getItem(0).get_cellByColumnKey("COD").get_value();
                        CambiarArticulosFavoritos(sArtCod, sAccion)
                    };
                };
            };
            if (sdesde == "") {
                oArt = p.fsGeneralEntry_getById(document.forms["frmArticulos"].elements["txtArtClientID"].value);
                if ((oArt.idEntryPREC) || (oArt.idEntryPROV)) {
                    oCell = oRow.getItem(0).get_cellByColumnKey("PREC_ULT_ADJ")
                    lPrecioUltADJ = oCell.get_value();
                    oCell = oRow.getItem(0).get_cellByColumnKey("CODPROV_ULT_ADJ")
                    sCODPROVUltADJ = oCell.get_value()
                    oCell = oRow.getItem(0).get_cellByColumnKey("DENPROV_ULT_ADJ")
                    sDENPROVUltADJ = oCell.get_value()
                }
                var sCodOrgCompras = ''; var sDenOrgCompras = '';
                var sCodCentro = ''; var sDenCentro = ''; bUsar_OrgCompras = false;
                if ((oArt.idDataEntryDependent) || (oArt.OrgComprasDependent.value != '') || (sDataEntryOrgComprasFORM) || ((sCodOrgComprasOculto) && (sCodOrgComprasOculto != '')))
                    if ((oArt.idDataEntryDependent2) || (oArt.CentroDependent.value != '') || (sDataEntryCentroFORM) || ((sCodCentroOculto) && (sCodCentroOculto != ''))) {
                        bUsar_OrgCompras = true;
                        oCell = oRow.getItem(0).get_cellByColumnKey("CODORGCOMPRAS")
                        sCodOrgCompras = oCell.get_value()
                        oCell = oRow.getItem(0).get_cellByColumnKey("DENORGCOMPRAS")
                        sDenOrgCompras = oCell.get_value()
                        oCell = oRow.getItem(0).get_cellByColumnKey("CODCENTRO")
                        sCodCentro = oCell.get_value()
                        oCell = oRow.getItem(0).get_cellByColumnKey("DENCENTRO")
                        sDenCentro = oCell.get_value()
                    }

                oArt.articuloGenerico = bArticuloGenerico;
                oArt.articuloCodificado = true;
                oArt.Concepto = sConcepto;
                oArt.Almacenamiento = sAlmacenamiento;
                oArt.Recepcion = sRecepcion;

                if (oArt.tipoGS == 118)// Denominacion	
                {
                    oArt.setValue(sDen)
                    oArt.setDataValue(sDen)
                    oArt.DenArticuloModificado = false
                    oArt.codigoArticulo = sCod;

                    oFSEntryCod = null;
                    idEntryCod = p.calcularIdDataEntryCelda(document.forms["frmArticulos"].elements["txtArtClientID"].value, oArt.nColumna - 1, oArt.nLinea, 0);
                    if (idEntryCod)
                        oFSEntryCod = p.fsGeneralEntry_getById(idEntryCod);
                    if ((oFSEntryCod) && (oFSEntryCod.tipoGS == 119)) {
                        oFSEntryCod.articuloGenerico = bArticuloGenerico;
                        oFSEntryCod.Concepto = sConcepto;
                        oFSEntryCod.Almacenamiento = sAlmacenamiento;
                        oFSEntryCod.Recepcion = sRecepcion;
                        oFSEntryCod.setValue(sCod)
                        oFSEntryCod.setDataValue(sCod)

                        if (oFSEntryCod.idEntryCANT) {
                            if (oFSEntryCod.idEntryCANT != '') {
                                oCantidad = p.fsGeneralEntry_getById(oFSEntryCod.idEntryCANT);
                                if (oCantidad) {
                                    if (oFSEntryCod.idEntryUNI) {
                                        if (oFSEntryCod.idEntryUNI != '') {
                                            oUnidad = p.fsGeneralEntry_getById(oFSEntryCod.idEntryUNI);
                                            if (oUnidad) {
                                                if ((document.forms["frmArticulos"].elements["sCodUni"].value != "") && (document.forms["frmArticulos"].elements["sCodUni"].value != oUnidad.getDataValue())) {
                                                    oCantidad.NumeroDeDecimales = document.forms["frmArticulos"].elements["sNumDecUni"].value
                                                    oCantidad.UnidadOculta = document.forms["frmArticulos"].elements["sDenUni"].value
                                                } else {
                                                    oCantidad.NumeroDeDecimales = sNumeroDecimales;
                                                    oCantidad.UnidadOculta = sDenUnidad
                                                }
                                            }
                                        }
                                    }
                                    p.limiteDecimalesTextChanged(oCantidad.Editor, oCantidad.Editor.text, null)
                                    oCantidad.TipoRecepcion = sTipoRecepcion
                                    if (sTipoRecepcion == "1") {
                                        oCantidad.setValue("1");
                                    }
                                }
                            }
                        }

                        //Aqui vamos a rellenar el IdEntryUniPedido del codigo de articulo pq si esta invisible se pierde
                        if (oFSEntryCod.IdEntryUniPedido) {
                            if (oFSEntryCod.IdEntryUniPedido != '') {
                                oUnidad = p.fsGeneralEntry_getById(oFSEntryCod.IdEntryUniPedido);
                                if (oUnidad) {
                                    if (document.forms["frmArticulos"].elements["sCodUni"].value != "") {
                                        oUnidad.setDataValue(document.forms["frmArticulos"].elements["sCodUni"].value);
                                        oUnidad.setValue(document.forms["frmArticulos"].elements["sDenUni"].value);
                                    } else {
                                        oUnidad.setDataValue(sUnidades);
                                        oUnidad.setValue(sDenUnidad);
                                    }
                                }
                                else {
                                    if (document.forms["frmArticulos"].elements["sCodUni"].value != "")
                                        oFSEntryCod.UnidadPedidoDependent.value = document.forms["frmArticulos"].elements["sCodUni"].value;
                                    else
                                        oFSEntryCod.UnidadPedidoDependent.value = sUnidades;
                                }
                            }
                        }

                        //Aqui vamos a rellenar el identryuni del codigo de articulo pq si esta invisible se pierde
                        if (oFSEntryCod.idEntryUNI)
                            if (oFSEntryCod.idEntryUNI != '') {
                                oUnidad = p.fsGeneralEntry_getById(oFSEntryCod.idEntryUNI);
                                if (oUnidad) {
                                    if ((document.forms["frmArticulos"].elements["sCodUni"].value != "") && (document.forms["frmArticulos"].elements["sCodUni"].value != oUnidad.getDataValue())) {
                                        //Pone el valor seleccionado desde Ultimas adjudicaciones.
                                        if (oUnidad.readOnly) {
                                            msg = document.forms["frmArticulos"].elements["sMensaje1"].value.replace("#", "\n")
                                            msg = msg.replace("#", "\n")
                                            alert(msg + document.forms["frmArticulos"].elements["sCodUni"].value)
                                        }
                                        oUnidad.setDataValue(document.forms["frmArticulos"].elements["sCodUni"].value);
                                        oUnidad.setValue(document.forms["frmArticulos"].elements["sDenUni"].value);
                                    } else {
                                        oUnidad.setDataValue(sUnidades);
                                        oUnidad.setValue(sDenUnidad);
                                    }
                                } else {
                                    if (document.forms["frmArticulos"].elements["sCodUni"].value != "")
                                        oFSEntryCod.UnidadDependent.value = document.forms["frmArticulos"].elements["sCodUni"].value;
                                    else
                                        oFSEntryCod.UnidadDependent.value = sUnidades;
                                }
                            }
                    } else
                        oArt.codigoArticulo = sCod;
                }
                else
                    if (oArt.tipoGS == 119) { // NuevoCodArt
                        oArt.setValue(sCod)
                        oArt.setDataValue(sCod)
                        oFSEntryDen = null;
                        idEntryDen = p.calcularIdDataEntryCelda(document.forms["frmArticulos"].elements["txtArtClientID"].value, oArt.nColumna + 1, oArt.nLinea, 0);

                        if (idEntryDen)
                            oFSEntryDen = p.fsGeneralEntry_getById(idEntryDen);
                        if ((oFSEntryDen) && (oFSEntryDen.tipoGS == 118)) {
                            oFSEntryDen.setValue(sDen)
                            oFSEntryDen.setDataValue(sDen)
                            oFSEntryDen.DenArticuloModificado = false
                            oFSEntryDen.articuloGenerico = bArticuloGenerico;
                            oFSEntryDen.codigoArticulo = sCod;

                            if (oFSEntryDen.idEntryCANT) {
                                if (oFSEntryDen.idEntryCANT != '') {
                                    oCantidad = p.fsGeneralEntry_getById(oFSEntryDen.idEntryCANT);
                                    if (oCantidad) {
                                        if (oFSEntryDen.idEntryUNI) {
                                            if (oFSEntryDen.idEntryUNI != '') {
                                                oUnidad = p.fsGeneralEntry_getById(oFSEntryDen.idEntryUNI);
                                                if (oUnidad) {
                                                    if ((document.forms["frmArticulos"].elements["sCodUni"].value != "") && (document.forms["frmArticulos"].elements["sCodUni"].value != oUnidad.getDataValue())) {
                                                        oCantidad.NumeroDeDecimales = document.forms["frmArticulos"].elements["sNumDecUni"].value
                                                        oCantidad.UnidadOculta = document.forms["frmArticulos"].elements["sDenUni"].value
                                                    } else {
                                                        oCantidad.NumeroDeDecimales = sNumeroDecimales;
                                                        oCantidad.UnidadOculta = sDenUnidad
                                                    }
                                                }
                                            }
                                        }
                                        p.limiteDecimalesTextChanged(oCantidad.Editor, oCantidad.Editor.text, null)
                                        oCantidad.TipoRecepcion = sTipoRecepcion
                                        if (sTipoRecepcion == "1") {
                                            oCantidad.setValue("1");
                                        }
                                    }
                                }
                            }

                            //Aqui vamos a rellenar el IdEntryUniPedido del codigo de articulo pq si esta invisible se pierde
                            if (oFSEntryDen.IdEntryUniPedido) {
                                if (oFSEntryDen.IdEntryUniPedido != '') {
                                    oUnidad = p.fsGeneralEntry_getById(oFSEntryDen.IdEntryUniPedido);
                                    if (oUnidad) {
                                        if (document.forms["frmArticulos"].elements["sCodUni"].value != "") {
                                            oUnidad.setDataValue(document.forms["frmArticulos"].elements["sCodUni"].value);
                                            oUnidad.setValue(document.forms["frmArticulos"].elements["sDenUni"].value);
                                        } else {
                                            oUnidad.setDataValue(sUnidades);
                                            oUnidad.setValue(sDenUnidad);
                                        }
                                    }
                                    else {
                                        if (document.forms["frmArticulos"].elements["sCodUni"].value != "")
                                            oFSEntryDen.UnidadPedidoDependent.value = document.forms["frmArticulos"].elements["sCodUni"].value;
                                        else
                                            oFSEntryDen.UnidadPedidoDependent.value = sUnidades;
                                    }
                                }
                            }

                            //Aqui vamos a rellenar el idEntryUNI del codigo de articulo pq si esta invisible se pierde
                            if (oFSEntryDen.idEntryUNI)
                                if (oFSEntryDen.idEntryUNI != '') {
                                    oUnidad = p.fsGeneralEntry_getById(oFSEntryDen.idEntryUNI);
                                    if (oUnidad) {
                                        if ((document.forms["frmArticulos"].elements["sCodUni"].value != "") && (document.forms["frmArticulos"].elements["sCodUni"].value != oUnidad.getDataValue())) {
                                            //Pone el valor seleccionado desde Ultimas adjudicaciones.
                                            if (oUnidad.readOnly) {
                                                msg = document.forms["frmArticulos"].elements["sMensaje1"].value.replace("#", "\n")
                                                msg = msg.replace("#", "\n")
                                                alert(msg + document.forms["frmArticulos"].elements["sCodUni"].value)
                                            }
                                            oUnidad.setDataValue(document.forms["frmArticulos"].elements["sCodUni"].value);
                                            oUnidad.setValue(document.forms["frmArticulos"].elements["sDenUni"].value);
                                        } else {
                                            oUnidad.setDataValue(sUnidades);
                                            oUnidad.setValue(sDenUnidad);
                                        }

                                    } else {
                                        if (document.forms["frmArticulos"].elements["sCodUni"].value != "")
                                            oFSEntryDen.UnidadDependent.value = document.forms["frmArticulos"].elements["sCodUni"].value;
                                        else
                                            oFSEntryDen.UnidadDependent.value = sUnidades;
                                    }
                                }

                        } else
                            oArt.tag = sDen

                    }
                    else { //Codigo Articulo (104)	

                        oArt.setValue(sCod + " - " + sDen)
                        oArt.setDataValue(sCod)
                    }


                if ((oArt.idEntryPREC) && (!bArticuloGenerico)) {
                    if (lPrecioUltADJ == null)
                        lPrecioUltADJ = '';

                    if (oArt.idEntryPREC != '') {
                        oPrecio = p.fsGeneralEntry_getById(oArt.idEntryPREC);

                        if (oPrecio) {
                            oPrecio.setValue(lPrecioUltADJ);
                            oPrecio.setDataValue(lPrecioUltADJ);
                        }
                    }
                }

                sCodProv = document.forms["frmArticulos"].elements["sCodProv"].value;
                sDenProv = document.forms["frmArticulos"].elements["sDenProv"].value;
                lPrecio = document.forms["frmArticulos"].elements["lPrecio"].value;


                //
                //oArt.idEntryPROV solo contiene el proveedor de la linea DESGLOSE. Hemos dicho q el 
                //proveedor en negociados esta en cabecera SIEMPRE->oArt.idEntryPROV=Nnull

                if (sOrigenLlamada == 9) {  //9=PedidoNegociado
                    if (document.getElementById("hProveedorIniNegociado").value != sCODPROVUltADJ) {
                        oProveedor = p.fsGeneralEntry_getById(document.getElementById("IDCodProve").value);
                        sProveedor = p.fsGeneralEntry_getById(document.getElementById("IDCodProve").value);
                        if (oProveedor) {
                            oProveedor.setDataValue(sCODPROVUltADJ);
                            oProveedor.setValue(sCODPROVUltADJ + " - " + sDENPROVUltADJ);

                            if (document.getElementById("IDFPago").value != "") {
                                oFPago = p.fsGeneralEntry_getById(document.getElementById("IDFPago").value);

                                if ((sFPagoPROVUltADJ == null) || (sFPagoPROVUltADJ == "null")) {
                                    sFPagoPROVUltADJ = '';
                                    sDenFPagoPROVUltADJ = '';
                                }
                                else {
                                    sDenFPagoPROVUltADJ = " - " + sDenFPagoPROVUltADJ;
                                }
                                oFPago.setDataValue(sFPagoPROVUltADJ);
                                oFPago.setValue(sFPagoPROVUltADJ + sDenFPagoPROVUltADJ);

                            }
                            if (document.getElementById("IDFPagoHidden").value != "") {

                                oProveedor.FPagoOculta = sFPagoPROVUltADJ

                            }
                        } else {
                            oProveedor = pp.fsGeneralEntry_getById(document.getElementById("IDCodProve").value);

                            if (oProveedor) {
                                oProveedor.setDataValue(sCODPROVUltADJ);
                                oProveedor.setValue(sCODPROVUltADJ + " - " + sDENPROVUltADJ);

                                if (document.getElementById("IDFPago").value != "") {
                                    oFPago = pp.fsGeneralEntry_getById(document.getElementById("IDFPago").value);

                                    if ((sFPagoPROVUltADJ == null) || (sFPagoPROVUltADJ == "null")) {
                                        sFPagoPROVUltADJ = '';
                                        sDenFPagoPROVUltADJ = '';
                                    }
                                    else {
                                        sDenFPagoPROVUltADJ = " - " + sDenFPagoPROVUltADJ;
                                    }
                                    oFPago.setDataValue(sFPagoPROVUltADJ);
                                    oFPago.setValue(sFPagoPROVUltADJ + sDenFPagoPROVUltADJ);

                                }
                                if (document.getElementById("IDFPagoHidden").value != "") {

                                    oProveedor.FPagoOculta = sFPagoPROVUltADJ
                                }
                            }
                        }
                    }
                }

                //Si hemos seleccionado un proveedor en el buscador y se trata  de un pedido exprés(
                //siempre y cuando esté activado el parámetro para mostrar pedidos express negocidados), rellenamos el campo proveedor de la solicitud
                if (isPedidoExpressNegociable && sCODPROVUltADJ != "" && sCODPROVUltADJ) {
                    oProveedor = p.fsGeneralEntry_getById(document.getElementById("IDCodProve").value);
                    sProveedor = sCODPROVUltADJ;
                    if (oProveedor) {
                        oProveedor.setDataValue(sCODPROVUltADJ);
                        oProveedor.setValue(sCODPROVUltADJ + " - " + sDENPROVUltADJ);

                        if (document.getElementById("IDFPago").value != "") {
                            oFPago = p.fsGeneralEntry_getById(document.getElementById("IDFPago").value);

                            if ((sFPagoPROVUltADJ == null) || (sFPagoPROVUltADJ == "null")) {
                                sFPagoPROVUltADJ = '';
                                sDenFPagoPROVUltADJ = '';
                            }
                            else {
                                sDenFPagoPROVUltADJ = " - " + sDenFPagoPROVUltADJ;
                            }
                            oFPago.setDataValue(sFPagoPROVUltADJ);
                            oFPago.setValue(sFPagoPROVUltADJ + sDenFPagoPROVUltADJ);

                        }
                        if (document.getElementById("IDFPagoHidden").value != "") {

                            oProveedor.FPagoOculta = sFPagoPROVUltADJ

                        }
                    } else {
                        oProveedor = pp.fsGeneralEntry_getById(document.getElementById("IDCodProve").value);

                        if (oProveedor) {
                            oProveedor.setDataValue(sProveedor);
                            oProveedor.setValue(document.getElementById("txtProveedor").value);

                            if (document.getElementById("IDFPago").value != "") {
                                oFPago = pp.fsGeneralEntry_getById(document.getElementById("IDFPago").value);

                                if ((sFPagoPROVUltADJ == null) || (sFPagoPROVUltADJ == "null")) {
                                    sFPagoPROVUltADJ = '';
                                    sDenFPagoPROVUltADJ = '';
                                }
                                else {
                                    sDenFPagoPROVUltADJ = " - " + sDenFPagoPROVUltADJ;
                                }
                                oFPago.setDataValue(sFPagoPROVUltADJ);
                                oFPago.setValue(sFPagoPROVUltADJ + sDenFPagoPROVUltADJ);

                            }
                            if (document.getElementById("IDFPagoHidden").value != "") {

                                oProveedor.FPagoOculta = sFPagoPROVUltADJ
                            }
                        }
                    }

                }

                if ((oArt.idEntryPROV) && (!bArticuloGenerico)) {
                    if (sCODPROVUltADJ == null)
                        sCODPROVUltADJ = '';
                    if (sDENPROVUltADJ == null)
                        sDENPROVUltADJ = '';
                    if (oArt.idEntryPROV != '') {
                        oProveedor = p.fsGeneralEntry_getById(oArt.idEntryPROV);
                        if (oProveedor)
                            if ((sCodProv != '') && (lPrecioUltADJ == lPrecio)) { //La 2ª Parte del AND es para cndo viene de seleccionar una Ultima adjudicacion pero cambia de columna
                                oProveedor.setDataValue(sCodProv);
                                oProveedor.setValue(sCodProv + " - " + sDenProv);
                            } else {
                                oProveedor.setDataValue(sCODPROVUltADJ);
                                if (sCODPROVUltADJ == '')
                                    oProveedor.setValue('');
                                else
                                    oProveedor.setValue(sCODPROVUltADJ + " - " + sDENPROVUltADJ);
                            }

                    }
                }

                if (oArt.idEntryCANT) {
                    if (oArt.idEntryCANT != '') {
                        oCantidad = p.fsGeneralEntry_getById(oArt.idEntryCANT);
                        if (oArt.idEntryUNI) {
                            if (oArt.idEntryUNI != '') {
                                oUnidad = p.fsGeneralEntry_getById(oArt.idEntryUNI);
                                if (oCantidad && oUnidad) {
                                    if ((document.forms["frmArticulos"].elements["sCodUni"].value != "") && (document.forms["frmArticulos"].elements["sCodUni"].value != oUnidad.getDataValue())) {
                                        oCantidad.NumeroDeDecimales = document.forms["frmArticulos"].elements["sNumDecUni"].value
                                        oCantidad.UnidadOculta = document.forms["frmArticulos"].elements["sDenUni"].value
                                    } else {
                                        oCantidad.NumeroDeDecimales = sNumeroDecimales;
                                        oCantidad.UnidadOculta = sDenUnidad
                                    }
                                    p.limiteDecimalesTextChanged(oCantidad.Editor, oCantidad.Editor.text, null)
                                }
                            }
                        }
                        if (oCantidad) {
                            oCantidad.TipoRecepcion = sTipoRecepcion
                            if (sTipoRecepcion == "1") {
                                oCantidad.setValue("1");
                            }
                        }
                    }
                }

                //Aqui vamos a rellenar el IdEntryUniPedido del codigo de articulo pq si esta invisible se pierde
                if (oArt.IdEntryUniPedido) {
                    if (oArt.IdEntryUniPedido != '') {
                        oUnidad = p.fsGeneralEntry_getById(oArt.IdEntryUniPedido);
                        if (oUnidad) {
                            if (document.forms["frmArticulos"].elements["sCodUni"].value != "") {
                                oUnidad.setDataValue(document.forms["frmArticulos"].elements["sCodUni"].value);
                                oUnidad.setValue(document.forms["frmArticulos"].elements["sDenUni"].value);
                            } else {
                                oUnidad.setDataValue(sUnidades);
                                oUnidad.setValue(sDenUnidad);
                            }
                        }
                        else {
                            if (document.forms["frmArticulos"].elements["sCodUni"].value != "")
                                oArt.UnidadPedidoDependent.value = document.forms["frmArticulos"].elements["sCodUni"].value;
                            else
                                oArt.UnidadPedidoDependent.value = sUnidades;
                        }
                    }
                };
                if (oArt.idEntryUNI)
                    if (oArt.idEntryUNI != '') {
                        oUnidad = p.fsGeneralEntry_getById(oArt.idEntryUNI);
                        if (oUnidad) {
                            if ((document.forms["frmArticulos"].elements["sCodUni"].value != "") && (document.forms["frmArticulos"].elements["sCodUni"].value != oUnidad.getDataValue())) {
                                //Pone el valor seleccionado desde Ultimas adjudicaciones.
                                if (oUnidad.readOnly) {
                                    msg = document.forms["frmArticulos"].elements["sMensaje1"].value.replace("#", "\n")
                                    msg = msg.replace("#", "\n")
                                    alert(msg + document.forms["frmArticulos"].elements["sCodUni"].value)
                                }
                                oUnidad.setDataValue(document.forms["frmArticulos"].elements["sCodUni"].value);
                                oUnidad.setValue(document.forms["frmArticulos"].elements["sDenUni"].value);
                            } else {
                                oUnidad.setDataValue(sUnidades);
                                oUnidad.setValue(sDenUnidad);
                            }

                        } else {
                            if (document.forms["frmArticulos"].elements["sCodUni"].value != "")
                                oArt.UnidadDependent.value = document.forms["frmArticulos"].elements["sCodUni"].value;
                            else
                                oArt.UnidadDependent.value = sUnidades;
                        }
                    };
                if (bUsar_OrgCompras == true) {
                    if (oArt.idDataEntryDependent) {
                        oOrganizacionCompras = p.fsGeneralEntry_getById(oArt.idDataEntryDependent);
                        if ((oOrganizacionCompras) && (oOrganizacionCompras.tipoGS == 123)) {
                            if ((sCodOrgCompras != "") && (oOrganizacionCompras.getDataValue() != sCodOrgCompras))
                                window.opener.borrarPresupuestos(oArt.id)
                            oOrganizacionCompras.setDataValue(sCodOrgCompras);
                            oOrganizacionCompras.setValue(sCodOrgCompras + ' - ' + sDenOrgCompras)
                        }
                    } else {
                        if (sDataEntryOrgComprasFORM) {
                            oOrganizacionCompras = pp.fsGeneralEntry_getById(sDataEntryOrgComprasFORM);
                            if ((oOrganizacionCompras) && (oOrganizacionCompras.tipoGS == 123)) {
                                oOrganizacionCompras.setDataValue(sCodOrgCompras);
                                oOrganizacionCompras.setValue(sCodOrgCompras + ' - ' + sDenOrgCompras)
                            }
                        }
                    }

                    if ((oArt.idDataEntryDependent2) || (sDataEntryCentroFORM)) {
                        if (oArt.idDataEntryDependent2)
                            oCentro = p.fsGeneralEntry_getById(oArt.idDataEntryDependent2)
                        else
                            oCentro = pp.fsGeneralEntry_getById(sDataEntryCentroFORM);
                        if ((oCentro) && (oCentro.tipoGS == 124) && (sCodCentro) && (oCentro.getDataValue() != sCodCentro)) {
                            oCentro.setDataValue(sCodCentro);
                            oCentro.setValue(sCodCentro + " - " + sDenCentro);
                            //Borrar almacén si existe
                            p.vaciarAlmacen(oCentro)
                        }

                    }
                } else {
                    if (oArt.idDataEntryDependent3) {
                        oUnidadOrganizativa = p.fsGeneralEntry_getById(oArt.idDataEntryDependent3);
                        if ((oUnidadOrganizativa) && (oUnidadOrganizativa.tipoGS == 121)) {                            
                            if (currentTree) {
                                var selNode = currentTree.get_selectedNodes();
                                if (currentTree.get_selectedNodes().length !== 0) {
                                    var sDen = currentTree.get_selectedNodes()[0].get_text();
                                    var oNode = currentTree.get_selectedNodes()[0];
                                    var iNivel = oNode.get_level();
                                    var sUON1, sUON2, sUON3;
                                    switch (iNivel) {
                                        case 1:
                                            sUON1 = oNode.get_valueString();
                                            break;
                                        case 2:
                                            sUON1 = oNode.get_parentNode().get_valueString();
                                            sUON2 = oNode.get_valueString();
                                            break;
                                        case 3:
                                            sUON1 = oNode.get_parentNode().get_parentNode().get_valueString();
                                            sUON2 = oNode.get_parentNode().get_valueString();
                                            sUON3 = oNode.get_valueString();
                                            break;
                                    };
                                    oUnidadOrganizativa.setDataValue(sUON1 + (sUON2 == undefined ? '' : ' - ' + sUON2) + (sUON3 == undefined ? '' : ' - ' + sUON3) + ' - ' + sDen);
                                    oUnidadOrganizativa.setValue(sUON1 + (sUON2 == undefined ? '' : ' - ' + sUON2) + (sUON3 == undefined ? '' : ' - ' + sUON3) + ' - ' + sDen);
                                } else {
                                    oUnidadOrganizativa.setDataValue('');
                                    oUnidadOrganizativa.setValue('');
                                };
                            }
                            else {
                                //caso de UONs en sólo lectura                                
                                var UONs = document.getElementById("lblUnidadOrganizativaSeleccionada").innerText.split(' - ');
                                sDen = UONs[UONs.length - 1];
                                UONs = UONs.slice(0, UONs.length - 1);
                                oUnidadOrganizativa.setDataValue(UONs[0] + (UONs[1] == undefined ? '' : ' - ' + UONs[1]) + (UONs[2] == undefined ? '' : ' - ' + UONs[2]) + ' - ' + sDen);
                                oUnidadOrganizativa.setValue(UONs[0] + (UONs[1] == undefined ? '' : ' - ' + UONs[1]) + (UONs[2] == undefined ? '' : ' - ' + UONs[2]) + ' - ' + sDen);
                            }                            
                        };
                    } else {
                        if (sDataEntryUnidadOrganizativaFORM) {
                            oUnidadOrganizativa = pp.fsGeneralEntry_getById(sDataEntryUnidadOrganizativaFORM);
                            if ((oUnidadOrganizativa) && (oUnidadOrganizativa.tipoGS == 121)) {
                                if (currentTree) {
                                    var selNode = currentTree.get_selectedNodes();
                                    if (currentTree.get_selectedNodes().length !== 0) {
                                        var sDen = currentTree.get_selectedNodes()[0].get_text();
                                        var oNode = currentTree.get_selectedNodes()[0];
                                        var iNivel = oNode.get_level();
                                        var sUON1, sUON2, sUON3;
                                        switch (iNivel) {
                                            case 1:
                                                sUON1 = oNode.get_valueString();
                                                break;
                                            case 2:
                                                sUON1 = oNode.get_parentNode().get_valueString();
                                                sUON2 = oNode.get_valueString();
                                                break;
                                            case 3:
                                                sUON1 = oNode.get_parentNode().get_parentNode().get_valueString();
                                                sUON2 = oNode.get_parentNode().get_valueString();
                                                sUON3 = oNode.get_valueString();
                                                break;
                                        };
                                        oUnidadOrganizativa.setDataValue(sUON1 + (sUON2 == undefined ? '' : ' - ' + sUON2) + (sUON3 == undefined ? '' : ' - ' + sUON3) + ' - ' + sDen);
                                        oUnidadOrganizativa.setValue(sUON1 + (sUON2 == undefined ? '' : ' - ' + sUON2) + (sUON3 == undefined ? '' : ' - ' + sUON3) + ' - ' + sDen);
                                    } else {
                                        oUnidadOrganizativa.setDataValue('');
                                        oUnidadOrganizativa.setValue('');
                                    };
                                }
                                else {
                                    //caso de UONs en sólo lectura                                
                                    var UONs = document.getElementById("lblUnidadOrganizativaSeleccionada").innerText.split(' - ');
                                    sDen = UONs[UONs.length - 1];
                                    UONs = UONs.slice(0, UONs.length - 1);
                                    oUnidadOrganizativa.setDataValue(UONs[0] + (UONs[1] == undefined ? '' : ' - ' + UONs[1]) + (UONs[2] == undefined ? '' : ' - ' + UONs[2]) + ' - ' + sDen);
                                    oUnidadOrganizativa.setValue(UONs[0] + (UONs[1] == undefined ? '' : ' - ' + UONs[1]) + (UONs[2] == undefined ? '' : ' - ' + UONs[2]) + ' - ' + sDen);
                                }
                            };
                        };
                    };
                };

                s = ""
                for (i = 0; i < ilGMN1 - sGMN1.length; i++)
                    s += " "

                sMat = s + sGMN1
                if (sGMN2) {

                    s = ""
                    for (i = 0; i < ilGMN2 - sGMN2.length; i++)
                        s += " "
                    sMat = sMat + s + sGMN2
                }

                if (sGMN3) {

                    s = ""
                    for (i = 0; i < ilGMN3 - sGMN3.length; i++)
                        s += " "
                    sMat = sMat + s + sGMN3
                }
                if (sGMN4) {

                    s = ""
                    for (i = 0; i < ilGMN4 - sGMN4.length; i++)
                        s += " "
                    sMat = sMat + s + sGMN4
                }

                oArt.Dependent.value = sMat
                if (oArt.tipoGS == 119) { // NuevoCodArt
                    if ((oFSEntryDen) && (oFSEntryDen.tipoGS == 118)) {
                        oFSEntryDen.Dependent.value = sMat
                    }
                }

                p.mat_seleccionado(document.forms["frmArticulos"].elements["txtMatClientID"].value, sGMN1, sGMN2, sGMN3, sGMN4, 4, sGMNDen, '',TodosNiveles)
            } else {
                if (sdesde == 'VisorSol') {
                    codArt = window.opener.document.getElementById(document.forms["frmArticulos"].elements["idControlArtCod"].value);
                    if (codArt) codArt.value = sCod;
                    destinoResultado = window.opener.document.getElementById(document.forms["frmArticulos"].elements["idControlArtDen"].value);
                    switch (parseInt(document.getElementById("hTipoCampoGS").value)) {
                        case 104:
                        case 119: //codigo articulo
                            destinoResultado.value = sCod + (document.getElementById("EsCampoGeneral").value == 0 ? '' : ' - ' + sDen);
                            break;
                        case 118: //denominacion articulo
                            destinoResultado.value = sDen;
                            break;
                    };
                } else {
                    if (document.forms["frmArticulos"].elements["idControlMatDen"].value != "") {
                        materialDen = window.opener.document.getElementById(document.forms["frmArticulos"].elements["idControlMatDen"].value);
                        if (materialDen) {
                            if (sGMN4)
                                if (TodosNiveles ==1)
                                    sDenMat = sGMN1 + " - " + sGMN2 + " - " + sGMN3 + " - "  +sGMN4 + " - ";
                                else
                                    sDenMat = sGMN4 + " - ";
                            sDenMat = sDenMat + sGMNDen;
                            materialDen.value = sDenMat;
                        }
                    }
                    if (document.forms["frmArticulos"].elements["idControlMatBD"].value != "") {
                        materialGS = window.opener.document.getElementById(document.forms["frmArticulos"].elements["idControlMatBD"].value);
                        if (materialGS)
                            materialGS.value = sGMN1 + "-" + sGMN2 + "-" + sGMN3 + "-" + sGMN4;
                    }
                    if (document.forms["frmArticulos"].elements["idControlArtCod"].value != "") {
                        codArt = window.opener.document.getElementById(document.forms["frmArticulos"].elements["idControlArtCod"].value);
                        if (codArt)
                            codArt.value = sCod;
                    }
                    if (document.forms["frmArticulos"].elements["idControlArtDen"].value != "") {
                        denArt = window.opener.document.getElementById(document.forms["frmArticulos"].elements["idControlArtDen"].value);
                        if (denArt)
                            denArt.value = sDen;
                    }
                    if (document.forms["frmArticulos"].elements["idHidControl"].value != "") {
                        codArt = window.opener.document.getElementById(document.forms["frmArticulos"].elements["idHidControl"].value);
                        if (codArt)
                            codArt.value = sCod;
                    }
                    if (document.forms["frmArticulos"].elements["ClientId"].value != "") {
                        denArt = window.opener.document.getElementById(document.forms["frmArticulos"].elements["ClientId"].value);
                        if (denArt)
                            denArt.value = sDen;
                    };
                };
            };
            //Si se trata de pedido express con activación de articulos negociados, cargamos el precio de la solicitud (campo tipo 9 o 10) con el 
            //de la última adjudicación.
            if (isPedidoExpressNegociable) {

                oEntry = p.getEntryTipoByLinea(9, oArt.idLinea);
                if (oEntry) {
                    oEntry.setValue(lPrecioUltADJ);
                } else {
                    oEntry = p.getEntryTipoByLinea(10, oArt.idLinea);
                    if (oEntry) {
                        oEntry.setValue(lPrecioUltADJ);
                    }
                }
                //si el textbox de proveedor se ha vaciado borrarlo de la solicitud
                if (document.getElementById("txtProveedor").value != "") {
                    oProveedor = p.fsGeneralEntry_getById(document.getElementById("IDCodProve").value);
                    oProveedor.setValue(document.getElementById("txtProveedor").value);
                    oProveedor.setDataValue(document.getElementById("hProveedor").value);
                }
            }
            //Validación de recepción por importe (si es de este tipo pongo cantidad a 1)
            if (typeof (oArt) !== 'undefined' && oArt.isDesglose) {
                if (sTipoRecepcion == "1") {
                    oEntry = p.getEntryTipoByLinea(4, oArt.idLinea);
                    if (oEntry) {
                        oEntry.setValue("1");
                    }
                }
            }
            window.close()
        };
        /*
        ''' <summary>
        ''' Control de favoritos en la Grid.
        ''' </summary>
        ''' <param name="sender">parametro de sistema</param>
        ''' <param name="e">parametro de sistema</param>            
        ''' <remarks>Llamada desde: Sistema ;Tiempo máximo: 0</remarks>
        */
        
        function wdgArticulos_Selection_CellSelectionChanged(sender, eventArgs) {
            //upon changing the cell selection, also apply the change in the row selection
            var row = eventArgs.getSelectedCells().getItem(0).get_row();
            sender.get_behaviors().get_selection().get_selectedRows().add(row);
        }
        function wdgArticulos_Selection_CellSelectionChanging(sender, eventArgs) {
            if (eventArgs.getNewSelectedCells().getCell(0).get_column().get_key() == "FAVORITO") {
                eventArgs.set_cancel(true);

                //if cancelling the new selection, the old row needs to be reselected
                if (eventArgs.getCurrentSelectedCells().getCell(0) != null) {
                    var row = eventArgs.getCurrentSelectedCells().getCell(0).get_row();
                    sender.get_behaviors().get_selection().get_selectedRows().add(row);
                }
            }
        }

        function CambiarArticulosFavoritos(sArtCod, sAccion) {
            params = { codArt: sArtCod, accion: sAccion };
            $.ajax({
                type: "POST",
                url: RutaFS + '_Common/App_Services/Consultas.asmx/CambiarArticuloFavoritosBD',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(params),
                dataType: "json",
                async: true
            });                      
        }
        var bLimpiar
        bLimpiar = true
        function limpiarArt() {
            if (!bLimpiar) {
                bLimpiar = true
                return
            }
            o = fsGeneralEntry_getById("txtMaterial")
            oGrid = $find("wdgArticulos")
            oDiv = document.getElementById("G_wdgArticulos")
            if (oDiv)
                oDiv.style.visibility = "hidden"

            otbl = document.all("tblBusqueda")
            otbl.rows[6].cells[1].style.visibility = "hidden"
            otbl.rows[7].style.visibility = "hidden"
        };
        //Se mira si se ha pulsado sobre la celda que contiene el botón, para abrir la ventana de ultimasadjudicaciones.
        //LLamada desde: Al hacer click sobre la grid.
        function grid_CellClick(sender, e) {
            if (e.get_type() == "cell" && e.get_item().get_row().get_index() !== -1) {
                var cell = e.get_item();
                switch (cell.get_column().get_key()) {
                    case 'FAVORITO':
                        var sAccion = ""
                        var sArtCod = cell.get_row().get_cellByColumnKey('COD').get_value();
                        var favorito = cell.get_value();
                        if (favorito == "0") {
                            sAccion="Insertar"
                            cell.set_value("1")
                            cell.get_element().innerHTML='<IMG style="TEXT-ALIGN: center" src="<%=System.Configuration.ConfigurationManager.AppSettings("ruta")%>App_themes/<%=Page.Theme%>/images/favoritos_si.gif">'
                        }
                        else {
                            sAccion="Eliminar" 
                            cell.set_value("0")
                            cell.get_element().innerHTML = '<IMG style="TEXT-ALIGN: center" src="<%=System.Configuration.ConfigurationManager.AppSettings("ruta")%>App_themes/<%=Page.Theme%>/images/favoritos_no.gif">'
                        }    
                        CambiarArticulosFavoritos(sArtCod, sAccion)
                        break;
                    case "BTN":
                        oGrid = $find("wdgArticulos")
                        var lPrecioUltADJ = cell.get_row().get_cellByColumnKey('PREC_ULT_ADJ').get_value();
                        if (lPrecioUltADJ != null) {
                            oCell = cell.get_row().get_cellByColumnKey("COD")
                            sCod = oCell.get_value()

                            MostrarUltimasAdjucaciones(sCod, sInstanciaMoneda)
                        }
                        break;
                }
            }
        };
        function mostrarAdj(sCod) {
            MostrarUltimasAdjucaciones(sCod, sInstanciaMoneda)
            return false;
        };
        /*
        ''' <summary>
        ''' Trae el precio de la adjudicación seleccionada
        ''' </summary>
        ''' <param name="lPrecio">Precio</param>  
        ''' <param name="sCODPROVUltADJ">Proveedor</param>
        ''' <param name="sDENPROVUltADJ">Den Proveedor</param>  
        ''' <param name="sCodUni">Unidad</param>
        ''' <param name="sDenUni">Den Unidad</param>  
        ''' <param name="sNumDecUni">Num decimales</param>
        ''' <param name="sMoneda">Moneda</param>  
        ''' <remarks>Llamada desde: UltimasAdjudicaciones.aspx ; Tiempo máximo: 0,2</remarks>*/
        function PonerUltADJ(lPrecio, sCODPROVUltADJ, sDENPROVUltADJ, sCodUni, sDenUni, sNumDecUni, sMoneda, sDenMoneda, sFPago, sDenFPago) {
            var oRow = oGrid.get_behaviors().get_selection().get_selectedRows(0);

            oCell = oRow.getItem(0).get_cellByColumnKey("PREC_ULT_ADJ");
            oCell.set_value(lPrecio);

            sOrigenLlamada = ""
            if (document.forms["frmArticulos"].elements["hTipoSolicitud"]) {
                sOrigenLlamada = document.forms["frmArticulos"].elements["hTipoSolicitud"].value;
                if (sOrigenLlamada == 9 || isPedidoExpressNegociable) { //9=PedidoNegociado
                    oCell = oRow.getItem(0).get_cellByColumnKey("PREC_VIGENTE");
                    oCell.set_value(lPrecio + ' ' + sMoneda + '/' + sCodUni);

                    oCell = oRow.getItem(0).get_cellByColumnKey("UNI_ULT_ADJ");
                    oCell.set_value(sCodUni);

                    oCell = oRow.getItem(0).get_cellByColumnKey("DENUNI_ULT_ADJ");
                    oCell.set_value(sDenUni);

                    oCell = oRow.getItem(0).get_cellByColumnKey("MON_ULT_ADJ");
                    oCell.set_value(sMoneda);

                    oCell = oRow.getItem(0).get_cellByColumnKey("DENMON_ULT_ADJ");
                    oCell.set_value(sDenMoneda);

                    oCell = oRow.getItem(0).get_cellByColumnKey("CODPROV_ULT_ADJ");
                    oCell.set_value(sCODPROVUltADJ);

                    oCell = oRow.getItem(0).get_cellByColumnKey("DENPROV_ULT_ADJ");
                    oCell.set_value(sDENPROVUltADJ);

                    if ((document.getElementById("IDFPago").value != "") || (document.getElementById("IDFPagoHidden").value != "")) {
                        oCell = oRow.getItem(0).get_cellByColumnKey("PAG_ULT_ADJ");
                        oCell.set_value(sFPago);

                        oCell = oRow.getItem(0).get_cellByColumnKey("DENPAG_ULT_ADJ");
                        oCell.set_value(sDenFPago);
                    }
                }
            }

            document.forms["frmArticulos"].elements["lPrecio"].value = lPrecio;
            document.forms["frmArticulos"].elements["sCodProv"].value = sCODPROVUltADJ;
            document.forms["frmArticulos"].elements["sDenProv"].value = sDENPROVUltADJ;

            document.forms["frmArticulos"].elements["sCodUni"].value = sCodUni;
            document.forms["frmArticulos"].elements["sDenUni"].value = sDenUni;
            document.forms["frmArticulos"].elements["sNumDecUni"].value = sNumDecUni;
        };
        //''' <summary>
        //''' Carga la selección de materiales
        //''' </summary>
        //''' <remarks>Llamada desde: imgBuscarMaterial.onClientClick ; Tiempo máximo: 0,2</remarks>
        function AbrirMateriales() {
            if (!NivelSeleccion) NivelSeleccion = 0;
            if (!FamiliaMateriales) FamiliaMateriales = '';
            var DelProve = "";
            var sOrigenLlamada = ""
            if (document.forms["frmArticulos"].elements["hTipoSolicitud"]) {
                sOrigenLlamada = document.forms["frmArticulos"].elements["hTipoSolicitud"].value;
                if (sOrigenLlamada == 8) {  //8=PedidoExpress
                    if (document.getElementById("chkVerNoProveedor").checked == false) {
                        if (document.getElementById("hProveedor").value != "") {
                            DelProve = "&CodProve=" + document.getElementById("hProveedor").value
                        }
                    }
                } else {
                    if (sOrigenLlamada == 9) {  //9=PedidoNegociado
                        if (document.getElementById("hProveedor").value != "") {
                            DelProve = "&CodProve=" + document.getElementById("hProveedor").value
                        }
                    }
                }
                if (sOrigenLlamada == 5) {  //5=Contrato
                    if (document.getElementById("hProveedor").value != "") {
                        DelProve = "&CodProve=" + document.getElementById("hProveedor").value
                    }
                }
            }
            var newWindow = window.open(rutaPM + "_common/materiales.aspx?Valor=" + document.getElementById("hMat").value + "&IdControl=txtMaterial&Mat=" + FamiliaMateriales + "&readonly=0" + DelProve + "&NivelSeleccion=" + NivelSeleccion, "_blank", "width=550,height=550,status=yes,resizable=no,top=100,left=200");
            newWindow.focus();
        };
        function LimpiarMaterial() {
            document.getElementById("txtMaterial").value = "";
            document.getElementById("txtMaterial").title = "";
            document.getElementById("hMat").value = "";
        };
        //''' <summary>
        //''' Limpiar el Proveedor
        //''' </summary>
        //''' <remarks>Llamada desde: txtProveedor/onkeydown ; Tiempo máximo: 0,2</remarks>
        function LimpiarProveedor() {
            document.getElementById("txtProveedor").value = "";
            document.getElementById("txtProveedor").title = "";
            document.getElementById("hProveedor").value = "";
        };
        /*''' <summary>
        ''' Rellenar la caja de texto con el material seleccionado
        ''' </summary>
        ''' <param name="IDControl">id de la caja de texto donde va el material seleccionado</param>
        ''' <param name="sGMN1">Gmn1 de material seleccionado</param>        
        ''' <param name="sGMN2">Gmn2 de material seleccionado</param>
        ''' <param name="sGMN3">Gmn3 de material seleccionado</param>  
        ''' <param name="sGMN4">Gmn4 de material seleccionado</param>
        ''' <param name="iNivel">Nivel de material seleccionado</param>  
        ''' <param name="sDen">Denominación del material</param>
        ''' <param name="desde">Vacio si viene de una pantalla q no sea Materiales.aspx eoc 'Materiales'</param>  
        ''' <remarks>Llamada desde: materiales.aspx/seleccionarMaterial(); Tiempo máximo:0</remarks>*/
        function mat_seleccionado(IDControl, sGMN1, sGMN2, sGMN3, sGMN4, iNivel, sDen, desde, TodosNiveles) {
            //Si el texto del nodo era, por ejemplo, C'a"ta >600mm. El oNode.getText() devuelve C'a"ta &gt;600mm. Lo q ha
            //de verse es > no &gt;
            sDen = replaceAll(sDen, "&gt;", ">")
            sDen = replaceAll(sDen, "&lt;", "<")
            var sDenAux = sDen
            if (IDControl == "")
                return

            o = document.getElementById(IDControl)
            s = ""
            for (i = 0; i < ilGMN1 - sGMN1.length; i++)
                s += " "

            sMat = s + sGMN1
            sDenAux += ' (' + sGMN1
            if (TodosNiveles == 1) {
                sDenShort = sGMN1 + ' - '
            } else {
                sDenShort = sGMN1 + ' - ' + sDen
            }

            if (sGMN2) {
                s = ""
                for (i = 0; i < ilGMN2 - sGMN2.length; i++)
                    s += " "
                sMat = sMat + s + sGMN2
                sDenAux += ' - ' + sGMN2
                if (TodosNiveles == 1) {
                    sDenShort += sGMN2 + ' - '
                } else {
                    sDenShort = sGMN2 + ' - ' + sDen
                }
            }               
            if (sGMN3) {
                s = ""
                for (i = 0; i < ilGMN3 - sGMN3.length; i++)
                    s += " "
                sMat = sMat + s + sGMN3
                sDenAux += ' - ' + sGMN3
                if (TodosNiveles == 1) {
                    sDenShort += sGMN3 + ' - '
                } else {
                    sDenShort = sGMN3 + ' - ' + sDen
                }
            }
            if (sGMN4) {
                s = ""
                for (i = 0; i < ilGMN4 - sGMN4.length; i++)
                    s += " "
                sMat = sMat + s + sGMN4
                sDenAux += ' - ' + sGMN4
                if (TodosNiveles == 1) {
                    sDenShort += sGMN4 + ' - '
                } else {
                    sDenShort = sGMN4 + ' - ' + sDen
                }

            }
            sDenAux += ')'

            if (TodosNiveles == 1) {
                o.value = sDenShort + sDen
            } else {
                o.value = sDenShort
            }
            o.title = sDenAux

            o1 = document.getElementById("hMat")
            if (o1)
                o1.value = sMat
        }

        /*''' <summary>
        ''' Evitar los enter q hacen q el 1er botón, busqueda proves, se ejecute
        ''' </summary>
        ''' <param name="event">Pulsación</param>
        ''' <returns>True para Cualquier tecla q pulses excepto el enter</returns>
        ''' <remarks>Llamada desde: sistema; Tiempo máximo:0</remarks>*/
        function Form_KeyPres(evt) {
            var mytarget;
            var keyCode;

            if (document.all) {
                //IE antiguos
                keyCode = event.keyCode;
                mytarget = window.event.srcElement.type;
            }
            else if (document.addEventListener) {
                //Resto de navegaodres
                mytarget = evt.target.type;
                keyCode = evt.keyCode;
            }
            if ((mytarget != 'textarea') && (keyCode == 13))
                return false;
            return true;
        }
        /*
        ''' <summary>
        ''' Reemplazar todas las ocurrencias de un texto dado (sBuscar) por otro texto (sSustituir) dentro de un string (sOriginal)
        ''' </summary>
        ''' <param name="sOriginal">texto</param>
        ''' <param name="sBuscar">texto a Reemplazar</param>        
        ''' <param name="sSustituir">texto por el q Reemplazar</param>      
        ''' <remarks>Llamada desde: mat_seleccionado ; Tiempo máximo: 0</remarks>*/
        function replaceAll(sOriginal, sBuscar, sSustituir) {
            if (sOriginal == null)
                return null

            var re = eval("/" + sBuscar + "/")
            var sTmp = ""

            while (sOriginal.search(re) >= 0) {
                sTmp += sOriginal.substr(0, sOriginal.search(re)) + sSustituir
                sOriginal = sOriginal.substr(sOriginal.search(re) + sBuscar.length, sOriginal.length)
            }
            sTmp += sOriginal
            return (sTmp)

        }
    </script>



    <script type="text/javascript">
        /*Funciones para paginacion*/
        function AdministrarPaginador() {
            var grid = $find("<%= wdgArticulos.ClientID %>");
            var parentGrid = grid;
            $('[id$=lblCount]').text(parentGrid.get_behaviors().get_paging().get_pageCount());
            if (parentGrid.get_behaviors().get_paging().get_pageIndex() == 0) {
                $('[id$=ImgBtnFirst]').attr('disabled', 'disabled');
                $('[id$=ImgBtnFirst]').attr('src', rutaTheme + '/images/primero_desactivado.gif');
                $('[id$=ImgBtnPrev]').attr('disabled', 'disabled');
                $('[id$=ImgBtnPrev]').attr('src', rutaTheme + '/images/anterior_desactivado.gif');
            } else {
                $('[id$=ImgBtnFirst]').removeAttr('disabled');
                $('[id$=ImgBtnFirst]').attr('src', rutaTheme + '/images/primero.gif');
                $('[id$=ImgBtnPrev]').removeAttr('disabled');
                $('[id$=ImgBtnPrev]').attr('src', rutaTheme + '/images/anterior.gif');
            }
            if (parentGrid.get_behaviors().get_paging().get_pageIndex() == parentGrid.get_behaviors().get_paging().get_pageCount() - 1) {
                $('[id$=ImgBtnNext]').attr('disabled', 'disabled');
                $('[id$=ImgBtnNext]').attr('src', rutaTheme + '/images/siguiente_desactivado.gif');
                $('[id$=ImgBtnLast]').attr('disabled', 'disabled');
                $('[id$=ImgBtnLast]').attr('src', rutaTheme + '/images/ultimo_desactivado.gif');
            } else {
                $('[id$=ImgBtnNext]').removeAttr('disabled');
                $('[id$=ImgBtnNext]').attr('src', rutaTheme + '/images/siguiente.gif');
                $('[id$=ImgBtnLast]').removeAttr('disabled');
                $('[id$=ImgBtnLast]').attr('src', rutaTheme + '/images/ultimo.gif');
            }
        };
        function wdgArticulos_PageIndexChanged(a, b, c, d) {
            var grid = $find("<%= wdgArticulos.ClientID %>");
            var parentGrid = grid;
            var dropdownlist = $('[id$=PagerPageList]')[0];

            dropdownlist.options[parentGrid.get_behaviors().get_paging().get_pageIndex()].selected = true;
            AdministrarPaginador();
        };
        function IndexChanged() {
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var grid = $find("<%= wdgArticulos.ClientID %>");
            var parentGrid = grid;
            var newValue = dropdownlist.selectedIndex;
            parentGrid.get_behaviors().get_paging().set_pageIndex(newValue);
            AdministrarPaginador();
        };
        function FirstPage() {
            var grid = $find("<%= wdgArticulos.ClientID %>");
            var parentGrid = grid;
            parentGrid.get_behaviors().get_paging().set_pageIndex(0);
        };
        function PrevPage() {
            var grid = $find("<%= wdgArticulos.ClientID %>");
            var parentGrid = grid;
            var dropdownlist = $('[id$=PagerPageList]')[0];
            if (parentGrid.get_behaviors().get_paging().get_pageIndex() > 0) {
                parentGrid.get_behaviors().get_paging().set_pageIndex(parentGrid.get_behaviors().get_paging().get_pageIndex() - 1);
            }
        };
        function NextPage() {
            var grid = $find("<%= wdgArticulos.ClientID %>");
            var parentGrid = grid;
            var dropdownlist = $('[id$=PagerPageList]')[0];
            if (parentGrid.get_behaviors().get_paging().get_pageIndex() < parentGrid.get_behaviors().get_paging().get_pageCount() - 1) {
                parentGrid.get_behaviors().get_paging().set_pageIndex(parentGrid.get_behaviors().get_paging().get_pageIndex() + 1);
            }
        };
        function LastPage() {
            var grid = $find("<%= wdgArticulos.ClientID %>");
            var parentGrid = grid;
            parentGrid.get_behaviors().get_paging().set_pageIndex(parentGrid.get_behaviors().get_paging().get_pageCount() - 1);
        };
        function MostrarUnidadesOrganizativas() {
        };
        var currentTree;
        function initTree(tree) {
            currentTree = tree;
        };
        function LimpiarUON() {
            document.getElementById("txtUnidadOrganizativa").value = "";
            document.getElementById("txtUnidadOrganizativa").title = "";
            document.getElementById("hUnidadOrganizativa").value = "";
        };
        function MostrarUONs() {
            $('[id$=wdtUnidadesOrganizativas]').parent('div').toggle();
        };
        $('[id$=imgBuscarUnidadOrganizativa]').live('click', function (event) {
            MostrarUONs();
            event.stopPropagation();
        });
        function seleccionarUnidadOrg(sender, e) {
            var selNode = currentTree.get_selectedNodes();
            if (currentTree.get_selectedNodes().length == 0) return false;
            var sDen = currentTree.get_selectedNodes()[0].get_text();
            var oNode = currentTree.get_selectedNodes()[0];
            var iNivel = oNode.get_level();
            var sUON1, sUON2, sUON3;
            switch (iNivel) {
                case 1:
                    sUON1 = oNode.get_valueString();
                    break;
                case 2:
                    sUON1 = oNode.get_parentNode().get_valueString();
                    sUON2 = oNode.get_valueString();
                    break;
                case 3:
                    sUON1 = oNode.get_parentNode().get_parentNode().get_valueString();
                    sUON2 = oNode.get_parentNode().get_valueString();
                    sUON3 = oNode.get_valueString();
                    break;
            };
            $('[id$=txtUnidadOrganizativa]').val(sUON1 + (sUON2 == undefined ? '' : ' - ' + sUON2) + (sUON3 == undefined ? '' : ' - ' + sUON3) + ' - ' + sDen);
            $('[id$=txtUnidadOrganizativa]').prop('title', sUON1 + (sUON2 == undefined ? '' : ' - ' + sUON2) + (sUON3 == undefined ? '' : ' - ' + sUON3) + ' - ' + sDen);
            MostrarUONs();
            CargarTablaAtributos(sUON1, sUON2, sUON3, 1);
        };
    </script>
</body>
</html>
