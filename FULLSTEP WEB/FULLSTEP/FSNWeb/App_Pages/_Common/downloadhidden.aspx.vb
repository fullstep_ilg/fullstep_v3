
Public Class downloadhidden
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    Protected WithEvents txtPath As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents idContrato As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents idArchivoContrato As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents instancia As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtNombre As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object
#End Region

    Dim sPath As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        Dim mpath As String = ConfigurationManager.AppSettings("ruta")
        Dim servidor As String = Request.ServerVariables("SERVER_NAME")
        mpath = Replace(mpath, "http://" & servidor, "")
        mpath = Replace(mpath, "https://" & servidor, "")

        If Server.UrlDecode(Request("nombre")) = "MemoriaJustificativa.pdf" And Not Request("MemoriaJustificativa") Is Nothing Then
            While Session("MemoriaJustificativa_DataSize") = Nothing
                Threading.Thread.Sleep(500)
            End While
            Dim sPathMemoriaJustificativa As String = Session("MemoriaJustificativa").ToString
            Dim sFicheroMemoriaJustificativa As String = sPathMemoriaJustificativa.Substring(sPathMemoriaJustificativa.IndexOf("\") + 1, sPathMemoriaJustificativa.Length - sPathMemoriaJustificativa.IndexOf("\") - 1)
            sPath = mpath & "download/" & sPathMemoriaJustificativa.Substring(0, sPathMemoriaJustificativa.IndexOf("\")) & "/"
            Me.txtPath.Value = Server.MapPath(sPath)
            Me.txtNombre.Value = Server.UrlEncode(sFicheroMemoriaJustificativa)
            Me.idContrato.Value = ""

        Else
            sPath = mpath & "download/" & Request("path") & "/"
            Me.txtPath.Value = Server.MapPath(sPath)
            Me.txtNombre.Value = Server.UrlEncode(Request("nombre"))
            Me.idContrato.Value = Request("idContrato")
            Me.idArchivoContrato.Value = DBNullToInteger(Request("idArchivoContrato"))
            Me.instancia.Value = Request("instancia")
        End If
    End Sub
End Class