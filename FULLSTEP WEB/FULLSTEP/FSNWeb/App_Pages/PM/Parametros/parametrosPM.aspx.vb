﻿Imports Fullstep.FSNServer
Partial Public Class parametrosPM
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack() Then
            Exit Sub
        End If

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Opciones

        Me.btnAceptar.Text = Textos(0)
        Me.lblTitulo.Text = Textos(31)
        Me.lblProvMat.Text = Textos(28) & ":"
        Me.lblNumeroFilas.Text = Textos(32) & ":"

        ddlMostrarDefProvMat.Items.Add(CrearItem("0", Textos(29)))
        ddlMostrarDefProvMat.Items.Add(CrearItem("1", Textos(30)))
        ddlMostrarDefProvMat.SelectedValue = IIf(FSNUser.PMMostrarDefProvMat, "1", "0")

        txtNumeroFilas.Text = FSNUser.PMNumeroFilas
    End Sub

    Private Function CrearItem(ByVal Valor As String, ByVal Texto As String) As System.Web.UI.WebControls.ListItem
        Dim iListItem As New System.Web.UI.WebControls.ListItem
        iListItem.Value = Valor
        iListItem.Text = Texto
        CrearItem = iListItem
    End Function

    Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        FSNUser.PMMostrarDefProvMat = ddlMostrarDefProvMat.SelectedValue
        If DBNullToDbl(txtNumeroFilas.Text) < 10 Then
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.Opciones
            Me.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Minimo", "alert('" + Me.Textos(33) + "');", True)
            Exit Sub
        End If
        FSNUser.PMNumeroFilas = txtNumeroFilas.Text
        FSNUser.Actualizar_ParametrosPM(ddlMostrarDefProvMat.SelectedValue, txtNumeroFilas.Text)

        ' Cerramos la ventana del cliente
        Me.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Close", "window.close();", True)
    End Sub
End Class