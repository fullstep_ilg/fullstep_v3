﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="parametrosPM.aspx.vb" Inherits="Fullstep.FSNWeb.parametrosPM" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
</head>
<body style="background-color: white">
    <form id="form1" runat="server">
    <div>
    <table border="0" cellpadding="4">
    <tr><td valign="top"><asp:Image ID="imgParametros" runat="server" SkinID="Parametros" /></td><td valign="middle" colspan="2"><asp:label id="lblTitulo" runat="server" CssClass="RotuloGrande">DParámetros</asp:label></TD>
	</tr>
	<tr><td rowspan="3"></td><td valign="middle" style="WIDTH: 495px"><asp:label id="lblProvMat" runat="server" CssClass="Listado" Text="DMostrar por defecto sólo los proveedores favoritos del material:"></asp:label></td>
					<td valign="middle">
					<asp:DropDownList runat="server" ID="ddlMostrarDefProvMat" Width="50px">
					</asp:DropDownList>
					</td>
					
				</tr>
    <tr><td valign="middle" style="WIDTH: 495px"><asp:label id="lblNumeroFilas" runat="server" CssClass="Listado" Text="DNúmero de filas por página:"></asp:label></td>
		<td valign="middle"><asp:TextBox ID="txtNumeroFilas" runat="server" Width="50px"></asp:TextBox></td>					
	</tr>
    <tr><td align="center" colspan="2" height="100px" valign="bottom">
    <table><tr><td><fsn:FSNButton ID="btnAceptar" runat="server" Text="Aceptar" Alineacion="Left" /></td></tr></table>
        </td>
		</TR>
	   
    </table>
    </div>
    </form>
</body>
</html>
