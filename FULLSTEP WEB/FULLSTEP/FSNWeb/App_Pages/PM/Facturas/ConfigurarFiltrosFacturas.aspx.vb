﻿Imports Infragistics.Web.UI.GridControls
Imports Infragistics.Web.UI
Imports System.Web.UI


Public Class ConfigurarFiltrosFacturas
    Inherits FSNPage

    ''' <summary>
    ''' Pone el texto correspondiente en la etiqueta "Cargando..."
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: El prerender de la etiqueta. Tiempo máximo: 0sg.</remarks>
    Protected Sub LblProcesando_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, Label).Text = Textos(33)
    End Sub

    ''' <summary>
    ''' Se pone el texto en el idioma correspondiente en el botón Aprobar
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: Al dibujarse el botón. Tiempo máximo: 0sg</remarks>
    Protected Sub btnAprobar_OnPreRender(ByVal sender As Object, ByVal e As EventArgs)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltrosFacturas
        CType(sender, FSNWebControls.FSNButton).Text = Textos(28)
    End Sub

    ''' <summary>
    ''' Se pone el texto en el idioma correspondiente en el botón Rechazar
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: Al dibujarse el botón. Tiempo máximo: 0sg</remarks>
    Protected Sub btnRechazar_OnPreRender(ByVal sender As Object, ByVal e As EventArgs)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltrosFacturas
        CType(sender, FSNWebControls.FSNButton).Text = Textos(29)
    End Sub

    ''' <summary>
    ''' Carga de la pagina
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>    
    ''' <remarks>Llamada desde: Sistema; Tiempo máximo:1seg.</remarks> 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltrosFacturas

        Busqueda.RutaBuscadorArticulo = ConfigurationManager.AppSettings("rutaFS") & "_common/BuscadorArticulos.aspx?desde=Visor"
        Busqueda.PropiedadesBuscadorArticulo = "width=850,height=660,status=yes,resizable=no,top=50,left=200"
        Busqueda.RutaBuscadorGestor = ConfigurationManager.AppSettings("rutaFS") & "_Common/BuscadorUsuarios.aspx"
        Busqueda.RutaBuscadorCentroCoste = ConfigurationManager.AppSettings("rutaFS") & "_Common/BuscadorCentrosCoste.aspx"
        Busqueda.RutaBuscadorPartidas = ConfigurationManager.AppSettings("rutaFS") & "_Common/BuscadorPartidas.aspx"
        Busqueda.RutaWsAutoComplete = ConfigurationManager.AppSettings("rutaFS") & "_Common/App_Services/Autocomplete.asmx"
        Busqueda.FilaProveedorEmpresaVisible = True

        FSNPageHeader.UrlImagenCabecera = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/funnel_icon.gif"

        If Not Me.IsPostBack Then

            ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorFacturas
            Busqueda.Textos = TextosModuloCompleto
            Busqueda.AccesoFSSM = Acceso.gbAccesoFSSM
            Busqueda.OblCodPedDir = Acceso.gbOblCodPedDir
            Busqueda.EstadosFacturas = FSNWeb.modFacturas.DevolverEstados(Idioma).Tables(0)
            Busqueda.PartidasDataTable = FSNWeb.modFacturas.DevolverPartidas(Idioma)

            ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltrosFacturas
            CargarTextos()

            FSNPageHeader.VisibleBotonVolver = True
            FSNPageHeader.OnClientClickVolver = "window.location='VisorFacturas.aspx';return false;"

            InicializarParametrosBusqueda()

            If Request("IDFiltroConfigurando") <> "" Then
                Dim f As Fullstep.FSNServer.FiltroFactura = FSNServer.Get_Object(GetType(FSNServer.FiltroFactura))
                f.IDFiltroUsuario = Request("IDFiltroConfigurando")
                f.Persona = Usuario.CodPersona
                f.Idioma = Usuario.Idioma
                f.LoadFiltro()
                If f.IsLoaded Then
                    Session("IDFiltroFactura") = f.IDFiltro
                    Session("IDFiltroFacturaUsuario") = f.IDFiltroUsuario
                    'Session("NombreFiltroFactura") = Request.QueryString("NombreTabla")
                    Session("FiltroFactura") = f

                    FSNPageHeader.TituloCabecera = f.NombreFiltro
                    txtNombreFiltro.Text = f.NombreFiltro
                    chkFiltroPorDefecto.Checked = f.PorDefecto

                    CargarBusquedaAvanzada()
                    CargarConfiguracionCamposGenerales()



                    Dim ds As DataSet = f.LoadConfiguracionInstancia()

                    If ds.Tables(0).Rows.Count = 0 Then
                        ds.Tables(0).Rows.Add()
                    End If

                    With whgConfigurarFiltros
                        .Rows.Clear()
                        .GridView.ClearDataSource()
                        CreacionEdicionColumnasGrid()
                        .DataSource = ds
                        .GridView.DataSource = .DataSource

                        '.DataMember = "ContratosFiltrados"
                        .DataKeyFields = "ID"
                        .DataBind()

                    End With


                    phConfiguracionFiltro.Style("display") = "block"
                    btnEliminarFiltro.Visible = True
                    btnAceptarFiltro.Visible = True
                    btnCancelarFiltro.Visible = True

                    rbTodosFormularios.Visible = False
                    rbUnFormulario.Visible = False
                    wddFormularios.Visible = False

                    btnEliminarFiltro.Attributes.Add("onClick", "return ConfirmarEliminar('" & JSText(Textos(31)) & "');")
                Else
                    Response.Redirect("..\Facturas\VisorFacturas.aspx")
                End If
            Else
                rbUnFormulario.Checked = True
                CargarFiltrosDisponibles(TiposDeDatos.TipoDeSolicitud.Autofactura)
                Session("IDFiltroFactura") = ""
                Session("IDFiltroFacturaUsuario") = ""
            End If
        End If

        Accion.Value = ""
    End Sub

    '''' <summary>
    '''' Carga la lista con los formularios disponibles para el usuario
    '''' </summary>
    '''' <remarks>Page_Load()</remarks>
    Private Sub CargarFiltrosDisponibles(ByVal iTipoSolicitud As Byte)
        Dim cFiltros As Fullstep.FSNServer.Filtros
        cFiltros = FSNServer.Get_Object(GetType(FSNServer.Filtros))
        wddFormularios.DataSource = cFiltros.LoadDisponibles(Usuario.CodPersona, iTipoSolicitud, Me.Acceso.gbPymes, Me.FSNUser.UON1, Me.FSNUser.UON2, Me.FSNUser.UON3)
        wddFormularios.TextField = "DEN"
        wddFormularios.ValueField = "ID"
        wddFormularios.DataBind()
    End Sub

    ''' <summary>
    ''' Inicializa un dataset con los campos generales y se almacena en cache
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load. Tiempo máximo: 0sg.</remarks>
    Private Function ObtenerEstructuraCamposGenerales() As DataSet
        Dim campos As DataSet
        If Not HttpContext.Current.Cache("CAMPOS_FACT_GEN_" & Idioma) Is Nothing Then
            campos = CType(HttpContext.Current.Cache("CAMPOS_FACT_GEN_" & Idioma), DataSet)
        Else
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltrosFacturas

            campos = New DataSet
            Dim dt As DataTable = campos.Tables.Add("CAMPOS")
            dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("CAMPO_ID", System.Type.GetType("System.String"))
            dt.Columns.Add("NOMBRE", System.Type.GetType("System.String"))
            dt.Columns.Add("TAMANYO", System.Type.GetType("System.Int32"))
            dt.Rows.Add(New Object() {ConfiguracionFiltrosFacturas.NumeroFactura, "NUM_FACTURA", Textos(15), 130})
            dt.Rows.Add(New Object() {ConfiguracionFiltrosFacturas.FechaFactura, "FECHA_FACTURA", Textos(21), 120})
            dt.Rows.Add(New Object() {ConfiguracionFiltrosFacturas.Empresa, "EMPRESA", Textos(20), 70})
            dt.Rows.Add(New Object() {ConfiguracionFiltrosFacturas.FechaContabilizacion, "FECHA_CONTABILIZACION", Textos(22), 150})
            dt.Rows.Add(New Object() {ConfiguracionFiltrosFacturas.CodigoProveedor, "COD_PROVE", Textos(16), 130})
            dt.Rows.Add(New Object() {ConfiguracionFiltrosFacturas.NumeroFacturaSAP, "NUM_FACTURA_SAP", "Numero factura SAP", 130})
            dt.Rows.Add(New Object() {ConfiguracionFiltrosFacturas.DenominacionProveedor, "DEN_PROVE", Textos(17), 160})
            dt.Rows.Add(New Object() {ConfiguracionFiltrosFacturas.SituacionActual, "SITUACION_ACTUAL", Textos(26), 120})
            dt.Rows.Add(New Object() {ConfiguracionFiltrosFacturas.Importe, "IMPORTE", Textos(18), 70})
            dt.Rows.Add(New Object() {ConfiguracionFiltrosFacturas.Pago, "PAGO", "Pago", 70})
            dt.Rows.Add(New Object() {ConfiguracionFiltrosFacturas.Estado, "ESTADO", Textos(19), 70})
            dt.Rows.Add(New Object() {ConfiguracionFiltrosFacturas.Monitorizacion, "MONITORIZACION", "Monitorización", 100})
            dt.Rows.Add(New Object() {ConfiguracionFiltrosFacturas.TipoFactura, "TIPO_FACTURA", "Tipo factura", 100})

            Me.InsertarEnCache("CAMPOS_FACT_GEN_" & Idioma, campos)
        End If
        Return campos
    End Function

    ''' <summary>
    ''' Carga los textos de los controles con el idioma del usuario.
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load. Tiempo máximo: 0sg.</remarks>
    Private Sub CargarTextos()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltrosFacturas

        FSNPageHeader.TituloCabecera = Textos(0)

        lblNombreFiltro.Text = String.Format("{0}:", Textos(1))
        chkFiltroPorDefecto.Text = Textos(2)
        rbTodosFormularios.Text = Textos(3)
        rbUnFormulario.Text = Textos(4)
        btnEliminarFiltro.Text = Textos(30)
        btnAceptarFiltro.Text = Textos(6)
        btnCancelarFiltro.Text = Textos(7)
        lblTituloBusqueda.Text = Textos(8)

        lblTituloCampos.Text = Textos(9)
        lblTituloCamposGenerales.Text = String.Format("{0}:", Textos(10))
        lblTituloGridConfiguracion.Text = Textos(27)
        rfvNombreFiltro.ErrorMessage = Textos(32)
    End Sub

    ''' <summary>
    ''' Inicializar los valores de los parámetros de búsqueda
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load</remarks>
    Private Sub InicializarParametrosBusqueda()
        Busqueda.FechaFacturaDesde = Nothing
        Busqueda.FechaFacturaHasta = Nothing
        Busqueda.FechaContabilizacionDesde = Nothing
        Busqueda.FechaContabilizacionHasta = Nothing
        Busqueda.ImporteDesde = Nothing
        Busqueda.ImporteHasta = Nothing
        'estado, partidas y centro de coste ¿?
        Busqueda.CentroCosteHidden = Nothing
        Busqueda.AnyoPedido = Nothing
        Busqueda.NumCesta = Nothing
        Busqueda.NumPedido = Nothing
        Busqueda.ArticuloHidden = Nothing
        Busqueda.ArticuloTexto = Nothing
        Busqueda.GestorHidden = Nothing
        Busqueda.NumeroAlbaran = Nothing
        Busqueda.NumeroFacturaSAP = Nothing
        Busqueda.PedidoERP = Nothing
        Busqueda.FacturaOriginal = True
        Busqueda.FacturaRectificativa = True

        upBusquedaAvanzada.Update()
    End Sub

    ''' <summary>
    ''' Cuando se selecciona un formulario, se cargan los campos y se chequean los campos generales marcados por defecto.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: Al cambiar el dropdownlist con los formularios. Tiempo máximo: 1 sg.</remarks>
    Private Sub wddFormularios_SelectionChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownSelectionChangedEventArgs) Handles wddFormularios.SelectionChanged
        If rbUnFormulario.Checked Then
            If IsNumeric(wddFormularios.SelectedValue) Then
                Dim selectedID As Integer = CInt(wddFormularios.SelectedValue)
                If Session("IDFiltroFactura") <> selectedID.ToString() Then
                    If selectedID > 0 Then
                        InicializarParametrosBusqueda()
                        upBusquedaAvanzada.Update()

                        Session("IDFiltroFactura") = selectedID

                        'campos generales
                        CargarConfiguracionPorDefectoCamposGenerales()

                        Dim f As Fullstep.FSNServer.FiltroFactura = FSNServer.Get_Object(GetType(FSNServer.FiltroFactura))



                        Dim ds As DataSet = f.LoadConfiguracionInstancia()

                        If ds.Tables(0).Rows.Count = 0 Then
                            ds.Tables(0).Rows.Add()
                        End If

                        With whgConfigurarFiltros
                            .Rows.Clear()
                            .GridView.ClearDataSource()
                            CreacionEdicionColumnasGrid()
                            .DataSource = ds
                            .GridView.DataSource = .DataSource

                            '.DataMember = "ContratosFiltrados"
                            '.DataKeyFields = "ID"
                            .DataBind()

                        End With


                        phConfiguracionFiltro.Style("display") = "block"
                        btnAceptarFiltro.Visible = True
                        btnCancelarFiltro.Visible = True

                        upCampos.Update()
                    End If
                End If
            Else
                phConfiguracionFiltro.Style("display") = "none"
                btnAceptarFiltro.Visible = False
                btnCancelarFiltro.Visible = False
            End If

            upBotones.Update()
        End If
    End Sub

    ''' <summary>
    ''' Carga la configuracion por defecto para los campos generales de la factura
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load</remarks>
    Private Sub CargarConfiguracionPorDefectoCamposGenerales()
        Dim camposGenerales As DataSet = ObtenerEstructuraCamposGenerales()

        rpCamposGenerales.DataSource = camposGenerales
        rpCamposGenerales.DataBind()

        Dim chkCampoGeneral As CheckBox
        Dim txtNombre As TextBox
        For i = 0 To rpCamposGenerales.Items.Count - 1
            chkCampoGeneral = CType(rpCamposGenerales.Items(i).FindControl("CampoGeneral_Visible"), CheckBox)
            chkCampoGeneral.Attributes.Add("CampoGeneralID", camposGenerales.Tables("CAMPOS").Rows(i)("CAMPO_ID"))
            chkCampoGeneral.Checked = True
            txtNombre = CType(rpCamposGenerales.Items(i).FindControl("CampoGeneral_NombrePersonalizado"), TextBox)
            txtNombre.Attributes.Add("CampoGeneralID", camposGenerales.Tables("CAMPOS").Rows(i)("CAMPO_ID"))
        Next
    End Sub

    ''' <summary>
    ''' Carga los campos generales del filtro especificado
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load.</remarks>
    Private Sub CargarConfiguracionCamposGenerales()
        If Not Session("FiltroFactura") Is Nothing Then
            Dim f As FSNServer.FiltroFactura = CType(Session("FiltroFactura"), FSNServer.FiltroFactura)
            If Not f.CamposGenerales Is Nothing Then
                Dim view As New DataView(f.CamposGenerales)
                view.Sort = "CAMPO"

                Dim camposGenerales As DataSet = ObtenerEstructuraCamposGenerales()

                rpCamposGenerales.DataSource = camposGenerales
                rpCamposGenerales.DataBind()

                Dim chkCampoGeneral As CheckBox
                Dim txtNombre As TextBox
                For i = 0 To rpCamposGenerales.Items.Count - 1
                    chkCampoGeneral = CType(rpCamposGenerales.Items(i).FindControl("CampoGeneral_Visible"), CheckBox)
                    chkCampoGeneral.Attributes.Add("CampoGeneralID", camposGenerales.Tables("CAMPOS").Rows(i)("CAMPO_ID"))
                    txtNombre = CType(rpCamposGenerales.Items(i).FindControl("CampoGeneral_NombrePersonalizado"), TextBox)
                    txtNombre.Attributes.Add("CampoGeneralID", camposGenerales.Tables("CAMPOS").Rows(i)("CAMPO_ID"))
                    Dim index As Integer = view.Find(camposGenerales.Tables("CAMPOS").Rows(i)("ID"))
                    If index >= 0 Then
                        chkCampoGeneral.Checked = view(index)("VISIBLE")
                        If chkCampoGeneral.Checked = True And UCase(DBNullToStr(view(index)("NOMBRE"))) <> UCase(DBNullToStr(camposGenerales.Tables(0).Rows(i).Item("NOMBRE"))) Then
                            txtNombre.Text = DBNullToStr(view(index)("NOMBRE"))
                        Else
                            txtNombre.Text = ""
                        End If
                    End If
                Next
            End If
        End If
    End Sub

    ''' <summary>
    ''' Se pueden cambiar el orden de las columnas en la grid. Se pueden cambiar todos los campos menos las dos
    ''' primeras columnas que son las de Aprobar y Rechazar. Siempre tienen que ser la posición 1 y 2.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>LLamada desde: Al pinchar en una columna y arrastrarla. Tiempo máximo: 0 sg.</remarks>

    Private Sub whgConfigurarFiltros_ColumnMoved(sender As Object, e As Infragistics.Web.UI.GridControls.ColumnMovingEventArgs) Handles whgConfigurarFiltros.ColumnMoved
        If e.Column.VisibleIndex < 2 Then
            e.Column.VisibleIndex = e.PreviousVisibleIndex
        End If
    End Sub


    'Private Sub ActualizarGrid(ByVal sender As Object, ByVal e As System.EventArgs)

    ''' <summary>
    ''' Actualiza el grid de configuración de campos. 
    ''' </summary>
    ''' <remarks>Llamada desde:ConfigurarFiltrosContratos_LoadComplete; Tiempo máximo:0,4seg.</remarks>
    Private Sub ActualizarGrid()
        Dim index As Integer
        Dim chkCampoVisible As CheckBox
        Dim txtCampoNombre As TextBox
        Dim key As String
        'Recorrer CamposGenerales
        For index = 0 To rpCamposGenerales.Items.Count - 1
            chkCampoVisible = CType(rpCamposGenerales.Items(index).FindControl("CampoGeneral_Visible"), CheckBox)
            txtCampoNombre = CType(rpCamposGenerales.Items(index).FindControl("CampoGeneral_NombrePersonalizado"), TextBox)
            key = chkCampoVisible.Attributes("CampoGeneralID")
            With whgConfigurarFiltros.GridView.Columns(key)
                .Hidden = Not chkCampoVisible.Checked
                If chkCampoVisible.Checked = True Then
                    .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                    .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                End If
                If txtCampoNombre.Text <> "" Then
                    .Header.Text = txtCampoNombre.Text


                Else
                    .Header.Text = CType(rpCamposGenerales.Items(index).FindControl("CampoGeneral_lblCampoGeneral"), Label).Text


                End If
            End With
        Next

        upGrid.Update()

    End Sub

    ''' <summary>
    ''' Evento que salta al cargarse completamente la pagina. La variable Accion me indica que evento a pulsado el usuario.
    ''' </summary>
    ''' <param name="sender">las del evento</param>
    ''' <param name="e">las del evento</param>
    ''' <remarks>Al no poder programar el evento del check del repeater este evento nos sirve para ejecutar código dependiendo del valor
    ''' almacenado en la variable Accion.</remarks>        
    Private Sub ConfigurarFiltrosFacturas_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Me.IsPostBack Then
            If Accion.Value = "" Then
                ActualizarGrid()
            End If
        End If
        Accion.Value = ""
    End Sub

    ''' <summary>
    ''' Al pulsar el botón Cancelar se cierra la configuración de filtros sin hacer ningun cambio y se muestra la página del visor de facturas
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelarFiltro.Click
        Dim sURL As String = "VisorFacturas.aspx"
        If Request("IDFiltroConfigurando") <> "" Then
            Dim stringRequest As String = "FiltroUsuarioIdAnt=" & Session("IDFiltroFacturaUsuario") & "&FiltroIdAnt=" & Session("IDFiltroFactura")
            sURL = sURL & "?" & stringRequest
        End If
        Response.Redirect(sURL)
        Accion.Value = "Cancelar"
    End Sub

    ''' <summary>
    ''' Al pulsar el botón Aceptar, se guarda el nombre del filtro, los campos que ha marcado como visibles y el tamaño de los mismos,
    ''' y se muestra el visor de facturas con el filtro actualizado.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarFiltro.Click
        If Page.IsValid Then
            Dim f As Fullstep.FSNServer.FiltroFactura = FSNServer.Get_Object(GetType(FSNServer.FiltroFactura))
            If Not IsNumeric(Session("IDFiltroFacturaUsuario")) Then
                If rbUnFormulario.Checked Then
                    f.IDFiltro = CInt(wddFormularios.SelectedValue)
                Else
                    'IDFiltro = 1 es el ID del filtro para todos los formularios
                    f.IDFiltro = 1
                End If
                f.IDFiltroUsuario = 0
            Else
                f.IDFiltro = CInt(Session("IDFiltroFactura"))
                f.IDFiltroUsuario = CInt(Session("IDFiltroFacturaUsuario"))
            End If
            f.NombreFiltro = txtNombreFiltro.Text
            f.Persona = Usuario.CodPersona

            Dim ds As New DataSet
            Dim dt As DataTable = ds.Tables.Add("CAMPOSCONFIGURACION")
            dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("VISIBLE", System.Type.GetType("System.Int32"))
            dt.Columns.Add("POSICION", System.Type.GetType("System.Int32"))
            dt.Columns.Add("TAMANYO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("NOMBRE", System.Type.GetType("System.String"))

            With whgConfigurarFiltros.GridView.Columns
                For columnIndex As Integer = 0 To .Count - 1
                    Dim index As Nullable(Of Integer)
                    index = Nothing

                    Select Case .Item(columnIndex).Key
                        Case "NUM_FACTURA"
                            index = ConfiguracionFiltrosFacturas.NumeroFactura
                        Case "EMPRESA"
                            index = ConfiguracionFiltrosFacturas.Empresa
                        Case "COD_PROVE"
                            index = ConfiguracionFiltrosFacturas.CodigoProveedor
                        Case "DEN_PROVE"
                            index = ConfiguracionFiltrosFacturas.DenominacionProveedor
                        Case "IMPORTE"
                            index = ConfiguracionFiltrosFacturas.Importe
                        Case "ESTADO"
                            index = ConfiguracionFiltrosFacturas.Estado
                        Case "TIPO_FACTURA"
                            index = ConfiguracionFiltrosFacturas.TipoFactura
                        Case "FECHA_FACTURA"
                            index = ConfiguracionFiltrosFacturas.FechaFactura
                        Case "FECHA_CONTABILIZACION"
                            index = ConfiguracionFiltrosFacturas.FechaContabilizacion
                        Case "NUM_FACTURA_SAP"
                            index = ConfiguracionFiltrosFacturas.NumeroFacturaSAP
                        Case "SITUACION_ACTUAL"
                            index = ConfiguracionFiltrosFacturas.SituacionActual
                        Case "PAGO"
                            index = ConfiguracionFiltrosFacturas.Pago
                        Case "MONITORIZACION"
                            index = ConfiguracionFiltrosFacturas.Monitorizacion
                    End Select

                    If index.HasValue Then
                        Dim newRow As DataRow = dt.NewRow
                        newRow.Item("ID") = index
                        newRow.Item("VISIBLE") = Not whgConfigurarFiltros.GridView.Columns.Item(columnIndex).Hidden
                        newRow.Item("POSICION") = whgConfigurarFiltros.GridView.Columns.Item(columnIndex).VisibleIndex

                        If (newRow.Item("VISIBLE") = 1) And (whgConfigurarFiltros.GridView.Columns.Item(columnIndex).Width.Value = 0) Then
                            whgConfigurarFiltros.GridView.Columns.Item(columnIndex).Width = 100
                        End If

                        newRow.Item("TAMANYO") = whgConfigurarFiltros.GridView.Columns.Item(columnIndex).Width.Value
                        Dim nombre As String = ObtenerTextoPersonalizadoCampoGeneral(.Item(columnIndex).Key)
                        If Not String.IsNullOrEmpty(nombre) Then
                            newRow.Item("NOMBRE") = nombre
                        Else
                            newRow.Item("NOMBRE") = whgConfigurarFiltros.GridView.Columns.Item(columnIndex).Header.Text
                        End If
                        dt.Rows.Add(newRow)
                    End If
                Next
            End With

            dt = ds.Tables.Add("CAMPOSBUSQUEDA")
            dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("FEC_FACT_DESDE", System.Type.GetType("System.DateTime"))
            dt.Columns.Add("FEC_FACT_HASTA", System.Type.GetType("System.DateTime"))
            dt.Columns.Add("FEC_CONT_DESDE", System.Type.GetType("System.DateTime"))
            dt.Columns.Add("FEC_CONT_HASTA", System.Type.GetType("System.DateTime"))
            dt.Columns.Add("ALBARAN", System.Type.GetType("System.String"))
            dt.Columns.Add("ESTADO", System.Type.GetType("System.String"))
            dt.Columns.Add("NUM_FACTURA_ERP", System.Type.GetType("System.String"))
            dt.Columns.Add("NUM_PEDIDO_ERP", System.Type.GetType("System.String"))
            dt.Columns.Add("IMPORTE_DESDE", System.Type.GetType("System.Decimal"))
            dt.Columns.Add("IMPORTE_HASTA", System.Type.GetType("System.Decimal"))
            dt.Columns.Add("ART", System.Type.GetType("System.String"))
            dt.Columns.Add("GESTOR", System.Type.GetType("System.String"))
            dt.Columns.Add("EMPRESA", System.Type.GetType("System.String"))
            dt.Columns.Add("CENTRO_COSTE", System.Type.GetType("System.String"))
            dt.Columns.Add("PARTIDA", System.Type.GetType("System.String"))
            dt.Columns.Add("PROVE", System.Type.GetType("System.String"))
            dt.Columns.Add("TIPO_ORIGINAL", System.Type.GetType("System.Int32"))
            dt.Columns.Add("TIPO_RECTIFICATIVA", System.Type.GetType("System.Int32"))
            dt.Columns.Add("ANYO_PEDIDO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("CESTA_PEDIDO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("NUM_PEDIDO", System.Type.GetType("System.Int32"))

            Dim newRow1 As DataRow = dt.NewRow
            If (Busqueda.FechaFacturaDesde <> CDate(#12:00:00 AM#)) Then
                newRow1.Item("FEC_FACT_DESDE") = Busqueda.FechaFacturaDesde
            End If
            If (Busqueda.FechaFacturaHasta <> CDate(#12:00:00 AM#)) Then
                newRow1.Item("FEC_FACT_HASTA") = Busqueda.FechaFacturaHasta
            End If
            If (Busqueda.FechaContabilizacionDesde <> CDate(#12:00:00 AM#)) Then
                newRow1.Item("FEC_CONT_DESDE") = Busqueda.FechaContabilizacionDesde
            End If
            If (Busqueda.FechaContabilizacionHasta <> CDate(#12:00:00 AM#)) Then
                newRow1.Item("FEC_CONT_HASTA") = Busqueda.FechaContabilizacionHasta
            End If
            If Not String.IsNullOrEmpty(Busqueda.NumeroAlbaran) Then
                newRow1.Item("ALBARAN") = Busqueda.NumeroAlbaran
            End If
            If Not String.IsNullOrEmpty(Busqueda.Estado) Then
                newRow1.Item("ESTADO") = Busqueda.Estado
            End If
            If Not String.IsNullOrEmpty(Busqueda.NumeroFacturaSAP) Then
                newRow1.Item("NUM_FACTURA_ERP") = Busqueda.NumeroFacturaSAP
            End If
            If Not String.IsNullOrEmpty(Busqueda.PedidoERP) Then
                newRow1.Item("NUM_PEDIDO_ERP") = Busqueda.PedidoERP
            End If
            If IsNumeric(Busqueda.ImporteDesde) Then
                newRow1.Item("IMPORTE_DESDE") = Busqueda.ImporteDesde
            End If
            If IsNumeric(Busqueda.ImporteHasta) Then
                newRow1.Item("IMPORTE_HASTA") = Busqueda.ImporteHasta
            End If
            If Not String.IsNullOrEmpty(Busqueda.ArticuloHidden) Then
                newRow1.Item("ART") = Busqueda.ArticuloHidden
            End If
            If Not String.IsNullOrEmpty(Busqueda.GestorHidden) Then
                newRow1.Item("GESTOR") = Busqueda.GestorHidden
            End If
            If Not String.IsNullOrEmpty(Busqueda.EmpresaHidden) Then
                newRow1.Item("EMPRESA") = Busqueda.EmpresaHidden
            End If
            If Not String.IsNullOrEmpty(Busqueda.CentroCosteHidden) Then
                newRow1.Item("CENTRO_COSTE") = Busqueda.CentroCosteHidden
            End If
            'If Not String.IsNullOrEmpty(Busqueda.Partidas) Then
            '    newRow1.Item("PARTIDA") = Busqueda.Partidas
            'End If
            If Not String.IsNullOrEmpty(Busqueda.ProveedorHidden) Then
                newRow1.Item("PROVE") = Busqueda.ProveedorHidden
            End If
            newRow1.Item("TIPO_ORIGINAL") = Busqueda.FacturaOriginal
            newRow1.Item("TIPO_RECTIFICATIVA") = Busqueda.FacturaRectificativa
            If IsNumeric(Busqueda.AnyoPedido) Then
                newRow1.Item("ANYO_PEDIDO") = Busqueda.AnyoPedido
            End If
            If IsNumeric(Busqueda.NumCesta) Then
                newRow1.Item("CESTA_PEDIDO") = Busqueda.NumCesta
            End If
            If IsNumeric(Busqueda.ImporteDesde) Then
                newRow1.Item("NUM_PEDIDO") = Busqueda.NumPedido
            End If
            dt.Rows.Add(newRow1)

            f.PorDefecto = chkFiltroPorDefecto.Checked
            f.CamposConfigurados = ds
            f.GuardarConfiguracion()

            Dim sURL As String = "VisorFacturas.aspx"
            Dim stringRequest As String
            If Request("IDFiltroConfigurando") <> "" Then
                stringRequest = "FiltroUsuarioIdAnt=" & Session("IDFiltroFacturaUsuario") & "&FiltroIdAnt=" & Session("IDFiltroFactura") & "&CargarBusquedaAvanzada=1"
            Else
                stringRequest = "FiltroUsuarioIdAnt=" & f.IDFiltroUsuario & "&FiltroIdAnt=" & f.IDFiltro & "&CargarBusquedaAvanzada=1"
            End If
            sURL = sURL & "?" & stringRequest

            Response.Redirect(sURL)
        End If
    End Sub

    Private Function ObtenerTextoPersonalizadoCampoGeneral(ByVal key As String) As String
        Dim resultText As String = Nothing
        For index As Integer = 0 To rpCamposGenerales.Items.Count - 1
            Dim txtCampoNombre As TextBox = CType(rpCamposGenerales.Items(index).FindControl("CampoGeneral_NombrePersonalizado"), TextBox)
            If key = txtCampoNombre.Attributes("CampoGeneralID") Then
                resultText = txtCampoNombre.Text
                Exit For
            End If
        Next
        Return resultText
    End Function

    Private Sub rpCamposGenerales_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles rpCamposGenerales.DataBinding
        '1 Esta linea es necesaria para que se carguen correctamente los textos traducidos del control de campos generales
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltrosFacturas
    End Sub

    ''' <summary>
    ''' Cargar los valores de los criterios de busqueda avanzada para el filtro actual
    ''' </summary>
    ''' 
    Private Sub CargarBusquedaAvanzada()
        If Not Session("FiltroFactura") Is Nothing Then
            Dim f As FSNServer.FiltroFactura = CType(Session("FiltroFactura"), FSNServer.FiltroFactura)
            If Not f.Busqueda Is Nothing AndAlso f.Busqueda.Rows.Count > 0 Then
                With Busqueda
                    .Filtro = f.IDFiltro

                    .ArticuloHidden = DBNullToStr(f.Busqueda.Rows(0)("ART"))
                    .ArticuloTexto = DBNullToStr(f.Busqueda.Rows(0)("ART_DEN"))
                    .EmpresaHidden = DBNullToStr(f.Busqueda.Rows(0)("EMPRESA"))
                    .EmpresaTexto = DBNullToStr(f.Busqueda.Rows(0)("EMP_DEN"))
                    .Estado = DBNullToInteger(f.Busqueda.Rows(0)("ESTADO"))
                    If IsDate(f.Busqueda.Rows(0)("FEC_CONT_DESDE")) Then
                        .FechaContabilizacionDesde = DBNullToSomething(f.Busqueda.Rows(0)("FEC_CONT_DESDE"))
                    Else
                        .FechaContabilizacionDesde = Nothing
                    End If
                    .FechaContabilizacionHasta = DBNullToSomething(f.Busqueda.Rows(0)("FEC_CONT_HASTA"))
                    .FechaFacturaDesde = DBNullToSomething(f.Busqueda.Rows(0)("FEC_FACT_DESDE"))
                    .FechaFacturaHasta = DBNullToSomething(f.Busqueda.Rows(0)("FEC_FACT_HASTA"))
                    .ImporteDesde = DBNullToDbl(f.Busqueda.Rows(0)("IMPORTE_DESDE"))
                    .ImporteHasta = DBNullToDbl(f.Busqueda.Rows(0)("IMPORTE_HASTA"))
                    .NumeroAlbaran = DBNullToSomething(f.Busqueda.Rows(0)("ALBARAN"))
                    .NumeroFacturaSAP = DBNullToSomething(f.Busqueda.Rows(0)("NUM_FACTURA_ERP"))
                    .PedidoERP = DBNullToSomething(f.Busqueda.Rows(0)("NUM_PEDIDO_ERP"))
                    .GestorHidden = DBNullToSomething(f.Busqueda.Rows(0)("GESTOR"))
                    .GestorTexto = DBNullToSomething(f.Busqueda.Rows(0)("GESTOR_DEN"))
                    'dt.Columns.Add("CENTRO_COSTE", System.Type.GetType("System.String"))
                    'dt.Columns.Add("PARTIDA", System.Type.GetType("System.String"))
                    .ProveedorHidden = DBNullToStr(f.Busqueda.Rows(0)("PROVE"))
                    .ProveedorTexto = DBNullToStr(f.Busqueda.Rows(0)("PROVE_DEN"))
                    .FacturaOriginal = DBNullToSomething(f.Busqueda.Rows(0)("TIPO_ORIGINAL"))
                    .FacturaRectificativa = DBNullToSomething(f.Busqueda.Rows(0)("TIPO_RECTIFICATIVA"))
                    .AnyoPedido = DBNullToInteger(f.Busqueda.Rows(0)("ANYO_PEDIDO"))
                    .NumCesta = DBNullToInteger(f.Busqueda.Rows(0)("CESTA_PEDIDO"))
                    .NumPedido = DBNullToInteger(f.Busqueda.Rows(0)("NUM_PEDIDO"))
                End With
            End If
        End If
    End Sub

    ''' <summary>
    ''' Elimina la configuración del filtro.
    ''' </summary>
    Private Sub btnEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminarFiltro.Click
        Dim f As Fullstep.FSNServer.FiltroFactura = FSNServer.Get_Object(GetType(FSNServer.FiltroFactura))
        If IsNumeric(Session("IDFiltroFacturaUsuario")) Then
            f.EliminarConfiguracion(CInt(Session("IDFiltroFacturaUsuario")))
            Response.Redirect("VisorFacturas.aspx")
            Accion.Value = "Eliminar"
        End If
    End Sub

    Private Sub rbTodosFormularios_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbTodosFormularios.CheckedChanged
        If rbTodosFormularios.Checked Then
            wddFormularios.SelectedItemIndex = 0
            wddFormularios.SelectedValue = ""
            wddFormularios.Enabled = False

            'IDFiltro=1 es el id filtro que se introduce para la opcion de "Todos los formularios"
            Dim selectedID As Integer = 1

            InicializarParametrosBusqueda()

            Session("IDFiltroFactura") = selectedID

            'upBusquedaAvanzada.Update()

            'campos generales
            CargarConfiguracionPorDefectoCamposGenerales()

            'grid de configuracion
            Dim f As Fullstep.FSNServer.FiltroFactura = FSNServer.Get_Object(GetType(FSNServer.FiltroFactura))

            Dim ds As DataSet = f.LoadConfiguracionInstancia()

            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add()
            End If

            With whgConfigurarFiltros
                .Rows.Clear()
                .GridView.ClearDataSource()
                CreacionEdicionColumnasGrid()
                .DataSource = ds
                .GridView.DataSource = .DataSource

                '.DataMember = "ContratosFiltrados"
                .DataKeyFields = "ID"
                .DataBind()

            End With

            phConfiguracionFiltro.Style("display") = "block"
            btnAceptarFiltro.Visible = True
            btnCancelarFiltro.Visible = True

            upCampos.Update()
            upBotones.Update()
        End If
    End Sub

    Private Sub rbUnFormulario_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbUnFormulario.CheckedChanged
        If rbUnFormulario.Checked Then
            'ocultamos el tipo
            'Busqueda.FilaProveedorVisible = True
            'Busqueda.CeldaLabelTipoVisible = True
            'Busqueda.CeldawddTipoVisible = True
            upBusquedaAvanzada.Update()

            wddFormularios.Enabled = True
            wddFormularios.SelectedItemIndex = 0
            wddFormularios.SelectedValue = ""

            phConfiguracionFiltro.Style("display") = "hidden"

            upBotones.Update()
        End If
    End Sub

    Private Sub CreacionEdicionColumnasGrid()

        ObtenerColumnasGrid(whgConfigurarFiltros.UniqueID)

    End Sub

    ''' <summary>
    ''' Inicializa las columnas del grid con los campos generales y los campos personalizados
    ''' </summary>
    ''' <remarks>Llamada desde=Page_Load u// ; Tiempo máximo:0seg.</remarks>
    Private Sub ObtenerColumnasGrid(ByVal gridId As String)

        Dim grid As WebHierarchicalDataGrid = CType(Me.FindControl(gridId), WebHierarchicalDataGrid)

        Dim camposGenerales As DataSet = ObtenerEstructuraCamposGenerales()

        Dim camposGrid As New List(Of Infragistics.Web.UI.GridControls.BoundDataField)
        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField


        If Session("FiltroFactura") Is Nothing Then
            Dim Posicion As Integer = 2
            For Each oRow As DataRow In camposGenerales.Tables("CAMPOS").Rows
                'Tratamiento campos Generales
                'Oculto todos por defecto, luego ya los mostraré
                campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
                With campoGrid
                    .Key = oRow("CAMPO_ID")
                    .DataFieldName = oRow("CAMPO_ID")
                    .Hidden = True
                    .Header.CssClass = "hiddenElement headerNoWrap"
                    .CssClass = "hiddenElement SinSalto"
                    .Header.Text = DBNullToSomething(oRow("NOMBRE"))
                    .Header.Tooltip = DBNullToSomething(oRow("NOMBRE"))
                    .Width = DBNullToDbl(oRow("TAMANYO"))
                End With
                whgConfigurarFiltros.GridView.Columns.Insert(Posicion, campoGrid)
                AnyadirTextoAColumnaNoFiltrada(gridId, campoGrid.Key, True)
                Posicion += 1
                camposGrid.Add(campoGrid)
            Next

            'para añadir los campos de un formulario que se carga desde el combo
        '    If Session("IDFiltroFactura") IsNot Nothing Then

        '        Dim f As FSNServer.FiltroFactura = FSNServer.Get_Object(GetType(FSNServer.FiltroFactura))
        '        Dim ds As DataSet = f.LoadCampos(Usuario.CodPersona, Session("IDFiltroFactura"), Idioma)

        '        For Each field As DataRow In ds.Tables(1).Rows
        '            Dim keyField As String = "C_" & field("ID").ToString()
        '            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
        '            With campoGrid
        '                .Key = keyField
        '                .DataFieldName = keyField
        '                .Hidden = True
        '                .Header.CssClass = "hiddenElement headerNoWrap"
        '                .CssClass = "hiddenElement SinSalto"
        '                .Header.Text = keyField
        '                .Width = 65
        '            End With
        '            whgConfigurarFiltros.GridView.Columns.Insert(Posicion, campoGrid)
        '            AnyadirTextoAColumnaNoFiltrada(gridId, campoGrid.Key, True)
        '            Posicion += 1
        '            camposGrid.Add(campoGrid)
        '        Next
        '    End If
        Else

            Dim f As FSNServer.FiltroFactura = CType(Session("FiltroFactura"), FSNServer.FiltroFactura)
            'campos generales
            For Each oRow As DataRow In camposGenerales.Tables("CAMPOS").Rows
                campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
                Dim view As New DataView(f.CamposGenerales)
                view.Sort = "CAMPO"
                Dim index As Integer = view.Find(oRow("ID"))
                If index >= 0 Then
                    With campoGrid
                        If DBNullToStr(view(index)("NOMBRE")) <> "" Then
                            .Header.Text = view(index)("NOMBRE")
                            .Header.Tooltip = view(index)("NOMBRE")
                        Else
                            .Header.Text = oRow("NOMBRE")
                            .Header.Tooltip = oRow("NOMBRE")
                        End If
                        .Key = oRow("CAMPO_ID")
                        .DataFieldName = oRow("CAMPO_ID")
                        .VisibleIndex = view(index)("POS")
                        .Width = IIf(DBNullToDbl(view(index)("TAMANYO")) = 0, Unit.Pixel(100), Unit.Pixel(view(index)("TAMANYO")))
                        .Hidden = Not CBool(view(index)("VISIBLE"))
                        If CBool(view(index)("VISIBLE")) = True Then
                            .CssClass = "SinSalto"
                            .Header.CssClass = "headerNoWrap"
                        Else
                            .CssClass = "hiddenElement SinSalto"
                            .Header.CssClass = "hiddenElement headerNoWrap"
                        End If
                    End With

                    whgConfigurarFiltros.GridView.Columns.Add(campoGrid)
                    AnyadirTextoAColumnaNoFiltrada(gridId, campoGrid.Key, True)
                    camposGrid.Add(campoGrid)
                End If
            Next
        End If
        Session("arrDatosGeneralesVCont") = camposGrid

        'cFiltro = Nothing
    End Sub

    Private Sub AnyadirTextoAColumnaNoFiltrada(ByVal gridId As String, ByVal fieldKey As String, ByVal EnableResize As Boolean)

        Dim grid As WebHierarchicalDataGrid = CType(Me.FindControl(gridId), WebHierarchicalDataGrid)

        Dim fieldResize As Infragistics.Web.UI.GridControls.ColumnResizeSetting
        fieldResize = New Infragistics.Web.UI.GridControls.ColumnResizeSetting
        fieldResize.ColumnKey = fieldKey
        fieldResize.EnableResize = EnableResize
        grid.GridView.Behaviors.ColumnResizing.ColumnSettings.Add(fieldResize)
    End Sub


    Private Sub rpCamposGenerales_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpCamposGenerales.ItemDataBound
        If e.Item.ItemType = ListItemType.Header Then
            CType(e.Item.FindControl("lblCampoGeneral"), Label).Text = "Campo"
            CType(e.Item.FindControl("lblVisible"), Label).Text = "Visible"
            CType(e.Item.FindControl("lblNombrePersonalizado"), Label).Text = "Nombre personalizado"
            CType(e.Item.FindControl("lblCampoGeneral2"), Label).Text = "Campo"
            CType(e.Item.FindControl("lblVisible2"), Label).Text = "Visible"
            CType(e.Item.FindControl("lblNombrePersonalizado2"), Label).Text = "Nombre personalizado"
        End If
    End Sub
End Class