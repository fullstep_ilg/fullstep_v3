﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.Master" CodeBehind="DetalleFactura.aspx.vb" Inherits="Fullstep.FSNWeb.DetalleFactura" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="head" runat="server">  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
    <div id="divDetalleFactura" runat="server">
        <fsn:FSNPageHeader ID="FSNPageHeader" runat="server" VisibleBotonVolver="true">
        </fsn:FSNPageHeader>
        <div id="divAcciones" style="position:absolute; display:none;">
            <ig:WebDataMenu runat="server" ID="wdmAcciones" EnableScrolling="true" MaxDataBindDepth="1">
                <GroupSettings Orientation="Vertical" EnableAnimation="true"
                    AnimationType="OpacityAnimation" AnimationDuration="500" />               
            </ig:WebDataMenu>            
        </div>
        <p>
            <br />
        </p>
        <p>
            <br />
        </p>
        <fspm:DatosGeneralesFactura ID="DatosGenerales" runat="server"></fspm:DatosGeneralesFactura>
        <p>
            <br />
        </p>
        <fspm:LineasFactura ID="Lineas" runat="server"></fspm:LineasFactura>
        <p>
            <br />
        </p>
        <fspm:NotificacionesFactura ID="HistoricoNotificaciones" runat="server"></fspm:NotificacionesFactura>
        <p>
            <br />
        </p>
    </div>
    <div id="divDevolverSiguientesEtapas" style="display: none; width: 100%; height: 100%;
        float: left" runat="server">
        <asp:UpdatePanel ID="upDevolverSiguientesEtapas" runat="server" UpdateMode="Conditional"
            ChildrenAsTriggers="false">
            <ContentTemplate>
                <div id="divPageHeader" style="clear: both; float: left; width: 100%; position: relative;
                    z-index: 0;">
                    <div id="divPageHeaderBack" class="PageHeaderBackground">
                    </div>
                    <div id="divPageHeaderImagen" style="float: left; width: 32px; height: 30px; padding-left: 10px;
                        padding-right: 5px;">
                        <img src="" alt="" id="imgCabeceraConfirmacion" style="max-width: 32px; max-height: 30px;"
                            runat="server" />
                    </div>
                    <div id="divPageHeaderTitulo" style="float: left; margin-top: 15px; white-space: nowrap;
                        overflow: hidden;">
                        <asp:Label ID="lblCabeceraConfirmacion" runat="server" CssClass="LabelTituloCabeceraCustomControl"></asp:Label>
                    </div>
                </div>
                <table id="tblGeneral" style="width: 97%; float: left" border="0" runat="server">
                    <tbody>
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblEtapas" runat="server" CssClass="Etiqueta"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ListBox ID="lstEtapas" Width="100%" runat="server"></asp:ListBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 25px" valign="bottom" align="left">
                                <asp:Label ID="lblRoles" runat="server" CssClass="Etiqueta"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ig:WebHierarchicalDataGrid ID="whdgRoles" Width="100%" Height="60px" runat="server"
                                    AutoGenerateColumns="false" ShowFooter="false" ShowHeader="true" EnableAjax="true">
                                    <Columns>
                                        <ig:BoundDataField Key="DEN" DataFieldName="DEN" Width="35%"></ig:BoundDataField>
                                        <ig:BoundDataField Key="NOMBRE" DataFieldName="NOMBRE" Width="60%"></ig:BoundDataField>
                                        <ig:BoundDataField Key="ROL" DataFieldName="ROL" Hidden="true"></ig:BoundDataField>
                                        <ig:BoundDataField Key="PER" DataFieldName="PER" Hidden="true"></ig:BoundDataField>
                                        <ig:BoundDataField Key="PROVE" DataFieldName="PROVE" Hidden="true"></ig:BoundDataField>
                                        <ig:BoundDataField Key="COMO_ASIGNAR" DataFieldName="COMO_ASIGNAR" Hidden="true"></ig:BoundDataField>
                                        <ig:BoundDataField Key="CON" DataFieldName="CON" Hidden="true"></ig:BoundDataField>
                                        <ig:BoundDataField Key="TIPO" DataFieldName="TIPO" Hidden="true"></ig:BoundDataField>
                                        <ig:BoundDataField Key="ASIGNAR" DataFieldName="ASIGNAR" Hidden="true"></ig:BoundDataField>
                                        <ig:BoundDataField Key="CAMPO" DataFieldName="CAMPO" Hidden="true"></ig:BoundDataField>
                                        <ig:UnboundField Key="CONTACTOS" Hidden="true"></ig:UnboundField>
                                        <ig:UnboundField Key="SEL" Hidden="false" Width="5%"></ig:UnboundField>
                                    </Columns>
                                </ig:WebHierarchicalDataGrid>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 25px" valign="bottom" align="left">
                                <asp:Label ID="lblComentarios" runat="server" Width="100%" CssClass="Etiqueta"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <textarea onkeypress="return validarLength(this,500)" id="txtComent" onkeydown="textCounter(this,500)"
                                    onkeyup="textCounter(this,500)" style="width: 100%; height: 100px" name="txtComent"
                                    onchange="return validarLength(this,500)" runat="server">
		                        </textarea>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table>
                                    <tr>
                                        <td>
                                            <fsn:FSNButton ID="btnAceptar" runat="server" Alineacion="Right" Text="Confirmar envío"
                                                OnClientClick="return Confirmar();"></fsn:FSNButton>
                                        </td>
                                        <td>
                                            <fsn:FSNButton ID="btnCancelar" runat="server" Alineacion="Left" Text="Cancelar"
                                                OnClientClick="return VolverAlDetalle()"></fsn:FSNButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
<script type="text/javascript">
    //---------------------------------------
    //INICIO Confirmación de la acción
    
    //Abre el buscador de proveedores para elegir el destinatario de la acción
	//Parámetros de entrada:    Rol: Id del rol
	//En el parámetro FilaGrid le pasamos ahora el id del rol para encontrar la fila en la que estamos asignando el proveedor
	function BuscadorProveedores(Rol) {
	    var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>_Common/BuscadorProveedores.aspx?PM=true&Rol=" + Rol + "&Instancia=" + document.getElementById("Instancia").value + "&IDControl=whdgRoles&idFilaGrid=" + Rol + "&ComboContactos=true", "_blank", "width=835,height=635,status=yes,resizable=yes,top=0,left=150,scrollbars=yes");
        
	}

    //Abre el buscador de usuarios para elegir el destinatario de la acción
	//Parámetros de entrada:    Rol: Id del rol
	//En el parámetro FilaGrid le pasamos ahora el id del rol para encontrar la fila en la que estamos asignando el usuario
	function BuscadorUsuarios(Rol) {
    	var newWindow = window.open ("<%=ConfigurationManager.AppSettings("rutaFS")%>_Common/BuscadorUsuarios.aspx?Rol=" + Rol + "&Instancia=" + document.getElementById("Instancia").value + "&idFilaGrid=" + Rol  ,"_blank", "width=750,height=475,status=yes,resizable=no,top=200,left=200");
        
	}

    //Muestra el detalle de la persona
	//Parámetro de entrada: Cod de la persona
	function VerDetallePersona(Per){
	    var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/detallepersona.aspx?CodPersona=" + Per.toString(),"_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");
        
	}
	
	//Muestra el detalle del proveedor
	//Parámetro de entrada: Cod del proveedor
	function VerDetalleProveedor(Prove){
	    var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/detalleproveedor.aspx?codProv=" + Prove.toString(),"_blank", "width=500,height=200,status=yes,resizable=no,top=200,left=250");
        
	}

    function VolverAlDetalle() {
        document.getElementById('<%=divDevolverSiguientesEtapas.ClientID%>').style.display='none';
        document.getElementById('<%=divDetalleFactura.ClientID%>').style.display='inline';
        
        return false;
    }


    function Confirmar() {
        var grid, gridview, rows, row;
        var ejecutar_accion = true;

        grid = $find("<%=whdgRoles.ClientID%>")
        if (grid) {
            gridview = grid.get_gridView();
            rows = gridview.get_rows();
            for (var i = 0; i < rows.get_length(); i++) {
                row = rows.get_row(i);

                asignar = row.get_cell(8).get_value();
                prove = row.get_cell(4).get_value();
                per = row.get_cell(3).get_value();
                como_asignar = row.get_cell(5).get_value();
    
                if ((asignar == 1) && (prove == "") && (per == "") && (como_asignar != 3)){
                    ejecutar_accion = false;
                    break;
                }
            }

            if (ejecutar_accion == false) {
                for (var i = 0; i < rows.get_length(); i++) {
                    row = rows.get_row(i);
            
                    asignar = row.get_cell(8).get_value();
                    prove = row.get_cell(4).get_value();
                    per = row.get_cell(3).get_value();

                    if ((per == "") && (prove == "") && (asignar == 1))
                        row.get_cell(1)._element.className="fntRequired" //nombre
                    else    
                        row.get_cell(1)._element.className=""
                }

                alert(sMensaje);
                return false;
            } else {
                for (var i = 0; i < rows.get_length(); i++) {
                    row = rows.get_row(i);

                    asignar = row.get_cell(8).get_value();
                    como_asignar = row.get_cell(5).get_value();

                    if ((asignar == 1) && (como_asignar != 2)) {
                        if (row.get_cell(10).get_value() == "")
                            contacto = row.get_cell(6).get_value();
                        else
                            contacto = row.get_cell(10).get_value();

                        Fullstep.FSNWeb.Facturas.InsertarRol(row.get_cell(2).get_value(),row.get_cell(3).get_value(),row.get_cell(4).get_value(), contacto, row.get_cell(7).get_value())
                    }
                }
                return true;
            }
        }
     }

     function validarLength(e,l)
	{
		if (e.value.length>l)
			return false
		else
			return true
	}

	function textCounter(e, l) 
	{ 
		if (e.value.length > l) 
			e.value = e.value.substring(0, l); 
	} 
    //FIN Confirmación de la acción
    //---------------------------------------

    function Guardar() {
        return false;
    }

    function Trasladar() {
        return false;
    }
    
    
    function strToNum(strNum) {
        if (strNum == undefined || strNum == null || strNum == '')
            strNum = '0'
        
        while (strNum.indexOf(UsuNumberGroupSeparator) >= 0) {
            strNum = strNum.replace(UsuNumberGroupSeparator, "")
        }
        strNum = strNum.replace(UsuNumberDecimalSeparator, ".")
        return parseFloat(strNum)
    }    
    
    function formatNumber(num) {
        if (num == undefined || num == null || num == '')
            num = 0
        num=parseFloat(num)
        var result = num.toFixed(UsuNumberNumDecimals);
        result = addSeparadorMiles(result.toString())
        var result1 = result.substring(0, result.length - parseInt(UsuNumberNumDecimals) - 1)
        var result2 = result.substring(result.length - parseInt(UsuNumberNumDecimals), result.length)
        result = result1 + UsuNumberDecimalSeparator + result2
        return result
    }

    function addSeparadorMiles(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + UsuNumberGroupSeparator + '$2');
        }
        var retorno = x1 + x2;
        return retorno
    }

    function EjecutarAccion(id) {
        var tbl;
        var den, operacion, valor, importe;
        var ddl;
        var txt;
		var ArrDen=new Array();
        var ArrOperacion=new Array();   
        var ArrValor=new Array();   
        var ArrImporte=new Array();
        var ArrIdDefAtrib=new Array();   
                
        ddlFormaPago = document.getElementById("ddlFormasPago")
        ddlViaPago = document.getElementById("ddlViasPago")
        txt = document.getElementById("txtObservaciones")

        if (txt) {  //si el detalle de la factura es editable recogemos los datos por si ha habido cambios, si no, no habrá habido cambios
            
            importeTotal = strToNum(document.getElementById("lblImporteTotal").innerHTML)  
            totalCostes = strToNum(document.getElementById("lblCostesGenerales").innerHTML) 
            totalDescuentos = strToNum(document.getElementById("lblDescuentosGenerales").innerHTML) 

            //1º comprobamos que no se ha superado la tolerancia definida
            Fullstep.FSNWeb.Facturas.ComprobarTolerancia(ToleranciaImporte, ToleranciaPorcentaje, importeTotal, totalCostes, function(result) {

            if (!result) {
                alert(TextoTolerancia);
                return;
            }

            
            Fullstep.FSNWeb.Facturas.InsertarDatosFactura(<%=Request("ID")%>, importeTotal, ddlFormaPago.value, ddlViaPago.value, txt.value, 1, totalCostes, totalDescuentos,function(){
        
                Fullstep.FSNWeb.Facturas.LimpiarCostesDescuentosGenerales(function() {
                //Costes Generales
                tbl = document.getElementById("tblCostesGenerales");
                if (tbl) {
                    for (var i = 0; i < tbl.tBodies[0].rows.length ; i++) {
                        if(tbl.tBodies[0].rows[i].children.item(0).textContent ==undefined)
                            den = tbl.tBodies[0].rows[i].children.item(0).innerText
                        else
                            den = tbl.tBodies[0].rows[i].children.item(0).textContent
                        ArrDen.push(den)   
                        operacion = tbl.tBodies[0].rows[i].children.item(1).firstChild.value
                        ArrOperacion.push(operacion)
                        valor = strToNum(tbl.tBodies[0].rows[i].children.item(2).firstChild.value)
                        ArrValor.push(valor)
                        if (tbl.tBodies[0].rows[i].children.item(3).firstChild.innerHTML == undefined) 
                            importe = strToNum(tbl.tBodies[0].rows[i].children.item(3).innerHTML)
                        else
                            importe = strToNum(tbl.tBodies[0].rows[i].children.item(3).firstChild.innerHTML)
                        ArrImporte.push(importe)
                        if (tbl.tBodies[0].rows[i].children.item(6) == null)
                            def_atrib_id = tbl.tBodies[0].rows[i].children.item(0).firstChild.value
                        else
                            def_atrib_id = tbl.tBodies[0].rows[i].children.item(6).firstChild.value
                        ArrIdDefAtrib.push(def_atrib_id)
                    }
                }
                Fullstep.FSNWeb.Facturas.InsertarCosteGeneral(ArrIdDefAtrib,ArrDen,ArrOperacion,ArrValor,ArrImporte,function(){
                    ArrDen.splice(0,ArrDen.length)
                    ArrOperacion.splice(0,ArrOperacion.length)
                    ArrValor.splice(0,ArrValor.length)
                    ArrImporte.splice(0,ArrImporte.length)
                    ArrIdDefAtrib.splice(0,ArrIdDefAtrib.length)
                    //Descuentos generales
                    tbl = document.getElementById("tblDescuentosGenerales");
                    if (tbl) {
                        for (var i = 0; i < tbl.tBodies[0].rows.length ; i++) {
                            if(tbl.tBodies[0].rows[i].children.item(0).textContent ==undefined)
                                den = tbl.tBodies[0].rows[i].children.item(0).innerText
                            else
                                den = tbl.tBodies[0].rows[i].children.item(0).textContent
                            ArrDen.push(den)   
                            operacion = tbl.tBodies[0].rows[i].children.item(1).firstChild.value
                            ArrOperacion.push(operacion)
                            valor = strToNum(tbl.tBodies[0].rows[i].children.item(2).firstChild.value)
                            ArrValor.push(valor)
                            if (tbl.tBodies[0].rows[i].children.item(3).firstChild.innerHTML == undefined) 
                                importe = strToNum(tbl.tBodies[0].rows[i].children.item(3).innerHTML)
                            else
                                importe = strToNum(tbl.tBodies[0].rows[i].children.item(3).firstChild.innerHTML)
                            ArrImporte.push(importe)
                            if (tbl.tBodies[0].rows[i].children.item(6) == null)
                                def_atrib_id = tbl.tBodies[0].rows[i].children.item(0).firstChild.value
                            else
                                def_atrib_id = tbl.tBodies[0].rows[i].children.item(6).firstChild.value
                            ArrIdDefAtrib.push(def_atrib_id)
                        }
                    }
                    Fullstep.FSNWeb.Facturas.InsertarDescuentoGeneral(ArrIdDefAtrib,ArrDen,ArrOperacion,ArrValor,ArrImporte,function(){
                        __doPostBack("EjecutarAccion", id);
                    })
                })
            });
			});
            });
        } else {    //No está editable
            hFormaPago = document.getElementById("hFormaPago").value
            hViaPago = document.getElementById("hViaPago").value
            lblObservaciones = document.getElementById("lblObservaciones").innerHTML

            importeTotal = strToNum(document.getElementById("lblImporteTotal").innerHTML) 
            totalCostes = strToNum(document.getElementById("lblCostesGenerales").innerHTML)  
            totalDescuentos = strToNum(document.getElementById("lblDescuentosGenerales").innerHTML)  

            Fullstep.FSNWeb.Facturas.InsertarDatosFactura(<%=Request("ID")%>, importeTotal, hFormaPago , hViaPago, lblObservaciones, 1, totalCostes, totalDescuentos)
			
			__doPostBack('EjecutarAccion', id)
        }        
              
        return false;
    }

    function VerFlujo(instancia,NumFactura) {
        var newWindow = window.open("<%=System.Configuration.ConfigurationManager.AppSettings("rutaPM")%>seguimiento/NWcomentariossolic.aspx?Instancia=" + instancia + "&NumFactura=" + NumFactura,"_blank", "width=1000,height=560,status=yes,resizable=no,top=200,left=200");
        
	    return false;
    }

    function AbrirEFactura(tipo, idEFactura, numFactura) {
        var newWindow = window.open("<%=System.Configuration.ConfigurationManager.AppSettings("rutaPM2008")%>Facturas/EFactura.aspx?Id=" + idEFactura + "&Tipo=" + tipo + "&Num=" + numFactura, "_blank", "width=800,height=700,status=no,resizable=yes,top=200,left=200,menubar=no,scrollbars=yes");
        
        return false;        
    }

    function VerEmailNotificacion(IdMail) {
		var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>_Common/EnvioMails.aspx?IdMail=" + IdMail + "&Volver=DetalleFactura", "_blank", "width=800,height=700,status=no,resizable=no,top=200,left=200,menubar=no");
        
    }
</script>


    <script type="text/javascript">        var ModalProgress = '<%= ModalProgress.ClientID %>';</script>
    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
        <div style="position: relative; top: 30%; text-align: center;">
            <asp:Image ID="ImgProgress" runat="server" SkinID="ImgProgress" />
            <asp:Label ID="LblProcesando" runat="server" Text="Cargando ..."></asp:Label>
        </div>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
</asp:Content>