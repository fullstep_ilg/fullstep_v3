﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.Master" CodeBehind="VisorFacturas.aspx.vb" Inherits="Fullstep.FSNWeb.VisorFacturas" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" language="javascript">
    var factura = 0;
    var gestor = 0;
    var receptor = 0;

    //Recoge el ID del proveedor seleccionado con el autocompletar
    function selected_Proveedor(sender, e) {
        oIdProveedor = document.getElementById('<%= hidProveedor.clientID %>');
        if (oIdProveedor)
            oIdProveedor.value = e._value;
    }

    function prove_seleccionado2(sCIF, sProveCod, sProveDen) {
        document.getElementById("<%=hidProveedor.ClientID%>").value = sProveCod
        document.getElementById("<%=txtProveedor.ClientID%>").value = sProveDen
    }

    /*
    Descripcion:= Funcion que carga los valores del buscador de empresas en la caja de texto
    Parametros:=
    idEmpresa --> Id Empresa
    nombreEmpresa --> Nombre empresa
    Llamada desde:= BuscadorEmpresas.aspx --> aceptar()
    */
    function SeleccionarEmpresa(idEmpresa, nombreEmpresa, nifEmpresa) {
        oIdEmpresa = document.getElementById('<%= hidEmpresa.clientID %>');
        if (oIdEmpresa)
            oIdEmpresa.value = idEmpresa;

        otxtEmpresa = document.getElementById('<%=txtEmpresa.clientID %>');
        if (otxtEmpresa)
            otxtEmpresa.value = nombreEmpresa;
    }

    //Recoge el ID del proveedor seleccionado con el autocompletar
    function selected_Empresa(sender, e) {
        oIdEmpresa = document.getElementById('<%= hidEmpresa.clientID %>');
        if (oIdEmpresa)
            oIdEmpresa.value = e._value;
    }

    /*
    Al pulsar el botón de añadir filtro, te lleva a la página de configuración.
    */
    function AnyadirFiltro() {
    window.location = "ConfigurarFiltrosFacturas.aspx"
    }

    /*
    Se lanza al pulsar una celda de la grid. Dependiendo de la celda pulsada, realizará una acción u otra.
    */
 function onCellSelectionChanged(sender, e) {
     var row = e.getNewSelectedCells().getCell(0).get_row();
     e.set_cancel(true);
     switch (e.getNewSelectedCells().getCell(0).get_column().get_key()) {
            case "SEG": //SEGUIMIENTO DE FACTURAS 
                var nuevo_valor_seg;
                if (e.getNewSelectedCells().getCell(0).get_value() == "0") {
                    nuevo_valor_seg = 1;
                    row.get_element().style.color = "red";
                    e.getNewSelectedCells().getCell(0).set_value(nuevo_valor_seg, '');
                    e.getNewSelectedCells().getCell(0)._element.innerHTML = '<IMG src="<%=System.Configuration.ConfigurationManager.AppSettings("ruta")%>App_themes/<%=Page.Theme%>/images/seg_selec.gif">';
                } else {
                    nuevo_valor_seg = 0;
                    row.get_element().style.color = "black";
                    e.getNewSelectedCells().getCell(0).set_value(nuevo_valor_seg, '');
                    e.getNewSelectedCells().getCell(0)._element.innerHTML = '<IMG src="<%=System.Configuration.ConfigurationManager.AppSettings("ruta")%>App_themes/<%=Page.Theme%>/images/seg_noselec.gif">';
                }
                
                Monitorizar(row.get_cellByColumnKey("INSTANCIA").get_value(), nuevo_valor_seg,row.get_index())
                
                break;
            case "PAGOS":  //DETALLE DE LOS PAGOS
                if (row.get_cellByColumnKey("PAGOS").get_value() != 0) {
                    $get("<%=lblTituloPagos.ClientID %>").innerText = row.get_cellByColumnKey("NUM_FACTURA").get_value()
                    Fullstep.FSNWeb.Consultas.Obtener_DatosDetallepagos(row.get_cellByColumnKey("FACTURA_ID").get_value(), OnGetPagosComplete)
                }
                break;
            case "DEN_ESTADO": //MOTIVO DE ANULACION
                if (row.get_cellByColumnKey("ANULADO").get_value() == "1")
                    FSNMostrarPanel('<%=FSNPanelMotivoAnulacion.AnimationClientID%>', event, '<%=FSNPanelMotivoAnulacion.DynamicPopulateClientID%>', row.get_cellByColumnKey("FACTURA_ID").get_value(), null, null, x - 200, y);
                else {
                    factura = row.get_cellByColumnKey("FACTURA_ID").get_value();
                    Fullstep.FSNWeb.Consultas.ComprobarEnProceso(row.get_cellByColumnKey("INSTANCIA").get_value(), OnGetEnProcesoComplete);
                }
                break;
            case "TIENE_DISCREP":
                if (row.get_cellByColumnKey("TIENE_DISCREP").get_value() == 1) {
                    idFactura = row.get_cellByColumnKey("FACTURA_ID").get_value();
                    numFactura = row.get_cellByColumnKey("NUM_FACTURA").get_value();
                    MostrarFormularioDiscrepanciasFactura(idFactura, numFactura);
                }
                break;
            case "SITUACION_ACTUAL":
            case "APROBAR":
            case "RECHAZAR":
                break;
            default: //COMPROBAMOS SI ESTA EN PROCESO
                factura = row.get_cellByColumnKey("FACTURA_ID").get_value();
                gestor = row.get_cellByColumnKey("GESTOR").get_value();
                receptor = row.get_cellByColumnKey("RECEPTOR").get_value();
                Fullstep.FSNWeb.Consultas.ComprobarEnProceso(row.get_cellByColumnKey("INSTANCIA").get_value(), OnGetEnProcesoComplete);
                break;
        }
    }

    /* 
    Una vez obtenido el detalle de los pagos de la factura, muestra el modalpopup.
    Llamada desde: onCellSelectionChanged
    */
    function OnGetPagosComplete(result) {
        $get('divPagosContent').innerHTML = result;
        popUpShowed = $find('<%=mpePagos.ClientID%>');
        popUpShowed.show();
        popUpShowed._backgroundElement.style.zIndex += 10;
        popUpShowed._foregroundElement.style.zIndex += 10;
    }

    /* 
    Una vez comprobado si está o no en proceso te redirige a la página de detalle o te muestra el aviso de que está en proceso.
    Llamada desde: onCellSelectionChanged
    */
    function OnGetEnProcesoComplete(result) {
        if (result == 0)
            window.location = "detalleFactura.aspx?ID=" + factura + "&gestor=" + gestor + "&receptor=" + receptor
        else 
            FSNMostrarPanel('<%=FSNPanelEnProceso.AnimationClientID%>', event, '<%=FSNPanelEnProceso.DynamicPopulateClientID%>', 7, null, null, x, y);
    }

    /*
    Obtiene las posiciones del cursor, necesario para el FireFox
    */
    var x = 0
    var y = 0  
    function Posicion(event) {
        x = event.clientX;
        y = event.clientY;

    }


    /*
    ''' <summary>
    ''' Exporta a Excel
    ''' </summary>
    ''' <remarks>Llamada desde: onclick del div q contiene al link excel y a la img excel; Tiempo máximo:0,5</remarks>
    */
    function ExportarExcel() {
        __doPostBack('ExportarExcel', '')
    }

    /*
    ''' <summary>
    ''' Exporta a Pdf
    ''' </summary>
    ''' <remarks>Llamada desde: onclick del div q contiene al link pdf y a la img pdf; Tiempo máximo:0,5</remarks>
    */
    function ExportarPDF() {
        __doPostBack('ExportarPDF', '')
    }

    //////////////////////////////
    //Funciones Paginación
    //////////////////////////////
    function IndexChanged() {
        var dropdownlist = $('[id$=PagerPageList]')[0];
        var grid = $find("<%= whdgFacturas.ClientID %>");
        var parentGrid = grid.get_gridView();
        var newValue = dropdownlist.selectedIndex;
        parentGrid.get_behaviors().get_paging().set_pageIndex(newValue);
    }
    function FirstPage() {
        var grid = $find("<%= whdgFacturas.ClientID %>");
        var parentGrid = grid.get_gridView();
        parentGrid.get_behaviors().get_paging().set_pageIndex(0);
    }
    function PrevPage() {
        var grid = $find("<%= whdgFacturas.ClientID %>");
        var parentGrid = grid.get_gridView();
        var dropdownlist = $('[id$=PagerPageList]')[0];
        if (parentGrid.get_behaviors().get_paging().get_pageIndex() > 0) {
            parentGrid.get_behaviors().get_paging().set_pageIndex(parentGrid.get_behaviors().get_paging().get_pageIndex() - 1);
        }
    }
    function NextPage() {
        var grid = $find("<%= whdgFacturas.ClientID %>");
        var parentGrid = grid.get_gridView();
        var dropdownlist = $('[id$=PagerPageList]')[0];
        if (parentGrid.get_behaviors().get_paging().get_pageIndex() < parentGrid.get_behaviors().get_paging().get_pageCount() - 1) {
            parentGrid.get_behaviors().get_paging().set_pageIndex(parentGrid.get_behaviors().get_paging().get_pageIndex() + 1);
        }
    }
    function LastPage() {
        var grid = $find("<%= whdgFacturas.ClientID %>");
        var parentGrid = grid.get_gridView();
        parentGrid.get_behaviors().get_paging().set_pageIndex(parentGrid.get_behaviors().get_paging().get_pageCount() - 1);
    }
    function AdministrarPaginador() {
        var grid = $find("<%= whdgFacturas.ClientID %>");
        var parentGrid = grid.get_gridView();
        $('[id$=lblCount]').text(parentGrid.get_behaviors().get_paging().get_pageCount());
        if (parentGrid.get_behaviors().get_paging().get_pageIndex() == 0) {
            $('[id$=ImgBtnFirst]').attr('disabled', 'disabled');
            $('[id$=ImgBtnFirst]').attr('src', rutaTheme + '/images/primero_desactivado.gif');
            $('[id$=ImgBtnPrev]').attr('disabled', 'disabled');
            $('[id$=ImgBtnPrev]').attr('src', rutaTheme + '/images/anterior_desactivado.gif');
        } else {
            $('[id$=ImgBtnFirst]').removeAttr('disabled');
            $('[id$=ImgBtnFirst]').attr('src', rutaTheme + '/images/primero.gif');
            $('[id$=ImgBtnPrev]').removeAttr('disabled');
            $('[id$=ImgBtnPrev]').attr('src', rutaTheme + '/images/anterior.gif');
        }
        if (parentGrid.get_behaviors().get_paging().get_pageIndex() == parentGrid.get_behaviors().get_paging().get_pageCount() - 1) {
            $('[id$=ImgBtnNext]').attr('disabled', 'disabled');
            $('[id$=ImgBtnNext]').attr('src', rutaTheme + '/images/siguiente_desactivado.gif');
            $('[id$=ImgBtnLast]').attr('disabled', 'disabled');
            $('[id$=ImgBtnLast]').attr('src', rutaTheme + '/images/ultimo_desactivado.gif');
        } else {
            $('[id$=ImgBtnNext]').removeAttr('disabled');
            $('[id$=ImgBtnNext]').attr('src', rutaTheme + '/images/siguiente.gif');
            $('[id$=ImgBtnLast]').removeAttr('disabled');
            $('[id$=ImgBtnLast]').attr('src', rutaTheme + '/images/ultimo.gif');
        }
    }
    function whdgFacturas_PageIndexChanged(a, b, c, d) {
        var grid = $find("<%= whdgFacturas.ClientID %>");
        var parentGrid = grid.get_gridView();
        var dropdownlist = $('[id$=PagerPageList]')[0];

        dropdownlist.options[parentGrid.get_behaviors().get_paging().get_pageIndex()].selected = true;
        AdministrarPaginador();
    }

    //funcion que salta cuando se cambia el centro de coste, al meter un codigo validara si es valido o no
    //oTxtCentro: caja de texto del centro de coste
    function CentroCosteChange(oTxtCentro) {

        var sCentroCoste = oTxtCentro.value

        $.ajax({
            type: "POST",
            url: rutaFS + '_Common/App_Services/Consultas.asmx/Buscar_CentroCoste',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ CentroCoste: sCentroCoste }),
            dataType: "json",
            async: false,
            success: function (json_data) {
                var ohidCentroCoste = $("[id$=hid_CentroCostes]")[0]
                if (json_data.d.ExisteCentroCoste) {
                    ohidCentroCoste.value = json_data.d.CodCentroCoste
                    oTxtCentro.value = json_data.d.DenCentroCoste
                    oTxtCentro.style.backgroundColor = "green"
                } else {
                    oTxtCentro.style.backgroundColor = "red"
                    oTxtCentro.value = ''
                    ohidCentroCoste.value = ''
                }
            }
        })
    }

    //funcion que monitoriza una factura
    function Monitorizar(instancia, seg, fila) {

        $.ajax({
            type: "POST",
            url: rutaFS + 'PM/Facturas/VisorFacturas.aspx/Monitorizar',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ lInstancia: instancia, iSeg: seg, Fila:fila }),
            dataType: "json",
            async: false
        })
    }
</script>
<style type="text/css">
    .seguimiento
    {
        color: Red;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
    <ig:WebExcelExporter ID="whdgExcelExporter" runat="server"></ig:WebExcelExporter>
	<ig:WebDocumentExporter ID="whdgPDFExporter" runat="server" ExportMode="Download"></ig:WebDocumentExporter>
    <table width="100%" cellpadding="0" border="0">
        <tr>
            <td align="left">
                <fsn:FSNPageHeader ID="FSNPageHeader" runat="server" UrlImagenCabecera="factura.jpg">
                </fsn:FSNPageHeader>
            </td>
        </tr>
    </table>
     <table width="100%" cellpadding="0" border="0">
        <tr>
            <td id="CeldaBusquedaGeneral" runat="server" width="60%">
                <asp:UpdatePanel ID="upBusquedaGeneral" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                <table cellpadding="2" cellspacing="0" border="0">
                    <tr><td><asp:Label ID="lblProveedor" runat="server" CssClass="Etiqueta"></asp:Label></td>
                        <td><table style="background-color: White; width: 150px; border: solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
                             <tr><td><asp:TextBox ID="txtProveedor" runat="server" Width="190px" BorderWidth="0px" BackColor="White" Height="16px"></asp:TextBox>
                                     <ajx:AutoCompleteExtender ID="txtProveedor_AutoCompleteExtender" runat="server" CompletionSetCount="10"
                                          DelimiterCharacters="" Enabled="True" MinimumPrefixLength="1" ServiceMethod="GetProveedores"
                                          ServicePath="~/App_Pages/_Common/App_Services/AutoComplete.asmx" TargetControlID="txtProveedor" EnableCaching="False" OnClientItemSelected="selected_Proveedor">
                                     </ajx:AutoCompleteExtender>
                                  </td>
                                  <td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px">
                                    <asp:ImageButton ID="imgProveedorLupa" runat="server" SkinID="BuscadorLupa" />
                                  </td>
                             </tr>
                             <asp:HiddenField ID="hidProveedor" runat="server" />
                             </table>
                        </td>
                        <td><asp:Label ID="lblEmpresa" runat="server" CssClass="Etiqueta"></asp:Label></td>
                        <td>
                        <table style="background-color:White;width:150px;border:solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtEmpresa" runat="server" Width="190px" BorderWidth="0px"></asp:TextBox>
                                    <ajx:AutoCompleteExtender ID="txtEmpresa_AutoCompleteExtender" 
                                     runat="server" CompletionSetCount="10" DelimiterCharacters="" Enabled="True"
                                     MinimumPrefixLength="1" ServiceMethod="GetEmpresas" ServicePath="~/App_Pages/_Common/App_Services/AutoComplete.asmx"
                                     TargetControlID="txtEmpresa" EnableCaching="False" OnClientItemSelected="selected_Empresa"  ></ajx:AutoCompleteExtender >
                                </td>
                                <td style="border-left:solid 1px #BBBBBB;padding-left:2px;padding-right:0px">
                                    <asp:ImageButton id="imgEmpresaLupa" runat="server" SkinID="BuscadorLupa" /> 
                                </td>
                            </tr>                        
                            <asp:HiddenField ID="hidEmpresa" runat="server" />
                        </table>
                        </td>
                        <td><asp:Label ID="lblNumeroFactura" runat="server" CssClass="Etiqueta"></asp:Label></td>
                        <td><asp:TextBox ID="txtNumeroFactura" runat="server" Width="140px" BackColor="White"></asp:TextBox></td>
                        <td><fsn:FSNButton ID="btnBuscar" runat="server" Alineacion="Left"></fsn:FSNButton></td>
                    </tr>
                </table>
                </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td align="center">
                <!-- Columna en la que está la alerta de facturas pendientes -->
                <asp:UpdatePanel ID="upAlerta" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                         <asp:PlaceHolder ID="phAlerta" runat="server" Visible="false">
                            <div>
                                <table cellpadding="2" cellspacing="0" border="0">
                                    <tr>
                                        <td rowspan="3">
                                            <asp:Image ID="imgAlerta" runat="server" Height="35" Width="35" SkinID="Alerta" />
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblFacturasPendientes" runat="server" CssClass="Etiqueta"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </asp:PlaceHolder>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlCabeceraParametros" runat="server" Width="99%">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding-top: 5px;
            padding-bottom: 5px">
            <tr>
                <td valign="middle" align="left" style="padding-top: 5px">
                    <asp:Panel runat="server" ID="pnlBusquedaAvanzada" Style="cursor: pointer" Font-Bold="True"
                        Height="20px" Width="100%" CssClass="PanelCabecera">
                        <table border="0">
                            <tr>
                                <td>
                                    <asp:Image ID="imgExpandir" runat="server" SkinID="Contraer" />
                                </td>
                                <td style="vertical-align: top">
                                    <asp:Label ID="lblBusquedaAvanzada" runat="server" Font-Bold="True" ForeColor="White"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlParametros" runat="server" CssClass="Rectangulo" Width="100%" style="display:none;">
        <asp:UpdatePanel ID="upBusquedaAvanzada" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <fspm:BusquedaAvanzadaFAC ID="Busqueda" runat="server"></fspm:BusquedaAvanzadaFAC>
                <table width="80%" border="0" cellpadding="4" cellspacing="0">
                    <tr>
                        <td width="60px">
                            <fsn:FSNButton ID="btnBuscarBusquedaAvanzada" runat="server" Alineacion="left"></fsn:FSNButton>
                        </td>
                        <td width="60px">
                            <fsn:FSNButton ID="btnLimpiarBusquedaAvanzada" runat="server" Alineacion="Right"></fsn:FSNButton>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajx:CollapsiblePanelExtender runat="server" CollapseControlID="pnlBusquedaAvanzada"
        ExpandControlID="pnlBusquedaAvanzada" SuppressPostBack="true" TargetControlID="pnlParametros"
        ImageControlID="imgExpandir" SkinID="FondoRojo" Collapsed="true">
    </ajx:CollapsiblePanelExtender>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
    <ContentTemplate>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td valign="bottom">
                                    <asp:DataList ID="dlFiltrosConfigurados" Width="220px" runat="server" DataSourceID="odsFiltrosConfigurados"
                                        RepeatDirection="Horizontal" CellPadding="0" RepeatLayout="Table" RepeatColumns="5">
                                        <ItemTemplate>
                                            <td>
                                                <fsn:FSNTab ID="fsnTabFiltro" Alineacion="Left" runat="server" Visible="true" SkinID="PestanyasFiltros"
                                                    Text='<%# IIf(Eval("ID")=0,Textos(22),Mid(Eval("NOM_FILTRO"),1,20)) & IIf(Len(Eval("NOM_FILTRO"))>20,"...","") %>'
                                                    ToolTip='<%# IIf(Eval("ID")=0,"",Eval("NOM_FILTRO")) %>' OnClick="fsnTabFiltro_Click"
                                                    CommandArgument='<%# Eval("ID") & "#" & Eval("IDFILTRO") %>'
                                                    BorderStyle="None" BorderColor="#AAAAAA" BorderWidth="1px"></fsn:FSNTab>
                                            </td>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </td>
                                <td width="10px">
                                    &nbsp;
                                </td>
                                <td>
                                    <fsn:FSNButton ID="btnAnyadirFiltros" runat="server" OnClientClick="AnyadirFiltro(); return false;"></fsn:FSNButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
    <asp:ObjectDataSource ID="odsFiltrosConfigurados" runat="server" SelectMethod="LoadConfigurados"
        TypeName="Fullstep.FSNServer.Filtros">
        <SelectParameters>
            <asp:Parameter Name="sUsuario" Type="String" />
            <asp:Parameter Name="sTipo" Type="Int16" DefaultValue="7" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <div onmouseover="Posicion(event)">
    			
	
	
    <ig:WebHierarchicalDataGrid ID="whdgFacturas" runat="server" AutoGenerateColumns="false" ShowHeader="true" EnableAjax="true" Width="100%" EnableDataViewState="false" EnableAjaxViewState="false">
    <Columns>
        <ig:TemplateDataField Key="APROBAR" Width="80px">
                   <HeaderTemplate>
                   <div style="text-align: center"><table><tr><td><fsn:FSNButton ID="btnAprobar" runat="server" OnPreRender="btnAprobar_PreRender" OnClick="btnAprobar_Click"></fsn:FSNButton></td></tr></table></div>
                   </HeaderTemplate>
                   <ItemTemplate>
                   <div style="text-align: center"><asp:CheckBox runat="server" id="chk" /></div>
                   </ItemTemplate>
        </ig:TemplateDataField>
        <ig:TemplateDataField Key="RECHAZAR" Width="80px">
                   <HeaderTemplate>
                   <div style="text-align: center"><table><tr><td><fsn:FSNButton ID="btnRechazar" runat="server" OnPreRender="btnRechazar_PreRender" OnClick="btnRechazar_Click"></fsn:FSNButton></td></tr></table></div>
                   </HeaderTemplate>
                   <ItemTemplate>
                   <div style="text-align: center"><asp:CheckBox runat="server" id="chk" /></div>
                   </ItemTemplate>
        </ig:TemplateDataField>
    </Columns>
    <Behaviors>
    <ig:Selection CellClickAction="Cell" CellSelectType="Single" SelectionClientEvents-CellSelectionChanging="onCellSelectionChanged"></ig:Selection>
                <ig:Filtering Alignment="Top" Enabled="true" Visibility="Visible" EnableInheritance="true"></ig:Filtering>                
                <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
                <ig:ColumnMoving Enabled="true"></ig:ColumnMoving>
                <ig:Sorting Enabled="true" SortingMode="Multi"></ig:Sorting> 
                <ig:Paging Enabled="true" PagerAppearance="Top">
                    <PagingClientEvents PageIndexChanged="whdgFacturas_PageIndexChanged" />
							<PagerTemplate>
								<div class="CabeceraBotones" style="clear:both; float:left; width:100%;"> 
									<div style="float:left; padding:3px 0px 2px 0px; margin-left:10px;">
										<div style="clear: both; float: left; margin-right: 5px;">
                                            <asp:Image ID="ImgBtnFirst" runat="server"  style="margin-right:2px;" />
                                            <asp:Image ID="ImgBtnPrev" runat="server" />
                                        </div>
                                        <div style="float: left; margin-right:5px;">
                                            <asp:Label ID="lblPage" runat="server" Text="dPage" Style="margin-right: 3px;"></asp:Label>
                                            <asp:DropDownList ID="PagerPageList" runat="server" Style="margin-right: 3px;" onchange="return IndexChanged()" />
                                            <asp:Label ID="lblOF" runat="server" Text="dOf" Style="margin-right: 3px;"></asp:Label>
                                            <asp:Label ID="lblCount" runat="server" />
                                        </div>
                                        <div style="float: left;">
                                            <asp:Image ID="ImgBtnNext" runat="server"  style="margin-right:2px;" />
                                            <asp:Image ID="ImgBtnLast" runat="server"  />
                                        </div>
									</div>
									<div style="float:right; margin:5px 5px 0px 0px; display: inline">
                                        <div id="divFechaDesde" runat="server" style="display:inline; float:left; height:100%">
											<asp:Label ID="lblFechaDesde" style="position: relative; top: -20%; text-align:center;" runat="server"></asp:Label>
											<asp:Image ID="imgCalendarioFecDesde" runat="server" />
											<asp:Label ID="lblExceptoFechaDesde" style="position: relative; top: -20%; text-align:center;" runat="server"></asp:Label>
										</div>
                                        <div style="float:left;"><fsn:FSNButton ID="btnConfigurar" runat="server" AgregarOnClick="false" ></fsn:FSNButton></div>
                                            <div onclick="ExportarExcel()" class="botonDerechaCabecera" style="float:left;">            
                                                <asp:Image runat="server" ID="imgExcel" CssClass="imagenCabecera" />
                                                <asp:Label runat="server" ID="lblExcel" CssClass="fntLogin2"></asp:Label>           
                                            </div>
			                                <div onclick="ExportarPDF()" class="botonDerechaCabecera" style="float:left; margin-left:15px;">            
				                                 <asp:Image runat="server" ID="imgPDF" CssClass="imagenCabecera" />
				                                 <asp:Label runat="server" ID="lblPDF" CssClass="fntLogin2"></asp:Label>           
			                                </div>
    									</div>
	    							</div>	
					
							</PagerTemplate>
						</ig:Paging>						
    </Behaviors>
    </ig:WebHierarchicalDataGrid>
    <p><br /></p>
    </div>
    </ContentTemplate>
     <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
		<asp:AsyncPostBackTrigger ControlID="btnBuscarBusquedaAvanzada" EventName="Click" />
	</Triggers>
    </asp:UpdatePanel>

    <script type="text/javascript" language="javascript">
        var ModalProgress = '<%= ModalProgress.ClientID %>';
    </script>


    <ajx:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress" Style="display: none">
        <div style="position: relative; top: 30%; text-align: center;">
            <asp:Image ID="ImgProgress" runat="server" SkinID="ImgProgress" />
            <asp:Label ID="LblProcesando" runat="server" ForeColor="Black" Text="Cargando ..."></asp:Label>
        </div>
    </asp:Panel>
    
    <asp:Button ID="btnPagos" runat="server" Style="display: none" />
    <ajx:ModalPopupExtender ID="mpePagos" runat="server" Enabled="True" PopupControlID="panPagos" TargetControlID="btnPagos" CancelControlID="imgCerrarPagos" />
    <asp:Panel ID="panPagos" runat="server" Style="display: none" Width="600px" CssClass="modalPopup">
        <div id="Cabecera"><table width="100%">
        <tr><td align="left"><asp:Image ID="imgPagos" runat="server" /></td><td><asp:Label ID="lblLitTituloPagos" runat="server" CssClass="RotuloGrande"></asp:Label>&nbsp;<asp:Label ID="lblTituloPagos" runat="server" CssClass="RotuloGrande"></asp:Label></td>
            <td align="right"><asp:ImageButton id="imgCerrarPagos" runat="server" /></td></tr></table>        
        </div>
        <p><br /></p>
        <div id="divPagosContent"></div>
        <p><br /></p>
    </asp:Panel>
     
     <fsn:FSNPanelInfo ID="FSNPanelMotivoAnulacion" runat="server" ServiceMethod="Obtener_MotivoAnulacion" TipoDetalle="5" Height="120px" Width="370px"></fsn:FSNPanelInfo>
     <fsn:FSNPanelInfo ID="FSNPanelEnProceso" runat="server" ServiceMethod="Obtener_TextoEnProceso" TipoDetalle="4" Height="120px" Width="370px"></fsn:FSNPanelInfo>
     

</asp:Content>