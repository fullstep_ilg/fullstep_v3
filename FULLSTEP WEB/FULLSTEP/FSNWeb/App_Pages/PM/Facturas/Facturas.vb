﻿
Module modFacturas

    Private ReadOnly Property FSWSServer() As FSNServer.Root
        Get
            Return CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        End Get
    End Property

    ''' <summary>
    ''' Devuelve las distintas partidas de la instalación
    ''' </summary>
    ''' <remarks></remarks>
    Public Function DevolverEstados(ByVal Idioma As String) As DataSet
        Dim cEstadosFactura As FSNServer.EstadosFactura
        cEstadosFactura = FSWSServer.Get_Object(GetType(FSNServer.EstadosFactura))
        cEstadosFactura.LoadData(Idioma)
        DevolverEstados = cEstadosFactura.Data
    End Function

    ''' <summary>
    ''' Devuelve las distintas partidas de la instalación
    ''' </summary>
    ''' <remarks></remarks>
    Public Function DevolverPartidas(ByVal Idioma As String) As DataTable
        Dim cPres5 As FSNServer.PRES5
        cPres5 = FSWSServer.Get_Object(GetType(FSNServer.PRES5))
        Return cPres5.LoadPartidasInstalacion(Idioma)
    End Function

End Module

