﻿var scriptsLoaded = false;
var idFacturaPopUp;
function MostrarFormularioDiscrepanciasFactura(idFactura, numFactura) {
    idFacturaPopUp = idFactura;
    if (scriptsLoaded) {
        LoadDiscrepancias(idFactura);
        $('#DiscrepancialblTituloPopUpNuevoMensaje').text(TextosDiscrepancias[19] + ' ' + numFactura);
    } else {        
        LoadScripts();        
        $.get(rutaFS + 'CN/html/_mensajes.tmpl.htm', function (templates) {
            $('body').append(templates);
            $('body').prepend('<div id="popupPantallaDiscrep" class="popupCN" style="display:none; position:absolute; z-index:1001; width:95%; max-height:80%; text-align:left; padding:5px; overflow:auto;"></div>');
            var cabecera = '<div id="btnCerrarPopUpDiscrepancias" class="CerrarPopUp" style="float:right; width:30px; height:30px;"></div>'
            cabecera = cabecera + '<div class="FondoHeader" style="float:left; margin-bottom:5px;"><img src="' + ruta + 'images/Discrepancia.png" style="float:left; margin-left:10px; max-height:25px;" />';
            cabecera = cabecera + '<span id="DiscrepancialblTituloPopUpNuevoMensaje" class="LabelTituloCabeceraCustomControl" style="margin-left:10px; line-height:25px;"></span></div>';
            cabecera = cabecera + '<div id="divDetalleDiscrepancias" style="clear:both; float:left; width:100%; overflow:auto;"></div>';
            $('#popupPantallaDiscrep').prepend(cabecera);
            $('#DiscrepancialblTituloPopUpNuevoMensaje').text(TextosDiscrepancias[19] + ' ' + numFactura);
            $.get(rutaFS + 'CN/html/_detalle_discrp_linea.tmpl.htm', function (templates) {
                $('body').append(templates);
                LoadDiscrepancias(idFactura);
            });
        });
    }
}
function LoadScripts() {
    $('#btnCerrarPopUpDiscrepancias').live('click', function () {
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'PM/App_Services/Facturas.asmx/Comprobar_DiscrepanciasCerradas',
            data: JSON.stringify({ IdFactura: idFacturaPopUp, Linea: 0 }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true
        })).done(function (msg) {
            if (parseInt(msg.d) == 0) {
                $('#imgDiscrepancias_' + idFacturaPopUp).hide();
                $('#imgDiscrepanciasCerradas_' + idFacturaPopUp).show();
            } else {
                $('#imgDiscrepancias_' + idFacturaPopUp).show();
                $('#imgDiscrepanciasCerradas_' + idFacturaPopUp).hide();
            };
            $('#popupFondo').hide();
            $('#popupPantallaDiscrep').hide();
        });
    });
    $('[id^=btnCerrarDiscrepancia_]').live('click', function () {
        var IdDiscrepancia = $(this).attr('id').split('_')[1];
        $('[id^=lblDiscrepanciaAbierta_' + IdDiscrepancia + ']').hide();
        $('[id^=lblDiscrepanciaCerrada_' + IdDiscrepancia + ']').show();
        $('[id^=btnCerrarDiscrepancia_' + IdDiscrepancia + ']').hide();
        $.ajax({
            type: "POST",
            url: rutaFS + 'PM/App_Services/Facturas.asmx/Cerrar_Discrepancia',
            data: JSON.stringify({ IdDiscrepancia: IdDiscrepancia }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true
        });
    });
    $('[id^=divMostrarIncidenciasCerradas_]').live('click', function () {
        var IdLinea = $(this).attr('id').split('_')[1];
        $('#divMuro_' + IdLinea + ' li').show();
        $('#divMostrarIncidenciasCerradas_' + IdLinea).hide();
        CentrarPopUp($('#popupPantallaDiscrep'));
    });    
    scriptsLoaded = true;
}
function LoadDiscrepancias(idFactura) {
    $('#divDetalleDiscrepancias').empty();
    var IdLinea;
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/App_Services/Facturas.asmx/Obtener_Lineas_Factura',
        data: JSON.stringify({ Factura: idFactura, Linea:0 }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true
    })).done(function (msg) {
        lineasFactura = msg.d;
        var totLineas = lineasFactura.length;
        $.each(lineasFactura, function (index) {
            IdLinea = this.Id;
            $('#detalleDiscrpLinea').tmpl(this).appendTo($('#divDetalleDiscrepancias'));
            $('#divMuro_' + IdLinea).cn_ObtenerMensajesUsuario({
                WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/ObtenerMensajesUsuario',
                tipo: 0,
                grupo: 0,
                categoria: 0,
                megusta: false,
                pagina: 1,
                historico: false,
                discrepancias: true,
                factura: idFactura,
                linea: IdLinea,
                onDone: function () { ShowPopUpDiscrepancias(totLineas == index + 1,IdLinea) }
            });
        });

    });
}
function ShowPopUpDiscrepancias(LoadComplete,idLinea) {
    if (LoadComplete) {        
        $('[id^=lblDetalleLinea_]').text(TextosDiscrepancias[5]);
        $('[id^=lblHeaderCodigo_]').text(TextosDiscrepancias[6]);
        $('[id^=lblHeaderDenominacion_]').text(TextosDiscrepancias[7]);
        $('[id^=lblHeaderCantidad_]').text(TextosDiscrepancias[8]);
        $('[id^=lblHeaderPrecio_]').text(TextosDiscrepancias[9]);
        $('[id^=lblHeaderTotalCostes_]').text(TextosDiscrepancias[10]);
        $('[id^=lblHeaderTotalDescuentos_]').text(TextosDiscrepancias[11]);
        $('[id^=lblHeaderImporteNeto_]').text(TextosDiscrepancias[4]);
        $('[id^=lblHeaderObservaciones_]').text(TextosDiscrepancias[12]);
        $('[id^=lblHeaderPedidoFullstep_]').text(TextosDiscrepancias[13]);
        $('[id^=lblHeaderRefFactura_]').text(TextosDiscrepancias[2]);
        $('[id^=lblHeaderAlbaran_]').text(TextosDiscrepancias[1]);
        $('[id^=lblHeaderNumDocSAP_]').text(TextosDiscrepancias[3]);

        $('[id^=lblInfoLinea_]').text(TextosDiscrepancias[14]);
        $('[id^=lblDiscrepanciaAbierta_]').text('(' + TextosDiscrepancias[15] + ')');
        $('[id^=lblDiscrepanciaCerrada_]').text('(' + TextosDiscrepancias[16] + ')');
        $('[id^=lblCerrarDiscrepancia_]').text(TextosDiscrepancias[17]);

        $('[id^=lblMostrarIncidenciasCerradas_]').text(TextosDiscrepancias[18]);

        $('#popupPantallaDiscrep').show();
        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();        
        if ($('#divMuro_' + idLinea + ' li:visible').length == 0) $('#divMuro_' + idLinea + ' li').show();
        if ($('#divMuro_' + idLinea + ' li:hidden').length > 0) {
            $('#divMostrarIncidenciasCerradas_' + idLinea).show();
        } else {
            $('#divMostrarIncidenciasCerradas_' + idLinea).hide();
        }
        CentrarPopUp($('#popupPantallaDiscrep'));
    }
};
var edicionDiscrepancia = false;
$('#popupPantallaDiscrep [id^=msgEditar]').live('click', function () {
    $('#popupPantallaDiscrep').hide();
    edicionDiscrepancia = true;
});
$('#popupNuevoMensaje [id^=btnCancelarNuevoMensajeMaster]').live('click', function () {
    if (typeof (edicionDiscrepancia) !== 'undefined' && edicionDiscrepancia) {
        edicionDiscrepancia = false;
        $('#popupFondo').show();
        $('#popupPantallaDiscrep').show();
    }
});
$('#popupNuevoMensaje [id^=btnAceptarNuevoMensajeMaster]').live('click', function () {
    if (typeof (edicionDiscrepancia) !== 'undefined' && edicionDiscrepancia) {
        edicionDiscrepancia = false;
        $('#popupFondo').show();
        $('#popupPantallaDiscrep').show();
        LoadDiscrepancias(idFacturaPopUp);
    }
});
