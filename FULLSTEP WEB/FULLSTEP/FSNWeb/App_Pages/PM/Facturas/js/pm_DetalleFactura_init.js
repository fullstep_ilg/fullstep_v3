﻿var scriptsLoaded = false;
var idFacturaPopUp, lineaFacturaPopUp;
$(document).ready(function () {
    if (usuario != null && usuario.AccesoCN && usuario.CNPermisoCrearMensajes) $('#lnkNuevaDiscrepanciaMaster').show();
    $.get(rutaFS + 'PM/Facturas/html/_menuDetalleDiscrepancias.htm', function (menuDetalleDiscrepancias) {
        $('body').prepend(menuDetalleDiscrepancias);
    });
    $('#lnkNuevaDiscrepanciaMaster').live('click', function () {
        var gridId = $('[id$=whdgLineas]').attr('id');
        if ($find(gridId).get_gridView().get_behaviors().get_selection().get_selectedRows().get_length() == 0) {
            alert(TextosDiscrepancia[1]);
            return false;
        }
        var row = $find(gridId).get_gridView().get_behaviors().get_selection().get_selectedRows().getItem(0);

        idFactura = row.get_cellByColumnKey("FACTURA").get_value();
        linea = row.get_cellByColumnKey("LINEA").get_value();
        var pedido = row.get_cellByColumnKey("NUM_PEDIDO").get_value();
        var albaran = row.get_cellByColumnKey("ALBARAN").get_value();
        var heightContenido = $('#Contenido').height();
        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();
        $('#Contenido').css('height', heightContenido);
        if ($('#divNuevosMensajesMaster').length == 0) {
            $.get(rutaFS + 'CN/html/_nuevo_mensaje.htm', function (menuNuevoMensaje) {
                $('body').prepend('<div id="popupNuevaDiscrepancia" class="popupCN" style="display:none; position:absolute; z-index:1001; width:75%; max-height:80%; text-align:left; padding:5px; overflow:auto;"></div>');
                menuNuevoMensaje = menuNuevoMensaje.replace(/src="/gi, 'src="' + ruta);
                $('#popupNuevaDiscrepancia').prepend(menuNuevoMensaje);
                $.each($('#popupNuevaDiscrepancia [id]'), function () {
                    $(this).attr('id', 'Discrepancia' + $(this).attr('id'));
                });
                $('#DiscrepanciadivMenuNuevoMensaje').hide();
                var cabecera = '<div class="FondoHeader" style="float:left; margin-bottom:5px;"><img src="' + ruta + 'images/Discrepancia.png" style="float:left; margin-left:10px; max-height:25px;" />';
                cabecera = cabecera + '<span id="DiscrepancialblTituloPopUpNuevoMensaje" class="LabelTituloCabeceraCustomControl" style="margin-left:10px; line-height:25px;"></span></div>';
                $('#popupNuevaDiscrepancia').prepend(cabecera);
                EstablecerTextosMenuDiscrepanciaNueva();
                $('#txtNuevoMensajeTituloDiscrepancia').autoGrow();

                MostrarFormularioNuevaDiscrepancia(true, idFactura, numFactura, linea, pedido, albaran);
                CentrarPopUp($('#popupNuevaDiscrepancia'));
            });
        } else {
            MostrarFormularioNuevaDiscrepancia(true, idFactura, numFactura, linea, pedido, albaran);
            CentrarPopUp($('#popupNuevaDiscrepancia'));
        }
    });
    $('#btnCerrarMenuDetalleDiscrepancia').live('click', function () {
        $('#divMenuDetalleDiscrepancia').hide();
    });
    $('#btnVerDetalleDiscrepanciasLinea').live('click', function () {
        var idFactura = $('#divMenuDetalleDiscrepancia').attr('IdFactura');
        var numFactura = $('#divMenuDetalleDiscrepancia').attr('NumFactura');
        var idLinea = $('#divMenuDetalleDiscrepancia').attr('IdLinea');
        MostrarFormularioDiscrepanciasFactura(idFactura, idLinea, numFactura)
        $('#divMenuDetalleDiscrepancia').hide();
    });
    $('#btnCerrarDiscrepanciasLinea').live('click', function () {
        if (confirm(TextosDiscrepancias[20])) {
            var idFactura = $('#divMenuDetalleDiscrepancia').attr('IdFactura');
            var idLinea = $('#divMenuDetalleDiscrepancia').attr('IdLinea');
            $.ajax({
                type: "POST",
                url: rutaFS + 'PM/App_Services/Facturas.asmx/Cerrar_Discrepancias_Linea',
                data: JSON.stringify({ Factura: idFactura, Linea: idLinea }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true
            });
            $('#imgDiscrepancias_' + idLinea).hide();
            $('#imgDiscrepanciasCerradas_' + idLinea).show();
        }
        $('#divMenuDetalleDiscrepancia').hide();
    });
});
function MostrarMenuDiscrepanciasLinea(idFactura, numFactura, idLineaFactura, posX, posY) {    
    $('#divMenuDetalleDiscrepancia').attr('IdFactura', idFactura);
    $('#divMenuDetalleDiscrepancia').attr('NumFactura', numFactura);
    $('#divMenuDetalleDiscrepancia').attr('IdLinea', idLineaFactura);
    $('#divMenuDetalleDiscrepancia').css('left', posX);
    $('#divMenuDetalleDiscrepancia').css('top', posY);
    $('#lblVerDetalleDiscrepancias').text(TextosDiscrepancias[21]);
    $('#lblCerrarDiscrepancias').text(TextosDiscrepancias[22]);
    $('#divMenuDetalleDiscrepancia').show();
    if ($('#imgDiscrepanciasCerradas_' + idLineaFactura).is(':visible')) $('#btnCerrarDiscrepanciasLinea').hide();
    else $('#btnCerrarDiscrepanciasLinea').show();
}
function EstablecerTextosMenuDiscrepanciaNueva() {
    $('[id$=lblNuevoMensajeAceptar]').text(Textos[20]);
    $('[id$=btnAceptarNuevoMensaje]').attr('title', Textos[20]);
    $('[id$=lblNuevoMensajeCancelar]').text(Textos[21]);
    $('[id$=btnCancelarNuevoMensaje]').attr('title', Textos[20]);

    $('[id$=lblAgregarEtiqueta]').text(Textos[31]);
    $('[id$=btnAgregarEtiqueta]').attr('title', Textos[31]);

    $('[id$=lblNuevoMensajeAdjunto]').text(Textos[28]);
    $('[id$=NuevoMensajeAdjunto]').attr('title', Textos[28]);
    $('[id$=lblNuevoMensajeEnlace]').text(Textos[29]);
    $('[id$=NuevoMensajeEnlace]').attr('title', Textos[29]);
    $('[id$=lblNuevoMensajeEtiqueta]').text(Textos[30]);
    $('[id$=NuevoMensajeEtiqueta]').attr('title', Textos[30]);

    $('[id$=lblNuevoMensajeCategoria]').text(Textos[32]);
    $('[id$=lblNuevoMensajePara]').text(Textos[33]);
}
function MostrarFormularioDiscrepanciasFactura(idFactura, idLinea, numFactura) {
    idFacturaPopUp = idFactura;
    lineaFacturaPopUp = idLinea;
    if (scriptsLoaded) {
        LoadDiscrepancias(idFactura, idLinea);
        $('#DiscrepancialblTituloPopUpNuevoMensaje').text(TextosDiscrepancias[19] + ' ' + numFactura);
    } else {
        LoadScripts();        
        $.get(rutaFS + 'CN/html/_mensajes.tmpl.htm', function (templates) {
            $('body').append(templates);
            $('body').prepend('<div id="popupPantallaDiscrep" class="popupCN" style="display:none; position:absolute; z-index:1002; width:95%; max-height:80%; text-align:left; padding:5px; overflow:auto;"></div>');
            var cabecera = '<div id="btnCerrarPopUpDiscrepancias" class="CerrarPopUp" style="float:right; width:30px; height:30px;"></div>'
            cabecera = cabecera + '<div class="FondoHeader" style="float:left; margin-bottom:5px;"><img src="' + ruta + 'images/Discrepancia.png" style="float:left; margin-left:10px; max-height:25px;" />';
            cabecera = cabecera + '<span id="DiscrepancialblTituloPopUpNuevoMensaje" class="LabelTituloCabeceraCustomControl" style="margin-left:10px; line-height:25px;"></span></div>';
            cabecera = cabecera + '<div id="divDetalleDiscrepancias" style="clear:both; float:left; width:100%; overflow:auto;"></div>';
            $('#popupPantallaDiscrep').prepend(cabecera);
            $('#DiscrepancialblTituloPopUpNuevoMensaje').text(TextosDiscrepancias[19] + ' ' + numFactura);
            $.get(rutaFS + 'CN/html/_detalle_discrp_linea.tmpl.htm', function (templates) {
                $('body').append(templates);
                LoadDiscrepancias(idFactura, idLinea);
            });
        });
    }
}
function LoadScripts() {
    $('#btnCerrarPopUpDiscrepancias').live('click', function () {
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'PM/App_Services/Facturas.asmx/Comprobar_DiscrepanciasCerradas',
            data: JSON.stringify({ IdFactura: idFacturaPopUp, Linea: lineaFacturaPopUp }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true
        })).done(function (msg) {
            if(parseInt(msg.d)==0){
                $('#imgDiscrepancias_' + lineaFacturaPopUp).hide();
                $('#imgDiscrepanciasCerradas_' + lineaFacturaPopUp).show();
            }else{
                $('#imgDiscrepancias_' + lineaFacturaPopUp).show();
                $('#imgDiscrepanciasCerradas_' + lineaFacturaPopUp).hide();
            };
            $('#popupFondo').hide();
            $('#popupPantallaDiscrep').hide();
        });        
    });
    $('[id^=btnCerrarDiscrepancia_]').live('click', function () {
        var IdDiscrepancia = $(this).attr('id').split('_')[1];
        $('[id^=lblDiscrepanciaAbierta_' + IdDiscrepancia + ']').hide();
        $('[id^=lblDiscrepanciaCerrada_' + IdDiscrepancia + ']').show();
        $('[id^=btnCerrarDiscrepancia_' + IdDiscrepancia + ']').hide();
        $.ajax({
            type: "POST",
            url: rutaFS + 'PM/App_Services/Facturas.asmx/Cerrar_Discrepancia',
            data: JSON.stringify({ IdDiscrepancia: IdDiscrepancia }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true
        });
    });
    $('[id^=divMostrarIncidenciasCerradas_]').live('click', function () {
        var IdLinea = $(this).attr('id').split('_')[1];
        $('#divMuro_' + IdLinea + ' li').show();
        $('#divMostrarIncidenciasCerradas_' + IdLinea).hide();
        CentrarPopUp($('#popupPantallaDiscrep'));
    });
    scriptsLoaded = true;
}
function LoadDiscrepancias(idFactura, idLinea) {
    $('#divDetalleDiscrepancias').empty();
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/App_Services/Facturas.asmx/Obtener_Lineas_Factura',
        data: JSON.stringify({ Factura: idFactura, Linea: idLinea }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true
    })).done(function (msg) {
        $('#detalleDiscrpLinea').tmpl(msg.d).appendTo($('#divDetalleDiscrepancias'));
        $('#divMuro_' + idLinea).cn_ObtenerMensajesUsuario({
            WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/ObtenerMensajesUsuario',
            tipo: 0,
            grupo: 0,
            categoria: 0,
            megusta: false,
            pagina: 1,
            historico: false,
            discrepancias: true,
            factura: idFactura,
            linea: idLinea,
            onDone: function () { ShowPopUpDiscrepancias(true,idLinea) }
        });

    });
}
function ShowPopUpDiscrepancias(LoadComplete,idLinea) {
    if (LoadComplete) {
        $('[id^=lblDetalleLinea_]').text(TextosDiscrepancias[5]);
        $('[id^=lblHeaderCodigo_]').text(TextosDiscrepancias[6]);
        $('[id^=lblHeaderDenominacion_]').text(TextosDiscrepancias[7]);
        $('[id^=lblHeaderCantidad_]').text(TextosDiscrepancias[8]);
        $('[id^=lblHeaderPrecio_]').text(TextosDiscrepancias[9]);
        $('[id^=lblHeaderTotalCostes_]').text(TextosDiscrepancias[10]);
        $('[id^=lblHeaderTotalDescuentos_]').text(TextosDiscrepancias[11]);
        $('[id^=lblHeaderImporteNeto_]').text(TextosDiscrepancias[4]);
        $('[id^=lblHeaderObservaciones_]').text(TextosDiscrepancias[12]);
        $('[id^=lblHeaderPedidoFullstep_]').text(TextosDiscrepancias[13]);
        $('[id^=lblHeaderRefFactura_]').text(TextosDiscrepancias[2]);
        $('[id^=lblHeaderAlbaran_]').text(TextosDiscrepancias[1]);
        $('[id^=lblHeaderNumDocSAP_]').text(TextosDiscrepancias[3]);
        $('[id^=lblInfoLinea_]').text(TextosDiscrepancias[14]);
        $('[id^=lblDiscrepanciaAbierta_]').text('(' + TextosDiscrepancias[15] + ')');
        $('[id^=lblDiscrepanciaCerrada_]').text('(' + TextosDiscrepancias[16] + ')');
        $('[id^=lblCerrarDiscrepancia_]').text(TextosDiscrepancias[17]);
        $('[id^=lblMostrarIncidenciasCerradas_]').text(TextosDiscrepancias[18]);        
        
        $('#popupPantallaDiscrep').show();
        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();
        if ($('#divMuro_' + idLinea + ' li:visible').length == 0) $('#divMuro_' + idLinea + ' li').show();
        if ($('#divMuro_' + idLinea + ' li:hidden').length > 0) {
            $('#divMostrarIncidenciasCerradas_' + idLinea).show();
        } else {
            $('#divMostrarIncidenciasCerradas_' + idLinea).hide();
        }
        CentrarPopUp($('#popupPantallaDiscrep'));
    }
};
var edicionDiscrepancia = false;
$('#popupPantallaDiscrep [id^=msgEditar]').live('click', function () {
    $('#popupPantallaDiscrep').hide();
    edicionDiscrepancia = true;
});
$('#popupNuevoMensaje [id^=btnCancelarNuevoMensajeMaster]').live('click', function () {
    if (typeof (edicionDiscrepancia) !== 'undefined' && edicionDiscrepancia) {
        edicionDiscrepancia = false;
        $('#popupFondo').show();
        $('#popupPantallaDiscrep').show();
    }
});
$('#popupNuevoMensaje [id^=btnAceptarNuevoMensajeMaster]').live('click', function () {
    if (typeof (edicionDiscrepancia) !== 'undefined' && edicionDiscrepancia) {
        edicionDiscrepancia = false;
        $('#popupFondo').show();
        $('#popupPantallaDiscrep').show();
        LoadDiscrepancias(idFacturaPopUp, lineaFacturaPopUp);
    }
});
function MostrarMenuAcciones(btn) {
    var top = $(btn).offset().top + 20;
    var left = $(btn).offset().left;
    $('#divAcciones').css({ top: top + 'px' });
    $('#divAcciones').css({ left: left + 'px' });
    $('#divAcciones').show();
}