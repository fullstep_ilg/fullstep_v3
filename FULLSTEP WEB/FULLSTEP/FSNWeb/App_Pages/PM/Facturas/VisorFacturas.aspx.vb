﻿Imports Infragistics.Web.UI.GridControls
Imports Infragistics.Documents.Reports
Imports Infragistics.Documents.Reports.Report
Imports Fullstep.FSNServer

Public Class VisorFacturas
    Inherits FSNPage

    Private _pageNumber As Integer = 0
    ''' <summary>
    ''' Dataset de Facturas
    ''' </summary>
    Private ReadOnly Property DatasetFacturas() As DataSet
        Get
            If HttpContext.Current.Cache("dsFacturas_" & FSNUser.Cod & "_" & Idioma) IsNot Nothing Then
                Return HttpContext.Current.Cache("dsFacturas_" & FSNUser.Cod & "_" & Idioma)
            Else
                Return ObtenerDataSetFacturas()
            End If
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorFacturas

        ' Discrepancias
        ' Limite de mensajes cargados 
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "LimiteMensajesCargados") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "LimiteMensajesCargados", "<script>var LimiteMensajesCargados =" & System.Configuration.ConfigurationManager.AppSettings("LimiteMensajesCargados") & "</script>")
        End If

        Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.Colaboracion
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPantalla") Then
            Dim sVariableJavascriptTextos As String = "var Textos = new Array();"
            Dim sVariableJavascriptTextosCKEditor As String = "var TextosCKEditor = new Array();"
            Dim sVariableJavascriptTextosDiscrepancias As String = "var TextosDiscrepancias = new Array();"
            For i As Integer = 1 To 73
                sVariableJavascriptTextos &= "Textos[" & i & "]='" & JSText(Textos(i)) & "';"
                If i = 20 Then sVariableJavascriptTextosCKEditor &= "TextosCKEditor[1]='" & JSText(Textos(i)) & "';"
                If i = 21 Then sVariableJavascriptTextosCKEditor &= "TextosCKEditor[2]='" & JSText(Textos(i)) & "';"
            Next

            For i As Integer = 83 To 85
                sVariableJavascriptTextos &= "Textos[" & i - 9 & "]='" & JSText(Textos(i)) & "';"
            Next
            For i As Integer = 74 To 89
                sVariableJavascriptTextosCKEditor &= "TextosCKEditor[" & i - 71 & "]='" & JSText(Textos(i)) & "';"
            Next
            For i As Integer = 90 To 121
                sVariableJavascriptTextos &= "Textos[" & i - 13 & "]='" & JSText(Textos(i)) & "';"
            Next
            For i As Integer = 142 To 143
                sVariableJavascriptTextos &= "Textos[" & i - 33 & "]='" & JSText(Textos(i)) & "';"
            Next
            For i As Integer = 144 To 147
                sVariableJavascriptTextosCKEditor &= "TextosCKEditor[" & i - 125 & "]='" & JSText(Textos(i)) & "';"
            Next
            For i As Integer = 148 To 150
                sVariableJavascriptTextos &= "Textos[" & i - 37 & "]='" & JSText(Textos(i)) & "';"
            Next
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorFacturas
            sVariableJavascriptTextosDiscrepancias &= "TextosDiscrepancias[1]='" & JSText(Textos(8)) & "';"
            sVariableJavascriptTextosDiscrepancias &= "TextosDiscrepancias[2]='" & JSText(Textos(11)) & "';"
            sVariableJavascriptTextosDiscrepancias &= "TextosDiscrepancias[3]='" & JSText(Textos(14)) & "';"
            sVariableJavascriptTextosDiscrepancias &= "TextosDiscrepancias[4]='" & JSText(Textos(34)) & "';"
            For i As Integer = 55 To 69
                sVariableJavascriptTextosDiscrepancias &= "TextosDiscrepancias[" & i - 50 & "]='" & JSText(Textos(i)) & "';"
            Next
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextos & sVariableJavascriptTextosCKEditor & sVariableJavascriptTextosDiscrepancias, True)
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutaTheme") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaTheme", "<script>var rutaTheme = '" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "'</script>")
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutaFS") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("ruta") & "'</script>")
        End If
        If Not IsPostBack Then

            Dim mMaster = CType(Me.Master, FSNWeb.Menu)
            mMaster.Seleccionar("Facturacion", "Autofacturacion")

            ObtenerDatosIntegracion()

            FSNPageHeader.TituloCabecera = Textos(0)
            FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/factura.png"

            Busqueda.Textos = TextosModuloCompleto
            Busqueda.AccesoFSSM = Acceso.gbAccesoFSSM
            Busqueda.OblCodPedDir = (Acceso.gbOblCodPedDir OrElse Acceso.gbOblCodPedido)

            Busqueda.RutaBuscadorArticulo = ConfigurationManager.AppSettings("rutaFS") & "_common/BuscadorArticulos.aspx?desde=Visor"
            Busqueda.PropiedadesBuscadorArticulo = "width=850,height=660,status=yes,resizable=no,top=50,left=200"
            Busqueda.RutaBuscadorGestor = ConfigurationManager.AppSettings("rutaFS") & "_Common/BuscadorUsuarios.aspx"
            Busqueda.RutaBuscadorCentroCoste = ConfigurationManager.AppSettings("rutaFS") & "_Common/BuscadorCentrosCoste.aspx"
            Busqueda.RutaBuscadorPartidas = ConfigurationManager.AppSettings("rutaFS") & "_Common/BuscadorPartidas.aspx"
            Busqueda.RutaWsAutoComplete = ConfigurationManager.AppSettings("rutaFS") & "_Common/App_Services/Autocomplete.asmx"

            'Busqueda.HayIntegracionFacturas = Session("HayIntegracionFacturas")

            'Establecemos en el campo desde de Fecha Factura de la busqueda avanzada la fecha configurada
            Busqueda.FechaFacturaDesde = DevolverFechaDesdeConfigurada()
            Busqueda.EstadosFacturas = FSNWeb.modFacturas.DevolverEstados(Idioma).Tables(0)
            Busqueda.PartidasDataTable = FSNWeb.modFacturas.DevolverPartidas(Idioma)

            imgCerrarPagos.ImageUrl = ConfigurationManager.AppSettings("ruta") & "images/Bt_Cerrar.png"
            imgPagos.ImageUrl = ConfigurationManager.AppSettings("ruta") & "images/Pedido.png"

            DirectCast(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgPDF"), Image).ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/PDF.png"
            DirectCast(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgExcel"), Image).ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/Excel.png"
            DirectCast(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgCalendarioFecDesde"), Image).ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Page.Theme & "/images/calendar-color.png"

            FSNPanelEnProceso.ServicePath = ConfigurationManager.AppSettings("rutaFS") & "_Common/App_Services/Consultas.asmx"
            FSNPanelEnProceso.ImagenPopUp = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Page.Theme & "/images/Icono_Error_Amarillo_40x40.gif"

            FSNPanelMotivoAnulacion.ServicePath = ConfigurationManager.AppSettings("rutaFS") & "_Common/App_Services/Consultas.asmx"
            FSNPanelMotivoAnulacion.ImagenPopUp = ConfigurationManager.AppSettings("ruta") & "images/trans.gif"

            ModuloIdioma = TiposDeDatos.ModulosIdiomas.Notificaciones
            CType(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPage"), Label).Text = Textos(46)
            CType(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblOF"), Label).Text = Textos(48)

            ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorFacturas
            lblLitTituloPagos.Text = Textos(26)
            lblProveedor.Text = String.Format("{0}:", Textos(1))
            lblEmpresa.Text = String.Format("{0}:", Textos(16))
            lblNumeroFactura.Text = String.Format("{0}:", Textos(2))
            btnBuscar.Text = Textos(4)
            btnBuscarBusquedaAvanzada.Text = Textos(4)
            btnLimpiarBusquedaAvanzada.Text = Textos(18)
            lblBusquedaAvanzada.Text = Textos(5)
            btnAnyadirFiltros.Text = Textos(19)

            DirectCast(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("btnConfigurar"), FSNWebControls.FSNButton).Text = Textos(23)
            DirectCast(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblExceptoFechaDesde"), Label).Text = Textos(50)
            DirectCast(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblExcel"), Label).Text = Textos(24)
            DirectCast(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPDF"), Label).Text = Textos(25)

            DirectCast(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), Image).Attributes.Add("onclick", "FirstPage();")
            DirectCast(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), Image).Attributes.Add("onclick", "PrevPage();")
            DirectCast(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), Image).Attributes.Add("onclick", "NextPage();")
            DirectCast(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), Image).Attributes.Add("onclick", "LastPage();")

            DirectCast(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("btnConfigurar"), FSNWebControls.FSNButton).Visible = (Session("IDFiltroFacturaUsuario") <> Nothing)
            DirectCast(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("btnConfigurar"), FSNWebControls.FSNButton).Attributes.Add("onclick", "window.location='ConfigurarFiltrosFacturas.aspx?IDFiltroConfigurando=" & Session("IDFiltroFacturaUsuario") & "';return false;")

            SeleccionarPestanyaDefecto()

            CargarBusquedaAvanzada()

            WriteScripts()

            CreateColumns()

            whdgFacturas.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PaginacionFacturas")
            whdgFacturas.GridView.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PaginacionFacturas")

            CargarGridFacturas(ObtenerDataSetFacturas())

            CargarNumeroFacturasPendientes()

            With CType(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero_desactivado.gif"
                .Enabled = False
            End With
            With CType(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior_desactivado.gif"
                .Enabled = False
            End With
            Dim SoloUnaPagina As Boolean = (whdgFacturas.GridView.Behaviors.Paging.PageCount = 1)
            With CType(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & If(SoloUnaPagina, "_desactivado", "") & ".gif"
                .Enabled = Not SoloUnaPagina
            End With
            With CType(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & If(SoloUnaPagina, "_desactivado", "") & ".gif"
                .Enabled = Not SoloUnaPagina
            End With
        Else
            Select Case Request("__EVENTTARGET")
                Case "ExportarExcel"
                    CargarGridFacturas()
                    ExportarExcel()
                Case "ExportarPDF"
                    CargarGridFacturas()
                    ExportarPDF()
                Case Else
                    CargarGridFacturas()
            End Select
        End If
    End Sub
    ''' <summary>
    ''' Muestra la alerta con el número de facturas pendientes.
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load.</remarks>
    Private Sub CargarNumeroFacturasPendientes()
        Dim lNumFacturasPendientes As Long

        Dim f As FSNServer.Facturas = FSNServer.Get_Object(GetType(FSNServer.Facturas))
        lNumFacturasPendientes = f.DevolverNumeroFacturasPendientes(FSNUser.CodPersona)
        f = Nothing

        If lNumFacturasPendientes > 0 Then
            If lNumFacturasPendientes <> 1 Then
                lblFacturasPendientes.Text = Replace(Textos(35), "###", lNumFacturasPendientes)
            Else
                lblFacturasPendientes.Text = Replace(Textos(70), "###", lNumFacturasPendientes)
            End If
            phAlerta.Visible = True
            CeldaBusquedaGeneral.Width = Unit.Percentage(60).ToString
        Else
            phAlerta.Visible = False
            CeldaBusquedaGeneral.Width = Unit.Percentage(90).ToString
        End If
    End Sub
    ''' <summary>
    ''' Seleccionada la pestaña en la que estamos o la que se ha marcado como defecto cuando se entra por primera vez.
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load()</remarks>
    Private Sub SeleccionarPestanyaDefecto()
        Dim arrParametros() As String
        Dim FiltroUsuarioIdAnt, FiltroIdAnt As String

        FiltroUsuarioIdAnt = Request.QueryString("FiltroUsuarioIdAnt")
        FiltroIdAnt = Request.QueryString("FiltroIdAnt")

        If FiltroIdAnt = String.Empty Then
            arrParametros = Split(CType(dlFiltrosConfigurados.Items(0).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).CommandArgument, "#")
            Session("FACT_Ordenacion") = ""
            If arrParametros(0) = 0 Then
                Session("IDFiltroFacturaUsuario") = ""
                Session("IDFiltroFactura") = ""
            Else
                Session("IDFiltroFacturaUsuario") = arrParametros(0)
                Session("IDFiltroFactura") = arrParametros(1)
            End If
            CType(dlFiltrosConfigurados.Items(0).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).Selected = True
        Else
            Dim sTabSeleccionado As String = FiltroUsuarioIdAnt & "#" & FiltroIdAnt
            For index As Integer = 0 To dlFiltrosConfigurados.Items.Count - 1
                If sTabSeleccionado = CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).CommandArgument Then
                    arrParametros = Split(CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).CommandArgument, "#")
                    Session("IDFiltroFacturaUsuario") = arrParametros(0)
                    Session("IDFiltroFactura") = arrParametros(1)
                    CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).Selected = True
                    Exit For
                End If
            Next
        End If
    End Sub
    ''' <summary>
    ''' Inicializa javascript de los elementos
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0seg.</remarks>
    Private Sub WriteScripts()
        txtProveedor.Attributes.Add("onkeydown", "javascript:return eliminarProveedor(event)")
        txtEmpresa.Attributes.Add("onkeydown", "javascript:return eliminarEmpresa(event)")

        imgProveedorLupa.OnClientClick = "var newWindow = window.open('" & ConfigurationManager.AppSettings("RutaFS") & "_Common/BuscadorProveedores.aspx?PM=true&Contr=true', '_blank', 'width=830,height=530,status=yes,resizable=no,top=150,left=150');return false;"
        imgEmpresaLupa.Attributes.Add("onClick", "javascript:var newWindow = window.open('" & ConfigurationManager.AppSettings("rutaFS") & "_common/BuscadorEmpresas.aspx','_blank','width=800,height=600, location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =no');return false;")

        'Funcion que sirve para eliminar el proveedor
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarProveedor") Then
            Dim sScript As String = "function eliminarProveedor(event) {" & vbCrLf & _
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf & _
                    "  " & vbCrLf & _
            " } else { " & vbCrLf & _
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                    " campoProveedor = document.getElementById('" & txtProveedor.ClientID & "')" & vbCrLf & _
                    " if (campoProveedor) { campoProveedor.value = ''; } " & vbCrLf & _
                    " campoHiddenProveedor = document.getElementById('" & hidProveedor.ClientID & "')" & vbCrLf & _
                    " if (campoHiddenProveedor) { campoHiddenProveedor.value = ''; } " & vbCrLf & _
            " } " & vbCrLf & _
            "  " & vbCrLf & _
            " } }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarProveedor", sScript, True)
        End If

        'Funcion que sirve para eliminar la empresa relacionada
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarEmpresa") Then
            Dim sScript As String = "function eliminarEmpresa(event) {" & vbCrLf & _
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf & _
                    "  " & vbCrLf & _
            " } else { " & vbCrLf & _
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                    " campoEmpresa = document.getElementById('" & txtEmpresa.ClientID & "')" & vbCrLf & _
                    " if (campoEmpresa) { campoEmpresa.value = ''; } " & vbCrLf & _
                    " campoHiddenEmpresa = document.getElementById('" & hidEmpresa.ClientID & "')" & vbCrLf & _
                    " if (campoHiddenEmpresa) { campoHiddenEmpresa.value = ''; } " & vbCrLf & _
            " } " & vbCrLf & _
            "  " & vbCrLf & _
            " } }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarEmpresa", sScript, True)
        End If
    End Sub
    Protected Sub fsnTabFiltro_Click(ByVal sender As Object, ByVal e As EventArgs)
        whdgFacturas.Behaviors.Filtering.ColumnSettings.Clear()
        whdgFacturas.GridView.Behaviors.Filtering.ColumnSettings.Clear()
        whdgFacturas.Columns.Clear()
        whdgFacturas.GridView.Columns.Clear()

        Dim arrParametros() As String
        arrParametros = Split(CType(sender, FSNWebControls.FSNTab).CommandArgument, "#")

        Session("FACT_Ordenacion") = ""
        If arrParametros(0) = 0 Then
            Session("IDFiltroFacturaUsuario") = ""
            Session("IDFiltroFactura") = ""
        Else
            Session("IDFiltroFacturaUsuario") = arrParametros(0)
            Session("IDFiltroFactura") = arrParametros(1)
        End If

        CType(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("btnConfigurar"), FSNWebControls.FSNButton).Visible = (Session("IDFiltroFacturaUsuario") <> "")
        If CType(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("btnConfigurar"), FSNWebControls.FSNButton).Visible Then
            CType(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("btnConfigurar"), FSNWebControls.FSNButton).Attributes.Add("onClick", "window.location='ConfigurarFiltrosFacturas.aspx?IDFiltroConfigurando=" & Session("IDFiltroFacturaUsuario") & "';return false;")
        End If

        VaciarControlesRecursivo(pnlParametros)
        txtProveedor.Text = String.Empty
        hidProveedor.Value = String.Empty
        txtEmpresa.Text = String.Empty
        hidEmpresa.Value = String.Empty
        CargarBusquedaAvanzada()

        upBusquedaAvanzada.Update()
        upBusquedaGeneral.Update()

        whdgFacturas.GridView.Behaviors.Paging.PageIndex = 0

        CreateColumns()
        CargarGridFacturas(ObtenerDataSetFacturas())

        With CType(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero_desactivado.gif"
            .Enabled = False
        End With
        With CType(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior_desactivado.gif"
            .Enabled = False
        End With
        Dim SoloUnaPagina As Boolean = (whdgFacturas.GridView.Behaviors.Paging.PageCount = 1)

        With CType(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & If(SoloUnaPagina, "_desactivado", "") & ".gif"
            .Enabled = Not SoloUnaPagina
        End With


        With CType(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & If(SoloUnaPagina, "_desactivado", "") & ".gif"
            .Enabled = Not SoloUnaPagina
        End With

        'solo se queda chequeada la pestaña del filtro chequeada
        For index As Integer = 0 To dlFiltrosConfigurados.Items.Count - 1
            CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).Selected = False
        Next
        CType(sender, FSNWebControls.FSNTab).Selected = True
    End Sub
    ''' <summary>
    ''' Evento que se lanza al acceder a datos y que usamos para decirle al origen de datos cual va a ser su objeto.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: En la carga de la página VisorSolicitudes.aspx; Tiempo máximo: 0 sg.</remarks>
    Private Sub odsFiltrosConfigurados_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles odsFiltrosConfigurados.ObjectCreating
        e.ObjectInstance = FSNServer.Get_Object(GetType(FSNServer.Filtros))
    End Sub
    ''' <summary>
    ''' Evento que se lanza cuando el objeto de origen de datos recupera los datos. Lo usamos para pasarle los parámetros que necesita su método SELECT configurado.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: En la carga de la página VisorSolicitudes.aspx; Tiempo máximo: 0 sg.</remarks>
    Private Sub odsFiltrosConfigurados_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsFiltrosConfigurados.Selecting
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorFacturas
        e.InputParameters.Item("sUsuario") = FSNUser.CodPersona
    End Sub
#Region "Template columna CHECKBOX"
    Private Class CheckboxTemplate
        Implements ITemplate

        ''' <summary>
        ''' Rellena un Item del webdatagrid 
        ''' </summary>
        ''' <param name="container">Item del webdatagrid a rellenar</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Public Sub InstantiateIn(ByVal container As System.Web.UI.Control) Implements System.Web.UI.ITemplate.InstantiateIn
            Dim chk As New CheckBox
            chk.ID = "chk"
            chk.Width = Unit.Pixel(25)
            chk.Attributes.Add("align", "center")
            container.Controls.Add(chk)
        End Sub

        Public Sub New()

        End Sub
    End Class
#End Region
#Region "Template cabecera BUTTON"
    Private Class ButtonTemplate
        Implements ITemplate

        Private _texto As String
        Private _id As String

        ''' <summary>
        ''' Rellena un Item del webdatagrid 
        ''' </summary>
        ''' <param name="container">Item del webdatagrid a rellenar</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Public Sub InstantiateIn(ByVal container As System.Web.UI.Control) Implements System.Web.UI.ITemplate.InstantiateIn
            Dim btn As New FSNWebControls.FSNButton
            btn.ID = _id
            btn.Text = _texto
            container.Controls.Add(btn)
        End Sub

        Public Sub New(ByVal Id As String, ByVal Texto As String)
            _id = Id
            _texto = Texto
        End Sub
    End Class
#End Region
#Region "Eventos Grid"

    Private Sub whdgFacturas_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles whdgFacturas.DataBound
        Dim pagerList As DropDownList = DirectCast(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("PagerPageList"), DropDownList)
        pagerList.Items.Clear()
        For i As Integer = 1 To whdgFacturas.GridView.Behaviors.Paging.PageCount
            pagerList.Items.Add(i.ToString())
        Next
        CType(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblCount"), Label).Text = whdgFacturas.GridView.Behaviors.Paging.PageCount

    End Sub

    Private Sub whdgFacturas_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgFacturas.InitializeRow
        Dim UrlImage As String
        Dim sNombreCss As String = "itemSeleccionable"

        e.Row.Items.FindItemByKey("IMPORTE").Text = FSNLibrary.FormatNumber(e.Row.Items.FindItemByKey("IMPORTE").Value, FSNUser.NumberFormat) & " " & e.Row.Items.FindItemByKey("MON").Value
        If e.Row.Items.FindItemByKey("ACCION_APROBAR").Value = 0 Then
            Dim chkAprobar As CheckBox = CType(e.Row.Items.FindItemByKey("APROBAR").FindControl("chk"), CheckBox)
            If chkAprobar IsNot Nothing Then
                chkAprobar.Enabled = False
            End If
        End If

        If e.Row.Items.FindItemByKey("ACCION_RECHAZAR").Value = 0 Then
            Dim chkRechazar As CheckBox = CType(e.Row.Items.FindItemByKey("RECHAZAR").FindControl("chk"), CheckBox)
            If chkRechazar IsNot Nothing Then
                chkRechazar.Enabled = False
            End If
        End If

        If e.Row.Items.FindItemByKey("ICONO").Value = 0 Then
            e.Row.Items.FindItemByKey("ICONO").Text = ""
        Else
            e.Row.Items.FindItemByKey("ICONO").Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Img_Ico_Alerta_Peq.gif" & "' style='text-align:center;'/>"

        End If

        If Session("HayIntegracionPagos") Then
            If e.Row.Items.FindItemByKey("PAGOS").Value = 0 Then
                e.Row.Items.FindItemByKey("PAGOS").Text = ""
            Else
                e.Row.Items.FindItemByKey("PAGOS").Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/aprobado.gif" & "' style='text-align:center;'/>"
                e.Row.Items.FindItemByKey("PAGOS").CssClass = "itemSeleccionable"
            End If
        End If

        If e.Row.Items.FindItemByKey("SEG").Value = "1" Then
            sNombreCss = sNombreCss & " datoRojo"
            e.Row.Items.FindItemByKey("SEG").Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/seg_selec.gif" & "' style='text-align:center;' />"
        Else
            e.Row.Items.FindItemByKey("SEG").Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/seg_noselec.gif" & "' style='text-align:center;' />"
        End If

        If e.Row.Items.FindItemByKey("TIPO").Value = "1" Then
            e.Row.Items.FindItemByKey("TIPO").Text = Textos(36)
        Else
            e.Row.Items.FindItemByKey("TIPO").Text = Textos(37)
        End If

        If e.Row.Items.FindItemByKey("EN_PROCESO").Value = "1" Then
            e.Row.Items.FindItemByKey("DEN_ESTADO").Text = Textos(54)
            e.Row.Items.FindItemByKey("ETAPA_ACTUAL").Text = Textos(54)
        Else
            e.Row.Items.FindItemByKey("DEN_ESTADO").CssClass = sNombreCss
            If e.Row.Items.FindItemByKey("ANULADO").Value = "1" Then
                UrlImage = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_Expirado.gif"
                e.Row.Items.FindItemByKey("DEN_ESTADO").Text = "<img src='" & UrlImage & "' style='text-align:center;vertical-align:middle;'/> <u>" & e.Row.Items.FindItemByKey("DEN_ESTADO").Value & "</u>"
            Else
                If e.Row.Items.FindItemByKey("ESTADO").Value < 100 Then
                    UrlImage = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_Pendiente.gif"
                Else
                    UrlImage = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_Vigente.gif"
                End If
                e.Row.Items.FindItemByKey("DEN_ESTADO").Text = "<img src='" & UrlImage & "' style='text-align:center;vertical-align:middle;'/> " & e.Row.Items.FindItemByKey("DEN_ESTADO").Value
            End If
        End If

        If Not CType(e.Row.DataItem.Item.Row.Item("TIENE_DISCREP"), Boolean) Then
            e.Row.Items.FindItemByKey("TIENE_DISCREP").Text = ""
        Else
            If CType(e.Row.DataItem.Item.Row.Item("TIENE_DISCREP_ABTAS"), Boolean) Then
                e.Row.Items.FindItemByKey("TIENE_DISCREP").Text = "<img id='imgDiscrepancias_" & CType(e.Row.DataItem.Item.Row.Item("FACTURA_ID"), Integer) & "' src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/Discrepancia.png" & "' class='Link' style='text-align:center;'/>"
                e.Row.Items.FindItemByKey("TIENE_DISCREP").Text &= "<img id='imgDiscrepanciasCerradas_" & CType(e.Row.DataItem.Item.Row.Item("FACTURA_ID"), Integer) & "' src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/Discrepancias_cerradas.png" & "' class='Link' style='text-align:center; display:none;'/>"
            Else
                e.Row.Items.FindItemByKey("TIENE_DISCREP").Text = "<img id='imgDiscrepancias_" & CType(e.Row.DataItem.Item.Row.Item("FACTURA_ID"), Integer) & "' src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/Discrepancia.png" & "' class='Link' style='text-align:center; display:none;'/>"
                e.Row.Items.FindItemByKey("TIENE_DISCREP").Text &= "<img id='imgDiscrepanciasCerradas_" & CType(e.Row.DataItem.Item.Row.Item("FACTURA_ID"), Integer) & "' src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/Discrepancias_cerradas.png" & "' class='Link' style='text-align:center;'/>"
            End If
        End If

        e.Row.Items.FindItemByKey("SEG").CssClass = sNombreCss
        e.Row.Items.FindItemByKey("TIPO").CssClass = sNombreCss
        e.Row.Items.FindItemByKey("NUM_FACTURA").CssClass = sNombreCss
        e.Row.Items.FindItemByKey("EMPRESA").CssClass = sNombreCss
        e.Row.Items.FindItemByKey("COD_PROVE").CssClass = sNombreCss
        e.Row.Items.FindItemByKey("PROVEEDOR").CssClass = sNombreCss
        e.Row.Items.FindItemByKey("FECHA").CssClass = sNombreCss
        e.Row.Items.FindItemByKey("FEC_CONTA").CssClass = sNombreCss
        If Session("HayIntegracionFacturas") Then e.Row.Items.FindItemByKey("NUM_ERP").CssClass = sNombreCss
        e.Row.Items.FindItemByKey("IMPORTE").CssClass = sNombreCss
        e.Row.Items.FindItemByKey("ETAPA_ACTUAL").CssClass = sNombreCss
    End Sub

#End Region
    Private Sub ObtenerDatosIntegracion()
        Dim oIntegracion As FSNServer.Integracion
        oIntegracion = FSNServer.Get_Object(GetType(FSNServer.Integracion))

        Session("HayIntegracionFacturas") = False
        If oIntegracion.HayIntegracion(TablasIntegracion.Facturas) Then
            Session("HayIntegracionFacturas") = True
        End If

        Session("HayIntegracionPagos") = False
        If oIntegracion.HayIntegracion(TablasIntegracion.Pagos, SentidoIntegracion.Entrada) Then
            Session("HayIntegracionPagos") = True
        End If
    End Sub
    Private Function DevolverFechaDesdeConfigurada() As Date
        Dim iMesesDesde As Byte = DBNullToInteger(System.Configuration.ConfigurationManager.AppSettings("MesesVisorFacturas"))
        Dim dFechaDesdeConfigurada As Date
        dFechaDesdeConfigurada = DateAdd(DateInterval.Month, CDbl("-" & iMesesDesde.ToString), Now.Date)
        DevolverFechaDesdeConfigurada = dFechaDesdeConfigurada
    End Function
    ''' <summary>
    ''' Funcion que obtiene el Dataset con los datos de facturas
    ''' </summary>
    ''' <returns>dataset con las facturas</returns>
    ''' <remarks></remarks>
    Private Function ObtenerDataSetFacturas() As DataSet
        Dim Proveedor As String
        Dim Empresa As Long
        Dim NumFactura As String
        Dim FecFacturaDesde As Date
        Dim FecFacturaHasta As Date
        Dim FecContaDesde As Date
        Dim FecContaHasta As Date
        Dim ImporteDesde As Double
        Dim ImporteHasta As Double
        Dim CentroCoste As String
        Dim Partidas As DataTable
        Dim Estado As Integer
        Dim AnyoPedido As String
        Dim NumCesta As Long
        Dim NumOrden As Long
        Dim Articulo As String
        Dim Albaran As String
        Dim PedidoERP As String
        Dim NumFacturaSAP As String
        Dim Gestor As String
        Dim FacturaOriginal As Boolean
        Dim FacturaRectificativa As Boolean

        'Actualizo el label que hay en el paginador del grid de facturas con la fecha
        DirectCast(Me.whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblFechaDesde"), Label).Text = Textos(48) & " " & Busqueda.FechaFacturaDesde.ToShortDateString & " "
        DirectCast(Me.whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("divFechaDesde"), HtmlGenericControl).Visible = If(Busqueda.FechaFacturaDesde = Nothing, False, True)

        Proveedor = hidProveedor.Value
        Empresa = If(hidEmpresa.Value = "", 0, CLng(hidEmpresa.Value))
        NumFactura = txtNumeroFactura.Text
        FecFacturaDesde = Busqueda.FechaFacturaDesde
        FecFacturaHasta = Busqueda.FechaFacturaHasta
        FecContaDesde = Busqueda.FechaContabilizacionDesde
        FecContaHasta = Busqueda.FechaContabilizacionHasta
        ImporteDesde = Busqueda.ImporteDesde
        ImporteHasta = Busqueda.ImporteHasta
        CentroCoste = Busqueda.CentroCosteHidden
        Partidas = Busqueda.Partidas
        Gestor = Busqueda.GestorHidden
        Estado = Busqueda.Estado
        AnyoPedido = Busqueda.AnyoPedido
        NumCesta = Busqueda.NumCesta
        NumOrden = Busqueda.NumPedido
        Articulo = Busqueda.ArticuloHidden
        Albaran = Busqueda.NumeroAlbaran
        PedidoERP = Busqueda.PedidoERP
        NumFacturaSAP = Busqueda.NumeroFacturaSAP
        FacturaOriginal = Busqueda.FacturaOriginal
        FacturaRectificativa = Busqueda.FacturaRectificativa

        Dim f As FSNServer.Facturas = FSNServer.Get_Object(GetType(FSNServer.Facturas))
        Dim dsFacturas As DataSet = f.Visor_Facturas(FSNUser.CodPersona, FSNUser.Idioma, Proveedor, Empresa, NumFactura, FecFacturaDesde, FecFacturaHasta, FecContaDesde, FecContaHasta, ImporteDesde, ImporteHasta, CentroCoste, Partidas, Estado, AnyoPedido, NumCesta, NumOrden, Articulo, Albaran, PedidoERP, NumFacturaSAP, Gestor, FacturaOriginal, FacturaRectificativa)

        If Not dsFacturas Is Nothing Then Me.InsertarEnCache("dsFacturas_" & FSNUser.Cod & "_" & Idioma, dsFacturas, CacheItemPriority.BelowNormal)

        Return dsFacturas
    End Function
    ''' <summary>
    ''' Metodo que enlaza los datos de facturas con el grid
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarGridFacturas(Optional ByVal dsFacturas As DataSet = Nothing)
        If dsFacturas Is Nothing Then
            whdgFacturas.DataSource = DatasetFacturas
        Else
            whdgFacturas.DataSource = dsFacturas
        End If
        whdgFacturas.DataMember = "FACTURAS"
        whdgFacturas.DataKeyFields = "FACTURA_ID"
        whdgFacturas.Rows.Clear() 'Pruebas 31900.7 / 2013 / 108 NO SE ACTUALIZABA con el nuevo dataset de facturas nos mostraba un dato anterior
        whdgFacturas.DataBind()
    End Sub
    Private Sub CreateColumns()
        If IsPostBack Then
            AddColumnTemplate("btnAprobar", "APROBAR", Textos(20))
            AddColumnTemplate("btnRechazar", "RECHAZAR", Textos(21))
        End If

        AddColumn("ICONO", "", True, 20)
        AddColumn("FACTURA_ID", "", False)

        If Session("IDFiltroFacturaUsuario") = Nothing Then
            AddColumn("NUM_FACTURA", Textos(30), True, 120)
            AddColumn("EMPRESA", Textos(16), True, 200)
            AddColumn("COD_PROVE", Textos(1), True, 150)
            AddColumn("PROVEEDOR", Textos(31), True, 200)
            AddColumn("FECHA", Textos(32), True, 70)
            AddColumn("FEC_CONTA", Textos(33), True, 70)

            If Session("HayIntegracionFacturas") Then AddColumn("NUM_ERP", Textos(14), True, 100)

            AddColumn("IMPORTE", Textos(34), True, 70)
            AddColumn("DEN_ESTADO", Textos(38), True, 200)
            AddColumn("ETAPA_ACTUAL", Textos(39), True, 200)
            AddColumn("TIPO", Textos(40), True, 70)

            If Session("HayIntegracionPagos") Then AddColumn("PAGOS", Textos(41), True, 50)

            AddColumn("SEG", "", True, 20)
        Else
            Dim arrDatosGenerales() As String
            ReDim arrDatosGenerales(13)
            arrDatosGenerales(1) = "NUM_FACTURA"
            arrDatosGenerales(2) = "FECHA"
            arrDatosGenerales(3) = "EMPRESA"
            arrDatosGenerales(4) = "FEC_CONTA"
            arrDatosGenerales(5) = "COD_PROVE"
            arrDatosGenerales(6) = "NUM_ERP"
            arrDatosGenerales(7) = "PROVEEDOR"
            arrDatosGenerales(8) = "ETAPA_ACTUAL"
            arrDatosGenerales(9) = "IMPORTE"
            arrDatosGenerales(10) = "PAGOS"
            arrDatosGenerales(11) = "DEN_ESTADO"
            arrDatosGenerales(12) = "SEG"
            arrDatosGenerales(13) = "TIPO"

            Dim cFiltro As FSNServer.FiltroFactura
            cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroFactura))
            cFiltro.IDFiltro = Session("IDFiltroFactura")
            cFiltro.IDFiltroUsuario = Session("IDFiltroFacturaUsuario")
            cFiltro.Persona = FSNUser.CodPersona
            cFiltro.Idioma = FSNUser.Idioma
            cFiltro.LoadFiltro()

            With cFiltro.CamposGenerales
                For i = 0 To 12
                    Select Case arrDatosGenerales(.Rows(i).Item("CAMPO"))
                        Case "NUM_ERP"
                            If Session("HayIntegracionFacturas") Then AddColumn("NUM_ERP", .Rows(i).Item("NOMBRE"), .Rows(i).Item("VISIBLE"), .Rows(i).Item("TAMANYO"), .Rows(i).Item("POS"))
                        Case "PAGOS"
                            If Session("HayIntegracionPagos") Then AddColumn("PAGOS", .Rows(i).Item("NOMBRE"), .Rows(i).Item("VISIBLE"), .Rows(i).Item("TAMANYO"), .Rows(i).Item("POS"))
                        Case Else
                            AddColumn(arrDatosGenerales(.Rows(i).Item("CAMPO")), .Rows(i).Item("NOMBRE"), .Rows(i).Item("VISIBLE"), .Rows(i).Item("TAMANYO"), .Rows(i).Item("POS"))
                    End Select
                Next
            End With
        End If

        AddColumn("ESTADO", "", False)
        AddColumn("ANULADO", "", False)
        AddColumn("OBSERVADOR", "", False)
        AddColumn("INSTANCIA", "", False)
        AddColumn("TRASLADADA", "", False)
        AddColumn("EN_PROCESO", "", False)
        AddColumn("ACCION_APROBAR", "", False)
        AddColumn("ACCION_RECHAZAR", "", False)
        AddColumn("BLOQUE", "", False)
        AddColumn("MON", "", False)
        AddColumn("GESTOR", "", False)
        AddColumn("RECEPTOR", "", False)
        AddColumn("TIENE_DISCREP", "", True, 30)
    End Sub
    Private Sub AddColumn(ByVal fieldName As String, ByVal headerText As String, Optional ByVal Visible As Boolean = True, Optional ByVal Tamanyo As Integer = 0, Optional ByVal Posicion As Integer = 0)
        Dim field As New BoundDataField(True)
        field.Key = fieldName
        field.DataFieldName = fieldName
        field.Header.Text = headerText
        field.Hidden = Not Visible
        If Tamanyo > 0 Then
            field.Width = Unit.Pixel(Tamanyo)
        End If
        'Me.whdgFacturas.Columns.Add(field)
        If Posicion > 0 Then
            Me.whdgFacturas.GridView.Columns.Insert(Posicion, field)
        Else
            Me.whdgFacturas.GridView.Columns.Add(field)
        End If

        AnyadirTextoAColumnaNoFiltrada(field.Key)
    End Sub
    Private Sub AddColumnTemplate(ByVal id As String, ByVal key As String, ByVal buttonText As String, Optional ByVal index As Integer = -1)
        Dim field As New TemplateDataField
        field.HeaderTemplate = New ButtonTemplate(id, buttonText)
        field.ItemTemplate = New CheckboxTemplate
        field.Key = key
        field.Width = Unit.Pixel(80)
        If index = -1 Then
            Me.whdgFacturas.GridView.Columns.Add(field)
        Else
            Me.whdgFacturas.GridView.Columns.Insert(index, field)
        End If
    End Sub
    Private Sub AnyadirTextoAColumnaNoFiltrada(ByVal fieldKey As String, Optional ByVal Enabled As Boolean = True, _
                                               Optional ByVal AddToGridview As Boolean = True)
        Dim fieldSetting As Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldSetting.ColumnKey = fieldKey
        fieldSetting.Enabled = Enabled
        fieldSetting.BeforeFilterAppliedAltText = Textos(42)
        If Not AddToGridview Then whdgFacturas.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        If AddToGridview Then whdgFacturas.GridView.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
    End Sub
    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Dim dsFacturas As DataSet

        dsFacturas = ObtenerDataSetFacturas()
        CargarGridFacturas(dsFacturas)
    End Sub
    Private Sub btnBuscarBusquedaAvanzada_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarBusquedaAvanzada.Click
        Dim dsFacturas As DataSet
        dsFacturas = ObtenerDataSetFacturas()
        CargarGridFacturas(dsFacturas)
    End Sub
    ''' <summary>
    ''' Limpia los campos de la busqueda avanzada
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnLimpiarBusquedaAvanzada_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiarBusquedaAvanzada.Click
        'Recorremos los controles del panel y los vamos limpiando según el tipo
        VaciarControlesRecursivo(pnlParametros)
        Busqueda.Estado = 0 'Incidencia 34 al limpiar se queda el estado elegido
        upBusquedaAvanzada.Update()
    End Sub
    ''' <summary>
    ''' Método que recorre de manera recursiva el control recibido y vacía las selecciones de todos los controles
    ''' </summary>
    ''' <param name="oControlBase">Control a recorrer</param>
    ''' <remarks>Llamada desde btnLimpiar_Click</remarks>
    Private Sub VaciarControlesRecursivo(ByVal oControlBase As Object)
        For Each oControl In oControlBase.Controls
            Select Case oControl.GetType().ToString
                Case GetType(DropDownList).ToString
                    If CType(oControl, DropDownList).SelectedItem IsNot Nothing Then _
                        CType(oControl, DropDownList).SelectedItem.Selected = False
                Case GetType(TextBox).ToString
                    CType(oControl, TextBox).Text = String.Empty
                Case GetType(Infragistics.Web.UI.EditorControls.WebDatePicker).ToString
                    CType(oControl, Infragistics.Web.UI.EditorControls.WebDatePicker).Value = Nothing
                Case GetType(Infragistics.Web.UI.EditorControls.WebNumericEditor).ToString
                    CType(oControl, Infragistics.Web.UI.EditorControls.WebNumericEditor).Value = Nothing
                Case GetType(System.Web.UI.WebControls.HiddenField).ToString
                    'No tiene que borrar los hidden donde se indica el codigo de cada raiz de partida presupuestaria
                    If Not CType(oControl, System.Web.UI.WebControls.HiddenField).ClientID.Contains("hid_Pres5") Then
                        CType(oControl, System.Web.UI.WebControls.HiddenField).Value = String.Empty
                    End If
                Case Else
                    If oControl.Controls IsNot Nothing AndAlso oControl.Controls.Count > 0 Then
                        VaciarControlesRecursivo(oControl)
                    End If
            End Select
        Next
    End Sub
    ''' <summary>
    ''' Cargar en el módulo de busqueda avanzada los datos configurados en el filtro.
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load. Tiempo máximo: 1 sg.</remarks>
    Private Sub CargarBusquedaAvanzada()
        If Session("IDFiltroFacturaUsuario") <> "" Then
            Dim f As Fullstep.FSNServer.FiltroFactura = FSNServer.Get_Object(GetType(FSNServer.FiltroFactura))
            f.IDFiltroUsuario = Session("IDFiltroFacturaUsuario")
            f.Persona = Usuario.CodPersona
            f.Idioma = Usuario.Idioma
            f.LoadFiltro()
            If f.IsLoaded Then
                If Not f.Busqueda Is Nothing AndAlso f.Busqueda.Rows.Count > 0 Then
                    With Busqueda
                        .Filtro = f.IDFiltro

                        .ArticuloHidden = DBNullToStr(f.Busqueda.Rows(0)("ART"))
                        .ArticuloTexto = DBNullToStr(f.Busqueda.Rows(0)("ART_DEN"))
                        '.EmpresaHidden = DBNullToStr(f.Busqueda.Rows(0)("EMPRESA"))
                        '.EmpresaTexto = DBNullToStr(f.Busqueda.Rows(0)("EMP_DEN"))
                        .Estado = DBNullToInteger(f.Busqueda.Rows(0)("ESTADO"))
                        If IsDate(f.Busqueda.Rows(0)("FEC_CONT_DESDE")) Then
                            .FechaContabilizacionDesde = DBNullToSomething(f.Busqueda.Rows(0)("FEC_CONT_DESDE"))
                        Else
                            .FechaContabilizacionDesde = Nothing
                        End If
                        .FechaContabilizacionHasta = DBNullToSomething(f.Busqueda.Rows(0)("FEC_CONT_HASTA"))
                        .FechaFacturaDesde = DBNullToSomething(f.Busqueda.Rows(0)("FEC_FACT_DESDE"))
                        .FechaFacturaHasta = DBNullToSomething(f.Busqueda.Rows(0)("FEC_FACT_HASTA"))
                        .ImporteDesde = DBNullToDbl(f.Busqueda.Rows(0)("IMPORTE_DESDE"))
                        .ImporteHasta = DBNullToDbl(f.Busqueda.Rows(0)("IMPORTE_HASTA"))
                        .NumeroAlbaran = DBNullToSomething(f.Busqueda.Rows(0)("ALBARAN"))
                        .NumeroFacturaSAP = DBNullToSomething(f.Busqueda.Rows(0)("NUM_FACTURA_ERP"))
                        .PedidoERP = DBNullToSomething(f.Busqueda.Rows(0)("NUM_PEDIDO_ERP"))
                        .GestorHidden = DBNullToSomething(f.Busqueda.Rows(0)("GESTOR"))
                        .GestorTexto = DBNullToSomething(f.Busqueda.Rows(0)("GESTOR_DEN"))
                        'dt.Columns.Add("CENTRO_COSTE", System.Type.GetType("System.String"))
                        'dt.Columns.Add("PARTIDA", System.Type.GetType("System.String"))
                        '.ProveedorHidden = DBNullToStr(f.Busqueda.Rows(0)("PROVE"))
                        '.ProveedorTexto = DBNullToStr(f.Busqueda.Rows(0)("PROVE_DEN"))
                        .FacturaOriginal = DBNullToSomething(f.Busqueda.Rows(0)("TIPO_ORIGINAL"))
                        .FacturaRectificativa = DBNullToSomething(f.Busqueda.Rows(0)("TIPO_RECTIFICATIVA"))
                        .AnyoPedido = DBNullToInteger(f.Busqueda.Rows(0)("ANYO_PEDIDO"))
                        .NumCesta = DBNullToInteger(f.Busqueda.Rows(0)("CESTA_PEDIDO"))
                        .NumPedido = DBNullToInteger(f.Busqueda.Rows(0)("NUM_PEDIDO"))
                    End With

                    txtEmpresa.Text = DBNullToStr(f.Busqueda.Rows(0)("EMP_DEN"))
                    hidEmpresa.Value = DBNullToStr(f.Busqueda.Rows(0)("EMPRESA"))
                    txtProveedor.Text = DBNullToStr(f.Busqueda.Rows(0)("PROVE_DEN"))
                    hidProveedor.Value = DBNullToStr(f.Busqueda.Rows(0)("PROVE"))
                End If
            End If
        Else
            Busqueda.FechaFacturaDesde = DevolverFechaDesdeConfigurada()
        End If
    End Sub
#Region "EXPORTAR EXCEL/PDF"

    ''' <summary>
    ''' Exporta a Excel
    ''' </summary>
    ''' <remarks>Llamada desde: Page_load; Tiempo máximo:0,1</remarks>
    Private Sub ExportarExcel()
        Dim fileName As String = HttpUtility.UrlEncode(FSNPageHeader.TituloCabecera)
        fileName = fileName.Replace("+", "_")

        With whdgFacturas
            .GridView.Columns.FromKey("ICONO").Hidden = True
            .GridView.Columns.FromKey("APROBAR").Hidden = True
            .GridView.Columns.FromKey("RECHAZAR").Hidden = True
            .GridView.Columns.FromKey("SEG").Hidden = True
        End With

        With whdgExcelExporter
            .DownloadName = fileName
            .DataExportMode = DataExportMode.AllDataInDataSource

            .Export(whdgFacturas)
        End With

        With whdgFacturas
            '.Columns.FromKey("ICONO").Hidden = False
            '.Columns.FromKey("APROBAR").Hidden = False
            '.Columns.FromKey("RECHAZAR").Hidden = False
            '.Columns.FromKey("SEG").Hidden = False
            '.GridView.Columns.FromKey("INDICADOR_EN_PROCESO").Hidden = True
            '.GridView.Columns.FromKey("IMAGE_PROVE").Hidden = True
            '.GridView.Columns.FromKey("IMAGE_ESTADO").Hidden = True
        End With
    End Sub



    Private Sub whdgExcelExporter_GridRecordItemExported(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.GridRecordItemExportedEventArgs) Handles whdgExcelExporter.GridRecordItemExported

        Select Case e.GridCell.Column.Key
            Case "PAGOS"
                e.WorksheetCell.Value = If(e.GridCell.Value > 0, Textos(43), Textos(44))
            Case "TIENE_DISCREP"
                e.WorksheetCell.Value = If(e.GridCell.Value > 0, Textos(43), Textos(44))
            Case "TIPO"
                e.WorksheetCell.Value = If(e.GridCell.Value = 1, Textos(36), Textos(37))
            Case "IMPORTE"
                e.WorksheetCell.Value = FSNLibrary.FormatNumber(e.GridCell.Row.DataItem.Item("IMPORTE"), FSNUser.NumberFormat) & " " & e.GridCell.Row.DataItem.Item("MON")
                e.WorksheetCell.CellFormat.Alignment = Infragistics.Documents.Excel.HorizontalCellAlignment.Right
            Case Else
                Select Case e.GridCell.Column.Type
                    Case System.Type.GetType("System.DateTime")
                        'e.WorksheetCell.CellFormat.FormatString = FSNUser.DateFormat.ShortDatePattern
                    Case Else
                End Select
        End Select
    End Sub

    ''' <summary>
    ''' Exporta a PDF
    ''' </summary>
    ''' <remarks>Llamada desde: Page_load; Tiempo máximo:0,1</remarks>
    Private Sub ExportarPDF()
        Dim fileName As String = HttpUtility.UrlEncode(FSNPageHeader.TituloCabecera)
        fileName = fileName.Replace("+", "_")
        whdgPDFExporter.DownloadName = fileName

        With whdgFacturas
            .GridView.Columns.FromKey("ICONO").Hidden = True
            .GridView.Columns.FromKey("APROBAR").Hidden = True
            .GridView.Columns.FromKey("RECHAZAR").Hidden = True
            .GridView.Columns.FromKey("SEG").Hidden = True
            .GridView.Columns.FromKey("TIENE_DISCREP").Hidden = True
        End With

        With whdgPDFExporter
            .EnableStylesExport = True
            .DataExportMode = DataExportMode.AllDataInDataSource
            .TargetPaperOrientation = PageOrientation.Landscape

            .Margins = PageMargins.GetPageMargins("Narrow")
            .TargetPaperSize = PageSizes.GetPageSize("A4")
            .Export(whdgFacturas)
        End With
    End Sub
#End Region
    ''' <summary>
    ''' Pone el texto correspondiente en el botóon Aprobar
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: El prerender de la etiqueta. Tiempo máximo: 0sg.</remarks>
    Protected Sub btnAprobar_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, FSNWebControls.FSNButton).Text = Textos(20)
    End Sub
    ''' <summary>
    ''' Pone el texto correspondiente en el botóon Aprobar
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: El prerender de la etiqueta. Tiempo máximo: 0sg.</remarks>
    Protected Sub btnRechazar_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, FSNWebControls.FSNButton).Text = Textos(21)
    End Sub
    ''' <summary>
    ''' Al hacer click sobre el botón de la grid "Rechazar", rechaza las solicitudes chequeadas.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: click del botón "Rechazar". Tiempo máximo: 0 sg.</remarks>
    Protected Sub btnRechazar_Click(ByVal sender As Object, ByVal e As EventArgs)
        ProcesarAcciones("RECHAZAR")
    End Sub
    ''' <summary>
    ''' Al hacer click sobre el botón de la grid "Aprobar", aprueba las solicitudes chequeadas.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: click del botón "Aprobar". Tiempo máximo: 0 sg.</remarks>
    Protected Sub btnAprobar_Click(ByVal sender As Object, ByVal e As EventArgs)
        ProcesarAcciones("APROBAR")
    End Sub
    ''' Revisado por: blp. Fecha: 02/02/2012
    ''' <summary>
    ''' Procesa las solicitudes que se han chequeado. Las pone en proceso y llama al webservice para que las procese.
    ''' Se actualiza el nº de solicitudes pendientes de la zona de alerta.
    ''' </summary>
    ''' <param name="sNombreCampos">sNombreCampos: Operación a realizar (Aprobar/Rechazar)</param>
    ''' <remarks>Llamada desde: btnRechazar_Click y btnAprobar_Click. Tiempo máximo: 0 sg.</remarks>
    Protected Sub ProcesarAcciones(ByVal sNombreCampos As String)
        Dim sInstanciasActualizarEnProceso As String = ""
        Dim sInstanciasValidar As String = ""
        Dim cInstancia As Instancia
        cInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))

        Dim dsXML As New DataSet
        dsXML.Tables.Add("SOLICITUD")
        Dim drSolicitud As DataRow
        With dsXML.Tables("SOLICITUD").Columns
            .Add("TIPO_PROCESAMIENTO_XML")
            .Add("TIPO_DE_SOLICITUD")
            .Add("COMPLETO")
            .Add("CODIGOUSUARIO")
            .Add("INSTANCIA")
            .Add("USUARIO")
            .Add("USUARIO_EMAIL")
            .Add("USUARIO_IDIOMA")
            .Add("IDACCION")
            .Add("COMENTARIO")
            .Add("BLOQUE_ORIGEN")
            .Add("THOUSANFMT")
            .Add("DECIAMLFMT")
            .Add("PRECISIONFMT")
            .Add("DATEFMT")
            .Add("REFCULTURAL")
            .Add("TIPOEMAIL")
            .Add("IDTIEMPOPROC")
        End With

        For fila As Integer = 0 To whdgFacturas.Rows.Count - 1
            If CType(whdgFacturas.Rows.Item(fila).Items(0).FindControl("chk"), CheckBox).Checked Then
                If (sInstanciasActualizarEnProceso <> "") Then
                    sInstanciasActualizarEnProceso = sInstanciasActualizarEnProceso & ","
                End If
                If sInstanciasValidar <> "" Then
                    sInstanciasValidar = sInstanciasValidar & "#"
                End If

                sInstanciasActualizarEnProceso = sInstanciasActualizarEnProceso & whdgFacturas.Rows.Item(fila).Items.FindItemByKey("INSTANCIA").Value
                sInstanciasValidar = sInstanciasValidar & whdgFacturas.Rows.Item(fila).Items.FindItemByKey("INSTANCIA").Value & "|" & whdgFacturas.Rows.Item(fila).Items.FindItemByKey("BLOQUE").Value & "|" & whdgFacturas.Rows.Item(fila).Items.FindItemByKey("ACCION_" & sNombreCampos).Value & "||" & TiposDeDatos.TipoDeSolicitud.Autofactura
            End If
        Next

        If sInstanciasActualizarEnProceso <> "" Then
            Dim cInstancias As Instancias
            cInstancias = FSNServer.Get_Object(GetType(FSNServer.Instancias))
            cInstancias.Actualizar_En_Proceso2(sInstanciasActualizarEnProceso, FSNUser.CodPersona)

            Dim xmlName As String
            Dim lIDTiempoProc As Long
            For Each info As String In Split(sInstanciasValidar, "#")
                cInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, FSNUser.Cod, strToLong(Split(info, "|")(1)))
                drSolicitud = dsXML.Tables("SOLICITUD").NewRow
                With drSolicitud
                    .Item("TIPO_PROCESAMIENTO_XML") = CInt(TipoProcesamientoXML.FSNWeb_PM_AprobacionRechazoMultiple)
                    .Item("TIPO_DE_SOLICITUD") = CInt(TiposDeDatos.TipoDeSolicitud.Autofactura)
                    .Item("COMPLETO") = 1
                    .Item("CODIGOUSUARIO") = FSNUser.Cod
                    .Item("INSTANCIA") = Split(info, "|")(0)
                    .Item("USUARIO") = FSNUser.CodPersona
                    .Item("USUARIO_EMAIL") = FSNUser.Email
                    .Item("USUARIO_IDIOMA") = FSNUser.IdiomaCod
                    .Item("ACCION") = ""
                    .Item("IDACCION") = Split(info, "|")(2)
                    .Item("COMENTARIO") = ""
                    .Item("BLOQUE_ORIGEN") = Split(info, "|")(1)
                    .Item("THOUSANFMT") = FSNUser.ThousanFmt
                    .Item("DECIAMLFMT") = FSNUser.DecimalFmt
                    .Item("PRECISIONFMT") = FSNUser.PrecisionFmt
                    .Item("DATEFMT") = FSNUser.DateFmt
                    .Item("REFCULTURAL") = FSNUser.Idioma.RefCultural
                    .Item("TIPOEMAIL") = FSNUser.TipoEmail
                    .Item("IDTIEMPOPROC") = lIDTiempoProc
                End With
                xmlName = FSNUser.Cod & "#" & Split(info, "|")(0) & "#" & Split(info, "|")(1)
                dsXML.Tables("SOLICITUD").Rows.Add(drSolicitud)

                cInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, iFecha:=TiempoProcesamiento.InicioDisco)
                If ConfigurationManager.AppSettings("SERVIDOR_TRATAMIENTO_INSTANCIAS") = "0" Then
                    'Se hace con un StringWriter porque el método GetXml del dataset no incluye el esquema
                    Dim oSW As New System.IO.StringWriter()
                    dsXML.WriteXml(oSW, XmlWriteMode.WriteSchema)

                    Dim oSrvTrataInst As New FSNWebServiceXML.TratamientoInstancias
                    oSrvTrataInst.TransferirXMLEnEpera(oSW.ToString(), xmlName)
                Else
                    dsXML.WriteXml(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & "#FSNWEB.xml", XmlWriteMode.WriteSchema)
                    If IO.File.Exists(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml") Then _
                        IO.File.Delete(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml")
                    FileSystem.Rename(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & "#FSNWEB.xml", _
                                      ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml")
                End If
            Next
            cInstancias = Nothing

            CargarNumeroFacturasPendientes()
            upAlerta.Update()
        End If
    End Sub
    <System.Web.Services.WebMethodAttribute(), _
   System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function DevolverTexto(ByVal contextKey As String) As String
        Return contextKey
    End Function
    Private Sub whdgPDFExporter_GridRecordItemExporting(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.DocumentGridRecordItemExportingEventArgs) Handles whdgPDFExporter.GridRecordItemExporting
        Select Case e.GridCell.Column.Key
            Case "SITUACION_ACTUAL", "DEN_ESTADO"
                e.ExportValue = IIf(Not IsDBNull(e.GridCell.Value), e.GridCell.Value.ToString & " ", "")
        End Select
    End Sub
    Private Sub whdgExcelExporter_RowExported(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.ExcelRowExportedEventArgs) Handles whdgExcelExporter.RowExported
        Dim objDict As Hashtable = HttpContext.Current.Session("Hast_Seg_" & FSNUser.Cod)

        If Not e.GridRow Is Nothing Then
            If Not objDict Is Nothing AndAlso Not objDict.Item(CShort(e.GridRow.Index)) Is Nothing Then
                If objDict.Item(CShort(e.GridRow.Index)) = 1 Then
                    e.Worksheet.Rows(e.CurrentRowIndex).CellFormat.Font.Color = Drawing.Color.Red
                End If
            Else
                If e.GridRow.Items.FindItemByKey("SEG").Value = 1 Then
                    e.Worksheet.Rows(e.CurrentRowIndex).CellFormat.Font.Color = Drawing.Color.Red
                End If
            End If

        End If

    End Sub
    ''' <summary>
    ''' Procedimiento que graba la monitorización o no de una instancia
    ''' </summary>
    ''' <param name="lID">Identificador del contacto a modificar</param>
    ''' <param name="Proveedor">Proveedor</param>
    ''' <returns>Identificador del contacto a modificar</returns>
    ''' <remarks>Llamada desde: 2008/puntuacion/Contactos/wdgDatos_CellSelectionChanged; Tiempo maximo: 0,1 seg</remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Sub Monitorizar(ByVal lInstancia As Long, ByVal iSeg As Short, ByVal Fila As Short)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim objDict As Hashtable = HttpContext.Current.Session("Hast_Seg_" & FSNUser.Cod)
        If objDict Is Nothing Then objDict = New Hashtable


        objDict.Item(Fila) = iSeg
        HttpContext.Current.Session("Hast_Seg_" & FSNUser.Cod) = objDict
        Dim cInstancia As FSNServer.Instancia

        cInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        cInstancia.ID = lInstancia
        cInstancia.ActualizarMonitorizacion(iSeg, FSNUser.CodPersona)
    End Sub
End Class