﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VisorFacturas

    '''<summary>
    '''whdgExcelExporter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents whdgExcelExporter As Global.Infragistics.Web.UI.GridControls.WebExcelExporter

    '''<summary>
    '''whdgPDFExporter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents whdgPDFExporter As Global.Infragistics.Web.UI.GridControls.WebDocumentExporter

    '''<summary>
    '''FSNPageHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FSNPageHeader As Global.Fullstep.FSNWebControls.FSNPageHeader

    '''<summary>
    '''CeldaBusquedaGeneral control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CeldaBusquedaGeneral As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''upBusquedaGeneral control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upBusquedaGeneral As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''lblProveedor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblProveedor As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtProveedor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtProveedor As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtProveedor_AutoCompleteExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtProveedor_AutoCompleteExtender As Global.AjaxControlToolkit.AutoCompleteExtender

    '''<summary>
    '''imgProveedorLupa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgProveedorLupa As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''hidProveedor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hidProveedor As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''lblEmpresa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEmpresa As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtEmpresa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEmpresa As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtEmpresa_AutoCompleteExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEmpresa_AutoCompleteExtender As Global.AjaxControlToolkit.AutoCompleteExtender

    '''<summary>
    '''imgEmpresaLupa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgEmpresaLupa As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''hidEmpresa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hidEmpresa As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''lblNumeroFactura control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNumeroFactura As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtNumeroFactura control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtNumeroFactura As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnBuscar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnBuscar As Global.Fullstep.FSNWebControls.FSNButton

    '''<summary>
    '''upAlerta control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upAlerta As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''phAlerta control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phAlerta As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''imgAlerta control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgAlerta As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''lblFacturasPendientes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFacturasPendientes As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlCabeceraParametros control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlCabeceraParametros As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''pnlBusquedaAvanzada control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlBusquedaAvanzada As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''imgExpandir control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgExpandir As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''lblBusquedaAvanzada control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBusquedaAvanzada As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlParametros control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlParametros As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''upBusquedaAvanzada control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upBusquedaAvanzada As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Busqueda control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Busqueda As Global.FSNUserControls.wucBusquedaAvanzadaFacturas

    '''<summary>
    '''btnBuscarBusquedaAvanzada control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnBuscarBusquedaAvanzada As Global.Fullstep.FSNWebControls.FSNButton

    '''<summary>
    '''btnLimpiarBusquedaAvanzada control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnLimpiarBusquedaAvanzada As Global.Fullstep.FSNWebControls.FSNButton

    '''<summary>
    '''UpdatePanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''dlFiltrosConfigurados control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dlFiltrosConfigurados As Global.System.Web.UI.WebControls.DataList

    '''<summary>
    '''btnAnyadirFiltros control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAnyadirFiltros As Global.Fullstep.FSNWebControls.FSNButton

    '''<summary>
    '''odsFiltrosConfigurados control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents odsFiltrosConfigurados As Global.System.Web.UI.WebControls.ObjectDataSource

    '''<summary>
    '''whdgFacturas control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents whdgFacturas As Global.Infragistics.Web.UI.GridControls.WebHierarchicalDataGrid

    '''<summary>
    '''ModalProgress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ModalProgress As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''panelUpdateProgress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panelUpdateProgress As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ImgProgress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImgProgress As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''LblProcesando control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblProcesando As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''btnPagos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnPagos As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''mpePagos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mpePagos As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''panPagos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panPagos As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''imgPagos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgPagos As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''lblLitTituloPagos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLitTituloPagos As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTituloPagos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTituloPagos As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''imgCerrarPagos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgCerrarPagos As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''FSNPanelMotivoAnulacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FSNPanelMotivoAnulacion As Global.Fullstep.FSNWebControls.FSNPanelInfo

    '''<summary>
    '''FSNPanelEnProceso control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FSNPanelEnProceso As Global.Fullstep.FSNWebControls.FSNPanelInfo
End Class
