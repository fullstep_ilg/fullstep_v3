﻿Imports System.Security.Principal
Imports System.IO
Public Class EFactura
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ObtenerEFactura(Request("id"), If(Request("tipo") = "PDF", True, False), Request("Num"))
  End Sub

    ''' <summary>
    ''' Obtiene la efactura de base de datos y la muestra. Si hay que crear PDF hay que convertir el XML a PDF y luego mostrarlo.
    ''' </summary>
    ''' <param name="idEFactura">Id de la efactura</param>
    ''' <param name="bCrearPDF">Si hay que crear o no PDF. Si no, se muestra el XML tal cual está guardado en BBDD.</param>
    ''' <remarks>Llamada desde: Page_Load();</remarks>
    Private Sub ObtenerEFactura(ByVal idEFactura As Long, ByVal bCrearPDF As Boolean, ByVal sNumeroFactura As String)
        Dim impersonationContext As WindowsImpersonationContext
        Dim oFactura As FSNServer.Factura
        oFactura = FSNServer.Get_Object(GetType(FSNServer.Factura))

        FSNServer.Impersonate()

        impersonationContext = RealizarSuplantacion(FSNServer.ImpersonateLogin, FSNServer.ImpersonatePassword, FSNServer.ImpersonateDominio)

        Dim buffer As Byte()
        Dim ficheroXML As String
        Dim sPath As String

        sPath = ConfigurationManager.AppSettings("temp") & "\" & modUtilidades.GenerateRandomPath() & "\"
        If Not Directory.Exists(sPath) Then Directory.CreateDirectory(sPath)
        ficheroXML = sPath & sNumeroFactura & ".xml"

        buffer = oFactura.LeerEFactura(idEFactura)

        Dim oFileStream As System.IO.FileStream = Nothing
        oFileStream = New System.IO.FileStream(ficheroXML, System.IO.FileMode.Create)
        oFileStream.Write(buffer, 0, buffer.Length)
        oFileStream.Close()

        If Not impersonationContext Is Nothing Then
            impersonationContext.Undo()
        End If

        If bCrearPDF Then
            'Convertimos el fichero de XML a PDF
            Dim ficheroPDF As String = sPath & sNumeroFactura & ".pdf"
            Dim oCFacturaEPDF As FSNServer.CFacturaePDF = FSNServer.Get_Object(GetType(FSNServer.CFacturaePDF))
            oCFacturaEPDF.crearPDFFacV3_2(ficheroXML, ficheroPDF, Idioma)
            Response.ContentType = "application/pdf"
            Response.AppendHeader("Content-Disposition", "attachment; filename=""" & sNumeroFactura & ".pdf" & """")
            Response.WriteFile(ficheroPDF)
            Response.End()
        Else
            Response.ContentType = "application/xml"
            Response.AppendHeader("Content-Disposition", "attachment; filename=""" & sNumeroFactura & ".xml" & """")
            Response.WriteFile(ficheroXML)
            Response.End()
        End If
    End Sub
End Class