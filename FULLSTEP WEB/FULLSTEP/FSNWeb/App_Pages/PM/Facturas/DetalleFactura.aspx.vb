﻿Imports System.IO
Imports System.Threading

Public Class DetalleFactura
    Inherits FSNPage

#Region "PROPIEDADES"

    Private _oFactura As FSNServer.Factura
    ''' <summary>
    ''' Propiedad que carga los datos de la factura
    ''' </summary>
    Protected ReadOnly Property oFactura() As FSNServer.Factura
        Get
            If _oFactura Is Nothing Then
                If Me.IsPostBack Then
                    _oFactura = CType(Cache("oFactura" & FSNUser.Cod), FSNServer.Factura)
                Else
                    _oFactura = FSNServer.Get_Object(GetType(FSNServer.Factura))
                    _oFactura.ID = Request("ID")
                    _oFactura.EsGestor = (Request("gestor") = "1")
                    _oFactura.EsReceptor = (Request("receptor") = "1")
                    _oFactura.Load(Idioma, If(Request("gestor") = "1" OrElse Request("receptor") = "1", FSNUser.CodPersona, Nothing))
                    Me.InsertarEnCache("oFactura" & FSNUser.Cod, _oFactura)
                    Me.InsertarEnCache("dsFactura" & FSNUser.Cod, _oFactura.DatosFactura)
                End If
            End If
            Return _oFactura
        End Get
    End Property
    Private _oInstancia As FSNServer.Instancia
    ''' <summary>
    ''' Propiedad que carga los datos de la instancia
    ''' </summary>
    Protected ReadOnly Property oInstancia() As FSNServer.Instancia
        Get
            If _oInstancia Is Nothing Then
                If Me.IsPostBack Then
                    _oInstancia = CType(Cache("oInstancia" & FSNUser.Cod), FSNServer.Instancia)
                Else
                    _oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                    _oInstancia.ID = oFactura.Instancia
                    _oInstancia.Load(Idioma, True)
                    If Not _oInstancia.Solicitud Is Nothing Then
                        _oInstancia.Solicitud.Load(Idioma)
                    End If
                    _oInstancia.EsObservador = _oInstancia.ComprobarEsObservador(FSNUser.CodPersona)
                    If _oInstancia.EsObservador Then
                        _oInstancia.DetalleEditable = False
                    Else
                        _oInstancia.CargarCumplimentacionFactura(FSNUser.CodPersona, , _oFactura.EsGestor, _oFactura.EsReceptor)
                    End If

                    If Not _oInstancia Is Nothing Then Me.InsertarEnCache("oInstancia" & FSNUser.Cod, _oInstancia)
                End If
            End If
            Return _oInstancia
        End Get
    End Property
#End Region
    Private m_desdeGS As Boolean = False
    Private m_desdePM As Boolean = False 'Si se llama desde el campo Factura(Gs=138) de una solicitud de PM
    ''' <summary>
    ''' Evento preinit en el que configuramos la apariencia de algunos controles
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Not String.IsNullOrEmpty(Session("desdeGS")) Then
            m_desdeGS = True
            Me.MasterPageFile = "~/App_Master/EnBlanco.Master"
        End If
        If Request("DesdePM") = 1 Then
            m_desdePM = True
            Me.MasterPageFile = "~/App_Master/EnBlanco.Master"
        End If

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsPostBack Then
            Select Case Request("__EVENTTARGET")
                Case "EjecutarAccion"
                    ModuloIdioma = TiposDeDatos.ModulosIdiomas.DetalleFactura
                    ConfigurarCabeceraMenu()
                    GenerarScripts()
                    RealizarAccion(CLng(Request("__EVENTARGUMENT")))
            End Select
        End If

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.DetalleFactura
        If Not IsPostBack Then
            ConfigurarCabeceraMenu()
            GenerarScripts()

            DatosGenerales.Proveedor = oFactura.CodProve & " - " & oFactura.DenProve
            DatosGenerales.CodProveedor = oFactura.CodProve
            DatosGenerales.Empresa = oFactura.Empresa
            DatosGenerales.IdEmpresa = oFactura.IdEmpresa
            DatosGenerales.Importe = oFactura.Importe
            DatosGenerales.ImporteBruto = oFactura.ImporteBruto
            DatosGenerales.SumaCostesLineas = CalculoSumaCostesLinea()
            DatosGenerales.SumaDescuentosLineas = CalculoSumaDescuentosLinea()
            DatosGenerales.NumeroFactura = oFactura.NumeroFactura
            DatosGenerales.FechaFactura = oFactura.FechaFactura
            DatosGenerales.RetencionGarantia = oFactura.TotalRetencionGarantia
            DatosGenerales.NumeroERP = oFactura.NumeroERP
            DatosGenerales.FechaContabilizacion = oFactura.FechaContabilizacion
            DatosGenerales.Costes = oFactura.TotalCostes
            DatosGenerales.CodFormaPago = oFactura.CodFormaPago
            DatosGenerales.FormaPago = oFactura.FormaPago
            DatosGenerales.SituacionActual = If(oFactura.Estado = 0, Textos(14), If(oInstancia.DenEtapaActual = Nothing, oInstancia.CargarEstapaActual(Idioma), oInstancia.DenEtapaActual))
            DatosGenerales.Descuentos = oFactura.TotalDescuentos
            DatosGenerales.CodViaPago = oFactura.CodViaPago
            DatosGenerales.ViaPago = oFactura.ViaPago
            DatosGenerales.Observaciones = oFactura.Observaciones
            DatosGenerales.DataSourceCostes = oFactura.Costes
            DatosGenerales.DataSourceDescuentos = oFactura.Descuentos
            DatosGenerales.DataSourceRetencionGarantia = oFactura.RetencionGarantia
            DatosGenerales.Impuestos = oFactura.ImpuestosCabecera
            DatosGenerales.Instancia = oFactura.Instancia
            If m_desdePM Then
                DatosGenerales.Editable = False
            Else
                DatosGenerales.Editable = oInstancia.DetalleEditable
            End If


            'DatosGenerales.FechaInicioOriginal = oFactura.FechaInicioOriginal
            'DatosGenerales.FechaFinOriginal = oFactura.FechaFinOriginal
            'DatosGenerales.NumeroFacturaOriginal = oFactura.NumeroFacturaOriginal

            If DatosGenerales.Editable Then
                DatosGenerales.DataSourceFormasPago = DevolverFormasPago()
                DatosGenerales.DataSourceViasPago = DevolverViasPago()

                Dim oFacturas As FSNServer.Facturas
                oFacturas = FSNServer.Get_Object(GetType(FSNServer.Facturas))
                DatosGenerales.CostesFacturasProveedor = oFacturas.LoadCostesFacturas(oFactura.CodProve)
                DatosGenerales.DescuentosFacturasProveedor = oFacturas.LoadDescuentosFacturas(oFactura.CodProve)
                Lineas.CostesUtilizadosBuscador = oFacturas.LoadCostesLineaFacturas(oFactura.CodProve)
                Lineas.DescuentosUtilizadosBuscador = oFacturas.LoadDescuentosLineaFacturas(oFactura.CodProve)

                Lineas.Paises = CargarPaises()
                Lineas.CodPaisProve = oFactura.CodPaisProve
                If oFactura.CodPaisProve <> String.Empty Then
                    CargarProvincias(oFactura.CodPaisProve)
                End If

                DatosGenerales.RutaBuscadorArticulo = ConfigurationManager.AppSettings("rutaFS") & "_Common/BuscadorArticulos.aspx?desde=Visor"
                DatosGenerales.PropiedadesBuscadorArticulo = "width=850,height=660,status=yes,resizable=no,top=50,left=200"
                DatosGenerales.RutaBuscadorMaterial = ConfigurationManager.AppSettings("rutaPM") & "_common/materiales.aspx?desde=WebPart"

                Lineas.RutaBuscadorArticulo = ConfigurationManager.AppSettings("rutaFS") & "_Common/BuscadorArticulos.aspx"
                Lineas.PropiedadesBuscadorArticulo = "width=850,height=660,status=yes,resizable=no,top=50,left=200"
                Lineas.RutaBuscadorMaterial = ConfigurationManager.AppSettings("rutaPM") & "_common/materiales.aspx?desde=WebPart"
            End If

            DatosGenerales.LiteralNumeroERP = FSNServer.TipoAcceso.nomPedERP(Idioma)
            DatosGenerales.PathServicio = ConfigurationManager.AppSettings("rutaFS") & "_Common/App_Services/Consultas.asmx"

            Lineas.PathServicio = ConfigurationManager.AppSettings("rutaFS") & "_Common/App_Services/Consultas.asmx"
            Lineas.ImpuestosRepercutidos = oFactura.ImpuestosRepercutidos
            Lineas.ImpuestosRetenidos = oFactura.ImpuestosRetenidos
            Lineas.ImpuestosFactura = oFactura.ImpuestosFactura
            Lineas.PartidasDataTable = FSNWeb.modFacturas.DevolverPartidas(Idioma)
        Else
            Dim dtDescuentosGenerales As DataTable = CType(HttpContext.Current.Session("dsXMLFactura").Tables("DESCUENTOS"), DataTable)
            Dim dtCostesGenerales As DataTable = CType(HttpContext.Current.Session("dsXMLFactura").Tables("COSTES"), DataTable)
            If m_desdePM Then
                DatosGenerales.Editable = False
            Else
                DatosGenerales.Editable = oInstancia.DetalleEditable
            End If
            DatosGenerales.DataSourceCostes = dtCostesGenerales
            DatosGenerales.DataSourceDescuentos = dtDescuentosGenerales
            DatosGenerales.SumaCostesLineas = CalculoSumaCostesLinea()
            DatosGenerales.SumaDescuentosLineas = CalculoSumaDescuentosLinea()
            If CType(HttpContext.Current.Session("dsXMLFactura").Tables("FACTURA"), DataTable).Rows.Count > 0 Then
                DatosGenerales.Costes = CType(HttpContext.Current.Session("dsXMLFactura").Tables("FACTURA"), DataTable).Rows(0).Item("TOT_COSTES")
                DatosGenerales.Descuentos = CType(HttpContext.Current.Session("dsXMLFactura").Tables("FACTURA"), DataTable).Rows(0).Item("TOT_DESCUENTOS")
                DatosGenerales.Importe = CType(HttpContext.Current.Session("dsXMLFactura").Tables("FACTURA"), DataTable).Rows(0).Item("IMPORTE")
                DatosGenerales.ImporteBruto = CType(HttpContext.Current.Session("dsXMLFactura").Tables("FACTURA"), DataTable).Rows(0).Item("IMPORTE_BRUTO")
            End If
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "LimiteMensajesCargados") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "LimiteMensajesCargados", "<script>var LimiteMensajesCargados =" & System.Configuration.ConfigurationManager.AppSettings("LimiteMensajesCargados") & "</script>")
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosDiscrepancia") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosDiscrepancia", "<script>var TextosDiscrepancia = new Array(); TextosDiscrepancia[1]= '" & JSText(Textos(33)) & "'</script>")
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "NumFactura") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NumFactura", "<script>var NumFactura ='" & oFactura.NumeroFactura & "';</script>")
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "CosteGenerico") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "CosteGenerico", "<script>var CosteGenerico ='" & oFactura.IdCosteGenerico & "';</script>")
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "DescuentoGenerico") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "DescuentoGenerico", "<script>var DescuentoGenerico ='" & oFactura.IdDescuentoGenerico & "';</script>")
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextoTolerancia") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextoTolerancia", "<script>var TextoTolerancia = '" & JSText(Textos(36)) & "';</script>")
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "ToleranciaImporte") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ToleranciaImporte", "<script>var ToleranciaImporte = " & oFactura.ToleranciaImporte & ";</script>")
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "ToleranciaPorcentaje") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ToleranciaPorcentaje", "<script>var ToleranciaPorcentaje =" & oFactura.ToleranciaPorcentaje & ";</script>")
        End If

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.DatosGeneralesFactura
        DatosGenerales.Textos = TextosModuloCompleto
        DatosGenerales.RutaImagenes = ConfigurationManager.AppSettings("ruta") & "images/"
        DatosGenerales.Moneda = oFactura.Moneda
        DatosGenerales.IdCosteGenerico = oFactura.IdCosteGenerico
        DatosGenerales.IdDescuentoGenerico = oFactura.IdDescuentoGenerico
        DatosGenerales.NumberFormat = FSNUser.NumberFormat
        DatosGenerales.DateFormat = FSNUser.DateFormat
        DatosGenerales.SufijoCache = FSNUser.Cod

        Lineas.AccesoSM = Me.Acceso.gbAccesoFSSM
        Lineas.Textos = TextosModuloCompleto
        Lineas.RutaImagenes = ConfigurationManager.AppSettings("ruta") & "images/"
        If m_desdePM Then
            Lineas.Editable = False
        Else
            Lineas.Editable = oInstancia.DetalleEditable
        End If
        Lineas.CostesPorLinea = Acceso.gbCostesPorLinea
        Lineas.Moneda = oFactura.Moneda
        Lineas.DataSource = oFactura.Lineas
        Lineas.IdFactura = oFactura.ID
        Lineas.NumFactura = oFactura.NumeroFactura
        Lineas.CodProve = oFactura.CodProve
        Lineas.IdCosteGenerico = oFactura.IdCosteGenerico
        Lineas.IdDescuentoGenerico = oFactura.IdDescuentoGenerico
        Lineas.SufijoCache = FSNUser.Cod
        Lineas.NumberFormat = FSNUser.NumberFormat
        Lineas.DateFormat = FSNUser.DateFormat
        Lineas.MostrarLeyendaOtrosGestores = (Request("gestor") = "1") AndAlso oFactura.HayOtrosGestores

        HistoricoNotificaciones.Textos = TextosModuloCompleto
        HistoricoNotificaciones.RutaImagenes = ConfigurationManager.AppSettings("ruta") & "images/"
        If Not IsPostBack Then HistoricoNotificaciones.DataSource = oFactura.Load_Historico_Notificaciones(If(Not oInstancia.EsObservador, FSNUser.Cod, Nothing))

        If Not IsPostBack Then
            GenerarEstructuraDataset()
            Dim cFacturas As New Facturas
            cFacturas.ActualizarCostesDescuentosGenerales(oFactura.ID, oFactura.Importe, oFactura.TotalCostes, oFactura.TotalDescuentos, oFactura.ImporteBruto)
        End If
    End Sub
    ''' <summary>
    ''' Proceso que carga la propiedad del usercontrol con los paises
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,1seg.</remarks>
    Private Function CargarPaises()
        Dim oPaises As FSNServer.Paises
        oPaises = FSNServer.Get_Object(GetType(FSNServer.Paises))
        oPaises.LoadData(Idioma)

        CargarPaises = oPaises.Data.Tables(0)

        oPaises = Nothing
    End Function
    ''' <summary>
    ''' Carga la propiedad del usercontrol con las provincias de un determinado pais
    ''' </summary>
    ''' <param name="sCodPais">Codigo del pais</param>
    ''' <remarks>Llamada desde=wddSubtipo_ItemsRequested; Tiempo máximo=0seg.</remarks>
    Private Sub CargarProvincias(ByVal sCodPais As String) Handles Lineas.eventProvinciasItemRequested
        Dim oProvincias As FSNServer.Provincias
        oProvincias = FSNServer.Get_Object(GetType(FSNServer.Provincias))

        oProvincias.Pais = sCodPais
        oProvincias.LoadData(Idioma)

        Lineas.Provincias = oProvincias.Data.Tables(0)
        Lineas.CargarComboProvinciasImpuestos()

        oProvincias = Nothing
    End Sub
    ''' <summary>
    ''' Procedimiento que recalcula los diferentes importes en el resumen de la cabecera
    ''' </summary>
    ''' <param name="Tipo">Tipo de actualizacion:impuestos, costes o descuentos</param>
    ''' <param name="ImporteNetoLineasConCD">Importe Neto de las lineas con costes y descuentos incluidos, sin impuestos</param>
    ''' <remarks></remarks>
    Private Sub RecalcularImportesEnCabecera(ByVal Tipo As Byte, ByVal ImporteNetoLineasConCD As Double, ByVal dtImpuestos As DataTable, ByVal ImporteNetoLineasConCDYImpuestos As Double) Handles Lineas.EventoRecalcularImportes
        DatosGenerales.RecalcularImportesCabecera(Tipo, ImporteNetoLineasConCD, dtImpuestos, ImporteNetoLineasConCDYImpuestos)
    End Sub
    ''' <summary>
    ''' Carga la propiedad del usercontrol con los impuestos del buscador
    ''' </summary>
    ''' <param name="sCod">codigo del impuesto</param>
    ''' <param name="sDesc">descripcion del impuesto</param>
    ''' <param name="sPais">Pais</param>
    ''' <param name="sProv">provincia</param>
    ''' <param name="sCodArt">codigo de articulo</param>
    ''' <param name="sCodMat">codigo de material</param>
    ''' <param name="bRepercutido">Si hay que devolver los impuestos repercutidos</param>
    ''' <param name="bRetenido">Si hay que devolver los impuestos retenidos</param>
    ''' <param name="iConceptoImpuesto">Concepto del impuesto</param>
    ''' <remarks></remarks>
    Private Sub CargarImpuestosBuscador(ByVal sCod As String, ByVal sDesc As String, ByVal sPais As String, ByVal sProv As String, ByVal sCodArt As String, ByVal sCodMat As String, ByVal bRepercutido As Boolean, ByVal bRetenido As Boolean, ByVal iConceptoImpuesto As Nullable(Of TiposDeDatos.ConceptoImpuesto)) Handles Lineas.EventoBuscarImpuestos
        Dim oFacturas As FSNServer.Facturas
        oFacturas = FSNServer.Get_Object(GetType(FSNServer.Facturas))
        Lineas.ImpuestosBuscador = oFacturas.LoadImpuestosFacturas(Idioma, sCod, sDesc, sPais, sProv, sCodArt, sCodMat, bRepercutido, bRetenido, iConceptoImpuesto)
    End Sub
    ''' Revisado por: sra. Fecha: 24/01/2012
    ''' <summary>
    ''' Configura la cabecera, muestra en el menu las posibles acciones
    ''' Activamos los botones que pueden aparecer en la pantalla
    ''' </summary>
    ''' <remarks>Llamada desde:=Page_load; Tiempo máximo:=0,1seg.</remarks>
    Private Sub ConfigurarCabeceraMenu()
        FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/Factura.png"
        FSNPageHeader.TituloCabecera = If(oFactura.Tipo = 1, Textos(11), Textos(12)) & " (" & oFactura.EstadoDen & ")"

        If Not m_desdeGS Then FSNPageHeader.VisibleBotonVolver = True
        If m_desdePM Then
            FSNPageHeader.Visible = False
            Exit Sub
        End If

        FSNPageHeader.VisibleBotonMail = True

        FSNPageHeader.TextoBotonVolver = Textos(0)
        FSNPageHeader.TextoBotonMail = Textos(1)

        FSNPageHeader.OnClientClickVolver = "return Volver()"
        FSNPageHeader.OnClientClickMail = "return EnviarMail()"

        If oFactura.IdEFactura > 0 Then
            FSNPageHeader.VisibleBotonEFacturaPDF = True
            FSNPageHeader.VisibleBotonEFacturaXML = True
            FSNPageHeader.TextoBotonEFacturaPDF = Textos(9)
            FSNPageHeader.TextoBotonEFacturaXML = Textos(10)
            FSNPageHeader.OnClientClickEFacturaPDF = "return AbrirEFactura('PDF'," & oFactura.IdEFactura & ", '" & oFactura.NumeroFactura & "')"
            FSNPageHeader.OnClientClickEFacturaXML = "return AbrirEFactura('XML'," & oFactura.IdEFactura & ", '" & oFactura.NumeroFactura & "')"
        End If

        If oInstancia.EsObservador Then
            Exit Sub
        End If

        FSNPageHeader.TextoBotonEliminar = Textos(3)
        FSNPageHeader.TextoBotonGuardar = Textos(4)
        FSNPageHeader.TextoBotonAprobar = Textos(5)
        FSNPageHeader.TextoBotonRechazar = Textos(6)
        FSNPageHeader.TextoBotonAccion1 = Textos(7) 'Otras acciones

        FSNPageHeader.OnClientClickEliminar = "return EliminarInstancia()"
        FSNPageHeader.OnClientClickGuardar = "return Guardar()"


        Select Case oInstancia.Estado
            Case TipoEstadoSolic.Guardada   'Guardada
                FSNPageHeader.VisibleBotonEliminar = True
                FSNPageHeader.VisibleBotonGuardar = True
                FSNPageHeader.VisibleBotonAccion1 = True
            Case TipoEstadoSolic.Rechazada  'Rechazada    
                FSNPageHeader.VisibleBotonAccion1 = True
                FSNPageHeader.VisibleBotonEliminar = True
        End Select

        If oFactura.Estado = TipoEstadoSolic.Guardada Then
            'Si se esta en la etapa borrador pero se ha guardado la factura, el estado de la instancia sera 2(en curso) en vez de 0, hay que mirarlo por el estado
            'de la factura.
            FSNPageHeader.VisibleBotonEliminar = True
        End If

        ''''Carga las acciones posibles en el botón de Otras acciones
        'If oInstancia.Estado = TipoEstadoSolic.Guardada Or oInstancia.Estado = TipoEstadoSolic.Rechazada Then
        Dim oRol As FSNServer.Rol
        Dim oPopMenu As Infragistics.Web.UI.NavigationControls.WebDataMenu
        Dim oItem As Infragistics.Web.UI.NavigationControls.DataMenuItem

        oInstancia.DevolverEtapaActual(Idioma, FSNUser.CodPersona, , _oFactura.EsGestor, _oFactura.EsReceptor)

        'Botón  trasladar
        If FSNUser.PMPermitirTraslados AndAlso oInstancia.PermitirTraslados Then 'A oInstancia.PermitirTraslados se asigna valor en el método DevolverEtapaActual
            FSNPageHeader.TextoBotonTrasladar = Textos(13)
            FSNPageHeader.OnClientClickTrasladar = "return Trasladar(); return false;"
            FSNPageHeader.VisibleBotonTrasladar = True
        End If

        If oInstancia.RolActual = 0 Or oInstancia.Etapa = 0 Then
            FSNPageHeader.VisibleBotonAccion1 = False
            FSNPageHeader.VisibleBotonGuardar = False
        Else
            oRol = FSNServer.Get_Object(GetType(FSNServer.Rol))
            oRol.Id = oInstancia.RolActual
            oRol.Bloque = oInstancia.Etapa

            Dim oDSAcciones As DataSet
            If oInstancia.Estado = TipoEstadoSolic.Rechazada = True Then
                oDSAcciones = oRol.CargarAcciones(Idioma, True)
            Else
                oDSAcciones = oRol.CargarAcciones(Idioma)
            End If

            oPopMenu = Me.wdmAcciones
            oPopMenu.Visible = False
            oPopMenu.Items.Clear()

            Dim blnAprobar As Boolean = False
            Dim blnRechazar As Boolean = False
            Dim iAccionesRestarOtrasAcciones As Integer
            Dim iOtrasAcciones As Integer
            If oDSAcciones.Tables.Count > 0 Then
                If oDSAcciones.Tables(0).Rows.Count > 1 Then
                    oPopMenu.Visible = True
                    'Comprobamos si entre las acciones hay de tipo aprobar y rechazar
                    For Each oRow As DataRow In oDSAcciones.Tables(0).Rows
                        If DBNullToSomething(oRow.Item("RA_APROBAR")) = 1 Then
                            blnAprobar = True
                            iAccionesRestarOtrasAcciones = iAccionesRestarOtrasAcciones + 1
                            If blnRechazar Then Exit For
                        ElseIf DBNullToSomething(oRow.Item("RA_RECHAZAR")) = 1 Then
                            blnRechazar = True
                            iAccionesRestarOtrasAcciones = iAccionesRestarOtrasAcciones + 1
                            If blnAprobar Then Exit For
                        End If
                    Next
                    iOtrasAcciones = oDSAcciones.Tables(0).Rows.Count - iAccionesRestarOtrasAcciones
                    Dim cont As Integer = 0
                    For Each oRow In oDSAcciones.Tables(0).Rows
                        If iOtrasAcciones > 3 AndAlso DBNullToSomething(oRow.Item("RA_APROBAR")) <> 1 AndAlso DBNullToSomething(oRow.Item("RA_RECHAZAR")) <> 1 Then
                            If IsDBNull(oRow.Item("DEN")) Then
                                oItem = oPopMenu.Items.Add("&nbsp;")
                            Else
                                If oRow.Item("DEN") = "" Then
                                    oItem = oPopMenu.Items.Add("&nbsp;")
                                Else
                                    oItem = oPopMenu.Items.Add(DBNullToStr(oRow.Item("DEN")))
                                End If
                            End If
                            oItem.NavigateUrl = "javascript:EjecutarAccion(" + oRow.Item("ACCION").ToString() + ")"
                        ElseIf DBNullToSomething(oRow.Item("RA_APROBAR")) = 1 Then
                            FSNPageHeader.OnClientClickAprobar = "return EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + ")"
                        ElseIf DBNullToSomething(oRow.Item("RA_RECHAZAR")) = 1 Then
                            FSNPageHeader.OnClientClickRechazar = "return EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + ")"
                        Else
                            If cont = 0 Then
                                FSNPageHeader.OnClientClickAccion1 = "return EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + ")"
                                FSNPageHeader.TextoBotonAccion1 = DBNullToStr(oRow.Item("DEN"))
                                FSNPageHeader.VisibleBotonAccion1 = True
                                cont = 1
                            ElseIf cont = 1 Then
                                FSNPageHeader.OnClientClickAccion2 = "return EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + ")"
                                FSNPageHeader.TextoBotonAccion2 = DBNullToStr(oRow.Item("DEN"))
                                FSNPageHeader.VisibleBotonAccion2 = True
                                cont = 2
                            Else
                                FSNPageHeader.OnClientClickAccion3 = "return EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + ")"
                                FSNPageHeader.TextoBotonAccion3 = DBNullToStr(oRow.Item("DEN"))
                                FSNPageHeader.VisibleBotonAccion3 = True
                            End If
                        End If
                    Next
                    If blnAprobar Then
                        FSNPageHeader.VisibleBotonAprobar = True
                    End If
                    If blnRechazar Then
                        FSNPageHeader.VisibleBotonRechazar = True
                    End If
                    If iOtrasAcciones > 3 Then
                        FSNPageHeader.VisibleBotonAccion1 = True
                        FSNPageHeader.OnClientClickAccion1 = "MostrarMenuAcciones(this); return false;"
                    End If
                Else
                    If oDSAcciones.Tables(0).Rows.Count = 0 Then
                        FSNPageHeader.VisibleBotonAccion1 = False
                    Else
                        Dim oRow As DataRow
                        oRow = oDSAcciones.Tables(0).Rows(0)
                        If DBNullToSomething(oRow.Item("RA_APROBAR")) = 1 Then
                            FSNPageHeader.VisibleBotonAccion1 = False
                            FSNPageHeader.VisibleBotonAprobar = True
                            FSNPageHeader.OnClientClickAprobar = "return EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + ")"
                        ElseIf DBNullToSomething(oRow.Item("RA_RECHAZAR")) = 1 Then
                            FSNPageHeader.VisibleBotonAccion1 = False
                            FSNPageHeader.VisibleBotonRechazar = True
                            FSNPageHeader.OnClientClickRechazar = "return EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + ")"
                        Else
                            FSNPageHeader.VisibleBotonAccion1 = True
                            FSNPageHeader.OnClientClickAccion1 = "return EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + ")"
                            FSNPageHeader.TextoBotonAccion1 = DBNullToStr(oRow.Item("DEN"))
                        End If
                    End If
                End If
            Else
                FSNPageHeader.VisibleBotonAccion1 = False
            End If
        End If
    End Sub
    ''' <summary>
    ''' Revisado por: Sandra. Fecha: 27/08/2012
    ''' Registra las funcionea que vamos a utilizar en javascript
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0,1seg.</remarks>
    Sub GenerarScripts()
        Dim includeScript As String
        If Not Page.ClientScript.IsClientScriptBlockRegistered("Eliminar") Then
            includeScript = "function EliminarInstancia() {" & vbCrLf
            includeScript = includeScript & "if (confirm('" & JSText(Textos(8)) & "') == true) {" & vbCrLf
            includeScript = includeScript & "window.open('EliminarFactura.aspx?Instancia=" & oInstancia.ID & "&Factura=" & oFactura.ID & "','_self');" & vbCrLf
            includeScript = includeScript & "}" & vbCrLf
            includeScript = includeScript & "return false;" & vbCrLf
            includeScript = includeScript & "}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Eliminar", includeScript, True)
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered("Volver") Then
            includeScript = "function Volver() {" & vbCrLf
            If Request("Volver") <> Nothing Then
                includeScript = includeScript & "window.open('" & Request("Volver") & "','_self');" & vbCrLf
            Else
                includeScript = includeScript & "window.open('VisorFacturas.aspx','_self');" & vbCrLf
            End If
            includeScript = includeScript & "return false;" & vbCrLf
            includeScript = includeScript & "}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Volver", includeScript, True)
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered("EnviarMail") Then
            includeScript = "function EnviarMail() {" & vbCrLf
            includeScript = includeScript & "var newWindow = window.open('" & ConfigurationManager.AppSettings("rutaPM") & "_common/MailProveedores.aspx?Instancia=" & oInstancia.ID & "&NumeroFactura=" & oFactura.NumeroFactura & "','_blank', 'width=710,height=360,status=yes,resizable=no,top=180,left=200');" & vbCrLf
            includeScript = includeScript & "return false;" & vbCrLf
            includeScript = includeScript & "}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "EnviarMail", includeScript, True)
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "NumberFormatUsu") Then
            With FSNUser
                Dim sVariablesJavascript As String = "var UsuNumberDecimalSeparator = '" & .NumberFormat.NumberDecimalSeparator & "';"
                sVariablesJavascript = sVariablesJavascript & "var UsuNumberGroupSeparator='" & .NumberFormat.NumberGroupSeparator & "';"
                sVariablesJavascript = sVariablesJavascript & "var UsuNumberNumDecimals='" & .NumberFormat.NumberDecimalDigits & "';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NumberFormatUsu", sVariablesJavascript, True)
            End With
        End If
    End Sub
    ''' <summary>
    ''' Genera la estructura que va a tener el dataset donde se guarda toda la información de la factura para guardarla en xml y que la procese el webservice.
    ''' </summary>
    ''' <remarks>LLamada desde: Page_Load()</remarks>
    Sub GenerarEstructuraDataset()
        Dim dt As DataTable
        Dim ds As DataSet
        Dim keys(0) As DataColumn

        ds = New DataSet

        dt = ds.Tables.Add("FACTURA")
        dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
        dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
        dt.Columns.Add("IMPORTE_BRUTO", System.Type.GetType("System.Double"))
        dt.Columns.Add("PAG", System.Type.GetType("System.String"))
        dt.Columns.Add("VIA_PAG", System.Type.GetType("System.String"))
        dt.Columns.Add("OBS", System.Type.GetType("System.String"))
        dt.Columns.Add("TIPO", System.Type.GetType("System.Int32"))
        dt.Columns.Add("TOT_COSTES", System.Type.GetType("System.Double"))
        dt.Columns.Add("TOT_DESCUENTOS", System.Type.GetType("System.Double"))
        dt.Columns.Add("FEC_CONTA", System.Type.GetType("System.DateTime"))

        keys(0) = dt.Columns("ID")
        dt.PrimaryKey = keys

        dt = ds.Tables.Add("LINEAS_FACTURA")
        dt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
        dt.Columns.Add("NUM", System.Type.GetType("System.Int32"))
        dt.Columns.Add("TOTAL_COSTES", System.Type.GetType("System.Double"))
        dt.Columns.Add("TOTAL_DCTOS", System.Type.GetType("System.Double"))
        dt.Columns.Add("TOTAL_IMP", System.Type.GetType("System.Double"))
        dt.Columns.Add("IMPORTE_SIN_IMP", System.Type.GetType("System.Double"))
        dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
        dt.Columns.Add("OBS", System.Type.GetType("System.String"))

        dt = ds.Tables.Add("COSTES")
        dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
        dt.Columns.Add("DEN", System.Type.GetType("System.String"))
        dt.Columns.Add("OPERACION", System.Type.GetType("System.String"))
        dt.Columns.Add("VALOR", System.Type.GetType("System.Double"))
        dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
        dt.Columns.Add("PLANIFICADO", System.Type.GetType("System.Int32"))

        dt = ds.Tables.Add("DESCUENTOS")
        dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
        dt.Columns.Add("DEN", System.Type.GetType("System.String"))
        dt.Columns.Add("OPERACION", System.Type.GetType("System.String"))
        dt.Columns.Add("VALOR", System.Type.GetType("System.Double"))
        dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
        dt.Columns.Add("PLANIFICADO", System.Type.GetType("System.Int32"))

        dt = ds.Tables.Add("LINEA_IMPUESTOS")
        dt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
        dt.Columns.Add("COD", System.Type.GetType("System.String"))
        dt.Columns.Add("DEN", System.Type.GetType("System.String"))
        dt.Columns.Add("VALOR", System.Type.GetType("System.Double"))
        dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
        dt.Columns.Add("RETENIDO", System.Type.GetType("System.Int16"))
        dt.Columns.Add("COMENT", System.Type.GetType("System.String"))

        dt = ds.Tables.Add("COSTES_ALBARAN")
        dt.Columns.Add("ALBARAN", System.Type.GetType("System.String"))
        dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
        dt.Columns.Add("DEN", System.Type.GetType("System.String"))
        dt.Columns.Add("OPERACION", System.Type.GetType("System.String"))
        dt.Columns.Add("VALOR", System.Type.GetType("System.Double"))
        dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
        dt.Columns.Add("PLANIFICADO", System.Type.GetType("System.Int32"))

        dt = ds.Tables.Add("DESCUENTOS_ALBARAN")
        dt.Columns.Add("ALBARAN", System.Type.GetType("System.String"))
        dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
        dt.Columns.Add("DEN", System.Type.GetType("System.String"))
        dt.Columns.Add("OPERACION", System.Type.GetType("System.String"))
        dt.Columns.Add("VALOR", System.Type.GetType("System.Double"))
        dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
        dt.Columns.Add("PLANIFICADO", System.Type.GetType("System.Int32"))

        dt = ds.Tables.Add("LINEA_COSTES")
        dt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
        dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
        dt.Columns.Add("DEN", System.Type.GetType("System.String"))
        dt.Columns.Add("OPERACION", System.Type.GetType("System.String"))
        dt.Columns.Add("VALOR", System.Type.GetType("System.Double"))
        dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
        dt.Columns.Add("ALBARAN", System.Type.GetType("System.String"))
        dt.Columns.Add("PLANIFICADO", System.Type.GetType("System.Int32"))

        dt = ds.Tables.Add("LINEA_DESCUENTOS")
        dt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
        dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
        dt.Columns.Add("DEN", System.Type.GetType("System.String"))
        dt.Columns.Add("OPERACION", System.Type.GetType("System.String"))
        dt.Columns.Add("VALOR", System.Type.GetType("System.Double"))
        dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
        dt.Columns.Add("ALBARAN", System.Type.GetType("System.String"))
        dt.Columns.Add("PLANIFICADO", System.Type.GetType("System.Int32"))

        dt = ds.Tables.Add("LINEAS_ELIMINADAS")
        dt.Columns.Add("LINEAS", System.Type.GetType("System.String"))

        dt = ds.Tables.Add("TEMP")
        dt.Columns.Add("CC_ID", System.Type.GetType("System.Int32"))
        dt.Columns.Add("CCD_ID", System.Type.GetType("System.Int32"))
        dt.Columns.Add("CAMPO", System.Type.GetType("System.Int32"))
        dt.Columns.Add("VALOR_TEXT", System.Type.GetType("System.String"))
        dt.Columns.Add("VALOR_NUM", System.Type.GetType("System.Double"))
        dt.Columns.Add("VALOR_FEC", System.Type.GetType("System.DateTime"))
        dt.Columns.Add("VALOR_BOOL", System.Type.GetType("System.Int32"))
        dt.Columns.Add("ES_SUBCAMPO", System.Type.GetType("System.Int32"))
        dt.Columns.Add("SUBTIPO", System.Type.GetType("System.Int32"))
        dt.Columns.Add("TIPOGS", System.Type.GetType("System.Int32"))
        dt.Columns.Add("CAMBIO", System.Type.GetType("System.Double"))
        dt.Columns.Add("SAVE_VALUES", System.Type.GetType("System.Int32"))
        dt.Columns.Add("CAMPO_DEF", System.Type.GetType("System.Int32"))
        dt.Columns.Add("CONTACTOS", System.Type.GetType("System.String"))

        dt = ds.Tables.Add("TEMP_PARTICIPANTES")
        dt.Columns.Add("ROL", System.Type.GetType("System.Int32"))
        dt.Columns.Add("TIPO", System.Type.GetType("System.Int16"))
        dt.Columns.Add("PER", System.Type.GetType("System.String"))
        dt.Columns.Add("PROVE", System.Type.GetType("System.String"))
        dt.Columns.Add("CON", System.Type.GetType("System.String"))

        dt = ds.Tables.Add("ROLES")
        dt.Columns.Add("ROL", System.Type.GetType("System.Int32"))
        dt.Columns.Add("PER", System.Type.GetType("System.String"))
        dt.Columns.Add("PROVE", System.Type.GetType("System.String"))
        dt.Columns.Add("CON", System.Type.GetType("System.String"))
        dt.Columns.Add("TIPO", System.Type.GetType("System.Int32"))
        dt.Columns.Add("CAMPO", System.Type.GetType("System.Int32"))

        dt = ds.Tables.Add("SOLICITUD")
        With dt.Columns
            .Add("TIPO_PROCESAMIENTO_XML")
            .Add("TIPO_DE_SOLICITUD")
            .Add("COMPLETO")
            .Add("CODIGOUSUARIO")
            .Add("INSTANCIA")
            .Add("SOLICITUD")
            .Add("USUARIO")
            .Add("USUARIO_EMAIL")
            .Add("USUARIO_IDIOMA")
            .Add("PEDIDO_DIRECTO")
            .Add("FORMULARIO")
            .Add("WORKFLOW")
            .Add("IMPORTE", System.Type.GetType("System.Double"))
            .Add("COMENTALTANOCONF")
            .Add("ACCION")
            .Add("IDACCION")
            .Add("IDACCIONFORM")
            .Add("GUARDAR")
            .Add("NOTIFICAR")
            .Add("TRASLADO_USUARIO")
            .Add("TRASLADO_PROVEEDOR")
            .Add("TRASLADO_FECHA")
            .Add("TRASLADO_COMENTARIO")
            .Add("TRASLADO_PROVEEDOR_CONTACTO")
            .Add("DEVOLUCION_COMENTARIO")
            .Add("COMENTARIO")
            .Add("BLOQUE_ORIGEN")
            .Add("NUEVO_ID_INSTANCIA")
            .Add("BLOQUES_DESTINO")
            .Add("ROL_ACTUAL")
            .Add("FACTURA")             '30
            .Add("IDTIEMPOPROC")
        End With

        Session("dsXMLFactura") = ds
    End Sub
    ''' <summary>
    ''' Carga la propiedad del usercontrol con los costes del buscador
    ''' </summary>
    ''' <param name="sCod">codigo del coste</param>
    ''' <param name="sDesc">descripcion del coste</param>
    ''' <param name="sCodArt">codigo de articulo</param>
    ''' <param name="sCodMat">codigo de material</param>
    ''' <remarks></remarks>
    Private Sub CargarCostesBuscador(ByVal sCod As String, ByVal sDesc As String, ByVal sCodArt As String, ByVal sCodMat As String) Handles Lineas.EventoBuscarCostes
        Dim oFactura As FSNServer.Factura
        oFactura = FSNServer.Get_Object(GetType(FSNServer.Factura))
        Lineas.CostesBuscador = oFactura.Load_Atributos_BuscadorCostes(sCod, sDesc, sCodArt, sCodMat)
    End Sub
    ''' <summary>
    ''' Carga la propiedad del usercontrol con los descuentos del buscador
    ''' </summary>
    ''' <param name="sCod">codigo del Descuento</param>
    ''' <param name="sDesc">descripcion del Descuento</param>
    ''' <param name="sCodArt">codigo de articulo</param>
    ''' <param name="sCodMat">codigo de material</param>
    ''' <remarks></remarks>
    Private Sub CargarDescuentosBuscador(ByVal sCod As String, ByVal sDesc As String, ByVal sCodArt As String, ByVal sCodMat As String) Handles Lineas.EventoBuscarDescuentos
        Dim oFactura As FSNServer.Factura
        oFactura = FSNServer.Get_Object(GetType(FSNServer.Factura))
        Lineas.DescuentosBuscador = oFactura.Load_Atributos_BuscadorDescuentos(sCod, sDesc, sCodArt, sCodMat)
    End Sub
    Sub BuscarCostes(ByVal sCod As String, ByVal sDen As String, ByVal sCodArt As String, ByVal sCodMat As String) Handles DatosGenerales.EventoBuscarCostes
        DatosGenerales.AtributosBuscadorCostes = oFactura.Load_Atributos_BuscadorCostes(sCod, sDen, sCodArt, sCodMat)
    End Sub
    Sub BuscarDescuentos(ByVal sCod As String, ByVal sDen As String, ByVal sCodArt As String, ByVal sCodMat As String) Handles DatosGenerales.EventoBuscarDescuentos
        DatosGenerales.AtributosBuscadorDescuentos = oFactura.Load_Atributos_BuscadorDescuentos(sCod, sDen, sCodArt, sCodMat)
    End Sub
    ''' <summary>
    ''' Carga el detalle del albarán con sus costes y descuentos.
    ''' </summary>
    ''' <param name="sAlbaran">Nombre del albarán</param>
    ''' <remarks>Llamada desde: Al hacer click sobre la columna del albarán en el detalle de las líneas</remarks>
    Sub CargarDetalleAlbaran(ByVal sAlbaran As String) Handles Lineas.EventoCargarDetalleAlbaran
        oFactura.LoadDetalleAlbaran(sAlbaran)

        Lineas.AlbaranFechaRecepcion = oFactura.FechaAlbaran
        Lineas.AlbaranNumeroRecepcionERP = oFactura.NumRecepcionErp
        Lineas.CodReceptor = oFactura.CodReceptor
        Lineas.NombreReceptor = oFactura.NombreReceptor
        Lineas.Cargo = oFactura.Cargo
        Lineas.Departamento = oFactura.Departamento
        Lineas.Email = oFactura.Email
        Lineas.Telefono = oFactura.Telefono
        Lineas.Fax = oFactura.Fax
        Lineas.Organizacion = oFactura.Organizacion
        Lineas.CostesAlbaran = oFactura.CostesAlbaran
        Lineas.DescuentosAlbaran = oFactura.DescuentosAlbaran
    End Sub
    ''' <summary>
    ''' Devuelve las formas de pago para cargarlas en el combo
    ''' </summary>
    ''' <returns>Un dataset con las formas de pago</returns>
    ''' <remarks>Llamada desde: Page_Load()</remarks>
    Function DevolverFormasPago() As DataSet
        Dim oPags As FSNServer.FormasPago
        oPags = FSNServer.Get_Object(GetType(FSNServer.FormasPago))
        oPags.LoadData(Idioma)
        DevolverFormasPago = oPags.Data
    End Function
    ''' <summary>
    ''' Devuelve las vías de pago para cargarlas en el combo
    ''' </summary>
    ''' <returns>Un dataset con las vías de pago</returns>
    ''' <remarks>Llamada desde: Page_Load()</remarks>
    Function DevolverViasPago() As DataSet
        Dim oViasPago As FSNServer.ViasPago
        oViasPago = FSNServer.Get_Object(GetType(FSNServer.ViasPago))
        oViasPago.LoadData(Idioma)
        DevolverViasPago = oViasPago.Data
    End Function
    ''' <summary>
    ''' Funcion que calcula la suma total de los costes de las líneas de la factura
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CalculoSumaCostesLinea() As Double
        Dim Importe As Double = 0

        For Each drCoste As DataRow In oFactura.DatosFactura.Tables(11).Rows
            Importe = Importe + DBNullToDbl(drCoste("IMPORTE"))
        Next

        Return Importe
    End Function
    ''' <summary>
    ''' Funcion que calcula la suma total de los descuentos de las líneas de la factura
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CalculoSumaDescuentosLinea() As Double
        Dim Importe As Double = 0

        For Each drDescuento As DataRow In oFactura.DatosFactura.Tables(13).Rows
            Importe = Importe + DBNullToDbl(drDescuento("IMPORTE"))
        Next

        Return Importe
    End Function
#Region "Objects Data Source Methods"
#End Region
    Sub RealizarAccion(ByVal lAccion As Long)
        Dim bMostrarConfirmacionAccion As Boolean = True

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.RealizarAccion

        Session("EtapaFIN") = False

        'Textos de idiomas:
        Me.lblEtapas.Text = Textos(3)
        m_sIdiSeleccioneRol = Textos(5)
        Me.lblComentarios.Text = Textos(8)
        Me.btnAceptar.Text = Textos(9)
        Me.btnCancelar.Text = Textos(10)

        m_sIdiListaPart = Textos(12)

        'Carga la acción:
        Dim oAccion As FSNServer.Accion
        oAccion = FSNServer.Get_Object(GetType(FSNServer.Accion))
        oAccion.Id = lAccion
        oAccion.CargarAccion(Idioma)
        'Para poder acceder luego a ella¿?
        oInstancia.Accion = lAccion

        imgCabeceraConfirmacion.Src = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/Factura.png"
        lblCabeceraConfirmacion.Text = "Nº Factura: " & oFactura.NumeroFactura & " - " & Textos(11) & " " & oAccion.Den

        If oAccion.TipoRechazo = TipoRechazoAccion.RechazoDefinitivo Or oAccion.TipoRechazo = TipoRechazoAccion.RechazoTemporal Then
            Me.lblRoles.Text = Textos(14)
        Else
            Me.lblRoles.Text = Textos(4)
        End If

        'Comprueba las etapas por las que pasará:
        Dim oEtapas As DataSet
        oEtapas = oInstancia.DevolverSiguientesEtapas(lAccion, FSNUser.CodPersona, Idioma, , _oFactura.EsGestor, _oFactura.EsReceptor)

        If oEtapas.Tables.Count > 0 Then
            If oEtapas.Tables.Count > 1 Then
                For Each oRow As DataRow In oEtapas.Tables(0).Rows
                    'Me.BloqueDestinoWS.Value = Me.BloqueDestinoWS.Value & oRow.Item("BLOQUE") & " "
                Next

                lstEtapas.DataSource = oEtapas.Tables(0)
                lstEtapas.DataTextField = "DEN"
                lstEtapas.DataBind()

                If oEtapas.Tables(0).Rows(0).Item("TIPO") = 2 Then   'Si llega a la etapa de fin:
                    Me.lblRoles.Visible = False
                    Me.tblGeneral.Rows(3).Visible = False
                    Me.lblComentarios.Text = String.Format("{0}:", Textos(16))
                    Session("EtapaFIN") = True
                Else
                    Dim ds As New DataSet
                    ds.Tables.Add(oEtapas.Tables(1).Copy)
                    whdgRoles.DataSource = ds
                    whdgRoles.DataBind()
                End If
            Else ' = 1
                '            'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(),"AbrirEnfraWSMain", "<script>AbrirEnfraWSMain(""" & "../workflow/accionInvalida.aspx?Instancia=" & lInstancia & "&Accion=" & idAccion.Value & "&Version=" & Version.Value & """)</script>")
                Exit Sub
            End If

        Else
            If oAccion.Tipo = 0 And oAccion.TipoRechazo = 0 And oAccion.Guardar = True Then
                bMostrarConfirmacionAccion = False

                Dim oThread As Thread = New Thread(AddressOf GuardarThread)
                oThread.Start()
            Else
                'No provoca ningún cambio de etapa, así que quita del formulario las filas
                'correspondientes a los cambios de etapas y formularios:
                Dim i As Integer
                For i = 6 To 2 Step -1
                    Me.tblGeneral.Rows(i).Visible = False
                Next i
            End If
        End If

        If bMostrarConfirmacionAccion Then
            If Not Page.ClientScript.IsClientScriptBlockRegistered("varMensaje") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "varMensaje", "<script>var sMensaje = '" & JSText(Textos(13)) & "' </script>")
            ClientScript.RegisterStartupScript(Me.GetType(), "DevolverSiguientesEtapas", "<script>document.getElementById('" & divDevolverSiguientesEtapas.ClientID & "').style.display='inline';document.getElementById('" & divDetalleFactura.ClientID & "').style.display='none';</script>")
        End If

        oEtapas = Nothing
    End Sub
#Region "GENERAR XML Y LLAMAR AL WEBSERVICE"
    Private Sub GuardarThread()
        Dim NuevoIdInstancia As Long = 0
        Try
            'Ponemos la instancia en proceso
            oInstancia.Actualizar_En_proceso(1, oInstancia.Etapa)

            Dim lIDTiempoProc As Long
            oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, FSNUser.Cod, oInstancia.Etapa)

            Dim oAccion As FSNServer.Accion
            oAccion = FSNServer.Get_Object(GetType(FSNServer.Accion))
            oAccion.Id = oInstancia.Accion
            oAccion.CargarAccion(Idioma)

            If oAccion.IncrementarId And oAccion.TipoRechazo <> 2 And oAccion.TipoRechazo <> 3 Then
                NuevoIdInstancia = oInstancia.ReservarInstanciaID()
                oInstancia.ID = NuevoIdInstancia
                oInstancia.Actualizar_En_proceso(1, oInstancia.Etapa)
            End If

            InsertarLineas()
            InsertarImpuestos()
            InsertarCostesLinea()
            InsertarCostesAlbaran()
            InsertarDescuentosLinea()
            InsertarDescuentosAlbaran()
            If Session("EtapaFIN") Then
                InsertarFechaContabilizacion()
            End If
            oInstancia.DevolverEtapaActual(FSNUser.Idioma.ToString, FSNUser.CodPersona, oInstancia.Etapa)
            Dim drSolicitud As DataRow
            drSolicitud = CType(Session("dsXMLFactura").Tables("SOLICITUD"), DataTable).NewRow
            With drSolicitud
                .Item("TIPO_PROCESAMIENTO_XML") = CInt(TipoProcesamientoXML.FSNWeb)
                .Item("TIPO_DE_SOLICITUD") = CInt(TiposDeDatos.TipoDeSolicitud.Autofactura)  'Solicitud de pedido
                .Item("COMPLETO") = 1
                .Item("CODIGOUSUARIO") = FSNUser.Cod
                .Item("INSTANCIA") = oInstancia.ID
                .Item("SOLICITUD") = ""
                .Item("USUARIO") = FSNUser.CodPersona
                .Item("USUARIO_EMAIL") = DBNullToStr(FSNUser.Email)
                .Item("USUARIO_IDIOMA") = FSNUser.Idioma.ToString()
                .Item("FORMULARIO") = oInstancia.Solicitud.Formulario.Id
                .Item("WORKFLOW") = oInstancia.Solicitud.Workflow
                .Item("IDACCION") = oInstancia.Accion
                .Item("IDACCIONFORM") = oInstancia.Accion
                .Item("GUARDAR") = 1
                .Item("BLOQUE_ORIGEN") = oInstancia.Etapa
                .Item("NUEVO_ID_INSTANCIA") = NuevoIdInstancia
                .Item("ROL_ACTUAL") = oInstancia.RolActual
                .Item("FACTURA") = oFactura.ID
                .Item("IDTIEMPOPROC") = lIDTiempoProc
            End With

            CType(Session("dsXMLFactura").Tables("SOLICITUD"), DataTable).Clear()
            CType(Session("dsXMLFactura").Tables("SOLICITUD"), DataTable).Rows.Add(drSolicitud)

            Dim sXMLName As String = FSNUser.Cod & "#" & oInstancia.ID & "#" & oInstancia.Etapa
            oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, iFecha:=TiempoProcesamiento.InicioDisco)
            'le ponemos la P para saber que es de portal
            If ConfigurationManager.AppSettings("SERVIDOR_TRATAMIENTO_INSTANCIAS") = "0" Then
                'Se hace con un StringWriter porque el método GetXml del dataset no incluye el esquema
                Dim oSW As New System.IO.StringWriter()
                CType(Session("dsXMLFactura").Tables("SOLICITUD"), DataTable).WriteXml(oSW, XmlWriteMode.WriteSchema)

                Dim oSrvTrataInst As New FSNWebServiceXML.TratamientoInstancias
                oSrvTrataInst.TransferirXMLEnEpera(oSW.ToString(), sXMLName)
            Else
                CType(Session("dsXMLFactura").Tables("SOLICITUD"), DataTable).WriteXml(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & "#FSNWEB.xml", XmlWriteMode.WriteSchema)
                If File.Exists(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml") Then _
                    File.Delete(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml")
                FileSystem.Rename(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & "#FSNWEB.xml", _
                                  ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml")
            End If
        Catch ex As Exception

        End Try
    End Sub
    ''' <summary>
    ''' Inserta en el dataset la fecha de contabilización
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub InsertarFechaContabilizacion()
        Dim oDT As DataTable = CType(Session("dsXMLFactura").Tables("FACTURA"), DataTable)
        If oDT.Rows.Count > 0 Then
            Dim dtRow As DataRow = oDT.Rows(0)
            dtRow.Item("FEC_CONTA") = Now
        End If
    End Sub
    ''' <summary>
    ''' Inserta en el dataset las líneas
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub InsertarLineas()
        Dim oDTLineas As DataTable = CType(Session("dsXMLFactura").Tables("LINEAS_FACTURA"), DataTable)
        Dim dtNewRow As DataRow
        Dim drRowLinea As DataRow

        oDTLineas.Clear()
        For Each drRowLinea In CType(Cache("dsFactura" & FSNUser.Cod), DataSet).Tables(6).Rows
            If drRowLinea.RowState <> DataRowState.Deleted Then
                dtNewRow = oDTLineas.NewRow
                dtNewRow.Item("LINEA") = drRowLinea("LINEA")
                dtNewRow.Item("NUM") = drRowLinea("NUM")
                dtNewRow.Item("TOTAL_COSTES") = drRowLinea("TOTAL_COSTES")
                dtNewRow.Item("TOTAL_DCTOS") = drRowLinea("TOTAL_DCTOS")
                dtNewRow.Item("TOTAL_IMP") = drRowLinea("TOTAL_IMP")
                dtNewRow.Item("IMPORTE_SIN_IMP") = drRowLinea("IMPORTE_SIN_IMP")
                dtNewRow.Item("IMPORTE") = drRowLinea("IMPORTE")
                dtNewRow.Item("OBS") = drRowLinea("OBS")
                If dtNewRow.RowState = DataRowState.Detached Then
                    oDTLineas.Rows.Add(dtNewRow)
                End If
            End If
        Next
    End Sub
    ''' <summary>
    ''' Inserta en el dataset los impuestos de las líneas
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub InsertarImpuestos()
        Dim oDTImpuestos As DataTable = CType(Session("dsXMLFactura").Tables("LINEA_IMPUESTOS"), DataTable)
        Dim dtNewRow As DataRow
        Dim drRowImpuesto As DataRow

        oDTImpuestos.Clear()
        For Each drRowImpuesto In CType(Cache("dsFactura" & FSNUser.Cod), DataSet).Tables(10).Rows
            If drRowImpuesto.RowState <> DataRowState.Deleted AndAlso Not drRowImpuesto("VALOR") Is DBNull.Value Then
                dtNewRow = oDTImpuestos.NewRow
                dtNewRow.Item("LINEA") = drRowImpuesto("LINEA")
                dtNewRow.Item("COD") = drRowImpuesto("COD")
                dtNewRow.Item("DEN") = drRowImpuesto("DEN")
                dtNewRow.Item("VALOR") = drRowImpuesto("VALOR")
                dtNewRow.Item("IMPORTE") = drRowImpuesto("IMPORTE")
                dtNewRow.Item("RETENIDO") = drRowImpuesto("RETENIDO")
                dtNewRow.Item("COMENT") = drRowImpuesto("COMENT")
                If dtNewRow.RowState = DataRowState.Detached Then
                    oDTImpuestos.Rows.Add(dtNewRow)
                End If
            End If
        Next
    End Sub
    Public Sub InsertarCostesLinea()
        Dim oDTCostesLinea As DataTable = CType(Session("dsXMLFactura").Tables("LINEA_COSTES"), DataTable)
        Dim dtNewRow As DataRow
        Dim drRowCoste As DataRow

        oDTCostesLinea.Clear()
        For Each drRowCoste In CType(Cache("dsFactura" & FSNUser.Cod), DataSet).Tables(11).Rows
            If drRowCoste.RowState <> DataRowState.Deleted AndAlso Not drRowCoste("VALOR") Is DBNull.Value Then
                dtNewRow = oDTCostesLinea.NewRow
                dtNewRow.Item("LINEA") = drRowCoste("LINEA")
                dtNewRow.Item("ID") = drRowCoste("DEF_ATRIB_ID")
                dtNewRow.Item("DEN") = drRowCoste("DEN")
                dtNewRow.Item("OPERACION") = drRowCoste("OPERACION")
                dtNewRow.Item("VALOR") = drRowCoste("VALOR")
                dtNewRow.Item("IMPORTE") = drRowCoste("IMPORTE")
                dtNewRow.Item("ALBARAN") = drRowCoste("ALBARAN")
                dtNewRow.Item("PLANIFICADO") = DBNullToInteger(drRowCoste("PLANIFICADO"))
                If dtNewRow.RowState = DataRowState.Detached Then
                    oDTCostesLinea.Rows.Add(dtNewRow)
                End If
            End If
        Next
    End Sub
    Public Sub InsertarCostesAlbaran()
        Dim oDTCostesAlbaran As DataTable = CType(Session("dsXMLFactura").Tables("COSTES_ALBARAN"), DataTable)
        Dim dtNewRow As DataRow
        Dim drRowCoste As DataRow

        oDTCostesAlbaran.Clear()
        For Each drRowCoste In CType(Cache("dsFactura" & FSNUser.Cod), DataSet).Tables(12).Rows
            If drRowCoste.RowState <> DataRowState.Deleted AndAlso Not drRowCoste("VALOR") Is DBNull.Value Then
                dtNewRow = oDTCostesAlbaran.NewRow
                dtNewRow.Item("ALBARAN") = drRowCoste("ALBARAN")
                dtNewRow.Item("ID") = drRowCoste("DEF_ATRIB_ID")
                dtNewRow.Item("DEN") = drRowCoste("DEN")
                dtNewRow.Item("OPERACION") = drRowCoste("OPERACION")
                dtNewRow.Item("VALOR") = drRowCoste("VALOR")
                dtNewRow.Item("IMPORTE") = drRowCoste("IMPORTE")
                dtNewRow.Item("PLANIFICADO") = DBNullToInteger(drRowCoste("PLANIFICADO"))
                If dtNewRow.RowState = DataRowState.Detached Then
                    oDTCostesAlbaran.Rows.Add(dtNewRow)
                End If
            End If
        Next
    End Sub
    Public Sub InsertarDescuentosLinea()
        Dim oDTDescuentosLinea As DataTable = CType(Session("dsXMLFactura").Tables("LINEA_DESCUENTOS"), DataTable)
        Dim dtNewRow As DataRow
        Dim drRowDescuento As DataRow

        oDTDescuentosLinea.Clear()
        For Each drRowDescuento In CType(Cache("dsFactura" & FSNUser.Cod), DataSet).Tables(13).Rows
            If drRowDescuento.RowState <> DataRowState.Deleted AndAlso Not drRowDescuento("VALOR") Is DBNull.Value Then
                dtNewRow = oDTDescuentosLinea.NewRow
                dtNewRow.Item("LINEA") = drRowDescuento("LINEA")
                dtNewRow.Item("ID") = drRowDescuento("DEF_ATRIB_ID")
                dtNewRow.Item("DEN") = drRowDescuento("DEN")
                dtNewRow.Item("OPERACION") = drRowDescuento("OPERACION")
                dtNewRow.Item("VALOR") = drRowDescuento("VALOR")
                dtNewRow.Item("IMPORTE") = drRowDescuento("IMPORTE")
                dtNewRow.Item("ALBARAN") = drRowDescuento("ALBARAN")
                dtNewRow.Item("PLANIFICADO") = DBNullToInteger(drRowDescuento("PLANIFICADO"))
                If dtNewRow.RowState = DataRowState.Detached Then
                    oDTDescuentosLinea.Rows.Add(dtNewRow)
                End If
            End If
        Next
    End Sub
    Public Sub InsertarDescuentosAlbaran()
        Dim oDTDescuentosAlbaran As DataTable = CType(Session("dsXMLFactura").Tables("DESCUENTOS_ALBARAN"), DataTable)
        Dim dtNewRow As DataRow
        Dim drRowDescuento As DataRow

        oDTDescuentosAlbaran.Clear()
        For Each drRowDescuento In CType(Cache("dsFactura" & FSNUser.Cod), DataSet).Tables(14).Rows
            If drRowDescuento.RowState <> DataRowState.Deleted AndAlso Not drRowDescuento("VALOR") Is DBNull.Value Then
                dtNewRow = oDTDescuentosAlbaran.NewRow
                dtNewRow.Item("ALBARAN") = drRowDescuento("ALBARAN")
                dtNewRow.Item("ID") = drRowDescuento("DEF_ATRIB_ID")
                dtNewRow.Item("DEN") = drRowDescuento("DEN")
                dtNewRow.Item("OPERACION") = drRowDescuento("OPERACION")
                dtNewRow.Item("VALOR") = drRowDescuento("VALOR")
                dtNewRow.Item("IMPORTE") = drRowDescuento("IMPORTE")
                dtNewRow.Item("PLANIFICADO") = DBNullToInteger(drRowDescuento("PLANIFICADO"))
                If dtNewRow.RowState = DataRowState.Detached Then
                    oDTDescuentosAlbaran.Rows.Add(dtNewRow)
                End If
            End If
        Next
    End Sub
#End Region
#Region "WebHierarchicalDataGrid ROLES"
    Dim m_sIdiSeleccioneRol As String
    Dim m_sIdiListaPart As String
    Private Sub whdgRoles_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgRoles.InitializeRow
        If DBNullToInteger(e.Row.Items.FindItemByKey("ASIGNAR").Value) = 1 AndAlso DBNullToSomething(e.Row.Items.FindItemByKey("PROVE").Value) = Nothing AndAlso DBNullToSomething(e.Row.Items.FindItemByKey("PER").Value) = Nothing Then
            e.Row.Items.FindItemByKey("SEL").Text = "<input type=""button"" value = ""..."""
            If DBNullToSomething(e.Row.Items.FindItemByKey("TIPO").Value) = "2" AndAlso DBNullToSomething(e.Row.Items.FindItemByKey("PROVE").Value) = Nothing Then
                e.Row.Items.FindItemByKey("SEL").Text = e.Row.Items.FindItemByKey("SEL").Text & " onclick=""BuscadorProveedores(" & e.Row.Items.FindItemByKey("ROL").Value & ")"">"
            ElseIf DBNullToSomething(e.Row.Items.FindItemByKey("PER").Value) = Nothing Then
                e.Row.Items.FindItemByKey("SEL").Text = e.Row.Items.FindItemByKey("SEL").Text & " onclick=""BuscadorUsuarios(" & e.Row.Items.FindItemByKey("ROL").Value & ")"">"
            End If
        Else
            e.Row.Items.FindItemByKey("SEL").Text = "<input type=""button"" value = ""   """
            If DBNullToSomething(e.Row.Items.FindItemByKey("PROVE").Value) = Nothing Then
                e.Row.Items.FindItemByKey("SEL").Text = e.Row.Items.FindItemByKey("SEL").Text & " onclick=""VerDetallePersona('" & e.Row.Items.FindItemByKey("PER").Value & "')"">"
            Else
                e.Row.Items.FindItemByKey("SEL").Text = e.Row.Items.FindItemByKey("SEL").Text & " onclick=""VerDetalleProveedor('" & e.Row.Items.FindItemByKey("PROVE").Value & "')"">"
            End If
        End If

        If DBNullToInteger(e.Row.Items.FindItemByKey("COMO_ASIGNAR").Value) = 3 AndAlso DBNullToSomething(e.Row.Items.FindItemByKey("PER").Value) = Nothing Then 'Es una lista de participantes
            e.Row.Items.FindItemByKey("NOMBRE").Text = m_sIdiListaPart
        ElseIf DBNullToInteger(e.Row.Items.FindItemByKey("ASIGNAR").Value) = 1 AndAlso DBNullToSomething(e.Row.Items.FindItemByKey("PER").Value) = Nothing AndAlso DBNullToSomething(e.Row.Items.FindItemByKey("PROVE").Value) = Nothing Then
            e.Row.Items.FindItemByKey("NOMBRE").Text = m_sIdiSeleccioneRol
            e.Row.Items.FindItemByKey("NOMBRE").CssClass = "fntRequired"
        End If
    End Sub
    Private Sub whdgRoles_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles whdgRoles.PreRender
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.RealizarAccion
        whdgRoles.Columns("DEN").Header.Text = Textos(6)
        whdgRoles.Columns("NOMBRE").Header.Text = Textos(7)
    End Sub
#End Region
    Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Dim oThread As Thread
        oThread = New Thread(AddressOf GuardarThread)
        oThread.Start()
        'Ir al visor de facturas
        Response.Redirect("VisorFacturas.aspx")
    End Sub
End Class