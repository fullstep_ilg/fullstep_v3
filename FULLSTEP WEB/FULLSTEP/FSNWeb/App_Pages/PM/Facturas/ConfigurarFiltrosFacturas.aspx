﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.Master" CodeBehind="ConfigurarFiltrosFacturas.aspx.vb" Inherits="Fullstep.FSNWeb.ConfigurarFiltrosFacturas" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">   
        /*
        ''' <summary>
        ''' Muestra un mensaje
        ''' </summary>
        ''' <param name="pregunta">Texto a monstrar en el alert</param>       
        ''' <returns>True / false dependiendo de si aceptas o no el mensaje</returns>
        ''' <remarks>Llamada desde:=btnEliminar</remarks>
        */
        function ConfirmarEliminar(pregunta) {
            if (confirm(pregunta))
                return true;
            else
                return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
    <asp:HiddenField ID="Accion" runat="server" Value="" />
    <table width="100%" cellpadding="0" border="0">
        <tr>
            <td>
                <fsn:FSNPageHeader ID="FSNPageHeader" runat="server" VisibleBotonVolver="true">
                </fsn:FSNPageHeader>
            </td>
        </tr>
    </table>
    <asp:ValidationSummary ID="vsValidador" HeaderText="" DisplayMode="List" ShowMessageBox="true"
        ShowSummary="false" EnableClientScript="true" runat="server" ValidationGroup="obligatorio" />
    <table width="100%" border="0">
        <tr>
            <td nowrap="nowrap">
                <asp:Label ID="lblNombreFiltro" runat="server" Text="DNombre para el filtro:" CssClass="Etiqueta"></asp:Label>
            </td>
            <td colspan="2">
                <asp:TextBox ID="txtNombreFiltro" runat="server" MaxLength="50" Width="400px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvNombreFiltro" runat="server" Display="None" ControlToValidate="txtNombreFiltro"
                    ValidationGroup="obligatorio"></asp:RequiredFieldValidator>
                <asp:CheckBox ID="chkFiltroPorDefecto" runat="server" Text="DFiltro por defecto"
                    AutoPostBack="false" />
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:RadioButton ID="rbTodosFormularios" runat="server" Text="DAplicar el filtro a todos los formularios"
                    GroupName="radioGroup1" CssClass="Etiqueta" AutoPostBack="true" />
            </td>
        </tr>
        <tr>
            <td style="width: 200px; overflow: visible">
                <asp:RadioButton ID="rbUnFormulario" runat="server" Text="DAplicar el filtro al formulario"
                    GroupName="radioGroup1" CssClass="Etiqueta" Style="width: auto" AutoPostBack="true" />
            </td>
            <td colspan="2">
                <ig:WebDropDown ID="wddFormularios" runat="server" Width="400px" EnableAutoCompleteFirstMatch="false"
                    DropDownContainerWidth="400px" DropDownContainerHeight="200px" AutoPostBack="true" BackColor="White">
                </ig:WebDropDown>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            </td>
            <td align="left" width="80%">
                <asp:UpdatePanel ID="upBotones" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table style="width: 100%" cellpadding="5" cellspacing="0" border="0">
                            <tr>
                                <td style="width: 90%">
                                </td>
                                <td>
                                    <fsn:FSNButton ID="btnAceptarFiltro" runat="server" Visible="false" ValidationGroup="obligatorio"></fsn:FSNButton>
                                </td>
                                <td>
                                    <fsn:FSNButton ID="btnCancelarFiltro" runat="server" Visible="false"></fsn:FSNButton>
                                </td>
                                <td>
                                    <fsn:FSNButton ID="btnEliminarFiltro" runat="server" Visible="false"></fsn:FSNButton>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <asp:Panel ID="phConfiguracionFiltro" style="display: none" runat="server">
                <ajx:CollapsiblePanelExtender ID="CollapsiblePanelExtender0" runat="server" CollapseControlID="pnlBusqueda"
                    ExpandControlID="pnlBusqueda" ImageControlID="imgExpandir0" SuppressPostBack="True"
                    TargetControlID="pnlControlBusqueda" SkinID="FondoRojo">
                </ajx:CollapsiblePanelExtender>
                <asp:Panel ID="pnlBusqueda" runat="server" CssClass="PanelCabecera" Style="cursor: pointer;
                    width: 100%; height: 20px" Font-Bold="True">
                    <table border="0">
                        <tr>
                            <td>
                                <asp:Image ID="imgExpandir0" runat="server" SkinID="Contraer" />
                            </td>
                            <td style="vertical-align: top">
                                <asp:Label ID="lblTituloBusqueda" runat="server" Text="DSeleccione los criterios de búsqueda:"
                                    Font-Bold="True" ForeColor="White"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlControlBusqueda" runat="server" CssClass="Rectangulo" Width="99%">
                    <asp:UpdatePanel ID="upBusquedaAvanzada" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table width="99%">
                                <tr>
                                    <td>
                                        <fspm:BusquedaAvanzadaFAC ID="Busqueda" runat="server"></fspm:BusquedaAvanzadaFAC>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <p>
                    <br />
                </p>
                <asp:UpdatePanel runat="server" ID="upCampos" UpdateMode="Conditional" ChildrenAsTriggers="false">
                <ContentTemplate>
                <ajx:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server" CollapseControlID="pnlCampos"
                    ExpandControlID="pnlCampos" ImageControlID="imgExpandir2" SuppressPostBack="True"
                    TargetControlID="pnlConfiguracionCampos" SkinID="FondoRojo">
                </ajx:CollapsiblePanelExtender>
                <asp:Panel ID="pnlCampos" runat="server" CssClass="PanelCabecera" Style="cursor: pointer;
                    width: 100%; height: 20px" Font-Bold="True">
                    <table border="0">
                        <tr>
                            <td>
                                <asp:Image ID="imgExpandir2" runat="server" SkinID="Contraer" />
                            </td>
                            <td style="vertical-align: top">
                                <asp:Label ID="lblTituloCampos" runat="server" Text="DSeleccione los campos a visualizar en el filtro:"
                                    Font-Bold="True" ForeColor="White"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlConfiguracionCampos" runat="server" CssClass="Rectangulo" Style="width: 95%; overflow: hidden">
                    <br />
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr style="padding-bottom: 5px">
                            <td colspan="2">
                                <asp:Label ID="lblTituloCamposGenerales" runat="server" Text="DSeleccione los campos generales a visualizar en el filtro:"
                                    CssClass="Rotulo"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="width: 90%; padding-right: 15px">
                                <table width="100%" style="border-top: solid 1px #B2B2B2;border-left: solid 1px #B2B2B2;" cellpadding="0" cellspacing="0">
                                    <asp:Repeater ID="rpCamposGenerales" runat="server">
                                        <HeaderTemplate>
                                            <tr style="height: 20px">
                                                <td style="width: 20%; padding-left: 3px;border-bottom: solid 1px #B2B2B2">
                                                    <asp:Label ID="lblCampoGeneral" runat="server" Text=''></asp:Label>
                                                </td>
                                                <td align="center" style="width: 5%; border-right: solid 1px #B2B2B2; border-left: solid 1px #B2B2B2;border-bottom: solid 1px #B2B2B2">
                                                    <asp:Label ID="lblVisible" runat="server" Text=''></asp:Label>
                                                </td>
                                                <td style="width: 25%; padding-left: 3px;border-bottom: solid 1px #B2B2B2">
                                                    <asp:Label ID="lblNombrePersonalizado" runat="server" Text=''></asp:Label>
                                                </td>
                                                <td style="width: 20%; padding-left: 3px; border-left: solid 1px #B2B2B2;border-bottom: solid 1px #B2B2B2">
                                                    <asp:Label ID="lblCampoGeneral2" runat="server" Text=''></asp:Label>
                                                </td>
                                                <td align="center" style="width: 5%; border-right: solid 1px #B2B2B2;border-left: solid 1px #B2B2B2;border-bottom: solid 1px #B2B2B2">
                                                    <asp:Label ID="lblVisible2" runat="server" Text=''></asp:Label>
                                                </td>
                                                <td style="width: 25%; padding-left: 3px;border-bottom: solid 1px #B2B2B2;border-right: solid 1px #B2B2B2;">
                                                    <asp:Label ID="lblNombrePersonalizado2" runat="server" Text=''></asp:Label>
                                                </td>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr style="background-color: White">
                                                <td style="width: 20%; padding-left: 5px; border-bottom: solid 1px #B2B2B2">
                                                    <asp:Label ID="CampoGeneral_lblCampoGeneral" runat="server" Text='<%#Container.DataItem("NOMBRE")%>'></asp:Label>
                                                </td>
                                                <td align="center" style="width: 5%; border-right: solid 1px #B2B2B2; border-left: solid 1px #B2B2B2;
                                                    border-bottom: solid 1px #B2B2B2">
                                                    <asp:CheckBox ID="CampoGeneral_Visible" runat="server" AutoPostBack="true" />
                                                </td>
                                                <td style="width: 25%; padding-left: 3px; border-bottom: solid 1px #B2B2B2; padding-top: 2px;
                                                    padding-bottom: 2px;border-right: solid 1px #B2B2B2;">
                                                    <asp:TextBox ID="CampoGeneral_NombrePersonalizado" runat="server" Width="98%" AutoPostBack="true"></asp:TextBox>
                                                </td>
                                        </ItemTemplate>
                                       
                                        <AlternatingItemTemplate>
                                                <td style="width: 20%; padding-left: 5px; border-bottom: solid 1px #B2B2B2">
                                                    <asp:Label ID="CampoGeneral_lblCampoGeneral" runat="server" Text='<%#Container.DataItem("NOMBRE")%>'></asp:Label>
                                                </td>
                                                <td align="center" style="width: 5%; border-right: solid 1px #B2B2B2; border-left: solid 1px #B2B2B2;
                                                    border-bottom: solid 1px #B2B2B2">
                                                    <asp:CheckBox ID="CampoGeneral_Visible" runat="server" AutoPostBack="true" />
                                                </td>
                                                <td style="width: 25%; padding-left: 3px; border-bottom: solid 1px #B2B2B2; padding-top: 2px;
                                                    padding-bottom: 2px;border-right: solid 1px #B2B2B2;">
                                                    <asp:TextBox ID="CampoGeneral_NombrePersonalizado" runat="server" Width="98%" AutoPostBack="true"></asp:TextBox>
                                                </td>
                                                </tr>
                                        </AlternatingItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </td>
                            </tr>
                    </table>
                 </asp:Panel>
                 </ContentTemplate>
                </asp:UpdatePanel>
                    <br />
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <asp:Label ID="lblTituloGridConfiguracion" runat="server" Text="DPara modificar el orden de los campos desplace las columnas."
                                    CssClass="Rotulo"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:UpdatePanel runat="server" ID="upGrid" UpdateMode="Conditional">
                                <ContentTemplate>
                                <ig:WebHierarchicalDataGrid runat="server" ID="whgConfigurarFiltros" AutoGenerateBands="false"
					                AutoGenerateColumns="false"  Width="100%" Height="200px" EnableAjax="true" 
					                EnableAjaxViewState="false" EnableDataViewState="true" ExpansionColumnCss="hidExpColumn">
					            <Behaviors>
						            <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
						            <ig:ColumnMoving Enabled="true">
                                        <ColumnSettings>
                                            <ig:ColumnMoveSetting ColumnKey="TEMPLATEAPROBAR" EnableMove="false" />
                                            <ig:ColumnMoveSetting ColumnKey="TEMPLATERECHAZAR" EnableMove="false" />
                                        </ColumnSettings>
                                    </ig:ColumnMoving>	
					            </Behaviors>
                                <Columns>
                                    <ig:TemplateDataField Key="TEMPLATEAPROBAR" VisibleIndex="0"  Width="70px" Hidden="false">                   


                                    <HeaderTemplate>                           
                                        <fsn:FSNButton ID="btnAprobar2" runat="server" Alineacion="Left" Text="dAprobar" OnPreRender="btnAprobar_OnPreRender"></fsn:FSNButton>                        
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div style="text-align: center;">
                                            <asp:CheckBox ID="cbAprobar" runat="server" />
                                         </div>
                                    </ItemTemplate>
                                    </ig:TemplateDataField>
                                       
                                    <ig:TemplateDataField Key="TEMPLATERECHAZAR" VisibleIndex="1"  Width="80px" Hidden="false">
                                      <HeaderTemplate>
                                        <fsn:FSNButton ID="btnRechazar2" runat="server" Alineacion="Left" Text="dRechazar" OnPreRender="btnRechazar_OnPreRender"></fsn:FSNButton>
                                      </HeaderTemplate>
                                      <ItemTemplate>
                                        <div style="text-align: center;">
                                            <asp:CheckBox ID="cbRechazar" runat="server" />
                                        </div>
                                      </ItemTemplate>
                                     </ig:TemplateDataField>
                                </Columns>
				                </ig:WebHierarchicalDataGrid> 
                                </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
             </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="wddFormularios" EventName="SelectionChanged" />
        </Triggers>
    </asp:UpdatePanel>

    <script type="text/javascript" language="javascript">
        var ModalProgress = '<%= ModalProgress.ClientID %>';
    </script>
    <ajx:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
    <p>&nbsp;</p>
</asp:Content>
