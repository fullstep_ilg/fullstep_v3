﻿Public Class EliminarFactura
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Elimina la instancia:
        Dim oInstancia As FSNServer.Instancia
        Dim sUrl As String

        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        oInstancia.ID = Request("Instancia")
        oInstancia.EliminarFactura(Request("Factura"), FSNUser.Cod)

        sUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Facturas/VisorFacturas.aspx"
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "volver", "<script>EliminarOK('" & sUrl & "')</script>")
    End Sub

End Class