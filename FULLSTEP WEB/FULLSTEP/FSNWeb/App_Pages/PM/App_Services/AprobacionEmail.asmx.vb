﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.IO

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class AprobacionEmail
    Inherits System.Web.Services.WebService

    Public Enum TipoAccion
        Aprobacion = 1
        Rechazo = 2
    End Enum

    ''' <summary>
    ''' Procesar Acciones lanzadas desde un EMail usando el link de aprobar/rechazar
    ''' </summary>
    ''' <param name="Instancia">Id de la instancia</param>
    ''' <param name="Accion">Id de la acción</param>
    ''' <param name="Bloque">Id del bloque</param>
    ''' <param name="Tipo">aprobación/rechazo</param>
    ''' <param name="Random">nº random que sirve para comprobar la aprobación/rechazo</param>
    ''' <param name="Usuario">Usuario</param>
    ''' <param name="TipoSolicitud">Tipo de solicitud: factura, contrato, ...</param>
    ''' <remarks>Llamada desde: Notificar.vb     Login.master.vb    LoginEmail.aspx.vb; Tiempo máximo: 0,5 sg</remarks>
    <WebMethod()> _
    Public Sub ProcesarAccionesPMEMail(ByVal Instancia As Long, ByVal Accion As Long, ByVal Bloque As Long, ByVal Tipo As Integer, ByVal Random As Integer, _
                                       ByVal Usuario As String, ByVal TipoSolicitud As TiposDeDatos.TipoDeSolicitud)
        Dim FSNServer As New FSNServer.Root
        Dim oUser As FSNServer.User
        Dim oInstancias As FSNServer.Instancias
        Dim oInstancia As FSNServer.Instancia
        Dim oBloque As FSNServer.Bloque
        Dim dFechaMail As Date
        Dim Textos As DataSet
        Dim sTexto As String = Nothing

        oUser = FSNServer.Login(Usuario, True)
        oUser.LoadUserData(Usuario)

        oInstancias = FSNServer.Get_Object(GetType(FSNServer.Instancias))
        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        oBloque = FSNServer.Get_Object(GetType(FSNServer.Bloque))

        oBloque.GrabarAccionEmail(Accion, Instancia, Bloque)

        Dim FSPMDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        FSPMDict.LoadData(TiposDeDatos.ModulosIdiomas.AprobacionOK, oUser.Idioma)
        Textos = FSPMDict.Data

        'Comprobar si está procesada
        If oInstancias.ComprobarSiEstaProcesada(Instancia, Bloque) Then
            sTexto = Textos.Tables(0).Rows(12).Item(1)
            HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "PedidoAprobadoMail.aspx?Texto=" & HttpContext.Current.Server.UrlEncode(sTexto.Replace("#ID#", CStr(Instancia))))
        ElseIf oInstancias.ComprobarEstadoIncompatibleGS(Instancia) Then
            sTexto = Textos.Tables(0).Rows(12).Item(1)
            HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "PedidoAprobadoMail.aspx?Texto=" & HttpContext.Current.Server.UrlEncode(sTexto.Replace("#ID#", CStr(Instancia))))
        ElseIf (TipoSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoCatalogo Or TipoSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoContraAbierto) _
            AndAlso oInstancias.ComprobarEstadoIncompatibleCesta(Instancia) Then
            sTexto = Textos.Tables(0).Rows(12).Item(1)
            HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "PedidoAprobadoMail.aspx?Texto=" & HttpContext.Current.Server.UrlEncode(sTexto.Replace("#ID#", CStr(Instancia))))
        Else
            'Comprobar Email caducado
            If Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("Caducidad_Email")) Then
                dFechaMail = oBloque.ObtenerFecha(Instancia, Bloque)
                If dFechaMail.AddHours(CInt(ConfigurationManager.AppSettings("Caducidad_Email"))) < Now.Date Then
                    If Tipo = TipoAccion.Aprobacion Then
                        sTexto = Textos.Tables(0).Rows(13).Item(1)
                    Else
                        sTexto = Textos.Tables(0).Rows(14).Item(1)
                    End If
                    sTexto = Replace(sTexto, "XXX", CStr(Instancia))
                    HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "PedidoAprobadoMail.aspx?Texto=" & HttpContext.Current.Server.UrlEncode(sTexto))
                End If
            End If

            If Random = oBloque.ObtenerRandom(Instancia, Bloque) Then
                oInstancias.Actualizar_En_Proceso2(Instancia, oUser.CodPersona)

                If TipoSolicitud = 0 Then TipoSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras 'Desde login.master y LoginEmail no se le pasa. 0-> Otros. Por defecto debe ser 1->SolicitudDeCompras
                Dim xmlName As String
                Dim lIDTiempoProc As Long
                oInstancia.ID = Instancia
                oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, oUser.Cod, Bloque)

                Dim dsXML As New DataSet
                dsXML.Tables.Add("SOLICITUD")
                Dim drSolicitud As DataRow
                With dsXML.Tables("SOLICITUD").Columns
                    .Add("TIPO_PROCESAMIENTO_XML")
                    .Add("TIPO_DE_SOLICITUD")
                    .Add("COMPLETO")
                    .Add("CODIGOUSUARIO")
                    .Add("INSTANCIA")
                    .Add("USUARIO")
                    .Add("USUARIO_EMAIL")
                    .Add("USUARIO_IDIOMA")
                    .Add("ACCION")
                    .Add("IDACCION")
                    .Add("COMENTARIO")
                    .Add("BLOQUE_ORIGEN")
                    .Add("THOUSANFMT")
                    .Add("DECIAMLFMT")
                    .Add("PRECISIONFMT")
                    .Add("DATEFMT")
                    .Add("REFCULTURAL")
                    .Add("TIPOEMAIL")
                    .Add("IDTIEMPOPROC")
                End With

                drSolicitud = dsXML.Tables("SOLICITUD").NewRow
                With drSolicitud
                    .Item("TIPO_PROCESAMIENTO_XML") = CInt(TipoProcesamientoXML.FSNWeb_PM_AprobacionRechazoMultiple)
                    .Item("TIPO_DE_SOLICITUD") = CInt(TipoSolicitud)
                    .Item("COMPLETO") = 1
                    .Item("CODIGOUSUARIO") = oUser.Cod
                    .Item("INSTANCIA") = Instancia
                    .Item("USUARIO") = oUser.CodPersona
                    .Item("USUARIO_EMAIL") = oUser.Email
                    .Item("USUARIO_IDIOMA") = oUser.IdiomaCod
                    .Item("ACCION") = ""
                    .Item("IDACCION") = Accion
                    .Item("COMENTARIO") = ""
                    .Item("BLOQUE_ORIGEN") = Bloque
                    .Item("THOUSANFMT") = oUser.ThousanFmt
                    .Item("DECIAMLFMT") = oUser.DecimalFmt
                    .Item("PRECISIONFMT") = oUser.PrecisionFmt
                    .Item("DATEFMT") = oUser.DateFmt
                    .Item("REFCULTURAL") = oUser.Idioma.RefCultural
                    .Item("TIPOEMAIL") = oUser.TipoEmail
                    .Item("IDTIEMPOPROC") = lIDTiempoProc
                End With
                xmlName = oUser.Cod & "#" & Instancia & "#" & Bloque
                dsXML.Tables("SOLICITUD").Rows.Add(drSolicitud)

                oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, iFecha:=TiempoProcesamiento.InicioDisco)
                If ConfigurationManager.AppSettings("SERVIDOR_TRATAMIENTO_INSTANCIAS") = "0" Then
                    'Se hace con un StringWriter porque el método GetXml del dataset no incluye el esquema
                    Dim oSW As New System.IO.StringWriter()
                    dsXML.WriteXml(oSW, XmlWriteMode.WriteSchema)

                    Dim oSrvTrataInst As New FSNWebServiceXML.TratamientoInstancias
                    oSrvTrataInst.TransferirXMLEnEpera(oSW.ToString(), xmlName)
                Else
                    dsXML.WriteXml(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & "#FSNWEB.xml", XmlWriteMode.WriteSchema)
                    If File.Exists(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml") Then _
                        File.Delete(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml")
                    FileSystem.Rename(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & "#FSNWEB.xml",
                                      ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml")
                End If

                Select Case Tipo
                    Case TipoAccion.Aprobacion
                        sTexto = Textos.Tables(0).Rows(10).Item(1)
                    Case TipoAccion.Rechazo
                        sTexto = Textos.Tables(0).Rows(11).Item(1)
                End Select

                If TipoSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoCatalogo Then
                    Dim oPedido As FSNServer.Pedido = FSNServer.Get_Object(GetType(FSNServer.Pedido))
                    oPedido.LoadDetalle(oUser.Idioma, Instancia)
                    sTexto = Replace(sTexto, "#ID#", oPedido.NumPedidoCompleto)
                Else
                    sTexto = Replace(sTexto, "#ID#", CStr(Instancia))
                End If

                HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "PedidoAprobadoMail.aspx?Texto=" & HttpContext.Current.Server.UrlEncode(sTexto))
            End If
        End If
    End Sub
End Class