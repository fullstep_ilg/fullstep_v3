﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Facturas
    Inherits WebService
    <WebMethod(True)> _
    Public Sub LimpiarCostesDescuentosGenerales()
        Dim oDT As DataTable
        oDT = CType(HttpContext.Current.Session("dsXMLFactura").Tables("COSTES"), DataTable)
        oDT.Rows.Clear()
        oDT = CType(HttpContext.Current.Session("dsXMLFactura").Tables("DESCUENTOS"), DataTable)
        oDT.Rows.Clear()
    End Sub
    <WebMethod(True)> _
    Public Sub InsertarCosteGeneral(ByVal lId As List(Of Long), ByVal sDen As List(Of String), ByVal sOperacion As List(Of String), ByVal dValor As List(Of Double), ByVal dImporte As List(Of Double))
        Dim oDTCoste As DataTable = CType(HttpContext.Current.Session("dsXMLFactura").Tables("COSTES"), DataTable)
        For i = 0 To lId.Count - 1
            Dim dtNewRow As DataRow
            dtNewRow = oDTCoste.NewRow
            dtNewRow.Item("ID") = lId(i)
            dtNewRow.Item("DEN") = sDen(i)
            dtNewRow.Item("OPERACION") = sOperacion(i)
            dtNewRow.Item("VALOR") = dValor(i)
            dtNewRow.Item("IMPORTE") = dImporte(i)
            dtNewRow.Item("PLANIFICADO") = 0
            If dtNewRow.RowState = DataRowState.Detached Then
                oDTCoste.Rows.Add(dtNewRow)
            End If
        Next

    End Sub
    <WebMethod(True)> _
    Public Sub InsertarDescuentoGeneral(ByVal lId As List(Of Long), ByVal sDen As List(Of String), ByVal sOperacion As List(Of String), ByVal dValor As List(Of Double), ByVal dImporte As List(Of Double))
        Dim oDTCoste As DataTable = CType(HttpContext.Current.Session("dsXMLFactura").Tables("DESCUENTOS"), DataTable)
        For i = 0 To lId.Count - 1
            Dim dtNewRow As DataRow
            dtNewRow = oDTCoste.NewRow
            dtNewRow.Item("ID") = lId(i)
            dtNewRow.Item("DEN") = sDen(i)
            dtNewRow.Item("OPERACION") = sOperacion(i)
            dtNewRow.Item("VALOR") = dValor(i)
            dtNewRow.Item("IMPORTE") = dImporte(i)
            dtNewRow.Item("PLANIFICADO") = 0
            If dtNewRow.RowState = DataRowState.Detached Then
                oDTCoste.Rows.Add(dtNewRow)
            End If
        Next
    End Sub
    <WebMethod(True)> _
    Public Sub InsertarDatosFactura(ByVal lId As Long, ByVal dImporte As Double, ByVal sCodFormaPago As String, ByVal sCodViaPago As String, ByVal sObservaciones As String, ByVal iTipo As Integer, ByVal dTotalCostes As Double, ByVal dTotalDescuentos As Double)
        Dim oDTFactura As DataTable = CType(HttpContext.Current.Session("dsXMLFactura").Tables("FACTURA"), DataTable)
        Dim dtNewRow As DataRow
        Dim bNewRow As Boolean = False

        dtNewRow = oDTFactura.Rows.Find(lId)
        If dtNewRow Is Nothing Then
            bNewRow = True
            dtNewRow = oDTFactura.NewRow
        End If
        dtNewRow.Item("ID") = lId
        dtNewRow.Item("IMPORTE") = dImporte
        dtNewRow.Item("PAG") = sCodFormaPago
        dtNewRow.Item("VIA_PAG") = sCodViaPago
        dtNewRow.Item("OBS") = sObservaciones
        dtNewRow.Item("TIPO") = iTipo
        dtNewRow.Item("TOT_COSTES") = dTotalCostes
        dtNewRow.Item("TOT_DESCUENTOS") = dTotalDescuentos

        If bNewRow AndAlso dtNewRow.RowState = DataRowState.Detached Then
            oDTFactura.Rows.Add(dtNewRow)
        End If
    End Sub
    <WebMethod(True)>
    Public Sub ActualizarCostesDescuentosGenerales(ByVal lId As Long, ByVal dImporte As Double, ByVal dTotalCostes As Double, ByVal dTotalDescuentos As Double, ByVal dImporteBruto As Double)
        Dim oDTFactura As DataTable = CType(HttpContext.Current.Session("dsXMLFactura").Tables("FACTURA"), DataTable)
        Dim dtNewRow As DataRow
        Dim bNewRow As Boolean = False

        dtNewRow = oDTFactura.Rows.Find(lId)
        If dtNewRow Is Nothing Then
            bNewRow = True
            dtNewRow = oDTFactura.NewRow
        End If
        dtNewRow.Item("ID") = lId
        dtNewRow.Item("IMPORTE") = dImporte
        If dImporteBruto > 0 Then
            dtNewRow.Item("IMPORTE_BRUTO") = dImporteBruto
        End If
        dtNewRow.Item("TOT_COSTES") = dTotalCostes
        dtNewRow.Item("TOT_DESCUENTOS") = dTotalDescuentos

        If bNewRow AndAlso dtNewRow.RowState = DataRowState.Detached Then
            oDTFactura.Rows.Add(dtNewRow)
        End If
    End Sub
    <WebMethod(True)> _
    Public Sub InsertarRol(ByVal lRol As Long, ByVal sPer As String, ByVal sProve As String, ByVal sContactos As String, ByVal iTipo As Integer)
        Dim oDTRol As DataTable = CType(HttpContext.Current.Session("dsXMLFactura").Tables("ROLES"), DataTable)
        Dim dtNewRow As DataRow
        dtNewRow = oDTRol.NewRow
        dtNewRow.Item("ROL") = lRol
        dtNewRow.Item("PER") = sPer
        dtNewRow.Item("PROVE") = sProve
        dtNewRow.Item("CON") = sContactos
        dtNewRow.Item("TIPO") = iTipo
        dtNewRow.Item("CAMPO") = Nothing
        If dtNewRow.RowState = DataRowState.Detached Then
            oDTRol.Rows.Add(dtNewRow)
        End If
    End Sub
#Region "Tolerancia"
    <WebMethod(True)> _
    Public Function ComprobarTolerancia(ByVal ToleranciaImporte As Double, ByVal ToleranciaPorcentaje As Double, ByVal dImporteTotal As Double, ByVal dCostesGenerales As Double) As Boolean
        Dim bCumpleTolerancia As Boolean = True
        If ToleranciaImporte > 0 OrElse ToleranciaPorcentaje > 0 Then
            Dim dTotalImporteParaTolerancia As Double = 0
            Dim dTotalCosteParaTolerancia As Double = 0
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")

            Dim dtLineas As DataTable = CType(HttpContext.Current.Cache("dsFactura" & FSNUser.Cod), DataSet).Tables(6)
            dtLineas.DefaultView.Sort = "ALBARAN ASC"
            Dim sAlbaran As String = String.Empty
            For Each drLinea As DataRow In dtLineas.Rows
                If sAlbaran = String.Empty Then sAlbaran = drLinea("ALBARAN")
                If drLinea("ALBARAN") <> sAlbaran Then
                    dTotalCosteParaTolerancia = dTotalCosteParaTolerancia + (dTotalImporteParaTolerancia * dCostesGenerales) / dImporteTotal
                    If dTotalCosteParaTolerancia > 0 Then
                        If ToleranciaImporte > 0 AndAlso ToleranciaImporte < dTotalCosteParaTolerancia Then
                            bCumpleTolerancia = False
                            Exit For
                        End If
                        If bCumpleTolerancia AndAlso ToleranciaPorcentaje > 0 Then
                            If (dTotalImporteParaTolerancia * ToleranciaPorcentaje / 100) < dTotalCosteParaTolerancia Then
                                bCumpleTolerancia = False
                                Exit For
                            End If
                        End If
                    End If
                    sAlbaran = drLinea("ALBARAN")
                    dTotalImporteParaTolerancia = 0
                    dTotalCosteParaTolerancia = 0
                End If
                dTotalImporteParaTolerancia = dTotalImporteParaTolerancia + drLinea("IMPORTE")
                dTotalCosteParaTolerancia = dTotalCosteParaTolerancia + drLinea("TOTAL_COSTES")
            Next

            If bCumpleTolerancia Then
                dTotalCosteParaTolerancia = dTotalCosteParaTolerancia + (dTotalImporteParaTolerancia * dCostesGenerales) / dImporteTotal
                If dTotalCosteParaTolerancia > 0 Then
                    If ToleranciaImporte > 0 AndAlso ToleranciaImporte < dTotalCosteParaTolerancia Then
                        bCumpleTolerancia = False
                    End If
                    If bCumpleTolerancia AndAlso ToleranciaPorcentaje > 0 Then
                        If (dTotalImporteParaTolerancia * ToleranciaPorcentaje / 100) < dTotalCosteParaTolerancia Then
                            bCumpleTolerancia = False
                        End If
                    End If
                End If
            End If
        End If
        Return bCumpleTolerancia
    End Function
#End Region
#Region "Discrepancias"
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_Datos_NuevaDiscrepancia(ByVal Prove As String) As List(Of FSNServer.cn_fsItem)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")

        Dim ocnInfo As FSNServer.cnDiscrepancias
        ocnInfo = FSNServer.Get_Object(GetType(FSNServer.cnDiscrepancias))

        Dim info As List(Of FSNServer.cn_fsItem)

        With CN_Usuario
            info = ocnInfo.Obtener_Datos_NuevaDiscrepancia(Prove, .Idioma)
        End With

        Return info
    End Function
    ''' <summary>
    ''' Obtener las líneas de la factura
    ''' </summary>
    ''' <param name="Factura">Id. de factura</param>  
    ''' <returns>Proveedor</returns>
    ''' <remarks>JVS 22/06/2012</remarks>
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_Lineas_Factura(ByVal Factura As Integer, ByVal Linea As Integer) As Object
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim ocnInfo As FSNServer.cnDiscrepancias
        ocnInfo = FSNServer.Get_Object(GetType(FSNServer.cnDiscrepancias))
        Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")

        Dim oLineas As New List(Of FSNServer.cnLineaFactura)
        With CN_Usuario
            oLineas = ocnInfo.Obtener_Lineas_Factura(.Idioma, Factura, CN_Usuario.Cod, Linea, CN_Usuario.NumberFormat)
        End With
        Return oLineas
    End Function
    <Services.WebMethod(True)> _
        <Script.Services.ScriptMethod()> _
    Public Sub Cerrar_Discrepancia(ByVal IdDiscrepancia As Integer)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim ocnDiscrepancia As FSNServer.cnDiscrepancias
        ocnDiscrepancia = FSNServer.Get_Object(GetType(FSNServer.cnDiscrepancias))

        ocnDiscrepancia.Cerrar_Discrepancias(IdDiscrepancia:=IdDiscrepancia)
    End Sub
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Sub Cerrar_Discrepancias_Linea(ByVal Factura As Integer, ByVal Linea As Integer)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim ocnDiscrepancia As FSNServer.cnDiscrepancias
        ocnDiscrepancia = FSNServer.Get_Object(GetType(FSNServer.cnDiscrepancias))

        ocnDiscrepancia.Cerrar_Discrepancias(Factura, Linea)
    End Sub
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Comprobar_DiscrepanciasCerradas(ByVal IdFactura As Integer, ByVal Linea As Integer) As Integer
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim ocnDiscrepancia As FSNServer.cnDiscrepancias
        ocnDiscrepancia = FSNServer.Get_Object(GetType(FSNServer.cnDiscrepancias))
        Return ocnDiscrepancia.Comprobar_DiscrepanciasCerradas(IdFactura, Linea)
    End Function
#End Region
End Class