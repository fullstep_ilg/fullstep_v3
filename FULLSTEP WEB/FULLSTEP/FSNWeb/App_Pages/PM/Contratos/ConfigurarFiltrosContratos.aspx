﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.Master"
    CodeBehind="ConfigurarFiltrosContratos.aspx.vb" Inherits="Fullstep.FSNWeb.ConfigurarFiltrosContratos" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <style type="text/css">
        DIV.PanelCabecera + DIV
        {
            height:100% !important;
        }
    </style>
    <script type="text/javascript">   
        /*
        ''' <summary>
        ''' Muestra un mensaje
        ''' </summary>
        ''' <param name="pregunta">Texto a monstrar en el alert</param>       
        ''' <returns>True / false dependiendo de si aceptas o no el mensaje</returns>
        ''' <remarks>Llamada desde:=btnEliminar</remarks>
        */
        function ConfirmarEliminar(pregunta)  {
            if (confirm(pregunta))
                return true;
            else
                return false;
        }    
    
        /*
        Descripcion: Funcion que inicializa la caja de texto de los combos Codigo y Denominacion a ""
        Parametros entrada:=
        elem:=componente input
        event:=evento
        Llamada desde: Al cambiar el valor del combo tipo
        Tiempo ejecucion:= 0seg.    
    
       */        
        function ValueChanged_wddCommodity(elem, event) {
            
            comboCodigo = $find("<%=ddlCodigo.ClientID%>")
            if (comboCodigo)
                comboCodigo._elements.Input.value=""
                
            comboDenominacion = $find("<%=ddlDenominacion.ClientID%>")
            if (comboDenominacion)
                comboDenominacion._elements.Input.value=""

        } 
        
        /*
        Descripcion: Abrir el buscador de procesos"
        Llamada desde: onClick de ImgBuscadorProcesos     
        */          
        function AbrirBuscadorProcesos() {
            var sAnyo = '';
            var sCommodity = '';
            var sCodProceso = '';
            var sDenProceso = '';
            
            var wdd1 = $find('<%=ddlAnyo.clientID%>');            
            if (wdd1!=null)
                if (wdd1.get_selectedItem()!=null)                    
                    sAnyo= wdd1.get_selectedItem().get_value();
                
            var wdd2 = $find('<%=ddlCommodity.clientID%>');            
            if (wdd2!=null)                
                if (wdd2.get_selectedItem()!=null)
                    sCommodity= wdd2.get_selectedItem().get_value();
                
            var wdd3 = $find('<%=ddlCodigo.clientID%>');            
            if (wdd3!=null)
                if (wdd3.get_selectedItem()!=null)
                    sCodProceso= wdd3.get_selectedItem().get_value();                                
                                
            var wdd4 = $find('<%=ddlDenominacion.clientID%>');            
            if (wdd4!=null)
                if (wdd4.get_selectedItem()!=null)
                    sDenProceso= wdd4.get_selectedItem().get_value();

            var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>_common/BuscadorProcesos.aspx?desdeGS=<%=Session("desdeGS")%>&Anyo=" + escape(sAnyo) + "&Commodity=" + escape(sCommodity) + "&CodProceso=" + escape(sCodProceso) + "&DenProceso=" + escape(sDenProceso), "_blank", "width=800,height=500,status=yes,resizable=no,top=100,left=200");
            
        }

        
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
    
    <asp:HiddenField ID="Accion" runat="server" Value="" />
    <table width="100%" cellpadding="0" border="0">
        <tr>
            <td>
                <fsn:FSNPageHeader ID="FSNPageHeader" runat="server" VisibleBotonVolver="true">
                </fsn:FSNPageHeader>
            </td>
        </tr>
    </table>
    <asp:ValidationSummary ID="vsValidador" HeaderText="" DisplayMode="List" ShowMessageBox="true"
        ShowSummary="false" EnableClientScript="true" runat="server" ValidationGroup="obligatorio" />
    <table width="100%" border="0">
        <tr>
            <td nowrap="nowrap">
                <asp:Label ID="lblNombreFiltro" runat="server" Text="DNombre para el filtro:" CssClass="Etiqueta"></asp:Label>
            </td>
            <td colspan="2">
                <asp:TextBox ID="txtNombreFiltro" runat="server" MaxLength="50" Width="400px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvNombreFiltro" runat="server" Display="None" ControlToValidate="txtNombreFiltro"
                    ValidationGroup="obligatorio"></asp:RequiredFieldValidator>
                <asp:CheckBox ID="chkFiltroPorDefecto" runat="server" Text="DFiltro por defecto"
                    AutoPostBack="false" />
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:RadioButton ID="rbTodosFormularios" runat="server" Text="DAplicar el filtro a todos los formularios"
                    GroupName="radioGroup1" CssClass="Etiqueta" AutoPostBack="true" />
            </td>
        </tr>
        <tr>
            <td style="width: 200px; overflow: visible">
                <asp:RadioButton ID="rbUnFormulario" runat="server" Text="DAplicar el filtro al formulario"
                    GroupName="radioGroup1" CssClass="Etiqueta" Style="width: auto" AutoPostBack="true" />
            </td>
            <td colspan="2">
                <ig:WebDropDown ID="wddFormularios" runat="server" Width="400px" EnableAutoCompleteFirstMatch="false"
                    DropDownContainerWidth="400px" DropDownContainerHeight="80px" DropDownContainerMaxHeight="200px" AutoPostBack="true" BackColor="White">
                </ig:WebDropDown>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            </td>
            <td align="left" width="80%">
                <asp:UpdatePanel ID="upBotones" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table style="width: 100%" cellpadding="5" cellspacing="0" border="0">
                            <tr>
                                <td style="width: 90%">
                                </td>
                                <td>
                                    <fsn:FSNButton ID="btnAceptarFiltro" runat="server" Visible="false" ValidationGroup="obligatorio"></fsn:FSNButton>
                                </td>
                                <td>
                                    <fsn:FSNButton ID="btnCancelarFiltro" runat="server" Visible="false"></fsn:FSNButton>
                                </td>
                                <td>
                                    <fsn:FSNButton ID="btnEliminarFiltro" runat="server" Visible="false"></fsn:FSNButton>
                                </td>
                                <td>
                                    <fsn:FSNButton ID="btnVolver" runat="server" Visible="false"></fsn:FSNButton>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <asp:Panel ID="pConfiguracionFiltro" style="display:none" runat="server">
                <ajx:CollapsiblePanelExtender ID="CollapsiblePanelExtender0" runat="server" CollapseControlID="pnlBusqueda"
                    ExpandControlID="pnlBusqueda" ImageControlID="imgExpandir0" SuppressPostBack="True"
                    TargetControlID="pnlControlBusqueda" SkinID="FondoRojo">
                </ajx:CollapsiblePanelExtender>
                <asp:Panel ID="pnlBusqueda" runat="server" CssClass="PanelCabecera" Style="cursor: pointer;
                    width: 100%; height: 20px" Font-Bold="True">
                    <table border="0">
                        <tr>
                            <td>
                                <asp:Image ID="imgExpandir0" runat="server" SkinID="Contraer" />
                            </td>
                            <td style="vertical-align: top">
                                <asp:Label ID="lblTituloBusqueda" runat="server" Text="DSeleccione los criterios de búsqueda:"
                                    Font-Bold="True" ForeColor="White"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlControlBusqueda" runat="server" CssClass="Rectangulo" Width="99%">
                    <asp:UpdatePanel ID="upBusquedaAvanzada" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <fieldset>
                                                    <legend style="vertical-align: top">
                                                        <asp:Label ID="lblProceso" runat="server" Text="dProceso"></asp:Label></legend>
                                                    <table width="100%" cellpadding="0" border="0">
                                                        <tr>
                                                            <td style="padding-right: 10px">
                                                                <asp:Label ID="lblAño" runat="server" Text="DAño:" CssClass="Etiqueta"></asp:Label>
                                                            </td>
                                                            <td style="padding-right: 10px">
                                                                <ig:WebDropDown ID="ddlAnyo" runat="server" Width="80px" EnableClosingDropDownOnSelect="true"
                                                                    DropDownAnimationDuration="1" DropDownContainerWidth="80" EnableDropDownAsChild="false" AutoPostBack="true" BackColor="White">
                                                                    <ClientEvents ValueChanged="ValueChanged_wddCommodity" />
                                                                </ig:WebDropDown>
                                                            </td>
                                                            <td style="padding-right: 10px">
                                                                <asp:Label ID="lblCommodity" runat="server" Text="DCommodity:" CssClass="Etiqueta"></asp:Label>
                                                            </td>
                                                            <td style="padding-right: 10px">
                                                                <ig:WebDropDown ID="ddlCommodity" runat="server" Width="236px" EnableClosingDropDownOnSelect="true"
                                                                    DropDownAnimationDuration="1" DropDownContainerWidth="236" EnableDropDownAsChild="false"
                                                                    Height="22px" AutoPostBack="true" BackColor="White">
                                                                    <ClientEvents ValueChanged="ValueChanged_wddCommodity" />
                                                                </ig:WebDropDown>
                                                            </td>
                                                            <td style="padding-right: 10px">
                                                                <asp:Label ID="lblCodigo" runat="server" Text="DCodigo:" CssClass="Etiqueta"></asp:Label>
                                                            </td>
                                                            <td style="padding-right: 10px">
                                                                <ig:WebDropDown ID="ddlCodigo" runat="server" Width="80px" EnableClosingDropDownOnSelect="true"
                                                                    DropDownAnimationDuration="1" DropDownContainerWidth="80" EnableDropDownAsChild="false"
                                                                    AutoPostBack="true" BackColor="White" CurrentValue="">
                                                                </ig:WebDropDown>
                                                            </td>
                                                            <td style="padding-right: 10px">
                                                                <asp:Label ID="lblDenominacion" runat="server" Text="DDenominacion:" CssClass="Etiqueta"></asp:Label>
                                                            </td>
                                                            <td style="padding-right: 10px">
                                                                <ig:WebDropDown ID="ddlDenominacion" runat="server" Width="286px" EnableClosingDropDownOnSelect="true"
                                                                    DropDownAnimationDuration="1" DropDownContainerWidth="286" EnableDropDownAsChild="false"
                                                                    AutoPostBack="true" Height="22px" BackColor="White" CurrentValue="">
                                                                </ig:WebDropDown>
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="ImgBuscadorProcesos" runat="server" SkinID="BuscadorLupa" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td valign="middle" align="left">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                            
                            <table width="99%">
                                <tr>
                                    <td>
                                        <fspm:BusquedaAvanzadaCTR ID="Busqueda" runat="server"></fspm:BusquedaAvanzadaCTR>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <p>
                    <br />
                </p>
                <asp:UpdatePanel runat="server" ID="upCampos" UpdateMode="Conditional" ChildrenAsTriggers="false">
                <ContentTemplate>
                <ajx:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server" CollapseControlID="pnlCampos"
                    ExpandControlID="pnlCampos" ImageControlID="imgExpandir2" SuppressPostBack="True"
                    TargetControlID="pnlConfiguracionCampos" SkinID="FondoRojo"> 
                </ajx:CollapsiblePanelExtender>
                <asp:Panel ID="pnlCampos" runat="server" CssClass="PanelCabecera" Style="cursor: pointer;
                    width: 100%; height: 20px" Font-Bold="True">
                    <table border="0">
                        <tr>
                            <td>
                                <asp:Image ID="imgExpandir2" runat="server" SkinID="Contraer" />
                            </td>
                            <td style="vertical-align: top">
                                <asp:Label ID="lblTituloCampos" runat="server" Text="DSeleccione los campos a visualizar en el filtro:"
                                    Font-Bold="True" ForeColor="White"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlConfiguracionCampos" runat="server" CssClass="Rectangulo" Style="width: 100%; height: 100%; float:left">
                    <br />
                    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="height:100%">
                        <tr style="padding-bottom: 5px">
                            <td>
                                <asp:Label ID="lblTituloCamposGenerales" runat="server" Text="DSeleccione los campos generales a visualizar en el filtro:"
                                    CssClass="Rotulo"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblTituloCamposFormulario" runat="server" Text="DSeleccione los campos del contrato a visualizar en el filtro:"
                                    CssClass="Rotulo"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="width: 50%; padding-right: 15px">
                                <table width="100%" style="border: solid 1px #B2B2B2" cellpadding="0" cellspacing="0">
                                    <asp:Repeater ID="rpCamposGenerales" runat="server">
                                        <HeaderTemplate>
                                            <tr style="height: 20px">
                                                <td style="width: 40%; padding-left: 3px;">
                                                    <asp:Label ID="lblCampoGeneral" runat="server" Text='<%# Textos(12)%>'></asp:Label>
                                                </td>
                                                <td align="center" style="width: 10%; border-right: solid 1px #B2B2B2; border-left: solid 1px #B2B2B2;">
                                                    <asp:Label ID="lblVisible1" runat="server" Text='<%# Textos(13)%>'></asp:Label>
                                                </td>
                                                <td style="width: 50%; padding-left: 3px">
                                                    <asp:Label ID="lblNombrePersonalizado1" runat="server" Text='<%# Textos(14)%>'></asp:Label>
                                                </td>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr style="background-color: White">
                                                <td style="width: 40%; padding-left: 3px; border-top: solid 1px #B2B2B2">
                                                    <asp:Label ID="CampoGeneral_lblCampoGeneral" runat="server" Text='<%#Container.DataItem("NOMBRE")%>'></asp:Label>
                                                </td>
                                                <td align="center" style="width: 10%; border-right: solid 1px #B2B2B2; border-left: solid 1px #B2B2B2;
                                                    border-top: solid 1px #B2B2B2">
                                                    <asp:CheckBox ID="CampoGeneral_Visible" runat="server" AutoPostBack="true" />
                                                </td>
                                                <td style="width: 50%; padding-left: 3px; border-top: solid 1px #B2B2B2; padding-top: 2px;
                                                    padding-bottom: 2px">
                                                    <asp:TextBox ID="CampoGeneral_NombrePersonalizado" runat="server" Width="98%" AutoPostBack="true"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </td>
                            <td valign="top" style="width: 50%; padding-right: 15px">
                                <asp:Repeater ID="rpGrupos" runat="server">
                                    <ItemTemplate>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-bottom: solid 1px #B2B2B2;
                                            border-left: solid 1px #B2B2B2; border-right: solid 1px #B2B2B2; border-top: solid 1px #B2B2B2">
                                            <tr>
                                                <td>
                                                    <asp:Panel ID="pnlGrupo" runat="server" Style="width: 100%; padding-left: 5px; cursor: pointer;
                                                        height: 20px" Font-Bold="True">
                                                        <table border="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:Image ID="imgExpandir" runat="server" SkinID="ContraerFondoTrans" />
                                                                </td>
                                                                <td style="vertical-align: top">
                                                                    <b>
                                                                        <%#DataBinder.Eval(Container.DataItem, "NOMBRE")%></b>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Panel ID="pnlCampos" runat="server" Style="width: 100%; height: 25px;">
                                            <asp:Repeater ID="rpCamposFormulario" runat="server" DataSource='<%#Container.DataItem.Row.GetChildRows("REL_GRUPO_CAMPOS")%>'>
                                                <HeaderTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: White;
                                                        border-right: solid 1px #B2B2B2; border-left: solid 1px #B2B2B2; border-bottom: solid 1px #B2B2B2;">
                                                        <tr style="background-color: #FAFAFA; height: 20px">
                                                            <td style="width: 40%; padding-left: 3px;">
                                                                <asp:Label ID="lblCampoFormulario" runat="server" Text='<%# Textos(15)%>'></asp:Label>
                                                            </td>
                                                            <td align="center" style="width: 10%; border-right: solid 1px #B2B2B2; border-left: solid 1px #B2B2B2;">
                                                                <asp:Label ID="lblVisible2" runat="server" Text='<%# Textos(13)%>'></asp:Label>
                                                            </td>
                                                            <td style="width: 50%; padding-left: 3px">
                                                                <asp:Label ID="lblNombrePersonalizado2" runat="server" Text='<%# Textos(14)%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td style="width: 40%; padding-left: 3px; border-top: solid 1px #B2B2B2">
                                                            <asp:Label ID="CampoFormulario_lblCampoGeneral" runat="server" Text='<%#Container.DataItem("NOMBRE")%>'></asp:Label>
                                                        </td>
                                                        <td align="center" style="width: 10%; border-right: solid 1px #B2B2B2; border-left: solid 1px #B2B2B2;
                                                            border-top: solid 1px #B2B2B2">
                                                            <asp:HiddenField ID="CampoFormularioID" Value='<%#Container.DataItem("ID")%>' runat="server" />
                                                            <asp:CheckBox ID="CampoFormulario_Visible" runat="server" Checked='<%#Container.DataItem("VISIBLE")%>'
                                                                AutoPostBack="true" />
                                                        </td>
                                                        <td style="width: 50%; padding-left: 3px; border-top: solid 1px #B2B2B2; padding-top: 2px;
                                                            padding-bottom: 2px">
                                                            <asp:TextBox ID="CampoFormulario_NombrePersonalizado" runat="server" Width="98%"
                                                                AutoPostBack="true" Text='<%#Container.DataItem("NOMBRE_PER")%>'></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </asp:Panel>
                                        <ajx:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" CollapseControlID="pnlGrupo"
                                            ExpandControlID="pnlGrupo" SkinID="FondoTrans2" ImageControlID="imgExpandir"
                                            SuppressPostBack="True" TargetControlID="pnlCampos">
                                        </ajx:CollapsiblePanelExtender>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                 </asp:Panel>
                 </ContentTemplate>
                </asp:UpdatePanel>
                    <br />
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <asp:Label ID="lblTituloGridConfiguracion" runat="server" Text="DPara modificar el orden de los campos desplace las columnas."
                                    CssClass="Rotulo"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                             <asp:UpdatePanel runat="server" ID="upWHG" UpdateMode="Conditional" >
                             <ContentTemplate>
                                <ig:WebHierarchicalDataGrid runat="server" ID="whgConfigurarFiltros" AutoGenerateBands="false"
					                AutoGenerateColumns="false"  Width="100%" Height="200px" EnableAjax="true" 
					                EnableAjaxViewState="false" EnableDataViewState="true" ExpansionColumnCss="hidExpColumn">
					            <Behaviors>
						            <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
						            <ig:ColumnMoving Enabled="true"></ig:ColumnMoving>	
					            </Behaviors>
                                <Columns>
                                    <ig:TemplateDataField Key="TEMPLATEAPROBAR" VisibleIndex="0"  Width="70px" Hidden="false">                   
                                    <HeaderTemplate>                           
                                        <fsn:FSNButton ID="btnAprobar2" runat="server" Alineacion="Left" Text="dAprobar" OnPreRender="btnAprobar_OnPreRender"></fsn:FSNButton>                        
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div style="text-align: center;">
                                            <asp:CheckBox ID="cbAprobar" runat="server" />
                                         </div>
                                    </ItemTemplate>
                                    </ig:TemplateDataField>
                                       
                                    <ig:TemplateDataField Key="TEMPLATERECHAZAR" VisibleIndex="1"  Width="80px" Hidden="false">
                                      <HeaderTemplate>
                                        <fsn:FSNButton ID="btnRechazar2" runat="server" Alineacion="Left" Text="dRechazar" OnPreRender="btnRechazar_OnPreRender"></fsn:FSNButton>
                                      </HeaderTemplate>
                                      <ItemTemplate>
                                        <div style="text-align: center;">
                                            <asp:CheckBox ID="cbRechazar" runat="server" />
                                        </div>
                                      </ItemTemplate>
                                     </ig:TemplateDataField>

                                </Columns>
				                </ig:WebHierarchicalDataGrid> 
                             </ContentTemplate>
                             </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
             </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="wddFormularios" EventName="SelectionChanged" />
        </Triggers>
    </asp:UpdatePanel>

    <script type="text/javascript" language="javascript">
        var ModalProgress = '<%= ModalProgress.ClientID %>';
    </script>
    
    <ajx:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
    <p>&nbsp;</p>
</asp:Content>
