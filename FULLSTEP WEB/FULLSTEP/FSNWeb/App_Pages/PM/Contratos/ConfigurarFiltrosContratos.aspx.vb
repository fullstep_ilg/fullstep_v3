﻿Imports Infragistics.Web.UI
Imports Infragistics.Web.UI.GridControls
Imports System.Web.UI
Imports Fullstep.FSNLibrary.TiposDeDatos

Partial Public Class ConfigurarFiltrosContratos
    Inherits FSNPage

    Private m_desdeGS As Boolean = False

    ''' <summary>
    ''' Pone el texto correspondiente en la etiqueta "Cargando..."
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: El prerender de la etiqueta. Tiempo máximo: 0sg.</remarks>
    Protected Sub LblProcesando_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, Label).Text = Textos(33)
    End Sub

    ''' <summary>
    ''' Se pone el texto en el idioma correspondiente en el botón Aprobar
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: Al dibujarse el botón. Tiempo máximo: 0sg</remarks>
    Protected Sub btnAprobar_OnPreRender(ByVal sender As Object, ByVal e As EventArgs)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltrosContratos
        CType(sender, FSNWebControls.FSNButton).Text = Textos(43)
    End Sub

    ''' <summary>
    ''' Se pone el texto en el idioma correspondiente en el botón Rechazar
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: Al dibujarse el botón. Tiempo máximo: 0sg</remarks>
    Protected Sub btnRechazar_OnPreRender(ByVal sender As Object, ByVal e As EventArgs)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltrosContratos
        CType(sender, FSNWebControls.FSNButton).Text = Textos(44)
    End Sub

    ''' <summary>
    ''' Carga de la pagina
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>    
    ''' <remarks>Llamada desde: Sistema; Tiempo máximo:1seg.</remarks> 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltrosContratos
        If Not Me.IsPostBack Then
            CargarTextos()

            FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/funnel_icon.gif"
            If m_desdeGS Then
                FSNPageHeader.VisibleBotonVolver = False
                btnVolver.Visible = True
            Else
                FSNPageHeader.VisibleBotonVolver = True
            End If


            CargarCommodities()
            CargarAnyos()
            InicializarParametrosBusqueda()

            If Request("IDFiltroConfigurando") <> "" Then
                Dim f As FSNServer.FiltroContrato = FSNServer.Get_Object(GetType(FSNServer.FiltroContrato))
                f.IDFiltroUsuario = Request("IDFiltroConfigurando")
                f.Persona = Usuario.CodPersona
                f.Idioma = Usuario.Idioma
                f.LoadFiltro()
                If f.IsLoaded Then
                    Session("IDFiltroContrato") = f.IDFiltro
                    Session("IDFiltroContratoUsuario") = f.IDFiltroUsuario
                    Session("NombreFiltroContrato") = Request.QueryString("NombreTabla")
                    Session("FiltroContrato") = f

                    FSNPageHeader.TituloCabecera = f.NombreFiltro
                    txtNombreFiltro.Text = f.NombreFiltro
                    chkFiltroPorDefecto.Checked = f.PorDefecto

                    CargarBusquedaAvanzada()

                    CargarConfiguracionCamposGenerales()

                    If Not f.CamposFormulario Is Nothing AndAlso f.CamposFormulario.Tables.Count > 0 AndAlso f.CamposFormulario.Tables(0).Rows.Count > 0 Then
                        Dim camposFormulario As DataSet = f.CamposFormulario
                        rpGrupos.DataSource = camposFormulario.Tables(0)
                        rpGrupos.DataBind()
                    Else
                        lblTituloCamposFormulario.Visible = False
                        rpGrupos.Visible = False
                    End If

                    Dim ds As DataSet = f.LoadConfiguracionInstancia(f.IDFiltro, Idioma)

                    If ds.Tables(0).Rows.Count = 0 Then
                        ds.Tables(0).Rows.Add()
                    End If

                    With whgConfigurarFiltros
                        .Rows.Clear()
                        .GridView.ClearDataSource()
                        CreacionEdicionColumnasGrid()
                        .DataSource = ds
                        .GridView.DataSource = .DataSource
                        '.DataMember = "ContratosFiltrados"
                        '.DataKeyFields = "ID"
                        .DataBind()

                    End With

                    pConfiguracionFiltro.Style("display") = "block"
                    btnEliminarFiltro.Visible = True
                    btnAceptarFiltro.Visible = True
                    btnCancelarFiltro.Visible = True
                    btnVolver.Visible = False

                    rbTodosFormularios.Visible = False
                    rbUnFormulario.Visible = False
                    wddFormularios.Visible = False

                    btnEliminarFiltro.Attributes.Add("onClick", "return ConfirmarEliminar('" & JSText(Textos(42)) & "');")
                Else
                    Response.Redirect("..\contratos\VisorContratos.aspx")
                End If
            Else
                rbUnFormulario.Checked = True
                CargarFiltrosDisponibles(TiposDeDatos.TipoDeSolicitud.Contrato)
                Session("IDFiltroContrato") = ""
                Session("IDFiltroContratoUsuario") = ""
                Session("FiltroContrato") = Nothing
            End If

            FSNPageHeader.OnClientClickVolver = "window.location='" & ConfigurationManager.AppSettings("rutaPM2008") & "contratos/VisorContratos.aspx';return false;"
            btnVolver.OnClientClick = "window.location='" & ConfigurationManager.AppSettings("rutaPM2008") & "contratos/VisorContratos.aspx';return false;"

            ImgBuscadorProcesos.Attributes.Add("onClick", "AbrirBuscadorProcesos(); return false;")
        End If

        Accion.Value = ""
    End Sub

    '''' <summary>
    '''' Carga la lista con los formularios disponibles para el usuario
    '''' </summary>
    '''' <remarks>Page_Load()</remarks>
    Private Sub CargarFiltrosDisponibles(ByVal iTipoSolicitud As Byte)
        Dim cFiltros As FSNServer.FiltrosContrato
        cFiltros = FSNServer.Get_Object(GetType(FSNServer.FiltrosContrato))
        wddFormularios.DataSource = cFiltros.LoadDisponibles(Usuario.CodPersona)
        wddFormularios.TextField = "DEN"
        wddFormularios.ValueField = "ID"
        wddFormularios.DataBind()
    End Sub

    ''' <summary>
    ''' Inicializa un dataset con los campos generales y se almacena en cache
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load. Tiempo máximo: 0sg.</remarks>
    Private Function ObtenerEstructuraCamposGenerales() As DataSet
        Dim campos As DataSet
        If Not HttpContext.Current.Cache("CAMPOSGEN_" & Idioma) Is Nothing Then
            campos = CType(HttpContext.Current.Cache("CAMPOSGEN_" & Idioma), DataSet)
        Else
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltrosContratos

            campos = New DataSet
            Dim dt As DataTable = campos.Tables.Add("CAMPOS")
            dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("CAMPO_ID", System.Type.GetType("System.String"))
            dt.Columns.Add("NOMBRE", System.Type.GetType("System.String"))
            dt.Columns.Add("TAMANYO", System.Type.GetType("System.Int32"))
            dt.Rows.Add(New Object() {1, "CODIGO", Textos(38), 88})
            dt.Rows.Add(New Object() {2, "TIPO", Textos(21), 100})
            dt.Rows.Add(New Object() {3, "CODPROVE", Textos(22), 165})
            dt.Rows.Add(New Object() {4, "DENPROVE", Textos(23), 165})
            dt.Rows.Add(New Object() {5, "CONTACTO", Textos(24), 100})
            dt.Rows.Add(New Object() {6, "PETICIONARIO", Textos(25), 100})
            dt.Rows.Add(New Object() {7, "FECHAALTA", Textos(26), 95})
            dt.Rows.Add(New Object() {8, "MONEDA", Textos(27), 88})
            dt.Rows.Add(New Object() {9, "FECHAINICIO", Textos(28), 95})
            dt.Rows.Add(New Object() {10, "FECHAEXPIRACION", Textos(29), 125})
            dt.Rows.Add(New Object() {11, "ESTADO", Textos(30), 70})
            dt.Rows.Add(New Object() {12, "PROCESOCOMPRA", Textos(31), 125})
            dt.Rows.Add(New Object() {13, "IMPORTE", Textos(32), 88})
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorFacturas
            dt.Rows.Add(New Object() {14, "SITUACIONACTUAL", Textos(39), 100})
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltrosContratos
            dt.Rows.Add(New Object() {15, "ALERTA_EXPIRACION", Textos(45), 70})
            dt.Rows.Add(New Object() {16, "NOTIFICADOS", Textos(46), 125})
            dt.Rows.Add(New Object() {17, "FECHA_ULT_NOTIFICACION", Textos(47), 88})

            Me.InsertarEnCache("CAMPOSGEN_" & Idioma, campos)
        End If
        Return campos
    End Function

    ''' <summary>
    ''' Carga los textos de los controles con el idioma del usuario.
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load. Tiempo máximo: 0sg.</remarks>
    Private Sub CargarTextos()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltrosContratos

        Dim cParametros As FSNServer.Parametros
        cParametros = FSNServer.Get_Object(GetType(FSNServer.Parametros))
        Dim dsParametros As Data.DataSet = cParametros.LoadPMData()
        Dim dtGmn1() As Data.DataRow = dsParametros.Tables(2).Select("ID=9 AND IDI='" & FSNUser.Idioma.ToString & "'")

        FSNPageHeader.TituloCabecera = Textos(0)
        FSNPageHeader.TextoBotonVolver = Textos(1)
        btnVolver.Text = Textos(1)
        lblNombreFiltro.Text = String.Format("{0}:", Textos(2))
        chkFiltroPorDefecto.Text = Textos(3)
        rbTodosFormularios.Text = Textos(4)
        rbUnFormulario.Text = Textos(5)
        btnEliminarFiltro.Text = Textos(41)
        btnAceptarFiltro.Text = Textos(6)
        btnCancelarFiltro.Text = Textos(7)
        lblTituloBusqueda.Text = Textos(8)
        lblProceso.Text = Textos(35)
        lblAño.Text = String.Format("{0}:", Textos(36))
        lblCommodity.Text = String.Format("{0}:", dtGmn1(0)("DEN"))
        lblCodigo.Text = String.Format("{0}:", Textos(38))
        lblDenominacion.Text = String.Format("{0}:", Textos(39))

        lblTituloCampos.Text = Textos(9)
        lblTituloCamposGenerales.Text = String.Format("{0}:", Textos(10))
        lblTituloCamposFormulario.Text = String.Format("{0}:", Textos(11))
        lblTituloGridConfiguracion.Text = Textos(18)
        rfvNombreFiltro.ErrorMessage = Textos(34)
    End Sub

    ''' <summary>
    ''' Inicializar los valores de los parámetros de búsqueda
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load</remarks>
    Private Sub InicializarParametrosBusqueda()
        ddlAnyo.SelectedItemIndex = ddlAnyo.Items.IndexOf(ddlAnyo.Items.FindItemByValue(Year(Today).ToString))
        ddlCommodity.ClearSelection()
        ddlCodigo.ClearSelection()
        ddlCodigo.Items.Clear()
        ddlCodigo.CurrentValue = ""
        ddlDenominacion.ClearSelection()
        ddlDenominacion.Items.Clear()
        ddlDenominacion.CurrentValue = ""
        Busqueda.ProveedorTexto = ""
        Busqueda.ProveedorHidden = ""
        Busqueda.ProveedorCIFHidden = ""
        Busqueda.Tipo = ""
        Busqueda.MaterialHidden = ""
        Busqueda.MaterialTexto = ""
        Busqueda.ArticuloHidden = ""
        Busqueda.ArticuloTexto = ""
        Busqueda.Peticionario = ""
        Busqueda.Descripcion = ""
        Busqueda.EmpresaHidden = ""
        Busqueda.EmpresaTexto = ""
        Busqueda.Estado = ""
        Busqueda.FechaInicio = Nothing
        Busqueda.FechaExpiracion = Nothing
        Busqueda.PalabraClave = ""
        Busqueda.CentroCoste = ""

        Busqueda.AlertaExpiracion = 0
        Busqueda.AlertaPeriodo = 1
        Busqueda.AlertaNotificado = String.Empty

        If Me.Acceso.gbAccesoFSSM Then
            Busqueda.PartidasPres = Nothing
        End If
        upBusquedaAvanzada.Update()
    End Sub



    ''' <summary>
    ''' Carga los años en el combo de años del proceso. Desde el actual menos 10 hasta el actual más 10.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarAnyos()
        Dim iAnyoActual As Integer
        Dim iInd As Integer
        Dim ddItem As Infragistics.Web.UI.ListControls.DropDownItem
        iAnyoActual = Year(Now)
        ddlAnyo.Items.Clear()
        For iInd = iAnyoActual - 10 To iAnyoActual + 10
            ddItem = New Infragistics.Web.UI.ListControls.DropDownItem
            ddItem.Value = iInd
            ddItem.Text = iInd.ToString
            ddlAnyo.Items.Add(ddItem)
        Next
        ddlAnyo.SelectedItemIndex = ddlAnyo.Items.IndexOf(ddlAnyo.Items.FindItemByValue(iAnyoActual.ToString))
    End Sub

    ''' <summary>
    ''' Carga los materiales de nivel 1
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarCommodities()
        Dim oCommodities As FSNServer.GrupoMatNivel1 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel1))

        If FSNUser.Pyme > 0 Then
            Dim oGruposMaterial As FSNServer.GruposMaterial
            oGruposMaterial = FSNServer.Get_Object(GetType(FSNServer.GruposMaterial))
            oCommodities.LoadData(oGruposMaterial.DevolverGMN1_PYME(FSNUser.Pyme), , , , FSNUser.Idioma)
        Else
            oCommodities.LoadData(, , , , FSNUser.Idioma)
        End If

        ddlCommodity.Items.Clear()
        Dim oItem As ListControls.DropDownItem
        oItem = New ListControls.DropDownItem("", "")
        ddlCommodity.Items.Add(oItem)
        For Each oRow As DataRow In oCommodities.Data.Tables(0).Rows
            oItem = New ListControls.DropDownItem
            oItem.Value = DBNullToStr(oRow.Item("COD"))
            If IsDBNull(oRow.Item("COD")) Then
                oItem.Text = ""
            Else
                oItem.Text = oRow.Item("DEN" & "_" & Idioma)
            End If

            ddlCommodity.Items.Add(oItem)
        Next
        ddlCommodity.SelectedValue = ""
    End Sub

    ''' <summary>
    ''' Cuando se selecciona un formulario, se cargan los campos y se chequean los campos generales marcados por defecto.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: Al cambiar el dropdownlist con los formularios. Tiempo máximo: 1 sg.</remarks>
    Private Sub wddFormularios_SelectionChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownSelectionChangedEventArgs) Handles wddFormularios.SelectionChanged
        If rbUnFormulario.Checked Then
            If IsNumeric(wddFormularios.SelectedValue) Then
                Dim selectedID As Integer = CInt(wddFormularios.SelectedValue)
                If Session("IDFiltroContrato") <> selectedID.ToString() Then
                    If selectedID > 0 Then
                        InicializarParametrosBusqueda()
                        Busqueda.FilaProveedorVisible = True
                        Busqueda.CeldaLabelTipoVisible = False
                        Busqueda.CeldawddTipoVisible = False
                        upBusquedaAvanzada.Update()

                        Session("IDFiltroContrato") = selectedID

                        'campos generales
                        CargarConfiguracionPorDefectoCamposGenerales()

                        Dim f As FSNServer.FiltroContrato = FSNServer.Get_Object(GetType(FSNServer.FiltroContrato))

                        'campos del formulario
                        lblTituloCamposFormulario.Visible = True
                        rpGrupos.Visible = True
                        Dim camposFormulario As DataSet = f.LoadCampos(Usuario.CodPersona, selectedID, Idioma)
                        rpGrupos.DataSource = camposFormulario.Tables(0)
                        rpGrupos.DataBind()

                        'grid de configuracion
                        Dim ds As DataSet = f.LoadConfiguracionInstancia(selectedID, Idioma)

                        If ds.Tables(0).Rows.Count = 0 Then
                            ds.Tables(0).Rows.Add()
                        End If

                        With whgConfigurarFiltros
                            .Rows.Clear()
                            .GridView.ClearDataSource()
                            Dim contcolumn = .Columns.Count - 1
                            For i = 2 To contcolumn
                                .Columns.RemoveAt(2)
                            Next
                            Dim contcolgr = .GridView.Columns.Count - 1
                            For i = 2 To contcolgr
                                .GridView.Columns.RemoveAt(2)
                            Next
                            CreacionEdicionColumnasGrid()
                            .DataSource = ds
                            .GridView.DataSource = .DataSource
                            '.DataMember = "ContratosFiltrados"
                            '.DataKeyFields = "ID"
                            .DataBind()
                        End With

                        upWHG.Update()

                        pConfiguracionFiltro.Style("display") = "Block"
                        btnAceptarFiltro.Visible = True
                        btnCancelarFiltro.Visible = True
                        btnVolver.Visible = False

                        upCampos.Update()
                    End If
                End If
            Else
                pConfiguracionFiltro.Style("display") = "none"
                If m_desdeGS Then
                    btnVolver.Visible = True
                End If
                btnAceptarFiltro.Visible = False
                btnCancelarFiltro.Visible = False
            End If

            upBotones.Update()
        End If
    End Sub

    ''' <summary>
    ''' Carga la configuracion por defecto para los campos generales del contrato
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load</remarks>
    Private Sub CargarConfiguracionPorDefectoCamposGenerales()
        Dim camposGenerales As DataSet = ObtenerEstructuraCamposGenerales()

        rpCamposGenerales.DataSource = camposGenerales
        rpCamposGenerales.DataBind()

        Dim chkCampoGeneral As CheckBox
        Dim txtNombre As TextBox
        For i = 0 To rpCamposGenerales.Items.Count - 1
            chkCampoGeneral = CType(rpCamposGenerales.Items(i).FindControl("CampoGeneral_Visible"), CheckBox)
            chkCampoGeneral.Attributes.Add("CampoGeneralID", camposGenerales.Tables("CAMPOS").Rows(i)("CAMPO_ID"))
            chkCampoGeneral.Checked = True
            txtNombre = CType(rpCamposGenerales.Items(i).FindControl("CampoGeneral_NombrePersonalizado"), TextBox)
            txtNombre.Attributes.Add("CampoGeneralID", camposGenerales.Tables("CAMPOS").Rows(i)("CAMPO_ID"))
        Next
    End Sub

    ''' <summary>
    ''' Carga los campos generales del filtro especificado
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load.</remarks>
    Private Sub CargarConfiguracionCamposGenerales()
        If Not Session("FiltroContrato") Is Nothing Then
            Dim f As FSNServer.FiltroContrato = CType(Session("FiltroContrato"), FSNServer.FiltroContrato)
            If Not f.CamposGenerales Is Nothing Then
                Dim view As New DataView(f.CamposGenerales)
                view.Sort = "CAMPO"

                Dim camposGenerales As DataSet = ObtenerEstructuraCamposGenerales()

                rpCamposGenerales.DataSource = camposGenerales
                rpCamposGenerales.DataBind()

                Dim chkCampoGeneral As CheckBox
                Dim txtNombre As TextBox
                For i = 0 To rpCamposGenerales.Items.Count - 1
                    chkCampoGeneral = CType(rpCamposGenerales.Items(i).FindControl("CampoGeneral_Visible"), CheckBox)
                    chkCampoGeneral.Attributes.Add("CampoGeneralID", camposGenerales.Tables("CAMPOS").Rows(i)("CAMPO_ID"))
                    txtNombre = CType(rpCamposGenerales.Items(i).FindControl("CampoGeneral_NombrePersonalizado"), TextBox)
                    txtNombre.Attributes.Add("CampoGeneralID", camposGenerales.Tables("CAMPOS").Rows(i)("CAMPO_ID"))
                    Dim index As Integer = view.Find(camposGenerales.Tables("CAMPOS").Rows(i)("ID"))
                    If index >= 0 Then
                        chkCampoGeneral.Checked = view(index)("VISIBLE")
                        If chkCampoGeneral.Checked = True And UCase(DBNullToStr(view(index)("NOMBRE"))) <> UCase(DBNullToStr(camposGenerales.Tables(0).Rows(i).Item("NOMBRE"))) Then
                            txtNombre.Text = DBNullToStr(view(index)("NOMBRE"))
                        Else
                            txtNombre.Text = ""
                        End If
                    End If
                Next
            End If
        End If
    End Sub

    ''' <summary>
    ''' Se pueden cambiar el orden de las columnas en la grid. Se pueden cambiar todos los campos menos las dos
    ''' primeras columnas que son las de Aprobar y Rechazar. Siempre tienen que ser la posición 1 y 2.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>LLamada desde: Al pinchar en una columna y arrastrarla. Tiempo máximo: 0 sg.</remarks>
    Private Sub whgConfigurarFiltros_ColumnMoved(sender As Object, e As Infragistics.Web.UI.GridControls.ColumnMovingEventArgs) Handles whgConfigurarFiltros.ColumnMoved
        If e.Column.VisibleIndex < 2 Then
            e.Column.VisibleIndex = e.PreviousVisibleIndex
        End If
    End Sub

    ''' <summary>
    ''' Actualiza el grid de configuración de campos. 
    ''' </summary>
    ''' <remarks>Llamada desde:ConfigurarFiltrosContratos_LoadComplete; Tiempo máximo:0,4seg.</remarks>
    Private Sub ActualizarGrid()
        Dim index, indexCampos As Integer
        Dim chkCampoVisible As CheckBox
        Dim txtCampoNombre As TextBox
        Dim key As String
        'Recorrer CamposGenerales
        For index = 0 To rpCamposGenerales.Items.Count - 1
            chkCampoVisible = CType(rpCamposGenerales.Items(index).FindControl("CampoGeneral_Visible"), CheckBox)
            txtCampoNombre = CType(rpCamposGenerales.Items(index).FindControl("CampoGeneral_NombrePersonalizado"), TextBox)
            key = chkCampoVisible.Attributes("CampoGeneralID")
            With whgConfigurarFiltros.GridView.Columns(key)
                .Hidden = Not chkCampoVisible.Checked
                If chkCampoVisible.Checked = True Then
                    .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                    .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                End If
                If txtCampoNombre.Text <> "" Then
                    .Header.Text = txtCampoNombre.Text
                Else
                    .Header.Text = CType(rpCamposGenerales.Items(index).FindControl("CampoGeneral_lblCampoGeneral"), Label).Text
                End If
            End With
        Next

        Dim rpCampos As Repeater
        Dim hid As HiddenField
        'Recorrer CamposFormulario
        For index = 0 To rpGrupos.Items.Count - 1
            rpCampos = (CType(rpGrupos.Items(index).FindControl("rpCamposFormulario"), Repeater))
            For indexCampos = 0 To rpCampos.Items.Count - 1
                chkCampoVisible = CType(rpCampos.Items(indexCampos).FindControl("CampoFormulario_Visible"), CheckBox)
                txtCampoNombre = CType(rpCampos.Items(indexCampos).FindControl("CampoFormulario_NombrePersonalizado"), TextBox)
                hid = CType(rpCampos.Items(indexCampos).FindControl("CampoFormularioID"), HiddenField)
                key = hid.Value
                With whgConfigurarFiltros.GridView.Columns
                    If .Item("C_" + key) IsNot Nothing Then
                        .FromKey("C_" + key).Hidden = Not chkCampoVisible.Checked
                        If chkCampoVisible.Checked = True Then
                            .Item("C_" + key).CssClass = .Item("C_" + key).CssClass.Replace("hiddenElement", String.Empty)
                            .Item("C_" + key).Header.CssClass = .Item("C_" + key).Header.CssClass.Replace("hiddenElement", String.Empty)
                        End If
                        If txtCampoNombre.Text <> "" Then
                            .Item("C_" + key).Header.Text = txtCampoNombre.Text
                        Else
                            .Item("C_" + key).Header.Text = CType(rpCampos.Items(indexCampos).FindControl("CampoFormulario_lblCampoGeneral"), Label).Text
                        End If
                    End If
                End With
            Next
        Next
        upWHG.Update()
    End Sub

    ''' <summary>
    ''' Evento que salta al cargarse completamente la pagina. La variable Accion me indica que evento a pulsado el usuario.
    ''' </summary>
    ''' <param name="sender">las del evento</param>
    ''' <param name="e">las del evento</param>
    ''' <remarks>Al no poder programar el evento del check del repeater este evento nos sirve para ejecutar código dependiendo del valor
    ''' almacenado en la variable Accion.</remarks>        
    Private Sub ConfigurarFiltrosContratos_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Me.IsPostBack Then
            If Accion.Value = "" Then
                ActualizarGrid()
            End If
        End If
        Accion.Value = ""

    End Sub

    ''' <summary>
    ''' Al pulsar el botón Cancelar se cierra la configuración de filtros sin hacer ningun cambio y se muestra la página del visor de contratos
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelarFiltro.Click
        Dim sURL As String = "..\contratos\VisorContratos.aspx"
        If Request("IDFiltroConfigurando") <> "" Then
            Dim stringRequest As String = "FiltroUsuarioIdAnt=" & Session("IDFiltroContratoUsuario") & "&NombreTablaAnt=" & Session("NombreFiltroContrato") & "&FiltroIdAnt=" & Session("IDFiltroContrato")
            sURL = sURL & "?" & stringRequest
        End If
        Response.Redirect(sURL)
        Accion.Value = "Cancelar"
    End Sub

    ''' <summary>
    ''' Al pulsar el botón Aceptar, se guarda el nombre del filtro, los campos que ha marcado como visibles y el tamaño de los mismos,
    ''' y se muestra el visor de contratos con el filtro actualizado.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarFiltro.Click
        If Page.IsValid Then
            Dim f As FSNServer.FiltroContrato = FSNServer.Get_Object(GetType(FSNServer.FiltroContrato))
            If Not IsNumeric(Session("IDFiltroContratoUsuario")) Then
                If rbUnFormulario.Checked Then
                    f.IDFiltro = CInt(wddFormularios.SelectedValue)
                Else
                    'IDFiltro = 1 es el ID del filtro para todos los formularios
                    f.IDFiltro = 1
                End If
                f.IDFiltroUsuario = 0
            Else
                f.IDFiltro = CInt(Session("IDFiltroContrato"))
                f.IDFiltroUsuario = CInt(Session("IDFiltroContratoUsuario"))
            End If
            f.NombreFiltro = txtNombreFiltro.Text
            f.Persona = Usuario.CodPersona

            Dim ds As New DataSet
            Dim dt As DataTable = ds.Tables.Add("CAMPOSCONFIGURACION")
            dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("VISIBLE", System.Type.GetType("System.Int32"))
            dt.Columns.Add("POSICION", System.Type.GetType("System.Int32"))
            dt.Columns.Add("TAMANYO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("NOMBRE", System.Type.GetType("System.String"))
            dt.Columns.Add("CAMPO_GENERAL", System.Type.GetType("System.Int32"))

            With whgConfigurarFiltros.GridView.Columns
                For columnIndex As Integer = 0 To .Count - 1
                    If Left(.Item(columnIndex).Key, 2) = "C_" Then
                        Dim newRow As DataRow = dt.NewRow
                        newRow.Item("ID") = Mid(.Item(columnIndex).Key, 3)
                        newRow.Item("VISIBLE") = Not whgConfigurarFiltros.GridView.Columns.Item(columnIndex).Hidden
                        newRow.Item("POSICION") = whgConfigurarFiltros.GridView.Columns.Item(columnIndex).VisibleIndex
                        newRow.Item("TAMANYO") = whgConfigurarFiltros.GridView.Columns.Item(columnIndex).Width.Value

                        Dim nombre As String = ObtenerTextoPersonalizadoCampoFormulario(newRow.Item("ID"))
                        If Not String.IsNullOrEmpty(nombre) Then
                            newRow.Item("NOMBRE") = nombre
                        End If
                        newRow.Item("CAMPO_GENERAL") = 0
                        dt.Rows.Add(newRow)
                    Else
                        Dim index As Nullable(Of Integer)
                        index = Nothing
                        Select Case .Item(columnIndex).Key
                            Case "CODIGO"
                                index = ConfiguracionFiltrosContratos.CodigoContrato
                            Case "TIPO"
                                index = ConfiguracionFiltrosContratos.Tipo
                            Case "CODPROVE"
                                index = ConfiguracionFiltrosContratos.CodigoProveedor
                            Case "DENPROVE"
                                index = ConfiguracionFiltrosContratos.DenominacionProveedor
                            Case "CONTACTO"
                                index = ConfiguracionFiltrosContratos.Contacto
                            Case "PETICIONARIO"
                                index = ConfiguracionFiltrosContratos.Peticionario
                            Case "FECHAALTA"
                                index = ConfiguracionFiltrosContratos.FechaAlta
                            Case "MONEDA"
                                index = ConfiguracionFiltrosContratos.Moneda
                            Case "FECHAINICIO"
                                index = ConfiguracionFiltrosContratos.FechaInicio
                            Case "FECHAEXPIRACION"
                                index = ConfiguracionFiltrosContratos.FechaExpiracion
                            Case "SITUACIONACTUAL"
                                index = ConfiguracionFiltrosContratos.SituacionActual
                            Case "PROCESOCOMPRA"
                                index = ConfiguracionFiltrosContratos.ProcesoCompra
                            Case "IMPORTE"
                                index = ConfiguracionFiltrosContratos.Importe
                            Case "ESTADO"
                                index = ConfiguracionFiltrosContratos.Estado
                            Case "ALERTA_EXPIRACION"
                                index = ConfiguracionFiltrosContratos.AlertaExpiracion
                            Case "NOTIFICADOS"
                                index = ConfiguracionFiltrosContratos.Notificados
                            Case "FECHA_ULT_NOTIFICACION"
                                index = ConfiguracionFiltrosContratos.FechaUltNotificacion

                        End Select
                        If index.HasValue Then
                            Dim newRow As DataRow = dt.NewRow
                            newRow.Item("ID") = index
                            newRow.Item("VISIBLE") = Not whgConfigurarFiltros.GridView.Columns.Item(columnIndex).Hidden
                            newRow.Item("POSICION") = whgConfigurarFiltros.GridView.Columns.Item(columnIndex).VisibleIndex
                            newRow.Item("TAMANYO") = whgConfigurarFiltros.GridView.Columns.Item(columnIndex).Width.Value

                            Dim nombre As String = ObtenerTextoPersonalizadoCampoGeneral(.Item(columnIndex).Key)
                            If Not String.IsNullOrEmpty(nombre) Then
                                newRow.Item("NOMBRE") = nombre
                            Else
                                newRow.Item("NOMBRE") = whgConfigurarFiltros.GridView.Columns.Item(columnIndex).Header.Text
                            End If
                            newRow.Item("CAMPO_GENERAL") = 1
                            dt.Rows.Add(newRow)
                        End If
                    End If
                Next
            End With

            dt = ds.Tables.Add("CAMPOSBUSQUEDA")
            dt.Columns.Add("PROCE_ANYO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("PROCE_GMN1", System.Type.GetType("System.String"))
            dt.Columns.Add("PROCE_COD", System.Type.GetType("System.Int32"))
            dt.Columns.Add("PROCE_DEN", System.Type.GetType("System.String"))
            dt.Columns.Add("PROVE", System.Type.GetType("System.String"))
            dt.Columns.Add("GMN1", System.Type.GetType("System.String"))
            dt.Columns.Add("GMN2", System.Type.GetType("System.String"))
            dt.Columns.Add("GMN3", System.Type.GetType("System.String"))
            dt.Columns.Add("GMN4", System.Type.GetType("System.String"))
            dt.Columns.Add("ART", System.Type.GetType("System.String"))
            dt.Columns.Add("EMP", System.Type.GetType("System.String"))
            dt.Columns.Add("TIPO", System.Type.GetType("System.String"))
            dt.Columns.Add("DESCRIPCION", System.Type.GetType("System.String"))
            dt.Columns.Add("PALABRA_CLAVE", System.Type.GetType("System.String"))
            dt.Columns.Add("FEC_INI", System.Type.GetType("System.DateTime"))
            dt.Columns.Add("FEC_HASTA", System.Type.GetType("System.DateTime"))
            dt.Columns.Add("PETICIONARIO", System.Type.GetType("System.String"))
            dt.Columns.Add("ESTADO", System.Type.GetType("System.String"))
            dt.Columns.Add("CENTRO_COSTE", System.Type.GetType("System.String"))
            dt.Columns.Add("PARTIDA1", System.Type.GetType("System.String"))
            dt.Columns.Add("PARTIDA2", System.Type.GetType("System.String"))
            dt.Columns.Add("PARTIDA3", System.Type.GetType("System.String"))
            dt.Columns.Add("PARTIDA4", System.Type.GetType("System.String"))
            dt.Columns.Add("ALERTA_EXPIRACION", System.Type.GetType("System.Int32"))
            dt.Columns.Add("ALERTA_PERIODO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("ALERTA_NOTIFICADO", System.Type.GetType("System.String"))

            Dim arrMat(3) As String
            If Not String.IsNullOrEmpty(Busqueda.MaterialHidden) Then
                arrMat = Split(Busqueda.MaterialHidden, "-")
            End If

            Dim newRow1 As DataRow = dt.NewRow
            newRow1.Item("PROCE_ANYO") = CInt(ddlAnyo.SelectedValue)
            If Not String.IsNullOrEmpty(ddlCommodity.SelectedValue) Then
                newRow1.Item("PROCE_GMN1") = ddlCommodity.SelectedValue
            End If
            If IsNumeric(ddlCodigo.SelectedValue) Then
                newRow1.Item("PROCE_COD") = CInt(ddlCodigo.SelectedValue)
            End If
            If Not String.IsNullOrEmpty(ddlDenominacion.SelectedValue) Then
                newRow1.Item("PROCE_DEN") = ddlDenominacion.SelectedValue
            End If
            If Not String.IsNullOrEmpty(Busqueda.ProveedorHidden) Then
                newRow1.Item("PROVE") = Busqueda.ProveedorHidden
            End If
            newRow1.Item("TIPO") = Busqueda.Tipo
            If Not String.IsNullOrEmpty(arrMat(0)) Then
                newRow1.Item("GMN1") = arrMat(0)
            End If
            If arrMat.Length >= 2 AndAlso Not String.IsNullOrEmpty(arrMat(1)) Then
                newRow1.Item("GMN2") = arrMat(1)
            End If
            If arrMat.Length >= 3 AndAlso Not String.IsNullOrEmpty(arrMat(2)) Then
                newRow1.Item("GMN3") = arrMat(2)
            End If
            If arrMat.Length = 4 AndAlso Not String.IsNullOrEmpty(arrMat(3)) Then
                newRow1.Item("GMN4") = arrMat(3)
            End If
            If Not String.IsNullOrEmpty(Busqueda.ArticuloHidden) Then
                newRow1.Item("ART") = Busqueda.ArticuloHidden
            End If
            If Not String.IsNullOrEmpty(Busqueda.EmpresaHidden) Then
                newRow1.Item("EMP") = Busqueda.EmpresaHidden
            End If
            If Not String.IsNullOrEmpty(Busqueda.Estado) Then
                newRow1.Item("ESTADO") = Busqueda.Estado
            End If
            If Not String.IsNullOrEmpty(Busqueda.Descripcion) Then
                newRow1.Item("DESCRIPCION") = Busqueda.Descripcion
            End If
            If Not String.IsNullOrEmpty(Busqueda.Peticionario) Then
                newRow1.Item("PETICIONARIO") = Busqueda.Peticionario
            End If
            If Not String.IsNullOrEmpty(Busqueda.PalabraClave) Then
                newRow1.Item("PALABRA_CLAVE") = Busqueda.PalabraClave
            End If
            If (Busqueda.FechaInicio <> CDate(#12:00:00 AM#)) Then
                newRow1.Item("FEC_INI") = Busqueda.FechaInicio
            End If
            If (Busqueda.FechaExpiracion <> CDate(#12:00:00 AM#)) Then
                newRow1.Item("FEC_HASTA") = Busqueda.FechaExpiracion
            End If
            If Not String.IsNullOrEmpty(Busqueda.CentroCoste) Then
                newRow1.Item("CENTRO_COSTE") = Busqueda.CentroCoste
            End If
            If Me.Acceso.gbAccesoFSSM Then
                Dim sPartidasPres As String() = Busqueda.getPartidasPres()
                If sPartidasPres IsNot Nothing AndAlso sPartidasPres.Length > 0 Then
                    'En la tarea 3015 se ha definido que solo pueden guardarse 4 partidas (pres0). En caso de haber más, no se guardarán sus filtros
                    Dim NumPartidas = IIf(sPartidasPres.Length > 4, 4, sPartidasPres.Length)
                    For i = 1 To NumPartidas
                        newRow1.Item("PARTIDA" & i) = sPartidasPres(i - 1)
                    Next
                End If
            End If

            If Busqueda.AlertaExpiracion.ToString.Length > 0 And IsNumeric(Busqueda.AlertaExpiracion) Then
                newRow1.Item("ALERTA_EXPIRACION") = Busqueda.AlertaExpiracion
                If (Busqueda.AlertaPeriodo <> 0) Then
                    newRow1.Item("ALERTA_PERIODO") = Busqueda.AlertaPeriodo
                End If
            End If

            If Busqueda.AlertaNotificado.Length > 0 Then
                newRow1.Item("ALERTA_NOTIFICADO") = Busqueda.AlertaNotificado
            End If

            dt.Rows.Add(newRow1)

            f.PorDefecto = chkFiltroPorDefecto.Checked
            f.CamposConfigurados = ds
            f.GuardarConfiguracion()

            Dim sURL As String = "..\contratos\VisorContratos.aspx"
            Dim stringRequest As String
            If Request("IDFiltroConfigurando") <> "" Then
                stringRequest = "FiltroUsuarioIdAnt=" & Session("IDFiltroContratoUsuario") & "&NombreTablaAnt=" & Session("NombreFiltroContrato") & "&FiltroIdAnt=" & Session("IDFiltroContrato") & "&CargarBusquedaAvanzada=1"
            Else
                stringRequest = "FiltroUsuarioIdAnt=" & f.IDFiltroUsuario & "&NombreTablaAnt=" & String.Format("CONTR_FILTRO{0}", f.IDFiltro) & "&FiltroIdAnt=" & f.IDFiltro & "&CargarBusquedaAvanzada=1"
            End If
            sURL = sURL & "?" & stringRequest

            Response.Redirect(sURL)

        End If

    End Sub

    Private Function ObtenerTextoPersonalizadoCampoGeneral(ByVal key As String) As String
        Dim resultText As String = Nothing
        For index As Integer = 0 To rpCamposGenerales.Items.Count - 1
            Dim txtCampoNombre As TextBox = CType(rpCamposGenerales.Items(index).FindControl("CampoGeneral_NombrePersonalizado"), TextBox)
            If key = txtCampoNombre.Attributes("CampoGeneralID") Then
                resultText = txtCampoNombre.Text
                Exit For
            End If
        Next
        Return resultText
    End Function

    Private Function ObtenerTextoPersonalizadoCampoFormulario(ByVal key As String) As String
        Dim resultText As String = Nothing
        Dim rpCampos As Repeater
        For index As Integer = 0 To rpGrupos.Items.Count - 1
            rpCampos = (CType(rpGrupos.Items(index).FindControl("rpCamposFormulario"), Repeater))
            For indexCampos = 0 To rpCampos.Items.Count - 1
                Dim txtCampoNombre As TextBox = CType(rpCampos.Items(indexCampos).FindControl("CampoFormulario_NombrePersonalizado"), TextBox)
                Dim hid As HiddenField = CType(rpCampos.Items(indexCampos).FindControl("CampoFormularioID"), HiddenField)
                If key = hid.Value Then
                    resultText = txtCampoNombre.Text
                    Exit For
                End If
            Next
        Next
        Return resultText
    End Function

    Private Sub ddlCodigo_SelectionChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownSelectionChangedEventArgs) Handles ddlCodigo.SelectionChanged
        Dim valor As String = ddlCodigo.SelectedItem.Value.ToString
        Dim oItem As ListControls.DropDownItem

        For Each oItem In ddlDenominacion.Items
            If oItem.Value = valor Then
                oItem.Selected = True
                ddlDenominacion.CurrentValue = oItem.Text
            Else
                oItem.Selected = False
            End If
        Next
    End Sub

    Private Sub ddlDenominacion_SelectionChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownSelectionChangedEventArgs) Handles ddlDenominacion.SelectionChanged
        Dim valor As String = ddlDenominacion.SelectedItem.Value.ToString
        Dim oItem As ListControls.DropDownItem

        For Each oItem In ddlCodigo.Items
            If oItem.Value = valor Then
                oItem.Selected = True
                ddlCodigo.CurrentValue = oItem.Text
            Else
                oItem.Selected = False
            End If
        Next
    End Sub

    ''' <summary>
    ''' Cargar combo GMN de procesos
    ''' </summary>
    ''' <param name="Anyo">Año proceso</param>
    ''' <param name="Commodity">gmn proceso</param>
    ''' <remarks>Llamada desde: CargarProcesoEnDropDowns   CargarProcesos; Tiempo máximo:0</remarks>
    Private Sub CargarProcesos(ByVal Anyo As Short, ByVal Commodity As String)
        Dim oProceso As FSNServer.Procesos
        Dim ds As DataSet

        oProceso = FSNServer.Get_Object(GetType(FSNServer.Procesos))
        ddlDenominacion.SelectedValue = ""
        ddlCodigo.SelectedValue = ""

        ddlDenominacion.Items.Clear()
        ddlCodigo.Items.Clear()
        If Commodity <> "" Then
            oProceso.CargarProcesos(Idioma, Anyo, Commodity)
            ds = oProceso.Data
            If ds.Tables(0).Rows.Count > 0 Then
                ddlCodigo.DataSource = ds
                ddlCodigo.ValueField = "COD"
                ddlCodigo.TextField = "COD"
                ddlCodigo.DataBind()
                ddlCodigo.EnableClosingDropDownOnSelect = False

                ddlDenominacion.DataSource = ds
                ddlDenominacion.ValueField = "COD"
                ddlDenominacion.TextField = "DEN"
                ddlDenominacion.DataBind()
                ddlDenominacion.EnableClosingDropDownOnSelect = False
            Else
                ddlCodigo.EnableClosingDropDownOnSelect = True
                ddlDenominacion.EnableClosingDropDownOnSelect = True
            End If
        End If

        oProceso = Nothing
    End Sub

    Private Sub rpGrupos_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles rpGrupos.DataBinding
        '1 Esta linea es necesaria para que se carguen correctamente los textos traducidos del control de campos del formulario
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltrosContratos
    End Sub

    Private Sub rpCamposGenerales_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles rpCamposGenerales.DataBinding
        '1 Esta linea es necesaria para que se carguen correctamente los textos traducidos del control de campos generales
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltrosContratos
    End Sub

    ''' <summary>
    ''' Cargar los valores de los criterios de busqueda avanzada para el filtro actual
    ''' </summary>
    Private Sub CargarBusquedaAvanzada()
        If Not Session("FiltroContrato") Is Nothing Then
            Dim f As FSNServer.FiltroContrato = CType(Session("FiltroContrato"), FSNServer.FiltroContrato)
            If Not f.Busqueda Is Nothing AndAlso f.Busqueda.Rows.Count > 0 Then
                If Not IsDBNull(f.Busqueda.Rows(0).Item("PROCE_GMN1")) Then
                    CargarProcesoEnDropDowns(DBNullToStr(f.Busqueda.Rows(0)("PROCE_ANYO")), f.Busqueda.Rows(0)("PROCE_GMN1"), DBNullToStr(f.Busqueda.Rows(0)("PROCE_COD")))
                End If

                Busqueda.Filtro = f.IDFiltro
                If Not String.IsNullOrEmpty(DBNullToStr(f.Busqueda.Rows(0)("TIPO"))) Then
                    Dim tipos() As String = System.Text.RegularExpressions.Regex.Split(CStr(f.Busqueda.Rows(0)("TIPO")), "[\s$][\s$][\s$]")
                    Busqueda.Tipo = String.Join("$$$", tipos)
                End If

                Busqueda.MaterialHidden = DBNullToStr(f.Busqueda.Rows(0)("MAT"))
                Busqueda.MaterialTexto = DBNullToStr(f.Busqueda.Rows(0)("MAT_DEN"))
                Busqueda.ArticuloHidden = DBNullToStr(f.Busqueda.Rows(0)("ART"))
                Busqueda.ArticuloTexto = DBNullToStr(f.Busqueda.Rows(0)("ART_DEN"))
                Busqueda.Peticionario = DBNullToStr(f.Busqueda.Rows(0)("PETICIONARIO"))
                Busqueda.Descripcion = DBNullToStr(f.Busqueda.Rows(0)("DESCRIPCION"))
                Busqueda.EmpresaHidden = DBNullToStr(f.Busqueda.Rows(0)("EMP"))
                Busqueda.EmpresaTexto = DBNullToStr(f.Busqueda.Rows(0)("EMP_DEN"))
                Busqueda.Estado = DBNullToStr(f.Busqueda.Rows(0)("ESTADO"))
                Busqueda.ProveedorHidden = DBNullToStr(f.Busqueda.Rows(0)("PROVE"))
                Busqueda.ProveedorTexto = DBNullToStr(f.Busqueda.Rows(0)("PROVE_DEN"))
                If Me.Acceso.gbAccesoFSSM Then
                    Busqueda.CentroCoste = DBNullToStr(f.Busqueda.Rows(0)("CENTRO_COSTE"))
                    'ahora usamos Busqueda.getPartidas en vez de Busqueda.getPartidasPres porque se trata de comprobar
                    'que hay partidas, no que tienen valores (ahora vamos a ponerles valor)
                    Dim dtPartidasPres As DataTable = Busqueda.getPartidas(Me.Usuario.Idioma)
                    If dtPartidasPres.Rows.Count > 0 Then
                        Dim sPartidasPres As String()
                        For i As Integer = 1 To 4
                            If DBNullToStr(f.Busqueda.Rows(0)("PARTIDA" & i)) <> String.Empty Then
                                ReDim Preserve sPartidasPres(i - 1)
                                sPartidasPres(i - 1) = DBNullToStr(f.Busqueda.Rows(0)("PARTIDA" & i))
                            End If
                        Next
                        If sPartidasPres IsNot Nothing AndAlso sPartidasPres.Length > 0 Then
                            Busqueda.PartidasPres = sPartidasPres
                        End If
                    End If
                End If
                Busqueda.FilaProveedorVisible = True

                If IsDate(f.Busqueda.Rows(0)("FEC_INI")) Then
                    Busqueda.FechaInicio = f.Busqueda.Rows(0)("FEC_INI")
                Else
                    Busqueda.FechaInicio = Nothing
                End If
                If IsDate(f.Busqueda.Rows(0)("FEC_HASTA")) Then
                    Busqueda.FechaExpiracion = f.Busqueda.Rows(0)("FEC_HASTA")
                Else
                    Busqueda.FechaExpiracion = Nothing
                End If
                Busqueda.PalabraClave = DBNullToStr(f.Busqueda.Rows(0)("PALABRA_CLAVE"))

                If Not IsDBNull(f.Busqueda.Rows(0)("ALERTA_EXPIRACION")) AndAlso f.Busqueda.Rows(0)("ALERTA_EXPIRACION") > 0 Then
                    Busqueda.AlertaExpiracion = f.Busqueda.Rows(0)("ALERTA_EXPIRACION")
                    Busqueda.AlertaPeriodo = f.Busqueda.Rows(0)("ALERTA_PERIODO")
                End If
                Busqueda.AlertaNotificado = DBNullToStr(f.Busqueda.Rows(0)("ALERTA_NOTIFICADO"))
            End If
        End If
    End Sub

    ''' <summary>
    ''' Elimina la configuración del filtro.
    ''' </summary>
    Private Sub btnEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminarFiltro.Click
        Dim f As FSNServer.FiltroContrato = FSNServer.Get_Object(GetType(FSNServer.FiltroContrato))
        If IsNumeric(Session("IDFiltroContratoUsuario")) Then
            f.EliminarConfiguracion(CInt(Session("IDFiltroContratoUsuario")))
            Response.Redirect("..\contratos\VisorContratos.aspx")
            Accion.Value = "Eliminar"
        End If
    End Sub

    Private Sub rbTodosFormularios_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbTodosFormularios.CheckedChanged
        If rbTodosFormularios.Checked Then
            wddFormularios.SelectedItemIndex = 0
            wddFormularios.SelectedValue = ""
            wddFormularios.Enabled = False

            'IDFiltro=1 es el id filtro que se introduce para la opcion de "Todos los formularios"
            Dim selectedID As Integer = 1

            InicializarParametrosBusqueda()

            Session("IDFiltroContrato") = selectedID

            'mostramos el tipo
            Busqueda.FilaProveedorVisible = True
            Busqueda.CeldaLabelTipoVisible = True
            Busqueda.CeldawddTipoVisible = True
            upBusquedaAvanzada.Update()

            'campos generales
            CargarConfiguracionPorDefectoCamposGenerales()

            'ocultar campos de formulario
            lblTituloCamposFormulario.Visible = False
            rpGrupos.Visible = False

            'grid de configuracion
            Dim f As FSNServer.FiltroContrato = FSNServer.Get_Object(GetType(FSNServer.FiltroContrato))
            Dim ds As DataSet = f.LoadConfiguracionInstancia(selectedID, Idioma)

            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add()
            End If

            With whgConfigurarFiltros
                .Rows.Clear()
                .GridView.ClearDataSource()
                Dim contcolumn = .Columns.Count - 1
                For i = 2 To contcolumn
                    .Columns.RemoveAt(2)
                Next
                Dim contcolgr = .GridView.Columns.Count - 1
                For i = 2 To contcolgr
                    .GridView.Columns.RemoveAt(2)
                Next
                .DataSource = ds
                .GridView.DataSource = .DataSource
                CreacionEdicionColumnasGrid()
                .DataBind()

            End With

            upWHG.Update()

            pConfiguracionFiltro.Style("display") = "block"
            btnAceptarFiltro.Visible = True
            btnCancelarFiltro.Visible = True
            btnVolver.Visible = False

            upCampos.Update()
            upBotones.Update()
        End If
    End Sub

    Private Sub rbUnFormulario_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbUnFormulario.CheckedChanged
        If rbUnFormulario.Checked Then
            'ocultamos el tipo
            Busqueda.FilaProveedorVisible = True
            Busqueda.CeldaLabelTipoVisible = True
            Busqueda.CeldawddTipoVisible = True
            upBusquedaAvanzada.Update()

            wddFormularios.Enabled = True
            wddFormularios.SelectedItemIndex = 0
            wddFormularios.SelectedValue = ""

            pConfiguracionFiltro.Style("Display") = "none"
            upBotones.Update()
        End If
    End Sub

    Private Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Not String.IsNullOrEmpty(Session("desdeGS")) Then
            m_desdeGS = True
        End If
        If m_desdeGS Then
            Me.MasterPageFile = "~/App_Master/EnBlanco.Master"
            ConfigurarSkin()
        Else
            Me.MasterPageFile = "~/App_Master/Menu.Master"
        End If
    End Sub

    ''' <summary>
    ''' Configura el Skin de algunos controles si el visor se muestra desde GS para que tengan la apariencia correcta
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ConfigurarSkin()
        Dim master As MasterPage
        'Hace que se carge la MasterPage y con ello hace que se carguen los controles, si no, no podriamos cambiar la propiedad SkinID
        master = Me.Master

        Me.FSNPageHeader.AspectoGS = True
        Me.btnAceptarFiltro.SkinID = "GS"
        Me.btnCancelarFiltro.SkinID = "GS"
        Me.btnEliminarFiltro.SkinID = "GS"
        Me.btnVolver.SkinID = "GS"
        Me.ImgBuscadorProcesos.SkinID = "BuscadorLupa_GS"
        Me.pnlBusqueda.CssClass = "PanelCabeceraGS"
        Me.pnlControlBusqueda.CssClass = "RectanguloGS"
        Me.pnlCampos.CssClass = "PanelCabeceraGS"
        Me.pnlConfiguracionCampos.CssClass = "RectanguloGS"
    End Sub

    ''' <summary>
    ''' Evento que salta cuando se cambia la seleccion del combo
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ddlCommodity_SelectionChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownSelectionChangedEventArgs) Handles ddlCommodity.SelectionChanged
        CargarProcesos()
    End Sub

    ''' <summary>
    ''' Evento que salta cuando se cambia la seleccion del combo
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ddlAnyo_SelectionChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownSelectionChangedEventArgs) Handles ddlAnyo.SelectionChanged
        CargarProcesos()
    End Sub

    Private Sub CargarProcesos()
        Dim Commodity As String = ""
        Dim Anyo As Short

        If ddlCommodity.SelectedValue <> "" Then
            Commodity = ddlCommodity.SelectedValue
        End If

        If IsNumeric(ddlAnyo.SelectedValue) Then
            Anyo = ddlAnyo.SelectedValue
        End If

        If Commodity <> "" Then
            CargarProcesos(Anyo, Commodity)
        Else
            ddlCodigo.Items.Clear()
            ddlDenominacion.Items.Clear()
        End If
        ddlCodigo.CurrentValue = ""
        ddlDenominacion.CurrentValue = ""
    End Sub

    ''' <summary>
    ''' Evento que salta cuando se cambia el valor del combo de denominacion de proceso
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ddlDenominacion_ValueChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownValueChangedEventArgs) Handles ddlDenominacion.ValueChanged
        If Not ddlDenominacion.SelectedItem Is Nothing Then
            If ddlDenominacion.CurrentValue <> ddlDenominacion.SelectedItem.Text Then
                ddlDenominacion.ClearSelection()
                ddlCodigo.ClearSelection()
                ddlCodigo.CurrentValue = ""
                ddlDenominacion.CurrentValue = ""
            End If
        End If
    End Sub

    ''' <summary>
    ''' Carga el proceso seleccionado desde el buscador de procesos en los combos
    ''' </summary>
    ''' <param name="sAnyo">Año del proceso</param>
    ''' <param name="sGMN1">Commodity del proceso</param>
    ''' <param name="Codigo">Codigo del proceso</param>
    ''' <remarks></remarks>
    Private Sub CargarProcesoEnDropDowns(ByVal sAnyo As String, ByVal sGMN1 As String, ByVal Codigo As String)
        'deseleccionamos los combos pues si habria algun proceso seleccionado
        If sAnyo = "" Or sGMN1 = "" Then
            If ddlAnyo.SelectedItems.Count > 0 Then
                For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlAnyo.SelectedItems
                    ddi.Selected = False
                Next
            End If
            If ddlCommodity.SelectedItems.Count > 0 Then
                For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlCommodity.SelectedItems
                    ddi.Selected = False
                Next
            End If
            ddlCommodity.CurrentValue = ""
            If ddlCodigo.SelectedItems.Count > 0 Then
                For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlCodigo.SelectedItems
                    ddi.Selected = False
                Next
            End If
            ddlCodigo.CurrentValue = ""
            If ddlDenominacion.SelectedItems.Count > 0 Then
                For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlDenominacion.SelectedItems
                    ddi.Selected = False
                Next
            End If
            ddlDenominacion.CurrentValue = ""
        Else
            For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlAnyo.Items
                If ddi.Value = sAnyo Then
                    ddi.Selected = True
                    ddlAnyo.CurrentValue = ddi.Text
                    Exit For
                End If
            Next

            For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlCommodity.Items
                If ddi.Value = sGMN1 Then
                    ddi.Selected = True
                    ddlCommodity.CurrentValue = ddi.Text
                    Exit For
                End If
            Next

            If Codigo <> "" Then
                CargarProcesos(sAnyo, sGMN1)

                For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlCodigo.Items
                    If ddi.Value = Codigo Then
                        ddi.Selected = True
                        ddlCodigo.CurrentValue = ddi.Text
                        Exit For
                    End If
                Next

                For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlDenominacion.Items
                    If ddi.Value = Codigo Then
                        ddi.Selected = True
                        ddlDenominacion.CurrentValue = ddi.Text
                        Exit For
                    End If
                Next
            End If
        End If

    End Sub

    Private Sub CreacionEdicionColumnasGrid()

        ObtenerColumnasGrid(whgConfigurarFiltros.UniqueID)

    End Sub

    ''' <summary>
    ''' Inicializa las columnas del grid con los campos generales y los campos personalizados
    ''' </summary>
    ''' <remarks>Llamada desde=Page_Load u// ; Tiempo máximo:0seg.</remarks>
    Private Sub ObtenerColumnasGrid(ByVal gridId As String)

        Dim grid As WebHierarchicalDataGrid = CType(Me.FindControl(gridId), WebHierarchicalDataGrid)

        Dim camposGenerales As DataSet = ObtenerEstructuraCamposGenerales()

        Dim camposGrid As New List(Of Infragistics.Web.UI.GridControls.BoundDataField)
        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField


        If Session("FiltroContrato") Is Nothing Then
            Dim Posicion As Integer = 2
            For Each oRow As DataRow In camposGenerales.Tables("CAMPOS").Rows
                'Tratamiento campos Generales
                'Oculto todos por defecto, luego ya los mostraré
                campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
                With campoGrid
                    .Key = oRow("CAMPO_ID")
                    .DataFieldName = oRow("CAMPO_ID")
                    .Hidden = True
                    .Header.CssClass = "hiddenElement headerNoWrap"
                    .CssClass = "hiddenElement SinSalto"
                    .Header.Text = DBNullToSomething(oRow("NOMBRE"))
                    .Header.Tooltip = DBNullToSomething(oRow("NOMBRE"))
                    .Width = DBNullToDbl(oRow("TAMANYO"))
                End With
                whgConfigurarFiltros.GridView.Columns.Insert(Posicion, campoGrid)
                AnyadirTextoAColumnaNoFiltrada(gridId, campoGrid.Key, True)
                Posicion += 1
                camposGrid.Add(campoGrid)
            Next

            'para añadir los campos de un formulario que se carga desde el combo
            If Session("IDFiltroContrato") IsNot Nothing Then

                Dim f As FSNServer.FiltroContrato = FSNServer.Get_Object(GetType(FSNServer.FiltroContrato))
                Dim ds As DataSet = f.LoadCampos(Usuario.CodPersona, Session("IDFiltroContrato"), Idioma)

                For Each field As DataRow In ds.Tables(1).Rows
                    Dim keyField As String = "C_" & field("ID").ToString()
                    campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
                    With campoGrid
                        .Key = keyField
                        .DataFieldName = keyField
                        .Hidden = True
                        .Header.CssClass = "hiddenElement headerNoWrap"
                        .CssClass = "hiddenElement SinSalto"
                        .Header.Text = keyField
                        .Width = 65
                    End With
                    whgConfigurarFiltros.GridView.Columns.Insert(Posicion, campoGrid)
                    AnyadirTextoAColumnaNoFiltrada(gridId, campoGrid.Key, True)
                    Posicion += 1
                    camposGrid.Add(campoGrid)
                Next
            End If
        Else

            Dim f As FSNServer.FiltroContrato = CType(Session("FiltroContrato"), FSNServer.FiltroContrato)

            Dim MaxVisibleIndex As Integer = camposGenerales.Tables("CAMPOS").Rows.Count
            MaxVisibleIndex = MaxVisibleIndex + 2 'Aprobar Rechazar
            If f.CamposFormulario.Tables.Count > 0 Then MaxVisibleIndex = MaxVisibleIndex + f.CamposFormulario.Tables(1).Rows.Count
            MaxVisibleIndex = MaxVisibleIndex - 1 'Visible index 17 falla si tienes 17 columnas. El ultimo es visible index 16.

            'campos generales
            For Each oRow As DataRow In camposGenerales.Tables("CAMPOS").Rows
                campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
                Dim view As New DataView(f.CamposGenerales)
                view.Sort = "CAMPO"
                Dim index As Integer = view.Find(oRow("ID"))
                If index >= 0 Then
                    With campoGrid
                        If DBNullToStr(view(index)("NOMBRE")) <> "" Then
                            .Header.Text = view(index)("NOMBRE")
                            .Header.Tooltip = view(index)("NOMBRE")
                        Else
                            .Header.Text = oRow("NOMBRE")
                            .Header.Tooltip = oRow("NOMBRE")
                        End If
                        .Key = oRow("CAMPO_ID")
                        .DataFieldName = oRow("CAMPO_ID")
                        If view(index)("POS") > MaxVisibleIndex Then
                            .VisibleIndex = MaxVisibleIndex
                        Else
                            .VisibleIndex = view(index)("POS")
                        End If
                        .Width = IIf(DBNullToDbl(view(index)("TAMANYO")) = 0, Unit.Pixel(100), Unit.Pixel(view(index)("TAMANYO")))
                        .Hidden = Not CBool(view(index)("VISIBLE"))
                        If CBool(view(index)("VISIBLE")) = True Then
                            .CssClass = "SinSalto"
                            .Header.CssClass = "headerNoWrap"
                        Else
                            .CssClass = "hiddenElement SinSalto"
                            .Header.CssClass = "hiddenElement headerNoWrap"
                        End If
                    End With

                    whgConfigurarFiltros.GridView.Columns.Add(campoGrid)
                        AnyadirTextoAColumnaNoFiltrada(gridId, campoGrid.Key, True)
                        camposGrid.Add(campoGrid)
                    End If
                Next

            'campos de formulario
            If f.CamposFormulario.Tables.Count > 0 Then
                For Each oRow As DataRow In f.CamposFormulario.Tables(1).Rows
                    campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
                    Dim keyField As String = "C_" & oRow("ID").ToString()
                    With campoGrid
                        If whgConfigurarFiltros.Columns.Item(keyField) Is Nothing AndAlso (DBNullToStr(keyField) <> "") Then
                            If DBNullToStr(oRow("NOMBRE_PER")) <> "" Then
                                .Header.Text = oRow("NOMBRE_PER")
                                .Header.Tooltip = oRow("NOMBRE_PER")
                            Else
                                .Header.Text = oRow("NOMBRE")
                                .Header.Tooltip = oRow("NOMBRE")
                            End If
                            .Key = keyField
                            If DBNullToDbl(oRow("POS")) > MaxVisibleIndex Then
                                .VisibleIndex = MaxVisibleIndex
                            Else
                                .VisibleIndex = DBNullToDbl(oRow("POS"))
                            End If

                            .Width = IIf(DBNullToDbl(oRow("TAMANYO")) = 0, Unit.Pixel(100), Unit.Pixel(DBNullToDbl(oRow("TAMANYO"))))
                            .Hidden = Not CBool(oRow("VISIBLE"))
                            If CBool(oRow("VISIBLE")) = True Then
                                .CssClass = "SinSalto"
                                .Header.CssClass = "headerNoWrap"
                            Else
                                .CssClass = "hiddenElement SinSalto"
                                    .Header.CssClass = "hiddenElement headerNoWrap"
                                End If

                                whgConfigurarFiltros.GridView.Columns.Add(campoGrid)
                                AnyadirTextoAColumnaNoFiltrada(gridId, campoGrid.Key, True)
                                camposGrid.Add(campoGrid)
                            End If
                        End With
                    Next
                End If
            End If
            Session("arrDatosGeneralesVCont") = camposGrid

        'cFiltro = Nothing
    End Sub

    Private Sub AnyadirTextoAColumnaNoFiltrada(ByVal gridId As String, ByVal fieldKey As String, ByVal EnableResize As Boolean)

        Dim grid As WebHierarchicalDataGrid = CType(Me.FindControl(gridId), WebHierarchicalDataGrid)

        Dim fieldResize As Infragistics.Web.UI.GridControls.ColumnResizeSetting
        fieldResize = New Infragistics.Web.UI.GridControls.ColumnResizeSetting
        fieldResize.ColumnKey = fieldKey
        fieldResize.EnableResize = EnableResize
        grid.GridView.Behaviors.ColumnResizing.ColumnSettings.Add(fieldResize)
    End Sub

    Private Sub whgConfigurarFiltros_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whgConfigurarFiltros.InitializeRow
        Dim items As Infragistics.Web.UI.GridControls.GridRecordItemCollection = e.Row.Items
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorContratos
        Select Case CType(DBNullToInteger(e.Row.DataItem.Item.Row.Item("ESTADO")), Integer)
                Case EstadosVisorContratos.Guardado
                    items.FindItemByKey("ESTADO").Text = Textos(0)
                Case EstadosVisorContratos.En_Curso_De_Aprobacion
                    items.FindItemByKey("ESTADO").Text = Textos(1)
                Case EstadosVisorContratos.Vigentes
                    items.FindItemByKey("ESTADO").Text = Textos(2)
                Case EstadosVisorContratos.Proximo_a_Expirar
                    items.FindItemByKey("ESTADO").Text = Textos(3)
                Case EstadosVisorContratos.Expirados
                    items.FindItemByKey("ESTADO").Text = Textos(4)
                Case EstadosVisorContratos.Rechazados
                    items.FindItemByKey("ESTADO").Text = Textos(5)
                Case EstadosVisorContratos.Anulados
                    items.FindItemByKey("ESTADO").Text = Textos(6)
            End Select
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltrosContratos
    End Sub
End Class