﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ConfigurarFiltrosContratos

    '''<summary>
    '''Accion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Accion As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''FSNPageHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FSNPageHeader As Global.Fullstep.FSNWebControls.FSNPageHeader

    '''<summary>
    '''vsValidador control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents vsValidador As Global.System.Web.UI.WebControls.ValidationSummary

    '''<summary>
    '''lblNombreFiltro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNombreFiltro As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtNombreFiltro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtNombreFiltro As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''rfvNombreFiltro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rfvNombreFiltro As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''chkFiltroPorDefecto control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkFiltroPorDefecto As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''rbTodosFormularios control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbTodosFormularios As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''rbUnFormulario control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbUnFormulario As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''wddFormularios control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents wddFormularios As Global.Infragistics.Web.UI.ListControls.WebDropDown

    '''<summary>
    '''upBotones control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upBotones As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''btnAceptarFiltro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAceptarFiltro As Global.Fullstep.FSNWebControls.FSNButton

    '''<summary>
    '''btnCancelarFiltro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCancelarFiltro As Global.Fullstep.FSNWebControls.FSNButton

    '''<summary>
    '''btnEliminarFiltro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnEliminarFiltro As Global.Fullstep.FSNWebControls.FSNButton

    '''<summary>
    '''btnVolver control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnVolver As Global.Fullstep.FSNWebControls.FSNButton

    '''<summary>
    '''UpdatePanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''pConfiguracionFiltro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pConfiguracionFiltro As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''CollapsiblePanelExtender0 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CollapsiblePanelExtender0 As Global.AjaxControlToolkit.CollapsiblePanelExtender

    '''<summary>
    '''pnlBusqueda control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlBusqueda As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''imgExpandir0 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgExpandir0 As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''lblTituloBusqueda control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTituloBusqueda As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlControlBusqueda control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlControlBusqueda As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''upBusquedaAvanzada control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upBusquedaAvanzada As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''UpdatePanel2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel2 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''lblProceso control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblProceso As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAño control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAño As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlAnyo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlAnyo As Global.Infragistics.Web.UI.ListControls.WebDropDown

    '''<summary>
    '''lblCommodity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCommodity As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlCommodity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCommodity As Global.Infragistics.Web.UI.ListControls.WebDropDown

    '''<summary>
    '''lblCodigo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCodigo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlCodigo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCodigo As Global.Infragistics.Web.UI.ListControls.WebDropDown

    '''<summary>
    '''lblDenominacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDenominacion As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlDenominacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlDenominacion As Global.Infragistics.Web.UI.ListControls.WebDropDown

    '''<summary>
    '''ImgBuscadorProcesos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImgBuscadorProcesos As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Busqueda control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Busqueda As Global.Fullstep.FSNWeb.wucBusquedaAvanzadaContratos

    '''<summary>
    '''upCampos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upCampos As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''CollapsiblePanelExtender2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CollapsiblePanelExtender2 As Global.AjaxControlToolkit.CollapsiblePanelExtender

    '''<summary>
    '''pnlCampos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlCampos As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''imgExpandir2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgExpandir2 As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''lblTituloCampos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTituloCampos As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlConfiguracionCampos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlConfiguracionCampos As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblTituloCamposGenerales control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTituloCamposGenerales As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTituloCamposFormulario control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTituloCamposFormulario As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rpCamposGenerales control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rpCamposGenerales As Global.System.Web.UI.WebControls.Repeater

    '''<summary>
    '''rpGrupos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rpGrupos As Global.System.Web.UI.WebControls.Repeater

    '''<summary>
    '''lblTituloGridConfiguracion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTituloGridConfiguracion As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''upWHG control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upWHG As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''whgConfigurarFiltros control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents whgConfigurarFiltros As Global.Infragistics.Web.UI.GridControls.WebHierarchicalDataGrid

    '''<summary>
    '''ModalProgress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ModalProgress As Global.AjaxControlToolkit.ModalPopupExtender
End Class
