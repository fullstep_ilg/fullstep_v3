<%@ Page Language="vb" Debug="true" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.Master" CodeBehind="VisorContratos.aspx.vb" EnableEventValidation="false" Inherits="Fullstep.FSNWeb.VisorContratos" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="head" runat="server">  

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
<script type="text/javascript" language="javascript">
        var x = 0;
        var y = 0;        
        var xmlHttp;
        var ColCheck=0;
        var Aprobar;
        $('[id$=btnAprobar]').live('click',function(){           
            var whGrid = $('[id$=whgContratos]').attr('id');
            var ID,BLOQUE,ACCION_APROBAR;
            var contratos=[];
            var contrato;
            MostrarCargando()
            for (i = 0; i < $find(whGrid).get_gridView().get_rows().get_length(); i++) {
                 if($('[id$=cbAprobar]')[i].checked){
                    ID = $find(whGrid).get_gridView().get_rows().get_row(i).get_cellByColumnKey("ID").get_value();
                    BLOQUE = $find(whGrid).get_gridView().get_rows().get_row(i).get_cellByColumnKey("BLOQUE").get_value();
                    ACCION_APROBAR = $find(whGrid).get_gridView().get_rows().get_row(i).get_cellByColumnKey("ACCION_APROBAR").get_value();
                    contrato={id:ID,bloque:BLOQUE,accion:ACCION_APROBAR};
                    contratos.push(contrato);
                    $find(whGrid).get_gridView().get_rows().get_row(i).get_element().style.display = 'none';
               }
            } 
            $.when($.ajax({
                type: "POST",
                url: rutaFS + 'PM/Contratos/VisorContratos.aspx/AprobarContratos',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({Contratos: contratos }),
                dataType: "json",
                async: false,	
                success:function(){		
                    setTimeout('OcultarCargando()',500);
                }				    
            }));
        });
        $('[id$=btnRechazar]').live('click',function(){           
            var whGrid = $('[id$=whgContratos]').attr('id');
            var ID,BLOQUE,ACCION_RECHAZAR;
            var contratos=[];
            var contrato;
            MostrarCargando();
            for (i = 0; i < $find(whGrid).get_gridView().get_rows().get_length(); i++) {
                if($('[id$=cbRechazar]')[i].checked){
                    ID = $find(whGrid).get_gridView().get_rows().get_row(i).get_cellByColumnKey("ID").get_value();
                    BLOQUE = $find(whGrid).get_gridView().get_rows().get_row(i).get_cellByColumnKey("BLOQUE").get_value();
                    ACCION_RECHAZAR = $find(whGrid).get_gridView().get_rows().get_row(i).get_cellByColumnKey("ACCION_RECHAZAR").get_value();
                    contrato={id:ID,bloque:BLOQUE,accion:ACCION_RECHAZAR};
                    contratos.push(contrato);
                    $find(whGrid).get_gridView().get_rows().get_row(i).get_element().style.display = 'none';
               }
            } 
            $.when($.ajax({
                type: "POST",
                url: rutaFS + 'PM/Contratos/VisorContratos.aspx/RechazarContratos',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({Contratos: contratos }),
                dataType: "json",
                async: false,	
                success:function(){		
                    setTimeout('OcultarCargando()',500);
                }			    
            }));
        });                
        $(document).ready(function () {	
            $('[id$=ImgBtnFirst]').live('click',function(){ FirstPage();});
            $('[id$=ImgBtnPrev]').live('click',function(){ PrevPage();});
            $('[id$=ImgBtnNext]').live('click',function(){ NextPage();});
            $('[id$=ImgBtnLast]').live('click',function(){ LastPage();});		       
		});
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(LoadCache);
        function LoadCache(sender,args){
            if($('[id$=hCacheLoaded]').val()==0){
                var btnRecargaCache = $('[id$=btnRecargaCache]').attr('id');
                __doPostBack(btnRecargaCache,'');
            }
        }

         /*
        ''' <summary>
        ''' Function que oculta la pantalla de Cargando
        ''' </summary>
        ''' <remarks>Llamada desde; Tiempo m�ximo:0seg.</remarks>*/ 
		function OcultarCargando() {
			var modalprog = $find(ModalProgress);
			if (modalprog) modalprog.hide();
		}; 
                
        //Creamos el objeto xmlHttpRequest
	    CreateXmlHttp();
     /*
    ''' <summary>
    ''' Function que crea el objeto para la llamada AJAX
    ''' </summary>
    ''' <remarks>Llamada desde; Tiempo m�ximo:0seg.</remarks>*/      		      
	function CreateXmlHttp() {	    
		// Probamos con IE
		try
		{
			// Funcionar� para JavaScript 5.0
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch(e)
		{
			try
			{
				xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(oc)
			{   
				xmlHttp = null;
			}
		}

		// Si no se trataba de un IE, probamos con esto
		if(!xmlHttp && typeof XMLHttpRequest != "undefined")
		{
			xmlHttp = new XMLHttpRequest();
		}

		return xmlHttp;
	}
        
        //funcion que evita que al seleccionar un nuevo valor del webdropdown ya sea por seleccion o escribiendo se despliegue el combo
	    function CancelDropDownOpening(sender,args){
	        if(sender.__isDeleting==true && sender.__isButtonClick==false){
	            args.set_cancel(true);
	        }
            if(sender.__isDeleting==false && sender.get_currentValue()!=sender._previousValue){
	            args.set_cancel(true);
	        }
	    }
	        
        function prove_seleccionado2(sCIF, sProveCod, sProveDen) {
            document.getElementById("<%=hidProveedorCIF.ClientID%>").value = sCIF
            document.getElementById("<%=hidProveedor.ClientID%>").value = sProveCod
            document.getElementById("<%=txtProveedor.ClientID%>").value = sProveDen
        }
        
    //Recoge el ID del proveedor seleccionado con el autocompletar
	function selected_Proveedor(sender, e) { 
	    oIdProveedor = document.getElementById('<%= hidProveedor.clientID %>');
        if (oIdProveedor)
            oIdProveedor.value = e._value;
    } 
        
        /*
        Descripcion: Funcion que inicializa la caja de texto de los combos Codigo y Denominacion a ""
        Parametros entrada:=
        elem:=componente input
        event:=evento
        Llamada desde: Al cambiar el valor del combo tipo
        Tiempo ejecucion:= 0seg.    
    
       */
        function ValueChanged_wddCommodity(elem, event) {
            
            comboCodigo = $find("<%=ddlCodigo.ClientID%>")
            if (comboCodigo)
                comboCodigo._elements.Input.value=""
                
            comboDenominacion = $find("<%=ddlDenominacion.ClientID%>")
            if (comboDenominacion)
                comboDenominacion._elements.Input.value=""

        }

        /*''' <summary>
        ''' Responder al evento click en una celda del grid
        ''' </summary>
        ''' <param name="gridName">Nombre del grid</param>
        ''' <param name="cellId">Id de la celda</param>        
        ''' <param name="button">Bot�n pulsado</param>  
        ''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>*/
        function uwgContratos_CellClickHandler(gridName, cellId, button) {
            
            var hid_Cellkey = document.getElementById("<%=hid_Cellkey.clientId %>");
            var row = igtbl_getRowById(cellId);
            var celda = igtbl_getColumnById(cellId)
            var skey = ''
            if (celda)
                skey = celda.Key

            if (row.getCellFromKey(celda.Key).getTargetURL() == 'EDITOR'){
                var campo_origen;
                hid_Cellkey.value = '';
                igtbl_cancelPostBack(row.gridId);
                campo_origen = celda.Key.substring(2); //Ej: C_31391
                window.open("../_common/Editor.aspx??SessionId=<%=Session("sSession")%>&titulo=" + celda.HeaderText + "&ID=" + row.getCellFromKey("ID").getValue() + "&CAMPO_ORIGEN=" + campo_origen + "&readOnly=1", "Editor", "width=910,height=600,status=yes,resizable=yes,top=20,left=100,scrollbars=yes")
                return false;
            }

            if (row.IsFilterRow) {
                igtbl_cancelPostBack(row.gridId)
                return false;
            }
            
            if (skey =="APROBAR" || skey=="RECHAZAR"){
                ColCheck=1;
                igtbl_cancelPostBack(row.gridId)
                hid_Cellkey.value = ''
                return false;
            } else {
                var hid_CodContrato = document.getElementById("<%=hid_CodContrato.clientId%>");
                var hid_CodPeticionario = document.getElementById("<%=hid_CodPeticionario.clientId%>");
                var hid_CodProveedor = document.getElementById("<%=hid_CodProveedor.clientId%>");
                var hid_Proveedor = document.getElementById("<%=hid_Proveedor.clientId%>");
                var hid_CodEmpresa = document.getElementById("<%=hid_CodEmpresa.clientId%>");
                var hid_Empresa = document.getElementById("<%=hid_Empresa.clientId%>");
                var hid_Identificador = document.getElementById("<%=hid_Identificador.clientId%>");
                var hid_Icono = document.getElementById("<%=hid_Icono.clientId%>");
                var hid_Trasladada = document.getElementById("<%=hid_Trasladada.clientId%>");
                var hid_Observador = document.getElementById("<%=hid_Observador.clientId%>");
                var hid_InstanciaEstado = document.getElementById("<%=hid_InstanciaEstado.clientId%>");
                var hid_Contrato = document.getElementById("<%=hid_Contrato.clientId%>");
                hid_CodContrato.value = row.getCellFromKey("CODIGO").getValue() //Cod Contrato
                hid_CodPeticionario.value = row.getCellFromKey("CODPET").getValue()
                hid_CodProveedor.value = row.getCellFromKey("CODPRO").getValue() // Cod Proveedor
                hid_Proveedor.value = row.getCellFromKey("PROVEEDOR").getValue() // Proveedor
                hid_CodEmpresa.value = row.getCellFromKey("CODEMP").getValue() // Cod Empresa
                hid_Empresa.value = row.getCellFromKey("EMPRESA").getValue() // Empresa
                hid_Identificador.value = row.getCellFromKey("ID").getValue()               
                hid_Icono.value = row.getCellFromKey("ICONO").getValue()               
                hid_Trasladada.value = row.getCellFromKey("TRASLADADA").getValue()               
                hid_Observador.value = row.getCellFromKey("OBSERVADOR").getValue()     
                hid_InstanciaEstado.value = row.getCellFromKey("INSTANCIA_ESTADO").getValue()          
                hid_Contrato.value = row.getCellFromKey("ID_CONTRATO").getValue()          
                hid_Cellkey.value = skey
                return false;
            }
        }
    
        /*
        <summary>
        Funcion que abre la ventana de buscador de procesos
        </summary>
        <remarks>Llamada desde: Icono de buscador de procesos</remarks>
        */
        function show_buscadorProcesos(){       
            
            var sAnyo = '';
            var sCommodity = '';
            var sCodProceso = '';
            var sDenProceso = '';
            
            var wdd1 = $find('<%=ddlAnyo.clientID%>');            
            if (wdd1!=null)
                if (wdd1.get_selectedItem()!=null)                    
                    sAnyo= wdd1.get_selectedItem().get_value();
                
            var wdd2 = $find('<%=ddlCommodity.clientID%>');            
            if (wdd2!=null)                
                if (wdd2.get_selectedItem()!=null)
                    sCommodity= wdd2.get_selectedItem().get_value();
                
            var wdd3 = $find('<%=ddlCodigo.clientID%>');            
            if (wdd3!=null)
                if (wdd3.get_selectedItem()!=null)
                    sCodProceso= wdd3.get_selectedItem().get_value();                                
                                
            var wdd4 = $find('<%=ddlDenominacion.clientID%>');            
            if (wdd4!=null)
                if (wdd4.get_selectedItem()!=null)
                    sDenProceso= wdd4.get_selectedItem().get_text();               
                                                              
            var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>_common/BuscadorProcesos.aspx?SessionId=<%=Session("sSession")%>&desdeGS=<%=Session("desdeGS")%>&Anyo=" + escape(sAnyo) + "&Commodity=" + escape(sCommodity) + "&CodProceso=" + escape(sCodProceso) + "&DenProceso=" + escape(sDenProceso),"_blank","width=800,height=500,status=yes,resizable=no,top=100,left=200");
            

        }
        
        /*Descripcion:= Funcion que carga los valores del buscador de empresas en la caja de texto
        Parametros:=
        idEmpresa --> Id Empresa
        nombreEmpresa --> Nombre empresa
        Llamada desde:= BuscadorEmpresas.aspx --> aceptar()*/
    
    function SeleccionarProceso(sANYO, sGMN1, sID, sDEN) {
         __doPostBack("<%=ddlAnyo.clientID%>", "CargarProce#"+ sANYO+ "#" + sGMN1 + "#" + sID + "#" + sDEN);
         
    }
    
	/*
	''' <summary>
    ''' Comprueba si la instancia esta en proceso
    ''' </summary>
    ''' <param name="instancia">ID Instancia</param>
    ''' <remarks>Llamada desde:IrAContratos; Tiempo m�ximo:0,4seg.</remarks> */
	function ComprobarEnProceso(instancia) {
	    var params ="ID=" + instancia; 
	    
	    xmlHttp.open("POST", rutaFS + "_common/comprobarEnProceso.aspx", false);
		xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
		xmlHttp.send(params);
		
		if (xmlHttp.responseText.substr(0,1) != 0) {
		    var newWindow = window.open(rutaPM + "seguimiento/NWcomentariossolic.aspx?SessionId=<%=Session("sSession")%>&Instancia=" + instancia + "&EnProceso=1","_blank", "width=600,height=300,status=yes,resizable=no,top=200,left=200");
            
		    return false;
		} else 
		    return true;
		
	}
    /*
        ''' <summary>
        ''' Ordena los datos por la columna indicada, si es q es ordenable por ese concepto.
        ''' </summary>
        ''' <param name="sender">Origen del evento</param>
        ''' <param name="e">evento</param>        
        ''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>*/
		function whgContratos_Sorting(sender,e) {
			switch (e.get_column().get_key()) {
				case "INDICADOR_EN_PROCESO":
				case "TEMPLATEAPROBAR":
				case "TEMPLATERECHAZAR":
                case "IMAGE_EST":
					e.set_cancel(true);
					break;
				default:
                    var sortSentence;
                    if (e.get_clear()){
                        sortSentence=e.get_column().get_key() + " " + (e.get_sortDirection()==1?"ASC":"DESC");
                    }else{
                        sortSentence=$('[id$=hOrdered]').val() + "," + e.get_column().get_key() + " " + (e.get_sortDirection()==1?"ASC":"DESC");
                    }
                    $('[id$=hOrdered]').val(sortSentence);
                    if($('[id$=hFiltered]').val()=='1'){
                        var btnOrder = $('[id$=btnOrder]').attr('id');
                        __doPostBack(btnOrder,'');
                    }
					break;
			}
		}
        function IndexChanged(){
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var btnPager = $('[id$=btnPager]').attr('id');
            var selectedIndex = dropdownlist.selectedIndex;
            __doPostBack(btnPager,dropdownlist.selectedIndex);
        }
        function FirstPage(){
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var btnPager = $('[id$=btnPager]').attr('id');
            dropdownlist.options[0].selected = true;
            __doPostBack(btnPager,0);
        }
        function PrevPage(){
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var btnPager = $('[id$=btnPager]').attr('id');
            var selectedIndex = dropdownlist.selectedIndex;
            if(selectedIndex-1>=0){
                dropdownlist.options[selectedIndex-1].selected = true;
                __doPostBack(btnPager,dropdownlist.selectedIndex);
            }
        }
        function NextPage(){
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var btnPager = $('[id$=btnPager]').attr('id');
            var selectedIndex = dropdownlist.selectedIndex;
            if(selectedIndex+1<=dropdownlist.length-1){
                dropdownlist.options[selectedIndex+1].selected = true;
                __doPostBack(btnPager,dropdownlist.selectedIndex);
            }                 
        }
        function LastPage(){
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var btnPager = $('[id$=btnPager]').attr('id');
            dropdownlist.options[dropdownlist.length-1].selected = true;
            __doPostBack(btnPager,dropdownlist.length-1);
        }
        function PagerBD(){
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var btnPager = $('[id$=btnPagerBD]').attr('id');
            __doPostBack(btnPager,dropdownlist.selectedIndex);
            return false;
        }
        /*
        ''' <summary>
        ''' Function que filtra el grid (el "embudo")
        ''' </summary>
        ''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0seg.</remarks>*/     
        function whgContratos_Filtering(sender,e){
            var gridFiltered=false;
            var filteredColumns,filteredCondition,filteredValue;
            var grid1erFilterValueVacio=false;
            $('[id$=hFilteredColumns]').val('');
            $('[id$=hFilteredCondition]').val('');
            $('[id$=hFilteredValue]').val('');
            for (i=0;i<e.get_columnFilters().length;i++){
                if(e.get_columnFilters()[i].get_condition().get_rule()!=0){
                    gridFiltered=true;                    
                    filteredColumns=$('[id$=hFilteredColumns]').val();
                    filteredCondition=$('[id$=hFilteredCondition]').val();
                    filteredValue=$('[id$=hFilteredValue]').val();
                    $('[id$=hFilteredColumns]').val((filteredColumns==''?e.get_columnFilters()[i].get_columnKey():filteredColumns +"#"+e.get_columnFilters()[i].get_columnKey()));
                    $('[id$=hFilteredCondition]').val((filteredCondition==''?e.get_columnFilters()[i].get_condition().get_rule():filteredCondition +"#"+e.get_columnFilters()[i].get_condition().get_rule()));
                    if (grid1erFilterValueVacio){
                        grid1erFilterValueVacio =false;
                        $('[id$=hFilteredValue]').val("#"+e.get_columnFilters()[i].get_condition().get_value());
                        filteredValue=$('[id$=hFilteredValue]').val();
                    }
                    else{
                        $('[id$=hFilteredValue]').val((filteredValue==''?e.get_columnFilters()[i].get_condition().get_value():filteredValue +"#"+e.get_columnFilters()[i].get_condition().get_value()));
                    }
                    if ((e.get_columnFilters()[i].get_condition().get_value()=='')&&(filteredValue=='')){
                        grid1erFilterValueVacio =true;
                    }
                }    
            }
            var btnFilter = $('[id$=btnFilter]').attr('id');
            if(gridFiltered) {
                $('[id$=hFiltered]').val(1);                
            }else{
                $('[id$=hFiltered]').val(0);
            }
        } 
         /*
        ''' <summary>
        ''' Se mira si se ha pulsado sobre una celda q muestra popup con info ("PROVEEDOR","PETICIONARIO","EMPRESA")
        ''' </summary>
        ''' <param name="sender">Origen del evento</param>
        ''' <param name="e">evento</param>        
        ''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>*/
        function grid_CellClick(sender, e) {
        if (e.get_type() == "cell") {
                if (e.get_item().get_grid().get_behaviors().get_activation().get_activeCellResolved() !== null) {
                    var cell = e.get_item().get_grid().get_behaviors().get_activation().get_activeCellResolved();

    			    posicionX=$(cell.get_element()).position().left + ($(cell.get_element()).innerWidth() / 4);
    			    posicionY=$(cell.get_element()).position().top + 20;

                    switch (cell.get_column().get_key()) {
					    case "CODPRO":
					    case "PROVEEDOR":
                            FSNMostrarPanel(ProveAnimationClientID, e, ProveDynamicPopulateClienteID, cell.get_row().get_cellByColumnKey("CODPRO").get_value(),null,null,posicionX,posicionY);                            
						    break;
					    case "PETICIONARIO":
                            if (cell.get_row().get_cellByColumnKey("CODPET").get_value() != ""){
						        FSNMostrarPanel(PeticionarioAnimationClientID, e, PeticionarioDynamicPopulateClienteID, cell.get_row().get_cellByColumnKey("CODPET").get_value(),null,null,posicionX,posicionY);
                            }
						    break;
                        }
               }
          }

        }

        function ExportarExcel(){
			__doPostBack('Excel','');
		}

		function ExportarPDF(){
			__doPostBack('PDF','');
		}

        //''' <summary>
        //''' Event fired before a Cell's selection changes. 
        //''' </summary>
        //''' <remarks>Llamada desde: Grid SelectionClientEvents-CellSelectionChanging ; Tiempo m�ximo: 0,2</remarks>
        function onCellSelectionChanged(sender, e) {
        var row = e.getNewSelectedCells().getCell(0).get_row();

             switch (e.getNewSelectedCells().getCell(0).get_column().get_key()) {
             // los dos primeros casos es para la exclusion de checks
                    case "TEMPLATEAPROBAR": //COLUMNA DE APROBAR
                        if ($('[id$=cbRechazar]',$(row.get_element())).prop('checked')){
                            $('[id$=cbRechazar]',$(row.get_element())).prop('checked',false);
                        }
                        break;
                    case "TEMPLATERECHAZAR": //COLUMNA DE APROBAR
                        if ($('[id$=cbAprobar]',$(row.get_element())).prop('checked')){
                            $('[id$=cbAprobar]',$(row.get_element())).prop('checked',false);
                        }
                        break;
                    //Para llamar al FSNPanelInfo
                    case "CODPRO":
					case "PROVEEDOR":
                            posicionX=$(e.getNewSelectedCells().getCell(0).get_element()).position().left + ($(e.getNewSelectedCells().getCell(0).get_element()).innerWidth() / 4);
    			            posicionY=$(e.getNewSelectedCells().getCell(0).get_element()).position().top + 20;
                            FSNMostrarPanel(ProveAnimationClientID, e, ProveDynamicPopulateClienteID, row.get_cellByColumnKey("CODPRO").get_value(),null,null,posicionX,posicionY);                            
						    break;
					case "PETICIONARIO":
                            posicionX=$(e.getNewSelectedCells().getCell(0).get_element()).position().left + ($(e.getNewSelectedCells().getCell(0).get_element()).innerWidth() / 4);
    			            posicionY=$(e.getNewSelectedCells().getCell(0).get_element()).position().top + 20;
                            if (row.get_cellByColumnKey("CODPET").get_value() != ""){
						        FSNMostrarPanel(PeticionarioAnimationClientID, e, PeticionarioDynamicPopulateClienteID, row.get_cellByColumnKey("CODPET").get_value(),null,null,posicionX,posicionY);
                            }
						    break;
                    default:
                        //Redirigir a detalleContrato si no esta en proceso   
                    	IdFiltroUsu=document.getElementById("<%=PMFiltroUsuarioIdVContr.ClientID%>").value;
						IdFiltro=document.getElementById("<%=PMFiltroIdVContr.ClientID%>").value;
                        TablaNombre = document.getElementById("<%=PMNombreTablaVContr.ClientID%>").value;   
                        CodigoContrato = document.getElementById("<%=txtCodigoContrato.ClientID%>").value;
                        CodigoProveedor = document.getElementById("<%=hidProveedor.ClientID%>").value;
                        CodigoProveedorCIF = document.getElementById("<%=hidProveedorCIF.ClientID%>").value;
                        CodigoProveedorText = document.getElementById("<%=txtProveedor.ClientID%>").value;
                        ListaTiposAnt = '';
                        var i;
                        dropdown = $find("<%=wddTipo.ClientID %>");
                        if (dropdown){
                            for (i = 0; i < dropdown.get_selectedItems().length; i++) {
                                if (ListaTiposAnt == '') {
                                    ListaTiposAnt = dropdown.get_selectedItems()[i].get_value();
                                } else {
                                    ListaTiposAnt = ListaTiposAnt + '$$$' + dropdown.get_selectedItems()[i].get_value();
                                }
                            }
                        }

							$.when($.ajax({       
								type: 'POST',
								url: rutaFS + '_Common/App_services/Consultas.asmx/ComprobarEnProceso',
								data: JSON.stringify({ contextKey: row.get_cellByColumnKey("ID").get_value() }),
								contentType: 'application/json; charset=utf-8',
								dataType: 'json',
								async: true
							})).done(function (msg) {
								if (msg.d=="1"){
                                    posicionX=$(e.getNewSelectedCells().getCell(0).get_element()).position().left + ($(e.getNewSelectedCells().getCell(0).get_element()).innerWidth() / 4);
    			                    posicionY=$(e.getNewSelectedCells().getCell(0).get_element()).position().top + 20;                    
									FSNMostrarPanel(ProcesoAnimationClientID,event,ProcesoDynamicPopulateClienteID,5,null,null,posicionX,posicionY);						
								}else{
									TempCargando();
									var Icono, Trasladada, Instancia, Contrato, Observador, Codigo
                                    Icono = row.get_cellByColumnKey("ICONO").get_value();
                                    Trasladada = row.get_cellByColumnKey("TRASLADADA").get_value();
                                    Instancia = row.get_cellByColumnKey("ID").get_value();
                                    Contrato = row.get_cellByColumnKey("ID_CONTRATO").get_value();
                                    Codigo = row.get_cellByColumnKey("CODIGO").get_value();
                                    Observador = row.get_cellByColumnKey("OBSERVADOR").get_value();

                                    svolver = "*volver=<%=ConfigurationManager.AppSettings("rutaPM2008")%>Contratos/VisorContratos.aspx?FiltroUsuarioIdAnt=" + IdFiltroUsu + "**NombreTablaAnt=" + TablaNombre + "**FiltroIdAnt=" + IdFiltro + "**CodigoContratoAnt=" + CodigoContrato + '**CodigoProveedorAnt=' + CodigoProveedor + '**CodigoProveedorCifAnt=' + CodigoProveedorCIF  + '**CodigoProveedorTextAnt=' + CodigoProveedorText + '**ListaTiposAnt=' + ListaTiposAnt
                                    sref= "<%=ConfigurationManager.AppSettings("rutaPM")%>frames.aspx?SessionId=<%=Session("sSession")%>&pagina=contratos/ComprobarAprobContratos.aspx?Instancia=" + Instancia + "*Contrato=" + Contrato + "*Codigo=" + Codigo + "*ConfiguracionGS=<%=Session("desdeGS")%>"
                                    window.open(sref  + svolver ,"_self");	
								}
							});   
                    break;
               }        
        }
</script>
    <ig:WebExcelExporter ID="wdgExcelExporter" runat="server"></ig:WebExcelExporter>
	<ig:WebDocumentExporter ID="wdgPDFExporter" runat="server" ExportMode="Download"></ig:WebDocumentExporter>
    <fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
    <table width="100%" cellpadding="0" border="0">
        <tr>
            <td width="60%">
                <table width="100%" cellpadding="0" border="0">
                    <tr>
                        <td>
                            <asp:Label ID="lblCodigoContrato" runat="server" Text="DC�digo" 
                                CssClass="Etiqueta"></asp:Label>
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtCodigoContrato" runat="server" Width="112px" BackColor="White"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td>
                            <asp:Label ID="lblProveedor" runat="server" Text="DProveedor" CssClass="Etiqueta"></asp:Label>
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table style="background-color: White; width: 150px; border: solid 1px #BBBBBB" cellpadding="0"
                                        cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtProveedor" runat="server" Width="240px" BorderWidth="0px" BackColor="White"
                                                    Height="16px"></asp:TextBox>
                                                <ajx:AutoCompleteExtender ID="txtProveedor_AutoCompleteExtender" runat="server" CompletionSetCount="10"
                                                    DelimiterCharacters="" Enabled="True" MinimumPrefixLength="1" ServiceMethod="GetProveedores"
                                                    ServicePath="~/App_Pages/_Common/App_Services/AutoComplete.asmx" TargetControlID="txtProveedor" EnableCaching="False" OnClientItemSelected="selected_Proveedor">
                                                </ajx:AutoCompleteExtender>
                                            </td>
                                            <td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px">
                                                <asp:ImageButton ID="imgProveedorLupa" runat="server" SkinID="BuscadorLupa" />
                                            </td>
                                        </tr>
                                        <asp:HiddenField ID="hidProveedor" runat="server" />
                                        <asp:HiddenField ID="hidProveedorCIF" runat="server" />
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td>
                            <asp:Label ID="lblTipo" runat="server" Text="DTipo:" CssClass="Etiqueta"></asp:Label>
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                                <ContentTemplate>
                                    <ig:WebDropDown ID="wddTipo" runat="server" Width="250px" BackColor="White" EnableMultipleSelection="true"
                                        MultipleSelectionType="Checkbox" EnableClosingDropDownOnSelect="false">
                                    </ig:WebDropDown>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="5%">
                <!-- Columna en la que esta el boton de buscador de procesos -->
                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <fsn:FSNButton ID="btnBuscarIdentificador" runat="server" Text="Buscar" Alineacion="Left"></fsn:FSNButton>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td rowspan="2" width="35%" align="center">
                <!-- Columna en la que estan las alertas de Contratos -->
                <asp:UpdatePanel ID="upAlerta" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:PlaceHolder ID="phAlerta" runat="server" Visible="false">
                            <div>
                                <table cellpadding="2" cellspacing="0" border="0">
                                    <tr>
                                        <td rowspan="3">
                                            <asp:Image ID="imgAlerta" runat="server" SkinID="Alerta" />
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblSolPendientes" runat="server" CssClass="Etiqueta"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:LinkButton ID="lbPendientes" runat="server" Text="DVer solicitudes" class="Rotulo"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:LinkButton ID="lbExpirar" runat="server" Text="DVer Contratos Expirar" class="Rotulo"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </asp:PlaceHolder>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <!-- Fila en la que esta la busqueda de procesos -->
            <td runat="server" id="CeldaBuscadorProcesos" colspan="2">
                <table width="100%" cellpadding="0" border="0">
                    <tr>
                        <td colspan="7">
                            <asp:UpdatePanel ID="Up_Procesos" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <fieldset>
                                        <legend style="vertical-align: top">
                                            <asp:Label ID="lblProceso" runat="server" Text="dProceso"></asp:Label></legend>
                                        <table width="100%" cellpadding="0" border="0">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblAnyo" runat="server" Text="DAño:" CssClass="Etiqueta"></asp:Label>
                                                </td>
                                                <td>
                                                    <ig:WebDropDown ID="ddlAnyo" runat="server" AutoPostBack="true" EnableMultipleSelection="false"
                                                        EnableClosingDropDownOnSelect="true" BackColor="White" Width="70px" EnableCustomValues="false"
                                                        DropDownContainerWidth="80" EnableDropDownAsChild="false" EnableCustomValueSelection="false"
                                                         EnableMarkingMatchedText="true" AutoSelectOnMatch="true" ClientEvents-DropDownOpening="CancelDropDownOpening">
                                                    </ig:WebDropDown>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCommodity" runat="server" Text="DCommodity:" CssClass="Etiqueta"></asp:Label>
                                                </td>
                                                <td>
                                                    <ig:WebDropDown ID="ddlCommodity" runat="server" Width="216px" BackColor="White"
                                                        EnableClosingDropDownOnSelect="true" DropDownAnimationDuration="1" DropDownContainerWidth="236"
                                                        EnableDropDownAsChild="false" Height="22px" AutoPostBack="true" ClientEvents-DropDownOpening="CancelDropDownOpening">
                                                        <ClientEvents ValueChanged="ValueChanged_wddCommodity" />
                                                    </ig:WebDropDown>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCodigo" runat="server" Text="DCodigo:" CssClass="Etiqueta"></asp:Label>
                                                </td>
                                                <td>
                                                    <ig:WebDropDown ID="ddlCodigo" runat="server" Width="70px" BackColor="White" EnableClosingDropDownOnSelect="true"
                                                        DropDownAnimationDuration="0" DropDownContainerWidth="80" EnableDropDownAsChild="false" EnableAutoCompleteFirstMatch="false"
                                                        AutoPostBack="true" EnableCustomValues="false" EnableMarkingMatchedText="false" EnableCustomValueSelection="false" EnableMultipleSelection="false" ClientEvents-DropDownOpening="CancelDropDownOpening" AutoSelectOnMatch="false" CurrentValue="">
                                                    </ig:WebDropDown>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDenominacion" runat="server" Text="DDenominacion:" CssClass="Etiqueta"></asp:Label>
                                                </td>
                                                <td>
                                                    <ig:WebDropDown ID="ddlDenominacion" runat="server" Width="256px" BackColor="White"
                                                        EnableClosingDropDownOnSelect="true" DropDownAnimationDuration="1" DropDownContainerWidth="286"
                                                        EnableDropDownAsChild="false" AutoPostBack="true" Height="22px" EnableMultipleSelection="false" CurrentValue="">
                                                    </ig:WebDropDown>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ImgBuscadorProcesos" runat="server" SkinID="BuscadorLupa" OnClientClick="show_buscadorProcesos(); return false;" />
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlCabeceraParametros" runat="server" Width="99%">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding-top: 5px;
            padding-bottom: 5px">
            <tr>
                <td valign="middle" align="left" style="padding-top: 5px">
                    <asp:Panel runat="server" ID="pnlBusquedaAvanzada" Style="cursor: pointer" Font-Bold="True"
                        Height="20px" Width="100%" CssClass="PanelCabecera">
                        <table border="0">
                            <tr>
                                <td>
                                    <asp:Image ID="imgExpandir" runat="server" SkinID="Contraer" />
                                </td>
                                <td style="vertical-align: top">
                                    <asp:Label ID="lblBusquedaAvanzada" runat="server" Text=" DSeleccione los criterios de b�squeda generales"
                                        Font-Bold="True" ForeColor="White"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlParametros" runat="server" CssClass="Rectangulo" Width="100%">
        <asp:UpdatePanel ID="upBusquedaAvanzada" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <fspm:BusquedaAvanzadaCTR ID="Busqueda" runat="server"></fspm:BusquedaAvanzadaCTR>
                <table width="80%" border="0" cellpadding="4" cellspacing="0">
                    <tr>
                        <td width="60px">
                            <fsn:FSNButton ID="btnBuscarBusquedaAvanzada" runat="server" Text="DBuscar" Alineacion="left"></fsn:FSNButton>
                        </td>
                        <td width="60px">
                            <fsn:FSNButton ID="btnLimpiarBusquedaAvanzada" runat="server" Text="DLimpiar" Alineacion="Right"></fsn:FSNButton>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajx:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" CollapseControlID="pnlBusquedaAvanzada"
        ExpandControlID="pnlBusquedaAvanzada" SuppressPostBack="true" TargetControlID="pnlParametros"
        ImageControlID="imgExpandir" SkinID="FondoRojo" Collapsed="true">
    </ajx:CollapsiblePanelExtender>
    <br />
    <asp:UpdatePanel ID="updFiltros" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <asp:HiddenField ID="hCacheLoaded" runat="server" Value="0" />
			<asp:HiddenField ID="PMFiltroUsuarioIdVContr" runat="server" Value="0" />
			<asp:HiddenField ID="PMFiltroIdVContr" runat="server" Value="0" />
			<asp:HiddenField ID="PMNombreTablaVContr" runat="server" Value="" />
                <div style="margin-right:15px;">
                    <fsn:FSNButton ID="btnAnyadirFiltros" runat="server" style="margin-left:10px;" Text="DA�adir filtros"></fsn:FSNButton>
                </div>
                <div>
                    <fsn:FSNButton ID="btnConfigurar" runat="server" Text="DConfigurar"></fsn:FSNButton>
                </div>
                <div>
                    <asp:DataList ID="dlFiltrosConfigurados" Width="220px" runat="server" DataSourceID="odsFiltrosConfigurados"
                        RepeatDirection="Horizontal" CellPadding="0" RepeatLayout="Table" RepeatColumns="5">
                        <ItemTemplate>
                            <td>
                                <fsn:FSNTab ID="fsnTabFiltro" Alineacion="Left" runat="server" Visible="true" SkinID="PestanyasFiltros"
                                    Text='<%# IIf(Eval("ID")=0,Textos(8),Mid(Eval("NOM_FILTRO"),1,20)) & IIf(Len(Eval("NOM_FILTRO"))>20,"...","") %>'
                                    ToolTip='<%# IIf(Eval("ID")=0,"",Eval("NOM_FILTRO")) %>' OnClick="fsnTabFiltro_Click"
                                    CommandArgument='<%# Eval("ID") & "#" & Eval("NOM_TABLA")  & "#" & Eval("IDFILTRO") %>'
                                    BorderStyle="None" BorderColor="#AAAAAA" BorderWidth="1px"></fsn:FSNTab>
                            </td>
                        </ItemTemplate>
                    </asp:DataList>
                </div>     
            </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Button runat="server" ID="btnAprobar" style="display:none; " />	

    <asp:UpdatePanel ID="updGridContratos" ChildrenAsTriggers="false" runat="server" UpdateMode="Conditional">						
			<Triggers>
				<asp:AsyncPostBackTrigger ControlID="btnBuscarBusquedaAvanzada" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnPager" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnPagerBD" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnAprobar" EventName="Click" />
			</Triggers>
			<ContentTemplate>
            <asp:HiddenField ID="hCacheTabsEstados" runat="server" Value="0" />
            <table border="0" width="99%" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <asp:Panel ID="panTabsEstados" runat="server" BackColor="#aaaaaa" Height="28px" Width="100%">
                            <div style="border: 1px solid #aaaaaa;">
                                <table id="tblEstados" width="100%" border="0">
                                    <tr>
                                        <td nowrap="nowrap">
                                            <fsn:FSNButton ID="btnBoton1" runat="server" Alineacion="left" Font-Size="12px" SkinID="SubmenuFiltros"
                                                ColorFondo="" ColorFondoHover="" ImagenesBordes="" Text="Borrador()" />
                                        </td>
                                        <td nowrap="nowrap">
                                            <fsn:FSNButton ID="btnBoton2" runat="server" Alineacion="left" Font-Size="12px" SkinID="SubmenuFiltros"
                                                ColorFondo="" ColorFondoHover="" ImagenesBordes="" Text="En curso de Aprobaci�n()" />
                                        </td>
                                        <td nowrap="nowrap">
                                            <fsn:FSNButton ID="btnBoton3" runat="server" Alineacion="left" Font-Size="12px" SkinID="SubmenuFiltros"
                                                ColorFondo="" ColorFondoHover="" ImagenesBordes="" Text="Vigentes()" />
                                        </td>
                                        <td nowrap="nowrap">
                                            <fsn:FSNButton ID="btnBoton4" runat="server" Alineacion="left" Font-Size="12px" SkinID="SubmenuFiltros"
                                                ColorFondo="" ColorFondoHover="" ImagenesBordes="" Text="Pr�ximo a expirar()" />
                                        </td>
                                        <td nowrap="nowrap">
                                            <fsn:FSNButton ID="btnBoton5" runat="server" Alineacion="left" Font-Size="12px" SkinID="SubmenuFiltros"
                                                ColorFondo="" ColorFondoHover="" ImagenesBordes="" Text="Expirados()" />
                                        </td>
                                        <td nowrap="nowrap">
                                            <fsn:FSNButton ID="btnBoton6" runat="server" Alineacion="left" Font-Size="12px" SkinID="SubmenuFiltros"
                                                ColorFondo="" ColorFondoHover="" ImagenesBordes="" Text="Rechazados()" />
                                        </td>
                                        <td nowrap="nowrap">
                                            <fsn:FSNButton ID="btnBoton7" runat="server" Alineacion="left" Font-Size="12px" SkinID="SubmenuFiltros"
                                                ColorFondo="" ColorFondoHover="" ImagenesBordes="" Text="Anulados()" />
                                        </td>
                                        <td nowrap="nowrap">
                                            <fsn:FSNButton ID="btnBoton8" runat="server" Alineacion="left" Font-Size="12px" SkinID="SubmenuFiltros"
                                                ColorFondo="" ColorFondoHover="" ImagenesBordes="" Text="Todos()" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>                            
                            <ig:WebHierarchicalDataGrid runat="server" ID="whgContratosExportacion" 
                                Visible="false" AutoGenerateBands="false"
					            AutoGenerateColumns="false" Width="100%" EnableAjax="true" 
					            EnableAjaxViewState="false" EnableDataViewState="false">
                                <Behaviors>
                                    <ig:Filtering Enabled="true"></ig:Filtering>
                                    <ig:Sorting Enabled="true" SortingMode="Multi"></ig:Sorting>
                                    <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
                                </Behaviors>
                            </ig:WebHierarchicalDataGrid>
                            <ig:WebHierarchicalDataGrid runat="server" ID="whgContratos" AutoGenerateBands="false"
					                AutoGenerateColumns="false" Width="100%" EnableAjax="true" 
					                EnableAjaxViewState="false" EnableDataViewState="false">
					        <GroupingSettings EnableColumnGrouping="True" GroupAreaVisibility="Visible"></GroupingSettings>
					        <Behaviors>
                                <ig:Selection CellClickAction="Cell" CellSelectType="Single" SelectionClientEvents-CellSelectionChanging="onCellSelectionChanged"></ig:Selection>
                                <ig:Activation Enabled="true"/>
						        <ig:Filtering Alignment="Top" Enabled="true" Visibility="Visible" AnimationEnabled="false">
                                    <FilteringClientEvents DataFiltering="whgContratos_Filtering" />
                                </ig:Filtering>
						        <ig:Sorting Enabled="true" SortingMode="Multi" SortingClientEvents-ColumnSorting="whgContratos_Sorting"></ig:Sorting>
						        <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
						        <ig:ColumnMoving Enabled="false"></ig:ColumnMoving>								
						        <ig:Paging Enabled="true" PagerAppearance="Top">
							        <PagerTemplate>
								        <div class="CabeceraBotones" style="clear:both; float:left; width:100%;"> 
									        <div style="float:left; padding:3px 0px 2px 0px; margin-left:10px;">
										        <div style="clear: both; float: left; margin-right: 5px;">
                                                    <asp:Image ID="ImgBtnFirst" runat="server" OnClientClick="FirstPage()" style="margin-right:2px;" />
                                                    <asp:Image ID="ImgBtnPrev" runat="server" OnClientClick="PrevPage()" />
                                                </div>
                                                <div style="float: left; margin-right:5px;">
                                                    <asp:Label ID="lblPage" runat="server" Text="dPage" Style="margin-right: 3px;"></asp:Label>
                                                    <asp:DropDownList ID="PagerPageList" runat="server" Style="margin-right: 3px;" onchange="return IndexChanged()" />
                                                    <asp:Label ID="lblOF" runat="server" Text="dOf" Style="margin-right: 3px;"></asp:Label>
                                                    <asp:Label ID="lblCount" runat="server" />
                                                </div>
                                                <div style="float: left;">
                                                    <asp:Image ID="ImgBtnNext" runat="server" OnClientClick="NextPage()" style="margin-right:2px;" />
                                                    <asp:Image ID="ImgBtnLast" runat="server" OnClientClick="LastPage()" />
                                                </div>
									        </div>
									        <div style="float:right; margin:5px 5px 0px 0px;">
										        <div onclick="ExportarExcel()" class="botonDerechaCabecera" style="float:left;">            
											        <asp:Image runat="server" ID="imgExcel" CssClass="imagenCabecera" />
											        <asp:Label runat="server" ID="lblExcel" CssClass="fntLogin2" style="margin-left:3px;" Text="dExcel"></asp:Label>           
										        </div>
										        <div onclick="ExportarPDF()" class="botonDerechaCabecera" style="float:left;">            
											        <asp:Image runat="server" ID="imgPDF" CssClass="imagenCabecera" />
											        <asp:Label runat="server" ID="lblPDF" CssClass="fntLogin2" style="margin-left:3px;" Text="dPDF"></asp:Label>           
										        </div>
									        </div>
								        </div>						
							        </PagerTemplate>
						        </ig:Paging>						
					        </Behaviors>
                            <Columns>
                            </Columns>
				</ig:WebHierarchicalDataGrid>        
                        </div>
                    </td>
                </tr>
            </table>
            <div id="div_Label" runat="server" class="EtiquetaScroll" style="display: none; position: absolute">
                <asp:Label ID="label2" runat="server" Font-Bold="true"></asp:Label>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Button runat="server" ID="btnOrder" style="display:none;" />
    <asp:Button runat="server" ID="btnFilter" style="display:none;" />
    <asp:Button runat="server" ID="btnRecargaCache" style="display:none;" />    
    <asp:UpdatePanel runat="server" ID="upPager" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnOrder" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnFilter" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnRecargaCache" EventName="Click" />            
        </Triggers>
        <ContentTemplate>
            <asp:HiddenField runat="server" ID="hFiltered" Value="0" />
            <asp:HiddenField runat="server" ID="hFilteredColumns" Value="" />
            <asp:HiddenField runat="server" ID="hFilteredCondition" Value="" />
            <asp:HiddenField runat="server" ID="hFilteredValue" Value="" />
            <asp:HiddenField runat="server" ID="hOrdered" Value="" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Button runat="server" ID="btnPager" style="display:none;" />	
    <asp:Button runat="server" ID="btnPagerBD" style="display:none;" />

    <asp:ObjectDataSource ID="odsFiltrosConfigurados" runat="server" SelectMethod="LoadConfigurados"
        TypeName="Fullstep.FSNServer.FiltrosContrato">
        <SelectParameters>
            <asp:Parameter Name="sUsuario" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:UpdatePanel ID="updPanelHidden" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:HiddenField ID="hid_Identificador" runat="server" />
            <asp:HiddenField ID="hid_CodContrato" runat="server" />
            <asp:HiddenField ID="hid_IdNC" runat="server" />
            <asp:HiddenField ID="hid_CodEmpresa" runat="server" />
            <asp:HiddenField ID="hid_Empresa" runat="server" />
            <asp:HiddenField ID="hid_Estado" runat="server" />
            <asp:HiddenField ID="hid_CodPeticionario" runat="server" />
            <asp:HiddenField ID="hid_CodProveedor" runat="server" />
            <asp:HiddenField ID="hid_Proveedor" runat="server" />
            <asp:HiddenField ID="hid_Cellkey" runat="server" />
            <asp:HiddenField ID="hid_Icono" runat="server" />
            <asp:HiddenField ID="hid_Trasladada" runat="server" />
            <asp:HiddenField ID="hid_Observador" runat="server" />
            <asp:HiddenField ID="hid_InstanciaEstado" runat="server" />
            <asp:HiddenField ID="hid_Contrato" runat="server" />
            <asp:HiddenField ID="hid_ContratosAprobar" runat="server" />
            <asp:HiddenField ID="hid_ContratosRechazar" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <br />

    <fsn:FSNPanelInfo ID="FSNPanelDatosProveedor" runat="server" 
		ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_DatosProveedor" 
		TipoDetalle="1" />
	<fsn:FSNPanelInfo ID="FSNPanelDatosPersona" runat="server" 
		ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_DatosPersona" 
		TipoDetalle="2"/>
	<fsn:FSNPanelInfo ID="FSNPanelDatosEmpresa" runat="server" 
		ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_DatosEmpresa" 
		TipoDetalle="4" />
	<fsn:FSNPanelInfo ID="FSNPanelEnProceso" runat="server" 
		ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_TextoEnProceso" 
		TipoDetalle="4"
		Height="120px" Width="370px"/>
</asp:Content>
