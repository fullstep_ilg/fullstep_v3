﻿Imports Fullstep.FSNLibrary.TiposDeDatos

Partial Public Class AltaContratos
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Solicitudes As FSNServer.Solicitudes

        Dim mMaster = CType(Me.Master, FSNWeb.Menu)
        mMaster.Seleccionar("Contratos", "AltaContratos")

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Contratos

        FSNPageHeader.TituloCabecera = Textos(0)
        FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/AltaContrato.png"

        Me.lblParrafo.Text = Textos(1)
        Me.lblCabeceraAltaSolicitudes.Text = Textos(2)
        Me.lblFooter.Text = Textos(3)
        Me.lblSinDatos.Text = Textos(4)

        Solicitudes = FSNServer.Get_Object(GetType(FSNServer.Solicitudes))
        Solicitudes.LoadData(FSNUser.Cod, Idioma, iTipoSolicitud:=TipoDeSolicitud.Contrato, bNuevoWorkfl:=True, sCodPer:=FSNUser.CodPersona)

        If Solicitudes.Data.Tables.Count > 0 Then
            uwgSolicitudes_WDG.DataSource = Solicitudes.Data
            uwgSolicitudes_WDG.DataBind()
        Else
            uwgSolicitudes_WDG.Visible = False
            Me.tblSolicitudes.Visible = False
            Me.lblSinDatos.Visible = True
        End If
    End Sub

    Private Sub uwgSolicitudes_WDG_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles uwgSolicitudes_WDG.InitializeRow
        Dim Index As Integer
        If CType(e.Row.DataItem, System.Data.DataRowView).Row("ADJUN") > 0 Then
            Index = sender.Columns.FromKey("IMGADJUN").Index
            e.Row.Items.Item(Index).Text = "<img src='" & ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/coment.gif" & "' style='text-align:center;'/>"
        End If

        Index = sender.Columns.FromKey("IMGIMPORTAR").Index
        e.Row.Items.Item(Index).Text = "<img src='" & ConfigurationManager.AppSettings("ruta") & "Images/importar.gif" & "' style='text-align:center;'/>"

    End Sub

End Class