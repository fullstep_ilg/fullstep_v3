﻿Imports Fullstep.FSNServer
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports Infragistics.Web.UI.GridControls
Imports Infragistics.Documents.Reports.Report
Partial Public Class VisorContratos
    Inherits FSNPage

    Private arrDatosGenerales() As String
    Private arrDatosGeneralesDen() As String
    Private arrOrdenEstados(7) As String
    Private arrTextosEstados(7) As String
    Private arrTextosEstadosMini(7) As String
    Private m_bMostrarComentario As Boolean
    Private m_bSaltarLayout As Byte = 0
    Private m_bExportando As Boolean = False
    Private m_sTextoCargando As String = "Cargando..."
    Private m_sTextoEnProceso As String = "En curso de aprobación"
    Private m_bConConfiguracionAnterior As Boolean = False
    Private m_bBusqueda As Boolean = False
    Private seleccionadoEstadoGrid As Boolean = False
    Private m_desdeGS As Boolean = False
    Private bMostrarColumnaAprobar As Boolean
    Private bMostrarColumnaRechazar As Boolean
    Private _exportacion As Boolean = False
    Private _dsContratos As DataSet
    Private _pageNumber As Integer = 0
    ''' <summary>
    ''' Devuelve los contratos
    ''' </summary>
    ''' <returns>Dataset con los contratos</returns>
    ''' <remarks>Llamada desde: ReCargarContratos, Tiempo maximo: 0,2</remarks>
    Protected ReadOnly Property Contratos() As DataSet
        Get
            Dim bOrdenacionEnBDVacio As Boolean = False
            If CType(hFiltered.Value, Boolean) OrElse _exportacion Then
                whgContratos.GridView.Behaviors.Paging.PageIndex = _pageNumber
                Dim dsAuxiliar As New DataSet
                If hCacheTabsEstados.Value = 0 Then RecargarCache()
                hCacheTabsEstados.Value = 1
                dsAuxiliar = CType(Cache("dsContratos_" & FSNUser.Cod), DataSet).Copy
                Dim sFiltro As String = String.Empty
                Dim sFilterCondition As String = String.Empty
                Dim fColumns As String() = Split(hFilteredColumns.Value, "#")
                Dim fConditions As String() = Split(hFilteredCondition.Value, "#")
                Dim fValues As String() = Split(hFilteredValue.Value, "#")
                For i As Integer = 0 To fColumns.Length - 1
                    If Not fColumns(i) = String.Empty Then
                        Select Case UCase(dsAuxiliar.Tables("ContratosFiltrados").Columns(fColumns(i)).DataType.FullName)
                            Case "SYSTEM.DATETIME"
                                Select Case CType(fConditions(i), Integer)
                                    Case 2
                                        sFilterCondition = " < '" & fValues(i) & "'"
                                    Case 3
                                        sFilterCondition = " > '" & fValues(i) & "'"
                                    Case 5
                                        sFilterCondition = " = '" & Today & "'"
                                    Case Else
                                        sFilterCondition = " = '" & fValues(i) & "'"
                                End Select
                            Case "SYSTEM.STRING"
                                Select Case CType(fConditions(i), Integer)
                                    Case 2
                                        sFilterCondition = " NOT LIKE '" & fValues(i) & "'"
                                    Case 3
                                        sFilterCondition = " LIKE '" & fValues(i) & "%'"
                                    Case 4
                                        sFilterCondition = " LIKE '%" & fValues(i) & "'"
                                    Case 5
                                        sFilterCondition = " LIKE '%" & fValues(i) & "%'"
                                    Case 6
                                        sFilterCondition = " NOT LIKE '%" & fValues(i) & "%'"
                                    Case 7
                                        sFilterCondition = " IS NULL"
                                    Case 8
                                        sFilterCondition = " IS NOT NULL"
                                    Case Else
                                        sFilterCondition = " LIKE '" & fValues(i) & "'"
                                End Select
                            Case Else
                        End Select
                        sFilterCondition = "(" & fColumns(i) & sFilterCondition & ")"
                        sFiltro = IIf(sFiltro = String.Empty, sFilterCondition, sFiltro & " AND " & sFilterCondition)
                    End If
                Next
                With dsAuxiliar.DefaultViewManager
                    With .DataViewSettings("ContratosFiltrados")
                        .RowFilter = sFiltro
                    End With
                End With
                _dsContratos = New DataSet
                _dsContratos.Tables.Add(dsAuxiliar.DefaultViewManager.DataViewSettings("ContratosFiltrados").Table.DefaultView.ToTable.Copy)

                InicializarArraysCamposGenerales()

                Dim mi_boton As New Fullstep.FSNWebControls.FSNButton
                For index As Integer = 1 To 8
                    mi_boton = updGridContratos.FindControl("btnBoton" & index)
                    Select Case mi_boton.Attributes("Estado")
                        Case EstadosVisorContratos.Guardado
                            mi_boton.Text = arrTextosEstadosMini(index - 1) & " (" + CStr(mi_boton.Attributes("NumeroContratos")) + ")"
                            mi_boton.ToolTip = arrTextosEstados(index - 1) & " (" + CStr(mi_boton.Attributes("NumeroContratos")) + ")"
                        Case EstadosVisorContratos.En_Curso_De_Aprobacion
                            mi_boton.Text = arrTextosEstadosMini(index - 1) & " (" + CStr(mi_boton.Attributes("NumeroContratos")) + ")"
                            mi_boton.ToolTip = arrTextosEstados(index - 1) & " (" + CStr(mi_boton.Attributes("NumeroContratos")) + ")"
                        Case EstadosVisorContratos.Vigentes
                            mi_boton.Text = arrTextosEstadosMini(index - 1) & " (" + CStr(mi_boton.Attributes("NumeroContratos")) + ")"
                            mi_boton.ToolTip = arrTextosEstados(index - 1) & " (" + CStr(mi_boton.Attributes("NumeroContratos")) + ")"
                        Case EstadosVisorContratos.Proximo_a_Expirar
                            mi_boton.Text = arrTextosEstadosMini(index - 1) & " (" + CStr(mi_boton.Attributes("NumeroContratos")) + ")"
                            mi_boton.ToolTip = arrTextosEstados(index - 1) & " (" + CStr(mi_boton.Attributes("NumeroContratos")) + ")"
                        Case EstadosVisorContratos.Expirados
                            mi_boton.Text = arrTextosEstadosMini(index - 1) & " (" + CStr(mi_boton.Attributes("NumeroContratos")) + ")"
                            mi_boton.ToolTip = arrTextosEstados(index - 1) & " (" + CStr(mi_boton.Attributes("NumeroContratos")) + ")"
                        Case EstadosVisorContratos.Rechazados
                            mi_boton.Text = arrTextosEstadosMini(index - 1) & " (" + CStr(mi_boton.Attributes("NumeroContratos")) + ")"
                            mi_boton.ToolTip = arrTextosEstados(index - 1) & " (" + CStr(mi_boton.Attributes("NumeroContratos")) + ")"
                        Case EstadosVisorContratos.Anulados
                            mi_boton.Text = arrTextosEstadosMini(index - 1) & " (" + CStr(mi_boton.Attributes("NumeroContratos")) + ")"
                            mi_boton.ToolTip = arrTextosEstados(index - 1) & " (" + CStr(mi_boton.Attributes("NumeroContratos")) + ")"
                        Case EstadosVisorContratos.Todas
                            mi_boton.Text = arrTextosEstadosMini(index - 1) & " (" + CStr(mi_boton.Attributes("NumeroContratos")) + ")"
                            mi_boton.ToolTip = arrTextosEstados(index - 1) & " (" + CStr(mi_boton.Attributes("NumeroContratos")) + ")"
                    End Select
                Next
            Else
                Dim cFiltro As FiltroContrato
                cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroContrato))
                'Aqui es donde tengo que mirar que cosas se hacen en contratos y que en certificados no antes de llamar 
                'a la la funcion devolverfiltros
                Dim sEstadoContadores As String = ""
                Dim sMaterial As String = String.Empty
                Dim sEstado As String = String.Empty
                Dim sCodProceso As Integer = 0
                Dim iAlertaExpiracion As Integer = 0
                Dim sNotificado As String = String.Empty

                Dim sAnyo As Short = 0
                Dim material() As String
                Dim sCodigoContrato As String = String.Empty
                Dim sTipo As String = String.Empty
                Dim sProveedor As String
                Dim sArticulo As String = ""
                Dim bVigentesSeleccionado As Boolean = False
                Dim iBotonesConContratos As Byte = 0
                'Dim ContratosFiltrados As DataRow()
                Dim dtContratosFiltrados As New DataTable
                Dim mi_boton As New Fullstep.FSNWebControls.FSNButton
                'If seleccionadoEstadoGrid Then
                sEstado = CStr(Session("CONTR_FilaEstado"))
                Dim sSeparador As Short = Busqueda.Estado.IndexOf(",")
                sEstadoContadores = Busqueda.Estado
                If Busqueda.Estado <> "" And Busqueda.Estado.IndexOf(sEstado) = -1 Then
                    If sEstado = "8" Then 'Ha pulsado "Todos"
                        sEstado = Busqueda.Estado
                    Else
                        sEstado = "-1"
                    End If
                End If

                If Busqueda.EmpresaHidden = "" Then
                    If Busqueda.EmpresaTexto <> "" Then
                        'Si se ha escrito algo en el campo empresa pero no se ha seleccionado del Autocompletar ponemos -1 para que no devuelva nada
                        Busqueda.EmpresaHidden = "-1"
                    End If
                End If

                If Busqueda.ArticuloHidden = "" Then
                    sArticulo = Busqueda.ArticuloTexto
                Else
                    sArticulo = Busqueda.ArticuloHidden
                End If

                If Busqueda.MaterialHidden <> "" Then
                    material = Split(Busqueda.MaterialHidden, "-")
                    Dim i As Byte
                    Dim lLongCod As Long

                    For i = 0 To material.GetUpperBound(0)
                        Select Case i
                            Case 0
                                lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                            Case 1
                                lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                            Case 2
                                lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                            Case 3
                                lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN4
                        End Select
                        If Trim(material(i)) = "" Then Exit For
                        sMaterial = sMaterial + Space(lLongCod - Len(material(i))) + material(i)
                    Next
                End If

                If txtCodigoContrato.Text <> "" Then
                    sCodigoContrato = txtCodigoContrato.Text
                End If

                For i = 0 To wddTipo.Items.Count - 1
                    If wddTipo.Items(i).Selected Then
                        If sTipo <> "" Then sTipo += "$$$"
                        sTipo = sTipo + wddTipo.Items(i).Value
                    End If
                Next

                sProveedor = hidProveedor.Value
                If ddlAnyo.SelectedValue <> "0" And ddlAnyo.SelectedValue <> "" Then
                    sAnyo = CType(ddlAnyo.SelectedValue, Short)
                End If

                If ddlCodigo.SelectedItems.Count > 0 Then
                    sCodProceso = CType(ddlCodigo.SelectedValue, Short)
                End If

                'Cargar La ordenacion del Filtro (por BBDD) 
                'o la de por defecto (por cookie o por "fecha de solicitud de más a menos recientes" sino hay cooki)
                Dim sOrdenacion As String = ""
                If hOrdered.Value = "" Then
                    If Session("IDFiltroContratoUsuario") = "" Then
                        Dim cookie As HttpCookie
                        cookie = Request.Cookies("ORDENACION_CONTR")
                        If Not cookie Is Nothing Then
                            sOrdenacion = cookie.Value
                            bOrdenacionEnBDVacio = (sOrdenacion = "")
                        End If
                    Else
                        cFiltro.IDFiltroUsuario = Session("IDFiltroContratoUsuario")
                        cFiltro.cargarOrdenacionFiltro()
                        sOrdenacion = cFiltro.Ordenacion
                        bOrdenacionEnBDVacio = (sOrdenacion = "")
                    End If
                    If InStr(sOrdenacion, "ID") = 0 Then sOrdenacion = IIf(sOrdenacion = "", "ID ASC", sOrdenacion & ",ID ASC")
                    hOrdered.Value = sOrdenacion
                Else
                    sOrdenacion = hOrdered.Value
                    bOrdenacionEnBDVacio = False
                End If

                iAlertaExpiracion = 0
                If Busqueda.AlertaExpiracion > 0 AndAlso Busqueda.AlertaPeriodo > 0 Then
                    Select Case Busqueda.AlertaPeriodo
                        Case 1
                            iAlertaExpiracion = Busqueda.AlertaExpiracion
                        Case 2
                            iAlertaExpiracion = Busqueda.AlertaExpiracion * 7
                        Case 3
                            iAlertaExpiracion = Busqueda.AlertaExpiracion * 30
                    End Select
                End If

                sNotificado = Busqueda.AlertaNotificado

                _pageNumber = IIf(HttpContext.Current.Session("pageNumberVCont") = "", 0, HttpContext.Current.Session("pageNumberVCont"))
                Dim _pageSize As Integer = System.Configuration.ConfigurationManager.AppSettings("PageSize")
                cFiltro.Visor_DevolverContratos(FSNUser.CodPersona, FSNUser.Idioma, sEstadoContadores, strToSQLLIKE(sCodigoContrato), sTipo, strToSQLLIKE(Busqueda.Descripcion), sProveedor, Busqueda.Peticionario,
                                                Busqueda.FechaInicio, Busqueda.FechaExpiracion, sMaterial, sArticulo, Busqueda.EmpresaHidden, ddlCommodity.SelectedValue, sCodProceso, sAnyo, sEstado,
                                                Session("PMCNombreTabla"), sOrdenacion, Busqueda.PalabraClave, _pageNumber + 1, _pageSize, CType(hFiltered.Value, Boolean), Busqueda.CentroCoste, Busqueda.PartidasPresUONS,
                                                iAlertaExpiracion, sNotificado)

                dtContratosFiltrados = cFiltro.ContratosVisor.Tables(0).Copy

                InicializarArraysCamposGenerales()

                For index As Integer = 1 To 8
                    mi_boton = updGridContratos.FindControl("btnBoton" & index)
                    Select Case mi_boton.Attributes("Estado")
                        Case EstadosVisorContratos.Guardado
                            mi_boton.Text = arrTextosEstadosMini(index - 1) & " (" + CStr(cFiltro.Guardados) + ")"
                            mi_boton.ToolTip = arrTextosEstados(index - 1) & " (" + CStr(cFiltro.Guardados) + ")"
                            mi_boton.Attributes.Add("NumeroContratos", cFiltro.Guardados)
                        Case EstadosVisorContratos.En_Curso_De_Aprobacion
                            mi_boton.Text = arrTextosEstadosMini(index - 1) & " (" + CStr(cFiltro.EnCursoAprobacion) + ")"
                            mi_boton.ToolTip = arrTextosEstados(index - 1) & " (" + CStr(cFiltro.EnCursoAprobacion) + ")"
                            mi_boton.Attributes.Add("NumeroContratos", cFiltro.EnCursoAprobacion)
                        Case EstadosVisorContratos.Vigentes
                            mi_boton.Text = arrTextosEstadosMini(index - 1) & " (" + CStr(cFiltro.Vigentes) + ")"
                            mi_boton.ToolTip = arrTextosEstados(index - 1) & " (" + CStr(cFiltro.Vigentes) + ")"
                            mi_boton.Attributes.Add("NumeroContratos", cFiltro.Vigentes)
                        Case EstadosVisorContratos.Proximo_a_Expirar
                            mi_boton.Text = arrTextosEstadosMini(index - 1) & " (" + CStr(cFiltro.ProximoAExpirar) + ")"
                            mi_boton.ToolTip = arrTextosEstados(index - 1) & " (" + CStr(cFiltro.ProximoAExpirar) + ")"
                            mi_boton.Attributes.Add("NumeroContratos", cFiltro.ProximoAExpirar)
                        Case EstadosVisorContratos.Expirados
                            mi_boton.Text = arrTextosEstadosMini(index - 1) & " (" + CStr(cFiltro.Expirados) + ")"
                            mi_boton.ToolTip = arrTextosEstados(index - 1) & " (" + CStr(cFiltro.Expirados) + ")"
                            mi_boton.Attributes.Add("NumeroContratos", cFiltro.Expirados)
                        Case EstadosVisorContratos.Rechazados
                            mi_boton.Text = arrTextosEstadosMini(index - 1) & " (" + CStr(cFiltro.Rechazados) + ")"
                            mi_boton.ToolTip = arrTextosEstados(index - 1) & " (" + CStr(cFiltro.Rechazados) + ")"
                            mi_boton.Attributes.Add("NumeroContratos", cFiltro.Rechazados)
                        Case EstadosVisorContratos.Anulados
                            mi_boton.Text = arrTextosEstadosMini(index - 1) & " (" + CStr(cFiltro.Anulados) + ")"
                            mi_boton.ToolTip = arrTextosEstados(index - 1) & " (" + CStr(cFiltro.Anulados) + ")"
                            mi_boton.Attributes.Add("NumeroContratos", cFiltro.Anulados)
                        Case EstadosVisorContratos.Todas
                            mi_boton.Text = arrTextosEstadosMini(index - 1) & " (" + CStr(cFiltro.Todas) + ")"
                            mi_boton.ToolTip = arrTextosEstados(index - 1) & " (" + CStr(cFiltro.Todas) + ")"
                            mi_boton.Attributes.Add("NumeroContratos", cFiltro.Todas)
                    End Select
                Next
                dtContratosFiltrados.TableName = "ContratosFiltrados"
                cFiltro.ContratosVisor.Tables.Add(dtContratosFiltrados)

                _dsContratos = New DataSet
                _dsContratos.Tables.Add(cFiltro.ContratosVisor.Tables("ContratosFiltrados").Copy)
                _dsContratos.Tables.Add(cFiltro.ContratosVisor.Tables(2).Copy)
                _dsContratos.Tables(0).TableName = "ContratosFiltrados"

                cFiltro = Nothing
            End If

            Dim sortedColumns() As String = Split(hOrdered.Value, ",")
            Dim groupedColumns() As String = Split(Session("ColsGroupBy" & Session("QAFiltroUsuarioIdVCert")), "#")

            Dim sItemSinOrder As String
            Dim sGrupoSinOrder As String
            Dim bNoOrdenar As Boolean

            For Each item As String In sortedColumns
                If Not item = "" Then
                    sItemSinOrder = Trim(Replace(Replace(UCase(item), " ASC", ""), " DESC", ""))

                    bNoOrdenar = False
                    'Comprueba q las columnas por las q ordenar no esten en las columnas a agrupar o el SortedColumns.Add fallara
                    For Each sGrupoConOrder As String In groupedColumns
                        sGrupoSinOrder = Trim(Replace(Replace(UCase(sGrupoConOrder), " ASC", ""), " DESC", ""))
                        If sGrupoSinOrder = sItemSinOrder Then
                            bNoOrdenar = True
                            Exit For
                        End If
                    Next

                    If Not bNoOrdenar Then
                        If Not bOrdenacionEnBDVacio Then 'Si hay datos creo q siempre esta bien
                            whgContratos.GridView.Behaviors.Sorting.SortedColumns.Add(Split(item, " ")(0),
                                IIf(Split(item, " ")(1) = "DESC", Infragistics.Web.UI.SortDirection.Descending, Infragistics.Web.UI.SortDirection.Ascending))
                        ElseIf Split(item, " ")(0) = "ID" Then
                            whgContratos.GridView.Behaviors.Sorting.SortedColumns.Add(Split(item, " ")(0),
                                IIf(Split(item, " ")(1) = "DESC", Infragistics.Web.UI.SortDirection.Descending, Infragistics.Web.UI.SortDirection.Ascending))
                        End If
                    End If
                End If
            Next

            For Each item As String In groupedColumns
                If Not item = "" Then whgContratos.GroupingSettings.GroupedColumns.Add(Split(item, " ")(0),
                    IIf(Split(item, " ")(1) = "DESC", Infragistics.Web.UI.GridControls.GroupingSortDirection.Descending, Infragistics.Web.UI.GridControls.GroupingSortDirection.Ascending))
            Next

            'para q el hOrdered quede fijado
            upPager.Update()
            _exportacion = False
            Return _dsContratos
        End Get
    End Property
#Region "Template columna CHECKBOX"
    Private Class CheckboxTemplate
        Implements ITemplate
        Private _id As String
        ''' <summary>
        ''' Rellena un Item del webdatagrid 
        ''' </summary>
        ''' <param name="container">Item del webdatagrid a rellenar</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Public Sub InstantiateIn(ByVal container As System.Web.UI.Control) Implements System.Web.UI.ITemplate.InstantiateIn
            Dim chk As New CheckBox
            chk.ID = _id
            chk.Width = Unit.Pixel(25)
            container.Controls.Add(chk)
        End Sub
        Public Sub New(ByVal Id As String)
            _id = Id
        End Sub
    End Class
#End Region
#Region "Template cabecera BUTTON"
    Private Class ButtonTemplate
        Implements ITemplate
        Private _texto As String
        Private _id As String
        ''' <summary>
        ''' Rellena un Item del webdatagrid 
        ''' </summary>
        ''' <param name="container">Item del webdatagrid a rellenar</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Public Sub InstantiateIn(ByVal container As System.Web.UI.Control) Implements System.Web.UI.ITemplate.InstantiateIn
            Dim divContenedor As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
            Dim divBoton As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
            Dim lblBoton As New System.Web.UI.HtmlControls.HtmlGenericControl("span")
            With divContenedor
                .Style.Add("position", "relative")
                .Style.Add("clear", "both")
                .Style.Add("float", "left")
                .Style.Add("width", "100%")
                .Style.Add("text-align", "center")
            End With
            With divBoton
                .ID = _id
                .Attributes.Add("class", "botonRedondeado")
            End With
            lblBoton.InnerText = _texto
            divBoton.Controls.Add(lblBoton)
            divContenedor.Controls.Add(divBoton)
            container.Controls.Add(divContenedor)
        End Sub
        Public Sub New(ByVal Id As String, ByVal Texto As String)
            _id = Id
            _texto = Texto
        End Sub
    End Class
#End Region
#Region "Pagina"
    ''' <summary>
    ''' Evento preinit en el que configuramos la apariencia de algunos controles
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Not String.IsNullOrEmpty(Session("desdeGS")) Then
            m_desdeGS = True
        End If
        If m_desdeGS Then
            Me.MasterPageFile = "~/App_Master/EnBlanco.Master"
            ConfigurarSkin()
        Else
            Me.MasterPageFile = "~/App_Master/Menu.Master"
        End If
    End Sub
    ''' <summary>
    ''' Antes de dibujar la pagina establece filtros busqueda y datos paginados
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            LimpiarBusquedaAvanzada()
            cargarSituacionActual()
            RecargarContratosWHG()
        End If
    End Sub
    ''' <summary>
    ''' Carga de la pagina
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorContratos
        CargarIdiomas()

        If Not IsPostBack Then
            FSNPageHeader.TituloCabecera = Textos(8)
            FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/Contrato.png"
            If m_desdeGS = False Then
                CType(Me.Master, Menu).Seleccionar("Contratos", "Seguimiento")
            End If
            If Session("CONTR_FilaEstado") <> Nothing Then
                seleccionadoEstadoGrid = True
            End If

            Dim scriptReference As ScriptReference
            scriptReference = New ScriptReference
            scriptReference.Path = ConfigurationManager.AppSettings("ruta") & "js/jsUpdateProgress.js"
            If m_desdeGS Then
                Dim mMasterTotal As FSNWeb.EnBlanco = CType(Me.Master, FSNWeb.EnBlanco)
                CType(mMasterTotal.FindControl("ScriptManager1"), ScriptManager).Scripts.Add(scriptReference)
            Else
                Dim mMasterTotal As FSNWeb.Cabecera = CType(CType(Me.Master, Menu).Master, FSNWeb.Cabecera)
                CType(mMasterTotal.FindControl("ScriptManager1"), ScriptManager).Scripts.Add(scriptReference)
            End If

            InicializacionControles()
            _pageNumber = 0
            Session("CONTR_arrDatosGenerales") = Nothing
            InicializarArraysCamposGenerales()
            WriteScripts()
            'Tipos de solicitudes
            CargarTiposSolicitudes()
            'Commodities
            CargarCommodities()
            'Combo de Años
            CargarAnyos()
            'Carga los diferentes estados de los contratos
            CargarEstados()
            DesseleccionarEstados()
            '2. Comprobar si viene de configurar un filtro para cargarle ese filtro por defecto
            cargarSituacionActual()
            '3.Configurar estados a mostrar
            SeleccionarEstado()
            'Alterminar tarea descomentar esta linea
            btnConfigurar.Visible = (Session("IDFiltroContratoUsuario") <> "")
            btnConfigurar.Attributes.Add("onClick", "window.location='ConfigurarFiltrosContratos.aspx?desdeGS=" & IIf(m_desdeGS, "1", "") & "&SessionId = " & Request.QueryString("SessionID") & "&IDFiltroConfigurando=" & Session("IDFiltroContratoUsuario") & "&NombreTabla=" & Session("PMCNombreTabla") & "';return false;")
            FSNPanelEnProceso.ImagenPopUp = "~/App_Themes/" & Me.Page.Theme & "/images/Icono_Error_Amarillo_40x40.gif"
        Else
            Dim RecargarGrid As Boolean = True
            For index As Integer = 0 To dlFiltrosConfigurados.Items.Count - 1
                If Request("__EVENTTARGET") = CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).UniqueID Then
                    RecargarGrid = False
                    Exit For
                End If
            Next
            'Si se ha pulsado algun boton de estado
            For i = 1 To 8
                If Request("__EVENTTARGET") = CType(panTabsEstados.FindControl("btnBoton" & i), FSNWebControls.FSNButton).UniqueID Then
                    RecargarGrid = False
                    Exit For
                End If
            Next
            Select Case Request("__EVENTTARGET")
                Case btnPager.ClientID
                    _pageNumber = CType(Request("__EVENTARGUMENT"), Integer)
                    Session("pageNumberVCont") = CType(Request("__EVENTARGUMENT"), String)
                Case btnPagerBD.ClientID
                    _pageNumber = CType(Request("__EVENTARGUMENT"), Integer)
                    Session("pageNumberVCont") = CType(Request("__EVENTARGUMENT"), String)
                Case btnOrder.ClientID
                    Exit Sub
                Case btnRecargaCache.ClientID
                    RecargarCache()
                    Exit Sub
                Case ddlAnyo.ClientID
                    If Request("__EVENTARGUMENT").IndexOf("CargarProce") <> -1 Then
                        Dim Proce() As String
                        Proce = Request("__EVENTARGUMENT").Split("#")
                        CargarProcesoEnDropDowns(Proce(1), Proce(2), Proce(3))
                    End If
                    Exit Sub
                Case lbExpirar.ClientID, Replace(lbExpirar.ClientID, "_", "$"), lbPendientes.ClientID, Replace(lbPendientes.ClientID, "_", "$")
                    RecargarGrid = False
                Case btnBuscarIdentificador.ClientID, Replace(btnBuscarIdentificador.ClientID, "_", "$")
                    RecargarGrid = False
                Case btnBuscarBusquedaAvanzada.ClientID, Replace(btnBuscarBusquedaAvanzada.ClientID, "_", "$")
                    RecargarGrid = False
            End Select
            If RecargarGrid Then RecargarContratosWHG()
            Select Case Request("__EVENTTARGET")
                Case "Excel"
                    CargarGridExportacion()
                    ExportarExcel()
                Case "PDF"
                    CargarGridExportacion()
                    ExportarPDF()
                Case Else
                    updGridContratos.Update()
            End Select
        End If
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM2008", "<script>var rutaPM2008 = '" & ConfigurationManager.AppSettings("rutaPM2008") & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")

        CType(whgContratos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgExcel"), Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/Excel.png"
        CType(whgContratos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgPDF"), Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/PDF.png"

        'Variables para los panelinfo
        Dim ProveAnimationClientID As String = FSNPanelDatosProveedor.AnimationClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ProveAnimationClientID", "<script>var ProveAnimationClientID = '" & ProveAnimationClientID & "' </script>")

        Dim ProveDynamicPopulateClienteID As String = FSNPanelDatosProveedor.DynamicPopulateClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ProveDynamicPopulateClienteID", "<script>var ProveDynamicPopulateClienteID = '" & ProveDynamicPopulateClienteID & "' </script>")

        Dim PeticionarioAnimationClientID As String = FSNPanelDatosPersona.AnimationClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "PeticionarioAnimationClientID", "<script>var PeticionarioAnimationClientID = '" & PeticionarioAnimationClientID & "' </script>")

        Dim PeticionarioDynamicPopulateClienteID As String = FSNPanelDatosPersona.DynamicPopulateClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "PeticionarioDynamicPopulateClienteID", "<script>var PeticionarioDynamicPopulateClienteID = '" & PeticionarioDynamicPopulateClienteID & "' </script>")

        Dim EmpresaAnimationClientID As String = FSNPanelDatosEmpresa.AnimationClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "EmpresaAnimationClientID", "<script>var EmpresaAnimationClientID = '" & EmpresaAnimationClientID & "' </script>")

        Dim EmpresaDynamicPopulateClienteID As String = FSNPanelDatosEmpresa.DynamicPopulateClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "EmpresaDynamicPopulateClienteID", "<script>var EmpresaDynamicPopulateClienteID = '" & EmpresaDynamicPopulateClienteID & "' </script>")

        Dim ProcesoAnimationClientID As String = FSNPanelEnProceso.AnimationClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ProcesoAnimationClientID", "<script>var ProcesoAnimationClientID = '" & ProcesoAnimationClientID & "' </script>")

        Dim ProcesoDynamicPopulateClienteID As String = FSNPanelEnProceso.DynamicPopulateClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ProcesoDynamicPopulateClienteID", "<script>var ProcesoDynamicPopulateClienteID = '" & ProcesoDynamicPopulateClienteID & "' </script>")

        Dim ModalProgressClientID As String
        If m_desdeGS Then
            ModalProgressClientID = CType(Master.FindControl("ModalProgress"), AjaxControlToolkit.ModalPopupExtender).ClientID
        Else
            ModalProgressClientID = CType(Master.Master.FindControl("ModalProgress"), AjaxControlToolkit.ModalPopupExtender).ClientID
        End If
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalClientID", "<script>var ModalProgress = '" & ModalProgressClientID & "' </script>")
    End Sub
#End Region
    ''' <summary>
    ''' Configura el Skin de algunos controles si el visor se muestra desde GS para que tengan la apariencia correcta
    ''' </summary>
    Private Sub ConfigurarSkin()
        Dim master As MasterPage
        'Hace que se carge la MasterPage y con ello hace que se carguen los controles, si no, no podriamos cambiar la propiedad SkinID
        master = Me.Master
        FSNPageHeader.AspectoGS = True
        btnBuscarIdentificador.SkinID = "GS"
        btnBuscarBusquedaAvanzada.SkinID = "GS"
        btnAnyadirFiltros.SkinID = "GS"
        btnConfigurar.SkinID = "GS"
        btnLimpiarBusquedaAvanzada.SkinID = "GS"
        ImgBuscadorProcesos.SkinID = "BuscadorLupa_GS"
        pnlBusquedaAvanzada.CssClass = "PanelCabeceraGS"
        dlFiltrosConfigurados.SkinID = "PestanyasFiltrosGS"
        pnlParametros.CssClass = "RectanguloGS"
    End Sub
    ''' <summary>
    ''' Carga el proceso seleccionado desde el buscador de procesos en los combos
    ''' </summary>
    ''' <param name="sAnyo">Año del proceso</param>
    ''' <param name="sGMN1">Commodity del proceso</param>
    ''' <param name="Codigo">Codigo del proceso</param>
    ''' <remarks></remarks>
    Private Sub CargarProcesoEnDropDowns(ByVal sAnyo As String, ByVal sGMN1 As String, ByVal Codigo As String)
        'deseleccionamos los combos pues si habria algun proceso seleccionado
        If sAnyo = "" Or sGMN1 = "" Then
            If ddlAnyo.SelectedItems.Count > 0 Then
                For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlAnyo.SelectedItems
                    ddi.Selected = False
                Next
            End If
            If ddlCommodity.SelectedItems.Count > 0 Then
                For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlCommodity.SelectedItems
                    ddi.Selected = False
                Next
            End If
            ddlCommodity.CurrentValue = ""
            If ddlCodigo.SelectedItems.Count > 0 Then
                For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlCodigo.SelectedItems
                    ddi.Selected = False
                Next
            End If
            ddlCodigo.CurrentValue = ""
            If ddlDenominacion.SelectedItems.Count > 0 Then
                For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlDenominacion.SelectedItems
                    ddi.Selected = False
                Next
            End If
            ddlDenominacion.CurrentValue = ""
        Else
            For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlAnyo.Items
                If ddi.Value = sAnyo Then
                    ddi.Selected = True
                    ddlAnyo.CurrentValue = ddi.Text
                    Exit For
                End If
            Next
            For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlCommodity.Items
                If ddi.Value = sGMN1 Then
                    ddi.Selected = True
                    ddlCommodity.CurrentValue = ddi.Text
                    Exit For
                End If
            Next
            If Codigo <> "" Then
                CargarProcesos(sAnyo, sGMN1)

                For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlCodigo.Items
                    If ddi.Value = Codigo Then
                        ddi.Selected = True
                        ddlCodigo.CurrentValue = ddi.Text
                        Exit For
                    End If
                Next
                For Each ddi As Infragistics.Web.UI.ListControls.DropDownItem In ddlDenominacion.Items
                    If ddi.Value = Codigo Then
                        ddi.Selected = True
                        ddlDenominacion.CurrentValue = ddi.Text
                        Exit For
                    End If
                Next
            End If
        End If
    End Sub
    ''' <summary>
    ''' Inicializa los componentes y funciones javascript de los elementos
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0seg.</remarks>
    Private Sub InicializacionControles()
        CollapsiblePanelExtender1.CollapsedImage = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/expandir.gif"
        CollapsiblePanelExtender1.ExpandedImage = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/contraer.gif"
        txtProveedor.Attributes.Add("onkeydown", "javascript:return eliminarProveedor(event)")
        If Acceso.gbAccesoFSGS = False Then
            CeldaBuscadorProcesos.Visible = False
        Else
            CeldaBuscadorProcesos.Visible = True
        End If
    End Sub
    ''' <summary>
    ''' Inicializa javascript de los elementos
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0seg.</remarks>
    Private Sub WriteScripts()
        imgProveedorLupa.OnClientClick = "var newWindow = window.open('" & ConfigurationManager.AppSettings("RutaFS") & "_common/BuscadorProveedores.aspx?desdeGS=" & IIf(m_desdeGS, "1", "") & "&SessionID=" & Request.QueryString("SessionID") & "&PM=true&Contr=true', '_blank', 'width=835,height=635,status=yes,resizable=no,top=0,left=150,scrollbars=yes');return false;"
        'Funcion que sirve para eliminar el proveedor
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarProveedor") Then
            Dim sScript As String = "function eliminarProveedor(event) {" & vbCrLf &
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf &
                    "  " & vbCrLf &
            " } else { " & vbCrLf &
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf &
                    " campoProveedor = document.getElementById('" & txtProveedor.ClientID & "')" & vbCrLf &
                    " if (campoProveedor) { campoProveedor.value = ''; } " & vbCrLf &
                    " campoHiddenProveedor = document.getElementById('" & hidProveedor.ClientID & "')" & vbCrLf &
                    " if (campoHiddenProveedor) { campoHiddenProveedor.value = ''; } " & vbCrLf &
            " } " & vbCrLf &
            "  " & vbCrLf &
            " } }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarProveedor", sScript, True)
        End If
    End Sub
    ''' <summary>
    ''' Revisado por: Sandra. Fecha: 21/03/2011.
    ''' Proceso que carga el combo de Tipo con los tipos de las solicitudes..
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,1seg.</remarks>
    Private Sub CargarTiposSolicitudes()
        Dim oSolicitudes As FSNServer.Solicitudes
        oSolicitudes = FSNServer.Get_Object(GetType(FSNServer.Solicitudes))
        wddTipo.TextField = "DEN"
        wddTipo.ValueField = "ID"
        wddTipo.DataSource = oSolicitudes.LoadTiposSolicitudes(FSNUser.CodPersona, Idioma, TiposDeDatos.TipoDeSolicitud.Contrato)
        wddTipo.DataBind()
        wddTipo.Items.RemoveAt(0)
        wddTipo.CurrentValue = "" 'para que aparezca el webdropdown vacío. Si no, aparece la 1ª opción
        wddTipo.EnableAutoFiltering = Infragistics.Web.UI.ListControls.AutoFiltering.Client
        wddTipo.AutoFilterQueryType = Infragistics.Web.UI.ListControls.AutoFilterQueryTypes.Contains
        wddTipo.AutoFilterSortOrder = Infragistics.Web.UI.ListControls.DropDownAutoFilterSortOrder.Ascending
        oSolicitudes = Nothing
    End Sub
    ''' <summary>
    ''' Carga los años en el combo de años del proceso. Desde el actual menos 10 hasta el actual más 10.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarAnyos()
        Dim iAnyoActual As Integer
        Dim iInd As Integer
        Dim ddItem As Infragistics.Web.UI.ListControls.DropDownItem
        iAnyoActual = Year(Now)
        ddlAnyo.Items.Clear()
        For iInd = iAnyoActual - 10 To iAnyoActual + 10
            ddItem = New Infragistics.Web.UI.ListControls.DropDownItem
            ddItem.Value = iInd
            ddItem.Text = iInd.ToString
            ddlAnyo.Items.Add(ddItem)
        Next
        ddlAnyo.SelectedItemIndex = ddlAnyo.Items.IndexOf(ddlAnyo.Items.FindItemByValue(iAnyoActual.ToString))
    End Sub
    ''' <summary>
    ''' Carga los materiales de nivel 1
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarCommodities()
        Dim oCommodities As FSNServer.GrupoMatNivel1
        oCommodities = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel1))
        If FSNUser.Pyme > 0 Then
            Dim oGruposMaterial As FSNServer.GruposMaterial
            oGruposMaterial = FSNServer.Get_Object(GetType(FSNServer.GruposMaterial))
            oCommodities.LoadData(oGruposMaterial.DevolverGMN1_PYME(FSNUser.Pyme), , , , FSNUser.Idioma)
        Else
            oCommodities.LoadData(, , , , FSNUser.Idioma)
        End If
        Dim oItem As Infragistics.Web.UI.ListControls.DropDownItem
        ddlCommodity.Items.Clear()
        oItem = New Infragistics.Web.UI.ListControls.DropDownItem("", "")
        ddlCommodity.Items.Add(oItem)
        For Each oRow As DataRow In oCommodities.Data.Tables(0).Rows
            oItem = New Infragistics.Web.UI.ListControls.DropDownItem
            oItem.Value = DBNullToStr(oRow.Item("COD"))
            If IsDBNull(oRow.Item("COD")) Then
                oItem.Text = ""
            Else
                oItem.Text = DBNullToStr(oRow.Item("DEN" & "_" & Idioma))
            End If
            ddlCommodity.Items.Add(oItem)
        Next
        oCommodities = Nothing
    End Sub
    ''' <summary>
    ''' Evento que se lanza al acceder a datos y que usamos para decirle al origen de datos cual va a ser su objeto.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: En la carga de la página VisorSolicitudes.aspx; Tiempo máximo: 0 sg.</remarks>
    Private Sub odsFiltrosConfigurados_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles odsFiltrosConfigurados.ObjectCreating
        e.ObjectInstance = FSNServer.Get_Object(GetType(FSNServer.FiltrosContrato))
    End Sub
    ''' <summary>
    ''' Evento que se lanza cuando el objeto de origen de datos recupera los datos. Lo usamos para pasarle los parámetros que necesita su método SELECT configurado.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: En la carga de la página VisorSolicitudes.aspx; Tiempo máximo: 0 sg.</remarks>
    Private Sub odsFiltrosConfigurados_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsFiltrosConfigurados.Selecting
        e.InputParameters.Item("sUsuario") = FSNUser.CodPersona
    End Sub
    ''' <summary>
    ''' Devuelve los procesos con los filtros indicados
    ''' </summary>
    ''' <param name="Anyo">Año del proceso</param>
    ''' <param name="Commodity">Commodity del proceso</param>
    ''' <remarks></remarks>
    Private Sub CargarProcesos(ByVal Anyo As Short, ByVal Commodity As String)
        ddlDenominacion.Items.Clear()
        ddlCodigo.Items.Clear()
        ddlDenominacion.SelectedValue = ""
        ddlCodigo.SelectedValue = ""
        Dim oProceso As FSNServer.Procesos
        oProceso = FSNServer.Get_Object(GetType(FSNServer.Procesos))
        If Commodity <> "" Then
            oProceso.CargarProcesos(Idioma, Anyo, Commodity)
            If oProceso.Data.Tables(0).Rows.Count > 0 Then
                ddlCodigo.DataSource = oProceso.Data.Tables(0)
                ddlCodigo.ValueField = "COD"
                ddlCodigo.TextField = "COD"
                ddlCodigo.DataBind()
                ddlDenominacion.DataSource = oProceso.Data.Tables(0)
                ddlDenominacion.ValueField = "COD"
                ddlDenominacion.TextField = "DEN"
                ddlDenominacion.DataBind()
            End If
        End If
        oProceso = Nothing
    End Sub
    ''' <summary>
    ''' Desselecciona todos los botones de estados y pone la columna de los iconos a no visible.
    ''' </summary>
    ''' <remarks>Llamada desde: btnGuardadas_Click, btnAbiertas_Click, btnPendientesRevisarCierre_Click, btnCierreDentroPlazo_Click, btnCierreFueraPlazo_Click, btnCierreNoEficaz_Click, btnTodas_Click; Tiempo máximo: 0 sg.</remarks>
    Private Sub DesseleccionarEstados()
        Dim mi_boton As New Fullstep.FSNWebControls.FSNButton
        For i = 1 To 8
            mi_boton = updGridContratos.FindControl("btnBoton" & i)
            mi_boton.Selected = False
            mi_boton.Text = arrTextosEstadosMini(i - 1)
            mi_boton.ToolTip = arrTextosEstados(i - 1)
        Next
    End Sub
    '''' <summary>
    '''' Selecciona el estado activo cuando se carga la página sin postback.
    '''' Si no hay estado activo (primera vez) se activa el estado "Guardadas".
    '''' Actualizo la ordenacion de la grid
    '''' </summary>
    '''' <remarks>Llamada desde: Page_Load(). Tiempo Máximo: 0 sg.</remarks>
    Private Sub SeleccionarEstado()
        Dim mi_boton As New Fullstep.FSNWebControls.FSNButton
        If Session("CONTR_FilaEstado") <> Nothing Then
            For index As Integer = 1 To 8
                mi_boton = updGridContratos.FindControl("btnBoton" & index)
                If mi_boton.Attributes("Estado") = Session("CONTR_FilaEstado") Then
                    mi_boton.Selected = True
                End If
            Next
        Else
            Session("CONTR_FilaEstado") = btnBoton2.Attributes.Item("Estado") 'Selecciona el de en curso de aprobacion
            btnBoton2.Selected = True
            seleccionadoEstadoGrid = True
        End If
    End Sub
    ''' <summary>
    ''' Carga los estados en las que se puede encontrar las noConformidades en las pestañas
    ''' </summary>
    ''' <remarks>Llamada desde=Page_Load; Tiempo máximo=0seg.</remarks>
    Private Sub CargarEstados()
        Dim mi_boton As New Fullstep.FSNWebControls.FSNButton
        For i = 1 To 8
            mi_boton = updGridContratos.FindControl("btnBoton" & i)
            mi_boton.Attributes.Add("Estado", arrOrdenEstados(i - 1))
            mi_boton.Text = arrTextosEstadosMini(i - 1)
            mi_boton.ToolTip = arrTextosEstados(i - 1)
            mi_boton.Visible = True
        Next
    End Sub
    ''' <summary>
    ''' Revisado por: Sandra. Fecha: 09/03/2011
    ''' Carga de los elementos con el idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo=0,2</remarks>
    Private Sub CargarIdiomas()
        Dim cParametros As FSNServer.Parametros
        cParametros = FSNServer.Get_Object(GetType(FSNServer.Parametros))
        Dim dsParametros As Data.DataSet = cParametros.LoadPMData()
        Dim dtGmn1() As Data.DataRow = dsParametros.Tables(2).Select("ID=9 AND IDI='" & FSNUser.Idioma.ToString & "'")
        FSNPageHeader.TituloCabecera = Textos(8)
        lblCodigoContrato.Text = Textos(24) & ":" ' Codigo
        lblProveedor.Text = Textos(20) & ":"
        btnBuscarIdentificador.Text = Textos(35) ' Buscar
        lblTipo.Text = Textos(21) & ":" 'Tipo
        lblBusquedaAvanzada.Text = Textos(37) 'Seleccione los criterios de búsqueda generales
        btnBuscarBusquedaAvanzada.Text = Textos(35)
        btnLimpiarBusquedaAvanzada.Text = Textos(36) 'Limpiar
        btnAnyadirFiltros.Text = Textos(39) 'Añadir filtros
        lblAnyo.Text = Textos(22) & ":"
        lblCommodity.Text = dtGmn1(0)("DEN") & ":"
        lblCodigo.Text = Textos(24) & ":" ' Codigo
        lblDenominacion.Text = Textos(25) & ":"
        btnConfigurar.Text = Textos(43) ' Configurar
        lblProceso.Text = Textos(64) ' procesos
        m_sTextoEnProceso = Textos(88)
        m_sTextoCargando = Textos(91)
        whgContratos.GroupingSettings.EmptyGroupAreaText = Textos(40) 'Desplace una columna aquí para agrupar por ese concepto
        CType(whgContratos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblExcel"), Label).Text = Textos(94) 'Excel
        CType(whgContratos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPDF"), Label).Text = Textos(95)   'PDF
        CType(whgContratos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPage"), Label).Text = Textos(96)  'Pagina
        CType(whgContratos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblOF"), Label).Text = Textos(97)    'de
    End Sub
    ''' <summary>
    ''' Carga la ultima situacion conocida del usuario.
    ''' </summary>
    ''' <remarks>Llamada desde= Page_load ; Tiempo ejecucion:=0,1</remarks>
    Private Sub cargarSituacionActual()
        Dim FiltroUsuarioIdAnt, NombreTablaAnt, FiltroIdAnt, CodigoContrato As String
        Dim CargaDesdeVolver As Boolean = False
        Dim ListaTiposVolver As String = ""

        FiltroUsuarioIdAnt = Request.QueryString("FiltroUsuarioIdAnt")
        NombreTablaAnt = Request.QueryString("NombreTablaAnt")
        FiltroIdAnt = Request.QueryString("FiltroIdAnt")

        Me.txtCodigoContrato.Text = Request.QueryString("CodigoContratoAnt")

        Me.hidProveedor.Value = Request.QueryString("CodigoProveedorAnt")
        Me.hidProveedorCIF.Value = Request.QueryString("CodigoProveedorCifAnt")
        Me.txtProveedor.Text = Request.QueryString("CodigoProveedorTextAnt")

        ListaTiposVolver = Request.QueryString("ListaTiposAnt")

        CargaDesdeVolver = (Me.txtCodigoContrato.Text <> "") OrElse (Me.hidProveedor.Value <> "") OrElse (ListaTiposVolver <> "") OrElse (FiltroUsuarioIdAnt <> "")

        Dim arrParametros() As String = Nothing
        Dim bEncontradoTab As Boolean = False
        'Seleccionar el filtro
        If FiltroUsuarioIdAnt <> "" Then 'Ha venido de volver (Seleccionamos el filtro que tenia seleccionado anteriormente)
            m_bConConfiguracionAnterior = True
            Dim sTabSeleccionado As String = FiltroUsuarioIdAnt & "#" & NombreTablaAnt & "#" & FiltroIdAnt
            For index As Integer = 0 To dlFiltrosConfigurados.Items.Count - 1
                If sTabSeleccionado = CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).CommandArgument Then
                    arrParametros = Split(CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).CommandArgument, "#")
                    CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).Selected = True
                    bEncontradoTab = True
                    Exit For
                End If
            Next
        Else
            m_bConConfiguracionAnterior = False
        End If
        If Session("IDFiltroContratoUsuario") <> Nothing Then
            m_bConConfiguracionAnterior = True
            Dim sTabSeleccionado As String = Session("IDFiltroContratoUsuario") & "#" & Session("PMCNombreTabla") & "#" & Session("IDFiltroContrato")
            For index As Integer = 0 To dlFiltrosConfigurados.Items.Count - 1
                If sTabSeleccionado = CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).CommandArgument Then
                    arrParametros = Split(CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).CommandArgument, "#")
                    CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).Selected = True
                    bEncontradoTab = True
                    Exit For
                End If
            Next
        End If
        If Not bEncontradoTab Then ' Si no encuentra ninguno, o es la primera vez, selecciono el primero
            arrParametros = Split(CType(dlFiltrosConfigurados.Items(0).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).CommandArgument, "#")
            CType(dlFiltrosConfigurados.Items(0).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).Selected = True
        End If
        If arrParametros(0) = 0 Then
            Session("IDFiltroContratoUsuario") = ""
            Session("PMCNombreTabla") = ""
            Session("IDFiltroContrato") = ""
        Else
            Session("IDFiltroContratoUsuario") = arrParametros(0)
            Session("PMCNombreTabla") = arrParametros(1)
            Session("IDFiltroContrato") = arrParametros(2)
        End If

        PMFiltroUsuarioIdVContr.Value = Session("IDFiltroContratoUsuario")
        PMNombreTablaVContr.Value = Session("PMCNombreTabla")
        PMFiltroIdVContr.Value = Session("IDFiltroContrato")

        CargarBusquedaAvanzada(CargaDesdeVolver, ListaTiposVolver)
        hCacheLoaded.Value = 0
        updFiltros.Update()
    End Sub
    ''' <summary>
    ''' Recargar la Cache q es el datasource del grid.
    ''' Al entrar, pulsar bt busqueda, pulsar bt busqueda usercontrol y cambiar de estado.
    ''' </summary>
    ''' <remarks>Llamada desde: Sys.WebForms.PageRequestManager.getInstance().add_endRequest; Tiempo maximo: 1 sg;</remarks>
    Private Sub RecargarCache()
        Dim sEstadoContadores As String = ""
        Dim sMaterial As String = String.Empty
        Dim sEstado As String = String.Empty
        Dim sCodProceso As Integer = 0
        Dim sAnyo As Short = 0
        Dim material() As String
        Dim sCodigoContrato As String = String.Empty
        Dim sTipo As String = String.Empty
        Dim sProveedor As String
        Dim sArticulo As String = ""
        Dim bVigentesSeleccionado As Boolean = False
        Dim iBotonesConContratos As Byte = 0
        Dim iAlertaExpiracion As Integer = 0
        Dim sNotificado As String = String.Empty

        sEstado = CStr(Session("CONTR_FilaEstado"))
        Dim sSeparador As Short = Busqueda.Estado.IndexOf(",")
        sEstadoContadores = Busqueda.Estado
        If Busqueda.Estado <> "" AndAlso Not (IsNothing(Session("CONTR_FilaEstado"))) AndAlso Busqueda.Estado.IndexOf(sEstado) = -1 Then
            If sEstado = "8" Then 'Ha pulsado "Todos"
                sEstado = Busqueda.Estado
            Else
                sEstado = "-1"
            End If
        End If
        If Busqueda.EmpresaHidden = "" Then
            If Busqueda.EmpresaTexto <> "" Then
                'Si se ha escrito algo en el campo empresa pero no se ha seleccionado del Autocompletar ponemos -1 para que no devuelva nada
                Busqueda.EmpresaHidden = "-1"
            End If
        End If
        If Busqueda.ArticuloHidden = "" Then
            sArticulo = Busqueda.ArticuloTexto
        Else
            sArticulo = Busqueda.ArticuloHidden
        End If
        If Busqueda.MaterialHidden <> "" Then
            material = Split(Busqueda.MaterialHidden, "-")
            Dim i As Byte
            Dim lLongCod As Long
            i = 1
            For i = 1 To material.GetUpperBound(0)
                Select Case i
                    Case 1
                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                    Case 2
                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                    Case 3
                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                    Case 4
                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN4
                End Select
                sMaterial = sMaterial + Space(lLongCod - Len(material(i - 1))) + material(i - 1)
            Next
        End If
        If txtCodigoContrato.Text <> "" Then
            sCodigoContrato = txtCodigoContrato.Text
        End If
        For i = 0 To wddTipo.Items.Count - 1
            If wddTipo.Items(i).Selected Then
                If sTipo <> "" Then sTipo += "$$$"
                sTipo = sTipo + wddTipo.Items(i).Value
            End If
        Next
        sProveedor = hidProveedor.Value
        If ddlAnyo.SelectedValue <> "0" And ddlAnyo.SelectedValue <> "" Then
            sAnyo = CType(ddlAnyo.SelectedValue, Short)
        End If
        If ddlCodigo.SelectedItems.Count > 0 Then
            sCodProceso = CType(ddlCodigo.SelectedValue, Short)
        End If
        Dim cFiltro As FiltroContrato
        cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroContrato))
        Dim _pageSize As Integer = System.Configuration.ConfigurationManager.AppSettings("PageSize")

        iAlertaExpiracion = 0
        If Busqueda.AlertaExpiracion > 0 AndAlso Busqueda.AlertaPeriodo > 0 Then
            Select Case Busqueda.AlertaPeriodo
                Case 1
                    iAlertaExpiracion = Busqueda.AlertaExpiracion
                Case 2
                    iAlertaExpiracion = Busqueda.AlertaExpiracion * 7
                Case 3
                    iAlertaExpiracion = Busqueda.AlertaExpiracion * 30
            End Select
        End If

        sNotificado = Busqueda.AlertaNotificado

        cFiltro.Visor_DevolverContratos(FSNUser.CodPersona, FSNUser.Idioma, sEstadoContadores, strToSQLLIKE(sCodigoContrato),
                                        sTipo, strToSQLLIKE(Busqueda.Descripcion), sProveedor, Busqueda.Peticionario, Busqueda.FechaInicio,
                                        Busqueda.FechaExpiracion, sMaterial, sArticulo, Busqueda.EmpresaHidden, ddlCommodity.SelectedValue,
                                        sCodProceso, sAnyo, sEstado, Session("PMCNombreTabla"), hOrdered.Value,
                                        Busqueda.PalabraClave, 0, 0, True, Busqueda.CentroCoste, Busqueda.PartidasPresUONS,
                                        iAlertaExpiracion, sNotificado)

        cFiltro.ContratosVisor.Tables(0).TableName = "ContratosFiltrados"
        Me.InsertarEnCache("dsContratos_" & FSNUser.Cod, cFiltro.ContratosVisor, CacheItemPriority.BelowNormal)
        hCacheLoaded.Value = 1
        updFiltros.Update()
    End Sub
    ''' <summary>
    ''' Nos indica si un nodo esta dentro de una lista o no
    ''' </summary>
    ''' <param name="listaElementos">Cadena de nodos seleccionada</param>
    ''' <param name="valorABuscar">Valor del nodo a buscar en la lista</param>        
    ''' <returns>True o False si a enontrado o no el nodo</returns>
    ''' <remarks>Llamada desde=SyncChanges//CargarCertificados; Tiempo máximo=0,2seg.</remarks>
    Private Function buscarElemento(ByVal listaElementos As String, ByVal valorABuscar As String) As Boolean
        Dim pos As Integer
        Dim elementosSeleccionados() As String
        Dim bEncontrado As Boolean = False
        pos = InStr(listaElementos, valorABuscar)
        If pos > 0 Then
            If Len(listaElementos) = Len(valorABuscar) Then 'El nodo coincide con la seleccion
                Return True
            Else
                If pos - 1 > 0 Then
                    bEncontrado = False
                    elementosSeleccionados = System.Text.RegularExpressions.Regex.Split(listaElementos, "[\s$][\s$][\s$]")
                    For i = 0 To UBound(elementosSeleccionados)
                        If elementosSeleccionados(i) = valorABuscar Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next
                    Return bEncontrado
                Else
                    Return True

                End If
            End If
        Else
            Return False
        End If
    End Function
    ''' <summary>
    ''' Cargar en el módulo de busqueda avanzada los datos configurados en el filtro.
    ''' Se hace con una variable de session porque cuando se carga la pagina se necesita acceder a esta funcion 2 veces. una vez que se carga completamente la pagina y otra antes
    ''' </summary>
    ''' <remarks>Llamada desde: Page_PreRender. Tiempo máximo: 1 sg.</remarks>
    Private Sub CargarBusquedaAvanzada(Optional ByVal CargaDesdeVolver As Boolean = False, Optional ByVal ListaTiposVolver As String = "")
        If Not m_bConConfiguracionAnterior Or (Not Request.QueryString("CargarBusquedaAvanzada") Is Nothing) Then
            If Session("IDFiltroContratoUsuario") <> "" Then
                Dim sMat As String
                Dim sFechaInicio As String = ""
                Dim sFechaExpiracion As String = ""
                Dim cFiltro As FiltroContrato
                cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroContrato))
                cFiltro.IDFiltroUsuario = Session("IDFiltroContratoUsuario")
                cFiltro.LoadConfiguracionCamposBusqueda(Idioma)
                Busqueda.Filtro = cFiltro.IDFiltro
                If Not cFiltro.CamposConfigurados.Tables("CAMPOSBUSQUEDA") Is Nothing Then
                    If cFiltro.CamposConfigurados.Tables("CAMPOSBUSQUEDA").Rows.Count > 0 Then
                        With cFiltro.CamposConfigurados.Tables("CAMPOSBUSQUEDA").Rows(0)
                            'Tipo                            
                            Dim sTexto As String = ""
                            If CargaDesdeVolver AndAlso (ListaTiposVolver <> "") Then
                                For i = 0 To wddTipo.Items.Count - 1
                                    If wddTipo.Items(i).Value <> "" Then
                                        If buscarElemento(ListaTiposVolver, wddTipo.Items(i).Value) Then
                                            wddTipo.Items(i).Selected = True
                                            If sTexto <> "" Then sTexto += ","
                                            sTexto = sTexto + wddTipo.Items(i).Text
                                        Else
                                            wddTipo.Items(i).Selected = False
                                        End If
                                    End If
                                Next
                                wddTipo.CurrentValue = sTexto
                            ElseIf DBNullToStr(.Item("TIPO")) <> "" Then
                                'Si el filtro tiene restricción por tipo/s se carga con dicho/s tipo/s, si no, se mira si el usuario está filtrando por este campo
                                For i = 0 To wddTipo.Items.Count - 1
                                    If wddTipo.Items(i).Value <> "" Then
                                        If buscarElemento(DBNullToStr(.Item("TIPO")), wddTipo.Items(i).Value) Then
                                            wddTipo.Items(i).Selected = True
                                            If sTexto <> "" Then sTexto += ","
                                            sTexto = sTexto + wddTipo.Items(i).Text
                                        End If
                                    End If
                                Next
                                wddTipo.CurrentValue = sTexto
                            Else
                                If wddTipo.SelectedItems.Count > 0 Then
                                    For i = 0 To wddTipo.Items.Count - 1
                                        wddTipo.Items(i).Selected = False
                                    Next
                                End If
                                wddTipo.CurrentValue = ""
                            End If
                            'Si el filtro tiene restricción por proceso se carga con dicho proceso, si no, NO se actualizan los valores por si el usuario está filtrando por este campo
                            If Not IsDBNull(.Item("PROCE_GMN1")) Then
                                CargarProcesoEnDropDowns(DBNullToStr(.Item("PROCE_ANYO")), DBNullToStr(.Item("PROCE_GMN1")), DBNullToStr(.Item("PROCE_COD")))
                            Else
                                LimpiarProcesos()
                            End If
                            'Si el filtro tiene restricción por proveedor se carga con dicho proveedor, si no, NO se actualizan los valores por si el usuario está filtrando por este campo
                            If Not IsDBNull(.Item("PROVEEDOR")) Then
                                txtProveedor.Text = DBNullToStr(.Item("PROVEDEN"))
                                hidProveedor.Value = DBNullToStr(.Item("PROVEEDOR"))
                            ElseIf Not CargaDesdeVolver Then
                                txtProveedor.Text = Nothing
                                hidProveedor.Value = Nothing
                            End If
                            sMat = DBNullToStr(.Item("GMN1")) & "-" & DBNullToStr(.Item("GMN2")) & "-" & DBNullToStr(.Item("GMN3")) & "-" & DBNullToStr(.Item("GMN4"))
                            Busqueda.MaterialHidden = sMat
                            Busqueda.MaterialTexto = DBNullToStr(.Item("MATDEN"))
                            Busqueda.ArticuloHidden = DBNullToStr(.Item("ARTICULO"))
                            Busqueda.ArticuloTexto = DBNullToStr(.Item("ARTDEN"))
                            Busqueda.EmpresaHidden = DBNullToStr(.Item("EMP"))
                            Busqueda.EmpresaTexto = DBNullToStr(.Item("EMPDEN"))
                            'Fecha inicio
                            If Not IsDBNull(.Item("FECHA_INICIO")) Then
                                sFechaInicio = .Item("FECHA_INICIO")
                            End If
                            If Not IsDBNull(.Item("FECHA_EXP")) Then
                                sFechaExpiracion = DBNullToStr(.Item("FECHA_EXP"))
                            End If
                            If sFechaInicio <> "" Then
                                Busqueda.FechaInicio = sFechaInicio
                            End If
                            If sFechaExpiracion <> "" Then
                                Busqueda.FechaExpiracion = sFechaExpiracion
                            End If
                            Busqueda.Estado = DBNullToStr(.Item("ESTADO"))
                            Busqueda.Peticionario = DBNullToStr(.Item("PETICIONARIO"))
                            Busqueda.Descripcion = DBNullToStr(.Item("DESCRIPCION"))
                            Busqueda.PalabraClave = DBNullToStr(.Item("PALABRA_CLAVE"))
                            If Me.Acceso.gbAccesoFSSM Then
                                Busqueda.CentroCoste = DBNullToStr(.Item("CENTRO_COSTE"))
                                'ahora usamos Busqueda.getPartidas en vez de Busqueda.getPartidasPres porque se trata de comprobar
                                'que hay partidas, no que tienen valores (ahora vamos a ponerles valor)
                                Dim dtPartidasPres As DataTable = Busqueda.getPartidas(Me.Usuario.Idioma)
                                If dtPartidasPres.Rows.Count > 0 Then
                                    Dim sPartidasPres As String() = Nothing
                                    For i As Integer = 1 To 4
                                        If DBNullToStr(.Item("PARTIDA" & i)) <> String.Empty Then
                                            ReDim Preserve sPartidasPres(i - 1)
                                            sPartidasPres(i - 1) = DBNullToStr(.Item("PARTIDA" & i))
                                        End If
                                    Next
                                    If sPartidasPres IsNot Nothing AndAlso sPartidasPres.Length > 0 Then
                                        Busqueda.PartidasPres = sPartidasPres
                                    End If
                                End If
                            End If

                            If DBNullToStr(.Item("ALERTA_EXPIRACION")).Length > 0 Then
                                Busqueda.AlertaExpiracion = DBNullToStr(.Item("ALERTA_EXPIRACION"))
                            Else
                                Busqueda.AlertaExpiracion = 0
                            End If

                            If DBNullToStr(.Item("ALERTA_PERIODO")).Length > 0 Then
                                Busqueda.AlertaPeriodo = DBNullToStr(.Item("ALERTA_PERIODO"))
                            Else
                                Busqueda.AlertaPeriodo = 0
                            End If

                            Busqueda.AlertaNotificado = DBNullToStr(.Item("ALERTA_NOTIFICADO"))
                        End With
                        UpdatePanel4.Update()
                        UpdatePanel5.Update()
                        upBusquedaAvanzada.Update()
                        Up_Procesos.Update()
                    End If
                End If
                cFiltro = Nothing
                AlmacenarBusquedaAvanzada()
            Else
                If CargaDesdeVolver Then
                    Dim sTexto As String = ""
                    For i = 0 To wddTipo.Items.Count - 1
                        If wddTipo.Items(i).Value <> "" Then
                            If buscarElemento(ListaTiposVolver, wddTipo.Items(i).Value) Then
                                wddTipo.Items(i).Selected = True
                                If sTexto <> "" Then sTexto += ","
                                sTexto = sTexto + wddTipo.Items(i).Text
                            Else
                                wddTipo.Items(i).Selected = False
                            End If
                        End If
                    Next
                    wddTipo.CurrentValue = sTexto
                Else
                    txtProveedor.Text = ""
                    hidProveedor.Value = ""
                    For i = 0 To wddTipo.Items.Count - 1
                        wddTipo.Items(i).Selected = False
                    Next
                    wddTipo.CurrentValue = ""
                End If
                UpdatePanel4.Update()
                UpdatePanel5.Update()
                If Not Session("CONTR_BusquedaAvanzada0") Is Nothing Then
                    Dim arrbusquedaTemporal As New ArrayList
                    arrbusquedaTemporal = CType(Session("CONTR_BusquedaAvanzada0"), ArrayList)
                    Busqueda.MaterialHidden = arrbusquedaTemporal(0)
                    Busqueda.MaterialTexto = arrbusquedaTemporal(1)
                    Busqueda.ArticuloHidden = arrbusquedaTemporal(2)
                    Busqueda.ArticuloTexto = arrbusquedaTemporal(3)
                    Busqueda.FechaInicio = arrbusquedaTemporal(4)
                    Busqueda.FechaExpiracion = arrbusquedaTemporal(5)
                    Busqueda.Peticionario = arrbusquedaTemporal(6)
                    Busqueda.Estado = arrbusquedaTemporal(7)
                    Busqueda.PalabraClave = arrbusquedaTemporal(8)
                    Busqueda.Descripcion = arrbusquedaTemporal(9)
                    Busqueda.EmpresaHidden = arrbusquedaTemporal(10)
                    Busqueda.EmpresaTexto = arrbusquedaTemporal(11)
                    Busqueda.CentroCoste = arrbusquedaTemporal(12)
                    Busqueda.AlertaExpiracion = arrbusquedaTemporal(13)
                    Busqueda.AlertaPeriodo = arrbusquedaTemporal(14)
                    Busqueda.AlertaNotificado = arrbusquedaTemporal(15)

                    upBusquedaAvanzada.Update()
                End If
            End If
        Else
            If Session("IDFiltroContratoUsuario") <> "" Then
                If Not Session("CONTR_BusquedaAvanzada") Is Nothing Then
                    'Inicio Filtros fuera Buscador avanzado: 
                    '     codigo/proveedor/tipo
                    '     anyo/cmd/cod/den
                    Dim cFiltro As FiltroContrato
                    cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroContrato))
                    cFiltro.IDFiltroUsuario = Session("IDFiltroContratoUsuario")
                    cFiltro.LoadConfiguracionCamposBusqueda(Idioma)
                    Busqueda.Filtro = cFiltro.IDFiltro
                    'Inicio Buscador avanzado Contratos
                    Dim arrbusquedaTemporal As New ArrayList
                    arrbusquedaTemporal = CType(Session("CONTR_BusquedaAvanzada"), ArrayList)
                    Busqueda.MaterialHidden = arrbusquedaTemporal(0)
                    Busqueda.MaterialTexto = arrbusquedaTemporal(1)
                    Busqueda.ArticuloHidden = arrbusquedaTemporal(2)
                    Busqueda.ArticuloTexto = arrbusquedaTemporal(3)
                    Busqueda.FechaInicio = arrbusquedaTemporal(4)
                    Busqueda.FechaExpiracion = arrbusquedaTemporal(5)
                    Busqueda.Peticionario = arrbusquedaTemporal(6)
                    Busqueda.Estado = arrbusquedaTemporal(7)
                    Busqueda.PalabraClave = arrbusquedaTemporal(8)
                    Busqueda.Descripcion = arrbusquedaTemporal(9)
                    Busqueda.EmpresaHidden = arrbusquedaTemporal(10)
                    Busqueda.EmpresaTexto = arrbusquedaTemporal(11)
                    Busqueda.CentroCoste = arrbusquedaTemporal(12)
                    Busqueda.AlertaExpiracion = arrbusquedaTemporal(13)
                    Busqueda.AlertaPeriodo = arrbusquedaTemporal(14)
                    Busqueda.AlertaNotificado = arrbusquedaTemporal(15)

                    upBusquedaAvanzada.Update()
                    '''''''''''''''''''''''''''
                    If Not cFiltro.CamposConfigurados.Tables("CAMPOSBUSQUEDA") Is Nothing Then
                        If cFiltro.CamposConfigurados.Tables("CAMPOSBUSQUEDA").Rows.Count > 0 Then
                            'Tipo                            
                            Dim sTexto As String = ""
                            If CargaDesdeVolver AndAlso (ListaTiposVolver <> "") Then
                                For i = 0 To wddTipo.Items.Count - 1
                                    If wddTipo.Items(i).Value <> "" Then
                                        If buscarElemento(ListaTiposVolver, wddTipo.Items(i).Value) Then
                                            wddTipo.Items(i).Selected = True
                                            If sTexto <> "" Then sTexto += ","
                                            sTexto = sTexto + wddTipo.Items(i).Text
                                        Else
                                            wddTipo.Items(i).Selected = False
                                        End If
                                    End If
                                Next
                                wddTipo.CurrentValue = sTexto
                            ElseIf DBNullToStr(cFiltro.CamposConfigurados.Tables("CAMPOSBUSQUEDA").Rows(0).Item("TIPO")) <> "" Then
                                'Si el filtro tiene restriccion por tipo/s se carga con dicho/s tipo/s, si no, se mira si el usuario esta filtrando por este campo
                                For i = 0 To wddTipo.Items.Count - 1
                                    If wddTipo.Items(i).Value <> "" Then
                                        If buscarElemento(DBNullToStr(cFiltro.CamposConfigurados.Tables("CAMPOSBUSQUEDA").Rows(0).Item("TIPO")), wddTipo.Items(i).Value) Then
                                            wddTipo.Items(i).Selected = True
                                            If sTexto <> "" Then sTexto += ","
                                            sTexto = sTexto + wddTipo.Items(i).Text
                                        End If
                                    End If
                                Next
                                wddTipo.CurrentValue = sTexto
                            Else
                                If wddTipo.SelectedItems.Count > 0 Then
                                    For i = 0 To wddTipo.Items.Count - 1
                                        wddTipo.Items(i).Selected = False
                                    Next
                                End If
                                wddTipo.CurrentValue = ""
                            End If

                            'Si el filtro tiene restriccion por proceso se carga con dicho proceso, si no, NO se actualizan los valores por si el usuario esta filtrando por este campo
                            If Not IsDBNull(cFiltro.CamposConfigurados.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROCE_GMN1")) Then
                                CargarProcesoEnDropDowns(DBNullToStr(cFiltro.CamposConfigurados.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROCE_ANYO")), DBNullToStr(cFiltro.CamposConfigurados.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROCE_GMN1")), DBNullToStr(cFiltro.CamposConfigurados.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROCE_COD")))
                            Else
                                LimpiarProcesos()
                            End If
                            'Si el filtro tiene restriccion por proveedor se carga con dicho proveedor, si no, NO se actualizan los valores por si el usuario esta filtrando por este campo
                            If Not IsDBNull(cFiltro.CamposConfigurados.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROVEEDOR")) Then
                                txtProveedor.Text = DBNullToStr(cFiltro.CamposConfigurados.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROVEDEN"))
                                hidProveedor.Value = DBNullToStr(cFiltro.CamposConfigurados.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROVEEDOR"))
                            ElseIf Not CargaDesdeVolver Then
                                txtProveedor.Text = Nothing
                                hidProveedor.Value = Nothing
                            End If
                            UpdatePanel4.Update()
                            UpdatePanel5.Update()
                        End If
                    End If
                End If
            Else
                If Not Session("CONTR_BusquedaAvanzada0") Is Nothing Then
                    Dim arrbusquedaTemporal As New ArrayList
                    arrbusquedaTemporal = CType(Session("CONTR_BusquedaAvanzada0"), ArrayList)
                    Busqueda.MaterialHidden = arrbusquedaTemporal(0)
                    Busqueda.MaterialTexto = arrbusquedaTemporal(1)
                    Busqueda.ArticuloHidden = arrbusquedaTemporal(2)
                    Busqueda.ArticuloTexto = arrbusquedaTemporal(3)
                    Busqueda.FechaInicio = arrbusquedaTemporal(4)
                    Busqueda.FechaExpiracion = arrbusquedaTemporal(5)
                    Busqueda.Peticionario = arrbusquedaTemporal(6)
                    Busqueda.Estado = arrbusquedaTemporal(7)
                    Busqueda.PalabraClave = arrbusquedaTemporal(8)
                    Busqueda.Descripcion = arrbusquedaTemporal(9)
                    Busqueda.EmpresaHidden = arrbusquedaTemporal(10)
                    Busqueda.EmpresaTexto = arrbusquedaTemporal(11)
                    Busqueda.CentroCoste = arrbusquedaTemporal(12)
                    Busqueda.AlertaExpiracion = arrbusquedaTemporal(13)
                    Busqueda.AlertaPeriodo = arrbusquedaTemporal(14)
                    Busqueda.AlertaNotificado = arrbusquedaTemporal(15)

                    upBusquedaAvanzada.Update()
                End If
            End If
        End If
    End Sub
#Region "Filtros"
    ''' <summary>
    ''' Carga en la grid las columnas y las instancias del filtro seleccionado. 
    ''' Carga el desplegable con las columnas, el nº de instancias por estado y activa la pestaña del filtro.
    ''' Muestra el botón de Configurar para hacer modificaciones en la configuración del filtro.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: Al hacer click sobre una de las pestañas de los filtros. Tiempo máximo: 0 sg.</remarks>
    Protected Sub fsnTabFiltro_Click(ByVal sender As Object, ByVal e As EventArgs)
        _pageNumber = 0
        Dim arrParametros() As String
        arrParametros = Split(CType(sender, FSNWebControls.FSNTab).CommandArgument, "#")
        Session("CONTR_FilaEstado") = Nothing
        seleccionadoEstadoGrid = False
        Session("CONTR_Ordenacion") = ""
        If arrParametros(0) = 0 Then
            Session("IDFiltroContratoUsuario") = ""
            Session("PMCNombreTabla") = ""
            Session("IDFiltroContrato") = ""
        Else
            Session("IDFiltroContratoUsuario") = arrParametros(0)
            Session("PMCNombreTabla") = arrParametros(1)
            Session("IDFiltroContrato") = arrParametros(2)
        End If
        Session("pageNumberVCont") = "0"

        PMFiltroUsuarioIdVContr.Value = Session("IDFiltroContratoUsuario")
        PMNombreTablaVContr.Value = Session("PMCNombreTabla")
        PMFiltroIdVContr.Value = Session("IDFiltroContrato")

        Me.txtCodigoContrato.Text = ""
        UpdatePanel6.Update()

        btnConfigurar.Visible = (Session("IDFiltroContratoUsuario") <> "")
        btnConfigurar.Attributes.Add("onClick", "window.location='ConfigurarFiltrosContratos.aspx?desdeGS=" & IIf(m_desdeGS, "1", "") & "&SessionID=" & Request.QueryString("SessionID") & "&IDFiltroConfigurando=" & Session("IDFiltroContratoUsuario") & "&NombreTabla=" & Session("PMCNombreTabla") & "';return false;")
        hOrdered.Value = ""
        whgContratos.GridView.Behaviors.Sorting.SortedColumns.Clear()
        whgContratos.GroupingSettings.GroupedColumns.Clear()
        LimpiarBusquedaAvanzada()
        CargarBusquedaAvanzada()
        DesseleccionarEstados()
        SeleccionarEstado()
        ''limpiar el filtering tras cada cambio de filtro
        whgContratos.Behaviors.Filtering.ColumnSettings.Clear()
        whgContratos.GridView.Behaviors.Filtering.ColumnSettings.Clear()
        whgContratos.Behaviors.Filtering.ColumnFilters.Clear()
        whgContratos.GridView.Behaviors.Filtering.ColumnFilters.Clear()
        whgContratos.GridView.Behaviors.Filtering.ApplyFilter()
        hFiltered.Value = "0"
        whgContratos.Columns.Clear()
        whgContratos.GridView.Columns.Clear()
        whgContratos.GridView.Behaviors.Paging.PageIndex = 0
        RecargarContratosWHG()
        hCacheLoaded.Value = 0
        hCacheTabsEstados.Value = 1
        updFiltros.Update()
        'solo se queda chequeada la pestaña del filtro chequeada
        For index As Integer = 0 To dlFiltrosConfigurados.Items.Count - 1
            CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).Selected = False
        Next
        CType(sender, FSNWebControls.FSNTab).Selected = True
        Dim sColsGroupBy As String = "ColsGroupBy" & Session("IDFiltroContratoUsuario")
        Session(sColsGroupBy) = ""
        InitGroupBy()
    End Sub
    ''' <summary>
    ''' Refleja en el control paginador los ultimos cambios en los contratos a mostrar
    ''' </summary>
    ''' <param name="_pageCount">Numero de paginas</param>
    ''' <remarks>Llamada desde:whgContratos_DataBound; Tiempo maximo: 0,2 </remarks>
    Private Sub Paginador(ByVal _pageCount As Integer)
        Dim pagerList As DropDownList = DirectCast(whgContratos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("PagerPageList"), DropDownList)
        pagerList.Items.Clear()
        For i As Integer = 1 To _pageCount
            pagerList.Items.Add(i.ToString())
        Next
        CType(whgContratos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblCount"), Label).Text = _pageCount
        If _pageCount > 0 Then pagerList.SelectedIndex = _pageNumber
        Dim Desactivado As Boolean = (_pageNumber = 0)
        With CType(whgContratos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        With CType(whgContratos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        Desactivado = (_pageCount = 1 OrElse _pageNumber + 1 = _pageCount)
        With CType(whgContratos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        With CType(whgContratos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
    End Sub
#End Region
    Private Sub ddlDenominacion_SelectionChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownSelectionChangedEventArgs) Handles ddlDenominacion.SelectionChanged
        Dim valor As String = ddlDenominacion.Items(ddlDenominacion.ActiveItemIndex).Value
        Dim oItem As Infragistics.Web.UI.ListControls.DropDownItem
        ddlCodigo.ClearSelection()
        For Each oItem In ddlCodigo.Items
            If oItem.Value = valor Then
                oItem.Selected = True
                ddlCodigo.CurrentValue = oItem.Text
                Exit For
            End If
        Next
    End Sub
    Private Sub InitGroupBy()
        Dim sColsGroupBy As String = "ColsGroupBy" & Session("IDFiltroContratoUsuario")
        If Session(sColsGroupBy) <> Nothing Then
            Dim Coleccion() As String = Split(Session(sColsGroupBy).ToString, "#")
            For Each Columna As String In Coleccion
                If Not (Columna = "") Then
                    If Right(Columna, 3) = "ASC" Then
                        Columna = Columna.Replace(" ASC", "")
                    Else
                        Columna = Columna.Replace(" DESC", "")
                    End If
                End If
            Next
        End If
    End Sub
    ''' <summary>
    ''' Prueba de funcion que solo recarga el grid hierarchical
    ''' </summary>
    ''' <remarks>Llamada desde: Los clicks de todos los estados. // Tiempo máximo: 2seg (Segun el nº de noConformidades)
    ''' </remarks>
    Private Sub RecargarContratosWHG()
        Dim ds As DataSet
        ds = Contratos
        With whgContratos
            .Rows.Clear()
            .Columns.Clear()
            .GridView.Columns.Clear()
            .GridView.ClearDataSource()
            .DataSource = ds
            .GridView.DataSource = .DataSource
            CreacionEdicionColumnasGrid()
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorContratos
            .DataMember = "ContratosFiltrados"
            .DataKeyFields = "ID"
            .DataBind()
            updGridContratos.Update()
        End With
        If ds.Tables.Count = 1 Then
            ComprobarAlertas(0, 0)
        Else
            ComprobarAlertas(ds.Tables(1).Rows(0)("PDTE_DE_TRATAR"), ds.Tables(1).Rows(0)("PROXIMO_A_EXPIRAR"))
        End If
    End Sub
    Private Sub CargarGridExportacion()
        Dim ds As DataSet
        RecargarCache()
        With whgContratosExportacion
            .Rows.Clear()
            .Columns.Clear()
            .GridView.Columns.Clear()
            .GridView.ClearDataSource()
            _exportacion = True
            ds = Contratos
            .DataSource = ds
            .GridView.DataSource = .DataSource
            CreacionEdicionColumnasGridExportacion()
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorContratos
            .DataMember = "ContratosFiltrados"
            .DataKeyFields = "ID"
            .DataBind()
            updGridContratos.Update()
        End With
    End Sub
    ''' <summary>
    ''' Comprueba si el usuario tiene contratos pendientes
    ''' </summary>
    ''' <param name="lNumContratosPendientes"></param>
    ''' <param name="lNumContratosExpirar"></param>
    Private Sub ComprobarAlertas(ByVal lNumContratosPendientes As Long, ByVal lNumContratosExpirar As Long)
        If Not FSNServer.TipoAcceso.gbCMVerTriangulo Then
            phAlerta.Visible = False
        Else
            If lNumContratosPendientes > 0 Or lNumContratosExpirar > 0 Then
                phAlerta.Visible = True
                If lNumContratosPendientes > 0 Then
                    lbPendientes.Text = Textos(41)
                Else
                    lbPendientes.Text = ""
                End If
                If lNumContratosExpirar > 0 Then
                    lbExpirar.Text = Textos(42)
                Else
                    lbExpirar.Text = ""
                End If
            Else
                phAlerta.Visible = False
            End If
        End If
        upAlerta.Update()
    End Sub
    ''' <summary>
    ''' Inicializa los arrays con los campos generales
    ''' </summary>
    ''' <remarks>Llamada desde=Page_Load uwgNoConformidades_InitializeLayout// ; Tiempo máximo:0seg.</remarks>
    Private Sub InicializarArraysCamposGenerales()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorContratos
        If Session("CONTR_arrDatosGenerales") Is Nothing Then
            'NOMBRE DE LAS COLUMNAS DE LA GRID!!!
            ReDim arrDatosGenerales(17)
            arrDatosGenerales(1) = "CODIGO"
            arrDatosGenerales(2) = "TIPO_CONTRATO"
            arrDatosGenerales(3) = "CODPRO"
            arrDatosGenerales(4) = "PROVEEDOR"
            arrDatosGenerales(5) = "CONTACTO"
            arrDatosGenerales(6) = "PETICIONARIO"
            arrDatosGenerales(7) = "FEC_ALTA"
            arrDatosGenerales(8) = "MON"
            arrDatosGenerales(9) = "FECHA_INICIO"
            arrDatosGenerales(10) = "FECHA_EXP"
            arrDatosGenerales(11) = "ESTADO"
            arrDatosGenerales(12) = "PROCE"
            arrDatosGenerales(13) = "IMPORTE"
            arrDatosGenerales(14) = "SITUACIONACTUAL"
            arrDatosGenerales(15) = "ALERTA_EXPIRACION"
            arrDatosGenerales(16) = "NOTIFICADOS"
            arrDatosGenerales(17) = "FECHA_ULT_NOTIFICACION"

            Session("CONTR_arrDatosGenerales") = arrDatosGenerales

            ReDim arrDatosGeneralesDen(20)
            arrDatosGeneralesDen(1) = Textos(24)
            For i = 2 To 10
                arrDatosGeneralesDen(i) = Textos(i + 8)
            Next
            arrDatosGeneralesDen(11) = Textos(34)
            arrDatosGeneralesDen(12) = Textos(64)
            arrDatosGeneralesDen(13) = Textos(65)
            arrDatosGeneralesDen(14) = Textos(53)
            arrDatosGeneralesDen(15) = Textos(69)
            arrDatosGeneralesDen(16) = Textos(70)
            arrDatosGeneralesDen(18) = Textos(103)
            arrDatosGeneralesDen(19) = Textos(104)
            arrDatosGeneralesDen(20) = Textos(105)

            ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorFacturas
            arrDatosGeneralesDen(17) = Textos(39)
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorContratos
            Session("CONTR_arrDatosGeneralesDen") = arrDatosGeneralesDen

            arrOrdenEstados(0) = EstadosVisorContratos.Guardado
            arrOrdenEstados(1) = EstadosVisorContratos.En_Curso_De_Aprobacion
            arrOrdenEstados(2) = EstadosVisorContratos.Vigentes
            arrOrdenEstados(3) = EstadosVisorContratos.Proximo_a_Expirar
            arrOrdenEstados(4) = EstadosVisorContratos.Expirados
            arrOrdenEstados(5) = EstadosVisorContratos.Rechazados
            arrOrdenEstados(6) = EstadosVisorContratos.Anulados
            arrOrdenEstados(7) = EstadosVisorContratos.Todas
            Session("CONTR_arrOrdenEstados") = arrOrdenEstados

            arrTextosEstados(0) = Textos(0)  '"Guardados"
            arrTextosEstados(1) = Textos(1)  '"En curso de aprobacion"
            arrTextosEstados(2) = Textos(2)  '"Vigentes"
            arrTextosEstados(3) = Textos(3) '"proximo a expirar"
            arrTextosEstados(4) = Textos(4) '"Expirados"
            arrTextosEstados(5) = Textos(5) '"Rechazados"
            arrTextosEstados(6) = Textos(6) '"Anulados"
            arrTextosEstados(7) = Textos(7) '"Todos"
            Session("CONTR_arrTextosEstados") = arrTextosEstados

            arrTextosEstadosMini(0) = Textos(0)  '"Guardados"
            arrTextosEstadosMini(1) = Textos(1)  '"En curso de aprobacion"
            arrTextosEstadosMini(2) = Textos(2)  '"Vigentes"
            arrTextosEstadosMini(3) = Textos(3) '"proximo a expirar"
            arrTextosEstadosMini(4) = Textos(4) '"Expirados"
            arrTextosEstadosMini(5) = Textos(5) '"Rechazados"
            arrTextosEstadosMini(6) = Textos(6) '"Anulados"
            arrTextosEstadosMini(7) = Textos(7) '"Todos"
            Session("CONTR_arrTextosEstadosMini") = arrTextosEstadosMini
        Else
            arrDatosGenerales = CType(Session("CONTR_arrDatosGenerales"), Array)
            arrDatosGeneralesDen = CType(Session("CONTR_arrDatosGeneralesDen"), Array)
            arrTextosEstados = CType(Session("CONTR_arrTextosEstados"), Array)
            arrOrdenEstados = CType(Session("CONTR_arrOrdenEstados"), Array)
            arrTextosEstadosMini = CType(Session("CONTR_arrTextosEstadosMini"), Array)
        End If
    End Sub
    ''' <summary>
    ''' Limpia las propiedades de la busqueda avanzada
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LimpiarBusquedaAvanzada()
        Busqueda.MaterialHidden = ""
        Busqueda.MaterialTexto = ""
        Busqueda.ArticuloHidden = ""
        Busqueda.ArticuloTexto = ""
        Busqueda.EmpresaTexto = ""
        Busqueda.EmpresaHidden = ""
        Busqueda.FechaInicio = Nothing
        Busqueda.FechaExpiracion = Nothing
        Busqueda.Peticionario = ""
        Busqueda.Estado = ""
        Busqueda.Descripcion = ""
        Busqueda.PalabraClave = ""
        Busqueda.CentroCoste = ""
        Busqueda.AlertaExpiracion = 0
        Busqueda.AlertaPeriodo = 1
        Busqueda.AlertaNotificado = String.Empty

        upBusquedaAvanzada.Update()
    End Sub
    ''' <summary>
    ''' Limpia los combos de procesos
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LimpiarProcesos()
        ddlCommodity.ClearSelection()
        ddlCommodity.CurrentValue = ""
        ddlCodigo.Items.Clear()
        ddlCodigo.CurrentValue = ""
        ddlDenominacion.Items.Clear()
        ddlDenominacion.CurrentValue = ""
    End Sub
    ''' <summary>
    ''' Almacena en una variable de session la configuracion que tiene el usuario en la busqueda avanzada, para 
    ''' que al ir al detalle de la noConformidad y al volver pueda cargar la configuracion que tenia
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub AlmacenarBusquedaAvanzada()
        Dim arrbusquedaTemporal As New ArrayList
        arrbusquedaTemporal.Add(Busqueda.MaterialHidden)
        arrbusquedaTemporal.Add(Busqueda.MaterialTexto)
        arrbusquedaTemporal.Add(Busqueda.ArticuloHidden)
        arrbusquedaTemporal.Add(Busqueda.ArticuloTexto)
        arrbusquedaTemporal.Add(Busqueda.FechaInicio)
        arrbusquedaTemporal.Add(Busqueda.FechaExpiracion)
        arrbusquedaTemporal.Add(Busqueda.Peticionario)
        arrbusquedaTemporal.Add(Busqueda.Estado)
        arrbusquedaTemporal.Add(Busqueda.PalabraClave)
        arrbusquedaTemporal.Add(Busqueda.Descripcion)
        arrbusquedaTemporal.Add(Busqueda.EmpresaHidden)
        arrbusquedaTemporal.Add(Busqueda.EmpresaTexto)
        arrbusquedaTemporal.Add(Busqueda.CentroCoste)

        arrbusquedaTemporal.Add(Busqueda.AlertaExpiracion)
        arrbusquedaTemporal.Add(Busqueda.AlertaPeriodo)
        arrbusquedaTemporal.Add(Busqueda.AlertaNotificado)

        If Session("IDFiltroContratoUsuario") <> "" Then
            Session("CONTR_BusquedaAvanzada") = arrbusquedaTemporal
        Else
            Session("CONTR_BusquedaAvanzada0") = arrbusquedaTemporal
        End If
    End Sub
    ''' <summary>
    ''' Nos lleva a la pagina que lleva el proceso de añadir nuevos filtros de usuario
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Tiempo máximo=0,1</remarks>
    Private Sub btnAnyadirFiltros_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnyadirFiltros.Click
        Response.Redirect("ConfigurarFiltrosContratos.aspx?desdeGS=" & IIf(m_desdeGS, "1", "") & "&SessionID=" & Request.QueryString("SessionID"))
    End Sub
    ''' <summary>
    ''' Realiza la busqueda de contratos
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnBuscarIdentificador_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarIdentificador.Click
        If Page.IsValid Then
            m_bBusqueda = True
            'Session("BuscarPorProveedor") = txtProveedor.Text
            'Session("BuscarPorTipo") = wddTipo.CurrentValue
            RecargarContratosWHG()
            m_bBusqueda = False
            updGridContratos.Update()

            Dim sColsGroupBy As String = "ColsGroupBy" & Session("IDFiltroContratoUsuario")
            Session(sColsGroupBy) = ""
            InitGroupBy()
        End If
    End Sub
    ''' <summary>
    ''' Limpiar los criterios de busqueda
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Tiempo máximo=0</remarks>
    Private Sub btnLimpiarBusquedaAvanzada_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiarBusquedaAvanzada.Click
        LimpiarBusquedaAvanzada()
        AlmacenarBusquedaAvanzada()
    End Sub
#Region "Eventos de la Grid"
    Private Sub CreacionEdicionColumnasGrid()
        whgContratos.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize")
        whgContratos.GridView.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize")

        ObtenerColumnasGrid(whgContratos.UniqueID)
        CrearColumnasGrid(whgContratos.UniqueID)
    End Sub
    Private Sub CreacionEdicionColumnasGridExportacion()
        ObtenerColumnasGrid(whgContratosExportacion.UniqueID)
        CrearColumnasGrid(whgContratosExportacion.UniqueID)
    End Sub
    ''' <summary>
    ''' Inicializa las columnas del grid con los campos generales
    ''' </summary>
    ''' <remarks>Llamada desde=Page_Load uwgCertificados_InitializeLayout// ; Tiempo máximo:0seg.</remarks>
    Private Sub ObtenerColumnasGrid(ByVal gridId As String)
        InicializarArraysCamposGenerales()

        Dim grid As WebHierarchicalDataGrid = CType(Me.FindControl(gridId), WebHierarchicalDataGrid)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorContratos
        If Session("IDFiltroContratoUsuario") = "" OrElse Session("IDFiltroContratoUsuario") = "0" Then
            'NOMBRE DE LAS COLUMNAS DE LA GRID!!!
            Dim camposGrid As New List(Of Infragistics.Web.UI.GridControls.BoundDataField)
            Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "ID"
                .DataFieldName = "ID"
                .Header.Text = "ID"
                .Width = Unit.Pixel(0)
                .Hidden = True
                .VisibleIndex = 3
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "CODIGO"
                .DataFieldName = "CODIGO"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(1) 'Código
                .Header.Tooltip = arrDatosGeneralesDen(1) 'Código
                .Width = Unit.Pixel(105)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 4
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "FEC_ALTA"
                .DataFieldName = "FEC_ALTA"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(2) '"Fecha anulación"
                .Header.Tooltip = arrDatosGeneralesDen(2) '"Fecha anulación"
                .Width = Unit.Pixel(95)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 5
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "FECHA_INICIO"
                .DataFieldName = "FECHA_INICIO"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(3) '"Fecha anulación"
                .Header.Tooltip = arrDatosGeneralesDen(3) '"Fecha anulación"
                .Width = Unit.Pixel(95)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 6
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "FECHA_EXP"
                .DataFieldName = "FECHA_EXP"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(4) ' "Fecha cierre"
                .Header.Tooltip = arrDatosGeneralesDen(4) ' "Fecha cierre"
                .Width = Unit.Pixel(125)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 7
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "CODPRO"
                .DataFieldName = "CODPRO"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(15) '"Proveedor"
                .Header.Tooltip = arrDatosGeneralesDen(15) '"Proveedor"
                .Width = Unit.Pixel(100)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 8
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "PROVEEDOR"
                .DataFieldName = "PROVEEDOR"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(16) '"Proveedor"
                .Header.Tooltip = arrDatosGeneralesDen(16) '"Proveedor"
                .Width = Unit.Pixel(165)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 9
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "ESTADO"
                .DataFieldName = "ESTADO"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(10)
                .Header.Tooltip = arrDatosGeneralesDen(10)
                .Width = Unit.Pixel(165)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 10
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "PETICIONARIO"
                .DataFieldName = "PETICIONARIO"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(8) '"Peticionario"
                .Header.Tooltip = arrDatosGeneralesDen(8) '"Peticionario"
                .Width = Unit.Pixel(165)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 12
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "CODPET"
                .DataFieldName = "CODPET"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(8) '"Codigo Peticionario"
                .Header.Tooltip = arrDatosGeneralesDen(8) '" Codigo Peticionario"
                .Width = Unit.Pixel(165)
                .Hidden = True
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 13
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "TIPO_CONTRATO"
                .DataFieldName = "TIPO_CONTRATO"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(9)
                .Header.Tooltip = arrDatosGeneralesDen(9)
                .Width = Unit.Pixel(165)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 14
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "MON"
                .DataFieldName = "MON"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(14)
                .Header.Tooltip = arrDatosGeneralesDen(14)
                .Width = Unit.Pixel(100)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 15
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "BLOQUE"
                .DataFieldName = "BLOQUE"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(14)
                .Header.Tooltip = arrDatosGeneralesDen(14)
                .Width = Unit.Pixel(100)
                .Hidden = True
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 16
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "ACCION_APROBAR"
                .DataFieldName = "ACCION_APROBAR"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(14)
                .Header.Tooltip = arrDatosGeneralesDen(14)
                .Width = Unit.Pixel(100)
                .Hidden = True
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 17
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "ACCION_RECHAZAR"
                .DataFieldName = "ACCION_RECHAZAR"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(14)
                .Header.Tooltip = arrDatosGeneralesDen(14)
                .Width = Unit.Pixel(100)
                .Hidden = True
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 18
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "CODEMP"
                .DataFieldName = "CODEMP"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(14)
                .Header.Tooltip = arrDatosGeneralesDen(14)
                .Width = Unit.Pixel(100)
                .Hidden = True
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 19
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "ICONO"
                .DataFieldName = "ICONO"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(14)
                .Header.Tooltip = arrDatosGeneralesDen(14)
                .Width = Unit.Pixel(100)
                .Hidden = True
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 20
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "TRASLADADA"
                .DataFieldName = "TRASLADADA"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(14)
                .Header.Tooltip = arrDatosGeneralesDen(14)
                .Width = Unit.Pixel(100)
                .Hidden = True
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 21
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "OBSERVADOR"
                .DataFieldName = "OBSERVADOR"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(14)
                .Header.Tooltip = arrDatosGeneralesDen(14)
                .Width = Unit.Pixel(100)
                .Hidden = True
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 22
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "ID_CONTRATO"
                .DataFieldName = "ID_CONTRATO"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(14)
                .Header.Tooltip = arrDatosGeneralesDen(14)
                .Width = Unit.Pixel(100)
                .Hidden = True
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 23
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "SITUACIONACTUAL"
                .DataFieldName = "SITUACIONACTUAL"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(17)
                .Header.Tooltip = arrDatosGeneralesDen(17)
                .Width = Unit.Pixel(215)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 24
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "ALERTA_EXPIRACION"
                .DataFieldName = "ALERTA_EXPIRACION"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(18)
                .Header.Tooltip = arrDatosGeneralesDen(18)
                .Width = Unit.Pixel(215)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 25
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "NOTIFICADOS"
                .DataFieldName = "NOTIFICADOS"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(19)
                .Header.Tooltip = arrDatosGeneralesDen(19)
                .Width = Unit.Pixel(215)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 26
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "FECHA_ULT_NOTIFICACION"
                .DataFieldName = "FECHA_ULT_NOTIFICACION"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(20)
                .Header.Tooltip = arrDatosGeneralesDen(20)
                .Width = Unit.Pixel(215)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
                .VisibleIndex = 27
            End With
            camposGrid.Add(campoGrid)

            Session("arrDatosGeneralesVCont") = camposGrid

        Else
            For i As Integer = 0 To grid.Columns.Count - 1
                grid.Columns(i).Hidden = True
            Next
            Dim cFiltro As FiltroContrato
            cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroContrato))
            cFiltro.IDFiltro = strToDbl(Session("IDFiltroContrato"))
            cFiltro.IDFiltroUsuario = Session("IDFiltroContratoUsuario")
            cFiltro.Persona = FSNUser.CodPersona
            cFiltro.Idioma = FSNUser.Idioma
            cFiltro.LoadFiltro()

            Dim camposGrid As New List(Of Infragistics.Web.UI.GridControls.BoundDataField)
            Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField

            '--------------------------------------------------------------------------------------------------------------------
            For Each oRow As DataRow In cFiltro.CamposOrdenadosParaVisor.Rows
                'Tratamiento campos Generales + Formulario. Se recibe ORDER BY POS. Segun llega a crearlo.
                campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
                With campoGrid
                    If oRow("ES_GENERAL") = 1 Then
                        Select Case CType(oRow("CAMPO"), Integer)
                            Case ConfiguracionFiltrosContratos.CodigoContrato
                                .Key = "CODIGO"
                                .DataFieldName = "CODIGO"
                            Case ConfiguracionFiltrosContratos.Tipo
                                .Key = "TIPO_CONTRATO"
                                .DataFieldName = "TIPO_CONTRATO"
                            Case ConfiguracionFiltrosContratos.CodigoProveedor
                                .Key = "CODPRO"
                                .DataFieldName = "CODPRO"
                            Case ConfiguracionFiltrosContratos.DenominacionProveedor
                                .Key = "PROVEEDOR"
                                .DataFieldName = "PROVEEDOR"
                            Case ConfiguracionFiltrosContratos.Contacto
                                .Key = "CONTACTO"
                                .DataFieldName = "CONTACTO"
                            Case ConfiguracionFiltrosContratos.Peticionario
                                .Key = "PETICIONARIO"
                                .DataFieldName = "PETICIONARIO"
                            Case ConfiguracionFiltrosContratos.FechaAlta
                                .Key = "FEC_ALTA"
                                .DataFieldName = "FEC_ALTA"
                            Case ConfiguracionFiltrosContratos.Moneda
                                .Key = "MON"
                                .DataFieldName = "MON"
                            Case ConfiguracionFiltrosContratos.FechaInicio
                                .Key = "FECHA_INICIO"
                                .DataFieldName = "FECHA_INICIO"
                            Case ConfiguracionFiltrosContratos.FechaExpiracion
                                .Key = "FECHA_EXP"
                                .DataFieldName = "FECHA_EXP"
                            Case ConfiguracionFiltrosContratos.Estado
                                .Key = "ESTADO"
                                .DataFieldName = "ESTADO"
                            Case ConfiguracionFiltrosContratos.ProcesoCompra
                                .Key = "PROCE"
                                .DataFieldName = "PROCE"
                            Case ConfiguracionFiltrosContratos.Importe
                                .Key = "IMPORTE"
                                .DataFieldName = "IMPORTE"
                            Case ConfiguracionFiltrosContratos.SituacionActual
                                .Key = "SITUACIONACTUAL"
                                .DataFieldName = "SITUACIONACTUAL"
                            Case ConfiguracionFiltrosContratos.AlertaExpiracion
                                .Key = "ALERTA_EXPIRACION"
                                .DataFieldName = "ALERTA_EXPIRACION"
                            Case ConfiguracionFiltrosContratos.Notificados
                                .Key = "NOTIFICADOS"
                                .DataFieldName = "NOTIFICADOS"
                            Case ConfiguracionFiltrosContratos.FechaUltNotificacion
                                .Key = "FECHA_ULT_NOTIFICACION"
                                .DataFieldName = "FECHA_ULT_NOTIFICACION"
                        End Select
                        .Header.Text = DBNullToSomething(oRow("NOMBRE"))
                        .Header.Tooltip = DBNullToSomething(oRow("NOMBRE"))
                        .Width = DBNullToDbl(oRow("TAMANYO"))
                        .Hidden = Not CBool(oRow("VISIBLE"))
                    Else
                        .Key = "C_" & oRow("CAMPO")
                        .DataFieldName = "C_" & oRow("CAMPO")
                        If DBNullToStr(oRow("NOMBRE_PER")) <> "" Then
                            .Header.Text = DBNullToSomething(oRow("NOMBRE_PER"))
                            .Header.Tooltip = DBNullToSomething(oRow("NOMBRE_PER"))
                        Else
                            .Header.Text = DBNullToSomething(oRow("NOMBRE"))
                            .Header.Tooltip = DBNullToSomething(oRow("NOMBRE"))
                        End If
                        .Width = DBNullToDbl(oRow("TAMANYO"))
                        .Hidden = Not CBool(oRow("VISIBLE"))

                        If grid.Columns(campoGrid.Key) IsNot Nothing Then grid.Columns(campoGrid.Key).Hidden = Not CType(oRow("VISIBLE"), Boolean)
                    End If

                    camposGrid.Add(campoGrid)
                End With
            Next
            '--------------------------------------------------------------------------------------------------------------------

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "ID"
                .DataFieldName = "ID"
                .Header.Text = "ID"
                .Width = Unit.Pixel(0)
                .Hidden = True
            End With
            camposGrid.Add(campoGrid)

            'Campos anyadidos a compos generales necesarios para el funcionamiento de contratos
            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "ACCION_APROBAR"
                .DataFieldName = "ACCION_APROBAR"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(14)
                .Header.Tooltip = arrDatosGeneralesDen(14)
                .Width = Unit.Pixel(100)
                .Hidden = True
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "ACCION_RECHAZAR"
                .DataFieldName = "ACCION_RECHAZAR"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(14)
                .Header.Tooltip = arrDatosGeneralesDen(14)
                .Width = Unit.Pixel(100)
                .Hidden = True
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "CODEMP"
                .DataFieldName = "CODEMP"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(14)
                .Header.Tooltip = arrDatosGeneralesDen(14)
                .Width = Unit.Pixel(100)
                .Hidden = True
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "ICONO"
                .DataFieldName = "ICONO"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(14)
                .Header.Tooltip = arrDatosGeneralesDen(14)
                .Width = Unit.Pixel(100)
                .Hidden = True
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "TRASLADADA"
                .DataFieldName = "TRASLADADA"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(14)
                .Header.Tooltip = arrDatosGeneralesDen(14)
                .Width = Unit.Pixel(100)
                .Hidden = True
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "OBSERVADOR"
                .DataFieldName = "OBSERVADOR"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(14)
                .Header.Tooltip = arrDatosGeneralesDen(14)
                .Width = Unit.Pixel(100)
                .Hidden = True
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "ID_CONTRATO"
                .DataFieldName = "ID_CONTRATO"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(14)
                .Header.Tooltip = arrDatosGeneralesDen(14)
                .Width = Unit.Pixel(100)
                .Hidden = True
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "BLOQUE"
                .DataFieldName = "BLOQUE"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = arrDatosGeneralesDen(14)
                .Header.Tooltip = arrDatosGeneralesDen(14)
                .Width = Unit.Pixel(100)
                .Hidden = True
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            Session("arrDatosGeneralesVCont") = camposGrid

            cFiltro = Nothing
        End If
    End Sub
    Private Sub CrearColumnasGrid(ByVal gridId As String)
        Dim grid As WebHierarchicalDataGrid = CType(Me.FindControl(gridId), WebHierarchicalDataGrid)
        Dim campoGridTemplate As Infragistics.Web.UI.GridControls.UnboundField

        campoGridTemplate = New Infragistics.Web.UI.GridControls.UnboundField(True)
        With campoGridTemplate
            .Key = "INDICADOR_EN_PROCESO"
            .Header.Text = ""
            .Header.Tooltip = ""
            .Width = Unit.Pixel(25)
            .Hidden = False
            .VisibleIndex = 0
        End With
        If grid.GridView.Columns.Item(campoGridTemplate.Key) Is Nothing Then
            grid.GridView.Columns.Add(campoGridTemplate)
            AnyadirTextoAColumnaNoFiltrada(gridId, campoGridTemplate.Key, False, False)
        End If

        AddColumnTemplate("btnAprobar", "TEMPLATEAPROBAR", Textos(89), "cbAprobar", 1)
        AddColumnTemplate("btnRechazar", "TEMPLATERECHAZAR", Textos(90), "cbRechazar", 2)

        For Each field As Infragistics.Web.UI.GridControls.BoundDataField In CType(Session("arrDatosGeneralesVCont"), List(Of Infragistics.Web.UI.GridControls.BoundDataField))
            Select Case field.Key
                Case "ESTADO"
                    campoGridTemplate = New Infragistics.Web.UI.GridControls.UnboundField(True)
                    With campoGridTemplate
                        .Key = "IMAGE_EST"
                        .Header.Text = ""
                        .Header.Tooltip = ""
                        .Header.CssClass = "headerNoWrap"
                        .Width = Unit.Pixel(30)
                        .Hidden = False
                        .CssClass = "itemSeleccionable SinSalto"
                        .VisibleIndex = 9
                    End With
                    If grid.GridView.Columns.Item(campoGridTemplate.Key) Is Nothing Then
                        grid.GridView.Columns.Add(campoGridTemplate)
                        AnyadirTextoAColumnaNoFiltrada(gridId, campoGridTemplate.Key, False, False)
                    Else
                        grid.GridView.Columns.Item(campoGridTemplate.Key).Hidden = field.Hidden
                    End If

            End Select
            If grid.GridView.Columns.Item(field.Key) Is Nothing AndAlso (DBNullToStr(field.Key) <> "") Then
                grid.GridView.Columns.Add(field)
                AnyadirTextoAColumnaNoFiltrada(gridId, field.Key, True, True)
            End If
        Next
    End Sub
    Private Sub AddColumnTemplate(ByVal id As String, ByVal key As String, ByVal buttonText As String, ByVal idChck As String, Optional ByVal index As Integer = -1)
        Dim field As New TemplateDataField
        field.HeaderTemplate = New ButtonTemplate(id, buttonText)
        field.ItemTemplate = New CheckboxTemplate(idChck)

        field.Key = key
        field.Header.CssClass = "itemCenterAligment"
        field.CssClass = "itemCenterAligment"
        field.VisibleIndex = index
        field.Width = Unit.Pixel(80)
        field.Hidden = True

        If Me.whgContratos.GridView.Columns(field.Key) Is Nothing Then
            Me.whgContratos.GridView.Columns.Add(field)
        End If
        If Me.whgContratosExportacion.GridView.Columns(field.Key) Is Nothing Then
            Me.whgContratosExportacion.GridView.Columns.Add(field)
        End If
    End Sub
    Private Sub AnyadirTextoAColumnaNoFiltrada(ByVal gridId As String, ByVal fieldKey As String, ByVal EnableFilter As Boolean, ByVal EnableResize As Boolean)
        Dim grid As WebHierarchicalDataGrid = CType(Me.FindControl(gridId), WebHierarchicalDataGrid)
        Dim fieldFilter As Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldFilter = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldFilter.ColumnKey = fieldKey
        fieldFilter.Enabled = EnableFilter
        fieldFilter.BeforeFilterAppliedAltText = Textos(72) 'Filtro no aplicado
        grid.GridView.Behaviors.Filtering.ColumnSettings.Add(fieldFilter)

        Dim fieldResize As Infragistics.Web.UI.GridControls.ColumnResizeSetting
        fieldResize = New Infragistics.Web.UI.GridControls.ColumnResizeSetting
        fieldResize.ColumnKey = fieldKey
        fieldResize.EnableResize = EnableResize
        grid.GridView.Behaviors.ColumnResizing.ColumnSettings.Add(fieldResize)
    End Sub
    Private Sub whgContratos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles whgContratos.DataBound
        Dim _pageCount As Integer
        Dim _rows As Integer
        Dim mi_boton As New Fullstep.FSNWebControls.FSNButton
        Dim dt As DataTable = CType(whgContratos.DataSource, DataSet).Tables("ContratosFiltrados")

        If CType(hFiltered.Value, Boolean) Then
            _rows = dt.Rows.Count
        Else
            If Session("CONTR_FilaEstado") <> Nothing Then
                For index As Integer = 1 To 8
                    mi_boton = updGridContratos.FindControl("btnBoton" & index)
                    If mi_boton.Selected Then
                        _rows = CType(mi_boton.Attributes("NumeroContratos"), Integer)
                    End If
                Next
            End If
        End If
        _pageCount = _rows \ System.Configuration.ConfigurationManager.AppSettings("PageSize")
        _pageCount = _pageCount + (IIf(_rows Mod System.Configuration.ConfigurationManager.AppSettings("PageSize") = 0, 0, 1))
        Paginador(_pageCount)
        updGridContratos.Update()
    End Sub
    ''' <summary>
    ''' Tras filtrarse actualiza el grid
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub whgContratos_DataFiltered(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.FilteredEventArgs) Handles whgContratos.DataFiltered
        updGridContratos.Update()
    End Sub
    ''' <summary>
    ''' Tras agrupar actualiza el grid
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub whgContratos_GroupedColumnsChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.GroupedColumnsChangedEventArgs) Handles whgContratos.GroupedColumnsChanged
        Dim sColsGroupBy As String = "ColsGroupBy" & Session("IDFiltroContratoUsuario")
        Dim groupedColumnList As String = String.Empty
        For Each column As Infragistics.Web.UI.GridControls.GroupedColumn In e.GroupedColumns
            groupedColumnList = IIf(groupedColumnList Is String.Empty, String.Empty, "#")
            groupedColumnList &= column.ColumnKey & IIf(column.SortDirection = Infragistics.Web.UI.GridControls.GroupingSortDirection.Ascending, " ASC", " DESC")
        Next
        Session(sColsGroupBy) = groupedColumnList
        updGridContratos.Update()
    End Sub
    Private Sub whgContratos_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whgContratos.InitializeRow
        Dim gridCellIndex As Integer
        Dim items As Infragistics.Web.UI.GridControls.GridRecordItemCollection = e.Row.Items

        If CType(e.Row.DataItem.Item.Row.Item("ICONO"), Integer) = 1 Then
            items.FindItemByKey("INDICADOR_EN_PROCESO").Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Img_Ico_Alerta_Peq.gif" & "'/>"
        End If
        'Estado
        If whgContratos.GridView.Columns.Item("EN_PROCESO") IsNot Nothing Then
            If CType(e.Row.DataItem.Item.Row.Item("EN_PROCESO"), Integer) = 1 Or CType(e.Row.DataItem.Item.Row.Item("EN_PROCESO"), Integer) = 2 Then
                items.FindItemByKey("ESTADO").Text = Textos(88)
            End If
        Else
            Select Case CType(e.Row.DataItem.Item.Row.Item("CODESTADO"), Integer)
                Case EstadosVisorContratos.Guardado
                    items.FindItemByKey("ESTADO").Text = Textos(0)
                Case EstadosVisorContratos.En_Curso_De_Aprobacion
                    items.FindItemByKey("ESTADO").Text = Textos(1)
                Case EstadosVisorContratos.Vigentes
                    items.FindItemByKey("ESTADO").Text = Textos(2)
                    items.FindItemByKey("ESTADO").CssClass = "certificadoVigente"
                    items.FindItemByKey("IMAGE_EST").Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_Vigente.gif" & "'/>"
                Case EstadosVisorContratos.Proximo_a_Expirar
                    items.FindItemByKey("ESTADO").Text = Textos(3)
                    items.FindItemByKey("ESTADO").CssClass = "certificadoPendiente"
                    items.FindItemByKey("IMAGE_EST").Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_Pendiente.gif" & "'/>"
                Case EstadosVisorContratos.Expirados
                    items.FindItemByKey("ESTADO").Text = Textos(4)
                    items.FindItemByKey("ESTADO").CssClass = "certificadoExpirado"
                    items.FindItemByKey("IMAGE_EST").Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_Expirado.gif" & "'/>"
                Case EstadosVisorContratos.Rechazados
                    items.FindItemByKey("ESTADO").Text = Textos(5)
                Case EstadosVisorContratos.Anulados
                    items.FindItemByKey("ESTADO").Text = Textos(6)
            End Select
        End If

        If Not bMostrarColumnaAprobar AndAlso CType(e.Row.DataItem.Item.Row.Item("ACCION_APROBAR"), Integer) <> 0 Then
            whgContratos.GridView.Columns("TEMPLATEAPROBAR").Hidden = False
        End If
        If Not bMostrarColumnaRechazar AndAlso CType(e.Row.DataItem.Item.Row.Item("ACCION_RECHAZAR"), Integer) <> 0 Then
            whgContratos.GridView.Columns("TEMPLATERECHAZAR").Hidden = False
        End If

        Dim index As Integer = whgContratos.GridView.Columns.FromKey("TEMPLATEAPROBAR").VisibleIndex
        Dim chkAprobar As CheckBox = CType(e.Row.Items(index).FindControl("cbAprobar"), CheckBox)
        'Si no tiene asociada accion tipo aprobar o rechazar se deshabilitan los checks
        If (CType(e.Row.DataItem.Item.Row.Item("ACCION_APROBAR"), Integer) = 0) Then
            chkAprobar.Enabled = False
        Else
            chkAprobar.Enabled = True
        End If
        index = whgContratos.GridView.Columns.FromKey("TEMPLATERECHAZAR").VisibleIndex
        Dim chkRechazar As CheckBox = CType(e.Row.Items(index).FindControl("cbRechazar"), CheckBox)
        If (CType(e.Row.DataItem.Item.Row.Item("ACCION_RECHAZAR"), Integer) = 0) Then
            chkRechazar.Enabled = False
        Else
            chkRechazar.Enabled = True
        End If

        If e.Row.DataItem.Item.Row.Item("SITUACIONACTUAL") = "##" Then items.FindItemByKey("SITUACIONACTUAL").Text = Textos(102) 'Etapas en paralelo

        For Each item As Object In whgContratos.GridView.Columns
            If item.Type = System.Type.GetType("System.DateTime") Then
                gridCellIndex = whgContratos.GridView.Columns.FromKey(item.Key).Index
                CType(items(gridCellIndex).Column, Infragistics.Web.UI.GridControls.BoundDataField).DataFormatString = "{0:" & FSNUser.DateFormat.ShortDatePattern & "}"
            ElseIf item.Type = System.Type.GetType("System.Byte") _
            AndAlso Left(item.key, 2) = "C_" _
            AndAlso Not (item.hidden) _
            AndAlso e.Row.DataItem.Item.Row.Item(item.key) IsNot System.DBNull.Value Then
                If e.Row.DataItem.Item.Row.Item(item.key) Then
                    items.FindItemByKey(item.key).Text = Textos(92) ' "Sí"            
                Else
                    items.FindItemByKey(item.key).Text = Textos(93) '"No"
                End If
            End If
        Next

        If Not IsNothing(e.Row.DataItem.Item.Row.Item("ALERTA_EXPIRACION")) AndAlso CStr(e.Row.DataItem.Item.Row.Item("ALERTA_EXPIRACION")).Length > 0 AndAlso
           Not IsNothing(e.Row.DataItem.Item.Row.Item("PERIODO_ALERTA")) Then
            Select Case e.Row.DataItem.Item.Row.Item("PERIODO_ALERTA")
                Case 1
                    e.Row.DataItem.Item.Row.Item("ALERTA_EXPIRACION") = CStr(e.Row.DataItem.Item.Row.Item("ALERTA_EXPIRACION")).Replace("##", Textos(106))
                Case 2
                    e.Row.DataItem.Item.Row.Item("ALERTA_EXPIRACION") = CStr(e.Row.DataItem.Item.Row.Item("ALERTA_EXPIRACION")).Replace("##", Textos(107))
                Case 3
                    e.Row.DataItem.Item.Row.Item("ALERTA_EXPIRACION") = CStr(e.Row.DataItem.Item.Row.Item("ALERTA_EXPIRACION")).Replace("##", Textos(108))
                Case Else
                    e.Row.DataItem.Item.Row.Item("ALERTA_EXPIRACION") = String.Empty
            End Select
        End If


    End Sub
#End Region
    ''' <summary>
    ''' Realiza la busqueda de contratos
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnBuscarBusquedaAvanzada_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarBusquedaAvanzada.Click
        If Page.IsValid Then
            m_bBusqueda = True
            hCacheLoaded.Value = 0
            RecargarContratosWHG()
            m_bBusqueda = False
            updFiltros.Update()
            updGridContratos.Update()
            AlmacenarBusquedaAvanzada()
            Dim sColsGroupBy As String = "ColsGroupBy" & Session("IDFiltroContratoUsuario")
            Session(sColsGroupBy) = ""
            Session("pageNumberVCont") = "0"
        End If
    End Sub
#Region "panTabsEstados"
    ''' <summary>
    ''' Carga en la grid las solicitudes guardadas y marca como seleccionado este botón.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>LLamada desde: Al hacer click en este botón. Tiempo máximo: 0 sg.</remarks>
    Private Sub btnBorrador_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBoton1.Click
        DesseleccionarEstados()
        CType(sender, FSNWebControls.FSNButton).Selected = True
        Session("CONTR_FilaEstado") = CType(sender, FSNWebControls.FSNButton).Attributes("Estado")
        Session("pageNumberVCont") = "0"
        seleccionadoEstadoGrid = True
        RecargarContratosWHG()
        seleccionadoEstadoGrid = False
        hCacheTabsEstados.Value = 0
    End Sub
    ''' <summary>
    ''' Carga en la grid las solicitudes guardadas y marca como seleccionado este botón.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>LLamada desde: Al hacer click en este botón. Tiempo máximo: 0 sg.</remarks>
    Private Sub btnEnCurso_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBoton2.Click
        DesseleccionarEstados()
        CType(sender, FSNWebControls.FSNButton).Selected = True
        Session("CONTR_FilaEstado") = CType(sender, FSNWebControls.FSNButton).Attributes("Estado")
        Session("pageNumberVCont") = "0"
        seleccionadoEstadoGrid = True
        RecargarContratosWHG()
        seleccionadoEstadoGrid = False
        hCacheTabsEstados.Value = 0
    End Sub
    ''' <summary>
    ''' Carga en la grid las solicitudes guardadas y marca como seleccionado este botón.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>LLamada desde: Al hacer click en este botón. Tiempo máximo: 0 sg.</remarks>
    Private Sub btnVigentes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBoton3.Click
        DesseleccionarEstados()
        CType(sender, FSNWebControls.FSNButton).Selected = True
        Session("CONTR_FilaEstado") = CType(sender, FSNWebControls.FSNButton).Attributes("Estado")
        Session("pageNumberVCont") = "0"
        seleccionadoEstadoGrid = True
        RecargarContratosWHG()
        seleccionadoEstadoGrid = False
        hCacheTabsEstados.Value = 0
    End Sub
    ''' <summary>
    ''' Carga en la grid las solicitudes guardadas y marca como seleccionado este botón.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>LLamada desde: Al hacer click en este botón. Tiempo máximo: 0 sg.</remarks>
    Private Sub btnProxExpirar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBoton4.Click
        DesseleccionarEstados()
        CType(sender, FSNWebControls.FSNButton).Selected = True
        Session("CONTR_FilaEstado") = CType(sender, FSNWebControls.FSNButton).Attributes("Estado")
        Session("pageNumberVCont") = "0"
        seleccionadoEstadoGrid = True
        RecargarContratosWHG()
        seleccionadoEstadoGrid = False
        hCacheTabsEstados.Value = 0
    End Sub
    ''' <summary>
    ''' Carga en la grid las solicitudes guardadas y marca como seleccionado este botón.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>LLamada desde: Al hacer click en este botón. Tiempo máximo: 0 sg.</remarks>
    Private Sub btnExpirados_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBoton5.Click
        DesseleccionarEstados()
        CType(sender, FSNWebControls.FSNButton).Selected = True
        Session("CONTR_FilaEstado") = CType(sender, FSNWebControls.FSNButton).Attributes("Estado")
        Session("pageNumberVCont") = "0"
        seleccionadoEstadoGrid = True
        RecargarContratosWHG()
        seleccionadoEstadoGrid = False
        hCacheTabsEstados.Value = 0
    End Sub
    ''' <summary>
    ''' Carga en la grid las solicitudes guardadas y marca como seleccionado este botón.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>LLamada desde: Al hacer click en este botón. Tiempo máximo: 0 sg.</remarks>
    Private Sub btnRechazados_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBoton6.Click
        DesseleccionarEstados()
        CType(sender, FSNWebControls.FSNButton).Selected = True
        Session("CONTR_FilaEstado") = CType(sender, FSNWebControls.FSNButton).Attributes("Estado")
        Session("pageNumberVCont") = "0"
        seleccionadoEstadoGrid = True
        RecargarContratosWHG()
        seleccionadoEstadoGrid = False
        hCacheTabsEstados.Value = 0
    End Sub
    ''' <summary>
    ''' Carga en la grid las solicitudes guardadas y marca como seleccionado este botón.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>LLamada desde: Al hacer click en este botón. Tiempo máximo: 0 sg.</remarks>
    Private Sub btnAnulados_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBoton7.Click
        DesseleccionarEstados()
        CType(sender, FSNWebControls.FSNButton).Selected = True
        Session("CONTR_FilaEstado") = CType(sender, FSNWebControls.FSNButton).Attributes("Estado")
        Session("pageNumberVCont") = "0"
        seleccionadoEstadoGrid = True
        RecargarContratosWHG()
        seleccionadoEstadoGrid = False
        hCacheTabsEstados.Value = 0
    End Sub
    ''' <summary>
    ''' Carga en la grid las solicitudes guardadas y marca como seleccionado este botón.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>LLamada desde: Al hacer click en este botón. Tiempo máximo: 0 sg.</remarks>
    Private Sub btnTodos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBoton8.Click
        DesseleccionarEstados()
        CType(sender, FSNWebControls.FSNButton).Selected = True
        Session("CONTR_FilaEstado") = CType(sender, FSNWebControls.FSNButton).Attributes("Estado")
        Session("pageNumberVCont") = "0"
        seleccionadoEstadoGrid = True
        RecargarContratosWHG()
        seleccionadoEstadoGrid = False
        hCacheTabsEstados.Value = 0
    End Sub
#End Region
    ''' <summary>
    ''' Selecciona la pestaña de los contratos pendientes
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lbPendientes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbPendientes.Click
        Dim mi_boton As New Fullstep.FSNWebControls.FSNButton
        mi_boton = updGridContratos.FindControl("btnBoton2")
        btnEnCurso_Click(mi_boton, e)
        updGridContratos.Update()
    End Sub
    ''' <summary>
    ''' Selecciona la pestaña de los contratos proximos a expirar
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lbExpirar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbExpirar.Click
        Dim mi_boton As New Fullstep.FSNWebControls.FSNButton
        mi_boton = updGridContratos.FindControl("btnBoton4")
        btnProxExpirar_Click(mi_boton, e)
        updGridContratos.Update()
    End Sub
    Private Sub dlFiltrosConfigurados_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlFiltrosConfigurados.Load
        If m_desdeGS Then
            For Each Control As Control In dlFiltrosConfigurados.Items
                CType(Control.FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).ImagenesBordes = System.Configuration.ConfigurationManager.AppSettings("rutaPM2008") & "/App_Themes/" & Page.Theme & "/images/BordesTabFiltro_GS.PNG"
            Next
        End If
    End Sub
    ''' <summary>
    ''' Evento que salta cuando se cambia la seleccion del combo
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ddlCommodity_SelectionChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownSelectionChangedEventArgs) Handles ddlCommodity.SelectionChanged
        CargarProcesos()
    End Sub
    ''' <summary>
    ''' Procedimiento que carga los procesos en los combos cuando hay cambio de seleccion en uno de ellos
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarProcesos()
        Dim Commodity As String = ""
        Dim Anyo As Short
        If ddlCommodity.SelectedValue <> "" Then
            Commodity = ddlCommodity.SelectedValue
        End If

        If IsNumeric(ddlAnyo.SelectedValue) Then
            Anyo = ddlAnyo.SelectedValue
        End If

        If Commodity <> "" Then
            CargarProcesos(Anyo, Commodity)
        Else
            ddlCodigo.Items.Clear()
            ddlDenominacion.Items.Clear()
        End If
        ddlCodigo.CurrentValue = ""
        ddlDenominacion.CurrentValue = ""
    End Sub
    ''' <summary>
    ''' Evento que salta cuando se cambia el valor del combo del anyo de proceso
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ddlAnyo_ValueChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownValueChangedEventArgs) Handles ddlAnyo.ValueChanged
        If Not ddlAnyo.Items.FindItemByValue(e.NewValue) Is Nothing Then
            Dim oItem As Infragistics.Web.UI.ListControls.DropDownItem = ddlAnyo.Items.FindItemByValue(e.NewValue)
            ddlAnyo.ClearSelection()
            oItem.Selected = True
            ddlAnyo.CurrentValue = oItem.Text
            ddlCommodity.ClearSelection()
            CargarProcesos()
        Else
            If e.NewValue.ToString.Length < e.OldValue.ToString.Length Then
                'Esta borrando el anyo
                ddlAnyo.ClearSelection()
                ddlAnyo.CurrentValue = ""
                ddlCommodity.ClearSelection()
                CargarProcesos()
            End If
        End If
    End Sub
    ''' <summary>
    ''' Evento que salta cuando se cambia el valor del combo de commodity de proceso
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ddlCommodity_ValueChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownValueChangedEventArgs) Handles ddlCommodity.ValueChanged
        If Not ddlCommodity.SelectedItem Is Nothing Then
            If ddlCommodity.CurrentValue <> ddlCommodity.SelectedItem.Text Then
                ddlCommodity.ClearSelection()
                ddlCommodity.CurrentValue = ""
                CargarProcesos()
            End If
        End If
    End Sub
    ''' <summary>
    ''' Evento que salta cuando se cambia el valor del combo de codigo de proceso
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ddlCodigo_ValueChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownValueChangedEventArgs) Handles ddlCodigo.ValueChanged
        If Not ddlCodigo.SelectedItem Is Nothing Then
            If ddlCodigo.CurrentValue <> ddlCodigo.SelectedItem.Text Then
                ddlCodigo.ClearSelection()
                ddlCodigo.CurrentValue = ""
                ddlDenominacion.ClearSelection()
                ddlDenominacion.CurrentValue = ""
            End If
        End If
        If Not ddlCodigo.Items.FindItemByValue(e.NewValue) Is Nothing Then
            Dim oItem As Infragistics.Web.UI.ListControls.DropDownItem = ddlCodigo.Items.FindItemByValue(e.NewValue)
            oItem.Selected = True
        Else
            Exit Sub
        End If
        Dim valor As String = ddlCodigo.Items(ddlCodigo.SelectedItemIndex).Value 'ddlCodigo.SelectedItem.Value.ToString
        ddlCodigo.CurrentValue = ddlCodigo.Items(ddlCodigo.SelectedItemIndex).Text
        ddlDenominacion.ClearSelection()
        For Each oItem In ddlDenominacion.Items
            If oItem.Value = valor Then
                oItem.Selected = True
                ddlDenominacion.CurrentValue = oItem.Text
                Exit For
            End If
        Next
    End Sub
    ''' <summary>
    ''' Evento que salta cuando se cambia el valor del combo de denominacion de proceso
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ddlDenominacion_ValueChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownValueChangedEventArgs) Handles ddlDenominacion.ValueChanged
        If Not ddlDenominacion.SelectedItem Is Nothing Then
            If ddlDenominacion.CurrentValue <> ddlDenominacion.SelectedItem.Text Then
                ddlDenominacion.ClearSelection()
                ddlCodigo.ClearSelection()
                ddlCodigo.CurrentValue = ""
                ddlDenominacion.CurrentValue = ""
            End If
        End If
    End Sub
    ''' <summary>
    ''' Recibe un numero y lo devuelve formateado segun configuracion del usuario y aparte quita los decimales si son 0
    ''' </summary>
    ''' <param name="sValor">Numero</param>
    ''' <returns>numero formateado</returns>
    ''' <remarks>Llamadas desde: uwgCertificados_InitializeRow ; Tiempo maximo:0 </remarks>
    Function QuitarDecimalSiTodoCeros(ByVal sValor As String) As String
        Dim decimales As String = ""
        If FSNUser.PrecisionFmt > 0 AndAlso Right(sValor, FSNUser.PrecisionFmt + 1) = FSNUser.DecimalFmt & decimales.PadRight(FSNUser.PrecisionFmt, "0") Then
            sValor = Left(sValor, Len(sValor) - FSNUser.PrecisionFmt - 1)
        End If
        QuitarDecimalSiTodoCeros = sValor
    End Function
#Region "Exportación"
    Private Sub EstablecerOpcionesUsuario()
        For Each item As Infragistics.Web.UI.GridControls.SortedColumnInfo In whgContratos.GridView.Behaviors.Sorting.SortedColumns
            With whgContratosExportacion
                .GridView.Behaviors.Sorting.SortedColumns.Add(item.ColumnKey, item.SortDirection)
            End With
        Next
        For Each item As Infragistics.Web.UI.GridControls.GroupedColumn In whgContratos.GroupingSettings.GroupedColumns
            whgContratosExportacion.GroupingSettings.GroupedColumns.Add(item)
        Next
        For Each item As Infragistics.Web.UI.GridControls.ColumnFilter In whgContratos.GridView.Behaviors.Filtering.ColumnFilters
            whgContratosExportacion.GridView.Behaviors.Filtering.ColumnFilters.Add(item)
        Next
    End Sub
    ''' <summary>
    ''' Exporta a Excel
    ''' </summary>
    ''' <remarks>Llamada desde: Page_load; Tiempo máximo:0,1</remarks>
    Private Sub ExportarExcel()
        EstablecerOpcionesUsuario()
        updGridContratos.Update()
        Dim fileName As String = HttpUtility.UrlEncode(FSNPageHeader.TituloCabecera)
        fileName = fileName.Replace("+", "%20")
        With whgContratosExportacion.GridView
            .Columns.FromKey("IMAGE_EST").Hidden = True
            .Columns.FromKey("INDICADOR_EN_PROCESO").Hidden = True
        End With
        With wdgExcelExporter
            .DownloadName = fileName
            .DataExportMode = DataExportMode.AllDataInDataSource
            .EnableStylesExport = False
            .WorkbookFormat = Infragistics.Documents.Excel.WorkbookFormat.Excel2007
            .Export(whgContratosExportacion)
        End With
    End Sub
    Private Sub wdgExcelExporter_Exported(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.ExcelExportedEventArgs) Handles wdgExcelExporter.Exported
        e.Worksheet.Name = Textos(8)
    End Sub
    Private Sub wdgExcelExporter_GridRecordItemExported(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.GridRecordItemExportedEventArgs) Handles wdgExcelExporter.GridRecordItemExported
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorContratos
        Select Case e.GridCell.Column.Key
            Case "ESTADO"
                If whgContratosExportacion.GridView.Columns.Item("EN_PROCESO") IsNot Nothing Then
                    If (e.GridCell.Row.DataItem.Item("EN_PROCESO") = 1 Or e.GridCell.Row.DataItem.Item("EN_PROCESO") = 2) Then
                        e.WorksheetCell.Value = Textos(88)
                    End If
                Else
                    Select Case e.GridCell.Row.DataItem.Item("CODESTADO")
                        Case EstadosVisorContratos.Guardado
                            e.WorksheetCell.Value = Textos(0)
                        Case EstadosVisorContratos.En_Curso_De_Aprobacion
                            e.WorksheetCell.Value = Textos(1)
                        Case EstadosVisorContratos.Vigentes
                            e.WorksheetCell.Value = Textos(2)
                        Case EstadosVisorContratos.Proximo_a_Expirar
                            e.WorksheetCell.Value = Textos(3)
                        Case EstadosVisorContratos.Expirados
                            e.WorksheetCell.Value = Textos(4)
                        Case EstadosVisorContratos.Rechazados
                            e.WorksheetCell.Value = Textos(5)
                        Case EstadosVisorContratos.Anulados
                            e.WorksheetCell.Value = Textos(6)
                    End Select
                End If
            Case Else
                Select Case e.GridCell.Column.Type
                    Case System.Type.GetType("System.Byte")
                        If e.WorksheetCell.Value IsNot DBNull.Value AndAlso Left(e.GridCell.Column.Key, 2) = "C_" Then
                            e.WorksheetCell.Value = IIf(e.GridCell.Value, Textos(92), Textos(93))
                        End If
                    Case System.Type.GetType("System.DateTime")
                        e.WorksheetCell.CellFormat.FormatString = FSNUser.DateFormat.ShortDatePattern
                    Case Else
                End Select
        End Select
    End Sub
    ''' <summary>
    ''' Exporta a PDF
    ''' </summary>
    ''' <remarks>Llamada desde: Page_load; Tiempo máximo:0,1</remarks>
    Private Sub ExportarPDF()
        EstablecerOpcionesUsuario()
        updGridContratos.Update()
        Dim fileName As String = HttpUtility.UrlEncode(FSNPageHeader.TituloCabecera)
        fileName = fileName.Replace("+", "%20")
        wdgPDFExporter.DownloadName = fileName
        With whgContratosExportacion.GridView
            .Columns.FromKey("IMAGE_EST").Hidden = True
            .Columns.FromKey("INDICADOR_EN_PROCESO").Hidden = True
        End With
        With wdgPDFExporter
            .EnableStylesExport = True

            .DataExportMode = DataExportMode.AllDataInDataSource
            .TargetPaperOrientation = PageOrientation.Landscape

            .Margins = PageMargins.GetPageMargins("Narrow")
            .TargetPaperSize = PageSizes.GetPageSize("A4")

            .Export(whgContratosExportacion)
        End With
    End Sub
    Private Sub wdgPDFExporter_GridRecordItemExporting(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.DocumentGridRecordItemExportingEventArgs) Handles wdgPDFExporter.GridRecordItemExporting
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorCertificados
        Select Case e.GridCell.Column.Key
            Case "PUBLICADA"
                e.ExportValue = IIf(Not IsDBNull(e.GridCell.Value) AndAlso e.GridCell.Value = 1, Textos(13), Textos(14))
            Case Else
                Select Case e.GridCell.Column.Type
                    Case System.Type.GetType("System.Byte")
                        If e.ExportValue IsNot DBNull.Value AndAlso Left(e.GridCell.Column.Key, 2) = "C_" Then
                            e.ExportValue = IIf(e.GridCell.Value, Textos(13), Textos(14))
                        End If
                    Case System.Type.GetType("System.DateTime")
                        If IsDBNull(e.ExportValue) Then
                            e.ExportValue = ""
                        Else
                            e.ExportValue = FormatDate(e.ExportValue, FSNUser.DateFormat)
                        End If
                    Case Else
                End Select
        End Select
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorContratos
        Select Case e.GridCell.Column.Key
            Case "ESTADO"
                If whgContratosExportacion.GridView.Columns.Item("EN_PROCESO") IsNot Nothing Then
                    If (e.GridCell.Row.DataItem.Item("EN_PROCESO") = 1 Or e.GridCell.Row.DataItem.Item("EN_PROCESO") = 2) Then
                        e.ExportValue = Textos(88)
                    End If
                Else
                    Select Case e.GridCell.Row.DataItem.Item("CODESTADO")
                        Case EstadosVisorContratos.Guardado
                            e.ExportValue = Textos(0)
                        Case EstadosVisorContratos.En_Curso_De_Aprobacion
                            e.ExportValue = Textos(1)
                        Case EstadosVisorContratos.Vigentes
                            e.ExportValue = Textos(2)
                        Case EstadosVisorContratos.Proximo_a_Expirar
                            e.ExportValue = Textos(3)
                        Case EstadosVisorContratos.Expirados
                            e.ExportValue = Textos(4)
                        Case EstadosVisorContratos.Rechazados
                            e.ExportValue = Textos(5)
                        Case EstadosVisorContratos.Anulados
                            e.ExportValue = Textos(6)
                    End Select
                End If
        End Select
    End Sub
    Private Sub whgContratosExportacion_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whgContratosExportacion.InitializeRow
        Dim gridCellIndex As Integer
        Dim items As Infragistics.Web.UI.GridControls.GridRecordItemCollection = e.Row.Items
        For Each item As Object In whgContratosExportacion.GridView.Columns
            If item.Type = System.Type.GetType("System.DateTime") Then
                gridCellIndex = whgContratosExportacion.GridView.Columns.FromKey(item.Key).Index
                CType(items(gridCellIndex).Column, Infragistics.Web.UI.GridControls.BoundDataField).DataFormatString = "{0:" & FSNUser.DateFormat.ShortDatePattern & "}"
            ElseIf item.Type = System.Type.GetType("System.Byte") _
            AndAlso Left(item.key, 2) = "C_" _
            AndAlso Not (item.hidden) _
            AndAlso e.Row.DataItem.Item.Row.Item(item.key) IsNot System.DBNull.Value Then
                If e.Row.DataItem.Item.Row.Item(item.key) Then
                    items.FindItemByKey(item.key).Text = Textos(92) ' "Sí"            
                Else
                    items.FindItemByKey(item.key).Text = Textos(93) '"No"
                End If
            End If
        Next
    End Sub
#End Region
#Region "Page Methods"
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Sub AprobarContratos(ByVal Contratos As Object)
        RealizarAccion(Contratos)
    End Sub
    <System.Web.Services.WebMethod(True), _
      System.Web.Script.Services.ScriptMethod()> _
    Public Shared Sub RechazarContratos(ByVal Contratos As Object)
        RealizarAccion(Contratos)
    End Sub
    Private Shared Sub RealizarAccion(ByVal contratos As Object)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Try
            Dim sInstanciasActualizarEnProceso As String = ""
            Dim sInstanciasValidar As String = ""
            Dim cInstancia As Instancia
            cInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))

            Dim dsXML As New DataSet
            dsXML.Tables.Add("SOLICITUD")
            Dim drSolicitud As DataRow
            With dsXML.Tables("SOLICITUD").Columns
                .Add("TIPO_PROCESAMIENTO_XML")
                .Add("TIPO_DE_SOLICITUD")
                .Add("COMPLETO")
                .Add("CODIGOUSUARIO")
                .Add("INSTANCIA")
                .Add("USUARIO")
                .Add("USUARIO_EMAIL")
                .Add("USUARIO_IDIOMA")
                .Add("ACCION")
                .Add("IDACCION")
                .Add("COMENTARIO")
                .Add("BLOQUE_ORIGEN")
                .Add("THOUSANFMT")
                .Add("DECIAMLFMT")
                .Add("PRECISIONFMT")
                .Add("DATEFMT")
                .Add("REFCULTURAL")
                .Add("TIPOEMAIL")
                .Add("IDTIEMPOPROC")
            End With

            For Each contrato As Object In contratos
                If Not String.IsNullOrEmpty(sInstanciasActualizarEnProceso) Then
                    sInstanciasActualizarEnProceso = sInstanciasActualizarEnProceso & ","
                End If
                If Not String.IsNullOrEmpty(sInstanciasValidar) Then
                    sInstanciasValidar = sInstanciasValidar & "#"
                End If

                sInstanciasActualizarEnProceso &= contrato("id")
                sInstanciasValidar &= contrato("id") & "|" & contrato("bloque") & "|" & contrato("accion") & "||" & TiposDeDatos.TipoDeSolicitud.Contrato
            Next

            If Not String.IsNullOrEmpty(sInstanciasActualizarEnProceso) Then
                Dim cInstancias As Instancias
                cInstancias = FSNServer.Get_Object(GetType(FSNServer.Instancias))
                cInstancias.Actualizar_En_Proceso2(sInstanciasActualizarEnProceso, FSNUser.CodPersona)

                Dim xmlName As String
                Dim lIDTiempoProc As Long
                For Each info As String In Split(sInstanciasValidar, "#")
                    cInstancia.ID = Split(info, "|")(0)
                    cInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, FSNUser.Cod, strToLong(Split(info, "|")(1)))
                    drSolicitud = dsXML.Tables("SOLICITUD").NewRow
                    With drSolicitud
                        .Item("TIPO_PROCESAMIENTO_XML") = CInt(TipoProcesamientoXML.FSNWeb_PM_AprobacionRechazoMultiple)
                        .Item("TIPO_DE_SOLICITUD") = CInt(TiposDeDatos.TipoDeSolicitud.Contrato)
                        .Item("COMPLETO") = 1
                        .Item("CODIGOUSUARIO") = FSNUser.Cod
                        .Item("INSTANCIA") = Split(info, "|")(0)
                        .Item("USUARIO") = FSNUser.CodPersona
                        .Item("USUARIO_EMAIL") = FSNUser.Email
                        .Item("USUARIO_IDIOMA") = FSNUser.IdiomaCod
                        .Item("ACCION") = ""
                        .Item("IDACCION") = Split(info, "|")(2)
                        .Item("COMENTARIO") = ""
                        .Item("BLOQUE_ORIGEN") = Split(info, "|")(1)
                        .Item("THOUSANFMT") = FSNUser.ThousanFmt
                        .Item("DECIAMLFMT") = FSNUser.DecimalFmt
                        .Item("PRECISIONFMT") = FSNUser.PrecisionFmt
                        .Item("DATEFMT") = FSNUser.DateFmt
                        .Item("REFCULTURAL") = FSNUser.Idioma.RefCultural
                        .Item("TIPOEMAIL") = FSNUser.TipoEmail
                        .Item("IDTIEMPOPROC") = lIDTiempoProc
                    End With
                    xmlName = FSNUser.Cod & "#" & Split(info, "|")(0) & "#" & Split(info, "|")(1)
                    dsXML.Tables("SOLICITUD").Rows.Add(drSolicitud)

                    cInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, iFecha:=TiempoProcesamiento.InicioDisco)
                    If ConfigurationManager.AppSettings("SERVIDOR_TRATAMIENTO_INSTANCIAS") = "0" Then
                        'Se hace con un StringWriter porque el método GetXml del dataset no incluye el esquema
                        Dim oSW As New System.IO.StringWriter()
                        dsXML.WriteXml(oSW, XmlWriteMode.WriteSchema)

                        Dim oSrvTrataInst As New FSNWebServiceXML.TratamientoInstancias
                        oSrvTrataInst.TransferirXMLEnEpera(oSW.ToString(), xmlName)
                    Else
                        dsXML.WriteXml(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & "#FSNWEB.xml", XmlWriteMode.WriteSchema)
                        If IO.File.Exists(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml") Then _
                            IO.File.Delete(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml")
                        FileSystem.Rename(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & "#FSNWEB.xml",
                                          ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml")
                    End If
                Next
                cInstancias = Nothing
            End If
        Catch ex As Exception
            HttpContext.Current.Session("AjaxException") = ex
            Throw ex
        End Try
    End Sub
#End Region
End Class