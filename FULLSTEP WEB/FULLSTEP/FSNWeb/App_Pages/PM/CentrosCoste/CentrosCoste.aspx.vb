﻿Public Partial Class CentrosCoste
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        IDCONTROL.Value = Request("IdControl")
        If Request("UON1") <> "" Then
            CentrosCoste.Valor = Request("UON1") & "#" & Request("UON2") & "#" & Request("UON3") & "#" & Request("UON4")
        Else
            CentrosCoste.Valor = Request("Valor")

        End If
        CentrosCoste.VerUON = Request("VerUON")

        CentrosCoste.VerBts = True
        If Not IsNothing(Request("VerBts")) Then
            CentrosCoste.VerBts = Request("VerBts")
        End If

        CentrosCoste.IdEmpresa = Request("IdEmpresa")

        If Request("desde") = "ControlPresupuestario" Then
            CentrosCoste.Desde = 1
        End If

    End Sub

End Class