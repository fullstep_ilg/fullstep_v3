﻿Public Partial Class Partidas
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        IDCONTROL.Value = Request("IdControl")
        IDCentroCoste.Value = Request("CCEntryId")
        Partidas.Valor = Request("Valor")
        Partidas.PRES5 = Request("PRES5")
        Partidas.CentroCoste = Request("CentroCoste")
        If Not Session("FSSMPargen") Is Nothing Then
            Dim configuracion As FSNServer.PargenSM
            configuracion = Session("FSSMPargen").Item(Partidas.PRES5)
            If Not configuracion Is Nothing Then
                Partidas.Plurianual = configuracion.Plurianual
            End If
        End If
    End Sub

    'Recojo los atributos de la UON seleccionada y sus valores por defecto
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function DeterminarSiEstaEnUon(ByVal Articulo As String, ByVal Centro As String) As Object
        Try
            Dim CentroTroceado As String() = Split(Centro, "#")

            Dim ListaUon As FSNServer.UnidadesOrg
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")

            ListaUon = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))
            ListaUon.CargarUnidadconArticulo(Articulo)

            If ListaUon.Data.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To ListaUon.Data.Tables(0).Rows.Count - 1
                    If DBNullToSomething(ListaUon.Data.Tables(0).Rows(i).Item("UON3")) <> Nothing AndAlso CentroTroceado.Length > 2 Then
                        If CentroTroceado(2) = ListaUon.Data.Tables(0).Rows(i).Item("UON3") _
                        AndAlso CentroTroceado(1) = ListaUon.Data.Tables(0).Rows(i).Item("UON2") _
                        AndAlso CentroTroceado(0) = ListaUon.Data.Tables(0).Rows(i).Item("UON1") Then
                            Return True
                        End If
                    ElseIf DBNullToSomething(ListaUon.Data.Tables(0).Rows(i).Item("UON2")) <> Nothing AndAlso CentroTroceado.Length > 1 Then
                        If CentroTroceado(1) = ListaUon.Data.Tables(0).Rows(i).Item("UON2") _
                        AndAlso CentroTroceado(0) = ListaUon.Data.Tables(0).Rows(i).Item("UON1") Then
                            Return True
                        End If
                    ElseIf DBNullToSomething(ListaUon.Data.Tables(0).Rows(i).Item("UON1")) <> Nothing Then
                        If CentroTroceado(0) = ListaUon.Data.Tables(0).Rows(i).Item("UON1") Then
                            Return True
                        End If
                    End If
                Next
            End If

            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class