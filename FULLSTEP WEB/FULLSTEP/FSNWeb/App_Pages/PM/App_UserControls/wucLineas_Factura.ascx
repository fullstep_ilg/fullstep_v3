﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="wucLineas_Factura.ascx.vb"
	Inherits="FSNUserControls.wucLineas_Factura" %>

		<ajx:CollapsiblePanelExtender ID="cpeLineas" runat="server" CollapseControlID="pnlLineas"
			ExpandControlID="pnlLineas" ImageControlID="imgExpandir0" SuppressPostBack="True"
			TargetControlID="pnlDetalleLineas" SkinID="FondoRojo">
		</ajx:CollapsiblePanelExtender>
		<asp:Panel ID="pnlLineas" runat="server" Style="cursor: pointer; width: 99%; height: 25px;">
			<table width="100%" style="background-color: #dAdAdA; border: 1px solid #000000;">
				<tr>
					<td width="95%" style="vertical-align: top">
						<asp:Label ID="lblTituloLineas" runat="server" CssClass="EtiquetaMediano"></asp:Label>
					</td>
					<td align="right">
						<asp:Image ID="imgExpandir0" runat="server" SkinID="Contraer" />
					</td>
				</tr>
			</table>
		</asp:Panel>
		<asp:Panel ID="pnlDetalleLineas" runat="server" Style="background-color: #FAFAFA;
			border: 1px solid #000000;" Width="99%">
            <div id="lnkNuevaDiscrepanciaMaster" style="cursor:pointer; float:right; margin:3px; display:none;">
					<asp:Image runat="server" ID="imgNuevaDiscrepancia"  style="vertical-align: middle;" />
					<asp:Label runat="server" ID="lblNuevaDiscrepancia" style="margin-left:3px;" Text="DIntroducir discrepancia"></asp:Label>
				</div>
                <div id="LeyendaOtrosGestores" style="float: left; margin: 3px; display:none;" runat="server">
                    <asp:Label runat="server" ID="lblLeyendaOtrosGestores" CssClass="Normal" style="margin-left:3px;color:red;" Text="D¡Existen líneas de otros gestores no visualizadas!"></asp:Label>
                </div>
            <asp:UpdatePanel ID="upLineas" runat="server" UpdateMode="Conditional">
	        <ContentTemplate>
			<div style="margin:5px;">
				<div style="float:left;">
					<ig:WebHierarchicalDataGrid ID="whdgLineas" runat="server" AutoGenerateColumns="false" 
						ShowHeader="true" EnableAjax="true" OnCellSelectionChanged="whdgLineas_CellSelectionChanged"
						EnableDataViewState="true" Width="100%" Height="460px">
						<Behaviors>
                            <ig:Activation Enabled ="true" ></ig:Activation>
							<ig:Selection Enabled="true" RowSelectType="Single" CellSelectType="Single" ColumnSelectType="None">
								<SelectionClientEvents CellSelectionChanging="whdgLineas_CellClick" />
							</ig:Selection>
							<ig:RowSelectors Enabled="true" EnableInheritance="true"></ig:RowSelectors>
							<ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
							<ig:ColumnMoving Enabled="false"></ig:ColumnMoving>		
						</Behaviors>
					</ig:WebHierarchicalDataGrid>
				</div>
				<asp:HiddenField ID="hid_FacturaUpdating" runat="server" />
				<asp:HiddenField ID="hid_UltimoNuevoIdImpuesto" runat="server" />
				<asp:HiddenField ID="hid_UltimoNuevoIdCoste" runat="server" />
				<asp:HiddenField ID="hid_UltimoNuevoIdDescuento" runat="server" />
				<asp:HiddenField ID="hid_CodLineaUpdating" runat="server" />
				<asp:HiddenField ID="hid_ArticuloUpdating" runat="server" />
                <asp:HiddenField ID="hid_LineasEliminadas" runat="server" />
			</div>
			<div style="height: auto; background-color: #dddddd; border: 1px solid #dddddd;">
				<table width="95%" cellpadding="4" cellspacing="0">
					<tr>
						<td width="85%" align="right" style="border-bottom: 1px solid #cccccc">
							<asp:Label ID="lblImporteBruto" runat="server" CssClass="Normal"></asp:Label>
						</td>
						<td width="10%" align="right" style="border-bottom: 1px solid #cccccc">
							<asp:Label ID="lblImporteBrutoDato" runat="server" CssClass="Normal"></asp:Label>
						</td>
					</tr>
					<tr>
						<td width="85%" align="right" style="border-bottom: 1px solid #cccccc">
							<asp:Label ID="lblImporteNeto" runat="server" CssClass="Normal"></asp:Label>
						</td>
						<td width="10%" align="right" style="border-bottom: 1px solid #cccccc">
							<asp:Label ID="lblImporteNetoDato" runat="server" CssClass="Normal"></asp:Label>
						</td>
					</tr>
					<tr>
						<td width="85%" align="right" style="border-bottom: 1px solid #cccccc">
							<asp:Label ID="lblImpuestos" runat="server" CssClass="Normal"></asp:Label>
						</td>
						<td width="10%" align="right" style="border-bottom: 1px solid #cccccc">
							<asp:Label ID="lblImpuestosDato" runat="server" CssClass="Normal"></asp:Label>
						</td>
					</tr>
					<tr>
						<td width="85%" align="right" style="border-bottom: 1px solid #cccccc">
							<asp:Label ID="lblImporteTotal" runat="server" CssClass="Etiqueta"></asp:Label>
						</td>
						<td width="10%" align="right" style="border-bottom: 1px solid #cccccc">
							<asp:Label ID="lblImporteTotalDato" runat="server" CssClass="Etiqueta"></asp:Label>
						</td>
					</tr>
				</table>
			</div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnAceptarConfirm" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
	</asp:Panel>
	
<fsn:FSNPanelInfo ID="FSNPanelDatosDetallePedido" runat="server" ServiceMethod="Obtener_DatosDetallePedido"
	TipoDetalle="5" BackColor="#FFFFFF">
</fsn:FSNPanelInfo>
<%-- Panel de detalle del Albaran --%>
<asp:Button ID="btnMostrarPanelDetalleAlbaran" runat="server" EnableViewState="false"
	Style="display: none" />
<asp:Panel ID="PnlDetalleAlbaran" runat="server" Width="850px" Style="padding: 10px;
	display: none; background-color: white; border: 1px solid #000000;">
	<table width="100%">
		<tr>
			<td align="right">
				<asp:ImageButton ID="imgCerrarDetalleAlbaran" runat="server" />
			</td>
		</tr>
	</table>
	<asp:UpdatePanel ID="UpPanelDetalleAlbaran" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<div>
				<table width="100%">
					<tr>
						<td width="5%">
							<img runat="server" id="imgDetalleAlbaran" alt="" />
						</td>
						<td>
							<asp:Label ID="lblDetalleAlbaran" runat="server" CssClass="EtiquetaMediano"></asp:Label>
						</td>
					</tr>
				</table>
				<br />
				<table width="100%" cellpadding="2">
					<tr>
						<td style="width:10%">
							<asp:Label ID="lblLitFechaRecepcion" runat="server" CssClass="Etiqueta"></asp:Label>
						</td>
						<td style="width:10%">
							<asp:Label ID="lblFechaRecepcion" runat="server" CssClass="Normal"></asp:Label>
						</td>
						<td style="width:13%">
							<asp:Label ID="lblLitNumeroRecepcionERP" runat="server" CssClass="Etiqueta"></asp:Label>
						</td>
						<td style="width:11%">
							<asp:Label ID="lblNumeroRecepcionERP" runat="server" CssClass="Normal"></asp:Label>
						</td>
						<td align="right" style="width:56%">
							<asp:LinkButton ID="lblLitEliminarAlbaran" runat="server" CssClass="Etiqueta" 
								Visible="false" Font-Underline="true">
							</asp:LinkButton>
                            <asp:HiddenField ID="hAlbaranAEliminar" runat="server" />
						</td>
					</tr>
				</table>
				<br />
				<fieldset>
					<legend align="left">
						<asp:Label ID="lblTituloDatosReceptor" runat="server" CssClass="Etiqueta"></asp:Label>
					</legend>
					<div style="margin-top: 10px">
						<table width="100%" cellpadding="5">
							<tr>
								<td>
									<asp:Label ID="lblLitCodReceptor" runat="server" CssClass="Etiqueta"></asp:Label>
								</td>
								<td>
									<asp:Label ID="lblCodReceptor" runat="server" CssClass="Normal"></asp:Label>
								</td>
								<td>
									<asp:Label ID="lblLitNombreReceptor" runat="server" CssClass="Etiqueta"></asp:Label>
								</td>
								<td>
									<asp:Label ID="lblNombreReceptor" runat="server" CssClass="Normal"></asp:Label>
								</td>
								<td>
									<asp:Label ID="lblLitCargo" runat="server" CssClass="Etiqueta"></asp:Label>
								</td>
								<td>
									<asp:Label ID="lblCargo" runat="server" CssClass="Normal"></asp:Label>
								</td>
								<td>
									<asp:Label ID="lblLitDepartamento" runat="server" CssClass="Etiqueta"></asp:Label>
								</td>
								<td>
									<asp:Label ID="lblDepartamento" runat="server" CssClass="Normal"></asp:Label>
								</td>
							</tr>
							<tr>
								<td>
									<asp:Label ID="lblLitEmail" runat="server" CssClass="Etiqueta"></asp:Label>
								</td>
								<td>
									<asp:Label ID="lblEmail" runat="server" CssClass="Normal"></asp:Label>
								</td>
								<td>
									<asp:Label ID="lblLitTelefono" runat="server" CssClass="Etiqueta"></asp:Label>
								</td>
								<td>
									<asp:Label ID="lblTelefono" runat="server" CssClass="Normal"></asp:Label>
								</td>
								<td>
									<asp:Label ID="lblLitFax" runat="server" CssClass="Etiqueta"></asp:Label>
								</td>
								<td>
									<asp:Label ID="lblFax" runat="server" CssClass="Normal"></asp:Label>
								</td>
								<td>
									<asp:Label ID="lblLitOrganizacion" runat="server" CssClass="Etiqueta"></asp:Label>
								</td>
								<td>
									<asp:Label ID="lblOrganizacion" runat="server" CssClass="Normal"></asp:Label>
								</td>
							</tr>
						</table>
					</div>
				</fieldset>
				<br />
				<asp:Panel ID="pnlCostesDescuentosAlbaran" runat="server" Visible="false">
					<table width="100%" cellpadding="6" cellspacing="0">
						<tr>
							<td colspan="2" style="border: 1px solid #cccccc; background-color: #eeeeee; vertical-align: middle;">
								<img runat="server" src="" id="imgCostesDescuentosAlbaran" alt="" style="margin-right: 10px" />
								<asp:Label ID="lblTituloCostesDescuentosAlbaran" runat="server" CssClass="EtiquetaMediano"></asp:Label>
							</td>
						</tr>
						<tr>
							<td width="50%" valign="top" class="ToplessBorder">
								<div>
									<asp:Label ID="lblTituloCostesAlbaran" runat="server" CssClass="EtiquetaMediano"></asp:Label>
								</div>
								<asp:Repeater ID="rpCostesAlbaran" runat="server">
									<HeaderTemplate>
										<table width="100%" cellspacing="0" cellpadding="3" class="tabla">
									</HeaderTemplate>
									<ItemTemplate>
										<tr>
											<td width="75%" class="costes">
												<%#Container.DataItem("DEN")%>
											</td>
											<td class="Right costes" width="5%">
												<%#Container.DataItem("OPERACION")%>
											</td>
											<td class="Right costes" width="10%" nowrap="nowrap">
												<asp:Label ID="lblValor" runat="server" CssClass="Normal"></asp:Label>
											</td>
											<td class="Right costes" width="10%" nowrap="nowrap">
												<asp:Label ID="lblImporte" runat="server" CssClass="Normal"></asp:Label>
											</td>
										</tr>
									</ItemTemplate>
									<FooterTemplate>
										<tr>
											<td colspan="4" class="Right" style="background-color: #cccccc">
												<asp:Label ID="lblTotal" runat="server" CssClass="Normal"></asp:Label>
											</td>
										</tr>
										</table>
									</FooterTemplate>
								</asp:Repeater>
								<div style="margin: 15px">
									<asp:Label ID="lblSinCostesAlbaran" runat="server" CssClass="Etiqueta" Visible="false"></asp:Label>
								</div>
							</td>
							<td width="50%" valign="top" class="costes">
								<div>
									<asp:Label ID="lblTituloDescuentosAlbaran" runat="server" CssClass="EtiquetaMediano"></asp:Label>
								</div>
								<asp:Repeater ID="rpDescuentosAlbaran" runat="server">
									<HeaderTemplate>
										<table width="100%" cellspacing="0" cellpadding="3" class="tabla">
									</HeaderTemplate>
									<ItemTemplate>
										<tr>
											<td width="75%" class="costes">
												<%#Container.DataItem("DEN")%>
											</td>
											<td class="Right costes" width="5%">
												<%#Container.DataItem("OPERACION")%>
											</td>
											<td class="Right costes" width="10%" nowrap="nowrap">
												<asp:Label ID="lblValor" runat="server" CssClass="Normal"></asp:Label>
											</td>
											<td class="Right costes" width="10%" nowrap="nowrap">
												<asp:Label ID="lblImporte" runat="server" CssClass="Normal"></asp:Label>
											</td>
										</tr>
									</ItemTemplate>
									<FooterTemplate>
										<tr>
											<td colspan="4" class="Right" style="background-color: #cccccc">
												<asp:Label ID="lblTotal" runat="server" CssClass="Normal"></asp:Label>
											</td>
										</tr>
										</table>
									</FooterTemplate>
								</asp:Repeater>
								<div style="margin: 15px">
									<asp:Label ID="lblSinDescuentosAlbaran" runat="server" CssClass="Etiqueta" Visible="false"></asp:Label>
								</div>
							</td>
						</tr>
					</table>
				</asp:Panel>
			</div>
		</ContentTemplate>
		<Triggers>
			<asp:AsyncPostBackTrigger ControlID="btnMostrarPanelDetalleAlbaran" EventName="Click" />
		</Triggers>
	</asp:UpdatePanel>
</asp:Panel>
<asp:Button runat="server" ID="BtnOcultoDetalleAlbaran" EnableViewState="false" Style="display: none" />
<ajx:ModalPopupExtender ID="mpePanelDetalleAlbaran" runat="server" PopupControlID="PnlDetalleAlbaran"
	CancelControlID="imgCerrarDetalleAlbaran" TargetControlID="BtnOcultoDetalleAlbaran">
</ajx:ModalPopupExtender>
<%-- Panel de las observaciones de la línea --%>
<asp:Panel ID="PnlObservaciones" runat="server" Width="450px"  Style="padding: 10px;
	display: none; background-color: white; border: 1px solid #000000;">
	<table width="100%">
	<tr><td><asp:Label ID="lblTituloObservaciones" runat="server" CssClass="EtiquetaMediano" Text="Observaciones"></asp:Label></td>
		<td align="right">
				<asp:ImageButton ID="imgCerrarObservaciones" runat="server" />
		</td>
	</tr>
	<tr><td colspan="2"><asp:Label ID="lblObservaciones" runat="server" CssClass="Normal" ClientIDMode="Static"></asp:Label>
						<asp:HiddenField ID="hSinObservaciones" runat="server" ClientIDMode="Static" Value="No hay observaciones." /></td></tr>
	</table>
</asp:Panel>
<asp:Button runat="server" ID="BtnOcultoObservaciones" EnableViewState="false" Style="display: none" />
<ajx:ModalPopupExtender ID="mpePanelObservaciones" runat="server" PopupControlID="PnlObservaciones"
	CancelControlID="imgCerrarObservaciones" TargetControlID="BtnOcultoObservaciones">
</ajx:ModalPopupExtender>
<%-- Panel de cálculo del importe --%>
<asp:Panel ID="PnlCalculoImporte" runat="server" Width="450px" Style="padding: 10px;
	display: none; background-color: white; border: 1px solid #000000;">
	<table width="100%">
		<tr>
			<td width="5%">
				<img runat="server" id="imgCalculoImporte" alt="" />
			</td>
			<td>
				<asp:Label ID="lblTituloCalculoImporte" runat="server" CssClass="EtiquetaMediano"></asp:Label>
			</td>
			<td align="right">
				<asp:ImageButton ID="imgCerrarCalculoImporte" runat="server" />
			</td>
		</tr>
		</table>
	<div style="min-height: 150px;">
		<table width="100%" cellpadding="4" border="0" cellspacing="0">
			<tr>
				<td width="40%" align="left">
					<asp:Label ID="lblCILitImporteBruto" runat="server" CssClass="Normal"></asp:Label>
				</td>
				<td width="60%" align="right">
					<asp:Label ID="lblCIImporteBruto" runat="server" CssClass="Normal" ClientIDMode="Static"></asp:Label>
				</td>
			</tr>
			<tr>
				<td align="left">
					<asp:Label ID="lblCILitTotalCostes" runat="server" CssClass="Normal"></asp:Label>
				</td>
				<td align="right">
					<asp:Label ID="lblCITotalCostes" runat="server" CssClass="Normal" ClientIDMode="Static"></asp:Label>
				</td>
			</tr>
			<tr>
				<td align="left">
					<asp:Label ID="lblCILitTotalDescuentos" runat="server" CssClass="Normal"></asp:Label>
				</td>
				<td align="right">
					<asp:Label ID="lblCITotalDescuentos" runat="server" CssClass="Normal" ClientIDMode="Static"></asp:Label>
				</td>
			</tr>
			<tr>
				<td align="left">
					<asp:Label ID="lblCILitImporteSinImpuestos" runat="server" CssClass="Normal"></asp:Label>
				</td>
				<td align="right">
					<asp:Label ID="lblCIImporteSinImpuestos" runat="server" CssClass="Normal" ClientIDMode="Static"></asp:Label>
				</td>
			</tr>
			<tr>
				<td align="left" style="background-color: #cccccc">
					<asp:Label ID="lblCILitImporteTotal" runat="server" CssClass="Etiqueta"></asp:Label>
				</td>
				<td align="right" style="background-color: #cccccc">
					<asp:Label ID="lblCIImporteTotal" runat="server" CssClass="Etiqueta" ClientIDMode="Static"></asp:Label>
				</td>
			</tr>
		</table>
	</div>
</asp:Panel>
<asp:Button runat="server" ID="BtnOcultoCalculoImporte" EnableViewState="false" Style="display: none" />
<ajx:ModalPopupExtender ID="mpePanelCalculoImporte" runat="server" PopupControlID="PnlCalculoImporte"
	CancelControlID="imgCerrarCalculoImporte" TargetControlID="BtnOcultoCalculoImporte">
</ajx:ModalPopupExtender>
<asp:Button ID="btnMostrarPanelDetalleImpuestos" runat="server" Style="display: none" />
<%-- Panel de detalle de los impuestos de las lineas de factura --%>
<asp:Panel ID="PanelDetalleImpuestos" runat="server" Width="650px" Style="padding: 10px;
	display: none; background-color: white; border: 1px solid #000000;">
    <asp:ImageButton ID="imgCerrarDetalleimpuestos" runat="server" OnClientClick="CerrarDetalleImpuestos();" style="float:right;vertical-align:top" />
    <span style="width:100%">
	<asp:UpdatePanel ID="upDetalleImpuestosLinea" runat="server" UpdateMode="Conditional">
		<Triggers>
			<asp:AsyncPostBackTrigger ControlID="btnMostrarPanelDetalleImpuestos" EventName="Click" />
		</Triggers>
		<ContentTemplate>
    	<table width="95%">
			<tr>
				<td style="width:5%">
					<img runat="server" id="imgDetalleImpuesto" alt="" />
				</td>
				<td style="width:13%; vertical-align :top ">
					<asp:Label ID="lblLiteralImpuestosDetalle" runat="server" CssClass="EtiquetaMediano"></asp:Label>
				</td>
				<td style="width:50%; vertical-align :top ">
					<asp:Label ID="lblDescripcionLinea" CssClass="EtiquetaMediano" runat="server"></asp:Label>
                </td>
                <td style="vertical-align:top ">
                    <asp:Label runat="server" ID="lblCalculoImporteLinea" CssClass="EtiquetaMediano"></asp:Label>
                </td>
			</tr>
		</table>
		<div id="divAgregarImpuesto" style="padding: 5px">
			<asp:Image runat="server" ID="imgAgregarImpuesto" Style="cursor: pointer" /><asp:Label Style="cursor: pointer"
				runat="server" ID="lblAgregarImpuesto" Font-Underline="true">
			</asp:Label>
		</div>
		<table width="100%" cellspacing="0">
		<asp:Repeater ID="rptImpuestosLineaFactura" runat="server">
			<HeaderTemplate>
			<tr>
				<td style="background-color: #eeeeee">
					<asp:Label ID="lblCodigoHeader" runat="server"></asp:Label>
				</td>
				<td style="background-color: #eeeeee">
					<asp:Label ID="lblDenominacionHeader" runat="server"></asp:Label>
				</td>
				<td style="background-color: #eeeeee">
					<asp:Label ID="lblImpRetenidoHeader" runat="server"></asp:Label>
				</td>
				<td align="right" style="background-color: #eeeeee;">
					<asp:Label ID="lblPorcentajeHeader" runat="server"></asp:Label>
				</td>
				<td align="right" style="background-color: #eeeeee">
					<asp:Label ID="lblImporteImpuestosHeader" runat="server"></asp:Label>
				</td>
				<td style="background-color: #eeeeee"></td>
				<td style="background-color: #eeeeee"></td>
				<td style="background-color: #eeeeee"></td>
			</tr>
			</HeaderTemplate>
			<ItemTemplate>
			<tr>
				<td>
					<asp:Label ID="lblCodigo" runat="server"></asp:Label>
					<asp:HiddenField ID="hid_idImpuesto" runat="server" />
					<asp:HiddenField ID="hid_Linea" runat="server" />
				</td>
				<td>
					<asp:Label ID="lblDenominacion" runat="server"></asp:Label>
				</td>
				<td>
					<asp:CheckBox ID="chkImpRetenido" runat="server" onclick="javascript: return false;" />
				</td>
				<td align="right">
					<igpck:webnumericeditor id="wnePorcentaje" runat="server" 
						autopostbackflags-valuechanged="On" onvaluechanged="PorcentajeImpuestosChange">
					</igpck:webnumericeditor>
					<asp:Label ID="lblPorcentaje" runat="server"></asp:Label>
				</td>
				<td align="right">
					<asp:Label ID="lblImporteImpuestos" runat="server"></asp:Label>
				</td>
				<td id="CeldaEliminarImpuesto" runat="server">
					<asp:ImageButton ID="imgEliminarImpuestoLinea" runat="server" CommandName="EliminarImpuestoLinea" />
				</td>
				<td id="CeldaAplicarImpuesto" runat="server">
					<asp:ImageButton ID="imgAplicarRestoLineas" runat="server" CommandName="AplicarRestoLineas" />
				</td>
				<td id="CeldaVerMotivoExencion" runat="server">
					<asp:ImageButton ID="imgVerMotivoExencion" runat="server" CommandName="VerMotivoExencion" />
					<asp:TextBox ID="txtMotivoExencion2" runat="server" TextMode="MultiLine" Visible="false"></asp:TextBox>
				</td>
			</tr>
			</ItemTemplate>
			<FooterTemplate>
            <tr>
			<td colspan="5" align="right" style="background-color: #eeeeee;">
				<asp:Label ID="lblTotalImpuestosFooter" runat="server"></asp:Label>
				<asp:Label ID="lblTotalImporteImpuestosFooter" runat="server"></asp:Label>
			</td>
			<td colspan="3" style="background-color: #eeeeee"></td>
			</tr>
			</FooterTemplate>
		</asp:Repeater>
        </table>
		</ContentTemplate>
	</asp:UpdatePanel>
</span>
</asp:Panel>
<asp:Button runat="server" ID="BtnOcultoDetalleImpuestos" EnableViewState="false"
	Style="display: none" />
<ajx:ModalPopupExtender ID="mpePanelDetalleImpuestos" runat="server" PopupControlID="PanelDetalleImpuestos"
	CancelControlID="imgCerrarDetalleimpuestos" TargetControlID="BtnOcultoDetalleImpuestos">
</ajx:ModalPopupExtender>
<%-- Panel de buscador de Impuestos --%>
<asp:Panel ID="PnlBuscadorImpuestos" runat="server" Width="650px" Style="padding: 10px;
	display: none; background-color: white; border: 1px solid #000000;">
	<table width="100%">
		<tr>
			<td width="5%">
				<img runat="server" id="imgImpuestosFacturas" alt="" />
			</td>
			<td>
				<asp:Label ID="lblBuscadorImpuestos" runat="server" CssClass="EtiquetaMediano"></asp:Label>
			</td>
			<td align="right">
				<asp:ImageButton ID="imgCerrarImpuestosFacturas" runat="server" />
			</td>
		</tr>
	</table>
	<table width="100%">
			<tr>
			<td colspan="2">
                <asp:UpdatePanel ID="upBuscadorImpuestos" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
	            <ContentTemplate>
				<table width="100%" border="0" style="border: 1px solid #cccccc; padding: 5px 10px 5px 10px">
					<tr>
						<td>
							<asp:Label ID="lblCodigo" runat="server" CssClass="Etiqueta"></asp:Label>
						</td>
						<td align="left">
							<asp:TextBox ID="txtCodigo" runat="server" Width="450px" MaxLength="200"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td>
							<asp:Label ID="lblDescripcion" runat="server" CssClass="Etiqueta"></asp:Label>
						</td>
						<td align="left">
							<asp:TextBox ID="txtDescripcion" runat="server" Width="450px" MaxLength="200"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td>
							<asp:Label ID="lblPais" runat="server" CssClass="Etiqueta"></asp:Label>
						</td>
						<td align="left">
						<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
						<td>
							<ig:WebDropDown ID="wddPaises" runat="server" Width="160px" EnableClosingDropDownOnSelect="true"
								DropDownAnimationDuration="1" DropDownContainerWidth="200" EnableDropDownAsChild="true">
								<ClientEvents ValueChanged="ValueChanged_wddPaises_CambioPais" />
							</ig:WebDropDown>
                        </td>
                        <td>
							<asp:Label ID="lblProvincia" runat="server" CssClass="Etiqueta" style="display:inline;"></asp:Label>
                        </td>
                        <td>
							<ig:WebDropDown ID="wddProvincias" runat="server" Width="160px" TextField="Text"
								ValueField="Value" EnableAutoCompleteFirstMatch="false" DropDownContainerWidth="200px"
								DropDownContainerHeight="200px" EnableMultipleSelection="false" EnableDropDownAsChild="true"
								EnableClosingDropDownOnSelect="true">
								<ClientEvents InputKeyDown="InputKeyDown_wwdProvincias_Inicializar" />
							</ig:WebDropDown>
						</td>
    					</tr>
                        </table>
						</td>
					</tr>
					<tr>
						<td>
							<asp:Label ID="lblMaterial" runat="server" CssClass="Etiqueta"></asp:Label>
						</td>
						<td>
							<table style="background-color: White; width: 450px; border: solid 1px #BBBBBB" 
								cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<asp:TextBox ID="txtMaterial" runat="server" Width="410px" BorderWidth="0px"></asp:TextBox>
									</td>
									<td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px">
										<asp:ImageButton ID="imgMaterialLupa" runat="server" SkinID="BuscadorLupa" />
									</td>
								</tr>
								<asp:HiddenField ID="hidMaterial" runat="server" />
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<asp:Label ID="lblArticulo" runat="server" CssClass="Etiqueta"></asp:Label>
						</td>
						<td>
							<table style="background-color: White; width: 450px; border: solid 1px #BBBBBB" cellpadding="0"
								cellspacing="0">
								<tr>
									<td>
										<asp:TextBox ID="txtArticulo" runat="server" Width="410px" BorderWidth="0px"></asp:TextBox>
									</td>
									<td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px">
										<asp:ImageButton ID="imgArticuloLupa" runat="server" SkinID="BuscadorLupa" />
									</td>
								</tr>
								<asp:HiddenField ID="hidArticulo" runat="server" />
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<asp:Label ID="lblConceptoAplicable" runat="server" CssClass="Etiqueta"></asp:Label>
						</td>
						<td align="left">
							<ig:WebDropDown ID="wddConceptoImpuestos" runat="server" Width="450px" EnableClosingDropDownOnSelect="true"
								DropDownAnimationDuration="1" DropDownContainerWidth="450" EnableDropDownAsChild="true">
							</ig:WebDropDown>
						</td>
					</tr>
					<tr>
						<td>
							<asp:CheckBox ID="chkImp_repercutido" runat="server" Checked="true" />
						</td>
						<td align="left">
							<asp:CheckBox ID="chkImp_retenido" runat="server" Checked="true" />
						</td>
					</tr>
					<tr>
                        <td colspan="2">
                        <table width="100%">
                            <tr>
						        <td style="width: 50%">
							        <fsn:FSNButton ID="btnBuscarImpuestos" runat="server" Alineacion="Right"></fsn:FSNButton>
						        </td>
						        <td style="width: 50%">
							        <fsn:FSNButton ID="btnLimpiarImpuestos" runat="server" Alineacion="Left"></fsn:FSNButton>
						        </td>
                            </tr>
                        </table>
                        </td>
					</tr>
				</table>
	            </ContentTemplate>
	            </asp:UpdatePanel>
                <asp:UpdatePanel ID="upWhdgImpuestos" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
	            <ContentTemplate>
				<ig:WebHierarchicalDataGrid ID="whdgImpuestos" Width="100%" Height="200px"
					runat="server" AutoGenerateColumns="false" ShowHeader="true" EnableAjax="true"
					EnableDataViewState="true" EnableViewState="true" ExpansionColumnCss="hideExpandArea">
					<Columns>
						<ig:BoundDataField Key="COD" DataFieldName="COD"></ig:BoundDataField>
						<ig:BoundDataField Key="DEN" DataFieldName="DEN"></ig:BoundDataField>
						<ig:BoundDataField Key="RETENIDO" DataFieldName="RETENIDO"></ig:BoundDataField>
						<ig:BoundDataField Key="PORCENTAJE" DataFieldName="PORCENTAJE"></ig:BoundDataField>
						<ig:BoundDataField Key="VALOR" DataFieldName="VALOR" Hidden="true"></ig:BoundDataField>
						<ig:BoundDataField Key="CONCEPTO" DataFieldName="CONCEPTO" Hidden="true"></ig:BoundDataField>
						<ig:BoundDataField Key="COMMENT" DataFieldName="COMMENT" Hidden="true"></ig:BoundDataField>
						<ig:BoundDataField Key="GRP_COMP" DataFieldName="GRP_COMP" Hidden="true"></ig:BoundDataField>
					</Columns>
					<Behaviors>
						<ig:Selection RowSelectType="Single" Enabled="True" CellClickAction="Row"></ig:Selection>
						<ig:Sorting Enabled="true" SortingMode="Multi"></ig:Sorting>
						<ig:RowSelectors Enabled="true" RowNumbering="false"></ig:RowSelectors>
					</Behaviors>
				</ig:WebHierarchicalDataGrid>
                <table width="100%">		
                <tr>
			        <td style="width: 50%">
				        <fsn:FSNButton ID="btnAceptarImpuesto" runat="server" Alineacion="Right" OnClientClick="return comprobarImpuestoSeleccionado();"></fsn:FSNButton>
			        </td>
			        <td style="width: 50%">
				        <fsn:FSNButton ID="btnCancelarImpuesto" runat="server" 
					    OnClientClick="return CerrarBuscadorImpuestos()" Alineacion="Left">
				        </fsn:FSNButton>
			        </td>
		        </tr>
                </table>
	            </ContentTemplate>
	            </asp:UpdatePanel>
		    </td>
	    </tr>
    </table>
    <asp:UpdatePanel ID="upAvisoErrorImpuesto" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
	<ContentTemplate>
    	<asp:Label ID="lblAvisoErrorImpuesto" runat="server" Visible="false"></asp:Label>
	</ContentTemplate>
	</asp:UpdatePanel>
</asp:Panel>
<asp:Button runat="server" ID="BtnOcultoImpuestosFacturas" EnableViewState="false"
	Style="display: none" />
<ajx:ModalPopupExtender ID="mpePanelBuscadorImpuestos" runat="server" PopupControlID="PnlBuscadorImpuestos"
	CancelControlID="imgCerrarImpuestosFacturas" TargetControlID="BtnOcultoImpuestosFacturas">
</ajx:ModalPopupExtender>
<%-- Panel de detalle de los costes de las lineas de factura --%>
<asp:Button ID="btnMostrarPanelDetalleCostes" runat="server" EnableViewState="false"
	Style="display: none" />
<asp:Panel ID="PanelDetalleCostes" runat="server" Width="650px" Style="padding: 10px;
	display: none; background-color: white; border: 1px solid #000000;">
	<asp:UpdatePanel ID="upDetalleCostesLinea" runat="server" UpdateMode="Conditional">
		<Triggers>
			<asp:AsyncPostBackTrigger ControlID="btnMostrarPanelDetalleCostes" EventName="Click" />
		</Triggers>
		<ContentTemplate>
			<table width="100%">
				<tr>
					<td style="width:5%;">
						<img runat="server" id="imgDetalleCoste" alt="" />
					</td>
					<td style="width:10%; vertical-align:top">
						<asp:Label ID="lblLiteralCostes" runat="server" CssClass="EtiquetaMediano"></asp:Label>
					</td>
					<td style="width:50%; vertical-align:top">
						<asp:Label ID="lblDescripcionLinea_Costes" CssClass="EtiquetaMediano" runat="server"></asp:Label>
					</td>
                    <td style="vertical-align:top">
                        <asp:Label ID="lblCalculoImporteLinea_Costes" CssClass="EtiquetaMediano" runat="server"></asp:Label>
                    </td>
					<td style="vertical-align:top" align="right">
						<asp:ImageButton ID="imgCerrarDetalleCostes" runat="server" OnClientClick="return CerrarDetalleCostes()" />
					</td>
				</tr>
			</table>
			<div id="divAgregarCoste" style="padding: 5px">
				<asp:Image runat="server" ID="imgAgregarCoste" Style="cursor: pointer" />
				<asp:Label Style="cursor: pointer" runat="server" ID="lblAgregarCoste" Font-Underline="true"></asp:Label>
			</div>
			<asp:Repeater ID="rptCostesLineaFactura" runat="server">
				<HeaderTemplate>
					<table width="100%" cellspacing="1" style="background: #DCDCDC;">
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td style="background-color: #eeeeee;">
							<asp:Label ID="lblDenominacionCosteLinea" runat="server"></asp:Label>
							<asp:HiddenField ID="hid_idCosteLinea" runat="server" />
							<asp:HiddenField ID="hid_Linea" runat="server" />
							<asp:HiddenField ID="hid_Albaran" runat="server" />
						</td>
						<td style="background-color: #eeeeee;">
							<asp:Label ID="lblCostePorLineaAlbaran" runat="server"></asp:Label>
						</td>
						<td style="background-color: #FFFFFF; width: 45px" runat="server" id="tdOperaciones">
							<asp:DropDownList ID="ddlOperaciones" runat="server" Width="100%" OnSelectedIndexChanged="ddlOperacionesCostes_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="+%" Text="+%"></asp:ListItem>
								<asp:ListItem Value="+" Text="+"></asp:ListItem>
							</asp:DropDownList>
							<asp:Label ID="lblOperaciones" runat="server"></asp:Label>
						</td>
						<td align="right" style="background-color: #FFFFFF; width: 90px" runat="server" id="tdPorcentaje">
							<igpck:webnumericeditor id="wnePorcentaje" runat="server" width="90px"
								autopostbackflags-valuechanged="On" onvaluechanged="PorcentajeCostesChange">
							</igpck:webnumericeditor>
							<asp:Label ID="lblPorcentaje" runat="server" Width="90px"></asp:Label>
						</td>
						<td align="right" style="background-color: #eeeeee; width: 120px">
							<asp:Label ID="lblImporteCostes" runat="server"></asp:Label>
						</td>
						<td runat="server" id="CeldaEliminarCoste" style="background-color: #FFFFFF;">
							<asp:ImageButton ID="imgEliminarCosteLinea" runat="server" CommandName="EliminarCosteLinea" />
						</td>
						<td runat="server" id="CeldaAplicarCoste" style="background-color: #FFFFFF;">
							<asp:ImageButton ID="imgAplicarCosteRestoLineas" runat="server" CommandName="AplicarCosteRestoLineas" />
						</td>
					</tr>
				</ItemTemplate>
				<FooterTemplate>
					<td colspan="5" align="right" style="background-color: #DCDCDC;">
						<asp:Label ID="lblTotalCostesFooter" runat="server"></asp:Label>
						<asp:Label ID="lblTotalImporteCostesFooter" runat="server"></asp:Label>
					</td>
					<td colspan="2" style="background-color: #DCDCDC;"></td>
					</table>
				</FooterTemplate>
			</asp:Repeater>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Panel>
<asp:Button runat="server" ID="BtnOcultoDetalleCostes" Style="display: none" />
<ajx:ModalPopupExtender ID="mpePanelDetalleCostes" runat="server" PopupControlID="PanelDetalleCostes"
	CancelControlID="imgCerrarDetalleCostes" TargetControlID="BtnOcultoDetalleCostes">
</ajx:ModalPopupExtender>
<%-- Panel de buscador de costes por linea de factura --%>
<asp:Panel ID="PnlBuscadorCostes" runat="server" Width="650px" Style="padding: 10px;
	display: none; background-color: white; border: 1px solid #000000;">
	<table width="100%">
		<tr>
			<td width="5%">
				<img runat="server" id="imgCostesFacturas" alt="" />
			</td>
			<td>
				<asp:Label ID="lblBuscadorCostes" runat="server" CssClass="EtiquetaMediano"></asp:Label>
			</td>
			<td align="right">
				<asp:ImageButton ID="imgCerrarCostesFacturas" runat="server" />
			</td>
		</tr>
	</table>
	<asp:UpdatePanel ID="upBusquedaCostes" UpdateMode="Conditional" runat="server">
		<ContentTemplate>
			<table width="100%">
				<tr>
					<td colspan="2">
						<asp:Label ID="lblTipoCoste_BusqCostesLinea" runat="server"></asp:Label>
                        <asp:Label ID="lblAlbaran_BusqCostesLinea" runat="server"></asp:Label>
					</td>
				</tr>
				<tr id="FilaTipoCostes" runat="server">
					<td style="width: 50%">
						<asp:RadioButton ID="rbCosteLinea" runat="server" GroupName="Costes" Checked="true" />
					</td>
					<td style="width: 50%">
						<asp:RadioButton ID="rbCosteAlbaran" runat="server" GroupName="Costes" />
					</td>
				</tr>
				<tr>
					<td style="width: 50%">
						<asp:Label ID="lblCostesSeleccionados" runat="server"></asp:Label>
					</td>
					<td style="width: 50%">
						<asp:DropDownList ID="ddlCostesFacturas" DataTextField="DEN" DataValueField="DEN"
							runat="server" Width="300px">
						</asp:DropDownList>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<asp:UpdatePanel ID="upBuscadorInternoCostes" UpdateMode="Conditional" runat="server">
						<ContentTemplate>
						<table width="90%" border="0" style="border: 1px solid #cccccc; padding: 5px 10px 5px 10px">
							<tr>
								<td>
									<asp:Label ID="lblCodigo_BusqCostesLinea" runat="server" CssClass="Etiqueta"></asp:Label>
								</td>
								<td align="left">
									<asp:TextBox ID="txtCodigo_BusqCostesLinea" runat="server" Width="250px" MaxLength="200"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td>
									<asp:Label ID="lblDescripcion_BusqCostesLinea" runat="server" CssClass="Etiqueta"></asp:Label>
								</td>
								<td align="left">
									<asp:TextBox ID="txtDescripcion_BusqCostesLinea" runat="server" Width="250px" MaxLength="200"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td>
									<asp:Label ID="lblFamMateriales_BusqCostesLinea" runat="server" CssClass="Etiqueta"></asp:Label>
								</td>
								<td>
									<table style="background-color: White; width: 150px; border: solid 1px #BBBBBB" cellpadding="0"
										cellspacing="0">
										<tr>
											<td>
												<asp:TextBox ID="txtMaterial_BusqCosteLinea" runat="server" Width="235px" BorderWidth="0px"></asp:TextBox>
											</td>
											<td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px">
												<asp:ImageButton ID="imgMaterialLupaCostes" runat="server" SkinID="BuscadorLupa" />
											</td>
										</tr>
										<asp:HiddenField ID="hidMaterialCostes" runat="server" />
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<asp:Label ID="lblArticulo_BusqCostesLinea" runat="server" CssClass="Etiqueta"></asp:Label>
								</td>
								<td>
									<table style="background-color: White; width: 150px; border: solid 1px #BBBBBB" cellpadding="0"
										cellspacing="0">
										<tr>
											<td>
												<asp:TextBox ID="txtArticulo_BusqCosteLinea" runat="server" Width="235px" BorderWidth="0px"></asp:TextBox>
											</td>
											<td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px">
												<asp:ImageButton ID="imgArticuloLupaCostes" runat="server" SkinID="BuscadorLupa" />
											</td>
										</tr>
										<asp:HiddenField ID="hidArticuloCostes" runat="server" />
									</table>
								</td>
							</tr>
							<tr>
								<td style="width: 50%">
									<fsn:FSNButton ID="btnBuscarCostes" runat="server" Alineacion="Right"></fsn:FSNButton>
								</td>
								<td style="width: 50%">
									<fsn:FSNButton ID="btnLimpiarCostes" runat="server" Alineacion="Left"></fsn:FSNButton>
								</td>
							</tr>
						</table>
						<ig:WebHierarchicalDataGrid ID="whdgBuscadorCostes" Width="100%" Height="200px" runat="server"
							AutoGenerateColumns="false" ShowHeader="true" EnableAjax="true" EnableDataViewState="true" ExpansionColumnCss="hideExpandArea">
							<Columns>
								<ig:BoundDataField Key="COD" DataFieldName="COD">
								</ig:BoundDataField>
								<ig:BoundDataField Key="DEN" DataFieldName="DEN">
								</ig:BoundDataField>
								<ig:BoundDataField Key="ID" DataFieldName="ID" Hidden="true">
								</ig:BoundDataField>
							</Columns>
							<Behaviors>
								<ig:Selection RowSelectType="Single" Enabled="True" CellClickAction="Row">
									<SelectionClientEvents RowSelectionChanged="WebDataGrid_RowSelectionChanged" />
								</ig:Selection>
								<ig:Sorting Enabled="true" SortingMode="Multi">
								</ig:Sorting>
								<ig:RowSelectors Enabled="true" RowNumbering="false">
								</ig:RowSelectors>
							</Behaviors>
						</ig:WebHierarchicalDataGrid>
						<asp:PlaceHolder ID="phCosteGenerico" Visible="false" runat="server">
							<asp:Label ID="lblSinResultado" runat="server" CssClass="Etiqueta"></asp:Label><br />
							<asp:Label ID="lblLitCosteGenerico" runat="server" CssClass="Etiqueta"></asp:Label><br />
							<asp:TextBox ID="txtCosteGenerico" runat="server" Width="250px" MaxLength="200"></asp:TextBox>
						</asp:PlaceHolder>
						</ContentTemplate>
						</asp:UpdatePanel>
					</td>
				</tr>
				<tr>
					<td>
						<fsn:FSNButton ID="btnAceptarCoste" runat="server" Alineacion="Right"></fsn:FSNButton>
					</td>
					<td>
						<fsn:FSNButton ID="btnCancelarCoste" runat="server" OnClientClick="return CerrarBuscadorCostes()"
							Alineacion="Left"></fsn:FSNButton>
					</td>
				</tr>
			</table>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Panel>
<asp:Button runat="server" ID="BtnOcultoCostesFacturas" Style="display: none" EnableViewState="False" />
<ajx:ModalPopupExtender ID="mpePanelBuscadorCostes" runat="server" PopupControlID="PnlBuscadorCostes"
	CancelControlID="imgCerrarCostesFacturas" TargetControlID="BtnOcultoCostesFacturas">
</ajx:ModalPopupExtender>
<%-- Panel de detalle de los descuentos de las lineas de factura --%>
<asp:Button ID="btnMostrarPanelDetalleDescuentos" runat="server" EnableViewState="false"
	Style="display: none" />
<asp:Panel ID="PanelDetalleDescuentos" runat="server" Width="650px" Style="padding: 10px;
	display: none; background-color: white; border: 1px solid #000000;">
	<asp:UpdatePanel ID="upDetalleDescuentosLinea" runat="server" UpdateMode="Conditional">
		<Triggers>
			<asp:AsyncPostBackTrigger ControlID="btnMostrarPanelDetalleDescuentos" EventName="Click" />
		</Triggers>
		<ContentTemplate>
			<table width="100%">
				<tr>
					<td width="5%">
						<img runat="server" id="imgDetalleDescuento" alt="" />
					</td>
					<td style="width:10%; vertical-align :top">
						<asp:Label ID="lblLiteralDescuentos" runat="server" CssClass="EtiquetaMediano"></asp:Label>
					</td>
					<td style="width:50%; vertical-align :top">
						<asp:Label ID="lblDescripcionLinea_Descuentos" CssClass="EtiquetaMediano" runat="server"></asp:Label>
					</td>
                    <td style="vertical-align :top">
                        <asp:Label ID="lblCalculoImporteLinea_Descuentos" CssClass="EtiquetaMediano" runat="server"></asp:Label>
                    </td>
					<td style="vertical-align:top" align="right">
						<asp:ImageButton ID="imgCerrarDetalleDescuentos" OnClientClick="return CerrarDetalleDescuentos()"
							runat="server" />
					</td>
				</tr>
			</table>
			<div id="divAgregarDescuento" style="padding: 5px">
				<asp:Image runat="server" ID="imgAgregarDescuento" Style="cursor: pointer" /><asp:Label Style="cursor: pointer"
					runat="server" ID="lblAgregarDescuento" Font-Underline="true"></asp:Label>
			</div>
			<asp:Repeater ID="rptDescuentosLineaFactura" runat="server">
				<HeaderTemplate>
					<table width="100%" cellspacing="1" style="background: #DCDCDC;">
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td style="background-color: #eeeeee;">
							<asp:Label ID="lblDenominacionDescuentoLinea" runat="server"></asp:Label>
							<asp:HiddenField ID="hid_idDescuentoLinea" runat="server" />
							<asp:HiddenField ID="hid_Linea" runat="server" />
							<asp:HiddenField ID="hid_Albaran" runat="server" />
						</td>
						<td style="background-color: #eeeeee;">
							<asp:Label ID="lblDescuentoPorLineaAlbaran" runat="server"></asp:Label>
						</td>
						<td style="background-color: #FFFFFF; width: 45px" runat="server" id="tdOperaciones">
							<asp:DropDownList ID="ddlOperaciones" runat="server" Width="100%" OnSelectedIndexChanged="ddlOperacionesDescuentos_SelectedIndexChanged" AutoPostBack="true">
								<asp:ListItem Value="-%" Text="-%"></asp:ListItem>
								<asp:ListItem Value="-" Text="-"></asp:ListItem>
							</asp:DropDownList>
							<asp:Label ID="lblOperaciones" runat="server"></asp:Label>
						</td>
						<td align="right" style="background-color: #FFFFFF; width: 90px" runat="server" id="tdPorcentaje">
							<igpck:webnumericeditor id="wnePorcentaje" runat="server" width="100%" autopostbackflags-valuechanged="On"
								onvaluechanged="PorcentajeDescuentosChange"></igpck:webnumericeditor>
							<asp:Label ID="lblPorcentaje" runat="server" Width="100%"></asp:Label>
						</td>
						<td align="right" style="background-color: #eeeeee; width: 120px">
							<asp:Label ID="lblImporteDescuentos" runat="server"></asp:Label>
						</td>
						<td runat="server" id="CeldaEliminarDescuento" style="background-color: #FFFFFF;">
							<asp:ImageButton ID="imgEliminarDescuentoLinea" runat="server" CommandName="EliminarDescuentoLinea" />
						</td>
						<td runat="server" id="CeldaAplicarDescuento" style="background-color: #FFFFFF;">
							<asp:ImageButton ID="imgAplicarDescuentoRestoLineas" runat="server" CommandName="AplicarDescuentoRestoLineas" />
						</td>
					</tr>
				</ItemTemplate>
				<FooterTemplate>
					<td colspan="5" align="right" style="background-color: #DCDCDC;">
						<asp:Label ID="lblTotalDescuentosFooter" runat="server"></asp:Label><asp:Label ID="lblTotalImporteDescuentosFooter"
							runat="server"></asp:Label>
					</td>
					<td colspan="2" style="background-color: #DCDCDC;">
					</td>
					</table>
				</FooterTemplate>
			</asp:Repeater>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Panel>
<asp:Button runat="server" ID="BtnOcultoDetalleDescuentos" Style="display: none" />
<ajx:ModalPopupExtender ID="mpePanelDetalleDescuentos" runat="server" PopupControlID="PanelDetalleDescuentos"
	CancelControlID="imgCerrarDetalleDescuentos" TargetControlID="BtnOcultoDetalleDescuentos">
</ajx:ModalPopupExtender>
<%-- Panel de buscador de descuentos por linea de factura --%>
<asp:Panel ID="PnlBuscadorDescuentos" runat="server" Width="650px" Style="padding: 10px;
	display: none; background-color: white; border: 1px solid #000000;">
	<table width="100%">
		<tr>
			<td width="5%">
				<img runat="server" id="imgDescuentosFacturas" alt="" />
			</td>
			<td>
				<asp:Label ID="lblBuscadorDescuentos" runat="server" CssClass="EtiquetaMediano"></asp:Label>
			</td>
			<td align="right">
				<asp:ImageButton ID="imgCerrarDescuentosFacturas" runat="server" />
			</td>
		</tr>
	</table>
	<asp:UpdatePanel ID="upBusquedaDescuentos" UpdateMode="Conditional" runat="server">
		<ContentTemplate>
			<table width="100%">
				<tr>
					<td colspan="2">
						<asp:Label ID="lblTipoDescuento_BusqDescuentosLinea" runat="server"></asp:Label>
                        <asp:Label ID="lblAlbaran_BusqDescuentosLinea" runat="server"></asp:Label>
					</td>
				</tr>
				<tr id="FilaTipoDescuento" runat="server">
					<td style="width: 50%">
						<asp:RadioButton ID="rbDescuentoLinea" runat="server" GroupName="Descuentos" Checked="true" />
					</td>
					<td style="width: 50%">
						<asp:RadioButton ID="rbDescuentoAlbaran" runat="server" GroupName="Descuentos" />
					</td>
				</tr>
				<tr>
					<td style="width: 50%">
						<asp:Label ID="lblDescuentosSeleccionados" runat="server"></asp:Label>
					</td>
					<td style="width: 50%">
						<asp:DropDownList ID="ddlDescuentosFacturas" DataTextField="DEN" DataValueField="DEN"
							runat="server" Width="300px">
						</asp:DropDownList>
					</td>
				</tr>
				<tr>
					<td colspan="2">
					<asp:UpdatePanel ID="upBuscadorInternoDescuentos" UpdateMode="Conditional" runat="server">
						<ContentTemplate>
						<table width="90%" border="0" style="border: 1px solid #cccccc; padding: 5px 10px 5px 10px">
							<tr>
								<td>
									<asp:Label ID="lblCodigo_BusqDescuentosLinea" runat="server" CssClass="Etiqueta"></asp:Label>
								</td>
								<td align="left">
									<asp:TextBox ID="txtCodigo_BusqDescuentosLinea" runat="server" Width="250px" MaxLength="200"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td>
									<asp:Label ID="lblDescripcion_BusqDescuentosLinea" runat="server" CssClass="Etiqueta"></asp:Label>
								</td>
								<td align="left">
									<asp:TextBox ID="txtDescripcion_BusqDescuentosLinea" runat="server" Width="250px"
										MaxLength="200"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td>
									<asp:Label ID="lblFamMateriales_BusqDescuentosLinea" runat="server" CssClass="Etiqueta"></asp:Label>
								</td>
								<td>
									<table style="background-color: White; width: 150px; border: solid 1px #BBBBBB" cellpadding="0"
										cellspacing="0">
										<tr>
											<td>
												<asp:TextBox ID="txtMaterial_BusqDescuentosLinea" runat="server" Width="235px" BorderWidth="0px"></asp:TextBox>
											</td>
											<td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px">
												<asp:ImageButton ID="imgMaterialLupaDescuentos" runat="server" SkinID="BuscadorLupa" />
											</td>
										</tr>
										<asp:HiddenField ID="hidMaterialDescuentos" runat="server" />
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<asp:Label ID="lblArticulo_BusqDescuentosLinea" runat="server" CssClass="Etiqueta"></asp:Label>
								</td>
								<td>
									<table style="background-color: White; width: 150px; border: solid 1px #BBBBBB" cellpadding="0"
										cellspacing="0">
										<tr>
											<td>
												<asp:TextBox ID="txtArticulo_BusqDescuentosLinea" runat="server" Width="235px" BorderWidth="0px"></asp:TextBox>
											</td>
											<td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px">
												<asp:ImageButton ID="imgArticuloLupaDescuentos" runat="server" SkinID="BuscadorLupa" />
											</td>
										</tr>
										<asp:HiddenField ID="hidArticuloDescuentos" runat="server" />
									</table>
								</td>
							</tr>
							<tr>
								<td style="width: 50%">
									<fsn:FSNButton ID="btnBuscarDescuentos" runat="server" Alineacion="Right"></fsn:FSNButton>
								</td>
								<td style="width: 50%">
									<fsn:FSNButton ID="btnLimpiarDescuentos" runat="server" Alineacion="Left"></fsn:FSNButton>
								</td>
							</tr>
						</table>
						<ig:WebHierarchicalDataGrid ID="whdgBuscadorDescuentos" Width="100%" Height="200px"
							runat="server" AutoGenerateColumns="false" ShowHeader="true" EnableAjax="true"
							EnableDataViewState="true" ExpansionColumnCss="hideExpandArea">
							<Columns>
								<ig:BoundDataField Key="COD" DataFieldName="COD">
								</ig:BoundDataField>
								<ig:BoundDataField Key="DEN" DataFieldName="DEN">
								</ig:BoundDataField>
								<ig:BoundDataField Key="ID" DataFieldName="ID" Hidden="true">
								</ig:BoundDataField>
							</Columns>
							<Behaviors>
								<ig:Selection RowSelectType="Single" Enabled="True" CellClickAction="Row">
									<SelectionClientEvents RowSelectionChanged="WebDataGrid_RowSelectionChanged" />
								</ig:Selection>
								<ig:Sorting Enabled="true" SortingMode="Multi">
								</ig:Sorting>
								<ig:RowSelectors Enabled="true" RowNumbering="false">
								</ig:RowSelectors>
							</Behaviors>
						</ig:WebHierarchicalDataGrid>
						<asp:PlaceHolder ID="phDescuentoGenerico" Visible="false" runat="server">
							<asp:Label ID="lblSinResultadoDescuentos" runat="server" CssClass="Etiqueta"></asp:Label><br />
							<asp:Label ID="lblLitDescuentoGenerico" runat="server" CssClass="Etiqueta"></asp:Label><br />
							<asp:TextBox ID="txtDescuentoGenerico" runat="server" Width="250px" MaxLength="200"></asp:TextBox>
						</asp:PlaceHolder>
						</ContentTemplate>
					</asp:UpdatePanel>
					</td>
				</tr>
				<tr>
					<td>
						<fsn:FSNButton ID="btnAceptarDescuento" runat="server" Alineacion="Right"></fsn:FSNButton>
					</td>
					<td>
						<fsn:FSNButton ID="btnCancelarDescuento" runat="server" OnClientClick="return CerrarBuscadorDescuentos()"
							Alineacion="Left"></fsn:FSNButton>
					</td>
				</tr>
			</table>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Panel>
<asp:Button runat="server" ID="BtnOcultoDescuentosFacturas" Style="display: none"
	EnableViewState="False" />
<ajx:ModalPopupExtender ID="mpePanelBuscadorDescuentos" runat="server" PopupControlID="PnlBuscadorDescuentos"
	CancelControlID="imgCerrarDescuentosFacturas" TargetControlID="BtnOcultoDescuentosFacturas">
</ajx:ModalPopupExtender>
<%-- Panel de motivo de exencion de un impuesto --%>
<asp:Panel ID="pnlMotivoImpuestoExento" runat="server" Width="550px" Style="padding: 10px;
	display: none; background-color: white; border: 1px solid #000000;">
	<table width="100%">
		<tr>
			<td width="5%">
				<img runat="server" id="imgMotivoImpuestoExento" alt="" />
			</td>
			<td>
				<asp:Label ID="lblMotivoImpuestoExento" runat="server" CssClass="EtiquetaMediano"></asp:Label>
			</td>
			<td align="right">
				<asp:ImageButton ID="imgCerrarPanelMotivoImpuestoExento" runat="server" />
			</td>
		</tr>
	</table>
	<asp:UpdatePanel ID="upMotivoExecionImpuesto" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<div id="DivContenedorTitulo" style="width: 100%; height: auto">
				<div>
					<asp:Label ID="lblImpuestoSeleccionado" runat="server"></asp:Label></div>
				<div>
					<asp:Label ID="lblDescImpuestoExento" runat="server"></asp:Label></div>
			</div>
			<br />
			<div>
				<asp:Label ID="lblMotivo" runat="server"></asp:Label>
			</div>
			<div id="divTextAreaMotivo" style="width: 100%; height: auto">
				<asp:TextBox ID="txtMotivoExencion" runat="server" TextMode="MultiLine" Rows="12"
					Width="100%"></asp:TextBox>
			</div>
			<table style="width: 100%">
				<tr>
					<td style="width: 49%">
						<fsn:FSNButton ID="btnAceptarMotivImpExento" runat="server" Alineacion="Right"></fsn:FSNButton>
					</td>
					<td style="width: 49%"><fsn:FSNButton ID="btnCancelarMotivImpExento" runat="server" OnClientClick="return CerrarMotivoImpuestoExento()"  Alineacion="Left" ></fsn:FSNButton>
					</td>
				</tr>
			</table>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Panel>
<asp:Button runat="server" ID="btnOcultarPanelMotivoExencion" EnableViewState="false"
	Style="display: none" />
<ajx:ModalPopupExtender ID="mpePnlMotivoImpuestoExento" runat="server" PopupControlID="pnlMotivoImpuestoExento"
	CancelControlID="imgCerrarPanelMotivoImpuestoExento" TargetControlID="btnOcultarPanelMotivoExencion">
</ajx:ModalPopupExtender>
<ajx:CollapsiblePanelExtender ID="cpeResumenImpuestos" runat="server" CollapseControlID="pnlResumenImpuestos"
	ExpandControlID="pnlResumenImpuestos" ImageControlID="imgExpandirResumenImpuestos"
	SuppressPostBack="True" TargetControlID="pnlDetalleResumenImpuestos" SkinID="FondoRojo">
</ajx:CollapsiblePanelExtender>
<br />
<asp:Panel ID="pnlResumenImpuestos" runat="server" Style="cursor: pointer; width: 99%;
	height: 25px;">
	<table width="100%" style="background-color: #dAdAdA; border: 1px solid #000000;">
		<tr>
			<td width="95%" style="vertical-align: top">
				<asp:Label ID="lblResumenImpuestos" runat="server" CssClass="EtiquetaMediano"></asp:Label>
			</td>
			<td align="right">
				<asp:Image ID="imgExpandirResumenImpuestos" runat="server" SkinID="Contraer" />
			</td>
		</tr>
	</table>
</asp:Panel>
<%-- Panel de detalle de resumen de impuestos repercutidos y retenidos --%>
<asp:Panel ID="pnlDetalleResumenImpuestos" runat="server">
	<asp:UpdatePanel ID="upResumentImpuestoRepercutidos" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<asp:Repeater ID="rpImpuestosRepercutidos" runat="server">
				<HeaderTemplate>
					<br />
					<table width="80%" cellspacing="0" cellpadding="3" class="tabla">
						<tr>
							<td colspan="5" class="costes" style="background-color: #eeeeee">
								<span id="lblImpReper" runat="server">Impuestos repercutidos</span>
							</td>
						</tr>
						<tr>
							<td class="costes" style="background-color: #eeeeee">
								<span id="lblCodImpReper" runat="server">Código</span>
							</td>
							<td class="costes" style="background-color: #eeeeee">
								<span id="lblDenImpReper" runat="server">Denominación</span>
							</td>
							<td class="costes" style="background-color: #eeeeee">
								<span id="lblComenImpReper" runat="server">Comentario</span>
							</td>
							<td class="costes" style="background-color: #eeeeee">
								%
							</td>
							<td class="costes" style="background-color: #eeeeee">
								<span id="lblImporteImpReper" runat="server">Importe impuestos</span>
							</td>
						</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td width="10%" class="costes">
							<%#Container.DataItem("COD")%>
						</td>
						<td width="60%" class="costes">
							<%#Container.DataItem("DEN")%>
						</td>
						<td width="60%" class="costes">
							<%#Container.DataItem("COMMENT")%>
						</td>
						<td class="Right costes" width="15%">
							<asp:Label ID="lblValor" runat="server" CssClass="Normal"></asp:Label>
						</td>
						<td class="Right costes" width="15%">
							<asp:Label ID="lblImporte" runat="server" CssClass="Normal"></asp:Label>
						</td>
					</tr>
				</ItemTemplate>
				<FooterTemplate>
					<tr>
						<td colspan="5" class="Right" style="background-color: #cccccc">
							<asp:Label ID="lblTotal" runat="server" CssClass="Normal"></asp:Label>
						</td>
					</tr>
					</table>
				</FooterTemplate>
			</asp:Repeater>
		</ContentTemplate>
	</asp:UpdatePanel>
	<br />
	<asp:UpdatePanel ID="upResumenImpuestosRetenidos" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<asp:Repeater ID="rpImpuestosRetenidos" runat="server">
				<HeaderTemplate>
					<table width="80%" cellspacing="0" cellpadding="3" class="tabla">
						<tr>
							<td colspan="5" class="costes" style="background-color: #eeeeee">
								<span id="lblImpReten" runat="server">Impuestos retenidos</span>
							</td>
						</tr>
						<tr>
							<td class="costes" style="background-color: #eeeeee">
								<span id="lblCodImpReten" runat="server">Código</span>
							</td>
							<td class="costes" style="background-color: #eeeeee">
								<span id="lblDenImpReten" runat="server">Denominación</span>
							</td>
							<td class="costes" style="background-color: #eeeeee">
								<span id="lblComImpReten" runat="server">Comentario</span>
							</td>
							<td class="costes" style="background-color: #eeeeee">
								%
							</td>
							<td class="costes" style="background-color: #eeeeee">
								<span id="lblImporteImpReten" runat="server">Importe impuestos</span>
							</td>
						</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td width="10%" class="costes">
							<%#Container.DataItem("COD")%>
						</td>
						<td width="60%" class="costes">
							<%#Container.DataItem("DEN")%>
						</td>
						<td width="60%" class="costes">
							<%#Container.DataItem("COMMENT")%>
						</td>
						<td class="Right costes" width="15%">
							<asp:Label ID="lblValor" runat="server" CssClass="Normal"></asp:Label>
						</td>
						<td class="Right costes" width="15%">
							<asp:Label ID="lblImporte" runat="server" CssClass="Normal"></asp:Label>
						</td>
					</tr>
				</ItemTemplate>
				<FooterTemplate>
					<tr>
						<td colspan="5" class="Right" style="background-color: #cccccc">
							<asp:Label ID="lblTotal" runat="server" CssClass="Normal"></asp:Label>
						</td>
					</tr>
					</table>
				</FooterTemplate>
			</asp:Repeater>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Panel>
<%-- Panel de confirmación--%>
<asp:Panel ID="pnlConfirm" runat="server" BackColor="White" BorderColor="DimGray"
BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Center" Style="display: none;
min-width:300px;padding:8px;" Width="350px" ClientIDMode="Static">
    <table cellpadding="0" cellspacing="20" border="0">
        <tr>
            <td align="left">
                <asp:Image ID="imgConfirm" runat="server" ClientIDMode="Static" />
            </td>
			<td colspan="2" align="left">
                <asp:Label ID="lblConfirm" runat="server" Text="Label" EnableViewState="true" ClientIDMode="Static"></asp:Label>
            </td>
			<td align="right" style="vertical-align:top;">
				<asp:Image ID="imgCerrarConfirm" runat="server" ClientIDMode="Static" OnClick="CerrarPnl('pnlConfirm'); return false;" style="cursor:pointer;" />
			</td>
        </tr>
        <tr style="width:300px;">
            <td colspan="2" align="right" style="width:175px;"><fsn:FSNButton ID="btnAceptarConfirm" runat="server" Text="FSNButton" OnClientClick="CerrarPnl('pnlConfirm');"></fsn:FSNButton></td>
            <td colspan="2" align="left" style="width:175px;"><fsn:FSNButton ID="btnCancelarConfirm" runat="server" Text="FSNButton" Alineacion="Left" OnClientClick="CerrarPnl('pnlConfirm');return false;" ClientIDMode="Static"></fsn:FSNButton></td>
        </tr>
    </table>
</asp:Panel>
<script type="text/javascript">
    var click = 0
    var rowSelected;
    //evento que salta cuando se pincha en cualquier celda del grid de lineas de factura
    function whdgLineas_CellClick(sender, e) {
        var column = e.getNewSelectedCells().getCell(0).get_column();
        rowSelected = e.getNewSelectedCells().getCell(0).get_row();

        switch (column.get_key()) {
            case "ALBARAN":
                if (rowSelected.get_cellByColumnKey("ALBARAN").get_text() != '') {
                    var idLineaFactura = rowSelected.get_cellByColumnKey("LINEA").get_text();
                    GuardarNumFacturaUpdating(idLineaFactura)

                    var btnMostrarPanel = document.getElementById("<%= btnMostrarPanelDetalleAlbaran.clientID %>")
                    btnMostrarPanel.click()
                }
                e.set_cancel(true);
                break;
            case "PEDIDO":
                FSNMostrarPanel('<%=FSNPanelDatosDetallePedido.AnimationClientID%>', event, '<%=FSNPanelDatosDetallePedido.DynamicPopulateClientID%>', e.getNewSelectedCells().getCell(0).get_value() + "#" + e.getNewSelectedCells().getCell(0).get_row().get_cellByColumnKey("LINEA").get_text());
                e.set_cancel(true);
                break;
            case "IMPORTE":
                var row = e.getNewSelectedCells().getCell(0).get_row();
                $get('lblCIImporteBruto').innerHTML = row.get_cellByColumnKey("PREC_UP").get_text() + " * " + row.get_cellByColumnKey("CANT_PED").get_text() + " = " + row.get_cellByColumnKey("PREC_UP_POR_CANT_FORMATEADO").get_value()
                $get('lblCITotalCostes').innerHTML = row.get_cellByColumnKey("TOTAL_COSTES_FORMATEADO").get_text()
                $get('lblCITotalDescuentos').innerHTML = row.get_cellByColumnKey("TOTAL_DCTOS_FORMATEADO").get_text()
                $get('lblCIImporteSinImpuestos').innerHTML = row.get_cellByColumnKey("IMPORTE_SIN_IMP_FORMATEADO").get_text()
                $get('lblCIImporteTotal').innerHTML = row.get_cellByColumnKey("IMPORTE").get_text()
                $find('<%=mpePanelCalculoImporte.ClientID%>').show()
                e.set_cancel(true);
                break;
            case "OBS":
                var row = e.getNewSelectedCells().getCell(0).get_row();
                if (row.get_cellByColumnKey("OBS").get_value() == "") {
                    $get('lblObservaciones').innerHTML = $get('hSinObservaciones').value
                } else {
                    $get('lblObservaciones').innerHTML = row.get_cellByColumnKey("OBS").get_value()
                }
                $find('<%=mpePanelObservaciones.ClientID%>').show();
                e.set_cancel(true);
                break;
            case "TIENE_DISCREP":
                var idLineaFactura = rowSelected.get_cellByColumnKey("LINEA").get_value();
                var element = ($('#imgDiscrepanciasCerradas_' + idLineaFactura).is(':visible') ? $('#imgDiscrepanciasCerradas_' + idLineaFactura) : $('#imgDiscrepancias_' + idLineaFactura));
                var posX = element.offset().left + element.outerWidth();
                var posY = element.offset().top + element.outerHeight();
                if ($('#imgDiscrepanciasCerradas_' + idLineaFactura).is(':visible') || $('#imgDiscrepancias_' + idLineaFactura).is(':visible')) {
                    var idFactura = rowSelected.get_cellByColumnKey("FACTURA").get_value();
                    var numFactura = NumFactura;
                    MostrarMenuDiscrepanciasLinea(idFactura, numFactura, idLineaFactura, posX, posY);
                }
                e.set_cancel(true);
                break;
            default:
                e.set_cancel(true);
                break;
        }
    }


    function borrarAlbaran(lFactura, sAlbaran, sMensajeConfirmacion) {
        if (confirm(sMensajeConfirmacion)) {
            var hAlbaranAEliminar = document.getElementById("<%= hAlbaranAEliminar.clientID %>")
            if (hAlbaranAEliminar)
                hAlbaranAEliminar.value = sAlbaran
            return true;
        } else {
            return false;
        }
    }

    //funcion que salta cuando se cambia el combo de pais, borrando la provincia seleccionada si la hubiese
    function ValueChanged_wddPaises_CambioPais(elem, event) {
        comboProvincias = $find("<%= wddProvincias.clientID %>")
        if (comboProvincias)
            comboProvincias.set_currentValue("", true)
    }
    function InputKeyDown_wwdProvincias_Inicializar(elem, event) {
        comboProvincias = $find("<%= wddProvincias.clientID %>")
        if (comboProvincias)
            comboProvincias.set_currentValue("", true)
    }
    //funcion que abre el buscador de impuestos
    function AbrirBuscadorImpuestos() {
        $find("<%= mpePanelBuscadorImpuestos.clientID %>").show()
    }
    //funcion que cierra el buscador de impuestos para lineas
    function CerrarBuscadorImpuestos() {
        $find("<%= mpePanelBuscadorImpuestos.clientID %>").hide()
        var olblAvisoErrorImpuesto = $get("<%= lblAvisoErrorImpuesto.clientID %>");
        if(olblAvisoErrorImpuesto)
            olblAvisoErrorImpuesto.style.display = "none";
        return false;
    }
    //funcion que abre el buscador de costes para lineas
    function AbrirBuscadorCostes() {
        $find("<%= mpePanelBuscadorCostes.clientID %>").show()
    }
    //funcion que cierra el buscador de costes para lineas
    function CerrarBuscadorCostes() {
        $find("<%= mpePanelBuscadorCostes.clientID %>").hide()
        return false;
    }
    //funcion que abre el buscador de Descuentos para lineas
    function AbrirBuscadorDescuentos() {
        $find("<%= mpePanelBuscadorDescuentos.clientID %>").show()
    }
    //funcion que cierra el buscador de Descuentos para lineas
    function CerrarBuscadorDescuentos() {
        $find("<%= mpePanelBuscadorDescuentos.clientID %>").hide()
        return false;
    }
    //funcion que cierra el detalle de costes de lineas
    function CerrarDetalleCostes() {
        $find("<%= mpePanelDetalleCostes.clientID %>").hide()
        return false;
    }
    //funcion que cierra el detalle de descuentos de lineas
    function CerrarDetalleDescuentos() {
        $find("<%= mpePanelDetalleDescuentos.clientID %>").hide()
        return false;
    }
    //funcion que cierra el detalle de impuestos de lineas
    function CerrarDetalleImpuestos() {
        $find("<%= mpePanelDetalleImpuestos.clientID %>").hide()
        return false;
    }

    function CerrarMotivoImpuestoExento() {
        $find("<%= mpePnlMotivoImpuestoExento.clientID %>").hide()
        return false;
    }

    //funcion que desde la llamada del grid de lineas de factura abre el buscador o el detalle de costes de lineas de factura
    function AbrirPanelCostesLinea() {
        var idLineaFactura = rowSelected.get_cellByColumnKey("LINEA").get_text()
        var sCostes = rowSelected.get_cellByColumnKey("TOTAL_COSTES").get_value()

        //Ponemos en el radiobutton de linea del buscador de costes la linea que se ha pinchado
        var rbCosteLinea = document.getElementById("<%= rbCosteLinea.clientID %>")
        if (rbCosteLinea) {
            var CodLineaFactura = rowSelected.get_cellByColumnKey("LINEA_GRID").get_text()
            rbCosteLinea.parentNode.innerHTML = rbCosteLinea.parentNode.innerHTML + CodLineaFactura
        }

        //Ponemos en el radiobutton de albaran del buscador de costes el albaran de la linea que se ha pinchado
        var rbCosteAlbaran = document.getElementById("<%= rbCosteAlbaran.clientID %>")
        var AlbaranFactura = rowSelected.get_cellByColumnKey("ALBARAN").get_text()
        if (rbCosteAlbaran) {
            rbCosteAlbaran.parentNode.innerHTML = rbCosteAlbaran.parentNode.innerHTML + AlbaranFactura
        } else {
            var LitCosteAlbaran = document.getElementById("<%= lblAlbaran_BusqCostesLinea.clientID %>")
            if (LitCosteAlbaran)
                LitCosteAlbaran.innerHTML = AlbaranFactura
        }

        //Guardamos en hiddens datos de la linea que hemos pinchado
        var DenArticulo = rowSelected.get_cellByColumnKey("ART_DEN").get_text()
        var hidCodLinea = document.getElementById("<%= hid_CodLineaUpdating.clientID %>")
        hidCodLinea.value = CodLineaFactura
        var hidArticulo = document.getElementById("<%= hid_ArticuloUpdating.clientID %>")
        hidArticulo.value = DenArticulo


        GuardarNumFacturaUpdating(idLineaFactura)
        //Recogemos si tiene algun coste la linea para saber si tenemos que mostrar el buscador o
        //el panel con los costes que ya tiene metidos la linea de factura
        if (sCostes == 0)
            $find("<%= mpePanelBuscadorCostes.clientID %>").show()
        else {
            var btnMostrarPanel = document.getElementById("<%= btnMostrarPanelDetalleCostes.clientID %>")
            btnMostrarPanel.click()
        }
    }
    //funcion que desde la llamada del grid de lineas de factura abre el buscador o el detalle de descuentos de lineas de factura
    function AbrirPanelDescuentosLinea() {
        var idLineaFactura = rowSelected.get_cellByColumnKey("LINEA").get_text()
        var sDescuentos = rowSelected.get_cellByColumnKey("TOTAL_DCTOS").get_value()

        //Ponemos en el radiobutton de linea del buscador de Descuentos la linea que se ha pinchado
        var rbDescuentoLinea = document.getElementById("<%= rbDescuentoLinea.clientID %>")
        if (rbDescuentoLinea) {
            var CodLineaFactura = rowSelected.get_cellByColumnKey("LINEA_GRID").get_text()
            rbDescuentoLinea.parentNode.innerHTML = rbDescuentoLinea.parentNode.innerHTML + CodLineaFactura
        }

        //Ponemos en el radiobutton de albaran del buscador de Descuentos el albaran de la linea que se ha pinchado
        var rbDescuentoAlbaran = document.getElementById("<%= rbDescuentoAlbaran.clientID %>")
        var AlbaranFactura = rowSelected.get_cellByColumnKey("ALBARAN").get_text()
        if (rbDescuentoAlbaran) {
            rbDescuentoAlbaran.parentNode.innerHTML = rbDescuentoAlbaran.parentNode.innerHTML + AlbaranFactura
        } else {
            var LitDescuentoAlbaran = document.getElementById("<%= lblAlbaran_BusqDescuentosLinea.clientID %>")
            if (LitDescuentoAlbaran)
                LitDescuentoAlbaran.innerHTML = AlbaranFactura
        }

        //Guardamos en hiddens datos de la linea que hemos pinchado
        var DenArticulo = rowSelected.get_cellByColumnKey("ART_DEN").get_text()
        var hidCodLinea = document.getElementById("<%= hid_CodLineaUpdating.clientID %>")
        hidCodLinea.value = CodLineaFactura
        var hidArticulo = document.getElementById("<%= hid_ArticuloUpdating.clientID %>")
        hidArticulo.value = DenArticulo


        GuardarNumFacturaUpdating(idLineaFactura)
        //Recogemos si tiene algun Descuento la linea para saber si tenemos que mostrar el buscador o
        //el panel con los Descuentos que ya tiene metidos la linea de factura
        if (sDescuentos == 0)
            $find("<%= mpePanelBuscadorDescuentos.clientID %>").show()
        else {
            var btnMostrarPanel = document.getElementById("<%= btnMostrarPanelDetalleDescuentos.clientID %>")
            btnMostrarPanel.click()
        }
    }

    function AbrirAlbaran() {
        //var btnMostrarPanel = document.getElementById("<%= btnMostrarPanelDetalleImpuestos.clientID %>")
        //btnMostrarPanel.click()
    }

    //funcion que desde la llamada del grid de lineas de factura abre el buscador o el detalle de impuestos de lineas de factura
    function AbrirPanelImpuestos(impuestos) {
        var idLineaFactura = rowSelected.get_cellByColumnKey("LINEA").get_text()

        GuardarNumFacturaUpdating(idLineaFactura)
        //para saber si tenemos que mostrar el buscador o
        //el panel con los impuestos que ya tiene metidos la linea de factura
        if (impuestos == 0)
            $find("<%= mpePanelBuscadorImpuestos.clientID %>").show()
        else {
            var btnMostrarPanel = document.getElementById("<%= btnMostrarPanelDetalleImpuestos.clientID %>")
            btnMostrarPanel.click()
        }
    }

    //Funcion que guarda en el hidden el numero de linea de la factura que se esta modificando
    //numFactura: numero de linea de factura que se modifica en el grid de lineas de factura
    function GuardarNumFacturaUpdating(idLineaFactura) {
        var oHid_FacturaUpdating = document.getElementById("<%= hid_FacturaUpdating.clientID %>")

        //Guardamos la linea de Factura que podemos modificar para meterle un impuesto
        oHid_FacturaUpdating.value = idLineaFactura
    }
    /*  Descripcion: Elimina el contenido del campo articulo del buscador de impuestos, al pulsar eliminar. En caso contrario solo lectura
    Parametros entrada:
    elem:la caja de texto
    event:Evento pulsado.
    Llamada desde: Evento que salta al pulsar la tecla de <-, suprimir
    Tiempo ejecucion:=0seg. */
    function ControlArticuloImpuestos(elem, event) {
        //tanto si escribe algo como si pulsa la tecla de <- o suprimir, limpiamos lo que hay en el hidden
        oIdArticulo = document.getElementById('<%= hidArticulo.clientID %>');
        if (oIdArticulo)
            oIdArticulo.value = "";
        if (event.keyCode == 8 || event.keyCode == 46) {  //quitamos el espacio || event.keyCode == 32) {
            elem.value = ""
            return false;
        } else
            return true;
    }
    /*  Descripcion: Elimina el contenido del campo articulo del buscador de costes, al pulsar eliminar. En caso contrario solo lectura
    Parametros entrada:
    elem:la caja de texto
    event:Evento pulsado.
    Llamada desde: Evento que salta al pulsar la tecla de <-, suprimir
    Tiempo ejecucion:=0seg. */
    function ControlArticuloCostes(elem, event) {
        //tanto si escribe algo como si pulsa la tecla de <- o suprimir, limpiamos lo que hay en el hidden
        oIdArticulo = document.getElementById('<%= hidArticuloCostes.clientID %>');
        if (oIdArticulo)
            oIdArticulo.value = "";
        if (event.keyCode == 8 || event.keyCode == 46) {  //quitamos el espacio || event.keyCode == 32) {
            elem.value = ""
            return false;
        } else
            return true;
    }
    /*  Descripcion: Elimina el contenido del campo articulo del buscador de descuentos, al pulsar eliminar. En caso contrario solo lectura
    Parametros entrada:
    elem:la caja de texto
    event:Evento pulsado.
    Llamada desde: Evento que salta al pulsar la tecla de <-, suprimir
    Tiempo ejecucion:=0seg. */
    function ControlArticuloDescuentos(elem, event) {
        //tanto si escribe algo como si pulsa la tecla de <- o suprimir, limpiamos lo que hay en el hidden
        oIdArticulo = document.getElementById('<%= hidArticuloDescuentos.clientID %>');
        if (oIdArticulo)
            oIdArticulo.value = "";
        if (event.keyCode == 8 || event.keyCode == 46) {  //quitamos el espacio || event.keyCode == 32) {
            elem.value = ""
            return false;
        } else
            return true;
    }
    //CerrarPanel
    function CerrarPnl(panelID) {
        $("#" + panelID).hide();
    }
    //Función para centrar
    (function ($) {
        $.fn.extend({
            center: function () {
                return this.each(function () {
                    var otop = Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px";
                    var oleft = Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px";
                    $(this).css({ position: 'absolute', margin: 0, top: otop, left: oleft });
                    $(this).css('z-index', '220000');
                });
            }
        });
    })(jQuery);
    //Centramos el panel cuyo id pasamos
    function centrarPnl(panelID) {
        $('#' + panelID).center();
    }
    /*Comprueba que se ha seleccionado un impuesto en el grid. Si no es así, devuelve false (y detiene el postback)*/
    function comprobarImpuestoSeleccionado(){
        var oWhdImpuestos = $find("<%=whdgImpuestos.clientID %>");
        if (oWhdImpuestos.get_gridView().get_behaviors().get_selection().get_selectedRowsCollections().length <= 0) {
            return false;
        } else {
            $find("<%= mpePanelBuscadorImpuestos.clientID %>").hide();
        }
    }
</script>
