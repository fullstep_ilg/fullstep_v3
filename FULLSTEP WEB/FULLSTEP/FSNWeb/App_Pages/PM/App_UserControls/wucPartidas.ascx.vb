﻿Public Partial Class wucPartidas
    Inherits System.Web.UI.UserControl

    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
    "<script language=""{0}"">{1}</script>"

    Private _sValor As String
    Private _sPRES5 As String
    Private _sCentroCoste As String
    Private _bPlurianual As Boolean

    Public Property Valor() As String
        Get
            Valor = _sValor
        End Get
        Set(ByVal Value As String)
            _sValor = Value
        End Set
    End Property

    Public Property PRES5() As String
        Get
            PRES5 = _sPRES5
        End Get
        Set(ByVal Value As String)
            _sPRES5 = Value
        End Set
    End Property

    Public Property CentroCoste() As String
        Get
            CentroCoste = _sCentroCoste
        End Get
        Set(ByVal Value As String)
            _sCentroCoste = Value
        End Set
    End Property

    Public Property Plurianual() As Boolean
        Get
            Return _bPlurianual
        End Get
        Set(ByVal Value As Boolean)
            _bPlurianual = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CType(Page, FSNPage).ModuloIdioma = TiposDeDatos.ModulosIdiomas.Partidas

        Me.lblTitulo.Text = CType(Page, FSNPage).Textos(8) & " " & Request("Titulo")
        Me.lblBuscar.Text = CType(Page, FSNPage).Textos(0)
        Me.lblCentro.Text = CType(Page, FSNPage).Textos(1)
        Me.lblFechaInicioDesde.Text = CType(Page, FSNPage).Textos(2)
        Me.lblFechaHastaFin.Text = CType(Page, FSNPage).Textos(3)
        Me.btnBuscar.Text = CType(Page, FSNPage).Textos(4)
        Me.chkPartidasNoVigentes.Text = CType(Page, FSNPage).Textos(5)
        Me.btnSeleccionar.Text = CType(Page, FSNPage).Textos(6)
        Me.btnCancelar.Text = CType(Page, FSNPage).Textos(7)

        If Not IsPostBack() Then
            'Para el autocompletar
            Session("PRES5") = Request("PRES5")
            Session("CentroCoste") = Request("CentroCoste")

            If _sCentroCoste <> Nothing Then
                Me.txtCentro.Visible = False
                Me.hCentroCod.Value = _sCentroCoste
                Me.imgBuscarCentro.Visible = False
                Me.lblCentroCosteDen.Visible = True
                Me.lblCentroCosteDen.Text = Server.HtmlEncode(Request("CentroCosteDen"))
            Else
                Me.txtCentro.Visible = True
                Me.imgBuscarCentro.Visible = True
                Me.lblCentroCosteDen.Visible = False
            End If

            dteFechaInicioDesde.NullText = ""
            dteFechaHastaFin.NullText = ""

            Me.imgBuscarCentro.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/lupa.gif"
            Me.imgSiguiente.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/flecha_siguiente.gif"

            CargarArbolCentrosPartidas()

            If _bPlurianual Then
                Me.lblFechaInicioDesde.Visible = False
                Me.lblFechaHastaFin.Visible = False
                Me.chkPartidasNoVigentes.Checked = True
                Me.chkPartidasNoVigentes.Visible = False
                Me.dteFechaInicioDesde.Visible = False
                Me.dteFechaHastaFin.Visible = False
            Else
                Me.lblFechaInicioDesde.Visible = True
                Me.lblFechaHastaFin.Visible = True
                Me.chkPartidasNoVigentes.Checked = False
                Me.chkPartidasNoVigentes.Visible = True
                Me.dteFechaInicioDesde.Visible = True
                Me.dteFechaHastaFin.Visible = True
            End If

            Dim sClientTexts As String
            sClientTexts = ""
            sClientTexts += "var arrTextosML = new Array();"
            sClientTexts += "arrTextosML[0] = '" + JSText("Debe seleccionar un contrato") + "';"
            sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)


            txtBuscar.Attributes.Add("onchange", "FindClicked('" & txtBuscar.ClientID & "','" & Me.wdtCentrosPartidas.ClientID & "')")
            txtBuscar.Attributes.Add("onkeyup", "FindClicked('" & txtBuscar.ClientID & "','" & wdtCentrosPartidas.ClientID & "')")

            imgSiguiente.Attributes.Add("onclick", "FindNextClicked('" & txtBuscar.ClientID & "','" & wdtCentrosPartidas.ClientID & "');return false;")
            imgSiguiente.ToolTip = CType(Page, FSNPage).Textos(13)
        End If
    End Sub

    ''' <summary>
    ''' Carga el Arbol
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load     btnBuscar_Click ; Tiempo máximo: 0,3</remarks>
    Private Sub CargarArbolCentrosPartidas()
        Dim cPartidas As Fullstep.FSNServer.PRES5

        cPartidas = Session("FSN_Server").Get_Object(GetType(FSNServer.PRES5))

        wdtCentrosPartidas.DataSource = TodasRelaciones(cPartidas.Partidas_LoadData(_sPRES5, CType(Page, FSNPage).Usuario.Cod, CType(Page, FSNPage).Idioma, _bPlurianual, hCentroCod.Value, Not chkPartidasNoVigentes.Checked, dteFechaInicioDesde.Value, dteFechaHastaFin.Value, CType(Page, FSNPage).Usuario.DateFmt))

        Me.wdtCentrosPartidas.DataBind()

        For Each node As Infragistics.Web.UI.NavigationControls.DataTreeNode In wdtCentrosPartidas.AllNodes
            If node.Nodes.Count > 0 Then
                node.Expanded = True
            End If
        Next
    End Sub

    Dim ds As DataSet

    Private Sub wdtCentrosPartidas_NodeBound(sender As Object, e As Infragistics.Web.UI.NavigationControls.DataTreeNodeEventArgs) Handles wdtCentrosPartidas.NodeBound
        Dim iNivelNodo As Integer
        'Dim Tipo As 
        Dim oRows() As DataRow = Nothing
        Dim Datanode As Infragistics.Web.UI.Framework.Data.DataSetNode
        ds = sender.datasource

        Datanode = e.Node.DataItem

        If Datanode.Type = "Table" Then
            iNivelNodo = 0
        Else
            iNivelNodo = Right(Datanode.Type, 1)
        End If
        Select Case iNivelNodo
            Case "0"
                e.Node.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/world.gif"
            Case "5", "6", "7", "8"
                e.Node.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/candado.jpg"
            Case Else
                e.Node.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/carpetaNaranja.gif"
        End Select

        Select Case iNivelNodo
            Case "1"
                oRows = ds.Tables(1).Select("COD= '" & e.Node.Key & "'")
            Case "2"
                oRows = ds.Tables(iNivelNodo).Select("UON1= '" & e.Node.ParentNode.Key & "' AND COD= '" & e.Node.Key & "'")
            Case "3"
                oRows = ds.Tables(iNivelNodo).Select("UON1= '" & e.Node.ParentNode.ParentNode.Key & "' AND UON2= '" & e.Node.ParentNode.Key & "' AND COD= '" & e.Node.Key & "'")
            Case "4"
                oRows = ds.Tables(iNivelNodo).Select("UON1= '" & e.Node.ParentNode.ParentNode.ParentNode.Key & "' AND UON2= '" & e.Node.ParentNode.ParentNode.Key & "' AND UON3= '" & e.Node.ParentNode.Key & "' AND COD='" & e.Node.Key & "'")
            Case "5"
                Select Case e.Node.Level
                    Case 2
                        oRows = ds.Tables(5).Select("UON1= '" & e.Node.ParentNode.Key & "' AND COD='" & e.Node.Key & "'")
                    Case 3
                        oRows = ds.Tables(5).Select("UON1= '" & e.Node.ParentNode.ParentNode.Key & "' AND UON2= '" & e.Node.ParentNode.Key & "' AND COD='" & e.Node.Key & "'")
                    Case 4
                        oRows = ds.Tables(5).Select("UON1= '" & e.Node.ParentNode.ParentNode.ParentNode.Key & "' AND UON2= '" & e.Node.ParentNode.ParentNode.Key & "' AND UON3= '" & e.Node.ParentNode.Key & "' AND COD='" & e.Node.Key & "'")
                    Case 5
                        oRows = ds.Tables(5).Select("UON1= '" & e.Node.ParentNode.ParentNode.ParentNode.ParentNode.Key & "' AND UON2= '" & e.Node.ParentNode.ParentNode.ParentNode.Key & "' AND UON3= '" & e.Node.ParentNode.ParentNode.Key & "' AND UON4='" & e.Node.ParentNode.Key & "' AND COD='" & e.Node.Key & "'")
                End Select

        End Select

        If iNivelNodo > 0 AndAlso iNivelNodo < 6 AndAlso oRows.Length > 0 Then
            If Not IsDBNull(oRows(0).Item("CENTRO_SM")) Then
                If oRows(0).Item("CENTRO_SM") = "PARTIDA" Then
                    ConfigurarNodoPartida(e.Node, oRows)
                Else
                    e.Node.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/centrocoste_small.jpg"
                    e.Node.Target = 1 'para buscar
                End If
            End If
        ElseIf iNivelNodo > 0 Then
            oRows = ds.Tables(6).Select("COD='" & e.Node.Key & "'")
            If oRows.Length > 0 Then
                ConfigurarNodoPartida(e.Node, oRows)
            Else
                oRows = ds.Tables(7).Select("COD='" & e.Node.Key & "'")
                If oRows.Length > 0 Then
                    ConfigurarNodoPartida(e.Node, oRows)
                Else
                    oRows = ds.Tables(8).Select("COD='" & e.Node.Key & "'")
                    If oRows.Length > 0 Then
                        ConfigurarNodoPartida(e.Node, oRows)
                    End If
                End If
            End If
        End If
        If _sValor <> Nothing Then
            Dim sUnidadOrg As Object = Split(_sValor, "#")
            Dim sUON1 As String = ""
            Dim sUON2 As String = ""
            Dim sUON3 As String = ""
            Dim sUON4 As String = ""
            Dim sUON5 As String = ""
            Dim iNivel As Integer = UBound(sUnidadOrg) + 1

            If InStr(sUnidadOrg(iNivel - 1), "|") > 0 Then
                ReDim Preserve sUnidadOrg(iNivel)

                Dim aux As Object = Split(sUnidadOrg(iNivel - 1), "|")
                iNivel = UBound(sUnidadOrg) + 1

                sUnidadOrg(iNivel - 1) = sUnidadOrg(iNivel - 2)
                sUnidadOrg(iNivel - 2) = aux(0)
            End If

            Select Case iNivel
                Case 1
                    sUON1 = sUnidadOrg(0)
                Case 2
                    sUON1 = sUnidadOrg(0)
                    sUON2 = sUnidadOrg(1)
                Case 3
                    sUON1 = sUnidadOrg(0)
                    sUON2 = sUnidadOrg(1)
                    sUON3 = sUnidadOrg(2)
                Case 4
                    sUON1 = sUnidadOrg(0)
                    sUON2 = sUnidadOrg(1)
                    sUON3 = sUnidadOrg(2)
                    sUON4 = sUnidadOrg(3)
                Case 5
                    sUON1 = sUnidadOrg(0)
                    sUON2 = sUnidadOrg(1)
                    sUON3 = sUnidadOrg(2)
                    sUON4 = sUnidadOrg(3)
                    sUON5 = sUnidadOrg(4)
                Case Else
                    sUON1 = sUnidadOrg(0)
                    sUON2 = sUnidadOrg(1)
                    sUON3 = sUnidadOrg(2)
                    sUON4 = sUnidadOrg(3)
                    sUON5 = sUnidadOrg(iNivel - 1)
            End Select

            If iNivel >= 5 Then
                If e.Node.Key = sUON5 Then
                    e.Node.ParentNode.Expanded = True
                    e.Node.ParentNode.ParentNode.Expanded = True
                    e.Node.Selected = True
                End If
            ElseIf iNivelNodo = iNivel Then
                If iNivel = 4 Then
                    If e.Node.Key = sUON4 And e.Node.ParentNode.Key = sUON3 And e.Node.ParentNode.ParentNode.Key = sUON2 And e.Node.ParentNode.ParentNode.ParentNode.Key = sUON1 Then
                        e.Node.ParentNode.Expanded = True
                        e.Node.ParentNode.ParentNode.Expanded = True
                        e.Node.Selected = True
                    End If
                ElseIf iNivel = 3 Then
                    If e.Node.Key = sUON3 And e.Node.ParentNode.Key = sUON2 And e.Node.ParentNode.ParentNode.Key = sUON1 Then
                        e.Node.ParentNode.Expanded = True
                        e.Node.ParentNode.ParentNode.Expanded = True
                        e.Node.Selected = True
                    End If
                ElseIf iNivel = 2 Then
                    If e.Node.Key = sUON2 And e.Node.ParentNode.Key = sUON1 Then
                        e.Node.ParentNode.Expanded = True
                        e.Node.Selected = True
                    End If
                ElseIf iNivel = 1 Then
                    If e.Node.Key = sUON1 Then
                        e.Node.Selected = True
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        CargarArbolCentrosPartidas()
    End Sub

    Private Sub ConfigurarNodoPartida(ByVal nodo As Infragistics.Web.UI.NavigationControls.DataTreeNode, ByVal drRow() As DataRow)
        If drRow.Length > 0 AndAlso Not IsDBNull(drRow(0).Item("CENTRO_SM")) AndAlso drRow(0).Item("CENTRO_SM") = "PARTIDA" Then
            Dim Fechas As String = String.Empty

            If Not _bPlurianual Then               
                If drRow(0)("FECINI") IsNot DBNull.Value Then
                    Fechas = " (" & FormatDate(drRow(0).Item("FECINI"), CType(Page, FSNPage).Usuario.DateFormat) & " - "
                End If
                If drRow(0)("FECFIN") Is DBNull.Value Then
                    Fechas = IIf(Fechas = String.Empty, Fechas, Fechas & ")")
                Else
                    Fechas = IIf(Fechas = String.Empty, " ( - " & FormatDate(drRow(0).Item("FECFIN"), CType(Page, FSNPage).Usuario.DateFormat) & ")", Fechas & FormatDate(drRow(0).Item("FECFIN"), CType(Page, FSNPage).Usuario.DateFormat) & ")")
                End If
            End If
            nodo.Text = nodo.Text & Fechas
            nodo.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/candado.jpg"
            If drRow(0).Item("IMPUTABLE") = 1 Then
                'nodo.Target = 2 'para poder seleccionar y buscar  
                If _bPlurianual Then
                    nodo.Target = 4 'para poder seleccionar y buscar y que la descripcion se ponga tal cual (en Partidas.aspx no recorte buscando los parentesis de las fechas)
                Else
                    nodo.Target = 2 'para poder seleccionar y buscar
                End If
            Else
                nodo.Target = 3 'para poder buscar
                    Dim sNiveles As Object = nodo.Key.ToString.Split("|")
                    If UBound(sNiveles) = 0 Then
                        ds.Tables(6).DefaultView.RowFilter = "cod like '" & nodo.Key & "|%'"
                        'Tarea 3301: en la migración del control UltraWebTree a WebDataTree nos encontramos con que la propiedad databind
                        'del nodo ya no se implementa. Al no tener partidas hasta este nivel no hemos sido capaces de ver exactamente que 
                        'información le transferimos al nodo. Queda pendiente de resolver.
                        'nodo.DataBind(ds.Tables(6).DefaultView, "6")
                    ElseIf UBound(sNiveles) = 1 Then
                        ds.Tables(7).DefaultView.RowFilter = "cod like '" & nodo.Key & "|%'"
                        'nodo.DataBind(ds.Tables(7).DefaultView, "7")
                    ElseIf UBound(sNiveles) = 2 Then
                        ds.Tables(8).DefaultView.RowFilter = "cod like '" & nodo.Key & "|%'"
                        'nodo.DataBind(ds.Tables(8).DefaultView, "8")
                    End If
                End If
                If Not (drRow(0).Item("VIGENTE") = 1) Then
                    nodo.CssClass = "PartidaNoVigente"
                End If
            End If
    End Sub

    ''' <summary>
    ''' El stored de PRES5/Partidas_Load da un dataset con las partidas y las uons para mostrarlas en el tree y una serie de relaciones entre partidas. Pero no relaciona
    ''' todas las tablas de partidas. Aqui se cambia la estructura de tablas para poder establecer las relaciones q faltan.
    ''' </summary>
    ''' <param name="Origen">Dataset con las partidas y las uons para mostrarlas en el tree tal y como lo devuelve el stored</param>
    ''' <returns>Dataset con las partidas y las uons para mostrarlas en el tree. Ahora con relaciones a todos los niveles de uon y partida.</returns>
    ''' <remarks>Llamada desde: CargarArbolCentrosPartidas ; Tiempo máximo: 0,2</remarks>
    Private Function TodasRelaciones(ByVal Origen As DataSet) As DataSet
        Dim Partida1 As New DataColumn
        Dim Partida2 As New DataColumn
        Dim Partida3 As DataColumn
        Dim Partida4 As DataColumn

        Partida1.ColumnName = "P1"
        Partida1.DataType = System.Type.GetType("System.String")
        Partida2.ColumnName = "P2"
        Partida2.DataType = System.Type.GetType("System.String")

        Origen.Tables(6).Columns.Add(Partida1)
        Origen.Tables(6).Columns.Add(Partida2)

        For Each row As DataRow In Origen.Tables(6).Rows
            row("P1") = Split(row("COD"), "|")(0)
            row("P2") = Split(row("COD"), "|")(1)
        Next

        '''''''''
        Partida1 = New DataColumn
        Partida1.ColumnName = "P1"
        Partida1.DataType = System.Type.GetType("System.String")
        Partida2 = New DataColumn
        Partida2.ColumnName = "P2"
        Partida2.DataType = System.Type.GetType("System.String")
        Partida3 = New DataColumn
        Partida3.ColumnName = "P3"
        Partida3.DataType = System.Type.GetType("System.String")

        Origen.Tables(7).Columns.Add(Partida1)
        Origen.Tables(7).Columns.Add(Partida2)
        Origen.Tables(7).Columns.Add(Partida3)

        For Each row As DataRow In Origen.Tables(7).Rows
            row("P1") = Split(row("COD"), "|")(0)
            row("P2") = Split(row("COD"), "|")(1)
            row("P3") = Split(row("COD"), "|")(2)
        Next

        '''''''''
        Partida1 = New DataColumn
        Partida1.ColumnName = "P1"
        Partida1.DataType = System.Type.GetType("System.String")

        Partida2 = New DataColumn
        Partida2.ColumnName = "P2"
        Partida2.DataType = System.Type.GetType("System.String")

        Partida3 = New DataColumn
        Partida3.ColumnName = "P3"
        Partida3.DataType = System.Type.GetType("System.String")

        Partida4 = New DataColumn
        Partida4.ColumnName = "P4"
        Partida4.DataType = System.Type.GetType("System.String")

        Origen.Tables(8).Columns.Add(Partida1)
        Origen.Tables(8).Columns.Add(Partida2)
        Origen.Tables(8).Columns.Add(Partida3)
        Origen.Tables(8).Columns.Add(Partida4)

        For Each row As DataRow In Origen.Tables(8).Rows
            row("P1") = Split(row("COD"), "|")(0)
            row("P2") = Split(row("COD"), "|")(1)
            row("P3") = Split(row("COD"), "|")(2)
            row("P4") = Split(row("COD"), "|")(3)
        Next

        ''''''''' RELACIONES
        Dim parentCols(0) As DataColumn
        Dim childCols(0) As DataColumn
        Dim ColumnsUons As Integer = 0
        Dim iPos As Integer = 0

        If Origen.Tables(2).Rows.Count > 0 Then ColumnsUons = ColumnsUons + 1
        If Origen.Tables(3).Rows.Count > 0 Then ColumnsUons = ColumnsUons + 1
        If Origen.Tables(4).Rows.Count > 0 Then ColumnsUons = ColumnsUons + 1

        For Tratando As Integer = 1 To 3
            'tabla 6: n UONS + 1 COD: Parent 5 Child 6
            'tabla 7: n UONS + 2 COD: Parent 6 Child 7
            'tabla 8: n UONS + 3 COD: Parent 7 Child 8
            ReDim parentCols(ColumnsUons + Tratando)
            ReDim childCols(ColumnsUons + Tratando)

            iPos = 0
            parentCols(iPos) = Origen.Tables(Tratando + 4).Columns("UON1")
            childCols(iPos) = Origen.Tables(Tratando + 5).Columns("UON1")
            iPos = iPos + 1
            If ColumnsUons > 0 Then
                parentCols(iPos) = Origen.Tables(Tratando + 4).Columns("UON2")
                childCols(iPos) = Origen.Tables(Tratando + 5).Columns("UON2")
                iPos = iPos + 1
                If ColumnsUons > 1 Then
                    parentCols(iPos) = Origen.Tables(Tratando + 4).Columns("UON3")
                    childCols(iPos) = Origen.Tables(Tratando + 5).Columns("UON3")
                    iPos = iPos + 1
                    If ColumnsUons > 2 Then
                        parentCols(iPos) = Origen.Tables(Tratando + 4).Columns("UON4")
                        childCols(iPos) = Origen.Tables(Tratando + 5).Columns("UON4")
                        iPos = iPos + 1
                    End If
                End If
            End If
            If Tratando = 1 Then
                parentCols(iPos) = Origen.Tables(Tratando + 4).Columns("COD")
                childCols(iPos) = Origen.Tables(Tratando + 5).Columns("P1")
            End If
            If Tratando = 2 Then
                parentCols(iPos) = Origen.Tables(Tratando + 4).Columns("P1")
                childCols(iPos) = Origen.Tables(Tratando + 5).Columns("P1")
                iPos = iPos + 1
                parentCols(iPos) = Origen.Tables(Tratando + 4).Columns("P2")
                childCols(iPos) = Origen.Tables(Tratando + 5).Columns("P2")
            End If
            If Tratando = 3 Then
                parentCols(iPos) = Origen.Tables(Tratando + 4).Columns("P1")
                childCols(iPos) = Origen.Tables(Tratando + 5).Columns("P1")
                iPos = iPos + 1
                parentCols(iPos) = Origen.Tables(Tratando + 4).Columns("P2")
                childCols(iPos) = Origen.Tables(Tratando + 5).Columns("P2")
                iPos = iPos + 1
                parentCols(iPos) = Origen.Tables(Tratando + 4).Columns("P3")
                childCols(iPos) = Origen.Tables(Tratando + 5).Columns("P3")
            End If

            Origen.Relations.Add("PARTIDAS_" & Tratando, parentCols, childCols, False)

        Next

        '''''''''

        Return Origen

    End Function
End Class