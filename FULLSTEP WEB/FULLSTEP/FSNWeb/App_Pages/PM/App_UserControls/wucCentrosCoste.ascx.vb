﻿Public Partial Class wucCentrosCoste
    Inherits System.Web.UI.UserControl

    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
    "<script language=""{0}"">{1}</script>"

    Private _sValor As String
    Private _bVerUON As Boolean
    Private _sDesde As String
    Private _bVerBts As Boolean
    Private _iIdEmpresa As Integer

    Public Property Valor() As String
        Get
            Valor = _sValor
        End Get
        Set(ByVal Value As String)
            _sValor = Value
        End Set
    End Property

    Public Property VerUON() As Boolean
        Get
            VerUON = _bVerUON
        End Get
        Set(ByVal Value As Boolean)
            _bVerUON = Value
        End Set
    End Property

    Public Property VerBts() As Boolean
        Get
            VerBts = _bVerBts
        End Get
        Set(ByVal Value As Boolean)
            _bVerBts = Value
        End Set
    End Property

    ''' <summary>
    ''' Para saber si se solicita este user control desde las partidas presupuestarias, 
    ''' porque asi las consultas a la base de datos es diferente
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Desde() As String
        Get
            Desde = _sDesde
        End Get
        Set(ByVal Value As String)
            _sDesde = Value
        End Set
    End Property

    Public Property IdEmpresa() As Integer
        Get
            IdEmpresa = _iIdEmpresa
        End Get
        Set(ByVal Value As Integer)
            _iIdEmpresa = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CType(Page, FSNPage).ModuloIdioma = TiposDeDatos.ModulosIdiomas.CentrosCoste

        CType(Page, FSNPage).Title = CType(Page, FSNPage).Textos(0)
        Me.lblTitulo.Text = CType(Page, FSNPage).Textos(0)
        Me.lblCentro.Text = CType(Page, FSNPage).Textos(1)
        Me.btnSeleccionar.Text = CType(Page, FSNPage).Textos(2)
        Me.btnCancelar.Text = CType(Page, FSNPage).Textos(3)

        If Not IsPostBack() Then
            CargarArbolCentros()

            Dim sClientTexts As String
            sClientTexts = ""
            sClientTexts += "var arrTextosML = new Array();"
            sClientTexts += "arrTextosML[0] = '" + JSText(CType(Page, FSNPage).Textos(4)) + "';"
            sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)

            txtCentro.Attributes.Add("onkeyup", "FindClicked('" & txtCentro.ClientID & "','" & wdtCentros.ClientID & "')")
        End If

        If Not VerBts Then

            Me.BtCentroEdit.Visible = False
            Me.CentroEdit.Visible = False


            Me.dCamposFiltro.Attributes.Add("style", "height: 345px; width:95%")
        End If

    End Sub
    ''' <summary>
    ''' Carga el arbol de los centros de coste dependiendo de si viene de las partidas presupuestarias o no
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarArbolCentros()
        Dim cCentros As Fullstep.FSNServer.Centros_SM

        cCentros = Session("FSN_Server").Get_Object(GetType(FSNServer.Centros_SM))

        Me.wdtCentros.DataSource = cCentros.LoadData(CType(Page, FSNPage).Usuario.Cod, CType(Page, FSNPage).Idioma, _bVerUON, _sDesde, _iIdEmpresa)
        Me.wdtCentros.DataBind()

        If Not IsNothing(wdtCentros.Nodes) Then
            For Each node As Infragistics.Web.UI.NavigationControls.DataTreeNode In wdtCentros.AllNodes
                If node.Nodes.Count > 0 Then
                    node.Expanded = True
                End If
            Next
        End If

        If wdtCentros.Nodes.Count = 1 Then
            If wdtCentros.Nodes(0).Nodes.Count = 1 Then
                wdtCentros.Nodes(0).Nodes(0).Selected = True
            End If
        End If
    End Sub
    Private Sub wdtCentros_NodeBound(sender As Object, e As Infragistics.Web.UI.NavigationControls.DataTreeNodeEventArgs) Handles wdtCentros.NodeBound
        Dim ds As DataSet
        Dim oRows() As DataRow = Nothing

        ds = sender.datasource
        Select Case e.Node.Level
            Case 1
                oRows = ds.Tables(e.Node.Level).Select("COD= '" & e.Node.Key & "'")
            Case 2
                oRows = ds.Tables(e.Node.Level).Select("UON1= '" & e.Node.ParentNode.Key & "' AND COD= '" & e.Node.Key & "'")
            Case 3
				oRows = ds.Tables(e.Node.Level).Select("UON1= '" & e.Node.ParentNode.ParentNode.Key & "' AND UON2= '" & e.Node.ParentNode.Key & "' AND COD= '" & e.Node.Key & "'")
			Case 4
                oRows = ds.Tables(e.Node.Level).Select("UON1= '" & e.Node.ParentNode.ParentNode.ParentNode.Key & "' AND UON2= '" & e.Node.ParentNode.ParentNode.Key & "' AND UON3= '" & e.Node.ParentNode.Key & "' AND COD='" & e.Node.Key & "'")
        End Select

        Select Case e.Node.Level
            Case 0
                e.Node.ImageUrl = ConfigurationManager.AppSettings("ruta") & "images/world.gif"
            Case Else
                e.Node.ImageUrl = ConfigurationManager.AppSettings("ruta") & "images/carpetaNaranja.gif"
        End Select

        If e.Node.Level > 0 AndAlso oRows.Length > 0 Then
            If Not IsDBNull(oRows(0).Item("CENTRO_SM")) Then
                e.Node.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/centrocoste_small.jpg"
                e.Node.Target = 1
                e.Node.Attributes.Add("esCentro", 1)
            End If
        End If

        If e.Node.Level = 0 AndAlso Not _bVerUON Then
            e.Node.Text = CType(Page, FSNPage).Textos(5)
        End If

        If _sValor <> Nothing Then
            Dim sUnidadOrg As Object = Split(_sValor, "#")
            Dim sUON1 As String = ""
            Dim sUON2 As String = ""
            Dim sUON3 As String = ""
            Dim sUON4 As String = ""
            Dim iNivel As Integer = UBound(sUnidadOrg) + 1

            If _bVerUON Then
                Select Case iNivel
                    Case 1
                        sUON1 = sUnidadOrg(0)
                    Case 2
                        sUON1 = sUnidadOrg(0)
                        sUON2 = sUnidadOrg(1)
                    Case 3
                        sUON1 = sUnidadOrg(0)
                        sUON2 = sUnidadOrg(1)
                        sUON3 = sUnidadOrg(2)
                    Case 4
                        sUON1 = sUnidadOrg(0)
                        sUON2 = sUnidadOrg(1)
                        sUON3 = sUnidadOrg(2)
                        sUON4 = sUnidadOrg(3)
                End Select
            Else
                iNivel = 1
                sUON1 = _sValor
            End If

            If e.Node.Level = iNivel Then
                    If iNivel = 4 Then
                        If e.Node.Key = sUON4 And e.Node.ParentNode.Key = sUON3 And e.Node.ParentNode.ParentNode.Key = sUON2 And e.Node.ParentNode.ParentNode.ParentNode.Key = sUON1 Then
                            e.Node.ParentNode.Expanded = True
                            e.Node.ParentNode.ParentNode.Expanded = True
                            e.Node.Selected = True
                        End If
                    ElseIf iNivel = 3 Then
                        If e.Node.Key = sUON3 And e.Node.ParentNode.Key = sUON2 And e.Node.ParentNode.ParentNode.Key = sUON1 Then
                            e.Node.ParentNode.Expanded = True
                            e.Node.ParentNode.ParentNode.Expanded = True
                            e.Node.Selected = True
                        End If
                    ElseIf iNivel = 2 Then
                        If e.Node.Key = sUON2 And e.Node.ParentNode.Key = sUON1 Then
                            e.Node.ParentNode.Expanded = True
                            e.Node.Selected = True
                        End If
                    ElseIf iNivel = 1 Then
                        If e.Node.Key = sUON1 Then
                            e.Node.Selected = True
                        End If
                    End If
                End If
            End If

    End Sub
End Class