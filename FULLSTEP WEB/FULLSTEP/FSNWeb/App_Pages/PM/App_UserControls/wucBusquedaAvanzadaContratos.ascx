﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="wucBusquedaAvanzadaContratos.ascx.vb" Inherits="Fullstep.FSNWeb.wucBusquedaAvanzadaContratos" %>
 <script language="javascript" type="text/javascript" >
     /*
     Descripcion:= Funcion que carga los valores del buscador de empresas en la caja de texto
     Parametros:=
     idEmpresa --> Id Empresa
     nombreEmpresa --> Nombre empresa
     Llamada desde:= BuscadorEmpresas.aspx --> aceptar()
     */
     function SeleccionarEmpresa(idEmpresa, nombreEmpresa, nifEmpresa) {
         oIdEmpresa = document.getElementById('<%= hidEmpresa.clientID %>');
         if (oIdEmpresa)
             oIdEmpresa.value = idEmpresa;

         otxtEmpresa = document.getElementById('<%=txtEmpresa.clientID %>');
         if (otxtEmpresa)
             otxtEmpresa.value = nombreEmpresa;
     }

     //Recoge el ID del proveedor seleccionado con el autocompletar
     function selected_Empresa(sender, e) {
         oIdEmpresa = document.getElementById('<%= hidEmpresa.clientID %>');
         if (oIdEmpresa)
             oIdEmpresa.value = e._value;
     } 
 </script>
<%--Panel Selección Peticionario --%>
<input id="btnPeticionariosOculto" type="button" value="button" runat="server" style="display: none" />
<asp:Panel ID="pnlPeticionariosOculto" runat="server" Style="display: none">
</asp:Panel>
<asp:Panel ID="pnlPeticionarios" runat="server" BackColor="White" BorderColor="DimGray"
    BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Left" Style="display: none;
    padding: 2px">
    <table width="550">
        <tr>
            <td align="left" style="line-height:30px;">
                <img src="../../../Images/usuario_buscar.gif" style="padding-left:10px; vertical-align:middle" />
                <asp:Label ID="lblPeticionarios" runat="server" CssClass="RotuloGrande" style="padding-left:10px;">
                </asp:Label>
            </td>
            <td align="right" valign="top">
                <asp:ImageButton runat="server" ID="ImgBtnCerrarPanelPeticionarios" ImageUrl="~/images/Bt_Cerrar.png" style="vertical-align:top;" OnClientClick="ocultarPanelModal('mpePeticionarios')" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="lblFiltroPeticionarios" runat="server" CssClass="Rotulo" />
                <input class="Normal" type="text" id="FiltroPeticionarios" runat="server" style="width:320px;" />
                <asp:Label ID="minCharPeticionarios" runat="server"></asp:Label>
                <div id="listadoPeticionarios" runat="server" style="height: 400px; overflow: auto;border-top:solid 1px #909090; clear: both;margin-top:10px;">
                </div>
            </td>       
        </tr>
    </table>
</asp:Panel>
<ajx:ModalPopupExtender ID="mpePeticionarios" runat="server" PopupControlID="pnlPeticionarios"
    TargetControlID="btnPeticionariosOculto" CancelControlID="ImgBtnCerrarPanelPeticionarios" RepositionMode="None"
    PopupDragHandleControlID="pnlPeticionariosOculto" ClientIDMode="Static">
</ajx:ModalPopupExtender>
 
<%--Panel Selección centro de coste --%>
<input id="btnCentrosOculto" type="button" value="button" runat="server" style="display: none" />
<asp:Panel ID="pnlCentrosOculto" runat="server" Style="display: none">
</asp:Panel>
<asp:Panel ID="pnlCentros" runat="server" BackColor="White" BorderColor="DimGray"
    BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Left" Style="display: none;
    padding: 2px">
    <table width="550">
        <tr>
            <td align="left" style="line-height:30px;">
                <img src="../../../Images/centrocoste.JPG" style="padding-left:10px; vertical-align:middle" />
                <asp:Label ID="lblCentros" runat="server" CssClass="RotuloGrande" style="padding-left:10px;">
                </asp:Label>
            </td>
            <td align="right" valign="top">
                <asp:ImageButton runat="server" ID="ImgBtnCerrarPanelCentros" ImageUrl="~/images/Bt_Cerrar.png" style="vertical-align:top;" OnClientClick="ocultarPanelModal('mpeCentros')" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="lblFiltroCentros" runat="server" CssClass="Rotulo" />
                <input class="Normal" type="text" id="FiltroCentros" runat="server" style="width:320px;" />
                <div id="listadoCentros" runat="server" style="height: 400px; overflow: auto;border-top:solid 1px #909090; clear: both;margin-top:10px;">
                </div>
            </td>       
        </tr>
    </table>
</asp:Panel>
<ajx:ModalPopupExtender ID="mpeCentros" runat="server" PopupControlID="pnlCentros"
    TargetControlID="btnCentrosOculto" CancelControlID="ImgBtnCerrarPanelCentros" RepositionMode="None"
    PopupDragHandleControlID="pnlCentrosOculto" ClientIDMode="Static">
</ajx:ModalPopupExtender>
<%--Panel Seleccion Partidas--%>
<input id="btnPartidasOculto" type="button" value="button" runat="server" style="display: none" />
<asp:Panel ID="pnlPartidasOculto" runat="server" Style="display: none">
</asp:Panel>
<asp:Panel ID="pnlPartidas" runat="server" BackColor="White" BorderColor="DimGray"
    BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Left" Style="display: none;
    padding: 2px">
    <table width="550">
        <tr>
            <td align="left">
                <img src="../../../Images/centrocoste.JPG" style="padding-left:10px; vertical-align:middle;" />
                <asp:Label ID="lblPartidaPres0" runat="server" CssClass="RotuloGrande" style="padding-left:10px;">
                </asp:Label>
            </td>
            <td align="right" valign="top">
                <asp:ImageButton runat="server" ID="ImgBtnCerrarPanelPartida" ImageUrl="~/images/Bt_Cerrar.png" style="vertical-align:top;" OnClientClick="ocultarPanelModal('mpePartidas')" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="listadoInputsFiltro" runat="server">
                </div>
                <div id="listadoPartidas" runat="server"></div>
            </td>       
        </tr>
    </table>
</asp:Panel>
<ajx:ModalPopupExtender ID="mpePartidas" runat="server" PopupControlID="pnlPartidas"
    TargetControlID="btnPartidasOculto" CancelControlID="ImgBtnCerrarPanelPartida" RepositionMode="None"
    PopupDragHandleControlID="pnlPartidasOculto" ClientIDMode="Static">
</ajx:ModalPopupExtender>

<table width="99%" id="marco" runat="server" >
    <tr id="filaProveedor" runat="server" visible ="false">
        <td><asp:Label ID="lblProveedor" runat="server" Text="DProveedor:" CssClass="Etiqueta"></asp:Label></td>
        <td width="300px">
            <table style="background-color:White;width:150px;border:solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
                <tr>
                        <td>
                            <asp:TextBox ID="txtProveedor" runat="server" Width="200px" BorderWidth="0px"></asp:TextBox>
                            <ajx:AutoCompleteExtender ID="txtProveedor_AutoCompleteExtender" runat="server" CompletionSetCount="10"
                            DelimiterCharacters="" Enabled="True" MinimumPrefixLength="1" ServiceMethod="GetProveedores"
                            ServicePath="/App_Pages/_Common/App_Services/AutoComplete.asmx" TargetControlID="txtProveedor"  EnableCaching="False" >
                            </ajx:AutoCompleteExtender>
                        </td>
                        <td style="border-left:solid 1px #BBBBBB;padding-left:2px;padding-right:0px">
                            <asp:ImageButton ID="imgProveedorLupa" runat="server" SkinID="BuscadorLupa" />
                        </td>
                </tr>                        
                <asp:HiddenField ID="hidProveedor" runat="server" />
                <asp:HiddenField ID="hidProveedorCIF" runat="server" />
            </table>
        </td>
        <td id="CeldalblTipo" runat="server"><asp:Label ID="lblTipo" runat="server" CssClass="Etiqueta"></asp:Label></td>
        <td id="CeldawddTipo" runat="server"> <ig:WebDropDown ID="wddTipo" runat="server" Width="200px" EnableMultipleSelection="true"
                                            MultipleSelectionType="Checkbox" EnableClosingDropDownOnSelect="false" BackColor="White">
                                        </ig:WebDropDown>
        </td>
        <td></td>
        <td></td>
    </tr> 
    <tr id="tdMat" runat="server">
        <td><asp:Label ID="lblMaterial" runat="server" Text="DMaterial:" CssClass="Etiqueta"></asp:Label></td>
        <td width="300px">
            <table style="background-color:White;width:150px;border:solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <asp:TextBox ID="txtMaterial" runat="server" Width="200px" BorderWidth="0px"></asp:TextBox>
                    </td>
                    <td style="border-left:solid 1px #BBBBBB;padding-left:2px;padding-right:0px">
                        <asp:ImageButton id="imgMaterialLupa" runat="server" SkinID="BuscadorLupa" /> 
                    </td>
                </tr>                        
                <asp:HiddenField ID="hidMaterial" runat="server" />
            </table>
        </td>
        <td width="185px"><asp:Label ID="lblDescripcion" runat="server" Text="DDescripcion:" CssClass="Etiqueta"></asp:Label></td>
        <td><asp:TextBox ID="txtDescripcion" runat="server" Width="239px"></asp:TextBox>
            <ajx:AutoCompleteExtender ID="txtDescripcion_AutoCompleteExtender" 
                         runat="server" CompletionSetCount="10" DelimiterCharacters="" Enabled="True"
                         MinimumPrefixLength="1" ServiceMethod="GetDescripciones" ServicePath="/App_Pages/_Common/App_Services/AutoComplete.asmx"
                         TargetControlID="txtDescripcion" EnableCaching="False" ></ajx:AutoCompleteExtender >
        </td>
        <td><asp:Label ID="lblPeticionario" runat="server" Text="DPeticionario:" CssClass="Etiqueta"></asp:Label></td>
        <td width="300px">
            <asp:TextBox ID="txtPeticionario" runat="server" CssClass="CajaTexto" style="float:left;width:240px;overflow-y:auto;overflow-x:none;resize:none;height:16px;font-size:12px;" ClientIDMode="Static" TextMode="MultiLine" Rows="1"></asp:TextBox>
            <asp:HiddenField ID="txtPeticionario_Hidden" runat="server" ClientIDMode="Static" />
            <img src="../../../Images/abrir_ptos_susp.gif" id="imgPanelPeticionarios" runat="server" style="margin-left:2px;cursor:pointer;" />
        </td>
    </tr> 
    <tr>
        <td><asp:Label ID="lblArticulo" runat="server" Text="DArticulo:" CssClass="Etiqueta"></asp:Label></td>
        <td>
            <table style="background-color:White;width:150px;border:solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <asp:TextBox ID="txtArticulo" runat="server" Width="200px" BorderWidth="0px"></asp:TextBox>
                        <ajx:AutoCompleteExtender ID="txtArticulo_AutoCompleteExtender" 
                         runat="server" CompletionSetCount="10" DelimiterCharacters="" Enabled="True"
                         MinimumPrefixLength="1" ServiceMethod="GetArticulosContratos" ServicePath="/App_Pages/_Common/App_Services/AutoComplete.asmx"
                         TargetControlID="txtArticulo" EnableCaching="False" ></ajx:AutoCompleteExtender >
                    </td>
                    <td style="border-left:solid 1px #BBBBBB;padding-left:2px;padding-right:0px">
                        <asp:ImageButton id="imgArticuloLupa" runat="server" SkinID="BuscadorLupa" /> 
                    </td>
                </tr>                        
                <asp:HiddenField ID="hidArticulo" runat="server" />
            </table>
        </td>
        <td><asp:Label ID="lblPalabraClave" runat="server" Text="DPalabra Clave:" CssClass="Etiqueta"></asp:Label></td>
        <td><asp:TextBox ID="txtPalabraClave" runat="server" Width="239px" MaxLength="200"></asp:TextBox></td>
        <td><asp:Label ID="lblEstado" runat="server" Text="DEstado:" CssClass="Etiqueta"></asp:Label></td>
        <td><ig:webdropdown runat="server" ID="wddEstado" BackColor="White" EnableMultipleSelection="true" MultipleSelectionType="Checkbox" 
        EnableClosingDropDownOnSelect="false" Width="261" DropDownContainerWidth="261px" EnableDropDownAsChild="false">
        </ig:webdropdown></td>
    </tr>
    <tr>
        <td><asp:Label ID="lblEmpresa" runat="server" Text="DEmpresa:" CssClass="Etiqueta"></asp:Label></td>
        <td>
            <table style="background-color:White;width:150px;border:solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <asp:TextBox ID="txtEmpresa" runat="server" Width="200px" BorderWidth="0px"></asp:TextBox>
                        <ajx:AutoCompleteExtender ID="txtEmpresa_AutoCompleteExtender" 
                         runat="server" CompletionSetCount="10" DelimiterCharacters="" Enabled="True"
                         MinimumPrefixLength="1" ServiceMethod="GetEmpresas" ServicePath="/App_Pages/_Common/App_Services/AutoComplete.asmx"
                         TargetControlID="txtEmpresa" EnableCaching="False" OnClientItemSelected="selected_Empresa"  ></ajx:AutoCompleteExtender >
                    </td>
                    <td style="border-left:solid 1px #BBBBBB;padding-left:2px;padding-right:0px">
                        <asp:ImageButton id="imgEmpresaLupa" runat="server" SkinID="BuscadorLupa" /> 
                    </td>
                </tr>                        
                <asp:HiddenField ID="hidEmpresa" runat="server" />
                
            </table>
        </td>
        <td><asp:Label ID="lblFechaInicio" runat="server" Text="DFechaInicio:" CssClass="Etiqueta"></asp:Label></td>
        <td><igpck:WebDatePicker runat="server" ID="dtFechaInicio" Width="262px" SkinID="Calendario"></igpck:WebDatePicker></td>
        <td><asp:Label ID="lblExpiracion" runat="server" Text="DExpiracion:" CssClass="Etiqueta"></asp:Label></td>
        <td><igpck:WebDatePicker runat="server" ID="dtExpiracion" Width="261px" SkinID="Calendario"></igpck:WebDatePicker></td>
    </tr>
    <tr runat="server" id="CentroPartidas">
        <td>
            <asp:Label ID="lblCentroCoste" runat="server" Text="DCentro de Coste:" CssClass="Etiqueta"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="tbCtroCoste" runat="server" CssClass="Normal" style="float:left;width:200px;height:16px;" ClientIDMode="Static"></asp:TextBox>
            <asp:HiddenField ID="tbCtroCoste_Hidden" runat="server" ClientIDMode="Static" />
            <img src="../../../Images/abrir_ptos_susp.gif" id="imgPanelCentros" runat="server" style="margin-left:2px;cursor:pointer;" />
        </td>
        <td colspan="4"></td>
    </tr> 
    
    <tr runat="server"  id="filaAlerta">
                <td>
             <asp:Label ID="lblMostrarAlerta" runat="server" Text="Mostrar Alerta" CssClass="Etiqueta"></asp:Label>
        </td>
        <td>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <asp:TextBox ID="txtAlerta" runat="server" Text="0" Width="40px"></asp:TextBox>
                    </td>
                    <td>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:Image ID="imgArribaAlerta" runat="server" ImageUrl="~/App_Pages/PMWEB/contratos/images/flecha_arriba.GIF"  style="padding-left: 5px;padding-right: 5px;"/></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Image ID="imgAbajoAlerta" runat="server" ImageUrl="~/App_Pages/PMWEB/contratos/images/flecha_abajo.GIF" style="padding-left: 5px;padding-right: 5px;"/></td>
                            </tr>
                            </tr>
                        </table>
                    </td>
                    <td>
                       <ig:WebDropDown ID="wddPeriodo" runat="server" Width="100px" EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true" DropDownContainerHeight="100px" DropDownContainerWidth="100px" EnableDropDownAsChild="false" EnableCustomValues="false">
                       </ig:WebDropDown>

                    </td>
                    <td>
                          <asp:Label ID="lblAlertaAntes" runat="server" Text="antes de expirar" CssClass="Etiqueta" style="padding-left:2px;padding-right:0px">></asp:Label>

                    </td>

                </tr>
            </table> 
        </td>
        <td>
           
             <asp:Label ID="lblNotificados" runat="server" Text="Configurar notificados:" CssClass="Etiqueta"></asp:Label>
        </td>
        <td>
            <table style="background-color:White;width:260px;border:solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <asp:TextBox ID="txtNotificados" runat="server" BorderWidth="0px" Width="240px"></asp:TextBox>
                                          </td>
                    <td style="border-left:solid 1px #BBBBBB;padding-left:2px;padding-right:0px">
                        <asp:ImageButton id="imgBuscarNotificados" runat="server" SkinID="BuscadorLupa" /> 
                    </td>

                </tr>                        
                <asp:HiddenField ID="hidNotificados" runat="server" />
            </table>


        </td> 

    </tr> 
</table> 
                    <ajx:NumericUpDownExtender ID="NumericUpDownExtender1" runat="server"
                        TargetControlID="txtAlerta" Width="40" TargetButtonDownID="imgAbajoAlerta"
                        TargetButtonUpID="imgArribaAlerta" Minimum="0"
                        RefValues="" ServiceDownMethod="" ServiceUpMethod="" />


<script type="text/javascript">
    function usu_seleccionado(sUsuCod, sUsuNom, sUsuEmail) {
        var txtNotif = document.getElementById('<%= txtNotificados.ClientID %>')
        var hidNotif = document.getElementById('<%= hidNotificados.ClientID %>')
        txtNotif.value = sUsuNom + ' (' + sUsuEmail + ')'
        hidNotif.value = sUsuCod
    }
    </script>