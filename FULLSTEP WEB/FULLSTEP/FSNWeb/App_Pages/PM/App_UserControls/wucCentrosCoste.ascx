﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="wucCentrosCoste_2.ascx.vb" Inherits="Fullstep.FSNWeb.wucCentrosCoste" %>
<script language="javascript">
    function FindClicked(txtbxID, treeID) {
        // Get the TextBox that has the Regular Expression
        
        var txtbx = document.getElementById(txtbxID);

        // Set Regular Expression variable
        var regex = txtbx.value;
        regex = regex.toLowerCase();
        var tree = $find("CentrosCoste_wdtCentros");

        // Call the Find function
        var node = find(tree, regex);
        // Set the selected node on the tree to the node that was returned by the function

        if (node) {
            node.set_selected(true);
            node.get_element().scrollIntoView()
        }
        // Reset objects
        tree = null;
        node = null;
        regex = null;
        txtbx = null;
    }

    function find(tree, regex) {

        // Call the findInCollection method on the tree's nodes
        var node = findInCollection(tree.getNodes(), regex);

        // Return the node that was found
        return node;
    }

    function findInCollection(NodesCollection, regex) {
        // Loop through the Nodes collection
        for (var i = 0; i < NodesCollection.get_length(); i++) {
            // Get the next Node in collection
            var thisNode = NodesCollection.getNode(i);

            // Convert the Node's text to JavaScript String object
            var nodeStr = new String(thisNode.get_text());
            nodeStr = nodeStr.toLowerCase();

            // Check if the string matches the Regular Expression - if so, this is the Node we are looking for
            if ((thisNode.get_target() == "1") && (nodeStr.match(regex)) && (regex != '')) {
                // Reset objects
                NodesCollection = null;
                regex = null;
                nodeStr = null;

                // Return the node that was found
                return thisNode;
            }
            // If this Node has child Nodes, search recursively through its children as well
            else if (thisNode.hasChildren()) {
                // Clear the nodeStr variable
                nodeStr = null;

                // Get the value that is returned after the recursion has finished
                var node = findInCollection(thisNode.getItems(), regex);

                // If a Node was returned, pass it up stack as this is the node we are looking for
                if (node != null) {
                    // Reset objects
                    NodesCollection = null;
                    regex = null;

                    // Return the node that was found
                    return node;
                }
            }


            // Clear the nodeStr variable
            nodeStr = null;
        }

        // Reset objects
        NodesCollection = null;
        regex = null;
    }
</script>

<style>
    .NodeSelected
        {
            background-color:Navy;
            color: white;
            padding-bottom:3px !important;
            padding-top:3px !important;
        }
</style>

<div>
    <table cellspacing="1" cellpadding="3" width="100%" border="0">
    <tr><td valign="middle" colspan="2">
        <table cellpadding="4" cellspacing="0" border="0" width="100%">
        <tr>
        <td width="65"><img alt="" src="<%=ConfigurationManager.AppSettings("ruta")%>images/centrocoste.jpg" /></td>
        <td><asp:label id="lblTitulo" runat="server" cssClass="RotuloGrande"></asp:label></td>
        <td align="right" valign="top">
            <asp:ImageButton ID="imgCerrar" runat="server" SkinID="Cerrar" OnClientClick="javascript:self.close()" /></td>
        </tr>
        </table>
    </td>
    </tr>
    <tr><td style="height:20px"></td></tr>
    <tr id="CentroEdit" runat="server">
        <td colspan="2"><asp:Label ID="lblCentro" runat="server" CssClass="Etiqueta" Text="Centro:"></asp:Label>
        <asp:TextBox ID="txtCentro" runat="server" Width="400px"></asp:TextBox></td>
    </tr>
    <tr><td colspan="2">
            <div id="dCamposFiltro" runat="server" class="dContenedor" style="height: 250px; width:95%">

                <ig:WebDataTree ID="wdtCentros" runat="server"
                    CssClass="igTree" SelectionType="Single" Width="90%" Height="264px" >
                    <NodeSettings SelectedCssClass="NodeSelected" />
                    
                    <DataBindings>
                        <ig:DataTreeNodeBinding DataMember="Table" TextField="DEN" ValueField="COD" KeyField ="COD" />
                        <ig:DataTreeNodeBinding DataMember="Table1" TextField="DEN" ValueField="COD" KeyField ="COD" />
                        <ig:DataTreeNodeBinding DataMember="Table2" TextField="DEN" ValueField="COD" KeyField ="COD" />
                        <ig:DataTreeNodeBinding DataMember="Table3" TextField="DEN" ValueField="COD" KeyField ="COD" />
                        <ig:DataTreeNodeBinding DataMember="Table4" TextField="DEN" ValueField="COD" KeyField ="COD" />
                    </DataBindings>
                </ig:WebDataTree>
            </div>
			</td></tr>
    <TR id="BtCentroEdit" runat="server">
        <TD align="right" width="50%"><fsn:FSNButton ID="btnSeleccionar" runat="server" Text="Seleccionar" Alineacion="Right" OnClientClick="return seleccionarCentroCoste();"></fsn:FSNButton></TD>
		<TD align="left" width="50%"><fsn:FSNButton ID="btnCancelar" runat="server" Text="Cancelar" Alineacion="Left" OnClientClick="window.close()"></fsn:FSNButton></TD>
	    <td></td>
	</TR>
    </table>  
    </div>