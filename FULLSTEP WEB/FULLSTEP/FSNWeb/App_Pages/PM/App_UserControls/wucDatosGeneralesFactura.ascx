﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="wucDatosGeneralesFactura.ascx.vb" Inherits="FSNUserControls.wucDatosGeneralesFactura" %>
<%@ Register Src="wucBusquedaCostesDescuentos.ascx" TagName="BusquedaCostesDescuentos" TagPrefix="fscd" %>
<asp:UpdatePanel ID="upDatosGenerales" UpdateMode="Conditional" ChildrenAsTriggers="false" runat="server">
<ContentTemplate>
<ajx:CollapsiblePanelExtender ID="cpeDatosGenerales" runat="server" CollapseControlID="pnlDatosGenerales"
                    ExpandControlID="pnlDatosGenerales" ImageControlID="imgExpandir0" SuppressPostBack="True"
                    TargetControlID="pnlDetalleDatosGenerales" SkinID="FondoRojo">
                </ajx:CollapsiblePanelExtender>
<asp:Panel ID="pnlDatosGenerales" runat="server" Style="cursor: pointer; width: 99%; height: 25px;">
    <table width="100%" style="background-color:#dAdAdA; border:1px solid #000000;">
        <tr><td width="95%" style="vertical-align: top"><asp:Label ID="lblTituloDatosGenerales" runat="server" CssClass="EtiquetaMediano"></asp:Label></td>
            <td align="right"><asp:Image ID="imgExpandir0" runat="server" SkinID="Contraer" /></td>
        </tr>
     </table>
</asp:Panel>
<asp:Panel ID="pnlDetalleDatosGenerales" runat="server" style="background-color:#FAFAFA; border:1px solid #000000;" Width="99%">
<table width="100%" cellpadding="4" border="0">
<tr>
    <td width="15%"><asp:Label id="lblLitProveedor" runat="server" CssClass="Etiqueta"></asp:Label></td>
    <td colspan="4"><asp:Label id="lblProveedor" runat="server" CssClass="Normal Subrayado" style="cursor: pointer"></asp:Label></td>
</tr>
<tr>
    <td width="15%"><asp:Label id="lblLitEmpresa" runat="server" CssClass="Etiqueta"></asp:Label></td>
    <td><asp:Label id="lblEmpresa" runat="server" CssClass="Normal Subrayado" style="cursor: pointer"></asp:Label></td>
    <td width="12%"><asp:Label id="lblLitNumeroFactura" runat="server" CssClass="Etiqueta"></asp:Label></td>
    <td><asp:Label id="lblNumeroFactura" runat="server" CssClass="Normal"></asp:Label></td>
    <td rowspan="6" valign="top" width="40%">
        <asp:UpdatePanel ID="upImpuestosCabecera" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        <table border="0" width="100%" cellspacing="0" cellpadding="4">
        <tr><td><asp:Label id="lblLitImporteBruto" runat="server" CssClass="Etiqueta"></asp:Label></td>
            <td style="text-align:right;"><asp:Label id="lblImporteBruto" runat="server" CssClass="Normal"></asp:Label><asp:Label id="lblMonedaImporteBruto" runat="server" CssClass="Normal"></asp:Label></td>
        </tr>
        <tr><td><asp:Label id="lblLitImporteBrutoLineas" runat="server" CssClass="Etiqueta"></asp:Label></td>
            <td style="text-align:right;"><asp:Label id="lblImporteBrutoLineas" runat="server" CssClass="Normal"></asp:Label><asp:Label id="lblMonedaImporteBrutoLineas" runat="server" CssClass="Normal"></asp:Label></td>
        </tr>
        <tr><td style="border-top: 1px solid black;"><asp:Label id="lblLitCostesGenerales" runat="server" CssClass="Etiqueta"></asp:Label></td>
            <td style="border-top: 1px solid black; text-align:right;"><asp:Label id="lblSignoCostesGenerales" runat="server" CssClass="Normal"></asp:Label><asp:Label id="lblCostesGenerales" runat="server" CssClass="Normal" ClientIDMode="Static"></asp:Label><asp:Label id="lblMonedaCostesGenerales" runat="server" CssClass="Normal"></asp:Label></td>
        </tr>
        <tr><td><asp:Label id="lblLitDescuentosGenerales" runat="server" CssClass="Etiqueta"></asp:Label></td>
            <td style="text-align:right;"><asp:Label id="lblSignoDescuentosGenerales" runat="server" CssClass="Normal"></asp:Label><asp:Label id="lblDescuentosGenerales" runat="server" CssClass="Normal" ClientIDMode="Static"></asp:Label><asp:Label id="lblMonedaDescuentosGenerales" runat="server" CssClass="Normal"></asp:Label></td>
        </tr>
        <tr><td style="border-top: 1px solid black;"><asp:Label id="lblLitImporteBrutoTotal" runat="server" CssClass="Etiqueta"></asp:Label></td>
            <td style="border-top: 1px solid black;text-align:right;"><asp:Label id="lblImporteBrutoTotal" runat="server" CssClass="Normal"></asp:Label><asp:Label id="lblMonedaImporteBrutoTotal" runat="server" CssClass="Normal"></asp:Label></td>
        </tr>

        <asp:Repeater id="rpImpuestos" runat="server" EnableViewState="true">
        <HeaderTemplate></HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td><asp:Label id="lblLitImpuesto" runat="server" CssClass="Etiqueta"></asp:Label></td>
            <td style="text-align:right;"><asp:Label id="lblImpuesto" runat="server" CssClass="Normal"></asp:Label></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate></FooterTemplate>
        </asp:Repeater>
        
        <tr class="fondo"><td style="border-top: 1px solid black;"><asp:Label id="lblLitImporteTotal" runat="server" CssClass="EtiquetaMediano"></asp:Label></td>
            <td style="border-top: 1px solid black;text-align:right;"><asp:Label id="lblImporteTotal" runat="server" CssClass="EtiquetaMediano" ClientIDMode="Static"></asp:Label><asp:Label id="lblMonedaImporteTotal" runat="server" CssClass="EtiquetaMediano"></asp:Label></td>
        </tr>
        </table>
        </ContentTemplate>
        </asp:UpdatePanel>
    </td>
</tr>
<tr>
    <td width="15%"><asp:Label id="lblLitFechaFactura" runat="server" CssClass="Etiqueta"></asp:Label></td>
    <td>
        <asp:Label id="lblFechaFactura" runat="server" CssClass="Normal"></asp:Label>
        <asp:Label id="lblLitFechaContabilizacion" runat="server" CssClass="Etiqueta" Visible="false"></asp:Label>
        <asp:Label id="lblFechaContabilizacion" runat="server" CssClass="Normal" Visible="false"></asp:Label>
    </td>
    <td width="12%"><asp:Label id="lblLitNumeroERP" runat="server" CssClass="Etiqueta"></asp:Label></td>
    <td><asp:Label id="lblNumeroERP" runat="server" CssClass="Normal"></asp:Label></td>
</tr>
<tr>
    <td width="15%"><asp:Label id="lblLitSituacionActual" runat="server" CssClass="Etiqueta"></asp:Label></td>
    <td><asp:Label id="lblSituacionActual" runat="server" CssClass="Normal Subrayado" style="cursor: pointer"></asp:Label></td>
    <td width="12%"><asp:Label id="lblLitFormaPago" runat="server" CssClass="Etiqueta"></asp:Label></td>
    <td>
        <asp:Label id="lblFormaPago" runat="server" CssClass="Normal"></asp:Label><input type="hidden" id="hFormaPago" runat="server" />
        <asp:DropDownList ID="ddlFormasPago" ClientIDMode="Static" DataTextField="DEN" DataValueField="COD" runat="server" Width="225px" Visible="false"></asp:DropDownList>
    </td>
</tr>
<tr>
    <td width="15%"><asp:Label id="lblLitRetencionGarantia" runat="server" CssClass="Etiqueta"></asp:Label></td>
    <td><asp:Label id="lblRetencionGarantia" runat="server" CssClass="Normal Subrayado" style="cursor: pointer"></asp:Label></td>
    <td width="12%"><asp:Label id="lblLitViaPago" runat="server" CssClass="Etiqueta"></asp:Label></td>
    <td>
        <asp:Label id="lblViaPago" runat="server" CssClass="Normal"></asp:Label><input type="hidden" id="hViaPago" runat="server" />
        <asp:DropDownList ID="ddlViasPago" ClientIDMode="Static" DataTextField="DEN" DataValueField="COD" runat="server" Width="225px" Visible="false"></asp:DropDownList>
    </td>    
</tr>
<tr><td colspan=2></td>
    <td width="12%"><asp:Label id="lblLitPeriodoOriginal" runat="server" CssClass="Etiqueta" Visible="false"></asp:Label></td>
    <td><asp:Label id="lblPeriodoOriginal" runat="server" CssClass="Normal" Visible="false"></asp:Label></td>
</tr>
<tr><td colspan=2></td>
    <td width="12%"><asp:Label id="lblLitFacturaOriginal" runat="server" CssClass="Etiqueta" Visible="false"></asp:Label></td>
    <td><asp:Label id="lblFacturaOriginal" runat="server" CssClass="Normal" Visible="false"></asp:Label></td>
</tr>
<tr>
    <td colspan="4"><asp:Label id="lblLitObservaciones" runat="server" CssClass="Etiqueta"></asp:Label></td>
    <td width="40%"></td>
</tr>
<tr>
    <td colspan="4" rowspan="2" valign="top">
        <asp:Label id="lblObservaciones" runat="server" CssClass="Normal"></asp:Label>
        <textarea id="txtObservaciones" ClientIDMode="Static" runat="server" rows="4" style="width:95%;padding:2px 5px 2px 5px;" visible="false"></textarea>
    </td>
    <td width="40%"></td>
</tr>
</table>
</asp:Panel>                                 
<fsn:FSNPanelInfo ID="FSNPanelDatosEmpresa" runat="server" ServiceMethod="Obtener_DatosEmpresa" TipoDetalle="0"></fsn:FSNPanelInfo>
<fsn:FSNPanelInfo ID="FSNPanelDatosProveedor" runat="server" ServiceMethod="Obtener_DatosProveedor" TipoDetalle="0"></fsn:FSNPanelInfo>
<p><br /></p>
<!-- Panel costes y descuentos -->
<ajx:CollapsiblePanelExtender ID="cpdCostesDescuentosGenerales" runat="server" CollapseControlID="pnlCostesDescuentosGenerales"
                    ExpandControlID="pnlCostesDescuentosGenerales" ImageControlID="imgExpandir0" SuppressPostBack="True"
                    TargetControlID="pnlDetalleCostesDescuentosGenerales" SkinID="FondoRojo">
                </ajx:CollapsiblePanelExtender>
<asp:Panel ID="pnlCostesDescuentosGenerales" runat="server" Style="cursor: pointer; width: 99%; height: 25px;">
    <table width="100%" style="background-color:#dAdAdA; border:1px solid #000000;">
        <tr><td width="95%" style="vertical-align: top"><asp:Label ID="lblTituloCostesDescuentosGenerales" runat="server" CssClass="EtiquetaMediano"></asp:Label></td>
            <td align="right"><asp:Image ID="imgExpandir1" runat="server" SkinID="Contraer" /></td>
        </tr>
     </table>
</asp:Panel>
<asp:Panel ID="pnlDetalleCostesDescuentosGenerales" runat="server" style="background-color:#FAFAFA; border:1px solid #000000;" Width="99%">
<asp:Panel ID="pnlNoExistenCostesDescuentos" runat="server" Visible="false">
<div style="height:40px; margin-top:20px; margin-left:10px">
    <table width="90%"><tr>
    <td><asp:Label ID="lblNoExistenCostesDescuentos" runat="server" CssClass="Etiqueta"></asp:Label></td>
    <td><asp:Image runat="server" ID="imgAgregarCosteNoExisten" style="cursor: pointer" /><asp:Label runat="server" ID="lblAgregarCosteNoExisten" CssClass="Normal Subrayado" style="cursor: pointer"></asp:Label></td>
    <td><asp:Image runat="server" ID="imgAgregarDescuentoNoExisten" style="cursor: pointer" /><asp:Label runat="server" ID="lblAgregarDescuentoNoExisten" CssClass="Normal Subrayado" style="cursor: pointer"></asp:Label></td>
    </tr></table>
</div>
</asp:Panel>
<asp:Panel ID="pnlDesgloseCostesDescuentosGenerales" runat="server">
<div style="margin:10px">
<table width="100%" cellpadding="5">
<tr><td width="50%" valign="top">
<div style="float:left; width: 100%">
    <div style="display:table-cell;padding-right:5px; width: 75%"><asp:Label ID="lblTituloCostes" runat="server" CssClass="EtiquetaMediano"></asp:Label></div>
    <div id="divAgregarCoste" runat="server" style="display:table-cell;padding:5px; width: 25%;">
    <asp:Image runat="server" ID="imgAgregarCoste" style="cursor: pointer" /><asp:Label runat="server" ID="lblAgregarCoste" CssClass="Normal Subrayado" style="cursor: pointer"></asp:Label>
    </div>
</div>
    <div style="clear: both"></div>
    <asp:Repeater id="rpCostes" runat="server">
    <HeaderTemplate>
    <table width="100%" cellspacing="0" cellpadding="3" class="tabla">
    </HeaderTemplate>
    <ItemTemplate>
    <tr>
        <td width="75%" class="costes"><%#Container.DataItem("DEN")%></td>
        <td class="Right costes" width="5%"><%#Container.DataItem("OPERACION")%></td>
        <td class="Right costes" width="10%" nowrap><asp:Label ID="lblValor" runat="server" CssClass="Normal"></asp:Label></td>
        <td class="Right costes" width="10%" nowrap><asp:Label ID="lblImporte" runat="server" CssClass="Normal"></asp:Label></td>
    </tr>
    </ItemTemplate>
    <FooterTemplate>
    <tr><td colspan="4" class="Right" style="background-color: #cccccc"><asp:Label ID="lblTotal" runat="server" CssClass="Normal"></asp:Label></td></tr>
    </table>
    </FooterTemplate>
    </asp:Repeater>
    <br />
    <asp:Panel ID="pnlCostesEditable" runat="server">
    <asp:Repeater id="rpCostesEditable" runat="server">
    <HeaderTemplate>
    <table border="0" width="100%" id="tblCostesGenerales" cellspacing="0" class="tabla">
    <tbody>
    </HeaderTemplate>
    <ItemTemplate>
    <tr>
        <td width="75%" class="costes"><input type="hidden" id="hID" value="<%#Container.DataItem("ID")%>" /><%#Container.DataItem("DEN")%></td>
        <td width="5%" class="costes"><select onchange="recalcularImportes('tblCostesGenerales')"><option value="+%" <%#If (Container.DataItem("OPERACION")="+%","selected","") %>>+%</option>
                            <option value="+" <%#If (Container.DataItem("OPERACION")="+","selected","") %>>+</option>
                            </select>
        <td width="5%" class="costes"><input type="text" id="txtVALOR" name="txtVALOR" size="5" value="<%#Container.DataItem("VALOR")%>" onblur="recalcularImportes('tblCostesGenerales')" /></td>
        <td width="5%" class="Right Bottom"><asp:Label ID="lblImporte" runat="server" CssClass="Normal"></asp:Label></td>
        <td width="5%" class="Right costes"><asp:Label ID="lblMoneda" runat="server" CssClass="Normal"></asp:Label></td>
        <td width="5%" class="costes"><input type="image" id="imgEliminarCoste" runat="server" onclick="deleteRow(this); return false;" /></td>
    </tr>
    </ItemTemplate>
    <FooterTemplate>
    </tbody>
    </table>
    </FooterTemplate>
    </asp:Repeater>
    <table width="100%" cellspacing="0" id="tblTotalDesgloseCostes" class="tabla" runat="server">
    <tr style="background-color: #cccccc"><td width="85%" class="Right"><asp:Label ID="lblLitTotalDesgloseCostes" runat="server" CssClass="Normal"></asp:Label></td>
        <td width="10%" class="Right" nowrap><asp:Label ID="lblTotalDesgloseCostes" runat="server" CssClass="Normal"></asp:Label><asp:Label ID="lblMonedaTotalDesgloseCostes" runat="server" CssClass="Normal"></asp:Label></td>
        <td width="5%"></td></tr>
    </table>
    </asp:Panel>
    <asp:Label ID="lblNoHayCostes" runat="server" CssClass="Etiqueta" style="visibility: hidden"></asp:Label>
    </td>
    <td width="50%" valign="top">
    <div style="float:left; width: 100%">
    <div style="display:table-cell;padding-right:5px; width: 75%"><asp:Label ID="lblTituloDescuentos" runat="server" CssClass="EtiquetaMediano"></asp:Label></div>
    <div id="divAgregarDescuento" runat="server" style="display:table-cell;padding:5px; width: 25%">
    <asp:Image runat="server" ID="imgAgregarDescuento" style="cursor: pointer" /><asp:Label runat="server" ID="lblAgregarDescuento" CssClass="Normal Subrayado" style="cursor: pointer"></asp:Label>
    </div>
    </div>
    <div style="clear: both"></div>
    <asp:Repeater id="rpDescuentos" runat="server">
    <HeaderTemplate>
    <table width="100%" cellspacing="0" cellpadding="3" class="tabla">
    </HeaderTemplate>
    <ItemTemplate>
    <tr>
        <td width="75%" class="costes"><%#Container.DataItem("DEN")%></td>
        <td class="Right costes" width="5%"><%#Container.DataItem("OPERACION")%></td>
        <td class="Right costes" width="10%" nowrap><asp:Label ID="lblValor" runat="server" CssClass="Normal"></asp:Label></td>
        <td class="Right costes" width="10%" nowrap><asp:Label ID="lblImporte" runat="server" CssClass="Normal"></asp:Label></td>
    </tr>
    </ItemTemplate>
    <FooterTemplate>
    <tr><td colspan="4" class="Right" style="background-color: #cccccc"><asp:Label ID="lblTotal" runat="server" CssClass="Normal"></asp:Label></td></tr>
    </table>
    </FooterTemplate>
    </asp:Repeater>
    <br />
    <asp:Panel ID="pnlDescuentosEditable" runat="server">
    <asp:Repeater id="rpDescuentosEditable" runat="server">
    <HeaderTemplate>
    <table border="0" width="100%" id="tblDescuentosGenerales" cellspacing="0" class="tabla">
    <tbody>
    </HeaderTemplate>
    <ItemTemplate>
    <tr>
        <td width="75%" class="costes"><input type="hidden" id="hID" value="<%#Container.DataItem("ID")%>" /><%#Container.DataItem("DEN")%></td>
        <td width="5%" class="costes"><select onchange="recalcularImportes('tblDescuentosGenerales')"><option value="-%" <%#If (Container.DataItem("OPERACION")="-%","selected","") %>>-%</option>
                            <option value="-" <%#If (Container.DataItem("OPERACION")="-","selected","") %>>-</option>
                            </select>
        <td width="5%" class="costes"><input type="text" id="txtVALOR" name="txtVALOR" size="5" value="<%#Container.DataItem("VALOR")%>" onblur="recalcularImportes('tblDescuentosGenerales')" /></td>
        <td width="5%" class="Right Bottom"><asp:Label ID="lblImporte" runat="server" CssClass="Normal"></asp:Label></td>
        <td width="5%" class="Right costes"><asp:Label ID="lblMoneda" runat="server" CssClass="Normal"></asp:Label></td>
        <td width="5%" class="costes"><input type="image" id="imgEliminarDescuento" runat="server" onclick="deleteRow(this); return false;" /></td>
    </tr>
    </ItemTemplate>
    <FooterTemplate>
    </tbody>
    </table>
    </FooterTemplate>
    </asp:Repeater>
    <table width="100%" cellspacing="0" id="tblTotalDesgloseDescuentos" runat="server" class="tabla">
    <tr style="background-color: #cccccc"><td width="85%" class="Right"><asp:Label ID="lblLitTotalDesgloseDescuentos" runat="server" CssClass="Normal"></asp:Label></td>
        <td width="10%" class="Right" nowrap><asp:Label ID="lblTotalDesgloseDescuentos" runat="server" CssClass="Normal"></asp:Label><asp:Label ID="lblMonedaTotalDesgloseDescuentos" runat="server" CssClass="Normal"></asp:Label></td>
        <td width="5%"></td></tr>
    </table>
    </asp:Panel>
    <asp:Label ID="lblNoHayDescuentos" runat="server" CssClass="Etiqueta" style="visibility: hidden"></asp:Label>
    </td>
    </tr>
</table>
</div>
</asp:Panel>
</asp:Panel>

<asp:Button ID="btnActualizarImpuestos" runat="server" EnableViewState="false"
	Style="display: none" />

<%-- Panel de Retención en garantía --%>
<asp:Panel ID="PnlRetencionGarantia" runat="server" Width="850px" Style="padding: 10px; display: none; background-color:white; border:1px solid #000000;">
<table width="100%">
<tr><td width="5%"><img runat="server" id="imgRetencionGarantia" alt="" /></td><td><asp:Label ID="lblTituloRetencionGarantia" runat="server" CssClass="EtiquetaMediano"></asp:Label></td><td align="right"><asp:ImageButton id="imgCerrar" runat="server" /></td></tr>
</table>
<ig:WebDataGrid ID="wdgRetencionGarantia" runat="server" AutoGenerateColumns="false" ShowHeader="true" EnableAjax="true" Height="200px">
</ig:WebDataGrid>
<div style="height:20px;background-color:#cccccc;border:1px solid #cccccc;">
<table width="100%">
<tr><td width="80%" align="right"><asp:Label ID="lblLitTotalRetencionGarantia" runat="server" CssClass="Etiqueta"></asp:Label></td>
    <td width="10%" align="right"><asp:Label ID="lblTotalPorcentajeRetencionGarantia" runat="server" CssClass="Etiqueta"></asp:Label></td>
    <td width="10%" align="right"><asp:Label ID="lblTotalRetencionGarantia" runat="server" CssClass="Etiqueta"></asp:Label></td>
</tr>
</table>
</div>
</asp:Panel>
<asp:Button runat="server" ID="BtnOcultoRetencionGarantia" Style="display: none" EnableViewState="False" />
<ajx:ModalPopupExtender ID="mpePanelRetencionGarantia" runat="server" PopupControlID="PnlRetencionGarantia"
    CancelControlID="imgCerrar" TargetControlID="BtnOcultoRetencionGarantia">
</ajx:ModalPopupExtender>

<%-- Panel de buscador de costes --%>
<asp:Panel ID="PnlBuscadorCostes" runat="server" Width="650px" Style="padding: 10px; display: none; background-color:white; border:1px solid #000000;">
<table width="100%">
<tr><td width="5%"><img runat="server" id="imgCostesFacturas" alt="" /></td><td><asp:Label ID="lblCostesFacturas" runat="server" CssClass="EtiquetaMediano"></asp:Label></td><td align="right"><asp:ImageButton id="imgCerrarCostesFacturas" runat="server" /></td></tr>
</table>
<fscd:BusquedaCostesDescuentos name="BusquedaCostesGenerales" ID="BusquedaCostesGenerales" Tipo="0" Nivel="1" runat="server"></fscd:BusquedaCostesDescuentos>
</asp:Panel>
<asp:Button runat="server" ID="BtnOcultoCostesFacturas" Style="display: none" EnableViewState="False" />
<ajx:ModalPopupExtender ID="mpePanelBuscadorCostes" runat="server" PopupControlID="PnlBuscadorCostes"
    CancelControlID="imgCerrarCostesFacturas" TargetControlID="BtnOcultoCostesFacturas">
</ajx:ModalPopupExtender>


<%-- Panel de buscador de descuentos --%>
<asp:Panel ID="PnlBuscadorDescuentos" runat="server" Width="650px" Style="padding: 10px; display: none; background-color:white; border:1px solid #000000;">
<table width="100%">
<tr><td width="5%"><img runat="server" id="imgDescuentosFacturas" alt="" /></td><td><asp:Label ID="lblDescuentosFacturas" runat="server" CssClass="EtiquetaMediano"></asp:Label></td><td align="right"><asp:ImageButton id="imgCerrarDescuentosFacturas" runat="server" /></td></tr>
</table>
<fscd:BusquedaCostesDescuentos name="BusquedaDescuentosGenerales" ID="BusquedaDescuentosGenerales" Tipo="1" Nivel="1" runat="server"></fscd:BusquedaCostesDescuentos>
</asp:Panel>
<asp:Button runat="server" ID="BtnOcultoDescuentosFacturas" Style="display: none" EnableViewState="False" />
<ajx:ModalPopupExtender ID="mpePanelBuscadorDescuentos" runat="server" PopupControlID="PnlBuscadorDescuentos"
    CancelControlID="imgCerrarDescuentosFacturas" TargetControlID="BtnOcultoDescuentosFacturas">
</ajx:ModalPopupExtender>
</ContentTemplate>
</asp:UpdatePanel>

<script type="text/javascript">
    //guardamos la moneda de la factura
    var Moneda = document.getElementById('<%=lblMonedaImporteBrutoTotal.ClientID%>').innerHTML 

    function Cancelar(pnlBuscador) {
        $find(pnlBuscador).hide();        
    }


    var ROW_BASE = 1; // first number (for display)
    var hasLoaded = true;
    function deleteRow(obj) {
        var delRow = obj.parentNode.parentNode;
        var tbl = delRow.parentNode.parentNode;
        var rIndex = delRow.sectionRowIndex;
        var rowArray = new Array(delRow);
        deleteRows(rowArray);
        recalcularImportes(tbl.id);
        if (tbl.tBodies[0].rows.length == 0) {
            if (tbl.id == 'tblCostesGenerales') {
                document.getElementById("<%= pnlCostesEditable.ClientID %>").style.visibility = 'hidden'
                document.getElementById("<%= lblNoHayCostes.ClientID %>").style.visibility = 'visible'
            } else {
                document.getElementById("<%= pnlDescuentosEditable.ClientID %>").style.visibility = 'hidden'
                document.getElementById("<%= lblNoHayDescuentos.ClientID %>").style.visibility = 'visible'
            }
        }
    }

        function reorderRows(tbl, startingIndex) {
            if (hasLoaded) {
                if (tbl.tBodies[0].rows[startingIndex]) {
                    var count = startingIndex + ROW_BASE;
                    for (var i = startingIndex; i < tbl.tBodies[0].rows.length; i++) {
                        //tbl.tBodies[0].rows[i].myRow.two.name = 'txtVALOR' + count; // input text
                        count++;
                    }
                }
            }
        }

        function deleteRows(rowObjArray) {
            if (hasLoaded) {
                for (var i = 0; i < rowObjArray.length; i++) {
                    var rIndex = rowObjArray[i].sectionRowIndex;
                    rowObjArray[i].parentNode.deleteRow(rIndex);
                }
            }
        }


        function addRowToTable(pnlBuscador, ddlCDFacturas, txtCDGenerico, tableName, tipo) {
            // cell 0 - denominación del coste
            var den = '';
            var def_atrib_id;
            var cmbUltimosCD;

            if (tipo == 0) {
                def_atrib_id = CosteGenerico
            } else {
                def_atrib_id = DescuentoGenerico
            }
            
            if (document.getElementById(txtCDGenerico))
                den = document.getElementById(txtCDGenerico).value;

            if (trim(den) == '')
                if (texto_seleccionado != '') {
                    def_atrib_id = id_seleccionado
                    den = texto_seleccionado;
                }

            if (trim(den) == '') {
                def_atrib_id = document.getElementById(ddlCDFacturas).value
                cmbUltimosCD = document.getElementById(ddlCDFacturas)

                if (cmbUltimosCD.selectedIndex == -1)
                    den=''
                else
                    den= cmbUltimosCD.options[cmbUltimosCD.selectedIndex].text;
            }
            
            if (hasLoaded && trim(den)!='') {
                var tbl = document.getElementById(tableName);
                var nextRow = tbl.tBodies[0].rows.length;
                var iteration = nextRow + ROW_BASE;
                var num = nextRow;
                
                //añadimos la fila
                var row = tbl.tBodies[0].insertRow(num);

                
                                                                              
                var cell0 = row.insertCell(0);
                var textNode = document.createTextNode(den);
                cell0.className="costes"
                cell0.appendChild(textNode);

                //cell 1 - combo con la operación
                var cellSel = row.insertCell(1);
                var sel = document.createElement('select');
                sel.name = 'selOperacion' + iteration;
                if (tipo == 0) {
                    sel.options[0] = new Option('+%', '+%');
                    sel.options[1] = new Option('+', '+');
                } else {
                    sel.options[0] = new Option('-%', '-%');
                    sel.options[1] = new Option('-', '-');
                }
                sel.setAttribute('onchange', 'recalcularImportes("' + tableName + '")')
                cellSel.setAttribute('onchange', 'recalcularImportes("' + tableName + '")')
                cellSel.className = "costes"
                cellSel.appendChild(sel);


                // cell 2 - input text con el valor a aplicar
                var cell1 = row.insertCell(2);
                var txtInp1 = document.createElement('input');
                txtInp1.setAttribute('type', 'text');
                txtInp1.setAttribute('name', "txtVALOR" + iteration);
                txtInp1.setAttribute('size', '5');
                txtInp1.setAttribute('value', '');
                txtInp1.setAttribute('onblur', 'recalcularImportes("' + tableName + '")')
                cell1.className = "costes"
                cell1.appendChild(txtInp1);
                
                // cell 3 - importe
                var cell2 = row.insertCell(3);
                var textNodeImporte = document.createTextNode('');
                cell2.className = "Right Bottom"
                cell2.appendChild(textNodeImporte);

                // cell 4 - moneda
                var cell3 = row.insertCell(4);
                var textNodeMoneda = document.createTextNode(Moneda);
                cell3.className = "Right costes"
                cell3.appendChild(textNodeMoneda);
                
                // cell 5 - eliminar
                var cell4 = row.insertCell(5);
                var imgEliminar = document.createElement('input');
                imgEliminar.setAttribute('type', 'image');
                imgEliminar.setAttribute('name', 'imgEliminarCoste');
                imgEliminar.src = '<%=If(ConfigurationManager.AppSettings("ruta") = Nothing, ConfigurationManager.AppSettings("rutaPM") & "/UserControls", ConfigurationManager.AppSettings("ruta"))%>/images/eliminar.png'
                imgEliminar.onclick = function () { deleteRow(this) };
                cell4.className = "costes"
                cell4.appendChild(imgEliminar);

                var cell5 = row.insertCell(6)
                var txtInp2 = document.createElement('input');
                txtInp2.setAttribute('type', 'hidden');
                txtInp2.setAttribute('name', "hID" + iteration);
                txtInp2.setAttribute('value', def_atrib_id);
                cell5.appendChild(txtInp2);

                // Pass in the elements you want to reference later
                // Store the myRow object in each row
                row.myRow = new myRowObject(textNode, sel, txtInp1, textNodeImporte, txtInp2);

                if (tipo == 0) {
                    document.getElementById("<%= lblNoHayCostes.ClientID %>").style.visibility = 'hidden'
                    document.getElementById("<%= pnlCostesEditable.ClientID %>").style.visibility = 'visible'
                } else {
                    document.getElementById("<%= lblNoHayDescuentos.ClientID %>").style.visibility = 'hidden'
                    document.getElementById("<%= pnlDescuentosEditable.ClientID %>").style.visibility = 'visible'
                }
            }

            //ocultamos el buscador
            $find(pnlBuscador).hide(); 
        }

       
        // myRowObject is an object for storing information about the table rows
        function myRowObject(one, two, three, four, five) {
            this.one = one; // text object
            this.two = two; // select input text object
            this.three = three; // input text object
            this.four = four; // text object
            this.five = five; // input type hidden
        }


        function recalcularImportes(tableName) {
            var total = 0;
            var tbl = document.getElementById(tableName);
            
            for (var i = 0; i < tbl.tBodies[0].rows.length; i++) {
                if (tbl.tBodies[0].rows[i].myRow) {
                    if (validaFloat(tbl.tBodies[0].rows[i].myRow.three.value.toString().replace(",", "."))) {

                        if ((tbl.tBodies[0].rows[i].myRow.two.value == '+') || (tbl.tBodies[0].rows[i].children.item(1).firstChild.value == '-'))
                            tbl.tBodies[0].rows[i].myRow.four.nodeValue = formatNumber(strToNum(tbl.tBodies[0].rows[i].myRow.three.value.toString()));
                        else {
                            tbl.tBodies[0].rows[i].myRow.four.nodeValue = formatNumber((strToNum(document.getElementById('<%=lblImporteBruto.ClientID%>').innerHTML) * strToNum(tbl.tBodies[0].rows[i].myRow.three.value.toString())) / 100)
                        }
                        total = total + strToNum(tbl.tBodies[0].rows[i].myRow.four.nodeValue)
                    } else {
                        tbl.tBodies[0].rows[i].myRow.four.nodeValue = formatNumber(0)
                        tbl.tBodies[0].rows[i].myRow.three.value=''
                    }
                } else {
                if (validaFloat(tbl.tBodies[0].rows[i].children.item(2).firstChild.value.toString().replace(",", "."))) {
                    if ((tbl.tBodies[0].rows[i].children.item(1).firstChild.value == '+') || (tbl.tBodies[0].rows[i].children.item(1).firstChild.value == '-'))
                        tbl.tBodies[0].rows[i].children.item(3).innerHTML = formatNumber(strToNum(tbl.tBodies[0].rows[i].children.item(2).firstChild.value.toString()));
                    else {
                        tbl.tBodies[0].rows[i].children.item(3).innerHTML = formatNumber(strToNum(document.getElementById('<%=lblImporteBruto.ClientID%>').innerHTML) * strToNum(tbl.tBodies[0].rows[i].children.item(2).firstChild.value.toString()) / 100)
                    }
                    total = total + strToNum(tbl.tBodies[0].rows[i].children.item(3).innerHTML)
                } else {
                    tbl.tBodies[0].rows[i].children.item(3).innerHTML = formatNumber(0)
                    tbl.tBodies[0].rows[i].children.item(2).firstChild.value=''
                }
                }
            }

            if (tableName == 'tblCostesGenerales') {
                document.getElementById('<%=lblTotalDesgloseCostes.ClientID%>').innerHTML = formatNumber(total);
                document.getElementById('<%=lblCostesGenerales.ClientID%>').innerHTML = formatNumber(total);
            } else {
                document.getElementById('<%=lblTotalDesgloseDescuentos.ClientID%>').innerHTML = formatNumber(total);
                document.getElementById('<%=lblDescuentosGenerales.ClientID%>').innerHTML = formatNumber(total);
            }

            var impuestos = 1;  //nos va a decir si hay impuestos o no en la factura para hacer el postback para calcularlos o no.
            if (document.getElementById('<%=lblImporteBrutoTotal.ClientID%>').innerHTML == document.getElementById('<%=lblImporteTotal.ClientID%>').innerHTML)
                impuestos = 0

            document.getElementById('<%=lblImporteBrutoTotal.ClientID%>').innerHTML = formatNumber(strToNum(document.getElementById('<%=lblImporteBrutoLineas.ClientID%>').innerHTML) + strToNum(document.getElementById('<%=lblCostesGenerales.ClientID%>').innerHTML) - strToNum(document.getElementById('<%=lblDescuentosGenerales.ClientID%>').innerHTML))
            document.getElementById('<%=lblImporteTotal.ClientID%>').innerHTML = formatNumber(strToNum(document.getElementById('<%=lblImporteBrutoLineas.ClientID%>').innerHTML) + strToNum(document.getElementById('<%=lblCostesGenerales.ClientID%>').innerHTML) - strToNum(document.getElementById('<%=lblDescuentosGenerales.ClientID%>').innerHTML))

            if (Fullstep.PMPortalWeb == undefined) 
                Fullstep.FSNWeb.Facturas.ActualizarCostesDescuentosGenerales(<%=Request("ID")%>, strToNum(document.getElementById("lblImporteTotal").innerHTML), strToNum(document.getElementById("lblCostesGenerales").innerHTML), strToNum(document.getElementById("lblDescuentosGenerales").innerHTML), 0,function(){
                    if (impuestos == 1) {
                        var btnActualizarImpuestos = document.getElementById("<%= btnActualizarImpuestos.clientID %>")
                        btnActualizarImpuestos.click()
                    }   
                });
            else
                Fullstep.PMPortalWeb.Facturas.ActualizarCostesDescuentosGenerales(<%=Request("ID")%>, strToNum(document.getElementById("lblImporteTotal").innerHTML), strToNum(document.getElementById("lblCostesGenerales").innerHTML), strToNum(document.getElementById("lblDescuentosGenerales").innerHTML), 0,function(){
                    if (impuestos == 1) {
                        var btnActualizarImpuestos = document.getElementById("<%= btnActualizarImpuestos.clientID %>")
                        btnActualizarImpuestos.click()
                    }
                });
        }

       
        //Carga en la variable global el texto seleccionado en la grid
        var texto_seleccionado = '';
        var id_seleccionado = '';
        function WebDataGrid_RowSelectionChanged(sender, e) {

            //Gets the selected rows collection of the WebDataGrid
            var selectedRows = e.getSelectedRows();
            //Gets the row that is selected from the selected rows collection
            var row = selectedRows.getItem(0);
            
            //Obtenemos la denominación del coste/descuento seleccionadoa
            var cell = row.get_cell(1);
            texto_seleccionado = cell.get_text();
            //Obtenemos el id del coste/descuento seleccionado
            cell = row.get_cell(2);
            id_seleccionado = cell.get_text();
            //e.getSelectedRows().remove(e.getSelectedRows().getItem(0))
        }

        function validaFloat(numero) {
            if (!/^([0-9])*[.]?[0-9]*$/.test(numero)) {
                alert("El valor " + numero + " no es un número");
                return false;
            } else
                return true
        }

        function trim(cadena) {
            return cadena.replace(/^\s+/g,'').replace(/\s+$/g,'')
        }

</script>
<style type="text/css">
    tbody > tr > td.Left
    {
        text-align:left;
        padding-left: 5px;
        }
    tbody > tr > td.Right
    {
        text-align:right;
        padding-right: 5px;
    }
    
    .costes
    {
    border: solid;
	border-color: #cccccc;
	border-width: 0px 1px 1px 0px;
    }
    
    .Bottom
    {
    border: solid;
	border-color: #cccccc;
	border-width: 0px 0px 1px 0px;
    }
    
    .ToplessBorder
    {
    border: solid;
	border-color: #cccccc;
	border-width: 0px 1px 1px 1px;
    }
    
    .tabla
    {
    	background-color:White; 
    	border: 1px solid #cccccc; 
    	border-width: 1px 0px 1px 1px
    }
</style>

