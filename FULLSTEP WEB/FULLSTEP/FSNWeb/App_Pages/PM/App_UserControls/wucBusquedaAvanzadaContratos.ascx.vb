﻿Imports Infragistics.Web.UI
Partial Public Class wucBusquedaAvanzadaContratos
    Inherits System.Web.UI.UserControl

    Private pag As FSNPage
    Private oTextos As DataTable
    Private mlFiltro As Long = 0

#Region " PROPIEDADES "

    Public Property Peticionario() As String '(Seleccion Multiple)
        Get
            Dim sPeticionario As String = ""
            If Not String.IsNullOrEmpty(txtPeticionario.Text) Then
                If Not String.IsNullOrEmpty(txtPeticionario_Hidden.Value) Then
                    Return txtPeticionario_Hidden.Value 'HAY QUE SEPARARLOS POR $$$ EN EL JAVASCRIPT
                End If
            End If
            Return String.Empty
        End Get
        Set(value As String)
            cargarPeticionarios(value)
        End Set
    End Property
    Public Property Tipo() As String '(Seleccion Multiple)
        Get
            Dim sTipo As String = ""
            For i = 0 To wddTipo.Items.Count - 1
                If wddTipo.Items(i).Selected Then
                    If sTipo <> "" Then sTipo += "$$$"
                    sTipo = sTipo + wddTipo.Items(i).Value
                End If
            Next
            Tipo = sTipo
        End Get

        Set(ByVal Value As String)
            Dim sTexto As String = ""
            If Value <> "" Then
                For i = 0 To wddTipo.Items.Count - 1
                    If wddTipo.Items(i).Value <> "" Then
                        If buscarElemento(Value, wddTipo.Items(i).Value) Then
                            wddTipo.Items(i).Selected = True
                            If sTexto <> "" Then sTexto += ","
                            sTexto = sTexto + wddTipo.Items(i).Text
                        End If
                    End If
                Next
            Else
                If wddTipo.SelectedItems.Count > 0 Then
                    For i = 0 To wddTipo.Items.Count - 1
                        wddTipo.Items(i).Selected = False
                    Next
                End If
            End If
            wddTipo.CurrentValue = sTexto
        End Set
    End Property
    Public Property FilaProveedorVisible() As Boolean
        Get
            FilaProveedorVisible = filaProveedor.Visible
        End Get
        Set(ByVal Value As Boolean)
            filaProveedor.Visible = Value
            If Value Then
                CargarScriptProveedor()
            End If
        End Set
    End Property
    Public Property CeldaLabelTipoVisible() As Boolean
        Get
            CeldaLabelTipoVisible = CeldalblTipo.Visible
        End Get
        Set(ByVal Value As Boolean)
            CeldalblTipo.Visible = Value
        End Set
    End Property
    Public Property CeldawddTipoVisible() As Boolean
        Get
            CeldawddTipoVisible = CeldawddTipo.Visible
        End Get
        Set(ByVal Value As Boolean)
            CeldawddTipo.Visible = Value
        End Set
    End Property
    Public Property FilaAlertaVisible() As Boolean
        Get
            FilaAlertaVisible = filaAlerta.Visible
        End Get
        Set(ByVal Value As Boolean)
            filaAlerta.Visible = Value
        End Set
    End Property
    Public Property Descripcion() As String
        Get
            Descripcion = txtDescripcion.Text
        End Get
        Set(ByVal Value As String)
            txtDescripcion.Text = Value
        End Set
    End Property
    Public Property PalabraClave() As String
        Get
            PalabraClave = txtPalabraClave.Text
        End Get
        Set(ByVal Value As String)
            txtPalabraClave.Text = Value
        End Set
    End Property
    Public Property EmpresaTexto() As String
        Get
            EmpresaTexto = txtEmpresa.Text
        End Get
        Set(ByVal Value As String)
            txtEmpresa.Text = Value
        End Set
    End Property
    'Almacenar para la BBDD
    Public Property EmpresaHidden() As String
        Get
            EmpresaHidden = hidEmpresa.Value
        End Get
        Set(ByVal Value As String)
            '(gmn1-gmn2-gmn3-gmn4)
            hidEmpresa.Value = Value
        End Set
    End Property

    Public Property MaterialTexto() As String
        Get
            MaterialTexto = txtMaterial.Text
        End Get
        Set(ByVal Value As String)
            txtMaterial.Text = Value
        End Set
    End Property
    'Almacenar para la BBDD
    Public Property MaterialHidden() As String
        Get
            MaterialHidden = hidMaterial.Value
        End Get
        Set(ByVal Value As String)
            '(gmn1-gmn2-gmn3-gmn4)
            hidMaterial.Value = Value
        End Set
    End Property

    Public Property ArticuloTexto() As String
        Get
            ArticuloTexto = txtArticulo.Text
        End Get
        Set(ByVal Value As String)
            txtArticulo.Text = Value
        End Set
    End Property

    Public Property ProveedorTexto() As String
        Get
            ProveedorTexto = txtProveedor.Text
        End Get
        Set(ByVal Value As String)
            txtProveedor.Text = Value
        End Set
    End Property
    Public Property ProveedorHidden() As String
        Get
            ProveedorHidden = hidProveedor.Value
        End Get
        Set(ByVal Value As String)
            '(gmn1-gmn2-gmn3-gmn4)
            hidProveedor.Value = Value
        End Set
    End Property
    Public Property ProveedorCIFHidden() As String
        Get
            ProveedorCIFHidden = hidProveedorCIF.Value
        End Get
        Set(ByVal Value As String)
            '(gmn1-gmn2-gmn3-gmn4)
            hidProveedorCIF.Value = Value
        End Set
    End Property
    'Almacenar para la BBDD
    Public Property ArticuloHidden() As String
        Get
            ArticuloHidden = hidArticulo.Value
        End Get
        Set(ByVal Value As String)
            '(gmn1-gmn2-gmn3-gmn4)
            hidArticulo.Value = Value
        End Set
    End Property


    Public Property FechaInicio() As Date
        Get
            FechaInicio = IIf(dtFechaInicio.Value <> Nothing, dtFechaInicio.Value, CDate(Nothing))
        End Get
        Set(ByVal Value As Date)
            dtFechaInicio.Value = Value
        End Set
    End Property

    Public Property FechaExpiracion() As Date
        Get
            FechaExpiracion = IIf(dtExpiracion.Value <> Nothing, dtExpiracion.Value, CDate(Nothing))
        End Get
        Set(ByVal Value As Date)
            dtExpiracion.Value = Value
        End Set
    End Property


    Public Property Estado() As String '(Seleccion Multiple)
        Get
            Dim sEstados As String = ""
            For i = 0 To wddEstado.Items.Count - 1
                If wddEstado.Items(i).Selected Then
                    If sEstados <> "" Then sEstados += "$$$"
                    sEstados = sEstados + wddEstado.Items(i).Value
                End If
            Next
            Estado = sEstados
        End Get

        Set(ByVal Value As String)
            Dim sTexto As String = ""
            If Value <> "" Then
                For i = 0 To wddEstado.Items.Count - 1
                    If wddEstado.Items(i).Value <> "" Then
                        If buscarElemento(Value, wddEstado.Items(i).Value) Then
                            wddEstado.Items(i).Selected = True
                            If sTexto <> "" Then sTexto += ","
                            sTexto = sTexto + wddEstado.Items(i).Text
                        End If
                    End If
                Next
            Else
                If wddEstado.SelectedItems.Count > 0 Then
                    For i = 0 To wddEstado.Items.Count - 1
                        wddEstado.Items(i).Selected = False
                    Next
                End If
            End If
            wddEstado.CurrentValue = sTexto
        End Set

    End Property

    Public Property CentroCoste As String
        Get
            If Not String.IsNullOrEmpty(tbCtroCoste.Text) Then
                If Not String.IsNullOrEmpty(tbCtroCoste_Hidden.Value) Then
                    Return tbCtroCoste_Hidden.Value
                End If
            End If
            Return String.Empty
        End Get
        Set(value As String)
            cargarCentroCoste(value)
        End Set
    End Property

    Public Property AlertaExpiracion() As Integer
        Get
            AlertaExpiracion = IIf(txtAlerta.Text.Length > 0, txtAlerta.Text, 0)
        End Get
        Set(ByVal Value As Integer)
            txtAlerta.Text = IIf(Value <> 0, Value, String.Empty)
        End Set
    End Property

    Public Property AlertaPeriodo() As Integer
        Get
            AlertaPeriodo = wddPeriodo.SelectedValue
        End Get
        Set(ByVal Value As Integer)
            If wddPeriodo.Items.Count = 0 Then CargarPeriodos()
            wddPeriodo.SelectedValue = Value
        End Set
    End Property

    Public Property AlertaNotificado() As String
        Get

            If Not String.IsNullOrEmpty(txtNotificados.Text) Then
                If Not String.IsNullOrEmpty(hidNotificados.Value) Then
                    Return hidNotificados.Value
                End If
            End If

            Return String.Empty
        End Get
        Set(ByVal Value As String)
            cargarNotificado(Value)
        End Set
    End Property

    ''' <summary>
    ''' Devuelve en tabla un listado de las UON asociadas a la(s) partida(s) seleccionada(s) en los filtros
    ''' La estructura devuelta es: UON1#UON2#UON3#UON4#PRESN, donde N es el nivel de imputación
    ''' ESTA PROPIEDAD SE USA SOLO EN VISORCONTRATOS. PARA CONFIGURARFILTROSCONTRATOS HAY QUE USAR LA SUYA
    ''' </summary>
    Public ReadOnly Property PartidasPresUONS As DataTable
        Get
            'Partidas en los cuadros de texto de partidas del buscador
            Dim sPartidasPres As String() = arrPartidasPres()

            Dim dtPartidasPres As DataTable
            If sPartidasPres IsNot Nothing AndAlso sPartidasPres.Length > 0 Then
                Dim oOrdenes As FSNServer.COrdenes = pag.FSNServer.Get_Object(GetType(FSNServer.COrdenes))
                dtPartidasPres = oOrdenes.CargarTablaPartidas(sPartidasPres, TiposDeDatos.Aplicaciones.PM)

                'ahora que tenemos las partidas en una tabla, con la estructura pres0#res1#pres2#pres3#pres4
                'vamos a hacer una consulta para recibir las uon que tienen asociada esa partida.
                'Vamos a recibir una estructura uon1#uon2#uon3#uon4#presN, donde N es el nivel de imputación
                Dim oPRES5 As FSNServer.PRES5 = pag.FSNServer.Get_Object(GetType(FSNServer.PRES5))
                dtPartidasPres = oPRES5.DevolverPartidasUONsPM(dtPartidasPres)
            End If
            Return dtPartidasPres
        End Get
    End Property

    ''' <summary>
    ''' Insertamos en la búsqueda los valores de las partidas guardados en los filtros
    ''' ESTA PROPIEDAD SE USA SOLO EN CONFIGURARFILTROSCONTRATOS. PARA RECUPERAR SU VALOR HAY QUE USAR LA FUNCIÓN getPartidasPres, así ahorramos en iteraciones
    ''' </summary>
    Public WriteOnly Property PartidasPres As String()
        'Get
        '    Dim sPartidasPres As String() = arrPartidasPres()
        '    Return sPartidasPres
        'End Get
        Set(value As String())
            'CARGAMOS LAS PARTIDAS DEL FILTRO EN LOS CONTROLES DE PARTIDAS
            CargarPartidasPres(value)
        End Set
    End Property

    ''' Revisado por: blp. Fecha: 28/08/2013
    ''' <summary>
    ''' Devuelve en tabla un listado de las la(s) partida(s) seleccionada(s) en los filtros
    ''' La estructura devuelta es: PRES0#PRES1#PRES2#PRES3#PRES4. SE DETIENE EN EL NIVEL DE IMPUTACIÓN
    ''' </summary>
    ''' <returns>La estructura devuelta es: PRES0#PRES1#PRES2#PRES3#PRES4. SE DETIENE EN EL NIVEL DE IMPUTACIÓN</returns>
    ''' <remarks>Llamada desde: ConfigurarFiltrosContratos.aspx.vb. Max. 0,1</remarks>
    Public Function getPartidasPres() As String()
        Dim sPartidasPres As String() = arrPartidasPres()
        Return sPartidasPres
    End Function

    Public WriteOnly Property Filtro() As Long
        Set(ByVal Value As Long)
            If pag Is Nothing Then pag = CType(Me.Page, FSNPage) 'se carga antes del load
            'CargarPeticionarios(pag.Usuario.CodPersona, Nothing)
            CargarEstados()
            CargarTiposSolicitudes()
            mlFiltro = Value
        End Set
    End Property
#End Region


    ''' Revisado por:blp. Fecha: 22/08/2013
    ''' <summary>
    ''' Init de la página
    ''' </summary>
    ''' <param name="sender">lanza el evento</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamado desde el evento. Max 0,1 seg</remarks>
    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        If pag Is Nothing Then pag = CType(Me.Page, FSNPage)
        pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorContratos

        If pag.Acceso.gbAccesoFSSM Then CargarPanelPartidas() 'Su numero varía -> cada recarga lo pinta

        If Not Page.IsPostBack Then
            'Si dejasen de cargarse los peticionarios por jQuery, habría que eliminar esta la llamada a CargarScriptBuscadoresJQuery del Init y descomentar la de CargarCombosPartidas
            CargarScriptBuscadoresJQuery()

            CargarPanelPeticionarios()

            If pag.Acceso.gbAccesoFSSM Then                
                CargarPanelCentros()
            Else
                CentroPartidas.Visible = False
            End If
        End If
    End Sub
    ''' <summary>
    ''' Carga el control de usuario
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CargarIdiomasControles()
        InicializacionControles()
        If IsPostBack Then Exit Sub

        If mlFiltro = Nothing Then
            CargarPeriodos()
            wddPeriodo.SelectedValue = 0
            CargarTiposSolicitudes()
            CargarEstados()
        End If
    End Sub

    ''' <summary>
    ''' Inicializa los componentes y funciones javascript de los elementos
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0seg.</remarks>
    Private Sub InicializacionControles()

        imgBuscarNotificados.Attributes.Add("onClick", "javascript:var newWindow = window.open('" & ConfigurationManager.AppSettings("rutaFS") & "_common/BuscadorUsuarios.aspx?desde=contratos&IdControl=" & Me.ClientID & "_" & txtNotificados.ClientID & "&idHidControl=" & hidNotificados.ClientID & "&Multiple=0','_blank','width=800,height=600, location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =no');return false;")

        imgMaterialLupa.Attributes.Add("onClick", "javascript:var newWindow = window.open('" & ConfigurationManager.AppSettings("rutaPM") & "_common/materiales.aspx?desde=WebPart&IdControl=" & Me.ClientID & "_" & txtMaterial.ID & "&MaterialGS_BBDD=" & ClientID & "_" & hidMaterial.ID & "&SessionId=" & HttpContext.Current.Session("sSession") & "','_blank', 'width=550,height=550,status=yes,resizable=no,top=100,left=200');return false;")
        imgEmpresaLupa.Attributes.Add("onClick", "javascript:var newWindow = window.open('" & ConfigurationManager.AppSettings("rutaFS") & "_common/BuscadorEmpresas.aspx?desde=visorcontratos&IdControl=" & Me.ClientID & "_" & txtMaterial.ID & "&MaterialGS_BBDD=" & ClientID & "_" & hidMaterial.ID & "&SessionId=" & HttpContext.Current.Session("sSession") & "','_blank','width=800,height=600, location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =no');return false;")
        txtMaterial.Attributes.Add("onkeydown", "javascript:return eliminarMaterial(event)")
        txtArticulo.Attributes.Add("onkeydown", "javascript:return eliminarArticulo(event)")
        txtEmpresa.Attributes.Add("onkeydown", "javascript:return eliminarEmpresa(event)")
        imgProveedorLupa.OnClientClick = "var newWindow = window.open('" & ConfigurationManager.AppSettings("RutaFS") &
                                            "_common/BuscadorProveedores.aspx?SessionId=" & HttpContext.Current.Session("sSession") & "&PM=true&Contr=true&desde=TextBoxAndHiddenField" &
                                            "&HiddenFieldID=" & hidProveedor.ID & "', '_blank', 'width=835,height=635,status=yes,resizable=no,top=0,left=150,scrollbars=yes');return false;"
        txtProveedor.Attributes.Add("onKeyDown", "return ControlProveedor(this, event);")
        txtNotificados.Attributes.Add("onkeydown", "javascript:return EliminarNotificadoInterno(event)")


        If Not Page.ClientScript.IsClientScriptBlockRegistered("EliminarNotificadoInterno") Then
            Dim sScript As String = "function EliminarNotificadoInterno() {" & vbCrLf &
                    " campoNotif = document.getElementById('" & txtNotificados.ClientID & "');" & vbCrLf &
                    " campoHiddenNotif = document.getElementById('" & hidNotificados.ClientID & "');" & vbCrLf &
                    " campoNotif.value = '';  " & vbCrLf &
                    " campoHiddenNotif = '';  " & vbCrLf &
                    " return false; " & vbCrLf &
            " } " & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "EliminarNotificadoInterno", sScript, True)
        End If

        'Funcion que sirve para eliminar el material y el articulo relacionado
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarMaterial") Then
            Dim sScript As String = "function eliminarMaterial(event) {" & vbCrLf &
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf &
                    "  " & vbCrLf &
            " } else { " & vbCrLf &
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf &
                    " campo = document.getElementById('" & txtMaterial.ClientID & "')" & vbCrLf &
                    " if (campo) { campo.value = ''; } " & vbCrLf &
                    " campoHidden = document.getElementById('" & hidMaterial.ClientID & "')" & vbCrLf &
                    " if (campoHidden) { campoHidden.value = ''; } " & vbCrLf &
                    " campoArticulo = document.getElementById('" & txtArticulo.ClientID & "')" & vbCrLf &
                    " if (campoArticulo) { campoArticulo.value = ''; } " & vbCrLf &
                    " campoHiddenArticulo = document.getElementById('" & hidArticulo.ClientID & "')" & vbCrLf &
                    " if (campoHiddenArticulo) { campoHiddenArticulo.value = ''; } " & vbCrLf &
            " } " & vbCrLf &
            " return false; " & vbCrLf &
            " } }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarMaterial", sScript, True)
        End If

        'Funcion que sirve para eliminar el articulo relacionado
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarArticulo") Then
            Dim sScript As String = "function eliminarArticulo(event) {" & vbCrLf &
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf &
                    "  " & vbCrLf &
            " } else { " & vbCrLf &
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf &
                    " campoArticulo = document.getElementById('" & txtArticulo.ClientID & "')" & vbCrLf &
                    " if (campoArticulo) { campoArticulo.value = ''; } " & vbCrLf &
                    " campoHiddenArticulo = document.getElementById('" & hidArticulo.ClientID & "')" & vbCrLf &
                    " if (campoHiddenArticulo) { campoHiddenArticulo.value = ''; } " & vbCrLf &
             " } " & vbCrLf &
            "  " & vbCrLf &
            " } }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarArticulo", sScript, True)
        End If


        'Funcion que sirve para eliminar la empresa relacionada
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarEmpresa") Then
            Dim sScript As String = "function eliminarEmpresa(event) {" & vbCrLf &
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf &
                    "  " & vbCrLf &
            " } else { " & vbCrLf &
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf &
                    " campoEmpresa = document.getElementById('" & txtEmpresa.ClientID & "')" & vbCrLf &
                    " if (campoEmpresa) { campoEmpresa.value = ''; } " & vbCrLf &
                    " campoHiddenEmpresa = document.getElementById('" & hidEmpresa.ClientID & "')" & vbCrLf &
                    " if (campoHiddenEmpresa) { campoHiddenEmpresa.value = ''; } " & vbCrLf &
            " } " & vbCrLf &
            "  " & vbCrLf &
            " } }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarEmpresa", sScript, True)
        End If

        txtProveedor.Attributes.Add("onKeyDown", "return ControlProveedor(this, event);")

        'Funcion que sirve para eliminar la empresa relacionada 
        If Not Page.ClientScript.IsClientScriptBlockRegistered("ControlProveedor") Then
            Dim sScript As String = " function ControlProveedor(elem, event)  {" & vbCrLf &
                    " oIdProveedor = document.getElementById('" & hidProveedor.ClientID & "')" & vbCrLf &
                    " if (oIdProveedor) { oIdProveedor.value = ''; } " & vbCrLf &
                    " if (event.keyCode == 8 || event.keyCode == 46) { " & vbCrLf &
                    " elem.value = ''" & vbCrLf &
                    " return false; " & vbCrLf &
                    " } else" & vbCrLf &
                    " return true;" & vbCrLf &
            " } " & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ControlProveedor", sScript, True)
        End If


    End Sub

    ''' Revisado por: blp. Fecha: 03/10/2012
    ''' <summary>
    ''' Carga el script necesario para cargar los datos del proveedor seleccionado en el buscador
    ''' </summary>
    ''' <remarks>Llamada desde: la propiedad FilaProveedorVisible (set)</remarks>
    Sub CargarScriptProveedor()
        If Not Page.ClientScript.IsClientScriptBlockRegistered("prove_seleccionado") Then
            Dim sScript As String = " function prove_seleccionado2(sCIF, sProveCod, sProveDen) {" & vbCrLf & _
                    " document.getElementById('" & hidProveedorCIF.ClientID & "').value = sCIF " & vbCrLf & _
                    " document.getElementById('" & hidProveedor.ClientID & "').value = sProveCod " & vbCrLf & _
                    " document.getElementById('" & txtProveedor.ClientID & "').value = sProveDen " & vbCrLf & _
            " } " & vbCrLf
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "prove_seleccionado", sScript, True)
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered("selected_Proveedor") Then
            Dim sScript As String = "function selected_Proveedor(sender, e) {" & vbCrLf & _
                    " oIdProveedor = document.getElementById('" & hidProveedor.ClientID & "')" & vbCrLf & _
                    " if (oIdProveedor) { oIdProveedor.value = e._value; } " & vbCrLf & _
            " } " & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "selected_Proveedor", sScript, True)
        End If
    End Sub

    ''' <summary>
    ''' Carga los controles con el idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde:=Page_load; Tiempo máximo:=0seg.</remarks>
    Private Sub CargarIdiomasControles()
        pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorContratos
        lblMaterial.Text = pag.Textos(26) & ":"
        lblArticulo.Text = pag.Textos(27) & ":"
        lblEmpresa.Text = pag.Textos(28) & ":"
        lblDescripcion.Text = pag.Textos(29) & ":"
        lblPalabraClave.Text = pag.Textos(30) & ":"
        lblFechaInicio.Text = pag.Textos(31) & ":"
        lblExpiracion.Text = pag.Textos(32) & ":"
        lblPeticionario.Text = pag.Textos(33) & ":"
        lblEstado.Text = pag.Textos(34) & ":"
        lblProveedor.Text = String.Format("{0}:", pag.Textos(13))
        lblTipo.Text = String.Format("{0}:", pag.Textos(21))
        lblCentroCoste.Text = pag.Textos(98) & ":" 'Centro de Coste:
        lblFiltroCentros.Text = pag.Textos(98) & ": " 'Centro de coste:
        lblCentros.Text = pag.Textos(99) & ": " & pag.Textos(98) 'Selección de: Centro de coste
        lblPeticionarios.Text = pag.Textos(100) 'Búsqueda de Peticionario
        lblFiltroPeticionarios.Text = pag.Textos(33) & ":" 'Peticionario:
        minCharPeticionarios.Text = pag.Textos(101) '(Mín. 4 caracteres)
        lblMostrarAlerta.Text = pag.Textos(103) & ":" 'Alerta expiraciÃ³n:
        lblNotificados.Text = pag.Textos(104) & ":" 'Notificados:
        lblAlertaAntes.Text = pag.Textos(109) 'antes de expirar
    End Sub

    ''' <summary>
    ''' Llama al buscador de articulos al pinchar en la lupa.
    ''' Se le pasa el material GS elegido, si lo hay
    ''' </summary>
    ''' <param name="sender">propios del evento</param>
    ''' <param name="e">propios del evento</param>            
    ''' <remarks>Tiempo máximo=0seg.</remarks>
    Private Sub imgArticuloLupa_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgArticuloLupa.Click
        Dim sCodArticuloID As String = ""
        Dim sDenArticuloID As String = ""
        Dim sMaterialBD, sMaterialDen As String

        Dim material() As String
        Dim sMat As String = ""
        If hidMaterial.Value <> "" Then
            material = Split(hidMaterial.Value, "-")
            Dim i As Byte
            Dim lLongCod As Long
            i = 1
            For i = 1 To material.GetUpperBound(0)
                Select Case i
                    Case 1
                        lLongCod = pag.FSNServer.LongitudesDeCodigos.giLongCodGMN1
                    Case 2
                        lLongCod = pag.FSNServer.LongitudesDeCodigos.giLongCodGMN2
                    Case 3
                        lLongCod = pag.FSNServer.LongitudesDeCodigos.giLongCodGMN3
                    Case 4
                        lLongCod = pag.FSNServer.LongitudesDeCodigos.giLongCodGMN4
                End Select
                sMat = sMat + Space(lLongCod - Len(material(i - 1))) + material(i - 1)
            Next
        End If

        sCodArticuloID = Me.ClientID & "_" & hidArticulo.ID
        sDenArticuloID = Me.ClientID & "_" & txtArticulo.ID
        sMaterialBD = Me.ClientID & "_" & hidMaterial.ID
        sMaterialDen = Me.ClientID & "_" & txtMaterial.ID
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Buscador_articulos", "var newWindow = window.open('" & ConfigurationManager.AppSettings("rutaFS") & "_common/BuscadorArticulos.aspx?desde=VisorContratos&IdMaterialBD=" & sMaterialBD & "&IdMaterialDen=" & sMaterialDen & "&IdControlCodArt=" & sCodArticuloID & "&IdControlDenArt=" & sDenArticuloID & "&TabContainer=" & Me.ID & "&mat=" & sMat & "&SessionId=" & HttpContext.Current.Session("sSession") & "', '_blank', 'width=850,height=630,status=yes,resizable=no,top=200,left=200');", True)
    End Sub

    ''' <summary>
    ''' Nos indica si un nodo esta dentro de una lista o no
    ''' </summary>
    ''' <param name="listaElementos">Cadena de nodos seleccionada</param>
    ''' <param name="valorABuscar">Valor del nodo a buscar en la lista</param>        
    ''' <returns>True o False si a enontrado o no el nodo</returns>
    ''' <remarks>Llamada desde=SyncChanges//CargarCertificados; Tiempo máximo=0,2seg.</remarks>
    Private Function buscarElemento(ByVal listaElementos As String, ByVal valorABuscar As String) As Boolean
        Dim pos As Integer
        Dim elementosSeleccionados() As String
        Dim bEncontrado As Boolean = False
        pos = InStr(listaElementos, valorABuscar)
        If pos > 0 Then
            If Len(listaElementos) = Len(valorABuscar) Then 'El nodo coincide con la seleccion
                Return True
            Else
                If pos - 1 > 0 Then
                    bEncontrado = False
                    elementosSeleccionados = System.Text.RegularExpressions.Regex.Split(listaElementos, "[\s$][\s$][\s$]")
                    For i = 0 To UBound(elementosSeleccionados)
                        If elementosSeleccionados(i) = valorABuscar Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next
                    Return bEncontrado
                Else
                    Return True

                End If

            End If
        Else
            Return False
        End If
    End Function

    ''' Revisado por: blp. Fecha: 29/08/2013
    ''' <summary>
    ''' Registra el script con las funciones para usar con paneles de búsqueda construidos con jQuery
    ''' </summary>
    ''' <remarks>LLamada desde el Page_Load; Tiempo máximo: 0,5 sg</remarks>
    Private Sub CargarScriptBuscadoresJQuery()
        Dim oScript As HtmlGenericControl
        oScript = pag.CargarBuscadoresJQuery(TiposDeDatos.Aplicaciones.PM)
        'Cuando todo se hace en CargarScriptPeticionarios, se devuelve un script "EMPTY" y no hay nada más que hacer. Si no, hay que insertar en la página el script q se recibe
        If oScript IsNot Nothing AndAlso oScript.TagName = "script" Then
            'pag.ScriptPeticionarios = oScript
            pag.RegistrarScript(oScript, "ScriptBuscadoresJQuery")
        End If
    End Sub

    ''' <summary>
    ''' Carga los Contratos en el panel de Contratos. 
    ''' </summary>
    ''' <remarks>LLamada desde el Page_Load; Tiempo máximo: 0,5 sg</remarks>
    Private Sub CargarPanelPartidas()
        '1. coger las partidas (pres0) que se ha pasado como parámetro hay en la caché (o llamada a la función) de las combos
        Dim dtPartidasPres As DataTable = getPartidas(pag.Usuario.Idioma)
        '2. Para cada partida, crear un label con el texto del punto 1, un textbox readonly y un botón para abrir el panel
        'Dim arrInputPartidas As String()
        If dtPartidasPres.Rows.Count > 0 Then
            'Dim TablaAsp As New Table
            Dim FilaAsp As New HtmlTableRow
            Dim Celda1Asp As New HtmlTableCell
            Dim Celda2Asp As New HtmlTableCell
            Dim i As Integer = 0
            For Each PartidaPres As DataRow In dtPartidasPres.Rows
                'Inserta una serie de controles para cada una de las partidas dentro del control PlaceHolder->phrPartidasPresBuscador
                Celda1Asp = New HtmlTableCell
                Celda2Asp = New HtmlTableCell
                If i > 0 Then
                    FilaAsp = New HtmlTableRow
                End If
                Celda1Asp.Style.Add("padding-bottom", "3px")
                Celda1Asp.Width = "185px"
                Celda2Asp.Width = "310px"
                Celda1Asp.Style.Add("text-align", "left")

                Dim tbPartida As New TextBox
                Dim tbPartidaHidden As New HiddenField
                Dim lblPartida As New Label()
                Dim imgPartida As New HtmlControls.HtmlImage
                lblPartida.Text = PartidaPres("PARTIDA_DEN") & ":"
                lblPartida.CssClass = "Etiqueta"
                tbPartida.ID = "tbPPres_" & PartidaPres("PRES0")
                tbPartida.Style.Add("float", "left")
                tbPartida.Style.Add("height", "16px")
                tbPartida.Style.Add("display", "Table-row")

                tbPartida.ClientIDMode = UI.ClientIDMode.Static
                tbPartidaHidden.ID = "tbPPres_Hidden_" & PartidaPres("PRES0")
                'tbPartida.Attributes.Add("onKeyDown", "setSelectedPRES0('" & PartidaPres("PRES0") & "'); limpiarPanelPartidas('" & listadoPartidas.ClientID & "'); abrirPanelPartidas('" & mpePartidas.ClientID & "', '" & listadoPartidas.ClientID & "', '" & PartidaPres("PRES0") & "', '" & PartidaPres("PARTIDA_DEN") & "', '" & lblPartidaPres0.ClientID & "', '" & "lblFiltroPartida" & PartidaPres("PRES0") & "', '" & pag.Textos(99) & "', '" & listadoInputsFiltro.ClientID & "', event, '" & tbPartida.ClientID & "', '" & tbPartidaHidden.ClientID & "','PM');")
                tbPartida.Attributes.Add("onKeyDown", "abrirPanelPartidas('" & mpePartidas.ClientID & "', '" & listadoPartidas.ClientID & "', '" & PartidaPres("PRES0") & "', '" & PartidaPres("PARTIDA_DEN") & "', '" & lblPartidaPres0.ClientID & "', '" & "lblFiltroPartida" & PartidaPres("PRES0") & "', '" & pag.Textos(99) & "', '" & listadoInputsFiltro.ClientID & "', event, '" & tbPartida.ClientID & "', '" & tbPartidaHidden.ClientID & "','PM');")
                tbPartidaHidden.ClientIDMode = UI.ClientIDMode.Static
                imgPartida.ID = "imgPPres_" & PartidaPres("PRES0")
                imgPartida.Src = "../../../Images/abrir_ptos_susp.gif"
                imgPartida.Style.Add("border", "0px")
                'imgPartida.Attributes.Add("onClick", "setSelectedPRES0('" & PartidaPres("PRES0") & "'); limpiarPanelPartidas('" & listadoPartidas.ClientID & "'); abrirPanelPartidas('" & mpePartidas.ClientID & "', '" & listadoPartidas.ClientID & "', '" & PartidaPres("PRES0") & "', '" & PartidaPres("PARTIDA_DEN") & "', '" & lblPartidaPres0.ClientID & "', '" & "lblFiltroPartida" & PartidaPres("PRES0") & "', '" & pag.Textos(99) & "', '" & listadoInputsFiltro.ClientID & "', event, null, null,'PM');")
                imgPartida.Attributes.Add("onClick", "abrirPanelPartidas('" & mpePartidas.ClientID & "', '" & listadoPartidas.ClientID & "', '" & PartidaPres("PRES0") & "', '" & PartidaPres("PARTIDA_DEN") & "', '" & lblPartidaPres0.ClientID & "', '" & "lblFiltroPartida" & PartidaPres("PRES0") & "', '" & pag.Textos(99) & "', '" & listadoInputsFiltro.ClientID & "', event, null, null,'PM');")
                imgPartida.Style.Add("cursor", "pointer")
                imgPartida.Style.Add("margin-left", "2px")
                tbPartida.Width = Unit.Pixel(wddEstado.Width.Value - 22)
                tbPartida.CssClass = "Normal"

                Celda1Asp.Controls.Add(lblPartida)
                Celda2Asp.Controls.Add(tbPartida)
                Celda2Asp.Controls.Add(tbPartidaHidden)
                Celda2Asp.Controls.Add(imgPartida)
                If i > 0 Then
                    FilaAsp.ID = "trPPres_" & PartidaPres("PRES0")
                    FilaAsp.Cells.Add(New HtmlTableCell)
                    FilaAsp.Cells.Add(New HtmlTableCell)
                    FilaAsp.Cells.Add(Celda1Asp)
                    FilaAsp.Cells.Add(Celda2Asp)
                    FilaAsp.Cells.Add(New HtmlTableCell)
                    FilaAsp.Cells.Add(New HtmlTableCell)
                    marco.Rows.Add(FilaAsp)
                Else
                    CentroPartidas.Cells(2).Visible = False
                    CentroPartidas.Cells.Add(Celda1Asp)
                    CentroPartidas.Cells.Add(Celda2Asp)
                End If
                i += 1

                Dim DivOculto As New HtmlControls.HtmlGenericControl("DIV")

                DivOculto.ID = PartidaPres("PRES0")
                DivOculto.Attributes.Add("style", "display:inline;width:100%;height:400px;overflow:auto;border-top:solid 1px #909090;margin-top:10px;")
                listadoPartidas.Controls.Add(DivOculto)

                Dim TablaAsp As New Table
                TablaAsp.Attributes.Add("style", "float:left;")
                Dim FilaTablaHtml As New TableRow
                Dim CeldaTablaHtml0 As New TableCell
                Dim CeldaTablaHtml1 As New TableCell
                Dim CeldaTablaHtml2 As New TableCell
                CeldaTablaHtml0.Style.Add("text-align", "left")
                CeldaTablaHtml1.Style.Add("text-align", "left")
                CeldaTablaHtml2.Style.Add("text-align", "left")

                Dim lblFiltroPartida As New Label
                lblFiltroPartida.ID = "lblFiltroPartida" & PartidaPres("PRES0")
                lblFiltroPartida.Text = "Contratol:" '(Mín. 4 caracteres)
                lblFiltroPartida.CssClass = "Rotulo"

                Dim TxtPartida As New TextBox
                TxtPartida.Attributes.Add("class", "Normal")
                TxtPartida.Attributes.Add("style", "width:300px; display:inline;")
                TxtPartida.ID = "FiltroPartidas" & PartidaPres("PRES0")
                TxtPartida.Attributes.Add("onkeyup", "setSelectedPRES0('" & PartidaPres("PRES0") & "'); cargarDatos('" & TiposDeDatos.ControlesBusquedaJQuery.PartidaPres & "', '" & listadoPartidas.ClientID & "', '" & listadoInputsFiltro.ClientID & "', '" & TiposDeDatos.AplicacionesEnNumero.PM & "');")
                'En jquery paste: TxtPartida.Attributes.Add("onpaste", "function () { setSelectedPRES0('" & PartidaPres("PRES0") & "'); setTimeout(function () { cargarDatos('" & TiposDeDatos.ControlesBusquedaJQuery.PartidaPres & "', '" & listadoPartidas.ClientID & "', '" & listadoInputsFiltro.ClientID & "', '" & TiposDeDatos.AplicacionesEnNumero.PM & "');}, 100);}")

                Dim lblminCharPartidas As New Label
                lblminCharPartidas.ID = "minCharPartidas" & PartidaPres("PRES0")
                lblminCharPartidas.Text = pag.Textos(101) '(Mín. 4 caracteres)

                CeldaTablaHtml0.Controls.Add(lblFiltroPartida)
                CeldaTablaHtml1.Controls.Add(TxtPartida)
                CeldaTablaHtml2.Controls.Add(lblminCharPartidas)

                FilaTablaHtml.Cells.Add(CeldaTablaHtml0)
                FilaTablaHtml.Cells.Add(CeldaTablaHtml1)
                FilaTablaHtml.Cells.Add(CeldaTablaHtml2)

                TablaAsp.Rows.Add(FilaTablaHtml)

                listadoInputsFiltro.Controls.Add(TablaAsp)
            Next
        End If

    End Sub

    ''' <summary>
    ''' Carga los centros en el panel de centros. 
    ''' </summary>
    ''' <remarks>LLamada desde el Page_Load; Tiempo máximo: 0,5 sg</remarks>
    Private Sub CargarPanelCentros()
        Dim sScript As String = "cargarDatos('" & TiposDeDatos.ControlesBusquedaJQuery.CentroCoste & "', '" & listadoCentros.ClientID & "', '" & FiltroCentros.ClientID & "', '" & TiposDeDatos.AplicacionesEnNumero.PM & "');"
        imgPanelCentros.Attributes.Add("onclick", sScript & "abrirPanelCentros('" & mpeCentros.ClientID & "', '" & FiltroCentros.ClientID & "', event, 'tbCtroCoste', 'tbCtroCoste_Hidden')")

        'Vaciamos el contenido de FiltroCentros para asegurarnos de que al cambiar su contenido no mezclamos en la función de js CargarDatos textos de dos búsquedas distintas
        tbCtroCoste.Attributes.Add("onKeyDown", sScript & "abrirPanelCentros('" & mpeCentros.ClientID & "','" & FiltroCentros.ClientID & "', event, '" & tbCtroCoste.ClientID & "', '" & tbCtroCoste_Hidden.ClientID & "');")

        'FiltroCentros.Attributes.Add("onkeyup", sScript)
        FiltroCentros.Attributes.Add("autocomplete", "off") 'Esto impide la función autocompletar de FF. Si se usase, se controlaría mal la carga de Centros.
    End Sub

    ''' Revisado por: blp. Fecha: 29/08/2013
    ''' <summary>
    ''' Carga los peticionarios en el panel de peticionarios. 
    ''' </summary>
    ''' <remarks>LLamada desde el Page_Load; Tiempo máximo: 0,5 sg</remarks>
    Private Sub CargarPanelPeticionarios()
        'Cargar el filtrado cada vez que escribe
        Dim sScript As String = "cargarDatosPeticionario('" & TiposDeDatos.ControlesBusquedaJQuery.Peticionario & "', '" & listadoPeticionarios.ClientID & "', '" & FiltroPeticionarios.ClientID & "', '" & TiposDeDatos.AplicacionesEnNumero.PM & "');"
        imgPanelPeticionarios.Attributes.Add("onclick", "abrirPanelPeticionarios('" & mpePeticionarios.ClientID & "', '" & FiltroPeticionarios.ClientID & "', event, '" & txtPeticionario.ClientID & "', '" & txtPeticionario_Hidden.ClientID & "', false, '" & listadoPeticionarios.ClientID & "')")
        'Vaciamos el contenido de FiltroPeticionarios para asegurarnos de que al cambiar su contenido no mezclamos en la función de js CargarDatos textos de dos búsquedas distintas
        txtPeticionario.Attributes.Add("onKeyDown", "limpiarPanelPeticionarios('" & FiltroPeticionarios.ClientID & "','" & listadoPeticionarios.ClientID & "'); abrirPanelPeticionarios('" & mpePeticionarios.ClientID & "','" & FiltroPeticionarios.ClientID & "', event, '" & txtPeticionario.ClientID & "', '" & txtPeticionario_Hidden.ClientID & "', true,'" & listadoPeticionarios.ClientID & "');")
        FiltroPeticionarios.Attributes.Add("onkeyup", sScript)
        FiltroPeticionarios.Attributes.Add("autocomplete", "off") 'Esto impide la función autocompletar de FF. Si se usase, se controlaría mal la carga de peticionarios.
    End Sub

    ''' <summary>
    ''' Proceso que carga el combo de Tipo con los tipos de las solicitudes
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,1seg.</remarks>
    Private Sub CargarTiposSolicitudes()
        Dim FSWSServer As FSNServer.Root
        FSWSServer = Session("FSN_Server")

        Dim FSNUser As FSNServer.User
        FSNUser = Session("FSN_User")
        Dim oSolicitudes As FSNServer.Solicitudes = FSWSServer.Get_Object(GetType(FSNServer.Solicitudes))

        wddTipo.TextField = "DEN"
        wddTipo.ValueField = "ID"
        wddTipo.DataSource = oSolicitudes.LoadTiposSolicitudes(FSNUser.CodPersona, FSNUser.Idioma, TiposDeDatos.TipoDeSolicitud.Contrato)
        wddTipo.DataBind()

        wddTipo.Items.RemoveAt(0)
        wddTipo.CurrentValue = "" 'para que aparezca el webdropdown vacío. Si no, aparece la 1ª opción

        wddTipo.EnableAutoFiltering = ListControls.AutoFiltering.Client
        wddTipo.AutoFilterQueryType = ListControls.AutoFilterQueryTypes.Contains
        wddTipo.AutoFilterSortOrder = ListControls.DropDownAutoFilterSortOrder.Ascending
    End Sub

    ''' <summary>
    ''' Carga los distintos tipos de estados
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarEstados()
        pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorContratos

        Dim oItem As Infragistics.Web.UI.ListControls.DropDownItem
        wddEstado.Items.Clear()

        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 0
        oItem.Text = pag.Textos(0)
        wddEstado.Items.Add(oItem)

        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 2
        oItem.Text = pag.Textos(1)
        wddEstado.Items.Add(oItem)

        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 3
        oItem.Text = pag.Textos(2)
        wddEstado.Items.Add(oItem)

        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 4
        oItem.Text = pag.Textos(3)
        wddEstado.Items.Add(oItem)

        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 5
        oItem.Text = pag.Textos(4)
        wddEstado.Items.Add(oItem)

        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 6
        oItem.Text = pag.Textos(5)
        wddEstado.Items.Add(oItem)

        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 7
        oItem.Text = pag.Textos(6)
        wddEstado.Items.Add(oItem)


        wddEstado.EnableAutoFiltering = Infragistics.Web.UI.ListControls.AutoFiltering.Client
        wddEstado.AutoFilterQueryType = Infragistics.Web.UI.ListControls.AutoFilterQueryTypes.Contains
        wddEstado.AutoFilterSortOrder = Infragistics.Web.UI.ListControls.DropDownAutoFilterSortOrder.Ascending
    End Sub

    ''' <summary>
    ''' Carga los distintos periodos
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarPeriodos()
        pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorContratos

        Dim oItem As Infragistics.Web.UI.ListControls.DropDownItem
        wddPeriodo.Items.Clear()

        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 1
        oItem.Text = pag.Textos(106)
        wddPeriodo.Items.Add(oItem)

        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 2
        oItem.Text = pag.Textos(107)
        wddPeriodo.Items.Add(oItem)

        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 3
        oItem.Text = pag.Textos(108)
        wddPeriodo.Items.Add(oItem)

        wddPeriodo.EnableAutoFiltering = Infragistics.Web.UI.ListControls.AutoFiltering.Client
        wddPeriodo.AutoFilterQueryType = Infragistics.Web.UI.ListControls.AutoFilterQueryTypes.Contains
        wddPeriodo.AutoFilterSortOrder = Infragistics.Web.UI.ListControls.DropDownAutoFilterSortOrder.Ascending
    End Sub


    ''' Revisado por: blp. Fecha: 20/08/2013
    ''' <summary>
    ''' Vamos a obtener la información nacesaria para los dropdown desde el stored pero sin recuperar ni almacenar en cache el resto de información (órdenes, lineas e imputación)
    ''' </summary>
    ''' <param name="Idioma">idioma en el que devolver el dataset</param>
    ''' <remarks>Llamada desde wucBusquedaAvanzadaContratos.ascx.vb. Máx. 0,2 seg.</remarks>
    Public Function getPartidas(ByVal Idioma As FSNLibrary.Idioma) As DataTable
        Dim oPRES5 As FSNServer.PRES5 = pag.FSNServer.Get_Object(GetType(FSNServer.PRES5))
        Dim dtPartidasPres0 As New DataTable
        If HttpContext.Current.Cache("DsetPartidasPres0_" & "_" & Idioma.ToString()) Is Nothing Then
            dtPartidasPres0 = oPRES5.DevolverPartidasPres0PM(Idioma)
            pag.InsertarEnCache("DsetPartidasPres0_" & "_" & Idioma.ToString(), dtPartidasPres0)
        Else
            dtPartidasPres0 = HttpContext.Current.Cache("DsetPartidasPres0_" & "_" & Idioma.ToString())
        End If
        Return dtPartidasPres0
    End Function

    ''' <summary>
    ''' Recoge los valores seleccionados en los filtros de las partidas y los agrupa en un array
    ''' </summary>
    ''' <returns>Array con las partidas</returns>
    ''' <remarks>Llamada desde wucBusquedaAvanzadaContratos. Max. 0,1 seg.</remarks>
    Public Function arrPartidasPres() As String()
        Dim sPartidasPres As String()
        For Each fila As Control In marco.Rows
            For Each celda As Control In CType(fila, HtmlTableRow).Cells
                For Each tb As Control In CType(celda, HtmlTableCell).Controls
                    If TypeOf tb Is TextBox AndAlso tb.ID.StartsWith("tbPPres_") Then
                        Dim tbPartida As TextBox = CType(tb, System.Web.UI.WebControls.TextBox)
                        If Not String.IsNullOrEmpty(tbPartida.Text) Then
                            Dim sPartidaHiddenID As String = tbPartida.ID.Replace("tbPPres_", "tbPPres_Hidden_")
                            Dim tbPartidaHidden As HiddenField = CType(CType(celda, HtmlTableCell).FindControl(sPartidaHiddenID), System.Web.UI.WebControls.HiddenField)
                            Dim sPRES0 As String = (tb.ID).Replace("tbPPres_", "")
                            If tbPartidaHidden IsNot Nothing AndAlso Not String.IsNullOrEmpty(tbPartidaHidden.Value) Then
                                If sPartidasPres Is Nothing Then
                                    ReDim Preserve sPartidasPres(0)
                                Else
                                    ReDim Preserve sPartidasPres(sPartidasPres.Length)
                                End If
                                sPartidasPres(sPartidasPres.Length - 1) = tbPartidaHidden.Value
                            End If
                        End If
                    ElseIf TypeOf tb Is System.Web.UI.WebControls.Label Then
                        For Each tb2 As Control In CType(tb, Label).Controls
                            If TypeOf tb2 Is System.Web.UI.WebControls.TextBox AndAlso tb2.ID.StartsWith("tbPPres_") Then
                                Dim tbPartida As TextBox = CType(tb2, System.Web.UI.WebControls.TextBox)
                                If Not String.IsNullOrEmpty(tbPartida.Text) Then
                                    Dim sPartidaHiddenID As String = tbPartida.ID.Replace("tbPPres_", "tbPPres_Hidden_")
                                    Dim tbPartidaHidden As HiddenField = CType(CType(tb, Label).FindControl(sPartidaHiddenID), System.Web.UI.WebControls.HiddenField)
                                    Dim sPRES0 As String = (tb2.ID).Replace("tbPPres_", "")
                                    If tbPartidaHidden IsNot Nothing AndAlso Not String.IsNullOrEmpty(tbPartidaHidden.Value) Then
                                        If sPartidasPres Is Nothing Then
                                            ReDim Preserve sPartidasPres(0)
                                        Else
                                            ReDim Preserve sPartidasPres(sPartidasPres.Length)
                                        End If
                                        sPartidasPres(sPartidasPres.Length - 1) = tbPartidaHidden.Value
                                    End If
                                End If
                            End If
                        Next
                    End If
                Next
            Next
        Next
        Return sPartidasPres

    End Function

    ''' Revisado por: blp. fecha: 28/08/2013
    ''' <summary>
    ''' Carga las partidas de los filtros en los cuadros de texto de las partidas
    ''' </summary>
    ''' <remarks>Llamada desde wucBusquedaAvanzadaContratos. Max. 0,1 seg.</remarks>
    Public Sub CargarPartidasPres(ByVal sPartidasPres As String())
        Dim sPartidasPresTemp As New ArrayList
        If sPartidasPres IsNot Nothing Then
            For Each sPartida As String In sPartidasPres
                sPartidasPresTemp.Add(sPartida)
            Next
        End If
        For Each fila As Control In marco.Rows
            For Each celda As Control In CType(fila, HtmlTableRow).Cells
                For Each tb As Control In CType(celda, HtmlTableCell).Controls
                    If TypeOf tb Is TextBox AndAlso tb.ID.StartsWith("tbPPres_") Then
                        Dim tbPartida As TextBox = CType(tb, System.Web.UI.WebControls.TextBox)
                        Dim sPartidaHiddenID As String = tbPartida.ID.Replace("tbPPres_", "tbPPres_Hidden_")
                        Dim tbPartidaHidden As HiddenField = CType(CType(celda, HtmlTableCell).FindControl(sPartidaHiddenID), System.Web.UI.WebControls.HiddenField)
                        Dim sPRES0 As String = (tb.ID).Replace("tbPPres_", "")
                        If tbPartidaHidden IsNot Nothing Then
                            If sPartidasPresTemp.Count > 0 Then
                                For Each sPartidaPres As String In sPartidasPresTemp
                                    Dim sPRES0Temp As String = sPartidaPres.Substring(0, sPartidaPres.IndexOf("#"))
                                    If sPRES0 = sPRES0Temp Then
                                        'Cargar el texto de la partida en el cuadro de texto
                                        Dim oPRES5 As FSNServer.PRES5 = pag.FSNServer.Get_Object(GetType(FSNServer.PRES5))
                                        Dim dtPartidaPres As DataTable = oPRES5.DevolverPartidaPM(sPartidaPres, pag.Usuario.Idioma)
                                        'Si hay datos, podemos guardarlo en el cuadro de búsqueda
                                        If dtPartidaPres IsNot Nothing AndAlso dtPartidaPres.Rows.Count > 0 Then
                                            tbPartida.Text = dtPartidaPres.Rows(0).Item("PRES0") & "-" & dtPartidaPres.Rows(0).Item("PRESID") & " " & dtPartidaPres.Rows(0).Item("PRESDEN")
                                            'Guardar la partida en el hidden
                                            tbPartidaHidden.Value = sPartidaPres
                                        End If
                                        'Eliminar del temp la partida
                                        sPartidasPresTemp.Remove(sPartidaPres)
                                        Exit For
                                    End If
                                Next
                            Else
                                tbPartida.Text = String.Empty
                                tbPartidaHidden.Value = String.Empty
                            End If
                        End If
                    ElseIf TypeOf tb Is System.Web.UI.WebControls.Label Then
                        For Each tb2 As Control In CType(tb, Label).Controls
                            If TypeOf tb2 Is System.Web.UI.WebControls.TextBox AndAlso tb.ID.StartsWith("tbPPres_") Then
                                Dim tbPartida As TextBox = CType(tb2, System.Web.UI.WebControls.TextBox)
                                Dim sPartidaHiddenID As String = tbPartida.ID.Replace("tbPPres_", "tbPPres_Hidden_")
                                Dim tbPartidaHidden As HiddenField = CType(CType(tb, Label).FindControl(sPartidaHiddenID), System.Web.UI.WebControls.HiddenField)
                                Dim sPRES0 As String = (tb2.ID).Replace("tbPPres_", "")
                                If tbPartidaHidden IsNot Nothing Then
                                    If sPartidasPresTemp.Count > 0 Then
                                        For Each sPartidaPres As String In sPartidasPresTemp
                                            Dim sPRES0Temp As String = sPartidaPres.Substring(0, sPartidaPres.IndexOf("#"))
                                            If sPRES0 = sPRES0Temp Then
                                                'Cargar el texto de la partida en el cuadro de texto
                                                Dim oPRES5 As FSNServer.PRES5 = pag.FSNServer.Get_Object(GetType(FSNServer.PRES5))
                                                Dim dtPartidaPres As DataTable = oPRES5.DevolverPartidaPM(sPartidaPres, pag.Usuario.Idioma)
                                                'Si hay datos, podemos guardarlo en el cuadro de búsqueda
                                                If dtPartidaPres IsNot Nothing AndAlso dtPartidaPres.Rows.Count > 0 Then
                                                    tbPartida.Text = dtPartidaPres.Rows(0).Item("PRES0") & "-" & dtPartidaPres.Rows(0).Item("PRESID") & " " & dtPartidaPres.Rows(0).Item("PRESDEN")
                                                    'Guardar la partida en el hidden
                                                    tbPartidaHidden.Value = sPartidaPres
                                                End If
                                                'Eliminar del temp la partida
                                                sPartidasPresTemp.Remove(sPartidaPres)
                                                Exit For
                                            End If
                                        Next
                                    Else
                                        tbPartida.Text = String.Empty
                                        tbPartidaHidden.Value = String.Empty
                                    End If
                                End If
                            End If
                        Next
                    End If
                Next
            Next
        Next
    End Sub

    ''' Revisado por: blp. fecha: 28/08/2013
    ''' <summary>
    ''' Carga los peticionarios de los filtros en el cuadro de texto de peticionario
    ''' </summary>
    ''' <param name="codPeticionarios">Código de los peticionarios separados por $$$</param>
    ''' <remarks>Llamada desde wucBusquedaAvanzadaContratos. Max. 0,1 seg.</remarks>
    Private Sub cargarPeticionarios(ByVal codPeticionarios As String)
        If codPeticionarios <> String.Empty Then
            'CARGAMOS LOS DATOS DE LOS PETICIONARIOS FILTRADOS EN EL TXTPETICIONARIOS
            'Dim sCodPeticionarios As String() = value.Split({"$$$"}, System.StringSplitOptions.None)
            Dim dtCodPeticionarios = pag.CrearTBCADENATEXTO()
            dtCodPeticionarios = pag.CargarTablaStrings(codPeticionarios, "$$$")
            Dim cRol As FSNServer.Rol = pag.FSNServer.Get_Object(GetType(FSNServer.Rol))
            Dim dtPeticionarios As DataTable = Nothing
            dtPeticionarios = cRol.CargarPeticionarios_filtrado(dtCodPeticionarios)
            If dtPeticionarios IsNot Nothing Then
                For Each Peticionario As DataRow In dtPeticionarios.Rows
                    If txtPeticionario.Text <> "" Then
                        txtPeticionario.Text += ", "
                    End If
                    txtPeticionario.Text += Peticionario("COD") & " - " & Peticionario("DEN")
                    If dtPeticionarios.Rows.Count > 1 Then txtPeticionario.Height = Unit.Pixel(txtPeticionario.Height.Value * dtPeticionarios.Rows.Count) '23
                Next
                txtPeticionario_Hidden.Value = codPeticionarios
            Else
                txtPeticionario.Text = String.Empty
                txtPeticionario_Hidden.Value = String.Empty
            End If
        Else
            txtPeticionario.Text = String.Empty
            txtPeticionario_Hidden.Value = String.Empty
        End If
    End Sub

    ''' Revisado por: blp. fecha: 28/08/2013
    ''' <summary>
    ''' Carga el centro de coste de los filtros en el cuadro de texto de centro de coste
    ''' </summary>
    ''' <param name="CodCentroCoste">UONS del centro de coste a cargar. Estructura: UON1#UON2#UON3#UON4</param>
    ''' <remarks>Llamada desde wucBusquedaAvanzadaContratos. Max. 0,1 seg.</remarks>
    Private Sub cargarCentroCoste(ByVal CodCentroCoste As String)
        If CodCentroCoste <> String.Empty Then
            'CARGAMOS LOS DATOS DEL CENTRO DEL FILTRO EN EL TBCTROCOSTE
            Dim oCentro_SM As FSNServer.Centro_SM = pag.FSNServer.Get_Object(GetType(FSNServer.Centro_SM))
            Dim dtCentro As DataTable = oCentro_SM.DevolverCentroPM(CodCentroCoste, pag.Usuario.Idioma)
            If dtCentro IsNot Nothing AndAlso dtCentro.Rows.Count > 0 Then
                tbCtroCoste_Hidden.Value = CodCentroCoste
                tbCtroCoste.Text = dtCentro.Rows(0).Item("CCUON") & " - " & dtCentro.Rows(0).Item("CCDEN")
            Else
                tbCtroCoste_Hidden.Value = String.Empty
                tbCtroCoste.Text = String.Empty
            End If
        Else
            tbCtroCoste_Hidden.Value = String.Empty
            tbCtroCoste.Text = String.Empty
        End If
    End Sub

    ''' <summary>
    ''' Carga el centro de coste de los filtros en el cuadro de texto de notificado
    ''' </summary>
    ''' <param name="CodNotif">ciodigo persona</param>
    ''' <remarks>Llamada desde wucBusquedaAvanzadaContratos. Max. 0,1 seg.</remarks>
    Private Sub cargarNotificado(ByVal CodNotif As String)
        If CodNotif <> String.Empty Then
            'CARGAMOS LOS DATOS DEL usuario DEL FILTRO EN EL 
            Dim oPer As FSNServer.Persona = pag.FSNServer.Get_Object(GetType(FSNServer.Persona))
            oPer.LoadData(CodNotif)

            txtNotificados.Text = oPer.Nombre & "(" & oPer.EMail & ")"
            hidNotificados.Value = CodNotif

        Else
            hidNotificados.Value = String.Empty
            txtNotificados.Text = String.Empty
        End If
    End Sub

End Class