﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="wucPartidas_2.ascx.vb" Inherits="Fullstep.FSNWeb.wucPartidas" %>
<script language="javascript">
    function FindClicked(txtbxID, treeID) {
        // Get the TextBox that has the Regular Expression
        var txtbx = document.getElementById(txtbxID);

        // Set Regular Expression variable
        var regex = txtbx.value;
        regex = regex.toLowerCase();

        // Get a reference to the WebTree object
        var tree = $find("Partidas_wdtCentrosPartidas");
        
        // Call the Find function
        var node = find(tree, regex);

        // Set the selected node on the tree to the node that was returned by the function
        if (node) {
            node.set_selected(true);
            node.get_element().scrollIntoView()
            //node.scrollIntoView(true);
        }

        var node = findNext(tree, regex);
        var imgNext = document.getElementById("<%=imgSiguiente.ClientID%>");
        if (node) {
            imgNext.style.visibility = 'visible';
        } else {
            imgNext.style.visibility = 'hidden';
        }
        
        // Reset objects
        tree = null;
        node = null;
        regex = null;
        txtbx = null;
    }
    function find(tree, regex) {
        // Call the findInCollection method on the tree's nodes
        var node = findInCollection(tree.getNodes(), regex);

        // Return the node that was found
        return node;
    }

    function FindNextClicked(txtbxID, treeID) {
        // Get the TextBox that has the Regular Expression
        var txtbx = document.getElementById(txtbxID);

        // Set Regular Expression variable
        var regex = txtbx.value;

        // Get a reference to the WebTree object
        var tree = $find("Partidas_wdtCentrosPartidas");

        // Call FindNext function
        var node = findNext(tree, regex);
        if (node) {
            node.set_selected(true);
            node.get_element().scrollIntoView();
        }

        var node = findNext(tree, regex);
        var imgNext = document.getElementById("<%=imgSiguiente.ClientID%>");
        if (node) {
            imgNext.style.visibility = 'visible';
        } else {
            imgNext.style.visibility = 'hidden';
        }

        // Reset objects
        tree = null;
        node = null;
        regex = null;
        txtbx = null;
    }
    
    function findNext(tree, regex) {
        // Get the currently selected node
        var selectedNode = tree.get_selectedNodes()[0];

        // Create a node object, and initialize it to null
        var node = null;

        // If the selected node is null it means that there were no previously selected nodes on this tree,
        // so run the find function on the tree
        if (selectedNode == null) {
            node = findInCollection(tree.getNodes(), regex);

            // Reset objects
            tree = null;
            selectedNode = null;

            // Return the node that was found
            return node;
        }

        // If the selected node has children, run the findInCollection function in its collection
        if (selectedNode.hasChildren()) {
            node = findInCollection(selectedNode.getItems(), regex);

            // If the node we're looking for is found, wrap up
            if (node != null) {
                // Reset objects
                tree = null;
                selectedNode = null;

                // Return the node that was found
                return node;
            }
        }        
    }

    function findInNextSiblings(node, regex) {
        // Create an empty array object of next siblings
        var nextSibCollection = new Array();

        // Create a sibling count variable, initialized to 0
        var sibCount = 0;

        // While the node has a next sibling, add the sibling to the sibling array
        nextSibCollection[sibCount++] = node.getNextSibling();

        while (nextSibCollection[sibCount - 1].getNextSibling() != null)
            nextSibCollection[sibCount++] = nextSibCollection[sibCount - 2].getNextSibling();

        // Run the findInCollection method on the array of collection of next siblings
        var node = findInCollection(nextSibCollection, regex);

        // Reset objects
        nextSibCollection = null;
        sibCount = null;

        // Return the node that was found
        return node;
    }

    function findInCollection(NodesCollection, regex) {
        // Loop through the Nodes collection
        for (var i = 0; i < NodesCollection.get_length(); i++) {
            // Get the next Node in collection
            var thisNode = NodesCollection.getNode(i);

                // Convert the Node's text to JavaScript String object
                var nodeStr = new String(thisNode.get_text());
                nodeStr = nodeStr.toLowerCase();
                
                // Check if the string matches the Regular Expression - if so, this is the Node we are looking for
                if ((thisNode.get_target() != null) && (nodeStr.match(regex)) && (regex != '')) {
                    // Reset objects
                    NodesCollection = null;
                    regex = null;
                    nodeStr = null;

                    // Return the node that was found
                    return thisNode;
                }
                // If this Node has child Nodes, search recursively through its children as well
                else if (thisNode.hasChildren()) {
                    // Clear the nodeStr variable
                    nodeStr = null;

                    // Get the value that is returned after the recursion has finished
                    var node = findInCollection(thisNode.getItems(), regex);

                    // If a Node was returned, pass it up stack as this is the node we are looking for
                    if (node != null) {
                        // Reset objects
                        NodesCollection = null;
                        regex = null;

                        // Return the node that was found
                        return node;
                    }
                }

                // Clear the nodeStr variable
                nodeStr = null;
        }

        // Reset objects
        NodesCollection = null;
        regex = null;
    }

    //se le llama desde el buscador de centros de coste
    function CentroCoste_seleccionado(IDControl, sUON1, sUON2, sUON3, sUON4, sDen) {
        var sValor = '';

        //lo que se guarda en valor_text
        if (sUON1)
            sValor = sUON1;
        if (sUON2)
            sValor = sValor + '#' + sUON2;
        if (sUON3)
            sValor = sValor + '#' + sUON3;
        if (sUON4)
            sValor = sValor + '#' + sUON4;

        document.getElementById("<%=hCentroCod.ClientID%>").value = sValor;
        document.getElementById("<%=txtCentro.ClientID%>").value = sDen;
    }
</script>

<style>
    .NodeSelected
        {
            background-color:Navy;
            color: white;
            padding-bottom:3px !important;
            padding-top:3px !important;
        }
</style>

<div>
    <table cellspacing="1" cellpadding="3" width="100%" border="0">
        <tr>
            <td valign="middle" colspan="5">
                <table cellpadding="4" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td width="65">
                            <img alt="" src="<%=ConfigurationManager.AppSettings("ruta")%>images/centrocoste.jpg" />
                        </td>
                        <td>
                            <asp:Label ID="lblTitulo" runat="server" CssClass="RotuloGrande"></asp:Label>
                        </td>
                        <td align="right" valign="top">
                            <asp:ImageButton ID="imgCerrar" runat="server" SkinID="Cerrar" OnClientClick="javascript:self.close()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblBuscar" runat="server" CssClass="Etiqueta"></asp:Label>
            </td>
            <td colspan="4">
                <asp:TextBox ID="txtBuscar" runat="server" Width="350px"></asp:TextBox>&nbsp;&nbsp;<asp:ImageButton
                    ID="imgSiguiente" runat="server" Style="visibility: hidden" />
                <ajx:AutoCompleteExtender ID="AutoCompleteExtender" runat="server" CompletionSetCount="10"
                    DelimiterCharacters="" Enabled="True" MinimumPrefixLength="3" ServiceMethod="GetPartidasYCentros"
                    ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" TargetControlID="txtBuscar"
                    EnableCaching="False">
                </ajx:AutoCompleteExtender>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblCentro" runat="server" CssClass="Etiqueta"></asp:Label>
            </td>
            <td colspan="3">
                <asp:TextBox ID="txtCentro" runat="server" Width="350px" ReadOnly></asp:TextBox>&nbsp;&nbsp;<asp:ImageButton
                    ID="imgBuscarCentro" runat="server" OnClientClick="AbrirBuscadorCentros();return false;" />
                <asp:Label ID="lblCentroCosteDen" runat="server" CssClass="Etiqueta"></asp:Label>
                <input type="hidden" name="hCentroCod" id="hCentroCod" runat="server" />
            </td>
            <td>
                <fsn:FSNButton ID="btnBuscar" runat="server" Alineacion="Left"></fsn:FSNButton>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblFechaInicioDesde" runat="server" CssClass="Etiqueta"></asp:Label>
            </td>
            <td>
                <igpck:WebDatePicker runat="server" ID="dteFechaInicioDesde" SkinID="Calendario">
                </igpck:WebDatePicker>
            </td>
            <td nowrap>
                <asp:Label ID="lblFechaHastaFin" runat="server" CssClass="Etiqueta"></asp:Label>
            </td>
            <td>
                <igpck:WebDatePicker runat="server" ID="dteFechaHastaFin" SkinID="Calendario">
                </igpck:WebDatePicker>
            </td>
            <td nowrap>
                <asp:CheckBox ID="chkPartidasNoVigentes" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <asp:UpdatePanel ID="upAlerta" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                    <ContentTemplate>
                        <div id="dCamposFiltro" class="dContenedor" style="height: 250px; width: 95%">
                            <ig:WebDataTree ID="wdtCentrosPartidas" runat="server" CssClass="igTree" SelectionType="Single" Width="90%">
                                <NodeSettings SelectedCssClass="NodeSelected" />
                                <DataBindings>
                                    <ig:DataTreeNodeBinding DataMember="Table" TextField="DEN" ValueField="COD" KeyField="COD" />
                                    <ig:DataTreeNodeBinding DataMember="Table1" TextField="DEN" ValueField="COD" KeyField="COD" />
                                    <ig:DataTreeNodeBinding DataMember="Table2" TextField="DEN" ValueField="COD" KeyField="COD" />
                                    <ig:DataTreeNodeBinding DataMember="Table3" TextField="DEN" ValueField="COD" KeyField="COD" />
                                    <ig:DataTreeNodeBinding DataMember="Table4" TextField="DEN" ValueField="COD" KeyField="COD" />
                                    <ig:DataTreeNodeBinding DataMember="Table5" TextField="DEN" ValueField="COD" KeyField="COD" />
                                    <ig:DataTreeNodeBinding DataMember="Table6" TextField="DEN" ValueField="COD" KeyField="COD" />
                                    <ig:DataTreeNodeBinding DataMember="Table7" TextField="DEN" ValueField="COD" KeyField="COD" />
                                    <ig:DataTreeNodeBinding DataMember="Table8" TextField="DEN" ValueField="COD" KeyField="COD" />
                                </DataBindings>
                            </ig:WebDataTree>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                <fsn:FSNButton ID="btnSeleccionar" runat="server" Alineacion="Right" OnClientClick="return seleccionarPartida();"></fsn:FSNButton>
            </td>
            <td colspan="3" align="left">
                <fsn:FSNButton ID="btnCancelar" runat="server" Alineacion="Left" OnClientClick="window.close()"></fsn:FSNButton>
            </td>
        </tr>
    </table>
</div>