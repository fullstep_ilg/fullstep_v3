﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SolicitudesFavoritas.aspx.vb" Inherits="Fullstep.FSNWeb.SolicitudesFavoritas" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
	<title></title>
</head>
<body style="background-color: #FFFFFF; margin-top: 0px; margin-left: 5px; margin-right: 0px">
	<script type="text/javascript">

			//Se ejecuta cuando se hace click sobre una celda del datagrid
			function WebDataGrid_CellSelectionChanged(sender, e) {

				var cell = e.getSelectedCells().getItem(0)

				var column = sender.get_columns().get_columnFromKey("ID");

				var IdSolicitudFav = e.getSelectedCells().getItem(0).get_row().get_cellByColumn(column).get_value();

				var column = sender.get_columns().get_columnFromKey("DEN");

				var DenSolicitudFav = e.getSelectedCells().getItem(0).get_row().get_cellByColumn(column).get_value();

				IdSolicitud = window.document.Form1.Solicitud.value
				
				


				//elimino la seleccion sobre la celda pulsada para si vuelve a pulsar salte el evento, si la celda sigue 
				//seleccionada no saltaria el evento al hacer click
				e.getSelectedCells().remove(e.getSelectedCells().getItem(0))

				if (cell.get_column()._key == "ELIMINAR") {
					if (confirm(document.forms["Form1"].elements["textoConfirmacion"].value)) {
						document.forms["Form1"].elements["IdSolicitudFavorita_Eliminar"].value = IdSolicitudFav
						document.forms["Form1"].submit();
						//Actualizar la grid de solicitudes.
						if (document.forms["Form1"].elements["nFavoritos"].value <= 2)
							window.opener.location.reload();
					}
				} else
					if (cell.get_column()._key == "DEN") {
						var rutaPM = '<%=ConfigurationManager.AppSettings("rutaPM")%>frames.aspx?pagina='
						window.opener.open(rutaPM + "alta/NWAlta.aspx?Solicitud=" + IdSolicitud.toString() + "*Favorito=1*IdFavorito=" + IdSolicitudFav.toString(), "_self")
						window.close();
					}
			   }
</script>
		<form id="Form1" method="post" runat="server">
			<asp:ScriptManager ID="ScriptMng" runat="server" EnablePageMethods="true" ></asp:ScriptManager>					
			<input type="hidden" id="IdSolicitudFavorita_Eliminar" name="IdSolicitudFavorita_Eliminar" value="0" runat="server" />
			<input type="hidden" id="Solicitud" name="Solicitud" runat="server" />
			<input type="hidden" id="textoConfirmacion" name="textoConfirmacion" runat="server" />
			<input type="hidden" id="nFavoritos" name="nFavoritos" runat="server" />
			<asp:Panel ID="pnlTitulo" runat="server" Width="100%">
			<table width="100%" cellpadding="0" border="0">
				<tr>
					<td>
						<fsn:FSNPageHeader ID="FSNPageHeader" runat="server" >
						</fsn:FSNPageHeader>
					</td>
				</tr>
			</table>
		</asp:Panel>
			<br />
			<TABLE id="Table1"	cellspacing="1" cellpadding="1" border="0">
				<TR>
					<TD>
						<asp:label id="lblTipoSolicitud" runat="server" CssClass="Etiqueta">Tipo de solicitud:</asp:label></TD>
					<TD>
						<asp:label id="lblSolicitud" runat="server" CssClass="Normal"></asp:label></TD>
				</TR>
			</TABLE>
			<br />
			<TABLE id="tblSolicitudes"	cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
				<TR>
					<TD colspan="2"><p><asp:label id="lblTexto1" runat="server" CssClass="Rotulo10">Dispone de distintas solicitudes preconfiguradas. Selleccione cual de ellas quiere utilizar para el alta.</asp:label></p></TD>
				</TR>
				<TR>
					<TD class="Cabecera Texto12" width="90%" style="HEIGHT: 20px"><asp:label id="lblCabeceraTabla" runat="server" Width="100%">Lista de solicitudes favoritas</asp:label></TD>
					<td width="10%">&nbsp;</td>
				</TR>
				<TR>
					<TD width="90%">
						<ig:WebDataGrid id="wgSolicitudesFavoritas"  runat="server" Width="100%" Height="150px" AutoGenerateColumns="false" ShowHeader="false" >
							<Behaviors>
								<ig:Selection RowSelectType="Single" CellClickAction="Cell"  CellSelectType="Single" Enabled="true" >
									<SelectionClientEvents 
									 CellSelectionChanged="WebDataGrid_CellSelectionChanged" />
								</ig:Selection>
							</Behaviors>
							<Columns>
								<ig:BoundDataField DataFieldName="ID" Key="ID" Hidden="true" ></ig:BoundDataField>
								<ig:BoundDataField DataFieldName="DEN" Key="DEN" Width="65%"></ig:BoundDataField>
								<ig:BoundDataField DataFieldName="FEC_CREACION" Key="FEC_CREACION" Width="20%"></ig:BoundDataField>
								<ig:TemplateDataField Key="ELIMINAR" Width="15%" ></ig:TemplateDataField>
							</Columns>
						</ig:WebDataGrid>
					</TD>
					<td width="10%">&nbsp;</td>
				</TR>
				<tr>
					<td colspan="2">&nbsp;
					</td>
				</tr>
				<tr>
					<td colspan="2"><asp:label id="lblTexto2" runat="server" Height="48px" CssClass="Rotulo10" Width="720px">Para modificar los datos de alguna de las solicitudes favoritas, seleccione dicha solicitud, modifique los datos y use el comando de "Enviar a favoritos" de nuevo. Posteriormete elimine la solicitud original.</asp:label></td>
				</tr>
			</TABLE>
		</form>
</body>
</html>
