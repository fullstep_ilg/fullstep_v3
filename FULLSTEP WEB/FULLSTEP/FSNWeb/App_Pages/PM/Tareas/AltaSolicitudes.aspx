﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.Master" CodeBehind="AltaSolicitudes.aspx.vb" Inherits="Fullstep.FSNWeb.AltaSolicitudes" %>

<asp:Content ID="ContentHead" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
    <script type="text/javascript">
    var IdPersona;
    function WebDataGrid_CellSelectionChanged(sender, e) {
        var rutaPM = '<%=ConfigurationManager.AppSettings("rutaPM")%>frames.aspx?pagina=';
        var cell = e.getSelectedCells().getItem(0);
        var column = sender.get_columns().get_columnFromKey("ID");
        var IdSolicitud = e.getSelectedCells().getItem(0).get_row().get_cellByColumn(column).get_value();
        var column = sender.get_columns().get_columnFromKey("DEN");
        var Solicitud = e.getSelectedCells().getItem(0).get_row().get_cellByColumn(column).get_value();
        var column = sender.get_columns().get_columnFromKey("FAVORITOS");
        var nFavoritos = e.getSelectedCells().getItem(0).get_row().get_cellByColumn(column).get_value();
        var column = sender.get_columns().get_columnFromKey("ID_FAVORITA");
        var Id_Favorita = e.getSelectedCells().getItem(0).get_row().get_cellByColumn(column).get_value();
        var column = sender.get_columns().get_columnFromKey("ADJUN");
        var nAdjun = e.getSelectedCells().getItem(0).get_row().get_cellByColumn(column).get_value();

        //elimino la seleccion sobre la celda pulsada para si vuelve a pulsar salte el evento, si la celda sigue 
        //seleccionada no saltaria el evento al hacer click
        e.getSelectedCells().remove(e.getSelectedCells().getItem(0));
        switch(cell.get_column()._key){
            case "IMGADJUN":
                if (nAdjun > 0) {
                    var newWindow = window.open('<%=ConfigurationManager.AppSettings("rutaPM")%>alta/solicitudPMWEB.aspx?Solicitud=' + IdSolicitud, "_blank", "width=700,height=450,status=no,resizable=no,top=100,left=100");                    
                }
                break;
            case "IMGFAV":
                if (nFavoritos > 1) {
                    newWindow = window.open('SolicitudesFavoritas.aspx?Solicitud=' + IdSolicitud + '&DenSolicitud=' + Solicitud + '&nFavoritos=' + nFavoritos + '&TipoSolicitud=' + tipoSolicitud, '_blank', 'width=750,height=300,status=no,resizable=no,top=100,left=100');
                }else {
                    if (nFavoritos == 1) window.open(rutaPM + escape('alta/NWAlta.aspx?Solicitud=' + IdSolicitud + '&Favorito=1&IdFavorito=' + Id_Favorita + '&TipoSolicitud=' + tipoSolicitud), '_self');
                    else window.open(rutaPM + 'alta/NWAlta.aspx?Solicitud=' + IdSolicitud + '&TipoSolicitud=' + tipoSolicitud, '_self');
                }
                break;
            case "IMGIMPORTAR":
                var newWindow = window.open('../../../App_Pages/_common/BuscadorSolicitudes.aspx?Solicitud=' + IdSolicitud + '&Importar=1&TipoSolicitud=' + tipoSolicitud, "_blank", "width=920,height=480,status=yes,resizable=yes,top=100,left=100");                
                return false;
                break;
            default:
                window.open(rutaPM + 'alta/NWAlta.aspx?Solicitud=' + IdSolicitud, '_self');
        }
    }
</script>

 <table width="100%" cellpadding="0" border="0">
        <tr>
            <td align="left">
                <fsn:FSNPageHeader ID="FSNPageHeader" runat="server">
                </fsn:FSNPageHeader>
            </td>
        </tr>
    </table>
    <p><asp:label id="lblParrafo"	runat="server" CssClass="Rotulo10">Seleccione el tipo de solicitud que desea dar de alta haciendo click sobre la fila en cuestión.</asp:label>
    	<asp:Label id="lblSinDatos"	runat="server" CssClass="Rotulo" ForeColor="Red" Visible="False">No hay solicitudes configuradas para su cumplimentación.</asp:Label>
    </p>
	<table id="tblSolicitudes"
		cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
		<tr>
			<td class="Cabecera Texto12"><asp:label id="lblCabeceraAltaSolicitudes" runat="server">Tipos de solicitudes disponibles</asp:label></td>
		</tr>
		<tr>
			<td>
				<style>
				tbody.igg_FullstepItem>tr>td
                {
                    border-right:none;
	                border-left:none;
	                cursor: pointer;
                }
				</style>
				<ig:WebDataGrid id="uwgSolicitudes_WDG"  runat="server" Width="100%" AutoGenerateColumns="false" ShowHeader="false">
					<Behaviors>
					<ig:Selection RowSelectType="Single" CellClickAction="Cell"  CellSelectType="Single" Enabled="true"  >
					    <SelectionClientEvents 
					        CellSelectionChanged="WebDataGrid_CellSelectionChanged" />
					</ig:Selection>
					</Behaviors>
					<Columns>
					    <ig:TemplateDataField Key="IMGFAV" Width="4%"></ig:TemplateDataField>
                        <ig:TemplateDataField Key="IMGIMPORTAR"  Width="4%"></ig:TemplateDataField>
					    <ig:BoundDataField DataFieldName="ID" Key="ID" Hidden="true"></ig:BoundDataField>
					    <ig:BoundDataField DataFieldName="TIPO" Key="TIPO" Hidden="true"></ig:BoundDataField>
					    <ig:BoundDataField DataFieldName="COD" Key="COD" Hidden="true"></ig:BoundDataField>
					    <ig:BoundDataField DataFieldName="DEN" Key="DEN" Width="91%"></ig:BoundDataField>
					    <ig:BoundDataField DataFieldName="ADJUN" Key="ADJUN" Hidden="true" ></ig:BoundDataField>
					    <ig:BoundDataField DataFieldName="FAVORITOS" Key="FAVORITOS" Hidden="true"></ig:BoundDataField>
                        <ig:BoundDataField DataFieldName="ID_FAVORITA" Key="ID_FAVORITA" Hidden="true"></ig:BoundDataField>
					    <ig:TemplateDataField Key="IMGADJUN" Width="5%"></ig:TemplateDataField>
					</Columns>
				</ig:WebDataGrid>
			</td>
		</tr>
		<tr>
			<td>&nbsp;
			</td>
		</tr>
	</table>
	<p><asp:label id="lblFooter" runat="server" CssClass="Rotulo10">Si la solicitud que usted necesita no se encuentra disponible en la lista consulte con el administrador del sistema.</asp:label></p>
</asp:Content>