﻿Imports System.IO
Imports System.Web.Script.Serialization
Imports Fullstep.FSNServer
Imports Infragistics.Web.UI.GridControls

Partial Public Class VisorSolicitudes
    Inherits FSNPage
    Private Shared _camposArchivo As New Dictionary(Of String, Integer)
    Private Shared _camposEditor As New Dictionary(Of String, Integer)
    Private Shared _camposTexto As New Dictionary(Of String, Integer)
    Private Const VALOR_ESTADO_OTRAS As String = "1000#0#2#6#8#100#101#102#103#104"
    Dim InstanciasAprobar As New Dictionary(Of String, String)
    Dim InstanciasRechazar As New Dictionary(Of String, String)
    Dim iTipoVisor As TipoVisor = TipoVisor.Solicitudes

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("TipoVisor") IsNot Nothing Then iTipoVisor = CInt(Request.QueryString("TipoVisor"))
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "tipoVisor", "<script>var tipoVisor = '" & CInt(iTipoVisor) & "';</script>")

        Dim mMaster = CType(Me.Master, FSNWeb.Menu)
        Select Case iTipoVisor
            Case FSNLibrary.TipoVisor.Facturas
                mMaster.Seleccionar("Facturacion", "Seguimiento")
            Case FSNLibrary.TipoVisor.Encuestas
                mMaster.Seleccionar("Calidad", "Encuestas")
            Case FSNLibrary.TipoVisor.SolicitudesQA
                mMaster.Seleccionar("Calidad", "SolicitudesQA")
            Case Else
                mMaster.Seleccionar("Procesos", "Seguimiento")
        End Select

        FSAL_Personalizado_Para_Visores()

        FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/Images/seguimiento.gif"
        Establecer_Textos_Pantalla()

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutaPM") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM",
                "<script>var rutaPM = '" & System.Configuration.ConfigurationManager.AppSettings("rutaPM") & "';</script>")
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "GMNLong") Then
            Dim sScript As String = "var ilGMN1 = " + FSNServer.LongitudesDeCodigos.giLongCodGMN1.ToString() + ";"
            sScript += "var ilGMN2 = " + FSNServer.LongitudesDeCodigos.giLongCodGMN2.ToString() + ";"
            sScript += "var ilGMN3 = " + FSNServer.LongitudesDeCodigos.giLongCodGMN3.ToString() + ";"
            sScript += "var ilGMN4 = " + FSNServer.LongitudesDeCodigos.giLongCodGMN4.ToString() + ";"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "GMNLong", "<script>" & sScript & "</script>")
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "NumberFormatUsu") Then
            With FSNUser
                Dim sVariablesJavascript As String = "var UsuNumberDecimalSeparator = '" & .NumberFormat.NumberDecimalSeparator & "';"
                sVariablesJavascript = sVariablesJavascript & "var UsuNumberGroupSeparator='" & .NumberFormat.NumberGroupSeparator & "';"
                sVariablesJavascript = sVariablesJavascript & "var UsuNumberNumDecimals='" & .NumberFormat.NumberDecimalDigits & "';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NumberFormatUsu", sVariablesJavascript, True)
            End With
        End If
        ' Creamos la variable de permisos
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "UserPermissions") Then
            With FSNUser
                Dim lPermisos As New List(Of String)
                ' AÃ±adimos los permisos que intervienten en esta pantalla
                lPermisos.Add("CREAR_ESCENARIOS : " & .PMPermitirCrearEscenarios.ToString().ToLower())
                lPermisos.Add("CREAR_ESCENARIOS_PARA_OTROS_USUARIOS : " & .PMPermitirCrearEscenariosOtrosUsuarios.ToString().ToLower())
                lPermisos.Add("EDITAR_ESCENARIOS_COMPARTIDOS_NO_CREADOS_POR_EL_USUARIO : " & .PMPermitirEditarEscenariosCompartidosNoCreadosPorUsuario.ToString().ToLower())
                lPermisos.Add("OCULTAR_ESCENARIO_POR_DEFECTO : " & .PMOcultarEscenarioDefecto.ToString().ToLower())
                lPermisos.Add("RESTRINGIR_CREAR_ESCENARIOS_USUARIOS_UON_USU : " & .PMRestringirCrearEscenariosUsuariosUONUsu.ToString().ToLower())
                lPermisos.Add("RESTRINGIR_CREAR_ESCENARIOS_USUARIOS_UONS_PERFIL_USU : " & .PMRestringirCrearEscenariosUsuariosUONsPerfilUsu.ToString().ToLower())
                lPermisos.Add("RESTRINGIR_CREAR_ESCENARIOS_USUARIOS_DEP_USU : " & .PMRestringirCrearEscenariosUsuariosDEPUsu.ToString().ToLower())
                lPermisos.Add("RESTRINGIR_EDITAR_ESCENARIOS_USUARIOS_UON_USU : " & .PMRestringirEdicionEscenariosUONUsu.ToString().ToLower())
                lPermisos.Add("RESTRINGIR_EDITAR_ESCENARIOS_USUARIOS_UONS_PERFIL_USU : " & .PMRestringirEdicionEscenariosUONsPerfilUsu.ToString().ToLower())
                lPermisos.Add("RESTRINGIR_EDITAR_ESCENARIOS_USUARIOS_DEP_USU : " & .PMRestringirEdicionEscenariosDEPUsu.ToString().ToLower())
                lPermisos.Add("CREAR_ESCENARIOS_PARA_PORTAL : " & .PMPermitirCrearEscenariosProve.ToString().ToLower())
                Dim sVariablesJavascript = String.Format("var permisos = {{{0}}}", String.Join(",", lPermisos))
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "UserPermissions", sVariablesJavascript, True)
            End With
        End If

        imgGuardadoFiltro.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/Images/checked.gif"
        CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgGuardandoVista"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/Images/cargando.gif"
        CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgGuardadoVista"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/Images/checked.gif"
        CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgExcel"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/Images/Excel.png"

        Dim ModalProgressClientID As String = CType(Master.Master.FindControl("ModalProgress"), AjaxControlToolkit.ModalPopupExtender).ClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalClientID", "<script>var ModalProgress = '" & ModalProgressClientID & "' </script>")
        Dim PersonaAnimationClientID As String = FSNPanelDatosPersona.AnimationClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "PersonaAnimationClientID", "<script>var PersonaAnimationClientID = '" & PersonaAnimationClientID & "'</script>")
        Dim PersonaDynamicPopulateClientID As String = FSNPanelDatosPersona.DynamicPopulateClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "PersonaDynamicPopulateClientID", "<script>var PersonaDynamicPopulateClientID = '" & PersonaDynamicPopulateClientID & "'</script>")
        Dim UsuarioAnimationClientID As String = FSNPanelDatosUsuario.AnimationClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "UsuarioAnimationClientID", "<script>var UsuarioAnimationClientID = '" & UsuarioAnimationClientID & "'</script>")
        Dim UsuarioDynamicPopulateClientID As String = FSNPanelDatosUsuario.DynamicPopulateClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "UsuarioDynamicPopulateClientID", "<script>var UsuarioDynamicPopulateClientID = '" & UsuarioDynamicPopulateClientID & "'</script>")
        Dim ProveedorAnimationClientID As String = FSNPanelDatosProveedor.AnimationClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ProveedorAnimationClientID", "<script>var ProveedorAnimationClientID = '" & ProveedorAnimationClientID & "'</script>")
        Dim ProveedorDynamicPopulateClientID As String = FSNPanelDatosProveedor.DynamicPopulateClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ProveedorDynamicPopulateClientID", "<script>var ProveedorDynamicPopulateClientID = '" & ProveedorDynamicPopulateClientID & "'</script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "OblCodPedDir", "<script>var OblCodPedDir=" & Me.Acceso.gbOblCodPedDir.ToString.ToLower & "</script>")

        With whdgSolicitudes
            .Behaviors.Paging.PageSize = FSNUser.PMNumeroFilas
            .GridView.Behaviors.Paging.PageSize = FSNUser.PMNumeroFilas
        End With
        If Request.QueryString("carpeta") IsNot Nothing Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "carpetaVolver",
                "<script>var carpetaVolver = " & CType(Request.QueryString("carpeta"), Integer) & ";</script>")
        End If
        If Request.QueryString("escenario") IsNot Nothing Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "escenarioVolver",
                "<script>var escenarioVolver = " & CType(Request.QueryString("escenario"), Integer) & ";</script>")
        End If
        If Request.QueryString("filtro") IsNot Nothing Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "filtroVolver",
                "<script>var filtroVolver = " & CType(Request.QueryString("filtro"), Integer) & ";</script>")
        End If
        If Request.QueryString("vista") IsNot Nothing Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "vistaVolver",
                "<script>var vistaVolver = " & CType(Request.QueryString("vista"), Integer) & ";</script>")
        End If
        If Request.QueryString("numpag") IsNot Nothing Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "numpag",
                "<script>var numpag = " & CType(Request.QueryString("numpag"), Integer) & ";</script>")
        End If
        If Request.QueryString("IdEscenario") IsNot Nothing Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdEscenario",
                "<script>var IdEscenario = " & CType(Request.QueryString("IdEscenario"), Integer) & ";</script>")
        End If
        If Request.QueryString("carpeta") IsNot Nothing AndAlso Request.QueryString("escenario") IsNot Nothing _
            AndAlso Request.QueryString("filtro") IsNot Nothing AndAlso Session("FiltroCondiciones") IsNot Nothing Then
            Dim serializer As New JavaScriptSerializer
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdEscenario",
                "<script>var formulaCondicionesSession = " & serializer.Serialize(CType(Session("FiltroCondiciones"), List(Of EscenarioFiltroCondicion))) & ";" &
                IIf(HttpContext.Current.Session("chkProcesosPedidos") IsNot Nothing, "var chkProcesosPedidos=" & serializer.Serialize(CType(Session("chkProcesosPedidos"), List(Of Escenario_Campo))), "") & "</script>")
        End If
        If HttpContext.Current.Session("chkProcesosPedidos") IsNot Nothing Then CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("chkInfoPedidosProcesos"), CheckBox).Checked = True
        If HttpContext.Current.Session("PMFiltroUsuarioDefectoOrden") Is Nothing Then HttpContext.Current.Session("PMFiltroUsuarioDefectoOrden") = "FECHA DESC"
        If IsPostBack Then
            whdgSolicitudes.Visible = True
            Select Case Request("__EVENTTARGET")
                Case btnRecargarGridSolicitudes.ClientID, btnPager.ClientID, "Excel"
                    HttpContext.Current.Session("chkProcesosPedidos") = Nothing
                    'Para cargar las solicitudes en el grid necesitamos el filtro que se aplica y la vista, ademas de saber si se ataca sobre todas las solicitudes
                    'o solo contra las de un tipo de solicitud/formulario. Dependiendo de que campos se desean ver obtendremos una subconsulta de las columnas necesarias
                    'para crear el grid. Tener en cuenta que necesitamos una tabla relacionada con los datos de desglose.
                    Dim bExportar As Boolean = False
                    If Request("__EVENTTARGET") = "Excel" Then bExportar = True

                    Dim serializer As New JavaScriptSerializer
                    Dim oInfoFiltroVista As Object = serializer.Deserialize(Of Object)(Request("__EVENTARGUMENT"))
                    Dim IdFormulario As Integer = serializer.Deserialize(Of Integer)(oInfoFiltroVista("IdFormulario"))
                    Dim oFiltro As EscenarioFiltro = serializer.Deserialize(Of EscenarioFiltro)(oInfoFiltroVista("Filtro"))
                    Dim oVista As EscenarioVista = serializer.Deserialize(Of EscenarioVista)(oInfoFiltroVista("Vista"))
                    Dim lSolicitudesVinculadas As String = serializer.Deserialize(Of String)(oInfoFiltroVista("SolicitudFormularioVinculados"))
                    If Request("__EVENTTARGET") = btnPager.ClientID Then
                        InstanciasAprobar = serializer.Deserialize(Of Dictionary(Of String, String))(oInfoFiltroVista("InstanciasAprobar"))
                        InstanciasRechazar = serializer.Deserialize(Of Dictionary(Of String, String))(oInfoFiltroVista("InstanciasRechazar"))
                    End If

                    Dim columNames() As String = {}
                    Dim columNamesDesglose() As String = {}
                    Dim ObtenerProcesos, ObtenerPedidos, VistaDesglose As Boolean
                    Dim bVaLaLinea As Boolean
                    Seleccionar_Columnas_Grid(oVista.Campos_Vista, columNames, columNamesDesglose, ObtenerProcesos, ObtenerPedidos, VistaDesglose, bVaLaLinea)
                    For Each oCampoTexto As Escenario_Campo In oVista.Campos_Vista
                        If (oCampoTexto.TipoCampoGS = 0 OrElse ({TiposDeDatos.TipoCampoGS.DescrBreve, TiposDeDatos.TipoCampoGS.DescrDetallada}.Contains(oCampoTexto.TipoCampoGS) AndAlso Not oCampoTexto.EsCampoGeneral)) _
                            AndAlso {TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoTextoCorto}.Contains(oCampoTexto.TipoCampo) Then
                            _camposTexto(oCampoTexto.Denominacion_BD) = oCampoTexto.Id
                        End If
                    Next
                    Dim oPageNumber As Integer = serializer.Deserialize(Of Integer)(oInfoFiltroVista("Page"))

                    Dim sOrden As String = oVista.Orden
                    If oVista.Escenario = 0 Then sOrden = HttpContext.Current.Session("PMFiltroUsuarioDefectoOrden")

                    If bExportar Then
                        Configurar_Columnas_Grid_Solicitudes_Exportar(oVista.Campos_Vista, sOrden)
                    ElseIf Not Request("__EVENTTARGET") = btnPager.ClientID Then
                        Configurar_Columnas_Grid_Solicitudes(oVista.Campos_Vista, sOrden)
                    End If

                    Dim cInstancias As Instancias
                    cInstancias = FSNServer.Get_Object(GetType(FSNServer.Instancias))
                    Dim AbtasUsted, Participo, Pendientes, PendientesDevolucion, Trasladadas, Otras, ObservadorSustitucion As Nullable(Of Boolean)
                    Dim FiltroDesglose As Boolean
                    Dim sentenciaWhere As String = Generar_SentenciaWhere_Filtro(oFiltro.FormulaCondiciones, FiltroDesglose)
                    Dim sentenciaWhereCamposGenerales As String
                    If oFiltro.FormulaAvanzada Then
                        sentenciaWhereCamposGenerales = ""
                        AbtasUsted = True
                        Participo = True
                        Pendientes = True
                        PendientesDevolucion = True
                        Trasladadas = True
                        Otras = True
                        ObservadorSustitucion = True
                    Else
                        sentenciaWhereCamposGenerales = Generar_SentenciaWhere_CamposGenerales(oFiltro.FormulaCondiciones, AbtasUsted, Participo, Pendientes, PendientesDevolucion, Trasladadas, Otras, ObservadorSustitucion, lSolicitudesVinculadas)
                    End If
                    Dim ds As DataSet = cInstancias.DevolverInstancias(FSNUser.CodPersona, FSNUser.Idioma, IdFormulario,
                                        sentenciaWhere, sOrden, sentenciaWhereCamposGenerales, oPageNumber, FSNUser.PMNumeroFilas,
                                        BuscarProveedorArticulo(oFiltro.FormulaCondiciones, oVista.Campos_Vista), AbtasUsted, Participo, Pendientes,
                                        PendientesDevolucion, Trasladadas, Otras, ObservadorSustitucion, ObtenerProcesos, ObtenerPedidos,
                                        Acceso.gbOblCodPedDir, FiltroDesglose, VistaDesglose, iTipoVisor)
                    Dim view As New DataView(ds.Tables(0))
                    Dim dsResultado As New DataSet
                    Dim dtSolicitudes As DataTable = view.ToTable("SOLICITUDES", True, columNames)
                    Dim dtSolicitudesClone As DataTable = dtSolicitudes.Clone
                    dtSolicitudesClone.Locale = System.Globalization.CultureInfo.InvariantCulture
                    For Each columna As Escenario_Campo In oVista.Campos_Vista
                        If Not (columna.Id = CamposGeneralesVisor.IDENTIFICADOR And columna.EsCampoGeneral) AndAlso Not columna.EsCampoDesglose _
                            AndAlso Not {TiposDeDatos.TipoCampoGS.Empresa, TiposDeDatos.TipoCampoGS.Almacen}.Contains(columna.TipoCampoGS) _
                            AndAlso columna.TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico Then
                            dtSolicitudesClone.Columns(columna.Denominacion_BD).DataType = GetType(System.Double)
                        End If
                    Next
                    For Each row As DataRow In dtSolicitudes.Rows
                        dtSolicitudesClone.ImportRow(row)
                    Next
                    dsResultado.Tables.Add(dtSolicitudesClone)
                    If Not IdFormulario = 0 AndAlso (columNamesDesglose.Count > 3 OrElse bVaLaLinea) Then
                        Dim listaDesgloses As List(Of String) = columNamesDesglose.Where(Function(x) Split(x, "_").Length = 4).Select(Function(x) Split(x, "_")(1)).Distinct().ToList
                        Dim query1 = From Datos In ds.Tables(0).AsEnumerable()
                                     Where (listaDesgloses.Contains(Datos.Item("DESGLOSE").ToString))
                                     Select Datos Distinct
                        Dim dtDesglose As DataTable = Nothing
                        If query1.Any Then dtDesglose = query1.CopyToDataTable()

                        If dtDesglose IsNot Nothing Then
                            Dim viewDesglose As New DataView(dtDesglose)
                            Dim dtDesgloseDist = viewDesglose.ToTable("DESGLOSE", True, columNamesDesglose)
                            Dim dtDesgloseClone As DataTable = dtDesgloseDist.Clone
                            dtDesgloseClone.Locale = System.Globalization.CultureInfo.InvariantCulture
                            For Each columna As Escenario_Campo In oVista.Campos_Vista
                                If Not (columna.Id = CamposGeneralesVisor.IDENTIFICADOR And columna.EsCampoGeneral) AndAlso columna.EsCampoDesglose _
                                    AndAlso Not {TiposDeDatos.TipoCampoGS.Empresa, TiposDeDatos.TipoCampoGS.Almacen}.Contains(columna.TipoCampoGS) _
                                    AndAlso columna.TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico Then
                                    dtDesgloseClone.Columns(columna.Denominacion_BD).DataType = GetType(System.Double)
                                End If
                            Next
                            For Each oRow As DataRow In dtDesgloseDist.Rows
                                dtDesgloseClone.ImportRow(oRow)
                            Next

                            dsResultado.Tables.Add(dtDesgloseClone)

                            Dim parentColsVista(0) As DataColumn
                            Dim childColsVista(0) As DataColumn
                            parentColsVista(0) = dsResultado.Tables("SOLICITUDES").Columns("ID")
                            childColsVista(0) = dsResultado.Tables("DESGLOSE").Columns("FORM_INSTANCIA")
                            dsResultado.Relations.Add("SOLICITUD_DESGLOSE", parentColsVista, childColsVista, False)
                        End If
                    End If
                    If Pendientes Then
                        dsResultado.Tables.Add(New DataView(ds.Tables(1)).ToTable("SOLICITUDES_APROBAR", True, New String() {"ID", "ACCION_APROBAR", "BLOQUE"}))
                        dsResultado.Tables.Add(New DataView(ds.Tables(2)).ToTable("SOLICITUDES_RECHAZAR", True, New String() {"ID", "ACCION_RECHAZAR", "BLOQUE"}))
                    End If

                    If Not bExportar Then
                        InsertarEnCache("dsSolicitudes_" & FSNUser.Cod, dsResultado, CacheItemPriority.BelowNormal)
                        With whdgSolicitudes
                            .Rows.Clear()
                            .GridView.ClearDataSource()
                            .DataKeyFields = "ID,ROWNUMBER"
                            .DataMember = "SOLICITUDES"
                            .DataSource = dsResultado
                            .GridView.DataSource = dsResultado
                            Establecer_Orden_Columnas_Grid_Solicitudes(sOrden)
                            .DataBind()
                        End With
                        Dim numSolicitudes As Integer
                        If ds.Tables(0).Rows.Count = 0 Then
                            numSolicitudes = 0
                        Else
                            numSolicitudes = ds.Tables(0).Rows(0)("TOTALSOLICITUDES")
                        End If
                        iNumeroSolicitudesFiltroSeleccionado.Value = numSolicitudes
                        Paginador((numSolicitudes \ FSNUser.PMNumeroFilas) + (IIf(numSolicitudes Mod FSNUser.PMNumeroFilas = 0, 0, 1)), oPageNumber)
                        updSolicitudes.Update()

                        Dim dsConfVista As New DataSet
                        Dim dtDatosVista As New DataTable
                        Dim dtDatosVistaDesglose As New DataTable
                        dtDatosVista.TableName = "SOLICITUDES"
                        dtDatosVistaDesglose.TableName = "DESGLOSE"
                        dtDatosVistaDesglose.Columns.Add("FORM_INSTANCIA", GetType(System.Int32))
                        dtDatosVista.Columns.Add("ID", GetType(System.Int32))
                        dsConfVista.Tables.Add(dtDatosVista)
                        dsConfVista.Tables.Add(dtDatosVistaDesglose)
                        Dim parentCols(0) As DataColumn
                        Dim childCols(0) As DataColumn
                        parentCols(0) = dsConfVista.Tables("SOLICITUDES").Columns("ID")
                        childCols(0) = dsConfVista.Tables("DESGLOSE").Columns("FORM_INSTANCIA")
                        dsConfVista.Relations.Add("SOLICITUD_DESGLOSE", parentCols, childCols, False)
                        With whdgConfigVista
                            .Rows.Clear()
                            .GridView.ClearDataSource()
                            .DataSource = dsConfVista
                            .GridView.DataSource = .DataSource
                            .DataKeyFields = "ID"
                            .DataMember = "SOLICITUDES"
                            .GridView.Behaviors.Sorting.SortedColumns.Clear()
                            .DataBind()
                            .ExpandAll()
                        End With

                        updConfiguracionVista.Update()
                    Else
                        InsertarEnCache("dsSolicitudesExportar_" & FSNUser.Cod, dsResultado, CacheItemPriority.BelowNormal)
                        With whdgExportacion
                            .Rows.Clear()
                            .GridView.ClearDataSource()
                            .DataKeyFields = "ID,ROWNUMBER"
                            .DataMember = "SOLICITUDES"
                            .DataSource = dsResultado
                            .GridView.DataSource = dsResultado
                            .InitialDataBindDepth = -1
                            .DataBind()
                        End With
                        ExportarExcel()
                    End If
                Case btnRecargarConfigVista.ClientID
                    'Mantenemos el grid de generacion de la vista con las columnas seleccionadas
                    Dim serializer As New JavaScriptSerializer
                    Dim oInfoVista As Object = serializer.Deserialize(Of Object)(Request("__EVENTARGUMENT"))
                    Dim lCamposVista As List(Of Escenario_Campo) = serializer.Deserialize(Of List(Of Escenario_Campo))(oInfoVista("CamposVista"))
                    Dim ordenVista As String = CType(oInfoVista("OrdenVista"), String)
                    Configurar_Columnas_Grid_ConfiguradorVista(lCamposVista, IIf(ordenVista Is Nothing, "", String.Join(",", Split(ordenVista, ",").Where(Function(x) lCamposVista.Any(Function(y) y.Denominacion_BD = Split(x, " ")(0))))))
                    updConfiguracionVista.Update()
                Case btnBuscarSolicitud.ClientID
                    Dim serializer As New JavaScriptSerializer
                    Dim oInfo As Object = serializer.Deserialize(Of Object)(Request("__EVENTARGUMENT"))
                    Dim oVista As EscenarioVista = serializer.Deserialize(Of EscenarioVista)(oInfo("Vista"))
                    Dim sOrden As String = oVista.Orden
                    If oVista.Escenario = 0 Then sOrden = HttpContext.Current.Session("PMFiltroUsuarioDefectoOrden")
                    Configurar_Columnas_Grid_Solicitudes(oVista.Campos_Vista, sOrden)
                    Dim columNames() As String = {}
                    Dim columNamesDesglose() As String = {}
                    Dim ObtenerProcesos, ObtenerPedidos As Boolean
                    Dim VistaDesglose As Boolean
                    Seleccionar_Columnas_Grid(oVista.Campos_Vista, columNames, columNamesDesglose, ObtenerProcesos, ObtenerPedidos, VistaDesglose)

                    Dim ds As DataSet = CType(Cache("dsSolicitudes_" & FSNUser.Cod), DataSet)
                    Dim view As New DataView(ds.Tables(0))
                    Dim viewDesglose As New DataView(ds.Tables(0))
                    Dim dsResultado As New DataSet
                    Dim dtSolicitudes As DataTable = view.ToTable("SOLICITUDES", True, columNames)
                    dsResultado.Tables.Add(dtSolicitudes)

                    With whdgSolicitudes
                        .Rows.Clear()
                        .GridView.ClearDataSource()
                        .DataKeyFields = "ID,ROWNUMBER"
                        .DataMember = "SOLICITUDES"
                        .DataSource = dsResultado
                        .GridView.DataSource = dsResultado
                        Establecer_Orden_Columnas_Grid_Solicitudes(sOrden)
                        .DataBind()
                    End With
                    Dim numSolicitudes As Integer
                    If ds.Tables(0).Rows.Count = 0 Then
                        numSolicitudes = 0
                        Paginador(0, 1)
                    Else
                        numSolicitudes = ds.Tables(0).Rows(0)("TOTALSOLICITUDES")
                        Paginador(1, 1)
                    End If
                    updSolicitudes.Update()
                Case btnColumnMove.ClientID
                    Dim serializer As New JavaScriptSerializer
                    Dim oInfo As Object = serializer.Deserialize(Of Object)(Request("__EVENTARGUMENT"))
                    Dim oVista As EscenarioVista = serializer.Deserialize(Of EscenarioVista)(oInfo("Vista"))
                    Dim sOrden As String = oVista.Orden
                    If oVista.Escenario = 0 Then sOrden = HttpContext.Current.Session("PMFiltroUsuarioDefectoOrden")
                    Configurar_Columnas_Grid_Solicitudes(oVista.Campos_Vista, sOrden)
                    With whdgSolicitudes
                        .Rows.Clear()
                        .GridView.ClearDataSource()
                        .DataKeyFields = "ID,ROWNUMBER"
                        .DataMember = "SOLICITUDES"
                        .DataSource = CType(Cache("dsSolicitudes_" & FSNUser.Cod), DataSet)
                        .GridView.DataSource = CType(Cache("dsSolicitudes_" & FSNUser.Cod), DataSet)
                        .DataBind()
                    End With
                    updSolicitudes.Update()
                Case Else
                    With whdgSolicitudes
                        .Rows.Clear()
                        .GridView.ClearDataSource()
                        .DataKeyFields = "ID,ROWNUMBER"
                        .DataMember = "SOLICITUDES"
                        .DataSource = CType(Cache("dsSolicitudes_" & FSNUser.Cod), DataSet)
                        .GridView.DataSource = CType(Cache("dsSolicitudes_" & FSNUser.Cod), DataSet)
                        .DataBind()
                    End With
                    updSolicitudes.Update()

                    If CType(Cache("dsConfigVista_" & FSNUser.Cod), DataSet) IsNot Nothing _
                        AndAlso Not CType(Cache("dsConfigVista_" & FSNUser.Cod), DataSet).Tables("SOLICITUDES").Rows.Count = 0 Then
                        With whdgConfigVista
                            .Rows.Clear()
                            .GridView.ClearDataSource()
                            .DataSource = CType(Cache("dsConfigVista_" & FSNUser.Cod), DataSet)
                            .GridView.DataSource = .DataSource
                            .DataKeyFields = "ID,ROWNUMBER"
                            .DataMember = "SOLICITUDES"
                            .DataBind()
                            .ExpandAll()
                        End With
                        updConfiguracionVista.Update()
                    End If
            End Select
        End If
    End Sub
    ''' <summary>
    ''' Metodo para registrar tiempos de FSAL del cargado del grid principal en la carga total de la pagina y en posteriores cargas parciales.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FSAL_Personalizado_Para_Visores()
        ''Codigo para guardar datos del FSAL recogidos desde FSNLibrary y así poder utilizarlos en el PostBack cuando se cargue el Grid.
        ''Si es primera llamada a la pagina y no se han enviado los tiempos para update, se pasa el idRegistro inicial para localizar el registro a modificar
        ''si no, al hacer una insercion de un nuevo registro se pasa el idRegistro de la carga actual.
        If HttpContext.Current.Items("FECHAPET") IsNot Nothing AndAlso IsDate(HttpContext.Current.Items("FECHAPET")) AndAlso
           HttpContext.Current.Items("IDREGISTRO") IsNot Nothing AndAlso HttpContext.Current.Items("IDREGISTRO") <> "" Then
            bActivadoFSAL.Value = "1"
            Dim fechaIniFSAL8 As DateTime = HttpContext.Current.Items("FECHAPET")
            sFechaIniFSAL8.Value = Format(fechaIniFSAL8, "yyyy-MM-dd HH:mm:ss") & "." & Format(fechaIniFSAL8.Millisecond, "000")
            If bEnviadoFSAL8Inicial.Value = "0" AndAlso Not IsPostBack Then
                sIdRegistroFSAL1.Value = HttpContext.Current.Items("IDREGISTRO")
                sPagina.Value = HttpContext.Current.Request.Url.AbsoluteUri.Split("?")(0) 'para quitar ?=prove=
                iP.Value = HttpContext.Current.Request.UserHostAddress
            ElseIf bEnviadoFSAL8Inicial.Value = "1" AndAlso HttpContext.Current.Request.Headers("x-ajax") Is Nothing Then
                sProducto.Value = System.Configuration.ConfigurationManager.AppSettings("FSAL_Origen") '"FULLSTEP WEB ó FULLSTEP PORTAL"
                sPagina.Value = HttpContext.Current.Request.Url.AbsoluteUri.Split("?")(0) 'para quitar ?=prove=
                iPost.Value = 3
                iP.Value = HttpContext.Current.Request.UserHostAddress
                sUsuCod.Value = DirectCast(Session("FSN_User"), Fullstep.FSNServer.User).Cod
                sPaginaOrigen.Value = sPagina.Value
                sNavegador.Value = HttpContext.Current.Request.ServerVariables("HTTP_USER_AGENT") ' Obtener el navegador
                Dim rndRandom As New Random
                sIdRegistroFSAL8.Value = fechaIniFSAL8.Ticks.ToString() & Format(rndRandom.Next(1, 99999999), "00000000")
                sProveCod.Value = ""
                If HttpContext.Current.Request.Url.AbsoluteUri.Split("?").Length > 1 Then
                    sQueryString.Value = HttpContext.Current.Request.Url.AbsoluteUri.Split("?")(1)
                Else
                    sQueryString.Value = ""
                End If
            End If
        End If
        ''Fin codigo FSAL
    End Sub
    ''' <summary>
    ''' Creamos un array con las columnas necesarias para la grid y poder obtener de la tabla con los resultados las columnas necesarias
    ''' </summary>
    ''' <param name="Campos_Vista"></param>
    ''' <param name="columNames"></param>
    ''' <param name="columNamesDesglose"></param>
    ''' <param name="bVaLaLinea">Si hay q mostrar en la vista la linea o no</param>
    ''' <remarks></remarks>
    Private Shared Sub Seleccionar_Columnas_Grid(ByVal Campos_Vista As List(Of Escenario_Campo), ByRef columNames As String(), ByRef columNamesDesglose As String(),
                ByRef ObtenerProcesos As Boolean, ByRef ObtenerPedidos As Boolean, ByRef VistaDesglose As Boolean, Optional ByRef bVaLaLinea As Boolean = False)
        ReDim Preserve columNamesDesglose(2)
        columNamesDesglose(0) = "FORM_INSTANCIA"
        columNamesDesglose(1) = "DESGLOSE"
        columNamesDesglose(2) = "LINEA"

        ReDim Preserve columNames(0)
        columNames(0) = "ROWNUMBER"

        bVaLaLinea = False

        For Each column As Escenario_Campo In Campos_Vista
            If column.EsCampoDesglose Then
                VistaDesglose = True

                If (Not column.TipoCampo = TiposDeDatos.TipoGeneral.TipoDesglose) Then
                    ReDim Preserve columNamesDesglose(columNamesDesglose.Length)
                    columNamesDesglose(columNamesDesglose.Length - 1) = column.Denominacion_BD
                Else
                    bVaLaLinea = True
                End If
            Else
                If (column.Id = CamposGeneralesVisor.TIPOSOLICITUD AndAlso column.EsCampoGeneral AndAlso Not columNames.Contains("TIPO")) Or Not columNames.Contains(column.Denominacion_BD) Then
                    ReDim Preserve columNames(columNames.Length)
                    If column.Id = CamposGeneralesVisor.TIPOSOLICITUD AndAlso column.EsCampoGeneral Then
                        columNames(columNames.Length - 1) = "TIPO"
                    ElseIf column.Id = CamposGeneralesVisor.PROVEEDOR AndAlso column.EsCampoGeneral Then
                        columNames(columNames.Length - 1) = "PROVEEDOR_INSTANCIA"
                        ReDim Preserve columNames(columNames.Length)
                        columNames(columNames.Length - 1) = "PROVEEDOR_INSTANCIA_DEN"
                        If Not columNames.Contains("PROVEEDOR") Then
                            ReDim Preserve columNames(columNames.Length)
                            columNames(columNames.Length - 1) = "PROVEEDOR"
                        End If
                    ElseIf column.Id = CamposGeneralesVisor.ARTICULO AndAlso column.EsCampoGeneral Then
                        columNames(columNames.Length - 1) = "ARTICULO_INSTANCIA"
                        ReDim Preserve columNames(columNames.Length)
                        columNames(columNames.Length - 1) = "ARTICULO_INSTANCIA_DEN"
                    ElseIf column.Id = CamposGeneralesVisor.ORIGEN AndAlso column.EsCampoGeneral Then
                        columNames(columNames.Length - 1) = "ORIGEN_DEN"
                    ElseIf column.Id = CamposGeneralesVisor.ESTADOHOMOLOGACION AndAlso column.EsCampoGeneral Then
                        columNames(columNames.Length - 1) = "ESTADO_HOMOLOGACION"
                        ReDim Preserve columNames(columNames.Length)
                        columNames(columNames.Length - 1) = "ESTADO_HOMOLOGACION_DEN"
                    Else
                        columNames(columNames.Length - 1) = column.Denominacion_BD
                    End If
                End If
                If column.EsCampoGeneral Then
                    Select Case column.Id
                        Case CamposGeneralesVisor.INFOPROCESOSASOCIADOS
                            ObtenerProcesos = True
                        Case CamposGeneralesVisor.INFOPEDIDOSASOCIADOS
                            ObtenerPedidos = True
                        Case CamposGeneralesVisor.IDENTIFICADOR
                            ReDim Preserve columNames(columNames.Length)
                            columNames(columNames.Length - 1) = "NUM_REEMISIONES"
                        Case CamposGeneralesVisor.IMPORTE
                            ReDim Preserve columNames(columNames.Length)
                            columNames(columNames.Length - 1) = "TIPO_SOLIC"
                            ReDim Preserve columNames(columNames.Length)
                            columNames(columNames.Length - 1) = "MON"
                        Case CamposGeneralesVisor.DEPARTAMENTO
                            If Not columNames.Contains("VER_DETALLE_PER") Then
                                ReDim Preserve columNames(columNames.Length)
                                columNames(columNames.Length - 1) = "VER_DETALLE_PER"
                            End If
                        Case CamposGeneralesVisor.USUARIO, CamposGeneralesVisor.PETICIONARIO, CamposGeneralesVisor.ESTADO, CamposGeneralesVisor.SITUACIONACTUAL
                            If Not columNames.Contains("EN_PROCESO") Then
                                ReDim Preserve columNames(columNames.Length)
                                columNames(columNames.Length - 1) = "EN_PROCESO"
                            End If
                            If {CamposGeneralesVisor.USUARIO, CamposGeneralesVisor.PETICIONARIO}.Contains(column.Id) Then
                                If Not columNames.Contains("TRASLADADA") Then
                                    ReDim Preserve columNames(columNames.Length)
                                    columNames(columNames.Length - 1) = "TRASLADADA"
                                End If
                                If Not columNames.Contains("VER_DETALLE_PER") Then
                                    ReDim Preserve columNames(columNames.Length)
                                    columNames(columNames.Length - 1) = "VER_DETALLE_PER"
                                End If
                                If Not columNames.Contains("VER_DETALLE_PER_ROL") Then
                                    ReDim Preserve columNames(columNames.Length)
                                    columNames(columNames.Length - 1) = "VER_DETALLE_PER_ROL"
                                End If
                                If Not columNames.Contains("PROVEEDOR") Then
                                    ReDim Preserve columNames(columNames.Length)
                                    columNames(columNames.Length - 1) = "PROVEEDOR"
                                End If
                                If Not columNames.Contains("FECHA_ACT") Then
                                    ReDim Preserve columNames(columNames.Length)
                                    columNames(columNames.Length - 1) = "FECHA_ACT"
                                End If
                                If Not columNames.Contains("FECHA_LIMITE") Then
                                    ReDim Preserve columNames(columNames.Length)
                                    columNames(columNames.Length - 1) = "FECHA_LIMITE"
                                End If
                                If Not columNames.Contains("TRASLADO_A_NOMBRE") Then
                                    ReDim Preserve columNames(columNames.Length)
                                    columNames(columNames.Length - 1) = "TRASLADO_A_NOMBRE"
                                End If
                                If Not columNames.Contains("TRASLADO_A_PROV") Then
                                    ReDim Preserve columNames(columNames.Length)
                                    columNames(columNames.Length - 1) = "TRASLADO_A_PROV"
                                End If
                                If Not columNames.Contains("TRASLADO_A_USU") Then
                                    ReDim Preserve columNames(columNames.Length)
                                    columNames(columNames.Length - 1) = "TRASLADO_A_USU"
                                End If
                                If column.Id = CamposGeneralesVisor.USUARIO Then
                                    If Not columNames.Contains("USU") Then
                                        ReDim Preserve columNames(columNames.Length)
                                        columNames(columNames.Length - 1) = "USU"
                                    End If
                                End If
                                If column.Id = CamposGeneralesVisor.PETICIONARIO Then
                                    If Not columNames.Contains("PET") Then
                                        ReDim Preserve columNames(columNames.Length)
                                        columNames(columNames.Length - 1) = "PET"
                                    End If
                                    If Not columNames.Contains("PETICIONARIO_PROVE") Then
                                        ReDim Preserve columNames(columNames.Length)
                                        columNames(columNames.Length - 1) = "PETICIONARIO_PROVE"
                                    End If
                                End If
                            End If
                            If {CamposGeneralesVisor.ESTADO, CamposGeneralesVisor.SITUACIONACTUAL}.Contains(column.Id) Then
                                If Not columNames.Contains("ETAPA") Then
                                    ReDim Preserve columNames(columNames.Length)
                                    columNames(columNames.Length - 1) = "ETAPA"
                                End If
                                If Not columNames.Contains("VER_FLUJO") Then
                                    ReDim Preserve columNames(columNames.Length)
                                    columNames(columNames.Length - 1) = "VER_FLUJO"
                                End If
                            End If
                        Case CamposGeneralesVisor.ORIGEN
                            If Not columNames.Contains("VER_DETALLE_PER") Then
                                ReDim Preserve columNames(columNames.Length)
                                columNames(columNames.Length - 1) = "VER_DETALLE_PER"
                            End If
                    End Select
                End If
            End If
            If column.TipoCampo = TiposDeDatos.TipoGeneral.TipoArchivo Then
                _camposArchivo(column.Denominacion_BD) = column.Id
            End If
            If column.TipoCampo = TiposDeDatos.TipoGeneral.TipoEditor Then
                _camposEditor(column.Denominacion_BD) = column.Id
            End If
        Next
        If Not columNames.Contains("ID") Then
            ReDim Preserve columNames(columNames.Length)
            columNames(columNames.Length - 1) = "ID"
            ReDim Preserve columNames(columNames.Length)
            columNames(columNames.Length - 1) = "NUM_REEMISIONES"
        End If
        If Not columNames.Contains("SEG") Then
            ReDim Preserve columNames(columNames.Length)
            columNames(columNames.Length - 1) = "SEG"
        End If
        If Not columNames.Contains("KO") Then
            ReDim Preserve columNames(columNames.Length)
            columNames(columNames.Length - 1) = "KO"
        End If
        If Not columNames.Contains("EST_VALIDACION") Then
            ReDim Preserve columNames(columNames.Length)
            columNames(columNames.Length - 1) = "EST_VALIDACION"
        End If
        If Not columNames.Contains("ICONO") Then
            ReDim Preserve columNames(columNames.Length)
            columNames(columNames.Length - 1) = "ICONO"
        End If
        If Not columNames.Contains("ERROR") Then
            ReDim Preserve columNames(columNames.Length)
            columNames(columNames.Length - 1) = "ERROR"
        End If
        If Not columNames.Contains("FECHA_VALIDACION") Then
            ReDim Preserve columNames(columNames.Length)
            columNames(columNames.Length - 1) = "FECHA_VALIDACION"
        End If
        If Not columNames.Contains("ERROR_VALIDACION") Then
            ReDim Preserve columNames(columNames.Length)
            columNames(columNames.Length - 1) = "ERROR_VALIDACION"
        End If
        If Not columNames.Contains("ACCION_APROBAR") Then
            ReDim Preserve columNames(columNames.Length)
            columNames(columNames.Length - 1) = "ACCION_APROBAR"
        End If
        If Not columNames.Contains("ACCION_RECHAZAR") Then
            ReDim Preserve columNames(columNames.Length)
            columNames(columNames.Length - 1) = "ACCION_RECHAZAR"
        End If
        If Not columNames.Contains("BLOQUE") Then
            ReDim Preserve columNames(columNames.Length)
            columNames(columNames.Length - 1) = "BLOQUE"
        End If
        If Not columNames.Contains("ETAPA_ACTUAL") Then
            ReDim Preserve columNames(columNames.Length)
            columNames(columNames.Length - 1) = "ETAPA_ACTUAL"
        End If
        If Not columNames.Contains("OBSERVADOR") Then
            ReDim Preserve columNames(columNames.Length)
            columNames(columNames.Length - 1) = "OBSERVADOR"
        End If
        If Not columNames.Contains("ESTADO") Then
            ReDim Preserve columNames(columNames.Length)
            columNames(columNames.Length - 1) = "ESTADO"
        End If
        If Not columNames.Contains("TIPO_SOLIC") Then
            ReDim Preserve columNames(columNames.Length)
            columNames(columNames.Length - 1) = "TIPO_SOLIC"
        End If
        If Not columNames.Contains("TRASLADADA") Then
            ReDim Preserve columNames(columNames.Length)
            columNames(columNames.Length - 1) = "TRASLADADA"
        End If
        If Not columNames.Contains("OBSYPART") Then
            ReDim Preserve columNames(columNames.Length)
            columNames(columNames.Length - 1) = "OBSYPART"
        End If
    End Sub
    Private Sub Establecer_Orden_Columnas_Grid_Solicitudes(ByVal Orden As String)
        whdgSolicitudes.GridView.Behaviors.Sorting.SortedColumns.Clear()

        For Each columnaOrdenada As String In Orden.Split(",")
            If Not columnaOrdenada = "" AndAlso Not whdgSolicitudes.GridView.Columns.Item(Split(columnaOrdenada, " ")(0)) Is Nothing Then
                whdgSolicitudes.GridView.Behaviors.Sorting.SortedColumns.Add(Split(columnaOrdenada, " ")(0),
                    IIf(Split(columnaOrdenada, " ")(1) = "DESC", Infragistics.Web.UI.SortDirection.Descending, Infragistics.Web.UI.SortDirection.Ascending))
            End If
        Next
    End Sub
    Private Sub Establecer_Textos_Pantalla()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Seguimiento
        Select Case iTipoVisor
            Case TipoVisor.Facturas
                FSNPageHeader.TituloCabecera = Textos(224)
                lblSolicitudesPendientes.Text = Textos(225)
                lnkSolicitudesPendientes.Text = Textos(226)
            Case TipoVisor.SolicitudesQA
                FSNPageHeader.TituloCabecera = Textos(230)
                lblSolicitudesPendientes.Text = Textos(231)
                lnkSolicitudesPendientes.Text = Textos(232)
            Case TipoVisor.Encuestas
                FSNPageHeader.TituloCabecera = Textos(233)
                lblSolicitudesPendientes.Text = Textos(234)
                lnkSolicitudesPendientes.Text = Textos(235)
            Case Else
                FSNPageHeader.TituloCabecera = Textos(0)
                lblSolicitudesPendientes.Text = Textos(12)
                lnkSolicitudesPendientes.Text = Textos(13)
        End Select
        lblBotonBuscar.Text = Textos(11)
        lblEscenariosCarpetas.Text = Textos(14)
        lblEscenariosCarpetasAgregar.Text = Textos(15)
        lblEscenariosFavoritos.Text = Textos(16)
        lblEscenariosTituloBarra.Text = Textos(17)
        imgEscenariosOrder.ToolTip = Textos(164)
        lblEscenariosTituloSeleccionado.Text = Textos(18) & ":"
        lblEscenarioFiltrosTituloBarra.Text = Textos(19)
        imgEscenarioFiltrosOrder.ToolTip = Textos(163)
        lblEscenarioFiltrosTituloSeleccionado.Text = Textos(18) & ":"
        chkEscenariosDefecto.Text = Textos(20)
        chkEscenarioFiltrosDefecto.Text = Textos(20)
        lblFiltrarIdentificador.Text = Textos(139) & ":"
        lblBotonBuscarIdentificador.Text = Textos(11)
        imgEscenarioVistasOrder.ToolTip = Textos(165)
        CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), Image).ToolTip = Textos(175)
        CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), Image).ToolTip = Textos(176)
        CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPage"), Label).Text = Textos(90)
        CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblOF"), Label).Text = Textos(26)
        CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), Image).ToolTip = Textos(177)
        CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), Image).ToolTip = Textos(178)
        CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblVistaDefectoFiltro"), Label).Text = Textos(157)
        CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblExcel"), Label).Text = Textos(93)
        CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("chkInfoPedidosProcesos"), CheckBox).Text = Textos(199)
        CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("chkInfoPedidosProcesos"), CheckBox).ToolTip = Textos(199)
        whdgSolicitudes.GroupingSettings.EmptyGroupAreaText = Textos(202)

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPantalla") Then
            Dim sVariableJavascriptTextosPantalla As String = "var TextosPantalla = new Array();"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[0]='" & JSText(Textos(21)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[1]='" & JSText(Textos(22)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[2]='" & JSText(Textos(23)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[3]='" & JSText(Textos(24)) & ":" & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[4]='" & JSText(Textos(25)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[5]='" & JSText(Textos(26)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[6]='" & JSText(Textos(27)) & ":" & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[7]='" & JSText(Textos(28)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[8]='" & JSText(Textos(29)) & ":" & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[9]='" & JSText(Textos(30)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[10]='" & JSText(Textos(31)) & ":" & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[11]='" & JSText(Textos(32)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[12]='" & JSText(Textos(33)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[13]='" & JSText(Textos(34)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[14]='" & JSText(Textos(35)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[15]='" & JSText(Textos(36)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[16]='" & JSText(Textos(37)) & ":" & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[17]='" & JSText(Textos(38)) & ":" & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[18]='" & JSText(Textos(39)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[19]='" & JSText(Textos(40)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[20]='" & JSText(Textos(53)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[21]='" & JSText(Textos(54)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[22]='" & JSText(Textos(55)) & ":" & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[23]='" & JSText(Textos(56)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[24]='" & JSText(Textos(57)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[25]='" & JSText(Textos(58)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[26]='" & JSText(Textos(59)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[27]='" & JSText(Textos(60)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[28]='" & JSText(Textos(61)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[29]='" & JSText(Textos(62)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[30]='" & JSText(Textos(63)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[31]='" & JSText(Textos(64)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[32]='" & JSText(Textos(65)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[33]='" & JSText(Textos(66)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[34]='" & JSText(Textos(67)) & ":" & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[35]='" & JSText(Textos(68)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[36]='" & JSText(Textos(69)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[37]='" & JSText(Textos(70)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[38]='" & JSText(Textos(71)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[39]='" & JSText(Textos(72)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[40]='" & JSText(Textos(73)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[41]='" & JSText(Textos(74)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[42]='" & JSText(Textos(75)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[43]='" & JSText(Textos(76)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[44]='" & JSText(Textos(77)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[45]='" & JSText(Textos(78)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[46]='" & JSText(Textos(79)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[47]='" & JSText(Textos(80)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[48]='" & JSText(Textos(81)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[49]='" & JSText(Textos(82)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[50]='" & JSText(Textos(83)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[51]='" & JSText(Textos(84)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[52]='" & JSText(Textos(85)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[53]='" & JSText(Textos(86)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[54]='" & JSText(Textos(87)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[55]='" & JSText(Textos(88)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[56]='" & JSText(Textos(94)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[57]='" & JSText(Textos(95)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[58]='" & JSText(Textos(96)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[59]='" & JSText(Textos(97)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[60]='" & JSText(Textos(98)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[61]='" & JSText(Textos(99)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[62]='" & JSText(Textos(100)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[63]='" & JSText(Textos(101)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[64]='" & JSText(Textos(102)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[65]='" & JSText(Textos(103)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[66]='" & JSText(Textos(104)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[67]='" & JSText(Textos(105)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[68]='" & JSText(Textos(126)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[69]='" & JSText(Textos(127)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[70]='" & JSText(Textos(134)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[71]='" & JSText(Textos(135)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[72]='" & JSText(Textos(136)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[73]='" & JSText(Textos(137)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[74]='" & JSText(Textos(138)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[75]='" & JSText(Textos(141)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[76]='" & JSText(Textos(142)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[77]='" & JSText(Textos(143)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[78]='" & JSText(Textos(144)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[79]='" & JSText(Textos(145)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[80]='" & JSText(Textos(146)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[81]='" & JSText(Textos(147)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[82]='" & JSText(Textos(148)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[83]='" & JSText(Textos(149)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[84]='" & JSText(Textos(150)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[85]='" & JSText(Textos(151)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[86]='" & JSText(Textos(152)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[87]='" & JSText(Textos(153)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[88]='" & JSText(Textos(158)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[89]='" & JSText(Textos(159)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[90]='" & JSText(Textos(160)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[91]='" & JSText(Textos(161)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[92]='" & JSText(Textos(162)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[93]='" & JSText(Textos(163)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[94]='" & JSText(Textos(164)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[95]='" & JSText(Textos(165)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[96]='" & JSText(Textos(166)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[97]='" & JSText(Textos(167)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[98]='" & JSText(Textos(168)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[99]='" & JSText(Textos(169)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[100]='" & JSText(Textos(170)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[101]='" & JSText(Textos(171)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[102]='" & JSText(Textos(172)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[103]='" & JSText(Textos(173)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[104]='" & JSText(Textos(174)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[105]='" & JSText(Textos(181)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[106]='" & JSText(Textos(190)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[107]='" & JSText(Textos(191)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[108]='" & JSText(Textos(192)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[109]='" & JSText(Textos(193)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[110]='" & JSText(Textos(194)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[111]='" & JSText(Textos(195)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[112]='" & JSText(Textos(196)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[113]='" & JSText(Textos(197)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[114]='" & JSText(Textos(198)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[115]='" & JSText(Textos(200)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[116]='" & JSText(Textos(201)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[117]='" & JSText(Textos(204)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[118]='" & JSText(Textos(205)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[119]='" & JSText(Textos(206)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[120]='" & JSText(Textos(207)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[121]='" & JSText(Textos(208)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[122]='" & JSText(Textos(209)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[123]='" & JSText(Textos(210)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[124]='" & JSText(Textos(211)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[125]='" & JSText(Textos(212)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[126]='" & JSText(Textos(227)) & "';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextosPantalla, True)
        End If

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Menu
        lblCargandoDatos.Text = Textos(28)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Seguimiento
    End Sub
    Private Sub Configurar_Columnas_Grid_Solicitudes(ByVal lCamposVista As List(Of Escenario_Campo), ByVal Orden As String)
        Dim dtDatosVista As New DataTable
        Dim dtDatosVistaDesglose As New DataTable
        Dim campoGrid As Infragistics.Web.UI.GridControls.UnboundField
        Dim campoGridBound As Infragistics.Web.UI.GridControls.BoundDataField
        Dim campoDesgloseGrid As Infragistics.Web.UI.GridControls.UnboundField
        Dim campoDesgloseGridBound As Infragistics.Web.UI.GridControls.BoundDataField

        With whdgSolicitudes
            .Rows.Clear()
            .Columns.Clear()
            .GridView.Columns.Clear()
            .Bands("DESGLOSE").Columns.Clear()
        End With
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Seguimiento
        Dim campoGridTemplate As New Infragistics.Web.UI.GridControls.TemplateDataField
        With campoGridTemplate
            .Key = "ACCION_APROBAR"
            .Header.Text = "<input id='chkAprobarCheckAll' type='checkbox' style='margin-right:10px'><input id='btnAprobar' type='submit' class='botonRedondeado' value='" & Textos(179) & "'/>"
            .Header.Tooltip = Textos(179)
            .Hidden = True
            .Header.CssClass = "headerNoWrap"
            .CssClass = "center itemSeleccionable SinSalto"
            .Width = Unit.Pixel(110)
        End With
        whdgSolicitudes.Columns.Add(campoGridTemplate)
        whdgSolicitudes.GridView.Columns.Add(campoGridTemplate)
        campoGridTemplate = New Infragistics.Web.UI.GridControls.TemplateDataField
        With campoGridTemplate
            .Key = "ACCION_RECHAZAR"
            .Header.Text = "<input id='chkRechazarCheckAll' type='checkbox' style='margin-right:10px'><input id='btnRechazar' type='submit' class='botonRedondeado' value='" & Textos(180) & "'/>"
            .Header.Tooltip = Textos(180)
            .Hidden = True
            .Header.CssClass = "headerNoWrap"
            .CssClass = "center itemSeleccionable SinSalto"
            .Width = Unit.Pixel(110)
        End With
        whdgSolicitudes.Columns.Add(campoGridTemplate)
        whdgSolicitudes.GridView.Columns.Add(campoGridTemplate)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Menu
        campoGridTemplate = New Infragistics.Web.UI.GridControls.TemplateDataField
        With campoGridTemplate
            .Key = "ERROR"
            .Header.Text = ""
            .Header.Tooltip = ""
            .Hidden = True
            .Header.CssClass = "celdaImagenWebHierarchical headerNoWrap"
            .CssClass = "celdaImagenWebHierarchical itemSeleccionable SinSalto"
            .Width = Unit.Pixel(20)
        End With
        whdgSolicitudes.Columns.Add(campoGridTemplate)
        whdgSolicitudes.GridView.Columns.Add(campoGridTemplate)
        campoGridTemplate = New Infragistics.Web.UI.GridControls.TemplateDataField
        With campoGridTemplate
            .Key = "ERROR_VALIDACION"
            .Header.Text = ""
            .Header.Tooltip = ""
            .Hidden = True
            .Header.CssClass = "celdaImagenWebHierarchical headerNoWrap"
            .CssClass = "celdaImagenWebHierarchical itemSeleccionable SinSalto"
            .Width = Unit.Pixel(20)
        End With
        whdgSolicitudes.Columns.Add(campoGridTemplate)
        whdgSolicitudes.GridView.Columns.Add(campoGridTemplate)
        campoGridTemplate = New Infragistics.Web.UI.GridControls.TemplateDataField
        With campoGridTemplate
            .Key = "ERROR_INTEGRACION"
            .Header.Text = ""
            .Header.Tooltip = ""
            .Hidden = True
            .Header.CssClass = "celdaImagenWebHierarchical headerNoWrap"
            .CssClass = "celdaImagenWebHierarchical itemSeleccionable SinSalto"
            .Width = Unit.Pixel(20)
        End With
        whdgSolicitudes.Columns.Add(campoGridTemplate)
        whdgSolicitudes.GridView.Columns.Add(campoGridTemplate)
        campoGridTemplate = New Infragistics.Web.UI.GridControls.TemplateDataField
        With campoGridTemplate
            .Key = "RESUMEN"
            .Header.Text = ""
            .Header.Tooltip = ""
            .Hidden = False
            .Header.CssClass = "celdaImagenWebHierarchical headerNoWrap"
            .CssClass = "celdaImagenWebHierarchical itemSeleccionable SinSalto"
            .Width = Unit.Pixel(30)
        End With
        whdgSolicitudes.Columns.Add(campoGridTemplate)
        whdgSolicitudes.GridView.Columns.Add(campoGridTemplate)
        For Each campoVista As Escenario_Campo In lCamposVista
            If campoVista.EsCampoDesglose Then
                If campoVista.TipoCampo = TiposDeDatos.TipoGeneral.TipoDesglose Then
                    ModuloIdioma = TiposDeDatos.ModulosIdiomas.Seguimiento

                    campoDesgloseGridBound = New Infragistics.Web.UI.GridControls.BoundDataField
                    With campoDesgloseGridBound
                        .Key = "LINEA"
                        .DataFieldName = "LINEA"
                        .Header.Text = IIf(campoVista.NombrePersonalizado = "", Textos(203), campoVista.NombrePersonalizado)
                        .Header.Tooltip = IIf(campoVista.NombrePersonalizado = "", Textos(203), campoVista.NombrePersonalizado)
                        .Hidden = False
                        .Width = Unit.Pixel(campoVista.AnchoVisualizacion)
                    End With
                    whdgSolicitudes.Bands("DESGLOSE").Columns.Add(campoDesgloseGridBound)
                    whdgSolicitudes.Bands("DESGLOSE").Columns("LINEA").VisibleIndex = 0
                    ModuloIdioma = TiposDeDatos.ModulosIdiomas.Menu
                ElseIf campoVista.TipoCampoGS = 0 _
                    AndAlso {TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoTextoCorto}.Contains(campoVista.TipoCampo) Then
                    campoDesgloseGrid = New Infragistics.Web.UI.GridControls.UnboundField
                    With campoDesgloseGrid
                        .Key = campoVista.Denominacion_BD
                        .Header.Text = IIf(campoVista.NombrePersonalizado = "", campoVista.Denominacion, campoVista.NombrePersonalizado)
                        .Header.Tooltip = IIf(campoVista.NombrePersonalizado = "", campoVista.Denominacion, campoVista.NombrePersonalizado)
                        .Hidden = IIf(campoVista.Denominacion_BD = "FORM_INSTANCIA", True, False)
                        .Width = Unit.Pixel(campoVista.AnchoVisualizacion)
                        .CssClass = "itemSeleccionable SinSalto"
                        'Si es de tipo numÃ©rico alineamos a la derecha
                        If campoVista.TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico AndAlso Not campoVista.TipoCampoGS = TiposDeDatos.TipoCampoGS.Empresa Then .CssClass &= " itemRightAligment"
                    End With
                    whdgSolicitudes.Bands("DESGLOSE").Columns.Add(campoDesgloseGrid)
                Else
                    campoDesgloseGridBound = New Infragistics.Web.UI.GridControls.BoundDataField
                    With campoDesgloseGridBound
                        .Key = campoVista.Denominacion_BD
                        .DataFieldName = campoVista.Denominacion_BD
                        .Header.Text = IIf(campoVista.NombrePersonalizado = "", campoVista.Denominacion, campoVista.NombrePersonalizado)
                        .Header.Tooltip = IIf(campoVista.NombrePersonalizado = "", campoVista.Denominacion, campoVista.NombrePersonalizado)
                        .Hidden = IIf(campoVista.Denominacion_BD = "FORM_INSTANCIA", True, False)
                        .Width = Unit.Pixel(campoVista.AnchoVisualizacion)
                        .CssClass = "itemSeleccionable SinSalto"
                        'Si es de tipo numÃ©rico alineamos a la derecha
                        If campoVista.TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico AndAlso Not campoVista.TipoCampoGS = TiposDeDatos.TipoCampoGS.Empresa Then .CssClass &= " itemRightAligment"
                    End With
                    whdgSolicitudes.Bands("DESGLOSE").Columns.Add(campoDesgloseGridBound)
                End If
            Else
                If campoVista.TipoCampoGS = 0 _
                            AndAlso {TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoTextoCorto}.Contains(campoVista.TipoCampo) Then
                    campoGrid = New Infragistics.Web.UI.GridControls.UnboundField
                    With campoGrid
                        .Key = campoVista.Denominacion_BD
                        .Header.Text = IIf(campoVista.NombrePersonalizado = "",
                                        IIf(campoVista.EsCampoGeneral AndAlso campoVista.Id = CamposGeneralesVisor.MARCASEGUIMIENTO, "", campoVista.Denominacion),
                                        campoVista.NombrePersonalizado)
                        .Header.Tooltip = IIf(campoVista.NombrePersonalizado = "", campoVista.Denominacion, campoVista.NombrePersonalizado)
                        .Hidden = False
                        .Header.CssClass = "headerNoWrap"
                        .CssClass = "itemSeleccionable SinSalto"
                        .Width = Unit.Pixel(campoVista.AnchoVisualizacion)
                        'Si es de tipo numÃ©rico alineamos a la derecha
                        If campoVista.TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico AndAlso Not campoVista.TipoCampoGS = TiposDeDatos.TipoCampoGS.Empresa Then .CssClass &= " itemRightAligment"
                    End With
                    whdgSolicitudes.Columns.Add(campoGrid)
                    whdgSolicitudes.GridView.Columns.Add(campoGrid)
                Else
                    campoGridBound = New Infragistics.Web.UI.GridControls.BoundDataField
                    With campoGridBound
                        .Key = campoVista.Denominacion_BD
                        .DataFieldName = campoVista.Denominacion_BD
                        .Header.Text = IIf(campoVista.NombrePersonalizado = "",
                                        IIf(campoVista.EsCampoGeneral AndAlso campoVista.Id = CamposGeneralesVisor.MARCASEGUIMIENTO, "", campoVista.Denominacion),
                                        campoVista.NombrePersonalizado)
                        .Header.Tooltip = IIf(campoVista.NombrePersonalizado = "", campoVista.Denominacion, campoVista.NombrePersonalizado)
                        .Hidden = False
                        .Header.CssClass = "headerNoWrap"
                        .CssClass = "itemSeleccionable SinSalto"
                        .Width = Unit.Pixel(campoVista.AnchoVisualizacion)
                        'Si es de tipo numÃ©rico alineamos a la derecha
                        If campoVista.TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico AndAlso Not campoVista.TipoCampoGS = TiposDeDatos.TipoCampoGS.Empresa Then .CssClass &= " itemRightAligment"
                        'si es de tipo fecha formatear con formato usuario
                        If campoVista.EsCampoGeneral Then
                            Select Case campoVista.Id
                                Case CamposGeneralesVisor.SITUACIONACTUAL
                                    .Key = "ETAPA_ACTUAL"
                                    .DataFieldName = "ETAPA_ACTUAL"
                                Case CamposGeneralesVisor.TIPOSOLICITUD  'TIPO_SOLIC
                                    .Key = "TIPO"
                                    .DataFieldName = "TIPO"
                                Case CamposGeneralesVisor.PROVEEDOR
                                    .Key = "PROVEEDOR_INSTANCIA"
                                    .DataFieldName = "PROVEEDOR_INSTANCIA"
                                Case CamposGeneralesVisor.ARTICULO
                                    .Key = "ARTICULO_INSTANCIA"
                                    .DataFieldName = "ARTICULO_INSTANCIA"
                                Case CamposGeneralesVisor.ORIGEN
                                    .Key = "ORIGEN_DEN"
                                    .DataFieldName = "ORIGEN_DEN"
                            End Select
                        End If
                    End With
                    whdgSolicitudes.Columns.Add(campoGridBound)
                    whdgSolicitudes.GridView.Columns.Add(campoGridBound)
                End If
            End If
        Next

        ' Añadimos una columna que ocupe el resto del espacio. Esto es para que salga la barra de scroll si el grid de 2Âº nivel es mÃ¡s largo que el de 1er nivel
        campoGridTemplate = New Infragistics.Web.UI.GridControls.TemplateDataField
        With campoGridTemplate
            .Key = "EmptySpace"
            .Header.CssClass = "headerEmpty"
            .CssClass = "itemEmpty"
            ' Calculamos el tamaÃ±o del primer nivel
            Dim AnchoNivel1 As Double = (From x As GridField In whdgSolicitudes.Columns Where Not x.Hidden Select x.Width.Value).Sum()
            ' Calculamos el tamaÃ±o del segundo nivel
            Dim AnchoNivel2 As Double = 15 + (From x As GridField In whdgSolicitudes.Bands("DESGLOSE").Columns Where Not x.Hidden Select x.Width.Value).Sum()
            ' Establecemos el tamaÃ±o de esta columna
            Dim AnchoColumnaRelleno = AnchoNivel2 - AnchoNivel1
            If AnchoColumnaRelleno < 0 Then
                AnchoColumnaRelleno = 0
                .Hidden = True
            End If
            .Width = Unit.Pixel(AnchoColumnaRelleno)
        End With
        whdgSolicitudes.Columns.Add(campoGridTemplate)
        whdgSolicitudes.GridView.Columns.Add(campoGridTemplate)

        For Each column As String In Split(Orden, ",")
            If column IsNot String.Empty Then _
                whdgSolicitudes.GridView.Behaviors.Sorting.SortedColumns.Add(Split(column, " ")(0),
                            IIf(Split(column, " ")(1).ToLower = "asc", Infragistics.Web.UI.SortDirection.Ascending, Infragistics.Web.UI.SortDirection.Descending))
        Next
    End Sub
    Private Sub Configurar_Columnas_Grid_Solicitudes_Exportar(ByVal lCamposVista As List(Of Escenario_Campo), ByVal Orden As String)
        Dim dtDatosVista As New DataTable
        Dim dtDatosVistaDesglose As New DataTable
        Dim campoGridBoundTemplate As Infragistics.Web.UI.GridControls.BoundDataField
        Dim campoDesgloseGridBoundTemplate As Infragistics.Web.UI.GridControls.BoundDataField

        With whdgExportacion
            .Rows.Clear()
            .Columns.Clear()
            .GridView.Columns.Clear()
            .Bands("DESGLOSE").Columns.Clear()
        End With
        Dim campoGridTemplate As New Infragistics.Web.UI.GridControls.TemplateDataField
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Menu
        With campoGridTemplate
            .Key = "ERROR"
            .Header.Text = "ERROR"
            .Hidden = True
            .Width = Unit.Pixel(20)
        End With
        whdgExportacion.Columns.Add(campoGridTemplate)
        whdgExportacion.GridView.Columns.Add(campoGridTemplate)
        campoGridTemplate = New Infragistics.Web.UI.GridControls.TemplateDataField
        With campoGridTemplate
            .Key = "ERROR_VALIDACION"
            .Header.Text = "ERROR_VALIDACION"
            .Hidden = True
            .Width = Unit.Pixel(20)
        End With
        whdgExportacion.Columns.Add(campoGridTemplate)
        whdgExportacion.GridView.Columns.Add(campoGridTemplate)
        campoGridTemplate = New Infragistics.Web.UI.GridControls.TemplateDataField
        With campoGridTemplate
            .Key = "ERROR_INTEGRACION"
            .Header.Text = "ERROR_INTEGRACION"
            .Hidden = True
            .Width = Unit.Pixel(20)
        End With
        whdgExportacion.Columns.Add(campoGridTemplate)
        whdgExportacion.GridView.Columns.Add(campoGridTemplate)
        For Each campoVista As Escenario_Campo In lCamposVista
            If campoVista.EsCampoDesglose Then
                If campoVista.TipoCampoGS <> TiposDeDatos.TipoCampoGS.Desglose Then
                    campoDesgloseGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
                    With campoDesgloseGridBoundTemplate
                        .Key = campoVista.Denominacion_BD
                        .DataFieldName = campoVista.Denominacion_BD
                        .Header.Text = IIf(campoVista.NombrePersonalizado = "", campoVista.Denominacion, campoVista.NombrePersonalizado)
                        .Hidden = IIf(campoVista.Denominacion_BD = "FORM_INSTANCIA", True, False)
                        .Width = Unit.Pixel(campoVista.AnchoVisualizacion)
                    End With
                    whdgExportacion.Bands("DESGLOSE").Columns.Add(campoDesgloseGridBoundTemplate)
                End If
            Else
                campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
                With campoGridBoundTemplate
                    .Key = campoVista.Denominacion_BD
                    .DataFieldName = campoVista.Denominacion_BD
                    .Header.Text = IIf(campoVista.NombrePersonalizado = "",
                                    IIf(campoVista.EsCampoGeneral AndAlso campoVista.Id = CamposGeneralesVisor.MARCASEGUIMIENTO, "", campoVista.Denominacion),
                                    campoVista.NombrePersonalizado)
                    .Hidden = False
                    .Width = Unit.Pixel(campoVista.AnchoVisualizacion)
                    'si es de tipo fecha formatear con formato usuario
                    If campoVista.EsCampoGeneral Then
                        Select Case campoVista.Id
                            Case CamposGeneralesVisor.TIPOSOLICITUD  'TIPO_SOLIC
                                .Key = "TIPO"
                                .DataFieldName = "TIPO"
                            Case CamposGeneralesVisor.PROVEEDOR
                                .Key = "PROVEEDOR_INSTANCIA"
                                .DataFieldName = "PROVEEDOR_INSTANCIA"
                            Case CamposGeneralesVisor.ARTICULO
                                .Key = "ARTICULO_INSTANCIA"
                                .DataFieldName = "ARTICULO_INSTANCIA"
                            Case CamposGeneralesVisor.ORIGEN
                                .Key = "ORIGEN_DEN"
                                .DataFieldName = "ORIGEN_DEN"
                        End Select
                    End If
                End With
                whdgExportacion.Columns.Add(campoGridBoundTemplate)
                whdgExportacion.GridView.Columns.Add(campoGridBoundTemplate)
            End If
        Next
    End Sub
    ''' <summary>
    ''' Configurar Columnas Grid ConfiguradorVista
    ''' </summary>
    ''' <param name="lCamposVista">lista de campos de la vista</param>
    ''' <remarks>Llamada desde: Page_Load; Tiempo mÃƒÂ¡ximo:0</remarks>
    Private Sub Configurar_Columnas_Grid_ConfiguradorVista(ByVal lCamposVista As List(Of Escenario_Campo), ByVal Orden As String)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Seguimiento
        Dim dtDatosVista As New DataTable
        Dim dtDatosVistaDesglose As New DataTable
        Dim dsDatosVista As New DataSet
        Dim campoGridBoundTemplate As Infragistics.Web.UI.GridControls.BoundDataField
        Dim campoDesgloseGridBoundTemplate As Infragistics.Web.UI.GridControls.BoundDataField
        Dim hayCampoDesglose As Boolean = False
        With whdgConfigVista
            .Columns.Clear()
            .GridView.Columns.Clear()
            whdgConfigVista.Bands("DESGLOSE").Columns.Clear()
        End With

        dtDatosVista.TableName = "SOLICITUDES"
        dtDatosVistaDesglose.TableName = "DESGLOSE"
        For Each campoVista As Escenario_Campo In lCamposVista
            If campoVista.EsCampoDesglose Then
                hayCampoDesglose = True
                If Not dtDatosVistaDesglose.Columns.Contains("FORM_INSTANCIA") Then
                    dtDatosVistaDesglose.Columns.Add("FORM_INSTANCIA", GetType(System.Int32))
                End If
                campoDesgloseGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
                With campoDesgloseGridBoundTemplate
                    If campoVista.TipoCampo = TiposDeDatos.TipoGeneral.TipoDesglose OrElse campoVista.TipoCampoGS = TiposDeDatos.IdsFicticios.NumLinea Then
                        .Key = "LINEA"
                        .DataFieldName = "LINEA"
                        dtDatosVistaDesglose.Columns.Add("LINEA", GetType(System.Int32))
                    Else
                        .Key = campoVista.Denominacion_BD
                        .DataFieldName = campoVista.Denominacion_BD
                    End If
                    .Header.Text = IIf(campoVista.NombrePersonalizado = "",
                                    IIf(campoVista.EsCampoGeneral AndAlso campoVista.Id = CamposGeneralesVisor.MARCASEGUIMIENTO, "",
                                        IIf(.Key = "LINEA", Textos(203), campoVista.Denominacion)),
                                    campoVista.NombrePersonalizado)
                    .Header.Tooltip = .Header.Text
                    .Hidden = False
                    .Width = Unit.Pixel(campoVista.AnchoVisualizacion)
                    If campoVista.TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha Then .DataFormatString = "{0:" & Replace(FSNUser.DateFmt, "mm", "MM") & "}"
                End With
                whdgConfigVista.Bands("DESGLOSE").Columns.Add(campoDesgloseGridBoundTemplate)
                Select Case campoVista.TipoCampo
                    Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto,
                        TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoEditor
                        dtDatosVistaDesglose.Columns.Add(campoVista.Denominacion_BD, GetType(System.String))
                    Case TiposDeDatos.TipoGeneral.TipoNumerico
                        dtDatosVistaDesglose.Columns.Add(campoVista.Denominacion_BD, GetType(System.Int32))
                    Case TiposDeDatos.TipoGeneral.TipoFecha
                        dtDatosVistaDesglose.Columns.Add(campoVista.Denominacion_BD, GetType(System.DateTime))
                    Case TiposDeDatos.TipoGeneral.TipoBoolean
                        dtDatosVistaDesglose.Columns.Add(campoVista.Denominacion_BD, GetType(System.String))
                    Case TiposDeDatos.TipoGeneral.TipoEditor
                        dtDatosVistaDesglose.Columns.Add(campoVista.Denominacion_BD, GetType(System.String))
                    Case Else
                        dtDatosVistaDesglose.Columns.Add(campoVista.Denominacion_BD, GetType(System.String))
                End Select
                If campoVista.TipoCampo = TiposDeDatos.TipoGeneral.TipoDesglose OrElse campoVista.TipoCampoGS = TiposDeDatos.IdsFicticios.NumLinea Then
                    whdgConfigVista.Bands("DESGLOSE").Columns("LINEA").VisibleIndex = 0
                End If
            Else
                Select Case campoVista.TipoCampo
                    Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto,
                        TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoEditor
                        dtDatosVista.Columns.Add(campoVista.Denominacion_BD, GetType(System.String))
                    Case TiposDeDatos.TipoGeneral.TipoNumerico
                        dtDatosVista.Columns.Add(campoVista.Denominacion_BD, GetType(System.Int32))
                    Case TiposDeDatos.TipoGeneral.TipoFecha
                        dtDatosVista.Columns.Add(campoVista.Denominacion_BD, GetType(System.DateTime))
                    Case TiposDeDatos.TipoGeneral.TipoBoolean
                        dtDatosVista.Columns.Add(campoVista.Denominacion_BD, GetType(System.String))
                    Case TiposDeDatos.TipoGeneral.TipoEditor
                        dtDatosVista.Columns.Add(campoVista.Denominacion_BD, GetType(System.String))
                    Case Else
                        dtDatosVista.Columns.Add(campoVista.Denominacion_BD, GetType(System.String))
                End Select
                campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
                With campoGridBoundTemplate
                    .Key = campoVista.Denominacion_BD
                    .DataFieldName = campoVista.Denominacion_BD
                    .Header.Text = IIf(campoVista.NombrePersonalizado = "",
                                    IIf(campoVista.EsCampoGeneral AndAlso campoVista.Id = CamposGeneralesVisor.MARCASEGUIMIENTO, "", campoVista.Denominacion),
                                    campoVista.NombrePersonalizado)
                    .Header.Tooltip = .Header.Text
                    .Hidden = False
                    .Header.CssClass = "headerNoWrap"
                    .CssClass = "itemSeleccionable SinSalto"
                    .Width = Unit.Pixel(campoVista.AnchoVisualizacion)
                    If campoVista.TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha Then
                        .DataFormatString = "{0:" & Replace(FSNUser.DateFmt, "mm", "MM") & "}"
                    ElseIf campoVista.TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico Then
                        .CssClass &= " itemRightAligment"
                    End If
                End With
                whdgConfigVista.Columns.Add(campoGridBoundTemplate)
                whdgConfigVista.GridView.Columns.Add(campoGridBoundTemplate)
            End If
        Next

        If Not dtDatosVista.Columns.Contains("ID") Then
            dtDatosVista.Columns.Add("ID", GetType(System.Int32))
            campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
            With campoGridBoundTemplate
                .Key = "ID"
                .DataFieldName = "ID"
                .Hidden = True
            End With
            whdgConfigVista.Columns.Add(campoGridBoundTemplate)
            whdgConfigVista.GridView.Columns.Add(campoGridBoundTemplate)
        End If

        ' AÃƒÂ±adimos una columna que ocupe el resto del espacio. Esto es para que salga la barra de scroll si el grid de 2Ã‚Âº nivel es mÃƒÂ¡s largo que el de 1er nivel
        Dim campoGridTemplate As Infragistics.Web.UI.GridControls.TemplateDataField = New Infragistics.Web.UI.GridControls.TemplateDataField
        With campoGridTemplate
            .Key = "EmptySpace"
            .Header.CssClass = "headerEmpty"
            .CssClass = "itemEmpty"
            ' Calculamos el tamaÃƒÂ±o del primer nivel
            Dim AnchoNivel1 As Double = (From x As GridField In whdgConfigVista.Columns Where Not x.Hidden Select x.Width.Value).Sum()
            ' Calculamos el tamaÃƒÂ±o del segundo nivel
            Dim AnchoNivel2 As Double = 15 + (From x As GridField In whdgConfigVista.Bands("DESGLOSE").Columns Where Not x.Hidden Select x.Width.Value).Sum()
            ' Establecemos el tamaÃƒÂ±o de esta columna
            Dim AnchoColumnaRelleno = AnchoNivel2 - AnchoNivel1
            If AnchoColumnaRelleno < 0 Then
                AnchoColumnaRelleno = 0
                .Hidden = True
            End If
            .Width = Unit.Pixel(AnchoColumnaRelleno)
        End With
        whdgConfigVista.Columns.Add(campoGridTemplate)
        whdgConfigVista.GridView.Columns.Add(campoGridTemplate)

        Dim rowVista As DataRow = dtDatosVista.NewRow
        For Each column As Escenario_Campo In lCamposVista.Where(Function(x) Not x.EsCampoDesglose).ToList
            Select Case column.TipoCampo
                Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto,
                    TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoEditor
                    rowVista(column.Denominacion_BD) = "abcd"
                Case TiposDeDatos.TipoGeneral.TipoNumerico
                    rowVista(column.Denominacion_BD) = 123
                Case TiposDeDatos.TipoGeneral.TipoFecha
                    rowVista(column.Denominacion_BD) = Now.Date
                Case TiposDeDatos.TipoGeneral.TipoBoolean
                    rowVista(column.Denominacion_BD) = Textos(110)
                Case TiposDeDatos.TipoGeneral.TipoEditor
                    rowVista(column.Denominacion_BD) = "abcd"
                Case Else
                    rowVista(column.Denominacion_BD) = "abcd"
            End Select
        Next
        dtDatosVista.Rows.Add(rowVista)

        dsDatosVista.Tables.Add(dtDatosVista)
        If hayCampoDesglose Then
            Dim rowVistaDesglose As DataRow = dtDatosVistaDesglose.NewRow
            rowVistaDesglose("FORM_INSTANCIA") = 123
            For Each column As Escenario_Campo In lCamposVista.Where(Function(x) x.EsCampoDesglose).ToList
                Select Case column.TipoCampo
                    Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto,
                        TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoEditor
                        rowVistaDesglose(column.Denominacion_BD) = "abcd"
                    Case TiposDeDatos.TipoGeneral.TipoNumerico
                        rowVistaDesglose(column.Denominacion_BD) = 123
                        If column.TipoCampoGS = TiposDeDatos.IdsFicticios.NumLinea Then
                            rowVistaDesglose("LINEA") = 1
                        End If
                    Case TiposDeDatos.TipoGeneral.TipoFecha
                        rowVistaDesglose(column.Denominacion_BD) = Now.Date
                    Case TiposDeDatos.TipoGeneral.TipoBoolean
                        rowVistaDesglose(column.Denominacion_BD) = Textos(110)
                    Case TiposDeDatos.TipoGeneral.TipoEditor
                        rowVistaDesglose(column.Denominacion_BD) = "abcd"
                    Case Else
                        rowVistaDesglose(column.Denominacion_BD) = "abcd"
                End Select
            Next
            dtDatosVistaDesglose.Rows.Add(rowVistaDesglose)
            dsDatosVista.Tables.Add(dtDatosVistaDesglose)

            Dim parentCols(0) As DataColumn
            Dim childCols(0) As DataColumn
            parentCols(0) = dsDatosVista.Tables("SOLICITUDES").Columns("ID")
            childCols(0) = dsDatosVista.Tables("DESGLOSE").Columns("FORM_INSTANCIA")
            dsDatosVista.Relations.Add("SOLICITUD_DESGLOSE", parentCols, childCols, False)
        End If

        InsertarEnCache("dsConfigVista_" & FSNUser.Cod, dsDatosVista)

        whdgConfigVista.GridView.Behaviors.Sorting.SortedColumns.Clear()
        For Each columna As String In Split(Orden, ",")
            If columna IsNot String.Empty Then
                whdgConfigVista.GridView.Behaviors.Sorting.SortedColumns.Add(Split(columna, " ")(0),
                                IIf(Split(columna, " ")(1) = "ASC", Infragistics.Web.UI.SortDirection.Ascending, Infragistics.Web.UI.SortDirection.Descending))
            End If
        Next

        With whdgConfigVista
            .Rows.Clear()
            .GridView.ClearDataSource()
            .DataSource = dsDatosVista
            .GridView.DataSource = .DataSource
            .DataKeyFields = "ID"
            .DataMember = "SOLICITUDES"
            .DataBind()
            .ExpandAll()
        End With
    End Sub
    Private Sub Paginador(ByVal _pageCount As Integer, ByVal _pageNumber As Integer)
        Dim pagerList As DropDownList = DirectCast(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("PagerPageList"), DropDownList)
        pagerList.Items.Clear()
        If _pageCount = 0 Then pagerList.Items.Add("")
        For i As Integer = 1 To _pageCount
            pagerList.Items.Add(i.ToString())
        Next
        CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblCount"), Label).Text = _pageCount
        If _pageCount > 0 Then pagerList.SelectedIndex = _pageNumber - 1
        Dim Desactivado As Boolean = (_pageNumber = 1)
        With CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        With CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        Desactivado = (_pageCount = 1 OrElse _pageNumber = _pageCount)
        With CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        With CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
    End Sub
    ''' <summary>
    ''' Exporta a Excel
    ''' </summary>
    ''' <remarks>Llamada desde: Page_load; Tiempo máximo:0,1</remarks>
    Private Sub ExportarExcel()
		With whdgExcelExporter
			ModuloIdioma = TiposDeDatos.ModulosIdiomas.Seguimiento
			Select Case iTipoVisor
				Case TipoVisor.Facturas
					.DownloadName = Textos(224)
				Case TipoVisor.SolicitudesQA
					.DownloadName = Textos(230)
				Case TipoVisor.Encuestas
					.DownloadName = Textos(233)
				Case Else
					.DownloadName = Textos(0)
			End Select

			.DataExportMode = DataExportMode.AllDataInDataSource
			.EnableStylesExport = False
			.WorkbookFormat = Infragistics.Documents.Excel.WorkbookFormat.Excel2007
			.Export(whdgExportacion)
		End With
	End Sub
#Region "Page Methods"
	''' <summary>
	''' Obtiene los escenarios que el usuario tiene creados. Creamos una estructura con los escenarios, sus filtros y vistas. 
	''' De no tener ningun escenario creado, mostraremos uno por defecto con las tareas pendientes y los filtros por defecto
	''' pendientes, guardadas, en curso, rechazadas, anuladas, finalizadas y cerradas
	''' </summary>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Obtener_Escenarios_Usuario(ByVal iTipoVisor As Integer) As Object
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			Dim serializer As New JavaScriptSerializer
			Dim oResultado As Object = cEscenarios.FSN_Get_Escenarios_Usuario(FSNUser.Cod, FSNUser.Idioma, iTipoVisor, FSNUser.PMOcultarEscenarioDefecto, FSNUser.PMPermitirEditarEscenariosCompartidosNoCreadosPorUsuario,
																			  FSNUser.PMRestringirEdicionEscenariosDEPUsu, FSNUser.PMRestringirEdicionEscenariosUONUsu, FSNUser.PMRestringirEdicionEscenariosUONsPerfilUsu)
			Return serializer.Serialize(oResultado)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Obtiene las carpetas creadas por el usuario para organizar los escenarios
	''' </summary>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Obtener_Carpetas_Usuario(ByVal iTipoVisor As Integer) As String
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			Dim serializer As New JavaScriptSerializer
			Return serializer.Serialize(cEscenarios.FSN_Get_Carpetas_Usuario(FSNUser.Cod, iTipoVisor))
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Inserta una nueva carpeta para organizar los escenarios para el usuario
	''' </summary>
	''' <param name="Nombre">Nombre para la carpeta</param>
	''' <param name="Carpeta">Id de la carpeta contenedora de la carpeta a crear</param>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Agregar_Carpeta_Usuario(ByVal Nombre As String, ByVal Carpeta As Integer, ByVal iTipoVisor As Integer) As List(Of cn_fsTreeViewItem)
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			Return cEscenarios.FSN_Insert_Carpeta_Usuario(FSNUser.Cod, Nombre, Carpeta, iTipoVisor)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Modifica el nombre de la carpeta 
	''' </summary>
	''' <param name="Nombre">Nuevo nombre para la carpeta</param>
	''' <param name="Carpeta">Id de la carpeta a modificar</param>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Editar_Carpeta_Usuario(ByVal Nombre As String, ByVal Carpeta As Integer, ByVal iTipoVisor As Integer) As List(Of cn_fsTreeViewItem)
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			Return cEscenarios.FSN_Update_Carpeta_Usuario(Carpeta, FSNUser.Cod, Nombre, iTipoVisor)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Elimina la carpeta, las subcarpetas que contenga y todos los escenarios asociados a estas carpetas y subcarpetas se asociaran a la raiz, es decir,
	''' se desasociaran de las carpetas y subcarpetas eliminadas
	''' </summary>
	''' <param name="Carpeta">Id de la carpeta a eliminar</param>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Eliminar_Carpeta_Usuario(ByVal Carpeta As Integer, ByVal iTipoVisor As Integer) As List(Of cn_fsTreeViewItem)
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			Return cEscenarios.FSN_Delete_Carpeta_Usuario(Carpeta, FSNUser.Cod, iTipoVisor)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Simula el mover una carpeta de una ubicacion a otra
	''' </summary>
	''' <param name="Id">Id de la carpeta a mover</param>
	''' <param name="Id_Destino">Id de la carpeta que contendra a la carpeta a mover</param>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Pegar_Carpeta_Usuario(ByVal Id As Integer, ByVal Id_Destino As Integer, ByVal iTipoVisor As Integer) As List(Of cn_fsTreeViewItem)
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			Return cEscenarios.FSN_Paste_Carpeta_Usuario(Id, Id_Destino, FSNUser.Cod, iTipoVisor)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Obtiene los datos del escenario elegido, además de enviar los textos para el wizard y los datos necesarios para formar el wizard del escenario
	''' </summary>
	''' <param name="IdEscenario"></param>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Obtener_Escenario(ByVal IdEscenario As Integer, ByVal Edicion As Boolean, ByVal iTipoVisor As Integer) As Object
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			Dim serializer As New JavaScriptSerializer
			Dim oResultado As Object = cEscenarios.FSN_Get_Escenario_Usuario(IdEscenario, FSNUser.Cod, FSNUser.Idioma, iTipoVisor, Edicion, FSNUser.PMPermitirEditarEscenariosCompartidosNoCreadosPorUsuario,
																			 FSNUser.PMRestringirEdicionEscenariosDEPUsu, FSNUser.PMRestringirEdicionEscenariosUONUsu, FSNUser.PMRestringirEdicionEscenariosUONsPerfilUsu)
			Return serializer.Serialize(oResultado)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Obtiene los campos de un formulario dado
	''' </summary>
	''' <param name="IdFormulario">Id del formulario</param>
	''' <param name="Paso">1-filtro 2-wizard 3-Vista</param>
	''' <returns>campos de un formulario dado</returns>
	''' <remarks>Llamada desde: VisorSolicitudes_NuevoEscenario.js; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Obtener_Campos_Formulario(ByVal IdFormulario As Integer, ByVal Paso As Integer) As Dictionary(Of String, Escenario_Campo)
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))

			Return cEscenarios.FSN_Get_Campos_Formulario(FSNUser.CodPersona, FSNUser.Idioma, IdFormulario, Paso)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Guarda los datos de un escenario dado. Si el id del escenario es 0 se añade uno nuevo
	''' </summary>
	''' <param name="EscenarioNuevo">Datos del escenario pasados como string para parsearlo a tipo Escenario</param>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Insert_Escenario(ByVal oEscenario As String, ByVal iTipoVisor As Integer) As String
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim serializer As New JavaScriptSerializer
			Dim cEscenario As Escenario = serializer.Deserialize(Of Escenario)(oEscenario)
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			Dim oResultado As Escenario = cEscenarios.FSN_Insert_Escenario_Usuario(FSNUser.Cod, FSNUser.Idioma, cEscenario, FSNUser.PMOcultarEscenarioDefecto, FSNUser.PMPermitirEditarEscenariosCompartidosNoCreadosPorUsuario,
																				   FSNUser.PMRestringirEdicionEscenariosDEPUsu, FSNUser.PMRestringirEdicionEscenariosUONUsu, FSNUser.PMRestringirEdicionEscenariosUONsPerfilUsu)
			Return serializer.Serialize(oResultado)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Update_Escenario(ByVal oEscenario As String, ByVal iTipoVisor As Integer) As String
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim serializer As New JavaScriptSerializer
			Dim cEscenario As Escenario = serializer.Deserialize(Of Escenario)(oEscenario)
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			Dim oResultado As Escenario = cEscenarios.FSN_Edit_Escenario_Usuario(FSNUser.Cod, FSNUser.Idioma, cEscenario, FSNUser.PMPermitirEditarEscenariosCompartidosNoCreadosPorUsuario, FSNUser.PMRestringirEdicionEscenariosDEPUsu,
																				 FSNUser.PMRestringirEdicionEscenariosUONUsu, FSNUser.PMRestringirEdicionEscenariosUONsPerfilUsu)
			Return serializer.Serialize(oResultado)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Elimina el escenario seleccionado, ademas de sus filtro y vistas asociados
	''' </summary>
	''' <param name="idEscenario">Id del escenarioa borrar</param>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Sub Delete_Escenario(ByVal idEscenario As Integer, ByVal carpeta As Integer)
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			Dim serializer As New JavaScriptSerializer
			cEscenarios.FSN_Delete_Escenario_Usuario(FSNUser.Cod, idEscenario, carpeta)
		Catch ex As Exception
			Throw ex
		End Try
	End Sub
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Insert_Escenario_Filtro(ByVal Filtro As String, ByVal iTipoVisor As Integer) As String
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim serializer As New JavaScriptSerializer
			Dim oFiltro As EscenarioFiltro = serializer.Deserialize(Of EscenarioFiltro)(Filtro)
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			Dim oResultado As EscenarioFiltro
			oResultado = cEscenarios.FSN_Insert_Escenario_Filtro_Usuario(FSNUser.Cod, FSNUser.Idioma, oFiltro, iTipoVisor)

			Return serializer.Serialize(oResultado)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Update_Escenario_Filtro(ByVal Filtro As String, ByVal iTipoVisor As Integer) As Object
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim serializer As New JavaScriptSerializer
			Dim oFiltro As EscenarioFiltro = serializer.Deserialize(Of EscenarioFiltro)(Filtro)
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			Dim oResultado As EscenarioFiltro
			oResultado = cEscenarios.FSN_Edit_Escenario_Filtro_Usuario(FSNUser.Cod, FSNUser.Idioma, oFiltro, iTipoVisor)

			Return serializer.Serialize(oResultado)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Sub Delete_Escenario_Filtro(ByVal idEscenario As Integer, ByVal idEscenarioFiltro As Integer)
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim serializer As New JavaScriptSerializer
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			cEscenarios.FSN_Delete_Escenario_Filtro_Usuario(idEscenario, idEscenarioFiltro)
		Catch ex As Exception
			Throw ex
		End Try
	End Sub
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Insert_Escenario_Vista(ByVal Vista As String, ByVal iTipoVisor As Integer) As String
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim serializer As New JavaScriptSerializer
			Dim oVista As EscenarioVista = serializer.Deserialize(Of EscenarioVista)(Vista)
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			Dim oResultado As EscenarioVista
			oResultado = cEscenarios.FSN_Insert_Escenario_Vista_Usuario(FSNUser.Cod, FSNUser.Idioma, oVista, iTipoVisor)

			Return serializer.Serialize(oResultado)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Update_Escenario_Vista(ByVal Vista As String, ByVal iTipoVisor As Integer) As String
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim serializer As New JavaScriptSerializer
			Dim oVista As EscenarioVista = serializer.Deserialize(Of EscenarioVista)(Vista)
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			Dim oResultado As EscenarioVista
			oResultado = cEscenarios.FSN_Edit_Escenario_Vista_Usuario(FSNUser.Cod, FSNUser.Idioma, oVista, iTipoVisor)

			Return serializer.Serialize(oResultado)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Sub Delete_Escenario_Vista(ByVal idEscenarioVista As Integer, ByVal posicion As Integer, ByVal idEscenario As Integer)
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim serializer As New JavaScriptSerializer
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			cEscenarios.FSN_Delete_Escenario_Vista_Usuario(idEscenarioVista, idEscenario)
		Catch ex As Exception
			Throw ex
		End Try
	End Sub
	''' <summary>
	''' Metodo que se llama desde cualquier campo de tipo lista para devolver los item para rellenar la lista
	''' </summary>
	''' <param name="Campo_Escenario">Info del campo de tipo lista para obtener los item</param>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Obtener_Opciones_Lista(ByVal Campo_Escenario As Object, ByVal Escenario As Object, ByVal ValorCampoPadre As String, ByVal iTipoVisor As Integer) As List(Of cn_fsItem)
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim serializer As New JavaScriptSerializer
			Dim oCampo_Escenario As Escenario_Campo = serializer.Deserialize(Of Escenario_Campo)(Campo_Escenario)
			Dim oEscenario As Escenario = serializer.Deserialize(Of Escenario)(Escenario)
			Dim lOpcionesLista As New List(Of cn_fsItem)
			Dim iOpcionLista As cn_fsItem

			If oCampo_Escenario.EsCampoGeneral Then
				Select Case oCampo_Escenario.Id
					Case CamposGeneralesVisor.ORIGEN   'Departamento
						Dim cDepartamentos As FSNServer.Departamentos
						cDepartamentos = FSNServer.Get_Object(GetType(FSNServer.Departamentos))
						cDepartamentos.LoadData(, , , , , FSNUser.Pyme)
						For Each departamento As DataRow In cDepartamentos.Data.Tables(0).Select("COD IS NOT NULL")
							iOpcionLista = New cn_fsItem
							iOpcionLista.value = departamento("COD").ToString
							iOpcionLista.text = departamento("DEN").ToString
							lOpcionesLista.Add(iOpcionLista)
						Next
					Case CamposGeneralesVisor.TIPOSOLICITUD
						Dim cTiposSolicitud As FSNServer.Escenarios
						cTiposSolicitud = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
						Dim idsSolicitud As String = String.Empty
						For Each solicitudFormulario As String In oEscenario.SolicitudFormularioVinculados.Keys
							idsSolicitud &= (IIf(idsSolicitud Is String.Empty, "", ",") & solicitudFormulario)
						Next
						lOpcionesLista = cTiposSolicitud.FSN_Get_Opciones_Lista_TipoSolicitud(FSNUser.Cod, FSNUser.Idioma, idsSolicitud, iTipoVisor)
					Case CamposGeneralesVisor.ESTADOHOMOLOGACION
						Dim cOpcionesLista As FSNServer.Escenarios
						cOpcionesLista = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
						lOpcionesLista = cOpcionesLista.FSN_Get_Opciones_Lista_EstadoHomologacion(FSNUser.Idioma, oEscenario.SolicitudFormularioVinculados)
					Case Else
						iOpcionLista = New cn_fsItem
						iOpcionLista.value = ""
						iOpcionLista.text = ""
						lOpcionesLista.Add(iOpcionLista)
				End Select
			Else
				Select Case oCampo_Escenario.TipoCampoGS
					Case TiposDeDatos.TipoCampoGS.FormaPago
						Dim oFormasPago As FSNServer.FormasPago
						oFormasPago = FSNServer.Get_Object(GetType(FSNServer.FormasPago))
						oFormasPago.LoadData(FSNUser.Idioma.ToString())
						For Each formaPago As DataRow In oFormasPago.Data.Tables(0).Rows
							iOpcionLista = New cn_fsItem
							With iOpcionLista
								.value = formaPago("COD").ToString
								.text = formaPago("COD").ToString & " - " & formaPago("DEN").ToString
							End With
							lOpcionesLista.Add(iOpcionLista)
						Next
					Case TiposDeDatos.TipoCampoGS.Moneda
						Dim oMonedas As FSNServer.Monedas
						oMonedas = FSNServer.Get_Object(GetType(FSNServer.Monedas))
						oMonedas.LoadData(FSNUser.Idioma.ToString())
						For Each moneda As DataRow In oMonedas.Data.Tables(0).Rows
							iOpcionLista = New cn_fsItem
							With iOpcionLista
								.value = moneda("COD").ToString
								.text = moneda("DEN").ToString
							End With
							lOpcionesLista.Add(iOpcionLista)
						Next
					Case TiposDeDatos.TipoCampoGS.Unidad, TiposDeDatos.TipoCampoGS.UnidadPedido
						Dim oUnidades As FSNServer.Unidades
						oUnidades = FSNServer.Get_Object(GetType(FSNServer.Unidades))
						oUnidades.LoadData(FSNUser.Idioma.ToString())
						For Each unidad As DataRow In oUnidades.Data.Tables(0).Rows
							iOpcionLista = New cn_fsItem
							With iOpcionLista
								.value = unidad("COD").ToString
								.text = unidad("COD").ToString & " - " & unidad("DEN").ToString
							End With
							lOpcionesLista.Add(iOpcionLista)
						Next
					Case TiposDeDatos.TipoCampoGS.Dest
						Dim oDests As FSNServer.Destinos
						oDests = FSNServer.Get_Object(GetType(FSNServer.Destinos))
						oDests.LoadData(FSNUser.Idioma.ToString(), FSNUser.CodPersona)
						For Each destino As DataRow In oDests.Data.Tables(0).Rows
							iOpcionLista = New cn_fsItem
							With iOpcionLista
								.value = destino("COD").ToString
								.text = destino("COD").ToString & " - " & destino("DEN").ToString & " (" & destino("POB").ToString & ")"
							End With
							lOpcionesLista.Add(iOpcionLista)
						Next
					Case TiposDeDatos.TipoCampoGS.Departamento
						Dim oDepartamentos As FSNServer.Departamentos
						oDepartamentos = FSNServer.Get_Object(GetType(FSNServer.Departamentos))
						oDepartamentos.LoadData()
						For Each departamento As DataRow In oDepartamentos.Data.Tables(0).Rows
							If Not IsDBNull(departamento("COD")) AndAlso Not IsDBNull(departamento("COD")) Then
								iOpcionLista = New cn_fsItem
								With iOpcionLista
									.value = departamento("COD").ToString
									.text = departamento("COD").ToString & " - " & departamento("DEN").ToString
								End With
								lOpcionesLista.Add(iOpcionLista)
							End If
						Next
					Case TiposDeDatos.TipoCampoGS.OrganizacionCompras
						Dim oOrganizacionesCompras As FSNServer.OrganizacionesCompras
						oOrganizacionesCompras = FSNServer.Get_Object(GetType(FSNServer.OrganizacionesCompras))
						oOrganizacionesCompras.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil)
						For Each orgCompras As DataRow In oOrganizacionesCompras.Data.Tables(0).Rows
							iOpcionLista = New cn_fsItem
							With iOpcionLista
								.value = orgCompras("COD").ToString
								.text = orgCompras("COD").ToString & " - " & orgCompras("DEN").ToString
							End With
							lOpcionesLista.Add(iOpcionLista)
						Next
					Case TiposDeDatos.TipoCampoGS.Centro
						Dim oCentros As FSNServer.Centros
						oCentros = FSNServer.Get_Object(GetType(FSNServer.Centros))
						oCentros.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil)
						For Each centro As DataRow In oCentros.Data.Tables(0).Rows
							iOpcionLista = New cn_fsItem
							With iOpcionLista
								.value = centro("COD").ToString
								.text = centro("COD").ToString & " - " & centro("DEN").ToString
							End With
							lOpcionesLista.Add(iOpcionLista)
						Next
					Case TiposDeDatos.TipoCampoGS.Almacen
						Dim oAlmac As FSNServer.Almacenes
						oAlmac = FSNServer.Get_Object(GetType(FSNServer.Almacenes))
						oAlmac.LoadData()
						For Each almacen As DataRow In oAlmac.Data.Tables(0).Rows
							iOpcionLista = New cn_fsItem
							With iOpcionLista
								.value = almacen("ID").ToString
								.text = almacen("COD").ToString & " - " & almacen("DEN").ToString
							End With
							lOpcionesLista.Add(iOpcionLista)
						Next
					Case TiposDeDatos.TipoCampoGS.ProveedorERP
						Dim oProvesERP As FSNServer.CProveERPs
						oProvesERP = FSNServer.Get_Object(GetType(FSNServer.CProveERPs))
						Dim dsResultado As DataSet = oProvesERP.CargarProveedoresERPtoDS()
						For Each proveERP As DataRow In dsResultado.Tables(0).Rows
							iOpcionLista = New cn_fsItem
							With iOpcionLista
								.value = proveERP("COD").ToString
								.text = proveERP("COD").ToString & " - " & proveERP("DEN").ToString
							End With
							lOpcionesLista.Add(iOpcionLista)
						Next
					Case TiposDeDatos.TipoCampoGS.Pais
						Dim cListaPaises As FSNServer.Paises
						cListaPaises = FSNServer.Get_Object(GetType(FSNServer.Paises))
						cListaPaises.LoadData(FSNUser.Idioma)
						iOpcionLista = New cn_fsItem
						iOpcionLista.value = ""
						iOpcionLista.text = ""
						lOpcionesLista.Add(iOpcionLista)
						For Each pais As DataRow In cListaPaises.Data.Tables(0).Rows
							iOpcionLista = New cn_fsItem
							With iOpcionLista
								.value = pais("COD").ToString
								.text = pais("DEN").ToString
							End With
							lOpcionesLista.Add(iOpcionLista)
						Next
					Case TiposDeDatos.TipoCampoGS.Provincia
						If ValorCampoPadre IsNot String.Empty Then
							Dim cListaProvincias As FSNServer.Provincias
							cListaProvincias = FSNServer.Get_Object(GetType(FSNServer.Provincias))
							cListaProvincias.Pais = ValorCampoPadre
							cListaProvincias.LoadData(FSNUser.Idioma)
							iOpcionLista = New cn_fsItem
							iOpcionLista.value = ""
							iOpcionLista.text = ""
							lOpcionesLista.Add(iOpcionLista)
							For Each pais As DataRow In cListaProvincias.Data.Tables(0).Rows
								iOpcionLista = New cn_fsItem
								With iOpcionLista
									.value = pais("PAICOD").ToString & "#" & pais("COD").ToString
									.text = pais("DEN").ToString
								End With
								lOpcionesLista.Add(iOpcionLista)
							Next
						End If
					Case TiposDeDatos.TipoCampoGS.TipoPedido
						Dim oTiposPedido As FSNServer.TiposPedido
						Dim dsTiposPedidos As DataSet
						oTiposPedido = FSNServer.Get_Object(GetType(FSNServer.TiposPedido))
						dsTiposPedidos = oTiposPedido.LoadData(FSNUser.Idioma)
						For Each tipoPedido As DataRow In dsTiposPedidos.Tables(0).Rows
							iOpcionLista = New cn_fsItem
							With iOpcionLista
								.value = tipoPedido("COD").ToString
								.text = tipoPedido("DEN").ToString
							End With
							lOpcionesLista.Add(iOpcionLista)
						Next
					Case Else
						If oCampo_Escenario.TipoCampo = TiposDeDatos.TipoGeneral.TipoBoolean Then
							Dim Textos As DataTable
							Dim oDict As Dictionary
							oDict = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
							oDict.LoadData(TiposDeDatos.ModulosIdiomas.Seguimiento, FSNUser.Idioma)
							Textos = oDict.Data.Tables(0)

							Dim cOpcionesLista As FSNServer.Escenarios
							cOpcionesLista = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
							lOpcionesLista = cOpcionesLista.Get_Opciones_Lista_Boolean(Textos)
						Else
							Dim cOpcionesLista As FSNServer.Escenarios
							cOpcionesLista = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
							lOpcionesLista = cOpcionesLista.FSN_Get_Opciones_Lista(FSNUser.Idioma, oCampo_Escenario.Id, oCampo_Escenario.TipoCampo)
						End If
				End Select
			End If

			Return lOpcionesLista
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Obtiene los usuarios posibles con los que se puede compartir un escenario
	''' </summary>
	''' <returns>Lista con los usuarios posibles con el formato {id:-, name:-} compatible con la extension jQuery.tokenInput</returns>
	''' <remarks></remarks>
	<Services.WebMethod(EnableSession:=True)>
	<Script.Services.ScriptMethod()>
	Public Shared Function Obtener_UONs_Compartir(ByVal q As String, ByVal iTipoVisor As Integer, ByVal DeOtroUsuario As Boolean) As IList
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim cEscenarios As Escenarios
			Dim dsUONsCompartir As DataSet
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			'Gestion de restricciones. Si el escenario es propio utilizamos las restricciones de creacion, si no, las de edicion de escenarios de otros usuarios.
			Dim RestriccionUONUsu, RestriccionDepUsu, RestriccionUONsPerfilUsu As Boolean
			With FSNUser
				If DeOtroUsuario Then
					RestriccionUONUsu = .PMRestringirEdicionEscenariosUONUsu
					RestriccionDepUsu = .PMRestringirEdicionEscenariosDEPUsu
					RestriccionUONsPerfilUsu = .PMRestringirEdicionEscenariosUONsPerfilUsu
				Else
					RestriccionUONUsu = .PMRestringirCrearEscenariosUsuariosUONUsu
					RestriccionDepUsu = .PMRestringirCrearEscenariosUsuariosDEPUsu
					RestriccionUONsPerfilUsu = .PMRestringirCrearEscenariosUsuariosUONsPerfilUsu
				End If

				dsUONsCompartir = cEscenarios.FSN_Obtener_UONsCompartir(.Cod, .Idioma, .UON1, .UON2, .UON3, .Dep, RestriccionUONUsu, RestriccionDepUsu, RestriccionUONsPerfilUsu, q)
			End With

			Dim oUonsTodos = dsUONsCompartir.Tables(0).Rows.OfType(Of DataRow).Where(Function(x) x("ESDEPARTAMENTO") = 0 AndAlso x("PERMISO") = 1).Select(Function(x) New With {Key .id = x("COD"), .name = x("DEN")}).ToList()
			If dsUONsCompartir.Tables.Count > 1 Then
				oUonsTodos = oUonsTodos.Union(dsUONsCompartir.Tables(1).Rows.OfType(Of DataRow).Where(Function(x) x("ESDEPARTAMENTO") = 0 AndAlso x("PERMISO") = 1).Select(Function(x) New With {Key .id = x("COD") & "###", .name = x("COD") & " - " & x("DEN")}).ToList()).ToList()
				oUonsTodos = oUonsTodos.Union(dsUONsCompartir.Tables(1).Rows.OfType(Of DataRow).Where(Function(x) x("ESDEPARTAMENTO") = 1 AndAlso x("PERMISO") = 1).Select(Function(x) New With {Key .id = "###" & x("COD"), .name = x("COD") & " - " & x("DEN")}).ToList()).ToList()
				If dsUONsCompartir.Tables.Count > 2 Then
					oUonsTodos = oUonsTodos.Union(dsUONsCompartir.Tables(2).Rows.OfType(Of DataRow).Where(Function(x) x("ESDEPARTAMENTO") = 0 AndAlso x("PERMISO") = 1).Select(Function(x) New With {Key .id = x("UON1") & "#" & x("COD") & "##", .name = x("UON1") & " - " & x("COD") & " - " & x("DEN")}).ToList()).ToList()
					oUonsTodos = oUonsTodos.Union(dsUONsCompartir.Tables(2).Rows.OfType(Of DataRow).Where(Function(x) x("ESDEPARTAMENTO") = 1 AndAlso x("PERMISO") = 1).Select(Function(x) New With {Key .id = x("UON1") & "###" & x("COD"), .name = x("UON1") & " - " & x("COD") & " - " & x("DEN")}).ToList()).ToList()
					If dsUONsCompartir.Tables.Count > 3 Then
						oUonsTodos = oUonsTodos.Union(dsUONsCompartir.Tables(3).Rows.OfType(Of DataRow).Where(Function(x) x("ESDEPARTAMENTO") = 0 AndAlso x("PERMISO") = 1).Select(Function(x) New With {Key .id = x("UON1") & "#" & x("UON2") & "#" & x("COD") & "#", .name = x("UON1") & " - " & x("UON2") & " - " & x("COD") & " - " & x("DEN")}).ToList()).ToList()
						oUonsTodos = oUonsTodos.Union(dsUONsCompartir.Tables(3).Rows.OfType(Of DataRow).Where(Function(x) x("ESDEPARTAMENTO") = 1 AndAlso x("PERMISO") = 1).Select(Function(x) New With {Key .id = x("UON1") & "#" & x("UON2") & "##" & x("COD"), .name = x("UON1") & " - " & x("UON2") & " - " & x("COD") & " - " & x("DEN")}).ToList()).ToList()
						If dsUONsCompartir.Tables.Count > 4 Then
							oUonsTodos = oUonsTodos.Union(dsUONsCompartir.Tables(4).Rows.OfType(Of DataRow).Where(Function(x) x("ESDEPARTAMENTO") = 1 AndAlso x("PERMISO") = 1).Select(Function(x) New With {Key .id = x("UON1") & "#" & x("UON2") & "#" & x("UON3") & "#" & x("COD"), .name = x("UON1") & " - " & x("UON2") & " - " & x("UON3") & " - " & x("COD") & " - " & x("DEN")}).ToList()).ToList()
						End If
					End If
				End If
			End If
			Return oUonsTodos
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	<Services.WebMethod(EnableSession:=True)>
	<Script.Services.ScriptMethod()>
	Public Shared Function Obtener_UONs_Compartir_Tree(ByVal UON0 As String, ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, ByVal iTipoVisor As Integer, ByVal DeOtroUsuario As Boolean) As IList
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim dsUONsCompartir As DataSet

			Dim oUONsNivel As IList
			Dim Nivel As Integer
			If String.IsNullOrEmpty(UON0) Then
				Nivel = 0
			ElseIf String.IsNullOrEmpty(UON1) Then
				Nivel = 1
			ElseIf String.IsNullOrEmpty(UON2) Then
				Nivel = 2
			ElseIf String.IsNullOrEmpty(UON3) Then
				Nivel = 3
			Else
				Nivel = 4
			End If
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			'Gestion de restricciones. Si el escenario es propio utilizamos las restricciones de creacion, si no, las de edicion de escenarios de otros usuarios.
			Dim RestriccionUONUsu, RestriccionDepUsu, RestriccionUONsPerfilUsu As Boolean
			With FSNUser
				If DeOtroUsuario Then
					RestriccionUONUsu = .PMRestringirEdicionEscenariosUONUsu
					RestriccionDepUsu = .PMRestringirEdicionEscenariosDEPUsu
					RestriccionUONsPerfilUsu = .PMRestringirEdicionEscenariosUONsPerfilUsu
				Else
					RestriccionUONUsu = .PMRestringirCrearEscenariosUsuariosUONUsu
					RestriccionDepUsu = .PMRestringirCrearEscenariosUsuariosDEPUsu
					RestriccionUONsPerfilUsu = .PMRestringirCrearEscenariosUsuariosUONsPerfilUsu
				End If

				dsUONsCompartir = cEscenarios.FSN_Obtener_UONsCompartir(.Cod, .Idioma, .UON1, .UON2, .UON3, .Dep, RestriccionUONUsu, RestriccionDepUsu, RestriccionUONsPerfilUsu, Nivel:=Nivel)
			End With

			Select Case Nivel
				Case 0
					oUONsNivel = dsUONsCompartir.Tables(0).Rows.OfType(Of DataRow).Select(Function(x) New With {Key .id = "##", .text = x("DEN"), .icon = False, .children = True,
																												.state = New With {Key .opened = False, .disabled = Not CType(x("PERMISO"), Boolean)}}).ToList()
				Case 1
					Dim oUons = dsUONsCompartir.Tables(0).Rows.OfType(Of DataRow).Where(Function(x) x("ESDEPARTAMENTO") = 0).Select(
						Function(x) New With {Key .id = x("COD") & "###", .text = x("COD") & " - " & x("DEN"), .icon = False, .state = New With {Key .opened = False, .disabled = Not CType(x("PERMISO"), Boolean)},
									.children = dsUONsCompartir.Tables(1).Rows.OfType(Of DataRow).Where(Function(y) y("UON1") = x("COD")).Any}) _
									.OrderBy(Of String)(Function(x) "COD").ToList()

					oUons = oUons.Union(dsUONsCompartir.Tables(0).Rows.OfType(Of DataRow).Where(Function(x) x("ESDEPARTAMENTO") = 1).Select(
						Function(x) New With {Key .id = "###" & x("COD"), .text = x("COD") & " - " & x("DEN"), .icon = False, .state = New With {Key .opened = False, .disabled = Not CType(x("PERMISO"), Boolean)}, .children = False}) _
									.OrderBy(Of String)(Function(x) "COD").ToList()).ToList()
					oUONsNivel = oUons
				Case 2
					Dim oUons = dsUONsCompartir.Tables(0).Rows.OfType(Of DataRow).Where(Function(x) x("ESDEPARTAMENTO") = 0 AndAlso x("UON1") = UON1).Select(
						Function(x) New With {Key .id = x("UON1") & "#" & x("COD") & "##", .text = x("UON1") & " - " & x("COD") & " - " & x("DEN"), .icon = False, .state = New With {Key .opened = False, .disabled = Not CType(x("PERMISO"), Boolean)},
									.children = dsUONsCompartir.Tables(1).Rows.OfType(Of DataRow).Where(
									Function(y) y("UON1") = x("UON1") AndAlso y("UON2") = x("COD")).Any}) _
									.OrderBy(Of String)(Function(x) "COD").ToList()

					oUons = oUons.Union(dsUONsCompartir.Tables(0).Rows.OfType(Of DataRow).Where(Function(x) x("ESDEPARTAMENTO") = 1 AndAlso x("UON1") = UON1).Select(
												Function(x) New With {Key .id = x("UON1") & "###" & x("COD"), .text = x("UON1") & " - " & x("COD") & " - " & x("DEN"), .icon = False,
															.state = New With {Key .opened = False, .disabled = Not CType(x("PERMISO"), Boolean)}, .children = False}) _
															.OrderBy(Of String)(Function(x) "COD").ToList()).ToList()
					oUONsNivel = oUons
				Case 3
					Dim oUons = dsUONsCompartir.Tables(0).Rows.OfType(Of DataRow).Where(Function(x) x("ESDEPARTAMENTO") = 0 AndAlso x("UON1") = UON1 AndAlso x("UON2") = UON2).Select(
										Function(x) New With {Key .id = x("UON1") & "#" & x("UON2") & "#" & x("COD") & "#", .text = x("UON1") & " - " & x("UON2") & " - " & x("COD") & " - " & x("DEN"), .icon = False,
												.state = New With {Key .opened = False, .disabled = Not CType(x("PERMISO"), Boolean)}, .children = dsUONsCompartir.Tables(1).Rows.OfType(Of DataRow).Where(
															Function(y) y("UON1") = x("UON1") AndAlso y("UON2") = x("UON2") AndAlso y("UON3") = x("COD")).Any}) _
												.OrderBy(Of String)(Function(x) "COD").ToList()

					oUons = oUons.Union(dsUONsCompartir.Tables(0).Rows.OfType(Of DataRow).Where(Function(x) x("ESDEPARTAMENTO") = 1 AndAlso x("UON1") = UON1 AndAlso x("UON2") = UON2).Select(
												Function(x) New With {Key .id = x("UON1") & "#" & x("UON2") & "##" & x("COD"), .text = x("UON1") & " - " & x("UON2") & " - " & x("COD") & " - " & x("DEN"), .icon = False,
															.state = New With {Key .opened = False, .disabled = Not CType(x("PERMISO"), Boolean)}, .children = False}) _
													.OrderBy(Of String)(Function(x) "COD").ToList()).ToList()
					oUONsNivel = oUons
				Case Else
					oUONsNivel = dsUONsCompartir.Tables(0).Rows.OfType(Of DataRow).Where(Function(x) x("ESDEPARTAMENTO") = 1 AndAlso x("UON1") = UON1 AndAlso x("UON2") = UON2 AndAlso x("UON3") = UON3).Select(
						Function(x) New With {Key .id = x("UON1") & "#" & x("UON2") & "#" & x("UON3") & "#" & x("COD"), .text = x("UON1") & " - " & x("UON2") & " - " & x("UON3") & " - " & x("COD") & " - " & x("DEN"),
													  .icon = False, .state = New With {Key .opened = False, .disabled = Not CType(x("PERMISO"), Boolean)}, .children = False}).OrderBy(Of String)(Function(x) "COD").ToList()
			End Select

			Return oUONsNivel
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Obtener_Usuarios_Compartir(ByVal q As String, ByVal iTipoVisor As Integer, ByVal DeOtroUsuario As Boolean) As IList
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim cEscenarios As Escenarios
			Dim dsUONsCompartir As DataSet
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			'Gestion de restricciones. Si el escenario es propio utilizamos las restricciones de creacion, si no, las de edicion de escenarios de otros usuarios.
			Dim RestriccionUONUsu, RestriccionDepUsu, RestriccionUONsPerfilUsu As Boolean
			With FSNUser
				If DeOtroUsuario Then
					RestriccionUONUsu = .PMRestringirEdicionEscenariosUONUsu
					RestriccionDepUsu = .PMRestringirEdicionEscenariosDEPUsu
					RestriccionUONsPerfilUsu = .PMRestringirEdicionEscenariosUONsPerfilUsu
				Else
					RestriccionUONUsu = .PMRestringirCrearEscenariosUsuariosUONUsu
					RestriccionDepUsu = .PMRestringirCrearEscenariosUsuariosDEPUsu
					RestriccionUONsPerfilUsu = .PMRestringirCrearEscenariosUsuariosUONsPerfilUsu
				End If

				dsUONsCompartir = cEscenarios.FSN_Obtener_USUCompartir(.Cod, .Idioma, Nothing, Nothing, Nothing, q, .UON1, .UON2, .UON3, .Dep, RestriccionUONUsu, RestriccionDepUsu, RestriccionUONsPerfilUsu)
			End With

			Return dsUONsCompartir.Tables(0).Rows.OfType(Of DataRow).Select(Function(x) New With {Key .id = x("COD"), .name = x("COD") & " - " & x("NOM") & " " & x("APE")}).ToList()
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Actualiza los usuarios con los que se comparte el escenario
	''' </summary>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Obtener_Escenario_Usuarios_Compartir_Usu(ByVal IdEscenario As Integer) As IList
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim cEscenarios As Escenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			Dim dtResultado As DataTable = cEscenarios.FSN_Get_Escenario_Compartir_Usu(IdEscenario)

			Return dtResultado.Rows.OfType(Of DataRow).Select(Function(x) New With {Key .id = x("COD"), .name = x("COD") & " - " & x("NAME")}).ToList()
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Obtener_Escenario_UONs_Compartir(ByVal IdEscenario As Integer) As IList
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim cEscenarios As Escenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			Dim dtResultado As DataTable = cEscenarios.FSN_Get_Escenario_Compartir_UON(IdEscenario, FSNUser.Idioma)

			Return dtResultado.Rows.OfType(Of DataRow).Select(Function(x) New With {Key .id = x("COD"), .name = x("DEN")}).ToList()
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Actualiza los usuarios con los que se comparte el escenario
	''' </summary>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Sub Update_Escenario_Usuarios_Compartir(ByVal IdEscenario As Integer, ByVal CompartirUON0 As Boolean,
														  ByVal CompartidosUSU As List(Of String), ByVal CompartidosUON As List(Of String),
														  ByVal CompartirPortal As Boolean)
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim cEscenarios As Escenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))

			'Generamos el XML de los usuarios seleccionados
			Dim xmlUsuarios As XElement = New XElement("List", CompartidosUSU.Select(Function(x) New XElement("Item", x)))
			Dim xmlUONs As XElement = New XElement("List", CompartidosUON.Select(Function(x) New XElement("Item", {
																					 New XElement("UON1", Split(x, "#")(0)),
																					 New XElement("UON2", Split(x, "#")(1)),
																					 New XElement("UON3", Split(x, "#")(2)),
																					 New XElement("DEP", Split(x, "#")(3))
																				 })))

			cEscenarios.FSN_Edit_Escenario_Compartir(IdEscenario, CompartirUON0, xmlUsuarios.ToString(), xmlUONs.ToString(), CompartirPortal)
		Catch ex As Exception
			Throw ex
		End Try
	End Sub
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Sub Procesar_Acciones(ByVal datosInstancias As String)
		Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
		Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
		Dim serializer As New JavaScriptSerializer
		Dim oDatosInstanciaAprobar As Dictionary(Of String, String) = serializer.Deserialize(Of Dictionary(Of String, String))(datosInstancias)
		Dim cInstancia As Instancia
		cInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))

		Dim Comentario As String
		Dim instanciasActualizarEnProceso As String = String.Empty
		Dim instanciasValidar As String = String.Empty

		Dim dsXML As New DataSet
		dsXML.Tables.Add("SOLICITUD")
		Dim drSolicitud As DataRow
		With dsXML.Tables("SOLICITUD").Columns
			.Add("TIPO_PROCESAMIENTO_XML")
			.Add("TIPO_DE_SOLICITUD")
			.Add("COMPLETO")
			.Add("CODIGOUSUARIO")
			.Add("INSTANCIA")
			.Add("USUARIO")
			.Add("USUARIO_EMAIL")
			.Add("USUARIO_IDIOMA")
			.Add("ACCION")
			.Add("IDACCION")
			.Add("COMENTARIO")
			.Add("BLOQUE_ORIGEN")
			.Add("THOUSANFMT")
			.Add("DECIAMLFMT")
			.Add("PRECISIONFMT")
			.Add("DATEFMT")
			.Add("REFCULTURAL")
			.Add("TIPOEMAIL")
			.Add("IDTIEMPOPROC")
		End With

		For Each instancia As KeyValuePair(Of String, String) In oDatosInstanciaAprobar
			Comentario = HttpContext.Current.Session("COMENT" & instancia.Key)

			instanciasActualizarEnProceso &= IIf(instanciasActualizarEnProceso Is String.Empty, "", ",") & instancia.Key
			instanciasValidar &= IIf(instanciasValidar Is String.Empty, "", "#") & instancia.Key & "|" & instancia.Value & "|" & Comentario
		Next
		If Not instanciasActualizarEnProceso = "" Then
			Dim cInstancias As Instancias
			cInstancias = FSNServer.Get_Object(GetType(FSNServer.Instancias))
			cInstancias.Actualizar_En_Proceso2(instanciasActualizarEnProceso, FSNUser.CodPersona)

			Dim xmlName As String
			Dim lIDTiempoProc As Long
			For Each info As String In Split(instanciasValidar, "#")
				lIDTiempoProc = 0
				dsXML.Tables("SOLICITUD").Rows.Clear()
				cInstancia.ID = strToLong(Split(info, "|")(0))
				cInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, FSNUser.Cod, strToLong(Split(info, "|")(1)))
				drSolicitud = dsXML.Tables("SOLICITUD").NewRow
				With drSolicitud
					.Item("TIPO_PROCESAMIENTO_XML") = CInt(TipoProcesamientoXML.FSNWeb_PM_AprobacionRechazoMultiple)
					.Item("TIPO_DE_SOLICITUD") = CInt(TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras)
					.Item("COMPLETO") = 1
					.Item("CODIGOUSUARIO") = FSNUser.Cod
					.Item("INSTANCIA") = Split(info, "|")(0)
					.Item("USUARIO") = FSNUser.CodPersona
					.Item("USUARIO_EMAIL") = FSNUser.Email
					.Item("USUARIO_IDIOMA") = FSNUser.IdiomaCod
					.Item("ACCION") = ""
					.Item("IDACCION") = Split(info, "|")(2)
					.Item("COMENTARIO") = Split(info, "|")(3)
					.Item("BLOQUE_ORIGEN") = Split(info, "|")(1)
					.Item("THOUSANFMT") = FSNUser.ThousanFmt
					.Item("DECIAMLFMT") = FSNUser.DecimalFmt
					.Item("PRECISIONFMT") = FSNUser.PrecisionFmt
					.Item("DATEFMT") = FSNUser.DateFmt
					.Item("REFCULTURAL") = FSNUser.Idioma.RefCultural
					.Item("TIPOEMAIL") = FSNUser.TipoEmail
					.Item("IDTIEMPOPROC") = lIDTiempoProc
				End With
				xmlName = FSNUser.Cod & "#" & Split(info, "|")(0) & "#" & Split(info, "|")(1)
				dsXML.Tables("SOLICITUD").Rows.Add(drSolicitud)

				cInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, iFecha:=TiempoProcesamiento.InicioDisco)
				If ConfigurationManager.AppSettings("SERVIDOR_TRATAMIENTO_INSTANCIAS") = "0" Then
					'Se hace con un StringWriter porque el método GetXml del dataset no incluye el esquema
					Dim oSW As New System.IO.StringWriter()
					dsXML.WriteXml(oSW, XmlWriteMode.WriteSchema)

					Dim oSrvTrataInst As New FSNWebServiceXML.TratamientoInstancias
					oSrvTrataInst.TransferirXMLEnEpera(oSW.ToString(), xmlName)
				Else
					dsXML.WriteXml(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & "#FSNWEB.xml", XmlWriteMode.WriteSchema)
					If File.Exists(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml") Then _
						File.Delete(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml")
					FileSystem.Rename(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & "#FSNWEB.xml",
									  ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml")
				End If
			Next
			cInstancias = Nothing
		End If
	End Sub
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Sub Monitorizar_Solicitud(ByVal idInstancia As Integer, ByVal Seg As Integer)
		Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
		Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")

		Dim cInstancia As Instancia
		cInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
		cInstancia.ID = idInstancia
		cInstancia.ActualizarMonitorizacion(Seg, FSNUser.CodPersona)
	End Sub
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Obtener_Numero_Pendientes(ByVal iTipoVisor As Integer) As Integer
		Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
		Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")

		Dim condicionesPendientes As New List(Of EscenarioFiltroCondicion)
		Dim iEscenarioFiltroCondicion As New EscenarioFiltroCondicion
		With iEscenarioFiltroCondicion
			.IdCampo = CamposGeneralesVisor.ESTADO
			.EsCampoGeneral = True
			.Denominacion_BD = "ESTADO"
			.Operador = Operadores.Campos.ES
			.Valores.Add("1000")
		End With
		condicionesPendientes.Add(iEscenarioFiltroCondicion)
		Dim cInstancias As Instancias
		cInstancias = FSNServer.Get_Object(GetType(FSNServer.Instancias))
		Dim sentenciaWhereCamposGenerales As String = Generar_SentenciaWhere_CamposGenerales(condicionesPendientes)
		Dim ds As DataSet = cInstancias.DevolverInstancias(FSNUser.CodPersona, FSNUser.Idioma, 0, Generar_SentenciaWhere_Filtro(condicionesPendientes),
														"", sentenciaWhereCamposGenerales, 1, 1, False,
														False, False, True, True, True, False, False, TipoVisor:=iTipoVisor)

		If ds.Tables(0).Rows.Count = 0 Then
			Return 0
		Else
			Return ds.Tables(0).Rows(0)("TOTALSOLICITUDES")
		End If
	End Function
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Sub Establecer_Solicitudes_Ordenacion(ByVal data As Object)
		Dim serializer As New JavaScriptSerializer
		Dim oInfoFiltroVista As Object = serializer.Deserialize(Of Object)(data)
		Dim oVista As EscenarioVista = serializer.Deserialize(Of EscenarioVista)(oInfoFiltroVista("Vista"))

		If oVista.Escenario = 0 Then
			HttpContext.Current.Session("PMFiltroUsuarioDefectoOrden") = oVista.Orden
		Else
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			cEscenarios.GuardarOrdenacion(oVista.Id, oVista.Orden)
		End If
	End Sub
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Sub Obtener_Solicitudes_Ordenacion(ByVal data As Object, ByVal OblCodPedDir As Boolean, ByVal iTipoVisor As Integer)
		Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
		Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
		Dim serializer As New JavaScriptSerializer
		Dim oInfoFiltroVista As Object = serializer.Deserialize(Of Object)(data)
		Dim IdFormulario As Integer = serializer.Deserialize(Of Integer)(oInfoFiltroVista("IdFormulario"))
		Dim oFiltro As EscenarioFiltro = serializer.Deserialize(Of EscenarioFiltro)(oInfoFiltroVista("Filtro"))
		Dim oVista As EscenarioVista = serializer.Deserialize(Of EscenarioVista)(oInfoFiltroVista("Vista"))
		Dim lSolicitudesVinculadas As String = serializer.Deserialize(Of String)(oInfoFiltroVista("SolicitudFormularioVinculados"))
		Dim oPageNumber As Integer = serializer.Deserialize(Of Integer)(oInfoFiltroVista("Page"))
		Dim columNames() As String = {}
		Dim columNamesDesglose() As String = {}
		Dim ObtenerProcesos, ObtenerPedidos, VistaDesglose As Boolean
		Seleccionar_Columnas_Grid(oVista.Campos_Vista, columNames, columNamesDesglose, ObtenerProcesos, ObtenerPedidos, VistaDesglose)
		Dim cInstancias As Instancias
		cInstancias = FSNServer.Get_Object(GetType(FSNServer.Instancias))
		Dim AbtasUsted, Participo, Pendientes, PendientesDevolucion, Trasladadas, Otras, ObservadorSustitucion As Nullable(Of Boolean)
		Dim FiltroDesglose As Boolean
		Dim sentenciaWhere As String = Generar_SentenciaWhere_Filtro(oFiltro.FormulaCondiciones, FiltroDesglose)
		Dim sentenciaWhereCamposGenerales As String
		If oFiltro.FormulaAvanzada Then
			sentenciaWhereCamposGenerales = ""
			AbtasUsted = True
			Participo = True
			Pendientes = True
			PendientesDevolucion = True
			Trasladadas = True
			Otras = True
			ObservadorSustitucion = True
		Else
			sentenciaWhereCamposGenerales = Generar_SentenciaWhere_CamposGenerales(oFiltro.FormulaCondiciones, AbtasUsted, Participo, Pendientes, PendientesDevolucion, Trasladadas, Otras, ObservadorSustitucion, lSolicitudesVinculadas)
		End If
		Dim ds As DataSet = cInstancias.DevolverInstancias(FSNUser.CodPersona, FSNUser.Idioma, IdFormulario,
							sentenciaWhere, oVista.Orden, sentenciaWhereCamposGenerales,
							oPageNumber, FSNUser.PMNumeroFilas, BuscarProveedorArticulo(oFiltro.FormulaCondiciones, oVista.Campos_Vista),
							AbtasUsted, Participo, Pendientes, PendientesDevolucion, Trasladadas, Otras, ObservadorSustitucion,
							ObtenerProcesos, ObtenerPedidos, OblCodPedDir, FiltroDesglose, VistaDesglose, iTipoVisor)
		Dim view As New DataView(ds.Tables(0))
		Dim dsResultado As New DataSet
		Dim dtSolicitudes As DataTable = view.ToTable("SOLICITUDES", True, columNames)
		dsResultado.Tables.Add(dtSolicitudes)
		If Not IdFormulario = 0 AndAlso columNamesDesglose.Count > 3 Then
			Dim listaDesgloses As List(Of String) = columNamesDesglose.Where(Function(x) Split(x, "_").Length > 1).Select(Function(x) Split(x, "_")(1)).Distinct().ToList
			Dim query = From Datos In ds.Tables(0).AsEnumerable()
						Where (listaDesgloses.Contains(Datos.Item("DESGLOSE").ToString))
						Select Datos Distinct
			Dim dtDesglose As DataTable = Nothing
			If query.Any Then
				dtDesglose = query.CopyToDataTable
			End If
			Dim viewDesglose As New DataView(dtDesglose)
			dsResultado.Tables.Add(viewDesglose.ToTable("DESGLOSE", True, columNamesDesglose))

			Dim parentColsVista(0) As DataColumn
			Dim childColsVista(0) As DataColumn
			parentColsVista(0) = dsResultado.Tables("SOLICITUDES").Columns("ID")
			childColsVista(0) = dsResultado.Tables("DESGLOSE").Columns("FORM_INSTANCIA")
			dsResultado.Relations.Add("SOLICITUD_DESGLOSE", parentColsVista, childColsVista, False)
		End If
		If Pendientes Then
			dsResultado.Tables.Add(New DataView(ds.Tables(1)).ToTable("SOLICITUDES_APROBAR", True, New String() {"ID", "ACCION_APROBAR", "BLOQUE"}))
			dsResultado.Tables.Add(New DataView(ds.Tables(2)).ToTable("SOLICITUDES_RECHAZAR", True, New String() {"ID", "ACCION_RECHAZAR", "BLOQUE"}))
		End If
		HttpContext.Current.Cache.Insert("dsSolicitudes_" & FSNUser.Cod, dsResultado, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0), CacheItemPriority.BelowNormal, Nothing)
	End Sub
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Obtener_Solicitudes_Aprobar() As IList
		Try
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim dtSolicitudes As DataTable = CType(HttpContext.Current.Cache("dsSolicitudes_" & FSNUser.Cod), DataSet).Tables("SOLICITUDES_APROBAR")

			Return dtSolicitudes.Rows.OfType(Of DataRow).Select(Function(x) New With {Key .ID = x("ID"), .AccionAprobar = x("ACCION_APROBAR"), .Bloque = x("BLOQUE")}).ToList()
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Obtener_Solicitudes_Rechazar() As IList
		Try
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim dtSolicitudes As DataTable = CType(HttpContext.Current.Cache("dsSolicitudes_" & FSNUser.Cod), DataSet).Tables("SOLICITUDES_RECHAZAR")

			Return dtSolicitudes.Rows.OfType(Of DataRow).Select(Function(x) New With {Key .ID = x("ID"), .AccionRechazar = x("ACCION_RECHAZAR"), .Bloque = x("BLOQUE")}).ToList()
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Comprobar_Formula_Avanzada(ByVal FiltroCondicionesFormula As String) As Integer
		Dim serializer As New JavaScriptSerializer
		Dim oCondicionesFormula As List(Of EscenarioFiltroCondicion) = serializer.Deserialize(Of List(Of EscenarioFiltroCondicion))(FiltroCondicionesFormula)
		Dim formulaAvanzada As String = String.Empty
		Dim numBloque As Integer = 1
		Dim variablesFormula() As String = {}
		For Each oCondicionFormula As EscenarioFiltroCondicion In oCondicionesFormula
			If oCondicionFormula.IdCampo = 0 Then
				Select Case oCondicionFormula.Operador
					Case Operadores.Formula.Y
						formulaAvanzada &= " AND "
					Case Operadores.Formula.O
						formulaAvanzada &= " OR "
					Case Operadores.Formula.PARENTESIS_ABIERTO
						formulaAvanzada &= "("
					Case Operadores.Formula.PARENTESIS_CERRADO
						formulaAvanzada &= ")"
				End Select
			Else
				formulaAvanzada &= "FB" & numBloque & " "
				ReDim Preserve variablesFormula(variablesFormula.Length)
				variablesFormula(variablesFormula.Length - 1) = "FB" & numBloque
				numBloque += 1
			End If
		Next
		If formulaAvanzada Is String.Empty Then
			Return 0
		Else
			Dim iEq As New USPExpress.USPExpression
			Try
				iEq.ImplicitMultiplication = False
				iEq.Parse(formulaAvanzada, variablesFormula)
				Return 0
			Catch ex As USPExpress.UnbalancedParenthesesException
				Return 1
			Catch ex As USPExpress.InvalidOperatorLocationException
				Return 2
			Catch ex As USPExpress.InvalidTermLocationException
				Return 3
			Catch ex As Exception
				Return -1
			End Try
		End If
	End Function
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Obtener_EscenariosFavoritos_Usuario(ByVal iTipoVisor As Integer) As String
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			Dim serializer As New JavaScriptSerializer
			Dim oResultado As Object = cEscenarios.FSN_Get_EscenariosFavoritos_Usuario(FSNUser.Cod, FSNUser.Idioma, iTipoVisor, FSNUser.PMOcultarEscenarioDefecto)
			Return serializer.Serialize(oResultado)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Obtener_Numero_Solicitudes_Filtro(ByVal oEscenario As String, ByVal IdFiltro As Integer, ByVal iTipoVisor As Integer) As Object
		Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
		Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
		Dim serializer As New JavaScriptSerializer
		Dim cEscenario As Escenario = serializer.Deserialize(Of Escenario)(oEscenario)

		Dim cInstancias As Instancias
		cInstancias = FSNServer.Get_Object(GetType(FSNServer.Instancias))
		Dim IdFormulario As Integer = 0
		If cEscenario.SolicitudFormularioVinculados.Select(Function(x) x.Value.Key).Distinct().Count() = 1 Then
			IdFormulario = cEscenario.SolicitudFormularioVinculados.First.Value.Key
		End If

		Dim lSolicitudesVinculadas As String = String.Join(",", cEscenario.SolicitudFormularioVinculados.Select(Function(x) x.Key).Distinct())
		Dim AbtasUsted, Participo, Pendientes, PendientesDevolucion, Trasladadas, Otras, ObservadorSustitucion As Nullable(Of Boolean)
		Dim FiltroDesglose As Boolean
		Dim sentenciaWhere As String = Generar_SentenciaWhere_Filtro(cEscenario.EscenarioFiltros(IdFiltro).FormulaCondiciones, FiltroDesglose)
		Dim sentenciaWhereCamposGenerales As String
		If cEscenario.EscenarioFiltros(IdFiltro).FormulaAvanzada Then
			sentenciaWhereCamposGenerales = ""
			AbtasUsted = True
			Participo = True
			Pendientes = True
			PendientesDevolucion = True
			Trasladadas = True
			Otras = True
			ObservadorSustitucion = True
		Else
			sentenciaWhereCamposGenerales = Generar_SentenciaWhere_CamposGenerales(cEscenario.EscenarioFiltros(IdFiltro).FormulaCondiciones, AbtasUsted, Participo, Pendientes, PendientesDevolucion, Trasladadas,
																				   Otras, ObservadorSustitucion, lSolicitudesVinculadas)
		End If
		Dim ds As DataSet = cInstancias.DevolverInstancias(FSNUser.CodPersona, FSNUser.Idioma, IdFormulario,
							sentenciaWhere, "", sentenciaWhereCamposGenerales,
							1, 1, BuscarProveedorArticulo(cEscenario.EscenarioFiltros(IdFiltro).FormulaCondiciones, Nothing),
							AbtasUsted, Participo, Pendientes, PendientesDevolucion, Trasladadas, Otras, ObservadorSustitucion,
							FiltroDesglose:=FiltroDesglose, TipoVisor:=iTipoVisor)
		If ds.Tables(0).Rows.Count = 0 Then
			Return {0, cEscenario.Id, IdFiltro}
		Else
			Return {ds.Tables(0).Rows(0)("TOTALSOLICITUDES"), cEscenario.Id, IdFiltro}
		End If
	End Function
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Sub Establecer_Escenario_Favorito(ByVal IdEscenario As Integer, ByVal Favorito As Boolean, ByVal iTipoVisor As Integer)
		Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
		Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
		Dim serializer As New JavaScriptSerializer

		Dim cEscenarios As Escenarios
		cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
		cEscenarios.FSN_Establecer_Escenario_Favorito(FSNUser.Cod, iTipoVisor, IdEscenario, Favorito)
	End Sub
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Sub Ordenar_Filtros(ByVal IdEscenario As Integer, ByVal IdFiltro As Integer,
										 ByVal PosicionAnterior As Integer, ByVal PosicionActual As Integer)
		Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
		Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
		Dim serializer As New JavaScriptSerializer

		Dim cEscenarios As Escenarios
		cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
		cEscenarios.FSN_Ordenar_Filtros(IdEscenario, IdFiltro, PosicionAnterior, PosicionActual)
	End Sub
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Sub Ordenar_Vistas(ByVal IdEscenario As Integer, ByVal IdVista As Integer,
										 ByVal PosicionAnterior As Integer, ByVal PosicionActual As Integer)
		Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
		Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
		Dim serializer As New JavaScriptSerializer

		Dim cEscenarios As Escenarios
		cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
		cEscenarios.FSN_Ordenar_Vistas(IdEscenario, IdVista, PosicionAnterior, PosicionActual)
	End Sub
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Sub Establecer_VistaDefecto_Filtro(ByVal IdFiltro As Integer, ByVal IdVista As Integer)
		Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
		Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
		Dim serializer As New JavaScriptSerializer

		Dim cEscenarios As Escenarios
		cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
		cEscenarios.FSN_Establecer_VistaDefecto_Filtro(IdFiltro, IdVista)
	End Sub
	''' <summary>
	''' Buscar una Solicitud concreta usando su Identificador
	''' </summary>
	''' <param name="Id">Identificador</param>
	''' <param name="chkProcesosPedidos">Mostrar Procesos y Pedidos, o no</param>
	''' <returns>Estado Solicitud</returns>
	''' <remarks>Llamada desde: Bt de pantalla; Tiempo maximo: 0,3sg;</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Buscar_Solicitud_Identificador(ByVal Id As Integer, ByVal chkProcesosPedidos As Boolean, ByVal iTipoVisor As Integer) As Integer
		Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
		Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
		Dim serializer As New JavaScriptSerializer
		Dim cInstancias As Instancias
		cInstancias = FSNServer.Get_Object(GetType(FSNServer.Instancias))
		Dim ds As DataSet = cInstancias.DevolverInstancias(FSNUser.CodPersona, FSNUser.Idioma, 0,
							"RESULT.ID=" & Id, "", "I.ID=" & Id, 1, FSNUser.PMNumeroFilas, False, True, True, True, True, True, True, True, chkProcesosPedidos, chkProcesosPedidos, False, TipoVisor:=iTipoVisor)
		If ds.Tables(0).Rows.Count = 0 Then
			Return 0
		Else
			ds.Tables(0).TableName = "SOLICITUDES"
			ds.Tables(1).TableName = "SOLICITUDES_APROBAR"
			ds.Tables(2).TableName = "SOLICITUDES_RECHAZAR"
			HttpContext.Current.Cache.Insert("dsSolicitudes_" & FSNUser.Cod, ds, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))

			Select Case CType(ds.Tables(0).Rows(0)("ESTADO"), Integer)
				Case 0
					Return 2 'Guardadas
				Case 6
					Return 4 'Rechazadas
				Case 8
					Return 5 'Anuladas
				Case 1, 7, 100, 101, 102, 103
					Return 6 'Finalizadas
				Case 104
					Return 7 'Cerradas
				Case Else
					If {MotivoVisibilidadSolicitud.SolicitudPendiente, MotivoVisibilidadSolicitud.SolicitudPenditenteDevolucion,
						MotivoVisibilidadSolicitud.SolicitudTrasladadaEsperaDevolucion}.Contains(CType(ds.Tables(0).Rows(0)("MOTIVO_VISIBILIDAD"), Integer)) Then
						Return 1 'Pendientes
					Else
						Return 3 'En curso
					End If
			End Select
		End If
	End Function
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Obtener_Campos_Generales() As Dictionary(Of String, Escenario_Campo)
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))

			Return cEscenarios.FSN_Obtener_Campos_Generales(FSNUser.Idioma)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Sub GuardarOpcionesFiltradoSesion(ByVal filtroCondicionesSession As String, ByVal chkProcesosPedidos As Boolean)
		Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
		Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
		Dim serializer As New JavaScriptSerializer
		Dim lFiltroCondiciones As List(Of EscenarioFiltroCondicion) = serializer.Deserialize(Of List(Of EscenarioFiltroCondicion))(filtroCondicionesSession)
		HttpContext.Current.Session("FiltroCondiciones") = lFiltroCondiciones
		If chkProcesosPedidos Then
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			Dim camposInfo As New List(Of Escenario_Campo)
			camposInfo.Add(cEscenarios.Get_Campo_General(CamposGeneralesVisor.INFOPEDIDOSASOCIADOS, FSNUser.Idioma))
			camposInfo.Add(cEscenarios.Get_Campo_General(CamposGeneralesVisor.INFOPROCESOSASOCIADOS, FSNUser.Idioma))
			HttpContext.Current.Session("chkProcesosPedidos") = camposInfo
		Else
			HttpContext.Current.Session("chkProcesosPedidos") = Nothing
		End If
	End Sub
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function ObtenerTextoLargo(ByVal Instancia As Integer, ByVal Columna As String) As String
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			Dim serializer As New JavaScriptSerializer
			Dim IdCampoOrigen As Integer = CType(Split(Columna, "_")(Split(Columna, "_").Length - 1), Integer)
			Dim EsDesglose As Boolean = IIf(Split(Columna, "_").Length = 3, False, True)
			Return cEscenarios.FSN_Get_TextoLargo_Instancia(Instancia, IdCampoOrigen, EsDesglose).ToString
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Sub Establecer_Opciones_Usu(ByVal tipoOpcion As Integer, ByVal oOpcionesUsu As String)
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			Dim serializer As New JavaScriptSerializer

			Select Case tipoOpcion
				Case 1 'Escenarios
					Dim oEscenarios As List(Of Escenario)
					oEscenarios = serializer.Deserialize(Of List(Of Escenario))(oOpcionesUsu)
					cEscenarios.FSN_Establecer_Escenario_Opciones_Usu(FSNUser.Cod, oEscenarios)
				Case (2) 'Filtros
					Dim oFiltros As Dictionary(Of String, EscenarioFiltro)
					oFiltros = serializer.Deserialize(Of Dictionary(Of String, EscenarioFiltro))(oOpcionesUsu)
					cEscenarios.FSN_Establecer_Filtro_Opciones_Usu(FSNUser.Cod, oFiltros)
				Case Else 'Vistas
					Dim oVistas As Dictionary(Of String, EscenarioVista)
					oVistas = serializer.Deserialize(Of Dictionary(Of String, EscenarioVista))(oOpcionesUsu)
					cEscenarios.FSN_Establecer_Vista_Opciones_Usu(FSNUser.Cod, oVistas)
			End Select
		Catch ex As Exception
			Throw ex
		End Try
	End Sub
	''' <summary>
	''' Obtiene las UON de un nivel dado con las que el usuario puede compartir el escenario. Si nivel=0 se obtienen todas las UON
	''' </summary>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Update_Escenario_Filtro_Condiciones(ByVal Filtro As String, ByVal iTipoVisor As Integer) As Object
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim serializer As New JavaScriptSerializer
			Dim oFiltro As EscenarioFiltro = serializer.Deserialize(Of EscenarioFiltro)(Filtro)
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			Dim oResultado As EscenarioFiltro
			oResultado = cEscenarios.FSN_Edit_Escenario_Filtro_Usuario_Condiciones(FSNUser.Cod, FSNUser.Idioma, oFiltro, iTipoVisor)

			Return serializer.Serialize(oResultado)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Update_Escenario_Vista_Configuracion(ByVal Vista As String, ByVal iTipoVisor As Integer) As Object
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim serializer As New JavaScriptSerializer
			Dim oVista As EscenarioVista = serializer.Deserialize(Of EscenarioVista)(Vista)
			Dim cEscenarios As Escenarios
			cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
			Dim oResultado As EscenarioVista
			oResultado = cEscenarios.FSN_Edit_Escenario_Vista_Usuario_Configuracion(FSNUser.Cod, FSNUser.Idioma, oVista, iTipoVisor)

			Return serializer.Serialize(oResultado)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	<System.Web.Services.WebMethod(True),
	   System.Web.Script.Services.ScriptMethod()>
	Public Shared Sub Establecer_Filtro_Defecto(ByVal IdFiltro As Integer, ByVal Defecto As Boolean, ByVal iTipoVisor As Integer)
		Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
		Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
		Dim serializer As New JavaScriptSerializer

		Dim cEscenarios As Escenarios
		cEscenarios = FSNServer.Get_Object(GetType(FSNServer.Escenarios))
		cEscenarios.FSN_Establecer_Filtro_Defecto(FSNUser.Cod, iTipoVisor, IdFiltro, Defecto)
	End Sub
#End Region
#Region "Funciones"
	''' <summary>
	''' Creamos la sentencia where que ira al stored procedure para filtrar los resultados
	''' </summary>
	''' <param name="FiltroCondiciones"></param>
	''' <returns></returns>
	''' <remarks></remarks>
	Private Shared Function Generar_SentenciaWhere_Filtro(ByVal FiltroCondiciones As List(Of EscenarioFiltroCondicion), Optional ByRef FiltroDesglose As Boolean = False)
		Dim sentenciaWhere As StringBuilder = New StringBuilder()
		Dim clausula As String = String.Empty
		Dim campo As String
		Dim operadorLista As String
		Dim operadorEstadoOtras As String

		For Each condicion As EscenarioFiltroCondicion In FiltroCondiciones
			If condicion.IdCampo = 0 Then
				' Operadores
				Select Case condicion.Operador
					Case Operadores.Formula.Y
						sentenciaWhere.Append(" AND ")
					Case Operadores.Formula.O
						sentenciaWhere.Append(" OR ")
					Case Operadores.Formula.PARENTESIS_ABIERTO
						sentenciaWhere.Append(" ( ")
					Case Operadores.Formula.PARENTESIS_CERRADO
						sentenciaWhere.Append(" ) ")
				End Select
			ElseIf condicion.Valores.Count > 0 Then
				' Condicion de campo que tiene valores
				clausula = "({0})"

				If condicion.EsCampoGeneral Then
					Select Case condicion.IdCampo
						Case CamposGeneralesVisor.FECHATRASLADO, CamposGeneralesVisor.PERSONATRASLADO,
							CamposGeneralesVisor.PROVEEDORTRASLADO, CamposGeneralesVisor.FECHALIMITEDEVOLUCION
							clausula = String.Format(clausula, "(TRASLADADA=1 AND {0})")
						Case CamposGeneralesVisor.ESTADO
							If condicion.Valores.Count = 8 Then
								'Se han seleccionado todas las opciones
								clausula = String.Format(clausula, "1 = 1")
							End If
						Case CamposGeneralesVisor.MOTIVOVISIBILIDAD
							If condicion.Valores.Count = [Enum].GetValues(GetType(MotivoVisibilidadSolicitud)).Length Then
								'Se han seleccionado todas las opciones
								clausula = String.Format(clausula, "1 = 1")
							End If
						Case CamposGeneralesVisor.ESTADOHOMOLOGACION
							clausula = String.Format(clausula, "1 = 1")
					End Select
				Else
					If Split(condicion.Denominacion_BD, "_").Length = 4 Then FiltroDesglose = True
				End If

				' Si hay parámetros que introducir a la condición
				If clausula.IndexOf("{0}") <> -1 Then
					Select Case condicion.TipoCampoGS
						Case TiposDeDatos.TipoCampoGS.Material, TiposDeDatos.TipoCampoGS.CodArticulo,
							TiposDeDatos.TipoCampoGS.DenArticulo, TiposDeDatos.TipoCampoGS.NuevoCodArticulo,
							TiposDeDatos.TipoCampoGS.Pais, TiposDeDatos.TipoCampoGS.Provincia, TiposDeDatos.TipoCampoGS.Unidad,
							TiposDeDatos.TipoCampoGS.Proveedor, TiposDeDatos.TipoCampoGS.FormaPago, TiposDeDatos.TipoCampoGS.Moneda,
							TiposDeDatos.TipoCampoGS.Dest, TiposDeDatos.TipoCampoGS.Contacto, TiposDeDatos.TipoCampoGS.Persona,
							TiposDeDatos.TipoCampoGS.ProveContacto, TiposDeDatos.TipoCampoGS.Rol, TiposDeDatos.TipoCampoGS.UnidadOrganizativa,
							TiposDeDatos.TipoCampoGS.Departamento, TiposDeDatos.TipoCampoGS.OrganizacionCompras, TiposDeDatos.TipoCampoGS.Centro,
							TiposDeDatos.TipoCampoGS.Almacen, TiposDeDatos.TipoCampoGS.CentroCoste, TiposDeDatos.TipoCampoGS.Partida,
							TiposDeDatos.TipoCampoGS.Activo, TiposDeDatos.TipoCampoGS.TipoPedido, TiposDeDatos.TipoCampoGS.UnidadPedido,
							TiposDeDatos.TipoCampoGS.ProveedorERP, TiposDeDatos.TipoCampoGS.Comprador, TiposDeDatos.TipoCampoGS.Empresa
							campo = "ISNULL (" & condicion.Denominacion_BD & IIf(condicion.EsCampoGeneral, String.Empty, "_COD") & ",'')"
						Case Else
							If condicion.EsLista AndAlso Not condicion.EsCampoGeneral _
								AndAlso Not condicion.TipoCampo = TiposDeDatos.TipoGeneral.TipoBoolean _
								AndAlso Not condicion.TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico _
								AndAlso Not condicion.TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha Then
								campo = "ISNULL (" & condicion.Denominacion_BD & "_COD,'')"
							Else
								Select Case condicion.TipoCampo
									Case TiposDeDatos.TipoGeneral.TipoFecha
										campo = "ISNULL(CONVERT(DATE," & If(condicion.IdCampo = CamposGeneralesVisor.FECHAULTIMAAPROBACION, "FECHA_ACT", condicion.Denominacion_BD) & "),'')"
									Case Else
										If condicion.IdCampo = CamposGeneralesVisor.IDENTIFICADOR AndAlso condicion.EsCampoGeneral Then
											campo = "RESULT." & condicion.Denominacion_BD
										Else
											campo = condicion.Denominacion_BD
										End If
								End Select
							End If
					End Select

					'Establecemos el nombre del campo
					clausula = String.Format(clausula, campo & " {0}")

					Select Case condicion.Operador
						Case Operadores.Campos.CONTIENE
							If Not condicion.EsCampoGeneral AndAlso {TiposDeDatos.TipoCampoGS.CodArticulo, TiposDeDatos.TipoCampoGS.DenArticulo, TiposDeDatos.TipoCampoGS.NuevoCodArticulo}.Contains(condicion.TipoCampoGS) Then
								clausula = String.Format(clausula, "LIKE '%" & strToSQLLIKE(condicion.Valores(1)) & "%'")
							Else
								clausula = String.Format(clausula, "LIKE '%" & strToSQLLIKE(condicion.Valores(0)) & "%'")
							End If
						Case Operadores.Campos.EMPIEZAPOR
							clausula = String.Format(clausula, "LIKE '" & strToSQLLIKE(condicion.Valores(0)) & "%'")
						Case Operadores.Campos.TERMINAEN
							clausula = String.Format(clausula, "LIKE '%" & strToSQLLIKE(condicion.Valores(0)) & "'")
						Case Operadores.Campos.NOCONTIENE
							clausula = String.Format(clausula, "NOT LIKE '%" & strToSQLLIKE(condicion.Valores(0)) & "%'")
						Case Operadores.Campos.ENTRE
							If condicion.TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha Then
								clausula = String.Format(clausula, "BETWEEN Convert(date,'" & Format(CType(condicion.Valores(0), Date), "MM-dd-yyyy HH:mm:ss") & "',110) AND Convert(date,'" & Format(CType(condicion.Valores(1), Date), "MM-dd-yyyy HH:mm:ss") & "',110)")
							Else
								clausula = String.Format(clausula, "BETWEEN " & condicion.Valores(0) & " AND " & condicion.Valores(1))
							End If
						Case Operadores.Campos.ES, Operadores.Campos.NOES
							If condicion.Operador = Operadores.Campos.ES Then
								operadorLista = "IN ({0})"
								operadorEstadoOtras = "NOT IN ({0})"
							Else
								operadorLista = "NOT IN ({0})"
								operadorEstadoOtras = "IN ({0})"
							End If
							Dim condicionesEstado As New List(Of String)
                            If condicion.EsCampoGeneral AndAlso condicion.IdCampo = CamposGeneralesVisor.ESTADO Then
                                'Tenemos un problema. Si busca por estado pendiente (ademas de otros) pero no por las de en curso, tenemos que añadir a la sentencia
                                'que el motivo de visibilidad no sea abt por ud, participo y otras(observador). ej: Pide pendientes y guardadas, 
                                'sentencia where= estado in (2,0). El stored calcula las tablas temporales de pendientes, pendientes devolucion y trasladas para las pendientes
                                'y las de abiertas por usted para las guardadas. Esta ocurriendo, que una en curso cumple los requisitos, ya que esta entre las abiertas
                                'por ud y con estado 0. La unica forma de diferenciar es que no este en las abiertas por ud con estado 2.
                                If condicion.Valores.Any(Function(x) Split(x, "###")(0) = "1000") AndAlso Not condicion.Valores.Any(Function(x) Split(x, "###")(0) = "2") Then
                                    condicionesEstado = condicion.Valores.Where(Function(x) Split(x, "###")(0) <> "1000").ToList
                                    clausula = "(" & IIf(condicionesEstado.Any, clausula & " {1} ", "") & "(ISNULL(ESTADO,'') {2}(2) {3} ISNULL(MOTIVO_VISIBILIDAD,'') {4}(3,4,5)))"
                                Else
                                    condicionesEstado = condicion.Valores
                                End If
                                If Not condicion.Valores.Any(Function(x) Split(x, "###")(0) = "1000") AndAlso condicion.Valores.Any(Function(x) Split(x, "###")(0) = "2") Then
                                    condicionesEstado = condicion.Valores.Where(Function(x) Split(x, "###")(0) <> "2").ToList
                                    clausula = "(" & IIf(condicionesEstado.Any, clausula & " {1} ", "") & "(ISNULL(ESTADO,'') {2}(2) {3} ISNULL(MOTIVO_VISIBILIDAD,'') {4}(1,2,6,7)))"
                                End If
                                If condicion.Operador = Operadores.Campos.ES Then
                                    clausula = String.Format(clausula, "{0}", "OR", "IN", "AND", "IN")
                                Else
                                    clausula = String.Format(clausula, "{0}", "AND", "NOT IN", "OR", "NOT IN")
                                End If
                            End If

                            If condicion.EsCampoGeneral And condicion.IdCampo = CamposGeneralesVisor.ESTADO And condicion.Valores.Any(Function(x) Split(x, "###")(0) = VALOR_ESTADO_OTRAS) Then
                                operadorEstadoOtras = String.Format(operadorEstadoOtras, Replace(VALOR_ESTADO_OTRAS, "#", ","))
                                ' Si el campo es Estado y se ha seleccionado la opcion Otras
                                If (condicion.Valores.Count > 1) Then
                                    clausula = String.Format(clausula, "{0} OR " & campo & " " & operadorEstadoOtras)
                                Else
                                    clausula = String.Format(clausula, operadorEstadoOtras)
                                End If
                            End If

                            'Establecemos el operador de lista (si es necesario)
                            clausula = String.Format(clausula, operadorLista)

                            'Si es un NOES también hay que tener en cuenta los que están a NULL
                            If Not (condicion.EsCampoGeneral AndAlso condicion.IdCampo = CamposGeneralesVisor.ESTADO) AndAlso condicion.Operador = Operadores.Campos.NOES Then
                                clausula = String.Format("(" & condicion.Denominacion_BD & " IS NULL OR {0})", clausula)
                            End If

                            If condicion.EsCampoGeneral AndAlso {CamposGeneralesVisor.ESTADO, CamposGeneralesVisor.MOTIVOVISIBILIDAD, CamposGeneralesVisor.ESTADOHOMOLOGACION}.Contains(condicion.IdCampo) Then
                                ' Si es el campo Estado o MotivoVisibilidad, descomponemos los valores con ids compuestos
                                Select Case condicion.IdCampo
                                    Case CamposGeneralesVisor.MOTIVOVISIBILIDAD
                                        clausula = String.Format(clausula, String.Join(",", condicion.Valores.Where(Function(x) Split(x, "###")(0) <> VALOR_ESTADO_OTRAS).SelectMany(Function(x) Split(Replace(Split(x, "###")(0), "1000", "2"), "#"))))
                                    Case Else
                                        clausula = String.Format(clausula, String.Join(",", condicionesEstado.Where(Function(x) Split(x, "###")(0) <> VALOR_ESTADO_OTRAS).SelectMany(Function(x) Split(Replace(Split(x, "###")(0), "1000", "2"), "#"))))
                                End Select
                            Else
                                Select Case condicion.TipoCampoGS
                                    Case -2, -1, TiposDeDatos.TipoCampoGS.Proveedor, TiposDeDatos.TipoCampoGS.Material, TiposDeDatos.TipoCampoGS.CodArticulo,
                                                TiposDeDatos.TipoCampoGS.DenArticulo, TiposDeDatos.TipoCampoGS.NuevoCodArticulo, TiposDeDatos.TipoCampoGS.Empresa  '-2=usuario,-1=peticionario
                                        ' Si es un usuario o peticionario cogemos los ids del primer valor de la condicion separados por ###
                                        clausula = String.Format(clausula, String.Join(",", Split(condicion.Valores(0), "###").Select(Function(x) "'" & x & "'")))
                                    Case TiposDeDatos.TipoCampoGS.Provincia
                                        ' Si es una provincia cogemos el id del primer valor a partir del # e interpretado como string
                                        clausula = String.Format(clausula, "'" & Split(condicion.Valores(0), "#")(1) & "'")
                                    Case Else
                                        Select Case condicion.TipoCampo
                                            Case TiposDeDatos.TipoGeneral.TipoNumerico
                                                clausula = String.Format(clausula, String.Join(",", condicion.Valores.Select(Function(x) "'" & Split(x, "###")(1) & "'")))
                                            Case TiposDeDatos.TipoGeneral.TipoFecha
                                                clausula = String.Format(clausula, String.Join(",", condicion.Valores.Select(Function(x) "Convert(date,'" & Format(CType(Split(x, "###")(1), Date), "MM-dd-yyyy HH:mm:ss") & "',110)")))
                                            Case Else
                                                clausula = String.Format(clausula, String.Join(",", condicion.Valores.Select(Function(x) "'" & Split(x, "###")(0) & "'")))
                                        End Select
                                End Select
                            End If
                        Case Operadores.Campos.HACE
                            Dim intervalo As DateInterval
                            Dim valor As Double = CType(condicion.Valores(1), Double)
                            Select Case condicion.Valores(0)
                                Case 0 'hora
                                    intervalo = DateInterval.Hour
                                Case 1
                                    intervalo = DateInterval.Day
                                    valor = valor
                                Case 2
                                    intervalo = DateInterval.Day
                                    valor = valor * 7
                                Case 3 'mes
                                    intervalo = DateInterval.Month
                                Case Else 'años
                                    intervalo = DateInterval.Year
                            End Select
                            Dim fechaRelativa As Date = DateAdd(intervalo, (-1) * valor, Now.Date)
                            clausula = String.Format(clausula, ">= Convert(date,'" & Format(fechaRelativa, "MM-dd-yyyy HH:mm:ss") & "',110)")
                        Case Operadores.Campos.PERIODO
                            Dim fechaPeriodo As Date
                            Select Case CType(condicion.Valores(0), Integer)
                                Case 0 'hoy
                                    fechaPeriodo = Now.Date
                                Case 1 'semana
                                    fechaPeriodo = DateAdd(DateInterval.Day, (-1) * IIf(Now.DayOfWeek = DayOfWeek.Sunday, 6, Now.DayOfWeek - 1), Now.Date)
                                Case 2 'mes
                                    fechaPeriodo = New Date(Now.Year, Now.Month, 1)
                                Case 3 'trimestre
                                    fechaPeriodo = New Date(Now.Year, (((Now.Month - 1) \ 3) * 3) + 1, 1)
                                Case 4 'semestre
                                    fechaPeriodo = New Date(Now.Year, (((Now.Month - 1) \ 6) * 6) + 1, 1)
                                Case Else 'año
                                    fechaPeriodo = New Date(Now.Year, 1, 1)
                            End Select
                            clausula = String.Format(clausula, ">= Convert(date,'" & Format(fechaPeriodo, "MM-dd-yyyy HH:mm:ss") & "',110)")
                        Case Else
                            Select Case condicion.Operador
                                Case Operadores.Campos.IGUAL
                                    clausula = String.Format(clausula, "= {0}")
                                Case Operadores.Campos.DISTINTO
                                    clausula = String.Format(clausula, "<> {0}")
                                Case Operadores.Campos.MAYOR
                                    clausula = String.Format(clausula, "> {0} ")
                                Case Operadores.Campos.MAYORIGUAL
                                    clausula = String.Format(clausula, ">= {0}")
                                Case Operadores.Campos.MENOR
                                    clausula = String.Format(clausula, "< {0}")
                                Case Operadores.Campos.MENORIGUAL
                                    clausula = String.Format(clausula, "<= {0}")
                            End Select
                            If condicion.TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha Then
                                clausula = String.Format(clausula, "Convert(date,'" & Format(CType(condicion.Valores(0), Date), "MM-dd-yyyy HH:mm:ss") & "',110)")
                            ElseIf condicion.TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico Then
                                clausula = String.Format(clausula, condicion.Valores(0))
                            Else
                                clausula = String.Format(clausula, "'" & condicion.Valores(0) & "'")
                            End If
                    End Select
                End If
                'Añadimos la cláusula a la sentencia
                sentenciaWhere.Append(clausula)
            Else
                'Si estamos en una formula avanzada en la que inicialmente la condicion tenia un valor y se edita en pantalla para no tener valor, necesitamos
                'que el hueco de esa condicion exista para que la formula no casque
                sentenciaWhere.Append("1=1")
            End If
        Next

        Return sentenciaWhere.ToString()
    End Function
    Private Function DevolverUltimoComentario(ByVal instancia As Long) As String
        Dim cInstancia As Instancia
        cInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        DevolverUltimoComentario = cInstancia.DevolverUltimoComentario(instancia)
    End Function
    Private Shared Function Generar_SentenciaWhere_CamposGenerales(ByVal FiltroCondiciones As List(Of EscenarioFiltroCondicion),
                    Optional ByRef AbtasUsted As Nullable(Of Boolean) = True, Optional ByRef Participo As Nullable(Of Boolean) = True,
                    Optional ByRef Pendientes As Nullable(Of Boolean) = True, Optional ByRef PendientesDevolucion As Nullable(Of Boolean) = True,
                    Optional ByRef Trasladadas As Nullable(Of Boolean) = True, Optional ByRef Otras As Nullable(Of Boolean) = True,
                    Optional ByRef ObservadorSustitucion As Nullable(Of Boolean) = True, Optional ByVal idsSolicitud As String = "")
        Dim sentenciaWhere As StringBuilder = New StringBuilder
        Dim clausula As String = String.Empty
        Dim OperadorLista As String
        Dim OperadorEstadoOtras As String

        If Not String.IsNullOrEmpty(idsSolicitud) Then sentenciaWhere.Append("(S.ID IN (" & idsSolicitud & "))")

        'Recorremos las condiciones generales de los campos FechaAlta, Identificador, Peticionario, TipoSolicitud y Estado que tengan valores
        For Each condicion As EscenarioFiltroCondicion In FiltroCondiciones.Where(Function(x) x.EsCampoGeneral And {CamposGeneralesVisor.FECHAALTA,
                                                                                                                    CamposGeneralesVisor.IDENTIFICADOR,
                                                                                                                    CamposGeneralesVisor.PETICIONARIO,
                                                                                                                    CamposGeneralesVisor.TIPOSOLICITUD,
                                                                                                                    CamposGeneralesVisor.ESTADO,
                                                                                                                    CamposGeneralesVisor.ESTADOHOMOLOGACION}.Contains(x.IdCampo) And x.Valores.Count > 0)
            clausula = "({0})"
            ' Si hay más cláusulas en la sentencia añadimos un AND previamente
            If sentenciaWhere.Length > 0 Then clausula = " AND " & clausula

            Select Case condicion.IdCampo
                Case CamposGeneralesVisor.FECHAALTA
                    clausula = String.Format(clausula, "I.FEC_ALTA {0}")
                Case CamposGeneralesVisor.IDENTIFICADOR
                    clausula = String.Format(clausula, "I.ID {0}")
                Case CamposGeneralesVisor.PETICIONARIO
                    clausula = String.Format(clausula, "I.PETICIONARIO {0}")
                Case CamposGeneralesVisor.TIPOSOLICITUD
                    clausula = String.Format(clausula, "S.ID {0}")
                Case CamposGeneralesVisor.ESTADO
                    If condicion.Valores.Count = 8 Then
                        'Se han seleccionado todas las opciones
                        clausula = String.Format(clausula, "1 = 1")
                    Else
                        clausula = String.Format(clausula, "I.ESTADO {0}")
                    End If
                Case CamposGeneralesVisor.ESTADOHOMOLOGACION
                    clausula = String.Format(clausula, "IV.ESTADO_HOMOLOGACION {0}")
            End Select

            ' Si hay parámetros que introducir a la condición
            If clausula.IndexOf("{0}") <> -1 Then
                Select Case condicion.Operador
                    Case Operadores.Campos.ENTRE
                        If condicion.TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha Then
                            clausula = String.Format(clausula, "BETWEEN Convert(date,'" & Format(CType(condicion.Valores(0), Date), "MM-dd-yyyy HH:mm:ss") & "',110) AND Convert(date,'" & Format(CType(condicion.Valores(1), Date), "MM-dd-yyyy HH:mm:ss") & "',110)")
                        Else
                            clausula = String.Format(clausula, "BETWEEN " & condicion.Valores(0) & " AND " & condicion.Valores(1))
                        End If
                    Case Operadores.Campos.ES, Operadores.Campos.NOES
                        If condicion.Operador = Operadores.Campos.ES Then
                            OperadorLista = "IN ({0})"
                            OperadorEstadoOtras = "NOT IN ({0})"
                        Else
                            OperadorLista = "NOT IN ({0})"
                            OperadorEstadoOtras = "IN ({0})"
                        End If

                        If condicion.IdCampo = CamposGeneralesVisor.ESTADO And condicion.Valores.Any(Function(x) Split(x, "###")(0) = VALOR_ESTADO_OTRAS) Then
                            OperadorEstadoOtras = String.Format(OperadorEstadoOtras, Replace(VALOR_ESTADO_OTRAS, "#", ","))
                            ' Si el campo es Estado y se ha seleccionado la opcion Otras
                            If (condicion.Valores.Count > 1) Then
                                clausula = String.Format(clausula, "{0} OR I.Estado " & OperadorEstadoOtras)
                            Else
                                clausula = String.Format(clausula, OperadorEstadoOtras)
                            End If
                        End If

                        'Establecemos el operador de lista (si es necesario)
                        clausula = String.Format(clausula, OperadorLista)

                        If condicion.IdCampo = CamposGeneralesVisor.ESTADO Then
                            ' Si es el campo Estado o MotivoVisibilidad, descomponemos los valores con ids compuestos                               
                            clausula = String.Format(clausula, String.Join(",", condicion.Valores.Where(Function(x) Split(x, "###")(0) <> VALOR_ESTADO_OTRAS).SelectMany(Function(x) Split(Replace(Split(x, "###")(0), "1000", "2"), "#"))))

                            For Each valor As String In condicion.Valores
                                Select Case Split(valor, "###")(0)
                                    Case "1000" 'Pendientes
                                        If condicion.Operador = Operadores.Campos.ES Then
                                            If AbtasUsted Is Nothing Then AbtasUsted = False
                                            If Participo Is Nothing Then Participo = False
                                            If Otras Is Nothing Then Otras = False
                                            If ObservadorSustitucion Is Nothing Then ObservadorSustitucion = False
                                            Pendientes = True
                                            PendientesDevolucion = True
                                            Trasladadas = True
                                        Else
                                            If Pendientes Is Nothing Then Pendientes = False
                                            If PendientesDevolucion Is Nothing Then PendientesDevolucion = False
                                            If Trasladadas Is Nothing Then Trasladadas = False
                                            AbtasUsted = True
                                            Participo = True
                                            Otras = True
                                            ObservadorSustitucion = True
                                        End If
                                    Case "0" 'Guardadas
                                        If condicion.Operador = Operadores.Campos.ES Then
                                            If Participo Is Nothing Then Participo = False
                                            If Pendientes Is Nothing Then Pendientes = False
                                            If PendientesDevolucion Is Nothing Then PendientesDevolucion = False
                                            If Trasladadas Is Nothing Then Trasladadas = False
                                            If Otras Is Nothing Then Otras = False
                                            If ObservadorSustitucion Is Nothing Then ObservadorSustitucion = False
                                            AbtasUsted = True
                                        Else
                                            If AbtasUsted Is Nothing Then AbtasUsted = False
                                            Participo = True
                                            Pendientes = True
                                            PendientesDevolucion = True
                                            Trasladadas = True
                                            Otras = True
                                            ObservadorSustitucion = True
                                        End If
                                    Case "2" 'En curso
                                        If condicion.Operador = Operadores.Campos.ES Then
                                            If Pendientes Is Nothing Then Pendientes = False
                                            If PendientesDevolucion Is Nothing Then PendientesDevolucion = False
                                            If Trasladadas Is Nothing Then Trasladadas = False
                                            AbtasUsted = True
                                            Participo = True
                                            Otras = True
                                            ObservadorSustitucion = True
                                        Else
                                            If AbtasUsted Is Nothing Then AbtasUsted = False
                                            If Participo Is Nothing Then Participo = False
                                            If Otras Is Nothing Then Otras = False
                                            If ObservadorSustitucion Is Nothing Then ObservadorSustitucion = False
                                            Pendientes = True
                                            PendientesDevolucion = True
                                            Trasladadas = True
                                        End If
                                    Case "6" 'Rechazadas
                                        If condicion.Operador = Operadores.Campos.ES Then
                                            If PendientesDevolucion Is Nothing Then PendientesDevolucion = False
                                            If Trasladadas Is Nothing Then Trasladadas = False
                                            AbtasUsted = True
                                            Participo = True
                                            Pendientes = False
                                            Otras = True
                                            ObservadorSustitucion = True
                                        Else
                                            If AbtasUsted Is Nothing Then AbtasUsted = False
                                            If Participo Is Nothing Then Participo = False
                                            If Pendientes Is Nothing Then Pendientes = True
                                            If Otras Is Nothing Then Otras = False
                                            If ObservadorSustitucion Is Nothing Then ObservadorSustitucion = False
                                            PendientesDevolucion = True
                                            Trasladadas = True
                                        End If
                                    Case "8" 'Anuladas
                                        If condicion.Operador = Operadores.Campos.ES Then
                                            If PendientesDevolucion Is Nothing Then PendientesDevolucion = False
                                            If Trasladadas Is Nothing Then Trasladadas = False
                                            AbtasUsted = True
                                            Participo = True
                                            Pendientes = False
                                            Otras = True
                                            ObservadorSustitucion = True
                                        Else
                                            If AbtasUsted Is Nothing Then AbtasUsted = False
                                            If Participo Is Nothing Then Participo = False
                                            If Pendientes Is Nothing Then Pendientes = True
                                            If Otras Is Nothing Then Otras = False
                                            If ObservadorSustitucion Is Nothing Then ObservadorSustitucion = False
                                            PendientesDevolucion = True
                                            Trasladadas = True
                                        End If
                                    Case "100#101#102#103" 'Finalizadas
                                        If condicion.Operador = Operadores.Campos.ES Then
                                            If Pendientes Is Nothing Then Pendientes = False
                                            If PendientesDevolucion Is Nothing Then PendientesDevolucion = False
                                            If Trasladadas Is Nothing Then Trasladadas = False
                                            AbtasUsted = True
                                            Participo = True
                                            Otras = True
                                            ObservadorSustitucion = True
                                        Else
                                            If AbtasUsted Is Nothing Then AbtasUsted = False
                                            If Participo Is Nothing Then Participo = False
                                            If Otras Is Nothing Then Otras = False
                                            If ObservadorSustitucion Is Nothing Then ObservadorSustitucion = False
                                            Pendientes = True
                                            PendientesDevolucion = True
                                            Trasladadas = True
                                        End If
                                    Case "104" 'Cerradas
                                        If condicion.Operador = Operadores.Campos.ES Then
                                            If Pendientes Is Nothing Then Pendientes = False
                                            If PendientesDevolucion Is Nothing Then PendientesDevolucion = False
                                            If Trasladadas Is Nothing Then Trasladadas = False
                                            AbtasUsted = True
                                            Participo = True
                                            Otras = True
                                            ObservadorSustitucion = True
                                        Else
                                            If AbtasUsted Is Nothing Then AbtasUsted = False
                                            If Participo Is Nothing Then Participo = False
                                            If Otras Is Nothing Then Otras = False
                                            If ObservadorSustitucion Is Nothing Then ObservadorSustitucion = False
                                            Pendientes = True
                                            PendientesDevolucion = True
                                            Trasladadas = True
                                        End If
                                    Case Else
                                        If condicion.Operador = Operadores.Campos.ES Then
                                            If AbtasUsted Is Nothing Then AbtasUsted = False
                                            If Participo Is Nothing Then Participo = False
                                            If Pendientes Is Nothing Then Pendientes = False
                                            If PendientesDevolucion Is Nothing Then PendientesDevolucion = False
                                            If Trasladadas Is Nothing Then Trasladadas = False
                                            Otras = True
                                            ObservadorSustitucion = True
                                        Else
                                            If Otras Is Nothing Then Otras = False
                                            If ObservadorSustitucion Is Nothing Then ObservadorSustitucion = False
                                            AbtasUsted = True
                                            Participo = True
                                            Pendientes = True
                                            PendientesDevolucion = True
                                            Trasladadas = True
                                        End If
                                End Select
                            Next
                        Else
                            Select Case condicion.TipoCampoGS
                                Case -1 '-1=peticionario
                                    ' Si es un usuario o peticionario cogemos los ids del primer valor de la condicion separados por ###
                                    clausula = String.Format(clausula, String.Join(",", Split(condicion.Valores(0), "###").Select(Function(x) "'" & x & "'")))
                                Case TiposDeDatos.TipoCampoGS.EstadoHomologacion
                                    clausula = String.Format(clausula, String.Join(",", condicion.Valores.Select(Function(x) "'" & Split(x, "###")(0) & "'")))
                                Case Else
                                    Select Case condicion.TipoCampo
                                        Case TiposDeDatos.TipoGeneral.TipoFecha
                                            clausula = String.Format(clausula, String.Join(",", condicion.Valores.Select(Function(x) "Convert(date,'" & Format(CType(x, Date), "MM-dd-yyyy HH:mm:ss") & "',110),")))
                                        Case TiposDeDatos.TipoGeneral.TipoNumerico
                                            clausula = String.Format(clausula, String.Join(",", condicion.Valores))
                                        Case Else
                                            clausula = String.Format(clausula, String.Join(",", condicion.Valores.Select(Function(x) "'" & strToSQLLIKE(Split(x, "###")(0)) & "'")))
                                    End Select
                            End Select
                        End If
                    Case Operadores.Campos.HACE
                        Dim intervalo As DateInterval
                        Dim valor As Double = CType(condicion.Valores(1), Double)
                        Select Case condicion.Valores(0)
                            Case 0 'hora
                                intervalo = DateInterval.Hour
                            Case 1, 2 'dia, semanas (que multiplicaremos el valor por 7
                                intervalo = DateInterval.Day
                                valor = valor * 7
                            Case 3 'mes
                                intervalo = DateInterval.Month
                            Case Else 'años
                                intervalo = DateInterval.Year
                        End Select
                        Dim fechaRelativa As Date = DateAdd(intervalo, (-1) * valor, Now.Date)
                        clausula = String.Format(clausula, ">= Convert(date,'" & Format(fechaRelativa, "MM-dd-yyyy HH:mm:ss") & "',110)")
                    Case Operadores.Campos.PERIODO
                        Dim fechaPeriodo As Date
                        Select Case CType(condicion.Valores(0), Integer)
                            Case 0 'hoy
                                fechaPeriodo = Now.Date
                            Case 1 'semana
                                fechaPeriodo = DateAdd(DateInterval.Day, (-1) * IIf(Now.DayOfWeek = DayOfWeek.Sunday, 6, Now.DayOfWeek - 1), Now.Date)
                            Case 2 'mes
                                fechaPeriodo = New Date(Now.Year, Now.Month, 1)
                            Case 3 'trimestre
                                fechaPeriodo = New Date(Now.Year, (((Now.Month - 1) \ 3) * 3) + 1, 1)
                            Case 4 'semestre
                                fechaPeriodo = New Date(Now.Year, (((Now.Month - 1) \ 6) * 6) + 1, 1)
                            Case Else 'año
                                fechaPeriodo = New Date(Now.Year, 1, 1)
                        End Select
                        clausula = String.Format(clausula, ">= Convert(date,'" & Format(fechaPeriodo, "MM-dd-yyyy HH:mm:ss") & "',110)")
                    Case Else
                        Select Case condicion.Operador
                            Case Operadores.Campos.IGUAL
                                clausula = String.Format(clausula, "= {0}")
                            Case Operadores.Campos.DISTINTO
                                clausula = String.Format(clausula, "<> {0}")
                            Case Operadores.Campos.MAYOR
                                clausula = String.Format(clausula, "> {0}")
                            Case Operadores.Campos.MAYORIGUAL
                                clausula = String.Format(clausula, ">= {0}")
                            Case Operadores.Campos.MENOR
                                clausula = String.Format(clausula, "< {0}")
                            Case Operadores.Campos.MENORIGUAL
                                clausula = String.Format(clausula, "<= {0}")
                        End Select
                        If condicion.TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha Then
                            clausula = String.Format(clausula, "Convert(date,'" & Format(CType(condicion.Valores(0), Date), "MM-dd-yyyy HH:mm:ss") & "',110)")
                        ElseIf condicion.TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico Then
                            clausula = String.Format(clausula, condicion.Valores(0))
                        Else
                            clausula = String.Format(clausula, "'" & strToSQLLIKE(condicion.Valores(0)) & "'")
                        End If
                End Select
            End If
            'Añadimos las cláusula a la sentencia
            sentenciaWhere.Append(clausula)
        Next
        If AbtasUsted Is Nothing Then AbtasUsted = True
        If Participo Is Nothing Then Participo = True
        If Pendientes Is Nothing Then Pendientes = True
        If PendientesDevolucion Is Nothing Then PendientesDevolucion = True
        If Trasladadas Is Nothing Then Trasladadas = True
        If Otras Is Nothing Then Otras = True
        If ObservadorSustitucion Is Nothing Then ObservadorSustitucion = True

        Return sentenciaWhere.ToString()
    End Function
    Private Shared Function BuscarProveedorArticulo(ByVal FiltroCondiciones As List(Of EscenarioFiltroCondicion),
                                                    ByVal CamposVista As List(Of Escenario_Campo)) As Integer
        Dim instanciaDesgloseMaterialProveedor As Integer = 0
        Dim instanciaDesgloseMaterialArticulo As Integer = 0
        For Each condicion As EscenarioFiltroCondicion In FiltroCondiciones
            If condicion.EsCampoGeneral AndAlso condicion.IdCampo = CamposGeneralesVisor.PROVEEDOR Then instanciaDesgloseMaterialProveedor = 1
            If condicion.EsCampoGeneral AndAlso condicion.IdCampo = CamposGeneralesVisor.ARTICULO Then instanciaDesgloseMaterialArticulo = 2
        Next
        If CamposVista IsNot Nothing Then
            For Each Campo As Escenario_Campo In CamposVista
                If Campo.EsCampoGeneral AndAlso Campo.Id = CamposGeneralesVisor.PROVEEDOR Then instanciaDesgloseMaterialProveedor = 1
                If Campo.EsCampoGeneral AndAlso Campo.Id = CamposGeneralesVisor.ARTICULO Then instanciaDesgloseMaterialArticulo = 2
            Next
        End If
        Return instanciaDesgloseMaterialProveedor + instanciaDesgloseMaterialArticulo
    End Function
    ''' Codigo para guardar datos del FSAL, funcion llamada desde el evento 'Unload' del Grid de Solicitudes.
    ''' <summary>
    ''' Procedimiento incoporporado para realizar la llamada de forma asíncrona a través de un Thread
    ''' </summary>
    ''' <param name="sArgs">Array con los argumentos de llamada a FSNServer.FSAL</param>
    ''' <remarks></remarks>
    Private Sub CallFSALRegistrarActualizarAcceso(ByVal sArgs As Object)
        Dim m_FSNServer As New FSNServer.FSAL

        If DirectCast(sArgs, Array).GetUpperBound(0) = 12 Then
            m_FSNServer.RegistrarAcceso(sArgs(0), sArgs(1), sArgs(2), sArgs(3), sArgs(4), sArgs(5), sArgs(6), sArgs(7), sArgs(8), sArgs(9), sArgs(10), sArgs(11), sArgs(12))
        ElseIf DirectCast(sArgs, Array).GetUpperBound(0) = 5 Then
            m_FSNServer.ActualizarAcceso(sArgs(0), sArgs(1), sArgs(2), sArgs(3), sArgs(4), sArgs(5))
        End If

    End Sub
#End Region
#Region "whdgSolicitudes"
    Private Sub whdgSolicitudes_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgSolicitudes.InitializeRow
        Dim nivel As Integer = CType(e.Row, ContainerGridRecord).Level
        Dim items As Infragistics.Web.UI.GridControls.GridRecordItemCollection = e.Row.Items
        Dim dataRow As DataRow = e.Row.DataItem.Item.Row
        Dim grid As ContainerGrid = items.Grid
        Dim columns As GridFieldCollection = grid.Columns
        Dim gridCellIndex As Integer

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Seguimiento
        If nivel = 0 Then
            ' Fila de primer nivel    
            Dim iEstado As Integer
            If CType(dataRow("ICONO"), Boolean) OrElse CType(dataRow("TRASLADADA"), Boolean) Then
                iEstado = 1000
            ElseIf dataRow("OBSERVADOR") Then
                iEstado = 3
            Else
                iEstado = CType(dataRow("ESTADO"), Boolean)
            End If
            If whdgSolicitudes.GridView.Columns.Item("DEP") IsNot Nothing AndAlso Not DBNullToBoolean(e.Row.DataItem.Item.Row.Item("VER_DETALLE_PER")) Then
                gridCellIndex = whdgSolicitudes.GridView.Columns.FromKey("DEP").Index
                items(gridCellIndex).Text = ""
            End If
            If dataRow("SEG") Then
                e.Row.CssClass = "datoRojo"
            End If
            For Each columna As GridField In whdgSolicitudes.GridView.Columns
                ' Si es el espacio vacío del final no hacemos nada
                If columna.Key = "EmptySpace" Then Continue For
                gridCellIndex = whdgSolicitudes.GridView.Columns.FromKey(columna.Key).Index
                If _camposArchivo.ContainsKey(columna.Key) Then
                    Dim cAdjuntos As FSNServer.Adjuntos
                    cAdjuntos = FSNServer.Get_Object(GetType(FSNServer.Adjuntos))
                    cAdjuntos.LoadAdjuntosPorInstanciayCampo(dataRow("ID"), _camposArchivo(columna.Key))
                    If cAdjuntos.Data.Tables(0).Rows.Count > 0 Then
                        Dim params As String
                        If cAdjuntos.Data.Tables(0).Rows.Count = 1 Then
                            params = "instancia=" & CType(dataRow("ID"), Integer) & "&id=" & cAdjuntos.Data.Tables(0).Rows(0).Item(0)
                            items(gridCellIndex).Text = "<span rutaAdjunto='" & params & "'>" & items(gridCellIndex).Text & "</span>"
                        Else
                            Dim campo As String = ""
                            Dim valor As String = ""
                            For Each orow As DataRow In cAdjuntos.Data.Tables(0).Rows
                                campo = orow.Item(1)
                                If valor <> "" Then valor = valor & "xx"
                                valor = valor & orow.Item(0)
                            Next
                            params = "instancia=" & CType(dataRow("ID"), Integer) & "&Campo=" & campo & "&Valor=" & valor
                            items(gridCellIndex).Text = "<span rutaAdjuntos='" & params & "'>" & items(gridCellIndex).Text & "</span>"
                        End If
                    End If
                ElseIf _camposEditor.ContainsKey(columna.Key) Then
                    items(gridCellIndex).Text = "<img src='" & ConfigurationManager.AppSettings("ruta") & "Images/edit2.gif' name='editor'/>" &
                        "<span style='display:none;' titulo='" & CType(columns(columna.Key), BoundDataField).Header.Text & "' id='" & dataRow("ID") & "' campo_origen='" & Split(columna.Key, "_")(Split(columna.Key, "_").Length - 1) & "'></span>"
                ElseIf _camposTexto.ContainsKey(columna.Key) AndAlso Len(dataRow(columna.Key).ToString.Trim()) > 0 Then
                    items(gridCellIndex).Text = "<img class='imgTexto' src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/coment.gif' name='comentario' " &
                        "onclick='MostrarInfoTextoLargo(""" & columns(columna.Key).Header.Text & """,this,""" & CType(dataRow("ID"), Integer) & """,""" & columna.Key & """)'/>" &
                        "<span name='comentario' instanciaId='" & CType(dataRow("ID"), Integer) & "' " &
                                    "instanciaEstado='" & iEstado & "' " &
                                    "instanciaTipoSolicitud='" & DBNullToInteger(dataRow("TIPO_SOLIC")) & "' " &
                                    "instanciaBloque='" & CType(dataRow("BLOQUE"), Integer) & "'" &
                                    "obsypart='" & dataRow("OBSYPART") & "'>" & IIf(dataRow(columna.Key).length > 50, Left(dataRow(columna.Key), 50) & "...", dataRow(columna.Key)) & "</span>"
                    items(gridCellIndex).CssClass = "imgOcultoCeldaWebHierarchical"
                Else
                    ' Preparamos el span con las propiedades de la fila
                    Dim cellText As String = "<span instanciaId='" & CType(dataRow("ID"), Integer) & "' " &
                        "instanciaEstado='" & iEstado & "' " &
                        "instanciaTipoSolicitud='" & DBNullToInteger(dataRow("TIPO_SOLIC")) & "' " &
                        "instanciaBloque='" & CType(dataRow("BLOQUE"), Integer) & "'" &
                        "obsypart='" & dataRow("OBSYPART") & "'>{0}</span>"
                    Select Case columna.Key
                        Case "ACCION_APROBAR"
                            If Not DBNullToInteger(e.Row.DataItem.Item.Row.Item("ACCION_APROBAR")) = 0 Then
                                whdgSolicitudes.Columns("ACCION_APROBAR").Hidden = False
                                items(gridCellIndex).Text = "<input type='checkbox' " &
                                    "data='" & dataRow("BLOQUE") & "|" & dataRow("ACCION_APROBAR") & "' " &
                                    "id='chkAprobar_" & dataRow("ID") & "'" & If(InstanciasAprobar.ContainsKey(dataRow("ID")), " checked", "") & "/>"
                            Else
                                items(gridCellIndex).Text = "<input type='checkbox' id='chkAprobar_" & dataRow("ID") & "' disabled='true'/>"
                            End If
                        Case "ACCION_RECHAZAR"
                            If Not DBNullToInteger(dataRow("ACCION_RECHAZAR")) = 0 Then
                                whdgSolicitudes.Columns("ACCION_RECHAZAR").Hidden = False
                                items(gridCellIndex).Text = "<input type='checkbox' " &
                                    "data='" & dataRow("BLOQUE") & "|" & dataRow("ACCION_RECHAZAR") & "' " &
                                    "id='chkRechazar_" & dataRow("ID") & "'" & If(InstanciasRechazar.ContainsKey(dataRow("ID")), " checked", "") & "/>"
                            Else
                                items(gridCellIndex).Text = "<input type='checkbox' id='chkRechazar_" & dataRow("ID") & "' disabled='true'/>"
                            End If
                        Case "RESUMEN"
                            items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/abrir_ptos_susp.gif'" &
                                "class='Image16' onclick=""" &
                                "IrADetalleConsulta('" & dataRow("ID") & "','" & dataRow("ACCION_APROBAR") & "','" &
                                dataRow("ACCION_RECHAZAR") & "','" & dataRow("BLOQUE") & "','" &
                                IIf(Not IsDBNull(dataRow("ETAPA_ACTUAL")) AndAlso dataRow("ETAPA_ACTUAL") = "##", Textos(124),
                                dataRow("ETAPA_ACTUAL")) & "'," & dataRow("OBSERVADOR") & ");return false;""/>"
                        Case "ERROR_INTEGRACION"
                            If DBNullToBoolean(dataRow("KO")) Then
                                whdgSolicitudes.Columns("ERROR_INTEGRACION").Hidden = False
                                items(gridCellIndex).Text = "<img style='margin:0em 0.1em;' title='" & Textos(120) & " " & dataRow("ERROR") & "' " &
                                    "src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Icono_Error_Amarillo.gif" & "'/>"
                            End If
                        Case "PROCESOS"
                            If Not DBNullToSomething(dataRow("PROCESOS")) = Nothing Then
                                Dim processCellText As String = "<span instanciaId='" & CType(dataRow("ID"), Integer) & "' " &
                                        "instanciaEstado='" & iEstado & "' " &
                                        "instanciaTipoSolicitud='" & DBNullToInteger(dataRow("TIPO_SOLIC")) & "' " &
                                        "instanciaBloque='" & CType(dataRow("BLOQUE"), Integer) & "'" &
                                        "obsypart='" & dataRow("OBSYPART") & "' {1}>{0}</span>"

                                items(gridCellIndex).Text = String.Format(processCellText, Left(FormatoDenEstado(dataRow("PROCESOS")), FormatoDenEstado(dataRow("PROCESOS")).Length - 1),
                                                                          "onclick=""MostrarProcesosAsociados('" & columns(columna.Key).Header.Text & "','" &
                                                                            Replace(Left(FormatoDenEstado(dataRow("PROCESOS")), FormatoDenEstado(dataRow("PROCESOS")).Length - 1), ",", ",<br/>") & "')""")
                                items(gridCellIndex).Tooltip = Replace(Left(FormatoDenEstado(dataRow("PROCESOS")), FormatoDenEstado(dataRow("PROCESOS")).Length - 1), ",", "," & vbCrLf)
                            Else
                                items(gridCellIndex).Text = String.Format(cellText, String.Empty)
                            End If
                        Case "ERROR_VALIDACION"
                            If CType(dataRow("EST_VALIDACION"), Integer) > 1 AndAlso CType(dataRow("EST_VALIDACION"), Integer) < 99 Then
                                whdgSolicitudes.Columns("ERROR_VALIDACION").Hidden = False
                                items(gridCellIndex).Text = "<img style='margin:0em 0.1em;' " &
                                    "title='" & IIf(CType(dataRow("EST_VALIDACION"), Integer) = 9,
                                    Textos(121) & " " & dataRow("FECHA_VALIDACION") & " " & Mid(Textos(122), 1, InStr(Textos(122), ".")) & " " & Textos(123),
                                    Textos(121) & " " & dataRow("FECHA_VALIDACION") & " " & Textos(122) & " " & dataRow("ERROR_VALIDACION")) & "' " &
                                    "src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Icono_Error_Amarillo.gif" & "'/>"
                            End If
                        Case "ERROR"
                            If DBNullToBoolean(dataRow("ICONO")) Then
                                whdgSolicitudes.Columns("ERROR").Hidden = False
                                items(gridCellIndex).Text = "<img style='margin:0em 0.1em;'  title='' class='Image16' " &
                                    "src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Img_Ico_Alerta.gif" & "'/>"
                            End If
                        Case "SEG"
                            If CType(dataRow("SEG"), Boolean) Then
                                items(gridCellIndex).Text = "<img id='monitorizacionOn_" & CType(dataRow("ID"), Integer) & "' " &
                                    "src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/seg_selec.gif" & "'/>" &
                                    "<img id='monitorizacionOff_" & CType(dataRow("ID"), Integer) & "' " &
                                    "src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/seg_noselec.gif" & "' style='display:none;'/>"
                            Else
                                items(gridCellIndex).Text = "<img id='monitorizacionOn_" & CType(dataRow("ID"), Integer) & "' " &
                                    "src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/seg_selec.gif" & "' style='display:none;'/>" &
                                    "<img id='monitorizacionOff_" & CType(dataRow("ID"), Integer) & "' " &
                                    "src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/seg_noselec.gif" & "'/>"
                            End If
                        Case "ESTADO", "ETAPA", "ETAPA_ACTUAL"
                            ' Para un Estado o Etapa reemplazamos su Id por el Texto en el lenguaje correspondiente
                            If {1, 2}.Contains(CType(dataRow("EN_PROCESO"), Integer)) Then
                                ' En Proceso
                                items(gridCellIndex).Text = Textos(112)
                            Else
                                Select Case CType(dataRow("ESTADO"), Integer)
                                    Case Fullstep.FSNLibrary.TipoEstadoSolic.SCRechazada, TipoEstadoSolic.SCAnulada,
                                    TipoEstadoSolic.SCPendiente, TipoEstadoSolic.SCAprobada
                                        items(gridCellIndex).Text = Textos(113)
                                    Case Fullstep.FSNLibrary.TipoEstadoSolic.Rechazada
                                        items(gridCellIndex).Text = Textos(114)
                                    Case Fullstep.FSNLibrary.TipoEstadoSolic.Anulada
                                        items(gridCellIndex).Text = Textos(115)
                                    Case Fullstep.FSNLibrary.TipoEstadoSolic.SCCerrada
                                        ' Cerrada
                                        items(gridCellIndex).Text = Textos(116)
                                    Case Else  'Esta guardada o en curso.Se muestra la etapa actual
                                        If columna.Key = "ESTADO" Then
                                            Select Case CType(dataRow("ESTADO"), Integer)
                                                Case Fullstep.FSNLibrary.TipoEstadoSolic.Guardada
                                                    items(gridCellIndex).Text = Textos(183)
                                                Case Fullstep.FSNLibrary.TipoEstadoSolic.EnCurso
                                                    If DBNullToBoolean(dataRow("ICONO")) Then
                                                        items(gridCellIndex).Text = Textos(186)
                                                    Else
                                                        items(gridCellIndex).Text = Textos(4)
                                                    End If
                                            End Select
                                        Else
                                            If dataRow("ETAPA_ACTUAL") = "##" Then 'Etapas en paralelo
                                                items(gridCellIndex).Text = Textos(124)
                                            Else
                                                items(gridCellIndex).Text = dataRow("ETAPA_ACTUAL")
                                            End If
                                            If DBNullToBoolean(dataRow("VER_FLUJO")) Then
                                                items(gridCellIndex).Text = "<span instanciaId='" & CType(dataRow("ID"), Integer) & "'>" &
                                                    items(gridCellIndex).Text & Space(1) & "<img " &
                                                        "src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/observaciones_12x12.gif" & "'/></span>"
                                            End If
                                        End If
                                End Select
                            End If
                        Case "ESTADO_HOMOLOGACION"
                            items(gridCellIndex).Text = String.Format(cellText, DBNullToStr(dataRow("ESTADO_HOMOLOGACION_DEN")))
                        Case "USUARIO", "PETICIONARIO"
                            ' Para Usuario o Peticionario metemos el texto dentro de un span con las propiedades del Usuario/Peticionario correspondiente
                            If {1, 2}.Contains(CType(dataRow("EN_PROCESO"), Integer)) AndAlso columna.Key = "USUARIO" Then
                                ' Escribimos "En Proceso"
                                items(gridCellIndex).Text = Textos(112)
                            Else
                                If (Not CType(dataRow("TRASLADADA"), Boolean) AndAlso columna.Key = "USUARIO") OrElse columna.Key = "PETICIONARIO" Then
                                    If DBNullToBoolean(dataRow("VER_DETALLE_PER")) OrElse DBNullToBoolean(dataRow("VER_DETALLE_PER_ROL")) Then
                                        If columna.Key = "USUARIO" AndAlso IsDBNull(dataRow("USUARIO")) AndAlso Not {1, 2}.Contains(CType(dataRow("EN_PROCESO"), Integer)) Then
                                            items(gridCellIndex).Text = "<span proveedor='" & dataRow("PROVEEDOR") & "'>" & dataRow("USU") & "</span>"
                                        Else
                                            items(gridCellIndex).Text = "<span " & LCase(columna.Key) & "='"
                                            If columna.Key = "USUARIO" Then
                                                items(gridCellIndex).Text &= dataRow("USUARIO") & "'>" & dataRow("USU")
                                            Else
                                                If String.IsNullOrEmpty(dataRow("PETICIONARIO").ToString) Then
                                                    items(gridCellIndex).Text &= dataRow("PETICIONARIO_PROVE") & "' proveedor"
                                                Else
                                                    items(gridCellIndex).Text &= dataRow("PETICIONARIO") & "'"
                                                End If
                                                items(gridCellIndex).Text &= ">" & dataRow("PET")
                                            End If
                                            items(gridCellIndex).Text &= "</span>"
                                        End If
                                    Else 'en blanco
                                        items(gridCellIndex).Text = String.Empty
                                    End If
                                ElseIf CType(dataRow("TRASLADADA"), Boolean) AndAlso columna.Key = "USUARIO" Then
                                    items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/TrasladarSolicitud.png" & "'/>" & Space(1) & dataRow("TRASLADO_A_NOMBRE")
                                    items(gridCellIndex).Tooltip = Textos(118) & Chr(10) & Chr(13) & Textos(117) & ": " & FormatDate(dataRow("FECHA_ACT"), FSNUser.DateFormat, "g")
                                    If Not IsDBNull(dataRow("FECHA_LIMITE")) Then
                                        items(gridCellIndex).Tooltip &= (Chr(10) & Chr(13) & Textos(119) & " " & FormatDate(dataRow("FECHA_LIMITE"), FSNUser.DateFormat, "d"))
                                    End If
                                    If Not IsDBNull(dataRow("TRASLADO_A_PROV")) Then
                                        items(gridCellIndex).Text = "<span proveedor='" & dataRow("TRASLADO_A_PROV") & "'>" & items(gridCellIndex).Text & "</span>"
                                    ElseIf Not IsDBNull(dataRow("TRASLADO_A_USU")) Then
                                        items(gridCellIndex).Text = "<span usuario='" & dataRow("TRASLADO_A_USU") & "'>" & items(gridCellIndex).Text & "</span>"
                                    End If
                                End If
                            End If
                        Case "MOTIVO_VISIBILIDAD"
                            ' Para "Motivo Visibilidad" reemplazamos su Id por el Texto en el lenguaje correspondiente
                            Select Case CType(dataRow("MOTIVO_VISIBILIDAD"), Integer)
                                Case MotivoVisibilidadSolicitud.SolicitudAbiertaUsted  'Solicitudes abiertas por usted
                                    items(gridCellIndex).Text = Textos(184)
                                Case MotivoVisibilidadSolicitud.SolicitudParticipo
                                    items(gridCellIndex).Text = Textos(185)
                                Case MotivoVisibilidadSolicitud.SolicitudPendiente
                                    items(gridCellIndex).Text = Textos(186)
                                Case MotivoVisibilidadSolicitud.SolicitudPenditenteDevolucion
                                    items(gridCellIndex).Text = Textos(187)
                                Case MotivoVisibilidadSolicitud.SolicitudTrasladadaEsperaDevolucion
                                    items(gridCellIndex).Text = Textos(188)
                                Case Else 'Observador
                                    items(gridCellIndex).Text = Textos(189)
                            End Select
                        Case "IMPORTE"
                            If dataRow("IMPORTE") IsNot Nothing AndAlso Not IsDBNull(dataRow("IMPORTE")) Then
                                items(gridCellIndex).Text = FSNLibrary.FormatNumber(CType(DBNullToDbl(dataRow("IMPORTE")), Double), FSNUser.NumberFormat) & " " & dataRow("MON")
                            Else
                                items(gridCellIndex).Text = ""
                            End If
                            ' Metemos el valor en el span
                            items(gridCellIndex).Text = String.Format(cellText, items(gridCellIndex).Text)
                        Case "ID"
                            If Not CType(dataRow("NUM_REEMISIONES"), Integer) = 0 Then
                                items(gridCellIndex).Text = items(gridCellIndex).Text & " (" & CType(dataRow("NUM_REEMISIONES"), Integer) & ")"
                            End If
                            ' Metemos el valor en el span
                            items(gridCellIndex).Text = String.Format(cellText, items(gridCellIndex).Text)
                        Case "FECHA_ACT"
                            If dataRow("TRASLADADA") Then
                                items(gridCellIndex).Text = FormatDate(CType(dataRow("FECHA_ACT"), Date), FSNUser.DateFormat, "d")
                            Else
                                items(gridCellIndex).Text = String.Empty
                            End If
                            ' Metemos el valor en el span
                            items(gridCellIndex).Text = String.Format(cellText, items(gridCellIndex).Text)
                        Case "FECULTAPROBACION"
                            If String.IsNullOrEmpty(dataRow("FECULTAPROBACION").ToString) Then
                                items(gridCellIndex).Text = ""
                            Else
                                items(gridCellIndex).Text = FormatDate(dataRow("FECULTAPROBACION"), FSNUser.DateFormat, "d")
                            End If

                            ' Metemos el valor en el span
                            items(gridCellIndex).Text = String.Format(cellText, items(gridCellIndex).Text)
                        Case "FECHA_LIMITE"
                            If dataRow("TRASLADADA") Then
                                items(gridCellIndex).Text = FormatDate(CType(dataRow("FECHA_LIMITE"), Date), FSNUser.DateFormat, "d")
                            Else
                                items(gridCellIndex).Text = String.Empty
                            End If
                            ' Metemos el valor en el span
                            items(gridCellIndex).Text = String.Format(cellText, items(gridCellIndex).Text)
                        Case "PROVEEDOR_INSTANCIA"
                            If IsDBNull(dataRow("PROVEEDOR_INSTANCIA")) Then
                                If dataRow.Table.Columns.Contains("PROVEEDOR") Then
                                    items(gridCellIndex).Text = DBNullToStr(dataRow("PROVEEDOR"))
                                End If
                            Else
                                items(gridCellIndex).Text = DBNullToStr(dataRow("PROVEEDOR_INSTANCIA")) & " - " & DBNullToStr(dataRow("PROVEEDOR_INSTANCIA_DEN"))
                            End If
                            items(gridCellIndex).Text = String.Format(cellText, items(gridCellIndex).Text)
                        Case "ARTICULO_INSTANCIA"
                            If Not IsDBNull(dataRow("ARTICULO_INSTANCIA")) Then
								items(gridCellIndex).Text = DBNullToStr(dataRow("ARTICULO_INSTANCIA")) & " - " & DBNullToStr(dataRow("ARTICULO_INSTANCIA_DEN"))
							End If
						Case Else
							' Si no estamos en una celda que no debe ir al detalle, metemos el texto dentro de un span con las propiedades de la fila
							If Not {"ESTADO", "ETAPA", "USUARIO", "PETICIONARIO", "SEG"}.Contains(columna.Key) Then
								If items(gridCellIndex).Value IsNot Nothing Then
									Select Case items(gridCellIndex).Value.GetType
										Case GetType(System.Boolean), GetType(System.Byte)
											' Si es booleano, escribimos Sí/No en el lenguaje correspondiente
											If CType(items(gridCellIndex).Value, Boolean) Then
												items(gridCellIndex).Text = Textos(110)
											Else
												items(gridCellIndex).Text = Textos(111)
											End If
										Case GetType(System.Int16), GetType(System.Int32), GetType(System.Int64), GetType(System.Double)
											If Not IsDBNull(items(gridCellIndex).Value) Then items(gridCellIndex).Text = FSNLibrary.FormatNumber(CType(items(gridCellIndex).Value, Double), FSNUser.NumberFormat)
										Case GetType(System.DateTime)
											' Si es una fecha, la formateamos según las preferencias de usuario
											items(gridCellIndex).Text = FormatDate(items(gridCellIndex).Value, FSNUser.DateFormat, "d")
									End Select
								End If
								' Metemos el valor en el span
								items(gridCellIndex).Text = String.Format(cellText, items(gridCellIndex).Text)
							End If
					End Select
				End If
			Next
		Else
			' Fila de una banda
			Dim band As Band = CType(e.Row, ContainerGridRecord).Owner.ControlMain.Band
			For Each columna As GridField In band.Columns
				If columna.Key = "LINEA" OrElse dataRow("DESGLOSE") = Split(columna.Key, "_")(1) Then
					If dataRow(columna.Key) IsNot Nothing AndAlso Not IsDBNull(dataRow(columna.Key)) Then
						If _camposArchivo.ContainsKey(columna.Key) Then
							Dim cAdjuntos As FSNServer.Adjuntos
							cAdjuntos = FSNServer.Get_Object(GetType(FSNServer.Adjuntos))
							cAdjuntos.LoadAdjuntosPorInstanciayCampo(dataRow("FORM_INSTANCIA"), _camposArchivo(columna.Key))
							If cAdjuntos.Data.Tables(0).Rows.Count > 0 Then
								Dim params As String
								If cAdjuntos.Data.Tables(0).Rows.Count = 1 Then
									params = "instancia=" & CType(dataRow("ID"), Integer) & "&id=" & cAdjuntos.Data.Tables(0).Rows(0).Item(0)
									items(columna.Index).Text = "<span rutaAdjunto='" & params & "'>" & items(columna.Index).Text & "</span>"
								Else
									Dim campo As String = ""
									Dim valor As String = ""
									For Each orow As DataRow In cAdjuntos.Data.Tables(0).Rows
										campo = orow.Item(1)
										If valor <> "" Then valor = valor & "xx"
										valor = valor & orow.Item(0)
									Next
									params = "instancia=" & CType(dataRow("ID"), Integer) & "&Campo=" & campo & "&Valor=" & valor
									items(columna.Index).Text = "<span rutaAdjuntos='" & params & "'>" & items(columna.Index).Text & "</span>"
								End If
							End If
						ElseIf _camposEditor.ContainsKey(columna.Key) Then
							items(columna.Index).Text = "<img src='" & ConfigurationManager.AppSettings("ruta") & "Images/edit2.gif' name='editor'/>" &
								"<span style='display:none;' titulo='" & CType(columns(columna.Key), BoundDataField).Header.Text & "' id='" & dataRow("ID") & "' campo_origen='" & Split(columna.Key, "_")(Split(columna.Key, "_").Length - 1) & "'></span>"
						ElseIf _camposTexto.ContainsKey(columna.Key) AndAlso Len(dataRow(columna.Key).ToString.Trim()) > 0 Then
							items(columna.Index).Text = "<img class='imgTexto' src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/coment.gif' name='comentario' " &
								"onclick='MostrarInfoTextoLargo(""" & columns(columna.Key).Header.Text & """,this,""" & CType(dataRow("FORM_INSTANCIA"), Integer) & """,""" & columna.Key & """)'/>" &
								"<span name='comentario'>" & IIf(dataRow(columna.Key).length > 50, Left(dataRow(columna.Key), 50) & "...", dataRow(columna.Key)) & "</span>"
							items(columna.Index).CssClass = "imgOcultoCeldaWebHierarchical"
						ElseIf columna.Key = "LINEA" Then
							items(columna.Index).Text = CType(items(columna.Index).Value, Integer)
						Else
							If items(columna.Index).Value IsNot Nothing Then
								Select Case items(columna.Index).Value.GetType
									Case GetType(System.Boolean), GetType(System.Byte)
										' Si es booleano, escribimos Sí/No en el lenguaje correspondiente
										If CType(items(columna.Index).Value, Boolean) Then
											items(columna.Index).Text = Textos(110)
										Else
											items(columna.Index).Text = Textos(111)
										End If
									Case GetType(System.Int16), GetType(System.Int32), GetType(System.Int64), GetType(System.Double)
										If Not IsDBNull(items(columna.Index).Value) Then items(columna.Index).Text = FSNLibrary.FormatNumber(CType(items(columna.Index).Value, Double), FSNUser.NumberFormat)
									Case GetType(System.DateTime)
										' Si es una fecha, la formateamos según las preferencias de usuario
										items(columna.Index).Text = FormatDate(items(columna.Index).Value, FSNUser.DateFormat, "d")
								End Select
							End If
						End If
					End If
				Else
					items(columna.Index).CssClass = "celdaValorNuloWebHierarchical"
					items(columna.Index).Text = ""
				End If
			Next
		End If
	End Sub
	Private Function FormatoDenEstado(ByVal sender As String) As String
		Dim sEstado As String = ""
		sEstado = Replace(sender, "(##" & CStr(TipoEstadoProceso.ConItemsSinValidar) & "##)", "(" & Textos(214) & ")")
		sEstado = Replace(sEstado, "(##" & CStr(TipoEstadoProceso.Conproveasignados) & "##)", "(" & Textos(215) & ")")
		sEstado = Replace(sEstado, "(##" & CStr(TipoEstadoProceso.conasignacionvalida) & "##)", "(" & Textos(216) & ")")
		sEstado = Replace(sEstado, "(##" & CStr(TipoEstadoProceso.conpeticiones) & "##)", "(" & Textos(217) & ")")
		sEstado = Replace(sEstado, "(##" & CStr(TipoEstadoProceso.ConObjetivosSinNotificar) & "##)", "(" & Textos(218) & ")")
		sEstado = Replace(sEstado, "(##" & CStr(TipoEstadoProceso.PreadjYConObjNotificados) & "##)", "(" & Textos(219) & ")")
		sEstado = Replace(sEstado, "(##" & CStr(TipoEstadoProceso.ParcialmenteCerrado) & "##)", "(" & Textos(220) & ")")
		sEstado = Replace(sEstado, "(##" & CStr(TipoEstadoProceso.conadjudicaciones) & "##)", "(" & Textos(221) & ")")
		sEstado = Replace(sEstado, "(##" & CStr(TipoEstadoProceso.ConAdjudicacionesNotificadas) & "##)", "(" & Textos(222) & ")")
		sEstado = Replace(sEstado, "(##" & CStr(TipoEstadoProceso.Cerrado) & "##)", "(" & Textos(223) & ")")
		Return sEstado
	End Function
	Private Sub whdgExcelExporter_Exported(sender As Object, e As Infragistics.Web.UI.GridControls.ExcelExportedEventArgs) Handles whdgExcelExporter.Exported
		Dim i As Integer
		e.Worksheet.Columns.Item(0).Width = 256 * 65
		For i = 1 To e.Worksheet.Columns.Count - 1
			e.Worksheet.Columns.Item(i).Width = 256 * 30
		Next
	End Sub
	''' <summary>
	''' Formatea los datos a exportar (numeros / fecha) o traduce los numeros por texos comprensibles (ejemplo: booleanos/EN_PROCESO).
	''' </summary>
	''' <param name="sender">grid</param>
	''' <param name="e">evento</param>
	''' <remarks>Llamada desde: Sistema; Tiempo máximo:0</remarks>
	Private Sub whdgExcelExporter_GridRecordItemExported(sender As Object, e As Infragistics.Web.UI.GridControls.GridRecordItemExportedEventArgs) Handles whdgExcelExporter.GridRecordItemExported
		Dim FormatoNum As String = FormatoNumeroExcel()

		ModuloIdioma = TiposDeDatos.ModulosIdiomas.Seguimiento
		Select Case e.GridCell.Column.Key
			Case "ERROR_INTEGRACION"
				If DBNullToBoolean(e.GridCell.Row.DataItem.Item("KO")) Then
					whdgExportacion.Columns("ERROR_INTEGRACION").Hidden = False
					whdgExportacion.GridView.Columns("ERROR_INTEGRACION").Hidden = False
					e.WorksheetCell.Value = Textos(120) & " " & e.GridCell.Row.DataItem.Item("ERROR")
				End If
			Case "ERROR_VALIDACION"
				If CType(e.GridCell.Row.DataItem.Item("EST_VALIDACION"), Integer) > 1 AndAlso CType(e.GridCell.Row.DataItem.Item("EST_VALIDACION"), Integer) < 99 Then
					whdgSolicitudes.Columns("ERROR_VALIDACION").Hidden = False
					whdgSolicitudes.GridView.Columns("ERROR_VALIDACION").Hidden = False
					e.WorksheetCell.Value = IIf(CType(e.GridCell.Row.DataItem.Item("EST_VALIDACION"), Integer) = 9,
						Textos(121) & " " & e.GridCell.Row.DataItem.Item("FECHA_VALIDACION") & " " & Mid(Textos(122), 1, InStr(Textos(122), ".")) & " " & Textos(123),
						Textos(121) & " " & e.GridCell.Row.DataItem.Item("FECHA_VALIDACION") & " " & Textos(122) & " " & e.GridCell.Row.DataItem.Item("ERROR_VALIDACION"))
				End If
			Case "ESTADO", "ETAPA"
				' Para un Estado o Etapa reemplazamos su Id por el Texto en el lenguaje correspondiente
				If {1, 2}.Contains(e.GridCell.Row.DataItem.Item("EN_PROCESO")) Then
					' En Proceso
					e.WorksheetCell.Value = Textos(112)
				Else
					Select Case e.GridCell.Row.DataItem.Item("ESTADO")
						Case Fullstep.FSNLibrary.TipoEstadoSolic.SCRechazada, TipoEstadoSolic.SCAnulada,
						TipoEstadoSolic.SCPendiente, TipoEstadoSolic.SCAprobada
							' Finalizada
							e.WorksheetCell.Value = Textos(113)
						Case Fullstep.FSNLibrary.TipoEstadoSolic.Rechazada
							' Rechazada
							e.WorksheetCell.Value = Textos(114)
						Case Fullstep.FSNLibrary.TipoEstadoSolic.Anulada
							' Anulada
							e.WorksheetCell.Value = Textos(115)
						Case Fullstep.FSNLibrary.TipoEstadoSolic.SCCerrada
							' Cerrada
							e.WorksheetCell.Value = Textos(116)
						Case Else  'Esta guardada o en curso.Se muestra la etapa actual
							If e.GridCell.Column.Key = "ESTADO" Then
								Select Case e.GridCell.Row.DataItem.Item("ESTADO")
									Case Fullstep.FSNLibrary.TipoEstadoSolic.Guardada
										' Guardada
										e.WorksheetCell.Value = Textos(183)
									Case Fullstep.FSNLibrary.TipoEstadoSolic.EnCurso
										' En Curso
										e.WorksheetCell.Value = Textos(4)
								End Select
							Else
								If e.GridCell.Row.DataItem.Item("ETAPA_ACTUAL") = "##" Then 'Etapas en paralelo
									e.WorksheetCell.Value = Textos(29)
								Else
									e.WorksheetCell.Value = e.GridCell.Row.DataItem.ITEM("ETAPA_ACTUAL")
								End If
							End If
					End Select
				End If
			Case "PROCESOS"
				If Not DBNullToSomething(e.GridCell.Row.DataItem.Item("PROCESOS")) = Nothing Then
					e.WorksheetCell.Value = FormatoDenEstado(e.GridCell.Row.DataItem.Item("PROCESOS"))
				Else
					e.WorksheetCell.Value = String.Empty
				End If
			Case "IMPORTE"
				If e.GridCell.Value IsNot Nothing Then
					If Not IsDBNull(e.GridCell.Value) Then
						If {TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras, TiposDeDatos.TipoDeSolicitud.PedidoExpress, TiposDeDatos.TipoDeSolicitud.PedidoNegociado}.Contains(CType(e.GridCell.Row.DataItem.Item("TIPO_SOLIC"), Integer)) Then
							e.WorksheetCell.Value = FSNLibrary.FormatNumber(CType(e.GridCell.Value, Double), FSNUser.NumberFormat) & " " & e.GridCell.Row.DataItem.Item("MON")
						ElseIf Not {"ID"}.Contains(e.GridCell.Column.Key) AndAlso
						{TiposDeDatos.TipoDeSolicitud.Otros}.Contains(CType(e.GridCell.Row.DataItem.Item("TIPO_SOLIC"), Integer)) Then
							e.WorksheetCell.CellFormat.FormatString = FormatoNum
						End If
					End If
				Else
					e.WorksheetCell.CellFormat.FormatString = FormatoNum
				End If
			Case "USUARIO", "PETICIONARIO"
				If {1, 2}.Contains(CType(e.GridCell.Row.DataItem.Item("EN_PROCESO"), Integer)) AndAlso e.GridCell.Column.Key = "USUARIO" Then
					' Escribimos "En Proceso"
					e.WorksheetCell.Value = Textos(112)
				Else
					If (Not CType(e.GridCell.Row.DataItem.Item("TRASLADADA"), Boolean) AndAlso e.GridCell.Column.Key = "USUARIO") OrElse e.GridCell.Column.Key = "PETICIONARIO" Then
						If DBNullToBoolean(e.GridCell.Row.DataItem.Item("VER_DETALLE_PER")) OrElse DBNullToBoolean(e.GridCell.Row.DataItem.Item("VER_DETALLE_PER_ROL")) Then
							If e.GridCell.Column.Key = "USUARIO" And Not IsDBNull(e.GridCell.Row.DataItem.Item("PROVEEDOR")) AndAlso Not {1, 2}.Contains(CType(e.GridCell.Row.DataItem.Item("EN_PROCESO"), Integer)) Then
								e.WorksheetCell.Value = e.GridCell.Row.DataItem.Item("PROVEEDOR")
							Else
								If e.GridCell.Column.Key = "USUARIO" Then
									e.WorksheetCell.Value = e.GridCell.Row.DataItem.Item("USU")
								Else
									e.WorksheetCell.Value = e.GridCell.Row.DataItem.Item("PET")
								End If
							End If
						Else 'en blanco
							e.WorksheetCell.Value = String.Empty
						End If
					ElseIf CType(e.GridCell.Row.DataItem.Item("TRASLADADA"), Boolean) AndAlso e.GridCell.Column.Key = "USUARIO" Then
						e.WorksheetCell.Value = e.GridCell.Row.DataItem.Item("TRASLADO_A_NOMBRE")
					End If
				End If
			Case "MOTIVO_VISIBILIDAD"
				' Para "Motivo Visibilidad" reemplazamos su Id por el Texto en el lenguaje correspondiente
				Select Case CType(e.GridCell.Row.DataItem.Item("MOTIVO_VISIBILIDAD"), Integer)
					Case MotivoVisibilidadSolicitud.SolicitudAbiertaUsted  'Solicitudes abiertas por usted
						e.WorksheetCell.Value = Textos(184)
					Case MotivoVisibilidadSolicitud.SolicitudParticipo
						e.WorksheetCell.Value = Textos(185)
					Case MotivoVisibilidadSolicitud.SolicitudPendiente
						e.WorksheetCell.Value = Textos(186)
					Case MotivoVisibilidadSolicitud.SolicitudPenditenteDevolucion
						e.WorksheetCell.Value = Textos(187)
					Case MotivoVisibilidadSolicitud.SolicitudTrasladadaEsperaDevolucion
						e.WorksheetCell.Value = Textos(188)
					Case Else 'Observador
						e.WorksheetCell.Value = Textos(189)
				End Select
			Case "PROVEEDOR_INSTANCIA"
				If IsDBNull(e.GridCell.Row.DataItem.Item("PROVEEDOR_INSTANCIA")) Then
					If e.GridCell.Row.DataItem.Item.DataView.table.Columns.Contains("PROVEEDOR") Then
						e.WorksheetCell.Value = DBNullToStr(e.GridCell.Row.DataItem.Item("PROVEEDOR"))
					End If
				Else
					e.WorksheetCell.Value = DBNullToStr(e.GridCell.Row.DataItem.Item("PROVEEDOR_INSTANCIA")) & " - " & DBNullToStr(e.GridCell.Row.DataItem.Item("PROVEEDOR_INSTANCIA_DEN"))
				End If
			Case "ARTICULO_INSTANCIA"
				If Not IsDBNull(e.GridCell.Row.DataItem.Item("ARTICULO_INSTANCIA")) Then
					e.WorksheetCell.Value = DBNullToStr(e.GridCell.Row.DataItem.Item("ARTICULO_INSTANCIA")) & " - " & DBNullToStr(e.GridCell.Row.DataItem.Item("ARTICULO_INSTANCIA_DEN"))
				End If
			Case Else
				If _camposEditor.ContainsKey(e.GridCell.Column.Key) Then
                    Dim dsCampo As DataSet
                    Dim oInstancia As FSNServer.Instancia
                    Dim oValor As Object = DBNull.Value
                    oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                    dsCampo = oInstancia.cargarCampoOrigen(e.GridCell.Row.DataItem.Item("ID"), Split(e.GridCell.Column.Key, "_")(Split(e.GridCell.Column.Key, "_").Length - 1))
                    oInstancia = Nothing
                    If Not dsCampo Is Nothing AndAlso dsCampo.Tables(0).Rows.Count > 0 AndAlso Not IsDBNull(dsCampo.Tables(0).Rows(0).Item("VALOR_TEXT")) Then
                        oValor = dsCampo.Tables(0).Rows(0).Item("VALOR_TEXT")
                    End If
                    dsCampo = Nothing
                    If IsDBNull(oValor) Then
                        e.WorksheetCell.Value = ""
                    Else
                        e.WorksheetCell.Value = StripTags(HttpUtility.HtmlDecode(Server.UrlDecode(oValor)))
                    End If
                Else
                    Select Case e.GridCell.Column.Type
                        Case System.Type.GetType("System.Boolean"), System.Type.GetType("System.Byte")
                            If e.GridCell.Value IsNot System.DBNull.Value Then e.WorksheetCell.Value = IIf(e.GridCell.Value, Textos(110), Textos(111))
                        Case System.Type.GetType("System.Int16"), System.Type.GetType("System.Int32"), System.Type.GetType("System.Int64"), System.Type.GetType("System.Double")
                            If Not {"ID"}.Contains(e.GridCell.Column.Key) Then
                                e.WorksheetCell.CellFormat.FormatString = FormatoNum
                            End If
                        Case System.Type.GetType("System.DateTime")
                            e.WorksheetCell.CellFormat.FormatString = FSNUser.DateFormat.ShortDatePattern
                        Case Else
                    End Select
                End If
        End Select
    End Sub
    Private Sub whdgExcelExporter_RowExported(sender As Object, e As Infragistics.Web.UI.GridControls.ExcelRowExportedEventArgs) Handles whdgExcelExporter.RowExported
        Dim nivel As Integer = e.CurrentOutlineLevel
        If e.IsHeaderRow Then
            e.Worksheet.Rows(e.CurrentRowIndex).CellFormat.Font.Bold = Infragistics.Documents.Excel.ExcelDefaultableBoolean.True
        End If
    End Sub
    ''Codigo para guardar datos del FSAL cuando el grid termine de cargarse en el servidor.
    ''Llama a la funcion de registro con diferentes parametros dependiendo de si hay que actualizar o registrar uno nuevo.
    Private Sub whdgSolicitudes_Unload(sender As Object, e As System.EventArgs) Handles whdgSolicitudes.Unload
        If bActivadoFSAL.Value = "1" Then
            Dim fechaFinFSAL8 As DateTime = DateTime.UtcNow
            If bEnviadoFSAL8Inicial.Value = "0" AndAlso IsPostBack Then
                Dim sArgs(5) As Object
                sArgs(0) = 8
                sArgs(1) = sFechaIniFSAL8.Value
                sArgs(2) = Format(fechaFinFSAL8, "yyyy-MM-dd HH:mm:ss") & "." & Format(fechaFinFSAL8.Millisecond, "000")
                sArgs(3) = sIdRegistroFSAL1.Value
                sArgs(4) = sPagina.Value
                sArgs(5) = iP.Value
                System.Threading.ThreadPool.QueueUserWorkItem(New System.Threading.WaitCallback(AddressOf CallFSALRegistrarActualizarAcceso), sArgs)
            ElseIf bEnviadoFSAL8Inicial.Value = "1" AndAlso HttpContext.Current.Request.Headers("x-ajax") Is Nothing Then
                Dim sArgs(12) As Object
                sArgs(0) = 1
                sArgs(1) = sProducto.Value
                sArgs(2) = sFechaIniFSAL8.Value
                sArgs(3) = Format(fechaFinFSAL8, "yyyy-MM-dd HH:mm:ss") & "." & Format(fechaFinFSAL8.Millisecond, "000")
                sArgs(4) = sPagina.Value
                sArgs(5) = iPost.Value
                sArgs(6) = iP.Value
                sArgs(7) = sUsuCod.Value
                sArgs(8) = sPaginaOrigen.Value
                sArgs(9) = sNavegador.Value
                sArgs(10) = sIdRegistroFSAL8.Value
                sArgs(11) = sProveCod.Value
                sArgs(12) = sQueryString.Value
                System.Threading.ThreadPool.QueueUserWorkItem(New System.Threading.WaitCallback(AddressOf CallFSALRegistrarActualizarAcceso), sArgs)
            End If
        End If

    End Sub
    ''
    ''Fin codigo FSAL
#End Region
    ''' <summary>
    ''' Strip HTML tags.
    ''' </summary>
    Private Function StripTags(ByVal html As String) As String
        ' Remove HTML tags.
        Return Regex.Replace(html, "<.*?>", "")
    End Function
    ''' <summary>
    ''' Da el Formato Numero a usar en Excel para q las columnas numericas sean reconocidas como numero y no como texto
    ''' </summary>
    ''' <returns>Formato Numero a usar en Excel</returns>
    ''' <remarks>Llamada desde:whdgExcelExporter_GridRecordItemExported ; Tiempo maximo:0</remarks>
    Private Function FormatoNumeroExcel() As String
        'El excel espera "#,###.00", no el formato de usuario. El excel admite lo q admite por el regional setting
        Dim FormatoNum As String = "#,###"
        For i As Integer = 1 To FSNUser.NumberFormat.NumberDecimalDigits
            If i = 1 Then
                FormatoNum = FormatoNum & "."
            End If
            FormatoNum = FormatoNum & "0"
        Next
        Return FormatoNum
    End Function
End Class