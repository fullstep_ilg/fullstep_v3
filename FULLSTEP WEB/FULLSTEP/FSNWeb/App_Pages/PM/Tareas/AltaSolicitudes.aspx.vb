﻿Public Partial Class AltaSolicitudes
    Inherits FSNPage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Solicitudes As FSNServer.Solicitudes
        Dim mMaster = CType(Master, FSNWeb.Menu)
        Dim tipoSolicitud As Integer = 0

        If Request("TipoSolicitud") <> Nothing Then tipoSolicitud = CInt(Request("TipoSolicitud"))
        Select Case tipoSolicitud
            Case TiposDeDatos.TipoDeSolicitud.Factura
                mMaster.Seleccionar("Facturacion", "Alta")
                ModuloIdioma = TiposDeDatos.ModulosIdiomas.Factura
                FSNPageHeader.TituloCabecera = Textos(0)
            Case TiposDeDatos.TipoDeSolicitud.Encuesta
                mMaster.Seleccionar("Calidad", "Encuestas")
                ModuloIdioma = TiposDeDatos.ModulosIdiomas.Solicitudes
                FSNPageHeader.TituloCabecera = Textos(5)
                lblParrafo.Text = Textos(7)
                lblCabeceraAltaSolicitudes.Text = Textos(8)
                lblFooter.Text = Textos(9)
                lblSinDatos.Text = Textos(10)
            Case TiposDeDatos.TipoDeSolicitud.SolicitudQA
                mMaster.Seleccionar("Calidad", "SolicitudesQA")
                ModuloIdioma = TiposDeDatos.ModulosIdiomas.Solicitudes
                FSNPageHeader.TituloCabecera = Textos(6)
                lblParrafo.Text = Textos(1)
                lblCabeceraAltaSolicitudes.Text = Textos(2)
                lblFooter.Text = Textos(3)
                lblSinDatos.Text = Textos(4)
            Case Else
                mMaster.Seleccionar("Procesos", "AltaSolicitudes")
                ModuloIdioma = TiposDeDatos.ModulosIdiomas.Solicitudes
                FSNPageHeader.TituloCabecera = Textos(0)
                lblParrafo.Text = Textos(1)
                lblCabeceraAltaSolicitudes.Text = Textos(2)
                lblFooter.Text = Textos(3)
                lblSinDatos.Text = Textos(4)
        End Select

        If Not ClientScript.IsStartupScriptRegistered("tipoSolicitud") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "tipoSolicitud", "var tipoSolicitud=" & tipoSolicitud & ";", True)



        FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/altaSolicitud.jpg"



        Solicitudes = FSNServer.Get_Object(GetType(FSNServer.Solicitudes))
        If tipoSolicitud = 0 Then
            Solicitudes.LoadData(FSNUser.Cod, Idioma, bNuevoWorkfl:=True, sCodPer:=FSNUser.CodPersona)
        Else
            Solicitudes.LoadData(FSNUser.Cod, Idioma, iTipoSolicitud:=tipoSolicitud, bNuevoWorkfl:=True, sCodPer:=FSNUser.CodPersona)
        End If

        If Solicitudes.Data.Tables.Count > 0 Then
            uwgSolicitudes_WDG.DataSource = Solicitudes.Data
            uwgSolicitudes_WDG.DataBind()
        Else
            uwgSolicitudes_WDG.Visible = False
            tblSolicitudes.Visible = False
            lblSinDatos.Visible = True
        End If
    End Sub
    Private Sub uwgSolicitudes_WDG_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles uwgSolicitudes_WDG.InitializeRow
        Dim Index As Integer
        If CType(e.Row.DataItem, System.Data.DataRowView).Row("ADJUN") > 0 Then
            Index = sender.Columns.FromKey("IMGADJUN").Index
            e.Row.Items.Item(Index).Text = "<img src='" & ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/coment.gif" & "' style='text-align:center;'/>"
        End If

        If CType(e.Row.DataItem, System.Data.DataRowView).Row("FAVORITOS") > 0 Then
            Index = sender.Columns.FromKey("IMGFAV").Index
            e.Row.Items.Item(Index).Text = "<img src='" & ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/favoritos_si.gif" & "' style='text-align:center;'/>"
        End If

        Index = sender.Columns.FromKey("IMGIMPORTAR").Index
        e.Row.Items.Item(Index).Text = "<img src='" & ConfigurationManager.AppSettings("ruta") & "Images/importar.gif" & "' style='text-align:center;'/>"
    End Sub
End Class