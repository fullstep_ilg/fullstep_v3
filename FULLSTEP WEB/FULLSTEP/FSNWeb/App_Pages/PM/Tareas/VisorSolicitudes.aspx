﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" EnableEventValidation="false" MasterPageFile="~/App_Master/Menu.Master" CodeBehind="VisorSolicitudes.aspx.vb" Inherits="Fullstep.FSNWeb.VisorSolicitudes"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        tbody>tr>td.center{text-align : center;}
    </style>
    <!-- Estilos del plugin jquery.tokeninput -->    
    <link rel="stylesheet" href="<%=ResolveClientUrl("~/js/jquery/plugins/styles/token-input.css")%>" type="text/css" />
    <link rel="stylesheet" href="<%=ResolveClientUrl("~/js/jquery/plugins/styles/token-input-facebook.css")%>" type="text/css" />
    <link rel="stylesheet" href="<%=ResolveClientUrl("~/js/jquery/plugins/styles/style.min.css")%>" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">        
    <fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
    <div id="pnlSolicitudesPendientes" style="display:none; position:relative;">
        <div id="pnlSolicitudesPendientes2" style="position:absolute; top:-1.5em; right:1em; text-align:right;">
            <asp:Image ID="imgSolicitudesPendientes" runat="server" SkinID="AlertaPeq" style="vertical-align:middle;"/>
            <asp:Label runat="server" ID="lblSolicitudesPendientes" CssClass="Etiqueta" style="padding:0.5em" ></asp:Label>
            <asp:LinkButton ID="lnkSolicitudesPendientes" runat="server" class="Rotulo" ></asp:LinkButton>
        </div>        
    </div>
    <div class="Texto12 BarraMenuHorizontal" style="clear:both;">
        <div class="SeparadorRight" style="display:inline-block;">
            <div id="EscenariosFavoritos" class="OptionMenuHorizontal" style="display:inline-block;">
                <div style="padding:0.4em;">
                    <asp:Image runat="server" ID="imgEscenariosFavoritos" ImageUrl="~/Images/Favoritos.png" style="max-height:2em; vertical-align:bottom;" />
                    <asp:Label runat="server" ID="lblEscenariosFavoritos" CssClass="Texto12 Negrita"></asp:Label>
                </div>
                <div id="EscenariosListadoFavoritos" class="OptionItemsMenuHorizontal" style="display:none;">                                    
                    <div id="ulEscenariosFavoritos"></div>
                </div>
            </div>
        </div>
        <div style="display:inline-block;">
            <div id="EscenariosCarpetas" class="OptionMenuHorizontal" style="display:inline-block;">
                <div style="padding:0.4em;">
                    <asp:Image runat="server" ID="imgEscenariosCarpetas" ImageUrl="~/Images/Carpetas.png" style="max-height:2em; vertical-align:bottom;" />
                    <asp:Label runat="server" ID="lblEscenariosCarpetas" CssClass="Texto12 Negrita"></asp:Label>
                    <span id="lblEscenariosCarpetaSeleccionado" class="Texto12 Negrita"></span>                    
                </div>
                <div id="EscenariosListadoCarpetas" class="OptionItemsMenuHorizontal" style="display:none;">                
                    <label id="btnEscenariosCarpetaAgregar" class="Texto12 treeTipo10" style="display:block; cursor:pointer; margin-top:0.2em; margin-left:0.5em; padding:0.3em;">
                        <asp:Image runat="server" ID="imgEscenariosCarpetasAgregar" ImageUrl="~/Images/CarpetasAgregar.png" style="vertical-align:middle;" />
                        <asp:Label runat="server" ID="lblEscenariosCarpetasAgregar" CssClass="Texto12"></asp:Label>
                    </label>
                    <div id="ulEscenariosCarpetas" style="margin-top:0.2em;"></div>
                </div>
            </div>            
        </div>
        <div style="display:none; float:right; padding:0em 0.5em;">
            <asp:Label runat="server" ID="lblFiltrarIdentificador"></asp:Label>
            <input id="txtFiltroIdentificador" type="text" style="width:6em; margin-top:0.1em;" />
            <div id="btnBuscarPorIdentificador" class="botonRedondeado" style="float:right; margin-left:0.5em;">
                <asp:Label runat="server" ID="lblBotonBuscarIdentificador"></asp:Label>
            </div>
            <asp:Button runat="server" ID="btnBuscarSolicitud" style="display:none;" />            
        </div>
    </div>
    <div id="cabeceraEscenarios" class="CabeceraBotones Texto12" style="clear:both; height:1.5em; padding:0.2em 0.3em; cursor:pointer;">
		<div id="cabeceraEscenariosNombre" style="display:inline-block; width:10em">
			<asp:Image runat="server" ID="imgEscenariosContraerBarra" SkinID="ContraerColor" style="display:none;" />
			<asp:Image runat="server" ID="imgEscenariosExpandirBarra" SkinID="ExpandirColor" />
			<asp:Label runat="server" ID="lblEscenariosTituloBarra" CssClass="Rotulo" style="width:20em; line-height:1.5em;"></asp:Label>
			<asp:Image runat="server" ID="imgEscenariosOrder" CssClass="Image16" ImageUrl="~/Images/sort.png" style="cursor:pointer;" />
		</div>
        <asp:Label runat="server" ID="lblEscenariosTituloSeleccionado" CssClass="Texto12 TextoAlternativo Negrita" style="line-height:1.5em; margin-right:0.5em;"></asp:Label>
        <span id="lblEscenariosSeleccionado" class="Texto12"></span>
        <asp:CheckBox runat="server" ID="chkEscenariosDefecto" CssClass="TextoAlternativo" style="float:right; cursor:pointer; margin-right:2em;" />
        <span id="btnEscenarioNuevo" class="ui-icon ui-icon-document optionBarraDesplegable" style="display:none;"></span>
        <span id="btnEscenarioEditar" class="ui-icon ui-icon-pencil optionBarraDesplegable" style="display:none;"></span>
        <span id="btnEscenarioEliminar" class="ui-icon ui-icon-trash optionBarraDesplegable" style="display:none;"></span>
        <span id="btnEscenarioCompartir" class="ui-icon ui-icon-newwin optionBarraDesplegable" style="display:none"></span>        
    </div>
    <div id="panelEscenarios" class="Texto12" style="display:none; max-height:10em; overflow-y:auto;"></div>
    <div id="cabeceraEscenarioFiltros" class="CabeceraBotones Texto12" style="clear:both; height:1.5em; padding:0.2em 0.3em; cursor:pointer;">
		<div id="cabeceraFiltrosNombre" style="display:inline-block; width:10em">
			<asp:Image runat="server" ID="imgEscenarioFiltrosContraerBarra" SkinID="ContraerColor" style="display:none;" />
			<asp:Image runat="server" ID="imgEscenarioFiltrosExpandirBarra" SkinID="ExpandirColor" />
			<asp:Label runat="server" ID="lblEscenarioFiltrosTituloBarra" CssClass="Rotulo" style="line-height:1.5em;"></asp:Label>
			<asp:Image runat="server" ID="imgEscenarioFiltrosOrder" CssClass="Image16" ImageUrl="~/Images/sort.png" style="cursor:pointer;" />
		</div>
        <asp:Label runat="server" ID="lblEscenarioFiltrosTituloSeleccionado" CssClass="Texto12 TextoAlternativo Negrita" style="line-height:1.5em; margin-right:0.5em;"></asp:Label>
        <asp:Image runat="server" ID="imgEscenarioFiltrosFormula" ImageUrl="~/Images/Formula.png" class="botonFormula" style="border:0.1em;"/>
        <span id="lblEscenarioFiltrosSeleccionado" class="Texto12"></span>
        <asp:CheckBox runat="server" ID="chkEscenarioFiltrosDefecto" CssClass="TextoAlternativo" style="float:right; cursor:pointer; margin-right:2em;" />
        <span id="btnEscenarioFiltroNuevo" class="ui-icon ui-icon-document optionBarraDesplegable" style="display:none;"></span>
        <span id="btnEscenarioFiltroEditar" class="ui-icon ui-icon-pencil optionBarraDesplegable" style="display:none;"></span>        
        <span id="btnEscenarioFiltroEliminar" class="ui-icon ui-icon-trash optionBarraDesplegable" style="display:none;"></span>            
        <span id="btnEscenarioFiltroGuardar" class="ui-icon ui-icon-disk optionBarraDesplegable" style="display:none;"></span>
        <asp:Image runat="server" ID="imgGuardadoFiltro" CssClass="imagenCabecera Image16 optionBarraDesplegable" style="display:none; margin:0.3em 0em 0em 0.3em;" />
        <div id="divFormulaFiltro" class="popupCN" style="position:absolute; display:none; padding:0.5em;"></div>
    </div>
    <div id="panelEscenarioFiltros" class="Texto12" style="display:none; max-height:10em; overflow-y:auto;"></div>
    <div id="cabeceraEscenarioFiltroOpciones" class="CabeceraBotones" style="height:0.5em; text-align:center; cursor:pointer;">
        <asp:Image runat="server" ID="imgEscenarioFiltroOpcionesCollapse" SkinID="Collapse"  style="display:none;" />
        <asp:Image runat="server" ID="imgEscenarioFiltroOpcionesExpand" SkinID="Expand" />
    </div>
    <div id="panelEscenarioFiltrosOpcionesFiltrado" class="Texto12 Rectangulo" style="display:none; padding:0.2em 0.3em; overflow:auto; max-height:10em;"></div> 
    <div id="panelEscenarioVistas" style="margin-top:0.5em;">
        <asp:Image runat="server" ID="imgEscenarioVistasOrder" CssClass="Image16" ImageUrl="~/Images/sort.png" style="cursor:pointer;" />
        <div id="btnBuscarSolicitudes" class="botonRedondeado" style="display:none; float:right; margin-right:0.5em;">
            <asp:Label runat="server" ID="lblBotonBuscar"></asp:Label>
        </div>
    </div>
    <asp:Button runat="server" ID="btnRecargarGridSolicitudes" style="display:none;" />        
    <asp:Button runat="server" ID="btnPager" style="display:none;" />
    <asp:Button runat="server" ID="btnOrder" style="display:none;" />
    <asp:Button runat="server" ID="btnColumnMove" style="display:none;" />
    <ig:WebExcelExporter ID="whdgExcelExporter" runat="server"></ig:WebExcelExporter>
    <asp:UpdatePanel runat="server" ID="updSolicitudes" UpdateMode="Conditional" style="overflow-y:auto;">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnRecargarGridSolicitudes" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnPager" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnOrder" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnBuscarSolicitud" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnColumnMove" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:HiddenField runat="server" ID="iNumeroSolicitudesFiltroSeleccionado" />
            <asp:HiddenField runat="server" ID="sProducto" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="sFechaIniFSAL8" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="sPagina" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="iPost" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="iP" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="sUsuCod" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="sPaginaOrigen" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="sNavegador" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="sIdRegistroFSAL8" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="sProveCod" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="sQueryString" ClientIDMode="Static" />
            <ig:WebHierarchicalDataGrid runat="server" ID="whdgExportacion" 
				Visible="false" AutoGenerateBands="false"
				AutoGenerateColumns="false" Width="100%" EnableAjax="true" 
				EnableAjaxViewState="false" EnableDataViewState="false">
                <ClientEvents Initialize="whdgExportacion_Initialize"/>
                <Bands>
                    <ig:Band Key="DESGLOSE" DataMember="DESGLOSE" DataKeyFields="FORM_INSTANCIA" AutoGenerateColumns="false">
                    </ig:Band>
                </Bands>
			</ig:WebHierarchicalDataGrid>
            <ig:WebHierarchicalDataGrid runat="server" ID="whdgSolicitudes" Visible="false"
                AutoGenerateBands="false" AutoGenerateColumns="false" 
                EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="false" Width="100%"
                InitialDataBindDepth="-1" InitialExpandDepth="-1">
                <ExpandCollapseAnimation SlideOpenDirection="Auto" SlideOpenDuration="300" SlideCloseDirection="Auto" SlideCloseDuration="300" />                
                <GroupingSettings EnableColumnGrouping="True" GroupAreaVisibility="Visible" />
                <ClientEvents Click="whdgSolicitudes_CellClick" Initialize="whdgSolicitudes_Initialize"/>
                <Bands>
                    <ig:Band Key="DESGLOSE" DataMember="DESGLOSE" DataKeyFields="FORM_INSTANCIA,DESGLOSE,LINEA" AutoGenerateColumns="false">
                        <Behaviors>
                            <ig:Filtering Enabled="false"></ig:Filtering>
                            <ig:Sorting Enabled="false"></ig:Sorting>
                            <ig:ColumnResizing Enabled="true">
                                <ColumnResizingClientEvents ColumnResized="whdgSolicitudes_DesgloseColumnResized" />
                            </ig:ColumnResizing>
                            <ig:ColumnMoving Enabled="true">
                                <ColumnMovingClientEvents HeaderDropped="whdgSolicitudes_DesgloseHeaderDropped" />
                            </ig:ColumnMoving>
                        </Behaviors>
                    </ig:Band>
                </Bands>
                <Behaviors>
                    <ig:Activation Enabled="true"/>
                    <ig:ColumnResizing Enabled="true">
                        <ColumnResizingClientEvents ColumnResized="whdgSolicitudes_ColumnResized" />
                    </ig:ColumnResizing>
                    <ig:Sorting Enabled="true" SortingMode="Multi">
                        <SortingClientEvents ColumnSorting="whdgSolicitudes_Sorting" ColumnSorted="whdgSolicitudes_Sorted" />
                    </ig:Sorting>
                    <ig:ColumnMoving Enabled="true">
                        <ColumnMovingClientEvents HeaderDropped="whdgSolicitudes_HeaderDropped" />
                    </ig:ColumnMoving>
                    <ig:Paging Enabled="true" PagerAppearance="Top">                            
                        <PagerTemplate>
                            <div class="CabeceraBotones" style="text-align:left;">                                            
                                <div style="display:inline-block; width:40%; margin:0.5em 0em; vertical-align:middle;">
                                    <asp:Image ID="ImgBtnFirst" runat="server" OnClientClick="FirstPage()" style="margin-left:2em; vertical-align:bottom;" />
                                    <asp:Image ID="ImgBtnPrev" runat="server" OnClientClick="PrevPage()" style="margin-left:0.4em; vertical-align:bottom;" />
                                    <asp:Label ID="lblPage" runat="server" Style="margin-left:0.6em;"></asp:Label>
                                    <asp:DropDownList ID="PagerPageList" runat="server" Style="margin-left:0.4em;" onchange="return IndexChanged();"></asp:DropDownList>
                                    <asp:Label ID="lblOF" runat="server" Style="margin-left:0.6em;"></asp:Label>
                                    <asp:Label ID="lblCount" runat="server" Style="margin-left:0.5em;"></asp:Label>
                                    <asp:Image ID="ImgBtnNext" runat="server" OnClientClick="NextPage()" style="margin-left:0.6em; vertical-align:bottom;" />
                                    <asp:Image ID="ImgBtnLast" runat="server" OnClientClick="LastPage()" style="margin-left:0.4em; vertical-align:bottom;" />
                                    <div id="OptionInfoPedidosProcesos" style="float:right;">
                                        <asp:CheckBox runat="server" ID="chkInfoPedidosProcesos" />
                                    </div>
                                </div>
                                <div style="display:inline-block; width:60%; vertical-align:middle;">
                                    <div onclick="ExportarExcel()" class="botonDerechaCabecera" style="float:right;">            
                                        <asp:Image runat="server" ID="imgExcel" CssClass="imagenCabecera" />
                                        <asp:Label runat="server" ID="lblExcel" CssClass="fntLogin2" style="line-height:2em; margin-left:3px;"></asp:Label>           
                                    </div>
                                    <div id="btnEscenarioVistaNuevo" onclick="NuevaVista()" class="optionBarraDesplegable" style="padding:0.2em; display:none;">
                                        <span class="ui-icon ui-icon-document"></span>
                                    </div>
                                    <div id="btnEscenarioVistaEditar" onclick="EditarVista()" class="optionBarraDesplegable" style="padding:0.2em; display:none;">            
                                        <span class="ui-icon ui-icon-pencil"></span>  
                                    </div>
                                    <div id="btnEscenarioVistaEliminar" onclick="EliminarVista()" class="optionBarraDesplegable" style="padding:0.2em; display:none;">            
                                        <span class="ui-icon ui-icon-trash"></span>
                                    </div>                                    
                                    <div id="btnEscenarioVistaGuardar" onclick="GuardarConfiguracion()" class="optionBarraDesplegable" style="padding:0.2em; display:none;"> 
                                        <span class="ui-icon ui-icon-disk"></span>                                       
                                    </div>                                    
                                    <div style="float:right; width:2.5em; height:0.5em;">
                                        <asp:Image runat="server" ID="imgGuardandoVista" CssClass="imagenCabecera Image20" style="display:none; margin:0.3em 0.2em 0em 0em;" />
                                        <asp:Image runat="server" ID="imgGuardadoVista" CssClass="imagenCabecera Image16" style="display:none; margin:0.6em 0em 0em 0.8em;" />
                                    </div>   
                                    <label id="lblOptionVistaDefectoFiltro" for="chkVistaDefectoFiltro" class="optionBarraDesplegable" style="padding:0.2em; float:left;">
                                        <input type="checkbox" id="chkVistaDefectoFiltro" name="chkVistaDefectoFiltro" />
                                        <asp:Label runat="server" ID="lblVistaDefectoFiltro"></asp:Label>
                                    </label>                                 
                                </div>
                            </div>
                        </PagerTemplate>
                    </ig:Paging>
                </Behaviors>
            </ig:WebHierarchicalDataGrid>            
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="CarpetaVaciaEscenarios" style="display:none; text-align:center;">
        <span id="CarpetaVacia"></span>
    </div>
    <div id="divCargandoSolicitudes" style="display:none; text-align:center;">
        <asp:Image runat="server" ImageUrl="~/Images/cargando.gif" />
    </div>
    <div id="wizardEscenario" class="popupCN" style="display:none; position:absolute; z-index:1002; padding:0% 1%; width:65%; height:90%;">
        <div id="btnWizardEscenarioCerrarPopUp" class="CerrarPopUp" style="float:right; width:30px; height:30px;"></div>
        <div style="height:15%;">
            <asp:Image runat="server" ID="imgWizardEscenario" ImageUrl="~/Images/escenario.png" style="float:left; vertical-align:bottom; margin-top:-1em;" />
            <div class="PopUp_Header_Background" style="height:1.5em;">
                <span id="lblWizardEscenarioTitulo" class="Texto16 TextoClaro Negrita"></span>
            </div>
            <div style="clear:both; float:left; height:1.5em;">
                <input id="chkWizardEscenarioDefecto" type="checkbox" style="vertical-align:middle; cursor:pointer;" />
                <label id="lblWizardEscenarioDefecto" for="chkWizardEscenarioDefecto" style="cursor:pointer;"></label>
            </div>
            <div style="float:right; height:1.5em;">			
                <span id="lblWizardEscenarioFavorito" class="Texto12 TextoOscuro" style="cursor:pointer;"></span>
                <asp:Image runat="server" ID="imgWizardEscenarioNoFavorito" ImageUrl="~/Images/starDesSelect.png" style="width:1.5em; vertical-align:bottom; cursor:pointer;" />
                <asp:Image runat="server" ID="imgWizardEscenarioFavorito" ImageUrl="~/Images/starSelect.png" style="display:none; width:1.5em; vertical-align:bottom; cursor:pointer;" />
                <asp:Image runat="server" ID="imgWizardEscenarioNoFavoritoNoEdit" ImageUrl="~/Images/starDesSelect.png" style="display:none; width:1.5em; vertical-align:bottom;" />
                <asp:Image runat="server" ID="imgWizardEscenarioFavoritoNoEdit" ImageUrl="~/Images/starSelect.png" style="display:none; width:1.5em; vertical-align:bottom;" />
            </div>
            <div style="clear:both; padding:0.5em 0em; height:2em;">
                <span id="lblWizardEscenarioNombreEscenario" class="Texto14 TextoAlternativo Negrita"></span>
                <input id="txtWizardEscenarioNombre" type="text" style="width:25em;" />
                <span id="lblWizardEscenarioNombre" class="Texto12" style="display:none;"></span>
            </div>
        </div>
        <fieldset class="Resaltar_BordeRedondeado" style="height:72%; padding-top:10px; padding-bottom:20px;">
            <legend class="Info_BordeRedondeado" style="clear:both; float:right; margin-top:-20px; margin-right:3em; padding:0.3em 1em;">
                <span id="lblWizardEscenarioStepInfo"></span>
                <span id="lblWizardEscenarioStepNumber" style="margin:0em 0.1em;"></span>
                <span id="lblWizardEscenarioStepOf"></span>
                <span id="lblWizardEscenarioStepCount"></span>
            </legend>
            <div id="wizardEscenarioStep1" style="clear:both; height:95%; margin-top:1%; margin-left:0.5%; overflow:auto;">
                <div>
                    <span id="lblWizarEscenarioTipoSolicitud" class="Texto12"></span>
                </div>
                <div style="margin-top:0.5em; margin-left:1em;">
                    <input type="radio" id="rWizardEscenarioTodasSolicitudes" name="tiposolicitud" value="0" style="vertical-align:middle; cursor:pointer;" />
                    <label for="rWizardEscenarioTodasSolicitudes" id="lblWizardEscenarioTodasSolicitudes" style="cursor:pointer;"></label>
                </div>
                <div style="margin-top:0.5em; margin-left:1em;">                    
                    <input type="radio" id="rWizardEscenarioSolicitud" name="tiposolicitud" value="1" style="vertical-align:middle; cursor:pointer;" />
                    <label for="rWizardEscenarioSolicitud" id="lblWizardEscenarioSolicitudes" style="cursor:pointer;"></label>
                    <div style="display:inline-block; vertical-align:middle;">
                        <select id="ddWizardEscenarioTipoSolicitud" style="min-width:15em;" multiple="multiple" disabled="disabled"></select>
                    </div>
                </div>
                <div style="margin-top:2em;">
                    <span id="lblWizardEscenarioCarpeta" class="Texto12"></span>
                </div>
                <div style="margin-top:0.5em; margin-left:1em;">
                    <span id="lblWizardEscenarioCarpetaSeleccionada" class="Texto12 TextoOscuro"></span>
                    <div style="display:inline-block; vertical-align:middle;">
                        <select id="ddWizardEscenarioCarpetaEscenario" style="min-width:15em;"></select>
                    </div>
                </div>
            </div>
            <div id="wizardEscenarioStep2" style="display:none; clear:both; height:95%; margin-top:1%; margin-left:0.5%;">
                <div style="padding:0em 0.5em; margin-bottom:0.5em;">
                    <input id="chkWizarEscenarioFiltroDefecto" type="checkbox" style="vertical-align:middle; cursor:pointer;" />
                    <label id="lblWizardEscenarioFiltroDefecto" for="chkWizarEscenarioFiltroDefecto" style="cursor:pointer;"></label>
                </div>
                <div style="padding:0em 0.5em;">
                    <span id="lblWizardEscenarioTituloFiltro" class="Texto12"></span>
                </div>
                <div style="padding:0em 0.5em; margin-top:0.5em;">
                    <span id="lblWizardEscenarioFiltroNombre" class="Texto12"></span>
                    <input id="txtWizardEscenarioFiltroNombre" type="text" style="width:30em;" />
                </div>
                <div style="padding:0.5em; margin-top:0.5em;">
                    <span id="lblWizardEscenarioFiltroSeleccionCampos" class="Texto12"></span>
                </div>                
                <div style="height:80%; overflow:auto;">                
                    <fieldset style="display:inline-block; width:45%; height:90%; margin-top:0.5em; overflow:hidden;">
                        <legend>
                            <span id="lblWizardEscenarioFiltroCamposGenerales" class="Texto12"></span>
                        </legend>                        
                        <div class="escenarioCamposFiltroCabecera">
                            <span id="lblWizardEscenarioFiltroCamposGeneralesSeleccion" class="escenarioCamposFiltroCabeceraColumna" style="width:3em;">_Sel.</span>
                            <span id="lblWizardEscenarioFiltroCamposGeneralesVisible" class="escenarioCamposFiltroCabeceraColumna" style="width:4em;">_Visible</span>
                        </div>
                        <ul id="lCamposGenerales" style="height:90%; margin:0.5em; overflow:auto; list-style:none; padding:0em;"></ul>                        
                    </fieldset>
                    <fieldset id="fsWizardEscenarioCamposFormulario" style="display:inline-block; width:45%; height:90%; vertical-align:top; margin-top:0.5em; overflow:hidden;">
                        <legend>
                            <span id="lblWizardEscenarioFiltroCamposFormulario" class="Texto12"></span>
                        </legend>
                        <div class="escenarioCamposFiltroCabecera">
                            <span id="lblWizardEscenarioFiltroCamposFormularioSeleccion" class="escenarioCamposFiltroCabeceraColumna" style="width:3em;">_Sel.</span>
                            <span id="lblWizardEscenarioFiltroCamposFormularioVisible" class="escenarioCamposFiltroCabeceraColumna" style="width:4em;">_Visible</span>
                        </div>
                        <ul id="lCamposFormulario" style="height:90%; overflow:auto; list-style:none; padding:0em;"></ul>
                    </fieldset>
                </div>
            </div>
            <div id="wizardEscenarioStep3" style="display:none; clear:both; height:95%; margin-top:1%; margin-left:0.5%; overflow:auto;">
                <div id="basicWizardEscenarioStep3" style="height:100%;">
                    <div id="basicChangeWizardEscenarioStep3" style="display:inline-block; cursor:pointer; margin-top:0.5%; padding:0% 0.5%;">
                        <span class="ui-icon ui-icon-gear" style="display:inline-block; vertical-align:bottom;"></span>
                        <span id="btnWizardFiltroConfigAvanzada" class="Texto12 Negrita Subrayado TextoAlternativo"></span>
                    </div>
                    <div style="margin-top:1em; padding:0em 0.5em;">
                        <span id="lblWizardEscenarioFiltroTituloBasico" class="Texto12"></span>
                    </div>
                    <div id="lWizardEscenarioListaCamposFiltroBasico" class="SeparadorSup" style="height:85%; margin-top:0.5%; overflow:auto;"></div>				    
                </div>
                <div id="advancedWizardEscenarioStep3" style="display:none; height:100%;">
                    <div id="advancedChangeWizardEscenarioStep3" style="display:inline-block; cursor:pointer; margin-top:0.5em; padding:0em 0.5em;">
                        <span class="ui-icon ui-icon-gear" style="display:inline-block; vertical-align:bottom;"></span>
                        <span id="btnWizardFiltroConfigBasica" class="Texto12 Negrita Subrayado TextoAlternativo"></span>
                    </div>
                    <div style="margin-top:1em; padding:0em 0.5em;">
                        <span id="lblWizarEscenarioInfoFiltro" class="Texto12"></span>
                    </div>					
                    <div style="padding:0em 0.5em; margin-top:0.2em;">                        
                        <div id="divWizardEscenarioFormula" class="CajaTexto" style="padding:0.2em; min-height:2em;"></div>
                    </div>
                    <div style="padding:0.3em; margin-top:0.2em;">
                        <span id="btnWizardEscenarioOperadorY" class="CabeceraBotones operador_Formula_Escenario">Y</span>
                        <span id="btnWizardEscenarioOperadorO" class="CabeceraBotones operador_Formula_Escenario">O</span>
                        <span id="btnWizardEscenarioOperadorAbrir" class="CabeceraBotones operador_Formula_Escenario">(</span>
                        <span id="btnWizardEscenarioOperadorCerrar" class="CabeceraBotones operador_Formula_Escenario">)</span>					    					    
                        <span id="btnWizardEscenarioLimpiarFormula" class="botonRedondeado"></span>
                    </div>
                    <div id="lWizardEscenarioListaCamposFiltroAvanzado" class="SeparadorSup" style="height:75%; margin-top:0.5em; overflow:auto;"></div>               
                </div>
            </div>
            <div id="wizardEscenarioStep4" style="display:none; clear:both; height:100%; margin-top:1%; margin-left:0.5%; overflow:auto;">
                <div style="margin-bottom:0.5em;">
                    <input id="chkWizarEscenarioVistaDefecto" type="checkbox" style="vertical-align:middle; cursor:pointer;" />
                    <label id="lblWizardEscenarioVistaDefecto" for="chkWizarEscenarioVistaDefecto" style="cursor:pointer;"></label>
                </div>
                <div style="clear:both;">
                    <span id="lblWizardEscenarioTituloVista" class="Texto12"></span>
                </div>
                <div style="padding:0.5em; margin-top:0.5em;">
                    <span id="lblWizardEscenarioVistaNombre" class="Texto12"></span>
                    <input id="txtWizardEscenarioVistaNombre" type="text" style="width:20em;" />
                </div>
                <div style="margin-top:1em;">
                    <span id="lblWizarEscenarioInfoVista" class="Texto12"></span>
                </div>                
                <fieldset style="display:inline-block; width:46%; height:60%; vertical-align:top; margin-top:0.5em; overflow:hidden;">
                    <legend>
                        <span id="lblWizardEscenarioVistaCamposGenerales" class="Texto12"></span>
                    </legend>
                    <ul id="lCamposGeneralesVista" style="height:95%; margin:0.5em; overflow:auto; list-style:none; padding:0em;"></ul>
                </fieldset>
                <fieldset id="fsWizardEscenarioVistaCamposFormulario" style="display:inline-block; width:46%; height:60%; vertical-align:top; margin-top:0.5em; overflow:hidden;">
                    <legend>
                        <span id="lblWizardEscenarioVistaCamposFormulario" class="Texto12"></span>
                    </legend>
                    <ul id="lCamposFormularioVista" style="height:95%; margin:0.5em; overflow:auto; list-style:none; padding:0em;"></ul>
                </fieldset>
                <asp:Button runat="server" ID="btnRecargarConfigVista" style="display:none;" />
                <div class="Rectangulo" style="margin-top:0.5em;">
                    <asp:UpdatePanel runat="server" ID="updConfiguracionVista" UpdateMode="Conditional" style="overflow-x:scroll;">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnRecargarConfigVista" EventName="Click" />
                        </Triggers>
                        <ContentTemplate>
                            <ig:WebHierarchicalDataGrid runat="server" ID="whdgConfigVista"
                                AutoGenerateBands="false" AutoGenerateColumns="false" 
                                EnableAjax="true" EnableAjaxViewState="false"
                                EnableDataViewState="false" Width="100%"
                                InitialDataBindDepth="-1" InitialExpandDepth="-1">
                                <ExpandCollapseAnimation SlideOpenDirection="Auto" SlideOpenDuration="300" SlideCloseDirection="Auto" SlideCloseDuration="300" />
                                <Bands>
                                    <ig:Band Key="DESGLOSE" DataMember="DESGLOSE" DataKeyFields="FORM_INSTANCIA" AutoGenerateColumns="false">
                                        <Behaviors>
                                            <ig:Filtering Enabled="false"></ig:Filtering>
                                            <ig:Sorting Enabled="false"></ig:Sorting>
                                            <ig:ColumnResizing Enabled="true">
                                                <ColumnResizingClientEvents ColumnResized="whdgConfigVista_DesgloseColumnResized" />
                                            </ig:ColumnResizing>
                                            <ig:ColumnMoving Enabled="true">
                                                <ColumnMovingClientEvents HeaderDropped="whdgConfigVista_DesgloseHeaderDropped" />
                                            </ig:ColumnMoving>
                                        </Behaviors>
                                        <Columns>
                                            <ig:BoundDataField DataFieldName="FORM_INSTANCIA" Key="FORM_INSTANCIA" Hidden="true"></ig:BoundDataField>
                                        </Columns>
                                    </ig:Band>
                                </Bands>
                                <Behaviors>                                                        
                                    <ig:Activation Enabled="false"/>
                                    <ig:Filtering Alignment="Top" Enabled="false"></ig:Filtering>
                                    <ig:RowSelectors Enabled="false"></ig:RowSelectors>
                                    <ig:Selection Enabled="false"></ig:Selection>
                                    <ig:Sorting Enabled="true" SortingMode="Multi">
                                        <SortingClientEvents ColumnSorted="whdgConfigVista_ColumnSorted" />
                                    </ig:Sorting>
                                    <ig:ColumnResizing Enabled="true" >
                                        <ColumnResizingClientEvents ColumnResized="whdgConfigVista_ColumnResized" />
                                    </ig:ColumnResizing>
                                    <ig:ColumnMoving Enabled="true">
                                        <ColumnMovingClientEvents HeaderDropped="whdgConfigVista_HeaderDropped" />
                                    </ig:ColumnMoving>								
                                    <ig:Paging Enabled="false"></ig:Paging>						
                                </Behaviors>
                            </ig:WebHierarchicalDataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>	
                    <div id="divCargandoConfiguracionVista" style="display:none; text-align:center;">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/cargando.gif" />
                    </div>				
                </div>
            </div>
            <div id="wizardEscenarioStep5" style="display:none; clear:both; height:100%; margin-top:1%; margin-left:0.5%; overflow:auto;">
                
            </div>
        </fieldset>        
        <div style="text-align:right; height:3em;">
            <div id="btnWizardEscenarioStepDown" class="CabeceraBotones Texto12" style="visibility:hidden; display:inline-block; cursor:pointer; width:7em; margin-top:0.5em; padding:0.5em;">
                <asp:Image runat="server" ID="imgWizardEscenarioAceptar" ImageUrl="~/Images/contraer_horiz_color.gif" />
                <span id="lblWizardEscenario" style="clear:both; display:inline-block; margin-left:0.5em;"></span>            
            </div>
            <div id="btnWizardEscenarioCancelar" class="CabeceraBotones Texto12" style="display:inline-block; cursor:pointer; width:7em; text-align:center; margin-top:0.5em; padding:0.5em;">
                <span style="clear:both; display:inline-block; margin-right:0.5em;"></span>
            </div>
            <div id="btnWizardEscenarioStepUp" class="CabeceraBotones Texto12" style="display:inline-block; cursor:pointer; width:7em; margin-top:0.5em; padding:0.5em;">
                <span style="clear:both; display:inline-block; margin-right:0.5em;"></span>
                <asp:Image runat="server" ID="imgWizardEscenarioSiguiente" ImageUrl="~/Images/expandir_horiz_color.gif" />
            </div>
            <div id="btnWizardEscenarioEnd" class="CabeceraBotones Texto12" style="display:none; cursor:pointer; width:7em; text-align:center; margin-top:0.5em; padding:0.5em;">
                <span style="clear:both; display:inline-block; margin-right:0.5em;"></span>
            </div>
        </div>
    </div>
    <div id="panelDetalleTextoLargo" class="popupCN" style="display:none; width:40em; position:absolute; z-index:1002;">
        <div class="PopUp_Header_Background" style="margin:0em; padding:0.5em;">
            <span id="lblCampoInfo" class="Texto16 TextoClaro Negrita" style="margin-left:0.5em;"></span>
        </div>
        <div class="bordes" style="height:22em; margin:1.5em; padding:0.2em; overflow-y:auto; max-height:30em;">
            <span id="lblCampoValor" class="Texto12"></span>
        </div>
        <div style="height:2em; text-align:center;">
            <span id="btnCerrarInfoTextoLargo" class="botonRedondeado"></span>
        </div>
    </div>
    <div id="panelOrder" class="popupCN" style="display:none; width:40em; height:40em; position:absolute; z-index:1002;">
        <div class="PopUp_Header_Background" style="margin:0em; padding:0.5em;">
            <span id="lblCabeceraPanelOrder" class="Texto16 TextoClaro Negrita" style="margin-left:0.5em;"></span>
        </div>
        <ul id="listaOrder" style="list-style:none; height:33em; overflow-y:auto; margin:0.5em; padding:0em;"></ul>
        <div style="height:2em; text-align:center; margin-top:1em;">
            <span id="btnCerrarPanelOrder" class="botonRedondeado"></span>
        </div>
    </div>
    <div id="panelCompartir" class="popupCN" style="display:none; padding: 0em 1em 1em 1em; z-index:1002; min-width:40em;">
        <div id="pnlCompartirHeader">
            <div id="btnPanelCompartirCerrarPopUp" class="CerrarPopUp" style="float:right; width:30px; height:30px;"></div>        
            <asp:Image runat="server" ID="imgPanelCompartir" ImageUrl="~/Images/escenario.png" style="float:left; vertical-align:bottom; margin-top:-1em;" />
            <div class="PopUp_Header_Background" style="height:1.5em;">
                <span id="lblPanelCompartirTitulo" class="Texto16 TextoClaro Negrita"></span>
            </div>            
        </div>
        <div style="clear:both; padding:0.5em 0em 0em 0em; height:2em;">
            <span id="lblPanelCompartirLabelDenominacion" class="Texto14 TextoAlternativo Negrita"></span>                
            <span id="lblPanelCompartirNombreEscenario" class="Texto12"></span>
        </div>
        <fieldset class="Resaltar_BordeRedondeado" style="height:72%; padding-top:10px; padding-bottom:20px;">
            <div id="lblPanelCompartirTituloDetalle" class="Texto12 CompartirUSU" style="margin-bottom:1em"></div>
            <div class="CompartirUSU" style="margin:0.4em 0.1em;">
                <span id="lblPanelCompartirUONDep" style="vertical-align: middle; display:inline-block; width:15em;" class="Texto12"></span>
                <input type="text" id="txtPanelCompartirUONDep" />            
                <asp:Image ID="imgBuscadorUONsCompartir" runat="server" ImageUrl="~/Images/Buscador.png" class='Image20' style='vertical-align: middle; cursor:pointer;' />
                <div id="tvUONsCompartir" class="Bordear" style="display:none; height:30em; overflow:auto; margin-top:1em;"></div>    
            </div>
            <div class="CompartirUSU" style="margin:0.4em 0.1em 2em 0.1em;">
                <span id="lblPanelCompartirLabelUsuarios" style="vertical-align: middle; display:inline-block; width:15em;" class="Texto12"></span>
                <input type="text" id="txtPanelCompartirUsuarios" />            
                <asp:Image runat="server" ImageUrl="~/Images/Buscador.png" class='Image20' style='vertical-align: middle; cursor:pointer;' onclick='BuscarUsuariosCompartir(this);' />
            </div>
            <div id="pnlPanelCompartirProveedores" style="margin:0.4em 0.1em;">
                <input type="checkbox" id="chkPanelCompartirProveedores" style="vertical-align:middle;" /> 
                <span id="lblPanelCompartirProveedores" style="vertical-align: middle; display:inline-block; width:15em;" class="Texto12"></span>                               
            </div>
        </fieldset>        
        <div style="text-align:right; height:3em;">           
            <div id="btnPanelCompartirCancelar" class="CabeceraBotones Texto12" style="display:inline-block; cursor:pointer; width:7em; text-align:center; margin-top:0.5em; padding:0.5em;">
                <span style="clear:both; display:inline-block; margin-right:0.5em;"></span>
            </div>            
            <div id="btnPanelCompartirFinalizar" class="CabeceraBotones Texto12" style="display:inline-block; cursor:pointer; width:7em; text-align:center; margin-top:0.5em; padding:0.5em;">
                <span style="clear:both; display:inline-block; margin-right:0.5em;"></span>
            </div>
        </div>
    </div>
    <fsn:FSNPanelInfo ID="FSNPanelDatosUsuario" runat="server" ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_DatosPersona" TipoDetalle="3"/>
    <fsn:FSNPanelInfo ID="FSNPanelDatosPersona" runat="server" ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_DatosPersona" TipoDetalle="2"/>
    <fsn:FSNPanelInfo ID="FSNPanelDatosProveedor" runat="server" ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_DatosProveedor" TipoDetalle="1" />
    <div id="pnlFondoCargando" class="FondoModal" style="position:absolute; z-index:1001; margin:auto; width:100%; top:0; left:0;"></div>
    <div id="pnlCargando" class="updateProgress" style="z-index:1002;">
		<div style="position:relative; top:30%; text-align:center;">
			<asp:Image ID="ImgProgress" runat="server" SkinID="ImgProgress" />
			<asp:Label ID="lblCargandoDatos" runat="server" ForeColor="Black" Text=""></asp:Label>
		</div>
	</div>

    <script type="text/javascript">
        function strToNum(strNum) {
            if (strNum == undefined || strNum == null || strNum == '')
                strNum = '0'

            while (strNum.indexOf(UsuNumberGroupSeparator) >= 0) {
                strNum = strNum.replace(UsuNumberGroupSeparator, "")
            }
            strNum = strNum.replace(UsuNumberDecimalSeparator, ".")
            return parseFloat(strNum)
        }

        function formatNumber(num) {
            if (num == undefined || num == null || num == '')
                num = 0
            num = parseFloat(num)
            var result = num.toFixed(UsuNumberNumDecimals);
            result = addSeparadorMiles(result.toString())
            var result1 = result.substring(0, result.length - parseInt(UsuNumberNumDecimals) - 1)
            var result2 = result.substring(result.length - parseInt(UsuNumberNumDecimals), result.length)
            result = result1 + UsuNumberDecimalSeparator + result2
            return result
        }

        function addSeparadorMiles(nStr) {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + UsuNumberGroupSeparator + '$2');
            }
            var retorno = x1 + x2;
            return retorno
        }
    </script>

    <asp:HiddenField runat="server" ID="bActivadoFSAL" ClientIDMode="Static" Value="0" />
    <asp:HiddenField runat="server" ID="bEnviadoFSAL8Inicial" ClientIDMode="Static" Value="0" />
    <asp:HiddenField runat="server" ID="sIdRegistroFSAL1" ClientIDMode="Static" />

</asp:Content>
