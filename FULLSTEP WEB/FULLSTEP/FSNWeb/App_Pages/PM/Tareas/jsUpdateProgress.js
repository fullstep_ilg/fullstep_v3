﻿Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginReq);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endReq);
function beginReq(sender, args) {
    // shows the Popup
    var Emisor;
    var Comparacion;
    var Mostrar = 'si';
    
    if (String(args._postBackElement.id).search("cblCamposGenerales") != -1) Mostrar = 'no';
    if (String(args._postBackElement.id).search("tvCamposFiltro") != -1) Mostrar = 'no';
    if (String(args._postBackElement.id).search("uwgConfigurarFiltros") != -1) Mostrar = 'no';
    if (Mostrar == 'si') {
        if ($find(ModalProgress))
            $find(ModalProgress).show();
    }
}

function endReq(sender, args) {
    //  shows the Popup 
    if ($find(ModalProgress))
        $find(ModalProgress).hide();
} 
