﻿var OPERADORES_FORMULA = {
    AND: 1,
    OR: 2,
    PARENTESIS_ABIERTO: 3,
    PARENTESIS_CERRADO: 4
};
var DENOMINACION_OPERADORES_FORMULA = {
    1: TextosPantalla[30],
    2: TextosPantalla[31],
    3: '(',
    4: ')'
};
var OPERADORES = {
    IGUAL: 10,
    DISTINTO: 11,
    MAYOR: 12,
    MAYORIGUAL: 13,
    MENOR: 14,
    MENORIGUAL: 15,
    CONTIENE: 20,
    EMPIEZAPOR: 21,
    TERMINAEN: 22,
    NOCONTIENE: 23,
    ENTRE: 30,
    ES: 31,
    NOES: 32,
    HACE: 50,
    PERIODO: 51
};
var DENOMINACION_OPERADORES = {
    10: TextosPantalla[44],
    11: TextosPantalla[45],
    12: TextosPantalla[46],
    13: TextosPantalla[47],
    14: TextosPantalla[48],
    15: TextosPantalla[49],
    20: TextosPantalla[50],
    21: TextosPantalla[51],
    22: TextosPantalla[52],
    23: TextosPantalla[75],
    30: TextosPantalla[53],
    31: TextosPantalla[54],
    32: TextosPantalla[55],
    50: TextosPantalla[87],
    51: TextosPantalla[54]
};
var FECHAS_RELATIVAS = {
    HORAS: 0,
    DIAS: 1,
    SEMANAS: 2,
    MESES: 3,
    AÑOS: 4
};
var DENOMINACION_FECHAS_RELATIVAS = {
    0: TextosPantalla[76],
    1: TextosPantalla[77],
    2: TextosPantalla[78],
    3: TextosPantalla[79],
    4: TextosPantalla[80]
};
var FECHAS_PERIODO = {
    HOY: 0,
    ESTA_SEMANA: 1,
    ESTE_MES: 2,
    ESTE_TRIMESTRE: 3,
    ESTE_SEMESTRE: 4,
    ESTE_AÑO: 5
};
var DENOMINACION_FECHAS_PERIODO = {
    0: TextosPantalla[81],
    1: TextosPantalla[82],
    2: TextosPantalla[83],
    3: TextosPantalla[84],
    4: TextosPantalla[85],
    5: TextosPantalla[86]
};
var escenarioEdicion, stepWizardEscenario, totalStepsWizardEscenario, idEscenarioEdicion, idFiltroEdicion, idVistaEdicion, sOrdenVista;
var campos_generales, campos_formulario;
var formulaFiltro = {};
var lCamposSeleccionadosVista = [];
var lCamposSeleccionadosVistaDesglose = [];
var bEditarVista = false;
$('#btnEscenarioNuevo').live('click', function (event) {
    ejecutandoPartialPostBack = true;
    MostrarCargando();
    idEscenarioEdicion = 0;
    idFiltroEdicion = 0;
    idVistaEdicion = 0;
    Establecer_Textos_Pantalla();
    stepWizardEscenario = 1;
    totalStepsWizardEscenario = 4;
    $('#lblWizardEscenarioTitulo').text(TextosPantalla[0]);
    $('#lblWizardEscenarioTituloFiltro').text(TextosPantalla[15]);
    $('#lblWizardEscenarioStepNumber').text(stepWizardEscenario);
    $('#lblWizardEscenarioStepCount').text(totalStepsWizardEscenario);
    $('#lblWizardEscenarioFiltroCamposGeneralesSeleccion').text(TextosPantalla[113]);
    $('#lblWizardEscenarioFiltroCamposGeneralesVisible').text(TextosPantalla[114]);
    $('#lblWizardEscenarioFiltroCamposFormularioSeleccion').text(TextosPantalla[113]);
    $('#lblWizardEscenarioFiltroCamposFormularioVisible').text(TextosPantalla[114]);
    $('[id^=wizardEscenarioStep]').hide();
    $('#lWizardEscenarioListaCamposFiltroBasico').empty();
    $('#lWizardEscenarioListaCamposFiltroAvanzado').empty();
    formulaFiltro = {};
    lCamposSeleccionadosVista = [];
    lCamposSeleccionadosVistaDesglose = [];
    $('#divWizardEscenarioFormula').empty();
    $('#txtWizardEscenarioVistaNombre').val('');
    $('#chkWizarEscenarioFiltroDefecto').prop('checked', false);
    $('#chkWizarEscenarioVistaDefecto').prop('checked', false);
    $('[id$=imgWizardEscenarioNoFavoritoNoEdit]').hide(); //Ocultamos la imagen de no favorito no editable
    $('[id$=imgWizardEscenarioFavoritoNoEdit]').hide(); // Ocultamos la imagen de favorito no editable
    $('[id$=updConfiguracionVista]').hide();

    $('#lblWizardEscenarioNombre').hide().text('');
    $('#txtWizardEscenarioFiltroNombre').val('');
    $('#txtWizardEscenarioVistaNombre').val('');

    //INICIALIZAMOS LAS OPCIONES DEL NUEVO ESCENARIO
    $('#chkWizardEscenarioDefecto').prop('checked', false).prop('disabled', false); //Descheckeamos que sea el escenario por defecto
    $('#lblWizardEscenarioFavorito').show();
    $('[id$=imgWizardEscenarioNoFavorito]').show(); //Mostramos la imagen de no favorito
    $('[id$=imgWizardEscenarioFavorito]').hide(); // Ocultamos la imagen de favorito    
    $('#rWizardEscenarioTodasSolicitudes').prop('checked', true);
    $('#rWizardEscenarioTodasSolicitudes').prop('disabled', false);
    $('#rWizardEscenarioSolicitud').prop('disabled', false);
    $("#ddWizardEscenarioTipoSolicitud").prop('disabled', true);

    $('#basicWizardEscenarioStep3').show();
    $('#advancedWizardEscenarioStep3').hide();
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Escenario',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ IdEscenario: -1, Edicion: true, iTipoVisor: tipoVisor }),
        dataType: "json",
        async: true
    })).done(function (msg) {
        //OBTENEMOS LOS DATOS NECESARIO PARA CREAR UN NUEVO ESCENARIO.
        //LOS TIPOS DE SOLICITUD QUE PUEDE ELEGIR EL USUARIO
        //LAS CARPETAS QUE TIENE CREADAS EL USUARIO
        ejecutandoPartialPostBack = false;
        ActualizarContadoresFiltro();
        OcultarCargando();
        var info = $.parseJSON(msg.d);
        escenarioEdicion = info[0];

        //Recargamos el combo de solicitudes
        $('#ddWizardEscenarioTipoSolicitud option').remove();
        var optionsTiposSolicitud = $('#ddWizardEscenarioTipoSolicitud').prop('options');
        $.each(info[1], function () {
            optionsTiposSolicitud[optionsTiposSolicitud.length] = new Option(this.text, this.value);
        });
        $('#ddWizardEscenarioTipoSolicitud').multiselect('refresh');

        //Recargamos el combo de carpetas
        $('#ddWizardEscenarioCarpetaEscenario option').remove();
        var optionsCarpetas = $('#ddWizardEscenarioCarpetaEscenario').prop('options');
        $.each(info[2], function () {
            optionsCarpetas[optionsCarpetas.length] = new Option(this.text, this.value, true, true);
        });
        $('#ddWizardEscenarioCarpetaEscenario option:eq(0)').prop('selected', true);

        $('#wizardEscenarioStep3').data('formulaAvanzada', false);
        
        campos_generales = info[3];
        $('#lCamposGenerales').empty();
        if (tipoVisor == TIPOVISOR.SOLICITUDESQA)
            $('#campoGeneral').tmpl($.map(campos_generales, function (campo_general) {
                if (campo_general.Id !== CAMPOSGENERALES.SITUACIONACTUAL && campo_general.Id !== CAMPOSGENERALES.INFOPEDIDOSASOCIADOS && campo_general.Id !== CAMPOSGENERALES.INFOPROCESOSASOCIADOS) return campo_general;
            })).appendTo($('#lCamposGenerales'));
        else
            $('#campoGeneral').tmpl($.map(campos_generales, function (campo_general) {
                if (campo_general.Id !== CAMPOSGENERALES.SITUACIONACTUAL && campo_general.Id !== CAMPOSGENERALES.INFOPEDIDOSASOCIADOS && campo_general.Id !== CAMPOSGENERALES.INFOPROCESOSASOCIADOS && campo_general.Id !== CAMPOSGENERALES.ESTADOHOMOLOGACION) return campo_general;
            })).appendTo($('#lCamposGenerales'));
        $('#lCamposGeneralesVista').empty();
        switch (Number(tipoVisor)) {
            case TIPOVISOR.SOLICITUDES:
                $('#campoGeneralVista').tmpl($.map(campos_generales, function (campo_general) { if (campo_general.Id !== CAMPOSGENERALES.ESTADOHOMOLOGACION) return campo_general })).appendTo($('#lCamposGeneralesVista'));
                break;
            case TIPOVISOR.SOLICITUDESQA:
                $('#campoGeneralVista').tmpl($.map(campos_generales, function (campo_general) {
                    if (campo_general.Id !== CAMPOSGENERALES.INFOPEDIDOSASOCIADOS && campo_general.Id !== CAMPOSGENERALES.INFOPROCESOSASOCIADOS) return campo_general;
                })).appendTo($('#lCamposGeneralesVista'));
                break;
            default:
                $('#campoGeneralVista').tmpl($.map(campos_generales, function (campo_general) {
                    if (campo_general.Id !== CAMPOSGENERALES.INFOPEDIDOSASOCIADOS && campo_general.Id !== CAMPOSGENERALES.INFOPROCESOSASOCIADOS && campo_general.Id !== CAMPOSGENERALES.ESTADOHOMOLOGACION) return campo_general;
                })).appendTo($('#lCamposGeneralesVista'));
        }        

        $('[id^=txtEscenarioVistaCampoNombrePersonalizado_]').prop('placeholder', TextosPantalla[27]);

        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();
        Cambiar_Step_Wizard(1);
        CentrarPopUp($('#wizardEscenario'));
        $('#wizardEscenario').show();
        sOrdenVista = '';
        if ($find($('[id$=whdgConfigVista]').attr('id')).get_gridView().get_behaviors().get_sorting().get_sortedColumns().length > 0)
            $find($('[id$=whdgConfigVista]').attr('id')).get_gridView().get_behaviors().get_sorting().clear();
        $('#txtWizardEscenarioNombre').show().val('').focus();
    });
    event.stopPropagation();
});
$('#btnEscenarioFiltroNuevo').live('click', function () {
    ejecutandoPartialPostBack = true;
    MostrarCargando();
    idEscenarioEdicion = 0;
    idFiltroEdicion = 0;
    Establecer_Textos_Pantalla();
    stepWizardEscenario = 1;
    totalStepsWizardEscenario = 2;
    $('#lblWizardEscenarioTitulo').text(TextosPantalla[56]);
    $('#lblWizardEscenarioStepNumber').text(stepWizardEscenario);
    $('#lblWizardEscenarioStepCount').text(totalStepsWizardEscenario);
    $('#lblWizardEscenarioFiltroCamposGeneralesSeleccion').text(TextosPantalla[113]);
    $('#lblWizardEscenarioFiltroCamposGeneralesVisible').text(TextosPantalla[114]);
    $('#lblWizardEscenarioFiltroCamposFormularioSeleccion').text(TextosPantalla[113]);
    $('#lblWizardEscenarioFiltroCamposFormularioVisible').text(TextosPantalla[114]);
    $('[id^=wizardEscenarioStep]').hide();
    $('#lWizardEscenarioListaCamposFiltroBasico').empty();
    $('#lWizardEscenarioListaCamposFiltroAvanzado').empty();
    formulaFiltro = {};
    formulaFiltro.FormulaCondiciones = [];
    $('#divWizardEscenarioFormula').empty();
    $('#txtWizardEscenarioVistaNombre').val('');
    $('#chkWizarEscenarioFiltroDefecto').prop('checked', false);

    $('#lblWizardEscenarioFavorito').hide();
    $('[id$=imgWizardEscenarioNoFavorito]').hide(); //Ocultamos la imagen de no favorito
    $('[id$=imgWizardEscenarioFavorito]').hide(); // Ocultamos la imagen de favorito
    $('[id$=imgWizardEscenarioNoFavoritoNoEdit]').hide(); //Ocultamos la imagen de no favorito no editable
    $('[id$=imgWizardEscenarioFavoritoNoEdit]').hide(); // Ocultamos la imagen de favorito no editable

    $('#basicWizardEscenarioStep3').show();
    $('#advancedWizardEscenarioStep3').hide();
    Cambiar_Step_Wizard(1);
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Escenario',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ IdEscenario: idEscenarioSeleccionado, Edicion: false, iTipoVisor: tipoVisor }),
        dataType: "json",
        async: true
    })).done(function (msg) {
        //OBTENEMOS LOS DATOS NECESARIO PARA CREAR UN NUEVO ESCENARIO.
        //LOS TIPOS DE SOLICITUD QUE PUEDE ELEGIR EL USUARIO
        //LAS CARPETAS QUE TIENE CREADAS EL USUARI
        ejecutandoPartialPostBack = false;
        ActualizarContadoresFiltro();
        OcultarCargando();
        var info = $.parseJSON(msg.d);
        escenarioEdicion = info[0];
        //INICIALIZAMOS LAS OPCIONES DEL NUEVO ESCENARIO
        $('#chkWizardEscenarioDefecto').prop('checked', escenarioEdicion.Defecto).prop('disabled', 'disabled'); //Descheckeamos que sea el escenario por defecto
        if (escenarioEdicion.Favorito) $('[id$=imgWizardEscenarioFavoritoNoEdit]').show();
        else $('[id$=imgWizardEscenarioNoFavoritoNoEdit]').show();

        $('#txtWizardEscenarioNombre').hide().val(escenarioEdicion.Nombre);
        $('#lblWizardEscenarioNombre').show().text(escenarioEdicion.Nombre);

        $('#wizardEscenarioStep3').data('formulaAvanzada', false);

        campos_generales = info[3];
        $('#lCamposGenerales').empty();        
        if (tipoVisor == TIPOVISOR.SOLICITUDESQA)
            $('#campoGeneral').tmpl($.map(campos_generales, function (campo_general) {
                if (campo_general.Id !== CAMPOSGENERALES.SITUACIONACTUAL && campo_general.Id !== CAMPOSGENERALES.INFOPEDIDOSASOCIADOS && campo_general.Id !== CAMPOSGENERALES.INFOPROCESOSASOCIADOS) return campo_general;
            })).appendTo($('#lCamposGenerales'));
        else
            $('#campoGeneral').tmpl($.map(campos_generales, function (campo_general) {
                if (campo_general.Id !== CAMPOSGENERALES.SITUACIONACTUAL && campo_general.Id !== CAMPOSGENERALES.INFOPEDIDOSASOCIADOS && campo_general.Id !== CAMPOSGENERALES.INFOPROCESOSASOCIADOS && campo_general.Id !== CAMPOSGENERALES.ESTADOHOMOLOGACION) return campo_general;
            })).appendTo($('#lCamposGenerales'));

        var idSolicitudFormulario = 0;
        if (!escenarioEdicion.AplicaTodas) {
            for (var key in escenarioEdicion.SolicitudFormularioVinculados) break;
            idSolicitudFormulario = escenarioEdicion.SolicitudFormularioVinculados[key].Key;
        };
        Cargar_Campos_Formulario(idSolicitudFormulario, 1);
        $('[id^=txtEscenarioVistaCampoNombrePersonalizado_]').prop('placeholder', TextosPantalla[27]);

        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();

        CentrarPopUp($('#wizardEscenario'));
        $('#wizardEscenario').show();

        $('#txtWizardEscenarioFiltroNombre').val('').focus();
    });
    return false;
});
$('#btnEscenarioEditar').live('click', function () {
    ejecutandoPartialPostBack = true;
    MostrarCargando();
    idEscenarioEdicion = idEscenarioSeleccionado;
    Establecer_Textos_Pantalla();
    stepWizardEscenario = 1;
    totalStepsWizardEscenario = 1;
    $('#lblWizardEscenarioTitulo').text(TextosPantalla[96]);
    $('#lblWizardEscenarioTituloFiltro').text(TextosPantalla[15]);
    $('#lblWizardEscenarioStepNumber').text(stepWizardEscenario);
    $('#lblWizardEscenarioStepCount').text(totalStepsWizardEscenario);
    $('#lblWizardEscenarioFiltroCamposGeneralesSeleccion').text(TextosPantalla[113]);
    $('#lblWizardEscenarioFiltroCamposGeneralesVisible').text(TextosPantalla[114]);
    $('#lblWizardEscenarioFiltroCamposFormularioSeleccion').text(TextosPantalla[113]);
    $('#lblWizardEscenarioFiltroCamposFormularioVisible').text(TextosPantalla[114]);
    $('[id^=wizardEscenarioStep]').hide();
    $('#lWizardEscenarioListaCamposFiltroBasico').empty();
    $('#lWizardEscenarioListaCamposFiltroAvanzado').empty();
    formulaFiltro = {};
    $('#divWizardEscenarioFormula').empty();
    $('#txtWizardEscenarioVistaNombre').val('');
    $('#chkWizarEscenarioFiltroDefecto').prop('checked', false);
    $('#chkWizarEscenarioVistaDefecto').prop('checked', false);
    $('[id$=imgWizardEscenarioNoFavoritoNoEdit]').hide(); //Ocultamos la imagen de no favorito no editable
    $('[id$=imgWizardEscenarioFavoritoNoEdit]').hide(); // Ocultamos la imagen de favorito no editable
    $('[id$=updConfiguracionVista]').hide();

    $('#txtWizardEscenarioFiltroNombre').val('');
    $('#txtWizardEscenarioVistaNombre').val('');

    //INICIALIZAMOS LAS OPCIONES DEL NUEVO ESCENARIO
    $('#chkWizardEscenarioDefecto').prop('checked', false).prop('disabled', false); //Descheckeamos que sea el escenario por defecto
    //Por defecto mostramos el escenario como No Favorito
    $('#lblWizardEscenarioFavorito').show();
    $('[id$=imgWizardEscenarioNoFavorito]').show(); //Mostramos la imagen de no favorito
    $('[id$=imgWizardEscenarioFavorito]').hide(); // Ocultamos la imagen de favorito

    $('#basicWizardEscenarioStep3').show();
    $('#advancedWizardEscenarioStep3').hide();
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Escenario',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ IdEscenario: idEscenarioSeleccionado, Edicion: true, iTipoVisor: tipoVisor }),
        dataType: "json",
        async: false
    })).done(function (msg) {
        //OBTENEMOS LOS DATOS NECESARIO PARA CREAR UN NUEVO ESCENARIO.
        //LOS TIPOS DE SOLICITUD QUE PUEDE ELEGIR EL USUARIO
        //LAS CARPETAS QUE TIENE CREADAS EL USUARIO 
        ejecutandoPartialPostBack = false;
        ActualizarContadoresFiltro();
        OcultarCargando();
        var info = $.parseJSON(msg.d);
        escenarioEdicion = info[0];
        $('#chkWizardEscenarioDefecto').prop('checked', escenarioEdicion.Defecto).prop('disabled', false);
        //Si el escenario es favorito mostramos la imagen de favorito
        if (escenarioEdicion.Favorito) {
            $('#lblWizardEscenarioFavorito').hide(); //Ocultamos el texto "añadir a favoritos"
            $('[id$=imgWizardEscenarioNoFavorito]').hide(); //Ocultamos la imagen de no favorito
            $('[id$=imgWizardEscenarioFavorito]').show(); //Mostramos la imagen de favorito            
        }

        //Recargamos el combo de solicitudes      
        $('#ddWizardEscenarioTipoSolicitud option').remove();
        var optionsTiposSolicitud = $('#ddWizardEscenarioTipoSolicitud').prop('options');
        $.each(info[1], function () {
            optionsTiposSolicitud[optionsTiposSolicitud.length] = new Option(this.text, this.value);
        });

        var TodosSioNo = false
        if (escenarioEdicion.AplicaTodas) {
            //Graba "AplicaTodas" si hay 2 o mas formularios. Pero debes cargar los formularios
            TodosSioNo = true
            $.each(escenarioEdicion.SolicitudFormularioVinculados, function (k, v) {
                TodosSioNo = false;
                return
            });
        }

        $('#rWizardEscenarioTodasSolicitudes').prop('checked', TodosSioNo);
        $('#rWizardEscenarioTodasSolicitudes').prop('disabled', true);
        $('#rWizardEscenarioSolicitud').prop('checked', !TodosSioNo);
        $('#rWizardEscenarioSolicitud').prop('disabled', true);
        $("#ddWizardEscenarioTipoSolicitud").prop('disabled', true);
        //Establecemos el valor del combo de solicitudes
        if (TodosSioNo == 1) $('#ddWizardEscenarioTipoSolicitud').multiselect("uncheckAll");
        else {
            //Si los tipos de solicitudes seleccionados no estan en la lista, las añadimos únicamente para ser mostradas         
            $.each(escenarioEdicion.SolicitudFormularioVinculados, function (k, v) {
                if ($("#ddWizardEscenarioTipoSolicitud option[value='" + v.Key + "###" + k + "']").length > 0) return true;
                optionsTiposSolicitud[optionsTiposSolicitud.length] = new Option(v.Value, v.Key + "###" + k);
            });
            var tiposSolicitudSeleccionados = $.map(escenarioEdicion.SolicitudFormularioVinculados, function (v, k) { return v.Key + '###' + k });
            $('#ddWizardEscenarioTipoSolicitud').val(tiposSolicitudSeleccionados);
        };
        $('#ddWizardEscenarioTipoSolicitud').multiselect('refresh');

        $('#ddWizardEscenarioCarpetaEscenario option').remove();
        var optionsCarpetas = $('#ddWizardEscenarioCarpetaEscenario').prop('options');
        $.each(info[2], function () {
            optionsCarpetas[optionsCarpetas.length] = new Option(this.text, this.value, true, true);
        });
        $('#ddWizardEscenarioCarpetaEscenario option[value=' + escenarioEdicion.Carpeta + ']').prop('selected', true);

        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();

        $('#wizardEscenarioStep1').show();
        $('#btnWizardEscenarioStepDown').invisible();
        $('#btnWizardEscenarioStepUp').hide();
        $('#btnWizardEscenarioEnd').css('display', 'inline-block');
        $('#txtWizardEscenarioVistaNombre').focus();

        CentrarPopUp($('#wizardEscenario'));
        $('#wizardEscenario').show();

        $('#txtWizardEscenarioNombre').hide();
        $('#lblWizardEscenarioNombre').hide();
        $('#txtWizardEscenarioNombre').val(escenarioEdicion.Nombre);
        $('#lblWizardEscenarioNombre').text(escenarioEdicion.Nombre);

        if (escenarios[idEscenarioSeleccionado].Editable)
            $('#txtWizardEscenarioNombre').show().focus();
        else
            $('#lblWizardEscenarioNombre').show();

    });
    return false;
});
$('#btnEscenarioEliminar').live('click', function () {
    if (confirm((escenarios[idEscenarioSeleccionado].Modo == MODOSESCENARIO.NO_COMPARTIDO ? '' : TextosPantalla[122]) + TextosPantalla[43])) {
        MostrarCargando();
        var idCarpeta = escenarios[idEscenarioSeleccionado].Carpeta;
        var escenariosOpcionesUsuCarpetaActual;
        escenariosOpcionesUsuCarpetaActual = $.map(escenarios, function (x) { if (x.Carpeta == escenarios[idEscenarioSeleccionado].Carpeta && x.Id !== idEscenarioSeleccionado) return x; });
        opcionesUsu = JSON.stringify({ tipoOpcion: 1, oOpcionesUsu: JSON.stringify($.each(escenariosOpcionesUsuCarpetaActual.sort(ordenPorPosicion), function (index) { this.Posicion = index; })) });
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Establecer_Opciones_Usu',
            contentType: "application/json; charset=utf-8",
            data: opcionesUsu,
            dataType: "json",
            async: true
        })).done(function (msg) {
            $.when($.ajax({
                type: "POST",
                url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Delete_Escenario',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ idEscenario: idEscenarioSeleccionado, carpeta: idCarpeta }),
                dataType: "json",
                async: true
            })).done(function (msg) {
                ejecutandoPartialPostBack = false;
                ActualizarContadoresFiltro();
                OcultarCargando();
                var info = $.parseJSON(msg.d);
                delete escenarios[idEscenarioSeleccionado];
                if ($.map(escenarios, function (escenario) { return escenario }).length == 1) $('#btnEscenarioEliminar').invisible();
                else $('#btnEscenarioEliminar').visible();
                Seleccionar_Carpeta(idCarpeta);
                if (escenariosCarpeta.length == 0) {
                    $('#panelEscenarios').empty();
                    $('#panelEscenarioFiltros').empty();
                    $('#panelEscenarioFiltrosOpcionesFiltrado').empty();
                    $('#panelEscenarioVistas .escenarioVista').remove();
                    $('[id$=updSolicitudes]').hide();
                } else {
                    Seleccionar_Escenario(idEscenarioSeleccionado, function () {
                        Crear_Filtros();
                        Seleccionar_Filtro(idFiltroSeleccionado);
                        Cargar_Campos_Condiciones_Filtro();
                        Crear_Vistas();
                        Seleccionar_Vista(idVistaSeleccionado);
                        Cargar_Solicitudes();
                    });
                };
                OcultarCargando();
            });
        });
    };
    return false;
});
$('#btnEscenarioFiltroEditar').live('click', function () {
    ejecutandoPartialPostBack = true;
    MostrarCargando();
    idEscenarioEdicion = 0;
    idFiltroEdicion = idFiltroSeleccionado;
    Establecer_Textos_Pantalla();
    stepWizardEscenario = 1;
    totalStepsWizardEscenario = 2;
    $('#lblWizardEscenarioTitulo').text(TextosPantalla[90]);
    $('#lblWizardEscenarioStepNumber').text(stepWizardEscenario);
    $('#lblWizardEscenarioStepCount').text(totalStepsWizardEscenario);
    $('#lblWizardEscenarioFiltroCamposGeneralesSeleccion').text(TextosPantalla[113]);
    $('#lblWizardEscenarioFiltroCamposGeneralesVisible').text(TextosPantalla[114]);
    $('#lblWizardEscenarioFiltroCamposFormularioSeleccion').text(TextosPantalla[113]);
    $('#lblWizardEscenarioFiltroCamposFormularioVisible').text(TextosPantalla[114]);
    $('[id^=wizardEscenarioStep]').hide();
    $('#lWizardEscenarioListaCamposFiltroBasico').empty();
    $('#lWizardEscenarioListaCamposFiltroAvanzado').empty();
    formulaFiltro = {};
    $('#divWizardEscenarioFormula').empty();
    $('#chkWizarEscenarioFiltroDefecto').prop('checked', false);
    $('[id$=imgWizardEscenarioNoFavorito]').hide(); //Ocultamos la imagen de no favorito
    $('[id$=imgWizardEscenarioFavorito]').hide(); // Ocultamos la imagen de favorito
    $('[id$=imgWizardEscenarioNoFavoritoNoEdit]').hide(); //Ocultamos la imagen de no favorito no editable
    $('[id$=imgWizardEscenarioFavoritoNoEdit]').hide(); // Ocultamos la imagen de favorito no editable
    $('#lblWizardEscenarioFavorito').hide();
    Cambiar_Step_Wizard(1);
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Escenario',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ IdEscenario: idEscenarioSeleccionado, Edicion: false, iTipoVisor: tipoVisor }),
        dataType: "json",
        async: true
    })).done(function (msg) {
        //OBTENEMOS LOS DATOS NECESARIO PARA CREAR UN NUEVO ESCENARIO.
        //LOS TIPOS DE SOLICITUD QUE PUEDE ELEGIR EL USUARIO
        //LAS CARPETAS QUE TIENE CREADAS EL USUARIO       
        ejecutandoPartialPostBack = false;
        ActualizarContadoresFiltro();
        OcultarCargando();
        var info = $.parseJSON(msg.d);
        escenarioEdicion = info[0];
        //INICIALIZAMOS LAS OPCIONES DEL NUEVO ESCENARIO
        $('#chkWizardEscenarioDefecto').prop('checked', escenarioEdicion.Defecto).prop('disabled', 'disabled'); //Descheckeamos que sea el escenario por defecto
        if (escenarioEdicion.Favorito) $('[id$=imgWizardEscenarioFavoritoNoEdit]').show();
        else $('[id$=imgWizardEscenarioNoFavoritoNoEdit]').show();

        $('#txtWizardEscenarioFiltroNombre').val(escenarioEdicion.EscenarioFiltros[idFiltroEdicion].Nombre);
        formulaFiltro.FormulaCondiciones = JSON.parse(JSON.stringify(escenarioEdicion.EscenarioFiltros[idFiltroEdicion].FormulaCondiciones));
        $('#chkWizarEscenarioFiltroDefecto').prop('checked', escenarioEdicion.EscenarioFiltros[idFiltroEdicion].Defecto);

        $('#txtWizardEscenarioNombre').hide().val(escenarioEdicion.Nombre);
        $('#lblWizardEscenarioNombre').show().text(escenarioEdicion.Nombre);

        $('#wizardEscenarioStep3').data('formulaAvanzada', escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroEdicion].FormulaAvanzada);

        campos_generales = info[3];
        $('#lCamposGenerales').empty();        
        if (tipoVisor == TIPOVISOR.SOLICITUDESQA)
            $('#campoGeneral').tmpl($.map(campos_generales, function (campo_general) {
                if (campo_general.Id !== CAMPOSGENERALES.SITUACIONACTUAL && campo_general.Id !== CAMPOSGENERALES.INFOPEDIDOSASOCIADOS && campo_general.Id !== CAMPOSGENERALES.INFOPROCESOSASOCIADOS) return campo_general;
            })).appendTo($('#lCamposGenerales'));
        else
            $('#campoGeneral').tmpl($.map(campos_generales, function (campo_general) {
                if (campo_general.Id !== CAMPOSGENERALES.SITUACIONACTUAL && campo_general.Id !== CAMPOSGENERALES.INFOPEDIDOSASOCIADOS && campo_general.Id !== CAMPOSGENERALES.INFOPROCESOSASOCIADOS && campo_general.Id !== CAMPOSGENERALES.ESTADOHOMOLOGACION) return campo_general;
            })).appendTo($('#lCamposGenerales'));

        var idSolicitudFormulario = 0;
        if (!escenarioEdicion.AplicaTodas) {
            for (var key in escenarioEdicion.SolicitudFormularioVinculados) break;
            idSolicitudFormulario = escenarioEdicion.SolicitudFormularioVinculados[key].Key;
        };
        Cargar_Campos_Formulario(idSolicitudFormulario, 1);
        $('[id^=txtEscenarioVistaCampoNombrePersonalizado_]').prop('placeholder', TextosPantalla[27]);

        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();

        CentrarPopUp($('#wizardEscenario'));
        $('#wizardEscenario').show();

        //Marcamos los checkboxes de campos que deben estar seleccionados y visibles
        $.each(escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroEdicion].Filtro_Campos, function () {
            $('#chk_Campo' + (this.EsCampoGeneral ? 'General' : 'Formulario') + '_' + this.Id).prop('checked', true);
            $('#chk_Campo' + (this.EsCampoGeneral ? 'General' : 'Formulario') + 'Visible_' + this.Id).prop('checked', this.Visible);
        });
        if (escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroEdicion].FormulaAvanzada) {
            $('#basicWizardEscenarioStep3').hide();
            $('#advancedWizardEscenarioStep3').show();

            //Establecemos los textos en lenguaje natural en la propiedad "Condicion" de cada condición de la fórmula
            EstablecerTextosCondiciones(escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].FormulaCondiciones);
            formulaFiltro.FormulaCondiciones = JSON.parse(JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroEdicion].FormulaCondiciones));
            Cargar_Formula_Avanzada();
        } else {
            $('#basicWizardEscenarioStep3').show();
            $('#advancedWizardEscenarioStep3').hide();
        };
        $('#txtWizardEscenarioFiltroNombre').focus();
    });
    return false;
});
$('#btnEscenarioFiltroEliminar').live('click', function () {
    if (confirm((escenarios[idEscenarioSeleccionado].Modo == MODOSESCENARIO.NO_COMPARTIDO ? '' : TextosPantalla[123]) + TextosPantalla[68])) {
        var idCarpeta = escenarios[idEscenarioSeleccionado].Carpeta;
        MostrarCargando();
        var nuevoEscenarioFiltros = $.parseJSON(JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioFiltros));
        delete nuevoEscenarioFiltros[idFiltroSeleccionado];
        opcionesUsu = JSON.stringify({ tipoOpcion: 2, oOpcionesUsu: JSON.stringify(nuevoEscenarioFiltros) });
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Establecer_Opciones_Usu',
            contentType: "application/json; charset=utf-8",
            data: opcionesUsu,
            dataType: "json",
            async: true
        })).done(function (msg) {
            $.when($.ajax({
                type: "POST",
                url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Delete_Escenario_Filtro',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ idEscenario: idEscenarioSeleccionado, idEscenarioFiltro: idFiltroSeleccionado }),
                dataType: "json",
                async: true
            })).done(function (msg) {
                OcultarCargando();
                var info = $.parseJSON(msg.d);
                $.each(escenarios[idEscenarioSeleccionado].EscenarioFiltros, function () {
                    if (this.Posicion > escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].Posicion)
                        this.Posicion = this.Posicion - 1;
                });
                delete escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado];
                Crear_Filtros();
                Seleccionar_Filtro(idFiltroSeleccionado);
                Cargar_Campos_Condiciones_Filtro();
                Cargar_Solicitudes();
            });
        });
    };
    return false;
});
$('#btnWizardEscenarioCerrarPopUp,#btnWizardEscenarioCancelar').live('click', function () {
    $('#wizardEscenario').hide();
    $('#popupFondo').hide();
});
$('[id$=imgWizardEscenarioNoFavorito],[id$=imgWizardEscenarioFavorito],#lblWizardEscenarioFavorito').live('click', function () {
    $('[id$=imgWizardEscenarioNoFavorito]').toggle();
    $('[id$=imgWizardEscenarioFavorito]').toggle();
});
//Cambio en el radiobutton de aplicar a tipos de solicitud
$('input[name="tiposolicitud"]').live('change', function () {
    $("#ddWizardEscenarioTipoSolicitud").prop('disabled', function () { return !$(this).prop('disabled'); });
    $("#ddWizardEscenarioTipoSolicitud").multiselect($("#ddWizardEscenarioTipoSolicitud").prop('disabled') ? 'disable' : 'enable');
    $('#selectCampoFiltro_Lista_General_7 option').remove();
    $('#selectCampoFiltroAvanzado_Lista_General_7 option').remove();
});
$('#btnWizardEscenarioStepUp').live('click', function () {
    switch (stepWizardEscenario + 1) {
        case 2:
            switch (totalStepsWizardEscenario) {
                case 2:
                    if (Comprobar_Datos_Escenario(2)) {
                        Cambiar_Step_Wizard(2);
                        Cargar_CamposFiltro();
                    } else return false;
                    break;
                case 4:
                    for (var first in escenarioEdicion.SolicitudFormularioVinculados) break;
                    var formularioAnteriorSeleccionado = (first == undefined || escenarioEdicion.AplicaTodas) ? 0 : escenarioEdicion.SolicitudFormularioVinculados[first].Key;
                    if (Comprobar_Datos_Escenario(1)) {
                        Cambiar_Step_Wizard(2);
                        var valoresFormulario;
                        if ($('#rWizardEscenarioSolicitud').prop('checked')) {
                            valoresFormulario = $('#ddWizardEscenarioTipoSolicitud').val()[0].split("###")[0];
                            if ($.grep($('#ddWizardEscenarioTipoSolicitud').val(), function (x) { return x.split('###')[0] !== valoresFormulario }).length > 0)
                                valoresFormulario = 0;
                        } else valoresFormulario = 0;
                        if (valoresFormulario == 0 || formularioAnteriorSeleccionado !== valoresFormulario)
                            Cargar_Campos_Formulario(valoresFormulario, 2);
                    } else return false;
                    break;
            };
            break;
        case 3:
            if (Comprobar_Datos_Escenario(2)) {
                Cambiar_Step_Wizard(3);
                Cargar_CamposFiltro();
            } else return false;
            break;
        case 4:
            if (Comprobar_Datos_Escenario(3))
                Cambiar_Step_Wizard(4);
            break;
        default:
            Cambiar_Step_Wizard(stepWizardEscenario + 1);
            break;
    };
});
$('#btnWizardEscenarioStepDown').live('click', function () {
    switch (totalStepsWizardEscenario) {
        case 2:
            if (stepWizardEscenario == 2)
                if (!$('#wizardEscenarioStep3').data('formulaAvanzada')) Obtener_Formula_Basica();
            break;
        case 4:
            if (stepWizardEscenario == 3)
                if (!$('#wizardEscenarioStep3').data('formulaAvanzada')) Obtener_Formula_Basica();
            break;
    };
    Cambiar_Step_Wizard(stepWizardEscenario - 1);
});
$('#btnWizardEscenarioEnd').live('click', function () {
    Comprobar_Datos_Escenario((stepWizardEscenario == 2 ? 3 : stepWizardEscenario));
});
$('[id^=imgWizardEscenarioExpandEscenario],[id^=imgWizardEscenarioCollapseEscenario],#lCamposFormulario span,#lCamposFormularioVista span').live('click', function () {
    $(this).closest('div').children('img').toggle();
    $(this).closest('div').siblings('ul[id^=camposWizardEscenarioFormulario]').toggle();
});

//Click en botón "cambiar a vista avanzada/básica"
$('#advancedChangeWizardEscenarioStep3,#basicChangeWizardEscenarioStep3').live('click', function () {
    if ($('#wizardEscenarioStep3').data('formulaAvanzada')) {
        if (confirm(TextosPantalla[70])) {
            $('#lWizardEscenarioListaCamposFiltroBasico').empty();
            formulaFiltro.FormulaCondiciones = [];
            $('#divWizardEscenarioFormula').empty();
            Cargar_CamposFiltro_Basico();
            $('#wizardEscenarioStep3').data('formulaAvanzada', false);
            $('#basicWizardEscenarioStep3').toggle();
            $('#advancedWizardEscenarioStep3').toggle();
        };
    } else {
        Obtener_Formula_Basica();
        Cargar_Formula_Avanzada();
        Cargar_CamposFiltro_Avanzado();
        $('#wizardEscenarioStep3').data('formulaAvanzada', true);
        $('#basicWizardEscenarioStep3').toggle();
        $('#advancedWizardEscenarioStep3').toggle();
    };
});
$('#lCamposGeneralesVista [type=checkbox],#lCamposFormularioVista [type=checkbox]').live('click', function () {
    var EsCampoGeneral = ($(this).parents('#lCamposGeneralesVista').length == 1);
    var id_Campo_Seleccionado = parseInt($(this).closest('li').attr('id').split('_')[1]);
    if ($(this).parents('ul[id^=camposWizardEscenarioFormularioVista_Desglose_]').length == 0) id_Campo_Desglose = 0;
    else id_Campo_Desglose = parseInt($(this).parents('ul[id^=camposWizardEscenarioFormularioVista_Desglose_]').attr('id').split('_')[2]);
    if ($(this).prop('checked')) {
        if (EsCampoGeneral) {
            lCamposSeleccionadosVista.push(campos_generales[id_Campo_Seleccionado]);
            lCamposSeleccionadosVista[lCamposSeleccionadosVista.length - 1].NombrePersonalizado = $(this).closest('li').find('[id^=txtEscenarioVistaCampoNombrePersonalizado_]').val();
            lCamposSeleccionadosVista[lCamposSeleccionadosVista.length - 1].OrdenVisualizacion = (lCamposSeleccionadosVista.length == 0 ? 0 : lCamposSeleccionadosVista.length - 1);
        } else {
            if (id_Campo_Desglose == 0) {
                lCamposSeleccionadosVista.push(campos_formulario[id_Campo_Seleccionado]);
                lCamposSeleccionadosVista[lCamposSeleccionadosVista.length - 1].NombrePersonalizado = $(this).closest('li').find('[id^=txtEscenarioVistaCampoNombrePersonalizado_]').val();
                lCamposSeleccionadosVista[lCamposSeleccionadosVista.length - 1].OrdenVisualizacion = (lCamposSeleccionadosVista.length == 0 ? 0 : lCamposSeleccionadosVista.length - 1);
            } else {
                lCamposSeleccionadosVistaDesglose.push(campos_formulario[id_Campo_Desglose].CamposDesglose[id_Campo_Seleccionado]);
                lCamposSeleccionadosVistaDesglose[lCamposSeleccionadosVistaDesglose.length - 1].NombrePersonalizado = $(this).closest('li').find('[id^=txtEscenarioVistaCampoNombrePersonalizado_]').val();
                lCamposSeleccionadosVistaDesglose[lCamposSeleccionadosVistaDesglose.length - 1].OrdenVisualizacion = (lCamposSeleccionadosVistaDesglose.length == 0 ? 0 : lCamposSeleccionadosVistaDesglose.length - 1);
            };
        };
    } else {
        $.each(lCamposSeleccionadosVista, function (index) {
            if (this.EsCampoGeneral == EsCampoGeneral && this.Id == id_Campo_Seleccionado) {
                var grid = $find($('[id$=whdgConfigVista]').attr('id')).get_gridView().get_behaviors().get_sorting();
                var campoEliminado = this;
                var camposOrdenados = $.map(grid.get_sortedColumns(), function (x) { if (x.get_key() == campoEliminado.Denominacion_BD) return x; });
                if (camposOrdenados.length > 0) {
                    grid.clear();
                    sOrdenVista = '';
                    $.each(grid.get_sortedColumns(), function () {
                        sOrdenVista += (sOrdenVista == '' ? '' : ',') + this.get_key() + ' ' + (grid.getSortDirection(this) == 1 ? 'ASC' : 'DESC');
                    });
                };
                lCamposSeleccionadosVista.splice(index, 1);
            };
        });
        $.each(lCamposSeleccionadosVistaDesglose, function (index) {
            if (this.EsCampoGeneral == EsCampoGeneral && this.Id == id_Campo_Seleccionado) lCamposSeleccionadosVistaDesglose.splice(index, 1);
        });
        $.each(lCamposSeleccionadosVista, function (index) {
            this.OrdenVisualizacion = index;
        });
        $.each(lCamposSeleccionadosVistaDesglose, function (index) {
            this.OrdenVisualizacion = index;
        });
    };
    if (lCamposSeleccionadosVista.length == 0 && lCamposSeleccionadosVistaDesglose.length == 0) $('[id$=updConfiguracionVista]').hide();
    else {
        $('[id$=updConfiguracionVista]').show();
        var btnRecargarConfigVista = $('[id$=btnRecargarConfigVista]').attr('id');
        sParameters = {};
        sParameters.CamposVista = JSON.stringify($.merge($.merge([], lCamposSeleccionadosVista), lCamposSeleccionadosVistaDesglose));
        sParameters.OrdenVista = (sOrdenVista == undefined ? '' : sOrdenVista);
        __doPostBack(btnRecargarConfigVista, JSON.stringify(sParameters));
    };
});
$('#lCamposFormulario [type=checkbox]').live('click', function () {
    var id_Campo_Seleccionado = parseInt($(this).closest('li').attr('id').split('_')[1]);
    if ($(this).parents('ul[id^=camposWizardEscenarioFormularioFiltro_Desglose_]').length == 0) id_Campo_Desglose = 0;
    else id_Campo_Desglose = parseInt($(this).parents('ul[id^=camposWizardEscenarioFormularioFiltro_Desglose_]').attr('id').split('_')[2]);
    var campoProvincia;
    if (id_Campo_Desglose == 0) campoProvincia = campos_formulario[id_Campo_Seleccionado];
    else campoProvincia = campos_formulario[id_Campo_Desglose].CamposDesglose[id_Campo_Seleccionado];
    if ($(this).prop('checked')) {
        if (campoProvincia.TipoCampoGS == 108) $('#' + $(this).closest('li').attr('id').replace(id_Campo_Seleccionado, campoProvincia.CampoPadre) + ' [type=checkbox]').prop('checked', true);
    } else {
        if (campoProvincia.TipoCampoGS == 107) $('#' + $(this).closest('li').attr('id').replace(id_Campo_Seleccionado, campoProvincia.CampoHijo) + ' [type=checkbox]').prop('checked', false);
    };
});
$('[id^=txtEscenarioVistaCampoNombrePersonalizado_]').live('change', function () {
    var elem = this;
    var modificarConfiguracion = false;
    if (lCamposSeleccionadosVista.length == 0) $('[id$=updConfiguracionVista]').hide();
    else {
        var EsCampoGeneral = ($(this).parents('#lCamposGeneralesVista').length == 1);
        var id_Campo_Seleccionado = parseInt($(this).closest('li').attr('id').split('_')[1]);
        $.each(lCamposSeleccionadosVista, function (index) {
            if (this.EsCampoGeneral == EsCampoGeneral && this.Id == id_Campo_Seleccionado) {
                lCamposSeleccionadosVista[index].NombrePersonalizado = $(elem).val();
                modificarConfiguracion = true;
            };
        });
        $.each(lCamposSeleccionadosVistaDesglose, function (index) {
            if (this.EsCampoGeneral == EsCampoGeneral && this.Id == id_Campo_Seleccionado) {
                lCamposSeleccionadosVistaDesglose[index].NombrePersonalizado = $(elem).val();
                modificarConfiguracion = true;
            };
        });
        $('[id$=updConfiguracionVista]').show();
        if (modificarConfiguracion) {
            var lCamposVista = $.merge($.merge([], lCamposSeleccionadosVista), lCamposSeleccionadosVistaDesglose);
            var btnRecargarConfigVista = $('[id$=btnRecargarConfigVista]').attr('id')
            sParameters = {};
            sParameters.CamposVista = JSON.stringify(lCamposVista);
            sParameters.OrdenVista = (sOrdenVista == undefined ? '' : sOrdenVista);
            __doPostBack(btnRecargarConfigVista, JSON.stringify(sParameters));
        };
    };
});
$('#btnWizardEscenarioLimpiarFormula').live('click', function () {
    if (confirm(TextosPantalla[41])) {
        $('#divWizardEscenarioFormula').empty();
        formulaFiltro.FormulaCondiciones = [];
    };
});
$('#btnWizardEscenarioOperadorY,#btnWizardEscenarioOperadorO,#btnWizardEscenarioOperadorAbrir,#btnWizardEscenarioOperadorCerrar').live('click', function () {
    var ordenInsercionFormula;
    if ($('[id^=bloqueFormulaAvanzada_Condicion_].BloqueFormulaSelected').length == 0) ordenInsercionFormula = formulaFiltro.FormulaCondiciones.length + 1;
    else ordenInsercionFormula = $('[id^=bloqueFormulaAvanzada_Condicion_].BloqueFormulaSelected').data('formulaCondicion').Orden;
    var formulaCondicion = {};
    formulaCondicion.Orden = ordenInsercionFormula;
    formulaCondicion.IdCampo = 0;
    formulaCondicion.EsCampoGeneral = false;
    formulaCondicion.Denominacion = '';
    formulaCondicion.Denominacion_BD = '';
    switch ($(this).attr('id').replace('btnWizardEscenarioOperador', '')) {
        case 'Y':
            formulaCondicion.Operador = OPERADORES_FORMULA.AND;
            break;
        case 'O':
            formulaCondicion.Operador = OPERADORES_FORMULA.OR;
            break;
        case 'Abrir':
            formulaCondicion.Operador = OPERADORES_FORMULA.PARENTESIS_ABIERTO;
            break;
        case 'Cerrar':
            formulaCondicion.Operador = OPERADORES_FORMULA.PARENTESIS_CERRADO;
            break;
    };
    formulaCondicion.Valores = [];
    formulaCondicion.Condicion = DENOMINACION_OPERADORES_FORMULA[formulaCondicion.Operador].toLowerCase();
    formulaFiltro.FormulaCondiciones.splice(ordenInsercionFormula - 1, 0, formulaCondicion);
    $.each(formulaFiltro.FormulaCondiciones, function (index) {
        this.Orden = index + 1;
    });
    $('#divWizardEscenarioFormula').empty();
    Cargar_Formula_Avanzada();
});

//Click en botón Agregar de un campo de filtro avanzado
$('#lWizardEscenarioListaCamposFiltroAvanzado fieldset label[name=Agregar]').live('click', function () {
    var datosCampoFormula = $(this).parents('fieldset[id^=EscenarioFiltroAvanzado_Campo]').data('campoEscenario');
    if (datosCampoFormula.TipoCampoGS == TIPOSCAMPOGS.PROVINCIA) {
        var cPais = $('#' + $(this).parents('fieldset[id^=EscenarioFiltroAvanzado_Campo]').attr('id').replace(datosCampoFormula.Id, datosCampoFormula.CampoPadre));
        cPais.find('label[name=Agregar]').trigger('click');
        $('#btnWizardEscenarioOperadorY').trigger('click');
    };
    var operadorCondicionFormula = parseInt($(this).parents('fieldset[id^=EscenarioFiltroAvanzado_Campo]').find('select.CabeceraBotones').val());
    var ordenInsercionFormula;
    if ($('[id^=bloqueFormulaAvanzada_Condicion_].BloqueFormulaSelected').length == 0) ordenInsercionFormula = formulaFiltro.FormulaCondiciones.length + 1;
    else ordenInsercionFormula = $('[id^=bloqueFormulaAvanzada_Condicion_].BloqueFormulaSelected').data('formulaCondicion').Orden;
    var formulaCondicion = {};
    formulaCondicion.Orden = ordenInsercionFormula;

    //Establecemos las propiedades de la condición
    EstablecerCondicion(formulaCondicion, $(this).closest('fieldset'), datosCampoFormula)

    //Si no hay valores, salimos
    if (formulaCondicion.Valores == undefined || formulaCondicion.Valores.length == 0) return false;

    formulaFiltro.FormulaCondiciones.splice(ordenInsercionFormula - 1, 0, formulaCondicion);
    $.each(formulaFiltro.FormulaCondiciones, function (index) {
        this.Orden = index + 1;
    });
    $('#divWizardEscenarioFormula').empty();
    Cargar_Formula_Avanzada();
});

//Click en botón Aplicar Cambios de un campo de filtro avanzado
$('#lWizardEscenarioListaCamposFiltroAvanzado fieldset label[name=AplicarCambios]').live('click', function () {
    //Obtenemos el índice del bloque seleccionado entre los bloques de la formula    
    var idx = $('#divWizardEscenarioFormula div.BloqueFormulaSelected').index();
    //Agregamos el campo (se añadirá delante)
    $(this).parent().find('label[name=Agregar]').trigger('click');
    //Eliminamos el bloque seleccionado
    $('#divWizardEscenarioFormula div[class=BloqueFormulaSelected]').find('.ui-icon.ui-icon-close').trigger('click');
    //Seleccionamos el bloque recién agregado (estará en el mismo índice que el seleccionado inicialmente)
    //Le indicamos como parámetro del evento que no establezca el foco
    $('#divWizardEscenarioFormula div[class^=BloqueFormula]').eq(idx).trigger('click', false);
});

//Click en un Bloque de la Fórmula Avanzada
$('[id^=bloqueFormulaAvanzada_Condicion_]').live('click', function (event, foco) {
    if (typeof (foco) === 'undefined') foco = true;

    //Obtenemos el bloque sobre el que hemos pulsado
    formulaCondicion = formulaFiltro.FormulaCondiciones[$(this).index()];

    //Establecemos los estilos de todos los bloques como NO SELECCIONADOS
    $('#divWizardEscenarioFormula div.BloqueFormulaSelected').removeClass('BloqueFormulaSelected').addClass('BloqueFormula');

    //Quitamos todos los botones de Aplicar Cambios
    $("fieldset [id^=EscenarioFiltroAvanzado] [name=AplicarCambios]").hide();

    //Si el bloque está seleccionado, quitamos la seleccion
    if (formulaCondicion.Seleccionado) {
        formulaCondicion.Seleccionado = false;
    } else {
        //Si el bloque no está seleccionado quitamos la seleccion anterior y seleccionamos éste
        formulaFiltro.FormulaCondiciones.forEach(function (e) { e.Seleccionado = false; });
        formulaCondicion.Seleccionado = true;
        //Reemplazamos el estilo del bloque seleccionado
        $(this).removeClass('BloqueFormula').addClass('BloqueFormulaSelected');

        //Obtenemos el campo de filtro correspondiente
        var campo = $("fieldset[id^=EscenarioFiltroAvanzado][id$=_" + formulaCondicion.IdCampo + "]");

        //Establecemos el operador
        $("[id*=EscenarioFiltroAvanzado][id$=_" + formulaCondicion.IdCampo + "] select:first").val(formulaCondicion.Operador).trigger('change');

        //Si es una fecha....
        var modoFecha = $('img[name=ModoFecha]', campo);
        if (modoFecha.length != 0) {
            //Establecemos la correspondencia entre el operador de la formula y el modo de la fecha         
            var modo = formulaCondicion.Operador == OPERADORES.HACE ? '1' : formulaCondicion.Operador == OPERADORES.PERIODO ? '2' : '0';

            //Cambiamos el modo hasta que corresponda con el de la formula            
            while (modoFecha.attr("data-type") != modo)
                modoFecha.trigger("click");

            //Establecemos los valores segun el modo
            switch (modo) {
                case "0": //Desde-Hasta                    
                    $("div[index=" + modo + "] input[id*=_Desde_]", campo).datepicker('setDate', new Date(formulaCondicion.Valores[0]));
                    if (formulaCondicion.Operador == OPERADORES.ENTRE) $("div[index=" + modo + "] input[id*=_Hasta_]", campo).datepicker('setDate', new Date(formulaCondicion.Valores[1]));
                    break;
                case "1": //Fecha relativa
                    $("div[index=" + modo + "] input:first", campo).val(formulaCondicion.Valores[1]);
                    $("div[index=" + modo + "] select:first option[value='" + formulaCondicion.Valores[0] + "']", campo).prop('selected', 'selected');
                    break;
                case "2": //Periodo
                    $("div[index=" + modo + "] select:first option[value='" + formulaCondicion.Valores[0] + "']", campo).prop('selected', 'selected');
                    break;
            }

            //Ponemos el foco en el control adecuado 
            if (foco) $((modo == "2" ? "select" : "input") + ":visible:first", campo).focus();
        } else {
            //El campo no es una fecha

            $("select[id^=selectCampoFiltroAvanzado]", campo).multiselect('uncheckAll');
            if (foco) $("select[id^=selectCampoFiltroAvanzado]", campo).multiselect('open');

            //Establecemos los valores
            $.each(formulaCondicion.Valores, function (i, e) {
                //Valores de lista 
                //NOTA: (en los valores booleanos hay que quedarse con la primera parte, ya que han sido cargados como 1###Sí ó 0###No)
                $("select[id^=selectCampoFiltroAvanzado] option[value='" + e.toString().split("###")[0] + "']", campo).prop('selected', 'selected');
                //Valores texto
                $("input[id^=txtCampoFiltroAvanzado][id*=_String_]", campo).val(e);
                //Valores numéricos (desde/hasta)               
                $("input[id^=txtCampoFiltroAvanzado][id*=" + (i == 0 ? "_Desde_" : "_Hasta_") + "]", campo).numericFormatted('val', parseFloat(e));
                //Valores de seleccion (con busqueda avanzada). En el primer valor vienen los ids y en el segundo los textos
                $("input[id^=txtCampoFiltro_]" + (i == 0 ? "[type=hidden]" : "[readonly]"), campo).val(e);
            });

            //Ponemos el foco en el control adecuado        
            if (foco) $("[id*=CampoFiltroAvanzado]:first", campo).focus();
            $("select[id^=selectCampoFiltroAvanzado]", campo).multiselect('refresh');
        }

        //Mostramos el botón de Aplicar Cambios
        $("label[name=AplicarCambios]", campo).show();
    }
});

//Mostrar el boton de eliminar bloque al posicionar el cursor encima
$('[id^=bloqueFormulaAvanzada_Condicion_]').live('mouseenter mouseleave', function () {
    $(this).children('.ui-icon').toggle();
});

//Click en botón de eliminar bloque
$('[class^=BloqueFormula] .ui-icon.ui-icon-close').live('click', function (e) {
    //Impedimos la propagación del evento para que no salte el click del bloque completo
    e.stopPropagation();

    //Obtenemos el índice del bloque en el que hemos pulsado el boton de eliminar
    var bloque = $(this).parent('[class^=BloqueFormula]')

    //Si el bloque está seleccionado, ocultamos todos los botones de Aplicar Cambios
    if (bloque.hasClass('BloqueFormulaSelected'))
        $("fieldset [id^=EscenarioFiltroAvanzado] [name=AplicarCambios]").hide();

    //Eliminamos la condicion de la fórmula
    formulaFiltro.FormulaCondiciones.splice(bloque.index(), 1);
    $.each(formulaFiltro.FormulaCondiciones, function (index) {
        this.Orden = index + 1;
    });
    //Eliminamos el bloque de la fórmula
    $(this).parent('[class^=BloqueFormula]').remove();
});

$('#lWizardEscenarioListaCamposFiltroAvanzado select.CabeceraBotones,#panelEscenarioFiltrosOpcionesFiltrado select.CabeceraBotones').live('change', function () {
    if ($(this).parent('div[index=1]').length == 0 && $(this).parent('div[index=2]').length == 0) {
        if ($(this).val() == OPERADORES.ENTRE) {
            $(this).siblings('span').show();
            $(this).siblings('span').next('div').visible();
        } else {
            $(this).siblings('span').hide();
            $(this).siblings('span').next('div').invisible();
        };
    };
});
$("#ddWizardEscenarioTipoSolicitud").live('change', function () {
    $('#selectCampoFiltro_Lista_General_7 option').remove();
    $('#selectCampoFiltroAvanzado_Lista_General_7 option').remove();
});
$('#txtWizardEscenarioNombre').live('change', function () {
    $('#lblWizardEscenarioNombre').text($(this).val());
});
//Click en el selector de Modo de las Fechas
$('img[name=ModoFecha]').live('click', function () {
    //Definimos un array con la imagen de cada modo de fecha
    var imagenes = ["fecha_desde_hasta.png", "fecha_pasado.png", "fecha_concreta.png"];

    //Obtenemos el primer atributo index de las capas visibles (modo que se esta visualizando)
    var index = parseInt($(this).siblings('div:visible').attr('index'));

    //Ocultamos todos los modos de la fecha    
    $(this).siblings('div').removeClass('inlineBlock').addClass('Ocultar');

    //Calculamos el siguiente modo
    var siguienteModo = (index + 1) % 3;

    //Mostramos el siguiente modo que corresponda
    $(this).attr('src', ruta + "/images/" + imagenes[siguienteModo]);
    $(this).siblings('div[index=' + siguienteModo + ']').removeClass('Ocultar').addClass('inlineBlock')

    //Almacenamos en la imagen el nuevo modo visualizado
    $(this).attr('data-type', siguienteModo);
});

//Click en el checkbox de seleccion de un campo para el filtro
$('#lCamposGenerales input[id^=chk_Campo]:not([id*=Visible_]),#lCamposFormulario input[id^=chk_Campo]:not([id*=Visible_])').live('change', function () {
    //Establecemos el mismo estado del check al de campo visible
    $('input[id^=chk_Campo][id$=Visible_' + $(this).attr('id').split('_')[2] + ']').prop('checked', $(this).prop('checked'));
});

//Click en el checkbox de visible de un campo para el filtro
$('#lCamposGenerales input[id^=chk_Campo][id*=Visible_],#lCamposFormulario input[id^=chk_Campo][id*=Visible_]').live('change', function () {
    //Si se ha activado el checkbox de Visible y el checkbox de seleccion no está activado, lo activamos
    var checkSeleccion = $('#lCamposGenerales input[id^=chk_Campo]:not([id*=Visible_])[id$=_' + $(this).attr('id').split('_')[2] + '],#lCamposFormulario input[id^=chk_Campo]:not([id*=Visible_])[id$=_' + $(this).attr('id').split('_')[2] + ']');
    if ($(this).prop('checked') && !checkSeleccion.prop('checked')) checkSeleccion.prop('checked', true);
});

//#region Compartir Escenario

//Click en botón Compartir Escenario
$('#btnEscenarioCompartir').live('click', function (event) {
    event.stopPropagation();

    $('#lblPanelCompartirLabelDenominacion').text(TextosPantalla[3]);
    $('#lblPanelCompartirNombreEscenario').text(escenarios[idEscenarioSeleccionado].Nombre);
    $('#lblPanelCompartirTituloDetalle').text(TextosPantalla[108]);
    $('#lblPanelCompartirLabelUsuarios').text(TextosPantalla[109]);
    $('#btnPanelCompartirCancelar').text(TextosPantalla[11]);
    $('#btnPanelCompartirFinalizar').text(TextosPantalla[14]);
    $('#lblPanelCompartirUONDep').text(TextosPantalla[120]);
    $('#lblPanelCompartirProveedores').text(TextosPantalla[126]);
    //Establecemos los textos
    if (escenarios[idEscenarioSeleccionado].Modo == MODOSESCENARIO.NO_COMPARTIDO)
        $('#lblPanelCompartirTitulo').text(TextosPantalla[107]); //Crear el escenario para otros usuarios
    else
        $('#lblPanelCompartirTitulo').text(TextosPantalla[115]); //Modificar usuarios compartidos

    if (permisos.CREAR_ESCENARIOS_PARA_OTROS_USUARIOS) {
        //Cargamos la sugerencias de unidades organizativas
        CargarSugerenciasCompartirEscenarioUONS();
        //Cargamos la sugerencias de usuarios
        CargarSugerenciasCompartirEscenarioUsuario();
        //Cargamos las UON con los que se tiene compartido el escenario
        CargarUONSCompartirEscenario();
        //Cargamos los usuarios con los que se tiene compartido el escenario
        CargarUsuariosCompartirEscenario();

        //Deseleccionamos los tokens recien aÃ±adidos
        $('#token-input-txtPanelCompartirUsuarios').focus().blur();
        //Hacemos que el panel sea modal
        $('#popupFondo').css('height', $(document).height()).show();
        $('#tvUONsCompartir').hide();
    }
    else $('.CompartirUSU').hide();

    if (permisos.CREAR_ESCENARIOS_PARA_PORTAL) {
        $('#chkPanelCompartirProveedores').prop('checked', escenarios[idEscenarioSeleccionado].Portal);
        $('#pnlPanelCompartirProveedores').show();
    } else $('#pnlPanelCompartirProveedores').hide();

    //Centramos el panel en la pantalla
    CentrarPopUp($('#panelCompartir'));
    //Mostramos el panel
    $('#panelCompartir').show();
});

//Click en el botón cerrar del panel Compartir Escenario
$('#btnPanelCompartirCerrarPopUp').live('click', function (event) {
    //Ocultamos el panel
    $('#popupFondo').hide();
    $('#panelCompartir').hide();
    selectedTokenInputs = [];
});

//Click en el botón Cancelar del panel Compartir Escenario
$('#btnPanelCompartirCancelar').live('click', function (event) {
    //Ocultamos el panel
    $('#popupFondo').hide();
    $('#panelCompartir').hide();
    selectedTokenInputs = [];
});

//Click en el botón Finalizar del panel Compartir Escenario
$('#btnPanelCompartirFinalizar').live('click', function (event) {
    var CompartirUON0 = false, CompartirUSU = [], CompartirUONs = [];
    var portal = false;

    //Obtenemos el control de usuarios
    var UONsCompartir = $('#txtPanelCompartirUONDep');
    var usuariosCompartir = $('#txtPanelCompartirUsuarios');
    var escenarioSel = escenarios[idEscenarioSeleccionado];
    escenarioSel.CompartirUONs = [];
    escenarioSel.CompartirUSU = [];

    if (permisos.CREAR_ESCENARIOS_PARA_OTROS_USUARIOS) {
        //Actualizamos los usuarios en el escenario    
        escenarioSel.CompartirUONs = UONsCompartir.tokenInput('get');
        escenarioSel.CompartirUSU = usuariosCompartir.tokenInput('get');

        //No permitimos añadir al propietario del escenario
        escenarioSel.CompartirUSU = $.grep(escenarioSel.CompartirUSU, function (x) { return x.id != escenarioSel.Propietario; });

        CompartirUON0 = ($.map(escenarioSel.CompartirUONs, function (x) { if (x.id == '##') return x.id; }).length == 1);
        CompartirUSU = $.map(escenarioSel.CompartirUSU, function (x) { return x.id; });
        CompartirUONs = $.map(escenarioSel.CompartirUONs, function (x) { if (x.id !== '##') return x.id; });
    }

    if (permisos.CREAR_ESCENARIOS_PARA_PORTAL) portal = $('#chkPanelCompartirProveedores').prop('checked');
    escenarios[idEscenarioSeleccionado].Portal = portal;

    //Si el escenario esta compartido, lo establecemos como protegido
    escenarioSel.Modo = (escenarioSel.CompartirUSU.length == 0 && escenarioSel.CompartirUONs.length == 0 && !$('#chkPanelCompartirProveedores').prop('checked')) ? MODOSESCENARIO.NO_COMPARTIDO : MODOSESCENARIO.COMPARTIDO_PROTEGIDO;

    //Refrescamos el bloque de escenario
    $('#escenario_' + idEscenarioSeleccionado).replaceWith($('#escenario').tmpl(escenarioSel));

    //Refrescamos el tooltip del botón de compartir
    if (escenarioSel.Modo == MODOSESCENARIO.NO_COMPARTIDO)
        $('#btnEscenarioCompartir').prop('title', TextosPantalla[107]); //Crear el escenario para otros usuarios
    else
        $('#btnEscenarioCompartir').prop('title', TextosPantalla[115]); //Modificar usuarios compartidos  

    var params = JSON.stringify({
        IdEscenario: idEscenarioSeleccionado,
        CompartirUON0: CompartirUON0,
        CompartidosUSU: CompartirUSU,
        CompartidosUON: CompartirUONs,
        CompartirPortal: portal
    });
    //Guardamos los usuarios compartidos
    $.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Update_Escenario_Usuarios_Compartir',
        contentType: "application/json; charset=utf-8",
        data: params,
        dataType: "json",
        async: true
    });

    //En caso de que un usuario (no propietario) se quite a sí mismo, debemos recargar toda la carpeta actual
    if (escenarioSel.Propietario != usuario.Cod
        && $.grep(escenarioSel.CompartirUSU, function (x) { return x.id == usuario.Cod }).length == 0
        && $.grep(escenarioSel.CompartirUONs, function (x) {
            return ((x.id = '##') || (x.id.split('#')[0] == usuario.UON1 && x.id.split('#')[1] == '' && x.id.split('#')[2] == '' && x.id.split('#')[3] == '') ||
                    (x.id.split('#')[0] == usuario.UON1 && x.id.split('#')[1] == usuario.UON2 && x.id.split('#')[2] == '' && x.id.split('#')[3] == '') ||
                    (x.id.split('#')[0] == usuario.UON1 && x.id.split('#')[1] == usuario.UON2 && x.id.split('#')[2] == usuario.UON3 && x.id.split('#')[3] == '') ||
                    (x.id.split('#')[0] == usuario.UON1 && x.id.split('#')[1] == usuario.UON2 && x.id.split('#')[2] == usuario.UON3 && x.id.split('#')[3] == usuario.Dep))
    }).length == 0) {
        //Eliminamos el escenario
        delete escenarios[idEscenarioSeleccionado];
        //Recargamos la carpeta
        Ir_A_Carpeta(idCarpetaSeleccionada);
    }

    //Ocultamos el panel
    $('#popupFondo').hide();
    $('#panelCompartir').hide();
    selectedTokenInputs = [];
});

//Click en el boton de buscar UONs para compartir
$('[id$=imgBuscadorUONsCompartir]').live('click', function () {
    $('#tvUONsCompartir').toggle();
    $('#tvUONsCompartir').jstree('close_all');
    $('#tvUONsCompartir').jstree('open_node', '##');
    CentrarPopUp($('#panelCompartir'));
});
var selectedTokenInputs = [];
var updatingToken = false;
//Carga las UON disponibles para compartir el escenario
function CargarSugerenciasCompartirEscenarioUONS() {
    //Obtenemos el control de usuarios
    var usuariosCompartir = $('#txtPanelCompartirUONDep');

    //Si el control ya estÃ¡ inicializado, salimos
    if (usuariosCompartir.data("tokenInputObject") !== undefined) return;
    
    usuariosCompartir.tokenInput('VisorSolicitudes.aspx/Obtener_UONs_Compartir?queryParam=q&iTipoVisor=' + tipoVisor + '&DeOtroUsuario=' + !(usuario.Cod == escenarios[idEscenarioSeleccionado].Propietario), {
        theme: "facebook",
        hintText: TextosPantalla[125],
        searchingText: TextosPantalla[111],
        noResultsText: TextosPantalla[112],
        minChars: 3,
        resultsLimit: 50,
        preventDuplicates: true,
        method: "POST",
        onReady: function () {
            //Ponemos el display de la lista inline-block
            $("ul[class^=token-input-list]", usuariosCompartir.parent()).css('display', 'inline-block');
            $("ul[class^=token-input-list]", usuariosCompartir.parent()).css('vertical-align', 'middle');
            $("ul[class^=token-input-list]", usuariosCompartir.parent()).css('max-height', '10em');
            $("ul[class^=token-input-list]", usuariosCompartir.parent()).css('overflow-y', 'auto');
            //En las ventanas modales el dropdown se debe mostrar en primer plano
            $("div[class^=token-input-dropdown]").css("z-index", "9999");
        },
        onAdd: function (item) {
            if (!updatingToken) GestionarUONsSeleccionadas(item, true);
        },
        onDelete: function (item) {
            if (!updatingToken) GestionarUONsSeleccionadas(item, false);
        }
    });

    $('#tvUONsCompartir').on('loaded.jstree', function () {
        $('#tvUONsCompartir').jstree('close_all');
    }).on('select_node.jstree', function (e, data) {
        if (updatingToken) {
            CheckChildren(data.node.id, data);
            CheckParentAllChildren(data.instance.get_node(data.node.id).parent, data);
        } else GestionarUONsSeleccionadas({ 'id': data.node.id, 'name': data.node.text }, true);
    }).on('deselect_node.jstree', function (e, data) {
        if (!updatingToken) GestionarUONsSeleccionadas({ 'id': data.node.id, 'name': data.node.text }, false);
    }).on('after_open.jstree', function (a, b) {
        $('#tvUONsCompartir').jstree('deselect_all');
        $('#tvUONsCompartir').jstree('select_node', $.map(selectedTokenInputs, function (x) { return { 'id': x.id, 'text': x.name } }));
    }).jstree({
        'core': {
            'data': {
                'type': 'POST',
                'dataType': 'json',
                'async': true,
                'contentType': 'application/json;',
                'url': rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_UONs_Compartir_Tree',
                'data': function (node) {
                    if ($(node).attr('id') == '#') {
                        return JSON.stringify({ operation: 'get_children', UON0: '', UON1: '', UON2: '', UON3: '', iTipoVisor: tipoVisor, DeOtroUsuario: !(usuario.Cod == escenarios[idEscenarioSeleccionado].Propietario)});
                    } else {
                        var UON1, UON2, UON3;
                        UON1 = $(node).attr('id').split('#')[0];
                        UON2 = $(node).attr('id').split('#')[1];
                        UON3 = $(node).attr('id').split('#')[2];

                        return JSON.stringify({ operation: 'get_children', UON0: '#', UON1: UON1, UON2: UON2, UON3: UON3, iTipoVisor: tipoVisor, DeOtroUsuario: !(usuario.Cod == escenarios[idEscenarioSeleccionado].Propietario) });
                    };
                },
                'success': function (retval) {
                    return retval.d;
                }
            }
        },
        'plugins': ["checkbox"],
        checkbox: {
            three_state: false
        }
    });
};
function GestionarUONsSeleccionadas(item, add) {
    updatingToken = true;
    if (add) {
        //Si no tiene padres seleccionados y no esta en el array lo añadimos
        if ($.map(selectedTokenInputs, function (x) { if (x.id == item.id.split('#').slice(0, 3).join('#').split('#').slice(0, $.map(item.id.split('#'), function (x) { if (x != '') return x; }).length - 1).join('#') || x.id == item.id) return x; }).length == 0) selectedTokenInputs.push(item);
        //Quitamos del array todos los hijos seleccionados
        if (item.id.split('#').slice(3, 4) == '') {
            var node = item.id.split('#').slice(0, 3).join('#').split('#').slice(0, $.map(item.id.split('#'), function (x) { if (x != '') return x; }).length).join('#');
            selectedTokenInputs = $.map(selectedTokenInputs, function (x) { if (node != x.id.split('#').slice(0, $.map(item.id.split('#'), function (x) { if (x != '') return x; }).length).join('#') || item.id == x.id) return x; });
        };
    } else {
        var nivel = $.map(item.id.split('#'), function (x) { if (x != '') return x; }).length;
        var hermanosDeseleccionado = [];
        //Añadimos los hermanos del deseleccionado y hermanos de antecesores (esto solo puede pasar cuando se deselecciona de treeview)
        if (nivel > 1 && $('#tvUONsCompartir').jstree('get_node', item.id)) {
            var nodoAntecesor = $('#tvUONsCompartir').jstree('get_node', $('#tvUONsCompartir').jstree('get_node', item.id).parent);
            var nodoEvitar = item.id;
            for (i = nivel - 1; i > 0; i--) {
                $.each(nodoAntecesor.children, function () { if (nodoEvitar != this && $('#tvUONsCompartir').jstree('get_node', this).state.selected) hermanosDeseleccionado.push({ 'id': this, 'name': $('#tvUONsCompartir').jstree('get_node', this).text }) });
                if (i > 1) {
                    nodoEvitar = nodoAntecesor.id;
                    nodoAntecesor = $('#tvUONsCompartir').jstree('get_node', nodoAntecesor.parent);
                };
            }
        };
        //Quitamos el deseleccionado y sus antecesores
        selectedTokenInputs = $.map(selectedTokenInputs, function (x) { if (x.id != item.id) return x; });
        for (i = 1; i < $.map(item.id.split('#'), function (x) { if (x != '') return x; }).length; i++) {
            selectedTokenInputs = $.map(selectedTokenInputs, function (x) { if (x.id.split('#').slice(0, 3).join('#').split('#').slice(0, i).join('#') + '###'.split('#').slice(0, 3).join('#') != item.id.split('#').slice(0, 3).join('#').split('#').slice(0, i).join('#') + '###'.split('#').slice(0, 3).join('#') || x.id == item.id) return x; });
        };
        //Quitamos tambien los hijos del deseleccionado        
        var node = item.id.split('#').slice(0, 3).join('#').split('#').slice(0, $.map(item.id.split('#'), function (x) { if (x != '') return x; }).length).join('#');
        selectedTokenInputs = $.map(selectedTokenInputs, function (x) { if (node != x.id.split('#').slice(0, $.map(item.id.split('#'), function (x) { if (x != '') return x; }).length).join('#') || item.id == x.id) return x; });
        $.merge(selectedTokenInputs, hermanosDeseleccionado);
    };
    $('#txtPanelCompartirUONDep').tokenInput('clear').tokenInput('addRange', selectedTokenInputs);
    $('#tvUONsCompartir').jstree('deselect_all').jstree('select_node', $.map(selectedTokenInputs, function (x) { return { 'id': x.id, 'text': x.name } }));
    updatingToken = false;
};
function CheckChildren(id, data) {
    $.each(data.instance.get_node(id).children, function () {
        $('#tvUONsCompartir').jstree('select_node', this);
    });
};

function CheckParentAllChildren(id, data) {
    var parentNode = data.instance.get_node(id);
    if (!parentNode.state.disabled && !parentNode.state.selected && parentNode.id != '#') {
        if ($.grep(parentNode.children, function (x) { return (!data.instance.get_node(x).state.selected) }).length == 0) {
            GestionarUONsSeleccionadas({ 'id': parentNode.id, 'name': parentNode.text }, true);
        };
    };
};
//Carga los usuarios disponibles para compartir el escenario
function CargarSugerenciasCompartirEscenarioUsuario() {
    //Obtenemos el control de usuarios
    var usuariosCompartir = $('#txtPanelCompartirUsuarios');

    //Si el control ya está inicializado, salimos
    if (usuariosCompartir.data("tokenInputObject") !== undefined) return;

    usuariosCompartir.tokenInput('VisorSolicitudes.aspx/Obtener_Usuarios_Compartir?queryParam=q&iTipoVisor=' + tipoVisor + '&DeOtroUsuario=' + !(usuario.Cod == escenarios[idEscenarioSeleccionado].Propietario), {
        theme: "facebook",
        hintText: TextosPantalla[110],
        searchingText: TextosPantalla[111],
        noResultsText: TextosPantalla[112],
        minChars: 3,
        resultsLimit: 50,
        preventDuplicates: true,
        method: "POST",
        onReady: function () {
            //Ponemos el display de la lista inline-block
            $("ul[class^=token-input-list]", usuariosCompartir.parent()).css('display', 'inline-block');
            $("ul[class^=token-input-list]", usuariosCompartir.parent()).css('vertical-align', 'middle');
            //En las ventanas modales el dropdown se debe mostrar en primer plano
            $("div[class^=token-input-dropdown]").css("z-index", "9999");
        }
    });
};

//Carga las UONs que tienen compartido el escenario
function CargarUONSCompartirEscenario() {
    updatingToken = true;
    //Si ya tiene valores, salimos
    if (escenarios[idEscenarioSeleccionado].CompartirUONs !== undefined) {
        selectedTokenInputs = $.parseJSON(JSON.stringify(escenarios[idEscenarioSeleccionado].CompartirUONs));
        $('#txtPanelCompartirUONDep').tokenInput('clear').tokenInput('addRange', escenarios[idEscenarioSeleccionado].CompartirUONs);
        updatingToken = false;
    } else {
        var params = JSON.stringify({ IdEscenario: idEscenarioSeleccionado });
        //Obtenemos los usuarios con los que se comparte el escenario
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Escenario_UONs_Compartir',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        })).done(function (msg) {
            escenarios[idEscenarioSeleccionado].CompartirUONs = msg.d;
            selectedTokenInputs = msg.d;
            $('#txtPanelCompartirUONDep').tokenInput('clear').tokenInput('addRange', escenarios[idEscenarioSeleccionado].CompartirUONs);
            updatingToken = false;
        });
    };
};

//Carga los usuarios que tienen compartido el escenario seleccionado
function CargarUsuariosCompartirEscenario() {
    //Si ya tiene valores, salimos
    if (escenarios[idEscenarioSeleccionado].CompartirUSU !== undefined) {
        $('#txtPanelCompartirUsuarios').tokenInput('clear').tokenInput('addRange', escenarios[idEscenarioSeleccionado].CompartirUSU);
    } else {
        var params = JSON.stringify({ IdEscenario: idEscenarioSeleccionado });
        //Obtenemos los usuarios con los que se comparte el escenario
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Escenario_Usuarios_Compartir_Usu',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        })).done(function (msg) {
            escenarios[idEscenarioSeleccionado].CompartirUSU = msg.d;
            $('#txtPanelCompartirUsuarios').tokenInput('clear').tokenInput('addRange', escenarios[idEscenarioSeleccionado].CompartirUSU);
        });
    };
}

//Muestra la ventana de búsqueda avanzada para el campo de usuarios para compartir el escenario
function BuscarUsuariosCompartir(button) {
    var btn = $(button);
    //Generamos un id único para el botón
    btn.uniqueId();

    //Vinculamos un evento personalizado al botón
    if (!(btn.data('events') && btn.data('events')['addItems'])) {
        btn.bind('addItems', function (e, items) {
            //Buscamos el input de la izquierda de este botón y le añadimos los items                
            btn.prev('input').tokenInput('addRange', $.map(items, function (x) { return { id: x.id, name: x.id + " - " + x.name }; }));
        });
    };

    //Llamamos a la página de búsqueda con el id generado
    var rutaURL = rutaFS + '_Common/BuscadorUsuarios.aspx?Desde=CompartirEscenario&Multiple=1&IdControl=' + $(button).attr('id');
    window.open(rutaURL, '_blank', 'width=850,height=630,status=yes,resizable=no,top=200,left=200');
};

//#endregion

function Guardar() {
    MostrarCargando();
    var campos_Filtro_Generales_Seleccionados = $('#lCamposGenerales input:not([id*=Visible_])[type=checkbox]:checked').closest('li');
    var campos_Filtro_Formulario_Seleccionados = $('#lCamposFormulario input:not([id*=Visible_])[type=checkbox]:checked').closest('li');
    with (escenarioEdicion) {
        if (totalStepsWizardEscenario == 4 || idEscenarioEdicion !== 0) {
            Nombre = $('#txtWizardEscenarioNombre').val();
            Carpeta = $('#ddWizardEscenarioCarpetaEscenario').val() == null ? 0 : $('#ddWizardEscenarioCarpetaEscenario').val();
            Defecto = $('#chkWizardEscenarioDefecto').prop('checked');
            Favorito = $('[id$=imgWizardEscenarioFavorito]').is(':visible');
            TipoVisor = tipoVisor;
        };
        if (totalStepsWizardEscenario == 4 || totalStepsWizardEscenario == 2) {
            var escenarioFiltroNuevo = {};
            var id_Campo_Seleccionado, id_Campo_Desglose;
            escenarioFiltroNuevo.Id = idFiltroEdicion;
            escenarioFiltroNuevo.Nombre = $('#txtWizardEscenarioFiltroNombre').val();
            escenarioFiltroNuevo.Defecto = $('#chkWizarEscenarioFiltroDefecto').prop('checked');
            escenarioFiltroNuevo.FormulaAvanzada = $('#wizardEscenarioStep3').data('formulaAvanzada');
            if (!escenarioFiltroNuevo.FormulaAvanzada) Obtener_Formula_Basica();
            escenarioFiltroNuevo.FormulaCondiciones = formulaFiltro.FormulaCondiciones;
            escenarioFiltroNuevo.Filtro_Campos = [];
            $.each(campos_Filtro_Generales_Seleccionados, function () {
                id_Campo_Seleccionado = parseInt($(this).attr('id').split('_')[1]);
                //Establecemos la visibilidad del campo
                campos_generales[id_Campo_Seleccionado].Visible = $('input[id=chk_CampoGeneralVisible_' + id_Campo_Seleccionado + ']').prop('checked');
                escenarioFiltroNuevo.Filtro_Campos.push(campos_generales[id_Campo_Seleccionado]);
            });
            $.each(campos_Filtro_Formulario_Seleccionados, function () {
                id_Campo_Seleccionado = parseInt($(this).attr('id').split('_')[1]);
                if ($(this).parents('ul[id^=camposWizardEscenarioFormularioFiltro_Desglose_]').length == 0) id_Campo_Desglose = 0;
                else id_Campo_Desglose = parseInt($(this).parents('ul[id^=camposWizardEscenarioFormularioFiltro_Desglose_]').attr('id').split('_')[2]);
                var campo_formulario = id_Campo_Desglose == 0 ? campos_formulario[id_Campo_Seleccionado] : campos_formulario[id_Campo_Desglose].CamposDesglose[id_Campo_Seleccionado];
                //Establecemos la visibilidad del campo
                campo_formulario.Visible = $('input[id=chk_CampoFormularioVisible_' + id_Campo_Seleccionado + ']').prop('checked');
                escenarioFiltroNuevo.Filtro_Campos.push(campo_formulario);
            });
            EscenarioFiltros[idFiltroEdicion] = escenarioFiltroNuevo;
        };
        if ((totalStepsWizardEscenario == 4 || totalStepsWizardEscenario == 1) && idEscenarioEdicion == 0) {
            var escenarioVistaNuevo = {};
            escenarioVistaNuevo.Id = idVistaEdicion;
            escenarioVistaNuevo.Nombre = $('#txtWizardEscenarioVistaNombre').val();
            escenarioVistaNuevo.Defecto = $('#chkWizarEscenarioVistaDefecto').prop('checked');
            escenarioVistaNuevo.Orden = sOrdenVista;
            escenarioVistaNuevo.Campos_Vista = $.merge($.merge([], lCamposSeleccionadosVista), lCamposSeleccionadosVistaDesglose);
            EscenarioVistas[idVistaEdicion] = escenarioVistaNuevo;
        };
    };
    var url, params, filtroCondicionesAUX;
    switch (totalStepsWizardEscenario) {
        case 1:
            if (idEscenarioEdicion == 0) {
                filtroCondicionesAUX = escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].FormulaCondiciones;
                if (idVistaEdicion == 0) {
                    url = 'Insert_Escenario_Vista';
                    escenarioEdicion.EscenarioVistas[0].Posicion = Object.keys(escenarios[idEscenarioSeleccionado].EscenarioVistas).length;
                    escenarioEdicion.EscenarioVistas[0].Escenario = idEscenarioSeleccionado;
                    params = JSON.stringify({ Vista: JSON.stringify(escenarioEdicion.EscenarioVistas[0]), iTipoVisor: tipoVisor });
                } else {
                    url = 'Update_Escenario_Vista';
                    escenarioEdicion.EscenarioVistas[idVistaEdicion].Escenario = idEscenarioSeleccionado;
                    params = JSON.stringify({ Vista: JSON.stringify(escenarioEdicion.EscenarioVistas[idVistaEdicion]), iTipoVisor: tipoVisor });
                };
            } else {
                url = 'Update_Escenario';
                escenarioEdicion.Posicion = escenarios[idEscenarioSeleccionado].Posicion;
                params = JSON.stringify({ oEscenario: JSON.stringify(escenarioEdicion), iTipoVisor: tipoVisor });
            };
            break;
        case 2:
            if (idFiltroEdicion == 0) {
                url = 'Insert_Escenario_Filtro';
                escenarioEdicion.EscenarioFiltros[0].Posicion = Object.keys(escenarios[idEscenarioSeleccionado].EscenarioFiltros).length;
                escenarioEdicion.EscenarioFiltros[0].Escenario = idEscenarioSeleccionado;
                params = JSON.stringify({ Filtro: JSON.stringify(escenarioEdicion.EscenarioFiltros[0]), iTipoVisor: tipoVisor });
            } else {
                url = 'Update_Escenario_Filtro';
                if (escenarioFiltroCondicionesOrden[idEscenarioSeleccionado] == undefined) escenarioFiltroCondicionesOrden[idEscenarioSeleccionado] = [];
                escenarioEdicion.EscenarioFiltros[idFiltroEdicion].Escenario = idEscenarioSeleccionado;
                escenarioFiltroCondicionesOrden[idEscenarioSeleccionado][idFiltroSeleccionado] = [];
                params = JSON.stringify({ Filtro: JSON.stringify(escenarioEdicion.EscenarioFiltros[idFiltroEdicion]), iTipoVisor: tipoVisor });
            };
            break;
        case 4:
            escenarioEdicion.Id = -1;
            url = 'Insert_Escenario';
            escenarioEdicion.Posicion = $.map(escenarios, function (x) { if (x.Carpeta == escenarioEdicion.Carpeta) return x; }).length;
            params = JSON.stringify({ oEscenario: JSON.stringify(escenarioEdicion), iTipoVisor: tipoVisor });
            break;
    };
    $('#wizardEscenario').hide();
    var opcionesUsu;
    var escenariosOpcionesUsuCarpetaActual = [], escenariosOpcionesUsuCarpetaAnterior = [];
    switch (url) {
        case 'Insert_Escenario':
        case 'Update_Escenario':
            escenariosOpcionesUsuCarpetaActual = $.map(escenarios, function (x) { if (x.Carpeta == escenarioEdicion.Carpeta) return x; });
            if (url == 'Update_Escenario' && escenarioEdicion.Carpeta !== escenarios[escenarioEdicion.Id].Carpeta) {
                escenarioEdicion.Posicion = escenariosOpcionesUsuCarpetaActual.length;
                escenariosOpcionesUsuCarpetaActual.push(escenarioEdicion);
                escenariosOpcionesUsuCarpetaAnterior = $.map(escenarios, function (x) { if (x.Carpeta == escenarios[escenarioEdicion.Id].Carpeta && x.Id !== escenarioEdicion.Id) return x; });
            };
            $.each(escenariosOpcionesUsuCarpetaActual.sort(ordenPorPosicion), function (index) { this.Posicion = index; });
            $.each(escenariosOpcionesUsuCarpetaAnterior.sort(ordenPorPosicion), function (index) { this.Posicion = index; });
            opcionesUsu = JSON.stringify({ tipoOpcion: 1, oOpcionesUsu: JSON.stringify($.merge($.merge([], escenariosOpcionesUsuCarpetaActual), escenariosOpcionesUsuCarpetaAnterior)) });
            break;
        case 'Insert_Escenario_Filtro':
        case 'Update_Escenario_Filtro':
            opcionesUsu = JSON.stringify({ tipoOpcion: 2, oOpcionesUsu: JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioFiltros) });
            break;
        default: //'Insert_Escenario_Vista','Update_Escenario_Vista':
            opcionesUsu = JSON.stringify({ tipoOpcion: 3, oOpcionesUsu: JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioVistas) });
            break;
    };
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Establecer_Opciones_Usu',
        contentType: "application/json; charset=utf-8",
        data: opcionesUsu,
        dataType: "json",
        async: true
    })).done(function (msg) {
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/' + url,
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: true
        })).done(function (msg) {
            OcultarCargando();
            switch (url) {
                case 'Insert_Escenario':
                case 'Update_Escenario':
                    escenarioEdicion = $.parseJSON(msg.d);
                    escenarios[escenarioEdicion.Id] = $.parseJSON(msg.d);
                    Seleccionar_Carpeta(escenarioEdicion.Carpeta);
                    idEscenarioSeleccionado = escenarioEdicion.Id;
                    if (escenarioEdicion.Defecto) {
                        //Quitamos el flag Defecto de todos los demás escenarios
                        $.each(escenarios, function () {
                            if (this.Id != idEscenarioSeleccionado)
                                this.Defecto = false;
                        });
                    };
                    Seleccionar_Escenario(idEscenarioSeleccionado);
                    Crear_Filtros();
                    if (url == 'Insert_Escenario') {
                        for (var first in escenarios[idEscenarioSeleccionado].EscenarioFiltros) break;
                        idFiltroSeleccionado = first;
                    };
                    Seleccionar_Filtro(idFiltroSeleccionado);
                    Cargar_Campos_Condiciones_Filtro();
                    Crear_Vistas();
                    if (url == 'Insert_Escenario') {
                        for (var first in escenarios[idEscenarioSeleccionado].EscenarioVistas) break;
                        idVistaSeleccionado = first;
                    };
                    Seleccionar_Vista(idVistaSeleccionado);
                    if (url == 'Insert_Escenario') Cargar_Solicitudes();
                    $('#popupFondo').hide();
                    OcultarCargando();
                    break;
                case 'Insert_Escenario_Filtro':
                case 'Update_Escenario_Filtro':
                    var filtroEditado = $.parseJSON(msg.d);
                    escenarios[filtroEditado.Escenario].EscenarioFiltros[filtroEditado.Id] = filtroEditado;
                    Crear_Filtros();
                    Seleccionar_Filtro(filtroEditado.Id);
                    Cargar_Campos_Condiciones_Filtro();
                    Cargar_Solicitudes();
                    break;
                default:
                    var vistaEditado = $.parseJSON(msg.d);
                    escenarios[vistaEditado.Escenario].EscenarioVistas[vistaEditado.Id] = vistaEditado;
                    Crear_Vistas();
                    Seleccionar_Vista(vistaEditado.Id);
                    Cargar_Solicitudes();
                    break;
            };
            $('#popupFondo').hide();
        });
    });
};
function Establecer_Textos_Pantalla() {
    $('#lblWizardEscenarioDefecto').text(TextosPantalla[1]);
    $('#lblWizardEscenarioFavorito').text(TextosPantalla[2]);
    $('#lblWizardEscenarioNombreEscenario').text(TextosPantalla[3]);
    $('#lblWizardEscenarioStepInfo').text(TextosPantalla[4]);
    $('#lblWizardEscenarioStepOf').text(TextosPantalla[5]);
    $('#lblWizarEscenarioTipoSolicitud').text(TextosPantalla[6]);
    $('#lblWizardEscenarioTodasSolicitudes').text(TextosPantalla[7]);
    $('#lblWizardEscenarioSolicitudes').text(TextosPantalla[8]);
    $('#lblWizardEscenarioCarpeta').text(TextosPantalla[9]);
    $('#lblWizardEscenarioCarpetaSeleccionada').text(TextosPantalla[10]);
    $('#btnWizardEscenarioCancelar span').text(TextosPantalla[11]);
    $('#btnWizardEscenarioStepUp span').text(TextosPantalla[12]);
    $('#btnWizardEscenarioStepDown span').text(TextosPantalla[13]);
    $('#btnWizardEscenarioEnd span').text(TextosPantalla[14]);
    $('#lblWizardEscenarioTituloFiltro').text(TextosPantalla[15]);
    $('#lblWizardEscenarioFiltroNombre').text(TextosPantalla[16]);
    $('#lblWizardEscenarioFiltroSeleccionCampos').text(TextosPantalla[17]);
    $('#lblWizardEscenarioFiltroCamposGenerales').text(TextosPantalla[18]);
    $('#lblWizardEscenarioVistaCamposGenerales').text(TextosPantalla[18]);
    $('#lblWizardEscenarioFiltroCamposFormulario').text(TextosPantalla[19]);
    $('#lblWizardEscenarioVistaCamposFormulario').text(TextosPantalla[19]);
    $('#btnWizardFiltroConfigAvanzada').text(TextosPantalla[20]);
    $('#btnWizardFiltroConfigBasica').text(TextosPantalla[21]);
    $('#lblWizardEscenarioFiltroTituloBasico').text(TextosPantalla[22]);
    $('#lblWizardEscenarioFiltroDefecto').text(TextosPantalla[26]);
    $('#lblWizarEscenarioInfoFiltro').text(TextosPantalla[28]);
    $('#btnWizardEscenarioLimpiarFormula').text(TextosPantalla[29]);
    $('#btnWizardEscenarioOperadorY').text(TextosPantalla[30]);
    $('#btnWizardEscenarioOperadorO').text(TextosPantalla[31]);
    $('#lblWizardEscenarioVistaDefecto').text(TextosPantalla[32]);
    $('#lblWizardEscenarioTituloVista').text(TextosPantalla[33]);
    $('#lblWizardEscenarioVistaNombre').text(TextosPantalla[34]);
    $('#lblWizarEscenarioInfoVista').text(TextosPantalla[35]);
};
function Cambiar_Step_Wizard(step) {
    stepWizardEscenario = step;
    $('[id^=wizardEscenarioStep]').hide();
    $('#lblWizardEscenarioStepNumber').text(stepWizardEscenario);

    switch (step) {
        case 1:
            $('#txtWizardEscenarioNombre').show();
            $('#lblWizardEscenarioNombre').hide();
            $('#btnWizardEscenarioStepDown').invisible();
            $('#btnWizardEscenarioStepUp').css('display', 'inline-block');
            $('#btnWizardEscenarioEnd').hide();
            switch (totalStepsWizardEscenario) {
                case 1: //VISTA
                    $('#wizardEscenarioStep4').show();
                    $('#btnWizardEscenarioStepUp').hide();
                    $('#btnWizardEscenarioEnd').css('display', 'inline-block');
                    break;
                case 2: //FILTRO
                    $('#wizardEscenarioStep2').show();
                    break;
                case 4: //ESCENARIO
                    $('#wizardEscenarioStep1').show();
                    break;
            };
            break;
        case 2:
            $('#txtWizardEscenarioNombre').hide();
            $('#lblWizardEscenarioNombre').show();
            $('#btnWizardEscenarioStepDown').visible();
            $('#btnWizardEscenarioStepUp').css('display', 'inline-block');
            $('#btnWizardEscenarioEnd').hide();
            switch (totalStepsWizardEscenario) {
                case 2: //FILTRO
                    $('#wizardEscenarioStep3').show();
                    $('#btnWizardEscenarioStepUp').hide();
                    $('#btnWizardEscenarioEnd').css('display', 'inline-block');
                    $('#divWizardEscenarioFormula').empty();
                    if ($('#wizardEscenarioStep3').data('formulaAvanzada')) Cargar_Formula_Avanzada();
                    break;
                case 4: //ESCENARIO
                    $('#wizardEscenarioStep2').show();
                    $('#txtWizardEscenarioFiltroNombre').focus();
                    break;
            };
            break;
        case 3:
            $('#txtWizardEscenarioNombre').hide();
            $('#lblWizardEscenarioNombre').show();
            $('#wizardEscenarioStep3').show();
            $('#btnWizardEscenarioStepDown').visible();
            $('#btnWizardEscenarioStepUp').css('display', 'inline-block');
            $('#btnWizardEscenarioEnd').hide();
            if (formulaFiltro.FormulaCondiciones) {
                $('#divWizardEscenarioFormula').empty();
                Cargar_Formula_Avanzada();
            };
            break;
        case 4:
            var idSolicitudFormulario = 0;
            if (!escenarioEdicion.AplicaTodas) {
                for (var key in escenarioEdicion.SolicitudFormularioVinculados) break;
                idSolicitudFormulario = escenarioEdicion.SolicitudFormularioVinculados[key].Key;
            };
            Cargar_Campos_Formulario(idSolicitudFormulario, 3);
            $('#txtWizardEscenarioNombre').hide();
            $('#lblWizardEscenarioNombre').show();
            $('#wizardEscenarioStep4').show();
            $('#btnWizardEscenarioStepDown').visible();
            $('#btnWizardEscenarioStepUp').hide();
            $('#btnWizardEscenarioEnd').css('display', 'inline-block');
            $('#txtWizardEscenarioVistaNombre').focus();
            break;
    };
};
function Cargar_Formula_Avanzada() {
    if (formulaFiltro.FormulaCondiciones.length !== 0) {
        var agregarSentenciaAnd = false;
        $.each(formulaFiltro.FormulaCondiciones, function () {
            $('#bloqueFormulaAvanzada').tmpl(this).appendTo($('#divWizardEscenarioFormula'));
            $('#bloqueFormulaAvanzada_Condicion_' + this.Orden).data('formulaCondicion', this);
        });
    };
};
/*''' <summary>
''' Cargar Campos Formulario
''' </summary>
''' <param name="idFormularioVinculado">id Formulario</param>
''' <param name="Paso">Donde estas</param>        
''' <remarks>Llamada desde: Todos los bts de escenarios de este js; Tiempo mÃ¡ximo: 0</remarks>*/
function Cargar_Campos_Formulario(idFormularioVinculado, Paso) {    
    if (idFormularioVinculado == 0) {
        $('#lCamposFormulario').empty();
        $('#fsWizardEscenarioCamposFormulario').hide();
        $('#fsWizardEscenarioVistaCamposFormulario').hide();
    } else {
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Campos_Formulario',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ IdFormulario: idFormularioVinculado, Paso: Paso }),
            dataType: "json",
            async: false
        })).done(function (msg) {
            //Obtenemos dos elementos, el primero un dictionary con los campos del formulario y el segundo un list con los mismos elementos.
            //Lo hacemos asi para no perder el orden en el que apareceran en el arbol (lista) y para poder acceder a ellos por la key(dictionary)
            campos_formulario = msg.d;
            if (Paso != 3) {
                $('#lCamposFormulario').empty();
            }
            if ((bEditarVista == true) || (Paso != 3)) {
                $('#lCamposFormularioVista').empty();
            }

            $('#fsWizardEscenarioCamposFormulario').show();
            $('#fsWizardEscenarioVistaCamposFormulario').show();

            var campos_formularioAUX = JSON.parse(JSON.stringify(campos_formulario));
            var listaCamposFormularioFiltro = $.map(campos_formularioAUX, function (campo_formulario) {
                if (campo_formulario.TipoCampo !== 8 && campo_formulario.TipoCampo !== 15) {
                    campo_formulario.CamposDesglose = $.map(campo_formulario.CamposDesglose, function (campo_desglose) { if (campo_desglose.TipoCampo !== 8) { campo_desglose.CamposDesglose = []; return campo_desglose; } }).sort(function (a, b) { return a.OrdenFormulario - b.OrdenFormulario });
                    return campo_formulario;
                }
            });
            campos_formularioAUX = JSON.parse(JSON.stringify(campos_formulario));
            var listaCamposFormularioVista = $.map(campos_formularioAUX, function (campo_formulario) {
                campo_formulario.CamposDesglose = $.map(campo_formulario.CamposDesglose, function (campo_desglose) { campo_desglose.CamposDesglose = []; return campo_desglose; }).sort(function (a, b) { return a.OrdenFormulario - b.OrdenFormulario });
                return campo_formulario;
            });
            listaCamposFormularioFiltro = listaCamposFormularioFiltro.sort(function (a, b) { return a.OrdenFormulario - b.OrdenFormulario });
            listaCamposFormularioVista = listaCamposFormularioVista.sort(function (a, b) { return a.OrdenFormulario - b.OrdenFormulario });
            if (Paso != 3) {
                $('#campoFormulario').tmpl(listaCamposFormularioFiltro).appendTo($('#lCamposFormulario'));
            }
            if ((bEditarVista == true) || (Paso != 3)) {
                $('#campoFormularioVista').tmpl(listaCamposFormularioVista).appendTo($('#lCamposFormularioVista'));
            }
            $('[id^=txtEscenarioVistaCampoNombrePersonalizado_]').prop('placeholder', TextosPantalla[27]);
        });
    };
};
function Cargar_CamposFiltro() {
    if ($('#basicWizardEscenarioStep3').is(':visible')) Cargar_CamposFiltro_Basico();
    else Cargar_CamposFiltro_Avanzado();
};
function Cargar_CamposFiltro_Basico() {
    var selectedValues;
    var campos_Filtro_Seleccionados = $('#lCamposGenerales input[type=checkbox]:checked').closest('li').toArray().concat($('#lCamposFormulario input[type=checkbox]:checked').closest('li').toArray());
    var selectedValuesCamposGenerales = {}, selectedValuesCamposFormulario = {};
    var lValoresListaCamposGenerales = {}, lValoresListaCamposFormulario = {};
    var esGeneral;
    if (formulaFiltro.FormulaCondiciones) {
        $.each(formulaFiltro.FormulaCondiciones, function () {
            if (this.EsCampoGeneral) selectedValuesCamposGenerales[this.IdCampo] = this.Valores;
            else selectedValuesCamposFormulario[this.IdCampo] = this.Valores;
        });
    };
    $.each($('#lWizardEscenarioListaCamposFiltroBasico [id^=selectCampoFiltro_Lista_]'), function () {
        esGeneral = ($(this).attr('id').indexOf('Formulario_') == -1);
        if (this.options.length > 0)
            if (esGeneral) lValoresListaCamposGenerales[$(this).closest('fieldset').attr('id').split('_')[2]] = $('#' + $(this).attr('id') + ' option');
            else lValoresListaCamposFormulario[$(this).closest('fieldset').attr('id').split('_')[2]] = $('#' + $(this).attr('id') + ' option');
    });
    $('#lWizardEscenarioListaCamposFiltroBasico').empty();
    var id_Campo_Seleccionado, id_Campo_Desglose;
    var campo_Formulario_Seleccionado;
    $.each(campos_Filtro_Seleccionados, function () {
        selectedValues = null;
        id_Campo_Seleccionado = parseInt($(this).attr('id').split('_')[1]);
        if ($(this).parents('ul[id^=camposWizardEscenarioFormularioFiltro_Desglose_]').length == 0) id_Campo_Desglose = 0;
        else id_Campo_Desglose = parseInt($(this).parents('ul[id^=camposWizardEscenarioFormularioFiltro_Desglose_]').attr('id').split('_')[2]);
        if ($(this).parents('#lCamposGenerales').length == 0) {
            if (id_Campo_Desglose == 0) campo_Formulario_Seleccionado = campos_formulario[id_Campo_Seleccionado];
            else campo_Formulario_Seleccionado = campos_formulario[id_Campo_Desglose].CamposDesglose[id_Campo_Seleccionado];
        } else {
            campo_Formulario_Seleccionado = campos_generales[id_Campo_Seleccionado];
        };
        $('#escenarioFiltroCampo_Basico').tmpl(campo_Formulario_Seleccionado).appendTo($('#lWizardEscenarioListaCamposFiltroBasico'));
        if (campo_Formulario_Seleccionado.EsCampoGeneral) {
            $('#EscenarioFiltro_CampoGeneral_' + campo_Formulario_Seleccionado.Id).data('selectedValues', selectedValuesCamposGenerales[campo_Formulario_Seleccionado.Id]);
            if (lValoresListaCamposGenerales[campo_Formulario_Seleccionado.Id] == undefined) {
                if (campo_Formulario_Seleccionado.OpcionesLista.length !== 0) $('#EscenarioFiltro_CampoGeneral_' + campo_Formulario_Seleccionado.Id).data('data', campo_Formulario_Seleccionado.OpcionesLista);
            } else $('#EscenarioFiltro_CampoGeneral_' + campo_Formulario_Seleccionado.Id).data('data', lValoresListaCamposGenerales[campo_Formulario_Seleccionado.Id]);
            $('#EscenarioFiltro_CampoGeneral_' + campo_Formulario_Seleccionado.Id).data('campoEscenario', campo_Formulario_Seleccionado);
        } else {
            $('#EscenarioFiltro_CampoFormulario_' + campo_Formulario_Seleccionado.Id + (campo_Formulario_Seleccionado.EsCampoDesglose ? '_' + campo_Formulario_Seleccionado.CampoDesglose : '')).data('selectedValues', selectedValuesCamposFormulario[campo_Formulario_Seleccionado.Id]);
            if (lValoresListaCamposFormulario[campo_Formulario_Seleccionado.Id] == undefined) {
                if (campo_Formulario_Seleccionado.OpcionesLista.length !== 0)
                    $('#EscenarioFiltro_CampoFormulario_' + campo_Formulario_Seleccionado.Id + (campo_Formulario_Seleccionado.EsCampoDesglose ? '_' + campo_Formulario_Seleccionado.CampoDesglose : '')).data('data', campo_Formulario_Seleccionado.OpcionesLista);
            } else $('#EscenarioFiltro_CampoFormulario_' + campo_Formulario_Seleccionado.Id + (campo_Formulario_Seleccionado.EsCampoDesglose ? '_' + campo_Formulario_Seleccionado.CampoDesglose : '')).data('data', lValoresListaCamposFormulario[campo_Formulario_Seleccionado.Id]);
            $('#EscenarioFiltro_CampoFormulario_' + campo_Formulario_Seleccionado.Id + (campo_Formulario_Seleccionado.EsCampoDesglose ? '_' + campo_Formulario_Seleccionado.CampoDesglose : '')).data('campoEscenario', campo_Formulario_Seleccionado);
        };
    });
    //tipo string
    $.each($('[id^=txtCampoFiltro_String_]'), function () {
        $(this).val($(this).closest('fieldset').data('selectedValues'));
    });
    //tipo lista
    $.each($('[id^=selectCampoFiltro_Lista_]'), function () {
        var opt;
        var elementoLista = $(this);
        var sel = $(this).multiselect({
            multiple: ($(this).closest('fieldset').data('campoEscenario').TipoCampo !== 4 && $(this).closest('fieldset').data('campoEscenario').TipoCampoGS !== 107 && $(this).closest('fieldset').data('campoEscenario').TipoCampoGS !== 108),
            header: ($(this).closest('fieldset').data('campoEscenario').TipoCampo !== 4),
            selectedList: 5,
            noneSelectedText: '',
            selectedText: TextosPantalla[67],
            checkAllText: TextosPantalla[65],
            uncheckAllText: TextosPantalla[66],
            click: function () {
                if ($(this).closest('fieldset').data('campoEscenario').TipoCampoGS == 107) {
                    $('#' + elementoLista.attr('id').replace(elementoLista.attr('id').split('_')[elementoLista.attr('id').split('_').length - 1], elementoLista.closest('fieldset').data('campoEscenario').CampoHijo)).closest('fieldset').removeData('data');
                    $('#' + elementoLista.attr('id').replace(elementoLista.attr('id').split('_')[elementoLista.attr('id').split('_').length - 1], elementoLista.closest('fieldset').data('campoEscenario').CampoHijo)).multiselect('uncheckAll')
                };
            },
            beforeopen: function () {
                if ($(this).closest('fieldset').data('data') == undefined) {
                    var el = this;
                    if (elementoLista.closest('fieldset').data('campoEscenario').TipoCampoGS == 108) {
                        var valorPais = $('#' + elementoLista.attr('id').replace(elementoLista.attr('id').split('_')[elementoLista.attr('id').split('_').length - 1], elementoLista.closest('fieldset').data('campoEscenario').CampoPadre)).multiselect('getChecked');
                        if (valorPais.length == 0) valorCampoPadre = '';
                        else valorCampoPadre = valorPais[0].value;
                    } else valorCampoPadre = '';
                    $.when($.ajax({
                        type: "POST",
                        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Opciones_Lista',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({ Campo_Escenario: JSON.stringify(elementoLista.closest('fieldset').data('campoEscenario')), Escenario: JSON.stringify(escenarioEdicion), ValorCampoPadre: valorCampoPadre, iTipoVisor: tipoVisor }),
                        async: false
                    })).done(function (msg) {
                        var lOpcionesLista = msg.d;
                        $(el).children('option').remove();
                        var selValues = elementoLista.closest('fieldset').data('selectedValues');
                        if (selValues !== undefined) selValues = selValues.map(function (x) { return x.split("###")[0] });
                        $.each(lOpcionesLista, function () {
                            opt = $('<option />', {
                                value: escape(this.value),
                                text: this.text
                            });
                            //Seleccionamos la opción si es necesario
                            if (selValues !== undefined && $.inArray(opt.val(), selValues) != -1) {
                                opt.attr('selected', 'selected');
                            }
                            opt.appendTo(el);
                        });
                        elementoLista.closest('fieldset').data('data', $('#' + elementoLista.attr('id') + ' option'));
                        elementoLista.multiselect('refresh');
                        if (selValues == undefined) sel.multiselect('uncheckAll');
                    });
                };
            }
        });
        if ($(this).closest('fieldset').data('campoEscenario').TipoCampo !== 4) sel.multiselectfilter({ label: TextosPantalla[63], placeholder: TextosPantalla[64] });
        if (elementoLista.closest('fieldset').data('data') !== undefined) {
            if (elementoLista.closest('fieldset').data('selectedValues') !== undefined)
                var valoresSeleccion = jQuery.map(elementoLista.closest('fieldset').data('selectedValues'), function (x) { return (x.split("###")[0]); });

            $.each(elementoLista.closest('fieldset').data('data'), function () {
                opt = $('<option />', {
                    value: this.value,
                    text: this.text
                });
                if ($.inArray($(opt).val(), valoresSeleccion) !== -1) opt.attr('selected', 'selected');

                opt.appendTo(sel);
            });
        } else if (elementoLista.closest('fieldset').data('selectedValues') !== undefined) {
            //Si hay valores seleccionados, cargamos sólo estas opciones de momento
            $.each(elementoLista.closest('fieldset').data('selectedValues'), function () {
                opt = $('<option />', {
                    value: this.split('###')[0],
                    text: this.split('###')[1]
                });
                opt.attr('selected', 'selected');
                opt.appendTo(sel);
            });
        }
        sel.multiselect('refresh');
    });
    //tipo numerico    
    $('input[id^=txtCampoFiltro_Numerico_]').numeric({ decimal: '' + usuario.DecimalFmt + '', negative: false }).numericFormatted({
        decimalSeparator: UsuNumberDecimalSeparator,
        groupSeparator: UsuNumberGroupSeparator,
        decimalDigits: UsuNumberNumDecimals
    });
    //Los campos de Tipo Identificador no van formateados
    $('input[id^=txtCampoFiltro_Numerico_][id$=_' + CAMPOSGENERALES.IDENTIFICADOR + ']').numericFormatted({
        groupSeparator: "",
        decimalDigits: 0
    });
    $.each($('[id^=txtCampoFiltro_Numerico_]'), function () {
        var elementoNumero = $(this);
        var idCampo = elementoNumero.attr('id').substring(elementoNumero.attr('id').lastIndexOf('_') + 1);
        var selValues = elementoNumero.closest('fieldset').data('selectedValues');
        if (selValues !== undefined) {
            var condicion = $.grep(formulaFiltro.FormulaCondiciones, function (x) { return x.IdCampo == idCampo })[0];
            if (condicion.Operador != OPERADORES.ENTRE) {
                //Si no se rellenan los 2 valores Desde/Hasta, se asigna el único valor al textbox que corresponde
                if ((condicion.Operador == OPERADORES.MAYORIGUAL && elementoNumero.attr('id').indexOf('Desde') != -1) ||
                    (condicion.Operador == OPERADORES.MENORIGUAL && elementoNumero.attr('id').indexOf('Hasta') != -1))
                    elementoNumero.numericFormatted('val', parseFloat(selValues[0]));
            } else
                elementoNumero.numericFormatted('val', parseFloat(selValues[(elementoNumero.attr('id').indexOf('Desde') !== -1 ? 0 : 1)]));
        }
    });
    //tipo fecha    
    $('[id^=txtCampoFiltro_Fecha_Desde_],[id^=txtCampoFiltro_Fecha_Hasta_]').inputmask(UsuMask.replace('MM', 'mm')).datepicker({
        showOn: 'both',
        buttonImage: ruta + 'images/colorcalendar.png',
        buttonImageOnly: true,
        buttonText: '',
        dateFormat: UsuMask.replace('MM', 'mm').replace('yyyy', 'yy'),
        showAnim: 'slideDown'
    });
    $.datepicker.setDefaults($.datepicker.regional[UsuLanguageTag]);
    CargarOpcionesFechasRelativas($('#lWizardEscenarioListaCamposFiltroBasico select[tipocombofecha=1]'));
    CargarOpcionesFechasPeriodo($('#lWizardEscenarioListaCamposFiltroBasico select[tipocombofecha=2]'));
    $('[tipocombofecha=1]').siblings('span').text(TextosPantalla[87]);
    //Establecemos la validación numérica para el textbox del modo "HACE" de la fecha
    $('img[name=ModoFecha]').siblings('div[index=1]').find('input:first').numeric({ decimal: false, negative: false });
    $.each($('[id^=txtCampoFiltro_Fecha_Desde]'), function () {
        var elementoFecha = $(this);
        var selValues = elementoFecha.closest('fieldset').data('selectedValues');
        if (selValues) {
            var idCampo = elementoFecha.attr('id').substring(elementoFecha.attr('id').lastIndexOf('_') + 1);
            var condicion = $.grep(formulaFiltro.FormulaCondiciones, function (x) { return x.IdCampo == idCampo })[0];
            var contenedorCondicionFiltro = $(this).parents('[id^=EscenarioFiltro_]');
            contenedorCondicionFiltro.find('div[index]').removeClass('inlineBlock');
            contenedorCondicionFiltro.find('div[index]').addClass('Ocultar');
            switch (condicion.Operador) {
                case OPERADORES.HACE: //Hace
                    $('img[name=ModoFecha]', contenedorCondicionFiltro).attr('data-type', 1);
                    contenedorCondicionFiltro.find('div[index=1]').removeClass('Ocultar');
                    contenedorCondicionFiltro.find('div[index=1]').addClass('inlineBlock');
                    contenedorCondicionFiltro.find('div[index=1] input').val(selValues[1]);
                    contenedorCondicionFiltro.find('div[index=1] select.CabeceraBotones option:[value=' + selValues[0] + ']').prop('selected', true);
                    break;
                case OPERADORES.PERIODO: //Periodo
                    $('img[name=ModoFecha]', contenedorCondicionFiltro).attr('data-type', 2);
                    contenedorCondicionFiltro.find('div[index=2]').removeClass('Ocultar');
                    contenedorCondicionFiltro.find('div[index=2]').addClass('inlineBlock');
                    contenedorCondicionFiltro.find('div[index=2] select.CabeceraBotones option:[value=' + selValues[0] + ']').prop('selected', true);
                    break;
                case OPERADORES.ENTRE:
                    contenedorCondicionFiltro.find('div[index=0] span').show();
                    contenedorCondicionFiltro.find('div[index=0] div').visible();
                default:
                    $('img[name=ModoFecha]', contenedorCondicionFiltro).attr('data-type', 0);
                    contenedorCondicionFiltro.find('div[index=0]').removeClass('Ocultar');
                    contenedorCondicionFiltro.find('div[index=0]').addClass('inlineBlock');
                    break;
            };
            //Establecemos el valor al combo de operadores
            var cboOperadores = $('#EscenarioFiltro_Campo' + (condicion.EsCampoGeneral ? 'General_' : 'Formulario_') + condicion.IdCampo + ' select.CabeceraBotones');
            cboOperadores.find('option[value=' + condicion.Operador + ']').prop('selected', true);
            //Forzamos el evento de cambio del combo de operadores
            cboOperadores.trigger('change');
        };
    });
    $.each($('[id^=txtCampoFiltro_Fecha_]'), function () {
        var elementoFecha = $(this);
        var idCampo = elementoFecha.attr('id').substring(elementoFecha.attr('id').lastIndexOf('_') + 1);
        var selValues = elementoFecha.closest('fieldset').data('selectedValues');
        if (selValues !== undefined) {
            var condicion = $.grep(formulaFiltro.FormulaCondiciones, function (x) { return x.IdCampo == idCampo })[0];
            if (condicion.Operador != OPERADORES.ENTRE) {
                //Si no se rellenan los 2 valores Desde/Hasta, se asigna el único valor al textbox que corresponde
                if ((condicion.Operador == OPERADORES.MAYORIGUAL && elementoFecha.attr('id').indexOf('Desde') != -1) ||
                    (condicion.Operador == OPERADORES.MENORIGUAL && elementoFecha.attr('id').indexOf('Hasta') != -1))
                    elementoFecha.datepicker('setDate', new Date(Date.parse(elementoFecha.closest('fieldset').data('selectedValues')[0])));
            } else
                elementoFecha.datepicker('setDate', new Date(Date.parse(selValues[(elementoFecha.attr('id').indexOf('Desde') !== -1 ? 0 : 1)])));
        }
    });
    $('[lblDesde]').text(TextosPantalla[23]);
    $('[lblHasta]').text(TextosPantalla[24]);
    //Proveedor
    $.each($('[id^=txtCampoFiltro_Proveedor_]'), function () {
        if ($(this).closest('fieldset').data('selectedValues') !== undefined) {
            $(this).val($(this).closest('fieldset').data('selectedValues')[1]);
            $(this).siblings('[id^=txtCampoFiltro_ProveedorValue]').val($(this).closest('fieldset').data('selectedValues')[0]);
        };
    });
    //Material
    $.each($('[id^=txtCampoFiltro_Material_]'), function () {
        if ($(this).closest('fieldset').data('selectedValues') !== undefined) {
            $(this).val($(this).closest('fieldset').data('selectedValues')[1]);
            $(this).siblings('[id^=txtCampoFiltro_MaterialValue]').val($(this).closest('fieldset').data('selectedValues')[0]);
        };
    });
    //Articulo
    $.each($('[id^=txtCampoFiltro_Articulo_]'), function () {
        if ($(this).closest('fieldset').data('selectedValues') !== undefined) {
            $(this).val($(this).closest('fieldset').data('selectedValues')[1]);
            $(this).siblings('[id^=txtCampoFiltro_ArticuloValue]').val($(this).closest('fieldset').data('selectedValues')[0]);
        };
    });
    //Peticionario
    $.each($('[id^=txtCampoFiltro_Peticionario_]'), function () {
        if ($(this).closest('fieldset').data('selectedValues') !== undefined) {
            $(this).val($(this).closest('fieldset').data('selectedValues')[1]);
            $(this).siblings('[id^=txtCampoFiltro_PeticionarioValue]').val($(this).closest('fieldset').data('selectedValues')[0]);
        };
    });
    //Usuario    
    $.each($('[id^=txtCampoFiltro_Usuario_]'), function () {
        if ($(this).closest('fieldset').data('selectedValues') !== undefined) {
            $(this).val($(this).closest('fieldset').data('selectedValues')[1]);
            $(this).siblings('[id^=txtCampoFiltro_UsuarioValue]').val($(this).closest('fieldset').data('selectedValues')[0]);
        };
    });
    //Comprador
    $.each($('[id^=txtCampoFiltro_Comprador_]'), function () {
        if ($(this).closest('fieldset').data('selectedValues') !== undefined) {
            $(this).val($(this).closest('fieldset').data('selectedValues')[1]);
            $(this).siblings('[id^=txtCampoFiltro_CompradorValue]').val($(this).closest('fieldset').data('selectedValues')[0]);
        };
    });
    //Persona
    $.each($('[id^=txtCampoFiltro_Persona_]'), function () {
        if ($(this).closest('fieldset').data('selectedValues') !== undefined) {
            $(this).val($(this).closest('fieldset').data('selectedValues')[1]);
            $(this).siblings('[id^=txtCampoFiltro_PersonaValue]').val($(this).closest('fieldset').data('selectedValues')[0]);
        };
    });
    //Unidad organizativa
    $.each($('[id^=txtCampoFiltro_UnidadOrganizativa_]'), function () {
        if ($(this).closest('fieldset').data('selectedValues') !== undefined) {
            $(this).val($(this).closest('fieldset').data('selectedValues')[1]);
            $(this).siblings('[id^=txtCampoFiltro_UnidadOrganizativaValue]').val($(this).closest('fieldset').data('selectedValues')[0]);
        };
    });
    //Empresa
    $.each($('[id^=txtCampoFiltro_Empresa_]'), function () {
        if ($(this).closest('fieldset').data('selectedValues') !== undefined) {
            $(this).val($(this).closest('fieldset').data('selectedValues')[1]);
            $(this).siblings('[id^=txtCampoFiltro_EmpresaValue]').val($(this).closest('fieldset').data('selectedValues')[0]);
        };
    });
};
function CargarOpcionesFechasRelativas(combo) {
    var j = 0;
    combo.children('option').remove();
    for (i = 76; i < 81; i++) {
        opt = $('<option />', {
            value: j,
            text: TextosPantalla[i]
        });
        opt.appendTo(combo);
        j++;
    };
};
function CargarOpcionesFechasPeriodo(combo) {
    var j = 0;
    combo.children('option').remove();
    for (i = 81; i < 87; i++) {
        opt = $('<option />', {
            value: j,
            text: TextosPantalla[i]
        });
        opt.appendTo(combo);
        j++;
    };
};
function Cargar_CamposFiltro_Avanzado() {
    var selectedValues;
    var campos_Filtro_Seleccionados = $('#lCamposGenerales input[type=checkbox]:checked').closest('li').toArray().concat($('#lCamposFormulario input[type=checkbox]:checked').closest('li').toArray());
    var selectedValuesCamposGenerales = {}, selectedValuesCamposFormulario = {};
    var lValoresListaCamposGenerales = {}, lValoresListaCamposFormulario = {};
    var esGeneral;
    if (formulaFiltro.FormulaCondiciones) {
        $.each(formulaFiltro.FormulaCondiciones, function () {
            if (this.EsCampoGeneral) selectedValuesCamposGenerales[this.IdCampo] = this.Valores;
            else selectedValuesCamposFormulario[this.IdCampo] = this.Valores;
        });
    };
    $.each($('#lWizardEscenarioListaCamposFiltroAvanzado [id^=selectCampoFiltroAvanzado_Lista_]'), function () {
        esGeneral = ($(this).attr('id').indexOf('Formulario_') == -1);
        if (esGeneral) if (this.options.length > 0) lValoresListaCamposGenerales[$(this).closest('fieldset').attr('id').split('_')[2]] = $('#' + $(this).attr('id') + ' option');
        else if (this.options.length > 0) lValoresListaCamposFormulario[$(this).closest('fieldset').attr('id').split('_')[2]] = $('#' + $(this).attr('id') + ' option');
    });
    $('#lWizardEscenarioListaCamposFiltroAvanzado').empty();
    var id_Campo_Seleccionado, id_Campo_Desglose;
    var campo_Formulario_Seleccionado;
    $.each(campos_Filtro_Seleccionados, function () {
        selectedValues = null;
        id_Campo_Seleccionado = parseInt($(this).attr('id').split('_')[1]);
        if ($(this).parents('ul[id^=camposWizardEscenarioFormularioFiltro_Desglose_]').length == 0) id_Campo_Desglose = 0;
        else id_Campo_Desglose = parseInt($(this).parents('ul[id^=camposWizardEscenarioFormularioFiltro_Desglose_]').attr('id').split('_')[2]);
        if ($(this).parents('#lCamposGenerales').length == 0) {
            if (id_Campo_Desglose == 0) campo_Formulario_Seleccionado = campos_formulario[id_Campo_Seleccionado];
            else campo_Formulario_Seleccionado = campos_formulario[id_Campo_Desglose].CamposDesglose[id_Campo_Seleccionado];
        } else {
            campo_Formulario_Seleccionado = campos_generales[id_Campo_Seleccionado];
        };
        $('#escenarioFiltroCampo_Avanzado').tmpl(campo_Formulario_Seleccionado).appendTo($('#lWizardEscenarioListaCamposFiltroAvanzado'));
        if (campo_Formulario_Seleccionado.EsCampoGeneral) {
            if (lValoresListaCamposGenerales[campo_Formulario_Seleccionado.Id] == undefined) {
                if (campo_Formulario_Seleccionado.OpcionesLista.length !== 0) $('#EscenarioFiltroAvanzado_CampoGeneral_' + campo_Formulario_Seleccionado.Id).data('data', campo_Formulario_Seleccionado.OpcionesLista);
            } else $('#EscenarioFiltroAvanzado_CampoGeneral_' + campo_Formulario_Seleccionado.Id).data('data', lValoresListaCamposGenerales[campo_Formulario_Seleccionado.Id]);
            $('#EscenarioFiltroAvanzado_CampoGeneral_' + campo_Formulario_Seleccionado.Id).data('campo', campo_Formulario_Seleccionado);
        } else {
            if (lValoresListaCamposFormulario[campo_Formulario_Seleccionado.Id] == undefined) {
                if (campo_Formulario_Seleccionado.OpcionesLista.length !== 0)
                    $('#EscenarioFiltroAvanzado_CampoFormulario_' + campo_Formulario_Seleccionado.Id + (campo_Formulario_Seleccionado.EsCampoDesglose ? '_' + campo_Formulario_Seleccionado.CampoDesglose : '')).data('data', campo_Formulario_Seleccionado.OpcionesLista);
            } else $('#EscenarioFiltroAvanzado_CampoFormulario_' + campo_Formulario_Seleccionado.Id + (campo_Formulario_Seleccionado.EsCampoDesglose ? '_' + campo_Formulario_Seleccionado.CampoDesglose : '')).data('data', lValoresListaCamposFormulario[campo_Formulario_Seleccionado.Id]);
            $('#EscenarioFiltroAvanzado_CampoFormulario_' + campo_Formulario_Seleccionado.Id + (campo_Formulario_Seleccionado.EsCampoDesglose ? '_' + campo_Formulario_Seleccionado.CampoDesglose : '')).data('campo', campo_Formulario_Seleccionado);
        };
        $('#EscenarioFiltroAvanzado_Campo' + (campo_Formulario_Seleccionado.EsCampoGeneral ? 'General_' : 'Formulario_') + id_Campo_Seleccionado).data('campoEscenario', campo_Formulario_Seleccionado);
        var operadoresString = [];
        var opcionesSelect = $('#EscenarioFiltroAvanzado_Campo' + (campo_Formulario_Seleccionado.EsCampoGeneral ? 'General_' : 'Formulario_') + id_Campo_Seleccionado + ' select.CabeceraBotones').prop('options');
        if (!campo_Formulario_Seleccionado.EsLista) {
            switch (campo_Formulario_Seleccionado.TipoCampoGS) {
                case TIPOSCAMPOGS.USUARIO: //USUARIO
                case TIPOSCAMPOGS.PETICIONARIO: //PETICIONARIO                
                    operadoresString = [OPERADORES.ES, OPERADORES.NOES]; // IN , NOT IN
                    $.each(operadoresString, function () {
                        opcionesSelect[opcionesSelect.length] = new Option(DENOMINACION_OPERADORES[this], this);
                    });
                    break;
                case TIPOSCAMPOGS.PROVEEDORADJ: //PROVEEDOR
                case TIPOSCAMPOGS.PROVEEDOR: //PROVEEDOR
                case TIPOSCAMPOGS.MATERIAL: //MATERIAL                
                case TIPOSCAMPOGS.PAIS: //PAIS
                case TIPOSCAMPOGS.PROVINCIA: //PROVINCIA
                case TIPOSCAMPOGS.PROVECONTACTO: //PROVEEDOR                
                case TIPOSCAMPOGS.PERSONA:
                case TIPOSCAMPOGS.EMPRESA:
                    operadoresString = [OPERADORES.IGUAL, OPERADORES.DISTINTO]; // = , <>
                    $.each(operadoresString, function () {
                        opcionesSelect[opcionesSelect.length] = new Option(DENOMINACION_OPERADORES[this], this);
                    });
                    break;
                case TIPOSCAMPOGS.CODARTICULO: //ARTICULO
                case TIPOSCAMPOGS.DENARTICULO: //ARTICULO
                case TIPOSCAMPOGS.NUEVOCODARTICULO: //ARTICULO 
                    operadoresString = [OPERADORES.IGUAL, OPERADORES.DISTINTO, OPERADORES.CONTIENE, OPERADORES.EMPIEZAPOR, OPERADORES.TERMINAEN];
                    $.each(operadoresString, function () {
                        opcionesSelect[opcionesSelect.length] = new Option(DENOMINACION_OPERADORES[this], this);
                    });
                    break;
                default:
                    switch (campo_Formulario_Seleccionado.TipoCampo) {
                        case 1:
                        case 5:
                        case 6:
                        case 7: //String
                            operadoresString = [OPERADORES.IGUAL, OPERADORES.DISTINTO, OPERADORES.CONTIENE, OPERADORES.EMPIEZAPOR, OPERADORES.TERMINAEN];
                            $.each(operadoresString, function () {
                                opcionesSelect[opcionesSelect.length] = new Option(DENOMINACION_OPERADORES[this], this);
                            });
                            break;
                        case 2: //Fecha
                        case 3: //Numerico
                            operadoresString = [OPERADORES.IGUAL, OPERADORES.DISTINTO, OPERADORES.MAYOR, OPERADORES.MAYORIGUAL, OPERADORES.MENOR, OPERADORES.MENORIGUAL, OPERADORES.ENTRE];
                            $.each(operadoresString, function () {
                                opcionesSelect[opcionesSelect.length] = new Option(DENOMINACION_OPERADORES[this], this);
                            });
                    };
                    break;
            };
        } else {
            operadoresString = [OPERADORES.ES, OPERADORES.NOES];
            $.each(operadoresString, function () {
                opcionesSelect[opcionesSelect.length] = new Option(DENOMINACION_OPERADORES[this], this);
            });
        };
    });
    //tipo lista
    $.each($('[id^=selectCampoFiltroAvanzado_Lista_]'), function () {
        var opt;
        var elementoLista = $(this);
        var sel = $(this).multiselect({
            multiple: ($(this).closest('fieldset').data('campoEscenario').TipoCampo !== 4 && $(this).closest('fieldset').data('campoEscenario').TipoCampoGS !== 107 && $(this).closest('fieldset').data('campoEscenario').TipoCampoGS !== 108),
            header: ($(this).closest('fieldset').data('campoEscenario').TipoCampo !== 4),
            selectedList: 5,
            noneSelectedText: '',
            selectedText: TextosPantalla[67],
            checkAllText: TextosPantalla[65],
            uncheckAllText: TextosPantalla[66],
            click: function () {
                if ($(this).closest('fieldset').data('campoEscenario').TipoCampoGS == 107) {
                    $('#' + elementoLista.attr('id').replace(elementoLista.attr('id').split('_')[elementoLista.attr('id').split('_').length - 1], elementoLista.closest('fieldset').data('campoEscenario').CampoHijo)).closest('fieldset').removeData('data');
                    $('#' + elementoLista.attr('id').replace(elementoLista.attr('id').split('_')[elementoLista.attr('id').split('_').length - 1], elementoLista.closest('fieldset').data('campoEscenario').CampoHijo)).multiselect('uncheckAll')
                };
            },
            beforeopen: function () {
                if ($(this).closest('fieldset').data('data') == undefined) {
                    var el = this;
                    if (elementoLista.closest('fieldset').data('campoEscenario').TipoCampoGS == 108) {
                        var valorPais = $('#' + elementoLista.attr('id').replace(elementoLista.attr('id').split('_')[elementoLista.attr('id').split('_').length - 1], elementoLista.closest('fieldset').data('campoEscenario').CampoPadre)).multiselect('getChecked');
                        if (valorPais.length == 0) valorCampoPadre = '';
                        else valorCampoPadre = valorPais[0].value;
                    } else valorCampoPadre = '';
                    $.when($.ajax({
                        type: "POST",
                        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Opciones_Lista',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({ Campo_Escenario: JSON.stringify(elementoLista.closest('fieldset').data('campoEscenario')), Escenario: JSON.stringify(escenarioEdicion), ValorCampoPadre: valorCampoPadre, iTipoVisor: tipoVisor }),
                        async: false
                    })).done(function (msg) {
                        var lOpcionesLista = msg.d;
                        $(el).children('option').remove();
                        $.each(lOpcionesLista, function () {
                            opt = $('<option />', {
                                value: escape(this.value),
                                text: this.text
                            });
                            opt.appendTo(el);
                        });
                        elementoLista.closest('fieldset').data('data', $('#' + elementoLista.attr('id') + ' option'));
                        elementoLista.multiselect('refresh');
                        if (elementoLista.closest('fieldset').data('selectedValues') == undefined) sel.multiselect('uncheckAll');
                    });
                };
            }
        });
        if ($(this).closest('fieldset').data('campoEscenario').TipoCampo !== 4) sel.multiselectfilter({ label: TextosPantalla[63], placeholder: TextosPantalla[64] });
        if (elementoLista.closest('fieldset').data('data') !== undefined) {
            $.each(elementoLista.closest('fieldset').data('data'), function () {
                opt = $('<option />', {
                    value: this.value,
                    text: this.text
                });
                if (elementoLista.closest('fieldset').data('selectedValues') !== undefined)
                    if ($.inArray($(opt).val(), elementoLista.closest('fieldset').data('selectedValues')) !== -1) opt.attr('selected', 'selected');
                opt.appendTo(sel);
            });
            sel.multiselect('refresh');
        }
    });
    //tipo numerico
    $('input[id^=txtCampoFiltroAvanzado_Numerico_]').numeric({ decimal: '' + usuario.DecimalFmt + '', negative: false }).numericFormatted({
        decimalSeparator: UsuNumberDecimalSeparator,
        groupSeparator: UsuNumberGroupSeparator,
        decimalDigits: UsuNumberNumDecimals
    });
    //Los campos de Tipo Identificador no van formateados
    $('input[id^=txtCampoFiltroAvanzado_Numerico_][id$=_' + CAMPOSGENERALES.IDENTIFICADOR + ']').numericFormatted({
        groupSeparator: "",
        decimalDigits: 0
    });
    //tipo fecha
    $('[id^=txtCampoFiltroAvanzado_Fecha_Desde_],[id^=txtCampoFiltroAvanzado_Fecha_Hasta_]').datepicker({
        showOn: 'both',
        buttonImage: ruta + 'images/colorcalendar.png',
        buttonImageOnly: true,
        buttonText: '',
        dateFormat: UsuMask.replace('MM', 'mm').replace('yyyy', 'yy'),
        showAnim: 'slideDown'
    });
    $.datepicker.setDefaults($.datepicker.regional[UsuLanguageTag]);
    //Establecemos la validación numérica para el textbox del modo "HACE" de la fecha
    $('img[name=ModoFecha]').siblings('div[index=1]').find('input:first').numeric({ decimal: false, negative: false });
    $('[lblDesde]').text(TextosPantalla[23]);
    $('[lblHasta]').text(TextosPantalla[24]);
    CargarOpcionesFechasRelativas($('#lWizardEscenarioListaCamposFiltroAvanzado select[tipocombofecha=1]'));
    CargarOpcionesFechasPeriodo($('#lWizardEscenarioListaCamposFiltroAvanzado select[tipocombofecha=2]'));
    $('[tipocombofecha=1]').siblings('span').text(TextosPantalla[87]);

    //aplicamos los textos a los botones de los campos    
    $('#lWizardEscenarioListaCamposFiltroAvanzado label[name=AplicarCambios] span[class^=Texto]').text(TextosPantalla[91]);
    $('#lWizardEscenarioListaCamposFiltroAvanzado label[name=Agregar] span[class^=Texto]').text(TextosPantalla[92]);
};
function Comprobar_Datos_Escenario(step) {
    var todoOK = true;
    var tipoComprobacion;
    switch (step) {
        case 1:
            switch (totalStepsWizardEscenario) {
                case 1:
                    if (idEscenarioEdicion == 0) tipoComprobacion = 4;
                    else tipoComprobacion = 1;
                    break;
                case 2:
                    tipoComprobacion = 2;
                    break;
                case 4:
                    tipoComprobacion = 1;
                    break;
            };
            break;
        case 2:
            tipoComprobacion = 2;
            break;
        case 3:
            tipoComprobacion = 3;
            break;
        case 4:
            tipoComprobacion = 4;
            break;
    };
    switch (tipoComprobacion) {
        case 1:
            if ($('#txtWizardEscenarioNombre').val() == '') {
                alert(TextosPantalla[38]);
                $('#txtWizardEscenarioNombre').focus();
                return false;
            }
            if ($('#rWizardEscenarioSolicitud').prop('checked')) {
                if ($('#ddWizardEscenarioTipoSolicitud').val() == null) {
                    alert(TextosPantalla[36]);
                    return false;
                }

                var tiposSolicitudFormulario = $('#ddWizardEscenarioTipoSolicitud').multiselect('getChecked')[0].value.split('###')[0];
                escenarioEdicion.AplicaTodas = false;
                escenarioEdicion.SolicitudFormularioVinculados = {};
                $.each($('#ddWizardEscenarioTipoSolicitud').multiselect('getChecked'), function (i, x) {
                    escenarioEdicion.SolicitudFormularioVinculados[x.value.split('###')[1]] = { Key: x.value.split('###')[0], Value: x.title };
                    if (tiposSolicitudFormulario !== x.value.split('###')[0]) escenarioEdicion.AplicaTodas = true;
                });
            } else {
                escenarioEdicion.AplicaTodas = true;
                escenarioEdicion.SolicitudFormularioVinculados = {};
            };
            if (totalStepsWizardEscenario == 1) Guardar();
            break;
        case 2:
            if ($('#txtWizardEscenarioFiltroNombre').val() == '') {
                alert(TextosPantalla[37]);
                $('#txtWizardEscenarioFiltroNombre').focus();
                return false;
            }
            if ($('#lCamposGenerales input[type=checkbox]:checked').length == 0 && $('#lCamposFormulario input[type=checkbox]:checked').length == 0) {
                alert(TextosPantalla[25]);
                return false;
            }
            var condFiltro;
            if ($('#wizardEscenarioStep3').data('formulaAvanzada')) {
                $.each(formulaFiltro.FormulaCondiciones, function (index) {
                    condFiltro = this;
                    if (condFiltro.IdCampo !== 0) {
                        if (condFiltro.EsCampoGeneral) {
                            if ($.grep($('#lCamposGenerales input[type=checkbox]:checked'), function (x) { return parseInt($(x).closest('li').attr('id').split('_')[1]) == condFiltro.IdCampo; }).length == 0)
                                formulaFiltro.FormulaCondiciones.splice(index, 1);
                        } else {
                            if ($.grep($('#lCamposFormulario input[type=checkbox]:checked'), function (x) { return parseInt($(x).closest('li').attr('id').split('_')[1]) == condFiltro.IdCampo; }).length == 0)
                                formulaFiltro.FormulaCondiciones.splice(index, 1);
                        };
                    };
                });
            }
            break;
        case 3:
            if ($('#wizardEscenarioStep3').data('formulaAvanzada')) {
                $.each(formulaFiltro.FormulaCondiciones, function () {
                    if (this.TieneError) {
                        todoOK = false;
                        alert(TextosPantalla[105]);
                        return false;
                    };
                });
                if (todoOK) {
                    $.when($.ajax({
                        type: "POST",
                        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Comprobar_Formula_Avanzada',
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify({ FiltroCondicionesFormula: JSON.stringify(formulaFiltro.FormulaCondiciones) }),
                        dataType: "json",
                        async: false
                    })).done(function (msg) {
                        switch (parseInt(msg.d)) {
                            case 1: // Balance de parantesis erroneo
                                alert(TextosPantalla[72]);
                                return false;
                            case 2: // Sucesion de operadores erroneo
                                alert(TextosPantalla[73]);
                                return false;
                            case 3: // Operador entre terminos inexistente
                                alert(TextosPantalla[74]);
                                return false;
                            case -1:
                                alert(TextosPantalla[105]);
                                return false;
                            default:
                                switch (totalStepsWizardEscenario) {
                                    case 2:
                                        Guardar();
                                        break;
                                    case 4:
                                        Cambiar_Step_Wizard(4);
                                        break;
                                };
                                break;
                        };
                    });
                };
            } else {
                if (totalStepsWizardEscenario == 2) Guardar();
            };
            break;
        case 4:
            if ($('#txtWizardEscenarioVistaNombre').val() == '') {
                alert(TextosPantalla[39]);
                $('#txtWizardEscenarioVistaNombre').focus();
                return false;
            }
            if (lCamposSeleccionadosVista.length == 0 && lCamposSeleccionadosVistaDesglose == 0) {
                alert(TextosPantalla[40]);
                return false;
            }
            Guardar();
            break;
    };

    return true;
};
function Obtener_Formula_Basica() {
    var formula = '';
    var formulaCondiciones = [];
    var formulaCondicion;
    var orden = 1;
    var campoFiltroGeneral; var idCampoFiltro; var idCampoDesglose; var oCampoFiltro;

    $.each($('#lWizardEscenarioListaCamposFiltroBasico fieldset'), function () {
        formulaCondicion = {};

        //Establecemos las propiedades de la condición
        campoFiltroGeneral = ($(this).attr('id').split('_')[1] == 'CampoGeneral');
        idCampoFiltro = $(this).attr('id').split('_')[2];
        id_Campo_Desglose = $(this).attr('id').split('_')[3];
        if (campoFiltroGeneral)
            oCampoFiltro = campos_generales[idCampoFiltro];
        else {
            if (id_Campo_Desglose == undefined) oCampoFiltro = campos_formulario[idCampoFiltro];
            else oCampoFiltro = campos_formulario[id_Campo_Desglose].CamposDesglose[idCampoFiltro];
        }
        EstablecerCondicion(formulaCondicion, $(this), oCampoFiltro);

        if (formulaCondicion.Valores !== undefined && formulaCondicion.Valores.length > 0) {
            //Si no es la primera condicion, añadimos un AND
            if (orden !== 1) {
                var formulaCondicionAND = {};
                formulaCondicionAND.Orden = orden;
                formulaCondicionAND.IdCampo = 0;
                formulaCondicionAND.EsCampoGeneral = false;
                formulaCondicionAND.Denominacion = '';
                formulaCondicionAND.Denominacion_BD = '';
                formulaCondicionAND.Operador = OPERADORES_FORMULA.AND;
                formulaCondicionAND.Valores = [];
                formulaCondicionAND.Condicion = DENOMINACION_OPERADORES_FORMULA[OPERADORES_FORMULA.AND]
                formulaCondiciones.push(formulaCondicionAND);
                formula += '<FFC' + orden + '/>';
                orden += 1;
            };

            //Establecemos el orden
            formulaCondicion.Orden = orden;

            formula += '<FFC' + orden + '/>';

            //Añadimos la condicion a la fórmula
            formulaCondiciones.push(formulaCondicion);
            orden += 1;
        };
    });

    formulaFiltro.FormulaCondiciones = formulaCondiciones;
};
function NuevaVista() {
    idEscenarioEdicion = 0;
    idVistaEdicion = 0;
    sOrdenVista = '';
    Establecer_Textos_Pantalla();
    stepWizardEscenario = 1;
    totalStepsWizardEscenario = 1;

    $('#lblWizardEscenarioTitulo').text(TextosPantalla[57]);
    $('#lblWizardEscenarioStepNumber').text(stepWizardEscenario);
    $('#lblWizardEscenarioStepCount').text(totalStepsWizardEscenario);
    $('[id^=wizardEscenarioStep]').hide();
    lCamposSeleccionadosVista = [];
    lCamposSeleccionadosVistaDesglose = [];
    $('#lWizardEscenarioListaCamposFiltroBasico').empty();
    $('#lWizardEscenarioListaCamposFiltroAvanzado').empty();
    $('#divWizardEscenarioFormula').empty();
    $('#chkWizarEscenarioVistaDefecto').prop('checked', false);
    $('#lblWizardEscenarioFavorito').hide();
    $('[id$=imgWizardEscenarioFavoritoNoEdit]').hide()
    $('[id$=imgWizardEscenarioNoFavoritoNoEdit]').hide()
    $('[id$=updConfiguracionVista]').hide();
    Cambiar_Step_Wizard(1);
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Escenario',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ IdEscenario: idEscenarioSeleccionado, Edicion: false, iTipoVisor: tipoVisor }),
        dataType: "json",
        async: false
    })).done(function (msg) {
        //OBTENEMOS LOS DATOS NECESARIO PARA CREAR UN NUEVO ESCENARIO.
        //LOS TIPOS DE SOLICITUD QUE PUEDE ELEGIR EL USUARIO
        //LAS CARPETAS QUE TIENE CREADAS EL USUARIO        
        var info = $.parseJSON(msg.d);
        escenarioEdicion = info[0];
        //INICIALIZAMOS LAS OPCIONES DEL NUEVO ESCENARIO        
        $('#chkWizardEscenarioDefecto').prop('checked', escenarioEdicion.Defecto).prop('disabled', true); //Deshabilitamos check al no se edicion de escenario
        if (escenarioEdicion.Favorito) $('[id$=imgWizardEscenarioFavoritoNoEdit]').show();
        else $('[id$=imgWizardEscenarioNoFavoritoNoEdit]').show();

        $('#txtWizardEscenarioNombre').hide().val(escenarioEdicion.Nombre);
        $('#lblWizardEscenarioNombre').show().text(escenarioEdicion.Nombre);

        campos_generales = info[3];
        $('#lCamposGeneralesVista').empty();        
        switch (Number(tipoVisor)) {
            case TIPOVISOR.SOLICITUDES:
                $('#campoGeneralVista').tmpl($.map(campos_generales, function (campo_general) { if (campo_general.Id !== CAMPOSGENERALES.ESTADOHOMOLOGACION) return campo_general })).appendTo($('#lCamposGeneralesVista'));
                break;
            case TIPOVISOR.SOLICITUDESQA:
                $('#campoGeneralVista').tmpl($.map(campos_generales, function (campo_general) {
                    if (campo_general.Id !== CAMPOSGENERALES.INFOPEDIDOSASOCIADOS && campo_general.Id !== CAMPOSGENERALES.INFOPROCESOSASOCIADOS) return campo_general;
                })).appendTo($('#lCamposGeneralesVista'));
                break;
            default:
                $('#campoGeneralVista').tmpl($.map(campos_generales, function (campo_general) {
                    if (campo_general.Id !== CAMPOSGENERALES.INFOPEDIDOSASOCIADOS && campo_general.Id !== CAMPOSGENERALES.INFOPROCESOSASOCIADOS && campo_general.Id !== CAMPOSGENERALES.ESTADOHOMOLOGACION) return campo_general;
                })).appendTo($('#lCamposGeneralesVista'));
        }

        var idSolicitudFormulario = 0;
        if (!escenarioEdicion.AplicaTodas) {
            for (var key in escenarioEdicion.SolicitudFormularioVinculados) break;
            idSolicitudFormulario = escenarioEdicion.SolicitudFormularioVinculados[key].Key;
        };
        Cargar_Campos_Formulario(idSolicitudFormulario, 3);
        $('[id^=txtEscenarioVistaCampoNombrePersonalizado_]').prop('placeholder', TextosPantalla[27]);

        CentrarPopUp($('#wizardEscenario'));
        $('#wizardEscenario').show();

        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();
        $('#txtWizardEscenarioVistaNombre').val('').focus();
    });
};
function EditarVista() {
    idEscenarioEdicion = 0;
    idVistaEdicion = idVistaSeleccionado;
    Establecer_Textos_Pantalla();
    stepWizardEscenario = 1;
    totalStepsWizardEscenario = 1;
    $('#lblWizardEscenarioTitulo').text(TextosPantalla[97]);
    $('#lblWizardEscenarioStepNumber').text(stepWizardEscenario);
    $('#lblWizardEscenarioStepCount').text(totalStepsWizardEscenario);
    $('[id^=wizardEscenarioStep]').hide();
    $('#txtWizardEscenarioFiltroNombre').val('');
    $('#lWizardEscenarioListaCamposFiltroBasico').empty();
    $('#lWizardEscenarioListaCamposFiltroAvanzado').empty();
    $('#divWizardEscenarioFormula').empty();
    $('#txtWizardEscenarioVistaNombre').val('');
    $('#lblWizardEscenarioFavorito').hide();
    $('[id$=imgWizardEscenarioFavorito]').hide();
    $('[id$=imgWizardEscenarioNoFavorito]').hide();
    $('[id$=imgWizardEscenarioFavoritoNoEdit]').hide();
    $('[id$=imgWizardEscenarioNoFavoritoNoEdit]').hide();

    Cambiar_Step_Wizard(1);
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Escenario',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ IdEscenario: idEscenarioSeleccionado, Edicion: false, iTipoVisor: tipoVisor }),
        dataType: "json",
        async: false
    })).done(function (msg) {
        //OBTENEMOS LOS DATOS NECESARIO PARA CREAR UN NUEVO ESCENARIO.
        //LOS TIPOS DE SOLICITUD QUE PUEDE ELEGIR EL USUARIO
        //LAS CARPETAS QUE TIENE CREADAS EL USUARIO
        var info = $.parseJSON(msg.d);
        escenarioEdicion = info[0];
        //INICIALIZAMOS LAS OPCIONES DEL NUEVO ESCENARIO
        $('#chkWizardEscenarioDefecto').prop('checked', escenarioEdicion.Defecto).prop('disabled', true); //Deshabilitamos check al no se edicion de escenario
        if (escenarioEdicion.Favorito) $('[id$=imgWizardEscenarioFavoritoNoEdit]').show();
        else $('[id$=imgWizardEscenarioNoFavoritoNoEdit]').show();

        $('#txtWizardEscenarioNombre').hide().val(escenarioEdicion.Nombre);
        $('#lblWizardEscenarioNombre').show().text(escenarioEdicion.Nombre);

        $('#chkWizarEscenarioVistaDefecto').prop('checked', escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Defecto);
        $('#txtWizardEscenarioVistaNombre').val(escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Nombre);

        campos_generales = info[3];
        $('#lCamposGeneralesVista').empty();        
        switch (Number(tipoVisor)) {
            case TIPOVISOR.SOLICITUDES:
                $('#campoGeneralVista').tmpl($.map(campos_generales, function (campo_general) { if (campo_general.Id !== CAMPOSGENERALES.ESTADOHOMOLOGACION) return campo_general })).appendTo($('#lCamposGeneralesVista'));
                break;
            case TIPOVISOR.SOLICITUDESQA:
                $('#campoGeneralVista').tmpl($.map(campos_generales, function (campo_general) {
                    if (campo_general.Id !== CAMPOSGENERALES.INFOPEDIDOSASOCIADOS && campo_general.Id !== CAMPOSGENERALES.INFOPROCESOSASOCIADOS) return campo_general;
                })).appendTo($('#lCamposGeneralesVista'));
                break;
            default:
                $('#campoGeneralVista').tmpl($.map(campos_generales, function (campo_general) {
                    if (campo_general.Id !== CAMPOSGENERALES.INFOPEDIDOSASOCIADOS && campo_general.Id !== CAMPOSGENERALES.INFOPROCESOSASOCIADOS && campo_general.Id !== CAMPOSGENERALES.ESTADOHOMOLOGACION) return campo_general;
                })).appendTo($('#lCamposGeneralesVista'));
        }

        var idSolicitudFormulario = 0;
        if (!escenarioEdicion.AplicaTodas) {
            for (var key in escenarioEdicion.SolicitudFormularioVinculados) break;
            idSolicitudFormulario = escenarioEdicion.SolicitudFormularioVinculados[key].Key;
        };
        bEditarVista = true;
        Cargar_Campos_Formulario(idSolicitudFormulario, 3);
        bEditarVista = false;
        $('[id^=txtEscenarioVistaCampoNombrePersonalizado_]').prop('placeholder', TextosPantalla[27]);

        $.each(escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Campos_Vista, function () {
            $('#chk_Campo' + (this.EsCampoGeneral ? 'General' : 'Formulario') + 'Vista_' + this.Id).prop('checked', true);
            if (this.NombrePersonalizado !== null) $('#txtEscenarioVistaCampoNombrePersonalizado_' + (this.EsCampoGeneral ? 'General' : 'Formulario') + '_' + this.Id).val(this.NombrePersonalizado);
        });
        lCamposSeleccionadosVista = $.map(escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Campos_Vista, function (campoVista) { if (!campoVista.EsCampoDesglose) return campoVista; });
        lCamposSeleccionadosVistaDesglose = $.map(escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Campos_Vista, function (campoVista) { if (campoVista.EsCampoDesglose) return campoVista; });
        CentrarPopUp($('#wizardEscenario'));
        $('#wizardEscenario').show();

        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();
        $('#txtWizardEscenarioVistaNombre').focus();
        sOrdenVista = escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Orden;
        $('[id$=updConfiguracionVista]').show();
        var btnRecargarConfigVista = $('[id$=btnRecargarConfigVista]').attr('id')
        sParameters = {};
        sParameters.CamposVista = JSON.stringify($.merge($.merge([], lCamposSeleccionadosVista), lCamposSeleccionadosVistaDesglose));
        sParameters.OrdenVista = (sOrdenVista == undefined ? '' : sOrdenVista);
        __doPostBack(btnRecargarConfigVista, JSON.stringify(sParameters));
    });
    return false;
};
function EliminarVista() {
    if (confirm((escenarios[idEscenarioSeleccionado].Modo == MODOSESCENARIO.NO_COMPARTIDO ? '' : TextosPantalla[124]) + TextosPantalla[69])) {
        var idCarpeta = escenarios[idEscenarioSeleccionado].Carpeta;
        MostrarCargando();
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Delete_Escenario_Vista',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ idEscenarioVista: idVistaSeleccionado, posicion: escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Posicion, idEscenario: idEscenarioSeleccionado }),
            dataType: "json",
            async: true
        })).done(function (msg) {
            OcultarCargando();
            var info = $.parseJSON(msg.d);
            $.each(escenarios[idEscenarioSeleccionado].EscenarioVistas, function () {
                if (this.Posicion > escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Posicion)
                    this.Posicion = this.Posicion - 1;
            });
            delete escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado];
            Crear_Vistas();
            Seleccionar_Vista(idVistaSeleccionado);
            Cargar_Solicitudes();
        });
    };
    return false;
};
function GuardarConfiguracion() {
    $('[id$=imgGuardandoVista]').show();
    var escenarioGuardarVista = $.parseJSON(JSON.stringify(escenarios[idEscenarioSeleccionado]));
    escenarioGuardarVista.EscenarioVistas[idFiltroSeleccionado] = $.parseJSON(JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado]));
    var opcionesUsu = JSON.stringify({ tipoOpcion: 3, oOpcionesUsu: JSON.stringify(escenarioGuardarVista.EscenarioVistas) });
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Establecer_Opciones_Usu',
        contentType: "application/json; charset=utf-8",
        data: opcionesUsu,
        dataType: "json",
        async: true
    })).done(function (msg) {
        var params = JSON.stringify({ Vista: JSON.stringify(escenarioGuardarVista.EscenarioVistas[idVistaSeleccionado]), iTipoVisor: tipoVisor });
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Update_Escenario_Vista_Configuracion',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        })).done(function (msg) {
            var info = $.parseJSON(msg.d);
            escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado] = info;
            $('[id$=imgGuardandoVista]').hide();
            $('[id$=imgGuardadoVista]').show();
            setTimeout(function () { $('[id$=imgGuardadoVista]').hide(); }, 3000);
        });
    });
    return false;
};