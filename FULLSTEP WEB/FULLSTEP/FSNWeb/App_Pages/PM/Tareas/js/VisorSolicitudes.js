﻿//#region Constantes
var CAMPOSGENERALES = {
    IDENTIFICADOR: 4,
    ESTADO: 9,
    SITUACIONACTUAL: 10,
    INFOPEDIDOSASOCIADOS: 17,
    INFOPROCESOSASOCIADOS: 18,
    ESTADOHOMOLOGACION: 24
};
var TIPOSCAMPOGS = {
    USUARIO: -2,
    PETICIONARIO: -1,
    DESCBREVE: 1,
    DESCRDETALLADA: 2,
    IMPORTE: 3,
    CANTIDAD: 4,
    FECNECESIDAD: 5,
    INISUMINISTRO: 6,
    FINSUMINISTRO: 7,
    ARCHIVOESPECIFIC: 8,
    PRECIOUNITARIO: 9,
    PRECIOUNITARIOADJ: 10,
    PROVEEDORADJ: 11,
    CANTIDADADJ: 12,
    TOTALLINEAADJ: 13,
    TOTALLINEAPREADJ: 14,
    PROVEEDOR: 100,
    FORMAPAGO: 101,
    MONEDA: 102,
    MATERIAL: 103,
    CODARTICULO: 104,
    UNIDAD: 105,
    DESGLOSE: 106,
    PAIS: 107,
    PROVINCIA: 108,
    DEST: 109,
    PRES1: 110,
    PRES2: 111,
    PRES3: 112,
    PRES4: 113,
    CONTACTO: 114,
    PERSONA: 115,
    PROVECONTACTO: 116,
    ROL: 117,
    DENARTICULO: 118,
    NUEVOCODARTICULO: 119,
    NUMSOLICTERP: 120,
    UNIDADORGANIZATIVA: 121,
    DEPARTAMENTO: 122,
    ORGANIZACIONCOMPRAS: 123,
    CENTRO: 124,
    ALMACEN: 125,
    LISTADOSPERSONALIZADOS: 126,
    IMPORTESOLICITUDESVINCULADAS: 127,
    REFSOLICITUD: 128,
    CENTROCOSTE: 129,
    PARTIDA: 130,
    ACTIVO: 131,
    TIPOPEDIDO: 132,
    DESGLOSEACTIVIDAD: 136,
    UNIDADPEDIDO: 142,
    PROVEEDORERP: 143,
    COMPRADOR: 144,
    EMPRESA: 149
};
var MODOSESCENARIO = {
    NO_COMPARTIDO: 0,
    COMPARTIDO_EDITABLE: 1,
    COMPARTIDO_PROTEGIDO: 2
}
var TIPOVISOR = {
    SOLICITUDES: 1,
    FACTURAS: 5,
    ENCUESTAS: 6,
    SOLICITUDESQA: 7
};
var TIPOSOLICITUD = {
    ENCUESTA: 12,
    FACTURA: 13,
    SOLICITUDQA: 14
}
//#endregion

//#region Variables de módulo
var escenarios, escenariosCarpeta, idEscenarioSeleccionado, idFiltroSeleccionado, idVistaSeleccionado;
var idCarpetaUsuario, AccionCarpetaUsuario, idCarpetaSeleccionada;
var instanciasAprobar = {};
var instanciasRechazar = {};
var escenarioFiltroContador = [];
var escenarioFiltroCondicionesOrden = [];
var FocusCarpetasEscenario = false;
var datosOrdenacionNoCargados = true;
var obtenerSolicitudesPendientes = true;
var txtAgregarCarpeta = '<li id="carpetaNueva"><div style="margin-left:2em;"><input type="text" id="txtCarpeta" class="CajaTexto" style="width:100%;"/></div></li>';
var txtEditarCarpeta = '<div id="editarCarpeta" style="margin-left:2em;"><span class="treeTipo10img"/><input type="text" id="txtCarpeta" class="CajaTexto" style="width:80%;"/></div>';
var consultas = {};
var ordenacionEscenario = [];
var ordenacionFiltro = [];
var ordenacionVista = [];
var buscandoIdentificador = false;
var idCarpetaActualVolver, idEscenarioActualVolver, idFiltroActualVolver, idVistaActualVolver;
var bAprobarAllChecked = false;
var bRechazarAllChecked = false;
//#endregion

//#region Inicialización

$('body').click($.proxy(doc_WindowClick, this));
function OcultarCargando() {
    var modalprog = $find(ModalProgress);
    if (modalprog) modalprog.hide();
};
function doc_WindowClick(e) {
    if (!FocusCarpetasEscenario && $(e.target).attr('id') !== 'txtCarpeta') {
        $('#EscenariosListadoCarpetas').hide();
        FocusCarpetasEscenario = false;
        $('#ulEscenariosCarpetas_' + idCarpetaUsuario + '>div').show();
        $('#carpetaNueva').remove();
        $('#editarCarpeta').remove();
    } else $('#txtCarpeta').focus();
};
//Opciones de menuque aparece al hacer click en una carpeta con el boton drcho del raton
var CLIPBOARD = '';
var opcionesContexMenuCarpetas = [
    { title: TextosPantalla[58], cmd: "Cut", uiIcon: "ui-icon-scissors" },
    { title: TextosPantalla[59], cmd: "Paste", uiIcon: "ui-icon-clipboard", disabled: true },
    { title: TextosPantalla[60], cmd: "Add", uiIcon: "ui-icon-copy" },
    { title: TextosPantalla[61], cmd: "Edit", uiIcon: "ui-icon-pencil" },
    { title: TextosPantalla[62], cmd: "Delete", uiIcon: "ui-icon-trash" }];
function PantallaCargada() {
    $('#pnlCargando').hide();
    $('#pnlFondoCargando').hide();
    OcultarCargando();
};
$(document).ready(function () {
    //Si esta esperando las llamadas de numero de solicitudes de los filtros y abandono sesion casca por no tener ya fsnserver
    $('[id$=lnkbtnCerrarSesion]').live('click', function () {
        $.each(consultas, function (i, x) {
            if (x && x.readyState != 4) {
                x.abort();
                delete x;
            }
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm.get_isInAsyncPostBack()) prm.abortPostBack();
    });
    $('#pnlFondoCargando').css('height', $(document).height());
    CentrarPopUp($('#pnlCargando'));
    $("#txtFiltroIdentificador").numeric({ decimal: false });
    $.get(rutaFS + 'PM/Tareas/html/escenario_filtro_vista.tmpl.htm?version=' + version, function (escenariosHtml) {
        escenariosHtml = escenariosHtml.replace(/src='/gi, 'src=\'' + ruta);
        $('#Contenido').append(escenariosHtml);
        //Cargamos la estructura de escenarios del usuario. Contiene los filtros y las vistas vinculadas.
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Escenarios_Usuario',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ iTipoVisor: tipoVisor }),
            dataType: "json",
            async: true
        })).done(function (msg) {
            var info = $.parseJSON(msg.d);
            escenarios = info[0];
            if (escenarios.length == 0) CarpetaSinEscenarios();
            else {
                var idEscenarioSeleccionado = parseInt(info[1]);
                $.when($.ajax({
                    type: "POST",
                    url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_EscenariosFavoritos_Usuario',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({ iTipoVisor: tipoVisor }),
                    dataType: "json",
                    async: true
                })).done(function (msg) {
                    var escenariosFavoritos = $.parseJSON(msg.d);
                    $('#escenarioFavorito').tmpl(escenariosFavoritos).appendTo($('#ulEscenariosFavoritos'));
                });
                //Cargamos las carpetas creadas por el usuario para organizar los escenarios
                $.when($.ajax({
                    type: "POST",
                    url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Carpetas_Usuario',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({ iTipoVisor: tipoVisor }),
                    dataType: "json",
                    async: true
                })).done(function (msg) {
                    var carpetas = $.parseJSON(msg.d);
                    //Montamos el arbol de las carpetas
                    if (carpetas !== null)
                        $('#ulEscenariosCarpetas').tree({
                            data: carpetas,
                            autoOpen: true,
                            selectable: false
                        });
                    else $('#ulEscenariosCarpetas').append('<ul></ul>');
                    //Obtenemos el script del menu contextual de las carpetas y programamos las acciones.                
                    $('#EscenariosCarpetas').contextmenu({
                        delegate: "div.treeTipo10",
                        menu: opcionesContexMenuCarpetas,
                        taphold: true,
                        beforeOpen: function (event, ui) {
                            if (parseInt($(ui.target).closest('li').attr('id').replace('ulEscenariosCarpetas_', '')) == 0) return false;
                            //Antes de abrir mostramos el panel de carpetas para que no se oculte al perder el foco mientras operamos con el menu
                            $('#EscenariosListadoCarpetas').show();
                            $('#ulEscenariosCarpetas_' + idCarpetaUsuario + '>div').show();
                            $('#carpetaNueva').remove();
                            $('#editarCarpeta').remove();
                            FocusCarpetasEscenario = true;
                            idCarpetaUsuario = parseInt($(ui.target).closest('li').attr('id').split('_')[1]);
                            $('#EscenariosCarpetas').contextmenu('enableEntry', 'Paste', !(CLIPBOARD == ''));
                        },
                        select: function (event, ui) {
                            //Si se selecciona algun item del menu no dejamos que se oculte el panel de carpetas hasta que haga click fuera del panel de carpetas                                
                            AccionCarpetaUsuario = ui.cmd;
                            switch (ui.cmd) {
                                case 'Add': //Añadir subcarpeta. Añadimos al arbol un input para que asigne el nombre de la carpeta
                                    $('#carpetaNueva').remove();
                                    $('#editarCarpeta').remove();
                                    if ($(ui.target).closest('li').children('ul').length == 0) $(ui.target).closest('li').append('<ul/>');
                                    $(ui.target).closest('li').children('ul').append(txtAgregarCarpeta);
                                    $('#ulEscenariosCarpetas').tree('open', $('#ulEscenariosCarpetas_' + idCarpetaUsuario));
                                    $('#txtCarpeta').focus();
                                    break;
                                case 'Edit': //Editar carpeta. Ocultamos el nombre de la carpeta y mostramos un input para editar el nombre
                                    $('#carpetaNueva').remove();
                                    $('#editarCarpeta').remove();
                                    $('#' + $(ui.target).closest('li').attr('id') + '>div').hide();
                                    $('#' + $(ui.target).closest('li').attr('id') + '>div').after(txtEditarCarpeta);
                                    $('#txtCarpeta').val($('#' + $(ui.target).closest('li').attr('id') + '>div>label').text());
                                    $('#txtCarpeta').focus();
                                    break;
                                case 'Delete': //Eliminar carpeta
                                    Eliminar_Carpeta_Usuario();
                                    break;
                                case 'Cut':
                                    CLIPBOARD = $(ui.target).closest('li');
                                    break;
                                case 'Paste':
                                    var Id_Cortado = parseInt(CLIPBOARD.attr('id').split('_')[1]);
                                    Pegar_Carpeta_Usuario(Id_Cortado);
                                    CLIPBOARD = '';
                                    break;
                            };
                        },
                        close: function (event) {
                            if (!FocusCarpetasEscenario)
                                $('#EscenariosListadoCarpetas').hide();
                            FocusCarpetasEscenario = false;
                        }
                    });
                    $.get(rutaFS + 'PM/Tareas/html/escenario_campos.tmpl.htm?version=' + version, function (campos) {
                        campos = campos.replace(/src='/gi, 'src=\'' + ruta);
                        $('#Contenido').append(campos);

                        idCarpetaSeleccionada = 0
                        if (typeof (escenarios[idEscenarioSeleccionado]) !== 'undefined') {
                            idCarpetaSeleccionada = escenarios[idEscenarioSeleccionado].Carpeta;
                        }
                        if (typeof (IdEscenario) != 'undefined') {
                            escenarioVolver = IdEscenario;
                        };
                        //Si se viene la pantalla de detalle, cargamos la carpeta, escenario, filtro y vista que había previamente                        
                        if (typeof (escenarioVolver) != 'undefined') {
                            idCarpetaSeleccionada = escenarios[escenarioVolver].Carpeta;
                            idEscenarioSeleccionado = escenarioVolver;
                            carpetaVolver = undefined;
                        }
                        if (typeof (numpag) == 'undefined') numpag = 1;
                        //Cargamos la carpeta
                        Seleccionar_Carpeta(idCarpetaSeleccionada);
                        //Cargamos el escenario
                        Seleccionar_Escenario(idEscenarioSeleccionado, function () {
                            //Creamos los fitros
                            Crear_Filtros();
                            //Seleccionamos el filtro adecuado
                            Seleccionar_Filtro(idFiltroSeleccionado);
                            //Cargamos las condiciones del filtro
                            Cargar_Campos_Condiciones_Filtro();
                            //Creamos las vistas
                            Crear_Vistas();
                            //Seleccionamos la vista adecuada
                            Seleccionar_Vista(idVistaSeleccionado);
                            //Ocultamos el panel de cargando...
                            PantallaCargada();
                            //Cargamos las solicitudes
                            Cargar_Solicitudes(numpag);
                        });
                    });
                });
            };
        });
    });
    InicializarComboTiposSolicitud();

    //Establecemos los tooltip de los iconos   
    $('#btnEscenarioNuevo').prop('title', TextosPantalla[0]);
    $('#btnEscenarioEditar').prop('title', TextosPantalla[96]);
    $('#btnEscenarioEliminar').prop('title', TextosPantalla[100]);

    $('#btnEscenarioFiltroNuevo').prop('title', TextosPantalla[56]);
    $('#btnEscenarioFiltroEditar').prop('title', TextosPantalla[90]);
    $('#btnEscenarioFiltroEliminar').prop('title', TextosPantalla[101]);
    $('#btnEscenarioFiltroGuardar').prop('title', TextosPantalla[103]);

    $('#CarpetaVacia').text(TextosPantalla[121]);

    //Aplicamos los permisos
    if (permisos.CREAR_ESCENARIOS) $('#btnEscenarioNuevo').show();
    if (!permisos.OCULTAR_ESCENARIO_POR_DEFECTO) $('#btnBuscarPorIdentificador').parent().show();
});

//#endregion

//#region Solicitudes Pendientes

//Click en el link de solicitudes pendientes
$('[id$=lnkSolicitudesPendientes]').live('click', function () {
    Seleccionar_Carpeta(0);
    Seleccionar_Escenario(0);
    Crear_Filtros();
    Seleccionar_Filtro(1);
    Cargar_Campos_Condiciones_Filtro();
    Crear_Vistas();
    Seleccionar_Vista(1);
    resetInstanciasCheck();
    Cargar_Solicitudes();
    return false;
});

function Obtener_Numero_Pendientes() {
    if (!permisos.OCULTAR_ESCENARIO_POR_DEFECTO) {
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Numero_Pendientes',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ iTipoVisor: tipoVisor }),
            dataType: "json",
            async: true
        })).done(function (msg) {
            var solicitudesPendientes = parseInt(msg.d);
            if (solicitudesPendientes == 0) $('#pnlSolicitudesPendientes').hide();
            else {
                $('[id$=lblSolicitudesPendientes]').text($('[id$=lblSolicitudesPendientes]').text().replace('###', solicitudesPendientes));
                $('#pnlSolicitudesPendientes').show();
            };
        });
    }
};

//#endregion

//#region Favoritos

//Mouseover y mouseout en el listado de Favoritos
$('#EscenariosFavoritos').live('mouseover', function () { $('#EscenariosListadoFavoritos').show(); }).live('mouseout', function () { $('#EscenariosListadoFavoritos').hide(); });
//Click en un escenario del listado de Favoritos
$('[id^=escenarioFavorito_]').live('click touchstart', function (event) {
    var carpetaEscenarioFavorito = parseInt($(this).attr('id').split('_')[1].split('#')[0]);
    var escenarioEscenarioFavorito = parseInt($(this).attr('id').split('_')[1].split('#')[1]);
    Seleccionar_Carpeta(carpetaEscenarioFavorito);
    Seleccionar_Escenario(escenarioEscenarioFavorito, function () {
        //Creamos los fitros
        Crear_Filtros();
        //Seleccionamos el filtro adecuado
        Seleccionar_Filtro(idFiltroSeleccionado);
        //Cargamos las condiciones del filtro
        Cargar_Campos_Condiciones_Filtro();
        //Creamos las vistas
        Crear_Vistas();
        //Seleccionamos la vista adecuada        
        Seleccionar_Vista(idVistaSeleccionado);
        resetInstanciasCheck();
        //Cargamos las solicitudes
        Cargar_Solicitudes();
    });

    event.stopPropagation();
});
//Click en la imagen de un elemento del listado de Favoritos
$('[id^=escenarioFavorito_] img').live('click', function (event) {
    event.stopPropagation();
    var id = parseInt($(this).parent('[id^=escenarioFavorito_]').attr('id').split('_')[1].split('#')[1]);
    $(this).parent('div').remove();
    if (escenarios[id].Favorito) {
        $('#escenario_' + id).find('img[name=favorito]').hide();
        $('#escenario_' + id).find('img[name=nofavorito]').show();
    } else {
        $('#escenario_' + id).find('img[name=nofavorito]').hide();
        $('#escenario_' + id).find('img[name=favorito]').show();
    };
    Establecer_Escenario_Favorito(id);
});

function Establecer_Escenario_Favorito(id) {
    escenarios[id].Favorito = !escenarios[id].Favorito;
    var escenariosOpcionesUsuCarpetaActual;
    escenariosOpcionesUsuCarpetaActual = $.map(escenarios, function (x) { if (x.Carpeta == escenarios[id].Carpeta) return x; });
    $.each(escenariosOpcionesUsuCarpetaActual.sort(ordenPorPosicion), function (index) { this.Posicion = index; });
    var opcionesUsu = JSON.stringify({ tipoOpcion: 1, oOpcionesUsu: JSON.stringify(escenariosOpcionesUsuCarpetaActual), iTipoVisor: tipoVisor });
    $.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Establecer_Opciones_Usu',
        contentType: "application/json; charset=utf-8",
        data: opcionesUsu,
        dataType: "json",
        async: true
    });
};

//#endregion

//#region Carpetas

//Mouseover y mouseout en el listado de Carpetas
$('#EscenariosCarpetas').live('mouseover', function () { $('#EscenariosListadoCarpetas').show(); }).live('mouseout', function () { if (!FocusCarpetasEscenario) $('#EscenariosListadoCarpetas').hide(); });
//Click en el botón "Agregar Carpeta"
$('#btnEscenariosCarpetaAgregar').live('click', function () {
    AccionCarpetaUsuario = 'Add';
    $('#carpetaNueva').remove();
    $('#editarCarpeta').remove();
    $('#EscenariosListadoCarpetas').show();
    FocusCarpetasEscenario = true;
    $('#ulEscenariosCarpetas>ul').append(txtAgregarCarpeta);
    $('#txtCarpeta').focus();
    idCarpetaUsuario = 0;
});
//Pulsación de teclado en el textbox del nombre de Carpeta
$('#txtCarpeta').live(($.browser.opera ? 'keypress' : 'keydown'), function (event) {
    var code;
    if (event.keyCode) code = event.keyCode;
    else if (event.which) code = event.which;
    switch (code) {
        case KEY.RETURN:
            switch (AccionCarpetaUsuario) {
                case 'Add':
                    Agregar_Carpeta_Usuario();
                    break;
                case 'Edit':
                    Editar_Carpeta_Usuario();
                    break;
            };
            FocusCarpetasEscenario = false;
            return false;
            break;
        case KEY.ESC:
            $('#ulEscenariosCarpetas_' + idCarpetaUsuario + '>div').show();
            $('#carpetaNueva').remove();
            $('#editarCarpeta').remove();
            FocusCarpetasEscenario = false;
            break;
        default:
            break;
    };
});
//Click en una carpeta del listado de Carpetas
$('#ulEscenariosCarpetas div.treeTipo10').live('click touchstart', function () {
    var idCarpeta = parseInt($(this).closest('li').attr('id').split('_')[1]);
    Ir_A_Carpeta(idCarpeta);
});
//''' <summary>
//''' Seleccionar Carpeta
//''' Si no hay escenario alguno, posible con permiso de "Ocultar el escenario por defecto", nosotros debemos ocultar el popup de Cargando. 
//''' </summary>
//''' <param name="idCarpeta">id Carpeta</param>
//''' <remarks>Llamada desde: document.ready     OrdenarEscenarios   btnBuscarPorIdentificador.live'click'    Ir_A_Carpeta     escenarioFavorito.live'click'  lnkSolicitudesPendientes.live'click'   Seleccionar_Carpeta    Actualizar_Arbol_Carpetas; Tiempo mÃ¡ximo: 0</remarks>
function Seleccionar_Carpeta(idCarpeta) {
    //Para hacer grep necesitamos que sea un array
    var arrEscenarios = $.map(escenarios, function (x) { return x; });
    //Seleccionamos los escenarios de la carpeta y menos el escenario por defecto (inicial) en caso de que tenga el permiso OCULTAR_ESCENARIO_POR_DEFECTO
    escenariosCarpeta = $.grep(arrEscenarios, function (x) { return x.Carpeta == idCarpeta && !(permisos.OCULTAR_ESCENARIO_POR_DEFECTO && x.Id == 0); });
    //Obtenemos cual es el escenario por defecto
    escenarioDefecto = $.grep(escenariosCarpeta, function (x) { return x.Defecto });

    if (typeof (escenarioVolver) != 'undefined') {
        //Hemos vuelto de otra página, Seleccionamos la carpeta en la que estábamos
        idEscenarioSeleccionado = escenarioVolver;
        escenarioVolver = undefined;
    } else {
        //Seleccionamos el escenario por defecto y si no existe, el primero        
        idEscenarioSeleccionado = escenarioDefecto.length == 0 ? (escenariosCarpeta.length == 0 ? null : escenariosCarpeta[0].Id) : escenarioDefecto[0].Id;
    };

    //Ordenamos los escenarios de la carpeta
    escenariosCarpeta = escenariosCarpeta.sort(ordenPorPosicion);

    //Renumeramos los escenarios con posicion 9999, estos son escenarios que no tienen registro de opciones
    //Para que puedan ser reordenados, los forzamos a que continuen con la secuencia
    var arrSecuencia = $.map($.grep(escenariosCarpeta, function (x) { return x.Posicion < 9999 }), function (x) { return x.Posicion });
    var maxSecuencia = Math.max.apply(Math, arrSecuencia);
    var arrSinOpciones = $.grep(escenariosCarpeta, function (x) { return x.Posicion == 9999 });
    $.each(arrSinOpciones, function () { this.Posicion = ++maxSecuencia });

    $('#lblEscenariosCarpetaSeleccionado').text((idCarpeta == 0 ? '' : '(' + $('#ulEscenariosCarpetas_' + idCarpeta).find('label').first().text() + ')'));
    idCarpetaSeleccionada = idCarpeta;
    $('[id^=ulEscenariosCarpetas_] div').removeClass('treeTipo10Selected');
    $('#ulEscenariosCarpetas_' + idCarpetaSeleccionada + '>div').addClass('treeTipo10Selected');
    $('#ulEscenariosCarpetas').tree('openParents', $('#ulEscenariosCarpetas_' + idCarpetaSeleccionada));
    $('#ulEscenariosCarpetas').tree('open', $('#ulEscenariosCarpetas_' + idCarpetaSeleccionada));
    if (escenariosCarpeta.length == 0 && idCarpeta !== 0) {
        //Si no hay escenarios en la carpeta y no es la raíz, vamos a la carpeta raíz
        CarpetaSinEscenarios();
    } else {
        $('[id$=updSolicitudes]').show();
        $('#lblEscenariosSeleccionado').text('');
        $('#lblEscenarioFiltrosSeleccionado').text('');
        $('#CarpetaVaciaEscenarios').hide();
        Crear_Escenarios();

        if (idEscenarioSeleccionado == null) {
            PantallaCargada();
        }
    };
};
function CarpetaSinEscenarios() {
    $('#panelEscenarios').empty();
    $('#lblEscenariosSeleccionado').text('');
    $('#panelEscenarios').html(TextosPantalla[121]);
    $('#panelEscenarioFiltros').empty();
    $('#lblEscenarioFiltrosSeleccionado').text('');
    idFiltroSeleccionado = undefined;
    $('#panelEscenarioFiltros').html(TextosPantalla[121]);
    $('#panelEscenarioVistas .escenarioVista').remove();
    $('[id$=updSolicitudes]').hide();
    $('#CarpetaVaciaEscenarios').show();
    $('[id$=imgEscenarioVistasOrder]').hide();
};
function Agregar_Carpeta_Usuario() {
    var params = { Nombre: $('#txtCarpeta').val(), Carpeta: idCarpetaUsuario, iTipoVisor: tipoVisor };
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Agregar_Carpeta_Usuario',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        Actualizar_Arbol_Carpetas(msg.d, false);
        var carpetas = msg.d;
    });
}
function Editar_Carpeta_Usuario() {
    var params = { Nombre: $('#txtCarpeta').val(), Carpeta: idCarpetaUsuario, iTipoVisor: tipoVisor };
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Editar_Carpeta_Usuario',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        Actualizar_Arbol_Carpetas(msg.d, true);
    });
}
function Eliminar_Carpeta_Usuario() {
    var Id = 0;
    var params = { Carpeta: idCarpetaUsuario, iTipoVisor: tipoVisor };
    idCarpetaUsuario = parseInt($('#ulEscenariosCarpetas_' + idCarpetaUsuario).parent('ul').closest('[id^=ulEscenariosCarpetas_]').attr('id').split('_')[1]);
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Eliminar_Carpeta_Usuario',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        Actualizar_Arbol_Carpetas(msg.d, true);
    });
}
function Pegar_Carpeta_Usuario(Id) {
    var params = { Id: Id, Id_Destino: idCarpetaUsuario, iTipoVisor: tipoVisor };
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Pegar_Carpeta_Usuario',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        Actualizar_Arbol_Carpetas(msg.d, true);
    });
}
function Actualizar_Arbol_Carpetas(carpetasNew, actualizarEscenarios) {
    $('#ulEscenariosCarpetas').tree('destroy');
    $('#ulEscenariosCarpetas').tree({
        data: carpetasNew,
        autoOpen: true,
        selectable: false
    });
    $('#ulEscenariosCarpetas').tree('openParents', $('#ulEscenariosCarpetas_' + idCarpetaUsuario));
    $('#ulEscenariosCarpetas').tree('open', $('#ulEscenariosCarpetas_' + idCarpetaUsuario));
    $('#carpetaNueva').remove();
    $('#editarCarpeta').remove();
    if (actualizarEscenarios) {
        var params = { iTipoVisor: tipoVisor };
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Escenarios_Usuario',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(params),
            dataType: "json",
            async: false
        })).done(function (msg) {
            var info = $.parseJSON(msg.d);
            escenarios = info[0];
            Seleccionar_Carpeta(idCarpetaUsuario);
        });
    };
}
//Mueve la selección a una carpeta en concreto
function Ir_A_Carpeta(idCarpeta) {
    Seleccionar_Carpeta(idCarpeta);
    if (escenariosCarpeta.length !== 0) {
        Seleccionar_Escenario(idEscenarioSeleccionado, function () {
            //Creamos los fitros
            Crear_Filtros();
            //Seleccionamos el filtro adecuado
            Seleccionar_Filtro(idFiltroSeleccionado);
            //Cargamos las condiciones del filtro
            Cargar_Campos_Condiciones_Filtro();
            //Creamos las vistas
            Crear_Vistas();
            //Seleccionamos la vista adecuada            
            Seleccionar_Vista(idVistaSeleccionado);
            resetInstanciasCheck();
            //Cargamos las solicitudes
            Cargar_Solicitudes();
        });
    }
}
//#endregion

//#region Buscar por Identificador
$('#btnBuscarPorIdentificador').live('click', function () {
    if ($('#txtFiltroIdentificador').val() == '') return false;

    //Cancelamos todas las consultas anteriores (escenario y filtros)
    $.each(consultas, function (i, x) {
        if (x && x.readyState != 4) {
            x.abort();
            delete x;
        }
    });
    //Cancelamos el postback que se esté ejecutando (solicitudes)
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm.get_isInAsyncPostBack()) prm.abortPostBack();

    MostrarCargando();
    var chkPedProce = false;
    if ($('[id$=chkInfoPedidosProcesos]').length > 0) chkPedProce = $('[id$=chkInfoPedidosProcesos]').prop('checked');

    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Buscar_Solicitud_Identificador',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ Id: $('#txtFiltroIdentificador').val(), chkProcesosPedidos: chkPedProce, iTipoVisor: tipoVisor }),
        dataType: "json",
        async: true
    })).done(function (msg) {
        OcultarCargando();
        var idFiltroEncontrado = parseInt(msg.d);
        if (idFiltroEncontrado == 0) {
            alert(TextosPantalla[99]);
        } else {
            buscandoIdentificador = true;
            $('#txtFiltroIdentificador').val('');

            resetInstanciasCheck();

            idCarpetaActualVolver = idCarpetaSeleccionada;
            idEscenarioActualVolver = idEscenarioSeleccionado;
            idFiltroActualVolver = idFiltroSeleccionado;
            idVistaActualVolver = idVistaSeleccionado;
            numPaginaActualVolver = ($('[id$=PagerPageList]').val() == '' ? 1 : $('[id$=PagerPageList]').val());
            //Seleccionamos el filtro devuelto en la carpeta por defecto, escenario por defecto y vista por defecto
            Seleccionar_Carpeta(0);
            Seleccionar_Escenario(0);
            Crear_Filtros();
            Crear_Vistas();
            Seleccionar_Vista(1);
            Seleccionar_Filtro(idFiltroEncontrado);

            //Del filtro seleccionado mostramos sólo la condición de ESTADO
            escenarios[0].EscenarioFiltros[idFiltroSeleccionado].FormulaCondicionesAUX = $.parseJSON(JSON.stringify(escenarios[0].EscenarioFiltros[idFiltroSeleccionado].FormulaCondiciones));
            var condicionesBusquedaSolicitud = $.grep(escenarios[0].EscenarioFiltros[idFiltroSeleccionado].FormulaCondiciones, function (x) { return x.EsCampoGeneral && x.IdCampo == CAMPOSGENERALES.ESTADO });
            if (condicionesBusquedaSolicitud.length > 0) condicionesBusquedaSolicitud[0].Orden = 1;
            escenarios[0].EscenarioFiltros[idFiltroSeleccionado].FormulaCondiciones = condicionesBusquedaSolicitud;
            Cargar_Campos_Condiciones_Filtro();
            escenarios[0].EscenarioFiltros[idFiltroSeleccionado].FormulaCondiciones = $.parseJSON(JSON.stringify(escenarios[0].EscenarioFiltros[idFiltroSeleccionado].FormulaCondicionesAUX));

            //Con el filtro y la vista seleccionados, cargamos el resultado de Solicitudes, que está almacenado en Cache por la llamada ajax a Buscar_Solicitud_Identificador
            var sLoadParameter = {};
            sLoadParameter.Vista = JSON.stringify(escenarios[0].EscenarioVistas[1]);
            var btnBuscarSolicitud = $('[id$=btnBuscarSolicitud]').attr('id');
            __doPostBack(btnBuscarSolicitud, JSON.stringify(sLoadParameter));
        };
    });
});
//#endregion

//#region Escenarios

//Click en la barra de Escenarios
$('#cabeceraEscenarios').live('click', function () {
    $('#panelEscenarios').toggle();
    $('[id$=imgEscenariosContraerBarra]').toggle();
    $('[id$=imgEscenariosExpandirBarra]').toggle();
});
//Click en el botón de Ordenación de escenarios
$('#cabeceraEscenarios [id$=imgEscenariosOrder]').live('click', function (event) {
    ejecutandoPartialPostBack = true;
    $("#listaOrder").sortable({
        cancel: '.ui-state-disabled',
        containment: 'parent',
        axis: 'y',
        scroll: true,
        scrollSensitivity: 50,
        scrollSpeed: 50,
        cursor: 'move',
        placeholder: 'ui-state-highlight',
        forcePlaceholderSize: true,
        start: function (event, ui) {
            var start_pos = ui.item.index();
            ui.item.data('start_pos', start_pos);
        },
        beforeStop: function (event, ui) {
            var idEscenarioOrden, carpetaEscenarioOrden, posicionAnterior, posicionActual;
            idEscenarioOrden = parseInt(ui.item.attr('id').replace('ordenEscenarioFiltroVista_', ''));
            carpetaEscenarioOrden = parseInt(escenarios[idEscenarioOrden].Carpeta);
            posicionAnterior = (ui.item.data('start_pos') - 1);
            posicionActual = (ui.item.index() - 1);;
            if (posicionActual !== posicionAnterior) {
                $.each(escenarios, function () {
                    if (this.Carpeta == escenarios[idEscenarioOrden].Carpeta) {
                        if (posicionActual < posicionAnterior) {
                            if (this.Posicion > posicionActual - 1 && this.Posicion < posicionAnterior) this.Posicion = this.Posicion + 1;
                        } else {
                            if (this.Posicion > posicionAnterior && this.Posicion < posicionActual + 1) this.Posicion = this.Posicion - 1;
                        };
                    };
                });
                escenarios[idEscenarioOrden].Posicion = posicionActual;
            };
            var escenariosOpcionesUsuCarpetaActual;
            escenariosOpcionesUsuCarpetaActual = $.map(escenarios, function (x) { if (x.Carpeta == escenarios[idEscenarioSeleccionado].Carpeta) return x; });
            var opcionesUsu = JSON.stringify({ tipoOpcion: 1, oOpcionesUsu: JSON.stringify(escenariosOpcionesUsuCarpetaActual) });
            OrdenarEscenarios(opcionesUsu);
        }
    });
    $("#listaOrder").disableSelection();
    $('#lblCabeceraPanelOrder').text(TextosPantalla[94]);
    $('#btnCerrarPanelOrder').text(TextosPantalla[89]);
    var escenariosOrdenar = $.parseJSON(JSON.stringify(escenariosCarpeta));
    escenariosOrdenar.unshift({ Nombre: '' });
    $('#ordenEscenarioFiltroVista').tmpl(escenariosOrdenar.sort(ordenPorPosicion)).appendTo($('#panelOrder ul'));
    $('#popupFondo').css('height', $(document).height());
    $('#popupFondo').show();
    CentrarPopUp($('#panelOrder'));
    $('#panelOrder').show();
    event.stopPropagation();
});
//Click en el checkbox de Escenario Por Defecto
$('[id$=chkEscenariosDefecto]').live('click', function (event) {
    event.stopPropagation();
    var checkDefecto = $(this);
    if (checkDefecto.prop('checked')) {
        var escenarioDefectoAnterior = $.map(escenarios, function (x) { if (x.Defecto) return x; });
        if (escenarioDefectoAnterior.length == 1) escenarios[escenarioDefectoAnterior[0].Id].Defecto = false;
    };
    var escenariosOpcionesUsuCarpetaActual;
    escenarios[idEscenarioSeleccionado].Defecto = checkDefecto.prop('checked');
    escenariosOpcionesUsuCarpetaActual = $.map(escenarios, function (x) { if (x.Carpeta == escenarios[idEscenarioSeleccionado].Carpeta) return x; });
    $.each(escenariosOpcionesUsuCarpetaActual.sort(ordenPorPosicion), function (index) { this.Posicion = index; });
    var opcionesUsu = JSON.stringify({ tipoOpcion: 1, oOpcionesUsu: JSON.stringify(escenariosOpcionesUsuCarpetaActual) });
    $.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Establecer_Opciones_Usu',
        contentType: "application/json; charset=utf-8",
        data: opcionesUsu,
        dataType: "json",
        async: true
    });
});
//Click en un Escenario
$('.escenario').live('click', function () {
    var idEscenarioSolicitado = parseInt($(this).attr('id').split('_')[1]);
    if (idEscenarioSolicitado !== idEscenarioSeleccionado) {
        idCarpetaActualVolver = undefined;

        $('#panelEscenarioFiltros').hide();
        Seleccionar_Escenario(idEscenarioSolicitado, function () {
            //Creamos los fitros
            Crear_Filtros();
            //Seleccionamos el filtro adecuado
            Seleccionar_Filtro(idFiltroSeleccionado);
            //Cargamos las condiciones del filtro
            Cargar_Campos_Condiciones_Filtro();
            //Creamos las vistas
            Crear_Vistas();
            //Seleccionamos la vista adecuada            
            Seleccionar_Vista(idVistaSeleccionado);
            //Cargamos las solicitudes
            resetInstanciasCheck();
            Cargar_Solicitudes();
        });
    }
});
//Click en la imagen de un escenario (Estrella Favorito)
$('.escenario img').live('click', function (event) {
    event.stopPropagation();
    var id = parseInt($(this).parent('.escenario').attr('id').split('_')[1]);
    $(this).parent('div').find('img').toggle();
    if (escenarios[id].Favorito) {
        $('#ulEscenariosFavoritos div[id$=' + id + ']').remove();
    } else {
        var escenarioFavorito = {};
        escenarioFavorito.value = idCarpetaSeleccionada + '#' + id;
        escenarioFavorito.text = escenarios[id].Nombre;
        $('#escenarioFavorito').tmpl(escenarioFavorito).appendTo($('#ulEscenariosFavoritos'));
    };
    Establecer_Escenario_Favorito(id);
});

function Crear_Escenarios() {
    $('#panelEscenarios').empty();
    $('#escenario').tmpl(escenariosCarpeta).appendTo($('#panelEscenarios'));
}

//''' <summary>
//''' Seleccionar Escenario
//''' Si no hay escenario alguno, posible con permiso de "Ocultar el escenario por defecto", no hace nada. 
//''' </summary>
//''' <param name="idEscenario">id Escenario</param>
//''' <param name="callback">funcio opcional a ejecutar de ya estar cargado de bbdd el escenario</param>
//''' <remarks>Llamada desde: document.ready     OrdenarEscenarios    escenario.live'click'   btnBuscarPorIdentificador.live'click'    Ir_A_Carpeta     escenarioFavorito.live'click'  lnkSolicitudesPendientes.live'click'   Seleccionar_Carpeta; Tiempo mÃ¡ximo: 0</remarks>
function Seleccionar_Escenario(idEscenario, callback) {
    //Cancelamos todas las consultas anteriores (escenario y filtros)
    $.each(consultas, function (i, x) {
        if (x && x.readyState != 4) {
            x.abort();
            delete x;
        }
    });
    //Cancelamos el postback que se esté ejecutando (solicitudes)
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm.get_isInAsyncPostBack()) prm.abortPostBack();

    idEscenarioSeleccionado = idEscenario;
    var selectedEscenario = escenarios[idEscenarioSeleccionado];
    $('#panelEscenarios .escenarioSeleccionado').removeClass('escenarioSeleccionado');
    $('#escenario_' + idEscenarioSeleccionado).addClass('escenarioSeleccionado');
    if (!$('#escenario_' + idEscenarioSeleccionado).hasClass('escenarioSeleccionado')) $('#escenario_' + idEscenarioSeleccionado).addClass('escenarioSeleccionado');

    if (selectedEscenario == undefined) {
        $('#lblEscenariosSeleccionado').text('');
        $('[id$=chkEscenariosDefecto]').prop('checked', false);
        $('#btnEscenarioCompartir').prop('title', '');

        $('#btnEscenarioEditar').hide();
        $('#btnEscenarioEliminar').hide();
        $('#btnEscenarioCompartir').hide();

        return false;
    }

    $('#lblEscenariosSeleccionado').text(selectedEscenario.Nombre);
    $('[id$=chkEscenariosDefecto]').prop('checked', selectedEscenario.Defecto);

    //Establecemos el tooltip del botón de compartir
    if (escenarios[idEscenarioSeleccionado].Modo == MODOSESCENARIO.NO_COMPARTIDO)
        $('#btnEscenarioCompartir').prop('title', TextosPantalla[107]); //Crear el escenario para otros usuarios
    else
        $('#btnEscenarioCompartir').prop('title', TextosPantalla[115]); //Modificar usuarios compartidos    

    if (escenarios[idEscenarioSeleccionado].Editable) {
        $('#btnEscenarioEditar').show();
        $('#btnEscenarioEliminar').show();
    } else {
        $('#btnEscenarioEditar').hide();
        $('#btnEscenarioEliminar').hide();
    };
    if ((selectedEscenario.Modo == MODOSESCENARIO.NO_COMPARTIDO && (permisos.CREAR_ESCENARIOS_PARA_OTROS_USUARIOS || permisos.CREAR_ESCENARIOS_PARA_PORTAL) && escenarios[idEscenarioSeleccionado].Id !== 0) ||
        (selectedEscenario.Modo != MODOSESCENARIO.NO_COMPARTIDO && escenarios[idEscenarioSeleccionado].Editable))
        $('#btnEscenarioCompartir').show();
    else
        $('#btnEscenarioCompartir').hide();

    //Si no esta cargado el escenario, lo cargamos
    if (!selectedEscenario.Cargado) {
        MostrarCargando();

        //Ocultar filtros
        $('#panelEscenarioFiltros').empty();
        $('#panelEscenarioFiltros').append('<img id="imgCargandoFiltros" src="' + ruta + 'images/cargando.gif" alt="" />');

        //Ocultar vistas
        $('#panelEscenarioVistas .escenarioVista').remove();

        //Cargamos el escenario
        var xhr = $.ajax({
            type: "POST",
            url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Escenario',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ IdEscenario: idEscenario, Edicion: false, iTipoVisor: tipoVisor }),
            async: true
        }).done(function (msg) {
            //Eliminamos la consulta ya devuelta
            if (consultas.Escenario !== undefined) delete consultas.Escenario;

            OcultarCargando();
            var objectInfo = $.parseJSON(msg.d);
            var infoEscenario = objectInfo[0];
            var posicionAnteriorEscenario = escenarios[infoEscenario.Id].Posicion;
            escenarios[infoEscenario.Id] = infoEscenario;
            escenarios[infoEscenario.Id].Posicion = posicionAnteriorEscenario;
            //Llamamos al callback
            if (typeof callback == "function") callback();
        });
        //Añadimos la peticion ajax a las consultas
        consultas['Escenario'] = xhr;
    } else {
        //El escenario está cargado, llamamos al callback
        if (typeof callback == "function") callback();
    }
}
function OrdenarEscenarios(opcionesUsu) {
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Establecer_Opciones_Usu',
        contentType: "application/json; charset=utf-8",
        data: opcionesUsu,
        dataType: "json",
        async: true
    })).done(function (msg) {
        var idEscenarioSeleccionadoVirtual = idEscenarioSeleccionado;
        Seleccionar_Carpeta(idCarpetaSeleccionada);
        Seleccionar_Escenario(idEscenarioSeleccionadoVirtual);
    });
};

//#endregion

//#region Filtros

//Click en la barra de Filtros
$('#cabeceraEscenarioFiltros').live('click', function () {
    $('#panelEscenarioFiltros').toggle();
    $('[id$=imgEscenarioFiltrosContraerBarra]').toggle();
    $('[id$=imgEscenarioFiltrosExpandirBarra]').toggle();
    //Actualizamos el resto de contadores
    ActualizarContadoresFiltro();
});
//Click en el botón de Ordenar Filtros
$('#cabeceraEscenarioFiltros [id$=imgEscenarioFiltrosOrder]').live('click', function (event) {
    ejecutandoPartialPostBack = true;
    $("#listaOrder").sortable({
        cancel: '.ui-state-disabled',
        containment: 'parent',
        axis: 'y',
        scroll: true,
        scrollSensitivity: 50,
        scrollSpeed: 50,
        cursor: 'move',
        placeholder: 'ui-state-highlight',
        forcePlaceholderSize: true,
        start: function (event, ui) {
            var start_pos = ui.item.index();
            ui.item.data('start_pos', start_pos);
        },
        beforeStop: function (event, ui) {
            var idEscenarioOrden, idFiltroOrden, posicionAnterior, posicionActual;
            idFiltroOrden = parseInt(ui.item.attr('id').replace('ordenEscenarioFiltroVista_', ''));
            posicionAnterior = (ui.item.data('start_pos') - 1);
            posicionActual = (ui.item.index() - 1);
            if (posicionActual !== posicionAnterior) {
                $.each(escenarios[idEscenarioSeleccionado].EscenarioFiltros, function () {
                    if (posicionActual < posicionAnterior) {
                        if (this.Posicion > posicionActual - 1 && this.Posicion < posicionAnterior) this.Posicion = this.Posicion + 1;
                    } else {
                        if (this.Posicion > posicionAnterior && this.Posicion < posicionActual + 1) this.Posicion = this.Posicion - 1;
                    };
                });
                escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroOrden].Posicion = posicionActual;
                var opcionesUsu = JSON.stringify({ tipoOpcion: 2, oOpcionesUsu: JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioFiltros) });
                OrdenarFiltros(opcionesUsu);
            };
        }
    });
    $("#listaOrder").disableSelection();
    $('#lblCabeceraPanelOrder').text(TextosPantalla[93]);
    $('#btnCerrarPanelOrder').text(TextosPantalla[89]);
    var filtrosOrdenar = $.map($.parseJSON(JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioFiltros)), function (x) { return x; });
    filtrosOrdenar.unshift({ Nombre: '' });
    $('#ordenEscenarioFiltroVista').tmpl(filtrosOrdenar.sort(ordenPorPosicion)).appendTo($('#panelOrder ul'));
    $('#popupFondo').css('height', $(document).height());
    $('#popupFondo').show();
    CentrarPopUp($('#panelOrder'));
    $('#panelOrder').show();
    event.stopPropagation();
});
//Click en el checkbox de Filtro Por Defecto
$('[id$=chkEscenarioFiltrosDefecto]').live('click', function (event) {
    event.stopPropagation();
    var checkDefecto = $(this);
    if (checkDefecto.prop('checked')) {
        var filtroDefectoAnterior = $.map(escenarios[idEscenarioSeleccionado].EscenarioFiltros, function (x) { if (x.Defecto) return x; });
        if (filtroDefectoAnterior.length == 1) escenarios[idEscenarioSeleccionado].EscenarioFiltros[filtroDefectoAnterior[0].Id].Defecto = false;
    };
    escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].Defecto = checkDefecto.prop('checked');
    var opcionesUsu = JSON.stringify({ tipoOpcion: 2, oOpcionesUsu: JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioFiltros) });
    $.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Establecer_Opciones_Usu',
        contentType: "application/json; charset=utf-8",
        data: opcionesUsu,
        dataType: "json",
        async: true
    });
});
//Mouseenter y mouseleave en el botón de fórmula del filtro
$('[id$=imgEscenarioFiltrosFormula]').live('mouseenter mouseleave', function () {
    if (idFiltroSeleccionado !== undefined && escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado]) {
        //Establecemos los textos en lenguaje natural en la propiedad "Condicion" de cada condición de la fórmula
        EstablecerTextosCondiciones(escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].FormulaCondiciones);
        var formula = '';
        $.each(escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].FormulaCondiciones, function () {
            formula += ' ' + this.Condicion;
        });
        if (formula == '') formula = '(' + TextosPantalla[106] + ')';
        $('#divFormulaFiltro').text(formula);
        $('#divFormulaFiltro').css('left', $(this).position().left + $(this).outerWidth());
        $('#divFormulaFiltro').css('top', $(this).position().top - $(this).outerHeight());
        $('#divFormulaFiltro').toggle();
    };
});
//Click en el botón de fórmula del filtro
$('[id$=imgEscenarioFiltrosFormula]').live('click', function (event) {
    event.stopPropagation();
    if (escenarios[idEscenarioSeleccionado].Editable) $('#btnEscenarioFiltroEditar').click();
});
//Click en el botón Guardar del filtro
$('#btnEscenarioFiltroGuardar').live('click', function () {
    //Cargamos los valores del de la pantalla en las condiciones del filtro
    if (escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].FormulaAvanzada) {
        Obtener_FiltroCondiciones_Buscar_Avanzada();
        MostrarCargando();
        $.ajax({
            type: "POST",
            url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Escenario',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ IdEscenario: idEscenarioSeleccionado, Edicion: false, iTipoVisor: tipoVisor }),
            async: true
        }).done(function (msg) {
            var condicionInicial, nuevaCondicion;
            var todoOKCondiciones = true;
            $.each($.parseJSON(msg.d)[0].EscenarioFiltros[idFiltroSeleccionado].FormulaCondiciones, function () {
                condicionInicial = this;
                nuevaCondicion = $.map(escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].FormulaCondiciones, function (x) {
                    if (x.IdCampo == condicionInicial.IdCampo && x.EsCampoGeneral == condicionInicial.EsCampoGeneral && x.Orden == condicionInicial.Orden) return x;
                });
                if (nuevaCondicion.length == 1 && nuevaCondicion[0].Valores.length == 0 && nuevaCondicion[0].IdCampo != 0) {
                    todoOKCondiciones = false;
                    return false;
                };
            });
            OcultarCargando();
            if (todoOKCondiciones) Guardar_Filtro();
            else alert(TextosPantalla[116].replace('#####', condicionInicial.Denominacion));
        });
    } else {
        Obtener_FiltroCondiciones_Buscar_Basico();
        Guardar_Filtro();
    };
    return false;
});
//Click en un Filtro
$('.escenarioFiltro').live('click', function () {
    if (parseInt($(this).attr('id').split('_')[1]) !== idFiltroSeleccionado) {
        idCarpetaActualVolver = undefined;
        //Cancelamos todas las consultas anteriores (escenario y filtros)
        $.each(consultas, function (i, x) {
            if (x && x.readyState != 4) {
                x.abort();
                delete x;
            }
        });
        //Cancelamos el postback que se esté ejecutando (solicitudes)
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm.get_isInAsyncPostBack()) prm.abortPostBack();

        Seleccionar_Filtro(parseInt($(this).attr('id').split('_')[1]));
        Cargar_Campos_Condiciones_Filtro();
        if (escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].IdVistaDefecto !== 0)
            Seleccionar_Vista(escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].IdVistaDefecto);

        resetInstanciasCheck();
        Cargar_Solicitudes();
    };
});
//Click en el separador de las Condiciones del filtro
$('#cabeceraEscenarioFiltroOpciones').live('click', function () {
    $('#panelEscenarioFiltrosOpcionesFiltrado').toggle();
    $('[id$=imgEscenarioFiltroOpcionesCollapse]').toggle();
    $('[id$=imgEscenarioFiltroOpcionesExpand]').toggle();
    $('#btnBuscarSolicitudes').toggle();
});
//Click en el checkbox de filtro Por Defecto
$('[id$=chkEscenarioFiltrosDefecto]').live('click', function (event) {
    var checkDefecto = $(this);
    if (checkDefecto.prop('checked') || idEscenarioSeleccionado !== 0) {
        $.ajax({
            type: "POST",
            url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Establecer_Filtro_Defecto',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ IdFiltro: idFiltroSeleccionado, Defecto: checkDefecto.prop('checked'), iTipoVisor: tipoVisor }),
            async: true
        });
        $.each(escenarios[idEscenarioSeleccionado].EscenarioFiltros, function () {
            if (checkDefecto.prop('checked')) {
                if (this.Id == idEscenarioSeleccionado) this.Defecto = true;
                else this.Defecto = false;
            } else {
                if (this.Id == 0) this.Defecto = true;
                else this.Defecto = false;
            };
        });
    } else return false;
    event.stopPropagation();
});
function Guardar_Filtro() {
    var escenarioGuardarFiltro = $.parseJSON(JSON.stringify(escenarios[idEscenarioSeleccionado]));
    escenarioGuardarFiltro.EscenarioFiltros[idFiltroSeleccionado] = $.parseJSON(JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado]));
    var opcionesUsu = JSON.stringify({ tipoOpcion: 2, oOpcionesUsu: JSON.stringify(escenarioGuardarFiltro.EscenarioFiltros) });
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Establecer_Opciones_Usu',
        contentType: "application/json; charset=utf-8",
        data: opcionesUsu,
        dataType: "json",
        async: true
    })).done(function (msg) {
        var params = JSON.stringify({ Filtro: JSON.stringify(escenarioGuardarFiltro.EscenarioFiltros[idFiltroSeleccionado]), iTipoVisor: tipoVisor });
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Update_Escenario_Filtro_Condiciones',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        })).done(function (msg) {
            var info = $.parseJSON(msg.d);
            escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado] = info;
            $('[id$=imgGuardadoFiltro]').show();
            setTimeout(function () { $('[id$=imgGuardadoFiltro]').hide(); }, 3000);
        });
    });
};
function Crear_Filtros() {
    $('#panelEscenarioFiltros').empty();
    //Obtenemos cual es el filtro por defecto
    var arrFiltros = $.map(escenarios[idEscenarioSeleccionado].EscenarioFiltros, function (x) { return x; }).sort(ordenPorPosicion); //Necesito el array para poder hacer grep
    filtroDefecto = $.grep(arrFiltros, function (x) { return x.Defecto; });

    if (typeof (filtroVolver) != 'undefined') {
        //Hemos vuelto de otra página, Seleccionamos el filtro en el que estábamos
        idFiltroSeleccionado = filtroVolver;
        filtroVolver = undefined;
    } else {
        //Seleccionamos el filtro por defecto y si no existe, el primero
        idFiltroSeleccionado = filtroDefecto.length == 0 ? (arrFiltros.length == 0 ? null : arrFiltros[0].Id) : filtroDefecto[0].Id;
    }

    $('#escenarioFiltro').tmpl($.map(escenarios[idEscenarioSeleccionado].EscenarioFiltros, function (escenarioFiltro) { return escenarioFiltro }).sort(ordenPorPosicion)).appendTo($('#panelEscenarioFiltros'));
    if (!$('#escenarioFiltro_' + idFiltroSeleccionado).hasClass('escenarioSeleccionado')) $('#escenarioFiltro_' + idFiltroSeleccionado).addClass('escenarioSeleccionado');
    $('[id^=escenarioFiltro_] img').parent('[id^=escenarioFiltro_]').prop('title', TextosPantalla[71]);
}

//''' <summary>
//''' Seleccionar Filtro
//''' Si no hay escenario alguno, posible con permiso de "Ocultar el escenario por defecto", se limita a ocultar los botones de escenario excepto Nuevo. 
//''' </summary>
//''' <param name="idFiltro">id Filtro</param>
//''' <remarks>Llamada desde: document.ready   OrdenarFiltros     escenarioFiltro.live'click'      escenario.live'click'      escenarioFavorito.live'click'     btnBuscarPorIdentificador.live'click'        lnkSolicitudesPendientes.live'click'   
//'''           Ir_A_Carpeta     Seleccionar_Carpeta; Tiempo mÃ¡ximo: 0</remarks>
function Seleccionar_Filtro(idFiltro) {
    idFiltroSeleccionado = idFiltro;
    $('#panelEscenarioFiltros .escenarioSeleccionado').removeClass('escenarioSeleccionado');
    if (idEscenarioSeleccionado !== null) {
        var selectedEscenarioFiltro = escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado];
        $('#lblEscenarioFiltrosSeleccionado').text(selectedEscenarioFiltro.Nombre);
        $('#escenarioFiltro_' + idFiltroSeleccionado).addClass('escenarioSeleccionado');
        $('[id$=chkEscenarioFiltrosDefecto]').prop('checked', selectedEscenarioFiltro.Defecto);
    }

    if (typeof (formulaCondicionesSession) !== 'undefined') {
        escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].FormulaCondiciones = formulaCondicionesSession;
        formulaCondicionesSession = undefined;
    };
    //Si el escenario es el de por defecto no podremos modificar las opciones de filtrado ni establecer un filtro por defecto para ese escenario
    if (idEscenarioSeleccionado !== 0) {
        $('#btnEscenarioFiltroGuardar').show();
        $('[id$=chkEscenarioFiltrosDefecto]').parent().show();
    } else {
        $('#btnEscenarioFiltroGuardar').hide();
        $('[id$=chkEscenarioFiltrosDefecto]').parent().hide();
    };
    if (escenarios[idEscenarioSeleccionado].Editable) {
        //El botón de eliminar filtro sólo lo mostramos si hay más de uno
        if ($.map(escenarios[idEscenarioSeleccionado].EscenarioFiltros, function (x) { return x; }).length > 1) $('#btnEscenarioFiltroEliminar').show();
        else $('#btnEscenarioFiltroEliminar').hide();
        $('#btnEscenarioFiltroEditar').show();
        $('#btnEscenarioFiltroNuevo').show();
    } else {
        $('#btnEscenarioFiltroEliminar').hide();
        $('#btnEscenarioFiltroEditar').hide();
        $('#btnEscenarioFiltroNuevo').hide();
    };
    if ($.map(escenarios[idEscenarioSeleccionado].EscenarioFiltros, function (x) { return x; }).length == 1 || idEscenarioSeleccionado == 0) $('#cabeceraEscenarioFiltros [src*=sort]').hide();
    else $('#cabeceraEscenarioFiltros [src*=sort]').show();
}
function OrdenarFiltros(opcionesUsu) {
    //Cancelamos todas las consultas anteriores (filtros)
    $.each(consultas, function (i, x) {
        if (i != 'Escenario' && x && x.readyState != 4) {
            x.abort();
            delete x;
        }
    });
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Establecer_Opciones_Usu',
        contentType: "application/json; charset=utf-8",
        data: opcionesUsu,
        dataType: "json",
        async: true
    })).done(function (msg) {
        var idFiltroSeleccionadoVirtual = idFiltroSeleccionado;
        Crear_Filtros();
        Seleccionar_Filtro(idFiltroSeleccionadoVirtual);
        //Mostramos el contador del filtro seleccionado, ya que no ha cambiado
        $('#numSolicitudes_' + idFiltroSeleccionadoVirtual).show();
    });
};
function Cargar_Campos_Condiciones_Filtro() {
    $('#panelEscenarioFiltrosOpcionesFiltrado').empty();
    var camposPosiblesFiltrado = [];
    var indicesCamposFiltrado = {};
    escenarioFiltroCondicionesOrden[idEscenarioSeleccionado] = [];
    escenarioFiltroCondicionesOrden[idEscenarioSeleccionado][idFiltroSeleccionado] = [];
    $.each(escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].FormulaCondiciones, function () {
        var campoCondicion = this;
        $.each(escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].Filtro_Campos, function () {
            if (this.EsCampoGeneral == campoCondicion.EsCampoGeneral && this.Id == campoCondicion.IdCampo) {
                camposPosiblesFiltrado.push($.parseJSON(JSON.stringify(this)));
                if (indicesCamposFiltrado[this.Id] == undefined) indicesCamposFiltrado[this.Id] = {};
                indicesCamposFiltrado[this.Id].index = (indicesCamposFiltrado[this.Id].index == undefined ? 1 : indicesCamposFiltrado[this.Id].index + 1);
                camposPosiblesFiltrado[camposPosiblesFiltrado.length - 1].index = indicesCamposFiltrado[this.Id].index;
                camposPosiblesFiltrado[camposPosiblesFiltrado.length - 1].PosicionEnFormula = campoCondicion.Orden;
                return false;
            };
        });
    });
    $.each(escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].Filtro_Campos, function () {
        if (indicesCamposFiltrado[this.Id] == undefined) {
            camposPosiblesFiltrado.push(this);
            indicesCamposFiltrado[this.Id] = {};
            indicesCamposFiltrado[this.Id].index = 1;
            camposPosiblesFiltrado[camposPosiblesFiltrado.length - 1].index = 1;
            camposPosiblesFiltrado[camposPosiblesFiltrado.length - 1].PosicionEnFormula = 0;
        };
    });
    if (escenarioFiltroCondicionesOrden[idEscenarioSeleccionado][idFiltroSeleccionado].length == 0)
        escenarioFiltroCondicionesOrden[idEscenarioSeleccionado][idFiltroSeleccionado] = camposPosiblesFiltrado;
    else {
        $.each(escenarioFiltroCondicionesOrden[idEscenarioSeleccionado][idFiltroSeleccionado], function () {
            var campoFiltroOrden = this;
            $.each(camposPosiblesFiltrado, function () {
                if (this.Id == campoFiltroOrden.Id) campoFiltroOrden = this;
            });
        });
        camposPosiblesFiltrado = escenarioFiltroCondicionesOrden[idEscenarioSeleccionado][idFiltroSeleccionado];
    };
    $.each(camposPosiblesFiltrado, function () {
        var campoFiltrado = this;
        if (indicesCamposFiltrado[campoFiltrado.Id].index == 1) campoFiltrado.index = 0;
        else {
            indicesCamposFiltrado[campoFiltrado.Id].indexActual = (indicesCamposFiltrado[campoFiltrado.Id].indexActual == undefined ? 1 : indicesCamposFiltrado[campoFiltrado.Id].indexActual + 1);
            campoFiltrado.index = indicesCamposFiltrado[campoFiltrado.Id].indexActual;
        };
        if (escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].FormulaAvanzada) {
            $('#escenarioFiltroCampoOpcion_Avanzado').tmpl(campoFiltrado).appendTo($('#panelEscenarioFiltrosOpcionesFiltrado'));
            var operadoresString = [];
            var opcionesSelect = $('#EscenarioFiltro_CampoOpcion' + (campoFiltrado.EsCampoGeneral ? 'General_' : 'Formulario_') + campoFiltrado.Id + '[index="' + (campoFiltrado.index == 0 ? 1 : campoFiltrado.index) + '"] select.CabeceraBotones').prop('options');
            if (!campoFiltrado.EsLista) {
                switch (campoFiltrado.TipoCampoGS) {
                    case TIPOSCAMPOGS.USUARIO: //USUARIO
                    case TIPOSCAMPOGS.PETICIONARIO: //PETICIONARIO
                    case TIPOSCAMPOGS.COMPRADOR: //COMPRADOR
                        operadoresString = [OPERADORES.ES, OPERADORES.NOES]; // IN , NOT IN
                        $.each(operadoresString, function () {
                            opcionesSelect[opcionesSelect.length] = new Option(DENOMINACION_OPERADORES[this], this);
                        });
                        break;
                    case TIPOSCAMPOGS.PROVEEDORADJ: //PROVEEDOR
                    case TIPOSCAMPOGS.PROVEEDOR: //PROVEEDOR
                    case TIPOSCAMPOGS.MATERIAL: //MATERIAL                    
                    case TIPOSCAMPOGS.PAIS: //PAIS
                    case TIPOSCAMPOGS.PROVINCIA: //PROVINCIA
                    case TIPOSCAMPOGS.PROVECONTACTO: //PROVEEDOR 
                    case TIPOSCAMPOGS.PERSONA:
                    case TIPOSCAMPOGS.EMPRESA:
                        operadoresString = [OPERADORES.IGUAL, OPERADORES.DISTINTO]; // = , <>
                        $.each(operadoresString, function () {
                            opcionesSelect[opcionesSelect.length] = new Option(DENOMINACION_OPERADORES[this], this);
                        });
                        break;
                    case TIPOSCAMPOGS.CODARTICULO: //ARTICULO
                    case TIPOSCAMPOGS.DENARTICULO: //ARTICULO
                    case TIPOSCAMPOGS.NUEVOCODARTICULO: //ARTICULO
                        operadoresString = [OPERADORES.IGUAL, OPERADORES.DISTINTO, OPERADORES.CONTIENE, OPERADORES.EMPIEZAPOR, OPERADORES.TERMINAEN];
                        $.each(operadoresString, function () {
                            opcionesSelect[opcionesSelect.length] = new Option(DENOMINACION_OPERADORES[this], this);
                        });
                        break;
                    case TIPOSCAMPOGS.UNIDADORGANIZATIVA: //Unidad Organizativa
                        var unOrganizativaValue = $('input[id^=txtCampo][id*=Filtro][id*=_UnidadOrganizativaValue]', contenedor);
                        var unOrganizativa = $('input[id^=txtCampo][id*=Filtro][id*=_UnidadOrganizativa]:not([id*=_UnidadOrganizativaValue])', contenedor);
                        if (unOrganizativaValue.val() !== '') {
                            if (isNaN(condicion.Operador)) condicion.Operador = OPERADORES.IGUAL;
                            condicion.Valores = [unOrganizativaValue.val(), unOrganizativa.val()];
                        }
                        break;
                    default:
                        switch (campoFiltrado.TipoCampo) {
                            case 1:
                            case 5:
                            case 6:
                            case 7: //String
                                operadoresString = [OPERADORES.IGUAL, OPERADORES.DISTINTO, OPERADORES.CONTIENE, OPERADORES.EMPIEZAPOR, OPERADORES.TERMINAEN];
                                $.each(operadoresString, function () {
                                    opcionesSelect[opcionesSelect.length] = new Option(DENOMINACION_OPERADORES[this], this);
                                });
                                break;
                            case 2: //Numerico
                            case 3: //Fecha  
                                operadoresString = [OPERADORES.IGUAL, OPERADORES.DISTINTO, OPERADORES.MAYOR, OPERADORES.MAYORIGUAL, OPERADORES.MENOR, OPERADORES.MENORIGUAL, OPERADORES.ENTRE];
                                $.each(operadoresString, function () {
                                    opcionesSelect[opcionesSelect.length] = new Option(DENOMINACION_OPERADORES[this], this);
                                });
                        };
                        break;
                };
            } else {
                operadoresString = [OPERADORES.ES, OPERADORES.NOES];
                $.each(operadoresString, function () {
                    opcionesSelect[opcionesSelect.length] = new Option(DENOMINACION_OPERADORES[this], this);
                });
            };
        } else $('#escenarioFiltroCampoOpcion_Basico').tmpl(campoFiltrado).appendTo($('#panelEscenarioFiltrosOpcionesFiltrado'));
        var campoOpcionFiltrado;
        if (this.EsCampoGeneral) {
            campoOpcionFiltrado = $('#EscenarioFiltro_CampoOpcionGeneral_' + this.Id + '[index="' + (this.index == 0 ? 1 : this.index) + '"]');
            campoOpcionFiltrado.data('campoEscenario', this);
            if (this.OpcionesLista.length !== 0) campoOpcionFiltrado.data('data', this.OpcionesLista);
        } else {
            campoOpcionFiltrado = $('#EscenarioFiltro_CampoOpcionFormulario_' + this.Id + (this.EsCampoDesglose ? '_' + this.CampoDesglose : '') + '[index="' + (this.index == 0 ? 1 : this.index) + '"]');
            campoOpcionFiltrado.data('campoEscenario', this);
            if (this.OpcionesLista.length !== 0) campoOpcionFiltrado.data('data', this.OpcionesLista);
        };
        if (this.TipoCampo == 3) {//Fecha
            CargarOpcionesFechasRelativas(campoOpcionFiltrado.find('select[tipocombofecha=1]'));
            CargarOpcionesFechasPeriodo(campoOpcionFiltrado.find('select[tipocombofecha=2]'));
        };
    });
    $.each(escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].FormulaCondiciones, function () {
        if (this.IdCampo > 0) {
            var contenedorCondicionFiltro;
            if (this.EsCampoGeneral) {
                if (indicesCamposFiltrado[this.IdCampo].index == 1) {
                    contenedorCondicionFiltro = $('#EscenarioFiltro_CampoOpcionGeneral_' + this.IdCampo + '[index="1"]');
                    indicesCamposFiltrado[this.IdCampo].indexCondicion = 1;
                } else {
                    indicesCamposFiltrado[this.IdCampo].indexCondicion = (indicesCamposFiltrado[this.IdCampo].indexCondicion == undefined ? 1 : indicesCamposFiltrado[this.IdCampo].indexCondicion + 1);
                    contenedorCondicionFiltro = $('#EscenarioFiltro_CampoOpcionGeneral_' + this.IdCampo + '[index="' + indicesCamposFiltrado[this.IdCampo].indexCondicion + '"]');
                };
                switch (this.TipoCampoGS) {
                    case TIPOSCAMPOGS.USUARIO:
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_UsuarioValue_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_Usuario_' + this.IdCampo).val(this.Valores[1]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_UsuarioValueAvanzado_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_UsuarioAvanzado_' + this.IdCampo).val(this.Valores[1]);
                        break;
                    case TIPOSCAMPOGS.PETICIONARIO:
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_PeticionarioValue_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_Peticionario_' + this.IdCampo).val(this.Valores[1]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_PeticionarioValueAvanzado_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_PeticionarioAvanzado_' + this.IdCampo).val(this.Valores[1]);
                        break;
                    case TIPOSCAMPOGS.PERSONA:
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_PersonaValue_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_Persona_' + this.IdCampo).val(this.Valores[1]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_PersonaValueAvanzado_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_PersonaAvanzado_' + this.IdCampo).val(this.Valores[1]);
                        break;
                    case TIPOSCAMPOGS.PROVEEDORADJ:
                    case TIPOSCAMPOGS.PROVEEDOR:
                    case TIPOSCAMPOGS.PROVECONTACTO: //Proveedor                        
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_ProveedorValue_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_Proveedor_' + this.IdCampo).val(this.Valores[1]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_ProveedorValueAvanzado_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_ProveedorAvanzado_' + this.IdCampo).val(this.Valores[1]);
                        break;
                    case TIPOSCAMPOGS.CODARTICULO:
                    case TIPOSCAMPOGS.DENARTICULO:
                    case TIPOSCAMPOGS.NUEVOCODARTICULO: //Articulo
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_ArticuloValue_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_Articulo_' + this.IdCampo).val(this.Valores[1]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_ArticuloValueAvanzado_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_ArticuloAvanzado_' + this.IdCampo).val(this.Valores[1]);
                        break;
                    case TIPOSCAMPOGS.EMPRESA:
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_EmpresaValue_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_Empresa_' + this.IdCampo).val(this.Valores[1]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_EmpresaValueAvanzado_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_EmpresaAvanzado_' + this.IdCampo).val(this.Valores[1]);
                        break;
                };
            } else {
                if (indicesCamposFiltrado[this.IdCampo].index == 1) contenedorCondicionFiltro = $('#EscenarioFiltro_CampoOpcionFormulario_' + this.IdCampo);
                else {
                    indicesCamposFiltrado[this.IdCampo].indexCondicion = (indicesCamposFiltrado[this.IdCampo].indexCondicion == undefined ? 0 : indicesCamposFiltrado[this.IdCampo].indexCondicion + 1);
                    contenedorCondicionFiltro = $($('#EscenarioFiltro_CampoOpcionFormulario_' + this.IdCampo + '[index]')[indicesCamposFiltrado[this.IdCampo].indexCondicion]);
                };
                switch (this.TipoCampoGS) {
                    case TIPOSCAMPOGS.PROVEEDORADJ:
                    case TIPOSCAMPOGS.PROVEEDOR:
                    case TIPOSCAMPOGS.PROVECONTACTO: //Proveedor                       
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_ProveedorValue_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_Proveedor_' + this.IdCampo).val(this.Valores[1]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_ProveedorValueAvanzado_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_ProveedorAvanzado_' + this.IdCampo).val(this.Valores[1]);
                        break;
                    case TIPOSCAMPOGS.MATERIAL: //Material
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_MaterialValue_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_Material_' + this.IdCampo).val(this.Valores[1]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_MaterialValueAvanzado_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_MaterialAvanzado_' + this.IdCampo).val(this.Valores[1]);
                        break;
                    case TIPOSCAMPOGS.PERSONA: //Persona
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_PersonaValue_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_Persona_' + this.IdCampo).val(this.Valores[1]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_PersonaValueAvanzado_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_PersonaAvanzado_' + this.IdCampo).val(this.Valores[1]);
                        break;
                    case TIPOSCAMPOGS.UNIDADORGANIZATIVA: //Unidad organizativa
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_UnidadOrganizativaValue_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_UnidadOrganizativa_' + this.IdCampo).val(this.Valores[1]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_UnidadOrganizativaValueAvanzado_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_UnidadOrganizativaAvanzado_' + this.IdCampo).val(this.Valores[1]);
                        break;
                    case TIPOSCAMPOGS.COMPRADOR: //Comprador
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_CompradorValue_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_Comprador_' + this.IdCampo).val(this.Valores[1]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_CompradorValueAvanzado_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_CompradorAvanzado_' + this.IdCampo).val(this.Valores[1]);
                        break;
                    case TIPOSCAMPOGS.CODARTICULO: //ARTICULO
                    case TIPOSCAMPOGS.DENARTICULO: //ARTICULO
                    case TIPOSCAMPOGS.NUEVOCODARTICULO: //ARTICULO
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_ArticuloValue_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_Articulo_' + this.IdCampo).val(this.Valores[1]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_ArticuloValueAvanzado_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_ArticuloAvanzado_' + this.IdCampo).val(this.Valores[1]);
                        break;
                    case TIPOSCAMPOGS.EMPRESA:
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_EmpresaValue_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_Empresa_' + this.IdCampo).val(this.Valores[1]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_EmpresaValueAvanzado_' + this.IdCampo).val(this.Valores[0]);
                        contenedorCondicionFiltro.find('#txtCampoOpcionFiltro_EmpresaAvanzado_' + this.IdCampo).val(this.Valores[1]);
                        break;
                };
            };
            contenedorCondicionFiltro.data('selectedValues', this.Valores);
            if (this.TipoCampo == 3) {//Fecha
                contenedorCondicionFiltro.find('div[index]').removeClass('inlineBlock');
                contenedorCondicionFiltro.find('div[index]').addClass('Ocultar');
                switch (this.Operador) {
                    case OPERADORES.HACE: //Hace
                        $('img[name=ModoFecha]', contenedorCondicionFiltro).attr('data-type', 1);
                        contenedorCondicionFiltro.find('div[index=1]').removeClass('Ocultar');
                        contenedorCondicionFiltro.find('div[index=1]').addClass('inlineBlock');
                        contenedorCondicionFiltro.find('div[index=1] input').val(this.Valores[1]);
                        contenedorCondicionFiltro.find('div[index=1] select.CabeceraBotones option[value=' + this.Valores[0] + ']').prop('selected', true);
                        break;
                    case OPERADORES.PERIODO: //Periodo
                        $('img[name=ModoFecha]', contenedorCondicionFiltro).attr('data-type', 2);
                        contenedorCondicionFiltro.find('div[index=2]').removeClass('Ocultar');
                        contenedorCondicionFiltro.find('div[index=2]').addClass('inlineBlock');
                        contenedorCondicionFiltro.find('div[index=2] select.CabeceraBotones option[value=' + this.Valores[0] + ']').prop('selected', true);
                        break;
                    case OPERADORES.ENTRE:
                        contenedorCondicionFiltro.find('div[index=0] span').show();
                        contenedorCondicionFiltro.find('div[index=0] div').visible();
                    default:
                        $('img[name=ModoFecha]', contenedorCondicionFiltro).attr('data-type', 0);
                        contenedorCondicionFiltro.find('div[index=0]').removeClass('Ocultar');
                        contenedorCondicionFiltro.find('div[index=0]').addClass('inlineBlock');
                        break;
                };
            };

            //Establecemos el valor al combo de operadores
            var cboOperadores = $('#EscenarioFiltro_CampoOpcion' + (this.EsCampoGeneral ? 'General_' : 'Formulario_') + this.IdCampo + '[index="' + indicesCamposFiltrado[this.IdCampo].index + '"] select.CabeceraBotones');
            cboOperadores.find('option[value=' + this.Operador + ']').prop('selected', true);
            //Forzamos el evento de cambio del combo de operadores
            cboOperadores.trigger('change');
        };
    });
    $.each($('[id^=txtCampoOpcionFiltro_String_]'), function () {
        $(this).val($(this).closest('fieldset').data('selectedValues'));
    });
    $.each($('[id^=selectCampoOpcionFiltro_Lista_]'), function () {
        var opt;
        var elementoLista = $(this);
        var sel = $(this).multiselect({
            multiple: ($(this).closest('fieldset').data('campoEscenario').TipoCampo !== 4 && $(this).closest('fieldset').data('campoEscenario').TipoCampoGS !== 107 && $(this).closest('fieldset').data('campoEscenario').TipoCampoGS !== 108),
            header: ($(this).closest('fieldset').data('campoEscenario').TipoCampo !== 4),
            selectedList: 5,
            noneSelectedText: '',
            selectedText: TextosPantalla[67],
            checkAllText: TextosPantalla[65],
            uncheckAllText: TextosPantalla[66],
            click: function () {
                if ($(this).closest('fieldset').data('campoEscenario').TipoCampoGS == 107) {
                    $('#' + elementoLista.attr('id').replace(elementoLista.attr('id').split('_')[elementoLista.attr('id').split('_').length - 1], elementoLista.closest('fieldset').data('campoEscenario').CampoHijo)).closest('fieldset').removeData('data');
                    $('#' + elementoLista.attr('id').replace(elementoLista.attr('id').split('_')[elementoLista.attr('id').split('_').length - 1], elementoLista.closest('fieldset').data('campoEscenario').CampoHijo)).multiselect('uncheckAll')
                };
            },
            beforeopen: function () {
                if ($(this).closest('fieldset').data('data') == undefined) {
                    var el = this;
                    if (elementoLista.closest('fieldset').data('campoEscenario').TipoCampoGS == 108) {
                        var valorPais = $('#' + elementoLista.attr('id').replace(elementoLista.attr('id').split('_')[elementoLista.attr('id').split('_').length - 1], elementoLista.closest('fieldset').data('campoEscenario').CampoPadre)).multiselect('getChecked');
                        if (valorPais.length == 0) valorCampoPadre = '';
                        else valorCampoPadre = valorPais[0].value;
                    } else valorCampoPadre = '';
                    $.when($.ajax({
                        type: "POST",
                        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Opciones_Lista',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({ Campo_Escenario: JSON.stringify(elementoLista.closest('fieldset').data('campoEscenario')), Escenario: JSON.stringify(escenarios[idEscenarioSeleccionado]), ValorCampoPadre: valorCampoPadre, iTipoVisor: tipoVisor }),
                        async: false
                    })).done(function (msg) {
                        var lOpcionesLista = msg.d;
                        //Recuperamos las opciones seleccionadas
                        var lValores = elementoLista.closest('fieldset').data('selectedValues');
                        //Nos quedamos con los valores
                        if (lValores !== undefined) lValores = lValores.map(function (selValue) { return selValue.split("###")[0] });
                        //Recargamos las opciones
                        $(el).children('option').remove();
                        $.each(lOpcionesLista, function () {
                            //Creamos la opción
                            opt = $('<option />', {
                                value: escape(this.value),
                                text: this.text
                            });
                            //Seleccionamos la opción si es necesario
                            if (lValores !== undefined && $.inArray(opt.val(), lValores) != -1) {
                                opt.attr('selected', 'selected');
                            }
                            //Agregamos la opcion al combo
                            opt.appendTo(el);
                        });
                        elementoLista.closest('fieldset').data('data', $('#' + elementoLista.attr('id') + ' option'));
                        elementoLista.multiselect('refresh');
                    });
                };
            }
        });
        if ($(this).closest('fieldset').data('campoEscenario').TipoCampo !== 4) sel.multiselectfilter({ label: TextosPantalla[63], placeholder: TextosPantalla[64] });
        switch ($(this).closest('fieldset').data('campoEscenario').TipoCampoGS) {
            case TIPOSCAMPOGS.PAIS:
                var el = this;
                $.when($.ajax({
                    type: "POST",
                    url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Opciones_Lista',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ Campo_Escenario: JSON.stringify(elementoLista.closest('fieldset').data('campoEscenario')), Escenario: JSON.stringify(escenarios[idEscenarioSeleccionado]), ValorCampoPadre: '', iTipoVisor: tipoVisor }),
                    async: false
                })).done(function (msg) {
                    var lOpcionesLista = msg.d;
                    $(el).children('option').remove();
                    elementoLista.closest('fieldset').data('data', lOpcionesLista);
                    elementoLista.multiselect('refresh');
                    $.each(elementoLista.closest('fieldset').data('data'), function () {
                        opt = $('<option />', {
                            value: this.value,
                            text: this.text
                        });
                        if (elementoLista.closest('fieldset').data('selectedValues') !== undefined)
                            if ($.inArray($(opt).val() + '###' + $(opt).text(), elementoLista.closest('fieldset').data('selectedValues')) !== -1) opt.attr('selected', 'selected');
                        opt.appendTo(sel);
                    });
                    sel.multiselect('refresh');
                    if (elementoLista.closest('fieldset').data('selectedValues') == undefined) sel.multiselect('uncheckAll');
                });
                break;
            case TIPOSCAMPOGS.PROVINCIA:
                var el = this;
                if (elementoLista.closest('fieldset').data('campoEscenario').TipoCampoGS == 108) {
                    var valorPais = $('#' + elementoLista.attr('id').replace(elementoLista.attr('id').split('_')[elementoLista.attr('id').split('_').length - 1], elementoLista.closest('fieldset').data('campoEscenario').CampoPadre)).multiselect('getChecked');
                    if (valorPais.length == 0) valorCampoPadre = '';
                    else valorCampoPadre = valorPais[0].value;
                } else valorCampoPadre = '';
                $.when($.ajax({
                    type: "POST",
                    url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Opciones_Lista',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ Campo_Escenario: JSON.stringify(elementoLista.closest('fieldset').data('campoEscenario')), Escenario: JSON.stringify(escenarios[idEscenarioSeleccionado]), ValorCampoPadre: valorCampoPadre, iTipoVisor: tipoVisor }),
                    async: false
                })).done(function (msg) {
                    var lOpcionesLista = msg.d;
                    $(el).children('option').remove();
                    elementoLista.closest('fieldset').data('data', lOpcionesLista);
                    elementoLista.multiselect('refresh');
                    $.each(elementoLista.closest('fieldset').data('data'), function () {
                        opt = $('<option />', {
                            value: this.value,
                            text: this.text
                        });
                        if (elementoLista.closest('fieldset').data('selectedValues') !== undefined)
                            if ($.inArray($(opt).val() + '###' + $(opt).text(), elementoLista.closest('fieldset').data('selectedValues')) !== -1) opt.attr('selected', 'selected');
                        opt.appendTo(sel);
                    });
                    sel.multiselect('refresh');
                    if (elementoLista.closest('fieldset').data('selectedValues') == undefined) sel.multiselect('uncheckAll');
                });
                break;
            default:
                if (elementoLista.closest('fieldset').data('data') !== undefined) {
                    //Nos quedamos con la parte del valor para que sea compatible con los valores booleanos
                    if (elementoLista.closest('fieldset').data('selectedValues') !== undefined)
                        var valoresSeleccion = jQuery.map(elementoLista.closest('fieldset').data('selectedValues'), function (x) { return (x.split("###")[0]); });

                    $.each(elementoLista.closest('fieldset').data('data'), function () {
                        opt = $('<option />', {
                            value: this.value,
                            text: this.text
                        });
                        //Seleccionamos el option si coincide con el valor
                        if ($.inArray($(opt).val(), valoresSeleccion) !== -1) opt.attr('selected', 'selected');
                        opt.appendTo(sel);
                    });
                    sel.multiselect('refresh');
                } else {
                    if (elementoLista.closest('fieldset').data('selectedValues') !== undefined) {
                        $.each(elementoLista.closest('fieldset').data('selectedValues'), function () {
                            opt = $('<option />', {
                                value: this.split('###')[0],
                                text: this.split('###')[1]
                            });
                            opt.attr('selected', 'selected');
                            opt.appendTo(sel);
                        });
                        sel.multiselect('refresh');
                    };
                };
                break;
        };
    });

    //Inicializamos los campos numéricos con formato
    $('input[id^=txtCampoOpcionFiltro_Numerico_]').numeric({ decimal: '' + usuario.DecimalFmt + '', negative: false }).numericFormatted({
        decimalSeparator: UsuNumberDecimalSeparator,
        groupSeparator: UsuNumberGroupSeparator,
        decimalDigits: UsuNumberNumDecimals
    });

    //Los campos de Tipo Identificador no van formateados
    $('input[id^=txtCampoOpcionFiltro_Numerico_][id$=_' + CAMPOSGENERALES.IDENTIFICADOR + ']').numericFormatted({
        groupSeparator: "",
        decimalDigits: 0
    });

    //Establecemos los valores de los campos numéricos
    $.each($('[id^=txtCampoOpcionFiltro_Numerico_]'), function () {
        var elementoNumero = $(this);
        var selValues = elementoNumero.closest('fieldset').data('selectedValues');
        var campo = elementoNumero.closest('fieldset').data('campoEscenario');
        var filtro = escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado];
        var condicion = filtro.FormulaCondiciones[campo.PosicionEnFormula - 1];
        if (selValues !== undefined) {
            if (!filtro.FormulaAvanzada && condicion.Operador != OPERADORES.ENTRE) {
                //En la fórmula básica si no se rellenan los 2 valores Desde/Hasta, se asigna el único valor al textbox que corresponde
                if ((condicion.Operador == OPERADORES.MAYORIGUAL && elementoNumero.attr('id').indexOf('Desde') != -1) ||
                    (condicion.Operador == OPERADORES.MENORIGUAL && elementoNumero.attr('id').indexOf('Hasta') != -1))
                    elementoNumero.numericFormatted('val', parseFloat(selValues[0]));
            } else {
                if (selValues.length == 2)
                    elementoNumero.numericFormatted('val', parseFloat(selValues[(elementoNumero.attr('id').indexOf('Desde') !== -1 ? 0 : 1)]));
                else
                    //Si sólo hay un valor se asigna al campo desde y se deja el hasta sin asignar
                    if (elementoNumero.attr('id').indexOf('Desde') !== -1) elementoNumero.numericFormatted('val', parseFloat(selValues[0]));
            }
        }
    });

    //Inicializamos los campos de fecha
    $('[id^=txtCampoOpcionFiltro_Fecha_Desde_],[id^=txtCampoOpcionFiltro_Fecha_Hasta_]').inputmask(UsuMask.replace('MM', 'mm')).datepicker({
        showOn: 'both',
        buttonImage: ruta + 'images/colorcalendar.png',
        buttonImageOnly: true,
        buttonText: '',
        dateFormat: UsuMask.replace('MM', 'mm').replace('yyyy', 'yy'),
        showAnim: 'slideDown'
    });
    $.datepicker.setDefaults($.datepicker.regional[UsuLanguageTag]);
    //Establecemos la validación numérica para el textbox del modo "HACE" de la fecha
    $('img[name=ModoFecha]').siblings('div[index=1]').find('input:first').numeric({ decimal: false, negative: false });
    //Establecemos los valores de los campos de fecha
    $.each($('[id^=txtCampoOpcionFiltro_Fecha_]'), function () {
        var elementoFecha = $(this);
        var selValues = elementoFecha.closest('fieldset').data('selectedValues');
        var campo = elementoFecha.closest('fieldset').data('campoEscenario');
        var filtro = escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado];
        var condicion = filtro.FormulaCondiciones[campo.PosicionEnFormula - 1];
        if (selValues !== undefined) {
            if (!filtro.FormulaAvanzada && condicion.Operador != OPERADORES.ENTRE) {
                if ((condicion.Operador == OPERADORES.MAYORIGUAL && elementoFecha.attr('id').indexOf('Desde') != -1) ||
                    (condicion.Operador == OPERADORES.MENORIGUAL && elementoFecha.attr('id').indexOf('Hasta') != -1))
                    elementoFecha.datepicker('setDate', new Date(Date.parse(selValues[0])));
            } else
                elementoFecha.datepicker('setDate', new Date(Date.parse(selValues[(elementoFecha.attr('id').indexOf('Desde') !== -1 ? 0 : 1)])));
        }
    });
    $('[lblDesde]').text(TextosPantalla[23]);
    $('[lblHasta]').text(TextosPantalla[24]);
    $('[tipocombofecha=1]').siblings('span').text(TextosPantalla[87]);
};
function Obtener_FiltroCondiciones_Buscar_Basico() {
    var filtroInfo = escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado];
    filtroInfo.FormulaCondicionesAUX = $.parseJSON(JSON.stringify(filtroInfo.FormulaCondiciones));
    var formulaCondiciones = [];
    var formulaCondicion;
    var orden = 1;
    var campoFiltroGeneral; var idCampoFiltro; var oCampoFiltro;

    $.each($('#panelEscenarioFiltrosOpcionesFiltrado fieldset'), function () {
        formulaCondicion = {};

        //Establecemos las propiedades de la condición
        campoFiltroGeneral = ($(this).attr('id').split('_')[1] == 'CampoOpcionGeneral');
        idCampoFiltro = $(this).attr('id').split('_')[2];
        oCampoFiltro = $.grep(filtroInfo.Filtro_Campos, function (x) { return x.EsCampoGeneral == campoFiltroGeneral && x.Id == idCampoFiltro; })[0];
        EstablecerCondicion(formulaCondicion, $(this), oCampoFiltro);
        if (formulaCondicion.Valores !== undefined && formulaCondicion.Valores.length > 0) {
            //Añadimos un AND si no es la primera condicion
            if (orden !== 1) {
                var formulaCondicionAND = {};
                formulaCondicionAND.Orden = orden;
                formulaCondicionAND.IdCampo = 0;
                formulaCondicionAND.EsCampoGeneral = false;
                formulaCondicionAND.Denominacion = '';
                formulaCondicionAND.Denominacion_BD = '';
                formulaCondicionAND.Operador = OPERADORES_FORMULA.AND;
                formulaCondicionAND.Valores = [];
                formulaCondicionAND.Condicion = DENOMINACION_OPERADORES_FORMULA[OPERADORES_FORMULA.AND]
                formulaCondiciones.push(formulaCondicionAND);
                orden += 1;
            };

            //Definimos el orden
            formulaCondicion.Orden = orden;

            //Añadimos la condicion al filtro
            formulaCondiciones.push(formulaCondicion);
            orden += 1;
        };
    });

    filtroInfo.FormulaCondiciones = formulaCondiciones;
};
function Obtener_FiltroCondiciones_Buscar_Avanzada() {
    var filtroInfo = escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado];
    filtroInfo.FormulaCondicionesAUX = $.parseJSON(JSON.stringify(filtroInfo.FormulaCondiciones));
    var formulaCondicion;
    var condicionFormulaAvanzada, contenedor;
    var campoFiltroGeneral; var idCampoFiltro; var oCampoFiltro;
    //Actualizamos los valores de las condiciones de la fórmula
    $.each(filtroInfo.FormulaCondiciones, function () {
        condicionFormulaAvanzada = this;
        condicionFormulaAvanzada.Valores = [];
        if (this.IdCampo !== 0) {
            contenedor = $('#panelEscenarioFiltrosOpcionesFiltrado [id^=EscenarioFiltro_CampoOpcion][orden=' + this.Orden + ']');

            //Establecemos las propiedades de la condición
            campoFiltroGeneral = (contenedor.attr('id').split('_')[1] == 'CampoOpcionGeneral');
            idCampoFiltro = contenedor.attr('id').split('_')[2];
            oCampoFiltro = $.grep(filtroInfo.Filtro_Campos, function (x) { return x.EsCampoGeneral == campoFiltroGeneral && x.Id == idCampoFiltro; })[0];
            EstablecerCondicion(condicionFormulaAvanzada, contenedor, oCampoFiltro);
        };
    });

    var parentesisCreados = false;
    //Por cada condición que no estaba en la fórmula inicial, la agregamos a la fórmula
    var orden = filtroInfo.FormulaCondiciones.length + 1;
    $.each($('#panelEscenarioFiltrosOpcionesFiltrado [id^=EscenarioFiltro_CampoOpcion][nuevoorden=0]'), function () {
        formulaCondicion = {};

        //Establecemos las propiedades de la condición
        campoFiltroGeneral = ($(this).attr('id').split('_')[1] == 'CampoOpcionGeneral');
        idCampoFiltro = $(this).attr('id').split('_')[2];
        oCampoFiltro = $.grep(filtroInfo.Filtro_Campos, function (x) { return x.EsCampoGeneral == campoFiltroGeneral && x.Id == idCampoFiltro; })[0];
        EstablecerCondicion(formulaCondicion, $(this), oCampoFiltro)

        //Si hay valores, añadimos este campo a la fórmula            
        if (formulaCondicion.Valores !== undefined && formulaCondicion.Valores.length !== 0) {
            //Si no es la primera condicion, añadimos AND a la fórmula
            if (orden > 1) {
                if (filtroInfo.FormulaCondiciones.length > 1 && !parentesisCreados) {
                    var formulaCondicionPARENTESISCERRADO = {};
                    formulaCondicionPARENTESISCERRADO.Orden = orden;
                    formulaCondicionPARENTESISCERRADO.IdCampo = 0;
                    formulaCondicionPARENTESISCERRADO.EsCampoGeneral = false;
                    formulaCondicionPARENTESISCERRADO.Denominacion = '';
                    formulaCondicionPARENTESISCERRADO.Denominacion_BD = '';
                    formulaCondicionPARENTESISCERRADO.Operador = OPERADORES_FORMULA.PARENTESIS_CERRADO;
                    formulaCondicionPARENTESISCERRADO.Valores = [];
                    formulaCondicionPARENTESISCERRADO.Condicion = DENOMINACION_OPERADORES_FORMULA[OPERADORES_FORMULA.PARENTESIS_CERRADO]
                    filtroInfo.FormulaCondiciones.push(formulaCondicionPARENTESISCERRADO);
                    orden += 1;
                    var formulaCondicionPARENTESISABIERTO = {};
                    formulaCondicionPARENTESISABIERTO.Orden = 0;
                    formulaCondicionPARENTESISABIERTO.IdCampo = 0;
                    formulaCondicionPARENTESISABIERTO.EsCampoGeneral = false;
                    formulaCondicionPARENTESISABIERTO.Denominacion = '';
                    formulaCondicionPARENTESISABIERTO.Denominacion_BD = '';
                    formulaCondicionPARENTESISABIERTO.Operador = OPERADORES_FORMULA.PARENTESIS_ABIERTO;
                    formulaCondicionPARENTESISABIERTO.Valores = [];
                    formulaCondicionPARENTESISABIERTO.Condicion = DENOMINACION_OPERADORES_FORMULA[OPERADORES_FORMULA.PARENTESIS_ABIERTO]
                    filtroInfo.FormulaCondiciones.unshift(formulaCondicionPARENTESISABIERTO);
                    orden += 1;
                    $($(filtroInfo.FormulaCondiciones).get().reverse()).each(function (index) {
                        $('#panelEscenarioFiltrosOpcionesFiltrado [id^=EscenarioFiltro_CampoOpcion][orden=' + this.Orden + ']').attr('orden', this.Orden + 1);
                        this.Orden = this.Orden + 1;
                    });
                    parentesisCreados = true;
                }
                var formulaCondicionAND = {};
                formulaCondicionAND.Orden = orden;
                formulaCondicionAND.IdCampo = 0;
                formulaCondicionAND.EsCampoGeneral = false;
                formulaCondicionAND.Denominacion = '';
                formulaCondicionAND.Denominacion_BD = '';
                formulaCondicionAND.Operador = OPERADORES_FORMULA.AND;
                formulaCondicionAND.Valores = [];
                formulaCondicionAND.Condicion = DENOMINACION_OPERADORES_FORMULA[OPERADORES_FORMULA.AND]
                filtroInfo.FormulaCondiciones.push(formulaCondicionAND);
                orden += 1;
            };

            //Establecemos el orden de la condicion
            formulaCondicion.Orden = orden;

            //Establecemos el atributo orden del contenedor y quitamos el atributo nuevoorden
            $(this).attr('orden', orden).removeAttr('nuevoorden');

            //Añadimos la condición a la fórmula            
            filtroInfo.FormulaCondiciones.push(formulaCondicion);
            orden += 1;
        };
    });
};

//#endregion

//#region Vistas

//Click en el botón "Ordenar Vistas"
$('#panelEscenarioVistas [id$=imgEscenarioVistasOrder]').live('click', function (event) {
    ejecutandoPartialPostBack = true;
    $("#listaOrder").sortable({
        cancel: '.ui-state-disabled',
        containment: 'parent',
        axis: 'y',
        scroll: true,
        scrollSensitivity: 50,
        scrollSpeed: 50,
        cursor: 'move',
        placeholder: 'ui-state-highlight',
        forcePlaceholderSize: true,
        start: function (event, ui) {
            var start_pos = ui.item.index();
            ui.item.data('start_pos', start_pos);
        },
        beforeStop: function (event, ui) {
            var idEscenarioOrden, idVistaOrden, posicionAnterior, posicionActual;
            idVistaOrden = parseInt(ui.item.attr('id').replace('ordenEscenarioFiltroVista_', ''));
            posicionAnterior = (ui.item.data('start_pos') - 1);
            posicionActual = (ui.item.index() - 1);
            if (posicionActual !== posicionAnterior) {
                $.each(escenarios[idEscenarioSeleccionado].EscenarioVistas, function () {
                    if (posicionActual < posicionAnterior) {
                        if (this.Posicion > posicionActual - 1 && this.Posicion < posicionAnterior) this.Posicion = this.Posicion + 1;
                    } else {
                        if (this.Posicion > posicionAnterior && this.Posicion < posicionActual + 1) this.Posicion = this.Posicion - 1;
                    };
                });
                escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaOrden].Posicion = posicionActual;
                var opcionesUsu = JSON.stringify({ tipoOpcion: 3, oOpcionesUsu: JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioVistas) });
                OrdenarVistas(opcionesUsu);
            };
        }
    });
    $("#listaOrder").disableSelection();
    $('#lblCabeceraPanelOrder').text(TextosPantalla[95]);
    $('#btnCerrarPanelOrder').text(TextosPantalla[89]);
    var vistasOrdenar = $.map($.parseJSON(JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioVistas)), function (x) { return x; });
    vistasOrdenar.unshift({ Nombre: '' });
    $('#ordenEscenarioFiltroVista').tmpl(vistasOrdenar.sort(ordenPorPosicion)).appendTo($('#panelOrder ul'));
    $('#popupFondo').css('height', $(document).height());
    $('#popupFondo').show();
    CentrarPopUp($('#panelOrder'));
    $('#panelOrder').show();
    event.stopPropagation();
});
//Click en el checkbox de Vista Por Defecto
$('[id$=chkVistaDefectoFiltro]').live('click', function (event) {
    event.stopPropagation();
    if (confirm(TextosPantalla[88])) {
        var checkDefecto = $(this);
        escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].IdVistaDefecto = (checkDefecto.prop('checked') ? idVistaSeleccionado : 0);
        var opcionesUsu = JSON.stringify({ tipoOpcion: 2, oOpcionesUsu: JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioFiltros) });
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Establecer_Opciones_Usu',
            contentType: "application/json; charset=utf-8",
            data: opcionesUsu,
            dataType: "json",
            async: true
        })).done(function (msg) {
            $('#lblOptionVistaDefectoFiltro').hide();
        });
    } else return false;
});
//Click en una Vista
$('.escenarioVista').live('click', function () {
    if (parseInt($(this).attr('id').split('_')[1]) !== idVistaSeleccionado) {
        //Cancelamos el postback que se esté ejecutando (solicitudes)
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm.get_isInAsyncPostBack()) prm.abortPostBack();

        Seleccionar_Vista(parseInt($(this).attr('id').split('_')[1]));
        resetInstanciasCheck();
        Cargar_Solicitudes();
    };
});
function Crear_Vistas() {
    $('#panelEscenarioVistas .escenarioVista').remove();

    //Obtenemos cual es la vista por defecto
    var vistaDefecto = escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].IdVistaDefecto;

    if (typeof (vistaVolver) != 'undefined') {
        //Hemos vuelto de otra página, Seleccionamos la vista en la que estábamos
        idVistaSeleccionado = vistaVolver;
        vistaVolver = undefined;
    } else {
        //Seleccionamos la vista por defecto y si no existe, la primera        
        idVistaSeleccionado = vistaDefecto != 0 ? vistaDefecto : $.map(escenarios[idEscenarioSeleccionado].EscenarioVistas, function (x) { return x.Id; })[0];
    }

    $('#escenarioVista').tmpl($.map(escenarios[idEscenarioSeleccionado].EscenarioVistas, function (escenarioFiltro) { return escenarioFiltro }).sort(ordenPorPosicion)).prependTo($('#panelEscenarioVistas'));
    if (!$('#escenarioVista_' + idVistaSeleccionado).hasClass('escenarioSeleccionado')) $('#escenarioVista_' + idVistaSeleccionado).addClass('escenarioSeleccionado');
    $('[id$=imgEscenarioVistasOrder]').show();
}
function Seleccionar_Vista(idVista) {
    idVistaSeleccionado = idVista;
    if (typeof (chkProcesosPedidos) !== 'undefined' && idEscenarioSeleccionado == 0) {
        escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Campos_Vista.push(chkProcesosPedidos[0]);
        escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Campos_Vista.push(chkProcesosPedidos[1]);
        chkProcesosPedidos = undefined;
    };
    $('#panelEscenarioVistas .escenarioSeleccionado').removeClass('escenarioSeleccionado');
    $('#escenarioVista_' + idVistaSeleccionado).addClass('escenarioSeleccionado');
    resetInstanciasCheck();
}
function OrdenarVistas(opcionesUsu) {
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Establecer_Opciones_Usu',
        contentType: "application/json; charset=utf-8",
        data: opcionesUsu,
        dataType: "json",
        async: true
    })).done(function (msg) {
        var idVistaSeleccionadaVirtual = idVistaSeleccionado;
        Crear_Vistas();
        Seleccionar_Vista(idVistaSeleccionadaVirtual);
    });
}
//#endregion

//#region Paneles de Ordenación

//Click en el botón de cerrar de un panel de ordenación
$('#btnCerrarPanelOrder').live('click', function () {
    $("#listaOrder").sortable('destroy');
    $("#listaOrder").empty();
    $('#popupFondo').hide();
    $('#panelOrder').hide();
    CerrarPanelOrdenacion();
});

//Cierra el panel de Ordenacion de Escenarios, Filtros o Vistas
function CerrarPanelOrdenacion() {
    if (ordenacionEscenario.length == 0 && ordenacionFiltro.length == 0 && ordenacionVista.length == 0) {
        OcultarCargando();
        ejecutandoPartialPostBack = false;
        ActualizarContadoresFiltro();
    } else {
        MostrarCargando();
    };
};

//#endregion

//#region Popup Cargando...
Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginReq);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endReq);
var ejecutandoPartialPostBack = false;
function beginReq(sender, args) {
    var Emisor;
    var Comparacion;
    Emisor = args._postBackElement.id;
    Comparacion = /btnRecargarGridSolicitudes/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar_Cargando_Datos();
    Comparacion = /btnPager/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar_Cargando_Datos();
    Comparacion = /btnRecargarConfigVista/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar_Cargando_Configuracion_Vista();
    Comparacion = /btnBuscarSolicitud/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar_Cargando_Datos();
    ejecutandoPartialPostBack = true;
};
function endReq(sender, args) {
    OcultarCargando();
    Ocultar_Cargando_Configuracion_Vista();
    ejecutandoPartialPostBack = false;
};
function Mostrar_Cargando_Datos() {
    $('[id$=updSolicitudes]').hide();
    $('#divCargandoSolicitudes').show();
};
function Ocultar_Cargando_Datos() {
    $('#divCargandoSolicitudes').hide();
    $('[id$=updSolicitudes]').show();

    //Después de recargar el panel hay que volver a establecer el estado de los botones de las barra de vistas
    if (idEscenarioSeleccionado !== 0 && !buscandoIdentificador) {
        if (escenarios[idEscenarioSeleccionado].Editable) {
            //El botón de eliminar vista sólo lo mostramos si hay más de una
            if ($.map(escenarios[idEscenarioSeleccionado].EscenarioVistas, function (x) { return x; }).length > 1)
                $('#btnEscenarioVistaEliminar').show();
            $('#btnEscenarioVistaEditar').show();
            $('#btnEscenarioVistaNuevo').show();
            $('#panelEscenarioVistas [src*=sort]').show();

            //Establecemos los tooltips
            $('#btnEscenarioVistaNuevo').prop('title', TextosPantalla[57]);
            $('#btnEscenarioVistaEditar').prop('title', TextosPantalla[97]);
            $('#btnEscenarioVistaEliminar').prop('title', TextosPantalla[102]);
            $('#btnEscenarioVistaGuardar').prop('title', TextosPantalla[104]);
        }
        $('#btnEscenarioVistaGuardar').show();
    };
    if (($.map(escenarios[idEscenarioSeleccionado].EscenarioVistas, function (escenarioFiltro) { return escenarioFiltro }).length == 1) ||
        (escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Id == escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].IdVistaDefecto))
        $('#lblOptionVistaDefectoFiltro').hide();
    else $('#lblOptionVistaDefectoFiltro').show();

    if (idEscenarioSeleccionado == 0 && tipoVisor == TIPOVISOR.SOLICITUDES) $('#OptionInfoPedidosProcesos').show();
    else $('#OptionInfoPedidosProcesos').hide();
    buscandoIdentificador = false;
};
function Mostrar_Cargando_Configuracion_Vista() {
    $('[id$=updConfiguracionVista]').hide();
    $('#divCargandoConfiguracionVista').show();

};
function Ocultar_Cargando_Configuracion_Vista() {
    $('[id$=updConfiguracionVista]').show();
    $('#divCargandoConfiguracionVista').hide();
};
//#endregion

//#region Eventos Grid Configuración Vista
function whdgConfigVista_HeaderDropped(sender, e) {
    var movedColumnIndex = e.get_column().get_index();
    var movedColumnNewIndex = e.get_targetIndex();
    if (movedColumnIndex < movedColumnNewIndex) movedColumnNewIndex = movedColumnNewIndex - 1;
    var column = lCamposSeleccionadosVista.slice(movedColumnIndex);
    lCamposSeleccionadosVista.splice(movedColumnIndex, 1);
    lCamposSeleccionadosVista.splice(movedColumnNewIndex, 0, column[0]);
    $.each(lCamposSeleccionadosVista, function (index) {
        this.OrdenVisualizacion = index;
    });
    e.set_cancel(true);
    var btnRecargarConfigVista = $('[id$=btnRecargarConfigVista]').attr('id');
    sParameters = {};
    sParameters.CamposVista = JSON.stringify($.merge($.merge([], lCamposSeleccionadosVista), lCamposSeleccionadosVistaDesglose));
    sParameters.OrdenVista = (sOrdenVista == undefined ? '' : sOrdenVista);
    __doPostBack(btnRecargarConfigVista, JSON.stringify(sParameters));
};
function whdgConfigVista_DesgloseHeaderDropped(sender, e) {
    var movedColumnIndex = e.get_column().get_index();
    var movedColumnNewIndex = e.get_targetIndex();
    if (movedColumnIndex < movedColumnNewIndex) movedColumnNewIndex = movedColumnNewIndex - 1;
    var column = lCamposSeleccionadosVistaDesglose.slice(movedColumnIndex);
    lCamposSeleccionadosVistaDesglose.splice(movedColumnIndex, 1);
    lCamposSeleccionadosVistaDesglose.splice(movedColumnNewIndex, 0, column[0]);
    $.each(lCamposSeleccionadosVistaDesglose, function (index) {
        this.OrdenVisualizacion = index;
    });
    e.set_cancel(true);
    var btnRecargarConfigVista = $('[id$=btnRecargarConfigVista]').attr('id');
    sParameters = {};
    sParameters.CamposVista = JSON.stringify($.merge($.merge([], lCamposSeleccionadosVista), lCamposSeleccionadosVistaDesglose));
    sParameters.OrdenVista = (sOrdenVista == undefined ? '' : sOrdenVista);
    __doPostBack(btnRecargarConfigVista, JSON.stringify(sParameters));
};
//Se ha redimensionado una columna del grid de configuración de vista
function whdgConfigVista_ColumnResized(sender, e) {
    var resizedColumnIndex = e.get_column().get_index();
    var newWidth = parseInt(e.get_column().get_width().replace('px', ''));
    //Buscamos el N elemento de la lista que tenga EsCampoDesglose a false y le establecemos la nueva anchura
    jQuery.grep(lCamposSeleccionadosVista, function (x) { return x.EsCampoDesglose == false; })[resizedColumnIndex].AnchoVisualizacion = newWidth
};
//Se ha redimensionado una columna del grid de segundo nivel de configuración de vista
function whdgConfigVista_DesgloseColumnResized(sender, e) {
    var resizedColumnIndex = e.get_column().get_index();
    var newWidth = parseInt(e.get_column().get_width().replace('px', ''));
    //Buscamos el N elemento de la lista que tenga EsCampoDesglose a true y le establecemos la nueva anchura
    jQuery.grep(lCamposSeleccionadosVistaDesglose, function (x) { return x.EsCampoDesglose == true; })[resizedColumnIndex].AnchoVisualizacion = newWidth

    //Redimensionamos la columna de relleno según el nuevo tamaño
    var cols = sender.get_parent().get_columns();
    var colsDesglose = sender.get_columns();
    var anchoNivel1 = 0;
    var anchoNivel2 = 15;
    //Obtenemos la anchura de los grids de ambos niveles
    var columnsCount = cols.get_length();
    for (var i = 0; i < columnsCount; i++) {
        var col = cols.get_column(i);
        if (!col.get_hidden() && col.get_key() != "EmptySpace") anchoNivel1 += parseInt(col.get_width().replace('px', ''))
    }
    columnsCount = colsDesglose.get_length();
    for (var i = 0; i < columnsCount; i++) {
        var col = colsDesglose.get_column(i);
        if (!col.get_hidden()) anchoNivel2 += parseInt(col.get_width().replace('px', ''))
    }
    //Si el primer nivel es mas ancho, ocultamos la columna de relleno
    var anchoColumnaRelleno = anchoNivel2 - anchoNivel1;
    if (anchoColumnaRelleno < 0) anchoColumnaRelleno = 0;

    //Establecemos el tamaño de la columna de relleno
    cols.get_columnFromKey("EmptySpace").set_width(anchoColumnaRelleno + "px");
};
function whdgConfigVista_ColumnSorted(sender, e) {
    sOrdenVista = '';
    $.each(sender.get_behaviors().get_sorting().get_sortedColumns(), function () {
        sOrdenVista += (sOrdenVista == '' ? '' : ',') + this.get_key() + ' ' + (sender.get_behaviors().get_sorting().getSortDirection(this) == 1 ? 'ASC' : 'DESC');
    });
};
//#endregion

//#region Grid Solicitudes

//Click en el Botón "Buscar" del grid de Solicitudes
$('#btnBuscarSolicitudes').live('click', function () {
    //Cancelamos todas las consultas anteriores (escenario y filtros)
    $.each(consultas, function (i, x) {
        if (x && x.readyState != 4) {
            x.abort();
            delete x;
        }
    });
    //Cancelamos el postback que se esté ejecutando (solicitudes)
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm.get_isInAsyncPostBack()) prm.abortPostBack();

    if (escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].FormulaAvanzada) Obtener_FiltroCondiciones_Buscar_Avanzada();
    else Obtener_FiltroCondiciones_Buscar_Basico();
    escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].NumSolicitudes = undefined;
    $('#numSolicitudes_' + idFiltroSeleccionado).hide();
    $('img#' + idFiltroSeleccionado).show();
    resetInstanciasCheck();
    Cargar_Solicitudes();
});
//Click en el checkbox "Aprobar" de una fila del grid de Solicitudes
$('[id^=chkAprobar]').live('click', function () {
    if ($(this).prop('checked') && instanciasAprobar[$(this).attr('id').replace('chkAprobar_', '')] == undefined) {
        instanciasAprobar[$(this).attr('id').replace('chkAprobar_', '')] = $(this).attr('data');
        $('#chkRechazar_' + $(this).attr('id').replace('chkAprobar_', '')).prop('checked', false);
        $('#chkRechazarCheckAll').prop('checked', false);
        bRechazarAllChecked = false;
        delete instanciasRechazar[$(this).attr('id').replace('chkAprobar_', '')];
    }
    else {
        $('#chkAprobarCheckAll').prop('checked', false);
        bAprobarAllChecked = false;
        delete instanciasAprobar[parseInt($(this).attr('id').replace('chkAprobar_', ''))];
    }
});
//Click en el checkbox "Rechazar" de una fila del grid de Solicitudes
$('[id^=chkRechazar]').live('click', function () {
    if ($(this).prop('checked') && instanciasRechazar[$(this).attr('id').replace('chkRechazar_', '')] == undefined) {
        instanciasRechazar[$(this).attr('id').replace('chkRechazar_', '')] = $(this).attr('data');
        $('#chkAprobar_' + $(this).attr('id').replace('chkRechazar_', '')).prop('checked', false);
        $('#chkAprobarCheckAll').prop('checked', false);
        bAprobarAllChecked = false;
        delete instanciasAprobar[$(this).attr('id').replace('chkRechazar_', '')];
    }
    else {
        $('#chkRechazarCheckAll').prop('checked', false);
        bRechazarAllChecked = false;
        delete instanciasRechazar[parseInt($(this).attr('id').replace('chkRechazar_', ''))];
    }
});
//Click en el botón de cerrar de un popup de Texto Largo
$('#btnCerrarInfoTextoLargo').live('click', function () {
    $('#panelDetalleTextoLargo').hide();
    $('#popupFondo').hide();
});
//Click en el checkbox "Aprobar" de la cabecera de la columna de aprobar del grid de Solicitudes
$('#chkAprobarCheckAll').live('click', function () { CheckAll(true, this.checked); });
//Click en el checkbox "Rechazar" de la cabecera de la columna de rechazar del grid de Solicitudes
$('#chkRechazarCheckAll').live('click', function () { CheckAll(false, this.checked); });
//Realiza el check en todas las filas de la columna de aprobar o rechazar del grid de Solicitudes
function CheckAll(bAprobar, bCheck) {
    //Obtener los datos de todas las solicitudes que se pueden aprobar o rechazar
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/' + (bAprobar ? 'Obtener_Solicitudes_Aprobar' : 'Obtener_Solicitudes_Rechazar'),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false
    })).done(function (msg) {
        if (bAprobar) {
            bAprobarAllChecked = bCheck;
            bRechazarAllChecked = !bCheck;
        }
        else {
            bRechazarAllChecked = bCheck;
            bAprobarAllChecked = !bCheck;
        }

        //Rellenar los arrays instanciasAprobar y instanciasRechazar. Hacer el check de las filas de la página actual
        var oGrid = $find($('[id$=whdgSolicitudes]').attr('id'));
        var sColumnKey = (bAprobar ? "ACCION_APROBAR" : "ACCION_RECHAZAR");
        $.each(msg.d, function () {
            if (bAprobar) {
                if (bCheck) {
                    instanciasAprobar[this.ID] = this.Bloque + '|' + this.AccionAprobar;
                    delete instanciasRechazar[this.ID];
                }
                else instanciasAprobar = {};
            }
            else {
                if (bCheck) {
                    instanciasRechazar[this.ID] = this.Bloque + '|' + this.AccionRechazar;
                    delete instanciasAprobar[this.ID];
                }
                else instanciasRechazar = {};
            }

            var oCheck = $((bAprobar ? "#chkAprobar_" : "#chkRechazar_") + this.ID.toString());
            if (oCheck.length > 0) {
                if (!oCheck.prop('disabled')) oCheck.prop('checked', bCheck);

                if (bCheck) {
                    var oCheck2 = $((bAprobar ? "#chkRechazar_" : "#chkAprobar_") + this.ID.toString());
                    var oCheckAll2 = $((bAprobar ? "#chkRechazarCheckAll" : "#chkAprobarCheckAll"));
                    if (oCheck2.length > 0) {
                        if (!oCheck2.prop('disabled') && oCheck2.prop('checked')) {
                            //Deschequear el check de la otra columna y el check all
                            oCheck2.prop('checked', false);
                            oCheckAll2.prop('checked', false);
                        }
                    }
                }
            }
        });
    });;
}
//resetea las var. correspondientes a las instancias seleccionadas para aprobar o rechazar
function resetInstanciasCheck() {
    instanciasAprobar = {};
    instanciasRechazar = {};
    bAprobarAllChecked = false;
    bRechazarAllChecked = false;
}
$('#btnAprobar').live('click', function () {
    MostrarCargando();
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Procesar_Acciones',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ datosInstancias: JSON.stringify(instanciasAprobar) }),
        async: true
    })).done(function () {
        resetInstanciasCheck();
        OcultarCargando();
        Cargar_Solicitudes($('[id$=PagerPageList]').val());
    });;
    return false;
});
$('#btnRechazar').live('click', function () {
    MostrarCargando();
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Procesar_Acciones',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ datosInstancias: JSON.stringify(instanciasRechazar) }),
        async: true
    })).done(function () {
        resetInstanciasCheck();
        OcultarCargando();
        Cargar_Solicitudes($('[id$=PagerPageList]').val());
    });
    return false;
});

function Cargar_Solicitudes(numpagina) {
    var btnRecargarGridSolicitudes = $('[id$=btnRecargarGridSolicitudes]').attr('id');
    var sLoadParameter = {};
    var idSolicitudFormulario = 0;
    if (!escenarios[idEscenarioSeleccionado].AplicaTodas) {
        for (var key in escenarios[idEscenarioSeleccionado].SolicitudFormularioVinculados) break;
        idSolicitudFormulario = escenarios[idEscenarioSeleccionado].SolicitudFormularioVinculados[key].Key;
    };
    sLoadParameter.IdFormulario = JSON.stringify(idSolicitudFormulario);
    sLoadParameter.Filtro = JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado]);
    sLoadParameter.Vista = JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado]);
    sLoadParameter.SolicitudFormularioVinculados = JSON.stringify($.map(escenarios[idEscenarioSeleccionado].SolicitudFormularioVinculados, function (x, index) { return index }).join());
    sLoadParameter.Page = (numpagina == undefined ? 1 : numpagina);
    if (escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].FormulaCondicionesAUX !== undefined) {
        delete escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].FormulaCondicionesAUX;
    };
    if (escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].ErrorFormula) $('[id$=whdgSolicitudes]').hide();
    else {
        $('[id$=whdgSolicitudes]').show();
        __doPostBack(btnRecargarGridSolicitudes, JSON.stringify(sLoadParameter));
    }
};
function IrASolicitud(instancia, estado, tipoSolicitud, bloque, ObsyPart) {
    if (typeof (idCarpetaActualVolver) !== 'undefined') {
        idCarpetaSeleccionada = idCarpetaActualVolver;
        idEscenarioSeleccionado = idEscenarioActualVolver;
        idFiltroSeleccionado = idFiltroActualVolver;
        idVistaSeleccionado = idVistaActualVolver;
        numpag = numPaginaActualVolver;
    };
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/GuardarOpcionesFiltradoSesion',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ filtroCondicionesSession: JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].FormulaCondiciones), chkProcesosPedidos: $('[id$=chkInfoPedidosProcesos]').prop('checked') }),
        dataType: "json",
        async: true
    })).done(function () {
        escenarioFiltroContador = [];
        MostrarCargando();
        var volver = '*volver=' + rutaFS + 'PM/Tareas/VisorSolicitudes.aspx?carpeta=' + idCarpetaSeleccionada + '**escenario=' + idEscenarioSeleccionado + '**filtro=' + idFiltroSeleccionado + '**vista=' + idVistaSeleccionado + '**numpag=' + numpag;
        switch (Number(tipoSolicitud)) {
            case TIPOSOLICITUD.ENCUESTA:
                volver += '**TipoVisor=' + TIPOVISOR.ENCUESTAS;
                break;
            case TIPOSOLICITUD.FACTURA:
                volver += '**TipoVisor=' + TIPOVISOR.FACTURAS;
                break;
            case TIPOSOLICITUD.SOLICITUDQA:
                volver += '**TipoVisor=' + TIPOVISOR.SOLICITUDESQA;
                break;
        }
        if (ComprobarEnProceso(instancia)) {
            if (estado == 1000) window.open(rutaPM + 'frames.aspx?pagina=' + escape('workflow/comprobaraprob.aspx?Instancia=' + instancia + '&Bloque=' + bloque) + volver, '_self')
            else {
                if (estado == 3) window.open(rutaPM + 'frames.aspx?pagina=' + escape('seguimiento/NWDetalleSolicitud.aspx?Instancia=' + instancia + '&ComboParticipante=3*ObsYPart=' + ObsyPart) + volver, '_self');
                else window.open(rutaPM + 'frames.aspx?pagina=seguimiento/NWDetalleSolicitud.aspx?Instancia=' + instancia + "*ObsYPart=" + ObsyPart + volver, '_self');
            };
        };
    });
};
function IrADetalleConsulta(instancia, aprobar, rechazar, bloque, estado, bObservador) {
    if (ComprobarEnProceso(instancia)) {
        var newWindow = window.open(rutaPM + 'workflow/detalleSolicConsulta.aspx?Instancia=' + instancia + '&Aprobar=' + aprobar + '&Rechazar=' + rechazar + '&Bloque=' + bloque + '&Estado=' + escape(estado) + '&EsObservador=' + bObservador, '_blank', 'width=950,height=600,status=yes,resizable=yes,scrollbars=yes,top=30,left=30');

    };
};
function IrAAdjunto(params) {
    var newWindow = window.open(rutaPM + '_common/attach.aspx?tipo=1&' + params, '_blank', 'width=600,height=275,status=no,resizable=yes,top=200,left=200');

};
function IrAAdjuntos(params) {
    var newWindow = window.open(rutaPM + 'workflow/atachedfiles.aspx?tipo=1&readOnly=1&Valor2=&' + params, '_blank', 'width=750,height=330,status=yes,resizable=no,top=200,left=200');

};
function MostrarInfoTextoLargo(title, img, instancia, columna) {
    $(img).attr('cargado', '1');
    MostrarCargando();
    $('#lblCampoInfo').text(title);
    $('#btnCerrarInfoTextoLargo').text(TextosPantalla[89]);
    $('#popupFondo').css('height', $(document).height());
    var params = { Instancia: instancia, Columna: columna };
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/ObtenerTextoLargo',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: true
    })).done(function (msg) {
        $('#lblCampoValor').text(msg.d);
        $('#popupFondo').show();
        CentrarPopUp($('#panelDetalleTextoLargo'));
        $('#panelDetalleTextoLargo').show();
        OcultarCargando();
    });
};
function MostrarProcesosAsociados(title, texto) {
    $('#lblCampoInfo').text(title);
    $('#btnCerrarInfoTextoLargo').text(TextosPantalla[89]);
    $('#popupFondo').css('height', $(document).height());
    $('#lblCampoValor').html(texto);
    $('#popupFondo').show();
    CentrarPopUp($('#panelDetalleTextoLargo'));
    $('#panelDetalleTextoLargo').show();
};
function ComprobarEnProceso(instancia) {
    var enProceso = false;
    $.when($.ajax({
        type: 'POST',
        url: rutaFS + '_Common/App_services/Consultas.asmx/ComprobarEnProceso',
        data: JSON.stringify({ contextKey: instancia }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: false
    })).done(function (msg) {
        if (msg.d == "1") {
            OcultarCargando();
            var newWindow = window.open(rutaPM + 'seguimiento/NWcomentariossolic.aspx?Instancia=' + instancia + '&EnProceso=1', '_blank', 'width=600,height=300,status=yes,resizable=no,top=200,left=200');

            enProceso = false;
        } else {
            enProceso = true;
        };
    });
    return enProceso;
};
function VerFlujo(instancia) {
    if (ComprobarEnProceso(instancia)) {
        var newWindow = window.open(rutaPM + 'seguimiento/NWcomentariossolic.aspx?Instancia=' + instancia, '_blank', 'width=1000,height=560,status=yes,resizable=no,top=200,left=200');

    };
};
function ExportarExcel() {
    var sLoadParameter = {};
    var idSolicitudFormulario = 0;
    if (!escenarios[idEscenarioSeleccionado].AplicaTodas) {
        for (var key in escenarios[idEscenarioSeleccionado].SolicitudFormularioVinculados) break;
        idSolicitudFormulario = escenarios[idEscenarioSeleccionado].SolicitudFormularioVinculados[key].Key;
    };
    sLoadParameter.IdFormulario = JSON.stringify(idSolicitudFormulario);
    sLoadParameter.Filtro = JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado]);
    sLoadParameter.Vista = JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado]);
    sLoadParameter.SolicitudFormularioVinculados = JSON.stringify($.map(escenarios[idEscenarioSeleccionado].SolicitudFormularioVinculados, function (x, index) { return index }).join());
    sLoadParameter.Page = -1;
    __doPostBack('Excel', JSON.stringify(sLoadParameter));
};
function whdgSolicitudes_CellClick(sender, e) {
    if (e.get_type() == "cell" && e.get_item().get_row().get_index() !== -1 && !(isNaN(e.get_item().get_row().get_index()))) {
        switch (e.get_item().get_column().get_key()) {
            case "EmptySpace":
                e.set_cancel(true);
                return false;
                break;
            case 'ACCION_APROBAR':
            case 'ACCION_RECHAZAR':
            case 'RESUMEN':
            case 'PROCESOS':
                break;
            case 'PETICIONARIO':
                if ($('span', e.get_item().get_element()).length == 1) {
                    if ($('span[proveedor]', e.get_item().get_element()).length == 1)
                        FSNMostrarPanel(ProveedorAnimationClientID, e, ProveedorDynamicPopulateClientID, $('span', e.get_item().get_element()).attr('peticionario'));
                    else
                        FSNMostrarPanel(PersonaAnimationClientID, e, PersonaDynamicPopulateClientID, $('span', e.get_item().get_element()).attr('peticionario'));
                }
                break;
            case 'USUARIO':
                if ($('span', e.get_item().get_element()).length == 1) {
                    if ($('span[proveedor]', e.get_item().get_element()).length == 1) FSNMostrarPanel(ProveedorAnimationClientID, e, ProveedorDynamicPopulateClientID, $('span', e.get_item().get_element()).attr('proveedor'));
                    else FSNMostrarPanel(UsuarioAnimationClientID, e, UsuarioDynamicPopulateClientID, $('span', e.get_item().get_element()).attr('usuario'));
                };
                break;
            case 'SEG':
                if ($(e.get_item().get_row().get_element()).hasClass('datoRojo')) $(e.get_item().get_row().get_element()).removeClass('datoRojo');
                else $(e.get_item().get_row().get_element()).addClass('datoRojo');
                break;
            case 'ESTADO':
            case 'ETAPA':
            case 'ETAPA_ACTUAL':
                if ($('span[instanciaId]', e.get_item().get_element()).length == 1) VerFlujo($('span', e.get_item().get_element()).attr('instanciaId'));
                break;
            default:
                if ($('img', e.get_item().get_element()).length == 1) {
                    switch ($('img', e.get_item().get_element()).attr('name')) {
                        case 'comentario':
                            if ($('img', e.get_item().get_element()).attr('cargado')) $('img', e.get_item().get_element()).removeAttr('cargado');
                            else GestionarRedireccion(e.get_item());
                            break;
                        case 'editor':
                            show_editor($('span', e.get_item().get_element()).attr('titulo'), $('span', e.get_item().get_element()).attr('id'), $('span', e.get_item().get_element()).attr('campo_origen'))
                            break;
                    };
                    e.set_cancel(true);
                    return false;
                };
                if ($('span[rutaAdjunto]', e.get_item().get_element()).length == 1) {
                    IrAAdjunto($('span', e.get_item().get_element()).attr('rutaAdjunto'));
                } else {
                    if ($('span[rutaAdjuntos]', e.get_item().get_element()).length == 1) {
                        IrAAdjuntos($('span', e.get_item().get_element()).attr('rutaAdjuntos'));
                    } else {
                        GestionarRedireccion(e.get_item());
                    };
                };
                break;
        };
    };
};
function GestionarRedireccion(item) {
    //Comprobamos si se ha hecho click en una fila de primer nivel
    var element = $('span[instanciaId][instanciaEstado][instanciaTipoSolicitud][instanciaBloque]', item.get_element());
    //Comprobamos si se ha hecho click en una fila de segundo nivel
    if (element.length == 0) element = $('span[instanciaId][instanciaEstado][instanciaTipoSolicitud][instanciaBloque]', item.get_grid().get_parentRow().get_element());
    //Si se ha encontrado el elemento, vamos a la pantalla de detalle
    if (element.length > 0)
        IrASolicitud(element.attr('instanciaId'), element.attr('instanciaEstado'), element.attr('instanciaTipoSolicitud'), element.attr('instanciaBloque'), element.attr('obsypart'));
};
function whdgSolicitudes_Sorting(sender, e) {
    switch (e.get_column().get_key()) {
        case 'ACCION_APROBAR':
        case 'ACCION_RECHAZAR':
        case 'ERROR':
        case 'ERROR_VALIDACION':
        case 'ERROR_INTEGRACION':
        case 'RESUMEN':
            e.set_cancel(true);
            break;
        default:
            var grid = $find($('[id$=whdgSolicitudes]').attr('id'));
            grid.get_ajaxIndicator().show(grid);
            sOrdenVista = '';
            if (e.get_clear()) {
                sOrdenVista = e.get_column().get_key() + " " + (e.get_sortDirection() == 1 ? "ASC" : "DESC");
            } else {
                sOrdenVista = escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Orden + "," + e.get_column().get_key() + " " + (e.get_sortDirection() == 1 ? "ASC" : "DESC");
            };
            escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Orden = sOrdenVista;
            if (parseInt($('[id$=lblCount]').text()) !== 1) {
                e.set_cancel(true);
                var dropdownlist = $('[id$=PagerPageList]')[0];
                var sLoadParameter = {};
                var idSolicitudFormulario = 0;
                if (!escenarios[idEscenarioSeleccionado].AplicaTodas) {
                    for (var key in escenarios[idEscenarioSeleccionado].SolicitudFormularioVinculados) break;
                    idSolicitudFormulario = escenarios[idEscenarioSeleccionado].SolicitudFormularioVinculados[key].Key;
                };
                sLoadParameter.IdFormulario = JSON.stringify(idSolicitudFormulario);
                sLoadParameter.Filtro = JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado]);
                sLoadParameter.Vista = JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado]);
                sLoadParameter.SolicitudFormularioVinculados = JSON.stringify($.map(escenarios[idEscenarioSeleccionado].SolicitudFormularioVinculados, function (x, index) { return index }).join());
                sLoadParameter.Page = (dropdownlist.value == '' ? 1 : parseInt(dropdownlist.value));
                $.ajax({
                    type: 'POST',
                    url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Solicitudes_Ordenacion',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    async: true,
                    data: JSON.stringify({ data: JSON.stringify(sLoadParameter), OblCodPedDir: OblCodPedDir, iTipoVisor: tipoVisor }),
                    success: function () {
                        sender.get_behaviors().get_sorting().sortColumn(e.get_column(), e.get_sortDirection(), e.get_clear());
                    }
                });
            };
            //Guardar Ordenacion siempre
            var sLoadParameter2 = {};
            sLoadParameter2.Vista = JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado]);
            $.ajax({
                type: 'POST',
                url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Establecer_Solicitudes_Ordenacion',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                async: true,
                data: JSON.stringify({ data: JSON.stringify(sLoadParameter2) })
            });
            break;
    };
};
function whdgSolicitudes_Sorted(sender, e) {
    Ocultar_Cargando_Datos();
    var grid = $find($('[id$=whdgSolicitudes]').attr('id'));
    grid.get_ajaxIndicator().hide();
};
function whdgSolicitudes_Initialize(sender, e) {
    if (obtenerSolicitudesPendientes) {
        Obtener_Numero_Pendientes();
        obtenerSolicitudesPendientes = false;
    };
    if (escenarios[idEscenarioSeleccionado].Cargado) {
        Ocultar_Cargando_Datos();
        Ocultar_Cargando_Configuracion_Vista();
        var params, idFiltro;

        //Actualizamos el contador que coincide con la consulta activa
        escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].NumSolicitudes = parseInt($('[id*=iNumeroSolicitudesFiltroSeleccionado]').val());
        $('#numSolicitudes_' + idFiltroSeleccionado).text('(' + parseInt($('[id*=iNumeroSolicitudesFiltroSeleccionado]').val()) + ')');
        $('#numSolicitudes_' + idFiltroSeleccionado).show();

        if (($.map(escenarios[idEscenarioSeleccionado].EscenarioVistas, function (escenarioFiltro) { return escenarioFiltro }).length == 1) ||
            (escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Id == escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado].IdVistaDefecto))
            $('#lblOptionVistaDefectoFiltro').hide();
        else $('#lblOptionVistaDefectoFiltro').show();
    };
    if (idEscenarioSeleccionado == 0 && tipoVisor == TIPOVISOR.SOLICITUDES) $('#OptionInfoPedidosProcesos').show();
    else $('#OptionInfoPedidosProcesos').hide();
    /*Codigo para guardar datos del FSAL cuando se cargue el Grid.
      Si es primera llamada a la pagina y no se han enviado los tiempos para update, el booleano estara a 'False'.
      Entonces se hara la update y se pondra a 'True' para hacer la insercion de un nuevo registro. */
    if ($('#bActivadoFSAL').val() == '1') {
        if ($('#bEnviadoFSAL8Inicial').val() == '0') {
            $('#bEnviadoFSAL8Inicial').val('1');
        }
    }

    $("#chkAprobarCheckAll").prop('checked', bAprobarAllChecked);
    $("#chkRechazarCheckAll").prop('checked', bRechazarAllChecked);
    /*Fin codigo FSAL*/
};
//Se ha redimensionado una columna del grid de solicitudes
function whdgSolicitudes_ColumnResized(sender, e) {
    //Restamos 6 que son las columnas auxiliares que utilizamos para el funcionamiento del grid y que las añadimos al inicio del grid
    var resizedColumnIndex = e.get_column().get_index() - 6;
    var newWidth = parseInt(e.get_column().get_width().replace('px', ''));
    //Buscamos el N elemento de la lista que tenga EsCampoDesglose a false y le establecemos la nueva anchura
    jQuery.grep(escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Campos_Vista, function (x) { return x.EsCampoDesglose == false; })[resizedColumnIndex].AnchoVisualizacion = newWidth
};
//Se ha redimensionado una columna del grid de segundo nivel de solicitudes
function whdgSolicitudes_DesgloseColumnResized(sender, e) {
    var resizedColumnIndex = e.get_column().get_index();
    var newWidth = parseInt(e.get_column().get_width().replace('px', ''));
    //Buscamos el N elemento de la lista que tenga EsCampoDesglose a true y le establecemos la nueva anchura
    jQuery.grep(escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Campos_Vista, function (x) { return x.EsCampoDesglose == true; })[resizedColumnIndex].AnchoVisualizacion = newWidth

    //Redimensionamos la columna de relleno según el nuevo tamaño
    var cols = sender.get_parent().get_columns();
    var colsDesglose = sender.get_columns();
    var anchoNivel1 = 0;
    var anchoNivel2 = 15;
    //Obtenemos la anchura de los grids de ambos niveles
    var columnsCount = cols.get_length();
    for (var i = 0; i < columnsCount; i++) {
        var col = cols.get_column(i);
        if (!col.get_hidden() && col.get_key() != "EmptySpace") anchoNivel1 += parseInt(col.get_width().replace('px', ''))
    }
    columnsCount = colsDesglose.get_length();
    for (var i = 0; i < columnsCount; i++) {
        var col = colsDesglose.get_column(i);
        if (!col.get_hidden()) anchoNivel2 += parseInt(col.get_width().replace('px', ''))
    }
    //Si el primer nivel es mas ancho, ocultamos la columna de relleno
    var anchoColumnaRelleno = anchoNivel2 - anchoNivel1;
    if (anchoColumnaRelleno < 0) anchoColumnaRelleno = 0;

    //Establecemos el tamaño de la columna de relleno
    cols.get_columnFromKey("EmptySpace").set_width(anchoColumnaRelleno + "px");
}
function whdgSolicitudes_HeaderDropped(sender, e) {
    MostrarCargando();
    var movedColumnIndex = e.get_column().get_index() - 6;
    var movedColumnNewIndex = e.get_targetIndex() - 6;
    if (movedColumnIndex < movedColumnNewIndex) movedColumnNewIndex = movedColumnNewIndex - 1;
    lCamposSeleccionadosVista = $.map(escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Campos_Vista, function (campoVista) { if (!campoVista.EsCampoDesglose) return campoVista; });
    lCamposSeleccionadosVistaDesglose = $.map(escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Campos_Vista, function (campoVista) { if (campoVista.EsCampoDesglose) return campoVista; });
    var column = jQuery.grep(lCamposSeleccionadosVista, function (x) { return x.OrdenVisualizacion == movedColumnIndex; })[0];
    lCamposSeleccionadosVista.splice(movedColumnIndex, 1);
    lCamposSeleccionadosVista.splice(movedColumnNewIndex, 0, column);
    $.each(lCamposSeleccionadosVista, function (index) {
        this.OrdenVisualizacion = index;
    });
    escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Campos_Vista = $.merge($.merge([], lCamposSeleccionadosVista), lCamposSeleccionadosVistaDesglose);
    e.set_cancel(true);
    var btnColumnMove = $('[id$=btnColumnMove]').attr('id');
    var sLoadParameter = {};
    sLoadParameter.Vista = JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado]);
    __doPostBack(btnColumnMove, JSON.stringify(sLoadParameter));
};
function whdgSolicitudes_DesgloseHeaderDropped(sender, e) {
    var movedColumnIndex = e.get_column().get_index();
    var movedColumnNewIndex = e.get_targetIndex();
    if (movedColumnIndex < movedColumnNewIndex) movedColumnNewIndex = movedColumnNewIndex - 1;
    lCamposSeleccionadosVista = $.map(escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Campos_Vista, function (campoVista) { if (!campoVista.EsCampoDesglose) return campoVista; });
    lCamposSeleccionadosVistaDesglose = $.map(escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Campos_Vista, function (campoVista) { if (campoVista.EsCampoDesglose) return campoVista; });
    var column = jQuery.grep(lCamposSeleccionadosVistaDesglose, function (x) { return x.OrdenVisualizacion == movedColumnIndex; })[0];
    lCamposSeleccionadosVistaDesglose.splice(movedColumnIndex, 1);
    lCamposSeleccionadosVistaDesglose.splice(movedColumnNewIndex, 0, column);
    $.each(lCamposSeleccionadosVistaDesglose, function (index) {
        this.OrdenVisualizacion = index;
    });
    escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Campos_Vista = $.merge($.merge([], lCamposSeleccionadosVista), lCamposSeleccionadosVistaDesglose);
};
//#endregion

//#region Paginación
$('[id$=ImgBtnFirst]').live('click', function () { FirstPage(); });
$('[id$=ImgBtnPrev]').live('click', function () { PrevPage(); });
$('[id$=ImgBtnNext]').live('click', function () { NextPage(); });
$('[id$=ImgBtnLast]').live('click', function () { LastPage(); });
function IndexChanged() {
    var dropdownlist = $('[id$=PagerPageList]')[0];
    Pager(dropdownlist.selectedIndex);
}
function FirstPage() {
    Pager(0);
}
function PrevPage() {
    var dropdownlist = $('[id$=PagerPageList]')[0];
    if (dropdownlist.selectedIndex - 1 >= 0) {
        Pager(dropdownlist.selectedIndex - 1);
    };
};
function NextPage() {
    var dropdownlist = $('[id$=PagerPageList]')[0];
    if (dropdownlist.selectedIndex + 1 <= dropdownlist.length - 1) {
        Pager(dropdownlist.selectedIndex + 1);
    };
};
function LastPage() {
    var dropdownlist = $('[id$=PagerPageList]')[0];
    Pager(dropdownlist.length - 1);
};
function Pager(page) {
    var dropdownlist = $('[id$=PagerPageList]')[0];
    var btnPager = $('[id$=btnPager]').attr('id');
    dropdownlist.options[page].selected = true;
    var sLoadParameter = {};
    var idSolicitudFormulario = 0;
    if (!escenarios[idEscenarioSeleccionado].AplicaTodas) {
        for (var key in escenarios[idEscenarioSeleccionado].SolicitudFormularioVinculados) break;
        idSolicitudFormulario = escenarios[idEscenarioSeleccionado].SolicitudFormularioVinculados[key].Key;
    };
    numpag = ($('[id$=PagerPageList]').val() == '' ? 1 : $('[id$=PagerPageList]').val());
    sLoadParameter.IdFormulario = JSON.stringify(idSolicitudFormulario);
    sLoadParameter.Filtro = JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado]);
    sLoadParameter.Vista = JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado]);
    sLoadParameter.SolicitudFormularioVinculados = JSON.stringify($.map(escenarios[idEscenarioSeleccionado].SolicitudFormularioVinculados, function (x, index) { return index }).join());
    sLoadParameter.Page = page + 1;
    sLoadParameter.InstanciasAprobar = JSON.stringify(instanciasAprobar);
    sLoadParameter.InstanciasRechazar = JSON.stringify(instanciasRechazar);

    __doPostBack(btnPager, JSON.stringify(sLoadParameter));
};
//#endregion

//#region Funciones comunes

function ordenPorPosicion(a, b) {
    return a.Posicion - b.Posicion;
};

//Inicializa el combo de Tipos de Solicitud
function InicializarComboTiposSolicitud() {
    var ddTipoSolicitud = $('#ddWizardEscenarioTipoSolicitud');
    ddTipoSolicitud.css('width', '400px');
    ddTipoSolicitud.multiselect({
        noneSelectedText: TextosPantalla[98],
        selectedText: TextosPantalla[67],
        selectedList: 5
    });
    //Mostramos el filtro de busqueda del combo
    ddTipoSolicitud.multiselectfilter({ label: TextosPantalla[63], placeholder: TextosPantalla[64] });
};

//Establece el texto correspondiente en lenguaje natural a cada una de las condiciones de la fórmula en su propiedad "Condicion"
function EstablecerTextosCondiciones(condiciones) {
    $.each(condiciones, function () {
        EstablecerTextoCondicion(this);
    });
}

//Establece el texto correspondiente en lenguaje natural en la propiedad "Condicion" de una condicion de fórmula
function EstablecerTextoCondicion(condicion) {
    if (condicion.IdCampo == 0) {
        //Es un operador
        condicion.Condicion = DENOMINACION_OPERADORES_FORMULA[condicion.Operador].toLowerCase();
        return;
    }

    //Si no hay valores, no establecemos ningún texto
    if (condicion.Valores === undefined || condicion.Valores.length == 0) return;

    switch (condicion.Operador) {
        case OPERADORES.ENTRE:
            //Si el desde y el hasta son iguales se establece un solo valor, por eso comprobamos en cada caso si existe el segundo valor
            if (condicion.TipoCampo == 3) { //Si es una fecha              
                condicion.Condicion = condicion.Denominacion + ' ' + DENOMINACION_OPERADORES[condicion.Operador].toLowerCase() + ' ' + new Date(condicion.Valores[0]).UsuShortPattern();
                if (condicion.Valores.length == 2) condicion.Condicion += (' ' + DENOMINACION_OPERADORES_FORMULA[1].toLowerCase() + ' ' + new Date(condicion.Valores[1]).UsuShortPattern());
            } else if (condicion.IdCampo == CAMPOSGENERALES.IDENTIFICADOR) {
                condicion.Condicion = condicion.Denominacion + ' ' + DENOMINACION_OPERADORES[condicion.Operador].toLowerCase() + ' ' + condicion.Valores[0];
                if (condicion.Valores.length == 2) condicion.Condicion += (' ' + DENOMINACION_OPERADORES_FORMULA[1].toLowerCase() + ' ' + condicion.Valores[1]);
            } else {
                condicion.Condicion = condicion.Denominacion + ' ' + DENOMINACION_OPERADORES[condicion.Operador].toLowerCase() + ' ' + formatNumber(condicion.Valores[0]);
                if (condicion.Valores.length == 2) condicion.Condicion += (' ' + DENOMINACION_OPERADORES_FORMULA[1].toLowerCase() + ' ' + formatNumber(condicion.Valores[1]));
            }
            break;
        case OPERADORES.HACE: //Fecha de tipo relativa
            condicion.Condicion = condicion.Denominacion + ' ' + DENOMINACION_OPERADORES[condicion.Operador].toLowerCase() + ' ' + condicion.Valores[1] + ' ' + DENOMINACION_FECHAS_RELATIVAS[condicion.Valores[0]].toLowerCase();
            break;
        case OPERADORES.PERIODO: //Fecha de tipo periodo                                   
            condicion.Condicion = condicion.Denominacion + ' ' + DENOMINACION_OPERADORES[condicion.Operador].toLowerCase() + ' ' + DENOMINACION_FECHAS_PERIODO[condicion.Valores[0]].toLowerCase();
            break;
        default:
            var stringValores = '';
            switch (condicion.TipoCampoGS) {
                case TIPOSCAMPOGS.PETICIONARIO:
                case TIPOSCAMPOGS.USUARIO:
                case TIPOSCAMPOGS.PROVEEDORADJ:
                case TIPOSCAMPOGS.PROVEEDOR:
                case TIPOSCAMPOGS.PROVECONTACTO: //Proveedor
                case TIPOSCAMPOGS.MATERIAL:
                case TIPOSCAMPOGS.CODARTICULO:
                case TIPOSCAMPOGS.PERSONA:
                case TIPOSCAMPOGS.DENARTICULO:
                case TIPOSCAMPOGS.NUEVOCODARTICULO: //Articulo
                case TIPOSCAMPOGS.UNIDADORGANIZATIVA:
                case TIPOSCAMPOGS.COMPRADOR:
                    stringValores = condicion.Valores[1];
                    break;
                default:
                    if (condicion.Valores.length > 5 && condicion.EsLista) stringValores = '(' + TextosPantalla[67].replace('#', condicion.Valores.length) + ')';
                    else {
                        $.each(condicion.Valores, function () {
                            if (condicion.EsLista || condicion.TipoCampo == 4) { //si es una lista o un booleano
                                var valorActual = this.toString().split("###");
                                if (valorActual.length == 2) stringValores += (stringValores !== '' ? ',' : '') + valorActual[1];
                            } else if (condicion.TipoCampo == 3) { //Si es una fecha                                                    
                                stringValores += (stringValores !== '' ? ',' : '') + new Date(this).UsuShortPattern();
                            } else if (condicion.TipoCampo == 2 && condicion.IdCampo != CAMPOSGENERALES.IDENTIFICADOR) { //Si es un numérico y no es identificador                        
                                stringValores += (stringValores !== '' ? ',' : '') + formatNumber(this);
                            } else {
                                stringValores += (stringValores !== '' ? ',' : '') + this;
                            }
                        });
                    };
                    break;
            };
            condicion.Condicion = condicion.Denominacion + ' ' + DENOMINACION_OPERADORES[condicion.Operador].toLowerCase() + ' ' + stringValores;
            break;
    }
}

//Establece las propiedades de una condición de fórmula a partir de su campo correspondiente del formulario
function EstablecerCondicion(condicion, contenedor, campo) {
    var oCampoFiltro;
    if (condicion.IdCampo !== 0) {
        //Buscamos el campo correspondiente
        if (campo !== undefined)
            oCampoFiltro = campo;
        else
            oCampoFiltro = condicion;

        //Establecemos las propiedades
        condicion.Operador = parseInt(contenedor.find('select.CabeceraBotones:visible').val());
        condicion.EsLista = oCampoFiltro.EsLista;
        condicion.IdCampo = oCampoFiltro.Id == undefined ? condicion.IdCampo : oCampoFiltro.Id;
        condicion.Denominacion = oCampoFiltro.Denominacion;
        condicion.Denominacion_BD = oCampoFiltro.Denominacion_BD;
        condicion.TipoCampo = oCampoFiltro.TipoCampo;
        condicion.TipoCampoGS = oCampoFiltro.TipoCampoGS;
        condicion.EsCampoGeneral = oCampoFiltro.EsCampoGeneral;
        //Establecemos los valores
        if (oCampoFiltro.EsLista) {
            if (isNaN(condicion.Operador)) condicion.Operador = OPERADORES.ES;
            condicion.Valores = [];
            var lista = $('select[id^=selectCampo][id*=Filtro][id*=_Lista_]', contenedor);
            if (lista.val() !== null && lista.val()[0] != '') {
                condicion.Valores = lista.multiselect('getChecked').map(function () { return unescape(this.value) + "###" + $('#' + this.name.replace(/^multiselect_/, '') + ' option[value="' + this.value + '"]', contenedor).text() }).get();
            }
        } else {
            switch (oCampoFiltro.TipoCampoGS) {
                case TIPOSCAMPOGS.USUARIO:
                    var usuarioValue = $('input[id^=txtCampo][id*=Filtro][id*=_UsuarioValue]', contenedor);
                    var usuarioDen = $('input[id^=txtCampo][id*=Filtro][id*=_Usuario]:not([id*=_UsuarioValue])', contenedor);
                    if (usuarioValue.val() !== '') {
                        if (isNaN(condicion.Operador)) condicion.Operador = OPERADORES.ES;
                        condicion.Valores = [usuarioValue.val(), usuarioDen.val()];
                    };
                    break;
                case TIPOSCAMPOGS.PETICIONARIO:
                    var peticionarioValue = $('input[id^=txtCampo][id*=Filtro][id*=_PeticionarioValue]', contenedor);
                    var peticionario = $('input[id^=txtCampo][id*=Filtro][id*=_Peticionario]:not([id*=_PeticionarioValue])', contenedor);
                    if (peticionarioValue.val() !== '') {
                        if (isNaN(condicion.Operador)) condicion.Operador = OPERADORES.ES;
                        condicion.Valores = [peticionarioValue.val(), peticionario.val()];
                    };
                    break;
                case TIPOSCAMPOGS.PERSONA:
                    var personaValue = $('input[id^=txtCampo][id*=Filtro][id*=_PersonaValue]', contenedor);
                    var persona = $('input[id^=txtCampo][id*=Filtro][id*=_Persona]:not([id*=_PersonaValue])', contenedor);
                    if (personaValue.val() !== '') {
                        if (isNaN(condicion.Operador)) condicion.Operador = OPERADORES.ES;
                        condicion.Valores = [personaValue.val(), persona.val()];
                    };
                    break;
                case TIPOSCAMPOGS.PROVEEDORADJ:
                case TIPOSCAMPOGS.PROVEEDOR:
                case TIPOSCAMPOGS.PROVECONTACTO: //Proveedor
                    var proveedorValue = $('input[id^=txtCampo][id*=Filtro][id*=_ProveedorValue]', contenedor);
                    var proveedor = $('input[id^=txtCampo][id*=Filtro][id*=_Proveedor]:not([id*=_ProveedorValue])', contenedor);
                    if (proveedorValue.val() !== '') {
                        if (isNaN(condicion.Operador)) condicion.Operador = OPERADORES.ES;
                        condicion.Valores = [proveedorValue.val(), proveedor.val()];
                    }
                    break;
                case TIPOSCAMPOGS.MATERIAL: //Material
                    var materialValue = $('input[id^=txtCampo][id*=Filtro][id*=_MaterialValue]', contenedor);
                    var material = $('input[id^=txtCampo][id*=Filtro][id*=_Material]:not([id*=_MaterialValue])', contenedor);
                    if (materialValue.val() !== '') {
                        if (isNaN(condicion.Operador)) condicion.Operador = OPERADORES.IGUAL;
                        condicion.Valores = [materialValue.val(), material.val()];
                    }
                    break;
                case TIPOSCAMPOGS.CODARTICULO:
                case TIPOSCAMPOGS.DENARTICULO:
                case TIPOSCAMPOGS.NUEVOCODARTICULO: //Articulo
                    var articuloValue = $('input[id^=txtCampo][id*=Filtro][id*=_ArticuloValue]', contenedor);
                    var articulo = $('input[id^=txtCampo][id*=Filtro][id*=_Articulo]:not([id*=_ArticuloValue])', contenedor);
                    if (articulo.val() !== '') {
                        if (isNaN(condicion.Operador)) {
                            condicion.Operador = (articuloValue.val() == '' ? OPERADORES.CONTIENE : OPERADORES.IGUAL);
                            condicion.Valores = (condicion.Operador == OPERADORES.IGUAL ? [articuloValue.val(), articulo.val()] : [articulo.val(), articulo.val()]);
                        } else condicion.Valores = [articulo.val(), articulo.val()];
                    }
                    break;
                case TIPOSCAMPOGS.EMPRESA:
                    var empresaValue = $('input[id^=txtCampo][id*=Filtro][id*=_EmpresaValue]', contenedor);
                    var empresa = $('input[id^=txtCampo][id*=Filtro][id*=_Empresa]:not([id*=_EmpresaValue])', contenedor);
                    if (empresaValue.val() !== '') {
                        if (isNaN(condicion.Operador)) condicion.Operador = OPERADORES.ES;
                        condicion.Valores = [empresaValue.val(), empresa.val()];
                    }
                    break;
                default:
                    switch (oCampoFiltro.TipoCampo) {
                        case 1:
                        case 5:
                        case 6:
                        case 7: //String
                            if (isNaN(condicion.Operador)) condicion.Operador = OPERADORES.CONTIENE;
                            var texto = $('input[id^=txtCampo][id*=Filtro][id*=_String_]', contenedor);
                            if (texto.val() !== '') {
                                condicion.Valores = [texto.val()];
                            };
                            break;
                        case 2: //Numerico
                            var desde = $('input[id^=txtCampo][id*=Filtro][id*=_Numerico_Desde_]', contenedor).numericFormatted('val');
                            var inputHasta = $('input[id^=txtCampo][id*=Filtro][id*=_Numerico_Hasta_]', contenedor);
                            var hasta = inputHasta.css('visibility') == 'visible' ? inputHasta.numericFormatted('val') : undefined;

                            if (typeof (desde) !== 'undefined' && typeof (hasta) !== 'undefined') {
                                if (desde == hasta) {
                                    condicion.Operador = OPERADORES.IGUAL;
                                    condicion.Valores = [desde];
                                } else {
                                    if (isNaN(condicion.Operador)) condicion.Operador = OPERADORES.ENTRE;
                                    condicion.Valores = [desde, hasta];
                                }
                            } else {
                                if (typeof (desde) !== 'undefined') {
                                    if (isNaN(condicion.Operador) || condicion.Operador == OPERADORES.ENTRE) condicion.Operador = OPERADORES.MAYORIGUAL;
                                    condicion.Valores = [desde];
                                }
                                if (typeof (hasta) !== 'undefined') {
                                    if (isNaN(condicion.Operador) || condicion.Operador == OPERADORES.ENTRE) condicion.Operador = OPERADORES.MENORIGUAL;
                                    condicion.Valores = [hasta];
                                }
                            }
                            break;
                        case 3: //Fecha                                
                            var index = $('img[name=ModoFecha]', contenedor).attr('data-type');
                            switch (parseInt(index)) {
                                case 0:
                                    var desde = $('input[id^=txtCampo][id*=Filtro][id*=_Fecha_Desde_]', contenedor).datepicker('getDate');
                                    var inputHasta = $('input[id^=txtCampo][id*=Filtro][id*=_Fecha_Hasta_]', contenedor);
                                    var hasta = inputHasta.css('visibility') == 'visible' ? inputHasta.datepicker('getDate') : undefined;

                                    if (desde && hasta) {
                                        if (desde.getTime() == hasta.getTime()) {
                                            condicion.Operador = OPERADORES.IGUAL;
                                            condicion.Valores = [desde];
                                        } else {
                                            if (isNaN(condicion.Operador)) condicion.Operador = OPERADORES.ENTRE;
                                            condicion.Valores = [desde, hasta];
                                        }
                                    } else {
                                        if (desde) {
                                            if (isNaN(condicion.Operador) || condicion.Operador == OPERADORES.ENTRE) condicion.Operador = OPERADORES.MAYORIGUAL;
                                            condicion.Valores = [desde];
                                        }
                                        if (hasta) {
                                            if (isNaN(condicion.Operador) || condicion.Operador == OPERADORES.ENTRE) condicion.Operador = OPERADORES.MENORIGUAL;
                                            condicion.Valores = [hasta];
                                        }
                                    }
                                    break;
                                case 1:
                                    condicion.Operador = OPERADORES.HACE;
                                    if ($('div[index=' + index + '] input', contenedor).val() !== '')
                                        condicion.Valores = [$('div[index=' + index + '] select', contenedor).val(), $('div[index=' + index + '] input', contenedor).val()];
                                    break;
                                case 2:
                                    condicion.Operador = OPERADORES.PERIODO;
                                    condicion.Valores = [$('div[index=' + index + '] select', contenedor).val()];
                                    break;
                            }
                            break;
                        case 4: //Boolean                            
                            //TODO: Ver si son iguales que las listas
                            if (isNaN(condicion.Operador)) condicion.Operador = OPERADORES.ES
                            condicion.Valores = [];
                            var lista = $('select[id^=selectCampo][id*=Filtro][id*=_Lista_]', contenedor);
                            if (lista.val() !== null && lista.val()[0] != '') {
                                condicion.Valores = lista.multiselect('getChecked').map(function () { return [this.value + "###" + this.title] }).get();
                            }
                            break;
                    }
                    break;
            }
        }
    }
    //Establecemos el texto de la condición
    EstablecerTextoCondicion(condicion);
};

//''' <summary>
//''' Actualizar Contadores Filtro. 
//''' Si no hay escenario alguno, posible con permiso de "Ocultar el escenario por defecto", no hace nada. 
//''' </summary>
//''' <remarks>Llamada desde: CerrarPanelOrdenacion      whdgSolicitudes_Initialize; Tiempo mÃ¡ximo: 0</remarks>
function ActualizarContadoresFiltro() {
    if (typeof (escenarios[idEscenarioSeleccionado]) == 'undefined') {
        return false;
    }
    //Actualizamos los contadores que no coinciden con la consulta activa
    $.each(escenarios[idEscenarioSeleccionado].EscenarioFiltros, function (i, x) {
        if (x.Id != idFiltroSeleccionado) {
            params = JSON.stringify({ oEscenario: JSON.stringify(escenarios[idEscenarioSeleccionado]), IdFiltro: x.Id, iTipoVisor: tipoVisor });
            var xhr = $.ajax({
                type: "POST",
                url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Numero_Solicitudes_Filtro',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: params,
                async: true,
                error: function (xhr, text_status, error_thrown) {
                    if (text_status != "abort") {
                        //Si la llamada no se ha abortado, mostramos el error
                        console.error(error_thrown);
                    }
                }
            }).done(function (msg) {
                //Eliminamos la consulta ya devuelta
                if (consultas[x.Id] !== undefined) delete consultas[x.Id];

                var numeroSolicitudes = parseInt(msg.d[0]);
                idEscenario = parseInt(msg.d[1]);
                idFiltro = parseInt(msg.d[2]);
                //Escribimos el resultado en el filtro correspondiente
                if (escenarios[idEscenario] !== undefined && escenarios[idEscenario].EscenarioFiltros[idFiltro] !== undefined) {
                    escenarios[idEscenario].EscenarioFiltros[idFiltro].NumSolicitudes = numeroSolicitudes;
                    $('#numSolicitudes_' + idFiltro).text('(' + numeroSolicitudes + ')');
                    $('#numSolicitudes_' + idFiltro).show();
                };
            });
            //Añadimos la peticion ajax a las consultas, indexado con el Id del Filtro            
            consultas[x.Id] = xhr;
        }
    });
};

/* Descripcion: Muestra el contenido del campo tipo editor en formato de solo lectura
parametros entrada:
titulo: denominación del campo
id: id del campo tipo editor
Llamada: Cuando seleccionamos una solicitud
*/
function show_editor(titulo, id, campo_origen) {
    window.open(rutaFS + "_Common/Editor.aspx?titulo=" + escape(titulo) + "&ID=" + id + "&CAMPO_ORIGEN=" + campo_origen + "&readOnly=1", "Editor", "width=910,height=600,status=yes,resizable=no,top=20,left=100,scrollbars=yes")
};
//#endregion

//#region Monitorizacion
//Cuando se hace click en la bandera de seguimiento de solicitudes
$('[id^=monitorizacion]').live('click', function () {
    var id = $(this).attr('id').split('_')[1];
    $('#monitorizacionOn_' + id).toggle();
    $('#monitorizacionOff_' + id).toggle();
    $.ajax({
        type: "POST",
        url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Monitorizar_Solicitud',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ idInstancia: id, Seg: ($('#monitorizacionOn_' + id).is(':visible') ? 1 : 0) }),
        async: true
    });
});
//#endregion

//#region Info Pedidos / Procesos
//Click en el checkbox en el escenario por defecto para mostrar los pedidos y procesos asociados a la solicitud
$('[id$=chkInfoPedidosProcesos]').live('change', function () {
    MostrarCargando();
    if (campos_generales == undefined) {
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'PM/Tareas/VisorSolicitudes.aspx/Obtener_Campos_Generales',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true
        })).done(function (msg) {
            campos_generales = msg.d;
            MostrarInfoPedidosProcesos($('[id$=chkInfoPedidosProcesos]').prop('checked'));
        });
    } else {
        MostrarInfoPedidosProcesos($('[id$=chkInfoPedidosProcesos]').prop('checked'));
    };
});
function MostrarInfoPedidosProcesos(mostrar) {
    var btnRecargarGridSolicitudes = $('[id$=btnRecargarGridSolicitudes]').attr('id');
    lCamposSeleccionadosVista = escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado].Campos_Vista;
    if (mostrar) {
        campos_generales[17].OrdenVisualizacion = lCamposSeleccionadosVista.length;
        lCamposSeleccionadosVista.push(campos_generales[17]);
        campos_generales[18].OrdenVisualizacion = lCamposSeleccionadosVista.length;
        lCamposSeleccionadosVista.push(campos_generales[18]);
    } else {
        delete lCamposSeleccionadosVista.splice(lCamposSeleccionadosVista.indexOf(campos_generales[17]), 1);
        delete lCamposSeleccionadosVista.splice(lCamposSeleccionadosVista.indexOf(campos_generales[18]), 1);
        $.each(lCamposSeleccionadosVista, function (index) {
            this.OrdenVisualizacion = index;
        });
    };
    OcultarCargando();
    var sLoadParameter = {};
    var idSolicitudFormulario = 0;
    if (!escenarios[idEscenarioSeleccionado].AplicaTodas) {
        for (var key in escenarios[idEscenarioSeleccionado].SolicitudFormularioVinculados) break;
        idSolicitudFormulario = escenarios[idEscenarioSeleccionado].SolicitudFormularioVinculados[key].Key;
    };
    sLoadParameter.IdFormulario = JSON.stringify(idSolicitudFormulario);
    sLoadParameter.Filtro = JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioFiltros[idFiltroSeleccionado]);
    sLoadParameter.Vista = JSON.stringify(escenarios[idEscenarioSeleccionado].EscenarioVistas[idVistaSeleccionado]);
    sLoadParameter.SolicitudFormularioVinculados = JSON.stringify($.map(escenarios[idEscenarioSeleccionado].SolicitudFormularioVinculados, function (x, index) { return index }).join());
    sLoadParameter.Page = ($('[id$=PagerPageList]').val() == '' ? 1 : $('[id$=PagerPageList]').val());
    __doPostBack(btnRecargarGridSolicitudes, JSON.stringify(sLoadParameter));
};
//#endregion