﻿Public Partial Class SolicitudesFavoritas
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim tipoSolicitud As Integer = 0
        If Request("TipoSolicitud") <> "" Then tipoSolicitud = CInt(Request("TipoSolicitud"))
        If tipoSolicitud = TiposDeDatos.TipoDeSolicitud.Factura Then
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.FacturasFavoritas
        Else
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.SolicitudesFavoritas
        End If

        Dim Solicitudes As FSNServer.Solicitudes
        Solicitudes = FSNServer.Get_Object(GetType(FSNServer.Solicitudes))

        If Request("Solicitud") <> "" Then
            Me.Solicitud.Value = Request("Solicitud")
            Me.lblSolicitud.Text = Request("DenSolicitud")
        End If
        If (Not IsPostBack) And Request("nFavoritos") <> "" Then
            nFavoritos.Value = Request("nFavoritos")
        End If

        ''Eliminar la solicitud Favorita IdSolicitudFavorita_Eliminar
        If IdSolicitudFavorita_Eliminar.Value > 0 Then
            Solicitudes.EliminarSolicitudFavorita(IdSolicitudFavorita_Eliminar.Value)
            Me.IdSolicitudFavorita_Eliminar.Value = 0
            nFavoritos.Value = nFavoritos.Value - 1
        End If

        FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/favoritos_small.gif"
        FSNPageHeader.TituloCabecera = Textos(0) 'Solicitudes favoritas
        Me.lblTipoSolicitud.Text = Textos(1) 'Tipo de solicitud:
        Me.lblTexto1.Text = Textos(2) 'Dispone de distintas solicitudes preconfiguradas...
        Me.lblCabeceraTabla.Text = Textos(3) 'Lista de solicitudes favoritas
        Me.lblTexto2.Text = Textos(4) 'Para modificar los datos de las solicitudes favoritas...
        Me.textoConfirmacion.Value = Textos(5) 'Se eliminará la solicitud favorita. ¿Desea continuar?

        Solicitudes.LoadFavoritos(Solicitud.Value, FSNUser.Cod)
        If Solicitudes.Data.Tables.Count > 0 Then
            wgSolicitudesFavoritas.DataSource = Solicitudes.Data
            wgSolicitudesFavoritas.DataBind()
        Else
            wgSolicitudesFavoritas.Visible = False
            Me.tblSolicitudes.Visible = False
        End If
    End Sub

    Private Sub wgSolicitudesFavoritas_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles wgSolicitudesFavoritas.InitializeRow
        Dim Index As Integer = sender.Columns.FromKey("ELIMINAR").Index
        e.Row.Items.Item(Index).Text = "<center><img src='" & ConfigurationManager.AppSettings("ruta") & "images/eliminar.gif" & "' style='text-align:center;'/></center>"
        sender.Columns.FromKey("FEC_CREACION").DataFormatString = "{0:" & FSNUser.DateFormat.ShortDatePattern & "}"
    End Sub
End Class