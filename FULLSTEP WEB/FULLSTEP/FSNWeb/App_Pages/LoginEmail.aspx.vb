﻿Public Class LoginEmail
    Inherits FSNPage
    Dim FSWSServer As FSNServer.Root
    Private oAccion As AccionEmail
    Private Enum AccionEmail As Byte
        AprobarPedido = 0
        RechazarPedido = 1
        AprobarSolicitudPM = 2
    End Enum
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
        Dim modulo As TiposDeDatos.ModulosIdiomas = pag.ModuloIdioma
        pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.Login
        CargarTextos()

        Select Case Request.QueryString("target")
            Case "aprobEmail"
                oAccion = AccionEmail.AprobarPedido
            Case "rechazEmail"
                oAccion = AccionEmail.RechazarPedido
            Case Else
                If Request("TipoAcceso") = "4" Then
                    oAccion = AccionEmail.AprobarSolicitudPM
                End If
        End Select
    End Sub
    ''' <summary>
    ''' Carga los textos en función del idioma de la página.
    ''' </summary>
    ''' <remarks>
    ''' La página debe implementar la interface IFSNPage.
    ''' Llamadas desde: Page_Load
    ''' </remarks>
    Private Sub CargarTextos()
        Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
        lblError.Text = pag.Textos(0) & " " & pag.Textos(1) & " " & pag.Textos(2)
        Select Case oAccion
            Case AccionEmail.AprobarPedido, AccionEmail.AprobarSolicitudPM
                Me.lblInfoAcceso.Text = pag.Textos(9)
            Case AccionEmail.RechazarPedido
                Me.lblInfoAcceso.Text = pag.Textos(10)
        End Select
        btnValidar.Value = pag.Textos(7)
        btnCancelar.Value = pag.Textos(8)
        txtCuentaBloqueada.InnerText = pag.Textos(17)
        txtContactoAdmin.InnerText = pag.Textos(18)
    End Sub
    Private Sub btnValidar_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnValidar.ServerClick
        Dim UserName As String = Nothing
        Dim Password As String = txtPassword.Text
        FSWSServer = New FSNServer.Root
        Dim oUser As FSNServer.User

        Session.Clear()

        Select Case oAccion
            Case AccionEmail.AprobarPedido, AccionEmail.RechazarPedido
                UserName = Request("CodAprob")
            Case AccionEmail.AprobarSolicitudPM
                UserName = Request("Usuario")
        End Select

        If FSWSServer.TipoDeAutenticacion = TiposDeAutenticacion.LDAP Then
            Dim webServiceLDAP As New FSNWebServicePWD.CambioPWD
            If webServiceLDAP.LoginLDAP(UserName, Password) = 1 Then
                oUser = FSWSServer.Login(UserName)
            Else
                oUser = New FSNServer.User(TipoResultadoLogin.UsuIncorrecto)
            End If
        Else
            oUser = FSWSServer.Login(UserName, Password, False)
        End If

        If oUser.ResultadoLogin <> TipoResultadoLogin.LoginOk Then
            lblError.Visible = True
            If FSWSServer.TipoDeAutenticacion = TiposDeAutenticacion.Windows Then
                TablaAutenticacion.Visible = False
            End If
        Else
            oUser.LoadUserData(UserName)
        End If

        Gestion_Login(oUser, UserName, Password)
    End Sub
    ''' <summary>
    ''' Gestiona el logeo del usuario
    ''' </summary>
    ''' <param name="oUser"></param>
    ''' <param name="UserName"></param>
    ''' <param name="Password"></param>
    ''' <remarks></remarks>
    Private Sub Gestion_Login(ByVal oUser As FSNServer.User, ByVal UserName As String, ByVal Password As String)
        Dim sSession As String = Nothing

        If oUser.ResultadoLogin <> TipoResultadoLogin.LoginOk Then
            If oUser.ResultadoLogin = TipoResultadoLogin.UsuBloqueado Then
                lblError.Visible = False
                'Bloqueo de cuenta: nÂº de intentos=mÃ¡x. permitido
                txtCuentaBloqueada.Visible = True
                txtContactoAdmin.Visible = True
            Else
                Session("FSN_User") = Nothing
                lblError.Visible = True
                If FSWSServer.TipoDeAutenticacion = TiposDeAutenticacion.Windows Then
                    TablaAutenticacion.Visible = False
                End If

                txtInfoIntentos.Visible = False
                txtCuentaBloqueada.Visible = False
                txtContactoAdmin.Visible = False
                If oUser.ResultadoLogin = TipoResultadoLogin.PwdIncorrecto Then
                    Dim oDatosBloq As Object = FSWSServer.LoginGestionBloqueoCuenta(UserName)
                    If oDatosBloq.LogPreBloq > 0 Then
                        If oDatosBloq.LogPreBloq = oDatosBloq.Bloq Then
                            lblError.Visible = False
                            'Bloqueo de cuenta: nÂº de intentos=mÃ¡x. permitido
                            txtCuentaBloqueada.Visible = True
                            txtContactoAdmin.Visible = True
                        Else
                            'Aviso de nÂº de intentos restantes
                            txtInfoIntentos.Visible = True
                            CType(Me.Page, FSNServer.IFSNPage).ModuloIdioma = TiposDeDatos.ModulosIdiomas.Login
                            txtInfoIntentos.InnerText = CType(Me.Page, FSNServer.IFSNPage).Textos(16)
                            txtInfoIntentos.InnerText = txtInfoIntentos.InnerText.Replace("@NUM_INT", (oDatosBloq.LogPreBloq - oDatosBloq.Bloq).ToString)
                        End If
                    End If
                End If
            End If
        Else
            If Not FSWSServer.TipoDeAutenticacion = TiposDeAutenticacion.Windows AndAlso Not FSWSServer.TipoDeAutenticacion = TiposDeAutenticacion.LDAP Then
                If FSWSServer.TipoAcceso.giEDAD_MAX_PWD > 0 AndAlso Date.Now > oUser.FechaPassword.AddDays(FSWSServer.TipoAcceso.giEDAD_MAX_PWD) Then
                    'La contraseÃ±a ha expirado
                    RequiredFieldValidator2.Enabled = False
                    Exit Sub
                End If
            End If

            'Si tiene acceso a algún producto creamos la variable de  y 
            'obtenemos las longitudes de campo, sino mostramos mensaje de error
            If Not oUser.AccesoPM AndAlso Not oUser.AccesoQA AndAlso Not oUser.AccesoEP AndAlso Not oUser.AccesoSM AndAlso Not oUser.AccesoFACT Then
                Session("FSN_User") = Nothing
                lblError.Visible = True
                If FSWSServer.TipoDeAutenticacion = TiposDeAutenticacion.Windows Then
                    TablaAutenticacion.Visible = False
                End If
            Else
                FSWSServer.Load_LongitudesDeCodigos()

                Session("FSN_User") = oUser
                Session("FSN_Server") = FSWSServer

                HttpContext.Current.User = Session("FSN_User")

                'A falta de más investigación esto es necesario para los WebPart
                ' Crear cookie de autenticaciÃƒÂ³n de usuario                    
                Dim ticket As FormsAuthenticationTicket = New FormsAuthenticationTicket(1,
                        UserName,
                        DateTime.Now,
                        DateTime.Now.AddMinutes(Session.Timeout),
                        False,
                        UserName,
                        FormsAuthentication.FormsCookiePath)
                ' Encrypt the ticket.
                Dim encTicket As String = FormsAuthentication.Encrypt(ticket)
                ' Create the cookie.
                Response.Cookies.Add(New HttpCookie(FormsAuthentication.FormsCookieName, encTicket))
                'Marcarla como segura si estas con https
                If InStr(ConfigurationManager.AppSettings("rutaFS"), "https:") > 0 Then
                    Dim cookie As HttpCookie
                    cookie = Request.Cookies(FormsAuthentication.FormsCookieName)
                    cookie.Secure = True
                    Response.SetCookie(cookie)
                End If

                If Request("TipoAcceso") <> Nothing AndAlso Request("Instancia") <> Nothing Then
                    Dim UrlDestino As String = Nothing

                    If Request("TipoAcceso") = "4" Then
                        UrlDestino = ConfigurationManager.AppSettings("rutaWSAprobacionEmail") & "AprobacionEmail.asmx/ProcesarAccionesPMEMail?Instancia=" & Request.QueryString("Instancia") & "&Bloque=" & Request.QueryString("Bloque") & "&Random=" & Request.QueryString("Random") & "&Usuario=" & Request.QueryString("Usuario") & "&Accion=" & Request.QueryString("Accion") & "&Tipo=" & Request.QueryString("Tipo") & "&FechaMail=" & Request.QueryString("FechaMail") & "&TipoSolicitud=" & Request.QueryString("TipoSolicitud")
                    End If
                    Response.Redirect(UrlDestino, True)
                End If

                If oUser.AccesoEP Then
                    Dim target As String
                    Dim RutaCompleta As String
                    Dim OrdenId As Integer
                    If Not Request.QueryString("Orden") Is Nothing Then
                        OrdenId = Request.QueryString("Orden")
                    End If
                    target = Request.QueryString("target")
                    If Not target Is Nothing Then
                        Select Case target
                            Case "aprob"
                                target = "Aprobacion.aspx"
                                RutaCompleta = ConfigurationManager.AppSettings("RutaEp") & target & "?Orden=" & OrdenId
                            Case "reemitir"
                                target = "Seguimiento.aspx"
                                RutaCompleta = ConfigurationManager.AppSettings("RutaEp") & target & "?Orden=" & OrdenId
                            Case "aprobEmail"
                                RutaCompleta = ConfigurationManager.AppSettings("RutaEp") & "App_Services/EP_Pedidos.asmx/AprobarPedidoEP?Idioma=" & Request.QueryString("Idioma") & "&Orden=" & Request.QueryString("Orden") & "&CodAprob=" & Request.QueryString("CodAprob") & "&Target=aprob" & "&CodRnd=" & Request.QueryString("CodRnd") & "&CodSeguridad=" & Request.QueryString("CodSeguridad") & "&FechaMail=" & Request.QueryString("FechaMail")
                            Case "rechazEmail"
                                RutaCompleta = ConfigurationManager.AppSettings("RutaEp") & "App_Services/EP_Pedidos.asmx/DenegarPedidoEP?Idioma=" & Request.QueryString("Idioma") & "&Orden=" & Request.QueryString("Orden") & "&CodAprob=" & Request.QueryString("CodAprob") & "&Target=aprob" & "&CodRnd=" & Request.QueryString("CodRnd") & "&CodSeguridad=" & Request.QueryString("CodSeguridad") & "&FechaMail=" & Request.QueryString("FechaMail")
                            Case Else
                                target = "Inicio.aspx"
                                RutaCompleta = ConfigurationManager.AppSettings("RutaEp") & target
                        End Select
                        Response.Redirect(RutaCompleta, True)
                    End If
                End If

                Response.Redirect("~/App_Pages/Inicio.aspx", False)
            End If
        End If
    End Sub
End Class