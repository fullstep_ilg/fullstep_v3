﻿Imports System.Web.Script.Serialization

Public Class SelectorPlantilla
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.SelectorPlantillasGS
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPantalla") Then
                Dim sVariableJavascriptTextosPantalla As String = "var TextosPantalla = new Array();"
                For i As Integer = 0 To 13
                    sVariableJavascriptTextosPantalla &= "TextosPantalla[" & i & "]='" & JSText(Textos(i)) & IIf(i = 2 OrElse i = 8, ":", "") & "';"
                Next
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextosPantalla, True)
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "proceso") Then
                Dim sproceso As String = "var proceso = '" &
                    "&a=" & Request.QueryString("a") & "&m=" & Request.QueryString("m") & "&d=" & Request.QueryString("d") &
                    "&h=" & Request.QueryString("h") & "&mn=" & Request.QueryString("mn") & "&s=" & Request.QueryString("s") & "';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "proceso", sproceso, True)
            End If
            Dim item As ListItem
            With optFormatoExportacion
                item = New ListItem
                item.Value = FormatoPlantillaGS.DOC
                item.Text = JSText(Textos(3))
                .Items.Add(item)
                item = New ListItem
                item.Value = FormatoPlantillaGS.DOCX
                item.Text = JSText(Textos(4))
                .Items.Add(item)
                item = New ListItem
                item.Value = FormatoPlantillaGS.PDF
                item.Text = JSText(Textos(5))
                .Items.Add(item)
                item = New ListItem
                item.Value = FormatoPlantillaGS.ODT
                item.Text = JSText(Textos(6))
                .Items.Add(item)
                item = New ListItem
                item.Value = FormatoPlantillaGS.EPUB
                item.Text = JSText(Textos(7))
                .Items.Add(item)
            End With
            Dim tipoDocumento As Integer
            tipoDocumento = IIf(Request.QueryString("tipoDocumento") Is Nothing, 0, CType(Request.QueryString("tipoDocumento"), Integer))
            Dim oPlantillas As FSNServer.Plantillas = FSNServer.Get_Object(GetType(FSNServer.Plantillas))
            Dim plantillas As List(Of FSNServer.InfoPlantilla)
            Select Case tipoDocumento
                Case DocumentoPlantillaGS.Agenda
                    plantillas = oPlantillas.GS_Obtener_Plantillas_Usu_Documento(Usuario.Cod, DocumentoPlantillaGS.Agenda, Usuario.Idioma, _
                                    Usuario.UON1, Usuario.UON2, Usuario.UON3)
                Case (DocumentoPlantillaGS.Acta)
                    plantillas = oPlantillas.GS_Obtener_Plantillas_Usu_Documento(Usuario.Cod, DocumentoPlantillaGS.Acta, Usuario.Idioma, _
                                    Usuario.UON1, Usuario.UON2, Usuario.UON3)
                Case Else
                    plantillas = oPlantillas.GS_Obtener_Plantillas_Usu_Documento(Usuario.Cod, DocumentoPlantillaGS.Agenda, Usuario.Idioma, _
                                    Usuario.UON1, Usuario.UON2, Usuario.UON3)
            End Select
            Dim serializer As New JavaScriptSerializer()
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "infoPlantillas", "var tipoDocumento=" & tipoDocumento & ";" &
                        "var idioma='" & FSNUser.IdiomaCod & "';var infoPlantillas=" & serializer.Serialize(DirectCast(plantillas, Object)).ToString() & ";", True)
        End If
    End Sub
#Region "Page Methods"
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Sub GS_Plantilla_Defecto_Usu(ByVal idPlantilla As Integer, ByVal formato As Integer)
        Try
            Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")

            Dim oPlantillas As FSNServer.Plantillas = FSNServer.Get_Object(GetType(FSNServer.Plantillas))
            oPlantillas.GS_Plantilla_Defecto_Usu(User.Cod, idPlantilla, formato)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
End Class