﻿Imports Aspose.Words
Imports System.IO
Imports Aspose.Words.Reporting
Imports Aspose.Words.Tables
Imports Aspose.Words.Saving

Public Class agendaExport
    Inherits FSNPage
    Shared i As New Integer

    ''' <summary>
    ''' Descarga la agenda
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>  
    ''' <remarks>Llamada desde: Menu.master.vb   Reuniones\SelectorPlantilla.aspx; Tiempo máximo: 0,2</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dataDir As String = ConfigurationManager.AppSettings("temp")
        If Not IsPostBack Then
            Dim fecha As DateTime
            Dim IdPlantilla, Formato As Integer
            With Request
                fecha = New DateTime(.QueryString("a"), .QueryString("m"), .QueryString("d"), .QueryString("h"), .QueryString("mn"), .QueryString("s"))
                IdPlantilla = CType(.QueryString("IdPlantilla"), Integer)
                Formato = CType(.QueryString("Formato"), Integer)
            End With
            ' Sample infrastructure.
            Dim dsDatosAgenda As DataSet
            Dim oReunionAgenda As FSNServer.Reuniones
            oReunionAgenda = FSNServer.Get_Object(GetType(FSNServer.Reuniones))
            dsDatosAgenda = oReunionAgenda.Reunion_Agenda(fecha, FSNUser.Idioma)
            'OBTENER PLANTILLA DE BASE DE DATOS
            Dim oPlantillas As FSNServer.Plantillas = FSNServer.Get_Object(GetType(FSNServer.Plantillas))
            Dim oPlantilla As FSNServer.InfoPlantilla
            oPlantilla = oPlantillas.GS_Visor_Plantillas_DetallePlantilla(IdPlantilla, FSNUser.Idioma)
            Dim byteBufferPlantilla() As Byte
            byteBufferPlantilla = oPlantillas.GS_Plantillas_GetAdjuntoPlantilla(IdPlantilla, FSNUser.Idioma)
            Dim RandomDirectory As String = modUtilidades.GenerateRandomPath()
            While IO.Directory.Exists(dataDir & "\" & RandomDirectory)
                RandomDirectory = modUtilidades.GenerateRandomPath()
            End While
            IO.Directory.CreateDirectory(dataDir & "\" & RandomDirectory)
            Dim temporal As String
            temporal = dataDir & "\" & RandomDirectory & "\" & oPlantilla.languageInfo(FSNUser.Idioma).NombreArchivo
            Dim oFS As New FileStream(temporal, FileMode.Append, FileAccess.Write)
            oFS.Write(byteBufferPlantilla, 0, byteBufferPlantilla.Length)
            oFS.Close()
            'ABRIR DOCUMENTO GUARDADO
            i = 0
            ' Open the template document.
            Dim doc As New Document(temporal)
            doc.MailMerge.FieldMergingCallback = New HandleMergeFieldAlternatingRows()
            ' Certain regions can be skipped from applying logic to by not adding the table name inside the CreateEmptyDataSource method.
            ' Enable this cleanup option so any regions which are not handled by the user's logic are removed automatically.
            doc.MailMerge.CleanupOptions = MailMergeCleanupOptions.RemoveUnusedRegions
            ' Execute the nested mail merge with regions
            doc.MailMerge.ExecuteWithRegions(dsDatosAgenda)
            Dim bmNode As Node
            For j As Integer = 1 To i
                If doc.Range.Bookmarks("DELETE_COL_" & j) IsNot Nothing Then
                    bmNode = doc.Range.Bookmarks("DELETE_COL_" & j).BookmarkStart
                    If DirectCast(bmNode.ParentNode, Paragraph).ParentNode.NodeType = NodeType.Cell Then
                        'Get cell with bookmark
                        Dim cell As Cell = DirectCast(DirectCast(bmNode.ParentNode, Paragraph).ParentNode, Cell)
                        'Get index of cell
                        Dim cellIndex As Integer = cell.ParentRow.IndexOf(cell)
                        'Remove all cells with cellIndex
                        For Each row As Row In cell.ParentRow.ParentTable
                            row.Cells(cellIndex).Remove()
                        Next
                    End If
                End If
            Next
            Dim guidi As String = Guid.NewGuid.ToString
            Dim OutputFormat As String
            Dim oFileStream As System.IO.FileStream
            Select Case Formato
                Case FormatoPlantillaGS.DOC 'DOC                    
                    OutputFormat = ".doc"
                Case FormatoPlantillaGS.DOCX 'DOCX
                    OutputFormat = ".docx"
                Case FormatoPlantillaGS.PDF 'PDF
                    OutputFormat = ".pdf"
                Case FormatoPlantillaGS.ODT  'ODT
                    OutputFormat = ".odt"
                Case 4 'EPUB
                    OutputFormat = ".epub"
                    Dim epubOptions As New HtmlSaveOptions(SaveFormat.Epub)
                    epubOptions.ExportHeadersFootersMode = ExportHeadersFootersMode.None
                    ' This option improves how tables are displayed in the reader.
                    epubOptions.TableWidthOutputMode = HtmlElementSizeOutputMode.None
                    doc.Save(dataDir & "\" & RandomDirectory & "\" & guidi & OutputFormat, epubOptions)
                Case Else
                    OutputFormat = ".docx"
            End Select
            If Not Formato = 4 Then doc.Save(dataDir & "\" & RandomDirectory & "\" & guidi & OutputFormat)
            oFileStream = New System.IO.FileStream(dataDir & "\" & RandomDirectory & "\" & guidi & OutputFormat, IO.FileMode.Open)
            Dim byteBuffer() As Byte
            ReDim byteBuffer(oFileStream.Length - 1)
            Call oFileStream.Read(byteBuffer, 0, Int32.Parse(oFileStream.Length.ToString()))
            oFileStream.Close()
            Context.Response.Clear()
            Context.Response.AddHeader("Content-Type", "application/octet-stream")
            Context.Response.AddHeader("Content-Disposition", "attachment;filename=""Agenda""" & OutputFormat)
            Context.Response.BinaryWrite(byteBuffer)
            Context.Response.End()
        End If
    End Sub
    Private Class HandleMergeFieldAlternatingRows
        Inherits FSNPage
        Implements IFieldMergingCallback
        ''' <summary>
        ''' Called for every merge field encountered in the document.
        ''' We can either return some data to the mail merge engine or do something
        ''' else with the document. In this case we modify cell formatting.
        ''' </summary>
        Private Sub IFieldMergingCallback_FieldMerging(ByVal e As FieldMergingArgs) Implements IFieldMergingCallback.FieldMerging
            If mBuilder Is Nothing Then
                mBuilder = New DocumentBuilder(e.Document)
            End If
            Dim Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Select Case e.FieldName
                Case "FEC_REUNION", "FECAPER", "FECPRES", "FECNEC", "FINI_SUM_PROCE", "FFIN_SUM_PROCE", "FINI_SUMINISTRO", "FFIN_SUMINISTRO", "FINI_SUM_GR", "FFIN_SUM_GR"
                    If IsDBNull(e.FieldValue) Then
                        e.Text = ""
                    Else
                        e.Text = Format(CType(e.FieldValue, Date), Usuario.DateFormat.ShortDatePattern)
                    End If
                Case "HORA_INI_REU", "HORA_PROCESO"
                    If IsDBNull(e.FieldValue) Then
                        e.Text = ""
                    Else
                        e.Text = Format(CType(e.FieldValue, Date), Usuario.DateFormat.ShortTimePattern)
                    End If
                Case "HORA_FIN_REU"
                    If IsDBNull(e.FieldValue) Then
                        e.Text = ""
                    Else
                        e.Text = " - " & Format(CType(e.FieldValue, Date), Usuario.DateFormat.ShortTimePattern)
                    End If
                Case "FECLIMOFE"
                    If IsDBNull(e.FieldValue) Then
                        e.Text = ""
                    Else
                        e.Text = Format(CType(e.FieldValue, Date), Usuario.DateFormat.ShortDatePattern) & " " & Format(CType(e.FieldValue, Date), Usuario.DateFormat.ShortTimePattern)
                    End If
                Case "CANTIDAD"
                    e.Text = FSNLibrary.modUtilidades.FormatNumber(DBNullToDbl(e.FieldValue), Usuario.NumberFormat)
                Case "VOL_PROCESO"
                    If IsDBNull(e.FieldValue) Then
                        e.Text = 0
                    Else
                        e.Text = FSNLibrary.modUtilidades.FormatNumber(DBNullToDbl(e.FieldValue), Usuario.NumberFormat)
                    End If
                Case "TIPO_REUNION"
                    ModuloIdioma = TiposDeDatos.ModulosIdiomas.agendaExport
                    If CType(e.FieldValue, Boolean) Then
                        e.Text = Textos(0)
                    Else
                        e.Text = Textos(1)
                    End If
                Case "ITEM_DEST", "ITEM_PAG", "ITEM_FECSUM"
                    If Not CType(e.FieldValue, Boolean) Then
                        i += 1
                        Dim builder As New DocumentBuilder(e.Document)
                        builder.MoveToField(e.Field, True)
                        builder.StartBookmark("DELETE_COL_" & i)
                        builder.Writeln("DELETE_COL_" & i)
                        builder.EndBookmark("DELETE_COL_" & i)
                    End If
                    e.Text = ""
                Case Else
            End Select
        End Sub
        Private Sub ImageFieldMerging(ByVal args As ImageFieldMergingArgs) Implements IFieldMergingCallback.ImageFieldMerging
        End Sub
        Private mBuilder As DocumentBuilder
        Private mRowIdx As Integer
    End Class
End Class