﻿var SelectedRows = [];
var SelectedRowCount = 0;
var infoPlantillaAlta, infoPlantilla, idiomaDefectoUsuario;
var celdaHtmlAntes;
var timeoutNuevoMensaje;
$(document).ready(function () {
    if (typeof (EnBlancoMaster) !== 'undefined' && EnBlancoMaster) $('body').addClass('fondoMasterEnBlanco');
    $('#divImgRestricUONs').append('<img id="imgParaUON" class="Image16" style="padding:2px;" src="' + ruta + 'images/UON.png" />');
    $('.CollapsiblePanelHeader').click(function () {
        $(this).toggleClass('CollapsiblePanelHeaderExpand').toggleClass('CollapsiblePanelHeaderCollapse');
        $('#' + $(this).attr('dependantPanel')).toggle();
    });
    $('#btnCancelarPlantilla').live('click', function () {
        TabActivo(idiomaDefectoUsuario);
        $('#popupFondo').hide();
        $('#popupPlantilla').hide();
    });
    $('#btnAceptarPlantilla').live('click', function () {        
        if (Comprobacion_Datos_Correcta()) GuardarPlantilla();
    });
    $('#btnCancelarFormatoDefecto').live('click', function () {
        $('#popupFormatoDefectoPlantillas').hide();
    });
    $('#btnAceptarFormatoDefecto').live('click', function () {
        AsignarFormatoDefectoPlantillas();
    });
    $('#optDocumentoPlantilla').live('change', function () {
        if ($(this).val() == 2) $('#divVistaPlantilla').show()
        else $('#divVistaPlantilla').hide();
    });
    $('#lblDefectoPlantilla').text(TextosPantalla[1]);
    $('#lblFormatoDefectoPlantilla').text(TextosPantalla[2]);
    $('#lblRestricUONPlantilla').text(TextosPantalla[3]);
    $('#lblAceptarPlantilla').text(TextosPantalla[5]);
    $('#lblCancelarPlantilla').text(TextosPantalla[6]);
    $('#lblAceptarFormatoDefecto').text(TextosPantalla[5]);
    $('#lblCancelarFormatoDefecto').text(TextosPantalla[6]);
    $('#lblMenuPlantilla').text(TextosPantalla[7]);
    $('#lblDocumentoPlantilla').text(TextosPantalla[8]);
    $('#lbltabPlantillas').text(TextosPantalla[9]);
    $('#lblUONsPlantilla').text(TextosPantalla[11]);
    $('#lblFormatoDefectoPlantillas').text(TextosPantalla[15]);
    $.get(rutaFS + 'GS/Reuniones/html/_adjuntoPlantilla.tmpl.htm', function (templates) {
        $('body').append(templates);
        $('#fileupload').fileupload();
        $('#fileupload').fileupload('onFileBeforeUpload', function () { HabilitarBotones(false) });
        $('#fileupload').fileupload('onFileAfterUpload', function (e) { AsociarPlantillaIdioma(e) });
        $('#fileupload').show();
        $('#fileupload .fileinput-button').css('right', '');
        $('#fileupload .fileinput-button').css('left', -10000);
        $('#fileupload .fileinput-button').css('top', -10000);
        $('#fileupload .fileinput-button').live('mouseleave click', function () {
            $('#fileupload .fileinput-button').css('left', -10000);
            $('#fileupload .fileinput-button').css('top', -10000);
            $('.Seleccionable').removeClass('Seleccionable');
        });
        $("#formFileupload").attr("action", rutaFS + 'cn/FileTransferHandler.ashx');
    });
    $('.template-download .name').live('click', function () {
        var nomPlantilla = $(this).closest('.template-download').attr('data-name');
        if ($(this).closest('.template-download').attr('data-url') == '') {
            GetPlantilla(infoPlantilla.Id, nomPlantilla, $(this).closest('[id^=divPlantillaIdioma_]').attr('id').split('_')[1]);
        } else {
            var filename = $(this).closest('.template-download').attr('data-url');            
            downloadWindow = window.open(rutaFS + '_Common/downloadfile.aspx?e=1&f=' + filename, 'downloadAttach', 'width=1px,height=1px,top=5000,location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =no');
            downloadWindow.moveTo(-1000, -1000);            
        }
    });
    var selectFormatosPlantilla = $('#optFormatosDefectoPlantilla');
    if (selectFormatosPlantilla.prop) {
        var optionsFormatosPlantilla = selectFormatosPlantilla.prop('options');
    }
    else {
        var optionsFormatosPlantilla = selectFormatosPlantilla.attr('options');
    }
    $('option', selectFormatosPlantilla).remove();
    $.each(dataFormatos, function (key, value) {
        optionsFormatosPlantilla[optionsFormatosPlantilla.length] = new Option(value.text, parseInt(value.value));
    });
});
function IndexChanged() {
    var dropdownlist = $('[id$=PagerPageList]')[0];
    var btnPager = $('[id$=btnPager]').attr('id');
    var selectedIndex = dropdownlist.selectedIndex;
    __doPostBack(btnPager, dropdownlist.selectedIndex);
}
function FirstPage() {
    var dropdownlist = $('[id$=PagerPageList]')[0];
    var btnPager = $('[id$=btnPager]').attr('id');
    dropdownlist.options[0].selected = true;
    __doPostBack(btnPager, 0);
}
function PrevPage() {
    var dropdownlist = $('[id$=PagerPageList]')[0];
    var btnPager = $('[id$=btnPager]').attr('id');
    var selectedIndex = dropdownlist.selectedIndex;
    if (selectedIndex - 1 >= 0) {
        dropdownlist.options[selectedIndex - 1].selected = true;
        __doPostBack(btnPager, dropdownlist.selectedIndex);
    }
}
function NextPage() {
    var dropdownlist = $('[id$=PagerPageList]')[0];
    var btnPager = $('[id$=btnPager]').attr('id');
    var selectedIndex = dropdownlist.selectedIndex;
    if (selectedIndex + 1 <= dropdownlist.length - 1) {
        dropdownlist.options[selectedIndex + 1].selected = true;
        __doPostBack(btnPager, dropdownlist.selectedIndex);
    }
}
function LastPage() {
    var dropdownlist = $('[id$=PagerPageList]')[0];
    var btnPager = $('[id$=btnPager]').attr('id');
    dropdownlist.options[dropdownlist.length - 1].selected = true;
    __doPostBack(btnPager, dropdownlist.length - 1);
}
function whdgPlantillas_RowSelected(sender, e) {
    var Add, Delete;
    if (sender.get_behaviors().get_selection().get_selectedRowsResolved().length <= SelectedRowCount) {
        var RowDelete;
        for (i = SelectedRows.length - 1; i >= 0; i--) {
            RowDelete = SelectedRows[i];
            Delete = true;
            $.each(sender.get_behaviors().get_selection().get_selectedRowsResolved(), function () {
                if (RowDelete.Id == this.get_idPair().key[0]) {
                    Delete = false;
                    return false;
                }
            });
            if (Delete) SelectedRows.splice(i, 1);
        }
    }
    var key, id;
    for (j = 0; j < sender.get_behaviors().get_selection().get_selectedRows().get_length(); j++) {
        Add = true;
        $.each(SelectedRows, function () {
            if (this.Id == sender.get_behaviors().get_selection().get_selectedRows().getItemID(j).key[0]) {
                Add = false;
                return false;
            }
        });
        if (Add) {
            var row = sender.get_behaviors().get_selection().get_selectedRows().getItem(j);
            id = row.get_cellByColumnKey('ID').get_value();
            var item = { Id: id };
            SelectedRows.push(item);
        }
    }
    SelectedRowCount = SelectedRows.length;
}
function AgregarPlantilla() {
    $('#divUONsPlantilla').hide();
    /*Obtenemos en formato string todos los datos que mostraremos en la pantalla*/
    if (infoPlantillaAlta) {
        infoPlantilla = $.parseJSON(JSON.stringify(infoPlantillaAlta))
        DatosAltaPlantilla();
    } else {
        $.when($.ajax({
            type: 'POST',
            url: rutaFS + 'GS/Reuniones/VisorPlantillasReunion.aspx/Obtener_Datos_Plantilla',
            data: JSON.stringify({ idPlantilla: 0 }),
            contentType: 'application/json;',
            async: true
        })).done(function (msg) {
            infoPlantillaAlta = $.parseJSON(msg.d);
            infoPlantilla = $.parseJSON(JSON.stringify(infoPlantillaAlta));
            DatosAltaPlantilla();
        });
    }
}
function DatosAltaPlantilla() {
    $('#popupPlantilla').css('width', $(window).outerWidth() * 0.6);
    $('[id^=divTab_]').live('click', function () {
        TabActivo($(this).attr('id').split('_')[1]);
    });
    $('[id^=txtPlantillaDenominacion_]').val('');
    $('[id^=txtPlantillaComentario_]').val('');
    $('[id^=divPlantillaIdioma_]').empty();
    $('#chkDefectoPlantilla').attr('checked', false);
    var selectMenu = $('#optMenuPlantilla');
    var selectDocumento = $('#optDocumentoPlantilla');
    var selectVista = $('#optVistaPlantilla');
    var selectFormato = $('#optFormatoDefectoPlantilla');
    if (selectMenu.prop) {
        var optionsMenu = selectMenu.prop('options');
        var optionsDocumento = selectDocumento.prop('options');
        var optionsVista = selectVista.prop('options');
        var optionsFormato = selectFormato.prop('options');
    }
    else {
        var optionsMenu = selectMenu.attr('options');
        var optionsDocumento = selectDocumento.attr('options');
        var optionsVista = selectVista.attr('options');
        var optionsFormato = selectFormato.attr('options');
    }
    $('option', selectMenu).remove();
    $('option', selectDocumento).remove();
    $('option', selectVista).remove();
    $('option', selectFormato).remove();
    $.each(infoPlantilla.menuData, function (key, value) {
        optionsMenu[optionsMenu.length] = new Option(value, key);
    });
    $.each(infoPlantilla.documentoData, function (key, value) {
        optionsDocumento[optionsDocumento.length] = new Option(value, key);
    });
    $.each(infoPlantilla.vistaData, function (key, value) {
        optionsVista[optionsVista.length] = new Option(value, key);
    });
    $.each(infoPlantilla.formatoData, function (key, value) {
        optionsFormato[optionsFormato.length] = new Option(value, key);
    });
    $('#divCabeceraPlantilla span').text(TextosPantalla[0]);
    if ($('#divTabsIdiomaPlantilla').children().length == 0) {
        $.each(infoPlantilla.languageInfo, function () {
            if (this.IdiomaDefecto) idiomaDefectoUsuario = this.Idioma;
            $('#divTabIdioma').tmpl(this).appendTo($('#divTabsIdiomaPlantilla'));
            $('.fileupload-content').before($('#divPlantillaIdioma').tmpl(this));
            infoPlantilla.languageInfo[this.Idioma].Denominacion = '';
            infoPlantilla.languageInfo[this.Idioma].Comentario = '';
        });
    }
    $('[id^=lblPlantillaDenominacion_]').text(TextosPantalla[12] + ':');
    $('[id^=lblPlantillaComentario_]').text(TextosPantalla[13] + ':');
    $('[id^=lblPlantillaDefecto_]').text(TextosPantalla[14]);
    $('#optDocumentoPlantilla').val(1);
    $('#divVistaPlantilla').hide();
    TabActivo(idiomaDefectoUsuario);
    $('[id^=imgPlantillaDefecto_]').attr('src', ruta + 'Images/adjunto.png');
    $('#divPlantillaDefecto').show();
    $('#divListaRestricUONs').empty();
    CentrarPopUp($('#popupPlantilla'));
    $('#popupFondo').css('height', $(document).height());
    $('#popupFondo').show();
    $('#popupPlantilla').show();
}
function DatosPlantilla() {
    $('#popupPlantilla').css('width', $(window).outerWidth() * 0.6);
    $('[id^=divTab_]').live('click', function () {
        TabActivo($(this).attr('id').split('_')[1]);
    });
    $('[id^=txtPlantillaDenominacion_]').val('');
    $('[id^=txtPlantillaComentario_]').val('');
    $('[id^=divPlantillaIdioma_]').empty();
    $('#chkDefectoPlantilla').attr('checked', infoPlantilla.PlantillaDefecto);
    if (infoPlantilla.PlantillaDefecto) $('#chkDefectoPlantilla').attr('disabled', true);
    else $('#chkDefectoPlantilla').attr('disabled', false);
    var selectMenu = $('#optMenuPlantilla');
    var selectDocumento = $('#optDocumentoPlantilla');
    var selectVista = $('#optVistaPlantilla');
    var selectFormato = $('#optFormatoDefectoPlantilla');
    if (selectMenu.prop) {
        var optionsMenu = selectMenu.prop('options');
        var optionsDocumento = selectDocumento.prop('options');
        var optionsVista = selectVista.prop('options');
        var optionsFormato = selectFormato.prop('options');
    }
    else {
        var optionsMenu = selectMenu.attr('options');
        var optionsDocumento = selectDocumento.attr('options');
        var optionsVista = selectVista.attr('options');
        var optionsFormato = selectFormato.attr('options');
    }
    $('option', selectMenu).remove();
    $('option', selectDocumento).remove();
    $('option', selectVista).remove();
    $('option', selectFormato).remove();
    $.each(infoPlantilla.menuData, function (key, value) {
        optionsMenu[optionsMenu.length] = new Option(value, key);
    });
    $.each(infoPlantilla.documentoData, function (key, value) {
        optionsDocumento[optionsDocumento.length] = new Option(value, key);
    });
    $.each(infoPlantilla.vistaData, function (key, value) {
        optionsVista[optionsVista.length] = new Option(value, key);
    });
    $.each(infoPlantilla.formatoData, function (key, value) {
        optionsFormato[optionsFormato.length] = new Option(value, key);
    });
    $('#divCabeceraPlantilla span').text(TextosPantalla[18]);
    $('#optMenuPlantilla').val(infoPlantilla.Menu);
    $('#optDocumentoPlantilla').val(infoPlantilla.Documento);
    if (infoPlantilla.Documento == 2) $('#divVistaPlantilla').show()
    else $('#divVistaPlantilla').hide();
    $('#optVistaPlantilla').val(infoPlantilla.Vista);
    $('#optFormatoDefectoPlantilla').val(infoPlantilla.FormatoDefecto);
    if ($('#divTabsIdiomaPlantilla').children().length == 0) {
        $.each(infoPlantilla.languageInfo, function () {
            $('#divTabIdioma').tmpl(this).appendTo($('#divTabsIdiomaPlantilla'));
            $('.fileupload-content').before($('#divPlantillaIdioma').tmpl(this));
            if (this.IdiomaDefecto) idiomaDefectoUsuario = this.Idioma;
        });
    }
    $.each(infoPlantilla.languageInfo, function () {
        $('#txtPlantillaDenominacion_' + this.Idioma).val(infoPlantilla.languageInfo[this.Idioma].Denominacion);
        $('#txtPlantillaComentario_' + this.Idioma).val(infoPlantilla.languageInfo[this.Idioma].Comentario);

        //crear ficheros
        if (infoPlantilla.languageInfo[this.Idioma].size >= 1000000000) {
            infoPlantilla.languageInfo[this.Idioma].sizeunit = 'GB';
        } else {
            if (infoPlantilla.languageInfo[this.Idioma].size >= 1000000) {
                infoPlantilla.languageInfo[this.Idioma].sizeunit = 'MB';
            } else { infoPlantilla.languageInfo[this.Idioma].sizeunit = 'KB'; }
        }
        infoPlantilla.languageInfo[this.Idioma].error = false;
        infoPlantilla.languageInfo[this.Idioma].name = infoPlantilla.languageInfo[this.Idioma].NombreArchivo;
        infoPlantilla.languageInfo[this.Idioma].url = '';
        $('#template-download').tmpl(infoPlantilla.languageInfo[this.Idioma]).appendTo($('#divPlantillaIdioma_' + this.Idioma));
        $('#divPlantillaIdioma_' + this.Idioma).find('.delete button').button({
            text: false,
            icons: { primary: 'ui-icon-trash' }
        });
    });
    $('#divListaRestricUONs').empty();
    $.each(infoPlantilla.UONsPlantilla, function () {
        $('#divListaRestricUONs').append('<div class="ItemPara" id="' + this.value + '">' + this.text + '</div>');
    });
    TabActivo(idiomaDefectoUsuario);
    $('[id^=lblPlantillaDefecto_]').text(TextosPantalla[14]);
    $('[id^=imgPlantillaDefecto_]').attr('src', ruta + 'Images/adjunto.png');
    $('#divPlantillaDefecto').show();
    CentrarPopUp($('#popupPlantilla'));
    $('#popupFondo').css('height', $(document).height());
    $('#popupFondo').show();
    $('#popupPlantilla').show();
}
$('[id^=divPlantillaDefecto_]').live('mouseenter', function () {    
    $('#fileupload .fileinput-button').css('width', $(this).outerWidth());
    $('#fileupload .fileinput-button').css('height', $(this).outerHeight());
    if ($(this).attr('id').indexOf('Master') == 0) {
        $('#fileupload .fileinput-button').css('left', $(this).position().left);
        $('#fileupload .fileinput-button').css('top', $(this).position().top);
    } else {
        $('#fileupload .fileinput-button').css('left', $(this).offset().left);
        $('#fileupload .fileinput-button').css('top', $(this).offset().top);
    }
    $('#fileupload .fileinput-button').css('z-index', 10000);
    $('#fileupload .fileinput-button').css('right', '');
});
function TabActivo(idiomaActivo) {
    var idiomaInactivo = $('#divTabsIdiomaPlantilla .tabInfoActive').attr('id').split('_')[1];
    infoPlantilla.languageInfo[idiomaInactivo].Denominacion = $('#txtPlantillaDenominacion_' + idiomaInactivo).val();
    infoPlantilla.languageInfo[idiomaInactivo].Comentario = $('#txtPlantillaComentario_' + idiomaInactivo).val();

    $('#divTabsIdiomaPlantilla .tabInfoActive').removeClass('tabInfoActive');
    $('#divTab_' + idiomaActivo).addClass('tabInfoActive');
    $('[id^=infoPanel_]').hide();
    $('#infoPanel_' + idiomaActivo).show();
}
function HabilitarBotones(habilitar) {
    if (habilitar) {
        $('[id^=divTab_]').live('click', function () {
            TabActivo($(this).attr('id').split('_')[1]);
        });
        $('#btnAceptarPlantilla').live('click', function () {
            if (Comprobacion_Datos_Correcta()) GuardarPlantilla();
        });
    } else {
        $('[id^=divTab_]').die('click');
        $('#btnAceptarPlantilla').die('click');
    }
}
function AsociarPlantillaIdioma(plantilla) {
    var idiomaActivo = $('#divTabsIdiomaPlantilla .tabInfoActive').attr('id').split('_')[1];
    $('#divPlantillaIdioma_' + idiomaActivo).empty();
    $('#divPlantillaIdioma_' + idiomaActivo).append(plantilla);
    CentrarPopUp($('#popupPlantilla'));
    HabilitarBotones(true)
}
function Comprobacion_Datos_Correcta() {    
    var idiomaActivo = $('#divTabsIdiomaPlantilla .tabInfoActive').attr('id').split('_')[1];
    TabActivo(idiomaActivo);
    var comprobacionesCorrectas = true;
    $.each(infoPlantilla.languageInfo, function () {
        var oPlantilla = this;
        if (oPlantilla.Denominacion == '') {
            alert(TextosPantalla[16] + ' ' + oPlantilla.DenominacionIdioma);
            comprobacionesCorrectas = false;
            return false;
        }
        if ($('#divPlantillaIdioma_' + oPlantilla.Idioma).children().length == 0) {
            alert(TextosPantalla[17] + ' ' + oPlantilla.DenominacionIdioma);
            comprobacionesCorrectas = false;
            return false;
        }
    });    
    return comprobacionesCorrectas;
}
function GuardarPlantilla() {
    infoPlantilla.Menu = $('#optMenuPlantilla').val();
    infoPlantilla.Documento = $('#optDocumentoPlantilla').val();
    infoPlantilla.Vista = ($('#optVistaPlantilla').is(':visible') ? $('#optVistaPlantilla').val() : 0);
    infoPlantilla.FormatoDefecto = $('#optFormatoDefectoPlantilla').val();
    infoPlantilla.PlantillaDefecto = $('#chkDefectoPlantilla').is(':checked');
    $.each(infoPlantilla.languageInfo, function () {
        var oPlantilla = this;
        $.each($('#divPlantillaIdioma_' + oPlantilla.Idioma).children(), function () {
            oPlantilla.NombreArchivo = $(this).attr('data-name');
            oPlantilla.Denominacion = $('#txtPlantillaDenominacion_' + oPlantilla.Idioma).val();
            oPlantilla.Comentario = $('#txtPlantillaComentario_' + oPlantilla.Idioma).val();
            oPlantilla.path = $(this).attr('data-url');
        });
    });
    var UONsPlantilla = [];
    var UON1, UON2, UON3;
    $.each($('#divListaRestricUONs .ItemPara'), function () {
        UON1 = null;
        UON2 = null;
        UON3 = null;
        for (i = 1; i < $(this).attr("id").split("-").length; i++) {
            switch ($(this).attr("id").split("-")[i].split("_")[0]) {
                case "UON1":
                    UON1 = $(this).attr("id").split("-")[i].split("_")[1];
                    break;
                case "UON2":
                    UON2 = $(this).attr("id").split("-")[i].split("_")[1];
                    break;
                case "UON3":
                    UON3 = $(this).attr("id").split("-")[i].split("_")[1];
                    break;
            }
        }
        codigo = { UON1: UON1, UON2: UON2, UON3: UON3 };
        UONsPlantilla.push(codigo);
    });
    /*QUITAMOS EL TEXTO DE LOS INPUT TEXT POR POSIBLE ERROR CON CARACTERES RAROS EN EL POST BACK*/
    $('[id^=txtPlantillaDenominacion_]').val('');
    $('[id^=txtPlantillaComentario_]').val('');
    $.when($.ajax({
        type: 'POST',
        url: rutaFS + 'GS/Reuniones/VisorPlantillasReunion.aspx/Plantilla_Guardar',
        data: JSON.stringify({ Plantilla: infoPlantilla, UONsPlantilla: UONsPlantilla }),
        contentType: 'application/json;',
        async: false
    })).done(function (msg) {
        RecargarGridPlantillas();
        $('#popupFondo').hide();
        $('#popupPlantilla').hide();
    });
}
function whdgPlantillas_PageIndexChanged() {
    var grid = $find($('[id$=whdgPlantillas]').attr('id')).get_gridView();
    var dropdownlist = $('[id$=PagerPageList]')[0];

    dropdownlist.options[grid.get_behaviors().get_paging().get_pageIndex()].selected = true;
}
function whdgPlantillas_CellClick(sender, e) {
    /*Cuando hace click en una celda, si es el código o la denominación redirige al detalle del perfil*/
    if (e.get_type() == "cell" && e.get_item().get_row().get_index() !== -1) {
        if (e.get_item().get_grid().get_behaviors().get_activation().get_activeCellResolved() !== null) {
            var cell = e.get_item().get_grid().get_behaviors().get_activation().get_activeCellResolved();
            switch (cell.get_column().get_key()) {
                case "IMAGE":
                case "NOM":
                    var idPlantilla = cell.get_row().get_cellByColumnKey("ID").get_value();
                    var nomPlantilla = cell.get_row().get_cellByColumnKey("NOM").get_value();
                    GetPlantilla(idPlantilla, nomPlantilla);
                    break;
                case "DEFECTO":
                    var idPlantilla = cell.get_row().get_cellByColumnKey("ID").get_value();
                    var documento = cell.get_row().get_cellByColumnKey("DOCUMENTO").get_value();
                    var vista = cell.get_row().get_cellByColumnKey("VISTA").get_value();
                    vista = (vista == '' ? 0 : vista);
                    $.when($.ajax({
                        type: 'POST',
                        url: rutaFS + 'GS/Reuniones/VisorPlantillasReunion.aspx/Plantilla_Defecto',
                        data: JSON.stringify({ idPlantilla: idPlantilla, documento: documento, vista: vista }),
                        contentType: 'application/json;',
                        async: true
                    })).done(function (msg) {
                        RecargarGridPlantillas();
                    });
                    break;
                case "UONSDETALLE":
                    var idPlantilla = cell.get_row().get_cellByColumnKey("ID").get_value();
                    var visible = $('#divUONsPlantillaAbrev_' + idPlantilla).is(':visible');
                    $('[id^=divUONsPlantillaAbrev_]').show();
                    $('[id^=divUONsPlantillaDetalle_]').hide();
                    if (visible) {
                        $('#divUONsPlantillaAbrev_' + idPlantilla).hide();
                        $('#divUONsPlantillaDetalle_' + idPlantilla).show();
                    } else {
                        $('#divUONsPlantillaAbrev_' + idPlantilla).show();
                        $('#divUONsPlantillaDetalle_' + idPlantilla).hide();
                    }
                    break;
                default:
                    $('#divUONsPlantilla').hide();
                    var idPlantilla = cell.get_row().get_cellByColumnKey("ID").get_value();
                    $.when($.ajax({
                        type: 'POST',
                        url: rutaFS + 'GS/Reuniones/VisorPlantillasReunion.aspx/Obtener_Datos_Plantilla',
                        data: JSON.stringify({ idPlantilla: idPlantilla }),
                        contentType: 'application/json;',
                        async: true
                    })).done(function (msg) {
                        infoPlantilla = $.parseJSON(msg.d);
                        DatosPlantilla();
                    });                    
                    break;
            }
        }
    }
}
function GetPlantilla(idPlantilla, nomPlantilla, idioma) {
    $.when($.ajax({
        type: 'POST',
        url: rutaFS + 'GS/Reuniones/VisorPlantillasReunion.aspx/GetPlantilla',
        data: JSON.stringify({ idPlantilla: idPlantilla, nomPlantilla: nomPlantilla }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: true
    })).done(function (msg) {
        var filename = msg.d;        
        downloadWindow = window.open(rutaFS + '_Common/downloadfile.aspx?e=1&f=' + filename, 'downloadAttach', 'width=1px,height=1px,top=5000,location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =no');
        downloadWindow.moveTo(-1000, -1000);        
        if (idioma && idioma !== '') {
            $('#divPlantillaIdioma_' + idioma + ' .template-download').attr('data-url', filename);
        }
    });
}
function EliminarPlantilla() {
    if (confirm(TextosPantalla[10])) {
        $.when($.ajax({
            type: 'POST',
            url: rutaFS + 'GS/Reuniones/VisorPlantillasReunion.aspx/Plantillas_Eliminar',
            data: JSON.stringify({ IdsPlantilla: SelectedRows }),
            contentType: 'application/json;',
            async: false
        })).done(function (msg) {
            RecargarGridPlantillas();
        });
    }
}
$('#divListaRestricUONs').live('click', function () {
    if ($('#divListaRestricUONs').children().length > 0) return false;
    if ($('#divUONsPlantilla').is(':visible')) {
        $('#divUONsPlantilla').hide();
    } else {
        $('#divUONsPlantilla').show();
        if ($('#tvUONsPlantillaTree').children().length == 0) {
            CargarPlantillaUONs()
        } else { CentrarPopUp($('#popupPlantilla')); }
    }
});
$('#divImgRestricUONs').live('click', function () {
    if ($('#divUONsPlantilla').is(':visible')) {
        $('#divUONsPlantilla').hide();
    } else {
        $('#divUONsPlantilla').show();
        if ($('#tvUONsPlantillaTree').children().length == 0) {
            CargarPlantillaUONs()
        }
    }
});
$('#divListaRestricUONs').live(($.browser.opera ? 'keypress' : 'keydown'), function (event) {    
    var code;
    if (event.keyCode) code = event.keyCode;
    else if (event.which) code = event.which;
    if (code == KEY.DEL || code == KEY.BACKSPACE) {
        if ($('.ItemParaSeleccionado').length > 0) {            
            $('.ItemParaSeleccionado').remove();
        }
    }
    return false;
});
function CargarPlantillaUONs() {
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'GS/Reuniones/VisorPlantillasReunion.aspx/Plantilla_UONs',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false
    })).done(function (msg) {
        var data = msg.d;
        $('#tvUONsPlantillaTree').tree({
            data: data,
            autoOpen: true,
            selectable: false
        });
        CentrarPopUp($('#popupPlantilla'));
    });
}
$('#tvUONsPlantillaTree .treeTipo3').live('click', function () {
    if ($('#divListaRestricUONs #' + $(this).attr("id").replace('tvUONsPlantillaTree', 'itemUONPlantilla')).length == 0) {
        $('#divListaRestricUONs').prepend('<div class="ItemPara" id="' + $(this).attr('id').replace('tvUONsPlantillaTree', 'itemUONPlantilla') + '">' + $(this).text() + '</div>');
    }
});
$('#txtUONsPlantilla').live(($.browser.opera ? 'keypress' : 'keydown'), function (event) {
    var code;
    if (event.keyCode) code = event.keyCode;
    else if (event.which) code = event.which;
    switch (true) {
        case (code == KEY.RETURN):
            var selectedItem = $('#tvUONsPlantillaTree span.treeSeleccionable.selected');
            if (selectedItem.length != 0) {
                if ($('#divListaRestricUONs #' + $(selectedItem).attr("id").replace('tvUONsPlantillaTree', 'itemUONPlantilla')).length == 0) {
                    $('#divListaRestricUONs').prepend('<div class="ItemPara" id="' + $(selectedItem).attr('id').replace('tvUONsPlantillaTree', 'itemUONPlantilla') + '">' + $(selectedItem).text() + '</div>');
                }
            }
            $('#txtUONsPlantilla').val('');
            $('#tvUONsPlantillaTree span.treeSeleccionable.selected').removeClass('selected');
            event.preventDefault();
            return false;
            break;
        case ((code >= 9 && code <= 45) || (code >= 91 && code <= 93) || (code >= 112 && code <= 186)):
            break;
        default:
            EncontrarItemTreeView('tvUONsPlantillaTree', 'txtUONsPlantilla', code);            
            break;
    };
});
$('.ItemPara').live('click', function () {
    $('.ItemParaSeleccionado').removeClass('ItemParaSeleccionado');
    $(this).addClass('ItemParaSeleccionado');
});
function ExportarExcel() { __doPostBack($('[id$=btnExportar]').attr('id'), 'Excel'); }
function ExportarPDF() { __doPostBack($('[id$=btnExportar]').attr('id'), 'PDF'); }
function FormatoExportacion(sender, event) {
    $('#popupFormatoDefectoPlantillas').css('right', $(window).outerWidth() - $(sender).position().left);
    $('#popupFormatoDefectoPlantillas').css('top', $(sender).position().top);
    $('#popupFormatoDefectoPlantillas').show();
    $('#popupFormatoDefectoPlantillas').css('width',$('#optFormatosDefectoPlantilla').outerWidth()+50);
}
function AsignarFormatoDefectoPlantillas() {    
    $.when($.ajax({
        type: 'POST',
        url: rutaFS + 'GS/Reuniones/VisorPlantillasReunion.aspx/Plantillas_FormatoDefecto_Plantillas',
        data: JSON.stringify({ IdsPlantilla: SelectedRows, formato: $('#optFormatosDefectoPlantilla').val() }),
        contentType: 'application/json;',
        async: false
    })).done(function (msg) {
        $('#popupFormatoDefectoPlantillas').hide();
        RecargarGridPlantillas();
    });
}
function RecargarGridPlantillas() {
    var btnRecargaPlantillas = $('[id$=btnRecargaPlantillas]').attr('id');
    __doPostBack(btnRecargaPlantillas, '');
}