﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/EnBlanco.Master" CodeBehind="VisorPlantillasReunion.aspx.vb" Inherits="Fullstep.FSNWeb.VisorPlantillasReunion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
    <style type="text/css">
        .template-download .name{
            cursor:pointer;
        }
    </style>
    <script id="divTabIdioma" type="text/x-jquery-tmpl">
		<div id='divTab_${Idioma}' title='${DenominacionIdioma}' style='float:left; margin-right:3px; padding-right:25px;' class='tabInfo{{if IdiomaDefecto}} tabInfoActive{{/if}}'>
            <img alt='${DenominacionIdioma}' src='${imgUrl}' />
        </div>
	</script>
    <script id="divPlantillaIdioma" type="text/x-jquery-tmpl">
        <div id="infoPanel_${Idioma}" class="infoPanel" style='clear:both; float:left; width:100%; margin-top:-1px;{{if !IdiomaDefecto}} display:none;{{/if}}'>                
            <div style="clear:both; float:left; width:100%; margin-top:10px;">
                <span id="lblPlantillaDenominacion_${Idioma}" class="Texto12" style="float:left; width:20%; margin-left:5px;"></span>
                <input type="text" id="txtPlantillaDenominacion_${Idioma}" style="width:75%; margin-left:5px;"/>
            </div>
            <div style="clear:both; float:left; width:100%; margin-top:10px;">
                <span id="lblPlantillaComentario_${Idioma}" class="Texto12" style="float:left; width:20%; margin-left:5px;"></span>
                <textarea name="txtPlantillaComentario" id="txtPlantillaComentario_${Idioma}" class="Texto12" rows="5" cols="50" style="width:75%; margin-left:5px;"></textarea>
            </div>
            <div id="divPlantillaDefecto_${Idioma}" class="botonAdjunto" style="position:relative; float:left; overflow:hidden; line-height:20px; margin:10px; padding:3px; cursor:pointer;">
                <img id="imgPlantillaDefecto_${Idioma}" alt="" class="Texto12" style="float:left;"/>
                <span id="lblPlantillaDefecto_${Idioma}" class="Texto12" style="float:left; margin-left:5px;"></span>
            </div>            
            <div id='divPlantillaIdioma_${Idioma}' style='clear:both; float:left; width:100%;'></div>
        </div>		
	</script>
    <script id="itemUONPlantilla" type="text/x-jquery-tmpl">
		<span id="${value}" class="Texto12" style="clear:both; float:left;">${text}</span>
	</script>
    <ig:WebExcelExporter runat="server" ID="wdgExcelExporter"></ig:WebExcelExporter>
    <ig:WebDocumentExporter runat="server" ID="wdgPDFExporter"></ig:WebDocumentExporter>
    <asp:Button runat="server" ID="btnExportar" style="display:none;"/>
    <div style="clear:both; float:left; width:100%;">
		<fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
	</div>
    <div id="divOpciones_1" style="clear:both; float:left; width:100%; margin-top:5px;">
		<div id="divCabeceraAcciones_1" dependantPanel="divContent_1" class="CollapsiblePanelHeader CollapsiblePanelHeaderCollapse" style="clear:both; float:left; width:98%; margin-left:5px;">
			<asp:Label runat="server" ID="lblCabeceraFSGS" CssClass="Texto12 Negrita" style="float:left; margin:5px;"></asp:Label>
		</div>        
		<div id="divContent_1" class="CollapsiblePanelContent" style="clear:both; float:left; width:98%; margin-left:5px;">
            <div id="tabCategorias" style="clear:both; float:left; margin-left:10px; margin-top:10px;" class="tabRedondeado">
                <span id="lbltabPlantillas" class="Texto14" style="line-height:15px;"></span>
            </div>
            <div id="divTabDetallePlantillas" class="Bordear" style="clear:both; float:left; width:98%; margin-left:10px; margin-bottom:10px;">
                <asp:Button runat="server" ID="btnRecargaPlantillas" style="display:none;" />                
                <asp:UpdatePanel runat="server" ID="updPlantillas" UpdateMode="Conditional">
			        <Triggers>
				        <asp:AsyncPostBackTrigger ControlID="btnRecargaPlantillas" EventName="Click" />
			        </Triggers>
			        <ContentTemplate>
                        <ig:WebHierarchicalDataGrid ID="whdgPlantillas" runat="server"
                            Width="100%" AutoGenerateColumns="false" AutoGenerateBands="false" CssClass="Texto12"
                            ShowHeader="true" InitialExpandDepth="0" InitialDataBindDepth="100" EnableAjax="false">                                                      
                            <ExpandCollapseAnimation SlideOpenDirection="Auto" SlideOpenDuration="300" 
                                SlideCloseDirection="Auto" SlideCloseDuration="300" />
                            <ClientEvents Click="whdgPlantillas_CellClick" />
                            <Behaviors>
                                <ig:Activation Enabled="true"/>
						        <ig:Filtering Alignment="Top" Enabled="false" Visibility="Visible" AnimationEnabled="false">
                                    <ColumnSettings>
                                        <ig:ColumnFilteringSetting ColumnKey="IMAGE" Enabled="false" />
                                        <ig:ColumnFilteringSetting ColumnKey="UONSDETALLE" Enabled="false" />
                                    </ColumnSettings>
                                </ig:Filtering>                 
						        <ig:RowSelectors Enabled="true" EnableInheritance="true">
							        <RowSelectorClientEvents RowSelectorClicked="whdgPlantillas_RowSelected" />                            
						        </ig:RowSelectors>
						        <ig:Selection Enabled="true" RowSelectType="Multiple" CellSelectType="None" ColumnSelectType="None"></ig:Selection>
						        <ig:Sorting Enabled="true" SortingMode="Multi"></ig:Sorting>
						        <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
						        <ig:ColumnMoving Enabled="false"></ig:ColumnMoving>
                                <ig:Paging Enabled="true" PagerAppearance="Top">
                                    <PagingClientEvents PageIndexChanged="whdgPlantillas_PageIndexChanged" />
							        <PagerTemplate>
								        <div class="CabeceraBotones" style="clear:both; float:left; width:100%;"> 
                                            <div style="clear:both; float:left;">
                                                <div onclick="AgregarPlantilla()" class="botonIzquierdaCabecera" style="float:left; line-height:20px; padding-bottom:2px; padding-top:3px;">
											        <asp:Label runat="server" ID="lblAgregar" CssClass="fntLogin2" style="margin-left:3px;"></asp:Label>           
										        </div>
										        <div onclick="EliminarPlantilla()" class="botonIzquierdaCabecera" style="float:left; line-height:20px; padding-bottom:2px; padding-top:3px;">
											        <asp:Label runat="server" ID="lblEliminar" CssClass="fntLogin2" style="margin-left:3px;"></asp:Label>           
										        </div>
                                            </div>
									        <div style="float:left; padding:3px 0px 2px 0px; margin-left:10px;">
										        <div style="clear: both; float: left; margin-right: 5px;">
                                                    <asp:Image ID="ImgBtnFirst" runat="server" OnClientClick="FirstPage()" style="margin-right:2px;" />
                                                    <asp:Image ID="ImgBtnPrev" runat="server" OnClientClick="PrevPage()" />
                                                </div>
                                                <div style="float: left; margin-right:5px;">
                                                    <asp:Label ID="lblPage" runat="server" Style="margin-right: 3px;"></asp:Label>
                                                    <asp:DropDownList ID="PagerPageList" runat="server" Style="margin-right: 3px;" onchange="return IndexChanged()" />
                                                    <asp:Label ID="lblOF" runat="server" Style="margin-right: 3px;"></asp:Label>
                                                    <asp:Label ID="lblCount" runat="server" />
                                                </div>
                                                <div style="float: left;">
                                                    <asp:Image ID="ImgBtnNext" runat="server" OnClientClick="NextPage()" style="margin-right:2px;" />
                                                    <asp:Image ID="ImgBtnLast" runat="server" OnClientClick="LastPage()" />
                                                </div>
									        </div>
									        <div style="float:right; margin:5px 5px 0px 0px;">
                                                <div onclick="FormatoExportacion(this,event)" class="botonDerechaCabecera" style="float:left;">
											        <asp:Label runat="server" ID="lblFormato" CssClass="fntLogin2" style="margin-left:3px;"></asp:Label>           
										        </div>
										        <div onclick="ExportarExcel()" class="botonDerechaCabecera" style="float:left;">            
											        <asp:Image runat="server" ID="imgExcel" CssClass="imagenCabecera" />
											        <asp:Label runat="server" ID="lblExcel" CssClass="fntLogin2" style="margin-left:3px;"></asp:Label>           
										        </div>
										        <div onclick="ExportarPDF()" class="botonDerechaCabecera" style="float:left;">            
											        <asp:Image runat="server" ID="imgPDF" CssClass="imagenCabecera" />
											        <asp:Label runat="server" ID="lblPDF" CssClass="fntLogin2" style="margin-left:3px;"></asp:Label>           
										        </div>
									        </div>
								        </div>						
							        </PagerTemplate>
						        </ig:Paging>
                            </Behaviors>
                            <Columns>                            
                                <ig:BoundDataField DataFieldName="ID" Key="ID" Hidden="true" />
                                <ig:BoundDataField DataFieldName="MENU" Key="MENU" Width="90px" Header-CssClass="headerNoWrap" CssClass="SinSalto" />
                                <ig:BoundDataField DataFieldName="DOCUMENTO" Key="DOCUMENTO" Width="90px" Header-CssClass="headerNoWrap" CssClass="SinSalto" />
                                <ig:BoundDataField DataFieldName="VISTA" Key="VISTA" Width="80px" Header-CssClass="headerNoWrap" CssClass="SinSalto" />
                                <ig:BoundDataField DataFieldName="DEN" Key="DEN" Width="100px" Header-CssClass="headerNoWrap" CssClass="SinSalto" />                        
                                <ig:BoundDataField DataFieldName="COMMENT" Key="COMMENT" Width="140px" Header-CssClass="headerNoWrap" CssClass="SinSalto" />
                                <ig:BoundDataField DataFieldName="NOM" Key="IMAGE" Width="20px" Header-CssClass="celdaImagenWebHierarchical headerNoWrap" CssClass="celdaImagenWebHierarchical itemSeleccionable SinSalto" />
                                <ig:BoundDataField DataFieldName="NOM" Key="NOM" Width="15%" Header-CssClass="headerNoWrap" CssClass="Link Subrayado SinSalto" />
                                <ig:BoundDataField DataFieldName="FORMATO" Key="FORMATO" Width="10%" Header-CssClass="headerNoWrap" CssClass="SinSalto" />
                                <ig:BoundDataField DataFieldName="DEFECTO" Key="DEFECTO" Width="55px" Header-CssClass="headerNoWrap" CssClass="SinSalto" />
                                <ig:BoundDataField DataFieldName="UONS" Key="UONS" Width="20%" Header-CssClass="celdaImagenWebHierarchical headerNoWrap" CssClass="celdaImagenWebHierarchical SinSalto" />
                                <ig:BoundDataField DataFieldName="UONS" Key="UONSDETALLE" Width="20px" Header-CssClass="headerNoWrap" CssClass="itemSeleccionable SinSalto" />
                            </Columns>
                        </ig:WebHierarchicalDataGrid>
                    </ContentTemplate>
		        </asp:UpdatePanel>
            </div>
        </div>
	</div>
    <div id="popupPlantilla" class="popupCN" style="display:none; position:absolute; z-index:1004; padding-bottom:10px;"> 
		<div id="divCabeceraPlantilla" class="tabInfoActive" style="clear:both; float:left; width:100%; top:0px;">			
			<span class="Texto16 TextoClaro Negrita" style="float:left; line-height:25px; margin-left:10px;"></span>
		</div>		
		<div id="divOpcionesPlantilla" style="position:relative; clear:both; float:left; width:98%; margin:10px 1%;">
			<div style="clear:both; float:left;">
                <div style="clear:both; float:left; height:30px;">
                    <span id="lblMenuPlantilla" class="Texto12 Negrita" style="clear:both; float:left; line-height:20px;"></span>
                    <select id="optMenuPlantilla" style="margin-left:5px;"></select>
                </div>
                <div style="clear:both; float:left; height:30px;">
                    <span id="lblDefectoPlantilla" class="Texto12 Negrita" style="clear:both; float:left; line-height:20px;"></span>
                    <input id="chkDefectoPlantilla" type="checkbox" style="margin-left:5px;" />
                </div>
            </div>
            <div style="float:left; height:30px; margin-left:5px;">
                <div style="clear:both; float:left; height:30px;">
                    <span id="lblDocumentoPlantilla" class="Texto12 Negrita" style="clear:both; float:left; line-height:20px;"></span>
                    <select id="optDocumentoPlantilla" style="margin-left:5px;"></select>
                </div>
                <div id="divVistaPlantilla" style="float:left; height:30px; display:none;">
                    <select id="optVistaPlantilla" style="margin-left:5px;"></select>
                </div>
                <div style="clear:both; height:30px;">
                    <span id="lblFormatoDefectoPlantilla" class="Texto12 Negrita" style="clear:both; float:left; line-height:20px;"></span>
                    <select id="optFormatoDefectoPlantilla" style="margin:0px 5px;"></select>
                </div>                
            </div>
            <div style="clear:both; float:left; width:100%;">
                <span id="lblRestricUONPlantilla" class="Texto12" style="clear:both; float:left;"></span>
            </div>
            <div style="clear:both; float:left; width:100%;">
                <div id="divListaRestricUONs" tabindex="0" class="divContenedor" style="clear:both; float:left; width:70%; margin-bottom:-1px;  margin-top:3px; min-height:20px; max-height:80px; overflow-y:auto;"></div>                                  
                <div id="divImgRestricUONs" class="Bordear ItemNuevoMensajePara" style="float:left; margin-top:3px; margin-left:-1px;"></div>
            </div> 
            <div id="divUONsPlantilla" class="Bordear ItemNuevoMensajeParaPanel" style="width:70%;">		            
			    <span id="lblUONsPlantilla" class="Texto12 Negrita" style="clear:both; float:left; margin-top:5px; margin-left:5px;"></span>		            
			    <input id="txtUONsPlantilla" class="CajaTexto" style="clear:both; float:left; width:90%; margin-left:5px; padding-left:5px;"/>		            
		        <div id="tvUONsPlantillaTree" style="clear:both; float:left; width:100%; margin-bottom:5px; max-height:200px; overflow-y:auto;"></div>
	        </div>            
		</div>
        <div id="divPlantillaDetalles" style="clear:both; float:left; position:relative; width:96%; margin-top:5px; margin-left:2%;">  
            <div id="divTabsIdiomaPlantilla" style="clear:both; float:left; position:relative; margin-top:5px;"></div>
            <div class="fileupload-content"><div id="tablaadjuntos" class="files"></div><div class="fileupload-progressbar"></div></div>
        </div>
        <div id="divBotonesPlantilla" style="position:relative; clear:both; float:left; width:100%; margin:15px 0px; text-align:center;">
			<div id="btnAceptarPlantilla" class="botonRedondeado" style="margin-right:5px;">
				<span id="lblAceptarPlantilla" style="line-height:23px;"></span>
			</div>
			<div id="btnCancelarPlantilla" class="botonRedondeado" style="margin-left:5px;">
				<span id="lblCancelarPlantilla" style="line-height:23px;"></span>
			</div>
		</div>
	</div>
    <div id="popupFormatoDefectoPlantillas" class="popupCN" style="display:none; position:absolute; z-index:1004; padding-bottom:10px;">
        <span id="lblFormatoDefectoPlantillas" class="Texto12" style="clear:both; float:left; margin:10px;">asdfsd</span>
        <select id="optFormatosDefectoPlantilla" style="clear:both; float:left; margin:10px;"></select>
        <div id="divBotonesFormatoDefecto" style="position:relative; clear:both; float:left; width:100%; margin:10px; text-align:center;">
			<div id="btnAceptarFormatoDefecto" class="botonRedondeado" style="margin-right:5px;">
				<span id="lblAceptarFormatoDefecto" style="line-height:23px;"></span>
			</div>
			<div id="btnCancelarFormatoDefecto" class="botonRedondeado" style="margin-left:5px;">
				<span id="lblCancelarFormatoDefecto" style="line-height:23px;"></span>
			</div>
		</div>
    </div>
</asp:Content>
