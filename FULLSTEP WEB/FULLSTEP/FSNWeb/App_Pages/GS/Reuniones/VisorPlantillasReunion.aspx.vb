﻿Imports System.Web.Script.Serialization
Imports Infragistics.Web.UI.GridControls
Imports System.IO
Imports Infragistics.Documents.Reports.Report

Public Class VisorPlantillasReunion
    Inherits FSNPage
    Private _pageNumber As Integer = 0
#Region "Cache"
    Private _dsPlantillas As DataSet
    Protected ReadOnly Property PlantillasVisor(Optional ByVal Recargar As Boolean = False) As DataSet
        Get
            If _dsPlantillas Is Nothing OrElse Recargar Then
                If IsPostBack AndAlso Not Recargar Then
                    _dsPlantillas = CType(Cache("dsPlantillasVisor" & FSNUser.Cod), DataSet)
                    If _dsPlantillas Is Nothing Then
                        Dim oPlantillas As FSNServer.Plantillas
                        oPlantillas = FSNServer.Get_Object(GetType(FSNServer.Plantillas))
                        _dsPlantillas = oPlantillas.GS_Plantillas_Visor(FSNUser.Idioma)

                        Me.InsertarEnCache("dsPlantillasVisor" & FSNUser.Cod, _dsPlantillas)
                    End If
                Else
                    Dim oPlantillas As FSNServer.Plantillas
                    oPlantillas = FSNServer.Get_Object(GetType(FSNServer.Plantillas))
                    _dsPlantillas = oPlantillas.GS_Plantillas_Visor(FSNUser.Idioma)

                    Me.InsertarEnCache("dsPlantillasVisor" & FSNUser.Cod, _dsPlantillas)
                End If
            End If
            Return _dsPlantillas
        End Get
    End Property
#End Region
#Region "Inicio"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/perfiles.png"
        If Not Page.IsPostBack Then
            whdgPlantillas.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize")
            whdgPlantillas.GridView.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize")

            CargarVisorPlantillas(True)
        Else
            Select Case Request("__EVENTTARGET")
                Case btnRecargaPlantillas.ClientID
                    CargarVisorPlantillas(True)
                Case btnExportar.ClientID
                    CargarVisorPlantillas()
                    Select Case Request("__EVENTARGUMENT")
                        Case "Excel"
                            ExportarExcel()
                        Case "PDF"
                            ExportarPDF()
                    End Select
                Case Else
                    CargarVisorPlantillas()
            End Select
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "desdeGS") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "desdeGS", "<script>var desdeGS=" & IIf(Request.QueryString("desdeGS") IsNot Nothing, "true", "false") & ";</script>")
        End If
        CargarRecursos()
        CType(whdgPlantillas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgExcel"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/Excel.png"
        CType(whdgPlantillas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgPDF"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/PDF.png"
    End Sub
    Private Sub CargarRecursos()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorPlantillasGS
        FSNPageHeader.TituloCabecera = Textos(0)

        With FSNPageHeader
            If .VisibleBotonGuardar Then .TextoBotonGuardar = Textos(1)
            If .VisibleBotonAgregarPerfil Then .TextoBotonAgregarPerfil = Textos(2)
            .TextoBotonCopiarPerfil = Textos(3)
            .TextoBotonCompararPerfil = Textos(4)
            .TextoBotonEliminarPerfil = Textos(5)
            .TextoBotonVolver = Textos(6)
        End With
        lblCabeceraFSGS.Text = Textos(35)
        With whdgPlantillas.GridView.Behaviors.Paging.PagerTemplateContainerTop
            CType(.FindControl("lblAgregar"), Label).Text = Textos(2)
            CType(.FindControl("lblEliminar"), Label).Text = Textos(3)
            CType(.FindControl("lblPage"), Label).Text = Textos(4)
            CType(.FindControl("lblOF"), Label).Text = Textos(5)
            CType(.FindControl("lblFormato"), Label).Text = Textos(6)
            CType(.FindControl("lblExcel"), Label).Text = Textos(7)
            CType(.FindControl("lblPDF"), Label).Text = Textos(8)
        End With
        For Each column As BoundDataField In whdgPlantillas.GridView.Columns
            Select Case column.Key
                Case "MENU"
                    column.Header.Text = Textos(9)
                Case "DOCUMENTO"
                    column.Header.Text = Textos(10)
                Case "VISTA"
                    column.Header.Text = Textos(11)
                Case "DEN"
                    column.Header.Text = Textos(12)
                Case "COMMENT"
                    column.Header.Text = Textos(13)
                Case "NOM"
                    column.Header.Text = Textos(14)
                Case "FORMATO"
                    column.Header.Text = Textos(15)
                Case "DEFECTO"
                    column.Header.Text = Textos(16)
                Case "UONS"
                    column.Header.Text = Textos(17)
                Case Else
                    column.Header.Text = ""
            End Select
        Next
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPantalla") Then
            Dim sVariableJavascriptTextosPantalla As String = "var TextosPantalla = new Array();"
            For i As Integer = 0 To 6
                sVariableJavascriptTextosPantalla &= "TextosPantalla[" & i & "]='" & JSText(Textos(i + 23)) & IIf(i = 1 OrElse i = 2, ":", "") & "';"
            Next
            sVariableJavascriptTextosPantalla &= "TextosPantalla[7]='" & JSText(Textos(9)) & ":';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[8]='" & JSText(Textos(10)) & ":';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[9]='" & JSText(Textos(1)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[10]='" & JSText(Textos(36)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[11]='" & JSText(Textos(37)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[12]='" & JSText(Textos(12)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[13]='" & JSText(Textos(13)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[14]='" & JSText(Textos(27)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[15]='" & JSText(Textos(40)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[16]='" & JSText(Textos(41)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[17]='" & JSText(Textos(42)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[18]='" & JSText(Textos(43)) & "';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextosPantalla, True)
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "optData") Then
            'Cargamos en una variable json las opciones de los combos de Alta plantilla
            Dim oPlantillas As FSNServer.Plantillas = FSNServer.Get_Object(GetType(FSNServer.Plantillas))
            Dim oPlantilla As FSNServer.InfoPlantilla = oPlantillas.GS_Visor_Plantillas_DetallePlantilla(0, FSNUser.Idioma)
            Dim serializer As New JavaScriptSerializer()
            Me.InsertarEnCache("oPlantilla_" & FSNUser.Cod, oPlantilla)
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "optData", "var optData=" & serializer.Serialize(DirectCast(oPlantilla, Object)).ToString() & ";", True)
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "dataFormatos") Then
            'Cargamos en una variable json las opciones de los combos de Alta plantilla            
            Dim serializer As New JavaScriptSerializer()
            Dim formatosPlantilla As New List(Of FSNServer.cn_fsItem)
            Dim formato As New FSNServer.cn_fsItem
            formato.value = FormatoPlantillaGS.DOC
            formato.text = Textos(30)
            formatosPlantilla.Add(formato)
            formato = New FSNServer.cn_fsItem
            formato.value = FormatoPlantillaGS.DOCX
            formato.text = Textos(31)
            formatosPlantilla.Add(formato)
            formato = New FSNServer.cn_fsItem
            formato.value = FormatoPlantillaGS.PDF
            formato.text = Textos(32)
            formatosPlantilla.Add(formato)
            formato = New FSNServer.cn_fsItem
            formato.value = FormatoPlantillaGS.ODT
            formato.text = Textos(33)
            formatosPlantilla.Add(formato)
            formato = New FSNServer.cn_fsItem
            formato.value = FormatoPlantillaGS.EPUB
            formato.text = Textos(34)
            formatosPlantilla.Add(formato)
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "dataFormatos", "var dataFormatos=" & serializer.Serialize(DirectCast(formatosPlantilla, Object)).ToString() & ";", True)
        End If
    End Sub
    ''' <summary>
    ''' Refleja en el control paginador los ultimos cambios en los certificados a mostrar
    ''' </summary>
    ''' <remarks>Llamada desde:whgCertificados_DataBound; Tiempo maximo: 0,2 </remarks>
    Private Sub Paginador()
        Dim pagerList As DropDownList = DirectCast(whdgPlantillas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("PagerPageList"), DropDownList)
        pagerList.Items.Clear()
        For i As Integer = 1 To whdgPlantillas.GridView.Behaviors.Paging.PageCount
            pagerList.Items.Add(i.ToString())
        Next
        pagerList.SelectedIndex = whdgPlantillas.GridView.Behaviors.Paging.PageIndex
        CType(whdgPlantillas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblCount"), Label).Text = whdgPlantillas.GridView.Behaviors.Paging.PageCount

        Dim desactivado As Boolean = ((whdgPlantillas.GridView.Behaviors.Paging.PageCount = 1) OrElse (whdgPlantillas.GridView.Behaviors.Paging.PageIndex = 0))
        With CType(whdgPlantillas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero" & IIf(desactivado, "_desactivado", "") & ".gif"
            .Enabled = False
        End With
        With CType(whdgPlantillas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior" & IIf(desactivado, "_desactivado", "") & ".gif"
            .Enabled = False
        End With
        desactivado = (whdgPlantillas.GridView.Behaviors.Paging.PageCount = 1)
        With CType(whdgPlantillas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(desactivado, "_desactivado", "") & ".gif"
            .Enabled = Not desactivado
        End With
        With CType(whdgPlantillas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(desactivado, "_desactivado", "") & ".gif"
            .Enabled = Not desactivado
        End With
    End Sub
    Private Sub CargarVisorPlantillas(Optional ByVal Recargar As Boolean = False)
        With whdgPlantillas
            .GridView.ClearDataSource()
            .DataSource = PlantillasVisor(Recargar)
            .GridView.DataSource = .DataSource
            .DataMember = "PLANTILLAS"
            .DataKeyFields = "ID"
            updPlantillas.Update()
        End With
    End Sub
#End Region
#Region "Grid"
    Private Sub whdgPlantillas_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgPlantillas.InitializeRow
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorPlantillasGS
        e.Row.Height = Unit.Pixel(25)
        Dim gridCellIndex As Integer
        Dim items As Infragistics.Web.UI.GridControls.GridRecordItemCollection = e.Row.Items

        If whdgPlantillas.GridView.Columns.Item("MENU") IsNot Nothing Then
            gridCellIndex = whdgPlantillas.GridView.Columns.FromKey("MENU").Index
            Select Case CType(e.Row.DataItem.Item.Row.Item("MENU"), Integer)
                Case MenuPlantillaGS.Reuniones
                    items(gridCellIndex).Text = Textos(18)
                Case Else
                    items(gridCellIndex).Text = ""
            End Select
        End If
        If whdgPlantillas.GridView.Columns.Item("DOCUMENTO") IsNot Nothing Then
            gridCellIndex = whdgPlantillas.GridView.Columns.FromKey("DOCUMENTO").Index
            Select Case CType(e.Row.DataItem.Item.Row.Item("DOCUMENTO"), Integer)
                Case DocumentoPlantillaGS.Agenda
                    items(gridCellIndex).Text = Textos(19)
                Case DocumentoPlantillaGS.Acta
                    items(gridCellIndex).Text = Textos(20)
                Case Else
                    items(gridCellIndex).Text = ""
            End Select
        End If
        If whdgPlantillas.GridView.Columns.Item("VISTA") IsNot Nothing Then
            gridCellIndex = whdgPlantillas.GridView.Columns.FromKey("VISTA").Index
            If IsDBNull(e.Row.DataItem.Item.Row.Item("VISTA")) Then
                items(gridCellIndex).Text = ""
            Else
                Select Case CType(e.Row.DataItem.Item.Row.Item("VISTA"), Integer)
                    Case VistaPlantillaGS.ActaProveedor
                        items(gridCellIndex).Text = Textos(21)
                    Case VistaPlantillaGS.ActaItem
                        items(gridCellIndex).Text = Textos(22)
                    Case Else
                        items(gridCellIndex).Text = ""
                End Select
            End If
        End If
        If whdgPlantillas.GridView.Columns.Item("IMAGE") IsNot Nothing Then
            gridCellIndex = whdgPlantillas.GridView.Columns.FromKey("IMAGE").Index
            Select Case Split(e.Row.DataItem.Item.Row.Item("NOM"), ".")(Split(e.Row.DataItem.Item.Row.Item("NOM"), ".").Length - 1)
                Case "doc", "docx"
                    items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "Images/wordAttach.png' style='max-width:15px; max-height:15px;'/>"
                Case "pdf"
                    items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "Images/pdfAttach.png' style='max-width:15px; max-height:15px;'/>"
                Case "odt"
                    items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "Images/odtAttach.png' style='max-width:15px; max-height:15px;'/>"
                Case "epub"
                    items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "Images/epubAttach.png' style='max-width:15px; max-height:15px;'/>"
                Case Else
                    items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "Images/Attach.png' style='max-width:15px; max-height:15px;'/>"
            End Select
        End If
        If whdgPlantillas.GridView.Columns.Item("FORMATO") IsNot Nothing Then
            gridCellIndex = whdgPlantillas.GridView.Columns.FromKey("FORMATO").Index
            If IsDBNull(e.Row.DataItem.Item.Row.Item("FORMATO")) Then
                items(gridCellIndex).Text = ""
            Else
                Select Case CType(e.Row.DataItem.Item.Row.Item("FORMATO"), Integer)
                    Case FormatoPlantillaGS.DOC
                        items(gridCellIndex).Text = Textos(30)
                    Case FormatoPlantillaGS.DOCX
                        items(gridCellIndex).Text = Textos(31)
                    Case FormatoPlantillaGS.PDF
                        items(gridCellIndex).Text = Textos(32)
                    Case FormatoPlantillaGS.ODT
                        items(gridCellIndex).Text = Textos(33)
                    Case FormatoPlantillaGS.EPUB
                        items(gridCellIndex).Text = Textos(34)
                    Case Else
                        items(gridCellIndex).Text = ""
                End Select
            End If
        End If
        If whdgPlantillas.GridView.Columns.Item("DEFECTO") IsNot Nothing Then
            gridCellIndex = whdgPlantillas.GridView.Columns.FromKey("DEFECTO").Index
            If IsDBNull(e.Row.DataItem.Item.Row.Item("DEFECTO")) Then
                items(gridCellIndex).Text = ""
            Else
                If CType(e.Row.DataItem.Item.Row.Item("DEFECTO"), Boolean) Then
                    items(gridCellIndex).CssClass = "itemCenterAligment"
                    items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "Images/aprobado.gif'/>"
                Else
                    items(gridCellIndex).Text = ""
                End If
            End If
        End If
        If whdgPlantillas.GridView.Columns.Item("UONS") IsNot Nothing Then
            gridCellIndex = whdgPlantillas.GridView.Columns.FromKey("UONS").Index
            Dim html As String
            If Not e.Row.DataItem.Item.Row.Item("UONS") = String.Empty Then
                html = "<div id='divUONsPlantillaAbrev_" & e.Row.DataItem.Item.Row.Item("ID") & "'>" & e.Row.DataItem.Item.Row.Item("UONS") & "</div>" & _
                    "<div id='divUONsPlantillaDetalle_" & e.Row.DataItem.Item.Row.Item("ID") & "' style='display:none;'>"
                For Each uon As DataRow In CType(HttpContext.Current.Cache("dsPlantillasVisor" & FSNUser.Cod), DataSet).Tables("UONS").Select("ID=" & e.Row.DataItem.Item.Row.Item("ID"))
                    html &= "<span style='clear:both; float:left;'>" & IIf(IsDBNull(uon("UON1")), "", uon("UON1") & "-") & _
                            IIf(IsDBNull(uon("UON2")), "", uon("UON2") & "-") & _
                            IIf(IsDBNull(uon("UON3")), "", uon("UON3") & "-") & uon("DEN").ToString & "</span>"
                Next
                items(gridCellIndex).Text = html & "</div>"
            End If
        End If
        If whdgPlantillas.GridView.Columns.Item("UONSDETALLE") IsNot Nothing Then
            gridCellIndex = whdgPlantillas.GridView.Columns.FromKey("UONSDETALLE").Index
            If Not e.Row.DataItem.Item.Row.Item("UONS") = String.Empty Then
                items(gridCellIndex).Text = "<img id='imgDetalleUONsPlantilla_" & e.Row.DataItem.Item.Row.Item("ID") & "'" & _
                    "src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "Images/3puntos.gif'" & _
                    "style='max-width:15px; max-height:15px;" & IIf(CType(HttpContext.Current.Cache("dsPlantillasVisor" & FSNUser.Cod), DataSet).Tables("UONS").Select("ID=" & e.Row.DataItem.Item.Row.Item("ID")).Length < 2, "display:none;", "") & "'/>"
            End If
        End If
    End Sub
    Private Sub whdgPlantillas_DataBound(sender As Object, e As System.EventArgs) Handles whdgPlantillas.DataBound
        Paginador()
    End Sub
#End Region
#Region "Exportacion"
    Private Sub ExportarExcel()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorPlantillasGS
        Dim fileName As String = Textos(1)

        whdgPlantillas.Columns.FromKey("IMAGE").Hidden = True
        whdgPlantillas.Columns.FromKey("UONSDETALLE").Hidden = True
        whdgPlantillas.GridView.Columns.FromKey("IMAGE").Hidden = True
        whdgPlantillas.GridView.Columns.FromKey("UONSDETALLE").Hidden = True
        With wdgExcelExporter
            .DownloadName = fileName
            .DataExportMode = DataExportMode.DataInGridOnly

            .Export(whdgPlantillas)
        End With
        whdgPlantillas.Columns.FromKey("IMAGE").Hidden = False
        whdgPlantillas.Columns.FromKey("UONSDETALLE").Hidden = False
        whdgPlantillas.GridView.Columns.FromKey("IMAGE").Hidden = False
        whdgPlantillas.GridView.Columns.FromKey("UONSDETALLE").Hidden = False
    End Sub
    Private Sub wdgExcelExporter_Exported(sender As Object, e As Infragistics.Web.UI.GridControls.ExcelExportedEventArgs) Handles wdgExcelExporter.Exported
        e.Worksheet.Name = Textos(1)
    End Sub
    Private Sub wdgExcelExporter_GridRecordItemExported(sender As Object, e As Infragistics.Web.UI.GridControls.GridRecordItemExportedEventArgs) Handles wdgExcelExporter.GridRecordItemExported
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorPlantillasGS
        Select Case e.GridCell.Column.Key
            Case "MENU"
                e.WorksheetCell.Value = Textos(18)
            Case "DOCUMENTO"
                Select Case DBNullToInteger(e.GridCell.Value)
                    Case DocumentoPlantillaGS.Agenda
                        e.WorksheetCell.Value = Textos(19)
                    Case DocumentoPlantillaGS.Acta
                        e.WorksheetCell.Value = Textos(20)
                    Case Else
                        e.WorksheetCell.Value = ""
                End Select
            Case "VISTA"
                Select Case DBNullToInteger(e.GridCell.Value)
                    Case VistaPlantillaGS.ActaProveedor
                        e.WorksheetCell.Value = Textos(21)
                    Case VistaPlantillaGS.ActaItem
                        e.WorksheetCell.Value = Textos(22)
                    Case Else
                        e.WorksheetCell.Value = ""
                End Select
            Case "FORMATO"
                Select Case DBNullToInteger(e.GridCell.Value)
                    Case FormatoPlantillaGS.DOC
                        e.WorksheetCell.Value = Textos(30)
                    Case FormatoPlantillaGS.DOCX
                        e.WorksheetCell.Value = Textos(31)
                    Case FormatoPlantillaGS.PDF
                        e.WorksheetCell.Value = Textos(32)
                    Case FormatoPlantillaGS.ODT
                        e.WorksheetCell.Value = Textos(33)
                    Case FormatoPlantillaGS.EPUB
                        e.WorksheetCell.Value = Textos(34)
                    Case Else
                        e.WorksheetCell.Value = ""
                End Select
            Case "DEFECTO"
                e.WorksheetCell.Value = IIf(Not IsDBNull(e.GridCell.Value) AndAlso e.GridCell.Value, Textos(38), Textos(39))
        End Select
    End Sub
    Private Sub ExportarPDF()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorPlantillasGS
        Dim fileName As String = Textos(1)
        wdgPDFExporter.DownloadName = fileName

        whdgPlantillas.Columns.FromKey("IMAGE").Hidden = True
        whdgPlantillas.Columns.FromKey("UONSDETALLE").Hidden = True
        whdgPlantillas.GridView.Columns.FromKey("IMAGE").Hidden = True
        whdgPlantillas.GridView.Columns.FromKey("UONSDETALLE").Hidden = True
        With wdgPDFExporter
            .EnableStylesExport = True
            .DataExportMode = DataExportMode.DataInGridOnly
            .TargetPaperOrientation = PageOrientation.Landscape
            .Margins = PageMargins.GetPageMargins("Narrow")
            .TargetPaperSize = PageSizes.GetPageSize("A4")

            .Export(whdgPlantillas)
        End With
        whdgPlantillas.Columns.FromKey("IMAGE").Hidden = False
        whdgPlantillas.Columns.FromKey("UONSDETALLE").Hidden = False
        whdgPlantillas.GridView.Columns.FromKey("IMAGE").Hidden = False
        whdgPlantillas.GridView.Columns.FromKey("UONSDETALLE").Hidden = False
    End Sub
    Private Sub wdgPDFExporter_GridRecordItemExporting(sender As Object, e As Infragistics.Web.UI.GridControls.DocumentGridRecordItemExportingEventArgs) Handles wdgPDFExporter.GridRecordItemExporting
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorPlantillasGS
        Select Case e.GridCell.Column.Key
            Case "MENU"
                e.ExportValue = Textos(9)
            Case "DOCUMENTO"
                Select Case DBNullToInteger(e.GridCell.Value)
                    Case DocumentoPlantillaGS.Agenda
                        e.ExportValue = Textos(19)
                    Case DocumentoPlantillaGS.Acta
                        e.ExportValue = Textos(20)
                    Case Else
                        e.ExportValue = ""
                End Select
            Case "VISTA"
                Select Case DBNullToInteger(e.GridCell.Value)
                    Case VistaPlantillaGS.ActaProveedor
                        e.ExportValue = Textos(21)
                    Case VistaPlantillaGS.ActaItem
                        e.ExportValue = Textos(22)
                    Case Else
                        e.ExportValue = ""
                End Select
            Case "FORMATO"
                Select Case DBNullToInteger(e.GridCell.Value)
                    Case FormatoPlantillaGS.DOC
                        e.ExportValue = Textos(30)
                    Case FormatoPlantillaGS.DOCX
                        e.ExportValue = Textos(31)
                    Case FormatoPlantillaGS.PDF
                        e.ExportValue = Textos(32)
                    Case FormatoPlantillaGS.ODT
                        e.ExportValue = Textos(33)
                    Case FormatoPlantillaGS.EPUB
                        e.ExportValue = Textos(34)
                    Case Else
                        e.ExportValue = ""
                End Select
            Case "DEFECTO"
                e.ExportValue = IIf(Not IsDBNull(e.GridCell.Value) AndAlso e.GridCell.Value, Textos(38), Textos(39))
            Case "UONS"
                e.GridCell.Text = e.GridCell.Value
        End Select
    End Sub
#End Region
#Region "Page Methods"
    ''' <summary>
    ''' Mandamos los datos guardados en cache en formato jSon, y seleccionaremos y cargaremos los datos en la parte de cliente
    ''' Lo serializamos en .net y lo pasamos en formato string para luego parsearlo en cliente ya que algunos tipos de objeto no
    ''' se serializan bien, por ejemplo los dictionary
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function Obtener_Datos_Plantilla(ByVal idPlantilla As Integer) As Object
        Try
            Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim serializer As New JavaScriptSerializer()
            Dim oPlantilla As FSNServer.InfoPlantilla
            If idPlantilla = 0 Then
                oPlantilla = TryCast(HttpContext.Current.Cache("oPlantilla_" & User.Cod), FSNServer.InfoPlantilla)
            Else
                Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
                Dim oPlantillas As FSNServer.Plantillas = FSNServer.Get_Object(GetType(FSNServer.Plantillas))
                oPlantilla = oPlantillas.GS_Visor_Plantillas_DetallePlantilla(idPlantilla, User.Idioma)
                For Each file As KeyValuePair(Of String, FSNServer.Plantilla) In oPlantilla.languageInfo
                    If (CType(file.Value, FSNServer.Plantilla).size >= 1000000000) Then
                        CType(file.Value, FSNServer.Plantilla).filesize = modUtilidades.FormatNumber(CType(file.Value, FSNServer.Plantilla).size / 1000000000, User.NumberFormat)
                    ElseIf CType(file.Value, FSNServer.Plantilla).size >= 1000000 Then
                        CType(file.Value, FSNServer.Plantilla).filesize = modUtilidades.FormatNumber(CType(file.Value, FSNServer.Plantilla).size / 1000000, User.NumberFormat)
                    Else
                        CType(file.Value, FSNServer.Plantilla).filesize = modUtilidades.FormatNumber(CType(file.Value, FSNServer.Plantilla).size / 1000, User.NumberFormat)
                    End If
                Next
            End If
            Return serializer.Serialize(DirectCast(oPlantilla, Object)).ToString
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Sub Plantilla_Guardar(ByVal Plantilla As Object, ByVal UONsPlantilla As Object)
        Try
            Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim serializer As New JavaScriptSerializer()
            Dim oInfoPlantilla As FSNServer.InfoPlantilla
            oInfoPlantilla = serializer.Deserialize(Of FSNServer.InfoPlantilla)(serializer.Serialize(DirectCast(Plantilla, Object)).ToString)

            Dim oPlantillas As FSNServer.Plantillas = FSNServer.Get_Object(GetType(FSNServer.Plantillas))
            oInfoPlantilla.Per = User.CodPersona
            Dim dtUONsPlantilla As New DataTable
            dtUONsPlantilla.Columns.Add("ID", GetType(System.Int64))
            dtUONsPlantilla.Columns.Add("UON1", GetType(System.String))
            dtUONsPlantilla.Columns.Add("UON2", GetType(System.String))
            dtUONsPlantilla.Columns.Add("UON3", GetType(System.String))
            Dim uonPlantilla As DataRow
            For Each uon As Object In UONsPlantilla
                uonPlantilla = dtUONsPlantilla.NewRow
                uonPlantilla("ID") = oInfoPlantilla.Id
                uonPlantilla("UON1") = uon("UON1")
                uonPlantilla("UON2") = uon("UON2")
                uonPlantilla("UON3") = uon("UON3")
                dtUONsPlantilla.Rows.Add(uonPlantilla)
            Next
            If oInfoPlantilla.Id = 0 Then
                oPlantillas.GS_Plantilla_Alta(oInfoPlantilla, dtUONsPlantilla)
            Else
                oPlantillas.GS_Plantilla_Editar(oInfoPlantilla, dtUONsPlantilla)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function GetPlantilla(ByVal idPlantilla As Integer, ByVal nomPlantilla As String) As String
        Try
            Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim byteBuffer() As Byte
            Dim oPlantillas As FSNServer.Plantillas = FSNServer.Get_Object(GetType(FSNServer.Plantillas))
            byteBuffer = oPlantillas.GS_Plantillas_GetAdjuntoPlantilla(idPlantilla, User.Idioma)
            Dim RandomDirectory As String = modUtilidades.GenerateRandomPath()
            While IO.Directory.Exists(ConfigurationManager.AppSettings("temp") & "\" & RandomDirectory)
                RandomDirectory = modUtilidades.GenerateRandomPath()
            End While
            IO.Directory.CreateDirectory(ConfigurationManager.AppSettings("temp") & "\" & RandomDirectory)
            Dim temporal As String
            temporal = ConfigurationManager.AppSettings("temp") & "\" & RandomDirectory & "\" & nomPlantilla
            Dim oFS As New FileStream(temporal, FileMode.Append, FileAccess.Write)
            oFS.Write(byteBuffer, 0, byteBuffer.Length)
            oFS.Close()
            Return temporal
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Sub Plantillas_Eliminar(ByVal IdsPlantilla As Object)
        Try
            Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim dtIdsPlantilla As New DataTable
            Dim rIdPlantilla As DataRow
            dtIdsPlantilla.Columns.Add("ID", GetType(System.Int32))
            For Each item As Object In IdsPlantilla
                rIdPlantilla = dtIdsPlantilla.NewRow
                rIdPlantilla("ID") = item("Id")
                dtIdsPlantilla.Rows.Add(rIdPlantilla)
            Next
            Dim oPlantillas As FSNServer.Plantillas = FSNServer.Get_Object(GetType(FSNServer.Plantillas))
            oPlantillas.GS_Plantilla_Eliminar(dtIdsPlantilla)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Sub Plantilla_Defecto(ByVal idPlantilla As Integer, ByVal documento As Integer, ByVal vista As Integer)
        Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oPlantillas As FSNServer.Plantillas = FSNServer.Get_Object(GetType(FSNServer.Plantillas))
        oPlantillas.GS_Plantilla_Defecto(idPlantilla)
    End Sub
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function Plantilla_UONs() As List(Of FSNServer.cn_fsTreeViewItem)
        Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oPlantillas As FSNServer.Plantillas = FSNServer.Get_Object(GetType(FSNServer.Plantillas))
        Dim oUONs As New List(Of FSNServer.cn_fsTreeViewItem)

        oUONs = oPlantillas.GS_Obtener_UONs(User.Idioma)
        Return oUONs
    End Function
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function Detalle_UONs_Plantilla(ByVal idPlantilla As Integer) As List(Of FSNServer.cn_fsItem)
        Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim uonsPlantilla As New List(Of FSNServer.cn_fsItem)
        Dim uonPlantilla As FSNServer.cn_fsItem
        For Each uon As DataRow In CType(HttpContext.Current.Cache("dsPlantillasVisor" & User.Cod), DataSet).Tables("UONS").Select("ID=" & idPlantilla)
            uonPlantilla = New FSNServer.cn_fsItem
            uonPlantilla.text = "itemUONPlantilla" & _
                            IIf(IsDBNull(uon("UON1")), "", "-UON1_" & uon("UON1")) & _
                            IIf(IsDBNull(uon("UON2")), "", "-UON2_" & uon("UON2")) & _
                            IIf(IsDBNull(uon("UON3")), "", "-UON3_" & uon("UON3"))
            uonPlantilla.value = IIf(IsDBNull(uon("UON1")), "", uon("UON1") & "-") & _
                            IIf(IsDBNull(uon("UON2")), "", uon("UON2") & "-") & _
                            IIf(IsDBNull(uon("UON3")), "", uon("UON3") & "-") & uon("DEN").ToString
            uonsPlantilla.Add(uonPlantilla)
        Next
        Return uonsPlantilla
    End Function
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Sub Plantillas_FormatoDefecto_Plantillas(ByVal IdsPlantilla As Object, ByVal formato As Integer)
        Try
            Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim dtIdsPlantilla As New DataTable
            Dim rIdPlantilla As DataRow
            dtIdsPlantilla.Columns.Add("ID", GetType(System.Int32))
            For Each item As Object In IdsPlantilla
                rIdPlantilla = dtIdsPlantilla.NewRow
                rIdPlantilla("ID") = item("Id")
                dtIdsPlantilla.Rows.Add(rIdPlantilla)
            Next
            If Not dtIdsPlantilla.Rows.Count = 0 Then
                Dim oPlantillas As FSNServer.Plantillas = FSNServer.Get_Object(GetType(FSNServer.Plantillas))
                oPlantillas.GS_Plantilla_FormatoDefecto(dtIdsPlantilla, formato)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
End Class