﻿Imports Aspose.Words
Imports System.IO
Imports Aspose.Words.Reporting
Imports Aspose.Words.Tables
Imports Aspose.Words.Saving

Public Class actaExport
    Inherits FSNPage
    Shared i As New Integer

    ''' <summary>
    ''' Descarga el acta
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>  
    ''' <remarks>Llamada desde: Menu.master.vb   Reuniones\SelectorPlantilla.aspx; Tiempo máximo: 0,2</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dataDir As String = ConfigurationManager.AppSettings("temp")
        If Not IsPostBack Then
            Dim fecha As DateTime
            Dim IdPlantilla, Formato As Integer
            With Request
                fecha = New DateTime(.QueryString("a"), .QueryString("m"), .QueryString("d"), .QueryString("h"), .QueryString("mn"), .QueryString("s"))
                IdPlantilla = CType(.QueryString("IdPlantilla"), Integer)
                Formato = CType(.QueryString("Formato"), Integer)
            End With
            ' Sample infrastructure.
            Dim dsDatosActa As DataSet
            Dim oReunionActa As FSNServer.Reuniones
            oReunionActa = FSNServer.Get_Object(GetType(FSNServer.Reuniones))
            Dim proveedoresNoAdjudicados As Boolean = True
            'OBTENER PLANTILLA DE BASE DE DATOS
            Dim oPlantillas As FSNServer.Plantillas = FSNServer.Get_Object(GetType(FSNServer.Plantillas))
            Dim oPlantilla As FSNServer.InfoPlantilla
            oPlantilla = oPlantillas.GS_Visor_Plantillas_DetallePlantilla(IdPlantilla, FSNUser.Idioma)
            Dim byteBufferPlantilla() As Byte
            byteBufferPlantilla = oPlantillas.GS_Plantillas_GetAdjuntoPlantilla(IdPlantilla, FSNUser.Idioma)
            Dim RandomDirectory As String = modUtilidades.GenerateRandomPath()
            While IO.Directory.Exists(dataDir & "\" & RandomDirectory)
                RandomDirectory = modUtilidades.GenerateRandomPath()
            End While
            IO.Directory.CreateDirectory(dataDir & "\" & RandomDirectory)
            Dim temporal As String
            temporal = dataDir & "\" & RandomDirectory & "\" & oPlantilla.languageInfo(FSNUser.Idioma).NombreArchivo
            Dim oFS As New FileStream(temporal, FileMode.Append, FileAccess.Write)
            oFS.Write(byteBufferPlantilla, 0, byteBufferPlantilla.Length)
            oFS.Close()
            ' Open the template document.
            Dim doc As Document
            doc = New Document(temporal)
            Select Case oPlantilla.Vista
                Case VistaPlantillaGS.ActaProveedor
                    dsDatosActa = oReunionActa.Reunion_Acta_AdjudicacionesProveedorPorItem(fecha, FSNUser.Idioma, proveedoresNoAdjudicados)
                    doc.MailMerge.FieldMergingCallback = New HandleMergeFieldAlternatingRows()
                    ' Certain regions can be skipped from applying logic to by not adding the table name inside the CreateEmptyDataSource method.
                    ' Enable this cleanup option so any regions which are not handled by the user's logic are removed automatically.
                    doc.MailMerge.CleanupOptions = MailMergeCleanupOptions.RemoveUnusedRegions
                    ' Execute the nested mail merge with regions
                    doc.MailMerge.ExecuteWithRegions(dsDatosActa)
                    Dim bmNode As Node
                    For j As Integer = 1 To i
                        If doc.Range.Bookmarks("DELETE_COL_" & j) IsNot Nothing Then
                            bmNode = doc.Range.Bookmarks("DELETE_COL_" & j).BookmarkStart
                            If DirectCast(bmNode.ParentNode, Paragraph).ParentNode.NodeType = NodeType.Cell Then
                                'Get cell with bookmark
                                Dim cell As Cell = DirectCast(DirectCast(bmNode.ParentNode, Paragraph).ParentNode, Cell)
                                cell.ParentRow.Remove()
                            End If
                        End If
                    Next
                Case VistaPlantillaGS.ActaItem
                    dsDatosActa = oReunionActa.Reunion_Acta_ItemsPorProveedorAdjudicado(fecha, FSNUser.Idioma, proveedoresNoAdjudicados)
                    doc.MailMerge.FieldMergingCallback = New HandleMergeFieldAlternatingRows()
                    ' Certain regions can be skipped from applying logic to by not adding the table name inside the CreateEmptyDataSource method.
                    ' Enable this cleanup option so any regions which are not handled by the user's logic are removed automatically.
                    doc.MailMerge.CleanupOptions = MailMergeCleanupOptions.RemoveUnusedRegions
                    ' Execute the nested mail merge with regions
                    doc.MailMerge.ExecuteWithRegions(dsDatosActa)
                    Dim bmNode As Node
                    For j As Integer = 1 To i
                        If doc.Range.Bookmarks("DELETE_COL_" & j) IsNot Nothing Then
                            bmNode = doc.Range.Bookmarks("DELETE_COL_" & j).BookmarkStart
                            If DirectCast(bmNode.ParentNode, Paragraph).ParentNode.NodeType = NodeType.Cell Then
                                'Get cell with bookmark
                                Dim cell As Cell = DirectCast(DirectCast(bmNode.ParentNode, Paragraph).ParentNode, Cell)
                                'Get index of cell
                                Dim cellIndex As Integer = cell.ParentRow.IndexOf(cell)
                                'Remove all cells with cellIndex
                                For Each row As Row In cell.ParentRow.ParentTable
                                    row.Cells(cellIndex).Remove()
                                Next
                            End If
                        End If
                    Next
            End Select
            Dim guidi As String = Guid.NewGuid.ToString
            Dim OutputFormat As String
            Dim oFileStream As System.IO.FileStream
            Select Case Formato
                Case FormatoPlantillaGS.DOC 'DOC                    
                    OutputFormat = ".doc"
                Case FormatoPlantillaGS.DOCX 'DOCX
                    OutputFormat = ".docx"
                Case FormatoPlantillaGS.PDF 'PDF
                    OutputFormat = ".pdf"
                Case FormatoPlantillaGS.ODT  'ODT
                    OutputFormat = ".odt"
                Case 4 'EPUB
                    OutputFormat = ".epub"
                    Dim epubOptions As New HtmlSaveOptions(SaveFormat.Epub)
                    epubOptions.ExportHeadersFootersMode = ExportHeadersFootersMode.None
                    ' This option improves how tables are displayed in the reader.
                    epubOptions.TableWidthOutputMode = HtmlElementSizeOutputMode.None
                    doc.Save(dataDir & "\" & RandomDirectory & "\" & guidi & OutputFormat, epubOptions)
                Case Else
                    OutputFormat = ".docx"
            End Select
            If Not Formato = 4 Then doc.Save(dataDir & "\" & RandomDirectory & "\" & guidi & OutputFormat)
            oFileStream = New System.IO.FileStream(dataDir & "\" & RandomDirectory & "\" & guidi & OutputFormat, IO.FileMode.Open)
            Dim byteBuffer() As Byte
            ReDim byteBuffer(oFileStream.Length - 1)
            Call oFileStream.Read(byteBuffer, 0, Int32.Parse(oFileStream.Length.ToString()))
            oFileStream.Close()
            Context.Response.Clear()
            Context.Response.AddHeader("Content-Type", "application/octet-stream")
            Context.Response.AddHeader("Content-Disposition", "attachment;filename=""Acta""" & OutputFormat)
            Context.Response.BinaryWrite(byteBuffer)
            Context.Response.End()
        End If
    End Sub
    Private Class HandleMergeFieldAlternatingRows
        Inherits FSNPage
        Implements IFieldMergingCallback
        ''' <summary>
        ''' Called for every merge field encountered in the document.
        ''' We can either return some data to the mail merge engine or do something
        ''' else with the document. In this case we modify cell formatting.
        ''' </summary>
        Private Sub IFieldMergingCallback_FieldMerging(ByVal e As FieldMergingArgs) Implements IFieldMergingCallback.FieldMerging
            If mBuilder Is Nothing Then
                mBuilder = New DocumentBuilder(e.Document)
            End If
            Dim Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Select Case e.FieldName
                Case "FEC_REUNION", "FECAPER", "FECPRES", "FECNEC", "FINI_SUM_PROCE", "FFIN_SUM_PROCE", _
                    "FINI_SUMINISTRO", "FFIN_SUMINISTRO", "FINI_SUM_ITEM", "FFIN_SUM_ITEM", "FINI_SUM_GR", "FFIN_SUM_GR"
                    If IsDBNull(e.FieldValue) Then
                        e.Text = ""
                    Else
                        e.Text = Format(CType(e.FieldValue, Date), Usuario.DateFormat.ShortDatePattern)
                    End If
                Case "HORA_INI_REU", "HORA_PROCESO"
                    If IsDBNull(e.FieldValue) Then
                        e.Text = ""
                    Else
                        e.Text = Format(CType(e.FieldValue, Date), Usuario.DateFormat.ShortTimePattern)
                    End If
                Case "HORA_FIN_REU"
                    If IsDBNull(e.FieldValue) Then
                        e.Text = ""
                    Else
                        e.Text = " - " & Format(CType(e.FieldValue, Date), Usuario.DateFormat.ShortTimePattern)
                    End If
                Case "FECLIMOFE"
                    If IsDBNull(e.FieldValue) Then
                        e.Text = ""
                    Else
                        e.Text = Format(CType(e.FieldValue, Date), Usuario.DateFormat.ShortDatePattern) & " " & Format(CType(e.FieldValue, Date), Usuario.DateFormat.ShortTimePattern)
                    End If
                Case "PRES_GEN", "ADJ_GEN", "AHORRO_GEN", "PRES_GR", "ADJ_GR", "AHORRO_GR", "PRES_TOT", "OFE_TOT", "AHORRO_TOT", "PRES_TOT_PROV", "OFE_TOT_PROV", _
                    "AHORRO_TOT_PROV", "PRES_TOT_PROV_NOADJ", "OFE_TOT_PROV_NOADJ", "AHORRO_TOT_PROV_NOADJ", "CANTIDAD", "PRECIO_PROVEEDOR", _
                    "PRECIO_PROVEEDOR", "CANT_PROVEEDOR", "TOTAL_PROVEEDOR", "PRECIO_PROVEEDOR_NOADJ", "ADJ_PROV", "AHORR_PROV", _
                    "PRES_AHORR", "ADJ_AHORR", "AHORR_GR", "PRECOFE", "IMPORTE", "AHORRO", "PRESUPUESTO_NOADJ", "IMPORTE_NOADJ", "AHORRO_NOADJ"
                    e.Text = FSNLibrary.modUtilidades.FormatNumber(DBNullToDbl(e.FieldValue), Usuario.NumberFormat)
                Case "PORCEN_GEN", "PORCEN_GR_RES", "PORCEN_TOT", "PORCEN_TOT_PROV", "PORCEN_TOT_PROV_NOADJ", "PORCEN_PROV", "PORCEN_GR", "AHORROPORCEN", "PORCEN_NOADJ"
                    e.Text = FSNLibrary.modUtilidades.FormatNumber(DBNullToDbl(e.FieldValue), Usuario.NumberFormat) & "%"
                Case "VOL_PROCESO"
                    If IsDBNull(e.FieldValue) Then
                        e.Text = 0
                    Else
                        e.Text = FSNLibrary.modUtilidades.FormatNumber(DBNullToDbl(e.FieldValue), Usuario.NumberFormat)
                    End If
                Case "TIPO_REUNION"
                    ModuloIdioma = TiposDeDatos.ModulosIdiomas.actaExport
                    If IsDBNull(e.FieldValue) Then
                        e.Text = ""
                    Else
                        If CType(e.FieldValue, Boolean) Then
                            e.Text = Textos(0)
                        Else
                            e.Text = Textos(1)
                        End If
                    End If
                Case "COMENT"
                    If IsDBNull(e.FieldValue) Then
                        For Each bmrk As Node In e.Field.Start.ParentNode.ChildNodes
                            If bmrk.NodeType = NodeType.BookmarkStart Then
                                If CType(bmrk, BookmarkStart).Name = "OPC_COMENTARIO" Then
                                    e.Field.Start.ParentParagraph.Remove()
                                Else
                                    e.Text = ""
                                End If
                            End If
                        Next
                    End If
                Case "FEC_PROXIMAR"
                    If IsDBNull(e.FieldValue) Then
                        For Each bmrk As Node In e.Field.Start.ParentNode.ChildNodes
                            If bmrk.NodeType = NodeType.BookmarkStart Then
                                If CType(bmrk, BookmarkStart).Name = "OPC_PROXIMA_REUNION" Then
                                    e.Field.Start.ParentParagraph.Remove()
                                Else
                                    e.Text = ""
                                End If
                            End If
                        Next
                    End If
                Case "TIPO_PROXIMAR"
                    ModuloIdioma = TiposDeDatos.ModulosIdiomas.actaExport
                    If IsDBNull(e.FieldValue) Then
                        e.Text = ""
                    Else
                        If CType(e.FieldValue, Boolean) Then
                            e.Text = Textos(0)
                        Else
                            e.Text = Textos(1)
                        End If
                    End If
                Case "ITEM_DEST", "ITEM_PAG", "ITEM_FECSUM", "IT_DEST_NOADJ", "IT_FECSUM_NOADJ", "IT_PAG_NOADJ"
                    If Not CType(e.FieldValue, Boolean) Then
                        i += 1
                        Dim builder As New DocumentBuilder(e.Document)
                        builder.MoveToField(e.Field, True)
                        builder.StartBookmark("DELETE_COL_" & i)
                        builder.Writeln("DELETE_COL_" & i)
                        builder.EndBookmark("DELETE_COL_" & i)
                    End If
                    e.Text = ""
                Case Else
            End Select
        End Sub
        Private Sub ImageFieldMerging(ByVal args As ImageFieldMergingArgs) Implements IFieldMergingCallback.ImageFieldMerging
        End Sub
        Private mBuilder As DocumentBuilder
        Private mRowIdx As Integer
    End Class
    ''' <summary>
    ''' Applies logic defined in the passed handler class to all unused regions in the document. This allows to manually control
    ''' how unused regions are handled in the document.
    ''' </summary>
    ''' <param name="doc">The document containing unused regions</param>
    ''' <param name="handler">The handler which implements the IFieldMergingCallback interface and defines the logic to be applied to each unmerged region.</param>
    Public Shared Sub ExecuteCustomLogicOnEmptyRegions(ByVal doc As Document, ByVal handler As IFieldMergingCallback)
        ExecuteCustomLogicOnEmptyRegions(doc, handler, Nothing) ' Pass null to handle all regions found in the document.
    End Sub
    ''' <summary>
    ''' Applies logic defined in the passed handler class to specific unused regions in the document as defined in regionsList. This allows to manually control
    ''' how unused regions are handled in the document.
    ''' </summary>
    ''' <param name="doc">The document containing unused regions</param>
    ''' <param name="handler">The handler which implements the IFieldMergingCallback interface and defines the logic to be applied to each unmerged region.</param>
    ''' <param name="regionsList">A list of strings corresponding to the region names that are to be handled by the supplied handler class. Other regions encountered will not be handled and are removed automatically.</param>
    Public Shared Sub ExecuteCustomLogicOnEmptyRegions(ByVal doc As Document, ByVal handler As IFieldMergingCallback, ByVal regionsList As ArrayList)
        ' Certain regions can be skipped from applying logic to by not adding the table name inside the CreateEmptyDataSource method.
        ' Enable this cleanup option so any regions which are not handled by the user's logic are removed automatically.
        doc.MailMerge.CleanupOptions = MailMergeCleanupOptions.RemoveUnusedRegions
        ' Set the user's handler which is called for each unmerged region.
        doc.MailMerge.FieldMergingCallback = handler
        ' Execute mail merge using the dummy dataset. The dummy data source contains the table names of 
        ' each unmerged region in the document (excluding ones that the user may have specified to be skipped). This will allow the handler 
        ' to be called for each field in the unmerged regions.
        doc.MailMerge.ExecuteWithRegions(CreateDataSourceFromDocumentRegions(doc, regionsList))
    End Sub
    ''' <summary>
    ''' Returns a DataSet object containing a DataTable for the unmerged regions in the specified document.
    ''' If regionsList is null all regions found within the document are included. If an ArrayList instance is present
    ''' the only the regions specified in the list that are found in the document are added.
    ''' </summary>
    Private Shared Function CreateDataSourceFromDocumentRegions(ByVal doc As Document, ByVal regionsList As ArrayList) As DataSet
        Const tableStartMarker As String = "TableStart:"
        Dim dataSet As New DataSet()
        Dim tableName As String = Nothing
        For Each fieldName As String In doc.MailMerge.GetFieldNames()
            If fieldName.Contains(tableStartMarker) Then
                tableName = fieldName.Substring(tableStartMarker.Length)
            ElseIf tableName IsNot Nothing Then
                ' Only add the table name as a new DataTable if it doesn't already exists in the DataSet.
                If dataSet.Tables(tableName) Is Nothing Then
                    Dim table As New DataTable(tableName)
                    table.Columns.Add(fieldName)
                    ' We only need to add the first field for the handler to be called for the fields in the region.
                    If regionsList Is Nothing OrElse regionsList.Contains(tableName) Then
                        table.Rows.Add("FirstField")
                    End If
                    dataSet.Tables.Add(table)
                End If
                tableName = Nothing
            End If
        Next fieldName
        Return dataSet
    End Function
    Public Class EmptyRegionsHandler
        Implements IFieldMergingCallback
        ''' <summary>
        ''' Called for each field belonging to an unmerged region in the document.
        ''' </summary>
        Public Sub FieldMerging(ByVal args As FieldMergingArgs) Implements IFieldMergingCallback.FieldMerging
            ' Remove the entire table of the Suppliers region. Also check if the previous paragraph
            ' before the table is a heading paragraph and if so remove that too.
            Select Case args.TableName
                Case "IT_DEST", "IT_PAGO", "IT_FECSUM"
                    Dim tableRow As Row = CType(args.Field.Start.GetAncestor(NodeType.Row), Row)
                    ' Check if the table has been removed from the document already.
                    If tableRow.ParentNode IsNot Nothing Then
                        tableRow.Remove()
                    End If
                Case Else
                    Dim paragraph As Paragraph = CType(args.Field.Start.GetAncestor(NodeType.Paragraph), Paragraph)
                    If paragraph.ParentNode IsNot Nothing Then
                        paragraph.Remove()
                    End If
            End Select
        End Sub
        ''' <summary>
        ''' Returns true if the paragraph uses any Heading style e.g Heading 1 to Heading 9
        ''' </summary>
        Private Function IsHeadingParagraph(ByVal para As Paragraph) As Boolean
            Return (para.ParagraphFormat.StyleIdentifier >= StyleIdentifier.Heading1 AndAlso para.ParagraphFormat.StyleIdentifier <= StyleIdentifier.Heading9)
        End Function
        Public Sub ImageFieldMerging(ByVal args As ImageFieldMergingArgs) Implements IFieldMergingCallback.ImageFieldMerging
        End Sub
    End Class
End Class