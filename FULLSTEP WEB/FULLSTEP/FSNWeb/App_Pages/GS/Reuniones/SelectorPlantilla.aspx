﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/EnBlanco.Master" CodeBehind="SelectorPlantilla.aspx.vb" Inherits="Fullstep.FSNWeb.SelectorPlantilla" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            if (typeof (EnBlancoMaster) !== 'undefined' && EnBlancoMaster) $('body').addClass('fondoMasterEnBlanco');
            var selectVista = $('#optVistaDefectoPlantilla');
            var selectPlantillas = $('#optPlantillaDefecto');
            if (selectVista.prop) {
                var optionsVista = selectVista.prop('options');
                var optionsPlantillas = selectPlantillas.prop('options');
            }
            else {
                var optionsVista = selectVista.attr('options');
                var optionsPlantillas = selectPlantillas.attr('options');
            }
            $('option', selectVista).remove();
            $('option', selectPlantillas).remove();
            optionsVista[optionsVista.length] = new Option(TextosPantalla[9], 1);
            optionsVista[optionsVista.length] = new Option(TextosPantalla[10], 2);
            var selectedPlantilla, vista, documento, formato;
            vista = 1; formato = 0; documento = '';
            var plantillas = {};
            $.each(infoPlantillas, function (key, value) {
                if (typeof (plantillas[value.Vista]) == 'undefined') plantillas[value.Vista] = [];
                plantillas[value.Vista].push({ Id: value.Id, Nom: value.languageInfo[idioma].NombreArchivo });
                if (value.FormatoDefecto) {
                    vista = parseInt(value.Vista);
                    documento = parseInt(value.Id);
                    formato = parseInt(value.FormatoDefecto);
                } else {
                    if ((value.PlantillaDefecto) && (documento == '')) {
                        vista = parseInt(value.Vista);
                        documento = parseInt(value.Id);
                        formato = parseInt(value.FormatoDefecto);
                    }
                }
            });
            switch (tipoDocumento) {
                case 1:
                    $('#lblCabecera').text(TextosPantalla[0]);
                    $('#optVistaDefectoPlantilla').hide();
                    $.each(plantillas[0], function (key, value) {
                        optionsPlantillas[optionsPlantillas.length] = new Option(value.Nom, value.Id);
                    });
                    break;
                case 2:
                    $('#lblCabecera').text(TextosPantalla[1]);
                    $('#optVistaDefectoPlantilla').show();
                    $('#optVistaDefectoPlantilla').val(vista);
                    $.each(plantillas[vista], function (key, value) {
                        optionsPlantillas[optionsPlantillas.length] = new Option(value.Nom, value.Id);
                    });
                    break;
            }
            if (typeof (documento) == 'undefined') $('#optPlantillaDefecto').val(documento);
            $('#lblSeleccionFormato').text(TextosPantalla[2]);
            $('#lblSeleccionPlantilla').text(TextosPantalla[8]);
            $('#lblGuardarConfiguracionDefecto').text(TextosPantalla[11]);
            $('#lblAceptarPlantilla').text(TextosPantalla[12]);
            $('#lblCancelarPlantilla').text(TextosPantalla[13]);
            $('[id$=optFormatoExportacion] input[value="' + formato + '"]').attr('checked', true);
            $('[checkbox]').live('click', function () {
                $('#' + $(this).attr('checkbox')).prop('checked', function (i, oldVal) { return !oldVal; });
            });
            $('#optVistaDefectoPlantilla').live('change', function () {
                $('option', $('#optPlantillaDefecto')).remove();
                $.each(plantillas[$('#optVistaDefectoPlantilla').val()], function (key, value) {
                    optionsPlantillas[optionsPlantillas.length] = new Option(value.Nom, value.Id);
                });
            });
            $('#btnAceptarPlantilla').live('click', function () {
                var IdPlantilla = $('#optPlantillaDefecto').val();

                if (IdPlantilla == null) {
                    alert(TextosPantalla[8].substr(0,TextosPantalla[8].length - 1));
                    return;
                }

                var Formato = $('[id$=optFormatoExportacion] input:checked').val();
                var rutaDocumento;
                if ($('#chkGuardarConfiguracionDefecto').is(':checked')) {
                    $.ajax({
                        type: 'POST',
                        url: rutaFS + 'GS/Reuniones/SelectorPlantilla.aspx/GS_Plantilla_Defecto_Usu',
                        data: JSON.stringify({ idPlantilla: IdPlantilla, formato: Formato }),
                        contentType: 'application/json;',
                        async: true
                    });
                }
                switch (tipoDocumento) {
                    case 1:
                        rutaDocumento = 'agendaExport.aspx?IdPlantilla=' + IdPlantilla + '&Formato=' + Formato;
                        break;
                    case 2:
                        rutaDocumento = 'actaExport.aspx?IdPlantilla=' + IdPlantilla + '&Formato=' + Formato;
                        break;
                    default:
                        break;
                }
                window.location.href = rutaFS + 'GS/Reuniones/' + rutaDocumento + proceso;
            });
            $('#btnCancelarPlantilla').live('click', function () {
                window.open('', '_self', '');
                window.close();
            });
        });
    </script>
    <div class="Bordear" style="float:left; width:98%; margin:1%; padding-bottom:10px;">
        <div id="divCabeceraPlantilla" class="tabInfoActive" style="clear:both; float:left; width:100%; top:0px;">			
		    <span id="lblCabecera" class="Texto16 TextoClaro Negrita" style="float:left; line-height:25px; margin-left:10px;"></span>
	    </div>
        <div style="clear:both; float:left; width:98%; margin:10px 1%;">
            <span id="lblSeleccionFormato"></span>
        </div>
        <div class="Bordear" style="float:left; width:50%; border-radius:5px; margin-left:1%;">
            <asp:RadioButtonList runat="server" ID="optFormatoExportacion">
            </asp:RadioButtonList>
        </div>
        <div style="clear:both; float:left; width:98%; margin:5px 1%;">
            <span id="lblSeleccionPlantilla"></span>
        </div>
        <div style="clear:both; float:left; width:100%; margin:5px 1%;">
            <select id="optVistaDefectoPlantilla" style="margin:0px 5px;"></select>
            <select id="optPlantillaDefecto" style="margin:0px 5px;"></select>
        </div>
        <div style="clear:both; float:left; width:100%; margin:5px 1%;">
            <input type="checkbox" id="chkGuardarConfiguracionDefecto" /> 
            <span id="lblGuardarConfiguracionDefecto" class="Texto12" style="cursor:pointer;" checkbox="chkGuardarConfiguracionDefecto"></span>
        </div>
        <div id="divBotonesPlantilla" style="position:relative; clear:both; float:left; width:100%; margin:15px 0px; text-align:center;">
			<div id="btnAceptarPlantilla" class="botonRedondeado" style="margin-right:5px;">
				<span id="lblAceptarPlantilla" style="line-height:23px;"></span>
			</div>
			<div id="btnCancelarPlantilla" class="botonRedondeado" style="margin-left:5px;">
				<span id="lblCancelarPlantilla" style="line-height:23px;"></span>
			</div>
		</div>
    </div>
</asp:Content>
