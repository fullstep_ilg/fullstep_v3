﻿Imports Fullstep.FSNLibrary.TiposDeDatos
Imports System.Web.Script.Serialization
Imports Fullstep.FSNServer
Public Class TraspasoAdjudicaciones
    Inherits FSNPage

    Private Const cnPageSize As Integer = 10
#Region "Propiedades y enumeraciones"
    Private Enum Panel As Byte
        Empresa = 0
        Proveedor = 1
        Grupo = 2
        Item = 3
    End Enum
    Private _EstadoIntegracionProveedor As EstadoIntegracionProce
    Private _AnyoProce As Short
    Private _Gmn1Proce As String
    Private _CodProce As Integer
    Private _listPosicionColumns As New List(Of String)
    Private _listColsEscalados As New List(Of String)
    Private _listColsAtributos As New List(Of String)
    Private _CabeceraEscalados As Boolean = False
    Private _EmpresaIntegrada As Boolean = False
    Private _ProveIntegrado As Boolean = False
    Private _GrupoIntegrado As Boolean = False
    Private _CodErpGrupoValue As String
    Private _CodErpGrupoText As String
    Private _GrupoSeleccionado As Boolean = False 'Variable que se pondra a true cuando un grupo es seleccionado desde el arbol de adjudicaciones
    Private _gEmpTieneIntegracion As Boolean
#End Region
#Region "Eventos Pagina"
    ''' <summary>
    ''' Establece las opciones del ScriptMgr usuario
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento init</param>
    ''' <remarks>Llamada desde: Sistema; MÃ¡x. 0,1 seg.</remarks>
    Private Sub TraspasoAdjudicaciones_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.ScriptMgr.EnableScriptGlobalization = True
        Me.ScriptMgr.EnableScriptLocalization = True
    End Sub
    Private Sub TraspasoAdjudicaciones_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.MasterPageFile = "~/App_Master/EnBlanco.Master"
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        _AnyoProce = Request("Anyo")
        _Gmn1Proce = Request("Gmn1")
        _CodProce = Request("Proce")

        ConfigurarControles()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.TraspasoAdjudicaciones
        CargarScripts()
        If Not IsPostBack Then
            CargarTextos()
            EliminarObjetoAdjudicacion()
            CargarArbolAdjudicaciones()

            whdgAdjAnt.Behaviors.Paging.PageSize = cnPageSize
            whdgAdjAnt.GridView.Behaviors.Paging.PageSize = cnPageSize
            If Not Request.QueryString("pag") Is Nothing Then
                whdgAdjAnt.GridView.Behaviors.Paging.PageIndex = CType(Request.QueryString("pag"), Integer)
            End If
            HttpContext.Current.Session("ComprobarAdjAnt") = True
        Else
            Dim sControlPostBack As String = Request.Form("__EVENTTARGET")
            If sControlPostBack.Contains("whdgItemsCambio") Then
                Dim serializer As New JavaScriptSerializer
                Dim oParametros As Object = serializer.Deserialize(Of Object)(Request("__EVENTARGUMENT"))
                Dim idEmp As Integer = oParametros("idEmp")
                Dim codGrupo As String = oParametros("codGrupo")
                Dim codProve As String = oParametros("codProve")
                Dim idGrupo As Integer = oParametros("idGrupo")
                Dim idItem As Integer = 0
                If oParametros.count > 4 Then
                    idItem = oParametros("idItem")
                End If
                _gEmpTieneIntegracion = Not wdtAdjudicaciones.ActiveNode.ParentNode.ParentNode.ImageUrl.Contains("sinInt")

                Dim EstIntegracion As Short
                Dim EstAux As String
                Dim dsAux As DataSet = ObtenerDsArbolAdjudicaciones()
                Dim Row As DataRow()
                Try
                    Row = dsAux.Tables(0).Select("EMPCOD=" & idEmp)
                    EstAux = DBNullToStr(Row(0)("EST_EMP"))
                    If EstAux = "" Then
                        EstIntegracion = -1
                    Else
                        EstIntegracion = CInt(EstAux)
                    End If
                Catch ex As Exception
                    EstIntegracion = -1
                End Try

                CargarItemsGrupo(codGrupo, _AnyoProce, _Gmn1Proce, _CodProce, codProve, idEmp, _gEmpTieneIntegracion, True, EstIntegracion)
                upItems.Update()
                CargarTextos()
                If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "CodEmpProveGrupo") Then
                    With FSNUser
                        Dim sVariablesJavascript As String = "var gIdEmp=" & idEmp & ";"
                        sVariablesJavascript = sVariablesJavascript & "var gCodGrupo='" & codGrupo & "';"
                        sVariablesJavascript = sVariablesJavascript & "var gCodProve='" & codProve & "';"
                        sVariablesJavascript = sVariablesJavascript & "var gIdGrupo=" & idGrupo & ";"
                        If idItem <> 0 Then
                            sVariablesJavascript = sVariablesJavascript & "var gidItem=" & idItem & ";"
                        End If
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "CodEmpProveGrupo", sVariablesJavascript, True)
                    End With
                End If
                ActualizarDatosTrasPostBack(_AnyoProce, _Gmn1Proce, _CodProce, codProve, idEmp, FSNUser.IdiomaCod, codGrupo, idGrupo)
            ElseIf sControlPostBack.Contains("whdgItems") Then
                'Compruebo que la empresa del grupo seleccionado tiene integracion, me sera necesario saberlo en el InitializeRow del grid de items
                _gEmpTieneIntegracion = Not wdtAdjudicaciones.ActiveNode.ParentNode.ParentNode.ImageUrl.Contains("sinInt")
				'Si el control que ha generado el postback es el grid es porque esta filtrando por la cabecera y le asignamos el datasource
				'cacheado previamente cuando se cargo el grupo
				If Not HttpContext.Current.Cache("dsItems_" & FSNUser.Cod) Is Nothing Then
					With whdgItems
						.Rows.Clear()
						.GridView.Rows.Clear()
						.DataSource = Cache("dsItems_" & FSNUser.Cod)
						.DataMember = "Items"
						.DataKeyFields = "ID_ITEM,CENTROS,CODART"
						.DataBind()
					End With
				End If
			Else
                If (Request("__EVENTTARGET") = btnHiddenAdjAnt.ClientID) Or (Request("__EVENTTARGET").StartsWith(whdgAdjAnt.ClientID)) Then CargarAdjsAntEmpresa(GetAdjudicacionesEmpresa)
            End If
        End If
    End Sub
#End Region
#Region "Procedimientos generales"
    Private Function GetAdjudicacionesEmpresa() As DataSet
        Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")

        Select Case Request("__EVENTTARGET")
            Case btnHiddenAdjAnt.ClientID
                Dim serializer As New JavaScriptSerializer
                Dim oParametros As Object = serializer.Deserialize(Of Object)(Request("__EVENTARGUMENT"))

                Dim iIdEmp As Integer = oParametros("IdEmp")
                Dim iAnyo As Integer = oParametros("Anyo")
                Dim sGmn1 As String = oParametros("Gmn1")
                Dim iProce As Integer = oParametros("codProce")

                Select Case oParametros("Nivel")
                    Case 1
                        Return AdjudicacionesEmpresa(oParametros("Nivel"), iAnyo, sGmn1, iProce, iIdEmp)
                    Case 2
                        Return AdjudicacionesEmpresa(oParametros("Nivel"), iAnyo, sGmn1, iProce, iIdEmp, oParametros("CodProve"))
                    Case 3
                        Return AdjudicacionesEmpresa(oParametros("Nivel"), iAnyo, sGmn1, iProce, iIdEmp, oParametros("CodProve"), oParametros("IdGrupo"))
                    Case 4, 5
                        Return AdjudicacionesEmpresa(oParametros("Nivel"), iAnyo, sGmn1, iProce, iIdEmp, oParametros("CodProve"), oParametros("IdGrupo"), oParametros("IdItem"))
                    Case Else
                        Return Nothing
                End Select
            Case Else
                Dim oNode As Infragistics.Web.UI.NavigationControls.DataTreeNode = wdtAdjudicaciones.ActiveNode
                Select Case oNode.Level
                    Case 0 'Empresa                        
                        Return AdjudicacionesEmpresa(1, _AnyoProce, _Gmn1Proce, _CodProce, oNode.Key)
                    Case 1 'Proveedor
                        Return AdjudicacionesEmpresa(2, _AnyoProce, _Gmn1Proce, _CodProce, oNode.ParentNode.Key, oNode.Key)
                    Case 2 'Grupos 
                        Return AdjudicacionesEmpresa(3, _AnyoProce, _Gmn1Proce, _CodProce, oNode.ParentNode.ParentNode.Key, oNode.ParentNode.Key, oNode.Value)
                    Case Else
                        Return Nothing
                End Select
        End Select
    End Function
    ''' <summary>Indica si hay adjudicaciones anteriores</summary>
    ''' <param name="iNivel"></param>
    ''' <param name="Anyo">Año del proceso</param>
    ''' <param name="Gmn1">Commodity del proceso</param>
    ''' <param name="IdEmp">Id empresa</param>
    ''' <param name="codProce">Codigo del proceso</param>
    ''' <param name="sCodProve">Codigo del proveedor</param>
    ''' <param name="iIdGrupo">Id del grupo</param>
    ''' <param name="iIdItem">Id del item</param>
    ''' <param name="AnyoProceAdj">Año del proceso al que pertenecen las adjudicaciones que se están buscando</param>
    ''' <param name="Gmn1ProceAdj">GMN del proceso al que pertenecen las adjudicaciones que se están buscando</param>
    ''' <param name="codProceProceAdj">Cod. del proceso al que pertenecen las adjudicaciones que se están buscando</param>
    ''' <param name="sCodProveProceAdj">Cod. del proveedor al que pertenecen las adjudicaciones que se están buscando</param>
    ''' <param name="EstadoActual">Estado de integración actual de los ítems para los que se busca las adj. anteriores</param>
    ''' <param name="bActualizarDatos"></param>
    ''' <remarks></remarks>
    Public Shared Function AdjudicacionesEmpresa(ByVal iNivel As Integer, ByVal Anyo As Integer, ByVal Gmn1 As String, ByVal codProce As Integer, ByVal IdEmp As Integer, Optional ByVal sCodProve As String = Nothing,
                                                 Optional ByVal iIdGrupo As Integer = 0, Optional ByVal iIdItem As Integer = 0, Optional ByVal AnyoProceAdj As Integer = 0, Optional ByVal Gmn1ProceAdj As String = Nothing,
                                                 Optional ByVal codProceProceAdj As Integer = 0, Optional ByVal sCodProveProceAdj As String = Nothing, Optional ByVal EstadoActual As Short = -2,
                                                 Optional ByVal bActualizarDatos As Boolean = False) As DataSet
        Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim dsDatos As DataSet = Nothing
        Dim sKey As String = Nothing
        Dim oAdjudicaciones As Fullstep.FSNServer.Adjudicaciones
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        oAdjudicaciones = FSNServer.Get_Object(GetType(FSNServer.Adjudicaciones))
        Select Case iNivel
            Case 1
                sKey = "Adjs_" & IdEmp & "_" & User.Cod
                If bActualizarDatos OrElse HttpContext.Current.Cache(sKey) Is Nothing Then dsDatos = oAdjudicaciones.ObtenerAdjsAnterioresProceso(Anyo, Gmn1, codProce, IdEmp,,,, AnyoProceAdj, Gmn1ProceAdj, codProceProceAdj, , EstadoActual)
            Case 2
                sKey = "Adjs_" & IdEmp & "_" & sCodProve & "_" & User.Cod
                If bActualizarDatos OrElse HttpContext.Current.Cache(sKey) Is Nothing Then dsDatos = oAdjudicaciones.ObtenerAdjsAnterioresProceso(Anyo, Gmn1, codProce, IdEmp, sCodProve,,, AnyoProceAdj, Gmn1ProceAdj, codProceProceAdj, sCodProveProceAdj, EstadoActual)
            Case 3
                sKey = "Adjs_" & IdEmp & "_" & sCodProve & "_" & iIdGrupo & "_" & User.Cod
                If bActualizarDatos OrElse HttpContext.Current.Cache(sKey) Is Nothing Then dsDatos = oAdjudicaciones.ObtenerAdjsAnterioresProceso(Anyo, Gmn1, codProce, IdEmp, sCodProve, iIdGrupo,, AnyoProceAdj, Gmn1ProceAdj, codProceProceAdj, sCodProveProceAdj, EstadoActual)
            Case 4, 5
                sKey = "Adjs_" & IdEmp & "_" & sCodProve & "_" & iIdGrupo & "_" & iIdItem & "_" & User.Cod
                If bActualizarDatos OrElse HttpContext.Current.Cache(sKey) Is Nothing Then dsDatos = oAdjudicaciones.ObtenerAdjsAnterioresProceso(Anyo, Gmn1, codProce, IdEmp, sCodProve, iIdGrupo, iIdItem, AnyoProceAdj, Gmn1ProceAdj, codProceProceAdj, sCodProveProceAdj, EstadoActual)
        End Select
        If dsDatos Is Nothing And Not bActualizarDatos Then
            dsDatos = HttpContext.Current.Cache(sKey)
        Else
            HttpContext.Current.Cache.Insert(sKey, dsDatos, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin_2"), 0))
            HttpContext.Current.Cache.Insert("Adjs_" & User.Cod, dsDatos, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin_2"), 0))
        End If
        Return dsDatos
    End Function
    Private Sub CargarTextos()
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "Textos") Then
            Dim sVariableJavascriptTextosBoolean As String = "var TextosBoolean = new Array();"
            sVariableJavascriptTextosBoolean &= "TextosBoolean[0]='" & JSText(Textos(44)) & "';"
            sVariableJavascriptTextosBoolean &= "TextosBoolean[1]='" & JSText(Textos(43)) & "';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Textos", sVariableJavascriptTextosBoolean, True)
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosScript") Then
            Dim sVariableJavascriptTextosJScript As String = "var TextosJScript = new Array();"
            sVariableJavascriptTextosJScript &= "TextosJScript[0]='" & JSText(Textos(39)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[1]='" & JSText(Textos(40)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[2]='" & JSText(Textos(41)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[3]='" & JSText(Textos(32)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[4]='" & JSText(Textos(33)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[5]='" & JSText(Textos(46)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[6]='" & JSText(Textos(0)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[7]='" & JSText(Textos(47)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[8]='" & JSText(Textos(48)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[9]='" & JSText(Textos(8)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[10]='" & JSText(Textos(34)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[11]='" & JSText(Textos(49)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[12]='" & JSText(Textos(31)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[13]='" & JSText(Textos(50)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[14]='" & FSNUser.RefCultural & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[15]='" & JSText(Textos(55)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[16]='" & JSText(Textos(38)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[17]='" & JSText(Textos(56)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[18]='" & JSText(Textos(57)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[19]='" & JSText(Textos(58)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[20]='" & JSText(Textos(45)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[21]='" & JSText(Textos(59)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[22]='" & JSText(Textos(60)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[23]='" & JSText(Textos(61)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[24]='""';" '  & JSText(Replace(lblEmpresaLiteralGrupo.ClientID, "lblEmpresaLiteralGrupo", "")) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[25]='" & JSText(Textos(64)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[26]='" & JSText(Me.FSNUser.Cod) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[27]='" & JSText(Textos(67)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[28]='" & JSText(Textos(87)) & "';"
            'Se va a proceder a trasladar el atributo  <NOMBRE_CAMPO> con el valor <VALOR_CAMPO> a todas las líneas.<SALTO_LINEA>¿Desea continuar?
            Dim MensajeAplicarTodos As String = JSText(Textos(88))
            sVariableJavascriptTextosJScript &= "TextosJScript[29]='" & Left(MensajeAplicarTodos, InStr(MensajeAplicarTodos, "<NOMBRE_CAMPO>") - 1) & "';"
            MensajeAplicarTodos = Right(MensajeAplicarTodos, Len(MensajeAplicarTodos) - InStr(MensajeAplicarTodos, "<NOMBRE_CAMPO>") - Len("<NOMBRE_CAMPO>") + 1)
            sVariableJavascriptTextosJScript &= "TextosJScript[30]='" & Left(MensajeAplicarTodos, InStr(MensajeAplicarTodos, "<VALOR_CAMPO>") - 1) & "';"
            MensajeAplicarTodos = Right(MensajeAplicarTodos, Len(MensajeAplicarTodos) - InStr(MensajeAplicarTodos, "<VALOR_CAMPO>") - Len("<VALOR_CAMPO>") + 1)
            sVariableJavascriptTextosJScript &= "TextosJScript[31]='" & Left(MensajeAplicarTodos, InStr(MensajeAplicarTodos, "<SALTO_LINEA>") - 1) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[32]='" & Right(MensajeAplicarTodos, Len(MensajeAplicarTodos) - InStr(MensajeAplicarTodos, "<SALTO_LINEA>") - Len("<SALTO_LINEA>") + 1) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[33]='" & JSText(Textos(18)) & "     " & JSText(Textos(89)) & "';" 'Distribuidores   (Actualmente distribuido a nivel de ítem) 
            sVariableJavascriptTextosJScript &= "TextosJScript[34]='" & JSText(Textos(90)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[35]='" & JSText(Textos(91)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[36]='" & JSText(Textos(92)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[37]='" & JSText(Textos(1)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[38]='" & JSText(Textos(51)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[39]='" & JSText(Textos(93)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[40]='" & JSText(Textos(94)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[41]='" & JSText(Textos(95)) & "';"

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosScript", sVariableJavascriptTextosJScript, True)
        End If

        Dim PeticionarioAnimationClientID As String = FSNPanelDatosPersona.AnimationClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "PeticionarioAnimationClientID", "<script>var PeticionarioAnimationClientID = '" & PeticionarioAnimationClientID & "' </script>")

        Dim PeticionarioDynamicPopulateClienteID As String = FSNPanelDatosPersona.DynamicPopulateClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "PeticionarioDynamicPopulateClienteID", "<script>var PeticionarioDynamicPopulateClienteID = '" & PeticionarioDynamicPopulateClienteID & "' </script>")

        lblEmpresaLiteralGrupo.Text = Textos(1) & ": "
        lblEmpresaLiteralProveedor.Text = Textos(1) & ": "
        lblProveedorLiteralGrupo.Text = Textos(2) & ": "
        lblCodigoProveedorLiteral.Text = Textos(2) & ": "
        lblCodigoERPLiteralGrupo.Text = Textos(7) & ": "
        lblCodigoERPLiteralProveedor.Text = Textos(7) & ": "
        lblCodigoErpDistribuidoresProveedor.Text = Textos(7)
        lblCodERPLiteral.Text = Textos(7) & ": "
        lblCodERPDistribuidoresItem.Text = Textos(7)
        lblCodERPDistribuidoresItemPanel.Text = Textos(7)
        lblDenProveedorLiteral.Text = Textos(6) & ": "
        lblEmpresaSinIntegracion.Text = Textos(13)
        lblProveedorSinIntegracion.Text = Textos(13)
        lblEmpresaSinIntegracionGrupo.Text = Textos(13)
        lblProveedoresAdjudicadosCabeceraEmpresa.Text = Textos(4)
        lblErpAsociadoEmpresaLiteral.Text = Textos(3) & ": "
        lblCodigoDistribuidoresProveedor.Text = Textos(5)
        lblCodigoAtributoTablaProveedor.Text = Textos(5)
        lblCodigoDistribuidoresItem.Text = Textos(5)
        lblCodigoDistribuidoresItemPanel.Text = Textos(5)
        lblCodigoAtributoTablaItem.Text = Textos(5)
        lblCodigoAtributoTablaGrupo.Text = Textos(5)
        lblDenominacionDistribuidoresProveedor.Text = Textos(6)
        lblDenominacionDistribuidoresItem.Text = Textos(6)
        lblDenomAtributoTablaGrupo.Text = Textos(6)
        lblDenomAtributoTablaItem.Text = Textos(6)
        lblDenomDistribuidoresItemPanel.Text = Textos(6)
        lblDenominacionAtributoTablaProveedor.Text = Textos(6)
        lblPorcentajeDistribuidoresItem.Text = Textos(52)
        lblPorcentajeDistribuidoresItemPanel.Text = Textos(52)
        lblPorcentajeDistribuidoresProveedor.Text = Textos(19)
        lblValorAtributoTablaGrupo.Text = Textos(16)
        lblValorAtributoTablaItem.Text = Textos(16)
        lblValorAtributoTablaProveedor.Text = Textos(16)
        lblDistribuidoresLiteralProveedor.Text = Textos(18)
        lblAtributosItemLiteral.Text = Textos(15)
        lblAtributosLiteralGrupo.Text = Textos(15)
        lblAtributosLiteralProveedor.Text = Textos(15)
        lblAtributosObligatoriosLiteralGrupo.Text = Textos(17)
        lblAtributosObligatoriosLiteralItem.Text = Textos(17)
        lblAtributosObligatoriosLiteralProveedor.Text = Textos(17)
        btnTrasladarAdjERP.Text = Textos(29)
        BtnTrasladarAdjERPDesdeProveedor.Text = Textos(29)
        btnTrasladarAdjERPDesdeEmpresa.Text = Textos(29)
        btnAceptarDistrItem.Text = Textos(37)
        btnAceptarPanelMensaje.Text = Textos(37)
        btnCancelarPanelMensaje.Text = Textos(38)
        btnAceptarPanelMensajeDistribuidores.Text = Textos(37)
        btnAceptar.Text = Textos(37)
        btnBorrarAdjERP.Text = Textos(30)
        BtnBorrarAdjERPDesdeProveedor.Text = Textos(30)
        btnBorrarAdjERPDesdeEmpresa.Text = Textos(30)
        btnCerrar.Text = Textos(38)
        btnCerrarDistrItem.Text = Textos(38)
        lblEstadoIntegracionItemLiteral.Text = Textos(21) & ": "
        lblEscaladoPreciosLiteral.Text = Textos(42) & ": "
        lblCabeceraDistrItemPanel.Text = Textos(18)
        lblCabeceraDistrItem.Text = Textos(18)
        lblFecIniItemLiteral.Text = Textos(35) & ": "
        lblFecFinItemLiteral.Text = Textos(36) & ": "
        lblCantAdjItemLiteral.Text = Textos(24) & ": "
        lblPrecUniItemLiteral.Text = Textos(25) & ": "
        lblCentroItemLiteral.Text = Textos(28) & ": "
        FSNPageHeader.TituloCabecera = Textos(53)
        lblProcesoArbol.Text = Textos(0) & " " & _AnyoProce & "/" & _Gmn1Proce & "/" & _CodProce
        lblTraspasarCentro.Text = Textos(68)
        chkComprobarAdjAntEmp.Text = Textos(75)
        chkComprobarAdjAntProve.Text = Textos(75)
        chkComprobarAdjAnt.Text = Textos(75)
        chkComprobarAdjAntItem.Text = Textos(75)
        lblCabeceraAdjAnt.Text = Textos(76)
        lblAdjAntTexto1.Text = Textos(77)
        lblAdjAntTexto2.Text = Textos(78)
        btnAceptarTraspasar.Text = Textos(85)
        btnAceptarAdjAnt.Text = Textos(37)
        btnCancelarAdjAnt.Text = Textos(55)
        btnAceptarConfirm.Value = Textos(43)
        btnCancelarConfirm.Value = Textos(44)
        lblTituloConfirm.Text = Textos(53)

        CType(whdgAdjAnt.GridView.Columns.FromKey("Key_SEL"), Infragistics.Web.UI.GridControls.UnboundField).Header.Text = ""
        CType(whdgAdjAnt.GridView.Columns.FromKey("Key_ART"), Infragistics.Web.UI.GridControls.BoundDataField).Header.Text = Textos(22)
        CType(whdgAdjAnt.GridView.Columns.FromKey("Key_DESCR"), Infragistics.Web.UI.GridControls.BoundDataField).Header.Text = Textos(79)
        CType(whdgAdjAnt.GridView.Columns.FromKey("Key_PROCE"), Infragistics.Web.UI.GridControls.UnboundField).Header.Text = Textos(0)
        CType(whdgAdjAnt.GridView.Columns.FromKey("Key_PROVE"), Infragistics.Web.UI.GridControls.UnboundField).Header.Text = Textos(51)
        CType(whdgAdjAnt.GridView.Columns.FromKey("Key_PROVE_ERP"), Infragistics.Web.UI.GridControls.BoundDataField).Header.Text = Textos(7)
        CType(whdgAdjAnt.GridView.Columns.FromKey("Key_CANTADJ"), Infragistics.Web.UI.GridControls.BoundDataField).Header.Text = Textos(24)
        CType(whdgAdjAnt.GridView.Columns.FromKey("Key_PREC_ADJ"), Infragistics.Web.UI.GridControls.BoundDataField).Header.Text = Textos(25)
        CType(whdgAdjAnt.GridView.Columns.FromKey("Key_FECINI"), Infragistics.Web.UI.GridControls.BoundDataField).Header.Text = Textos(80)
        CType(whdgAdjAnt.GridView.Columns.FromKey("Key_FECFIN"), Infragistics.Web.UI.GridControls.BoundDataField).Header.Text = Textos(81)
        CType(whdgAdjAnt.GridView.Columns.FromKey("Key_CENTRO"), Infragistics.Web.UI.GridControls.BoundDataField).Header.Text = Textos(28)
        CType(whdgAdjAnt.GridView.Columns.FromKey("Key_FECENV"), Infragistics.Web.UI.GridControls.BoundDataField).Header.Text = Textos(82)
        CType(whdgAdjAnt.GridView.Columns.FromKey("Key_USU"), Infragistics.Web.UI.GridControls.BoundDataField).Header.Text = Textos(65)

        CType(whdgAdjAnt.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblOF"), Label).Text = Textos(83)
        CType(whdgAdjAnt.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPage"), Label).Text = Textos(84)
    End Sub
    ''' <summary>
    ''' Elimina el objeto adjudicacion de la cache
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub EliminarObjetoAdjudicacion()
        System.Web.HttpContext.Current.Cache.Remove("oAdjudicacion_" & FSNUser.Cod)

        'Eliminar cache de adjudicaciones        
        Dim oEnumerator As System.Collections.IDictionaryEnumerator = Cache.GetEnumerator
        While oEnumerator.MoveNext
            If oEnumerator.Key.ToString.StartsWith("Adjs") Then
                System.Web.HttpContext.Current.Cache.Remove(oEnumerator.Key.ToString)
            End If
        End While
    End Sub
    ''' <summary>
    ''' Procedimiento que configura los controles de la pagina, asigna url a la imagenes...
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ConfigurarControles()
        FSNPageHeader.UrlImagenCabecera = "~/App_Themes/" & Page.Theme & "/images/TraspasoAdjudicaciones_head.PNG"
        imgProcesoArbol.Src = "~/App_Themes/" & Page.Theme & "/images/proceso.png"
        imgEmpresaCabecera.Src = "~/App_Themes/" & Page.Theme & "/images/empresa.png"
        imgProveedorCabecera.Src = "~/App_Themes/" & Page.Theme & "/images/proveedor.png"
        imgEstadoIntegracionEmpresa.Src = "~/App_Themes/" & Page.Theme & "/images/Ico_Vigente.gif"
        imgProveedoresAdjudicadosEmpresa.Src = "~/App_Themes/" & Page.Theme & "/images/proveedor.png"
        imgEmpresaSinIntegracion.Src = "~/App_Themes/" & Page.Theme & "/images/alert-rojo-mini.png"
        imgProveedorSinIntegracion.Src = "~/App_Themes/" & Page.Theme & "/images/alert-rojo-mini.png"
        imgEmpresaSinIntegracionGrupo.Src = "~/App_Themes/" & Page.Theme & "/images/alert-rojo-mini.png"
        imgAtributosProveedor.Src = "~/App_Themes/" & Page.Theme & "/images/atributo.png"
        imgAtributosGrupo.Src = "~/App_Themes/" & Page.Theme & "/images/atributo.png"
        imgAtributosItem.Src = "~/App_Themes/" & Page.Theme & "/images/atributo.png"
        imgDistribuidoresProveedor.Src = "~/App_Themes/" & Page.Theme & "/images/distribuidores.PNG"
        imgCabeceraDistrItem.Src = "~/App_Themes/" & Page.Theme & "/images/distribuidores.PNG"
        imgCabeceraDistrItemPanel.Src = "~/App_Themes/" & Page.Theme & "/images/distribuidores.PNG"
        imgItemsGrupoCabecera.Src = "~/App_Themes/" & Page.Theme & "/images/grupo.PNG"
        imgExpandirOcultarArbol.Src = "~/App_Themes/" & Page.Theme & "/images/incluir_color_izq.gif"
        imgCabeceraItem.Src = "~/App_Themes/" & Page.Theme & "/images/grupo.PNG"
        imgEstadoIntegracionItem.Src = "~/App_Themes/" & Page.Theme & "/images/Ico_Vigente.gif"
        imgMensajeAlerta.Src = "~/App_Themes/" & Page.Theme & "/images/Icono_Error_Amarillo_40x40.gif"
        imgMensajeAlertaAtribsObligEnEmpresa.Src = "~/App_Themes/" & Page.Theme & "/images/Icono_Error_Amarillo_40x40.gif"
        imgMensajeAlertaAtribsObligEnProveedor.Src = "~/App_Themes/" & Page.Theme & "/images/Icono_Error_Amarillo_40x40.gif"
        imgCabeceraAdjAnt.Src = "~/App_Themes/" & Page.Theme & "/images/Icono_Error_Amarillo_40x40.gif"

        btnTrasladarAdjERP.Style("display") = "none"
        btnBorrarAdjERP.Style("display") = "none"
    End Sub
    ''' <summary>
    ''' Funcion que carga variables javascript
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarScripts()
        Dim sVariablesJavascript As String

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "NumberFormatUsu") Then
            With FSNUser
                sVariablesJavascript = "var UsuNumberDecimalSeparator = '" & .NumberFormat.NumberDecimalSeparator & "';"
                sVariablesJavascript = sVariablesJavascript & "var UsuNumberGroupSeparator='" & .NumberFormat.NumberGroupSeparator & "';"
                sVariablesJavascript = sVariablesJavascript & "var UsuNumberNumDecimals='" & .NumberFormat.NumberDecimalDigits & "';"
                sVariablesJavascript = sVariablesJavascript & "var gAnyo=" & _AnyoProce & ";"
                sVariablesJavascript = sVariablesJavascript & "var gGmn1='" & _Gmn1Proce & "';"
                sVariablesJavascript = sVariablesJavascript & "var gCodProce=" & _CodProce & ";"
                sVariablesJavascript = sVariablesJavascript & "var bMantenerEstAdjEnTraspasoAdj=" & Acceso.gbMantEstIntEnTraspasoAdj.ToString.ToLower & ";"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NumberFormatUsu", sVariablesJavascript, True)
            End With

            sVariablesJavascript = "var ACCTraspAdjModifProveERPEmp = " & AccionesSummit.ACCTraspAdjModifProveERPEmp & ";"
            sVariablesJavascript &= "var ACCTraspAdjModifProveERProve = " & AccionesSummit.ACCTraspAdjModifProveERProve & ";"
            sVariablesJavascript &= "var ACCTraspAdjModifProveERPGrupo = " & AccionesSummit.ACCTraspAdjModifProveERPGrupo & ";"
            sVariablesJavascript &= "var ACCTraspAdjModifProveERPItem = " & AccionesSummit.ACCTraspAdjModifProveERPItem & ";"
            sVariablesJavascript &= "var ACCTraspAdjTraspasarEmp = " & AccionesSummit.ACCTraspAdjTraspasarEmp & ";"
            sVariablesJavascript &= "var ACCTraspAdjTraspasarProve = " & AccionesSummit.ACCTraspAdjTraspasarProve & ";"
            sVariablesJavascript &= "var ACCTraspAdjTraspasarGrupo = " & AccionesSummit.ACCTraspAdjTraspasarGrupo & ";"
            sVariablesJavascript &= "var ACCTraspAdjTraspasarItem = " & AccionesSummit.ACCTraspAdjTraspasarItem & ";"
            sVariablesJavascript &= "var ACCTraspAdjBorrarEmp = " & AccionesSummit.ACCTraspAdjBorrarEmp & ";"
            sVariablesJavascript &= "var ACCTraspAdjBorrarProve = " & AccionesSummit.ACCTraspAdjBorrarProve & ";"
            sVariablesJavascript &= "var ACCTraspAdjBorrarGrupo = " & AccionesSummit.ACCTraspAdjBorrarGrupo & ";"
            sVariablesJavascript &= "var ACCTraspAdjBorrarItem = " & AccionesSummit.ACCTraspAdjBorrarItem & ";"
            sVariablesJavascript &= "var ACCTraspAdjModifAtribGrupo = " & AccionesSummit.ACCTraspAdjModifAtribGrupo & ";"
            sVariablesJavascript &= "var ACCTraspAdjModifAtribItem = " & AccionesSummit.ACCTraspAdjModifAtribItem & ";"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "AccionesRegistro", sVariablesJavascript, True)

            sVariablesJavascript = "var gAnyo = " & _AnyoProce & ";"
            sVariablesJavascript &= "var gGmn1 = '" & _Gmn1Proce & "';"
            sVariablesJavascript &= "var gCodProce = " & _CodProce & ";"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Proceso", sVariablesJavascript, True)
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "ModalProgress") Then Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalProgress", "<script>var ModalProgress = '" & ModalProgress.ClientID & "' </script>")
    End Sub
    ''' <summary>
    ''' Pone el texto correspondiente en la etiqueta lblProcesando.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: El prerender de la etiqueta. Tiempo máximo: 0sg.</remarks>
    Protected Sub LblProcesando_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        ModuloIdioma = ModulosIdiomas.TraspasoAdjudicaciones
        CType(sender, Label).Text = Textos(45)
    End Sub
    ''' <summary>
    ''' Procedimiento que carga el arbol de las adjudicaciones
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarArbolAdjudicaciones()
        Dim oDataTreeNodeBinding As Infragistics.Web.UI.NavigationControls.DataTreeNodeBinding

        oDataTreeNodeBinding = New Infragistics.Web.UI.NavigationControls.DataTreeNodeBinding
        With oDataTreeNodeBinding
            .DataMember = "Empresas"
            .TextField = "EMPDEN"
            .KeyField = "EMPCOD"
            .ValueField = "NIF"
        End With
        wdtAdjudicaciones.DataBindings.Add(oDataTreeNodeBinding)

        oDataTreeNodeBinding = New Infragistics.Web.UI.NavigationControls.DataTreeNodeBinding
        With oDataTreeNodeBinding
            .DataMember = "Proveedores"
            .TextField = "PROVEDEN"
            .KeyField = "PROVECOD"
            .ValueField = "PROVEDEN"
        End With
        wdtAdjudicaciones.DataBindings.Add(oDataTreeNodeBinding)

        oDataTreeNodeBinding = New Infragistics.Web.UI.NavigationControls.DataTreeNodeBinding
        With oDataTreeNodeBinding
            .DataMember = "Grupos"
            .TextField = "GRUPODEN"
            .KeyField = "GRUPOCOD"
            .ValueField = "GRUPOID"
        End With
        wdtAdjudicaciones.DataBindings.Add(oDataTreeNodeBinding)

        wdtAdjudicaciones.SelectionType = Infragistics.Web.UI.NavigationControls.NodeSelectionTypes.Single
        wdtAdjudicaciones.EnableExpandOnClick = True

		wdtAdjudicaciones.DataSource = ObtenerDsArbolAdjudicaciones()
        wdtAdjudicaciones.DataBind()

        For Each node As Infragistics.Web.UI.NavigationControls.DataTreeNode In wdtAdjudicaciones.AllNodes
            node.Expanded = True
        Next

        If wdtAdjudicaciones.Nodes.Count > 0 Then
            wdtAdjudicaciones.Nodes(0).Selected = True
            wdtAdjudicaciones.ActiveNode = wdtAdjudicaciones.Nodes(0) 'Si no, la propiedad ActiveNode es Nothing

            Dim bEmpTieneIntegracion As Boolean = Not wdtAdjudicaciones.Nodes(0).ImageUrl.Contains("sinInt")
            Dim drNode As DataRow = DirectCast(DirectCast(wdtAdjudicaciones.Nodes(0).DataItem, Infragistics.Web.UI.Framework.Data.DataSetNode).Item, System.Data.DataRowView).Row
            Dim EstIntegracion As Short
            If drNode("EST_EMP") Is DBNull.Value Then
                EstIntegracion = -1
            Else
                EstIntegracion = drNode("EST_EMP")
            End If
            CargarEmpresa(wdtAdjudicaciones.Nodes(0).Key, bEmpTieneIntegracion, EstIntegracion, wdtAdjudicaciones.Nodes(0).Text)
        End If
    End Sub
    ''' <summary>
    ''' Funcion que obtiene los datos del arbol de adjudicaciones
    ''' </summary>
    ''' <remarks></remarks>
    Private Function ObtenerDsArbolAdjudicaciones() As DataSet
        Dim oAdjudicaciones As Fullstep.FSNServer.Adjudicaciones
        oAdjudicaciones = FSNServer.Get_Object(GetType(FSNServer.Adjudicaciones))

        Return oAdjudicaciones.ObtenerAdjudicacionesProceso(_AnyoProce, _Gmn1Proce, _CodProce)
    End Function
    ''' <summary>
    ''' Procedimiento que muestra un panel y oculta el resto
    ''' </summary>
    ''' <param name="PanelMostrar"></param>
    ''' <remarks></remarks>
    Private Sub MostrarPanel(ByVal PanelMostrar As Panel)
        Select Case PanelMostrar
            Case Panel.Empresa
                divEmpresa.Style.Add("display", "")
                divProveedor.Style.Add("display", "none")
                divItemsGrupo.Style.Add("display", "none")
                divContenedorArbol.Style.Add("display", "")
            Case Panel.Proveedor
                divEmpresa.Style.Add("display", "none")
                divProveedor.Style.Add("display", "")
                divItemsGrupo.Style.Add("display", "none")
                divContenedorArbol.Style.Add("display", "")
            Case Panel.Grupo
                divEmpresa.Style.Add("display", "none")
                divProveedor.Style.Add("display", "none")
                divItemsGrupo.Style.Add("display", "")
        End Select
    End Sub
#End Region
#Region "Panel Empresa"
    ''' <summary>
    ''' Procedimiento que carga los datos del panel de empresa cuando se seleccione desde el arbol de adjudicaciones
    ''' </summary>
    ''' <param name="idEmp">id de la empresa</param>
    ''' <param name="bEmpTieneIntegracion"></param>
    ''' <param name="EstIntegracion">estado de integracion de la empresa</param>
    ''' <remarks></remarks>
    Private Sub CargarEmpresa(ByVal idEmp As Long, ByVal bEmpTieneIntegracion As Boolean, ByVal EstIntegracion As Short, ByVal DenEmpresa As String)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.TraspasoAdjudicaciones
        MostrarPanel(Panel.Empresa)
        lblEmpresaLiteralCabecera.Text = Textos(1) & " " & DenEmpresa

        If Not HttpContext.Current.Session("ComprobarAdjAnt") Is Nothing Then chkComprobarAdjAntEmp.Checked = CType(HttpContext.Current.Session("ComprobarAdjAnt"), Boolean)

        CargarEstadoIntegracionNivelEmpresa(bEmpTieneIntegracion, EstIntegracion)
        CrearTablaProveedoresAdjudicadosEmpresa(idEmp, EstIntegracion, bEmpTieneIntegracion)
    End Sub
    ''' <summary>
    ''' Carga el estado de la integracion a nivel de empresa
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarEstadoIntegracionNivelEmpresa(ByVal TieneIntegracion As Boolean, ByVal EstIntegracion As Short)
        Select Case EstIntegracion
            Case EstadoIntegracionProce.Pendiente
                lblEstadoIntegracionEmpresa.Text = Textos(10)
                imgEstadoIntegracionEmpresa.Src = "~/App_Themes/" & Page.Theme & "/images/Ico_Pendiente.gif"
                btnTrasladarAdjERPDesdeEmpresa.Visible = True
                chkComprobarAdjAntEmp.Visible = btnTrasladarAdjERPDesdeEmpresa.Visible
                btnBorrarAdjERPDesdeEmpresa.Visible = False
                celdaBtnTrasladarAdjDesdeEmpresa.Style.Add("width", "60%")
                celdaBtnBorrarAdjDesdeEmpresa.Style.Add("width", "39%")
                btnTrasladarAdjERPDesdeEmpresa.Style.Add("float", "left")
            Case EstadoIntegracionProce.ParcialEsperaAcuseRecibo
                lblEstadoIntegracionEmpresa.Text = Textos(69)
                imgEstadoIntegracionEmpresa.Src = "~/App_Themes/" & Page.Theme & "/images/Ico_Opcional.gif"
                btnTrasladarAdjERPDesdeEmpresa.Visible = False
                chkComprobarAdjAntEmp.Visible = btnTrasladarAdjERPDesdeEmpresa.Visible
                btnBorrarAdjERPDesdeEmpresa.Visible = False
                celdaBtnTrasladarAdjDesdeEmpresa.Style.Add("width", "49%")
                celdaBtnBorrarAdjDesdeEmpresa.Style.Add("width", "49%")
            Case EstadoIntegracionProce.TotalEsperaAcuseRecibo
                lblEstadoIntegracionEmpresa.Text = Textos(72)
                imgEstadoIntegracionEmpresa.Src = "~/App_Themes/" & Page.Theme & "/images/Ico_Opcional.gif"
                btnTrasladarAdjERPDesdeEmpresa.Visible = False
                chkComprobarAdjAntEmp.Visible = btnTrasladarAdjERPDesdeEmpresa.Visible
                btnBorrarAdjERPDesdeEmpresa.Visible = False
                celdaBtnTrasladarAdjDesdeEmpresa.Style.Add("width", "49%")
                celdaBtnBorrarAdjDesdeEmpresa.Style.Add("width", "49%")
            Case EstadoIntegracionProce.ParcialOK
                lblEstadoIntegracionEmpresa.Text = Textos(70)
                imgEstadoIntegracionEmpresa.Src = "~/App_Themes/" & Page.Theme & "/images/Ico_Vigente.gif"
                btnTrasladarAdjERPDesdeEmpresa.Visible = True
                chkComprobarAdjAntEmp.Visible = btnTrasladarAdjERPDesdeEmpresa.Visible
                btnBorrarAdjERPDesdeEmpresa.Visible = True
                celdaBtnTrasladarAdjDesdeEmpresa.Style.Add("width", IIf(Acceso.gbMantEstIntEnTraspasoAdj, "60%", "0%"))
                celdaBtnBorrarAdjDesdeEmpresa.Style.Add("width", IIf(Acceso.gbMantEstIntEnTraspasoAdj, "39%", "100%"))
                btnBorrarAdjERPDesdeEmpresa.Style.Add("float", "left")
            Case EstadoIntegracionProce.TotalOK
                lblEstadoIntegracionEmpresa.Text = Textos(73)
                imgEstadoIntegracionEmpresa.Src = "~/App_Themes/" & Page.Theme & "/images/Ico_Vigente.gif"
                btnTrasladarAdjERPDesdeEmpresa.Visible = IIf(Acceso.gbMantEstIntEnTraspasoAdj, True, False)
                chkComprobarAdjAntEmp.Visible = btnTrasladarAdjERPDesdeEmpresa.Visible
                btnBorrarAdjERPDesdeEmpresa.Visible = True
                celdaBtnTrasladarAdjDesdeEmpresa.Style.Add("width", IIf(Acceso.gbMantEstIntEnTraspasoAdj, "60%", "0%"))
                celdaBtnBorrarAdjDesdeEmpresa.Style.Add("width", IIf(Acceso.gbMantEstIntEnTraspasoAdj, "39%", "100%"))
                btnBorrarAdjERPDesdeEmpresa.Style.Add("float", "left")
            Case EstadoIntegracionProce.ParcialError
                lblEstadoIntegracionEmpresa.Text = Textos(71)
                imgEstadoIntegracionEmpresa.Src = "~/App_Themes/" & Page.Theme & "/images/Ico_Expirado.gif"
                btnTrasladarAdjERPDesdeEmpresa.Visible = True
                chkComprobarAdjAntEmp.Visible = btnTrasladarAdjERPDesdeEmpresa.Visible
                btnBorrarAdjERPDesdeEmpresa.Visible = True
                celdaBtnTrasladarAdjDesdeEmpresa.Style.Add("width", IIf(Acceso.gbMantEstIntEnTraspasoAdj, "60%", "0%"))
                celdaBtnBorrarAdjDesdeEmpresa.Style.Add("width", IIf(Acceso.gbMantEstIntEnTraspasoAdj, "39%", "100%"))
                btnBorrarAdjERPDesdeEmpresa.Style.Add("float", "left")
            Case EstadoIntegracionProce.TotalError
                lblEstadoIntegracionEmpresa.Text = Textos(74)
                imgEstadoIntegracionEmpresa.Src = "~/App_Themes/" & Page.Theme & "/images/Ico_Expirado.gif"
                btnTrasladarAdjERPDesdeEmpresa.Visible = True
                chkComprobarAdjAntEmp.Visible = btnTrasladarAdjERPDesdeEmpresa.Visible
                btnBorrarAdjERPDesdeEmpresa.Visible = True
                celdaBtnTrasladarAdjDesdeEmpresa.Style.Add("width", IIf(Acceso.gbMantEstIntEnTraspasoAdj, "60%", "0%"))
                celdaBtnBorrarAdjDesdeEmpresa.Style.Add("width", IIf(Acceso.gbMantEstIntEnTraspasoAdj, "39%", "100%"))
                btnBorrarAdjERPDesdeEmpresa.Style.Add("float", "left")
            Case Else
                lblEstadoIntegracionEmpresa.Text = Textos(10)
                imgEstadoIntegracionEmpresa.Src = "~/App_Themes/" & Page.Theme & "/images/Ico_Pendiente.gif"
                btnTrasladarAdjERPDesdeEmpresa.Visible = True
                chkComprobarAdjAntEmp.Visible = btnTrasladarAdjERPDesdeEmpresa.Visible
                btnBorrarAdjERPDesdeEmpresa.Visible = False
                celdaBtnTrasladarAdjDesdeEmpresa.Style.Add("width", "60%")
                celdaBtnBorrarAdjDesdeEmpresa.Style.Add("width", "39%")
                btnTrasladarAdjERPDesdeEmpresa.Style.Add("float", "left")
        End Select
        If TieneIntegracion Then
            divEmpresaSinIntegracion.Style.Add("display", "none")
            divDatosEmpresa.Style.Add("display", "")
            celdaBtnTrasladarAdjDesdeEmpresa.Style.Add("display", "")
            celdaBtnBorrarAdjDesdeEmpresa.Style.Add("display", "")
        Else
            divEmpresaSinIntegracion.Style.Add("display", "")
            divDatosEmpresa.Style.Add("display", "none")
            celdaBtnTrasladarAdjDesdeEmpresa.Style.Add("display", "none")
            celdaBtnBorrarAdjDesdeEmpresa.Style.Add("display", "none")
        End If
    End Sub
    ''' <summary>
    ''' Procedimiento que crea la tabla de proveedores adjudicados en el panel de empresa
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CrearTablaProveedoresAdjudicadosEmpresa(ByVal idEmp As Long, ByVal EstadoIntegracionEmpresa As Short, ByVal bEmpTieneIntegracion As Boolean)
        Dim dsProvesAdj As DataSet = CargarProveedoresAdjudicados(idEmp, _AnyoProce, _Gmn1Proce, _CodProce)
        Dim HeaderRow As New HtmlTableRow
        Dim HeaderCell As HtmlTableCell

        If dsProvesAdj.Tables(2).Rows.Count > 0 Then
            lblErpAsociadoEmpresaDato.Text = dsProvesAdj.Tables(2)(0)("ERP_ASOCIADO_EMP")
        Else
            lblErpAsociadoEmpresaDato.Text = ""
        End If

        HeaderRow.Style.Add("background-color", "#D8D5D5")
        Dim ColsCamposPer As DataColumnCollection = dsProvesAdj.Tables("Proveedores").ChildRelations(0).ChildTable.Columns
        'Cabecera tabla
        For Each Col As DataColumn In ColsCamposPer
            Select Case Col.ColumnName
                Case "PROVECOD"
                    HeaderCell = New HtmlTableCell
                    HeaderCell.InnerText = Textos(5)
                    HeaderCell.Style.Add("width", Unit.Percentage(13).ToString)
                    HeaderRow.Cells.Add(HeaderCell)
                Case "PROVEDEN"
                    HeaderCell = New HtmlTableCell
                    HeaderCell.InnerText = Textos(6)
                    HeaderCell.Style.Add("width", Unit.Percentage(40 - ((ColsCamposPer.Count - 4) * 7)).ToString)
                    HeaderRow.Cells.Add(HeaderCell)
                Case "COD_ERP"
                    If bEmpTieneIntegracion Then
                        HeaderCell = New HtmlTableCell
                        HeaderCell.InnerText = Textos(7)
                        HeaderCell.Style.Add("width", Unit.Percentage(48 - ((ColsCamposPer.Count - 4) * 8)).ToString)
                        HeaderRow.Cells.Add(HeaderCell)
                    End If
                Case "DEN_ERP"
                Case Else
                    'Cabeceras de campos personalizados
                    If bEmpTieneIntegracion Then
                        HeaderCell = New HtmlTableCell
                        HeaderCell.InnerText = Col.ColumnName
                        HeaderCell.Style.Add("width", Unit.Percentage(15).ToString)
                        HeaderRow.Cells.Add(HeaderCell)
                    End If
            End Select
        Next
        tblProveedoresAdjudicados.Rows.Add(HeaderRow)

        'Lineas tabla
        Dim TableRow As HtmlTableRow
        Dim TableCell As HtmlTableCell
        Dim fila As Short = 1
        For Each row As DataRow In dsProvesAdj.Tables(0).Rows
            TableRow = New HtmlTableRow
            For Each Col As DataColumn In row.Table.Columns
                Select Case Col.ColumnName
                    Case "PROVECOD"
                        TableCell = New HtmlTableCell
                        TableCell.InnerText = row(Col.ColumnName)
                        TableCell.Style.Add("background-color", "#f0f0f0")
                        TableRow.Cells.Add(TableCell)
                    Case "PROVEDEN"
                        TableCell = New HtmlTableCell
                        TableCell.InnerText = row(Col.ColumnName)
                        TableCell.Style.Add("background-color", "#f0f0f0")
                        TableRow.Cells.Add(TableCell)

                        'Despues de PROVEDEN metemos el Codigo ERP y luego los campos personalizados si estarian activados
                        Dim childRows() As DataRow = row.GetChildRows("RELACION_1")
                        If bEmpTieneIntegracion Then
                            'Si no tiene integracion no se mostraran los campos Codigo Erp y campos personalizados
                            Select Case EstadoIntegracionEmpresa
                                Case EstadoIntegracionProce.TotalOK, EstadoIntegracionProce.ParcialOK, EstadoIntegracionProce.ParcialEsperaAcuseRecibo, EstadoIntegracionProce.TotalEsperaAcuseRecibo
                                    If row("NUM_DISTR") > 0 Then
                                        'Si el proveedor tiene proveedores relacionados no se podra editar la celda Codigo ERP y se pondra en blanco, tendra que ir a la seccion de proveedor
                                        TableCell = New HtmlTableCell
                                        TableCell.InnerText = ""

                                        TableRow.Cells.Add(TableCell)

                                        'Campos personalizados
                                        If childRows.Length > 0 Then
                                            For i = 4 To childRows(0).Table.Columns.Count - 1
                                                TableCell = New HtmlTableCell
                                                TableCell.InnerText = ""
                                                TableRow.Cells.Add(TableCell)
                                            Next
                                        End If
                                    Else 'Solo un codigo ERP, al que se asigno
                                        TableCell = New HtmlTableCell
                                        If childRows.Length > 0 Then
                                            TableCell.InnerText = DBNullToStr(childRows(0)("DEN_ERP"))
                                        End If
                                        TableRow.Cells.Add(TableCell)

                                        'Campos personalizados
                                        If childRows.Length > 0 Then
                                            For i = 4 To childRows(0).Table.Columns.Count - 1
                                                TableCell = New HtmlTableCell
                                                TableCell.InnerText = DBNullToStr(childRows(0)(i))
                                                TableRow.Cells.Add(TableCell)
                                            Next
                                        End If
                                    End If
                                Case EstadoIntegracionProce.ParcialOK
                                    Select Case row("EST_INTEGRACION") 'Dependiendo del estado de integracion a nivel de proveedor
                                        Case EstadoIntegracionProce.TotalOK, EstadoIntegracionProce.ParcialEsperaAcuseRecibo, EstadoIntegracionProce.TotalEsperaAcuseRecibo
                                            If row("NUM_DISTR") > 0 Then
                                                'Si el proveedor tiene proveedores relacionados no se podra editar la celda Codigo ERP y se pondra en blanco, tendra que ir a la seccion de proveedor
                                                TableCell = New HtmlTableCell
                                                TableCell.InnerText = ""
                                                TableRow.Cells.Add(TableCell)

                                                'Campos personalizados
                                                If childRows.Length > 0 Then
                                                    For i = 4 To childRows(0).Table.Columns.Count - 1
                                                        TableCell = New HtmlTableCell
                                                        TableCell.InnerText = ""
                                                        TableRow.Cells.Add(TableCell)
                                                    Next
                                                End If
                                            Else 'Solo un codigo ERP, al que se asigno
                                                TableCell = New HtmlTableCell
                                                If childRows.Length > 0 Then
                                                    TableCell.InnerText = childRows(0)("DEN_ERP")
                                                End If
                                                TableRow.Cells.Add(TableCell)

                                                'Campos personalizados
                                                If childRows.Length > 0 Then
                                                    For i = 4 To childRows(0).Table.Columns.Count - 1
                                                        TableCell = New HtmlTableCell
                                                        TableCell.InnerText = DBNullToStr(childRows(0)(i))
                                                        TableRow.Cells.Add(TableCell)
                                                    Next
                                                End If
                                            End If
                                        Case Else
                                            If row("NUM_DISTR") > 0 Then
                                                'Si el proveedor tiene proveedores relacionados no se podra editar la celda Codigo ERP y se pondra en blanco, tendra que ir a la seccion de proveedor
                                                TableCell = New HtmlTableCell
                                                TableCell.InnerText = ""
                                                TableRow.Cells.Add(TableCell)

                                                'Campos personalizados
                                                If childRows.Length > 0 Then
                                                    For i = 4 To childRows(0).Table.Columns.Count - 1
                                                        TableCell = New HtmlTableCell
                                                        TableCell.InnerText = ""
                                                        TableRow.Cells.Add(TableCell)
                                                    Next
                                                End If
                                            Else
                                                If childRows.Length >= 1 Then
                                                    'Si hay varios codigos ERP para ese proveedor
                                                    Dim cmbCodERPs As HtmlSelect = CrearComboCodigosERPProveedoresAdjudicadosEmpresa(childRows, fila, idEmp)
                                                    TableCell = New HtmlTableCell
                                                    TableCell.Controls.Add(cmbCodERPs)
                                                    TableRow.Cells.Add(TableCell)

                                                    'Campos personalizados
                                                    If cmbCodERPs.SelectedIndex > 0 Then
                                                        'Si hay un Codigo ERP seleccionado
                                                        For i = 4 To childRows(0).Table.Columns.Count - 1
                                                            TableCell = New HtmlTableCell
                                                            TableCell.InnerText = DBNullToStr(cmbCodERPs.Attributes.Item("CAMPO" & (i - 3).ToString))
                                                            TableRow.Cells.Add(TableCell)
                                                        Next
                                                    Else
                                                        'Si no hay codigo ERP seleccionado
                                                        If childRows.Length > 0 Then
                                                            For i = 3 To childRows(0).Table.Columns.Count - 1
                                                                TableCell = New HtmlTableCell
                                                                TableCell.InnerText = ""
                                                                TableRow.Cells.Add(TableCell)
                                                            Next
                                                        End If
                                                    End If
                                                Else
                                                    TableCell = New HtmlTableCell
                                                    TableCell.InnerText = ""
                                                    TableRow.Cells.Add(TableCell)

                                                    'Campos personalizados
                                                    If childRows.Length > 0 Then
                                                        For i = 4 To childRows(0).Table.Columns.Count - 1
                                                            TableCell = New HtmlTableCell
                                                            TableCell.InnerText = ""
                                                            TableRow.Cells.Add(TableCell)
                                                        Next
                                                    End If
                                                End If
                                            End If
                                    End Select
                                Case Else
                                    If row("NUM_DISTR") > 0 Then
                                        'Si el proveedor tiene proveedores relacionados no se podra editar la celda Codigo ERP y se pondra en blanco, tendra que ir a la seccion de proveedor
                                        TableCell = New HtmlTableCell
                                        TableCell.InnerText = ""
                                        TableRow.Cells.Add(TableCell)

                                        'Campos personalizados
                                        If childRows.Length > 0 Then
                                            For i = 4 To childRows(0).Table.Columns.Count - 1
                                                TableCell = New HtmlTableCell
                                                TableCell.InnerText = ""
                                                TableRow.Cells.Add(TableCell)
                                            Next
                                        End If
                                    Else
                                        If childRows.Length >= 1 Then
                                            'Si hay varios codigos ERP para ese proveedor
                                            Dim cmbCodERPs As HtmlSelect = CrearComboCodigosERPProveedoresAdjudicadosEmpresa(childRows, fila, idEmp)
                                            TableCell = New HtmlTableCell
                                            TableCell.Controls.Add(cmbCodERPs)
                                            TableRow.Cells.Add(TableCell)

                                            'Campos personalizados
                                            If cmbCodERPs.SelectedIndex > 0 Then
                                                'Si hay un Codigo ERP seleccionado
                                                For i = 4 To childRows(0).Table.Columns.Count - 1
                                                    TableCell = New HtmlTableCell
                                                    TableCell.InnerText = DBNullToStr(cmbCodERPs.Attributes.Item("CAMPO" & (i - 3).ToString))
                                                    TableRow.Cells.Add(TableCell)
                                                Next
                                            Else 'Si no hay codigo ERP seleccionado
                                                If childRows.Length > 0 Then
                                                    For i = 4 To childRows(0).Table.Columns.Count - 1
                                                        TableCell = New HtmlTableCell
                                                        TableCell.InnerText = ""
                                                        TableRow.Cells.Add(TableCell)
                                                    Next
                                                End If
                                            End If
                                        Else
                                            TableCell = New HtmlTableCell
                                            TableCell.InnerText = ""
                                            TableRow.Cells.Add(TableCell)
                                        End If
                                    End If
                            End Select
                        End If
                    Case "EST_INTEGRACION", "NUM_DISTR"
                        'Estas columnas no se representan en la tabla
                    Case Else
                        TableCell = New HtmlTableCell
                        TableCell.InnerText = row(Col.ColumnName)
                        TableRow.Cells.Add(TableCell)
                End Select
            Next
            tblProveedoresAdjudicados.Rows.Add(TableRow)
            fila += 1
        Next
    End Sub
    ''' <summary>
    ''' Funcion que devuelve el combo con los codigos ERP de ese proveedor adjudicado
    ''' </summary>
    ''' <param name="childRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CrearComboCodigosERPProveedoresAdjudicadosEmpresa(ByVal childRows() As DataRow, ByVal filaTabla As Short, ByVal idEmp As Long) As HtmlSelect
        Dim combo As New HtmlControls.HtmlSelect
        Dim NumCamposPersonalizados As Byte
        Dim ProveCod As String
        Static Num As Byte

        combo.ID = "cmbCodigoERPProvAdjEmpresa_" & Num
        combo.Style.Add("width", "100%")
        combo.Style.Add("border", "0px")

        Dim item As ListItem
        item = New ListItem
        item.Value = ""
        item.Text = ""
        combo.Items.Add(item)
        For Each row As DataRow In childRows
            item = New ListItem
            item.Value = Trim(row("COD_ERP"))
            item.Text = Trim(row("DEN_ERP"))
            For i = 4 To row.Table.Columns.Count - 1
                item.Attributes.Add("CAMPO" & (i - 3).ToString, DBNullToStr(row(i)))
            Next
            NumCamposPersonalizados = row.Table.Columns.Count - 4 ' Se resta 4 porque son el numero de campos que preceden a los campos personalizados(PROVCOD, PROVDEN, COD_ERP)
            combo.Items.Add(item)
        Next

        'Si solo hay un Codigo de Erp saldra precargado
        If childRows.Length >= 1 Then
            'Se mira si se habia seleccionado un cod Erp en la pantalla de empresa y esta guardado
            ProveCod = childRows(0)("PROVECOD")
            Dim oAdjudicacion As FSNServer.Adjudicacion

            oAdjudicacion = System.Web.HttpContext.Current.Cache("oAdjudicacion_" & FSNUser.Cod)
            Dim CodERPSeleccionado As Boolean
            If Not oAdjudicacion Is Nothing Then
                For Each EmpAdjudicada As FSNServer.EmpresaAdjudicada In oAdjudicacion.EmpresasAdjudicadas
                    If EmpAdjudicada.Id = idEmp Then
                        For Each ProvAdjudicado As FSNServer.ProveedorAdjudicado In EmpAdjudicada.ProveedoresAdjudicados
                            If ProvAdjudicado.Codigo = ProveCod Then
                                If Not combo.Items.FindByValue(ProvAdjudicado.CodErpSeleccionado) Is Nothing Then
                                    combo.Items.FindByValue(ProvAdjudicado.CodErpSeleccionado).Selected = True
                                    CodERPSeleccionado = True
                                End If
                            End If
                        Next
                    End If
                Next
            End If
            'Si no se habia seleccionado previamente en la pantalla empresa, pero solo hay un cod.ERP se seleccionara
            If childRows.Length = 1 AndAlso CodERPSeleccionado = False Then
                combo.Items(1).Selected = True
            End If
            If CodERPSeleccionado Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "CambioCodigoERPProvAdjudicado", "Ejecutar_CambioCodigoERPProvAdjudicado('" & combo.ClientID & "'," & NumCamposPersonalizados & "," & filaTabla & ");", True)
            End If
        End If
        Num += 1
        combo.Attributes.Add("onChange", "javascript:CambioCodigoERPProvAdjudicado(this," & NumCamposPersonalizados & "," & filaTabla & ");")
        combo.Attributes.Add("onFocusin", "javascript:GuardarValorAntiguo(this);")
        Return combo
    End Function
    ''' <summary>
    ''' Carga los proveedores adjudicados del proceso
    ''' </summary>
    ''' <returns>Dataset con los proveedores adjudicados y sus codigos ERP</returns>
    ''' <remarks></remarks>
    Private Function CargarProveedoresAdjudicados(ByVal idEmp As Long, ByVal Anyo As Short, ByVal Gmn1 As String, ByVal CodProce As Long)
        Dim oAdjudicaciones As Fullstep.FSNServer.Adjudicaciones
        oAdjudicaciones = FSNServer.Get_Object(GetType(FSNServer.Adjudicaciones))
        Return oAdjudicaciones.ObtenerProveedoresAdjudicados(Me.FSNUser.IdiomaCod, idEmp, Anyo, Gmn1, CodProce)
    End Function
#End Region
#Region "Panel Proveedor"
    ''' <summary>
    ''' procedimiento que carga el panel del proveedor
    ''' </summary>
    ''' <param name="Codigo">Codigo del proveedor</param>
    ''' <param name="idEmp">id de la empresa</param>
    ''' <param name="DenEmpresa">Denominacion de la empresa</param>
    ''' <remarks></remarks>
    Private Sub CargarProveedor(ByVal Codigo As String, ByVal idEmp As Long, ByVal DenEmpresa As String, ByVal DenProve As String, ByVal bEmpTieneIntegracion As Boolean, ByVal EstIntegracion As Short)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.TraspasoAdjudicaciones

        lblProveedorLiteralCabecera.Text = Textos(51) & " " & DenProve

        If Not HttpContext.Current.Session("ComprobarAdjAnt") Is Nothing Then chkComprobarAdjAntProve.Checked = CType(HttpContext.Current.Session("ComprobarAdjAnt"), Boolean)

        MostrarPanel(Panel.Proveedor)

        CargarDatosProveedorAdjudicado(Codigo, idEmp, FSNUser.IdiomaCod, DenEmpresa, bEmpTieneIntegracion)

        CargarEstadoIntegracionNivelProveedor(bEmpTieneIntegracion, EstIntegracion)

        Dim sScript As String
        sScript = "$(document).ready(function(){"
        sScript += vbCrLf
        sScript += "CargarAtributosProveedor(" & idEmp & ",'" & Codigo & "','" & JSText(DenEmpresa) & "','" & JSText(DenProve) & "') "
        sScript += vbCrLf
        sScript += "CargarDistribuidoresProveedor('" & Codigo & "'," & idEmp & "," & EstIntegracion & ")"
        sScript += vbCrLf
        sScript += "});"
        If Not Me.ClientScript.IsStartupScriptRegistered("jQuery") Then _
            ScriptManager.RegisterStartupScript(upPaneles, Me.GetType, "jQuery", sScript, True)
    End Sub
    ''' <summary>
    ''' Carga el estado de la integracion a nivel de proveedor
    ''' </summary>
    ''' <param name="bEmpTieneIntegracion">True si la empresa tiene integracion con ERP</param>
    ''' <remarks></remarks>
    Private Sub CargarEstadoIntegracionNivelProveedor(ByVal bEmpTieneIntegracion As Boolean, ByVal EstIntegracion As Short)
        Select Case EstIntegracion
            Case EstadoIntegracionProce.ParcialError
                lblEstadoIntegracionProveedor.Text = Textos(71)
                imgEstadoIntegracionProveedor.Src = "~/App_Themes/" & Page.Theme & "/images/Ico_Expirado.gif"
                chkComprobarAdjAntProve.Visible = True
                BtnTrasladarAdjERPDesdeProveedor.Visible = True
                BtnBorrarAdjERPDesdeProveedor.Visible = True
                celdaBtnTrasladarAdjERPDesdeProveedor.Style.Add("width", "49%")
                celdaBtnBorrarAdjERPDesdeProveedor.Style.Add("width", "49%")
            Case EstadoIntegracionProce.TotalError
                lblEstadoIntegracionProveedor.Text = Textos(74)
                imgEstadoIntegracionProveedor.Src = "~/App_Themes/" & Page.Theme & "/images/Ico_Expirado.gif"
                chkComprobarAdjAntProve.Visible = True
                BtnTrasladarAdjERPDesdeProveedor.Visible = True
                BtnBorrarAdjERPDesdeProveedor.Visible = True
                celdaBtnTrasladarAdjERPDesdeProveedor.Style.Add("width", "49%")
                celdaBtnBorrarAdjERPDesdeProveedor.Style.Add("width", "49%")
            Case EstadoIntegracionProce.ParcialEsperaAcuseRecibo
                lblEstadoIntegracionProveedor.Text = Textos(69)
                imgEstadoIntegracionProveedor.Src = "~/App_Themes/" & Page.Theme & "/images/Ico_Opcional.gif"
                chkComprobarAdjAntProve.Visible = False
                BtnTrasladarAdjERPDesdeProveedor.Visible = False
                BtnBorrarAdjERPDesdeProveedor.Visible = False
                celdaBtnTrasladarAdjERPDesdeProveedor.Style.Add("width", "49%")
                celdaBtnBorrarAdjERPDesdeProveedor.Style.Add("width", "49%")
            Case EstadoIntegracionProce.TotalEsperaAcuseRecibo
                lblEstadoIntegracionProveedor.Text = Textos(72)
                imgEstadoIntegracionProveedor.Src = "~/App_Themes/" & Page.Theme & "/images/Ico_Opcional.gif"
                chkComprobarAdjAntProve.Visible = False
                BtnTrasladarAdjERPDesdeProveedor.Visible = False
                BtnBorrarAdjERPDesdeProveedor.Visible = False
                celdaBtnTrasladarAdjERPDesdeProveedor.Style.Add("width", "49%")
                celdaBtnBorrarAdjERPDesdeProveedor.Style.Add("width", "49%")
            Case EstadoIntegracionProce.ParcialOK
                lblEstadoIntegracionProveedor.Text = Textos(70)
                imgEstadoIntegracionProveedor.Src = "~/App_Themes/" & Page.Theme & "/images/Ico_Vigente.gif"
                chkComprobarAdjAntProve.Visible = True
                BtnTrasladarAdjERPDesdeProveedor.Visible = True
                BtnBorrarAdjERPDesdeProveedor.Visible = True
                celdaBtnTrasladarAdjERPDesdeProveedor.Style.Add("width", IIf(Acceso.gbMantEstIntEnTraspasoAdj, "49%", "39%"))
                celdaBtnBorrarAdjERPDesdeProveedor.Style.Add("width", IIf(Acceso.gbMantEstIntEnTraspasoAdj, "49%", "60%"))
                BtnBorrarAdjERPDesdeProveedor.Style.Add("float", "left")
            Case EstadoIntegracionProce.TotalOK
                lblEstadoIntegracionProveedor.Text = Textos(73)
                imgEstadoIntegracionProveedor.Src = "~/App_Themes/" & Page.Theme & "/images/Ico_Vigente.gif"
                BtnTrasladarAdjERPDesdeProveedor.Visible = IIf(Acceso.gbMantEstIntEnTraspasoAdj, True, False)
                chkComprobarAdjAntProve.Visible = BtnTrasladarAdjERPDesdeProveedor.Visible
                BtnBorrarAdjERPDesdeProveedor.Visible = True
                celdaBtnTrasladarAdjERPDesdeProveedor.Style.Add("width", IIf(Acceso.gbMantEstIntEnTraspasoAdj, "49%", "39%"))
                celdaBtnBorrarAdjERPDesdeProveedor.Style.Add("width", IIf(Acceso.gbMantEstIntEnTraspasoAdj, "49%", "60%"))
                BtnBorrarAdjERPDesdeProveedor.Style.Add("float", "left")
            Case Else
                lblEstadoIntegracionProveedor.Text = Textos(10)
                imgEstadoIntegracionProveedor.Src = "~/App_Themes/" & Page.Theme & "/images/Ico_Pendiente.gif"
                chkComprobarAdjAntProve.Visible = True
                BtnTrasladarAdjERPDesdeProveedor.Visible = True
                BtnBorrarAdjERPDesdeProveedor.Visible = False
                celdaBtnTrasladarAdjERPDesdeProveedor.Style.Add("width", "60%")
                celdaBtnBorrarAdjERPDesdeProveedor.Style.Add("width", "39%")
                BtnTrasladarAdjERPDesdeProveedor.Style.Add("float", "left")
        End Select
        If bEmpTieneIntegracion Then
            divProveedorSinIntegracion.Style.Add("display", "none")
            divDatosProveedor.Style.Add("display", "")
            divAtributosProveedor.Style.Add("display", "")
            divDistribuidoresProveedor.Style.Add("display", "")
            celdaBtnTrasladarAdjERPDesdeProveedor.Style.Add("display", "")
            celdaBtnBorrarAdjERPDesdeProveedor.Style.Add("display", "")
        Else
            divProveedorSinIntegracion.Style.Add("display", "")
            divDatosProveedor.Style.Add("display", "none")
            divAtributosProveedor.Style.Add("display", "none")
            divDistribuidoresProveedor.Style.Add("display", "none")
            celdaBtnTrasladarAdjERPDesdeProveedor.Style.Add("display", "none")
            celdaBtnBorrarAdjERPDesdeProveedor.Style.Add("display", "none")
        End If
    End Sub
    ''' <summary>
    ''' Funcion que devuelve el combo con los codigos ERP de ese proveedor adjudicado
    ''' </summary>
    ''' <param name="CodsERP"></param>
    ''' <param name="CodERPSeleccionadoEnEmpresa"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CrearComboCodigosERPProveedorAdjudicadoProveedor(ByVal CodsERP As List(Of FSNServer.ProveedorAdjudicado.CodigoERP), Optional ByVal CodERPSeleccionadoEnEmpresa As String = "") As HtmlSelect
        Dim combo As New HtmlControls.HtmlSelect
        Dim NumCamposPersonalizados As Byte

        combo.ID = "cmbCodigosERPProvAdjProveedor"
        'combo.Style.Add("width", "100%")
        combo.Style.Add("border", "0px")

        Dim item As ListItem
        item = New ListItem
        item.Value = ""
        item.Text = ""
        combo.Items.Add(item)
        For Each CodERP As Fullstep.FSNServer.ProveedorAdjudicado.CodigoERP In CodsERP
            item = New ListItem
            item.Value = Trim(CodERP.CodigoERP)
            item.Text = Trim(CodERP.DenERP)
            For i = 1 To CodERP.NumCamposPersonalizadosAct
                Select Case i
                    Case 1
                        item.Attributes.Add("CAMPO" & (i).ToString, CodERP.DatoCampoPer1)
                        NumCamposPersonalizados = 1
                    Case 2
                        item.Attributes.Add("CAMPO" & (i).ToString, CodERP.DatoCampoPer2)
                        NumCamposPersonalizados = 2
                    Case 3
                        item.Attributes.Add("CAMPO" & (i).ToString, CodERP.DatoCampoPer3)
                        NumCamposPersonalizados = 3
                    Case 4
                        item.Attributes.Add("CAMPO" & (i).ToString, CodERP.DatoCampoPer4)
                        NumCamposPersonalizados = 4
                End Select
            Next
            combo.Items.Add(item)
        Next
        Dim cmbItem As ListItem
        If CodERPSeleccionadoEnEmpresa <> "" Then
            'Se pone el codigo Erp que se selecciono previamente en el panel de empresa
            cmbItem = combo.Items.FindByValue(CodERPSeleccionadoEnEmpresa)
            If Not cmbItem Is Nothing Then
                cmbItem.Selected = True
            End If
        Else
            'solo hay un Codigo de Erp que saldra precargado 
            If CodsERP.Count = 1 Then
                cmbItem = combo.Items(1)
                cmbItem.Selected = True
            End If
        End If

        'Si ya se ha preseleccionado un Codigo Erp del combo se cargan sus campos personalizados
        If CodsERP.Count = 1 Or CodERPSeleccionadoEnEmpresa <> "" Then
            Select Case NumCamposPersonalizados
                Case 1
                    lblCampo1Dato.Text = cmbItem.Attributes("CAMPO1")
                Case 2
                    lblCampo1Dato.Text = cmbItem.Attributes("CAMPO1")
                    lblCampo2Dato.Text = cmbItem.Attributes("CAMPO2")
                Case 3
                    lblCampo1Dato.Text = cmbItem.Attributes("CAMPO1")
                    lblCampo2Dato.Text = cmbItem.Attributes("CAMPO2")
                    lblCampo3Dato.Text = cmbItem.Attributes("CAMPO3")
                Case 4
                    lblCampo1Dato.Text = cmbItem.Attributes("CAMPO1")
                    lblCampo2Dato.Text = cmbItem.Attributes("CAMPO2")
                    lblCampo3Dato.Text = cmbItem.Attributes("CAMPO3")
                    lblCampo4Dato.Text = cmbItem.Attributes("CAMPO4")
            End Select
        End If

        OcultarCamposPersonalizablesNoActivos(NumCamposPersonalizados, Panel.Proveedor, combo)

        If NumCamposPersonalizados > 0 Then
            combo.Attributes.Add("onChange", "javascript:CambioCodigoERPProvAdjudicadoProveedor(this," & NumCamposPersonalizados & ");")
            combo.Attributes.Add("onFocusin", "javascript:GuardarValorAntiguo(this);")
        End If

        Return combo
    End Function
    ''' <summary>
    ''' Procedimiento que oculta los campos personalizables no activos
    ''' </summary>
    ''' <param name="NumCampos">Numero de campos personalizados que estan activos</param>
    ''' <remarks></remarks>
    Private Sub OcultarCamposPersonalizablesNoActivos(ByVal NumCampos As Byte, ByVal Panel As Panel, ByVal combo As HtmlControls.HtmlSelect)
        Select Case Panel
            Case TraspasoAdjudicaciones.Panel.Proveedor
                Select Case NumCampos
                    Case 0
                        divCampoPer1.Style.Add("display", "none")
                        divCampoPer2.Style.Add("display", "none")
                        divCampoPer3.Style.Add("display", "none")
                        divCampoPer4.Style.Add("display", "none")
                    Case 1
                        divCampoPer2.Style.Add("display", "none")
                        divCampoPer3.Style.Add("display", "none")
                        divCampoPer4.Style.Add("display", "none")
                    Case 2
                        divCampoPer3.Style.Add("display", "none")
                        divCampoPer4.Style.Add("display", "none")
                    Case 3
                        divCampoPer4.Style.Add("display", "none")
                End Select
            Case TraspasoAdjudicaciones.Panel.Grupo
                Select Case NumCampos
                    Case 0
                        divCampoPer1Grupo.Style.Add("display", "none")
                        divCampoPer2Grupo.Style.Add("display", "none")
                        divCampoPer3Grupo.Style.Add("display", "none")
                        divCampoPer4Grupo.Style.Add("display", "none")
                    Case 1
                        divCampoPer2Grupo.Style.Add("display", "none")
                        divCampoPer3Grupo.Style.Add("display", "none")
                        divCampoPer4Grupo.Style.Add("display", "none")
                    Case 2
                        divCampoPer3Grupo.Style.Add("display", "none")
                        divCampoPer4Grupo.Style.Add("display", "none")
                    Case 3
                        divCampoPer4Grupo.Style.Add("display", "none")
                End Select
            Case TraspasoAdjudicaciones.Panel.Item
                Select Case NumCampos
                    Case 0
                        divCampoPer1Item.Style.Add("display", "none")
                        divCampoPer2Item.Style.Add("display", "none")
                        divCampoPer3Item.Style.Add("display", "none")
                        divCampoPer4Item.Style.Add("display", "none")
                    Case 1
                        divCampoPer2Item.Style.Add("display", "none")
                        divCampoPer3Item.Style.Add("display", "none")
                        divCampoPer4Item.Style.Add("display", "none")
                    Case 2
                        divCampoPer3Item.Style.Add("display", "none")
                        divCampoPer4Item.Style.Add("display", "none")
                    Case 3
                        divCampoPer4Item.Style.Add("display", "none")
                End Select
        End Select

        If combo.SelectedIndex = 0 Then
            'Si no ha nada preseleccionado en el combo no tiene que aparecer ningun campo personalizado
            Select Case Panel
                Case TraspasoAdjudicaciones.Panel.Proveedor
                    divCampoPer1.Style.Add("display", "none")
                    divCampoPer2.Style.Add("display", "none")
                    divCampoPer3.Style.Add("display", "none")
                    divCampoPer4.Style.Add("display", "none")
                Case TraspasoAdjudicaciones.Panel.Grupo
                    divCampoPer1Grupo.Style.Add("display", "none")
                    divCampoPer2Grupo.Style.Add("display", "none")
                    divCampoPer3Grupo.Style.Add("display", "none")
                    divCampoPer4Grupo.Style.Add("display", "none")
                Case TraspasoAdjudicaciones.Panel.Item
                    divCampoPer1Item.Style.Add("display", "none")
                    divCampoPer2Item.Style.Add("display", "none")
                    divCampoPer3Item.Style.Add("display", "none")
                    divCampoPer4Item.Style.Add("display", "none")
            End Select
        End If
    End Sub
    ''' <summary>
    ''' Carga los datos del proveedor cuando se selecciona en el arbol de adjudicaciones
    ''' </summary>
    ''' <param name="Codigo">codigo del proveedor seleccionado</param>
    ''' <param name="IdEmp">id de la empresa del proceso</param>
    ''' <param name="sIdi">Idioma</param>
    ''' <param name="DenEmpresa">Denominacion de la empresa</param>
    ''' <param name="bEmpTieneIntegracion">Indica si la empresa tiene integracion o no</param>
    ''' <remarks></remarks>
    Private Sub CargarDatosProveedorAdjudicado(ByVal Codigo As String, ByVal IdEmp As Long, ByVal sIdi As String, ByVal DenEmpresa As String, ByVal bEmpTieneIntegracion As Boolean)
        Dim oAdjudicaciones As Fullstep.FSNServer.Adjudicaciones
        oAdjudicaciones = FSNServer.Get_Object(GetType(FSNServer.Adjudicaciones))
        Dim oProvAdjudicado As FSNServer.ProveedorAdjudicado = oAdjudicaciones.ObtenerDatosProveedorAdjudicado(_AnyoProce, _Gmn1Proce, _CodProce, IdEmp, Codigo, sIdi)
        Dim oAdjudicacion As FSNServer.Adjudicacion

        lblCodigoProveedorDato.Text = oProvAdjudicado.Codigo
        lblDenProveedorDato.Text = oProvAdjudicado.Denominacion
        lblEmpresaDatoProveedor.Text = DenEmpresa
        lblDistribuidoresLiteralProveedor.Text = Textos(18)

        Select Case oProvAdjudicado.EstadoIntegracion
            Case EstadoIntegracionProce.TotalOK, EstadoIntegracionProce.ParcialEsperaAcuseRecibo, EstadoIntegracionProce.TotalEsperaAcuseRecibo
                Dim lblCodERP As New Label

                lblCodERP.Text = oProvAdjudicado.CodigosERP(0).DenERP
                divCodigoERPProveedor.Controls.Add(lblCodERP)
                If oProvAdjudicado.CodigosERP.Count > 0 Then
                    Select Case oProvAdjudicado.CodigosERP(0).NumCamposPersonalizadosAct
                        Case 1
                            lblCampo1Dato.Text = oProvAdjudicado.CodigosERP(0).DatoCampoPer1
                        Case 2
                            lblCampo1Dato.Text = oProvAdjudicado.CodigosERP(0).DatoCampoPer1
                            lblCampo2Dato.Text = oProvAdjudicado.CodigosERP(0).DatoCampoPer2
                        Case 3
                            lblCampo1Dato.Text = oProvAdjudicado.CodigosERP(0).DatoCampoPer1
                            lblCampo2Dato.Text = oProvAdjudicado.CodigosERP(0).DatoCampoPer2
                            lblCampo3Dato.Text = oProvAdjudicado.CodigosERP(0).DatoCampoPer3
                        Case 4
                            lblCampo1Dato.Text = oProvAdjudicado.CodigosERP(0).DatoCampoPer1
                            lblCampo2Dato.Text = oProvAdjudicado.CodigosERP(0).DatoCampoPer2
                            lblCampo3Dato.Text = oProvAdjudicado.CodigosERP(0).DatoCampoPer3
                            lblCampo4Dato.Text = oProvAdjudicado.CodigosERP(0).DatoCampoPer4
                    End Select
                End If
            Case Else 'Con estos estados se creara un combo con los distintos codigos ERP que tenga el proveedor
                oAdjudicacion = System.Web.HttpContext.Current.Cache("oAdjudicacion_" & FSNUser.Cod)
                Dim CodERPSeleccionado As String = String.Empty
                If Not oAdjudicacion Is Nothing Then
                    For Each EmpAdjudicada As FSNServer.EmpresaAdjudicada In oAdjudicacion.EmpresasAdjudicadas
                        If EmpAdjudicada.Id = IdEmp Then
                            For Each ProvAdjudicado As FSNServer.ProveedorAdjudicado In EmpAdjudicada.ProveedoresAdjudicados
                                If ProvAdjudicado.Codigo = Codigo Then
                                    CodERPSeleccionado = ProvAdjudicado.CodErpSeleccionado
                                End If
                            Next
                        End If
                    Next
                End If
                divCodigoERPProveedor.Controls.Add(CrearComboCodigosERPProveedorAdjudicadoProveedor(oProvAdjudicado.CodigosERP, CodERPSeleccionado))
        End Select

        If oProvAdjudicado.CodigosERP.Count > 0 Then 'Se ponen los literales de los campos personalizados
            For i = 1 To 4
                Select Case i
                    Case 1
                        If oProvAdjudicado.CodigosERP(0).DenCampoPer1 <> String.Empty Then
                            lblCampo1Literal.Text = oProvAdjudicado.CodigosERP(0).DenCampoPer1 & ": "
                        Else
                            divCampoPer1.Style.Add("display", "none")
                        End If
                    Case 2
                        If oProvAdjudicado.CodigosERP(0).DenCampoPer2 <> String.Empty Then
                            lblCampo2Literal.Text = oProvAdjudicado.CodigosERP(0).DenCampoPer2 & ": "
                        Else
                            divCampoPer2.Style.Add("display", "none")
                        End If
                    Case 3
                        If oProvAdjudicado.CodigosERP(0).DenCampoPer3 <> String.Empty Then
                            lblCampo3Literal.Text = oProvAdjudicado.CodigosERP(0).DenCampoPer3 & ": "
                        Else
                            divCampoPer3.Style.Add("display", "none")
                        End If
                    Case 4
                        If oProvAdjudicado.CodigosERP(0).DenCampoPer4 <> String.Empty Then
                            lblCampo4Literal.Text = oProvAdjudicado.CodigosERP(0).DenCampoPer4 & ": "
                        Else
                            divCampoPer4.Style.Add("display", "none")
                        End If
                End Select
            Next
        End If

    End Sub
#End Region
#Region "Panel Grupo"
    Private Sub CargarGrupo(ByVal codGrupo As String, ByVal DenGrupo As String, ByVal idGrupo As Short, ByVal CodProve As String, ByVal DenProve As String, ByVal idEmp As Long, ByVal DenEmpresa As String, ByVal NifEmpresa As String, ByVal bEmpTieneIntegracion As Boolean, ByVal EstIntegracion As Short)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.TraspasoAdjudicaciones

        lblItemsGrupoLiteralCabecera.Text = Textos(20) & " " & DenGrupo

        If Not HttpContext.Current.Session("ComprobarAdjAnt") Is Nothing Then chkComprobarAdjAnt.Checked = CType(HttpContext.Current.Session("ComprobarAdjAnt"), Boolean)

        CargarEstadoIntegracionNivelGrupo(bEmpTieneIntegracion, EstIntegracion)

        MostrarPanel(Panel.Grupo)

        CargarDatosGrupoSeleccionado(_AnyoProce, _Gmn1Proce, _CodProce, CodProve, DenProve, idEmp, FSNUser.IdiomaCod, DenEmpresa, NifEmpresa, codGrupo)

        CargarItemsGrupo(codGrupo, _AnyoProce, _Gmn1Proce, _CodProce, CodProve, idEmp, bEmpTieneIntegracion,, EstIntegracion)

        Dim sScript As String
        sScript = "$(document).ready(function(){"
        sScript += vbCrLf
        sScript += "ObtenerObjetoAdjudicacion();"
        sScript += vbCrLf
        sScript += "CargarAtributosGrupo(" & idEmp & ",'" & CodProve & "','" & idGrupo & "','" & codGrupo & "','" & JSText(DenEmpresa) & "','" & JSText(DenProve) & "') "
        sScript += vbCrLf
        sScript += "});"
        If Not Me.ClientScript.IsStartupScriptRegistered("jQuery") Then _
            ScriptManager.RegisterStartupScript(upPaneles, Me.GetType, "jQuery", sScript, True)
    End Sub
    ''' <summary>Carga el grid de adjudicaciones anteriores a nivel de empresa</summary>  
    ''' <param name="dsDatos">Dataset con las dajudicaciones</param>
    ''' <remarks>Llamada desde: Page_load</remarks>
    Private Sub CargarAdjsAntEmpresa(ByVal dsDatos As DataSet)
        'Establecer la imagen para el check del header
        Dim bCheckAllChecked As Boolean
        bCheckAllChecked = (dsDatos.Tables(0).Select("SEL=False").Count = 0)

        CreatewhdgAdjAntColumnsYConfiguracion(bCheckAllChecked)

        With whdgAdjAnt
            .DataSource = dsDatos
            .DataBind()
        End With

        upAdjAnt.Update()
        mpeAdjAnt.Show()
    End Sub
    ''' <summary>GestiÃ³n de la paginaciÃ³n del grid de adjudicaciones anteriores</summary>
    ''' <remarks></remarks>
    Private Sub Paginador()
        Dim pagerList As DropDownList = DirectCast(whdgAdjAnt.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("PagerPageList"), DropDownList)
        pagerList.Items.Clear()
        For i As Integer = 1 To whdgAdjAnt.GridView.Behaviors.Paging.PageCount
            pagerList.Items.Add(i.ToString())
        Next
        pagerList.SelectedIndex = whdgAdjAnt.GridView.Behaviors.Paging.PageIndex
        CType(whdgAdjAnt.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblCount"), Label).Text = whdgAdjAnt.GridView.Behaviors.Paging.PageCount

        Dim desactivado As Boolean = ((whdgAdjAnt.GridView.Behaviors.Paging.PageCount = 1) OrElse (whdgAdjAnt.GridView.Behaviors.Paging.PageIndex = 0))
        With CType(whdgAdjAnt.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero" & IIf(desactivado, "_desactivado", "") & ".gif"
            .Enabled = False
        End With
        With CType(whdgAdjAnt.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior" & IIf(desactivado, "_desactivado", "") & ".gif"
            .Enabled = False
        End With
        desactivado = (whdgAdjAnt.GridView.Behaviors.Paging.PageCount = 1)
        With CType(whdgAdjAnt.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(desactivado, "_desactivado", "") & ".gif"
            .Enabled = Not desactivado
        End With
        With CType(whdgAdjAnt.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(desactivado, "_desactivado", "") & ".gif"
            .Enabled = Not desactivado
        End With
    End Sub
    ''' <summary>
    ''' MÃƒÂ©todo que se lanza en el evento InitializeRow (cuando se enlazan los datos del origen de datos con la lÃƒÂ­nea)
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub whdgAdjAnt_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgAdjAnt.InitializeRow
        Dim oData As DataRow = e.Row.DataItem.Item.Row

        e.Row.Items.FindItemByKey("Key_SEL").Text = "<img id='AdjAntChecked_" & oData("ID") & "' src=" & ConfigurationManager.AppSettings("ruta") & "images/baja.gif " & IIf(oData("SEL"), "", "style='display:none;'") & " />" &
            "<img id='AdjAntUnchecked_" & oData("ID") & "' src=" & ConfigurationManager.AppSettings("ruta") & "images/CheckboxUnchecked.gif " & IIf(oData("SEL"), "style='display:none;'", "") & " />"
        e.Row.Items.FindItemByKey("Key_PROCE").Text = e.Row.Items.FindItemByKey("Key_ANYO").Value & "/" & e.Row.Items.FindItemByKey("Key_GMN1_PROCE").Value & "/" & e.Row.Items.FindItemByKey("Key_PROCECOD").Value
        e.Row.Items.FindItemByKey("Key_PROVE").Text = e.Row.Items.FindItemByKey("Key_PROVECOD").Value & " - " & e.Row.Items.FindItemByKey("Key_PROVEDEN").Value
        Dim Valor As Double = DBNullToDbl(e.Row.Items.FindItemByKey("Key_PREC_ADJ").Value)
        e.Row.Items.FindItemByKey("Key_PREC_ADJ").Text = FSNLibrary.FormatNumber(Valor, FSNUser.NumberFormat) & " " & DBNullToStr(oData("MON"))
        Valor = DBNullToDbl(e.Row.Items.FindItemByKey("Key_CANTADJ").Value)
        e.Row.Items.FindItemByKey("Key_CANTADJ").Text = FSNLibrary.FormatNumber(Valor, FSNUser.NumberFormat) & " " & DBNullToStr(oData("UNI"))
    End Sub
    Private Sub whdgAdjAnt_PageIndexChanged(sender As Object, e As Infragistics.Web.UI.GridControls.PagingEventArgs) Handles whdgAdjAnt.PageIndexChanged
        Dim desactivado As Boolean = ((whdgAdjAnt.GridView.Behaviors.Paging.PageCount = 1) OrElse (whdgAdjAnt.GridView.Behaviors.Paging.PageIndex = 0))

        With CType(whdgAdjAnt.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero" & IIf(desactivado, "_desactivado.gif", ".gif")
            .Enabled = Not desactivado
            .CssClass = IIf(desactivado, "", "Link")
        End With
        With CType(whdgAdjAnt.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior" & IIf(desactivado, "_desactivado.gif", ".gif")
            .Enabled = Not desactivado
            .CssClass = IIf(desactivado, "", "Link")
        End With
        desactivado = ((whdgAdjAnt.GridView.Behaviors.Paging.PageCount = 1) OrElse (whdgAdjAnt.GridView.Behaviors.Paging.PageIndex = whdgAdjAnt.GridView.Behaviors.Paging.PageCount - 1))
        With CType(whdgAdjAnt.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(desactivado, "_desactivado.gif", ".gif")
            .Enabled = Not desactivado
            .CssClass = IIf(desactivado, "", "Link")
        End With
        With CType(whdgAdjAnt.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(desactivado, "_desactivado.gif", ".gif")
            .Enabled = Not desactivado
            .CssClass = IIf(desactivado, "", "Link")
        End With
    End Sub
    Private Sub whdgAdjAnt_DataBound(sender As Object, e As System.EventArgs) Handles whdgAdjAnt.DataBound
        Paginador()
    End Sub
    Private Sub whdgAdjAnt_DataFiltered(sender As Object, e As Infragistics.Web.UI.GridControls.FilteredEventArgs) Handles whdgAdjAnt.DataFiltered
        Paginador()
    End Sub
    Private Sub whdgAdjAnt_ColumnSorted(sender As Object, e As Infragistics.Web.UI.GridControls.SortingEventArgs) Handles whdgAdjAnt.ColumnSorted
        Paginador()
    End Sub
    ''' <summary>
    ''' Carga los items del grupo en el grid
    ''' </summary>
    ''' <param name="codGrupo">codigo del grupo seleccionado</param>
    ''' <param name="Anyo">Año del proceso</param>
    ''' <param name="Gmn1">Commodity del proceso</param>
    ''' <param name="CodProce">Codigo del proceso</param>
    ''' <param name="CodProve">Codigo del proveedor seleccionado</param>
    ''' <param name="EstIntegracionGrupo">-1 no se pasa. Eoc Grupo.ProceInt.Estado. Para saber si muestro el bt Trasladar o no lo muestro. Miro CargarEstadoIntegracionNivelProveedor para ver cuando mostrar</param>
    ''' <remarks></remarks>
    Private Sub CargarItemsGrupo(ByVal codGrupo As String, ByVal Anyo As Short, ByVal Gmn1 As String, ByVal CodProce As Long, ByVal CodProve As String, ByVal idEmp As Long, ByVal bEmpTieneIntegracion As Boolean _
                                 , Optional ByVal cambio As Boolean = False, Optional EstIntegracionGrupo As Short = -1)
        Dim oAdjudicaciones As Fullstep.FSNServer.Adjudicaciones
        oAdjudicaciones = FSNServer.Get_Object(GetType(FSNServer.Adjudicaciones))
        Dim dsItemsGrupo As DataSet = oAdjudicaciones.ObtenerDsItemsGrupo(codGrupo, Anyo, Gmn1, CodProce, CodProve, idEmp, FSNUser.NumberFormat, FSNUser.Cod, FSNUser.DateFormat, cambio)
        Dim bTieneEscalados As Boolean = dsItemsGrupo.Tables.Count = 4
        Dim oAdjudicacion As FSNServer.Adjudicacion
        Dim oEmpresa As FSNServer.EmpresaAdjudicada
        Dim oProveAdjudicado As FSNServer.ProveedorAdjudicado
        Dim oGrupo As FSNServer.ProveedorAdjudicado.Grupo
        Dim oGridFilters As List(Of Infragistics.Web.UI.GridControls.ColumnFilter)

        _gEmpTieneIntegracion = bEmpTieneIntegracion

        'Si se ha seleccionado en el combo un codigo ERP habra que actualizar el datatable y los items del objeto adjudicacion
        oAdjudicacion = System.Web.HttpContext.Current.Cache("oAdjudicacion_" & FSNUser.Cod)
        If Not oAdjudicacion Is Nothing Then
            oEmpresa = oAdjudicacion.EmpresasAdjudicadas.Find(Function(o As FSNServer.EmpresaAdjudicada) (o.Id = idEmp))
            If oEmpresa IsNot Nothing Then
                oProveAdjudicado = oEmpresa.ProveedoresAdjudicados.Find(Function(o As FSNServer.ProveedorAdjudicado) (o.Codigo.Trim = CodProve.Trim))

                If oProveAdjudicado IsNot Nothing Then
                    oGrupo = oProveAdjudicado.Grupos.Find(Function(o As FSNServer.ProveedorAdjudicado.Grupo) (o.Codigo.Trim = codGrupo))

                    If oGrupo IsNot Nothing Then
                        'Si se ha seleccionado en el combo un codigo ERP habra que actualizar el datatable y los items del objeto adjudicacion
                        'o si ya se habia entrado previamente al Grupo(Visto=1) habra que actualizar los atributos del datatable
                        If _CodErpGrupoValue <> "" OrElse oGrupo.Visto = 1 OrElse cambio Then
                            For Each oItem As FSNServer.ProveedorAdjudicado.Item In oGrupo.Items
                                Dim drItem As DataRow = (New DataView(dsItemsGrupo.Tables(0), "", "ID_ITEM", DataViewRowState.CurrentRows).FindRows(oItem.Id))(0).Row

                                'Se mirara si se selecciono un codigo erp distinto para el item, sino se mirara el grupo y sino el que se indico en la pantalla de proveedor
                                If _CodErpGrupoValue <> "" Or cambio Then
                                    If oItem.Estado = -1 OrElse oItem.Estado = 3 Then
                                        If oItem.CodERP <> "" Then
                                            drItem("PROVE_ERP") = oItem.CodERP
                                            drItem("DEN_ERP") = oItem.DenERP
                                        Else
                                            If oGrupo.CodErpSeleccionado <> "" Then
                                                oItem.CodERP = oGrupo.CodErpSeleccionado
                                                oItem.DenERP = oGrupo.DenErpSeleccionado
                                                drItem("PROVE_ERP") = oGrupo.CodErpSeleccionado
                                                drItem("DEN_ERP") = oGrupo.DenErpSeleccionado
                                            Else
                                                If oItem.Distribuidores.Count > 0 Then
                                                    oItem.CodERP = oItem.Distribuidores(0).CodErpSeleccionado
                                                    oItem.DenERP = oItem.Distribuidores(0).DenErpSeleccionado
                                                    drItem("PROVE_ERP") = oItem.Distribuidores(0).CodErpSeleccionado
                                                    drItem("DEN_ERP") = oItem.Distribuidores(0).DenErpSeleccionado
                                                Else
                                                    oItem.CodERP = _CodErpGrupoValue
                                                    oItem.DenERP = _CodErpGrupoText
                                                    drItem("PROVE_ERP") = _CodErpGrupoValue
                                                    drItem("DEN_ERP") = _CodErpGrupoText
                                                End If
                                            End If
                                        End If
                                    End If
                                End If

                                'Como a este grupo ya se ha entrado previamente y se podrian haber modificado los atributos de los items,modificamos
                                'el datatable con los datos de los atributos guardados previamente en el objeto Adjudicacion
                                If oGrupo.Visto = 1 Or cambio Then
                                    For Each oAtributo As FSNServer.ProveedorAdjudicado.Atributo In oItem.Atributos
                                        Select Case oAtributo.Tipo
                                            Case 1
                                                drItem("ATRIB_" & oAtributo.Tipo & IIf(oAtributo.Obligatorio, "_1_", "_0_") & oAtributo.Intro & "_" & oAtributo.Codigo) = oAtributo.Valor
                                            Case 2
                                                If oAtributo.Valor Is DBNull.Value OrElse oAtributo.Valor Is Nothing Then
                                                    drItem("ATRIB_" & oAtributo.Tipo & IIf(oAtributo.Obligatorio, "_1_", "_0_") & oAtributo.Intro & "_" & oAtributo.Codigo) = DBNull.Value
                                                ElseIf oAtributo.Intro = 1 Then
                                                    drItem("ATRIB_" & oAtributo.Tipo & IIf(oAtributo.Obligatorio, "_1_", "_0_") & oAtributo.Intro & "_" & oAtributo.Codigo) = oAtributo.Valor
                                                Else
                                                    drItem("ATRIB_" & oAtributo.Tipo & IIf(oAtributo.Obligatorio, "_1_", "_0_") & oAtributo.Intro & "_" & oAtributo.Codigo) = FSNLibrary.FormatNumber(DBNullToDbl(oAtributo.Valor), FSNUser.NumberFormat)
                                                End If
                                            Case 3
                                                If oAtributo.Valor Is DBNull.Value OrElse oAtributo.Valor Is Nothing Then
                                                    drItem("ATRIB_" & oAtributo.Tipo & IIf(oAtributo.Obligatorio, "_1_", "_0_") & oAtributo.Intro & "_" & oAtributo.Codigo) = DBNull.Value
                                                Else
                                                    drItem("ATRIB_" & oAtributo.Tipo & IIf(oAtributo.Obligatorio, "_1_", "_0_") & oAtributo.Intro & "_" & oAtributo.Codigo) = FSNLibrary.FormatDate(oAtributo.Valor, FSNUser.DateFormat)
                                                End If
                                            Case 4
                                                drItem("ATRIB_" & oAtributo.Tipo & IIf(oAtributo.Obligatorio, "_1_", "_0_") & oAtributo.Intro & "_" & oAtributo.Codigo) = oAtributo.Valor
                                        End Select
                                    Next
                                End If
                            Next

                            'Si el grupo está filtrado obtener los datos de los ítems filtrados de la cache
                            If oGrupo.Filtrado Then
                                Dim dsDataCache As DataSet = CType(HttpContext.Current.Cache("dsItems_" & FSNUser.Cod), DataSet)

                                For Each drItem As DataRow In dsItemsGrupo.Tables(0).Rows
                                    If oGrupo.Items.Find(Function(o As FSNServer.ProveedorAdjudicado.Item) (o.Id = drItem("ID_ITEM"))) Is Nothing Then
                                        Dim drItemCache As DataRow = (New DataView(dsDataCache.Tables(0), "", "ID_ITEM", DataViewRowState.CurrentRows).FindRows(drItem("ID_ITEM")))(0).Row
                                        For Each oCol As DataColumn In dsDataCache.Tables(0).Columns
                                            If oCol.ColumnName.StartsWith("ATRIB_") Then
                                                drItem(oCol.ColumnName) = drItemCache(oCol.ColumnName)
                                            End If
                                        Next
                                    End If
                                Next
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If oGrupo IsNot Nothing AndAlso oGrupo.Filtrado Then oGridFilters = New List(Of Infragistics.Web.UI.GridControls.ColumnFilter)
        EliminarColumnas(oGridFilters)
        CreatewhdgItemsColumnsYConfiguracion(bTieneEscalados, dsItemsGrupo.Tables(0), bEmpTieneIntegracion)
        System.Web.HttpContext.Current.Cache.Remove("dsItems_" & FSNUser.Cod)
        Cache.Insert("dsItems_" & FSNUser.Cod, dsItemsGrupo, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin_2"), 0))
		With whdgItems
			.Rows.Clear()
			.GridView.Rows.Clear()
			.DataSource = dsItemsGrupo
			.DataMember = "Items"
			.DataKeyFields = "ID_ITEM,CENTROS,CODART"
		End With

		'Variable con la q se le indica "No me saques bt Trasladar pq el grupo esta Ok/esperaacuse". Lo miro en CargarEstadoIntegracionNivelProveedor
		HttpContext.Current.Session("OcultarBtTrasladar") = False
        If EstIntegracionGrupo = EstadoIntegracionProce.ParcialEsperaAcuseRecibo OrElse EstIntegracionGrupo = EstadoIntegracionProce.TotalEsperaAcuseRecibo Then
            HttpContext.Current.Session("OcultarBtTrasladar") = True
        ElseIf (Not Acceso.gbMantEstIntEnTraspasoAdj) AndAlso (EstIntegracionGrupo = EstadoIntegracionProce.TotalOK) Then
            HttpContext.Current.Session("OcultarBtTrasladar") = True
        End If
        whdgItems.DataBind()

        If oGrupo IsNot Nothing AndAlso oGrupo.Filtrado Then AplicarFiltros(oGridFilters)
    End Sub
    ''' <summary>
    ''' Carga el estado de la integracion a nivel de empresa en el panel del grupo de items
    ''' </summary>
    ''' <param name="bEmpTieneIntegracion">True si la empresa tiene integracion con ERP</param>
    ''' <remarks></remarks>
    Private Sub CargarEstadoIntegracionNivelGrupo(ByVal bEmpTieneIntegracion As Boolean, ByVal EstIntegracion As Short)
        If bEmpTieneIntegracion Then
            divEmpresaSinIntegracionGrupo.Style.Add("display", "none")
            divCamposPerGrupo.Style.Add("display", "")
            divCodigoERPGrupoCont.Style.Add("display", "")
        Else
            divEmpresaSinIntegracionGrupo.Style.Add("display", "")
            divCamposPerGrupo.Style.Add("display", "none")
            divCodigoERPGrupoCont.Style.Add("display", "none")
        End If
        btnTrasladarAdjERP.Style.Add("display", "none")
        divCheckComprobarAdjAnt.Style.Add("display", "none")
        btnBorrarAdjERP.Style.Add("display", "none")
    End Sub
    Private Sub CargarDatosGrupoSeleccionado(ByVal Anyo As Short, ByVal sGmn1 As String, ByVal codProce As Long, ByVal CodProve As String, ByVal DenProve As String, ByVal IdEmp As Long, ByVal sIdi As String, ByVal DenEmpresa As String, ByVal NifEmpresa As String, ByVal codGrupo As String)
        Dim oAdjudicaciones As Fullstep.FSNServer.Adjudicaciones
        oAdjudicaciones = FSNServer.Get_Object(GetType(FSNServer.Adjudicaciones))
        Dim oProvAdjudicado As FSNServer.ProveedorAdjudicado = oAdjudicaciones.ObtenerDatosProveedorAdjudicado(Anyo, sGmn1, codProce, IdEmp, CodProve, sIdi)
        Dim oAdjudicacion As FSNServer.Adjudicacion

        lblProveedorDatoGrupo.Text = CodProve & "-" & DenProve
        lblEmpresaDatoGrupo.Text = NifEmpresa & "-" & DenEmpresa

        oAdjudicacion = System.Web.HttpContext.Current.Cache("oAdjudicacion_" & FSNUser.Cod)
        Dim CodERPSeleccionado As String = String.Empty
        If Not oAdjudicacion Is Nothing Then
            For Each EmpAdjudicada As FSNServer.EmpresaAdjudicada In oAdjudicacion.EmpresasAdjudicadas
                If EmpAdjudicada.Id = IdEmp Then
                    For Each ProvAdjudicado As FSNServer.ProveedorAdjudicado In EmpAdjudicada.ProveedoresAdjudicados
                        If ProvAdjudicado.Codigo = CodProve Then
                            CodERPSeleccionado = ProvAdjudicado.CodErpSeleccionado
                            Exit For
                        End If
                    Next
                End If
                Exit For
            Next
        End If
        divCodigoERPGrupo.Controls.Add(CrearComboCodigosERPProveedorAdjudicadoGrupo(oProvAdjudicado.CodigosERP, IdEmp, CodProve, codGrupo, CodERPSeleccionado))

        If oProvAdjudicado.CodigosERP.Count > 0 Then
            'Se ponen los literales de los campos personalizados
            For i = 1 To 4
                Select Case i
                    Case 1
                        If oProvAdjudicado.CodigosERP(0).DenCampoPer1 <> String.Empty Then
                            lblCampo1LiteralGrupo.Text = oProvAdjudicado.CodigosERP(0).DenCampoPer1 & ": "
                            lblCampo1DatoItem.Text = oProvAdjudicado.CodigosERP(0).DatoCampoPer1
                        Else
                            divCampoPer1Grupo.Style.Add("display", "none")
                        End If
                    Case 2
                        If oProvAdjudicado.CodigosERP(0).DenCampoPer2 <> String.Empty Then
                            lblCampo2LiteralGrupo.Text = oProvAdjudicado.CodigosERP(0).DenCampoPer2 & ": "
                            lblCampo2DatoItem.Text = oProvAdjudicado.CodigosERP(0).DatoCampoPer2
                        Else
                            divCampoPer2Grupo.Style.Add("display", "none")
                        End If
                    Case 3
                        If oProvAdjudicado.CodigosERP(0).DenCampoPer3 <> String.Empty Then
                            lblCampo3LiteralGrupo.Text = oProvAdjudicado.CodigosERP(0).DenCampoPer3 & ": "
                            lblCampo3DatoItem.Text = oProvAdjudicado.CodigosERP(0).DatoCampoPer3
                        Else
                            divCampoPer3Grupo.Style.Add("display", "none")
                        End If
                    Case 4
                        If oProvAdjudicado.CodigosERP(0).DenCampoPer4 <> String.Empty Then
                            lblCampo4LiteralGrupo.Text = oProvAdjudicado.CodigosERP(0).DenCampoPer4 & ": "
                            lblCampo4DatoItem.Text = oProvAdjudicado.CodigosERP(0).DatoCampoPer4
                        Else
                            divCampoPer4Grupo.Style.Add("display", "none")
                        End If
                End Select
            Next
        End If
        'PANEL DE EDICION DEL ITEM
        'Cargo el combo del panel de edicion del item
        divCodERPItem.Controls.Add(CrearComboCodigosERPItem(oProvAdjudicado.CodigosERP, IdEmp, CodProve, codGrupo, CodERPSeleccionado))

        If oProvAdjudicado.CodigosERP.Count > 0 Then
            'Se ponen los literales de los campos personalizados
            For i = 1 To 4
                Select Case i
                    Case 1
                        If oProvAdjudicado.CodigosERP(0).DenCampoPer1 <> String.Empty Then
                            lblCampo1LiteralItem.Text = oProvAdjudicado.CodigosERP(0).DenCampoPer1 & ": "
                        Else
                            divCampoPer1Item.Style.Add("display", "none")
                        End If
                    Case 2
                        If oProvAdjudicado.CodigosERP(0).DenCampoPer2 <> String.Empty Then
                            lblCampo2LiteralItem.Text = oProvAdjudicado.CodigosERP(0).DenCampoPer2 & ": "
                        Else
                            divCampoPer2Item.Style.Add("display", "none")
                        End If
                    Case 3
                        If oProvAdjudicado.CodigosERP(0).DenCampoPer3 <> String.Empty Then
                            lblCampo3LiteralItem.Text = oProvAdjudicado.CodigosERP(0).DenCampoPer3 & ": "
                        Else
                            divCampoPer3Item.Style.Add("display", "none")
                        End If
                    Case 4
                        If oProvAdjudicado.CodigosERP(0).DenCampoPer4 <> String.Empty Then
                            lblCampo4LiteralItem.Text = oProvAdjudicado.CodigosERP(0).DenCampoPer4 & ": "
                        Else
                            divCampoPer4Item.Style.Add("display", "none")
                        End If
                End Select
            Next
        End If

        If Not Acceso.gbUsar_OrgCompras Then
            'Si no se usa la distribucion por Organizacion de compras no se mostrara el combo de centros
            divCentro.Visible = False
        End If
    End Sub
    Private Sub ActualizarDatosTrasPostBack(ByVal Anyo As Short, ByVal sGmn1 As String, ByVal codProce As Long, ByVal CodProve As String, ByVal IdEmp As Long, ByVal sIdi As String, ByVal codGrupo As String, ByVal idGrupo As Integer)
        Dim oAdjudicaciones As Fullstep.FSNServer.Adjudicaciones
        oAdjudicaciones = FSNServer.Get_Object(GetType(FSNServer.Adjudicaciones))
        Dim oProvAdjudicado As FSNServer.ProveedorAdjudicado = oAdjudicaciones.ObtenerDatosProveedorAdjudicado(Anyo, sGmn1, codProce, IdEmp, CodProve, sIdi)
        Dim oAdjudicacion As FSNServer.Adjudicacion
        Dim DenEmpresa, DenProve As String

        oAdjudicacion = System.Web.HttpContext.Current.Cache("oAdjudicacion_" & FSNUser.Cod)
        Dim CodERPSeleccionado As String = String.Empty
        If Not oAdjudicacion Is Nothing Then
            For Each EmpAdjudicada As FSNServer.EmpresaAdjudicada In oAdjudicacion.EmpresasAdjudicadas
                If EmpAdjudicada.Id = IdEmp Then
                    For Each ProvAdjudicado As FSNServer.ProveedorAdjudicado In EmpAdjudicada.ProveedoresAdjudicados
                        If ProvAdjudicado.Codigo = CodProve Then
                            CodERPSeleccionado = ProvAdjudicado.CodErpSeleccionado
                            Exit For
                        End If
                    Next
                End If
                Exit For
            Next
        End If
        divCodigoERPGrupo.Controls.Add(CrearComboCodigosERPProveedorAdjudicadoGrupo(oProvAdjudicado.CodigosERP, IdEmp, CodProve, codGrupo, CodERPSeleccionado))
        divCodERPItem.Controls.Add(CrearComboCodigosERPItem(oProvAdjudicado.CodigosERP, IdEmp, CodProve, codGrupo, CodERPSeleccionado))
        Dim oNode As Infragistics.Web.UI.NavigationControls.DataTreeNode = wdtAdjudicaciones.ActiveNode
        Select Case oNode.Level
            Case 2 'Grupos 
                DenEmpresa = oNode.ParentNode.ParentNode.Text
                DenProve = oNode.ParentNode.Value
        End Select
        Dim sScript As String
        sScript = "$(document).ready(function(){"
        sScript += vbCrLf
        sScript += "ObtenerObjetoAdjudicacion();"
        sScript += vbCrLf
        sScript += "CargarAtributosGrupo(" & IdEmp & ",'" & CodProve & "','" & idGrupo & "','" & codGrupo & "','" & JSText(DenEmpresa) & "','" & JSText(DenProve) & "') "
        sScript += vbCrLf
        sScript += "});"
        If Not Me.ClientScript.IsStartupScriptRegistered("jQuery") Then _
            ScriptManager.RegisterStartupScript(upPaneles, Me.GetType, "jQuery", sScript, True)
    End Sub
    ''' <summary>
    ''' Funcion que devuelve el combo con los codigos ERP de ese proveedor adjudicado
    ''' </summary>
    ''' <param name="CodsERP"></param>
    ''' <param name="CodERPSeleccionadoEnEmpresa"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CrearComboCodigosERPProveedorAdjudicadoGrupo(ByVal CodsERP As List(Of FSNServer.ProveedorAdjudicado.CodigoERP), ByVal idEmp As Short, ByVal codProve As String, ByVal codGrupo As String, Optional ByVal CodERPSeleccionadoEnEmpresa As String = "") As HtmlSelect
        Dim combo As New HtmlControls.HtmlSelect
        Dim NumCamposPersonalizados As Byte

        combo.ID = "cmbCodigosERPProvAdjGrupo"
        'combo.Style.Add("width", "100%")
        combo.Style.Add("border", "0px")

        Dim item As ListItem
        item = New ListItem
        item.Value = ""
        item.Text = ""
        combo.Items.Add(item)
        For Each CodERP As Fullstep.FSNServer.ProveedorAdjudicado.CodigoERP In CodsERP
            item = New ListItem
            item.Value = Trim(CodERP.CodigoERP)
            item.Text = Trim(CodERP.DenERP)
            For i = 1 To CodERP.NumCamposPersonalizadosAct
                Select Case i
                    Case 1
                        item.Attributes.Add("CAMPO" & (i).ToString, CodERP.DatoCampoPer1)
                        NumCamposPersonalizados = 1
                    Case 2
                        item.Attributes.Add("CAMPO" & (i).ToString, CodERP.DatoCampoPer2)
                        NumCamposPersonalizados = 2
                    Case 3
                        item.Attributes.Add("CAMPO" & (i).ToString, CodERP.DatoCampoPer3)
                        NumCamposPersonalizados = 3
                    Case 4
                        item.Attributes.Add("CAMPO" & (i).ToString, CodERP.DatoCampoPer4)
                        NumCamposPersonalizados = 4
                End Select
            Next
            combo.Items.Add(item)
        Next
        _CodErpGrupoValue = String.Empty
        _CodErpGrupoText = String.Empty
        Dim itemSelected As ListItem = Nothing

        'solo hay un Codigo de Erp que saldra precargado 
        If CodsERP.Count = 1 Then
            itemSelected = combo.Items(1)
            itemSelected.Selected = True
            _CodErpGrupoValue = combo.Items(1).Value
            _CodErpGrupoText = combo.Items(1).Text
        Else
            'Si se ha seleccionado previamente en la pantalla proveedor, en la tabla de distribuidores se pondra el codigo Erp del distribuidor principal(que sera el 1º).
            Dim oAdjudicacion As FSNServer.Adjudicacion

            oAdjudicacion = System.Web.HttpContext.Current.Cache("oAdjudicacion_" & FSNUser.Cod)
            Dim bSeleccionado As Boolean = False
            If Not oAdjudicacion Is Nothing Then
                For Each oEmpresa As FSNServer.EmpresaAdjudicada In oAdjudicacion.EmpresasAdjudicadas
                    If oEmpresa.Id = idEmp Then
                        For Each oProveAdjudicado As FSNServer.ProveedorAdjudicado In oEmpresa.ProveedoresAdjudicados
                            If oProveAdjudicado.Codigo.Trim = codProve.Trim Then
                                For Each oGrupo As FSNServer.ProveedorAdjudicado.Grupo In oProveAdjudicado.Grupos
                                    If oGrupo.Codigo = codGrupo Then
                                        'Si ya se habria entrado en el grupo y seleccionado en el combo de la cabecera el codigo Erp pondremos ese
                                        If oGrupo.CodErpSeleccionado <> "" Then
                                            itemSelected = combo.Items.FindByValue(oGrupo.CodErpSeleccionado)
                                            itemSelected.Selected = True
                                            _CodErpGrupoValue = itemSelected.Value
                                            _CodErpGrupoText = itemSelected.Text
                                            bSeleccionado = True
                                        End If
                                        Exit For
                                    End If
                                Next
                                If oProveAdjudicado.Distribuidores.Count > 0 AndAlso bSeleccionado = False Then
                                    'Si no pondremos el que elegimos en la pantalla de proveedores
                                    If oProveAdjudicado.Distribuidores(0).CodErpSeleccionado <> "" Then
                                        itemSelected = combo.Items.FindByValue(oProveAdjudicado.Distribuidores(0).CodErpSeleccionado)
                                        itemSelected.Selected = True
                                        _CodErpGrupoValue = combo.Items.FindByValue(oProveAdjudicado.Distribuidores(0).CodErpSeleccionado).Value
                                        _CodErpGrupoText = combo.Items.FindByValue(oProveAdjudicado.Distribuidores(0).CodErpSeleccionado).Text
                                    End If
                                Else
                                    If CodERPSeleccionadoEnEmpresa <> "" Then
                                        'Se pone el codigo Erp que se selecciono previamente en el panel de empresa
                                        If itemSelected Is Nothing OrElse itemSelected.Selected = False Then
                                            itemSelected = combo.Items.FindByValue(CodERPSeleccionadoEnEmpresa)
                                            itemSelected.Selected = True
                                            _CodErpGrupoValue = combo.Items.FindByValue(CodERPSeleccionadoEnEmpresa).Value
                                            _CodErpGrupoText = combo.Items.FindByValue(CodERPSeleccionadoEnEmpresa).Text
                                        End If
                                    End If
                                End If
                                Exit For
                            End If
                        Next
                        Exit For
                    End If
                Next
            End If
        End If

        'Si ya se ha preseleccionado un Codigo Erp del combo se cargan sus campos personalizados
        If Not itemSelected Is Nothing Then
            Select Case NumCamposPersonalizados
                Case 1
                    lblCampo1DatoGrupo.Text = itemSelected.Attributes("CAMPO1")
                Case 2
                    lblCampo1DatoGrupo.Text = itemSelected.Attributes("CAMPO1")
                    lblCampo2DatoGrupo.Text = itemSelected.Attributes("CAMPO2")
                Case 3
                    lblCampo1DatoGrupo.Text = itemSelected.Attributes("CAMPO1")
                    lblCampo2DatoGrupo.Text = itemSelected.Attributes("CAMPO2")
                    lblCampo3DatoGrupo.Text = itemSelected.Attributes("CAMPO3")
                Case 4
                    lblCampo1DatoGrupo.Text = itemSelected.Attributes("CAMPO1")
                    lblCampo2DatoGrupo.Text = itemSelected.Attributes("CAMPO2")
                    lblCampo3DatoGrupo.Text = itemSelected.Attributes("CAMPO3")
                    lblCampo4DatoGrupo.Text = itemSelected.Attributes("CAMPO4")
            End Select
        End If

        OcultarCamposPersonalizablesNoActivos(NumCamposPersonalizados, Panel.Grupo, combo)

        combo.Attributes.Add("onChange", "javascript:CambioCodigoERPProvAdjudicadoGrupo(this," & NumCamposPersonalizados & ",'" & combo.ClientID & "');")
        combo.Attributes.Add("onFocusin", "javascript:GuardarValorAntiguo(this);")

        Return combo
    End Function
    ''' <summary>
    ''' Funcion que devuelve el combo con los codigos ERP del panel del Item
    ''' </summary>
    ''' <param name="CodsERP"></param>
    ''' <param name="CodERPSeleccionadoEnEmpresa"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CrearComboCodigosERPItem(ByVal CodsERP As List(Of FSNServer.ProveedorAdjudicado.CodigoERP), ByVal idEmp As Short, ByVal codProve As String, ByVal codGrupo As String, Optional ByVal CodERPSeleccionadoEnEmpresa As String = "") As HtmlSelect
        Dim combo As New HtmlControls.HtmlSelect
        Dim NumCamposPersonalizados As Byte

        combo.ID = "cmbCodigosERPItem"
        combo.Style.Add("margin-left", "10px")
        combo.Style.Add("border", "0px")

        Dim item As ListItem
        item = New ListItem
        item.Value = ""
        item.Text = ""
        combo.Items.Add(item)
        For Each CodERP As Fullstep.FSNServer.ProveedorAdjudicado.CodigoERP In CodsERP
            item = New ListItem
            item.Value = CodERP.CodigoERP
            item.Text = CodERP.DenERP
            For i = 1 To CodERP.NumCamposPersonalizadosAct
                Select Case i
                    Case 1
                        item.Attributes.Add("CAMPO" & (i).ToString, CodERP.DatoCampoPer1)
                        NumCamposPersonalizados = 1
                    Case 2
                        item.Attributes.Add("CAMPO" & (i).ToString, CodERP.DatoCampoPer2)
                        NumCamposPersonalizados = 2
                    Case 3
                        item.Attributes.Add("CAMPO" & (i).ToString, CodERP.DatoCampoPer3)
                        NumCamposPersonalizados = 3
                    Case 4
                        item.Attributes.Add("CAMPO" & (i).ToString, CodERP.DatoCampoPer4)
                        NumCamposPersonalizados = 4
                End Select

            Next
            combo.Items.Add(item)
        Next
        Dim itemSelected As ListItem = Nothing
        'solo hay un Codigo de Erp que saldra precargado 
        If CodsERP.Count = 1 Then
            itemSelected = combo.Items(1)
            itemSelected.Selected = True
            _CodErpGrupoValue = combo.Items(1).Value
            _CodErpGrupoText = combo.Items(1).Text
        Else
            'Si se ha seleccionado previamente en la pantalla proveedor, en la tabla de distribuidores se pondra el codigo Erp del distribuidor principal(que sera el 1º).
            Dim oAdjudicacion As FSNServer.Adjudicacion

            oAdjudicacion = System.Web.HttpContext.Current.Cache("oAdjudicacion_" & FSNUser.Cod)
            Dim bSeleccionado As Boolean = False
            If Not oAdjudicacion Is Nothing Then
                For Each oEmpresa As FSNServer.EmpresaAdjudicada In oAdjudicacion.EmpresasAdjudicadas
                    If oEmpresa.Id = idEmp Then
                        For Each oProveAdjudicado As FSNServer.ProveedorAdjudicado In oEmpresa.ProveedoresAdjudicados
                            If oProveAdjudicado.Codigo.Trim = codProve.Trim Then
                                For Each oGrupo As FSNServer.ProveedorAdjudicado.Grupo In oProveAdjudicado.Grupos
                                    If oGrupo.Codigo = codGrupo Then
                                        'Si ya se habria entrado en el grupo y seleccionado en el combo de la cabecera el codigo Erp pondremos ese
                                        If oGrupo.CodErpSeleccionado <> "" Then
                                            itemSelected = combo.Items.FindByValue(oGrupo.CodErpSeleccionado)
                                            itemSelected.Selected = True
                                            _CodErpGrupoValue = itemSelected.Value
                                            _CodErpGrupoText = itemSelected.Text
                                            bSeleccionado = True
                                        End If
                                        Exit For
                                    End If
                                Next
                                If oProveAdjudicado.Distribuidores.Count > 0 AndAlso bSeleccionado = False Then
                                    'Si no pondremos el que elegimos en la pantalla de proveedores
                                    If oProveAdjudicado.Distribuidores(0).CodErpSeleccionado <> "" Then
                                        itemSelected = combo.Items.FindByValue(oProveAdjudicado.Distribuidores(0).CodErpSeleccionado)
                                        itemSelected.Selected = True
                                        _CodErpGrupoValue = combo.Items.FindByValue(oProveAdjudicado.Distribuidores(0).CodErpSeleccionado).Value
                                        _CodErpGrupoText = combo.Items.FindByValue(oProveAdjudicado.Distribuidores(0).CodErpSeleccionado).Text
                                    End If
                                Else
                                    If CodERPSeleccionadoEnEmpresa <> "" Then
                                        'Se pone el codigo Erp que se selecciono previamente en el panel de empresa
                                        If itemSelected Is Nothing OrElse itemSelected.Selected = False Then
                                            itemSelected = combo.Items.FindByValue(CodERPSeleccionadoEnEmpresa)
                                            itemSelected.Selected = True
                                            _CodErpGrupoValue = combo.Items.FindByValue(CodERPSeleccionadoEnEmpresa).Value
                                            _CodErpGrupoText = combo.Items.FindByValue(CodERPSeleccionadoEnEmpresa).Text
                                        End If
                                    End If
                                End If
                                Exit For
                            End If
                        Next
                        Exit For
                    End If
                Next
            End If
        End If

        'Si ya se ha preseleccionado un Codigo Erp del combo se cargan sus campos personalizados
        If CodsERP.Count = 1 Or CodERPSeleccionadoEnEmpresa <> "" Then
            Select Case NumCamposPersonalizados
                Case 1
                    lblCampo1DatoGrupo.Text = combo.Items(1).Attributes("CAMPO1")
                Case 2
                    lblCampo1DatoGrupo.Text = combo.Items(1).Attributes("CAMPO1")
                    lblCampo2DatoGrupo.Text = combo.Items(1).Attributes("CAMPO2")
                Case 3
                    lblCampo1DatoGrupo.Text = combo.Items(1).Attributes("CAMPO1")
                    lblCampo2DatoGrupo.Text = combo.Items(1).Attributes("CAMPO2")
                    lblCampo3DatoGrupo.Text = combo.Items(1).Attributes("CAMPO3")
                Case 4
                    lblCampo1DatoGrupo.Text = combo.Items(1).Attributes("CAMPO1")
                    lblCampo2DatoGrupo.Text = combo.Items(1).Attributes("CAMPO2")
                    lblCampo3DatoGrupo.Text = combo.Items(1).Attributes("CAMPO3")
                    lblCampo4DatoGrupo.Text = combo.Items(1).Attributes("CAMPO4")
            End Select
        End If

        OcultarCamposPersonalizablesNoActivos(NumCamposPersonalizados, Panel.Item, combo)

        If NumCamposPersonalizados > 0 Then
            combo.Attributes.Add("onChange", "javascript:CambioCodigoERPItem(this," & NumCamposPersonalizados & ");")
            combo.Attributes.Add("onFocusin", "javascript:GuardarValorAntiguo(this);")
        End If

        Return combo
    End Function
    ''' <summary>Da el estilo a cada una de las columnas del grid de adjudicaciones anteriores</summary>
    ''' <remarks></remarks>
    Private Sub CreatewhdgAdjAntColumnsYConfiguracion(ByVal bCheckAllChecked As Boolean)
        With whdgAdjAnt
            .Columns.FromKey("Key_PROVE_ERP").Hidden = Not _gEmpTieneIntegracion
            .Columns.FromKey("Key_CENTRO").Hidden = Not Me.Acceso.gbUsar_OrgCompras

            'Checbox cabecera           
            .Columns("Key_SEL").Header.Text = "<img id='AdjCheckAllChecked" & "' src=" & ConfigurationManager.AppSettings("ruta") & "images/baja.gif " & IIf(bCheckAllChecked, "", "style='display:none;'") & " />" &
                                              "<img id='AdjCheckAllUnchecked" & "' src=" & ConfigurationManager.AppSettings("ruta") & "images/CheckboxUnchecked.gif " & IIf(bCheckAllChecked, "style='display:none;'", "") & "/>"
            .GridView.Columns("Key_SEL").Header.Text = .Columns("Key_SEL").Header.Text

            setDateFormat(.Columns.FromKey("Key_FECINI"))
            setDateFormat(.Columns.FromKey("Key_FECFIN"))
            setDateTimeFormat(.Columns.FromKey("Key_FECENV"))
        End With
    End Sub
    ''' <summary>
    ''' Da el estilo a cada una de las columnas del grid de items del grupo, texto de la cebecera, visibilidad....
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CreatewhdgItemsColumnsYConfiguracion(ByVal TieneEscalados As Boolean, ByVal dtItems As DataTable, ByVal bEmpTieneIntegracion As Boolean)
        AddColumnYConfiguracion("ID_ITEM", "DEstado", , 0, , False)
        If bEmpTieneIntegracion Then
            Me.AddColumnYConfiguracion("ESTADOINT_ICO", "", "", 20, -1, True, "", False, True)
            Me.AddColumnYConfiguracion("ESTADOINT_TEXT", Textos(21), , 90)
            Me.AddColumnYConfiguracion("ESTADOINT", "",, 0,, False)
        End If
        AddColumnYConfiguracion("CODART", Textos(22), , 140)
        AddColumnYConfiguracion("DENART", Textos(23), , 160)
        AddColumnYConfiguracion("UON1", "DUON1", , 0, , False)
        AddColumnYConfiguracion("UON2", "DUON2", , 0, , False)
        AddColumnYConfiguracion("UON3", "DUON3", , 0, , False)
        If TieneEscalados Then
            For Each oCol As DataColumn In dtItems.Columns
                If oCol.ColumnName.StartsWith("ESC_") Then
                    Dim NomEscalado() As String = Split(oCol.ColumnName, "_")
                    Dim sNomEscalado As String = String.Empty
                    For i = 1 To NomEscalado.Length - 1
                        If i > 1 Then
                            sNomEscalado += "-" & NomEscalado(i)
                        Else
                            sNomEscalado = NomEscalado(i)
                        End If
                    Next
                    AddColumnYConfiguracion(oCol.ColumnName, sNomEscalado, , 70)
                    _listColsEscalados.Add(oCol.ColumnName)
                End If
            Next
        Else
            AddColumnYConfiguracion("CANT_ADJ", Textos(24), , 90)
            AddColumnYConfiguracion("PREC_ADJ", Textos(25), , 90)
        End If

        RellenaListaAtrib(dtItems)

        AddColumnYConfiguracion("FECINI_SUM", Textos(26), , 70)
        AddColumnYConfiguracion("FECFIN_SUM", Textos(27), , 70)
        If Acceso.gbUsar_OrgCompras Then
            AddColumnYConfiguracion("CENTRO", Textos(28), , 60)
        Else
            AddColumnYConfiguracion("CENTRO", Textos(28), , 0, , False)
        End If
        If bEmpTieneIntegracion Then
            AddUnBoundColumnYConfiguracion("PROVE_ERP", Textos(7), , 160)
        End If
        AddUnBoundColumnYConfiguracion("DISTRIBUIDORES", Textos(18), , 70)
        If bEmpTieneIntegracion Then
            AddUnBoundColumnYConfiguracion("ACCION", Textos(54), , IIf(Acceso.gbMantEstIntEnTraspasoAdj, 80, 60))
        End If
        AddColumnYConfiguracion("USU", Textos(65), , 75)
        AddColumnYConfiguracion("FECENV", Textos(66), , 110)
        setDateFormat(whdgItems.Columns.FromKey("Key_FECINI_SUM"))
        setDateFormat(whdgItems.Columns.FromKey("Key_FECFIN_SUM"))
        setDateTimeFormat(whdgItems.Columns.FromKey("Key_FECENV"))
        confAnchosVisibilidadYPosicion()
    End Sub
    ''' <summary>
    ''' Evento que salta al realizar el filtrado en el grid de items, se fuerza al updatepanel de items a actualizarse
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub whdgItems_DataFiltered(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.FilteredEventArgs) Handles whdgItems.DataFiltered
        'Grupo seleccionado
        Dim nodeSelected As Infragistics.Web.UI.NavigationControls.DataTreeNode = wdtAdjudicaciones.ActiveNode
        Dim oAdjudicacion As FSNServer.Adjudicacion = System.Web.HttpContext.Current.Cache("oAdjudicacion_" & FSNUser.Cod)
        Dim bAddItems As Boolean

        'Actualizar el objeto adjudicación
        Dim oEmp As EmpresaAdjudicada = oAdjudicacion.EmpresasAdjudicadas.Find(Function(o As EmpresaAdjudicada) o.Id = nodeSelected.ParentNode.ParentNode.Key)
        If oEmp IsNot Nothing AndAlso oEmp.ProveedoresAdjudicados IsNot Nothing AndAlso oEmp.ProveedoresAdjudicados.Count > 0 Then
            Dim oProve As ProveedorAdjudicado = oEmp.ProveedoresAdjudicados.Find(Function(o As ProveedorAdjudicado) o.Codigo = nodeSelected.ParentNode.Key)
            If oProve IsNot Nothing AndAlso oProve.Grupos IsNot Nothing AndAlso oProve.Grupos.Count > 0 Then
                Dim oGrupo As ProveedorAdjudicado.Grupo = oProve.Grupos.Find(Function(o As ProveedorAdjudicado.Grupo) o.Codigo = nodeSelected.Key)

                If e.ColumnFilters.Count > 0 Then
                    If oGrupo IsNot Nothing AndAlso oGrupo.Items IsNot Nothing AndAlso oGrupo.Items.Count > 0 Then
                        oGrupo.Filtrado = True

                        Dim oGrid As Infragistics.Web.UI.GridControls.ContainerGrid = CType(sender, Infragistics.Web.UI.GridControls.ContainerGrid)
                        If oGrid.Rows.Count > 0 Then
                            Dim dsData As DataSet = CType(HttpContext.Current.Cache("dsItems_" & FSNUser.Cod), DataSet)

                            'Eliminar del objeto adjudicación las que no estén en el grid
                            For i As Integer = oGrupo.Items.Count - 1 To 0 Step -1
                                Dim oItem As ProveedorAdjudicado.Item = oGrupo.Items.Item(i)
                                If oGrid.Rows.FromKey(New Object() {oItem.Id, oItem.Centro, oItem.Codigo}) Is Nothing Then
                                    'Actualizar el dataset antes de eliminar el item
                                    Dim drItem As DataRow = (New DataView(dsData.Tables(0), "", "ID_ITEM", DataViewRowState.CurrentRows).FindRows(oItem.Id))(0).Row

                                    If _CodErpGrupoValue <> "" Then
                                        If oItem.Estado = -1 OrElse oItem.Estado = 3 Then
                                            If oItem.CodERP <> "" Then
                                                drItem("PROVE_ERP") = oItem.CodERP
                                                drItem("DEN_ERP") = oItem.DenERP
                                            Else
                                                If oGrupo.CodErpSeleccionado <> "" Then
                                                    oItem.CodERP = oGrupo.CodErpSeleccionado
                                                    oItem.DenERP = oGrupo.DenErpSeleccionado
                                                    drItem("PROVE_ERP") = oGrupo.CodErpSeleccionado
                                                    drItem("DEN_ERP") = oGrupo.DenErpSeleccionado
                                                Else
                                                    If oItem.Distribuidores.Count > 0 Then
                                                        oItem.CodERP = oItem.Distribuidores(0).CodErpSeleccionado
                                                        oItem.DenERP = oItem.Distribuidores(0).DenErpSeleccionado
                                                        drItem("PROVE_ERP") = oItem.Distribuidores(0).CodErpSeleccionado
                                                        drItem("DEN_ERP") = oItem.Distribuidores(0).DenErpSeleccionado
                                                    Else
                                                        oItem.CodERP = _CodErpGrupoValue
                                                        oItem.DenERP = _CodErpGrupoText
                                                        drItem("PROVE_ERP") = _CodErpGrupoValue
                                                        drItem("DEN_ERP") = _CodErpGrupoText
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                    For Each oAtributo As FSNServer.ProveedorAdjudicado.Atributo In oItem.Atributos
                                        Select Case oAtributo.Tipo
                                            Case 1
                                                drItem("ATRIB_" & oAtributo.Tipo & IIf(oAtributo.Obligatorio, "_1_", "_0_") & oAtributo.Intro & "_" & oAtributo.Codigo) = oAtributo.Valor
                                            Case 2
                                                If oAtributo.Valor Is DBNull.Value OrElse oAtributo.Valor Is Nothing Then
                                                    drItem("ATRIB_" & oAtributo.Tipo & IIf(oAtributo.Obligatorio, "_1_", "_0_") & oAtributo.Intro & "_" & oAtributo.Codigo) = DBNull.Value
                                                ElseIf oAtributo.Intro = 1 Then
                                                    drItem("ATRIB_" & oAtributo.Tipo & IIf(oAtributo.Obligatorio, "_1_", "_0_") & oAtributo.Intro & "_" & oAtributo.Codigo) = oAtributo.Valor
                                                Else
                                                    drItem("ATRIB_" & oAtributo.Tipo & IIf(oAtributo.Obligatorio, "_1_", "_0_") & oAtributo.Intro & "_" & oAtributo.Codigo) = FSNLibrary.FormatNumber(DBNullToDbl(oAtributo.Valor), FSNUser.NumberFormat)
                                                End If
                                            Case 3
                                                If oAtributo.Valor Is DBNull.Value OrElse oAtributo.Valor Is Nothing Then
                                                    drItem("ATRIB_" & oAtributo.Tipo & IIf(oAtributo.Obligatorio, "_1_", "_0_") & oAtributo.Intro & "_" & oAtributo.Codigo) = DBNull.Value
                                                Else
                                                    drItem("ATRIB_" & oAtributo.Tipo & IIf(oAtributo.Obligatorio, "_1_", "_0_") & oAtributo.Intro & "_" & oAtributo.Codigo) = FSNLibrary.FormatDate(oAtributo.Valor, FSNUser.DateFormat)
                                                End If
                                            Case 4
                                                drItem("ATRIB_" & oAtributo.Tipo & IIf(oAtributo.Obligatorio, "_1_", "_0_") & oAtributo.Intro & "_" & oAtributo.Codigo) = oAtributo.Valor
                                        End Select
                                    Next

                                    'Eliminar el item del objeto adjudicacion
                                    oGrupo.Items.Remove(oItem)
                                End If
                            Next

                            bAddItems = True
                        Else
                            oGrupo.Items.Clear()
                        End If
                    Else
                        oGrupo.Filtrado = False
                        bAddItems = True
                    End If

                    If bAddItems Then
                        'Añadir al objeto adjudicación las que estÃ©n en el grid y no en el objeto adjudicación
                        Dim oGrid As Infragistics.Web.UI.GridControls.ContainerGrid = CType(sender, Infragistics.Web.UI.GridControls.ContainerGrid)
                        Dim dsItems As DataSet = CType(HttpContext.Current.Cache("dsItems_" & FSNUser.Cod), DataSet)
                        For i As Integer = 0 To oGrid.Rows.Count - 1
                            Dim oRow As DataRow = CType(CType((oGrid.Rows.Item(i)).DataItem, Infragistics.Web.UI.Framework.Data.DataSetNode).Item, System.Data.DataRowView).Row
                            If oGrupo.Items.Find(Function(o As FSNServer.ProveedorAdjudicado.Item) (o.Id = oRow("ID_ITEM"))) Is Nothing Then
                                Dim oAdjudicaciones As Fullstep.FSNServer.Adjudicaciones = FSNServer.Get_Object(GetType(FSNServer.Adjudicaciones))
                                If dsItems.Tables.Count = 4 Then
                                    oAdjudicaciones.AgregarItemEscaladoAGrupo(oGrupo, oRow, _AnyoProce, _Gmn1Proce, _CodProce, oProve.Codigo, dsItems, FSNUser.NumberFormat, False, False)
                                Else
                                    oAdjudicaciones.AgregarItemAGrupo(oGrupo, oRow, _AnyoProce, _Gmn1Proce, _CodProce, oProve.Codigo, dsItems, False, False)
                                End If
                            End If
                        Next
                    End If
                End If
            End If
        End If

        upItems.Update()
    End Sub
    ''' <summary>
    ''' Método que se lanza en el evento InitializeRow (cuando se enlazan los datos del origen de datos con la línea)cge"
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub whdgItems_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgItems.InitializeRow
        If e.Row.Items.Count = 0 Then Exit Sub

        Dim oData As DataRow = e.Row.DataItem.Item.Row
        Dim idItem As String = DBNullToStr(oData("ID_ITEM"))
        Dim Centro As String = DBNullToStr(oData("CENTRO"))
        Dim CodERP As String = DBNullToStr(oData("PROVE_ERP"))
        Dim DenERP As String = DBNullToStr(oData("DEN_ERP"))
        Dim UsuTraspaso As String = DBNullToStr(oData("USU"))
        Dim codArt As String = DBNullToStr(oData("CODART"))
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.TraspasoAdjudicaciones
        e.Row.CssClass = "CssAncho"
        e.Row.Items.FindItemByKey("Key_DENART").Tooltip = DBNullToStr(oData("DENART"))
        e.Row.Items.FindItemByKey("Key_PROVE_ERP").Value = CodERP
        If DenERP <> "" Then
            If DenERP.Contains(CodERP & " - ") Then
                DenERP = DenERP.Substring(CodERP.Length + 3)
            End If
            e.Row.Items.FindItemByKey("Key_PROVE_ERP").Text = CodERP & " - " & DenERP
            e.Row.Items.FindItemByKey("Key_PROVE_ERP").Tooltip = CodERP & " - " & DenERP
        Else
            e.Row.Items.FindItemByKey("Key_PROVE_ERP").Text = DenERP
        End If

        If DBNullToStr(oData("DISTRIBUIDOR")) = "1" Then
            e.Row.Items.FindItemByKey("Key_DISTRIBUIDORES").Column.Hidden = False
            e.Row.Items.FindItemByKey("Key_DISTRIBUIDORES").Column.CssClass = "cssDistribuidores"
            e.Row.Items.FindItemByKey("Key_DISTRIBUIDORES").Text = "<div id='divDistrItem_" & idItem & "_" & Centro & "' style='background-color:#E0E0E0 ; width:97%; border:solid 1px #989898; height:18px ;float:left;text-align:center;cursor:pointer;'></div>"
        Else
            e.Row.Items.FindItemByKey("Key_DISTRIBUIDORES").Column.Hidden = True
        End If
        If oData("ESCALADOS") = 0 Then
            Dim Valor As Double = DBNullToDbl(e.Row.Items.FindItemByKey("Key_PREC_ADJ").Value)
            e.Row.Items.FindItemByKey("Key_PREC_ADJ").Text = FSNLibrary.FormatNumber(Valor, FSNUser.NumberFormat) & " " & DBNullToStr(oData("MON"))
            Valor = DBNullToDbl(e.Row.Items.FindItemByKey("Key_CANT_ADJ").Value)
            e.Row.Items.FindItemByKey("Key_CANT_ADJ").Text = FSNLibrary.FormatNumber(Valor, FSNUser.NumberFormat) & " " & DBNullToStr(oData("UNI"))
        Else
            For i = 0 To _listColsEscalados.Count - 1
                Dim Prec As Double = DBNullToDbl(e.Row.Items.FindItemByKey("Key_" & _listColsEscalados(i)).Value)
                Dim sPrec As String = FSNLibrary.FormatNumber(Prec, FSNUser.NumberFormat)
                e.Row.Items.FindItemByKey("Key_" & _listColsEscalados(i)).Text = sPrec & " " & DBNullToStr(oData("MON"))
                If _CabeceraEscalados = False Then
                    e.Row.Items.FindItemByKey("Key_" & _listColsEscalados(i)).Column.Header.Text = e.Row.Items.FindItemByKey("Key_" & _listColsEscalados(i)).Column.Header.Text & " " & oData("UNI")
                End If
            Next
            _CabeceraEscalados = True 'Ya se ha puesto la cabecera a las columnas de escalados con el primer item y no se volvera a hacer en el resto de items
        End If

        If _listColsAtributos.Count = 0 Then
            If Not HttpContext.Current.Cache("dsItems_" & FSNUser.Cod) Is Nothing Then RellenaListaAtrib(CType(Cache("dsItems_" & FSNUser.Cod), DataSet).Tables(0))
        End If

        'Ponemos el texto SI o NO a los atributos booleanos
        For i = 0 To _listColsAtributos.Count - 1
            Dim ColName As String = _listColsAtributos(i)
            Dim arrCol() As String = Split(ColName, "_")
            Dim Tipo As Byte = arrCol(1)
            Dim Intro As Byte = arrCol(3)
            If Tipo = 4 AndAlso Not IsDBNull(e.Row.Items.FindItemByKey("Key_" & _listColsAtributos(i)).Value) Then 'El atributos es booleano
                If e.Row.Items.FindItemByKey("Key_" & _listColsAtributos(i)).Value = "1" Then
                    e.Row.Items.FindItemByKey("Key_" & _listColsAtributos(i)).Text = Textos(43) 'si
                ElseIf e.Row.Items.FindItemByKey("Key_" & _listColsAtributos(i)).Value = "0" Then
                    e.Row.Items.FindItemByKey("Key_" & _listColsAtributos(i)).Text = Textos(44) 'no
                Else
                    e.Row.Items.FindItemByKey("Key_" & _listColsAtributos(i)).Text = "" 'vacio
                End If
            ElseIf Tipo = 2 AndAlso Not IsDBNull(e.Row.Items.FindItemByKey("Key_" & _listColsAtributos(i)).Value) Then 'El atributos es numero
                If Not (Intro = 1) Then
                    e.Row.Items.FindItemByKey("Key_" & _listColsAtributos(i)).Text = FSNLibrary.FormatNumber(DBNullToDbl(e.Row.Items.FindItemByKey("Key_" & _listColsAtributos(i)).Value), FSNUser.NumberFormat)
                End If
            End If
        Next

        'Usuario del traspaso
        e.Row.Items.FindItemByKey("Key_USU").Text = "<span id='span_UsuItem_" & idItem.ToString & "_" & Centro & "' style='padding-left:3px'>" & UsuTraspaso & "</span>"

        Dim EstadoInt As Short
        If oData("ESTADOINT") Is DBNull.Value Then
            EstadoInt = -1
        Else
            EstadoInt = DBNullToInteger(oData("ESTADOINT"))
        End If
        If _gEmpTieneIntegracion Then
            Select Case EstadoInt 'Dependiendo del estado de integracion del item pondremos una accion u otra
                Case -1
                    'pendiente
                    e.Row.Items.FindItemByKey("Key_ACCION").Text = " <img id='img_Accion_" & idItem.ToString & "_" & Centro & "' onclick=""PrevioTraspasoERPItem(" & idItem & ",false,'" & Centro & "','" & codArt & "'," & EstadoInt & ")"" src='" & ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/TraspasoERP.PNG' style='text-align:center;cursor:pointer;' />"
                    e.Row.Items.FindItemByKey("Key_ESTADOINT_ICO").Text = " <img id='img_EstadoItem_" & idItem.ToString & "_" & Centro & "' src='" & ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/Ico_Pendiente_18.gif' style='text-align:center;cursor:pointer; vertical-align:middle' />"
                    e.Row.Items.FindItemByKey("Key_ESTADOINT_TEXT").Text = " <span id='span_EstadoItem_" & idItem.ToString & "_" & Centro & "' style='color:orange; padding-left:3px'>" & Textos(8) & "</span>"
                    btnTrasladarAdjERP.Style.Add("display", "inline")
                    divCheckComprobarAdjAnt.Style.Add("display", "inline")
                Case 0, 1
                    '0:grabado en LOG_ITEM_ADJ, 1:grabado en LOG_ITEM_ADJ, se ha enviado la informacion y se espera el acuse de recibo
                    e.Row.Items.FindItemByKey("Key_ACCION").Text = ""
                    e.Row.Items.FindItemByKey("Key_ESTADOINT_ICO").Text = " <img id='img_EstadoItem_" & idItem.ToString & "_" & Centro & "'  src='" & ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/Ico_Opcional_18.gif' style='text-align:center;cursor:pointer; vertical-align:middle' />"
                    e.Row.Items.FindItemByKey("Key_ESTADOINT_TEXT").Text = " <span id='span_EstadoItem_" & idItem.ToString & "_" & Centro & "' style='padding-left:3px'>" & Textos(33) & "</span>"
                Case 3
                    'error                    
                    e.Row.Items.FindItemByKey("Key_ACCION").Text = " <img id='img_Accion_" & idItem.ToString & "_" & Centro & "' onclick=""PrevioTraspasoERPItem(" & idItem & ",false,'" & Centro & "','" & codArt & "'," & EstadoInt & ")""  src='" & ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/TraspasoERP.PNG' style='text-align:center;cursor:pointer;' />"
                    e.Row.Items.FindItemByKey("Key_ESTADOINT_ICO").Text = " <img id='img_EstadoItem_" & idItem.ToString & "_" & Centro & "' onclick='MostrarPanel(this," & DBNullToInteger(oData("LIA_ID")) & ")' src='" & ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/Ico_Expirado_18.gif' style='text-align:center;cursor:pointer; vertical-align:middle' />"
                    e.Row.Items.FindItemByKey("Key_ESTADOINT_TEXT").Text = " <span id='span_EstadoItem_" & idItem.ToString & "_" & Centro & "' onclick='MostrarPanel(this," & DBNullToInteger(oData("LIA_ID")) & ")'" & "title='" & null2str(oData("DESC_ERROR")) & "' " & "style='color:red; padding-left:3px'>" & Textos(32) & "</span>"
                    btnTrasladarAdjERP.Style.Add("display", "inline")
                    divCheckComprobarAdjAnt.Style.Add("display", "inline")
                Case 4
                    'Correcto
                    Dim sColText As String = " <img id='img_Accion_" & idItem.ToString & "_" & Centro & "' onclick=""PrevioTraspasoERPItem(" & idItem & ",true,'" & Centro & "','" & codArt & "')""  src='" & ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/eliminar.PNG' style='text-align:center;cursor:pointer;' />"
                    If Acceso.gbMantEstIntEnTraspasoAdj Then sColText = " <img id='img_Accion_" & idItem.ToString & "_" & Centro & "' onclick=""PrevioTraspasoERPItem(" & idItem & ",false,'" & Centro & "','" & codArt & "'," & EstadoInt & ")""  src='" & ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/TraspasoERP.PNG" & "' style='text-align:center;cursor:pointer;' />" & sColText
                    e.Row.Items.FindItemByKey("Key_ACCION").Text = sColText
                    e.Row.Items.FindItemByKey("Key_ESTADOINT_ICO").Text = " <img id='img_EstadoItem_" & idItem.ToString & "_" & Centro & "' src='" & ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/Ico_Vigente_18.gif' style='text-align:center;cursor:pointer; vertical-align:middle' />"
                    e.Row.Items.FindItemByKey("Key_ESTADOINT_TEXT").Text = " <span id='span_EstadoItem_" & idItem.ToString & "_" & Centro & "' " & "title='" & Textos(86).Replace("@FECHA", null2str(oData("FECENV_OK"))) & "' " & "style='color:green; padding-left:3px'>" & Textos(31) & "</span>"
                    If Not HttpContext.Current.Session("OcultarBtTrasladar") Then
                        btnTrasladarAdjERP.Style.Add("display", "inline")
                        divCheckComprobarAdjAnt.Style.Add("display", "inline")
                    End If
                    btnBorrarAdjERP.Style.Add("display", "inline")
            End Select
            If btnTrasladarAdjERP.Style("display") = "inline" AndAlso btnBorrarAdjERP.Style("display") = "inline" Then
                CeldabtnTrasladar.Style.Add("width", "49%")
                CeldabtnBorrar.Style.Add("width", "49%")
                btnTrasladarAdjERP.Style.Add("float", "right")
            ElseIf btnTrasladarAdjERP.Style("display") = "inline" AndAlso btnBorrarAdjERP.Style("display") = "none" Then
                CeldabtnTrasladar.Style.Add("width", "60%")
                CeldabtnBorrar.Style.Add("width", "39%")
                btnTrasladarAdjERP.Style.Add("float", "left")
            Else
                CeldabtnTrasladar.Style.Add("width", "39%")
                CeldabtnBorrar.Style.Add("width", "60%")
                btnBorrarAdjERP.Style.Add("float", "left")
            End If
        Else
            filaBotonesTrasladar.Style.Add("display", "none")
        End If
    End Sub
    ''' <summary>
    ''' Función que configura la columna del grid poniendo el texto en la cabecera y mostrando u ocultando la columna
    ''' </summary>
    ''' <param name="fieldName">Nombre de la columna</param>
    ''' <param name="headerText">Texto para la cabecera</param>
    ''' <param name="headerTooltip"></param>
    ''' <param name="Visible">True si es visible y False si se oculta</param>
    ''' <remarks></remarks>
    Private Sub EditColumnYConfiguracion(ByVal fieldName As String, ByVal headerText As String, Optional ByVal headerTooltip As String = "", Optional ByVal Visible As Boolean = True, Optional ByVal Ancho As Short = 0, Optional ByVal posicion As Short = -1)
        'Configurar el grid
        If Not whdgItems.GridView.Columns.Item("Key_" & fieldName) Is Nothing Then
            With whdgItems.GridView.Columns.Item("Key_" & fieldName)
                .Header.Text = headerText
                If headerTooltip = String.Empty Then headerTooltip = headerText
                .Header.Tooltip = headerTooltip
                .Hidden = Not Visible
                .Width = Unit.Pixel(Ancho)
            End With
            If whdgItems.Columns.Item("Key_" & fieldName) IsNot Nothing Then
                whdgItems.Columns.Item("Key_" & fieldName).Header.Text = headerText
                whdgItems.Columns.Item("Key_" & fieldName).Hidden = Not Visible
                If headerTooltip = String.Empty Then headerTooltip = headerText
                whdgItems.Columns.Item("Key_" & fieldName).Header.Tooltip = headerTooltip
                whdgItems.Columns.Item("Key_" & fieldName).Width = Unit.Pixel(Ancho)
            End If
        End If
        If _listPosicionColumns.IndexOf("Key_" & fieldName) = -1 Then
            _listPosicionColumns.Add("Key_" & fieldName)
        End If
    End Sub
    ''' <summary>
    ''' Función que añade una columna al control whdgItems, vinculada al origen de datos de éste.
    ''' </summary>
    ''' <param name="fieldName">Nombre del campo en el origen de datos</param>
    ''' <param name="headerText">Título de la columna</param>
    ''' <param name="headerTooltip">Tooltip de la columna</param>
    ''' <remarks>Llamada desde CreatewhdgEntregasColumns. Máx. 0,1 seg.</remarks>
    Private Sub AddColumnYConfiguracion(ByVal fieldName As String, ByVal headerText As String, Optional ByVal headerTooltip As String = "", Optional ByVal Ancho As Byte = Nothing, Optional ByVal posicion As Short = -1,
                                                    Optional ByVal visible As Boolean = True, Optional ByVal optDataFieldName As String = "", Optional ByVal SeleccionableCSS As Boolean = True, Optional ByVal EsIcono As Boolean = False)
        Try
            If Not EsIcono Then
                Dim field As New Infragistics.Web.UI.GridControls.BoundDataField(True)
                field.Key = "Key_" + fieldName
                If optDataFieldName <> "" Then
                    field.DataFieldName = optDataFieldName
                Else
                    field.DataFieldName = fieldName
                End If
                If SeleccionableCSS Then
                    field.CssClass = "itemSeleccionable"
                End If
                field.Header.Text = headerText
                If Not Ancho = Nothing Then
                    field.Width = Unit.Pixel(Ancho)
                End If

                Dim a As New Infragistics.Web.UI.GridControls.ColumnEditSetting()
                'Dim b As Infragistics.Web.UI.GridControls.GridBehavior
                If fieldName = "CANT_ADJ" Then
                    If field.DataType = "" Then field.DataType = "System.Double"
                    a.EditorID = "NumericEditor"
                    a.ColumnKey = "CANT_ADJ"
                    whdgItems.Behaviors.EditingCore.Behaviors.CellEditing.ColumnSettings.Add(a)
                Else
                    If fieldName = "PREC_ADJ" Then field.DataType = "System.Double"
                    Dim column As Infragistics.Web.UI.GridControls.EditingColumnSetting
                    column = New Infragistics.Web.UI.GridControls.EditingColumnSetting
                    column.ColumnKey = field.Key
                    column.ReadOnly = True
                    whdgItems.GridView.Behaviors.EditingCore.Behaviors.CellEditing.ColumnSettings.Add(column)
                End If

                field.Hidden = Not visible
                If headerTooltip = String.Empty Then headerTooltip = headerText
                field.Header.Tooltip = headerTooltip
                If whdgItems.Columns IsNot Nothing AndAlso Me.whdgItems.Columns(field.Key) Is Nothing Then
                    whdgItems.Columns.Add(field)
                End If

                If whdgItems.GridView.Columns IsNot Nothing AndAlso Me.whdgItems.GridView.Columns(field.Key) Is Nothing Then
                    whdgItems.GridView.Columns.Add(field)
                End If
            Else
                Dim field As New Infragistics.Web.UI.GridControls.UnboundField(True)
                With field
                    .Key = "Key_" + fieldName
                    .Header.Text = ""
                    .Header.Tooltip = ""
                    .Header.CssClass = "celdaImagenWebHierarchical headerNoWrap"
                    .Width = Unit.Pixel(20)
                    .Hidden = False
                    .CssClass = "celdaImagenWebHierarchical SinSalto" '"celdaImagenWebHierarchical itemSeleccionable SinSalto"
                End With
                If whdgItems.Columns IsNot Nothing AndAlso Me.whdgItems.Columns(field.Key) Is Nothing Then
                    whdgItems.Columns.Add(field)
                End If

                If whdgItems.GridView.Columns IsNot Nothing AndAlso Me.whdgItems.GridView.Columns(field.Key) Is Nothing Then
                    whdgItems.GridView.Columns.Add(field)
                End If

                Dim fieldFilter As Infragistics.Web.UI.GridControls.ColumnFilteringSetting
                fieldFilter = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
                fieldFilter.ColumnKey = field.Key
                fieldFilter.Enabled = False
                fieldFilter.BeforeFilterAppliedAltText = Textos(72) 'Filtro no aplicado
                whdgItems.Behaviors.Filtering.ColumnSettings.Add(fieldFilter)
                whdgItems.GridView.Behaviors.Filtering.ColumnSettings.Add(fieldFilter)

                Dim column As Infragistics.Web.UI.GridControls.EditingColumnSetting
                column = New Infragistics.Web.UI.GridControls.EditingColumnSetting
                column.ColumnKey = field.Key
                column.ReadOnly = True
                whdgItems.GridView.Behaviors.EditingCore.Behaviors.CellEditing.ColumnSettings.Add(column)

            End If
            If _listPosicionColumns.IndexOf("Key_" & fieldName) = -1 Then _listPosicionColumns.Add("Key_" & fieldName)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Función que añade una columna al control whdgItems, vinculada al origen de datos de éste.
    ''' </summary>
    ''' <param name="fieldName">Nombre del campo en el origen de datos</param>
    ''' <param name="headerText">Título de la columna</param>
    ''' <param name="headerTooltip">Tooltip de la columna</param>
    ''' <remarks>Llamada desde CreatewhdgEntregasColumns. Máx. 0,1 seg.</remarks>
    Private Sub AddUnBoundColumnYConfiguracion(ByVal fieldName As String, ByVal headerText As String, Optional ByVal headerTooltip As String = "", Optional ByVal Ancho As Byte = 0, Optional ByVal posicion As Short = -1, Optional ByVal visible As Boolean = True)
        Dim field As New Infragistics.Web.UI.GridControls.UnboundField(True)
        field.Key = "Key_" + fieldName
        field.Header.Text = headerText
        field.Width = Unit.Pixel(Ancho)
        field.Hidden = Not visible
        If headerTooltip = String.Empty Then headerTooltip = headerText
        field.Header.Tooltip = headerTooltip
        If whdgItems.Columns IsNot Nothing AndAlso Me.whdgItems.Columns(field.Key) Is Nothing Then
            whdgItems.Columns.Add(field)
        End If

        If whdgItems.GridView.Columns IsNot Nothing AndAlso Me.whdgItems.GridView.Columns(field.Key) Is Nothing Then
            whdgItems.GridView.Columns.Add(field)
        End If

        Dim column As Infragistics.Web.UI.GridControls.EditingColumnSetting
        column = New Infragistics.Web.UI.GridControls.EditingColumnSetting
        column.ColumnKey = field.Key
        column.ReadOnly = True
        whdgItems.GridView.Behaviors.EditingCore.Behaviors.CellEditing.ColumnSettings.Add(column)

        _listPosicionColumns.Add("Key_" & fieldName)
    End Sub
    Private Sub EliminarColumnas(ByRef oGridFilters As List(Of Infragistics.Web.UI.GridControls.ColumnFilter))
        If oGridFilters IsNot Nothing Then
            For Each oColFilter As Infragistics.Web.UI.GridControls.ColumnFilter In whdgItems.GridView.Behaviors.Filtering.ColumnFilters
                Dim oNewColFilter As Infragistics.Web.UI.GridControls.ColumnFilter = oColFilter.Clone
                oGridFilters.Add(oColFilter)
            Next
        End If

        whdgItems.Behaviors.Filtering.ColumnSettings.Clear()
        whdgItems.GridView.Behaviors.Filtering.ColumnSettings.Clear()
        whdgItems.Columns.Clear()
        whdgItems.GridView.Columns.Clear()
        whdgItems.GridView.ClearDataSource()
    End Sub
    Private Sub AplicarFiltros(ByRef oGridFilters As List(Of Infragistics.Web.UI.GridControls.ColumnFilter))
        If oGridFilters.Count > 0 Then
            For Each oColFilter As Infragistics.Web.UI.GridControls.ColumnFilter In oGridFilters
                Dim oNewColFilter As Infragistics.Web.UI.GridControls.ColumnFilter = oColFilter.Clone
                whdgItems.GridView.Behaviors.Filtering.ColumnFilters.Add(oNewColFilter)
            Next
            whdgItems.GridView.Behaviors.Filtering.ApplyFilter()
        End If
    End Sub
    Private Sub confAnchosVisibilidadYPosicion()
        whdgItems.Columns("Key_UON1").VisibleIndex = whdgItems.Columns.Count - 1
        whdgItems.Columns("Key_UON2").VisibleIndex = whdgItems.Columns.Count - 2
        whdgItems.Columns("Key_UON3").VisibleIndex = whdgItems.Columns.Count - 3
        For i = _listPosicionColumns.Count - 1 To 0 Step -1
            If Not whdgItems.Columns.Item(_listPosicionColumns(i)) Is Nothing Then whdgItems.Columns(_listPosicionColumns(i)).VisibleIndex = i
        Next
    End Sub

    ''' <summary>
    ''' Carga la lista de atributos del grid Items
    ''' Al usar el filtro del grid, no se cargaba la lista de atributos, entonces whdgItems_InitializeRow no formateaba los numericos ni mostraba texto para los boolean. 
    ''' </summary>
    ''' <param name="dtItems">Data source del grid</param>
    ''' <remarks>Llamada desde: CreatewhdgItemsColumnsYConfiguracion y whdgItems_InitializeRow. Máx. 0,1 seg.</remarks>
    Private Sub RellenaListaAtrib(ByVal dtItems As DataTable)
        For Each oCol As DataColumn In dtItems.Columns
            If oCol.ColumnName.Contains("ATRIB_") Then
                Dim NomAtributo() As String = Split(oCol.ColumnName, "_")
                Dim sNomAtributo As String = String.Empty
                For i = 4 To NomAtributo.Length - 1
                    If i > 4 Then
                        sNomAtributo += "-" & NomAtributo(i)
                    Else
                        If NomAtributo(2) = "1" Then 'Es un atributo obligatorio
                            sNomAtributo = "(*) " & NomAtributo(i)
                        Else
                            sNomAtributo = NomAtributo(i)
                        End If
                    End If
                Next
                AddColumnYConfiguracion(oCol.ColumnName, sNomAtributo, , 70)
                _listColsAtributos.Add(oCol.ColumnName)
            End If
        Next
    End Sub
#End Region
#Region "Eventos Arbol"
    Private Sub wdtAdjudicaciones_NodeBound(ByVal sender As Object, ByVal e As Infragistics.Web.UI.NavigationControls.DataTreeNodeEventArgs) Handles wdtAdjudicaciones.NodeBound
        Dim drNode As DataRow = DirectCast(DirectCast(e.Node.DataItem, Infragistics.Web.UI.Framework.Data.DataSetNode).Item, System.Data.DataRowView).Row
        Dim Estado As Byte
        Select Case e.Node.Level
            Case 0
                _EmpresaIntegrada = False
                Estado = DBNullToInteger(drNode("EST_EMP"))

                If DBNullToInteger(drNode("ERP")) > 0 Then
                    _gEmpTieneIntegracion = True
                Else
                    _gEmpTieneIntegracion = False
                End If

                If Estado = 4 And _gEmpTieneIntegracion = True Then 'Totalmente integrado
                    _EmpresaIntegrada = True
                    e.Node.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/empresa_check.png"
                ElseIf Estado <> 4 And _gEmpTieneIntegracion = True Then
                    e.Node.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/empresa.png"
                Else
                    e.Node.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/empresa_sinInt.png"
                End If
                e.Node.Attributes.Add("EST", DBNullToStr(drNode("EST_EMP")))
            Case 1
                _ProveIntegrado = False
                Estado = DBNullToInteger(drNode("EST_PROVE"))
                e.Node.Text = e.Node.Key & "-" & e.Node.Text
                If Estado = 4 AndAlso _EmpresaIntegrada = False Then 'Totalmente integrado
                    _ProveIntegrado = True
                    e.Node.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/proveedor_check.png"
                Else
                    e.Node.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/proveedor.png"
                End If
                e.Node.Attributes.Add("EST", DBNullToStr(drNode("EST_PROVE")))
            Case 2
                Estado = DBNullToInteger(drNode("EST_GRUPO"))
                If Estado = 4 AndAlso _EmpresaIntegrada = False AndAlso _ProveIntegrado = False Then 'Totalmente integrado
                    e.Node.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/grupo_check.png"
                Else
                    If _gEmpTieneIntegracion Then
                        e.Node.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/grupo.png"
                    Else
                        e.Node.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/grupo_gris.png"
                    End If
                End If
                e.Node.Attributes.Add("EST", DBNullToStr(drNode("EST_GRUPO")))
        End Select
    End Sub
    Private Sub wdtAdjudicaciones_SelectionChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.NavigationControls.DataTreeSelectionEventArgs) Handles wdtAdjudicaciones.SelectionChanged
        'Si estaba en un grupo comprobar si estaba filtrado y quitarle el filtro
        Dim oOldNode As Infragistics.Web.UI.NavigationControls.DataTreeNode = e.OldSelectedNodes.Item(0)
        If oOldNode.Level = 2 Then
            Dim oAdjudicacion As FSNServer.Adjudicacion = System.Web.HttpContext.Current.Cache("oAdjudicacion_" & FSNUser.Cod)

            Dim oEmp As EmpresaAdjudicada = oAdjudicacion.EmpresasAdjudicadas.Find(Function(o As EmpresaAdjudicada) o.Id = oOldNode.ParentNode.ParentNode.Key)
            If oEmp IsNot Nothing AndAlso oEmp.ProveedoresAdjudicados IsNot Nothing AndAlso oEmp.ProveedoresAdjudicados.Count > 0 Then
                Dim oProve As ProveedorAdjudicado = oEmp.ProveedoresAdjudicados.Find(Function(o As ProveedorAdjudicado) o.Codigo = oOldNode.ParentNode.Key)
                If oProve IsNot Nothing AndAlso oProve.Grupos IsNot Nothing AndAlso oProve.Grupos.Count > 0 Then
                    Dim oGrupo As ProveedorAdjudicado.Grupo = oProve.Grupos.Find(Function(o As ProveedorAdjudicado.Grupo) o.Codigo = oOldNode.Key)
                    If oGrupo IsNot Nothing AndAlso oGrupo.Items IsNot Nothing AndAlso oGrupo.Items.Count > 0 Then
                        If oGrupo.Filtrado Then
                            'Comprobar si en el objeto grupo están todos los items y añadir los que faltan
                            Dim oData As DataSet = CType(HttpContext.Current.Cache("dsItems_" & FSNUser.Cod), DataSet)
                            Dim oAdjudicaciones As Fullstep.FSNServer.Adjudicaciones = FSNServer.Get_Object(GetType(FSNServer.Adjudicaciones))
                            For Each oRow As DataRow In oData.Tables(0).Rows
                                If oGrupo.Items.Find(Function(o As ProveedorAdjudicado.Item) (o.Id = oRow("ID_ITEM"))) Is Nothing Then
                                    If oData.Tables.Count = 4 Then
                                        oAdjudicaciones.AgregarItemEscaladoAGrupo(oGrupo, oRow, _AnyoProce, _Gmn1Proce, _CodProce, oProve.Codigo, oData, FSNUser.NumberFormat, False, False)
                                    Else
                                        oAdjudicaciones.AgregarItemAGrupo(oGrupo, oRow, _AnyoProce, _Gmn1Proce, _CodProce, oProve.Codigo, oData, False, False)
                                    End If
                                End If
                            Next
                        End If

                        oGrupo.Filtrado = False

                        'Volver a cargar en el script el objeto grupo modificado
                        Dim sScript As String
                        sScript = "$(document).ready(function(){"
                        sScript += vbCrLf
                        sScript += "ObtenerObjetoAdjudicacion();"
                        sScript += vbCrLf
                        sScript += "CargarAtributosGrupo(" & oEmp.Id & ",'" & oProve.Codigo & "','" & oGrupo.Id & "','" & oGrupo.Codigo & "','" & JSText(oEmp.Denominacion) & "','" & JSText(oProve.Denominacion) & "') "
                        sScript += vbCrLf
                        sScript += "});"
                        If Not Me.ClientScript.IsStartupScriptRegistered("jQuery_GrupoOld") Then _
                            ScriptManager.RegisterStartupScript(upPaneles, Me.GetType, "jQuery_GrupoOld", sScript, True)
                    End If
                End If
            End If
        End If

        Dim nodeSelected As Infragistics.Web.UI.NavigationControls.DataTreeNode
        nodeSelected = CType(sender, Infragistics.Web.UI.NavigationControls.WebDataTree).ActiveNode
        Dim bEmpTieneIntegracion As Boolean = Not nodeSelected.ImageUrl.Contains("sinInt")
        Dim EstIntegracion As Short
        Dim EstAux As String
        Dim dsAux As DataSet = ObtenerDsArbolAdjudicaciones()
        Dim Row As DataRow()
        Select Case nodeSelected.Level
            Case 0 'Empresa
                bEmpTieneIntegracion = Not nodeSelected.ImageUrl.Contains("sinInt")
                Try
                    Row = dsAux.Tables(0).Select("EMPCOD=" & nodeSelected.Key)
                    EstAux = DBNullToStr(Row(0)("EST_EMP"))
                    If EstAux = "" Then
                        EstIntegracion = -1
                    Else
                        EstIntegracion = CInt(EstAux)
                    End If
                Catch ex As Exception
                    If nodeSelected.Attributes("EST") = String.Empty Then
                        EstIntegracion = -1 'No se ha integrado nada
                    Else
                        EstIntegracion = nodeSelected.Attributes("EST")
                    End If
                End Try
                CargarEmpresa(nodeSelected.Key, bEmpTieneIntegracion, EstIntegracion, nodeSelected.Text)
            Case 1 'Proveedor
                bEmpTieneIntegracion = Not nodeSelected.ParentNode.ImageUrl.Contains("sinInt")
                Try
                    Row = dsAux.Tables(1).Select("EMPCOD=" & nodeSelected.ParentNode.Key & " AND PROVECOD='" & nodeSelected.Key & "'")
                    EstAux = DBNullToStr(Row(0)("EST_PROVE"))
                    If EstAux = "" Then
                        EstIntegracion = -1
                    Else
                        EstIntegracion = CInt(EstAux)
                    End If
                Catch ex As Exception
                    If nodeSelected.Attributes("EST") = String.Empty Then
                        EstIntegracion = -1 'No se ha integrado nada
                    Else
                        EstIntegracion = nodeSelected.Attributes("EST")
                    End If
                End Try
                CargarProveedor(nodeSelected.Key, nodeSelected.ParentNode.Key, nodeSelected.ParentNode.Text, nodeSelected.Value, bEmpTieneIntegracion, EstIntegracion)
            Case 2 'Grupos
                bEmpTieneIntegracion = Not nodeSelected.ParentNode.ParentNode.ImageUrl.Contains("sinInt")
                Try
                    Row = dsAux.Tables(2).Select("EMPCOD=" & DBNullToInteger(nodeSelected.ParentNode.ParentNode.Key) & " AND PROVECOD='" & nodeSelected.ParentNode.Key & "' AND GRUPOID=" & DBNullToInteger(nodeSelected.Value))
                    EstAux = DBNullToStr(Row(0)("EST_GRUPO"))
                    If EstAux = "" Then
                        EstIntegracion = -1
                    Else
                        EstIntegracion = CInt(EstAux)
                    End If
                Catch ex As Exception
                    If nodeSelected.Attributes("EST") = String.Empty Then
                        EstIntegracion = -1 'No se ha integrado nada
                    Else
                        EstIntegracion = nodeSelected.Attributes("EST")
                    End If
                End Try
                _GrupoSeleccionado = True
                CargarGrupo(nodeSelected.Key, nodeSelected.Text, DBNullToInteger(nodeSelected.Value), nodeSelected.ParentNode.Key, nodeSelected.ParentNode.Value, DBNullToInteger(nodeSelected.ParentNode.ParentNode.Key), nodeSelected.ParentNode.ParentNode.Text, nodeSelected.ParentNode.ParentNode.Value, bEmpTieneIntegracion, EstIntegracion)
        End Select
    End Sub
#End Region
#Region "Web Methods"
    ''' <summary>Indica si hay adjudicaciones anteriores</summary>
    ''' <param name="Anyo">Año del proceso</param>
    ''' <param name="Gmn1">Commodity del proceso</param>
    ''' <param name="IdEmp">Id empresa</param>
    ''' <param name="codProce">Codigo del proceso</param>
    ''' <param name="sCodProve">Codigo del proveedor</param>
    ''' <param name="iIdGrupo">Id del grupo</param>
    ''' <param name="iIdItem">Id del item</param>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function HayAdjudicacionesPrevias(ByVal Anyo As Integer, ByVal Gmn1 As String, ByVal codProce As Integer, ByVal IdEmp As Integer, ByVal sCodProve As String, ByVal iIdGrupo As Integer, ByVal iIdItem As Integer,
                                                    ByVal AnyoProceAdj As Integer, ByVal Gmn1ProceAdj As String, ByVal codProceProceAdj As Integer, ByVal sCodProveProceAdj As String, ByVal iEstadoActual As Short,
                                                    ByVal bActualizarDatos As Boolean) As Object
        Dim iNivel As Integer
        If sCodProve Is Nothing OrElse sCodProve = String.Empty Then
            iNivel = 1
        ElseIf iIdGrupo = 0 Then
            iNivel = 2
        ElseIf iIdItem = 0 Then
            iNivel = 3
        Else
            iNivel = 4
        End If
        Dim dsDatos As DataSet = AdjudicacionesEmpresa(iNivel, Anyo, Gmn1, codProce, IdEmp, sCodProve, iIdGrupo, iIdItem, AnyoProceAdj, Gmn1ProceAdj, codProceProceAdj, sCodProveProceAdj, iEstadoActual, bActualizarDatos)

        Return (Not dsDatos Is Nothing AndAlso Not dsDatos.Tables Is Nothing AndAlso dsDatos.Tables(0).Rows.Count > 0)
    End Function
    ''' <summary>Actualiza la selección de una línea</summary>
    ''' <param name="id">id de la línea</param>
    ''' <param name="checkAll">indica si la línea está seleccionada</param>
    ''' <returns>Booleano indicando si la fila está seleccionada o no</returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function UpdateSeleccion(ByVal id As Integer, ByVal checkAll As Boolean) As Boolean
        Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")

        If Not HttpContext.Current.Cache("Adjs_" & User.Cod) Is Nothing Then
            Dim dsAdjsAnt As DataSet = CType(HttpContext.Current.Cache("Adjs_" & User.Cod), DataSet)
            If id = 0 Then
                For Each oRow As DataRow In dsAdjsAnt.Tables(0).Rows
                    oRow("SEL") = checkAll
                Next
            Else
                dsAdjsAnt.Tables(0).Select("ID=" & id)(0)("SEL") = Not dsAdjsAnt.Tables(0).Select("ID=" & id)(0)("SEL")
            End If
        Else
            Throw New Exception("The object is not in the cache.")
        End If
    End Function
    ''' <summary>Almacena en una var. de sesiÃƒÂ³n el valor del check de comprobar adjudicaciones anteriores</summary>
    ''' <param name="bChecked">Valor del check</param>    
    ''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃƒÆ’Ã‚Â¡ximo:0</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Sub GuardarValorComprobarAdjAnt(ByVal bChecked As Boolean)
        HttpContext.Current.Session("ComprobarAdjAnt") = bChecked
    End Sub
    ''' <summary>
    ''' Cargar Atributos Proceso
    ''' </summary>
    ''' <param name="iAnyo">Año de proceso</param>
    ''' <param name="sGmn1">Gmn Proceso</param>
    ''' <param name="iCodProce">Proceso</param>
    ''' <param name="idEmp">Empresa</param>
    ''' <param name="CodProve">Proveedor</param>
    ''' <returns>Atributos</returns>
    ''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function CargarAtributosProceso(ByVal iAnyo As Short, ByVal sGmn1 As String, ByVal iCodProce As Integer, ByVal idEmp As Long, ByVal CodProve As String) As Object
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oAdjudicaciones As Fullstep.FSNServer.Adjudicaciones
            Dim oAdjudicacion As FSNServer.Adjudicacion
            Dim oAtributos As List(Of Fullstep.FSNServer.ProveedorAdjudicado.Atributo)
            oAdjudicaciones = FSNServer.Get_Object(GetType(FSNServer.Adjudicaciones))

            oAtributos = oAdjudicaciones.CargarAtributosProceso(iAnyo, sGmn1, iCodProce, idEmp, CodProve)

            oAdjudicacion = System.Web.HttpContext.Current.Cache("oAdjudicacion_" & FSNUser.Cod)
            If Not oAdjudicacion Is Nothing Then
                'Revisaremos si ya tenemos los atributos guardados con valores en el objeto guardado en cache y le pondremos esos valores para que los cargue en pantalla
                For Each oEmpresa As FSNServer.EmpresaAdjudicada In oAdjudicacion.EmpresasAdjudicadas
                    If oEmpresa.Id = idEmp Then
                        For Each oProveAdjudicado As FSNServer.ProveedorAdjudicado In oEmpresa.ProveedoresAdjudicados
                            If oProveAdjudicado.Codigo.Trim = CodProve.Trim Then
                                For Each oAtributo As FSNServer.ProveedorAdjudicado.Atributo In oProveAdjudicado.Atributos
                                    For Each oAtrib As FSNServer.ProveedorAdjudicado.Atributo In oAtributos
                                        If oAtributo.Codigo.Trim = oAtrib.Codigo.Trim AndAlso DBNullToStr(oAtributo.Valor) <> "" Then
                                            oAtrib.Valor = oAtributo.Valor
                                            Exit For
                                        End If
                                    Next
                                Next
                                Exit For
                            End If
                        Next
                        Exit For
                    End If
                Next
            End If
            Return oAtributos
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Cargar Atributos Grupo
    ''' </summary>
    ''' <param name="iAnyo">AÃ±o de proceso</param>
    ''' <param name="sGmn1">Gmn Proceso</param>
    ''' <param name="iCodProce">Proceso</param>
    ''' <param name="idEmp">Empresa</param>
    ''' <param name="CodProve">Proveedor</param>
    ''' <param name="idGrupo">Grupo</param>
    ''' <returns>Atributos</returns>
    ''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function CargarAtributosGrupo(ByVal iAnyo As Short, ByVal sGmn1 As String, ByVal iCodProce As Integer, ByVal idEmp As Long, ByVal CodProve As String, ByVal idGrupo As Short) As Object
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oAdjudicaciones As Fullstep.FSNServer.Adjudicaciones
            Dim oAdjudicacion As FSNServer.Adjudicacion
            Dim oAtributos As List(Of Fullstep.FSNServer.ProveedorAdjudicado.Atributo)

            oAdjudicaciones = FSNServer.Get_Object(GetType(FSNServer.Adjudicaciones))

            oAtributos = oAdjudicaciones.CargarAtributosGrupo(iAnyo, sGmn1, iCodProce, CodProve, idGrupo, idEmp)

            oAdjudicacion = System.Web.HttpContext.Current.Cache("oAdjudicacion_" & FSNUser.Cod)
            If Not oAdjudicacion Is Nothing Then
                'Revisaremos si ya tenemos los atributos guardados con valores en el objeto guardado en cache y le pondremos esos valores para que los cargue en pantalla
                For Each oEmpresa As FSNServer.EmpresaAdjudicada In oAdjudicacion.EmpresasAdjudicadas
                    If oEmpresa.Id = idEmp Then
                        For Each oProveAdjudicado As FSNServer.ProveedorAdjudicado In oEmpresa.ProveedoresAdjudicados
                            If oProveAdjudicado.Codigo.Trim = CodProve.Trim Then
                                For Each oAtributo As FSNServer.ProveedorAdjudicado.Atributo In oProveAdjudicado.Atributos
                                    For Each oAtrib As FSNServer.ProveedorAdjudicado.Atributo In oAtributos
                                        If oAtributo.Codigo.Trim = oAtrib.Codigo.Trim AndAlso DBNullToStr(oAtributo.Valor) <> "" Then
                                            oAtrib.Valor = oAtributo.Valor
                                            Exit For
                                        End If
                                    Next
                                Next
                                Exit For
                            End If
                        Next
                        Exit For
                    End If
                Next
            End If

            Return oAtributos
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Cargar Atributos Item
    ''' </summary>
    ''' <param name="iAnyo">AÃ±o de proceso</param>
    ''' <param name="sGmn1">Gmn Proceso</param>
    ''' <param name="iCodProce">Proceso</param>
    ''' <param name="idEmp">Empresa</param>
    ''' <param name="CodProve">Proveedor</param>
    ''' <param name="idItem">Item</param>
    ''' <param name="idLIA">LOG_GRAL.ID_TABLA</param>
    ''' <param name="idGrupo">Grupo</param>
    ''' <returns>Atributos</returns>
    ''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function CargarAtributosItem(ByVal iAnyo As Short, ByVal sGmn1 As String, ByVal iCodProce As Integer, ByVal idEmp As Long, ByVal CodProve As String, ByVal idItem As Integer, ByVal idLIA As Long, ByVal idGrupo As Short) As Object
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oAdjudicaciones As Fullstep.FSNServer.Adjudicaciones
            Dim oAdjudicacion As FSNServer.Adjudicacion
            Dim oAtributos As List(Of Fullstep.FSNServer.ProveedorAdjudicado.Atributo)

            oAdjudicaciones = FSNServer.Get_Object(GetType(FSNServer.Adjudicaciones))

            oAtributos = oAdjudicaciones.CargarAtributosItem(iAnyo, sGmn1, iCodProce, CodProve, idEmp, idItem, idLIA, idGrupo)

            oAdjudicacion = System.Web.HttpContext.Current.Cache("oAdjudicacion_" & FSNUser.Cod)
            If Not oAdjudicacion Is Nothing Then
                'Revisaremos si ya tenemos los atributos guardados con valores en el objeto guardado en cache y le pondremos esos valores para que los cargue en pantalla
                For Each oEmpresa As FSNServer.EmpresaAdjudicada In oAdjudicacion.EmpresasAdjudicadas
                    If oEmpresa.Id = idEmp Then
                        For Each oProveAdjudicado As FSNServer.ProveedorAdjudicado In oEmpresa.ProveedoresAdjudicados
                            If oProveAdjudicado.Codigo.Trim = CodProve.Trim Then
                                For Each oAtributo As FSNServer.ProveedorAdjudicado.Atributo In oProveAdjudicado.Atributos
                                    For Each oAtrib As FSNServer.ProveedorAdjudicado.Atributo In oAtributos
                                        If oAtributo.Codigo.Trim = oAtrib.Codigo.Trim AndAlso DBNullToStr(oAtributo.Valor) <> "" Then
                                            oAtrib.Valor = oAtributo.Valor
                                            Exit For
                                        End If
                                    Next
                                Next
                                Exit For
                            End If
                        Next
                        Exit For
                    End If
                Next
            End If
            Return oAtributos
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Devuelve los distribuidores de un proveedor
    ''' </summary>
    ''' <param name="CodProve">Codigo del proveedor</param>
    ''' <returns>Distribuidores</returns>
    ''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function CargarDistribuidoresProveedor(ByVal CodProve As String, ByVal idEmp As Long) As Object
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oAdjudicaciones As Fullstep.FSNServer.Adjudicaciones
            oAdjudicaciones = FSNServer.Get_Object(GetType(FSNServer.Adjudicaciones))

            Return oAdjudicaciones.ObtenerDistribuidoresProveedor(CodProve, idEmp)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Devuelve los distribuidores de un item
    ''' </summary>
    ''' <param name="CodProve">Codigo del proveedor</param>
    ''' <returns>Distribuidores</returns>
    ''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function CargarDistribuidoresProveedorItem(ByVal CodProve As String, ByVal idEmp As Long, ByVal Anyo As Long, ByVal sGmn1 As String, ByVal codProce As Long, ByVal idItem As Long, ByVal sUon1 As String, ByVal sUon2 As String, ByVal sUon3 As String) As Object
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oAdjudicaciones As Fullstep.FSNServer.Adjudicaciones
            oAdjudicaciones = FSNServer.Get_Object(GetType(FSNServer.Adjudicaciones))

            Return oAdjudicaciones.ObtenerDistribuidoresProveedorItem(CodProve, idEmp, Anyo, sGmn1, codProce, idItem, sUon1, sUon2, sUon3)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Funcion que devuelve un objeto Adjudicacion para salvar los datos introducidos cuando se pasa de panel empresa a proveedor, o de proveedor a grupo
    ''' </summary>
    ''' <returns>Adjudicacion</returns>
    ''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function ObtenerObjetoAdjudicacion() As Object
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oAdjudicacion As Fullstep.FSNServer.Adjudicacion

            Dim Contexto As System.Web.HttpContext
            Contexto = System.Web.HttpContext.Current
            oAdjudicacion = Contexto.Cache("oAdjudicacion_" & FSNUser.Cod)
            If Not oAdjudicacion Is Nothing Then
                Return oAdjudicacion
            Else
                oAdjudicacion = New Fullstep.FSNServer.Adjudicacion
                Return oAdjudicacion
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Meter en cache la Adjudicacion
    ''' </summary>
    ''' <param name="objAdjudicacion">Adjudicacion</param>
    ''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Sub GuardarObjetoAdjudicacion(ByVal objAdjudicacion As Object, ByVal arItems As Integer())
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oAdjudicacion As Fullstep.FSNServer.Adjudicacion
            Dim serializer As New JavaScriptSerializer()
            oAdjudicacion = serializer.Deserialize(Of FSNServer.Adjudicacion)(serializer.Serialize(DirectCast(objAdjudicacion, Object)).ToString)

            'Si ha habido un cambio de estado se actualiza el dataset
            If Not arItems Is Nothing AndAlso arItems.Length > 0 Then
                Dim dsData As DataSet = CType(HttpContext.Current.Cache("dsItems_" & FSNUser.Cod), DataSet)

                For i As Integer = 0 To arItems.Length - 1
                    Dim drItem As DataRow = (New DataView(dsData.Tables(0), "", "ID_ITEM", DataViewRowState.CurrentRows).FindRows(arItems(0)))(0).Row
                    'Cuando se hace el envÃ­o en el aspx se pone el estado a 0
                    drItem("ESTADOINT") = 0
                Next
            End If

            System.Web.HttpContext.Current.Cache.Insert("oAdjudicacion_" & FSNUser.Cod, oAdjudicacion, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin_2"), 0))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Traspasar Adjudicacion Item a ERP
    ''' </summary>
    ''' <param name="Anyo">AÃ±o de proceso</param>
    ''' <param name="Gmn1">Gmn de proceso</param>
    ''' <param name="CodProce">CÃ³digo de proceso</param>
    ''' <param name="idEmp">Empresa</param>
    ''' <param name="CodProve">Proveedor</param>
    ''' <param name="codGrupo">CÃ³digo de Grupo</param>
    ''' <param name="idItem">Item</param>
    ''' <param name="hayEscalados">si hay escalados o no</param>
    ''' <param name="Borrar">Si se borra o no</param>
    ''' <param name="Centro">Centro</param>
    ''' <returns>0 si ha habido error en el traspaso, 1 si el traspaso ha sido correcto, texto de error si el artículo no está correctamente integrado</returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function TraspasarItemToERP(ByVal Anyo As Short, ByVal Gmn1 As String, ByVal codProce As Integer, ByVal idEmp As Short, ByVal codProve As String, ByVal codGrupo As String, ByVal idItem As Integer, ByVal hayEscalados As Byte,
                                              ByVal Borrar As Byte, ByVal Centro As String, ByVal bBorrarAdjAnt As Boolean) As String
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oAdjudicaciones As Fullstep.FSNServer.Adjudicaciones
            oAdjudicaciones = FSNServer.Get_Object(GetType(FSNServer.Adjudicaciones))
            Dim oAdjudicacion As Fullstep.FSNServer.Adjudicacion

            'Adjudicaciones anteriores a eliminar
            Dim oAdjsEliminar As Fullstep.FSNServer.Adjudicacion
            If HttpContext.Current.Session("ComprobarAdjAnt") Or bBorrarAdjAnt Then
                If Not HttpContext.Current.Cache("Adjs_" & FSNUser.Cod) Is Nothing Then
                    Dim dsAdjsAnt As DataSet = CType(HttpContext.Current.Cache("Adjs_" & FSNUser.Cod), DataSet)
                    Dim dvAdjsEliminar As New DataView(dsAdjsAnt.Tables(0), "SEL=1", "SEL", DataViewRowState.CurrentRows)
                    If dvAdjsEliminar.Count > 0 Then
                        oAdjsEliminar = New Fullstep.FSNServer.Adjudicacion(dvAdjsEliminar)
                    End If
                Else
                    Throw New Exception("The object is not in the cache.")
                End If
            End If

            oAdjudicacion = System.Web.HttpContext.Current.Cache("oAdjudicacion_" & FSNUser.Cod)
            Dim respuesta As String
            If Not oAdjudicacion Is Nothing Then
                If Borrar = 1 Then
                    Return oAdjudicaciones.BorrarItemDeERP(oAdjudicacion, Anyo, Gmn1, codProce, idEmp, codProve, codGrupo, idItem, FSNUser.Cod, Centro)
                Else
                    respuesta = oAdjudicaciones.TraspasarItemToERP(oAdjudicacion, Anyo, Gmn1, codProce, idEmp, codProve, codGrupo, idItem, hayEscalados, FSNUser.Cod, Centro, oAdjsEliminar, FSNServer.TipoAcceso.gbMantEstIntEnTraspasoAdj)

                    'Eliminar objetos de la cache
                    Dim oEnumerator As System.Collections.IDictionaryEnumerator = System.Web.HttpContext.Current.Cache.GetEnumerator
                    While oEnumerator.MoveNext
                        If oEnumerator.Key.ToString.StartsWith("Adjs") Then
                            System.Web.HttpContext.Current.Cache.Remove(oEnumerator.Key.ToString)
                        End If
                    End While

                    If respuesta.Length > 1 Then
                        'es un listado de articulos
                        Dim p As New FSNPage
                        p.ModuloIdioma = ModulosIdiomas.TraspasoAdjudicaciones
                        Dim txtAlerta As String
                        'txtAlerta = "¡DImposible trasladar la adjudicación al ERP!- El artículo ### no está correctamente integrado"
                        txtAlerta = p.Textos(63)
                        txtAlerta = Replace(txtAlerta, "###", respuesta)
                        Return txtAlerta
                    End If
                End If
                Return respuesta
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Traspasar Adjudicacion Grupo a ERP 
    ''' </summary>
    ''' <param name="Anyo">AÃ±o de proceso</param>
    ''' <param name="Gmn1">Gmn de proceso</param>
    ''' <param name="CodProce">CÃ³digo de proceso</param>
    ''' <param name="idEmp">Empresa</param>
    ''' <param name="CodProve">Proveedor</param>
    ''' <param name="codGrupo">CÃ³digo de Grupo</param>
    ''' <param name="hayEscalados">si hay escalados o no</param>
    ''' <returns>Resultado del traspaso</returns>
    ''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function TraspasarGrupoToERP(ByVal Anyo As Short, ByVal Gmn1 As String, ByVal codProce As Integer, ByVal idEmp As Short, ByVal codProve As String, ByVal codGrupo As String, ByVal hayEscalados As Byte,
                                               ByVal bBorrarAdjAnt As Boolean) As String
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oAdjudicaciones As Fullstep.FSNServer.Adjudicaciones
            oAdjudicaciones = FSNServer.Get_Object(GetType(FSNServer.Adjudicaciones))
            Dim oAdjudicacion As Fullstep.FSNServer.Adjudicacion

            'Adjudicaciones anteriores a eliminar
            Dim oAdjsEliminar As Fullstep.FSNServer.Adjudicacion
            If HttpContext.Current.Session("ComprobarAdjAnt") Or bBorrarAdjAnt Then
                'Esta variable puede estar vacia cuando se llega directamente desde la comparativa y se pulsa traspasar desde el nivel de empresa, por eso le paso ese nivel
                If HttpContext.Current.Cache("Adjs_" & FSNUser.Cod) Is Nothing Then
                    Dim dsDatos As DataSet = AdjudicacionesEmpresa(1, Anyo, Gmn1, codProce, idEmp)
                End If
                If Not HttpContext.Current.Cache("Adjs_" & FSNUser.Cod) Is Nothing Then
                    Dim dsAdjsAnt As DataSet = CType(HttpContext.Current.Cache("Adjs_" & FSNUser.Cod), DataSet)
                    Dim dvAdjsEliminar As New DataView(dsAdjsAnt.Tables(0), "SEL=1", "SEL", DataViewRowState.CurrentRows)
                    If dvAdjsEliminar.Count > 0 Then
                        oAdjsEliminar = New Fullstep.FSNServer.Adjudicacion(dvAdjsEliminar)
                    End If
                Else
                    Throw New Exception("The object is not in the cache.")
                End If
            End If

            oAdjudicacion = System.Web.HttpContext.Current.Cache("oAdjudicacion_" & FSNUser.Cod)
            If Not oAdjudicacion Is Nothing Then
                ''Return oAdjudicaciones.TraspasarGrupoToERP(oAdjudicacion, Anyo, Gmn1, codProce, idEmp, codProve, codGrupo, hayEscalados, FSNUser.Cod)
                Dim articulosNoIntegrados As String = ""
                Dim respuesta As String = oAdjudicaciones.TraspasarGrupoToERP(oAdjudicacion, Anyo, Gmn1, codProce, idEmp, codProve, codGrupo, hayEscalados, FSNUser.Cod, articulosNoIntegrados, oAdjsEliminar, FSNServer.TipoAcceso.gbMantEstIntEnTraspasoAdj)
                'tratamiento de respuesta
                If articulosNoIntegrados.Length > 1 Then
                    'es un listado de articulos
                    Dim p As New FSNPage
                    p.ModuloIdioma = ModulosIdiomas.TraspasoAdjudicaciones
                    respuesta = p.Textos(62) & articulosNoIntegrados  ' "¡Imposible trasladar adjudicaciones al ERP!. Los siguientes artículos no se encuentran correctamente integrados en el ERP" & respuesta ' & respuesta
                End If

                'Eliminar objetos de la cache
                Dim oEnumerator As System.Collections.IDictionaryEnumerator = System.Web.HttpContext.Current.Cache.GetEnumerator
                While oEnumerator.MoveNext
                    If oEnumerator.Key.ToString.StartsWith("Adjs") Then
                        System.Web.HttpContext.Current.Cache.Remove(oEnumerator.Key.ToString)
                    End If
                End While
                Return respuesta
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Borrar Grupo De proceso en ERP
    ''' </summary>
    ''' <param name="Anyo">AÃ±o de proceso</param>
    ''' <param name="Gmn1">Gmn de proceso</param>
    ''' <param name="CodProce">CÃ³digo de proceso</param>
    ''' <param name="idEmp">Empresa</param>
    ''' <param name="CodProve">Proveedor</param>
    ''' <param name="codGrupo">CÃ³digo de Grupo</param>
    ''' <returns>Resultado del borrado</returns>
    ''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function BorrarGrupoDeERP(ByVal Anyo As Short, ByVal Gmn1 As String, ByVal codProce As Integer, ByVal idEmp As Short, ByVal codProve As String, ByVal codGrupo As String) As Byte
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oAdjudicaciones As Fullstep.FSNServer.Adjudicaciones
            oAdjudicaciones = FSNServer.Get_Object(GetType(FSNServer.Adjudicaciones))
            Dim oAdjudicacion As Fullstep.FSNServer.Adjudicacion

            Dim Contexto As System.Web.HttpContext
            Contexto = System.Web.HttpContext.Current
            oAdjudicacion = Contexto.Cache("oAdjudicacion_" & FSNUser.Cod)
            If Not oAdjudicacion Is Nothing Then
                Return oAdjudicaciones.BorrarGrupoDeERP(oAdjudicacion, Anyo, Gmn1, codProce, idEmp, codProve, codGrupo, FSNUser.Cod)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Devuelve los centros de un item
    ''' </summary>
    ''' <param name="Uon1">Unidad de organizacion nivel 1</param>
    ''' <param name="Uon2">Unidad de organizacion nivel 2</param>
    ''' <param name="Uon3">Unidad de organizacion nivel 3</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function CargarCentrosItem(ByVal Uon1 As String, ByVal Uon2 As String, ByVal Uon3 As String, ByVal codArt As String) As Object
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oAdjudicaciones As Fullstep.FSNServer.Adjudicaciones
            oAdjudicaciones = FSNServer.Get_Object(GetType(FSNServer.Adjudicaciones))

            Dim oData As DataSet = oAdjudicaciones.ObtenerCentrosItem(Uon1, Uon2, Uon3, codArt)

            Dim oCentros As New List(Of Centro)
            Dim oCentro As Centro
            For Each oRow As DataRow In oData.Tables(0).Rows
                oCentro = New Centro
                oCentro.Denominacion = DBNullToStr(oRow("CENTROS"))
                oCentros.Add(oCentro)
            Next
            Return oCentros
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Validacion segun Erp
    ''' </summary>
    ''' <param name="idAtrib">Atributo</param>
    ''' <param name="Valor">Valor Atributo</param>
    ''' <param name="idEmp">Empresa</param>
    ''' <returns>Respuesta a la Validacion</returns>
    ''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function Validacion_Erp(ByVal idAtrib As Long, ByVal Valor As String, ByVal idEmp As Long) As String
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oAdjudicaciones As Fullstep.FSNServer.Adjudicaciones
            oAdjudicaciones = FSNServer.Get_Object(GetType(FSNServer.Adjudicaciones))

            Dim sMapper As String = oAdjudicaciones.ObtenerNombreMapper("", idEmp)

            Dim sResp As String = oAdjudicaciones.Validacion_ERP(sMapper, idAtrib, Valor, FSNUser.IdiomaCod)

            Return sResp
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Cargar el Mensaje Error Integracion
    ''' </summary>
    ''' <param name="Id_LIA">LOG_GRAL.ID_TABLA</param>
    ''' <returns>Mensaje</returns>
    ''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function Devolver_MensajeErrorIntegracion(ByVal Id_LIA As Long) As String
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oAdjudicaciones As Fullstep.FSNServer.Adjudicaciones
            oAdjudicaciones = FSNServer.Get_Object(GetType(FSNServer.Adjudicaciones))

            Dim sMensaje As String = oAdjudicaciones.Devolver_MensajeErrorIntegracion(Id_LIA)
            Return sMensaje
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Cargar los Items de un Grupo 
    ''' </summary>
    ''' <param name="codGrupo">CÃ³digo de Grupo</param>
    ''' <param name="Anyo">AÃ±o de proceso</param>
    ''' <param name="Gmn1">Gmn de proceso</param>
    ''' <param name="CodProce">CÃ³digo de proceso</param>
    ''' <param name="CodProve">Proveedor</param>
    ''' <param name="idEmp">Empresa</param>
    ''' <returns>Objeto Items</returns>
    ''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function CargarItemsGrupoParaTraspaso(ByVal codGrupo As String, ByVal Anyo As Short, ByVal Gmn1 As String, ByVal CodProce As Long, ByVal CodProve As String, ByVal idEmp As Long) As List(Of Fullstep.FSNServer.ProveedorAdjudicado.Item)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oAdjudicaciones As Fullstep.FSNServer.Adjudicaciones
            oAdjudicaciones = FSNServer.Get_Object(GetType(FSNServer.Adjudicaciones))

            Dim oItems As List(Of Fullstep.FSNServer.ProveedorAdjudicado.Item) = oAdjudicaciones.ObtenerItemsGrupo(codGrupo, Anyo, Gmn1, CodProce, CodProve, idEmp, FSNUser.NumberFormat, FSNUser.Cod, FSNUser.DateFormat)

            Return oItems
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Esta función se encarga de trasformar el objeto de la adjudicación en otro reducido de tamaño (únicamente necesitamos saber todos los datos que el usuario está
    ''' traspasando, no toda la adjudicación) y gestiona todo el tema de la llamada a FSIS (las validaciones las realizan la mapper de Integración).
    ''' </summary>
    ''' <param name="objAdjudicacion">El objeto adjudicación completo.</param>
    ''' <param name="iNivel">El nivel/panel desde donde se esté realizando el traspaso: 0 = Empresa, 1 = Proveedor, 2 = Grupo y 3 = Ítem.</param>
    ''' <param name="strEmpresa">El identificador de la empresa que se esté traspasando.</param>
    ''' <param name="strProveedor">El código Fullstep del proveedor que se esté traspasando o vacío si el nivel/panel es 0.</param>
    ''' <param name="strGrupo">El código del grupo que se esté traspasando o vacío si el nivel/panel es 0 o 1.</param>
    ''' <param name="strItem">El identificador del ítem que se esté traspasando o vacío si el nivel/panel es 0, 1 o 2.</param>
    ''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo máximo:0.</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function ValidacionesIntegracion(ByVal objAdjudicacion As Object, ByVal iNivel As Integer, ByVal strEmpresa As String, ByVal strProveedor As String,
                                      ByVal strGrupo As String, ByVal strItem As String) As String
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim serializer As New JavaScriptSerializer()
        Dim oAdjudicacion As Fullstep.FSNServer.Adjudicacion
        Dim oAdjReducida As New Fullstep.FSNServer.Adjudicacion
        Dim oIntegracion As Integracion = FSNServer.Get_Object(GetType(FSNServer.Integracion))
        Dim strERP As String = oIntegracion.getErpFromEmpresa(strEmpresa)
        Dim iIndiceEmp, iIndiceProve, iIndiceGrupo, iIndiceItem As Integer

        If Not oIntegracion.HayTraspasoADJERP Or Not oIntegracion.IntegracionWCFActiva(TablasIntegracion.Adj, CInt(strERP)) Then
            Return String.Empty 'No realizamos las validaciones y continuamos con la ejecuciÃ³n.
        End If

        oAdjudicacion = serializer.Deserialize(Of Fullstep.FSNServer.Adjudicacion)(serializer.Serialize(DirectCast(objAdjudicacion, Object)).ToString)
        oAdjReducida = serializer.Deserialize(Of Fullstep.FSNServer.Adjudicacion)(serializer.Serialize(DirectCast(objAdjudicacion, Object)).ToString)

        For Each oEmpresa In oAdjudicacion.EmpresasAdjudicadas
            If oEmpresa.Id <> strEmpresa Then 'Si no estamos en dicha empresa no nos interesa.
                oAdjReducida.EmpresasAdjudicadas.RemoveAt(iIndiceEmp)
                iIndiceEmp -= 1
            ElseIf iNivel >= Panel.Proveedor Then
                For Each oProveedor In oEmpresa.ProveedoresAdjudicados
                    If oProveedor.Codigo <> strProveedor Then 'Si no estamos en dicho proveedor no nos interesa.
                        oAdjReducida.EmpresasAdjudicadas.Item(iIndiceEmp).ProveedoresAdjudicados.RemoveAt(iIndiceProve)
                        iIndiceProve -= 1
                    ElseIf iNivel >= Panel.Grupo Then
                        For Each oGrupo In oProveedor.Grupos
                            If oGrupo.Codigo <> strGrupo Then
                                oAdjReducida.EmpresasAdjudicadas.Item(iIndiceEmp).ProveedoresAdjudicados.Item(iIndiceProve).Grupos.RemoveAt(iIndiceGrupo)
                                iIndiceGrupo -= 1
                            ElseIf iNivel = Panel.Item Then
                                For Each oItem In oGrupo.Items
                                    If oItem.Id <> strItem Then
                                        oAdjReducida.EmpresasAdjudicadas.Item(iIndiceEmp).ProveedoresAdjudicados.Item(iIndiceProve).Grupos.Item(iIndiceGrupo).Items.RemoveAt(iIndiceItem)
                                        iIndiceItem -= 1
                                    End If
                                    iIndiceItem += 1
                                Next
                            End If
                            iIndiceGrupo += 1
                        Next
                    End If
                    iIndiceProve += 1
                Next
            End If
            iIndiceEmp += 1
        Next

        'Llamada a FSIS.
        Return oIntegracion.ValidacionesWCF(oAdjReducida, iNivel, CInt(strERP))
    End Function

    ''' <summary>Genera un nuevo registro en la tabla LOG con los datos indicados</summary>    
    ''' <param name="Acc">Id accion</param>
    ''' <param name="sDat">Datos</param>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Sub RegistrarAccion(ByVal Acc As Integer, ByVal sDat As String)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        FSNServer.RegistrarAccion(FSNUser.Cod, Acc, sDat)
    End Sub

#End Region
#Region "Culture Info"
    ''' <summary>
    ''' Dar el formato del usuario a las columnas de fecha del grid
    ''' </summary>
    ''' <param name="field">columna seleccionada</param>
    ''' <remarks></remarks>
    Private Sub setDateFormat(ByVal field As Infragistics.Web.UI.GridControls.BoundDataField)
        field.DataFormatString = "{0:" & FSNUser.DateFormat.ShortDatePattern.ToString & "}"
    End Sub
    ''' <summary>
    ''' Dar el formato del usuario a las columnas de fecha y hora del grid
    ''' </summary>
    ''' <param name="field">columna seleccionada</param>
    ''' <remarks></remarks>
    Private Sub setDateTimeFormat(ByVal field As Infragistics.Web.UI.GridControls.BoundDataField)
        field.DataFormatString = "{0:" & FSNUser.DateFormat.ShortDatePattern.ToString & " " & FSNUser.DateFormat.ShortTimePattern & "}"
    End Sub
#End Region
    Structure Centro
        Private _Denominacion As String
        Public Property Denominacion() As String
            Get
                Return _Denominacion
            End Get
            Set(ByVal value As String)
                _Denominacion = value
            End Set
        End Property
    End Structure
    Private Sub CrearTablaProveedoresAdjudicadosEmpresa(ByVal idEmp As Long)
        Throw New NotImplementedException
    End Sub
End Class