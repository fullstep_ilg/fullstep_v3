﻿var gAdjudicacion, editRow, gCancelEventCombo, gModificadaDistribucionPanelProv
var gCodProve, gIdEmp, gCodGrupo, gIdItem, gbPanelItem, gIdGrupo;
var gEnvioPdte; //Esta variable se usa para cuando desde el panel item se le de a traspasar y salga algun mensaje, cuando le demos a aceptar haga el envio
var gIndEmp, gIndProve, gIndGrupo, gIndItem
var gArrDistribuidoresItem = new Array();
var gArrItemsConDistintaDistribucion = new Array();
var gOldValorDistribuidor, gCodDistribuidorModifDistribucionItems, gPorcentajeDistribuidorModifDistribucionItems;
var gBoolValidaPorcentajesDistribucionProveedor, gBoolValidaFloat, gAnchoModalPopUp
var gNivelTraspaso;
var gBorrar, gCentro, gCodArt
var hPostBack;
var bBorrarAdjAntEmpProve = false;
var bwhdgItemsPostback = false;

$('#imgInfoERP').live('click', function () {
	$('#pnlInfoERP').css('left', $(this).position().left);
	$('#pnlInfoERP').css('top', $(this).position().top);
	$(this).hide();
	$('#pnlInfoERP').show();
});
$('#imgCerrar').live('click', function () {
	$('#pnlInfoERP').hide();
	$('#imgInfoERP').show();
});

$(document).ready(function () {
	$('[id$=ImgBtnFirst]').live('click', function () { FirstPage(); });
	$('[id$=ImgBtnPrev]').live('click', function () { PrevPage(); });
	$('[id$=ImgBtnNext]').live('click', function () { NextPage(); });
	$('[id$=ImgBtnLast]').live('click', function () { LastPage(); });
	$('[id*=AdjAntChecked],[id*=AdjAntUnchecked],#AdjCheckAllChecked,#AdjCheckAllUnchecked').live('click', function () { whdgAdjAntCheckChange(this); });

	Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
	Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
	ObtenerObjetoAdjudicacion();
});
function BeginRequestHandler(sender, args) {
	if (typeof sender._postBackSettings.sourceElement !== "undefined") {
		if (sender._postBackSettings.sourceElement.id.indexOf("whdgItems") != -1) bwhdgItemsPostback = true;
	}
};
function EndRequestHandler(sender, args) {
	//Se comprueba si el postback es generado por whdgItems en BeginRequestHandler en vez de en esta funciÃ³n porque habÃ­a casos en los que en esta funciÃ³n sender._postBackSettings.sourceElement venÃ­a undefined
	if (bwhdgItemsPostback) {
		bwhdgItemsPostback = false;
		ObtenerObjetoAdjudicacion();
	}
};
//#region "Paginador"
function IndexChanged() {
	var dropdownlist = $('[id$=PagerPageList]')[0];
	var grid = $find($('[id$=whdgAdjAnt]').attr('id')).get_gridView();
	var newValue = dropdownlist.selectedIndex;
	grid.get_behaviors().get_paging().set_pageIndex(newValue);
};
function FirstPage() {
	var grid = $find($('[id$=whdgAdjAnt]').attr('id')).get_gridView();
	grid.get_behaviors().get_paging().set_pageIndex(0);
};
function PrevPage() {
	var grid = $find($('[id$=whdgAdjAnt]').attr('id')).get_gridView();
	var dropdownlist = $('[id$=PagerPageList]')[0];
	if (grid.get_behaviors().get_paging().get_pageIndex() > 0) {
		grid.get_behaviors().get_paging().set_pageIndex(grid.get_behaviors().get_paging().get_pageIndex() - 1);
	}
};
function NextPage() {
	var grid = $find($('[id$=whdgAdjAnt]').attr('id')).get_gridView();
	var dropdownlist = $('[id$=PagerPageList]')[0];
	if (grid.get_behaviors().get_paging().get_pageIndex() < grid.get_behaviors().get_paging().get_pageCount() - 1) {
		grid.get_behaviors().get_paging().set_pageIndex(grid.get_behaviors().get_paging().get_pageIndex() + 1);
	}
};
function LastPage() {
	var grid = $find($('[id$=whdgAdjAnt]').attr('id')).get_gridView();
	grid.get_behaviors().get_paging().set_pageIndex(grid.get_behaviors().get_paging().get_pageCount() - 1);
};
function whdgAdjAnt_PageIndexChanged() {
	var grid = $find($('[id$=whdgAdjAnt]').attr('id')).get_gridView();
	var dropdownlist = $('[id$=PagerPageList]')[0];

	dropdownlist.options[grid.get_behaviors().get_paging().get_pageIndex()].selected = true;
};
//#endregion
function customConfirm(message, callbackFunction, params) {
	//<summary>Función que simula un confirm. Como no se puede bloquear la ejecución del código en espera de la respuesta del usuario se simula llamando a una función de callback que se ejecuta en caso de que el usuario pulse sí</summary>
	$('#<%=lblMensajeConfirm.ClientID%>').text(message);
	$find('<%=mpeConfirm.ClientID%>').show();
	$("#<%=btnAceptarConfirm.ClientID%>").click(function () {
		callbackFunction(true, params);
		$find('<%=mpeConfirm.ClientID %>').hide();
		$("#<%=btnAceptarConfirm.ClientID%>").off('click');
		$("#<%=btnCancelarConfirm.ClientID%>").off('click');
		return false;
	});
	$("#<%=btnCancelarConfirm.ClientID%>").click(function () {
		callbackFunction(false, params);
		$find('<%=mpeConfirm.ClientID %>').hide();
		$("#<%=btnAceptarConfirm.ClientID%>").off('click');
		$("#<%=btnCancelarConfirm.ClientID%>").off('click');
		return false;
	});
};
function whdgAdjAntCheckChange(oImg) {
	//cambiar la imagen  
	var id;
	var checkAll;
	if (oImg.id.split("_").length > 1) {
		//Check en una fila
		id = oImg.id.split("_")[1];
		checkAll = false;
		$("#AdjAntChecked_" + id).toggle();
		$("#AdjAntUnchecked_" + id).toggle();
	} else {
		//Check en el header
		var styleChecked;
		var styleUnchecked;
		id = 0;
		if (oImg.id == "AdjCheckAllUnchecked") {
			checkAll = true;
			styleChecked = ""
			styleUnchecked = "display:none;"
		} else {
			checkAll = false;
			styleChecked = "display:none;"
			styleUnchecked = ""
		}
		$("#AdjCheckAllChecked").toggle();
		$("#AdjCheckAllUnchecked").toggle();
		$("[id*=AdjAntChecked_]").attr("style", styleChecked);
		$("[id*=AdjAntUnchecked_]").attr("style", styleUnchecked);
	}

	//Actualizar datos
	ActualizarSeleccionAdjsAnteriores(id, checkAll);
};
function ActualizarSeleccionAdjsAnteriores(id, checkAll) {
	$.ajax({
		type: 'POST',
		url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/UpdateSeleccion',
		data: JSON.stringify({ id: id, checkAll: checkAll }),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		async: true
	});
};
function CheckComprobarAdjAnt(bChecked) {
	$("[id$=chkComprobarAdjAntEmp]").prop("checked", bChecked)
	$("[id$=chkComprobarAdjAntProve]").prop("checked", bChecked)
	$("[id$=chkComprobarAdjAntItem]").prop("checked", bChecked)
	$("[id$=chkComprobarAdjAnt]").prop("checked", bChecked)

	var params = { bChecked: bChecked };
	$.ajax({
		type: "POST",
		url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/GuardarValorComprobarAdjAnt',
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(params),
		dataType: "json",
		async: false
	})
};
function getItem(idItem, centro, codArt) {
	//Funcion que devuelve el objeto item del grupo actual
	//idItem: Identificador del item
	//centro: Centro del item
	//codARt: Codigo del articulo de planta 
	var indEmp
	var oItem
	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		//buscamos en el objeto la empresa a la que pertenece el proveedor que se va a guardar
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == gIdEmp) {
			indEmp = z
			break
		}
	}
	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == gCodProve) {
			for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos.length - 1; z++) {
				if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Codigo == gCodGrupo) {
					for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items.length - 1; x++) {
						oItem = gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x]
						if (oItem.Id == parseInt(idItem) && oItem.Centro == centro && oItem.Codigo == codArt) {
							return oItem;
							break;
						} else {
							oItem = null;
						}
					}
					break;
				}
			}
			break;
		}
	}
	if (centro != '' && oItem == null) {
		//En caso de que el item tenga varios centros, el centro se pone en blanco en el grid, si el usuario le asigna un centro a ese item(en el detalle del item), a la hora de buscarlo
		//no lo encontraria ya que lo buscaria por el centro que acaba de seleccionar el usuario y ese item tendria el centro en blanco, por eso ponemos el centro en blanco y realizamos
		//de nuevo la busqueda
		centro = ''
		for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
			if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == gCodProve) {
				for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos.length - 1; z++) {
					if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Codigo == gCodGrupo) {
						for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items.length - 1; x++) {
							oItem = gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x]
							if (oItem.Id == parseInt(idItem) && oItem.Centro == centro && oItem.Codigo == codArt) {
								return oItem;
								break;
							} else {
								oItem = null;
							}
						}
						break;
					}
				}
				break;
			}
		}
	}
};
function CambiarEstadoProgressModalPopup(sEstado, AmpliarAncho) {
	$("[id$=LblProcesando]").text(sEstado);
};
function btnTrasladarAdjERPDesdeProveedor_Click() {
	var datatreeAdj = $find("<%= wdtAdjudicaciones.ClientID %>");
	var nodoProveedor = datatreeAdj.get_selectedNodes()
	var nodoEmpresa = nodoProveedor[0].get_parentNode()

	GuardarDatosPanelProveedor(nodoProveedor[0].get_key(), nodoProveedor[0].get_text(), nodoEmpresa.get_key());

	//////////////////////////////////////////////////////
	//VALIDACION ATRIBUTOS DE PROVEEDOR Y DE GRUPOS
	//////////////////////////////////////////////////////
	var idEmp = nodoEmpresa.get_key()
	var ProvNode = nodoProveedor[0]
	//validamos que se hayan seleccionado los codigos ERP para la distribucion
	respCodsERP = ValidarCodERPTraspasoDesdeProveedor(idEmp, ProvNode.get_key())

	if (respCodsERP.length > 0) {
		var TextoProvsNoDistr = TextosJScript[41];
		for (var x = 0; x <= respCodsERP.length - 1; x++) {
			if (x == 0)
				TextoProvsNoDistr += respCodsERP[x]
			else
				TextoProvsNoDistr += ', ' + respCodsERP[x]
		}
		$("[id$=lblMensajeAlertaAtribsObligEnProveedor]").text(TextoProvsNoDistr)
		$("[id$=divMensajeAlertaAtribsObligEnProveedor]").show()
		$("[id$=ImgProgress]").css('visibility', 'visible')
		CambiarEstadoProgressModalPopup(TextosJScript[20], false)
		$find('<%=ModalProgress.ClientID%>').hide()
		return false
	} else {
		$("[id$=divMensajeAlertaAtribsObligEnProveedor]").hide()
	}

	//Valido los atributos de ese proveedor
	CambiarEstadoProgressModalPopup("DCargando Atribs " + ProvNode.get_key(), true)
	CargarAtributosProveedorAlTraspasarEmpresa(ProvNode.get_key(), ProvNode.get_text(), idEmp)
	var resp = ValidarAtributosObligatoriosDeProveedoresAlTraspasarEmpresa(idEmp, ProvNode.get_key(), 2)
	if (resp == false) {
		$("[id$=ImgProgress]").css('visibility', 'visible')
		$find('<%=ModalProgress.ClientID%>').hide()
		CambiarEstadoProgressModalPopup(TextosJScript[20], false)
		return false //Si hay algun atributo obligatorio de ese proveedor sin valor no seguimos
	}

	//Una vez que se han comprobado los atributos del proveedor, comprobariamos los atributos de los grupos del proveedor
	for (var y = 0; y <= ProvNode.getItems().get_length() - 1; y++) {
		//recorro los grupos de ese proveedor
		var GroupNode = ProvNode.getItems().getNode(y)
		CambiarEstadoProgressModalPopup("DCargando Atribs " + GroupNode.get_key(), true)
		CargarAtributosGrupoAlTraspasarEmpresa(GroupNode.get_key(), ProvNode.get_key(), GroupNode.get_text(), idEmp, parseInt(GroupNode.get_valueString()))
		var resp = ValidarAtributosObligatoriosDeGruposAlTraspasarEmpresa(idEmp, ProvNode.get_key(), GroupNode.get_key(), 2)
		if (resp == false) {
			$("[id$=ImgProgress]").css('visibility', 'visible')
			$find('<%=ModalProgress.ClientID%>').hide()
			CambiarEstadoProgressModalPopup(TextosJScript[20], false)
			return false //Si hay algun atributo obligatorio de algun Grupo sin valor no seguimos
		}
	}

	////////////////////////////////////////////////////////////////////////////
	//CARGA DE ITEMS DE CADA GRUPO Y VALIDACION DE LOS ATRIBUTOS DE LOS ITEMS
	/////////////////////////////////////////////////////////////////////////////
	//Recorro los grupos de ese proveedor
	for (var y = 0; y <= ProvNode.getItems().get_length() - 1; y++) {
		//recorro los grupos de ese proveedor
		var GroupNode = ProvNode.getItems().getNode(y)
		CambiarEstadoProgressModalPopup("DCargando Atribs Items " + GroupNode.get_key(), true)
		CargarItemsGrupoAlTraspasarEmpresa(idEmp, ProvNode.get_key(), GroupNode.get_key(), GroupNode.get_valueString(), GroupNode.get_text())
		var resp = ValidarAtributosObligatoriosDeItemsAlTraspasarEmpresa(idEmp, ProvNode.get_key(), GroupNode.get_key(), 2)
		if (resp == false) {
			$("[id$=ImgProgress]").css('visibility', 'visible')
			$find('<%=ModalProgress.ClientID%>').hide()
			CambiarEstadoProgressModalPopup(TextosJScript[20], false)
			return false //Si hay algun atributo obligatorio de algun Item sin valor no seguimos
		}
	}

	////////////////////////////////////////
	//VALIDACIONES A MEDIDA DE INTEGRACIÃ“N
	////////////////////////////////////////
	if (ValidacionesIntegracion(gAdjudicacion, 1, nodoEmpresa.get_key(), ProvNode.get_key(), "", "") == false) {
		$("[id$=ImgProgress]").css('visibility', 'visible')
		$find('<%=ModalProgress.ClientID%>').hide()
		CambiarEstadoProgressModalPopup(TextosJScript[20], false)
		return false
	}

	////////////////////////////////////////
	//TRASPASO DE LOS ITEMS DE CADA GRUPO
	////////////////////////////////////////
	GuardarObjetoAdjudicacion();

	for (var y = 0; y <= ProvNode.getItems().get_length() - 1; y++) {
		//recorro los grupos de ese proveedor
		var GroupNode = ProvNode.getItems().getNode(y)
		CambiarEstadoProgressModalPopup("DTraspasando Items " + GroupNode.get_key(), true)
		TraspasoItemsGrupoERP(idEmp, ProvNode.get_key(), GroupNode.get_key(), 2)
	}

	var sData = TextosJScript[34] + ': ' + gAnyo + '/' + gGmn1 + '/' + gCodProce + ', ' + TextosJScript[37] + ': ' + nodoEmpresa.get_text() + ', ' + TextosJScript[38] + ': ' + ProvNode.get_text();
	RegistrarAccion(ACCTraspAdjTraspasarProve, sData);

	$("[id$=ImgProgress]").css('visibility', 'visible')
	$find('<%=ModalProgress.ClientID%>').hide()
	CambiarEstadoProgressModalPopup(TextosJScript[20], false)
};
function btnBorrarAdjERPDesdeProveedor_Click() {
	//funcion que borra los traspasos de una empresa, desde el panel de empresa
	var datatreeAdj = $find("<%= wdtAdjudicaciones.ClientID %>");
	var nodoProveedor = datatreeAdj.get_selectedNodes();
	var nodoEmpresa = nodoProveedor[0].get_parentNode();
	var idEmp = nodoEmpresa.get_key();

	////////////////////////////////
	//CARGA DE ITEMS DE CADA GRUPO 
	///////////////////////////////
	var ProvNode = nodoProveedor[0]
	for (var y = 0; y <= ProvNode.getItems().get_length() - 1; y++) {
		//recorro los grupos de ese proveedor
		var GroupNode = ProvNode.getItems().getNode(y)
		CambiarEstadoProgressModalPopup("DCargando Items " + GroupNode.get_key(), true)
		CargarItemsGrupoAlTraspasarEmpresa(idEmp, ProvNode.get_key(), GroupNode.get_key(), GroupNode.get_valueString(), GroupNode.get_text())
	}

	////////////////////////////////////////
	//TRASPASO DE LOS ITEMS DE CADA GRUPO
	////////////////////////////////////////
	GuardarObjetoAdjudicacion();

	for (var x = 0; x <= nodoEmpresa.getItems().get_length() - 1; x++) {
		//Recorro los proveedores de esa empresa
		var ProvNode = nodoEmpresa.getItems().getNode(x)
		for (var y = 0; y <= ProvNode.getItems().get_length() - 1; y++) {
			//recorro los grupos de ese proveedor
			var GroupNode = ProvNode.getItems().getNode(y)
			CambiarEstadoProgressModalPopup("DBorrando Items " + GroupNode.get_key(), true)
			BorrarItemsGrupoERP(idEmp, ProvNode.get_key(), GroupNode.get_key(), 2)
		}
	}

	var sData = TextosJScript[34] + ': ' + gAnyo + '/' + gGmn1 + '/' + gCodProce + ', ' + TextosJScript[37] + ': ' + nodoEmpresa.get_text() + ', ' + TextosJScript[38] + ': ' + nodoProveedor[0].get_text();
	RegistrarAccion(ACCTraspAdjBorrarProve, sData);

	$("[id$=ImgProgress]").css('visibility', 'visible')
	$find('<%=ModalProgress.ClientID%>').hide()
	CambiarEstadoProgressModalPopup(TextosJScript[20], false)
};
function btnBorrarAdjERPDesdeEmpresa_Click() {
	//funcion que borra los traspasos de una empresa, desde el panel de empresa
	var datatreeAdj = $find("<%= wdtAdjudicaciones.ClientID %>");
	var nodoEmpresa = datatreeAdj.get_selectedNodes();
	var idEmp = nodoEmpresa[0].get_key();

	//primero guardamos la empresa por si aun no estaria guardada(se guarda cuando sale del panel empresa)
	GuardarDatosPanelEmpresa(nodoEmpresa[0].get_key(), nodoEmpresa[0].get_text());

	////////////////////////////////////////////////////////////////////////////
	//CARGA DE ITEMS DE CADA GRUPO Y VALIDACION DE LOS ATRIBUTOS DE LOS ITEMS
	/////////////////////////////////////////////////////////////////////////////
	for (var x = 0; x <= nodoEmpresa[0].getItems().get_length() - 1; x++) {
		//Recorro los proveedores de esa empresa
		var ProvNode = nodoEmpresa[0].getItems().getNode(x)
		for (var y = 0; y <= ProvNode.getItems().get_length() - 1; y++) {
			//recorro los grupos de ese proveedor
			var GroupNode = ProvNode.getItems().getNode(y)
			CambiarEstadoProgressModalPopup("DCargando Items " + GroupNode.get_key(), true)
			CargarItemsGrupoAlTraspasarEmpresa(idEmp, ProvNode.get_key(), GroupNode.get_key(), GroupNode.get_valueString(), GroupNode.get_text())
		}
	}

	////////////////////////////////////////
	//TRASPASO DE LOS ITEMS DE CADA GRUPO
	////////////////////////////////////////
	GuardarObjetoAdjudicacion();

	for (var x = 0; x <= nodoEmpresa[0].getItems().get_length() - 1; x++) {
		//Recorro los proveedores de esa empresa
		var ProvNode = nodoEmpresa[0].getItems().getNode(x)
		for (var y = 0; y <= ProvNode.getItems().get_length() - 1; y++) {
			//recorro los grupos de ese proveedor
			var GroupNode = ProvNode.getItems().getNode(y)
			CambiarEstadoProgressModalPopup("DBorrando Items " + GroupNode.get_key(), true)
			BorrarItemsGrupoERP(idEmp, ProvNode.get_key(), GroupNode.get_key(), 1)
		}
	}

	var sData = TextosJScript[34] + ': ' + gAnyo + '/' + gGmn1 + '/' + gCodProce + ', ' + TextosJScript[37] + ': ' + nodoEmpresa[0].get_text();
	RegistrarAccion(ACCTraspAdjBorrarEmp, sData);

	$("[id$=ImgProgress]").css('visibility', 'visible')
	$find('<%=ModalProgress.ClientID%>').hide()
	CambiarEstadoProgressModalPopup(TextosJScript[20], false)
};
function ValidarCodERPTraspasoDesdeProveedor(idEmp, codProve) {
	var indEmp
	var arrAtributosObligatoriosVacios = new Array();
	var arrCodsERP = new Array();
	var bDistribucionCorrecta = false
	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == idEmp) {
			for (var y = 0; y <= gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados.length - 1; y++) {
				if (gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].Codigo == codProve) {
					if (gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].Distribuidores.length > 0) {
						for (var v = 0; v <= gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].Distribuidores.length - 1; v++) {
							Distribuidor = gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].Distribuidores[v]
							if (Distribuidor.CodErpSeleccionado != '' && Distribuidor.CodErpSeleccionado != null && Distribuidor.PorcentajeDistribuido > 0) {
								bDistribucionCorrecta = true
							}
						}
					}

					if (bDistribucionCorrecta == false) {
						if (gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].Grupos) {
							if (gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].Grupos.length > 0)
								bDistribucionCorrecta = true //recorremos todos los grupos del proveedor, para que la distribucion fuera correcta todos los grupos tendrian que tener el cod erp seleccionado
							for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].Grupos.length - 1; x++) {
								Grupo = gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].Grupos[x]
								if (Grupo.CodErpSeleccionado == '' || Grupo.CodErpSeleccionado == null)
									bDistribucionCorrecta = false
							}
						}
					}

					if (bDistribucionCorrecta == false) {
						Proveedor = gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y]
						if (Proveedor.CodErpSeleccionado == '' || Proveedor.CodErpSeleccionado == null)
							arrCodsERP.push(Proveedor.Codigo)
					}
					break
				}
			}
			break
		}
	}
	return arrCodsERP
};
function ValidarCodERPTrapasoDesdeEmpresa(idEmp) {
	var indEmp
	var arrAtributosObligatoriosVacios = new Array();
	var arrCodsERP = new Array();
	var bDistribucionCorrecta = false
	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == idEmp) {
			for (var y = 0; y <= gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados.length - 1; y++) {
				Proveedor = gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y]
				if (Proveedor.CodErpSeleccionado == '' || Proveedor.CodErpSeleccionado == null) {
					if (gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].Distribuidores.length == 0) {
						arrCodsERP.push(Proveedor.Codigo)
					} else {
						var bDistribucionCorrecta = false
						for (var v = 0; v <= gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].Distribuidores.length - 1; v++) {
							Distribuidor = gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].Distribuidores[v]
							if (Distribuidor.CodErpSeleccionado != '' && Distribuidor.CodErpSeleccionado != null && Distribuidor.PorcentajeDistribuido > 0) {
								bDistribucionCorrecta = true
							}
						}
						if (bDistribucionCorrecta == false) {
							if (gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].Grupos) {
								if (gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].Grupos.length > 0)
									bDistribucionCorrecta = true //recorremos todos los grupos del proveedor, para que la distribucion fuera correcta todos los grupos tendrian que tener el cod erp seleccionado
								for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].Grupos.length - 1; x++) {
									Grupo = gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].Grupos[x]
									if (Grupo.CodErpSeleccionado == '' || Grupo.CodErpSeleccionado == null)
										bDistribucionCorrecta = false
								}
							}
						}
						if (bDistribucionCorrecta == false) {
							arrCodsERP.push(Proveedor.Codigo)
						}
					}
				}
			}
			break
		}
	}
	return arrCodsERP
};
function TrasladarAdjERP() {
	switch (gNivelTraspaso) {
		case 1:
			//Nivel de empresa
			TrasladarAdjERPDesdeEmpresa();
			break;
		case 2:
			//Nivel de proveedor
			TrasladarAdjERPDesdeProveedor();
			break;
		case 3:
			//Nivel de grupo
			btnTrasladarAdjERP_Click();
			break;
		case 4:
			//Nivel de item                
			btnAceptarItem_Click();
			if ($("[id$=pnlMensaje]").css('display') != "block") { //Si hay un mensaje de advertencia en la distribucion de los distribuidores no haremos el traspaso
				TraspasoERPItem(gIdItem, gBorrar, gCentro, gCodArt);
			}
			else {
				//Ocultar el panel de adjudicaciones anteriores si está visible
				gEnvioPdte = true;
				$find('<%=mpeAdjAnt.ClientID%>').hide();
			}
			break;
		case 5:
			//Nivel de item (desde el grid)                 
			TraspasoERPItem(gIdItem, gBorrar, gCentro, gCodArt);
			break;
	}
};
function PrevioTrasladarAdjERPDesdeEmpresa() {
	//Boton aceptar y traspasar al ERP los ítems de una empresa
	//Se comprueba el estado de los ítems del grupo.
	//Items en estado pendiente: Si está marcado el check de comprobar adj. anteriores funcionará como hasta ahora (en el grid aparecerán sólo los ítems en este estado. Si no está marcado se mostrará la pregunta una vez para todos los ítems)
	//Items en estado Ok o Error: Si hay adj. anteriores se mostrará la pregunta para los ítems en este estado
	var oEstItems = { bHayItemsPdte: false, bHayItemsOkError: false };
	var bMostrarConfirm = false;

	//Comprobar si están cargados los datos, si no cargarlos
	CargarDatosEmpresa();

	comprobarEstadoItems(gIdEmp, undefined, undefined, oEstItems);

	if (oEstItems.bHayItemsOkError) {
		//Estados de integración correcto o error
		if (bMantenerEstAdjEnTraspasoAdj) {
			bMostrarConfirm = HayAdjudicacionesPrevias(gAnyo, gGmn1, gCodProce, gIdEmp, undefined, undefined, undefined, gAnyo, gGmn1, gCodProce, undefined, 3, true);   //Error
			bMostrarConfirm = (bMostrarConfirm || HayAdjudicacionesPrevias(gAnyo, gGmn1, gCodProce, gIdEmp, undefined, undefined, undefined, gAnyo, gGmn1, gCodProce, undefined, 4, true));   //OK
		}
	}
	if (oEstItems.bHayItemsPdte) {
		if (!$("[id$=chkComprobarAdjAnt]").prop("checked")) {
			if (bMantenerEstAdjEnTraspasoAdj) {
				bMostrarConfirm = (bMostrarConfirm || HayAdjudicacionesPrevias(gAnyo, gGmn1, gCodProce, gIdEmp, undefined, undefined, undefined, gAnyo, gGmn1, gCodProce, undefined, -1, true));
			}
		}
	}

	bBorrarAdjAntEmpProve = false;
	if (bMostrarConfirm) {
		customConfirm(TextosJScript[28], PrevioTrasladarAdjERPDesdeEmpresaCallback, [oEstItems]);
	} else {
		PrevioTrasladarAdjERPDesdeEmpresaCallback(false, [oEstItems]);
	}
};
function PrevioTrasladarAdjERPDesdeEmpresaCallback(confirm, params) {
	if (confirm) {
		ActualizarSeleccionAdjsAnteriores(0, true);
		bBorrarAdjAntEmpProve = true;
	}

	if ((bMantenerEstAdjEnTraspasoAdj && params[0].bHayItemsPdte) || (!bMantenerEstAdjEnTraspasoAdj)) {
		var datatreeAdj = $find("<%= wdtAdjudicaciones.ClientID %>");
		var nodoEmpresa = datatreeAdj.get_selectedNodes();
		var idEmp = nodoEmpresa[0].get_key();

		if ($("[id$=chkComprobarAdjAntEmp]").prop("checked")) {
			if (HayAdjudicacionesPrevias(gAnyo, gGmn1, gCodProce, idEmp)) {
				//Si hay adjudicaciones previas mostrar la pantalla con ellas                        
				//Si no está en la primera pág. ponerlo en la primera
				var grid = $find($('[id$=whdgAdjAnt]').attr('id')).get_gridView();
				if (grid.get_behaviors().get_paging().get_pageIndex() > 0) {
					grid.get_behaviors().get_paging().set_pageIndex(0);
				}

				//Cargar el grid con las adjudicaciones trasladadas anteriormente        
				gNivelTraspaso = 1;
				var params = { Nivel: gNivelTraspaso, IdEmp: idEmp, Anyo: gAnyo, Gmn1: gGmn1, codProce: gCodProce };
				var btn = $('[id$=btnHiddenAdjAnt]').attr('id');
				__doPostBack(btn, JSON.stringify(params));
			} else {
				TrasladarAdjERPDesdeEmpresa();
			}
		} else {
			TrasladarAdjERPDesdeEmpresa();
		}
	} else {
		TrasladarAdjERPDesdeEmpresa();
	}
};
function CargarDatosEmpresa() {
	var datatreeAdj = $find("<%= wdtAdjudicaciones.ClientID %>");
	var nodoEmpresa = datatreeAdj.get_selectedNodes();
	var IdEmpresa = nodoEmpresa[0].get_key();

	var oEmpresa;
	var empresa = $.grep(gAdjudicacion.EmpresasAdjudicadas, function (emp) { return (emp.Id == IdEmpresa); });
	if (empresa.length == 0) {
		var DenEmpresa = nodoEmpresa[0].get_text();

		//Si aun no hay ninguna empresa guardada
		ProveedorAdjudicado = new Array();
		oEmpresa = { Id: IdEmpresa, Denominacion: DenEmpresa, ProveedoresAdjudicados: ProveedorAdjudicado };
		gAdjudicacion.EmpresasAdjudicadas.push(oEmpresa);
	} else {
		oEmpresa = empresa[0];
	}

	GuardarDatosPanelEmpresa(IdEmpresa, DenEmpresa);

	//Cargar datos de proveedores
	for (var y = 0; y <= oEmpresa.ProveedoresAdjudicados.length - 1; y++) {
		GuardarDatosPanelProveedor(oEmpresa.ProveedoresAdjudicados[y].Codigo, oEmpresa.ProveedoresAdjudicados[y].Denominacion, IdEmpresa)
	}

	//Cargar datos de grupos
	for (var i = 0; i <= (nodoEmpresa[0].get_childrenCount() - 1) ; i++) {
		var oProve = nodoEmpresa[0].get_childNode(i);

		for (var k = 0; k <= (oProve.get_childrenCount() - 1) ; k++) {
			var oGrupo = oProve.get_childNode(k);
			//GuardarDatosPanelGrupo(oGrupo.get_key());
			CargarItemsGrupoAlTraspasarEmpresa(IdEmpresa, oProve.get_key(), oGrupo.get_key(), oGrupo.get_valueString(), oGrupo.get_text())
		}
	}
};
function HayAdjudicacionesPrevias(Anyo, Gmn1, CodProce, IdEmp, CodProve, CodGrupo, idItem, AnyoProceAdj, Gmn1ProceAdj, codProceProceAdj, sCodProveProceAdj, EstadoActual, bActualizarDatos) {
	var resp = false;
	if (typeof (CodProve) == 'undefined') { CodProve = '' }
	if (typeof (CodGrupo) == 'undefined') { CodGrupo = 0 }
	if (typeof (idItem) == 'undefined') { idItem = 0 }
	if (typeof (AnyoProceAdj) == 'undefined') { AnyoProceAdj = 0 }
	if (typeof (Gmn1ProceAdj) == 'undefined') { Gmn1ProceAdj = '' }
	if (typeof (codProceProceAdj) == 'undefined') { codProceProceAdj = 0 }
	if (typeof (sCodProveProceAdj) == 'undefined') { sCodProveProceAdj = '' }
	if (typeof (EstadoActual) == 'undefined') { EstadoActual = -2 }
	if (typeof (bActualizarDatos) == 'undefined') { bActualizarDatos = false }
	params = {
		Anyo: Anyo, Gmn1: Gmn1, codProce: CodProce, IdEmp: IdEmp, sCodProve: CodProve, iIdGrupo: CodGrupo, iIdItem: idItem, AnyoProceAdj: AnyoProceAdj, Gmn1ProceAdj: Gmn1ProceAdj, codProceProceAdj: codProceProceAdj,
		sCodProveProceAdj: sCodProveProceAdj, iEstadoActual: EstadoActual, bActualizarDatos: bActualizarDatos
	};
	$.when($.ajax({
		type: "POST",
		url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/HayAdjudicacionesPrevias',
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(params),
		dataType: "json",
		async: false
	})).done(function (msg) {
		resp = msg.d;
	})
	return resp;
};
function TrasladarAdjERPDesdeEmpresa() {
	$("[id$=ImgProgress]").css('visibility', 'hidden') //oculto la imagen porque se queda parado el gif y da sensacion de quedarse colgado
	CambiarEstadoProgressModalPopup(TextosJScript[22], true)
	$find('<%=ModalProgress.ClientID%>').show()
	setTimeout("btnTrasladarAdjERPDesdeEmpresa_Click()", 300) //se pone el timeout para que se muestre el modal popup
};
function PrevioTrasladarAdjERPDesdeProveedor() {
	//Boton aceptar y traspasar al ERP los ítems de un proveedor
	//Se comprueba el estado de los ítems del grupo.
	//Items en estado pendiente: Si está marcado el check de comprobar adj. anteriores funcionará como hasta ahora (en el grid aparecerán sólo los ítems en este estado. Si no está marcado se mostrará la pregunta una vez para todos los ítems)
	//Items en estado Ok o Error: Si hay adj. anteriores se mostrará la pregunta para los ítems en este estado
	var oEstItems = { bHayItemsPdte: false, bHayItemsOkError: false };
	var bMostrarConfirm = false;

	CargarDatosProveedor();

	comprobarEstadoItems(gIdEmp, gCodProve, undefined, oEstItems);

	if (oEstItems.bHayItemsOkError) {
		//Estados de integración correcto o error
		if (bMantenerEstAdjEnTraspasoAdj) {
			bMostrarConfirm = HayAdjudicacionesPrevias(gAnyo, gGmn1, gCodProce, gIdEmp, gCodProve, undefined, undefined, gAnyo, gGmn1, gCodProce, gCodProve, 3, true);   //Error
			bMostrarConfirm = (bMostrarConfirm || HayAdjudicacionesPrevias(gAnyo, gGmn1, gCodProce, gIdEmp, gCodProve, undefined, undefined, gAnyo, gGmn1, gCodProce, gCodProve, 4, true));   //OK
		}
	}
	if (oEstItems.bHayItemsPdte) {
		if (!$("[id$=chkComprobarAdjAnt]").prop("checked")) {
			if (bMantenerEstAdjEnTraspasoAdj) {
				bMostrarConfirm = (bMostrarConfirm || HayAdjudicacionesPrevias(gAnyo, gGmn1, gCodProce, gIdEmp, gCodProve, undefined, undefined, gAnyo, gGmn1, gCodProce, gCodProve, -1, true));
			}
		}
	}

	bBorrarAdjAntEmpProve = false;
	if (bMostrarConfirm) {
		customConfirm(TextosJScript[28], PrevioTrasladarAdjERPDesdeProveedorCallback, [oEstItems]);
	} else {
		PrevioTrasladarAdjERPDesdeProveedorCallback(false, [oEstItems]);
	}
};
function PrevioTrasladarAdjERPDesdeProveedorCallback(confirm, params) {
	if (confirm) {
		ActualizarSeleccionAdjsAnteriores(0, true);
		bBorrarAdjAntEmpProve = true;
	}

	if ((bMantenerEstAdjEnTraspasoAdj && params[0].bHayItemsPdte) || (!bMantenerEstAdjEnTraspasoAdj)) {
		if ($("[id$=chkComprobarAdjAntProve]").prop("checked")) {
			if (HayAdjudicacionesPrevias(gAnyo, gGmn1, gCodProce, gIdEmp, gCodProve)) {
				//Si no está en la primera pág. ponerlo en la primera
				var grid = $find($('[id$=whdgAdjAnt]').attr('id')).get_gridView();
				if (grid.get_behaviors().get_paging().get_pageIndex() > 0) {
					grid.get_behaviors().get_paging().set_pageIndex(0);
				}

				//Cargar el grid con las adjudicaciones trasladadas anteriormente        
				gNivelTraspaso = 2;
				var params = { Nivel: gNivelTraspaso, IdEmp: gIdEmp, Anyo: gAnyo, Gmn1: gGmn1, codProce: gCodProce, CodProve: gCodProve };
				var btn = $('[id$=btnHiddenAdjAnt]').attr('id');
				__doPostBack(btn, JSON.stringify(params));
			}
			else {
				TrasladarAdjERPDesdeProveedor();
			}
		}
		else {
			TrasladarAdjERPDesdeProveedor();
		}
	}
	else {
		TrasladarAdjERPDesdeProveedor();
	}
};
function CargarDatosProveedor() {
	var datatreeAdj = $find("<%= wdtAdjudicaciones.ClientID %>");
	var nodoProve = datatreeAdj.get_selectedNodes();

	//Cargar datos de grupos       
	for (var k = 0; k <= (nodoProve[0].get_childrenCount() - 1) ; k++) {
		var oGrupo = nodoProve[0].get_childNode(k);
		//GuardarDatosPanelGrupo(oGrupo.get_key());
		CargarItemsGrupoAlTraspasarEmpresa(gIdEmp, nodoProve[0].get_key(), oGrupo.get_key(), oGrupo.get_valueString(), oGrupo.get_text())
	}
};
function TrasladarAdjERPDesdeProveedor() {
	$("[id$=ImgProgress]").css('visibility', 'hidden') //oculto la imagen porque se queda parado el gif y da sensacion de quedarse colgado
	CambiarEstadoProgressModalPopup(TextosJScript[22], true)
	$find('<%=ModalProgress.ClientID%>').show()
	setTimeout("btnTrasladarAdjERPDesdeProveedor_Click()", 300) //se pone el timeout para que se muestre el modal popup
};
function BorrarAdjERPDesdeProveedor() {
	$("[id$=ImgProgress]").css('visibility', 'hidden') //oculto la imagen porque se queda parado el gif y da sensacion de quedarse colgado
	CambiarEstadoProgressModalPopup(TextosJScript[23], true)
	$find('<%=ModalProgress.ClientID%>').show()
	setTimeout("btnBorrarAdjERPDesdeProveedor_Click()", 300) //se pone el timeout para que se muestre el modal popup
};
function BorrarAdjERPDesdeEmpresa() {
	$("[id$=ImgProgress]").css('visibility', 'hidden') //oculto la imagen porque se queda parado el gif y da sensacion de quedarse colgado
	CambiarEstadoProgressModalPopup(TextosJScript[23], true)
	$find('<%=ModalProgress.ClientID%>').show()
	setTimeout("btnBorrarAdjERPDesdeEmpresa_Click()", 300) //se pone el timeout para que se muestre el modal popup
};
function btnTrasladarAdjERPDesdeEmpresa_Click() {
	//funcion que traspasa al ERP los items de una empresa, desde el panel de empresa
	var TraspasarProveedor;
	var datatreeAdj = $find("<%= wdtAdjudicaciones.ClientID %>");
	var nodoEmpresa = datatreeAdj.get_selectedNodes()

	//primero guardamos la empresa por si aun no estaria guardada(se guarda cuando sale del panel empresa)
	GuardarDatosPanelEmpresa(nodoEmpresa[0].get_key(), nodoEmpresa[0].get_text())


	var idEmp = nodoEmpresa[0].get_key()

	//validamos que se hayan seleccionado los codigos ERP para la distribucion
	//Me devolvera aquellos proveedores que no esten distribuidos para no traspasarlos
	respCodsERP = ValidarCodERPTrapasoDesdeEmpresa(idEmp)

	if (respCodsERP.length == nodoEmpresa[0].get_childrenCount()) {
		//Si a ningun proveedor se le ha indicado el Cod.ERP se mostrara mensaje de advertencia
		var TextoProvsNoDistr = TextosJScript[41];
		for (var x = 0; x <= respCodsERP.length - 1; x++) {
			if (x == 0)
				TextoProvsNoDistr += respCodsERP[x]
			else
				TextoProvsNoDistr += ', ' + respCodsERP[x]
		}
		$("[id$=lblMensajeAlertaAtribsObligEnEmpresa]").text(TextoProvsNoDistr)
		$("[id$=divMensajeAlertaAtribsObligEnEmpresa]").show()
		$("[id$=ImgProgress]").css('visibility', 'visible')
		CambiarEstadoProgressModalPopup(TextosJScript[20], false)
		$find('<%=ModalProgress.ClientID%>').hide()
		return false
	} else {
		$("[id$=divMensajeAlertaAtribsObligEnEmpresa]").hide()
	}

	//////////////////////////////////////////////////////
	//VALIDACION ATRIBUTOS DE PROVEEDOR Y DE GRUPOS
	//////////////////////////////////////////////////////<
	for (var x = 0; x <= nodoEmpresa[0].getItems().get_length() - 1; x++) {
		//Recorro los proveedores de esa empresa, cargando los atributos si no los tendria ya grabados
		var ProvNode = nodoEmpresa[0].getItems().getNode(x)

		//Omito los proveedores que no van a ser traspasados
		TraspasarProveedor = true
		for (var i = 0; i <= respCodsERP.length - 1; i++) {
			if (respCodsERP[i] == ProvNode.get_key())
				TraspasarProveedor = false
		}

		if (TraspasarProveedor == true) {
			CargarAtributosProveedorAlTraspasarEmpresa(ProvNode.get_key(), ProvNode.get_text(), idEmp)
			var resp = ValidarAtributosObligatoriosDeProveedoresAlTraspasarEmpresa(idEmp, ProvNode.get_key(), 1)
			if (resp == false) {
				$("[id$=ImgProgress]").css('visibility', 'visible')
				$find('<%=ModalProgress.ClientID%>').hide()
				CambiarEstadoProgressModalPopup(TextosJScript[20], false)
				return false //Si hay algun atributo obligatorio de algun proveedor sin valor no seguimos
			}
		}
	}

	//Una vez que se han comprobado los atributos de los proveedores, comprobariamos los atributos de los grupos de los proveedores
	for (var x = 0; x <= nodoEmpresa[0].getItems().get_length() - 1; x++) {
		//Recorro los proveedores de esa empresa
		var ProvNode = nodoEmpresa[0].getItems().getNode(x)

		//Omito los proveedores que no van a ser traspasados
		TraspasarProveedor = true
		for (var i = 0; i <= respCodsERP.length - 1; i++) {
			if (respCodsERP[i] == ProvNode.get_key()) {
				TraspasarProveedor = false
				break
			}
		}

		if (TraspasarProveedor == true) {
			for (var y = 0; y <= ProvNode.getItems().get_length() - 1; y++) {
				//recorro los grupos de ese proveedor
				var GroupNode = ProvNode.getItems().getNode(y)
				CargarAtributosGrupoAlTraspasarEmpresa(GroupNode.get_key(), ProvNode.get_key(), GroupNode.get_text(), idEmp, parseInt(GroupNode.get_valueString()))
				var resp = ValidarAtributosObligatoriosDeGruposAlTraspasarEmpresa(idEmp, ProvNode.get_key(), GroupNode.get_key(), 1)
				if (resp == false) {
					$("[id$=ImgProgress]").css('visibility', 'visible')
					$find('<%=ModalProgress.ClientID%>').hide()
					CambiarEstadoProgressModalPopup(TextosJScript[20], false)
					return false //Si hay algun atributo obligatorio de algun Grupo sin valor no seguimos
				}
			}
		}
	}
	////////////////////////////////////////////////////////////////////////////
	//CARGA DE ITEMS DE CADA GRUPO Y VALIDACION DE LOS ATRIBUTOS DE LOS ITEMS
	/////////////////////////////////////////////////////////////////////////////
	for (var x = 0; x <= nodoEmpresa[0].getItems().get_length() - 1; x++) {
		//Recorro los proveedores de esa empresa
		var ProvNode = nodoEmpresa[0].getItems().getNode(x)

		//Omito los proveedores que no van a ser traspasados
		TraspasarProveedor = true
		for (var i = 0; i <= respCodsERP.length - 1; i++) {
			if (respCodsERP[i] == ProvNode.get_key()) {
				TraspasarProveedor = false
				break
			}
		}

		if (TraspasarProveedor == true) {
			for (var y = 0; y <= ProvNode.getItems().get_length() - 1; y++) {
				//recorro los grupos de ese proveedor
				var GroupNode = ProvNode.getItems().getNode(y)
				//CambiarEstadoProgressModalPopup("DCargando Atribs Items " + GroupNode.get_key(), true)
				CargarItemsGrupoAlTraspasarEmpresa(idEmp, ProvNode.get_key(), GroupNode.get_key(), GroupNode.get_valueString(), GroupNode.get_text())
				var resp = ValidarAtributosObligatoriosDeItemsAlTraspasarEmpresa(idEmp, ProvNode.get_key(), GroupNode.get_key(), 1)
				if (resp == false) {
					$("[id$=ImgProgress]").css('visibility', 'visible')
					$find('<%=ModalProgress.ClientID%>').hide()
					CambiarEstadoProgressModalPopup(TextosJScript[20], false)
					return false //Si hay algun atributo obligatorio de algun Item sin valor no seguimos
				}
			}
		}
	}

	////////////////////////////////////////
	//VALIDACIONES A MEDIDA DE INTEGRACIÓN
	////////////////////////////////////////
	if (ValidacionesIntegracion(gAdjudicacion, 0, idEmp, "", "", "") == false) {
		$("[id$=ImgProgress]").css('visibility', 'visible')
		$find('<%=ModalProgress.ClientID%>').hide()
		CambiarEstadoProgressModalPopup(TextosJScript[20], false)
		return false;
	}

	////////////////////////////////////////
	//TRASPASO DE LOS ITEMS DE CADA GRUPO
	////////////////////////////////////////
	GuardarObjetoAdjudicacion();

	for (var x = 0; x <= nodoEmpresa[0].getItems().get_length() - 1; x++) {
		//Recorro los proveedores de esa empresa
		var ProvNode = nodoEmpresa[0].getItems().getNode(x)

		//Omito los proveedores que no van a ser traspasados
		TraspasarProveedor = true
		for (var i = 0; i <= respCodsERP.length - 1; i++) {
			if (respCodsERP[i] == ProvNode.get_key()) {
				TraspasarProveedor = false
				break
			}
		}

		if (TraspasarProveedor == true) {
			for (var y = 0; y <= ProvNode.getItems().get_length() - 1; y++) {
				//recorro los grupos de ese proveedor
				var GroupNode = ProvNode.getItems().getNode(y)
				TraspasoItemsGrupoERP(idEmp, ProvNode.get_key(), GroupNode.get_key(), 1)
			}
		}
	}

	var sData = TextosJScript[34] + ': ' + gAnyo + '/' + gGmn1 + '/' + gCodProce + ', ' + TextosJScript[37] + ': ' + nodoEmpresa[0].get_text();
	RegistrarAccion(ACCTraspAdjTraspasarEmp, sData);

	$("[id$=ImgProgress]").css('visibility', 'visible');
	$find('<%=ModalProgress.ClientID%>').hide();
	CambiarEstadoProgressModalPopup(TextosJScript[20], false);
};
function BorrarItemsGrupoERP(idEmp, codProve, codGrupo, origen) {
	//funcion que borra los traspasos de una empresa, desde el panel de empresa
	var indEmp
	var arrAtributosObligatoriosVacios = new Array();
	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == idEmp) {
			indEmp = z
			break
		}
	}
	var indProve
	var oGrupo
	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == codProve) {
			for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos.length - 1; x++) {
				if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[x].Codigo == codGrupo) {
					oGrupo = gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[x]
					break
				}

			}
			break
		}
	}

	FechasAtributos()

	params = { Anyo: gAnyo, Gmn1: gGmn1, codProce: gCodProce, idEmp: idEmp, codProve: codProve, codGrupo: codGrupo };
	$.when($.ajax({
		type: "POST",
		url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/BorrarGrupoDeERP',
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(params),
		dataType: "json",
		async: false
	})).done(function (msg) {
		var resp = msg.d;
		var grid = $find("<%= whdgItems.ClientID %>");
		var oItem;
		if (resp == 1) {
			//Se ha traspasado ok
			if (oGrupo != undefined) {
				for (i = 0; i <= oGrupo.Items.length - 1; i++) {
					oItem = oGrupo.Items[i]
					if (oItem.Estado == 4) {
						oItem.Estado = 0
					}
				}
			}
			if (origen == 1) {
				$("[id$=btnBorrarAdjERPDesdeEmpresa]").css("display", "none")
				if ($("[id$=btnTrasladarAdjERPDesdeEmpresa]").css("display") == "inline") {
					$("[id$=CeldaBtnTrasladarAdjERPDesdeEmpresa]").css("width", "60%")
					$("[id$=CeldaBtnBorrarAdjERPDesdeEmpresa]").css("width", "39%")
				}
			} else {
				$("[id$=BtnBorrarAdjERPDesdeProveedor]").css("display", "none")
				if ($("[id$=BtnTrasladarAdjERPDesdeProveedor]").css("display") == "inline") {
					$("[id$=CeldaBtnTrasladarAdjERPDesdeProveedor]").css("width", "60%")
					$("[id$=CeldaBtnBorrarAdjERPDesdeProveedor]").css("width", "39%")
				}
			}

		}
	})
};
function TraspasoItemsGrupoERP(idEmp, codProve, codGrupo, origen) {
	//function que Traspaso los items del grupo desde el boton de traspaso de empresa o proveedor
	//idEmp:id de la empresa
	//codProve: codigo del proveedor
	//codGrupo: codigo del grupo
	//origen: 1 si es desde el panel de empresa, 2 si es desde el panel de proveedor
	var indEmp
	var arrAtributosObligatoriosVacios = new Array();
	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == idEmp) {
			indEmp = z
			break
		}
	}
	var indProve
	var oGrupo
	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == codProve) {
			for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos.length - 1; x++) {
				if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[x].Codigo == codGrupo) {
					oGrupo = gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[x]
					break
				}

			}
			break
		}
	}

	FechasAtributos()
	var TieneEscalados = oGrupo.TieneEscalados
	params = { Anyo: gAnyo, Gmn1: gGmn1, codProce: gCodProce, idEmp: idEmp, codProve: codProve, codGrupo: codGrupo, hayEscalados: TieneEscalados, bBorrarAdjAnt: bBorrarAdjAntEmpProve };
	$.when($.ajax({
		type: "POST",
		url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/TraspasarGrupoToERP',
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(params),
		dataType: "json",
		async: false
	})).done(function (msg) {
		bBorrarAdjAntEmpProve = false;
		var resp = msg.d;

		var oItem;
		if (resp == 1) {
			//Se ha traspasado ok
			for (i = 0; i <= oGrupo.Items.length - 1; i++) {
				oItem = oGrupo.Items[i]
				if (oItem.Estado == -1 || oItem.Estado == 3) {
					oItem.Estado = 0
				}
			}
			//Comprobamos la visibilidad de los botones de traslado y borrado
			if (origen == 1) {
				$("[id$=btnTrasladarAdjERPDesdeEmpresa]").css("display", "none")
				$('[id$=divCheckComprobarAdjAntEmp]').css("display", "none")
				if ($("[id$=btnBorrarAdjERPDesdeEmpresa]").css("display") == "inline") {
					$("[id$=CeldaBtnTrasladarAdjERPDesdeEmpresa]").css("width", "39%")
					$("[id$=CeldaBtnBorrarAdjERPDesdeEmpresa]").css("width", "60%")
				}
			} else {
				$("[id$=BtnTrasladarAdjERPDesdeProveedor]").css("display", "none")
				$('[id$=divCheckComprobarAdjAntProve]').css("display", "none")
				if ($("[id$=BtnBorrarAdjERPDesdeProveedor]").css("display") == "inline") {
					$("[id$=CeldaBtnTrasladarAdjERPDesdeProveedor]").css("width", "39%")
					$("[id$=CeldaBtnBorrarAdjERPDesdeProveedor]").css("width", "60%")
				}
			}
		} else if (resp.length > 1) {
			//la respuesta es un texto (articulos no integrados)
			if (origen == 1) {
				$("[id$=lblErrorIntegracionEmpresa]").text(resp);
				$("#alertaIntegracionEmpresa").show();
			} else if (origen == 2) {
				$("[id$=lblErrorIntegracionProveedor]").text(resp);
				$("#alertaIntegracionProveedor").show();
			}
			return resp;
		} else {
			//Ha dado error
			for (i = 0; i <= oGrupo.Items.length - 1; i++) {
				oItem = oGrupo.Items[i]
				if (oItem.Estado == -1 || oItem.Estado == 3) {
					oItem.Estado = 3
				}
			}

		}

		//Si se había mostrado el panel de adjudicaciones anteriores ocultarlo
		//Ocultar el panel de items                        
		$find('<%=mpeAdjAnt.ClientID%>').hide()
	});
};
function ValidarAtributosObligatoriosDeItemsAlTraspasarEmpresa(idEmp, codProve, codGrupo, origen) {
	//funcion que al traspasar la empresa comprobara que los atributos obligatorios de los items tengan valor
	var indEmp
	var arrAtributosObligatoriosVacios = new Array();
	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == idEmp) {
			indEmp = z
			break
		}
	}
	var indProve
	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == codProve) {
			for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos.length - 1; x++) {
				if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[x].Codigo == codGrupo) {
					for (var y = 0; y <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[x].Items.length - 1; y++) {
						for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[x].Items[y].Atributos.length - 1; z++) {
							var Atributo = gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[x].Items[y].Atributos[z]
							if (Atributo.Obligatorio == true && (Atributo.Valor === '' || Atributo.Valor == null)) {
								arrAtributosObligatoriosVacios.push(Atributo.Codigo + " (" + TextosJScript[8] + ")")
							}
						}

					}
					break
				}

			}
			break
		}
	}

	if (arrAtributosObligatoriosVacios.length > 0) {
		var TextoAtribOblig = TextosJScript[0]
		for (var x = 0; x <= arrAtributosObligatoriosVacios.length - 1; x++) {
			if (x == 0)
				TextoAtribOblig += arrAtributosObligatoriosVacios[x]
			else
				TextoAtribOblig += ', ' + arrAtributosObligatoriosVacios[x]
		}
		if (origen == 1) {
			$("[id$=lblMensajeAlertaAtribsObligEnEmpresa]").text(TextoAtribOblig)
			$("[id$=divMensajeAlertaAtribsObligEnEmpresa]").show()
		} else {
			$("[id$=lblMensajeAlertaAtribsObligEnProveedor]").text(TextoAtribOblig)
			$("[id$=divMensajeAlertaAtribsObligEnProveedor]").show()
		}

		return false
	} else {
		if (origen == 1)
			$("[id$=divMensajeAlertaAtribsObligEnEmpresa]").hide()
		else
			$("[id$=divMensajeAlertaAtribsObligEnProveedor]").hide()
		return true
	}
};
function CargarItemsGrupoAlTraspasarEmpresa(idEmp, codProve, codGrupo, idGrupo, denGrupo) {
	//Funcion que al traspasar al Erp desde empresa comprobara si los grupos tienen los items cargados en el objeto adjudicacion
	//y si no fuera asi los cargaria para comprobar sus atributos y si estan correctos, posteriormente traspasar los items al ERP
	var indEmp, bGrupoEncontrado, bSinItems
	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == idEmp) {
			indEmp = z
			break
		}
	}

	var indProve
	bGrupoEncontrado = false
	bSinItems = false
	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == codProve) {
			for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos.length - 1; x++) {
				if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[x].Codigo == codGrupo) {
					bGrupoEncontrado = true
					if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[x].Items.length == 0)
						bSinItems = true
					break
				}
			}
			//Si no se habia entrado en ese Grupo o no tiene items, le cargaremos los items del grupo
			if (bGrupoEncontrado == false || bSinItems == true) {
				var params = { codGrupo: codGrupo, Anyo: gAnyo, Gmn1: gGmn1, CodProce: gCodProce, CodProve: codProve, idEmp: idEmp };
				$.when($.ajax({
					type: "POST",
					url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/CargarItemsGrupoParaTraspaso',
					contentType: "application/json; charset=utf-8",
					data: JSON.stringify(params),
					dataType: "json",
					async: false
				})).done(function (msg) {
					var oItems = msg.d;
					if (bGrupoEncontrado) {
						gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[x].Items = oItems
						//Comprobamos si el grupo tiene escalados
						if (oItems.length > 0) {
							gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[x].TieneEscalados = oItems[0].TieneEscalados
						}
					} else {
						var TieneEscalados
						Atributo = new Array();
						Distribuidor = new Array();
						Item = new Array();
						//Comprobamos si el grupo tiene escalados
						if (oItems.length > 0) {
							TieneEscalados = oItems[0].TieneEscalados
						}
						Grupo = { Id: idGrupo, Codigo: codGrupo, Denominacion: denGrupo, Visto: 0, Atributos: Atributo, AtributosProceso: Atributo, Items: oItems, TieneEscalados: TieneEscalados }
						gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos.push(Grupo)
					}
				});
			}
		}
	}
};
function ValidarAtributosObligatoriosDeProveedoresAlTraspasarEmpresa(idEmp, codProve, tipo) {
	//funcion que al traspasar la empresa comprobara que los atributos obligatorios de los proveedores tengan valor
	//idEmp: id de la empresa
	//codProve: Codigo del proveedor
	//tipo: 1 si se llama desde el panel de empresa y 2 si es desde el panel de proveedor
	var indEmp
	var arrAtributosObligatoriosVacios = new Array();
	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == idEmp) {
			indEmp = z
			break
		}
	}
	var indProve
	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == codProve) {
			for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Atributos.length - 1; x++) {
				var Atributo = gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Atributos[x]
				if (Atributo.Obligatorio == true && (Atributo.Valor === '' || Atributo.Valor == null)) {
					arrAtributosObligatoriosVacios.push(Atributo.Codigo + " (" + TextosJScript[6] + ")")
				}
			}
			break
		}
	}

	if (arrAtributosObligatoriosVacios.length > 0) {
		var TextoAtribOblig = TextosJScript[0]
		for (var x = 0; x <= arrAtributosObligatoriosVacios.length - 1; x++) {
			if (x == 0)
				TextoAtribOblig += arrAtributosObligatoriosVacios[x]
			else
				TextoAtribOblig += ', ' + arrAtributosObligatoriosVacios[x]
		}
		if (tipo == 1) {
			$("[id$=lblMensajeAlertaAtribsObligEnEmpresa]").text(TextoAtribOblig)
			$("[id$=divMensajeAlertaAtribsObligEnEmpresa]").show()
		} else {
			$("[id$=lblMensajeAlertaAtribsObligEnProveedor]").text(TextoAtribOblig)
			$("[id$=divMensajeAlertaAtribsObligEnProveedor]").show()
		}
		return false
	} else {
		if (tipo == 1)
			$("[id$=divMensajeAlertaAtribsObligEnEmpresa]").hide()
		else
			$("[id$=divMensajeAlertaAtribsObligEnProveedor]").hide()
		return true
	}
};
function ValidarAtributosObligatoriosDeGruposAlTraspasarEmpresa(idEmp, codProve, codGrupo, origen) {
	//funcion que al traspasar la empresa comprobara que los atributos obligatorios de los grupos tengan valor
	var indEmp
	var arrAtributosObligatoriosVacios = new Array();
	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == idEmp) {
			indEmp = z
			break
		}
	}
	var indProve
	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == codProve) {
			for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos.length - 1; x++) {
				if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[x].Codigo == codGrupo) {
					for (var y = 0; y <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[x].Atributos.length - 1; y++) {
						var Atributo = gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[x].Atributos[y]
						if (Atributo.Obligatorio == true && (Atributo.Valor === '' || Atributo.Valor == null)) {
							arrAtributosObligatoriosVacios.push(Atributo.Codigo + " (" + TextosJScript[7] + ")")
						}
					}
					break
				}

			}
			break
		}
	}

	if (arrAtributosObligatoriosVacios.length > 0) {
		var TextoAtribOblig = TextosJScript[0]
		for (var x = 0; x <= arrAtributosObligatoriosVacios.length - 1; x++) {
			if (x == 0)
				TextoAtribOblig += arrAtributosObligatoriosVacios[x]
			else
				TextoAtribOblig += ', ' + arrAtributosObligatoriosVacios[x]
		}
		if (origen == 1) {
			$("[id$=lblMensajeAlertaAtribsObligEnEmpresa]").text(TextoAtribOblig)
			$("[id$=divMensajeAlertaAtribsObligEnEmpresa]").show()
		} else {
			$("[id$=lblMensajeAlertaAtribsObligEnProveedor]").text(TextoAtribOblig)
			$("[id$=divMensajeAlertaAtribsObligEnProveedor]").show()
		}

		return false
	} else {
		if (origen == 1)
			$("[id$=divMensajeAlertaAtribsObligEnEmpresa]").hide()
		else
			$("[id$=divMensajeAlertaAtribsObligEnProveedor]").hide()
		return true
	}
};
function CargarAtributosGrupoAlTraspasarEmpresa(codGrupo, codProve, denGrupo, idEmp, idGrupo) {
	//Funcion que al traspasar al Erp desde empresa comprobara si los atributos de grupo ya existen en el objeto
	//Adjudicacion y si no fuera asi los cargaria
	var indEmp, bGrupoEncontrado, bSinAtributos
	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == idEmp) {
			indEmp = z
			break
		}
	}
	var indProve
	bGrupoEncontrado = false
	bSinAtributos = false
	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == codProve) {
			if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos) {
				for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos.length - 1; x++) {
					if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[x].Codigo == codGrupo) {
						bGrupoEncontrado = true
						if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[x].Atributos.length == 0)
							bSinAtributos = true
						break
					}
				}
			}
			//Si no se habia entrado en ese Grupo o no tiene atributos le meteremos los atributos
			if (bGrupoEncontrado == false || bSinAtributos == true) {
				var params = { iAnyo: gAnyo, sGmn1: gGmn1, iCodProce: gCodProce, idEmp: idEmp, CodProve: codProve, idGrupo: idGrupo };
				$.when($.ajax({
					type: "POST",
					url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/CargarAtributosGrupo',
					contentType: "application/json; charset=utf-8",
					data: JSON.stringify(params),
					dataType: "json",
					async: false
				})).done(function (msg) {
					var oAtributos = msg.d;
					if (bGrupoEncontrado) {
						gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[x].Atributos = oAtributos
					} else {
						//en el caso de que no haya items en el grupo, desde VB no se crea el grupo en el objeto adjudicacion
						if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos == undefined) {
							gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos = new Array();
						}
						Atributo = new Array();
						Distribuidor = new Array();
						Item = new Array();
						Grupo = { Id: idGrupo, Codigo: codGrupo, Denominacion: denGrupo, Visto: 0, Atributos: oAtributos, AtributosProceso: Atributo, Items: Item, TieneEscalados: 0 }
						gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos.push(Grupo)
					}
				})
			}


		}
	}
};
function CargarAtributosProveedorAlTraspasarEmpresa(codProve, denProve, idEmp) {
	//Funcion que al traspasar al Erp desde empresa comprobara si los atributos de proveedor
	//ya existen en el objeto Adjudicacion y si no fuera asi los cargaria
	var indEmp, bProveEncontrado, bSinAtributos
	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == idEmp) {
			indEmp = z
			break
		}
	}
	var indProve
	bProveEncontrado = false
	bSinAtributos = false
	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == codProve) {
			bProveEncontrado = true
			if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Atributos.length == 0)
				bSinAtributos = true
			break
		}
	}

	//Si no se habia entrado en ese proveedor o no tiene atributos le meteremos los atributos  
	if (bProveEncontrado == false || bSinAtributos == true) {
		var params = { iAnyo: gAnyo, sGmn1: gGmn1, iCodProce: gCodProce, idEmp: idEmp, CodProve: codProve };
		$.when($.ajax({
			type: "POST",
			url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/CargarAtributosProceso',
			contentType: "application/json; charset=utf-8",
			data: JSON.stringify(params),
			dataType: "json",
			async: false
		})).done(function (msg) {
			var oAtributos = msg.d;
			if (bProveEncontrado) {
				gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Atributos = oAtributos
			} else {
				Atributo = oAtributos
				Distribuidor = new Array();
				Grupos = new Array();
				Proveedor = { Codigo: CodProveedor, Denominacion: Den, DenErpSeleccionado: '', CodErpSeleccionado: null, Atributos: Atributo, Distribuidores: Distribuidor, Grupos: Grupos }
				gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.push(Proveedor)
			}
		});
	}
};
function ObtenerColumnasEscalados(row) {
	//Funcion que devuelve las columnas que son escalados
	var colEscalados = new Array();
	var indCol = 0
	if (row == undefined) {
		var grid = $find("<%= whdgItems.ClientID %>");
		row = grid.get_gridView().get_rows().get_row(0)
	}

	var cols = row.get_grid().get_columns()
	for (var i = 0; i <= cols.get_length() - 1; i++) {
		var key = cols.get_column(i).get_key()
		if (key.indexOf("ESC_") != -1) {
			colEscalados[indCol] = key
			indCol += 1
		}
	}
	return colEscalados
};
function PrevioTraspasoERPItem(idItem, borrar, centro, codArt, estadoInt) {
	//Funcion que traspasara al ERP el item seleccionado
	if (bMantenerEstAdjEnTraspasoAdj && (estadoInt == 3 || estadoInt == 4)) {
		var bBorrarAdjAnt = false;
		//Estados de integración correcto o error            
		if (HayAdjudicacionesPrevias(gAnyo, gGmn1, gCodProce, gIdEmp, gCodProve, gIdGrupo, idItem, gAnyo, gGmn1, gCodProce, gCodProve, estadoInt, true)) {
			customConfirm(TextosJScript[28], PrevioTraspasoERPItemCallback, [idItem, borrar, centro, codArt, bBorrarAdjAnt]);
		} else {
			PrevioTraspasoERPItemCallback(false, [idItem, borrar, centro, codArt, bBorrarAdjAnt]);
		}
	} else {
		//Estado de integración pendiente   
		//Prevalece la funcionalidad del check
		if ($("[id$=chkComprobarAdjAntItem]").prop("checked")) {
			if (HayAdjudicacionesPrevias(gAnyo, gGmn1, gCodProce, gIdEmp, gCodProve, gIdGrupo, idItem)) {
				//Ocultar el panel de items            
				$("[id$=pnlItem]").hide();
				$("[id$=mpePanelItem_backgroundElement]").hide();

				//Si no está en la primera pág. ponerlo en la primera
				var grid = $find($('[id$=whdgAdjAnt]').attr('id')).get_gridView();
				if (grid.get_behaviors().get_paging().get_pageIndex() > 0) {
					grid.get_behaviors().get_paging().set_pageIndex(0);
				}

				//Cargar el grid con las adjudicaciones trasladadas anteriormente
				gNivelTraspaso = 5;
				gIdItem = idItem;
				gBorrar = borrar;
				gCentro = centro;
				gCodArt = codArt;

				var params = { Nivel: gNivelTraspaso, IdEmp: gIdEmp, Anyo: gAnyo, Gmn1: gGmn1, codProce: gCodProce, CodProve: gCodProve, IdGrupo: gIdGrupo, IdItem: idItem };
				var btn = $('[id$=btnHiddenAdjAnt]').attr('id');
				__doPostBack(btn, JSON.stringify(params));
			} else {
				TraspasoERPItem(idItem, borrar, centro, codArt);
			}
		} else {
			var bBorrarAdjAnt = false;
			if (bMantenerEstAdjEnTraspasoAdj) {
				if (HayAdjudicacionesPrevias(gAnyo, gGmn1, gCodProce, gIdEmp, gCodProve, gIdGrupo, idItem, gAnyo, gGmn1, gCodProce, gCodProve, estadoInt, true)) {
					customConfirm(TextosJScript[28], PrevioTraspasoERPItemCallback, [idItem, borrar, centro, codArt, bBorrarAdjAnt]);
				} else {
					PrevioTraspasoERPItemCallback(false, [idItem, borrar, centro, codArt, bBorrarAdjAnt]);
				}
			} else {
				PrevioTraspasoERPItemCallback(false, [idItem, borrar, centro, codArt, bBorrarAdjAnt]);
			}
		}
	}
};
function PrevioTraspasoERPItemCallback(confirm, params) {
	var bBorrarAdjAnt = params[4];
	if (confirm) {
		ActualizarSeleccionAdjsAnteriores(0, true);
		bBorrarAdjAnt = true;
	}

	TraspasoERPItem(params[0], params[1], params[2], params[3], bBorrarAdjAnt);
};
function TraspasoERPItem(idItem, borrar, centro, codArt, bBorrarAdjAnt) {
	//Funcion que traspasara al ERP el item seleccionado
	var colEscalados = new Array()
	var hayEscalados;
	colEscalados = ObtenerColumnasEscalados()
	if (colEscalados.length > 0)
		hayEscalados = 1
	else
		hayEscalados = 0

	var respAtribOblig = new Array();
	var respCodsERP = new Array();

	GuardarDatosPanelGrupo(gCodGrupo); //Guardamos por si se han modificado los atributos

	//Comprobamos que los atributos obligatorios de todos los ambitos tienen valor
	respAtribOblig = ValidarAtributosObligatorios(idItem);

	if (respAtribOblig.length > 0) {
		var TextoAtribOblig = TextosJScript[0]
		for (var x = 0; x <= respAtribOblig.length - 1; x++) {
			if (x == 0)
				TextoAtribOblig += respAtribOblig[x]
			else
				TextoAtribOblig += ', ' + respAtribOblig[x]
		}
		$("[id$=lblMensajeAlerta]").text(TextoAtribOblig)
		$("[id$=divMensajeAlerta]").show()

		return false
	} else {
		$("[id$=divMensajeAlerta]").hide()
	}

	//Comprobamos que todos los items esten distribuidos        
	respCodsERP = ValidarCodsERP(idItem, gIdEmp, gCodProve, centro)
	if (respCodsERP.length > 0) {
		var TextoItemsNoDistr = TextosJScript[40];
		for (var x = 0; x <= respCodsERP.length - 1; x++) {
			if (x == 0)
				TextoItemsNoDistr += respCodsERP[x]
			else
				TextoItemsNoDistr += ', ' + respCodsERP[x]
		}
		$("[id$=lblMensajeAlerta]").text(TextoItemsNoDistr)
		$("[id$=divMensajeAlerta]").show()

		return false
	} else {
		$("[id$=divMensajeAlerta]").hide()
	}

	////////////////////////////////////////
	//VALIDACIONES A MEDIDA DE INTEGRACIÃ“N
	////////////////////////////////////////
	if (ValidacionesIntegracion(gAdjudicacion, 3, gIdEmp, gCodProve, gCodGrupo, idItem) == false) {
		$("[id$=ImgProgress]").css('visibility', 'visible')
		$find('<%=ModalProgress.ClientID%>').hide()
		CambiarEstadoProgressModalPopup(TextosJScript[20], false)
		return false
	}

	FechasAtributos();

	//1º guardamos el objeto adjudicacion por si hubiera habido cambios y luego llamamos a la funcion para trasladar la adjudicacion al ERP
	var params = { objAdjudicacion: gAdjudicacion, arItems: new Array() };
	var iBorrar;
	if (borrar == true)
		iBorrar = 1
	else
		iBorrar = 0

	if (typeof (bBorrarAdjAnt) == 'undefined') bBorrarAdjAnt = false;

	$.when($.ajax({
		type: "POST",
		url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/GuardarObjetoAdjudicacion',
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(params),
		dataType: "json",
		async: false
	})).done(function (msg) {
		params = { Anyo: gAnyo, Gmn1: gGmn1, codProce: gCodProce, idEmp: gIdEmp, codProve: gCodProve, codGrupo: gCodGrupo, idItem: idItem, hayEscalados: hayEscalados, Borrar: iBorrar, Centro: centro, codArt: codArt, bBorrarAdjAnt: bBorrarAdjAnt };
		$.when($.ajax({
			type: "POST",
			url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/TraspasarItemToERP',
			contentType: "application/json; charset=utf-8",
			data: JSON.stringify(params),
			dataType: "json",
			async: false
		})).done(function (msg) {
			var resp = msg.d;
			var grid = $find("<%= whdgItems.ClientID %>");
			var oItem;
			if (resp == 1) {
				//Se ha traspasado ok
				var rows = grid.get_gridView().get_rows()
				var idItemFila, bMantenerVisibleBoton, centroItemFila
				for (i = 0; i <= rows.get_length() - 1; i++) {
					idItemFila = rows.get_row(i).get_cellByColumnKey("Key_ID_ITEM").get_value()
					centroItemFila = rows.get_row(i).get_cellByColumnKey("Key_CENTRO").get_value()
					codArtItemFila = rows.get_row(i).get_cellByColumnKey("Key_CODART").get_value()
					//Si la columna Key_CENTRO tiene como valor un string vacÃ­o get_value() devuelve un null
					if (centroItemFila == null) { centroItemFila = "" }
					if (idItemFila == idItem && centro == centroItemFila && codArt == codArtItemFila) {
						oItem = getItem(idItemFila, centroItemFila, codArtItemFila)
						if (oItem.Estado == -1 || oItem.Estado == 3 || oItem.Estado == 4) {
							$("[id$=img_Accion_" + oItem.Id + "_" + centro + "]").hide()
							$("[id$=img_EstadoItem_" + oItem.Id + "_" + centro + "]").attr("src", ruta + "/App_Themes/<%=Me.Page.Theme%>/images/Ico_Opcional_18.gif")
							$("[id$=span_EstadoItem_" + oItem.Id + "_" + centro + "]").text(TextosJScript[4])
							$("[id$=span_EstadoItem_" + oItem.Id + "_" + centro + "]").css("color", "black");
							$("[id$=span_UsuItem_" + oItem.Id + "_" + centro + "]").text(TextosJScript[26])
							oItem.Estado = 0
						}
					}
					oItem = getItem(idItemFila, centroItemFila, codArtItemFila)
					if (borrar) {
						if (oItem.Estado == 4)
							bMantenerVisibleBoton = true //Si todavia hay algun item en estado correcto mantendremos el boton borrar
					} else {
						if (oItem.Estado == -1 || oItem.Estado == 3)
							bMantenerVisibleBoton = true //Si todavia hay algun item en pendiente o error , mantendremos el boton borrar
					}
				}
				if (borrar) {
					if (bMantenerVisibleBoton != true) {
						$("[id$=btnBorrarAdjERP]").css("display", "none")
						if ($("[id$=btnTrasladarAdjERP]").css("display") == "inline") {
							$("[id$=CeldabtnTrasladar]").css("width", "60%")
							$("[id$=CeldabtnBorrar]").css("width", "39%")
						}
					}
				} else {
					if (bMantenerVisibleBoton != true) {
						$("[id$=btnTrasladarAdjERP]").css("display", "none")
						if ($("[id$=btnBorrarAdjERP]").css("display") == "inline") {
							$("[id$=CeldabtnTrasladar]").css("width", "39%")
							$("[id$=CeldabtnBorrar]").css("width", "60%")
						}
					}
				}
				cerrarItem();
			} else {
				if (resp.length > 1) {
					//El articulo no estÃ¡ integrado
					$("[id$=pnlItem]").show();
					$("[id$=lblErrorIntegracionArticulo]").text(resp);
					$("#alertaIntegracionArticulo").show();
					$("#hErrorIntegracion").val(1);
				} else {
					//Ha dado error
					var rows = grid.get_gridView().get_rows()
					for (i = 0; i <= rows.get_length() - 1; i++) {
						if (rows.get_row(i).get_cellByColumnKey("Key_ID_ITEM").get_value() == idItem) {
							oItem = getItem(rows.get_row(i).get_cellByColumnKey("Key_ID_ITEM").get_value(), rows.get_row(i).get_cellByColumnKey("Key_CENTRO").get_value(), rows.get_row(i).get_cellByColumnKey("Key_CODART").get_value())
							if (oItem.Estado == -1 || oItem.Estado == 3) {
								$("[id$=img_EstadoItem_" + oItem.Id + "_" + centro + "]").attr("src", ruta + "/App_Themes/<%=Me.Page.Theme%>/images/Ico_Expirado_18.gif")
								$("[id$=img_EstadoItem_" + oItem.Id + "_" + centro + "]").attr("onclick", 'MostrarPanel(this,-1)')
								$("[id$=span_EstadoItem_" + oItem.Id + "_" + centro + "]").text(TextosJScript[3])
								$("[id$=span_EstadoItem_" + oItem.Id + "_" + centro + "]").css("color", "red");
								oItem.Estado = 3
							}
							break
						}
						cerrarItem();
					}
				}
			}
		});
	});

	ConfiguracionCodERPGrupo()

	var datatreeAdj = $find("<%= wdtAdjudicaciones.ClientID %>");
	var nodoGrupo = datatreeAdj.get_selectedNodes()[0];
	var nodoProveedor = nodoGrupo.get_parentNode();
	var nodoEmpresa = nodoProveedor.get_parentNode();
	var sData = TextosJScript[34] + ': ' + gAnyo + '/' + gGmn1 + '/' + gCodProce + ', ' + TextosJScript[37] + ': ' + nodoEmpresa.get_text() + ', ' + TextosJScript[38] + ': ' + nodoProveedor.get_text() +
		', ' + TextosJScript[7] + ': ' + gCodGrupo + '-' + nodoGrupo.get_text() + ', ' + TextosJScript[8] + ': ' + codArt;
	RegistrarAccion(borrar ? ACCTraspAdjBorrarItem : ACCTraspAdjTraspasarItem, sData);
};
function GuardaValorDistribuidor(sender) {
	//funcion que guardar el porcentaje del distribuidor cuando te situas en el, para posteriormente utilizarlo en la funcion ValidaDistribucionItems
	gOldValorDistribuidor = sender.value
};
function cerrarItem() {
	//Oculta la ventana de itema y elimina mensaje de advertencia de integracion
	//Si se había mostrado el panel de adjudicaciones anteriores ocultarlo
	//Ocultar el panel de items                        
	$find('<%=mpeAdjAnt.ClientID%>').hide()
	$("#pnlItem").hide();
};
function ValidaDistribucionItems(sender, args) {
	//Funcion que ,cuando en el panel de proveedor se cambia el porcentaje de distribucion de un distribuidores
	//validara si
	var indEmp
	var CodDistr

	if (gBoolValidaFloat == false || gBoolValidaPorcentajesDistribucionProveedor == false)
		return false

	CodDistr = $("[id$=" + sender.id + "]").parent().parent().find('td:nth-child(1)').text()
	//Estas variables las utilizaremos si aceptamos redistribuir los items
	gCodDistribuidorModifDistribucionItems = CodDistr
	gPorcentajeDistribuidorModifDistribucionItems = sender.value

	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == gIdEmp) {
			indEmp = z
			break
		}
	}
	var indProve
	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == gCodProve) {
			indProve = i
			break
		}
	}
	var ItemEncontrado = false
	gArrItemsConDistintaDistribucion.splice(0)

	for (var y = 0; y <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos.length - 1; y++) {
		for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[y].Items.length - 1; x++) {
			for (var t = 0; t <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[y].Items[x].Distribuidores.length - 1; t++) {
				if (CodDistr == gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[y].Items[x].Distribuidores[t].Codigo) {
					if ((parseFloat(gOldValorDistribuidor) / 100) * gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[y].Items[x].CantAdj != parseFloat(gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[y].Items[x].Distribuidores[t].Cantidad_Adj)) {
						for (var z = 0; z <= gArrItemsConDistintaDistribucion.length - 1; z++) {
							if (gArrItemsConDistintaDistribucion[z].Id == gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[y].Items[x].Id)
								ItemEncontrado = true
						}
						if (ItemEncontrado == false)
							gArrItemsConDistintaDistribucion.push(gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[y].Items[x])
					}
				}
			}
		}
	}

	//Compruebo si hay algun item al que habria que redistribuir con lo que se ha indicado en los distribuidores del panel de proveedor
	var sMensaje
	var sItems
	sItems = ''
	sMensaje = TextosJScript[19].replace("XXXXX", CodDistr);
	if (gArrItemsConDistintaDistribucion.length > 0) {
		for (var z = 0; z <= gArrItemsConDistintaDistribucion.length - 1; z++) {
			sItems += '- ' + gArrItemsConDistintaDistribucion[z].Codigo + ' ' + gArrItemsConDistintaDistribucion[z].Denominacion + '<br>'
		}
		sMensaje = sMensaje.replace("YYYYY", sItems);
		$('[id$=imgCerrarPanelMensajeDistribuidores]').attr('src', ruta + '/images/Bt_Cerrar.png')
		$('[id$=imgPanelMensajeDistribuidores]').attr('src', ruta + '/App_Themes/<%=Me.Page.Theme%>/images/Icono_Error_Amarillo_40x40.gif')
		$('[id$=lblMensajeDistribuidores]').html(sMensaje)
		$find('<%=mpePanelMensajeDistribuidores.ClientID%>').show()
	}
};
function ValidarCodsERP(idItem, idEmp, codProve, centro) {
	//Funcion que valida a la hora de traspasar al ERP que todos los items a traspasar tengan un Codigo Erp asignado
	var arrCodsERP = new Array();
	var oItem, Distribuidor;
	var indEmp;
	var bItemDistribuido;

	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == idEmp) {
			indEmp = z
			break
		}
	}
	var indProve
	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == codProve) {
			indProve = i
			break
		}
	}

	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[z].Codigo == gCodGrupo) {
			indGrupo = z
			//Revisamos los codigos ERP de cada item
			for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[z].Items.length - 1; x++) {
				bItemDistribuido = false
				oItem = gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[z].Items[x]
				if (idItem == null || (oItem.Id == idItem && oItem.Centro == centro)) {
					if (oItem.Distribuidores.length > 0) { //Se ha entrado a los distribuidores o al detalle del item y estan cargados los distribuidores a nivel de item
						for (var v = 0; v <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[z].Items[x].Distribuidores.length - 1; v++) {
							Distribuidor = gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[z].Items[x].Distribuidores[v]
							if (Distribuidor.Cantidad_Adj > 0) {
								if (v == 0) {
									if (Distribuidor.CodErpSeleccionado != '' && Distribuidor.CodErpSeleccionado != null && Distribuidor.CodErpSeleccionado == oItem.CodERP) {
										bItemDistribuido = true
									} else {
										bItemDistribuido = false
										break
									}
								} else {
									if (Distribuidor.CodErpSeleccionado != '' && Distribuidor.CodErpSeleccionado != null) {
										bItemDistribuido = true
									} else {
										bItemDistribuido = false
										break
									}
								}
							}

						}
					} else {
						if (bItemDistribuido == false) {
							if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores.length > 0) {
								for (var v = 0; v <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores.length - 1; v++) {
									Distribuidor = gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores[v]
									if (v == 0) {
										//Proveedor principal
										if (Distribuidor.CodErpSeleccionado != '' && Distribuidor.CodErpSeleccionado != null && Distribuidor.PorcentajeDistribuido > 0 && Distribuidor.CodErpSeleccionado == oItem.CodERP) {
											bItemDistribuido = true
										}
									} else {
										if (Distribuidor.CodErpSeleccionado != '' && Distribuidor.CodErpSeleccionado != null && Distribuidor.PorcentajeDistribuido > 0) {
											bItemDistribuido = true
										}
									}
								}
							}
							if (bItemDistribuido == false) {
								if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[z].CodErpSeleccionado != '' && gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[z].CodErpSeleccionado != null) {
									bItemDistribuido = true
								}
							}
							if (bItemDistribuido == false) {
								if (oItem.CodERP != null && oItem.CodERP != '')
									bItemDistribuido = true
							}
						}
					}

					if (bItemDistribuido == false) {
						arrCodsERP.push(oItem.Codigo)
					}
				}
			}
			break;
		}
	}

	return arrCodsERP
};
function ValidarAtributosObligatorios(idItem) {
	//Funcion que valida a la hora de traspasar al ERP que todos los atributos obligatorios esten con valor
	var arrAtributosObligatoriosVacios = new Array();
	var oItem, Atributo
	var indEmp, itemYaIntegrado
	var RevisarAtribProce

	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == gIdEmp) {
			indEmp = z
			break
		}
	}
	var indProve
	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == gCodProve) {
			indProve = i
			//Revisamos si habria Atributos a nivel de proceso
			if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos.length > 0) {
				RevisarAtribProce = false
				for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos.length - 1; z++) {
					Atributo = gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos[z]
					if (Atributo.Obligatorio == true && (Atributo.Valor === '' || Atributo.Valor == null)) {
						arrAtributosObligatoriosVacios.push(gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos[z].Codigo + " (" + TextosJScript[6] + ")")
					}
				}
			} else {
				RevisarAtribProce = true
			}
			break
		}
	}
	var indItem, indGrupo
	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[z].Codigo == gCodGrupo) {
			indGrupo = z
			if (RevisarAtribProce == true) {
				for (var y = 0; y <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[z].AtributosProceso.length - 1; y++) {
					Atributo = gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[z].AtributosProceso[y]
					if (Atributo.Obligatorio == true && (Atributo.Valor === '' || Atributo.Valor == null)) {
						arrAtributosObligatoriosVacios.push(gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[z].AtributosProceso[y].Codigo + " (" + TextosJScript[6] + ")")
					}
				}
			}
			//Revisamos los atributos de grupo
			for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[z].Atributos.length - 1; x++) {
				Atributo = gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[z].Atributos[x]
				if (Atributo.Obligatorio == true && (Atributo.Valor === '' || Atributo.Valor == null)) {
					arrAtributosObligatoriosVacios.push(gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[z].Atributos[x].Codigo + " (" + TextosJScript[7] + ")")
				}
			}
			//Revisamos los atributos de los items
			for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[z].Items.length - 1; x++) {
				if (idItem == null || gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[z].Items[x].Id == idItem) {
					for (var v = 0; v <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[z].Items[x].Atributos.length - 1; v++) {
						Atributo = gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[z].Items[x].Atributos[v]
						if (Atributo.Obligatorio == true && (Atributo.Valor === '' || Atributo.Valor == null)) {
							arrAtributosObligatoriosVacios.push(gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[z].Items[x].Atributos[v].Codigo + " (" + TextosJScript[8] + " " + (v + 1).toString() + ")")
						}
					}
				}
			}
			break
		}

	}
	return arrAtributosObligatoriosVacios
};
function FechasAtributos() {
	//Funcion que pasa a fecha con formato javascript, las fechas del objeto gAdjudicacion que al volver de servidor vienen en formato de milisegundos.
	//Cuando se volvia a servidor estas fechas en formato de milisegundos daban error al intentar convertirse en fechas de formato servidor.
	var oItem, Atributo
	var indEmp, itemYaIntegrado
	var RevisarAtribProce

	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados.length - 1; i++) {
			indProve = i
			//Revisamos si habria Atributos a nivel de proceso
			if (gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[i].Atributos.length > 0) {
				RevisarAtribProce = false
				for (var v = 0; v <= gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[i].Atributos.length - 1; v++) {
					Atributo = gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[i].Atributos[v]
					if (Atributo.Tipo == '3' && Atributo.Valor != null && Atributo.Valor.toString().indexOf('Date') != -1) {
						Atributo.Valor = eval("new " + Atributo.Valor.slice(1, -1));
					}
				}
			}

			if (gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[i].Grupos) {
				for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[i].Grupos.length - 1; x++) {
					//Atributos por proceso que no se han rellenado en el panel del proveedor
					for (var y = 0; y <= gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[i].Grupos[x].AtributosProceso.length - 1; y++) {
						Atributo = gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[i].Grupos[x].AtributosProceso[y]
						if (Atributo.Tipo == '3' && Atributo.Valor != null && Atributo.Valor.toString().indexOf('Date') != -1) {
							Atributo.Valor = eval("new " + Atributo.Valor.slice(1, -1));
						}
					}
					//Atributos a nivel de grupo
					for (var y = 0; y <= gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[i].Grupos[x].Atributos.length - 1; y++) {
						Atributo = gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[i].Grupos[x].Atributos[y]
						if (Atributo.Tipo == '3' && Atributo.Valor != null && Atributo.Valor.toString().indexOf('Date') != -1) {
							Atributo.Valor = eval("new " + Atributo.Valor.slice(1, -1));
						}
					}
					//Atributos a nivel de item
					for (var y = 0; y <= gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[i].Grupos[x].Items.length - 1; y++) {
						for (var v = 0; v <= gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[i].Grupos[x].Items[y].Atributos.length - 1; v++) {
							Atributo = gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[i].Grupos[x].Items[y].Atributos[v]
							if (Atributo.Tipo == '3' && Atributo.Valor != null && Atributo.Valor.toString().indexOf('Date') != -1) {
								Atributo.Valor = eval("new " + Atributo.Valor.slice(1, -1));
							}
						}
					}
				}
			}
		}
	}
};
function PrevioTrasladarAdjERP() {
	//Boton aceptar y traspasar al ERP los ítems de un grupo
	//Se comprueba el estado de los ítems del grupo.
	//Items en estado pendiente: Si está marcado el check de comprobar adj. anteriores funcionará como hasta ahora (en el grid aparecerán sólo los ítems en este estado. Si no está marcado se mostrará la pregunta una vez para todos los ítems)
	//Items en estado Ok o Error: Si hay adj. anteriores se mostrará la pregunta para los ítems en este estado
	var oEstItems = { bHayItemsPdte: false, bHayItemsOkError: false };
	var bMostrarConfirm = false;

	comprobarEstadoItems(gIdEmp, gCodProve, gIdGrupo, oEstItems);

	if (oEstItems.bHayItemsOkError) {
		//Estados de integración correcto o error
		if (bMantenerEstAdjEnTraspasoAdj) {
			bMostrarConfirm = HayAdjudicacionesPrevias(gAnyo, gGmn1, gCodProce, gIdEmp, gCodProve, gIdGrupo, undefined, gAnyo, gGmn1, gCodProce, gCodProve, 3, true);   //Error
			bMostrarConfirm = (bMostrarConfirm || HayAdjudicacionesPrevias(gAnyo, gGmn1, gCodProce, gIdEmp, gCodProve, gIdGrupo, undefined, gAnyo, gGmn1, gCodProce, gCodProve, 4, true));   //OK
		}
	}
	if (oEstItems.bHayItemsPdte) {
		if (!$("[id$=chkComprobarAdjAnt]").prop("checked")) {
			if (bMantenerEstAdjEnTraspasoAdj) {
				bMostrarConfirm = (bMostrarConfirm || HayAdjudicacionesPrevias(gAnyo, gGmn1, gCodProce, gIdEmp, gCodProve, gIdGrupo, undefined, gAnyo, gGmn1, gCodProce, gCodProve, -1, true));
			}
		}
	}

	bBorrarAdjAntEmpProve = false;
	if (bMostrarConfirm) {
		customConfirm(TextosJScript[28], PrevioTrasladarAdjERPCallback, [oEstItems]);
	} else {
		PrevioTrasladarAdjERPCallback(false, [oEstItems]);
	}
};
function PrevioTrasladarAdjERPCallback(confirm, params) {
	if (confirm) {
		ActualizarSeleccionAdjsAnteriores(0, true);
		bBorrarAdjAntEmpProve = true;
	}

	if ((bMantenerEstAdjEnTraspasoAdj && params[0].bHayItemsPdte) || (!bMantenerEstAdjEnTraspasoAdj)) {
		if ($("[id$=chkComprobarAdjAnt]").prop("checked")) {
			if (HayAdjudicacionesPrevias(gAnyo, gGmn1, gCodProce, gIdEmp, gCodProve, gIdGrupo)) {
				//Si no está en la primera pág. ponerlo en la primera
				var grid = $find($('[id$=whdgAdjAnt]').attr('id')).get_gridView();
				if (grid.get_behaviors().get_paging().get_pageIndex() > 0) {
					grid.get_behaviors().get_paging().set_pageIndex(0);
				}

				//Cargar el grid con las adjudicaciones trasladadas anteriormente                    
				gNivelTraspaso = 3;
				var params = { Nivel: gNivelTraspaso, IdEmp: gIdEmp, Anyo: gAnyo, Gmn1: gGmn1, codProce: gCodProce, CodProve: gCodProve, IdGrupo: gIdGrupo };
				var btn = $('[id$=btnHiddenAdjAnt]').attr('id');
				__doPostBack(btn, JSON.stringify(params));
			} else {
				btnTrasladarAdjERP_Click();
			}
		} else {
			btnTrasladarAdjERP_Click();
		}
	} else {
		btnTrasladarAdjERP_Click();
	}
};
function comprobarEstadoItems(gIdEmp, gCodProve, gIdGrupo, oEstItems) {
	//<summary>Función que comprueba el estado de los ítems</summary>
	var empresa = $.grep(gAdjudicacion.EmpresasAdjudicadas, function (emp) { return (emp.Id == gIdEmp); });
	if (empresa.length) {
		if (typeof gCodProve !== 'undefined') {
			var proveedor = $.grep(empresa[0].ProveedoresAdjudicados, function (prove) { return (prove.Codigo == gCodProve); });
			if (proveedor.length) {
				if (typeof gIdGrupo !== 'undefined') {
					var grupo = $.grep(proveedor[0].Grupos, function (gr) { return (gr.Codigo == gCodGrupo); });
					if (grupo.length) {
						for (var i = 0; i <= grupo[0].Items.length - 1; i++) {
							if (grupo[0].Items[i].Estado == 3 || grupo[0].Items[i].Estado == 4) oEstItems.bHayItemsOkError = true;
							if (grupo[0].Items[i].Estado == -1) oEstItems.bHayItemsPdte = true;
						}
					}
				}
				else {
					for (var j = 0; j <= proveedor[0].Grupos.length - 1; j++) {
						for (var i = 0; i <= proveedor[0].Grupos[j].Items.length - 1; i++) {
							if (proveedor[0].Grupos[j].Items[i].Estado == 3 || proveedor[0].Grupos[j].Items[i].Estado == 4) oEstItems.bHayItemsOkError = true;
							if (proveedor[0].Grupos[j].Items[i].Estado == -1) oEstItems.bHayItemsPdte = true;
						}
					}
				}
			}
		}
		else {
			for (var k = 0; k <= empresa[0].ProveedoresAdjudicados.length - 1; k++) {
				for (var j = 0; j <= empresa[0].ProveedoresAdjudicados[k].Grupos.length - 1; j++) {
					for (var i = 0; i <= empresa[0].ProveedoresAdjudicados[k].Grupos[j].Items.length - 1; i++) {
						if (empresa[0].ProveedoresAdjudicados[k].Grupos[j].Items[i].Estado == 3 || empresa[0].ProveedoresAdjudicados[k].Grupos[j].Items[i].Estado == 4) oEstItems.bHayItemsOkError = true;
						if (empresa[0].ProveedoresAdjudicados[k].Grupos[j].Items[i].Estado == -1) oEstItems.bHayItemsPdte = true;
					}
				}
			}
		}
	}
};
function btnTrasladarAdjERP_Click() {
	//Funcion que traspasa todos los items del grupo que esten en estado de ser traspasados, ya se en estado pendiente o en error
	GuardarDatosPanelGrupo(gCodGrupo)

	var colEscalados = new Array()
	var hayEscalados;
	colEscalados = ObtenerColumnasEscalados()
	if (colEscalados.length > 0)
		hayEscalados = 1
	else
		hayEscalados = 0

	var respAtribOblig = new Array();
	var respCodsERP = new Array();
	//Comprobamos que los atributos obligatorios de todos los ambitos tienen valor
	respAtribOblig = ValidarAtributosObligatorios(null);

	if (respAtribOblig.length > 0) {
		var TextoAtribOblig = TextosJScript[0]
		for (var x = 0; x <= respAtribOblig.length - 1; x++) {
			if (x == 0)
				TextoAtribOblig += respAtribOblig[x]
			else
				TextoAtribOblig += ', ' + respAtribOblig[x]
		}
		$("[id$=lblMensajeAlerta]").text(TextoAtribOblig)
		$("[id$=divMensajeAlerta]").show()

		return false
	} else {
		$("[id$=divMensajeAlerta]").hide()
	}

	//Comprobamos que todos los items esten distribuidos
	respCodsERP = ValidarCodsERP(null, gIdEmp, gCodProve, null)
	if (respCodsERP.length > 0) {
		var TextoItemsNoDistr = TextosJScript[40];
		for (var x = 0; x <= respCodsERP.length - 1; x++) {
			if (x == 0)
				TextoItemsNoDistr += respCodsERP[x]
			else
				TextoItemsNoDistr += ', ' + respCodsERP[x]
		}
		$("[id$=lblMensajeAlerta]").text(TextoItemsNoDistr)
		$("[id$=divMensajeAlerta]").show()

		return false
	} else {
		$("[id$=divMensajeAlerta]").hide()
	}

	////////////////////////////////////////
	//VALIDACIONES A MEDIDA DE INTEGRACIÃ“N
	////////////////////////////////////////
	if (ValidacionesIntegracion(gAdjudicacion, 2, gIdEmp, gCodProve, gCodGrupo, "") == false) {
		$("[id$=ImgProgress]").css('visibility', 'visible')
		$find('<%=ModalProgress.ClientID%>').hide()
		CambiarEstadoProgressModalPopup(TextosJScript[20], false)
		return false
	}

	FechasAtributos();

	//1º guardamos el objeto adjudicacion por si hubiera habido cambios y luego llamamos a la funcion para trasladar la adjudicacion al ERP
	var params = { objAdjudicacion: gAdjudicacion, arItems: new Array() };
	$.when($.ajax({
		type: "POST",
		url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/GuardarObjetoAdjudicacion',
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(params),
		dataType: "json",
		async: false
	})).done(function (msg) {
		params = { Anyo: gAnyo, Gmn1: gGmn1, codProce: gCodProce, idEmp: gIdEmp, codProve: gCodProve, codGrupo: gCodGrupo, hayEscalados: hayEscalados, bBorrarAdjAnt: bBorrarAdjAntEmpProve };
		$.when($.ajax({
			type: "POST",
			url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/TraspasarGrupoToERP',
			contentType: "application/json; charset=utf-8",
			data: JSON.stringify(params),
			dataType: "json",
			async: false
		})).done(function (msg) {
			bBorrarAdjAntEmpProve = false;

			var resp = msg.d;
			var grid = $find("<%= whdgItems.ClientID %>");
			var oItem;
			if (resp == 1) {
				var arItems = new Array();

				//Se ha traspasado ok
				var rows = grid.get_gridView().get_rows()
				for (i = 0; i <= rows.get_length() - 1; i++) {
					oItem = getItem(rows.get_row(i).get_cellByColumnKey("Key_ID_ITEM").get_value(), rows.get_row(i).get_cellByColumnKey("Key_CENTRO").get_value(), rows.get_row(i).get_cellByColumnKey("Key_CODART").get_value())
					if (oItem.Estado == -1 || oItem.Estado == 3) {
						$("#img_Accion_" + oItem.Id + "_" + oItem.Centro).hide()
						$("#img_EstadoItem_" + oItem.Id + "_" + oItem.Centro).attr("src", ruta + "/App_Themes/<%=Me.Page.Theme%>/images/Ico_Opcional_18.gif")
						$("#span_EstadoItem_" + oItem.Id + "_" + oItem.Centro).text(TextosJScript[4])
						$("#span_EstadoItem_" + oItem.Id + "_" + oItem.Centro).css("color", "black");
						$("#span_UsuItem_" + oItem.Id + "_" + oItem.Centro).text(TextosJScript[26])
						oItem.Estado = 0

						arItems.push(oItem.Id);
					}
				}
				//Comprobamos la visibilidad de los botones de traslado y borrado
				$("[id$=btnTrasladarAdjERP]").css("display", "none")
				$('[id$=divCheckComprobarAdjAnt]').css("display", "none")
				if ($("[id$=btnBorrarAdjERP]").css("display") == "inline") {
					$("[id$=CeldabtnTrasladar]").css("width", "39%")
					$("[id$=CeldabtnBorrar]").css("width", "60%")
				}

				GuardarObjetoAdjudicacion(arItems);
			} else if (resp.length > 1) {
				//Articulos no integrados
				$("[id$=lblErrorIntegracionGrupo]").text(resp)
				$("#alertaIntegracionGrupo").show();
			} else {
				//Ha dado error
				var rows = grid.get_gridView().get_rows()
				for (i = 0; i <= rows.get_length() - 1; i++) {
					oItem = getItem(rows.get_row(i).get_cellByColumnKey("Key_ID_ITEM").get_value(), rows.get_row(i).get_cellByColumnKey("Key_CENTRO").get_value(), rows.get_row(i).get_cellByColumnKey("Key_CODART").get_value())
					if (oItem.Estado == -1 || oItem.Estado == 3) {
						$("#img_EstadoItem_" + oItem.Id + "_" + oItem.Centro).attr("src", ruta + "/App_Themes/<%=Me.Page.Theme%>/images/Ico_Expirado_18.gif")
						$("#span_EstadoItem_" + oItem.Id + "_" + oItem.Centro).text(TextosJScript[3])
						$("#span_EstadoItem_" + oItem.Id + "_" + oItem.Centro).css("color", "red");
						oItem.Estado = 3
					}
				}
			}
			//Si se había mostrado el panel de adjudicaciones anteriores ocultarlo
			//Ocultar el panel de items                        
			$find('<%=mpeAdjAnt.ClientID%>').hide()
		});
	});

	ConfiguracionCodERPGrupo();

	var datatreeAdj = $find("<%= wdtAdjudicaciones.ClientID %>");
	var nodoGrupo = datatreeAdj.get_selectedNodes()[0];
	var nodoProveedor = nodoGrupo.get_parentNode();
	var nodoEmpresa = nodoProveedor.get_parentNode();
	var sData = TextosJScript[34] + ': ' + gAnyo + '/' + gGmn1 + '/' + gCodProce + ', ' + TextosJScript[37] + ': ' + nodoEmpresa.get_text() + ', ' + TextosJScript[38] + ': ' + nodoProveedor.get_text() +
		', ' + TextosJScript[7] + ': ' + gCodGrupo + '-' + nodoGrupo.get_text();
	RegistrarAccion(ACCTraspAdjTraspasarGrupo, sData);
};
function btnBorrarAdjERP_Click() {
	FechasAtributos();

	//1º guardamos el objeto adjudicacion por si hubiera habido cambios y luego llamamos a la funcion para trasladar la adjudicacion al ERP
	var params = { objAdjudicacion: gAdjudicacion, arItems: new Array() };
	$.when($.ajax({
		type: "POST",
		url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/GuardarObjetoAdjudicacion',
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(params),
		dataType: "json",
		async: false
	})).done(function (msg) {
		params = { Anyo: gAnyo, Gmn1: gGmn1, codProce: gCodProce, idEmp: gIdEmp, codProve: gCodProve, codGrupo: gCodGrupo };
		$.when($.ajax({
			type: "POST",
			url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/BorrarGrupoDeERP',
			contentType: "application/json; charset=utf-8",
			data: JSON.stringify(params),
			dataType: "json",
			async: false
		})).done(function (msg) {
			var resp = msg.d;
			var grid = $find("<%= whdgItems.ClientID %>");
			var oItem;
			if (resp == 1) {
				//Se ha borrado ok
				var rows = grid.get_gridView().get_rows()
				for (i = 0; i <= rows.get_length() - 1; i++) {
					oItem = getItem(rows.get_row(i).get_cellByColumnKey("Key_ID_ITEM").get_value(), rows.get_row(i).get_cellByColumnKey("Key_CENTRO").get_value(), rows.get_row(i).get_cellByColumnKey("Key_CODART").get_value())
					if (oItem.Estado == 4) {
						$("[id$=img_Accion_" + oItem.Id + "_" + oItem.Centro + "]").hide()
						$("[id$=img_EstadoItem_" + oItem.Id + "_" + oItem.Centro + "]").attr("src", ruta + "/App_Themes/<%=Me.Page.Theme%>/images/Ico_Opcional_18.gif")
						$("[id$=span_EstadoItem_" + oItem.Id + "_" + oItem.Centro + "]").text(TextosJScript[4])
						$("[id$=span_EstadoItem_" + oItem.Id + "_" + oItem.Centro + "]").css("color", "black");
						$("#span_UsuItem_" + oItem.Id + "_" + oItem.Centro).text(TextosJScript[26])
						oItem.Estado = 0
					}
				}
				$("[id$=btnBorrarAdjERP]").css("display", "none")
				if ($("[id$=btnTrasladarAdjERP]").css("display") == "inline") {
					$("[id$=CeldabtnTrasladar]").css("width", "60%")
					$("[id$=CeldabtnBorrar]").css("width", "39%")
					$("[id$=btnTrasladarAdjERP]").css("float", "left")
				}
			}

			var datatreeAdj = $find("<%= wdtAdjudicaciones.ClientID %>");
			var nodoGrupo = datatreeAdj.get_selectedNodes()[0];
			var nodoProveedor = nodoGrupo.get_parentNode();
			var nodoEmpresa = nodoProveedor.get_parentNode();
			var sData = TextosJScript[34] + ': ' + gAnyo + '/' + gGmn1 + '/' + gCodProce + ', ' + TextosJScript[37] + ': ' + nodoEmpresa.get_text() + ', ' + TextosJScript[38] + ': ' + nodoProveedor.get_text() +
				', ' + TextosJScript[7] + ': ' + gCodGrupo + '-' + nodoGrupo.get_text();
			RegistrarAccion(ACCTraspAdjBorrarGrupo, sData);
		})
	})
};
function whdgItems_CellClick(sender, args) {
	//Funcion que salta al hacer click en el 
	// de los items de cada grupo
	var oItem
	var posicionX, posicionY
	if (args.get_type() == "cell") {
		ObtenerObjetoAdjudicacion();

		var row = args.get_item().get_row()

		if (row.get_index() == -1) //Si pincha en la fila de filtros salimos de la funcion
			return false
		editRow = row
		var idItem = row.get_cellByColumnKey("Key_ID_ITEM").get_value()
		var uon1 = row.get_cellByColumnKey("Key_UON1").get_value()
		var uon2 = row.get_cellByColumnKey("Key_UON2").get_value()
		var uon3 = row.get_cellByColumnKey("Key_UON3").get_value()
		var centro = row.get_cellByColumnKey("Key_CENTRO").get_value()
		if (centro == null) centro = ''
		var codArt = row.get_cellByColumnKey("Key_CODART").get_value()
		gCentro = centro;
		gCodArt = codArt;
		var oItem = getItem(idItem, centro, codArt)
		if (!oItem) {
			return false
		}

		if (oItem.TieneEscalados == 1) {
			var colEscalados = ObtenerColumnasEscalados(row)
		} else {
			var cant_adj = row.get_cellByColumnKey("Key_CANT_ADJ").get_text()
			var prec_adj = row.get_cellByColumnKey("Key_PREC_ADJ").get_text()
		}

		var fecini = row.get_cellByColumnKey("Key_FECINI_SUM").get_text()
		var fecfin = row.get_cellByColumnKey("Key_FECFIN_SUM").get_text()

		var denart = row.get_cellByColumnKey("Key_DENART").get_value()
		var estado = row.get_cellByColumnKey("Key_ESTADOINT").get_value()
		var cod_erp = row.get_cellByColumnKey("Key_PROVE_ERP").get_value()
		var fecenv = row.get_cellByColumnKey("Key_FECENV").get_text()

		gIdItem = idItem
		oItem = getItem(idItem, centro, codArt)

		var usu = $("#span_UsuItem_" + oItem.Id + "_" + oItem.Centro).text()
		switch (args.get_item().get_column().get_key()) {
			case "Key_DISTRIBUIDORES":
				//Muestra el panel de distribuidores del item
				$find('<%=mpePanelDistribuidores.ClientID%>').show()
				CargarDistribuidoresProveedorItem(gCodProve, gIdEmp, gAnyo, gGmn1, gCodProce, idItem, false, uon1, uon2, uon3, centro)
				switch (oItem.Estado) {
					case -1:
						$("[id$=btnAceptarDistrItem]").show()
						break;
					case 0:
					case 1:
						$("[id$=btnAceptarDistrItem]").hide()
						break;
					case 3:
						$("[id$=btnAceptarDistrItem]").show()
						break;
					case 4:
						$("[id$=btnAceptarDistrItem]").hide()
						break
				}
				break;
			case "Key_USU":
				if (usu != null) {
					posicionX = $(row.get_cellByColumnKey("Key_USU").get_element()).position().left + ($(row.get_cellByColumnKey("Key_USU").get_element()).innerWidth() / 4);
					posicionY = $(row.get_cellByColumnKey("Key_USU").get_element()).position().top + 20;
					FSNMostrarPanel(PeticionarioAnimationClientID, args, PeticionarioDynamicPopulateClienteID, usu, null, null, posicionX, posicionY);
				}
				break
			case "Key_ESTADOINT_ICO":
				args.set_cancel(true);
				return false;
				break;
			case "Key_ACCION":
			case "Key_ESTADOINT_TEXT":
			case "Key_ESTADOINT":
			case "Key_CANT_ADJ":
				break;
			default:
				//Muestra el panel de edicion del item
				$("#alertaIntegracionArticulo").hide();
				switch (oItem.Estado) {
					case -1:
						$("[id$=lblEstadoIntegracionItemDato]").text(TextosJScript[9])
						$("[id$=lblEstadoIntegracionItemDato]").css("color", "orange");
						$("[id$=imgEstadoIntegracionItem]").attr("src", ruta + "/App_Themes/<%=Me.Page.Theme%>/images/Ico_Pendiente.gif")
						$("[id$=imgTraspasoERPItem]").attr("src", ruta + "/App_Themes/<%=Me.Page.Theme%>/images/TraspasoERP.PNG")
						$("[id$=lblTraspasoERPItem]").text(TextosJScript[10])
						$("[id$=imgTraspasoERPItem]").show()
						$("[id$=lblTraspasoERPItem]").show()
						$("[id$=divCheckComprobarAdjAntItem]").show()
						$("[id$=btnAceptarTraspasar]").show()
						$("[id$=btnAceptar]").show()
						$("[id$=btnCerrar]").children().children().find('tr:nth-child(1)').find('td:nth-child(2)').text(TextosJScript[16])
						break;
					case 0:
					case 1:
						$("[id$=lblEstadoIntegracionItemDato]").text(TextosJScript[4])
						$("[id$=lblEstadoIntegracionItemDato]").css("color", "black");
						$("[id$=imgEstadoIntegracionItem]").attr("src", ruta + "/App_Themes/<%=Me.Page.Theme%>/images/Ico_Opcional.gif")
						$("[id$=imgTraspasoERPItem]").hide()
						$("[id$=lblTraspasoERPItem]").hide()
						$("[id$=divCheckComprobarAdjAntItem]").hide()
						$("[id$=btnAceptarTraspasar]").hide()
						$("[id$=btnAceptar]").hide()
						$("[id$=btnCerrar]").children().children().find('tr:nth-child(1)').find('td:nth-child(2)').text(TextosJScript[15])
						break;
					case 3:
						$("[id$=lblEstadoIntegracionItemDato]").text(TextosJScript[3])
						$("[id$=lblEstadoIntegracionItemDato]").css("color", "red");
						$("[id$=imgEstadoIntegracionItem]").attr("src", ruta + "/App_Themes/<%=Me.Page.Theme%>/images/Ico_Expirado.gif")
						$("[id$=imgTraspasoERPItem]").attr("src", ruta + "/App_Themes/<%=Me.Page.Theme%>/images/TraspasoERP.PNG")
						$("[id$=lblTraspasoERPItem]").text(TextosJScript[10])
						$("[id$=imgTraspasoERPItem]").show()
						$("[id$=lblTraspasoERPItem]").show()
						$("[id$=divCheckComprobarAdjAntItem]").show()
						$("[id$=btnAceptarTraspasar]").show()
						$("[id$=btnAceptar]").show()
						$("[id$=btnCerrar]").children().children().find('tr:nth-child(1)').find('td:nth-child(2)').text(TextosJScript[16])
						break;
					case 4:
						$("[id$=lblEstadoIntegracionItemDato]").text(TextosJScript[12])
						$("[id$=lblEstadoIntegracionItemDato]").css("color", "green");
						$("[id$=imgEstadoIntegracionItem]").attr("src", ruta + "/App_Themes/<%=Me.Page.Theme%>/images/Ico_Vigente.gif")
						$("[id$=imgTraspasoERPItem]").attr("src", ruta + "/App_Themes/<%=Me.Page.Theme%>/images/eliminar.PNG")
						$("[id$=lblTraspasoERPItem]").text(TextosJScript[11])
						$("[id$=imgTraspasoERPItem]").show()
						$("[id$=lblTraspasoERPItem]").show()
						if (bMantenerEstAdjEnTraspasoAdj) {
							$("[id$=divCheckComprobarAdjAntItem]").show()
							$("[id$=btnAceptarTraspasar]").show()
						} else {
							$("[id$=divCheckComprobarAdjAntItem]").hide()
							$("[id$=btnAceptarTraspasar]").hide()
						}
						$("[id$=btnAceptar]").hide()
						$("[id$=btnCerrar]").children().children().find('tr:nth-child(1)').find('td:nth-child(2)').text(TextosJScript[15])
						break
				}
				if (fecenv === '' && usu === '') { //El usuario se graba en LOG_ITEM_ADJ al traspasar el item, pero la fecha(FECENV) la tendremos cuando el item se integre en el ERP
					$("[id$=lblIntegradoPor]").hide()
				} else if (fecenv === '' && usu != '') {
					$("[id$=lblIntegradoPor]").show()
					$("[id$=lblIntegradoPor]").text(TextosJScript[27] + ' ' + usu)
				} else if (fecenv != '' && usu != '') {
					$("[id$=lblIntegradoPor]").show()
					$("[id$=lblIntegradoPor]").text(TextosJScript[25].toString().replace("XXX", usu).toString().replace("YYY", fecenv))
				}

				$("[id$=lblCabeceraItem]").text(codArt + ' - ' + denart)
				$("[id$=lblCantAdjItemDato]").text(cant_adj)
				$("[id$=txtCantAdjItemDato]").val(cant_adj)
				$("[id$=lblPrecUniItemDato]").text(prec_adj)
				$("[id$=lblFecIniItemDato]").text(fecini)
				$("[id$=lblFecFinItemDato]").text(fecfin)

				if (cod_erp != '') {
					$("[id$=cmbCodigosERPItem]").children("option[value=" + cod_erp + "]").attr("selected", true);
					$('[id$=cmbCodigosERPItem]').trigger("onchange");
				} else {
					$("[id$=cmbCodigosERPItem]").children("option[value='']").attr("selected", true);
					$("[id$=divCampoPer1Item]").hide()
					$("[id$=divCampoPer2Item]").hide()
					$("[id$=divCampoPer3Item]").hide()
					$("[id$=divCampoPer4Item]").hide()
				}

				//Incidencia pruebas 2014/667: Mostrar el combo siempre que tengamos distintas opciones para poder modificarlo.
				//Si no hay centro, cuando se muestre el panel de edicion del item se tendra que cargar una combo con los distintos centros
				var params = { Uon1: uon1, Uon2: uon2, Uon3: uon3, codArt: codArt };
				$.when($.ajax({
					type: "POST",
					url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/CargarCentrosItem',
					contentType: "application/json; charset=utf-8",
					data: JSON.stringify(params),
					dataType: "json",
					async: false
				})).done(function (msg) {
					var oCentros = msg.d;
					$("[id$=divComboCentro]").show()
					$("[id$=divLblCentro]").hide()
					$("#cmbCentros").empty()

					if ($(oCentros).length > 1) {
						$("<option value=''></option>").appendTo("#cmbCentros");
						$(oCentros).each(function () {
							$("<option value='" + this.Denominacion + "'>" + this.Denominacion + "</option>").appendTo("#cmbCentros");
						});

						if (oItem.Centro != "")
							$("#cmbCentros option[value=" + oItem.Centro + "]").attr("selected", true);

						$("#divTraspasoCentro").show();
						$("#chkTraspasoCentro").show();
					} else {
						//Si hay centro se pone en un label
						$("[id$=divComboCentro]").hide()
						$("[id$=divLblCentro]").show()
						$("#lblCentro").text(centro)
						$("#divTraspasoCentro").hide();
						$("#chkTraspasoCentro").hide();
					}
				})

				CargarAtributosItem(gIdEmp, gCodProve, idItem, oItem.LIAId, centro, codArt);

				CargarDistribuidoresProveedorItem(gCodProve, gIdEmp, gAnyo, gGmn1, gCodProce, idItem, true, uon1, uon2, uon3, centro);
				if (oItem.TieneEscalados == 1) {
					$("#divEscalados").empty()
					$("#divCantEscalados").css('display', 'inline-block');
					$("#divCantPrecItem").hide()
					$(oItem.Escalados).each(function () {
						$("#tmplEscalado").tmpl(this).appendTo("#divEscalados");
					})
				} else {
					$("#divCantEscalados").hide()
					$("#divCantPrecItem").css('display', 'inline-block');
				}

				$find('<%=mpePanelItem.ClientID%>').show();
				break
		}
	}
};
function PrevioAceptarTraspasarItem() {
	//Boton aceptar y traspasar al ERP del panel de edicion del item
	hPostBack = true;
	var oItem = getItem(gIdItem, gCentro, gCodArt);
	if (bMantenerEstAdjEnTraspasoAdj && (oItem.Estado == 3 || oItem.Estado == 4)) {
		var bBorrarAdjAnt = false;
		//Estados de integración correcto o error                        
		if (HayAdjudicacionesPrevias(gAnyo, gGmn1, gCodProce, gIdEmp, gCodProve, gIdGrupo, oItem.Id, gAnyo, gGmn1, gCodProce, gCodProve, undefined, true)) {
			customConfirm(TextosJScript[28], PrevioAceptarTraspasarItemCallback, [hPostBack, bBorrarAdjAnt]);
		} else {
			PrevioAceptarTraspasarItemCallback(false, [hPostBack, bBorrarAdjAnt]);
		}
	} else {
		//Estado de integración pendiente   
		//Prevalece la funcionalidad del check
		if ($("[id$=chkComprobarAdjAntItem]").prop("checked")) {
			if (HayAdjudicacionesPrevias(gAnyo, gGmn1, gCodProce, gIdEmp, gCodProve, gIdGrupo, gIdItem)) {
				//Ocultar el panel de items            
				$("[id$=pnlItem]").hide();
				$("[id$=mpePanelItem_backgroundElement]").hide();

				//Cargar el grid con las adjudicaciones trasladadas anteriormente
				gNivelTraspaso = 4;
				var params = { Nivel: gNivelTraspaso, IdEmp: gIdEmp, Anyo: gAnyo, Gmn1: gGmn1, codProce: gCodProce, CodProve: gCodProve, IdGrupo: gIdGrupo, IdItem: gIdItem };
				var btn = $('[id$=btnHiddenAdjAnt]').attr('id');
				__doPostBack(btn, JSON.stringify(params));

				//Si no está en la primera pág. ponerlo en la primera
				var grid = $find($('[id$=whdgAdjAnt]').attr('id')).get_gridView();
				if (grid.get_behaviors().get_paging().get_pageIndex() > 0) {
					grid.get_behaviors().get_paging().set_pageIndex(0);
				}

				hPostBack = false;
			} else {
				btnAceptarItem_Click();
				EnvioItem(false);
			}

			if (hPostBack) {
				postBackCambioItems(gIdEmp, gCodProve, gCodGrupo, gIdGrupo, gIdItem);
			}
		} else {
			var bBorrarAdjAnt = false;
			if (bMantenerEstAdjEnTraspasoAdj) {
				if (HayAdjudicacionesPrevias(gAnyo, gGmn1, gCodProce, gIdEmp, gCodProve, gIdGrupo, gIdItem, gAnyo, gGmn1, gCodProce, gCodProve, undefined, true)) {
					customConfirm(TextosJScript[28], PrevioAceptarTraspasarItemCallback, [hPostBack, bBorrarAdjAnt]);
				} else {
					PrevioAceptarTraspasarItemCallback(false, [hPostBack, bBorrarAdjAnt]);
				}
			} else {
				PrevioAceptarTraspasarItemCallback(false, [hPostBack, bBorrarAdjAnt]);
			}
		}
	}
};
function PrevioAceptarTraspasarItemCallback(confirm, params) {
	var bBorrarAdjAnt = params[1];
	if (confirm) {
		ActualizarSeleccionAdjsAnteriores(0, true);
		bBorrarAdjAnt = true;
	}

	btnAceptarItem_Click();
	EnvioItem(bBorrarAdjAnt);

	if (params[0]) {
		postBackCambioItems(gIdEmp, gCodProve, gCodGrupo, gIdGrupo, gIdItem);
	}
};
function btnAceptarItem_Click() {
	//Boton aceptar del panel de edicion del item
	var indEmp, indProv, indGrupo, indItem, oItem, CantDistrTotal = 0
	var arrDistribuidores = new Array();
	gArrDistribuidoresItem.splice(0)
	gbPanelItem = true
	var centroItem
	var hayPanelDistribuidores = false;
	if ($('[id$=lblCentro]').is(':visible'))
		centroItem = $('[id$=lblCentro]').text()
	else
		centroItem = ''

	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		//buscamos en el objeto la empresa a la que pertenece el proveedor que se va a guardar
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == gIdEmp) {
			indEmp = z
			break
		}
	}

	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == gCodProve) {
			indProv = i
			for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos.length - 1; z++) {
				if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Codigo == gCodGrupo) {
					indGrupo = z
					for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items.length - 1; x++) {
						if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x].Id == parseInt(gIdItem)) {
							oItem = gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x]
							oItem.Visto = 1
							indItem = x
							gIndEmp = indEmp
							gIndProve = indProv
							gIndGrupo = indGrupo
							gIndItem = indItem
							var Centro = ""
							if ($('[id$=cmbCentros]').children("OPTION").length > 0)
								Centro = $('[id$=cmbCentros]').children("OPTION:selected").text()
							else
								Centro = $('[id$=lblCentro]').text()
							if (Centro == "" && gCentro != undefined && gCentro != "") Centro = gCentro;
							var CodErpSelec = $('[id$=cmbCodigosERPItem]').children("OPTION:selected").val()
							var DenErpSelec = $('[id$=cmbCodigosERPItem]').children("OPTION:selected").text()
							editRow.get_cellByColumnKey("Key_CENTRO").set_value(Centro)

							gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x].CodERP = CodErpSelec
							gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x].DenERP = DenErpSelec
							gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x].Centro = Centro

							//Guardamos los DISTRIBUIDORES a nivel de item
							//Una vez encontrado el item borramos los distribuidores y los volvemos a grabar
							gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x].Distribuidores.splice(0, gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x].Distribuidores.length);

							var tblDistribuidoresItem = $("[id$=tblDistribuidoresPanelItem]")
							var ind = 0;
							var Proveedor;
							var bodyDistrib = tblDistribuidoresItem.children()
							bodyDistrib.children().each(function (index) {
								var Cod, Den, CodERP, DenCodERP, CantDistr;

								//recorremos la tabla de distribuidores del proveedor
								$(this).children().each(function (index2) {
									if (ind > 0) {
										switch (index2) {
											case 0: //Codigo del proveedor
												Cod = ($(this).text()).trim();
												break
											case 1: //Denominacion del proveedor
												Den = $(this).text();
												break
											case 2: //Codigo ERP
												if ($(this).children().find('input.fsComboValueDisplay').length > 0) { //Se mira si esta en modo edicion o en solo lectura
													DenCodERP = $(this).children().find('input.fsComboValueDisplay').val()
													CodERP = $(this).children().find('li.fsComboItemSelected').attr('itemValue')
												} else {
													var ix = $(this).find('SPAN').text().indexOf(' - ')
													if (ix != -1) {
														CodERP = $(this).find('SPAN').text().substring(0, ix - 1)
														DenCodERP = $(this).find('SPAN').text().substring(ix + 1, 100)
													} else {
														CodERP = ''
														DenCodERP = ''
													}
												}
												break
											case 3: //Cantidad a distribuir

												if ($(this).find('INPUT').length > 0) { //Se mira si esta en modo edicion o en solo lectura
													CantDistr = $(this).find('INPUT').val()
												} else {
													CantDistr = $(this).find('SPAN').text()
												}
												CantDistr = strToNum(CantDistr)
												hayPanelDistribuidores = true;
												try {
													CantDistrTotal += parseFloat(CantDistr)
												}
												catch (err) {
													CantDistrTotal += 0
												}
												break
										}
									}
								})
								if (ind > 0) { //Para evitar la cabecera de la tabla
									if (ind == 1) { //Si es el distribuidor principal se pondra el codigo ERP elegido en la grid en el campo de codigo erp
										oItem.CodERP = CodERP
										oItem.DenERP = DenCodERP
										editRow.get_cellByColumnKey("Key_PROVE_ERP").set_value(CodERP, DenCodERP)
									}
									CodsERP = new Array();

									Distribuidor = { Id: 0, Codigo: Cod, Denominacion: Den, Cantidad_Adj: CantDistr, DenErpSeleccionado: DenCodERP, CodErpSeleccionado: CodERP, CodigosERP: CodsERP }
									arrDistribuidores.push(Distribuidor)
								}
								ind += 1
							})

							//Guardamos los ATRIBUTOS a nivel de item                               
							gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x].Atributos.splice(0, gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x].Atributos.length);
							var tblAtributosProv = $("[id$=tblAtributosItem]")
							var u = 0;
							var Proveedor;
							var body = tblAtributosProv.children()
							body.children().each(function (index) {
								var Id, Cod, Den, Tipo, Intro, Oblig, Valor, Valor_Text, Valid_Erp, Orden;
								//recorremos la tabla de atributos del proveedor
								$(this).children().each(function (index2) {
									if (u > 0) {
										switch (index2) {
											case 0: //Id del atributo
												Id = $(this).text();
												break
											case 1: //Codigo del atributo
												Cod = $.trim($(this).text());
												Cod = Cod.replace('(*) ', '')
												break
											case 2: //Denominacion del atributo
												Den = $(this).text();
												break
											case 3: //Tipo de dato
												Tipo = $(this).text();
												break
											case 4: //Si es lista o no
												Intro = $(this).text();
												break
											case 5:
												Oblig = $(this).text();
												break
											case 6:
												Valid_Erp = $(this).text();
												break
											case 7: //Valor
												Orden = -1
												switch (Intro) {
													case '1':
														Valor = $(this).find('.fsCombo').fsCombo('getSelectedText');
														Valor_Text = Valor
														Orden = $(this).find('.fsCombo').fsCombo('getSelectedValue');
														if (Orden === '') {
															Orden = -1 //Si no se selecciona nada, se le asigna -1 por ser un campo numerico
															if ((Tipo == '3') && (Valor == '')) {
																Valor = null;
															}
														}
														break
													default:
														switch (Tipo) {
															case '1': //Texto
																Valor = $(this).find('textarea').val()
																Valor_Text = Valor
																break
															case '2': //Numerico
																Valor = $(this).find('INPUT').val()
																Valor_Text = Valor
																Valor = strToNum(Valor)
																break
															case '3': //Fecha
																Valor = $('#inputFechaAtrib_' + Id).datepicker('getDate');
																if (Valor != null) {
																	//Valor = Valor.toString()
																	Valor_Text = $('#inputFechaAtrib_' + Id).val()
																}
																break
															case '4': //Boolean   
																Valor = $(this).find('.fsCombo').fsCombo('getSelectedValue');
																if (Valor == undefined)
																	//Si no hay nada seleccionado en la lista
																	Valor = ''
																if (Valor == '1')
																	Valor_Text = TextosBoolean[1] //Si
																else
																	if (Valor == '0')
																		Valor_Text = TextosBoolean[0] //No
																	else
																		Valor_Text = ''
																break
														}
														break
												}
										}
									}
								})
								if (u > 0) { //Para evitar la cabecera de la tabla
									if (Oblig == 'true') {
										Atributo = { Id: Id, Tipo: Tipo, Codigo: Cod, Denominacion: Den, Valor: Valor, Obligatorio: true, Orden: Orden, Intro: Intro }
										if (editRow.get_cellByColumnKey("Key_ATRIB_" + Tipo + '_1_' + Intro + '_' + Cod))
											editRow.get_cellByColumnKey("Key_ATRIB_" + Tipo + '_1_' + Intro + '_' + Cod).set_value(Valor_Text)
									} else {
										Atributo = { Id: Id, Tipo: Tipo, Codigo: Cod, Denominacion: Den, Valor: Valor, Obligatorio: false, Orden: Orden, Intro: Intro }
										if (editRow.get_cellByColumnKey("Key_ATRIB_" + Tipo + '_0_' + Intro + '_' + Cod))
											editRow.get_cellByColumnKey("Key_ATRIB_" + Tipo + '_0_' + Intro + '_' + Cod).set_value(Valor_Text)
									}
									gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x].Atributos.push(Atributo)
								}
								u += 1
							})

							break
						}
					}
					break
				}
			}
			break
		}
	}

	var TieneEscalados = gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProv].Grupos[indGrupo].TieneEscalados
	hPostBack = true;
	if ((hayPanelDistribuidores) && (TieneEscalados == 0)) { //Solo si no tiene escalados se mostraran los mensajes de cantidades
		if (CantDistrTotal > oItem.CantAdj) {
			$('[id$=lblMensaje]').text(TextosJScript[1] + formatNumber(oItem.CantAdj).toString() + ' ' + oItem.Unidad)
			$('[id$=imgCerrarPanelMensaje]').attr('src', ruta + '/images/Bt_Cerrar.png')
			$('[id$=imgCerrarPanelMensaje]').css('display', 'none')
			$('[id$=imgPanelMensaje]').attr('src', ruta + '/App_Themes/<%=Me.Page.Theme%>/images/Icono_Error_Amarillo_40x40.gif')
			gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProv].Grupos[indGrupo].Items[indItem].Distribuidores = arrDistribuidores
			hPostBack = false;
			$find('<%=mpePanelMensaje.ClientID%>').show()
		} else {
			if (oItem.CantAdj > CantDistrTotal) {
				$('[id$=lblMensaje]').text(TextosJScript[2].toString().replace("XXXXX", formatNumber(oItem.CantAdj).toString() + ' ' + oItem.Unidad))
				$('[id$=imgCerrarPanelMensaje]').attr('src', ruta + '/images/Bt_Cerrar.png')
				$('[id$=imgCerrarPanelMensaje]').css('display', '')
				$('[id$=imgPanelMensaje]').attr('src', ruta + '/App_Themes/<%=Me.Page.Theme%>/images/Icono_Error_Amarillo_40x40.gif')
				gArrDistribuidoresItem = arrDistribuidores
				hPostBack = false;
				$find('<%=mpePanelMensaje.ClientID%>').show()
				$('[id$=btnCancelarPanelMensaje]').show();
			} else {
				gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProv].Grupos[indGrupo].Items[indItem].Distribuidores = arrDistribuidores
			}
		}
	}

	var resp = ComprobarDistribuidoresItem(gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProv].Distribuidores, arrDistribuidores, gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProv].Grupos[indGrupo].Items[indItem].CantAdj)
	switch (resp) {
		case 0:
			$('[id$=divDistrItem_' + gIdItem + '_' + centroItem + ']').empty();
			break
		case 1: //Tiene distribuidores distintos, se pone el check
			$('[id$=divDistrItem_' + gIdItem + '_' + centroItem + ']').empty();
			$('[id$=divDistrItem_' + gIdItem + '_' + centroItem + ']').append("<img id='imgDistrItem_'" + gIdItem + "' style='vertical-align :middle' src='" + ruta + "/App_Themes/<%=Me.Page.Theme%>/images/aprobado_16.gif'/>")
			break
		case 2: //Tiene porcentajes distintos, se pone la exclamacion
			if (TieneEscalados == 0) {
				$('[id$=divDistrItem_' + gIdItem + '_' + centroItem + ']').empty();
				$('[id$=divDistrItem_' + gIdItem + '_' + centroItem + ']').append("<img id='imgDistrItem_'" + gIdItem + "' style='vertical-align :middle' src='" + ruta + "/App_Themes/<%=Me.Page.Theme%>/images/Icono_Error_Amarillo.gif'/>")
			}
			break
	}
	$find('<%=mpePanelItem.ClientID%>').hide()
	if (hPostBack) {
		postBackCambioItems(gIdEmp, gCodProve, gCodGrupo, gIdGrupo, gIdItem);
	} else {
		inthPostBack = setInterval(function () {
			if (hPostBack) {
				postBackCambioItems(gIdEmp, gCodProve, gCodGrupo, gIdGrupo, gIdItem);
				clearInterval(inthPostBack);
			}
		}, 500);
	}
};
function postBackCambioItems(idEmp, codProve, codGrupo, idGrupo, idItem) {
	GuardarObjetoAdjudicacion();
	var params = { idEmp: idEmp, codProve: codProve, codGrupo: codGrupo, idGrupo: idGrupo, idItem: idItem };
	__doPostBack('whdgItemsCambio', JSON.stringify(params));
};
function btnAceptarDistrItem_Click() {
	//Boton de aceptar del panel de distribuidores del item
	var indEmp, indProv, indGrupo, indItem, oItem, CantDistrTotal = 0
	var arrDistribuidores = new Array();
	gArrDistribuidoresItem.splice(0)
	gbPanelItem = false

	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		//buscamos en el objeto la empresa a la que pertenece el proveedor que se va a guardar
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == gIdEmp) {
			indEmp = z
			break
		}
	}

	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == gCodProve) {
			indProv = i
			for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos.length - 1; z++) {
				if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Codigo == gCodGrupo) {
					indGrupo = z
					for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items.length - 1; x++) {
						if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x].Id == parseInt(gIdItem)) {
							indItem = x
							gIndEmp = indEmp
							gIndProve = indProv
							gIndGrupo = indGrupo
							gIndItem = indItem
							oItem = gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x]
							//Una vez encontrado el item borramos los distribuidores y los volvemos a grabar
							gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x].Distribuidores.splice(0, gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x].Distribuidores.length);

							var tblDistribuidoresItem = $("[id$=tblDistribuidoresItem]")
							var ind = 0;
							var Proveedor;
							var bodyDistrib = tblDistribuidoresItem.children()
							bodyDistrib.children().each(function (index) {
								var Cod, Den, CodERP, DenCodERP, CantDistr;
								//recorremos la tabla de distribuidores del proveedor
								$(this).children().each(function (index2) {
									if (ind > 0) {
										switch (index2) {
											case 0: //Codigo del proveedor
												Cod = $(this).text();
												break
											case 1: //Denominacion del proveedor
												Den = $(this).text();
												break
											case 2: //Codigo ERP
												if ($(this).children().find('input.fsComboValueDisplay').length > 0) { //Se mira si esta en modo edicion o en solo lectura
													DenCodERP = $(this).children().find('input.fsComboValueDisplay').val()
													CodERP = $(this).children().find('li.fsComboItemSelected').attr('itemValue')
												} else {
													var ix = $(this).find('SPAN').text().indexOf(' - ')
													if (ix != -1) {
														CodERP = $(this).find('SPAN').text().substring(0, ix - 1)
														DenCodERP = $(this).find('SPAN').text().substring(ix + 1, 100)
													} else {
														CodERP = ''
														DenCodERP = ''
													}
												}
												break
											case 3: //Cantidad a distribuir
												if ($(this).find('INPUT').length > 0) { //Se mira si esta en modo edicion o en solo lectura
													CantDistr = $(this).find('INPUT').val()
												} else {
													CantDistr = $(this).find('SPAN').text()
												}
												CantDistr = strToNum(CantDistr)
												try {
													CantDistrTotal += parseFloat(CantDistr)
												}
												catch (err) {
													CantDistrTotal += 0
												}
												break
										}
									}
								})
								if (ind > 0) { //Para evitar la cabecera de la tabla
									CodsERP = new Array();

									Distribuidor = { Id: 0, Codigo: Cod, Denominacion: Den, Cantidad_Adj: CantDistr, DenErpSeleccionado: DenCodERP, CodErpSeleccionado: CodERP, CodigosERP: CodsERP }
									arrDistribuidores.push(Distribuidor)//gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x].Distribuidores.push(Distribuidor)
								}
								ind += 1
							})
							break
						}
					}
					break
				}
			}
		}
	}

	hPostBack = true;
	if (CantDistrTotal > oItem.CantAdj) {
		$('[id$=lblMensaje]').text(TextosJScript[1] + formatNumber(oItem.CantAdj).toString() + ' ' + oItem.Unidad)
		$('[id$=imgCerrarPanelMensaje]').attr('src', ruta + '/Bt_Cerrar.png')
		$('[id$=imgCerrarPanelMensaje]').css('display', 'none')
		$('[id$=imgPanelMensaje]').attr('src', ruta + '/App_Themes/<%=Me.Page.Theme%>/images/Icono_Error_Amarillo_40x40.gif')
		gArrDistribuidoresItem = arrDistribuidores
		hPostBack = false;
		$find('<%=mpePanelMensaje.ClientID%>').show()
	} else {
		if (oItem.CantAdj > CantDistrTotal) {
			$('[id$=lblMensaje]').text(TextosJScript[2].toString().replace("XXXXX", formatNumber(oItem.CantAdj).toString() + ' ' + oItem.Unidad))
			$('[id$=imgCerrarPanelMensaje]').attr('src', ruta + '/images/Bt_Cerrar.png')
			$('[id$=imgCerrarPanelMensaje]').css('display', '')
			$('[id$=imgPanelMensaje]').attr('src', ruta + '/images/Icono_Error_Amarillo_40x40.gif')
			gArrDistribuidoresItem = arrDistribuidores
			hPostBack = false;
			$find('<%=mpePanelMensaje.ClientID%>').show()
			$('[id$=btnCancelarPanelMensaje]').show();
		} else {
			gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProv].Grupos[indGrupo].Items[indItem].Distribuidores = arrDistribuidores
		}
	}

	var centroItem
	centroItem = gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProv].Grupos[indGrupo].Items[indItem].Centro
	var resp = ComprobarDistribuidoresItem(gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProv].Distribuidores, arrDistribuidores, gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProv].Grupos[indGrupo].Items[indItem].CantAdj)
	switch (resp) {
		case 0:
			$('[id$=divDistrItem_' + gIdItem + '_' + centroItem + ']').empty();
			break
		case 1: //Tiene distribuidores distintos, se pone el check
			$('[id$=divDistrItem_' + gIdItem + '_' + centroItem + ']').empty();
			$('[id$=divDistrItem_' + gIdItem + '_' + centroItem + ']').append("<img id='imgDistrItem_'" + gIdItem + "' style='vertical-align :middle' src='" + ruta + "/App_Themes/<%=Me.Page.Theme%>/images/aprobado_16.gif'/>")
			break
		case 2: //Tiene porcentajes distintos, se pone la exclamacion
			$('[id$=divDistrItem_' + gIdItem + '_' + centroItem + ']').empty();
			$('[id$=divDistrItem_' + gIdItem + '_' + centroItem + ']').append("<img id='imgDistrItem_'" + gIdItem + "' style='vertical-align :middle' src='" + ruta + "/App_Themes/<%=Me.Page.Theme%>/images/Icono_Error_Amarillo.gif'/>")
			break
	}

	$find('<%=mpePanelDistribuidores.ClientID%>').hide()

	if (hPostBack) {
		GuardarObjetoAdjudicacion();
	}
};
function btnAceptarMensajeDistribuidores_Click() {
	//boton de aceptar en el panel de mensaje cuando se modifica la distribucion de los distribuidores en el panel de proveedores
	//y algun item se le ha modificado la distribucion previamente
	var indEmp

	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == gIdEmp) {
			indEmp = z
			break
		}
	}
	var indProve
	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == gCodProve) {
			indProve = i
			break
		}
	}

	for (var v = 0; v <= gArrItemsConDistintaDistribucion.length - 1; v++) {
		for (var y = 0; y <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos.length - 1; y++) {
			for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[y].Items.length - 1; x++) {
				if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[y].Items[x].Id == gArrItemsConDistintaDistribucion[v].Id) {
					for (var t = 0; t <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[y].Items[x].Distribuidores.length - 1; t++) {
						if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[y].Items[x].Distribuidores[t].Codigo == gCodDistribuidorModifDistribucionItems) {
							gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[y].Items[x].Distribuidores[t].Cantidad_Adj = (parseFloat(gPorcentajeDistribuidorModifDistribucionItems) / 100) * gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[y].Items[x].CantAdj
						}
					}
				}

			}
		}
	}

	$find('<%=mpePanelMensajeDistribuidores.ClientID%>').hide()
};
function btnAceptarMensaje_Click() {
	//boton de aceptar en el panel de mensaje que indica alguna anomalia en la distribucion a los distribuidores
	if (gArrDistribuidoresItem.length > 0) {
		gAdjudicacion.EmpresasAdjudicadas[gIndEmp].ProveedoresAdjudicados[gIndProve].Grupos[gIndGrupo].Items[gIndItem].Distribuidores = gArrDistribuidoresItem
		GuardarObjetoAdjudicacion();
	}
	if (gEnvioPdte == true) { //Si hemos pulsado en envio y ha salido el mensaje, en caso de aceptar el mensaje hay que hacer el envio
		var idItem = editRow.get_cellByColumnKey("Key_ID_ITEM").get_value()
		var centro = editRow.get_cellByColumnKey("Key_CENTRO").get_value()
		var codArt = editRow.get_cellByColumnKey("Key_CODART").get_value()
		var oItem = getItem(idItem, centro, codArt)

		if (oItem.Estado == -1 || oItem.Estado == 3) {
			TraspasoERPItem(idItem, false, centro, codArt)
		} else {
			TraspasoERPItem(idItem, true, centro, codArt)
		}
		gEnvioPdte = false
	}
	hPostBack = true;
	$find('<%=mpePanelMensaje.ClientID%>').hide()
};
function imgCerrarPanelMensaje_Click() {
	if (gArrDistribuidoresItem.length > 0) {
		if (gbPanelItem == true) {
			$find('<%=mpePanelItem.ClientID%>').show();
			$("[id$=pnlMensaje]").hide();
			$("[id$=mpePanelMensaje_backgroundElement]").hide();
		}
		else {
			$find('<%=mpePanelDistribuidores.ClientID%>').show();
			$("[id$=pnlMensaje]").hide();
			$("[id$=mpePanelMensaje_backgroundElement]").hide();
		}
	}
};
function ComprobarDistribuidoresItem(DistribuidoresProv, DistribuidoresItem, Cantidad_Adj) {
	//Funcion que al cerrar el panel de distribuidores del item comprobara si hay diferencias respecto a las distribuciones por proveedor
	var PorcentajesDistintos, DistribuidoresDistintos, DistrEncontrado
	DistrEncontrado = false
	if (DistribuidoresProv.length == 0) {
		//Si no hay distribuidores de proveedor es que no se ha pasado por la seccion proveedor y no se han cargado aun en el objeto
		for (var y = 1; y <= DistribuidoresItem.length - 1; y++) {
			//Miraremos los distribuidores(omitiremos el primer distribuidor que es el proveedor principal), para ver si tienen cantidad adjudicada
			if (DistribuidoresItem[y].Cantidad_Adj > 0) {
				return 1 //Si alguno tiene cantidad mostraremos el check ya que ha elegido distribuidores distintos
				break
			}
		}
		return 0 //Si ninguno tiene cantidad, entonces la distribucion es al proveedor principal y no pondremos nada
	} else {
		ArrDistribuidoresProveedor = new Array();
		ArrDistribuidoresItem = new Array();
		for (var y = 0; y <= DistribuidoresProv.length - 1; y++) {
			if (parseFloat(DistribuidoresProv[y].PorcentajeDistribuido) > 0)
				ArrDistribuidoresProveedor[y] = DistribuidoresProv[y].Codigo
			else
				ArrDistribuidoresProveedor[y] = ''

		}

		for (var y = 0; y <= DistribuidoresItem.length - 1; y++) {
			if (parseFloat(DistribuidoresItem[y].Cantidad_Adj) > 0)
				ArrDistribuidoresItem[y] = DistribuidoresItem[y].Codigo
			else
				ArrDistribuidoresItem[y] = ''
		}


		for (var y = 0; y <= DistribuidoresItem.length - 1; y++) {
			if (ArrDistribuidoresItem[y] != ArrDistribuidoresProveedor[y]) {
				return 1 //Se marca el check si hay distribuidores distintos
				break
			}
		}

		//en caso de que estarian los mismos distribuidores mirariamos si las cantidades distribuidas son las mismas
		for (var x = 0; x <= DistribuidoresProv.length - 1; x++) {
			for (var y = 0; y <= DistribuidoresItem.length - 1; y++) {
				if (DistribuidoresProv[x].Codigo == DistribuidoresItem[y].Codigo) {
					if ((parseFloat(DistribuidoresProv[x].PorcentajeDistribuido) / 100) * Cantidad_Adj != parseFloat(DistribuidoresItem[y].Cantidad_Adj)) {
						PorcentajesDistintos = true
					}
					DistrEncontrado = true
					break
				}
			}
		}
	}

	if (PorcentajesDistintos == true)
		return 2
	else
		return 0
};
function Ejecutar_CambioCodigoERPProvAdjudicado(id, NumCamposPersonalizados, Fila) {
	//Ejecuta la funcion CambioCodigoERPProvAdjudicado para que en una carga inicial se carguen los valores
	CambioCodigoERPProvAdjudicado($('[id$=' + id + ']')[0], NumCamposPersonalizados, Fila);
};
function CambioCodigoERPProvAdjudicado(sender, NumCamposPersonalizados, Fila) {
	//funcion que carga los campos personalizados que corresponden al Codigo Erp seleccionado
	//sender: combo que ha sido seleccionado
	//NumCamposPersonalizados: numero de campos personalizados que estan activados
	//Fila: fila de la tabla en la que se ha seleccionado el combo
	var idCombo = sender.id
	for (i = 3; i <= (3 + NumCamposPersonalizados - 1) ; i++) {
		var valor = $("#" + idCombo + " option:selected").attr("CAMPO" + (i - 2))
		if (valor == undefined)
			var valor = ''
		$($('[id$=tblProveedoresAdjudicados]').find('tbody > tr')[Fila]).children('td')[i].innerHTML = valor
	}

	if ($(sender).data('oldValue') != undefined && $(sender).data('oldValue') != $(sender).val()) {
		var sData = TextosJScript[34] + ': ' + gAnyo + '/' + gGmn1 + '/' + gCodProce + ', ' + TextosJScript[35] + ': ' + $(sender).data('oldValue') + ', ' + TextosJScript[36] + ': ' + $(sender).val();
		RegistrarAccion(ACCTraspAdjModifProveERPEmp, sData);
		$(sender).data('oldValue', $(sender).val());
	}
};
function GuardarValorAntiguo(sender) {
	$(sender).data('oldValue', $(sender).val());
};
function CambioCodigoERPProvAdjudicadoProveedor(sender, NumCamposPersonalizados) {
	//funcion que carga en la seccion proveedor los campos personalizados que corresponden al Codigo Erp seleccionado
	//sender: combo del codigo ERP del Proveedor
	//NumCamposPersonalizados: numero de campos personalizados que estan activados
	var idCombo = sender.id
	gModificadaDistribucionPanelProv = true
	var cboDistrPrincipal = $('[id^=cboDistribCodERP_]')[0]
	if (cboDistrPrincipal != undefined) {
		var IdcboDistrPrincipal = cboDistrPrincipal.id
		//Recogemos el combo de codigos Erp del proveedor principal que seria el 1º
		cboDistrPrincipal = $('[id^=' + IdcboDistrPrincipal + ']')
		var valueCodERP = $('#' + idCombo + " option:selected").val()
		//Al cambiar la seleccion del codigo Erp en el combo del proveedor principal se cambiara el combo de la tabla de distribuidores del proveedor principal            
		if (valueCodERP == '')
			cboDistrPrincipal.fsCombo('selectItem', 1); //Se selecciona la opcion en blanco
		else
			cboDistrPrincipal.fsCombo('selectValue', valueCodERP); //Se selecciona la opcion en blanco
	}

	switch (NumCamposPersonalizados) {
		case 1:
			var valor = $('#' + idCombo + " option:selected").attr("CAMPO1")
			if (valor == undefined) {
				//Si se selecciona la opcion vacia del combo se limpian todos los campos personalizables
				valor = ''
				$('[id$=divCampoPer1]').hide()
			} else {
				$('[id$=divCampoPer1]').show()
				$('[id$=lblCampo1Dato]').text(valor)
			}
			break;
		case 2:
			var valor = $('#' + idCombo + " option:selected").attr("CAMPO1")
			if (valor == undefined) {
				//Si se selecciona la opcion vacia del combo se limpian todos los campos personalizables
				valor = ''
				$('[id$=divCampoPer1]').hide()
				$('[id$=divCampoPer2]').hide()
			} else {
				$('[id$=divCampoPer1]').show()
				$('[id$=divCampoPer2]').show()
				$('[id$=lblCampo1Dato]').text(valor)
				valor = $('#' + idCombo + " option:selected").attr("CAMPO2")
				$('[id$=lblCampo2Dato]').text(valor)
			}

			break;
		case 3:
			var valor = $('#' + idCombo + " option:selected").attr("CAMPO1")
			if (valor == undefined) {
				//Si se selecciona la opcion vacia del combo se limpian todos los campos personalizables
				valor = ''
				$('[id$=divCampoPer1]').hide()
				$('[id$=divCampoPer2]').hide()
				$('[id$=divCampoPer3]').hide()
			} else {
				$('[id$=divCampoPer1]').show()
				$('[id$=divCampoPer2]').show()
				$('[id$=divCampoPer3]').show()
				$('[id$=lblCampo1Dato]').text(valor)
				valor = $('#' + idCombo + " option:selected").attr("CAMPO2")
				$('[id$=lblCampo2Dato]').text(valor)
				valor = $('#' + idCombo + " option:selected").attr("CAMPO3")
				$('[id$=lblCampo3Dato]').text(valor)
			}

			break;
		case 4:
			var valor = $('#' + idCombo + " option:selected").attr("CAMPO1")
			if (valor == undefined) {
				//Si se selecciona la opcion vacia del combo se limpian todos los campos personalizables
				valor = ''
				$('[id$=divCampoPer1]').hide()
				$('[id$=divCampoPer2]').hide()
				$('[id$=divCampoPer3]').hide()
				$('[id$=divCampoPer4]').hide()
			} else {
				$('[id$=divCampoPer1]').show()
				$('[id$=divCampoPer2]').show()
				$('[id$=divCampoPer3]').show()
				$('[id$=divCampoPer4]').show()
				$('[id$=lblCampo1Dato]').text(valor)
				valor = $("#" + idCombo + " option:selected").attr("CAMPO2")
				$('[id$=lblCampo2Dato]').text(valor)
				valor = $("#" + idCombo + " option:selected").attr("CAMPO3")
				$('[id$=lblCampo3Dato]').text(valor)
				valor = $("#" + idCombo + " option:selected").attr("CAMPO4")
				$('[id$=lblCampo4Dato]').text(valor)
			}
			break;
	}

	if ($(sender).data('oldValue') != undefined && $(sender).data('oldValue') != $(sender).val()) {
		var sData = TextosJScript[34] + ': ' + gAnyo + '/' + gGmn1 + '/' + gCodProce + ', ' + TextosJScript[35] + ': ' + $(sender).data('oldValue') + ', ' + TextosJScript[36] + ': ' + $(sender).val();
		RegistrarAccion(ACCTraspAdjModifProveERProve, sData);
		$(sender).data('oldValue', $(sender).val());
	}
};
function CambioCodigoERPItem(sender, NumCamposPersonalizados) {
	//funcion que carga en el panel de edicion del Item los campos personalizados que corresponden al Codigo Erp seleccionado
	//sender: combo del codigo ERP del Proveedor
	//NumCamposPersonalizados: numero de campos personalizados que estan activados
	var idCombo = sender.id

	var cboDistrPrincipal = $('[id^=cboDistribItemPanelCodERP_]')[0] //Recogemos el combo de codigos Erp del proveedor principal que seria el 1º

	if (cboDistrPrincipal != undefined) {
		var IdcboDistrPrincipal = cboDistrPrincipal.id
		var cboDistrPrincipal = $('[id^=' + IdcboDistrPrincipal + ']')

		var valueCodERP = $('#' + idCombo + " option:selected").val()
		//Al cambiar la seleccion del codigo Erp en el combo del proveedor principal se cambiara el combo de la tabla de distribuidores del proveedor principal
		if (valueCodERP == '')
			cboDistrPrincipal.fsCombo('selectItem', 1); //Se selecciona la opcion en blanco
		else
			cboDistrPrincipal.fsCombo('selectValue', valueCodERP); //Se selecciona la opcion en blanco
	}
	if ($('#' + idCombo + " option:selected").val() == '') $('#imgInfoERP').hide();
	else $('#imgInfoERP').show();
	switch (NumCamposPersonalizados) {
		case 1:
			var valor = $('#' + idCombo + " option:selected").attr("CAMPO1")
			if (valor == undefined) {
				//Si se selecciona la opcion vacia del combo se limpian todos los campos personalizables
				valor = ''
				$('[id$=divCampoPer1Item]').hide()
			} else {
				$('[id$=divCampoPer1Item]').show()
				$('[id$=lblCampo1DatoItem]').text(valor)
			}
			break;
		case 2:
			var valor = $('#' + idCombo + " option:selected").attr("CAMPO1")
			if (valor == undefined) {
				//Si se selecciona la opcion vacia del combo se limpian todos los campos personalizables
				valor = ''
				$('[id$=divCampoPer1Item]').hide()
				$('[id$=divCampoPer2Item]').hide()
			} else {
				$('[id$=divCampoPer1Item]').show()
				$('[id$=divCampoPer2Item]').show()
				$('[id$=lblCampo1DatoItem]').text(valor)
				valor = $('#' + idCombo + " option:selected").attr("CAMPO2")
				$('[id$=lblCampo2DatoItem]').text(valor)
			}

			break;
		case 3:
			var valor = $('#' + idCombo + " option:selected").attr("CAMPO1")
			if (valor == undefined) {
				//Si se selecciona la opcion vacia del combo se limpian todos los campos personalizables
				valor = ''
				$('[id$=divCampoPer1Item]').hide()
				$('[id$=divCampoPer2Item]').hide()
				$('[id$=divCampoPer3Item]').hide()
			} else {
				$('[id$=divCampoPer1Item]').show()
				$('[id$=divCampoPer2Item]').show()
				$('[id$=divCampoPer3Item]').show()
				$('[id$=lblCampo1DatoItem]').text(valor)
				valor = $('#' + idCombo + " option:selected").attr("CAMPO2")
				$('[id$=lblCampo2DatoItem]').text(valor)
				valor = $('#' + idCombo + " option:selected").attr("CAMPO3")
				$('[id$=lblCampo3DatoItem]').text(valor)
			}

			break;
		case 4:
			var valor = $('#' + idCombo + " option:selected").attr("CAMPO1")
			if (valor == undefined) {
				//Si se selecciona la opcion vacia del combo se limpian todos los campos personalizables
				valor = ''
				$('[id$=divCampoPer1Item]').hide()
				$('[id$=divCampoPer2Item]').hide()
				$('[id$=divCampoPer3Item]').hide()
				$('[id$=divCampoPer4Item]').hide()
			} else {
				$('[id$=divCampoPer1Item]').show()
				$('[id$=divCampoPer2Item]').show()
				$('[id$=divCampoPer3Item]').show()
				$('[id$=divCampoPer4Item]').show()
				$('[id$=lblCampo1DatoItem]').text(valor)
				valor = $("#" + idCombo + " option:selected").attr("CAMPO2")
				$('[id$=lblCampo2DatoItem]').text(valor)
				valor = $("#" + idCombo + " option:selected").attr("CAMPO3")
				$('[id$=lblCampo3DatoItem]').text(valor)
				valor = $("#" + idCombo + " option:selected").attr("CAMPO4")
				$('[id$=lblCampo4DatoItem]').text(valor)
			}
			break;
	}

	if ($(sender).data('oldValue') != undefined && $(sender).data('oldValue') != $(sender).val()) {
		var sData = TextosJScript[34] + ': ' + gAnyo + '/' + gGmn1 + '/' + gCodProce + ', ' + TextosJScript[35] + ': ' + $(sender).data('oldValue') + ', ' + TextosJScript[36] + ': ' + $(sender).val();
		RegistrarAccion(ACCTraspAdjModifProveERPItem, sData);
		$(sender).data('oldValue', $(sender).val());
	}
};
function CambioCodigoERPProvAdjudicadoGrupo(sender, NumCamposPersonalizados, senderId) {
	//funcion que carga en la seccion Grupo los campos personalizados que corresponden al Codigo Erp seleccionado
	//sender: combo del codigo ERP del Proveedor
	//NumCamposPersonalizados: numero de campos personalizados que estan activados
	var idCombo
	if (sender != null)
		idCombo = sender.id
	else
		idCombo = senderId

	var grid = $find("<%= whdgItems.ClientID %>");
	var valueCodERP = $('#' + idCombo + " option:selected").val()
	var textCodERP = $('#' + idCombo + " option:selected").text()
	var oItem
	//Actualizamos el distribuidor principal de los items del objeto adjudicacion si es que los tuviera
	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == gIdEmp) {
			indEmp = z
			break
		}
	}
	var indProve
	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == gCodProve) {
			indProve = i
			break
		}
	}
	var indItem, indGrupo
	if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos != undefined) {
		//guardamos la seleccion del codigo Erp para el proveedor principal para este grupo
		for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos.length - 1; z++) {
			if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Codigo == gCodGrupo) {
				indGrupo = z
				gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].CodErpSeleccionado = valueCodERP
				gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].DenErpSeleccionado = textCodERP
			}
		}
	}

	//Cambio el codigo ERP del distribuidor principal a todos los items del grid
	var rows = grid.get_gridView().get_rows()
	for (i = 0; i <= rows.get_length() - 1; i++) {
		oItem = getItem(rows.get_row(i).get_cellByColumnKey("Key_ID_ITEM").get_value(), rows.get_row(i).get_cellByColumnKey("Key_CENTRO").get_value(), rows.get_row(i).get_cellByColumnKey("Key_CODART").get_value())
		if (oItem.Estado == -1 || oItem.Estado == 3) {
			rows.get_row(i).get_cellByColumnKey("Key_PROVE_ERP").set_value(valueCodERP, textCodERP)
			oItem.CodERP = valueCodERP
			oItem.DenERP = textCodERP
			if (oItem.Distribuidores.length > 0) {
				oItem.Distribuidores[0].CodErpSeleccionado = valueCodERP
				oItem.Distribuidores[0].DenErpSeleccionado = textCodERP
			}
		}

	}
	switch (NumCamposPersonalizados) {
		case 1:
			var valor = $('#' + idCombo + " option:selected").attr("CAMPO1")
			if (valor == undefined) {
				//Si se selecciona la opcion vacia del combo se limpian todos los campos personalizables
				valor = ''
				$('[id$=divCampoPer1Grupo]').hide()
			} else {
				$('[id$=divCampoPer1Grupo]').show()
				$('[id$=lblCampo1DatoGrupo]').text(valor)
			}
			break;
		case 2:
			var valor = $('#' + idCombo + " option:selected").attr("CAMPO1")
			if (valor == undefined) {
				//Si se selecciona la opcion vacia del combo se limpian todos los campos personalizables
				valor = ''
				$('[id$=divCampoPer1Grupo]').hide()
				$('[id$=divCampoPer2Grupo]').hide()
			} else {
				$('[id$=divCampoPer1Grupo]').show()
				$('[id$=divCampoPer2Grupo]').show()
				$('[id$=lblCampo1DatoGrupo]').text(valor)
				valor = $('#' + idCombo + " option:selected").attr("CAMPO2")
				$('[id$=lblCampo2DatoGrupo]').text(valor)
			}

			break;
		case 3:
			var valor = $('#' + idCombo + " option:selected").attr("CAMPO1")
			if (valor == undefined) {
				//Si se selecciona la opcion vacia del combo se limpian todos los campos personalizables
				valor = ''
				$('[id$=divCampoPer1Grupo]').hide()
				$('[id$=divCampoPer2Grupo]').hide()
				$('[id$=divCampoPer3Grupo]').hide()
			} else {
				$('[id$=divCampoPer1Grupo]').show()
				$('[id$=divCampoPer2Grupo]').show()
				$('[id$=divCampoPer3Grupo]').show()
				$('[id$=lblCampo1DatoGrupo]').text(valor)
				valor = $('#' + idCombo + " option:selected").attr("CAMPO2")
				$('[id$=lblCampo2DatoGrupo]').text(valor)
				valor = $('#' + idCombo + " option:selected").attr("CAMPO3")
				$('[id$=lblCampo3DatoGrupo]').text(valor)
			}

			break;
		case 4:
			var valor = $('#' + idCombo + " option:selected").attr("CAMPO1")
			if (valor == undefined) {
				//Si se selecciona la opcion vacia del combo se limpian todos los campos personalizables
				valor = ''
				$('[id$=divCampoPer1Grupo]').hide()
				$('[id$=divCampoPer2Grupo]').hide()
				$('[id$=divCampoPer3Grupo]').hide()
				$('[id$=divCampoPer4Grupo]').hide()
			} else {
				$('[id$=divCampoPer1Grupo]').show()
				$('[id$=divCampoPer2Grupo]').show()
				$('[id$=divCampoPer3Grupo]').show()
				$('[id$=divCampoPer4Grupo]').show()
				$('[id$=lblCampo1DatoGrupo]').text(valor)
				valor = $("#" + idCombo + " option:selected").attr("CAMPO2")
				$('[id$=lblCampo2DatoGrupo]').text(valor)
				valor = $("#" + idCombo + " option:selected").attr("CAMPO3")
				$('[id$=lblCampo3DatoGrupo]').text(valor)
				valor = $("#" + idCombo + " option:selected").attr("CAMPO4")
				$('[id$=lblCampo4DatoGrupo]').text(valor)
			}
			break;
	}

	if ($(sender).data('oldValue') != undefined && $(sender).data('oldValue') != $(sender).val()) {
		var sData = TextosJScript[34] + ': ' + gAnyo + '/' + gGmn1 + '/' + gCodProce + ', ' + TextosJScript[35] + ': ' + $(sender).data('oldValue') + ', ' + TextosJScript[36] + ': ' + $(sender).val();
		RegistrarAccion(ACCTraspAdjModifProveERPGrupo, sData);
		$(sender).data('oldValue', $(sender).val());
	}

	postBackCambioItems(gIdEmp, gCodProve, gCodGrupo, gIdGrupo, gIdItem);
};
function ConfiguracionCodERPGrupo() {
	//funcion que al cargar los items del un grupo habilita o deshabilita el combo de Codigo Erp dependiendo
	//de si hay items en estado de ser traspasados o no.
	var cmbCodigosERPProvAdjGrupo = $("[id$=cmbCodigosERPProvAdjGrupo]")
	var lblCodigoERPGrupo = $("[id$=lblCodigoERPGrupo]")
	if ($("[id$=btnTrasladarAdjERP]").css('display') == 'none') {
		cmbCodigosERPProvAdjGrupo.css('display', 'none')
		lblCodigoERPGrupo.css('display', '')
		lblCodigoERPGrupo.text(cmbCodigosERPProvAdjGrupo.children("OPTION:selected").text())
	} else {
		cmbCodigosERPProvAdjGrupo.css('display', '')
		lblCodigoERPGrupo.css('display', 'none')
	}

};
function AnchoColumnasGridItems() {
	//Esta funcion es debido a una incidencia en IE7, la primera fila del grid no se muestra bien cuando el texto
	//es mayor que el ancho de la columna y ese texto tiene espacios en blanco
	$($(".CssAncho")).each(function (index) { //recojo las filas del grid, que tienen esa clase CSS
		var item = this;
		if (index == 0) { //Solo se hara para la primera fila ya que es en esa fila donde el Grid le pone los pixeles a las columnas, que nosotros le quitaremos
			var trId = item.id
			$("[id^='" + trId + "']").children().each(function () {
				var itemTD = this;
				if (itemTD.style.width == '120px' || itemTD.style.width == '80px') //Solo para el campo denominacion y Cod Erp
					itemTD.style.width = ''
			})
			return
		}
	});
};
function CargarAtributosGrupo(idEmp, CodProve, idGrupo, codGrupo, DenEmpresa, DenProve) {
	//funcion que carga los atributos a nivel de grupo(Panel Grupo)
	gIdEmp = idEmp
	gCodProve = CodProve
	gCodGrupo = codGrupo
	gIdGrupo = idGrupo

	AnchoColumnasGridItems();

	var indEmp, GuardarObjeto, AtribYaGrabados
	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == idEmp) {
			indEmp = z
			break
		}
	}

	var indProve
	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == CodProve) {
			indProve = i
			break
		}
	}
	var indGrupo
	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Codigo == gCodGrupo) {
			indGrupo = z
			if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Atributos.length > 0)
				AtribYaGrabados = 1 //Los atributos del grupo ya ha sido grabados en el objeto adjudicacion   
		}
	}

	if (indEmp == undefined) {
		//Si se selecciona un grupo en el arbol sin haber seleccionado su empresa o proveedor previamente
		ProveedorAdjudicado = new Array();
		Empresa = { Id: idEmp, Denominacion: DenEmpresa, ProveedoresAdjudicados: ProveedorAdjudicado }
		indEmp = gAdjudicacion.EmpresasAdjudicadas.push(Empresa)

		Atributo = new Array();
		Distribuidor = new Array();
		Grupo = new Array();
		Proveedor = { Codigo: CodProve, Denominacion: DenProve, CodErpSeleccionado: null, Atributos: Atributo, Distribuidores: Distribuidor, Grupos: Grupo }
		gAdjudicacion.EmpresasAdjudicadas[indEmp - 1].ProveedoresAdjudicados.push(Proveedor)

		GuardarObjeto = true
	}

	var params = { iAnyo: gAnyo, sGmn1: gGmn1, iCodProce: gCodProce, idEmp: idEmp, CodProve: CodProve, idGrupo: idGrupo };
	$.when($.ajax({
		type: "POST",
		url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/CargarAtributosGrupo',
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(params),
		dataType: "json",
		async: false
	})).done(function (msg) {
		var oAtributos = msg.d;
		if (oAtributos.length == 0) //Si no hay atributos ocultamos el Div de Atributos    
			$('[id$=divAtributosGrupo]').hide()
		$(oAtributos).each(function () {
			var item = this;
			switch (this.Intro) {
				case 1: //Opcion Lista
					{
						$("#tmplCombo").tmpl(this).appendTo("#tblAtributosGrupo");

						var items = "{"
						//vamos componiendo con los valores de la combo el objeto Json
						for (i = 0; i <= item.Valores.length - 1; i++) {
							items += '"' + i + '": { "value":"' + item.Valores[i].Orden + '", "text": "' + item.Valores[i].Valor + '" }, '
							//Como no se guarda el orden de la opción elegida si no el valor directamente, saco asÃƒÂ­ el orden para poder dejar marcado el correcto.
							if (item.Valor != null && item.Valor != undefined) {
								if (item.Valor == item.Valores[i].Valor) item.Orden = item.Valores[i].Orden;
							}
						}
						items = items.substr(0, items.length - 2) //quitamos la ultima coma que sobra
						items += "}"

						var optionsPermiso = { valueField: "value", textField: "text", adjustWidthToSpace: true, dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: false, data: $.parseJSON(items) };
						$('#cboAtrib_' + item.Id).fsCombo(optionsPermiso);
						$('#cboAtrib_' + item.Id).fsCombo('onSelectedValueChange', function () { RegistrarCambioAtributo($('#cboAtrib_' + item.Id)[0]); });
						if (AtribYaGrabados == 1) {
							for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Atributos.length - 1; i++) {
								if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Atributos[i].Id == item.Id) {
									$('#cboAtrib_' + item.Id).fsCombo('selectValue', gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Atributos[i].Orden);
									break
								}
							}
						} else {
							$('#cboAtrib_' + item.Id).fsCombo('selectValue', item.Orden);
						}
						break;
					}
				default:
					{
						switch (this.Tipo) {
							case 1: //Texto
								$("#tmplCampoTexto").tmpl(this).appendTo("#tblAtributosGrupo");
								if (AtribYaGrabados == 1) {
									for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Atributos.length - 1; i++) {
										if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Atributos[i].Id == item.Id) {
											$('#inputTextAtrib_' + item.Id).val(gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Atributos[i].Valor)
											break
										}
									}
								}
								break
							case 2: //Numerico
								$("#tmplCampoNumerico").tmpl(this).appendTo("#tblAtributosGrupo");
								if (AtribYaGrabados == 1) {
									for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Atributos.length - 1; i++) {
										if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Atributos[i].Id == item.Id) {
											$('#inputNumAtrib_' + item.Id).val(formatNumber(gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Atributos[i].Valor))
											break
										}
									}
								}
								break;
							case 3: //Fecha
								$("#tmplCampoFecha").tmpl(this).appendTo("#tblAtributosGrupo");
								var item = this;
								var idioma = TextosJScript[14];
								$('#inputFechaAtrib_' + item.Id).datepicker({
									showOn: 'both',
									buttonImage: ruta + 'images/colorcalendar.png',
									buttonImageOnly: true,
									buttonText: '',
									dateFormat: UsuMask.replace('MM', 'mm').replace('yyyy', 'yy'),
									showAnim: 'slideDown'
								});
								$.datepicker.setDefaults($.datepicker.regional[idioma]);
								if (AtribYaGrabados == 1) {
									for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Atributos.length - 1; i++) {
										if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Atributos[i].Id == item.Id) {
											item = gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Atributos[i]
											if (item.Valor != null && item.Valor != undefined) {
												if (item.Valor.toString().indexOf('Date') != -1) //Viene de BD
													$('#inputFechaAtrib_' + item.Id).datepicker('setDate', eval("new " + item.Valor.slice(1, -1)));
												else
													$('#inputFechaAtrib_' + item.Id).datepicker('setDate', eval("new Date('" + item.Valor + "')"));
											}
											break
										}
									}
								} else {
									if (item.Valor != null && item.Valor != undefined) {
										if (item.Valor.toString().indexOf('Date') != -1) //Viene de BD
											$('#inputFechaAtrib_' + item.Id).datepicker('setDate', eval("new " + item.Valor.slice(1, -1)));
										else
											$('#inputFechaAtrib_' + item.Id).datepicker('setDate', eval("new Date('" + item.Valor + "')"));
									}
								}
								break;
							case 4: //Boolean
								$("#tmplCombo").tmpl(this).appendTo("#tblAtributosGrupo");
								var item = this;
								var items = "{"
								//vamos componiendo con los valores de la combo el objeto Json
								for (i = 1; i >= 0; i--) {
									items += '"' + i + '": { "value":"' + i + '", "text": "' + TextosBoolean[i] + '" }, '
								}
								items = items.substr(0, items.length - 2) //quitamos la ultima coma que sobra
								items += "}"

								var optionsPermiso = { valueField: "value", textField: "text", adjustWidthToSpace: true, dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: false, data: $.parseJSON(items) };
								$('#cboAtrib_' + item.Id).fsCombo(optionsPermiso);
								//Se pone el valor si lo tuviera el atributo
								if (AtribYaGrabados == 1) {
									for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Atributos.length - 1; i++) {
										if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Atributos[i].Id == item.Id) {
											item = gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Atributos[i]
											$('#cboAtrib_' + item.Id).fsCombo('selectValue', item.Valor);
											break
										}
									}
								} else {
									$('#cboAtrib_' + item.Id).fsCombo('selectValue', item.Valor);
								}
								break;
						}
					}
			}
		})
	});

	ConfiguracionCodERPGrupo()
};
function getAtributoItem(Atributos, id) {
	//Funcion que devuelve un atributo de un item
	for (i = 0; i <= Atributos.length - 1; i++) {
		if (Atributos[i].Id == id) {
			return Atributos[i]
		}
	}
};
function CargarAtributosItem(idEmp, CodProve, IdItem, idLIA, centro, codArt) {
	//funcion que carga los atributos a nivel de Item(Panel edicion Item)
	var oItem, indAtributo = 0
	oItem = getItem(IdItem, centro, codArt)
	$('#cboAtrib_' + item.Id).fsCombo('selectValue', item.Orden);

	var params = { iAnyo: gAnyo, sGmn1: gGmn1, iCodProce: gCodProce, idEmp: idEmp, CodProve: CodProve, idItem: IdItem, idLIA: idLIA, idGrupo: gIdGrupo };
	$.when($.ajax({
		type: "POST",
		url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/CargarAtributosItem',
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(params),
		dataType: "json",
		async: false
	})).done(function (msg) {
		var oAtributos = msg.d;
		if (oAtributos.length == 0) //Si no hay atributos ocultamos el Div de Atributos    
			$('[id$=divAtributosItem]').hide()

		//Se eliminan las filas si las hubiese de la tabla(menos la cabecera)
		var table = $("#tblAtributosItem")
		table.find('tr:not(:first)').remove();

		$(oAtributos).each(function () {
			indAtributo += 1

			switch (this.Intro) {
				case 1:
					//Opcion Lista
					{
						$("#tmplCombo").tmpl(this).appendTo("#tblAtributosItem");
						var item = this;
						var items = "{"
						//vamos componiendo con los valores de la combo el objeto Json
						for (i = 0; i <= item.Valores.length - 1; i++) {
							if (item.Valores[i].Valor.toString().indexOf('Date') != -1) {
								var fecha = eval("new " + item.Valores[i].Valor.slice(1, -1))
								var strFecha = fecha.format(UsuMask);
								items += '"' + i + '": { "value":"' + item.Valores[i].Orden + '", "text": "' + strFecha + '" }, '
							} else {
								items += '"' + i + '": { "value":"' + item.Valores[i].Orden + '", "text": "' + item.Valores[i].Valor + '" }, '
							}
							//Como no se guarda el orden de la opción elegida si no el valor directamente, saco asÃƒÂ­ el orden para poder dejar marcado el correcto.
							if (item.Valor != null && item.Valor != undefined) {
								if (this.Tipo == 3) {
									if (item.Valor.toString().indexOf('Date') != -1) {
										var fechaVal = eval("new " + item.Valor.slice(1, -1))
										var strFechaVal = fechaVal.format(UsuMask);
										if (strFechaVal == strFecha) item.Orden = item.Valores[i].Orden;
									} else {
										if (item.Valor == item.Valores[i].Valor) item.Orden = item.Valores[i].Orden;
									}
								} else {
									if (item.Valor == item.Valores[i].Valor) item.Orden = item.Valores[i].Orden;
								}
							}
						}
						items = items.substr(0, items.length - 2) //quitamos la ultima coma que sobra
						items += "}"

						var optionsPermiso = { valueField: "value", textField: "text", adjustWidthToSpace: true, dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: false, data: $.parseJSON(items) };
						$('#cboAtrib_' + item.Id).fsCombo(optionsPermiso);
						$('#cboAtrib_' + item.Id).fsCombo('selectValue', item.Orden);
						$('#cboAtrib_' + item.Id).fsCombo('onSelectedValueChange', function () { RegistrarCambioAtributo($('#cboAtrib_' + item.Id)[0]); });

						if (oItem.Visto == 1) { //Si ya se ha entrado en la edicion del item previamente y estan grabados
							$('#cboAtrib_' + item.Id).fsCombo('selectValue', getAtributoItem(oItem.Atributos, item.Id).Orden);
						}
						break
					}
				default:
					{
						switch (this.Tipo) {
							case 1: //Texto
								$("#tmplCampoTexto").tmpl(this).appendTo("#tblAtributosItem");
								if (oItem.Atributos.length > 0) { //Si ya se ha entrado en la edicion del item previamente y estan grabados
									$('#inputTextAtrib_' + this.Id).val(getAtributoItem(oItem.Atributos, this.Id).Valor)
								}
								break
							case 2: //Numerico
								$("#tmplCampoNumerico").tmpl(this).appendTo("#tblAtributosItem");
								if (oItem.Atributos.length > 0) { //Si ya se ha entrado en la edicion del item previamente y estan grabados
									$('#inputNumAtrib_' + this.Id).val(formatNumber(getAtributoItem(oItem.Atributos, this.Id).Valor))
								}
								break
							case 3: //Fecha
								$("#tmplCampoFecha").tmpl(this).appendTo("#tblAtributosItem");
								var item = this;
								var idioma = TextosJScript[14];
								$('#inputFechaAtrib_' + item.Id).datepicker({
									showOn: 'both',
									buttonImage: ruta + 'images/colorcalendar.png',
									buttonImageOnly: true,
									buttonText: '',
									dateFormat: UsuMask.replace('MM', 'mm').replace('yyyy', 'yy'),
									showAnim: 'slideDown'
								}, $.datepicker.regional[idioma]);
								if (item.Valor != null && item.Valor != undefined)
									if (item.Valor.toString().indexOf('Date') != -1) //Viene de BD en milisegundos
										$('#inputFechaAtrib_' + this.Id).datepicker('setDate', eval("new " + item.Valor.slice(1, -1)));
									else
										$('#inputFechaAtrib_' + this.Id).datepicker('setDate', eval("new Date('" + item.Valor + "')"));
								if (oItem.Atributos.length > 0) { //Si ya se ha entrado en la edicion del item previamente y estan grabados
									if (getAtributoItem(oItem.Atributos, this.Id).Valor == null)
										$('#inputFechaAtrib_' + this.Id).datepicker('setDate', null);
									else {
										if (getAtributoItem(oItem.Atributos, this.Id).Valor != null && getAtributoItem(oItem.Atributos, this.Id).Valor != undefined) {
											if (getAtributoItem(oItem.Atributos, this.Id).Valor.toString().indexOf('Date') != -1) //Viene de BD en milisegundos
												$('#inputFechaAtrib_' + this.Id).datepicker('setDate', eval("new " + getAtributoItem(oItem.Atributos, this.Id).Valor.slice(1, -1)));
											else
												$('#inputFechaAtrib_' + this.Id).datepicker('setDate', eval("new Date('" + getAtributoItem(oItem.Atributos, this.Id).Valor + "')"));
										}
									}
								}
								break
							case 4: //Boolean
								$("#tmplCombo").tmpl(this).appendTo("#tblAtributosItem");
								var item = this;
								var items = "{"
								//vamos componiendo con los valores de la combo el objeto Json
								for (i = 1; i >= 0; i--) {
									items += '"' + i + '": { "value":"' + i + '", "text": "' + TextosBoolean[i] + '" }, '
								}
								items = items.substr(0, items.length - 2) //quitamos la ultima coma que sobra
								items += "}"

								var optionsPermiso = { valueField: "value", textField: "text", adjustWidthToSpace: true, dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: false, data: $.parseJSON(items) };
								$('#cboAtrib_' + item.Id).fsCombo(optionsPermiso);
								//Se pone el valor si lo tuviera el atributo
								$('#cboAtrib_' + item.Id).fsCombo('selectValue', item.Valor);

								if (oItem.Atributos.length > 0) { //Si ya se ha entrado en la edicion del item previamente y estan grabados
									$('#cboAtrib_' + item.Id).fsCombo('selectValue', getAtributoItem(oItem.Atributos, item.Id).Valor);
								}
								break;
						}
					}
			}
		})
	});
};
function CargarAtributosProveedor(idEmp, CodProve, DenEmpresa, DenProve) {
	//funcion que carga los atributos del proveedor si los tuviera(Panel proveedor)
	gIdEmp = idEmp
	gCodProve = CodProve

	gModificadaDistribucionPanelProv = false

	var indEmp, AtribPorProveedor
	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == idEmp) {
			indEmp = z
			break
		}
	}

	if (indEmp == undefined) {
		//Si se selecciona un proveedor en el arbol sin haber seleccionado su empresa previamente
		ProveedorAdjudicado = new Array();
		Empresa = { Id: idEmp, Denominacion: DenEmpresa, ProveedoresAdjudicados: ProveedorAdjudicado }
		indEmp = gAdjudicacion.EmpresasAdjudicadas.push(Empresa)
		indEmp = indEmp - 1

		Atributo = new Array();
		Distribuidor = new Array();
		Grupos = new Array();
		Proveedor = { Codigo: CodProve, Denominacion: DenProve, CodErpSeleccionado: null, Atributos: Atributo, Distribuidores: Distribuidor, Grupos: Grupos }
		gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.push(Proveedor);
	}

	var indProve
	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == CodProve) {
			indProve = i
			break
		}
	}

	if (indProve == undefined) {
		Atributo = new Array();
		Distribuidor = new Array();
		Grupos = new Array();
		Proveedor = { Codigo: CodProve, Denominacion: DenProve, CodErpSeleccionado: null, Atributos: Atributo, Distribuidores: Distribuidor, Grupos: Grupos }
		indProve = gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.push(Proveedor)
		indProve = indProve - 1
	}

	if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos.length > 0)
		AtribPorProveedor = true   //Si ya se han guardado atributos en el panel proveedor previamente    

	var params = { iAnyo: gAnyo, sGmn1: gGmn1, iCodProce: gCodProce, idEmp: idEmp, CodProve: CodProve };
	$.when($.ajax({
		type: "POST",
		url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/CargarAtributosProceso',
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(params),
		dataType: "json",
		async: false
	})).done(function (msg) {
		var oAtributos = msg.d;
		if (oAtributos.length == 0) //Si no hay atributos ocultamos el Div de Atributos    
			$('[id$=divAtributosProveedor]').hide()
		$(oAtributos).each(function () {
			switch (this.Intro) {
				case 1:
					//Opcion Lista
					{
						$("#tmplCombo").tmpl(this).appendTo("#tblAtributosProveedor");
						var item = this;
						var items = "{"
						//vamos componiendo con los valores de la combo el objeto Json
						for (i = 0; i <= item.Valores.length - 1; i++) {
							items += '"' + i + '": { "value":"' + item.Valores[i].Orden + '", "text": "' + item.Valores[i].Valor + '" }, '
							//Como no se guarda el orden de la opción elegida si no el valor directamente, saco asÃ­ el orden para poder dejar marcado el correcto.
							if (item.Valor != null && item.Valor != undefined) {
								if (item.Valor == item.Valores[i].Valor) item.Orden = item.Valores[i].Orden;
							}
						}
						items = items.substr(0, items.length - 2) //quitamos la ultima coma que sobra
						items += "}"

						var optionsPermiso = { valueField: "value", textField: "text", adjustWidthToSpace: true, dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: false, data: $.parseJSON(items) };
						$('#cboAtrib_' + item.Id).fsCombo(optionsPermiso);
						if (AtribPorProveedor != true)
							$('#cboAtrib_' + item.Id).fsCombo('selectValue', item.Orden);
						else {
							for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos.length - 1; i++) {
								if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos[i].Id == item.Id) {
									$('#cboAtrib_' + item.Id).fsCombo('selectValue', gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos[i].Orden);
									break
								}
							}
						}
						break
					}
				default:
					{
						switch (this.Tipo) {
							case 1: //Texto
								$("#tmplCampoTexto").tmpl(this).appendTo("#tblAtributosProveedor");
								if (AtribPorProveedor == true) {
									for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos.length - 1; i++) {
										if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos[i].Id == this.Id) {
											$('#inputTextAtrib_' + this.Id).val(gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos[i].Valor)
											break
										}
									}
								}
								break
							case 2: //Numerico
								$("#tmplCampoNumerico").tmpl(this).appendTo("#tblAtributosProveedor");
								if (AtribPorProveedor == true) {
									for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos.length - 1; i++) {
										if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos[i].Id == this.Id) {
											$('#inputNumAtrib_' + this.Id).val(formatNumber(gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos[i].Valor))
											break
										}
									}
								}
								break
							case 3: //Fecha
								$("#tmplCampoFecha").tmpl(this).appendTo("#tblAtributosProveedor");
								var item = this;
								var idioma = TextosJScript[14];
								$('#inputFechaAtrib_' + item.Id).datepicker({
									showOn: 'both',
									buttonImage: ruta + 'images/colorcalendar.png',
									buttonImageOnly: true,
									buttonText: '',
									dateFormat: UsuMask.replace('MM', 'mm').replace('yyyy', 'yy'),
									showAnim: 'slideDown'
								}, $.datepicker.regional[idioma]);
								if (item.Valor != null && item.Valor != undefined) {
									if (item.Valor.toString().indexOf('Date') != -1) //Viene de BD en milisegundos
										$('#inputFechaAtrib_' + item.Id).datepicker('setDate', eval("new " + item.Valor.slice(1, -1)));
									else
										$('#inputFechaAtrib_' + item.Id).datepicker('setDate', eval("new Date('" + item.Valor + "')"));
								}
								if (AtribPorProveedor == true) {
									for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos.length - 1; i++) {
										if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos[i].Id == this.Id) {
											if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos[i].Valor != null && gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos[i].Valor != undefined) {
												if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos[i].Valor.toString().indexOf('Date') != -1) //Viene de BD
													$('#inputFechaAtrib_' + item.Id).datepicker('setDate', eval("new " + gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos[i].Valor.slice(1, -1)));
												else
													$('#inputFechaAtrib_' + item.Id).datepicker('setDate', eval("new Date('" + gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos[i].Valor + "')"));
											}
											break
										}
									}
								}
								break
							case 4: //Boolean
								$("#tmplCombo").tmpl(this).appendTo("#tblAtributosProveedor");
								var item = this;
								var items = "{"
								//vamos componiendo con los valores de la combo el objeto Json
								for (i = 1; i >= 0; i--) {
									items += '"' + i + '": { "value":"' + i + '", "text": "' + TextosBoolean[i] + '" }, '
								}
								items = items.substr(0, items.length - 2) //quitamos la ultima coma que sobra
								items += "}"

								var optionsPermiso = { valueField: "value", textField: "text", adjustWidthToSpace: true, dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: false, data: $.parseJSON(items) };
								$('#cboAtrib_' + item.Id).fsCombo(optionsPermiso);
								//Se pone el valor si lo tuviera el atributo
								if (AtribPorProveedor != true)
									$('#cboAtrib_' + item.Id).fsCombo('selectValue', item.Valor);
								else {
									for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos.length - 1; i++) {
										if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos[i].Id == item.Id) {
											$('#cboAtrib_' + item.Id).fsCombo('selectValue', gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Atributos[i].Valor);
											break
										}
									}
								}
								break;
						}
					}
			}
		})
	});
};
function CargarDistribuidoresProveedorItem(Prove, Emp, Anyo, Gmn1, CodProce, Item, bPanel, Uon1, Uon2, Uon3, centro) {
	//funcion que carga los distribuidores del item
	var oItem
	var indEmp, itemYaIntegrado, distrPorProveedor, distrPorItem
	var CodERPSel = -1
	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == Emp) {
			indEmp = z
			break
		}
	}
	var indProve
	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == Prove) {
			indProve = i
			break
		}
	}
	var indItem, indGrupo
	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Codigo == gCodGrupo) {
			indGrupo = z
			for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items.length - 1; x++) {
				if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x].Id == parseInt(gIdItem) && gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x].Centro == centro) {
					indItem = x
					oItem = gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x]
					if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x].Distribuidores.length > 0) {
						distrPorItem = true
					}
					break
				}
			}
		}
	}

	if (distrPorItem != true) { //Si se ha hecho la distribucion por item no se miraria si hay distribucion por proveedor(desde el panel de proveedor)
		for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores.length - 1; i++) {
			if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores[i].PorcentajeDistribuido != "") {
				distrPorProveedor = true //Si vemos que en el panel del proveedor se ha indicado porcentajes de distribucion para todos los items y que no vengamos de empresa y se haya cambiado el codigo ERP
				break
			}
		}
	}

	var params = { CodProve: Prove, idEmp: Emp, Anyo: Anyo, sGmn1: Gmn1, codProce: CodProce, idItem: Item, sUon1: Uon1, sUon2: Uon2, sUon3: Uon3 };
	$.when($.ajax({
		type: "POST",
		url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/CargarDistribuidoresProveedorItem',
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(params),
		dataType: "json",
		async: false
	})).done(function (msg) {
		var oDistribuidores = msg.d;
		if (oDistribuidores.length <= 1 && bPanel) //Si no hay distribuidores ocultamos el Div de Distribuidores del panel de edicion del item, el distribuidor que vendra siempre es el proveedor principal
			$('[id$=divDistribuidoresItemPanel]').hide();
		else
			$('[id$=divDistribuidoresItemPanel]').show();

		$(oDistribuidores).each(function () {
			if (this.CodigosERP.length > 0) {
				for (i = 0; i <= this.CodigosERP.length - 1; i++) {
					if (this.CodigosERP[i].Traspasado == true) {
						itemYaIntegrado = true
						break
					}
				}
			}
		});

		//Se eliminan las filas si las hubiese de las tablas(menos la cabecera)
		if (bPanel) {
			var table = $("#tblDistribuidoresPanelItem")
			table.find('tr:not(:first)').remove();
		} else {
			var table = $("#tblDistribuidoresItem")
			table.find('tr:not(:first)').remove();
		}

		$(oDistribuidores).each(function () {
			if (bPanel) {
				if (itemYaIntegrado == true)
					$("#tmplDistribuidorItemTraspasado").tmpl(this).appendTo("#tblDistribuidoresPanelItem");
				else
					$("#tmplDistribuidorItemPanel").tmpl(this).appendTo("#tblDistribuidoresPanelItem");
			} else {
				if (itemYaIntegrado == true)
					$("#tmplDistribuidorItemTraspasado").tmpl(this).appendTo("#tblDistribuidoresItem");
				else
					$("#tmplDistribuidorItem").tmpl(this).appendTo("#tblDistribuidoresItem");
			}
			var item = this;
			if (item.CodigosERP.length > 0) {
				var items = "{"
				//vamos componiendo con los valores de la combo el objeto Json
				for (i = 0; i <= item.CodigosERP.length - 1; i++) {
					items += '"' + i + '": { "value":"' + item.CodigosERP[i].CodigoERP + '", "text": "' + item.CodigosERP[i].DenERP + '" }, '
					if (item.CodigosERP[i].Traspasado == true) {
						CodERPSel = i
						break
					}
				}

				items = items.substr(0, items.length - 2) //quitamos la ultima coma que sobra
				items += "}"
				var optionsPermiso = { valueField: "value", textField: "text", adjustWidthToSpace: true, dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: false, data: $.parseJSON(items) };
				if (bPanel)
					$('#cboDistribItemPanelCodERP_' + item.Codigo).fsCombo(optionsPermiso);
				else
					$('#cboDistribItemCodERP_' + item.Codigo).fsCombo(optionsPermiso);

				if (item.Id == 0) {
					//Le ponemos el manejador de evento para que cuando se cambie en la tabla de distribuidores el proveedor principal se cambie tambien el combo de la cabecera
					if (bPanel)
						$('#cboDistribItemPanelCodERP_' + item.Codigo).fsCombo('onSelectedValueChange', function () {
							ActualizarComboERPItem('cboDistribItemPanelCodERP_' + item.Codigo);
						});
					else
						$('#cboDistribItemCodERP_' + item.Codigo).fsCombo('onSelectedValueChange', function () {
							ActualizarComboERPItem('cboDistribItemCodERP_' + item.Codigo);
						});
				}

				if (itemYaIntegrado == true && CodERPSel != -1) { //Si el item ya ha sido traspasado y este fue el Codigo Erp seleccionado en su momento
					CodERPSel += 2
					if (bPanel)
						$('#cboDistribItemPanelCodERP_' + item.Codigo).fsCombo('selectItem', CodERPSel);
					else
						$('#cboDistribItemCodERP_' + item.Codigo).fsCombo('selectItem', CodERPSel);
				} else {
					if (distrPorItem == true) {//Si se ha abierto el panel de edicion del item o el panel de distribuidores del item
						for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Items[indItem].Distribuidores.length - 1; x++) {
							if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Items[indItem].Distribuidores[x].Codigo.trim() == item.Codigo) {
								if (bPanel) {
									$('#cboDistribItemPanelCodERP_' + item.Codigo).fsCombo('selectValue', gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Items[indItem].Distribuidores[x].CodErpSeleccionado);
									$('#inputDistribItemPanelPorcen_' + item.Codigo).val(gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Items[indItem].Distribuidores[x].Cantidad_Adj)
								} else {
									$('#cboDistribItemCodERP_' + item.Codigo).fsCombo('selectValue', gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Items[indItem].Distribuidores[x].CodErpSeleccionado);
									$('#inputDistribItemPorcen_' + item.Codigo).val(gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Items[indItem].Distribuidores[x].Cantidad_Adj)
								}
								break
							}
						}
					} else {
						if (distrPorProveedor == true) { //Si se ha grabado la distribucion en el panel del proveedor
							for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores.length - 1; i++) {
								if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores[i].Codigo.trim() == item.Codigo) {
									if (bPanel) {
										if (i == 0 && oItem.CodERP != null) //(i == 0 && oItem.CodERP != '' && oItem.CodERP != gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores[i].CodErpSeleccionado)
											$('#cboDistribItemPanelCodERP_' + item.Codigo).fsCombo('selectValue', oItem.CodERP);
										//else
										//$('#cboDistribItemPanelCodERP_' + item.Codigo).fsCombo('selectValue', gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores[i].CodErpSeleccionado);
										$('#inputDistribItemPanelPorcen_' + item.Codigo).val(formatNumber(oItem.CantAdj / (100 / gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores[i].PorcentajeDistribuido)))
									} else {
										$('#cboDistribItemCodERP_' + item.Codigo).fsCombo('selectValue', gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores[i].CodErpSeleccionado);
										$('#inputDistribItemPorcen_' + item.Codigo).val(formatNumber(oItem.CantAdj / (100 / gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores[i].PorcentajeDistribuido)))
									}
									break
								}
							}
						}
					}
				}

				//Se asignara el 100% de la distribucion al proveedor principal que sera el que tenga id=0
				if (itemYaIntegrado != true && distrPorProveedor != true && distrPorItem != true && item.Id == 0) {
					if (item.CodigosERP.length == 1) {
						if (bPanel) {
							$('#cboDistribItemPanelCodERP_' + item.Codigo).fsCombo('selectItem', 2); //Se selecciona el unico codigo Erp del proveedor principal
							$('#inputDistribItemPanelPorcen_' + item.Codigo).val(formatNumber(item.Cantidad_Adj))
						} else {
							$('#cboDistribItemCodERP_' + item.Codigo).fsCombo('selectItem', 2); //Se selecciona el unico codigo Erp del proveedor principal
							$('#inputDistribItemPorcen_' + item.Codigo).val(formatNumber(item.Cantidad_Adj))
						}
					} else {
						//Si no se mostrara el seleccionando en el panel de empresa, solo para el proveedor principal, no para sus distribuidores
						if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].CodErpSeleccionado != '' && gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].CodErpSeleccionado != null) {
							if (bPanel)
								$('#cboDistribItemPanelCodERP_' + item.Codigo).fsCombo('selectValue', gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].CodErpSeleccionado);
							else
								$('#cboDistribItemCodERP_' + item.Codigo).fsCombo('selectValue', gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].CodErpSeleccionado);
						} else {
							if (oItem.CodERP != '') {
								if (bPanel)
									$('#cboDistribItemPanelCodERP_' + item.Codigo).fsCombo('selectValue', oItem.CodERP)
								else
									$('#cboDistribItemCodERP_' + item.Codigo).fsCombo('selectValue', oItem.CodERP)
							}
						}

						if (bPanel)
							$('#inputDistribItemPanelPorcen_' + item.Codigo).val(formatNumber(item.Cantidad_Adj))
						else
							$('#inputDistribItemPorcen_' + item.Codigo).val(formatNumber(item.Cantidad_Adj));
					}
				}
			} else {
				if (distrPorItem == true) {//Si se ha abierto el panel de edicion del item o el panel de distribuidores del item
					for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Items[indItem].Distribuidores.length - 1; x++) {
						if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Items[indItem].Distribuidores[x].Codigo.trim() == item.Codigo) {
							if (bPanel) {
								$('#inputDistribItemPanelPorcen_' + item.Codigo).val(gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Items[indItem].Distribuidores[x].Cantidad_Adj)
							} else {
								$('#inputDistribItemPorcen_' + item.Codigo).val(gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[indGrupo].Items[indItem].Distribuidores[x].Cantidad_Adj)
							}
							break
						}
					}
				} else {
					if (distrPorProveedor == true) { //Si se ha grabado la distribucion en el panel del proveedor
						for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores.length - 1; i++) {
							if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores[i].Codigo.trim() == item.Codigo) {
								if (bPanel) {
									$('#inputDistribItemPanelPorcen_' + item.Codigo).val(formatNumber(oItem.CantAdj / (100 / gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores[i].PorcentajeDistribuido)))
								} else {
									$('#inputDistribItemPorcen_' + item.Codigo).val(formatNumber(oItem.CantAdj / (100 / gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores[i].PorcentajeDistribuido)))
								}
								break
							}
						}
					}
				}
			}
		});

		//Se borra la columna del porcentaje de distribucion si el item tiene escalados
		if (oItem.TieneEscalados == 1) {
			if (bPanel) {
				var table = $("#tblDistribuidoresPanelItem")
				table.find('tr').each(function () {
					$(this).find('th:nth-child(4), td:nth-child(4)').hide();
				});
			} else {
				var table = $("#tblDistribuidoresItem")
				table.find('tr').each(function () {
					$(this).find('th:nth-child(4), td:nth-child(4)').hide();
				});
			}
		} else {
			if (bPanel) {
				var table = $("#tblDistribuidoresPanelItem")
				table.find('tr').each(function () {
					$(this).find('th:nth-child(4), td:nth-child(4)').show();
				});
			} else {
				var table = $("#tblDistribuidoresItem")
				table.find('tr').each(function () {
					$(this).find('th:nth-child(4), td:nth-child(4)').show();
				});
			}
		}
	});
};
function ActualizarComboERPItem(idCombo) {
	var valor = $("#" + idCombo).children().find('li.fsComboItemSelected').attr("itemValue")
	var denErp = $("#" + idCombo).children().find('input.fsComboValueDisplay').val()
	$("[id$=cmbCodigosERPItem]").children("option[value=" + valor + "]").attr("selected", true);
};
function ActualizarComboERPProveedor(idCombo) {
	var valor = $("#" + idCombo).children().find('li.fsComboItemSelected').attr("itemValue")
	var denErp = $("#" + idCombo).children().find('input.fsComboValueDisplay').val()
	$("[id$=cmbCodigosERPProvAdjProveedor]").children("option[value=" + valor + "]").attr("selected", true);

	if (gCancelEventCombo == true) { //Solo se realiza la funcion por click directo en el combo del usuario, no cuando seleccionas el nodo en el arbol y se auto selecciona el combo
		gCancelEventCombo = false
		return false
	}

	//Actualizamos el distribuidor principal de los items del objeto adjudicacion si es que los tuviera
	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == gIdEmp) {
			indEmp = z
			break
		}
	}
	var indProve
	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == gCodProve) {
			indProve = i
			break
		}
	}
	var indItem, indGrupo
	if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos != undefined) {
		//Si ya se han guardado datos de este proveedor
		for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos.length - 1; z++) {
			if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Codigo == gCodGrupo) {
				indGrupo = z
				gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].CodErpSeleccionado = valor
				gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].DenErpSeleccionado = denErp
				for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items.length - 1; x++) {
					indItem = x
					if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x].Distribuidores.length > 0) {
						gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x].Distribuidores[0].CodErpSeleccionado = valor
						gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items[x].Distribuidores[0].DenErpSeleccionado = denErp
					}
				}
				break
			}
		}
	}
};
function CargarDistribuidoresProveedor(Prove, Emp, EstadoIntegracion) {
	//funcion que carga los distribuidores del proveedor si los tuviera(Panel proveedor)
	var indEmp, distrPorProveedor;

	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == Emp) {
			indEmp = z
			break
		}
	}
	var indProve
	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == Prove) {
			indProve = i
			break
		}
	}

	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores[i].CodErpSeleccionado != null && (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores[i].PorcentajeDistribuido != "")) {
			distrPorProveedor = true //Si vemos que en el panel del proveedor se ha indicado porcentajes de distribucion
			break
		}
	}

	var bAvisoMiraDistribItem = false;
	if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos.length !== 0) {
		for (var y = 0; y <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos.length - 1; y++) {
			for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[y].Items.length - 1; x++) {
				if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Grupos[y].Items[x].Distribuidores.length !== 0) {
					$("[id$=lblDistribuidoresLiteralProveedor]").text(TextosJScript[33])
					bAvisoMiraDistribItem = true;
					break;
				}
			}
			if (bAvisoMiraDistribItem) break;
		}
	}

	var params = { CodProve: Prove, idEmp: Emp };
	$.when($.ajax({
		type: "POST",
		url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/CargarDistribuidoresProveedor',
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(params),
		dataType: "json",
		async: false
	})).done(function (msg) {
		var item;
		var oDistribuidores = msg.d;
		if (oDistribuidores.length <= 1) //Si no hay distribuidores ocultamos el Div de Distribuidores, el distribuidor que vendra siempre es el proveedor principal
			$('[id$=divDistribuidoresProveedor]').hide()
		else
			$('[id$=divDistribuidoresProveedor]').show()
		$(oDistribuidores).each(function () {
			switch (EstadoIntegracion) {
				case 1: //Si el estado es Enviado(1) o Completamente integrado(4) no habra combo con los Cod ERP de los distribuidores, se sacara un label con el codERP del que se envio
				case 4:
					$("#tmplDistribuidorTraspasado").tmpl(this).appendTo("#tblDistribuidoresProveedor");
					item = this;
					if (item.CodigosERP.length > 0) {
						if (item.Id == 0 && item.CodigosERP.length == 1) {
							$('#spanDistribCodERP_' + item.Codigo).text(item.CodigosERP[0].DenERP);
						} else {
							//Si no se mostrara el seleccionando en el panel de empresa, solo para el proveedor principal, no para sus distribuidores
							if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].CodErpSeleccionado != '' && gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].CodErpSeleccionado != null && item.Id == 0) {
								$('#spanDistribCodERP_' + item.Codigo).text(gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].DenErpSeleccionado);
							}
						}
					}
					break;
				default:
					$("#tmplDistribuidor").tmpl(this).appendTo("#tblDistribuidoresProveedor");
					item = this;
					if (item.CodigosERP.length > 0) {
						var items = "{"
						//vamos componiendo con los valores de la combo el objeto Json
						for (i = 0; i <= item.CodigosERP.length - 1; i++) {
							items += '"' + i + '": { "value":"' + item.CodigosERP[i].CodigoERP + '", "text": "' + item.CodigosERP[i].DenERP + '" }, '
						}

						items = items.substr(0, items.length - 2) //quitamos la ultima coma que sobra
						items += "}"
						var optionsPermiso = { valueField: "value", textField: "text", dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, adjustWidthToSpace: true, autoComplete: false, data: $.parseJSON(items) };
						$('#cboDistribCodERP_' + item.Codigo).fsCombo(optionsPermiso);

						if (item.Id == 0) {
							//Le ponemos el manejador de evento para que cuando se cambie en la tabla de distribuidores el proveedor principal se cambie tambien el combo de la cabecera
							$('#cboDistribCodERP_' + item.Codigo).fsCombo('onSelectedValueChange', function () {
								ActualizarComboERPProveedor('cboDistribCodERP_' + item.Codigo);
							});
						}

						if ((distrPorProveedor == true) || (bAvisoMiraDistribItem == true)) { //Si se ha grabado la distribucion en el panel del proveedor previamente
							for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores.length - 1; i++) {
								if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores[i].Codigo.trim() == item.Codigo) {
									gCancelEventCombo = true
									$('#cboDistribCodERP_' + item.Codigo).fsCombo('selectValue', gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores[i].CodErpSeleccionado);
									if (bAvisoMiraDistribItem == false)
										$('#inputDistribPorcen_' + item.Codigo).val(formatNumber(gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].Distribuidores[i].PorcentajeDistribuido))
									break
								}
							}
						} else {
							if (item.Id == 0 && item.CodigosERP.length == 1) {
								gCancelEventCombo = true
								$('#cboDistribCodERP_' + item.Codigo).fsCombo('selectItem', 2); //Se selecciona el unico codigo Erp del proveedor principal
								$('#inputDistribPorcen_' + item.Codigo).val(formatNumber(100))
							} else {
								//Si no se mostrara el seleccionando en el panel de empresa, solo para el proveedor principal, no para sus distribuidores
								if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].CodErpSeleccionado != '' && gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].CodErpSeleccionado != null && item.Id == 0) {
									gCancelEventCombo = true
									$('#cboDistribCodERP_' + item.Codigo).fsCombo('selectValue', gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProve].CodErpSeleccionado);
									$('#inputDistribPorcen_' + item.Codigo).val(formatNumber(100))
								}
							}
						}
					}
			}
		});
	});
};
function ObtenerObjetoAdjudicacion() {
	//Funcion que retorna un objeto Adjudicacion.vb en el que se guardaran los datos de las empresas del proceso, proveedores adjudicados....que va introduciendo
	//el usuario en los paneles de Empresa y Proveedor
	$.when($.ajax({
		type: "POST",
		url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/ObtenerObjetoAdjudicacion',
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: false
	})).done(function (msg) {
		var oAdjudicacion = msg.d;
		gAdjudicacion = oAdjudicacion;
	})
};
function GuardarObjetoAdjudicacion(arItems) {
	//Funcion que guarda el objeto gAdjudicacion en la cache
	//arItems contiene los items para los que se ha cambiado el estado al hacer un traspaso desde un grupo
	FechasAtributos();

	if (typeof arItems == "undefined") arItems = new Array();
	var params = { objAdjudicacion: gAdjudicacion, arItems: arItems };
	$.when($.ajax({
		type: "POST",
		url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/GuardarObjetoAdjudicacion',
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(params),
		dataType: "json",
		async: false
	}))
};
function Validacion_ERP(sender) {
	var id = sender.id
	var atribId = id.substring(id.indexOf('_') + 1) //Recojo el id del atributo
	if ($(sender).closest('tr').find("td").eq(6).find("span").text() == "false")
		return false
	var valor = sender.value;
	var params = { idAtrib: atribId, Valor: valor, idEmp: gIdEmp };
	$.when($.ajax({
		type: "POST",
		url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/Validacion_Erp',
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(params),
		dataType: "json",
		async: false
	})).done(function (msg) {
		var sMensaje = msg.d;
		if (sMensaje != '' && sMensaje != null)
			alert(sMensaje)
		else
			return true
	});
};
function RegistrarCambioAtributo(sender) {
	var oldValue;
	var oldText;
	var text;
	var esGrupo;

	if ($(sender)[0].id.startsWith('cboAtrib')) {
		oldValue = $(sender).fsCombo('getOldValue');
		oldText = $(sender).fsCombo('getOldText');
		text = $(sender).fsCombo('getSelectedText');
	} else {
		oldValue = $(sender).data('oldValue');
		oldText = $(sender).data('oldValue');
		text = $(sender).val();

		if (oldText != undefined && oldText != text) $(sender).data('oldValue', text);
	}

	if (oldValue != undefined && oldText != text) {
		var datatreeAdj = $find("<%= wdtAdjudicaciones.ClientID %>");
		var nodoGrupo = datatreeAdj.get_selectedNodes()[0];
		var nodoProveedor = nodoGrupo.get_parentNode();
		var nodoEmpresa = nodoProveedor.get_parentNode();

		var atributo = $(sender).closest('tr').find("td").eq(1).find("span").text().trim();
		if (atributo.indexOf('(*) ') >= 0) atributo = atributo.replace('(*) ', '');

		if ($(sender).closest('table')[0].id.indexOf('tblAtributosGrupo') >= 0) {
			esGrupo = true;
			var sData = TextosJScript[34] + ': ' + gAnyo + '/' + gGmn1 + '/' + gCodProce + ', ' + TextosJScript[37] + ': ' + nodoEmpresa.get_text() + ', ' + TextosJScript[38] + ': ' + nodoProveedor.get_text() +
				', ' + TextosJScript[7] + ': ' + gCodGrupo + '-' + nodoGrupo.get_text() + ', ' + TextosJScript[39] + ': ' + atributo + ', ' + TextosJScript[35] + ': ' + oldText + ', ' + TextosJScript[36] + ': ' + text;
		} else {
			var sData = TextosJScript[34] + ': ' + gAnyo + '/' + gGmn1 + '/' + gCodProce + ', ' + TextosJScript[37] + ': ' + nodoEmpresa.get_text() + ', ' + TextosJScript[38] + ': ' + nodoProveedor.get_text() +
				', ' + TextosJScript[7] + ': ' + gCodGrupo + '-' + nodoGrupo.get_text() + ', ' + TextosJScript[8] + ': ' + gCodArt + ', ' + TextosJScript[39] + ': ' + atributo + ', ' + TextosJScript[35] + ': ' + oldText +
				', ' + TextosJScript[36] + ': ' + text;
		}
		RegistrarAccion(esGrupo ? ACCTraspAdjModifAtribGrupo : ACCTraspAdjModifAtribItem, sData);
	}
};
function validaFloat(sender, old) {
	//Funcion que valida si es un numero valido y si lo es lo formatea
	gModificadaDistribucionPanelProv = true
	var numero = sender.value;
	var exp
	if (UsuNumberDecimalSeparator == ',') {
		exp = /^([0-9])*[.]?([0-9])*[,]?[0-9]*$/
	} else {
		exp = /^([0-9])*[,]?([0-9])*[.]?[0-9]*$/
	}

	if (!exp.test(numero)) {
		alert(TextosJScript[13].toString().replace("XXXXX", numero));
		sender.value = ''
		gBoolValidaFloat = false
		return false;
	} else {
		numero = strToNum(numero)
		numero = formatNumber(numero)
		sender.value = numero
		gBoolValidaFloat = true
		return true
	}
};
function validaDate(sender) {
	//Funcion que valida la fecha que se introduce en los atributos
	//param=sender:Input de la fecha
	var caracterSeparador;
	var anyo;
	var mes;
	var dia;
	var valor = sender.value;
	var fechaArr = new Array();
	//Comprobamos que la fecha introducida tiene el separador correcto
	caracterSeparador = (valor.indexOf(UsuMaskSeparator) > -1 ? 1 : 0)
	if (caracterSeparador == 1) {
		fechaArr = valor.split(UsuMaskSeparator);
		//Recogemos el año, mes, dia dependiendo del formato de fecha del usuario
		switch (UsuMask) {
			case 'dd' + UsuMaskSeparator + 'MM' + UsuMaskSeparator + 'yyyy':
				anyo = fechaArr[2];
				mes = fechaArr[1];
				dia = fechaArr[0];
				break;
			case 'MM' + UsuMaskSeparator + 'dd' + UsuMaskSeparator + 'yyyy':
				anyo = fechaArr[2];
				mes = fechaArr[0];
				dia = fechaArr[1];
				break;
			case 'yyyy' + UsuMaskSeparator + 'MM' + UsuMaskSeparator + 'dd':
				anyo = fechaArr[0];
				mes = fechaArr[1];
				dia = fechaArr[2];
				break;
			default: //No es un formato valido
				sender.value = ''
				return false;
		}
	} else { //No es un formato valido
		sender.value = ''
		return false;
	}

	try {
		//Creamos el objeto fecha y comprobamos que el año,mes, dia del objeto creado es el mismo que lo introducido en el input
		var newDate = new Date(anyo, mes - 1, dia);
		if (!newDate || newDate.getFullYear() == anyo && newDate.getMonth() == mes - 1 && newDate.getDate() == dia) {
			return true;
		} else {
			sender.value = ''
			return false;
		}
	}
	catch (err) {
		sender.value = ''
		return false;
	}
};
function ValidaPorcentajesDistribucionProveedor(sender) {
	//funcion que valida que los porcentajes de distribucion que se indican en el panel de proveedor no superen el 100%
	if (gBoolValidaFloat == false) //Si no es un numero no se validara el porcentaje
		return false

	var Porcentaje;
	Porcentaje = 0
	$("[id^=inputDistribPorcen]").each(function (index) {
		Porcentaje += strToNum($(this).val())
		gBoolValidaPorcentajesDistribucionProveedor = true
	})
	if (Porcentaje > 100) {
		gBoolValidaPorcentajesDistribucionProveedor = false
		alert(TextosJScript[18])
		$("#" + sender.id).val(formatNumber(0))
	}
};
function strToNum(strNum) {
	if (strNum == undefined || strNum == null || strNum === '')
		strNum = '0'

	while (strNum.indexOf(UsuNumberGroupSeparator) >= 0) {
		strNum = strNum.replace(UsuNumberGroupSeparator, "")
	}
	strNum = strNum.replace(UsuNumberDecimalSeparator, ".")
	return parseFloat(strNum)
};
function formatNumber(num) {
	if (num == undefined || num == null || num === '')
		num = 0
	num = parseFloat(num)
	var result = num.toFixed(UsuNumberNumDecimals);
	result = addSeparadorMiles(result.toString())
	var result1 = result.substring(0, result.length - parseInt(UsuNumberNumDecimals) - 1)
	var result2 = result.substring(result.length - parseInt(UsuNumberNumDecimals), result.length)
	result = result1 + UsuNumberDecimalSeparator + result2
	return result
};
function addSeparadorMiles(nStr) {
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + UsuNumberGroupSeparator + '$2');
	}
	var retorno = x1 + x2;
	return retorno
};
function NodeChanging(sender, args) {
	//funcion que se ejecuta al seleccionar un nodo del arbol de adjudicaciones, desde aqui se guardaran
	//los datos en el objeto gAdjudicacion
	//miramos cual es el nodo que estaba seleccionado previamente para guardar sus datos		
	switch (args.getOldSelectedNodes()[0].get_level()) {
		case 0: //Empresa                
			sender._element.scrollLeft = 0
			GuardarDatosPanelEmpresa(args.getOldSelectedNodes()[0].get_key(), args.getOldSelectedNodes()[0].get_text())
			break
		case 1: //Proveedor                
			sender._element.scrollLeft = 0
			GuardarDatosPanelProveedor(args.getOldSelectedNodes()[0].get_key(), args.getOldSelectedNodes()[0].get_text(), args.getOldSelectedNodes()[0].get_parentNode().get_key())

			break
		case 2: //Grupo                   
			sender._element.scrollLeft = 0
			GuardarDatosPanelGrupo(args.getOldSelectedNodes()[0].get_key())
			GuardarObjetoAdjudicacion()
			break
	}
};
function GuardarDatosPanelEmpresa(IdEmpresa, DenEmpresa) {
	//Funcion que guarda en la variable global gAdjudicacion el codigo Erp seleccionado para cada Proveedor Adjudicado
	var tabla = $("[id$=tblProveedoresAdjudicados]");
	var i = 0;
	var Proveedor;
	var body = tabla.children()
	body.children().each(function (index) {
		var Cod, Den, DenCodErp, CodErp;
		//recorremos la tabla de proveedores adjudicados y guardamos los datos
		$(this).children().each(function (index2) {
			if (i > 0) {
				switch (index2) {
					case 0: //Codigo
						Cod = $(this).text();
						break
					case 1: //Denominacion
						Den = $(this).text();
						break
					case 2: //Codigo Erp
						DenCodErp = $(this).children().children("OPTION:selected").text()
						CodErp = $(this).children().children("OPTION:selected").val()
						if ($(this).children().children("OPTION:selected").length == 0) {
							var CodigoERP = $(this).text(); //Si no hay combo estara el codigo en la celda
							if (CodigoERP.indexOf('-') != -1) {
								CodErp = CodigoERP.substring(0, CodigoERP.indexOf('-') - 1)
								DenCodErp = CodigoERP.substring(CodigoERP.indexOf('-') + 1, CodigoERP.length)
							}
						}
						break
				}
			}
		});
		if (i > 0) { //Para evitar la cabecera de la tabla
			//GUARDAMOS LOS DATOS DE LA EMPRESA
			if (gAdjudicacion.EmpresasAdjudicadas.length == 0) {
				//Si aun no hay ninguna empresa guardada
				ProveedorAdjudicado = new Array();
				Empresa = { Id: IdEmpresa, Denominacion: DenEmpresa, ProveedoresAdjudicados: ProveedorAdjudicado }
				gAdjudicacion.EmpresasAdjudicadas.push(Empresa)

				Atributo = new Array();
				Distribuidor = new Array();
				Grupos = new Array();
				Proveedor = { Codigo: Cod, Denominacion: Den, DenErpSeleccionado: DenCodErp, CodErpSeleccionado: CodErp, Atributos: Atributo, Distribuidores: Distribuidor, Grupos: Grupos }
				gAdjudicacion.EmpresasAdjudicadas[0].ProveedoresAdjudicados.push(Proveedor)
			} else {
				var ExisteEmp = false
				//Si ya hay empresas en el objeto gAdjudicacion habra que ver si esta ya existe
				for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
					if (gAdjudicacion.EmpresasAdjudicadas[z].Id == IdEmpresa) {
						//Si existe buscamos el Proveedor y modificamos el Codigo Erp
						var ExisteProv = false
						for (var y = 0; y <= gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados.length - 1; y++) {
							if (gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].Codigo == Cod) {
								//Si existe  el Proveedor,modificamos el Codigo Erp
								ExisteProv = true
								gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].CodErpSeleccionado = CodErp
								//Tambien cambiariamos el codigo Erp del grupo y de los items si el objeto adjudicacion ya los tendria
								if (gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].Grupos) {
									for (o = 0; o <= gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].Grupos.length - 1; o++) {
										gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].Grupos[o].CodErpSeleccionado = CodErp
										if (gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].Grupos[o].Items) {
											for (t = 0; t <= gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].Grupos[o].Items.length - 1; t++) {
												var oItem;
												oItem = gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[y].Grupos[o].Items[t]
												if (oItem.Estado == -1 || oItem.Estado == 3) {
													oItem.CodERP = CodErp
													oItem.DenERP = DenCodErp
													if (oItem.Distribuidores.length > 0) {
														oItem.Distribuidores[0].CodErpSeleccionado = CodErp
														oItem.Distribuidores[0].DenErpSeleccionado = DenCodErp
													}
												}
											}
										}
									}
								}
								break
							}
						}
						ExisteEmp = true
						if (ExisteProv == false) {
							//Si no existe el proveedor adjudicado lo creamos
							Atributo = new Array();
							Distribuidor = new Array();
							Grupos = new Array();
							Proveedor = { Codigo: Cod, Denominacion: Den, DenErpSeleccionado: DenCodErp, CodErpSeleccionado: CodErp, Atributos: Atributo, Distribuidores: Distribuidor, Grupos: Grupos }
							gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados.push(Proveedor)
						}
						break
					}

				}
				if (ExisteEmp == false) {
					//Si no existe esta empresa se añadira y se añadiran los proveedores de esta empresa
					ProveedorAdjudicado = new Array();
					Empresa = { Id: IdEmpresa, Denominacion: DenEmpresa, ProveedoresAdjudicados: ProveedorAdjudicado }
					var indEmp = gAdjudicacion.EmpresasAdjudicadas.push(Empresa)

					Atributo = new Array();
					Distribuidor = new Array();
					Grupos = new Array();
					Proveedor = { Codigo: Cod, Denominacion: Den, DenErpSeleccionado: DenCodErp, CodErpSeleccionado: CodErp, Atributos: Atributo, Distribuidores: Distribuidor, Grupos: Grupos }
					gAdjudicacion.EmpresasAdjudicadas[indEmp - 1].ProveedoresAdjudicados.push(Proveedor)
				}
			}
		}
		i += 1
	});

	GuardarObjetoAdjudicacion();
};
function GuardarDatosPanelProveedor(CodProveedor, DenProveedor, IdEmp) {
	//Funcion que guarda en la variable global gAdjudicacion los datos de atributos..etc del panel de proveedor cuando selecciona otro nodo
	var CodErpSelec = $('[id$=cmbCodigosERPProvAdjProveedor]').children("OPTION:selected").val()
	var DenErpSelec = $('[id$=cmbCodigosERPProvAdjProveedor]').children("OPTION:selected").text()

	var indEmp
	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		//buscamos en el objeto la empresa a la que pertenece el proveedor que se va a guardar
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == IdEmp) {
			indEmp = z
			break
		}
	}

	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == CodProveedor) {
			//Actualizamos el codigo Erp de los grupos e items de ese proveedor
			if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos) {
				for (o = 0; o <= gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[i].Grupos.length - 1; o++) {
					if (gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[i].Grupos[o].CodErpSeleccionado == '')
						gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[i].Grupos[o].CodErpSeleccionado = CodErpSelec
					if (gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[i].Grupos[o].Items) {
						for (t = 0; t <= gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[i].Grupos[o].Items.length - 1; t++) {
							var oItem;
							oItem = gAdjudicacion.EmpresasAdjudicadas[z].ProveedoresAdjudicados[i].Grupos[o].Items[t]
							if (oItem.Estado == -1 || oItem.Estado == 3) {
								if (oItem.CodERP == '') {
									oItem.CodERP = CodErpSelec
									oItem.DenERP = DenErpSelec
								}
								if (oItem.Distribuidores.length > 0) {
									if (oItem.Distribuidores[0].CodErpSeleccionado == '') {
										oItem.Distribuidores[0].CodErpSeleccionado = CodErpSelec
										oItem.Distribuidores[0].DenErpSeleccionado = DenErpSelec
									}
								}
							}
						}
					}
				}
			};

			//Guardamos los ATRIBUTOS a nivel de proceso
			gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Atributos.splice(0, gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Atributos.length);
			var tblAtributosProv = $("[id$=tblAtributosProveedor]");
			var x = 0;
			var Proveedor;
			var body = tblAtributosProv.children();
			body.children().each(function (index) {
				var Id, Cod, Den, Tipo, Intro, Oblig, Valor, Valid_Erp, Orden;
				//recorremos la tabla de atributos del proveedor
				$(this).children().each(function (index2) {
					if (x > 0) {
						switch (index2) {
							case 0: //Id del atributo
								Id = $(this).text();
								break
							case 1: //Codigo del atributo
								Cod = $(this).text();
								break
							case 2: //Denominacion del atributo
								Den = $(this).text();
								break
							case 3: //Tipo de dato
								Tipo = $(this).text();
								break
							case 4: //Si es lista o no
								Intro = $(this).text();
								break
							case 5:
								Oblig = $(this).text();
								break
							case 6:
								Valid_Erp = $(this).text();
								break
							case 7: //Valor
								Orden = -1
								switch (Intro) {
									case '1':
										Valor = $(this).find('.fsCombo').fsCombo('getSelectedText');
										Orden = $(this).find('.fsCombo').fsCombo('getSelectedValue');
										if (Orden === '')
											Orden = -1 //Si no se selecciona nada, se le asigna -1 por ser un campo numerico
										break
									default:
										switch (Tipo) {
											case '1': //Texto
												Valor = $(this).find('textarea').val()
												break
											case '2': //Numerico
												Valor = $(this).find('INPUT').val()
												Valor = strToNum(Valor)
												break
											case '3': //Fecha
												Valor = $('#inputFechaAtrib_' + Id).datepicker('getDate');
												if (Valor != null) {
													//Valor = Valor.toString()
												}
												break
											case '4': //Boolean   
												Valor = $(this).find('.fsCombo').fsCombo('getSelectedValue');
												if (Valor == undefined)
													//Si no hay nada seleccionado en la lista
													Valor = ''
												break
										}
										break
								}
						}
					}
				})
				if (x > 0) { //Para evitar la cabecera de la tabla
					if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length == 0) {
						Atributo = new Array();
						Distribuidor = new Array();
						Grupos = new Array();
						Proveedor = { Codigo: CodProveedor, Denominacion: Den, DenErpSeleccionado: DenErpSelec, CodErpSeleccionado: CodErp, Atributos: Atributo, Distribuidores: Distribuidor, Grupos: Grupos }
						gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.push(Proveedor)
					} else {
						for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; z++) {
							if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[z].Codigo == CodProveedor) {
								if (Oblig == 'true') {
									Atributo = { Id: Id, Tipo: Tipo, Codigo: Cod, Denominacion: Den, Valor: Valor, Obligatorio: true, Orden: Orden }
								} else {
									Atributo = { Id: Id, Tipo: Tipo, Codigo: Cod, Denominacion: Den, Valor: Valor, Obligatorio: false, Orden: Orden }
								}
								gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[z].Atributos.push(Atributo)
							}
						}
					}
				}
				x += 1
			});

			var bGuardar = true
			if ($("[id$=lblDistribuidoresLiteralProveedor]").text() == TextosJScript[33]) {
				//Se ha quitado de pantalla los % para indicar q la distribución a nivel de item, PERO, si de n items solo n-2 estan distr en item, hay 2 items q siguen a nivel de prove--> no guardar
				//Si se han vuelto a poner % es q se quiere cambiar la distr de todos los item. Entonces, guardar.
				bGuardar = false;

				var x = 0;
				var tblDistribuidoresProv = $("[id$=tblDistribuidoresProveedor]")
				var bodyDistrib = tblDistribuidoresProv.children()
				bodyDistrib.children().each(function (index) {
					$(this).children().each(function (index2) {
						if (x > 0) {
							if (index2 == 3) {//Porcentaje a distribuir
								Porcentaje = $(this).find('INPUT').val()

								Porcentaje = strToNum(Porcentaje)
								if (Porcentaje !== 0) {
									bGuardar = true
								}
							}
						}
					})
					x = x + 1
				})
			}
			if (bGuardar == true) {
				//Borramos los distribuidores si los hubiese antes de grabarlos de nuevo
				gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Distribuidores.splice(0, gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Distribuidores.length);
				var tblDistribuidoresProv = $("[id$=tblDistribuidoresProveedor]");
				var x = 0;
				var Proveedor;
				var bodyDistrib = tblDistribuidoresProv.children()
				bodyDistrib.children().each(function (index) {
					var Cod, Den, CodERP, DenCodERP, Porcentaje;
					//recorremos la tabla de distribuidores del proveedor
					$(this).children().each(function (index2) {
						if (x > 0) {

							switch (index2) {
								case 0: //Codigo del proveedor
									Cod = $(this).text();
									break
								case 1: //Denominacion del proveedor
									Den = $(this).text();
									break
								case 2: //Codigo ERP
									DenCodERP = $(this).children().find('input.fsComboValueDisplay').val()
									CodERP = $(this).children().find('li.fsComboItemSelected').attr('itemValue')
									if (DenCodERP == undefined) {
										//Si es undefined es que el proveedor esta traspasado y no hay combo con los distribuidores,hay un span donde esta el seleccionado
										DenCodERP = $(this).children().text()
										CodERP = DenCodERP.substr(1, DenCodERP.indexOf('-') - 1).trim()
									} else if (CodERP == undefined) {
										//Si es undefined es que no hay ningun cod erp seleccionado en el combo
										CodERP = ''
										DenCodERP = ''
									} else if ((CodERP == '') && !(DenCodERP == '')) {
										CodERP = DenCodERP.substr(0, DenCodERP.indexOf(' - ')).trim()
									}
									break
								case 3: //Porcentaje a distribuir
									Porcentaje = $(this).find('INPUT').val()

									Porcentaje = strToNum(Porcentaje)
									if (Porcentaje === '')
										Porcentaje = '0'
									break
							}
						}
					});

					if (x > 0) { //Para evitar la cabecera de la tabla
						if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length == 0) {
							Atributo = new Array();
							Distribuidor = new Array();
							Grupos = new Array();
							Proveedor = { Codigo: CodProveedor, Denominacion: Den, DenErpSeleccionado: DenErpSelec, CodErpSeleccionado: CodErp, Atributos: Atributo, Distribuidores: Distribuidor, Grupos: Grupos }
							gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.push(Proveedor)
						} else {
							for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; z++) {
								if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[z].Codigo == CodProveedor) {
									Distribuidor = { Codigo: Cod, Denominacion: Den, PorcentajeDistribuido: Porcentaje, DenErpSeleccionado: DenCodERP, CodErpSeleccionado: CodERP }
									gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[z].Distribuidores.push(Distribuidor)
								}
							}
						}
					}
					x += 1
				});

				GuardarObjetoAdjudicacion();
			}
		}
	}
};
function GuardarDatosPanelGrupo(CodGrupo) {
	//Funcion que guarda en la variable global gAdjudicacion los datos de atributos..etc del panel de proveedor cuando selecciona otro nodo
	var CodErpSelec = $('[id$=cmbCodigosERPProvAdjGrupo]').children("OPTION:selected").val()
	var DenErpSelec = $('[id$=cmbCodigosERPProvAdjGrupo]').children("OPTION:selected").text()

	var indEmp
	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		//buscamos en el objeto la empresa a la que pertenece el proveedor que se va a guardar
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == gIdEmp) {
			indEmp = z
			break
		}
	}

	var indProv, indGrupo
	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == gCodProve) {
			indProv = i
			for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos.length - 1; z++) {
				if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Codigo == gCodGrupo) {
					//Indicamos que este grupo ya ha sido cargado
					gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Visto = 1
					indGrupo = z
					break
				}

			}
			break
		}
	}
	//Guardamos los ATRIBUTOS a nivel de grupo
	gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProv].Grupos[indGrupo].Atributos.splice(0, gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProv].Grupos[indGrupo].Atributos.length);
	gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProv].Grupos[indGrupo].CodErpSeleccionado = CodErpSelec
	gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProv].Grupos[indGrupo].DenErpSeleccionado = DenErpSelec
	var tblAtributosGrupo = $("[id$=tblAtributosGrupo]")
	var x = 0;
	var Proveedor;
	var body = tblAtributosGrupo.children()
	body.children().each(function (index) {
		var Id, Cod, Den, Tipo, Intro, Oblig, Valor, Valid_Erp, Orden;
		//recorremos la tabla de atributos del grupo
		$(this).children().each(function (index2) {
			if (x > 0) {
				switch (index2) {
					case 0: //Id del atributo
						Id = $(this).text();
						break
					case 1: //Codigo del atributo
						Cod = $(this).text();
						break
					case 2: //Denominacion del atributo
						Den = $(this).text();
						break
					case 3: //Tipo de dato
						Tipo = $(this).text();
						break
					case 4: //Si es lista o no
						Intro = $(this).text();
						break
					case 5:
						Oblig = $(this).text();
						break
					case 6:
						Valid_Erp = $(this).text();
						break
					case 7: //Valor
						Orden = -1 //Inicializo el orden, solo si es lista entrara a recoger el orden del valor seleccionado
						switch (Intro) {
							case '1':
								Valor = $(this).find('.fsCombo').fsCombo('getSelectedText');
								Orden = $(this).find('.fsCombo').fsCombo('getSelectedValue');
								if (Orden === '')
									Orden = -1 //Si no se selecciona nada, se le asigna -1 por ser un campo numerico
								break
							default:
								switch (Tipo) {
									case '1': //Texto
										Valor = $(this).find('textarea').val()
										break
									case '2': //Numerico
										Valor = $(this).find('INPUT').val()
										Valor = strToNum(Valor)
										break
									case '3': //Fecha
										Valor = $('#inputFechaAtrib_' + Id).datepicker('getDate');
										if (Valor != null) {
											//Valor = Valor.toString()
										}
										break
									case '4': //Boolean   
										Valor = $(this).find('.fsCombo').fsCombo('getSelectedValue');
										if (Valor == undefined)
											//Si no hay nada seleccionado en la lista
											Valor = ''
										break
								}
								break
						}
				}
			}
		});
		if (x > 0) { //Para evitar la cabecera de la tabla
			for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProv].Grupos.length - 1; z++) {
				if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProv].Grupos[z].Codigo == gCodGrupo) {
					if (Oblig == 'true') {
						Atributo = { Id: Id, Tipo: Tipo, Codigo: Cod, Denominacion: Den, Valor: Valor, Obligatorio: true, Orden: Orden }
					} else {
						Atributo = { Id: Id, Tipo: Tipo, Codigo: Cod, Denominacion: Den, Valor: Valor, Obligatorio: false, Orden: Orden }
					}
					gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[indProv].Grupos[z].Atributos.push(Atributo)
				}
			}
		}
		x += 1
	});
};
function EnvioItem(bBorrarAdjAnt) {
	gEnvioPdte = false;

	btnAceptarItem_Click()
	var idItem = editRow.get_cellByColumnKey("Key_ID_ITEM").get_value()
	var centro = editRow.get_cellByColumnKey("Key_CENTRO").get_value()
	var codArt = editRow.get_cellByColumnKey("Key_CODART").get_value()
	var oItem = getItem(idItem, centro, codArt)
	if ($("[id$=pnlMensaje]").css('display') != "block") { //Si hay un mensaje de advertencia en la distribucion de los distribuidores no haremos el traspaso
		if (bMantenerEstAdjEnTraspasoAdj) {
			TraspasoERPItem(idItem, false, centro, codArt, bBorrarAdjAnt)
		}
		else {
			if (oItem.Estado == -1 || oItem.Estado == 3) {
				TraspasoERPItem(idItem, false, centro, codArt, bBorrarAdjAnt)
			} else {
				TraspasoERPItem(idItem, true, centro, codArt, bBorrarAdjAnt)
			}
		}
		if ($("#hErrorIntegracion").val() != "1") {
			$find('<%=mpePanelItem.ClientID%>').hide()
			gEnvioPdte = false;
		} else {
			gEnvioPdte = true;
			$("#hErrorIntegracion").val("0");
		}
	} else {
		gEnvioPdte = true
	}
};
function ExpandirOcultarArbol() {
	//Expande u oculta el arbol de Adjudicaciones desde el panel del grupo
	if ($("[id$=divContenedorArbol]").css('display') == "none") {
		$("[id$=divContenedorArbol]").css('width', '25%')
		$("[id$=divContenedorArbol]").css('display', '')
		$("[id$=divItemsGrupo]").css('width', '72%')
		$("[id$=imgExpandirOcultarArbol]").attr('src', ruta + "/App_Themes/<%=Me.Page.Theme%>/images/incluir_color_izq.gif")
	} else {
		$("[id$=divContenedorArbol]").css('display', 'none')
		$("[id$=divItemsGrupo]").css('width', '97%')
		$("[id$=imgExpandirOcultarArbol]").attr('src', ruta + "/App_Themes/<%=Me.Page.Theme%>/images/incluir.gif")
	}
};
function getWindowSize() {
	/*Función con la que recuperamos el alto y ancho de la pantalla, no de la página, es decir, no tiene en cuenta el scroll, 
			por lo que sirve para controlar que los paneles se muestren hacia arriba si la posición en la que vamos a mostrar el panel es cercana a alguno de los límites*/
	var winW = 630, winH = 460;
	if (document.body && document.body.offsetWidth) {
		winW = document.body.offsetWidth;
		winH = document.body.offsetHeight;
	}
	if (document.compatMode == 'CSS1Compat' &&
			document.documentElement &&
			document.documentElement.offsetWidth) {
		winW = document.documentElement.offsetWidth;
		winH = document.documentElement.offsetHeight;
	}
	if (window.innerWidth && window.innerHeight) {
		winW = window.innerWidth;
		winH = window.innerHeight;
	}
	return [winW, winH];
};
function showPosition(relativeElement, showElement) {
	/*Mediante las funciones findPositionWithScrolling y getWindowSize, determinamos el punto en el que posicionar un panel de forma que no quede cortado en ningún extremo
			relativeElement: Elemento en relación al cual vamos a calcular la posición en la que mostrar le shownElement
			shownElement: Elemento a mostrar, vamos a tener en cuenta sus dimensiones.*/
	var position = [0, 0];
	var dimensions = [0, 0];
	var appart = 10; //lo separamos un poco
	var x = 0, y = 0;
	if (relativeElement)
		var position = findPositionWithScrolling(relativeElement);
	if (showElement)
		dimensions = [showElement.offsetWidth, showElement.offsetHeight];
	if (dimensions[0] == 0 && dimensions[1] == 0) {
		var showElementX = parseInt(showElement.style.width);
		var showElementY = parseInt(showElement.style.height);
		dimensions = [showElementX, showElementY];
	}
	var windowSize = getWindowSize();
	var scrollPosition = getScroll();
	if (position[0] + dimensions[0] + appart > windowSize[0] + scrollPosition[0]) {
		if (position[0] - dimensions[0] - appart > 0)
			x = position[0] - dimensions[0] - appart;
		else
			x = 0;
	} else {
		x = position[0] + appart;
	}
	if (position[1] + dimensions[1] + appart > windowSize[1] + scrollPosition[1]) {
		if (position[1] - dimensions[1] - appart > 0)
			y = position[1] - dimensions[1] - appart;
		else
			y = 0;
	} else {
		y = position[1] + appart;
	}
	return [x, y];
};
function findPositionWithScrolling(oElement) {
	//Función que retorna la posicion en la pagina del objeto
	if (typeof (oElement.offsetParent) != 'undefined') {
		var originalElement = oElement;
		for (var posX = 0, posY = 0; oElement; oElement = oElement.offsetParent) {
			posX += oElement.offsetLeft;
			posY += oElement.offsetTop;
			if (oElement != originalElement && oElement != document.body && oElement != document.documentElement) {
				posX -= oElement.scrollLeft;
				posY -= oElement.scrollTop;
			}
		}
		return [posX, posY];
	} else {

		return [oElement.x, oElement.y];
	}
};
function MostrarPanel(relativeElement, Id_LIA) {
	/*Muestra el panel con la descripcion del error de la integracion del item
			//relativeelement: Elemento relativo a partir del cual se mostrara el panel a su lado
			//Id_LIA: Id de LOG_ITEM_ADJ*/
	var divMensaje = document.getElementById("divMensajeErrorIntegracion")
	var posicion = showPosition(relativeElement, divMensaje);
	var params = { Id_LIA: Id_LIA };
	x = posicion[0];
	y = posicion[1];

	$.when($.ajax({
		type: "POST",
		url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/Devolver_MensajeErrorIntegracion',
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(params),
		dataType: "json",
		async: false
	})).done(function (msg) {
		var sMensaje = msg.d;
		if (sMensaje != '' && sMensaje != null)
			$("[id$=divMensajeErrorIntegracion]").text(sMensaje)
		else
			$("[id$=divMensajeErrorIntegracion]").text(TextosJScript[17]) //Se ha producido un error al grabar en Bd
	});

	divMensaje.style.top = y;
	divMensaje.style.left = x;
	divMensaje.style.display = 'block';
};
function OcultarPanelMensajeError(sender) {
	sender.style.display = 'none';
};
function getScroll() {
	if (window.pageYOffset != undefined) {
		return [pageXOffset, pageYOffset];
	} else {
		var sx, sy, d = document, r = d.documentElement, b = d.body;
		sx = r.scrollLeft || b.scrollLeft || 0;
		sy = r.scrollTop || b.scrollTop || 0;
		return [sx, sy];
	}
};
function actualizarAdj(sender, e) {
	if (e.getCell().get_column().get_key() == "Key_CANT_ADJ") {
		var idItem = e.getCell().get_row().get_cellByColumnKey("Key_ID_ITEM").get_value();
		var centro = e.getCell().get_row().get_cellByColumnKey("Key_CENTRO").get_value();
		var codArt = e.getCell().get_row().get_cellByColumnKey("Key_CODART").get_value();
		var cantAdj = e.get_displayText();
		oItem = getItem(idItem, centro, codArt);
		if (cantAdj <= oItem.CantAdj) {
			oItem.cantAdj = cantAdj;
			e.getCell().set_text(oItem.cantAdj + ' ' + oItem.Unidad);
		} else {
			e.set_cancel(true);
			$('[id$=lblMensaje]').text(TextosJScript[1].toString().replace("XXXXX", formatNumber(cantAdj).toString() + ' ' + oItem.Unidad))
			$('[id$=imgCerrarPanelMensaje]').attr('src', ruta + '/images/Bt_Cerrar.png')
			$('[id$=imgCerrarPanelMensaje]').css('display', '')
			$('[id$=imgPanelMensaje]').attr('src', ruta + '/App_Themes/<%=Me.Page.Theme%>/images/Icono_Error_Amarillo_40x40.gif')
			$('[id$=btnCancelarPanelMensaje]').hide();
			$find('<%=mpePanelMensaje.ClientID%>').show();
			e.getCell().set_text(oItem.cantAdj + ' ' + oItem.Unidad);
		}
	}
};
function getItems() {
	//Devuelve un listado con los items del proceso y proveedor seleccionado
	var indEmp;
	var oItems = new Array();

	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		//buscamos en el objeto la empresa a la que pertenece el proveedor que se va a guardar
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == gIdEmp) {
			indEmp = z
			break
		}
	}

	for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
		if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == gCodProve) {
			for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos.length - 1; z++) {
				if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Codigo == gCodGrupo) {
					for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items.length - 1; x++) {
						return gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[z].Items;
					}
					break
				}
			}
			break
		}
	}
	return oItems;
};
function TraspasarCentro() {
	//<summary>Traspasa el centro seleccionado en el combo a todos los items cargados en pantalla</summary>//
	var checked = $("#chkTraspasarCentro").prop("checked");
	var centro = $('[id$=cmbCentros]').children("OPTION:selected").text();
	var oItems = getItems();
	if (checked == true) {
		var grid = $find("<%= whdgItems.ClientID %>");
		var rows = grid.get_gridView().get_rows();

		for (j = 0; j < rows.get_length() ; j++) {
			var EstadoInt = rows.get_row(j).get_cellByColumnKey("Key_ESTADOINT").get_value();
			//Traspasamos el centro a aquellas lineas que no se hayan integrado, o bien cuyo traspaso haya sido erroneo.
			if (EstadoInt == "" || EstadoInt == "3") {
				oItems[j].Centro = centro;
				rows.get_row(j).get_cellByColumnKey("Key_CENTRO").set_value(centro);
			}
		}
	}
};
function ValidacionesIntegracion(gAdjudicacion, iNivel, Empresa, Proveedor, Grupo, Item) {
	var params = { objAdjudicacion: gAdjudicacion, iNivel: iNivel, strEmpresa: Empresa, strProveedor: Proveedor, strGrupo: Grupo, strItem: Item };
	var integracionOK = false;
	$.when($.ajax({
		type: "POST",
		url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/ValidacionesIntegracion',
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(params),
		dataType: "json",
		async: false
	})).done(function (msg) {
		if (msg.d != "") {
			if (iNivel == 0) { //Nivel de empresa
				$("[id$=lblErrorIntegracionEmpresa]").text(msg.d)
				$("#alertaIntegracionEmpresa").show()
			} else if (iNivel == 1) { //Nivel de proveedor
				$("[id$=lblErrorIntegracionProveedor]").text(msg.d)
				$("#alertaIntegracionProveedor").show()
			} else if (iNivel >= 2) { //Nivel de grupo o de Item, usamos el mismo label/div.
				$("[id$=lblErrorIntegracionGrupo]").text(msg.d)
				$("#alertaIntegracionGrupo").show()
			}
		} else {
			integracionOK = true
			$("#alertaIntegracionEmpresa").hide()
			$("#alertaIntegracionProveedor").hide()
			$("#alertaIntegracionGrupo").hide()
		}
	});
	return integracionOK;
};
function AplicarTodos(Control, Id, Quien) {
	var indEmp
	var indProve
	var indGrupo
	var indCombo
	var valorCombo

	for (var z = 0; z <= gAdjudicacion.EmpresasAdjudicadas.length - 1; z++) {
		//buscamos en el objeto la empresa a la que pertenece el proveedor que se va a guardar
		if (gAdjudicacion.EmpresasAdjudicadas[z].Id == gIdEmp) {
			indEmp = z;

			for (var i = 0; i <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados.length - 1; i++) {
				if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Codigo == gCodProve) {
					indProve = i

					for (var j = 0; j <= gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos.length - 1; j++) {
						if (gAdjudicacion.EmpresasAdjudicadas[indEmp].ProveedoresAdjudicados[i].Grupos[j].Codigo == gCodGrupo) {
							indGrupo = j

							if (Control == 'cboAtrib_') {
								indCombo = $('#cboAtrib_' + Id).fsCombo('getSelectedValue')
								valorCombo = $('#cboAtrib_' + Id).fsCombo('getSelectedText')

								if (indCombo == '') {
									indCombo = -1;
								}
							}
							break;
						}
					}
					break;
				}
			}
			break;
		}
	};

	var mensaje
	mensaje = TextosJScript[29] + Quien //Se va a proceder a trasladar el atributo  <NOMBRE_CAMPO> 
	if (Control == 'cboAtrib_') {
		mensaje += TextosJScript[30] + valorCombo; //con el valor <VALOR_CAMPO>
	} else {
		mensaje += TextosJScript[30] + $('[id=' + Control + Id + ']').val(); //con el valor <VALOR_CAMPO>
	}
	mensaje += TextosJScript[31] + "\n"//a todas las líneas.
	mensaje += TextosJScript[32] //¿Desea continuar?

	customConfirm(mensaje, AplicarTodosCallBack, [Control, Id, indEmp, indProve, indGrupo, indCombo, valorCombo]);

	return false;
};
function AplicarTodosCallBack(confirm, params) {
	if (confirm) {
		for (var x = 0; x <= gAdjudicacion.EmpresasAdjudicadas[params[2]].ProveedoresAdjudicados[params[3]].Grupos[params[4]].Items.length - 1; x++) {
			if (gAdjudicacion.EmpresasAdjudicadas[params[2]].ProveedoresAdjudicados[params[3]].Grupos[params[4]].Items[x].Id !== parseInt(gIdItem)) {
				if (gAdjudicacion.EmpresasAdjudicadas[params[2]].ProveedoresAdjudicados[params[3]].Grupos[params[4]].Items[x].Estado == -1) {
					for (var v = 0; v <= gAdjudicacion.EmpresasAdjudicadas[params[2]].ProveedoresAdjudicados[params[3]].Grupos[params[4]].Items[x].Atributos.length - 1; v++) {
						Atributo = gAdjudicacion.EmpresasAdjudicadas[params[2]].ProveedoresAdjudicados[params[3]].Grupos[params[4]].Items[x].Atributos[v]
						if (Atributo.Id == params[1]) {
							if (params[0] == 'cboAtrib_') {
								gAdjudicacion.EmpresasAdjudicadas[params[2]].ProveedoresAdjudicados[params[3]].Grupos[params[4]].Items[x].Visto = 1; //Para q carge con este valor, los combos de las otras lineas, aunque aun no los hayas abierto.
								switch (Atributo.Tipo.toString()) {
									case "4":
										if (params[5] == -1)
											params[5] = ''
										Atributo.Valor = params[5]
										break;
									case "3":
										if (params[5] == -1) {
											params[5] = 0
											params[6] = null
										}
										Atributo.Orden = params[5]
										Atributo.Valor = params[6]
										break;
									default:
										Atributo.Orden = params[5]
										Atributo.Valor = params[6]
								}
							} else {
								switch (Atributo.Tipo.toString()) {
									case "2":
										Atributo.Valor = strToNum($('[id=' + params[0] + params[1] + ']').val());
										break;
									case "3":
										Atributo.Valor = $('[id=' + params[0] + params[1] + ']').datepicker('getDate');
										break;
									default:
										Atributo.Valor = $('[id=' + params[0] + params[1] + ']').val();
								}
								break;
							}
						}
					}
				}
			}
		}
	}
};
function RegistrarAccion(Accion, sData) {
	var params = { Acc: Accion, sDat: sData };
	$.ajax({
		type: "POST",
		url: rutaFS + 'GS/Adjudicaciones/TraspasoAdjudicaciones.aspx/RegistrarAccion',
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(params),
		dataType: "json",
		async: false
	});
};