﻿Imports System.IO
Public Class downloadAdjuntosZip
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dataDir As String = ConfigurationManager.AppSettings("temp")
        If Not IsPostBack Then
            Dim oOferta As FSNServer.Ofertas
            oOferta = FSNServer.Get_Object(GetType(FSNServer.Ofertas))
            With Request
                oOferta.Anyo = CType(.QueryString("Anyo"), Integer)
                oOferta.GMN = CType(.QueryString("GMN1"), String)
                oOferta.Proce = CType(.QueryString("Proce"), Integer)
                oOferta.Proveedor = CType(.QueryString("Prove"), String)
                oOferta.Id = CType(.QueryString("Ofe"), Long)
            End With
            Dim dsAdjuntos As DataSet

            dsAdjuntos = oOferta.ObtenerAdjuntos
            Dim dt As DataTable = dsAdjuntos.Tables(0)
            Dim i As Integer
            Dim sPath As String = ConfigurationManager.AppSettings("temp") + "\" + FSNLibrary.modUtilidades.GenerateRandomPath()
            For i = 0 To dt.Rows.Count - 1
                If Not IsDBNull(dt.Rows(i)("IDPORTAL")) Then
                    'Si el IDPORTAL tiene valor quiere decir que el fichero subido desde portal no se ha transferido
                    oOferta.TransferirAdjuntoDePortal(dt.Rows(i)("TIPO"), dt.Rows(i)("ID"), dt.Rows(i)("IDPORTAL"))
                End If
                oOferta.NomAdjunto = dt.Rows(i)("NOM")
                oOferta.SaveAdjunToDiskZip(sPath, dt.Rows(i)("ID"))
            Next i
            Dim NombreFicheroZip As String
            Dim arrPath() As String = Nothing
            NombreFicheroZip = FSNLibrary.modUtilidades.Comprimir(sPath, "*.*", sPath, True, False)
            Dim FicheroZip As String() = Split(NombreFicheroZip, "##")
            Dim fullName As String = sPath & "\" & FicheroZip(0)
            Dim input As System.IO.FileStream = New System.IO.FileStream(fullName, System.IO.FileMode.Open, System.IO.FileAccess.Read)
            Dim buffer(input.Length - 1) As Byte
            input.Read(buffer, 0, buffer.Length)

            Call Context.Response.AddHeader("Content-Type", "application/octet-stream")
            Call Context.Response.AddHeader("Content-Disposition", "attachment;filename=" & FicheroZip(0))
            Call Context.Response.BinaryWrite(buffer)
            Call Context.Response.End()
            input.Close()
        End If
    End Sub
End Class