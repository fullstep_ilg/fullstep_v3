﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Script.Services
Imports System.Text.RegularExpressions


' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
Public Class ValidarCIF
    Inherits System.Web.Services.WebService

    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    <WebMethod()> _
    Public Function ValidarCIF_NIF(ByVal strCifNif As String) As Resultado
        Dim MiRespuesta As New Resultado
        Try
            If Trim(strCifNif) = "" Then
                MiRespuesta.Resultado = ""
            Else
                Dim NifValidado As NumeroNif = NumeroNif.CompruebaNif(strCifNif)
                If NifValidado.EsCorrecto Then
                    MiRespuesta.Resultado = ""
                Else
                    MiRespuesta.Resultado = "El NIF/CIF es incorrecto"
                End If
            End If
            Return MiRespuesta
        Catch ex As Exception
            MiRespuesta.Resultado = "El NIF/CIF es incorrecto"
            Return MiRespuesta
        End Try
    End Function
    Public Function Verificar_NIF(ByVal valor As String) As Boolean
        Dim aux As String

        valor = valor.ToUpper ' ponemos la letra en mayúscula
        aux = valor.Substring(0, valor.Length - 1) ' quitamos la letra del NIF

        If aux.Length >= 7 AndAlso IsNumeric(aux) Then
            aux = CalculaNIF(aux) ' calculamos la letra del NIF para comparar con la que tenemos
        Else
            Return False
        End If

        If valor <> aux Then ' comparamos las letras
            Return False
        End If

        Return True
    End Function

    Private Function Verificar_CIF(ByVal valor As String) As Boolean
        Dim strLetra As String, strNumero As String, strDigit As String
        Dim strDigitAux As String
        Dim auxNum As Integer
        Dim i As Integer
        Dim suma As Integer
        Dim letras As String

        'letras = "ABCDEFGHKLMPQSX"
        letras = "ABCDEFGHLMJKNPQRSUVWXYZ"
        valor = UCase(valor)

        If Len(valor) < 9 OrElse Not IsNumeric(Mid(valor, 2, 7)) Then
            Return False
        End If

        strLetra = Mid(valor, 1, 1)     ' letra del CIF
        strNumero = Mid(valor, 2, 7)    ' Codigo de Control
        strDigit = Mid(valor, 9)        ' CIF menos primera y ultima posiciones

        If InStr(letras, strLetra) = 0 Then ' comprobamos la letra del CIF (1ª posicion)
            Return False
        End If

        For i = 1 To 7
            If i Mod 2 = 0 Then
                suma = suma + CInt(Mid(strNumero, i, 1))
            Else
                auxNum = CInt(Mid(strNumero, i, 1)) * 2
                suma = suma + (auxNum \ 10) + (auxNum Mod 10)
            End If
        Next

        suma = (10 - (suma Mod 10)) Mod 10

        Select Case strLetra
            Case "F", "J", "K", "N", "P", "Q", "R", "S", "U", "V", "W"
                suma = suma + 64
                strDigitAux = Chr(suma)
            Case "X", "Y", "Z"
                If strLetra = "X" Then
                    strDigitAux = Mid(CalculaNIF(strNumero), 8, 1)
                ElseIf strLetra = "Y" Then
                    strDigitAux = Mid(CalculaNIF("1" & strNumero), 8, 1)
                ElseIf strLetra = "Z" Then
                    strDigitAux = Mid(CalculaNIF("2" & strNumero), 8, 1)
                End If
            Case "A", "B", "C", "D", "E", "G", "H", "L", "M" 'ABCDEFGHLM
                strDigitAux = CStr(suma)
        End Select
        If strDigit = strDigitAux Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Function CalculaNIF(ByVal strA As String) As String

        Const cCADENA As String = "TRWAGMYFPDXBNJZSQVHLCKE"
        Const cNUMEROS As String = "0123456789"
        Dim a, b, c, NIF As Integer
        Dim sb As New StringBuilder

        strA = Trim(strA)
        If Len(strA) = 0 Then Return ""

        ' Dejar sólo los números
        For i As Integer = 0 To strA.Length - 1
            If cNUMEROS.IndexOf(strA(i)) > -1 Then
                sb.Append(strA(i))
            End If
        Next

        strA = sb.ToString
        a = 0
        NIF = CInt(Val(strA))
        Do
            b = CInt(Int(NIF / 24))
            c = NIF - (24 * b)
            a = a + c
            NIF = b
        Loop While b <> 0
        b = CInt(Int(a / 23))
        c = a - (23 * b)

        Return strA & Mid(cCADENA, CInt(c + 1), 1)
    End Function


End Class




''' <summary>
''' Representa un número. En la clase se desglosan las distintas opciones que se puedan
''' encontrar
''' </summary>
Public Class NumeroNif
    ''' <summary>
    ''' Tipos de Códigos.
    ''' </summary>
    ''' <remarks>Aunque actualmente no se utilice el término CIF, se usa en la enumeración
    ''' por comodidad</remarks>
    Private Enum TiposCodigosEnum
        NIF
        NIE
        NIE_Y
        NIE_Z
        CIF
    End Enum

    ' Número tal cual lo introduce el usuario
    Private m_numero As String
    Private tipo As TiposCodigosEnum

    ''' <summary>
    ''' Parte de Nif: En caso de ser un Nif intracomunitario, permite obtener el cógido del país
    ''' </summary>
    Public Property CodigoIntracomunitario() As String
        Get
            Return m_CodigoIntracomunitario
        End Get
        Friend Set(value As String)
            m_CodigoIntracomunitario = Value
        End Set
    End Property
    Private m_CodigoIntracomunitario As String
    Friend Property EsIntraComunitario() As Boolean
        Get
            Return m_EsIntraComunitario
        End Get
        Set(value As Boolean)
            m_EsIntraComunitario = Value
        End Set
    End Property
    Private m_EsIntraComunitario As Boolean

    ''' <summary>
    ''' Parte de Nif: Letra inicial del Nif, en caso de tenerla
    ''' </summary>
    Public Property LetraInicial() As String
        Get
            Return m_LetraInicial
        End Get
        Friend Set(value As String)
            m_LetraInicial = Value
        End Set
    End Property
    Private m_LetraInicial As String

    ''' <summary>
    ''' Parte de Nif: Bloque numérico del NIF. En el caso de un NIF de persona física,
    ''' corresponderá al DNI
    ''' </summary>
    Public Property Numero() As Integer
        Get
            Return i_Numero
        End Get
        Friend Set(value As Integer)
            i_Numero = value
        End Set
    End Property
    Private i_Numero As Integer

    ''' <summary>
    ''' Parte de Nif: Dígito de control. Puede ser número o letra
    ''' </summary>
    Public Property DigitoControl() As String
        Get
            Return m_DigitoControl
        End Get
        Friend Set(value As String)
            m_DigitoControl = Value
        End Set
    End Property
    Private m_DigitoControl As String

    ''' <summary>
    ''' Valor que representa si el Nif introducido es correcto
    ''' </summary>
    Public Property EsCorrecto() As Boolean
        Get
            Return m_EsCorrecto
        End Get
        Friend Set(value As Boolean)
            m_EsCorrecto = Value
        End Set
    End Property
    Private m_EsCorrecto As Boolean

    ''' <summary>
    ''' Cadena que representa el tipo de Nif comprobado:
    '''     - NIF : Número de identificación fiscal de persona física
    '''     - NIE : Número de identificación fiscal extranjería
    '''     - CIF : Código de identificación fiscal (Entidad jurídica)
    ''' </summary>
    Public ReadOnly Property TipoNif() As String
        Get
            Return tipo.ToString()
        End Get
    End Property

    ''' <summary>
    ''' Constructor. Al instanciar la clase se realizan todos los cálculos
    ''' </summary>
    ''' <param name="numero">Cadena de 9 u 11 caracteres que contiene el DNI/NIF
    ''' tal cual lo ha introducido el usuario para su verificación</param>
    Private Sub New(numero As String)
        ' Se eliminan los carácteres sobrantes
        numero = EliminaCaracteres(numero)

        ' Todo en maýusculas
        numero = numero.ToUpper()

        ' Comprobación básica de la cadena introducida por el usuario
        If numero.Length <> 9 AndAlso numero.Length <> 11 Then
            Throw New ArgumentException("El NIF no tiene un número de caracteres válidos")
        End If

        Me.m_numero = numero
        Desglosa()

        Select Case tipo
            Case TiposCodigosEnum.NIF, TiposCodigosEnum.NIE, TiposCodigosEnum.NIE_Y, TiposCodigosEnum.NIE_Z
                Me.EsCorrecto = CompruebaNif()
                Exit Select
            Case TiposCodigosEnum.CIF
                Me.EsCorrecto = CompruebaCif()
                Exit Select
        End Select
    End Sub

#Region "Preparación del número (desglose)"

    ''' <summary>
    ''' Realiza un desglose del número introducido por el usuario en las propiedades
    ''' de la clase
    ''' </summary>
    Private Sub Desglosa()
        Dim n As Int32
        If m_numero.Length = 11 Then
            ' Nif Intracomunitario
            EsIntraComunitario = True
            CodigoIntracomunitario = m_numero.Substring(0, 2)
            LetraInicial = m_numero.Substring(2, 1)
            Int32.TryParse(m_numero.Substring(3, 7), n)
            DigitoControl = m_numero.Substring(10, 1)
            tipo = GetTipoDocumento(LetraInicial(0))
        Else
            ' Nif español
            tipo = GetTipoDocumento(m_numero(0))
            EsIntraComunitario = False
            If tipo = TiposCodigosEnum.NIF Then
                LetraInicial = String.Empty
                Int32.TryParse(m_numero.Substring(0, 8), n)
            ElseIf tipo = TiposCodigosEnum.NIE_Y Then
                LetraInicial = m_numero.Substring(0, 1)
                Int32.TryParse("1" & m_numero.Substring(1, 7), n)
            ElseIf tipo = TiposCodigosEnum.NIE_Z Then
                LetraInicial = m_numero.Substring(0, 1)
                Int32.TryParse("2" & m_numero.Substring(1, 7), n)
            Else
                LetraInicial = m_numero.Substring(0, 1)
                Int32.TryParse(m_numero.Substring(1, 7), n)
            End If
            DigitoControl = m_numero.Substring(8, 1)
        End If
        Numero = n
    End Sub

    ''' <summary>
    ''' En base al primer carácter del código, se obtiene el tipo de documento que se intenta
    ''' comprobar
    ''' </summary>
    ''' <param name="letra">Primer carácter del número pasado</param>
    ''' <returns>Tipo de documento</returns>
    Private Function GetTipoDocumento(letra As Char) As TiposCodigosEnum
        Dim regexNumeros As New Regex("[0-9]")
        If regexNumeros.IsMatch(letra.ToString()) Then
            Return TiposCodigosEnum.NIF
        End If

        'Dim regexLetrasNIE As New Regex("[XYZ]")
        ' If regexLetrasNIE.IsMatch(letra.ToString()) Then
        'Return TiposCodigosEnum.NIE
        'End If
        If letra = "X" Then
            Return TiposCodigosEnum.NIE
        End If
        If letra = "Y" Then
            ' m_numero = Replace(m_numero, "Y", "1")
            Return TiposCodigosEnum.NIE_Y
        End If
        If letra = "Z" Then
            ' m_numero = Replace(m_numero, "Z", "2")
            Return TiposCodigosEnum.NIE_Z
        End If
        Dim regexLetrasCIF As New Regex("[ABCDEFGHJPQRSUVNW]")
        If regexLetrasCIF.IsMatch(letra.ToString()) Then
            Return TiposCodigosEnum.CIF
        End If

        Throw New ApplicationException("El código no es reconocible")
    End Function



    ''' <summary>
    ''' Eliminación de todos los carácteres no numéricos o de texto de la cadena
    ''' </summary>
    ''' <param name="numero">Número tal cual lo escribe el usuario</param>
    ''' <returns>Cadena de 9 u 11 carácteres sin signos</returns>
    Private Function EliminaCaracteres(numero As String) As String
        ' Todos los carácteres que no sean números o letras
        Dim caracteres As String = "[^\w]"
        Dim regex As New Regex(caracteres)
        Return regex.Replace(numero, "")
    End Function

#End Region

#Region "Cálculos"

    Private Function CompruebaNif() As Boolean
        Return DigitoControl = GetLetraNif()
    End Function

    ''' <summary>
    ''' Cálculos para la comprobación del Cif (Entidad jurídica)
    ''' </summary>
    Private Function CompruebaCif() As Boolean
        Dim letrasCodigo As String() = {"J", "A", "B", "C", "D", "E", _
         "F", "G", "H", "I"}

        Dim n As String = Numero.ToString("0000000")
        Dim sumaPares As Int32 = 0
        Dim sumaImpares As Int32 = 0
        Dim sumaTotal As Int32 = 0
        Dim i As Int32 = 0
        Dim retVal As Boolean = False

        ' Recorrido por todos los dígitos del número
        For i = 0 To n.Length - 1
            Dim aux As Int32
            Int32.TryParse(n(i).ToString(), aux)

            If (i + 1) Mod 2 = 0 Then
                ' Si es una posición par, se suman los dígitos
                sumaPares += aux
            Else
                ' Si es una posición impar, se multiplican los dígitos por 2 
                aux = aux * 2

                ' se suman los dígitos de la suma
                sumaImpares += SumaDigitos(aux)
            End If
        Next
        ' Se suman los resultados de los números pares e impares
        sumaTotal += sumaPares + sumaImpares

        ' Se obtiene el dígito de las unidades
        Dim unidades As Int32 = sumaTotal Mod 10

        ' Si las unidades son distintas de 0, se restan de 10
        If unidades <> 0 Then
            unidades = 10 - unidades
        End If

        Select Case LetraInicial
            ' Sólo números
            Case "A", "B", "E", "H"
                retVal = DigitoControl = unidades.ToString()
                Exit Select

                ' Sólo letras
            Case "K", "P", "Q", "S"
                retVal = DigitoControl = letrasCodigo(unidades)
                Exit Select
            Case Else

                retVal = (DigitoControl = unidades.ToString()) OrElse (DigitoControl = letrasCodigo(unidades))
                Exit Select
        End Select

        Return retVal

    End Function

    ''' <summary>
    ''' Obtiene la suma de todos los dígitos
    ''' </summary>
    ''' <returns>de 23, devuelve la suma de 2 + 3</returns>
    Private Function SumaDigitos(digitos As Int32) As Int32
        Dim sNumero As String = digitos.ToString()
        Dim suma As Int32 = 0

        For i As Int32 = 0 To sNumero.Length - 1
            Dim aux As Int32
            Int32.TryParse(sNumero(i).ToString(), aux)
            suma += aux
        Next
        Return suma
    End Function

    ''' <summary>
    ''' Obtiene la letra correspondiente al Dni
    ''' </summary>
    Private Function GetLetraNif() As String
        Dim indice As Integer = Numero Mod 23
        Return "TRWAGMYFPDXBNJZSQVHLCKET"(indice).ToString()
    End Function

    ''' <summary>
    ''' Obtiene una cadena con el número de identificación completo
    ''' </summary>
    Public Overrides Function ToString() As String
        Dim nif As String
        Dim formato As String = "{0:0000000}"

        If tipo = TiposCodigosEnum.CIF AndAlso LetraInicial = "" Then
            formato = "{0:00000000}"
        End If
        If tipo = TiposCodigosEnum.NIF Then
            formato = "{0:00000000}"
        End If

        nif = If(EsIntraComunitario, CodigoIntracomunitario, Convert.ToString(String.Empty + LetraInicial & String.Format(formato, Numero)) & DigitoControl)
        Return nif
    End Function

#End Region

    ''' <summary>
    ''' Comprobación de un número de identificación fiscal español
    ''' </summary>
    ''' <param name="numero">Numero a analizar</param>
    ''' <returns>Instancia de <see cref="NumeroNif"/> con los datos del número.
    ''' Destacable la propiedad <seealso cref="NumeroNif.EsCorrecto"/>, que contiene la verificación
    ''' </returns>
    Public Shared Function CompruebaNif(numero As String) As NumeroNif
        Return New NumeroNif(numero)
    End Function

End Class