﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Script.Services

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
Public Class ValidarIBAN
    Inherits System.Web.Services.WebService

    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    <WebMethod()> _
    Public Function ValidarIBAN(ByVal strIBAN As String) As Resultado
        Dim Letras As String '* 26
        Dim Dividendo As Integer
        Dim Resto As Integer
        Dim MiRespuesta As New Resultado
        Try
            If Trim(strIBAN) = "" Then
                MiRespuesta.Resultado = ""
                Return MiRespuesta
            Else
                Letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

                ' Quita la palabra IBAN
                strIBAN = Replace(strIBAN, "IBAN", "")

                ' Quita los posibles espaciosjkj
                strIBAN = Replace(strIBAN, " ", "")

                ' Calcula el valor de las letras, las quita y añade el valor al final
                strIBAN = Mid(strIBAN, 3, Len(strIBAN) - 2) & CStr(InStr(1, Letras, Left(strIBAN, 1)) + 9) & CStr(InStr(1, Letras, Mid(strIBAN, 2, 1)) + 9)

                ' Quita los digitos de control y los pone al final
                strIBAN = Mid(strIBAN, 3, Len(strIBAN) - 2) & Left(strIBAN, 2)

                For Contador = 1 To Len(strIBAN)
                    Dividendo = Resto & Mid(strIBAN, Contador, 1)
                    Resto = Dividendo Mod 97
                Next Contador

                If Resto = 1 Then
                    MiRespuesta.Resultado = ""
                Else
                    MiRespuesta.Resultado = "El IBAN es incorrecto"
                End If
                Return MiRespuesta
            End If
        Catch ex As Exception
            MiRespuesta.Resultado = "El IBAN es incorrecto"
            Return MiRespuesta
        End Try
    End Function

End Class