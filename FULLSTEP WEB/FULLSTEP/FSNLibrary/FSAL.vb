﻿Imports System.Configuration
Imports System.Threading
Imports System.Web
Imports System.Web.UI


Public Class FSAL
    Implements IHttpModule
    Private m_sErrorFile_FSAL As String = ConfigurationManager.AppSettings("logtemp") & "\log_FSALErrores.txt"

    ' In the Init function, register for HttpApplication 
    ' events by adding your handlers.
    Public Sub Init(ByVal application As HttpApplication) Implements IHttpModule.Init
        AddHandler application.BeginRequest, AddressOf Me.Application_BeginRequest
        AddHandler application.EndRequest, AddressOf Me.Application_EndRequest
        AddHandler application.PreRequestHandlerExecute, AddressOf Me.Application_PreRequestHandlerExecute
    End Sub

    ''' <summary>
    ''' Elimina los recursos (distintos de la memoria) utilizados por el módulo que implementa IHttpModule.
    ''' </summary>
    ''' <remarks>El método Dispose realiza todo el trabajo de limpieza final anterior a la eliminación del módulo de la canalización de ejecución.</remarks>
    Public Sub Dispose() Implements IHttpModule.Dispose

    End Sub

    Private Sub Application_PreRequestHandlerExecute(ByVal source As Object, ByVal e As EventArgs)
        Dim application As HttpApplication = CType(source, HttpApplication)
        Dim context As HttpContext = application.Context

        If ConfigurationManager.AppSettings("FSAL_Origen").Contains("PORTAL") Then
            If Not IsNothing(context.Session) Then
                If Not IsNothing(context.Session("FS_Portal_User")) Then
                    HttpContext.Current.Items("USUCOD") = context.Session("FS_Portal_User").Cod
                    HttpContext.Current.Items("PROVECOD") = context.Session("FS_Portal_User").CodProve
                End If
            End If
        End If

    End Sub

    Private Sub Application_BeginRequest(ByVal source As Object, ByVal e As EventArgs)
        ' Create HttpApplication and HttpContext objects to access
        ' request and response properties.
        Dim application As HttpApplication = CType(source, HttpApplication)
        Dim context As HttpContext = application.Context
        Dim fecha As DateTime = DateTime.UtcNow
        Dim ms As Integer = fecha.Millisecond
        Dim sfechapet As String
        Dim fechapet As DateTime
        Dim sIdRegistro As String
        Dim rndRandom As New Random

        Dim sURLWebService As String = ConfigurationManager.AppSettings("URL_FSAL_RegistrarAccesos_Client")

        Dim sURLjquery As String = ConfigurationManager.AppSettings("ruta")

        'Inserta el script en las páginas
        If context.Request.CurrentExecutionFilePathExtension = ".aspx" Then
            sfechapet = Format(fecha, "yyyy-MM-dd HH:mm:ss") & "." & Format(ms, "000")
            fechapet = fecha
            sIdRegistro = fechapet.Ticks.ToString() & Format(rndRandom.Next(1, 99999999), "00000000")
            HttpContext.Current.Items("FECHAPET") = fechapet
            HttpContext.Current.Items("IDREGISTRO") = sIdRegistro

            If (context.Request.Headers("x-microsoftajax") Is Nothing) And (context.Request.Headers("x-ajax") Is Nothing) Then
                HttpContext.Current.Response.CacheControl = "no-cache" '' HTTP 1.1.
                HttpContext.Current.Response.AppendHeader("Pragma", "no-cache") '' HTTP 1.0.
                HttpContext.Current.Response.Expires = -1 '' Proxies.
                Dim _watcher As New FSALFilter(context.Response.Filter, sURLWebService, sIdRegistro, context.Request.Url.AbsoluteUri.Split("?")(0))
                application.Response.Filter = _watcher
                _watcher.Flush()
                _watcher.Dispose()
                _watcher = Nothing
            End If
        End If
    End Sub

    Private Sub Application_EndRequest(ByVal source As Object, ByVal e As EventArgs)
        Dim application As HttpApplication = CType(source, HttpApplication)

        Dim context As HttpContext = application.Context
        Dim fecha As DateTime = DateTime.UtcNow
        Dim ms As Integer = fecha.Millisecond
        Dim sfecha As String = Format(fecha, "yyyy-MM-dd HH:mm:ss") & "." & Format(ms, "000")
        Dim sfechapet As String
        Dim Pagina As Page
        Dim iPost As Int16 = 0
        Dim sUrlPagina As String
        Dim iP As String
        Dim sUsuario As String
        Dim sPaginaOrigen As String
        Dim iTipo As Int16
        Dim sNavegador As String
        Dim fechapet As DateTime
        Dim sIdRegistro As String
        Dim sProveedor As String
        Dim sQueryString As String

        If context.Request.CurrentExecutionFilePathExtension = ".aspx" AndAlso application.Response.ContentType.Contains("text/html") Then
            'cuando se está descargando un adjunto, se está mostrando una excel,... el contentType cambia a: "application/octet-stream","application/vnd.ms-excel"...
            fechapet = HttpContext.Current.Items("FECHAPET")
            sIdRegistro = HttpContext.Current.Items("IDREGISTRO")
            If TypeOf context.Handler Is Page Then
                Pagina = context.Handler
                If Pagina.IsPostBack Then
                    iPost = 1
                End If
                If Not (context.Request.Headers("x-microsoftajax") Is Nothing) Or Not (context.Request.Headers("x-ajax") Is Nothing) Then
                    iPost = 2
                End If
            End If

            sUrlPagina = context.Request.Url.AbsoluteUri.Split("?")(0) 'para quitar ?=prove=
            If context.Request.Url.AbsoluteUri.Split("?").Length > 1 Then
                sQueryString = context.Request.Url.AbsoluteUri.Split("?")(1)
            Else
                sQueryString = Nothing
            End If

            iP = context.Request.UserHostAddress

            If ConfigurationManager.AppSettings("FSAL_Origen").Contains("PORTAL") Then
                sUsuario = HttpContext.Current.Items("USUCOD")
                sProveedor = HttpContext.Current.Items("PROVECOD")
            Else
                If Not context.User Is Nothing Then
                    If Not IsNothing(context.User.Identity.Name) AndAlso context.User.Identity.Name.Contains("\") AndAlso context.User.Identity.Name.Split("\").Length > 1 Then
                        sUsuario = context.User.Identity.Name.Split("\")(1)
                    Else
                        sUsuario = context.User.Identity.Name
                    End If
                Else
                    sUsuario = ""
                End If
                sProveedor = Nothing
            End If

            If Not context.Request.UrlReferrer Is Nothing Then
                sPaginaOrigen = DBNullToStr(context.Request.UrlReferrer.AbsoluteUri.Split("?")(0))
            Else
                sPaginaOrigen = Nothing
            End If

            iTipo = 1

            sfechapet = Format(fechapet, "yyyy-MM-dd HH:mm:ss") & "." & Format(fechapet.Millisecond, "000")
            sNavegador = HttpContext.Current.Request.ServerVariables("HTTP_USER_AGENT") ' Obtener el navegador  

            Dim sArgs As String = ""

            sArgs &= "Tipo=" & iTipo
            sArgs &= "&Producto=" & ConfigurationManager.AppSettings("FSAL_Origen") '"FULLSTEP WEB ó FULLSTEP PORTAL"
            sArgs &= "&fechapet=" & sfechapet
            sArgs &= "&fecha=" & sfecha
            sArgs &= "&pagina=" & sUrlPagina
            sArgs &= "&iPost=" & iPost
            sArgs &= "&iP=" & iP
            sArgs &= "&sUsuCod=" & DBNullToStr(sUsuario)
            sArgs &= "&sPaginaOrigen=" & DBNullToStr(sPaginaOrigen)
            sArgs &= "&sNavegador=" & DBNullToStr(sNavegador)
            sArgs &= "&IdRegistro=" & DBNullToStr(sIdRegistro)
            sArgs &= "&sProveCod=" & DBNullToStr(sProveedor)
            sArgs &= "&sQueryString=" & HttpUtility.UrlEncode(DBNullToStr(sQueryString))
            sArgs &= "&sNormalizarFechas=" & 0

            'La llamada real al servicio se hace mediante un thread para que no interfiera en el tiempo de carga de la página
            'Dim oThread As Thread = New Thread(AddressOf CallFSALService)
            'oThread.Start(sArgs)
            ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf CallFSALService), sArgs)
        End If
    End Sub

    ''' <summary>
    ''' Procedimiento incoporporado apra realizar la llamada de forma asincrona a traves de un Thread
    ''' </summary>
    ''' <param name="sArgs">Array con los argumentos de llamada al WebService</param>
    ''' <remarks></remarks>
    Private Sub CallFSALService(ByVal sArgs As Object)

        Try

            Dim webRequest As Net.HttpWebRequest
            Dim bParams As Byte()

            bParams = Text.Encoding.ASCII.GetBytes(sArgs)

            webRequest = Net.HttpWebRequest.Create(ConfigurationManager.AppSettings("URL_FSAL_RegistrarAccesos_Server") & "/" & "FSALRegistrarAccesos")
            webRequest.Method = "POST"
            webRequest.KeepAlive = False
            webRequest.ContentType = "application/x-www-form-urlencoded"
            webRequest.ContentLength = bParams.Length

            Dim strRequestStream As IO.Stream
            strRequestStream = webRequest.GetRequestStream()
            strRequestStream.Write(bParams, 0, bParams.Length)
            strRequestStream.Close()

            Dim webResponse As Net.HttpWebResponse
            webResponse = webRequest.GetResponse()

        Catch ex As Exception

            Dim sMensaje As String = "Error FSNLibrary en " & ConfigurationManager.AppSettings("FSAL_Origen") & ", Sub CallFSALService(): " & ex.Message
            EscribirError(sMensaje)

        End Try

    End Sub

    ''' <summary>
    ''' Escribe un fichero log con los errores controlados que se han producido durante la ejecucion de FSNLibrary-FSAL
    ''' </summary>
    ''' <param name="sMensaje">Mensaje del error controlado que hay que escribir en el log</param>
    ''' <remarks>Llamada desde=CallFSALService.</remarks>
    Private Sub EscribirError(ByVal sMensaje As String)
        Dim oFileW As IO.StreamWriter
        Dim bExisteLog As Boolean = IO.File.Exists(m_sErrorFile_FSAL)

        Try
            oFileW = New IO.StreamWriter(m_sErrorFile_FSAL, True)
            If Not bExisteLog Then
                oFileW.WriteLine("Fichero de Errores del Registro de Accesos FSAL")
                oFileW.WriteLine("===============================================")
            End If
            oFileW.WriteLine(Format(Now(), "yyyy/MM/dd HH:mm:ss") & " => " & sMensaje)
            oFileW.Close()
        Catch ex As Exception
            'NO vamos a registrar los posibles errores de acceso al log de errores de la FSNLibrary por su continua y multiple ejecucion
            Exit Sub
        End Try

    End Sub

End Class