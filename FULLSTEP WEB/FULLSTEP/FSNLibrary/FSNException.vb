﻿Public Class FSNException
    Inherits Exception

    Public Enum TipoError
        CodigoAlbaranDuplicado = 1
        NoExisteArchivo = 2
        ErrorEnPaqueteDTS = 3
        TodasLasLineasHanSidoRecepcionadas = 4
    End Enum

    Private _Number As TipoError

    Public Property Number() As TipoError
        Get
            Return _Number
        End Get
        Set(ByVal value As TipoError)
            _Number = value
        End Set
    End Property

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

End Class
