﻿Public Class TiposDeDatos
    Public Enum TipoGeneral
        SinTipo = 0
        TipoString = 1
        TipoNumerico = 2
        TipoFecha = 3
        TipoBoolean = 4
        TipoTextoCorto = 5 '25 char
        TipoTextoMedio = 6 '800 char
        TipoTextoLargo = 7 '4000 char
        TipoArchivo = 8
        TipoDesglose = 9
        TipoCheckBox = 10
        TipoPresBajaLog = 11
        TipoDepBajaLog = 12
        TipoDesgloseOblig = 13
        TipoDesgloseObligNoPopup = 14
        TipoEditor = 15
        TipoEnlace = 16
    End Enum
    Public Enum Aplicaciones
        EP = &H7FFF
        EMP = &H8000
        PM = &HFFFF
        QA = &H1FFFF
        Otra = &H5FFF
    End Enum
    Public Enum AplicacionesEnNumero
        EP = 32767
        EMP = 32768
        PM = 65535
        QA = 131071
        Otra = 24575
    End Enum
    Public Enum EstadoIntegracion As Byte
        ParcialmenteIntegrado = 0
        EsperaAcuseRecibo = 1
        ConError = 3
        TotalmenteIntegrado = 4
    End Enum
    'Estado de integracion de los procesos (PROCE_INT.ESTADO o PROCE_INT.ESTADO_ADJ)
    Public Enum EstadoIntegracionProce As Byte
        Pendiente = 0
        ParcialEsperaAcuseRecibo = 11
        ParcialError = 13
        ParcialOK = 14
        TotalEsperaAcuseRecibo = 21
        TotalError = 23
        TotalOK = 24
    End Enum
    Public Structure Aplicacion
        Private _Aplicacion As Aplicaciones
        Public ReadOnly Property Codigo() As Integer
            Get
                Return _Aplicacion
            End Get
        End Property
        Public ReadOnly Property CodigoSQL() As Integer
            Get
                Select Case _Aplicacion
                    Case Aplicaciones.EP
                        Return 4
                    Case Aplicaciones.PM
                        Return 2
                    Case Aplicaciones.QA
                        Return 3
                    Case Else
                        Return 0
                End Select
            End Get
        End Property
        Public ReadOnly Property Nombre() As String
            Get
                Select Case _Aplicacion
                    Case Aplicaciones.EP
                        Return "FULLSTEP EP"
                    Case Aplicaciones.PM
                        Return "FULLSTEP PM"
                    Case Aplicaciones.QA
                        Return "FULLSTEP QA"
                    Case Else
                        Return String.Empty
                End Select
            End Get
        End Property
        ''' <summary>
        ''' Constructor de la clase Aplicacion
        ''' </summary>
        ''' <param name="Aplicacion">Código de aplicación</param>
        Public Sub New(ByVal Aplicacion As Aplicaciones)
            _Aplicacion = Aplicacion
            If String.IsNullOrEmpty(_Aplicacion) Then _
                Throw New Exception("Código de aplicación no válido: " & Aplicacion)
        End Sub
        ''' <summary>
        ''' Función de conversión de tipos del tipo Aplicacion a tipo Integer
        ''' </summary>
        ''' <param name="value">Elemento Aplicacion a convertir</param>
        ''' <returns>Un elemento Integer con el valor del código de aplicación</returns>
        Public Shared Widening Operator CType(ByVal value As Aplicacion) As Integer
            Return value.Codigo
        End Operator
        ''' <summary>
        ''' Función de conversión de tipos del tipo Integer a tipo Aplicacion
        ''' </summary>
        ''' <param name="value">Elemento Integer a convertir</param>
        ''' <returns>Un elemento Aplicacion con el código de aplicación especificado si es válido</returns>
        Public Shared Narrowing Operator CType(ByVal value As Integer) As Aplicacion
            Return New Aplicacion(value)
        End Operator
        ''' <summary>
        ''' Función de conversión de tipos del tipo Aplicacion a tipo String
        ''' </summary>
        ''' <param name="value">Elemento Aplicacion a convertir</param>
        ''' <returns>Un elemento String con el nombre de la aplicación</returns>
        Public Shared Widening Operator CType(ByVal value As Aplicacion) As String
            Return value.ToString()
        End Operator
        ''' <summary>
        ''' Función de conversión de tipos del tipo String a tipo Aplicacion
        ''' </summary>
        ''' <param name="value">Nombre de la aplicación</param>
        ''' <returns>Un elemento Aplicacion con la aplicación especificada si es válida</returns>
        Public Shared Narrowing Operator CType(ByVal value As String) As Aplicacion
            Select Case value
                Case "FULLSTEP EP"
                    Return New Aplicacion(Aplicaciones.EP)
                Case "FULLSTEP PM"
                    Return New Aplicacion(Aplicaciones.PM)
                Case "FULLSTEP QA"
                    Return New Aplicacion(Aplicaciones.QA)
                Case Else
                    Throw New Exception("Nombre de aplicación no válido: " & value)
            End Select
        End Operator
        ''' <summary>
        ''' Función de conversión de tipos del tipo Aplicacion a tipo Aplicaciones
        ''' </summary>
        ''' <param name="value">Elemento Aplicacion a convertir</param>
        ''' <returns>Un elemento Aplicaciones con el valor de la aplicación</returns>
        Public Shared Widening Operator CType(ByVal value As Aplicacion) As Aplicaciones
            Return CType(value.Codigo, Aplicaciones)
        End Operator
        ''' <summary>
        ''' Función de conversión de tipos del tipo Aplicaciones a tipo Aplicacion
        ''' </summary>
        ''' <param name="value">Variable de tipo Aplicaciones</param>
        ''' <returns>Un elemento Aplicacion inicializado con la aplicación especificada si es válida</returns>
        Public Shared Narrowing Operator CType(ByVal value As Aplicaciones) As Aplicacion
            Return New Aplicacion(value)
        End Operator
        ''' <summary>
        ''' Sobreescribe la función ToString para devolver el nombre de la aplicación
        ''' </summary>
        ''' <returns>El nombre de la aplicación</returns>
        Public Overrides Function ToString() As String
            Return Nombre
        End Function
    End Structure
    Public Enum ModulosIdiomas
        Menu = 1
        Solicitudes = 2
        AltaSolicitudes = 3
        Solicitud = 4
        Seguimiento = 5
        DetalleSeguimiento = 6
        HistoricoEstados = 7
        AnularSolicitud = 8
        DetallePedido = 9
        DetalleProceso = 10
        ComentEstado = 11
        AccionSolicitud = 12
        ComentariosSolic = 13
        DenominacionDest = 14
        DenominacionUP = 15
        Workflow = 16
        Aprobacion = 17
        Rechazo = 18
        AprobSuperior = 19
        DetalleCampoArchivo = 20
        Traslados = 21
        AprobacionOK = 22
        Usuarios = 23
        Trasladar = 24
        TrasladoOK = 25
        DetallePersona = 26
        GestionTraslado = 27
        Devolucion = 28
        DevolucionOK = 29
        Desglose = 30
        EmisionOK = 31
        PedidoDirecto = 32
        RechazoOK = 33
        ImposibleAccion = 34
        DetalleProveedor = 35
        DesgloseControl = 36
        CamposControl = 37
        DownloadFile = 38
        AttachFile = 39
        Certificados = 40
        SolicitarCertif = 41
        AltaCertificados = 42
        CertifProveedores = 43
        ImpExp = 44
        Opciones = 45
        CertifCalendario = 46
        ImpExp_Sel = 47
        CertifPublicadoOK = 48
        CertifNoPublicado = 49
        DetalleCertificado = 50
        EnviarAvisos = 51
        PublicarCertificado = 52
        Proveedores = 53
        AttachFileProcesos = 54
        BusquedaArticulos = 55
        ValidarArticulo = 56
        PuntProveedores = 57
        FichaProveedor = 58
        Materiales = 59
        Presupuestos = 60
        EnviarSolicitud = 61
        ReemitirSolicitud = 62
        NoConformidades = 63
        AltaNoConformidad = 64
        NoConformidadEmitidaOK = 65
        NoConfonfEmitidaWorkfl = 66
        DetalleNoConformidad = 67
        GuardarInstancia = 68
        CierreNoConformidad = 69
        AprobRechazoAccion = 70
        ComprobarNoConfPortal = 71
        AttachFilePedidos = 72
        MantenimientoMat = 73
        AltaMaterialQA = 74
        ModificarDenMatQA = 75
        AsignaCertifAMatQA = 76
        AsignarMaterialesAMatQA = 77
        EliminarMaterialesQA = 78
        SelectorCamposMatQA = 79
        ParametrosGenerales = 80
        FiltroCertificados = 81
        ResultCertificados = 82
        SelectorCamposCertif = 83
        CierreNoConformidadConRevisor = 84
        CertifSolicitadosOK = 85
        NoConformidadRevisor = 86
        DetalleMatGSProv = 87
        RechazoNoConfRevisor = 88
        SelectorCamposProveQA = 89
        Listados = 90
        flowDiagram = 91
        RealizarAccion = 92
        AccionRealizadaOK = 93
        AvisosPrecondiciones = 94
        AccionInvalida = 95
        EnviarMailProveedores = 96
        MailProveedores = 97
        DetalleDenominacionPresupuestos = 98
        UnidadesOrganizativas = 99
        Espera = 100
        Otros = 101
        BusquedaNuevoCodArticulos = 102
        UsuarioNoParticipante = 103
        BusquedaSolicitudes = 104
        DetalleSeguimientoPadre = 105
        UnidadesNegocio = 106
        ModificarDenUnidadNeg = 107
        EliminarUnidadNeg = 108
        AñadirUnidadNeg = 109
        UltimasAdjudicaciones = 110
        ProvFavServer = 111
        SolicitudesFavoritas = 112
        EnviarFavoritos = 113
        DetalleSolicitudesHijas = 114
        popupDesglose = 115
        TablaExterna = 116
        AyudaCampo = 117
        PaginaError = 118
        NoConformidadEnProceso = 119
        AsignarObjetivosySuelos = 120
        Webparts = 121
        Login = 122
        Contraseñas = 123
        Activos = 124
        CentrosCoste = 125
        Partidas = 126
        BusquedaAvanzadaNC = 127
        ConfiguracionFiltroQA = 128
        VisorNoConformidades = 129
        TipoPedido = 130
        PanelInfo = 131
        EnvioMails = 132
        ListaValores = 133
        NoConformidadAnulada = 134
        PopupFichaProveedor = 135
        Adjuntos = 136
        AltaContrato = 137
        BusquedaEmpresas = 138
        BusquedaContratos = 139
        BusquedaAvanzadaCert = 140
        VisorCertificados = 141
        ConfiguracionFiltrosContratos = 142
        VisorContratos = 143
        DetalleContrato = 144
        GestionContrato = 145
        GestionContratoTraslado = 146
        BuscadorSolicitudes = 147
        DetalleEmpresa = 148
        HistoricoPartidasPresupuestarias = 149
        PartidasPresupuestarias = 150
        MantenimientoControlPresupuestacion = 151
        NoPasoControlarLineaVinculada = 152
        AsignacionUsuarios = 153
        controlarLineaVinculadaMover = 154
        DesgloseActividad = 155
        Editor = 156
        Colaboracion = 157 'Pendiente de saber si definitivamente tendrÃƒÂ¡ este nÃƒÂºmero
        Notificaciones = 158
        PerfilUsuarioCN = 159
        CategoriasCN = 160
        GruposCN = 161
        WebpartPedidos = 162
        VisorIntegracion = 163
        VisorFacturas = 164
        ConfiguracionFiltrosFacturas = 165
        DatosGeneralesFactura = 166
        DetalleFactura = 167
        FacturaPDF = 168
        Contratos = 169
        PopUpTextosMedioYLargo = 170
        VisorPerfiles = 171
        DetallePerfil = 172
        ComparativaPerfiles = 173
        'GS
        agendaExport = 174
        actaExport = 175
        VisorPlantillasGS = 176
        TraspasoAdjudicaciones = 177
        SelectorPlantillasGS = 178
        'BI
        Cubos = 179
        'EnvioMail
        Enviomail = 180
        VisorActividad = 181
        PoliticaCookies = 182
        'Entidades de notificacion
        EntidadesNotificacion = 185
        BuscadorAtribsListaExterna = 186
        QlikApps = 187
        VisorQlik = 188
        AsignarUnidadesOrganizativas = 190
        ConfigNivelEscalacionProves = 191
        PanelEscalacionProveedores = 192
        Factura = 193
        FacturasFavoritas = 194
        BuscadorContratos = 195

        ' Módulos EP (Sumar 32768 al cod del módulo en BBDD)
        EP_CatalogoWeb = 32773
        EP_Destinos = 32775
        EP_UnidadesPedido = 32776
        EP_AprobacionPedidos = 32782
        EP_DetalleAprovisionador = 32791
        EP_RecepcionPedidos = 32797
        EP_Seguimiento = 32799
        EP_DatosContacto = 32822
        EP_PedidoLibre = 32823
        EP_PedidoLibreRellenar = 32825
        EP_PedidoLibreOkEnviarCesta = 32826
        EP_EmisionPedidos = 32828
        EP_SelecFavoritos = 32852
        EP_Favoritos = 32841
        EP_NuevoFavorito = 32853
        EP_Activos = 32856
        EP_Parametros = 32796
        EP_PaginadorWebHierarchical = 32860
        EP_Notificaciones = 32869
        EP_PedidoAbierto = 32870
    End Enum
    Public Enum TipoDeSolicitud
        SolicitudDeCompras = 1
        Certificado = 2
        NoConformidad = 3
        Otros = 0
        Contrato = 5
        Autofactura = 7
        PedidoExpress = 8
        PedidoNegociado = 9
        SolicitudDePedidoCatalogo = 10
        SolicitudDePedidoContraAbierto = 11
        Encuesta = 12
        Factura = 13
        SolicitudQA = 14
    End Enum
    Public Enum TipoCampoGS
        SinTipo = 0
        'Public Enum TipoCampoSC
        DescrBreve = 1
        DescrDetallada = 2
        Importe = 3
        Cantidad = 4
        FecNecesidad = 5
        IniSuministro = 6
        FinSuministro = 7
        ArchivoEspecific = 8
        PrecioUnitario = 9
        PrecioUnitarioAdj = 10
        ProveedorAdj = 11
        CantidadAdj = 12
        TotalLineaAdj = 13
        TotalLineaPreadj = 14
        'End Enum

        'Public Enum TipoCampoCertificado
        NombreCertif = 20
        FecObtencion = 21
        Certificado = 23
        EntidadCertificadora = 24
        Alcance = 25
        NumCertificado = 26
        FecDespublicacion = 27
        FechaLimCumplim = 28
        'End Enum

        'Public Enum TipoCampoNoConformidad
        Titulo = 30
        Motivo = 31
        CausasYConclus = 32
        Acciones = 34
        Accion = 35
        Fec_inicio = 36
        Fec_cierre = 37
        Responsable = 38
        Observaciones = 39
        Documentación = 40

        EstadoNoConf = 42
        EstadoInternoNoConf = 43
        PiezasDefectuosas = 44
        ImporteRepercutido = 45
        Subtipo = 46
        FechaImputacionDefectos = 48


        Proveedor = 100
        FormaPago = 101
        Moneda = 102
        Material = 103
        CodArticulo = 104
        Unidad = 105
        Desglose = 106
        Pais = 107
        Provincia = 108
        Dest = 109
        PRES1 = 110
        Pres2 = 111
        Pres3 = 112
        Pres4 = 113
        Contacto = 114
        Persona = 115
        ProveContacto = 116
        Rol = 117
        DenArticulo = 118
        NuevoCodArticulo = 119
        NumSolicitERP = 120

        UnidadOrganizativa = 121
        Departamento = 122
        OrganizacionCompras = 123
        Centro = 124
        Almacen = 125
        ListadosPersonalizados = 126
        ImporteSolicitudesVinculadas = 127
        RefSolicitud = 128

        CentroCoste = 129
        Partida = 130
        Activo = 131
        TipoPedido = 132
        DescrBreveContrato = 133
        DescrDetalladaContrato = 134
        ArchivoContrato = 135
        DesgloseActividad = 136
        DesgloseFactura = 137
        Factura = 138
        InicioAbono = 139
        FinAbono = 140
        RetencionEnGarantia = 141
        UnidadPedido = 142
        ProveedorERP = 143
        Comprador = 144
        RenovacionAutomatica = 147
        PeriodoRenovacion = 148
        Empresa = 149
        EstadoHomologacion = 150
		AnyoPartida = 151

		NumLinea = 3000
    End Enum
    Public Enum EstadosVisorContratos
        Guardado = 0
        En_Curso_De_Aprobacion = 2
        Proximo_a_Expirar = 4
        Vigentes = 3
        Expirados = 5
        Rechazados = 6
        Anulados = 7
        Todas = 8
    End Enum
    <Serializable()>
    Public Enum TipoDeDatoXML
        Entero = 1
        Texto = 2
        Booleano = 3
        Real = 4
        Fecha = 5
    End Enum
    <Serializable()>
    Public Structure ParametrosOrigenDeDatos
        Dim Nombre As String
        Dim TipoDeDatos As TipoDeDatoXML
        Dim ValorDato As String
    End Structure
    Public Enum OrigenRegistroRecepcionesPedido As Short
        Ninguno = -1
        Integracion = 0
        RegistroActividad = 1
        IntegracionyRegistroActividad = 2
    End Enum
    ' Indica el origen de un movimiento en el LOG
    Public Enum OrigenIntegracion
        FSGSInt = 0
        FSGSReg = 1
        FSGSIntReg = 2
        erp = 3
    End Enum
    Public Structure AccionIntegracion
        Shared ReadOnly Alta As Char = "A"
        Shared ReadOnly Modificacion As Char = "U"
        Shared ReadOnly Baja As Char = "D"
        Shared ReadOnly Reubicacion As Char = "4"
    End Structure
    Public Structure ParametrosGenerales
        Dim gbSolicitudesCompras As Boolean
        Dim gsAccesoFSPM As TipoAccesoFSPM
        Dim gsAccesoFSQA As TipoAccesoFSQA
        Dim gbAccesoFSEP As Boolean
        Dim gbAccesoFSSM As Boolean
        Dim gbAccesoFSGS As Boolean
        Dim gbAccesoFSCN As Boolean
        Dim gbAccesoFSBI As Boolean
        Dim giAccesoFSBITipo As TipoAccesoFSBI
        Dim gbAccesoFSAL As Boolean
        Dim g_bAccesoFSFA As Boolean
        Dim g_bAccesoIS As Boolean
        Dim gbAccesoContratos As Boolean
        Dim gbAccesoQACertificados As Boolean
        Dim gbAccesoQANoConformidad As Boolean
        Dim gbOblCodPedido As Boolean
        Dim gbOblCodPedDir As Boolean
        Dim gbOblCodPedidoBloq As Boolean
        Dim gbOblCodPedidoObl As Boolean
        Dim gbOblPedidoFecEntrega As Boolean
        Dim gbCodRecepERP As Boolean
        Dim nomPedERP As Dictionary(Of FSNLibrary.Idioma, String)
        Dim nomPedDirERP As Dictionary(Of FSNLibrary.Idioma, String)
        Dim nomCampo1 As Dictionary(Of FSNLibrary.Idioma, String)
        Dim nomCampo2 As Dictionary(Of FSNLibrary.Idioma, String)
        Dim nomCampo3 As Dictionary(Of FSNLibrary.Idioma, String)
        Dim nomCampo4 As Dictionary(Of FSNLibrary.Idioma, String)
        Dim gbFSQA_Revisor As Boolean 'Figura del revisor
        Dim gbUsar_OrgCompras As Boolean
        Dim gbArticulosGenericos As Boolean ' edu 436
        Dim gbVarCal_Materiales As Boolean
        Dim gbUsarART4_UON As Boolean
        Dim gbOblAsignarPresArt As Boolean
        Dim gbPymes As Boolean
        Dim giPedEmail As ComunicacionPedido
        Dim giPedEmailModo As TipoComunicacionPedido
        Dim giPedPubPortal As ComunicacionPedidoPORTAL
        Dim UON0_Denominacion As Dictionary(Of FSNLibrary.Idioma, String)
        Dim gbUsarPedidosAbiertos As Boolean
        Dim gbMantEstIntEnTraspasoAdj As Boolean
        Dim gbBajaLOGPedidos As Boolean
        ' Parámetros EP
        Dim giLOGPREBLOQ As Short
        Dim gbTrasladoAProvERP As Boolean
        Dim gbActivLog As Boolean
        Dim giRecepAprov As OrigenRegistroRecepcionesPedido
        Dim gbControlFechaRecepcion As Boolean
        Dim gbControlCantidadRecepcion As Boolean
        Dim gbVerNumeroProveedor As Boolean
        Dim gbPedidoLibre As Boolean
        Dim gbEPDesactivarCargaArticulos As Boolean
        Dim gbEPActivarCargaSolPM As Boolean
        Dim gbEPRecepcionAutomatica As Integer
        Dim gbEPMarcarAcepProve As Boolean
        Dim gbMostrarRecepSalidaAlmacen As Boolean
        Dim gbEPNotifProveAdjuntarPedidoPDF As Boolean
        Dim gbMostrarFechaContable As Boolean
        Dim gdDesvioRecepcionDefecto As Double
        Dim gbPedidosHitosFacturacion As Boolean
        ''' <summary>
        ''' Obligatoriedad de la dirección de envío de la factura
        ''' </summary>
        Dim gbDirEnvFacObl As Boolean
        Dim gbArticulosNegociadosPedExpress As Boolean
        Dim giRecepDirecto As OrigenRegistroRecepcionesPedido
        'Parametros de seguridad de acceso
        Dim giWinSecurityWeb As TiposDeAutenticacion
        Dim giWinSecurity As TiposDeAutenticacion
        'Parametros de seguridad de contraseÃƒÂ±as
        Dim giMIN_SIZE_PWD As Integer
        Dim gbCOMPLEJIDAD_PWD As Boolean
        Dim giHISTORICO_PWD As Integer
        Dim giEDAD_MIN_PWD As Integer
        Dim giEDAD_MAX_PWD As Integer
        'Campos para proveedores
        Dim gbCampo1 As Boolean
        Dim gbCampo2 As Boolean
        Dim gbCampo3 As Boolean
        Dim gbCampo4 As Boolean
        Dim gWinSecAdminUser As String
        Dim gWinSecAdminPwd As String
        Dim giProce_Prove_Grupos As Integer
        Dim gsMonedaCentral As String
        Dim gbAccesoLCX As Boolean
        Dim gbCostesPorLinea As Boolean
        Dim gbFechaRecepBloqueada As Boolean
        'Mant Perfiles
        Dim gbSubasta As Boolean
        Dim gbPedidosAprov As Boolean
        Dim gbPedidosDirectos As Boolean
        Dim gbPedidosERP As Boolean
        Dim gEPAvisoEmitirPedido As String
        Dim gbUsarPres1 As Boolean
        Dim gbUsarPres2 As Boolean
        Dim gbUsarPres3 As Boolean
        Dim gbUsarPres4 As Boolean
        Dim goPres0Denominacion As Dictionary(Of FSNLibrary.Idioma, String)
        Dim gbPres1Denominacion As Dictionary(Of FSNLibrary.Idioma, String)
        Dim gbPres2Denominacion As Dictionary(Of FSNLibrary.Idioma, String)
        Dim gbPres3Denominacion As Dictionary(Of FSNLibrary.Idioma, String)
        Dim gbPres4Denominacion As Dictionary(Of FSNLibrary.Idioma, String)
        Dim gbUsarPedPres1 As Boolean
        Dim gbUsarPedPres2 As Boolean
        Dim gbUsarPedPres3 As Boolean
        Dim gbUsarPedPres4 As Boolean
        Dim gbPres1Obligatorio As Boolean
        Dim gbPres2Obligatorio As Boolean
        Dim gbPres3Obligatorio As Boolean
        Dim gbPres4Obligatorio As Boolean
        Dim giPres1Nivel As Integer
        Dim giPres2Nivel As Integer
        Dim giPres3Nivel As Integer
        Dim giPres4Nivel As Integer
        'Parámetros QA
        Dim gbPotAValProvisionalSelProve As Boolean
        Dim gbProveERPVisibleNC As Boolean
        'Codigos de material GMN1,GMN2,GMN3,GMN4
        Dim gbGMN1Cod As String
        Dim gbGMN2Cod As String
        Dim gbGMN3Cod As String
        Dim gbGMN4Cod As String
        'Nombre empresa y codigos UON1,UON2,UON3
        Dim gbEmpresa As String
        Dim gbUON1Cod As String
        Dim gbUON2Cod As String
        Dim gbUON3Cod As String
        '6110- Ver todos los gmn o solo el cuarto nivel mas denominaciÃ³n material
        Dim gbMaterialVerTodosNiveles As Boolean
        Dim gbCMVerTriangulo As Boolean
    End Structure
    'Mant Perfiles
    Public Structure ParametrosIntegracion
        Dim gbSoloImportar_Mon As Boolean
        Dim gbSoloImportar_Pag As Boolean
        Dim gbSoloImportar_ViaPag As Boolean
        Dim gbSoloImportar_Pais As Boolean
        Dim gbSoloImportar_Provi As Boolean
        Dim gbSoloImportar_Uni As Boolean
        Dim gbSoloImportar_Dest As Boolean
        Dim gbSoloImportar_Art4 As Boolean
        Dim gbSoloImportar_Prove As Boolean
        Dim gbSoloImportar_PedDirecto As Boolean
        Dim gbExportar_Adj As Boolean
    End Structure
    Public Structure LongitudesDeCodigos
        Dim giLongCodART As Integer
        Dim giLongCodCAL As Integer
        Dim giLongCodCOM As Integer
        Dim giLongCodDEP As Integer
        Dim giLongCodDEST As Integer
        Dim giLongCodEQP As Integer
        Dim giLongCodGMN1 As Integer
        Dim giLongCodGMN2 As Integer
        Dim giLongCodGMN3 As Integer
        Dim giLongCodGMN4 As Integer
        Dim giLongCodMON As Integer
        Dim giLongCodOFEEST As Integer
        Dim giLongCodPAG As Integer
        Dim giLongCodPAI As Integer
        Dim giLongCodPER As Integer
        Dim giLongCodPERF As Integer
        Dim giLongCodPRESCON1 As Integer
        Dim giLongCodPRESCON2 As Integer
        Dim giLongCodPRESCON3 As Integer
        Dim giLongCodPRESCON4 As Integer
        Dim giLongCodPRESCONCEP31 As Integer
        Dim giLongCodPRESCONCEP32 As Integer
        Dim giLongCodPRESCONCEP33 As Integer
        Dim giLongCodPRESCONCEP34 As Integer
        Dim giLongCodPRESCONCEP41 As Integer
        Dim giLongCodPRESCONCEP42 As Integer
        Dim giLongCodPRESCONCEP43 As Integer
        Dim giLongCodPRESCONCEP44 As Integer
        Dim giLongCodPRESPROY1 As Integer
        Dim giLongCodPRESPROY2 As Integer
        Dim giLongCodPRESPROY3 As Integer
        Dim giLongCodPRESPROY4 As Integer
        Dim giLongCodPROVE As Integer
        Dim giLongCodPROVI As Integer
        Dim giLongCodROL As Integer
        Dim giLongCodUNI As Integer
        Dim giLongCodUON1 As Integer
        Dim giLongCodUON2 As Integer
        Dim giLongCodUON3 As Integer
        Dim giLongCodUON4 As Integer
        Dim giLongCodUSU As Integer
        Dim giLongCodACT1 As Integer
        Dim giLongCodACT2 As Integer
        Dim giLongCodACT3 As Integer
        Dim giLongCodACT4 As Integer
        Dim giLongCodACT5 As Integer
        Dim giLongCia As Integer
        Dim giLongCodCAT1 As Integer
        Dim giLongCodCAT2 As Integer
        Dim giLongCodCAT3 As Integer
        Dim giLongCodCAT4 As Integer
        Dim giLongCodCAT5 As Integer
        Dim giLongCodGRUPOPROCE As Integer
        Dim giLongCodDENART As Integer
        Dim giLongCodACTIVO As Integer
        Dim giLongCodCENTROCOSTE As Integer
        Dim giLongCodFSGA_TARE_COD As Short
        Dim giLongCodFSGA_TARE_DEN As Short
        Dim giLongCodFSGA_CAT_TARE_COD As Short
        Dim giLongCodFSGA_CAT_TARE_DEN As Short
        Dim giLongCodFSGA_TARE_OBS As Short
        Dim giLongPRES5_NIV0Cod As Integer
        Dim giLongPRES5_NIV1Cod As Integer
        Dim giLongPRES5_NIV2Cod As Integer
        Dim giLongPRES5_NIV3Cod As Integer
        Dim giLongPRES5_NIV4Cod As Integer
    End Structure
    Public Enum TipoFecha
        FechaAlta = 1
        UnDiaDespues = 2
        DosDiasDespues = 3
        TresDiasDespues = 4
        CuatroDiasDespues = 5
        CincoDiasDespues = 6
        SeisDiasDespues = 7
        UnaSemanaDespues = 8
        DosSemanaDespues = 9
        TresSemanaDespues = 10
        UnMesDespues = 11
        DosMesDespues = 12
        TresMesDespues = 13
        CuatroMesDespues = 14
        CincoMesDespues = 15
        SeisMesDespues = 16
        UnAnyoDespues = 17
    End Enum
    Public Enum TipoAccesoFSPM
        AccesoFSPMSolicCompra = 1
        AccesoFSPMCompleto = 2
        SinAcceso = 3
    End Enum
    Public Enum TipoAccesoFSQA
        AccesoFSQABasico = 1
        SinAcceso = 2
        Modulos = 3
    End Enum
    Public Enum TipoAccesoFSBI
        Completo = 1
        SoloCubosBI = 2
        SoloQlikApps = 3
        SinAcceso = 4
    End Enum
    Public Enum IdsFicticios
        EstadoActual = 1000
        Comentario = 1020
        EstadoInterno = 1010
        Grupo = 900
        DesgloseVinculado = 2000
        NumLinea = 3000
    End Enum
    Public Enum TipoAtributo
        CuentaContable = 886
    End Enum
    Public Enum LiteralesParametros
        PedidoERP = 31
        RecepcionERP = 47
    End Enum
    Public Enum TiposPedido
        GS = 0
        EP = 1
        ERP = 2
    End Enum
    Public Enum ConceptoImpuesto
        Gasto = 0
        Inversion = 1
        Ambos = 2
    End Enum
    Public Enum ConceptoFactura
        Gasto = 0
        Inversion = 1
    End Enum
    Public Structure Coste
        Public Enum Planificacion
            NoPlanificado = 0 '
            Planificado = 1
        End Enum
    End Structure
    Public Structure Descuento
        Public Enum Planificacion
            NoPlanificado = 0 '
            Planificado = 1
        End Enum
    End Structure
    Public Enum TiposErrores
        TENotificacion = 0
    End Enum
    Public Enum ControlesBusquedaJQuery
        CentroCoste = 0
        PartidaPres = 1
        Peticionario = 2
    End Enum
    Public Structure Adjunto
        ''' <summary>
        ''' Tipo de elemento al que estÃ¡ vinculado el adjunto: Orden, LÃ­nea de una orden, Favorito, lÃ­nea de un favorito, cabecera de una cesta, lÃ­nea de una cabecera de una cesta
        ''' </summary>
        Public Enum Tipo As Short
            Solicitud = 1
            Orden_Entrega = 2
            Linea_Pedido = 3
            Favorito_Orden_Entrega = 4
            Favorito_Linea_Pedido = 5
            Categoria1 = 6
            Categoria2 = 7
            Categoria3 = 8
            Categoria4 = 9
            Categoria5 = 10
            EspecificacionLineaCatalogo = 11
            Cesta_Cabecera = 12
            Cesta = 13 'LÃ­neas de la cesta
            RecepcionPedido = 14
            Log_Orden_Entrega = 15
            Log_Linea_Pedido = 16
            EspecificacionProceso = 17
            EspecificacionProcesoGrupo = 18
            EspecificacionItem = 19
            Articulo = 20
            ProveedorArticulo = 21
            Proveedor = 21
            PlantillaProceso = 22
            Proceso = 23
            RegistroEmail = 24
        End Enum
    End Structure
    Public Sub New()

    End Sub
End Class
Public Enum TipoPonderacionCert
    SinPonderacion = 0
    EscalaContinua = 1
    EscalaDiscreta = 2
    Formula = 3
    Manual = 4
    SiNo = 5
    PorCaducidad = 6
    Automatico = 7
End Enum
Public Enum TipoPonderacionVariables
    PondNCNumero = 0
    PondNCMediaPesos = 1
    PondNCSumaPesos = 2
    PondCertificado = 3
    PondIntEscContinua = 4
    PondIntFormula = 5
    PondManual = 6
    PondPPM = 7
    PondCargoProveedores = 8
    PondTasaServicios = 9
    PondEncuesta = 10
    PondNoPonderacion = 99
End Enum
Public Enum TipoEstadoSolic
    Guardada = 0
    Enviada = 1
    EnCurso = 2
    EnCursoFlujoModificacion = 3
    EnCursoFlujoBaja = 4
    Rechazada = 6
    Aprobada = 7
    Anulada = 8

    SCPendiente = 100
    SCAprobada = 101
    SCRechazada = 102
    SCAnulada = 103
    SCCerrada = 104

    CertificadoPub = 200
    NoConformidadEnviada = 300
    NoConformidadGuardada = 301
    NoConformidadAnulada = 302

    Pendiente = 1000
End Enum
Public Enum TipoAccionSolicitud
    Alta = 1
    Modificacion = 2
    Traslado = 3
    Devolucion = 4
    Aprobacion = 5
    Rechazo = 6
    Anulacion = 7
    Reemision = 8
    RechazoEficazRevisor = 9 'Para Controlar  el rechazo del revisor
    RechazoEficazRevisorAnterior = -9
    RechazoNoEficazRevisor = 10
    RechazoNoEficazRevisorAnterior = -10
    CierreNoConformidad = 11
    CierreAnteriorNoConformidad = -11
    AprobacionNoConformidad = 12 'Cuando aprueba el revisor
End Enum
Public Enum TipoEstadoOrdenEntrega
    PendienteDeAprobacion = 0
    DenegadoParcialAprob = 1
    EmitidoAlProveedor = 2
    AceptadoPorProveedor = 3
    EnCamino = 4
    EnRecepcion = 5
    RecibidoYCerrado = 6
    Anulado = 20
    RechazadoPorProveedor = 21
    DenegadoTotalAprobador = 22
End Enum
Public Enum TipoEstadoPedidoAbierto
    NoVigente = 100
    Abierto = 101
    Cerrado = 102
    Anulado = 103
End Enum
Public Enum TipoEstadoProceso
    sinitems = 1        'Sin validar y sin items
    ConItemsSinValidar = 2      'Con items y sin validar
    validado = 3        ' Validado y sin provedores asignados
    Conproveasignados = 4  ' Validado y con proveedores asignados pero sin validar la asignacion
    conasignacionvalida = 5 ' Con asignacion de proveedores validada
    conpeticiones = 6
    conofertas = 7
    ConObjetivosSinNotificar = 8
    ConObjetivosSinNotificarYPreadjudicado = 9
    PreadjYConObjNotificados = 10
    ParcialmenteCerrado = 11
    conadjudicaciones = 12
    ConAdjudicacionesNotificadas = 13
    Cerrado = 20
End Enum
Public Enum TipoCampoPredefinido
    Normal = 0
    CampoGS = 1  ' Solicitud de compras
    Atributo = 2   ' Atributo
    Calculado = 3
    Certificado = 4
    NoConformidad = 5
End Enum
Public Enum TipoAccesoFSPM
    AccesoFSPMSolicCompra = 1
    AccesoFSPMCompleto = 2
    SinAcceso = 3
End Enum
Public Enum TipoAccesoFSQA
    AccesoFSQABasico = 1
    SinAcceso = 2
    Modulos = 3
End Enum
Public Enum TipoAccesoFSEP
    Basico = 0
    SoloAprovisionador = 1
    SoloAprobador = 2
    AprobadorYAprovisionador = 3
End Enum
Public Enum TipoAmbitoProceso
    AmbProceso = 1
    AmbGrupo = 2
    AmbItem = 3
End Enum
Public Enum TipoEstadoNoConformidad
    Guardada = 0
    Abierta = 1
    CierrePosEnPlazo = 2
    CierrePosFueraPlazo = 3
    CierreNegativo = 4
    CierrePosSinRevisar = 5
    CierreNegSinRevisar = 6
    Anulada = 7
    CierrePosPendiente = 8  'Pendiente validar cierre eficaz
    EnCursoDeAprobacion = 9
End Enum
Public Enum TipoEstadoAcciones
    SinRevisar = 1
    Aprobada = 2
    Rechazada = 3
    FinalizadaSinRevisar = 4
    FinalizadaRevisada = 5
End Enum
Public Enum IdsFicticios
    EstadoActual = 1000
    Comentario = 1020
    EstadoInterno = 1010
    Grupo = 900
End Enum
Public Enum TipoRechazoAccion
    RechazoTemporal = 1
    RechazoDefinitivo = 2
    Anulacion = 3
End Enum
Public Enum TipoBloqueoEtapa
    SinBloqueo = 0
    BloqueoInicio = 1
    BloqueoSalida = 2
End Enum
Public Enum ErroresEMail
    Send = 0
    Adjuntos = 1
    SinError = 2
    CreacionObjeto = 3 'Casque en creacion objeto o llamada
End Enum
Public Enum MapperModuloMensaje
    PedidoDirecto = 1
    Recepciones = 7
End Enum
Public Enum SentidoIntegracion
    SinSentido = 0
    Salida = 1
    Entrada = 2
    EntradaYSalida = 3
End Enum
Public Enum TipoDestinoIntegracion
    NTFS = 1
    FTP = 2
    WEBSERVICE = 3
    WCF = 4
End Enum
Public Enum TablasIntegracion
    Mon = 1
    Pai = 2
    Provi = 3
    Pag = 4
    Dest = 5
    Uni = 6
    art4 = 7
    Prove = 8
    Adj = 9
    Recibo = 10
    con = 101
    prove_ERP = 102
    PED_Aprov = 11

    Rec_Aprov = 12
    PED_directo = 13
    Rec_Directo = 14
    Materiales = 15
    PresArt = 16
    VARCAL = 17
    PM = 18

    Presupuestos1 = 19
    Presupuestos2 = 20
    Presupuestos3 = 21
    Presupuestos4 = 22
    Proceso = 23
    TablasExternas = 24
    ViaPag = 25
    PPM = 26
    TasasServicio = 27
    CargosProveedor = 28
    UnidadesOrganizativas = 29
    Usuarios = 30
    Activos = 31
    Facturas = 32
    Pagos = 33
    PRES5 = 34
    CentroCoste = 35
    Empresa = 36
    AsignacionProv = 37
    RecepcionOFE = 38

    ProveArticulo = 39
    ProvePortal = 40
    Certificado = 41
End Enum
Public Enum TipoAvisoBloqueo
    Bloqueo = 0
    Aviso = 1
    SinAsignar = -1
End Enum
Public Enum EstadoValidacion
    SinEstado = 0
    EnCola = 1
    ProximosParticipantes = 2
    CamposObligatorios = 3
    Precondiciones = 4
    ControlImportes = 5
    Integracion = 6
    SiguienteEtapa = 7
    YaAsignadaAOtroUsuario = 8
    ErrorGeneral = 9
    Completada = 99
End Enum
Public Enum CalidadSubtipo
    CalIntegracion = 1
    CalManual = 2
    CalCertificado = 3
    CalNoConformidad = 4
    CalPPM = 5
    CalCargoProveedores = 6
    CalTasaServicio = 7
    CalEncuesta = 8
End Enum
Public Enum TiposDeAutenticacion
    Fullstep = 0
    UsuariosLocales = 1
    Windows = 2
    LDAP = 3
    ServidorExterno = 4
    ADFS = 5
End Enum
<Serializable()>
Public Structure Idioma
    Private _Idioma As String
    Private _refCultural As String
    ''' <summary>
    ''' Constructor de la clase Idioma
    ''' </summary>
    ''' <param name="Idioma">Código de idioma</param>
    ''' <remarks>Comprueba que el código de idioma sea válido antes de asignarlo</remarks>
    Public Sub New(ByVal Idioma As String)
        _Idioma = ComprobarIdioma(Idioma)
        If String.IsNullOrEmpty(_Idioma) Then _
            Throw New Exception("Código de idioma no válido: " & Idioma)
    End Sub
    ''' <summary>
    ''' Función de conversión de tipos del tipo Idioma a tipo String
    ''' </summary>
    ''' <param name="value">Elemento Idioma a convertir</param>
    ''' <returns>Un elemento String con el valor del código de idioma</returns>
    Public Shared Widening Operator CType(ByVal value As Idioma) As String
        Return value._Idioma
    End Operator
    ''' <summary>
    ''' Función de conversión de tipos del tipo String a tipo Idioma
    ''' </summary>
    ''' <param name="value">Elemento String a convertir</param>
    ''' <returns>Un elemento Idioma con el código de idioma especificado si es válido</returns>
    Public Shared Narrowing Operator CType(ByVal value As String) As Idioma
        Return New Idioma(value)
    End Operator
    ''' <summary>
    ''' Sobreescribe la función ToString para devolver el código de idioma
    ''' </summary>
    ''' <returns>El código de idioma</returns>
    Public Overrides Function ToString() As String
        Return _Idioma
    End Function
    ''' <summary>
    ''' Obtiene la referencia cultural del idioma
    ''' </summary>
    ''' <returns>Una cadena con la referencia cultural del idioma</returns>
    Public Property RefCultural() As String
        Get
            Return Me._refCultural
        End Get
        Set(ByVal value As String)
            Me._refCultural = value
        End Set
    End Property
End Structure
Public Enum TipoEmail
    Texto = 0
    HTML = 1
End Enum
Public Enum TipoAccesoUNQAS
    EmitirNoConformidades = 1
    ConsultarNoConformidades = 2
    ReabrirNoConformidades = 3
    MantenerObjetivosSuelos = 4
    ConsultarObjetivosSuelos = 5
    AsignarProveedoresUNQA = 6
    ModificarPuntuacionesPanelCalidad = 7
    ConsultarPuntuacionesPanelCalidad = 8
    ModificarNoConformidadesOtroUsuario = 9
    RecibirAvisoExpirCertProvesUNQA = 10
    RecibirAvisosEscProvesNivel1 = 11
    ConsultarEscProvesNivel1 = 12
    AprobarRechazarEscProvesNivel1 = 13
    RecibirAvisosEscProvesNivel2 = 14
    ConsultarEscProvesNivel2 = 15
    AprobarRechazarEscProvesNivel2 = 16
    RecibirAvisosEscProvesNivel3 = 17
    ConsultarEscProvesNivel3 = 18
    AprobarRechazarEscProvesNivel3 = 19
    RecibirAvisosEscProvesNivel4 = 20
    ConsultarEscProvesNivel4 = 21
    AprobarRechazarEscProvesNivel4 = 22
End Enum
'*********** REGISTRO DE NOTIFICACIONES ***********************
Public Enum Producto
    GS = 1
    PM = 2
    QA = 3
    EP = 4
    OTRO = 5
    USADO_POR_FSGSNOTIFIER = 6
    INT = 7
    CM = 8
    IM = 9
    SM = 10
End Enum
Public Enum EntidadNotificacion
    'Las denominaciones en diferentes idiomas se encuentran en el módulo 185 de WEBFSWTEXT, coincidiendo el id de cada
    'registro con el enumerador. Si se cambia algun nombre o número hay que actualizar dicho módulo.
    NoDefinido = 0
    Calificaciones = 1
    Certificado = 2
    NoConformidad = 3
    Colaboracion = 4
    Orden = 5 'EP
    Recepcion = 6 'EP
    Solicitud = 7 'PM o GS
    Contrato = 8 'CM
    AutoFactura = 9 'IM
    ProcesoCompra = 10 'GS
    PedDirecto = 11 'GS
    ControlPresupuestario = 12 'SM
    EscalacionProveedores = 13 'QA
    Encuesta = 14 'QA
    Factura = 15 'IM
    SolicitudQA = 16 'QA
    '**********************************************
    'Las denominaciones en diferentes idiomas se encuentran en el módulo 185 de WEBFSWTEXT, coincidiendo el id de cada
    'registro con el enumerador. Si se cambia algun nombre o número hay que actualizar dicho módulo.
End Enum
Public Enum TipoNotificacion
    NoDefinido = 0

    'Notificaciones GS
    'solicitudes
    AsignacionSolicitudCompra = 1
    AprobacionSolicitudPet = 2
    RechazoSolicitudPet = 3
    AnulacionSolicitudPet = 4
    CierreSolicitudPet = 5
    ReaperturaSolicitudPet = 6

    'Procesos
    PetOferta = 50
    PetOfertaWeb = 51
    PetOfertaSubasta = 52
    PetOfertaWebSubasta = 53
    SubastaWPF = 54
    ComObjetivos = 55
    ComObjetivosWeb = 56
    AdjudicacionYOrdenCompra = 57
    ProveNoAdjudicado = 58
    ProximaDespublicacion = 59
    AvisoFinSubasta = 60
    InicioSubasta = 61
    ProximidadInicioSubasta = 62
    ConvocatoriaReunion = 63

    AvisoInvitacionProceso = 64
    AvisoExclusionProceso = 65
    AvisoPreadjProceso = 66
    AvisoCierreProceso = 67
    AvisoAnulacionProceso = 68

    'Ped directo
    PedEmision = 200
    NotifPedidoReceptor = 201
    PedAnulacionMail = 202
    PedRecepKo = 203
    PedRecepOk = 204
    PetDatosExtMail = 205
    PedAceptacion = 206
    PedNotificacionProve = 207

    'Tipo de notificaciones EP
    NotificadoresTipo1 = 1001
    NotificadoresTipo2 = 1002
    OrdenDenegadaParcialmente = 1003
    OrdenDenegadaTotalmente = 1004
    RecepcionIncorrectaProve = 1005
    RecepcionCorrectaProve = 1006
    EmisionAUsuario = 1007
    OrdenAProveedor = 1008
    AprobadoresTipo1 = 1009
    AprobadoresTipo2 = 1010
    RecepcionCorrectaAprov = 1011
    RecepcionIncorrectaAprov = 1012
    EmisionNotificados = 1013
    AnulacionDeOrdenAProveedor = 1014
    ErrorEnAprobacion = 1015
    ErrorEnRechazo = 1016
    OrdenAAprobador = 1017
    OrdenBorrar = 1018
    OrdenDeshacerBorrado = 1019
    LineaBorrar = 1020

    CertificadoEmitido = 2001
    CertificadoEnviado = 2002
    CertificadoRenovado = 2003
    BorradoPortal_Certif = 2004
    ExpiracionCertif = 2005
    ProxExpiracionCertif = 2006
    EnvioCertificado = 2007
    CertificadosRevisados = 2008

    NoConformidadAnuladaProveedor = 2051
    NoConformidadAnuladaContactos = 2052
    NoConformidadAnuladaRevisor = 2053
    NoConformidadEmitida = 2054
    NoConformidadEmitidaContactos = 2055
    NoConformidadEmitidaRevisor = 2056
    ModifEstadosAccionesNoConf = 2057
    ModifEstadosAccionesNoConfContactos = 2058
    CierrePositivo = 2059
    CierrePositivoContactos = 2060
    CierreNegativo = 2061
    CierreNegativoContactos = 2062
    ReaperturaNoConf = 2063
    ReaperturaNoConfContactos = 2064
    AprobacionCierrePosRevisor = 2065
    AprobacionCierreNegRevisor = 2066
    RechazoCierrePosRevisor = 2067
    RechazoCierreNegRevisor = 2068
    CierrePositivoRevisor = 2069
    CierreNegativoRevisor = 2070
    BorradoPortal_NC = 2071
    NoConformidadFechasFin = 2072
    EnvioNoConformidad = 2073

    CambioCalificaciónTotalProveedor = 2100
    ErroresCalculoPuntuaciones = 2101
    AvisoPasoProveQAaReal = 2012
    InicioTareaFSGA = 2150

    NoConformidadAnulada_Generico = 2900
    NoConformidadEmitida_Generico = 2901
    ModifEstadosAccionesNoConf_Generico = 2902
    CierrePositivo_Generico = 2903
    CierreNegativo_Generico = 2904
    ReaperturaNoConf_Generico = 2905

    'Tipo Notificaciones PM (contratos y facturas)
    Devolucion = 3001
    Traslado = 3002
    TrasladoAProveedor = 3003
    DevolucionPortal = 3004
    RealizarAccion = 3005
    CambioEtapa = 3006
    RealizarAccionProveedor = 3007
    CambioEtapaProveedor = 3008
    RealizarAccionPortal = 3009
    CambioEtapaPortal = 3010
    ContratoProximoExpirar = 3011
    ErrorValidacion = 3012
    AvisoExpiracion = 3013

    'Tipo Notificaciones SM
    SMDisponibleNegativo = 4001

    EmailNoAutomatico = 3000

    NotificacionColaboracion = 6000

    AvisoEscalacionPendienteAprobar = 7000
    AvisoEscalacionProveedor = 7001
    AvisoEscalacionPendienteValidarCierreEficaz = 7002
    AvisoEscalacionSinCerrarXMeses = 7003
End Enum
'************************************************************************************
Public Module TextosSeparadores
    Public espacioGuionEspacio As String = " - "
    Public dosArrobas As String = "@@"
    ''' <summary>
    ''' Método Para crear instancias de la clase. No queremso hacer nada más que instanciar por lo que va vacío
    ''' </summary>
    ''' <remarks>Llamada desde allí donde se instancie esta clase</remarks>
    Sub New()
    End Sub
End Module
Public Enum EstadosNoConformidad
    Guardada = 0
    Abierta = 1
    CierrePosIN = 2
    CierrePosOUT = 3
    CierreNoEficaz = 4
    CierrePdteRevisar = 5
    Anulada = 7
    Todas = 6
    EnCursoDeAprobacion = 8
End Enum
Public Enum ConfiguracionFiltrosQA_NC
    Identificador = 1
    Tipo = 2
    Titulo = 3
    CodigoProveedor = 4
    DenominacionProveedor = 5
    UnidadNegocio = 6
    Peticionario = 7
    Revisor = 8
    FechaAlta = 9
    FechaAnulacion = 10
    FechaLimiteResolucion = 11
    FechaEmision = 12
    FechaRespuestaProveedor = 13
    FechaActualizacion = 14
    FechaCierre = 15
    Comentarios = 16
    ComentarioApertura = 17
    ComentarioCierre = 18
    ComentarioRevisor = 19
    ComentarioAnulacion = 20
    EstadosAcciones = 21
    ProveedorERP = 22
End Enum
Public Enum ConfiguracionFiltrosQA_Cert
    Identificador = 1
    CodigoProveedor = 2
    DenominacionProveedor = 3
    Real = 4
    Baja = 5
    MatQA = 6
    Tipo = 7
    Estado = 8
    FechaSolicitud = 9
    FechaLimiteCumpl = 10
    FechaRespuestaProveedor = 11
    Publicado = 12
    FechaPublicacion = 13
    FechaDesPublicacion = 14
    FechaActualizacion = 15
    CodigoPeticionario = 16
    Modo = 17
    FechaExpiracion = 18
    Obligatorio = 19
    FechaCumplimentacion = 20
    Validado = 21
    Provisional = 22
End Enum
Public Enum TipoEstadosNoConformidadesAbiertas
    PendienteEmitir = 1
    SinRespuestaParteProveedor = 2
    AccionesPendientesRevisarPeticionario = 3
    AccionesPendientesFinalizarProveedor = 4
    AccionesPendientesFinalizarPeticionario = 5
    PendientesCerrar = 6
    PendientesRevisarCierre = 7
End Enum
Public Enum ConfiguracionFiltrosContratos
    CodigoContrato = 1
    Tipo = 2
    CodigoProveedor = 3
    DenominacionProveedor = 4
    Contacto = 5
    Peticionario = 6
    FechaAlta = 7
    Moneda = 8
    FechaInicio = 9
    FechaExpiracion = 10
    Estado = 11
    ProcesoCompra = 12
    Importe = 13
    SituacionActual = 14
    AlertaExpiracion = 15
    Notificados = 16
    FechaUltNotificacion = 17
End Enum
Public Enum ConfiguracionFiltrosFacturas
    NumeroFactura = 1
    FechaFactura = 2
    Empresa = 3
    FechaContabilizacion = 4
    CodigoProveedor = 5
    NumeroFacturaSAP = 6
    DenominacionProveedor = 7
    SituacionActual = 8
    Importe = 9
    Pago = 10
    Estado = 11
    Monitorizacion = 12
    TipoFactura = 13
End Enum
Public Enum TipoVigencia
    NoAplica = 0
    Avisar = 1
    Bloquear = 2
End Enum
Public Enum EnumModoPresupuestacion
    IndependienteEstructuraOrganizativa = 0
    DependienteEstructuraOrganizativa = 1
End Enum
Public Enum EnumModoImputacion
    NivelCabecera = 0
    NivelLinea = 1
End Enum
Public Enum EnumTipoImputacion
    NoSeImputa = 0
    Opcional = 1
    Obligatorio = 2
End Enum
Public Enum EnumTipoAcumulacion
    SolicitudCompra = 0
    EmisionPedido = 1
    RecepcionPedido = 2
End Enum
Public Enum EnumTipoControlDisponible
    NoImputa = 0
    Aviso = 1
    Bloqueo = 2
End Enum
Public Enum TiposWorkFlow
    Alta = 0
    Modificacion = 1
    Baja = 2
End Enum
Public Enum PeriodosAlertaContratos
    Dias = 1
    Semanas = 2
    Meses = 3
End Enum
'******************** Organización de red (RESTRICCIONES) *****************
Public Enum OrganizacionRed
    RestrUsu = 1
    RestrDepUsu = 2
    RestrUONUsu = 3
    NoRestr = 4
End Enum
'******************** Permiso de publicaciÃ³n *****************
Public Enum PermisoPublicacion
    Privado = 0
    Publico = 1
    SinPermiso = 2
End Enum
Public Enum TipoCostesDecuentos
    Costes = 0
    Descuentos = 1
End Enum
Public Enum NivelCostesDescuentos
    Cabecera = 1
    Lineas = 2
End Enum
Public Enum TipoFicheroFileStream
    Adjunto = 1
    EFactura = 2
    Especificacion = 3
End Enum
Public Enum TipoAccesoModulos
    FSSG = 0
    FSGS = 1
    FSPM = 2
    FSEP = 3
    FSCM = 4
    FSSM = 5
    FSIM = 6 'Facturación
    FSQA = 7
    FSIS = 8 'Integración
    FSCN = 9
    FSBI = 10
    FSAL = 11
    FSSOL = 12
End Enum
Public Enum AccionesDeSeguridad
    'ACCIONES GS
    '===========
    GSEstProceRestComp = 17001
    GSEstProceRestEqp = 17002
    GSEstProceRestCompResp = 17003
    GSEstProceRestMatComp = 17004
    GSEstProceRestUsuAper = 17017
    GSEstProceRestUsuUON = 17018
    GSEstProceRestUsuDep = 17019
    GSEstProceRestUsuPerfUON = 17020
    GSPermitirVisorNotificaciones = 17021

    'ACCIONES FSPM 
    '====================
    PMRestringirConceptosPresupuestariosUOUsuario = 22000
    PMPermitirTraslados = 22001
    PMRestringirPersonasUOUsuario = 22002
    PMRestringirPersonasDepartamentoUsuario = 22003
    PMRestringirProveedoresMaterialUsuario = 22004
    PMRestringirMaterialAsignadoUsuario = 22005
    PMRestringirDestinosUOUsuario = 22006
    PMRestringirSolicitudesReferenciadasAbiertasUOUsuario = 22007
    PMPermitirVerPreciosUltimasAdjudicacionesArticulos = 22008
    PMPermitirVisorNotificaciones = 22009
    PMRestringirUONSolicitudAUONUsuario = 22010
    PMRestringirUONSolicitudAUONPerfil = 22011
    PMPermitirCrearEscenarios = 22012
    PMPermitirCrearEscenariosOtrosUsuarios = 22013
    PMPermitirEditarEscenariosCompartidosNoCreadosPorUsuario = 22014
    PMOcultarEscenarioDefecto = 22015
    PMRestringirCrearEscenariosUsuariosUONUsu = 22016
    PMRestringirCrearEscenariosUsuariosUONsPerfilUsu = 22017
    PMRestringirCrearEscenariosUsuariosDEPUsu = 22018
    PMRestringirEdicionEscenariosUONUsu = 22019
    PMRestringirEdicionEscenariosUONsPerfilUsu = 22020
    PMRestringirEdicionEscenariosDEPUsu = 22021
    PMRestringirEmpresaUSU = 22022
    PMRestringirEmpresasPerfilUSU = 22023
    PMPermitirCrearEscenariosProveedores = 22024

    'ACCIONES FSEP
    '====================
    EPPermitirRealizarPedidosLibre = 23000
    EPRestringirPedidosEmpresaUsuario = 23001
    EPPermitirModificarPreciosEmisionPedido = 23002
    EPPermitirAnularRecepciones = 23003
    EPPermitirCerrarPedido = 23004
    EPPermitirReabrirPedido = 23005
    EPPermitirVerPedidosCentrosCosteUsuario = 23006
    EPPermitirRecepcionarPedidosCentrosCosteUsuario = 23007
    EPPermitirActivarBloqueoAlbaranesFacturacion = 23008
    EPPermitirVisorNotificaciones = 23009
    EPRestringirPedidosArticulosUONUsuario = 23010  'FSN_DES_EP_PRT_2809
    EPRestringirPedidosArticulosUONPerfil = 23011   'FSN_DES_EP_PRT_2809
    EPPermitirRealizarPedidosCatalogo = 23012
    EPPermitirEmitirPedidosDesdePedidoAbierto = 23013
    EPRestringirEmisionPedidosAbiertosEmpresaUsu = 23014
    EPRestringirEmisionPedidosAbiertosEmpresasUONsPerfil = 23015
    EPPermitirSeleccionarPresupuestosConcepto1UONsPorEncimaUsu = 23016
    EPPermitirSeleccionarPresupuestosConcepto2UONsPorEncimaUsu = 23017
    EPPermitirSeleccionarPresupuestosConcepto3UONsPorEncimaUsu = 23018
    EPPermitirSeleccionarPresupuestosConcepto4UONsPorEncimaUsu = 23019
    EPPermitirSeleccionarPresupuestosConcepto1UONUsu = 23020
    EPPermitirSeleccionarPresupuestosConcepto2UONUsu = 23021
    EPPermitirSeleccionarPresupuestosConcepto3UONUsu = 23022
    EPPermitirSeleccionarPresupuestosConcepto4UONUsu = 23023
    EPPermitirSeleccionarPresupuestosConcepto1UONsPerfil = 23024
    EPPermitirSeleccionarPresupuestosConcepto2UONsPerfil = 23025
    EPPermitirSeleccionarPresupuestosConcepto3UONsPerfil = 23026
    EPPermitirSeleccionarPresupuestosConcepto4UONsPerfil = 23027
    EPRestringirSeleccionCentroAprovUsuario = 23028
    EPPermitirAnularPedidos = 23029
    EPPermitirVerPedidosBorradosSeguimiento = 23030
    EPPermitirBorrarLineasPedido = 23031
    EPPermitirDeshacerBorradoLineasPedido = 23032
    EPPermitirBorrarPedidos = 23033
    EPPermitirDeshacerBorradoPedidos = 23034
    EPPermitirModificarPorcentajeDesvio = 23035

    'ACCIONES FSQA
    '====================
    QAPermitirModificarModoTraspasoProveedoresPanelCalidad = 24000
    QAPermitirModificarContactoCertificadosAutomaticos = 24001
    QAPermitirModificarParametrosNotificacionesCambioMaterialGS = 24002
    QAPermitirModificarPeriodoAntelacionCertificadosProximosExpirar = 24003
    QAPermitirModificarPeriodoValidezCertificadosExpirados = 24004
    QAPermitirModificarParametroNotificacionProveedorCambioCalificacionTotal = 24005
    QAPermitirModificarParametrosNotificacionProximidadFechaFinNoConformidad = 24006
    QAPermitirModificarNivelUnidadesNegocioMostrarDefectoProveedorCalificaciones = 24007
    QAPermitirModificarTipoFiltroMaterial = 24008
    'Configuración. Mantenimientos:
    QAPermitirGestionarMantenimientoMaterialesQA = 24100
    QAPermitirGestionarMantenimientoProveedoresQA = 24101
    QAPermitirGestionarMantenimientoUnidadesNegocioQA = 24102
    QAPermitirGestionarMantenimientoObjetivosSuelosVariablesCalidad = 24103
    QAPermitirGestionarMantenimientoVariablesCalidad = 24104
    QAPermitirMantNivelesEscalacion = 24105
    'Configuración. Proveedores:
    QARestringirProveedoresMinimoUnContactoCalidad = 24200
    QARestringirProveedoresMaterialUsuario = 24201
    QARestringirProveedoresEquipoUsuario = 24202

    'Certificados:
    QAPermitirSolicitarCertificados = 24300
    QAPermitirRenovarCertificados = 24301
    QAPermitirEnviarCertificados = 24302
    QAPermitirPublicarDespublicarCertificadosOtrosUsuarios = 24303
    QAPermitirModificarDatosCertificadosOtrosUsuarios = 24304
    QARestringirConsultaCertificadosSolicitadosUsuario = 24305
    QARestringirConsultaCertificadosSolicitadosUOUsuario = 24306
    QARestringirConsultaCertificadosSolicitadosDepartamentoUsuario = 24307
    QARestringirConsultaCertificadosUnidadesNegocioUsuario = 24308
    QARestringirConsultaCertificadossUnidadesNegocio = 24309

    'No conformidades:
    QAPermitirCerrarNoConformidadesConAccionesSinFinalizar = 24400
    QAPermitirReabrirNoConformidadesCerradas = 24401
    QAPermitirRevisarCierreNoConformidades = 24402
    QARestringirConsultaNoConformidadesRealizadasUsuario = 24403
    QARestringirConsultaNoConformidadesRealizadasUOUsuario = 24404
    QARestringirConsultaNoConformidadesRealizadasDepartamentoUsuario = 24405
    QARestringirSeleccionRevisoresUOUsuario = 24406
    QARestringirSeleccionRevisoresDepartamentoUsuario = 24407
    QARestringirSeleccionNotificadosInternosUOUsuario = 24408
    QARestringirSeleccionNotificadosInternosDepartamentoUsuario = 24409
    QAPermitirVisorNotificaciones = 24410


    'ACCIONES FSSM
    '====================
    SMConsultaActivos = 25000
    SMAltaNuevosActivos = 25001
    SMModificacionActivos = 25002
    SMEliminacionActivos = 25003
    'SMControlPresupuestario. Este control no tiene que estar en ACC, se activa en función del Control presupuestario
    SMControlPresupuestario = 25004
    SMNotificaciones = 25005

    'ACCIONES FSCN
    '====================
    CNPermitirOrganizarContenidosRedGestionCategorias = 26000
    CNPermitirOrganizarMiembrosRedGestionGrupos = 26001
    CNRestringirGestionCategoriasGruposRedACategoriasGruposUsuario = 26002
    CNRestringirGestionCategoriasGruposRedACategoriasGruposDepartamentoUsuario = 26003
    CNRestringirGestionCategoriasGruposRedACategoriasGruposUOUsuario = 26004
    'Uso de la red de colaboración:
    CNUsoRedColaboracionCrearMensajes = 26100
    CNUsoRedColaboracionCrearMensajesUrgentes = 26101
    CNEnvioMensajesEstructuraOrganizativaRestringirEnvioMiembrosUOUsuario = 26102
    CNEnvioMensajesEstructuraOrganizativaRestringirEnvioMiembrosDepartamentoUsuario = 26103
    CNEnvioMensajesProveedoresRestringirEnvioProveedoresMaterialUsuario = 26104
    CNEnvioMensajesProveedoresRestringirEnvioProveedoresEquipoComprasUsuario = 26105
    CNEnvioMensajesProveedoresRestringirEnvioProveedoresConContactoCalidad = 26106
    CNEnvioMensajesEstructuraComprasRestringirEnvioMiembrosEquipoComprasUsuario = 26107

    'ACCIONES FSBI
    '====================
    BIConfigurarCubos = 27000
    BIConfigurarQlikApps = 27001
    BIRestringirVisibilidadQlikAppsUONs = 27002
    BIRestringirVisibilidadQlikAppsGMNs = 27003

    'ACCIONES FSAL
    '====================
    ALVisorActividad = 28000

    'ACCIONES FSIS
    '====================
    INTRelanzarIntegracion = 29000

    'ACCIONES FSIM
    IMPermitirVisorNotificaciones = 30000   'Permitir ver las notificaciones de IM
    IMAltaFacturas = 30001   'Alta de facturas
    IMSeguimientoFacturas = 30002    'Seguimiento de facturas        
    IMVisorAutofacturas = 30014  'Visor de autofacturas

    'ACCIONES FSCM
    CMPermitirVisorNotificaciones = 31000
End Enum
Public Enum MenuPlantillaGS
    Reuniones = 1
End Enum
Public Enum DocumentoPlantillaGS
    Agenda = 1
    Acta = 2
End Enum
Public Enum VistaPlantillaGS
    ActaProveedor = 1
    ActaItem = 2
End Enum
Public Enum FormatoPlantillaGS
    DOC = 0
    DOCX = 1
    PDF = 2
    ODT = 3
    EPUB = 4
End Enum
Public Enum TipoVisor
    Solicitudes = 1
    NoConformidades = 2
    Certificados = 3
    Contratos = 4
    Facturas = 5
    Encuestas = 6
    SolicitudesQA = 7
    Proyectos = 8
    Otras = 9
    PedidosExpres = 10
End Enum
Public Enum MotivoVisibilidadSolicitud
    SolicitudAbiertaUsted = 1
    SolicitudPendiente = 3
    SolicitudPenditenteDevolucion = 4
    SolicitudTrasladadaEsperaDevolucion = 5
    SolicitudParticipo = 2
    SolicitudObservador = 6
    SolicitudObservadorSustitucion = 7
End Enum
Public Enum CamposGeneralesVisor
    DENOMINACION = 1
    ORIGEN = 2
    FECHAALTA = 3
    IDENTIFICADOR = 4
    IMPORTE = 5
    PETICIONARIO = 6
    TIPOSOLICITUD = 7
    USUARIO = 8
    ESTADO = 9
    SITUACIONACTUAL = 10
    MOTIVOVISIBILIDAD = 11
    MARCASEGUIMIENTO = 12
    FECHATRASLADO = 13
    PERSONATRASLADO = 14
    PROVEEDORTRASLADO = 15
    FECHALIMITEDEVOLUCION = 16
    INFOPEDIDOSASOCIADOS = 17
    INFOPROCESOSASOCIADOS = 18
    PROVEEDOR = 19
    ARTICULO = 20
    FECHAFINFLUJO = 21
    DEPARTAMENTO = 22
    FECHAULTIMAAPROBACION = 23
    ESTADOHOMOLOGACION = 24
End Enum
Public Enum ComunicacionPedido
    NoActivo = 0
    Activo = 1
    ConPedidoIntegrado = 2
End Enum
Public Enum TipoComunicacionPedido
    ComAuto = 1
    ComManual = 2
    ComPreguntar = 3
End Enum
Public Enum ComunicacionPedidoPORTAL
    NoActivo = 0
    Activo = 1
    ConPedidoIntegrado = 2
End Enum
Public Enum OrdenEntregaNotificacion
    RespuestaComunicarIntegrar = -3
    RespuestaComunicarEmitir = -2
    RespuestaNoComunicar = -1
    NoNotificado = 0
    Notificado = 1
    PendienteIntegracion = 2
    PendienteAprobacion = 3
End Enum
Public Class Operadores
    Public Enum Campos
        IGUAL = 10
        DISTINTO = 11
        MAYOR = 12
        MAYORIGUAL = 13
        MENOR = 14
        MENORIGUAL = 15
        CONTIENE = 20
        EMPIEZAPOR = 21
        TERMINAEN = 22
        NOCONTIENE = 23
        ENTRE = 30
        ES = 31
        NOES = 32
        HACE = 50
        PERIODO = 51
    End Enum
    Public Enum Formula
        Y = 1
        O = 2
        PARENTESIS_ABIERTO = 3
        PARENTESIS_CERRADO = 4
    End Enum
    Public Enum Fechas_Relativas
        HORAS = 0
        DIAS = 1
        SEMANAS = 2
        MESES = 3
        AÑOS = 4
    End Enum
    Public Enum Fechas_Periodo
        HOY = 0
        ESTA_SEMANA = 1
        ESTE_MES = 2
        ESTE_TRIMESTRE = 3
        ESTE_SEMESTRE = 4
        ESTE_AÑO = 5
    End Enum

    Public Enum FiltroMaterial
        FiltroMatQA = 0
        FiltroMatGS = 1
        FiltroMatQAGS = 2
    End Enum
    Public Enum TipoEspecificacion
        EspProceso = 0
        EspGrupo = 1
        EspItem = 2
        EspArticulo = 3
        EspProveedor = 4
        EspArticuloProveedor = 5
        EspLineaCatalogo = 6
        EspPlantilla = 7
        EspAdjudicacion = 8
        EspMailAdjun = 9
    End Enum
End Class
Public Enum TipoPedidoAbierto
    Importe = 2
    Cantidad = 3
End Enum
Public Enum TipoDePedido
    Directo = 0   'Ahora se llaman negociados
    Aprovisionamiento = 1  'Ahora se llaman de catalogo
    PedidosPM = 2  'Ahora se llaman directos
    PedidosERP = 3 'Ahora se llaman de ERP
    PedidosExpress = 4 'FSN_DES_GS_PRT_3278 - Pedidos generados desde solicitudes de tipo Pedido Express
    PedidoAbierto = 5
    ContraPedidoAbierto = 6
End Enum
#Region "CONFIGURACION WORKFLOW"
Public Enum TipoBloque
    Intermedio = 0
    Peticionario = 1
    Final = 2
End Enum
Public Enum TipoAccionBloque
    Aprobacion = 1
    Rechazo = 2
End Enum
Public Enum TipoRechazoAccionBloque
    RechazoTemporal = 1
    RechazoDefinitivo = 2
    Anulacion = 3
End Enum
Public Enum TipoPMRol
    RolComprador = 1
    RolProveedor = 2
    RolUsuario = 3
    RolPeticionario = 4
    RolObservador = 5
    RolGestor = 6
    RolReceptor = 7
    RolPeticionarioProveedor = 8
End Enum
Public Enum CuandoAsignarPMRol
    EnEtapa = 1
    Ahora = 2
    ProveedorDelContrato = 3
End Enum
Public Enum ComoAsignarPMRol
    PorCampo = 1
    PorPMRol = 2
    PorLista = 3
    PorServicioExt = 4
End Enum
Public Enum TipoBloqueoPMEnlace
    NoBloquea = 0
    Inicio = 1
    fin = 2
End Enum
Public Enum TipoPrecondicion
    Aviso = 1
    Bloquea = 2
End Enum
Public Enum OrigenCondicionEnlace
    Enlace = 1
    Precondicion = 2
End Enum
Public Enum TipoBloqueo
    Apertura = 0
    Adjudicacion = 1
    PedidosDirectos = 2
    PedidosEP = 3
End Enum
Public Enum TipoSustitucion
    SinSustitucion = 0
    Finalizadas = 1
    EnCurso = 2
    Todas = 3
End Enum
Public Enum TipoCampoCondicionEnlace
    CampoDeFormulario = 1
    Peticionario = 2
    DepPetcionario = 3
    UONPeticionario = 4
    NumProcesAbiertos = 5
    ImporteAdjudicado = 6
    ImporteSolicitud = 7
    ImporteAbierto = 8
    ImporteEmitido = 9
    ImporteFactura = 10
    ImporteCostesPlanificados = 11
    ImporteCostesNoPlanificados = 12
    ImporteDctosPlanificados = 13
    ImporteDctosNoPlanificados = 14
    NumDiscrepanciasAbiertas = 15
End Enum
Public Enum TipoValorCondicionEnlace
    CampoDeFormulario = 1
    Peticionario = 2
    DepPetcionario = 3
    UONPeticionario = 4
    NumProcesAbiertos = 5
    ImporteAdjudicado = 6
    ImporteSolicitud = 7
    ImporteAbierto = 8
    ImporteEmitido = 9
    ValorEstatico = 10
    '    FechaActual = 3
End Enum
Public Enum TipoCampo
    CampoDeFormulario = 1
    Peticionario = 2
    DepPeticionario = 3
    UONPeticionario = 4
    ImporteAdjudicado = 5
    ImporteSolicitud = 6
    ValorEstatico = 10
    Moneda = 11
    Proveedor = 12
    Contacto = 13
    Empresa = 14
    FechaInicio = 15
    FechaExpiracion = 16
    MostrarAlerta = 17
    EnviarMail = 18
    ImporteDesde = 19
    ImporteHasta = 20
End Enum
#End Region
Public Enum TipoProcesamientoXML
    FSNWeb = 1
    Portal = 2
    FSNWeb_PM_AprobacionRechazoMultiple = 3
    Portal_PM_AprobacionRechazoMultiple = 4
End Enum
Public Enum TiempoProcesamiento
    InicioDisco = 1
    InicioBD = 2
    Fin = 3
End Enum
Public Enum TipoErrorProcesamiento
    SinIniciarProceso = 0
    EnProceso = 1
    Procesada = 2
    ProcesoFallido = 3
End Enum
Public Enum TipoRecepcionAutomatica
    SinEspecificar = 0
    Automatica = 1
    Manual = 2
End Enum
Public Enum Estado_NivelEscalacion
    Pendiente = 0
    EnCurso = 1
    Rechazada = 2
End Enum
Public Enum PermisoAprobarRechazar_Escalacion_UNQA
    Nivel_1 = 14
    Nivel_2 = 17
    Nivel_3 = 20
    Nivel_4 = 23
End Enum
Public Enum PermisoRecibirAviso_Escalacion_UNQA
    Nivel_1 = 12
    Nivel_2 = 15
    Nivel_3 = 18
    Nivel_4 = 21
End Enum
Public Enum PermisoConsulta_Escalacion_UNQA
    Nivel_1 = 13
    Nivel_2 = 16
    Nivel_3 = 19
    Nivel_4 = 22
End Enum
Public Enum TipoResultadoLogin
    LoginOk = 0
    UsuIncorrecto = 1
    PwdIncorrecto = 2
    UsuBloqueado = 3
End Enum
Public Enum AccionesSummit
    ACCTraspAdjModifProveERPEmp = 4400
    ACCTraspAdjModifProveERProve = 4401
    ACCTraspAdjModifProveERPGrupo = 4402
    ACCTraspAdjModifProveERPItem = 4403
    ACCTraspAdjTraspasarEmp = 4404
    ACCTraspAdjTraspasarProve = 4405
    ACCTraspAdjTraspasarGrupo = 4406
    ACCTraspAdjTraspasarItem = 4407
    ACCTraspAdjBorrarEmp = 4408
    ACCTraspAdjBorrarProve = 4409
    ACCTraspAdjBorrarGrupo = 4410
    ACCTraspAdjBorrarItem = 4411
    ACCTraspAdjModifAtribGrupo = 4412
    ACCTraspAdjModifAtribItem = 4413
End Enum
Public Enum TipoParticipacionSolicitud
    Peticionario = 1
    Participante = 2
End Enum
Public Enum VarCalPCertCampoGeneral
    FecCumplimentacion = -1
    FecExpiracion = -2
End Enum
Public Enum TipoValidacionIntegracion
    SinMensaje = 0
    Mensaje_Bloqueo = 1
    Mensaje_Aviso = 2
End Enum
Public Enum TipoNotifCambioCalif
    Mensual = 0
    EnCadaCambio = 1
End Enum

Public Enum TLog_Pedido
    Alta_Log_Pedido = 0
    Cabecera_LOG = 1
    Adjuntos_Cabecera = 2
    Atrib_Cabecera = 3
    CD_Cabecera = 4
    LineaPedido = 5
    CD_LineaPedido = 6
    Impuestos_Linea = 7
    Imputacion_GS = 8
    Imputacion_SM = 9
    Atrib_LineaPedido = 10
    PlanEntrega_LineaPedido = 11
    Nueva_LineaPedido = 12
    Eliminar_LineaPedido = 13
    Eliminar_CD_Cabecera = 14
    Adjuntos_Linea = 15
    Eliminar_CD_Linea = 16
    Eliminar_PlanEntrega_Linea = 17
    Deshacer_Eliminar_LineaPedido = 18
    Cantidad_Pedida_Linea = 19
    Deshacer_Eliminar_Pedido = 20
    Borrar_Pedido = 21
End Enum

Public Enum VarCalOpcionConf
    EvaluarFormula = 0
    MediaPonderadaSegunVarHermana = 1
    NoCalcular = 2
End Enum
