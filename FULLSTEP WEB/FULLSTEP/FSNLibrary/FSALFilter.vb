﻿Imports System.IO
Imports System.Text
Imports System.Text.RegularExpressions

Public Class FSALFilter
    Inherits MemoryStream

    Private _base As Stream
    Private endHead As New Regex("</head>", RegexOptions.Compiled Or RegexOptions.Multiline)
    Private endHtml As New Regex("</html>", RegexOptions.Compiled Or RegexOptions.Multiline)
    Private _URLWebS As String
    Private _IdRegistro As String
    Private _Pagina As String

    Public Sub New(ByVal stream As Stream, ByVal sURLWebService As String, ByVal sIdRegistro As String, ByVal pagina As String)
        _URLWebS = sURLWebService
        _base = stream
        _IdRegistro = sIdRegistro
        _Pagina = pagina
    End Sub

    Public Overrides Sub Flush()
        _base.Flush()
    End Sub

    Public Overrides Sub Write(ByVal buffer As Byte(), ByVal offset As Integer, ByVal count As Integer)
        Dim contentInBuffer As String = UTF8Encoding.UTF8.GetString(buffer)

        If contentInBuffer.Contains("</head>") OrElse contentInBuffer.Contains("</html>") Then
            contentInBuffer = endHead.Replace(contentInBuffer, BeginScript)
            contentInBuffer = endHtml.Replace(contentInBuffer, EndScript)
            _base.Write(UTF8Encoding.UTF8.GetBytes(contentInBuffer), offset, UTF8Encoding.UTF8.GetByteCount(contentInBuffer))
        Else
            _base.Write(buffer, offset, count)
        End If

    End Sub

    Private Function BeginScript() As String
        Dim s As String = vbCrLf

        s += "<script type='text/javascript'>" & vbCrLf
        s += "var xmlobj;" & vbCrLf
        s += "function crearObjetoXmlHttpRequest(){" & vbCrLf
        s += "  if(window.XMLHttpRequest) {" & vbCrLf
        s += "    xmlobj = new XMLHttpRequest();" & vbCrLf
        s += "  }" & vbCrLf
        s += "  else  {" & vbCrLf
        s += "    try {" & vbCrLf
        s += "      xmlobj = new ActiveXObject('Microsoft.XMLHTTP');" & vbCrLf
        s += "    }" & vbCrLf
        s += "    catch (e) {" & vbCrLf
        s += "    }" & vbCrLf
        s += "  }" & vbCrLf
        s += "  return xmlobj;" & vbCrLf
        s += "}" & vbCrLf
        s += "" & vbCrLf
        s += "function manejadorRespuesta(Fn){" & vbCrLf
        s += "  if(xmlobj.readyState ==4) {" & vbCrLf
        s += "      var respuesta = xmlobj.responseText;" & vbCrLf
        s += "      /*Código DOM para actualizar el documento XHTML*/" & vbCrLf
        s += "      eval(Fn);" & vbCrLf
        s += "      xmlobj=null;" & vbCrLf
        s += "  }" & vbCrLf
        s += "}" & vbCrLf
        s += "" & vbCrLf
        s += "function addZero(x,n){" & vbCrLf
        s += "  while (x.toString().length < n){" & vbCrLf
        s += "    x = '0' + x;" & vbCrLf
        s += "  }" & vbCrLf
        s += "  return x;" & vbCrLf
        s += "}" & vbCrLf
        s += "" & vbCrLf
        s += "function fechaAhora(){" & vbCrLf
        s += "  var fecha = new Date();" & vbCrLf
        s += "  var yyyy,MM,dd,HH,mm,ss,ms;" & vbCrLf
        s += "  yyyy = fecha.getUTCFullYear();" & vbCrLf
        s += "  MM = addZero(fecha.getUTCMonth()+1, 2);" & vbCrLf
        s += "  dd = addZero(fecha.getUTCDate(), 2);" & vbCrLf
        s += "  HH = addZero(fecha.getUTCHours(), 2);" & vbCrLf
        s += "  mm = addZero(fecha.getUTCMinutes(), 2);" & vbCrLf
        s += "  ss = addZero(fecha.getUTCSeconds(), 2);" & vbCrLf
        s += "  ms = addZero(fecha.getUTCMilliseconds(), 3);" & vbCrLf
        s += "  return yyyy + '-' + MM + '-' + dd + ' ' + HH + ':' + mm + ':' + ss + '.' + ms;" & vbCrLf
        s += "}" & vbCrLf
        s += "" & vbCrLf
        s += "var fechaBegin = fechaAhora();" & vbCrLf
        s += "</script>" & vbCrLf
        s += "</head>" & vbCrLf

        BeginScript = s

    End Function

    Private Function EndScript() As String
        Dim s As String = ""

        s += "<script type='text/javascript'>" & vbCrLf
        s += "xmlobj = crearObjetoXmlHttpRequest();" & vbCrLf
        s += "xmlobj.open('POST','" & _URLWebS & "/FSALActualizarAccesos',true);" & vbCrLf
        s += "xmlobj.setRequestHeader('Content-Type','application/x-www-form-urlencoded');" & vbCrLf
        's += "xmlobj.onreadystatechange = FunNull;" & vbCrLf
        s += "var fechaEnd = fechaAhora();" & vbCrLf
        s += "var pagina = window.location.href;" & vbCrLf
        s += "xmlobj.send('Tipo=3&IdRegistro=" & _IdRegistro & "&fechapet=' + fechaBegin.toString() + '&fecha=' + fechaEnd.toString() + '&sNormalizarFechas=1&pagina=" & _Pagina & "&iP=');" & vbCrLf
        s += "</script>" & vbCrLf
        s += "</html>" & vbCrLf

        EndScript = s

    End Function

End Class