﻿Imports VB = Microsoft.VisualBasic
Imports ICSharpCode.SharpZipLib.Zip
Imports ICSharpCode.SharpZipLib.Checksums
Imports System.Runtime.InteropServices.Marshal
Imports System.Runtime.InteropServices
Imports System.IO
Imports System.Drawing
Imports System.Web
Imports System.Security.Permissions
Imports System.Text
Imports ExpertPdf.HtmlToPdf

Public Module modUtilidades
    <System.Runtime.CompilerServices.Extension()> _
    Public Function CopyAnonymusToDataTable(Of T)(ByVal info As IEnumerable(Of T), Optional ByVal dateCols As List(Of String) = Nothing, Optional ByVal doubleCols As List(Of String) = Nothing) As DataTable
        Dim type = GetType(T)
        Dim dt As New DataTable()
        Dim r As DataRow

        type.GetProperties().ToList().ForEach(Function(a) dt.Columns.Add(a.Name))

        If Not dateCols Is Nothing Then
            For Each col As String In dateCols
                dt.Columns(col).DataType = GetType(DateTime)
            Next
        End If

        If Not doubleCols Is Nothing Then
            For Each col As String In doubleCols
                dt.Columns(col).DataType = GetType(Double)
            Next
        End If

        For Each c As Object In info
            Dim x As Object = c
            r = dt.NewRow()
            x.[GetType]().GetProperties().ToList().ForEach(Function(a) InlineAssignHelper(r(a.Name), a.GetValue(x, Nothing)))
            dt.Rows.Add(r)
        Next

        Return dt
    End Function
    Private Function InlineAssignHelper(Of T)(ByRef target As T, ByVal value As T) As T
        target = value
        Return value
    End Function
#Region "Variables suplantacion"
    Private Const LOGON32_LOGON_INTERACTIVE As Integer = 2
    Private Const LOGON32_PROVIDER_DEFAULT As Integer = 0

    Declare Function LogonUserA Lib "advapi32.dll" (ByVal lpszUsername As String, _
                            ByVal lpszDomain As String, _
                            ByVal lpszPassword As String, _
                            ByVal dwLogonType As Integer, _
                            ByVal dwLogonProvider As Integer, _
                            ByRef phToken As IntPtr) As Integer

    Declare Auto Function DuplicateToken Lib "advapi32.dll" ( _
                            ByVal ExistingTokenHandle As IntPtr, _
                            ByVal ImpersonationLevel As Integer, _
                            ByRef DuplicateTokenHandle As IntPtr) As Integer

    Declare Auto Function RevertToSelf Lib "advapi32.dll" () As Long
    Declare Auto Function CloseHandle Lib "kernel32.dll" (ByVal handle As IntPtr) As Long
    Public Declare Function FindMimeFromData Lib "urlmon.dll" (ByVal pBC As IntPtr, <MarshalAs(UnmanagedType.LPWStr)> ByVal pwzUrl As String, <MarshalAs(UnmanagedType.LPArray, ArraySubType:=UnmanagedType.I1, SizeParamIndex:=3)> ByVal pBuffer As Byte(), ByVal cbSize As Integer, _
  <MarshalAs(UnmanagedType.LPWStr)> ByVal pwzMimeProposed As String, _
  ByVal dwMimeFlags As Integer, <MarshalAs(UnmanagedType.LPWStr)> _
  ByRef ppwzMimeOut As String, ByVal dwReserved As Integer) As Integer
#End Region
#Region " Funciones del EPNotificador pasadas aqui "
    ''' <summary>
    ''' Funcion que devuelve la fecha en el formato pasado como parámetro
    ''' </summary>
    ''' <param name="Fecha">Fecha a transformar</param>
    ''' <param name="datFmt">Formato de fecha a visualizar</param>
    ''' <returns>Una variable de tipo fecha en el formato introducido como parámetro</returns>
    ''' <remarks>
    ''' Llamada desde: EPServer\Notificador\Notificar.vb
    ''' Tiempo máximo: 0 seg</remarks>
    Public Function VisualizacionFecha(ByVal Fecha As Object, ByVal datFmt As Object) As Object
        'Revisado por sgp a 22/12/08 para migracion
        Dim returnFecha As Object
        Dim sUrte As Object
        Dim sMes As Object
        Dim sDia As Object
        'Comentado por no usar-->Dim sepFecha As Object

        'Comentado por no usar-->sepFecha = Mid(datFmt, 3, 1)
        If IsDBNull(Fecha) Then
            VisualizacionFecha = ""
            Exit Function
        End If
        sDia = CStr(VB.Day(Fecha))
        sMes = CStr(Month(Fecha))
        sUrte = CStr(Year(Fecha))


        sDia = Left("00", 2 - Len(sDia)) & sDia
        sMes = Left("00", 2 - Len(sMes)) & sMes

        returnFecha = Replace(datFmt, "dd", sDia)
        returnFecha = Replace(returnFecha, "mm", sMes)
        returnFecha = Replace(returnFecha, "MM", sMes)
        returnFecha = Replace(returnFecha, "yyyy", sUrte)
        VisualizacionFecha = returnFecha
    End Function
    Public Function null2str(ByVal valor As Object) As String
        null2str = IIf(IsDBNull(valor), "", valor)
    End Function
    Public Function ReplaceV(ByVal s1 As String, ByVal s2 As String, ByVal s3 As Object) As String
        ReplaceV = Replace(s1, s2, null2str(s3))
    End Function
    Public Function VB2HTML(ByVal text As Object) As String
        Dim t As Object
        If IsDBNull(text) Then
            VB2HTML = ""
            Exit Function
        End If
        t = Replace(text, Chr(13) & Chr(10), "<br>")
        t = Replace(t, Chr(10), "<br>")
        t = Replace(t, Chr(13), "<br>")
        VB2HTML = t
    End Function
    ''' <summary>
    ''' Funcion que devuelve la fecha en el formato pasado como parámetro
    ''' </summary>
    ''' <param name="ecFmt">Formato de los decimales</param>
    ''' <param name="Num">Número a cambiar el formato</param>
    ''' <param name="PrecFmt">Precision de los decimales, en dígitos</param>
    ''' <param name="ThouFmt">Formato de miles</param>
    ''' <returns>Una variable de tipo numero en el formato introducido como parámetro</returns>
    ''' <remarks>
    ''' Llamada desde: EPServer\Notificador\Notificar.vb, EPServer\Aprobación.aspx
    ''' Tiempo máximo: 0 seg</remarks>
    Public Function VisualizacionNumero(ByVal Num As Object, ByVal ecFmt As Object, ByVal ThouFmt As Object, ByVal PrecFmt As Object) As Object
        'Revisado por sgp a fecha de 22/12/08 para la migracion a VS2008
        Dim s As Object
        Dim sNum As Object
        Dim ParteEntera As Object
        Dim numeroLocal As Object
        Dim snumeroLocal As Object
        Dim decimalloc As Object
        Dim i As Object
        Dim ParteDecimal As Object
        Dim tmpParteDecimal As Object
        Dim quitarCeros As Object
        Dim newValor As Object

        numeroLocal = 1 / 10
        If IsDBNull(Num) Or IsNothing(Num) Then
            VisualizacionNumero = ""
            Exit Function
        End If

        If Num = 0 Then
            VisualizacionNumero = "0"
            Exit Function
        End If

        snumeroLocal = CStr(numeroLocal)
        If InStr(snumeroLocal, ".") Then
            decimalloc = "."
        Else
            decimalloc = ","
        End If

        Num = System.Math.Round(Num, CType(PrecFmt, Integer))

        If IsDBNull(Num) Or Num = 0 Then
            VisualizacionNumero = ""
            Exit Function
        End If

        ParteEntera = CStr(Fix(Num))
        sNum = CStr(Num)
        tmpParteDecimal = ""

        If InStr(sNum, decimalloc) > 0 Then
            ParteDecimal = Mid(sNum, InStr(sNum, decimalloc) + 1, PrecFmt)
        Else
            ParteDecimal = ""
        End If

        quitarCeros = True

        For i = Len(ParteDecimal) To 1 Step -1
            If Mid(ParteDecimal, i, 1) = "0" And quitarCeros Then
            Else
                tmpParteDecimal = Mid(ParteDecimal, i, 1) & tmpParteDecimal
                quitarCeros = False
            End If
        Next

        ParteDecimal = tmpParteDecimal
        newValor = ""
        s = 0
        For i = Len(ParteEntera) To 1 Step -1
            If s = 3 Then
                newValor = ThouFmt & newValor
                s = 0
            End If
            newValor = Mid(ParteEntera, i, 1) & newValor
            s = s + 1
        Next
        Dim vPrec As Short
        Dim ret As String

        If ParteDecimal <> "" Then
            ret = newValor & ecFmt & ParteDecimal
        Else
            ret = newValor
        End If

        vPrec = PrecFmt
        While ret = "0" And Num <> 0
            vPrec = vPrec + 1
            ret = VisualizacionNumero(Num, ecFmt, ThouFmt, vPrec)

        End While

        VisualizacionNumero = ret
    End Function
#End Region
    Public Function DBNullToSomething(Optional ByVal value As Object = Nothing) As Object
        If IsDBNull(value) Then
            Return Nothing
        Else
            Return value
        End If
    End Function
    Public Function VacioToNothing(ByVal s As Object) As Object
        Dim o As Object = DBNullToSomething(s)
        If o Is Nothing Then Return Nothing
        If o.ToString() = "" Then
            Return Nothing
        Else
            Return o
        End If
    End Function
    Public Function NothingToDBNull(Optional ByVal value As Object = Nothing) As Object
        If IsNothing(value) Then
            Return System.DBNull.Value
        Else
            Return value
        End If
    End Function
    ''' <summary>
    ''' Tratamos de convertir el objeto recibido a Double
    ''' </summary>
    ''' <param name="value">Valor a convertir a Double</param>
    ''' <returns>Valor convertido a double. Si no es posible o el value es null o nothing, devuelve 0</returns>
    ''' <remarks>Llamado desde toda la aplicación, donde se requiera su uso. Tiempo máx inferior a 0,1 seg</remarks>
    Public Function DBNullToDbl(Optional ByVal value As Object = Nothing) As Double
        If IsDBNull(value) Then
            Return 0
        ElseIf value Is Nothing Then
            Return 0
        Else
            Try
                Dim valueDbl As Double = CDbl(value)
                Return valueDbl
            Catch ex As Exception
                Return 0
            End Try
        End If
    End Function
    ''' <summary>
    ''' Tratamos de convertir el objeto recibido a Integer
    ''' </summary>
    ''' <param name="value">Valor a convertir a Integer</param>
    ''' <returns>Valor convertido a Integer. Si no es posible o el value es null o nothing, devuelve 0</returns>
    ''' <remarks>Llamado desde toda la aplicación, donde se requiera su uso. Tiempo máx inferior a 0,1 seg</remarks>
    Public Function DBNullToInteger(Optional ByVal value As Object = Nothing) As Integer
        If IsDBNull(value) Then
            Return 0
        ElseIf value Is Nothing Then
            Return 0
        Else
            Return CInt(value)
        End If
    End Function
    ''' <summary>
    ''' Tratamos de convertir el objeto recibido a Long
    ''' </summary>
    ''' <param name="value">Valor a convertir a Long</param>
    ''' <returns>Valor convertido a Long. Si no es posible o el value es null o nothing, devuelve 0</returns>
    ''' <remarks>Llamado desde toda la aplicaciÃ³n, donde se requiera su uso. Tiempo mÃ¡x inferior a 0,1 seg</remarks>
    Public Function DBNullToLong(Optional ByVal value As Object = Nothing) As Long
        If IsDBNull(value) Then
            Return 0
        ElseIf value Is Nothing Then
            Return 0
        Else
            Return CLng(value)
        End If
    End Function
    Public Function DBNullToDec(Optional ByVal value As Object = Nothing) As Decimal
        If IsDBNull(value) Then
            Return 0
        ElseIf value Is Nothing Then
            Return 0
        Else
            Return CDec(value)
        End If
    End Function
    Public Function GenerateRandomPath() As String
        Dim s As String = ""
        Dim i As Integer
        Dim lowerbound As Integer = 65
        Dim upperbound As Integer = 90
        Randomize()
        For i = 1 To 10
            s = s & Chr(Int((upperbound - lowerbound + 1) * Rnd() + lowerbound))
        Next
        Return s
    End Function
    ''' <summary>
    ''' Devuelve un numero aleatorio
    ''' </summary>
    ''' <param name="Valor">Valor opcional en funcion del cual junto con la fecha se obtendria el numero aleatorio</param>
    ''' <returns>retorna el numero aleatorio</returns>
    ''' <remarks></remarks>
    Public Function GetRandomNumber(Optional ByVal Valor As Int32 = System.Int32.MaxValue) As Integer
        Dim objRandom As New System.Random(CType(System.DateTime.Now.Ticks Mod Valor, Integer))

        Return objRandom.Next
    End Function
    Public Function igDateToDate(ByVal s As String) As DateTime
        Try
            If s = Nothing Or s = "" Then
                Return Nothing
            End If
            Dim arr As Array = s.Split("-")
            Return DateSerial(arr(0), arr(1), arr(2))
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Public Function DateToigDate(ByVal dt As DateTime) As String
        Try
            If dt = Nothing Then
                Return ""
            End If
            '2005-7-14-12-7-14-244
            'anyo-m-di-ho-m-se-mil

            Dim s As String = Year(dt).ToString() + "-" + Month(dt).ToString() + "-" + Day(dt).ToString() + "-" + Hour(dt).ToString() + "-" + Minute(dt).ToString() + "-" + Second(dt).ToString()
            Return s
        Catch ex As Exception
            Return ""
        End Try
    End Function
    Public Function SQLBinaryToBoolean(ByVal var As Object) As Boolean
        If IsDBNull(var) OrElse var = Nothing Then
            SQLBinaryToBoolean = False
        Else
            If var = 1 Then
                SQLBinaryToBoolean = True
            Else
                SQLBinaryToBoolean = False
            End If
        End If
    End Function
    Public Function BooleanToSQLBinary(ByVal b As Object) As Object
        If IsDBNull(b) Then
            BooleanToSQLBinary = "NULL"
        Else
            If b Then
                BooleanToSQLBinary = 1
            Else
                BooleanToSQLBinary = 0
            End If
        End If
    End Function
    Public Function BooleanToSQLBinaryDBNull(ByVal b As Object) As Object
        If IsDBNull(b) Then
            BooleanToSQLBinaryDBNull = System.DBNull.Value
        Else
            If b Then
                BooleanToSQLBinaryDBNull = 1
            Else
                BooleanToSQLBinaryDBNull = 0
            End If
        End If
    End Function
    ''' <summary>
    ''' Devuelve un objeto de tipo fecha relativa a una fecha que puede ser dias, semanas, meses o años
    ''' </summary>
    ''' <param name="iTipoFecha">Tipo de fecha a devolver</param>
    ''' <param name="dtFechaFija">Fecha relativa a la que devolver la fecha</param>
    ''' <returns>Devuelve una fecha relativa a la pasada como parámetro, dependiendo del parámetro iTipoFecha</returns>
    ''' <remarks>
    ''' Llamada desde:PM\Solicitud y PM\Instancia </remarks>
    Public Function DevolverFechaRelativaA(ByVal iTipoFecha As TiposDeDatos.TipoFecha, ByVal dtFechaFija As Date, Optional ByVal bFavorito As Boolean = False) As Object
        If Not dtFechaFija = Nothing And Not bFavorito Then
            Return dtFechaFija
        End If
        Select Case iTipoFecha
            Case TiposDeDatos.TipoFecha.FechaAlta
                Return Now
            Case TiposDeDatos.TipoFecha.UnDiaDespues
                Return DateAdd(DateInterval.Day, 1, Today)
            Case TiposDeDatos.TipoFecha.DosDiasDespues
                Return DateAdd(DateInterval.Day, 2, Today)
            Case TiposDeDatos.TipoFecha.TresDiasDespues
                Return DateAdd(DateInterval.Day, 3, Today)
            Case TiposDeDatos.TipoFecha.CuatroDiasDespues
                Return DateAdd(DateInterval.Day, 4, Today)
            Case TiposDeDatos.TipoFecha.CincoDiasDespues
                Return DateAdd(DateInterval.Day, 5, Today)
            Case TiposDeDatos.TipoFecha.SeisDiasDespues
                Return DateAdd(DateInterval.Day, 6, Today)
            Case TiposDeDatos.TipoFecha.UnaSemanaDespues
                Return DateAdd(DateInterval.Day, 7, Today)
            Case TiposDeDatos.TipoFecha.DosSemanaDespues
                Return DateAdd(DateInterval.Day, 14, Today)
            Case TiposDeDatos.TipoFecha.TresSemanaDespues
                Return DateAdd(DateInterval.Day, 21, Today)
            Case TiposDeDatos.TipoFecha.UnMesDespues
                Return DateAdd(DateInterval.Month, 1, Today)
            Case TiposDeDatos.TipoFecha.DosMesDespues
                Return DateAdd(DateInterval.Month, 2, Today)
            Case TiposDeDatos.TipoFecha.TresMesDespues
                Return DateAdd(DateInterval.Month, 3, Today)
            Case TiposDeDatos.TipoFecha.CuatroMesDespues
                Return DateAdd(DateInterval.Month, 4, Today)
            Case TiposDeDatos.TipoFecha.CincoMesDespues
                Return DateAdd(DateInterval.Month, 5, Today)
            Case TiposDeDatos.TipoFecha.SeisMesDespues
                Return DateAdd(DateInterval.Month, 6, Today)
            Case TiposDeDatos.TipoFecha.UnAnyoDespues
                Return DateAdd(DateInterval.Year, 1, Today)
            Case Else
                If Not dtFechaFija = Nothing Then
                    Return dtFechaFija
                Else
                    Return DBNull.Value
                End If
        End Select
    End Function
    Public Function StrToSQLNULL(ByVal StrThatCanBeEmpty As Object) As Object
        If IsDBNull(StrThatCanBeEmpty) Then
            StrToSQLNULL = "NULL"
        Else
            If StrThatCanBeEmpty = "" Then
                StrToSQLNULL = "NULL"
            Else
                StrToSQLNULL = "'" & DblQuote(CStr(StrThatCanBeEmpty)) & "'"
            End If
        End If
    End Function
    Public Function DblQuote(ByVal StrToDblQuote As String) As String
        DblQuote = Replace(StrToDblQuote, "'", "''")
    End Function
    Public Function DateToSQLDate(ByVal DateThatCanBeEmpty As Object) As Object
        Try
            If IsDBNull(DateThatCanBeEmpty) Then
                Return "NULL"
            Else
                If IsDate(DateThatCanBeEmpty) Then
                    Return "Convert(datetime,'" & Format(DateThatCanBeEmpty, "MM-dd-yyyy") & "',110)"
                Else
                    If DateThatCanBeEmpty = "" Then
                        Return "NULL"
                    Else
                        Return "Convert(datetime,'" & Format(DateThatCanBeEmpty, "MM-dd-yyyy") & "',110)"
                    End If
                End If
            End If
        Catch ex As Exception
            Return "NULL"
        End Try
    End Function
    Public Function DateToSQLTimeDate(ByVal DateThatCanBeEmpty) As Object
        Try
            If IsDBNull(DateThatCanBeEmpty) Then
                Return "NULL"
            Else
                If IsDate(DateThatCanBeEmpty) Then
                    Return "Convert(datetime,'" & Format(DateThatCanBeEmpty, "MM-dd-yyyy HH:mm:ss") & "',110)"
                Else
                    If DateThatCanBeEmpty = "" Then
                        Return "NULL"
                    Else
                        Return "Convert(datetime,'" & Format(DateThatCanBeEmpty, "MM-dd-yyyy HH:mm:ss") & "',110)"
                    End If
                End If
            End If
        Catch ex As Exception
            Return "NULL"
        End Try
    End Function
    Public Function DateToBDDate(ByVal DateValue As Date) As String
        'Ponemos la fecha en formato dd/mm/yyyy pq en la BD se trata asi
        Dim DateFormat As System.Globalization.DateTimeFormatInfo
        Dim sCurrentCulture As String

        Try
            If IsDBNull(DateValue) Then
                DateToBDDate = "NULL"
            Else
                sCurrentCulture = System.Globalization.CultureInfo.CurrentCulture.Name
                DateFormat = New System.Globalization.CultureInfo(sCurrentCulture, False).DateTimeFormat
                DateFormat.ShortDatePattern = "dd/MM/yyyy"
                DateToBDDate = FormatDate(DateValue, DateFormat)
            End If
        Catch ex As Exception
            Return "NULL"
        End Try
    End Function
    Public Function DblToSQLFloat(ByVal DblToConvert As Object) As String
        Dim StrToConvert As String

        Try
            If DblToConvert Is Nothing Then
                DblToSQLFloat = "NULL"
                Exit Function
            End If
            If IsDBNull(DblToConvert) Then
                DblToSQLFloat = "NULL "
                Exit Function
            End If

            If DblToConvert = "" Then
                DblToSQLFloat = "NULL"
                Exit Function
            End If
            StrToConvert = CStr(DblToConvert)
            StrToConvert = Replace(StrToConvert, ",", ".")
            DblToSQLFloat = StrToConvert
        Catch ex As Exception
            If IsNumeric(DblToConvert) Then
                StrToConvert = CStr(DblToConvert)
                StrToConvert = Replace(StrToConvert, ",", ".")
                Return StrToConvert
            Else
                Return "NULL"
            End If
        End Try
    End Function
    Public Function DblToSQLFloatDBNull(ByVal DblToConvert As Object) As Object
        Dim StrToConvert As String

        Try
            If DblToConvert Is Nothing Then
                DblToSQLFloatDBNull = System.DBNull.Value
                Exit Function
            End If
            If IsDBNull(DblToConvert) Then
                DblToSQLFloatDBNull = System.DBNull.Value
                Exit Function
            End If

            If DblToConvert = "" Then
                DblToSQLFloatDBNull = System.DBNull.Value
                Exit Function
            End If
            StrToConvert = CStr(DblToConvert)
            StrToConvert = Replace(StrToConvert, ",", ".")
            DblToSQLFloatDBNull = StrToConvert
        Catch ex As Exception
            If IsNumeric(DblToConvert) Then
                StrToConvert = CStr(DblToConvert)
                StrToConvert = Replace(StrToConvert, ",", ".")
                Return StrToConvert
            Else
                Return System.DBNull.Value
            End If
        End Try
    End Function
    Public Function IsTime(ByVal DataAsked) As Boolean
        Dim s As String

        s = CStr(DataAsked)

        If IsDate("01/01/1990 " & DataAsked) = False Then
            IsTime = False
            Exit Function
        End If

        IsTime = True
    End Function
    Public Function DBNullToStr(Optional ByVal value As Object = Nothing) As String
        If IsDBNull(value) Then
            Return ""
        Else
            Return value
        End If
    End Function
    Public Function strToDBNull(Optional ByVal value As Object = Nothing) As Object
        If value = "" Then
            Return System.DBNull.Value
        Else
            Return value
        End If
    End Function
    Public Function strToNothing(Optional ByVal value As Object = Nothing) As Object
        If value = "" Then
            Return Nothing
        Else
            Return value
        End If
    End Function
    ''' <summary>
    ''' Función que recibe una variable string y trata de pasarla a un numérico Double.
    ''' Si el valor recibido es vacío o no es posible efectuar la conversión, devuelve 0
    ''' </summary>
    ''' <param name="value">string a convertir</param>
    ''' <returns>El valor convertido. Si el valor recibido es vacío o no es posible efectuar la conversión, devuelve 0</returns>
    ''' <remarks>Llamada desde allí donde se necesite usar la función. Tiempo máx: inferior a 0,1 seg</remarks>
    Public Function strToDbl(ByVal value As String) As Double
        If value = "" Then
            Return 0
        Else
            Try
                Dim valueDbl As Double = CDbl(value)
                Return valueDbl
            Catch ex As Exception
                Return 0
            End Try
        End If
    End Function
    ''' <summary>
    ''' Función que recibe una variable string y trata de pasarla a un numérico Integer.
    ''' Si el valor recibido es vacío o no es posible efectuar la conversión, devuelve 0
    ''' </summary>
    ''' <param name="value">string a convertir</param>
    ''' <returns>El valor convertido. Si el valor recibido es vacío o no es posible efectuar la conversión, devuelve 0</returns>
    ''' <remarks>Llamada desde allí donde se necesite usar la función. Tiempo máx: inferior a 0,1 seg</remarks>
    Public Function strToInt(ByVal value As String) As Integer
        If value = "" Then
            Return 0
        Else
            Try
                Dim valueInt As Integer = CInt(value)
                Return valueInt
            Catch ex As Exception
                Return 0
            End Try
        End If
    End Function
    ''' <summary>
    ''' Función que recibe una variable string y trata de pasarla a un BOOLEAN.
    ''' Si el valor recibido es vacío o no es posible efectuar la conversión, devuelve false
    ''' </summary>
    ''' <param name="value">string a convertir</param>
    ''' <returns>El valor convertido. Si el valor recibido es vacío o no es posible efectuar la conversión, devuelve false</returns>
    ''' <remarks>Llamada desde allí donde se necesite usar la función. Tiempo máx: inferior a 0,1 seg</remarks>
    Public Function strToBoolean(ByVal value As String) As Integer
        If value = "" Then
            Return False
        Else
            Try
                Dim valueBool As Integer = CBool(value)
                Return valueBool
            Catch ex As Exception
                Return False
            End Try
        End If
    End Function
    ''' <summary>
    ''' Función que recibe una variable string y trata de pasarla a un numérico Integer.
    ''' Si el valor recibido es vacío o no es posible efectuar la conversión, devuelve 0
    ''' </summary>
    ''' <param name="value">string a convertir</param>
    ''' <returns>El valor convertido. Si el valor recibido es vacío o no es posible efectuar la conversión, devuelve 0</returns>
    ''' <remarks>Llamada desde allí donde se necesite usar la función. Tiempo máx: inferior a 0,1 seg</remarks>
    Public Function strToLong(ByVal value As String) As Long
        If value = "" Then
            Return 0
        Else
            Try
                Dim valueInt As Long = CLng(value)
                Return valueInt
            Catch ex As Exception
                Return 0
            End Try
        End If
    End Function
    ''' <summary>
    ''' Crea la ruta completa al path proporcionado.
    ''' </summary>
    ''' <param name="sPath">String con el path a crear</param>
    ''' <remarks></remarks>
    Public Sub CrearArbol(ByVal sPath As String)
        If Not IO.Directory.Exists(sPath) Then _
            IO.Directory.CreateDirectory(sPath)
    End Sub
    ''' <summary>
    ''' Reemplaza los caracteres especiales que puedan dar problemas en nombres de archivo por guiones bajos.
    ''' </summary>
    ''' <param name="sFileName">Nombre del archivo.</param>
    ''' <returns>Un string con el nombre del archivo con los caracteres especiales reemplazados.</returns>
    ''' <remarks></remarks>
    Public Function ValidFilename(ByVal sFileName As String) As String
        Dim s As String = sFileName

        s = Replace(s, "\", "_")
        s = Replace(s, "/", "_")
        s = Replace(s, ":", "_")
        s = Replace(s, "*", "_")
        s = Replace(s, "?", "_")
        s = Replace(s, """", "_")
        s = Replace(s, ">", "_")
        s = Replace(s, "<", "_")
        s = Replace(s, "|", "_")
        s = Replace(s, "@", "_")
        s = Replace(s, "#", "_")

        Return s
    End Function
    ''' <summary>
    ''' Devuelve un número a partir de una cadena.
    ''' </summary>
    ''' <param name="value">String con el valor a convertir.</param>
    ''' <returns>Double con el valor obtenido al convertir el String.</returns>
    Public Function Numero(ByVal value As String) As Double
        Return Numero(value, "", "")
    End Function
    ''' <summary>
    ''' Devuelve un número a partir de una cadena.
    ''' </summary>
    ''' <param name="value">String con el valor a convertir.</param>
    ''' <param name="sDecimalSeparator">Caracter que se utiliza en la cadena como separador decimal.</param>
    ''' <returns>Double con el valor obtenido al convertir el String.</returns>
    Public Function Numero(ByVal value As String, ByVal sDecimalSeparator As String) As Double
        Return Numero(value, sDecimalSeparator, "")
    End Function
    ''' <summary>
    ''' Devuelve un número a partir de una cadena.
    ''' </summary>
    ''' <param name="value">String con el valor a convertir.</param>
    ''' <param name="sDecimalSeparator">Caracter que se utiliza en la cadena como separador decimal.</param>
    ''' <param name="sThousanSeparator">Caracter que se utiliza en la cadena como</param>
    ''' <returns>Double con el valor obtenido al convertir el String.</returns>
    Public Function Numero(ByVal value As String, ByVal sDecimalSeparator As String, ByVal sThousanSeparator As String) As Double
        If value = Nothing Then
            Return Nothing
        Else
            If sDecimalSeparator <> "" Then
                Try
                    value = value.Replace(sThousanSeparator, "")
                Catch
                End Try

                value = value.Replace(sDecimalSeparator, System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)

                Try
                    Return CDbl(value)
                Catch
                    Return Nothing
                End Try

            Else
                If IsNumeric(value) Then
                    Return CDbl(value)
                Else
                    Return Nothing
                End If
            End If
        End If
    End Function
    ''' <summary>
    ''' Devuelve un string con el numero que recibe formateado con el formato que recibe
    ''' </summary>
    ''' <param name="dNumber">Numero a formatear</param>
    ''' <param name="oNumberFormat">formato</param>
    ''' <returns>String con el numero formateado</returns>
    ''' <remarks></remarks>
    Public Function FormatNumber(ByVal dNumber As Double, ByVal oNumberFormat As System.Globalization.NumberFormatInfo) As String
        Return dNumber.ToString("N", oNumberFormat)
    End Function
    ''' <summary>
    ''' Devuelve un string con la fecha pasada en dDate formateada según oDateFormat, por defecto aplica formato d:01/01/1900
    ''' </summary>
    ''' <param name="dDate">fecha a formatear</param>
    ''' <param name="oDateFormat">IFormatProvider</param>
    ''' <param name="sFormat">Format: d :08/17/2000; D :Thursday, August 17, 2000; f :Thursday, August 17, 2000 16:32; F :Thursday, August 17, 2000 16:32:32; g :08/17/2000 16:32;G :08/17/2000 16:32:32...
    '''</param>
    ''' <returns>String con la fecha</returns>
    ''' <remarks></remarks>
    Public Function FormatDate(ByVal dDate As Date, ByVal oDateFormat As System.Globalization.DateTimeFormatInfo, Optional ByVal sFormat As String = "d") As String
        Return dDate.ToString(sFormat, oDateFormat)
    End Function
    ''' <summary>
    ''' Convierte un texto para utilizarlo como cadena en javascript
    ''' </summary>
    ''' <param name="text">Texto a convertir</param>
    ''' <returns>Un String con el texto formateado para poderlo utilizar como cadena en javascript</returns>
    Function JSText(ByVal text As String) As String
        Dim t As String
        If text = Nothing Then
            Return ""
            Exit Function
        End If
        t = Replace(text, Chr(92), "\\")
        t = Replace(t, "'", "\'")
        t = Replace(t, """", "\""")
        t = Replace(t, Chr(9), "\t")
        t = Replace(t, Chr(13) & Chr(10), "\n")
        t = Replace(t, Chr(10), "\n")
        t = Replace(t, Chr(13), "\n")

        Return t
    End Function
    ''' <summary>
    ''' Reemplaza el separador decimal "," por "."
    ''' </summary>
    ''' <param name="Num">String con el número a formatear</param>
    ''' <returns>Un String con el número con separador decimal "."</returns>
    Function JSNum(ByVal Num As String) As String
        Dim t As String
        Try
            If IsDBNull(Num) Then
                Return "null"
            End If
            t = Replace(Num.ToString(), ",", ".")
            Return t
        Catch ex As Exception

            Return "null"
        End Try
    End Function
    ''' <summary>
    ''' Función que se encarga de dar formato al texto escrito por el usuario en la caja de texto destinada a la busqueda e artículos, encargada de ignorar acentos, mayúsculas y minúsculas...
    ''' </summary>
    ''' <param name="Cadena">Texto a buscar</param>
    ''' <param name="IgnorarAcentos">Valor booleano que indica si se deben ignorar las diferencias de acentos, diéresis, etc.</param>
    ''' <returns>Cadena para operador LIKE</returns>
    Function strToSQLLIKE(ByVal Cadena As String, Optional ByVal IgnorarAcentos As Boolean = False, Optional IgnorarCaracteresEspeciales As Boolean = True) As String
        If IgnorarCaracteresEspeciales Then
            Cadena = Replace(Cadena, "[", "[[]")
            Cadena = Replace(Cadena, "_", "[_]")
            Cadena = Replace(Cadena, "%", "[%]")
            Cadena = Replace(Cadena, "'", "''''")
        End If
        Cadena = UCase(Cadena)
        If IgnorarAcentos Then
            Cadena = Replace(Cadena, "A", "[AÁÀÄÂ]")
            Cadena = Replace(Cadena, "E", "[EÉÈËÊ]")
            Cadena = Replace(Cadena, "I", "[IÍÌÏÎ]")
            Cadena = Replace(Cadena, "O", "[OÓÒÖÔ]")
            Cadena = Replace(Cadena, "U", "[UÚÙÜÛ]")
        End If
        Return Cadena
    End Function
    ''' <summary>
    ''' Evita que se produzca un error por causa de un bug del Framework.
    ''' A causa del bug al añadir un atributo de estilo con índice 20 se genera una excepción IndexOutOfRangeException.
    ''' </summary>
    ''' <param name="writer">Objeto HtmlTextWriter</param>
    ''' <param name="Atributo">Atributo a escribir</param>
    ''' <param name="Valor">Valor del atributo</param>
    Public Sub AddAtributoEstilo(ByVal writer As Web.UI.HtmlTextWriter, ByVal Atributo As Web.UI.HtmlTextWriterStyle, ByVal Valor As String)
        Try
            writer.AddStyleAttribute(Atributo, Valor)
        Catch ex As IndexOutOfRangeException
            Dim count As Reflection.FieldInfo = writer.GetType().GetField("_styleCount", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic)
            Dim i As Integer = count.GetValue(writer)
            count.SetValue(writer, i + 1)
            writer.AddStyleAttribute(Atributo, Valor)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Evita que se produzca un error por causa de un bug del Framework.
    ''' A causa del bug al añadir un atributo de estilo con índice 20 se genera una excepción IndexOutOfRangeException.
    ''' </summary>
    ''' <param name="writer">Objeto HtmlTextWriter</param>
    ''' <param name="Atributo">Atributo a escribir</param>
    ''' <param name="Valor">Valor del atributo</param>
    Public Sub AddAtributoEstilo(ByVal writer As Web.UI.HtmlTextWriter, ByVal Atributo As String, ByVal Valor As String)
        Try
            writer.AddStyleAttribute(Atributo, Valor)
        Catch ex As IndexOutOfRangeException
            Dim count As Reflection.FieldInfo = writer.GetType().GetField("_styleCount", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic)
            Dim i As Integer = count.GetValue(writer)
            count.SetValue(writer, i + 1)
            writer.AddStyleAttribute(Atributo, Valor)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Comprueba que la cadena recibida es un código de idioma válido
    ''' </summary>
    ''' <param name="Idioma">Código de idioma</param>
    ''' <returns>El código de idioma si es válido, sino una cadena vacía</returns>
    ''' <remarks>Debe utilizarse la función antes de construir consultas SQL dinámicamente 
    ''' utilizando el código de idioma para generar los nombres de los campos</remarks>
    Public Function ComprobarIdioma(ByVal Idioma As String) As String
        Select Case Idioma
            Case "SPA", "ENG", "GER", "FRA"
                Return Idioma
            Case Else
                Return String.Empty
        End Select
    End Function
    ''' <summary>
    ''' Encuentra, si lo hay, el elemento separador en un formato de fecha y lo devuelve
    ''' Si no lo hay, devuelve un string vacío
    ''' </summary>
    ''' <param name="formatoFecha">Formate de fecha en el que se va a buscar </param>
    ''' <returns>El separador de fecha o un string vacío si no lo hay</returns>
    ''' <remarks>Llamada desde: FSNServer\User.vb\LoadUserData. Tiempo máx. 0 seg</remarks>
    Public Function encontrarSeparadorFecha(ByVal formatoFecha As String) As String
        If String.IsNullOrEmpty(formatoFecha) Then
            Return ""
        Else
            Dim sSeparadorFecha As String = System.Text.RegularExpressions.Regex.Replace(formatoFecha, "[\w]", "")
            If sSeparadorFecha.Length > 1 Then
                sSeparadorFecha = Left(sSeparadorFecha, 1)
            End If
            Return sSeparadorFecha
        End If
    End Function
    ''' <summary>
    ''' Función que devuelve el número de decimales de 
    ''' </summary>
    ''' <param name="numero">valor del que se contarán los decimales</param>
    ''' <returns>número de decimales del valor recibido</returns>
    ''' <remarks>llamada desde EPweb: Favoritos.aspx.vb</remarks>
    Public Function NumeroDeDecimales(ByVal numero As String) As Integer
        Dim numeroDecimales As Integer = 0
        If numero.IndexOf(".") > 0 Then
            numeroDecimales = numero.ToString().Substring(numero.ToString().IndexOf(".") + 1).Length
        ElseIf numero.IndexOf(",") > 0 Then
            numeroDecimales = numero.ToString().Substring(numero.ToString().IndexOf(",") + 1).Length
        Else
            numeroDecimales = 0
        End If
        Return numeroDecimales
    End Function
    ''' <summary>
    ''' Función que acorta un texto y le coloca puntos suspensivos al final.
    ''' </summary>
    ''' <param name="cadena">Texto a acortar</param>
    ''' <param name="numCaracteres">Número de caracteres a mostrar. Este número debe ser mayor que 3</param>
    ''' <returns>
    ''' El texto recortado y con tres puntos al final salvo cuando:
    ''' El texto sea de una longitud inferior al número de caracteres al que se quiere acortar,
    ''' o cuando el número de caracteres sea inferior a 3 (el mínimo de longitud de un texto recortado debe ser mayor que 3,
    ''' dado que tiene que tener parte del texto original y 3 puntos suspensivos).
    ''' En esos casos se devolverá el texto sin acortar.
    ''' </returns>
    ''' <remarks>Llamado desde: todas las páginas que deseen hacer uso de la función. Tiempo máx: inferior a 0,1 seg</remarks>
    Public Function AcortarTexto(ByVal cadena As String, ByVal numCaracteres As Integer) As String
        Dim texto As String = DBNullToStr(cadena)
        If numCaracteres > 3 AndAlso texto.Length > numCaracteres Then
            texto = texto.Substring(0, numCaracteres - 3) & "..."
        End If
        Return texto
    End Function
    ''' <summary>
    ''' Función que recibe un objeto ienumerable de cualquier tipo y devuelve un datatable con sus datos.
    ''' </summary>
    ''' <param name="ien">elemento IEnumerable a convertir</param>
    ''' <returns>
    ''' Un datatable con los datos que contenía el IEnumerable y sus mismos tipos de datos
    ''' </returns>
    ''' <remarks>Llamado desde: todas las páginas que deseen hacer uso de la función. Tiempo máx: inferior a 0,1 seg</remarks>
    Public Function ObtainDataTableFromIEnumerable(ByVal ien As IEnumerable) As DataTable
        Dim dt As New DataTable()
        For Each obj As Object In ien
            Dim t As Type = obj.[GetType]()
            Dim pis As System.Reflection.PropertyInfo() = t.GetProperties()
            If dt.Columns.Count = 0 Then
                For Each pi As System.Reflection.PropertyInfo In pis
                    dt.Columns.Add(pi.Name, pi.PropertyType)
                Next
            End If
            Dim dr As DataRow = dt.NewRow()
            For Each pi As System.Reflection.PropertyInfo In pis
                Dim value As Object = pi.GetValue(obj, Nothing)
                dr(pi.Name) = value
            Next
            dt.Rows.Add(dr)
        Next
        Return dt
    End Function
    ''' <summary>
    ''' Comprime los ficheros del directorio en un Zip
    ''' </summary>
    ''' <param name="directorio">directorio a comprimir</param>
    ''' <param name="filtro">filtro para seleccionar los ficheros del directorio</param>
    ''' <param name="zipFic">Nombre del fichero zip</param>
    ''' <param name="crearAuto">crea el nombre automaticamente si esta a true</param>
    ''' <param name="guardarPathCompleto">guarda el path completo del fichero si esta a true</param>
    ''' <returns>retorna el nombre del zip creado</returns>
    ''' <remarks></remarks>
    ''' <revisado>JVS 27/06/2011</revisado>
    Public Function Comprimir(ByVal directorio As String, _
                         ByVal filtro As String, _
                         ByVal zipFic As String, _
                         Optional ByVal crearAuto As Boolean = True, _
                         Optional ByVal guardarPathCompleto As Boolean = True) As String
        Dim fileNames(0) As String
        If directorio = "" Then
            fileNames(0) = filtro
        Else
            fileNames = System.IO.Directory.GetFiles(directorio, filtro)

        End If

        ' llamar a la version sobrecargada que recibe un array
        Return Comprimir(fileNames, zipFic, crearAuto, guardarPathCompleto)
    End Function
    ''' <summary>
    ''' Funcion que recibe un array de ficheros y los comprime
    ''' </summary>
    ''' <param name="fileNames"> arrya de ficheros</param>
    ''' <param name="zipFic">Nombre del zip</param>
    ''' <param name="crearAuto">crea el nombre automaticamente si esta a true</param>
    ''' <param name="guardarPathCompleto">guarda el path completo del fichero si esta a true</param>
    ''' <returns>retorna el nombre del zip creado</returns>
    ''' <remarks></remarks>
    Public Function Comprimir(ByVal fileNames() As String, _
                         ByVal zipFic As String, _
                         Optional ByVal crearAuto As Boolean = False, _
                         Optional ByVal guardarPathCompleto As Boolean = True) As String
        ' comprimir los ficheros del array en el zip indicado
        ' si crearAuto = True, zipfile será el directorio en el que se guardará
        ' y se generará automáticamente el nombre con la fecha y hora actual
        Dim objCrc32 As New Crc32()
        Dim strmZipOutputStream As ICSharpCode.SharpZipLib.Zip.ZipOutputStream
        Dim NombreZip As String = String.Empty
        '
        If zipFic = "" Then
            zipFic = "."
            crearAuto = True
        End If
        If crearAuto Then
            ' si hay que crear el nombre del fichero
            ' Ã©ste será el path indicado y la fecha actual
            NombreZip = "\ZIP" & DateTime.Now.ToString("yyMMddHHmmss") & ".zip"
            zipFic &= NombreZip
        Else
            NombreZip = zipFic
        End If
        strmZipOutputStream = New ZipOutputStream(System.IO.File.Create(zipFic))
        ' Compression Level: 0-9
        ' 0: no(Compression)
        ' 9: maximum compression
        strmZipOutputStream.SetLevel(6)
        '
        Dim strFile As String
        For Each strFile In fileNames
            Dim strmFile As System.IO.FileStream = System.IO.File.OpenRead(strFile)
            Dim abyBuffer(Convert.ToInt32(strmFile.Length - 1)) As Byte
            '
            strmFile.Read(abyBuffer, 0, abyBuffer.Length)
            '
            '------------------------------------------------------------------
            ' para guardar sin el primer path
            'Dim sFile As String = strFile
            'Dim i As Integer = sFile.IndexOf("\")
            'If i > -1 Then
            '    sFile = sFile.Substring(i + 1).TrimStart
            'End If
            '------------------------------------------------------------------
            ' para guardar sólo el nombre del fichero
            ' esto sólo se debe hacer si no se procesan directorios
            ' que puedan contener nombres repetidos
            'Dim sFile As String = Path.GetFileName(strFile)
            'Dim theEntry As ZipEntry = New ZipEntry(sFile)
            '------------------------------------------------------------------
            ' se guarda con el path completo o no, dependiendo de la variable 
            Dim theEntry As ZipEntry
            If Not guardarPathCompleto Then
                Dim sFile As String = Path.GetFileName(strFile)
                theEntry = New ZipEntry(sFile)
            Else
                theEntry = New ZipEntry(strFile)
            End If
            ' guardar la fecha y hora de la Ãºltima modificación
            Dim fi As New System.IO.FileInfo(strFile)
            theEntry.DateTime = fi.LastWriteTime
            'theEntry.DateTime = DateTime.Now
            '
            theEntry.Size = strmFile.Length
            strmFile.Close()
            objCrc32.Reset()
            objCrc32.Update(abyBuffer)
            theEntry.Crc = objCrc32.Value
            strmZipOutputStream.PutNextEntry(theEntry)
            strmZipOutputStream.Write(abyBuffer, 0, abyBuffer.Length)
        Next
        strmZipOutputStream.Finish()
        Dim TamanoFichero As String = strmZipOutputStream.Length
        strmZipOutputStream.Close()

        Return NombreZip.Substring(1) & "##" & TamanoFichero
    End Function
    ''' <summary>
    ''' Para poder trabajar con FILESTREAM necesitamos suplantacion de identidad, es decir, necesitamos un usuario SQL que pueda
    ''' trabajar con este tipo de ficheros. Para ellos necesitamos que el usuario este creado con esos permisos en SQL
    ''' </summary>
    ''' <returns>Contexto de la suplantacion</returns>
    ''' <remarks>Llamada desde:GrabarNuevoAdjunto; Tiempo máximo:0seg.</remarks>
    Public Function RealizarSuplantacion(ByVal UserName As String, ByVal password As String, ByVal Domain As String) As System.Security.Principal.WindowsImpersonationContext
        Dim impersonationContext As System.Security.Principal.WindowsImpersonationContext = Nothing
        Dim impersonateValidUser As Boolean
        Dim tempWindowsIdentity As System.Security.Principal.WindowsIdentity
        Dim token As IntPtr = IntPtr.Zero
        Dim tokenDuplicate As IntPtr = IntPtr.Zero
        impersonateValidUser = False

        If RevertToSelf() Then
            If LogonUserA(UserName, Domain, password, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, token) <> 0 Then
                If DuplicateToken(token, 2, tokenDuplicate) <> 0 Then
                    tempWindowsIdentity = New System.Security.Principal.WindowsIdentity(tokenDuplicate)
                    impersonationContext = tempWindowsIdentity.Impersonate()
                    If Not impersonationContext Is Nothing Then
                        impersonateValidUser = True
                    End If
                End If
            End If
        End If
        If Not tokenDuplicate.Equals(IntPtr.Zero) Then
            CloseHandle(tokenDuplicate)
        End If
        If Not token.Equals(IntPtr.Zero) Then
            CloseHandle(token)
        End If

        Return impersonationContext
    End Function
    Public Function getMimeFromFile(ByVal file As String) As String
        Dim mimeout As String = ""
        Dim MaxContent As Integer
        Dim fs As FileStream
        Dim buf() As Byte
        Dim result As String

        If Not System.IO.File.Exists(file) Then Throw New FileNotFoundException(file + " not found")
        If MaxContent > 4096 Then MaxContent = 4096

        MaxContent = CInt(New FileInfo(file).Length)
        fs = New FileStream(file, FileMode.Open)
        ReDim buf(MaxContent)
        fs.Read(buf, 0, MaxContent)
        fs.Close()

        result = FindMimeFromData(IntPtr.Zero, file, buf, MaxContent, Nothing, 0, mimeout, 0)
        Return mimeout
    End Function

    Public Const AnchoDeTab As Double = 240 * 0.7

    ''' <summary>
    ''' Ajusta el texto dado a un ancho en pixeles añadiendo ... si el texto no entra
    ''' </summary>
    ''' <param name="sTextoParaAjustar">El texto para ajustar</param>
    ''' <param name="iPixeles">Pixeles máximo que ocupara el texto</param>
    ''' <param name="sFontName">La fuente en la que esta el texto</param>
    ''' <param name="iFontSize">El tamaño en el que esta el texto</param>
    ''' <param name="bEsNegrita">Si el texto esta en negrita</param>
    ''' <param name="sTextoAgregadoFinal">Si el texto es compuesto y solo queremos reducir una parte esta es la segunda parte que se añadira tras el ajuste</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function AjustarAnchoTextoPixels(ByVal sTextoParaAjustar As String, ByVal iPixeles As Integer,
                                              ByVal sFontName As String, ByVal iFontSize As Integer,
                                              Optional ByVal bEsNegrita As Boolean = False,
                                              Optional ByVal sTextoAgregadoFinal As String = Nothing) As String
        Dim f As Font
        f = New Font(sFontName, iFontSize, IIf(bEsNegrita, FontStyle.Bold, FontStyle.Regular),
                     GraphicsUnit.Pixel, 0)

        AjustarAnchoTextoPixels = sTextoParaAjustar & sTextoAgregadoFinal

        While Windows.Forms.TextRenderer.MeasureText(AjustarAnchoTextoPixels, f).Width > iPixeles
            sTextoParaAjustar = Left(sTextoParaAjustar, sTextoParaAjustar.Length - 1)
            AjustarAnchoTextoPixels = sTextoParaAjustar & "..." & sTextoAgregadoFinal
        End While

        f.Dispose()

        Return AjustarAnchoTextoPixels
    End Function
    ''' <summary>
    ''' Function que devuelve el texto ajustado para las cajas de texto de comentario de los adjuntos cuando superan las 2 lineas
    ''' </summary>
    ''' <param name="sTexto">Texto total del comentario</param>
    ''' <param name="CaracteresLinea">Caracteres por lines</param>
    ''' <param name="CaracteresTotal">Caracteres totales para las lineas a mostrar</param>
    ''' <param name="iLineas">lineas a mostrar</param>
    ''' <returns>Devuelve el texto que se podra mostrar</returns>
    ''' <remarks></remarks>
    Public Function DevolverTextoAjustado(ByVal sTexto As String, ByVal CaracteresLinea As Short, ByVal CaracteresTotal As Short, ByVal iLineas As Short) As String
        Dim Texto() As String
        Dim Caracteres As Short = 0
        Dim iLineasCargadas As Short = 0
        Dim LongTextoAnterior As Short = 0
        Dim sTextoDevuelto As String = ""

        Texto = Split(sTexto, " ")
        For i = 0 To Texto.GetUpperBound(0)
            Caracteres += IIf(Texto(i).Length > 0, Texto(i).Length + 1, 1)
            If Caracteres > (CaracteresLinea * (iLineasCargadas + 1)) Then
                iLineasCargadas += 1
                If iLineasCargadas = iLineas Then
                    For z = 0 To i - 1
                        Caracteres = 0
                        sTextoDevuelto = sTextoDevuelto & IIf(Texto(z).Length > 0, Texto(z) + " ", " ")
                        Caracteres += IIf(Texto(z).Length > 0, Texto(z).Length + 1, 1)
                    Next
                    Return sTextoDevuelto
                Else
                    Caracteres = CaracteresLinea + Texto(i).Length
                End If
            End If
        Next
        Return sTexto
    End Function
    ''' <summary>
    ''' Tratamos de convertir el objeto recibido a Boolean
    ''' </summary>
    ''' <param name="value">Valor a convertir a Boolean</param>
    ''' <returns>Valor convertido a Boolean. Si no es posible o el value es null o nothing, devuelve False</returns>
    ''' <remarks>Llamado desde toda la aplicación, donde se requiera su uso. Tiempo máx inferior a 0,1 seg</remarks>
    Public Function DBNullToBoolean(Optional ByVal value As Object = Nothing) As Boolean
        If IsDBNull(value) Then
            Return False
        ElseIf value Is Nothing Then
            Return False
        Else
            Return CType(value, Boolean)
        End If
    End Function
    ''' <summary>
    ''' Función basada en http://blogs.msdn.com/b/michkap/archive/2007/05/14/2629747.aspx
    ''' Para quitar de un string las tildes, espacios y caracteres reservados por Microsoft
    ''' </summary>
    ''' <param name="s">Texto a modificar</param>
    ''' <returns>Texto modificado</returns>
    ''' <remarks>Llamada desde Seguimiento.aspx.vb y Recepción.aspx.vb. Maximo 0,1 seg</remarks>
    Public Function ReemplazaTexto(ByVal s As String) As String
        Dim normalizedString As String = s.Normalize(System.Text.NormalizationForm.FormD)
        Dim stringBuilder As System.Text.StringBuilder = New System.Text.StringBuilder
        Dim i As Integer = 0
        Do While (i < normalizedString.Length)
            Dim c As Char = normalizedString(i)
            If (System.Globalization.CharUnicodeInfo.GetUnicodeCategory(c) <> System.Globalization.UnicodeCategory.NonSpacingMark) Then
                If CaracterReservadoMS(c) OrElse c = " " Then
                    stringBuilder.Append("_")
                Else
                    stringBuilder.Append(c)
                End If
            End If
            i = (i + 1)
        Loop
        Return stringBuilder.ToString.Normalize(System.Text.NormalizationForm.FormC)
    End Function
    ''' <summary>
    ''' Indica si el char pasado como parámetro es un carácter reservado por Windows o no (>,:,",/,\,|,?,*)
    ''' </summary>
    ''' <param name="i"></param>
    ''' <returns>True si el parámetro es reservado, False si no lo es</returns>
    ''' <remarks>Llamada desde ReemplazaTexto. Max. 0,1 seg</remarks>
    Public Function CaracterReservadoMS(ByVal i As Char) As Boolean
        Dim sReservados As String = ">:""/\|?*"
        If sReservados.Contains(i) Then
            Return True
        Else
            Return False
        End If
    End Function
    ''' <summary>
    ''' http://www.west-wind.com/weblog/posts/2007/Jul/14/Embedding-JavaScript-Strings-from-an-ASPNET-Page
    ''' Encodes a string to be represented as a string literal. The format
    ''' is essentially a JSON string.
    ''' The string returned includes outer quotes 
    ''' Example Output: "Hello \"Rick\"!\r\nRock on"
    ''' </summary>
    ''' <param name="s">Texto a formatear</param>
    ''' <returns>Texto formateado. Max 0,1</returns>
    Public Function EncodeJsString(s As String) As String
        Dim sb As New StringBuilder()
        sb.Append("""")
        For Each c As Char In s
            Select Case c
                Case """"c
                    sb.Append("\""")
                    Exit Select
                Case "\"c
                    sb.Append("\\")
                    Exit Select
                Case ControlChars.Back
                    sb.Append("\b")
                    Exit Select
                Case ControlChars.FormFeed
                    sb.Append("\f")
                    Exit Select
                Case ControlChars.Lf
                    sb.Append("\n")
                    Exit Select
                Case ControlChars.Cr
                    sb.Append("\r")
                    Exit Select
                Case ControlChars.Tab
                    sb.Append("\t")
                    Exit Select
                Case Else
                    Dim i As Integer = AscW(c)
                    If i < 32 OrElse i > 127 Then
                        sb.AppendFormat("\u{0:X04}", i)
                    Else
                        sb.Append(c)
                    End If
                    Exit Select
            End Select
        Next
        sb.Append("""")

        Return sb.ToString()
    End Function
    ''' <summary>
    ''' Convierte un fichero HTML en otro con formato PDF
    ''' </summary>
    ''' <param name="sPathHTML">Path y nombre del fichero HTML</param>
    ''' <param name="sPathPDF">Path donde se va a dejar y nombre</param>
    ''' <remarks>LLamada desde: AsignarOBJySuelos.aspx, mantenimientoMat.aspx, UnidadesNegocio.aspx, detallepedidovis.aspx, detalleprocesovis.aspx,	impexp.aspx, flowDiagramExport.aspx; Tiempo máximo: 0,1 sg.</remarks>
    Sub ConvertHTMLToPDF(ByVal sPathHTML As String, ByVal sPathPDF As String)
        'Pasamos el HTML a PDF utilizando el componente ExpertPdf (ephtmltopdf.dll)
        Dim pdfConverter As ExpertPdf.HtmlToPdf.PdfConverter = New PdfConverter()
        Dim pdfDocument As PdfDocument.Document

        pdfConverter.LicenseKey = "HzQtPyc/LCYvPygxLz8sLjEuLTEmJiYm"    ''Le pasamos la clave de licencia
        pdfConverter.PdfDocumentOptions.LeftMargin = 15
        pdfConverter.PdfDocumentOptions.RightMargin = 15
        pdfConverter.PdfDocumentOptions.TopMargin = 15
        pdfConverter.PdfDocumentOptions.BottomMargin = 15

        pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlFile(sPathHTML)
        pdfDocument.Save(sPathPDF)
        pdfDocument.Close()
    End Sub
#Region "XSS"
    ''' <summary>
    ''' Controlar el cross-site scripting (XSS) con una lista negra de codigo javascript
    ''' </summary>
    ''' <param name="strRequest">string a controlar</param>      
    ''' <returns>0 hay XSS / 1 no hay XSS</returns>
    ''' <remarks>Llamada desde:ControlarXSS; Tiempo máximo: 0,1</remarks>
    Public Function CtrlPosibleXSS(ByVal strRequest As String) As Boolean
        Dim i As Integer
        Dim ABuscar(99) As String

        If strRequest = "" Then
        Else
            ABuscar(1) = "javascript"
            ABuscar(2) = "vbscript"
            ABuscar(3) = "expression"
            ABuscar(4) = "applet"
            ABuscar(5) = "meta"
            ABuscar(6) = "xml"
            ABuscar(7) = "blink"
            ABuscar(8) = "link"
            ABuscar(9) = "style"
            ABuscar(10) = "script"
            ABuscar(11) = "embed"
            ABuscar(12) = "object"
            ABuscar(13) = "iframe"
            ABuscar(14) = "frame"
            ABuscar(15) = "frameset"
            ABuscar(16) = "ilayer"
            ABuscar(17) = "layer"
            ABuscar(18) = "bgsound"
            ABuscar(19) = "title"
            ABuscar(20) = "base"
            ABuscar(21) = "onabort"
            ABuscar(22) = "onactivate"
            ABuscar(23) = "onafterprint"
            ABuscar(24) = "onafterupdate"
            ABuscar(25) = "onbeforeactivate"
            ABuscar(26) = "onbeforecopy"
            ABuscar(27) = "onbeforecut"
            ABuscar(28) = "onbeforedeactivate"
            ABuscar(29) = "onbeforeeditfocus"
            ABuscar(30) = "onbeforepaste"
            ABuscar(31) = "onbeforeprint"
            ABuscar(32) = "onbeforeunload"
            ABuscar(33) = "onbeforeupdate"
            ABuscar(34) = "onblur"
            ABuscar(35) = "onbounce"
            ABuscar(36) = "oncellchange"
            ABuscar(37) = "onchange"
            ABuscar(38) = "onclick"
            ABuscar(39) = "oncontextmenu"
            ABuscar(40) = "oncontrolselect"
            ABuscar(41) = "oncopy"
            ABuscar(42) = "oncut"
            ABuscar(43) = "ondataavailable"
            ABuscar(44) = "ondatasetchanged"
            ABuscar(45) = "ondatasetcomplete"
            ABuscar(46) = "ondblclick"
            ABuscar(47) = "ondeactivate"
            ABuscar(48) = "ondrag"
            ABuscar(49) = "ondragend"
            ABuscar(50) = "ondragenter"
            ABuscar(51) = "ondragleave"
            ABuscar(52) = "ondragover"
            ABuscar(53) = "ondragstart"
            ABuscar(54) = "ondrop"
            ABuscar(55) = "onerror"
            ABuscar(56) = "onerrorupdate"
            ABuscar(57) = "onfilterchange"
            ABuscar(58) = "onfinish"
            ABuscar(59) = "onfocus"
            ABuscar(60) = "onfocusin"
            ABuscar(61) = "onfocusout"
            ABuscar(62) = "onhelp"
            ABuscar(63) = "onkeydown"
            ABuscar(64) = "onkeypress"
            ABuscar(65) = "onkeyup"
            ABuscar(66) = "onlayoutcomplete"
            ABuscar(67) = "onload"
            ABuscar(68) = "onlosecapture"
            ABuscar(69) = "onmousedown"
            ABuscar(70) = "onmouseenter"
            ABuscar(71) = "onmouseleave"
            ABuscar(72) = "onmousemove"
            ABuscar(73) = "onmouseout"
            ABuscar(74) = "onmouseover"
            ABuscar(75) = "onmouseup"
            ABuscar(76) = "onmousewheel"
            ABuscar(77) = "onmove"
            ABuscar(78) = "onmoveend"
            ABuscar(79) = "onmovestart"
            ABuscar(80) = "onpaste"
            ABuscar(81) = "onpropertychange"
            ABuscar(82) = "onreadystatechange"
            ABuscar(83) = "onreset"
            ABuscar(84) = "onresize"
            ABuscar(85) = "onresizeend"
            ABuscar(86) = "onresizestart"
            ABuscar(87) = "onrowenter"
            ABuscar(88) = "onrowexit"
            ABuscar(89) = "onrowsdelete"
            ABuscar(90) = "onrowsinserted"
            ABuscar(91) = "onscroll"
            ABuscar(92) = "onselect"
            ABuscar(93) = "onselectionchange"
            ABuscar(94) = "onselectstart"
            ABuscar(95) = "onstart"
            ABuscar(96) = "onstop"
            ABuscar(97) = "onsubmit"
            ABuscar(98) = "onunload"
            ABuscar(99) = "alert"

            For i = 1 To 20
                If CtrlPosibleEntrada(UCase(strRequest), UCase("<" & ABuscar(i))) = 0 Then
                    CtrlPosibleXSS = True
                    Exit Function
                End If
            Next

            For i = 21 To 99
                If CtrlPosibleEntrada(UCase(strRequest), UCase(ABuscar(i))) = 0 Then
                    CtrlPosibleXSS = True
                    Exit Function
                End If
            Next
        End If

        CtrlPosibleXSS = False
    End Function
    ''' <summary>
    ''' Trasformar el strRequest quitandole los espacios
    ''' </summary>
    ''' <param name="strRequest">string a controlar</param>          
    ''' <returns>strRequest sin espacios</returns>
    ''' <remarks>Llamada desde:CtrlPosible; Tiempo máximo:0</remarks>
    Private Function QuitaEspacios(strRequest)
        Dim sAux
        Dim i
        Dim NumCar
        Dim car
        Dim asci3
        Dim asci4

        sAux = ""
        NumCar = Len(strRequest)
        i = 1

        While i < NumCar + 1
            car = Mid(strRequest, i, 1)
            asci3 = UCase(Mid(strRequest, i, 3))
            asci4 = UCase(Mid(strRequest, i, 4))

            If car = " " Then
                i = i + 1
            ElseIf asci3 = "%32" Then
                i = i + 3
            ElseIf asci4 = "\X20" Then
                i = i + 4
            Else
                sAux = sAux & car

                i = i + 1
            End If
        End While

        QuitaEspacios = sAux
    End Function
    ''' <summary>
    ''' Trasformar en strRequest los caracteres q me vinieran en decimal o hexadecimal en codigo ascii
    ''' </summary>
    ''' <param name="strRequest">string a controlar</param>          
    ''' <returns>strRequest con los caracteres convertidos a ascii</returns>
    ''' <remarks>Llamada desde:CtrlPosible; Tiempo máximo:0</remarks>
    Private Function QuitaAsciiHex(strRequest)
        Dim i
        Dim j
        Dim k
        Dim sAux
        Dim car
        Dim asci3
        Dim asci4
        Dim c_asci3
        Dim c_asci4
        Dim NumCar
        Dim encontrado

        sAux = ""
        NumCar = Len(strRequest)
        i = 1

        While i < NumCar + 1
            car = Mid(strRequest, i, 1)
            asci3 = UCase(Mid(strRequest, i, 3))
            asci4 = UCase(Mid(strRequest, i, 4))

            If car = "%" Or car = "\" Then
                encontrado = False
                'mayusculas
                k = 41
                For j = 65 To 90
                    c_asci3 = "%" & CStr(j)

                    Select Case j
                        Case 74
                            c_asci4 = "\X4A"
                        Case 75
                            c_asci4 = "\X4B"
                        Case 76
                            c_asci4 = "\X4C"
                        Case 77
                            c_asci4 = "\X4D"
                        Case 78
                            c_asci4 = "\X4E"
                        Case 79
                            c_asci4 = "\X4F"
                        Case 90
                            c_asci4 = "\X5A"
                        Case Else
                            c_asci4 = "\X" & CStr(k)
                            k = k + 1
                    End Select

                    If asci3 = c_asci3 Then
                        sAux = sAux & Chr(j)

                        i = i + 2
                        encontrado = True

                        Exit For
                    ElseIf asci4 = c_asci4 Then
                        sAux = sAux & Chr(j)

                        i = i + 3
                        encontrado = True
                        Exit For
                    End If
                Next
                'minusculas
                k = 61
                For j = 97 To 122
                    c_asci3 = "%" & CStr(j)

                    Select Case j
                        Case 106
                            c_asci4 = "\X6A"
                        Case 107
                            c_asci4 = "\X6B"
                        Case 108
                            c_asci4 = "\X6C"
                        Case 109
                            c_asci4 = "\X6D"
                        Case 110
                            c_asci4 = "\X6E"
                        Case 111
                            c_asci4 = "\X6F"
                        Case 122
                            c_asci4 = "\X7A"
                        Case Else
                            c_asci4 = "\X" & CStr(k)
                            k = k + 1
                    End Select

                    If asci3 = c_asci3 Then
                        sAux = sAux & UCase(Chr(j))

                        i = i + 2
                        encontrado = True

                        Exit For
                    ElseIf asci4 = c_asci4 Then
                        sAux = sAux & UCase(Chr(j))

                        i = i + 3
                        encontrado = True
                        Exit For
                    End If
                Next

                If encontrado = False Then
                    sAux = sAux & car
                End If
            Else
                sAux = sAux & car
            End If
            i = i + 1
        End While

        QuitaAsciiHex = sAux
    End Function
    ''' <summary>
    ''' Controlar el cross-site scripting (XSS) contra una entrada de la lista negra javascript
    ''' </summary>
    ''' <param name="strRequest">string a controlar</param>      
    ''' <param name="strABuscar">string lista negra</param>   
    ''' <returns>0 hay XSS / 1 no hay XSS</returns>
    ''' <remarks>Llamada desde: CtrlPosible; Tiempo máximo:0,1</remarks>
    Private Function CtrlPosibleEntrada(strRequest, strABuscar)
        Dim j
        Dim k
        Dim i
        Dim car_ABuscar
        Dim car_strRequest
        Dim carSiguiente_strRequest
        Dim esBueno
        Dim bEsEvento ' Me dice si el string de la lista negra es un evento

        bEsEvento = False

        car_ABuscar = Mid(strABuscar, 1, 1)

        If car_ABuscar <> "<" Then
            bEsEvento = True
        End If

        For j = 1 To Len(strRequest)
            car_strRequest = Mid(strRequest, j, 1)

            If car_ABuscar = car_strRequest Then
                i = j + 1
                esBueno = False

                For k = 2 To Len(strABuscar)
                    If Mid(strRequest, i, 1) = Mid(strABuscar, k, 1) Then
                        i = i + 1
                    Else
                        esBueno = True
                        Exit For
                    End If
                Next

                If esBueno = False Then
                    If bEsEvento Then ' Si se trata de un evento se calcula el carácter siguiente al de la entrada y se da ésta por válida si el carácter es letra o número
                        carSiguiente_strRequest = Mid(strRequest, i, 1)
                        'If RegExpTest("[0-9A-ZÀ-Ü]", carSiguiente_strRequest) Then
                        If IsNumeric(carSiguiente_strRequest) _
                        OrElse (Asc(LCase(carSiguiente_strRequest)) >= Asc("a") AndAlso Asc(LCase(carSiguiente_strRequest)) <= Asc("z")) _
                        OrElse (Asc(LCase(carSiguiente_strRequest)) >= Asc("à") AndAlso Asc(LCase(carSiguiente_strRequest)) <= Asc("ü")) Then
                            CtrlPosibleEntrada = 1
                            Exit Function
                        End If
                    End If
                    CtrlPosibleEntrada = 0
                    Exit Function
                End If
            End If
        Next

        CtrlPosibleEntrada = 1
    End Function
#End Region
    Public Sub subDelay(ByVal sngDelay As Single)
        Dim sngStart As DateTime             ' Start time.
        Dim sngStop As DateTime              ' Stop time.
        Dim sngNow As DateTime               ' Current time.

        sngStart = System.DateTime.Now ' Get current timer.
        sngStop = DateAdd(DateInterval.Second, sngDelay, sngStart)    ' Set up stop time based on
        ' delay.
        While sngNow < sngStop
            sngNow = System.DateTime.Now ' Get current timer again.
        End While        ' Has time elapsed?
    End Sub
    Public Function RandomNumber(ByVal MaxNumber As Integer, Optional ByVal MinNumber As Integer = 0) As Integer
        ' initialize random number generator
        Dim r As New Random(System.DateTime.Now.Millisecond)

        'if passed incorrect arguments, swap them
        'can also throw exception or return 0
        If MinNumber > MaxNumber Then
            Dim t As Integer = MinNumber
            MinNumber = MaxNumber
            MaxNumber = t
        End If

        Return r.Next(MinNumber, MaxNumber)
    End Function
    Function GenerarDataTableAcciones() As DataTable
        Dim dt As DataTable
        Dim dc As DataColumn
        dt = New DataTable("ACCIONES")
        dc = New DataColumn("ID")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        dc = New DataColumn("IMAGEN")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        dc = New DataColumn("TEXTO")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        dc = New DataColumn("JAVASCRIPTEVENT")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        dc = New DataColumn("TITULO")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        dc = New DataColumn("WIDTH")
        dc.DataType = System.Type.GetType("System.Double")
        dt.Columns.Add(dc)
        Return dt
    End Function
    Function GetTipoVisorFromTipoSolicitud(ByVal iTipoSolicitud As TiposDeDatos.TipoDeSolicitud)
        Select Case iTipoSolicitud
            Case TiposDeDatos.TipoDeSolicitud.Certificado
                Return TipoVisor.Certificados
            Case TiposDeDatos.TipoDeSolicitud.NoConformidad
                Return TipoVisor.NoConformidades
            Case TiposDeDatos.TipoDeSolicitud.Factura
                Return TipoVisor.Facturas
            Case TiposDeDatos.TipoDeSolicitud.Encuesta
                Return TipoVisor.Encuestas
            Case TiposDeDatos.TipoDeSolicitud.Contrato
                Return TipoVisor.Contratos
            Case TiposDeDatos.TipoDeSolicitud.SolicitudQA
                Return TipoVisor.SolicitudesQA
            Case Else
                Return TipoVisor.Solicitudes
        End Select
    End Function
End Module


