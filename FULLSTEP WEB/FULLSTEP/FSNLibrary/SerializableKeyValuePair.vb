﻿''' <summary>
''' Clase auxiliar que permite deserializar correctamente un objeto con estructura de KeyValuePair
''' </summary>
''' <typeparam name="T">Tipo asociado a la clave</typeparam>
''' <typeparam name="U">Tipo asociado al valor</typeparam>
''' <remarks></remarks>
Public Class SerializableKeyValuePair(Of T, U)
    Private _key As T
    Public Property Key() As T
        Get
            Return _key
        End Get
        Set(ByVal value As T)
            _key = value
        End Set
    End Property

    Private _value As U
    Public Property Value() As U
        Get
            Return _value
        End Get
        Set(ByVal value As U)
            _value = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(key As T, value As U)
        _key = key
        _value = value
    End Sub
End Class
