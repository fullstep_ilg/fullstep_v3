﻿Imports System.Threading
Imports System.Collections.Concurrent

Public Class SemaphoreSlimFIFO
    Private semaphore As SemaphoreSlim
    Private queue As New ConcurrentQueue(Of TaskCompletionSource(Of Boolean))()

    Public Sub New(initialCount As Integer, maxCount As Integer)
        semaphore = New SemaphoreSlim(initialCount, maxCount)
    End Sub
    Public Sub Wait()
        WaitAsync().Wait()
    End Sub
    Public Function WaitAsync() As Task
        Dim tcs = New TaskCompletionSource(Of Boolean)()
        queue.Enqueue(tcs)

        semaphore.WaitAsync().ContinueWith(Sub(t)
                                               Dim popped As TaskCompletionSource(Of Boolean) = Nothing
                                               If queue.TryDequeue(popped) Then popped.SetResult(True)
                                           End Sub)
        Return tcs.Task
    End Function
    Public Sub Release()
        semaphore.Release()
    End Sub
End Class