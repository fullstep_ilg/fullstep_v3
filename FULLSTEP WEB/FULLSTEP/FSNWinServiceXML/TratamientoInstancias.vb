﻿Imports System.Configuration
Imports System.Threading
Imports System.IO
Imports Fullstep.FSNLibrary
Imports Fullstep.FSNServer
Public Class TratamientoInstancias
#Region "Properties"
    Private directorioEnEspera, directorioEnTratamiento, directorioError, directorioBackup As DirectoryInfo
    Private FileSystemEventHandler_XMLEnEspera_Rename As FileSystemWatcher
    Private Shared poolSemaforo As SemaphoreSlimFIFO
    Private listaProcesamientoXML As New List(Of Long)
    Private colaXMLEnEspera As New Dictionary(Of Long, Queue(Of String))
    Private colaSemaphore As New Queue(Of String)
    Private _FSNServer As Root
    Public Property FSNServer() As Root
        Get
            If _FSNServer Is Nothing Then _FSNServer = New Root(True)
            Return _FSNServer
        End Get
        Set(ByVal value As Root)
            _FSNServer = value
        End Set
    End Property
    Private Enum EstadoProcesamiento
        InicioDisco = 1
        InicioBD = 2
        IniValidaciones = 3
        IniMapper = 4
        Fin = 5
    End Enum
    Private TratamientosSimultaneosInicial As Integer
    Private MaximoTratamientosSimultaneos As Integer
#End Region
#If DEBUG Then
    Public Sub EmpezarDebug()
        Me.OnStart(Nothing)
    End Sub
#End If
    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            'Handler para errores no controlados (no evita que el servicio se pare)
            AddHandler AppDomain.CurrentDomain.UnhandledException, AddressOf UnhandledExceptionHandler

            LecturaParametrosServicio()

            poolSemaforo = New SemaphoreSlimFIFO(TratamientosSimultaneosInicial, MaximoTratamientosSimultaneos)

            'Para los posibles xml que se han quedado a la espera en la anterior ejecucion del servicio,
            'comprobamos si pasarlos a tratamiento
            GestionarFicherosExistentes()

            'Definimos un FileSystemWatcher para detectar que se ha creado un xml a la carpeta de espera 
            'y poder gestionarlo en la funcion correspondiente
            FileSystemEventHandler_XMLEnEspera_Rename = New FileSystemWatcher
            With FileSystemEventHandler_XMLEnEspera_Rename
                .Path = directorioEnEspera.FullName
                .NotifyFilter = NotifyFilters.FileName 'Info que obtendremos del fichero que lanza el watcher
                .Filter = "*.xml" 'Filtrar por este tipo de fichero
            End With
            AddHandler FileSystemEventHandler_XMLEnEspera_Rename.Renamed, AddressOf OnRenamed
            ' Begin watching.
            FileSystemEventHandler_XMLEnEspera_Rename.EnableRaisingEvents = True
        Catch ex As Exception
            Dim sError As String = ex.Message
            Dim oErrores As Errores
            oErrores = FSNServer.Get_Object(GetType(Errores))
            Try
                oErrores.Create("FSNTratamientoInstancias\OnStart", "", ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
                oErrores = Nothing
            Catch exError As Exception
                oErrores.SendErrorEmail(sError & " / " & exError.Message, ConfigurationManager.AppSettings("NombreServicio"), "OnStart", "", "", AppDomain.CurrentDomain.BaseDirectory & "\FromInfo.txt")
            End Try
        End Try
    End Sub
    ''' <summary>Handlre para excepciones no controladas. No evita que el servicio se pare</summary>
    ''' <param name="sender"></param>
    ''' <param name="args"></param>
    Private Sub UnhandledExceptionHandler(sender As Object, args As UnhandledExceptionEventArgs)
        Dim ex As Exception = DirectCast(args.ExceptionObject, Exception)
        Dim oErrores As Errores
        oErrores = FSNServer.Get_Object(GetType(Errores))
        Try
            oErrores.Create("FSNTratamientoInstancias\UnhandledExceptionHandler", "", ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
            oErrores = Nothing
        Catch exError As Exception
            oErrores.SendErrorEmail(ex.Message & " / " & exError.Message, "UnhandledExceptionHandler", ConfigurationManager.AppSettings("NombreServicio"), "", "", AppDomain.CurrentDomain.BaseDirectory & "\FromInfo.txt")
        End Try
    End Sub

    Private Sub LecturaParametrosServicio()
        directorioEnEspera = New DirectoryInfo(ConfigurationManager.AppSettings("rutaXMLEnEspera"))
        directorioEnTratamiento = New DirectoryInfo(ConfigurationManager.AppSettings("rutaXMLEnTratamiento"))
        directorioError = New DirectoryInfo(ConfigurationManager.AppSettings("rutaXMLError"))
        directorioBackup = New DirectoryInfo(ConfigurationManager.AppSettings("rutaXMLBackup"))

        TratamientosSimultaneosInicial = CType(ConfigurationManager.AppSettings("TratamientosSimultaneosInicial"), Integer)
        MaximoTratamientosSimultaneos = CType(ConfigurationManager.AppSettings("MaximoTratamientosSimultaneos"), Integer)
    End Sub
    Private Sub GestionarFicherosExistentes()
        'Primero veremos si queda algo en la carpeta en tratamiento
        Dim oXMLDoc As New System.Xml.XmlDocument
        Dim dsXML As DataSet
        Dim lIDTiempoProc As Long
        Dim oInstancia As Instancia = FSNServer.Get_Object(GetType(Instancia))
        For Each xmlFile As FileInfo In New DirectoryInfo(directorioEnTratamiento.FullName).GetFiles()
            Try
                Dim xmlFileEnTratamiento As String = xmlFile.Name
                oXMLDoc.Load(directorioEnTratamiento.FullName & "\" & xmlFile.Name)
                dsXML = New DataSet
                dsXML.ReadXml(New Xml.XmlNodeReader(oXMLDoc))
                lIDTiempoProc = strToLong(dsXML.Tables("SOLICITUD").Rows(0)("IDTIEMPOPROC"))
                Select Case oInstancia.Obtener_Info_Procesamiento(lIDTiempoProc)
                    Case TipoErrorProcesamiento.SinIniciarProceso
                        ComprobarXMLEnEspera(xmlFileEnTratamiento)
                    Case TipoErrorProcesamiento.EnProceso
                        oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, iFecha:=EstadoProcesamiento.Fin, HayError:=True)
                        If File.Exists(directorioError.FullName & "\" & xmlFileEnTratamiento) Then File.Delete(directorioError.FullName & "\" & xmlFileEnTratamiento)
                        File.Move(directorioEnTratamiento.FullName & "\" & xmlFileEnTratamiento, directorioError.FullName & "\" & xmlFileEnTratamiento)
                    Case TipoErrorProcesamiento.Procesada
                        Dim infoXMLFile As String() = SplitXml(Replace(xmlFileEnTratamiento, ".xml", ""))
                        oInstancia.ID = infoXMLFile(1)
                        GestionarXMLEnEspera(oInstancia, infoXMLFile(2), xmlFileEnTratamiento)
                    Case Else 'TipoErrorProcesamiento.ProcesoFallido 
                        If File.Exists(directorioError.FullName & "\" & xmlFileEnTratamiento) Then File.Delete(directorioError.FullName & "\" & xmlFileEnTratamiento)
                        File.Move(directorioEnTratamiento.FullName & "\" & xmlFileEnTratamiento, directorioError.FullName & "\" & xmlFileEnTratamiento)
                End Select
            Catch ex As Exception
                If File.Exists(directorioError.FullName & "\" & xmlFile.Name) Then File.Delete(directorioError.FullName & "\" & xmlFile.Name)
                File.Move(directorioEnTratamiento.FullName & "\" & xmlFile.Name, directorioError.FullName & "\" & xmlFile.Name)

                If oInstancia.ID <> 0 Then listaProcesamientoXML.Remove(oInstancia.ID)

                Dim sError As String = ex.Message
                Dim oErrores As Errores
                oErrores = FSNServer.Get_Object(GetType(Errores))
                Try
                    oErrores.Create("FSNTratamientoInstancias\GestionarFicherosExistentes", SplitXml(xmlFile.Name)(0).ToString, ex.GetType().FullName, ex.Message, ex.StackTrace, SplitXml(xmlFile.Name)(1).ToString, "")
                    oErrores = Nothing
                Catch exError As Exception
                    oErrores.SendErrorEmail(sError & " / " & exError.Message, "GestionarFicherosExistentes", ConfigurationManager.AppSettings("NombreServicio"), SplitXml(xmlFile.Name)(0).ToString, SplitXml(xmlFile.Name)(1).ToString, AppDomain.CurrentDomain.BaseDirectory & "\FromInfo.txt")
                End Try
            End Try
        Next
        For Each xmlFile As FileInfo In New DirectoryInfo(directorioEnEspera.FullName).GetFiles().ToList.OrderBy(Function(x) x.CreationTime)
            ComprobarXMLEnEspera(xmlFile.Name)
        Next
    End Sub
    ''' <summary>
    ''' Para el nombre de fichero dado, comprobamos si para esa instancia ya hay un xml en tratamiento
    ''' SI=> no se hace nada y se deja el xml en la carpeta de espera
    ''' NO=> se mueve el xml a la carpeta de tratamiento y se trata
    ''' </summary>
    ''' <param name="fileName"></param>
    ''' <remarks></remarks>
    Private Sub ComprobarXMLEnEspera(ByVal fileName As String)
        Try
            Dim lInstancia As Long = strToLong(SplitXml(fileName)(1))

            If fileName.Contains("(=R=)") Then
                If Not listaProcesamientoXML.Contains(lInstancia) Then listaProcesamientoXML.Add(lInstancia)

                If File.Exists(directorioEnEspera.FullName & "\" & fileName) Then
                    If File.Exists(directorioEnTratamiento.FullName & "\" & fileName) Then File.Delete(directorioEnTratamiento.FullName & "\" & fileName)
                    File.Move(directorioEnEspera.FullName & "\" & fileName, directorioEnTratamiento.FullName & "\" & fileName)
                End If
                ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf ProcesarXML), fileName)
            Else
                If Not listaProcesamientoXML.Contains(lInstancia) Then
                    listaProcesamientoXML.Add(lInstancia)
                    If File.Exists(directorioEnEspera.FullName & "\" & fileName) Then
                        If File.Exists(directorioEnTratamiento.FullName & "\" & fileName) Then File.Delete(directorioEnTratamiento.FullName & "\" & fileName)
                        File.Move(directorioEnEspera.FullName & "\" & fileName, directorioEnTratamiento.FullName & "\" & fileName)
                    End If
                    ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf ProcesarXML), fileName)
                Else
                    If Not colaXMLEnEspera.ContainsKey(lInstancia) Then colaXMLEnEspera.Add(lInstancia, New Queue(Of String))
                    If Not colaXMLEnEspera(lInstancia).Contains(fileName) Then colaXMLEnEspera(lInstancia).Enqueue(fileName)
                End If
            End If
        Catch ex As Exception
            Dim sError As String = ex.Message
            Dim oErrores As Errores
            oErrores = FSNServer.Get_Object(GetType(Errores))
            Try
                oErrores.Create("FSNTratamientoInstancias\ComprobarXMLEnEspera", SplitXml(fileName)(0).ToString, ex.GetType().FullName, ex.Message, ex.StackTrace, SplitXml(fileName)(1).ToString, "")
                oErrores = Nothing
            Catch exError As Exception
                oErrores.SendErrorEmail(sError & " / " & exError.Message, "ComprobarXMLEnEspera", ConfigurationManager.AppSettings("NombreServicio"), SplitXml(fileName)(0).ToString, SplitXml(fileName)(1).ToString, AppDomain.CurrentDomain.BaseDirectory & "\FromInfo.txt")
            End Try
        End Try
    End Sub
    ''' <summary>
    ''' Cuando se crea un xml en la carpeta de espera debemos comprobar que no hay ningun otro xml de esta instancia en tratamiento.
    ''' Si lo hay, mantenemos el xml en la carpeta de espera, ya que al terminar el tratamiento de la primera miraremos si hay algun
    ''' xml para la instancia tratada en la carpeta de espera y la lanzaremos
    ''' Si no lo hay, movemos el xml a la carpeta de tratamiento y Tratamos el xml
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub OnRenamed(source As Object, e As FileSystemEventArgs)
        ComprobarXMLEnEspera(e.Name)
    End Sub
    Private Sub ProcesarXML(ByVal fileName As String)
        Dim dsXML As New DataSet
        Dim lInstancia As Long

        Try
            'Comprobar que el bloque del xml es el bloque actual, si no serÃ¡ que ha habido algÃºn error
            Dim infoXMLFile As String() = SplitXml(fileName.Replace("(=R=)", "").Replace(".xml", ""))
            lInstancia = strToLong(infoXMLFile(1).ToString)
            Dim lBloque As Long = strToLong(infoXMLFile(2).ToString)
            Dim oBloque As Bloque = FSNServer.Get_Object(GetType(Bloque))
            'Si lBloque=0 es de QA (no tienen bloque)
            If lBloque = 0 OrElse oBloque.EsBloqueActual(lInstancia, lBloque) Then
                Try
                    poolSemaforo.Wait()

                    Dim LecturaOK As Boolean = False
                    Dim numIntentos As Integer = 0
                    While (Not LecturaOK AndAlso numIntentos < 10)
                        numIntentos += 1
                        Try
                            dsXML = New DataSet
                            Dim oXMLDoc As New System.Xml.XmlDocument
                            oXMLDoc.Load(directorioEnTratamiento.FullName & "\" & fileName)
                            dsXML.ReadXml(New Xml.XmlNodeReader(oXMLDoc))
                            oXMLDoc = Nothing
                            LecturaOK = True
                        Catch ex As Exception
                            If numIntentos >= 10 Then
                                Throw ex
                            Else
                                Thread.Sleep(500)
                            End If
                        End Try
                    End While

                    If {TipoProcesamientoXML.FSNWeb_PM_AprobacionRechazoMultiple, TipoProcesamientoXML.Portal_PM_AprobacionRechazoMultiple}.Contains(CType(dsXML.Tables("SOLICITUD").Rows(0)("TIPO_PROCESAMIENTO_XML"), TipoProcesamientoXML)) Then
                        Tratar_AprobacionRechazoMultiple(dsXML, fileName)
                    Else
                        TratarInstancia(dsXML, fileName)
                    End If
                Catch ex As Exception
                    Throw ex
                Finally
                    poolSemaforo.Release()
                End Try
            Else
                'SerÃ¡ un fallo, hay que moverlo a error y gestionarXMLEnEspera
                If File.Exists(directorioEnTratamiento.FullName & "\" & fileName) Then
                    If File.Exists(directorioError.FullName & "\" & fileName) Then File.Delete(directorioError.FullName & "\" & fileName)
                    File.Move(directorioEnTratamiento.FullName & "\" & fileName, directorioError.FullName & "\" & fileName)
                End If
                Dim oInstancia As Instancia
                oInstancia = FSNServer.Get_Object(GetType(Instancia))
                oInstancia.ID = lInstancia
                If colaXMLEnEspera.ContainsKey(oInstancia.ID) Then 'Si tiene mas xml en cola proceso el siguiente
                    Dim newFileName As String = colaXMLEnEspera(oInstancia.ID).Peek()
                    colaXMLEnEspera(oInstancia.ID).Dequeue()
                    If colaXMLEnEspera(oInstancia.ID).Count = 0 Then colaXMLEnEspera.Remove(oInstancia.ID)

                    ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf ProcesarXML), newFileName)
                Else
                    If Not lBloque = 0 Then oInstancia.Actualizar_En_proceso(0, lBloque)
                    oInstancia.Actualizar_En_proceso(0)
                    listaProcesamientoXML.Remove(oInstancia.ID)
                End If
            End If
        Catch ex As Exception
            If File.Exists(directorioError.FullName & "\" & fileName) Then File.Delete(directorioError.FullName & "\" & fileName)
            File.Move(directorioEnTratamiento.FullName & "\" & fileName, directorioError.FullName & "\" & fileName)

            listaProcesamientoXML.Remove(lInstancia)

            Dim sError As String = ex.Message
            Dim oErrores As Errores
            oErrores = FSNServer.Get_Object(GetType(Errores))
            Try
                oErrores.Create("FSNTratamientoInstancias\ProcesarXML", SplitXml(fileName)(0).ToString, ex.GetType().FullName, ex.Message, ex.StackTrace, SplitXml(fileName)(1).ToString, "")
                oErrores = Nothing
            Catch exError As Exception
                oErrores.SendErrorEmail(sError & " / " & exError.Message, ConfigurationManager.AppSettings("NombreServicio"), "ProcesarXML", SplitXml(fileName)(0).ToString, SplitXml(fileName)(1).ToString, AppDomain.CurrentDomain.BaseDirectory & "\FromInfo.txt")
            End Try
        End Try
    End Sub
    Private Sub TratarInstancia(ByVal dsXML As DataSet, ByVal fileName As String)
        Dim oInstancia As Instancia
        Dim SolicitudXML As DataRow
        Dim iTipoSolicitud As TiposDeDatos.TipoDeSolicitud
        Dim sAccion, sUserEmail, sIdi As String
        Dim lFactura, lIDTiempoProc As Long
        Dim sStoredEnEjecucion As String = String.Empty
        Dim tipoOperacionProcesamientoPortal, tipoOperacionProcesamientoGuardado, tipoOperacionProcesamientoAccion As Integer

        tipoOperacionProcesamientoGuardado = 0
        tipoOperacionProcesamientoAccion = 0
        oInstancia = FSNServer.Get_Object(GetType(Instancia))
        SolicitudXML = dsXML.Tables("SOLICITUD").Rows(0)
        Try
            oInstancia.ID = strToLong(SolicitudXML("INSTANCIA").ToString)

            sAccion = SolicitudXML("ACCION").ToString
            lIDTiempoProc = strToLong(SolicitudXML("IDTIEMPOPROC").ToString)
            oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, iFecha:=EstadoProcesamiento.InicioBD)
            iTipoSolicitud = CType(SolicitudXML("TIPO_DE_SOLICITUD").ToString, TiposDeDatos.TipoDeSolicitud)
            sUserEmail = SolicitudXML("USUARIO_EMAIL").ToString
            sIdi = SolicitudXML("USUARIO_IDIOMA").ToString

            'Si es una solicitud de pm se pasa workflow a true
            If Not DBNullToBoolean(SolicitudXML("COMPLETO")) Then dsXML = oInstancia.CompletarFormulario(dsXML, SolicitudXML("USUARIO").ToString,
                                                                                    Not {TiposDeDatos.TipoDeSolicitud.NoConformidad,
                                                                                        TiposDeDatos.TipoDeSolicitud.Certificado,
                                                                                        TiposDeDatos.TipoDeSolicitud.Contrato}.Contains(iTipoSolicitud),
                                                                                    bUsarCampoDef:=True)
            Select Case CType(SolicitudXML("TIPO_PROCESAMIENTO_XML").ToString, TipoProcesamientoXML)
                Case TipoProcesamientoXML.FSNWeb
                    tipoOperacionProcesamientoPortal = 0
                    With oInstancia
                        .Peticionario = SolicitudXML("USUARIO").ToString
                        .IdFormulario = strToLong(SolicitudXML("FORMULARIO").ToString)
                        .IdWorkflow = strToLong(SolicitudXML("WORKFLOW").ToString)
                        .Importe = strToDbl(SolicitudXML("IMPORTE").ToString)
                        .PedidoDirecto = strToDbl(SolicitudXML("PEDIDO_DIRECTO").ToString)
                    End With

                    Select Case iTipoSolicitud
                        Case TiposDeDatos.TipoDeSolicitud.NoConformidad, TiposDeDatos.TipoDeSolicitud.Certificado
                            tipoOperacionProcesamientoGuardado = 1
                            If oInstancia.IdWorkflow > 1 Then
                                Tratar_FSNWeb_PM(oInstancia, SolicitudXML, sIdi, sUserEmail, iTipoSolicitud, dsXML, sAccion, sStoredEnEjecucion, tipoOperacionProcesamientoGuardado)
                                TratarAccion(oInstancia, SolicitudXML, sIdi, sUserEmail, iTipoSolicitud, dsXML, sAccion, tipoOperacionProcesamientoAccion)
                            Else
                                Tratar_FSNWeb_QA(oInstancia, SolicitudXML, sIdi, sUserEmail, iTipoSolicitud, dsXML, sStoredEnEjecucion, tipoOperacionProcesamientoGuardado)
                            End If
                        Case TiposDeDatos.TipoDeSolicitud.Contrato
                            Tratar_FSNWeb_Contrato(oInstancia, SolicitudXML, sIdi, sUserEmail, dsXML, sAccion, sStoredEnEjecucion, tipoOperacionProcesamientoGuardado)
                            TratarAccion(oInstancia, SolicitudXML, sIdi, sUserEmail, iTipoSolicitud, dsXML, sAccion, tipoOperacionProcesamientoAccion)
                        Case Else 'PM : SI HAY QUE HACER GUARDADO DE VERSION
                            Tratar_FSNWeb_PM(oInstancia, SolicitudXML, sIdi, sUserEmail, iTipoSolicitud, dsXML, sAccion, sStoredEnEjecucion, tipoOperacionProcesamientoGuardado)
                            TratarAccion(oInstancia, SolicitudXML, sIdi, sUserEmail, iTipoSolicitud, dsXML, sAccion, tipoOperacionProcesamientoAccion)
                    End Select
                Case TipoProcesamientoXML.Portal
                    tipoOperacionProcesamientoPortal = 1
                    tipoOperacionProcesamientoGuardado = 1
                    Select Case iTipoSolicitud
                        Case TiposDeDatos.TipoDeSolicitud.Certificado, TiposDeDatos.TipoDeSolicitud.NoConformidad
                            Tratar_Portal_QA(oInstancia, SolicitudXML, sIdi, sUserEmail, iTipoSolicitud, dsXML, sAccion, sStoredEnEjecucion, tipoOperacionProcesamientoGuardado)
                        Case Else
                            With oInstancia
                                .PeticionarioProve = SolicitudXML("PETICIONARIO_PROVE").ToString
                                .PeticionarioProveContacto = strToInt(SolicitudXML("PETICIONARIO_PROVECON").ToString)
                                .IdFormulario = strToLong(SolicitudXML("FORMULARIO").ToString)
                                .IdWorkflow = strToLong(SolicitudXML("WORKFLOW").ToString)
                                .Importe = strToDbl(SolicitudXML("IMPORTE").ToString)
                            End With
                            Tratar_Portal_PM(oInstancia, SolicitudXML, sIdi, sUserEmail, iTipoSolicitud, dsXML, sAccion, sStoredEnEjecucion, tipoOperacionProcesamientoGuardado)
                    End Select
                Case Else 'TipoProcesamientoXML.FSNWeb_PM_AprobacionRechazoMultiple
                    tipoOperacionProcesamientoPortal = 0
                    tipoOperacionProcesamientoAccion = 2
                    TratarAccion(oInstancia, SolicitudXML, sIdi, sUserEmail, iTipoSolicitud, dsXML, sAccion, tipoOperacionProcesamientoAccion)
            End Select

            Dim oAccion As Accion
            oAccion = CargarDatosAccion(If(SolicitudXML.Table.Columns.Contains("IDACCION"), strToLong(SolicitudXML("IDACCION").ToString), 0), sIdi)

            If oAccion.GenerarEFactura Then
                Dim oFactura As Factura = FSNServer.Get_Object(GetType(Factura))
                Dim oCFacturaE As CFacturae = FSNServer.Get_Object(GetType(CFacturae))
                Dim sNumFactura As String

                sNumFactura = oFactura.DevolverNumeroFactura(oInstancia.ID)
                oCFacturaE.crearFacV3_2(lFactura.ToString, ConfigurationManager.AppSettings("temp") & "\" & sNumFactura & ".xml", sIdi)
                oFactura.Save_EFactura(sNumFactura & ".xml", ConfigurationManager.AppSettings("temp") & "\", lFactura)
            End If

            oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, iFecha:=EstadoProcesamiento.Fin,
                                                     EsPortal:=tipoOperacionProcesamientoPortal,
                                                     TipoGuardado:=tipoOperacionProcesamientoGuardado,
                                                     TipoAccion:=tipoOperacionProcesamientoAccion)
            dsXML = Nothing

            GestionarXMLEnEspera(oInstancia, If(SolicitudXML.Table.Columns.Contains("BLOQUE_ORIGEN"), strToLong(SolicitudXML("BLOQUE_ORIGEN").ToString), 0), fileName)

            'INTEGRACION
            Dim oIntegracion As Integracion
            oIntegracion = FSNServer.Get_Object(GetType(Integracion))
            oIntegracion.LlamarFSIS(TablasIntegracion.PM, oInstancia.ID)
        Catch ex As Exception
            Dim sError As String = ex.Message
            Dim oErrores As Errores
            oErrores = FSNServer.Get_Object(GetType(Errores))
            Try
                oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, iFecha:=EstadoProcesamiento.Fin, HayError:=True)
                If File.Exists(directorioError.FullName & "\" & fileName) Then File.Delete(directorioError.FullName & "\" & fileName)
                File.Move(directorioEnTratamiento.FullName & "\" & fileName, directorioError.FullName & "\" & fileName)
                oErrores.Create("FSNTratamientoInstancias\TratarInstancia" & If(String.IsNullOrEmpty(sStoredEnEjecucion), "", " - " & sStoredEnEjecucion),
                            SplitXml(fileName)(0).ToString, ex.GetType().FullName, ex.Message, ex.StackTrace, SplitXml(fileName)(1).ToString, "")
            Catch exError As Exception
                oErrores.SendErrorEmail(sError & " / " & exError.Message, ConfigurationManager.AppSettings("NombreServicio"), "TratarInstancia", SplitXml(fileName)(0).ToString, SplitXml(fileName)(1).ToString, AppDomain.CurrentDomain.BaseDirectory & "\FromInfo.txt")
            Finally
                oErrores = Nothing
            End Try
        End Try
    End Sub
    Private Sub TratarAccion(ByVal oInstancia As Instancia, ByVal SolicitudXML As DataRow, ByVal sIdi As String, ByVal sUserEmail As String,
                            ByVal iTipoSolicitud As TiposDeDatos.TipoDeSolicitud, ByVal dsXML As DataSet, ByVal sAccion As String,
                            ByRef tipoOperacionProcesamientoAccion As Integer)
        Try
            Dim dsRoles As DataSet
            dsRoles = New DataSet
            If Not dsXML.Tables("ROLES") Is Nothing Then dsRoles.Tables.Add(dsXML.Tables("ROLES").Copy)

            Select Case sAccion
                Case "trasladarinstancia", "trasladadatrasladarinstancia", "trasladarcontrato", "trasladadatrasladarcontrato" 'Acciones para los traslados
                    tipoOperacionProcesamientoAccion = 3
                    Dim sPerInicialTraslado As String
                    Dim oBloque As Bloque
                    Dim dsEstadoBloque As New DataSet

                    oBloque = FSNServer.Get_Object(GetType(Bloque))
                    dsEstadoBloque = oBloque.Devolver_Persona_Inicial_Traslado(oInstancia.ID, oInstancia.InstanciaBloque)

                    If oInstancia.InstanciaBloqueTrasladada Then
                        sPerInicialTraslado = dsEstadoBloque.Tables(0).Rows(0)("PER")
                    Else
                        sPerInicialTraslado = oInstancia.Peticionario
                    End If

                    If String.IsNullOrEmpty(SolicitudXML("TRASLADO_USUARIO")) Then 'Traslada a un proveedor:
                        oInstancia.Trasladar(oInstancia.Peticionario, sUserEmail, SolicitudXML("TRASLADO_PROVEEDOR"), SolicitudXML("TRASLADO_COMENTARIO"), SolicitudXML("TRASLADO_FECHA"), True,
                                             SolicitudXML("TRASLADO_PROVEEDOR_CONTACTO"), oInstancia.InstanciaBloque, sPerInicialTraslado, iTipoSolicitud, sIdi)
                    Else 'Traslada a un usuario
                        oInstancia.Trasladar(oInstancia.Peticionario, sUserEmail, SolicitudXML("TRASLADO_USUARIO").ToString, SolicitudXML("TRASLADO_COMENTARIO").ToString, SolicitudXML("TRASLADO_FECHA").ToString, False, 0,
                                             oInstancia.InstanciaBloque, sPerInicialTraslado, iTipoSolicitud, sIdi)
                    End If
                Case "trasladadadevolverinstancia", "trasladadadevolvercontrato"
                    tipoOperacionProcesamientoAccion = 4
                    oInstancia.DevolverEtapaActual(sIdi, oInstancia.Peticionario)
                    oInstancia.Devolucion(sPer:=oInstancia.Peticionario, sFrom:=sUserEmail, sComentario:=SolicitudXML("DEVOLUCION_COMENTARIO").ToString, lBloque:=oInstancia.InstanciaBloque, iTipoSolicitud:=iTipoSolicitud)
                Case "guardarsolicitud"
                    oInstancia.RealizarAccion(lAccion:=strToInt(SolicitudXML("IDACCION").ToString), sPer:=SolicitudXML("USUARIO").ToString, sFrom:=sUserEmail, sComent:=SolicitudXML("COMENTARIO").ToString, oRoles:=dsRoles,
                                              iTipoSolicitud:=iTipoSolicitud, AccionGuardar:=True, sIdiomaDefecto:=sIdi)
                Case "guardarcontrato"
                    oInstancia.RealizarAccion(lAccion:=strToInt(SolicitudXML("IDACCION").ToString), sPer:=SolicitudXML("USUARIO").ToString, sFrom:=sUserEmail, sComent:=SolicitudXML("COMENTARIO").ToString, oRoles:=dsRoles,
                                              iTipoSolicitud:=iTipoSolicitud, AccionGuardar:=True, sIdiomaDefecto:=sIdi)
                Case Else 'Accion                    
                    If tipoOperacionProcesamientoAccion = 0 Then tipoOperacionProcesamientoAccion = 1
                        If SolicitudXML.Table.Columns.Contains("NUEVO_ID_INSTANCIA") Then
                            If Not IsDBNull(SolicitudXML.Item("NUEVO_ID_INSTANCIA")) Then
                                If Not String.IsNullOrEmpty(SolicitudXML.Item("NUEVO_ID_INSTANCIA")) Then
                                    oInstancia.NuevoID = SolicitudXML.Item("NUEVO_ID_INSTANCIA").ToString()
                                End If

                            End If
                        End If
                        If CType(dsXML.Tables("SOLICITUD").Rows(0)("TIPO_PROCESAMIENTO_XML"), TipoProcesamientoXML) = TipoProcesamientoXML.Portal_PM_AprobacionRechazoMultiple Then
                            oInstancia.RealizarAccion(strToInt(SolicitudXML("IDACCION").ToString), , SolicitudXML("USUARIO_EMAIL").ToString, SolicitudXML.Item("COMENTARIO").ToString, dsRoles, SolicitudXML.Item("PROVEGS").ToString,
                                          SolicitudXML("CODCIA").ToString, SolicitudXML("DENCIA").ToString, SolicitudXML("NIFCIA").ToString, SolicitudXML("NOMBRE").ToString, SolicitudXML("APELLIDOS").ToString,
                                          SolicitudXML("TELEFONO").ToString, SolicitudXML("USUARIO_EMAIL").ToString, SolicitudXML("FAX").ToString, iTipoSolicitud, bPortal:=True, sIdiomaDefecto:=sIdi)
                        Else
                            oInstancia.RealizarAccion(lAccion:=strToInt(SolicitudXML.Item("IDACCION").ToString), sPer:=SolicitudXML("USUARIO").ToString, sFrom:=sUserEmail, sComent:=SolicitudXML("COMENTARIO").ToString, oRoles:=dsRoles,
                                                  iTipoSolicitud:=iTipoSolicitud, sIdiomaDefecto:=sIdi)
                        End If

                    'Notificación de las no conformidades con flujo que han llegado a fin y pasan a estar emitidas
                    If iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad Then
                        Dim dtDatos As DataTable = oInstancia.ComprobarEstadoNoConformidad()
                        If Not dtDatos Is Nothing AndAlso dtDatos.Rows.Count > 0 Then
                            If dtDatos.Rows(0)("ESTADO") = TipoEstadoSolic.NoConformidadEnviada Then
                                Dim oNoConformidad As NoConformidad = FSNServer.Get_Object(GetType(NoConformidad))
                                With oNoConformidad
                                    .ID = dtDatos.Rows(0)("NOCONFORMIDAD")
                                    .IdInstanciaNotificacion = oInstancia.ID
                                    .TipoNotificacion = 1
                                    .EmailEnvioNotificacion = sUserEmail
                                    .NotificarNoConformidadQA()
                                End With
                            End If
                        End If
                    End If
            End Select
            If {TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoCatalogo, TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoContraAbierto}.Contains(iTipoSolicitud) Then
                'Si es una solicitud de pedido miraremos si la orden de entrega esta emitida al proveedor, si es el caso la integraremos
                Dim dsOrden As DataSet = oInstancia.ComprobarEstadoOrdenEntrega
                If Not dsOrden Is Nothing AndAlso dsOrden.Tables.Count > 0 AndAlso dsOrden.Tables(0).Rows.Count > 0 Then
                    Dim bHayQueDenegarLineas As Boolean = False

                    Dim oOrden As COrden = FSNServer.Get_Object(GetType(COrden))
                    oOrden.ID = dsOrden.Tables(0).Rows(0)("ORDEN")
                    oOrden.PedidoId = dsOrden.Tables(0).Rows(0)("PEDIDO")
                    If dsOrden.Tables(0).Rows(0)("EST") = TipoEstadoOrdenEntrega.EmitidoAlProveedor Then
                        'Esta emitida al proveedor
                        'Sumamos el importe comprometido del SM
                        Dim obDetallePedido As cDetallePedidos = FSNServer.Get_Object(GetType(cDetallePedidos))
                        obDetallePedido.ActualizarImputacion(dsOrden.Tables(0).Rows(0)("ORDEN"))
                        'Llamamos al servicio de integraciÃƒÂ³n WCF.
                        Dim obIntegracion As Integracion = FSNServer.Get_Object(GetType(Integracion))
                        obIntegracion.LlamarFSIS(TablasIntegracion.PED_Aprov, dsOrden.Tables(0).Rows(0)("ORDEN"))

                        Dim oEmisionPedido As CEmisionPedidos = FSNServer.Get_Object(GetType(CEmisionPedidos))
                        oEmisionPedido.TrasCrearOrdenEntregaLineaActualizarCatalogo(dsOrden.Tables(0).Rows(0)("ORDEN"), SolicitudXML("INSTANCIA").ToString)
                    ElseIf (iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoCatalogo) AndAlso (dsOrden.Tables(0).Rows(0)("EST") = TipoEstadoOrdenEntrega.DenegadoParcialAprob) Then
                        If CType(SolicitudXML("TIPO_PROCESAMIENTO_XML").ToString, TipoProcesamientoXML) = TipoProcesamientoXML.FSNWeb_PM_AprobacionRechazoMultiple _
                        OrElse (Not oOrden.Comprobar_HayLineasAprobadas) Then 'Es Rechazar desde Mail OrElse (Sin lineas aprobadas Y Accion rechazar desde detalle)-> totalmente denegado 
                            bHayQueDenegarLineas = True
                            oOrden.DenegarPedido(SolicitudXML("CODIGOUSUARIO").ToString, SolicitudXML("USUARIO_IDIOMA").ToString, "", 2)
                        End If
                    End If
                    Select Case dsOrden.Tables(0).Rows(0)("EST") 'Notificaciones dependiendo del estado de la orden de entrega
                        Case TipoEstadoOrdenEntrega.DenegadoParcialAprob
                            If bHayQueDenegarLineas Then
                                oOrden.NotificarDenegacionTotalOrden(sUserEmail)
                            Else
                                oOrden.NotificarDenegacionParcialOrden(sUserEmail)
                            End If
                        Case TipoEstadoOrdenEntrega.DenegadoTotalAprobador
                            oOrden.NotificarDenegacionTotalOrden(sUserEmail)
                        Case TipoEstadoOrdenEntrega.EmitidoAlProveedor
                            oOrden.NotificarOrden(sUserEmail, dsOrden.Tables(0).Rows(0)("NOTIFICADO"))
                    End Select
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub Tratar_FSNWeb_QA(ByVal oInstancia As Instancia, ByVal SolicitudXML As DataRow, ByVal sIdi As String, ByVal sUserEmail As String,
                                 ByVal iTipoSolicitud As TiposDeDatos.TipoDeSolicitud, ByVal dsXML As DataSet, ByRef sStoredEnEjecucion As String,
                                 ByRef tipoOperacionProcesamientoGuardado As Integer)
        Dim lNoConformidad, lCertificado As Long
        lNoConformidad = strToLong(SolicitudXML("NOCONFORMIDAD").ToString)
        lCertificado = strToLong(SolicitudXML("CERTIFICADO").ToString)

        Dim bEsAlta As Boolean = (SolicitudXML("WORKFLOW").ToString = "1") 'En QA se reutiliza el campo WORKFLOW que no se usa para saber que es una 1º versión
        Dim bEnviar As Boolean = (SolicitudXML("GUARDAR").ToString = "1") 'En QA se reutiliza el campo GUARDAR para saber si es un envio
        If bEsAlta Then
            oInstancia.Create(oDs:=dsXML, bGuardar:=bEnviar, tipoOperacionProcesamientoGuardado:=tipoOperacionProcesamientoGuardado,
                            bPedidoAut:=False, sComentario:=SolicitudXML("COMENTALTANOCONF").ToString,
                            lNoconformidad:=lNoConformidad, lCertificado:=lCertificado, sStoredEnEjecucion:=sStoredEnEjecucion, sIdioma:=sIdi)

            'NOTIFICACIONES-----------------------------------------------------
            If iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.Certificado Then
                'Si la instancia no pasa por el workflow entonces los certificados se han emitido.Se envía el email a cada proveedor:
                If oInstancia.Estado = TipoEstadoSolic.CertificadoPub Then
                    Dim oCertificado As Certificado = FSNServer.Get_Object(GetType(Certificado))
                    With oCertificado
                        .ID = lCertificado
                        .EmailEnvioNotificacion = sUserEmail
                        .NotificarEnvioCertificadoQA()
                    End With
                End If
            Else
                If bEnviar Then
                    Dim oNoConformidad As NoConformidad
                    oNoConformidad = FSNServer.Get_Object(GetType(NoConformidad))

                    With oNoConformidad
                        .ID = lNoConformidad
                        .IdInstanciaNotificacion = oInstancia.ID
                        .EmailEnvioNotificacion = sUserEmail
                        .TipoNotificacion = 1
                        'Llamamos a método de las notificaciones 
                        .NotificarNoConformidadQA()
                    End With
                End If
            End If
        Else 'No es primera versión
            tipoOperacionProcesamientoGuardado = 1
            oInstancia.Save(oDs:=dsXML, sPer:=oInstancia.Peticionario, bEnviar:=bEnviar, tipoOperacionProcesamientoGuardado:=tipoOperacionProcesamientoGuardado, bPedidoAut:=False,
                            lCertificado:=lCertificado, lNoConformidad:=lNoConformidad, sEmail:=sUserEmail, bNotificar:=IIf(SolicitudXML.Item("NOTIFICAR").ToString = "1", True, False),
                            sComentario:=SolicitudXML("COMENTALTANOCONF").ToString, sStoredEnEjecucion:=sStoredEnEjecucion, sIdioma:=sIdi)

            Dim oNoConformidad As NoConformidad
            oNoConformidad = FSNServer.Get_Object(GetType(NoConformidad))
            With oNoConformidad
                .ID = lNoConformidad
                .IdInstanciaNotificacion = oInstancia.ID
                If oInstancia.Estado = TipoEstadoSolic.NoConformidadEnviada Then
                    .TipoNotificacion = IIf(bEnviar, 1, IIf(SolicitudXML("NOTIFICAR").ToString = "1", 2, 0))
                    .EmailEnvioNotificacion = sUserEmail
                    .NotificarNoConformidadQA()
                End If
            End With
        End If

        'Comprobar si existen tipos de certificados que le corresponden al proveedor que dependan del certificado para ser solicitados
        'Desde aquÃƒÆ’Ã‚Â­ no se crean versiones de tipo borrador
        If Not lCertificado = 0 Then
            Dim oCertificado As Certificado = FSNServer.Get_Object(GetType(Certificado))
            With oCertificado
                .ID = lCertificado
                .Instancia = oInstancia.ID

                ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf .SolicitudAutomaticaTiposCertificadosCondiciones))
            End With
        End If
    End Sub
    Private Sub Tratar_FSNWeb_Contrato(ByVal oInstancia As Instancia, ByVal SolicitudXML As DataRow, ByVal sIdi As String, ByVal sUserEmail As String,
                                       ByVal dsXML As DataSet, ByVal sAccion As String, ByRef sStoredEnEjecucion As String, ByRef tipoOperacionProcesamientoGuardado As Integer)
        Dim lContrato As Long = strToLong(SolicitudXML("CONTRATO").ToString)
        Dim dsRoles As DataSet
        dsRoles = New DataSet
        If Not dsXML.Tables("ROLES") Is Nothing Then dsRoles.Tables.Add(dsXML.Tables("ROLES").Copy)

        Dim idArchivoContrato As Long
        If Not dsXML.Tables("TEMPADJUN") Is Nothing Then
            Dim dr() As System.Data.DataRow
            dr = dsXML.Tables("TEMPADJUN").Select("TIPO=5")
            If dr.Count > 0 Then
                Dim arrDatosAdjuntos(0) As Object
                arrDatosAdjuntos = dr(0).ItemArray
                idArchivoContrato = arrDatosAdjuntos(2)
            End If
        End If

        If (SolicitudXML("GUARDAR").ToString = "1") OrElse sAccion = "guardarcontrato" OrElse String.IsNullOrEmpty(SolicitudXML("SOLICITUD").ToString) Then
            If Not String.IsNullOrEmpty(SolicitudXML("SOLICITUD").ToString) Then
                'HEMOS ENTRADO DESDE NWALTA
                FSNServer.Impersonate()
                oInstancia.Create(oDs:=dsXML, bGuardar:=(SolicitudXML("GUARDAR").ToString = "1"), tipoOperacionProcesamientoGuardado:=tipoOperacionProcesamientoGuardado, bPedidoAut:=(SolicitudXML("PEDIDO_DIRECTO").ToString = "1"),
                                  sComentario:=SolicitudXML("COMENTALTANOCONF").ToString, lIdContrato:=lContrato, sStoredEnEjecucion:=sStoredEnEjecucion,
                                  sIdioma:=sIdi, lIdArchivoContrato:=idArchivoContrato, tipoSolicitud:=TiposDeDatos.TipoDeSolicitud.Contrato, Rol:=strToLong(SolicitudXML("ROL_ACTUAL").ToString))
            Else 'si la solicitud ha sido despublicada no se podrá emitir.
                Dim GuardarInstancia As Boolean = True
                If Not sAccion = "guardarcontrato" AndAlso oInstancia.Estado = TipoEstadoSolic.Guardada Then
                    oInstancia.Load(sIdi, True)
                    If Not oInstancia.Solicitud.Pub Then GuardarInstancia = False
                End If

                If GuardarInstancia Then
                    tipoOperacionProcesamientoGuardado = 1
                    oInstancia.DevolverEtapaActual(sIdi, oInstancia.Peticionario)
                    oInstancia.Save(oDs:=dsXML, sPer:=oInstancia.Peticionario, bEnviar:=False,
                                    tipoOperacionProcesamientoGuardado:=tipoOperacionProcesamientoGuardado, dImporte:=oInstancia.Importe, sEmail:=sUserEmail,
                                    bNotificar:=(SolicitudXML("NOTIFICAR").ToString = "1"), sStoredEnEjecucion:=sStoredEnEjecucion, sIdioma:=sIdi,
                                    lIdContrato:=lContrato, lIdArchivoContrato:=idArchivoContrato)
                End If
            End If
        End If
    End Sub
    Private Sub Tratar_FSNWeb_PM(ByVal oInstancia As Instancia, ByVal SolicitudXML As DataRow, ByVal sIdi As String, ByVal sUserEmail As String,
                                 ByVal iTipoSolicitud As TiposDeDatos.TipoDeSolicitud, ByVal dsXML As DataSet, ByVal sAccion As String, ByRef sStoredEnEjecucion As String,
                                 ByRef tipoOperacionProcesamientoGuardado As Integer)
        If SolicitudXML("GUARDAR").ToString = "1" OrElse sAccion = "guardarsolicitud" OrElse Not String.IsNullOrEmpty(SolicitudXML("SOLICITUD").ToString) Then
            tipoOperacionProcesamientoGuardado = 1
            If Not String.IsNullOrEmpty(SolicitudXML("SOLICITUD").ToString) Then 'PM: SI ES ALTA
                tipoOperacionProcesamientoGuardado = 1
                oInstancia.Create(oDs:=dsXML, bGuardar:=(SolicitudXML("GUARDAR").ToString = "1"), tipoOperacionProcesamientoGuardado:=tipoOperacionProcesamientoGuardado, bPedidoAut:=(SolicitudXML("PEDIDO_DIRECTO").ToString = "1"),
                                  sComentario:="", sStoredEnEjecucion:=sStoredEnEjecucion, sIdioma:=sIdi, tipoSolicitud:=iTipoSolicitud, Rol:=strToLong(SolicitudXML("ROL_ACTUAL").ToString))
            Else
                Dim lFactura As Long = 0
                If SolicitudXML.Table.Columns.Contains("FACTURA") Then lFactura = strToLong(SolicitudXML("FACTURA").ToString)

                tipoOperacionProcesamientoGuardado = 1
                oInstancia.DevolverEtapaActual(sIdi, IIf(Not {TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoCatalogo, TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoContraAbierto}.Contains(iTipoSolicitud), Nothing, oInstancia.Peticionario))
                oInstancia.Save(oDs:=dsXML, sPer:=oInstancia.Peticionario, bEnviar:=False, tipoOperacionProcesamientoGuardado:=tipoOperacionProcesamientoGuardado,
                                dImporte:=oInstancia.Importe, sEmail:=sUserEmail, bNotificar:=(SolicitudXML("NOTIFICAR").ToString = "1"),
                                sStoredEnEjecucion:=sStoredEnEjecucion, sIdioma:=sIdi, lIdFactura:=lFactura, lSolicitudDePedido:=strToLong(SolicitudXML("PEDIDO").ToString), tipoSolicitud:=iTipoSolicitud)
            End If
        End If
    End Sub
    ''' <summary>
    ''' Funcion que carga propiedades de la acción y datos de la llamada externa
    ''' </summary>
    ''' <param name="idAccion"></param>
    ''' <returns>Devuelve el nombre del Xml que contiene la llamada externa</returns>
    ''' <remarks></remarks>
    Private Function CargarDatosAccion(ByVal idAccion As Int32, ByVal sIdi As String) As Accion
        Dim oAccion As Accion = FSNServer.Get_Object(GetType(Accion))
        oAccion.Id = idAccion
        oAccion.CargarAccion(sIdi)

        Return oAccion
    End Function
    Private Sub Tratar_Portal_QA(ByVal oInstancia As Instancia, ByVal SolicitudXML As DataRow, ByVal sIdi As String, ByVal sUserEmail As String,
                                 ByVal iTipoSolicitud As TiposDeDatos.TipoDeSolicitud, ByVal dsXML As DataSet, ByVal sAccion As String,
                                 ByRef sStoredEnEjecucion As String, ByRef tipoOperacionProcesamientoGuardado As Integer)
        Dim lNoConformidad, lCertificado As Long
        lNoConformidad = strToLong(SolicitudXML("NOCONFORMIDAD").ToString)
        lCertificado = strToLong(SolicitudXML("CERTIFICADO").ToString)
        oInstancia.ID = strToLong(SolicitudXML("INSTANCIA").ToString)

        If Not DBNullToLong(SolicitudXML("SOLICITUD")) = 0 Then 'PORTAL. CERTIFICADO. CREAR LA PRIMERA VERSION DESDE PORTAL
            oInstancia.Create(oDs:=dsXML, bGuardar:=True, tipoOperacionProcesamientoGuardado:=tipoOperacionProcesamientoGuardado, bPedidoAut:=False, lNoconformidad:=lNoConformidad, lCertificado:=lCertificado, bPortal:=True,
                                      sProveGS:=SolicitudXML("PROVEGS").ToString, lSolicitud:=strToLong(SolicitudXML("SOLICITUD").ToString),
                                      sStoredEnEjecucion:=sStoredEnEjecucion, sIdioma:=SolicitudXML("USUARIO_IDIOMA").ToString, sUsuNom:=SolicitudXML("NOMBRE").ToString,
                                      tipo:=IIf(sAccion = "guardarcertificado", 3,
                                                IIf(sAccion = "enviarCertificadoYtodosPdtesdeEnviar", 1,
                                                    IIf(sAccion = "enviarCertificadosPortal", 1,
                                                        IIf(sAccion = "guardarCertificadoPdteEnviar", 2, 0)))))
        Else 'PORTAL. CERTIFICADO/NO CONFORMIDAD. NO ES PRIMERA VERSION   
            tipoOperacionProcesamientoGuardado = 1
            oInstancia.Save(oDs:=dsXML, sPer:="", bEnviar:=(sAccion = "enviarCertificadosPortal" OrElse sAccion = "enviarCertificadoYtodosPdtesdeEnviar" OrElse sAccion = "enviarNoConformidadesPortal"),
                            tipoOperacionProcesamientoGuardado:=tipoOperacionProcesamientoGuardado, bPedidoAut:=False, lCertificado:=lCertificado, lNoConformidad:=lNoConformidad, sEmail:=SolicitudXML.Item("USUARIO_EMAIL").ToString,
                            bNotificar:=(SolicitudXML.Item("NOTIFICAR") = "1"), bPortal:=True, sProveGS:=SolicitudXML.Item("PROVEGS").ToString, sUsuNom:=SolicitudXML.Item("NOMBRE").ToString, sStoredEnEjecucion:=sStoredEnEjecucion,
                            sIdioma:=SolicitudXML.Item("USUARIO_IDIOMA").ToString, bCertPdteEnviar:=(sAccion = "guardarCertificadoPdteEnviar"))
        End If

        Select Case sAccion
            Case "altaCertificadoPortal", "enviarCertificadosPortal"
                tipoOperacionProcesamientoGuardado = 1
                Dim oCertificado As Certificado
                oCertificado = FSNServer.Get_Object(GetType(Certificado))

                With oCertificado
                    .ID = lCertificado
                    .EmailEnvioNotificacion = SolicitudXML.Item("USUARIO_EMAIL").ToString
                    .ProveedorEnvioNotificacion = SolicitudXML.Item("PROVEGS").ToString
                    .NombreEnvioNotificacion = SolicitudXML.Item("NOMBRE").ToString
                    .IdInstanciaNotificacion = oInstancia.ID
                    .NotificarEnvioCertificadoPortal()
                End With
            Case "enviarNoConformidadesPortal"
                tipoOperacionProcesamientoGuardado = 1
                Dim oNoConformidad As NoConformidad
                oNoConformidad = FSNServer.Get_Object(GetType(NoConformidad))

                With oNoConformidad
                    .ID = lNoConformidad
                    .ProveedorEnvioNotificacion = SolicitudXML.Item("PROVEGS").ToString
                    .EmailEnvioNotificacion = SolicitudXML.Item("USUARIO_EMAIL").ToString
                    .NombreEnvioNotificacion = SolicitudXML.Item("NOMBRE").ToString
                    .IdInstanciaNotificacion = oInstancia.ID
                    .NotificarEnvioNoConformidadPortal()
                End With
            Case "enviarCertificadoYtodosPdtesdeEnviar"
                tipoOperacionProcesamientoGuardado = 1
                Dim oCertificado As Certificado
                oCertificado = FSNServer.Get_Object(GetType(Certificado))

                With oCertificado
                    .ID = lCertificado
                    .EmailEnvioNotificacion = SolicitudXML.Item("USUARIO_EMAIL").ToString
                    .ProveedorEnvioNotificacion = SolicitudXML.Item("PROVEGS").ToString
                    .NombreEnvioNotificacion = SolicitudXML.Item("NOMBRE").ToString
                    .IdInstanciaNotificacion = oInstancia.ID
                    .NotificarEnvioCertificadoPortal()
                End With

                Dim oCertificados As Certificados
                Dim dCertificadosPendientes As DataSet
                oCertificados = FSNServer.Get_Object(GetType(Certificados))
                dCertificadosPendientes = oCertificados.ObtenerCertPendientesEnviar(lCertificado, SolicitudXML.Item("PROVEGS"))

                For Each rowCertificado In dCertificadosPendientes.Tables(0).Rows
                    With oCertificado
                        .ID = rowCertificado.Item("IDCERTIFICADO").ToString
                        .EmailEnvioNotificacion = SolicitudXML.Item("USUARIO_EMAIL").ToString
                        .ProveedorEnvioNotificacion = SolicitudXML.Item("PROVEGS").ToString
                        .NombreEnvioNotificacion = SolicitudXML.Item("NOMBRE").ToString
                        .IdInstanciaNotificacion = rowCertificado.Item("IDINSTANCIA").ToString
                        .FinalizarCertPendienteEnviar()
                        .NotificarEnvioCertificadoPortal()
                    End With
                Next
        End Select

        'Comprobar si existen tipos de certificados que le corresponden al proveedor que dependan del certificado para ser solicitados
        'Las acciones guardarcertificado y guardarCertificadoPdteEnviar crean versiones de tipo borrador
        If lCertificado <> 0 And sAccion <> "guardarcertificado" And sAccion <> "guardarCertificadoPdteEnviar" Then
            Dim oCertificado As Certificado = FSNServer.Get_Object(GetType(Certificado))
            With oCertificado
                .ID = lCertificado
                .Instancia = oInstancia.ID

                ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf .SolicitudAutomaticaTiposCertificadosCondiciones))
            End With
        End If
    End Sub
    Private Sub Tratar_Portal_PM(ByVal oInstancia As Instancia, ByVal SolicitudXML As DataRow, ByVal sIdi As String, ByVal sUserEmail As String,
                                 ByVal iTipoSolicitud As TiposDeDatos.TipoDeSolicitud, ByVal dsXML As DataSet, ByVal sAccion As String,
                                 ByRef sStoredEnEjecucion As String, ByRef tipoOperacionProcesamientoGuardado As Integer)
        Dim lFactura As Long
        lFactura = strToLong(SolicitudXML("FACTURA").ToString)

        Dim dsRoles As DataSet
        dsRoles = New DataSet
        If Not dsXML.Tables("ROLES") Is Nothing Then dsRoles.Tables.Add(dsXML.Tables("ROLES").Copy)

        ' GUARDADO DE VERSION
        If SolicitudXML("GUARDAR").ToString = "1" OrElse sAccion = "devolversolicitud" OrElse sAccion = "guardarsolicitud" OrElse Not String.IsNullOrEmpty(SolicitudXML("SOLICITUD").ToString) Then 'FALTA ID CONTRATO Y ID ARCHIVO CONTRATO
            If Not String.IsNullOrEmpty(SolicitudXML("SOLICITUD").ToString) Then 'SI ES ALTA
                oInstancia.Create(oDs:=dsXML, bGuardar:=(SolicitudXML("GUARDAR").ToString = "1"), tipoOperacionProcesamientoGuardado:=tipoOperacionProcesamientoGuardado, bPortal:=True,
                                  sComentario:="", sStoredEnEjecucion:=sStoredEnEjecucion, sIdioma:=sIdi, tipoSolicitud:=iTipoSolicitud, Rol:=strToLong(SolicitudXML("ROL_ACTUAL").ToString))
            Else
                oInstancia.Save(dsXML, sPer:="", bEnviar:=False, tipoOperacionProcesamientoGuardado:=tipoOperacionProcesamientoGuardado, dImporte:=strToDbl(SolicitudXML("IMPORTE").ToString),
                            sEmail:=SolicitudXML("USUARIO_EMAIL").ToString, bNotificar:=IIf(SolicitudXML("NOTIFICAR").ToString = "1", True, False), bPortal:=True, sProveGS:=SolicitudXML("PROVEGS").ToString,
                            sUsuNom:=SolicitudXML("NOMBRE").ToString & " " & SolicitudXML("APELLIDOS").ToString, sIdiomaProve:=SolicitudXML("USUARIO_IDIOMA").ToString,
                            sStoredEnEjecucion:=sStoredEnEjecucion, sIdioma:=SolicitudXML("USUARIO_IDIOMA").ToString, lIdFactura:=lFactura, tipoSolicitud:=iTipoSolicitud)
            End If
        End If

        'FLUJO DE TRABAJO
        Select Case sAccion
            Case "devolversolicitud", "trasladadadevolvercontrato"
                oInstancia.Devolucion(sPer:="", sFrom:=SolicitudXML("USUARIO_EMAIL").ToString, sComentario:=SolicitudXML("DEVOLUCION_COMENTARIO").ToString, lBloque:=SolicitudXML("BLOQUES_DESTINO").ToString,
                                      sProveedor:=SolicitudXML("PROVEGS").ToString, sCodCia:=SolicitudXML("CODCIA").ToString, sDenCia:=SolicitudXML("DENCIA").ToString, sNIFCia:=SolicitudXML("NIFCIA").ToString,
                                      sNombre:=SolicitudXML("NOMBRE").ToString, sApellidos:=SolicitudXML("APELLIDOS").ToString, sTelefono:=SolicitudXML("TELEFONO").ToString, sEmail:=SolicitudXML("USUARIO_EMAIL").ToString,
                                      sFax:=SolicitudXML("FAX").ToString, iTipoSolicitud:=iTipoSolicitud, bPortal:=True)
            Case "guardarsolicitud"
            Case Else '"realizarAccionPortal"
                dsRoles = New DataSet
                If Not dsXML.Tables("ROLES") Is Nothing Then _
                    dsRoles.Tables.Add(dsXML.Tables("ROLES").Copy)

                oInstancia.RealizarAccion(strToInt(SolicitudXML("IDACCION").ToString), , SolicitudXML("USUARIO_EMAIL").ToString, SolicitudXML.Item("COMENTARIO").ToString, dsRoles, SolicitudXML.Item("PROVEGS").ToString,
                                          SolicitudXML("CODCIA").ToString, SolicitudXML("DENCIA").ToString, SolicitudXML("NIFCIA").ToString, SolicitudXML("NOMBRE").ToString, SolicitudXML("APELLIDOS").ToString,
                                          SolicitudXML("TELEFONO").ToString, SolicitudXML("USUARIO_EMAIL").ToString, SolicitudXML("FAX").ToString, iTipoSolicitud, bPortal:=True, sIdiomaDefecto:=sIdi)
        End Select

    End Sub
    Private Sub Tratar_AprobacionRechazoMultiple(ByVal dsXML As DataSet, ByVal fileName As String)
        Dim oInstancia As Instancia = FSNServer.Get_Object(GetType(Instancia))
        Dim SolicitudXML As DataRow
        Dim lBloque, lAccion, lIDTiempoProc As Long
        Dim UsuCod, sIdi As String
        Dim bEjecutarAcciones As Boolean = False
        Dim iTipoSolicitud As TiposDeDatos.TipoDeSolicitud
        Dim tipoProcesamiento As TipoProcesamientoXML

        tipoProcesamiento = CType(dsXML.Tables("SOLICITUD").Rows(0)("TIPO_PROCESAMIENTO_XML"), TipoProcesamientoXML)

        SolicitudXML = dsXML.Tables("SOLICITUD").Rows(0)
        lIDTiempoProc = DBNullToLong(SolicitudXML("IDTIEMPOPROC"))
        lAccion = strToLong(SolicitudXML("IDACCION").ToString)
        lBloque = strToLong(SolicitudXML("BLOQUE_ORIGEN").ToString)
        UsuCod = If(tipoProcesamiento = TipoProcesamientoXML.FSNWeb_PM_AprobacionRechazoMultiple, SolicitudXML("CODIGOUSUARIO").ToString, SolicitudXML("PROVEGS").ToString)
        iTipoSolicitud = CType(SolicitudXML("TIPO_DE_SOLICITUD").ToString, TiposDeDatos.TipoDeSolicitud)
        sIdi = SolicitudXML("USUARIO_IDIOMA").ToString
        oInstancia.ID = strToLong(SolicitudXML("INSTANCIA").ToString)
        oInstancia.ActualizarEstadoValidacion(EstadoValidacion.EnCola, lAccion & "," & lBloque & "," & If(tipoProcesamiento = TipoProcesamientoXML.FSNWeb_PM_AprobacionRechazoMultiple, UsuCod, SolicitudXML("PROVEGS").ToString))

        Dim NumberFormat As System.Globalization.NumberFormatInfo = ObtenerNumberFormat(SolicitudXML("THOUSANFMT").ToString, SolicitudXML("DECIAMLFMT").ToString,
                                                                                        SolicitudXML("PRECISIONFMT").ToString, SolicitudXML("REFCULTURAL").ToString)
        Try
            oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, iFecha:=EstadoProcesamiento.IniValidaciones)
            oInstancia.Cargar(SolicitudXML("USUARIO_IDIOMA").ToString)
            If oInstancia.Grupos Is Nothing Then
                If tipoProcesamiento = TipoProcesamientoXML.FSNWeb_PM_AprobacionRechazoMultiple Then
                    oInstancia.CargarCamposInstancia(SolicitudXML("USUARIO_IDIOMA").ToString, sUsuario:=SolicitudXML("USUARIO").ToString, bNuevoWorkfl:=True)
                Else
                    oInstancia.CargarCamposInstancia(SolicitudXML("USUARIO_IDIOMA").ToString, sProve:=SolicitudXML("PROVEGS").ToString, bNuevoWorkfl:=True)
                End If
            End If
            Dim oAccion As Accion = FSNServer.Get_Object(GetType(Accion))
            oAccion.Id = lAccion
            oAccion.CargarAccion(SolicitudXML("USUARIO_IDIOMA").ToString)
            Dim bValidacion As Boolean = True
            If tipoProcesamiento = TipoProcesamientoXML.FSNWeb_PM_AprobacionRechazoMultiple Then
                ' Si es autoasignación, asignarle el rol y comprobar que la solicitud no está asignada ya a otro usuario
                oInstancia.ActualizarEstadoValidacion(EstadoValidacion.YaAsignadaAOtroUsuario, Nothing)
                bValidacion = oInstancia.ComprobarAsignacion(SolicitudXML("USUARIO").ToString, SolicitudXML("USUARIO_IDIOMA").ToString)
            Else
                'llamar a devolveretapaactual de portal
                oInstancia.DevolverEtapaActualPortal(SolicitudXML("USUARIO_IDIOMA").ToString, SolicitudXML("PROVEGS").ToString)
            End If
            ' Comprobar Próxima Etapa
            Dim sRolPorWebService As String = "-1"
            Dim dsSiguienteEtapa As DataSet = Nothing
            If bValidacion Then
                oInstancia.ActualizarEstadoValidacion(EstadoValidacion.SiguienteEtapa, Nothing)
                If tipoProcesamiento = TipoProcesamientoXML.FSNWeb_PM_AprobacionRechazoMultiple Then
                    dsSiguienteEtapa = oInstancia.ComprobarTieneSiguienteEtapa(lAccion, SolicitudXML("USUARIO").ToString, SolicitudXML("USUARIO_IDIOMA").ToString, sRolPorWebService)
                Else
                    dsSiguienteEtapa = oInstancia.DevolverSiguientesEtapasPortal(lAccion, SolicitudXML("USUARIO_IDIOMA").ToString, lBloque, oInstancia.RolActual)
                End If
                bValidacion = (dsSiguienteEtapa.Tables.Count > 1)
            End If
            ' Comprobar Próximos participantes
            If bValidacion Then
                oInstancia.ActualizarEstadoValidacion(EstadoValidacion.ProximosParticipantes, Nothing)
                If Not (sRolPorWebService = "-1") Then ' Los participantes se deciden en Web Service.
                    Dim sBloqueRolPorWebService() As String
                    sBloqueRolPorWebService = Split(sRolPorWebService, ",")
                    Dim sArrRolPorWebService() As String
                    Dim lRolPorWebService As String

                    Dim oDTPMParticipantes As New DataTable
                    oDTPMParticipantes.Columns.Add("ID", System.Type.GetType("System.Int32"))
                    oDTPMParticipantes.Columns.Add("ROL", System.Type.GetType("System.Int32"))
                    oDTPMParticipantes.Columns.Add("PER", System.Type.GetType("System.String"))
                    'esto es un caso muy concreto de gestamp, nunca habra proveedor implicado

                    Dim keys(1) As DataColumn
                    keys(0) = oDTPMParticipantes.Columns("ROL")
                    keys(1) = oDTPMParticipantes.Columns("PER")
                    oDTPMParticipantes.PrimaryKey = keys

                    For Each oRow As DataRow In dsSiguienteEtapa.Tables(0).Rows
                        lRolPorWebService = -1
                        For pos As Integer = 0 To UBound(sBloqueRolPorWebService)
                            sArrRolPorWebService = Split(sBloqueRolPorWebService(pos), "@")

                            If sArrRolPorWebService(0) = CStr(oRow.Item("BLOQUE")) Then
                                lRolPorWebService = CLng(sArrRolPorWebService(1))
                                Exit For
                            End If
                        Next
                        If lRolPorWebService > -1 Then
                            bValidacion = Instancia_LlamadaWebService(lRolPorWebService, oInstancia.ID, oRow.Item("BLOQUE"), oDTPMParticipantes)
                        Else 'Los participantes se deciden en pantalla.  Paralelo: 1 por web y 1 ahora/campo/pestaÃ±a/etc
                            bValidacion = oInstancia.ComprobarParticipantes(SolicitudXML("USUARIO_IDIOMA").ToString, lBloque)
                        End If
                        If Not bValidacion Then Exit For
                    Next

                    If bValidacion Then
                        'Comprobar si hay algún rol asignable directamente (si tiene sólo una persona participante)
                        'Obtenemos los distintos roles
                        Dim dvParticipantes As New DataView(oDTPMParticipantes)
                        Dim dtRoles As DataTable = dvParticipantes.ToTable(True, "ROL")
                        'Se mira si para cada rol hay una única persona
                        For Each drRol As DataRow In dtRoles.Rows
                            'Primero se limpia una posible asignación hecha durante una validación que haya resultado negativa 
                            Dim oRol As Rol = FSNServer.Get_Object(GetType(Rol))
                            oRol.Id = drRol("ROL")
                            oRol.IdInstancia = oInstancia.ID
                            oRol.AsignarRol(Nothing)

                            Dim drPersonas As DataRow() = oDTPMParticipantes.Select("ROL=" & drRol("ROL"))
                            If drPersonas.Count = 1 Then
                                'Si sólo hay una persona para ese rol se asigna directamente
                                oRol.AsignarRol(drPersonas(0)("PER"))
                            End If
                        Next

                        'El resultado de dicha llamada se guardarÃ¡ en PM_COPIA_PARTICIPANTES y se pasarÃ¡ adelante con las demÃ¡s validaciones. 
                        oInstancia.Actualizar_ParticipantesExternos(oDTPMParticipantes)
                    End If

                Else 'Los participantes se deciden en pantalla
                    If oAccion.TipoRechazo = 0 Then 'Si es un rechazo no los validamos pq va a etapas anteriores.
                        bValidacion = oInstancia.ComprobarParticipantes(SolicitudXML("USUARIO_IDIOMA").ToString, lBloque)
                    End If
                End If
            End If
            ' Comprobar Campos Obligatorios
            If bValidacion AndAlso oAccion.CumpOblRol Then
                If tipoProcesamiento = TipoProcesamientoXML.FSNWeb_PM_AprobacionRechazoMultiple Then
                    oInstancia.CargarCamposDesglose(SolicitudXML("USUARIO_IDIOMA").ToString, SolicitudXML("USUARIO").ToString)
                Else
                    oInstancia.CargarCamposDesglosePortal(SolicitudXML("USUARIO_IDIOMA").ToString, SolicitudXML("PROVEGS").ToString)
                End If
                oInstancia.ActualizarEstadoValidacion(EstadoValidacion.CamposObligatorios, Nothing)
                bValidacion = oInstancia.ComprobarCamposObligatorios(SolicitudXML("USUARIO_IDIOMA").ToString)
            End If
            ' Comprobar Precondiciones
            If bValidacion Then
                oInstancia.ActualizarEstadoValidacion(EstadoValidacion.Precondiciones, Nothing)
                bValidacion = oInstancia.ComprobarPrecondiciones(lAccion, SolicitudXML("USUARIO_IDIOMA").ToString)
            End If
            ' Comprobar Bloqueos Solicitudes Padre
            If bValidacion Then
                oInstancia.ActualizarEstadoValidacion(EstadoValidacion.ControlImportes, Nothing)
                bValidacion = oInstancia.ComprobarBloqueosSolicitudesPadre(SolicitudXML("USUARIO_IDIOMA").ToString, NumberFormat)
            End If
            ' Comprobar Validaciones Integración
            If bValidacion Then
                oInstancia.ActualizarEstadoValidacion(EstadoValidacion.Integracion, Nothing)
                oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, iFecha:=EstadoProcesamiento.IniMapper)
                'DE MOMENTO LAS VALIDACIONES SE HACEN EN EL ALTA DE LAS FACTURAS EN PORTAL PERO NO ESTA HECHO PARA CAMBIOS DE ETAPA
                If tipoProcesamiento = TipoProcesamientoXML.FSNWeb_PM_AprobacionRechazoMultiple Then
                    bValidacion = oInstancia.ComprobarMapper(SolicitudXML("USUARIO_IDIOMA").ToString, SolicitudXML("USUARIO").ToString, lAccion, dsSiguienteEtapa)
                End If
            End If

            If bValidacion Then
                oInstancia.ActualizarEstadoValidacion(EstadoValidacion.Completada, lAccion & "," & lBloque & "," & If(tipoProcesamiento = TipoProcesamientoXML.FSNWeb_PM_AprobacionRechazoMultiple, UsuCod, SolicitudXML("PROVEGS").ToString))
                oInstancia.Actualizar_En_proceso(1, lBloque)
                oInstancia.ActualizarEstadoValidacion(EstadoValidacion.SinEstado, Nothing)

                If iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras Then
                    If (oInstancia.AccionSinControlDisponible(lAccion)) Then
                        'Es un rechazo o anulacion / Eliminaria de las partidas el solicitado anteriormente guardado
                        Dim oPRES5 As Partida = FSNServer.Get_Object(GetType(Partida))
                        oPRES5.ActualizarImportesPartidaRechazoAnulacion(oInstancia.ID, sIdi)
                    End If
                End If
                TratarInstancia(dsXML, fileName)
            Else
                oInstancia.Actualizar_En_proceso(0, lBloque)
                oInstancia.Actualizar_En_proceso(0)
                listaProcesamientoXML.Remove(oInstancia.ID)
                If File.Exists(directorioEnTratamiento.FullName & "\" & fileName) Then _
                    File.Delete(directorioEnTratamiento.FullName & "\" & fileName)

                Try
                    oInstancia.NotificarErrorValidacion(SolicitudXML("USUARIO_IDIOMA").ToString, SolicitudXML("USUARIO_EMAIL").ToString,
                                                        SolicitudXML("TIPOEMAIL").ToString, NumberFormat, SolicitudXML("DATEFMT").ToString)
                Catch ex As Exception
                    Throw ex
                End Try
            End If
            oInstancia = Nothing
        Catch ex As Exception
            Dim sError As String = ex.Message
            If File.Exists(directorioError.FullName & "\" & fileName) Then File.Delete(directorioError.FullName & "\" & fileName)
            File.Move(directorioEnTratamiento.FullName & "\" & fileName, directorioError.FullName & "\" & fileName)
            oInstancia.Actualizar_En_proceso(0, lBloque)
            oInstancia.Actualizar_En_proceso(0)
            listaProcesamientoXML.Remove(oInstancia.ID)
            Dim oErrores As Errores
            oErrores = FSNServer.Get_Object(GetType(Errores))
            Try
                oErrores.Create("FSNTratamientoInstancias\Tratar_AprobacionRechazoMultiple", SplitXml(fileName)(0).ToString, ex.GetType().FullName, ex.Message, ex.StackTrace, SplitXml(fileName)(1).ToString, "")
                oErrores = Nothing
            Catch exError As Exception
                oErrores.SendErrorEmail(sError & " / " & exError.Message, ConfigurationManager.AppSettings("NombreServicio"), "Tratar_AprobacionRechazoMultiple", SplitXml(fileName)(0).ToString, SplitXml(fileName)(1).ToString, AppDomain.CurrentDomain.BaseDirectory & "\FromInfo.txt")
            End Try
        End Try
    End Sub
    Private Function ObtenerNumberFormat(ByVal ThousanFmt As String, ByVal DecimalFmt As String, ByVal PrecisionFmt As String, ByVal RefCultural As String) As System.Globalization.NumberFormatInfo
        Dim moNumberFormat As System.Globalization.NumberFormatInfo = New System.Globalization.CultureInfo(RefCultural, False).NumberFormat
        If Not PrecisionFmt = Nothing Then
            moNumberFormat.NumberDecimalDigits = PrecisionFmt
        ElseIf PrecisionFmt = 0 Then
            moNumberFormat.NumberDecimalDigits = PrecisionFmt
        End If
        If Not DecimalFmt = Nothing Then moNumberFormat.NumberDecimalSeparator = DecimalFmt
        If Not ThousanFmt = Nothing Then moNumberFormat.NumberGroupSeparator = ThousanFmt

        'En bbdd usuario thousanfmt=null decimalfmt=null no entrara nunca en el if. Se usara la configuraciÃ³n del windows
        'thousanfmt=. decimalfmt=null   ->  moNumberFormat #.##0.00
        'thousanfmt=null decimalfmt=,   ->  moNumberFormat #,##0,00
        If moNumberFormat.NumberDecimalSeparator = moNumberFormat.NumberGroupSeparator Then
            '1234.56 mil doscientos treinta y cuatro con cincuenta y seis
            '   si formato #.##0.00 devuelve    1.23
            '   si formato #,##0,00 devuelve    1,23
            If DecimalFmt Is Nothing Then
                'En bbdd usuario decimalfmt=null
                If moNumberFormat.NumberDecimalSeparator = moNumberFormat.CurrencyDecimalSeparator Then
                    moNumberFormat.NumberDecimalSeparator = moNumberFormat.CurrencyGroupSeparator
                Else
                    moNumberFormat.NumberDecimalSeparator = moNumberFormat.CurrencyDecimalSeparator
                End If
            Else
                'En bbdd usuario thousanfmt=null
                If moNumberFormat.NumberGroupSeparator = moNumberFormat.CurrencyGroupSeparator Then
                    moNumberFormat.NumberGroupSeparator = moNumberFormat.CurrencyDecimalSeparator
                Else
                    moNumberFormat.NumberGroupSeparator = moNumberFormat.CurrencyGroupSeparator
                End If
            End If
        End If
        Return moNumberFormat
    End Function
    Private Sub GestionarXMLEnEspera(ByVal oInstancia As Instancia, ByVal lBloque As Long, ByVal fileName As String)
        File.Copy(directorioEnTratamiento.FullName & "\" & fileName, directorioBackup.FullName & "\" & fileName, True)
        File.Delete(directorioEnTratamiento.FullName & "\" & fileName)

        If colaXMLEnEspera.ContainsKey(oInstancia.ID) Then
            Dim newFileName As String = colaXMLEnEspera(oInstancia.ID).Peek
            If File.Exists(directorioEnEspera.FullName & "\" & newFileName) Then
                If File.Exists(directorioEnTratamiento.FullName & "\" & newFileName) Then File.Delete(directorioEnTratamiento.FullName & "\" & newFileName)
                File.Move(directorioEnEspera.FullName & "\" & newFileName, directorioEnTratamiento.FullName & "\" & newFileName)
            End If
            colaXMLEnEspera(oInstancia.ID).Dequeue()
            If colaXMLEnEspera(oInstancia.ID).Count = 0 Then colaXMLEnEspera.Remove(oInstancia.ID)

            ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf ProcesarXML), newFileName)
        Else 'Si no hay mas que tramitar para la instancia, ponemos en proceso a 0 y quitamos de la lista de instancias en procesamiento
            If Not lBloque = 0 Then oInstancia.Actualizar_En_proceso(0, lBloque)
            oInstancia.Actualizar_En_proceso(0)
            listaProcesamientoXML.Remove(oInstancia.ID)
        End If
    End Sub
#Region "Asignación de roles relacionado con un servicio externo"
    ''' <summary>
    ''' Gestamp tiene 100 etapas a las q ir desde peticionario en varios flujos. Se quitan para q solo sea una etapa y un web 
    ''' service desde integraciÃ³n nos dice cuales son las personas de autoasignaciÃ³n.
    ''' </summary>
    ''' <param name="lRolPorWebService">Rol para 100 etapas.</param>
    ''' <param name="Instancia">Instancia</param>
    ''' <param name="BloqueDestino">esa unica etapa q antes eran 100</param>
    ''' <param name="oDTPMParticipantes">Por si tiene q ir a varias etapas, los participantes cambian de uno a otro, hay q ir guardandolos para updatar mas tarde.</param> 
    ''' <returns>Si ha podido o no devolver una lista de personas separadas por #</returns>
    ''' <remarks>Llamada desde: RealizarAccion_Guardar ; Tiempo maximo: 0,3</remarks>
    Private Function Instancia_LlamadaWebService(ByVal lRolPorWebService As Long, ByVal Instancia As Long, ByVal BloqueDestino As Long, _
                                                 ByRef oDTPMParticipantes As DataTable) As Boolean
        Dim oValorCampo As Object
        oValorCampo = LlamarWSServicio(ConfigurationManager.AppSettings("UrlParticipantesExterno"), "oXMLDocCadena", "", "oXMLDocInstancia", Instancia, "oXMLDocBLoque", BloqueDestino, "DevolverAprobadorResponse")

        'oValorCampo sera una lista de personas separadas por #. Ejemplo: 37HA#C227K#
        Dim dtNewRow As DataRow
        Dim sAux() As String = Split(oValorCampo, "#")
        For i As Integer = 0 To UBound(sAux) - 1 Step 1
            dtNewRow = oDTPMParticipantes.NewRow

            dtNewRow.Item("ID") = i + 1
            dtNewRow.Item("ROL") = lRolPorWebService
            dtNewRow.Item("PER") = sAux(i)

            oDTPMParticipantes.Rows.Add(dtNewRow)
        Next

        Return (Not String.IsNullOrEmpty(oValorCampo))
    End Function
    ''' <summary>
    ''' Ejecuta la llamada a un webservice q no incluimos en el proyecto.
    ''' </summary>
    ''' <param name="sUrl">url del servicio con la funciÃ³n a realizar. Ejemplo:http://localhost/FSNWebService_31900_9/GES_Tallent_WS.asmx/DevolverAprobador </param>
    ''' <param name="nomEntradaXml">Nombre de la variable Fichero q usa el webservice a realizar.</param>
    ''' <param name="valorXml">Para gestamp Tarea 3400, es vacio</param> 
    ''' <param name="nomEntradaInstancia">Nombre de la variable Instancia q usa el webservice a realizar.</param>
    ''' <param name="valorInstancia">Instancia q usa el webservice a realizar.</param>
    ''' <param name="nomEntradaBloque">Nombre de la variable "Bloque destino q usa el webservice a realizar"</param>
    ''' <param name="valorBloque">Bloque q usa el webservice a realizar</param>  
    ''' <param name="nomResult">Nombre de la variable del webservice donde se deja el resultado</param>
    ''' <returns>Lo q responde el webservice</returns>
    ''' <remarks>Llamada desde: ComprobarCondiciones    Instancia_LlamadaWebService ; Tiempo mÃƒÂ¡ximo: 0,3</remarks>
    Private Function LlamarWSServicio(ByVal sUrl As String, ByVal nomEntradaXml As String, ByVal valorXml As String, _
                                      ByVal nomEntradaInstancia As String, ByVal valorInstancia As String, _
                                      ByVal nomEntradaBloque As String, ByVal valorBloque As String, ByVal nomResult As String) As String
        Dim valorResult As String = ""
        Dim url, metodo, sSoapAction, sXml As String
        Dim num As Integer

        num = sUrl.IndexOf(".asmx")
        url = sUrl.Substring(0, num + 5)
        metodo = sUrl.Substring(num + 6)
        sSoapAction = "http://tempuri.org/" + metodo

        sXml = "<?xml version=""1.0"" encoding=""utf-8""?>"
        sXml = sXml + "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">"
        sXml = sXml + "<soap:Body>"
        sXml = sXml + "<" + metodo + " xmlns=""http://tempuri.org/"">"
        sXml = sXml + "<" + nomEntradaXml + ">" + valorXml + "</" + nomEntradaXml + ">"
        sXml = sXml + "<" + nomEntradaInstancia + ">" + valorInstancia + "</" + nomEntradaInstancia + ">"
        sXml = sXml + "<" + nomEntradaBloque + ">" + valorBloque + "</" + nomEntradaBloque + ">"
        sXml = sXml + "</" + metodo + ">"
        sXml = sXml + "</soap:Body>"
        sXml = sXml + "</soap:Envelope>"

        Dim targetURI As New Uri(url)
        Dim req As System.Net.HttpWebRequest = DirectCast(System.Net.WebRequest.Create(targetURI), System.Net.HttpWebRequest)
        req.Headers.Add("SOAPAction", sSoapAction)
        req.ContentType = "text/xml; charset=""utf-8"""
        req.Accept = "text/xml"
        req.Method = "POST"
        Dim stm As Stream = req.GetRequestStream
        Dim stmw As New StreamWriter(stm)
        stmw.Write(sXml)
        stmw.Close()
        stm.Close()

        'Se realiza la llamada al WS
        Dim response As System.Net.WebResponse
        response = req.GetResponse()
        Dim responseStream As Stream = response.GetResponseStream

        Dim sr As New StreamReader(responseStream)
        Dim soapResult As String
        'Recogemos la respuesta del WS
        soapResult = sr.ReadToEnd
        Dim xmlResponse As New System.Xml.XmlDocument
        xmlResponse.LoadXml(soapResult)
        Dim xmlResult As System.Xml.XmlNode
        xmlResult = xmlResponse.GetElementsByTagName(nomResult).Item(0)
        'Recojo el valor del indicador de error
        If Not xmlResult Is Nothing Then valorResult = xmlResult.InnerText

        Return valorResult
    End Function
#End Region
    Protected Overrides Sub OnStop()
        FileSystemEventHandler_XMLEnEspera_Rename.Dispose()
    End Sub

    ''' <summary>
    ''' Los ficheros de fsnweb son Usuario#Instancia#Bloque
    ''' Los ficheros de portal son Proveeedor#Usuario#Instancia#Bloque
    ''' El program espera un arry de string q cumpla 0->usu  1->Instancia  2->Bloque
    ''' </summary>
    ''' <param name="fileName">Nombre del fichero</param>
    ''' <returns>String con 0->usu  1->Instancia  2->Bloque</returns>
    Private Function SplitXml(ByVal fileName As String) As String()
        Dim infoXMLFile(2) As String

        If Split(fileName, "#").Length = 3 Then
            infoXMLFile = Split(fileName, "#")
        Else 'Portal
            infoXMLFile(0) = Split(fileName, "#")(1) 'Usu
            infoXMLFile(1) = Split(fileName, "#")(2) 'Instancia
            infoXMLFile(2) = Split(fileName, "#")(3) 'Bloque
        End If

        Return infoXMLFile
    End Function

End Class