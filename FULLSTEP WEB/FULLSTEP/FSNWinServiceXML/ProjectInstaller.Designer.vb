﻿<System.ComponentModel.RunInstaller(True)> Partial Class ProjectInstaller
    Inherits System.Configuration.Install.Installer

    'Installer overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.FSNWinServiceXMLProcessInstaller = New System.ServiceProcess.ServiceProcessInstaller()
        Me.FSNWinServiceXMLInstaller = New System.ServiceProcess.ServiceInstaller()
        '
        'FSNWinServiceXMLProcessInstaller
        '
        Me.FSNWinServiceXMLProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem
        Me.FSNWinServiceXMLProcessInstaller.Password = Nothing
        Me.FSNWinServiceXMLProcessInstaller.Username = Nothing
        '
        'FSNWinServiceXMLInstaller
        '
        Me.FSNWinServiceXMLInstaller.Description = "Servicio de tratamiento de instancias de FULLSTEP"
        Me.FSNWinServiceXMLInstaller.ServiceName = "FSNWinServiceXML"
        Me.FSNWinServiceXMLInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic
        '
        'ProjectInstaller
        '
        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.FSNWinServiceXMLProcessInstaller, Me.FSNWinServiceXMLInstaller})

    End Sub
    Friend WithEvents FSNWinServiceXMLProcessInstaller As System.ServiceProcess.ServiceProcessInstaller
    Friend WithEvents FSNWinServiceXMLInstaller As System.ServiceProcess.ServiceInstaller

End Class
