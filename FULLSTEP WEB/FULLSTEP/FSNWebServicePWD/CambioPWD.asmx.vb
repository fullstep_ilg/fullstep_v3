﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports Fullstep.FSNLibrary
Imports Fullstep.FSNServer
'Imports Fullstep.PMServer
Imports System.DirectoryServices
'<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
'<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/FSNWebServicePWD/CambioPWD")> _
<ToolboxItem(False)> _
Public Class CambioPWD
    Inherits System.Web.Services.WebService
    Private paramGenerales As TiposDeDatos.ParametrosGenerales
    Private m_FSNServer As New Fullstep.FSNServer.Root

    ''' <summary>
    ''' Cambiar Login de un usuario
    ''' </summary>
    ''' <param name="adm">Si es administrador o no</param>
    ''' <param name="usu">usuario</param>
    ''' <param name="PWDActual">password</param>
    ''' <param name="usuNuevo">usuario nuevo</param>
    ''' <param name="PWDNueva">password nueva</param>
    ''' <param name="FechaPWD_year">año fecha con la q se encripta la nueva password</param>
    ''' <param name="FechaPWD_month">mes fecha con la q se encripta la nueva password</param>
    ''' <param name="FechaPWD_day">día fecha con la q se encripta la nueva password</param>
    ''' <param name="FechaPWD_hour">hora fecha con la q se encripta la nueva password</param>
    ''' <param name="FechaPWD_min">minutos fecha con la q se encripta la nueva password</param>
    ''' <param name="FechaPWD_sec">segundos fecha con la q se encripta la nueva password</param>
    ''' <param name="iChange">0 todas las llamadas</param>
    ''' <param name="Mail">Mail todas las llamadas</param>
    ''' <param name="FechaAnteriorPWd_year">año fecha con la q se encripto la password a cambiar</param>
    ''' <param name="FechaAnteriorPWd_month">mes fecha con la q se encripto la password a cambiar</param>
    ''' <param name="FechaAnteriorPWd_day">día fecha con la q se encripto la password a cambiar</param>
    ''' <param name="FechaAnteriorPWd_hour">hora fecha con la q se encripto la password a cambiar</param>
    ''' <param name="FechaAnteriorPWd_min">minutos fecha con la q se encripto la password a cambiar</param>
    ''' <param name="FechaAnteriorPWd_sec">segundos fecha con la q se encripto la password a cambiar</param>
    ''' <returns>Resultado del cambio</returns>
    ''' <remarks>Llamada desde: GS/CGestorSeguridad/PostWebserviceLDAP;    FSNWeb\App_Master\Cabecera.Master.vb\btnAceptarPWD_Click     
    ''' FSNWeb\App_Master\Login.master.vb\btnAceptarPWD_Click              PMWeb\script\_common\CambiarPWD.aspx.vb\cmdAceptarPWD_Click; Tiempo maximo:0</remarks>
    <WebMethod()> _
    Public Function CambiarLogin(ByVal adm As Integer, _
                                  ByVal usu As String, _
                                  ByVal PWDActual As String, _
                                  ByVal usuNuevo As String, _
                                  ByVal PWDNueva As String, _
                                  ByVal FechaPWD_year As Integer, _
                                  ByVal FechaPWD_month As Integer, _
                                  ByVal FechaPWD_day As Integer, _
                                  ByVal FechaPWD_hour As Integer, _
                                  ByVal FechaPWD_min As Integer, _
                                  ByVal FechaPWD_sec As Integer, _
                                  ByVal iChange As Integer, _
                                  ByVal Mail As String, _
                                  ByVal FechaAnteriorPWd_year As Integer, _
                                  ByVal FechaAnteriorPWd_month As Integer, _
                                  ByVal FechaAnteriorPWd_day As Integer, _
                                  ByVal FechaAnteriorPWd_hour As Integer, _
                                  ByVal FechaAnteriorPWd_min As Integer, _
                                  ByVal FechaAnteriorPWd_sec As Integer) _
                                  As Integer
        Dim codError As Integer
        Dim PWDNuevaDesencriptada As String
        Dim PWDActualDesencriptada As String
        Dim PwdNuevaEncriptada As String
        Dim accesoGS As Boolean

        PWDNuevaDesencriptada = Server.UrlDecode(PWDNueva)
        PWDActualDesencriptada = Server.UrlDecode(PWDActual)

        Dim oUser As Fullstep.FSNServer.User
        oUser = m_FSNServer.Login(usu, PWDActualDesencriptada, False)
        If oUser.ResultadoLogin = TipoResultadoLogin.LoginOk Then
            oUser.LoadUserData(usu)
            accesoGS = oUser.AccesoGS
        End If

        codError = 0
        CargarParametrosGenerales()

        Dim FechaPWD As Date = New Date(FechaPWD_year, FechaPWD_month, FechaPWD_day, FechaPWD_hour, FechaPWD_min, FechaPWD_sec)
        PwdNuevaEncriptada = Encrypter.Encrypt(PWDNuevaDesencriptada, usuNuevo, True, Encrypter.TipoDeUsuario.Persona, FechaPWD)

        If ValidarRequerimientosComplejidad(usuNuevo, PWDNuevaDesencriptada, paramGenerales.gbCOMPLEJIDAD_PWD) = False Then
            codError = 2  'No cumple la politica de segurirad de contraseña
            GoTo NoCumpleAlgo
        End If

        codError = VigenciaMinimaContraseña(usu) 'Si 3 No se puede cambiar la contraseña todavia
        If codError <> 0 Then GoTo NoCumpleAlgo
        If paramGenerales.giHISTORICO_PWD > 0 Then
            codError = ComprobarHistorialContraseña(usu, PWDNuevaDesencriptada) 'Si 4 se ha repetido la contraseña
            If codError <> 0 Then GoTo NoCumpleAlgo
        End If
        codError = LongitudMinimaPWD(PWDNuevaDesencriptada) 'No llega al mínimo de 1 caracter
        If codError <> 0 Then GoTo NoCumpleAlgo

        Try
            If codError = 0 Then
                Dim CodErrorCL As Object
                CodErrorCL = m_FSNServer.CambiarPwd(adm, usu, PwdNuevaEncriptada, FechaPWD, iChange, usuNuevo, paramGenerales.giHISTORICO_PWD)
                If CodErrorCL = 0 Then codError = 0 Else codError = 9
            End If
            If codError = 0 Then
                If paramGenerales.giWinSecurityWeb = TiposDeAutenticacion.UsuariosLocales OrElse (paramGenerales.giWinSecurity = TiposDeAutenticacion.UsuariosLocales AndAlso accesoGS) Then

                    Dim AdminUsers As New Fullstep.FSNLibraryCOM.AdminUsers

                    AdminUsers.WinSecAdminUser = m_FSNServer.TipoAcceso.gWinSecAdminUser
                    AdminUsers.WinSecAdminPwd = m_FSNServer.TipoAcceso.gWinSecAdminPwd
                    AdminUsers.Instancia = m_FSNServer.DBServidor & "\" & m_FSNServer.DBName

                    AdminUsers.Codigo = usu

                    AdminUsers.PWDDes = PWDNuevaDesencriptada
                    AdminUsers.PWDOld = PWDActualDesencriptada

                    Dim StrResultado As String = ""

                    codError = AdminUsers.CambiarLogin(StrResultado)

                    If codError >= 9 Then
                        Dim ErrorAdminUsers As Errores
                        ErrorAdminUsers = m_FSNServer.Get_Object(GetType(Errores))
                        ErrorAdminUsers.Create("CambioPWD.asmx.vb\CambiarLogin", StrResultado, usu)
                        ErrorAdminUsers = Nothing
                    End If
                End If
            End If
Fin_Funcion:
            Return codError
            Exit Function
        Catch ex As Exception
            Return 9
            Exit Function
        End Try
NoCumpleAlgo:
        Return codError
    End Function
    Private Sub CargarParametrosGenerales()
        paramGenerales = m_FSNServer.TipoAcceso
    End Sub
    Private Function ValidarRequerimientosComplejidad(ByVal usu As String, ByVal PWDNueva As String, ByVal ComplejidadPWD As Boolean) As Boolean
        Dim mayusculas As New System.Text.RegularExpressions.Regex("[A-Z]")
        Dim minusculas As New System.Text.RegularExpressions.Regex("[a-z]")
        Dim digitos As New System.Text.RegularExpressions.Regex("[0-9]")
        Dim noalfabeticos As New System.Text.RegularExpressions.Regex("[^a-zA-Z0-9]")
        Dim contador As Integer
        contador = 0
        If ComplejidadPWD = False Then
            Return True
        End If
        If mayusculas.Matches(PWDNueva).Count < 1 Then
            contador += 1
        End If
        If minusculas.Matches(PWDNueva).Count < 1 Then
            contador += 1
        End If
        If digitos.Matches(PWDNueva).Count < 1 Then
            contador += 1
        End If
        If noalfabeticos.Matches(PWDNueva).Count < 1 Then
            contador += 1
        End If
        If contador > 1 Then
            Return False
        End If
        For i = 1 To Len(PWDNueva) - 3
            If InStr(1, usu, Mid(PWDNueva, i, 4), CompareMethod.Text) Then
                Return False 'No puede tener partes del usuario 
            End If
        Next i
        Return True
    End Function
    Private Function VigenciaMinimaContraseña(ByVal usu As String) As Integer
        If m_FSNServer.CumpleVigenciaPWD(usu) Then
            Return 0
        Else
            Return 3 'No se puede cambiar todavia la contraseña
        End If
    End Function
    Private Function LongitudMinimaPWD(ByVal PWDnueva) As Integer
        Dim bCumpleLongitudMinima As Boolean
        Dim codError As Integer = 1 'No puede ser menor que el MIN_SIZE_PWD

        If paramGenerales.gbCOMPLEJIDAD_PWD Then
            If paramGenerales.giMIN_SIZE_PWD > 8 Then 'Si hay complejidad, como mínimo el tamaño de la contraseña será 8
                bCumpleLongitudMinima = (Len(PWDnueva) >= paramGenerales.giMIN_SIZE_PWD)
            Else
                bCumpleLongitudMinima = (Len(PWDnueva) >= 8)
            End If
        ElseIf paramGenerales.giMIN_SIZE_PWD > 0 Then
            bCumpleLongitudMinima = (Len(PWDnueva) >= paramGenerales.giMIN_SIZE_PWD)
        ElseIf paramGenerales.giMIN_SIZE_PWD = 0 Then
            If Len(PWDnueva) < 1 Then
                codError = 5 'Si no tiene puesto longitud minima tiene que ser como minimo 1 caracter
            Else
                bCumpleLongitudMinima = True
            End If
        End If

        If bCumpleLongitudMinima Then
            Return 0
        Else
            Return codError
        End If
    End Function
    Private Function ComprobarHistorialContraseña(ByVal usu As String, ByVal PWDnueva As String) As Integer
        If m_FSNServer.ComprobarPWDRepetido(usu, PWDnueva) Then
            Return 4 'Contraseña usada con anterioridad
        Else
            Return 0 'La contraseña no ha sido utilizado
        End If
    End Function
    ''' <summary>
    ''' Funcion que trata de simular server.URLDecode transforma la contraseña que viene ya transformada desde el GS
    ''' y del servicio y la trasforma a contraseña correcta de BBDD
    ''' </summary>
    ''' <param name="sEncodedURL">Contraseña a transformar</param>
    ''' <returns>String con cadena transformada</returns>
    ''' <remarks>Llamada desde=propia pagina; Tiempo máximo=0seg.</remarks>
    ''' 
    Public Function URLDecode(ByVal sEncodedURL As String) As String

        Dim iLoop As Integer
        Dim sRtn As String = ""
        Dim sTmp As String = ""



        If Len(sEncodedURL) > 0 Then
            For iLoop = 1 To Len(sEncodedURL)
                sTmp = Mid(sEncodedURL, iLoop, 1)
                sTmp = Replace(sTmp, "+", " ")

                If sTmp = "%" And Len(sEncodedURL) + 1 > iLoop + 2 Then
                    sTmp = Mid(sEncodedURL, iLoop + 1, 2)
                    sTmp = Chr(CDec("&H" & sTmp))
                    ' Increment loop by 2
                    iLoop = iLoop + 2
                End If
                sRtn = sRtn & sTmp
            Next iLoop

        End If
        URLDecode = sRtn
    End Function
    <WebMethod()> _
    Public Function LoginLDAP(ByVal UserName As String, ByVal UserPassword As String) As Integer


        ''Lee los datos del webconfig
        Dim sLDAPIpUrl As String = ConfigurationManager.AppSettings("LDAPIPURL").ToString
        Dim sLDAPRutaBase As String = ConfigurationManager.AppSettings("LDAPORGANIZATION").ToString
        Dim iLDAPAutenticacion As System.DirectoryServices.AuthenticationTypes = ConfigurationManager.AppSettings("LDAPAutenticacion").ToString
        Dim sRutaUsuarios As String = ConfigurationManager.AppSettings("LDAPRutaUsuarios").ToString
        Dim sDominioUsuarios As String = ConfigurationManager.AppSettings("LDAPDominioUsuarios").ToString
        Dim sRutaBusqueda As String = ConfigurationManager.AppSettings("LDAPRutaBusqueda").ToString
        Dim bBusquedaAdicional As Boolean = ConfigurationManager.AppSettings("LDAPObtenerResultados")
        Dim sFiltro As String = ConfigurationManager.AppSettings("LDAPFilter").ToString
        Dim sScope As System.DirectoryServices.SearchScope = ConfigurationManager.AppSettings("LDAPScope").ToString
        Dim sPropiedadesCargar() As String = Split(ConfigurationManager.AppSettings("LDAPProperties").ToString, ",")
        Dim sResultadoEsperado() As String = Split(ConfigurationManager.AppSettings("LDAPResultados").ToString, "#")


        Dim oResult As System.DirectoryServices.SearchResult
        Dim oResults As System.DirectoryServices.SearchResultCollection
        Dim sRutaCompletaLDAP As String = ""
        Dim sRutaUsu As String = ""
        Dim iResultados As Integer = 0
        Dim iCount As Integer = 0
        Dim bResultadoEncontrado As Boolean = True  ''Se mantiene a true salvo que se ejecute la consulta de búsqueda adicional y no encuentre resultados
        Dim bArrResultadoEncontrado() As Boolean


        'Obtención ruta búsqueda en el LDAP: "LDAP: //" + LDAPIPURL + "/" + LDAPRUTABusqueda  (si existe) + LDAPORGANIZATION 
        sRutaCompletaLDAP = sRutaBusqueda + "," + sLDAPRutaBase
        If Strings.Left(sRutaCompletaLDAP, 1) = "," Then sRutaCompletaLDAP = Strings.Right(sRutaCompletaLDAP, Len(sRutaCompletaLDAP) - 1)
        sRutaCompletaLDAP = "LDAP://" + sLDAPIpUrl + "/" + sRutaCompletaLDAP


        ''PASO 1.- Si no se indica la ruta de usuarios activos ni el dominio, se obtiene la ruta accediendo al LDAP y buscando el usuario. Se vuelca el dato a sRutaUsu
        If sRutaUsuarios = "" And sDominioUsuarios = "" Then
            Try
                Dim entry1 As New DirectoryEntry("LDAP://" + sLDAPIpUrl + "/" + sLDAPRutaBase, "", "", AuthenticationTypes.Anonymous)
                Dim search1 As DirectorySearcher = New DirectorySearcher(entry1)
                search1.SearchScope = SearchScope.Subtree  ''Debe buscar en todos los subdirectorios de la raiz 
                search1.Filter = "uid=" + UserName
                oResult = search1.FindOne ''Recupera la primera entrada que encuentre

                If (oResult Is Nothing) Then
                    Return 0  ''Si no encuentra el usuario sale con error.
                Else
                    sRutaUsu = oResult.Path  ''Ruta completa del usuario (LDAP: //dirIP/uid=xxx, ou=xx.....Raiz
                    sRutaUsu = Strings.Right(sRutaUsu, Len(sRutaUsu) - InStrRev(sRutaUsu, "/"))  ''Se queda con la parte uid=xx, ou=xx...raiz
                End If
            Catch
                Return 0
            End Try
        End If

        ''PASO 2.- Comprueba los datos del login del usuario
        'Si no se ha pasado por el punto 1, la ruta del usuario se obtiene del webconfig: uid=usu + LDAPRutaUsuarios + LDAPRutaBase
        If sRutaUsu = "" Then
            If sDominioUsuarios <> "" Then
                sRutaUsu = sDominioUsuarios + "\" + UserName
            Else
                sRutaUsu = UserName
            End If
            If sRutaUsuarios <> "" Then
                sRutaUsu = "uid=" + sRutaUsu + "," + sRutaUsuarios + "," + sLDAPRutaBase
            End If
        End If

        Dim entry As New DirectoryEntry(sRutaCompletaLDAP, sRutaUsu, UserPassword, iLDAPAutenticacion)

        ''Autenticación del usuario:
        Try
            Dim obj As Object = entry.NativeObject  ''Bind to the native AdsObject to force authentication.	
        Catch ex As Exception
            Return 0
        End Try


        ''PASO 3: Búsqueda adicional.
        Try
            If bBusquedaAdicional Then

                ReDim bArrResultadoEncontrado(UBound(sResultadoEsperado))  ''Se almacenará true o false para cada resultado que buscamos
                For i = 0 To UBound(bArrResultadoEncontrado)
                    bArrResultadoEncontrado(i) = False
                Next

                ''Sustituye "**" por el código del usuario en la cadena de compración
                For i = 0 To UBound(sResultadoEsperado)
                    sResultadoEsperado(i) = IIf(sResultadoEsperado(i).ToString <> "", Replace(sResultadoEsperado(i), "**", UserName), sResultadoEsperado(i).ToString)
                Next
                ''Sustituye "**" por el código del usuario en el filtro
                If sFiltro <> "" Then Replace(sFiltro, "**", UserName)

                Dim search As DirectorySearcher = New DirectorySearcher(entry)
                search.SearchScope = sScope  ''Ámbito de búsqueda indicado en el webconfig
                search.PropertiesToLoad.AddRange(sPropiedadesCargar)
                search.Filter = sFiltro ''Filtro indicado en el webconfig
                oResults = search.FindAll ''Recupera todos los resultados
                iCount = oResults.Count

                If iCount > 0 Then
                    For Each oResult In oResults
                        ''En la colección de resultados tendremos las propiedades cargadas.
                        For j = 0 To UBound(sPropiedadesCargar)
                            iResultados = oResult.Properties(sPropiedadesCargar(j)).Count
                            If sResultadoEsperado(j).ToString = "" Then ''No hay que buscar nada
                                bArrResultadoEncontrado(j) = True
                            Else
                                For i = 0 To iResultados - 1
                                    ''Comprueba si para esa propiedad el valor coincide con el que esperamos encontrar.
                                    If (InStr(oResult.Properties(sPropiedadesCargar(j)).Item(i).ToString(), sResultadoEsperado(j).ToString) > 0) Then
                                        bArrResultadoEncontrado(j) = True
                                        Exit For
                                    End If
                                Next
                            End If
                        Next
                    Next

                    For i = 0 To UBound(bArrResultadoEncontrado)
                        If bArrResultadoEncontrado(i) = False Then bResultadoEncontrado = False
                    Next
                Else
                    bResultadoEncontrado = False
                End If
            End If

            If bResultadoEncontrado Then  'Estará a true si el paso 2 ha ido OK y no se ha ejecutado el 3, o si el 3 ha devuelto OK
                Return 1
            Else
                Return 0
            End If

        Catch ex As Exception
            Return 0
        End Try

    End Function
End Class