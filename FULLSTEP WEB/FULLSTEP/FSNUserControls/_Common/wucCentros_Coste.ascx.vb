﻿Public Class wucCentros_Coste
    Inherits System.Web.UI.UserControl

    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
    Private _sValor As String
    Private _bVerUON As Boolean
    Private _sDesde As String
    Private _urlImagenCerrar As String = String.Empty
    Private _urlImagenCentrosCoste As String = String.Empty
    Private _urlImagenRaiz As String = String.Empty
    Private _urlImagenNodo As String = String.Empty
    Private _RutaBuscadorCentroCoste As String = String.Empty
    Private oTextos As DataTable

    Public Property Valor() As String
        Get
            Valor = _sValor
        End Get
        Set(ByVal Value As String)
            _sValor = Value
        End Set
    End Property
    Public Property VerUON() As Boolean
        Get
            VerUON = _bVerUON
        End Get
        Set(ByVal Value As Boolean)
            _bVerUON = Value
        End Set
    End Property
    ''' <summary>
    ''' Ruta para abrir el buscador de Empresa
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property urlImagenCerrar As String
        Get
            Return _urlImagenCerrar
        End Get
        Set(ByVal value As String)
            _urlImagenCerrar = value
        End Set
    End Property
    ''' <summary>
    ''' Ruta para abrir el buscador de Articulo
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property urlImagenCentrosCoste As String
        Get
            Return _urlImagenCentrosCoste
        End Get
        Set(ByVal value As String)
            _urlImagenCentrosCoste = value
        End Set
    End Property
    ''' <summary>
    ''' Ruta para mostrar la imagen raiz
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property urlImagenRaiz As String
        Get
            Return _urlImagenRaiz
        End Get
        Set(ByVal value As String)
            _urlImagenRaiz = value
        End Set
    End Property
    ''' <summary>
    ''' Ruta para mostrar la imagen de los nodos
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property urlImagenNodo As String
        Get
            Return _urlImagenNodo
        End Get
        Set(ByVal value As String)
            _urlImagenNodo = value
        End Set
    End Property
    ''' <summary>
    ''' Para saber si se solicita este user control desde las partidas presupuestarias, 
    ''' porque asi las consultas a la base de datos es diferente
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Desde() As String
        Get
            Desde = _sDesde
        End Get
        Set(ByVal Value As String)
            _sDesde = Value
        End Set
    End Property
    Private _DatosTreeView As DataSet
    ''' <summary>
    ''' Propiedad con los datos de centros de coste
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property DatosTreeView As DataSet
        Get
            Return _DatosTreeView
        End Get
        Set(ByVal value As DataSet)
            _DatosTreeView = value
        End Set
    End Property
    ''' <summary>
    ''' Propiedad con los textos de la pantalla
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property Textos() As DataTable
        Set(ByVal Value As DataTable)
            oTextos = Value
        End Set
    End Property
    ''' <summary>
    ''' Procedimiento que carga el Usercontrol
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack() Then
            FSNPageHeader.UrlImagenCabecera = _urlImagenCentrosCoste
            FSNPageHeader.TituloCabecera = oTextos(0)(1)

            Me.lblCentro.Text = oTextos(1)(1)
            Me.btnSeleccionar.Text = oTextos(2)(1)
            Me.btnCancelar.Text = oTextos(3)(1)

            CargarArbolCentros()

            Dim sClientTexts As String
            sClientTexts = ""
            sClientTexts += "var arrTextosML = new Array();"
            sClientTexts += "arrTextosML[0] = '" + oTextos(4)(1) + "';"
            sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)

            txtCentro.Attributes.Add("onkeyup", "FindClicked('" & txtCentro.ClientID & "','" & wdtCentros_Coste.ClientID & "')")
        End If
    End Sub
    ''' <summary>
    ''' Carga el arbol de los centros de coste dependiendo de si viene de las partidas presupuestarias o no
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarArbolCentros()
        Dim oDataTreeNodeBinding As Infragistics.Web.UI.NavigationControls.DataTreeNodeBinding

        oDataTreeNodeBinding = New Infragistics.Web.UI.NavigationControls.DataTreeNodeBinding
        With oDataTreeNodeBinding
            .DataMember = "Tabla_0"
            .TextField = "DEN"
            .KeyField = "ID"
        End With
        wdtCentros_Coste.DataBindings.Add(oDataTreeNodeBinding)

        oDataTreeNodeBinding = New Infragistics.Web.UI.NavigationControls.DataTreeNodeBinding
        With oDataTreeNodeBinding
            .DataMember = "Tabla_1"
            .TextField = "DEN"
            .KeyField = "COD"
        End With
        wdtCentros_Coste.DataBindings.Add(oDataTreeNodeBinding)
        wdtCentros_Coste.DataSource = _DatosTreeView
        wdtCentros_Coste.DataBind()

        wdtCentros_Coste.SelectionType = Infragistics.Web.UI.NavigationControls.NodeSelectionTypes.Single
        wdtCentros_Coste.EnableExpandOnClick = True
    End Sub
    ''' <summary>
    ''' evento que salta en el Databound de cada uno de los nodos
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub wdtCentros_Coste_NodeBound(ByVal sender As Object, ByVal e As Infragistics.Web.UI.NavigationControls.DataTreeNodeEventArgs) Handles wdtCentros_Coste.NodeBound
        Dim ds As DataSet
        Dim oRows() As DataRow = Nothing

        ds = DirectCast(sender.datasource, System.Data.DataSet).DefaultViewManager.DataSet

        Select Case e.Node.Level
            Case 1
                oRows = ds.Tables(e.Node.Level).Select("COD= '" & e.Node.Key & "'")
            Case 2
                oRows = ds.Tables(e.Node.Level).Select("UON1= '" & e.Node.ParentNode.Key & "' AND COD= '" & e.Node.Key & "'")
            Case 3
                oRows = ds.Tables(e.Node.Level).Select("UON1= '" & e.Node.ParentNode.ParentNode.Key & "' AND UON2= '" & e.Node.ParentNode.Key & "' AND COD= '" & e.Node.Key & "'")
            Case 4
                oRows = ds.Tables(e.Node.Level).Select("UON1= '" & e.Node.ParentNode.ParentNode.ParentNode.Key & "' AND UON2= '" & e.Node.ParentNode.ParentNode.Key & "' AND UON3= '" & e.Node.ParentNode.Key & "' AND COD='" & e.Node.Key & "'")
        End Select

        If e.Node.Level > 0 AndAlso oRows.Length > 0 Then
            If Not IsDBNull(oRows(0).Item("CENTRO_SM")) Then
                e.Node.ImageUrl = _urlImagenNodo
                e.Node.Target = 1
                e.Node.ParentNode.Expanded = True
            End If
        End If

        If e.Node.Level = 0 AndAlso Not _bVerUON Then
            e.Node.Text = oTextos(5).Item(1)
            e.Node.ImageUrl = _urlImagenRaiz
        End If
    End Sub
End Class