﻿Imports Fullstep.FSNLibrary.modUtilidades
Public Class wucPartidas
    Inherits System.Web.UI.UserControl

    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
    "<script language=""{0}"">{1}</script>"

    
    Private oTextos As DataTable
    Private _DatosTreeView As DataSet
    Private _DatosUons As DataSet
    Private _DatosPartidas As New DataSet
    Private _Culture As Globalization.CultureInfo
    Private _urlImagenPartidas As String = String.Empty
    Private _urlImagenBuscarCentro As String = String.Empty
    Private _urlImagenCentroCoste As String = String.Empty
    Private _urlImagenCentroCosteSmall As String = String.Empty
    Private _urlImagenSiguiente As String = String.Empty
    Private _urlImagenCandado As String = String.Empty
    Private _fechaInicio As Date
    Private _fechaFin As Date
    Private _noVigentes As Boolean
    Private _sValor As String
    Private _sPRES5 As String
    Private _sCentroCoste As String
    Private _sCentroCosteDen As String
    Private _bPlurianual As Boolean

    Public Event eventBuscarPartidas()


    Public Property urlImagenSiguiente As String
        Get
            Return _urlImagenSiguiente
        End Get
        Set(ByVal value As String)
            _urlImagenSiguiente = value
        End Set
    End Property
    Public Property urlImagenCandado As String
        Get
            Return _urlImagenCandado
        End Get
        Set(ByVal value As String)
            _urlImagenCandado = value
        End Set
    End Property

    Public Property urlImagenBuscarCentro As String
        Get
            Return _urlImagenBuscarCentro
        End Get
        Set(ByVal value As String)
            _urlImagenBuscarCentro = value
        End Set
    End Property

    Public Property urlImagenCentroCosteSmall As String
        Get
            Return _urlImagenCentroCosteSmall
        End Get
        Set(ByVal value As String)
            _urlImagenCentroCosteSmall = value
        End Set
    End Property

    Public Property urlImagenCentroCoste As String
        Get
            Return _urlImagenCentroCoste
        End Get
        Set(ByVal value As String)
            _urlImagenCentroCoste = value
        End Set
    End Property

    
    Public Property urlImagenPartidas As String
        Get
            Return _urlImagenPartidas
        End Get
        Set(ByVal value As String)
            _urlImagenPartidas = value
        End Set
    End Property

    Public Property Valor() As String
        Get
            Valor = _sValor
        End Get
        Set(ByVal Value As String)
            _sValor = Value
        End Set
    End Property

    Public Property PRES5() As String
        Get
            PRES5 = _sPRES5
        End Get
        Set(ByVal Value As String)
            _sPRES5 = Value
        End Set
    End Property

    Public Property CentroCoste() As String
        Get
            CentroCoste = _sCentroCoste
        End Get
        Set(ByVal Value As String)
            _sCentroCoste = Value
        End Set
    End Property

    Public Property CentroCosteDen() As String
        Get
            CentroCosteDen = _sCentroCosteDen
        End Get
        Set(ByVal Value As String)
            _sCentroCosteDen = Value
        End Set
    End Property

    Public ReadOnly Property FechaInicio As Date
        Get
            Return Me.dteFechaInicioDesde.Value
        End Get
    End Property

    Public ReadOnly Property FechaHasta As Date
        Get
            Return Me.dteFechaHastaFin.Value
        End Get
    End Property

    Public ReadOnly Property NoVigentes As Boolean
        Get
            Return Not Me.chkPartidasNoVigentes.Checked
        End Get
    End Property

    ''' <summary>
    ''' Propiedad con los datos de centros de coste
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property DatosTreeView As DataSet
        Get
            Return _DatosTreeView
        End Get
        Set(ByVal value As DataSet)
            _DatosTreeView = value
        End Set
    End Property

    ''' <summary>
    ''' Propiedad con los textos de la pantalla
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property Textos() As DataTable
        Set(ByVal Value As DataTable)
            oTextos = Value
        End Set
    End Property

    Public WriteOnly Property Culture As Globalization.CultureInfo
        Set(ByVal value As Globalization.CultureInfo)
            _Culture = value
        End Set
    End Property

    Public Property Plurianual As Boolean
        Get
            Return _bPlurianual
        End Get
        Set(value As Boolean)
            _bPlurianual = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack() Then
            'Para el autocompletar
            Session("PRES5") = Request("PRES5")
            Session("CentroCoste") = Request("CentroCoste")

            If _sCentroCoste <> Nothing Then
                Me.txtCentro.Visible = False
                Me.hCentroCod.Value = _sCentroCoste
                Me.imgBuscarCentro.Visible = False
                Me.lblCentroCosteDen.Visible = True
                Me.lblCentroCosteDen.Text = _sCentroCosteDen
            Else
                Me.txtCentro.Visible = True
                Me.imgBuscarCentro.Visible = True
                Me.lblCentroCosteDen.Visible = False
            End If

            FSNPageHeader.UrlImagenCabecera = urlImagenCentroCoste
            FSNPageHeader.TituloCabecera = oTextos(8)(1) & " " & Request("PartidaDen")

            Me.lblBuscar.Text = oTextos(0)(1)
            Me.lblCentro.Text = oTextos(1)(1)
            Me.lblFechaInicioDesde.Text = oTextos(2)(1)
            Me.lblFechaHastaFin.Text = oTextos(3)(1)
            Me.btnBuscar.Text = oTextos(4)(1)
            Me.chkPartidasNoVigentes.Text = oTextos(5)(1)
            Me.btnSeleccionar.Text = oTextos(6)(1)
            Me.btnCancelar.Text = oTextos(7)(1)

            dteFechaInicioDesde.Culture = _Culture
            dteFechaHastaFin.Culture = _Culture
            dteFechaInicioDesde.OpenCalendarOnFocus = False
            dteFechaInicioDesde.NullText = ""
            dteFechaHastaFin.OpenCalendarOnFocus = False
            dteFechaHastaFin.NullText = ""


            Me.imgBuscarCentro.ImageUrl = _urlImagenBuscarCentro
            'Me.imgSiguiente.ImageUrl = _urlImagenSiguiente

            CargarArbolCentrosPartidas()

            If _bPlurianual Then
                Me.lblFechaInicioDesde.Visible = False
                Me.lblFechaHastaFin.Visible = False
                Me.chkPartidasNoVigentes.Checked = True
                Me.chkPartidasNoVigentes.Visible = False
                Me.dteFechaInicioDesde.Visible = False
                Me.dteFechaHastaFin.Visible = False
            Else
                Me.lblFechaInicioDesde.Visible = True
                Me.lblFechaHastaFin.Visible = True
                Me.chkPartidasNoVigentes.Checked = False
                Me.chkPartidasNoVigentes.Visible = True
                Me.dteFechaInicioDesde.Visible = True
                Me.dteFechaHastaFin.Visible = True
            End If

            Dim sClientTexts As String
            sClientTexts = ""
            sClientTexts += "var arrTextosML = new Array();"
            sClientTexts += "arrTextosML[0] = '" + JSText("Debe seleccionar un contrato") + "';"
            sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)

            'txtBuscar.Attributes.Add("onchange", "FindClicked('" & txtBuscar.ClientID & "','" & wdtPartidas.ClientID & "')")
            txtBuscar.Attributes.Add("onkeyup", "FindClicked('" & txtBuscar.ClientID & "','" & wdtPartidas.ClientID & "')")
            imgSiguiente.Attributes.Add("onclick", "FindNextClicked('" & txtBuscar.ClientID & "','" & wdtPartidas.ClientID & "');return false;")
            imgSiguiente.ToolTip = oTextos(13)(1)
        End If
    End Sub

    Private Sub CargarArbolCentrosPartidas()

        Dim oDataTreeNodeBinding As Infragistics.Web.UI.NavigationControls.DataTreeNodeBinding


        oDataTreeNodeBinding = New Infragistics.Web.UI.NavigationControls.DataTreeNodeBinding
        With oDataTreeNodeBinding
            .DataMember = "Tabla_0"
            .TextField = "DEN"
            .KeyField = "COD"
        End With
        wdtPartidas.DataBindings.Add(oDataTreeNodeBinding)

        oDataTreeNodeBinding = New Infragistics.Web.UI.NavigationControls.DataTreeNodeBinding
        With oDataTreeNodeBinding
            .DataMember = "Tabla_1"
            .TextField = "DEN"
            .KeyField = "COD"
        End With
        wdtPartidas.DataBindings.Add(oDataTreeNodeBinding)

        oDataTreeNodeBinding = New Infragistics.Web.UI.NavigationControls.DataTreeNodeBinding
        With oDataTreeNodeBinding
            .DataMember = "Tabla_2"
            .TextField = "DEN"
            .KeyField = "COD"
        End With
        wdtPartidas.DataBindings.Add(oDataTreeNodeBinding)

        oDataTreeNodeBinding = New Infragistics.Web.UI.NavigationControls.DataTreeNodeBinding
        With oDataTreeNodeBinding
            .DataMember = "Tabla_3"
            .TextField = "DEN"
            .KeyField = "COD"
        End With
        wdtPartidas.DataBindings.Add(oDataTreeNodeBinding)

        oDataTreeNodeBinding = New Infragistics.Web.UI.NavigationControls.DataTreeNodeBinding
        With oDataTreeNodeBinding
            .DataMember = "Tabla_4"
            .TextField = "DEN"
            .KeyField = "COD"
        End With
        wdtPartidas.DataBindings.Add(oDataTreeNodeBinding)

        oDataTreeNodeBinding = New Infragistics.Web.UI.NavigationControls.DataTreeNodeBinding
        With oDataTreeNodeBinding
            .DataMember = "Tabla_5"
            .TextField = "DEN"
            .KeyField = "COD"
        End With
        wdtPartidas.DataBindings.Add(oDataTreeNodeBinding)
       
        GenerarDsPartidas()
        wdtPartidas.DataSource = _DatosUons
        wdtPartidas.DataBind()

        wdtPartidas.SelectionType = Infragistics.Web.UI.NavigationControls.NodeSelectionTypes.Single
        wdtPartidas.EnableExpandOnClick = True


    End Sub

    Dim ds As DataSet
    Private Sub GenerarDsPartidas()
        _DatosUons = _DatosTreeView.Copy
        'Borramos las tablas de partidas, copiamos el dataset para mantener las claves y las relaciones
        With _DatosUons.Tables
            .RemoveAt(.Count - 1)
            .RemoveAt(.Count - 1)
            .RemoveAt(.Count - 1)
        End With
        _DatosPartidas.Tables.Add(_DatosTreeView.Tables(6).Copy)
        _DatosPartidas.Tables.Add(_DatosTreeView.Tables(7).Copy)
        _DatosPartidas.Tables.Add(_DatosTreeView.Tables(8).Copy)
    End Sub

    Private Sub wdtPartidas_NodeBound(ByVal sender As Object, ByVal e As Infragistics.Web.UI.NavigationControls.DataTreeNodeEventArgs) Handles wdtPartidas.NodeBound
        Dim iNivelNodo As Integer
        Dim oRows() As DataRow = Nothing

        ds = DirectCast(sender.datasource, System.Data.DataSet)

        iNivelNodo = e.Node.Level

        Select Case e.Node.Level
            Case 1
                oRows = ds.Tables(e.Node.Level).Select("COD= '" & e.Node.Key & "'")
            Case 2
                oRows = ds.Tables(iNivelNodo).Select("UON1= '" & e.Node.ParentNode.Key & "' AND COD= '" & e.Node.Key & "'")
            Case 3
                oRows = ds.Tables(iNivelNodo).Select("UON1= '" & e.Node.ParentNode.ParentNode.Key & "' AND UON2= '" & e.Node.ParentNode.Key & "' AND COD= '" & e.Node.Key & "'")
            Case 4
                oRows = ds.Tables(iNivelNodo).Select("UON1= '" & e.Node.ParentNode.ParentNode.ParentNode.Key & "' AND UON2= '" & e.Node.ParentNode.ParentNode.Key & "' AND UON3= '" & e.Node.ParentNode.Key & "' AND COD='" & e.Node.Key & "'")
            Case 5
                oRows = ds.Tables(e.Node.Level).Select("UON1= '" & e.Node.ParentNode.ParentNode.ParentNode.ParentNode.Key & "' AND UON2= '" & e.Node.ParentNode.ParentNode.ParentNode.Key & "' AND UON3= '" & e.Node.ParentNode.ParentNode.Key & "' AND UON4='" & e.Node.ParentNode.Key & "' AND COD='" & e.Node.Key & "'")
        End Select

        If e.Node.Level > 0 AndAlso e.Node.Level < 6 AndAlso oRows.Length > 0 Then
            If Not IsDBNull(oRows(0).Item("CENTRO_SM")) Then
                If oRows(0).Item("CENTRO_SM") = "PARTIDA" Then
                    ConfigurarNodoPartida(e.Node, oRows(0))
                Else
                    e.Node.ImageUrl = _urlImagenCentroCosteSmall
                    e.Node.Target = 1 'para buscar
                End If
            End If
        ElseIf e.Node.Level > 0 Then
            oRows = _DatosPartidas.Tables(0).Select("COD='" & e.Node.Key & "'")
            If oRows.Length > 0 Then
                ConfigurarNodoPartida(e.Node, oRows(0))
            Else
                oRows = _DatosPartidas.Tables(1).Select("COD='" & e.Node.Key & "'")
                If oRows.Length > 0 Then
                    ConfigurarNodoPartida(e.Node, oRows(0))
                Else
                    oRows = _DatosPartidas.Tables(2).Select("COD='" & e.Node.Key & "'")
                    If oRows.Length > 0 Then
                        ConfigurarNodoPartida(e.Node, oRows(0))
                    End If
                End If
            End If
        End If

        If _sValor <> Nothing Then
            Dim sUnidadOrg As Object = Split(_sValor, "#")
            Dim sUON1 As String = ""
            Dim sUON2 As String = ""
            Dim sUON3 As String = ""
            Dim sUON4 As String = ""
            Dim sUON5 As String = ""
            Dim iNivel As Integer = UBound(sUnidadOrg) + 1

            Select Case iNivel
                Case 1
                    sUON1 = sUnidadOrg(0)
                Case 2
                    sUON1 = sUnidadOrg(0)
                    sUON2 = sUnidadOrg(1)
                Case 3
                    sUON1 = sUnidadOrg(0)
                    sUON2 = sUnidadOrg(1)
                    sUON3 = sUnidadOrg(2)
                Case 4
                    sUON1 = sUnidadOrg(0)
                    sUON2 = sUnidadOrg(1)
                    sUON3 = sUnidadOrg(2)
                    sUON4 = sUnidadOrg(3)
                Case 5
                    sUON1 = sUnidadOrg(0)
                    sUON2 = sUnidadOrg(1)
                    sUON3 = sUnidadOrg(2)
                    sUON4 = sUnidadOrg(3)
                    sUON5 = sUnidadOrg(4)
            End Select

            If iNivel >= 5 Then
                If e.Node.Key = sUON5 Then
                    e.Node.ParentNode.Expanded = True
                    e.Node.ParentNode.ParentNode.Expanded = True
                    e.Node.Selected = True
                End If
            ElseIf e.Node.Level = iNivel Then
                If iNivel = 4 Then
                    If e.Node.Key = sUON4 And e.Node.ParentNode.Key = sUON3 And e.Node.ParentNode.ParentNode.Key = sUON2 And e.Node.ParentNode.ParentNode.ParentNode.Key = sUON1 Then
                        e.Node.ParentNode.Expanded = True
                        e.Node.ParentNode.ParentNode.Expanded = True
                        e.Node.Selected = True
                    End If
                ElseIf iNivel = 3 Then
                    If e.Node.Key = sUON3 And e.Node.ParentNode.Key = sUON2 And e.Node.ParentNode.ParentNode.Key = sUON1 Then
                        e.Node.ParentNode.Expanded = True
                        e.Node.ParentNode.ParentNode.Expanded = True
                        e.Node.Selected = True
                    End If
                ElseIf iNivel = 2 Then
                    If e.Node.Key = sUON2 And e.Node.ParentNode.Key = sUON1 Then
                        e.Node.ParentNode.Expanded = True
                        e.Node.Selected = True
                    End If
                ElseIf iNivel = 1 Then
                    If e.Node.Key = sUON1 Then
                        e.Node.Selected = True
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        RaiseEvent eventBuscarPartidas()
        CargarArbolCentrosPartidas()
    End Sub

    Private Sub ConfigurarNodoPartida(ByVal nodo As Infragistics.Web.UI.NavigationControls.DataTreeNode, ByVal drRow As DataRow)
        If Not IsDBNull(drRow.Item("CENTRO_SM")) AndAlso drRow.Item("CENTRO_SM") = "PARTIDA" Then
            Dim Fechas As String = String.Empty

            If Not _bPlurianual Then
                If drRow("FECINI") IsNot DBNull.Value Then
                    Fechas = " (" & FormatDate(drRow.Item("FECINI"), _Culture.DateTimeFormat) & " - "
                End If
                If drRow("FECFIN") Is DBNull.Value Then
                    Fechas = IIf(Fechas = String.Empty, Fechas, Fechas & ")")
                Else
                    Fechas = IIf(Fechas = String.Empty, " ( - " & FormatDate(drRow.Item("FECFIN"), _Culture.DateTimeFormat) & ")", Fechas & FormatDate(drRow.Item("FECFIN"), _Culture.DateTimeFormat) & ")")
                End If
            End If
            nodo.Text = nodo.Text & Fechas

                nodo.ImageUrl = _urlImagenCandado
                If drRow.Item("IMPUTABLE") = 1 Then
                    'nodo.Target = 2 'para poder seleccionar y buscar  
                    If _bPlurianual Then
                        nodo.Target = 4 'para poder seleccionar y buscar y que la descripcion se ponga tal cual (Partidas.aspx no recorte den buscando los parentesis de las fechas)
                    Else
                        nodo.Target = 2 'para poder seleccionar y buscar
                    End If
                Else
                    nodo.Target = 3 'para poder buscar
                    Dim sNiveles As Object = nodo.Key.ToString.Split("|")
                    If UBound(sNiveles) = 0 Then
                        _DatosPartidas.Tables(0).DefaultView.RowFilter = "cod like '" & nodo.Key & "|%'"
                        Dim x As Byte = 0
                        For Each dr As DataRow In _DatosPartidas.Tables(0).DefaultView.ToTable.Rows
                            Dim ChildrenNode As New Infragistics.Web.UI.NavigationControls.DataTreeNode
                            nodo.Nodes.Add(ChildrenNode)
                            ChildrenNode.Value = dr("COD")
                            ChildrenNode.Key = dr("COD")
                            ChildrenNode.Text = dr("DEN")
                            ConfigurarNodoPartida(ChildrenNode, _DatosPartidas.Tables(0).DefaultView.ToTable.Rows(0))
                            x += 1
                        Next
                    ElseIf UBound(sNiveles) = 1 Then
                        _DatosPartidas.Tables(1).DefaultView.RowFilter = "cod like '" & nodo.Key & "|%'"
                        Dim x As Byte = 0
                        For Each dr As DataRow In _DatosPartidas.Tables(1).DefaultView.ToTable.Rows
                            Dim ChildrenNode As New Infragistics.Web.UI.NavigationControls.DataTreeNode
                            nodo.Nodes.Add(ChildrenNode)
                            ChildrenNode.Value = dr("COD")
                            ChildrenNode.Key = dr("COD")
                            ChildrenNode.Text = dr("DEN")
                            ConfigurarNodoPartida(ChildrenNode, _DatosPartidas.Tables(1).DefaultView.ToTable.Rows(0))
                            x += 1
                        Next
                    ElseIf UBound(sNiveles) = 2 Then
                        _DatosPartidas.Tables(2).DefaultView.RowFilter = "cod like '" & nodo.Key & "|%'"
                        Dim x As Byte = 0
                        For Each dr As DataRow In _DatosPartidas.Tables(2).DefaultView.ToTable.Rows
                            Dim ChildrenNode As New Infragistics.Web.UI.NavigationControls.DataTreeNode
                            nodo.Nodes.Add(ChildrenNode)
                            ChildrenNode.Value = dr("COD")
                            ChildrenNode.Key = dr("COD")
                            ChildrenNode.Text = dr("DEN")
                            ConfigurarNodoPartida(ChildrenNode, _DatosPartidas.Tables(2).DefaultView.ToTable.Rows(0))
                            x += 1
                        Next
                    End If
                End If
                If Not (drRow.Item("VIGENTE") = 1) Then
                    nodo.CssClass = "PartidaNoVigente"
                End If
            End If
    End Sub

End Class