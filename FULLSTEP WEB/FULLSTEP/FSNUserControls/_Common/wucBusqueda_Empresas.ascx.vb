﻿Imports Fullstep.FSNLibrary
Public Class wucBusqueda_Empresas
    Inherits System.Web.UI.UserControl
    Private _dtPaises As DataTable
    Private _dtProvincias As DataTable
    Private _dtEmpresas As DataTable
    Private _Textos As DataTable


    Public Event eventProvinciasItemRequested(ByVal sCodPais As String)
    Public Event eventBtnBuscarClick()

    ''' <summary>
    ''' Propiedad de los paises
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property Paises() As DataTable
        Set(ByVal Value As DataTable)
            _dtPaises = Value
        End Set
    End Property

    ''' <summary>
    ''' Propiedad de las provincias
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property Provincias() As DataTable
        Set(ByVal Value As DataTable)
            _dtProvincias = Value
        End Set
    End Property

    ''' <summary>
    ''' Propiedad con las empresas
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property Empresas() As DataTable
        Set(ByVal Value As DataTable)
            _dtEmpresas = Value
        End Set
    End Property
    ''' <summary>
    ''' Propiedad con los textos de la pantalla
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property Textos() As DataTable
        Set(ByVal Value As DataTable)
            _Textos = Value
        End Set
    End Property

    Public ReadOnly Property Nif As String
        Get
            If txtNIF.Text = "" Then
                Return Nothing
            Else
                Return String.Concat("*", strToSQLLIKE(UCase(txtNIF.Text), True), "*")
            End If
        End Get
    End Property

    Public ReadOnly Property Denominacion As String
        Get
            If txtDen.Text = "" Then
                Return Nothing
            Else
                Return String.Concat("*", strToSQLLIKE(UCase(txtDen.Text), True), "*")
            End If

        End Get
    End Property

    Public ReadOnly Property Cp As String
        Get
            If txtCP.Text = "" Then
                Return Nothing
            Else
                Return String.Concat("*", strToSQLLIKE(UCase(txtCP.Text), True), "*")
            End If
        End Get
    End Property

    Public ReadOnly Property Dir As String
        Get
            If txtDir.Text = "" Then
                Return Nothing
            Else
                Return String.Concat("*", strToSQLLIKE(UCase(txtDir.Text), True), "*")
            End If
        End Get
    End Property

    Public ReadOnly Property Pais As String
        Get
            If Not wddPaises.SelectedItem Is Nothing Then
                Return String.Concat("*", strToSQLLIKE(UCase(wddPaises.SelectedItem.Text), True), "*")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property Provincia As String
        Get
            If Not wddProvincias.SelectedItem Is Nothing Then
                Return String.Concat("*", strToSQLLIKE(UCase(wddProvincias.SelectedItem.Text), True), "*")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Private _RutaImagenes As String
    Public Property RutaImagenes As String
        Get
            Return _RutaImagenes
        End Get
        Set(ByVal value As String)
            _RutaImagenes = value
        End Set
    End Property

    ''' <summary>
    ''' Carga de la pagina
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        wddPaises.ClientEvents.DropDownClosing = "cambioPais"
        If Not Page.ClientScript.IsClientScriptBlockRegistered("cambioPais") Then
            Dim sScript As String = "function cambioPais(sender, eventArgs) {" & vbCrLf &
                " var comboProv = $find('" & wddProvincias.ClientID & "')" & vbCrLf &
                    " comboProv.loadItems();" & vbCrLf &
                " } " & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "cambioPais", sScript, True)
        End If

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "wOpener", "<script>var wOpener = '" & IIf(Request.QueryString("opener") Is Nothing, "", Request.QueryString("opener")) & "';" &
                                                    "var IDCONTROLHID='" & IIf(Request.QueryString("IdControlHid") Is Nothing, "", Request.QueryString("IdControlHid")) & "';" &
                                                    "var IDCONTROL='" & IIf(Request.QueryString("IdControl") Is Nothing, "", Request.QueryString("IdControl")) & "'</script>")

        If Request.QueryString("idControl") IsNot Nothing Then
            hidIdControl.Value = Request.QueryString("idControl")
        End If
        If Not IsPostBack Then
            CargarComboPaises()
        End If
        CargarIdiomas()
        ConfigurarCabeceraMenu()
        RaiseEvent eventBtnBuscarClick()
    End Sub
    ''' <summary>
    ''' Proceso que carga el combo de paises
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,1seg.</remarks>
    Private Sub CargarComboPaises()
        Dim oItem As Infragistics.Web.UI.ListControls.DropDownItem

        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = ""
        oItem.Text = ""

        wddProvincias.NullText = ""
        wddProvincias.CurrentValue = ""
        wddPaises.Items.Add(oItem)

        For Each oRow As DataRow In _dtPaises.Rows
            oItem = New Infragistics.Web.UI.ListControls.DropDownItem
            oItem.Value = DBNullToStr(oRow.Item("COD"))
            oItem.Text = DBNullToStr(oRow.Item("DEN"))

            wddPaises.Items.Add(oItem)
        Next

        wddPaises.EnableAutoFiltering = Infragistics.Web.UI.ListControls.AutoFiltering.Client
        wddPaises.AutoFilterQueryType = Infragistics.Web.UI.ListControls.AutoFilterQueryTypes.Contains
        wddPaises.AutoFilterSortOrder = Infragistics.Web.UI.ListControls.DropDownAutoFilterSortOrder.Ascending
    End Sub

    ''' <summary>
    ''' Carga la combo con las provincias
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub CargarComboProvincias()
        wddProvincias.Items.Clear()

        Dim oItem As Infragistics.Web.UI.ListControls.DropDownItem

        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = ""
        oItem.Text = ""

        wddProvincias.Items.Add(oItem)

        For Each oRow As DataRow In _dtProvincias.Rows
            oItem = New Infragistics.Web.UI.ListControls.DropDownItem
            oItem.Value = DBNullToStr(oRow.Item("COD"))
            oItem.Text = DBNullToStr(oRow.Item("DEN"))

            wddProvincias.Items.Add(oItem)
        Next
    End Sub

    ''' <summary>
    ''' Configura los textos de la pantalla
    ''' </summary>
    ''' <remarks>Llamada desde:=Page_load</remarks>
    Private Sub CargarIdiomas()
        'Title = _Textos(0)(1)
        Me.lblNIF.Text = _Textos(1)(1)
        Me.lblDen.Text = _Textos(2)(1)
        Me.lblCP.Text = _Textos(3)(1)
        Me.lblDir.Text = _Textos(4)(1)
        Me.lblProv.Text = _Textos(5)(1)
        Me.lblPais.Text = _Textos(6)(1)
        Me.btnBuscar.Text = _Textos(7)(1)
        Me.btnAceptar.Text = _Textos(8)(1)
        Me.btnCancelar.Text = _Textos(9)(1)
        Me.lblEmpresasEncontradas.Text = _Textos(11)(1)
    End Sub

    ''' <summary>
    ''' Evento que salta al cambiar el valor de la combo TIPO. Carga los valores de subtipo relacionados con el tipo seleccionado.
    ''' </summary>
    ''' <param name="sender">propios del evento</param>
    ''' <param name="e">propios del evento</param>        
    ''' <remarks>Llamada desde; Tiempo máximo=0,1seg.</remarks>
    Private Sub wddProvincias_ItemsRequested(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownItemsRequestedEventArgs) Handles wddProvincias.ItemsRequested
        Dim sCodPais As String

        sCodPais = wddPaises.SelectedValue

        If sCodPais <> "" Then
            RaiseEvent eventProvinciasItemRequested(sCodPais)
        Else
            wddProvincias.Items.Clear()
        End If
    End Sub

    ''' <summary>
    ''' Carga la grid con las empresas
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load()  ; Tiempo máximo:1seg.</remarks>
    Public Sub CargarEmpresas()
        Dim result As DataView = Nothing
        If Not _dtEmpresas Is Nothing AndAlso _dtEmpresas.Rows.Count > 0 Then
            'valor por defecto
            result = _dtEmpresas.Clone.DefaultView

            Dim patternNIF As String = "*"
            Dim patternDenominacion As String = "*"
            Dim patternCP As String = "*"
            Dim patternDireccion As String = "*"
            Dim patternPais As String = "*"
            Dim patternProvincia As String = "*"

            If Not String.IsNullOrEmpty(txtNIF.Text) Then
                patternNIF = String.Concat("*", strToSQLLIKE(UCase(txtNIF.Text), True), "*")
            End If
            If Not String.IsNullOrEmpty(txtDen.Text) Then
                patternDenominacion = String.Concat("*", strToSQLLIKE(UCase(txtDen.Text), True), "*")
            End If
            If Not String.IsNullOrEmpty(txtCP.Text) Then
                patternCP = String.Concat("*", strToSQLLIKE(UCase(txtCP.Text), True), "*")
            End If
            If Not String.IsNullOrEmpty(txtDir.Text) Then
                patternDireccion = String.Concat("*", strToSQLLIKE(UCase(txtDir.Text), True), "*")
            End If
            If Not String.IsNullOrEmpty(wddPaises.SelectedValue) Then
                patternPais = String.Concat("*", strToSQLLIKE(UCase(wddPaises.SelectedItem.Text), True), "*")
                If Not String.IsNullOrEmpty(wddProvincias.SelectedValue) Then
                    patternProvincia = String.Concat("*", strToSQLLIKE(UCase(wddProvincias.SelectedItem.Text), True), "*")
                End If
            End If

            Dim match As EnumerableRowCollection(Of DataRow) = From Datos In _dtEmpresas _
                        Where (UCase(DBNullToStr(Datos("NIF")).ToString()) Like patternNIF) _
                        And (UCase(DBNullToStr(Datos("DEN")).ToString()) Like patternDenominacion) _
                        And (UCase(DBNullToStr(Datos("DIR")).ToString()) Like patternDireccion) _
                        And (UCase(DBNullToStr(Datos("CP")).ToString()) Like patternCP) _
                        And (UCase(DBNullToStr(Datos("PROVI_DEN")).ToString()) Like patternProvincia) _
                        And (UCase(DBNullToStr(Datos("PAI_DEN")).ToString()) Like patternPais) _
                        Select Datos

            If match.Count > 0 Then
                result = match.AsDataView
            End If

        End If

        Dim dsEmpresas As New DataSet
        dsEmpresas.Tables.Add(result.ToTable)

        wdg_Empresas.Rows.Clear()
        wdg_Empresas.DataSource = dsEmpresas
        wdg_Empresas.DataBind()
    End Sub

    ''' <summary>
    ''' Dibuja la cabecera
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0seg</remarks>
    Private Sub ConfigurarCabeceraMenu()
        FSNPageHeader.UrlImagenCabecera = If(_RutaImagenes <> Nothing, _RutaImagenes, System.Configuration.ConfigurationManager.AppSettings("ruta")) & "App_Themes/" & Page.Theme & "/images/BuscadorEmpresas.gif"
        FSNPageHeader.TituloCabecera = _Textos(0)(1)
        FSNPageHeader.AspectoGS = True
    End Sub

    Private Sub wdg_Empresas_DataFiltered(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.FilteredEventArgs) Handles wdg_Empresas.DataFiltered
        UpdatePanelEmpresas.Update()
    End Sub

    Private Sub wdg_Empresas_InitializeBand(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.BandEventArgs) Handles wdg_Empresas.InitializeBand
        e.Band.Columns("NIF").Header.Text = _Textos(1)(1)
        e.Band.Columns("DEN").Header.Text = _Textos(2)(1)
        e.Band.Columns("CP").Header.Text = _Textos(3)(1)
        e.Band.Columns("DIR").Header.Text = _Textos(4)(1)
        e.Band.Columns("PROVI_DEN").Header.Text = _Textos(5)(1)
        e.Band.Columns("PAI_DEN").Header.Text = _Textos(6)(1)
    End Sub
End Class