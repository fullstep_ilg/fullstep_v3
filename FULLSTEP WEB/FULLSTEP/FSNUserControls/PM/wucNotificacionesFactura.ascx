﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="wucNotificacionesFactura.ascx.vb" Inherits="FSNUserControls.wucNotificacionesFactura" %>
<asp:UpdatePanel runat="server" ID="upNotificaciones" UpdateMode="Conditional" ChildrenAsTriggers="false">
<ContentTemplate>
<ajx:CollapsiblePanelExtender ID="CollapsiblePanelExtender0" runat="server" CollapseControlID="pnlNotificaciones"
                    ExpandControlID="pnlNotificaciones" ImageControlID="imgExpandir0" SuppressPostBack="True"
                    TargetControlID="pnlDetalleNotificaciones" SkinID="FondoRojo">
                </ajx:CollapsiblePanelExtender>
<asp:Panel ID="pnlNotificaciones" runat="server" Style="cursor: pointer; width: 99%; height: 25px;">
    <table width="100%" style="background-color:#dAdAdA; border:1px solid #000000;">
        <tr><td width="95%" style="vertical-align: top"><asp:Label ID="lblTituloNotificaciones" runat="server" CssClass="EtiquetaMediano"></asp:Label></td>
            <td align="right"><asp:Image ID="imgExpandir0" runat="server" SkinID="Contraer" /></td>
        </tr>
     </table>
</asp:Panel>
<asp:Panel ID="pnlDetalleNotificaciones" runat="server" style="background-color:#FAFAFA; border:1px solid #000000;" Width="99%">
<div style="margin:10px">
<div><asp:Label ID="lblSubtituloNotificaciones" runat="server" CssClass="Etiqueta"></asp:Label></div>
<ig:WebDataGrid ID="wdgNotificaciones" runat="server" AutoGenerateColumns="false" ShowHeader="true" EnableAjax="true">
</ig:WebDataGrid>
</div>
</asp:Panel>
</ContentTemplate>
</asp:UpdatePanel>
