﻿Imports Infragistics.Web.UI.GridControls
Public Class wucNotificacionesFactura
    Inherits System.Web.UI.UserControl
#Region " PROPIEDADES "
    Private oTextos As DataTable
    Public WriteOnly Property Textos() As DataTable
        Set(ByVal Value As DataTable)
            oTextos = Value
        End Set
    End Property

    Private m_dsDataSource As DataSet
    Public WriteOnly Property DataSource() As DataSet
        Set(ByVal Value As DataSet)
            m_dsDataSource = Value
        End Set
    End Property

    Private _RutaImagenes As String
    Public Property RutaImagenes As String
        Get
            Return _RutaImagenes
        End Get
        Set(ByVal value As String)
            _RutaImagenes = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CargarTextos()
            CreateColumns()

            wdgNotificaciones.DataSource = m_dsDataSource
            wdgNotificaciones.DataBind()
        End If
    End Sub

    Private Sub CreateColumns()
        AddColumn("ID", "", 0, False)
        AddColumn("SUBJECT", oTextos(83).Item(1), 25)
        AddColumn("DIR_RESPUESTA", oTextos(84).Item(1), 25)
        AddColumn("PARA", oTextos(85).Item(1), 25)
        AddColumn("FECHA", oTextos(86).Item(1), 15)
        AddColumn("KO", oTextos(87).Item(1), 5)
        AddColumn("ID_EMAIL", oTextos(88).Item(1), 5)
    End Sub

    Private Sub AddColumn(ByVal fieldName As String, ByVal headerText As String, ByVal Width As Integer, Optional ByVal Visible As Boolean = True)
        Dim field As New BoundDataField(True)
        field.Key = fieldName
        field.DataFieldName = fieldName
        field.Header.Text = headerText
        field.Hidden = Not Visible
        field.Width = Unit.Percentage(Width)
        Me.wdgNotificaciones.Columns.Add(field)
    End Sub

    Private Sub CargarTextos()
        lblTituloNotificaciones.Text = oTextos(89).Item(1)
        lblSubtituloNotificaciones.Text = oTextos(90).Item(1)
    End Sub

    Private Sub wdgNotificaciones_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles wdgNotificaciones.InitializeRow
        If e.Row.Items.Item(sender.Columns.FromKey("KO").Index).Value = 0 Then
            e.Row.Items.Item(sender.Columns.FromKey("KO").Index).Text = "<img src='" & _RutaImagenes & "aprobado.gif' style='text-align:center;'/>"
        Else
            e.Row.Items.Item(sender.Columns.FromKey("KO").Index).Text = ""
        End If

        e.Row.Items.Item(sender.Columns.FromKey("ID_EMAIL").Index).Text = "<img src='" & _RutaImagenes & "Email.png' style='text-align:center; cursor:pointer' onclick='VerEmailNotificacion(" & e.Row.Items.Item(sender.Columns.FromKey("ID").Index).Value & ")'/>"
    End Sub
End Class