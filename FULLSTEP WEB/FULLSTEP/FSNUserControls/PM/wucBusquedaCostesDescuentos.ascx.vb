﻿Imports Infragistics.Web.UI.GridControls
Imports Fullstep.FSNLibrary

Public Class wucBusquedaCostesDescuentos
    Inherits System.Web.UI.UserControl

#Region "PROPIEDADES"
    Private m_sTipo As Short
    Public WriteOnly Property Tipo() As Short
        Set(ByVal Value As Short)
            m_sTipo = Value
        End Set
    End Property

    'Si es a nivel de cabecera o de linea
    Private m_sNivel As Short
    Public WriteOnly Property Nivel() As Short
        Set(ByVal Value As Short)
            m_sNivel = Value
        End Set
    End Property

    Private m_dsCDFacturasProveedor As DataSet
    Public WriteOnly Property CDFacturasProveedor() As DataSet
        Set(ByVal Value As DataSet)
            m_dsCDFacturasProveedor = Value
        End Set
    End Property

    Private m_dsAtributosBuscadorCostesDescuentos As DataSet
    Public WriteOnly Property AtributosBuscadorCostesDescuentos() As DataSet
        Set(ByVal Value As DataSet)
            m_dsAtributosBuscadorCostesDescuentos = Value
        End Set
    End Property

    Private m_sPanelBuscadorClientID As String
    Public WriteOnly Property PanelBuscadorClientID() As String
        Set(ByVal Value As String)
            m_sPanelBuscadorClientID = Value
        End Set
    End Property

    Private m_sNombreTabla As String
    Public WriteOnly Property NombreTabla() As String
        Set(ByVal Value As String)
            m_sNombreTabla = Value
        End Set
    End Property

    Private oTextos As DataTable
    Public WriteOnly Property Textos() As DataTable
        Set(ByVal Value As DataTable)
            oTextos = Value
        End Set
    End Property

    Private _RutaBuscadorArticulo As String
    Public Property RutaBuscadorArticulo As String
        Get
            Return _RutaBuscadorArticulo
        End Get
        Set(ByVal value As String)
            _RutaBuscadorArticulo = value
        End Set
    End Property

    Private _RutaBuscadorMaterial As String
    Public Property RutaBuscadorMaterial As String
        Get
            Return _RutaBuscadorMaterial
        End Get
        Set(ByVal value As String)
            _RutaBuscadorMaterial = value
        End Set
    End Property

    Private _PropiedadesBuscadorArticulo As String = String.Empty
    Public Property PropiedadesBuscadorArticulo As String
        Get
            Return _PropiedadesBuscadorArticulo
        End Get
        Set(ByVal value As String)
            _PropiedadesBuscadorArticulo = value
        End Set
    End Property
#End Region

    Public Event EventoBuscarDescuentos(ByVal sCod As String, ByVal sDen As String, ByVal sCodArt As String, ByVal sCodMat As String)
    Public Event EventoBuscarCostes(ByVal sCod As String, ByVal sDen As String, ByVal sCodArt As String, ByVal sCodMat As String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CargarTextos()

            tipoCD.Style("display") = If(m_sNivel = NivelCostesDescuentos.Lineas, "", "none")

            btnCDCancelar.OnClientClick = "Cancelar('" & m_sPanelBuscadorClientID & "');return false;"
            btnCDAceptar.OnClientClick = "addRowToTable('" & m_sPanelBuscadorClientID & "','" & ddlCDFacturas.ClientID & "','" & txtCDGenerico.ClientID & "','" & m_sNombreTabla & "'," & m_sTipo & ");return false;"

            imgCDMaterialLupa.Attributes.Add("onClick", "javascript:window.open('" & _RutaBuscadorMaterial & If(InStr(_RutaBuscadorMaterial, "?") > 0, "&", "?") & "ClientId=" & hidCDMaterial.ClientID & "&IdControl=" & Me.ClientID & "_" & txtCDMaterial.ID & "&MaterialGS_BBDD=" & Me.ClientID & "_" & hidCDMaterial.ID & "','_blank', 'width=550,height=550,status=yes,resizable=no,top=100,left=200'); return false;")
            imgCDArticuloLupa.Attributes.Add("onclick", "javascript:window.open('" & _RutaBuscadorArticulo & If(InStr(_RutaBuscadorArticulo, "?") > 0, "&", "?") & "ClientId=" & txtCDArticulo.ClientID & "&idHidControl=" & hidCDArticulo.ClientID & "&Origen=VisorFacturas', '_blank'," & If(_PropiedadesBuscadorArticulo <> Nothing, "'" & _PropiedadesBuscadorArticulo & "'", "'width=500,height=400,status=yes,resizable=no,top=200,left=200'") & ");return false;")

            Dim iListItem As New System.Web.UI.WebControls.ListItem
            iListItem.Value = String.Empty
            iListItem.Text = String.Empty
            ddlCDFacturas.DataTextField = "DEN"
            ddlCDFacturas.DataValueField = "ID"
            ddlCDFacturas.DataSource = m_dsCDFacturasProveedor
            ddlCDFacturas.DataBind()
            ddlCDFacturas.Items.Insert(0, iListItem)

            Dim oDS = New DataSet
            Dim oTable As DataTable = oDS.Tables.Add("TEMP")
            oTable.Columns.Add("COD", System.Type.GetType("System.String"))
            oTable.Columns.Add("DEN", System.Type.GetType("System.String"))
            whdgBuscadorCostesDescuentos.DataSource = oDS
            whdgBuscadorCostesDescuentos.DataBind()
        Else
            whdgBuscadorCostesDescuentos.DataSource = Session("AtributosBuscadorCostesDescuentos")
        End If
    End Sub

    Private Sub CargarTextos()
        lblLitCDTipo.Text = If(m_sTipo = TipoCostesDecuentos.Costes, oTextos(31).Item(1), oTextos(32).Item(1))
        rblCDTipo.Items(0).Text = If(m_sTipo = TipoCostesDecuentos.Costes, oTextos(33).Item(1), oTextos(34).Item(1))
        rblCDTipo.Items(1).Text = If(m_sTipo = TipoCostesDecuentos.Costes, oTextos(35).Item(1), oTextos(36).Item(1))

        lblLitCDFacturasAnteriores.Text = If(m_sTipo = TipoCostesDecuentos.Costes, String.Format("{0}:", oTextos(37).Item(1)), String.Format("{0}:", oTextos(38).Item(1)))
        lblCDCodigo.Text = String.Format("{0}:", oTextos(39).Item(1))
        lblCDDescripcion.Text = String.Format("{0}:", oTextos(40).Item(1))
        lblCDMaterial.Text = String.Format("{0}:", oTextos(41).Item(1))
        lblCDArticulo.Text = String.Format("{0}:", oTextos(42).Item(1))
        lblCDSinResultado.Text = If(m_sTipo = TipoCostesDecuentos.Costes, oTextos(43).Item(1), oTextos(44).Item(1))
        lblLitCDGenerico.Text = If(m_sTipo = TipoCostesDecuentos.Costes, String.Format("{0}:", oTextos(45).Item(1)), String.Format("{0}:", oTextos(46).Item(1)))

        btnCDBuscar.Text = oTextos(47).Item(1)
        btnCDLimpiar.Text = oTextos(48).Item(1)
        btnCDAceptar.Text = oTextos(49).Item(1)
        btnCDCancelar.Text = oTextos(50).Item(1)
    End Sub

    Private Sub btnCDBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCDBuscar.Click
        If m_sTipo = TipoCostesDecuentos.Costes Then
            RaiseEvent EventoBuscarCostes(txtCDCodigo.Text, txtCDDescripcion.Text, hidCDArticulo.Value, hidCDMaterial.Value)
        Else
            RaiseEvent EventoBuscarDescuentos(txtCDCodigo.Text, txtCDDescripcion.Text, hidCDArticulo.Value, hidCDMaterial.Value)
        End If

        If m_dsAtributosBuscadorCostesDescuentos.Tables(0).Rows.Count = 0 Then
            phCDGenerico.Visible = True
            whdgBuscadorCostesDescuentos.Visible = False
        Else
            phCDGenerico.Visible = False
            txtCDGenerico.Text = String.Empty
            whdgBuscadorCostesDescuentos.Visible = True
            Session("AtributosBuscadorCostesDescuentos") = m_dsAtributosBuscadorCostesDescuentos
            whdgBuscadorCostesDescuentos.DataSource = m_dsAtributosBuscadorCostesDescuentos
            whdgBuscadorCostesDescuentos.DataBind()
        End If
    End Sub

    Private Sub btnCDLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCDLimpiar.Click
        txtCDArticulo.Text = String.Empty
        hidCDArticulo.Value = String.Empty
        hidCDMaterial.Value = String.Empty
        txtCDCodigo.Text = String.Empty
        txtCDDescripcion.Text = String.Empty
        txtCDMaterial.Text = String.Empty
        txtCDGenerico.Text = String.Empty
       
    End Sub
End Class
