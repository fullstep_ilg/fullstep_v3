﻿Imports System.ComponentModel
Imports System.Configuration.Install
Imports System.Xml

Public Class ProjectInstaller

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add initialization code after the call to InitializeComponent
        ServiceInstaller1.ServiceName = ObtenerNombreServicio()
        ServiceInstaller1.DisplayName = ServiceInstaller1.ServiceName
    End Sub

    ''' <summary>
    ''' Obtiene el nombre del servicio a partir del license que tiene el servicio
    ''' </summary>
    ''' <returns>Devuelve string con Nombre del servicio</returns>
    ''' <remarks>Llamada desde=Propia pagina; Tiempo máximo=0,1seg.</remarks>
    Private Function ObtenerNombreServicio() As String
        Dim sNombre As String = ""
        Dim doc As New XmlDocument

        Try
            doc.Load(System.AppDomain.CurrentDomain.BaseDirectory() & "FSNWinServiceCustom.exe.config")

        Catch
            Err.Raise(10000, , System.AppDomain.CurrentDomain.BaseDirectory() & "FSNWinServiceCustom.exe.config file missing!")

        End Try


        Dim nodeList As XmlNodeList
        nodeList = doc.SelectNodes("/configuration/appSettings")


        Dim title As XmlNode

        Dim str As String
        Dim posiInicial, posi1, posi2 As Integer

        For Each title In nodeList
            str = title.InnerXml
            posiInicial = InStr(str, "NombreServicio")
            If posiInicial > 0 Then
                posi1 = InStr(posiInicial, str, "value")
                If posi1 > 0 Then
                    posi1 = posi1 + 7
                    posi2 = InStr(posi1, str, "/>") - 2
                    sNombre = Mid(str, posi1, (posi2 - posi1))

                End If

            End If
        Next
        If sNombre = "" Then
            Err.Raise(10000, , "Corrupted Config file !")
        End If

        doc = Nothing
        Return sNombre
    End Function

    Private Sub ServiceInstaller1_AfterInstall(sender As Object, e As InstallEventArgs) Handles ServiceInstaller1.AfterInstall

    End Sub
End Class
