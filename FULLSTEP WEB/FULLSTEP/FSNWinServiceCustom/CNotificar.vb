﻿Imports System.Data.SqlClient
Imports System.Net.Mail

Public Class CNotificar
    Inherits CBase

    Private WithEvents _oEmail As Fullstep.FSNLibraryCOM.Email
    Private _SmtpAuthentication As Integer
    Private _SmtpUsu As String = String.Empty
    Private _SmtpPassword As String = String.Empty
    Private _SmtpCuentaServicio As String = String.Empty
    Private _SmtpDominio As String = String.Empty
    Private _SmtpServer As String = String.Empty
    Private _SmtpPort As Integer = 25
    Private _SmtpEnableSsl As Integer
    Private _SmtpDeliveryMethod As Integer

#Region "Constructor"

    Public Sub New()
        Dim oRoot As New BDRoot

        Try
            Dim oData As Dictionary(Of String, Object) = oRoot.DevolverPargenMail

            _SmtpAuthentication = DBNullToInteger(oData.Item("AUTENTICACION"))
            If _SmtpAuthentication = 2 Then 'windows
                _SmtpUsu = oData.Item("USU")
                _SmtpPassword = Encrypter.Encrypt(oData.Item("PWD"), oData.Item("USU"), False, Encrypter.TipoDeUsuario.Persona, oData.Item("FECPWD"))
            ElseIf _SmtpAuthentication = 1 Then 'basica
                _SmtpUsu = oData.Item("CUENTAMAIL")
                _SmtpPassword = Encrypter.Encrypt(oData.Item("PWD"), oData.Item("CUENTAMAIL"), False, Encrypter.TipoDeUsuario.Persona, oData.Item("FECPWD"))
            Else
                _SmtpUsu = String.Empty
                _SmtpPassword = String.Empty
            End If
            _SmtpCuentaServicio = DBNullToStr(oData.Item("CUENTAMAIL"))
            _SmtpDominio = DBNullToStr(oData.Item("DOMINIO"))
            _SmtpServer = DBNullToStr(oData.Item("SERVIDOR"))
            _SmtpPort = DBNullToDbl(oData.Item("PUERTO"))
            _SmtpEnableSsl = DBNullToInteger(oData.Item("SSL"))
            _SmtpDeliveryMethod = DBNullToInteger(oData.Item("METODO_ENTREGA"))

            'Crea una instancia de la clase Email pasándole la configuración del servidor de correo para los emails que se envíen desde la instancia de notificar.
            _oEmail = New Fullstep.FSNLibraryCOM.Email(_SmtpServer, _SmtpPort, _SmtpAuthentication, _SmtpEnableSsl, _SmtpDeliveryMethod, _SmtpUsu, _SmtpPassword, _SmtpDominio, True)
        Catch ex As Exception
            GuardarError(ex, "CNotificar.New")
        End Try
    End Sub

#End Region

#Region "Métodos públicos"

    ''' <summary>
    ''' Función que realiza el envío de los emails.   
    ''' </summary>
    ''' <param name="sFrom">El from del mensaje</param>
    ''' <param name="sSubject">El asunto</param>
    ''' <param name="sPara">El destinatario</param>
    ''' <param name="Message">El mensaje</param>
    ''' <param name="iError">Código de error (por referencia)</param>
    ''' <param name="strError">Descripción del error (por referencia)</param>        
    ''' <param name="isHTML">Si el formato del email es HTML o no</param>   
    ''' <param name="bGrabaEnvio">Si hay que grabar el envío en BD</param>
    ''' <param name="CodProve">Proveedor al q se le envia el mail</param>
    ''' <param name="IdMail">Tipo de Notificacion. Es un valor de FSNLibrary.TipoNotificacion</param> 
    ''' <param name="lIdInstancia">Instancia que asociaremos al mail enviado</param>     
    ''' <param name="sParaNombre">Nombre y apellidos del q recibe el mail</param> 
    ''' <param name="EnvioAsincrono">Si el envio es sincrono o no</param>
    ''' <param name="Idioma">Idioam para el subject "De parte de"</param>
    ''' <remarks>Llamada desde: Todas las funciones de Notificación q provienen de PMNotificador/Notificar.vb. Tb lo llama el EnviarMail de lo q proviene de EP.
    ''' Tiempo máximo: 1 sg.</remarks>
    Public Sub EnviarMail(ByVal sFrom As String, ByVal sSubject As String, ByVal sPara As String, ByVal Message As String, ByRef iError As Integer, ByRef strError As String, _
                          Optional ByVal isHTML As Boolean = False, Optional ByVal bGrabaEnvio As Boolean = True, _
                          Optional ByVal CodProve As String = Nothing, Optional ByVal IdMail As Integer = 0, Optional ByVal lIdInstancia As Long = 0, _
                          Optional ByVal sParaNombre As String = "", Optional ByVal EnvioAsincrono As Boolean = False, Optional ByVal Idioma As String = "SPA")
        Dim lIdError As Long = -1

        Try
            _oEmail.EnviarMail(_SmtpCuentaServicio, sFrom, sSubject, sPara, Message, "", sParaNombre, "", "", "", "", isHTML, EnvioAsincrono, False, iError, strError, Producto.CM, bGrabaEnvio)

            If Not EnvioAsincrono Then
                If iError < 2 Then
                    lIdError = GuardarError(New Exception("FSNLibraryCOM.Email Indica error: " & strError), "CNotificar.EnviarMail")
                End If

                If bGrabaEnvio Then
                    Dim oBDRoot As New BDRoot
                    If iError = 2 Then '2-todo ok
                        oBDRoot.GrabarEnviarMensaje(_SmtpCuentaServicio, sSubject, sPara, Message, isHTML, False, "", CodProve, IdMail, lIdInstancia, sParaNombre, "")
                    Else
                        oBDRoot.GrabarEnviarMensaje(_SmtpCuentaServicio, sSubject, sPara, Message, isHTML, True, strError, CodProve, IdMail, lIdInstancia, sParaNombre, "", lIdError)
                    End If
                End If
            End If
        Catch ex As Exception
            iError = 0 'Fallo el send
            strError = ex.Message

            GuardarError(ex, "CNotificar.EnviarMail")

            If bGrabaEnvio Then
                Dim oBDRoot As New BDRoot
                oBDRoot.GrabarEnviarMensaje(_SmtpCuentaServicio, sSubject, sPara, Message, isHTML, True, ex.Message, CodProve, IdMail, lIdInstancia, sParaNombre, lIdError)
            End If
        End Try
    End Sub

#End Region

    ''' <summary>
    ''' Método que se ejecuta cuando finalice un envío asíncrono
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">Objeto AsyncCompletedEventArgs con la información del evento</param>
    Private Sub _oEmail_FinEnviomailAsincrono(Sender As Object, e As System.ComponentModel.AsyncCompletedEventArgs) Handles _oEmail.FinEnviomailAsincrono
        Dim oBDRoot As New BDRoot
        Dim oMail As MailMessage = CType(e.UserState, Fullstep.FSNLibraryCOM.Email.MiMensaje)

        If e.Error Is Nothing Then
            oBDRoot.GrabarEnviarMensaje(oMail.From.ToString, oMail.Subject, oMail.To.ToString, oMail.Body, oMail.IsBodyHtml, False, String.Empty, String.Empty, FSNLibrary.TipoNotificacion.NoDefinido, 0, oMail.To.ToString, String.Empty, 0)
        Else
            Dim lIdError As Long = GuardarError(e.Error, "CNotificar._oEmail_FinEnviomailAsincrono")

            If CType(e.UserState, Fullstep.FSNLibraryCOM.Email.MiMensaje).Grabar Then
                If TryCast(e.Error, System.Security.SecurityException) IsNot Nothing Then
                    oBDRoot.GrabarEnviarMensaje(oMail.From.ToString, oMail.Subject, oMail.To.ToString, oMail.Body, oMail.IsBodyHtml, True, "Error en log", String.Empty, FSNLibrary.TipoNotificacion.NoDefinido, 0, oMail.To.ToString, String.Empty, lIdError)
                ElseIf TryCast(e.Error, ArgumentNullException) IsNot Nothing Then
                    If oMail Is Nothing Then
                        oBDRoot.GrabarEnviarMensaje(String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, True, "El parámetro Message es Nothing.", String.Empty, FSNLibrary.TipoNotificacion.NoDefinido, 0, String.Empty, String.Empty, lIdError)
                    ElseIf oMail.From Is Nothing Then
                        oBDRoot.GrabarEnviarMensaje(String.Empty, oMail.Subject, oMail.To.ToString, oMail.Body, oMail.IsBodyHtml, True, "La propiedad MailMessage.From es Nothing.", String.Empty, FSNLibrary.TipoNotificacion.NoDefinido, 0, _
                                                    oMail.To.ToString, String.Empty, lIdError)
                    ElseIf oMail.To Is Nothing Then
                        oBDRoot.GrabarEnviarMensaje(oMail.From.ToString, oMail.Subject, String.Empty, oMail.Body, oMail.IsBodyHtml, True, "La propiedad MailMessage.To es Nothing.", String.Empty, FSNLibrary.TipoNotificacion.NoDefinido, 0, _
                                                    String.Empty, String.Empty, lIdError)
                    End If
                ElseIf TryCast(e.Error, ArgumentOutOfRangeException) IsNot Nothing Then
                    oBDRoot.GrabarEnviarMensaje(oMail.From.ToString, oMail.Subject, String.Empty, oMail.Body, oMail.IsBodyHtml, True, "No hay ningún destinatario en To, ni en CC o Bcc.", String.Empty, FSNLibrary.TipoNotificacion.NoDefinido, 0, _
                                                String.Empty, String.Empty, lIdError)
                ElseIf TryCast(e.Error, SmtpFailedRecipientsException) IsNot Nothing Then
                    oBDRoot.GrabarEnviarMensaje(oMail.From.ToString, oMail.Subject, String.Empty, oMail.Body, oMail.IsBodyHtml, True, "No se pudo entregar message a uno o varios de los destinatarios de To, CC o Bcc.", String.Empty, _
                                                FSNLibrary.TipoNotificacion.NoDefinido, 0, oMail.To.ToString, String.Empty, lIdError)
                Else
                    oBDRoot.GrabarEnviarMensaje(oMail.From.ToString, oMail.Subject, String.Empty, oMail.Body, oMail.IsBodyHtml, True, e.Error.Message, String.Empty, FSNLibrary.TipoNotificacion.NoDefinido, 0, oMail.To.ToString, String.Empty, lIdError)
                End If
            End If
        End If

        If Not _oEmail Is Nothing Then
            _oEmail.FuerzaFinalizeSmtpClient()
            _oEmail = Nothing
        End If
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()

        If Not _oEmail Is Nothing Then
            _oEmail.FuerzaFinalizeSmtpClient()
            _oEmail = Nothing
        End If
    End Sub
End Class
