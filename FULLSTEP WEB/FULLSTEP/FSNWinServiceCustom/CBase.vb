﻿Public MustInherit Class CBase

    ''' <summary>Guarda un error en BD</summary>
    ''' <param name="oError">Error a grabar</param>
    ''' <param name="sOrigen">Origen del error</param>
    ''' <returns>Id del error creado</returns>
    ''' <remarks>Llamada desde: </remarks>
    Public Function GuardarError(ByVal oError As Exception, ByVal sOrigen As String) As Long
        Dim oBDRoot As New BDRoot
        Return oBDRoot.GuardarError(oError, sOrigen)
    End Function

End Class
