﻿Public Class CContratos
    Inherits CBase
    Implements IList(Of CContrato)

    Private _Lista As List(Of CContrato)
    Private _sFromEMailsAlerta As String
    Private _AsuntoMail As Dictionary(Of String, String)

#Region "Métodos públicos"

    ''' <summary>Carga la lista con los contratos para los que hay que enviar alerta</summary>
    ''' <remarks></remarks>
    Public Sub LCX_CargarContratosAlertaEncuesta()
        Try
            Dim oBDRoot As New BDRoot
            Dim dsContratos As DataSet = oBDRoot.LCX_DevolverContratosAlertaEncuesta
            If Not dsContratos Is Nothing AndAlso dsContratos.Tables.Count > 0 Then
                _sFromEMailsAlerta = dsContratos.Tables(0).Rows(0)("CUENTAMAIL")
                _AsuntoMail = New Dictionary(Of String, String)
                _AsuntoMail.Add("SPA", dsContratos.Tables(0).Rows(0)("TEXT_SPA"))
                _AsuntoMail.Add("ENG", dsContratos.Tables(0).Rows(0)("TEXT_ENG"))
                _AsuntoMail.Add("GER", dsContratos.Tables(0).Rows(0)("TEXT_GER"))
                _AsuntoMail.Add("FRA", dsContratos.Tables(0).Rows(0)("TEXT_FRA"))

                _Lista = New List(Of CContrato)
                For Each oRow As DataRow In dsContratos.Tables(0).Rows
                    'Pongo este segundo try-catch para continuar con el bucle en caso de que un contrato de error
                    Try
                        Dim oEmpresa As CEmpresa = Nothing
                        If Not oRow.IsNull("EMP_NIF") Then oEmpresa = New CEmpresa(oRow("EMP_NIF"), oRow("EMP_DEN"))
                        Dim oProve As CProveedor = Nothing
                        If Not oRow.IsNull("PROVE") Then oProve = New CProveedor(oRow("PROVE"), oRow("PROVE_DEN"), DBNullToSomething(oRow("PROVE_NIF")), DBNullToSomething(oRow("CON_NOM")), DBNullToSomething(oRow("CON_APE")), DBNullToSomething(oRow("CON_EMAIL")))

                        Dim sfmtDate As String = DBNullToSomething(oRow("DATEFMT"))
                        If sfmtDate = Nothing Then
                            sfmtDate = "dd/MM/yyyy"
                        Else
                            sfmtDate = Replace(sfmtDate, "mm", "MM")
                        End If

                        Dim oPersona As CPersona = Nothing
                        If Not oRow.IsNull("PETICIONARIO") Then oPersona = New CPersona(oRow("PETICIONARIO"), DBNullToSomething(oRow("PET_NOM")), oRow("PET_APE"), DBNullToSomething(oRow("PET_TFNO")), DBNullToSomething(oRow("PET_EMAIL")),
                                                                                        DBNullToSomething(oRow("PET_IDIOMA")), oRow("PET_TIPOEMAIL"), sfmtDate)

                        Me.Add(New CContrato(oRow("CODIGO"), oRow("FEC_INI"), If(oRow.IsNull("FEC_FIN"), Nothing, oRow("FEC_FIN")), DBNullToSomething(oRow("NOMBRE")), oRow("INSTANCIA"), oEmpresa, oProve, oPersona))
                    Catch ex As Exception
                        GuardarError(ex, "CContratos.LCX_CargarContratosAlertaEncuesta")
                    End Try
                Next
            End If

        Catch ex As Exception
            GuardarError(ex, "CContratos.LCX_CargarContratosAlertaEncuesta")
        End Try
    End Sub

    ''' <summary>Envía los eMails para los contratos a los que hay que generar alerta</summary>
    ''' <remarks></remarks>
    Public Sub LCX_ContratosEnviarMailsAlertaEncuesta()
        Dim oNotif As New CNotificar

        For Each oContrato As CContrato In Me
            Try
                Dim sCuerpoEmail As String = oContrato.CuerpoEmailAlerta
                If Not sCuerpoEmail Is Nothing Then
                    Dim iError As Integer
                    Dim sError As String

                    oNotif.EnviarMail(_sFromEMailsAlerta, _AsuntoMail(oContrato.Peticionario.Idioma), oContrato.Peticionario.EMail, sCuerpoEmail, iError, sError, IIf(oContrato.Peticionario.TipoEmail = 0, False, True),
                                       , IIf(oContrato.Proveedor Is Nothing, Nothing, oContrato.Proveedor.Codigo), FSNLibrary.TipoNotificacion.NoDefinido, oContrato.Instancia, oContrato.Peticionario.Denominacion, , oContrato.Peticionario.Idioma)
                End If
            Catch ex As Exception
                GuardarError(ex, "CContratos.LCX_ContratosEnviarMailsAlertaEncuesta")
            End Try
        Next
    End Sub

#End Region

#Region "Interfaz"

    Public Sub Add(item As CContrato) Implements System.Collections.Generic.ICollection(Of CContrato).Add
        _Lista.Add(item)
    End Sub

    Public Sub Clear() Implements System.Collections.Generic.ICollection(Of CContrato).Clear
        _Lista.Clear()
    End Sub

    Public Function Contains(item As CContrato) As Boolean Implements System.Collections.Generic.ICollection(Of CContrato).Contains
        Return _Lista.Contains(item)
    End Function

    Public Sub CopyTo(array() As CContrato, arrayIndex As Integer) Implements System.Collections.Generic.ICollection(Of CContrato).CopyTo
        _Lista.CopyTo(array, arrayIndex)
    End Sub

    Public ReadOnly Property Count As Integer Implements System.Collections.Generic.ICollection(Of CContrato).Count
        Get
            Return _Lista.Count
        End Get
    End Property

    Public ReadOnly Property IsReadOnly As Boolean Implements System.Collections.Generic.ICollection(Of CContrato).IsReadOnly
        Get
            Return False
        End Get
    End Property

    Public Function Remove(item As CContrato) As Boolean Implements System.Collections.Generic.ICollection(Of CContrato).Remove
        Return _Lista.Remove(item)
    End Function

    Public Function GetEnumerator() As System.Collections.Generic.IEnumerator(Of CContrato) Implements System.Collections.Generic.IEnumerable(Of CContrato).GetEnumerator
        Return _Lista.GetEnumerator
    End Function

    Public Function IndexOf(item As CContrato) As Integer Implements System.Collections.Generic.IList(Of CContrato).IndexOf
        Return _Lista.IndexOf(item)
    End Function

    Public Sub Insert(index As Integer, item As CContrato) Implements System.Collections.Generic.IList(Of CContrato).Insert
        _Lista.Insert(index, item)
    End Sub

    Default Public Property Item(index As Integer) As CContrato Implements System.Collections.Generic.IList(Of CContrato).Item
        Get
            Return _Lista.Item(index)
        End Get
        Set(value As CContrato)
            _Lista.Item(index) = value
        End Set
    End Property

    Public Sub RemoveAt(index As Integer) Implements System.Collections.Generic.IList(Of CContrato).RemoveAt
        _Lista.RemoveAt(index)
    End Sub

    Public Function GetEnumerator1() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        Return _Lista.GetEnumerator
    End Function

#End Region

End Class
