Aviso de encuesta sin cumplimentar
----------------------------------

Le comunicamos que debe rellenar la encuesta correspondiente al siguiente contrato:


	Datos del contrato
	------------------------------------------------------
  	C�digo: 		@CODIGO
  	Fecha de inicio: 	@FEC_INI
	Fecha de expiraci�n:	@FECHA_FIN
	Descripci�n:		@DEN

	Datos de la empresa
	------------------------------------------------------
	NIF:			@EMP_NIF
  	Raz�n social:		@EMP_DEN

	Datos del proveedor
	------------------------------------------------------
	C�digo:			@PROVE_COD
	Denominaci�n:		@PROVE_DEN
	NIF:			@PROVE_NIF	
	Contacto:		@CON_NOMAPE
	Email contacto:		@CON_EMAIL

	Datos del peticionario
	------------------------------------------------------
	C�digo:			@PET_COD
	Nombre:			@PET_NOM
	Tel�fono:		@PET_TFNO
	Email:			@PET_MAIL

