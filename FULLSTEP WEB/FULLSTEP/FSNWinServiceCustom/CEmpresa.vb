﻿Public Class CEmpresa
    Inherits CBase

    Private _sNIF As String
    Private _sRazonSocial As String

#Region "Propiedades"

    Public Property NIF As String
        Get
            Return _sNIF
        End Get
        Set(value As String)
            _sNIF = value
        End Set
    End Property

    Public Property RazonSocial As String
        Get
            Return _sRazonSocial
        End Get
        Set(value As String)
            _sRazonSocial = value
        End Set
    End Property

#End Region

#Region "Constructor"

    Public Sub New(ByVal sNIF As String, ByVal sRazonSocial As String)
        _sNIF = sNIF
        _sRazonSocial = sRazonSocial
    End Sub

#End Region

End Class
