﻿Imports System.IO

Public Class CContrato
    Inherits CBase

    Private _sCodigo As String
    Private _dFechaInicio As Date
    Private _dFechaExpiracion As Nullable(Of Date)
    Private _sDescripcion As String
    Private _iInstancia As Integer
    Private _oEmpresa As CEmpresa
    Private _oProveedor As CProveedor
    Private _oPeticionario As CPersona

#Region "Propiedades"

    Public Property Codigo As String
        Get
            Return _sCodigo
        End Get
        Set(value As String)
            _sCodigo = value
        End Set
    End Property

    Public Property FechaInicio As Date
        Get
            Return _dFechaInicio
        End Get
        Set(value As Date)
            _dFechaInicio = value
        End Set
    End Property

    Public Property FechaExpiracion As Nullable(Of Date)
        Get
            Return _dFechaExpiracion
        End Get
        Set(value As Nullable(Of Date))
            _dFechaExpiracion = value
        End Set
    End Property

    Public Property Descripcion As String
        Get
            Return _sDescripcion
        End Get
        Set(value As String)
            _sDescripcion = value
        End Set
    End Property

    Public Property Instancia As Integer
        Get
            Return _iInstancia
        End Get
        Set(value As Integer)
            _iInstancia = value
        End Set
    End Property

    Public Property Empresa As CEmpresa
        Get
            Return _oEmpresa
        End Get
        Set(value As CEmpresa)
            _oEmpresa = value
        End Set
    End Property

    Public Property Proveedor As CProveedor
        Get
            Return _oProveedor
        End Get
        Set(value As CProveedor)
            _oProveedor = value
        End Set
    End Property

    Public Property Peticionario As CPersona
        Get
            Return _oPeticionario
        End Get
        Set(value As CPersona)
            _oPeticionario = value
        End Set
    End Property

#End Region

#Region "Constructor"

    Public Sub New(ByVal sCodigo As String, ByVal dFechaInicio As Date, ByVal dFechaExpiracion As Nullable(Of Date), ByVal sDescripcion As String, ByVal iInstancia As Integer, ByVal oEmpresa As CEmpresa, ByVal oProveedor As CProveedor, _
                   ByVal oPeticionario As CPersona)
        _sCodigo = sCodigo
        _dFechaInicio = dFechaInicio
        _dFechaExpiracion = dFechaExpiracion
        _sDescripcion = sDescripcion
        _iInstancia = iInstancia
        _oEmpresa = oEmpresa
        _oProveedor = oProveedor
        _oPeticionario = oPeticionario
    End Sub

#End Region

#Region "Métodos públicos"

    ''' <summary>Construye el cuerpo del email a enviar</summary>    
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CuerpoEmailAlerta() As String
        Dim sCuerpo As String

        Try
            Dim sPath As String = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _oPeticionario.PlantillaAlerta)

            Dim oStream As New System.IO.StreamReader(sPath, System.Text.Encoding.Default, True)
            sCuerpo = oStream.ReadToEnd()
            oStream.Close()

            'Datos del contrato
            sCuerpo = ReplaceV(sCuerpo, "@CODIGO", _sCodigo)
            If Not _oPeticionario Is Nothing AndAlso Not _oPeticionario.FormatoFecha Is Nothing AndAlso _oPeticionario.FormatoFecha.Length > 0 Then
                sCuerpo = ReplaceV(sCuerpo, "@FEC_INI", Format(_dFechaInicio, _oPeticionario.FormatoFecha))
            Else
                sCuerpo = ReplaceV(sCuerpo, "@FEC_INI", _dFechaInicio)
            End If
            If Not _oPeticionario Is Nothing AndAlso Not _oPeticionario.FormatoFecha Is Nothing AndAlso _oPeticionario.FormatoFecha.Length > 0 Then
                sCuerpo = ReplaceV(sCuerpo, "@FECHA_FIN", IIf(_dFechaExpiracion.HasValue, Format(_dFechaExpiracion, _oPeticionario.FormatoFecha), String.Empty))
            Else
                sCuerpo = ReplaceV(sCuerpo, "@FECHA_FIN", IIf(_dFechaExpiracion.HasValue, _dFechaExpiracion, String.Empty))
            End If
            sCuerpo = ReplaceV(sCuerpo, "@DEN", IIf(Not _sDescripcion Is Nothing, _sDescripcion, String.Empty))
            'Datos de la empresa
            If Not _oEmpresa Is Nothing Then
                With _oEmpresa
                    sCuerpo = ReplaceV(sCuerpo, "@EMP_NIF", .NIF)
                    sCuerpo = ReplaceV(sCuerpo, "@EMP_DEN", .RazonSocial)
                End With
            Else
                sCuerpo = ReplaceV(sCuerpo, "@EMP_NIF", String.Empty)
                sCuerpo = ReplaceV(sCuerpo, "@EMP_DEN", String.Empty)
            End If
            'Datos del proveedor
            If Not _oProveedor Is Nothing Then
                With _oProveedor
                    sCuerpo = ReplaceV(sCuerpo, "@PROVE_COD", .Codigo)
                    sCuerpo = ReplaceV(sCuerpo, "@PROVE_DEN", .Denominacion)
                    sCuerpo = ReplaceV(sCuerpo, "@PROVE_NIF", IIf(Not .NIF Is Nothing, .NIF, String.Empty))
                    sCuerpo = ReplaceV(sCuerpo, "@CON_NOMAPE", .ContactoDenominacion)
                    sCuerpo = ReplaceV(sCuerpo, "@CON_EMAIL", IIf(Not .ContactoEMail Is Nothing, .ContactoEMail, String.Empty))
                End With
            Else
                sCuerpo = ReplaceV(sCuerpo, "@PROVE_COD", String.Empty)
                sCuerpo = ReplaceV(sCuerpo, "@PROVE_DEN", String.Empty)
                sCuerpo = ReplaceV(sCuerpo, "@PROVE_NIF", String.Empty)
                sCuerpo = ReplaceV(sCuerpo, "@CON_NOMAPE", String.Empty)
                sCuerpo = ReplaceV(sCuerpo, "@CON_EMAIL", String.Empty)
            End If
            'Datos del peticionario
            If Not _oPeticionario Is Nothing Then
                With _oPeticionario
                    sCuerpo = ReplaceV(sCuerpo, "@PET_COD", .Codigo)
                    sCuerpo = ReplaceV(sCuerpo, "@PET_NOM", .Denominacion)
                    sCuerpo = ReplaceV(sCuerpo, "@PET_TFNO", IIf(Not .Telefono Is Nothing, .Telefono, String.Empty))
                    sCuerpo = ReplaceV(sCuerpo, "@PET_MAIL", IIf(Not .EMail Is Nothing, .EMail, String.Empty))
                End With
            Else
                sCuerpo = ReplaceV(sCuerpo, "@PET_COD", String.Empty)
                sCuerpo = ReplaceV(sCuerpo, "@PET_NOM", String.Empty)
                sCuerpo = ReplaceV(sCuerpo, "@PET_TFNO", String.Empty)
                sCuerpo = ReplaceV(sCuerpo, "@PET_MAIL", String.Empty)
            End If

        Catch ex As Exception
            GuardarError(ex, "CContratos.CuerpoEmailAlerta")
            sCuerpo = Nothing
        End Try

        Return sCuerpo
    End Function

#End Region

End Class
