﻿Imports System.Data.SqlClient
Imports System.Xml
Imports Fullstep.FSNLibrary.Encrypter
Imports System.Configuration

Public Class BDRoot
    Private _sConnectionString As String

#Region "Constructor"

    Public Sub New()
        Dim sDBLogin As String = ""
        Dim sDBPassword As String = ""
        Dim sDBName As String = ""
        Dim sDBServer As String = ""

        'Comprobar tema de licencias y cargar datos de conexión
        Dim sDB As String = ConfigurationManager.AppSettings("DB:FS_WS")
        If sDB Is Nothing Then
            Err.Raise(10000, , "Configuration file corrupted. Missing FS_WS entry !")
        End If
        'Tenemos que extraer el login y la contraseña de conexión.
        Dim doc As New XmlDocument

        Try
            doc.Load(System.AppDomain.CurrentDomain.BaseDirectory() & "License.xml")

        Catch
            Err.Raise(10000, , "LICENSE file missing!")
        End Try

        Dim child As XmlNode
        child = doc.SelectSingleNode("/LICENSE/DB_LOGIN")
        If Not (child Is Nothing) Then
            sDBLogin = Encrypt(child.InnerText, , False, TipoDeUsuario.Administrador)
        End If
        child = Nothing
        child = doc.SelectSingleNode("/LICENSE/DB_PASSWORD")
        If Not (child Is Nothing) Then
            sDBPassword = Encrypt(child.InnerText, , False, TipoDeUsuario.Administrador)
        Else
            sDBPassword = ""
        End If

        child = doc.SelectSingleNode("/LICENSE/DB_NAME")
        If Not (child Is Nothing) Then
            sDBName = Encrypt(child.InnerText, , False, TipoDeUsuario.Administrador)
        End If
        child = Nothing
        child = doc.SelectSingleNode("/LICENSE/DB_SERVER")
        If Not (child Is Nothing) Then
            sDBServer = Encrypt(child.InnerText, , False, TipoDeUsuario.Administrador)
        End If

        If sDBServer = "" Or sDBName = "" Or sDBLogin = "" Then
            Err.Raise(10000, , "Corrupted LICENSE file !")
        End If

        'PENDIENTE METER ENCRIPTACIÓN

        sDB = sDB.Replace("DB_LOGIN", sDBLogin)
        sDB = sDB.Replace("DB_PASSWORD", sDBPassword)
        sDB = sDB.Replace("DB_NAME", sDBName)
        sDB = sDB.Replace("DB_SERVER", sDBServer)
        doc = Nothing
        _sConnectionString = sDB
    End Sub

#End Region

    ''' <summary>Guarda un error en BD</summary>
    ''' <param name="ex">Error</param>
    ''' <param name="sOrigenError">Origen del error</param>
    ''' <returns>Id del error creado</returns>
    ''' <remarks>Llamada desde: </remarks>

    Public Function GuardarError(ByVal ex As Exception, ByVal sOrigenError As String) As Long
        Dim cn As New SqlConnection(_sConnectionString)
        Dim cm As New SqlCommand
        Dim dr As SqlDataReader = Nothing
        Try
            cn.Open()
            With cm
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "FSPM_INSERTAR_ERROR"
                .Parameters.AddWithValue("@PAGINA", "FSNWindowsServiceCustom." & sOrigenError)
                .Parameters.AddWithValue("@USUARIO", String.Empty)
                .Parameters.AddWithValue("@FULLNAME", ex.GetType().FullName)
                .Parameters.AddWithValue("@MESSAGE", IIf(String.IsNullOrEmpty(ex.Message), String.Empty, ex.Message))
                .Parameters.AddWithValue("@STACKTRACE", IIf(String.IsNullOrEmpty(ex.StackTrace), String.Empty, ex.StackTrace))
                .Parameters.AddWithValue("@QUERYSTRING", String.Empty)
                .Parameters.AddWithValue("@USERAGENT", String.Empty)
                .Parameters.AddWithValue("@ID", SqlDbType.Int).Direction = ParameterDirection.Output
                .ExecuteNonQuery()
                Return DBNullToSomething(cm.Parameters("@ID").Value)
            End With
        Catch e As Exception
            Throw e
        Finally
            If Not dr Is Nothing AndAlso Not dr.IsClosed Then
                dr.Close()
            End If
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function

    Public Function LCX_DevolverContratosAlertaEncuesta() As DataSet
        Dim cn As New SqlConnection(_sConnectionString)
        Dim cm As SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = "LCX_DEVOLVER_CONTRATOS_ALERTA_ENCUESTA"
            cm.CommandType = CommandType.StoredProcedure

            da.SelectCommand = cm
            da.Fill(ds)

            Return ds
        Catch e As Exception
            GuardarError(e, "BDRoot.LCX_DevolverContratosAlertaEncuesta")
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function

    ''' <summary>Cargar los parametros necesarios para las notificaciones</summary>
    ''' <returns>Estructura de datos con los valores de los parámetros</returns>
    ''' <remarks>Llamada desde: CNotificar.New</remarks>
    Public Function DevolverPargenMail() As Dictionary(Of String, Object)
        Dim cn As New SqlConnection(_sConnectionString)
        Dim dr As SqlDataReader
        Dim cm As SqlCommand

        Try
            cm = New SqlCommand("FSGS_DEVOLVER_PARAM_CONFIG_SERVICIOEMAIL", cn)
            cm.CommandType = CommandType.StoredProcedure
            cn.Open()
            dr = cm.ExecuteReader()

            dr.Read()

            Dim oValues As New Dictionary(Of String, Object)
            oValues.Add("AUTENTICACION", dr.Item("AUTENTICACION"))
            oValues.Add("USU", dr.Item("USU"))
            oValues.Add("PWD", dr.Item("PWD"))
            oValues.Add("FECPWD", dr.Item("FECPWD"))
            oValues.Add("CUENTAMAIL", dr.Item("CUENTAMAIL"))
            oValues.Add("DOMINIO", dr.Item("DOMINIO"))
            oValues.Add("SERVIDOR", dr.Item("SERVIDOR"))
            oValues.Add("PUERTO", dr.Item("PUERTO"))
            oValues.Add("SSL", dr.Item("SSL"))
            oValues.Add("METODO_ENTREGA", dr.Item("METODO_ENTREGA"))

            Return oValues
        Catch ex As Exception
            GuardarError(ex, "BDRoot.DevolverPargenMail")
        Finally
            dr.Close()
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function

    ''' <summary>Función que graba en la tabla REGISTRO_EMAIL toda la informacion del email enviado.</summary>
    ''' <param name="From">El from del mensaje</param>
    ''' <param name="Subject">El asunto</param>
    ''' <param name="Para">El destinatario</param>
    ''' <param name="Message">El mensaje</param>    
    ''' <param name="bHTML">Si el email se ha enviado con formato HTML o no.</param>
    ''' <param name="bKO">Si ha ido bien el envi­o.</param>
    ''' <param name="sTextoError">El texto de error (si hay)</param>
    ''' <param name="CodProve">Proveedor al q se le envia el mail</param>
    ''' <param name="IdMail">Tipo de Notificacion. Es un valor de FSNLibrary.TipoNotificacion</param>
    ''' <param name="lIdInstancia">Id de la instancia a la que asociaremos la notificacion</param>
    ''' <param name="ParaNombre">Nombre y apellidos del q recibe el mail</param> 
    ''' <param name="Usuario">Usuario</param>  
    ''' <param name="IDError">id del error en tabla errores</param>
    ''' <remarks>Llamada desde: PMNotificador --> Notificar.vb --> Enviarmail. Tiempo maximo: 0 sg.</remarks>
    Public Sub GrabarEnviarMensaje(ByVal From As String, ByVal Subject As String, ByVal Para As String, ByVal Message As String, ByVal bHTML As Boolean, ByVal bKO As Boolean, ByVal sTextoError As String, _
                                   ByVal CodProve As String, ByVal IdMail As Integer, ByVal lIdInstancia As Long, ByVal ParaNombre As String, ByVal Usuario As String, Optional ByVal IDError As Long = -1)
        Dim cn As New SqlConnection(_sConnectionString)
        Dim cm As SqlCommand
        Dim lRegMailId As Long          

        Try
            cn.Open()

            cm = New SqlCommand("BEGIN TRANSACTION", cn)
            cm.ExecuteNonQuery()

            cm = New SqlCommand("FSGS_INSERT_REGISTRO_MAIL", cn)
            cm.CommandType = CommandType.StoredProcedure

            If IsNothing(Subject) OrElse Subject = "" Then Subject = "Error antes de crear subject"
            cm.Parameters.AddWithValue("@SUBJECT", Subject)
            If IsNothing(Para) OrElse Para = "" Then Para = "Error antes de crear para"
            cm.Parameters.AddWithValue("@PARA", Para)
            cm.Parameters.AddWithValue("@CC", "")
            cm.Parameters.AddWithValue("@CCO", "")
            cm.Parameters.AddWithValue("@DIR_RESPUESTA", From)
            If IsNothing(Message) OrElse Message = "" Then Message = "Error antes de crear cuerpo"
            cm.Parameters.AddWithValue("@CUERPO", Message)
            cm.Parameters.AddWithValue("@ACUSE_RECIBO", 0)
            cm.Parameters.AddWithValue("@TIPO", IIf(bHTML, 1, 0))
            cm.Parameters.AddWithValue("@PRODUCTO", Producto.CM)
            cm.Parameters.AddWithValue("@USU", IIf(String.IsNullOrEmpty(Usuario), "", Usuario))
            cm.Parameters.AddWithValue("@KO", IIf(bKO, 1, 0))
            cm.Parameters.AddWithValue("@TEXTO_ERROR", sTextoError)
            cm.Parameters.Add("@IDENTIFICADOR", SqlDbType.Int).Direction = ParameterDirection.Output

            cm.Parameters.AddWithValue("@PROVE", CodProve)
            cm.Parameters.AddWithValue("@ID_EMAIL", IdMail)
            If lIdInstancia <> 0 Then cm.Parameters.AddWithValue("@INSTANCIA", lIdInstancia)
            cm.Parameters.AddWithValue("@ENTIDAD", EntidadNotificacion.Contrato)
            If IDError > -1 Then
                cm.Parameters.AddWithValue("@ID_ERROR", IDError)
            Else
                cm.Parameters.AddWithValue("@ID_ERROR", System.DBNull.Value)
            End If
            If ParaNombre <> "" Then cm.Parameters.AddWithValue("@PARA_NOMBRE", ParaNombre)

            cm.ExecuteNonQuery()

            lRegMailId = cm.Parameters("@IDENTIFICADOR").Value

            cm = New SqlCommand("COMMIT TRANSACTION", cn)
            cm.ExecuteNonQuery()
        Catch ex As Exception
            Try
                cm = New SqlCommand("FSGS_UPDATE_REGISTRO_MAIL", cn)
                cm.CommandType = CommandType.StoredProcedure

                cm.Parameters.AddWithValue("@IDENTIFICADOR", lRegMailId)
                cm.Parameters.AddWithValue("@TEXTO_ERROR", ex.Message)

                cm.ExecuteNonQuery()

                cm = New SqlCommand("COMMIT TRANSACTION", cn)
                cm.ExecuteNonQuery()

            Catch ex2 As Exception
                cm = New SqlCommand("ROLLBACK TRANSACTION", cn)
                cm.ExecuteNonQuery()
            End Try
        Finally
            cn.Dispose()
            cm.Dispose()            
        End Try
    End Sub

End Class
