﻿Public Class CProveedor
    Inherits CBase

    Private _sCodigo As String
    Private _sDenominacion As String
    Private _sNIF As String
    Private _sContactoNombre As String
    Private _sContactoApellido As String
    Private _sContactoEMail As String

#Region "Propiedades"

    Public Property Codigo As String
        Get
            Return _sCodigo
        End Get
        Set(value As String)
            _sCodigo = value
        End Set
    End Property

    Public Property Denominacion As String
        Get
            Return _sDenominacion
        End Get
        Set(value As String)
            _sDenominacion = value
        End Set
    End Property

    Public Property NIF As String
        Get
            Return _sNIF
        End Get
        Set(value As String)
            _sNIF = value
        End Set
    End Property

    Public Property ContactoNombre As String
        Get
            Return _sContactoNombre
        End Get
        Set(value As String)
            _sContactoNombre = value
        End Set
    End Property

    Public Property ContactoApellido As String
        Get
            Return _sContactoApellido
        End Get
        Set(value As String)
            _sContactoApellido = value
        End Set
    End Property

    Public Property ContactoEMail As String
        Get
            Return _sContactoEMail
        End Get
        Set(value As String)
            _sContactoEMail = value
        End Set
    End Property

#End Region

#Region "Constructor"

    Public Sub New(ByVal sCodigo As String, ByVal sDenominacion As String, ByVal sNIF As String, ByVal sContactoNombre As String, ByVal sContactoApellido As String, ByVal sEMailContacto As String)
        _sCodigo = sCodigo
        _sDenominacion = sDenominacion
        _sNIF = sNIF
        _sContactoNombre = sContactoNombre
        _sContactoApellido = sContactoApellido
        _sContactoEMail = sEMailContacto
    End Sub

#End Region

#Region "Métodos públicos"

    ''' <summary>Devuelve la denominación del contacto (nombre+apellido)</summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ContactoDenominacion() As String
        Dim sDen As String = String.Empty

        If Not _sContactoNombre Is Nothing AndAlso _sContactoNombre.Length > 0 Then sDen &= _sContactoNombre
        If Not _sContactoApellido Is Nothing AndAlso _sContactoApellido.Length > 0 Then
            If sDen.Length > 0 Then sDen &= " "
            sDen &= _sContactoApellido
        End If

        Return sDen
    End Function

#End Region

End Class
