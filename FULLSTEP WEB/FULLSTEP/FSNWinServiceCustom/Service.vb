﻿Imports System.ServiceProcess
Imports System.Configuration
Imports System.Threading
Public Class Service
#Region "Variables globales"
    Private sPath As String
    Private m_sFile As String
    Private m_bActivoServicioAlertaEncuesta As Boolean = True
    Private m_DiasEjecucion_LCX_AlertaEncuesta As Double
    Private m_HoraEjecucion_LCX_AlertaEncuesta As String
    Private m_DiaArranque_LCX_AlertaEncuesta As Date
    Private WithEvents temporizador As Timers.Timer
#End Region
#If DEBUG Then
    Public Sub EmpezarDebug()
        Me.OnStart(Nothing)
    End Sub
#End If
    Protected Overrides Sub OnStart(ByVal args() As String)
#If DEBUG Then
        System.Diagnostics.Debugger.Launch()
#End If
        LeerParametros()
        ArrancarTemporizador()
    End Sub
    ''' <summary>Lee los parámetros de configuración del servicio del archivo app.config</summary>
    ''' <remarks>Llamada desde. Service1.OnStart</remarks>
    Private Sub LeerParametros()
        Dim sPath As String
        sPath = ConfigurationManager.AppSettings("Path")
        m_sFile = sPath & "\LCX_Contratos_AlertaEncuesta.txt"

        If (ConfigurationManager.AppSettings("DIAS_SERVICIO_LCX_ALERTA_ENCUESTA") = "") _
        OrElse (ConfigurationManager.AppSettings("HORA_EJECUCION_SERVICIO_LCX_ALERTA_ENCUESTA") = "") _
        OrElse (Not IsDate(ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_LCX_ALERTA_ENCUESTA"))) Then
            m_bActivoServicioAlertaEncuesta = False
            GuardarError(New Exception(My.Resources.Resource1.AppConfigIncorrecto))
        Else
            m_DiasEjecucion_LCX_AlertaEncuesta = ConfigurationManager.AppSettings("DIAS_SERVICIO_LCX_ALERTA_ENCUESTA")
            m_HoraEjecucion_LCX_AlertaEncuesta = ConfigurationManager.AppSettings("HORA_EJECUCION_SERVICIO_LCX_ALERTA_ENCUESTA")
            m_DiaArranque_LCX_AlertaEncuesta = ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_LCX_ALERTA_ENCUESTA")
        End If
    End Sub
    ''' <summary>Arranca el temporizador que regulará la periodicidad con que se revisará la necesidad de enviar alertas</summary>
    ''' <remarks>Llamadad esde: OnStart</remarks>
    Private Sub ArrancarTemporizador()
        temporizador = New Timers.Timer
        temporizador.AutoReset = True

        'Cada minuto se comprueba si se ha cumplido el periodo indicado en diasEjecucion para su ejecucion
        temporizador.Interval = (60 * 1000)
        temporizador.Enabled = True
        If m_bActivoServicioAlertaEncuesta Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_LCX_AlertaEncuesta))

        temporizador.Start()
    End Sub
    ''' <summary>Guarda un error en BD</summary>
    ''' <param name="oError">Error</param>
    ''' <remarks>Llamada desde. LeerParametros</remarks>
    Private Sub GuardarError(ByVal oError As Exception)
        Dim oContratos As New CContratos
        oContratos.GuardarError(oError, "Service1")
    End Sub
    Private Sub _oTemporizador_Elapsed(sender As Object, e As System.Timers.ElapsedEventArgs) Handles temporizador.Elapsed
        If m_bActivoServicioAlertaEncuesta Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_LCX_AlertaEncuesta))
    End Sub
    Private Function EjecucionServicio_LCX_AlertaEncuesta() As Boolean
        EjecucionServicio_LCX_AlertaEncuesta = True
        Dim dFechaHora As Date
        Dim dFechaHoraFichero As Date = System.IO.File.GetLastWriteTime(m_sFile)
        dFechaHora = FechaProximaEjecucionServicio_LCX_AlertaEncuesta()
        If DateDiff("n", Now(), dFechaHora) <= 0 Then
            'Decidimos borrar y escribir el fichero antes de la ejecucion del servicio propiamente dicho para que si el servicio tarda mas de lo debido
            'el archivo tenga fecha de modificacion actual y asi no lance por segunda vez el servicio.
            'Borrar Fichero
            BorrarFicheroServicio_LCX_AlertaEncuesta()
            'Escribir Fichero log, para indicarle cuando se realizo por ultima vez
            EscribirFicheroServicio_LCX_AlertaEncuesta(Now(), dFechaHora, dFechaHoraFichero)
            'LlamarWebService
            LCX_ContratosAlertasEncuestas()
        End If
        EjecucionServicio_LCX_AlertaEncuesta = False
    End Function
    Private Function FechaProximaEjecucionServicio_LCX_AlertaEncuesta() As Date
        Dim fechaFichero As Date
        'If no existe fichero de expiracion certif
        If Not System.IO.File.Exists(m_sFile) Then
            FechaProximaEjecucionServicio_LCX_AlertaEncuesta = CDate(m_DiaArranque_LCX_AlertaEncuesta & " " & m_HoraEjecucion_LCX_AlertaEncuesta)
        Else
            'fecha anterior ejecucion con la hora de ejecucion del servicio
            fechaFichero = System.IO.File.GetLastWriteTime(m_sFile)
            FechaProximaEjecucionServicio_LCX_AlertaEncuesta = fechaFichero

            'suma el periodo diario en dias a la fecha de la ultima ejecucion
            FechaProximaEjecucionServicio_LCX_AlertaEncuesta = DateAdd("d", CDbl(m_DiasEjecucion_LCX_AlertaEncuesta), FechaProximaEjecucionServicio_LCX_AlertaEncuesta)

            FechaProximaEjecucionServicio_LCX_AlertaEncuesta = New Date(Year(FechaProximaEjecucionServicio_LCX_AlertaEncuesta), Month(FechaProximaEjecucionServicio_LCX_AlertaEncuesta), Day(FechaProximaEjecucionServicio_LCX_AlertaEncuesta), Hour(m_HoraEjecucion_LCX_AlertaEncuesta), Minute(m_HoraEjecucion_LCX_AlertaEncuesta), 0)
        End If
    End Function
    Private Sub BorrarFicheroServicio_LCX_AlertaEncuesta()
        System.IO.File.Delete(m_sFile)
    End Sub
    Private Sub EscribirFicheroServicio_LCX_AlertaEncuesta(ByVal fechaActual As Date, ByVal FechaEjecucion As Date, ByVal fechaFichero As Date)
        Dim str = "Fichero de Alerta de encuesta LCX. "
        Dim oFileW As System.IO.StreamWriter
        oFileW = New System.IO.StreamWriter(m_sFile, True)
        oFileW.WriteLine(str)
        oFileW.WriteLine("Fecha Actual = " & fechaActual)
        oFileW.WriteLine("Fecha Ejecucion = " & FechaEjecucion)
        oFileW.WriteLine("Fecha Fichero = " & fechaFichero)
        oFileW.Close()
    End Sub
    ''' <summary>Envía las alertas de encuestas de contratos</summary>
    ''' <remarks></remarks>
    Private Sub LCX_ContratosAlertasEncuestas()
        Try
            Dim oContratos As New CContratos
            oContratos.LCX_CargarContratosAlertaEncuesta()
            oContratos.LCX_ContratosEnviarMailsAlertaEncuesta()
        Catch ex As Exception
            GuardarError(ex)
        End Try
    End Sub
End Class