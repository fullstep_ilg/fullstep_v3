﻿Public Class CPersona
    Inherits CBase

    Private _sCodigo As String
    Private _sNombre As String
    Private _sApellido As String
    Private _sTelefono As String
    Private _sEMail As String
    Private _sIdioma As String
    Private _iTipoEmail As Integer
    Private _sFormatoFecha As String

#Region "Propiedades"

    Public Property Codigo As String
        Get
            Return _sCodigo
        End Get
        Set(value As String)
            _sCodigo = value
        End Set
    End Property

    Public Property Nombre As String
        Get
            Return _sNombre
        End Get
        Set(value As String)
            _sNombre = value
        End Set
    End Property

    Public Property Apellido As String
        Get
            Return _sApellido
        End Get
        Set(value As String)
            _sApellido = value
        End Set
    End Property

    Public Property Telefono As String
        Get
            Return _sTelefono
        End Get
        Set(value As String)
            _sTelefono = value
        End Set
    End Property

    Public Property EMail As String
        Get
            Return _sEMail
        End Get
        Set(value As String)
            _sEMail = value
        End Set
    End Property

    Public Property Idioma As String
        Get
            Return _sIdioma
        End Get
        Set(value As String)
            _sIdioma = value
        End Set
    End Property

    Public Property TipoEmail As Integer
        Get
            Return _iTipoEmail
        End Get
        Set(value As Integer)
            _iTipoEmail = value
        End Set
    End Property

    Public Property FormatoFecha As String
        Get
            Return _sFormatofecha
        End Get
        Set(value As String)
            _sFormatofecha = value
        End Set
    End Property

#End Region

#Region "Cosntructor"

    Public Sub New(ByVal sCodigo As String, ByVal sNombre As String, ByVal sApellido As String, ByVal sTelefono As String, ByVal sEMail As String, ByVal sIdioma As String, ByVal iTipoEmail As Integer, ByVal sFormatoFecha As String)
        _sCodigo = sCodigo
        _sNombre = sNombre
        _sApellido = sApellido
        _sTelefono = sTelefono
        _sEMail = sEMail
        _sIdioma = sIdioma
        _iTipoEmail = iTipoEmail
        _sFormatoFecha = sFormatoFecha
    End Sub

#End Region

#Region "Métodos públicos"

    ''' <summary>Devuelve el nombre de la plantilla para el mail de alerta</summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function PlantillaAlerta() As String
        Return _sIdioma & "_AlertaEncuesta" & IIf(_iTipoEmail = 0, ".txt", ".html")
    End Function

    ''' <summary>Devuelve la denominación de la persona (nombre y apellido)</summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Denominacion() As String
        If Not _sNombre Is Nothing AndAlso _sNombre.Length > 0 Then
            Return _sNombre & " " & _sApellido
        Else
            Return _sApellido
        End If
    End Function

#End Region

End Class
