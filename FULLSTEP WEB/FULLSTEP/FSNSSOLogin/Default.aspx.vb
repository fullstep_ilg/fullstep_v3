﻿Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Web.Hosting
Imports Fullstep.FSNServer.Saml

Public Class _Default
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Dim cert = New X509Certificate2(HostingEnvironment.MapPath("~/App_Data/fullstep.net.pfx"), "fullSTEP-3-4")
		Dim certBytes = cert.RawData
		Dim samlCertificate As String = Convert.ToBase64String(certBytes)
		Dim samlResponse As Response = New Response(samlCertificate)

		samlResponse.LoadXmlFromBase64(Page.Request.Form("SAMLResponse"))
		File.WriteAllText(MapPath("~/App_Data/") & "Berge.xml", samlResponse.Xml.ToString)

		If samlResponse.IsValid() Then
			Dim sSession As String = Nothing
			Dim webService As New FSNSessionService.SessionService
			sSession = webService.GetNewSessionID(sSession)

			webService.SetSessionData(sSession, "Usuario", Split(samlResponse.GetNameID(), "@")(0))
			Response.Redirect(ConfigurationManager.AppSettings("ruta") & "Default.aspx?sSession=" & sSession)
		End If
	End Sub
End Class