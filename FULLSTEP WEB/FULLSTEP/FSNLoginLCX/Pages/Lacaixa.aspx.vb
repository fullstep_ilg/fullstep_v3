﻿Public Class lacaixa
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.form1.Action = ConfigurationManager.AppSettings("rutaSubmit")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ruta", "var ruta='" & ConfigurationManager.AppSettings("ruta") & "'", True)
    End Sub

    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function ValidarPWD(ByVal sPwd As String) As Boolean
        Dim oUsuario As New Usuario
        Return oUsuario.ComprobarPwdLoginLCX(sPwd)
    End Function

End Class