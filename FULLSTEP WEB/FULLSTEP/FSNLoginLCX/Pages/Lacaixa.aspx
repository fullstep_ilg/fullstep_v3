﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Lacaixa.aspx.vb" Inherits="FSNLoginLCX.lacaixa"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>          
    <style type="text/css">
        .style1
        {
            text-decoration: underline;
        }
        .titulo{            
            font-weight:bold;
            font-size:xx-large;
            padding-top :1em;
            padding-bottom :1em;
            display:inline-block;
            color:#666666;
            font-style:italic;
        }
        .fuente{
            font-family: Arial, Helvetica, sans-serif;
            font-size:small; 
            color:#666666;           
        }
        input{
            font-family: Arial, Helvetica, sans-serif;
            font-size:small;
            color:#666666;
        }
    </style>
</head>
<body>
<form id="form1" onsubmit="return validarPWD();" runat="server" method="post">
    <asp:ScriptManager ID="ScriptManager1" runat="server">                    
		<CompositeScript>
			<Scripts>
				<asp:ScriptReference Path="~/js/jquery.min.js" />	
                <asp:ScriptReference Path="~/js/json2.js" />					
			</Scripts>
		</CompositeScript>		
    </asp:ScriptManager>     

    <script lang="javascript" type="text/javascript">
        function txtCodCentro_onclick() {
        }

        function validarPWD() {
            var ret;
            
            $.when($.ajax({
                type: "POST",
                url: ruta + '/Pages/Lacaixa.aspx/ValidarPWD',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ sPwd: $('#txtPwd')[0].value }),
                dataType: "json",
                async: false
            })).done(function (msg) {                
                ret = msg.d;

                if (!ret) alert('La contraseña introducida no es correcta');
            })
            return ret;
        }
    </script>
        
    <div style="width:60%; text-align:left; position:absolute; left:10%;" class="fuente">
        <div><img src="../images/logo.png" style="display:inline-block;"/></div>
        <span class="titulo">LOGIN DE ACCESO A LA CAIXA</span>
        <div style="clear:both; padding-bottom:0.3em;">
            <span style="width:15em; display:inline-block;">Introduzca el código de usuario:</span>
            <input id="txtCodUsu" name="txtCodUsu" value="" style="display:inline-block;"/>
        </div>
        <div style="clear:both; padding-bottom:0.3em;">
            <span style="width:15em; display:inline-block;">Introduzca el código de empresa:</span>
            <input id="txtCodEmpresa" name="txtCodEmpresa" value="" style="display:inline-block;"/><br />
        </div>
        <div style="clear:both; padding-bottom:0.3em;">
            <span style="width:15em; display:inline-block;">Introduzca el código de centro:</span>
            <input id="txtCodCentro" name="txtCodCentro" value="" style="display:inline-block;" onclick="return txtCodCentro_onclick()" /><br />
        </div>
        <div style="clear:both; padding-bottom:0.3em;">
            <span style="width:15em; display:inline-block;">Introduzca el código de perfil:</span>
            <input id="txtPerfil" name="txtPerfil" value="Administrador" style="display:inline-block;"/><br/>
        </div>
        <div style="clear:both; padding-bottom:0.3em;">
            <span style="width:15em; display:inline-block;">Introduzca la contraseña:</span>
            <input type="password" id="txtPwd" name="txtPwd" value="" style="display:inline-block;"/><br/>
        </div>  
        <div style="padding-top:1em; text-align:center;">            
            <img src="../images/login.gif" onclick="if (validarPWD()) {document.forms[0].submit();}"/>
        </div>         
        <br/>
        <br/>
        <br/>
        <em><span class="style1">Perfiles disponibles: </span><span>Administrador, Gestor, Consulta, Auditor, Compras</span></em> 
        <br/>
        <em><span class="style1">Empresas disponibles: </span><span>00001</span></em>
        <br/>
        <em><span class="style1">Centros disponibles: </span><span>09053,09058,09061,09065,09094,09105,09106,09111,09114,09130,09261,09305,09306,09331,09338,09340,09345,09350</span>
        <br/>
        <span>,09383,09385,09393,09394,09422,09425,09474,09484,09498,09503,09504,09507,09520,09521,09530,09545,09620,09636,09641,09645,09652</span>
        <br/>
        <span>,09653,09654,09655,09664,09666,09672,09673,09681,09691,09701,09702,09703,09704,09706,09707,09712,09716,09722,09724,09727,09734</span>
        <br/>
        <span>,09736,09740,09745,09746,09747,09750,09752,09755,09758,09759,09761,09764,09767,09768,09769,09771,09776,09779,09800,09805,09815</span>
        <br/>
        <span>,09821,09826,09833,09834,09836,09852,09853,09855,09860,09885,09906,09917</span>
        </em>
    </div>        
</form>
</body>
</html>
