﻿Imports Fullstep.FSNLibrary.Encrypter

Public Class Usuario

    Public Function ComprobarPwdLoginLCX(ByVal sPWD As String) As Boolean
        Dim oBD As New BDRoot
        Dim dsData As DataSet = oBD.DevolverPWDLoginLCX()
        Dim sPwdLogin As String = Encrypt(dsData.Tables(0).Rows(0)("PWD"), , False, TipoDeUsuario.Persona, dsData.Tables(0).Rows(0)("FECPWD"))
        Return sPwdLogin.Equals(sPWD)
    End Function

End Class
