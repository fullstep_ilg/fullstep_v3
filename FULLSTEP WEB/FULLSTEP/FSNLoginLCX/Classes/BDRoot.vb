﻿Imports System.Data.SqlClient
Imports System.Xml
Imports Fullstep.FSNLibrary.modUtilidades
Imports Fullstep.FSNLibrary.Encrypter
Imports System.Configuration

Public Class BDRoot
    Private _sConnectionString As String

#Region "Constructor"

    Public Sub New()
        Dim sDBLogin As String = ""
        Dim sDBPassword As String = ""
        Dim sDBName As String = ""
        Dim sDBServer As String = ""

        'Comprobar tema de licencias y cargar datos de conexión
        Dim sDB As String = ConfigurationManager.AppSettings("DB:FS_WS")
        If sDB Is Nothing Then
            Err.Raise(10000, , "Configuration file corrupted. Missing FS_WS entry !")
        End If
        'Tenemos que extraer el login y la contraseña de conexión.
        Dim doc As New XmlDocument

        Try
            doc.Load(System.AppDomain.CurrentDomain.BaseDirectory() & "License.xml")

        Catch
            Err.Raise(10000, , "LICENSE file missing!")
        End Try

        Dim child As XmlNode
        child = doc.SelectSingleNode("/LICENSE/DB_LOGIN")
        If Not (child Is Nothing) Then
            sDBLogin = Encrypt(child.InnerText, , False, TipoDeUsuario.Administrador)
        End If
        child = Nothing
        child = doc.SelectSingleNode("/LICENSE/DB_PASSWORD")
        If Not (child Is Nothing) Then
            sDBPassword = Encrypt(child.InnerText, , False, TipoDeUsuario.Administrador)
        Else
            sDBPassword = ""
        End If

        child = doc.SelectSingleNode("/LICENSE/DB_NAME")
        If Not (child Is Nothing) Then
            sDBName = Encrypt(child.InnerText, , False, TipoDeUsuario.Administrador)
        End If
        child = Nothing
        child = doc.SelectSingleNode("/LICENSE/DB_SERVER")
        If Not (child Is Nothing) Then
            sDBServer = Encrypt(child.InnerText, , False, TipoDeUsuario.Administrador)
        End If

        If sDBServer = "" Or sDBName = "" Or sDBLogin = "" Then
            Err.Raise(10000, , "Corrupted LICENSE file !")
        End If

        'PENDIENTE METER ENCRIPTACIÓN

        sDB = sDB.Replace("DB_LOGIN", sDBLogin)
        sDB = sDB.Replace("DB_PASSWORD", sDBPassword)
        sDB = sDB.Replace("DB_NAME", sDBName)
        sDB = sDB.Replace("DB_SERVER", sDBServer)
        doc = Nothing
        _sConnectionString = sDB
    End Sub

#End Region

    Public Function DevolverPWDLoginLCX() As DataSet
        Dim cn As New SqlConnection(_sConnectionString)
        Dim cm As SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = "LCX_DEVOLVER_PWD_LOGIN"
            cm.CommandType = CommandType.StoredProcedure

            da.SelectCommand = cm
            da.Fill(ds)

            Return ds
        Catch e As Exception
            GuardarError(e, "BDRoot.DevolverPWDLoginLCX")
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function

    ''' <summary>Guarda un error en BD</summary>
    ''' <param name="ex">Error</param>
    ''' <param name="sOrigenError">Origen del error</param>
    ''' <returns>Id del error creado</returns>
    ''' <remarks>Llamada desde: </remarks>

    Public Function GuardarError(ByVal ex As Exception, ByVal sOrigenError As String) As Long
        Dim cn As New SqlConnection(_sConnectionString)
        Dim cm As New SqlCommand
        Dim dr As SqlDataReader = Nothing
        Try
            cn.Open()
            With cm
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "FSPM_INSERTAR_ERROR"
                .Parameters.AddWithValue("@PAGINA", "FSNWindowsServiceCustom." & sOrigenError)
                .Parameters.AddWithValue("@USUARIO", String.Empty)
                .Parameters.AddWithValue("@FULLNAME", ex.GetType().FullName)
                .Parameters.AddWithValue("@MESSAGE", IIf(String.IsNullOrEmpty(ex.Message), String.Empty, ex.Message))
                .Parameters.AddWithValue("@STACKTRACE", IIf(String.IsNullOrEmpty(ex.StackTrace), String.Empty, ex.StackTrace))
                .Parameters.AddWithValue("@QUERYSTRING", String.Empty)
                .Parameters.AddWithValue("@USERAGENT", String.Empty)
                .Parameters.AddWithValue("@ID", SqlDbType.Int).Direction = ParameterDirection.Output
                .ExecuteNonQuery()
                Return DBNullToSomething(cm.Parameters("@ID").Value)
            End With
        Catch e As Exception
            Throw e
        Finally
            If Not dr Is Nothing AndAlso Not dr.IsClosed Then
                dr.Close()
            End If
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function

End Class
