﻿Imports System.Configuration
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Exception
Imports System.IO
Imports System.Object
Imports Fullstep.FSNLibrary

Partial Public Class Root

#Region "FSAL"

    Public Function FSAL_RegistrarAccesosASPX(ByVal tipo As Int16, ByVal sProducto As String, ByVal fechapet As String, ByVal fecha As String, ByVal pagina As String, ByVal ipost As Int16, ByVal iP As String, ByVal sUsuCod As String, ByVal sPaginaOrigen As String, ByVal sNavegador As String, ByVal sIdRegistro As String, ByVal sProveCod As String, ByVal sQueryString As String) As Integer
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter
        Dim rvp As New SqlParameter("@OUTPUT", SqlDbType.Int)     'Se declara y añade el parametro de salida, pero no le damos tratamiento de momento.
        Try
            cn.Open()
            cm.Connection = cn

            cm.CommandText = "FSAL_REGISTRAR_ACCESO_ASPX" '
            cm.Parameters.AddWithValue("@TIPO", tipo)
            cm.Parameters.AddWithValue("@PAGINA", pagina)
            cm.Parameters.AddWithValue("@FPETICION", fechapet)
            cm.Parameters.AddWithValue("@FECHA", fecha)
            cm.Parameters.AddWithValue("@IDSESION", "")
            If (sUsuCod Is Nothing) OrElse (sUsuCod = "") Then
                sUsuCod = "AntesLogin"
            End If
            cm.Parameters.AddWithValue("@USUCOD", sUsuCod)
            cm.Parameters.AddWithValue("@IPDIR", iP)
            cm.Parameters.AddWithValue("@PRODUCTO", sProducto)
            If sPaginaOrigen Is Nothing Then
                sPaginaOrigen = ""
            End If
            cm.Parameters.AddWithValue("@PAGINAORIGEN", sPaginaOrigen)
            cm.Parameters.AddWithValue("@POSTBACK", ipost)
            cm.Parameters.AddWithValue("@NAVEGADOR", sNavegador)
            cm.Parameters.AddWithValue("@IDREGISTRO", sIdRegistro)
            If sProveCod Is Nothing Then
                sProveCod = ""
            End If
            cm.Parameters.AddWithValue("@CIACOD", sProveCod)
            If sQueryString Is Nothing Then
                sQueryString = ""
            End If
            cm.Parameters.AddWithValue("@QUERYSTRING", sQueryString)
            rvp.Direction = ParameterDirection.Output
            cm.Parameters.Add(rvp)
            cm.CommandType = CommandType.StoredProcedure
            cm.ExecuteNonQuery()
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function FSAL_ActualizarAccesosASPX(ByVal tipo As Int16, ByVal fechapet As String, ByVal fecha As String, ByVal sIdRegistro As String, ByVal pagina As String, ByVal iP As String) As Integer
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter
        Dim rvp As New SqlParameter("@OUTPUT", SqlDbType.Int)
        Dim iIntentosExec As Integer = 5
        Dim iMilisegundosEspera = 200
        Try
            cn.Open()
            cm.Connection = cn

            cm.CommandText = "FSAL_REGISTRAR_ACCESO_ASPX"
            cm.Parameters.AddWithValue("@TIPO", tipo)
            cm.Parameters.AddWithValue("@PAGINA", pagina)
            cm.Parameters.AddWithValue("@FPETICION", fechapet)
            cm.Parameters.AddWithValue("@FECHA", fecha)
            cm.Parameters.AddWithValue("@IDSESION", "")
            cm.Parameters.AddWithValue("@USUCOD", "USUCOD")
            cm.Parameters.AddWithValue("@IPDIR", iP)
            cm.Parameters.AddWithValue("@PRODUCTO", "PRODUCTO")
            cm.Parameters.AddWithValue("@NAVEGADOR", "NAVEGADOR")
            cm.Parameters.AddWithValue("@POSTBACK", 0)
            cm.Parameters.AddWithValue("@IDREGISTRO", sIdRegistro)
            rvp.Direction = ParameterDirection.Output
            cm.Parameters.Add(rvp)
            cm.CommandType = CommandType.StoredProcedure
            cm.ExecuteNonQuery()
            'Se comprueba que el stored se ha ejecutado hasta el final y si no, se espera un lapso y se vuelve a intentar, un máximo de 3 veces.
            'Esto se debe a que puede llegar antes la llamada al webservice para actualizar que para registrar, y en ese caso, 
            'al no existir registro que actualizar, el stored devuelve un valor <> 0 sin llegar a terminar toda su ejecucion.
            If CInt(rvp.Value) <> 0 Then
                Do Until (CInt(rvp.Value) = 0) Or (iIntentosExec = 0)
                    Threading.Thread.Sleep(iMilisegundosEspera)
                    cm.ExecuteNonQuery()
                    iIntentosExec -= 1
                Loop
            End If
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
#End Region
End Class