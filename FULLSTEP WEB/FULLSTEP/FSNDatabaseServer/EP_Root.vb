﻿Imports System.Configuration
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Exception
Imports System.IO
Imports System.Object
Imports Fullstep.FSNLibrary
Partial Public Class Root
#Region "EP Data Access Methods"
#Region " Material data access methods "
	''' <summary>
	''' Función que devuelve un DataSet con el arbol de los materiales
	''' </summary>
	''' <returns>Un DataSet con el arbol completo de materiales, identificados por su código, y con su descripción, así como el identificador del padre.</returns>
	''' <remarks>
	''' Llamada desde: La Clase cMaterial, metodo DevolverArbolMateriales
	''' Tiempo máximo: 1 seg</remarks>
	Public Function Material_DevolverArbolMateriales() As DataSet
		Authenticate()
		Dim Cn As New SqlConnection(mDBConnection)
		Dim SqlCmd As New SqlCommand
		Dim Dset As New DataSet
		Dim Da As New SqlDataAdapter
		SqlCmd.Connection = Cn
		SqlCmd.CommandText = "FSEP_CARGAR_ARBOL_MATERIALES"
		SqlCmd.CommandType = CommandType.StoredProcedure
		Cn.Open()
		Da.SelectCommand = SqlCmd
		Da.Fill(Dset)
		Cn.Close()
		SqlCmd.Dispose()
		Da.Dispose()
		Return Dset
		Dset.Clear()
	End Function
#End Region
#Region " cXML data access methods "
	''' <summary>
	''' Función que ejectuta un Procedimiento almacenado en la Base de Datos, dado su nombre y sus parámetros en una lista de strings
	''' </summary>
	''' <param name="NombreStored">Nombre del Procedimiento almacenado que se quiere ejecutar</param>
	''' <param name="Parametros">Parámetros de entrada del procedimiento almacenado</param>
	''' <returns>Un objeto de tipo DataSet con el resultado de la consulta del Procedimiento almacenado</returns>
	''' <remarks>
	''' Llamada desde: La clase cXML metodo EjecutaStored
	''' Tiempo máximo: Variable, depende completamente del Procedimiento al que se llame.</remarks>
	Public Function cXML_EjecutaStored(ByVal NombreStored As String, ByVal Parametros() As FSNLibrary.TiposDeDatos.ParametrosOrigenDeDatos) As DataSet
		Authenticate()

		Dim SqlCon As New SqlConnection(mDBConnection)
		Dim DsetResul As New DataSet
		Dim SqlCmd As New SqlCommand
		Dim sqlParam As New SqlParameter
		Dim SqlDa As New SqlDataAdapter
		SqlCmd.CommandText = NombreStored
		SqlCmd.CommandType = CommandType.StoredProcedure
		If Not Parametros Is Nothing AndAlso Parametros.Count > 0 Then
			Dim ContParametros As Integer = 0
			For Each param As FSNLibrary.TiposDeDatos.ParametrosOrigenDeDatos In Parametros
				If Not param.ValorDato.ToUpper = "SYSTEM.DBNULL.VALUE" Then
					sqlParam = SqlCmd.Parameters.AddWithValue(param.Nombre, param.ValorDato)
				Else
					sqlParam = SqlCmd.Parameters.AddWithValue(param.Nombre, System.DBNull.Value)
				End If
				Select Case param.TipoDeDatos
					Case FSNLibrary.TiposDeDatos.TipoDeDatoXML.Booleano
						SqlCmd.Parameters(ContParametros).DbType = DbType.Boolean
					Case FSNLibrary.TiposDeDatos.TipoDeDatoXML.Entero
						SqlCmd.Parameters(ContParametros).DbType = DbType.Int32
					Case FSNLibrary.TiposDeDatos.TipoDeDatoXML.Fecha
						SqlCmd.Parameters(ContParametros).DbType = DbType.DateTime
					Case FSNLibrary.TiposDeDatos.TipoDeDatoXML.Real
						SqlCmd.Parameters(ContParametros).DbType = DbType.Double
					Case FSNLibrary.TiposDeDatos.TipoDeDatoXML.Texto
						SqlCmd.Parameters(ContParametros).DbType = DbType.String
					Case Else
						SqlCmd.Parameters(ContParametros).DbType = DbType.String
				End Select
				ContParametros = ContParametros + 1
			Next
			SqlCon.Open()
			SqlCmd.Connection = SqlCon
			SqlDa.SelectCommand = SqlCmd
			SqlDa.Fill(DsetResul)
		Else
			SqlCon.Open()
			SqlCmd.Connection = SqlCon
			SqlDa.SelectCommand = SqlCmd
			SqlDa.Fill(DsetResul)
		End If
		SqlCon.Close()
		SqlCmd.Dispose()
		SqlDa.Dispose()
		Return DsetResul
		DsetResul.Clear()
	End Function
#End Region
#Region " Adjunto data access methods "
	''' <summary>
	''' Funcion que devolvera un dataset con los adjuntos de la linea de pedido y de la orden
	''' </summary>
	''' <param name="idOrden">identificador de la orden</param> 
	''' <param name="idLinea">identificador de la linea de pedido</param>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Function Adjuntos_DevolverAdjuntosLineaYOrden(ByVal idOrden As Long, ByVal IdLinea As Long) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand()
		Dim ds As New DataSet
		Dim da As New SqlDataAdapter
		Using cn
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_DEVOLVER_ADJUNTOS"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@IDORDEN", idOrden)
			cm.Parameters.AddWithValue("@IDLINEA", IdLinea)
			da.SelectCommand = cm
			da.Fill(ds)
			Return ds
		End Using
	End Function
	''' <summary>
	''' Funcion que devolvera un dataset con los adjuntos de la linea de pedido y de la orden
	''' </summary>
	''' <param name="pedidoRecep">identificador de la recepciÃ³n del pedido</param>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Function Adjuntos_DevolverAdjuntosRecepcionPedido(ByVal pedidoRecep As Long) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand()
		Dim ds As New DataSet
		Dim da As New SqlDataAdapter
		Using cn
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_DEVOLVER_ADJUNTOS_PEDIDO_RECEP"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@PEDIDO_RECEP", pedidoRecep)
			da.SelectCommand = cm
			da.Fill(ds)
			Return ds
		End Using
	End Function

	''' <summary>
	''' Elimina un archivo adjunto de la Base de Datos
	''' </summary>
	''' <param name="id">Identificador en la Base de Datos del adjunto</param>
	''' <param name="tipo">Tipo de Adjunto a Eliminar; 2: Orden, 3: Linea Pedido, 4: Favorito Orden, 5: Favorito Linea</param>
	''' <returns>Un entero que sera 0 si todo ha ido bien, o 1 en caso contrario</returns>
	''' <remarks>
	''' Llamada desde: La clase CAdjuntos método Eliminar adjunto
	''' Tiempo máximo: 0'4 sec.
	''' </remarks>
	Public Function Adjunto_EliminarAdjunto(ByVal id As Integer, ByVal tipo As TiposDeDatos.Adjunto.Tipo) As Integer
		Authenticate()

		Dim sqlCmd As New SqlCommand
		Dim cn As New SqlConnection(mDBConnection)
		Dim sqlParam As New SqlParameter
		Dim SqlTrans As SqlTransaction
		sqlParam = sqlCmd.Parameters.AddWithValue("@ID", id)
		If tipo = TiposDeDatos.Adjunto.Tipo.Orden_Entrega Then
			sqlCmd.CommandText = "Delete From ORDEN_ENTREGA_ADJUN where id= @ID"
		ElseIf tipo = TiposDeDatos.Adjunto.Tipo.Linea_Pedido Then
			sqlCmd.CommandText = "Delete From LINEAS_PEDIDO_ADJUN where id= @ID"
		ElseIf tipo = TiposDeDatos.Adjunto.Tipo.Favorito_Orden_Entrega Then
			sqlCmd.CommandText = "Delete From Favoritos_Orden_Entrega_adjun where id= @ID"
		ElseIf tipo = TiposDeDatos.Adjunto.Tipo.Favorito_Linea_Pedido Then
			sqlCmd.CommandText = "Delete From FAVORITOS_LINEAS_PEDIDO_ADJUN where id = @ID"
		ElseIf tipo = TiposDeDatos.Adjunto.Tipo.Cesta_Cabecera Then
			sqlCmd.CommandText = "DELETE FROM CESTA_CABECERA_ADJUN WHERE ID= @ID"
		ElseIf tipo = TiposDeDatos.Adjunto.Tipo.Cesta Then
			sqlCmd.CommandText = "DELETE FROM CESTA_ADJUN WHERE ID= @ID"
		End If
		cn.Open()
		sqlCmd.Connection = cn
		sqlCmd.CommandType = CommandType.Text
		SqlTrans = cn.BeginTransaction
		sqlCmd.Transaction = SqlTrans
		Try
			sqlCmd.ExecuteNonQuery()
			sqlCmd.Transaction.Commit()
		Catch ex As Exception
			sqlCmd.Transaction.Rollback()
			Return 1
		Finally
			cn.Dispose()
			sqlCmd.Dispose()
		End Try
		Return 0
	End Function

	''' <summary>
	''' Actualiza el comentario escrito de un adjunto en la Base de Datos
	''' </summary>
	''' <param name="IdAdjunto">Identificador del adjunto en la Base de Datos</param>
	''' <param name="Tipo">Tipo de Adjunto a Cambiar el comentario; 2: Orden, 3: Linea Pedido, 4: Favorito Orden, 5: Favorito Linea</param>
	''' <param name="NuevoComentario">El nuevo comentario a insertar en la BBDD</param>
	''' <returns>Un entero que identificara si ha habido errores al actualizar</returns>
	''' <remarks>
	''' Llamada desde:La clase cAdjunto, metodo CambiarComentario
	''' Tiempo máximo: 0,1 seg</remarks>
	Public Function Adjunto_CambiarComentario(ByVal IdAdjunto As Integer, ByVal Tipo As TiposDeDatos.Adjunto.Tipo, ByVal NuevoComentario As String) As Integer
		Authenticate()

		Dim sqlCmd As New SqlCommand
		Dim cn As New SqlConnection(mDBConnection)
		Dim sqlParam As New SqlParameter
		Dim SqlTrans As SqlTransaction
		sqlParam = sqlCmd.Parameters.AddWithValue("@ID", IdAdjunto)
		sqlParam = sqlCmd.Parameters.AddWithValue("@NUEVOCOMENTARIO", NuevoComentario)
		If Tipo = TiposDeDatos.Adjunto.Tipo.Orden_Entrega Then
			sqlCmd.CommandText = "UPDATE ORDEN_ENTREGA_ADJUN SET COM=@NUEVOCOMENTARIO where id= @ID"
		ElseIf Tipo = TiposDeDatos.Adjunto.Tipo.Linea_Pedido Then
			sqlCmd.CommandText = "UPDATE LINEAS_PEDIDO_ADJUN SET COM=@NUEVOCOMENTARIO where id= @ID"
		ElseIf Tipo = TiposDeDatos.Adjunto.Tipo.Favorito_Orden_Entrega Then
			sqlCmd.CommandText = "UPDATE Favoritos_Orden_Entrega_adjun SET COM=@NUEVOCOMENTARIO where id= @ID"
		ElseIf Tipo = TiposDeDatos.Adjunto.Tipo.Favorito_Linea_Pedido Then
			sqlCmd.CommandText = "UPDATE FAVORITOS_LINEAS_PEDIDO_ADJUN SET COM=@NUEVOCOMENTARIO where id = @ID"
		ElseIf Tipo = TiposDeDatos.Adjunto.Tipo.Cesta_Cabecera Then
			sqlCmd.CommandText = "UPDATE CESTA_CABECERA_ADJUN SET COM=@NUEVOCOMENTARIO WHERE ID = @ID"
		ElseIf Tipo = TiposDeDatos.Adjunto.Tipo.Cesta Then
			sqlCmd.CommandText = "UPDATE CESTA_ADJUN SET COM=@NUEVOCOMENTARIO WHERE ID = @ID"
		End If
		cn.Open()
		sqlCmd.Connection = cn
		sqlCmd.CommandType = CommandType.Text
		SqlTrans = cn.BeginTransaction
		sqlCmd.Transaction = SqlTrans
		Try
			sqlCmd.ExecuteNonQuery()
			sqlCmd.Transaction.Commit()
		Catch ex As Exception
			sqlCmd.Transaction.Rollback()
			cn.Dispose()
			sqlCmd.Dispose()
			Return 1
		End Try
		Return 0
	End Function
	''' Revisado por: blp. Fecha: 28/02/2012
	''' <summary>
	''' Devuelve los comentarios de un adjunto
	''' </summary>
	''' <param name="idAdj">Id del adjunto</param>
	''' <param name="tipoAdjunto">Tipo de adjunto: en principio sólo desarrollamos para los dos que necesitamos: adjunto de orden y de línea</param>
	''' <returns>String con los Comentarios del adjunto</returns>
	''' <remarks>Llamada desde CAdjunto.vb->GetComentarios(). Máx 0,1 seg.</remarks>
	Public Function Adjunto_GetComentarios(ByVal idAdj As Integer, ByVal tipoAdjunto As String) As String
		Authenticate()

		Dim sCom As String = ""
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Dim sCommandText As String = String.Empty
		Select Case tipoAdjunto
			Case "LineaPedido"
				sCommandText = "EP_OBT_ADJLINEA_COMENT"
			Case "Orden"
				sCommandText = "EP_OBT_ADJORDEN_COMENT"
		End Select
		If sCommandText <> String.Empty Then
			Try
				cn.Open()
				cm.Connection = cn
				cm.CommandText = sCommandText
				cm.CommandType = CommandType.StoredProcedure
				cm.Parameters.AddWithValue("@IDADJ", idAdj)
				sCom = cm.ExecuteScalar()
			Catch ex As Exception
				Throw ex
			Finally
				cm.Dispose()
				cn.Close()
				cn.Dispose()
			End Try
			Return sCom
		Else
			cm.Dispose()
			cn.Dispose()
			Return String.Empty
		End If
	End Function
#End Region
#Region " Aprovisionadores data access methods"
	''' <summary>
	''' Función que devuelve un dataset con los aprovisionadores para el usuario actual
	''' </summary>
	''' <param name="Per">Codigo de la persona actual</param>
	''' <returns>Dataset con los aprovisionadores</returns>
	''' <remarks>
	''' Llamada desde: La función DevolverAprovisionadoresParaAprobador de User.vb
	''' Tiempo: 0 seg</remarks>
	Public Function Aprovisionadores_DevolverAprovisionadoresParaAprobador(ByVal Per As String) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand()
		Dim dr As New DataSet
		Dim da As New SqlDataAdapter
		Using cn
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_DEVOLVER_APROVISIONADORES"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@PER", Per)
			da.SelectCommand = cm
			da.Fill(dr)
			Return dr
		End Using
	End Function
#End Region
#Region " Categorias data access methods"
	''' <summary>
	''' Función que nos devuelve un dataset con la estructura completa de categorías para un aprobador
	''' </summary>
	''' <param name="Persona">aprobador</param>
	''' <returns>un dataset con 2 campos:
	'''     ID: Que contendrá un identificador único con el id de la categoría actual (separado por "_")
	'''     PADRE: Que contendrá el id del padre que lo contiene (separado por "_")
	''' </returns>
	''' <remarks>
	''' Llamada desde: EpServer/CCategorias/CargarArbolCategorias
	''' Tiempo máximo: 0,2 seg</remarks>
	Public Function Categorias_CargarArbolCategorias(ByVal Persona As String, Optional ByVal conBajas As Integer = 0) As DataSet
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet

		Using cn
			cn.Open()
			cm = New SqlCommand
			cm.Connection = cn
			cm.CommandText = "FSEP_CARGAR_ARBOL_CATEGORIAS"
			cm.CommandType = CommandType.StoredProcedure
			If Persona <> "" Then cm.Parameters.AddWithValue("@PER", Persona)
			cm.Parameters.AddWithValue("@CONBAJAS", conBajas)

			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)

			Return dr
		End Using
	End Function
	''' <summary>
	''' Función que nos devuelve un dataset con los datos de una categoria concreta
	''' </summary>
	''' <param name="CatN1">Categoria de nivel 1</param>
	''' <param name="CatN2">Categoria de nivel 2</param>
	''' <param name="CatN3">Categoria de nivel 3</param>
	''' <param name="CatN4">Categoria de nivel 4</param>
	''' <param name="CatN5">Categoria de nivel 5</param>
	''' <param name="Nivel">Nivel al que pertenece la categoria</param>
	''' <returns>Dataset con los datos de una categoria concreta
	''' </returns>
	''' <remarks>
	''' llamada desde: CCategorias.CargarDetalleCategoria
	''' Tiempo máxima inferior 1seg.
	''' </remarks>
	Public Function Categorias_CargarDetalleCategoria(ByVal CatN1 As Object, ByVal CatN2 As Object, ByVal CatN3 As Object, ByVal CatN4 As Object, ByVal CatN5 As Object, ByVal Nivel As Integer) As DataSet
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet

		Using cn
			cn.Open()
			cm = New SqlCommand
			cm.Connection = cn
			cm.CommandText = "FSEP_CARGAR_CATEGORIA"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@CAT1", CatN1)
			cm.Parameters.AddWithValue("@CAT2", CatN2)
			cm.Parameters.AddWithValue("@CAT3", CatN3)
			cm.Parameters.AddWithValue("@CAT4", CatN4)
			cm.Parameters.AddWithValue("@CAT5", CatN5)
			cm.Parameters.AddWithValue("@NIVEL", Nivel)

			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)

			Return dr
		End Using
	End Function
	''' <summary>
	''' Devuelve un dataset con los adjuntos para la Categoria seleccionada
	''' </summary>
	''' <param name="CatN1">id de la Categoria de Nivel 1</param>
	''' <param name="CatN2">id de la Categoria de Nivel 2</param>
	''' <param name="CatN3">id de la Categoria de Nivel 3</param>
	''' <param name="CatN4">id de la Categoria de Nivel 4</param>
	''' <param name="CatN5">id de la Categoria de Nivel 5</param>
	''' <param name="Nivel">Nivel de la Categoria</param>
	''' <returns>El dataset de adjuntos de la categoria seleccionada</returns>
	''' <remarks>
	''' Llamada desde: CargarAdjuntos de CCategorias
	''' Tiempo Máximo: 0 sec
	''' </remarks>
	Public Function Categorias_CargarAdjuntosCategoria(ByVal CatN1 As Object, ByVal CatN2 As Object, ByVal CatN3 As Object, ByVal CatN4 As Object, ByVal CatN5 As Object, ByVal Nivel As Integer) As DataSet
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet

		Using cn
			cn.Open()
			cm = New SqlCommand
			cm.Connection = cn
			cm.CommandText = "FSEP_DEVOLVER_ADJUNTOS_CATEGORIA"
			cm.CommandType = CommandType.StoredProcedure

			cm.Parameters.AddWithValue("@CAT1", CatN1)
			cm.Parameters.AddWithValue("@CAT2", CatN2)
			cm.Parameters.AddWithValue("@CAT3", CatN3)
			cm.Parameters.AddWithValue("@CAT4", CatN4)
			cm.Parameters.AddWithValue("@CAT5", CatN5)
			cm.Parameters.AddWithValue("@NIVEL", Nivel)

			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)

			Return dr
		End Using
	End Function
#End Region
#Region " Contactos data access methods "
	''' <summary>
	''' Carga los datos del contacto principal del proveedor seleccionado
	''' </summary>
	''' <param name="Prove">código del proveedor seleccionado</param>
	''' <returns>Un dataset con una tabla con los datos del contacto</returns>
	''' <remarks>
	''' Llamada desde: PedidoLibre.aspx
	''' Tiempo máximo: 0 sec.
	''' </remarks>
	Public Function Contactos_CargarDatosContacto(ByVal Prove As String) As DataSet

		Dim cn As New SqlConnection(mDBConnection)

		Dim cm As New SqlCommand
		Dim dr As New DataSet
		Using cn

			If Not cn.State = ConnectionState.Open Then
				cn.Open()
			End If

			cm.Connection = cn

			'Obtiene un contacto del proveedor que esté marcado en el GS como aprovisionador
			'Si no hay ningún aprovisionador saca cualquier contacto.
			cm.CommandText = "SELECT TOP 1 NOM + ' ' + APE CONTACTO,DEP,CAR,TFNO,TFNO2,TFNO_MOVIL,FAX,EMAIL,PROVE.COD PROVECOD,PROVE.DEN PROVEDEN" &
						" FROM CON WITH (NOLOCK)" &
						" INNER JOIN PROVE WITH (NOLOCK) ON PROVE.COD = CON.PROVE " &
						" WHERE CON.PROVE='" & DblQuote(Prove) & "' " &
						" ORDER BY APROV DESC ,ID"

			cm.CommandType = CommandType.Text
			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)

			Return dr
		End Using
	End Function
#End Region
#Region "Destinos data access methods"
	''' <summary>
	''' Función que devuelve un dataset con los datos del destino aprovisionadores para el usuario actual
	''' </summary>
	''' <param name="sIdi">Codigo del idioma</param>
	''' <param name="Destino">Codigo del destino</param>
	''' <returns>un dataset con los datos del destino aprovisionadores para el usuario actual</returns>
	''' <remarks>
	''' Llamada desde: La función CargarDatosDestino de Destino.vb
	''' Tiempo: 0 seg</remarks>
	Public Function Destinos_CargarDatosDestino(ByVal sIdi As String, ByVal Destino As String, Optional ByVal lCodLinea As Long = 0) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand()
		Dim dr As New DataSet
		Dim da As New SqlDataAdapter
		Using cn
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "SP_DEVOLVER_DESTINOS"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@COD", Destino)
			cm.Parameters.AddWithValue("@IDI", sIdi)
			cm.Parameters.AddWithValue("@IDLINEA", lCodLinea)

			da.SelectCommand = cm
			da.Fill(dr)
			Return dr
		End Using
	End Function
#End Region
#Region " Linea data access methods"
	''' <summary>
	''' Devuelve un dataset con los adjuntos para la línea de Pedido
	''' </summary>
	''' <param name="Linea">Id de la línea de pedido</param>
	''' <returns>El dataset antes comentado</returns>
	''' <remarks>
	''' Llamada desde: CargarAdjuntos de CLinea
	''' Tiempo Máximo: 0 sec
	''' </remarks>
	Public Function Linea_CargarAdjuntos(ByVal Linea As Integer) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet
		Using cn
			cn.Open()
			cm = New SqlCommand
			cm.Connection = cn
			cm.CommandText = "FSEP_DEVOLVER_ADJUNTOS_LINEAS_PEDIDO"
			cm.CommandType = CommandType.StoredProcedure

			cm.Parameters.AddWithValue("@LINEA", Linea)

			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)

			Return dr
		End Using
	End Function
	''' <summary>
	''' Función que devolverá un dataset con las facturas asociadas a una línea de pedido. Si no se recibe línea de pedido devuelve todas las líneas de pedido y sus facturas asociadas.
	''' </summary>
	''' <param name="idioma">Idioma en el que se quieren recuperar los datos en multiidioma</param>
	''' <param name="IdLinea">Id de la línea de pedido</param>
	''' <returns>Dataset con las facturas asociadas a una línea de pedido. Si no se recibe línea de pedido devuelve todas las líneas de pedido y sus facturas asociadas.</returns>
	''' <remarks></remarks>
	Public Function Linea_DevolverFacturas(ByVal idioma As FSNLibrary.Idioma, Optional ByVal IdLinea As Integer = 0) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet
		Using cn
			cn.Open()

			cm = New SqlCommand
			cm.Connection = cn

			cm.CommandText = "FSEP_GET_FACTURAS"
			cm.CommandType = CommandType.StoredProcedure

			If Not IsNothing(idioma) And idioma.ToString <> "" Then
				cm.Parameters.AddWithValue("IDIOMA", idioma.ToString)
			Else
				cm.Parameters.AddWithValue("IDIOMA", "SPA")
			End If
			If Not IsNothing(IdLinea) And IdLinea <> 0 Then
				cm.Parameters.AddWithValue("LINEA_PEDIDO", IdLinea)
			Else
				cm.Parameters.AddWithValue("LINEA_PEDIDO", DBNull.Value)
			End If

			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)

			Return dr
		End Using
	End Function
	''' <summary>
	''' Función que devolverá un dataset con los pagos asociados a facturas aociadas a su vez a una línea de pedido. Si no se recibe línea de pedido devuelve todas las líneas de pedido y sus pagos.
	''' </summary>
	''' <param name="idioma">Idioma en el que se quieren recuperar los datos en multiidioma</param>
	''' <param name="IdLinea">Integer. Id de la línea de pedido</param>
	''' <returns>Dataset con los pagos asociados a facturas aociadas a su vez a una línea de pedido. 
	''' Si no se recibe línea de pedido devuelve todas las líneas de pedido y sus pagos.</returns>
	''' <remarks></remarks>
	Public Function Linea_DevolverPagos(ByVal idioma As FSNLibrary.Idioma, Optional ByVal IdLinea As Integer = 0, Optional ByVal lIdFactura As Long = 0) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet
		Using cn
			cn.Open()

			cm = New SqlCommand
			cm.Connection = cn

			cm.CommandText = "FSEP_GET_PAGOS"
			cm.CommandType = CommandType.StoredProcedure

			If Not IsNothing(idioma) And idioma.ToString <> "" Then
				cm.Parameters.AddWithValue("IDIOMA", idioma.ToString)
			Else
				cm.Parameters.AddWithValue("IDIOMA", "SPA")
			End If

			If Not IsNothing(IdLinea) And IdLinea <> 0 Then
				cm.Parameters.AddWithValue("LINEA_PEDIDO", IdLinea)
			Else
				cm.Parameters.AddWithValue("LINEA_PEDIDO", DBNull.Value)
			End If

			If Not IsNothing(lIdFactura) And lIdFactura <> 0 Then
				cm.Parameters.AddWithValue("FACTURA", lIdFactura)
			Else
				cm.Parameters.AddWithValue("FACTURA", DBNull.Value)
			End If

			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)

			Return dr
		End Using
	End Function
	''' <summary>
	''' Función que devolverá un dataset con las Recepciones asociadas a una línea de pedido. Si no se recibe línea de pedido devuelve todas las líneas de pedido y sus Recepciones.
	''' </summary>
	''' <param name="IdLinea">Integer. Id de la línea de pedido</param>
	''' <param name="FSFAActivado">Informa del estado activado o no del módulo de Facturas. En caso de estar activado se devolverán las recepciones no vinculadas a facturas con estado no anulado</param>
	''' <returns>Dataset con las Recepciones asociadas a una línea de pedido. 
	''' Si no se recibe línea de pedido devuelve todas las líneas de pedido y sus Recepciones.</returns>
	''' <remarks></remarks>
	Public Function Linea_DevolverRecepciones(Optional ByVal IdLinea As Integer = 0, Optional ByVal FSFAActivado As Boolean = False, Optional ByVal sIdioma As String = "SPA") As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet

		Using cn
			cn.Open()

			cm = New SqlCommand
			cm.Connection = cn

			cm.CommandText = "FSEP_GET_RECEPCIONES"
			cm.CommandType = CommandType.StoredProcedure

			If Not IsNothing(IdLinea) And IdLinea <> 0 Then
				cm.Parameters.AddWithValue("@LINEA_PED", IdLinea)
			Else
				cm.Parameters.AddWithValue("@LINEA_PED", DBNull.Value)
			End If

			If Not IsNothing(FSFAActivado) Then
				cm.Parameters.AddWithValue("@FSFA_ACTIVADO", BooleanToSQLBinary(FSFAActivado))
			Else
				cm.Parameters.AddWithValue("@FSFA_ACTIVADO", BooleanToSQLBinary(False))
			End If

			cm.Parameters.AddWithValue("@IDIOMA", sIdioma)

			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)

			Return dr
		End Using
	End Function
	''' <summary>
	''' Actualiza los datos de una entrada de la cesta en la BBDD
	''' </summary>
	''' <param name="CestaId">ID de la línea de la cesta</param>
	''' <param name="Cantidad">Cantidad</param>
	''' <param name="UnidadPedido">Unidad de pedido</param>
	''' <param name="FactorConversion">Factor de conversión de la unidad de pedido</param>
	''' <param name="Precio">Precio de la unidad de compra</param>
	''' <param name="Destino">Destino</param>
	''' <param name="FechaEntrega">Fecha de entrega</param>
	''' <param name="FechaObligatoria">Indica si la fecha de entrega es obligatoria</param>
	''' <param name="Observaciones">Observaciones</param>
	''' <param name="ValorCampo1">Valor del 1º campo de información adicional</param>
	''' <param name="ValorCampo2">Valor del 2º campo de información adicional</param>
	''' <param name="Almacen">ID del almacén</param>
	''' <param name="Activo">ID de activo</param>
	''' <param name="CentroSM">Código de centro de coste</param>
	''' <param name="PartidaPres5">ID de la partida presupuestaria</param>
	''' <param name="Especificaciones">Especificaciones para el proveedor</param>
	''' <remarks>revisado por mmv(22/11/2011)</remarks>
	Public Sub Linea_ActualizarCesta(ByVal iDesdeFavorito As Integer, ByVal CestaId As Integer, ByVal Cantidad As Double, ByVal UnidadPedido As String, ByVal FactorConversion As Double, ByVal Precio As Double,
								ByVal Destino As String, ByVal FechaEntrega As DateTime, ByVal FechaObligatoria As Boolean, ByVal Observaciones As String,
								ByVal Campo1 As Long, ByVal Campo2 As Long, ByVal ValorCampo1 As String, ByVal ValorCampo2 As String, ByVal Almacen As Integer, ByVal Activo As Integer,
								ByVal CentroSM As String, ByVal PartidaPres5 As Integer, ByVal Especificaciones As String, ByVal Descripcion As String, ByVal iTipoRecepcion As Integer,
								ByVal ImportePed As Double, ByVal UsuCod As String, ByVal NumLinea As Integer, ByVal CentroAprovisionamiento As String,
								ByVal ConRecepcionAutomatica As Boolean, ByVal ConPlanEntrega As Boolean, Optional ByVal DesvioRecepcion As Double = 0)
		Dim cm As New SqlCommand
		Dim cn As New SqlConnection(mDBConnection)
		Using cn
			cm.Connection = cn

			If iDesdeFavorito = 0 Then
				cm.CommandText = "FSEP_ACTUALIZAR_CESTAEP"
			Else
				cm.CommandText = "FSEP_ACTUALIZAR_CESTAEP_FAV"
			End If
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.Add("@UNIDAD", SqlDbType.VarChar, 20).Value = UnidadPedido
			cm.Parameters.Add("@FACTORCONVERSION", SqlDbType.Float).Value = FactorConversion
			cm.Parameters.Add("@DESTINO", SqlDbType.VarChar, 20).Value = IIf(String.IsNullOrEmpty(Destino), DBNull.Value, Destino)
			cm.Parameters.Add("@FECHA", SqlDbType.DateTime).Value = IIf(FechaEntrega = Date.MinValue, DBNull.Value, FechaEntrega)
			cm.Parameters.Add("@OBLIGATORIA", SqlDbType.Bit).Value = IIf(FechaObligatoria, 1, 0)
			cm.Parameters.Add("@OBSERVACIONES", SqlDbType.VarChar, 2000).Value = IIf(String.IsNullOrEmpty(Observaciones), DBNull.Value, Observaciones)
			cm.Parameters.Add("@CAMPO1", SqlDbType.Int).Value = IIf(Campo1 = 0, DBNull.Value, Campo1)
			cm.Parameters.Add("@CAMPO2", SqlDbType.Int).Value = IIf(Campo2 = 0, DBNull.Value, Campo2)
			cm.Parameters.Add("@VALOR1", SqlDbType.VarChar, 100).Value = IIf(String.IsNullOrEmpty(ValorCampo1), DBNull.Value, ValorCampo1)
			cm.Parameters.Add("@VALOR2", SqlDbType.VarChar, 100).Value = IIf(String.IsNullOrEmpty(ValorCampo2), DBNull.Value, ValorCampo2)
			cm.Parameters.Add("@ALMACEN", SqlDbType.Int).Value = IIf(Almacen = 0, DBNull.Value, Almacen)
			cm.Parameters.Add("@ACTIVO", SqlDbType.Int).Value = IIf(Activo = 0, DBNull.Value, Activo)
			cm.Parameters.Add("@OBSADJUN", SqlDbType.NVarChar, 2000).Value = IIf(String.IsNullOrEmpty(Especificaciones), DBNull.Value, Especificaciones)
			cm.Parameters.Add("@DESCR", SqlDbType.VarChar, 200).Value = Descripcion 'añadiendo descripcion cuando no es generico tambien me lo carga
			cm.Parameters.Add("@CESTAID", SqlDbType.Int).Value = CestaId
			If iTipoRecepcion = 0 Then
				cm.Parameters.Add("@CANTIDAD", SqlDbType.Float).Value = Cantidad
				cm.Parameters.Add("@PRECIO", SqlDbType.Float).Value = Precio
				cm.Parameters.Add("@IMPORTE_PED", SqlDbType.Float).Value = DBNull.Value

			Else
				cm.Parameters.Add("@CANTIDAD", SqlDbType.Float).Value = DBNull.Value
				cm.Parameters.Add("@PRECIO", SqlDbType.Float).Value = DBNull.Value
				cm.Parameters.Add("@IMPORTE_PED", SqlDbType.Float).Value = ImportePed
			End If
			If iDesdeFavorito = 1 Then
				cm.Parameters.Add("@USU", SqlDbType.VarChar, 20).Value = UsuCod
			End If
			'Nº linea
			cm.Parameters.Add("@NUM", SqlDbType.Int).Value = NumLinea
			cm.Parameters.Add("@CENTROAPROV", SqlDbType.VarChar, 50).Value = IIf(String.IsNullOrEmpty(CentroAprovisionamiento), DBNull.Value, CentroAprovisionamiento)
			If iDesdeFavorito = 0 Then cm.Parameters.AddWithValue("@CONRECEPCIONAUTOMATICA", ConRecepcionAutomatica)
			If iDesdeFavorito = 0 Then cm.Parameters.AddWithValue("@CONPLANENTREGA", ConPlanEntrega)
			If iDesdeFavorito = 0 Then cm.Parameters.Add("@DESVIO_RECEPCION", SqlDbType.Float).Value = DesvioRecepcion
			'cm.CommandType = CommandType.Text
			cn.Open()
			cm.ExecuteNonQuery()
			cm.CommandType = CommandType.Text
			If iDesdeFavorito = 0 Then
				If Not String.IsNullOrEmpty(CentroSM) Then
					cm.Parameters.Clear()
					' No se utiliza WITH (NOLOCK) puesto que se consulta la línea que se va a actualizar
					cm.CommandText = "IF EXISTS(SELECT ID FROM CESTA_IMPUTACION WITH(NOLOCK) WHERE CESTA=@CESTAID AND ID=1) " &
						"UPDATE CESTA_IMPUTACION SET CENTRO_SM=@CENTROSM, PRES5_IMP=@PARTIDA WHERE CESTA=@CESTAID AND ID=1 " &
						"ELSE " &
						"INSERT INTO CESTA_IMPUTACION (CESTA, ID, CENTRO_SM, PRES5_IMP) VALUES (@CESTAID, 1, @CENTROSM, @PARTIDA)"
					cm.Parameters.Add("@CESTAID", SqlDbType.Int).Value = CestaId
					cm.Parameters.Add("@CENTROSM", SqlDbType.VarChar, 50).Value = CentroSM
					cm.Parameters.Add("@PARTIDA", SqlDbType.Int).Value = IIf(PartidaPres5 = 0, DBNull.Value, PartidaPres5)
					cm.ExecuteNonQuery()
				Else
					cm.Parameters.Clear()
					' No se utiliza WITH (NOLOCK) puesto que se consulta la línea que se va a actualizar
					cm.CommandText = "IF EXISTS(SELECT ID FROM CESTA_IMPUTACION WITH(NOLOCK) WHERE CESTA=@CESTAID AND ID=1) " &
						"DELETE FROM CESTA_IMPUTACION WHERE CESTA=@CESTAID AND ID=1 "
					cm.Parameters.Add("@CESTAID", SqlDbType.Int).Value = CestaId
					cm.ExecuteNonQuery()
				End If
			Else
				If Not String.IsNullOrEmpty(CentroSM) Then
					cm.Parameters.Clear()
					' No se utiliza WITH (NOLOCK) puesto que se consulta la línea que se va a actualizar
					cm.CommandText = "IF EXISTS(SELECT ID FROM FAVORITOS_IMPUTACION WITH(NOLOCK) WHERE FAVORITO=@CESTAID AND ID=1) " &
						"UPDATE FAVORITOS_IMPUTACION SET CENTRO_SM=@CENTROSM, PRES5_IMP=@PARTIDA WHERE FAVORITO=@CESTAID AND ID=1 " &
						"ELSE " &
						"INSERT INTO FAVORITOS_IMPUTACION (FAVORITO, USU, ID, CENTRO_SM, PRES5_IMP) VALUES (@CESTAID, @USU, 1, @CENTROSM, @PARTIDA)"
					cm.Parameters.Add("@CESTAID", SqlDbType.Int).Value = CestaId
					cm.Parameters.Add("@CENTROSM", SqlDbType.VarChar, 50).Value = CentroSM
					cm.Parameters.Add("@USU", SqlDbType.VarChar, 20).Value = UsuCod
					cm.Parameters.Add("@PARTIDA", SqlDbType.Int).Value = IIf(PartidaPres5 = 0, DBNull.Value, PartidaPres5)
					cm.ExecuteNonQuery()
				Else
					cm.Parameters.Clear()
					' No se utiliza WITH (NOLOCK) puesto que se consulta la línea que se va a actualizar
					cm.CommandText = "IF EXISTS(SELECT ID FROM FAVORITOS_IMPUTACION WITH(NOLOCK) WHERE FAVORITO=@CESTAID AND ID=1) " &
						"DELETE FROM FAVORITOS_IMPUTACION WHERE FAVORITO=@CESTAID AND ID=1 "
					cm.Parameters.Add("@CESTAID", SqlDbType.Int).Value = CestaId
					cm.ExecuteNonQuery()
				End If
			End If
		End Using
	End Sub
	''' <summary>
	''' Actualiza los datos de una entrada de la cesta en la BBDD
	''' </summary>
	''' <param name="CestaId">ID de la línea de la cesta</param>
	''' <param name="Cantidad">Cantidad</param>
	''' <param name="UnidadPedido">Unidad de pedido</param>
	''' <param name="Precio">Precio de la unidad de compra</param>
	Public Sub CestaLinea_ActualizarCestaPedido(ByVal CestaId As Integer, ByVal Cantidad As Double, ByVal UnidadPedido As String, ByVal FactorConversion As Double, ByVal Precio As Double, ByVal TipoRecepcion As Integer, ByVal ImportePed As Double)
		Dim cm As New SqlCommand
		Dim cn As New SqlConnection(mDBConnection)
		Using cn
			cm.Connection = cn
			cm.CommandText = "FSEP_ACTUALIZAR_LINEA_CESTAEP"
			cm.CommandType = CommandType.StoredProcedure
			Select Case TipoRecepcion
				Case 0, 3
					cm.Parameters.Add("@CANTIDAD", SqlDbType.Float).Value = Cantidad
					cm.Parameters.Add("@PRECIO", SqlDbType.Float).Value = Precio
					cm.Parameters.Add("@IMPORTE_PED", SqlDbType.Float).Value = DBNull.Value
				Case Else
					cm.Parameters.Add("@CANTIDAD", SqlDbType.Float).Value = DBNull.Value
					cm.Parameters.Add("@PRECIO", SqlDbType.Float).Value = DBNull.Value
					cm.Parameters.Add("@IMPORTE_PED", SqlDbType.Float).Value = ImportePed
			End Select

			cm.Parameters.Add("@UNIDAD", SqlDbType.VarChar, 20).Value = UnidadPedido
			cm.Parameters.Add("@CESTAID", SqlDbType.Int).Value = CestaId
			cm.Parameters.Add("@FC", SqlDbType.Float).Value = FactorConversion
			cn.Open()
			cm.ExecuteNonQuery()
		End Using
	End Sub
	''' Revisado por: blp. Fecha: 28/02/2012
	''' <summary>
	''' Devuelve las observaciones de una línea
	''' </summary>
	''' <param name="idLinea">Id de la línea cuyas observaciones se desean</param>
	''' <returns>String Observaciones</returns>
	''' <remarks>Llamada desde CLinea.vb->GetObservaciones(). Máx 0,1 seg.</remarks>
	Public Function Linea_GetObservaciones(ByVal idLinea As Integer) As String
		Authenticate()

		Dim sObs As String = ""
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "EP_OBT_LINEA_OBS"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@IDLINEA", idLinea)
			sObs = cm.ExecuteScalar()
		Catch ex As Exception
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
		Return sObs
	End Function
	''' Revisado: blp. Fecha:20/05/2013
	''' <summary>
	''' Devuelve el centro de coste de una línea de pedido
	''' </summary>
	''' <param name="IdLinea">Id de la línea</param>
	''' <param name="Idioma">Idioma</param>
	''' <returns>Centro de coste. Si no lo hay, devuelve NULL</returns>
	''' <remarks>Llamada desde CLinea.vb-->DevolverCentro. Maximo 0,1 seg.</remarks>
	Public Function Linea_DevolverCentro(ByVal IdLinea As Integer, ByVal Idioma As FSNLibrary.Idioma) As DataSet
		Authenticate()

		Dim dsRdo As New DataSet
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Dim da As New SqlDataAdapter
		Using cn
			Try
				cn.Open()
				cm.Connection = cn
				cm.CommandType = CommandType.StoredProcedure
				cm.CommandText = "EP_OBT_LINEAPEDIDO_CENTROCOSTE"
				cm.Parameters.Add("@LINEAID", SqlDbType.Int)
				cm.Parameters("@LINEAID").Value = IdLinea
				cm.Parameters.AddWithValue("IDIOMA", Idioma.ToString)
				da.SelectCommand = cm
				da.Fill(dsRdo)
				Return dsRdo
			Catch ex As Exception
				Throw ex
			Finally
				cn.Close()
				cn.Dispose()
				cm.Dispose()
			End Try
		End Using
	End Function
	''' Revisado: blp. Fecha:23/05/2013
	''' <summary>
	''' Devuelve la denominación del proveedor, el id de la orden y el código de la moneda que corresponden a la línea de pedido pasada como parámetro
	''' </summary>
	''' <param name="IdLinea">Id de la línea</param>
	''' <returns>datarow con los tres datos</returns>
	''' <remarks>Llamada desde CLinea.vb-->DevolverProveOrdenMon. Maximo 0,1 seg.</remarks>
	Public Function Linea_DevolverProveOrdenMon(ByVal IdLinea As Integer) As DataRow
		Authenticate()

		Dim dsRdo As New DataSet
		Dim drRdo As DataRow = Nothing
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Dim da As New SqlDataAdapter
		Using cn
			Try
				cn.Open()
				cm.Connection = cn
				cm.CommandType = CommandType.StoredProcedure
				cm.CommandText = "EP_OBT_LINEA_DETALLE"
				cm.Parameters.Add("@LINEAID", SqlDbType.Int)
				cm.Parameters("@LINEAID").Value = IdLinea
				da.SelectCommand = cm
				da.Fill(dsRdo)
				If dsRdo IsNot Nothing AndAlso dsRdo.Tables(0) IsNot Nothing AndAlso dsRdo.Tables(0).Rows.Count > 0 Then
					drRdo = dsRdo.Tables(0).Rows(0)
				End If
				Return drRdo
			Catch ex As Exception
				Throw ex
			Finally
				cn.Close()
				cn.Dispose()
				cm.Dispose()
			End Try
		End Using
	End Function
#End Region
#Region " Linea Favoritos Data access methods "
	''' <summary>
	''' Devuelve en un DataSet los datos de los archivos adjuntos de una linea de pedido favorito
	''' </summary>
	''' <param name="Id">Identificador de la línea de pedido favorito</param>
	''' <param name="Usu">Código de Usuario de la aplicación</param>
	''' <returns>Un DataSet con los datos de los archivos adjuntos de la línea de pedido</returns>
	''' <remarks>
	''' Llamada desde: La clase cLineaFavoritos, método CargarAdjuntos
	''' Tiempo máximo:0,27 seg</remarks>
	Public Function LineaFavoritos_CargarAdjuntos(ByVal Id As Integer, ByVal Usu As String) As DataSet
		Authenticate()

		Dim ador As New DataSet
		Dim adoCom As New SqlCommand
		Dim oParam As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)

		'Ejecuta el store procedure FSEP_DEVOLVER_ADJUNTOS_LINEAS_PEDIDO_FAVORITOS
		adoCom.Connection = cn
		adoCom.CommandText = "FSEP_DEVOLVER_ADJUNTOS_LINEAS_PEDIDO_FAVORITOS" ' @LINEA = ?,@USU=?"
		adoCom.CommandType = CommandType.StoredProcedure
		oParam = adoCom.Parameters.AddWithValue("@LINEA", Id)
		oParam = adoCom.Parameters.AddWithValue("@USU", Usu)
		cn.Open()
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(ador)
		cn.Close()
		Return ador
		da.Dispose()
		adoCom.Dispose()
	End Function
	''' <summary>
	''' Función que elimina una línea de pedidos favoritos, eliminando también sus adjuntos, asi como reduciendo el importe a la orden correspondiente.
	''' </summary>
	''' <param name="IdLinea">Identificador de la línea a eliminar</param>
	''' <param name="IdOrden">Identificador de la orden a la que pertenece la línea</param>
	''' <param name="Usuario">Código de usuario de la aplicación</param>
	''' <returns>Un valor entero que valdra 1 si ha habido algún error, y 0 si todo ha ido bien</returns>
	''' <remarks>
	''' Llamada desde: La clase cLineaFavoritos, método EliminarFavorito
	''' Tiempo máximo: 0,41 seg</remarks>
	Public Function LineaFavoritos_EliminarFavorito(ByVal IdLinea As Integer, ByVal IdOrden As Integer, ByVal Usuario As String) As Integer
		Authenticate()

		Dim sConsulta As String
		Dim sConsulta2 As String
		Dim sqlcmd As New SqlCommand
		Dim SqlParam As New SqlParameter
		Dim TesError As Integer = 0
		Dim cn As New SqlConnection(mDBConnection)
		Dim sqlTrans As SqlTransaction
		cn.Open()
		sqlcmd.Connection = cn
		sqlTrans = cn.BeginTransaction
		sqlcmd.CommandText = "SET XACT_ABORT OFF"
		sqlcmd.CommandType = CommandType.Text
		sqlcmd.Transaction = sqlTrans
		sqlcmd.ExecuteNonQuery()

		'Actualiza el importe de la orden:
		sqlcmd = New SqlCommand
		SqlParam = sqlcmd.Parameters.AddWithValue("@USUARIO", Usuario)
		SqlParam = sqlcmd.Parameters.AddWithValue("@IDORDEN", IdOrden)
		SqlParam = sqlcmd.Parameters.AddWithValue("@IDLINEA", IdLinea)
		sConsulta2 = "SELECT case when TIPORECEPCION = 0 THEN (PREC_UP * CANT_PED) ELSE  IMPORTE_PED END IMPLINEA FROM FAVORITOS_LINEAS_PEDIDO WITH(NOLOCK) WHERE USU=@USUARIO AND ID=@IDLINEA"
		sConsulta = "UPDATE FAVORITOS_ORDEN_ENTREGA SET IMPORTE=IMPORTE - (" & sConsulta2 & ")"
		sConsulta = sConsulta & " WHERE USU=@USUARIO AND ID=@IDORDEN"
		sqlcmd.Connection = cn
		sqlcmd.Transaction = sqlTrans
		sqlcmd.CommandText = sConsulta
		sqlcmd.CommandType = CommandType.Text
		Try
			sqlcmd.ExecuteNonQuery()
		Catch ex As Exception
			TesError = 1
			sqlTrans.Rollback()
			cn.Close()
			GoTo fin
		End Try

		'Elimina las imputaciones de la línea
		SqlParam = New SqlParameter
		sqlcmd = New SqlCommand
		sqlcmd.Connection = cn
		SqlParam = sqlcmd.Parameters.AddWithValue("@USUARIO", Usuario)
		SqlParam = sqlcmd.Parameters.AddWithValue("@IDLINEA", IdLinea)
		sConsulta = "DELETE FROM FAVORITOS_IMPUTACION WHERE USU=@USUARIO"
		sConsulta = sConsulta & " AND FAVORITO =@IDLINEA"
		sqlcmd.Transaction = sqlTrans
		sqlcmd.CommandText = sConsulta
		sqlcmd.CommandType = CommandType.Text
		Try
			sqlcmd.ExecuteNonQuery()
		Catch ex As Exception
			TesError = 1
			sqlTrans.Rollback()
			cn.Close()
			GoTo fin
		End Try

		'Elimina los adjuntos de la línea
		SqlParam = New SqlParameter
		sqlcmd = New SqlCommand
		sqlcmd.Connection = cn
		SqlParam = sqlcmd.Parameters.AddWithValue("@USUARIO", Usuario)
		SqlParam = sqlcmd.Parameters.AddWithValue("@IDLINEA", IdLinea)
		sConsulta = "DELETE FROM FAVORITOS_LINEAS_PEDIDO_ADJUN WHERE USU=@USUARIO"
		sConsulta = sConsulta & " AND LINEA =@IDLINEA"
		sqlcmd.Transaction = sqlTrans
		sqlcmd.CommandText = sConsulta
		sqlcmd.CommandType = CommandType.Text
		Try
			sqlcmd.ExecuteNonQuery()
		Catch ex As Exception
			TesError = 1
			sqlTrans.Rollback()
			cn.Close()
			GoTo fin
		End Try

		'Elimina la línea
		SqlParam = New SqlParameter
		sqlcmd = New SqlCommand
		sqlcmd.Connection = cn
		sqlcmd.Transaction = sqlTrans
		SqlParam = sqlcmd.Parameters.AddWithValue("@USUARIO", Usuario)
		SqlParam = sqlcmd.Parameters.AddWithValue("@IDLINEA", IdLinea)
		sConsulta = "DELETE FROM FAVORITOS_LINEAS_PEDIDO WHERE USU=@USUARIO AND ID=@IDLINEA"
		sqlcmd.CommandText = sConsulta
		sqlcmd.CommandType = CommandType.Text
		Try
			sqlcmd.ExecuteNonQuery()
			sqlTrans.Commit()
			cn.Close()
			Return TesError
		Catch ex As Exception
			TesError = 1
			sqlTrans.Rollback()
			cn.Close()
			GoTo fin
		End Try

fin:
		TesError = 1
		Exit Function
	End Function
	''' <summary>
	''' Función que actualiza una línea de favoritos de la Base de Datos
	''' </summary>
	''' <param name="Id">Identificador de la línea a actualizar</param>
	''' <param name="Dest">Nuevo destino</param>
	''' <param name="Precio">Nuevo precio</param>
	''' <param name="Cant">Nueva cantidad</param>
	''' <param name="Obs">Nuevas Observaciones</param>
	''' <param name="Campo1Valor">Nuevo valor para el campo1</param>
	''' <param name="Campo2Valor">Nuevo valor para el campo2</param>
	''' <param name="USU">Usuario de la aplicación</param>
	''' <param name="FechaEntrega">Fecha de entrega solicitada para la línea</param>
	''' <param name="EntregaObl">Fecha de entrega obligatoria</param>
	''' <param name="IDAlmacen">ID del almacén de la línea</param>
	''' <param name="Activo">Activo de la línea</param>
	''' <param name="CentroSM">Centro de Coste al que se ha asignado la línea</param>
	''' <param name="PartidaPres">Partida Presupuestaria a la que se ha asignado la línea</param>
	''' <returns>Un valor entero que será 0 si se ha ejecutado correctamente y 1 en caso contrario</returns>
	''' <remarks>
	''' Llamada desde: EPServer\CLineaFavoritos\ActualizarLineaFavoritos.vb
	''' Tiempo máximo: 0,2 seg</remarks>
	Public Function LineaFavoritos_ActualizarLineaFavoritos(ByVal Id As Integer, ByVal Dest As String, ByVal Precio As Double, ByVal Cant As Double, ByVal Obs As String, ByVal Campo1Valor As String, ByVal Campo2Valor As String, ByVal USU As String, ByVal FechaEntrega As Object, ByVal EntregaObl As Object, ByVal IDAlmacen As Integer, ByVal Activo As Integer, ByVal CentroSM As String, ByVal PartidaPres As String, ByVal sDescr_Libre As String) As Integer
		Authenticate()

		Dim SqlCn As New SqlConnection(mDBConnection)
		Dim SqlCmd As New SqlCommand
		Dim SqlParam As New SqlParameter
		Dim SqlTrans As SqlTransaction
		SqlCn.Open()
		SqlTrans = SqlCn.BeginTransaction
		SqlCmd.Transaction = SqlTrans

		SqlParam = SqlCmd.Parameters.AddWithValue("@ID", Id)
		If Not String.IsNullOrEmpty(Dest) Then
			SqlParam = SqlCmd.Parameters.AddWithValue("@DEST", Dest)
		Else
			SqlParam = SqlCmd.Parameters.AddWithValue("@DEST", System.DBNull.Value)
		End If
		If Not String.IsNullOrEmpty(Obs) Then
			SqlParam = SqlCmd.Parameters.AddWithValue("@OBS", Obs)
		Else
			SqlParam = SqlCmd.Parameters.AddWithValue("@OBS", System.DBNull.Value)
		End If
		If Not String.IsNullOrEmpty(Campo1Valor) Then
			SqlParam = SqlCmd.Parameters.AddWithValue("@Campo1Valor", Campo1Valor)
		Else
			SqlParam = SqlCmd.Parameters.AddWithValue("@Campo1Valor", System.DBNull.Value)
		End If
		If Not String.IsNullOrEmpty(Campo2Valor) Then
			SqlParam = SqlCmd.Parameters.AddWithValue("@Campo2Valor", Campo2Valor)
		Else
			SqlParam = SqlCmd.Parameters.AddWithValue("@Campo2Valor", System.DBNull.Value)
		End If
		If Precio = Nothing Then
			SqlParam = SqlCmd.Parameters.AddWithValue("@PRECIO", 0.0)
		Else
			SqlParam = SqlCmd.Parameters.AddWithValue("@PRECIO", Precio)
		End If
		If Cant = Nothing Then
			SqlParam = SqlCmd.Parameters.AddWithValue("@Cant", 0.0)
		Else
			SqlParam = SqlCmd.Parameters.AddWithValue("@Cant", Cant)
		End If
		SqlParam = SqlCmd.Parameters.AddWithValue("@USU", USU)
		SqlParam = SqlCmd.Parameters.Add("@FECHA", SqlDbType.DateTime)
		SqlCmd.Parameters("@FECHA").Value = IIf(FechaEntrega = Date.MinValue, DBNull.Value, FechaEntrega)
		SqlParam = SqlCmd.Parameters.Add("@OBLIGATORIA", SqlDbType.Bit)
		SqlCmd.Parameters("@OBLIGATORIA").Value = IIf(EntregaObl, 1, 0)
		SqlParam = SqlCmd.Parameters.Add("@ALMACEN", SqlDbType.Int)
		SqlCmd.Parameters("@ALMACEN").Value = IIf(IDAlmacen = 0, DBNull.Value, IDAlmacen)
		SqlParam = SqlCmd.Parameters.Add("@ACTIVO", SqlDbType.Int)
		SqlCmd.Parameters("@ACTIVO").Value = IIf(Activo = 0, DBNull.Value, Activo)
		SqlParam = SqlCmd.Parameters.Add("@DESCR_LIBRE", SqlDbType.VarChar, 200)
		SqlCmd.Parameters("@DESCR_LIBRE").Value = sDescr_Libre
		SqlCmd.CommandType = CommandType.Text
		SqlCmd.Connection = SqlCn
		SqlCmd.CommandText = "UPDATE FAVORITOS_LINEAS_PEDIDO SET " _
		  & "PREC_UP=@PRECIO, CANT_PED=@CANT, DEST=@DEST, OBS=@OBS, VALOR1=@CAMPO1VALOR, VALOR2=@CAMPO2VALOR, " _
		  & "FECENTREGA=@FECHA, ENTREGA_OBL=@OBLIGATORIA, ALMACEN=@ALMACEN, ACTIVO=@ACTIVO, DESCR_LIBRE=@DESCR_LIBRE " _
		  & "WHERE ID=@ID AND USU=@USU"

		Try
			SqlCmd.ExecuteNonQuery()
		Catch ex As Exception
			SqlCmd.Transaction.Rollback()
			SqlCn.Close()
			Return 1
		End Try

		If Not String.IsNullOrEmpty(CentroSM) Then
			SqlCmd.Parameters.Clear()
			' No se utiliza WITH (NOLOCK) puesto que se consulta la línea que se va a actualizar
			SqlCmd.CommandText = "IF EXISTS(SELECT ID FROM FAVORITOS_IMPUTACION WITH(NOLOCK) WHERE FAVORITO=@FAVORITOID AND USU = @USU AND ID=1) " &
			 "UPDATE FAVORITOS_IMPUTACION SET CENTRO_SM=@CENTROSM, PRES5_IMP=@PARTIDA WHERE FAVORITO=@FAVORITOID AND USU = @USU AND ID=1 " &
			 "ELSE " &
			 "INSERT INTO FAVORITOS_IMPUTACION (FAVORITO, USU, ID, CENTRO_SM, PRES5_IMP) VALUES (@FAVORITOID, @USU, 1, @CENTROSM, @PARTIDA)"
			SqlCmd.Parameters.Add("@FAVORITOID", SqlDbType.Int).Value = Id
			SqlCmd.Parameters.Add("@USU", SqlDbType.VarChar, 20).Value = USU
			SqlCmd.Parameters.Add("@CENTROSM", SqlDbType.VarChar, 50).Value = CentroSM
			SqlCmd.Parameters.Add("@PARTIDA", SqlDbType.Int).Value = IIf(PartidaPres = 0, DBNull.Value, PartidaPres)
			Try
				SqlCmd.ExecuteNonQuery()
			Catch ex As Exception
				SqlCmd.Transaction.Rollback()
				SqlCmd.Dispose()
				SqlCn.Close()
				Return 1
			End Try
		Else
			SqlCmd.Parameters.Clear()
			' No se utiliza WITH (NOLOCK) puesto que se consulta la línea que se va a actualizar
			SqlCmd.CommandText = "IF EXISTS(SELECT ID FROM FAVORITOS_IMPUTACION WITH(NOLOCK) WHERE FAVORITO=@FAVORITOID AND USU = @USU AND ID=1) " &
			 "DELETE FROM FAVORITOS_IMPUTACION WHERE FAVORITO=@FAVORITOID AND USU= @USU AND ID=1 "
			SqlCmd.Parameters.Add("@FAVORITOID", SqlDbType.Int).Value = Id
			SqlCmd.Parameters.Add("@USU", SqlDbType.VarChar, 20).Value = USU
			Try
				SqlCmd.ExecuteNonQuery()
			Catch ex As Exception
				SqlCmd.Transaction.Rollback()
				SqlCmd.Dispose()
				SqlCn.Close()
				Return 1
			End Try
		End If

		SqlCmd.Transaction.Commit()
		SqlCmd.Dispose()
		SqlCn.Close()
		Return 0
	End Function

	Public Function LineaPedido_ComprobarDeshacerBorrarLinea(ByVal IDLinea As Long, ByVal OrdenEstado As Integer, ByVal OrdenId As Long, ByVal TipoPedido As TipoDePedido, ByRef LineasNoDeshacer As String, ByRef LineasADeshacer As String) As Integer
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSN_COMPROBACIONES_DESHACER_BORRADO_LOGICO_PEDIDO"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@IDORDEN", OrdenId)
			cm.Parameters.AddWithValue("@ESTADO_PEDIDO", OrdenEstado)
			cm.Parameters.AddWithValue("@TIPO_PEDIDO", TipoPedido)
			cm.Parameters.AddWithValue("@LINEAS", CStr(IDLinea) & ",")
			cm.Parameters.Add("@MOTIVO_NO_DESHACER_BORRADO", SqlDbType.Int, 0).Direction = ParameterDirection.Output
			cm.Parameters.Add("@LINEAS_NO_DESHACER_BORRADO", SqlDbType.VarChar, 2000).Direction = ParameterDirection.Output
			cm.Parameters.Add("@LINEAS_A_DESHACER_BORRADO", SqlDbType.VarChar, 2000).Direction = ParameterDirection.Output

			cm.ExecuteNonQuery()

			LineasNoDeshacer = DBNullToStr(cm.Parameters("@LINEAS_NO_DESHACER_BORRADO").Value)
			LineasADeshacer = DBNullToStr(cm.Parameters("@LINEAS_A_DESHACER_BORRADO").Value)

			Return cm.Parameters("@MOTIVO_NO_DESHACER_BORRADO").Value
		Catch ex As Exception
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Function

	Public Function LineaPedido_ComprobarBorrar(ByVal IDLinea As Long, ByVal IdOrden As Long) As Integer
		Authenticate()

		Dim SQL As String
		Dim adocom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		Dim Da As New SqlDataAdapter
		Try
			cn.Open()

			SQL = "SELECT ISNULL(BAJA_LOG,0) BAJA_LOG FROM LINEAS_PEDIDO WITH(NOLOCK) WHERE ID=" & CStr(IDLinea)
			adocom.Connection = cn
			adocom.CommandText = SQL
			adocom.CommandType = CommandType.Text
			Dim dr As SqlDataReader = adocom.ExecuteReader()
			If dr.Read() Then
				If dr.Item("BAJA_LOG") = 1 Then
					Return 1
				End If
			End If
			dr.Close()
			adocom = New SqlCommand

			'GS: 
			'If ModoFuncionamiento = ModoPedidoEstandar Then
			'   If oOrden.Estado = TipoEstadoOrdenEntrega.EnRecepcion Then
			'else
			'   If oOrden.LineasPedido.Item(CodigoLinea(vbm)).PedidoAbiertoTienePedidosEmitidos Then bOk = False
			'Se comprueba si exiten recepciones para la linea o lineas a eliminar
			'   Maper

			'ModoPedidoEstandar
			'CASO IMPOSIBLE: El bt no aparecera
			'ModoPedidoAbierto
			'Comprobar PedidoAbiertoTienePedidosEmitidos
			SQL = "SELECT COUNT(ID) NUMPED FROM LINEAS_PEDIDO WITH (NOLOCK) WHERE LINEA_PED_ABIERTO=" & CStr(IDLinea)
			adocom.Connection = cn
			adocom.CommandText = SQL
			adocom.CommandType = CommandType.Text
			dr = adocom.ExecuteReader()
			If dr.Read() Then
				If dr.Item("NUMPED") > 0 Then
					Return 2
				End If
			End If
			dr.Close()
			'Se comprueba si exiten recepciones para la linea o lineas a eliminar
			'Maper
			'Quedamos con Integración que NO se llama a mapper
			'¿¿¿¿recepciones CASO IMPOSIBLE: El bt no aparecera???

			'No deja borrar la ultima línea de la Orden. Para eso BORRA la orden.
			SQL = "SELECT COUNT(1) NUMLIN FROM LINEAS_PEDIDO WITH (NOLOCK) WHERE BAJA_LOG=0 AND NOT (ID=" & CStr(IDLinea) & ") AND ORDEN=" & CStr(IdOrden)
			adocom.Connection = cn
			adocom.CommandText = SQL
			adocom.CommandType = CommandType.Text
			dr = adocom.ExecuteReader()
			If dr.Read() Then
				If dr.Item("NUMLIN") = 0 Then
					Return 3
				End If
			End If
			dr.Close()

		Catch ex As Exception
			Throw ex
		Finally
			cn.Close()
			cn.Dispose()
			adocom.Dispose()
			Da.Dispose()
		End Try
	End Function
	Public Function LineaPedido_DarDeBaja(ByVal IDLinea As Long, ByVal AccesoFSSM As Boolean, ByVal IdOrden As Long, ByVal sUsuario As String, ByVal bDeshacer As Boolean, ByVal dImpPedido As Double,
										  ByVal dImpCostes As Double, ByVal dImpDescuentos As Double, Optional ByVal bDeshacerBorrarOrden As Boolean = False, Optional ByRef cnConexionGlobal As SqlConnection = Nothing) As Integer
		Dim cm As New SqlCommand
		Dim cn As SqlConnection = If(cnConexionGlobal Is Nothing, New SqlConnection(mDBConnection), cnConexionGlobal)
		Dim iResultado As Integer = 0

		Dim SQL As String
		Dim adoRS As New DataSet
		Dim Da As New SqlDataAdapter
		Dim dr As SqlDataReader

		Dim ds As New DataSet
		Dim dSmImporteCompr As Double
		Dim dSmImporteComprImpuestos As Double
		Dim dImpComprometido As Double
		Dim sMonedaCentral As String
		Dim AlSmRestasOSumas As Integer = IIf(bDeshacer, 1, -1)

		Dim bGrabarLog As Boolean

		'DesHacer
		Dim bAltaPedido As Boolean

		Authenticate()

		'Using cn
		Try
			If cn.State <> ConnectionState.Open Then cn.Open()

			cm = New SqlCommand
			SQL = "SELECT TIPO,ORDEN_EST,ISNULL(COD_PROVE_ERP,'') COD_PROVE_ERP,EMPRESA,TIPO FROM ORDEN_ENTREGA WITH (NOLOCK) WHERE ID =" & CStr(IdOrden)
			cm.Connection = cn
			cm.CommandText = SQL
			cm.CommandType = CommandType.Text
			dr = cm.ExecuteReader()
			If Not dr.Read() Then
				Return False
			End If
			Dim UdtTabla As Integer
			If dr.Item("TIPO") = TipoDePedido.Aprovisionamiento Then
				UdtTabla = TablasIntegracion.PED_Aprov
			Else
				UdtTabla = TablasIntegracion.PED_directo
			End If
			Dim OrdenEst As Integer = dr.Item("ORDEN_EST")
			Dim CodErp As String = dr.Item("COD_PROVE_ERP")
			Dim Empresa As Long = dr.Item("EMPRESA")
			Dim EsContraAbierto As Boolean = (dr.Item("TIPO") = TipoDePedido.ContraPedidoAbierto)
			dr.Close()

			cm = New SqlCommand("BEGIN DISTRIBUTED TRANSACTION", cn)
			cm.ExecuteNonQuery()

			'GS'For i = 0 To UBound(arLineas)
			'GS'   TESError = oLinea.BajaLogicaLineaPedido(bDeshacer, bAltaPedido)
			cm = New SqlCommand
			cm.Connection = cn

			If bDeshacer Then
				cm.CommandText = "FSGS_DESHACER_BAJA_LOGICA_LINEA_PEDIDO"
				cm.Parameters.Add("@BAJAORDEN", SqlDbType.TinyInt).Direction = ParameterDirection.Output
			Else
				cm.CommandText = "FSGS_BAJA_LOGICA_LINEA_PEDIDO"
			End If

			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@ID", IDLinea)
			cm.ExecuteNonQuery()

			If bDeshacer Then bAltaPedido = (cm.Parameters("@BAJAORDEN").Value = 1)

			'GS'   m_adores.Open ("SELECT SM_IMPORTECOMPR,SM_IMPORTECOMPRIMPUESTOS FROM LINEAS_PEDIDO WITH(NOLOCK) WHERE ID =" & oLinea.Id), m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
			'GS'   If m_adores.eof Then
			'GS'       rollback
			'GS'   end if
			cm = New SqlCommand
			SQL = "SELECT ISNULL(SM_IMPORTECOMPR,0) SM_IMPORTECOMPR,ISNULL(SM_IMPORTECOMPRIMPUESTOS,0) SM_IMPORTECOMPRIMPUESTOS FROM LINEAS_PEDIDO WITH(NOLOCK) WHERE ID=" & CStr(IDLinea)
			cm.Connection = cn
			cm.CommandText = SQL
			cm.CommandType = CommandType.Text
			dr = cm.ExecuteReader()
			If Not dr.Read() Then
				cm = New SqlCommand("ROLLBACK TRANSACTION", cn)
				cm.ExecuteNonQuery()
				Return False
			Else
				dSmImporteCompr = dr.Item("SM_IMPORTECOMPR")
				dSmImporteComprImpuestos = dr.Item("SM_IMPORTECOMPRIMPUESTOS")
			End If
			dr.Close()

			'GS'   If bDeshacer Then
			'GS'        j = 1
			'GS'   Else 
			'GS'        j = (-1)
			'GS'   If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
			'GS'   If Not g_oParametrosSM Is Nothing Then
			'GS'       Dim oImput As CImputacion
			'GS'       If Not oLinea Is Nothing Then
			'GS'           If Not oLinea.Imputaciones Is Nothing Then
			'GS'               If oLinea.Imputaciones.Count > 0 Then
			'GS'                   For Each oImput In oLinea.Imputaciones
			'GS'                       If g_oParametrosSM.Item(oImput.PRES5).ComprAcum = 1 Then
			'GS'                           If oImput.PRES1 <> "" Then
			'GS'                               impComprometido = j * IIf(g_oParametrosSM.Item(oImput.PRES5).acumularImpuestos, m_adores("SM_IMPORTECOMPRIMPUESTOS"), m_adores("SM_IMPORTECOMPR"))
			'GS'                               'libero del solicitado la misma cantidad que del comprometido (impuesto con o sin) si el pedido es por importe
			'GS'                               TESError = oImput.ActualizarImportesPartida(m_vMoneda, m_vCambio,
			'GS'                                                                       impComprometido, , ,
			'GS'                                                                       IIf(g_oParametrosSM.Item(oImput.PRES5).ImpSC = 0, 0, j * NullToDbl0(oLinea.CantidadPedida)),
			'GS'                                                                       IIf(g_oParametrosSM.Item(oImput.PRES5).ImpSC = 0, 0, impComprometido),
			'GS'                                                                       NullToDbl0(oLinea.Solicitud), NullToDbl0(oLinea.IdCampoSolicit),
			'GS'                                                                       NullToDbl0(oLinea.IdLineaSolicit), oLinea.tipoRecepcion)
			'GS'                               If TESError.NumError <> TESnoerror Then
			'GS'                                   m_oConexion.ROLLBACKTRANSACTION               
			If AccesoFSSM Then
				cm = New SqlCommand
				SQL = "SELECT MONCEN FROM PARGEN_DEF WITH(NOLOCK) WHERE ID=1"
				cm.Connection = cn
				cm.CommandText = SQL
				cm.CommandType = CommandType.Text
				dr = cm.ExecuteReader()
				If dr.Read() Then
					sMonedaCentral = dr.Item("MONCEN")
				End If
				dr.Close()

				cm = New SqlCommand
				cm.Connection = cn
				cm.CommandText = "Select COUNT(*) FROM PARGEN_SM WITH(NOLOCK)"
				cm.CommandType = CommandType.Text

				Da.SelectCommand = cm
				Da.Fill(ds)
				If ds.Tables(0).Rows(0).Item(0) > 0 Then
					SQL = "Select PRES0,PRES1,PRES2,PRES3,PRES4,CAMBIO,PRES5_IMP,CAMBIO,ISNULL(SM.COMPR_ACUM,0) COMPR_ACUM,SM.ACUMULARIMP,SM.IMP_SC,ISNULL(LP.SOLICIT,0) SOLICIT,ISNULL(LP.CANT_PED,0) CANT_PED,ISNULL(LP.TIPORECEPCION,0) TIPORECEPCION,ISNULL(CAMPO_SOLICIT,0) CAMPO_SOLICIT, ISNULL(LINEA_SOLICIT,0) LINEA_SOLICIT"
					SQL = SQL & " FROM LINEAS_PED_IMPUTACION LPI With(NOLOCK)"
					SQL = SQL & " INNER JOIN LINEAS_PEDIDO LP With(NOLOCK) On LP.ID=LPI.LINEA"
					SQL = SQL & " INNER JOIN PARGEN_SM SM With (NOLOCK) On SM.PRES5=LPI.PRES0"
					SQL = SQL & " WHERE LINEA=" & CStr(IDLinea)

					cm.CommandText = SQL
					cm.CommandType = CommandType.Text

					ds = New DataSet
					Da = New SqlDataAdapter
					Da.SelectCommand = cm
					Da.Fill(ds)

					For Each row As DataRow In ds.Tables(0).Rows
						If row("COMPR_ACUM") = 1 Then
							If DBNullToStr(row("PRES1")) <> "" Then
								dImpComprometido = AlSmRestasOSumas * IIf(row("ACUMULARIMP") = 1, dSmImporteComprImpuestos, dSmImporteCompr)

								cm.Connection = cn
								cm.CommandText = "FSSM_ACTUALIZAR_IMPORTES_PARTIDA"
								cm.CommandType = CommandType.StoredProcedure
								If Not String.IsNullOrEmpty(DBNullToStr(row("PRES0"))) Then cm.Parameters.AddWithValue("@PRES0", row("PRES0"))
								If Not String.IsNullOrEmpty(DBNullToStr(row("PRES1"))) Then cm.Parameters.AddWithValue("@PRES1", row("PRES1"))
								If Not String.IsNullOrEmpty(DBNullToStr(row("PRES2"))) Then cm.Parameters.AddWithValue("@PRES2", row("PRES2"))
								If Not String.IsNullOrEmpty(DBNullToStr(row("PRES3"))) Then cm.Parameters.AddWithValue("@PRES3", row("PRES3"))
								If Not String.IsNullOrEmpty(DBNullToStr(row("PRES4"))) Then cm.Parameters.AddWithValue("@PRES4", row("PRES4"))
								cm.Parameters.AddWithValue("@MONEDA", sMonedaCentral)
								cm.Parameters.AddWithValue("@TIENECONTROLDISPONIBLE", 0)
								cm.Parameters.AddWithValue("@CONTROLDISPCONPRES", 0)
								cm.Parameters.AddWithValue("@INSTANCIA", row("SOLICIT"))
								cm.Parameters.AddWithValue("@IDIOMA", "SPA")
								If Not row("IMP_SC") = 0 AndAlso row("TIPORECEPCION") = 0 AndAlso Not row("CANT_PED") = 0 AndAlso Not row("SOLICIT") = 0 Then cm.Parameters.AddWithValue("@CANT_PEDIDA", AlSmRestasOSumas * row("CANT_PED"))
								If Not row("IMP_SC") = 0 AndAlso row("TIPORECEPCION") = 1 AndAlso Not dImpComprometido = 0 AndAlso Not row("SOLICIT") = 0 Then cm.Parameters.AddWithValue("@IMPORTE_PEDIDO", dImpComprometido)
								If Not row("CAMPO_SOLICIT") = 0 Then cm.Parameters.AddWithValue("@IDCAMPOSOLICIT", row("CAMPO_SOLICIT"))
								If Not row("LINEA_SOLICIT") = 0 Then cm.Parameters.AddWithValue("@LINEAPEDIDO", row("LINEA_SOLICIT"))
								If Not dImpComprometido = 0 Then cm.Parameters.AddWithValue("@COMPROMETIDO", dImpComprometido)
								cm.Parameters.AddWithValue("@OK", 0).Direction = ParameterDirection.InputOutput
								cm.Parameters.Add("@CODPARTIDA", SqlDbType.NVarChar, 100).Direction = ParameterDirection.Output
								cm.Parameters.Add("@DENPARTIDA", SqlDbType.NVarChar, 100).Direction = ParameterDirection.Output
								cm.Parameters.Add("@PRESUPUESTADO", SqlDbType.Float).Direction = ParameterDirection.Output
								cm.Parameters.Add("@DISPONIBLE", SqlDbType.Float).Direction = ParameterDirection.Output
								cm.Parameters.Add("@EQUIV", SqlDbType.Float).Direction = ParameterDirection.Output
								cm.Parameters.Add("@MON", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Output
								cm.ExecuteNonQuery()
							End If
						End If
					Next
				End If
			End If
			'GS'Next

			'GS'CalcularImportes False
			'GS'ModificarImporteTotalOrden m_dblImporteTotal, m_vId
			'GS'ActualizarImportesPedido
			'GS'    sConsulta = "UPDATE ORDEN_ENTREGA SET COSTES=?,DESCUENTOS=?,IMPORTE=? WHERE ID=?"
			'---------------------------------------------------------------------------------------------------------------
			'Se hara en DetallePedido.aspx.vb, el calculo del nuevo importe. 
			If Not bDeshacerBorrarOrden Then
				'Si se trata de deshacer la baja de una orden no hay que tocar los importes de ORDEN_ENTREGA porque ya estÃ¡n correctamente calculados
				cm = New SqlCommand
				cm.Connection = cn
				If bDeshacer Then
					cm.CommandText = "UPDATE ORDEN_ENTREGA SET IMPORTE=IMPORTE+@IMPORTE+@COSTES-@DESCUENTOS,COSTES=COSTES+@COSTES,DESCUENTOS=DESCUENTOS+@DESCUENTOS WHERE ID=" & CStr(IdOrden)
				Else
					cm.CommandText = "UPDATE ORDEN_ENTREGA SET IMPORTE=IMPORTE-@IMPORTE-@COSTES+@DESCUENTOS,COSTES=COSTES-@COSTES,DESCUENTOS=DESCUENTOS-@DESCUENTOS WHERE ID=" & CStr(IdOrden)
				End If
				cm.Parameters.AddWithValue("@COSTES", dImpCostes)
				cm.Parameters.AddWithValue("@DESCUENTOS", dImpDescuentos)
				cm.Parameters.AddWithValue("@IMPORTE", dImpPedido)
				cm.ExecuteNonQuery()
			End If

			'GS'bGrabarLog = m_oLineasPedido.Item(CStr(arLineas(i))).GrabarEnLog
			'GS'If bGrabarLog Then
			'GS'   TESError = GrabarLogOrdenEntrega(lIdLogOrden, sERP, Accion_Reenvio)
			'GS'   TESError = oLinea.GrabarLogLineaPedido(lIdLogOrden, lIdLogLineasPedido, Accion_Modificacion, sERP, Not bDeshacer)
			'GS'   TESError = oLinea.GrabarLogResto(lIdLogOrden, lIdLogLineasPedido)
			'GS'endif

			bGrabarLog = False
			'Hecho al principio SQL = "SELECT TIPO,ORDEN_EST,ISNULL(COD_PROVE_ERP,'') COD_PROVE_ERP,EMPRESA FROM ORDEN_ENTREGA WITH (NOLOCK) WHERE ID =" & CStr(IdOrden)

			cm = New SqlCommand
			If Empresa <> 0 Then
				SQL = "SELECT DISTINCT CASE WHEN EO.ERP IS NULL THEN ERP_SOCIEDAD.ERP ELSE EO.ERP END AS ERP FROM EMP WITH (NOLOCK) LEFT JOIN ERP_SOCIEDAD WITH (NOLOCK) ON ERP_SOCIEDAD.SOCIEDAD = EMP.SOCIEDAD"
				SQL = SQL & " LEFT JOIN(ERP_ORGCOMPRAS EO WITH (NOLOCK) INNER JOIN SOCIEDAD_ORGCOMPRAS SO WITH (NOLOCK) ON SO.ORGCOMPRAS=EO.ORGCOMPRAS) ON SO.SOCIEDAD = EMP.SOCIEDAD WHERE EMP.ID =" & CStr(Empresa)
			Else
				SQL = "SELECT ERP FROM ERP_ORGCOMPRAS EO WITH (NOLOCK) INNER JOIN ORDEN_ENTREGA WITH (NOLOCK) ON EO.ORGCOMPRAS = ORDEN_ENTREGA.ORGCOMPRAS WHERE ORDEN_ENTREGA.ID=" & CStr(IdOrden)
			End If
			cm.Connection = cn
			cm.CommandText = SQL
			cm.CommandType = CommandType.Text
			dr = cm.ExecuteReader()
			Dim Erp As Integer = 0
			If dr.Read() Then Erp = dr.Item("ERP")
			dr.Close()

			cm = New SqlCommand
			SQL = "Select 1 FROM PARGEN_GEST With(NOLOCK) WHERE PARGEN_GEST.ID=1 And PARGEN_GEST.ACTIVLOG=1"
			cm.Connection = cn
			cm.CommandText = SQL
			cm.CommandType = CommandType.Text
			dr = cm.ExecuteReader()
			Dim bActivLog As Boolean = False
			If dr.Read() Then bActivLog = True
			dr.Close()

			cm = New SqlCommand
			SQL = "SELECT TRASLADO_APROV_ERP,TRASPASO_PED_ERP FROM PARGEN_INTERNO WITH (NOLOCK) WHERE ID=1"
			cm.Connection = cn
			cm.CommandText = SQL
			cm.CommandType = CommandType.Text
			dr = cm.ExecuteReader()
			Dim Traspaso As Boolean = False
			If dr.Read() Then
				If UdtTabla = TablasIntegracion.PED_Aprov Then
					Traspaso = (dr.Item("TRASLADO_APROV_ERP") = 1)
				Else
					Traspaso = (dr.Item("TRASPASO_PED_ERP") = 1)
				End If
			End If
			dr.Close()

			cm = New SqlCommand
			SQL = "SELECT COUNT(1) CNT FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA =" & CStr(UdtTabla) & " AND ISNULL(ACTIVA,0)=1"
			cm.Connection = cn
			cm.CommandText = SQL
			cm.CommandType = CommandType.Text
			dr = cm.ExecuteReader()
			Dim Exportar As Boolean = False
			If dr.Read() Then
				If dr.Item("CNT") > 0 Then
					dr.Close()

					cm = New SqlCommand
					SQL = "SELECT COUNT(1) CNT FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA =" & CStr(UdtTabla) & " AND NOT (ISNULL(SENTIDO,0)=2)"
					cm.Connection = cn
					cm.CommandText = SQL
					cm.CommandType = CommandType.Text
					dr = cm.ExecuteReader()
					If dr.Read() Then
						If dr.Item("CNT") > 0 Then
							Exportar = True
						End If
					End If
				End If
			End If
			dr.Close()

			If bActivLog OrElse Exportar OrElse Traspaso Then
				Dim UdtOrigen As TiposDeDatos.OrigenIntegracion
				If Traspaso Then
					bGrabarLog = True
					If bActivLog And Not CodErp = "" Then
						UdtOrigen = TiposDeDatos.OrigenIntegracion.FSGSIntReg
					ElseIf Not CodErp = "" Then
						UdtOrigen = TiposDeDatos.OrigenIntegracion.FSGSInt
					ElseIf bActivLog Then
						UdtOrigen = TiposDeDatos.OrigenIntegracion.FSGSReg
					Else
						bGrabarLog = False
					End If
				Else
					bGrabarLog = True
					If bActivLog AndAlso Exportar Then
						UdtOrigen = TiposDeDatos.OrigenIntegracion.FSGSIntReg
					ElseIf bActivLog Then
						UdtOrigen = TiposDeDatos.OrigenIntegracion.FSGSReg
					Else
						UdtOrigen = TiposDeDatos.OrigenIntegracion.FSGSInt
					End If
				End If

				If bGrabarLog Then

					If bDeshacer Then
						cm.Connection = cn
						cm.CommandText = "FSN_DESHACER_BAJA_LOGICA_LINEA_PEDIDO_INT"
						cm.CommandType = CommandType.StoredProcedure
						cm.Parameters.AddWithValue("@USU", sUsuario)
						cm.Parameters.AddWithValue("@ORIGEN", UdtOrigen)
						cm.Parameters.AddWithValue("@IDORDEN", IdOrden)
						cm.Parameters.AddWithValue("@ESALTADEPEDIDO", IIf(bAltaPedido, 1, 0))
						cm.Parameters.AddWithValue("@LINEAS", CStr(IDLinea) & ",")
						cm.Parameters.AddWithValue("@TIPOLOG", IIf(bAltaPedido, TLog_Pedido.Deshacer_Eliminar_Pedido, TLog_Pedido.Deshacer_Eliminar_LineaPedido))
						cm.ExecuteNonQuery()
					Else
						If UdtOrigen = TiposDeDatos.OrigenIntegracion.FSGSInt OrElse UdtOrigen = TiposDeDatos.OrigenIntegracion.FSGSIntReg Then
							cm = New SqlCommand
							cm.Connection = cn
							cm.CommandText = "UPDATE ORDEN_ENTREGA SET EST_INT = 0 WHERE ID=" & CStr(IdOrden)
							cm.ExecuteNonQuery()
						End If

						SQL = "INSERT INTO LOG_ORDEN_ENTREGA (ACCION,ID_ORDEN_ENTREGA,ID_PEDIDO,ID_ORDEN_EST,ANYO,NUM_PEDIDO,PROVE,NUM_ORDEN,NUMEXT,TIPO," & vbCrLf
						SQL = SQL & "FECHA,IMPORTE,MON,CAMBIO,EST,PAG,VIA_PAG,INCORRECTA," & vbCrLf
						SQL = SQL & "COMENT,TIPO_MODIFICACION,ORIGEN,PER,USU,EMPRESA,COD_PROVE_ERP,RECEPTOR,REFERENCIA," & vbCrLf
						SQL = SQL & "OBS,ORGCOMPRAS,ERP,TIPOPEDIDO,FAC_DIR_ENVIO,IM_RETGARANTIA, COD_PROVE_ERP_FACTURA," & vbCrLf
						SQL = SQL & "COMEN_INT,FEC_ALTA,PED_ABIERTO_TIPO,PED_ABIERTO_FECINI,PED_ABIERTO_FECFIN,ABONO_DESDE," & vbCrLf
						SQL = SQL & "ABONO_HASTA,FACTURA,IM_AUTOFACTURA,NUM_PED_ERP,ACEP_PROVE,VISITADO_P,ABONO," & vbCrLf
						SQL = SQL & "PLAN_ENTREGA,COSTES,DESCUENTOS,NOTIFICADO,TIPO_LOG_PEDIDO, CAMPOS_MODIFICADOS,EST_INT,BAJA_LOG,FEC_BAJA_LOG) " & vbCrLf
						SQL = SQL & "Select DISTINCT" & vbCrLf
						SQL = SQL & "'D'," & CStr(IdOrden) & ", ORDEN_ENTREGA.PEDIDO, ORDEN_ENTREGA.ORDEN_EST, ORDEN_ENTREGA.ANYO, PEDIDO.NUM, ORDEN_ENTREGA.PROVE, ORDEN_ENTREGA.NUM,ORDEN_ENTREGA.NUMEXT,ORDEN_ENTREGA.TIPO," & vbCrLf
						SQL = SQL & "ORDEN_ENTREGA.FECHA, ORDEN_ENTREGA.IMPORTE, ORDEN_ENTREGA.MON, ORDEN_ENTREGA.CAMBIO, ORDEN_ENTREGA.EST, ORDEN_ENTREGA.PAG, ORDEN_ENTREGA.VIA_PAG, ORDEN_ENTREGA.INCORRECTA, " & vbCrLf
						SQL = SQL & "ORDEN_ENTREGA.COMEN_INT,NULL," & CStr(UdtOrigen) & ",PEDIDO.PER,'" & sUsuario & "', ORDEN_ENTREGA.EMPRESA, ORDEN_ENTREGA.COD_PROVE_ERP,ORDEN_ENTREGA.RECEPTOR,ORDEN_ENTREGA.PED_ERP, " & vbCrLf
						SQL = SQL & "ORDEN_ENTREGA.OBS, ORDEN_ENTREGA.ORGCOMPRAS," & CStr(Erp) & ",ORDEN_ENTREGA.TIPOPEDIDO, ORDEN_ENTREGA.FAC_DIR_ENVIO, ORDEN_ENTREGA.IM_RETGARANTIA, ORDEN_ENTREGA.COD_PROVE_ERP_FACTURA," & vbCrLf
						SQL = SQL & "ORDEN_ENTREGA.COMEN_INT, ORDEN_ENTREGA.FEC_ALTA, ORDEN_ENTREGA.PED_ABIERTO_TIPO, ORDEN_ENTREGA.PED_ABIERTO_FECINI, ORDEN_ENTREGA.PED_ABIERTO_FECFIN, ORDEN_ENTREGA.ABONO_DESDE," & vbCrLf
						SQL = SQL & "ORDEN_ENTREGA.ABONO_HASTA,ORDEN_ENTREGA.FACTURA,ORDEN_ENTREGA.IM_AUTOFACTURA, ORDEN_ENTREGA.NUM_PED_ERP,ORDEN_ENTREGA.ACEP_PROVE,ORDEN_ENTREGA.VISITADO_P,ORDEN_ENTREGA.ABONO," & vbCrLf
						SQL = SQL & "ORDEN_ENTREGA.PLAN_ENTREGA,ORDEN_ENTREGA.COSTES, ORDEN_ENTREGA.DESCUENTOS,ORDEN_ENTREGA.NOTIFICADO," & CStr(TLog_Pedido.Eliminar_LineaPedido) & ",NULL, ORDEN_ENTREGA.EST_INT,1,GETDATE()" & vbCrLf
						SQL = SQL & "FROM ORDEN_ENTREGA With(NOLOCK) INNER JOIN PEDIDO With(NOLOCK) On PEDIDO.ID = ORDEN_ENTREGA.PEDIDO WHERE ORDEN_ENTREGA.ID=" & CStr(IdOrden) & vbCrLf
						SQL = SQL & " SET @ULT_ID = (SELECT SCOPE_IDENTITY())" & vbCrLf

						cm = New SqlCommand
						cm.Connection = cn
						cm.CommandText = SQL
						cm.Parameters.Add(New SqlParameter("@ULT_ID", SqlDbType.Int))
						cm.Parameters("@ULT_ID").Direction = ParameterDirection.Output

						cm.ExecuteNonQuery()
						Dim idLogOrdenEntrega = DBNullToInteger(cm.Parameters("@ULT_ID").Value)

						SQL = "INSERT INTO LOG_LINEAS_PEDIDO (ACCION, ID_LOG_ORDEN_ENTREGA, ID_LINEAS_PEDIDO, ART_INT, UP, PREC_UP, CANT_PED, DEST, ANYO, GMN1, PROCE, ITEM, ENTREGA_OBL, FECENTREGA, FECENTREGAPROVE, EST, CAT1, CAT2, CAT3, CAT4, CAT5," & vbCrLf
						SQL = SQL & "OBS, FC, SOLICIT, LINEA_SOLICIT, DESCR_LIBRE, CAMPO1, VALOR1, CAMPO2, VALOR2, OBSADJUN, CENTROS, ALMACEN, PEDIDOABIERTO, ERP, ACTIVO," & vbCrLf
						SQL = SQL & "NUM, TIPORECEPCION, IMPORTE_PED, BAJA_LOG, DEST_GENERICO, DEST_DIR, DEST_POB, DEST_CP, DEST_PROVI, DEST_PAI, DEST_TFNO, DEST_FAX, DEST_EMAIL," & vbCrLf
						SQL = SQL & "LINCAT, PROVEADJ, PORCEN_DESVIO, COSTES, DESCUENTOS, SM_IMPORTECOMPR, SM_IMPORTECOMPRIMPUESTOS, CAMPO_SOLICIT, CANTIDAD_PED_ABIERTO, IMPORTE_PED_ABIERTO, FEC_ALTA, ORDEN_PED_ABIERTO," & vbCrLf
						SQL = SQL & "ADD_MATR_ART, BLOQ_MOD, LINEA_PED_ABIERTO, LINEAS_EST, DEST_DEN, FEC_BAJA_LOG)" & vbCrLf
						SQL = SQL & "Select 'D'," & idLogOrdenEntrega & ",ID,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5" & vbCrLf
						SQL = SQL & ",OBS,FC,SOLICIT,LINEA_SOLICIT,DESCR_LIBRE,CAMPO1,VALOR1,CAMPO2,VALOR2,OBSADJUN,CENTROS,ALMACEN,PEDIDOABIERTO," & CStr(Erp) & ",ACTIVO," & vbCrLf
						SQL = SQL & "NUM, TIPORECEPCION, IMPORTE_PED, 1, DEST_GENERICO, DEST_DIR, DEST_POB, DEST_CP, DEST_PROVI, DEST_PAI, DEST_TFNO, DEST_FAX, DEST_EMAIL, LINCAT, PROVEADJ, PORCEN_DESVIO, COSTES, DESCUENTOS," & vbCrLf
						SQL = SQL & "SM_IMPORTECOMPR, SM_IMPORTECOMPRIMPUESTOS, CAMPO_SOLICIT, CANTIDAD_PED_ABIERTO, IMPORTE_PED_ABIERTO, FEC_ALTA, ORDEN_PED_ABIERTO, ADD_MATR_ART, BLOQ_MOD, LINEA_PED_ABIERTO, LINEAS_EST, DEST_DEN, GETDATE()" & vbCrLf
						SQL = SQL & "FROM LINEAS_PEDIDO With(NOLOCK) WHERE ID=" & CStr(IDLinea) & vbCrLf
						SQL = SQL & " SET @ULT_ID_PED = (SELECT SCOPE_IDENTITY())" & vbCrLf

						cm = New SqlCommand
						cm.Connection = cn
						cm.CommandText = SQL
						cm.Parameters.Add(New SqlParameter("@ULT_ID_PED", SqlDbType.Int))
						cm.Parameters("@ULT_ID_PED").Direction = ParameterDirection.Output

						cm.ExecuteNonQuery()
						Dim idLogLineasPedido = DBNullToInteger(cm.Parameters("@ULT_ID_PED").Value)

						SQL = "INSERT INTO LOG_PEDIDO_PRES1 (ID_LOG_ORDEN_ENTREGA, ID_LOG_LINEAS_PEDIDO, ID_LINEA, PRES1, PRES2, PRES3, PRES4, PORCEN)" & vbCrLf
						SQL = SQL & "SELECT " & CStr(idLogOrdenEntrega) & "," & idLogLineasPedido & "," & CStr(IDLinea) & ",P.PRES1,P.PRES2,P.PRES3,P.PRES4,P.PORCEN FROM LINEAS_PEDIDO_PRES1 P WITH(NOLOCK) WHERE LINEA=" & CStr(IDLinea) & vbCrLf
						cm = New SqlCommand
						cm.Connection = cn
						cm.CommandText = SQL
						cm.ExecuteNonQuery()

						SQL = "INSERT INTO LOG_PEDIDO_PRES2 (ID_LOG_ORDEN_ENTREGA, ID_LOG_LINEAS_PEDIDO, ID_LINEA, PRES1, PRES2, PRES3, PRES4, PORCEN)" & vbCrLf
						SQL = SQL & "SELECT " & CStr(idLogOrdenEntrega) & "," & CStr(idLogLineasPedido) & "," & CStr(IDLinea) & ",P.PRES1,P.PRES2,P.PRES3,P.PRES4,P.PORCEN FROM LINEAS_PEDIDO_PRES2 P WITH(NOLOCK) WHERE LINEA=" & CStr(IDLinea) & vbCrLf
						cm.CommandText = SQL
						cm.ExecuteNonQuery()

						SQL = "INSERT INTO LOG_PEDIDO_PRES3 (ID_LOG_ORDEN_ENTREGA, ID_LOG_LINEAS_PEDIDO, ID_LINEA, PRES1, PRES2, PRES3, PRES4, PORCEN) " & vbCrLf
						SQL = SQL & "SELECT " & CStr(idLogOrdenEntrega) & "," & CStr(idLogLineasPedido) & "," & CStr(IDLinea) & ",P.PRES1,P.PRES2,P.PRES3,P.PRES4,P.PORCEN FROM LINEAS_PEDIDO_PRES3 P WITH(NOLOCK) WHERE LINEA=" & CStr(IDLinea) & vbCrLf
						cm.CommandText = SQL
						cm.ExecuteNonQuery()

						SQL = "INSERT INTO LOG_PEDIDO_PRES4 (ID_LOG_ORDEN_ENTREGA, ID_LOG_LINEAS_PEDIDO, ID_LINEA, PRES1, PRES2, PRES3, PRES4, PORCEN)" & vbCrLf
						SQL = SQL & "SELECT " & CStr(idLogOrdenEntrega) & "," & CStr(idLogLineasPedido) & "," & CStr(IDLinea) & ",P.PRES1,P.PRES2,P.PRES3,P.PRES4,P.PORCEN FROM LINEAS_PEDIDO_PRES4 P WITH(NOLOCK) WHERE LINEA=" & CStr(IDLinea) & vbCrLf
						cm.CommandText = SQL
						cm.ExecuteNonQuery()

						SQL = "INSERT INTO LOG_LINEAS_PEDIDO_ATRIB ( ID_LOG_LINEA, ATRIB, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
						SQL = SQL & "SELECT " & CStr(idLogLineasPedido) & ",A.ATRIB,A.VALOR_NUM,A.VALOR_TEXT,A.VALOR_FEC,A.VALOR_BOOL FROM LINEAS_PEDIDO_ATRIB  A WITH(NOLOCK) WHERE LINEA=" & CStr(IDLinea) & vbCrLf
						cm.CommandText = SQL
						cm.ExecuteNonQuery()

						SQL = "INSERT INTO LOG_LINEAS_PEDIDO_CD (ID_LOG_LINEAS_PEDIDO, ATRIB, TIPO, OPERACION, VALOR, IMPORTE, FACTURAR, DESCR)" & vbCrLf
						SQL = SQL & "SELECT " & CStr(idLogLineasPedido) & ",OA.ATRIB,OA.TIPO,OA.OPERACION,OA.VALOR,OA.IMPORTE,OA.FACTURAR,OA.DESCR  FROM LINEAS_PEDIDO_CD OA WITH(NOLOCK) WHERE LINEA=" & CStr(IDLinea) & vbCrLf
						cm.CommandText = SQL
						cm.ExecuteNonQuery()

						SQL = "INSERT INTO LOG_LINEAS_PEDIDO_CD_IMPUESTO (ID_LOG_LINEAS_PEDIDO_CD, COSTE, IMPUESTO, VALOR)" & vbCrLf
						SQL = SQL & "SELECT LLPC.ID,OA.COSTE,OA.IMPUESTO,OA.VALOR FROM LINEAS_PEDIDO_CD_IMPUESTO OA WITH(NOLOCK)" & vbCrLf
						SQL = SQL & "INNER JOIN (LOG_LINEAS_PEDIDO_CD LLPC WITH(NOLOCK) INNER JOIN LOG_LINEAS_PEDIDO LLP WITH(NOLOCK) ON LLPC.ID_LOG_LINEAS_PEDIDO=LLP.ID) ON LLP.ID_LINEAS_PEDIDO=OA.LINEA" & vbCrLf
						SQL = SQL & "WHERE OA.LINEA=" & CStr(IDLinea) & vbCrLf
						cm.CommandText = SQL
						cm.ExecuteNonQuery()

						SQL = "INSERT INTO LOG_LINEAS_PEDIDO_IMPUESTO (ID_LOG_LINEAS_PEDIDO, IMPUESTO, LINEA, VALOR)" & vbCrLf
						SQL = SQL & "Select " & CStr(idLogLineasPedido) & ",OA.IMPUESTO,OA.LINEA,OA.VALOR FROM LINEAS_PEDIDO_IMPUESTO OA With(NOLOCK)  WHERE LINEA=" & CStr(IDLinea) & vbCrLf
						cm.CommandText = SQL
						cm.ExecuteNonQuery()

						SQL = "INSERT INTO LOG_LINEAS_PED_IMPUTACION(ID_LOG_LINEA, LINEA, UON1, UON2, UON3, UON4, PRES0, PRES1, PRES2, PRES3, PRES4, CAMBIO)" & vbCrLf
						SQL = SQL & "Select " & CStr(idLogLineasPedido) & ",LPI.LINEA,LPI.UON1,LPI.UON2,LPI.UON3,LPI.UON4,LPI.PRES0,LPI.PRES1,LPI.PRES2,LPI.PRES3,LPI.PRES4,LPI.CAMBIO " & vbCrLf
						SQL = SQL & "FROM LINEAS_PED_IMPUTACION LPI With(NOLOCK) WHERE LINEA=" & CStr(IDLinea) & vbCrLf
						cm.CommandText = SQL
						cm.ExecuteNonQuery()

						SQL = "INSERT INTO LOG_LINEAS_PEDIDO_ENTREGAS (ID_LOG_LINEAS_PEDIDO, ORDEN, FECHA, CANTIDAD, ALBARAN, IMPORTE, ID_LINEAS_PEDIDO_ENTREGAS)" & vbCrLf
						SQL = SQL & "Select " & CStr(idLogLineasPedido) & ",OA.ORDEN,OA.FECHA,OA.CANTIDAD,OA.ALBARAN,OA.IMPORTE,OA.ID FROM LINEAS_PEDIDO_ENTREGAS OA With(NOLOCK) WHERE LINEA_PEDIDO=" & CStr(IDLinea) & vbCrLf
						cm.CommandText = SQL
						cm.ExecuteNonQuery()

						SQL = "INSERT INTO LOG_LINEAS_PEDIDO_ADJUN (ID_LOG_LINEAS_PEDIDO, NOM, COM, DATA, DATASIZE, ID_LINEAS_PEDIDO_ADJUN)" & vbCrLf
						SQL = SQL & " Select " & CStr(idLogLineasPedido) & ",OA.NOM,OA.COM,OA.DATA,OA.DATASIZE,OA.ID FROM LINEAS_PEDIDO_ADJUN OA With(NOLOCK) WHERE LINEA=" & CStr(IDLinea) & vbCrLf
						cm.CommandText = SQL
						cm.ExecuteNonQuery()
					End If
				End If
			End If


			If EsContraAbierto And Not bDeshacerBorrarOrden Then
				SQL = "INSERT INTO NOTIFICAR_BORRADO_LOGICO (ORDEN,LINEA_PEDIDO,BAJA,FEC_ACCION,FEC_NOTIF)" & vbCrLf
				SQL = SQL & " VALUES (" & CStr(IdOrden) & "," & CStr(IDLinea) & "," & IIf(bDeshacer, "0", "1") & ",GETDATE(),NULL)" & vbCrLf
				cm.CommandType = CommandType.Text
				cm.CommandText = SQL
				cm.ExecuteNonQuery()
			End If
			'GS'commit
			cm = New SqlCommand("COMMIT TRANSACTION", cn)
			cm.ExecuteNonQuery()

			iResultado = 1

			'GS'Integrar
			'Se hace por codigo

		Catch ex As Exception
			cm = New SqlCommand("ROLLBACK TRANSACTION", cn)
			cm.ExecuteNonQuery()
		Finally
			cm.Dispose()
			If cnConexionGlobal Is Nothing Then
				cn.Close()
				cn.Dispose()
			End If
		End Try
		'End Using

		Return iResultado
	End Function
#End Region
#Region " Notificador data access methods"
	''' <summary>
	''' Revisado por: blp
	''' Función que carga una orden con sus datos para poder enviar mail
	''' </summary>
	''' <param name="OrdenId">Id de la orden a cargar</param>
	''' <param name="Idioma">Idioma</param>
	''' <returns>Un dataset con los datos de la orden a cargar</returns>
	''' <remarks>
	''' Llamada desde:Clase Notificador, método CargarOden
	''' Tiempo Máximo:? seg</remarks>
	Public Function Notificador_CargarOrden(ByVal OrdenId As Integer, Optional ByVal Idioma As String = Nothing) As DataSet
		Authenticate()

		Dim SQL As String
		Dim adocom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		Dim Da As New SqlDataAdapter
		Try
			cn.Open()

			Dim PartidaNom As String = ""
			If Not Idioma Is Nothing Then
				adocom.Connection = cn
				adoPar = adocom.Parameters.AddWithValue("@IDIOMA", Idioma)
				SQL = "SELECT DISTINCT CASE PSM.IMP_NIVEL WHEN 2 THEN ISNULL(P5I0.NOM_NIVEL2,'') WHEN 3 THEN ISNULL(P5I0.NOM_NIVEL3,'') WHEN 4 THEN ISNULL(P5I0.NOM_NIVEL4,'''') ELSE ISNULL(P5I0.NOM_NIVEL1,'') END PARTIDA_DEN" &
				" FROM PARGEN_SM PSM WITH (NOLOCK) LEFT JOIN PRES5_IDIOMAS P5I0 WITH (NOLOCK) ON P5I0.PRES0 = PSM.PRES5 AND P5I0.PRES1 IS NULL AND P5I0.PRES2 IS NULL AND P5I0.PRES3 IS NULL AND P5I0.PRES4 IS NULL AND P5I0.IDIOMA = @IDIOMA"
				adocom.CommandText = SQL
				adocom.CommandType = CommandType.Text
				Dim dr As SqlDataReader = adocom.ExecuteReader()
				If dr.Read() Then
					PartidaNom = dr.Item("PARTIDA_DEN")
				End If
				dr.Close()

				adocom = New SqlCommand
			End If

			adocom.Connection = cn
			adoPar = adocom.Parameters.AddWithValue("@ORDENID", OrdenId)
			SQL = "SELECT OE.ID, OE.PEDIDO, OE.ANYO, PE.NUM PEDIDONUM, OE.PROVE, OE.NUM, OE.NUMEXT, OE.EST, OE.TIPO, OE.FECHA, OE.IMPORTE, OE.INCORRECTA, OE.NUM_PED_ERP, "
			If Not Idioma Is Nothing Then
				SQL = SQL & " TPD.DEN TIPOPEDIDO_DEN,  "
				adoPar = adocom.Parameters.AddWithValue("@IDIOMA", Idioma)
			End If
			SQL = SQL & "OE.OBS, P.DEN PROVEDEN, OE.MON, OE.CAMBIO As EQUIV, PER.NOM, PER.APE, PER.UON1, PER.UON2, PER.UON3,coalesce(U3.DEN,u2.den, u1.den) UON_DEN, OE.EMPRESA,OE.COD_PROVE_ERP, OE.RECEPTOR, " &
			 "R.NOM RECEP_NOM, R.APE RECEP_APE, EMP.NIF CIF, EMP.DEN RAZON, EMP.DIR DIREMP, EMP.CP,EMP.POB,PAI_DEN.DEN PAIS,PROVI_DEN.DEN PROVINCIA, " &
			 "FDE.DIR DIRECCIONENVIOFRA, FDE.CP CPENVIOFRA, FDE.POB POBLACIONENVIOFRA, PAG_DEN.DEN FORMAPAGO, " &
			 "'" & PartidaNom & "' AS PARTIDA_NOM, PIN.EP_NOTIFPROVE_ADJUNTAR_PEDIDO_PDF, OE.ACEP_PROVE " &
			 "FROM ORDEN_ENTREGA OE WITH(NOLOCK) " &
			 "INNER JOIN PEDIDO PE WITH(NOLOCK) " &
			 "ON OE.PEDIDO=PE.ID "
			If Not Idioma Is Nothing Then
				SQL = SQL & " INNER JOIN TIPOPEDIDO_DEN TPD WITH(NOLOCK) ON TPD.TIPOPEDIDO = OE.TIPOPEDIDO AND TPD.IDI = @IDIOMA"
			End If
			SQL = SQL & " LEFT JOIN PER WITH(NOLOCK) " &
			  "ON PE.PER=PER.COD " &
			 "INNER JOIN (PROVE P WITH(NOLOCK) LEFT JOIN MON M WITH(NOLOCK) " &
			 " ON M.COD = P.MON) " &
			  "ON OE.PROVE = P.COD " &
			 "LEFT JOIN PER R WITH(NOLOCK) " &
			  "ON OE.RECEPTOR=R.COD " &
			 "LEFT JOIN EMP WITH(NOLOCK) " &
			  "ON OE.EMPRESA=EMP.ID " &
			 "LEFT JOIN PAI_DEN WITH(NOLOCK) " &
			  "ON EMP.PAI=PAI_DEN.PAI AND PAI_DEN.IDIOMA = '" & Idioma & "' " &
			 "LEFT JOIN PROVI_DEN WITH(NOLOCK) " &
			  "ON EMP.PROVI=PROVI_DEN.PROVI AND EMP.PAI=PROVI_DEN.PAI AND PROVI_DEN.IDIOMA = '" & Idioma & "' " &
			 "LEFT JOIN FAC_DIR_ENVIO FDE WITH(NOLOCK) " &
			  "ON OE.FAC_DIR_ENVIO = FDE.ID " &
			 "Left join UON1 u1 With (nolock) On per.uon1 = u1.cod " &
			 "Left Join UON2 u2 With (nolock) On per.uon1 = u2.UON1 And per.uon2 = u2.cod " &
			 "Left Join UON3 u3 with (nolock) on per.uon1 = u3.UON1 And per.uon2 = u3.uon2 And per.uon3 = u3.cod " &
			 " INNER JOIN PARGEN_INTERNO PIN WITH (NOLOCK) ON PIN.ID=1 " &
			 "LEFT JOIN PAG_DEN WITH(NOLOCK) " &
			  "ON OE.PAG = PAG_DEN.PAG AND PAG_DEN.IDIOMA = '" & Idioma & "' " &
			 "WHERE OE.ID = @ORDENID " &
			 "ORDER BY OE.NUM"
			adocom.CommandText = SQL
			adocom.CommandType = CommandType.Text
			Da.SelectCommand = adocom
			Da.Fill(adoRS)
			cn.Close()
			cn.Dispose()
			adocom.Dispose()
			Da.Dispose()
			Return adoRS
		Catch ex As Exception
			Throw ex
		Finally
			cn.Dispose()
			adocom.Dispose()
			Da.Dispose()
		End Try
	End Function
	''' <summary>
	''' Función que devuelve el tema de un mensaje a enviar, dado el modulo de idioma y el Id del texto
	''' </summary>
	''' <param name="Modulo">Módulo de idioma a Consultar</param>
	''' <param name="Id">Identificador del texto a consultar</param>
	''' <param name="Idioma">Idioma del usuario de la aplicación</param>
	''' <returns>Un DataSet con el Tema del mensaje a enviar</returns>
	''' <remarks>
	''' Llamada desde:El Proyecto EpNotificador, clase Notificador, funcion RecuperarTemaDeMensaje
	''' Tiempo máximo: ? seg</remarks>
	Public Function Notificador_RecuperarTemaDeMensaje(ByVal Modulo As Integer, ByVal Id As Integer, ByVal Idioma As String) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim SqlCmd As New SqlCommand
		Dim SqlParam As New SqlParameter
		Dim Dset As New DataSet
		Dim Da As New SqlDataAdapter
		Dim SqlCons As String
		Try
			cn.Open()
			SqlCmd.Connection = cn
			SqlCmd.CommandType = CommandType.Text
			SqlParam = SqlCmd.Parameters.AddWithValue("@MODULO", Modulo)
			SqlParam = SqlCmd.Parameters.AddWithValue("@ID", Id)
			SqlCons = "SELECT TEXT_" & Idioma & " FROM WEBFSEPTEXT WITH(NOLOCK) WHERE MODULO=@MODULO AND ID=@ID"
			SqlCmd.CommandText = SqlCons
			Da.SelectCommand = SqlCmd
			Da.Fill(Dset)
			cn.Close()
			cn.Dispose()
			SqlCmd.Dispose()
			Da.Dispose()
			Return Dset
		Catch ex As Exception
			Throw ex
		Finally
			cn.Dispose()
			SqlCmd.Dispose()
			Da.Dispose()
		End Try
	End Function
	''' <summary>
	''' Función que recupera los datos de un proveedor para envio de mails
	''' </summary>
	''' <param name="Prove">Código del proveedor del cual queremos recuperar los datos</param>
	''' <returns>Un dataset con los datos del proveedor buscado</returns>
	''' <remarks>
	''' Llamada desde: Proyecto EpNotificador, clase Notificador, método CargarDatosProveedor
	''' Tiempo máximo: ? seg</remarks>
	Public Function Notificador_CargarDatosProveedor(ByVal Prove As String) As DataSet
		Authenticate()

		Dim adorContac As New DataSet
		Dim SqlCm As New SqlCommand
		Dim cn As New SqlConnection(mDBConnection)
		Dim sConsulta As String
		Dim SqlParam As New SqlParameter
		Dim Da As New SqlDataAdapter
		Try
			SqlParam = SqlCm.Parameters.AddWithValue("@PROVE", Prove)
			cn.Open()
			SqlCm.Connection = cn
			SqlCm.CommandType = CommandType.Text
			sConsulta = "SELECT CON.APE,CON.NOM,CON.CAR,CON.TFNO,CON.FAX,CON.DEP,CON.TFNO2,CON.EMAIL,CON.ID_PORT, PROVE.FSP_COD " & "  FROM CON WITH(NOLOCK) " & "INNER JOIN PROVE WITH(NOLOCK) " & "ON PROVE.COD=CON.PROVE " & "WHERE PROVE.COD = @PROVE AND CON.APROV = 1"
			SqlCm.CommandText = sConsulta
			Da.SelectCommand = SqlCm
			Da.Fill(adorContac)
			cn.Close()
			cn.Dispose()
			SqlCm.Dispose()
			Da.Dispose()
			Return adorContac
		Catch ex As Exception
			Throw ex
		Finally
			cn.Dispose()
			SqlCm.Dispose()
			Da.Dispose()
		End Try
	End Function
	''' <summary>
	''' Función que devuelve el idioma de un proveedor.
	''' </summary>
	''' <param name="FSP">Valor para saber que Stored debe ejecutarse dependiendo de el producto</param>
	''' <param name="CodFSP">El código para el producto del Stored que se ejecute</param>
	''' <param name="IdUsu">El Identificador de usuario</param>
	''' <param name="msTo">La dirección de correo electrónico donde va dirigido el mail</param>
	''' <param name="CodProve">El código de proveedor a mirar</param>
	''' <returns>Un dataset con los datos en el idioma del proveedor</returns>
	''' <remarks>
	''' Llamada desde: El proyecto EPNotificador, clase Notificador, método DevolverIdiomaProveedor
	''' Tiempo máximo: ? seg</remarks>
	Public Function Notificador_DevolverIdiomaProveedor(ByVal FSP As String, ByVal CodFSP As String, ByVal IdUsu As String, ByVal msTo As String, ByVal CodProve As String) As DataSet
		Authenticate()

		Dim SqlCmd As New SqlCommand
		Dim SqlCn As New SqlConnection(mDBConnection)
		Dim Dset As New DataSet
		Dim Da As New SqlDataAdapter
		If Not String.IsNullOrEmpty(FSP) Then
			If Not IsDBNull(IdUsu) Then
				SqlCmd.Parameters.AddWithValue("@CIA", CodFSP)
				SqlCmd.Parameters.AddWithValue("@USU", IdUsu)
				SqlCmd.CommandText = FSP & "SP_DEVOLVER_USUARIO"
				SqlCmd.CommandType = CommandType.StoredProcedure
			End If
		Else
			SqlCmd.Parameters.AddWithValue("@TO", msTo)
			SqlCmd.Parameters.AddWithValue("@CODPROVE", CodProve)
			SqlCmd.CommandText = "SELECT IDI AS COD,DATEFMT,DECIMALFMT,THOUSANFMT,PRECISIONFMT,TIPOEMAIL" &
			 " FROM CON WITH(NOLOCK) " &
			 " WHERE APROV=1 AND EMAIL=@TO AND PROVE=@CODPROVE"
			SqlCmd.CommandType = CommandType.Text
		End If
		Using SqlCn
			SqlCn.Open()
			SqlCmd.Connection = SqlCn
			Da.SelectCommand = SqlCmd
			Da.Fill(Dset)
		End Using
		Return Dset
	End Function
	''' <summary>
	''' Actualiza el campo NUM_RANDOM de LINEAS_PEDIDO, ese numero sera enviado en las notificaciones por email para la aprobacion directa de un pedido
	''' </summary>
	''' <param name="iNumRandom">numero aleatorio que grabaremos</param>
	''' <param name="sIdioma">idioma de la aplicacion</param>
	''' <param name="lOrdenId">Identificador de la Orden de la cual se quieren actualizar las lineas con el numero Random</param>
	''' <param name="lIdSeguridad">Seguridad de  las lineas</param>
	''' <remarks></remarks>
	Public Sub GrabarNumeroAleatorioLineasPedido(ByVal iNumRandom As Integer, ByVal sIdioma As String, ByVal lOrdenId As Integer, Optional ByVal sAccion As String = "", Optional ByVal lIdSeguridad As Integer = 0, Optional ByVal Seguridad_Nivel As Byte = 0)
		Dim SQL As String
		Dim sqlCmd As New SqlCommand
		Dim cn As New SqlConnection(mDBConnection)
		Dim SqlParam As New SqlParameter

		Try
			cn.Open()
			SqlParam = sqlCmd.Parameters.AddWithValue("@ORDENID", lOrdenId)

			If sAccion <> "" Then
				If lIdSeguridad > 0 Then
					SqlParam = sqlCmd.Parameters.AddWithValue("@SEGURIDAD", lIdSeguridad)
					SqlParam = sqlCmd.Parameters.AddWithValue("@SEGURIDAD_NIVEL", Seguridad_Nivel)
				End If
				SqlParam = sqlCmd.Parameters.AddWithValue("@IDIOMA", sIdioma)
				SQL = "UPDATE LINEAS_PEDIDO SET ACCION_EMAIL='" & sAccion & "'"
				SQL = SQL & " WHERE ORDEN = @ORDENID" & vbCrLf

				If lIdSeguridad > 0 Then
					SQL = SQL & "AND SEGURIDAD_CATN= @SEGURIDAD AND SEGURIDAD_NIVEL=@SEGURIDAD_NIVEL"
				End If
			Else
				If lIdSeguridad > 0 Then
					SqlParam = sqlCmd.Parameters.AddWithValue("@SEGURIDAD", lIdSeguridad)
					SqlParam = sqlCmd.Parameters.AddWithValue("@SEGURIDAD_NIVEL", Seguridad_Nivel)
				End If
				SqlParam = sqlCmd.Parameters.AddWithValue("@IDIOMA", sIdioma)
				SQL = "UPDATE LINEAS_PEDIDO SET NUM_RANDOM=" & iNumRandom.ToString
				SQL = SQL & " WHERE ORDEN = @ORDENID" & vbCrLf

				If lIdSeguridad > 0 Then
					SQL = SQL & "AND SEGURIDAD_CATN= @SEGURIDAD AND SEGURIDAD_NIVEL=@SEGURIDAD_NIVEL"
				End If
			End If
			sqlCmd.CommandType = CommandType.Text
			sqlCmd.Connection = cn
			sqlCmd.CommandText = SQL
			sqlCmd.ExecuteNonQuery()
		Catch ex As Exception
			Throw ex
		Finally
			cn.Close()
			cn = Nothing
		End Try
	End Sub
	''' <summary>
	''' FunciÃ³n que carga en un DataSet los usuarios notificados de tipo 2
	''' </summary>
	''' <param name="Pedido">Id del pedido del cual queremos cargar los datos</param>
	''' <param name="SoloPendientes">Variable booleana que indica si solo se quieren cargar las ordenes con estado pendiente</param>
	''' <param name="Orden">Id de la orden a filtrar</param>
	''' <param name="Seguridad">Seguridad de la orden a filtrar</param>
	''' <param name="bAprobarTrasLimPedido">Variable booleana que indica si solo se quieren cargar los Notificados del PEDIDO.PER, es decir, emisor</param>
	''' <param name="Seguridad_Nivel" >Nivel de la categoria</param> 
	''' <returns>Un dataSet con los datos de los notificados de tepo 2</returns>
	''' <remarks>Llamada desde: El proyecto EPNotificador, clase Notificador, mÃ©todo CargarNotificadosTipo2
	''' Tiempo mÃ¡ximo: ? seg</remarks>
	''' Revisado por: jbg; Fecha: 13/02/2014  
	Public Function Notificador_CargarNotificadosTipo2(ByVal Pedido As Integer, ByVal SoloPendientes As Boolean, ByVal Orden As Integer, ByVal Seguridad As Integer, Optional ByVal bAprobarTrasLimPedido As Boolean = False, Optional ByVal Seguridad_Nivel As Byte = 0) As DataSet
		Authenticate()

		Dim sConsulta As String
		Dim SqlCmd As New SqlCommand
		Dim Cn As New SqlConnection(mDBConnection)
		Dim SqlParam As New SqlParameter
		Dim da As New SqlDataAdapter
		Dim ador As New DataSet
		If Pedido = 0 Then
			If SoloPendientes Then
				SqlParam = SqlCmd.Parameters.AddWithValue("@ORDEN", Orden)
				SqlParam = SqlCmd.Parameters.AddWithValue("@SEGURIDAD", Seguridad)
				SqlParam = SqlCmd.Parameters.AddWithValue("@SEGURIDAD_NIVEL", Seguridad_Nivel)

				sConsulta = "SELECT DISTINCT U.MON,MON.EQUIV,U.DATEFMT,U.DECIMALFMT,U.THOUSANFMT,U.PRECISIONFMT,U.IDIOMA,U.TIPOEMAIL,P.COD,P.EMAIL,P.NOM,P.APE,LP.ANYO,LP.PEDIDO,LP.ORDEN,LP.SEGURIDAD_CATN,LP.SEGURIDAD_NIVEL "
				sConsulta = sConsulta & " FROM LINEAS_PEDIDO LP WITH(NOLOCK) "
				sConsulta = sConsulta & " INNER JOIN INSTANCIA_EST IE WITH (NOLOCK) ON IE.INSTANCIA=LP.INSTANCIA_APROB"
				sConsulta = sConsulta & " LEFT JOIN PM_COPIA_ROL PR WITH(NOLOCK) ON PR.ID=IE.ROL"
				sConsulta = sConsulta & " INNER JOIN PER P WITH (NOLOCK) ON P.COD=IE.PER"
				sConsulta = sConsulta & " INNER JOIN USU U WITH (NOLOCK) ON P.COD=U.PER"
				sConsulta = sConsulta & " LEFT JOIN MON WITH (NOLOCK) ON MON.COD=U.MON"
				sConsulta = sConsulta & " WHERE LP.ORDEN=@ORDEN"
				sConsulta = sConsulta & " AND LP.EST = 0  AND LP.SEGURIDAD_CATN=@SEGURIDAD AND LP.SEGURIDAD_NIVEL=@SEGURIDAD_NIVEL"
				sConsulta = sConsulta & " AND PR.TIPO<>4"
			Else
				SqlParam = SqlCmd.Parameters.AddWithValue("@ORDEN", Orden)

				sConsulta = "SELECT DISTINCT U.MON,MON.EQUIV,U.DATEFMT,U.DECIMALFMT,U.THOUSANFMT,U.PRECISIONFMT,U.IDIOMA,U.TIPOEMAIL,P.COD,P.EMAIL,P.NOM,P.APE,LP.ANYO,LP.PEDIDO,LP.ORDEN,LP.SEGURIDAD_CATN,LP.SEGURIDAD_NIVEL "
				sConsulta = sConsulta & " FROM LINEAS_PEDIDO LP WITH(NOLOCK) "
				sConsulta = sConsulta & " INNER JOIN INSTANCIA_EST IE WITH (NOLOCK) ON IE.INSTANCIA=LP.INSTANCIA_APROB"
				sConsulta = sConsulta & " LEFT JOIN PM_COPIA_ROL PR WITH(NOLOCK) ON PR.ID=IE.ROL"
				sConsulta = sConsulta & " INNER JOIN PER P WITH (NOLOCK) ON P.COD=IE.PER"
				sConsulta = sConsulta & " INNER JOIN USU U WITH (NOLOCK) ON P.COD=U.PER"
				sConsulta = sConsulta & " LEFT JOIN MON WITH (NOLOCK) ON MON.COD=U.MON"
				sConsulta = sConsulta & " WHERE LP.ORDEN=@ORDEN"
				sConsulta = sConsulta & " AND PR.TIPO<>4"
			End If
		Else
			SqlParam = SqlCmd.Parameters.AddWithValue("@PEDIDO", Pedido)
			sConsulta = "SELECT DISTINCT U.MON,MON.EQUIV,U.DATEFMT,U.DECIMALFMT,U.THOUSANFMT,U.PRECISIONFMT,U.IDIOMA,U.TIPOEMAIL,P.COD,P.EMAIL,P.NOM,P.APE,LP.ANYO,LP.PEDIDO,LP.ORDEN,LP.SEGURIDAD_CATN,LP.SEGURIDAD_NIVEL "
			sConsulta = sConsulta & " FROM LINEAS_PEDIDO LP WITH(NOLOCK) "
			sConsulta = sConsulta & " INNER JOIN INSTANCIA_EST IE WITH (NOLOCK) ON IE.INSTANCIA=LP.INSTANCIA_APROB"
			sConsulta = sConsulta & " LEFT JOIN PM_COPIA_ROL PR WITH(NOLOCK) ON PR.ID=IE.ROL"
			sConsulta = sConsulta & " INNER JOIN PER P WITH (NOLOCK) ON P.COD=IE.PER"
			sConsulta = sConsulta & " INNER JOIN USU U WITH (NOLOCK) ON P.COD=U.PER"
			sConsulta = sConsulta & " LEFT JOIN MON WITH (NOLOCK) ON MON.COD=U.MON"
			sConsulta = sConsulta & " WHERE LP.PEDIDO=@PEDIDO"
			sConsulta = sConsulta & " AND PR.TIPO<>4"
		End If
		SqlCmd.CommandText = sConsulta
		SqlCmd.CommandType = CommandType.Text
		SqlCmd.Connection = Cn
		Cn.Open()
		da.SelectCommand = SqlCmd
		da.Fill(ador)
		Return ador
		Cn.Close()
		ador.Clear()
		SqlCmd.Dispose()
		da.Dispose()
	End Function
	''' Revisado por: blp. Fecha: 12/07/2012
	''' <summary>
	''' FunciÃ³n que devuelve las lÃ­neas de una orden concreta
	''' </summary>
	''' <param name="OrdenId">Identificaor de la Orden de la cual se quieren devolver las lineas</param>
	''' <param name="Idioma">Idioma en el cual se cargaran los datos</param>
	''' <param name="bSinImputacion">Si todavia no hay imputaciÃ³n esta consulta no saca nada. Forzar con un left join q salga al menos la linea.</param>  
	''' <returns>Un DataSet con los datos de las lÃ­neas de la orden pasada como parÃ¡metro</returns>
	''' <remarks>Llamada dese:El protecto EPNotificador, clase Notificador, mÃ©todo CargarLineasDeOrden
	''' Tiempo mÃ¡ximo ? seg</remarks>
	Public Function Notificador_CargarLineasDeOrden(ByVal OrdenId As Integer, ByVal Seguridad_Catn As Integer, ByVal Seguridad_Nivel As Byte, ByVal Idioma As String, Optional ByVal bAccesoSM As Boolean = False, Optional ByVal bSinImputacion As Boolean = False, Optional ByVal sDonde As String = "") As DataSet
		Authenticate()

		Dim SQL As String
		Dim sqlCmd As New SqlCommand
		Dim cn As New SqlConnection(mDBConnection)
		Dim SqlParam As New SqlParameter
		Dim da As New SqlDataAdapter
		Dim Ds As New DataSet
		Dim bMostrarBajas As Boolean = False

		Select Case sDonde
			Case "NotificarOrdenBorrada", "NotificarLineaBorrada", "NotificarOrdenDeshacerBorrado"
				bMostrarBajas = True
		End Select
		SqlParam = sqlCmd.Parameters.AddWithValue("@ORDENID", OrdenId)

		If Seguridad_Catn > 0 Then
			SqlParam = sqlCmd.Parameters.AddWithValue("@SEGURIDAD_CATN", Seguridad_Catn)
			SqlParam = sqlCmd.Parameters.AddWithValue("@SEGURIDAD_NIVEL", Seguridad_Nivel)
		End If
		SqlParam = sqlCmd.Parameters.AddWithValue("@IDIOMA", Idioma)
		SQL = "SET CONCAT_NULL_YIELDS_NULL OFF " & vbCrLf & "SELECT DISTINCT LP.ID LINEAID, LP.ART_INT CODART,  " &
			 "       LP.PROVE, ISNULL(LP.DESCR_LIBRE,I.DESCR) DENART, PA.COD_EXT ART_EXT, " &
			 "       LP.DEST DESTINO, D.DEN_" & Idioma &
			 " DESTDEN, LP.UP UNIDAD, UD.DEN" &
			 " UNIDEN, LP.PREC_UP PRECIOUNITARIO, " &
			 "       LP.CANT_PED CANTIDAD, LP.CANT_REC CANT_REC, IA.CANT_ADJ * LP.FC AS CANTADJUDICADA, LP.FC, " &
			 "       LP.PREC_UP * LP.CANT_PED IMPORTE, LP.FECEMISION FECHA, LP.FECENTREGA, " &
			 "       LP.EST, LP.ENTREGA_OBL, LP.FECENTREGAPROVE, LP.CAT1, LP.CAT2, LP.CAT3, LP.CAT4, LP.CAT5, " &
			 "       LP.OBS, LP.TIPORECEPCION,LP.IMPORTE_PED, D.DIR, D.POB, D.CP, PROVI_DEN.DEN PROVINCIA, PAI_DEN.DEN PAIS, "

		If bAccesoSM Then
			SQL = SQL & "LPI.UON1, LPI.UON2, LPI.UON3, LPI.UON4, LPI.PRES0, LPI.PRES1, LPI.PRES2, LPI.PRES3 , LPI.PRES4, PRES5_IDIOMAS.DEN PARTIDA_NOM, "
		End If

		SQL = SQL & "       case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.cod else c2.cod end else c3.cod end else c4.cod end else c5.cod end CATEGORIA, " & "       case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then '(' + c1.cod + ') ' + c1.den else '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den end else '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den end else '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den end else '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den end CATEGORIARAMA, " &
		 "       C1.DEN CAT1DEN, C2.DEN CAT2DEN, C3.DEN CAT3DEN, C4.DEN CAT4DEN, C5.DEN CAT5DEN, " &
		 "       LE.COMENT, I.ANYO ANYOPROCE, I.GMN1_PROCE, I.PROCE CODPROCE, I.ID ITEM,  " &
		 "       LP.VALOR1,LP.VALOR2,CC1.DEN AS DENCAMPO1,CC2.DEN AS DENCAMPO2," &
		 "       PROCE.DEN AS DENPROCE,LP.OBSADJUN, LP.NUM NUMLINEA, LP.FECENTREGA, PA.COD_EXT " & vbCrLf
		If sDonde = "NotificarLineaBorrada" Then
			SQL = SQL & ", NBL.BAJA LINEA_DE_BAJA, NBL.FEC_ACCION "
		End If
		SQL = SQL & " FROM LINEAS_PEDIDO LP WITH(NOLOCK)" & vbCrLf
		SQL = SQL & "     INNER JOIN DEST D WITH(NOLOCK)" & vbCrLf
		SQL = SQL & "             ON LP.DEST = D.COD" & vbCrLf
		SQL = SQL & "     INNER JOIN UNI U WITH(NOLOCK)" & vbCrLf
		SQL = SQL & "     LEFT JOIN UNI_DEN UD WITH (NOLOCK) ON UD.UNI = U.COD AND UD.IDIOMA = @IDIOMA" & vbCrLf
		SQL = SQL & "             ON LP.UP = U.COD" & vbCrLf
		SQL = SQL & "      LEFT JOIN ITEM I WITH(NOLOCK) " & vbCrLf
		SQL = SQL & "             ON LP.ANYO = I.ANYO" & vbCrLf
		SQL = SQL & "            AND LP.GMN1=I.GMN1_PROCE " & vbCrLf
		SQL = SQL & "            AND LP.PROCE=I.PROCE " & vbCrLf
		SQL = SQL & "            AND LP.ITEM=I.ID" & vbCrLf
		SQL = SQL & "      LEFT JOIN PROVE_ART4 PA WITH(NOLOCK)" & vbCrLf
		SQL = SQL & "             ON LP.PROVE = PA.PROVE" & vbCrLf
		SQL = SQL & "            AND LP.ART_INT = PA.ART" & vbCrLf
		SQL = SQL & "      LEFT JOIN ITEM_ADJ IA WITH(NOLOCK) " & vbCrLf
		SQL = SQL & "             ON LP.ITEM = IA.ITEM " & vbCrLf
		SQL = SQL & "            AND LP.ANYO = IA.ANYO" & vbCrLf
		SQL = SQL & "            AND LP.GMN1 = IA.GMN1 " & vbCrLf
		SQL = SQL & "            AND LP.PROCE= IA.PROCE " & vbCrLf
		SQL = SQL & "            AND LP.PROVE= IA.PROVE" & vbCrLf
		SQL = SQL & "      LEFT JOIN CATN1 C1 WITH(NOLOCK) " & vbCrLf
		SQL = SQL & "             ON LP.CAT1 = C1.ID" & vbCrLf
		SQL = SQL & "      LEFT JOIN CATN2 C2 WITH(NOLOCK) " & vbCrLf
		SQL = SQL & "             ON LP.CAT2 = C2.ID" & vbCrLf
		SQL = SQL & "      LEFT JOIN CATN3 C3 WITH(NOLOCK) " & vbCrLf
		SQL = SQL & "             ON LP.CAT3 = C3.ID" & vbCrLf
		SQL = SQL & "      LEFT JOIN CATN4 C4 WITH(NOLOCK) " & vbCrLf
		SQL = SQL & "             ON LP.CAT4 = C4.ID" & vbCrLf
		SQL = SQL & "      LEFT JOIN CATN5 C5 WITH(NOLOCK) " & vbCrLf
		SQL = SQL & "             ON LP.CAT5 = C5.ID" & vbCrLf
		SQL = SQL & "      LEFT JOIN LINEAS_EST LE WITH(NOLOCK) " & vbCrLf
		SQL = SQL & "             ON LP.LINEAS_EST = LE.ID" & vbCrLf
		SQL = SQL & "      LEFT JOIN CATALOG_CAMPOS AS CC1 WITH(NOLOCK) " & vbCrLf
		SQL = SQL & "       ON CC1.ID=LP.CAMPO1 AND CC1.IDIOMA=@IDIOMA" & vbCrLf
		SQL = SQL & "      LEFT JOIN CATALOG_CAMPOS AS CC2 WITH(NOLOCK) " & vbCrLf
		SQL = SQL & "            ON CC2.ID=LP.CAMPO2 AND CC2.IDIOMA=@IDIOMA" & vbCrLf
		SQL = SQL & "      LEFT JOIN PROCE WITH(NOLOCK) " & vbCrLf
		SQL = SQL & "       ON LP.ANYO=PROCE.ANYO AND LP.GMN1=PROCE.GMN1 AND LP.PROCE=PROCE.COD " & vbCrLf


		SQL = SQL & "LEFT JOIN PAI_DEN With(NOLOCK) " &
			  "On D.PAI=PAI_DEN.PAI And PAI_DEN.IDIOMA = @IDIOMA " &
			 "LEFT JOIN PROVI_DEN WITH(NOLOCK) " &
			  "ON D.PROVI=PROVI_DEN.PROVI AND D.PAI=PROVI_DEN.PAI AND PROVI_DEN.IDIOMA = @IDIOMA" & vbCrLf


		If bAccesoSM Then
			SQL = SQL & " LEFT JOIN LINEAS_PED_IMPUTACION LPI WITH(NOLOCK) " & vbCrLf
			SQL = SQL & "             ON LPI.LINEA = LP.ID" & vbCrLf
			SQL = SQL & " LEFT JOIN PRES5_IDIOMAS WITH(NOLOCK) " & vbCrLf
			SQL = SQL & "             ON PRES5_IDIOMAS.IDIOMA=@IDIOMA AND ISNULL(PRES5_IDIOMAS.PRES0,'') =ISNULL(LPI.PRES0,'') AND ISNULL(PRES5_IDIOMAS.PRES1,'')=ISNULL(LPI.PRES1,'') AND isnull(PRES5_IDIOMAS.PRES2,'') =isnull(LPI.PRES2,'') AND ISNULL(PRES5_IDIOMAS.PRES3,'')=ISNULL(LPI.PRES3,'') AND ISNULL(PRES5_IDIOMAS.PRES4,'')=ISNULL(LPI.PRES4,'')" & vbCrLf
		End If

		If sDonde = "NotificarLineaBorrada" Then
			SQL = SQL & " INNER JOIN NOTIFICAR_BORRADO_LOGICO NBL WITH(NOLOCK) on NBL.LINEA_PEDIDO =lp.id AND NBL.MARCA=0 "
		End If
		SQL = SQL & " WHERE LP.ORDEN = @ORDENID " & vbCrLf
		If Not bMostrarBajas Then
			SQL = SQL & " AND LP.BAJA_LOG=0 " & vbCrLf
		End If

		If Seguridad_Catn > 0 Then
			SQL = SQL & "AND  LP.SEGURIDAD_CATN= @SEGURIDAD_CATN AND  LP.SEGURIDAD_NIVEL= @SEGURIDAD_NIVEL " & vbCrLf
		End If
		If sDonde = "NotificarLineaBorrada" Then
			SQL = SQL & " ORDER BY NBL.FEC_ACCION,LP.NUM " & vbCrLf
		Else
			SQL = SQL & " ORDER BY LP.NUM " & vbCrLf
		End If

		If sDonde = "NotificarLineaBorrada" Then
			SQL = SQL & " UPDATE NOTIFICAR_BORRADO_LOGICO SET MARCA = 1 WHERE ORDEN = @ORDENID AND FEC_NOTIF IS NOT NULL AND MARCA = 0 " & vbCrLf
		End If

		SQL = SQL & " SET CONCAT_NULL_YIELDS_NULL ON"
		sqlCmd.CommandType = CommandType.Text
		sqlCmd.Connection = cn
		cn.Open()
		sqlCmd.CommandText = SQL
		da.SelectCommand = sqlCmd
		da.Fill(Ds)
		Return Ds
		cn.Close()
		sqlCmd.Dispose()
		da.Dispose()
		Ds.Clear()
	End Function
	''' <summary>
	''' Devuelve una tabla con las lineas que han de ser notificadas porque hayan sido borradas o se ha deshecho el borrado de las mismas y los datos necesarios para generar las notificaciones
	''' </summary>
	''' <returns></returns>
	Public Function Ordenes_DevolverLineasBorradasANotificar() As DataTable
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dt As New DataTable
		Using cn
			cn.Open()

			cm = New SqlCommand
			cm.Connection = cn

			cm.CommandText = "FSEP_DEVOLVER_NOTIFICAR_BORRADO_LOGICO_ORDENES"
			cm.CommandType = CommandType.StoredProcedure

			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dt)
			Return dt
		End Using
	End Function
	''' <summary>
	''' FunciÃ³n que devuelve en un dataset los datos de un aprovisionador concreto dando su nÂ¡
	''' </summary>
	''' <param name="OrdenId"></param>
	''' <returns>Un DataSet con todos los datos de un aprovisionador</returns>
	''' <remarks>
	''' Llamada desde: proyecto EPDataBaseServer, clase Notificador, mÃ©todo CargarDatosAprovisionador
	''' Tiempo mÃ¡ximo: ? seg</remarks>
	Public Function Notificador_CargarDatosAprovisionador(ByVal OrdenId As Integer) As DataSet
		Authenticate()

		Dim sConsulta As String
		Dim ador As New DataSet
		Dim SqlCmd As New SqlCommand
		Dim SqlParam As New SqlParameter
		Dim Da As New SqlDataAdapter
		Dim cn As New SqlConnection(mDBConnection)
		Try
			SqlParam = SqlCmd.Parameters.AddWithValue("@ORDENID", OrdenId)
			sConsulta = "SELECT USU.MON,MON.EQUIV,USU.DATEFMT,USU.THOUSANFMT,USU.PRECISIONFMT,USU.DECIMALFMT,USU.IDIOMA," _
				& " PER.EMAIL,USU.TIPOEMAIL,PER.NOM,PER.APE " _
				& " FROM USU WITH(NOLOCK)" _
				& " INNER JOIN PER WITH(NOLOCK)" _
				& " ON USU.PER=PER.COD " _
				& " INNER JOIN ORDEN_ENTREGA WITH(NOLOCK)" _
				& " ON ORDEN_ENTREGA.ID=@ORDENID INNER JOIN PEDIDO WITH(NOLOCK)" _
				& " ON PEDIDO.ID=ORDEN_ENTREGA.PEDIDO " _
				& " AND PEDIDO.PER=PER.COD" _
				& " LEFT JOIN MON WITH(NOLOCK) ON USU.MON=MON.COD"
			SqlCmd.CommandType = CommandType.Text
			SqlCmd.CommandText = sConsulta
			SqlCmd.Connection = cn
			cn.Open()
			Da.SelectCommand = SqlCmd
			Da.Fill(ador)
			cn.Close()
			cn.Dispose()
			SqlCmd.Dispose()
			Da.Dispose()
			Return ador
		Catch ex As Exception
			Throw ex
		Finally
			cn.Dispose()
			SqlCmd.Dispose()
			Da.Dispose()
		End Try
	End Function
	''' <summary>
	''' Funcion que devuelve los datos del aprobador
	''' </summary>
	''' <param name="sCodAprob">codigo de usuario del aprobador</param>
	''' <returns>rerorna el dataset con los datos del aprobador</returns>
	''' <remarks></remarks>
	Public Function Notificador_CargarAprobador(ByVal sCodAprob As String) As DataSet
		Authenticate()

		Dim sConsulta As String
		Dim ador As New DataSet
		Dim SqlCmd As New SqlCommand
		Dim SqlParam As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		Dim da As New SqlDataAdapter

		SqlParam = SqlCmd.Parameters.AddWithValue("@APROB", sCodAprob)
		sConsulta = "SELECT PER.COD AS PER,PER.APE,PER.NOM,PER.CAR,PER.TFNO" & ",PER.FAX,PER.DEP,PER.TFNO2" & ",USU.MON,MON.EQUIV,USU.DATEFMT,USU.DECIMALFMT,USU.THOUSANFMT,USU.PRECISIONFMT," & " USU.IDIOMA,PER.EMAIL,USU.TIPOEMAIL,USU.COD CODUSU FROM USU WITH(NOLOCK) INNER JOIN PER WITH(NOLOCK) ON USU.PER=PER.COD LEFT JOIN MON WITH(NOLOCK) ON USU.MON=MON.COD WHERE USU.COD=@APROB"

		SqlCmd.CommandType = CommandType.Text
		SqlCmd.CommandText = sConsulta
		SqlCmd.Connection = cn
		cn.Open()
		da.SelectCommand = SqlCmd
		da.Fill(ador)
		Return ador
		cn.Close()
		ador.Clear()
		SqlCmd.Dispose()
		da.Dispose()
	End Function
	''' <summary>
	''' FunciÃ³n que devuelve en un DataSet los datos de los aprobadores de tipo 1
	''' </summary>
	''' <param name="Pedido">Identificador del pedido del cual queremos saber los datos de los aprobadores</param>
	''' <returns>Un DataSet con los datos de todos los aprobadores de un pedido</returns>
	''' <remarks>
	''' Llamada desde: Proyecto EPNotificador, clase Notificador, mÃ©todo CargarAprobadoresTipo1
	''' Tiempo mÃ¡ximo: ? seg</remarks>
	Public Function Notificador_CargarAprobadoresTipo1(ByVal Pedido As Integer) As DataSet
		Authenticate()

		Dim sConsulta As String
		Dim Dset As New DataSet
		Dim SqlCmd As New SqlCommand
		Dim cn As New SqlConnection(mDBConnection)
		Dim SqlParam As New SqlParameter
		Dim da As New SqlDataAdapter
		SqlParam = SqlCmd.Parameters.AddWithValue("@PEDIDO", Pedido)
		sConsulta = "SELECT DISTINCT U.MON,MON.EQUIV,U.DATEFMT,U.DECIMALFMT,U.THOUSANFMT,U.PRECISIONFMT,U.IDIOMA,U.TIPOEMAIL,P.COD PER,P.EMAIL,P.NOM,P.APE,P.TFNO,P.TFNO2,P.FAX,P.CAR,P.DEP,LP.ANYO,LP.ORDEN,LP.SEGURIDAD_CATN,LP.SEGURIDAD_NIVEL,U.COD CODUSU"
		sConsulta = sConsulta & " FROM LINEAS_PEDIDO LP WITH(NOLOCK)  "
		sConsulta = sConsulta & " INNER JOIN USU U WITH(NOLOCK) ON U.PER=LP.APROBADOR INNER JOIN PER P WITH(NOLOCK) ON P.COD=U.PER LEFT JOIN MON WITH (NOLOCK) ON MON.COD=U.MON  "
		sConsulta = sConsulta & " WHERE NIVEL_APROB=0 AND LP.PEDIDO=@PEDIDO"

		SqlCmd.CommandType = CommandType.Text
		SqlCmd.CommandText = sConsulta
		SqlCmd.Connection = cn
		cn.Open()
		da.SelectCommand = SqlCmd
		da.Fill(Dset)
		Return Dset
		Dset.Clear()
		cn.Close()
		SqlCmd.Dispose()
		da.Dispose()
	End Function
	''' <summary>
	''' FunciÃ³n que carga en un DataSet los usuarios notificados de tipo 1, que son aquellos que se marcan en la configuracion de la seguridad de la categoria, en la pestaÃ±a de limite de adjudicacion
	''' </summary>
	''' <param name="Pedido">Id del pedido del cual queremos cargar los datos</param>
	''' <returns>Un dataSet con los datos de los notificados de tipo 1</returns>
	''' <remarks>Llamada desde: El proyecto EPNotificador, clase Notificador, mÃ©todo CargarNotificadosTipo1
	''' Tiempo mÃ¡ximo: ? seg</remarks>
	Public Function Notificador_CargarNotificadosTipo1(ByVal Pedido) As DataSet
		Authenticate()

		Dim sConsulta As String
		Dim ador As New DataSet
		Dim SqlCmd As New SqlCommand
		Dim SqlParam As New SqlParameter
		Dim Cn As New SqlConnection(mDBConnection)
		Dim Da As New SqlDataAdapter

		SqlParam = SqlCmd.Parameters.AddWithValue("@PEDIDO", Pedido)
		sConsulta = "SELECT DISTINCT U.MON, MON.EQUIV, U.IDIOMA, COALESCE(U.TIPOEMAIL, PAR.TIPOMAIL) AS TIPOEMAIL, " &
		 " P.EMAIL, P.NOM, P.APE, LP.ANYO, LP.PEDIDO, LP.ORDEN, LP.SEGURIDAD_CATN,LP.SEGURIDAD_NIVEL" &
		 " FROM LINEAS_PEDIDO LP WITH(NOLOCK) " &
		 " INNER JOIN NOTIF_CATN N WITH(NOLOCK) ON N.CATN=LP.SEGURIDAD_CATN AND N.NIVEL=LP.SEGURIDAD_NIVEL" &
		 " INNER JOIN PER P WITH(NOLOCK) ON P.COD=N.NOTIF" &
		 " INNER JOIN PARGEN_MAIL PAR WITH(NOLOCK) ON PAR.ID=1" &
		 " LEFT JOIN USU U WITH(NOLOCK) ON U.PER=P.COD" &
		 " LEFT JOIN MON WITH(NOLOCK) ON MON.COD=U.MON" &
		 " WHERE LP.NIVEL_APROB=0 AND LP.PEDIDO=@PEDIDO"
		SqlCmd.CommandType = CommandType.Text
		SqlCmd.CommandText = sConsulta
		SqlCmd.Connection = Cn
		Cn.Open()
		Da.SelectCommand = SqlCmd
		Da.Fill(ador)
		Return ador
		Cn.Close()
		ador.Clear()
		Da.Dispose()
		SqlCmd.Dispose()
	End Function
	''' <summary>
	''' Función que devuelve los datos de un pedido recepcionado
	''' </summary>
	''' <param name="RecepId">Identificador de la recepción que se quiere devolver</param>
	''' <returns>Un DataSet con los datos de la recepción buscada</returns>
	''' <remarks>
	''' Llamada desde: El proyecto EPNotificador, clase Notificador, método DevolverDatosPedidoRecep</remarks>
	Public Function Notificador_DevolverDatosPedidoRecep(ByVal RecepId As String) As DataSet
		Authenticate()

		Dim sConsulta As String
		sConsulta = "SELECT FECHA, ALBARAN, COMENT FROM PEDIDO_RECEP WITH(NOLOCK) WHERE ID=@RECEPID"
		Dim AdoCom As New SqlCommand
		Dim Cn As New SqlConnection(mDBConnection)
		Dim SqlParam As New SqlParameter
		Dim da As New SqlDataAdapter
		Dim Dset As New DataSet
		AdoCom.Connection = Cn
		AdoCom.CommandType = CommandType.Text
		AdoCom.CommandText = sConsulta
		SqlParam = AdoCom.Parameters.AddWithValue("@RecepId", RecepId)
		Cn.Open()
		da.SelectCommand = AdoCom
		da.Fill(Dset)
		Return Dset
		Cn.Close()
		Dset.Clear()
		AdoCom.Dispose()
		da.Dispose()
	End Function
	''' Revisado por: blp. Fecha: 12/07/2012
	''' <summary>
	''' Función que devuelve los datos de una Notificación Recepcionada correctamente
	''' </summary>
	''' <param name="sIdioma">Idioma actual de la aplicación</param>
	''' <param name="RecepId">Identificador de la recepción de la cual queremos saber los datos</param>
	''' <returns>Un dataset con los datos de la recepción filtrada</returns>
	''' <remarks>
	''' Llamada desde: el proyecto EPNotificador, clase Notificador, método NotificaciónRecepCorrecta
	''' Tiempo máximo: ? seg</remarks>
	Public Function Notificador_NotificacionRecepCorrecta(ByVal sIdioma As String, ByVal RecepId As Integer) As DataSet
		Authenticate()

		Dim sConsulta As String
		Dim SqlCmd As New SqlCommand
		Dim SqlParam As New SqlParameter
		Dim Dset As New DataSet
		Dim Cn As New SqlConnection(mDBConnection)
		Dim da As New SqlDataAdapter
		Using Cn
			Cn.Open()
			SqlCmd.Connection = Cn
			SqlCmd.CommandType = CommandType.Text
			SqlCmd.CommandText = "SET CONCAT_NULL_YIELDS_NULL OFF"
			SqlCmd.ExecuteNonQuery()

			sConsulta = "SELECT DISTINCT LP.ID LINEAID, "
			sConsulta = sConsulta & "                LP.ART_INT CODART, "
			sConsulta = sConsulta & "                ISNULL(LP.DESCR_LIBRE,I.DESCR) DENART, "
			sConsulta = sConsulta & "                PA.ART ART_EXT, "
			sConsulta = sConsulta & "                LP.DEST DESTINO, "
			sConsulta = sConsulta & "                D.DEN_" & sIdioma & " DESTDEN, "
			sConsulta = sConsulta & "                LP.UP UNIDAD, "
			sConsulta = sConsulta & "                UD.DEN UNIDEN, "
			sConsulta = sConsulta & "                LP.PREC_UP PRECIOUNITARIO, "
			sConsulta = sConsulta & "                LP.CANT_PED CANTIDAD, "
			sConsulta = sConsulta & "                LP.CANT_REC CANT_REC, "
			sConsulta = sConsulta & "                LP.TIPORECEPCION ,LP.IMPORTE_PED, "
			sConsulta = sConsulta & "                LR.CANT CANT_RECACT, "
			sConsulta = sConsulta & "                IA.CANT_ADJ * LP.FC AS CANTADJUDICADA, "
			sConsulta = sConsulta & "                LP.PREC_UP * LP.CANT_PED IMPORTE, "
			sConsulta = sConsulta & "                LP.EST AS APROBADA, "
			sConsulta = sConsulta & "                LP.FECEMISION FECHA, "
			sConsulta = sConsulta & "                LP.FECENTREGA FECENTREGA, "
			sConsulta = sConsulta & "                LP.EST,LP.ENTREGA_OBL,LP.FECENTREGA, "
			sConsulta = sConsulta & "                case when c5.cod is null then  "
			sConsulta = sConsulta & "                        case when c4.cod is null then  "
			sConsulta = sConsulta & "                             case when c3.cod is null then "
			sConsulta = sConsulta & "                                  case when c2.cod is null then "
			sConsulta = sConsulta & "                                       c1.cod "
			sConsulta = sConsulta & "                                  else "
			sConsulta = sConsulta & "                                  c2.cod "
			sConsulta = sConsulta & "                                  end "
			sConsulta = sConsulta & "                             else "
			sConsulta = sConsulta & "                             c3.cod "
			sConsulta = sConsulta & "                             end "
			sConsulta = sConsulta & "                        else "
			sConsulta = sConsulta & "                        c4.cod "
			sConsulta = sConsulta & "                        end "
			sConsulta = sConsulta & "                    else "
			sConsulta = sConsulta & "                    c5.cod "
			sConsulta = sConsulta & "                    end CATEGORIA, "
			sConsulta = sConsulta & "                case when c5.cod is null then  "
			sConsulta = sConsulta & "                        case when c4.cod is null then  "
			sConsulta = sConsulta & "                             case when c3.cod is null then "
			sConsulta = sConsulta & "                                  case when c2.cod is null then "
			sConsulta = sConsulta & "                                       '(' + c1.cod + ') ' + c1.den "
			sConsulta = sConsulta & "                                  else "
			sConsulta = sConsulta & "                                  '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den "
			sConsulta = sConsulta & "                                  end "
			sConsulta = sConsulta & "                             else "
			sConsulta = sConsulta & "                             '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den "
			sConsulta = sConsulta & "                             end "
			sConsulta = sConsulta & "                        else "
			sConsulta = sConsulta & "                        '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den "
			sConsulta = sConsulta & "                        end "
			sConsulta = sConsulta & "                    else "
			sConsulta = sConsulta & "                    '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den "
			sConsulta = sConsulta & "                    end CATEGORIARAMA, "
			sConsulta = sConsulta & "                C1.DEN CAT1DEN, "
			sConsulta = sConsulta & "                C2.DEN CAT2DEN, "
			sConsulta = sConsulta & "                C3.DEN CAT3DEN, "
			sConsulta = sConsulta & "                C4.DEN CAT4DEN, "
			sConsulta = sConsulta & "                C5.DEN CAT5DEN, "
			sConsulta = sConsulta & "                LE.COMENT, LP.NUM NUMLINEA "
			sConsulta = sConsulta & " FROM LINEAS_RECEP LR WITH(NOLOCK)  "
			sConsulta = sConsulta & "     INNER JOIN LINEAS_PEDIDO LP WITH(NOLOCK)"
			sConsulta = sConsulta & "             ON LR.LINEA = LP.ID "
			sConsulta = sConsulta & "     INNER JOIN DEST D WITH(NOLOCK) "
			sConsulta = sConsulta & "             ON LP.DEST = D.COD "
			sConsulta = sConsulta & "     INNER JOIN UNI U WITH(NOLOCK) "
			sConsulta = sConsulta & "     LEFT JOIN UNI_DEN UD WITH (NOLOCK) ON UD.UNI = U.COD AND UD.IDIOMA = @IDIOMA "
			sConsulta = sConsulta & "             ON LP.UP = U.COD "
			sConsulta = sConsulta & "      LEFT JOIN ITEM I  WITH(NOLOCK) "
			sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO "
			sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1_PROCE  "
			sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE  "
			sConsulta = sConsulta & "            AND LP.ITEM=I.ID "
			sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA WITH(NOLOCK) "
			sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE "
			sConsulta = sConsulta & "            AND LP.ART_INT = PA.ART "
			sConsulta = sConsulta & "      LEFT JOIN ITEM_ADJ IA  WITH(NOLOCK) "
			sConsulta = sConsulta & "             ON LP.ITEM = IA.ITEM  "
			sConsulta = sConsulta & "            AND LP.ANYO = IA.ANYO "
			sConsulta = sConsulta & "            AND LP.GMN1 = IA.GMN1  "
			sConsulta = sConsulta & "            AND LP.PROCE= IA.PROCE  "
			sConsulta = sConsulta & "            AND LP.PROVE= IA.PROVE "
			sConsulta = sConsulta & "      LEFT JOIN CATN1 C1 WITH(NOLOCK) "
			sConsulta = sConsulta & "             ON LP.CAT1 = C1.ID "
			sConsulta = sConsulta & "      LEFT JOIN CATN2 C2 WITH(NOLOCK) "
			sConsulta = sConsulta & "             ON LP.CAT2 = C2.ID "
			sConsulta = sConsulta & "      LEFT JOIN CATN3 C3 WITH(NOLOCK) "
			sConsulta = sConsulta & "             ON LP.CAT3 = C3.ID "
			sConsulta = sConsulta & "      LEFT JOIN CATN4 C4 WITH(NOLOCK) "
			sConsulta = sConsulta & "             ON LP.CAT4 = C4.ID "
			sConsulta = sConsulta & "      LEFT JOIN CATN5 C5 WITH(NOLOCK) "
			sConsulta = sConsulta & "             ON LP.CAT5 = C5.ID "
			sConsulta = sConsulta & "      LEFT JOIN LINEAS_EST LE WITH(NOLOCK) "
			sConsulta = sConsulta & "             ON LP.LINEAS_EST = LE.ID "
			sConsulta = sConsulta & " WHERE LR.PEDIDO_RECEP = @RECEPID"
			SqlParam = SqlCmd.Parameters.AddWithValue("@RECEPID", RecepId)
			SqlParam = SqlCmd.Parameters.AddWithValue("@IDIOMA", sIdioma)
			SqlCmd.CommandText = sConsulta

			da.SelectCommand = SqlCmd
			da.Fill(Dset)
			SqlCmd.Parameters.Clear()

			SqlCmd.CommandText = "SET CONCAT_NULL_YIELDS_NULL ON"
			SqlCmd.ExecuteNonQuery()

			Return Dset
		End Using
	End Function
#End Region
#Region " Orden data access methods"
	<Flags()>
	Public Enum EstadoLineaPedido As Short
		PendienteAnyadir = -1
		PendienteAprobar = 0
		Aprobada = 1
		RecibidoParcialmente = 2
		RecibidoEnSuTotalidad = 3
		Denegada = 20
		RechazadaProveedor = 21
		PendienteValidar = 22
		EnEsperaAprobacionLimiteAdjudicacion = 23
	End Enum
	''' <summary>
	''' Función que nos devolverá los archivos adjuntos para  una orden de entrega específica
	''' </summary>
	''' <param name="Orden">Id de la orden de entrega</param>
	''' <returns>El dataset con los adjuntos para esta orden de entrega</returns>
	''' <remarks>
	''' Llamada desde: COrden.CargarAdjuntos
	''' Tiempo máximo: 0 sec</remarks>
	Public Function Orden_CargarAdjuntos(ByVal Orden As Integer) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet

		Using cn
			cn.Open()
			cm = New SqlCommand
			cm.Connection = cn
			cm.CommandText = "FSEP_DEVOLVER_ADJUNTOS_ORDEN"
			cm.CommandType = CommandType.StoredProcedure

			cm.Parameters.AddWithValue("@ORDEN", Orden)

			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)

			Return dr
		End Using
	End Function
	''' <summary>
	''' Funcion que comprueba si el numero aleatorio que se pasa al Web service coincide y con el del pedido
	''' </summary>
	''' <param name="iOrdenId">id de la orden</param>
	''' <param name="iNumRnd">numero aleatorio que es pasado al web service desde el link del mail de aprobacion/rechazo de pedido</param>
	''' <returns>retorna del id del pedido</returns>
	''' <remarks></remarks>
	Public Function Orden_ValidarNumRnd(ByVal iOrdenId As Integer, ByVal iNumRnd As Integer) As String
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand

		cn.Open()

		cm = New SqlCommand
		cm.Connection = cn

		cm.CommandText = "SELECT CONVERT(NVARCHAR(5),P.ANYO) + '/' + CONVERT(NVARCHAR(10),P.NUM)+ '/' +CONVERT(NVARCHAR(10),OE.NUM) AS PEDIDO FROM LINEAS_PEDIDO LP WITH(NOLOCK) INNER JOIN PEDIDO P WITH(NOLOCK) ON P.ID=LP.PEDIDO  INNER JOIN ORDEN_ENTREGA OE WITH(NOLOCK) ON OE.PEDIDO =P.ID WHERE LP.ORDEN=@ORDENID AND LP.NUM_RANDOM=@NUMRND"
		cm.CommandType = CommandType.Text
		cm.Parameters.AddWithValue("@ORDENID", iOrdenId)
		cm.Parameters.AddWithValue("@NUMRND", iNumRnd)

		Return DBNullToStr(cm.ExecuteScalar)
	End Function
	''' <summary>Inserta un registro de comunicación de una orden</summary>
	''' <param name="IdOrden">Codigo de la orden de entrega</param>
	''' <param name="IdPedido">Codigo del pedido</param>
	''' <remarks>Llamada desde: CEmisionPedidos.NotificarAProveedores</remarks>
	Public Sub Orden_TratarComunicion(ByVal IdOrden As Integer, ByVal IdPedido As Integer, ByVal Notificado As Short)
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_TRATAR_COMUNICACION_ORDEN"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.Add("@ORDEN", SqlDbType.Int)
			cm.Parameters("@ORDEN").Value = IdOrden
			cm.Parameters.Add("@PEDIDO", SqlDbType.Int)
			cm.Parameters("@PEDIDO").Value = IdPedido
			cm.Parameters.Add("@NOTIFICADO", SqlDbType.SmallInt)
			cm.Parameters("@NOTIFICADO").Value = Notificado
			cm.ExecuteNonQuery()
		Catch ex As Exception
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Sub
	''' <summary>
	''' Funcion que devuelve la fecha de la orden
	''' </summary>
	''' <param name="IdOrden">Id de la orden</param>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Function Orden_ObtenerFecha(ByVal IdOrden As Long) As DateTime
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Dim dFechaOrden As Date
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "SELECT FECHA FROM ORDEN_EST WITH(NOLOCK) WHERE EST=0 AND ORDEN =@IDORDEN "
			cm.CommandType = CommandType.Text
			cm.Parameters.AddWithValue("@IDORDEN", IdOrden)
			dFechaOrden = cm.ExecuteScalar()
		Catch ex As Exception
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
		Return dFechaOrden
	End Function
	''' <summary>
	''' Función que devolverá un dataset con las líneas de pedido para una orden de entrega concreta.
	''' </summary>
	''' <param name="Id">Id de la orden de entrega</param>
	''' <param name="sIdioma">Código del idioma</param>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Function Orden_DevolverLineas(ByVal Id As Integer, ByVal sIdioma As Object) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet
		Using cn
			cn.Open()

			cm = New SqlCommand
			cm.Connection = cn

			cm.CommandText = "FSEP_DEVOLVER_LINEAS_DE_ORDEN"
			cm.CommandType = CommandType.StoredProcedure

			cm.Parameters.AddWithValue("ORDENID", Id)
			cm.Parameters.AddWithValue("IDI", sIdioma)

			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)
			Return dr
		End Using
	End Function
	''' <summary>
	''' Función que devolverá un dataset con los albaranes de una orden y las respectivas lineas del albaran.
	''' </summary>
	''' <param name="IdOrden">Integer. Id de la Orden del pedido</param>
	''' <returns>Dataset con los Albaranes y lineas de albaran de una Orden de pedido.</returns>
	''' <remarks></remarks>
	Public Function Orden_Albaranes(ByVal IdOrden As Integer, Optional ByVal sIdioma As String = "SPA") As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet
		Using cn
			cn.Open()

			cm = New SqlCommand
			cm.Connection = cn

			cm.CommandText = "FSEP_GET_ALBARANES"
			cm.CommandType = CommandType.StoredProcedure

			cm.Parameters.AddWithValue("@ID_ORDEN", IdOrden)
			cm.Parameters.AddWithValue("@IDIOMA", sIdioma)

			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)

			Return dr
		End Using
	End Function
	''' Revisado por: blp. Fecha: 20/07/2012
	''' <summary>
	''' Función que devolverá un dataset con las líneas de pedido para todas las órdenes pasadas por tabla
	''' </summary>
	''' <param name="Idioma">idioma</param>
	''' <param name="OrdenesId">Tipo Tabla definido por el usuario para pasar una tabla de IDs de órdenes al stored y filtrar por ellas</param>
	''' <param name="SMActivado">Determina si hay que mostrar las imputaciones presupuestarias o no</param>
	''' <returns>dataset con dos tablas: lineas e imputacion de las lineas</returns>
	''' <remarks>tiempo máx inferior a 0,5seg</remarks>
	Public Function Ordenes_DevolverLineas(ByVal Idioma As FSNLibrary.Idioma, ByVal OrdenesId As DataTable, ByVal SMActivado As Boolean) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet
		Using cn
			cn.Open()

			cm = New SqlCommand
			cm.Connection = cn

			cm.CommandText = "EP_ORDENES_DEVOLVER_LINEAS"
			cm.CommandType = CommandType.StoredProcedure

			If Not IsNothing(Idioma) And Idioma <> "" Then
				cm.Parameters.AddWithValue("IDI", Idioma.ToString)
			Else
				cm.Parameters.AddWithValue("IDI", "SPA")
			End If
			cm.Parameters.AddWithValue("SMACTIVADO", SMActivado)

			'Creamos un parámetro sql de tipo ORDENESID, que es un tipo "tabla" definido por el usuario en sql server
			Dim ParamOrdenesId As New SqlParameter("@ORDENESID", SqlDbType.Structured)
			ParamOrdenesId.TypeName = "ORDENESID"
			ParamOrdenesId.Value = OrdenesId
			cm.Parameters.Add(ParamOrdenesId)
			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)
			Return dr
		End Using
	End Function
	''' <summary>
	''' Comprueba si las líneas de una orden han sido modificadas después de una fecha determinada
	''' </summary>
	''' <param name="Id">ID de la orden de entrega</param>
	''' <param name="FechaModificacion">Fecha a partir de la cual se quiere realizar la comprobación</param>
	''' <returns>True si alguna de las líneas ha sido modificada, si no False</returns>
	''' <remarks></remarks>
	Function Orden_Modificada(ByVal Id As Integer, ByVal FechaModificacion As DateTime) As Boolean
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Using cn
			cn.Open()
			' Se omite el WITH (NOLOCK) puesto que la comprobación sirve para comprobar que los datos
			' recuperados se corresponden con los reales
			Dim cm As New SqlCommand("SELECT COUNT(*) FROM LINEAS_PEDIDO WHERE ORDEN=@ORDEN AND FECACT>@FECHA",
			   cn)
			cm.CommandType = CommandType.Text
			cm.Parameters.Add("@ORDEN", SqlDbType.Int)
			cm.Parameters.Add("@FECHA", SqlDbType.DateTime)
			cm.Parameters("@ORDEN").Value = Id
			cm.Parameters("@FECHA").Value = FechaModificacion
			Dim LinModif As Integer = cm.ExecuteScalar()
			Return (LinModif > 0)
		End Using
	End Function
	''' <summary>
	''' Función que devuelve un dataset con las UONs a las que el usuario no puede imputar presupuestos
	''' </summary>
	''' <param name="Id">Id de la orden de entrega</param>
	''' <param name="Aprovisionador">Codigo de la persona aprovisiondora</param>
	''' <returns>Un dataset con las UONs a las que el usuario no puede imputar presupuestos</returns>
	''' <remarks>
	''' Llamada desde: COrden.NoImputacion
	''' Tiempo máximo: 0 sec</remarks>
	Function Orden_NoImputacion(ByVal Id As Integer, ByVal Aprovisionador As String) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet
		Using cn
			cn.Open()

			cm = New SqlCommand
			cm.Connection = cn

			cm.CommandText = "FSEP_PRES5_NOIMPUTACION"
			cm.CommandType = CommandType.StoredProcedure

			cm.Parameters.AddWithValue("@PER", Aprovisionador)
			cm.Parameters.AddWithValue("@ORDEN", Id)

			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)
			Return dr
		End Using
	End Function
	''' <summary>
	''' Procedimiento que Reemite un pedido parcialmente denegado
	''' </summary>
	''' <param name="Id">Id de la orden de entrega</param>
	''' <param name="Aprovisionador">Código de la persona aprovisionadora</param>
	''' <param name="PedidoID">Número de cesta</param>
	''' <param name="InstWeb">Parámetro booleano que nos dirá si tenemos que actualizar la tabla REL_CIAS</param>
	''' <param name="Prove">Código de proveedor</param>
	''' <param name="PedidoDirectoEnvioEmail">Booleano que nos dirá si es un pedido directo</param>
	''' <param name="FSP_SRV">Servidor de la BBDD de Portal</param>
	''' <param name="FSP_BD">BBDD de Portal</param>
	''' <param name="FSP_CIA">Id de la compañía.</param>
	''' <remarks>
	''' Llamada desde: COrden.Reemitir
	''' Tiempo máximo: 1 sec.</remarks>
	Function Orden_Reemitir(ByVal Id As Integer, ByVal Aprovisionador As String, ByVal PedidoID As Integer, ByVal InstWeb As Short, ByVal Prove As String,
					   ByVal PedidoDirectoEnvioEmail As Boolean, ByVal FSP_SRV As String, ByVal FSP_BD As String, ByVal FSP_CIA As String) As Long
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim NotificadoProve As Long
		Using cn
			Try
				cn.Open()
				cm = New SqlCommand("BEGIN DISTRIBUTED TRANSACTION", cn)
				cm.ExecuteNonQuery()

				cm = New SqlCommand
				cm.Connection = cn
				cm.CommandText = "FSEP_REEMITIR_ORDEN"
				cm.CommandType = CommandType.StoredProcedure
				cm.Parameters.AddWithValue("@ORDEN", Id)
				cm.Parameters.AddWithValue("@APROVISIONADOR", Aprovisionador)
				cm.Parameters.Add("@NOTIFICADO", SqlDbType.Int).Direction = ParameterDirection.Output
				cm.ExecuteNonQuery()

				NotificadoProve = cm.Parameters("@NOTIFICADO").Value

				cm = New SqlCommand("COMMIT TRANSACTION", cn)
				cm.ExecuteNonQuery()

				Return NotificadoProve

			Catch e As SqlException
				cm = New SqlCommand("ROLLBACK TRANSACTION", cn)
				cm.ExecuteNonQuery()
				Throw e
			End Try
		End Using
	End Function
	''' <summary>
	''' Aprueba una orden de entrega
	''' </summary>
	''' <param name="OrdenId">ID de la orden de entrega</param>
	''' <param name="Aprobador">Código de persona del aprobador</param>
	''' <param name="PedidoDirectoEnvioEmail">True incluye todos los pedidos, False todos excepto directos</param>
	''' <param name="CodError">Parámetro de salida que devuelve el código de error</param>
	''' <remarks>Un dataset con dos tablas:
	''' Una tabla con los aprobadores a notificar en caso de traslado
	''' Una tabla con las líneas de catálogo en las que se ha actualizado el precio</remarks>
	Sub Orden_Aprobar(ByVal OrdenId As Integer, ByVal Aprobador As String, ByVal PedidoDirectoEnvioEmail As Boolean, ByRef CodError As Short)
		Dim cn As New SqlConnection(mDBConnection)
		Using cn
			cn.Open()
			Dim cm As New SqlCommand("FSEP_APROBAR_ORDEN", cn)
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@ORDEN", OrdenId)
			cm.Parameters.AddWithValue("@APROBADOR", Aprobador)
			cm.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
			Dim da As New SqlDataAdapter(cm)
			Dim ds As New DataSet
			da.Fill(ds)
			CodError = cm.Parameters("@ReturnValue").Value
		End Using
	End Sub
	''' <summary>
	''' Realiza las validaciones antes de emitir un pedido, validaciones de limite de adjudicacion y de limite de pedido
	''' </summary>
	''' <param name="idPedido">id del pedido</param>
	''' <param name="msAprov">aprovisionador del pedido</param>
	''' <param name="idTipoPedido">id del tipo de pedido </param>
	''' <param name="idEmp">id de la empresa</param>
	''' <param name="bSoloLimites">True si solo se ejecuta el stored de validacion de limites de pedido</param>
	''' <returns>dataset con las solicitudes de los flujos de aprobacion a iniciar</returns>
	''' <remarks></remarks>
	Public Function ValidacionesEmisionPedidoCatalogo(ByVal idPedido As Long, ByVal msAprov As String, ByVal idTipoPedido As Integer, ByVal idEmp As Long,
													  Optional ByVal bSoloLimites As Boolean = False, Optional ByRef EstadoOrden As Short = -1,
													  Optional ByVal iNotificar As OrdenEntregaNotificacion = 0) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Dim da As New SqlDataAdapter
		Dim ds As New DataSet
		Try
			cn.Open()
			cm.Connection = cn
			If Not bSoloLimites Then
				cm.CommandText = "FSEP_VALIDAR_ADJUDICACION"
				cm.CommandType = CommandType.StoredProcedure
				cm.Parameters.Add(New SqlParameter("@PEDIDO", SqlDbType.Int))
				cm.Parameters("@PEDIDO").Value = idPedido
				cm.ExecuteNonQuery()
			End If

			cm.CommandText = "FSEP_VALIDAR_LIMITES"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.Clear()
			cm.Parameters.Add(New SqlParameter("@PEDIDO", SqlDbType.Int))
			cm.Parameters("@PEDIDO").Value = idPedido
			If Not msAprov Is Nothing Then
				cm.Parameters.Add(New SqlParameter("@PER", SqlDbType.NVarChar))
				cm.Parameters("@PER").Value = msAprov
			End If
			cm.Parameters.Add(New SqlParameter("@ID_TIPOPEDIDO", SqlDbType.Int))
			cm.Parameters("@ID_TIPOPEDIDO").Value = idTipoPedido
			cm.Parameters.Add(New SqlParameter("@ID_EMP", SqlDbType.Int))
			cm.Parameters("@ID_EMP").Value = idEmp
			cm.Parameters.Add(New SqlParameter("@NOTIFICAR", SqlDbType.Int))
			cm.Parameters("@NOTIFICAR").Value = iNotificar
			cm.Parameters.Add("@ESTADO_ORDEN", SqlDbType.Int, 0)
			cm.Parameters("@ESTADO_ORDEN").Direction = ParameterDirection.Output
			da.SelectCommand = cm
			da.Fill(ds)
			EstadoOrden = cm.Parameters("@ESTADO_ORDEN").Value
			Return ds
		Catch ex As Exception
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Function
	''' <summary>
	''' Updatea las lineas de pedido grabandoles el id de la instancia de la solicitud de pedido(tipo 10) de aprobacion
	''' </summary>
	''' <param name="drSeguridad">fila con el id de la categoria y el nivel que tiene la seguridad</param>
	''' <param name="idPedido">id del pedido</param>
	''' <param name="Instancia">instancia de la solicitud del workflow de aprobacion del pedido</param>
	''' <remarks></remarks>
	Public Sub ActualizarLineasPedido_InstanciaSolicitudPedido(ByVal drSeguridad As DataRow, ByVal idPedido As Long,
					ByVal Instancia As Long, ByVal EsPedidoAbierto As Boolean)
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSPM_GRABAR_INSTANCIA_LINEAS_PEDIDO"
			cm.CommandType = CommandType.StoredProcedure
			If Not EsPedidoAbierto Then
				cm.Parameters.AddWithValue("@SEGURIDAD_CATN", drSeguridad("CAT"))
				cm.Parameters.AddWithValue("@SEGURIDAD_NIVEL", drSeguridad("NIVEL"))
			End If
			cm.Parameters.AddWithValue("@ID_PEDIDO", idPedido)
			cm.Parameters.AddWithValue("@INSTANCIA", Instancia)
			If EsPedidoAbierto Then cm.Parameters.AddWithValue("@ESPEDIDOABIERTO", EsPedidoAbierto)

			cm.ExecuteNonQuery()
		Catch ex As Exception
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Sub
	''' <summary>
	''' Devuelve el bloque de destino, el de origen y la accion para pasar desde el aprovisionador del pedido al aprobador
	''' </summary>
	''' <param name="idSolicitud">id de la solicitud</param>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Function ObtenerBloque_Accion(ByVal idSolicitud As Long) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Dim da As New SqlDataAdapter
		Dim ds As New DataSet
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSPM_DEVOLVER_BLOQUE_ACCION"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.Add(New SqlParameter("@ID_SOLICITUD", SqlDbType.Int))
			cm.Parameters("@ID_SOLICITUD").Value = idSolicitud
			da.SelectCommand = cm
			da.Fill(ds)

			Return ds
		Catch ex As Exception
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Function
	''' <summary>
	''' Deniega una orden de entrega
	''' </summary>
	''' <param name="OrdenId">ID de la orden de entrega</param>
	''' <param name="Aprobador">Código de persona del aprobador</param>
	''' <param name="Comentario">Comentario a incluir en las líneas denegadas</param>
	''' <param name="CodError">Parámetro de salida que devuelve el código de error</param>
	''' <param name="ArgError1">Parámetro de salida con información sobre el estado final de la orden</param>
	''' <param name="ArgError2">Parámetro de salida con información sobre el estado final de la orden</param>
	''' <param name="ArgError3">Parámetro de salida con información sobre el estado final de la orden</param>
	''' <param name="FuerzaDenegarTotal">1-> Desde Aprobacion.aspx. Denegara Total + Notif      
	''' 2-> Desde AprobacionEmail.aspx O detalle Pedido + Accion Rechazar + No hay lineas aprobadas. Denegara Total      
	''' 0-> eoc</param>
	''' <returns>Un dataset con una tabla con los aprobadores a notificar en caso de traslado</returns>
	Function Orden_Denegar(ByVal OrdenId As Integer, ByVal Aprobador As String, ByVal Comentario As String _
						   , ByRef CodError As Short, ByRef ArgError1 As String, ByRef ArgError2 As String, ByRef ArgError3 As String, Optional ByVal FuerzaDenegarTotal As Integer = 0) As DataSet
		Dim cn As New SqlConnection(mDBConnection)
		Using cn
			cn.Open()
			Dim cm As New SqlCommand("FSEP_DENEGAR_ORDEN", cn)
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.Add("@ORDEN", SqlDbType.Int).Value = OrdenId
			cm.Parameters.Add("@APROBADOR", SqlDbType.VarChar, 20).Value = Aprobador
			cm.Parameters.Add("@COMENTARIO", SqlDbType.NVarChar, 2000).Value = Comentario
			cm.Parameters.Add("@FORZARDENEGARTOTAL", SqlDbType.TinyInt).Value = (FuerzaDenegarTotal > 0)
			cm.Parameters.Add("@ARG1", SqlDbType.VarChar, 200).Direction = ParameterDirection.Output
			cm.Parameters.Add("@ARG2", SqlDbType.VarChar, 200).Direction = ParameterDirection.Output
			cm.Parameters.Add("@ARG3", SqlDbType.VarChar, 200).Direction = ParameterDirection.Output
			cm.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
			Dim da As New SqlDataAdapter(cm)
			Dim ds As New DataSet
			da.Fill(ds)
			CodError = cm.Parameters("@ReturnValue").Value
			ArgError1 = DBNullToStr(cm.Parameters("@ARG1").Value)
			ArgError2 = DBNullToStr(cm.Parameters("@ARG2").Value)
			ArgError3 = DBNullToStr(cm.Parameters("@ARG3").Value)
			Return ds
		End Using
	End Function

	''' <summary>
	''' Determina si hay alguna linea aprobada
	''' </summary>
	''' <param name="lIdOrden">ID de la orden de entrega</param>
	''' <returns></returns>
	Public Function Comprobar_HayLineasAprobadas(ByVal lIdOrden As Long) As Boolean
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Dim dr As New DataSet
		Dim da As New SqlDataAdapter
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "SELECT 1 FROM LINEAS_PEDIDO LP WITH (NOLOCK) WHERE LP.ORDEN=" & lIdOrden & " AND LP.EST=" & EstadoLineaPedido.Aprobada
			cm.CommandType = CommandType.Text
			cm.CommandTimeout = 120
			da.SelectCommand = cm
			da.Fill(dr)

			Return (dr.Tables(0).Rows.Count > 0)
		Catch ex As Exception
			Throw ex
		Finally
			cn.Dispose()
			cm.Dispose()
		End Try
	End Function

	''' <summary>
	''' Comprueba si una línea de una orden de entrega puede ser aprobada por un aprobador
	''' </summary>
	''' <param name="OrdenId">ID de la orden de entrega</param>
	''' <param name="LineaId">ID de la línea a aprobar</param>
	''' <param name="Aprobador">Código de persona del aprobador</param>
	''' <returns>Un valor con un código que indica en qué situación quedaría la orden de entrega:
	''' 0: La línea se aprobará y la orden quedará pendiente de aprobación
	''' 1: Habrá que traspasar la orden a un aprobador superior
	''' 2: La orden se emitirá
	''' 3: La línea ha sido eliminada por otro usuario
	''' 4: La orden debería ser traspasada pero no existe aprobador superior
	''' 8: El pedido quedará parcialmente denegado
	''' </returns>
	Function Orden_ComprobarAprobacionDenegacionLinea(ByVal OrdenId As Integer, ByVal LineaId As Integer, ByVal Aprobador As String,
													  ByVal Aprobar As Boolean, ByVal Motivo As String) As Integer
		Dim cn As New SqlConnection(mDBConnection)
		Using cn
			cn.Open()
			Dim cm As New SqlCommand("FSEP_COMPROBAR_APROBACION_DENEGACION_LINEA", cn)
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@ORDEN", OrdenId)
			cm.Parameters.AddWithValue("@LINEA", LineaId)
			cm.Parameters.AddWithValue("@APROBADOR", Aprobador)
			cm.Parameters.AddWithValue("@APROBAR", Aprobar)
			If Not Motivo Is String.Empty Then cm.Parameters.AddWithValue("@MOTIVO", Motivo)
			cm.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
			cm.ExecuteNonQuery()
			Return cm.Parameters("@ReturnValue").Value
		End Using
	End Function
	''' <summary>
	''' Comprueba si una orden de entrega puede ser aprobada por un aprobador
	''' </summary>
	''' <param name="OrdenId">ID de la orden de entrega</param>
	''' <param name="Aprobador">Código de persona del aprobador</param>
	''' <param name="Arg1">Parámetro de salida con información sobre el estado final de la orden</param>
	''' <param name="Arg2">Parámetro de salida con infomración sobre el estado final de la orden</param>
	''' <returns>Un valor que indica en qué situación quedaría la orden de entrega:
	''' 1: La orden se traspasará a un nivel superior de aprobación
	''' 2: La orden se emitirá al proveedor
	''' 3: La orden ha sido eliminada por otro usuario
	''' 4: No se puede emitir y no hay aprobador superior
	''' 5: Hay líneas denegadas por el aprobador
	''' 6: Hay líneas denegadas por otro usuario
	''' 7: Hay líneas pendientes de otros aprobadores    
	''' </returns>
	Function Orden_ComprobarAprobacionOrden(ByVal OrdenId As Integer, ByVal Aprobador As String, ByRef Arg1 As Double, ByRef Arg2 As Double,
											ByVal motivo As Byte, ByVal instanciaAprob As Long) As Integer
		Dim cn As New SqlConnection(mDBConnection)
		Using cn
			cn.Open()
			Dim cm As New SqlCommand("FSEP_COMPROBAR_APROBACION_ORDEN", cn)
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.Add("@ORDEN", SqlDbType.Int).Value = OrdenId
			cm.Parameters.Add("@APROBADOR", SqlDbType.VarChar, 20).Value = Aprobador
			cm.Parameters.Add("@ARG1", SqlDbType.Float).Direction = ParameterDirection.Output
			cm.Parameters.Add("@ARG2", SqlDbType.Float).Direction = ParameterDirection.Output
			cm.Parameters.Add("@INSTANCIA_APROB", SqlDbType.Int).Value = instanciaAprob
			cm.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
			cm.ExecuteNonQuery()
			Arg1 = cm.Parameters("@ARG1").Value
			Arg2 = cm.Parameters("@ARG2").Value
			Return cm.Parameters("@ReturnValue").Value
		End Using
	End Function
	''' <summary>
	''' Comprueba si una orden de entrega puede ser denegada por un aprobador
	''' </summary>
	''' <param name="OrdenId">ID de la orden de entrega</param>
	''' <param name="Aprobador">Código de persona del aprobador</param>
	''' <returns>Un valor que indica en qué situación quedaría la orden de entrega:
	''' 1: La orden se traspasará a un nivel superior de aprobación
	''' 2: La orden quedará pendiente de aprobar
	''' 3: La orden ha sido eliminada por otro usuario
	''' 4: No se puede emitir y no hay aprobador superior
	''' 5: La orden quedará parcialmente denegada
	''' 6: La orden quedará totalmente denegada
	''' </returns>
	Function Orden_ComprobarDenegacionOrden(ByVal OrdenId As Integer, ByVal Aprobador As String, ByVal motivo As Byte, ByVal instanciaAprob As Long) As Integer
		Dim cn As New SqlConnection(mDBConnection)
		Using cn
			cn.Open()
			Dim cm As New SqlCommand("FSEP_COMPROBAR_DENEGACION_ORDEN", cn)
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.Add("@ORDEN", SqlDbType.Int).Value = OrdenId
			cm.Parameters.Add("@APROBADOR", SqlDbType.VarChar, 20).Value = Aprobador
			cm.Parameters.Add("@INSTANCIA_APROB", SqlDbType.Int).Value = instanciaAprob
			cm.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
			cm.ExecuteNonQuery()
			Return cm.Parameters("@ReturnValue").Value
		End Using
	End Function
	''' <summary>
	''' Comprueba si se ha realizado correctamente la integración de la orden de entrega
	''' </summary>
	''' <param name="Id">ID de la orden de entrega</param>
	''' <returns>True si no hay integración de salida o la orden de entrega se ha integrado correctamente.
	''' Si no devuelve False.</returns>
	Function Orden_ValidarIntegracion(ByVal Id As Integer) As Boolean
		Dim cn As New SqlConnection(mDBConnection)
		Using cn
			cn.Open()
			Dim cm As New SqlCommand("FSEP_VALIDAR_INTEGRACION_ORDEN", cn)
			cm.CommandType = CommandType.StoredProcedure

			cm.Parameters.Add("@ORDEN", SqlDbType.Int)
			cm.Parameters("@ORDEN").Value = Id
			cm.Parameters.Add("@ReturnValue", SqlDbType.Int)
			cm.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
			cm.ExecuteNonQuery()
			Return (cm.Parameters("@ReturnValue").Value = 1)
		End Using
	End Function
	''' <summary>
	''' Actualiza los datos de la cabecera de pedido en la BBDD
	''' </summary>
	''' <param name="Persona">Código de persona</param>
	''' <param name="Proveedor">Código de proveedor</param>
	''' <param name="Moneda">Código de moneda</param>
	''' <param name="NumPedidoERP">Número Pedido ERP</param>
	''' <param name="Empresa">ID de la empresa</param>
	''' <param name="CodProveedorERP">Código de proveedor ERP</param>
	''' <param name="Receptor">Código del receptor del pedido</param>
	''' <param name="TipoPedido">ID del tipo de pedido</param>
	''' <param name="Observaciones">Observaciones</param>
	''' <param name="DireccionEnvioFactura">Id de la dirección a la que se debe enviar la factura del pedido</param>
	''' <remarks>Llamada desde COrden.ActualizarCesta. Tiempo Max: </remarks>
	Public Sub Orden_ActualizarCesta(ByVal Persona As String, ByVal Proveedor As String, ByVal Moneda As String,
									 ByVal NumPedidoERP As String, ByVal Empresa As Integer, ByVal CodProveedorERP As String,
									 ByVal Receptor As String, ByVal TipoPedido As Integer, ByVal Observaciones As String,
									 ByVal DireccionEnvioFactura As Integer)
		Dim cm As New SqlCommand
		Dim cn As New SqlConnection(mDBConnection)
		Using cn
			cm.Connection = cn
			cm.CommandText = "UPDATE CESTA_CABECERA" &
				" SET NUM_PED_ERP=@NUMPEDERP, EMP=@EMP, COD_PROVE_ERP=@CODPROVEERP, RECEPTOR=@RECEPTOR, TIPOPEDIDO=@TIPOPEDIDO, OBS=@OBSERVACIONES, FAC_DIR_ENVIO=@DIRECCIONENVIOFACTURA" &
				" WHERE PER=@PER AND PROVE=@PROVE AND MON=@MON"
			cm.Parameters.Add("@NUMPEDERP", SqlDbType.VarChar, 100).Value = IIf(String.IsNullOrEmpty(NumPedidoERP), DBNull.Value, NumPedidoERP)
			cm.Parameters.Add("@EMP", SqlDbType.Int).Value = IIf(Empresa > 0, Empresa, DBNull.Value)
			cm.Parameters.Add("@CODPROVEERP", SqlDbType.VarChar, 100).Value = IIf(String.IsNullOrEmpty(CodProveedorERP), DBNull.Value, CodProveedorERP)
			cm.Parameters.Add("@RECEPTOR", SqlDbType.VarChar, 20).Value = IIf(String.IsNullOrEmpty(Receptor), DBNull.Value, Receptor)
			cm.Parameters.Add("@TIPOPEDIDO", SqlDbType.Int).Value = IIf(TipoPedido > 0, TipoPedido, DBNull.Value)
			cm.Parameters.Add("@OBSERVACIONES", SqlDbType.NVarChar, 4000).Value = IIf(String.IsNullOrEmpty(Observaciones), DBNull.Value, Observaciones)
			cm.Parameters.Add("@PER", SqlDbType.VarChar, 20).Value = Persona
			cm.Parameters.Add("@PROVE", SqlDbType.VarChar, 50).Value = Proveedor
			cm.Parameters.Add("@MON", SqlDbType.VarChar, 20).Value = Moneda
			If Not DireccionEnvioFactura > 0 Then
				cm.Parameters.Add("@DIRECCIONENVIOFACTURA", SqlDbType.Int).Value = DBNull.Value
			Else
				cm.Parameters.Add("@DIRECCIONENVIOFACTURA", SqlDbType.Int).Value = DireccionEnvioFactura
			End If

			cm.CommandType = CommandType.Text
			cn.Open()
			cm.ExecuteNonQuery()
		End Using
	End Sub
	''' <summary>
	''' Actualiza los costes y descuentos de cabecera de una orden de entrega cuando se han modificado desde el detalle de pedido
	''' </summary>
	''' <param name="dtCostesDescuentosCabecera">datatable con los costes y descuentos de cabecera de una orden</param>
	''' <remarks></remarks>
	Public Sub Orden_ActualizarCostesDescuentosCabecera(ByVal dtCostesDescuentosCabecera As DataTable)
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim da As New SqlDataAdapter
		Try
			cm.CommandText = "FSEP_ACTUALIZAR_COSTES_DESCUENTOS_ORDEN"
			cm.CommandType = CommandType.StoredProcedure
			cm.CommandTimeout = 300
			cm.Connection = cn
			cm.Parameters.Add("@ID", SqlDbType.Int, 4, "ID")
			cm.Parameters.Add("@IMPORTE", SqlDbType.Float, 8, "IMPORTE")
			cm.Parameters.Add("@ORDEN", SqlDbType.Int, 8, "ORDEN")
			da.InsertCommand = cm
			da.Update(dtCostesDescuentosCabecera)
		Catch ex As Exception
			Throw ex
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Sub
	''' <summary>
	''' Actualiza los costes y descuentos de las lineas de pedido
	''' </summary>
	''' <param name="dtCostesDescuentosLinea">datatable con los costes y descuentos de las lineas de pedido</param>
	''' <remarks></remarks>
	Public Sub Orden_ActualizarCostesDescuentosLineas(ByVal dtCostesDescuentosLinea As DataTable)
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim da As New SqlDataAdapter
		Try
			cm.CommandText = "FSEP_ACTUALIZAR_COSTES_DESCUENTOS_LINEA_ORDEN"
			cm.CommandType = CommandType.StoredProcedure
			cm.CommandTimeout = 300
			cm.Connection = cn
			cm.Parameters.Add("@ID", SqlDbType.Int, 4, "ID")
			cm.Parameters.Add("@IMPORTE", SqlDbType.Float, 8, "IMPORTE")
			cm.Parameters.Add("@LINEA", SqlDbType.Int, 8, "LINEA")
			da.InsertCommand = cm
			da.Update(dtCostesDescuentosLinea)
		Catch ex As Exception
			Throw ex
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Sub
	''' <summary>
	''' Actualiza el precio, cantidad, estado y el importe de pedido de las lineas al modificarlo desde el detalle de pedido, si a la linea se le ha modificado el precio o la cantidad se grabara en el log(LINEAS_PEDIDO_REGISTRO)
	''' </summary>
	''' <param name="dtLineas">datatable con los datos de las lineas de pedido</param>
	''' <remarks></remarks>
	Public Sub Orden_ActualizarLineas(ByVal dtLineas As DataTable)
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim da As New SqlDataAdapter
		Try
			cm.CommandText = "FSEP_ACTUALIZAR_LINEAS_PEDIDO"
			cm.CommandType = CommandType.StoredProcedure
			cm.CommandTimeout = 300
			cm.Connection = cn
			cm.Parameters.Add("@ID", SqlDbType.Int, 4, "ID")
			cm.Parameters.Add("@CANT", SqlDbType.Float, 8, "CANT")
			cm.Parameters.Add("@PREC", SqlDbType.Float, 8, "PREC")
			cm.Parameters.Add("@IMPORTE_PED", SqlDbType.Float, 8, "IMP_PED")
			cm.Parameters.Add("@EST", SqlDbType.Int, 4, "EST")
			cm.Parameters.Add("@COMENT", SqlDbType.VarChar, 1000, "COMENT")
			cm.Parameters.Add("@PER", SqlDbType.VarChar, 20, "PER")
			cm.Parameters.Add("@MODIFICADA", SqlDbType.Int, 4, "MODIFICADA")
			da.InsertCommand = cm
			da.Update(dtLineas)
		Catch ex As Exception
			Throw ex
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Sub
	''' <summary>
	''' Actualiza el importe total de la orden de entrega
	''' </summary>
	''' <param name="idOrden">id de la orden</param>
	''' <param name="Importe">importe total de la orden(con impuestos, costes, descuentos)</param>
	''' <param name="ImporteSolicitud" >Importe de la solicitud que se ha generado para las lineas de pedido de esa seguridad</param>
	''' <param name="idInstancia" >id de la instancia que se genero al superar el importe de pedido al emitir de pedido</param>
	''' <remarks></remarks>
	Public Sub Orden_ActualizarImporte(ByVal idOrden As Long, ByVal Importe As Double, ByVal ImporteSolicitud As Long, ByVal idInstancia As Long)
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Try
			cm.CommandText = "FSEP_ACTUALIZAR_IMPORTE_PEDIDO"
			cm.CommandType = CommandType.StoredProcedure
			cm.CommandTimeout = 300
			cm.Connection = cn
			cm.Parameters.AddWithValue("@ID", idOrden)
			cm.Parameters.AddWithValue("@IMPORTE", Importe)
			cm.Parameters.AddWithValue("@IMPORTE_SOLICITUD", ImporteSolicitud)
			cm.Parameters.AddWithValue("@INSTANCIA", idInstancia)
			cn.Open()
			cm.ExecuteNonQuery()
		Catch ex As Exception
			Throw ex
		Finally
			cn.Dispose()
			cm.Dispose()
		End Try
	End Sub
	''' <summary>
	''' Comprueba si el usuario es aprovisionador para las categorías dadas.
	''' </summary>
	''' <param name="codUsu">código del usuario</param>
	''' <param name="idFav">Id del pedido favorito de cuyos artículos se va a comprobar si el usuario es aprovisionador</param>
	''' <returns>
	''' Es aprovisionador -> devuelve TRUE
	''' No es aprovisionador -> devuelve FALSE
	''' </returns>
	''' <remarks></remarks>
	Public Function ComprobarAprovisonador(ByVal codUsu As String, ByVal idFav As Integer) As DataTable
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Dim ds As New DataSet
		Dim da As New SqlDataAdapter
		Dim bRes As Boolean
		Try
			bRes = False

			cm.Connection = cn

			cm.CommandText = "FSEP_USU_COMPROBARAPROVISIONADOR"
			cm.CommandType = CommandType.StoredProcedure

			cm.Parameters.AddWithValue("@USU", codUsu)
			cm.Parameters.AddWithValue("@IDFAV", idFav)

			da.SelectCommand = cm
			da.Fill(ds)

			If ds.Tables(0) IsNot Nothing Then
				Return ds.Tables(0)
			Else
				Return Nothing
			End If
		Catch e As Exception
			Throw e
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Function
	''' <summary>
	''' Crea un nuevo favorito
	''' </summary>
	''' <param name="iId">ID de la cesta</param>
	''' <param name="dImporte">Importe total</param>
	''' <param name="sDescFavorito">Nombre favorito</param>
	''' <remarks>Llamada desde COrden.ActualizarCesta. Tiempo Max: </remarks>
	Public Function Orden_GuardarNuevoFavorito(ByVal iId As Integer, ByVal dImporte As Object, ByVal sDescFavorito As String) As DataSet
		Authenticate()

		Dim ador As New DataSet
		Dim adoCom As New SqlCommand
		Dim oParam As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)

		'Ejecuta el store procedure FSEP_DEVOLVER_LINEAS_DE_ORDEN_FAVORITOS
		adoCom = New SqlCommand
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_GENERAR_FAVORITO_CESTA"
		adoCom.CommandType = CommandType.StoredProcedure
		oParam = adoCom.Parameters.AddWithValue("@ORDENID", IIf(iId > 0, iId, DBNull.Value))
		oParam = adoCom.Parameters.AddWithValue("@DESC", IIf(String.IsNullOrEmpty(sDescFavorito), DBNull.Value, sDescFavorito))
		oParam = adoCom.Parameters.AddWithValue("@IMPORTE", IIf(dImporte Is Nothing, DBNull.Value, dImporte))

		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(ador)
		If ador.Tables.Count = 0 Then
			ador.Clear()
			ador = Nothing
			cn.Close()
			adoCom = Nothing
			da.Dispose()
			ador.Dispose()
			adoCom.Dispose()
			Return ador
			Exit Function
		Else
			'Devuelve un recordset desconectado
			'Ya no existe-->ador.let_Connection(Nothing)
			adoCom = Nothing
			da.Dispose()
			cn.Close()
			oParam = Nothing
			Return ador
		End If
	End Function
	''' <summary>
	''' Actualiza los datos de la cabecera de pedido en la BBDD
	''' </summary>
	''' <param name="Persona">Código de persona</param>
	''' <param name="Proveedor">Código de proveedor</param>
	''' <param name="Moneda">Código de moneda</param>
	''' <param name="NumPedidoERP">Número Pedido ERP</param>
	''' <param name="Empresa">ID de la empresa</param>
	''' <param name="CodProveedorERP">Código de proveedor ERP</param>
	''' <param name="Receptor">Código del receptor del pedido</param>
	''' <param name="TipoPedido">ID del tipo de pedido</param>
	''' <param name="Observaciones">Observaciones</param>
	''' <param name="DireccionEnvioFactura">Id de la dirección a la que se debe enviar la factura del pedido</param>
	''' <param name="OrgCompras">Organización de compras</param>
	''' <remarks>Llamada desde COrden.ActualizarCesta. Tiempo Max: </remarks>
	Public Sub Orden_ActualizarCestaCabecera(ByVal iDesdeFavorito As Integer, ByVal iId As Integer, ByVal Persona As String, ByVal Proveedor As String, ByVal Moneda As String,
									 ByVal NumPedidoERP As String, ByVal Empresa As Integer, ByVal CodProveedorERP As String,
									 ByVal Receptor As String, ByVal TipoPedido As Integer, ByVal Observaciones As String,
									 ByVal DireccionEnvioFactura As Integer, ByVal sDescFavorito As String, ByVal bAcepProve As Boolean, Optional ByVal ModoEdicion As Boolean = False,
									 Optional ByVal OrgCompras As String = Nothing)
		Try
			Dim cm As New SqlCommand
			Dim cn As New SqlConnection(mDBConnection)
			Using cn
				cm.Connection = cn
				If iDesdeFavorito = 0 Then
					cm.CommandText = "FSEP_ACTUALIZAR_CABECERA_PEDIDO"
				Else
					cm.CommandText = "FSEP_ACTUALIZAR_CABECERA_PEDIDO_FAV"
				End If
				cm.CommandType = CommandType.StoredProcedure

				cm.Parameters.AddWithValue("@ID", IIf(iId > 0, iId, DBNull.Value))
				cm.Parameters.Add("@NUMPEDERP", SqlDbType.VarChar, 100).Value = IIf(String.IsNullOrEmpty(NumPedidoERP), DBNull.Value, NumPedidoERP)
				cm.Parameters.Add("@EMP", SqlDbType.Int).Value = IIf(Empresa > 0, Empresa, DBNull.Value)
				cm.Parameters.Add("@CODPROVEERP", SqlDbType.VarChar, 100).Value = IIf(String.IsNullOrEmpty(CodProveedorERP), DBNull.Value, CodProveedorERP)
				cm.Parameters.Add("@RECEPTOR", SqlDbType.VarChar, 20).Value = IIf(String.IsNullOrEmpty(Receptor), DBNull.Value, Receptor)
				cm.Parameters.Add("@TIPOPEDIDO", SqlDbType.Int).Value = IIf(TipoPedido > 0, TipoPedido, DBNull.Value)
				cm.Parameters.Add("@OBSERVACIONES", SqlDbType.NVarChar, 4000).Value = IIf(String.IsNullOrEmpty(Observaciones), DBNull.Value, Observaciones)
				cm.Parameters.Add("@PER", SqlDbType.VarChar, 20).Value = Persona
				cm.Parameters.Add("@PROVE", SqlDbType.VarChar, 50).Value = Proveedor
				cm.Parameters.Add("@MON", SqlDbType.VarChar, 20).Value = Moneda
				If Not DireccionEnvioFactura > 0 Then
					cm.Parameters.Add("@DIRECCIONENVIOFACTURA", SqlDbType.Int).Value = DBNull.Value
				Else
					cm.Parameters.Add("@DIRECCIONENVIOFACTURA", SqlDbType.Int).Value = DireccionEnvioFactura
				End If
				If iDesdeFavorito = 1 Then
					If String.IsNullOrEmpty(sDescFavorito) Then
						cm.Parameters.Add("@DESC", SqlDbType.VarChar, 100).Value = DBNull.Value
					Else
						cm.Parameters.Add("@DESC", SqlDbType.VarChar, 100).Value = sDescFavorito
					End If
				End If
				If iDesdeFavorito = 0 Then
					'Si se ha editado el nº de linea, se marca en la cabecera del pedido
					If ModoEdicion = False Then
						cm.Parameters.Add("@MODOEDICION", SqlDbType.TinyInt).Value = 0
					Else
						cm.Parameters.Add("@MODOEDICION", SqlDbType.TinyInt).Value = 1
					End If
				End If
				cm.Parameters.Add("@ORGCOMPRAS", SqlDbType.VarChar, 4).Value = IIf(OrgCompras Is Nothing, DBNull.Value, OrgCompras)
				cm.Parameters.Add("@ACEP_PROVE", SqlDbType.TinyInt).Value = BooleanToSQLBinary(bAcepProve)
				cn.Open()
				cm.ExecuteNonQuery()
			End Using
		Catch ex As Exception
			Throw ex
		End Try
	End Sub
	''' <summary>
	''' Actualiza l
	''' </summary>
	''' <param name="idAtributo">idAtributo</param>
	''' <param name="idCestaCabecera">idCestaCabecera</param>
	''' <param name="tipo">tipo</param>
	''' <param name="dValor">dValor</param>
	''' <remarks>Llamada desde COrden.ActualizarCesta. Tiempo Max: </remarks>
	Public Sub ActualizarAtributosCabecera(ByVal iDesdeFavorito As Integer, ByVal idAtributo As Integer, ByVal idCestaCabecera As Integer, ByVal tipoValor As Object, ByVal tipo As Object,
												ByVal dValor As Object, ByVal bInterno As Boolean, ByVal bObligatorio As Boolean, ByVal UsuCod As String, ByVal bMostrarEnRecep As Boolean, Optional ByVal Valor_Cod As String = Nothing)
		Authenticate()

		Dim iResultado As Integer = 0
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Try
			cn.Open()
			cm.Connection = cn
			If iDesdeFavorito = 0 Then
				cm.CommandText = "FSEP_ACTUALIZAR_ATRIBUTOS_CABECERA"
			Else
				cm.CommandText = "FSEP_ACTUALIZAR_ATRIBUTOS_CABECERA_FAV"
			End If
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@IDATRIBUTO", idAtributo)
			cm.Parameters.AddWithValue("@IDCESTACABECERA", idCestaCabecera)
			If tipoValor = "1" Then
				cm.Parameters.AddWithValue("@VALTEXT", IIf(dValor Is Nothing, System.DBNull.Value, dValor)) 'dValor)
			Else
				cm.Parameters.AddWithValue("@VALTEXT", System.DBNull.Value)
			End If
			If tipoValor = "2" Then
				cm.Parameters.AddWithValue("@VALNUM", IIf(dValor Is Nothing, System.DBNull.Value, IIf(dValor.ToString() = String.Empty, System.DBNull.Value, Double.Parse(dValor.ToString())))) 'Double.Parse(dValor.ToString()))) 'Double.Parse(dValor))
			Else
				cm.Parameters.AddWithValue("@VALNUM", System.DBNull.Value)
			End If
			If tipoValor = "3" Then
				If dValor Is Nothing Or dValor Is DBNull.Value Then
					cm.Parameters.AddWithValue("@VALFEC", System.DBNull.Value)
				Else
					cm.Parameters.AddWithValue("@VALFEC", IIf(dValor Is Nothing, System.DBNull.Value, DateTime.Parse(dValor.ToString()))) 'DateTime.Parse(dValor))
				End If
			Else
				cm.Parameters.AddWithValue("@VALFEC", System.DBNull.Value)
			End If
			If tipoValor = "4" Then
				If dValor Is Nothing Or dValor.ToString() = "" Then
					cm.Parameters.AddWithValue("@VALBOOL", System.DBNull.Value)
				Else
					cm.Parameters.AddWithValue("@VALBOOL", Int32.Parse(dValor))
				End If
			Else
				cm.Parameters.AddWithValue("@VALBOOL", System.DBNull.Value)
			End If
			cm.Parameters.AddWithValue("@INTERNO", bInterno)
			cm.Parameters.AddWithValue("@OBLIGATORIO", bObligatorio)
			cm.Parameters.AddWithValue("@TIPO", Int32.Parse(tipo))
			If iDesdeFavorito = 1 Then
				cm.Parameters.AddWithValue("@USU", UsuCod)
			End If
			If Valor_Cod IsNot Nothing Then cm.Parameters.AddWithValue("@VALOR_COD", Valor_Cod)
			If iDesdeFavorito = 0 Then cm.Parameters.AddWithValue("@MOSTRARENRECEP", bMostrarEnRecep)
			cm.Transaction = cn.BeginTransaction()
			cm.ExecuteNonQuery()
			cm.Transaction.Commit()
			iResultado = 1
		Catch ex As Exception
			cm.Transaction.Rollback()
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Sub
	''' <summary>
	''' Inserta el pedido
	''' </summary>
	''' <remarks>Llamada desde COrden.ActualizarCesta. Tiempo Max: </remarks>
	Public Function InsertarPedido(ByVal codPersona As String, ByVal EsPedidoAbierto As Boolean) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Dim da As New SqlDataAdapter
		Dim dr As New DataSet
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_INSERTAR_PEDIDO"
			cm.CommandType = CommandType.StoredProcedure
			cm.Transaction = cn.BeginTransaction()
			cm.Parameters.AddWithValue("@PER", codPersona)
			cm.Parameters.AddWithValue("@ESPEDIDOABIERTO", EsPedidoAbierto)
			da.SelectCommand = cm
			da.Fill(dr)
			cm.Transaction.Commit()
			Return dr
		Catch ex As Exception
			cm.Transaction.Rollback()
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
			dr.Dispose()
		End Try
	End Function
	''' <summary>
	''' Actualiza l
	''' </summary>
	''' <remarks>Llamada desde COrden.ActualizarCesta. Tiempo Max: </remarks>
	Public Function Recuperar_FacturacionActivada() As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Dim da As New SqlDataAdapter
		Dim dr As New DataSet
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_FACTURACION_ACTIVADA"
			cm.CommandType = CommandType.StoredProcedure
			cm.Transaction = cn.BeginTransaction()
			cm.ExecuteNonQuery()
			cm.Transaction.Commit()
			da.SelectCommand = cm
			da.Fill(dr)
			Return dr
		Catch ex As Exception
			cm.Transaction.Rollback()
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
			dr.Dispose()
		End Try
	End Function
	''' <summary>
	''' Actualiza los atributos de la linea
	''' </summary>
	''' <param name="idAtributo">idAtributo</param>
	''' <param name="tipo">tipo</param>
	''' <param name="dValor">dValor</param>
	''' <remarks>Llamada desde COrden.ActualizarCesta. Tiempo Max: </remarks>
	Public Sub ActualizarAtributosLinea(ByVal iDesdeFavorito As Integer, ByVal idAtributo As Integer, ByVal idCesta As Integer, ByVal tipoValor As Object, ByVal tipo As Object,
											 ByVal dValor As Object, ByVal bInterno As Boolean, ByVal bObligatorio As Boolean, ByVal UsuCod As String, ByVal bMostrarEnRecep As Boolean, Optional ByVal Valor_Cod As String = Nothing)
		Authenticate()

		Dim iResultado As Integer = 0
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Try
			cn.Open()
			cm.Connection = cn
			If iDesdeFavorito = 0 Then
				cm.CommandText = "FSEP_ACTUALIZAR_ATRIBUTOS_LINEA"
			Else
				cm.CommandText = "FSEP_ACTUALIZAR_ATRIBUTOS_LINEA_FAV"
			End If
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@IDATRIBUTO", idAtributo)
			cm.Parameters.AddWithValue("@IDCESTA", idCesta)
			If tipoValor = "1" Then
				cm.Parameters.AddWithValue("@VALTEXT", IIf(dValor Is Nothing, System.DBNull.Value, dValor))
			Else
				cm.Parameters.AddWithValue("@VALTEXT", System.DBNull.Value)
			End If
			If tipoValor = "2" Then
				If String.IsNullOrEmpty(dValor) Then
					cm.Parameters.AddWithValue("@VALNUM", System.DBNull.Value)
				Else
					cm.Parameters.AddWithValue("@VALNUM", Double.Parse(dValor.ToString()))
				End If
			Else
				cm.Parameters.AddWithValue("@VALNUM", System.DBNull.Value)
			End If
			If tipoValor = "3" Then
				If dValor Is Nothing Then
					cm.Parameters.AddWithValue("@VALFEC", System.DBNull.Value)
				Else
					cm.Parameters.AddWithValue("@VALFEC", IIf(dValor Is Nothing, System.DBNull.Value, DateTime.Parse(dValor.ToString()))) 'DateTime.Parse(dValor))
				End If
			Else
				cm.Parameters.AddWithValue("@VALFEC", System.DBNull.Value)
			End If
			If tipoValor = "4" Then
				If dValor Is Nothing Or dValor.ToString() = "" Then
					cm.Parameters.AddWithValue("@VALBOOL", System.DBNull.Value)
				Else
					cm.Parameters.AddWithValue("@VALBOOL", Int32.Parse(dValor))
				End If
			Else
				cm.Parameters.AddWithValue("@VALBOOL", System.DBNull.Value)
			End If
			cm.Parameters.AddWithValue("@INTERNO", bInterno)
			cm.Parameters.AddWithValue("@OBLIGATORIO", bObligatorio)
			cm.Parameters.AddWithValue("@TIPO", Int32.Parse(tipo))
			If iDesdeFavorito = 1 Then
				cm.Parameters.AddWithValue("@USU", UsuCod)
			End If
			If Valor_Cod IsNot Nothing Then cm.Parameters.AddWithValue("@VALOR_COD", Valor_Cod)
			If iDesdeFavorito = 0 Then cm.Parameters.AddWithValue("@MOSTRARENRECEP", bMostrarEnRecep)
			cm.Transaction = cn.BeginTransaction()
			cm.ExecuteNonQuery()
			cm.Transaction.Commit()
			iResultado = 1
		Catch ex As Exception
			cm.Transaction.Rollback()
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Sub
	''' <summary>
	''' Crea la orden de entrega y las lineas de pedido
	''' </summary>
	''' <remarks>Llamada desde COrden.ActualizarCesta. Tiempo Max: </remarks>
	Public Function CrearOrdenEntregaLinea(ByVal idCesta As Integer, ByVal Cambio As Object, ByVal msAprov As String, ByVal Prove As String, ByVal Moneda As String, ByVal mlPedido As Integer,
										ByVal Referencia As Object, ByVal Obs As Object, ByVal Empresa As Object, ByVal Receptor As Object, ByVal CodErp As Object,
										ByVal idTipoPedido As Integer, ByVal DireccionEnvioFactura As Integer, ByVal bAutofactura As Boolean, ByVal iIM_Factrasrecep As Integer,
										ByVal sIdioma As String, ByVal Importe As Object, ByRef oe_num As Long, ByVal EsPedidoAbierto As Boolean, ByVal OrgCompras As String, ByVal bAcepProve As Boolean) As Integer
		Authenticate()

		Dim iResultado As Integer = 0
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_INSERTAR_ORDEN_LINEA_PEDIDO"
			cm.CommandType = CommandType.StoredProcedure
			cm.Transaction = cn.BeginTransaction()
			cm.Parameters.AddWithValue("@CAMBIO", Cambio)
			cm.Parameters.AddWithValue("@PER", msAprov)
			cm.Parameters.AddWithValue("@PROVE", Prove)
			cm.Parameters.AddWithValue("@MON", Moneda)
			cm.Parameters.AddWithValue("@PEDIDO", mlPedido)
			cm.Parameters.AddWithValue("@REFERENCIA", IIf(Referencia Is Nothing, DBNull.Value, Referencia))
			cm.Parameters.AddWithValue("@OBSERVACIONES", Obs)
			cm.Parameters.AddWithValue("@EMPRESA", Empresa)
			cm.Parameters.AddWithValue("@RECEPTOR", IIf(String.IsNullOrEmpty(Receptor), DBNull.Value, Receptor))
			cm.Parameters.AddWithValue("@CODERP", CodErp)
			cm.Parameters.AddWithValue("@TIPOPEDIDO", idTipoPedido)
			If Not DireccionEnvioFactura = 0 Then cm.Parameters.AddWithValue("@DIRECCIONENVIOFACTURA", DireccionEnvioFactura)
			'Antes de insertar las ÃƒÂ³rdenes de entrega, comprobamos si hay facturacion activada. 
			'  Si la hay:
			'   Comprobamos si el proveedor tiene la marca de autofactura activada. Si es asÃƒÂ­, aÃƒÂ±adiremos 
			'     en el insert de orden_entrega IM_AUTOFACTURA=1
			cm.Parameters.AddWithValue("@IM_AUTOFACTURA", bAutofactura)
			cm.Parameters.AddWithValue("@IM_FACTRASRECEP", iIM_Factrasrecep)
			cm.Parameters.AddWithValue("@CESTA", idCesta)
			If Importe IsNot Nothing Then cm.Parameters.AddWithValue("@IMPORTE", Importe)
			cm.Parameters.AddWithValue("@TIPO", IIf(EsPedidoAbierto, 6, 1)) 'Si es pedido contra pedido abierto es un pedido de tipo 6, sino, es de catalogo tipo=1            
			cm.Parameters.Add(New SqlParameter("@ORDEN", SqlDbType.Int))
			cm.Parameters("@ORDEN").Direction = ParameterDirection.Output
			cm.Parameters.Add(New SqlParameter("@NUM_ORDEN", SqlDbType.Int))
			cm.Parameters("@NUM_ORDEN").Direction = ParameterDirection.Output
			cm.Parameters.AddWithValue("@ORGCOMPRAS", IIf(String.IsNullOrEmpty(OrgCompras), DBNull.Value, OrgCompras))
			cm.Parameters.AddWithValue("@ACEP_PROVE", bAcepProve)

			cm.ExecuteNonQuery()
			cm.Transaction.Commit()
			'iResultado = 1
			oe_num = DBNullToInteger(cm.Parameters("@NUM_ORDEN").Value)
			iResultado = DBNullToInteger(cm.Parameters("@ORDEN").Value)
			Return iResultado
		Catch ex As Exception
			cm.Transaction.Rollback()
			iResultado = 0
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Function

	''' <summary>
	''' Updata (o no segun ModificarPreciosUsu) los precios de catalog-lin con la adjudicacion. Devuelve las lineas q toca, para poder actualizar la cache de "detalle articulos"
	''' </summary>
	''' <param name="idOrden">Orden</param>
	''' <param name="idPedido">Pedido</param>
	''' <param name="msAprov">Aprobador</param>
	''' <param name="ModificarPreciosUsu">Updatar si o no</param>
	''' <returns></returns>
	Public Function TrasCrearOrdenEntregaLineaActualizarCatalogo(ByVal idOrden As Long, ByVal idPedido As Long, ByVal msAprov As String, ByVal ModificarPreciosUsu As Boolean) As DataRowCollection
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Dim LinModif As DataRowCollection = Nothing
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_ACTUALIZAR_CATALOGO"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.Add("@PEDIDO", SqlDbType.Int)
			cm.Parameters.Add("@PER", SqlDbType.VarChar, 20)
			cm.Parameters.Add("@ORDEN", SqlDbType.Int)
			cm.Parameters("@PEDIDO").Value = IIf(idPedido = 0, DBNull.Value, idPedido)
			cm.Parameters("@PER").Value = msAprov
			cm.Parameters("@ORDEN").Value = IIf(idOrden = 0, DBNull.Value, idOrden)
			cm.Parameters.AddWithValue("@MODIFPREC", ModificarPreciosUsu)
			Dim da As New SqlDataAdapter(cm)
			Dim ds As New DataSet()
			da.Fill(ds)

			If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				LinModif = ds.Tables(0).Rows
			End If

		Catch ex As Exception
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try

		Return LinModif
	End Function

	''' <summary>
	''' Crea los impuestos de las lineas del pedido
	''' </summary>
	''' <remarks>Llamada desde COrden.ActualizarCesta. Tiempo Max: </remarks>
	Public Sub GuardarImpuestosLinea(ByVal idPedido As Integer, ByVal idImpuesto As Integer, ByVal dValor As Object, ByVal idLinea As Integer)
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_LINEAPEDIDO_INSERTAR_IMPUESTO"
			cm.CommandType = CommandType.StoredProcedure
			cm.Transaction = cn.BeginTransaction()
			cm.Parameters.Add(New SqlParameter("@PEDIDO", SqlDbType.Int))
			cm.Parameters.Add(New SqlParameter("@IMPUESTO", SqlDbType.Int))
			cm.Parameters.Add(New SqlParameter("@LINEA", SqlDbType.Int))
			cm.Parameters.Add(New SqlParameter("@VALOR", SqlDbType.Float))
			cm.Parameters("@PEDIDO").Value = idPedido
			cm.Parameters("@IMPUESTO").Value = idImpuesto
			cm.Parameters("@LINEA").Value = idLinea
			cm.Parameters("@VALOR").Value = IIf(dValor Is Nothing, DBNull.Value, Double.Parse(dValor.ToString))
			cm.ExecuteNonQuery()
			cm.Transaction.Commit()
		Catch ex As Exception
			cm.Transaction.Rollback()
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Sub
	''' <summary>
	''' Actualizar Activo en las lineas de pedido      
	''' </summary>
	''' <param name="idLinea">idLinea</param>
	''' <param name="activo">activo</param>
	''' <remarks>Llamada desde COrden.ActualizarCesta. Tiempo Max: </remarks>
	Public Sub ActualizarActivoLineaPedido(ByVal idLinea As Integer, ByVal activo As Integer)
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_ACTUALIZAR_ACTIVO_LINEA_PEDIDO"
			cm.CommandType = CommandType.StoredProcedure
			cm.Transaction = cn.BeginTransaction()
			cm.Parameters.Add(New SqlParameter("@LINEA", SqlDbType.Int))
			cm.Parameters.Add(New SqlParameter("@ACTIVO", SqlDbType.Int))
			cm.Parameters("@LINEA").Value = idLinea
			If activo = 0 Then
				cm.Parameters("@ACTIVO").Value = System.DBNull.Value
			Else
				cm.Parameters("@ACTIVO").Value = activo
			End If

			cm.ExecuteNonQuery()
			cm.Transaction.Commit()
		Catch ex As Exception
			cm.Transaction.Rollback()
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Sub
	''' <summary>
	''' Actualizar Activo en las lineas de pedido      
	''' </summary>
	''' <param name="idLinea">idLinea</param>
	''' <remarks>Llamada desde COrden.ActualizarCesta. Tiempo Max: </remarks>
	Public Sub TratarImputacionesLinea(ByVal idOrden As Integer, ByVal idLinea As Integer, ByVal mlPedido As Integer,
											ByVal sUON1 As String, ByVal sUON2 As String, ByVal sUON3 As String, ByVal sUON4 As String,
											ByVal sNIV0 As String, ByVal sNIV1 As String, ByVal sNIV2 As String, ByVal sNIV3 As String, ByVal sNIV4 As String, ByVal iModoImpuesto As Integer)
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_INSERTAR_LINEAS_PED_IMPUTACION"
			cm.CommandType = CommandType.StoredProcedure
			cm.Transaction = cn.BeginTransaction()
			cm.Parameters.Add(New SqlParameter("@UON1", SqlDbType.VarChar, 50))
			cm.Parameters.Add(New SqlParameter("@UON2", SqlDbType.VarChar, 50))
			cm.Parameters.Add(New SqlParameter("@UON3", SqlDbType.VarChar, 50))
			cm.Parameters.Add(New SqlParameter("@UON4", SqlDbType.VarChar, 50))
			cm.Parameters.Add(New SqlParameter("@PRES0", SqlDbType.VarChar, 500))
			cm.Parameters.Add(New SqlParameter("@PRES1", SqlDbType.VarChar, 500))
			cm.Parameters.Add(New SqlParameter("@PRES2", SqlDbType.VarChar, 500))
			cm.Parameters.Add(New SqlParameter("@PRES3", SqlDbType.VarChar, 500))
			cm.Parameters.Add(New SqlParameter("@PRES4", SqlDbType.VarChar, 500))
			cm.Parameters.Add(New SqlParameter("@LINEA", SqlDbType.Int))
			cm.Parameters.Add(New SqlParameter("@PEDIDO", SqlDbType.Int)).Value = mlPedido
			cm.Parameters.Add(New SqlParameter("@ORDEN", SqlDbType.Int))
			cm.Parameters.Add(New SqlParameter("@MODOIMP", SqlDbType.Int))
			cm.Parameters("@ORDEN").Value = idOrden
			cm.Parameters("@MODOIMP").Value = iModoImpuesto
			cm.Parameters("@LINEA").Value = idLinea
			cm.Parameters("@UON1").Value = sUON1
			cm.Parameters("@UON2").Value = IIf(String.IsNullOrEmpty(sUON2), DBNull.Value, sUON2)
			cm.Parameters("@UON3").Value = IIf(String.IsNullOrEmpty(sUON3), DBNull.Value, sUON3)
			cm.Parameters("@UON4").Value = IIf(String.IsNullOrEmpty(sUON4), DBNull.Value, sUON4)
			cm.Parameters("@PRES0").Value = sNIV0
			cm.Parameters("@PRES1").Value = sNIV1
			cm.Parameters("@PRES2").Value = IIf(String.IsNullOrEmpty(sNIV2), DBNull.Value, sNIV2)
			cm.Parameters("@PRES3").Value = IIf(String.IsNullOrEmpty(sNIV3), DBNull.Value, sNIV3)
			cm.Parameters("@PRES4").Value = IIf(String.IsNullOrEmpty(sNIV4), DBNull.Value, sNIV4)
			cm.ExecuteNonQuery()
			cm.Transaction.Commit()
		Catch ex As Exception
			cm.Transaction.Rollback()
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Sub
	''' <summary>
	''' Crea los impuestos de las lineas del pedido
	''' </summary>
	''' <remarks>Llamada desde COrden.ActualizarCesta. Tiempo Max: </remarks>
	Public Sub GuardarImpuestosCostesLinea(ByVal idPedido As Integer, ByVal idCoste As Integer, ByVal idImpuesto As Integer, ByVal dValor As Object, ByVal idLinea As Integer)
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_LINEAPEDIDOCD_INSERTAR_IMPUESTO"
			cm.CommandType = CommandType.StoredProcedure
			cm.Transaction = cn.BeginTransaction()
			cm.Parameters.Add(New SqlParameter("@PEDIDO", SqlDbType.Int))
			cm.Parameters.Add(New SqlParameter("@COSTE", SqlDbType.Int))
			cm.Parameters.Add(New SqlParameter("@IMPUESTO", SqlDbType.Int))
			cm.Parameters.Add(New SqlParameter("@VALOR", SqlDbType.Float))
			cm.Parameters.Add(New SqlParameter("@LINEA", SqlDbType.Int))
			cm.Parameters("@PEDIDO").Value = idPedido
			cm.Parameters("@COSTE").Value = idCoste
			cm.Parameters("@IMPUESTO").Value = idImpuesto
			cm.Parameters("@LINEA").Value = idLinea
			cm.Parameters("@VALOR").Value = IIf(dValor Is Nothing, DBNull.Value, dValor)

			cm.ExecuteNonQuery()
			cm.Transaction.Commit()
		Catch ex As Exception
			cm.Transaction.Rollback()
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Sub
	''' <summary>
	''' Crea los impuestosn de la cabecera del pedido
	''' </summary>
	''' <remarks>Llamada desde COrden.ActualizarCesta. Tiempo Max: </remarks>
	Public Sub GuardarImpuestosCabecera(ByVal idPedido As Integer, ByVal idCoste As Integer, ByVal idImpuesto As Integer, ByVal dValor As Object)
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_ORDENENTREGA_INSERTAR_IMPUESTO_CABECERA"
			cm.CommandType = CommandType.StoredProcedure
			cm.Transaction = cn.BeginTransaction()
			cm.Parameters.Add(New SqlParameter("@PEDIDO", SqlDbType.Int))
			cm.Parameters.Add(New SqlParameter("@COSTE", SqlDbType.Int))
			cm.Parameters.Add(New SqlParameter("@IMPUESTO", SqlDbType.Int))
			cm.Parameters.Add(New SqlParameter("@VALOR", SqlDbType.Float))
			cm.Parameters("@PEDIDO").Value = idPedido
			cm.Parameters("@COSTE").Value = idCoste
			cm.Parameters("@IMPUESTO").Value = idImpuesto
			cm.Parameters("@VALOR").Value = IIf(dValor Is Nothing, DBNull.Value, dValor)

			cm.ExecuteNonQuery()
			cm.Transaction.Commit()
		Catch ex As Exception
			cm.Transaction.Rollback()
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Sub
	''' <summary>
	''' Actualiza los costes/descuentos de la cabecera del pedido
	''' </summary>
	''' <param name="idAtributo">idAtributo</param>
	''' <param name="idCestaCabecera">idCestaCabecera</param>
	''' <remarks>Llamada desde COrden.ActualizarCesta. Tiempo Max: </remarks>
	Public Sub ActualizarCostesDescuentosCabecera(ByVal iDesdeFavorito As Integer, ByVal idAtributo As Integer, ByVal idCestaCabecera As Integer, ByVal tipo As Object,
										ByVal bInterno As Boolean, ByVal bObligatorio As Boolean, ByVal sOperacion As String, ByVal dValorNum As Object, ByVal dValor As Object, ByVal bMostrarEnRecep As Boolean)
		Authenticate()

		Dim iResultado As Integer = 0
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		If iDesdeFavorito = 0 Then
			Try
				cn.Open()
				cm.Connection = cn
				cm.CommandText = "FSEP_ACTUALIZAR_COSTESDESCUENTOS_CABECERA"
				cm.CommandType = CommandType.StoredProcedure
				cm.Parameters.AddWithValue("@IDCESTACABECERA", idCestaCabecera)
				cm.Parameters.AddWithValue("@IDATRIBUTO", idAtributo)
				cm.Parameters.AddWithValue("@INTERNO", bInterno)
				cm.Parameters.AddWithValue("@OBLIGATORIO", bObligatorio)
				cm.Parameters.AddWithValue("@TIPO", Int32.Parse(tipo))
				cm.Parameters.AddWithValue("@OPERACION", sOperacion)
				cm.Parameters.AddWithValue("@VALOR_NUM", IIf(dValorNum Is Nothing, System.DBNull.Value, Double.Parse(dValorNum.ToString())))
				cm.Parameters.AddWithValue("@VALOR", IIf(dValor Is Nothing, System.DBNull.Value, Double.Parse(dValor.ToString())))
				cm.Parameters.AddWithValue("@MOSTRARENRECEP", bMostrarEnRecep)
				cm.Transaction = cn.BeginTransaction()
				cm.ExecuteNonQuery()
				cm.Transaction.Commit()
				iResultado = 1
			Catch ex As Exception
				cm.Transaction.Rollback()
				Throw ex
			Finally
				cm.Dispose()
				cn.Close()
				cn.Dispose()
			End Try
		End If
	End Sub
	''' <summary>
	''' Actualiza los costes/descuentos de las lineas del pedido
	''' </summary>
	''' <param name="idAtributo">idAtributo</param>
	''' <param name="idCestaCabecera">idCestaCabecera</param>
	''' <remarks>Llamada desde COrden.ActualizarCesta. Tiempo Max: </remarks>
	Public Sub ActualizarCostesDescuentosLinea(ByVal idCestaCabecera As Integer, ByVal idAtributo As Integer, ByVal tipo As Object, ByVal bInterno As Boolean, ByVal bObligatorio As Boolean,
					ByVal sOperacion As String, ByVal dValorNum As Object, ByVal dValor As Object, ByVal bMostrarEnRecep As Boolean)
		Authenticate()
		Dim iResultado As Integer = 0
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_ACTUALIZAR_COSTESDESCUENTOS_LINEA"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@IDCESTA", idCestaCabecera)
			cm.Parameters.AddWithValue("@IDATRIBUTO", idAtributo)
			cm.Parameters.AddWithValue("@INTERNO", bInterno)
			cm.Parameters.AddWithValue("@OBLIGATORIO", bObligatorio)
			cm.Parameters.AddWithValue("@TIPO", Int32.Parse(tipo))
			cm.Parameters.AddWithValue("@OPERACION", sOperacion)
			cm.Parameters.AddWithValue("@VALOR_NUM", IIf(dValorNum Is Nothing, System.DBNull.Value, Double.Parse(dValorNum.ToString())))
			cm.Parameters.AddWithValue("@VALOR", IIf(dValor Is Nothing, System.DBNull.Value, Double.Parse(dValor.ToString())))
			cm.Parameters.AddWithValue("@MOSTRARENRECEP", bMostrarEnRecep)
			cm.Transaction = cn.BeginTransaction()
			cm.ExecuteNonQuery()
			cm.Transaction.Commit()
			iResultado = 1
		Catch ex As Exception
			cm.Transaction.Rollback()
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Sub
	''' <summary>
	''' Borra los costes/descuentos de la cabecera del pedido
	''' </summary>
	''' <param name="idCestaCabecera">idCestaCabecera</param>
	''' <remarks>Llamada desde COrden.ActualizarCesta. Tiempo Max: </remarks>
	Public Sub VaciarCostesDescuentosCabecera(ByVal iDesdeFavorito As Integer, ByVal idCestaCabecera As Integer)
		Authenticate()

		Dim iResultado As Integer = 0
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Try
			cn.Open()
			cm.Connection = cn
			If iDesdeFavorito = 0 Then
				cm.CommandText = "FSEP_VACIAR_COSTESDESCUENTOS"
			Else
				cm.CommandText = "FSEP_VACIAR_COSTESDESCUENTOS_FAV"
			End If
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@CESTACABECERAID", idCestaCabecera)
			cm.Transaction = cn.BeginTransaction()
			cm.ExecuteNonQuery()
			cm.Transaction.Commit()
			iResultado = 1
		Catch ex As Exception
			cm.Transaction.Rollback()
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Sub
	''' <summary>
	''' Borra los costes/descuentos de las lineas del pedido
	''' </summary>
	''' <remarks>Llamada desde COrden.ActualizarCesta. Tiempo Max: </remarks>
	Public Sub VaciarCostesDescuentosLinea(ByVal iDesdeFavorito As Integer, ByVal idCesta As Integer)
		Authenticate()

		Dim iResultado As Integer = 0
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Try
			cn.Open()
			cm.Connection = cn
			If iDesdeFavorito = 0 Then
				cm.CommandText = "FSEP_VACIAR_COSTESDESCUENTOS_LINEA"
			Else
				cm.CommandText = "FSEP_VACIAR_COSTESDESCUENTOS_LINEA_FAV"
			End If
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@CESTA", idCesta)
			cm.Transaction = cn.BeginTransaction()
			cm.ExecuteNonQuery()
			cm.Transaction.Commit()
			iResultado = 1
		Catch ex As Exception
			cm.Transaction.Rollback()
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Sub
	''' <summary>
	''' Comprueba si la empresa es valida
	''' </summary>
	''' <param name="idLinea">idLinea</param>
	''' <param name="iEmp">iEmp</param>
	''' <param name="EsPedidoAbierto"></param> 
	''' <param name="sOrgCompras">Organización de compras</param>
	''' <remarks> </remarks>
	Public Function Comprobar_Empresa_Adj(ByVal idLinea As Integer, ByVal iEmp As Integer, ByVal EsPedidoAbierto As Boolean, Optional ByVal sOrgCompras As String = Nothing, Optional ByVal modFav As Boolean = False) As Object
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim da As New SqlDataAdapter
		Dim dr As New DataSet
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_COMPROBAR_EMPRESA_ADJ"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@IDLINEA", idLinea)
			cm.Parameters.AddWithValue("@IDEMPRESA", iEmp)
			cm.Parameters.AddWithValue("@ESPEDIDOABIERTO", EsPedidoAbierto)
			cm.Parameters.AddWithValue("@ORGCOMPRAS", IIf(sOrgCompras Is Nothing, DBNull.Value, sOrgCompras))
			cm.Parameters.AddWithValue("@MODFAV", modFav)
			da.SelectCommand = cm
			da.Fill(dr)
			Return dr
		Catch ex As Exception
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
			da.Dispose()
		End Try
	End Function
	''' <summary>
	''' Comprueba si la empresa es valida
	''' </summary>
	''' <param name="idLinea">idLinea</param>
	''' <param name="iEmp">iEmp</param>
	''' <param name="EsPedidoAbierto"></param>
	''' <param name="sOrgCompras">Organización de compras</param>
	''' <remarks> </remarks>
	Public Function Comprobar_Empresa(ByVal idLinea As Integer, ByVal iEmp As Integer, ByVal EsPedidoAbierto As Boolean, Optional ByVal sOrgCompras As String = Nothing, Optional ByVal modFav As Boolean = False) As Object
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim da As New SqlDataAdapter
		Dim dr As New DataSet
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_COMPROBAR_EMPRESA"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@IDLINEA", idLinea)
			cm.Parameters.AddWithValue("@IDEMPRESA", iEmp)
			cm.Parameters.AddWithValue("@ESPEDIDOABIERTO", EsPedidoAbierto)
			cm.Parameters.AddWithValue("@ORGCOMPRAS", IIf(sOrgCompras Is Nothing, DBNull.Value, sOrgCompras))
			cm.Parameters.AddWithValue("@MODFAV", modFav)
			da.SelectCommand = cm
			da.Fill(dr)
			Return dr
		Catch ex As Exception
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
			da.Dispose()
		End Try
	End Function
	''' <summary>
	''' Borra los costes/descuentos de las lineas del pedido
	''' </summary>
	''' <remarks>Llamada desde COrden.ActualizarCesta. Tiempo Max: </remarks>
	Public Function ValidarReceptor(ByVal idOrden As Integer, ByVal sReceptor As String) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim da As New SqlDataAdapter
		Dim dr As New DataSet
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_VALIDAR_RECEPTOR"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@CESTA", idOrden)
			cm.Parameters.AddWithValue("@RECEPTOR", IIf(sReceptor = Nothing, DBNull.Value, sReceptor))
			da.SelectCommand = cm
			da.Fill(dr)
			Return dr
		Catch ex As Exception
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
			da.Dispose()
		End Try
	End Function
	''' <summary>
	''' Comprueba las cantidades de la linea de la cesta determinada
	''' </summary>
	''' <param name="idLineaCesta">idLineaCesta</param>
	''' <remarks>Llamada desde emisionpedidos. Tiempo Max: </remarks>
	Public Function ComprobarCantidades(ByVal idLineaCesta As Integer, ByVal sUPed As String) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim da As New SqlDataAdapter
		Dim dr As New DataSet
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_VALIDAR_CANTIDAD"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@CESTA", idLineaCesta)
			cm.Parameters.AddWithValue("@UPACTUAL", sUPed)
			da.SelectCommand = cm
			da.Fill(dr)
			Return dr
		Catch ex As Exception
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
			da.Dispose()
		End Try
	End Function
	''' <summary>
	''' Comprueba si existe integracion
	''' </summary>
	''' <param name="idEmpresa">idEmpresa</param>
	''' <remarks>Llamada desde emisionpedidos. Tiempo Max: </remarks>
	Public Function Comprobar_Integracion(ByVal idEmpresa As Integer) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim da As New SqlDataAdapter
		Dim dr As New DataSet
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_COMPROBAR_INTEGRACION"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@EMPRESA", idEmpresa)
			da.SelectCommand = cm
			da.Fill(dr)
			Return dr
		Catch ex As Exception
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
			da.Dispose()
		End Try
	End Function
	Public Function Orden_DeshacerBorrarOrden(ByRef oDatosOrden As Object, ByVal bAccesoFSSM As Boolean, ByVal sUsuario As String) As Boolean
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Dim bAccionOk As Boolean = True

		Try
			cn.Open()

			cm = New SqlCommand("BEGIN DISTRIBUTED TRANSACTION", cn)
			cm.ExecuteNonQuery()

			For Each oLinea As Object In oDatosOrden("Lineas")
				Dim iRes As Integer = LineaPedido_DarDeBaja(oLinea("Id"), bAccesoFSSM, oDatosOrden("IdOrden"), sUsuario, True, oLinea("ImporteBruto"), oLinea("ImporteCostes"), oLinea("ImporteDescuentos"), True, cn)

				If iRes <> 1 Then
					iRes = False
					Exit For
				End If
			Next

			cm = New SqlCommand(If(bAccionOk, "COMMIT TRANSACTION", "ROLLBACK TRANSACTION"), cn)
			cm.ExecuteNonQuery()

		Catch ex As Exception
			bAccionOk = False
			cm = New SqlCommand("ROLLBACK TRANSACTION", cn)
			cm.ExecuteNonQuery()
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try

		Return bAccionOk
	End Function
	''' <summary>Lleva a cabo el borrado lÃ³gico de una orden</summary>
	''' <returns></returns>
	Public Function Orden_BorrarOrden(ByVal OrdenId As Integer, ByVal Usu As String, ByVal Per As String, ByVal AccesoFSIM As Boolean, ByVal dImporteTotalOrden As Double, ByVal dImporteCostes As Double, ByVal dImporteDescuentos As Double) As Boolean
		Authenticate()

		Dim cm As New SqlCommand
		Dim cn As New SqlConnection(mDBConnection)
		Dim iResultado As Integer = 0

		Using cn
			Try
				cn.Open()

				cm = New SqlCommand("BEGIN DISTRIBUTED TRANSACTION", cn)
				cm.ExecuteNonQuery()

				cm = New SqlCommand
				cm.Connection = cn
				cm.CommandText = "FSN_BAJA_LOGICA_PEDIDO"
				cm.CommandType = CommandType.StoredProcedure
				cm.Parameters.AddWithValue("@ID", OrdenId)
				cm.Parameters.AddWithValue("@CAMBIA_EST_USU", Usu)
				cm.Parameters.AddWithValue("@CAMBIA_EST_PER", Per)
				cm.Parameters.AddWithValue("@ACTIVOSM", BooleanToSQLBinary(AccesoFSIM))
				cm.Parameters.AddWithValue("@TIPOLOGPEDIDO", TLog_Pedido.Borrar_Pedido)
				cm.ExecuteNonQuery()

				'Actualizar los importes de la orden
				cm = New SqlCommand
				cm.Connection = cn
				cm.CommandText = "UPDATE ORDEN_ENTREGA SET COSTES=@COSTES,DESCUENTOS=@DESCUENTOS,IMPORTE=@IMPORTE WHERE ID=@ID"
				cm.Parameters.AddWithValue("@COSTES", dImporteCostes)
				cm.Parameters.AddWithValue("@DESCUENTOS", dImporteDescuentos)
				cm.Parameters.AddWithValue("@IMPORTE", dImporteTotalOrden)
				cm.Parameters.AddWithValue("@ID", OrdenId)
				cm.ExecuteNonQuery()

				cm = New SqlCommand("COMMIT TRANSACTION", cn)
				cm.ExecuteNonQuery()

				iResultado = 1
			Catch ex As Exception
				cm = New SqlCommand("ROLLBACK TRANSACTION", cn)
				cm.ExecuteNonQuery()
				Throw ex
			Finally
				cm.Dispose()
				cn.Close()
				cn.Dispose()
			End Try
		End Using

		Return iResultado
	End Function
	''' <summary>
	''' Cambia el estado de la orden cuyo ID se pasa como parámetro a anulado (20)
	''' Inserta en la tabla de log la accion realizada
	''' </summary>
	''' <param name="OrdenID">Id de la orden a anular</param>
	''' <param name="sCodPersona">Codigo Persona</param>
	''' <param name="sIdioma">Idioma</param>
	''' <param name="bActivoSm">Si hay SM o no</param>
	''' <returns>1 si ha ido bien, 0 si se ha producido un error</returns>
	''' <remarks>Llamada desde COrden.vb->AnularOrden. Tiempo máx. inferior a 1 seg. revisado EPB(06/06/2016)</remarks>
	Public Function Orden_AnularOrden(ByVal OrdenID As Integer, ByVal sCodPersona As String, ByVal sIdioma As String, ByVal bActivoSM As Boolean) As Integer
		Dim cm As New SqlCommand
		Dim cn As New SqlConnection(mDBConnection)
		Dim iResultado As Integer = 0

		Using cn
			Try
				cn.Open()
				cm.Connection = cn
				cm.CommandText = "FSGS_ANULAR_ORDEN_ENTREGA"
				cm.CommandType = CommandType.StoredProcedure
				cm.Parameters.AddWithValue("@ID", OrdenID)
				cm.Parameters.AddWithValue("@CODPERSONA", sCodPersona)
				cm.Parameters.AddWithValue("@SM_ACTIVO", bActivoSM)
				cm.Parameters.AddWithValue("@IDIOMA", sIdioma)

				cm.Transaction = cn.BeginTransaction()
				cm.ExecuteNonQuery()
				cm.Transaction.Commit()
				iResultado = 1
			Catch ex As Exception
				cm.Transaction.Rollback()
				Throw ex
			Finally
				cm.Dispose()
				cn.Close()
				cn.Dispose()
			End Try
		End Using

		Return iResultado

	End Function
    ''' <summary>Realiza las comprobaciones previas al borrado (lÃ³gico) de un pedido</summary>
    ''' <param name="OrdenID">Id Orden</param>
    ''' <param name="EsPedidoAbierto">Pedido Abierto o estÃ¡ndar</param>
    ''' <param name="Estado">Estado orden</param>
    ''' <param name="EsAbonoONorecepcionable">Es abono o no recepcionable</param>
    ''' <param name="TipoAccesoFSIM">Tipo acceso FSIM</param>
    ''' <returns></returns>
    Public Function Orden_ComprobarBorrarOrden(ByVal OrdenID As Integer, ByVal EsPedidoAbierto As Boolean, ByVal Estado As Integer, ByVal EsAbonoONorecepcionable As Boolean, ByVal TipoAccesoFSIM As Integer) As Integer
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand()
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_COMPROBACIONES_BORRADO_LOGICO_PEDIDO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@ID_ORDEN_ENTREGA", OrdenID)
            cm.Parameters.AddWithValue("@MODO_FUNCIONAMIENTO", IIf(EsPedidoAbierto, 1, 0))
            cm.Parameters.AddWithValue("@ESTADO_PEDIDO", Estado)
            cm.Parameters.AddWithValue("@ESABONO_O_NORECEPCIONABLE", EsAbonoONorecepcionable)
            cm.Parameters.AddWithValue("@TIPOACCESOFSIM", TipoAccesoFSIM)
            cm.Parameters.Add("@MOTIVO_NO_BORRAR", SqlDbType.Int, 0)
            cm.Parameters("@MOTIVO_NO_BORRAR").Direction = ParameterDirection.Output
            da.SelectCommand = cm
            da.Fill(ds)
            Return ds.Tables(0).Rows(0)(0)

        Catch ex As Exception
            cm.Transaction.Rollback()
            Throw ex
        Finally
            cm.Dispose()
            cn.Close()
            cn.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>Comprueba si un pedido o linea de pedido tiene hitos de facturacion</summary>
    ''' <param name="OrdenID">Id Orden</param>
    ''' <param name="LineaId">Id línea pedido</param>
    ''' <returns>Booleano indicando si la orden o línea tienen hitos de facturación</returns>
    Public Function Orden_ComprobarTieneHitosFacturacion(ByVal OrdenID As Integer, Optional ByVal LineaId As Integer = 0) As Boolean
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand()
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_COMPROBAR_HITOS_FACTURACION"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@ID_ORDEN_ENTREGA", OrdenID)
            If LineaId > 0 Then cm.Parameters.AddWithValue("@ID_LINEA_PEDIDO", LineaId)
            da.SelectCommand = cm
            da.Fill(ds)
            Return CType(ds.Tables(0).Rows(0)("HAY_HITOS"), Boolean)

        Catch ex As Exception
            Throw ex
        Finally
            cm.Dispose()
            cn.Close()
            cn.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>Realiza las comprobaciones previas a deshacer el borrado (lÃƒÂ³gico) de un pedido</summary>
    ''' <param name="OrdenID">Id Orden</param>
    ''' <param name="Estado">Estado orden</param>
    ''' <param name="TipoPedido">Tipo de pedido</param>
    ''' <param name="CadenaLineas">Tipo acceso FSIM</param>
    ''' <returns></returns>
    Public Function Orden_ComprobarDeshacerBorrarOrden(ByVal OrdenID As Integer, ByVal Estado As Integer, ByVal TipoPedido As Integer, ByVal CadenaLineas As String) As Object
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Dim da As New SqlDataAdapter
		Dim ds As New DataSet
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSN_COMPROBACIONES_DESHACER_BORRADO_LOGICO_PEDIDO"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@IDORDEN", OrdenID)
			cm.Parameters.AddWithValue("@ESTADO_PEDIDO", Estado)
			cm.Parameters.AddWithValue("@TIPO_PEDIDO", TipoPedido)
			cm.Parameters.AddWithValue("@LINEAS", CadenaLineas)
			cm.Parameters.Add("@MOTIVO_NO_DESHACER_BORRADO", SqlDbType.Int)
			cm.Parameters("@MOTIVO_NO_DESHACER_BORRADO").Direction = ParameterDirection.Output
			cm.Parameters.Add("@LINEAS_NO_DESHACER_BORRADO", SqlDbType.VarChar, 2000)
			cm.Parameters("@LINEAS_NO_DESHACER_BORRADO").Direction = ParameterDirection.Output
			cm.Parameters.Add("@LINEAS_A_DESHACER_BORRADO", SqlDbType.VarChar, 2000)
			cm.Parameters("@LINEAS_A_DESHACER_BORRADO").Direction = ParameterDirection.Output

			da.SelectCommand = cm
			da.Fill(ds)
			Return New With {.MotivoNoDeshacerBorradoLogicoPedido = ds.Tables(0).Rows(0)(0), .LineasNoDeshacer = ds.Tables(0).Rows(0)(1), .LineasADeshacer = ds.Tables(0).Rows(0)(2)}

		Catch ex As Exception
			cm.Transaction.Rollback()
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
			da.Dispose()
		End Try
	End Function
	''' <summary>
	''' Funcion que comprueba si una orden de tipo pedido Recepcion Obligatoria tiene recepciones todas las lineas
	''' </summary>
	''' <param name="OrdenId">Ids de la orden a comprobar si se puede cerrar</param>    
	''' <returns>TRUE->se puede Cerrar FALSE->No se puede cerrar, alguna de las lÃ­neas no estÃ¡ recepcionada</returns>
	''' <remarks></remarks>

	Public Function Orden_ComprobarCerrarOrden(ByVal OrdenId As Integer) As Boolean
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim bCerrarOrden As Boolean = False
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSN_ORDEN_CON_RECEPCIONES_EN_TODAS_LAS_LINEAS"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("ORDENID", OrdenId)
			bCerrarOrden = cm.ExecuteScalar
			Return bCerrarOrden
		Catch ex As Exception
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Function
	''' Revisado por: blp. Fecha: 17/02/2012
	''' <summary>
	''' Función que cierra la orden
	''' </summary>
	''' <param name="Orden">id de la orden a cerrar</param>
	''' <param name="Persona">Código de la persona que efectúa el cambio</param>
	''' <remarks>llamada desde COrden.vb. Tiempo Máx inferior a 1 seg.</remarks>
	Public Function Orden_CerrarOrden(ByVal Orden As Integer, ByVal Persona As String) As Integer
		Authenticate()
		Dim iResultado As Integer = 0
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "EP_MOD_CERRAR_ORDEN"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@ORDEN", Orden)
			cm.Parameters.AddWithValue("@PER", Persona)
			cm.Transaction = cn.BeginTransaction()
			cm.ExecuteNonQuery()
			cm.Transaction.Commit()
			iResultado = 1
		Catch ex As Exception
			cm.Transaction.Rollback()
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
		Return iResultado
	End Function
	''' <summary>
	''' Funcion que cierra las ordenes
	''' </summary>
	''' <param name="Ordenes">Ids de las ordenes a cerrar</param>
	''' <param name="Persona">Codigo de la persona que efectua el cambio</param>
	''' <returns></returns>
	''' <remarks>ilg 17/09/2012</remarks>
	Public Function Orden_CerrarOrdenes(ByVal Ordenes As List(Of Integer), ByVal Persona As String) As Integer
		Authenticate()
		Dim iResultado As Integer = 0
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Try
			cn.Open()
			cm.Connection = cn
			cm.Transaction = cn.BeginTransaction()
			For Each orden As Integer In Ordenes
				cm.CommandText = "EP_MOD_CERRAR_ORDEN"
				cm.CommandType = CommandType.StoredProcedure
				cm.Parameters.Clear()
				cm.Parameters.AddWithValue("@ORDEN", orden)
				cm.Parameters.AddWithValue("@PER", Persona)
				cm.ExecuteNonQuery()
			Next
			cm.Transaction.Commit()
			iResultado = 1
		Catch ex As Exception
			cm.Transaction.Rollback()
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
		Return iResultado
	End Function
	''' Revisado por: blp. Fecha: 20/02/2012
	''' <summary>
	''' Función que reabre la orden
	''' </summary>
	''' <param name="Orden">id de la orden a reabrir</param>
	''' <param name="Persona">Código de la persona que efectúa el cambio</param>
	''' <returns>
	''' Devuelve el estado en el que se deja la orden. 
	''' En el stored se comprueba si la orden tiene facturas. Si tiene, no se actuliza y devuelve 0.
	''' </returns>
	''' <remarks>Llamada desde COrden.vb. Tiempo Máx inferior a 1 seg.</remarks>
	Public Function Orden_ReabrirOrden(ByVal Orden As Integer, ByVal Persona As String) As Integer
		Authenticate()
		Dim iRdo As Integer = 0
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "EP_MOD_REABRIR_ORDEN"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@ORDEN", Orden)
			cm.Parameters.AddWithValue("@PER", Persona)
			cm.Transaction = cn.BeginTransaction()
			iRdo = cm.ExecuteScalar()
			cm.Transaction.Commit()
		Catch ex As Exception
			cm.Transaction.Rollback()
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
		Return iRdo
	End Function
	''' Revisado por: blp. Fecha: 28/02/2012
	''' <summary>
	''' Devuelve las observaciones de una orden
	''' </summary>
	''' <param name="idOrden">Id de la orden cuyas observaciones se desean</param>
	''' <returns>String Observaciones</returns>
	''' <remarks>Llamada desde COrden.vb->GetObservaciones(). Máx 0,1 seg.</remarks>
	Public Function Orden_GetObservaciones(ByVal idOrden As Integer) As String
		Authenticate()
		Dim sObs As String = ""
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "EP_OBT_ORDEN_OBS"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@IDORDEN", idOrden)
			sObs = cm.ExecuteScalar()
		Catch ex As Exception
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
		Return sObs
	End Function
	''' Revisado por: blp. Fecha: 28/02/2012
	''' <summary>
	''' Devuelve el comentario del proveedor de una orden
	''' </summary>
	''' <param name="idOrden">Id de la orden cuyas comentario del proveedor se desea recuperar</param>
	''' <returns>String comentario del proveedor</returns>
	''' <remarks>Llamada desde COrden.vb->GetComentarioProve(). Máx 0,1 seg.</remarks>
	Public Function Orden_GetComentarioProve(ByVal idOrden As Integer) As String
		Authenticate()
		Dim oCom As Object = ""
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "EP_OBT_ORDEN_COMENTPROVE"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@IDORDEN", idOrden)
			oCom = cm.ExecuteScalar()
			If oCom Is Nothing Then
				oCom = String.Empty
			End If
		Catch ex As Exception
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
		Return oCom
	End Function

	''' Revisado por: blp. Fecha: 30/11/2012
	''' <summary>
	''' Función que devolverá un valore boolean indicado si la orden pasada como parámetro tiene recepciones asociadas (TRUE)
	''' o no las tiene (FALSE)
	''' </summary>
	''' <param name="OrdenId">ID de la orden</param>
	''' <returns>TRUE->tiene recepciones FALSE->No tiene</returns>
	''' <remarks>tiempo máx inferior a 0,3seg</remarks>
	Public Function Orden_TieneRecepciones(ByVal OrdenId As Integer) As Boolean
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim bTieneRecepciones As Boolean = False
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "EP_OBT_ORDEN_TIENERECEPCIONES"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("ORDENID", OrdenId)
			bTieneRecepciones = cm.ExecuteScalar
			Return bTieneRecepciones
		Catch ex As Exception
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Function
	''' <summary>
	''' FunciÃƒÂ³n que devolverÃƒÂ¡ un valor boolean indicando si la orden pasada como parÃƒÂ¡metro tiene error de integracion en recepciones borradas (TRUE)
	''' o no las tiene (FALSE)
	''' </summary>
	''' <param name="OrdenId">ID de la orden</param>
	''' <returns>TRUE->tiene error de integracion en recepciones borradas  FALSE->No tiene</returns>
	''' <remarks>tiempo mÃƒÂ¡x inferior a 0,3seg</remarks>

	Public Function Orden_TieneRecepcionesIntKO(ByVal OrdenId As Integer) As Boolean
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim bTieneRecepcionesIntKO As Boolean = False
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSN_ORDEN_CON_RECEPCIONES_INT_KO"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("ORDENID", OrdenId)
			bTieneRecepcionesIntKO = cm.ExecuteScalar
			Return bTieneRecepcionesIntKO
		Catch ex As Exception
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Function
	Public Function Orden_getIdOrdenDesdeInstancia(ByVal idInstancia As Long) As Long
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim idOrden As Long
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "EP_OBT_ORDEN_DEINSTANCIA"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@IDINSTANCIA", idInstancia)
			idOrden = cm.ExecuteScalar
			Return idOrden
		Catch ex As Exception
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Function
	Public Sub Actualizar_Pres_Cesta(ByVal IdCesta As Integer, ByVal dtPres As DataTable)
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim da As New SqlDataAdapter
		cn.Open()
		Dim transaccion As SqlTransaction = cn.BeginTransaction()
		Try
			cm.CommandText = "FSEP_ELIMINAR_CESTA_PRES"
			cm.CommandType = CommandType.StoredProcedure
			cm.CommandTimeout = 300
			cm.Transaction = transaccion
			cm.Connection = cn
			cm.Parameters.AddWithValue("@CESTA", IdCesta)
			cm.ExecuteNonQuery()

			cm = New SqlCommand
			cm.CommandText = "FSEP_ACTUALIZAR_CESTA_PRES"
			cm.CommandType = CommandType.StoredProcedure
			cm.CommandTimeout = 300
			cm.Transaction = transaccion
			cm.Connection = cn
			cm.Parameters.AddWithValue("@CESTA", IdCesta)
			cm.Parameters.Add("@PRES1", SqlDbType.Int, 4, "PRES1")
			cm.Parameters.Add("@PRES2", SqlDbType.Int, 4, "PRES2")
			cm.Parameters.Add("@PRES3", SqlDbType.Int, 4, "PRES3")
			cm.Parameters.Add("@PRES4", SqlDbType.Int, 4, "PRES4")
			cm.Parameters.Add("@PORCEN", SqlDbType.Float, 8, "PORCEN")
			cm.Parameters.Add("@TIPO", SqlDbType.Int, 4, "TIPO")
			da.InsertCommand = cm
			da.Update(dtPres)
			transaccion.Commit()
		Catch ex As Exception
			transaccion.Rollback()
			Throw ex
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Sub
	Public Sub ActualizarPlanesEntregaLinea(ByVal idCesta As Long, ByVal dtPlanEntrega As DataTable)
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim da As New SqlDataAdapter
		cn.Open()
		Dim transaccion As SqlTransaction = cn.BeginTransaction()
		Try
			cm.CommandText = "FSEP_ELIMINAR_PLAN_ENTREGA_LINEA"
			cm.CommandType = CommandType.StoredProcedure
			cm.Transaction = transaccion
			cm.Connection = cn
			cm.Parameters.AddWithValue("@CESTA", idCesta)
			cm.ExecuteNonQuery()

			cm = New SqlCommand
			cm.CommandText = "FSEP_ACTUALIZAR_PLAN_ENTREGA_LINEA"
			cm.CommandType = CommandType.StoredProcedure
			cm.CommandTimeout = 300
			cm.Transaction = transaccion
			cm.Connection = cn
			cm.Parameters.AddWithValue("@CESTA", idCesta)
			cm.Parameters.Add("@FECHAENTREGA", SqlDbType.DateTime, -1, "FECHAENTREGA")
			cm.Parameters.Add("@CANTIDAD", SqlDbType.Float, 0, "CANTIDAD")
			cm.Parameters.Add("@IMPORTE", SqlDbType.Float, 0, "IMPORTE")
			da.InsertCommand = cm
			da.Update(dtPlanEntrega)

			transaccion.Commit()
		Catch ex As Exception
			transaccion.Rollback()
			Throw ex
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Sub
#End Region
#Region " Ordenes data access methods"
	''' Revisado por: blp. Fecha: 05/03/2012
	''' <summary>
	''' Recupera los últimos pedidos del aprovisionador indicado.
	''' </summary>
	''' <param name="Estado">Filtro de pedidos según su estado.</param>
	''' <param name="CodAprovisionador">Código del Aprovisionador</param>
	''' <param name="NumLineas">Parámetro de entrada/salida. Indica el nº de pedidos a devolver y devuelve el nº total de pedidos existente.</param>
	''' <param name="pendientesAprobar">True->Mostrar pedidos pendientes de aprobar por el usuario al que corresponde el codAprovisionador</param>
	''' <param name="pendientesRecepcionar">True->Mostrar pedidos pendientes de recepcionar por el usuario al que corresponde el codAprovisionador. 
	''' El stored también comprueba si tiene permiso para recepcionar pedidos de sus Centros de Coste y si tiene configurado que se muestren. Si es así, también los muestra.
	''' </param>
	''' <returns>Un dataset con los pedidos que cumplen los criterios ordenados por fecha de emisión descendente.</returns>
	''' <remarks>
	''' Llamadas desde: COrdenes.BuscarPorAprovisionador
	''' Tiempo máximo: 0 sec.
	''' </remarks>
	Public Function Ordenes_GetOrdenesAprovisionador(ByVal Estado As Integer, ByVal CodAprovisionador As String,
			ByVal PermisoRecepcionarPedidosCCImputablesAprov As Boolean, ByVal RecepcionVerPedidosCCImputablesAprov As Boolean,
			ByRef NumLineas As Integer, ByVal pendientesAprobar As Boolean, ByVal pendientesRecepcionar As Boolean) As DataSet
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Using cn
			Dim cm As SqlCommand = New SqlCommand("FSEP_GET_ORDENES_APROVISIONADOR", cn)
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.Add("@NUMLINEAS", SqlDbType.Int, 0)
			cm.Parameters("@NUMLINEAS").Direction = ParameterDirection.InputOutput
			cm.Parameters("@NUMLINEAS").Value = NumLineas
			cm.Parameters.Add("@APROVISIONADOR", SqlDbType.VarChar, 50)
			cm.Parameters("@APROVISIONADOR").Value = CodAprovisionador
			cm.Parameters.Add("@EST", SqlDbType.Int, 0)
			cm.Parameters("@EST").Value = Estado
			cm.Parameters.Add("@PENDIENTESAPROBAR", SqlDbType.Bit)
			cm.Parameters("@PENDIENTESAPROBAR").Value = pendientesAprobar
			cm.Parameters.Add("@PENDIENTESRECEPCIONAR", SqlDbType.Bit)
			cm.Parameters("@PENDIENTESRECEPCIONAR").Value = pendientesRecepcionar
			cm.Parameters.AddWithValue("@PERMISORECEPCIONAR_CC", PermisoRecepcionarPedidosCCImputablesAprov)
			cm.Parameters.AddWithValue("@VERENRECEPCIONPEDIDOS_CC", RecepcionVerPedidosCCImputablesAprov)
			Dim da As New SqlDataAdapter
			Dim ds As New DataSet
			da.SelectCommand = cm
			da.Fill(ds)
			NumLineas = cm.Parameters("@NUMLINEAS").Value
			Return ds
		End Using
	End Function
	''' Revisado por: blp. Fecha: 30/11/2012
	''' <summary>
	''' Trae todas las ordenes de entrega
	''' </summary>
	''' <param name="Idioma">Idioma del usuario</param>
	''' <param name="codPer">Codigo del usuario aprovisionador</param>
	''' <param name="Anyo">Año del pedido</param>
	''' <param name="NumPed">Número dado al pedido (cesta) emitido en la aplicación</param>
	''' <param name="NumOrden">Número dado a la orden (pedido) emitida en la aplicación</param>
	''' <param name="NumPedProve">Nº de pedido dado por el proveedor</param>
	''' <param name="NumPedidoErp">Número de pedido dado en el ERP del cliente a la orden</param>
	''' <param name="FecEmisionDesde">Fecha a partir de la cual filtrar la fecha de emisión del pedido</param>
	''' <param name="FecEmisionHasta">Fecha hasta la cual filtrar la fecha de emisión</param>
	''' <param name="NumMeses">Número de meses desde el día de hoy hacia atrás de los que hay que devolver los pedidos</param>
	''' <param name="Empresa">Código de la empresa</param>
	''' <param name="Receptor">Codigo del receptor</param>
	''' <param name="Prove">Codigo del proveedor</param>
	''' <param name="Prove_Erp">Codigo del proveedor en el ERP</param>
	''' <param name="TipoPedido">Tipo de pedido</param>
	''' <param name="Estados">Valor sumado de los estados del pedido que se desean ver</param>
	''' <param name="Ver_Abonos">Valor 1: Ver los pedidos que son Abono. Valor 0: No verlos</param>
	''' <param name="Ver_Ped_Ep">Valor 1: Ver los pedidos de tipo (origen) EP (TIPO=1). Valor 0: No verlos</param>
	''' <param name="Ver_Ped_Ep_Libres">Valor 1: Ver los pedidos de tipo (origen) EP (TIPO=1) LIBRES. Valor 0: No verlos</param>
	''' <param name="Ver_Ped_Neg">Valor 1: Ver los pedidos de tipo (origen) GS (TIPO=0). Valor 0: No verlos</param>
	''' <param name="Ver_Ped_Erp">Valor 1: Ver los pedidos de tipo (origen) ERP (TIPO=2). Valor 0: No verlos</param>
	''' <param name="Usu_Ped">Valor 1: Ver los pedidos del usuario. Valor 0: No verlos</param>
	''' <param name="Cc_Ped">Valor 1: Ver los pedidos que no sean del aprovisionador y que sean de su CC. Valor 0: No verlos</param>
	''' <param name="Articulo">Texto a buscar como una parte del código del artículo, de su descripción, o de otros campos descriptivos del artículo</param>
	''' <param name="Categorias">Conjunto de códigos de partidas categorías pasados en un tipo tabla</param>
	''' <param name="CentroCoste">Código del centro de coste</param>
	''' <param name="PartidasPres">Conjunto de códigos de partidas presupuestarias pasados en un tipo tabla</param>
	''' <param name="AnyoFra">Año de la factura</param>
	''' <param name="NumFra">Número de la factura (búsqueda exacta, no aproximada)</param>
	''' <param name="EstFra">Estado de la factura</param>
	''' <param name="FecFraDesde">Fecha a partir de la cual filtrar la fecha de factura</param>
	''' <param name="FecFraHasta">Fecha hasta la cual filtrar la fecha de factura</param>
	''' <param name="NumPago">Número del pago (búsqueda exacta, no aproximada)</param>
	''' <param name="EstPago">Estado del pago</param>
	''' <param name="FecPagoDesde">Fecha a partir de la cual filtrar la fecha de pago</param>
	''' <param name="FecPagoHasta">Fecha hasta la cual filtrar la fecha de pago</param>
	''' <param name="gestor">gestor de las partidas a las que se han imputado las líneas de cada pedido</param>
	''' <param name="VerPedidosBloqueados">Ver los pedidos que tienen albaranes bloqueados</param>
	''' <param name="PageNumber">Número de página a devolver</param>
	''' <param name="PageSize">Número de registros a devolver</param>
	''' <param name="SortExpression">Campo por el que se va a filtrar</param>
	''' <param name="SortOrder">Orden de los datos: Ascendente (ASC) o descendente (DESC). Por defecto ASC</param>
	''' <param name="TotalRegistros">OUTPUT (BYREF)
	'''		A través de este parámetro devolvemos el total de registros. Se devuelve sólo cuando se solicita la página 1. Esto implica 
	'''		suponer que siempre que se cambien las condiciones de filtro se vuelve a la página 1.
	'''		Valor 0: No hay registros.
	'''		Valor -1: Se está cambiando de página sobre un filtrado previo por lo que el valor no se devuelve y la aplicación puede 
	'''		seguir usando el valor que conserve en memoria.
	'''     Valor -2: Se ha producido un error al recuperar los datos.
	''' </param>
	''' <param name="ParaExportacion">Indica si vamos a usar los datos para mostrar en Seguimiento o Exportar. Si es para exportar, no se paginan (se usa distinto Procedimiento almacenado)</param>
	''' <returns>Un dataset con una tabla de ORDENES y un parámetro Output con el total de registros. Máx. 2 seg.</returns>
	Public Function Ordenes_BuscarTodasOrdenes(ByVal Idioma As FSNLibrary.Idioma, ByVal codPer As String, ByVal Anyo As Integer, ByVal NumPed As Integer, ByVal NumOrden As Integer, ByVal NumPedProve As String, ByVal NumPedidoErp As String, ByVal FecEmisionDesde As Date, ByVal FecEmisionHasta As Date, ByVal NumMeses As Integer, ByVal Empresa As Integer, ByVal Receptor As String, ByVal Prove As String, ByVal Prove_Erp As String, ByVal TipoPedido As Integer, ByVal Estados As DataTable, ByVal Ver_Abonos As Boolean, ByVal Ver_Ped_Ep As Boolean, ByVal Ver_Ped_Ep_Libres As Boolean, ByVal Ver_Ped_Neg As Boolean, ByVal Ver_Ped_Erp As Boolean, ByVal Usu_Ped As Boolean, ByVal Cc_Ped As Boolean, ByVal Articulo As DataTable, ByVal Categorias As DataTable, ByVal CentroCoste As String, ByVal PartidasPres As DataTable, ByVal AnyoFra As Integer, ByVal NumFra As String, ByVal EstFra As Integer, ByVal FecFraDesde As Date, ByVal FecFraHasta As Date, ByVal NumPago As String, ByVal EstPago As Integer, ByVal FecPagoDesde As Date, ByVal FecPagoHasta As Date, ByVal gestor As String, ByVal VerPedidosBloqueados As Boolean, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal SortExpression As String, ByVal SortOrder As String, ByRef TotalRegistros As Integer, Optional ByVal ParaExportacion As Boolean = False, Optional ByVal Ver_ContraAbiertos As Boolean = False,
												Optional ByVal iAnyoPedAbierto As Integer = Nothing, Optional ByVal iCestaPedAbierto As Long = Nothing, Optional ByVal iPedidoPedAbierto As Long = Nothing, Optional ByVal Ver_Ped_Express As Boolean = False, Optional ByVal VerPedidosBorrados As Boolean = False) As DataSet
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Dim da As New SqlDataAdapter
		Dim ds As New DataSet
		Try
			cm.Connection = cn
			cm.CommandText = "EP_OBT_ORDENES_SEGUIMIENTO"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@IDIOMA", Idioma.ToString)
			cm.Parameters.AddWithValue("@PER", codPer)
			cm.Parameters.AddWithValue("@ANYO", Anyo)
			cm.Parameters.AddWithValue("@NUMPED", NumPed)
			cm.Parameters.AddWithValue("@NUMORDEN", NumOrden)
			cm.Parameters.AddWithValue("@NUMPEDPROVE", NumPedProve)
			cm.Parameters.AddWithValue("@NUMPEDIDOERP", NumPedidoErp)
			cm.Parameters.AddWithValue("@FECEMISIONDESDE", IIf(FecEmisionDesde = Date.MinValue, System.DBNull.Value, FecEmisionDesde))
			cm.Parameters.AddWithValue("@FECEMISIONHASTA", IIf(FecEmisionHasta = Date.MinValue, System.DBNull.Value, FecEmisionHasta))
			cm.Parameters.AddWithValue("@NUMMESES", NumMeses)
			cm.Parameters.AddWithValue("@EMPRESA", Empresa)
			cm.Parameters.AddWithValue("@RECEPTOR", Receptor)
			cm.Parameters.AddWithValue("@PROVE", Prove)
			cm.Parameters.AddWithValue("@PROVE_ERP", Prove_Erp)
			cm.Parameters.AddWithValue("@TIPOPEDIDO", TipoPedido)
			Dim ParamEstados As New SqlParameter("@ESTADOS", SqlDbType.Structured)
			ParamEstados.TypeName = "TBESTADO"
			ParamEstados.Value = Estados
			cm.Parameters.Add(ParamEstados)
			cm.Parameters.AddWithValue("@VER_ABONOS", Ver_Abonos)
			cm.Parameters.AddWithValue("@VER_PED_EP", Ver_Ped_Ep)
			cm.Parameters.AddWithValue("@VER_PED_EP_LIBRES", Ver_Ped_Ep_Libres)
			cm.Parameters.AddWithValue("@VER_PED_NEG", Ver_Ped_Neg)
			cm.Parameters.AddWithValue("@VER_PED_ERP", Ver_Ped_Erp)
			cm.Parameters.AddWithValue("@USU_PED", Usu_Ped)
			cm.Parameters.AddWithValue("@CC_PED", Cc_Ped)
			'Creamos un parámetro sql de tipo TBCADENATEXTO, que es un tipo "tabla" definido por el usuario en sql server
			Dim ParamArticulo As New SqlParameter("@ARTICULO", SqlDbType.Structured)
			ParamArticulo.TypeName = "TBCADENATEXTO"
			ParamArticulo.Value = Articulo
			cm.Parameters.Add(ParamArticulo)
			'Creamos un parámetro sql de tipo TBCATEGORIAS, que es un tipo "tabla" definido por el usuario en sql server
			Dim ParamCategorias As New SqlParameter("@CATEGORIAS", SqlDbType.Structured)
			ParamCategorias.TypeName = "TBCATEGORIAS"
			ParamCategorias.Value = Categorias
			cm.Parameters.Add(ParamCategorias)
			cm.Parameters.AddWithValue("@CENTROCOSTE", CentroCoste)
			'Creamos un parámetro sql de tipo TBPARTIDAS, que es un tipo "tabla" definido por el usuario en sql server
			Dim ParamPartidas As New SqlParameter("@PARTIDASPRES", SqlDbType.Structured)
			ParamPartidas.TypeName = "TBPARTIDAS"
			ParamPartidas.Value = PartidasPres
			cm.Parameters.Add(ParamPartidas)
			cm.Parameters.AddWithValue("@ANYOFRA", AnyoFra)
			cm.Parameters.AddWithValue("@NUMFRA", NumFra)
			cm.Parameters.AddWithValue("@ESTFRA", EstFra)
			cm.Parameters.AddWithValue("@FECFRADESDE", IIf(FecFraDesde = Date.MinValue, System.DBNull.Value, FecFraDesde))
			cm.Parameters.AddWithValue("@FECFRAHASTA", IIf(FecFraHasta = Date.MinValue, System.DBNull.Value, FecFraHasta))
			cm.Parameters.AddWithValue("@NUMPAGO", NumPago)
			cm.Parameters.AddWithValue("@ESTPAGO", EstPago)
			cm.Parameters.AddWithValue("@FECPAGODESDE", IIf(FecPagoDesde = Date.MinValue, System.DBNull.Value, FecPagoDesde))
			cm.Parameters.AddWithValue("@FECPAGOHASTA", IIf(FecPagoHasta = Date.MinValue, System.DBNull.Value, FecPagoHasta))
			cm.Parameters.AddWithValue("@GESTOR", gestor)
			cm.Parameters.AddWithValue("@VER_ALBARANES_BLOQUEADOS", VerPedidosBloqueados)
			cm.Parameters.AddWithValue("@PAGENUMBER", PageNumber)
			If ParaExportacion Then
				cm.Parameters.AddWithValue("@EXPORTACION", 1)
			Else
				cm.Parameters.AddWithValue("@PAGESIZE", PageSize)
			End If
			cm.Parameters.AddWithValue("@SORTEXPRESSION", SortExpression)
			cm.Parameters.AddWithValue("@SORTORDER", SortOrder)
			Dim TotalReg As New SqlParameter("@TOTALREGISTROS", SqlDbType.Int)
			TotalReg.Direction = ParameterDirection.Output
			TotalReg.Value = TotalRegistros
			cm.Parameters.Add(TotalReg)
			cm.Parameters.AddWithValue("@VER_CONTRAABIERTOS", Ver_ContraAbiertos)
			If iAnyoPedAbierto <> 0 Then
				cm.Parameters.AddWithValue("@ANYO_PED_ABIERTO", iAnyoPedAbierto)
			End If
			If iCestaPedAbierto <> 0 Then
				cm.Parameters.AddWithValue("@CESTA_PED_ABIERTO", iCestaPedAbierto)
			End If
			If iPedidoPedAbierto <> 0 Then
				cm.Parameters.AddWithValue("@PEDIDO_PED_ABIERTO", iPedidoPedAbierto)
			End If
			cm.Parameters.AddWithValue("@VER_PED_EXPRESS", Ver_Ped_Express)
			cm.Parameters.AddWithValue("@VER_PED_BORRADOS", VerPedidosBorrados)
			cm.CommandTimeout = 300
			cn.Open()
			da.SelectCommand = cm
			da.Fill(ds)

			TotalRegistros = TotalReg.Value
			Return ds
		Catch ex As Exception
			TotalRegistros = -2
			Throw ex
		Finally
			da.Dispose()
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Function
	''' Revisado por: blp. Fecha: 16/01/2012	
	''' <summary>
	''' Trae todas las ordenes de entrega RECEPCIONABLES
	''' </summary>
	''' <param name="Idioma">Código de idioma</param>
	''' <param name="CodPersona">Código de la tabla PER, del usuario del que queremos cargar las recepciones</param>
	''' <param name="Anyo">Año</param>
	''' <param name="NumPedido">Nº pedido</param>
	''' <param name="NumOrden">Nº de orden</param>
	''' <param name="NumPedidoErp">Nº de pedido en el ERP</param>
	''' <param name="fecRecepDesde">Fecha de recepción del pedido desde la que se van a devolver datos</param>
	''' <param name="fecRecepHasta">Fecha de recepción del pedido hasta la que se van a devolver datos</param>
	''' <param name="NumAlbaran">Nº Albarán</param>
	''' <param name="NumPedProve">Nº de pedido en el proveedor</param>
	''' <param name="Articulo">Parte o todo el código o descripción del artículo. Los parámetros Articulo y codArticulo no son excluyentes, dado que pueden servir para distinguir los artículos genéricos.</param>
	''' <param name="codArticulo">Código completo del artículo. Los parámetros Articulo y codArticulo no son excluyentes, dado que pueden servir para distinguir los artículos genéricos.</param>
	''' <param name="CodProve">Código del proveedor</param>
	''' <param name="ProveERP">Código del proveedor en el ERP</param>
	''' <param name="TipoPedido">Tipo de pedido: 0 GS, 1 EP, 2 ERP</param>
	''' <param name="Categorias">Categoría de artículos</param>
	''' <param name="fecEntregaSolicitDesde">Fecha de entrega solicitada por el aprovisionador desde la que se van a devolver datos</param>
	''' <param name="fecEntregaSolicitHasta">Fecha de entrega solicitada por el aprovisionador hasta la que se van a devolver datos</param>
	''' <param name="CodEmpresa">Id de la empresa</param>
	''' <param name="fecEntregaIndicadaDesde">Fecha de entrega indicada por el proveedor o indicada en el plan de entrega desde la que se van a devolver datos</param>
	''' <param name="fecEntregaIndicadaHasta">Fecha de entrega indicada por el proveedor o indicada en el plan de entrega hasta la que se van a devolver datos</param>
	''' <param name="fecEmisionDesde">Fecha de emisión del pedido desde la que se van a devolver datos</param>
	''' <param name="fecEmisionHasta">Fecha de emisión del pedido hasta la que se van a devolver datos</param>
	''' <param name="centroCoste">Código del centro de coste</param>
	''' <param name="PartidasPres">Tabla con los PRES0, PRES1, PRES2,3,4 identificativos de las partidas</param>
	''' <param name="TipoOrigen">Tipo de Origen</param>
	''' <param name="Estados">Tabla con los estados del pedido</param>
	''' <param name="CodGestor">Código del gestor</param>
	''' <param name="bCargarPedidosUsu">1-> Mostrar los pedidos de los que el usuario es receptor</param>
	''' <param name="bCargarPedidosImpCC">
	''' 	1->
	''' 	Si el usuario no es gestor: ver las recepciones que no sean del usuario y que sean de sus cc imputables. 
	''' 	Si el usuario es gestor: únicamente verá aquellos pedidos imputados a partidas de las que es gestor. 
	''' </param>
	''' <param name="SmActivado">Estado del módulo de control presupuestario</param>
	''' <returns>Un dataset con dos tablas "ENTREGAS" (las recepciones) y "PARTIDAS_DEN" (las denominaciones de las partidas presupuestarias</returns>
	''' <remarks>Llamada desde COrdenes.vb-->BuscarTodasOrdenesRecepcion. Máx. 1 seg.</remarks>
	Public Function Ordenes_BuscarTodasOrdenesRecepcion(ByVal Idioma As FSNLibrary.Idioma, ByVal CodPersona As String, ByVal Anyo As Integer, ByVal NumPedido As Integer, ByVal NumOrden As Integer, ByVal NumPedidoErp As String, ByVal fecRecepDesde As Date, ByVal fecRecepHasta As Date, ByVal NumAlbaran As String, ByVal NumPedProve As String, ByVal Articulo As String, ByVal codArticulo As String, ByVal CodProve As String, ByVal ProveERP As String, ByVal TipoPedido As Integer, ByVal Categorias As DataTable, ByVal fecEntregaSolicitDesde As Date, ByVal fecEntregaSolicitHasta As Date, ByVal CodEmpresa As Integer, ByVal fecEntregaIndicadaDesde As Date, ByVal fecEntregaIndicadaHasta As Date, ByVal fecEmisionDesde As Date, ByVal fecEmisionHasta As Date, ByVal centroCoste As String, ByVal PartidasPres As DataTable, ByVal TipoOrigen As DataTable, ByVal Estados As DataTable, ByVal CodGestor As String, ByVal CodReceptor As String, ByVal bCargarPedidosUsu As Boolean, ByVal bCargarPedidosImpCC As Boolean, ByVal SmActivado As Boolean, ByVal bSoloPendientes As Boolean) As DataSet
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Dim ds As New DataSet
		Dim da As New SqlDataAdapter

		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandTimeout = 120
			cm.CommandText = "FSEP_GET_ORDENES_RECEPCION"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.Add("@IDIOMA", SqlDbType.NVarChar)
			cm.Parameters("@IDIOMA").Value = Idioma.ToString
			cm.Parameters.Add("@PER", SqlDbType.NVarChar)
			cm.Parameters("@PER").Value = CodPersona
			cm.Parameters.Add("@ANYO", SqlDbType.Int)
			cm.Parameters("@ANYO").Value = Anyo
			cm.Parameters.Add("@NUMPEDIDO", SqlDbType.Int)
			cm.Parameters("@NUMPEDIDO").Value = NumPedido
			cm.Parameters.Add("@NUMORDEN", SqlDbType.Int)
			cm.Parameters("@NUMORDEN").Value = NumOrden
			cm.Parameters.Add("@NUMPEDIDOERP", SqlDbType.NVarChar)
			cm.Parameters("@NUMPEDIDOERP").Value = NumPedidoErp
			cm.Parameters.Add("@FECRECEPDESDE", SqlDbType.Date)
			cm.Parameters("@FECRECEPDESDE").Value = IIf(fecRecepDesde = Date.MinValue, System.DBNull.Value, fecRecepDesde)
			cm.Parameters.Add("@FECRECEPHASTA", SqlDbType.Date)
			cm.Parameters("@FECRECEPHASTA").Value = IIf(fecRecepHasta = Date.MinValue, System.DBNull.Value, fecRecepHasta)
			cm.Parameters.Add("@NUMALBARAN", SqlDbType.NVarChar)
			cm.Parameters("@NUMALBARAN").Value = NumAlbaran
			cm.Parameters.Add("@NUMPEDPROVE", SqlDbType.NVarChar)
			cm.Parameters("@NUMPEDPROVE").Value = NumPedProve
			cm.Parameters.Add("@ARTICULO", SqlDbType.NVarChar)
			cm.Parameters("@ARTICULO").Value = Articulo
			cm.Parameters.Add("@CODARTICULO", SqlDbType.NVarChar)
			cm.Parameters("@CODARTICULO").Value = codArticulo
			cm.Parameters.Add("@PROVE", SqlDbType.NVarChar)
			cm.Parameters("@PROVE").Value = CodProve
			cm.Parameters.Add("@PROVE_ERP", SqlDbType.NVarChar)
			cm.Parameters("@PROVE_ERP").Value = ProveERP
			If TipoPedido <> 0 Then
				cm.Parameters.Add("@TIPOPEDIDO", SqlDbType.Int)
				cm.Parameters("@TIPOPEDIDO").Value = TipoPedido
			End If
			'Creamos un parámetro sql de tipo TBCATEGORIAS, que es un tipo "tabla" definido por el usuario en sql server
			Dim ParamTipoTabla As New SqlParameter("@CATEGORIAS", SqlDbType.Structured)
			ParamTipoTabla.TypeName = "TBCATEGORIAS"
			ParamTipoTabla.Value = Categorias
			cm.Parameters.Add(ParamTipoTabla)
			cm.Parameters.Add("@FECENTREGASOLICITDESDE", SqlDbType.Date)
			cm.Parameters("@FECENTREGASOLICITDESDE").Value = IIf(fecEntregaSolicitDesde = Date.MinValue, System.DBNull.Value, fecEntregaSolicitDesde)
			cm.Parameters.Add("@FECENTREGASOLICITHASTA", SqlDbType.Date)
			cm.Parameters("@FECENTREGASOLICITHASTA").Value = IIf(fecEntregaSolicitHasta = Date.MinValue, System.DBNull.Value, fecEntregaSolicitHasta)
			cm.Parameters.Add("@CODEMPRESA", SqlDbType.Int)
			cm.Parameters("@CODEMPRESA").Value = CodEmpresa
			cm.Parameters.Add("@FECENTREGAINDICADAPROVEDESDE", SqlDbType.Date)
			cm.Parameters("@FECENTREGAINDICADAPROVEDESDE").Value = IIf(fecEntregaIndicadaDesde = Date.MinValue, System.DBNull.Value, fecEntregaIndicadaDesde)
			cm.Parameters.Add("@FECENTREGAINDICADAPROVEHASTA", SqlDbType.Date)
			cm.Parameters("@FECENTREGAINDICADAPROVEHASTA").Value = IIf(fecEntregaIndicadaHasta = Date.MinValue, System.DBNull.Value, fecEntregaIndicadaHasta)
			cm.Parameters.Add("@FECEMISIONDESDE", SqlDbType.Date)
			cm.Parameters("@FECEMISIONDESDE").Value = IIf(fecEmisionDesde = Date.MinValue, System.DBNull.Value, fecEmisionDesde)
			cm.Parameters.Add("@FECEMISIONHASTA", SqlDbType.Date)
			cm.Parameters("@FECEMISIONHASTA").Value = IIf(fecEmisionHasta = Date.MinValue, System.DBNull.Value, fecEmisionHasta)
			cm.Parameters.Add("@CENTROCOSTE", SqlDbType.VarChar)
			cm.Parameters("@CENTROCOSTE").Value = centroCoste
			'Creamos un parámetro sql de tipo TBPARTIDAS, que es un tipo "tabla" definido por el usuario en sql server
			ParamTipoTabla = New SqlParameter("@PARTIDASPRES", SqlDbType.Structured)
			ParamTipoTabla.TypeName = "TBPARTIDAS"
			ParamTipoTabla.Value = PartidasPres
			cm.Parameters.Add(ParamTipoTabla)
			'Creamos un parámetro sql de tipo TBCATEGORIAS, que es un tipo "tabla" definido por el usuario en sql server
			ParamTipoTabla = New SqlParameter("@TIPOORIGEN", SqlDbType.Structured)
			ParamTipoTabla.TypeName = "TBTIPOORIGEN"
			ParamTipoTabla.Value = TipoOrigen
			cm.Parameters.Add(ParamTipoTabla)
			'Creamos un parámetro sql de tipo TBCATEGORIAS, que es un tipo "tabla" definido por el usuario en sql server
			ParamTipoTabla = New SqlParameter("@ESTADOS", SqlDbType.Structured)
			ParamTipoTabla.TypeName = "TBESTADO"
			ParamTipoTabla.Value = Estados
			cm.Parameters.Add(ParamTipoTabla)
			cm.Parameters.Add("@GESTOR", SqlDbType.VarChar)
			cm.Parameters("@GESTOR").Value = CodGestor
			cm.Parameters.Add("@RECEPTOR", SqlDbType.VarChar)
			cm.Parameters("@RECEPTOR").Value = CodReceptor
			cm.Parameters.Add("@USU_RECEP", SqlDbType.Bit)
			cm.Parameters("@USU_RECEP").Value = bCargarPedidosUsu
			cm.Parameters.Add("@CC_RECEP", SqlDbType.Bit)
			cm.Parameters("@CC_RECEP").Value = bCargarPedidosImpCC
			cm.Parameters.Add("@SMACTIVADO", SqlDbType.Bit)
			cm.Parameters("@SMACTIVADO").Value = SmActivado
			cm.Parameters.Add("@SOLOLINEASPENDIENTES", SqlDbType.Bit)
			cm.Parameters("@SOLOLINEASPENDIENTES").Value = bSoloPendientes
			da.SelectCommand = cm
			da.Fill(ds)
			cn.Close()
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
			Return ds
		Catch e As Exception
			Throw e
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Function
	''' Revisado por: blp. Fecha: 19/12/2011
	''' <summary>
	''' Busca las órdenes de entrega en las que figura como aprobador la persona indicada
	''' </summary>
	''' <param name="Aprobador">Código de Persona del aprobador</param>
	''' <param name="Idioma">Idioma en el que devolver los datos</param>
	''' <param name="fecDesde">Fecha de emisión del pedido desde la que se van a devolver datos</param>
	''' <param name="fecHasta">Fecha de emisión del pedido hasta la que se van a devolver datos</param>
	''' <param name="incluirAprobados">
	''' False: Incluir los pedidos ya aprobados.
	''' True: No Incluir los pedidos ya aprobados.
	''' </param>
	''' <param name="incluirDenegados">
	''' False: Incluir los pedidos denegados.
	''' True: No Incluir los pedidos denegados.
	''' </param>
	''' <param name="incluirAprobNivelSup">
	''' False: Incluir los pedidos pendientes de aprobar en un nivel superior.
	''' True: No Incluir los pedidos pendientes de aprobar en un nivel superior.
	''' </param>
	''' <returns>Un dataset con las órdenes de entrega. Máx. depende de la consulta</returns>
	Public Function Ordenes_BuscarOrdenesAprobador(ByVal Aprobador As String, ByVal Idioma As String, ByVal fecDesde As Date, ByVal fecHasta As Date, ByVal incluirAprobados As Boolean, ByVal incluirDenegados As Boolean, ByVal incluirAprobNivelSup As Boolean, ByVal bIncluirPedidosCatalogo As Boolean, ByVal bIncluirPedidosContraAbiertos As Boolean _
												   , Optional ByVal iAnyoPedAbierto As Integer = Nothing, Optional ByVal iCestaPedAbierto As Long = Nothing, Optional ByVal iPedidoPedAbierto As Long = Nothing) As DataSet
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Dim da As New SqlDataAdapter
		Dim ds As New DataSet
		Using cn
			Try
				cm.Connection = cn
				cm.CommandText = "FSEP_APROB_DEVOLVER_ORDENES"
				cm.CommandType = CommandType.StoredProcedure
				cm.Parameters.Add("@APROBADOR", SqlDbType.VarChar)
				cm.Parameters("@APROBADOR").Value = Aprobador
				cm.Parameters.Add("@IDIOMA", SqlDbType.VarChar)
				cm.Parameters("@IDIOMA").Value = Idioma
				cm.Parameters.Add("@FECDESDE", SqlDbType.Date)
				cm.Parameters("@FECDESDE").Value = IIf(fecDesde = Date.MinValue, System.DBNull.Value, fecDesde)
				cm.Parameters.Add("@FECHASTA", SqlDbType.Date)
				If fecHasta = Date.MaxValue Then fecHasta = Date.MinValue
				cm.Parameters("@FECHASTA").Value = IIf(fecHasta = Date.MinValue, System.DBNull.Value, fecHasta)
				cm.Parameters.Add("@INCLUIRAPROBADOS", SqlDbType.Bit)
				cm.Parameters("@INCLUIRAPROBADOS").Value = incluirAprobados
				cm.Parameters.Add("@INCLUIRDENEGADOS", SqlDbType.Bit)
				cm.Parameters("@INCLUIRDENEGADOS").Value = incluirDenegados
				cm.Parameters.Add("@INCLUIRAPROBNIVELSUP", SqlDbType.Bit)
				cm.Parameters("@INCLUIRAPROBNIVELSUP").Value = incluirAprobNivelSup
				cm.Parameters.Add("@INCLUIRPEDIDOSCATALOGO", SqlDbType.Bit)
				cm.Parameters("@INCLUIRPEDIDOSCATALOGO").Value = bIncluirPedidosCatalogo
				cm.Parameters.Add("@INCLUIRPEDIDOSCONTRAABIERTO", SqlDbType.Bit)
				cm.Parameters("@INCLUIRPEDIDOSCONTRAABIERTO").Value = bIncluirPedidosContraAbiertos
				If iAnyoPedAbierto <> 0 Then
					cm.Parameters.Add("@ANYO_PED_ABIERTO", SqlDbType.Int)
					cm.Parameters("@ANYO_PED_ABIERTO").Value = iAnyoPedAbierto
				End If
				If iCestaPedAbierto <> 0 Then
					cm.Parameters.Add("@CESTA_PED_ABIERTO", SqlDbType.Int)
					cm.Parameters("@CESTA_PED_ABIERTO").Value = iCestaPedAbierto
				End If
				If iPedidoPedAbierto <> 0 Then
					cm.Parameters.Add("@PEDIDO_PED_ABIERTO", SqlDbType.Int)
					cm.Parameters("@PEDIDO_PED_ABIERTO").Value = iPedidoPedAbierto
				End If
				cn.Open()
				da.SelectCommand = cm
				da.Fill(ds)
				Return ds
			Catch e As Exception
				Throw e
			Finally
				cn.Dispose()
				cm.Dispose()
				da.Dispose()
			End Try
		End Using
	End Function
	''' <summary>
	''' Devuelve todos los artículos de la cesta de la compra del usuario, en su idioma
	''' </summary>
	''' <param name="sPer">Código de la persona del usuario que este utilizando la aplicación</param>
	''' <param name="Idioma">Idioma actual de la aplicación</param>
	''' <returns>Un DataSet ordenado por el parámetro de entrada con todos los artículos de la cesta del usuario</returns>
	''' <remarks>
	''' Llamada desde: La clase COrdenes método CargarCesta
	''' Tiempo máximo: Depende de los artículos de la cesta, aproximadamente 0,75 sg</remarks>
	Public Function Ordenes_CargarCesta(ByVal sPer As String, ByVal Idioma As FSNLibrary.Idioma) As DataSet
		Authenticate()
		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_CARGAR_CESTA"
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@PER", sPer)
		adoPar = adoCom.Parameters.AddWithValue("@IDIOMA", Idioma.ToString())
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>
	''' Devuelve todos los artículos de la cesta de la compra del usuario, en su idioma
	''' </summary>
	''' <param name="sPer">Código de la persona del usuario que este utilizando la aplicación</param>
	''' <param name="Idioma">Idioma actual de la aplicación</param>
	''' <returns>Un DataSet ordenado por el parámetro de entrada con todos los artículos de la cesta del usuario</returns>
	''' <remarks>
	''' Llamada desde: La clase COrdenes método CargarCesta
	''' Tiempo máximo: Depende de los artículos de la cesta, aproximadamente 0,75 sg</remarks>
	Public Function Ordenes_CargarLineasCesta(ByVal sPer As String, ByVal Idioma As FSNLibrary.Idioma, ByVal idCabecera As Integer, ByVal iDesdeFavorito As Boolean) As DataSet
		Authenticate()
		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		If iDesdeFavorito Then
			adoCom.CommandText = "FSEP_CARGAR_LINEAS_FAV"
		Else
			adoCom.CommandText = "FSEP_CARGAR_LINEAS_CESTA"
		End If
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@PER", sPer)
		adoPar = adoCom.Parameters.AddWithValue("@IDIOMA", Idioma.ToString())
		adoPar = adoCom.Parameters.AddWithValue("@CESTACABECERAID", idCabecera.ToString())
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>
	''' Elimina un artículo de la NUEVA cesta dado su Identificador y el id de la cestacabecera
	''' </summary>
	''' <param name="Id">Identificador de la fila a eliminar</param>
	''' <remarks>
	''' Llamada desde:La clase cordenes, método EliminarDeCesta
	''' </remarks>
	Public Sub Ordenes_EliminarDeCestaEP(ByVal iDesdeFavorito As Integer, ByVal Id As Integer, ByVal iCestaCabeceraID As Integer, ByVal Persona As String)
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Using cn
			cn.Open()
			Dim transaccion As SqlTransaction = cn.BeginTransaction()
			Try
				If iDesdeFavorito = 0 Then
					cm.CommandText = "FSEP_CESTA_ELIMINARLINEACESTA"
				Else
					cm.CommandText = "FSEP_CESTA_ELIMINARLINEACESTA_FAV"
				End If
				cm.CommandType = CommandType.StoredProcedure
				cm.Transaction = transaccion
				cm.Connection = cn
				cm.Parameters.AddWithValue("@ID", Id)
				cm.Parameters.AddWithValue("@PER", Persona)
				cm.Parameters.AddWithValue("@CESTACABECERAID", iCestaCabeceraID)
				cm.ExecuteNonQuery()

				cm = New SqlCommand
				cm.CommandText = "FSEP_ELIMINAR_PLAN_ENTREGA_LINEA"
				cm.CommandType = CommandType.StoredProcedure
				cm.Transaction = transaccion
				cm.Connection = cn
				cm.Parameters.AddWithValue("@CESTA", Id)
				cm.ExecuteNonQuery()

				transaccion.Commit()
			Catch ex As Exception
				transaccion.Rollback()
				Throw ex
			Finally
				cn.Dispose()
				cm.Dispose()
			End Try
		End Using
	End Sub
	''' <summary>
	''' Función que vacía completamente la cesta de la compra de un usuario
	''' </summary>
	''' <param name="Per">Código de la persona del usuario que esta utilizando la aplicación</param>
	''' <returns>Un número entero que valdrá 0 si todo ha ido bien, y 1 en caso de error.</returns>
	''' <remarks>
	''' Llamada desde: La clase COrdenes, método VaciarCesta
	''' </remarks>
	Public Function Ordenes_VaciarCesta(ByVal per As String) As Integer
		Authenticate()
		Dim sqlCmd As New SqlCommand
		Dim cn As New SqlConnection(mDBConnection)
		Dim sqlParam As New SqlParameter
		Dim SqlTrans As SqlTransaction
		sqlParam = sqlCmd.Parameters.AddWithValue("@PER", per)
		cn.Open()
		sqlCmd.Connection = cn
		sqlCmd.CommandText = "FSEP_VACIAR_CESTA"
		sqlCmd.CommandType = CommandType.StoredProcedure
		SqlTrans = cn.BeginTransaction
		sqlCmd.Transaction = SqlTrans
		Try
			sqlCmd.ExecuteNonQuery()
		Catch ex As Exception
			sqlCmd.Transaction.Rollback()
			cn.Close()
			Return 1
		End Try
		sqlCmd.Transaction.Commit()
		cn.Close()
		Return 0
	End Function
	Public Function Ordenes_Obtener_DetalleAlbaran(ByVal Albaran As String, ByVal FechaAlbaran As DateTime,
		 ByVal Prove As String, ByVal Idioma As String) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim dr As New DataSet
		Dim da As New SqlDataAdapter

		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "EP_OBT_DETALLEALBARAN"
			cm.Parameters.AddWithValue("@ALBARAN", Albaran)
			cm.Parameters.AddWithValue("@FECHAALBARAN", FechaAlbaran)
			cm.Parameters.AddWithValue("@PROVE", Prove)
			cm.Parameters.AddWithValue("@IDIOMA", Idioma)
			cm.CommandType = CommandType.StoredProcedure
			da.SelectCommand = cm
			da.Fill(dr)
			Return dr
		Catch e As Exception
			Throw e
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Function
	''' <summary>
	''' Lineas a recepcionar
	''' </summary>
	''' <param name="IdLineasPedido">líneas de las que recuperar los datos</param>
	''' <param name="Idioma">Idioma</param>
	''' <param name="SMActivado">Sm Activado</param>
	''' <returns>Dataset con las líneas</returns>
	''' <remarks>Llamadas desde COrdenes. 1 seg.</remarks>    
	Public Function Ordenes_Obtener_Lineas_Recepcion(ByVal IdLineasPedido As String,
		 ByVal Idioma As String, ByVal SMActivado As Boolean) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim dr As New DataSet
		Dim da As New SqlDataAdapter
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "EP_DEVOLVER_LINEAS_RECEPCION"
			cm.Parameters.AddWithValue("@IDLINEASPEDIDO", IdLineasPedido)
			cm.Parameters.AddWithValue("@IDI", Idioma)
			cm.Parameters.AddWithValue("@SMACTIVADO", SMActivado)
			cm.CommandType = CommandType.StoredProcedure
			da.SelectCommand = cm
			da.Fill(dr)
			dr.Tables(0).TableName = "DATOS_RECEPCIONES"
			dr.Tables(1).TableName = "DATOS_ATRIBUTOS_RECEPCION"
			Return dr
		Catch e As Exception
			Throw e
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Function
	''' <summary>
	''' Lineas pendientes de recepcionar
	''' </summary>
	''' <param name="Ordenes">Órdenes de las que se quieren recuperar las líneas</param>
	''' <param name="Idioma">Idioma</param>
	''' <param name="SMActivado">Sm Activado</param>
	''' <returns>Dataset con las líneas</returns>
	''' <remarks>Llamadas desde COrdenes. 1 seg.</remarks>
	Public Function Ordenes_Obtener_Lineas_PendientesRecepcionar(ByVal Ordenes As String,
		 ByVal Idioma As String, ByVal SMActivado As Boolean) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim dr As New DataSet
		Dim da As New SqlDataAdapter

		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "EP_DEVOLVER_LINEAS_PENDIENTESRECEPCIONAR"
			cm.Parameters.AddWithValue("@ORDENES", Ordenes)
			cm.Parameters.AddWithValue("@IDI", Idioma)
			cm.Parameters.AddWithValue("@SMACTIVADO", SMActivado)
			cm.CommandType = CommandType.StoredProcedure
			da.SelectCommand = cm
			da.Fill(dr)
			dr.Tables(0).TableName = "DATOS_RECEPCIONES"
			dr.Tables(1).TableName = "DATOS_ATRIBUTOS_RECEPCION"
			Return dr
		Catch e As Exception
			Throw e
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Function
	''' Revisado por: blp. Fecha: 23/08/2012
	''' <summary>
	''' Obtiene la configuracion de los campos a mostrar y su ancho en el Visor de Entregas (EntregasPedidos.aspx) del portal para el usuario y la compania (proveedor) dados
	''' </summary>
	''' <param name="CodUsu">Codigo del usuario</param>
	''' <remarks>
	''' Llamadas desde CRecepciones.vb-->Obt_Conf_Visor_Entregas(). Max 1 seg
	''' </remarks>
	Public Function Ordenes_Recepciones_Obt_Conf_Visor_Recepciones(ByVal CodUsu As String) As DataTable
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim dt As New DataTable
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "EP_OBT_CONF_VISOR_RECEPCIONES"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@USU", CodUsu)
			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dt)

			Dim key() As DataColumn = {dt.Columns("ID"), dt.Columns("TIPOCAMPO")}
			dt.PrimaryKey = key

			Return dt
		Catch ex As SqlException
			Throw ex
		Finally
			cn.Close()
			cn.Dispose()
			cm.Dispose()
			dt.Dispose()
		End Try
	End Function
	''' Revisado por: blp. Fecha: 28/08/2012
	''' <summary>
	''' Guarda la configuracion de los campos a mostrar y su ancho en el Visor de Entregas (EntregasPedidos.aspx) del portal para el usuario y la compania (proveedor) dados
	''' </summary>
	''' <param name="CodUsu">Codigo del usuario</param>
	''' <param name="DatosVisibilidadAnchoPosicion">Tabla con los datos de visibilidad, ancho y posicion de cada campo</param>
	''' <remarks>
	''' Llamadas desde CRecepciones.vb-->Mod_Conf_Visor_Entregas(). Max 1 seg
	''' </remarks>
	Public Sub Ordenes_Recepciones_Mod_Conf_Visor_Recepciones(ByVal CodUsu As String, ByVal DatosVisibilidadAnchoPosicion As DataTable)
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "EP_MOD_CONF_VISOR_RECEPCIONES"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@USU", CodUsu)
			'Creamos un parametro sql de tipo TB_EP_CONF_VISOR_RECEPCIONES, que es un tipo "tabla" definido por el usuario en sql server
			Dim ParamDatosCampos As New SqlParameter("@DATOSVISIBILIDADANCHOPOSICION", SqlDbType.Structured)
			ParamDatosCampos.TypeName = "TB_EP_CONF_VISOR_RECEPCIONES"
			ParamDatosCampos.Value = DatosVisibilidadAnchoPosicion
			cm.Parameters.Add(ParamDatosCampos)

			cm.ExecuteNonQuery()
		Catch ex As SqlException
			Throw ex
		Finally
			cn.Close()
			cn.Dispose()
			cm.Dispose()
		End Try
	End Sub
	''' Revisado por: blp. Fecha: 28/08/2012
	''' <summary>
	''' Devolvera una tabla con los datos de los proveedores cuyos codigos se han recibido como parametro
	''' </summary>
	''' <param name="Idioma">Idioma en el que se tienen que devolver los datos</param>
	''' <param name="ProveCods">Tabla con los codigos de los proveedores de los que se quiere recuperar la informacion</param>
	''' <remarks>
	''' Llamadas desde CRecepciones.vb-->Mod_Conf_Visor_Entregas(). Max 1 seg
	''' </remarks>
	Public Function Ordenes_Recepciones_Obt_DatosProveedores(ByVal Idioma As FSNLibrary.Idioma, ByVal ProveCods As DataTable) As DataTable
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim dt As New DataTable
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "EP_OBT_DATOS_PROVEEDORES"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@IDIOMA", Idioma.ToString)
			'Creamos un parametro sql de tipo TablaPartidas, que es un tipo "tabla" definido por el usuario en sql server
			Dim ParamProveCods As New SqlParameter("@PROVECODS", SqlDbType.Structured)
			ParamProveCods.TypeName = "TBPROVECOD"
			ParamProveCods.Value = ProveCods
			cm.Parameters.Add(ParamProveCods)

			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dt)

			Return dt
		Catch ex As SqlException
			Throw ex
		Finally
			cn.Close()
			cn.Dispose()
			cm.Dispose()
			dt.Dispose()
		End Try
	End Function
	''' Revisado por: blp. Fecha: 30/11/2012
	''' <summary>
	''' Devuelve los datos para mostrar en los combos del buscador de seguimiento de pedidos en EP
	''' </summary>
	''' <param name="Idioma">Idioma en el que devolver los datos</param>
	''' <param name="Per">Usuario de cuyos pedidos queremos devolver la información</param>
	''' <param name="VerPedidosOtrosUsuarios">Permiso para ver pedidos de otros usuarios</param>
	''' <returns>dataset con varias tablas que contienen la información a mostrar en los combos</returns>
	''' <remarks>Llamada desde COrdenes.vb. Máximo 5 seg.</remarks>
	Public Function Ordenes_ObtenerDatosCombosSeguimiento(ByVal Idioma As FSNLibrary.Idioma, ByVal Per As String, ByVal VerPedidosOtrosUsuarios As Boolean) As DataSet
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim ds As New DataSet
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "EP_OBT_DDLINFO_SEGUIMIENTO"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@IDIOMA", Idioma.ToString)
			cm.Parameters.AddWithValue("@PER", Per)
			cm.Parameters.AddWithValue("@PERMISO_VER_PEDIDOS_OTROS_USUARIOS", VerPedidosOtrosUsuarios)

			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(ds)

			Return ds
		Catch ex As SqlException
			Throw ex
		Finally
			cn.Close()
			cn.Dispose()
			cm.Dispose()
			ds.Dispose()
		End Try
	End Function
	''' Revisado por: blp. Fecha: 24/05/2013
	''' <summary>
	''' Recuperamos el año de la orden más antigua emitida.
	''' </summary>
	''' <returns>nullable integer con el año</returns>
	''' <remarks>Llamada desde COrdenes.vb. Máximo 0,1 seg.</remarks>
	Public Function Ordenes_ObtenerMinAnyo() As Nullable(Of Integer)
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim rdo As Nullable(Of Integer)
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "EP_OBT_ORDENES_MIN_FECHA"
			cm.CommandType = CommandType.StoredProcedure
			rdo = DBNullToSomething(cm.ExecuteScalar)
			Return rdo
		Catch ex As SqlException
			Throw ex
		Finally
			cn.Close()
			cn.Dispose()
			cm.Dispose()
		End Try
	End Function
#End Region
#Region " Orden Favoritos data access methods "
	''' <summary>
	''' Creamos una cesta desde un pedido favorito
	''' </summary>
	''' <param name="idFavorito">Identificador de la orden de favoritos para crear la cesta</param>
	''' <returns>Un dataset</returns>
	''' <remarks></remarks>
	Public Function CrearCestaDesdeFavorito(ByVal idFavorito As Integer) As DataSet
		Authenticate()
		Dim ador As New DataSet
		Dim adoCom As New SqlCommand
		Dim oParam As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)

		'Ejecuta el store procedure FSEP_DEVOLVER_LINEAS_DE_ORDEN_FAVORITOS
		adoCom = New SqlCommand
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_GENERAR_CESTA_FAVORITO"
		adoCom.CommandType = CommandType.StoredProcedure

		oParam = adoCom.Parameters.AddWithValue("@ORDENID", idFavorito)

		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(ador)
		If ador.Tables.Count = 0 Then
			ador.Clear()
			ador = Nothing
			cn.Close()
			adoCom = Nothing
			da.Dispose()
			ador.Dispose()
			adoCom.Dispose()
			Return ador
			Exit Function
		Else
			'Devuelve un recordset desconectado
			'Ya no existe-->ador.let_Connection(Nothing)
			adoCom = Nothing
			da.Dispose()
			cn.Close()
			oParam = Nothing
			Return ador
		End If
	End Function
	''' <summary>
	''' Recupera las lineas de pedido para la orden de favoritos pasada como parámetro
	''' </summary>
	''' <param name="sIdioma">Idioma utilizado para la denominación de la moneda.</param>
	''' <param name="mlId">Identificador de la orden de favoritos para cargar las lineas con los artículos</param>
	''' <returns>Un dataset con los artículos que componen la línea de pedido pasada como parámetro</returns>
	''' <remarks></remarks>
	Public Function OrdenFavoritos_DevolverLineasEP(ByVal sIdioma As Object, ByVal mlId As Integer) As DataSet
		Authenticate()
		Dim ador As New DataSet
		Dim adoCom As New SqlCommand
		Dim oParam As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)

		'Ejecuta el store procedure FSEP_DEVOLVER_LINEAS_DE_ORDEN_FAVORITOS
		adoCom = New SqlCommand
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_DEVOLVER_LINEAS_DE_ORDEN_FAVORITOS_EP"
		adoCom.CommandType = CommandType.StoredProcedure

		oParam = adoCom.Parameters.AddWithValue("@ORDENID", mlId)

		'Antes IsMissing
		If Not IsNothing(sIdioma) Then
			If sIdioma <> "" Then
				oParam = adoCom.Parameters.AddWithValue("@IDI", sIdioma)
			Else
				oParam = adoCom.Parameters.AddWithValue("@IDI", "SPA")
			End If
		Else
			oParam = adoCom.Parameters.AddWithValue("@IDI", "SPA")
		End If

		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(ador)
		If ador.Tables.Count = 0 Then
			ador.Clear()
			ador = Nothing
			cn.Close()
			adoCom = Nothing
			da.Dispose()
			ador.Dispose()
			adoCom.Dispose()
			Return ador
			Exit Function
		Else
			'Devuelve un recordset desconectado
			'Ya no existe-->ador.let_Connection(Nothing)
			adoCom = Nothing
			da.Dispose()
			cn.Close()
			oParam = Nothing
			Return ador
		End If
	End Function
	''' <summary>
	''' Añade la linea de pedido a un pedido favorito existente
	''' </summary>
	''' <param name="ArtId">Identificador del artículo a añadir</param>
	''' <param name="Campo1Id">Identificador del campo1 libre del artículo</param>
	''' <param name="Campo1Val">Valor del campo1 libre del artículo</param>
	''' <param name="Campo2Id">Identificador del campo2 libre del artículo</param>
	''' <param name="Campo2Val">Valor del campo2 libre del artículo</param>
	''' <param name="DESC_LIBRE">Valor de la descripción libre del artículo</param>
	''' <param name="Destino">Destino al que enviar el artículo</param>
	''' <param name="FC">Valor de FC del artículo</param>
	''' <param name="IdOrden">Id del pedido favorito</param>
	''' <param name="Linea">Identificador de la línea de pedido favorito para añadir el artículo</param>
	''' <param name="NumArt">Número de artículos a añadir</param>
	''' <param name=" ObsAdjun">Observaciones de los adjuntos</param>
	''' <param name="Prec_orig">Precio del artículo</param>
	''' <param name="SumImporte">Importe de la línea de pedido</param>
	''' <param name="TipoUnidad">Tipo de la unidad del artículo</param>
	''' <param name="Usu">Usuario de la aplicación</param>
	''' <param name="Obs">Observaciones del artículo</param>
	''' <param name="FechaEntrega">Fecha de entrega solicitada para la línea</param>
	''' <param name="EntregaObl">Fecha de entrega obligatoria</param>
	''' <param name="IDAlmacen">ID del almacén de la línea</param>
	''' <param name="Activo">Activo de la línea</param>
	''' <param name="CentroSM">Centro de Coste al que se ha asignado la línea</param>
	''' <param name="PartidaPres">Partida Presupuestaria a la que se ha asignado la línea</param>
	''' <returns>Un entero que devuelve 1 si ha habido error y 0 si todo ha ido bien</returns>
	''' <remarks></remarks>
	Public Function OrdenFavoritos_AnyadirLinea(ByVal Linea As Integer, ByVal Prec_orig As Double, ByVal IdOrden As Integer, ByVal Destino As String, ByVal Usu As String, ByVal TipoUnidad As String, ByVal SumImporte As Double, ByVal NumArt As Double, ByVal ArtId As Integer, ByVal Obs As String, ByVal FC As String, ByVal DESC_LIBRE As String, ByVal Campo1Id As Integer, ByVal Campo1Val As String, ByVal Campo2Id As Integer, ByVal Campo2Val As String, ByVal ObsAdjun As String, ByVal FechaEntrega As Object, ByVal EntregaObl As Object, ByVal IDAlmacen As Integer, ByVal Activo As Integer, ByVal CentroSM As String, ByVal PartidaPres As String) As Integer()
		Authenticate()
		Dim Resul As Integer()
		Dim adoCom As New SqlCommand
		Dim cn As New SqlConnection(mDBConnection)
		Dim SqlTrans As SqlTransaction
		cn.Open()
		Dim sConsulta As String
		adoCom = New SqlCommand
		adoCom.Connection = cn
		sConsulta = "FSEP_ACTUALIZAR_LINEA_CATALOGO"
		adoCom.CommandType = CommandType.StoredProcedure
		adoCom.Parameters.AddWithValue("@LINEA", Linea)
		adoCom.Parameters.AddWithValue("@PREC_ORIG", Prec_orig)
		adoCom.Parameters.AddWithValue("@IDORDEN", IdOrden)
		adoCom.Parameters.AddWithValue("@DESTINO", Destino)
		adoCom.Parameters.AddWithValue("@USU", Usu)
		adoCom.Parameters.AddWithValue("@TIPOUNIDAD", TipoUnidad)
		adoCom.Parameters.AddWithValue("@SUMIMPORTE", SumImporte)
		adoCom.Parameters.AddWithValue("@NUMART", NumArt)
		adoCom.Parameters.AddWithValue("@ARTID", ArtId)
		If Obs = Nothing Then
			Obs = ""
		End If
		adoCom.Parameters.AddWithValue("@OBS", Obs)
		adoCom.Parameters.AddWithValue("@FC", FC)
		If DESC_LIBRE = Nothing Then
			DESC_LIBRE = ""
		End If
		adoCom.Parameters.AddWithValue("@DESC_LIBRE", DESC_LIBRE)
		adoCom.Parameters.AddWithValue("@CAMPO1ID", Campo1Id)
		If Campo1Val Is Nothing OrElse Campo1Val = "" Then
			adoCom.Parameters.AddWithValue("@CAMPO1VAL", System.DBNull.Value)
		Else
			adoCom.Parameters.AddWithValue("@CAMPO1VAL", Campo1Val)
		End If
		adoCom.Parameters.AddWithValue("@CAMPO2ID", Campo2Id)
		If Campo2Val Is Nothing OrElse Campo2Val = "" Then
			adoCom.Parameters.AddWithValue("@CAMPO2VAL", System.DBNull.Value)
		Else
			adoCom.Parameters.AddWithValue("@CAMPO2VAL", Campo2Val)
		End If
		adoCom.Parameters.Add("@FECHAENTREGA", SqlDbType.DateTime).Value = IIf(FechaEntrega = Date.MinValue, DBNull.Value, FechaEntrega)
		adoCom.Parameters.Add("@ENTREGAOBL", SqlDbType.Bit).Value = IIf(EntregaObl, 1, 0)
		adoCom.Parameters.Add("@ALMACEN", SqlDbType.Int).Value = IIf(IDAlmacen = 0, DBNull.Value, IDAlmacen)
		adoCom.Parameters.Add("@ACTIVO", SqlDbType.Int).Value = IIf(Activo = 0, DBNull.Value, Activo)
		If ObsAdjun = Nothing Then ObsAdjun = ""
		adoCom.Parameters.AddWithValue("@OBSADJUN", ObsAdjun)
		adoCom.Parameters.Add("@ERROR", SqlDbType.Int)
		adoCom.Parameters("@ERROR").Direction = ParameterDirection.Output
		adoCom.Parameters.Add("@IDLINEAANYADIDA", SqlDbType.Int)
		adoCom.Parameters("@IDLINEAANYADIDA").Direction = ParameterDirection.Output
		adoCom.CommandText = sConsulta
		adoCom.CommandType = CommandType.StoredProcedure
		adoCom.CommandTimeout = 120
		SqlTrans = cn.BeginTransaction
		adoCom.Transaction = SqlTrans
		Try
			adoCom.ExecuteNonQuery()
		Catch ex As Exception
			adoCom.Transaction.Rollback()
			cn.Close()
			ReDim Preserve Resul(0)
			Resul(0) = 1
			ReDim Preserve Resul(1)
			Resul(1) = 0
			Return Resul
		End Try

		ReDim Preserve Resul(1)
		Resul(0) = DBNullToDbl(adoCom.Parameters("@ERROR").Value)
		Resul(1) = DBNullToDbl(adoCom.Parameters("@IDLINEAANYADIDA").Value)

		If Not String.IsNullOrEmpty(CentroSM) Then
			adoCom.Parameters.Clear()
			adoCom.CommandText = "INSERT INTO FAVORITOS_IMPUTACION (FAVORITO, USU, ID, CENTRO_SM, PRES5_IMP) VALUES (@FAVORITOID, @USU, 1, @CENTROSM, @PARTIDA)"
			adoCom.Parameters.Add("@FAVORITOID", SqlDbType.Int).Value = Resul(1)
			adoCom.Parameters.Add("@USU", SqlDbType.VarChar, 20).Value = Usu
			adoCom.Parameters.Add("@CENTROSM", SqlDbType.VarChar, 50).Value = CentroSM
			adoCom.Parameters.Add("@PARTIDA", SqlDbType.Int).Value = IIf(PartidaPres = 0, DBNull.Value, PartidaPres)
			adoCom.CommandType = CommandType.Text
			Try
				adoCom.ExecuteNonQuery()
			Catch ex As Exception
				adoCom.Transaction.Rollback()
				cn.Close()
				ReDim Preserve Resul(0)
				Resul(0) = 1
				ReDim Preserve Resul(1)
				Resul(1) = 0
				Return Resul
			End Try
		Else
			adoCom.Parameters.Clear()
			' No se utiliza WITH (NOLOCK) puesto que se consulta la línea que se va a actualizar
			adoCom.CommandText = "IF EXISTS(SELECT ID FROM FAVORITOS_IMPUTACION WITH(NOLOCK) WHERE FAVORITO=@FAVORITOID AND USU = @USU AND ID=1) " &
			 "DELETE FROM FAVORITOS_IMPUTACION WHERE FAVORITO=@FAVORITOID AND USU= @USU AND ID=1 "
			adoCom.Parameters.Add("@FAVORITOID", SqlDbType.Int).Value = Resul(1)
			adoCom.Parameters.Add("@USU", SqlDbType.VarChar, 20).Value = Usu
			adoCom.CommandType = CommandType.Text
			Try
				adoCom.ExecuteNonQuery()
			Catch ex As Exception
				adoCom.Transaction.Rollback()
				cn.Close()
				ReDim Preserve Resul(0)
				Resul(0) = 1
				ReDim Preserve Resul(1)
				Resul(1) = 0
				Return Resul
			End Try
		End If

		adoCom.Transaction.Commit()
		cn.Close()
		Return Resul
	End Function
	''' <summary>
	''' Añade un adjunto a la línea de pedido
	''' </summary>
	''' <param name="LineaId">Identificador de la linea del pedido</param>
	''' <param name="Usu">Usuario de la aplicación</param>
	''' <param name="Comentario">Comentario del archivo adjunto</param>
	''' <param name="NombreAdjun">Nombre del archivo adjunto</param>
	''' <param name="AdjuntoId">Identificador del adjunto en la BBDD</param>
	''' <returns>El número de registros actualizados (1 si correcto, 0 si no)</returns>
	Public Function OrdenFavoritos_AnyadirAdjuntoLinea(ByVal LineaId As Integer, ByVal Usu As String, ByVal Comentario As String, ByVal NombreAdjun As String, ByVal AdjuntoId As String) As Integer
		Authenticate()
		Dim sqlCom As New SqlCommand
		Dim SqlCn As New SqlConnection(mDBConnection)
		Using SqlCn
			If SqlCn.State <> ConnectionState.Open Then SqlCn.Open()
			sqlCom.Connection = SqlCn
			sqlCom.CommandType = CommandType.Text
			sqlCom.CommandText = "UPDATE FAVORITOS_LINEAS_PEDIDO_ADJUN" &
			 " SET  USU=@USUARIO, LINEA=@LINEA, NOM=@NOMADJUN, COM=@COMENADJUN, DATASIZE=DATALENGTH(DATA)" &
			 " WHERE ID = @ADJUNID"
			sqlCom.Parameters.AddWithValue("@LINEA", LineaId)
			sqlCom.Parameters.AddWithValue("@USUARIO", Usu)
			sqlCom.Parameters.AddWithValue("@COMENADJUN", Comentario)
			sqlCom.Parameters.AddWithValue("@NOMADJUN", NombreAdjun)
			sqlCom.Parameters.AddWithValue("@ADJUNID", AdjuntoId)
			Return sqlCom.ExecuteNonQuery()
		End Using
	End Function
	''' <summary>
	''' Añade un archivo de especificaciones com adjunto de la línea del pedido favorito
	''' </summary>
	''' <param name="LineaFavorito">Id de la línea del pedido</param>
	''' <param name="Usuario">Código de usuario</param>
	''' <param name="LineaCatalogo">Id de la línea del catálogo</param>
	''' <param name="Especificacion">Id del archivo de especificaciones</param>
	''' <returns>Un número de registros insertados (1 si correcto, 0 si no)</returns>
	Public Function OrdenFavoritos_AnyadirAdjuntoEspecificacionesALinea(ByVal LineaFavorito As Integer, ByVal Usuario As String, ByVal LineaCatalogo As Integer, ByVal Especificacion As Integer) As Integer
		Dim sqlCom As New SqlCommand
		Dim sqlCn As New SqlConnection(mDBConnection)
		Using sqlCn
			If sqlCn.State <> ConnectionState.Open Then sqlCn.Open()
			sqlCom.Connection = sqlCn
			sqlCom.CommandType = CommandType.Text
			sqlCom.CommandText = "INSERT INTO FAVORITOS_LINEAS_PEDIDO_ADJUN (LINEA, USU, NOM, COM, DATA, DATASIZE, FECACT)" &
			 " SELECT @IDLINFAV, @USUARIO, CLE.NOM, CLE.COM, CLE.DATA, DATALENGTH(CLE.DATA), GETDATE()" &
			 " FROM CATALOG_LIN_ESP CLE WITH (NOLOCK)" &
			 " WHERE CLE.LINEA=@LINCAT AND CLE.ID=@IDESP"
			sqlCom.Parameters.AddWithValue("@IDLINFAV", LineaFavorito)
			sqlCom.Parameters.AddWithValue("@USUARIO", Usuario)
			sqlCom.Parameters.AddWithValue("@IDESP", Especificacion)
			sqlCom.Parameters.AddWithValue("@LINCAT", LineaCatalogo)
			Return sqlCom.ExecuteNonQuery()
		End Using
	End Function
	''' <summary>
	''' Función que carga los adjuntos de un pedido favorito
	''' </summary>
	''' <param name="mlId">Identificador del pedido favorito</param>
	''' <param name="msUsu">Usuario de la aplicación</param>
	''' <returns>Un dataset con un archivo adjunto por línea del mismo</returns>
	''' <remarks>
	''' Llamada desde: Clase COrdenFavoritos, método CargarAdjuntos
	''' Tiempo máximo: 0,5 seg.
	''' </remarks>
	Public Function OrdenFavoritos_CargarAdjuntos(ByVal mlId As Integer, ByVal msUsu As String) As DataSet
		Authenticate()
		Dim ador As New DataSet
		Dim adoCom As New SqlCommand
		Dim oParam As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)

		'Ejecuta el store procedure FSEP_DEVOLVER_ADJUNTOS_ORDEN_FAVORITOS
		adoCom.Connection = cn
		adoCom.CommandText = "FSEP_DEVOLVER_ADJUNTOS_ORDEN_FAVORITOS" ' @ORDEN = ?,@USU=?"
		adoCom.CommandType = CommandType.StoredProcedure
		oParam = adoCom.Parameters.AddWithValue("@ORDEN", mlId)
		oParam = adoCom.Parameters.AddWithValue("@USU", msUsu)
		cn.Open()
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(ador)
		adoCom = Nothing
		da.Dispose()
		cn.Close()
		Return ador
		ador.Clear()
	End Function
	''' <summary>
	''' Función que elimina una orden de pedido favorito,sus adjuntos, así como todas sus líneas y los adjuntos de estas.
	''' </summary>
	''' <param name="IdOrden">Identificador de la orden que se quiere eliminar</param>
	''' <param name="Usuario">Codigo de usuario que está utilizando la aplicación</param>
	''' <returns>Un número entero que indica si todo ha salido bien(0) o si ha habido algún error.</returns>
	''' <remarks>
	''' Llamada desde: La clase cOrdenFavoritos, método EliminarFavorito
	''' Tiempo máximo: 0,47 seg aproximadamente</remarks>
	Public Function OrdenFavoritos_EliminarFavorito(ByVal IdOrden As Integer, ByVal Usuario As String) As Integer
		Authenticate()
		Dim sConsulta As String
		Dim sqlcmd As New SqlCommand
		Dim SqlParam As SqlParameter
		Dim TesError As Integer = 0
		Dim cn As New SqlConnection(mDBConnection)
		Dim sqlTrans As SqlTransaction

		cn.Open()

		'ELIMINAR IMPUTACIONES DE LAS LINEAS DEL PEDIDO FAVORITO
		sqlcmd.Connection = cn
		sqlTrans = cn.BeginTransaction
		sqlcmd.CommandText = "SET XACT_ABORT OFF"
		sqlcmd.CommandType = CommandType.Text
		sqlcmd.Transaction = sqlTrans
		sqlcmd.ExecuteNonQuery()
		SqlParam = New SqlParameter
		sqlcmd = New SqlCommand
		sqlcmd.Connection = cn
		sqlcmd.Transaction = sqlTrans
		SqlParam = sqlcmd.Parameters.AddWithValue("@USUARIO", Usuario)
		SqlParam = sqlcmd.Parameters.AddWithValue("@IDORDEN", IdOrden)
		sConsulta = "DELETE FROM FAVORITOS_IMPUTACION WHERE USU=@USUARIO "
		sConsulta = sConsulta & " AND FAVORITO IN (SELECT ID FROM FAVORITOS_LINEAS_PEDIDO WITH(NOLOCK) WHERE USU=@USUARIO "
		sConsulta = sConsulta & " AND ORDEN=@IDORDEN)"
		sqlcmd.CommandText = sConsulta
		sqlcmd.CommandType = CommandType.Text
		Try
			sqlcmd.ExecuteNonQuery()
		Catch ex As Exception
			TesError = 1
			sqlTrans.Rollback()
			cn.Close()
			GoTo fin
		End Try

		'ELIMINAR ADJUNTOS DE LAS LINEAS DEL PEDIDO FAVORITO
		sqlcmd = New SqlCommand
		SqlParam = New SqlParameter
		sqlcmd.Connection = cn
		sqlcmd.Transaction = sqlTrans
		SqlParam = sqlcmd.Parameters.AddWithValue("@USUARIO", Usuario)
		SqlParam = sqlcmd.Parameters.AddWithValue("@IDORDEN", IdOrden)
		sConsulta = "DELETE FROM FAVORITOS_LINEAS_PEDIDO_ADJUN WHERE USU=@USUARIO "
		sConsulta = sConsulta & " AND LINEA IN (SELECT ID FROM FAVORITOS_LINEAS_PEDIDO WITH(NOLOCK) WHERE USU=@USUARIO "
		sConsulta = sConsulta & " AND ORDEN=@IDORDEN)"
		sqlcmd.CommandText = sConsulta
		sqlcmd.CommandType = CommandType.Text
		Try
			sqlcmd.ExecuteNonQuery()
		Catch ex As Exception
			TesError = 1
			sqlTrans.Rollback()
			cn.Close()
			GoTo fin
		End Try

		'ELIMINAR ATRIBUTOS DE LAS LINEAS DEL PEDIDO FAVORITO
		sqlcmd = New SqlCommand
		SqlParam = New SqlParameter
		sqlcmd.Connection = cn
		sqlcmd.Transaction = sqlTrans
		SqlParam = sqlcmd.Parameters.AddWithValue("@USUARIO", Usuario)
		SqlParam = sqlcmd.Parameters.AddWithValue("@IDORDEN", IdOrden)
		sConsulta = "DELETE FROM FAVORITOS_LINEAS_PEDIDO_ATRIB WHERE USU=@USUARIO "
		sConsulta = sConsulta & " AND FAVORITOLINEAPEDIDO_ID IN (SELECT ID FROM FAVORITOS_LINEAS_PEDIDO WITH(NOLOCK) WHERE USU=@USUARIO "
		sConsulta = sConsulta & " AND ORDEN=@IDORDEN)"
		sqlcmd.CommandText = sConsulta
		sqlcmd.CommandType = CommandType.Text
		Try
			sqlcmd.ExecuteNonQuery()
		Catch ex As Exception
			TesError = 1
			sqlTrans.Rollback()
			cn.Close()
			GoTo fin
		End Try

		'ELIMINAR LAS LINEAS DEL PEDIDO FAVORITO
		sqlcmd = New SqlCommand
		SqlParam = New SqlParameter
		sqlcmd.Connection = cn
		sqlcmd.Transaction = sqlTrans
		SqlParam = sqlcmd.Parameters.AddWithValue("@USUARIO", Usuario)
		SqlParam = sqlcmd.Parameters.AddWithValue("@IDORDEN", IdOrden)
		sConsulta = "DELETE FROM FAVORITOS_LINEAS_PEDIDO WHERE USU=@USUARIO AND ORDEN=@IDORDEN"
		sqlcmd.CommandText = sConsulta
		sqlcmd.CommandType = CommandType.Text
		Try
			sqlcmd.ExecuteNonQuery()
		Catch ex As Exception
			TesError = 1
			sqlTrans.Rollback()
			cn.Close()
			GoTo fin
		End Try

		'ELIMINA LOS ADJUNTOS DEL PEDIDO FAVORITO
		sqlcmd = New SqlCommand
		SqlParam = New SqlParameter
		sqlcmd.Connection = cn
		sqlcmd.Transaction = sqlTrans
		SqlParam = sqlcmd.Parameters.AddWithValue("@USUARIO", Usuario)
		SqlParam = sqlcmd.Parameters.AddWithValue("@IDORDEN", IdOrden)
		sConsulta = "DELETE FROM FAVORITOS_ORDEN_ENTREGA_ADJUN WHERE USU=@USUARIO AND ORDEN=@IDORDEN"
		sqlcmd.CommandText = sConsulta
		sqlcmd.CommandType = CommandType.Text
		Try
			sqlcmd.ExecuteNonQuery()
		Catch ex As Exception
			TesError = 1
			sqlTrans.Rollback()
			cn.Close()
			GoTo fin
		End Try

		'ELIMINA LOS ARTIBUTOS DEL PEDIDO FAVORITO
		sqlcmd = New SqlCommand
		SqlParam = New SqlParameter
		sqlcmd.Connection = cn
		sqlcmd.Transaction = sqlTrans
		SqlParam = sqlcmd.Parameters.AddWithValue("@USUARIO", Usuario)
		SqlParam = sqlcmd.Parameters.AddWithValue("@IDORDEN", IdOrden)
		sConsulta = "DELETE FROM FAVORITOS_ORDEN_ENTREGA_ATRIB WHERE USU=@USUARIO AND FAVORITOORDENENTREGA_ID=@IDORDEN"
		sqlcmd.CommandText = sConsulta
		sqlcmd.CommandType = CommandType.Text
		Try
			sqlcmd.ExecuteNonQuery()
		Catch ex As Exception
			TesError = 1
			sqlTrans.Rollback()
			cn.Close()
			GoTo fin
		End Try

		'ELIMINA EL PEDIDO FAVORITO
		sqlcmd = New SqlCommand
		SqlParam = New SqlParameter
		sqlcmd.Connection = cn
		sqlcmd.Transaction = sqlTrans
		SqlParam = sqlcmd.Parameters.AddWithValue("@USUARIO", Usuario)
		SqlParam = sqlcmd.Parameters.AddWithValue("@IDORDEN", IdOrden)
		sConsulta = "DELETE FROM FAVORITOS_ORDEN_ENTREGA WHERE USU=@USUARIO AND ID=@IDORDEN"
		sqlcmd.CommandText = sConsulta
		sqlcmd.CommandType = CommandType.Text
		Try
			sqlcmd.ExecuteNonQuery()
			sqlTrans.Commit()
			cn.Close()
			Return TesError
		Catch ex As Exception
			TesError = 1
			sqlTrans.Rollback()
			cn.Close()
			GoTo fin
		End Try
fin:
		TesError = 1
		Exit Function
	End Function
#End Region
#Region " Ordenes favoritas data access methods "
	''' Revisado por: blp. fecha:15/02/2012
	''' <summary>
	''' Carga todas las ordenes del usuario dado para el pedido y proveedor pasado como parametros
	''' </summary>
	''' <param name="codMon">Código de la moneda
	''' 
	''' Nunca debe filtrarse por moneda para recuperar el conjunto de Favoritos. 
	''' Sólo si por ejemplo, se desea mostrar los favoritos con una moneda concreta para añadir un artículo que tiene esa moneda
	''' 
	''' </param>
	''' <param name="Pedido">Pedido favorito a cargar(si existe)</param>
	''' <param name="Prove">Proveedor del artículo a añadir</param>
	''' <param name="sUsuario">Usuario que quiere realizar la accion de añadir a facoritos</param>
	''' <returns>Un dataset con una tabla con todos los pedidos favoritos de los datos pasados</returns>
	''' <remarks>
	''' Llamada desde: SelecFavoritos.aspx
	''' Tiempo máximo: 0 sec.
	''' </remarks>
	Public Function OrdenesFavoritos_BuscarTodasOrdenes(ByVal sUsuario As String, ByVal Pedido As Integer, ByVal Prove As String, ByVal codMon As String, ByVal IDIOMA As String, Optional ByVal CargarLineas As Boolean = False) As DataSet
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Dim ador As New DataSet
		Dim SqlCmd As New SqlCommand
		Dim Da As New SqlDataAdapter
		Dim SqlParam As New SqlParameter
		SqlParam = SqlCmd.Parameters.Add("@USU", SqlDbType.VarChar)
		SqlParam = SqlCmd.Parameters.Add("@MON", SqlDbType.VarChar)
		SqlParam = SqlCmd.Parameters.Add("@IDIOMA", SqlDbType.VarChar)
		SqlParam = SqlCmd.Parameters.Add("@CARGARLINEAS", SqlDbType.TinyInt)
		SqlParam = SqlCmd.Parameters.Add("@PEDIDO", SqlDbType.Int)
		SqlParam = SqlCmd.Parameters.Add("@PROVE", SqlDbType.VarChar)
		SqlCmd.Parameters("@USU").Value = sUsuario
		If CargarLineas Then
			SqlCmd.Parameters("@IDIOMA").Value = IDIOMA
		Else
			SqlCmd.Parameters("@IDIOMA").Value = "SPA"
		End If
		If codMon Is Nothing OrElse codMon = "" OrElse IsDBNull(codMon) Then
			SqlCmd.Parameters("@MON").Value = System.DBNull.Value
		Else
			SqlCmd.Parameters("@MON").Value = codMon
		End If
		If Prove Is Nothing OrElse Prove = "" OrElse IsDBNull(Prove) Then
			SqlCmd.Parameters("@PROVE").Value = System.DBNull.Value
		Else
			SqlCmd.Parameters("@PROVE").Value = Prove
		End If
		If Pedido = Nothing OrElse Pedido = 0 OrElse IsDBNull(Pedido) Then
			SqlCmd.Parameters("@PEDIDO").Value = System.DBNull.Value
		Else
			SqlCmd.Parameters("@PEDIDO").Value = Pedido
		End If
		If CargarLineas Then
			SqlCmd.Parameters("@CARGARLINEAS").Value = 1
		Else
			SqlCmd.Parameters("@CARGARLINEAS").Value = 0
		End If
		SqlCmd.CommandType = CommandType.StoredProcedure
		SqlCmd.CommandText = "FSEP_GET_ORDENES_FAVORITOS"
		SqlCmd.Connection = cn
		Try
			cn.Open()
			Da.SelectCommand = SqlCmd
			Da.Fill(ador)
			Return ador
		Catch ex As Exception
			Throw ex
		Finally
			ador.Dispose()
			Da.Dispose()
			SqlCmd.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Function
	''' <summary>
	''' Recupera los pedidos favoritos del usuario indicado.
	''' </summary>
	''' <param name="Usuario">Código del usuario</param>
	''' <param name="NumLineas">Parámetro de entrada/salida. Indica el nº de pedidos a devolver y devuelve el nº total de pedidos existente.</param>
	''' <returns>Un dataset con los pedidos que cumplen los criterios.</returns>
	''' <remarks>Llamadas desde: COrdenesFavoritos.BuscarPorUsuario</remarks>
	Public Function OrdenesFavoritas_GetOrdenesUsuario(ByVal Usuario As String, ByRef NumLineas As Integer) As DataSet
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Using cn
			Dim cm As SqlCommand = New SqlCommand("FSEP_GET_ORDENESFAV_USU", cn)
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.Add("@NUMLINEAS", SqlDbType.Int, 0)
			cm.Parameters("@NUMLINEAS").Direction = ParameterDirection.InputOutput
			cm.Parameters("@NUMLINEAS").Value = NumLineas
			cm.Parameters.Add("@USUARIO", SqlDbType.VarChar, 50)
			cm.Parameters("@USUARIO").Value = Usuario

			Dim da As New SqlDataAdapter
			Dim ds As New DataSet
			da.SelectCommand = cm
			da.Fill(ds)
			NumLineas = cm.Parameters("@NUMLINEAS").Value
			Return ds
		End Using
	End Function
#End Region
#Region " Parametros data access methods"
	''' <summary>
	''' Función que devuelve un texto concreto de la tabla PARGEN_LIT para el idioma requerido
	''' </summary>
	''' <param name="lId">Id del literal</param>
	''' <param name="sIdi">Códgio de idioma</param>
	''' <returns>Devuelve el texto concreto de la tabla PARGEN_LIT para el idioma requerido</returns>
	''' <remarks>
	''' Llamada desde: CParametros.CargarLiteralParametros
	''' Tiempo máximo: 0 sec.
	''' </remarks>
	Public Function Parametros_CargarLiteralParametros(ByVal lId As Long, ByVal sIdi As String) As String
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet

		Using cn
			cn.Open()
			cm = New SqlCommand
			cm.Connection = cn
			cm.CommandText = "SELECT DEN FROM PARGEN_LIT WITH(NOLOCK) WHERE ID = " & lId & " AND IDI = '" & DblQuote(sIdi) & "'"
			cm.CommandType = CommandType.Text

			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)

			Return dr.Tables(0).Rows(0).Item(0).ToString
		End Using
	End Function
	''' <summary>
	''' Devuelve los valores de conexión al portal, así como el idioma y moneda por defecto
	''' </summary>
	''' <returns>Un DataRow con los valores indicados</returns>
	Public Function Parametros_Portal() As DataRow
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Using cn
			cm = New SqlCommand("FSWS_CONEXION_PORTAL", cn)
			cm.CommandType = CommandType.StoredProcedure
			Dim ds As New DataSet
			Dim da As New SqlDataAdapter(cm)
			cn.Open()
			da.Fill(ds)
			Return ds.Tables(0).Rows(0)
		End Using
	End Function
#End Region
#Region " Persona data access methods"
	''' <summary>
	''' Función que nos traerá un dataset con las empresas que hemos recuperado para el usuario.
	''' </summary>
	''' <param name="sPer">Código de la persona</param>
	''' <param name="iSMActivado">Parametro que nos indicará si el SM está activado o no, lo cual modifica las empresas disponibles en función de los permisos de imputación a centros de coste del usuario</param>
	''' <param name="Empresa">Parametro byref en el que recuperaremos el ID de la empresa de la persona</param>
	''' <returns>Devuelve el dataset mencionado</returns>
	''' <remarks>
	''' Llamada desde la carga inicial de Seguimiento.aspx
	''' Tiempo máximo: 0 seg.</remarks>
	Public Function Persona_CargarEmpresas(ByVal sPer As String, ByVal iSMActivado As EnumTipoImputacion,
			ByRef Empresa As Integer, ByVal PedidosOtrasEmp As Boolean,
			Optional ByVal UserCode As String = "", Optional ByVal UserPassword As String = "") As DataSet
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet

		Using cn
			cn.Open()
			cm = New SqlCommand
			cm.Connection = cn
			cm.CommandText = "FSEP_DEVOLVER_EMPRESAS_PERSONA"
			cm.CommandType = CommandType.StoredProcedure

			cm.Parameters.AddWithValue("@PER", sPer)

			cm.Parameters.Add("@ID", SqlDbType.Int)
			cm.Parameters("@ID").Direction = ParameterDirection.Output

			cm.Parameters.AddWithValue("@SMACTIVADO", iSMActivado)

			cm.Parameters.AddWithValue("@PED_OTRAS_EMPRESAS", PedidosOtrasEmp)

			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)

			Empresa = DBNullToDbl(cm.Parameters("@ID").Value)

			Return dr
		End Using
	End Function
	''' <summary>
	''' Carga todas las personas posibles a las que poder enviar un pedido como destino para el usuario dado, en el idioma pasado como parámetro.
	''' </summary>
	''' <param name="per">Código de la persona del usuario que está utilizando la aplicacion para poder cargar los posibles destinos de su pedido.</param>
	''' <param name="sIdi">Idioma del usuario actual</param>
	''' <returns>Un dataset con todas las personas posibles a las que enviar el pedido</returns>
	''' <remarks>
	''' Llamada desde: la clase CPersona, metodo CargarDestinos
	''' Tiempo máximo: 0 sec.
	''' </remarks>
	Public Function Persona_CargarDestinos(ByVal sIdi As String, ByVal per As String, ByVal sIdDest As String,
										   ByVal SoloDestinoRecepcion As Boolean, ByVal SoloDestinoEmision As Boolean) As DataSet
		Authenticate()
		Dim adoCom As New SqlCommand
		Dim adoPar As New SqlParameter
		Dim adoRS As New DataSet
		adoCom = New SqlCommand
		Dim cn As New SqlConnection(mDBConnection)
		cn.Open()
		adoCom.Connection = cn
		adoCom.CommandType = CommandType.StoredProcedure
		adoCom.CommandText = "FSEP_CARGAR_DESTINOS_PERSONA"
		adoPar = adoCom.Parameters.AddWithValue("@PER", per)
		adoPar = adoCom.Parameters.AddWithValue("@IDI", sIdi)
		adoPar = adoCom.Parameters.AddWithValue("@IDDEST", sIdDest)
		adoPar = adoCom.Parameters.AddWithValue("@SOLODESTINORECEPCION", SoloDestinoRecepcion)
		adoPar = adoCom.Parameters.AddWithValue("@SOLODESTINOEMISION", SoloDestinoEmision)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)
		cn.Close()
		da.Dispose()
		adoCom.Dispose()
		Return adoRS
		adoRS.Clear()
		adoRS = Nothing
	End Function
	''' <summary>
	''' Carga todos los datos de "persona" del usuario que este utilizando la aplicacion
	''' </summary>
	''' <param name="mscod">Código de la persona del usuario que está utilizando la aplicacion para cargar todos sus datos.</param>
	''' <returns>Un dataset con todo los datos de la persona</returns>
	''' <remarks>
	''' Llamada desde: la clase CPersona, metodo CargarPersona
	''' Tiempo máximo: 0 sec.
	''' </remarks>
	Public Function Persona_CargarPersona(ByVal mscod As String) As DataSet
		Authenticate()
		Dim ador As New DataSet
		Dim sConsulta As String
		Dim sqlcmd As New SqlCommand
		Dim cn As New SqlConnection(mDBConnection)
		ador = New DataSet
		cn.Open()
		sqlcmd.Connection = cn
		sConsulta = "SELECT PER.COD,PER.NOM,PER.APE,PER.CAR,PER.TFNO,PER.TFNO2,PER.FAX," & " Per.EMAIL,UON1.DEN AS UON1,UON2.DEN AS UON2,UON3.DEN AS UON3,DEP.DEN AS DEP" & " FROM PER WITH(NOLOCK) INNER JOIN DEP WITH(NOLOCK) ON DEP.COD=PER.DEP" & " LEFT JOIN UON1 WITH(NOLOCK) ON PER.UON1=UON1.COD" & " LEFT JOIN UON2 WITH(NOLOCK) ON PER.UON1=UON2.UON1 AND PER.UON2=UON2.COD" & " LEFT JOIN UON3 WITH(NOLOCK) ON PER.UON1=UON3.UON1 AND PER.UON2=UON2.COD AND PER.UON3=UON3.COD " & " WHERE PER.COD = '" & DblQuote(mscod) & "'"
		sqlcmd.CommandType = CommandType.Text
		sqlcmd.CommandText = sConsulta
		Dim da As New SqlDataAdapter
		da.SelectCommand = sqlcmd
		da.Fill(ador)
		da.Dispose()
		sqlcmd.Dispose()
		cn.Close()
		Return ador
		ador.Clear()
		ador = Nothing
	End Function
	''' Revisado por: blp. Fecha: 11/07/2012
	''' <summary>
	''' Devuelve tabla con códigos, nombre y apellidos de los gestores de los que se ha pasado el código como parámetro 
	''' </summary>
	''' <param name="CodGestores">Tabla con los códigos de los gestores</param>
	''' <returns>Tabla</returns>
	''' <remarks>Llamada desde CPersonas.vb-->CargarGestores. Max 0,1 seg.</remarks>
	Public Function Personas_CargarGestores(ByVal CodGestores As DataTable) As DataTable
		Authenticate()
		Dim oConexion As New SqlConnection(mDBConnection)
		Dim oCommand As SqlCommand = New SqlCommand()
		Dim da As New SqlDataAdapter
		Dim ds As New DataSet
		Try
			oConexion.Open()
			oCommand.Connection = oConexion
			oCommand.CommandText = "SM_OBT_DESCRIPCION_GESTORES"
			oCommand.CommandType = CommandType.StoredProcedure
			'Creamos un parámetro sql de tipo TablaPartidas, que es un tipo "tabla" definido por el usuario en sql server
			Dim ParamGestores As New SqlParameter("@GESTORES", SqlDbType.Structured)
			ParamGestores.TypeName = "GESTORES"
			ParamGestores.Value = CodGestores
			oCommand.Parameters.Add(ParamGestores)
			da.SelectCommand = oCommand
			da.Fill(ds)

			If ds.Tables.Count > 0 Then
				Return ds.Tables(0)
			Else
				CodGestores.Columns("COD").ColumnName = "GESTORCOD"
				Dim dcGestores As DataColumn = CodGestores.Columns("GESTORCOD")
				dcGestores.ColumnName = "GESTORDEN"
				CodGestores.Columns.Add(dcGestores)
				Return CodGestores
			End If

		Catch ex As Exception
			Throw ex
		Finally
			ds.Dispose()
			ds = Nothing
			da.Dispose()
			da = Nothing
			oConexion.Close()
			oConexion.Dispose()
			oCommand.Dispose()
		End Try
	End Function
#End Region
#Region " Personas data access methods"
	''' <summary>
	''' Devuelve un dataset con las personas receptoras relacionadas con la personas que le pasamos por parámetro
	''' </summary>
	''' <returns>El dataset comentado</returns>
	''' <remarks>
	''' Llamada desde: CargarReceptores de CPersona
	''' Tiempo máximo: 0 sec
	''' </remarks>
	''' <revision>LTG 10/06/2013</revision>
	Public Function Personas_CargarReceptores(ByVal sDest As String) As DataSet

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim ds As New DataSet

		Using cn
			cn.Open()
			cm = New SqlCommand
			cm.Connection = cn

			cm.CommandText = "FSEP_DEVOLVER_RECEPTORES"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@DEST", sDest)

			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(ds)

			Return ds
		End Using
	End Function
#End Region
#Region " Proveedores data access methods "
	''' <summary>
	''' Función que devuelve un dataset con los proveedores para hacer pedidos libres
	''' </summary>
	''' <param name="Per">Codigo de la persona</param>
	''' <param name="iNivel">Nivel de categoría</param>
	''' <param name="Cat1">Cod de la categoría de nivel 1</param>
	''' <param name="Cat2">Cod de la categoría de nivel 2</param>
	''' <param name="Cat3">Cod de la categoría de nivel 3</param>
	''' <param name="Cat4">Cod de la categoría de nivel 4</param>
	''' <param name="Cat5">Cod de la categoría de nivel 5</param>
	''' <returns>Un dataset con los proveedores</returns>
	''' <remarks>
	''' Llamada desde PedidoLibre.aspx.vb
	''' Tiempo: 1 seg.</remarks>
	Public Function Proveedores_DevolverProveedoresPedLibre(ByVal Per As String, ByVal iNivel As Short, ByVal Cat1 As Short, ByVal Cat2 As Short, ByVal Cat3 As Short, ByVal Cat4 As Short, ByVal Cat5 As Short, Optional ByVal UserCode As String = "", Optional ByVal UserPassword As String = "") As DataSet

		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet

		Using cn
			cn.Open()
			cm = New SqlCommand
			cm.Connection = cn
			cm.CommandText = "FSEP_CARGAR_PEDIDO_LIBRE"
			cm.CommandType = CommandType.StoredProcedure

			cm.Parameters.AddWithValue("@PER", Per)
			cm.Parameters.AddWithValue("@NIVEL", iNivel)
			cm.Parameters.AddWithValue("@CATN1", Cat1)
			cm.Parameters.AddWithValue("@CATN2", Cat2)
			cm.Parameters.AddWithValue("@CATN3", Cat3)
			cm.Parameters.AddWithValue("@CATN4", Cat4)
			cm.Parameters.AddWithValue("@CATN5", Cat5)
			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)

			Return dr
		End Using
	End Function
	''' <summary>
	''' Función que Devuelve un DataSet con los proveedores que cumplan las opciones de filtro
	''' </summary>
	''' <param name="Cod">Código del proveedor</param>
	''' <param name="Den">Denominación del proveedor</param>
	''' <param name="Pais">País del proveedor</param>
	''' <param name="Provincia">Provincia del proveedor</param>
	''' <param name="Poblacion">Población del proveedor</param>
	''' <param name="CP">Codigo postal del proveedor</param>
	''' <param name="Idioma">Idioma en el que se desea recuperar los datos multiidioma</param>
	''' <returns> Un DataSet con los datos de todos los proveedores que cumplan con las opciones del filtro</returns>
	''' <remarks>
	''' Llamada desde:EPServer\Proveedores\DevolverProveedoresFiltrados
	''' Tiempo Máximo: 0.5 seg</remarks>
	Public Function Proveedores_DevolverProveedoresFiltrados(ByVal Cod As String, ByVal CIF As String, ByVal Den As String, ByVal Pais As String, ByVal Provincia As String, ByVal Poblacion As String, ByVal CP As String, ByVal CodPer As String, ByVal idioma As FSNLibrary.Idioma) As DataSet
		Authenticate()
		Dim Cn As New SqlConnection(mDBConnection)
		Cn.Open()
		Dim SqlCmd As New SqlCommand("FSEP_DEVOLVER_PROVEEDORES_FILTRADOS", Cn)
		Dim SqlParam As New SqlParameter
		Dim Da As New SqlDataAdapter
		Dim Ds As New DataSet
		SqlCmd.CommandType = CommandType.StoredProcedure
		If Not String.IsNullOrEmpty(Cod) Then
			SqlParam = SqlCmd.Parameters.AddWithValue("@CODPROVE", Cod)
		Else
			SqlParam = SqlCmd.Parameters.AddWithValue("@CODPROVE", System.DBNull.Value)
		End If
		If Not String.IsNullOrEmpty(CIF) Then
			SqlParam = SqlCmd.Parameters.AddWithValue("@CIF", CIF)
		Else
			SqlParam = SqlCmd.Parameters.AddWithValue("@CIF", System.DBNull.Value)
		End If
		If Not String.IsNullOrEmpty(Den) Then
			SqlParam = SqlCmd.Parameters.AddWithValue("@DENPROVE", Den)
		Else
			SqlParam = SqlCmd.Parameters.AddWithValue("@DENPROVE", System.DBNull.Value)
		End If
		If Not String.IsNullOrEmpty(Pais) Then
			SqlParam = SqlCmd.Parameters.AddWithValue("@PAIS", Pais)
		Else
			SqlParam = SqlCmd.Parameters.AddWithValue("@PAIS", System.DBNull.Value)
		End If
		If Not String.IsNullOrEmpty(Provincia) Then
			SqlParam = SqlCmd.Parameters.AddWithValue("@PROVINCIA", Provincia)
		Else
			SqlParam = SqlCmd.Parameters.AddWithValue("@PROVINCIA", System.DBNull.Value)
		End If
		If Not String.IsNullOrEmpty(Poblacion) Then
			SqlParam = SqlCmd.Parameters.AddWithValue("@POBLACION", Poblacion)
		Else
			SqlParam = SqlCmd.Parameters.AddWithValue("@POBLACION", System.DBNull.Value)
		End If
		If Not String.IsNullOrEmpty(CP) Then
			SqlParam = SqlCmd.Parameters.AddWithValue("@CP", CP)
		Else
			SqlParam = SqlCmd.Parameters.AddWithValue("@CP", System.DBNull.Value)
		End If
		If Not String.IsNullOrEmpty(CodPer) Then
			SqlParam = SqlCmd.Parameters.AddWithValue("@PER", CodPer)
		Else
			SqlParam = SqlCmd.Parameters.AddWithValue("@PER", System.DBNull.Value)
		End If
		If Not IsNothing(idioma) And idioma.ToString <> "" Then
			SqlParam = SqlCmd.Parameters.AddWithValue("IDIOMA", idioma.ToString)
		Else
			SqlParam = SqlCmd.Parameters.AddWithValue("IDIOMA", "SPA")
		End If
		Da.SelectCommand = SqlCmd
		Da.Fill(Ds)
		Cn.Close()
		Return Ds
		SqlCmd.Dispose()
		Da.Dispose()
		Ds.Clear()
	End Function
	''' Revisado por: sra. Fecha: 22/02/2013
	''' <summary>
	''' Devuelve Dataset con los codigos y denominaciones de los proveedores que concuerdan con las condiciones pasadas como parametros
	''' </summary>
	''' <param name="textoFiltroProveedor">Texto a buscar en la denominacion del arti­culo</param>
	''' <param name="CodPer">Codigo de la persona</param>
	''' <param name="VerPedidosOtrosUsuarios">Permiso para ver pedidos de otros usuarios</param>
	''' <returns>Dataset con los codigos y denominaciones de los proveedores</returns>
	''' <remarks>Llamada desde CProveedores.vb-> DevolverDescripcionesProveedores. Maximo 2 segundos</remarks>
	Public Function Proveedores_DevolverProveedoresCoincidentes(ByVal textoFiltroProveedor As String, ByVal CodPer As String, ByVal VerPedidosOtrosUsuarios As Boolean) As DataSet
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Dim ds As New DataSet
		Dim da As New SqlDataAdapter
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "EP_OBT_PROVEEDORES"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@PER", CodPer)
			cm.Parameters.AddWithValue("@DENPROVE", textoFiltroProveedor)
			cm.Parameters.AddWithValue("@PERMISO_VER_PEDIDOS_OTROS_USUARIOS", VerPedidosOtrosUsuarios)
			da.SelectCommand = cm
			da.Fill(ds)
			Return ds
		Catch e As Exception
			Throw e
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Function
#End Region
#Region "Recepcion data access methods"
	''' Revisado por: blp. Fecha: 20/11/2012
	''' <summary>
	''' Función que graba en las tablas correspondientes la recepción
	''' </summary>
	''' <param name="Fecha">fecha</param>
	''' <param name="Albaran">código de albarán</param>
	''' <param name="Correcto">Campo correcto</param>
	''' <param name="Observaciones">Observaciones</param>
	''' <param name="Subsanado">Subsanado</param>
	''' <param name="Persona">Persona</param>
	''' <param name="Notificar">Notificador</param>
	''' <param name="Cerrar">Cerrar o no la recepción</param>
	''' <param name="Lineas">Líneas de recepción</param>
	''' <param name="grabarEnLog">Grabar o no la recepción en las tablas de log</param>
	''' <param name="UserCode">UserCode</param>
	''' <param name="UserPassword">UserPassword</param>
	''' <param name="FechaSistema">fecha en que se he registrado la recepcion en el sistema</param>
	''' <returns>Devuelve el ID de la recepción grabada en la tabla PEDIDO_RECEP</returns>
	''' <remarks>llamada desde recepcion.aspx.vb. Tiempo Máx inferior a 1 seg.</remarks>
	Public Function Recepcion_RegistrarRecepcion(ByVal Ordenes As List(Of Integer), ByVal Fecha As DateTime, ByVal Albaran As String,
				ByVal Correcto As Boolean, ByVal Observaciones As String, ByVal Subsanado As Boolean, ByVal Persona As String,
				ByVal Notificar As Boolean, ByVal Cerrar As Boolean, ByVal Lineas As List(Of Object),
				ByVal grabarEnLog As Boolean, ByVal origen As Integer, ByVal FechaSistema As DateTime, ByVal FechaContable As DateTime,
				Optional ByVal UserCode As String = "", Optional ByVal UserPassword As String = "") As Integer()
		Authenticate()

		Dim IDs(Ordenes.Count - 1) As Integer
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Using cn
			Try
				cn.Open()
				cm.Connection = cn
				cm.Transaction = cn.BeginTransaction()
				For i As Integer = 0 To Ordenes.Count - 1
					cm.Parameters.Clear()
					cm.CommandType = CommandType.StoredProcedure
					cm.CommandText = "FSEP_NUEVA_RECEPCION"
					cm.Parameters.Add("@ORDEN", SqlDbType.Int)
					cm.Parameters("@ORDEN").Value = Ordenes(i)
					cm.Parameters.Add("@FECHA", SqlDbType.SmallDateTime)
					cm.Parameters("@FECHA").Value = Fecha
					cm.Parameters.Add("@ALBARAN", SqlDbType.VarChar, 100)
					cm.Parameters("@ALBARAN").Value = Albaran
					cm.Parameters.Add("@CORRECTO", SqlDbType.TinyInt)
					cm.Parameters("@CORRECTO").Value = IIf(Correcto, 1, 0)
					cm.Parameters.Add("@OBS", SqlDbType.VarChar, 255)
					cm.Parameters("@OBS").Value = Observaciones
					cm.Parameters.Add("@SUBSANADO", SqlDbType.TinyInt)
					cm.Parameters("@SUBSANADO").Value = IIf(Subsanado, 1, 0)
					cm.Parameters.Add("@PER", SqlDbType.VarChar, 40)
					cm.Parameters("@PER").Value = Persona
					cm.Parameters.Add("@NOTIF", SqlDbType.TinyInt)
					cm.Parameters("@NOTIF").Value = IIf(Notificar, 1, 0)
					cm.Parameters.Add("@LOG", SqlDbType.TinyInt)
					cm.Parameters("@LOG").Value = origen
					cm.Parameters.Add("@ID", SqlDbType.Int)
					cm.Parameters("@ID").Direction = ParameterDirection.Output
					cm.Parameters.Add("@FECHASISTEMA", SqlDbType.SmallDateTime)
					cm.Parameters("@FECHASISTEMA").Value = FechaSistema
					If Not FechaContable = Date.MinValue Then
						cm.Parameters.Add("@FECHACONTABLE", SqlDbType.SmallDateTime)
						cm.Parameters("@FECHACONTABLE").Value = FechaContable
					End If

					cm.ExecuteNonQuery()
					IDs(i) = cm.Parameters("@ID").Value
					For Each lin As Object In Lineas
						If CType(lin.Orden, Integer) = Ordenes(i) AndAlso Not Double.IsNaN(lin.CantRec) Then
							cm.Parameters.Clear()
							cm.CommandText = "FSEP_REGISTRAR_CANTIDAD"
							cm.Parameters.Add("@ORDEN", SqlDbType.Int)
							cm.Parameters("@ORDEN").Value = Ordenes(i)
							cm.Parameters.Add("@RECEP", SqlDbType.Int)
							cm.Parameters("@RECEP").Value = IDs(i)
							cm.Parameters.Add("@LINEA", SqlDbType.Int)
							cm.Parameters("@LINEA").Value = lin.Id
							cm.Parameters.Add("@CANT", SqlDbType.Float)
							cm.Parameters("@CANT").Value = lin.CantRec
							cm.Parameters.Add("@ACTIVO", SqlDbType.Int)
							cm.Parameters("@ACTIVO").Value = IIf(lin.Activo > 0, lin.Activo, DBNull.Value)
							cm.Parameters.Add("@PER", SqlDbType.VarChar, 40)
							cm.Parameters("@PER").Value = Persona
							cm.Parameters.Add("@LOG", SqlDbType.TinyInt)
							cm.Parameters("@LOG").Value = IIf(grabarEnLog = False, DBNull.Value, 1)
							cm.Parameters.Add("@IMPORTE", SqlDbType.Float)
							cm.Parameters("@IMPORTE").Value = lin.Importe
							cm.Parameters.Add("@SALIDA_ALMACEN", SqlDbType.TinyInt)
							cm.Parameters("@SALIDA_ALMACEN").Value = lin.SalidaAlmacen
							cm.ExecuteNonQuery()
						End If
					Next

					cm.Parameters.Clear()
					cm.CommandText = "FSEP_CAMBIAR_ESTADO_RECEPCION"
					cm.Parameters.Add("@ORDEN", SqlDbType.Int)
					cm.Parameters("@ORDEN").Value = Ordenes(i)
					cm.Parameters.Add("@PER", SqlDbType.VarChar, 40)
					cm.Parameters("@PER").Value = Persona
					cm.Parameters.Add("@CERRAR", SqlDbType.TinyInt)
					cm.Parameters("@CERRAR").Value = IIf(Cerrar, 1, 0)
					cm.ExecuteNonQuery()
				Next
				cm.Transaction.Commit()
				cn.Close()
				Return IDs
			Catch ex As SqlException
				cm.Transaction.Rollback()
				If ex.Number = 2627 And ex.Class = 14 Then
					Dim e As New FSNException("Codigo de albaran duplicado.")
					e.Number = FSNException.TipoError.CodigoAlbaranDuplicado
					Throw e
				Else
					Throw ex
				End If
			Catch ex As Exception
				cm.Transaction.Rollback()
				Throw ex
			Finally
				cn.Dispose()
				cm.Dispose()
			End Try
		End Using
	End Function
	''' <summary>
	''' Función que actualiza la cantidad recepcionada de una línea concreta de la recepción
	''' </summary>
	''' <param name="idLinea">id de la línea recepcionada</param>
	''' <param name="Cantidad">Cantidad modificada</param>
	''' <param name="idCaso">Caso</param>
	''' <param name="UserCode">Código del usuario que realiza la modificación</param>
	''' <param name="sPer">Código de la persona que realiza la modificación</param>
	''' <returns>Devuelve la cantidad que se ha grabado realmente en base de datos</returns>
	''' <remarks>llamada desde CRecepcion.Recpcion_GrabarModificacionLineaRecepcion (a su vez, desde ws/Pedidos.asmx) Tiempo Máx inferior a 1 seg.
	''' revisado por ngo(20/12/2011)</remarks>
	Public Function Recepcion_GrabarModificacionLineaRecepcion(ByVal idLinea As Long, ByVal Cantidad As Double, ByVal idCaso As Byte, Optional ByVal UserCode As String = "", Optional ByVal sPer As String = "") As Double()
		Authenticate()
		Dim oConexion As New SqlConnection(mDBConnection)
		Dim oCommand As SqlCommand = New SqlCommand()
		Dim oReturn(3) As Double
		Dim gParametrosGenerales As TiposDeDatos.ParametrosGenerales
		Try
			oConexion.Open()
			oCommand.Connection = oConexion
			oCommand.CommandText = "FSEP_MODIFICAR_CANTIDAD_RECEPCION"
			oCommand.CommandType = CommandType.StoredProcedure
			oCommand.Parameters.AddWithValue("@ID_LINEA_RECEP", idLinea)
			oCommand.Parameters.AddWithValue("@NEW_CANT", Cantidad)
			oCommand.Parameters.AddWithValue("@CASO", idCaso)
			oCommand.Parameters.AddWithValue("@TIPO_ACUMULACION", EnumTipoAcumulacion.RecepcionPedido)
			oCommand.Parameters.AddWithValue("@ACCESO_SM", gParametrosGenerales.gbAccesoFSSM)
			oCommand.Parameters.AddWithValue("@COD_USU", UserCode)
			oCommand.Parameters.AddWithValue("@COD_PER", sPer)
			oCommand.Parameters.Add("@CANT_GRABADA", SqlDbType.Float)
			oCommand.Parameters("@CANT_GRABADA").Direction = ParameterDirection.Output
			oCommand.Parameters("@CANT_GRABADA").Size = -1
			oCommand.Parameters.Add("@CANT_PDTE", SqlDbType.Float)
			oCommand.Parameters("@CANT_PDTE").Direction = ParameterDirection.Output
			oCommand.Parameters("@CANT_PDTE").Size = -1
			oCommand.Parameters.Add("@ESTADO_LINEA", SqlDbType.Int)
			oCommand.Parameters("@ESTADO_LINEA").Direction = ParameterDirection.Output
			oCommand.Parameters("@ESTADO_LINEA").Size = -1
			oCommand.Parameters.Add("@ESTADO_PEDIDO", SqlDbType.Int)
			oCommand.Parameters("@ESTADO_PEDIDO").Direction = ParameterDirection.Output
			oCommand.Parameters("@ESTADO_PEDIDO").Size = -1

			oCommand.ExecuteNonQuery()
			oReturn(0) = DBNullToInteger(oCommand.Parameters("@CANT_GRABADA").Value)
			oReturn(1) = DBNullToInteger(oCommand.Parameters("@CANT_PDTE").Value)
			oReturn(2) = DBNullToInteger(oCommand.Parameters("@ESTADO_LINEA").Value)
			oReturn(3) = DBNullToInteger(oCommand.Parameters("@ESTADO_PEDIDO").Value)
			Return oReturn
		Catch ex As Exception
			Throw ex
		Finally
			oConexion.Close()
			oConexion.Dispose()
			oCommand.Dispose()
		End Try
	End Function
	''' <summary>
	''' Valida que la modificacion de la recepción se pueda realizar
	''' </summary>
	''' <param name="idLinea">id de la linea de la recepcion</param>
	''' <param name="Cantidad">cantidad a recepcionar</param>
	''' <returns>Devuelve la respuesta dependiendo de si se pueda recepcionar o no</returns>
	''' <remarks></remarks>
	Public Function Recepcion_ValidarModificacionLineaRecepcion(ByVal idLinea As Long, ByVal Cantidad As Double) As Integer
		Authenticate()
		Dim oConexion As New SqlConnection(mDBConnection)
		Dim oCommand As SqlCommand = New SqlCommand()
		Try
			oConexion.Open()
			oCommand.Connection = oConexion
			oCommand.CommandText = "FSEP_VALIDAR_MODIFICAR_CANTIDAD_RECEPCION"
			oCommand.CommandType = CommandType.StoredProcedure
			oCommand.Parameters.AddWithValue("@ID_LINEA_RECEP", idLinea)
			oCommand.Parameters.AddWithValue("@NEW_CANT", Cantidad)
			oCommand.Parameters.Add("@RESPUESTA", SqlDbType.Int)
			oCommand.Parameters("@RESPUESTA").Direction = ParameterDirection.Output
			oCommand.Parameters("@RESPUESTA").Size = -1
			oCommand.ExecuteNonQuery()

			Return DBNullToInteger(oCommand.Parameters("@RESPUESTA").Value)
		Catch ex As Exception
			Throw ex
		Finally
			oConexion.Close()
			oConexion.Dispose()
			oCommand.Dispose()
		End Try
	End Function
	''' Revisado por: blp. Fecha: 09/10/2012
	''' <summary>
	''' Función que anula la línea concreta de una recepción 
	''' </summary>
	''' <param name="idLinea">id de la línea recepcionada</param>
	''' <param name="UserCode">Código del usuario que realiza la modificación</param>
	''' <remarks>llamada desde CRecepcion.EliminarLineaDeRecepcion (y a su vez desde recepcion.aspx.vb) Tiempo Máx inferior a 1 seg.
	''' revisado por ngo(20/12/2011)</remarks>
	Public Sub Recepcion_EliminarLineaRecepcion(ByVal idLinea As Integer, Optional ByVal UserCode As String = "")
		Authenticate()
		Dim oConexion As New SqlConnection(mDBConnection)
		Dim oCommand As SqlCommand = New SqlCommand()
		Try
			oConexion.Open()
			oCommand.Connection = oConexion
			oCommand.CommandText = "FSEP_ELIMINAR_LINEA_RECEPCION"
			oCommand.CommandType = CommandType.StoredProcedure
			oCommand.Parameters.AddWithValue("@ID_LINEA_RECEP", idLinea)
			oCommand.Parameters.AddWithValue("@ESTADO_RECIBIDO", TipoEstadoOrdenEntrega.RecibidoYCerrado)
			oCommand.Parameters.AddWithValue("@ESTADO_EN_RECEPCION", TipoEstadoOrdenEntrega.EnRecepcion)
			oCommand.Parameters.AddWithValue("@TIPO_ACUMULACION", EnumTipoAcumulacion.RecepcionPedido)
			oCommand.Parameters.AddWithValue("@COD_USU", UserCode)
			oCommand.ExecuteNonQuery()
		Catch ex As Exception
			Throw ex
		Finally
			oConexion.Close()
			oConexion.Dispose()
			oCommand.Dispose()
		End Try
	End Sub
	''' <summary>
	''' Función que elimina una recepcion
	''' </summary>
	''' <param name="idRecepcion">Identificador de la recepción que se quiere eliminar</param>
	''' <param name="estadoOrden">Estado de la orden a la que corresponde la recepción</param>
	''' <param name="idOrden">Id de la orden a la que corresponde la recepción</param>
	''' <param name="acceso_g_bAccesoFSFA">1->Hay acceso a Facturación</param>
	''' <param name="AccesoFSSM">1->Hay acceso a gestión presupuestaria</param>
	''' <param name="albaran">Número de albarán de la recepción</param>
	''' <param name="codPersona">Código de la persona (tabla PER, campo COD)</param>
	''' <param name="codUsuario">Código del usuario (tabla USU, campo COD)</param>
	''' <param name="coment">comentario de la recepción</param>
	''' <param name="fechaRecep">fecha de la recepción</param>
	''' <param name="correcto">La recepción es correcta</param>
	''' <param name="GrabarEnLog">1->Grabar en log</param>
	''' <param name="idPedido"></param>
	''' <param name="idProveedor">Proveedor</param>
	''' <param name="m_udtOrigen">Origen</param>
	''' <param name="OrdenIncorrecta">Indica si la orden tiene alguna recepción marcada como incorrecta</param>
	''' <param name="TipoOrden">Tipo de orden</param>
	''' <param name="Usu_idioma">Idioma del usuario</param>
	''' <returns>Un número entero que indica si todo ha salido bien(0) o si ha habido algún error.</returns>
	''' <remarks>
	'''     ''' Llamada desde: cRecepcion.EliminarRecepcion
	''' Tiempo máximo: menos de 1 seg 
	''' revisado por ngo 20/01/2012</remarks>
	Public Function Recepcion_EliminarRecepcion(ByVal idRecepcion As Integer, ByVal estadoOrden As Integer, ByVal idOrden As Integer,
						ByVal idPedido As Integer, ByVal idProveedor As String, ByVal GrabarEnLog As Boolean, ByVal m_udtOrigen As Integer,
						ByVal TipoOrden As Integer, ByVal fechaRecep As DateTime, ByVal albaran As String, ByVal correcto As Boolean,
						ByVal coment As String, ByVal codPersona As String, ByVal codUsuario As String, ByVal OrdenIncorrecta As Integer,
						ByVal AccesoFSSM As Boolean, ByVal Usu_idioma As FSNLibrary.Idioma, ByVal acceso_g_bAccesoFSFA As Boolean) As Integer
		Authenticate()

		Dim oConexion As New SqlConnection(mDBConnection)
		Dim oCommand As SqlCommand
		Dim sConsulta As String
		Dim btrans As Boolean
		Dim drLinea As DataRow
		Dim iEstLineaPedido As Object
		Dim bINCORRECTA As Boolean
		Dim IdLog As Integer
		Dim drDataReader As SqlDataReader = Nothing
		Dim iEstOrden, TESError, iEmpresa, iEstado As Integer
		Dim Accion_Baja As String = "D"

		TESError = 0

		oConexion.Open()
		oCommand = New SqlCommand()
		oCommand.Connection = oConexion
		oCommand.Transaction = oConexion.BeginTransaction()

		btrans = True

		'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		'SACAR LINEAS POR CONSULTA EN FUNCION APARTE
		Dim dsLinea As DataSet = Recepcion_CargarLineasRecepcion(AccesoFSSM, idRecepcion, Usu_idioma, fechaRecep, oConexion, oCommand, iEmpresa)
		'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		'LAS USAMOS MÁS ABAJO
		'PERO LAS RECUPERAMOS AQUÍ PORQUE ELIMINAMOS LAS LÍNEAS DE RECEPCION ANTES DE USAR SUS DATOS
		'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

		oCommand.CommandText = "SELECT EST,EMPRESA FROM ORDEN_ENTREGA WITH(NOLOCK) WHERE ID = @ORDEN_ID "
		oCommand.CommandType = CommandType.Text
		oCommand.Parameters.AddWithValue("@ORDEN_ID", idOrden)
		Try
			drDataReader = oCommand.ExecuteReader
		Catch ex As Exception
			drDataReader.Close()
			oCommand.Transaction.Rollback()
			oCommand.Dispose()
			oConexion.Close()
			Throw ex
			Exit Function
		End Try

		If Not drDataReader.HasRows Then
			drDataReader.Close()
			drDataReader = Nothing
			TESError = 166 'No existe la orden de entrega correspondiente
			oCommand.Transaction.Rollback()
			oCommand.Dispose()
			oConexion.Close()
			Return TESError
			Exit Function
		End If
		drDataReader.Read()
		iEmpresa = drDataReader.Item("EMPRESA")
		iEstado = drDataReader.Item("EST")
		drDataReader.Close()
		If estadoOrden <> iEstado Then
			drDataReader.Close()
			drDataReader = Nothing
			TESError = 167 'Estado de la orden modificado
			oCommand.Transaction.Rollback()
			oCommand.Dispose()
			oConexion.Close()
			Return TESError
			Exit Function
		End If

		If acceso_g_bAccesoFSFA Then
			'Se comprueba para poder eliminar que no hay facturas sin anuladas

			'Leo si hay facturas con albaran,
			'si hay anuladas y no hay noanualadas --> dejo eliminar
			'si no hay anulafas y hay noanuladas --> NO dejo modificar
			'si no hay por albaran --> miro si hay sin albaran no anuladas y no dejo modificar
			sConsulta = "SELECT EF.ANULADO,COUNT(LPF.LINEA_FACTURA) NUM FROM LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK) INNER JOIN FACTURA F WITH (NOLOCK) ON LPF.FACTURA=F.ID"
			sConsulta = sConsulta & "  INNER JOIN EST_FACTURA EF WITH (NOLOCK) ON F.ESTADO=EF.ID"
			sConsulta = sConsulta & "  INNER JOIN LINEAS_PEDIDO LP WITH (NOLOCK) ON LP.ID=LPF.LINEA_PEDIDO "
			sConsulta = sConsulta & "  INNER JOIN PEDIDO_RECEP PR WITH (NOLOCK) ON LP.ORDEN=PR.ORDEN AND PR.ALBARAN=LPF.ALBARAN"
			sConsulta = sConsulta & "  WHERE F.PROVE= @PROVE_ID AND PR.ID= @RECEP_ID GROUP BY ANULADO ORDER BY ANULADO "

			oCommand.Parameters.Clear()
			oCommand.Parameters.AddWithValue("@PROVE_ID", idProveedor)
			oCommand.Parameters.AddWithValue("@RECEP_ID", idRecepcion)
			oCommand.CommandText = sConsulta
			oCommand.CommandType = CommandType.Text
			Try
				drDataReader = oCommand.ExecuteReader
			Catch ex As Exception
				drDataReader.Close()
				oCommand.Transaction.Rollback()
				oCommand.Dispose()
				oConexion.Close()
				Throw ex
				Exit Function
			End Try
			If Not drDataReader.HasRows Then
				'No hay del albarán, miro si hay sin albarán para las lineas de la recepción con cantidad
				drDataReader.Close()

				sConsulta = "SELECT COUNT(LPF.LINEA_FACTURA) NUM FROM LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK) INNER JOIN FACTURA F WITH (NOLOCK) ON LPF.FACTURA=F.ID"
				sConsulta = sConsulta & "  INNER JOIN EST_FACTURA EF WITH (NOLOCK) ON F.ESTADO=EF.ID"
				sConsulta = sConsulta & "  INNER JOIN LINEAS_PEDIDO LP WITH (NOLOCK) ON LP.ID=LPF.LINEA_PEDIDO "
				sConsulta = sConsulta & "  INNER JOIN LINEAS_RECEP LR WITH (NOLOCK) ON LP.ID=LR.LINEA "
				sConsulta = sConsulta & "  WHERE F.PROVE= @PROVE_ID AND LP.ORDEN= @ORDEN_ID AND EF.ANULADO=0 AND LPF.ALBARAN IS NULL AND LR.CANT<>0 AND LR.CANT IS NOT NULL"
				oCommand.Parameters.Clear()
				oCommand.Parameters.AddWithValue("@PROVE_ID", idProveedor)
				oCommand.Parameters.AddWithValue("@ORDEN_ID", idOrden)
				oCommand.CommandText = sConsulta
				oCommand.CommandType = CommandType.Text
				Try
					drDataReader = oCommand.ExecuteReader
				Catch ex As Exception
					drDataReader.Close()
					oCommand.Transaction.Rollback()
					oCommand.Dispose()
					oConexion.Close()
					Throw ex
					Exit Function
				End Try
				If drDataReader.HasRows Then
					drDataReader.Read()
					If drDataReader.Item("NUM") > 0 Then
						'hay facturas para la orden sin anular
						drDataReader.Close()
						drDataReader = Nothing '???
						TESError = 168
						oCommand.Transaction.Rollback()
						oCommand.Dispose()
						oConexion.Close()
						Return TESError
						Exit Function
					End If
				End If
				drDataReader.Close()
			Else
				Do While drDataReader.Read()
					If drDataReader.Item("ANULADO") = 0 And drDataReader.Item("NUM") > 0 Then
						'hay facturas para la recepción sin anular
						drDataReader.Close()
						drDataReader = Nothing '???
						TESError = 168
						oCommand.Transaction.Rollback()
						oCommand.Dispose()
						oConexion.Close()
						Return TESError
						Exit Function
					End If
				Loop
				drDataReader.Close()
			End If
		End If
		' INTEGRACION
		If GrabarEnLog Then
			''MultiERP
			Dim sERP As String = ""
			sConsulta = "SELECT ERP_SOCIEDAD.ERP FROM EMP WITH (NOLOCK)"
			sConsulta = sConsulta & " LEFT JOIN ERP_SOCIEDAD WITH (NOLOCK) ON ERP_SOCIEDAD.SOCIEDAD = EMP.SOCIEDAD"
			sConsulta = sConsulta & " WHERE EMP.ID = @EMPRESA "

			oCommand.Parameters.Clear()
			oCommand.Parameters.AddWithValue("@EMPRESA", iEmpresa)

			oCommand.CommandText = sConsulta
			oCommand.CommandType = CommandType.Text

			Try
				drDataReader = oCommand.ExecuteReader
			Catch ex As Exception
				drDataReader.Close()
				oCommand.Transaction.Rollback()
				oCommand.Dispose()
				oConexion.Close()
				Throw ex
				Exit Function
			End Try

			If drDataReader.HasRows Then
				drDataReader.Read()
				sERP = DBNullToStr(drDataReader.Item("ERP"))
			End If
			drDataReader.Close()

			sConsulta = "INSERT INTO LOG_PEDIDO_RECEP (ACCION,ID_PEDIDO_RECEP,ID_PEDIDO,ID_ORDEN_ENTREGA,TIPO,FECHA,ALBARAN,CORRECTO,COMENT,ORIGEN,PER,USU,ERP) VALUES " _
			   & "(@ACCION_BAJA, @RECEPCION_ID, @PEDIDO_ID, @ORDEN_ID, @TIPO_ORDEN, @FECHA_RECEP" _
			   & ", @ALBARAN, @CORRECTO, @COMENT, @ORIGEN, @PERSONA_COD, @USUARIO_COD, @ERP)"

			oCommand.Parameters.Clear()
			oCommand.Parameters.AddWithValue("@ACCION_BAJA", Accion_Baja)
			oCommand.Parameters.AddWithValue("@RECEPCION_ID", idRecepcion)
			oCommand.Parameters.AddWithValue("@PEDIDO_ID", idPedido)
			oCommand.Parameters.AddWithValue("@ORDEN_ID", idOrden)
			oCommand.Parameters.AddWithValue("@TIPO_ORDEN", TipoOrden)
			oCommand.Parameters.AddWithValue("@FECHA_RECEP", CType(fechaRecep, Date))
			oCommand.Parameters.AddWithValue("@ALBARAN", DblQuote(albaran))
			oCommand.Parameters.AddWithValue("@CORRECTO", BooleanToSQLBinary(correcto))
			oCommand.Parameters.AddWithValue("@COMENT", DBNullToStr(coment))
			oCommand.Parameters.AddWithValue("@ORIGEN", m_udtOrigen)
			oCommand.Parameters.AddWithValue("@PERSONA_COD", strToDBNull(codPersona))
			oCommand.Parameters.AddWithValue("@USUARIO_COD", strToDBNull(codUsuario))
			oCommand.Parameters.AddWithValue("@ERP", sERP)

			oCommand.CommandText = sConsulta
			oCommand.CommandType = CommandType.Text

			Try
				oCommand.ExecuteNonQuery()
			Catch ex As Exception
				drDataReader.Close()
				oCommand.Transaction.Rollback()
				oCommand.Dispose()
				oConexion.Close()
				Throw ex
				Exit Function
			End Try

			'Sin WITH(NOLOCK) en el FROM para asegurar la recuperación del mayor valor de la tabla
			sConsulta = "SELECT MAX(ID) AS IDENT FROM LOG_PEDIDO_RECEP WHERE ID_PEDIDO_RECEP= @RECEP_ID"
			oCommand.Parameters.Clear()
			oCommand.Parameters.AddWithValue("@RECEP_ID", idRecepcion)

			oCommand.CommandText = sConsulta
			oCommand.CommandType = CommandType.Text

			Try
				IdLog = oCommand.ExecuteScalar()
			Catch ex As Exception
				drDataReader.Close()
				oCommand.Transaction.Rollback()
				oCommand.Dispose()
				oConexion.Close()
				Throw ex
				Exit Function
			End Try

		End If

		sConsulta = "DELETE FROM LINEAS_RECEP WHERE PEDIDO_RECEP= @RECEPCION_ID"
		oCommand.CommandText = sConsulta
		oCommand.CommandType = CommandType.Text
		oCommand.Parameters.Clear()
		oCommand.Parameters.AddWithValue("@RECEPCION_ID", idRecepcion)
		Try
			oCommand.ExecuteNonQuery()
		Catch ex As Exception
			drDataReader.Close()
			oCommand.Transaction.Rollback()
			oCommand.Dispose()
			oConexion.Close()
			Throw ex
			Exit Function
		End Try

		'FSN_DES_EP_PRT_3314 - Modificar la eliminacion en una recepciÃ³n para eliminar los adjuntos de albarÃ¡n de la recepciÃ³n.
		sConsulta = "DELETE FROM PEDIDO_RECEP_ADJUN WHERE PEDIDO_RECEP= @RECEPCION_ID"
		oCommand.CommandText = sConsulta
		oCommand.CommandType = CommandType.Text
		oCommand.Parameters.Clear()
		oCommand.Parameters.AddWithValue("@RECEPCION_ID", idRecepcion)
		Try
			oCommand.ExecuteNonQuery()
		Catch ex As Exception
			drDataReader.Close()
			oCommand.Transaction.Rollback()
			oCommand.Dispose()
			oConexion.Close()
			Throw ex
			Exit Function
		End Try

		sConsulta = "DELETE FROM PEDIDO_RECEP WHERE ID= @RECEPCION_ID"
		oCommand.CommandText = sConsulta
		oCommand.CommandType = CommandType.Text
		oCommand.Parameters.Clear()
		oCommand.Parameters.AddWithValue("@RECEPCION_ID", idRecepcion)
		Try
			oCommand.ExecuteNonQuery()
		Catch ex As Exception
			drDataReader.Close()
			oCommand.Transaction.Rollback()
			oCommand.Dispose()
			oConexion.Close()
			Throw ex
			Exit Function
		End Try

		For Each drLinea In dsLinea.Tables(0).Rows

			If Not DBNullToSomething(drLinea.Item("CANT_RECIB")) Is Nothing Then
				'Actualizamos la tabla LINEAS_PEDIDO para cambiar el estado si hace falta,para ello recalculamos la cantidad recepcionada hasta el momento
				sConsulta = "SELECT SUM(CANT) AS CANT_RECEPCIONADA FROM LINEAS_RECEP WITH (NOLOCK) WHERE LINEA = @LINEAPED_ID"

				oCommand.CommandText = sConsulta
				oCommand.CommandType = CommandType.Text
				oCommand.Parameters.Clear()
				oCommand.Parameters.AddWithValue("@LINEAPED_ID", drLinea.Item("LINEA"))

				Dim cantidadRecep As Nullable(Of Double)
				Try
					Dim value As Double = DBNullToDbl(oCommand.ExecuteScalar())
					If Not IsDBNull(value) Then
						cantidadRecep = CDbl(value)
					End If
				Catch ex As Exception
					drDataReader.Close()
					oCommand.Transaction.Rollback()
					oCommand.Dispose()
					oConexion.Close()
					Throw ex
					Exit Function
				End Try

				If cantidadRecep.HasValue AndAlso drLinea.Item("CANT_RECEPCIONADA") <> cantidadRecep Then
					drLinea.Item("CANT_RECEPCIONADA") = DBNullToDbl(cantidadRecep)
				End If

				If drLinea.Item("CANT_RECEPCIONADA") = 0 Then
					iEstLineaPedido = 1
				Else
					If drLinea.Item("CANT_PED") > drLinea.Item("CANT_RECEPCIONADA") Then
						iEstLineaPedido = 2
					Else
						iEstLineaPedido = 3
					End If
				End If

				'INTEGRACION
				If GrabarEnLog Then
					sConsulta = "INSERT INTO LOG_LINEAS_RECEP (ID_LOG_PEDIDO_RECEP,ID_PEDIDO_RECEP,ID_ORDEN_ENTREGA,ID_LINEA_RECEP,ID_LINEA_PEDIDO,CANT,ACCION) VALUES "
					sConsulta = sConsulta & " (@LOG_ID,@ID_PEDIDO_RECEP,@ID_ORDEN_ENTREGA,@LINEA_RECEP_ID,@ID_LINEA_PEDIDO,@CANT_RECIBIDA,@ACCION)"

					oCommand.CommandText = sConsulta
					oCommand.CommandType = CommandType.Text
					oCommand.Parameters.Clear()
					oCommand.Parameters.AddWithValue("@CANT_RECIBIDA", DblToSQLFloat(drLinea.Item("CANT_RECIB")))
					oCommand.Parameters.AddWithValue("@LOG_ID", IdLog)
					oCommand.Parameters.AddWithValue("@LINEA_RECEP_ID", drLinea.Item("ID"))
					oCommand.Parameters.AddWithValue("@ID_PEDIDO_RECEP", idRecepcion)
					oCommand.Parameters.AddWithValue("@ID_ORDEN_ENTREGA", idOrden)
					oCommand.Parameters.AddWithValue("@ID_LINEA_PEDIDO", drLinea.Item("LINEA"))
					oCommand.Parameters.AddWithValue("@ACCION", Accion_Baja)

					Try
						oCommand.ExecuteNonQuery()
					Catch ex As Exception
						drDataReader.Close()
						oCommand.Transaction.Rollback()
						oCommand.Dispose()
						oConexion.Close()
						Throw ex
						Exit Function
					End Try
				End If

				'Actualizamos el estado de la línea y la cantidad recibida en LINEAS_PEDIDO
				sConsulta = "UPDATE LINEAS_PEDIDO SET EST = @LINEA_PEDIDO_EST,CANT_REC = @CANT_RECEPCIONADA WHERE ID = @LINEA_PED_ID"

				oCommand.CommandText = sConsulta
				oCommand.CommandType = CommandType.Text
				oCommand.Parameters.Clear()
				oCommand.Parameters.AddWithValue("@LINEA_PEDIDO_EST", iEstLineaPedido)
				oCommand.Parameters.AddWithValue("@CANT_RECEPCIONADA", DblToSQLFloat(drLinea.Item("CANT_RECEPCIONADA")))
				oCommand.Parameters.AddWithValue("@LINEA_PED_ID", drLinea.Item("LINEA"))

				Try
					oCommand.ExecuteNonQuery()
				Catch ex As Exception
					drDataReader.Close()
					oCommand.Transaction.Rollback()
					oCommand.Dispose()
					oConexion.Close()
					Throw ex
					Exit Function
				End Try

				'Añadimos registro a LINEAS_EST con el nuevo estado de la línea, independientemente de que sea el mismo que tenía previamente
				sConsulta = "INSERT INTO LINEAS_EST (PEDIDO, ORDEN, LINEA, EST, FECHA, PER, COMENT) VALUES (@PEDIDO, @ORDEN, @LINEA_PED_ID, @EST, GETDATE(), @PER, @COMENT)"

				oCommand.CommandText = sConsulta
				oCommand.CommandType = CommandType.Text
				oCommand.Parameters.Clear()
				oCommand.Parameters.AddWithValue("@PEDIDO", idPedido)
				oCommand.Parameters.AddWithValue("@ORDEN", idOrden)
				oCommand.Parameters.AddWithValue("@LINEA_PED_ID", drLinea.Item("LINEA"))
				oCommand.Parameters.AddWithValue("@EST", iEstLineaPedido)
				oCommand.Parameters.AddWithValue("@PER", codPersona)
				oCommand.Parameters.AddWithValue("@COMENT", "Recepcion Eliminada desde Recepcion_EliminarRecepcion")

				Try
					oCommand.ExecuteNonQuery()
				Catch ex As Exception
					drDataReader.Close()
					oCommand.Transaction.Rollback()
					oCommand.Dispose()
					oConexion.Close()
					Throw ex
					Exit Function
				End Try

				If DBNullToDbl(drLinea.Item("EST")) <> iEstLineaPedido Then
					drLinea.Item("EST") = iEstLineaPedido
				End If
			End If
		Next

		sConsulta = "SELECT SUM(CORRECTO) AS NUM_CORRECTO, COUNT(PEDIDO_RECEP.ID) AS NUM_RECEP  FROM PEDIDO_RECEP WITH(NOLOCK)  "
		sConsulta = sConsulta & "WHERE PEDIDO_RECEP.ORDEN = @ORDEN_ID"

		oCommand.CommandText = sConsulta
		oCommand.CommandType = CommandType.Text
		oCommand.Parameters.Clear()
		oCommand.Parameters.AddWithValue("@ORDEN_ID", idOrden)

		Try
			drDataReader = oCommand.ExecuteReader()
		Catch ex As Exception
			drDataReader.Close()
			oCommand.Transaction.Rollback()
			oCommand.Dispose()
			oConexion.Close()
			Throw ex
			Exit Function
		End Try

		If Not drDataReader.HasRows Then
			drDataReader.Close()
			oCommand.Transaction.Rollback()
			oCommand.Dispose()
			oConexion.Close()
			TESError = 169 'El siguiente dato ha sido eliminado en otra sesión. Dato:
			Return TESError
			oCommand.Transaction.Rollback()
			Exit Function
		End If
		drDataReader.Read()
		If DBNullToDbl(drDataReader.Item("NUM_CORRECTO")) < DBNullToDbl(drDataReader.Item("NUM_RECEP")) Then
			bINCORRECTA = True
		Else
			bINCORRECTA = False
		End If
		drDataReader.Close()

		sConsulta = "SELECT SUM(LINEAS_PEDIDO.CANT_REC)AS TOTAL_REC,SUM(LINEAS_PEDIDO.CANT_PED*ISNULL(1 + (LINEAS_PEDIDO.PORCEN_DESVIO/100),1)) TOTAL_PED FROM LINEAS_PEDIDO WITH(NOLOCK)  "
		sConsulta = sConsulta & "WHERE LINEAS_PEDIDO.BAJA_LOG = 0 AND LINEAS_PEDIDO.ORDEN = @ORDEN_ID"

		oCommand.CommandText = sConsulta
		oCommand.CommandType = CommandType.Text
		oCommand.Parameters.Clear()
		oCommand.Parameters.AddWithValue("@ORDEN_ID", idOrden)

		Try
			drDataReader = oCommand.ExecuteReader()
		Catch ex As Exception
			drDataReader.Close()
			oCommand.Transaction.Rollback()
			oCommand.Dispose()
			oConexion.Close()
			Throw ex
			Exit Function
		End Try

		drDataReader.Read()
		'Estado de la orden que pondremos por defecto si no quedan registros en orden_est para poner 
		If DBNullToDbl(drDataReader.Item("TOTAL_REC")) = 0 Then
			'Dado que ha habido recepciones eso significa que, si el proveedor tenía que aprobar la orden (ORDEN_ENTREGA.ACEP_PROVE=1), lo hizo en algún momento (si no, no hubiese habido recepciones)
			'de modo que, si al anular, la cantidad recibida es cero, entonces ponemos como estado directamente el 3 (aceptado por el proveedor), sin plantearnos si pudiese ser 2, no puede ser.
			iEstOrden = TipoEstadoOrdenEntrega.AceptadoPorProveedor '3
		Else
			iEstOrden = TipoEstadoOrdenEntrega.EnRecepcion '5
		End If

		drDataReader.Close()

		If OrdenIncorrecta <> bINCORRECTA Then
			sConsulta = "UPDATE ORDEN_ENTREGA SET INCORRECTA = @INCORRECTA WHERE ID = @ORDEN_ID"

			oCommand.CommandText = sConsulta
			oCommand.CommandType = CommandType.Text
			oCommand.Parameters.Clear()
			oCommand.Parameters.AddWithValue("@INCORRECTA", BooleanToSQLBinary(bINCORRECTA))
			oCommand.Parameters.AddWithValue("@ORDEN_ID", idOrden)

			Try
				oCommand.ExecuteNonQuery()
			Catch ex As Exception
				drDataReader.Close()
				oCommand.Transaction.Rollback()
				oCommand.Dispose()
				oConexion.Close()
				Throw ex
				Exit Function
			End Try

			OrdenIncorrecta = bINCORRECTA
		End If

		'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		'ACTUALIZAR EL ESTADO DE LA ORDEN EN ORDEN_ENTREGA Y EN EL HISTÓRICO DE ESTADOS (ORDEN_EST)
		'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		'Importante:
		'   La primera recepción inserta un estado en orden_est (puede verse en el stored FSEP_CAMBIAR_ESTADO_RECEPCION, que se llama desde Recepcion_RegistrarRecepción)
		'   Allí se ve que se inserta un estado 5 cuando comienza la recepción y un 6 cuando se cierra. 
		'   En las recepciones siguientes a la primera no se inserta estado alguno
		'   El estado 6 ha pasado a ser un estado voluntario, o sea, no se cierra porque se complete la recepción sino porque se quiere cerrar el pedido (por el motivo que sea)

		'Independientemente de que el estado de la orden se cambie o no, es decir, que iEstOrden coincida o no con estadoOrden
		'vamos a insertar un registro en ORDEN_EST con un comentario
		'Y sólo cambiaremos el estado de orden_entrega cuando iEstOrden no coincida con estadoOrden y haya que pasarlo de 5 a 3
		'Si el estado de la orden es 6 no cambiamos su estado porque ese estado prima sobre el 3
		'El estado 20 (anulado) sólo es posible si la orden no tiene recepciones y 21 y 22 no son compatibles tampoco con recepciones por lo que podemos ignorarlos.

		'Insertar el estado en ORDEN_EST con un comentario descriptivo
		'Actualizar con ese estado en ORDEN_ENTREGA
		Dim estadoDefinitivo As Integer
		If iEstOrden = TipoEstadoOrdenEntrega.AceptadoPorProveedor Then
			estadoDefinitivo = iEstOrden
		Else
			'Cuando iEstOrden es 5 o 6, si no coincide con el estado de la orden
			If iEstOrden <> estadoOrden Then
				If estadoOrden = TipoEstadoOrdenEntrega.RecibidoYCerrado Then
					estadoDefinitivo = TipoEstadoOrdenEntrega.RecibidoYCerrado
				Else
					estadoDefinitivo = iEstOrden
				End If
			Else
				estadoDefinitivo = estadoOrden
			End If
		End If

		sConsulta = "INSERT INTO ORDEN_EST (PEDIDO, ORDEN, EST, FECHA, PER, COMENT) VALUES (@PEDIDO_ID, @ORDEN_ID, @EST, GETDATE(), @PER, @COMENT)"

		oCommand.CommandText = sConsulta
		oCommand.CommandType = CommandType.Text
		oCommand.Parameters.Clear()
		oCommand.Parameters.AddWithValue("@PEDIDO_ID", idPedido)
		oCommand.Parameters.AddWithValue("@ORDEN_ID", idOrden)
		oCommand.Parameters.AddWithValue("@EST", estadoDefinitivo)
		oCommand.Parameters.AddWithValue("@PER", codPersona)
		oCommand.Parameters.AddWithValue("@COMENT", "Recepcion Eliminada desde Recepcion_EliminarRecepcion")

		Try
			oCommand.ExecuteNonQuery()
		Catch ex As Exception
			drDataReader.Close()
			oCommand.Transaction.Rollback()
			oCommand.Dispose()
			oConexion.Close()
			Throw ex
			Exit Function
		End Try

		'Cuando el estado definitivo sea AceptadoPorProveedor pero el de la orden sea cerrado, añadimos otro estado cerrado a orden_est 
		'para que cuando el usuario decida reabrir de la orden, esta se efectúe sin problemas (stored EP_MOD_REABRIR_ORDEN)
		If estadoDefinitivo = TipoEstadoOrdenEntrega.AceptadoPorProveedor Then
			sConsulta = "INSERT INTO ORDEN_EST (PEDIDO, ORDEN, EST, FECHA, PER, COMENT) VALUES (@PEDIDO_ID, @ORDEN_ID, @EST, GETDATE(), @PER, @COMENT)"

			oCommand.CommandText = sConsulta
			oCommand.CommandType = CommandType.Text
			oCommand.Parameters.Clear()
			oCommand.Parameters.AddWithValue("@PEDIDO_ID", idPedido)
			oCommand.Parameters.AddWithValue("@ORDEN_ID", idOrden)
			oCommand.Parameters.AddWithValue("@EST", TipoEstadoOrdenEntrega.RecibidoYCerrado)
			oCommand.Parameters.AddWithValue("@PER", codPersona)
			oCommand.Parameters.AddWithValue("@COMENT", "Recepcion Eliminada desde Recepcion_EliminarRecepcion")

			Try
				oCommand.ExecuteNonQuery()
			Catch ex As Exception
				drDataReader.Close()
				oCommand.Transaction.Rollback()
				oCommand.Dispose()
				oConexion.Close()
				Throw ex
				Exit Function
			End Try
		End If

		If iEstOrden <> estadoOrden AndAlso
		   iEstOrden = TipoEstadoOrdenEntrega.AceptadoPorProveedor AndAlso
		   estadoOrden <> TipoEstadoOrdenEntrega.RecibidoYCerrado Then

			'Actualizar con ese estado en ORDEN_ENTREGA
			sConsulta = "UPDATE ORDEN_ENTREGA SET EST = @EST WHERE ID = @ORDEN_ID"

			oCommand.CommandText = sConsulta
			oCommand.CommandType = CommandType.Text
			oCommand.Parameters.Clear()
			oCommand.Parameters.AddWithValue("@EST", estadoDefinitivo)
			oCommand.Parameters.AddWithValue("@ORDEN_ID", idOrden)

			Try
				oCommand.ExecuteNonQuery()
			Catch ex As Exception
				drDataReader.Close()
				oCommand.Transaction.Rollback()
				oCommand.Dispose()
				oConexion.Close()
				Throw ex
				Exit Function
			End Try
		End If

		oCommand.Transaction.Commit()
		oCommand.Dispose()
		oConexion.Close()

		btrans = False

		Return TESError

		Exit Function

	End Function
	''' Revisado por: blp. fecha:18/10/2012
	''' <summary>
	''' Carga las lineas de la recepción
	''' </summary>
	''' <param name="lEmp">Id de empresa del pedido para cargarla en las imputaciones</param>
	''' <returns>Dataset con las lineas de una recepcion</returns>
	''' <remarks>Llamada desde: Tiempo máximo: </remarks>
	Public Function Recepcion_CargarLineasRecepcion(ByVal AccesoFSSM As Boolean, ByVal idRecepcion As Integer, ByVal Usu_idioma As FSNLibrary.Idioma, ByVal fechaRecep As DateTime, ByRef oConexion As SqlConnection, ByRef oCommand As SqlCommand, Optional ByVal lEmp As Long = 0) As DataSet
		Dim oConex As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		cm = New SqlCommand
		Dim ds As New DataSet
		Dim hayPartidasPres As Boolean = False
		Dim da As New SqlDataAdapter

		Try

			oConex.Open()
			cm.Connection = oConex
			cm.Transaction = oConex.BeginTransaction()

			cm.CommandText = "SELECT COUNT(*) FROM PARGEN_SM WITH(NOLOCK)"
			cm.CommandType = CommandType.Text

			Try
				da.SelectCommand = cm
				da.Fill(ds)
				If ds.Tables(0).Rows(0).Item(0) > 0 Then
					hayPartidasPres = True
				End If
			Catch ex As Exception
				Throw ex
			Finally
				ds.Dispose()
				da.Dispose()
			End Try

			Dim sConsulta As String
			Dim sConsulta1 As String
			Dim sPAram As String
			Dim iIDLineaPed As Integer

			sConsulta1 = "SELECT LINEAS_RECEP.ID,LINEAS_RECEP.LINEA,LINEAS_PEDIDO.ART_INT AS COD,ITEM.DESCR + LINEAS_PEDIDO.DESCR_LIBRE AS DESCR, LINEAS_PEDIDO.DEST,LINEAS_PEDIDO.UP,LINEAS_PEDIDO.PREC_UP,LINEAS_PEDIDO.FECENTREGAPROVE,LINEAS_PEDIDO.CANT_PED*ISNULL(1 + (LINEAS_PEDIDO.PORCEN_DESVIO/100),1) AS CANT_PED,LINEAS_RECEP.CANT AS CANT_RECIB,LINEAS_RECEP.FECACT,LINEAS_PEDIDO.EST,LINEAS_PEDIDO.PEDIDOABIERTO,A4.CONCEPTO,A4.ALMACENAR,A4.RECEPCIONAR "
			sConsulta1 = sConsulta1 & " ,LINEAS_PEDIDO.ALMACEN,ALM.COD CODALMACEN,ALM.DEN DENALMACEN,ITEM.FECINI,ITEM.FECFIN"

			If AccesoFSSM Then
				sConsulta1 = sConsulta1 & " ,LINEAS_PEDIDO.ACTIVO,ACT.COD CODACTIVO,ACTDEN.DEN DENACTIVO"
				If hayPartidasPres Then
					sConsulta1 = sConsulta1 & " ,LPI.ID as IMP,LPI.UON1,LPI.UON2,LPI.UON3,LPI.UON4,LPI.PRES0,LPI.PRES1,LPI.PRES2,LPI.PRES3,LPI.PRES4"
					sConsulta1 = sConsulta1 & " ,PRES.DEN PRESDEN,UON1.DEN U1DEN,UON2.DEN U2DEN,UON3.DEN U3DEN,UON4.DEN U4DEN"
					sConsulta1 = sConsulta1 & " ,P5I.FECINI PFECINI,P5I.FECFIN PFECFIN,P5I.CERRADO"
				End If
			End If
			sConsulta1 = sConsulta1 & " FROM LINEAS_RECEP WITH (NOLOCK)"
			sConsulta1 = sConsulta1 & " INNER JOIN LINEAS_PEDIDO WITH (NOLOCK) ON LINEAS_RECEP.LINEA = LINEAS_PEDIDO.ID"
			sConsulta1 = sConsulta1 & " LEFT JOIN ART4 A4 WITH (NOLOCK) ON A4.COD = LINEAS_PEDIDO.ART_INT"
			sConsulta1 = sConsulta1 & " LEFT JOIN ALMACEN ALM WITH (NOLOCK) ON ALM.ID=LINEAS_PEDIDO.ALMACEN"

			If AccesoFSSM Then
				sConsulta1 = sConsulta1 & " LEFT JOIN ACTIVO ACT WITH (NOLOCK) ON ACT.ID=LINEAS_PEDIDO.ACTIVO"
				sConsulta1 = sConsulta1 & " LEFT JOIN ACTIVO_DEN ACTDEN WITH (NOLOCK) ON ACT.ID=ACTDEN.ACTIVO AND ACTDEN.IDI=@IDIOMA"
				If hayPartidasPres Then
					sConsulta1 = sConsulta1 & " LEFT JOIN LINEAS_PED_IMPUTACION LPI WITH (NOLOCK) ON LINEAS_PEDIDO.ID=LPI.LINEA"
					sConsulta1 = sConsulta1 & " LEFT JOIN PRES5_IDIOMAS PRES WITH (NOLOCK) ON LPI.PRES0=PRES.PRES0 AND LPI.PRES1=PRES.PRES1 AND ISNULL(LPI.PRES2,0)=ISNULL(PRES.PRES2,0) AND ISNULL(LPI.PRES3,0)=ISNULL(PRES.PRES3,0) AND ISNULL(LPI.PRES4,0)=ISNULL(PRES.PRES4,0) AND PRES.IDIOMA=@IDIOMA"
					sConsulta1 = sConsulta1 & " LEFT JOIN UON1 WITH (NOLOCK) ON LPI.UON1=UON1.COD "
					sConsulta1 = sConsulta1 & " LEFT JOIN UON2 WITH (NOLOCK) ON LPI.UON1=UON2.UON1 AND LPI.UON2=UON2.COD"
					sConsulta1 = sConsulta1 & " LEFT JOIN UON3 WITH (NOLOCK) ON LPI.UON1=UON3.UON1 AND LPI.UON2=UON3.UON2 AND LPI.UON3=UON3.COD "
					sConsulta1 = sConsulta1 & " LEFT JOIN UON4 WITH (NOLOCK) ON LPI.UON1=UON4.UON1 AND LPI.UON2=UON4.UON2 AND LPI.UON3=UON4.UON3 AND LPI.UON4=UON4.COD"
					sConsulta1 = sConsulta1 & " LEFT JOIN PRES5_IMPORTES P5I WITH (NOLOCK) ON LPI.PRES0=P5I.PRES0 AND LPI.PRES1=P5I.PRES1 AND ISNULL(LPI.PRES2,0)=ISNULL(P5I.PRES2,0) AND ISNULL(LPI.PRES3,0)=ISNULL(P5I.PRES3,0) AND ISNULL(LPI.PRES4,0)=ISNULL(P5I.PRES4,0)"
				End If
			End If

			sConsulta = sConsulta1 & " LEFT JOIN ITEM WITH (NOLOCK) ON LINEAS_PEDIDO.ANYO = ITEM.ANYO AND LINEAS_PEDIDO.GMN1 = ITEM.GMN1_PROCE AND LINEAS_PEDIDO.PROCE = ITEM.PROCE AND LINEAS_PEDIDO.ITEM = ITEM.ID"
			sConsulta = sConsulta & " WHERE LINEAS_RECEP.PEDIDO_RECEP=@RECEP"
			sConsulta = sConsulta & " ORDER BY COD, DESCR"

			cm.Parameters.AddWithValue("@IDIOMA", Usu_idioma.ToString())
			cm.Parameters.AddWithValue("@RECEP", idRecepcion)

			sConsulta = "EXEC SP_EXECUTESQL N'" & sConsulta & "',N'@IDIOMA VARCHAR(20),@RECEP INTEGER"

			''sPAram = "',?,?"
			sPAram = "',@IDIOMA, @RECEP"
			sConsulta = sConsulta & sPAram

			cm.CommandText = "SET CONCAT_NULL_YIELDS_NULL OFF"
			cm.CommandType = CommandType.Text
			cm.ExecuteNonQuery()

			cm.CommandText = sConsulta
			cm.CommandType = CommandType.Text

			ds = New DataSet
			da = New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(ds)

			cm.CommandText = "SET CONCAT_NULL_YIELDS_NULL ON"
			cm.CommandType = CommandType.Text
			cm.ExecuteNonQuery()

			If ds.Tables(0).Rows.Count > 0 Then

				ds.Tables(0).Columns.Add("CANT_RECEPCIONADA")

				For Each fila As DataRow In ds.Tables(0).Rows
					iIDLineaPed = DBNullToDec(fila.Item("LINEA"))

					If Not iIDLineaPed = 0 Then
						' Obtenemos la cantidad recepcionada hasta el momento
						sConsulta = "SELECT SUM(CANT) AS CANT_RECEPCIONADA FROM LINEAS_RECEP WITH (NOLOCK)"
						sConsulta = sConsulta & " INNER JOIN PEDIDO_RECEP WITH (NOLOCK) ON PEDIDO_RECEP.ID = LINEAS_RECEP.PEDIDO_RECEP AND (PEDIDO_RECEP.FECHA < @FECHARECEP OR (PEDIDO_RECEP.FECHA = @FECHARECEP AND PEDIDO_RECEP.ID < @RECEP_ID))"
						sConsulta = sConsulta & " WHERE LINEAS_RECEP.LINEA = @LINEA_PEDIDO_ID"

						cm.CommandText = sConsulta
						cm.CommandType = CommandType.Text
						cm.Parameters.Clear()

						cm.Parameters.AddWithValue("@FECHARECEP", CType(fechaRecep, Date))
						cm.Parameters.AddWithValue("@RECEP_ID", idRecepcion)
						cm.Parameters.AddWithValue("@LINEA_PEDIDO_ID", iIDLineaPed)

						Dim iCant_recep As Nullable(Of Double)
						Dim value As Object = cm.ExecuteScalar()
						If Not IsDBNull(value) Then
							iCant_recep = CDbl(value)
						End If
						If iCant_recep.HasValue Then
							fila.Item("CANT_RECEPCIONADA") = iCant_recep
						Else
							fila.Item("CANT_RECEPCIONADA") = 0
						End If
					End If
				Next

			End If
			cm.Transaction.Commit()
		Catch ex As Exception
			oCommand.Transaction.Rollback()
			oCommand.Dispose()
			oConexion.Close()
		End Try

		cm.Dispose()
		oConex.Close()
		Return ds

	End Function
	''' Revisado por: blp. Fecha: 09/10/2012
	''' <summary>
	''' Recuperar los datos de una recepción
	''' </summary>
	''' <param name="Albaran">Número de albarán</param>
	''' <param name="FechaAlbaran">Fecha de recepción del albarán</param>
	''' <param name="Prove">Código del proveedor</param>
	''' <returns>Dataset con los resultados</returns>
	''' <remarks>Llamada desde FSNServer\App_Classes\EP\CRecepcion.vb-->CargarDatosRecepcion. Máx 1 seg.</remarks>
	Public Function Recepcion_DatosRecepcionAlbaran(ByVal Albaran As String, ByVal FechaAlbaran As Date, ByVal Prove As String) As DataSet
		Dim oConexion As New SqlConnection(mDBConnection)
		Dim oCommand As SqlClient.SqlCommand
		Dim oDataAdapter As SqlClient.SqlDataAdapter
		Dim oDataset As New DataSet
		Dim sConsulta As String

		oConexion.Open()
		oCommand = New SqlCommand
		oCommand.Connection = oConexion

		sConsulta = "SELECT PR.ID RECEP_ID, PR.PEDIDO, PR.ORDEN, PR.CORRECTO, PR.COMENT RECEP_OBS"
		sConsulta = sConsulta & ", OE.EST ORDEN_EST,OE.INCORRECTA,OE.TIPO ORDEN_TIPO"
		sConsulta = sConsulta & " FROM ORDEN_ENTREGA OE WITH (NOLOCK)"
		sConsulta = sConsulta & " INNER JOIN PEDIDO_RECEP PR WITH (NOLOCK) "
		sConsulta = sConsulta & " ON OE.ID = PR.ORDEN"
		sConsulta = sConsulta & " WHERE "
		sConsulta = sConsulta & " PR.ALBARAN = @ALBARAN"
		sConsulta = sConsulta & " AND CONVERT(DATE, PR.FECHA) = @FECHA"
		sConsulta = sConsulta & " AND OE.PROVE = @PROVE"

		oCommand.CommandText = sConsulta
		oCommand.CommandType = CommandType.Text
		oCommand.Parameters.AddWithValue("@ALBARAN", Albaran)
		oCommand.Parameters.AddWithValue("@FECHA", FechaAlbaran)
		oCommand.Parameters.AddWithValue("@PROVE", Prove)

		oDataAdapter = New SqlDataAdapter

		Try
			oDataAdapter.SelectCommand = oCommand
			oDataAdapter.Fill(oDataset)
			Return oDataset
		Catch ex As Exception
			Throw ex
		Finally
			oCommand.Dispose()
			oConexion.Close()
		End Try
	End Function
	''' <summary>
	''' Función que comprueba si la línea (artículo) de una orden ya tiene recepciones por el total (o más) de su cantidad
	''' </summary>
	''' <param name="IdLinea">ID de la línea cuyas recepciones se quieren comprobar</param>
	''' <returns>
	''' True-> La línea está recepcionada por el total o más de su cantidad
	''' False-> Lo contrario
	''' </returns>
	''' <remarks>Llamada desde Crecepcion.vb->RegistrarRecepcion. Tiempo Máximo inferior a 0,1 seg</remarks>
	Public Function Recepcion_lineaTotalmenteRecepcionada(ByVal IdLinea As Integer) As Boolean
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Using cn
			Try
				cn.Open()
				cm.Connection = cn
				cm.CommandType = CommandType.StoredProcedure
				cm.CommandText = "FSEP_COMPROBAR_RECEPCION_LINEA"
				cm.Parameters.Add("@IDLINEA", SqlDbType.Int)
				cm.Parameters("@IDLINEA").Value = IdLinea
				Dim rdo As Nullable(Of Integer) = cm.ExecuteScalar
				cn.Close()
				cn.Dispose()
				cm.Dispose()
				If rdo Is Nothing Then
					Return False
				Else
					Return True
				End If
			Catch ex As Exception
				Throw ex
			Finally
				cn.Dispose()
				cm.Dispose()
			End Try
		End Using
	End Function
	''' Revisado: blp. Fecha:11/05/2012
	''' <summary>
	''' Bloquear para factuacion todas las ordenes de las que haya recepciones del albaran indicado
	''' </summary>
	''' <param name="Albaran">Numero de albaran para el que hay que bloquear sus ordenes asociadas</param>
	''' <param name="idOrden">Id de la orden de la cual hay que bloquear los albaranes que coincidan con el parametro Albaran</param>
	''' <param name="Observaciones">Comentarios relativos al bloqueo</param>
	''' <returns>0 -> se ha bloqueado correctamente. 1 -> No se ha actualizado ningun registro</returns>
	''' <remarks>Llamada desde Crecepcion.vb-->BloquearFacturacionAlbaran. Maximo 0,3 seg.</remarks>
	Public Function Recepcion_BloquearFacturacionAlbaran(ByVal Albaran As String, ByVal idOrden As Integer, ByVal Observaciones As String) As Integer
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Using cn
			Try
				cn.Open()
				cm.Connection = cn
				cm.CommandType = CommandType.StoredProcedure
				cm.CommandText = "EP_IM_MOD_BLOQUEO_ALBARAN"
				cm.Parameters.Add("@ALBARAN", SqlDbType.VarChar)
				cm.Parameters("@ALBARAN").Value = Albaran
				cm.Parameters.Add("@ORDEN", SqlDbType.Int)
				cm.Parameters("@ORDEN").Value = idOrden
				cm.Parameters.Add("@OBS", SqlDbType.VarChar)
				cm.Parameters("@OBS").Value = Left(Observaciones, 2000)
				cm.Parameters.Add("@BLOQUEO", SqlDbType.TinyInt)
				cm.Parameters("@BLOQUEO").Value = 1
				Dim rdo As Integer = cm.ExecuteNonQuery
				cm.Dispose()
				cn.Close()
				cn.Dispose()
				If rdo > 0 Then 'se han bloqueado una o mas ordenes
					Return 0 'Correcto
				Else
					Return 1 'No se ha actualizado ningun registro.
				End If
			Catch ex As Exception
				Throw ex
			Finally
				cn.Close()
				cn.Dispose()
				cm.Dispose()
			End Try
		End Using
	End Function

	''' Revisado: blp. Fecha:11/05/2012
	''' <summary>
	''' Desbloquear para factuación todas las órdenes de las que haya recepciones del albarán indicado
	''' </summary>
	''' <param name="Albaran">Número de albarán para el que hay que bloquear sus órdenes asociadas</param>
	''' <param name="idOrden">Id de la orden de la cual hay que bloquear los albaranes que coincidan con el parámetro Albaran</param>
	''' <returns>0 -> se ha desbloqueado correctamente. 1 -> No se ha desbloqueado ningún registro</returns>
	''' <remarks>Llamada desde Crecepcion.vb-->BloquearFacturacionAlbaran. Maximo 0,3 seg.</remarks>
	Public Function Recepcion_DesbloquearFacturacionAlbaran(ByVal Albaran As String, ByVal idOrden As Integer) As Integer
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Using cn
			Try
				cn.Open()
				cm.Connection = cn
				cm.CommandType = CommandType.StoredProcedure
				cm.CommandText = "EP_IM_MOD_BLOQUEO_ALBARAN"
				cm.Parameters.Add("@ALBARAN", SqlDbType.VarChar)
				cm.Parameters("@ALBARAN").Value = Albaran
				cm.Parameters.Add("@ORDEN", SqlDbType.Int)
				cm.Parameters("@ORDEN").Value = idOrden
				cm.Parameters.Add("@BLOQUEO", SqlDbType.TinyInt)
				cm.Parameters("@BLOQUEO").Value = 0
				Dim rdo As Integer = cm.ExecuteNonQuery
				cm.Dispose()
				cn.Close()
				cn.Dispose()
				If rdo > 0 Then 'se han bloqueado una o más órdenes
					Return 0 'Correcto
				Else
					Return 1 'No se ha actualizado ningún registro.
				End If
			Catch ex As Exception
				Throw ex
			Finally
				cn.Close()
				cn.Dispose()
				cm.Dispose()
			End Try
		End Using
	End Function
	''' <summary>
	''' Devuelve el importe total recibido de una determinada línea
	''' </summary>
	''' <param name="idLinea">identificador de línea</param>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Function getImporteRecibido(ByVal idLinea As Long) As Double
		Authenticate()
		Dim oConexion As New SqlConnection(mDBConnection)
		Dim oCommand As SqlClient.SqlCommand
		Dim oDataAdapter As SqlClient.SqlDataAdapter
		Dim oDataset As New DataSet
		Dim sConsulta As String
		Dim importe As Double
		oConexion.Open()
		oCommand = New SqlCommand
		oCommand.Connection = oConexion

		sConsulta = "SELECT LINEA,SUM(LINEAS_RECEP.IMPORTE) AS IMPORTE_RECIBIDO " _
									& "FROM LINEAS_RECEP WITH(NOLOCK) " _
									& "GROUP BY LINEA " _
									& "HAVING LINEA=@IDLINEA "
		oCommand.CommandText = sConsulta
		oCommand.CommandType = CommandType.Text
		oCommand.Parameters.AddWithValue("@IDLINEA", idLinea)

		oDataAdapter = New SqlDataAdapter

		Try

			oDataAdapter.SelectCommand = oCommand
			oDataAdapter.Fill(oDataset)
			If oDataset.Tables(0).Rows.Count > 0 Then
				importe = oDataset.Tables(0).Rows(0)("IMPORTE_RECIBIDO")
			Else
				importe = 0
			End If
			oDataset.Dispose()
		Catch ex As Exception
			Throw ex
		Finally
			oCommand.Dispose()
			oConexion.Close()
		End Try
		Return importe
	End Function
#End Region
#Region " Pedido data access methods "
	Public Function Pedido_NotificarAProveedores(ByVal Pedido As Integer) As DataSet
		Dim ador As New DataSet
		Dim sConsulta As String
		Dim cn As New SqlConnection(mDBConnection)
		'Obtiene las órdenes del pedido que no tienen ninguna
		'línea pendiente de aprobar y ninguna denegada

		sConsulta = "SELECT OE.ID,OE.PEDIDO,OE.ANYO,OE.PROVE,OE.NUM,OE.NUMEXT,OE.EST,OE.TIPO,OE.FECHA,OE.IMPORTE,OE.ORDEN_EST,OE.FECACT,OE.INCORRECTA,OE.MON,OE.CAMBIO,OE.OBS,OE.PED_ERP,OE.EMPRESA,OE.COD_PROVE_ERP,OE.RECEPTOR,OE.NUM_PED_ERP,OE.PAG,OE.ORGCOMPRAS,OE.VIA_PAG,OE.TIPOPEDIDO,OE.ACEP_PROVE,OE.VISITADO_P,OE.ABONO,OE.FAC_DIR_ENVIO "
		sConsulta = sConsulta & "  FROM ORDEN_ENTREGA OE WITH(NOLOCK)"
		sConsulta = sConsulta & " WHERE OE.PEDIDO=" & Pedido
		sConsulta = sConsulta & "   AND NOT EXISTS (SELECT LP.ORDEN FROM LINEAS_PEDIDO LP WITH(NOLOCK) WHERE LP.EST!=1 AND LP.ORDEN=OE.ID)"
		Dim sqlcmd As New SqlCommand
		sqlcmd.Connection = cn
		sqlcmd.CommandText = sConsulta
		sqlcmd.CommandType = CommandType.Text

		cn.Open()
		Dim da As New SqlDataAdapter
		da.SelectCommand = sqlcmd
		da.Fill(ador)
		Return ador
	End Function
	''' <summary>Devuelve los usuarios a los que hay que notificar el paso del proveedor de QA a real</summary>
	''' <param name="sProve">Cod. proveedor</param>
	''' <returns>datable con los usuarios a notificas</returns>
	''' <remarks>Llamada desde: CEmisionPedidos.NotificarPasoProveedorQAaReal</remarks>
	Public Function Notificador_NotificarPasoAReal(ByVal sProve As String) As DataTable
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Dim ds As New DataSet
		Dim da As New SqlDataAdapter
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_NOTIF_PROVEQA_A_REAL"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@PROVE", sProve)

			da.SelectCommand = cm
			da.Fill(ds)
			Return ds.Tables(0)
		Catch e As Exception
			Throw e
		Finally
			If Not cn Is Nothing Then cn.Dispose()
			If Not cm Is Nothing Then cm.Dispose()
			If Not da Is Nothing Then da.Dispose()
		End Try
	End Function
	Public Sub Proveedor_PasoQAaRealNotificado(ByVal dtIDs As DataTable)
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSQA_MARCAR_NOTIFICADO_PASOQA_A_REAL"
			cm.CommandType = CommandType.StoredProcedure
			Dim oParam As SqlParameter = cm.Parameters.AddWithValue("@IDS", dtIDs)
			oParam.SqlDbType = SqlDbType.Structured

			cm.ExecuteNonQuery()
		Catch e As Exception
			Throw e
		Finally
			If Not cn Is Nothing Then cn.Dispose()
			If Not cm Is Nothing Then cm.Dispose()
		End Try
	End Sub
#End Region
#Region " Proves_ERP data access methods "
	''' <summary>
	''' Devuelve los datos de los proveedores ERP para la empresa dada, o el proveedor dado, o sólo uno si se incluye el código
	''' </summary>
	''' <param name="lEmpresa">Código de la empresa, si no se introduce ninguno, se devolvera para todas</param>
	''' <param name="sProve">Código del proveedor, si no se introduce ninguno, se devolvera para todos</param>
	''' <param name="sCodERP">Código Erp, si se introduce solo se sacaran los datos de este</param>
	''' <returns>Un dataset con los datos de los proveedores ERP para los parámetros introducidos</returns>
	''' <remarks>
	''' Llamada desde:La clase CProveERPs, método CargarProveedoresERP
	''' Tiempo máximo: 50 milisegundos</remarks>
	Public Function ProvesErp_CargarProveedoresERP(ByVal lEmpresa As Integer, ByVal sProve As String, ByVal sCodERP As String) As DataSet
		Authenticate()
		Dim rs As New DataSet
		Dim cn As New SqlConnection(mDBConnection)
		Dim sqlCmd As New SqlCommand
		Dim SqlParam As New SqlParameter
		If sProve = "" Then
			sProve = StrToSQLNULL(sProve)
		End If

		SqlParam = sqlCmd.Parameters.AddWithValue("@EMPRESA", lEmpresa)
		SqlParam = sqlCmd.Parameters.AddWithValue("@PROVE", sProve)
		If sCodERP <> "" Then
			SqlParam = sqlCmd.Parameters.AddWithValue("@CODERP", sCodERP)
		Else
			SqlParam = sqlCmd.Parameters.AddWithValue("@CODERP", System.DBNull.Value)
		End If
		sqlCmd.CommandText = "FSEP_DEVOLVER_PROVES_ERP"
		sqlCmd.CommandType = CommandType.StoredProcedure
		sqlCmd.Connection = cn
		cn.Open()
		Dim da As New SqlDataAdapter
		da.SelectCommand = sqlCmd
		da.Fill(rs)
		da.Dispose()
		sqlCmd.Dispose()
		cn.Close()
		Return rs
		rs.Clear()
	End Function

	''' <summary>
	''' Devuelve los datos de los proveedores ERP para la el proveedor y la organizacion de compras, se utiliza para el campo proveedor ERP(143) de PM
	''' </summary>
	''' <param name="sProve">Codigo del proveedor</param>
	''' <param name="sOrgCompras">Codigo de la organizacion de compras</param>
	''' <returns>Dataset con los datos de los proveedores ERP</returns>
	''' <remarks></remarks>
	Public Function ProvesErp_CargarProveedoresERPtoDS(Optional ByVal sProve As String = Nothing, Optional ByVal sOrgCompras As String = Nothing, Optional ByVal sCodERP As String = Nothing) As DataSet
		Authenticate()
		Dim rs As New DataSet
		Dim cn As New SqlConnection(mDBConnection)
		Dim sqlCmd As New SqlCommand
		Dim SqlParam As New SqlParameter
		Dim da As New SqlDataAdapter
		Try

			SqlParam = sqlCmd.Parameters.AddWithValue("@PROVE", sProve)
			SqlParam = sqlCmd.Parameters.AddWithValue("@ORGCOMPRAS", sOrgCompras)
			SqlParam = sqlCmd.Parameters.AddWithValue("@COD_ERP", sCodERP)

			sqlCmd.CommandText = "FSPM_DEVOLVER_PROVES_ERP"
			sqlCmd.CommandType = CommandType.StoredProcedure
			sqlCmd.Connection = cn

			da.SelectCommand = sqlCmd
			da.Fill(rs)

			Return rs
		Catch ex As Exception
			Throw ex
		Finally
			cn.Close()
			da.Dispose()
			sqlCmd.Dispose()
		End Try
	End Function

	''' <summary>
	''' Comprueba si hay intergaciÃ³n de pedidos de catÃ¡logo en sentido salida o entrada/salida, y de proveedores con tabla intermedia 
	''' </summary>
	''' <param name="lEmp">Codigo de la empresa proveedor</param>
	''' <returns>Dataset con el estado de la integracion</returns>
	''' <remarks></remarks>

	Public Function ProvesErp_DevuelveCodERP(ByVal lEmp As Integer) As DataSet

		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand()
		Dim dr As New DataSet
		Dim da As New SqlDataAdapter
		Dim Param As New SqlParameter

		Try
			cn.Open()

			If lEmp <> 0 Then
				Param = cm.Parameters.AddWithValue("@EMP", lEmp)
				cm.CommandText = "FSEP_COMPROBAR_PROVE_ERP"
				cm.CommandType = CommandType.StoredProcedure
				cm.Connection = cn
				da.SelectCommand = cm
				da.Fill(dr)
			End If

			Return dr
		Catch e As Exception
			Throw e
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Function
#End Region
#Region " Destinos data access methods "
	''' <summary>
	''' Devuelve todos los destinos posibles del usuario en el idioma seleccionado.
	''' </summary>
	''' <param name="sIdi">Idioma de la aplicacion</param>
	''' <param name="usu">Código de usuario de la persona que este usando la aplicación</param>
	''' <returns>Un DataSet con todos los destinos posibles para los parámetros pasados</returns>
	''' <remarks>
	''' Llamada desde: La clase cDestinos, método CargarTodosLosDestinos</remarks>
	Public Function Destinos_CargarTodosLosDestinos(ByVal sIdi As String, ByVal usu As String) As DataSet
		Authenticate()
		Dim ador As New DataSet
		Dim adoComm As New SqlCommand
		Dim adoParam As New SqlParameter
		Dim sConsulta As String
		Dim cn As New SqlConnection(mDBConnection)
		adoComm.Connection = cn
		adoComm.CommandType = CommandType.Text
		cn.Open()
		If usu = "" Then
			' No se filtra por usuario,así que obtenemos todos los destinos
			sConsulta = "SELECT DEST.COD,DEST.SINTRANS, DEST.DEN_" & sIdi & " DEN, DEST.DIR, DEST.CP, DEST.POB, PROVID.DEN AS PROVI, PAID.DEN AS PAI " &
			   " FROM DEST WITH (NOLOCK) " &
			   " LEFT JOIN PAI WITH (NOLOCK) ON DEST.PAI=PAI.COD " &
			   " LEFT JOIN PAI_DEN PAID WITH (NOLOCK) ON PAID.PAI = PAI.COD AND PAID.IDIOMA = @IDIOMA " &
			   " LEFT JOIN PROVI WITH (NOLOCK) ON DEST.PAI=PROVI.PAI AND DEST.PROVI=PROVI.COD" &
			   " LEFT JOIN PROVI_DEN PROVID WITH (NOLOCK) ON PROVID.PROVI = PROVI.COD AND PROVID.PAI = PROVI.PAI AND PROVID.IDIOMA = @IDIOMA"
			adoParam = adoComm.Parameters.AddWithValue("@IDIOMA", sIdi)
		Else
			sConsulta = "SELECT DEST.COD,DEST.SINTRANS, DEST.DEN_" & sIdi & " DEN, DEST.DIR, DEST.CP, DEST.POB, PROVID.DEN AS PROVI, PAID.DEN AS PAI " &
			   " FROM DEST WITH (NOLOCK) " &
			   " INNER JOIN USU_DEST ON USU_DEST.DEST=DEST.COD" &
			   " LEFT JOIN PAI WITH (NOLOCK) ON DEST.PAI=PAI.COD " &
			   " LEFT JOIN PAI_DEN PAID WITH (NOLOCK) ON PAID.PAI = PAI.COD AND PAID.IDIOMA = @IDIOMA " &
			   " LEFT JOIN PROVI WITH (NOLOCK) ON DEST.PAI=PROVI.PAI AND DEST.PROVI=PROVI.COD" &
			   " LEFT JOIN PROVI_DEN PROVID WITH (NOLOCK) ON PROVID.PROVI = PROVI.COD AND PROVID.PAI = PROVI.PAI AND PROVID.IDIOMA = @IDIOMA" &
			   " WHERE USU_DEST.USU=@USUARIO"
			adoParam = adoComm.Parameters.AddWithValue("@IDIOMA", sIdi)
			adoParam = adoComm.Parameters.AddWithValue("@USUARIO", usu)
		End If
		sConsulta = sConsulta & " ORDER BY DEST.COD"
		adoComm.CommandText = sConsulta

		Dim da As New SqlDataAdapter
		da.SelectCommand = adoComm
		da.Fill(ador)

		If ador.Tables.Count = 0 Then
			ador.Clear()
			adoComm.Dispose()
			da.Dispose()
			Return ador
			Exit Function

		Else
			cn.Close()
			adoComm.Dispose()
			da.Dispose()
			Return ador
			ador.Clear()
		End If
	End Function
#End Region
#Region "UnidadPedido data access methods"
	''' <summary>
	''' Devuelve los datos de la unidad en el idioma especificado
	''' </summary>
	''' <param name="CodIdioma">Variable tipo Idioma con el código de idioma para las denominaciones</param>
	''' <param name="CodUnidad">Código de unidad</param>
	''' <returns>Un Dataset con los datos de la unidad</returns>
	''' <remarks>Llamada desde CUnidadesPedido</remarks>
	Public Function UnidadPedido_CargarDatosUnidad(ByVal CodIdioma As FSNLibrary.Idioma, ByVal CodUnidad As String) As DataSet
		Dim cn As New SqlConnection(mDBConnection)
		Using cn
			Dim sSql As String = "SELECT UNI.COD UP, UD.DEN AS DEN, NULL, NULL " &
								" FROM UNI WITH (NOLOCK)" &
								" LEFT JOIN UNI_DEN UD WITH (NOLOCK) ON UD.UNI = UNI.COD AND UD.IDIOMA = @IDIOMA " &
								" WHERE COD =@UNI"
			Dim cm As SqlCommand = New SqlCommand(sSql, cn)
			cm.CommandType = CommandType.Text
			Dim sqlPar As SqlParameter = New SqlParameter("@UNI", SqlDbType.VarChar, 20)
			sqlPar.Value = CodUnidad
			cm.Parameters.Add(sqlPar)
			sqlPar = New SqlParameter("@IDIOMA", SqlDbType.VarChar, 20)
			sqlPar.Value = CodIdioma.ToString
			cm.Parameters.Add(sqlPar)
			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			Dim ds As New DataSet
			da.Fill(ds)
			Return ds
		End Using
	End Function
	''' <summary>
	''' Devuelve los datos de la unidad en el idioma especificado
	''' </summary>
	''' <param name="CodIdioma">Variable tipo Idioma con el código de idioma para las denominaciones</param>
	''' <param name="CodUnidad">Código de unidad</param>
	''' <param name="Id">Identificador de la línea de catálogo o de CAT_LIN_MIN</param>
	''' <returns>Dataset con los datos de la unidad</returns>
	''' <remarks>Llamada desde CUnidadesPedido</remarks>
	Public Function UnidadPedido_CargarDatosUnidad(ByVal CodIdioma As FSNLibrary.Idioma, ByVal CodUnidad As String, ByVal Id As Integer) As DataSet
		Dim cn As New SqlConnection(mDBConnection)
		Using cn
			Dim cm As SqlCommand = New SqlCommand("SELECT UP, UNIPEDDEN.DEN AS DEN, FC, CANT_MIN, CATALOG_LIN.UC, UNICOMDEN.DEN AS DEN_UC, UNIPED.NUMDEC" &
												" FROM CAT_LIN_MIN WITH (NOLOCK) " &
												" INNER JOIN UNI AS UNIPED WITH (NOLOCK) ON UP=UNIPED.COD" &
												" LEFT JOIN UNI_DEN UNIPEDDEN WITH (NOLOCK) ON UNIPEDDEN.UNI = UNIPED.COD AND UNIPEDDEN.IDIOMA = @IDIOMA " &
												" INNER JOIN CATALOG_LIN WITH (NOLOCK) ON CATALOG_LIN.ID=CAT_LIN_MIN.LINEA" &
												" INNER JOIN UNI AS UNICOM WITH (NOLOCK) ON UC=UNICOM.COD" &
												" LEFT JOIN UNI_DEN UNICOMDEN WITH (NOLOCK) ON UNICOMDEN.UNI = UNICOM.COD AND UNICOMDEN.IDIOMA = @IDIOMA " &
												" WHERE LINEA=@ID AND UP=@UNI" &
												" UNION" &
												" SELECT UP_DEF AS UP, UNIDEN.DEN AS DEN, FC_DEF AS FC, CANT_MIN_DEF AS CANT_MIN, UC, UNIDEN2.DEN DEN_UC, UNI.NUMDEC" &
												" FROM CATALOG_LIN WITH (NOLOCK) " &
												" INNER JOIN UNI WITH (NOLOCK) ON CATALOG_LIN.UP_DEF=UNI.COD" &
												" LEFT JOIN UNI_DEN UNIDEN WITH (NOLOCK) ON UNIDEN.UNI = UNI.COD AND UNIDEN.IDIOMA = @IDIOMA " &
												" INNER JOIN UNI AS UNI2 WITH (NOLOCK) ON CATALOG_LIN.UC=UNI2.COD" &
												" LEFT JOIN UNI_DEN UNIDEN2 WITH (NOLOCK) ON UNIDEN2.UNI = UNI2.COD AND UNIDEN2.IDIOMA = @IDIOMA " &
												" WHERE CATALOG_LIN.ID=@ID AND UNI.COD=@UNI", cn)
			cm.CommandType = CommandType.Text
			Dim sqlPar As SqlParameter = New SqlParameter("@UNI", SqlDbType.VarChar, 20)
			sqlPar.Value = CodUnidad
			cm.Parameters.Add(sqlPar)
			sqlPar = New SqlParameter("@IDIOMA", SqlDbType.VarChar, 20)
			sqlPar.Value = CodIdioma.ToString
			cm.Parameters.Add(sqlPar)
			sqlPar = New SqlParameter("@ID", SqlDbType.Int)
			sqlPar.Value = Id
			cm.Parameters.Add(sqlPar)
			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			Dim ds As New DataSet
			da.Fill(ds)
			Return ds
		End Using
	End Function
#End Region
#Region " UnidadesPedido data access methods "
	''' <summary>
	''' Carga todas las unidades en el idioma seleccionado
	''' </summary>
	''' <param name="CodIdioma">código del idioma</param>
	''' <returns>Un dataset con una tabla con las unidades</returns>
	''' <remarks>
	''' Llamada desde: PedidoLibre.aspx
	''' Tiempo máximo: 0 sec.
	''' </remarks>
	Public Function UnidadesPedido_CargarTodasLasUnidades(ByVal CodIdioma As FSNLibrary.Idioma) As DataSet

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Dim dr As New DataSet

		Using cn
			If Not cn.State = ConnectionState.Open Then
				cn.Open()
			End If

			cm.Connection = cn

			cm.CommandText = "SELECT UNI.COD, UD.DEN AS DEN, COD + ' - ' + UD.DEN AS CODYDEN, NUMDEC " &
							" FROM UNI WITH (NOLOCK)" &
							" LEFT JOIN UNI_DEN UD WITH (NOLOCK) ON UD.UNI = UNI.COD AND UD.IDIOMA = @IDIOMA "

			Dim sqlPar As SqlParameter = New SqlParameter("@IDIOMA", SqlDbType.VarChar, 20)
			sqlPar.Value = CodIdioma.ToString
			cm.Parameters.Add(sqlPar)
			cm.CommandType = CommandType.Text
			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)

			Return dr
		End Using
	End Function
	''' <summary>
	''' Devuelve las unidades de pedido que se pueden aplicar a una línea de catálogo dada
	''' </summary>
	''' <param name="CodIdioma">Código de idioma</param>
	''' <param name="Linea">Id de la línea de catálogo</param>
	''' <returns>Devuelve un dataset con las unidades de pedido y su equivalencia con la unidad de compra</returns>
	''' <remarks>Llamada desde CUnidadesPedido</remarks>
	Public Function UnidadesPedido_DevolverUnidadesPedido(ByVal CodIdioma As FSNLibrary.Idioma, ByVal Linea As Integer) As DataSet
		Dim cn As New SqlConnection(mDBConnection)
		Using cn
			If Not cn.State = ConnectionState.Open Then cn.Open()
			Dim cm As New SqlCommand("SELECT CLM.UP COD, UD.DEN AS DEN, CLM.FC, CLM.CANT_MIN, CL.UC, UD2.DEN AS DEN_UC, U.NUMDEC" &
					" FROM CAT_LIN_MIN CLM WITH(NOLOCK) " &
					" INNER JOIN UNI U WITH (NOLOCK) ON UP=U.COD" &
					" LEFT JOIN UNI_DEN UD WITH (NOLOCK) ON UD.UNI = U.COD AND UD.IDIOMA = @IDIOMA " &
					" INNER JOIN (CATALOG_LIN CL " &
								"INNER JOIN UNI U2 WITH(NOLOCK) ON U2.COD = CL.UC" &
								" LEFT JOIN UNI_DEN UD2 WITH (NOLOCK) ON UD2.UNI = U2.COD AND UD2.IDIOMA = @IDIOMA " &
								") ON CLM.LINEA = CL.ID" &
					" WHERE LINEA =@ID" &
					" UNION" &
					" SELECT UP_DEF AS COD, UD.DEN AS DEN, FC_DEF AS FC, CANT_MIN_DEF AS CANT_MIN, UC, UNID2.DEN DEN_UC, U.NUMDEC" &
					" FROM CATALOG_LIN WITH(NOLOCK)" &
					" INNER JOIN UNI U WITH(NOLOCK) ON CATALOG_LIN.UP_DEF=U.COD " &
					" LEFT JOIN UNI_DEN UD WITH (NOLOCK) ON UD.UNI = U.COD AND UD.IDIOMA = @IDIOMA " &
					" INNER JOIN UNI AS UNI2 WITH(NOLOCK)  ON CATALOG_LIN.UC=UNI2.COD " &
					" LEFT JOIN UNI_DEN UNID2 WITH (NOLOCK) ON UNID2.UNI = UNI2.COD AND UNID2.IDIOMA = @IDIOMA " &
					" WHERE CATALOG_LIN.ID=@ID", cn)
			cm.CommandType = CommandType.Text
			Dim sqlPar As New SqlParameter("@ID", SqlDbType.Int)
			sqlPar.Value = Linea
			cm.Parameters.Add(sqlPar)
			sqlPar = New SqlParameter("@IDIOMA", SqlDbType.VarChar, 20)
			sqlPar.Value = CodIdioma.ToString
			cm.Parameters.Add(sqlPar)
			Dim da As New SqlDataAdapter
			Dim ds As New DataSet
			da.SelectCommand = cm
			da.Fill(ds)
			Return ds
		End Using
	End Function
#End Region
#Region " Destino data access methods "
	''' <summary>
	''' Función que devuelve un DataSet con los datos de los almacenes asociados a un destino
	''' </summary>
	''' <param name="CodDestino">Código de destino del que queremos conocer los almacenes</param>
	''' <returns>Un DataSet con los datos del almacen</returns>
	''' <remarks>
	''' Llamada desde: EPServer\cDestino.DevolverAlmacenesDestino
	''' Tiempo máximo: 0,3 seg</remarks>
	Public Function Destino_DevolverAlmacenesDestino(ByVal CodDestino As String) As DataSet
		Authenticate()
		Dim SqlCmd As New SqlCommand
		Dim sqlCn As New SqlConnection(mDBConnection)
		Dim SqlParam As New SqlParameter
		Dim Da As New SqlDataAdapter
		Dim DsResul As New DataSet
		sqlCn.Open()
		SqlCmd.Connection = sqlCn
		SqlCmd.CommandText = "FSEP_Devolver_Almacenes_Destino"
		SqlCmd.CommandType = CommandType.StoredProcedure
		SqlParam = SqlCmd.Parameters.AddWithValue("@CodDest", CodDestino)
		Da.SelectCommand = SqlCmd
		Da.Fill(DsResul)
		sqlCn.Close()
		Da.Dispose()
		SqlCmd.Dispose()
		Return DsResul
		DsResul.Clear()
	End Function
#End Region
#Region " TiposPedidos data access methods "
	''' <summary>
	''' Función que devuelve todos los tipos de pedido posibles para seleccionar
	''' </summary>
	''' <param name="Idioma">Idioma de la aplicación</param>
	''' <returns>Un objeto de la clase cTiposPedidos con todos los posibles tipos para seleccionar</returns>
	''' <remarks>
	''' Llamada desde: EPWEB\EmisionPedido.aspx.vb\DlCesta_ItemDataBound
	''' Tiempo máximo: 0,2 seg</remarks>
	Public Function TiposPedidos_CargarTiposPedido(ByVal Idioma As Idioma) As DataSet
		Authenticate()
		Dim SqlCon As New SqlConnection(mDBConnection)
		Dim SqlCmd As New SqlCommand
		Dim SqlParam As New SqlParameter
		Dim Da As New SqlDataAdapter
		Dim DsResul As New DataSet
		SqlCon.Open()
		SqlCmd.Connection = SqlCon
		SqlParam = SqlCmd.Parameters.AddWithValue("@IDIOMA", Idioma.ToString())
		SqlCmd.CommandText = "FSEP_DEVOLVER_TIPOS_PEDIDO"
		SqlCmd.CommandType = CommandType.StoredProcedure
		Da.SelectCommand = SqlCmd
		Da.Fill(DsResul)
		SqlCon.Close()
		Return DsResul
		DsResul.Clear()
		SqlCmd.Dispose()
		Da.Dispose()
	End Function
#End Region
#Region " TipoPedido data access methods "
	''' <summary>
	''' Devuelve los datos del tipo de pedido pasado su codigo e idioma
	''' </summary>
	''' <param name="Idioma">Idioma de la apliación</param>
	''' <param name="CodTipoPedido">Codigo del tipo de pedido del que se quieren saber los datos</param>
	''' <returns>Un DataSet con los datos del tipo de pedido del que se nos ha pasado el código</returns>
	''' <remarks>
	''' Llamada desde: EpServer\cTipoPedido\CargarTiposPedido
	''' Tiempo máximo: 0,25 seg</remarks>
	Public Function TipoPedido_CargarDatosTipoPedido(ByVal Idioma As String, ByVal CodTipoPedido As String) As DataSet
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Dim sqlCmd As New SqlCommand
		Dim SqlParam As New SqlParameter
		Dim da As New SqlDataAdapter
		Dim DsResul As New DataSet
		sqlCmd.CommandText = "FSEP_DEVOLVER_DATOS_TIPOPEDIDO"
		cn.Open()
		sqlCmd.Connection = cn
		SqlParam = sqlCmd.Parameters.AddWithValue("@IDIOMA", Idioma)
		SqlParam = sqlCmd.Parameters.AddWithValue("@COD", CodTipoPedido)
		sqlCmd.CommandType = CommandType.StoredProcedure
		da.SelectCommand = sqlCmd
		da.Fill(DsResul)
		cn.Close()
		Return DsResul
		da.Dispose()
		sqlCmd.Dispose()
		DsResul.Clear()
	End Function
#End Region
#Region "Facturas Data Access Methods"
	''' <summary>
	''' Función que devolverá un dataset con los estados de factura.
	''' </summary>
	''' <param name="idioma">Idioma en el que se quieren recuperar los datos en multiidioma</param>
	''' <returns>Los estados de factura existentes.</returns>
	''' <remarks>Llamado desde EspServer.CFacturas.CargarEstadosFra; Tiempo Máximo: 1 seg.</remarks>
	Public Function Facturas_DevolverEstadosFra(ByVal idioma As FSNLibrary.Idioma) As DataSet
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet

		Using cn
			cn.Open()

			cm = New SqlCommand
			cm.Connection = cn

			cm.CommandText = "FSEP_GET_ESTADOS_FRA"
			cm.CommandType = CommandType.StoredProcedure

			If Not IsNothing(idioma) And idioma.ToString <> "" Then
				cm.Parameters.AddWithValue("IDIOMA", idioma.ToString)
			Else
				cm.Parameters.AddWithValue("IDIOMA", "SPA")
			End If

			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)

			Return dr
		End Using
	End Function
#End Region
#Region "Pagos Data Access Methods"
	''' <summary>
	''' Función que devolverá un dataset con los estados de Pago.
	''' </summary>
	''' <param name="idioma">Idioma en el que se quieren recuperar los datos en multiidioma</param>
	''' <returns>Los estados de factura existentes.</returns>
	''' <remarks>Llamado desde EspServer.CPagos.CargarEstadosPago; Tiempo Máximo: 1 seg.</remarks>
	Public Function Pagos_DevolverEstadosPago(ByVal idioma As FSNLibrary.Idioma) As DataSet
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet

		Using cn
			cn.Open()

			cm = New SqlCommand
			cm.Connection = cn

			cm.CommandText = "FSEP_GET_ESTADOS_PAGO"
			cm.CommandType = CommandType.StoredProcedure

			If Not IsNothing(idioma) And idioma.ToString <> "" Then
				cm.Parameters.AddWithValue("IDIOMA", idioma.ToString)
			Else
				cm.Parameters.AddWithValue("IDIOMA", "SPA")
			End If

			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)

			Return dr
		End Using
	End Function
#End Region
#Region "Direcciones Envio Facturas"
	''' <summary>
	''' Función que devolverá un dataset con las direcciones de envío de facturas de todas las empresas
	''' </summary>
	''' <returns>Las direcciones de envío de facturas de la empresa dada</returns>
	''' <remarks>Llamado desde FSNServer.CEnvFacturaDirecciones.DevolverDireccionesEnvioFacturasDeUnaEmpresa; Tiempo Máximo: 0,5 seg.</remarks>
	Public Function DireccionesEnvioFacturas_Devolver() As DataSet
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet

		Using cn
			cn.Open()

			cm = New SqlCommand
			cm.Connection = cn

			cm.CommandText = "FSEP_GET_DIRECCIONES_ENVIO_FRAS"
			cm.CommandType = CommandType.StoredProcedure

			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)

			Return dr
		End Using
	End Function
#End Region
#Region " CategoriasN1 data access methods "
	''' <summary>
	''' Función que carga todas las categorias de una usuario dado su codigo de persona para un idioma tambien pasado como parámetro.Accede a Base de Datos
	''' </summary>
	''' <param name="Idioma">Idioma del usuario</param>
	''' <param name="Per">Código de la persona para cargar todas las categorias de la misma.</param>
	''' <returns>Un DataSet con todos los datos de las categorias para la persona pasada como parámetro y en el idioma dado.</returns>
	''' <remarks></remarks>
	Public Function CategoriasN1_CargarTodasLasCategorias(ByVal Per As String, ByVal Idioma As String) As DataSet
		Dim ador As New DataSet
		Dim adoCom As New SqlCommand
		Dim oParam As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		'Ejecuta el store procedure FSEP_CARGAR_CATEGORIAS
		cn.Open()
		adoCom.Connection = cn
		adoCom.CommandText = "FSEP_CARGAR_CATEGORIAS"
		adoCom.CommandType = CommandType.StoredProcedure

		oParam = adoCom.Parameters.AddWithValue("@PER", Per)
		oParam = adoCom.Parameters.AddWithValue("@NIVEL", 1)
		oParam = adoCom.Parameters.AddWithValue("@CATN1", 0)
		oParam = adoCom.Parameters.AddWithValue("@CATN2", 0)
		oParam = adoCom.Parameters.AddWithValue("@CATN3", 0)
		oParam = adoCom.Parameters.AddWithValue("@CATN4", 0)
		oParam = adoCom.Parameters.AddWithValue("@CATN5", 0)
		If Idioma <> "" Then
			oParam = adoCom.Parameters.AddWithValue("@IDI", Idioma)
		Else
			oParam = adoCom.Parameters.AddWithValue("@IDI", System.DBNull.Value)
		End If
		adoCom.Prepare()

		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(ador)
		'Cierra los objetos
		adoCom = Nothing
		da = Nothing
		cn.Close()
		oParam = Nothing
		Return ador
		ador = Nothing
	End Function
#End Region
#Region " Articulo data access methods "
	''' <summary>
	''' Funcion que añade un artículo a la cesta de la compra de un usuario dado, se accede a la base de datos.
	''' </summary>
	''' <param name="sPer">Código de la persona a la que añadir el artículo a la cesta</param>
	''' <param name="mdfc">Factor de conversión de la unidad de pedido</param>
	''' <param name="msprove">Código del proveedor</param>
	''' <param name="mlid">Id de la línea de catálogo</param>
	''' <param name="mlCat1">Código de categoría de nivel 1</param>
	''' <param name="mlCat2">Código de categoría de nivel 2</param>
	''' <param name="mlCat3">Código de categoría de nivel 3</param>
	''' <param name="mlCat4">Código de categoría de nivel 4</param>
	''' <param name="mlCat5">Código de categoría de nivel 5</param>
	''' <param name="msDen">Denominación del artículo</param>
	''' <param name="mdCantidad">Cantidad</param>
	''' <param name="msUP">Unidad de pedido</param>
	''' <param name="mdPrecioUc">Precio Unidad de compra</param>
	''' <param name="miTipo">Tipo</param>
	''' <param name="Mon">Código de moneda</param>
	Public Function Articulo_AnyadirACesta(ByVal sPer As String, ByVal mdfc As Double, ByVal msprove As String, ByVal mlid As Integer, ByVal mlCat1 As Integer, ByVal mlCat2 As Integer, ByVal mlCat3 As Integer, ByVal mlCat4 As Integer, ByVal mlCat5 As Integer, ByVal msDen As String, ByVal mdCantidad As Double, ByVal msUP As String, ByVal mdPrecioUc As Double, ByVal miTipo As Short, ByVal Mon As String, ByVal iCat As Integer, ByVal iNivel As Integer, ByVal iTipoRececion As Integer, Optional ByVal iNuevo As Integer = 0) As Integer
		Authenticate()

		Dim cm As New SqlCommand
		Dim cn As New SqlConnection(mDBConnection)
		Dim idCabecera As Integer
		Dim adoRs As New DataSet
		Dim adoRsPre As New DataSet
		Dim da As New SqlDataAdapter
		Dim dr As SqlDataReader
		Dim bEjecutarInsert As Boolean = False
		Using cn

			cn.Open()
			cm = New SqlCommand
			cm.Connection = cn

			If iNuevo = 0 Then

				cm.CommandText = " SELECT MAX(ID) EXISTE FROM CESTA_CABECERA WITH(NOLOCK) WHERE PER=@PER AND PROVE=@PROVE AND MON=@MON AND CAT = @CAT AND NIVEL = @NIVEL"
				cm.CommandType = CommandType.Text
				cm.Parameters.Add("@PER", SqlDbType.VarChar, 20).Value = sPer
				cm.Parameters.Add("@PROVE", SqlDbType.VarChar, 50).Value = msprove
				cm.Parameters.Add("@MON", SqlDbType.VarChar, 20).Value = Mon
				cm.Parameters.Add("@CAT", SqlDbType.Int).Value = iCat
				cm.Parameters.Add("@NIVEL", SqlDbType.Int).Value = iNivel
				dr = cm.ExecuteReader()
				If Not dr.Read() Then
					bEjecutarInsert = True
					dr.Close()
				Else
					If DBNullToLong(dr.Item("EXISTE")) > 0 Then
						idCabecera = dr.Item("EXISTE")
						dr.Close()
					Else
						dr.Close()
						If iCat = 0 Then
							bEjecutarInsert = True
							'Cuando los artículos para esa categoría no tienen atributos de cabecera (ATRIB_CABECERA=NULL en CATNX) les pone CAT=0, NIVEL=0 por lo que 
							'si fuera el caso habría salido que la primera comprobación es correcta e insertado en esa cesta
						Else
							cm = New SqlCommand
							cm.CommandText = " SELECT ID EXISTE,CAT,NIVEL FROM CESTA_CABECERA WITH(NOLOCK) WHERE PER=@PER AND PROVE=@PROVE AND MON=@MON ORDER BY ID DESC"
							cm.CommandType = CommandType.Text
							cm.Parameters.Add("@PER", SqlDbType.VarChar, 20).Value = sPer
							cm.Parameters.Add("@PROVE", SqlDbType.VarChar, 50).Value = msprove
							cm.Parameters.Add("@MON", SqlDbType.VarChar, 20).Value = Mon
							cm.Connection = cn
							dr = cm.ExecuteReader()

							If Not dr.Read() Then
								bEjecutarInsert = True
								dr.Close()
							Else
								If DBNullToLong(dr.Item("EXISTE")) = 0 Then
									bEjecutarInsert = True
									dr.Close()
								Else
									bEjecutarInsert = False

									'De momento parece q va a ir a esta cesta
									idCabecera = dr.Item("EXISTE")
									Dim idCabeceraCat As Long = dr.Item("CAT")
									Dim idCabeceraNivel As Long = dr.Item("NIVEL")
									dr.Close()

									'Si una categoría no tiene atributos de cabecera se puede incluir en un pedido con atributos de cabecera de otras categorías.
									'Si no habrá que comprobar los atributos
									If idCabeceraCat > 0 Then
										'nuevos atribs Cabecera por nuevo nivel+cat
										cm = New SqlCommand
										cm.Connection = cn
										cm.CommandText = "SELECT DISTINCT CTN.ATRIB FROM CATN" & iCat & "_ATRIB CTN WITH(NOLOCK) WHERE CTN.CATN" & iCat & "_ID =" & iNivel & " AND CTN.AMBITO = 0 AND CTN.TIPO = 1"
										cm.CommandType = CommandType.Text
										da.SelectCommand = cm
										da.Fill(adoRsPre)

										'Existentes atribs Cabecera por cesta (per+prove+mon) ahora solo falta q coincidan EXACTAMENTE los atribs
										cm = New SqlCommand
										cm.Connection = cn
										cm.CommandText = "SELECT DISTINCT CTN.ATRIB FROM CATN" & idCabeceraCat & "_ATRIB CTN WITH(NOLOCK) WHERE CTN.CATN" & idCabeceraCat & "_ID =" & idCabeceraNivel & " AND CTN.AMBITO = 0 AND CTN.TIPO = 1"
										cm.CommandType = CommandType.Text
										da.SelectCommand = cm
										da.Fill(adoRs)

										If adoRs.Tables(0).Rows.Count = 0 Then
											If adoRsPre.Tables(0).Rows.Count > 0 Then
												bEjecutarInsert = True
											End If
										Else
											If adoRsPre.Tables(0).Rows.Count = 0 Then
												bEjecutarInsert = True
											Else
												'Para que vaya al mismo pedido han de coincidir todos los atributos de cabecera, si no se crearán diferentes pedidos 
												Dim bEncontrado As Boolean = False
												For Each row As DataRow In adoRs.Tables(0).Rows
													'Inicializamos la variable
													bEncontrado = False
													For Each rowPre As DataRow In adoRsPre.Tables(0).Rows
														If row("ATRIB") = rowPre("ATRIB") Then
															bEncontrado = True
															Exit For
														End If
													Next
													If Not bEncontrado Then
														bEjecutarInsert = True
														Exit For
													End If
												Next
												If bEncontrado Then
													For Each rowPre As DataRow In adoRsPre.Tables(0).Rows
														'Inicializamos la variable
														bEncontrado = False
														For Each row As DataRow In adoRs.Tables(0).Rows
															If row("ATRIB") = rowPre("ATRIB") Then
																bEncontrado = True
																Exit For
															End If
														Next
														If Not bEncontrado Then
															bEjecutarInsert = True
															Exit For
														End If
													Next
												End If
											End If
										End If
									End If
								End If
							End If
						End If
					End If
				End If
			End If

			cm = New SqlCommand
			cm.Connection = cn
			cm.CommandType = CommandType.Text
			cm.Parameters.Add("@PER", SqlDbType.VarChar, 20).Value = sPer
			cm.Parameters.Add("@PROVE", SqlDbType.VarChar, 50).Value = msprove
			cm.Parameters.Add("@MON", SqlDbType.VarChar, 20).Value = Mon
			cm.Parameters.Add("@CAT", SqlDbType.Int).Value = iCat
			cm.Parameters.Add("@NIVEL", SqlDbType.Int).Value = iNivel
			If iNuevo = 0 Then
				If Not bEjecutarInsert Then
					'Ya tienes idCabecera
				Else
					cm.CommandText = " INSERT INTO CESTA_CABECERA (PER, PROVE, MON, CAT, NIVEL) VALUES (@PER, @PROVE, @MON, @CAT, @NIVEL)"
					cm.ExecuteNonQuery()

					adoRs = DevolverIdCestaCabecera(sPer, msprove, Mon, iCat, iNivel)
					If Not adoRs.Tables(0).Rows.Count = 0 Then
						If Not IsDBNull(adoRs.Tables(0).Rows(0).Item("ID")) Then
							idCabecera = adoRs.Tables(0).Rows(0).Item("ID")
						End If
					End If
				End If
			Else
				cm.CommandText = " INSERT INTO CESTA_CABECERA (PER, PROVE, MON, CAT, NIVEL) VALUES (@PER, @PROVE, @MON, @CAT, @NIVEL) SELECT @ORDEN=SCOPE_IDENTITY()"

				cm.Parameters.Add(New SqlParameter("@ORDEN", SqlDbType.Int))
				cm.Parameters("@ORDEN").Direction = ParameterDirection.Output

				cm.ExecuteNonQuery()
				'iResultado = 1
				idCabecera = DBNullToInteger(cm.Parameters("@ORDEN").Value)
			End If

			cm = New SqlCommand
			cm.Connection = cn
			cm.CommandText = "INSERT INTO CESTA (PER, PROVE, LINEA, CAT1, CAT2, CAT3, CAT4, CAT5, DESCR,CANT, UP, PREC,TIPO,FC, MON, CESTACABECERAID, TIPORECEPCION, IMPORTE_PED)" &
			" VALUES (@PER, @PROVE, @LINEA, @CAT1, @CAT2, @CAT3, @CAT4, @CAT5, @DESCR, @CANT, @UP, @PREC, @TIPO, @FC, @MON, @CESTACABECERAID, @TIPORECEPCION, @IMPORTE_PED)"
			If IsNothing(mdfc) Then
				mdfc = 1
			End If
			cm.Parameters.Add("@PER", SqlDbType.VarChar, 20).Value = sPer
			cm.Parameters.Add("@PROVE", SqlDbType.VarChar, 50).Value = msprove
			cm.Parameters.Add("@MON", SqlDbType.VarChar, 20).Value = Mon
			cm.Parameters.Add("@LINEA", SqlDbType.Int)
			cm.Parameters.Add("@CAT1", SqlDbType.Int)
			cm.Parameters.Add("@CAT2", SqlDbType.Int)
			cm.Parameters.Add("@CAT3", SqlDbType.Int)
			cm.Parameters.Add("@CAT4", SqlDbType.Int)
			cm.Parameters.Add("@CAT5", SqlDbType.Int)
			cm.Parameters.Add("@DESCR", SqlDbType.NVarChar, 200)
			cm.Parameters.Add("@CANT", SqlDbType.Float)
			cm.Parameters.Add("@UP", SqlDbType.VarChar, 20)
			cm.Parameters.Add("@PREC", SqlDbType.Float)
			cm.Parameters.Add("@TIPO", SqlDbType.Int)
			cm.Parameters.Add("@FC", SqlDbType.Float)
			cm.Parameters.Add("@CESTACABECERAID", SqlDbType.Int)
			cm.Parameters.Add("@TIPORECEPCION", SqlDbType.Int)
			cm.Parameters.Add("@IMPORTE_PED", SqlDbType.Float)
			cm.Parameters("@LINEA").Value = mlid
			cm.Parameters("@CAT1").Value = IIf(mlCat1 > 0, mlCat1, DBNull.Value)
			cm.Parameters("@CAT2").Value = IIf(mlCat2 > 0, mlCat2, DBNull.Value)
			cm.Parameters("@CAT3").Value = IIf(mlCat3 > 0, mlCat3, DBNull.Value)
			cm.Parameters("@CAT4").Value = IIf(mlCat4 > 0, mlCat4, DBNull.Value)
			cm.Parameters("@CAT5").Value = IIf(mlCat5 > 0, mlCat5, DBNull.Value)
			cm.Parameters("@DESCR").Value = msDen
			cm.Parameters("@UP").Value = msUP
			cm.Parameters("@TIPO").Value = miTipo
			cm.Parameters("@FC").Value = mdfc
			cm.Parameters("@CESTACABECERAID").Value = idCabecera
			cm.Parameters("@TIPORECEPCION").Value = iTipoRececion
			If (iTipoRececion = 0) Then
				cm.Parameters("@CANT").Value = mdCantidad
				cm.Parameters("@PREC").Value = mdPrecioUc
				cm.Parameters("@IMPORTE_PED").Value = DBNull.Value
			Else
				cm.Parameters("@CANT").Value = DBNull.Value
				cm.Parameters("@PREC").Value = DBNull.Value
				cm.Parameters("@IMPORTE_PED").Value = mdPrecioUc
			End If
			cm.ExecuteNonQuery()
			cn.Close()
			Return idCabecera
		End Using
	End Function

	Public Function ComprobarAtributoCabecera(ByVal iCat As Integer, ByVal iNivel As Integer) As DataSet
		Dim adoCom As SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom = New SqlCommand
		cn.Open()
		adoCom.Connection = cn

		adoCom.CommandText = "FSEP_DEVOLVER_ATRIB_CABECERA"
		adoCom.CommandType = CommandType.StoredProcedure

		adoPar = adoCom.Parameters.AddWithValue("@CAT", iCat)
		adoPar = adoCom.Parameters.AddWithValue("@NIVEL", iNivel)
		Dim DA As New SqlDataAdapter
		DA.SelectCommand = adoCom
		DA.Fill(adoRS)
		'Cierra los objetos
		cn.Close()
		adoCom = Nothing
		DA = Nothing
		adoPar = Nothing
		Return adoRS
		adoRS = Nothing
	End Function

	Public Function DevolverIdCestaCabecera(ByVal msPer As String, ByVal msprove As String, ByVal msmoneda As String, ByVal iCat As Integer, ByVal iNivel As Integer) As DataSet
		Dim adoCom As SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom = New SqlCommand
		cn.Open()
		adoCom.Connection = cn

		adoCom.CommandText = "FSEP_DEVOLVER_ID_CESTA_CABECERA"
		adoCom.CommandType = CommandType.StoredProcedure

		adoPar = adoCom.Parameters.AddWithValue("@PER", msPer)
		adoPar = adoCom.Parameters.AddWithValue("@PROVE", msprove)
		adoPar = adoCom.Parameters.AddWithValue("@MON", msmoneda)
		adoPar = adoCom.Parameters.AddWithValue("@CAT", iCat)
		adoPar = adoCom.Parameters.AddWithValue("@NIVEL", iNivel)
		Dim DA As New SqlDataAdapter
		DA.SelectCommand = adoCom
		DA.Fill(adoRS)
		'Cierra los objetos
		cn.Close()
		adoCom = Nothing
		DA = Nothing
		adoPar = Nothing
		Return adoRS
		adoRS = Nothing
	End Function


	''' <summary>
	''' Carga los datos necesarios del artículo favorito, para poder mostarlos en la aplicación de la que se llama, accede a la Base de Datos.
	''' </summary>
	''' <param name="sIdioma">Idioma utilizado para la muestra de datos del artículo.</param>
	''' <param name="sPer">Persona a la que esta asignada el artículo favorito.</param>
	''' <remarks></remarks>
	Public Function Articulo_CargarDatosArticuloFavorito(ByVal sPer As String, ByVal sIdioma As String, ByVal Id As Integer) As DataSet
		Dim adoCom As SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom = New SqlCommand
		cn.Open()
		adoCom.Connection = cn

		adoCom.CommandText = "FSEP_DEVOLVER_ARTICULO_FAVORITO"
		adoCom.CommandType = CommandType.StoredProcedure

		adoPar = adoCom.Parameters.AddWithValue("@ID", Id)
		adoPar = adoCom.Parameters.AddWithValue("@PER", sPer)
		adoPar = adoCom.Parameters.AddWithValue("@IDI", sIdioma)
		Dim DA As New SqlDataAdapter
		DA.SelectCommand = adoCom
		DA.Fill(adoRS)
		'Cierra los objetos
		cn.Close()
		adoCom = Nothing
		DA = Nothing
		adoPar = Nothing
		Return adoRS
		adoRS = Nothing
	End Function
	''' <summary>
	''' Función qarga las especificaciones del artículo dado su id, accede a la Base de Datos.
	''' </summary>
	''' <param name="Id">Identificador del artículo para el que queremos cargar las especificaciones.</param>
	''' <returns>Un DataSet con las especificaciones del artículo del que teniamos su Identificador apsado por parámetro.</returns>
	''' <remarks></remarks>
	Public Function Articulo_CargarEspecificaciones(ByVal Id As Integer) As DataSet
		Dim adoCom As SqlCommand
		Dim adoRes As New DataSet
		Dim oParam As SqlParameter
		Dim sConsulta As String
		Dim cn As New SqlConnection(mDBConnection)

		adoCom = New SqlCommand
		cn.Open()
		adoCom.Connection = cn
		sConsulta = "SELECT ID, NOM , COM, DATASIZE, FECACT FROM CATALOG_LIN_ESP WITH(NOLOCK) WHERE LINEA=@ID"

		oParam = adoCom.Parameters.AddWithValue("@ID", Id)
		adoCom.CommandText = sConsulta
		adoCom.CommandType = CommandType.Text
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRes)
		'Cierra los objetos
		adoCom = Nothing
		da = Nothing
		oParam = Nothing
		cn.Close()
		Return adoRes
		adoRes.Clear()
		adoRes = Nothing
	End Function
	Public Sub Articulo_Agregar_Cesta_PedidoAbierto(ByVal PerCod As String, ByVal Orden_PedidoAbierto As Integer, ByVal IdLineaPedido As Integer,
												ByVal Cantidad As Double, ByVal Importe As Double)
		'select de si existe cesta para pedido abierto en este usuario y pedido abierto
		'insert a cesta la linea
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand()
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_AGREGAR_CESTA_PEDIDOABIERTO"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@PER", PerCod)
			cm.Parameters.AddWithValue("@ORDEN_PED_ABIERTO", Orden_PedidoAbierto)
			cm.Parameters.AddWithValue("@ID_LINEA_PEDIDO", IdLineaPedido)
			cm.Parameters.AddWithValue("@CANT_PED", Cantidad)
			cm.Parameters.AddWithValue("@IMPORTE_PED", Importe)

			cm.ExecuteNonQuery()
		Catch e As Exception
			Throw e
		Finally
			cn.Dispose()
			cm.Dispose()
		End Try
	End Sub
#End Region
#Region " Articulos data access methods "
	''' <summary>
	''' Función que devuelve un DataSet con todos los artículos del catalogo del usuario pasado como parámetro filtrados por los parámetros también pasados.
	''' </summary>
	''' <param name="ID">Identificador del artículo en caso de que se quiera filtrar el resultado por un sólo artículo.</param>
	''' <param name="Cod">Código del artículo en caso de que se quiera filtrar el resultado por el código.</param>
	''' <param name="Den">Denominacion del artículo en caso de que se quieran filtrar los resultados por los que cumplan esta condición de denominación.</param>
	''' <param name="Prove">Parámetro que filtra los resultados que cumplan la condición de que pertenezcan al proveedor pasado por este parámetro.</param>
	''' <param name="sCat1">Parámetro que filtra los resultados que cumplan la condición de que pertenezcan a las categorias de nivel 1 pasada por este parámetro, este parámetro viene en forma de lista separado por comas.</param>
	''' <param name="scat2">Parámetro que filtra los resultados que cumplan la condición de que pertenezcan a las categorias de nivel 2 pasada por este parámetro, este parámetro viene en forma de lista separado por comas.</param>
	''' <param name="sCat3">Parámetro que filtra los resultados que cumplan la condición de que pertenezcan a las categorias de nivel 3 pasada por este parámetro, este parámetro viene en forma de lista separado por comas.</param>
	''' <param name="sCat4">Parámetro que filtra los resultados que cumplan la condición de que pertenezcan a las categorias de nivel 4 pasada por este parámetro, este parámetro viene en forma de lista separado por comas.</param>
	''' <param name="sCat5">Parámetro que filtra los resultados que cumplan la condición de que pertenezcan a las categorias de nivel 5 pasada por este parámetro, este parámetro viene en forma de lista separado por comas.</param>
	''' <param name="PrecioMax">Parámetro que filtra los resultados que cumplan la condición de que su precio máximo sea el valor del mismo.</param>
	''' <param name="Per">Parámetro que filtra la busqueda de los artículos del catálogo asignados a la persona con este valor.</param>
	''' <param name="CoincidenciaTotal">Parámetro que sirve para decidir si queremos que la coincidencia de la busqueda por la denominación y código de artículo coincida totalmente con el valor que esta en la BD.</param>
	''' <param name="sOrder">Parámetro que se utiliza para la ordenación de los artículos, siendo las posibilidades las siguientes: "cod":Código del artículo,"Precio":Precio del artículo,"Descripcion":Descripción del artículo(alfábeticamente se entiende),"Proveedor":Proveedor del artículo, también alfabeticamente,"Uso":Número de veces que se ha adquirido ese artículo</param>
	''' <param name="sCodExt">Parámetro que filtra los resultados que cumplan la condición de que contengan entre su Código externo el valor pasado por este parámetro.</param>
	''' <param name="sDest">Parámetro que filtra los resultados que cumplan la condición de que el destino a enviar los artículos sea el pasado por este parámetro.</param>
	''' <param name="sDirec">Parámetro que sirve para determinar el sentido de ordenación de los resultados, si debe ser ascendente o descendente</param>
	''' <returns>Un dataset con los artículos que cumplen los criterios de busqueda,ordenacion y sentido de la misma, pasados por los parámetros de la función.</returns>
	''' <remarks></remarks>
	Public Function Articulos_DevolverArticulosCatalogo(ByVal ID As Integer, ByVal Cod As Object, ByVal Den As Object, ByVal Prove As Object, ByVal sCat1 As Object, ByVal scat2 As Object, ByVal sCat3 As Object, ByVal sCat4 As Object, ByVal sCat5 As Object, ByVal PrecioMax As Object, ByVal Per As Object, ByVal CoincidenciaTotal As Short, ByVal sDest As String, ByVal sOrder As String, ByVal sDirec As String, ByVal sCodExt As String, Optional ByVal sIdEmp As String = "") As DataSet
		Authenticate()
		Dim ador As New DataSet
		Dim adoCom As SqlCommand
		Dim oParam As SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		Dim sConsulta As String
		'Ejecuta el store procedure FSEP_DEVOLVER_ARTICULOS_CATALOGO
		adoCom = New SqlCommand
		cn.Open()
		adoCom.Connection = cn
		sConsulta = "FSEP_DEVOLVER_ARTICULOS_CATALOGO" '  @COD = ?, @DEN = ?, @PROVE = ?, " & "   @CATN1 = ?, @CATN2 = ?, @CATN3 = ?, @CATN4 = ?,@CATN5 = ?, @ID=?, " & "   @PRECIOMAX =?, @PER =?, @COINCIDENCIA =?"

		adoCom.CommandType = CommandType.StoredProcedure
		oParam = adoCom.Parameters.AddWithValue("@COD", Cod)
		oParam = adoCom.Parameters.AddWithValue("@DEN", Den)
		If Not IsDBNull(Prove) AndAlso Prove <> "" AndAlso Not Prove Is Nothing Then
			oParam = adoCom.Parameters.AddWithValue("@PROVE", Prove)
		Else
			oParam = adoCom.Parameters.AddWithValue("@PROVE", System.DBNull.Value)
		End If
		oParam = adoCom.Parameters.AddWithValue("@CATN1", sCat1)
		oParam = adoCom.Parameters.AddWithValue("@CATN2", scat2)
		oParam = adoCom.Parameters.AddWithValue("@CATN3", sCat3)
		oParam = adoCom.Parameters.AddWithValue("@CATN4", sCat4)
		oParam = adoCom.Parameters.AddWithValue("@CATN5", sCat5)
		oParam = adoCom.Parameters.AddWithValue("@ID", ID)
		If Not IsDBNull(PrecioMax) AndAlso PrecioMax <> "" Then
			oParam = adoCom.Parameters.AddWithValue("@PRECIOMAX", PrecioMax)
		Else
			oParam = adoCom.Parameters.AddWithValue("@PRECIOMAX", System.DBNull.Value)
		End If
		oParam = adoCom.Parameters.AddWithValue("@PER", Per)
		oParam = adoCom.Parameters.AddWithValue("@COINCIDENCIA", CoincidenciaTotal)
		If sDest Is Nothing Then sDest = ""
		If sDest <> "" Then
			oParam = adoCom.Parameters.AddWithValue("@DEST", sDest)
		End If

		If sOrder <> "" Then
			oParam = adoCom.Parameters.AddWithValue("@ORDER", sOrder)
		End If
		If sDirec <> "" Then
			oParam = adoCom.Parameters.AddWithValue("@DIREC", sDirec)
		End If
		If sCodExt <> "" Then
			oParam = adoCom.Parameters.AddWithValue("@CODEXT", sCodExt)
		End If
		If sIdEmp <> "" Then
			oParam = adoCom.Parameters.AddWithValue("@IDEMP", sIdEmp)
		End If

		adoCom.CommandText = sConsulta
		adoCom.CommandTimeout = 360
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(ador)
		'Dim myReader As SqlDataReader = adoCom.ExecuteReader()
		'Dim arrVacio() As String
		'ador.Load(myReader, LoadOption.PreserveChanges, arrVacio)
		If ador.Tables.Count = 0 Then
			ador.Clear()
			ador = Nothing
			cn.Close()
			Articulos_DevolverArticulosCatalogo = Nothing
			Exit Function
		End If
		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return ador
		ador = Nothing
	End Function
	''' <summary>
	''' Funcion que carga los datos de un artículo dado su Identificador
	''' </summary>
	''' <param name="lId">Identificador del arículo que se quiere cargar</param>
	''' <returns>Un objeto de la clase artículo con todos los datos necesarios para ser mostrados en la aplicación.</returns>
	''' <remarks></remarks>
	Public Function Articulos_CargarArticulo(ByVal lId As Integer) As DataSet
		Dim sConsulta As String
		Dim adoCom As New SqlCommand
		Dim adoRes As New DataSet
		Dim oParam As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		cn.Open()
		adoCom.Connection = cn
		sConsulta = "FSEP_DEVOLVER_ARTICULO" ' @ID = ?"

		oParam = adoCom.Parameters.AddWithValue("@ID", lId)

		adoCom.CommandText = sConsulta
		adoCom.CommandType = CommandType.StoredProcedure

		Dim DA As New SqlDataAdapter
		DA.SelectCommand = adoCom
		DA.Fill(adoRes)
		adoCom = Nothing
		DA = Nothing
		cn.Close()
		Return adoRes
		adoRes.Clear()
	End Function
	''' <summary>
	''' Devuelve un dataset con los atributos para el artículo y proveedor seleccionado
	''' </summary>
	''' <param name="Art">Codigo del artículo</param>
	''' <param name="Prove">Código del proveedor</param>
	''' <returns>El dataset con los atributos para el artículo y proveedor seleccionado</returns>
	''' <remarks>
	''' Llamada desde DevolverAtributosArt de cArticulos
	''' Tiempo máximo: 0 sec.
	''' </remarks>
	Public Function Articulos_DevolverAtributosArt(ByVal Art As String, ByVal Prove As String) As DataSet
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet

		Using cn
			cn.Open()

			cm = New SqlCommand
			cm.Connection = cn

			cm.CommandText = "SELECT PAA.PROVE, PAA.ART, PAA.ATRIB AS ID, PAA.VALOR_NUM,PAA.VALOR_FEC,ART4.GMN1,ART4.GMN2,ART4.GMN3,ART4.GMN4,D.DEN,D.TIPO, CASE WHEN D.TIPO = 1 THEN PAA.VALOR_TEXT ELSE CASE WHEN D.TIPO = 2 THEN CONVERT(VARCHAR,PAA.VALOR_NUM) ELSE CASE WHEN D.TIPO = 3 THEN CONVERT(VARCHAR,PAA.VALOR_FEC) ELSE CONVERT(VARCHAR,PAA.VALOR_BOOL) END END END AS VALOR " &
			 " FROM PROVE_ART4_ATRIB PAA WITH(NOLOCK)" &
			 " INNER JOIN ART4 WITH(NOLOCK) ON ART4.COD=PAA.ART" &
			 " INNER JOIN ART4_ATRIB AA WITH(NOLOCK) ON PAA.ART=AA.ART AND PAA.ATRIB=AA.ATRIB" &
			 " LEFT JOIN DEF_ATRIB D WITH(NOLOCK) ON PAA.ATRIB=D.ID" &
			 " WHERE PAA.PROVE=@PROVE" &
			 " AND PAA.ART=@ART"
			cm.CommandType = CommandType.Text
			If Not Art Is Nothing AndAlso Not Prove Is Nothing Then
				Dim sqlPar As SqlParameter = New SqlParameter("@PROVE", SqlDbType.VarChar)
				sqlPar.Value = Prove
				cm.Parameters.Add(sqlPar)
				sqlPar = New SqlParameter("@ART", SqlDbType.VarChar)
				sqlPar.Value = Art
				cm.Parameters.Add(sqlPar)
				Dim da As New SqlDataAdapter
				da.SelectCommand = cm
				da.Fill(dr)

				Return dr
			Else
				Return Nothing
			End If
		End Using
	End Function

	''' <summary>
	''' Devuelve un dataset con los atributos para el artículo y proveedor seleccionado
	''' </summary>
	''' <param name="Art">Codigo del artículo</param>
	''' <param name="Prove">Código del proveedor</param>
	''' <returns>El dataset con los atributos para el artículo y proveedor seleccionado</returns>
	''' <remarks>
	''' Llamada desde DevolverAtributosArt de cArticulos
	''' Tiempo máximo: 0 sec.
	''' </remarks>
	Public Function Articulos_DevolverAtributosEspecificacionArt(ByVal Art As String, ByVal Prove As String) As DataSet
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet

		Using cn
			cn.Open()

			cm = New SqlCommand
			cm.Connection = cn

			cm.CommandText = "SELECT CATALOG_LIN.PROVE, CATALOG_LIN.ART_INT AS ART,CATALOG_LIN.ID, DEF_ATRIB.VALOR_NUM,CATALOG_LIN_ATRIB.VALOR_FEC,ART4.GMN1,ART4.GMN2,ART4.GMN3,ART4.GMN4,DEF_ATRIB.DEN,DEF_ATRIB.TIPO,  " &
			 " CASE WHEN DEF_ATRIB.TIPO = 1 THEN CATALOG_LIN_ATRIB.VALOR_TEXT  " &
			 " ELSE CASE WHEN DEF_ATRIB.TIPO = 2 THEN CONVERT(VARCHAR,CATALOG_LIN_ATRIB.VALOR_NUM)  " &
			 " ELSE CASE WHEN DEF_ATRIB.TIPO = 3 THEN CONVERT(VARCHAR,CATALOG_LIN_ATRIB.VALOR_FEC)  " &
			 " ELSE CONVERT(VARCHAR,DEF_ATRIB.VALOR_BOOL) END END END AS VALOR   " &
			 " FROM CATALOG_LIN WITH(NOLOCK)  " &
			 " INNER JOIN CATALOG_LIN_ATRIB WITH(NOLOCK) ON CATALOG_LIN.ID = CATALOG_LIN_ATRIB.CATALOG_LIN_ID  " &
			 " LEFT JOIN DEF_ATRIB WITH(NOLOCK) ON CATALOG_LIN_ATRIB.ATRIB=DEF_ATRIB.ID  " &
			 " LEFT JOIN ART4 ON CATALOG_LIN.ART_INT = ART4.COD WHERE CATALOG_LIN_ATRIB.TIPO=0  " &
			 " AND CATALOG_LIN.PROVE=@PROVE " &
			 " AND CATALOG_LIN.ART_INT=@ART "
			cm.CommandType = CommandType.Text
			If Not Art Is Nothing AndAlso Not Prove Is Nothing Then
				Dim sqlPar As SqlParameter = New SqlParameter("@PROVE", SqlDbType.VarChar)
				sqlPar.Value = Prove
				cm.Parameters.Add(sqlPar)
				sqlPar = New SqlParameter("@ART", SqlDbType.VarChar)
				sqlPar.Value = Art
				cm.Parameters.Add(sqlPar)
				Dim da As New SqlDataAdapter
				da.SelectCommand = cm
				da.Fill(dr)

				Return dr
			Else
				Return Nothing
			End If
		End Using
	End Function
	''' <summary>
	''' Función que devuelve la imagen del artículo requerido
	''' </summary>
	''' <param name="Art">Código del artículo</param>
	''' <param name="Prove">Código del proveedor del artículo</param>
	''' <returns>La imagen requerida</returns>
	''' <remarks>Llamada desde DevolverImagenArt de la clase CArticulos
	''' Tiempo máximo: 0 sec.</remarks>
	Public Function Articulos_DevolverImagenArt(ByVal Art As String, ByVal Prove As String)
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet
		Dim sConsulta As String

		Using cn
			cn.Open()

			cm = New SqlCommand
			cm.Connection = cn

			sConsulta = "SELECT IMAGEN FROM PROVE_ART4 WITH (NOLOCK)"
			sConsulta = sConsulta & " WHERE PROVE='" & DblQuote(Prove) & "' "
			sConsulta = sConsulta & " AND ART='" & DblQuote(Art) & "' "
			cm.CommandText = sConsulta

			cm.CommandType = CommandType.Text
			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)

			If dr.Tables(0).Rows.Count = 0 Then
				Return Nothing
			Else
				Return DBNullToSomething(dr.Tables(0).Rows(0).Item("IMAGEN"))
			End If

		End Using
	End Function
	''' <summary>
	''' Función que devuelve la Thumbnail del artículo requerido
	''' </summary>
	''' <param name="Art">Código del artículo</param>
	''' <param name="Prove">Código del proveedor del artículo</param>
	''' <returns>La Thumbnail requerida</returns>
	''' <remarks>Llamada desde DevolverThumbnailArt de la clase CArticulos
	''' Tiempo máximo: 0 sec.</remarks>
	Public Function Articulos_DevolverThumbnailArt(ByVal Art As String, ByVal Prove As String)
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet
		Dim sConsulta As String

		Using cn
			cn.Open()

			cm = New SqlCommand
			cm.Connection = cn

			sConsulta = "SELECT THUMBNAIL FROM PROVE_ART4 WITH (NOLOCK)"
			sConsulta = sConsulta & " WHERE PROVE='" & DblQuote(Prove) & "' "
			sConsulta = sConsulta & " AND ART='" & DblQuote(Art) & "' "
			cm.CommandText = sConsulta

			cm.CommandType = CommandType.Text
			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)

			If dr.Tables(0).Rows.Count = 0 Then
				Return Nothing
			Else
				Return DBNullToSomething(dr.Tables(0).Rows(0).Item("THUMBNAIL"))
			End If

		End Using
	End Function
	''' <summary>
	''' Función que devuelve todas las categorias utilizadas en los pedidos
	''' </summary>
	''' <returns>Un DataSet con la información de las categorias de los pedidos</returns>
	''' <remarks>
	''' Llamada desde:EPServer\cArticulos\DevolverCategoriasPedidos</remarks>
	Public Function Articulos_DevolverCategoriasPedidos() As DataSet
		Authenticate()
		Dim SqlCn As New SqlConnection(mDBConnection)
		Dim SqlCmd As New SqlCommand("FSEP_DEVOLVER_CATEGORIAS_PEDIDOS", SqlCn)
		Dim Ds As New DataSet
		Dim Da As New SqlDataAdapter
		SqlCmd.CommandType = CommandType.StoredProcedure
		SqlCn.Open()
		Da.SelectCommand = SqlCmd
		Da.Fill(Ds)
		SqlCn.Close()
		Return Ds
		Da.Dispose()
		SqlCmd.Dispose()
		SqlCn.Dispose()
	End Function
	''' <summary>
	''' Función que devuelve Las especificaciones del articulo
	''' </summary>
	''' <param name="sCodArt">Codigo articulo</param>
	''' <param name="sCodProve">Codigo proveedor</param>
	''' <returns>string con Las especificaciones del articulo</returns>
	''' <remarks>
	''' Llamada desde:EPServer\cArticulos.vb</remarks>
	Public Function Articulos_DevolverEspecificacionesArt(ByVal sCodArt As String, ByVal sCodProve As String) As String
		Authenticate()
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand
		Dim dr As New DataSet
		Dim sConsulta As String
		Dim resultado As String

		Using cn
			cn.Open()
			cm = New SqlCommand
			cm.Connection = cn
			sConsulta = "SELECT ESP FROM CATALOG_LIN WITH (NOLOCK) "
			sConsulta = sConsulta & " WHERE PROVE='" & DblQuote(sCodProve) & "' "
			sConsulta = sConsulta & " AND ART_INT='" & DblQuote(sCodArt) & "' "
			cm.CommandText = sConsulta
			cm.CommandType = CommandType.Text
			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(dr)

			If dr.Tables(0).Rows.Count = 0 Then
				resultado = ""
			Else
				resultado = DBNullToStr(dr.Tables(0).Rows(0).Item("ESP"))
			End If
			Return resultado

		End Using
	End Function
#End Region
#Region " Empresas data access methods "
	''' <summary>
	''' Devuelve todas las empresas.
	''' </summary>
	''' <returns>Un DataTable con todas las empresas</returns>
	''' <remarks>
	''' Llamada desde: CEmpresas.CargarDatosEmpresas </remarks>
	Public Function Empresas_CargarDatos() As DataTable
		Authenticate()

		Using sqlcmd As New SqlCommand
			Dim sText As String
			Using cn As New SqlConnection(mDBConnection)
				sqlcmd.Connection = cn
				sqlcmd.CommandType = CommandType.Text
				cn.Open()
				sText = "SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP, SOCIEDAD, ORG_COMPRAS, GRP_COMPRAS FROM EMP WITH(NOLOCK) ORDER BY DEN"
				sqlcmd.CommandText = sText

				Dim dt As New DataTable
				Dim da As New SqlDataAdapter
				da.SelectCommand = sqlcmd
				da.Fill(dt)

				Return dt
			End Using
		End Using
	End Function
#End Region
	''' <summary>
	''' Función que devuelve el nombre de la mapper de validaciones, a partir de la empresa o del pedido (orden_entrega.id)
	''' </summary>
	''' <param name="Empresa">Identificador de la empresa para la que se emite el pedido</param>
	''' <param name="IdOrden">Identificador de la orden de entrega</param>
	''' <returns>El nombre de la dll de la Mapper</returns>
	''' <remarks>Llamada desde ValidacionesIntegracion.vb\EvalMapper
	''' Tiempo maximo 0 sec
	''' revisado por ngo 20/01/2012</remarks>
	Public Function Devolver_Nombre_Mapper(Optional ByVal Empresa As Long = 0, Optional ByVal IdOrden As Long = 0) As String
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim dr As New DataSet
		Dim drAux As New DataSet
		Dim da As New SqlDataAdapter

		If Empresa = 0 And IdOrden <> 0 Then
			Try
				cn.Open()
				cm.Connection = cn
				'Si esta activa la integracion de pedidos en sentido Salida o entrada<->salida:
				cm.CommandText = "SELECT EMPRESA FROM ORDEN_ENTREGA OE WITH(NOLOCK) WHERE OE.ID=@ORDEN_ENTREGA"
				cm.CommandType = CommandType.Text
				cm.Parameters.AddWithValue("@ORDEN_ENTREGA", IdOrden)
				da.SelectCommand = cm
				da.Fill(drAux)

				If drAux.Tables(0).Rows.Count > 0 Then
					Empresa = drAux.Tables(0).Rows(0).Item(0)
				Else
					Empresa = 0
				End If
			Catch e As Exception
				Throw e
			Finally
				cn.Close()
				drAux.Dispose()
			End Try
		End If

		Try
			cn.Open()
			cm = New SqlCommand
			cm.Connection = cn
			cm.CommandText = "FSPM_DEVOLVER_MAPPER"
			cm.Parameters.AddWithValue("@EMPRESA", Empresa)
			cm.CommandType = CommandType.StoredProcedure
			da.SelectCommand = cm
			da.Fill(dr)
			Return dr.Tables(0).Rows(0).Item(0)
		Catch e As Exception
			Throw e
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Function
#End Region
#Region "EmisionPedido"
	''' <summary>
	''' Devuelve los datos generales de la cesta en iID
	''' </summary>
	''' <param name="iId">Código de la cesta</param>
	''' <param name="iFavorito">Codigo de favorito</param>
	''' <returns>Un DataSet ordenado por el parámetro de entrada con todos los artículos de la cesta del usuario</returns>
	''' <remarks>
	''' Tiempo máximo: aproximadamente 0,75 sg</remarks>
	Public Function Cargar_Datos_Generales(ByVal iId As Integer, ByVal iFavorito As Integer, ByVal iCodFavorito As Integer, Optional ByVal iModiFav As Integer = 0) As DataSet
		Authenticate()
		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_CARGAR_DATOS_GENERALES"
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@ID", iId)
		If (iFavorito = 1) Then
			adoPar = adoCom.Parameters.AddWithValue("@IDFAV", iCodFavorito)
		Else
			adoPar = adoCom.Parameters.AddWithValue("@IDFAV", 0)
		End If
		adoPar = adoCom.Parameters.AddWithValue("@MODIFFAV", iModiFav)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>
	''' Cargar los adjuntos de un pedido
	''' </summary>
	''' <param name="iId">id del pedido</param>
	''' <returns></returns>
	''' <remarks>
	''' Llamada desde: La clase CEmisionPedido método EP_Get_Emision_Pedido
	''' Tiempo máximo: aproximadamente 0,75 sg</remarks>
	Public Function Cargar_Adjuntos(ByVal iDesdeFavorito As Integer, ByVal iId As Integer) As DataSet
		Authenticate()
		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		If iDesdeFavorito = 0 Then
			adoCom.CommandText = "FSEP_CARGAR_ADJUNTOS"
		Else
			adoCom.CommandText = "FSEP_CARGAR_ADJUNTOS_FAV"
		End If
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@ID", iId)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function

	''' <summary>
	''' Carga los adjuntos de una linea
	''' </summary>
	''' <param name="iId">id de la linea</param>
	''' <returns></returns>
	''' <remarks>
	''' Llamada desde: La clase CEmisionPedido método EP_Get_Emision_Pedido
	''' Tiempo máximo: aproximadamente 0,75 sg</remarks>
	Public Function Cargar_AdjuntosLinea(ByVal iDesdeFavorito As Integer, ByVal iId As Integer) As DataSet
		Authenticate()
		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		If iDesdeFavorito = 0 Then
			adoCom.CommandText = "FSEP_CARGAR_ADJUNTOS_LINEA"
		Else
			adoCom.CommandText = "FSEP_CARGAR_ADJUNTOS_LINEA_FAV"
		End If
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@ID", iId)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>
	''' Devuelve los atributos de la cabecera del pedido
	''' </summary>
	''' <returns>Un DataSet</returns>
	''' <remarks>
	''' Llamada desde: La clase CEmisionPedidso método Cargar_Atributos
	''' Tiempo máximo: aproximadamente 0,75 sg</remarks>
	Public Function Linea_TipoPedido(ByVal iCodPedido As Integer, ByVal iIdTipoPedido As Integer, ByVal iEmp As Integer) As DataSet
		Authenticate()

		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_LINEA_TIPOPEDIDO"
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@IDPEDIDO", iCodPedido)
		adoPar = adoCom.Parameters.AddWithValue("@IDTIPOPEDIDO", iIdTipoPedido)
		adoPar = adoCom.Parameters.AddWithValue("@EMPRESA", iEmp)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>
	''' Devuelve los atributos de la cabecera del pedido
	''' </summary>
	''' <returns>Un DataSet</returns>
	''' <remarks>
	''' Llamada desde: La clase CEmisionPedidso método Cargar_Atributos
	''' Tiempo máximo: aproximadamente 0,75 sg</remarks>
	Public Function Comprobar_TipoPedido(ByVal iCodPedido As Integer, ByVal iIdTipoPedido As Integer, ByVal iEmp As Integer) As Integer()
		Authenticate()

		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim adoPar1 As New SqlParameter
		Dim adoPar2 As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_COMPROBAR_TIPOPEDIDO"
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@IDPEDIDO", iCodPedido)
		adoPar = adoCom.Parameters.AddWithValue("@IDTIPOPEDIDO", iIdTipoPedido)
		adoPar = adoCom.Parameters.AddWithValue("@EMPRESA", iEmp)
		adoPar1 = adoCom.Parameters.Add("@OUTPUT1", SqlDbType.BigInt)
		adoPar1.Direction = ParameterDirection.Output
		adoPar2 = adoCom.Parameters.Add("@OUTPUT2", SqlDbType.BigInt)
		adoPar2.Direction = ParameterDirection.Output
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()


		Dim oTipoPedido() As Integer
		Dim mRsTotal As Integer
		mRsTotal = DBNullToInteger(adoPar1.Value)
		Dim mRsExiste As Integer
		If IsDBNull(adoPar2.Value) Then
			mRsExiste = 1
		Else
			mRsExiste = adoPar2.Value
		End If
		ReDim Preserve oTipoPedido(0)
		oTipoPedido(0) = mRsTotal
		ReDim Preserve oTipoPedido(1)
		oTipoPedido(1) = mRsExiste


		Return oTipoPedido
		adoRS.Clear()
	End Function
	''' <summary>
	''' Devuelve los atributos de la cabecera del pedido
	''' </summary>
	''' <returns>Un DataSet</returns>
	''' <remarks>
	''' Llamada desde: La clase CEmisionPedidso método Cargar_Atributos
	''' Tiempo máximo: aproximadamente 0,75 sg</remarks>
	Public Function Cargar_Atributos_Pedido(ByVal iDesdeFavorito As Integer, ByVal iCodPedido As Integer, ByVal iIdTipoPedido As Integer,
											ByVal bHayIntegracion As Boolean, ByVal idEmpresa As Integer, ByVal EsPedidoAbierto As Boolean, ByVal iCodOrdenEmitir As Long,
											ByVal Usuario As String) As DataSet
		Authenticate()

		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		If EsPedidoAbierto Then
			adoCom.CommandText = "FSEP_CARGAR_ATRIBUTOS_PEDIDOABIERTO"
		Else
			If iDesdeFavorito = 0 Then
				adoCom.CommandText = "FSEP_CARGAR_ATRIBUTOS_PEDIDO"
			ElseIf iDesdeFavorito = 1 Then
				adoCom.CommandText = "FSEP_CARGAR_ATRIBUTOS_PEDIDO_FAV"
			Else 'Es una emision desde favorito sacar los atributos de int_atrib, catnX_atrib, etc. Pero los datos (valor_text, etc) q puedas los sacas de FAVORITOS_ORDEN_ENTREGA_ATRIB 
				adoCom.CommandText = "FSEP_CARGAR_ATRIBUTOS_PEDIDO"
			End If
		End If
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@IDPEDIDO", iCodPedido)
		adoPar = adoCom.Parameters.AddWithValue("@IDTIPOPEDIDO", iIdTipoPedido)
		adoPar = adoCom.Parameters.AddWithValue("@INTEGRACION", bHayIntegracion)
		adoPar = adoCom.Parameters.AddWithValue("@EMPRESA", idEmpresa)
		If Not EsPedidoAbierto AndAlso (iDesdeFavorito = 2) Then adoPar = adoCom.Parameters.AddWithValue("@EMITIRFAVORITO", iCodOrdenEmitir) 'Es una emision desde favorito sacar los atributos de int_atrib, catnX_atrib, etc. Pero los datos q puedas los sacas de FAVORITOS_ORDEN_ENTREGA_ATRIB 
		If Not EsPedidoAbierto Then adoPar = adoCom.Parameters.AddWithValue("@USU", Usuario)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>
	''' Devuelve los atributos de la cabecera del pedido
	''' </summary>
	''' <returns>Un DataSet</returns>
	''' <remarks>
	''' Llamada desde: La clase CEmisionPedidso método Cargar_Atributos
	''' Tiempo máximo: aproximadamente 0,75 sg</remarks>
	Public Function Cargar_Atributos_Integracion_Pedido(ByVal iCodPedido As Integer, ByVal iIdTipoPedido As Integer, ByVal bHayIntegracion As Boolean, ByVal idEmpresa As Integer, ByVal Usuario As String) As DataSet
		Authenticate()

		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_CARGAR_ATRIBUTOS_PEDIDO_INTEGRACION"
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@IDPEDIDO", iCodPedido)
		adoPar = adoCom.Parameters.AddWithValue("@IDTIPOPEDIDO", iIdTipoPedido)
		adoPar = adoCom.Parameters.AddWithValue("@INTEGRACION", bHayIntegracion)
		adoPar = adoCom.Parameters.AddWithValue("@EMPRESA", idEmpresa)
		adoPar = adoCom.Parameters.AddWithValue("@USU", Usuario)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>Devuelve los costes del pedido
	''' </summary>
	''' <param name="iCodPedido">Codigo del pedido</param>
	''' <returns>Un DataSet</returns>
	''' <remarks>
	''' Llamada desde: La clase CEmisionPedidso método Cargar_Atributos
	''' Tiempo máximo: aproximadamente 0,75 sg</remarks>
	Public Function Cargar_Atributos_Coste(ByVal iCodPedido As Integer, ByVal iIdTipoPedido As Integer, ByVal sIdioma As String, ByVal EsPedidoAbierto As Boolean) As DataSet
		Authenticate()
		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		If EsPedidoAbierto Then
			adoCom.CommandText = "FSEP_CARGAR_COSTES_PEDIDOABIERTO"
		Else
			adoCom.CommandText = "FSEP_CARGAR_COSTES_PEDIDO"
		End If
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@IDPEDIDO", iCodPedido)

		adoPar = adoCom.Parameters.AddWithValue("@IDIOMA", sIdioma)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>Devuelve los costes de la linea
	''' </summary>
	''' <returns>Un DataSet</returns>
	''' <remarks>
	''' Llamada desde: La clase CEmisionPedidso método Cargar_Atributos
	''' Tiempo máximo: aproximadamente 0,75 sg</remarks>
	Public Function Cargar_Atributos_Coste_Linea(ByVal iDesdeFavorito As Integer, ByVal iCodLinea As Integer, ByVal iIdTipoPedido As Integer, ByVal EsPedidoAbierto As Boolean) As DataSet
		Authenticate()
		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		If EsPedidoAbierto Then
			adoCom.CommandText = "FSEP_CARGAR_COSTES_LINEA_PEDIDOABIERTO"
		Else
			adoCom.CommandText = "FSEP_CARGAR_COSTES_LINEA"
		End If
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@IDLINEA", iCodLinea)
		adoPar = adoCom.Parameters.AddWithValue("@IDTIPOPEDIDO", iIdTipoPedido)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>Devuelve los descuentos del pedido
	''' </summary>
	''' <returns>Un DataSet</returns>
	''' <remarks>
	''' Llamada desde: La clase CEmisionPedidso método Cargar_Atributos
	''' Tiempo máximo: aproximadamente 0,75 sg</remarks>
	Public Function Cargar_Atributos_Descuento(ByVal iCodPedido As Integer, ByVal iIdTipoPedido As Integer, ByVal EsPedidoAbierto As Boolean) As DataSet
		Authenticate()
		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		If EsPedidoAbierto Then
			adoCom.CommandText = "FSEP_CARGAR_DESCUENTOS_PEDIDOABIERTO"
		Else
			adoCom.CommandText = "FSEP_CARGAR_DESCUENTOS_PEDIDO"
		End If
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@IDPEDIDO", iCodPedido)
		adoPar = adoCom.Parameters.AddWithValue("@IDTIPOPEDIDO", iIdTipoPedido)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>Devuelve los descuentos de la linea
	''' </summary>
	''' <returns>Un DataSet</returns>
	''' <remarks>
	''' Llamada desde: La clase CEmisionPedidso método Cargar_Atributos
	''' Tiempo máximo: aproximadamente 0,75 sg</remarks>
	Public Function Cargar_Atributos_Descuento_Linea(ByVal iDesdeFavorito As Integer, ByVal iCodLinea As Integer, ByVal iIdTipoPedido As Integer, ByVal EsPedidoAbierto As Boolean) As DataSet
		Authenticate()
		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		If EsPedidoAbierto Then
			adoCom.CommandText = "FSEP_CARGAR_DESCUENTOS_LINEA_PEDIDOABIERTO"
		Else
			adoCom.CommandText = "FSEP_CARGAR_DESCUENTOS_LINEA"
		End If

		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@IDLINEA", iCodLinea)
		adoPar = adoCom.Parameters.AddWithValue("@IDTIPOPEDIDO", iIdTipoPedido)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>
	''' Devuelve los atributos de la cabecera del pedido
	''' </summary>
	''' <returns>Un DataSet</returns>
	''' <remarks>
	''' Llamada desde: La clase CEmisionPedidso método Cargar_Atributos
	''' Tiempo máximo: aproximadamente 0,75 sg</remarks>
	Public Function Cargar_Atributos_Linea_Pedido(ByVal iDesdeFavorito As Integer, ByVal iIdTipoPedido As Integer, ByVal iCodLinea As Integer,
												ByVal bHayIntegracion As Boolean, ByVal idEmpresa As Integer, ByVal EsPedidoAbierto As Boolean,
												ByVal iCodLineaEmitir As Long, ByVal Usuario As String) As DataSet
		Authenticate()
		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		If EsPedidoAbierto Then
			adoCom.CommandText = "FSEP_CARGAR_ATRIBUTOS_LINEA_PEDIDOABIERTO"
		Else
			If iDesdeFavorito = 0 Then
				adoCom.CommandText = "FSEP_CARGAR_ATRIBUTOS_LINEA_PEDIDO"
			ElseIf iDesdeFavorito = 1 Then
				adoCom.CommandText = "FSEP_CARGAR_ATRIBUTOS_LINEA_PEDIDO_FAV"
			Else 'Es una emision desde favorito sacar los atributos de int_atrib, catnX_atrib, etc. Pero los datos (valor_text, etc) q puedas los sacas de FAVORITOS_LINEAS_PEDIDO_ATRIB 
				adoCom.CommandText = "FSEP_CARGAR_ATRIBUTOS_LINEA_PEDIDO"
			End If
		End If
		adoCom.CommandType = CommandType.StoredProcedure
		If EsPedidoAbierto OrElse Not (iDesdeFavorito = 1) Then adoPar = adoCom.Parameters.AddWithValue("@IDCODLINEA", iCodLinea)
		adoPar = adoCom.Parameters.AddWithValue("@IDTIPOPEDIDO", iIdTipoPedido)
		adoPar = adoCom.Parameters.AddWithValue("@INTEGRACION", bHayIntegracion)
		adoPar = adoCom.Parameters.AddWithValue("@EMPRESA", idEmpresa)
		If Not EsPedidoAbierto AndAlso (iDesdeFavorito = 1) Then adoPar = adoCom.Parameters.AddWithValue("@FAVORITO", iCodLinea)
		If Not EsPedidoAbierto AndAlso (iDesdeFavorito = 2) Then adoPar = adoCom.Parameters.AddWithValue("@EMITIRFAVORITO", iCodLineaEmitir)
		If Not EsPedidoAbierto Then adoPar = adoCom.Parameters.AddWithValue("@USU", Usuario)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>
	''' Devuelve los atributos de la cabecera del pedido
	''' </summary>
	''' <returns>Un DataSet</returns>
	''' <remarks>
	''' Llamada desde: La clase CEmisionPedidso método Cargar_Atributos
	''' Tiempo máximo: aproximadamente 0,75 sg</remarks>
	Public Function Cargar_Atributos_Integracion_Linea_Pedido(ByVal iDesdeFavorito As Integer, ByVal iIdTipoPedido As Integer, ByVal iCodLinea As Integer, ByVal bHayIntegracion As Boolean, ByVal idEmpresa As Integer _
															  , ByVal Usuario As String) As DataSet
		Authenticate()
		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_CARGAR_ATRIBUTOS_LINEA_PEDIDO_INTEGRACION"
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@IDCODLINEA", iCodLinea)
		adoPar = adoCom.Parameters.AddWithValue("@IDTIPOPEDIDO", iIdTipoPedido)
		adoPar = adoCom.Parameters.AddWithValue("@INTEGRACION", bHayIntegracion)
		adoPar = adoCom.Parameters.AddWithValue("@EMPRESA", idEmpresa)
		adoPar = adoCom.Parameters.AddWithValue("@USU", Usuario)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>
	''' Devuelve los atributos de la cabecera del pedido
	''' </summary>
	''' <returns>Un DataSet</returns>
	''' <remarks>
	''' Llamada desde: La clase CEmisionPedidso método Cargar_Atributos
	''' Tiempo máximo: aproximadamente 0,75 sg</remarks>
	Public Function Cargar_Impuestos_Atributo(ByVal idAtributo As Integer, ByVal sIdioma As String, ByVal idLinea As Long, ByVal EsPedidoAbierto As Boolean) As DataSet
		Authenticate()
		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		If EsPedidoAbierto Then
			adoCom.CommandText = "FSEP_CARGAR_IMPUESTOS_ATRIBUTO_PEDIDOABIERTO"
		Else
			adoCom.CommandText = "FSEP_CARGAR_IMPUESTOS_ATRIBUTO"
		End If
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@IDATRIB", idAtributo)
		adoPar = adoCom.Parameters.AddWithValue("@IDIOMA", sIdioma)
		adoPar = adoCom.Parameters.AddWithValue("@IDLINEA", idLinea)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>
	''' Devuelve los valores de los atributos
	''' </summary>
	''' <returns>Un DataSet</returns>
	''' <remarks>
	''' Llamada desde: La clase CEmisionPedidso método Cargar_Atributos
	''' Tiempo máximo: aproximadamente 0,75 sg</remarks>
	Public Function Cargar_Valores_Atributos_Pedido(ByVal iIdTipo As Integer) As DataSet
		Authenticate()
		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_CARGAR_VALORES_ATRIBUTOS_PEDIDO"
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@IDTIPOPEDIDO", iIdTipo)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>Devuelve las organizaciones de compras asociadas a una empresa</summary>
	''' <param name="iEmpresa">Id empresa</param>
	''' <param name="sDenOrgCompras">Den. de la org. de compras (caracteres iniciales)</param>
	''' <returns>Un DataSet</returns>
	''' <remarks>Llamada desde: COrganizacionesCompras.CargarOrganizacionesCompras</remarks>
	Public Function DevolverOrganizacionesCompras(ByVal iEmpresa As Integer, Optional ByVal sDenOrgCompras As String = Nothing) As DataSet
		Authenticate()
		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		Dim da As New SqlDataAdapter

		Try
			adoCom.Connection = cn
			cn.Open()
			adoCom.CommandText = "FSEP_DEVOLVER_ORGANIZACIONES_COMPRAS"
			adoCom.CommandType = CommandType.StoredProcedure
			adoPar = adoCom.Parameters.AddWithValue("@EMPRESA", iEmpresa)
			adoPar = adoCom.Parameters.AddWithValue("@DEN", sDenOrgCompras)
			da.SelectCommand = adoCom
			da.Fill(adoRS)

			Return adoRS
		Catch ex As Exception
			Throw ex
		Finally
			cn.Close()
			cn.Dispose()
			adoCom.Dispose()
			da.Dispose()
		End Try
	End Function
	''' <summary>Devuelve los centros asociados a un item</summary>       
	''' <param name="iAnyo">Anyo</param>
	''' <param name="sGMN1">GMN1</param>
	''' <param name="iProce">Cod. proceso</param>
	''' <param name="iItem">Id ítem</param>
	''' <param name="sOrgCompras">Org. compras</param>
	''' <remarks>Llamada desde: Centros.CargarCentrosAprovisionamiento</remarks>
	Public Function DevolverCentrosItem(ByVal iAnyo As Integer, ByVal sGMN1 As String, ByVal iProce As Integer, ByVal iItem As Integer, Optional ByVal sOrgCompras As String = Nothing, Optional ByVal sUsu As String = Nothing) As DataSet
		Authenticate()
		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim cn As New SqlConnection(mDBConnection)
		Dim da As New SqlDataAdapter

		Try
			adoCom.Connection = cn
			cn.Open()
			adoCom.CommandText = "FSEP_DEVOLVER_CENTROS_ITEM"
			adoCom.CommandType = CommandType.StoredProcedure
			adoCom.Parameters.AddWithValue("@ANYO", iAnyo)
			adoCom.Parameters.AddWithValue("@GMN1", sGMN1)
			adoCom.Parameters.AddWithValue("@PROCE", iProce)
			adoCom.Parameters.AddWithValue("@ITEM", iItem)
			adoCom.Parameters.AddWithValue("@ORGCOMPRAS", IIf(String.IsNullOrEmpty(sOrgCompras), DBNull.Value, sOrgCompras))
			adoCom.Parameters.AddWithValue("@USU", IIf(String.IsNullOrEmpty(sUsu), DBNull.Value, sUsu))
			da.SelectCommand = adoCom
			da.Fill(adoRS)

			Return adoRS
		Catch ex As Exception
			Throw ex
		Finally
			cn.Close()
			cn.Dispose()
			adoCom.Dispose()
			da.Dispose()
		End Try
	End Function
	''' <summary>Devuelve los centros asociados a un ítem no codificado (si artículo)</summary>       
	''' <param name="iAnyo">Anyo</param>
	''' <param name="sGMN1">GMN1</param>
	''' <param name="iProce">Cod. proceso</param>
	''' <param name="iItem">Id ítem</param>
	''' <param name="sCentro">Centro</param>
	''' <param name="sOrgCompras">Org. compras</param>
	''' <remarks>Llamada desde: CCentros.CargarCentrosAprovisionamiento</remarks>
	Public Function DevolverCentrosPorItemNoCodificado(ByVal iAnyo As Integer, ByVal sGMN1 As String, ByVal iProce As Integer, ByVal iItem As Integer, Optional ByVal sCentro As String = Nothing,
													   Optional ByVal sOrgCompras As String = Nothing, Optional ByVal sUsu As String = Nothing) As DataSet
		Authenticate()
		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim cn As New SqlConnection(mDBConnection)
		Dim da As New SqlDataAdapter

		Try
			adoCom.Connection = cn
			cn.Open()
			adoCom.CommandText = "FSEP_DEVOLVER_CENTROS_ITEM_NO_CODIFICADO"
			adoCom.CommandType = CommandType.StoredProcedure
			adoCom.Parameters.AddWithValue("@ANYO", iAnyo)
			adoCom.Parameters.AddWithValue("@GMN1", sGMN1)
			adoCom.Parameters.AddWithValue("@PROCE", iProce)
			adoCom.Parameters.AddWithValue("@ITEM", iItem)
			adoCom.Parameters.AddWithValue("@CODCENTRO", IIf(String.IsNullOrEmpty(sCentro), DBNull.Value, sCentro))
			adoCom.Parameters.AddWithValue("@ORGCOMPRAS", IIf(String.IsNullOrEmpty(sOrgCompras), DBNull.Value, sOrgCompras))
			adoCom.Parameters.AddWithValue("@USU", IIf(String.IsNullOrEmpty(sUsu), DBNull.Value, sUsu))
			da.SelectCommand = adoCom
			da.Fill(adoRS)

			Return adoRS
		Catch ex As Exception
			Throw ex
		Finally
			cn.Close()
			cn.Dispose()
			adoCom.Dispose()
			da.Dispose()
		End Try
	End Function
	''' <summary>Devuelve los centros asociados a un artículo</summary>       
	''' <param name="sCodArt">Cod. artículo</param>
	''' <param name="bAtravesItem">A través de ítem</param>
	''' <param name="iAnyo">Anyo</param>
	''' <param name="sGMN1">GMN1</param>
	''' <param name="iProce">Cod. proceso</param>
	''' <param name="iItem">Id. ítem</param>
	''' <param name="sCodCentro">Cod. centro</param>
	''' <param name="sOrgCompras">Org. compras</param>
	''' <param name="bAlmacen">Almacén</param>
	''' <remarks>Llamada desde: Centros.CargarCentrosAprovisionamiento</remarks>
	Public Function DevolverCentrosArticulo(ByVal sCodArt As String, Optional ByVal bAtravesItem As Boolean = False, Optional ByVal iAnyo As Integer = 0, Optional ByVal sGMN1 As String = Nothing, Optional ByVal iProce As Integer = 0,
											Optional ByVal iItem As Long = 0, Optional ByVal sCodCentro As String = Nothing, Optional ByVal sOrgCompras As String = Nothing, Optional ByVal bAlmacen As Boolean = False,
											Optional ByVal sUsu As String = Nothing) As DataSet
		Authenticate()
		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim cn As New SqlConnection(mDBConnection)
		Dim da As New SqlDataAdapter

		Try
			adoCom.Connection = cn
			cn.Open()
			adoCom.CommandText = "FSEP_DEVOLVER_CENTROS_ARTICULO"
			adoCom.CommandType = CommandType.StoredProcedure
			adoCom.Parameters.AddWithValue("@CODART", sCodArt)
			adoCom.Parameters.AddWithValue("@ATRAVESITEM", IIf(bAtravesItem, 1, 0))
			adoCom.Parameters.AddWithValue("@ANYO", iAnyo)
			adoCom.Parameters.AddWithValue("@GMN1", sGMN1)
			adoCom.Parameters.AddWithValue("@PROCE", iProce)
			adoCom.Parameters.AddWithValue("@ITEM", iItem)
			adoCom.Parameters.AddWithValue("@CODCENTRO", IIf(String.IsNullOrEmpty(sCodCentro), DBNull.Value, sCodCentro))
			adoCom.Parameters.AddWithValue("@ORGCOMPRAS", IIf(String.IsNullOrEmpty(sOrgCompras), DBNull.Value, sOrgCompras))
			adoCom.Parameters.AddWithValue("@ALMACEN", IIf(bAlmacen, 1, 0))
			adoCom.Parameters.AddWithValue("@USU", IIf(String.IsNullOrEmpty(sUsu), DBNull.Value, sUsu))

			da.SelectCommand = adoCom
			da.Fill(adoRS)

			Return adoRS
		Catch ex As Exception
			Throw ex
		Finally
			cn.Close()
			cn.Dispose()
			adoCom.Dispose()
			da.Dispose()
		End Try
	End Function
	''' <summary>Devuelve los almacenes asociados a un centro de aprovisionamiento</summary>           
	''' <param name="sCentro">Cod. centro</param>    
	''' <returns>Dataset con los almacenes del centro</returns>
	''' <remarks>Llamada desde: Centro.DevolverAlmacenesCentroAprovisionamiento</remarks>    
	Public Function DevolverAlmacenesCentroAprovisionamiento(ByVal sCentro As String) As DataSet
		Authenticate()
		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim cn As New SqlConnection(mDBConnection)
		Dim da As New SqlDataAdapter

		Try
			adoCom.Connection = cn
			cn.Open()
			adoCom.CommandText = "FSEP_DEVOLVER_ALMACENES_CENTRO_APROV"
			adoCom.CommandType = CommandType.StoredProcedure
			adoCom.Parameters.AddWithValue("@CENTRO", sCentro)

			da.SelectCommand = adoCom
			da.Fill(adoRS)

			Return adoRS
		Catch ex As Exception
			Throw ex
		Finally
			cn.Close()
			cn.Dispose()
			adoCom.Dispose()
			da.Dispose()
		End Try
	End Function
	''' <summary>
	''' Función que devuelve los atributos del pedido relacionados con el tipo de pedido
	''' </summary>
	''' <param name="iCodPedido">i Cod Pedido</param>
	''' <param name="iIdTipoPedido">Id Pedido</param>
	''' <param name="bHayIntegracion">Si hay integración</param>
	''' <param name="idEmpresa">Id Empresa</param> 
	''' <returns>DataSet</returns>
	Public Function Cargar_Atributos_TipoPedido(ByVal iCodPedido As Integer, ByVal iIdTipoPedido As Integer, ByVal bHayIntegracion As Boolean, ByVal idEmpresa As Integer, ByVal Usuario As String) As DataSet
		Authenticate()
		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_CARGAR_ATRIBUTOS_TIPOPEDIDO"
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@IDPEDIDO", iCodPedido)
		adoPar = adoCom.Parameters.AddWithValue("@IDTIPOPEDIDO", iIdTipoPedido)
		adoPar = adoCom.Parameters.AddWithValue("@INTEGRACION", bHayIntegracion)
		adoPar = adoCom.Parameters.AddWithValue("@EMPRESA", idEmpresa)
		adoPar = adoCom.Parameters.AddWithValue("@USU", Usuario)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function

	Public Function Cargar_Empresa_Primer_PluriAnual(ByVal iCesta As Long) As String
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Dim da As New SqlDataAdapter
		Dim adoRS As New DataSet

		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_CARGAR_EMPRESA_PLURIANUAL_CESTA"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@CESTACABECERAID", iCesta)
			da.SelectCommand = cm
			da.Fill(adoRS)

			If adoRS.Tables(0).Rows.Count = 0 Then
				Return ""
			Else
				Return DBNullToStr(adoRS.Tables(0).Rows(0)("EMPRESA"))
			End If
		Catch ex As Exception
			Throw ex
		Finally
			da = Nothing
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Function

	Public Function TienePresupuestoVigente(ByVal LineaId As Long, ByVal Pres5Imp As Long) As Integer
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Dim da As New SqlDataAdapter
		Dim adoRS As New DataSet

		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_CONTROL_ANYOPARTIDA_LINEA_CESTA"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@LINEACESTA", LineaId)
			cm.Parameters.AddWithValue("@PRES5IMP", Pres5Imp)
			da.SelectCommand = cm
			da.Fill(adoRS)

			If adoRS.Tables(0).Rows.Count = 0 Then
				Return Year(Now)
			Else
				Return adoRS.Tables(0).Rows(0)("RES") 'Todo Ok -> 100000
			End If
		Catch ex As Exception
			Throw ex
		Finally
			da = Nothing
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Function
#End Region
#Region "DetallePedido"
	''' <summary>
	''' Función que devuelve los adjuntos de la cabecera de la orden de entrega
	''' </summary>
	''' <param name="iId">id de la orden de la que obtener los adjuntos</param>
	''' <returns>Un DataSet con los adjuntos de la cabecera de la orden de entrega</returns>
	''' <remarks></remarks>
	Public Function Cargar_Adjuntos_Cabecera_Orden(ByVal iId As Integer) As DataSet
		Authenticate()

		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_CARGAR_ADJUNTOS_CAB_ORDEN"
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@ID", iId)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>
	''' Función que devuelve los adjuntos de la linea de la orden de entrega
	''' </summary>
	''' <param name="iIdLinea">id de la linea de la que obtener los adjuntos</param>
	''' <returns>Un DataSet con los adjuntos de la linea de la orden de entrega</returns>
	''' <remarks></remarks>
	Public Function Cargar_Adjuntos_Linea_Orden(ByVal iIdLinea As Integer) As DataSet
		Authenticate()

		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_CARGAR_ADJUNTOS_LINEA_ORDEN"
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@ID", iIdLinea)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function

	''' <summary>
	''' Devuelve los datos generales de la orden de entrega
	''' </summary>
	''' <param name="iId">Id de la orden de entrega</param>
	''' <returns>Un DataSet con los datos generales de la orden</returns>
	''' <remarks></remarks>
	Public Function Cargar_Datos_Generales_Orden(ByVal iId As Integer, ByVal sIdioma As String) As DataSet
		Authenticate()

		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_CARGAR_DATOS_GENERALES_ORDEN"
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@ID", iId)
		adoPar = adoCom.Parameters.AddWithValue("@IDI", sIdioma)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>
	''' Devuelve los atributos de la cabecera del pedido
	''' </summary>
	''' <returns>Un DataSet</returns>
	''' <remarks>
	''' Llamada desde: La clase CEmisionPedidso método Cargar_Atributos
	''' Tiempo máximo: aproximadamente 0,75 sg</remarks>
	Public Function Cargar_Atributos_Orden(ByVal idOrden As Integer, Optional ByVal bConInternos As Boolean = True, Optional ByVal bSiMostrarEnRecep As Boolean = False) As DataSet
		Authenticate()

		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_CARGAR_ATRIBUTOS_ORDEN"
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@IDORDEN", idOrden)
		'Si se quieren obtener los atributos internos o no
		If Not bConInternos Then
			adoPar = adoCom.Parameters.AddWithValue("@INTERNOS", 0)
		End If
		If bSiMostrarEnRecep Then adoCom.Parameters.AddWithValue("@MOSTRARENRECEP", 1)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function

	''' <summary>
	''' Devuelve los valores de los atributos de una orden
	''' </summary>
	''' <param name="iIdAtributo">id del atributo</param>
	''' <param name="idOrden">id de la orden</param>
	''' <returns>Un DataSet con los valores de los atributos de una orden</returns>
	''' <remarks></remarks>
	Public Function Cargar_Valores_Atributos_Orden(ByVal iIdAtributo As Integer, ByVal idOrden As Long) As DataSet
		Authenticate()

		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_CARGAR_VALORES_ATRIBUTOS_ORDEN"
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@ID", iIdAtributo)
		adoPar = adoCom.Parameters.AddWithValue("@ORDEN", idOrden)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>Devuelve los costes del pedido
	''' </summary>
	''' <returns>Un DataSet</returns>
	''' <remarks>
	''' Llamada desde: La clase CEmisionPedidso método Cargar_Atributos
	''' Tiempo máximo: aproximadamente 0,75 sg</remarks>
	Public Function Cargar_Atributos_Coste_Orden(ByVal idOrden As Long, Optional ByVal sIdioma As String = "SPA") As DataSet
		Authenticate()

		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_CARGAR_COSTES_ORDEN"
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@IDORDEN", idOrden)
		adoPar = adoCom.Parameters.AddWithValue("@IDIOMA", sIdioma)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>
	''' Devuelve los valores de los costes de la cabecera de la orden de entrega
	''' </summary>
	''' <returns>Un DataSet</returns>
	''' <remarks>
	''' Llamada desde: La clase CEmisionPedidso método Cargar_Atributos
	''' Tiempo máximo: aproximadamente 0,75 sg</remarks>
	Public Function Cargar_Valores_Atributos_Coste_Orden(ByVal IdCoste As Long, ByVal idAtrib As Long) As DataSet
		Authenticate()

		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_CARGAR_VALORES_COSTES_ORDEN"
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@ID_ATRIB", idAtrib)
		adoPar = adoCom.Parameters.AddWithValue("@IDCOSTE", IdCoste)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>
	''' Devuelve los descuentos de la orden de entrega
	''' </summary>
	''' <param name="IdOrden">id de la orden de entrega</param>
	''' <returns>Un DataSet</returns>
	''' <remarks></remarks>
	Public Function Cargar_Atributos_Descuento_Orden(ByVal IdOrden As Long) As DataSet
		Authenticate()

		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_CARGAR_DESCUENTOS_ORDEN"
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@IDORDEN", IdOrden)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>
	''' Devuelve los costes de la linea de pedido
	''' </summary>
	''' <param name="iCodLinea">id de la linea de pedido</param>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Function Cargar_Atributos_Coste_Linea_Orden(ByVal iCodLinea As Integer) As DataSet
		Authenticate()

		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_CARGAR_COSTES_LINEA_ORDEN"
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@IDLINEA", iCodLinea)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>
	''' Devuelve los descuentos de la linea de pedido
	''' </summary>
	''' <param name="iCodLinea">id de la linea de pedido</param>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Function Cargar_Atributos_Descuento_Linea_Orden(ByVal iCodLinea As Integer) As DataSet
		Authenticate()

		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_CARGAR_DESCUENTOS_LINEA_ORDEN"
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@IDLINEA", iCodLinea)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>
	''' Devuelve los impuestos del coste/descuento(atributo) de la linea de pedido
	''' </summary>
	''' <param name="idAtributo">id del atributo que sera un coste o un descuento</param>
	''' <param name="sIdioma">idioma</param>
	''' <param name="IdLinea">id de la linea del pedido</param>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Function Cargar_Impuestos_Atributo_Linea_Orden(ByVal idAtributo As Integer, ByVal sIdioma As String, ByVal IdLinea As Long) As DataSet
		Authenticate()

		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_CARGAR_IMPUESTOS_ATRIBUTO_LINEA_ORDEN"
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@IDATRIB", idAtributo)
		adoPar = adoCom.Parameters.AddWithValue("@IDIOMA", sIdioma)
		adoPar = adoCom.Parameters.AddWithValue("@IDLINEA", IdLinea)
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>
	''' Devuelve los atributos de de la linea de pedido(campos de pedido)
	''' </summary>
	''' <param name="iCodLinea">id de la linea de pedido</param>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Function Cargar_Atributos_Linea_Orden(ByVal iCodLinea As Integer, Optional ByVal bConInternos As Boolean = True) As DataSet
		Authenticate()

		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim adoPar As New SqlParameter
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_CARGAR_ATRIBUTOS_LINEA_ORDEN"
		adoCom.CommandType = CommandType.StoredProcedure
		adoPar = adoCom.Parameters.AddWithValue("@IDLINEA", iCodLinea)
		'Si se quieren obtener los atributos internos o no
		If Not bConInternos Then
			adoPar = adoCom.Parameters.AddWithValue("@INTERNOS", 0)
		End If
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function
	''' <summary>
	''' Comprueba las cantidades de la linea de la orden
	''' </summary>
	''' <param name="idLinea">idLineaCesta</param>
	''' <remarks>Llamada desde emisionpedidos. Tiempo Max: </remarks>
	Public Function ComprobarCantidadesLineaOrden(ByVal idLinea As Integer, ByVal sUPed As String) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim da As New SqlDataAdapter
		Dim dr As New DataSet
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_VALIDAR_CANTIDAD_LINEA_ORDEN"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@IDLINEA", idLinea)
			cm.Parameters.AddWithValue("@UPACTUAL", sUPed)
			da.SelectCommand = cm
			da.Fill(dr)
			Return dr
		Catch ex As Exception
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
			da.Dispose()
		End Try
	End Function
	''' <summary>
	''' Devuelve todos las lineas de pedido  de la orden de entrega
	''' </summary>
	''' <param name="sPer">Código de la persona del usuario que este utilizando la aplicación</param>
	''' <param name="Idioma">Idioma actual de la aplicación</param>
	''' 
	''' <returns>Un DataSet ordenado por el parámetro de entrada con todos los artículos de la cesta del usuario</returns>
	''' <remarks>
	''' Llamada desde: La clase COrdenes método CargarCesta
	''' Tiempo máximo: Depende de los artículos de la cesta, aproximadamente 0,75 sg</remarks>
	Public Function Ordenes_CargarLineasPedido(ByVal sPer As String, ByVal Idioma As FSNLibrary.Idioma, ByVal idOrden As Long, ByVal instanciaAprob As Long, ByVal bMostrarLineasBajaLog As Boolean, ByVal bDesdeAprob As Boolean) As DataSet
		Authenticate()
		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet

		Dim cn As New SqlConnection(mDBConnection)

		adoCom.Connection = cn
		cn.Open()
		adoCom.CommandText = "FSEP_CARGAR_LINEAS_ORDEN"
		adoCom.CommandType = CommandType.StoredProcedure
		adoCom.Parameters.AddWithValue("@PER", sPer)
		adoCom.Parameters.AddWithValue("@IDI", Idioma.ToString())
		adoCom.Parameters.AddWithValue("@IDORDEN", idOrden)
		adoCom.Parameters.AddWithValue("@INSTANCIA_APROB", instanciaAprob)
		adoCom.Parameters.AddWithValue("@MOSTRAR_LINEAS_BAJA_LOG", BooleanToSQLBinary(bMostrarLineasBajaLog))
		adoCom.Parameters.AddWithValue("@DESDE_APROBACION", BooleanToSQLBinary(bDesdeAprob))
		Dim da As New SqlDataAdapter
		da.SelectCommand = adoCom
		da.Fill(adoRS)

		da = Nothing
		adoCom = Nothing
		cn.Close()
		Return adoRS
		adoRS.Clear()
	End Function

	''' <summary>
	''' Actualiza las imputacion de las lineas de pedido
	''' </summary>
	''' <param name="idOrden">id de la orden de entrega</param>
	''' <remarks></remarks>
	Public Sub ActualizarImputacionesLineaPedidoEmitida(ByVal idOrden As Integer)
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand()
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_ACTUALIZAR_IMPUTACION_LINEAS_PEDIDO"
			cm.CommandType = CommandType.StoredProcedure
			cm.Transaction = cn.BeginTransaction()
			cm.Parameters.Add(New SqlParameter("@ORDEN", SqlDbType.Int))
			cm.Parameters("@ORDEN").Value = idOrden
			cm.ExecuteNonQuery()
			cm.Transaction.Commit()
		Catch ex As Exception
			cm.Transaction.Rollback()
			Throw ex
		Finally
			cm.Dispose()
			cn.Close()
			cn.Dispose()
		End Try
	End Sub
	Public Function CargarDetalleOrdenes(ByVal idioma As FSNLibrary.Idioma, dtOrdenes As DataTable, smactivo As Boolean, iVerBajasOrdenes As Integer) As DataSet
		Authenticate()

		Dim adoCom As New SqlCommand
		Dim adoRS As New DataSet
		Dim cn As New SqlConnection(mDBConnection)
		adoCom.Connection = cn
		Try
			cn.Open()
			adoCom.CommandText = "FSEP_OBTENER_DETALLE_ORDENES"
			adoCom.CommandType = CommandType.StoredProcedure
			Dim Param As New SqlParameter("@ORDENES", SqlDbType.Structured)
			Param.TypeName = "ORDENESID"
			Param.Value = dtOrdenes
			adoCom.Parameters.Add(Param)
			adoCom.Parameters.Add("@IDIOMA", SqlDbType.VarChar, 3)
			adoCom.Parameters("@IDIOMA").Value = idioma.ToString
			adoCom.Parameters.Add("@SMACTIVO", SqlDbType.TinyInt)
			adoCom.Parameters("@SMACTIVO").Value = IIf(smactivo, 1, 0)
			adoCom.Parameters.Add("@VER_PED_BORRADOS", SqlDbType.Bit)
			adoCom.Parameters("@VER_PED_BORRADOS").Value = iVerBajasOrdenes
			Dim da As New SqlDataAdapter
			da.SelectCommand = adoCom
			da.Fill(adoRS)

			da = Nothing
			adoCom = Nothing
			cn.Close()
			Return adoRS
		Catch ex As Exception
			Throw ex
		Finally
			cn.Close()
			cn.Dispose()
		End Try
	End Function
#End Region
#Region "Pedidos abiertos"
	Public Function Cargar_PedidosAbiertos(ByVal Usu As String, ByVal RestricEmpresaUsuPedidoAbierto As Boolean, ByVal RestricEmpresaPerfUsuPedidoAbierto As Boolean,
										Optional ByVal anio As Integer = 0, Optional ByVal numPedido As Integer = 0, Optional ByVal numOrden As Integer = 0,
										Optional ByVal idEmpresa As Integer = 0, Optional ByVal codArticulo As String = "", Optional ByVal denArticulo As String = "",
										Optional ByVal fecIniDesde As Date = Nothing, Optional ByVal fecIniHasta As Date = Nothing,
										Optional ByVal fecFinDesde As Date = Nothing, Optional ByVal fecFinHasta As Date = Nothing,
										Optional ByVal codProve As String = "") As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim ds As New DataSet
		Dim da As New SqlDataAdapter

		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_GET_PEDIDOS_ABIERTOS"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@USU", Usu)
			cm.Parameters.AddWithValue("@RESTRIC_PEDIDOABIERTO_USU_UON", RestricEmpresaUsuPedidoAbierto)
			cm.Parameters.AddWithValue("@RESTRIC_PEDIDOABIERTO_PERF_UON", RestricEmpresaPerfUsuPedidoAbierto)
			If Not anio = 0 Then cm.Parameters.AddWithValue("@ANIO", anio)
			If Not numPedido = 0 Then cm.Parameters.AddWithValue("@NUMPEDIDO", numPedido)
			If Not numOrden = 0 Then cm.Parameters.AddWithValue("@NUMORDEN", numOrden)
			If Not idEmpresa = 0 Then cm.Parameters.AddWithValue("@IDEMPRESA", idEmpresa)
			If Not codArticulo = "" Then cm.Parameters.AddWithValue("@CODARTICULO", codArticulo)
			If Not denArticulo = "" Then cm.Parameters.AddWithValue("@DENARTICULO", denArticulo)
			If Not fecIniDesde = Date.MinValue Then cm.Parameters.AddWithValue("@FECINIDESDE", fecIniDesde)
			If Not fecIniHasta = Date.MinValue Then cm.Parameters.AddWithValue("@FECINIHASTA", fecIniHasta)
			If Not fecFinDesde = Date.MinValue Then cm.Parameters.AddWithValue("@FECFINDESDE", fecFinDesde)
			If Not fecFinHasta = Date.MinValue Then cm.Parameters.AddWithValue("@FECFINHASTA", fecFinHasta)
			If Not codProve = "" Then cm.Parameters.AddWithValue("@CODPROVE", codProve)
			da.SelectCommand = cm
			da.Fill(ds)
			ds.Tables(0).TableName = "PEDIDOS_ABIERTOS"
			Return ds
		Catch e As Exception
			Throw e
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Function
	Public Function Cargar_Lineas_PedidoAbierto(ByVal PerCod As String, ByVal Orden As Integer, ByVal LineaPedido As Integer) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim ds As New DataSet
		Dim da As New SqlDataAdapter
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_GET_LINEAS_PEDIDO_ABIERTO"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("PER", PerCod)
			cm.Parameters.AddWithValue("ORDEN", Orden)
			If Not LineaPedido = 0 Then cm.Parameters.AddWithValue("LINEAPEDIDO", LineaPedido)
			da.SelectCommand = cm
			da.Fill(ds)
			Return ds
		Catch e As Exception
			Throw e
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Function
	Public Function Cargar_FiltrosAvanzados_PedidoAbierto(ByVal Usu As String,
						ByVal RestricEmpresaUsuPedidoAbierto As Boolean, ByVal RestricEmpresaPerfUsuPedidoAbierto As Boolean) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim ds As New DataSet
		Dim da As New SqlDataAdapter
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_GET_FILTROSAVANZADOS_PEDIDOABIERTO"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@USU", Usu)
			cm.Parameters.AddWithValue("@RESTRIC_PEDIDOABIERTO_USU_UON", RestricEmpresaUsuPedidoAbierto)
			cm.Parameters.AddWithValue("@RESTRIC_PEDIDOABIERTO_PERF_UON", RestricEmpresaPerfUsuPedidoAbierto)
			da.SelectCommand = cm
			da.Fill(ds)
			Return ds
		Catch e As Exception
			Throw e
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Function
	Public Function Obtener_Info_LineaPedidoAbierto(ByVal PerCod As String, ByVal IdCesta As Integer) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim ds As New DataSet
		Dim da As New SqlDataAdapter
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_OBTENER_DISPONIBLE_LINEAPEDIDOABIERTO"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("PER", PerCod)
			cm.Parameters.AddWithValue("IDCESTA", IdCesta)
			da.SelectCommand = cm
			da.Fill(ds)
			Return ds
		Catch e As Exception
			Throw e
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Function
	Public Function Cargar_Empresa_PedidoAbierto(ByVal IdPedidoAbierto As Integer) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim ds As New DataSet
		Dim da As New SqlDataAdapter
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_GET_EMPRESA_PEDIDO_ABIERTO"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("IDPEDIDOABIERTO", IdPedidoAbierto)
			da.SelectCommand = cm
			da.Fill(ds)
			Return ds
		Catch e As Exception
			Throw e
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Function
	Public Function DevolverSolicitud_PedidoContraAbierto(ByVal idPedido As Long, ByRef EstadoOrden As Short) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim ds As New DataSet
		Dim da As New SqlDataAdapter
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_GET_SOLICITUD_WORKFLOW_PEDIDO_ABIERTO"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("IDPEDIDO", idPedido)
			cm.Parameters.Add("@ESTADOORDEN", SqlDbType.Int)
			cm.Parameters("@ESTADOORDEN").Direction = ParameterDirection.Output
			da.SelectCommand = cm
			da.Fill(ds)
			EstadoOrden = cm.Parameters("@ESTADOORDEN").Value
			Return ds
		Catch e As Exception
			Throw e
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Function
	Public Function Comprobar_Importes_Cantidades_PedidoAbierto(ByVal IdLineaPedidoAbierto As Integer) As DataTable
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand
		Dim ds As New DataSet
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSEP_OBTENER_DISPONIBLE_EMISION_PEDIDOABIERTO"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@IDLINEAPEDIDOABIERTO", IdLineaPedidoAbierto)
			Dim da As New SqlDataAdapter
			da.SelectCommand = cm
			da.Fill(ds)

			Return ds.Tables(0)
		Catch ex As SqlException
			Throw ex
		Finally
			cn.Close()
			cn.Dispose()
			cm.Dispose()
			ds.Dispose()
		End Try
	End Function
#End Region
End Class