﻿Imports System.Configuration
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Exception
Imports System.IO
Imports System.Object
Imports Fullstep.FSNLibrary
Partial Public Class Root
#Region "SM. Data Access Methods "
#Region "Centro_SM data access methods"
    Public Function Centro_SM_Info(ByVal Idioma As String, ByVal sUON As String) As DataTable
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter
        Try
            Dim sqlText As String = Nothing
            Dim uons() As String = sUON.Split(New Char() {"#"})
            If uons.Length > 0 Then
                Select Case uons.Length
                    Case 1
                        sqlText = "SELECT C.CENTRO_SM AS COD, (SELECT DEN FROM CENTRO_SM_DEN D WITH (NOLOCK)  WHERE D.CENTRO_SM=C.CENTRO_SM AND D.IDI='{0}') AS DEN," & _
                          "(SELECT EMPRESA FROM UON1 WITH (NOLOCK) WHERE COD='{1}') AS EMPRESA " & _
                          "FROM CENTRO_SM_UON C WITH(NOLOCK) " & _
                          "WHERE UON1='{1}' AND UON2 IS NULL AND UON3 IS NULL AND UON4 IS NULL"
                        sqlText = String.Format(sqlText, Idioma, uons(0))
                    Case 2
                        sqlText = "SELECT C.CENTRO_SM AS COD, (SELECT DEN FROM CENTRO_SM_DEN D WITH (NOLOCK) WHERE D.CENTRO_SM=C.CENTRO_SM AND D.IDI='{0}') AS DEN," & _
                          "(SELECT EMPRESA FROM UON2 WITH (NOLOCK) WHERE UON1='{1}' AND COD='{2}') AS EMPRESA " & _
                          "FROM CENTRO_SM_UON C WITH(NOLOCK) " & _
                          "WHERE UON1='{1}' AND UON2 ='{2}' AND UON3 IS NULL AND UON4 IS NULL"
                        sqlText = String.Format(sqlText, Idioma, uons(0), uons(1))
                    Case 3
                        sqlText = "SELECT C.CENTRO_SM AS COD, (SELECT DEN FROM CENTRO_SM_DEN D WITH (NOLOCK) WHERE D.CENTRO_SM=C.CENTRO_SM AND D.IDI='{0}') AS DEN," & _
                          "(SELECT EMPRESA FROM UON3 WITH (NOLOCK) WHERE UON1='{1}' AND UON2='{2}' AND COD='{3}') AS EMPRESA " & _
                          "FROM CENTRO_SM_UON C WITH(NOLOCK) " & _
                          "WHERE UON1='{1}' AND UON2 ='{2}' AND UON3='{3}' AND UON4 IS NULL"
                        sqlText = String.Format(sqlText, Idioma, uons(0), uons(1), uons(2))
                    Case 4
                        sqlText = "SELECT C.CENTRO_SM AS COD, (SELECT DEN FROM CENTRO_SM_DEN D WITH (NOLOCK) WHERE D.CENTRO_SM=C.CENTRO_SM AND D.IDI='{0}') AS DEN," & _
                          "(SELECT EMPRESA FROM UON4 WITH (NOLOCK) WHERE UON1='{1}' AND UON2='{2}' AND UON3='{3}' AND COD='{4}') AS EMPRESA " & _
                          "FROM CENTRO_SM_UON C WITH(NOLOCK) " & _
                          "WHERE UON1='{1}' AND UON2 ='{2}' AND UON3='{3}' AND UON4='{4}'"
                        sqlText = String.Format(sqlText, Idioma, uons(0), uons(1), uons(2), uons(3))
                End Select

                If Not String.IsNullOrEmpty(sqlText) Then
                    cm.Connection = cn
                    cm.CommandText = sqlText
                    cm.CommandType = CommandType.Text
                    da.SelectCommand = cm
                    da.Fill(dt)
                End If
            End If
            Return dt
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Obtenemos la información de si el usuario tiene permiso unicamente a un centro de coste o varios
    ''' </summary>
    ''' <param name="sUsu">Código del usuario que quiere obtener las partidas presupuestarias</param>
    ''' <param name="sIdioma">Idioma de los centros de coste</param>
    ''' <returns>Array de string(2)</returns>
    ''' <remarks></remarks>
    Public Function Centro_SM_DevolverCCUnico(ByVal sUsu As String, ByVal sIdioma As String) As String()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter
        Dim sResultado(2) As String
        Dim sUON1 As String = ""
        Dim sUON2 As String = ""
        Dim sUON3 As String = ""
        Dim sUON4 As String = ""
        Dim bContinuar As Boolean = False
        Try
            Dim sqlText As String = Nothing

            sqlText = "SELECT UON1, UON2, UON3, UON4" & _
               " FROM USU_CC_CONTROL  WITH (NOLOCK)" & _
               " WHERE USU = '{0}'"

            sqlText = String.Format(sqlText, sUsu)

            cm.Connection = cn
            cm.CommandText = sqlText
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dt)
            If dt.Rows.Count = 1 Then
                sUON1 = DBNullToStr(dt.Rows(0).Item("UON1"))
                sUON2 = DBNullToStr(dt.Rows(0).Item("UON2"))
                sUON3 = DBNullToStr(dt.Rows(0).Item("UON3"))
                sUON4 = DBNullToStr(dt.Rows(0).Item("UON4"))
                bContinuar = True

            End If

            If bContinuar Then
                dt = New DataTable
                If sUON4 <> "" Then
                    sResultado(0) = sUON4
                    sqlText = "SELECT U.DEN" & _
                       " FROM UON4 U WITH (NOLOCK)" & _
                       " WHERE U.UON1 = '{0}' AND U.UON2 = '{1}' AND U.UON3 = '{2}' AND U.COD = '{3}'"
                    sqlText = String.Format(sqlText, sUON1, sUON2, sUON3, sUON4)
                ElseIf sUON3 <> "" Then
                    sResultado(0) = sUON3
                    sqlText = "SELECT U.DEN" & _
                         " FROM UON3 U WITH (NOLOCK)" & _
                         " WHERE U.UON1 = '{0}' AND U.UON2 = '{1}' AND U.COD = '{2}'"
                    sqlText = String.Format(sqlText, sUON1, sUON2, sUON3)
                ElseIf sUON2 <> "" Then
                    sResultado(0) = sUON2
                    sqlText = "SELECT U.DEN" & _
                       " FROM UON2 U WITH (NOLOCK)" & _
                       " WHERE U.UON1 = '{0}' AND U.COD = '{1}'"
                    sqlText = String.Format(sqlText, sUON1, sUON2)
                Else
                    sResultado(0) = sUON1
                    sqlText = "SELECT U.DEN" & _
                       " FROM UON1 U WITH (NOLOCK)" & _
                       " WHERE U.COD = '{0}'"
                    sqlText = String.Format(sqlText, sUON1)

                End If

                cm.Connection = cn
                cm.CommandText = sqlText
                cm.CommandType = CommandType.Text
                da.SelectCommand = cm
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    sResultado(1) = dt.Rows(0).Item(0)
                End If
                sResultado(2) = sUON1 & "#" & sUON2 & "#" & sUON3 & "#" & sUON4
            End If

            Return sResultado
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Obtiene los permisos del usuario (alta, modificación y presupuestación) sobre el centro de coste seleccionado
    ''' </summary>
    ''' <param name="sUON1">Código UON1 del centro de coste</param>
    ''' <param name="sUON2">Código UON1 del centro de coste</param>
    ''' <param name="sUON3">Código UON1 del centro de coste</param>
    ''' <param name="sUON4">Código UON1 del centro de coste</param>
    ''' <param name="sUsu">Código del usuario</param>
    ''' <returns>Devuelve un datarow con los permisos</returns>
    ''' <remarks></remarks>
    Public Function Centro_SM_DevolverPermisos(ByVal sUON1 As String, ByVal sUON2 As String, ByVal sUON3 As String, ByVal sUON4 As String, ByVal sUsu As String) As DataRow
        Dim SqlCn As New SqlConnection(mDBConnection)
        Dim SqlCmd As New SqlCommand
        Dim drResul As New DataSet
        Dim Da As New SqlDataAdapter
        Try
            SqlCn.Open()

            SqlCmd = New SqlCommand
            With SqlCmd
                .Connection = SqlCn
                .CommandText = "FSSM_GET_PERMISOS_CENTROCOSTE"
                .Parameters.AddWithValue("@UON1", IIf(sUON1 = "", Nothing, sUON1))
                .Parameters.AddWithValue("@UON2", IIf(sUON2 = "", Nothing, sUON2))
                .Parameters.AddWithValue("@UON3", IIf(sUON3 = "", Nothing, sUON3))
                .Parameters.AddWithValue("@UON4", IIf(sUON4 = "", Nothing, sUON4))
                .Parameters.AddWithValue("@USU", sUsu)
                .CommandType = CommandType.StoredProcedure
            End With

            Da.SelectCommand = SqlCmd
            Da.Fill(drResul)

            Return drResul.Tables(0).Rows(0)
        Catch e As Exception
            Throw e
        Finally
            SqlCn.Dispose()
            SqlCmd.Dispose()
            Da.Dispose()
        End Try
    End Function
    ''' Revisado por: blp. Fecha: 29/01/2013
    ''' <summary>
    ''' Devuelve los centros presentes en los pedidos que un usuario puede consultar en la pantalla de seguimiento de EP
    ''' </summary>
    ''' <param name="codPer">Código del usuario</param>
    ''' <param name="idioma">Idioma</param>
    ''' <param name="VerPedidosOtrosUsuarios">Permiso del usuario para ver pedidos de otros usuarios</param>
    ''' <returns>Dataset con los centros</returns>
    ''' <remarks>Llamada desde SM\Centro_SM.vb->DevolverCentrosPedidos. Máximo 1,5 seg.</remarks>
    Public Function Centros_DevolverCentrosPedidos(ByVal codPer As String, ByVal idioma As FSNLibrary.Idioma, ByVal VerPedidosOtrosUsuarios As Boolean) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "EP_OBT_CENTROS_SEGUIMIENTO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@PER", codPer)
            cm.Parameters.AddWithValue("@IDIOMA", idioma.ToString)
            cm.Parameters.AddWithValue("@PERMISO_VER_PEDIDOS_OTROS_USUARIOS", VerPedidosOtrosUsuarios)
            da.SelectCommand = cm
            da.Fill(ds)
            Return ds
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' Revisado por: blp. Fecha: 28/08/2013
    ''' <summary>
    ''' Devuelve todos los centro
    ''' </summary>
    ''' <param name="idioma">Idioma</param>
    ''' <returns>Dataset con los centros</returns>
    ''' <remarks>Llamada desde Centro_SM.vb. Maximo 0,5 seg.</remarks>
    Public Function Centros_DevolverCentrosPM(ByVal idioma As FSNLibrary.Idioma) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "PM_OBT_CENTROS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDIOMA", idioma.ToString)
            da.SelectCommand = cm
            da.Fill(ds)
            Return ds
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' Revisado por: blp. Fecha: 28/08/2013
    ''' <summary>
    ''' Devuelve los datos del centro solicitado
    ''' </summary>
    ''' <param name="centrocoste">CÓDIGO DE UONS DEL CENTRO DE COSTE: UON1#UON2#UON3#UON4</param>
    ''' <param name="idioma">Idioma</param>
    ''' <returns>Dataset con los datos del centro</returns>
    ''' <remarks>Llamada desde Centro_SM.vb. Maximo 0,5 seg.</remarks>
    Public Function Centros_DevolverCentroPM(ByVal centroCoste As String, ByVal idioma As FSNLibrary.Idioma) As DataTable
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "PM_OBT_CENTRO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@CENTROCOSTE", centroCoste)
            cm.Parameters.AddWithValue("@IDIOMA", idioma.ToString)
            da.SelectCommand = cm
            da.Fill(ds)
            If ds IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0)
            Else
                Return Nothing
            End If
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
#End Region
#Region "PRES5 data access methods"
    ''' <summary>
    ''' Procedimiento que carga los datos de un concepto presupuestario
    ''' </summary>
    ''' <param name="sPRES0">Código de concepto presupuestario, nivel 0</param>
    ''' <returns>Un DataSet con los datos del los conceptos presupuestarios de nivel 0</returns>
    ''' <remarks>
    ''' Llamada desde: SMServer/Pres5/Load
    ''' tiempo máximo: 0,2 seg</remarks>
    Public Function PRES5_LoadData(ByVal sPRES0 As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "FSSM_DEVOLVER_PRES5"
            cm.Parameters.AddWithValue("@PRES0", sPRES0)

            Dim da As New SqlDataAdapter
            da.SelectCommand = cm
            da.Fill(ds)

            'RELATIONS
            Dim parentCols(0) As DataColumn
            Dim childCols(0) As DataColumn
            parentCols(0) = ds.Tables(0).Columns("COD")
            childCols(0) = ds.Tables(1).Columns("PRES0")
            ds.Relations.Add("REL_NIV0_NIV1", parentCols, childCols, False)

            ReDim parentCols(1)
            ReDim childCols(1)

            parentCols(0) = ds.Tables(1).Columns("PRES0")
            parentCols(1) = ds.Tables(1).Columns("COD")
            childCols(0) = ds.Tables(2).Columns("PRES0")
            childCols(1) = ds.Tables(2).Columns("PRES1")
            ds.Relations.Add("REL_NIV1_NIV2", parentCols, childCols, False)

            ReDim parentCols(2)
            ReDim childCols(2)
            parentCols(0) = ds.Tables(2).Columns("PRES0")
            parentCols(1) = ds.Tables(2).Columns("PRES1")
            parentCols(2) = ds.Tables(2).Columns("COD")
            childCols(0) = ds.Tables(3).Columns("PRES0")
            childCols(1) = ds.Tables(3).Columns("PRES1")
            childCols(2) = ds.Tables(3).Columns("PRES2")

            ds.Relations.Add("REL_NIV2_NIV3", parentCols, childCols, False)

            ReDim parentCols(3)
            ReDim childCols(3)
            parentCols(0) = ds.Tables(3).Columns("PRES0")
            parentCols(1) = ds.Tables(3).Columns("PRES1")
            parentCols(2) = ds.Tables(3).Columns("PRES2")
            parentCols(3) = ds.Tables(3).Columns("COD")
            childCols(0) = ds.Tables(4).Columns("PRES0")
            childCols(1) = ds.Tables(4).Columns("PRES1")
            childCols(2) = ds.Tables(4).Columns("PRES2")
            childCols(3) = ds.Tables(4).Columns("PRES3")
            ds.Relations.Add("REL_NIV3_NIV4", parentCols, childCols, False)

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
        Return ds
    End Function
    ''' <summary>
    ''' Devuelve las partidas y las uons para mostrarlas en el autocompletar del buscador de partidas. 
    ''' </summary>
    ''' <param name="sPRES5">Código de la partida presupuetaria</param>
    ''' <param name="sUsuario">Código de la persona</param>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <param name="sCentroCoste">Código del centro de coste del que se quieren mostrar las partidas</param>
    ''' <returns>Devuelve un dataset</returns>
    ''' <remarks>Llamada desde: SMServer --> PRES5.vb --> Partidas_LoadAutoCompletar; Tiempo máximo: 1 sg;</remarks>
    Public Function PRES5_LoadAutoCompletar(ByVal sPRES5 As String, ByVal sUsuario As String, ByVal sIdioma As String, Optional ByVal sCentroCoste As String = Nothing)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn
            cm.CommandText = "FSSM_AUTOCOMPLETAR_CC_PP"
            cm.Parameters.AddWithValue("@PRES5", sPRES5)
            cm.Parameters.AddWithValue("@IDI", sIdioma)
            cm.Parameters.AddWithValue("@USU", sUsuario)
            If sCentroCoste <> Nothing Then
                Dim sUONs As Object = Split(sCentroCoste, "#")
                cm.Parameters.AddWithValue("@UON1", sUONs(0))
                If UBound(sUONs) >= 1 Then
                    cm.Parameters.AddWithValue("@UON2", sUONs(1))
                    If UBound(sUONs) >= 2 Then
                        cm.Parameters.AddWithValue("@UON3", sUONs(2))
                        If UBound(sUONs) = 3 Then
                            cm.Parameters.AddWithValue("@UON4", sUONs(3))
                        End If
                    End If
                End If
            End If
            cm.CommandType = CommandType.StoredProcedure

            da.SelectCommand = cm
            da.Fill(dr)

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
	''' <summary>
	''' Devuelve las partidas y las uons para mostrarlas en un treeview. Añade las relaciones a las tablas del dataset.
	''' </summary>
	''' <param name="sPRES5">Código de la partida presupuetaria</param>
	''' <param name="sUsuario">Código de la persona</param>
	''' <param name="sIdioma">Idioma del usuario</param>
	''' <param name="sCentroCoste">Código del centro de coste del que se quieren mostrar las partidas</param>
	''' <param name="bVigentes">Si true, muestra las partidas vigentes, si false, todas las partidas</param>
	''' <param name="dFechaInicioDesde">Fecha de inicio desde para buscar partidas</param>
	''' <param name="dFechaFinHasta">Fecha fin hasta para buscar partidas</param>
	''' <param name="sFormatoFecha">Formato en el que llegan las fechas</param>
	''' <returns>Devuelve un dataset</returns>
	''' <remarks>Llamada desde: SMServer --> PRES5.vb --> Partidas_LoadData; Tiempo máximo: 1 sg;</remarks>
	Public Function Partidas_Load(ByVal sPRES5 As String, ByVal sUsuario As String, ByVal sIdioma As String, ByVal bPlurianual As Boolean, Optional ByVal sCentroCoste As String = Nothing, Optional ByVal bVigentes As Boolean = True, Optional ByVal dFechaInicioDesde As Date = Nothing, Optional ByVal dFechaFinHasta As Date = Nothing, Optional ByVal sFormatoFecha As String = Nothing)
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand()
		Dim dr As New DataSet
		Dim da As New SqlDataAdapter
		Dim index As Integer = 1
		Try
			cn.Open()
			cm = New SqlCommand

			cm.Connection = cn
			cm.CommandText = "FSSM_DEVOLVER_PARTIDAS"

			cm.Parameters.AddWithValue("@PRES5", sPRES5)
			cm.Parameters.AddWithValue("@IDI", sIdioma)
			cm.Parameters.AddWithValue("@USU", sUsuario)
			If sCentroCoste <> Nothing Then
				Dim sUONs As Object = Split(sCentroCoste, "#")
				cm.Parameters.AddWithValue("@UON1", sUONs(0))
				If UBound(sUONs) >= 1 Then
					cm.Parameters.AddWithValue("@UON2", sUONs(1))
					If UBound(sUONs) >= 2 Then
						cm.Parameters.AddWithValue("@UON3", sUONs(2))
						If UBound(sUONs) = 3 Then
							cm.Parameters.AddWithValue("@UON4", sUONs(3))
						End If
					End If
				End If
			End If
			If bPlurianual Then bVigentes = False
			cm.Parameters.AddWithValue("@VIGENTES", bVigentes)
			If dFechaInicioDesde <> Nothing Then
				cm.Parameters.AddWithValue("@FECHAINICIODESDE", dFechaInicioDesde)
			End If
			If dFechaFinHasta <> Nothing Then
				cm.Parameters.AddWithValue("@FECHAFINHASTA", dFechaFinHasta)
			End If
			cm.Parameters.AddWithValue("@FORMATOFECHA", sFormatoFecha)
			cm.Parameters.AddWithValue("@PLURIANUAL", bPlurianual)
			cm.CommandType = CommandType.StoredProcedure

			da.SelectCommand = cm
			da.Fill(dr)

			'******************************************************************
			Dim parentCols(0) As DataColumn
			Dim childCols(0) As DataColumn

			parentCols(0) = dr.Tables(0).Columns("COD0")
			childCols(0) = dr.Tables(1).Columns("COD0")

			dr.Relations.Add("RELACION_1", parentCols, childCols, False)

			If dr.Tables(2).Rows.Count > 0 Then
				ReDim parentCols(0)
				ReDim childCols(0)
				index = 2

				parentCols(0) = dr.Tables(1).Columns("COD")
				childCols(0) = dr.Tables(2).Columns("UON1")

				dr.Relations.Add("RELACION_2", parentCols, childCols, False)

				If dr.Tables(3).Rows.Count > 0 Then

					ReDim parentCols(1)
					ReDim childCols(1)
					index = 3

					parentCols(0) = dr.Tables(2).Columns("UON1")
					parentCols(1) = dr.Tables(2).Columns("COD")
					childCols(0) = dr.Tables(3).Columns("UON1")
					childCols(1) = dr.Tables(3).Columns("UON2")
					dr.Relations.Add("RELACION_3", parentCols, childCols, False)

					If dr.Tables(4).Rows.Count > 0 Then
						ReDim parentCols(2)
						ReDim childCols(2)
						index = 4

						parentCols(0) = dr.Tables(3).Columns("UON1")
						parentCols(1) = dr.Tables(3).Columns("UON2")
						parentCols(2) = dr.Tables(3).Columns("COD")
						childCols(0) = dr.Tables(4).Columns("UON1")
						childCols(1) = dr.Tables(4).Columns("UON2")
						childCols(2) = dr.Tables(4).Columns("UON3")
						dr.Relations.Add("RELACION_4", parentCols, childCols, False)
					End If
				End If
			End If
			ReDim parentCols(3)
			ReDim childCols(3)

			parentCols(0) = dr.Tables(index).Columns("UON1")
			parentCols(1) = dr.Tables(index).Columns("UON2")
			parentCols(2) = dr.Tables(index).Columns("UON3")
			parentCols(3) = dr.Tables(index).Columns("UON4")
			childCols(0) = dr.Tables(5).Columns("UON1")
			childCols(1) = dr.Tables(5).Columns("UON2")
			childCols(2) = dr.Tables(5).Columns("UON3")
			childCols(3) = dr.Tables(5).Columns("UON4")
			dr.Relations.Add("RELACION_" & index + 1, parentCols, childCols, False)

			Return dr
		Catch e As Exception
			Throw e
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Function
	''' <summary>
	''' Devuelve las partidas y las uons para mostrarlas en un treeview. Añade las relaciones a las tablas del dataset.
	''' </summary>
	''' <param name="sPRES5">Código de la partida presupuetaria</param>
	''' <param name="sUsuario">Código de la persona</param>
	''' <param name="sIdioma">Idioma del usuario</param>
	''' <param name="sCentroCoste">Código del centro de coste del que se quieren mostrar las partidas</param>
	''' <param name="bVigentes">Si true, muestra las partidas vigentes, si false, todas las partidas</param>
	''' <param name="dFechaInicioDesde">Fecha de inicio desde para buscar partidas</param>
	''' <param name="dFechaFinHasta">Fecha fin hasta para buscar partidas</param>
	''' <param name="sFormatoFecha">Formato en el que llegan las fechas</param>
	''' <returns>Devuelve un dataset</returns>
	''' <remarks>Llamada desde: SMServer --> PRES5.vb --> Partidas_LoadData; Tiempo máximo: 1 sg;</remarks>
	Public Function Partidas_LoadEnIM(ByVal sPRES5 As String, ByVal sUsuario As String, ByVal sIdioma As String, ByVal bPlurianual As Boolean, Optional ByVal sCentroCoste As String = Nothing, Optional ByVal bVigentes As Boolean = True, Optional ByVal dFechaInicioDesde As Date = Nothing, Optional ByVal dFechaFinHasta As Date = Nothing, Optional ByVal sFormatoFecha As String = Nothing)
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As SqlCommand = New SqlCommand()
		Dim dr As New DataSet
		Dim da As New SqlDataAdapter
		Dim index As Integer = 1
		Try
			cn.Open()
			cm = New SqlCommand

			cm.Connection = cn
			cm.CommandText = "FSSM_DEVOLVER_PARTIDAS"
			cm.Parameters.AddWithValue("@PRES5", sPRES5)
			cm.Parameters.AddWithValue("@IDI", sIdioma)
			cm.Parameters.AddWithValue("@USU", sUsuario)

			If sCentroCoste <> Nothing Then
				Dim sUONs As Object = Split(sCentroCoste, "#")
				cm.Parameters.AddWithValue("@UON1", sUONs(0))
				If UBound(sUONs) >= 1 Then
					cm.Parameters.AddWithValue("@UON2", sUONs(1))
					If UBound(sUONs) >= 2 Then
						cm.Parameters.AddWithValue("@UON3", sUONs(2))
						If UBound(sUONs) = 3 Then
							cm.Parameters.AddWithValue("@UON4", sUONs(3))
						End If
					End If
				End If
			End If
			cm.Parameters.AddWithValue("@VIGENTES", bVigentes)
			If dFechaInicioDesde <> Nothing Then
				cm.Parameters.AddWithValue("@FECHAINICIODESDE", dFechaInicioDesde)
			End If
			If dFechaFinHasta <> Nothing Then
				cm.Parameters.AddWithValue("@FECHAFINHASTA", dFechaFinHasta)
			End If
			cm.Parameters.AddWithValue("@FORMATOFECHA", sFormatoFecha)
			cm.Parameters.AddWithValue("@PLURIANUAL", bPlurianual)
			cm.CommandType = CommandType.StoredProcedure

			da.SelectCommand = cm
			da.Fill(dr)

			Dim i As Byte
			For Each dt As DataTable In dr.Tables
				dt.TableName = "Tabla_" & i.ToString
				i += 1
			Next

			'******************************************************************
			Dim parentCols(0) As DataColumn
			Dim childCols(0) As DataColumn
			Dim dtPk0(0) As DataColumn
			Dim dtPk1(1) As DataColumn
			Dim dtPk2(2) As DataColumn
			Dim dtPk3(3) As DataColumn
			Dim dtPk4(4) As DataColumn
			Dim dtPk5(5) As DataColumn

			'Establezco las claves primarias de las tablas de las UONS
			dtPk0(0) = dr.Tables(0).Columns("COD0")
			dr.Tables(0).PrimaryKey = dtPk0
			dtPk1(0) = dr.Tables(1).Columns("COD0")
			dtPk1(1) = dr.Tables(1).Columns("COD")
			dr.Tables(1).PrimaryKey = dtPk1
			dtPk2(0) = dr.Tables(2).Columns("COD0")
			dtPk2(1) = dr.Tables(2).Columns("UON1")
			dtPk2(2) = dr.Tables(2).Columns("COD")
			dr.Tables(2).PrimaryKey = dtPk2
			dtPk3(0) = dr.Tables(3).Columns("COD0")
			dtPk3(1) = dr.Tables(3).Columns("UON1")
			dtPk3(2) = dr.Tables(3).Columns("UON2")
			dtPk3(3) = dr.Tables(3).Columns("COD")
			dr.Tables(3).PrimaryKey = dtPk3
			dtPk4(0) = dr.Tables(4).Columns("COD0")
			dtPk4(1) = dr.Tables(4).Columns("UON1")
			dtPk4(2) = dr.Tables(4).Columns("UON2")
            dtPk4(3) = dr.Tables(4).Columns("UON3")
            dtPk4(4) = dr.Tables(4).Columns("COD")
            dr.Tables(4).PrimaryKey = dtPk4
            dtPk5(0) = dr.Tables(5).Columns("COD0")
            dtPk5(1) = dr.Tables(5).Columns("UON1")
            dtPk5(2) = dr.Tables(5).Columns("UON2")
            dtPk5(3) = dr.Tables(5).Columns("UON3")
            dtPk5(4) = dr.Tables(5).Columns("UON4")
            dtPk5(5) = dr.Tables(5).Columns("COD")
            dr.Tables(5).PrimaryKey = dtPk5

            parentCols(0) = dr.Tables(0).Columns("COD0")
            childCols(0) = dr.Tables(1).Columns("COD0")

            dr.Relations.Add("RELACION_1", parentCols, childCols, False)

            If dr.Tables(2).Rows.Count > 0 Then
                ReDim parentCols(0)
                ReDim childCols(0)
                index = 2

                parentCols(0) = dr.Tables(1).Columns("COD")
                childCols(0) = dr.Tables(2).Columns("UON1")

                dr.Relations.Add("RELACION_2", parentCols, childCols, False)

                If dr.Tables(3).Rows.Count > 0 Then

                    ReDim parentCols(1)
                    ReDim childCols(1)
                    index = 3

                    parentCols(0) = dr.Tables(2).Columns("UON1")
                    parentCols(1) = dr.Tables(2).Columns("COD")
                    childCols(0) = dr.Tables(3).Columns("UON1")
                    childCols(1) = dr.Tables(3).Columns("UON2")
                    dr.Relations.Add("RELACION_3", parentCols, childCols, False)

                    If dr.Tables(4).Rows.Count > 0 Then
                        ReDim parentCols(2)
                        ReDim childCols(2)
                        index = 4

                        parentCols(0) = dr.Tables(3).Columns("UON1")
                        parentCols(1) = dr.Tables(3).Columns("UON2")
                        parentCols(2) = dr.Tables(3).Columns("COD")
                        childCols(0) = dr.Tables(4).Columns("UON1")
                        childCols(1) = dr.Tables(4).Columns("UON2")
                        childCols(2) = dr.Tables(4).Columns("UON3")
                        dr.Relations.Add("RELACION_4", parentCols, childCols, False)
                    End If
                End If
            End If
            ReDim parentCols(3)
            ReDim childCols(3)

            parentCols(0) = dr.Tables(index).Columns("UON1")
            parentCols(1) = dr.Tables(index).Columns("UON2")
            parentCols(2) = dr.Tables(index).Columns("UON3")
            parentCols(3) = dr.Tables(index).Columns("UON4")
            childCols(0) = dr.Tables(5).Columns("UON1")
            childCols(1) = dr.Tables(5).Columns("UON2")
            childCols(2) = dr.Tables(5).Columns("UON3")
            childCols(3) = dr.Tables(5).Columns("UON4")
            dr.Relations.Add("RELACION_" & index + 1, parentCols, childCols, False)

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Obtiene un arbol jerarquico de las partidas presupuestarias
    ''' </summary>
    ''' <param name="sUON1">Código UON1 del centro de coste</param>
    ''' <param name="sUON2">Código UON1 del centro de coste</param>
    ''' <param name="sUON3">Código UON1 del centro de coste</param>
    ''' <param name="sUON4">Código UON1 del centro de coste</param>
    ''' <param name="sUsu">Código del usuario</param>
    ''' <param name="sIdioma">Idioma para las denominaciones de las partidas presupuestarias</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Partidas_ArbolJerarquico(ByVal sUON1 As String, ByVal sUON2 As String, _
               ByVal sUON3 As String, ByVal sUON4 As String, _
               ByVal sUsu As String, ByVal sIdioma As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm = New SqlCommand
			cm.CommandTimeout = 300
			cm.Connection = cn
			cm.CommandText = "FSSM_GET_ARBOLPRESUPUESTARIO"
			cm.Parameters.AddWithValue("@UON1", IIf(sUON1 = "", Nothing, sUON1))
            cm.Parameters.AddWithValue("@UON2", IIf(sUON2 = "", Nothing, sUON2))
            cm.Parameters.AddWithValue("@UON3", IIf(sUON3 = "", Nothing, sUON3))
            cm.Parameters.AddWithValue("@UON4", IIf(sUON4 = "", Nothing, sUON4))
            cm.Parameters.AddWithValue("@USU", sUsu)
            cm.Parameters.AddWithValue("@IDI", sIdioma)
            cm.CommandType = CommandType.StoredProcedure

            da.SelectCommand = cm
            da.Fill(dr)
			'******************************************************************
			Dim parentCols(0) As DataColumn
			Dim childCols(0) As DataColumn

			parentCols(0) = dr.Tables(0).Columns("PRES0")
			childCols(0) = dr.Tables(1).Columns("PRES0")
			dr.Relations.Add("RELACION_1", parentCols, childCols, False)

			ReDim parentCols(1)
			ReDim childCols(1)

			parentCols(0) = dr.Tables(1).Columns("PRES0")
			parentCols(1) = dr.Tables(1).Columns("PRES1")
			childCols(0) = dr.Tables(2).Columns("PRES0")
			childCols(1) = dr.Tables(2).Columns("PRES1")
			dr.Relations.Add("RELACION_2", parentCols, childCols, False)

			'ReDim parentCols(0)
			'ReDim childCols(0)
			'parentCols(0) = dr.Tables(1).Columns("ID")
			'childCols(0) = dr.Tables(5).Columns("PRES5_IMP")
			'dr.Relations.Add("RELACION_21", parentCols, childCols, False)

			ReDim parentCols(2)
			ReDim childCols(2)

			parentCols(0) = dr.Tables(2).Columns("PRES0")
			parentCols(1) = dr.Tables(2).Columns("PRES1")
			parentCols(2) = dr.Tables(2).Columns("PRES2")
			childCols(0) = dr.Tables(3).Columns("PRES0")
			childCols(1) = dr.Tables(3).Columns("PRES1")
			childCols(2) = dr.Tables(3).Columns("PRES2")
			dr.Relations.Add("RELACION_3", parentCols, childCols, False)

			ReDim parentCols(3)
			ReDim childCols(3)

			parentCols(0) = dr.Tables(3).Columns("PRES0")
			parentCols(1) = dr.Tables(3).Columns("PRES1")
			parentCols(2) = dr.Tables(3).Columns("PRES2")
			parentCols(3) = dr.Tables(3).Columns("PRES3")
			childCols(0) = dr.Tables(4).Columns("PRES0")
			childCols(1) = dr.Tables(4).Columns("PRES1")
			childCols(2) = dr.Tables(4).Columns("PRES2")
			childCols(3) = dr.Tables(4).Columns("PRES3")
			dr.Relations.Add("RELACION_4", parentCols, childCols, False)

			With dr
                .Tables(0).TableName = "PRES5_NIV0"
                .Tables(1).TableName = "PRES5_NIV1"
                .Tables(2).TableName = "PRES5_NIV2"
                .Tables(3).TableName = "PRES5_NIV3"
                .Tables(4).TableName = "PRES5_NIV4"
            End With

            dr.Tables.Add("UONS")
            With dr.Tables("UONS")
                .Columns.Add("UON1", System.Type.GetType("System.String"))
                .Columns.Add("UON2", System.Type.GetType("System.String"))
                .Columns.Add("UON3", System.Type.GetType("System.String"))
                .Columns.Add("UON4", System.Type.GetType("System.String"))

                Dim row As DataRow = .NewRow
                row("UON1") = sUON1
                row("UON2") = sUON2
                row("UON3") = sUON3
                row("UON4") = sUON4

                dr.Tables("UONS").Rows.Add(row)
            End With

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Devuelve los centros y partidas con presupuestos abiertos
    ''' </summary>
    ''' <param name="Usuario">Código de usuario</param>
    ''' <param name="Idioma">Tipo FSNLibrary.Idioma con el idioma para las denominaciones</param>
    ''' <param name="Pres5">Código PRES0 del árbol de partidas presupuestarias</param>
    ''' <returns>Un DataTable con los centros y partidas con presupuestos abiertos</returns>
    Public Function PRES5_PartidasImputacion(ByVal Usuario As String, ByVal Idioma As FSNLibrary.Idioma, _
                    ByVal Pres5 As String, ByVal PedidosOtrasEmp As Boolean, _
                    Optional ByVal IdEmpresa As Integer = Nothing) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim ds As New DataSet

        Using cn
            cn.Open()

            cm = New SqlCommand
            cm.Connection = cn

            cm.CommandText = "FSSM_PARTIDAS_USU"
            cm.CommandType = CommandType.StoredProcedure

            cm.Parameters.Add(New SqlParameter("@USUARIO", SqlDbType.VarChar, 20))
            cm.Parameters("@USUARIO").Value = Usuario
            cm.Parameters.Add(New SqlParameter("@IDIOMA", SqlDbType.VarChar, 3))
            cm.Parameters("@IDIOMA").Value = Idioma.ToString()
            If Not String.IsNullOrEmpty(Pres5) Then
                cm.Parameters.Add(New SqlParameter("@PRES5", SqlDbType.VarChar, 50))
                cm.Parameters("@PRES5").Value = Pres5
            End If
            cm.Parameters.Add(New SqlParameter("@EMP", SqlDbType.Int))
            cm.Parameters("@EMP").Value = IdEmpresa
            cm.Parameters.AddWithValue("@PED_OTRAS_EMPRESAS", PedidosOtrasEmp)

            Dim da As New SqlDataAdapter
            da.SelectCommand = cm
            da.Fill(ds)
            Return ds.Tables(0)
        End Using
    End Function
    ''' <summary>
    ''' Función que devuelve los datos correspondientes a una partida dada, para un centro de coste específico
    ''' </summary>
    ''' <param name="idioma">idioma en que se quiere recuperar la información</param>
    ''' <param name="PRES0">Código del árbol presupuestario del que se quiere recuperar la partida (PRES5 en otros sitios)</param>
    ''' <param name="PRES1">Código de la partida de nivel 1</param>
    ''' <param name="PRES2">Código de la partida de nivel 2 (si la hay)</param>
    ''' <param name="PRES3">Código de la partida de nivel 3 (si la hay)</param>
    ''' <param name="PRES4">Código de la partida de nivel 4 (si la hay)</param>
    ''' <param name="CENTROSM">Código del centro de coste</param>
    ''' <returns>Datatable con la información</returns>
    ''' <remarks>Llamada desde SMServer\PartidaPRES5.vb -> Datos_Partida. Tiempo máximo inferior a 1 seg.</remarks>
    Public Function PRES5_Datos_Partida(ByVal idioma As FSNLibrary.Idioma, ByVal PRES0 As String, ByVal PRES1 As String, ByVal PRES2 As String, ByVal PRES3 As String, ByVal PRES4 As String, ByVal CENTROSM As String) As DataTable
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim ds As New DataSet

        Using cn
            cn.Open()

            cm = New SqlCommand
            cm.Connection = cn

            cm.CommandText = "FSSM_DEVOLVER_PARTIDA"
            cm.CommandType = CommandType.StoredProcedure

            cm.Parameters.Add(New SqlParameter("@IDIOMA", SqlDbType.VarChar, 3))
            If Not String.IsNullOrEmpty(idioma) Then
                cm.Parameters("@IDIOMA").Value = idioma.ToString()
            Else
                cm.Parameters("@IDIOMA").Value = "SPA"
            End If

            cm.Parameters.Add(New SqlParameter("@PRES0", SqlDbType.VarChar, 100))
            If Not String.IsNullOrEmpty(PRES0) Then
                cm.Parameters("@PRES0").Value = PRES0
            Else
                cm.Parameters("@PRES0").Value = System.DBNull.Value
            End If

            cm.Parameters.Add(New SqlParameter("@PRES1", SqlDbType.VarChar, 100))
            If Not String.IsNullOrEmpty(PRES1) Then
                cm.Parameters("@PRES1").Value = PRES1
            Else
                cm.Parameters("@PRES1").Value = System.DBNull.Value
            End If

            cm.Parameters.Add(New SqlParameter("@PRES2", SqlDbType.VarChar, 100))
            If Not String.IsNullOrEmpty(PRES2) Then
                cm.Parameters("@PRES2").Value = PRES2
            Else
                cm.Parameters("@PRES2").Value = System.DBNull.Value
            End If

            cm.Parameters.Add(New SqlParameter("@PRES3", SqlDbType.VarChar, 100))
            If Not String.IsNullOrEmpty(PRES3) Then
                cm.Parameters("@PRES3").Value = PRES3
            Else
                cm.Parameters("@PRES3").Value = System.DBNull.Value
            End If

            cm.Parameters.Add(New SqlParameter("@PRES4", SqlDbType.VarChar, 100))
            If Not String.IsNullOrEmpty(PRES4) Then
                cm.Parameters("@PRES4").Value = PRES4
            Else
                cm.Parameters("@PRES4").Value = System.DBNull.Value
            End If

            cm.Parameters.Add(New SqlParameter("@CENTROSM", SqlDbType.VarChar, 40))
            If Not String.IsNullOrEmpty(CENTROSM) Then
                cm.Parameters("@CENTROSM").Value = CENTROSM
            Else
                cm.Parameters("@CENTROSM").Value = System.DBNull.Value
            End If

            Dim da As New SqlDataAdapter
            da.SelectCommand = cm
            da.Fill(ds)
            Return ds.Tables(0)
        End Using
    End Function
    ''' <summary>
    ''' Obtiene los datos de denominación, código y presupuestación de una partida presupuestaria
    ''' </summary>
    ''' <param name="PRES0">Código PRES0 de la partida presupuestaria</param>
    ''' <param name="PRES1">Código PRES1 de la partida presupuestaria</param>
    ''' <param name="PRES2">Código PRES2 de la partida presupuestaria</param>
    ''' <param name="PRES3">Código PRES3 de la partida presupuestaria</param>
    ''' <param name="PRES4">Código PRES4 de la partida presupuestaria</param>
    ''' <param name="ObtenerMonedas">Si se obtiene un listado con las monedas o no para rellenar el combo de monedas</param>
    ''' <returns>Dataset con denominaciones, datos y presupuestación. Si es necesario otra tabla con las monedas</returns>
    ''' <remarks></remarks>
    Public Function Partida_PRES5_DevolverDatosPartidaPRES5(ByVal PRES0 As String, ByVal PRES1 As String, _
          ByVal PRES2 As String, ByVal PRES3 As String, ByVal PRES4 As String, _
          ByVal ObtenerMonedas As Boolean) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSSM_GET_PARTIDAPRESUPUESTARIA"
            cm.CommandType = CommandType.StoredProcedure

            cm.Parameters.Add(New SqlParameter("@PRES0", SqlDbType.NVarChar, 100))
            If Not String.IsNullOrEmpty(PRES0) Then
                cm.Parameters("@PRES0").Value = PRES0
            Else
                cm.Parameters("@PRES0").Value = System.DBNull.Value
            End If

            cm.Parameters.Add(New SqlParameter("@PRES1", SqlDbType.NVarChar, 100))
            If Not String.IsNullOrEmpty(PRES1) Then
                cm.Parameters("@PRES1").Value = PRES1
            Else
                cm.Parameters("@PRES1").Value = System.DBNull.Value
            End If

            cm.Parameters.Add(New SqlParameter("@PRES2", SqlDbType.NVarChar, 100))
            If Not String.IsNullOrEmpty(PRES2) Then
                cm.Parameters("@PRES2").Value = PRES2
            Else
                cm.Parameters("@PRES2").Value = System.DBNull.Value
            End If

            cm.Parameters.Add(New SqlParameter("@PRES3", SqlDbType.NVarChar, 100))
            If Not String.IsNullOrEmpty(PRES3) Then
                cm.Parameters("@PRES3").Value = PRES3
            Else
                cm.Parameters("@PRES3").Value = System.DBNull.Value
            End If

            cm.Parameters.Add(New SqlParameter("@PRES4", SqlDbType.NVarChar, 100))
            If Not String.IsNullOrEmpty(PRES4) Then
                cm.Parameters("@PRES4").Value = PRES4
            Else
                cm.Parameters("@PRES4").Value = System.DBNull.Value
            End If

            cm.Parameters.AddWithValue("@OBTENERMONEDAS", ObtenerMonedas)

            da.SelectCommand = cm
            da.Fill(ds)

            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Obtiene todas las posibles partidas para un centro de coste que se cargarán para el Autocomplete
    ''' </summary>
    ''' <param name="sUON1">Código UON1 del centro de coste</param>
    ''' <param name="sUON2">Código UON2 del centro de coste</param>
    ''' <param name="sUON3">Código UON3 del centro de coste</param>
    ''' <param name="sUON4">Código UON4 del centro de coste</param>
    ''' <param name="sUsu">Código de usuario para comprobar permisos</param>
    ''' <param name="sIdioma">Idioma en que se mostrarán las partidas</param>
    ''' <param name="bBajaLogica">Si se tienen en cuenta las que estan de baja logica</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Partida_PRES5_GetPartidasPresupuestarias(ByVal sUON1 As String, ByVal sUON2 As String, _
                   ByVal sUON3 As String, ByVal sUON4 As String, _
                   ByVal sUsu As String, ByVal sIdioma As String, _
                   ByVal bBajaLogica As Boolean) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSSM_GET_PARTIDAPRESUPUESTARIAS"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Parameters.AddWithValue("@UON1", sUON1)
            If Not sUON2 = "" Then cm.Parameters.AddWithValue("@UON2", sUON2)
            If Not sUON3 = "" Then cm.Parameters.AddWithValue("@UON3", sUON3)
            If Not sUON4 = "" Then cm.Parameters.AddWithValue("@UON4", sUON4)
            cm.Parameters.AddWithValue("@USU", sUsu)
            cm.Parameters.AddWithValue("@IDI", sIdioma)
            cm.Parameters.AddWithValue("@BAJALOG", IIf(bBajaLogica, Nothing, 0))

            da.SelectCommand = cm
            da.Fill(ds)

            Return ds.Tables(0)
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Agrega una nueva partida presupuestaria
    ''' </summary>
    ''' <param name="UON1">Código UON1 del centro de coste donde se añade la partida presupuestaria</param>
    ''' <param name="UON2">Código UON2 del centro de coste donde se añade la partida presupuestaria</param>
    ''' <param name="UON3">Código UON3 del centro de coste donde se añade la partida presupuestaria</param>
    ''' <param name="UON4">Código UON4 del centro de coste donde se añade la partida presupuestaria</param>
    ''' <param name="PRES0">Código PRES0 de la partida presupuestaria</param>
    ''' <param name="PRES1">Código PRES1 de la partida presupuestaria</param>
    ''' <param name="PRES2">Código PRES2 de la partida presupuestaria</param>
    ''' <param name="PRES3">Código PRES3 de la partida presupuestaria</param>
    ''' <param name="PRES4">Código PRES4 de la partida presupuestaria</param>
    ''' <param name="dtDenominaciones">Tabla de las denominaciones agregadas</param>
    ''' <param name="FecIni">Fecha de inicio</param>
    ''' <param name="FecFin">Fecha de fin</param>
    ''' <param name="Mon">Moneda de la partida</param>
    ''' <param name="Pres">Importe presupuestado</param>
    ''' <param name="Usu">Usuario que añade la partida</param>
    ''' <remarks></remarks>
    Public Function AgregarPartidaPresupuestaria(ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, ByVal UON4 As String, _
              ByVal PRES0 As String, ByVal PRES1 As String, ByVal PRES2 As String, _
              ByVal PRES3 As String, ByVal PRES4 As String, ByVal dtDenominaciones As DataTable, _
              ByVal FecIni As Nullable(Of DateTime), ByVal FecFin As Nullable(Of DateTime), _
              ByVal Mon As String, ByVal Pres As Nullable(Of Double), ByVal Usu As String) As Integer
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter
        cn.Open()
        Dim transaccion As SqlTransaction = cn.BeginTransaction()
        Try
            cm.Connection = cn
            cm.CommandText = "FSSM_AGREGAR_PARTIDAPRESUPUESTARIA"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion

            With cm.Parameters
                .AddWithValue("@UON1", IIf(UON1 = "", Nothing, UON1))
                .AddWithValue("@UON2", IIf(UON2 = "", Nothing, UON2))
                .AddWithValue("@UON3", IIf(UON3 = "", Nothing, UON3))
                .AddWithValue("@UON4", IIf(UON4 = "", Nothing, UON4))
                .AddWithValue("@PRES0", PRES0)
                .AddWithValue("@PRES1", PRES1)
                .AddWithValue("@PRES2", PRES2)
                .AddWithValue("@PRES3", PRES3)
                .AddWithValue("@PRES4", PRES4)
                .AddWithValue("@FECINI", IIf(FecIni = DateTime.MinValue, Nothing, FecIni))
                .AddWithValue("@FECFIN", IIf(FecFin = DateTime.MinValue, Nothing, FecFin))
                .AddWithValue("@PRES", Pres)
                .AddWithValue("@MON", Mon)
                .AddWithValue("@USU", Usu)
                .Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output
            End With

            cm.ExecuteNonQuery()

            Dim Id_Hist As Integer = cm.Parameters("@ID").Value
            If Id_Hist = 0 Then
                transaccion.Rollback()
                Return 2
            Else
                For Each row As DataRow In dtDenominaciones.Rows
                    row("ID") = Id_Hist
                Next

                cm = New SqlCommand("FSSM_AGREGAR_DENOMINACIONPARTIDA", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .Add("@ID", SqlDbType.Int, 4, "ID")
                    .Add("@PRES0", SqlDbType.NVarChar, 100, "PRES0")
                    .Add("@PRES1", SqlDbType.NVarChar, 100, "PRES1")
                    .Add("@PRES2", SqlDbType.NVarChar, 100, "PRES2")
                    .Add("@PRES3", SqlDbType.NVarChar, 100, "PRES3")
                    .Add("@PRES4", SqlDbType.NVarChar, 100, "PRES4")
                    .Add("@IDI", SqlDbType.NVarChar, 20, "COD")
                    .Add("@DEN", SqlDbType.NVarChar, 100, "DEN")
                    .Add("@ALTA", SqlDbType.Bit, 1, "ALTA")
                End With

                da.InsertCommand = cm
                da.Update(dtDenominaciones)

                transaccion.Commit()
                Return 1
            End If
        Catch ex As Exception
            transaccion.Rollback()
            Return 0
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Modifica los datos de una partida presupuestaria ya existente
    ''' </summary>
    ''' <param name="UON1">Código UON1 del centro de coste donde se modifica la partida presupuestaria</param>
    ''' <param name="UON2">Código UON2 del centro de coste donde se modifica la partida presupuestaria</param>
    ''' <param name="UON3">Código UON3 del centro de coste donde se modifica la partida presupuestaria</param>
    ''' <param name="UON4">Código UON4 del centro de coste donde se modifica la partida presupuestaria</param>
    ''' <param name="PRES0">Código PRES0 de la partida presupuestaria</param>
    ''' <param name="PRES1">Código PRES1 de la partida presupuestaria</param>
    ''' <param name="PRES2">Código PRES2 de la partida presupuestaria</param>
    ''' <param name="PRES3">Código PRES3 de la partida presupuestaria</param>
    ''' <param name="PRES4">Código PRES4 de la partida presupuestaria</param>
    ''' <param name="dtDenominaciones">Tabla de las denominaciones modificadas</param>
    ''' <param name="FecIni">Fecha de inicio</param>
    ''' <param name="FecFin">Fecha de fin</param>
    ''' <param name="Mon">Moneda de la partida</param>
    ''' <param name="Pres">Importe presupuestado</param>
    ''' <param name="Observaciones">Observaciones a la modificación</param> 
    ''' <param name="Usu">Usuario que modifica la partida</param>
    ''' <remarks></remarks>
    Public Sub ModificarPartidaPresupuestaria(ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, ByVal UON4 As String, _
              ByVal PRES0 As String, ByVal PRES1 As String, ByVal PRES2 As String, _
              ByVal PRES3 As String, ByVal PRES4 As String, ByVal BajaLogica As Boolean, _
              ByVal dtDenominaciones As DataTable, _
              ByVal FecIni As Nullable(Of DateTime), ByVal FecFin As Nullable(Of DateTime), _
              ByVal Mon As String, ByVal Pres As Nullable(Of Double), ByVal Observaciones As String, ByVal Usu As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter
        cn.Open()
        Dim transaccion As SqlTransaction = cn.BeginTransaction()
        Try
            cm.Connection = cn
            cm.CommandText = "FSSM_MODIFICAR_PARTIDAPRESUPUESTARIA"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion

            With cm.Parameters
                .AddWithValue("@UON1", IIf(UON1 = "", Nothing, UON1))
                .AddWithValue("@UON2", IIf(UON2 = "", Nothing, UON2))
                .AddWithValue("@UON3", IIf(UON3 = "", Nothing, UON3))
                .AddWithValue("@UON4", IIf(UON4 = "", Nothing, UON4))
                .AddWithValue("@PRES0", PRES0)
                .AddWithValue("@PRES1", PRES1)
                .AddWithValue("@PRES2", PRES2)
                .AddWithValue("@PRES3", PRES3)
                .AddWithValue("@PRES4", PRES4)
                .AddWithValue("@BAJALOG", BajaLogica)
                .AddWithValue("@FECINI", IIf(FecIni = DateTime.MinValue, Nothing, FecIni))
                .AddWithValue("@FECFIN", IIf(FecFin = DateTime.MinValue, Nothing, FecFin))
                .AddWithValue("@PRES", Pres)
                .AddWithValue("@MON", Mon)
                .AddWithValue("@COMENT", Observaciones)
                .AddWithValue("@USU", Usu)
                .Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output
            End With

            cm.ExecuteNonQuery()

            Dim Id_Hist As Integer = cm.Parameters("@ID").Value
            For Each row As DataRow In dtDenominaciones.Rows
                row("ID") = Id_Hist
            Next

            cm = New SqlCommand("FSSM_AGREGAR_DENOMINACIONPARTIDA", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion

            With cm.Parameters
                .Add("@ID", SqlDbType.Int, 4, "ID")
                .Add("@PRES0", SqlDbType.NVarChar, 100, "PRES0")
                .Add("@PRES1", SqlDbType.NVarChar, 100, "PRES1")
                .Add("@PRES2", SqlDbType.NVarChar, 100, "PRES2")
                .Add("@PRES3", SqlDbType.NVarChar, 100, "PRES3")
                .Add("@PRES4", SqlDbType.NVarChar, 100, "PRES4")
                .Add("@IDI", SqlDbType.NVarChar, 20, "COD")
                .Add("@DEN", SqlDbType.NVarChar, 100, "DEN")
                .Add("@ALTA", SqlDbType.Bit, 1, "ALTA")
            End With

            da.InsertCommand = cm
            da.Update(dtDenominaciones)

            transaccion.Commit()
        Catch ex As Exception
            transaccion.Rollback()
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Obtenemos de base de datos la longitud máxima permitida para los códigos de las partidas presupuestarias
    ''' </summary>
    ''' <returns>Datatable con las longitudes</returns>
    ''' <remarks></remarks>
    Public Function ObtenerLongitudesCodigos() As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSSM_GET_LONGITUDESCODIGOPARTIDAS"
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(ds)
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

        Return ds.Tables(0)
    End Function
    ''' <summary>
    ''' Cambia el permiso para modificar una partida presupuestaria externa
    ''' </summary>
    ''' <param name="UON1">Código UON1 del centro de coste donde se modifica la partida presupuestaria</param>
    ''' <param name="UON2">Código UON2 del centro de coste donde se modifica la partida presupuestaria</param>
    ''' <param name="UON3">Código UON3 del centro de coste donde se modifica la partida presupuestaria</param>
    ''' <param name="UON4">Código UON4 del centro de coste donde se modifica la partida presupuestaria</param>
    ''' <param name="PRES0">Código PRES0 de la partida presupuestaria</param>
    ''' <param name="PRES1">Código PRES1 de la partida presupuestaria</param>
    ''' <param name="PRES2">Código PRES2 de la partida presupuestaria</param>
    ''' <param name="PRES3">Código PRES3 de la partida presupuestaria</param>
    ''' <param name="PRES4">Código PRES4 de la partida presupuestaria</param>
    ''' <param name="Abierta">Si se puede modificar la partida presupuestaria externa(1) o no (0)</param>
    ''' <param name="Usu">Usuario que modifica la partida</param>
    ''' <param name="Idioma">Idioma en el que se muestran los datos</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ModificarControlCambiosPartidaPresupuestaria(ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, ByVal UON4 As String, _
              ByVal PRES0 As String, ByVal PRES1 As String, ByVal PRES2 As String, _
              ByVal PRES3 As String, ByVal PRES4 As String, ByVal Abierta As Integer, _
              ByVal Usu As String, ByVal Idioma As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSSM_MODIFICAR_CONTROLCAMBIOS_PARTIDAPRESUPUESTARIA"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300

            With cm.Parameters
                .AddWithValue("@UON1", IIf(UON1 = "", Nothing, UON1))
                .AddWithValue("@UON2", IIf(UON2 = "", Nothing, UON2))
                .AddWithValue("@UON3", IIf(UON3 = "", Nothing, UON3))
                .AddWithValue("@UON4", IIf(UON4 = "", Nothing, UON4))
                .AddWithValue("@PRES0", IIf(PRES0 = "", Nothing, PRES0))
                .AddWithValue("@PRES1", IIf(PRES1 = "", Nothing, PRES1))
                .AddWithValue("@PRES2", IIf(PRES2 = "", Nothing, PRES2))
                .AddWithValue("@PRES3", IIf(PRES3 = "", Nothing, PRES3))
                .AddWithValue("@PRES4", IIf(PRES4 = "", Nothing, PRES4))
                .AddWithValue("@ABIERTA", Abierta)
                .AddWithValue("@USU", Usu)
                .AddWithValue("@IDI", Idioma)
            End With
            cm.CommandType = CommandType.StoredProcedure

            da.SelectCommand = cm
            da.Fill(dr)

            '******************************************************************
            Dim parentCols(0) As DataColumn
            Dim childCols(0) As DataColumn

            parentCols(0) = dr.Tables(0).Columns("PRES0")
            childCols(0) = dr.Tables(1).Columns("PRES0")
            dr.Relations.Add("RELACION_1", parentCols, childCols, False)

            ReDim parentCols(1)
            ReDim childCols(1)

            parentCols(0) = dr.Tables(1).Columns("PRES0")
            parentCols(1) = dr.Tables(1).Columns("PRES1")
            childCols(0) = dr.Tables(2).Columns("PRES0")
            childCols(1) = dr.Tables(2).Columns("PRES1")
            dr.Relations.Add("RELACION_2", parentCols, childCols, False)

            ReDim parentCols(2)
            ReDim childCols(2)

            parentCols(0) = dr.Tables(2).Columns("PRES0")
            parentCols(1) = dr.Tables(2).Columns("PRES1")
            parentCols(2) = dr.Tables(2).Columns("PRES2")
            childCols(0) = dr.Tables(3).Columns("PRES0")
            childCols(1) = dr.Tables(3).Columns("PRES1")
            childCols(2) = dr.Tables(3).Columns("PRES2")
            dr.Relations.Add("RELACION_3", parentCols, childCols, False)

            ReDim parentCols(3)
            ReDim childCols(3)

            parentCols(0) = dr.Tables(3).Columns("PRES0")
            parentCols(1) = dr.Tables(3).Columns("PRES1")
            parentCols(2) = dr.Tables(3).Columns("PRES2")
            parentCols(3) = dr.Tables(3).Columns("PRES3")
            childCols(0) = dr.Tables(4).Columns("PRES0")
            childCols(1) = dr.Tables(4).Columns("PRES1")
            childCols(2) = dr.Tables(4).Columns("PRES2")
            childCols(3) = dr.Tables(4).Columns("PRES3")
            dr.Relations.Add("RELACION_4", parentCols, childCols, False)

            With dr
                .Tables(0).TableName = "PRES5_NIV0"
                .Tables(1).TableName = "PRES5_NIV1"
                .Tables(2).TableName = "PRES5_NIV2"
                .Tables(3).TableName = "PRES5_NIV3"
                .Tables(4).TableName = "PRES5_NIV4"
            End With

            dr.Tables.Add("UONS")
            With dr.Tables("UONS")
                .Columns.Add("UON1", System.Type.GetType("System.String"))
                .Columns.Add("UON2", System.Type.GetType("System.String"))
                .Columns.Add("UON3", System.Type.GetType("System.String"))
                .Columns.Add("UON4", System.Type.GetType("System.String"))

                Dim row As DataRow = .NewRow
                row("UON1") = UON1
                row("UON2") = UON2
                row("UON3") = UON3
                row("UON4") = UON4

                dr.Tables("UONS").Rows.Add(row)
            End With

            Return dr
        Catch ex As Exception
            Return Nothing
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' Revisado por: blp. Fecha: 27/06/2012
    ''' <summary>
    ''' Devuelve los Gestores de las partidas a las que puede imputar el usuario
    ''' </summary>
    ''' <param name="USU">Codigo del usuario</param>
    ''' <param name="PRES0">código del árbol de Partidas presupuestarias para la que estamos devolviendo los gestores</param>
    ''' <returns>Un DataTable con las partidas, código de los gestores y sus nombres y apellidos</returns>
    Public Function PRES5_ObtGestoresPartidasUsu(ByVal Usu As String, ByVal PRES0 As String) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Try
            Using cn
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "SM_OBT_GESTORES_PARTIDAS_USU"
                cm.CommandType = CommandType.StoredProcedure

                cm.Parameters.Add(New SqlParameter("@USU", SqlDbType.NVarChar, 100))
                cm.Parameters("@USU").Value = Usu
                cm.Parameters.Add(New SqlParameter("@PRES0", SqlDbType.NVarChar, 100))
                cm.Parameters("@PRES0").Value = PRES0

                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(ds)
                Return ds.Tables(0)
            End Using
        Catch e As Exception
            Throw e
        Finally
            cn.Close()
            cn.Dispose()
            cm.Dispose()
            ds.Dispose()
        End Try
    End Function
    Public Function PRES5_LoadPartidasInstalacion(ByVal sIdioma As String) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSPM_DEVOLVER_PARTIDAS"
            cm.Parameters.AddWithValue("@IDIOMA", sIdioma)
            cm.CommandType = CommandType.StoredProcedure

            da.SelectCommand = cm
            da.Fill(ds)
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

        Return ds.Tables(0)
    End Function
    ''' Revisado por: blp. Fecha: 25/01/2013
    ''' <summary>
    ''' Devuelve las partidas presentes en los pedidos que un usuario puede consultar en la pantalla de seguimiento de EP
    ''' </summary>
    ''' <param name="codPer">Código del usuario</param>
    ''' <param name="idioma">Idioma</param>
    ''' <param name="VerPedidosOtrosUsuarios">Permiso del usuario para ver pedidos de otros usuarios</param>
    ''' <returns>Dataset con las partidas</returns>
    ''' <remarks>Llamada desde SM\PRES5.vb->DevolverPartidasPedidos. Máximo 4,5 seg.</remarks>
    Public Function Partidas_DevolverPartidasPedidos(ByVal codPer As String, ByVal idioma As FSNLibrary.Idioma, ByVal VerPedidosOtrosUsuarios As Boolean)
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "EP_OBT_PARTIDAS_ARBOL_SEGUIMIENTO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@PER", codPer)
            cm.Parameters.AddWithValue("@IDIOMA", idioma.ToString)
            cm.Parameters.AddWithValue("@PERMISO_VER_PEDIDOS_OTROS_USUARIOS", VerPedidosOtrosUsuarios)
            da.SelectCommand = cm
            da.Fill(ds)
            Return ds
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>    ''' 
    ''' Devuelve las partidas presentes en los pedidos que un usuario puede consultar en la pantalla de seguimiento de EP
    ''' </summary>
    ''' <param name="Usuario">CÃ³digo del usuario</param>
    ''' <param name="idioma">Idioma</param>
    ''' <returns>Dataset con las partidas</returns>
    ''' <remarks>Revisado por: blp. Fecha: 25/01/2013 Llamada desde SM\PRES5.vb->DevolverPartidasPedidos. MÃ¡ximo 4,5 seg.</remarks>
    Public Function Partidas_DevolverPartidasEP(ByVal Usuario As String, ByVal Idioma As FSNLibrary.Idioma, _
                    ByVal Pres5 As String, ByVal PedidosOtrasEmp As Boolean, ByVal sCentro As String, _
                    Optional ByVal IdEmpresa As Integer = Nothing, Optional ByVal sTextoFiltro As String = Nothing)
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "EP_OBT_PARTIDAS_EP"
            cm.CommandType = CommandType.StoredProcedure

            cm.Parameters.Add(New SqlParameter("@USUARIO", SqlDbType.VarChar, 20))
            cm.Parameters("@USUARIO").Value = Usuario
            cm.Parameters.Add(New SqlParameter("@IDIOMA", SqlDbType.VarChar, 3))
            cm.Parameters("@IDIOMA").Value = Idioma.ToString()
            If Not String.IsNullOrEmpty(Pres5) Then
                cm.Parameters.Add(New SqlParameter("@PRES5", SqlDbType.VarChar, 50))
                cm.Parameters("@PRES5").Value = Pres5
            End If

            cm.Parameters.Add(New SqlParameter("@EMP", SqlDbType.Int))
            cm.Parameters("@EMP").Value = IdEmpresa
            cm.Parameters.AddWithValue("@PED_OTRAS_EMPRESAS", PedidosOtrasEmp)
            cm.Parameters.Add(New SqlParameter("@CENTRO", SqlDbType.VarChar, 50))
            cm.Parameters("@CENTRO").Value = sCentro.ToString()
            'cm.Parameters.AddWithValue("@CENTRO", sCentro)
            cm.Parameters.Add(New SqlParameter("@TEXTO", SqlDbType.VarChar, 200))
            cm.Parameters("@TEXTO").Value = sTextoFiltro.ToString()

            da.SelectCommand = cm
            da.Fill(ds)
            Return ds
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' Revisado por: blp. Fecha: 20/08/2013
    ''' <summary>
    ''' Cargas las partidas de nivel 0
    ''' </summary>
    ''' <param name="idioma">Idioma</param>
    ''' <returns>Dataset con las partidas</returns>
    ''' <remarks>Llamada desde PM\Partidas.vb. Maximo 0,2 seg.</remarks>
    Public Function Partidas_LoadPartidasPres0(ByVal idioma As FSNLibrary.Idioma) As DataTable
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "PM_OBT_PARTIDAS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDIOMA", idioma.ToString)
            da.SelectCommand = cm
            da.Fill(dt)
            Return dt
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' Revisado por: blp. Fecha: 20/08/2013
    ''' <summary>
    ''' Carga todas las partidas
    ''' </summary>
    ''' <param name="idioma">Idioma</param>
    ''' <param name="filtro">Texto por el que filtrar</param>
    ''' <returns>Dataset con las partidas</returns>
    ''' <remarks>Llamada desde PM\Partidas.vb. Maximo 0,2 seg.</remarks>
    Public Function Partidas_LoadPartidas(ByVal idioma As FSNLibrary.Idioma, ByVal filtro As String) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "PM_OBT_PARTIDAS_ARBOL"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDIOMA", idioma.ToString)
            cm.Parameters.AddWithValue("@FILTRO", filtro)
            da.SelectCommand = cm
            da.Fill(ds)
            Return ds
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' Revisado por: blp. Fecha: 26/08/2013
    ''' <summary>
    ''' Cargas las UONS de las partidas
    ''' </summary>
    ''' <param name="partidasPres">tipo tabla con listado de partidas</param>
    ''' <returns>Dataset con las UONS de las partidas</returns>
    ''' <remarks>Llamada desde PM\Partidas.vb. Maximo 0,2 seg.</remarks>
    Public Function Partidas_LoadPartidasUONs(ByVal partidasPres As DataTable) As DataTable
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "PM_OBT_PARTIDAS_UONS"
            cm.CommandType = CommandType.StoredProcedure
            'Creamos un parametro sql de tipo TBCADENATEXTO, que es un tipo "tabla" definido por el usuario en sql server
            Dim ParamPartidas As New SqlParameter("@PARTIDASPRES", SqlDbType.Structured)
            ParamPartidas.TypeName = "TBCADENATEXTO2"
            ParamPartidas.Value = partidasPres
            cm.Parameters.Add(ParamPartidas)
            da.SelectCommand = cm
            da.Fill(ds)
            If ds IsNot Nothing AndAlso ds.Tables(0) IsNot Nothing Then
                Return ds.Tables(0)
            Else
                Return Nothing
            End If
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' Revisado por: blp. Fecha: 20/08/2013
    ''' <summary>
    ''' Carga todas las partidas
    ''' </summary>
    ''' <param name="sPartidaPres">CÓDIGO DE LA PARTIDA: PRES0#PRES1#PRES2#PRES3#PRES4</param>
    ''' <param name="idioma">Idioma</param>
    ''' <returns>Dataset con los datos de la partida</returns>
    ''' <remarks>Llamada desde PM\Partidas.vb. Maximo 0,1 seg.</remarks>
    Public Function Partidas_LoadPartida(ByVal sPartidaPres As String, ByVal idioma As FSNLibrary.Idioma) As DataTable
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "PM_OBT_PARTIDA"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@PARTIDAPRES", sPartidaPres)
            cm.Parameters.AddWithValue("@IDIOMA", idioma.ToString)
            da.SelectCommand = cm
            da.Fill(ds)
            If ds IsNot Nothing AndAlso ds.Tables(0) IsNot Nothing Then
                Return ds.Tables(0)
            Else
                Return Nothing
            End If
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function PartidasPRES5_UsuariosNotifControlDisponible()
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            Using cn
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSSM_CONTROL_DISPONIBLE_USUARIOS"
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(ds)

                Return ds
            End Using
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function PartidasPRES5_NotificarDisponibleNegativo(sUsu As String, sIdi As String)
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            Using cn
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSSM_CONTROL_DISPONIBLE_PARTIDAS"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.Add(New SqlParameter("@IDI", SqlDbType.NVarChar, 3))
                cm.Parameters("@IDI").Value = sIdi
                cm.Parameters.Add(New SqlParameter("@USU", SqlDbType.NVarChar, 20))
                cm.Parameters("@USU").Value = sUsu

                da.SelectCommand = cm
                da.Fill(ds)
                Return ds.Tables(0)
            End Using
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function Obtener_Partidas_Presupuestarias(ByVal CodUsuario As String, ByVal Idioma As String, ByVal CodCentroCoste As String, ByVal PRES0 As String,
                                                     ByVal EsAutocompletar As Boolean, ByVal filtroCodigo As String, ByVal filtroDenominacion As String) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = "FSSM_OBTENER_PARTIDAS_PRESUPUESTARIAS"
            cm.Parameters.AddWithValue("@USU", CodUsuario)
            cm.Parameters.AddWithValue("@IDIOMA", Idioma)
            cm.Parameters.AddWithValue("@CENTRO_SM", CodCentroCoste)
            cm.Parameters.AddWithValue("@PRES0", PRES0)
            cm.Parameters.AddWithValue("@ESAUTOCOMPLETAR", EsAutocompletar)
            If Not String.IsNullOrEmpty(filtroCodigo) Then cm.Parameters.AddWithValue("@FILTROCODIGO", filtroCodigo)
            If Not String.IsNullOrEmpty(filtroDenominacion) Then cm.Parameters.AddWithValue("@FILTRODENOMINACION", filtroDenominacion)
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
	Public Function AgregarAnyoPartidaPresupuestaria(ByVal IdPartida As Integer, ByVal PRES0 As String, ByVal PRES1 As String, ByVal PRES2 As String,
				ByVal PRES3 As String, ByVal PRES4 As String, ByVal Anyo As Integer, ByVal Pres As Nullable(Of Double), ByVal Usu As String) As Integer
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Dim da As New SqlDataAdapter

		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSSM_AGREGAR_ANYO_PARTIDAPRESUPUESTARIA"
			cm.CommandType = CommandType.StoredProcedure
			cm.CommandTimeout = 300

			With cm.Parameters
				.AddWithValue("@IDPARTIDA", IdPartida)
				.AddWithValue("@PRES0", PRES0)
				.AddWithValue("@PRES1", PRES1)
				.AddWithValue("@PRES2", PRES2)
				.AddWithValue("@PRES3", PRES3)
				.AddWithValue("@PRES4", PRES4)
				.AddWithValue("@ANYO", Anyo)
				.AddWithValue("@PRES", Pres)
				.AddWithValue("@USU", Usu)
			End With

			cm.ExecuteNonQuery()
			Return 1
		Catch ex As Exception
			Return 0
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Function
	Public Function ModificarAnyoPartidaPresupuestaria(ByVal IdPartida As Integer, ByVal PRES0 As String, ByVal PRES1 As String, ByVal PRES2 As String,
				ByVal PRES3 As String, ByVal PRES4 As String, ByVal Anyo As Integer, ByVal Pres As Nullable(Of Double), ByVal Observaciones As String,
				ByVal Usu As String) As Integer
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Dim da As New SqlDataAdapter

		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSSM_MODIFICAR_ANYO_PARTIDAPRESUPUESTARIA"
			cm.CommandType = CommandType.StoredProcedure
			cm.CommandTimeout = 300

			With cm.Parameters
				.AddWithValue("@IDPARTIDA", IdPartida)
				.AddWithValue("@PRES0", PRES0)
				.AddWithValue("@PRES1", PRES1)
				.AddWithValue("@PRES2", PRES2)
				.AddWithValue("@PRES3", PRES3)
				.AddWithValue("@PRES4", PRES4)
				.AddWithValue("@ANYO", Anyo)
				.AddWithValue("@PRES", Pres)
				.AddWithValue("@COMENT", Observaciones)
				.AddWithValue("@USU", Usu)
			End With

			cm.ExecuteNonQuery()
			Return 1
		Catch ex As Exception
			Return 0
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Function
	Public Function Partida_PRES5_DevolverDatosPartidaPRES5Anyo(ByVal IdPartida As Integer, ByVal Anyo As Integer) As DataTable
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Dim ds As New DataSet
		Dim da As New SqlDataAdapter
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSSM_GET_PARTIDAPRESUPUESTARIA_ANYO"
			cm.CommandType = CommandType.StoredProcedure

			cm.Parameters.AddWithValue("@IDPARTIDA", IdPartida)
			cm.Parameters.AddWithValue("@ANYO", Anyo)

			da.SelectCommand = cm
			da.Fill(ds)

			Return ds.Tables(0)
		Catch ex As Exception
			Throw ex
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Function
#End Region
#Region "Activo"
	''' <summary>
	''' Buscar el código de especificado en el conjunto de activos asociados a los centros SM a los que el usuario puede imputar
	''' </summary>
	''' <param name="sIdioma">Código del idioma</param>
	''' <param name="sCodActivo">Código del activo que se quiere buscar</param>
	''' <param name="sCentroCoste">(opcional)Identificador del centro de coste</param>
	''' <returns>Un DataSet con el activo en caso de encontrar el registro</returns>
	''' <remarks>El activo debe tener traducción en ACTIVO_DEN</remarks>
	Public Function Activo_Buscar(ByVal sIdioma As String, ByVal sUsuario As String, ByVal sCodActivo As String, Optional ByVal sCentroCoste As String = Nothing) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn
            cm.CommandText = "FSSM_BUSCAR_ACTIVO"
            cm.Parameters.AddWithValue("@IDIOMA", sIdioma)
            cm.Parameters.AddWithValue("@USUARIO", sUsuario)
            cm.Parameters.AddWithValue("@CODACTIVO", sCodActivo)
            If sCentroCoste <> Nothing Then
                Dim sUONs As Object = Split(sCentroCoste, "#")
                cm.Parameters.AddWithValue("@UON1", sUONs(0))
                If UBound(sUONs) >= 1 Then
                    cm.Parameters.AddWithValue("@UON2", sUONs(1))
                    If UBound(sUONs) >= 2 Then
                        cm.Parameters.AddWithValue("@UON3", sUONs(2))
                        If UBound(sUONs) = 3 Then
                            cm.Parameters.AddWithValue("@UON4", sUONs(3))
                        End If
                    End If
                End If
            End If

            cm.CommandType = CommandType.StoredProcedure

            da.SelectCommand = cm
            da.Fill(dr)

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Devuelve el detalle del activo especificado
    ''' </summary>
    ''' <param name="sIdioma">Código del idioma del usuario</param>
    ''' <param name="sCodActivo">Código del activo que se quiere buscar</param>
    ''' <returns>Un DataSet con el activo en caso de encontrar el registro</returns>
    ''' <remarks>El activo debe tener traducción en ACTIVO_DEN</remarks>
    Public Function Activo_Detalle(ByVal sIdioma As String, ByVal sCodActivo As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn
            cm.CommandText = "FSSM_DETALLE_ACTIVO"
            cm.Parameters.AddWithValue("@IDIOMA", sIdioma)
            cm.Parameters.AddWithValue("@CODACTIVO", sCodActivo)
            cm.CommandType = CommandType.StoredProcedure

            da.SelectCommand = cm
            da.Fill(dr)

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
#End Region
#Region "Activos"
    ''' <summary>
    ''' Devuelve todos los activos con la denominación en el idioma especificado (incluye la denominación de centros y empresas) 
    ''' </summary>
    ''' <param name="Idioma">Código de idioma</param>
    ''' <param name="Empresa">Código de la empresa</param>
    ''' <returns>DataTable</returns>
    Public Function Activos_Cargar(ByVal Idioma As String, ByVal Empresa As Nullable(Of Integer), ByVal CentroCoste As String) As DataTable
        Authenticate()
        Using cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand("FSSM_DEVOLVER_ACTIVOS", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@IDIOMA", SqlDbType.VarChar)
            cm.Parameters("@IDIOMA").Value = Idioma
            If Empresa.HasValue Then
                cm.Parameters.Add("@EMPRESA", SqlDbType.Int)
                cm.Parameters("@EMPRESA").Value = Empresa
            Else
                cm.Parameters.Add("@EMPRESA", SqlDbType.Int)
                cm.Parameters("@EMPRESA").Value = System.DBNull.Value
            End If
            If Not String.IsNullOrEmpty(CentroCoste) Then
                cm.Parameters.Add("@CENTROCOSTE", SqlDbType.VarChar)
                cm.Parameters("@CENTROCOSTE").Value = CentroCoste
            End If
            cm.CommandTimeout = 300
            cn.Open()
            Dim da As New SqlDataAdapter(cm)
            Dim dt As New DataTable
            da.Fill(dt)
            cn.Close()
            Return dt
        End Using
    End Function
    ''' <summary>
    ''' Devuelve los activos asociados a los centros de coste SM a los que el usuario puede imputar
    ''' </summary>
    ''' <param name="Idioma">Idioma del usuario</param>
    ''' <param name="Usuario">Código del usuario</param>
    ''' <returns>Un DataTable con los activos</returns>
    Public Function Activos_CC_Imputacion_Cargar(ByVal Idioma As String, _
             ByVal Usuario As String, _
             Optional ByVal UON1 As String = Nothing, _
             Optional ByVal UON2 As String = Nothing, _
             Optional ByVal UON3 As String = Nothing, _
             Optional ByVal UON4 As String = Nothing, _
             Optional ByVal Empresa As Integer = 0) As DataTable
        Authenticate()
        Using cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand("FSSM_DEVOLVER_ACTIVOS_CC_IMPUTACION", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@IDIOMA", SqlDbType.VarChar)
            cm.Parameters("@IDIOMA").Value = Idioma
            cm.Parameters.Add("@USUARIO", SqlDbType.VarChar)
            cm.Parameters("@USUARIO").Value = Usuario
            If Not String.IsNullOrEmpty(UON1) Then
                cm.Parameters.Add("@UON1", SqlDbType.VarChar)
                cm.Parameters("@UON1").Value = UON1
                If Not String.IsNullOrEmpty(UON2) Then
                    cm.Parameters.Add("@UON2", SqlDbType.VarChar)
                    cm.Parameters("@UON2").Value = UON2
                    If Not String.IsNullOrEmpty(UON3) Then
                        cm.Parameters.Add("@UON3", SqlDbType.VarChar)
                        cm.Parameters("@UON3").Value = UON3
                        If Not String.IsNullOrEmpty(UON4) Then
                            cm.Parameters.Add("@UON4", SqlDbType.VarChar)
                            cm.Parameters("@UON4").Value = UON4
                        End If
                    End If
                End If
            End If
            If Empresa <> 0 Then
                cm.Parameters.Add("@EMPRESA", SqlDbType.Int)
                cm.Parameters("@EMPRESA").Value = Empresa
            End If
            cm.CommandTimeout = 300
            cn.Open()
            Dim da As New SqlDataAdapter(cm)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Using
    End Function
#End Region
#Region "PartidasPRES5"
#End Region
#Region " PargenSM data access methods "
    ''' <summary>
    ''' Función que devuelve un DataSet con la configuración de los Parámetros de SM para el usuario dado
    ''' </summary>
    ''' <param name="USU">Codigo de usuario</param>
    ''' <returns>Un DataSet con la configuración de los parámetros de SM para el usuario dado</returns>
    ''' <remarks>
    ''' Llamada desde PargenSM.CargarConfiguracionSM
    ''' Tiempo máximo:0,2 seg</remarks>
    Public Function PargensSMs_CargarConfiguracionSM(ByVal USU As String, ByVal Idioma As String, ByVal Pres5Niv_0 As String) As DataSet
        Dim SqlCn As New SqlConnection(mDBConnection)
        Dim SqlCmd As New SqlCommand
        Dim SqlParam As New SqlParameter
        Dim DsResul As New DataSet
        Dim Da As New SqlDataAdapter
        SqlCn.Open()
        SqlCmd.Connection = SqlCn
        SqlCmd.CommandType = CommandType.StoredProcedure
        SqlCmd.CommandText = "FSSM_GET_CONFIGURACION_SM"
        If USU <> "" Then
            SqlParam = SqlCmd.Parameters.AddWithValue("@USU", USU)
        Else
            SqlParam = SqlCmd.Parameters.AddWithValue("@USU", System.DBNull.Value)
        End If
        If Idioma <> "" Then
            SqlParam = SqlCmd.Parameters.AddWithValue("@IDIOMA", Idioma)
        Else
            SqlParam = SqlCmd.Parameters.AddWithValue("@IDIOMA", System.DBNull.Value)
        End If
        If Pres5Niv_0 <> "" Then
            SqlParam = SqlCmd.Parameters.AddWithValue("@PRES5", Pres5Niv_0)
        Else
            SqlParam = SqlCmd.Parameters.AddWithValue("@PRES5", System.DBNull.Value)
        End If
        Da.SelectCommand = SqlCmd
        Da.Fill(DsResul)
        SqlCn.Close()
        Return DsResul
        SqlCmd.Dispose()
        Da.Dispose()
    End Function
    ''' <summary>
    ''' Obtiene las cabeceras del grid que muestra el arbol jerarquico de las partidas presupuestarias
    ''' </summary>
    ''' <param name="Idioma">Idioma en el que se mustran las denominaciones de las partidas presupuestarias</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function PargensSMs_GetHeadersPartidasPresupuestarias(ByVal Idioma As String) As DataRow
        Dim SqlCn As New SqlConnection(mDBConnection)
        Dim SqlCmd As New SqlCommand
        Dim drResul As New DataSet
        Dim Da As New SqlDataAdapter
        Try
            SqlCn.Open()

            SqlCmd = New SqlCommand
            With SqlCmd
                .Connection = SqlCn
                .CommandText = "FSSM_GET_HEADERS_PARTIDASPRESUPUESTARIAS"
                .Parameters.AddWithValue("@IDI", Idioma)
                .CommandType = CommandType.StoredProcedure
            End With

            Da.SelectCommand = SqlCmd
            Da.Fill(drResul)

            Return drResul.Tables(0).Rows(0)
        Catch e As Exception
            Throw e
        Finally
            SqlCn.Dispose()
            SqlCmd.Dispose()
            Da.Dispose()
        End Try
    End Function
#End Region
#Region " Historico Partidas Presupuestarias "
	''' <summary>
	''' Función que devuelve el histórico de una partida dada
	''' </summary>    
	''' <param name="PRES0">Código del árbol presupuestario del que se quiere recuperar la partida (PRES5 en otros sitios)</param>
	''' <param name="PRES1">Código de la partida de nivel 1</param>
	''' <param name="PRES2">Código de la partida de nivel 2 (si la hay)</param>
	''' <param name="PRES3">Código de la partida de nivel 3 (si la hay)</param>
	''' <param name="PRES4">Código de la partida de nivel 4 (si la hay)</param>    
	''' <returns>Datatable con la información</returns>
	''' <remarks>Llamada desde FSNServer.PartidasPRES5.PRES5_HistoricoPartida. Tiempo máximo inferior a 1 seg.</remarks>    
	Public Function PRES5_HistoricoPartida(ByVal PRES0 As String, ByVal PRES1 As String, ByVal PRES2 As String, ByVal PRES3 As String, ByVal PRES4 As String) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Dim ds As New DataSet
		Dim da As New SqlDataAdapter
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSSM_DEVOLVER_HISTORICO_PARTIDA"
			cm.CommandType = CommandType.StoredProcedure

			cm.Parameters.Add(New SqlParameter("@PRES0", SqlDbType.VarChar, 100))
			If Not String.IsNullOrEmpty(PRES0) Then
				cm.Parameters("@PRES0").Value = PRES0
			Else
				cm.Parameters("@PRES0").Value = System.DBNull.Value
			End If

			cm.Parameters.Add(New SqlParameter("@PRES1", SqlDbType.VarChar, 100))
			If Not String.IsNullOrEmpty(PRES1) Then
				cm.Parameters("@PRES1").Value = PRES1
			Else
				cm.Parameters("@PRES1").Value = System.DBNull.Value
			End If

			cm.Parameters.Add(New SqlParameter("@PRES2", SqlDbType.VarChar, 100))
			If Not String.IsNullOrEmpty(PRES2) Then
				cm.Parameters("@PRES2").Value = PRES2
			Else
				cm.Parameters("@PRES2").Value = System.DBNull.Value
			End If

			cm.Parameters.Add(New SqlParameter("@PRES3", SqlDbType.VarChar, 100))
			If Not String.IsNullOrEmpty(PRES3) Then
				cm.Parameters("@PRES3").Value = PRES3
			Else
				cm.Parameters("@PRES3").Value = System.DBNull.Value
			End If

			cm.Parameters.Add(New SqlParameter("@PRES4", SqlDbType.VarChar, 100))
			If Not String.IsNullOrEmpty(PRES4) Then
				cm.Parameters("@PRES4").Value = PRES4
			Else
				cm.Parameters("@PRES4").Value = System.DBNull.Value
			End If

			da.SelectCommand = cm
			da.Fill(ds)
		Catch e As Exception
			Throw e
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
		Return ds
	End Function
	''' <summary>
	''' Función que devuelve el histórico de una partida dada
	''' </summary>       
	''' <param name="Usu">Usuario</param>
	''' <param name="PRES0">Código del árbol presupuestario del que se quiere recuperar la partida (PRES5 en otros sitios)</param>
	''' <param name="PRES1">Código de la partida de nivel 1</param>
	''' <param name="PRES2">Código de la partida de nivel 2 (si la hay)</param>
	''' <param name="PRES3">Código de la partida de nivel 3 (si la hay)</param>
	''' <param name="PRES4">Código de la partida de nivel 4 (si la hay)</param>    
	''' <returns>objeto Datatable con la información</returns>
	''' <remarks>Llamada desde SMWeb. Tiempo máximo inferior a 1 seg.</remarks>    
	Public Function PRES5_PermisosUsuCCPartida(ByVal Usu As String, ByVal PRES0 As String, ByVal PRES1 As String, ByVal PRES2 As String, ByVal PRES3 As String, ByVal PRES4 As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "[FSSM_DEVOLVER_PERMISOS_CCPARTIDA]"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add(New SqlParameter("@USU", SqlDbType.VarChar, 50))
            cm.Parameters("@USU").Value = Usu
            cm.Parameters.Add(New SqlParameter("@PRES0", SqlDbType.VarChar, 100))
            If Not String.IsNullOrEmpty(PRES0) Then
                cm.Parameters("@PRES0").Value = PRES0
            Else
                cm.Parameters("@PRES0").Value = System.DBNull.Value
            End If

            cm.Parameters.Add(New SqlParameter("@PRES1", SqlDbType.VarChar, 100))
            If Not String.IsNullOrEmpty(PRES1) Then
                cm.Parameters("@PRES1").Value = PRES1
            Else
                cm.Parameters("@PRES1").Value = System.DBNull.Value
            End If

            cm.Parameters.Add(New SqlParameter("@PRES2", SqlDbType.VarChar, 100))
            If Not String.IsNullOrEmpty(PRES2) Then
                cm.Parameters("@PRES2").Value = PRES2
            Else
                cm.Parameters("@PRES2").Value = System.DBNull.Value
            End If

            cm.Parameters.Add(New SqlParameter("@PRES3", SqlDbType.VarChar, 100))
            If Not String.IsNullOrEmpty(PRES3) Then
                cm.Parameters("@PRES3").Value = PRES3
            Else
                cm.Parameters("@PRES3").Value = System.DBNull.Value
            End If

            cm.Parameters.Add(New SqlParameter("@PRES4", SqlDbType.VarChar, 100))
            If Not String.IsNullOrEmpty(PRES4) Then
                cm.Parameters("@PRES4").Value = PRES4
            Else
                cm.Parameters("@PRES4").Value = System.DBNull.Value
            End If

            da.SelectCommand = cm
            da.Fill(ds)
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
        Return ds
    End Function
    ''' <summary>
    ''' Función que devuelve la denominación de una partida dada
    ''' </summary>
    ''' <param name="idioma">idioma en que se quiere recuperar la información</param>
    ''' <param name="PRES0">Código del árbol presupuestario del que se quiere recuperar la partida (PRES5 en otros sitios)</param>
    ''' <param name="PRES1">Código de la partida de nivel 1</param>
    ''' <param name="PRES2">Código de la partida de nivel 2 (si la hay)</param>
    ''' <param name="PRES3">Código de la partida de nivel 3 (si la hay)</param>
    ''' <param name="PRES4">Código de la partida de nivel 4 (si la hay)</param>    
    ''' <returns>Datatable con la información</returns>
    ''' <remarks>Llamada desde FSNServer.PartidasPRES5.DenominacionPartidaPRES5. Tiempo máximo inferior a 1 seg.</remarks>   
    Public Function PRES5_Denominacion_Partida(ByVal idioma As FSNLibrary.Idioma, ByVal PRES0 As String, ByVal PRES1 As String, ByVal PRES2 As String, ByVal PRES3 As String, ByVal PRES4 As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "[FSSM_DEVOLVER_DENOMINACION_PARTIDA]"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add(New SqlParameter("@IDI", SqlDbType.VarChar, 3))
            If Not String.IsNullOrEmpty(idioma) Then
                cm.Parameters("@IDI").Value = idioma.ToString()
            Else
                cm.Parameters("@IDI").Value = "SPA"
            End If

            cm.Parameters.Add(New SqlParameter("@PRES0", SqlDbType.VarChar, 100))
            If Not String.IsNullOrEmpty(PRES0) Then
                cm.Parameters("@PRES0").Value = PRES0
            Else
                cm.Parameters("@PRES0").Value = System.DBNull.Value
            End If

            cm.Parameters.Add(New SqlParameter("@PRES1", SqlDbType.VarChar, 100))
            If Not String.IsNullOrEmpty(PRES1) Then
                cm.Parameters("@PRES1").Value = PRES1
            Else
                cm.Parameters("@PRES1").Value = System.DBNull.Value
            End If

            cm.Parameters.Add(New SqlParameter("@PRES2", SqlDbType.VarChar, 100))
            If Not String.IsNullOrEmpty(PRES2) Then
                cm.Parameters("@PRES2").Value = PRES2
            Else
                cm.Parameters("@PRES2").Value = System.DBNull.Value
            End If

            cm.Parameters.Add(New SqlParameter("@PRES3", SqlDbType.VarChar, 100))
            If Not String.IsNullOrEmpty(PRES3) Then
                cm.Parameters("@PRES3").Value = PRES3
            Else
                cm.Parameters("@PRES3").Value = System.DBNull.Value
            End If

            cm.Parameters.Add(New SqlParameter("@PRES4", SqlDbType.VarChar, 100))
            If Not String.IsNullOrEmpty(PRES4) Then
                cm.Parameters("@PRES4").Value = PRES4
            Else
                cm.Parameters("@PRES4").Value = System.DBNull.Value
            End If

            da.SelectCommand = cm
            da.Fill(ds)
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
        Return ds
    End Function
	''' <summary>
	''' Función que actualiza el comentario de un registro de histórico de una partida
	''' </summary>       
	''' <param name="ID">Id del histórico</param>
	''' <param name="Coment">Nuevo comentario</param>
	''' <param name="Usu_Coment">Usuario que hace el comentario</param>           
	''' <remarks>Llamada desde PartidasPres5. Tiempo máximo inferior a 1 seg.</remarks>    
	Public Sub PRES5_ActualizarComentHistorico(ByVal ID As Integer, ByVal Coment As String, ByVal Usu_Coment As String)
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "[FSSM_ACTUALIZAR_PRES5_HIST_COMENT]"
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.Add(New SqlParameter("@ID", SqlDbType.Int))
			cm.Parameters("@ID").Value = ID
			cm.Parameters.Add(New SqlParameter("@COMENT", SqlDbType.NVarChar, 2000))
			cm.Parameters("@COMENT").Value = Coment
			cm.Parameters.Add(New SqlParameter("@USU_COMENT", SqlDbType.VarChar, 50))
			cm.Parameters("@USU_COMENT").Value = Usu_Coment
			cm.ExecuteNonQuery()
		Catch e As Exception
			Throw e
		Finally
			cn.Dispose()
			cm.Dispose()
		End Try
	End Sub
	''' <summary>
	''' Función que devuelve el histórico de una partida dada
	''' </summary>    
	''' <param name="PRES0">Código del árbol presupuestario del que se quiere recuperar la partida (PRES5 en otros sitios)</param>
	''' <param name="PRES1">Código de la partida de nivel 1</param>
	''' <param name="PRES2">Código de la partida de nivel 2 (si la hay)</param>
	''' <param name="PRES3">Código de la partida de nivel 3 (si la hay)</param>
	''' <param name="PRES4">Código de la partida de nivel 4 (si la hay)</param>  
	''' <param name="Anyo">Código de la partida de nivel 1</param>   
	''' <returns>Datatable con la información</returns>
	''' <remarks>Llamada desde FSNServer.PartidasPRES5.PRES5_HistoricoPartida. Tiempo máximo inferior a 1 seg.</remarks>    
	Public Function PRES5_HistoricoPartida_Anyo(ByVal PRES0 As String, ByVal PRES1 As String, ByVal PRES2 As String, ByVal PRES3 As String, ByVal PRES4 As String, ByVal Anyo As Integer) As DataSet
		Authenticate()

		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Dim ds As New DataSet
		Dim da As New SqlDataAdapter
		Try
			cn.Open()
			cm.Connection = cn
			cm.CommandText = "FSSM_DEVOLVER_HISTORICO_PARTIDA_ANYO"
			cm.CommandType = CommandType.StoredProcedure

			cm.Parameters.AddWithValue("@PRES0", PRES0)
			If Not String.IsNullOrEmpty(PRES1) Then cm.Parameters.AddWithValue("@PRES1", PRES1)
			If Not String.IsNullOrEmpty(PRES2) Then cm.Parameters.AddWithValue("@PRES2", PRES2)
			If Not String.IsNullOrEmpty(PRES3) Then cm.Parameters.AddWithValue("@PRES3", PRES3)
			If Not String.IsNullOrEmpty(PRES4) Then cm.Parameters.AddWithValue("@PRES4", PRES4)
			cm.Parameters.AddWithValue("ANYO", Anyo)

			da.SelectCommand = cm
			da.Fill(ds)
		Catch e As Exception
			Throw e
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
		Return ds
	End Function
#End Region
#End Region
End Class