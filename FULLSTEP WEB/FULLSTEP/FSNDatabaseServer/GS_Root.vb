﻿Imports System.Configuration
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Exception
Imports System.IO
Imports System.Object
Imports Fullstep.FSNLibrary
Partial Public Class Root
#Region "GS"
    Public Function GS_Reunion_Agenda(ByVal Fecha As DateTime, ByVal Idioma As String) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "GS_REUNION_AGENDA"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@FECHA", Fecha)
            cm.Parameters.AddWithValue("@IDIOMA", Idioma)
            da.SelectCommand = cm
            da.Fill(dr)

            dr.Tables(0).TableName = "AGENDA"
            dr.Tables(1).TableName = "PROCESO"
            dr.Tables(2).TableName = "DATOSGRUPOS"
            dr.Tables(3).TableName = "ITEM"
            dr.Tables(4).TableName = "IMPLICADO"
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function GS_Reunion_Acta(ByVal Fecha As DateTime, ByVal Idioma As String) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "GS_REUNION_ACTA"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@FECHA", Fecha)
            cm.Parameters.AddWithValue("@IDIOMA", Idioma)
            da.SelectCommand = cm
            da.Fill(dr)

            dr.Tables(0).TableName = "ACTA"
            dr.Tables(1).TableName = "ASISTENTE"
            dr.Tables(2).TableName = "IMPLICADO"
            dr.Tables(3).TableName = "PROCESO"
            dr.Tables(4).TableName = "MATERIALES_PROCESO"
            dr.Tables(5).TableName = "DATOSGRUPOS"
            dr.Tables(6).TableName = "PROVEEDORES"
            dr.Tables(7).TableName = "PROVEEDORES_NOADJUDICADOS"
            dr.Tables(8).TableName = "ADJUDICACIONES"
            dr.Tables(9).TableName = "GRUPOS_ASIGNACIONES"
            dr.Tables(10).TableName = "ATRIBUTOS"
            dr.Tables(11).TableName = "ITEM_TOD"
            dr.Tables(12).TableName = "ADJUDICACIONES_ITEM"
            dr.Tables(13).TableName = "ULTIMAS_OFERTAS_GRUPO"
            dr.Tables(14).TableName = "OFERTAS_ITEM"
            dr.Tables(15).TableName = "ADJUDICACIONES_GRUPO"
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function GS_Plantillas_Visor(ByVal Idioma As String) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "GS_PLANTILLAS_VISOR"
            cm.Parameters.AddWithValue("@IDIOMA", Idioma)
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(dr)
            dr.Tables(0).TableName = "PLANTILLAS"
            dr.Tables(1).TableName = "UONS"

            'RELATIONS
            Dim parentCols(0) As DataColumn
            Dim childCols(0) As DataColumn
            parentCols(0) = dr.Tables(0).Columns("ID")
            childCols(0) = dr.Tables(1).Columns("ID")
            dr.Relations.Add("REL_ID_CUSTOM_TEMPLATE", parentCols, childCols, False)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function GS_Visor_Plantillas_DetallePlantilla(ByVal Id As Integer, ByVal Idioma As String) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "GS_PLANTILLAS_PLANTILLA_DETALLE"
            cm.Parameters.AddWithValue("@ID", Id)
            cm.Parameters.AddWithValue("@IDIOMA", Idioma)
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(dr)
            dr.Tables(0).TableName = "IDIOMAS"
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Sub GS_Plantilla_Alta(ByVal Per As String, ByVal PlantillaDefecto As Boolean, ByVal FormatoDefecto As Integer, _
            ByVal Documento As Integer, ByVal Vista As Integer, ByVal dtUONsPlantilla As DataTable, ByVal dtAdjuntos As DataTable)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "GS_PLANTILLAS_PLANTILLA_ADD"
            With cm.Parameters
                .AddWithValue("@PER", Per)
                .AddWithValue("@DEFECTO", PlantillaDefecto)
                .AddWithValue("@FORMATO", FormatoDefecto)
                .AddWithValue("@DOCUMENTO", Documento)
                If Not Vista = 0 Then .AddWithValue("@VISTA", Vista)
                .Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output
            End With
            cm.CommandType = CommandType.StoredProcedure
            cm.ExecuteNonQuery()
            Dim IdPlantilla As Long = cm.Parameters("@ID").Value

            cm = New SqlCommand("GS_PLANTILLAS_PLANTILLA_UON", cn)
            cm.CommandType = CommandType.StoredProcedure
            With cm.Parameters
                .AddWithValue("@CUSTOM_TEMPLATE", IdPlantilla)
                .Add("@UON1", SqlDbType.NVarChar, 50, "UON1")
                .Add("@UON2", SqlDbType.NVarChar, 50, "UON2")
                .Add("@UON3", SqlDbType.NVarChar, 50, "UON3")
            End With
            da.InsertCommand = cm
            da.Update(dtUONsPlantilla)

            cm = New SqlCommand("GS_PLANTILLAS_PLANTILLA_ADJUNTO_ADD", cn)
            cm.CommandType = CommandType.StoredProcedure
            With cm.Parameters
                .AddWithValue("@CUSTOM_TEMPLATE", IdPlantilla)
                .Add("@IDIOMA", SqlDbType.NVarChar, 50, "IDIOMA")
                .Add("@NOM", SqlDbType.NVarChar, 300, "NOMBRE")
                .Add("@DEN", SqlDbType.NVarChar, 100, "DENOMINACION")
                .Add("@COMMENT", SqlDbType.NVarChar, 300, "COMENTARIOS")
                .Add("@SIZE", SqlDbType.Int, 0, "SIZE")
            End With
            da.InsertCommand = cm
            da.Update(dtAdjuntos)

            Dim nombreFichero As String
            For Each row As DataRow In dtAdjuntos.Rows
                nombreFichero = row("PATH")
                If Not nombreFichero = String.Empty Then
                    Dim input As System.IO.FileStream = New System.IO.FileStream(nombreFichero, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                    Dim buffer(input.Length - 1) As Byte
                    input.Read(buffer, 0, buffer.Length)
                    input.Close()
                    GrabarNuevaPlantilla(IdPlantilla, row("IDIOMA"), buffer)
                End If
            Next
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Realiza el guardado del adjunto a traves de FILESTREAM
    ''' </summary>
    ''' <param name="IdPlantilla">ID PLANTILLA</param>
    ''' <param name="Buffer">Buffer que ha cogido el adjunto</param>
    ''' <remarks>Llamada desde:Create; Tiempo máximo:0,6</remarks>
    Public Sub GrabarNuevaPlantilla(ByVal IdPlantilla As Long, ByVal Idioma As String, ByVal Buffer As Byte())
        Dim cn As New SqlConnection
        Dim mi_impersionationContext As Security.Principal.WindowsImpersonationContext

        cn = NuevaConexionFileStream()
        mi_impersionationContext = RealizarSuplantacion()
        cn.Open()

        Dim tx As SqlTransaction = cn.BeginTransaction()
        Dim oSqlFileStream As SqlTypes.SqlFileStream = GetSqlFileStreamAdjuntoPlantilla(IdPlantilla, Idioma, cn, tx, , False)
        If Not oSqlFileStream Is Nothing Then
            oSqlFileStream.Write(Buffer, 0, Buffer.Length)
            oSqlFileStream.Close()
        End If
        tx.Commit()
        cn.Close()
        DeshacerSuplantacion(mi_impersionationContext)
    End Sub
    ''' <summary>
    ''' Para poder trabajar con FILESTREAM necesitamos suplantacion de identidad, es decir, necesitamos un usuario SQL que pueda
    ''' trabajar con este tipo de ficheros. Para ellos necesitamos que el usuario este creado con esos permisos en SQL
    ''' </summary>
    ''' <returns>Contexto de la suplantacion</returns>
    ''' <remarks>Llamada desde:GrabarNuevoAdjunto; Tiempo máximo:0seg.</remarks>
    Private Function GetSqlFileStreamAdjuntoPlantilla(ByVal IdPlantilla As Long, ByVal Idioma As String, ByVal cn As SqlConnection, ByVal tx As SqlTransaction, _
            Optional ByVal TipoFichero As TipoFicheroFileStream = TipoFicheroFileStream.Adjunto, _
            Optional ByVal EsLectura As Boolean = True) As SqlTypes.SqlFileStream
        Dim sPath As String = String.Empty
        Dim arTransactionContext() As Byte = Nothing
        GetPathNameAndTxContextAdjuntoPlantilla(IdPlantilla, Idioma, cn, tx, sPath, arTransactionContext)
        '4 - open the filestream to the blob
        If Not sPath Is Nothing And Not arTransactionContext Is Nothing Then
            If EsLectura Then
                GetSqlFileStreamAdjuntoPlantilla = New SqlTypes.SqlFileStream(sPath, arTransactionContext, FileAccess.Read)
            Else
                GetSqlFileStreamAdjuntoPlantilla = New SqlTypes.SqlFileStream(sPath, arTransactionContext, FileAccess.Write)
            End If
        Else
            GetSqlFileStreamAdjuntoPlantilla = Nothing
        End If
    End Function
    ''' <summary>
    ''' Obtiene la referencia del adjunto en BBDD para poder trabajar (Escribir) con el 
    ''' </summary>
    ''' <param name="cn">Conexion</param>        
    ''' <param name="tx">Transaccion lectura adjunto</param>
    ''' <param name="Path">Path del adjunto</param>
    ''' <param name="TxContext">TRANSACTIONCONTEXT del adjunto</param>
    ''' <remarks>Llamada desde:GetSqlFileStreamForWriting; Tiempo máximo:0,35seg.</remarks>
    Private Sub GetPathNameAndTxContextAdjuntoPlantilla(ByVal IdPlantilla As Long, ByVal Idioma As String, ByVal cn As SqlConnection, ByVal tx As SqlTransaction, _
            ByRef Path As String, ByRef TxContext As Byte())
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "GS_PLANTILLAS_PLANTILLA_LEER"
        With cm.Parameters
            .AddWithValue("@CUSTOM_TEMPLATE", IdPlantilla)
            .AddWithValue("@IDIOMA", Idioma)
        End With
        cm.Connection = cn
        cm.Transaction = tx
        Dim da As New SqlDataAdapter
        da.SelectCommand = cm
        Dim ds As New System.Data.DataSet
        da.Fill(ds)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim oRow As DataRow = ds.Tables(0).Rows(0)
            If Not oRow.IsNull("PATHNAME") Then Path = oRow("PATHNAME")
            If Not oRow.IsNull("TRANSACTIONCONTEXT") Then TxContext = CType(oRow("TRANSACTIONCONTEXT"), Byte()) 'reader.GetSqlBytes(1).Buffer;
            If Path Is Nothing Then
                'Si se inserta un registro con la columna varbinary(max) a NULL no se crea archivo y FilePath devuelve nulo, con lo que no se puede
                'crear el sqlFileStream.
                'Insertando '' en la columna varbinary(max) se crea un registro de longitud cero y ya se puede obtener un handle para el archivo
                Dim oCom As New SqlCommand
                oCom.CommandType = CommandType.Text
                oCom.CommandText = "UPDATE CUSTOM_TEMPLATE_IDIOMA SET DATA=cast('' as varbinary(max)) WHERE CUSTOM_TEMPLATE=" & IdPlantilla & " AND IDIOMA='" & Idioma & "'"
                oCom.Connection = cn
                oCom.Transaction = tx
                oCom.ExecuteNonQuery()
                Dim obj As Object = cm.ExecuteScalar()
                If Not obj.Equals(DBNull.Value) Then Path = DirectCast(obj, String)
            End If
        End If
    End Sub
    Public Sub GS_Plantilla_Editar(ByVal IdPlantilla As Integer, ByVal Per As String, ByVal PlantillaDefecto As Boolean, ByVal FormatoDefecto As Integer, _
            ByVal Documento As Integer, ByVal Vista As Integer, ByVal dtUONsPlantilla As DataTable, ByVal dtAdjuntos As DataTable)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection=cn
            cm.CommandText = "GS_PLANTILLAS_PLANTILLA_UPDATE"
            With cm.Parameters
                .AddWithValue("@ID", IdPlantilla)
                .AddWithValue("@PER", Per)
                .AddWithValue("@DEFECTO", PlantillaDefecto)
                .AddWithValue("@FORMATO", FormatoDefecto)
                .AddWithValue("@DOCUMENTO", Documento)
                If Not Vista = 0 Then .AddWithValue("@VISTA", Vista)
            End With
            cm.CommandType = CommandType.StoredProcedure
            cm.ExecuteNonQuery()

            cm = New SqlCommand("GS_PLANTILLAS_PLANTILLA_DELETE_UON", cn)
            With cm.Parameters
                .AddWithValue("@CUSTOM_TEMPLATE", IdPlantilla)
            End With
            cm.CommandType = CommandType.StoredProcedure
            cm.ExecuteNonQuery()

            cm = New SqlCommand("GS_PLANTILLAS_PLANTILLA_UON", cn)
            cm.CommandType = CommandType.StoredProcedure
            With cm.Parameters
                .AddWithValue("@CUSTOM_TEMPLATE", IdPlantilla)
                .Add("@UON1", SqlDbType.NVarChar, 50, "UON1")
                .Add("@UON2", SqlDbType.NVarChar, 50, "UON2")
                .Add("@UON3", SqlDbType.NVarChar, 50, "UON3")
            End With
            da.InsertCommand = cm
            da.Update(dtUONsPlantilla)

            cm = New SqlCommand("GS_PLANTILLAS_PLANTILLA_ADJUNTO_UPDATE", cn)
            cm.CommandType = CommandType.StoredProcedure
            With cm.Parameters
                .AddWithValue("@CUSTOM_TEMPLATE", IdPlantilla)
                .Add("@IDIOMA", SqlDbType.NVarChar, 50, "IDIOMA")
                .Add("@NOM", SqlDbType.NVarChar, 300, "NOMBRE")
                .Add("@DEN", SqlDbType.NVarChar, 100, "DENOMINACION")
                .Add("@COMMENT", SqlDbType.NVarChar, 300, "COMENTARIOS")
                .Add("@SIZE", SqlDbType.Int, 0, "SIZE")
            End With
            da.InsertCommand = cm
            da.Update(dtAdjuntos)

            Dim nombreFichero As String
            For Each row As DataRow In dtAdjuntos.Rows
                nombreFichero = row("PATH")
                If Not nombreFichero = String.Empty Then
                    Dim input As System.IO.FileStream = New System.IO.FileStream(nombreFichero, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                    Dim buffer(input.Length - 1) As Byte
                    input.Read(buffer, 0, buffer.Length)
                    input.Close()
                    GrabarNuevaPlantilla(IdPlantilla, row("IDIOMA"), buffer)
                End If
            Next
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    Public Sub GS_Plantilla_Eliminar(ByVal idsPlantilla As DataTable)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.connection = cn
            cm.CommandText = "GS_PLANTILLAS_PLANTILLA_DELETE"
            cm.CommandType = CommandType.StoredProcedure
            With cm.Parameters
                .Add("@ID", SqlDbType.Int, 0, "ID")
            End With
            da.InsertCommand = cm
            da.Update(idsPlantilla)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Realiza la lectura del adjunto a traves de FILESTREAM
    ''' </summary>
    ''' <param name="IdPlantilla">ID PLANTILLA</param>
    ''' <param name="Idioma">Idioma</param>
    ''' <returns>adjunto</returns>
    ''' <remarks>Llamada desde:Plantillas.vb/GS_Plantillas_GetAdjuntoPlantilla; Tiempo mÃ¡ximo:0,6</remarks>
    Public Function GS_Plantillas_GetAdjuntoPlantilla(ByVal IdPlantilla As Integer, ByVal Idioma As String) As Byte()
        Dim cn As New SqlConnection
        Dim mi_impersionationContext As Security.Principal.WindowsImpersonationContext

        cn = NuevaConexionFileStream()
        mi_impersionationContext = RealizarSuplantacion()
        cn.Open()

        Dim tx As SqlTransaction = cn.BeginTransaction()
        Dim oSqlFileStream As SqlTypes.SqlFileStream = GetSqlFileStreamAdjuntoPlantilla(IdPlantilla, Idioma, cn, tx, , True)
        If Not oSqlFileStream Is Nothing Then
            Dim Buffer(oSqlFileStream.Length - 1) As Byte
            oSqlFileStream.Read(Buffer, 0, Buffer.Length)
            oSqlFileStream.Close()
            GS_Plantillas_GetAdjuntoPlantilla = Buffer
        Else
            GS_Plantillas_GetAdjuntoPlantilla = Nothing
        End If
        tx.Commit()
        cn.Close()
        DeshacerSuplantacion(mi_impersionationContext)
    End Function
    Public Sub GS_Plantilla_Defecto(ByVal idPlantilla As Integer)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "GS_PLANTILLAS_PLANTILLA_DEFECTO"
            cm.CommandType = CommandType.StoredProcedure
            With cm.Parameters
                .AddWithValue("@ID", idPlantilla)
            End With
            cm.ExecuteNonQuery()
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    Public Function GS_Plantillas_UONs(ByVal Idioma As String) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "GS_PLANTILLAS_OBTENER_UONS"
            cm.Parameters.AddWithValue("@IDI", Idioma)
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function GS_Obtener_Plantillas_Usu_Documento(ByVal Usu As String, ByVal Documento As Integer, ByVal Idioma As String, _
            ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "GS_OBTENER_PLANTILLAS_DOCUMENTO_VISTA_USU"
            cm.Parameters.AddWithValue("@USU", Usu)
            cm.Parameters.AddWithValue("@DOCUMENTO", Documento)
            cm.Parameters.AddWithValue("@IDIOMA", Idioma)
            If Not UON1 Is String.Empty Then cm.Parameters.AddWithValue("@UON1", UON1)
            If Not UON2 Is String.Empty Then cm.Parameters.AddWithValue("@UON2", UON2)
            If Not UON3 Is String.Empty Then cm.Parameters.AddWithValue("@UON3", UON3)
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Sub GS_Plantilla_Defecto_Usu(ByVal Usu As String, ByVal idPlantilla As Integer, ByVal formato As Integer)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "GS_PLANTILLAS_PLANTILLA_DOCUMENTO_DEFECTO_USU"
            cm.CommandType = CommandType.StoredProcedure
            With cm.Parameters
                .AddWithValue("@ID", idPlantilla)
                .AddWithValue("@USU", Usu)
                .AddWithValue("@FORMATO", formato)
            End With
            cm.ExecuteNonQuery()
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    Public Sub GS_Plantilla_FormatoDefecto(ByVal idsPlantilla As DataTable, ByVal formato As Integer)
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "GS_PLANTILLAS_FORMATO_DEFECTO_PLANTILLA"
            cm.CommandType = CommandType.StoredProcedure
            With cm.Parameters
                .Add("@ID", SqlDbType.Int, 0, "ID")
                .AddWithValue("@FORMATO", formato)
            End With
            da.InsertCommand = cm
            da.Update(idsPlantilla)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
#End Region
#Region "GSW Data Access Methods"
#Region "Adjudicaciones"
    ''' <summary>
    ''' Funcion que devuelve un dataset con los datos de las adjudicaciones de un proceso.Se devuelve las empresas,proveedores y grupos
    ''' </summary>
    ''' <param name="Anyo">Año del proceso</param>
    ''' <param name="Gmn1">Commodity del proceso</param>
    ''' <param name="CodigoProce">Codigo del proceso</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Adjudicaciones_ObtenerAdjudicacionesProceso(ByVal Anyo As Short, ByVal Gmn1 As String, ByVal CodigoProce As Integer) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSGSW_DEVOLVER_DATOSADJUDICACION"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@ANYO", Anyo)
            cm.Parameters.AddWithValue("@GMN1", Gmn1)
            cm.Parameters.AddWithValue("@PROCE", CodigoProce)

            cm.CommandTimeout = 300

            da.SelectCommand = cm

            da.Fill(dr)

            Dim query = From Datos In dr.Tables(0).AsEnumerable() _
                        Select Datos!EMPCOD, Datos!EMPDEN, Datos!NIF, Datos!EST_EMP, Datos!ERP Distinct

            Dim dtEmp As New DataTable
            dtEmp = query.CopyAnonymusToDataTable
            dtEmp.TableName = "Empresas"
            Dim dtPk0(0) As DataColumn
            dtPk0(0) = dtEmp.Columns("EMPCOD")
            dtEmp.PrimaryKey = dtPk0

            Dim queryProve = From Datos In dr.Tables(0).AsEnumerable() _
                        Select Datos!EMPCOD, Datos!EMPDEN, Datos!PROVECOD, Datos!PROVEDEN, Datos!EST_PROVE Distinct

            Dim dtProve As New DataTable
            dtProve = queryProve.CopyAnonymusToDataTable
            dtProve.TableName = "Proveedores"
            Dim dtPk1(1) As DataColumn
            dtPk1(0) = dtProve.Columns("EMPCOD")
            dtPk1(1) = dtProve.Columns("PROVECOD")
            dtProve.PrimaryKey = dtPk1

            Dim queryGrupos = From Datos In dr.Tables(0).AsEnumerable() _
                        Select Datos!EMPCOD, Datos!EMPDEN, Datos!PROVECOD, Datos!PROVEDEN, Datos!GRUPOID, Datos!GRUPOCOD, Datos!GRUPODEN, Datos!EST_GRUPO Distinct

            dr = Nothing

            Dim dtGrupos As New DataTable
            dtGrupos = queryGrupos.CopyAnonymusToDataTable
            dtGrupos.TableName = "Grupos"
            Dim dtPk2(2) As DataColumn
            dtPk2(0) = dtGrupos.Columns("EMPCOD")
            dtPk2(1) = dtGrupos.Columns("PROVECOD")
            dtPk2(2) = dtGrupos.Columns("GRUPOCOD")
            dtGrupos.PrimaryKey = dtPk2

            Dim dsAdjudicaciones As New DataSet("Adjudicaciones")
            dsAdjudicaciones.Tables.Add(dtEmp)
            dsAdjudicaciones.Tables.Add(dtProve)
            dsAdjudicaciones.Tables.Add(dtGrupos)

            Dim parentCols(0) As DataColumn
            Dim childCols(0) As DataColumn

            parentCols(0) = dsAdjudicaciones.Tables("Empresas").Columns("EMPCOD")
            childCols(0) = dsAdjudicaciones.Tables("Proveedores").Columns("EMPCOD")

            dsAdjudicaciones.Relations.Add("RELACION_1", parentCols, childCols, False)

            ReDim parentCols(1)
            ReDim childCols(1)

            parentCols(0) = dsAdjudicaciones.Tables("Proveedores").Columns("EMPCOD")
            parentCols(1) = dsAdjudicaciones.Tables("Proveedores").Columns("PROVECOD")

            childCols(0) = dsAdjudicaciones.Tables("Grupos").Columns("EMPCOD")
            childCols(1) = dsAdjudicaciones.Tables("Grupos").Columns("PROVECOD")

            dsAdjudicaciones.Relations.Add("RELACION_2", parentCols, childCols, False)

            Return dsAdjudicaciones
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>Funcion que devuelve un dataset con los datos de las adjudicaciones anteriores de los artÃ­culos de un proceso</summary>
    ''' <param name="Anyo">AÃ±o del proceso</param>
    ''' <param name="Gmn1">Commodity del proceso</param>
    ''' <param name="codProce">Codigo del proceso</param>
    ''' <param name="IdEmp">Id empresa</param>
    ''' <param name="sCodProve">Codigo del proveedor</param>
    ''' <param name="iIdGrupo">Id del grupo</param>
    ''' <param name="iIdItem">Id del item</param>
    ''' <param name="AnyoProceAdj">AÃ±o del proceso al que pertenecen las adjudicaciones que se estÃ¡n buscando</param>
    ''' <param name="Gmn1ProceAdj">GMN del proceso al que pertenecen las adjudicaciones que se estÃ¡n buscando</param>
    ''' <param name="codProceProceAdj">Cod. del proceso al que pertenecen las adjudicaciones que se estÃ¡n buscando</param>
    ''' <param name="sCodProveProceAdj">Cod. del proveedor al que pertenecen las adjudicaciones que se estÃ¡n buscando</param>
    ''' <param name="EstadoActual">Estado de integración actual de los ítems para los que se busca las adj. anteriores</param>
    ''' <returns>dataset con los datos</returns>
    ''' <remarks></remarks>
    Public Function ObtenerAdjsAnterioresProceso(ByVal Anyo As Integer, ByVal Gmn1 As String, ByVal codProce As Integer, ByVal IdEmp As Integer, Optional ByVal sCodProve As String = Nothing, Optional ByVal iIdGrupo As Integer = 0,
                                                 Optional ByVal iIdItem As Integer = 0, Optional ByVal AnyoProceAdj As Integer = 0, Optional ByVal Gmn1ProceAdj As String = Nothing, Optional ByVal codProceProceAdj As Integer = 0,
                                                 Optional ByVal sCodProveProceAdj As String = Nothing, Optional ByVal EstadoActual As Short = -2) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSGSW_DEVOLVER_ADJSANTPROCE"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@ANYO", Anyo)
            cm.Parameters.AddWithValue("@GMN1", Gmn1)
            cm.Parameters.AddWithValue("@PROCE", codProce)
            cm.Parameters.AddWithValue("@EMP", IdEmp)
            If Not sCodProve Is Nothing Then cm.Parameters.AddWithValue("@PROVE", sCodProve)
            If iIdGrupo <> 0 Then cm.Parameters.AddWithValue("@IDGRUPO", iIdGrupo)
            If iIdItem <> 0 Then cm.Parameters.AddWithValue("@IDITEM", iIdItem)
            If AnyoProceAdj <> 0 Then cm.Parameters.AddWithValue("@ANYO_PROCEADJ", AnyoProceAdj)
            If Not Gmn1ProceAdj Is Nothing Then cm.Parameters.AddWithValue("@GMN1_PROCEADJ", Gmn1ProceAdj)
            If codProceProceAdj <> 0 Then cm.Parameters.AddWithValue("@PROCE_PROCEADJ", codProceProceAdj)
            If sCodProveProceAdj <> String.Empty Then cm.Parameters.AddWithValue("@PROVE_PROCEADJ", sCodProveProceAdj)
            If EstadoActual <> -2 Then cm.Parameters.AddWithValue("@ESTADO_ACTUAL", EstadoActual)

            da.SelectCommand = cm
            da.Fill(ds)

            Return ds
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Funcion que devuelve un dataset con los datos de las adjudicaciones de un proceso.Se devuelve las empresas,proveedores y grupos
    ''' </summary>
    ''' <param name="Anyo">Año del proceso</param>
    ''' <param name="Gmn1">Commodity del proceso</param>
    ''' <param name="CodigoProce">Codigo del proceso</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Adjudicaciones_ObtenerProveedoresAdjudicados(ByVal sIdioma As String, ByVal idEmp As Long, ByVal Anyo As Short, ByVal Gmn1 As String, ByVal CodigoProce As Integer) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSGSW_DEVOLVER_PROVEEDORESADJUDICADOS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDI", sIdioma)
            cm.Parameters.AddWithValue("@IDEMP", idEmp)
            cm.Parameters.AddWithValue("@ANYO", Anyo)
            cm.Parameters.AddWithValue("@GMN1", Gmn1)
            cm.Parameters.AddWithValue("@PROCE", CodigoProce)

            da.SelectCommand = cm
            da.Fill(dr)

            Dim query = From Datos In dr.Tables(0).AsEnumerable() _
                        Select Datos!PROVECOD, Datos!PROVEDEN, Datos!NUM_DISTR, Datos!EST_INTEGRACION Distinct

            Dim dtProves As New DataTable
            dtProves = query.CopyAnonymusToDataTable
            dtProves.TableName = "Proveedores"
            Dim dtPk0(0) As DataColumn
            dtPk0(0) = dtProves.Columns("PROVECOD")
            dtProves.PrimaryKey = dtPk0

            Dim queryProve = From Datos In dr.Tables(0).AsEnumerable() _
                            Where Datos.Item("COD_ERP") IsNot DBNull.Value _
                        Select Datos!PROVECOD, Datos!PROVEDEN, Datos!COD_ERP, Datos!DEN_ERP, Datos!CAMPO1, Datos!CAMPO2, Datos!CAMPO3, Datos!CAMPO4, Datos!ACTCAMPO1 _
                        , Datos!ACTCAMPO2, Datos!ACTCAMPO3, Datos!ACTCAMPO4 Distinct

            Dim dtCodsERP As New DataTable
            dtCodsERP = queryProve.CopyAnonymusToDataTable
            dtCodsERP.TableName = "CodigosERPyCamposPer"
            Dim dtPk1(1) As DataColumn
            dtPk1(0) = dtCodsERP.Columns("PROVECOD")
            dtPk1(1) = dtCodsERP.Columns("COD_ERP")
            dtCodsERP.PrimaryKey = dtPk1

            If dtCodsERP.Rows.Count > 0 Then
                For ind = 1 To 4
                    If dtCodsERP(0)("ACTCAMPO" & ind) = 1 Then 'Se va comprobando si los campos personalizables estan activados
                        dtCodsERP.Columns("CAMPO" & ind).ColumnName = dr.Tables(1)(ind - 1)("DEN") 'Se le cambia el nombre a la columna para que sea la del campo personalizable
                        dtCodsERP.Columns.Remove("ACTCAMPO" & ind)
                    Else
                        dtCodsERP.Columns.Remove("ACTCAMPO" & ind)
                        dtCodsERP.Columns.Remove("CAMPO" & ind)
                    End If
                Next
            Else
                For ind = 1 To 4
                    dtCodsERP.Columns.Remove("ACTCAMPO" & ind)
                    dtCodsERP.Columns.Remove("CAMPO" & ind)
                Next
            End If

            Dim dsProveedoresAdjudicados As New DataSet("ProveedoresAdjudicados")
            dsProveedoresAdjudicados.Tables.Add(dtProves)
            dsProveedoresAdjudicados.Tables.Add(dtCodsERP)
            dsProveedoresAdjudicados.Tables.Add(dr.Tables(2).Copy)

            dr = Nothing

            'Se crea la relacion entre los proveedores adjudicados y sus correspondientes Codigos ERP
            Dim parentCols(0) As DataColumn
            Dim childCols(0) As DataColumn

            parentCols(0) = dsProveedoresAdjudicados.Tables("Proveedores").Columns("PROVECOD")
            childCols(0) = dsProveedoresAdjudicados.Tables("CodigosERPyCamposPer").Columns("PROVECOD")

            dsProveedoresAdjudicados.Relations.Add("RELACION_1", parentCols, childCols, False)
            Return dsProveedoresAdjudicados
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Funcion que devuelve los atributos a nivel de proceso
    ''' </summary>
    ''' <param name="Anyo">Año del proceso</param>
    ''' <param name="Gmn1">Commodity del proceso</param>
    ''' <param name="CodigoProce">Codigo del proceso</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Adjudicaciones_CargarAtributosProceso(ByVal Anyo As Short, ByVal Gmn1 As String, ByVal CodigoProce As Integer, ByVal idEmp As Long, ByVal CodProve As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSGSW_DEVOLVER_ATRIBUTOSPROCESO"

            cm.CommandType = CommandType.StoredProcedure

            cm.Parameters.AddWithValue("@ANYO", Anyo)
            cm.Parameters.AddWithValue("@GMN1", Gmn1)
            cm.Parameters.AddWithValue("@PROCE", CodigoProce)
            cm.Parameters.AddWithValue("@IDEMP", idEmp)
            cm.Parameters.AddWithValue("@PROVE", CodProve)

            da.SelectCommand = cm
            da.Fill(dr)

            Dim query = From Datos In dr.Tables(0).AsEnumerable() _
                        Select Datos!ID, Datos!COD, Datos!DEN, Datos!VALOR_TEXT, Datos!VALOR_NUM, Datos!VALOR_FEC, Datos!VALOR_BOOL, Datos!TIPO, Datos!INTRO, Datos!OBLIGATORIO, Datos!VALIDACION_ERP Distinct

            Dim dtAtrib As New DataTable
            dtAtrib = query.CopyAnonymusToDataTable
            dtAtrib.TableName = "Atributos"
            Dim dtPk0(0) As DataColumn
            dtPk0(0) = dtAtrib.Columns("ID")
            dtAtrib.PrimaryKey = dtPk0

            Dim queryDatosLista = From Datos In dr.Tables(0).AsEnumerable() _
                        Where Datos.Item("INTRO") = 1 _
                        Select Datos!ID, Datos!ORDEN, Datos!VALOR_TEXT_LISTA, Datos!VALOR_NUM_LISTA, Datos!VALOR_FEC_LISTA, Datos!INTRO Distinct

            Dim dtValoresLista As New DataTable
            dtValoresLista = queryDatosLista.CopyAnonymusToDataTable
            dtValoresLista.TableName = "ValoresListaAtributos"
            Dim dtPk1(1) As DataColumn
            dtPk1(0) = dtValoresLista.Columns("ID")
            dtPk1(1) = dtValoresLista.Columns("ORDEN")
            dtValoresLista.PrimaryKey = dtPk1

            Dim dsAtributos As New DataSet("Atributos")
            dsAtributos.Tables.Add(dtAtrib)
            dsAtributos.Tables.Add(dtValoresLista)
            dr = Nothing
            Dim parentCols(0) As DataColumn
            Dim childCols(0) As DataColumn

            parentCols(0) = dsAtributos.Tables("Atributos").Columns("ID")
            childCols(0) = dsAtributos.Tables("ValoresListaAtributos").Columns("ID")

            dsAtributos.Relations.Add("RELACION_1", parentCols, childCols, False)
            Return dsAtributos
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Funcion que devuelve los atributos a nivel de item
    ''' </summary>
    ''' <param name="Anyo">Año del proceso</param>
    ''' <param name="Gmn1">Commodity del proceso</param>
    ''' <param name="CodigoProce">Cod del proceso</param>
    ''' <param name="CodProve">Codigo del proveedor adjudicado</param>
    ''' <param name="idItem">id del item</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Adjudicaciones_CargarAtributosItem(ByVal Anyo As Short, ByVal Gmn1 As String, ByVal CodigoProce As Integer, ByVal CodProve As String, ByVal idEmp As Short, ByVal idItem As Short, ByVal idLIA As Long, ByVal idGrupo As Short) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSGSW_DEVOLVER_ATRIBUTOSITEM"

            cm.CommandType = CommandType.StoredProcedure

            cm.Parameters.AddWithValue("@ANYO", Anyo)
            cm.Parameters.AddWithValue("@GMN1", Gmn1)
            cm.Parameters.AddWithValue("@PROCE", CodigoProce)
            cm.Parameters.AddWithValue("@PROVE", CodProve)
            cm.Parameters.AddWithValue("@ITEM", idItem)
            cm.Parameters.AddWithValue("@IDEMP", idEmp)
            cm.Parameters.AddWithValue("@LIA_ID", idLIA)
            cm.Parameters.AddWithValue("@IDGRUPO", idGrupo)

            da.SelectCommand = cm
            da.Fill(dr)

            Dim query = From Datos In dr.Tables(0).AsEnumerable() _
                        Select Datos!ID, Datos!COD, Datos!DEN, Datos!VALOR_TEXT, Datos!VALOR_NUM, Datos!VALOR_FEC, Datos!VALOR_BOOL, Datos!TIPO, Datos!INTRO, Datos!OBLIGATORIO, Datos!VALIDACION_ERP Distinct

            Dim dtAtrib As New DataTable
            dtAtrib = query.CopyAnonymusToDataTable
            dtAtrib.TableName = "Atributos"
            Dim dtPk0(0) As DataColumn
            dtPk0(0) = dtAtrib.Columns("ID")
            dtAtrib.PrimaryKey = dtPk0

            Dim queryDatosLista = From Datos In dr.Tables(0).AsEnumerable() _
                        Where Datos.Item("INTRO") = 1 _
                        Select Datos!ID, Datos!ORDEN, Datos!VALOR_TEXT_LISTA, Datos!VALOR_NUM_LISTA, Datos!VALOR_FEC_LISTA, Datos!INTRO Distinct

            Dim dtValoresLista As New DataTable
            dtValoresLista = queryDatosLista.CopyAnonymusToDataTable
            dtValoresLista.TableName = "ValoresListaAtributos"
            Dim dtPk1(1) As DataColumn
            dtPk1(0) = dtValoresLista.Columns("ID")
            dtPk1(1) = dtValoresLista.Columns("ORDEN")
            dtValoresLista.PrimaryKey = dtPk1

            Dim dsAtributos As New DataSet("Atributos")
            dsAtributos.Tables.Add(dtAtrib)
            dsAtributos.Tables.Add(dtValoresLista)
            dr = Nothing
            Dim parentCols(0) As DataColumn
            Dim childCols(0) As DataColumn

            parentCols(0) = dsAtributos.Tables("Atributos").Columns("ID")
            childCols(0) = dsAtributos.Tables("ValoresListaAtributos").Columns("ID")

            dsAtributos.Relations.Add("RELACION_1", parentCols, childCols, False)
            Return dsAtributos
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Funcion que devuelve los atributos a nivel de grupo
    ''' </summary>
    ''' <param name="Anyo">Año del proceso</param>
    ''' <param name="Gmn1">Commodity del proceso</param>
    ''' <param name="CodigoProce">Cod del proceso</param>
    ''' <param name="CodProve">Codigo del proveedor adjudicado</param>
    ''' <param name="idGrupo">id del grupo seleccionado</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Adjudicaciones_CargarAtributosGrupo(ByVal Anyo As Short, ByVal Gmn1 As String, ByVal CodigoProce As Integer, ByVal CodProve As String, ByVal idGrupo As Short, ByVal idEmp As Short) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSGSW_DEVOLVER_ATRIBUTOSGRUPO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@ANYO", Anyo)
            cm.Parameters.AddWithValue("@GMN1", Gmn1)
            cm.Parameters.AddWithValue("@PROCE", CodigoProce)
            cm.Parameters.AddWithValue("@PROVE", CodProve)
            cm.Parameters.AddWithValue("@IDGRUPO", idGrupo)
            cm.Parameters.AddWithValue("@IDEMP", idEmp)

            da.SelectCommand = cm
            da.Fill(dr)

            Dim query = From Datos In dr.Tables(0).AsEnumerable() _
                        Select Datos!ID, Datos!COD, Datos!DEN, Datos!VALOR_TEXT, Datos!VALOR_NUM, Datos!VALOR_FEC, Datos!VALOR_BOOL, Datos!TIPO, Datos!INTRO, Datos!OBLIGATORIO, Datos!VALIDACION_ERP Distinct

            Dim dtAtrib As New DataTable
            dtAtrib = query.CopyAnonymusToDataTable
            dtAtrib.TableName = "Atributos"
            Dim dtPk0(0) As DataColumn
            dtPk0(0) = dtAtrib.Columns("ID")
            dtAtrib.PrimaryKey = dtPk0

            Dim queryDatosLista = From Datos In dr.Tables(0).AsEnumerable() _
                        Where Datos.Item("INTRO") = 1 _
                        Select Datos!ID, Datos!ORDEN, Datos!VALOR_TEXT_LISTA, Datos!VALOR_NUM_LISTA, Datos!VALOR_FEC_LISTA, Datos!INTRO Distinct

            Dim dtValoresLista As New DataTable
            dtValoresLista = queryDatosLista.CopyAnonymusToDataTable
            dtValoresLista.TableName = "ValoresListaAtributos"
            Dim dtPk1(1) As DataColumn
            dtPk1(0) = dtValoresLista.Columns("ID")
            dtPk1(1) = dtValoresLista.Columns("ORDEN")
            dtValoresLista.PrimaryKey = dtPk1

            Dim dsAtributos As New DataSet("Atributos")
            dsAtributos.Tables.Add(dtAtrib)
            dsAtributos.Tables.Add(dtValoresLista)
            dr = Nothing
            Dim parentCols(0) As DataColumn
            Dim childCols(0) As DataColumn

            parentCols(0) = dsAtributos.Tables("Atributos").Columns("ID")
            childCols(0) = dsAtributos.Tables("ValoresListaAtributos").Columns("ID")

            dsAtributos.Relations.Add("RELACION_1", parentCols, childCols, False)
            Return dsAtributos
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Devuelve los items del grupo seleccionado
    ''' </summary>
    ''' <param name="codGrupo">codigo del grupo seleccionado</param>
    ''' <param name="Anyo">Año del proceso</param>
    ''' <param name="Gmn1">Commodity del proceso</param>
    ''' <param name="CodProce">Codigo del proceso</param>
    ''' <param name="CodProve">Codigo del proveedor seleccionado</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Adjudicaciones_ObtenerItemsGrupo(ByVal codGrupo As String, ByVal Anyo As Short, ByVal Gmn1 As String, ByVal CodProce As Long, ByVal CodProve As String, ByVal idEmp As Long, ByVal NumberFormat As System.Globalization.NumberFormatInfo, ByVal DateFormat As System.Globalization.DateTimeFormatInfo) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSGSW_DEVOLVER_ITEMSGRUPO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@ANYO", Anyo)
            cm.Parameters.AddWithValue("@GMN1", Gmn1)
            cm.Parameters.AddWithValue("@PROCE", CodProce)
            cm.Parameters.AddWithValue("@CODGRUPO", codGrupo)
            cm.Parameters.AddWithValue("@PROVE", CodProve)
            cm.Parameters.AddWithValue("@IDEMP", idEmp)
            cm.CommandTimeout = 300

            da.SelectCommand = cm
            da.Fill(dr)

            Dim dsItems As New DataSet("Items")
            If dr.Tables(0).Columns.IndexOf("INICIO") <> -1 Then
                'Si existe esta columna quiere decir que el grupo tiene precios escalados
                Dim query = From Datos In dr.Tables(0).AsEnumerable()
                            Select Datos!ID_ITEM, Datos!CODART, Datos!DENART, Datos!CENTRO, Datos!CENTROS, Datos!UNI, Datos!ESTADOINT, Datos!ESTADOINT_TEXT, Datos!ESTADOINT_ICO, Datos!FECINI_SUM, Datos!FECFIN_SUM, Datos!PROVE_ERP, Datos!DEN_ERP, Datos!MON, Datos!UON1,
                            Datos!UON2, Datos!UON3, Datos!DISTRIBUIDOR, Datos!ESCALADOS, Datos!LIA_ID, Datos!USU, Datos!FECENV, Datos!DESC_ERROR, Datos!FECENV_OK Distinct

                Dim dtItems As New DataTable
                Dim DateCols As New List(Of String)
                DateCols.Add("FECINI_SUM")
                DateCols.Add("FECFIN_SUM")
                DateCols.Add("FECENV")

                dtItems = query.CopyAnonymusToDataTable(DateCols)
                dtItems.TableName = "Items"

                Dim dtPk0(2) As DataColumn
                dtPk0(0) = dtItems.Columns("ID_ITEM")
                dtPk0(1) = dtItems.Columns("CENTROS")
                dtPk0(2) = dtItems.Columns("CODART")
                dtItems.PrimaryKey = dtPk0
                Dim queryAtribs = From Datos In dr.Tables(0).AsEnumerable()
                                  Where Datos.Item("ATRIBID") IsNot DBNull.Value
                                  Select Datos!ID_ITEM, Datos!ATRIBID, Datos!ATRIBCOD, Datos!ATRIBDEN, Datos!ATRIBVALOR_TEXT, Datos!ATRIBVALOR_NUM, Datos!ATRIBVALOR_FEC, Datos!ATRIBVALOR_BOOL, Datos!ATRIBTIPO, Datos!ATRIBINTRO, Datos!ATRIBOBLIGATORIO, Datos!ORDEN, Datos!CODART Distinct

                Dim dtAtribs As New DataTable
                dtAtribs = queryAtribs.CopyAnonymusToDataTable
                dtAtribs.TableName = "Atributos"
                Dim dtPk1(2) As DataColumn
                dtPk1(0) = dtAtribs.Columns("ID_ITEM")
                dtPk1(1) = dtAtribs.Columns("ATRIBID")
                dtPk1(2) = dtAtribs.Columns("CODART")
                dtAtribs.PrimaryKey = dtPk1

                Dim queryCentros = From Datos In dr.Tables(0).AsEnumerable() _
                            Select Datos!ID_ITEM, Datos!CENTROS, Datos!CODART Distinct

                Dim dtCentros As New DataTable
                dtCentros = queryCentros.CopyAnonymusToDataTable
                dtCentros.TableName = "Centros"
                Dim dtPk4(2) As DataColumn
                dtPk4(0) = dtCentros.Columns("ID_ITEM")
                dtPk4(1) = dtCentros.Columns("CENTROS")
                dtPk4(2) = dtCentros.Columns("CODART")
                dtCentros.PrimaryKey = dtPk4

                Dim queryEscalados = From Datos In dr.Tables(0).AsEnumerable() _
                            Select Datos!ID_ITEM, Datos!ESC_ID, Datos!INICIO, Datos!FIN, Datos!PREC_ADJ, Datos!ESCALAUNI, Datos!MON Distinct

                Dim dtEscalados As New DataTable
                Dim DoubleCols As New List(Of String)
                DoubleCols.Add("INICIO")
                DoubleCols.Add("FIN")
                DoubleCols.Add("PREC_ADJ")
                dtEscalados = queryEscalados.CopyAnonymusToDataTable(, DoubleCols)
                dtEscalados.TableName = "Escalados"
                Dim dtPk2(1) As DataColumn
                dtPk2(0) = dtEscalados.Columns("ID_ITEM")
                dtPk2(1) = dtEscalados.Columns("ESC_ID")
                dtEscalados.PrimaryKey = dtPk2

                dsItems.Tables.Add(dtItems)
                dsItems.Tables.Add(dtAtribs)
                dsItems.Tables.Add(dtCentros)
                dsItems.Tables.Add(dtEscalados)

                dr = Nothing

                'Se crea la relacion entre los distribuidores y sus correspondientes Codigos ERP
                Dim parentCols(0) As DataColumn
                Dim childCols(0) As DataColumn

                parentCols(0) = dsItems.Tables("Items").Columns("ID_ITEM")
                childCols(0) = dsItems.Tables("Atributos").Columns("ID_ITEM")

                dsItems.Relations.Add("RELACION_1", parentCols, childCols, False)

                parentCols(0) = dsItems.Tables("Items").Columns("ID_ITEM")
                childCols(0) = dsItems.Tables("Escalados").Columns("ID_ITEM")

                dsItems.Relations.Add("RELACION_2", parentCols, childCols, False)

                ReDim parentCols(1)
                ReDim childCols(1)

                parentCols(0) = dsItems.Tables("Items").Columns("ID_ITEM")
                parentCols(1) = dsItems.Tables("Items").Columns("CENTROS")
                childCols(0) = dsItems.Tables("Centros").Columns("ID_ITEM")
                childCols(1) = dsItems.Tables("Centros").Columns("CENTROS")

                dsItems.Relations.Add("RELACION_3", parentCols, childCols, False)
                'Creo las columnas de atributos para la tabla de items
                'Todas las col. de dtAtribs son de tipo string y hay que crear un nuevo datatable con la col. ORDEN de tipo entero para poder crear un dataview ordenado
                Dim dtAtribsCopy As DataTable = dtAtribs.Clone
                dtAtribsCopy.Columns("ORDEN").DataType = GetType(Integer)
                For Each oRow As DataRow In dtAtribs.Rows
                    dtAtribsCopy.ImportRow(oRow)
                Next
                Dim dvAtrib As New DataView(dtAtribsCopy, "", "ORDEN ASC", DataViewRowState.CurrentRows)
                For Each drItem As DataRow In dtItems.Rows
                    For Each drAtrib As DataRowView In dvAtrib
                        If drItem("ID_ITEM") = drAtrib("ID_ITEM") Then
                            If dtItems.Columns.IndexOf("ATRIB_" & drAtrib("ATRIBTIPO") & "_" & drAtrib("ATRIBOBLIGATORIO") & "_" & drAtrib("ATRIBINTRO") & "_" & drAtrib("ATRIBCOD")) = -1 Then
                                dtItems.Columns.Add("ATRIB_" & drAtrib("ATRIBTIPO") & "_" & drAtrib("ATRIBOBLIGATORIO") & "_" & drAtrib("ATRIBINTRO") & "_" & drAtrib("ATRIBCOD"))
                            End If
                        End If
                    Next
                Next

                'Se meten los datos de los atributos en la tabla de items
                Dim drRows() As DataRow
                For Each drItem As DataRow In dtItems.Rows
                    drRows = drItem.GetChildRows("RELACION_1")
                    For Each drAtrib As DataRow In drRows
                        Select Case drAtrib("ATRIBTIPO")
                            Case 1
                                drItem("ATRIB_" & drAtrib("ATRIBTIPO") & "_" & drAtrib("ATRIBOBLIGATORIO") & "_" & drAtrib("ATRIBINTRO") & "_" & drAtrib("ATRIBCOD")) = drAtrib("ATRIBVALOR_TEXT")
                            Case 2
                                If drAtrib("ATRIBVALOR_NUM") Is DBNull.Value Then
                                    drItem("ATRIB_" & drAtrib("ATRIBTIPO") & "_" & drAtrib("ATRIBOBLIGATORIO") & "_" & drAtrib("ATRIBINTRO") & "_" & drAtrib("ATRIBCOD")) = DBNull.Value
                                ElseIf drAtrib("ATRIBINTRO") = 1 Then
                                    drItem("ATRIB_" & drAtrib("ATRIBTIPO") & "_" & drAtrib("ATRIBOBLIGATORIO") & "_" & drAtrib("ATRIBINTRO") & "_" & drAtrib("ATRIBCOD")) = drAtrib("ATRIBVALOR_NUM")
                                Else
                                    drItem("ATRIB_" & drAtrib("ATRIBTIPO") & "_" & drAtrib("ATRIBOBLIGATORIO") & "_" & drAtrib("ATRIBINTRO") & "_" & drAtrib("ATRIBCOD")) = FSNLibrary.FormatNumber(DBNullToDbl(drAtrib("ATRIBVALOR_NUM")), NumberFormat)
                                End If
                            Case 3
                                If drAtrib("ATRIBVALOR_FEC") Is DBNull.Value Then
                                    drItem("ATRIB_" & drAtrib("ATRIBTIPO") & "_" & drAtrib("ATRIBOBLIGATORIO") & "_" & drAtrib("ATRIBINTRO") & "_" & drAtrib("ATRIBCOD")) = DBNull.Value
                                Else
                                    drItem("ATRIB_" & drAtrib("ATRIBTIPO") & "_" & drAtrib("ATRIBOBLIGATORIO") & "_" & drAtrib("ATRIBINTRO") & "_" & drAtrib("ATRIBCOD")) = FSNLibrary.FormatDate(drAtrib("ATRIBVALOR_FEC"), DateFormat)
                                End If
                            Case 4
                                drItem("ATRIB_" & drAtrib("ATRIBTIPO") & "_" & drAtrib("ATRIBOBLIGATORIO") & "_" & drAtrib("ATRIBINTRO") & "_" & drAtrib("ATRIBCOD")) = drAtrib("ATRIBVALOR_BOOL")
                        End Select

                    Next
                Next


                'Se meten los datos de los centros en la tabla de items
                For Each drItem As DataRow In dtItems.Rows
                    drRows = drItem.GetChildRows("RELACION_3")
                    If drRows.Count > 1 Then
                        drItem("CENTRO") = ""
                    ElseIf drRows.Count = 1 Then
                        drItem("CENTRO") = drRows(0)("CENTROS")
                    End If
                Next

                'Creo las columnas de escalados para la tabla de items
                If Not dtEscalados.Rows(0)("FIN") Is DBNull.Value Then
                    'Si el escalado es por rango
                    For Each drItem As DataRow In dtItems.Rows
                        For Each drEsc As DataRow In dtEscalados.Rows
                            If drItem("ID_ITEM") = drEsc("ID_ITEM") Then
                                If dtItems.Columns.IndexOf("ESC_" & drEsc("INICIO") & "_" & drEsc("FIN")) = -1 Then
                                    dtItems.Columns.Add("ESC_" & drEsc("INICIO") & "_" & drEsc("FIN"))
                                End If
                            End If
                        Next
                    Next

                    'Se meten los datos de los escalados en la tabla de items
                    Dim drRowsEsc() As DataRow
                    For Each drItem As DataRow In dtItems.Rows
                        drRowsEsc = drItem.GetChildRows("RELACION_2")
                        For Each drEsc As DataRow In drRowsEsc
                            drItem("ESC_" & drEsc("INICIO") & "_" & drEsc("FIN")) = drEsc("PREC_ADJ")
                        Next
                    Next
                Else
                    'Si el escalado es por cantidades directas
                    For Each drItem As DataRow In dtItems.Rows
                        For Each drEsc As DataRow In dtEscalados.Rows
                            If drItem("ID_ITEM") = drEsc("ID_ITEM") Then
                                If dtItems.Columns.IndexOf("ESC_" & drEsc("INICIO")) = -1 Then
                                    dtItems.Columns.Add("ESC_" & drEsc("INICIO"))
                                End If
                            End If
                        Next
                    Next

                    'Se meten los datos de los escalados en la tabla de items
                    Dim drRowsEsc() As DataRow
                    For Each drItem As DataRow In dtItems.Rows
                        drRowsEsc = drItem.GetChildRows("RELACION_2")
                        For Each drEsc As DataRow In drRowsEsc
                            drItem("ESC_" & drEsc("INICIO")) = drEsc("PREC_ADJ")
                        Next
                    Next
                End If
            Else
                'Si no existe la columna en la tabla no habra precios escalados en el grupo
                Dim query = From Datos In dr.Tables(0).AsEnumerable()
                            Select Datos!ID_ITEM, Datos!CODART, Datos!DENART, Datos!CENTRO, Datos!CENTROS, Datos!UNI, Datos!PREC_ADJ, Datos!CANT_ADJ, Datos!ESTADOINT, Datos!ESTADOINT_TEXT, Datos!ESTADOINT_ICO, Datos!FECINI_SUM, Datos!FECFIN_SUM, Datos!PROVE_ERP,
                            Datos!DEN_ERP, Datos!MON, Datos!UON1, Datos!UON2, Datos!UON3, Datos!DISTRIBUIDOR, Datos!ESCALADOS, Datos!LIA_ID, Datos!USU, Datos!FECENV, Datos!DESC_ERROR, Datos!FECENV_OK Distinct

                Dim dtItems As New DataTable
                Dim DateCols As New List(Of String)
                DateCols.Add("FECINI_SUM")
                DateCols.Add("FECFIN_SUM")
                DateCols.Add("FECENV")

                Dim DoubleCols As New List(Of String)
                DoubleCols.Add("PREC_ADJ")
                DoubleCols.Add("CANT_ADJ")
                dtItems = query.CopyAnonymusToDataTable(DateCols, DoubleCols)
                dtItems.TableName = "Items"

                Dim dtPk0(2) As DataColumn
                dtPk0(0) = dtItems.Columns("ID_ITEM")
                dtPk0(1) = dtItems.Columns("CENTROS")
                dtPk0(2) = dtItems.Columns("CODART")
                dtItems.PrimaryKey = dtPk0

                Dim queryAtribs = From Datos In dr.Tables(0).AsEnumerable()
                                  Where Datos.Item("ATRIBID") IsNot DBNull.Value
                                  Select Datos!ID_ITEM, Datos!ATRIBID, Datos!ATRIBCOD, Datos!ATRIBDEN, Datos!ATRIBVALOR_TEXT, Datos!ATRIBVALOR_NUM, Datos!ATRIBVALOR_FEC, Datos!ATRIBVALOR_BOOL, Datos!ATRIBTIPO, Datos!ATRIBINTRO, Datos!ATRIBOBLIGATORIO, Datos!ORDEN, Datos!CODART Distinct

                Dim dtAtribs As New DataTable
                dtAtribs = queryAtribs.CopyAnonymusToDataTable()
                dtAtribs.TableName = "Atributos"
                Dim dtPk1(2) As DataColumn
                dtPk1(0) = dtAtribs.Columns("ID_ITEM")
                dtPk1(1) = dtAtribs.Columns("ATRIBID")
                dtPk1(2) = dtAtribs.Columns("CODART")
                dtAtribs.PrimaryKey = dtPk1

                Dim queryCentros = From Datos In dr.Tables(0).AsEnumerable() _
                            Select Datos!ID_ITEM, Datos!CENTROS, Datos!CODART Distinct

                Dim dtCentros As New DataTable
                dtCentros = queryCentros.CopyAnonymusToDataTable
                dtCentros.TableName = "Centros"
                Dim dtPk4(2) As DataColumn
                dtPk4(0) = dtCentros.Columns("ID_ITEM")
                dtPk4(1) = dtCentros.Columns("CENTROS")
                dtPk4(2) = dtCentros.Columns("CODART")
                dtCentros.PrimaryKey = dtPk4

                dsItems.Tables.Add(dtItems)
                dsItems.Tables.Add(dtAtribs)
                dsItems.Tables.Add(dtCentros)

                dr = Nothing

                'Se crea la relacion entre los distribuidores y sus correspondientes Codigos ERP
                Dim parentCols(0) As DataColumn
                Dim childCols(0) As DataColumn

                parentCols(0) = dsItems.Tables("Items").Columns("ID_ITEM")
                childCols(0) = dsItems.Tables("Atributos").Columns("ID_ITEM")

                dsItems.Relations.Add("RELACION_1", parentCols, childCols, False)

                ReDim parentCols(1)
                ReDim childCols(1)

                parentCols(0) = dsItems.Tables("Items").Columns("ID_ITEM")
                parentCols(1) = dsItems.Tables("Items").Columns("CENTROS")
                childCols(0) = dsItems.Tables("Centros").Columns("ID_ITEM")
                childCols(1) = dsItems.Tables("Centros").Columns("CENTROS")

                dsItems.Relations.Add("RELACION_3", parentCols, childCols, False)
                'Creo las columnas de atributos para la tabla de items
                'Todas las col. de dtAtribs son de tipo string y hay que crear un nuevo datatable con la col. ORDEN de tipo entero para poder crear un dataview ordenado
                Dim dtAtribsCopy As DataTable = dtAtribs.Clone
                dtAtribsCopy.Columns("ORDEN").DataType = GetType(Integer)
                For Each oRow As DataRow In dtAtribs.Rows
                    dtAtribsCopy.ImportRow(oRow)
                Next
                Dim dvAtrib As New DataView(dtAtribsCopy, "", "ORDEN ASC", DataViewRowState.CurrentRows)
                For Each drItem As DataRow In dtItems.Rows
                    For Each drAtrib As DataRowView In dvAtrib
                        If drItem("ID_ITEM") = drAtrib("ID_ITEM") Then
                            If dtItems.Columns.IndexOf("ATRIB_" & drAtrib("ATRIBTIPO") & "_" & drAtrib("ATRIBOBLIGATORIO") & "_" & drAtrib("ATRIBINTRO") & "_" & drAtrib("ATRIBCOD")) = -1 Then
                                dtItems.Columns.Add("ATRIB_" & drAtrib("ATRIBTIPO") & "_" & drAtrib("ATRIBOBLIGATORIO") & "_" & drAtrib("ATRIBINTRO") & "_" & drAtrib("ATRIBCOD"))
                            End If
                        End If
                    Next
                Next

                'Se meten los datos de los atributos en la tabla de items
                Dim drRows() As DataRow
                For Each drItem As DataRow In dtItems.Rows
                    drRows = drItem.GetChildRows("RELACION_1")
                    For Each drAtrib As DataRow In drRows
                        Select Case drAtrib("ATRIBTIPO")
                            Case 1
                                drItem("ATRIB_" & drAtrib("ATRIBTIPO") & "_" & drAtrib("ATRIBOBLIGATORIO") & "_" & drAtrib("ATRIBINTRO") & "_" & drAtrib("ATRIBCOD")) = drAtrib("ATRIBVALOR_TEXT")
                            Case 2
                                If drAtrib("ATRIBVALOR_NUM") Is DBNull.Value Then
                                    drItem("ATRIB_" & drAtrib("ATRIBTIPO") & "_" & drAtrib("ATRIBOBLIGATORIO") & "_" & drAtrib("ATRIBINTRO") & "_" & drAtrib("ATRIBCOD")) = DBNull.Value
                                ElseIf drAtrib("ATRIBINTRO") = 1 Then
                                    drItem("ATRIB_" & drAtrib("ATRIBTIPO") & "_" & drAtrib("ATRIBOBLIGATORIO") & "_" & drAtrib("ATRIBINTRO") & "_" & drAtrib("ATRIBCOD")) = drAtrib("ATRIBVALOR_NUM")
                                Else
                                    drItem("ATRIB_" & drAtrib("ATRIBTIPO") & "_" & drAtrib("ATRIBOBLIGATORIO") & "_" & drAtrib("ATRIBINTRO") & "_" & drAtrib("ATRIBCOD")) = FSNLibrary.FormatNumber(DBNullToDbl(drAtrib("ATRIBVALOR_NUM")), NumberFormat)
                                End If
                            Case 3
                                If drAtrib("ATRIBVALOR_FEC") Is DBNull.Value Then
                                    drItem("ATRIB_" & drAtrib("ATRIBTIPO") & "_" & drAtrib("ATRIBOBLIGATORIO") & "_" & drAtrib("ATRIBINTRO") & "_" & drAtrib("ATRIBCOD")) = DBNull.Value
                                Else
                                    drItem("ATRIB_" & drAtrib("ATRIBTIPO") & "_" & drAtrib("ATRIBOBLIGATORIO") & "_" & drAtrib("ATRIBINTRO") & "_" & drAtrib("ATRIBCOD")) = FSNLibrary.FormatDate(drAtrib("ATRIBVALOR_FEC"), DateFormat)
                                End If
                            Case 4
                                drItem("ATRIB_" & drAtrib("ATRIBTIPO") & "_" & drAtrib("ATRIBOBLIGATORIO") & "_" & drAtrib("ATRIBINTRO") & "_" & drAtrib("ATRIBCOD")) = drAtrib("ATRIBVALOR_BOOL")
                        End Select
                    Next
                Next

                'Se meten los datos de los centros en la tabla de items

                For Each drItem As DataRow In dtItems.Rows
                    drRows = drItem.GetChildRows("RELACION_3")
                    If drRows.Count > 1 Then
                        drItem("CENTRO") = ""
                    ElseIf drRows.Count = 1 Then
                        drItem("CENTRO") = drRows(0)("CENTROS")
                    End If
                Next
            End If

            Return dsItems
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Devuelve el valor que se ha dado en la oferta a un atributo a nivel de proceso
    ''' </summary>
    ''' <param name="Anyo">AÃ±o del proceso</param>
    ''' <param name="Gmn1">Commodity del proceso</param>
    ''' <param name="CodProce">Codigo del proceso</param>
    ''' <param name="CodProve">Codigo del proveedor seleccionado</param>
    ''' <param name="IdAtrib">Id del atributo seleccionado</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Adjudicaciones_ObtenerValorAtribOferta(ByVal Anyo As Short, ByVal Gmn1 As String, ByVal CodProce As Long, ByVal CodProve As String, ByVal IdAtrib As Integer, ByVal IdGrupo As Integer, ByVal IdItem As Integer) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSGSW_DEVOLVER_VALORATRIBOFE"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@ANYO", Anyo)
            cm.Parameters.AddWithValue("@GMN1", Gmn1)
            cm.Parameters.AddWithValue("@PROCE", CodProce)
            cm.Parameters.AddWithValue("@PROVE", CodProve)
            cm.Parameters.AddWithValue("@IDATRIB", IdAtrib)
            cm.Parameters.AddWithValue("@IDGRUPO", IdGrupo)
            cm.Parameters.AddWithValue("@IDITEM", IdItem)

            cm.CommandTimeout = 300
            da.SelectCommand = cm
            da.Fill(dr)

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Traspasar Escalados Adjudicacion a ERP 
    ''' </summary>
    ''' <param name="dt">Escalados</param>
    ''' <returns>1- todo ok eoc- algo fallo</returns>
    '''<remarks>Llamada desde: fsnserver/TraspasarGrupoToERP; Tiempo máximo:0</remarks>
    Public Function Adjudicaciones_TraspasarEscaladosToERP(ByVal dt As DataTable) As Byte
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim da As New SqlDataAdapter
        Try
            cm.CommandText = "FSGSW_TRASPASAR_ESCALADOS_ERP"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Connection = cn

            cm.Parameters.Add("@ANYO", SqlDbType.Int, 4, "ANYO")
            cm.Parameters.Add("@GMN1", SqlDbType.NVarChar, -1, "GMN1")
            cm.Parameters.Add("@PROCE", SqlDbType.Int, 4, "PROCE")
            cm.Parameters.Add("@PROVE", SqlDbType.NVarChar, -1, "PROVE_PADRE")
            cm.Parameters.Add("@ID_ITEM", SqlDbType.Int, 4, "ID_ITEM")
            cm.Parameters.Add("@LIA_ID", SqlDbType.Int, 4, "LIA_ID")
            da.InsertCommand = cm
            da.Update(dt)

            Return 1
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Traspasar Atributos Adjudicaciones
    ''' </summary>
    ''' <param name="dt">Atributos</param>
    ''' <returns>1- todo ok eoc- algo fallo</returns>
    ''' <remarks>Llamada desde: fsnserver/TraspasarGrupoToERP; Tiempo máximo:0</remarks>
    Public Function Adjudicaciones_TraspasarAtributosToERP(ByVal dt As DataTable) As Byte
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim da As New SqlDataAdapter
        Try
            cm.CommandText = "FSGSW_TRASPASAR_ATRIBUTOS_ERP"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Connection = cn
            cm.Parameters.Add("@LIA_ID", SqlDbType.Int, 4, "LIA_ID")
            cm.Parameters.Add("@ID_ATRIB", SqlDbType.Int, 4, "ID_ATRIB")
            cm.Parameters.Add("@AMBITO", SqlDbType.Int, 4, "AMBITO")
            cm.Parameters.Add("@VALOR_NUM", SqlDbType.Float, 8, "VALOR_NUM")
            cm.Parameters.Add("@VALOR_TEXT", SqlDbType.NVarChar, -1, "VALOR_TEXT")
            cm.Parameters.Add("@VALOR_FEC", SqlDbType.DateTime, -1, "VALOR_FEC")
            cm.Parameters.Add("@VALOR_BOOL", SqlDbType.Int, 4, "VALOR_BOOL")
            da.InsertCommand = cm
            da.Update(dt)

            Return 1
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Traspasar Adjudicaciones
    ''' </summary>
    ''' <param name="dt">Adjudicaciones</param>
    ''' <param name="codUsu">usuario</param>
    ''' <returns>datatable- todo ok eoc- algo fallo</returns>
    ''' <remarks>Llamada desde: fsnserver/TraspasarGrupoToERP; Tiempo máximo:0</remarks>
    Public Function Adjudicaciones_TraspasarToERP(ByVal dt As DataTable, ByVal codUsu As String) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim da As New SqlDataAdapter
        Try
            cm.CommandText = "FSGSW_TRASPASAR_ERP"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Connection = cn
            cm.Parameters.Add("@ANYO", SqlDbType.Int, 4, "ANYO")
            cm.Parameters.Add("@GMN1", SqlDbType.NVarChar, -1, "GMN1")
            cm.Parameters.Add("@PROCE", SqlDbType.Int, 4, "PROCE")
            cm.Parameters.Add("@ID_ITEM", SqlDbType.Int, 4, "ID_ITEM")
            cm.Parameters.Add("@IDEMP", SqlDbType.Int, 4, "ID_EMP")
            cm.Parameters.Add("@PROVE_PADRE", SqlDbType.NVarChar, -1, "PROVE_PADRE")
            cm.Parameters.Add("@PROVE", SqlDbType.NVarChar, -1, "PROVE")
            cm.Parameters.Add("@CANT_ADJ", SqlDbType.Float, 8, "CANT_ADJ")
            cm.Parameters.Add("@CENTRO", SqlDbType.NVarChar, -1, "CENTRO")
            cm.Parameters.Add("@PROVE_ERP", SqlDbType.NVarChar, -1, "PROVE_ERP")
            cm.Parameters.AddWithValue("@CODUSU", codUsu)
            cm.Parameters.Add("@LIA_ID", SqlDbType.Int, 4, "LIA_ID")
            cm.Parameters.Add("@CODART", SqlDbType.VarChar, 50, "CODART")
            da.InsertCommand = cm
            AddHandler da.RowUpdated, AddressOf Actualizar_LIA_Id
            da.Update(dt)
            RemoveHandler da.RowUpdated, AddressOf Actualizar_LIA_Id
            Return dt
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Cargar el Mensaje Error Integracion
    ''' </summary>
    ''' <param name="Id_LIA">LOG_GRAL.ID_TABLA</param>
    ''' <returns>Mensaje</returns>
    ''' <remarks>Llamada desde: fsnserver/root.vb/Devolver_MensajeErrorIntegracion; Tiempo máximo:0</remarks>
    Public Function Adjudicaciones_Devolver_MensajeErrorIntegracion(ByVal Id_LIA As Long) As String
        Authenticate()

        Dim sError As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim sConsulta As String = String.Empty
        Try
            cn.Open()
            sConsulta = "SELECT TEXTO FROM LOG_GRAL LG WITH (NOLOCK) WHERE TABLA=9 AND ID_TABLA=@LIA_ID"
            cm.CommandText = sConsulta
            cm.CommandType = CommandType.Text
            cm.CommandTimeout = 300
            cm.Connection = cn
            cm.Parameters.AddWithValue("@LIA_ID", Id_LIA)
            sError = DBNullToStr(cm.ExecuteScalar())
            Return sError
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    Private Sub Actualizar_LIA_Id(ByVal sender As Object, ByVal e As SqlRowUpdatedEventArgs)
        If e.Status = UpdateStatus.Continue AndAlso e.StatementType = StatementType.Insert Then
            ' Get the Identity column value
            Dim cmd As New SqlCommand
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "SELECT IDENT_CURRENT('LOG_ITEM_ADJ')"
            cmd.Connection = e.Command.Connection
            e.Row("LIA_ID") = Int32.Parse(cmd.ExecuteScalar().ToString())
            e.Row.AcceptChanges()
            cmd = Nothing
        End If
    End Sub
    ''' <summary>
    ''' Obtener Nombre Mapper
    ''' </summary>
    ''' <param name="sOrgCompras">Org Compras</param>
    ''' <param name="idEmpresa">Empresa</param>
    ''' <returns>Nombre Mapper</returns>
    ''' <remarks>Llamada desde: fsnserver/root.vb/ObtenerNombreMapper; Tiempo máximo:0</remarks>
    Public Function Adjudicaciones_ObtenerNombreMapper(ByVal sOrgCompras As String, ByVal idEmpresa As Short) As String
        Authenticate()

        Dim sMapper As String = "FSGSMapper"
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim sConsulta As String = String.Empty
        Try
            cn.Open()

            If sOrgCompras <> "" Then
                sConsulta = "SELECT MAPPER FROM ERP E WITH (NOLOCK) INNER JOIN ERP_ORGCOMPRAS EO WITH (NOLOCK) ON EO.ERP=E.COD "
                sConsulta = sConsulta & " WHERE EO.ORGCOMPRAS = @ORGCOMPRAS"
            Else
                sConsulta = "SELECT MAPPER FROM ERP E WITH (NOLOCK) INNER JOIN ERP_SOCIEDAD EO WITH (NOLOCK) ON EO.ERP=E.COD "
                sConsulta = sConsulta & " INNER JOIN EMP WITH(NOLOCK) ON EMP.SOCIEDAD=EO.SOCIEDAD"
                sConsulta = sConsulta & " WHERE EMP.ERP=1 AND EMP.ID =@IDEMP "
            End If

            cm.CommandText = sConsulta
            cm.CommandType = CommandType.Text
            cm.CommandTimeout = 300
            cm.Connection = cn
            cm.Parameters.AddWithValue("@IDEMP", idEmpresa)
            cm.Parameters.AddWithValue("@ORGCOMPRAS", sOrgCompras)
            sMapper = cm.ExecuteScalar()

            Return sMapper
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Borrar Adjudicaciones
    ''' </summary>
    ''' <param name="dt">Adjudicaciones</param>
    ''' <param name="codUsu">usuario</param>
    ''' <returns>1- todo ok eoc- algo fallo</returns>
    ''' <remarks>Llamada desde: fsnserver/BorrarGrupoDeERP; Tiempo máximo:0</remarks>
    Public Function Adjudicaciones_BorrarDeERP(ByRef dt As DataTable, ByVal codUsu As String) As Byte
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim da As New SqlDataAdapter
        Try
            cm.CommandText = "FSGSW_BORRAR_ADJ_ERP"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Connection = cn
            cm.Parameters.Add("@ANYO", SqlDbType.Int, 4, "ANYO")
            cm.Parameters.Add("@GMN1", SqlDbType.NVarChar, -1, "GMN1")
            cm.Parameters.Add("@PROCE", SqlDbType.NVarChar, -1, "PROCE")
            cm.Parameters.Add("@ID_ITEM", SqlDbType.Int, 4, "ID_ITEM")
            cm.Parameters.Add("@IDEMP", SqlDbType.Int, 4, "ID_EMP")
            cm.Parameters.Add("@PROVE_PADRE", SqlDbType.NVarChar, -1, "PROVE_PADRE")
            cm.Parameters.Add("@PROVE", SqlDbType.NVarChar, -1, "PROVE")
            cm.Parameters.Add("@CANT_ADJ", SqlDbType.Float, 8, "CANT_ADJ")
            cm.Parameters.Add("@CENTRO", SqlDbType.NVarChar, -1, "CENTRO")
            cm.Parameters.Add("@PROVE_ERP", SqlDbType.NVarChar, -1, "PROVE_ERP")
            cm.Parameters.AddWithValue("@CODUSU", codUsu)
            cm.Parameters.Add("@LIA_ID", SqlDbType.Int, 4, "LIA_ID")
            cm.Parameters.Add("@CODART", SqlDbType.VarChar, 50, "CODART")
            da.InsertCommand = cm
            AddHandler da.RowUpdated, AddressOf Actualizar_LIA_Id
            da.Update(dt)
            RemoveHandler da.RowUpdated, AddressOf Actualizar_LIA_Id

            Adjudicaciones_ActualizarProceInt(dt.Rows(0)("ANYO"), dt.Rows(0)("GMN1"), dt.Rows(0)("PROCE"))
            Return 1
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Funcion que devuelve los distribuidores del proveedor
    ''' </summary>
    ''' <param name="CodProve"></param>
    ''' <param name="idEmp"></param>
    ''' <param name="Anyo"></param>
    ''' <param name="sGmn1"></param>
    ''' <param name="codProce"></param>
    ''' <param name="idItem"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Adjudicaciones_ObtenerDistribuidoresProveedorItem(ByVal CodProve As String, ByVal idEmp As Long, ByVal Anyo As Long, ByVal sGmn1 As String, ByVal codProce As Long, ByVal idItem As Long, ByVal sUon1 As String, ByVal sUon2 As String, ByVal sUon3 As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSGSW_DEVOLVER_DISTRIBUIDORESPROVEEDOR_ITEM"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@PROVE", CodProve)
            cm.Parameters.AddWithValue("@IDEMP", idEmp)
            cm.Parameters.AddWithValue("@ANYO", Anyo)
            cm.Parameters.AddWithValue("@GMN1", sGmn1)
            cm.Parameters.AddWithValue("@PROCE", codProce)
            cm.Parameters.AddWithValue("@IDITEM", idItem)
            If sUon1 <> String.Empty Then
                cm.Parameters.AddWithValue("@UON1", sUon1)
            End If
            If sUon2 <> String.Empty Then
                cm.Parameters.AddWithValue("@UON2", sUon2)
            End If
            If sUon3 <> String.Empty Then
                cm.Parameters.AddWithValue("@UON3", sUon3)
            End If

            da.SelectCommand = cm
            da.Fill(dr)

            Dim query = From Datos In dr.Tables(0).AsEnumerable() _
                        Select Datos!COD, Datos!DEN, Datos!PORCEN, Datos!IA_CANT_ADJ, Datos!LIA_CANT_ADJ Distinct

            Dim dtDistrib As New DataTable
            dtDistrib = query.CopyAnonymusToDataTable
            dtDistrib.TableName = "Distribuidores"
            Dim dtPk0(0) As DataColumn
            dtPk0(0) = dtDistrib.Columns("COD")
            dtDistrib.PrimaryKey = dtPk0

            Dim queryCodsERP = From Datos In dr.Tables(0).AsEnumerable() _
                        Where Datos.Item("COD_ERP") <> "NULL" _
                        Select Datos!COD, Datos!COD_ERP, Datos!DEN_ERP, Datos!PROVE_ERP_SEL Distinct

            Dim dtCodigosERP As New DataTable
            dtCodigosERP = queryCodsERP.CopyAnonymusToDataTable
            dtCodigosERP.TableName = "CodigosERP"
            Dim dtPk1(1) As DataColumn
            dtPk1(0) = dtCodigosERP.Columns("COD")
            dtPk1(1) = dtCodigosERP.Columns("COD_ERP")
            dtCodigosERP.PrimaryKey = dtPk1

            Dim dsDistribuidores As New DataSet("Distribuidores")
            dsDistribuidores.Tables.Add(dtDistrib)
            dsDistribuidores.Tables.Add(dtCodigosERP)
            dr = Nothing

            'Se crea la relacion entre los distribuidores y sus correspondientes Codigos ERP
            Dim parentCols(0) As DataColumn
            Dim childCols(0) As DataColumn

            parentCols(0) = dsDistribuidores.Tables("Distribuidores").Columns("COD")
            childCols(0) = dsDistribuidores.Tables("CodigosERP").Columns("COD")

            dsDistribuidores.Relations.Add("RELACION_1", parentCols, childCols, False)
            Return dsDistribuidores
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Funcion que devuelve los distribuidores del proveedor
    ''' </summary>
    ''' <param name="CodProve">Codigo del proveedor</param>
    ''' <param name="idEmp">id de la empresa</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Adjudicaciones_ObtenerDistribuidoresProveedor(ByVal CodProve As String, ByVal idEmp As Long) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSGSW_DEVOLVER_DISTRIBUIDORESPROVEEDOR"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@PROVE", CodProve)
            cm.Parameters.AddWithValue("@IDEMP", idEmp)

            da.SelectCommand = cm
            da.Fill(dr)

            Dim query = From Datos In dr.Tables(0).AsEnumerable() _
                        Select Datos!COD, Datos!DEN Distinct

            Dim dtDistrib As New DataTable
            dtDistrib = query.CopyAnonymusToDataTable
            dtDistrib.TableName = "Distribuidores"
            Dim dtPk0(0) As DataColumn
            dtPk0(0) = dtDistrib.Columns("COD")
            dtDistrib.PrimaryKey = dtPk0

            Dim queryCodsERP = From Datos In dr.Tables(0).AsEnumerable() _
                        Where Datos.Item("COD_ERP") <> "NULL" _
                        Select Datos!COD, Datos!COD_ERP, Datos!DEN_ERP Distinct

            Dim dtCodigosERP As New DataTable
            dtCodigosERP = queryCodsERP.CopyAnonymusToDataTable
            dtCodigosERP.TableName = "CodigosERP"
            Dim dtPk1(1) As DataColumn
            dtPk1(0) = dtCodigosERP.Columns("COD")
            dtPk1(1) = dtCodigosERP.Columns("COD_ERP")
            dtCodigosERP.PrimaryKey = dtPk1

            Dim dsDistribuidores As New DataSet("Distribuidores")
            dsDistribuidores.Tables.Add(dtDistrib)
            dsDistribuidores.Tables.Add(dtCodigosERP)
            dr = Nothing

            'Se crea la relacion entre los distribuidores y sus correspondientes Codigos ERP
            Dim parentCols(0) As DataColumn
            Dim childCols(0) As DataColumn

            parentCols(0) = dsDistribuidores.Tables("Distribuidores").Columns("COD")
            childCols(0) = dsDistribuidores.Tables("CodigosERP").Columns("COD")

            dsDistribuidores.Relations.Add("RELACION_1", parentCols, childCols, False)
            Return dsDistribuidores
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Funcion que devuelve un dataset con los datos del proveedor adjudicado
    ''' </summary>
    ''' <param name="idEmp">Id de la empresa</param>
    ''' <param name="CodProve">Codigo del proveedor</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Adjudicaciones_ObtenerDatosProveedorAdjudicado(ByVal Anyo As Long, ByVal sGmn1 As String, ByVal codProce As Long, ByVal idEmp As Long, ByVal CodProve As String, ByVal sIdi As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSGSW_DEVOLVER_PROVEEDORADJUDICADO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@ANYO", Anyo)
            cm.Parameters.AddWithValue("@GMN1", sGmn1)
            cm.Parameters.AddWithValue("@PROCE", codProce)
            cm.Parameters.AddWithValue("@CODPROVE", CodProve)
            cm.Parameters.AddWithValue("@IDEMP", idEmp)
            cm.Parameters.AddWithValue("@IDI", sIdi)

            da.SelectCommand = cm
            da.Fill(dr)

            dr.Tables(0).TableName = "DatosProveedor"
            dr.Tables(1).TableName = "DenominacionCamposPer"

            For ind = 1 To 4
                If dr.Tables(0)(0)("ACTCAMPO" & ind) = 1 Then 'Se va comprobando si los campos personalizables estan activados
                    dr.Tables(0).Columns("CAMPO" & ind).ColumnName = dr.Tables(1)(ind - 1)("DEN") 'Se le cambia el nombre a la columna para que sea la del campo personalizable
                    dr.Tables(0).Columns.Remove("ACTCAMPO" & ind)
                Else
                    dr.Tables(0).Columns.Remove("ACTCAMPO" & ind)
                    dr.Tables(0).Columns.Remove("CAMPO" & ind)
                End If
            Next

            'Borramos la tabla de denominaciones de los campos personalizables
            dr.Tables.Remove("DenominacionCamposPer")

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Funcion que devuelve un dataset con los datos del proveedor adjudicado
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Adjudicaciones_ObtenerCentrosItem(ByVal Uon1 As String, ByVal Uon2 As String, ByVal Uon3 As String, ByVal codArt As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSGSW_DEVOLVER_CENTROSITEM"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@UON1", Uon1)
            cm.Parameters.AddWithValue("@UON2", Uon2)
            cm.Parameters.AddWithValue("@UON3", Uon3)
            cm.Parameters.AddWithValue("@ART4", codArt)

            da.SelectCommand = cm
            da.Fill(dr)

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Actualizar el estado de integracion de los procesos (tabla PROCE_INT)
    ''' </summary>
    ''' <param name="Anyo">año del proceso</param>
    ''' <param name="Gmn1">GMN1 del proceso</param>
    ''' <param name="codProce">Código del proceso</param>
    ''' <remarks>Llamada desde: fsnserver/TraspasarGrupoToERP; Tiempo máximo:0</remarks>
    Public Sub Adjudicaciones_ActualizarProceInt(ByVal Anyo As Short, ByVal Gmn1 As String, ByVal codProce As Integer)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSINT_ACTUALIZAR_PROCE_INT"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Connection = cn
            cm.Parameters.AddWithValue("@ANYO", Anyo)
            cm.Parameters.AddWithValue("@GMN1", Gmn1)
            cm.Parameters.AddWithValue("@PROCE", codProce)
            cm.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Sub

#End Region
#Region "Especificaciones"

    ''' <summary>
    ''' Lee especificaciones de GS segun tipo:
    '''   TIPO = 0 --> PROCE_ESP
    '''   TIPO = 1 --> PROCE_GRUPO_ESP
    '''   TIPO = 2 --> ITEM_ESP
    '''   TIPO = 3 --> ART4_ESP
    '''   TIPO = 4 --> PROVE_ESP
    '''   TIPO = 7 --> PLANTILLA_PROCE_ESP
    '''   TIPO = 8 --> PROCE_ADJUN
    '''   TIPO = 9 --> REGISTRO_EMAIL_ADJUN
    ''' </summary>
    ''' <param name="AdjunId">Id esprecificacion o archivo</param>
    ''' <param name="iTipo">Tipo</param>
    ''' <param name="Grupo">Grupo proceso</param>
    ''' <param name="Proce">CÃ³digo del proceso</param>
    ''' <param name="Anyo">aÃ±o del proceso</param>
    ''' <param name="Gmn1">GMN1 del proceso</param>
    ''' <param name="ChunkNumber">Num trozos archivo</param>
    ''' <param name="ChunkSize">TamaÃ±o trozo</param>
    ''' <returns>-</returns>
    ''' <remarks>Llamada desde: fsnserver/Especificacion; Tiempo mÃ¡ximo:0</remarks>
    Public Function Especificacion_LeerEspecificacion(ByVal AdjunId As Long, Optional ByVal iTipo As Integer = 0, Optional ByVal Grupo As Long = 0, Optional ByVal Proce As Long = 0, Optional ByVal Gmn1 As String = "", Optional ByVal Anyo As Long = 0, Optional ByVal ChunkNumber As Long = 0, Optional ByVal ChunkSize As Long = 0) As Byte()
        Authenticate()

        Dim cn As New SqlConnection
        Dim cm As SqlCommand = New SqlCommand
        Dim mi_impersionationContext As Security.Principal.WindowsImpersonationContext

        cn = NuevaConexionFileStream()
        mi_impersionationContext = RealizarSuplantacion()
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSGS_LEER_ESPECIFICACION"
            Select Case iTipo
                Case 0
                    cm.Parameters.AddWithValue("@ID", AdjunId)
                    cm.Parameters.AddWithValue("@TIPO", iTipo)
                    cm.Parameters.AddWithValue("@PROCE", Proce)
                    cm.Parameters.AddWithValue("@GMN1", Gmn1)
                    cm.Parameters.AddWithValue("@ANYO", Anyo)
                Case 1
                    cm.Parameters.AddWithValue("@ID", AdjunId)
                    cm.Parameters.AddWithValue("@TIPO", iTipo)
                    cm.Parameters.AddWithValue("@GRUPO", Grupo)
                    cm.Parameters.AddWithValue("@PROCE", Proce)
                    cm.Parameters.AddWithValue("@GMN1", Gmn1)
                    cm.Parameters.AddWithValue("@ANYO", Anyo)
                Case 2
                    cm.Parameters.AddWithValue("@ID", AdjunId)
                    cm.Parameters.AddWithValue("@TIPO", iTipo)
                    cm.Parameters.AddWithValue("@ITEM", Grupo)
                    cm.Parameters.AddWithValue("@PROCE", Proce)
                    cm.Parameters.AddWithValue("@GMN1", Gmn1)
                    cm.Parameters.AddWithValue("@ANYO", Anyo)
                Case 3
                    cm.Parameters.AddWithValue("@ID", AdjunId)
                    cm.Parameters.AddWithValue("@TIPO", iTipo)
                    cm.Parameters.AddWithValue("@ART4", Gmn1)
                Case 4
                    cm.Parameters.AddWithValue("@ID", AdjunId)
                    cm.Parameters.AddWithValue("@TIPO", iTipo)
                    cm.Parameters.AddWithValue("@PROVE", Gmn1)
                Case 7
                    cm.Parameters.AddWithValue("@ID", AdjunId)
                    cm.Parameters.AddWithValue("@TIPO", iTipo)
                    cm.Parameters.AddWithValue("@PLANT", Grupo)
                Case 8
                    cm.Parameters.AddWithValue("@ID", AdjunId)
                    cm.Parameters.AddWithValue("@TIPO", iTipo)
                    cm.Parameters.AddWithValue("@PROCE", Proce)
                    cm.Parameters.AddWithValue("@GMN1", Gmn1)
                    cm.Parameters.AddWithValue("@ANYO", Anyo)
            End Select
            cm.CommandType = CommandType.StoredProcedure

            'Retrieve the file path of the SQL FILESTREAM BLOB in the first row. 

            Dim filePath As String = Nothing

            Dim pathObj As Object = cm.ExecuteScalar()

            If Not pathObj.Equals(DBNull.Value) Then
                filePath = DirectCast(pathObj, String)
            Else
                Throw New System.Exception(" failed to read the path name en Adjunto_ReadData.")
            End If

            ' Obtain a transaction context. All FILESTREAM BLOB operations occur within a transaction context to maintain data consistency. 
            Dim transaction As SqlTransaction = cn.BeginTransaction("mainTranaction")
            cm.Transaction = transaction
            cm.CommandType = CommandType.Text
            cm.CommandText = "SELECT GET_FILESTREAM_TRANSACTION_CONTEXT()"
            Dim txContext As Byte() = Nothing
            Dim obj As Object = cm.ExecuteScalar()

            If Not obj.Equals(DBNull.Value) Then
                txContext = DirectCast(obj, Byte())
            Else
                Throw New System.Exception("GET_FILESTREAM_TRANSACTION_CONTEXT() failed")
            End If

            Dim sqlFileStream As New SqlTypes.SqlFileStream(filePath, txContext, FileAccess.Read)
            Dim buffer() As Byte
            If sqlFileStream.Length > ChunkSize Then
                ReDim buffer(ChunkSize - 1)
                sqlFileStream.Seek((ChunkNumber - 1) * ChunkSize, 0)

                Dim iBytesRead As Integer = sqlFileStream.Read(buffer, 0, buffer.Length)
                ReDim Preserve buffer(iBytesRead - 1)
            Else
                ReDim buffer(sqlFileStream.Length - 1)
                sqlFileStream.Read(buffer, 0, buffer.Length)
            End If

            ' Close the FILESTREAM handle. 
            sqlFileStream.Close()

            cm.Transaction.Commit()

            DeshacerSuplantacion(mi_impersionationContext)

            Especificacion_LeerEspecificacion = buffer
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Graba el fichero adjunto
    ''' </summary>
    ''' <param name="sNom">Nombre del fichero</param>
    ''' <param name="sPath">Path donde esta el fichero adjunto</param>
    ''' <returns>retorna el id del fichero grabado y su tamanyo</returns>
    ''' <remarks></remarks>
    Public Function Especificacion_GrabarEspecificacion(ByVal sNom As String, ByVal sPath As String, ByVal DBName As String, ByVal DBServer As String, Optional ByVal lId As Long = Nothing,
                                                        Optional ByVal sComent As String = "", Optional ByVal lIdRel As Long = Nothing, Optional ByVal DataSize As Long = Nothing,
                                                        Optional ByVal Idioma As String = Nothing, Optional ByVal Tipo As Long = Nothing, Optional ByVal FecAct As DateTime = Nothing,
                                                        Optional ByVal Proce As Long = 0, Optional ByVal Gmn1 As String = "", Optional ByVal Anyo As Long = 0) As Long()
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim arrReturn(1) As Long
        Try
            cn.Open()
            cm.Connection = cn

            cm.Parameters.AddWithValue("@NOM", sNom)
            cm.Parameters.AddWithValue("@TIPO", Tipo)
            cm.Parameters.AddWithValue("@ID", lId)

            cm.CommandText = "FSGS_GRABARESPECIFICACION"
            cm.Parameters.AddWithValue("@COMENT", sComent)

            Select Case Tipo
                Case 0
                    cm.Parameters.AddWithValue("@PROCE", Proce)
                    cm.Parameters.AddWithValue("@GMN1", Gmn1)
                    cm.Parameters.AddWithValue("@ANYO", Anyo)
                Case 1
                    cm.Parameters.AddWithValue("@GRUPO", lIdRel)
                    cm.Parameters.AddWithValue("@PROCE", Proce)
                    cm.Parameters.AddWithValue("@GMN1", Gmn1)
                    cm.Parameters.AddWithValue("@ANYO", Anyo)
                Case 2
                    cm.Parameters.AddWithValue("@ITEM", lIdRel)
                    cm.Parameters.AddWithValue("@PROCE", Proce)
                    cm.Parameters.AddWithValue("@GMN1", Gmn1)
                    cm.Parameters.AddWithValue("@ANYO", Anyo)
                Case 3
                    cm.Parameters.AddWithValue("@ART4", Gmn1)
                    cm.Parameters.AddWithValue("@DATASIZE", DataSize)
                Case 4
                    cm.Parameters.AddWithValue("@PROVE", Gmn1)
                    cm.Parameters.AddWithValue("@DATASIZE", DataSize)
                Case 7
                    cm.Parameters.AddWithValue("@PLANT", lIdRel)
                    cm.Parameters.AddWithValue("@DATASIZE", DataSize)
                Case 8
                    cm.Parameters.AddWithValue("@DATASIZE", DataSize)
                    cm.Parameters.AddWithValue("@PROCE", Proce)
                    cm.Parameters.AddWithValue("@GMN1", Gmn1)
                    cm.Parameters.AddWithValue("@ANYO", Anyo)
            End Select

            cm.CommandType = CommandType.StoredProcedure
            cm.ExecuteNonQuery()

            Dim sCarpeta As String = sPath
            Dim sNombreFichero As String = sCarpeta & "\" & sNom

            arrReturn(0) = lId
            If Not (sCarpeta = "" And sNom = "") Then
                arrReturn(1) = GrabarNuevoAdjuntoEspecificacion(lId, sNombreFichero, Tipo, lIdRel, Proce, Gmn1, Anyo)
            End If
            Return arrReturn
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    Public Function Especificacion_GrabarEspecificacionCia(ByVal Id As Long, ByVal Cia As Long, ByVal sNom As String, ByVal sComent As String, ByVal sPath As String) As Long()
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim arrReturn(1) As Long
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSP_GRABAR_ESPECIFICACION_CIA"
            cm.Parameters.AddWithValue("@ID", Id)
            cm.Parameters.AddWithValue("@CIA", Cia)
            cm.Parameters.AddWithValue("@NOM", sNom)
            cm.Parameters.AddWithValue("@COMENT", sComent)

            cm.CommandType = CommandType.StoredProcedure
            cm.ExecuteNonQuery()

            Dim sCarpeta As String = sPath
            Dim sNombreFichero As String = sCarpeta & "\" & sNom
            arrReturn(0) = Id
            If Not (sCarpeta = "" And sNom = "") Then
                arrReturn(1) = GrabarNuevoAdjuntoEspecificacionCia(Id, sNombreFichero, Cia)
            End If
            Return arrReturn
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    Public Function Especificacion_LeerEspecificacionCia(ByVal AdjunId As Long, ByVal Cia As Long, ByVal ChunkNumber As Long, ByVal ChunkSize As Long) As Byte()
        Authenticate()

        Dim cn As New SqlConnection
        Dim mi_impersionationContext As Security.Principal.WindowsImpersonationContext

        cn = NuevaConexionFileStream()
        mi_impersionationContext = RealizarSuplantacion()

        Dim cm As SqlCommand = New SqlCommand
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSP_LEER_ESPECIFICACION_CIA"
            cm.Parameters.AddWithValue("@ID", AdjunId)
            cm.Parameters.AddWithValue("@CIA", Cia)
            cm.CommandType = CommandType.StoredProcedure

            'Retrieve the file path of the SQL FILESTREAM BLOB in the first row. 
            Dim filePath As String = Nothing
            Dim pathObj As Object = cm.ExecuteScalar()

            If Not pathObj.Equals(DBNull.Value) Then
                filePath = DirectCast(pathObj, String)
            Else
                Throw New System.Exception(" failed to read the path name en Adjunto_ReadData.")
            End If

            ' Obtain a transaction context. All FILESTREAM BLOB operations occur within a transaction context to maintain data consistency. 
            Dim transaction As SqlTransaction = cn.BeginTransaction("mainTranaction")
            cm.Transaction = transaction
            cm.CommandType = CommandType.Text
            cm.CommandText = "SELECT GET_FILESTREAM_TRANSACTION_CONTEXT()"
            Dim txContext As Byte() = Nothing
            Dim obj As Object = cm.ExecuteScalar()

            If Not obj.Equals(DBNull.Value) Then
                txContext = DirectCast(obj, Byte())
            Else
                Throw New System.Exception("GET_FILESTREAM_TRANSACTION_CONTEXT() failed")
            End If

            Dim sqlFileStream As New SqlTypes.SqlFileStream(filePath, txContext, FileAccess.Read)
            Dim buffer() As Byte
            If sqlFileStream.Length > ChunkSize Then
                ReDim buffer(ChunkSize - 1)
                sqlFileStream.Seek((ChunkNumber - 1) * ChunkSize, 0)

                Dim iBytesRead As Integer = sqlFileStream.Read(buffer, 0, buffer.Length)
                ReDim Preserve buffer(iBytesRead - 1)
            Else
                ReDim buffer(sqlFileStream.Length - 1)
                sqlFileStream.Read(buffer, 0, buffer.Length)
            End If

            ' Close the FILESTREAM handle. 
            sqlFileStream.Close()

            cm.Transaction.Commit()

            DeshacerSuplantacion(mi_impersionationContext)

            Especificacion_LeerEspecificacionCia = buffer
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
#End Region
#Region "Ofertas"
    ''' <summary>
    ''' Devuelve todos los adjuntos en los tres ambitos de la oferta
    ''' </summary>
    ''' <returns>Dataset con los adjuntos de la oferta</returns>
    ''' <remarks>Llamada desde ObtenerAdjuntos de Ofertas.vb</remarks>
    ''' 
    Public Function Ofertas_ObtenerAdjuntos(ByVal Anyo As Integer, ByVal GMN As String, ByVal Proce As Integer, ByVal Proveedor As String, ByVal Id As Long) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSGS_ADJUNTOS_OFERTA"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@ANYO", Anyo)
            cm.Parameters.AddWithValue("@GMN", GMN)
            cm.Parameters.AddWithValue("@PROCE", Proce)
            cm.Parameters.AddWithValue("@PROVE", Proveedor)
            cm.Parameters.AddWithValue("@ID", Id)
            da.SelectCommand = cm
            da.Fill(dr)

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Sub Ofertas_TransferirAdjuntoDePortal(ByVal iTipo As Integer, ByVal iId As Integer, ByVal iIdPortal As Integer)
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()

        Try
            cn.Open()
            cm = New SqlCommand("BEGIN DISTRIBUTED TRANSACTION", cn)
            cm.ExecuteNonQuery()

            cm.Connection = cn
            cm.CommandText = "FSGS_ADJUNTOS_TRANSFERIRDEPORTAL"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@TIPO", iTipo)
            cm.Parameters.AddWithValue("@ID", iId)
            cm.Parameters.AddWithValue("@IDPORTAL", iIdPortal)
            cm.ExecuteNonQuery()

            cm = New SqlCommand("COMMIT TRANSACTION", cn)
            cm.ExecuteNonQuery()

        Catch e As Exception
            cm = New SqlCommand("ROLLBACK TRANSACTION", cn)
            cm.ExecuteNonQuery()
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    Public Function Ofertas_LeerAdjuntoOferta(ByVal AdjunId As Long, ByVal ChunkNumber As Long, ByVal ChunkSize As Long) As Byte()
        Authenticate()

        Dim cn As New SqlConnection
        Dim cm As SqlCommand = New SqlCommand
        Dim mi_impersionationContext As Security.Principal.WindowsImpersonationContext

        cn = NuevaConexionFileStream()
        mi_impersionationContext = RealizarSuplantacion()
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSGS_LEER_ADJUNTONUEVAOFERTA"
            cm.Parameters.AddWithValue("@ID", AdjunId)

            cm.CommandType = CommandType.StoredProcedure

            'Retrieve the file path of the SQL FILESTREAM BLOB in the first row. 
            Dim filePath As String = Nothing
            Dim pathObj As Object = cm.ExecuteScalar()

            If Not pathObj.Equals(DBNull.Value) Then
                filePath = DirectCast(pathObj, String)
            Else
                Throw New System.Exception(" failed to read the path name en Adjunto_ReadData.")
            End If

            ' Obtain a transaction context. All FILESTREAM BLOB operations occur within a transaction context to maintain data consistency. 
            Dim transaction As SqlTransaction = cn.BeginTransaction("mainTranaction")
            cm.Transaction = transaction
            cm.CommandType = CommandType.Text
            cm.CommandText = "SELECT GET_FILESTREAM_TRANSACTION_CONTEXT()"
            Dim txContext As Byte() = Nothing
            Dim obj As Object = cm.ExecuteScalar()

            If Not obj.Equals(DBNull.Value) Then
                txContext = DirectCast(obj, Byte())
            Else
                Throw New System.Exception("GET_FILESTREAM_TRANSACTION_CONTEXT() failed")
            End If

            Dim sqlFileStream As New SqlTypes.SqlFileStream(filePath, txContext, FileAccess.Read)
            Dim buffer() As Byte
            If sqlFileStream.Length > ChunkSize And ChunkSize > 0 Then
                ReDim buffer(ChunkSize - 1)
                sqlFileStream.Seek((ChunkNumber - 1) * ChunkSize, 0)

                Dim iBytesRead As Integer = sqlFileStream.Read(buffer, 0, buffer.Length)
                ReDim Preserve buffer(iBytesRead - 1)
            Else
                ReDim buffer(sqlFileStream.Length - 1)
                sqlFileStream.Read(buffer, 0, buffer.Length)
            End If

            ' Close the FILESTREAM handle. 
            sqlFileStream.Close()

            cm.Transaction.Commit()

            DeshacerSuplantacion(mi_impersionationContext)

            Ofertas_LeerAdjuntoOferta = buffer
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
#End Region
#End Region
End Class
