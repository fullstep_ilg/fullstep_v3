﻿Imports System.Configuration
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Exception
Imports System.IO
Imports System.Object
Imports Fullstep.FSNLibrary
Partial Public Class Root
#Region "CN. Data Access Methods"
#Region "Mensajes"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="Idioma"></param>
    ''' <param name="UON1"></param>
    ''' <param name="UON2"></param>
    ''' <param name="UON3"></param>
    ''' <param name="DEP"></param>
    ''' <param name="AccesoEP"></param>
    ''' <param name="AccesoGS"></param>
    ''' <param name="AccesoPM"></param>
    ''' <param name="AccesoQA"></param>
    ''' <param name="AccesoSM"></param>
    ''' <param name="LimiteMensajesCargados"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_ObtenerMensajesUsuario(ByVal UsuCod As String, ByVal Idioma As String, _
             ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, _
             ByVal DEP As String, ByVal AccesoEP As Boolean, ByVal AccesoGS As Boolean, _
             ByVal AccesoPM As Boolean, ByVal AccesoQA As Boolean, ByVal AccesoSM As Boolean, _
             ByVal tipo As Integer, ByVal grupo As Integer, ByVal categoria As Integer, ByVal megusta As Boolean, _
             ByVal LimiteMensajesCargados As Integer, ByVal Pagina As Integer, _
             ByVal Historico As Boolean, ByVal Discrepancias As Boolean, _
             ByVal Factura As Integer, ByVal Linea As Integer) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_OBTENER_MENSAJESUSUARIO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@IDI", Idioma)
            cm.Parameters.AddWithValue("@UON1", UON1)
            cm.Parameters.AddWithValue("@UON2", UON2)
            cm.Parameters.AddWithValue("@UON3", UON3)
            cm.Parameters.AddWithValue("@DEP", DEP)
            cm.Parameters.AddWithValue("@ACCESOEP", AccesoEP)
            cm.Parameters.AddWithValue("@ACCESOGS", AccesoGS)
            cm.Parameters.AddWithValue("@ACCESOPM", AccesoPM)
            cm.Parameters.AddWithValue("@ACCESOQA", AccesoQA)
            cm.Parameters.AddWithValue("@ACCESOSM", AccesoSM)
            If (Not tipo = 0 AndAlso Not tipo = 6) Then cm.Parameters.AddWithValue("@TIPO", tipo)
            If Not grupo = 0 Then cm.Parameters.AddWithValue("@IDGRUPO", grupo)
            If Not categoria = 0 Then cm.Parameters.AddWithValue("@IDCATEGORIA", categoria)
            If megusta Then cm.Parameters.AddWithValue("@MEGUSTA", megusta)
            cm.Parameters.AddWithValue("@LIMITEMENSAJESCARGADOS", LimiteMensajesCargados)
            cm.Parameters.AddWithValue("@PAGINA", Pagina)
            cm.Parameters.AddWithValue("@HISTORICO", Historico)
            cm.Parameters.AddWithValue("@DISCREPANCIAS", Discrepancias)
            If Not Factura = 0 Then cm.Parameters.AddWithValue("@FACTURA", Factura)
            If Not Linea = 0 Then cm.Parameters.AddWithValue("@LINEA", Linea)

            da.SelectCommand = cm
            da.Fill(dr)

            dr.Tables(0).TableName = "MENSAJES"
            dr.Tables(1).TableName = "RESPUESTAS"
            dr.Tables(2).TableName = "MEGUSTA"
            dr.Tables(3).TableName = "MEGUSTARESPUESTA"
            dr.Tables(4).TableName = "ADJUNTOS"

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="Idioma"></param>
    ''' <param name="IdMensaje"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_Obtener_Mensaje(ByVal UsuCod As String, ByVal Idioma As String, ByVal IdMensaje As Integer) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_OBTENER_MENSAJE"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@IDI", Idioma)
            cm.Parameters.AddWithValue("@IDMENSAJE", IdMensaje)

            da.SelectCommand = cm
            da.Fill(dr)

            dr.Tables(0).TableName = "MENSAJES"
            dr.Tables(1).TableName = "RESPUESTAS"
            dr.Tables(2).TableName = "MEGUSTA"
            dr.Tables(3).TableName = "MEGUSTARESPUESTA"
            dr.Tables(4).TableName = "ADJUNTOS"

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function CN_Mensajes_GetAdjuntoMensaje(ByVal DGuid As String) As Byte()
        Dim cn As New SqlConnection
        Dim mi_impersionationContext As Security.Principal.WindowsImpersonationContext
        CN_Mensajes_GetAdjuntoMensaje = Nothing

        cn = NuevaConexionFileStream()
        mi_impersionationContext = RealizarSuplantacion()
        cn.Open()

        Dim tx As SqlTransaction = cn.BeginTransaction()
        Dim oSqlFileStream As SqlTypes.SqlFileStream = CN_GetSqlFileStreamAdjunto(0, DGuid, cn, tx)
        If Not oSqlFileStream Is Nothing Then
            Dim Buffer(oSqlFileStream.Length - 1) As Byte
            oSqlFileStream.Read(Buffer, 0, Buffer.Length)
            oSqlFileStream.Close()
            CN_Mensajes_GetAdjuntoMensaje = Buffer
        End If
        tx.Commit()
        cn.Close()
        DeshacerSuplantacion(mi_impersionationContext)
    End Function
    ''' <summary>
    ''' Lee la imagen del usuario en producto cn
    ''' </summary>
    ''' <param name="Guid">Identificador del filestream</param>
    ''' <returns>La Imagen</returns>
    ''' <remarks>Llamadas desde: cnMensajes.vb/CN_Mensajes_GetImagenUsuario, Tiempo mÃ¡ximo: 0</remarks>
    Public Function CN_Mensajes_GetImagenUsuario(ByVal Guid As String) As Byte()
        Dim cn As New SqlConnection
        Dim mi_impersionationContext As Security.Principal.WindowsImpersonationContext
        CN_Mensajes_GetImagenUsuario = Nothing

        cn = NuevaConexionFileStream()
        mi_impersionationContext = RealizarSuplantacion()
        cn.Open()

        Dim tx As SqlTransaction = cn.BeginTransaction()
        Dim oSqlFileStream As SqlTypes.SqlFileStream = CN_GetSqlFileStreamImagen(Guid, cn, tx, True)
        If Not oSqlFileStream Is Nothing Then
            Dim Buffer(oSqlFileStream.Length - 1) As Byte
            oSqlFileStream.Read(Buffer, 0, Buffer.Length)
            oSqlFileStream.Close()
            CN_Mensajes_GetImagenUsuario = Buffer
        End If
        tx.Commit()
        cn.Close()
        DeshacerSuplantacion(mi_impersionationContext)
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="Idioma"></param>
    ''' <param name="UON1"></param>
    ''' <param name="UON2"></param>
    ''' <param name="UON3"></param>
    ''' <param name="DEP"></param>
    ''' <param name="AccesoEP"></param>
    ''' <param name="AccesoGS"></param>
    ''' <param name="AccesoPM"></param>
    ''' <param name="AccesoQA"></param>
    ''' <param name="AccesoSM"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_Categorias_Permiso_EmitirMensajeUsuario(ByVal UsuCod As String, ByVal Idioma As String, _
                ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, _
                ByVal DEP As String, ByVal AccesoEP As Boolean, ByVal AccesoGS As Boolean, _
                ByVal AccesoPM As Boolean, ByVal AccesoQA As Boolean, ByVal AccesoSM As Boolean) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_PERMISO_EMITIRMENSAJEUSUARIO_CATEGORIAS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@IDI", Idioma)
            cm.Parameters.AddWithValue("@UON1", UON1)
            cm.Parameters.AddWithValue("@UON2", UON2)
            cm.Parameters.AddWithValue("@UON3", UON3)
            cm.Parameters.AddWithValue("@DEP", DEP)

            da.SelectCommand = cm
            da.Fill(dr)

            Return dr.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="Idioma"></param>
    ''' <param name="UON1"></param>
    ''' <param name="UON2"></param>
    ''' <param name="UON3"></param>
    ''' <param name="DEP"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_Obtener_UONsPara(ByVal UsuCod As String, ByVal Idioma As String, _
                ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, ByVal DEP As String, _
                ByVal Restr_UON As Boolean, ByVal Restr_Dep As Boolean) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_PERMISO_EMITIRMENSAJEUSUARIO_UON"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@IDI", Idioma)
            cm.Parameters.AddWithValue("@UON1", UON1)
            cm.Parameters.AddWithValue("@UON2", UON2)
            cm.Parameters.AddWithValue("@UON3", UON3)
            cm.Parameters.AddWithValue("@DEP", DEP)
            cm.Parameters.AddWithValue("@RESTR_UON", Restr_UON)
            cm.Parameters.AddWithValue("@RESTR_DEP", Restr_Dep)

            da.SelectCommand = cm
            da.Fill(dr)

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Usu"></param>
    ''' <param name="tipo"></param>
    ''' <param name="Titulo"></param>
    ''' <param name="Contenido"></param>
    ''' <param name="Fecha"></param>
    ''' <param name="Donde"></param>
    ''' <param name="Categoria"></param>
    ''' <param name="UON0"></param>
    ''' <param name="Para_UON"></param>
    ''' <param name="Para_Prove_Tipo"></param>
    ''' <param name="Para_Prove_Con"></param>
    ''' <param name="GMN1"></param>
    ''' <param name="GMN2"></param>
    ''' <param name="GMN3"></param>
    ''' <param name="GMN4"></param>
    ''' <param name="MatQA"></param>
    ''' <param name="ProceCompra_Anyo"></param>
    ''' <param name="ProceCompra_GMN1"></param>
    ''' <param name="ProceCompra_Codigo"></param>
    ''' <param name="ProceParaResp"></param>
    ''' <param name="ProceParaInvitado"></param>
    ''' <param name="ProceParaCompradores"></param>
    ''' <param name="ProceParaProve"></param>
    ''' <param name="Para_EstrucCompras"></param>
    ''' <param name="Para_EstrucCompras_MatGS"></param>
    ''' <param name="Para_Grupo_EP"></param>
    ''' <param name="Para_Grupo_GS"></param>
    ''' <param name="Para_Grupo_PM"></param>
    ''' <param name="Para_Grupo_QA"></param>
    ''' <param name="Para_Grupo_SM"></param>
    ''' <param name="Para_Grupos_Grupos"></param>
    ''' <param name="Adjuntos"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_InsertarMensaje(ByVal Usu As String, ByVal tipo As Integer, ByVal FechaAlta As DateTime, ByVal Titulo As String, ByVal Contenido As String, _
            ByVal Fecha As Nullable(Of DateTime), ByVal Donde As String, ByVal Categoria As Integer, _
            ByVal UON0 As Boolean, ByVal Para_UON As DataTable, ByVal Para_Prove_Tipo As Integer, ByVal Para_Prove_Con As DataTable, _
            ByVal GMN1 As String, ByVal GMN2 As String, ByVal GMN3 As String, ByVal GMN4 As String, ByVal MatQA As Nullable(Of Integer), _
            ByVal ProceCompra_Anyo As Nullable(Of Integer), ByVal ProceCompra_GMN1 As String, ByVal ProceCompra_Codigo As Nullable(Of Integer), _
            ByVal ProceParaResp As Boolean, ByVal ProceParaInvitado As Boolean, _
            ByVal ProceParaCompradores As Boolean, ByVal ProceParaProve As Boolean, _
            ByVal Para_EstrucCompras As DataTable, ByVal Para_EstrucCompras_MatGS As Boolean, _
            ByVal Para_Grupo_EP As Boolean, ByVal Para_Grupo_GS As Boolean, ByVal Para_Grupo_PM As Boolean, _
            ByVal Para_Grupo_QA As Boolean, ByVal Para_Grupo_SM As Boolean, ByVal Para_Grupos_Grupos As DataTable, _
            ByVal Adjuntos As DataTable) As Integer
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter
        cn.Open()

        Dim transaccion As SqlTransaction = cn.BeginTransaction()
        Try
            cm.Connection = cn
            cm.CommandText = "CN_INSERTAR_MENSAJE"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion

            With cm.Parameters
                .AddWithValue("@USU", Usu)
                .AddWithValue("@TIPO", tipo)
                .AddWithValue("@FECALTA", FechaAlta)
                .AddWithValue("@TITULO", Titulo)
                .AddWithValue("@CONTENIDO", Contenido)
                .AddWithValue("@CUANDO", IIf(Fecha.HasValue, Fecha, Nothing))
                .AddWithValue("@DONDE", Donde)
                .AddWithValue("@CATEGORIA", Categoria)
                .AddWithValue("@PROCE_ANYO", IIf(ProceCompra_Anyo.HasValue, ProceCompra_Anyo, Nothing))
                .AddWithValue("@PROCE_GMN1", ProceCompra_GMN1)
                .AddWithValue("@PROCE_COD", IIf(ProceCompra_Codigo.HasValue, ProceCompra_Codigo, Nothing))
                .AddWithValue("@PROCEPARARESP", ProceParaResp)
                .AddWithValue("@PROCEPARAINVITADO", ProceParaInvitado)
                .AddWithValue("@PROCEPARACOMP", ProceParaCompradores)
                .AddWithValue("@PROCEPARAPROVE", ProceParaProve)
                .AddWithValue("@UON0", UON0)
                .AddWithValue("@GMN1", GMN1)
                .AddWithValue("@GMN2", GMN2)
                .AddWithValue("@GMN3", GMN3)
                .AddWithValue("@GMN4", GMN4)
                .AddWithValue("@MATQA", MatQA)
                .AddWithValue("@PROVEPARA", Para_Prove_Tipo)
                .AddWithValue("@COMPPARA", Para_EstrucCompras_MatGS)
                .AddWithValue("@USUEPPARA", Para_Grupo_EP)
                .AddWithValue("@USUGSPARA", Para_Grupo_GS)
                .AddWithValue("@USUPMPARA", Para_Grupo_PM)
                .AddWithValue("@USUQAPARA", Para_Grupo_QA)
                .AddWithValue("@USUSMPARA", Para_Grupo_SM)
                .Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output
            End With

            cm.ExecuteNonQuery()

            Dim Id As Integer = cm.Parameters("@ID").Value
            If Not Para_UON Is Nothing Then
                cm = New SqlCommand("CN_ASOCIAR_MENSAJE_USU", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@MEN", Id)

                    .Add("@UON1", SqlDbType.NVarChar, 20, "UON1")
                    .Add("@UON2", SqlDbType.NVarChar, 20, "UON2")
                    .Add("@UON3", SqlDbType.NVarChar, 20, "UON3")
                    .Add("@DEP", SqlDbType.NVarChar, 20, "DEP")
                    .Add("@USU", SqlDbType.NVarChar, 20, "USU")
                End With

                da.InsertCommand = cm
                da.Update(Para_UON)
            End If

            If Not Para_Prove_Con Is Nothing Then
                cm = New SqlCommand("CN_ASOCIAR_MENSAJE_PROVECON", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@MEN", Id)

                    .Add("@PROVE", SqlDbType.NVarChar, 20, "PROVE")
                    .Add("@CON", SqlDbType.Int, 0, "CON")
                End With

                da.InsertCommand = cm
                da.Update(Para_Prove_Con)
            End If

            If ProceCompra_Anyo.HasValue AndAlso ProceCompra_GMN1 IsNot String.Empty AndAlso ProceCompra_Codigo.HasValue Then
                cm = New SqlCommand("CN_ASOCIAR_MENSAJE_PROCECOMPRAS", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@MEN", Id)
                    .AddWithValue("@PROCE_ANYO", ProceCompra_Anyo)
                    .AddWithValue("@PROCE_GMN1", ProceCompra_GMN1)
                    .AddWithValue("@PROCE_COD", ProceCompra_Codigo)
                    .AddWithValue("@PROCECOMPRA_RESP", ProceParaResp)
                    .AddWithValue("@PROCECOMPRA_INVITADO", ProceParaInvitado)
                    .AddWithValue("@PROCECOMPRA_COMP", ProceParaCompradores)
                    .AddWithValue("@PROCECOMPRA_PROVE", ProceParaProve)
                End With
                cm.ExecuteNonQuery()
            End If

            If Not Para_EstrucCompras Is Nothing Then
                cm = New SqlCommand("CN_ASOCIAR_MENSAJE_EQP", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@MEN", Id)
                    .Add("@EQP", SqlDbType.NVarChar, 20, "EQP")
                    .Add("@USU", SqlDbType.NVarChar, 20, "USU")
                End With

                da.InsertCommand = cm
                da.Update(Para_EstrucCompras)
            End If

            If Not Para_Grupos_Grupos Is Nothing Then
                cm = New SqlCommand("CN_ASOCIAR_MENSAJE_GRUPOS", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@MEN", Id)
                    .Add("@GRUPO", SqlDbType.Int, 0, "GRUPO")
                End With

                da.InsertCommand = cm
                da.Update(Para_Grupos_Grupos)
            End If

            If Not Adjuntos Is Nothing Then
                cm = New SqlCommand("CN_ASOCIAR_MENSAJE_ADJUNTOS", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@MEN", Id)
                    .Add("@ID", SqlDbType.Int, 0, "ID").Direction = ParameterDirection.Output
                    .Add("@NOMBRE", SqlDbType.NVarChar, 200, "NOMBRE")
                    .Add("@GUID", SqlDbType.NVarChar, 50, "GUID")
                    .Add("@SIZE", SqlDbType.Float, 0, "SIZE")
                    .Add("@SIZEUNIT", SqlDbType.NVarChar, 10, "SIZEUNIT")
                    .Add("@TIPOADJUNTO", SqlDbType.TinyInt, 0, "TIPOADJUNTO")
                End With

                da.InsertCommand = cm
                da.Update(Adjuntos)
            End If
            transaccion.Commit()

            Dim nombreFichero As String
            For Each row As DataRow In Adjuntos.Rows
                nombreFichero = row("URL")
                If Not nombreFichero = String.Empty Then
                    Dim input As System.IO.FileStream = New System.IO.FileStream(nombreFichero, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                    Dim buffer(input.Length - 1) As Byte
                    input.Read(buffer, 0, buffer.Length)
                    input.Close()
                    CN_GrabarNuevoAdjun(row("ID"), buffer)
                End If
            Next
            Return Id
        Catch e As Exception
            transaccion.Rollback()
            Return 0
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    Public Sub CN_GrabarNuevoAdjun(ByVal Id As Long, ByVal Buffer As Byte())
        Dim cn As SqlConnection
        Dim mi_impersionationContext As Security.Principal.WindowsImpersonationContext

        cn = NuevaConexionFileStream()
        mi_impersionationContext = RealizarSuplantacion()
        cn.Open()
        Dim tx As SqlTransaction = cn.BeginTransaction()
        Dim oSqlFileStream As SqlTypes.SqlFileStream = CN_GetSqlFileStreamAdjunto(Id, String.Empty, cn, tx)
        If Not oSqlFileStream Is Nothing Then
            oSqlFileStream.Write(Buffer, 0, Buffer.Length)
            oSqlFileStream.Close()
        End If
        tx.Commit()
        cn.Close()
        DeshacerSuplantacion(mi_impersionationContext)
    End Sub
    ''' <summary>
    ''' Graba la imagen del usuario en producto cn
    ''' </summary>
    ''' <param name="DGuidImagen">Identificador del filestream</param>
    ''' <param name="Buffer">imagen</param>
    ''' <remarks>Llamadas desde: CN_Actualizar_Imagen_Usuario; Tiempo mÃ¡ximo: 0</remarks>
    Public Sub CN_GrabarImagen(ByVal DGuidImagen As String, ByVal Buffer As Byte())
        Dim cn As SqlConnection
        Dim mi_impersionationContext As Security.Principal.WindowsImpersonationContext

        cn = NuevaConexionFileStream()
        mi_impersionationContext = RealizarSuplantacion()
        cn.Open()
        Dim tx As SqlTransaction = cn.BeginTransaction()
        Dim oSqlFileStream As SqlTypes.SqlFileStream = CN_GetSqlFileStreamImagen(DGuidImagen, cn, tx, False)
        If Not oSqlFileStream Is Nothing Then
            oSqlFileStream.Write(Buffer, 0, Buffer.Length)
            oSqlFileStream.Close()
        End If
        tx.Commit()
        cn.Close()
        DeshacerSuplantacion(mi_impersionationContext)
    End Sub
    ''' <summary>
    ''' Lee/Escribe un adjunto en producto cn
    ''' Si el Identificador en bbdd es 0 es una lectura, sino es una escritura
    ''' </summary>
    ''' <param name="Id">Identificador en bbdd, a escribir</param>
    ''' <param name="DGuid">Identificador del filestream, a leer</param>
    ''' <param name="cn">Connection</param>
    ''' <param name="tx">Transaction </param>
    ''' <returns>filestream preparado para lectura o escritura</returns>
    ''' <remarks>Llamadas desde:CN_Mensajes_GetAdjuntoMensaje            CN_GrabarNuevoAdjun; Tiempo mÃ¡ximo: 0</remarks>
    Private Function CN_GetSqlFileStreamAdjunto(ByVal Id As Long, ByVal DGuid As String, ByVal cn As SqlConnection, ByVal tx As SqlTransaction) As SqlTypes.SqlFileStream
        Dim sPath As String = String.Empty
        Dim arTransactionContext() As Byte = Nothing
        CN_GetPathNameAndTxContextAdjunto(Id, DGuid, cn, tx, sPath, arTransactionContext)
        '4 - open the filestream to the blob
        CN_GetSqlFileStreamAdjunto = Nothing
        If Not sPath Is Nothing And Not arTransactionContext Is Nothing Then
            If Id = 0 Then
                CN_GetSqlFileStreamAdjunto = New SqlTypes.SqlFileStream(sPath, arTransactionContext, FileAccess.Read)
            Else
                CN_GetSqlFileStreamAdjunto = New SqlTypes.SqlFileStream(sPath, arTransactionContext, FileAccess.Write)
            End If
        End If
    End Function
    Private Sub CN_GetPathNameAndTxContextAdjunto(ByVal Id As Long, ByVal DGuid As String, ByVal cn As SqlConnection, ByVal tx As SqlTransaction, _
        ByRef Path As String, ByRef TxContext As Byte())
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "CN_ADJUNTOS_LEER"
        With cm.Parameters
            If Not Id = 0 Then .AddWithValue("@ID", Id)
            If Not DGuid Is String.Empty Then .AddWithValue("@DGUID", DGuid)
        End With
        cm.Connection = cn
        cm.Transaction = tx
        Dim da As New SqlDataAdapter
        da.SelectCommand = cm
        Dim ds As New System.Data.DataSet
        da.Fill(ds)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim oRow As DataRow = ds.Tables(0).Rows(0)
            If Not oRow.IsNull("PATHNAME") Then Path = oRow("PATHNAME")
            If Not oRow.IsNull("TRANSACTIONCONTEXT") Then TxContext = CType(oRow("TRANSACTIONCONTEXT"), Byte()) 'reader.GetSqlBytes(1).Buffer;
            If Path Is Nothing Then
                'Si se inserta un registro con la columna varbinary(max) a NULL no se crea archivo y FilePath devuelve nulo, con lo que no se puede
                'crear el sqlFileStream.
                'Insertando '' en la columna varbinary(max) se crea un registro de longitud cero y ya se puede obtener un handle para el archivo
                Dim oCom As New SqlCommand
                oCom.CommandType = CommandType.Text
                oCom.CommandText = "UPDATE CN_ADJUN SET DATA=cast('' as varbinary(max)) WHERE ID=" & Id & ""
                oCom.Connection = cn
                oCom.Transaction = tx
                oCom.ExecuteNonQuery()
                Dim obj As Object = cm.ExecuteScalar()
                If Not obj.Equals(DBNull.Value) Then Path = DirectCast(obj, String)
            End If
        End If
    End Sub
    ''' <summary>
    ''' Lee/Escribe una imagen en producto cn
    ''' </summary>
    ''' <param name="Guid">Identificador del filestream</param>
    ''' <param name="cn">Connection</param>
    ''' <param name="tx">Transaction </param>
    ''' <param name="EsLectura">indice si es lectura Ã³ escritura</param>
    ''' <returns>filestream preparado para lectura o escritura</returns>
    ''' <remarks>Llamadas desde:CN_Mensajes_GetImagenUsuario            CN_GrabarImagen; Tiempo mÃ¡ximo: 0</remarks>
    Private Function CN_GetSqlFileStreamImagen(ByVal Guid As String, ByVal cn As SqlConnection, ByVal tx As SqlTransaction, ByVal EsLectura As Boolean) As SqlTypes.SqlFileStream
        Dim sPath As String = String.Empty
        Dim arTransactionContext() As Byte = Nothing
        CN_GetPathNameAndTxContextImagen(Guid, cn, tx, sPath, arTransactionContext)
        '4 - open the filestream to the blob
        CN_GetSqlFileStreamImagen = Nothing
        If Not sPath Is Nothing And Not arTransactionContext Is Nothing Then
            If EsLectura Then
                CN_GetSqlFileStreamImagen = New SqlTypes.SqlFileStream(sPath, arTransactionContext, FileAccess.Read)
            Else
                CN_GetSqlFileStreamImagen = New SqlTypes.SqlFileStream(sPath, arTransactionContext, FileAccess.Write)
            End If
        End If
    End Function
    Private Sub CN_GetPathNameAndTxContextImagen(ByVal Guid As String, ByVal cn As SqlConnection, ByVal tx As SqlTransaction, _
         ByRef Path As String, ByRef TxContext As Byte())
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "CN_USUARIOS_LEER"
        cm.Parameters.AddWithValue("@DGUID", Guid)
        cm.Connection = cn
        cm.Transaction = tx
        Dim da As New SqlDataAdapter
        da.SelectCommand = cm
        Dim ds As New System.Data.DataSet
        da.Fill(ds)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim oRow As DataRow = ds.Tables(0).Rows(0)
            If Not oRow.IsNull("PATHNAME") Then Path = oRow("PATHNAME")
            If Not oRow.IsNull("TRANSACTIONCONTEXT") Then TxContext = CType(oRow("TRANSACTIONCONTEXT"), Byte()) 'reader.GetSqlBytes(1).Buffer;
            If Path Is Nothing Then
                'Si se inserta un registro con la columna varbinary(max) a NULL no se crea archivo y FilePath devuelve nulo, con lo que no se puede
                'crear el sqlFileStream.
                'Insertando '' en la columna varbinary(max) se crea un registro de longitud cero y ya se puede obtener un handle para el archivo
                Dim oCom As New SqlCommand
                oCom.CommandType = CommandType.Text
                oCom.CommandText = "UPDATE CN_USU_ADJUN SET DATA=cast('' as varbinary(max)) WHERE DGUID=" & Guid & ""
                oCom.Connection = cn
                oCom.Transaction = tx
                oCom.ExecuteNonQuery()
                Dim obj As Object = cm.ExecuteScalar()
                If Not obj.Equals(DBNull.Value) Then Path = DirectCast(obj, String)
            End If
        End If
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="IdMensaje"></param>
    ''' <param name="Usu"></param>
    ''' <param name="tipo"></param>
    ''' <param name="FechaAlta"></param>
    ''' <param name="Titulo"></param>
    ''' <param name="Contenido"></param>
    ''' <param name="Fecha"></param>
    ''' <param name="Donde"></param>
    ''' <param name="Categoria"></param>
    ''' <remarks></remarks>
    Public Sub CN_ActualizarMensaje(ByVal IdMensaje As Integer, ByVal Usu As String, ByVal tipo As Integer, ByVal FechaAlta As DateTime, ByVal Titulo As String, ByVal Contenido As String, _
            ByVal Fecha As Nullable(Of DateTime), ByVal Donde As String, ByVal Categoria As Integer, ByRef Adjuntos As DataTable)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter
        cn.Open()
        Dim transaccion As SqlTransaction = cn.BeginTransaction()
        Try
            cm.Connection = cn
            cm.CommandText = "CN_ACTUALIZAR_MENSAJE"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@IDMENSAJE", IdMensaje)
                .AddWithValue("@TIPO", tipo)
                .AddWithValue("@FECACT", FechaAlta)
                .AddWithValue("@TITULO", Titulo)
                .AddWithValue("@CONTENIDO", Contenido)
                .AddWithValue("@CUANDO", IIf(Fecha.HasValue, Fecha, Nothing))
                .AddWithValue("@DONDE", Donde)
                .AddWithValue("@CATEGORIA", Categoria)
            End With
            cm.ExecuteNonQuery()

            If Not Adjuntos Is Nothing Then
                cm = New SqlCommand("CN_ASOCIAR_MENSAJE_ADJUNTOS", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@MEN", IdMensaje)
                    .Add("@ID", SqlDbType.Int, 0, "ID").Direction = ParameterDirection.Output
                    .Add("@NOMBRE", SqlDbType.NVarChar, 200, "NOMBRE")
                    .Add("@GUID", SqlDbType.NVarChar, 50, "GUID")
                    .Add("@SIZE", SqlDbType.Float, 0, "SIZE")
                    .Add("@SIZEUNIT", SqlDbType.NVarChar, 10, "SIZEUNIT")
                    .Add("@TIPOADJUNTO", SqlDbType.TinyInt, 0, "TIPOADJUNTO")
                End With

                da.InsertCommand = cm
                da.Update(Adjuntos)
            End If
            transaccion.Commit()

            Dim nombreFichero As String
            For Each row As DataRow In Adjuntos.Rows
                nombreFichero = row("URL")
                If Not nombreFichero = String.Empty Then
                    Dim input As System.IO.FileStream = New System.IO.FileStream(nombreFichero, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                    Dim buffer(input.Length - 1) As Byte
                    input.Read(buffer, 0, buffer.Length)
                    input.Close()
                    CN_GrabarNuevoAdjun(row("ID"), buffer)
                End If
            Next
        Catch e As Exception
            transaccion.Rollback()
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Usu"></param>
    ''' <param name="IdMensaje"></param>
    ''' <param name="Contenido"></param>
    ''' <param name="MeGusta"></param>
    ''' <param name="IdRespuesta"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_InsertarRespuesta(ByVal Usu As String, ByVal IdMensaje As Integer, ByVal FechaAlta As DateTime, ByVal Contenido As String, ByVal MeGusta As Boolean, _
              ByVal IdRespuesta As Nullable(Of Integer), ByVal Adjuntos As DataTable) As Integer
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter
        cn.Open()
        Dim transaccion As SqlTransaction = cn.BeginTransaction()
        Try
            cm.Connection = cn
            cm.CommandText = "CN_INSERTAR_RESPUESTA"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion

            With cm.Parameters
                .AddWithValue("@USU", Usu)
                .AddWithValue("@IDMENSAJE", IdMensaje)
                .AddWithValue("@FECHAALTA", FechaAlta)
                .AddWithValue("@CONTENIDO", Contenido)
                .AddWithValue("@MEGUSTA", MeGusta)
                .AddWithValue("@IDRESPUESTA", IIf(IdRespuesta.HasValue, IdRespuesta, Nothing))
                .Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output
            End With
            cm.ExecuteNonQuery()

            Dim Id As Integer = cm.Parameters("@ID").Value
            If Not Adjuntos Is Nothing Then
                cm = New SqlCommand("CN_ASOCIAR_MENSAJE_ADJUNTOS", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@RESP", Id)
                    .AddWithValue("@MEN", IdMensaje)
                    .Add("@ID", SqlDbType.Int, 0, "ID").Direction = ParameterDirection.Output
                    .Add("@NOMBRE", SqlDbType.NVarChar, 200, "NOMBRE")
                    .Add("@GUID", SqlDbType.NVarChar, 50, "GUID")
                    .Add("@SIZE", SqlDbType.Float, 0, "SIZE")
                    .Add("@SIZEUNIT", SqlDbType.NVarChar, 10, "SIZEUNIT")
                    .Add("@TIPOADJUNTO", SqlDbType.TinyInt, 0, "TIPOADJUNTO")
                End With
                da.InsertCommand = cm
                da.Update(Adjuntos)
            End If
            transaccion.Commit()

            Dim nombreFichero As String
            For Each row As DataRow In Adjuntos.Rows
                nombreFichero = row("URL")
                If Not nombreFichero = String.Empty Then
                    Dim input As System.IO.FileStream = New System.IO.FileStream(nombreFichero, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                    Dim buffer(input.Length - 1) As Byte
                    input.Read(buffer, 0, buffer.Length)
                    input.Close()
                    CN_GrabarNuevoAdjun(row("ID"), buffer)
                End If
            Next
            Return Id
        Catch e As Exception
            Return 0
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="IdMensaje"></param>
    ''' <param name="IdRespuesta"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_InsertarMeGusta(ByVal Usu As String, ByVal Idi As String, ByVal IdMensaje As Integer, ByVal IdRespuesta As Integer) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm = New SqlCommand("CN_INSERTAR_MEGUSTA", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300

            With cm.Parameters
                .AddWithValue("@USU", Usu)
                .AddWithValue("@IDI", Idi)
                .AddWithValue("@IDMENSAJE", IdMensaje)
                If Not IdRespuesta = 0 Then
                    .AddWithValue("@IDRESPUESTA", IdRespuesta)
                End If
            End With

            da.SelectCommand = cm
            da.Fill(dr)

            dr.Tables(0).TableName = "MENSAJES"
            dr.Tables(1).TableName = "RESPUESTAS"
            dr.Tables(2).TableName = "MEGUSTA"
            dr.Tables(3).TableName = "MEGUSTARESPUESTA"
            dr.Tables(4).TableName = "ADJUNTOS"

            Return dr
        Catch e As Exception
            Return Nothing
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_ProveedoresContacto_Permiso_EmitirMensajeUsuario(ByVal UsuCod As String, ByVal EquipoUsu As String, ByVal RestriccionEnvioProveedoresMaterialUsu As Boolean, _
                 ByVal RestriccionEnvioProveedoresEquipoComprasUsu As Boolean, ByVal RestriccionEnvioProveedoresConContactoCalidad As Boolean, _
                 ByVal CodigoBuscado As String, ByVal NIFBuscado As String, ByVal DenominacionBuscada As String) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_PERMISO_EMITIRMENSAJEUSUARIO_PROVEEDORESCONTACTOS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@EQPUSU", EquipoUsu)
            cm.Parameters.AddWithValue("@RESTRICPROVEMAT", RestriccionEnvioProveedoresMaterialUsu)
            cm.Parameters.AddWithValue("@RETRICEQP", RestriccionEnvioProveedoresEquipoComprasUsu)
            cm.Parameters.AddWithValue("@RESTRICQACON", RestriccionEnvioProveedoresConContactoCalidad)
            cm.Parameters.AddWithValue("@COD", IIf(CodigoBuscado = String.Empty, Nothing, CodigoBuscado))
            cm.Parameters.AddWithValue("@NIF", IIf(NIFBuscado = String.Empty, Nothing, NIFBuscado))
            cm.Parameters.AddWithValue("@DEN", IIf(DenominacionBuscada = String.Empty, Nothing, DenominacionBuscada))

            da.SelectCommand = cm
            da.Fill(dr)

            Return dr.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_GruposCorporativos_Permiso_EmitirMensajeUsuario(ByVal UsuCod As String, ByVal Idi As String, _
                       ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, _
                       ByVal DEP As String) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_PERMISO_EMITIRMENSAJEUSUARIO_GRUPOSCORPORATIVOS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@IDI", Idi)
            cm.Parameters.AddWithValue("@UON1", UON1)
            cm.Parameters.AddWithValue("@UON2", UON2)
            cm.Parameters.AddWithValue("@UON3", UON3)
            cm.Parameters.AddWithValue("@DEP", DEP)

            da.SelectCommand = cm
            da.Fill(dr)

            Return dr.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_EquiposCompra_Permiso_EmitirMensajeUsuario(ByVal UsuCod As String, ByVal EquipoUsuario As String, _
                     ByVal RestriccionEquipoUsuario As Boolean) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_PERMISO_EMITIRMENSAJEUSUARIO_EQUIPOSCOMPRA"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@EQPUSU", EquipoUsuario)
            cm.Parameters.AddWithValue("@RESTRIC_EQPUSU", RestriccionEquipoUsuario)

            da.SelectCommand = cm
            da.Fill(ds)

            ds.Tables(0).TableName = "EQUIPOS"
            ds.Tables(1).TableName = "PERSONAS"
            Return ds
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_Obtener_ProcesoCompra_Anyos(ByVal UsuCod As String, ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, _
               ByVal Dep As String, ByVal Equipo As String, ByVal RestricMat As Boolean, _
               ByVal RestricAsigUsu As Boolean, ByVal RestricAsigEqpUsu As Boolean, _
               ByVal RestricUsuDep As Boolean, ByVal RestricUsuUO As Boolean, _
               ByVal RestricUsuAbto As Boolean, ByVal RestricUsuResponsable As Boolean, ByVal RestricPerfUsuUO As Boolean) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_OBTENER_PROCECOMPRA_ANYOS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@UON1", UON1)
            cm.Parameters.AddWithValue("@UON2", UON2)
            cm.Parameters.AddWithValue("@UON3", UON3)
            cm.Parameters.AddWithValue("@DEP", Dep)
            cm.Parameters.AddWithValue("@EQP", Equipo)
            cm.Parameters.AddWithValue("@RESTRIC_MAT_USU", RestricMat)
            cm.Parameters.AddWithValue("@RESTRIC_COMP_ASIG", RestricAsigUsu)
            cm.Parameters.AddWithValue("@RESTRIC_EQP_ASIG", RestricAsigEqpUsu)
            cm.Parameters.AddWithValue("@RESTRIC_USU_DEP", RestricUsuDep)
            cm.Parameters.AddWithValue("@RESTRIC_USU_UO", RestricUsuUO)
            cm.Parameters.AddWithValue("@RESTRIC_ABTO_USU", RestricUsuAbto)
            cm.Parameters.AddWithValue("@RESTRIC_USU_RESP", RestricUsuResponsable)
            cm.Parameters.AddWithValue("@RESTRIC_PERF_USU_UO", RestricPerfUsuUO)

            da.SelectCommand = cm
            da.Fill(ds)

            Return ds.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>Devuelve los diferentes códigos GMN1 de los procesos de compra que puede consultar el usuario</summary>
    ''' <param name="UsuCod">Código del usuario</param>
    ''' <param name="Idioma">Código del idioma</param>
    ''' <param name="UON1"> Código UON1 al que pertenece el usuario</param>
    ''' <param name="UON2">Código UON2 al que pertenece el usuario</param>
    ''' <param name="UON3">Código UON3 al que pertenece el usuario</param>
    ''' <param name="Dep">Código DEP al que pertenece el usuario</param>
    ''' <param name="Equipo">Código del equipo al que pertenece el usuario</param>
    ''' <param name="RestricMat">Si el usuario tiene restricción al material de usuario</param>
    ''' <param name="RestricAsigUsu">Si el usuario tiene restricción a los compradores asignados</param>
    ''' <param name="RestricAsigEqpUsu">Si el usuario tiene restricción al equipo asignado</param>
    ''' <param name="RestricUsuDep">Si el usuario tiene restricción al departamento de usuario</param>
    ''' <param name="RestricUsuUO">Si el usuario tiene restricción a la uon de usuario</param>
    ''' <param name="RestricUsuAbto">Si el usuario tiene restricción a los procesos abiertos por el usuario</param>
    ''' <param name="RestricUsuResponsable">Si el usuario tiene restricción a los procesos en los que el usuario es responsable</param>
    ''' <param name="RestricPerfUsuUO">Si el usuario tiene restricción a las uon del perfil de usuario</param>
    ''' <param name="Anyo"> El año por el que filtraremos la busqueda</param>    
    ''' <returns>DataTable con los datos</returns>
    ''' <revision>LTG 26/08/2013</revision>
    Public Function CN_Obtener_ProcesoCompra_GMNs(ByVal UsuCod As String, ByVal Idioma As String, ByVal UON1 As String, _
               ByVal UON2 As String, ByVal UON3 As String, ByVal Dep As String, ByVal Equipo As String, ByVal RestricMat As Boolean, _
               ByVal RestricAsigUsu As Boolean, ByVal RestricAsigEqpUsu As Boolean, ByVal RestricUsuDep As Boolean, ByVal RestricUsuUO As Boolean, _
               ByVal RestricUsuAbto As Boolean, ByVal RestricUsuResponsable As Boolean, ByVal RestricPerfUsuUO As Boolean, ByVal Anyo As Integer) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_OBTENER_PROCECOMPRA_GMNS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@IDI", Idioma)
            cm.Parameters.AddWithValue("@UON1", UON1)
            cm.Parameters.AddWithValue("@UON2", UON2)
            cm.Parameters.AddWithValue("@UON3", UON3)
            cm.Parameters.AddWithValue("@DEP", Dep)
            cm.Parameters.AddWithValue("@EQP", Equipo)
            cm.Parameters.AddWithValue("@RESTRIC_MAT_USU", RestricMat)
            cm.Parameters.AddWithValue("@RESTRIC_COMP_ASIG", RestricAsigUsu)
            cm.Parameters.AddWithValue("@RESTRIC_EQP_ASIG", RestricAsigEqpUsu)
            cm.Parameters.AddWithValue("@RESTRIC_USU_DEP", RestricUsuDep)
            cm.Parameters.AddWithValue("@RESTRIC_USU_UO", RestricUsuUO)
            cm.Parameters.AddWithValue("@RESTRIC_ABTO_USU", RestricUsuAbto)
            cm.Parameters.AddWithValue("@RESTRIC_USU_RESP", RestricUsuResponsable)
            cm.Parameters.AddWithValue("@RESTRIC_PERF_USU_UO", RestricPerfUsuUO)
            cm.Parameters.AddWithValue("@ANYO", Anyo)

            da.SelectCommand = cm
            da.Fill(ds)

            Return ds.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_Obtener_ProcesoCompra_CodDen(ByVal UsuCod As String, ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, _
               ByVal Dep As String, ByVal Equipo As String, ByVal RestricMat As Boolean, _
               ByVal RestricAsigUsu As Boolean, ByVal RestricAsigEqpUsu As Boolean, _
               ByVal RestricUsuDep As Boolean, ByVal RestricUsuUO As Boolean, _
               ByVal RestricUsuAbto As Boolean, ByVal RestricUsuResponsable As Boolean, ByVal RestricPerfUsuUO As Boolean, _
               ByVal Anyo As Integer, ByVal GMN1 As String, Optional ByVal Cod As Integer = 0) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_OBTENER_PROCECOMPRA_CODDEN"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@UON1", UON1)
            cm.Parameters.AddWithValue("@UON2", UON2)
            cm.Parameters.AddWithValue("@UON3", UON3)
            cm.Parameters.AddWithValue("@DEP", Dep)
            cm.Parameters.AddWithValue("@EQP", Equipo)
            cm.Parameters.AddWithValue("@RESTRIC_MAT_USU", RestricMat)
            cm.Parameters.AddWithValue("@RESTRIC_COMP_ASIG", RestricAsigUsu)
            cm.Parameters.AddWithValue("@RESTRIC_EQP_ASIG", RestricAsigEqpUsu)
            cm.Parameters.AddWithValue("@RESTRIC_USU_DEP", RestricUsuDep)
            cm.Parameters.AddWithValue("@RESTRIC_USU_UO", RestricUsuUO)
            cm.Parameters.AddWithValue("@RESTRIC_ABTO_USU", RestricUsuAbto)
            cm.Parameters.AddWithValue("@RESTRIC_USU_RESP", RestricUsuResponsable)
            cm.Parameters.AddWithValue("@RESTRIC_PERF_USU_UO", RestricPerfUsuUO)
            cm.Parameters.AddWithValue("@ANYO", Anyo)
            cm.Parameters.AddWithValue("@GMN1", GMN1)
            If Not Cod = 0 Then cm.Parameters.AddWithValue("@COD", Cod)

            da.SelectCommand = cm
            da.Fill(ds)

            Return ds.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_Obtener_BuscadorEntidad_Tipos(ByVal UsuCod As String, ByVal Idioma As String, ByVal TipoEntidad As Integer) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSPMQA_GET_SOLICITUDES"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@IDI", Idioma)
            cm.Parameters.AddWithValue("@COMBO", 1)
            cm.Parameters.AddWithValue("@TIPO", TipoEntidad)
            da.SelectCommand = cm
            da.Fill(dr)

            dr.Tables(0).Rows.RemoveAt(0)
            Return dr.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="TipoEntidad"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_Obtener_BuscadorEntidad_Proveedores(ByVal UsuCod As String, ByVal TipoEntidad As Integer, _
            ByVal CodProve As String, ByVal RestricProvMat As Boolean, _
            ByVal RestricProvEqp As Boolean, ByVal RestricProvCon As Boolean) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSQA_PROVEEDORES_CERTIF"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@USALIKE", False)
            If TipoEntidad = TiposDeDatos.TipoDeSolicitud.NoConformidad Then
                cm.Parameters.AddWithValue("@CON_NOCONF", 1)
            End If
            If CodProve IsNot String.Empty Then
                cm.Parameters.AddWithValue("@COD", CodProve)
            End If
            cm.Parameters.AddWithValue("@REST_MAT", RestricProvMat)
            cm.Parameters.AddWithValue("@REST_EQP", RestricProvEqp)
            cm.Parameters.AddWithValue("@REST_CON", RestricProvCon)

            da.SelectCommand = cm
            da.Fill(dr)
            Return dr.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="Idioma"></param>
    ''' <param name="TipoEntidad"></param>
    ''' <param name="TipoSolicitud"></param>
    ''' <param name="ProveCod"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_Obtener_BuscadorEntidad_Identificador(ByVal UsuCod As String, ByVal Idioma As String, ByVal TipoEntidad As Integer, _
            ByVal TipoSolicitud As Integer, ByVal ProveCod As String, _
            ByVal RestricSolicUsu As Boolean, ByVal RestricSolicUO As Boolean, ByVal RestricSolicDep As Boolean, _
            ByVal RestricProvMat As Boolean, ByVal RestricProvEqp As Boolean, ByVal RestricProvCon As Boolean) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSQA_DEVOLVER_TITULOS_USU"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@IDI", Idioma)
            cm.Parameters.AddWithValue("@TIPO", TipoEntidad)
            If Not TipoSolicitud = 0 Then
                cm.Parameters.AddWithValue("@TIPOSOLICITUD", TipoSolicitud)
            End If
            If Not ProveCod = String.Empty Then
                cm.Parameters.AddWithValue("@PROVECOD", ProveCod)
            End If
            cm.Parameters.AddWithValue("@REST_SOLICIT_USU", RestricSolicUsu)
            cm.Parameters.AddWithValue("@REST_SOLICIT_UO", RestricSolicUO)
            cm.Parameters.AddWithValue("@REST_SOLICIT_DEP", RestricSolicDep)
            cm.Parameters.AddWithValue("@REST_PROV_MAT", RestricProvMat)
            cm.Parameters.AddWithValue("@REST_PROV_EQP", RestricProvEqp)
            cm.Parameters.AddWithValue("@REST_PROV_CON", RestricProvCon)

            da.SelectCommand = cm
            da.Fill(dr)
            Return dr.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="Idioma"></param>
    ''' <param name="TipoEntidad"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_Comprobar_BuscadorEntidad_Identificador(ByVal UsuCod As String, ByVal Idioma As String, _
            ByVal TipoEntidad As Integer, ByVal Identificador As Integer, _
            ByVal RestricSolicUsu As Boolean, ByVal RestricSolicUO As Boolean, ByVal RestricSolicDep As Boolean, _
            ByVal RestricProvMat As Boolean, ByVal RestricProvEqp As Boolean, ByVal RestricProvCon As Boolean) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            If TipoEntidad = 1 Or TipoEntidad = 5 Then
                cm.CommandText = "FSPM_INSTANCIAS_USU"
            Else
                cm.CommandText = "CN_COMPROBAR_INSTANCIA_ENTIDAD"
                cm.Parameters.AddWithValue("@REST_SOLICIT_USU", RestricSolicUsu)
                cm.Parameters.AddWithValue("@REST_SOLICIT_UO", RestricSolicUO)
                cm.Parameters.AddWithValue("@REST_SOLICIT_DEP", RestricSolicDep)
                cm.Parameters.AddWithValue("@REST_PROV_MAT", RestricProvMat)
                cm.Parameters.AddWithValue("@REST_PROV_EQP", RestricProvEqp)
                cm.Parameters.AddWithValue("@REST_PROV_CON", RestricProvCon)
            End If
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@IDI", Idioma)
            cm.Parameters.AddWithValue("@TIPO", TipoEntidad)
            If TipoEntidad = 1 Then cm.Parameters.AddWithValue("@TODASSOLICITUDES", 1)
            cm.Parameters.AddWithValue("@INSTANCIA", Identificador)

            da.SelectCommand = cm
            da.Fill(dr)
            Return dr.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Idi"></param>
    ''' <param name="Pyme"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_Obtener_MaterialesQA(ByVal Idi As String, ByVal Pyme As Integer) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSQA_LOADMATERIALES"
            cm.Parameters.AddWithValue("@IDI", Idi)
            cm.Parameters.AddWithValue("@BAJA", False)
            cm.Parameters.AddWithValue("@CERTIFICADOS", 0)
            cm.Parameters.AddWithValue("@PYME", Pyme)
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(dr)

            Return dr.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Idi"></param>
    ''' <param name="lPyme"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_Obtener_MaterialesGS(ByVal Usu As String, ByVal Idi As String, ByVal lPyme As Integer, ByVal Restr_Prove_Mat As Boolean) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_PERMISO_EMITIR_MATGS"
            cm.Parameters.AddWithValue("@USU", Usu)
            cm.Parameters.AddWithValue("@IDI", Idi)
            cm.Parameters.AddWithValue("@PYME", lPyme)
            cm.Parameters.AddWithValue("@REST_PROVE_MAT", Restr_Prove_Mat)
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(dr)

            Dim parentCols(0) As DataColumn
            Dim childCols(0) As DataColumn

            parentCols(0) = dr.Tables(0).Columns("COD")
            childCols(0) = dr.Tables(1).Columns("GMN1")

            dr.Relations.Add("REL_NIV1_NIV2", parentCols, childCols, False)

            ReDim parentCols(1)
            ReDim childCols(1)

            parentCols(0) = dr.Tables(1).Columns("GMN1")
            parentCols(1) = dr.Tables(1).Columns("COD")
            childCols(0) = dr.Tables(2).Columns("GMN1")
            childCols(1) = dr.Tables(2).Columns("GMN2")
            dr.Relations.Add("REL_NIV2_NIV3", parentCols, childCols, False)

            ReDim parentCols(2)
            ReDim childCols(2)
            parentCols(0) = dr.Tables(2).Columns("GMN1")
            parentCols(1) = dr.Tables(2).Columns("GMN2")
            parentCols(2) = dr.Tables(2).Columns("COD")
            childCols(0) = dr.Tables(3).Columns("GMN1")
            childCols(1) = dr.Tables(3).Columns("GMN2")
            childCols(2) = dr.Tables(3).Columns("GMN3")

            dr.Relations.Add("REL_NIV3_NIV4", parentCols, childCols, False)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Usu"></param>
    ''' <param name="Idi"></param>
    ''' <param name="IdCategoria"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_Obtener_DatosAdicionales_Mensaje(ByVal Usu As String, ByVal Idi As String, _
           ByVal IdMensaje As Nullable(Of Integer), ByVal IdCategoria As Nullable(Of Integer), _
           ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, ByVal DEP As String, _
           ByVal Restr_UON As Boolean, ByVal Restr_Dep As Boolean, _
           ByVal EqpUsu As String, ByVal Restr_Eqp As Boolean) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_OBTENER_DATOSADICIONALES_MENSAJE"
            cm.Parameters.AddWithValue("@IDI", Idi)
            If IdMensaje Is Nothing Then
                cm.Parameters.AddWithValue("@USU", Usu)
            End If
            If IdMensaje IsNot Nothing Then cm.Parameters.AddWithValue("@IDMEN", IdMensaje)
            If IdCategoria IsNot Nothing Then cm.Parameters.AddWithValue("@IDCATEGORIA", IdCategoria)
            If IdMensaje Is Nothing AndAlso IdCategoria Is Nothing Then
                cm.Parameters.AddWithValue("@UON1", UON1)
                cm.Parameters.AddWithValue("@UON2", UON2)
                cm.Parameters.AddWithValue("@UON3", UON3)
                cm.Parameters.AddWithValue("@DEP", DEP)
                cm.Parameters.AddWithValue("@RESTR_UO", Restr_UON)
                cm.Parameters.AddWithValue("@RESTR_DEP", Restr_Dep)
                cm.Parameters.AddWithValue("@EQP", EqpUsu)
                cm.Parameters.AddWithValue("@RESTR_EQP", Restr_Eqp)
            End If
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(dr)

            dr.Tables(0).TableName = "INFOMENSAJE"
            dr.Tables(1).TableName = "PARAUON"
            dr.Tables(2).TableName = "PARAPROVE"
            dr.Tables(3).TableName = "PARAPROCECOMPRAS"
            dr.Tables(4).TableName = "PARAESTRUCCOMPRAS"
            dr.Tables(5).TableName = "PARAGRUPOS"

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Usu"></param>
    ''' <param name="Idi"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_Obtener_BuscadorInstancia_Usu(ByVal Usu As String, ByVal Idi As String, _
                   Optional ByVal TipoEntidad As Integer = 0, Optional ByVal TipoSolicitud As Integer = 0, _
                   Optional ByVal Prove As String = "", Optional ByVal TipoInfo As Integer = 0) As DataTable
        Authenticate()

        If Prove = "" Then Prove = String.Empty
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSPM_INSTANCIAS_USU"
            cm.Parameters.AddWithValue("@USU", Usu)
            cm.Parameters.AddWithValue("@IDI", Idi)
            If Not TipoEntidad = 0 Then cm.Parameters.AddWithValue("@TIPO", TipoEntidad)
            If TipoEntidad = 0 Then cm.Parameters.AddWithValue("@TODASSOLICITUDES", 1)
            If Not TipoSolicitud = 0 Then cm.Parameters.AddWithValue("@TIPOSOLICITUD", TipoSolicitud)
            If Not Prove Is String.Empty Then cm.Parameters.AddWithValue("@PROVE", Prove)
            If Not TipoInfo = 0 Then cm.Parameters.AddWithValue("@TIPOINFO", TipoInfo)
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(dr)

            Return dr.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="IdMensaje"></param>
    ''' <param name="Usu"></param>
    ''' <param name="UON0"></param>
    ''' <param name="Para_UON"></param>
    ''' <param name="Para_Prove_Tipo"></param>
    ''' <param name="Para_Prove_Con"></param>
    ''' <param name="GMN1"></param>
    ''' <param name="GMN2"></param>
    ''' <param name="GMN3"></param>
    ''' <param name="GMN4"></param>
    ''' <param name="MatQA"></param>
    ''' <param name="ProceCompra_Anyo"></param>
    ''' <param name="ProceCompra_GMN1"></param>
    ''' <param name="ProceCompra_Codigo"></param>
    ''' <param name="ProceParaResp"></param>
    ''' <param name="ProceParaInvitado"></param>
    ''' <param name="ProceParaCompradores"></param>
    ''' <param name="ProceParaProve"></param>
    ''' <param name="Para_EstrucCompras"></param>
    ''' <param name="Para_EstrucCompras_MatGS"></param>
    ''' <param name="Para_Grupo_EP"></param>
    ''' <param name="Para_Grupo_GS"></param>
    ''' <param name="Para_Grupo_PM"></param>
    ''' <param name="Para_Grupo_QA"></param>
    ''' <param name="Para_Grupo_SM"></param>
    ''' <param name="Para_Grupos_Grupos"></param>
    ''' <remarks></remarks>
    Public Sub CN_ModificarDestinatarios(ByVal IdMensaje As Integer, ByVal Usu As String, _
            ByVal UON0 As Boolean, ByVal Para_UON As DataTable, ByVal Para_Prove_Tipo As Integer, ByVal Para_Prove_Con As DataTable, _
            ByVal GMN1 As String, ByVal GMN2 As String, ByVal GMN3 As String, ByVal GMN4 As String, ByVal MatQA As Nullable(Of Integer), _
            ByVal ProceCompra_Anyo As Nullable(Of Integer), ByVal ProceCompra_GMN1 As String, ByVal ProceCompra_Codigo As Nullable(Of Integer), _
            ByVal ProceParaResp As Boolean, ByVal ProceParaInvitado As Boolean, _
            ByVal ProceParaCompradores As Boolean, ByVal ProceParaProve As Boolean, _
            ByVal Para_EstrucCompras As DataTable, ByVal Para_EstrucCompras_MatGS As Boolean, _
            ByVal Para_Grupo_EP As Boolean, ByVal Para_Grupo_GS As Boolean, ByVal Para_Grupo_PM As Boolean, _
            ByVal Para_Grupo_QA As Boolean, ByVal Para_Grupo_SM As Boolean, ByVal Para_Grupos_Grupos As DataTable)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter
        cn.Open()
        Dim transaccion As SqlTransaction = cn.BeginTransaction()
        Try
            cm = New SqlCommand("CN_DESASOCIAR_MENSAJE", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion

            With cm.Parameters
                .AddWithValue("@MEN", IdMensaje)
                .AddWithValue("@USU", Usu)
            End With

            cm.ExecuteNonQuery()

            cm = New SqlCommand("CN_ACTUALIZAR_PARA_MENSAJE", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion

            With cm.Parameters
                .AddWithValue("@MEN", IdMensaje)
                .AddWithValue("@PROCE_ANYO", IIf(ProceCompra_Anyo.HasValue, ProceCompra_Anyo, Nothing))
                .AddWithValue("@PROCE_GMN1", ProceCompra_GMN1)
                .AddWithValue("@PROCE_COD", IIf(ProceCompra_Codigo.HasValue, ProceCompra_Codigo, Nothing))
                .AddWithValue("@PROCEPARARESP", ProceParaResp)
                .AddWithValue("@PROCEPARAINVITADO", ProceParaInvitado)
                .AddWithValue("@PROCEPARACOMP", ProceParaCompradores)
                .AddWithValue("@PROCEPARAPROVE", ProceParaProve)
                .AddWithValue("@UON0", UON0)
                .AddWithValue("@GMN1", GMN1)
                .AddWithValue("@GMN2", GMN2)
                .AddWithValue("@GMN3", GMN3)
                .AddWithValue("@GMN4", GMN4)
                .AddWithValue("@MATQA", MatQA)
                .AddWithValue("@PROVEPARA", Para_Prove_Tipo)
                .AddWithValue("@COMPPARA", Para_EstrucCompras_MatGS)
                .AddWithValue("@USUEPPARA", Para_Grupo_EP)
                .AddWithValue("@USUGSPARA", Para_Grupo_GS)
                .AddWithValue("@USUPMPARA", Para_Grupo_PM)
                .AddWithValue("@USUQAPARA", Para_Grupo_QA)
                .AddWithValue("@USUSMPARA", Para_Grupo_SM)
            End With

            cm.ExecuteNonQuery()

            If Not Para_UON Is Nothing Then
                cm = New SqlCommand("CN_ASOCIAR_MENSAJE_USU", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@MEN", IdMensaje)

                    .Add("@UON1", SqlDbType.NVarChar, 20, "UON1")
                    .Add("@UON2", SqlDbType.NVarChar, 20, "UON2")
                    .Add("@UON3", SqlDbType.NVarChar, 20, "UON3")
                    .Add("@DEP", SqlDbType.NVarChar, 20, "DEP")
                    .Add("@USU", SqlDbType.NVarChar, 20, "USU")
                End With

                da.InsertCommand = cm
                da.Update(Para_UON)
            End If

            If Not Para_Prove_Con Is Nothing Then
                cm = New SqlCommand("CN_ASOCIAR_MENSAJE_PROVECON", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@MEN", IdMensaje)

                    .Add("@PROVE", SqlDbType.NVarChar, 20, "PROVE")
                    .Add("@CON", SqlDbType.Int, 0, "CON")
                End With

                da.InsertCommand = cm
                da.Update(Para_Prove_Con)
            End If

            If ProceCompra_Anyo.HasValue AndAlso ProceCompra_GMN1 IsNot String.Empty AndAlso ProceCompra_Codigo.HasValue Then
                cm = New SqlCommand("CN_ASOCIAR_MENSAJE_PROCECOMPRAS", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@MEN", IdMensaje)
                    .AddWithValue("@PROCE_ANYO", ProceCompra_Anyo)
                    .AddWithValue("@PROCE_GMN1", ProceCompra_GMN1)
                    .AddWithValue("@PROCE_COD", ProceCompra_Codigo)
                    .AddWithValue("@PROCECOMPRA_RESP", ProceParaResp)
                    .AddWithValue("@PROCECOMPRA_INVITADO", ProceParaInvitado)
                    .AddWithValue("@PROCECOMPRA_COMP", ProceParaCompradores)
                    .AddWithValue("@PROCECOMPRA_PROVE", ProceParaProve)
                End With
            End If

            If Not Para_EstrucCompras Is Nothing Then
                cm = New SqlCommand("CN_ASOCIAR_MENSAJE_EQP", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@MEN", IdMensaje)
                    .Add("@EQP", SqlDbType.NVarChar, 20, "EQP")
                    .Add("@USU", SqlDbType.NVarChar, 20, "USU")
                End With

                da.InsertCommand = cm
                da.Update(Para_EstrucCompras)
            End If

            If Not Para_Grupos_Grupos Is Nothing Then
                cm = New SqlCommand("CN_ASOCIAR_MENSAJE_GRUPOS", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@MEN", IdMensaje)
                    .Add("@GRUPO", SqlDbType.Int, 0, "GRUPO")
                End With

                da.InsertCommand = cm
                da.Update(Para_Grupos_Grupos)
            End If

            transaccion.Commit()
        Catch e As Exception
            transaccion.Rollback()
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Comprueba el número de mensajes nuevos del usuario por tipo.
    ''' </summary>
    ''' <param name="Usu"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_Comprobar_Notificaciones(ByVal Usu As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm = New SqlCommand("CN_COMPROBAR_NOTIFICACIONES", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300

            With cm.Parameters
                .AddWithValue("@USU", Usu)
            End With

            da.SelectCommand = cm
            da.Fill(dr)

            dr.Tables(0).TableName = "NOTIFICACIONES"
            dr.Tables(1).TableName = "URGENTES"

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Devuelve los datos del adjunto partiendo del identificador unico GUID
    ''' </summary>
    ''' <param name="DGuid">Guid del adjunto</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_Obtener_Nombre_Adjunto(ByVal DGuid As String) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_OBTENER_NOMBRE_ADJUNTO"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            With cm.Parameters
                .AddWithValue("@DGUID", DGuid)
            End With

            da.SelectCommand = cm
            da.Fill(dr)

            Return dr.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Carga los grupos a los que pertenece el usuario
    ''' </summary>
    ''' <param name="sUsuCod">CÃ³digo de usuario </param>
    ''' <param name="sIdioma">Idioma en q mostrar los textos</param> 
    ''' <returns>Los grupos del usuario</returns>
    ''' <remarks>Llamado desde: cnGrupos.vb  ; Tiempo mÃ¡ximo: 0,3</remarks>
    ''' <revisado>JVS 08/02/2012</revisado>
    Public Function CN_Load_Grupos_Usuario(ByVal sUsuCod As String, ByVal sIdioma As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_OBTENER_GRUPOSUSUARIO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", sUsuCod)
            cm.Parameters.AddWithValue("@IDI", sIdioma)

            da.SelectCommand = cm
            da.Fill(ds)

            Dim dks As DataColumn() = New DataColumn(0) {}
            dks(0) = ds.Tables(0).Columns(0)
            ds.Tables(0).PrimaryKey = dks

            Return ds
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Carga los usuarios, departamentos y UONs de los grupos
    ''' </summary>
    ''' <returns>usuarios, departamentos y UONs</returns>
    ''' <remarks>Llamado desde: cnGrupos.vb  ; Tiempo mÃ¡ximo: 0,3</remarks>
    ''' <revisado>JVS 08/02/2012</revisado>''' 
    Public Function CN_Load_Miembros_Grupos() As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_OBTENER_MIEMBROSGRUPOS"
            cm.CommandType = CommandType.StoredProcedure

            da.SelectCommand = cm
            da.Fill(ds)

            Return ds

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Guarda las opciones de CN del usuario
    ''' </summary>
    ''' <param name="sUsuCod">Código de usuario </param>
    ''' <param name="bNotificarResumenActividad">Opción de usuario: Resumen de actividad</param>
    ''' <param name="bNotificarIncluidoGrupo">Opción de usuario: Me incluyen en un grupo corporativo</param>
    ''' <param name="bConfiguracionDesocultar">Volver a mostrar automaticamente los mensajes ocultos en los que haya actividad</param>
    ''' <remarks>Llamado desde: ConsultasCN.asmx.vb  ; Tiempo máximo: 0,1</remarks>
    ''' <revisado>JVS 08/02/2012</revisado>''' 
    Public Sub CN_Update_Opciones_Usuario(ByVal sUsuCod As String,
               ByVal bNotificarResumenActividad As Boolean, ByVal bNotificarIncluidoGrupo As Boolean,
               ByVal bConfiguracionDesocultar As Boolean)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "BEGIN TRANSACTION"
            cm.ExecuteNonQuery()
            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = "CN_UPDATE_OPCIONES_USU"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Clear()
            cm.Parameters.AddWithValue("@USU", sUsuCod)
            cm.Parameters.AddWithValue("@NOTIFRESACTI", bNotificarResumenActividad)
            cm.Parameters.AddWithValue("@NOTIFINCGRUP", bNotificarIncluidoGrupo)
            cm.Parameters.AddWithValue("@CONFIGDESOCU", bConfiguracionDesocultar)
            cm.ExecuteNonQuery()

            cm = New SqlCommand("COMMIT TRANSACTION", cn)
            cm.ExecuteNonQuery()
        Catch e As Exception
            cm = New SqlCommand("IF @@TRANCOUNT>0 ROLLBACK TRANSACTION", cn)
            cm.ExecuteNonQuery()
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Guarda la imagen del usuario
    ''' </summary>
    ''' <param name="sUsuCod">Código de usuario </param>
    ''' <remarks>Llamado desde: ConsultasCN.asmx.vb  ; Tiempo máximo: 0,1</remarks>
    ''' <revisado>JVS 08/02/2012</revisado>''' 
    Public Sub CN_Actualizar_Imagen_Usuario(ByVal pathFoto As String, ByVal sUsuCod As String, ByRef DGuidImagen As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            If DGuidImagen Is String.Empty Then
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "BEGIN TRANSACTION"
                cm.ExecuteNonQuery()
                cm = New SqlCommand
                cm.Connection = cn
                cm.CommandText = "CN_UPDATE_IMAGEN_USU"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.Clear()
                cm.Parameters.AddWithValue("@USU", sUsuCod)
                cm.Parameters.Add("@DGUID", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output

                cm.ExecuteNonQuery()
                DGuidImagen = cm.Parameters("@DGUID").Value
                cm = New SqlCommand("COMMIT TRANSACTION", cn)
                cm.ExecuteNonQuery()
            End If
            Dim nombreFichero As String
            nombreFichero = pathFoto
            If Not nombreFichero = String.Empty Then
                Dim input As System.IO.FileStream = New System.IO.FileStream(nombreFichero, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Dim buffer(input.Length - 1) As Byte
                input.Read(buffer, 0, buffer.Length)
                input.Close()
                CN_GrabarImagen(DGuidImagen, buffer)
            End If

        Catch e As Exception
            cm = New SqlCommand("IF @@TRANCOUNT>0 ROLLBACK TRANSACTION", cn)
            cm.ExecuteNonQuery()
            Throw e
        Finally
            If cn.State = ConnectionState.Open Then
                cn.Dispose()
                cm.Dispose()
            End If
        End Try
    End Sub
    ''' <summary>
    ''' Carga de categorías
    ''' </summary>
    ''' <param name="sUsuCod">Código de usuario </param>
    ''' <param name="sIdioma">Idioma en q mostrar los textos</param> 
    ''' <param name="vRestricc">Restricción categorías:
    '''					  1 - dadas de alta por el usuario
    '''					  2 - dadas de alta en el departamento del usuario
    '''					  3 - dadas de alta en la unidad organizativa del usuario
    '''					  4 - sin restricción </param>
    ''' <returns>Las categorías</returns>
    ''' <remarks>Llamado desde: cnCategorias.vb  ; Tiempo máximo: 0,3</remarks>
    ''' <revisado>JVS 16/02/2012</revisado>
    Public Function CN_Load_Categorias(ByVal sUsuCod As String,
              ByVal sIdioma As String,
              ByVal vRestricc As OrganizacionRed) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand

        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_OBTENER_CATEGORIAS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", sUsuCod)
            cm.Parameters.AddWithValue("@IDI", sIdioma)
            cm.Parameters.AddWithValue("@RESTRICCION", vRestricc)

            da.SelectCommand = cm

            da.Fill(ds, "CN_CAT")
            Dim rel As New DataRelation("CN_CAT", ds.Tables("CN_CAT").Columns("ID"), ds.Tables("CN_CAT").Columns("CAT"))
            ds.Relations.Add(rel)
            ds.Tables("CN_CAT").PrimaryKey = New DataColumn() {ds.Tables("CN_CAT").Columns("ID")}

            Return ds
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Carga los miembros de la red con permiso de publicación en la categoría
    ''' </summary>
    ''' <param name="Idi">Idioma</param>
    ''' <param name="IdCategoria">Id de categoría</param>
    ''' <returns></returns>
    ''' <remarks>Llamado desde: cnCategoria.vb  ; Tiempo máximo: 0,3</remarks>
    ''' <revisado>JVS 08/03/2012</revisado>
    Public Function CN_Load_Miembros_Categoria(ByVal Idi As String, _
               ByVal IdCategoria As Nullable(Of Integer)) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_OBTENER_MIEMBROS_CATEGORIA"
            cm.Parameters.AddWithValue("@IDI", Idi)
            If IdCategoria IsNot Nothing Then cm.Parameters.AddWithValue("@IDCATEGORIA", IdCategoria)
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(dr)

            dr.Tables(0).TableName = "PARAUON"
            dr.Tables(1).TableName = "PARAGRUPOS"
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Inserta  en base de datos los datos de la categoría
    ''' </summary>
    ''' <param name="iIdCategPadre">Id de categoría padre</param>
    ''' <param name="sUsuCod">Código de usuario </param>
    ''' <param name="iNivel">Nivel </param>
    ''' <param name="sIdioma">Idioma en q mostrar los textos</param>
    ''' <param name="tPermisoPublic">Permiso para publicar (privado, público, sin permiso)</param>
    ''' <param name="bDespublicado">Despublicado</param>
    ''' <param name="dFechaAlta">Fecha de alta</param>
    ''' <param name="dtDenomCat">Tabla Denominación</param>
    ''' <param name="dtUON">tabla de UON</param>
    ''' <param name="dtGrupos">tabla de grupos</param>
    ''' <remarks>Llamado desde: cn_Categorias.aspx.vb  ; Tiempo máximo: 0,1</remarks>
    ''' <revisado>JVS 14/03/2012</revisado>
    Public Sub CN_InsertarCategoria(ByVal iIdCategPadre As Integer,
              ByVal sUsuCod As String,
              ByVal sIdioma As String,
              ByVal iNivel As Integer,
              ByVal tPermisoPublic As PermisoPublicacion,
              ByVal bDespublicado As Boolean,
              ByVal dFechaAlta As DateTime,
              ByVal dtDenomCat As DataTable,
              ByVal dtUON As DataTable,
              ByVal dtGrupos As DataTable)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter
        cn.Open()

        Dim transaccion As SqlTransaction = cn.BeginTransaction()
        Try
            cm.Connection = cn
            cm.CommandText = "CN_INSERTAR_CATEGORIA"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            cm.Parameters.Clear()
            If Not iIdCategPadre = 0 Then
                cm.Parameters.AddWithValue("@IDCATEGORIAPADRE", iIdCategPadre)
            End If
            cm.Parameters.AddWithValue("@USU", sUsuCod)
            cm.Parameters.AddWithValue("@IDI", sIdioma)
            cm.Parameters.AddWithValue("@NIVEL", iNivel)
            If tPermisoPublic = PermisoPublicacion.SinPermiso Then
                cm.Parameters.AddWithValue("@PERMISOPUBLIC", DBNull.Value)
            Else
                cm.Parameters.AddWithValue("@PERMISOPUBLIC", tPermisoPublic)
            End If
            cm.Parameters.AddWithValue("@DESPUBLICADO", BooleanToSQLBinary(bDespublicado))
            cm.Parameters.AddWithValue("@FECALTA", dFechaAlta)
            cm.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output
            cm.ExecuteNonQuery()

            Dim iIdCateg As Integer = cm.Parameters("@ID").Value

            If Not dtDenomCat Is Nothing Then
                cm = New SqlCommand("CN_UPDATE_DENOM_CATEGORIA", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@IDCAT", iIdCateg)
                    .Add("@IDI", SqlDbType.NVarChar, 3, "IDI")
                    .Add("@DENCAT", SqlDbType.NVarChar, 200, "DEN")
                End With

                da.InsertCommand = cm
                da.Update(dtDenomCat)

            End If

            If Not dtUON Is Nothing Then
                cm = New SqlCommand("CN_ASOCIAR_CATEGORIA_UON", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@IDCATEG", iIdCateg)

                    .Add("@UON1", SqlDbType.NVarChar, 20, "UON1")
                    .Add("@UON2", SqlDbType.NVarChar, 20, "UON2")
                    .Add("@UON3", SqlDbType.NVarChar, 20, "UON3")
                    .Add("@DEP", SqlDbType.NVarChar, 20, "DEP")
                End With

                da.InsertCommand = cm
                da.Update(dtUON)
            End If

            If Not dtGrupos Is Nothing Then
                cm = New SqlCommand("CN_ASOCIAR_CATEGORIA_GRUPOS", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@IDCATEG", iIdCateg)

                    .Add("@GRUPO", SqlDbType.Int, 0, "GRUPO")
                End With

                da.InsertCommand = cm
                da.Update(dtGrupos)
            End If

            transaccion.Commit()
        Catch e As Exception
            transaccion.Rollback()
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Guarda  en base de datos los datos de la categoría
    ''' </summary>
    ''' <param name="iIdCateg">Id de categoría </param>
    ''' <param name="sIdioma">Idioma en q mostrar los textos</param>
    ''' <param name="tPermisoPublic">Permiso para publicar (privado, público, sin permiso)</param>
    ''' <param name="bDespublicado">Despublicado</param>
    ''' <param name="dtDenomCat">Tabla Denominación</param>
    ''' <param name="dtUON">tabla de UON</param>
    ''' <param name="dtGrupos">tabla de grupos</param>
    ''' <remarks>Llamado desde: cn_Categorias.aspx.vb  ; Tiempo máximo: 0,1</remarks>
    ''' <revisado>JVS 09/03/2012</revisado>
    Public Sub CN_Update_Categoria(ByVal iIdCateg As Integer,
              ByVal sIdioma As String,
              ByVal tPermisoPublic As PermisoPublicacion,
              ByVal bDespublicado As Boolean,
              ByVal dtDenomCat As DataTable,
              ByVal dtUON As DataTable,
              ByVal dtGrupos As DataTable)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter

        cn.Open()
        Dim transaccion As SqlTransaction = cn.BeginTransaction()
        Try
            cm = New SqlCommand("CN_UPDATE_CATEGORIA", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion

            With cm.Parameters
                .AddWithValue("@IDCATEGORIA", iIdCateg)
                .AddWithValue("@IDI", sIdioma)
                If tPermisoPublic = PermisoPublicacion.SinPermiso Then
                    .AddWithValue("@PERMISOPUBLIC", DBNull.Value)
                Else
                    .AddWithValue("@PERMISOPUBLIC", CType(tPermisoPublic, Boolean))
                End If
                .AddWithValue("@DESPUBLICADO", BooleanToSQLBinary(bDespublicado))
            End With
            cm.ExecuteNonQuery()

            cm = New SqlCommand("CN_DESASOCIAR_CATEGORIA", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion

            With cm.Parameters
                .AddWithValue("@IDCATEG", iIdCateg)
            End With

            cm.ExecuteNonQuery()

            If Not dtDenomCat Is Nothing Then
                cm = New SqlCommand("CN_UPDATE_DENOM_CATEGORIA", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@IDCAT", iIdCateg)
                    .Add("@IDI", SqlDbType.NVarChar, 3, "IDI")
                    .Add("@DENCAT", SqlDbType.NVarChar, 200, "DEN")
                End With

                da.InsertCommand = cm
                da.Update(dtDenomCat)
            End If

            If Not dtUON Is Nothing Then
                cm = New SqlCommand("CN_ASOCIAR_CATEGORIA_UON", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@IDCATEG", iIdCateg)

                    .Add("@UON1", SqlDbType.NVarChar, 20, "UON1")
                    .Add("@UON2", SqlDbType.NVarChar, 20, "UON2")
                    .Add("@UON3", SqlDbType.NVarChar, 20, "UON3")
                    .Add("@DEP", SqlDbType.NVarChar, 20, "DEP")
                End With

                da.InsertCommand = cm
                da.Update(dtUON)
            End If

            If Not dtGrupos Is Nothing Then
                cm = New SqlCommand("CN_ASOCIAR_CATEGORIA_GRUPOS", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@IDCATEG", iIdCateg)

                    .Add("@GRUPO", SqlDbType.Int, 0, "GRUPO")
                End With

                da.InsertCommand = cm
                da.Update(dtGrupos)
            End If

            transaccion.Commit()
        Catch e As Exception
            transaccion.Rollback()
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Borra la categoría
    ''' </summary>
    ''' <param name="iIdCateg">Id de categoría </param>
    ''' <remarks>Llamado desde: CN.asmx.vb  ; Tiempo máximo: 0,1</remarks>
    ''' <revisado>JVS 09/03/2012</revisado>
    Public Sub CN_Delete_Categoria(ByVal iIdCateg As Integer)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_DELETE_CATEGORIA"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDCATEGORIA", iIdCateg)
            cm.ExecuteNonQuery()
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Determina si la categoría tiene mensajes.
    ''' </summary>
    ''' <param name="iIdCateg">Id de categoría </param>
    ''' <returns>Si la categoría tiene mensajes</returns>
    ''' <remarks>Llamado desde: CN.asmx.vb  ; Tiempo máximo: 0,1</remarks>
    Public Function CN_ComprobarDelete_Categoria(ByVal iIdCateg As Integer) As Boolean
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_COMPROBAR_HAYMENSAJESCATEGORIA"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@CATEG", iIdCateg)
            da.SelectCommand = cm
            da.Fill(dr)

            Return (dr.Tables(0).Rows(0).Item(0) > 0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Obtener categorías por nivel
    ''' </summary>
    ''' <param name="sUsuCod">Código de usuario </param>
    ''' <param name="sIdioma">Idioma en q mostrar los textos</param> 
    ''' <param name="vRestricc">Restricción categorías:
    '''					  1 - dadas de alta por el usuario
    '''					  2 - dadas de alta en el departamento del usuario
    '''					  3 - dadas de alta en la unidad organizativa del usuario
    '''					  4 - sin restricción </param>
    ''' <param name="iIdCat">Id de categoría </param>
    ''' <returns>Las categorías</returns>
    ''' <remarks>Llamado desde: cnCategorias.vb  ; Tiempo máximo: 0,3</remarks>
    ''' <revisado>JVS 01/03/2012</revisado>
    Public Function CN_Obtener_ListaCategorias(ByVal sUsuCod As String,
              ByVal sIdioma As String,
              ByVal vRestricc As OrganizacionRed,
              Optional ByVal iIdCat As Integer = 0) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand

        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_OBTENER_CATEGORIAS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", sUsuCod)
            cm.Parameters.AddWithValue("@IDI", sIdioma)
            cm.Parameters.AddWithValue("@RESTRICCION", vRestricc)
            cm.Parameters.AddWithValue("@IDCAT", iIdCat)

            da.SelectCommand = cm
            da.Fill(ds)

            Dim dks As DataColumn() = New DataColumn(0) {}
            dks(0) = ds.Tables(0).Columns(0)
            ds.Tables(0).PrimaryKey = dks

            Return ds.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Cargar datos de la categoría
    ''' </summary>
    ''' <param name="iIdCat">Id de categoría </param>
    ''' <param name="sIdi">Idioma del usuario</param>
    ''' <returns>Las categorías</returns>
    ''' <remarks>Llamado desde: cnCategorias.vb  ; Tiempo máximo: 0,3</remarks>
    ''' <revisado>JVS 05/03/2012</revisado>
    Public Function CN_LoadCategoriaData(ByVal iIdCat As Integer, ByVal sIdi As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand

        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_OBTENER_CATEGORIA"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDCAT", iIdCat)
            cm.Parameters.AddWithValue("@IDI", sIdi)

            da.SelectCommand = cm
            da.Fill(ds)

            Dim dks As DataColumn() = New DataColumn(0) {}
            dks(0) = ds.Tables(0).Columns(0)
            ds.Tables(0).PrimaryKey = dks

            Return ds
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>Indica si hay registros despublicados de categorías
    ''' </summary>
    ''' <param name="sUsuCod">Código de usuario </param>
    ''' <param name="sIdioma">Idioma en q mostrar los textos</param> 
    ''' <param name="vRestricc">Restricción categorías:
    '''					  1 - dadas de alta por el usuario
    '''					  2 - dadas de alta en el departamento del usuario
    '''					  3 - dadas de alta en la unidad organizativa del usuario
    '''					  4 - sin restricción </param>
    ''' <returns>0/1</returns>
    ''' <remarks>Llamado desde: cnCategorias.vb  ; Tiempo máximo: 0,3</remarks>
    ''' <revisado>JVS 08/05/2012</revisado>
    Public Function CN_Despublicados_Categorias(ByVal sUsuCod As String,
              ByVal sIdioma As String,
              ByVal vRestricc As OrganizacionRed) As Integer
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand

        Dim da As New SqlDataAdapter
        Dim iNumeroDespub As Integer
        Dim iDespub As Integer

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_OBTENER_NUMDESPUB_CATEGORIAS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", sUsuCod)
            cm.Parameters.AddWithValue("@IDI", sIdioma)
            cm.Parameters.AddWithValue("@RESTRICCION", vRestricc)

            iNumeroDespub = DBNullToSomething(cm.ExecuteScalar())
            cn.Close()

            If iNumeroDespub = 0 Then
                iDespub = 0
            Else
                iDespub = 1
            End If

            Return iDespub
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Obtener UONs para asignar a la Categoría
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="Idioma"></param>
    ''' <returns>DataSet</returns>
    ''' <remarks>Llamado desde: cnCategoria.vb  ; Tiempo máximo: 0,3</remarks>
    ''' <revisado>JVS 09/03/2012</revisado>
    Public Function CN_Obtener_UONs_Categoria(ByVal UsuCod As String, ByVal Idioma As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_PERMISO_EMITIRMENSAJEUSUARIO_UON"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDI", Idioma)
            cm.Parameters.AddWithValue("@RESTR_UON", False)
            cm.Parameters.AddWithValue("@RESTR_DEP", False)
            cm.Parameters.AddWithValue("@USUARIOS", False)

            da.SelectCommand = cm
            da.Fill(dr)

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Obtener grupos para asignar a la Categoría
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="Idioma"></param>
    ''' <returns>DataSet</returns>
    ''' <remarks>Llamado desde: cnCategoria.vb  ; Tiempo máximo: 0,3</remarks>
    ''' <revisado>JVS 13/03/2012</revisado>
    Public Function CN_Obtener_Grupos_Categoria(ByVal UsuCod As String, ByVal Idioma As String) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_PERMISO_EMITIRMENSAJEUSUARIO_GRUPOSCORPORATIVOS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDI", Idioma)

            da.SelectCommand = cm
            da.Fill(dr)

            Return dr.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Carga los grupos que puede administrar el usuario
    ''' </summary>
    ''' <param name="sUsuCod">CÃ³digo de usuario </param>
    ''' <param name="sIdioma">Idioma en q mostrar los textos</param> 
    ''' <param name="vRestricc">RestricciÃ³n grupos:
    '''					  1 - dados de alta por el usuario
    '''					  2 - dados de alta en el departamento del usuario
    '''					  3 - dados de alta en la unidad organizativa del usuario
    '''					  4 - sin restricciÃ³n </param>
    ''' <returns>Los grupos</returns>
    ''' <remarks>Llamado desde: cnGrupos.vb  ; Tiempo mÃ¡ximo: 0,3</remarks>
    ''' <revisado>JVS 27/03/2012</revisado>
    Public Function CN_Load_Grupos(ByVal sUsuCod As String,
              ByVal sIdioma As String,
              ByVal vRestricc As OrganizacionRed) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand

        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_OBTENER_GRUPOS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", sUsuCod)
            cm.Parameters.AddWithValue("@IDI", sIdioma)
            cm.Parameters.AddWithValue("@RESTRICCION", vRestricc)

            da.SelectCommand = cm
            da.Fill(ds)

            Dim dks As DataColumn() = New DataColumn(0) {}
            dks(0) = ds.Tables(0).Columns(0)
            ds.Tables(0).PrimaryKey = dks

            Return ds.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Carga los miembros del grupo
    ''' </summary>
    ''' <param name="sIdioma">Idioma en q mostrar los textos</param> 
    ''' <param name="iIdGrupo">Grupo </param>
    ''' <returns>Los miembros del grupo</returns>
    ''' <remarks>Llamado desde: cnGrupo.vb  ; Tiempo máximo: 0,3</remarks>
    ''' <revisado>JVS 27/03/2012</revisado>
    Public Function CN_Load_Miembros_Grupo(ByVal sIdioma As String, ByVal iIdGrupo As Integer) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand

        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_OBTENER_MIEMBROS_GRUPO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDGRUPO", iIdGrupo)
            cm.Parameters.AddWithValue("@IDI", sIdioma)

            da.SelectCommand = cm
            da.Fill(ds)

            Return ds.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Obtener UONs para asignar al grupo
    ''' </summary>
    ''' <param name="Idioma"></param>
    ''' <returns>DataSet</returns>
    ''' <remarks>Llamado desde: cnGrupo.vb  ; Tiempo máximo: 0,3</remarks>
    ''' <revisado>JVS 28/03/2012</revisado>
    Public Function CN_Obtener_UONs_Grupo(ByVal Idioma As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_PERMISO_EMITIRMENSAJEUSUARIO_UON"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDI", Idioma)
            cm.Parameters.AddWithValue("@RESTR_UON", False)
            cm.Parameters.AddWithValue("@RESTR_DEP", False)

            da.SelectCommand = cm
            da.Fill(dr)

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Inserta  en base de datos los datos del grupo
    ''' </summary>
    ''' <param name="sUsuCod">Código de usuario </param>
    ''' <param name="sIdioma">Idioma en q mostrar los textos</param>
    ''' <param name="dtDenom">tabla de denominaciones</param>0
    ''' <param name="dtUON">tabla de UON</param>
    ''' <remarks>Llamado desde: cnGrupo.vb  ; Tiempo máximo: 0,1</remarks>
    ''' <revisado>JVS 23/03/2012</revisado>
    Public Function CN_InsertarGrupo(ByVal sUsuCod As String, ByVal sIdioma As String, _
      ByVal dtDenom As DataTable, ByVal dtUON As DataTable)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter
        cn.Open()
        Dim transaccion As SqlTransaction = cn.BeginTransaction()
        Try
            cm.Connection = cn
            cm.CommandText = "CN_INSERTAR_GRUPO"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            cm.Parameters.Clear()
            cm.Parameters.AddWithValue("@USU", sUsuCod)

            cm.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output
            cm.ExecuteNonQuery()

            Dim iIdGrupo As Integer = cm.Parameters("@ID").Value

            If Not dtDenom Is Nothing Then
                cm = New SqlCommand("CN_UPDATE_DENOM_GRUPO", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@IDGRUPO", iIdGrupo)
                    .Add("@IDI", SqlDbType.NVarChar, 3, "IDI")
                    .Add("@NOMGRUPO", SqlDbType.NVarChar, 200, "DEN")
                    .Add("@DESCRGRUPO", SqlDbType.NVarChar, 500, "DESCR")
                End With

                da.InsertCommand = cm
                da.Update(dtDenom)
            End If

            If Not dtUON Is Nothing Then
                cm = New SqlCommand("CN_ASOCIAR_GRUPO_UON", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@IDGRUPO", iIdGrupo)
                    .Add("@UON1", SqlDbType.NVarChar, 20, "UON1")
                    .Add("@UON2", SqlDbType.NVarChar, 20, "UON2")
                    .Add("@UON3", SqlDbType.NVarChar, 20, "UON3")
                    .Add("@DEP", SqlDbType.NVarChar, 20, "DEP")
                    .Add("@USU", SqlDbType.NVarChar, 20, "USU")
                End With

                da.InsertCommand = cm
                da.Update(dtUON)
            End If
            transaccion.Commit()

            Return iIdGrupo
        Catch e As Exception
            transaccion.Rollback()
            Return 0
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Guarda  en base de datos los datos del grupo
    ''' </summary>
    ''' <param name="iIdGrupo">Id de grupo </param>
    ''' <param name="sIdioma">Idioma en q mostrar los textos</param>
    ''' <param name="dtDenom">tabla de denominaciones</param>
    ''' <param name="dtUON">tabla de UON</param>
    ''' <remarks>Llamado desde: cnGrupo.vb  ; Tiempo máximo: 0,1</remarks>
    ''' <revisado>JVS 23/03/2012</revisado>
    Public Sub CN_Update_Grupo(ByVal iIdGrupo As Integer, ByVal sIdioma As String, ByVal dtDenom As DataTable, ByRef dtUON As DataTable)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        cn.Open()
        Dim transaccion As SqlTransaction = cn.BeginTransaction()
        Try
            cm = New SqlCommand("CN_OBTENER_MIEMBROS_GRUPO", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDGRUPO", iIdGrupo)
            cm.Parameters.AddWithValue("@IDI", sIdioma)
            cm.Transaction = transaccion

            da.SelectCommand = cm
            da.Fill(ds)
            Dim sWhere As String
            For Each row As DataRow In ds.Tables(0).Rows
                sWhere = IIf(row("UON1") Is DBNull.Value, "UON1 IS NULL", "UON1='" & row("UON1") & "'") &
                 " AND " & IIf(row("UON2") Is DBNull.Value, "UON2 IS NULL", "UON2='" & row("UON2") & "'") &
                 " AND " & IIf(row("UON3") Is DBNull.Value, "UON3 IS NULL", "UON3='" & row("UON3") & "'") &
                 " AND " & IIf(row("DEP") Is DBNull.Value, "DEP IS NULL", "DEP='" & row("DEP") & "'") &
                 " AND " & IIf(row("USU") Is DBNull.Value, "USU IS NULL", "USU='" & row("USU") & "'")
                If dtUON.Select(sWhere).Length = 1 Then
                    dtUON.Select(sWhere)(0)("NOTIFICAR") = False
                End If
            Next

            cm = New SqlCommand("CN_DESASOCIAR_GRUPO", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion

            With cm.Parameters
                .AddWithValue("@IDGRUPO", iIdGrupo)
            End With

            cm.ExecuteNonQuery()

            If Not dtDenom Is Nothing Then
                cm = New SqlCommand("CN_UPDATE_DENOM_GRUPO", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@IDGRUPO", iIdGrupo)
                    .Add("@IDI", SqlDbType.NVarChar, 3, "IDI")
                    .Add("@NOMGRUPO", SqlDbType.NVarChar, 200, "DEN")
                    .Add("@DESCRGRUPO", SqlDbType.NVarChar, 500, "DESCR")
                End With

                da.InsertCommand = cm
                da.Update(dtDenom)
            End If

            If Not dtUON Is Nothing Then
                cm = New SqlCommand("CN_ASOCIAR_GRUPO_UON", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@IDGRUPO", iIdGrupo)

                    .Add("@UON1", SqlDbType.NVarChar, 20, "UON1")
                    .Add("@UON2", SqlDbType.NVarChar, 20, "UON2")
                    .Add("@UON3", SqlDbType.NVarChar, 20, "UON3")
                    .Add("@DEP", SqlDbType.NVarChar, 20, "DEP")
                    .Add("@USU", SqlDbType.NVarChar, 20, "USU")
                End With

                da.InsertCommand = cm
                da.Update(dtUON)
            End If

            transaccion.Commit()
        Catch e As Exception
            transaccion.Rollback()
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Borra el grupo
    ''' </summary>
    ''' <param name="iIdGrupo">Id de grupo </param>
    ''' <remarks>Llamado desde: cnGrupo.vb  ; Tiempo maximo: 0,1</remarks>
    ''' <revisado>JVS 23/03/2012</revisado>
    Public Sub CN_Delete_Grupo(ByVal iIdGrupo As Integer)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "BEGIN TRANSACTION"
            cm.ExecuteNonQuery()
            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = "CN_DELETE_GRUPO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Clear()
            cm.Parameters.AddWithValue("@IDGRUPO", iIdGrupo)
            cm.ExecuteNonQuery()

            cm = New SqlCommand("COMMIT TRANSACTION", cn)
            cm.ExecuteNonQuery()

        Catch e As Exception
            cm = New SqlCommand("IF @@TRANCOUNT>0 ROLLBACK TRANSACTION", cn)
            cm.ExecuteNonQuery()
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Carga los datos del grupo
    ''' </summary>
    ''' <param name="iIdGrupo">Grupo </param>
    ''' <returns>Los datos del grupo</returns>
    ''' <remarks>Llamado desde: cnGrupo.vb  ; Tiempo maximo: 0,3</remarks>
    ''' <revisado>JVS 28/03/2012</revisado>
    Public Function CN_Load_Datos_Grupo(ByVal iIdGrupo As Integer) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand

        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_OBTENER_DATOS_GRUPO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDGRUPO", iIdGrupo)

            da.SelectCommand = cm
            da.Fill(ds)

            Dim dks As DataColumn() = New DataColumn(0) {}
            dks(0) = ds.Tables(0).Columns(0)
            ds.Tables(0).PrimaryKey = dks

            Return ds.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="Idioma"></param>
    ''' <param name="IdMensaje"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_MensajeUrgente_Leido(ByVal UsuCod As String, ByVal Idioma As String, ByVal IdMensaje As Integer) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_MENSAJEURGENTE_LEIDO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@IDI", Idioma)
            cm.Parameters.AddWithValue("@IDMENSAJE", IdMensaje)

            da.SelectCommand = cm
            da.Fill(dr)

            If dr.Tables.Count > 0 Then
                dr.Tables(0).TableName = "MENSAJES"
                dr.Tables(1).TableName = "RESPUESTAS"
                dr.Tables(2).TableName = "MEGUSTA"
                dr.Tables(3).TableName = "MEGUSTARESPUESTA"
                dr.Tables(4).TableName = "ADJUNTOS"
            End If
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="Idioma"></param>
    ''' <param name="TipoOrdenacion"></param>
    ''' <param name="TextoBusqueda"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_ObtenerResultadosBusqueda(ByVal UsuCod As String, ByVal Idioma As String, ByVal Idioma_FTS As Integer, _
                ByVal TipoOrdenacion As Integer, ByVal TextoBusqueda As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_BUSCADOR"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@IDI", Idioma)
            cm.Parameters.AddWithValue("@LCID", Idioma_FTS)
            cm.Parameters.AddWithValue("@SEARCH", TextoBusqueda)
            cm.Parameters.AddWithValue("@ORDERTYPE", TipoOrdenacion)
            da.SelectCommand = cm
            da.Fill(dr)

            dr.Tables(0).TableName = "MENSAJES"
            dr.Tables(1).TableName = "RESPUESTAS"
            dr.Tables(2).TableName = "MEGUSTA"
            dr.Tables(3).TableName = "MEGUSTARESPUESTA"
            dr.Tables(4).TableName = "ADJUNTOS"

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="IdMensaje"></param>
    ''' <param name="Ocultar"></param>
    ''' <remarks></remarks>
    Public Sub CN_OcultarMensaje(ByVal UsuCod As String, ByVal IdMensaje As Integer, ByVal Ocultar As Boolean)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_OCULTAR_MENSAJE"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@MEN", IdMensaje)
            cm.Parameters.AddWithValue("@OCULTAR", Ocultar)
            cm.ExecuteNonQuery()
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_Comprobar_HayMensajesHistoricos(ByVal UsuCod As String) As Boolean
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_COMPROBAR_HAYMENSAJESHISTORICOS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            da.SelectCommand = cm
            da.Fill(dr)

            Return CType(dr.Tables(0).Rows(0).Item(0), Boolean)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CN_Obtener_Pedidos(ByVal PerCod As String, ByVal PermisoVerPedidosCCImputables As Boolean, _
         ByVal Anio As Nullable(Of Integer), ByVal NumPedido As Nullable(Of Integer), _
         ByVal NumOrden As Nullable(Of Integer)) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_OBTENER_PEDIDOS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@PER", PerCod)
            cm.Parameters.AddWithValue("@PERMISO_VER_CC_IMPUTABLES", PermisoVerPedidosCCImputables)
            cm.Parameters.AddWithValue("@ANIO", Anio)
            cm.Parameters.AddWithValue("@NUMPEDIDO", NumPedido)
            cm.Parameters.AddWithValue("@NUMORDEN", NumOrden)

            da.SelectCommand = cm
            da.Fill(ds)

            Return ds.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Backup de CN
    ''' </summary>
    ''' <param name="periodoExclusion">Periodo de exclusion</param>
    ''' <param name="limiteRegistros">Limite de registros</param>
    ''' <remarks></remarks>
    ''' <revisado>JVS 11/04/2012</revisado>
    Public Sub CN_Historico_BackupCNInicioTarea(ByVal periodoExclusion As Integer, ByVal limiteRegistros As Integer)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cn.Open()
            With cm
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "CN_BACKUPCN"
                .Parameters.AddWithValue("@PERIODOEXC", periodoExclusion)
                .Parameters.AddWithValue("@LIMITEREG", limiteRegistros)
                .ExecuteNonQuery()
            End With
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Obtiene las categorÃƒÂ­as en las que el usuario tiene mensajes que puede consultar
    ''' </summary>
    ''' <param name="UsuCod">cÃƒÂ³digo del usuario</param>
    ''' <param name="Idioma">idioma en el que obtener los datos</param>
    ''' <param name="AccesoEP">Si el usuario tiene acceso a EP</param>
    ''' <param name="AccesoGS">Si el usuario tiene acceso a GS</param>
    ''' <param name="AccesoPM">Si el usuario tiene acceso a PM</param>
    ''' <param name="AccesoQA">Si el usuario tiene acceso a QA</param>
    ''' <param name="AccesoSM">Si el usuario tiene acceso a SM</param>
    ''' <returns>las categorÃƒÂ­as</returns>
    ''' <remarks>Llamada desde: cnCategorias.Cargar_Categorias_MensajesUsuario; Tiempo mÃƒÂ¡ximo:0</remarks>
    Public Function CN_Cargar_Categorias_MensajesUsuario(ByVal UsuCod As String, ByVal Idioma As String, ByVal AccesoEP As Boolean, ByVal AccesoGS As Boolean, _
     ByVal AccesoPM As Boolean, ByVal AccesoQA As Boolean, ByVal AccesoSM As Boolean) As DataTable
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_OBTENER_CATEGORIAS_MENSAJES_USU"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@IDI", Idioma)
            cm.Parameters.AddWithValue("@ACCESOEP", AccesoEP)
            cm.Parameters.AddWithValue("@ACCESOGS", AccesoGS)
            cm.Parameters.AddWithValue("@ACCESOPM", AccesoPM)
            cm.Parameters.AddWithValue("@ACCESOQA", AccesoQA)
            cm.Parameters.AddWithValue("@ACCESOSM", AccesoSM)

            da.SelectCommand = cm
            da.Fill(dr)
            Return dr.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
#End Region
#Region "Discrepancias"
    Public Function CN_Obtener_Datos_NuevaDiscrepancia(ByVal Prove As String, ByVal Idioma As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "CN_OBTENER_DATOS_NUEVADISCREPANCIA"
            cm.CommandType = CommandType.StoredProcedure
            cm.Connection = cn
            cm.Parameters.AddWithValue("@PROVE", Prove)
            cm.Parameters.AddWithValue("@IDIOMA", Idioma)
            da.SelectCommand = cm
            da.Fill(dr)

            dr.Tables(0).TableName = "CATEGORIA"
            dr.Tables(1).TableName = "CONTACTOS"
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Sub CN_InsertarDiscrepancia(ByVal Usu As String, _
              ByVal tipo As Integer, ByVal FechaAlta As DateTime, ByVal Titulo As String, ByVal Contenido As String, _
              ByVal Fecha As Nullable(Of DateTime), ByVal Donde As String, ByVal Categoria As Integer, _
              ByVal UON0 As Boolean, ByVal Para_UON As DataTable, ByVal Para_Prove_Tipo As Integer, ByVal Para_Prove_Con As DataTable, _
              ByVal GMN1 As String, ByVal GMN2 As String, ByVal GMN3 As String, ByVal GMN4 As String, ByVal MatQA As Nullable(Of Integer), _
              ByVal ProceCompra_Anyo As Nullable(Of Integer), ByVal ProceCompra_GMN1 As String, ByVal ProceCompra_Codigo As Nullable(Of Integer), _
              ByVal ProceParaResp As Boolean, ByVal ProceParaInvitado As Boolean, _
              ByVal ProceParaCompradores As Boolean, ByVal ProceParaProve As Boolean, _
              ByVal Para_EstrucCompras As DataTable, ByVal Para_EstrucCompras_MatGS As Boolean, _
              ByVal Para_Grupo_EP As Boolean, ByVal Para_Grupo_GS As Boolean, ByVal Para_Grupo_PM As Boolean, _
              ByVal Para_Grupo_QA As Boolean, ByVal Para_Grupo_SM As Boolean, ByVal Para_Grupos_Grupos As DataTable, _
              ByRef Adjuntos As DataTable, ByVal Factura As Integer, ByVal Linea As Integer, _
              Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "")
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter
        cn.Open()
        Dim transaccion As SqlTransaction = cn.BeginTransaction()
        Try
            cm.Connection = cn
            cm.CommandText = "CN_INSERTAR_MENSAJE"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion

            With cm.Parameters
                .AddWithValue("@USU", Usu)
                .AddWithValue("@TIPO", tipo)
                .AddWithValue("@FECALTA", FechaAlta)
                .AddWithValue("@TITULO", Titulo)
                .AddWithValue("@CONTENIDO", Contenido)
                .AddWithValue("@CUANDO", IIf(Fecha.HasValue, Fecha, Nothing))
                .AddWithValue("@DONDE", Donde)
                .AddWithValue("@CATEGORIA", Categoria)
                .AddWithValue("@PROCE_ANYO", IIf(ProceCompra_Anyo.HasValue, ProceCompra_Anyo, Nothing))
                .AddWithValue("@PROCE_GMN1", ProceCompra_GMN1)
                .AddWithValue("@PROCE_COD", IIf(ProceCompra_Codigo.HasValue, ProceCompra_Codigo, Nothing))
                .AddWithValue("@PROCEPARARESP", ProceParaResp)
                .AddWithValue("@PROCEPARAINVITADO", ProceParaInvitado)
                .AddWithValue("@PROCEPARACOMP", ProceParaCompradores)
                .AddWithValue("@PROCEPARAPROVE", ProceParaProve)
                .AddWithValue("@UON0", UON0)
                .AddWithValue("@GMN1", GMN1)
                .AddWithValue("@GMN2", GMN2)
                .AddWithValue("@GMN3", GMN3)
                .AddWithValue("@GMN4", GMN4)
                .AddWithValue("@MATQA", MatQA)
                .AddWithValue("@PROVEPARA", Para_Prove_Tipo)
                .AddWithValue("@COMPPARA", Para_EstrucCompras_MatGS)
                .AddWithValue("@USUEPPARA", Para_Grupo_EP)
                .AddWithValue("@USUGSPARA", Para_Grupo_GS)
                .AddWithValue("@USUPMPARA", Para_Grupo_PM)
                .AddWithValue("@USUQAPARA", Para_Grupo_QA)
                .AddWithValue("@USUSMPARA", Para_Grupo_SM)
                .Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output
            End With
            cm.ExecuteNonQuery()

            Dim Id As Integer = cm.Parameters("@ID").Value

            If Not Para_UON Is Nothing Then
                cm = New SqlCommand("CN_ASOCIAR_MENSAJE_USU", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@MEN", Id)

                    .Add("@UON1", SqlDbType.NVarChar, 20, "UON1")
                    .Add("@UON2", SqlDbType.NVarChar, 20, "UON2")
                    .Add("@UON3", SqlDbType.NVarChar, 20, "UON3")
                    .Add("@DEP", SqlDbType.NVarChar, 20, "DEP")
                    .Add("@USU", SqlDbType.NVarChar, 20, "USU")
                End With

                da.InsertCommand = cm
                da.Update(Para_UON)
            End If
            If Not Para_Prove_Con Is Nothing Then
                cm = New SqlCommand("CN_ASOCIAR_MENSAJE_PROVECON", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@MEN", Id)
                    .Add("@PROVE", SqlDbType.NVarChar, 20, "PROVE")
                    .Add("@CON", SqlDbType.Int, 0, "CON")
                End With

                da.InsertCommand = cm
                da.Update(Para_Prove_Con)
            End If
            If Not Adjuntos Is Nothing Then
                cm = New SqlCommand("CN_ASOCIAR_MENSAJE_ADJUNTOS", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion

                With cm.Parameters
                    .AddWithValue("@MEN", Id)
                    .Add("@ID", SqlDbType.Int, 0, "ID").Direction = ParameterDirection.Output
                    .Add("@NOMBRE", SqlDbType.NVarChar, 200, "NOMBRE")
                    .Add("@GUID", SqlDbType.NVarChar, 50, "GUID")
                    .Add("@SIZE", SqlDbType.Float, 0, "SIZE")
                    .Add("@SIZEUNIT", SqlDbType.NVarChar, 10, "SIZEUNIT")
                    .Add("@TIPO", SqlDbType.TinyInt, 0, "TIPOADJUNTO")
                    .Add("@URL", SqlDbType.NVarChar, -1, "URL")
                End With

                da.InsertCommand = cm
                da.Update(Adjuntos)
            End If

            cm = New SqlCommand("CN_INSERTAR_DISCREPANCIA", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion

            With cm.Parameters
                .AddWithValue("@IDMENSAJE", Id)
                .AddWithValue("@FACTURA", Factura)
                .AddWithValue("@LINEA", Linea)
            End With

            cm.ExecuteNonQuery()

            transaccion.Commit()
        Catch e As Exception
            transaccion.Rollback()
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Obtener lí­neas de factura
    ''' </summary>
    ''' <param name="Idioma">Idioma</param>
    ''' <param name="Factura">ID Factura</param>
    ''' <returns>lí­neas de factura</returns>
    ''' <remarks></remarks>
    ''' <revisado>JVS 22/06/2012</revisado>
    Public Function CN_Obtener_Lineas_Factura(ByVal Idioma As String, ByVal Factura As Integer, ByVal Usu As String, ByVal Linea As Integer) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_OBTENER_LINEAS_FACTURA_DISCRP"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@FACTURA", Factura)
            cm.Parameters.AddWithValue("@USU", Usu)
            If Not Linea = 0 Then cm.Parameters.AddWithValue("@LINEA", Linea)

            da.SelectCommand = cm
            da.Fill(dr)

            Return dr.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Sub CN_Cerrar_Discrepancia(ByVal Factura As Integer, ByVal Linea As Integer, ByVal IdDiscrepancia As Integer)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_CERRAR_DISCREPANCIAS"
            cm.CommandType = CommandType.StoredProcedure
            If Not Factura = 0 Then cm.Parameters.AddWithValue("@FACTURA", Factura)
            If Not Linea = 0 Then cm.Parameters.AddWithValue("@LINEA", Linea)
            If Not IdDiscrepancia = 0 Then cm.Parameters.AddWithValue("@IDDISCREPANCIA", IdDiscrepancia)

            cm.ExecuteNonQuery()
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    Public Function CN_Comprobar_PermisoConsulta_Factura(ByVal Usu As String, ByVal Idioma As String, ByVal IdFactura As Integer) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSPM_FACTURAS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", Usu)
            cm.Parameters.AddWithValue("@IDI", Idioma)
            cm.Parameters.AddWithValue("@IDFACTURA", IdFactura)
            cm.Parameters.AddWithValue("@FAC_RECTIFICATIVA", True)
            cm.Parameters.AddWithValue("@FAC_ORIGINAL", True)
            da.SelectCommand = cm
            da.Fill(dr)

            Return dr.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    Public Function CN_Comprobar_DiscrepanciasCerradas(ByVal IdFactura As Integer, ByVal Linea As Integer) As Integer
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "CN_COMPROBAR_DISCREPANCIASCERRADAS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDFACTURA", IdFactura)
            If Not Linea = 0 Then cm.Parameters.AddWithValue("@LINEA", Linea)
            da.SelectCommand = cm
            da.Fill(dr)

            Return dr.Tables(0).Rows.Count
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
#End Region
#End Region
End Class