﻿Imports System.Configuration
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Exception
Imports System.IO
Imports System.Object
Imports Fullstep.FSNLibrary
Imports Fullstep.FSNLibrary.TiposDeDatos

Partial Public Class Root
#Region "INT. Data Acess Methods"
    ''' <summary>
    ''' Comprueba si hay integraciÃ³n para una determinado erp, en una determinada tabla de integraciÃ³n
    ''' para unos sentidos determinados
    ''' </summary>
    ''' <param name="iErp"></param>
    ''' <param name="iTabla"></param>
    ''' <param name="sentidoIntegracion1"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function HayIntegracionTablaErp(ByVal iErp As String, ByVal iTabla As TablasIntegracion, ByVal sentidoIntegracion1 As SentidoIntegracion, ByVal sentidoIntegracion2 As SentidoIntegracion) As Boolean
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandType = CommandType.Text
            cm.CommandText = "	SELECT ACTIVA " _
                            & " FROM TABLAS_INTEGRACION_ERP TI WITH(NOLOCK)" _
                            & "  WHERE TI.ERP =@ERP AND TABLA=@TABLAENTIDAD AND SENTIDO "

            If sentidoIntegracion2 <> SentidoIntegracion.SinSentido Then
                cm.CommandText &= "IN (" & sentidoIntegracion1 & "," & sentidoIntegracion2 & ")"
            Else
                cm.CommandText &= "=" & sentidoIntegracion1
            End If

            cm.Parameters.AddWithValue("@ERP", iErp)
            cm.Parameters.AddWithValue("@TABLAENTIDAD", iTabla)

            da.SelectCommand = cm
            da.Fill(ds)

            If (ds.Tables(0).Rows.Count = 0) Then
                Return False
            Else
                Return ds.Tables(0).Rows(0)("ACTIVA") = 1
            End If
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Devuelve True si hay integración para un ERP con una empresa concreta y una entidad indicada, del tipo que se indique y en los sentidos concretos.
    ''' Si sólo se quiere indicar un sentido, sentidoIntegracion2 debe se SinSentido
    ''' </summary>
    ''' <param name="iErp">Erp para el que se quiere ver la integración</param>
    ''' <param name="iEmp">Empresa para la que se quiere ver la integración</param>
    ''' <param name="iTabla">Tabla para la que se quiere ver la integración</param>
    ''' <param name="tipoIntegracion">TipoDestinoIntegracion</param>
    ''' <param name="sentidoIntegracion1">Sentido de la integración</param>
    ''' <param name="sentidoIntegracion2">2º sentido de integración. Si no se quiere indicar, debe ponerse como SinSentido (o 0)</param>
    ''' <returns>True si hay integración. False si no.</returns>
    ''' <remarks>
    ''' Función utilizada en FSNDatabaseServer.INT_Root.HayIntegracionSalidaWCF
    ''' Última revisión asg 28/03/2014
    ''' </remarks>
    Public Function HayIntegracionTablaEmp(ByVal iTabla As TablasIntegracion, ByVal tipoIntegracion As TipoDestinoIntegracion, ByVal sentidoIntegracion1 As SentidoIntegracion, ByVal sentidoIntegracion2 As SentidoIntegracion, Optional ByVal iEmp As Integer = 0, Optional ByVal iErp As Integer = 0) As Boolean
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandType = CommandType.Text
            cm.CommandText = "SELECT 1 FROM TABLAS_INTEGRACION_ERP T WITH(NOLOCK) "
            cm.CommandText &= "INNER JOIN ERP_SOCIEDAD ES WITH(NOLOCK) ON ES.ERP=T.ERP "
            cm.CommandText &= "INNER JOIN EMP WITH(NOLOCK) ON EMP.SOCIEDAD=ES.SOCIEDAD "
            cm.CommandText &= "WHERE T.ACTIVA=1 "
            cm.CommandText &= "AND EMP.ERP = 1 "
            cm.CommandText &= "AND T.DESTINO_TIPO = @TIPOINTEGRACION "
            cm.CommandText &= "AND T.TABLA = @TABLAENTIDAD "
            If iEmp <> 0 Then
                cm.CommandText &= "AND EMP.ID= @EMP "
            End If
            If iErp <> 0 Then
                cm.CommandText &= "AND ES.ERP= @ERP "
            End If
            cm.CommandText &= "AND T.SENTIDO "

            If sentidoIntegracion2 <> SentidoIntegracion.SinSentido Then
                cm.CommandText &= "IN (" & sentidoIntegracion1 & "," & sentidoIntegracion2 & ")"
            Else
                cm.CommandText &= "=" & sentidoIntegracion1
            End If

            cm.Parameters.AddWithValue("@TABLAENTIDAD", iTabla)
            cm.Parameters.AddWithValue("@TIPOINTEGRACION", tipoIntegracion)
            If iEmp <> 0 Then
                cm.Parameters.AddWithValue("@EMP", iEmp)
            End If
            If iErp <> 0 Then
                cm.Parameters.AddWithValue("@ERP", iErp)
            End If

            da.SelectCommand = cm
            da.Fill(ds)

            If (ds.Tables(0).Rows.Count = 0) Then
                Return False
            Else
                Return ds.Tables(0).Rows(0)(0) = 1
            End If
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Función que indica si hay integración de salida para una determinada entidad en una determinada empresa
    ''' </summary>
    ''' <param name="iEntidad">Entidad para la que se mira que hay integración</param>
    ''' <param name="iEmpresa">Empresa para la que se mira si hay integración</param>
    ''' <returns>Devuelve True si hay integración activa. False en caso contrario.</returns>
    ''' <remarks>Llamada desde EP_ValidacionesIntegracion.vb</remarks>
    ''' <revision>asg 03/06/2014</revision>
    Public Function HayIntegracionSalidaEmpresa(ByVal iEntidad As Integer, ByVal iEmpresa As Integer) As Boolean
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandType = CommandType.Text
            cm.CommandText = "SELECT 1 FROM TABLAS_INTEGRACION_ERP T WITH(NOLOCK) "
            cm.CommandText &= "INNER JOIN ERP_SOCIEDAD ES WITH(NOLOCK) ON ES.ERP=T.ERP "
            cm.CommandText &= "INNER JOIN EMP WITH(NOLOCK) ON EMP.SOCIEDAD=ES.SOCIEDAD "
            cm.CommandText &= "WHERE T.ACTIVA=1 "
            cm.CommandText &= "AND T.TABLA = @TABLAENTIDAD "
            cm.CommandText &= "AND EMP.ID= @EMP "
            cm.CommandText &= "AND T.SENTIDO IN (" & SentidoIntegracion.Salida & "," & SentidoIntegracion.EntradaYSalida & ")"

            cm.Parameters.AddWithValue("@TABLAENTIDAD", iEntidad)
            cm.Parameters.AddWithValue("@EMP", iEmpresa)

            da.SelectCommand = cm
            da.Fill(ds)

            If (ds.Tables(0).Rows.Count = 0) Then
                Return False
            Else
                Return ds.Tables(0).Rows(0)(0) = 1
            End If
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Devuelve True si hay integración de salida con WCF para una determinada tabla o entidad con una empresa.
    ''' </summary>
    ''' <param name="iEntidad">Id de la tabla o entidad</param>
    ''' <param name="iEmpresa">Id de la empresa</param>
    ''' <returns>True si hay integración, false si no</returns>
    ''' <remarks>
    ''' Función utilizada en FSNServer.App_Classes.EP.EP_ValidacionesIntegracion
    ''' Última revisión asg 28/03/2014
    ''' </remarks>
    Public Function HayIntegracionSalidaWCF(ByVal iEntidad As Integer, Optional ByVal iEmpresa As Integer = 0, Optional iErp As Integer = 0) As Boolean
        Return Me.HayIntegracionTablaEmp(iEntidad, TipoDestinoIntegracion.WCF, SentidoIntegracion.Salida, SentidoIntegracion.EntradaYSalida, iEmpresa, iErp)
    End Function

    Public Function hayIntegracionArticuloErp(ByVal sArticulo As String, ByVal sErp As String) As Boolean
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "FSINT_COMPROBAR_INTEGRACION_ARTICULO_ERP"
            cm.Parameters.AddWithValue("@ARTICULO", sArticulo)
            cm.Parameters.AddWithValue("@ERP", sErp)
            da.SelectCommand = cm
            da.Fill(ds)

            If (ds.Tables(0).Rows.Count = 0) Then
                Return False
            Else
                Return ds.Tables(0).Rows(0)("ESTADO") = 4
            End If
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Dada una organizaciÃ³n de compras obtiene su ERP
    ''' </summary>
    ''' <param name="orgcompras">codigo de la organizaciÃ³n de compras</param>
    ''' <returns></returns>
    ''' <remarks>Llamada desde DBServer , getEmpresaFromOrgCompras</remarks>
    Function getErpFromOrgCompras(ByVal orgcompras As String) As String
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter

        Dim bRes As Boolean
        Try
            bRes = False

            cm.Connection = cn
            cm.CommandText = "select ERP from ERP_ORGCOMPRAS WITH(NOLOCK) where ORGCOMPRAS=@ORGCOMPRAS"
            cm.CommandType = CommandType.Text
            cm.Parameters.AddWithValue("@ORGCOMPRAS", orgcompras)

            da.SelectCommand = cm
            da.Fill(ds)

            If (ds.Tables(0).Rows.Count = 0) Then
                Return 0
            Else
                Return ds.Tables(0).Rows(0)("ERP")
            End If
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Obtiene el cÃ³digo del ERP de una empresa que pertenece a una organizaciÃ³n de compras
    ''' </summary>
    ''' <param name="iempresa"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function getErpFromOrgCompras(ByVal iempresa As Integer) As String
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter

        Dim bRes As Boolean
        Try
            bRes = False

            cm.Connection = cn
            cm.CommandText = "FSGS_OBTENER_ERP_ORGCOMPRAS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@EMPRESA", iempresa)

            da.SelectCommand = cm
            da.Fill(ds)

            If (ds.Tables(0).Rows.Count = 0) Then
                Return 0
            Else
                Return ds.Tables(0).Rows(0)(0)
            End If
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Obtiene el cÃ³digo del erp vinculado a una empresa sin organizaciÃ³n de compras
    ''' </summary>
    ''' <param name="iempresa"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function getErpFromEmpresa(ByVal iempresa As Integer) As Integer
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter

        Dim bRes As Boolean
        Try
            bRes = False

            cm.Connection = cn
            cm.CommandText = "FSGS_OBTENER_ERP_EMPRESA"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@EMPRESA", iempresa)

            da.SelectCommand = cm
            da.Fill(ds)

            If (ds.Tables(0).Rows.Count = 0) Then
                Return 0
            Else
                Return ds.Tables(0).Rows(0)(0)
            End If
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Obtiene el cÃ³dogo del ERP a travÃ©s de su denominaciÃ³n.
    ''' </summary>
    ''' <param name="sDenERP">La denominaciÃ³n del ERP deseado</param>
    ''' <returns>Integer con el cÃ³digo del ERP.</returns>
    ''' <remarks>Llamada desde: Integracion.getErpFromDen.
    ''' Revisado por: aam (04/04/2016).
    ''' Tiempo mÃ¡ximo: menos de 1s.
    ''' </remarks>
    Function getErpFromDen(ByVal sDenERP As String) As Integer
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cm.Connection = cn
            cm.CommandText = "SELECT TOP 10 COD FROM ERP WITH(NOLOCK) WHERE DEN=@DEN"
            cm.CommandType = CommandType.Text
            cm.Parameters.AddWithValue("@DEN", sDenERP)

            da.SelectCommand = cm
            da.Fill(ds)

            If (ds.Tables(0).Rows.Count = 0) Then
                Return 0
            Else
                Return ds.Tables(0).Rows(0)(0)
            End If
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Relanza integraciÃ³n de determinada entidad, identificada por su idlog y la asocia a un usuario
    ''' determinado
    ''' </summary>
    ''' <param name="idlog">id del log general del registro a relanzar</param>
    ''' <param name="user">usuario que relanza intergraciÃ³n</param>
    ''' <returns>array de dos elementos (idLogGral insertado, el id de log de la entidad )</returns>
    ''' <remarks></remarks>
    Public Function relanzarIntegracion(ByVal idlog As Integer, ByVal user As String) As Integer()
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim res(1) As Integer
        Dim bRes As Boolean
        Try
            bRes = False

            cm.Connection = cn
            cm.CommandText = "FSINT_RELANZAR_INTEGRACION"
            cm.CommandType = CommandType.StoredProcedure

            cm.Parameters.AddWithValue("@IDLOGGRAL", idlog)
            cm.Parameters.AddWithValue("@USUARIO", user)


            da.SelectCommand = cm
            da.Fill(ds)

            If (ds.Tables(0).Rows.Count = 0) Then
                res(0) = 0
                res(1) = 0
            Else
                res(0) = ds.Tables(0).Rows(0)(0)
                res(1) = ds.Tables(0).Rows(0)(1)
            End If
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
        Return res
    End Function
#Region "VisorIntegracion"
    ''' <summary>
    ''' Devuelve el codigo y la denominacion de los ERPs existentes 
    ''' </summary>
    ''' <param name="lPyme">Si es una instalacion en modo PYME, contiene el ID de la pyme del usuario (0 en caso contrario)</param>
    ''' <returns></returns>
    ''' <remarks>Llamada desde VisorInt.vb Duracion: 0.1</remarks>
    ''' Revisado por ngo 21/06/2012
    Public Function INT_DevuelveERPs(ByVal lPyme As Integer) As DataSet

        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Dim Param As New SqlParameter

        Try
            cn.Open()
            cm.CommandText = "SELECT COD, DEN FROM ERP WITH(NOLOCK) "
            If lPyme <> 0 Then
                Param = cm.Parameters.AddWithValue("@PYME", lPyme)
                cm.CommandText = cm.CommandText = +" WHERE COD IN (SELECT DISTINCT ERP_SOCIEDAD.ERP  FROM UON1 WITH(NOLOCK) " + _
                 " INNER JOIN EMP WITH(NOLOCK) ON EMP.ID=UON1.EMPRESA  AND EMP.ERP=1 AND EMP.SOCIEDAD IS NOT NULL" + _
                 " INNER JOIN ERP_SOCIEDAD WITH(NOLOCK) ON ERP_SOCIEDAD.SOCIEDAD=EMP.SOCIEDAD " + _
                 " UNION SELECT DISTINCT ERP_SOCIEDAD.ERP  FROM UON1 WITH(NOLOCK) " + _
                 " INNER JOIN EMP WITH(NOLOCK) ON EMP.ID=UON1.EMPRESA  AND EMP.ERP=1 AND EMP.SOCIEDAD IS NOT NULL " + _
                 " INNER JOIN ERP_SOCIEDAD WITH(NOLOCK) ON ERP_SOCIEDAD.SOCIEDAD=EMP.SOCIEDAD  " + _
                 " WHERE(UON1.PYME = @PYME And UON1.EMPRESA Is Not NULL)" + _
                 " UNION SELECT DISTINCT ERP_SOCIEDAD.ERP FROM UON2 WITH(NOLOCK) INNER JOIN UON1 WITH(NOLOCK) ON UON1.COD=UON2.UON1 " + _
                 " INNER JOIN EMP WITH(NOLOCK) ON EMP.ID=UON2.EMPRESA  AND EMP.ERP=1 AND EMP.SOCIEDAD IS NOT NULL" + _
                 " INNER JOIN ERP_SOCIEDAD WITH(NOLOCK) ON ERP_SOCIEDAD.SOCIEDAD=EMP.SOCIEDAD " + _
                 " WHERE(UON1.PYME = @PYME And UON1.EMPRESA Is NULL And UON2.EMPRESA Is Not NULL) " + _
                 " UNION SELECT DISTINCT ERP_SOCIEDAD.ERP FROM UON3 WITH(NOLOCK) INNER JOIN UON1 WITH(NOLOCK) ON UON1.COD=UON3.UON1 " + _
                 " INNER JOIN EMP WITH(NOLOCK) ON EMP.ID=UON3.EMPRESA  AND EMP.ERP=1 AND EMP.SOCIEDAD IS NOT NULL " + _
                 " INNER JOIN ERP_SOCIEDAD WITH(NOLOCK) ON ERP_SOCIEDAD.SOCIEDAD=EMP.SOCIEDAD " + _
                 " WHERE(UON1.PYME = @PYME And UON1.EMPRESA Is NULL And UON3.EMPRESA Is Not NULL) "
            End If
            cm.Connection = cn
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Devuelve las entidades con integracion activada para todos los ERPs existentes (o para el ERP que se le pase como parametro)
    ''' NO devuelve las siguientes entidades:
    '''  TablasExternas (esta entidad no se va a mostrar en el visor de integracion)
    '''  Asignacion de proveedores (de momento no se muestra)
    '''  Recepcion de ofertas (de momento no se muestra)
    ''' </summary>
    ''' <param name="CodERP">OPCIONAL. Codigo del ERP para el que se buscan los datos</param>
    ''' <returns></returns>
    ''' <remarks>Llamada desde VisorInt.vb Duracion: 0.1</remarks>
    ''' ''' Revisado por ngo 21/06/2012
    Public Function INT_DevuelveEntidades(Optional ByVal CodErp As String = "") As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Dim Param As New SqlParameter
        Try

            cn.Open()
            cm.CommandText = "SELECT DISTINCT TABLA FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE ACTIVA=1 AND TABLA NOT IN (" _
             + CStr(TablasIntegracion.TablasExternas) + "," + CStr(TablasIntegracion.AsignacionProv) + "," + CStr(TablasIntegracion.RecepcionOFE) + ")"
            If CodErp <> "" Then
                Param = cm.Parameters.AddWithValue("@ERP", CodErp)
                cm.CommandText = cm.CommandText + " AND ERP=@ERP"
            End If
            cm.Connection = cn
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Devuelve los procesos existentes en las tablas de log
    ''' </summary>
    ''' <param name="ERP">Codigo del ERP para el que se buscan los datos</param>
    ''' <param name="iEntidad">Entidad para la que se buscan los datos: Adjudicaciones o Procesos</param>
    ''' <returns>Dataset</returns>
    ''' <remarks>Llamada desde VisorInt.vb. Duracion: 0.1</remarks>
    ''' ''' Revisado por ngo 21/06/2012
    Public Function INT_DevuelveProcesos(ByVal ERP As Integer, ByVal iEntidad As Integer, ByVal Anyo As Short, ByVal Gmn1 As String, ByVal CodProce As Integer) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Dim Param As New SqlParameter
        Try
            cn.Open()
            If iEntidad = TablasIntegracion.Proceso Then
                cm.CommandText = "SELECT DISTINCT ANYO,GMN1,COD FROM LOG_PROCE WITH(NOLOCK) WHERE ORIGEN<>1 AND ERP=@ERP"
                Param = cm.Parameters.AddWithValue("@ERP", ERP)
            ElseIf iEntidad = TablasIntegracion.Adj Then
                cm.CommandText = "SELECT DISTINCT ANYO,GMN1_PROCE AS GMN1,PROCE AS COD FROM LOG_ITEM_ADJ WITH(NOLOCK) WHERE ORIGEN<>1 AND ERP=@ERP"
                Param = cm.Parameters.AddWithValue("@ERP", ERP)
            Else
                Return Nothing
            End If
            If Anyo <> 0 Then
                cm.CommandText = cm.CommandText & " AND ANYO=@ANYO"
                Param = cm.Parameters.AddWithValue("@ANYO", Anyo)
            End If
            If Gmn1 <> "" Then
                If iEntidad = TablasIntegracion.Proceso Then
                    cm.CommandText = cm.CommandText & " AND GMN1=@GMN1"
                ElseIf iEntidad = TablasIntegracion.Adj Then
                    cm.CommandText = cm.CommandText & " AND GMN1_PROCE=@GMN1"
                End If
                Param = cm.Parameters.AddWithValue("@GMN1", Gmn1)
            End If
            If CodProce <> 0 Then
                If iEntidad = TablasIntegracion.Proceso Then
                    cm.CommandText = cm.CommandText & " AND COD=@COD"
                ElseIf iEntidad = TablasIntegracion.Adj Then
                    cm.CommandText = cm.CommandText & " AND PROCE=@COD"
                End If
                Param = cm.Parameters.AddWithValue("@COD", CodProce)
            End If

            cm.Connection = cn
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Devuelve los aÃ±os existentes con pedidos
    ''' </summary>
    ''' <returns>Dataset</returns>
    ''' <remarks>Llamada desde VisorInt.vb</remarks>
    Public Function INT_DevuelveAnyo() As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Dim sConsulta As String = ""
        Try

            cn.Open()

            cm.CommandText = "SELECT DISTINCT OE.ANYO FROM ORDEN_ENTREGA OE WITH(NOLOCK)"
            cm.Connection = cn
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Llama al stored que devuelve los registros de las tablas de LOG de integracion con los datos a mostrar en el visor.
    ''' </summary>
    ''' <param name="sERP">Codigo del ERP</param>
    ''' <param name="iEntidad">Identificador de la entidad</param>
    ''' <param name="iAnioProce">Anyo del proceso</param>
    ''' <param name="sGMN">GMN1 del proceso</param>
    ''' <param name="iCodProce">Codigo del proceso</param>
    ''' <param name="sCod">Código a buscar (en función de la entidad será el código de artículo, de proveedor, número de factura...)</param>
    ''' <param name="sCodErp">Codigo ERP a buscar (en función de la entidad será el código ERP del artículo, proveedor, pedido...)</param>
    ''' <param name="sAlbaranErp">Codigo ERP del albarán</param>
    ''' <param name="iAnioPedido">Anyo del pedido</param>
    ''' <param name="iNumPedido">Numero de pedido</param>
    ''' <param name="iNumOrden">Numero de la orden de entrega</param>
    ''' <param name="iIDLog">Identificador de la tabla de LOG (LOG_GRAL.ID)</param>
    ''' <param name="sAccion">Codigo de la accion (I=alta, U=modificacion, R=modificacion, D=borrado, C=Cambio codigo, 4=reubicacion)</param>
    ''' <param name="iEstado">Estado de integracion (0=pte tratar, 1=Enviado, 3=Error, 4=OK)</param>
    ''' <param name="iSentido">Sentido de integracion (1=salida, 2=entrada, 3=entrada/salida)</param>
    ''' <param name="dFechaDesde">Fecha desde</param>
    ''' <param name="dFechaHasta">Fecha hasta</param>
    ''' <param name="sDateFormat">Formato fecha</param>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <returns>DataSet con los datos a mostrar en el visor de integracion</returns>
    ''' <remarks>Llamada desde: VisorInt.cb; Duracion - 2 sg </remarks>
    ''' Revisado por ngo
    Public Function INT_CargarVisor(ByVal sERP As String, ByVal iEntidad As Integer, _
            ByVal iAnioProce As Integer, ByVal sGMN As String, ByVal iCodProce As Integer, _
            ByVal sCod As String, ByVal sCodErp As String, ByVal sAlbaranErp As String, _
            ByVal iAnioPedido As Integer, ByVal iNumPedido As Integer, ByVal iNumOrden As Integer, _
            ByVal iIDLog As Integer, ByVal sAccion As String, ByVal iEstado As Integer, ByVal iSentido As Integer, _
            ByVal dFechaDesde As Date, ByVal dFechaHasta As Date, ByVal sDateFormat As String, ByVal sIdioma As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSINT_VISOR"
            cm.CommandType = CommandType.StoredProcedure

            If sERP <> "" Then cm.Parameters.AddWithValue("@ERP", sERP)
            If iEntidad >= 1 Then cm.Parameters.AddWithValue("@TABLA", iEntidad)
            If sCod <> "" Then cm.Parameters.AddWithValue("@COD_FS", sCod)
            If sCodErp <> "" Then cm.Parameters.AddWithValue("@COD_ERP", sCodErp)
            If sAlbaranErp <> "" Then cm.Parameters.AddWithValue("@RECEP_ERP", sAlbaranErp)

            If iAnioProce <> -1 Then cm.Parameters.AddWithValue("@ANYO_PROCE", iAnioProce)
            If sGMN <> "" Then cm.Parameters.AddWithValue("@GMN1_PROCE", sGMN)
            If iCodProce <> -1 Then cm.Parameters.AddWithValue("@COD_PROCE", iCodProce)

            If iAnioPedido <> -1 Then cm.Parameters.AddWithValue("@ANYO_PEDIDO", iAnioPedido)
            If iNumPedido <> -1 Then cm.Parameters.AddWithValue("@NUM_PEDIDO", iNumPedido)
            If iNumOrden <> -1 Then cm.Parameters.AddWithValue("@NUM_ORDEN", iNumOrden)

            If iIDLog <> -1 Then cm.Parameters.AddWithValue("@ID_LOG", iIDLog)
            If sAccion <> "" Then cm.Parameters.AddWithValue("@ACCION", sAccion)
            If iEstado <> -1 Then cm.Parameters.AddWithValue("@ESTADO", iEstado)
            If iSentido <> -1 Then cm.Parameters.AddWithValue("@SENTIDO", iSentido)

            cm.Parameters.AddWithValue("@FECHA_DESDE", IIf(dFechaDesde = Date.MinValue, Nothing, dFechaDesde))
            cm.Parameters.AddWithValue("@FECHA_HASTA", IIf(dFechaHasta = Date.MinValue, Nothing, dFechaHasta))
            cm.Parameters.AddWithValue("@FORMATOFECHA", IIf(sDateFormat = "", Nothing, sDateFormat))

            cm.Parameters.AddWithValue("@IDIOMA", sIdioma)

            Dim da As New SqlDataAdapter
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Close()
            cn.Dispose()
            cm.Dispose()
            dr.Dispose()
        End Try

    End Function
    ''' <summary>
    ''' Devuelve el mensaje con el error de integracion
    ''' </summary>
    ''' <param name="IdLogGral" > identificador de log_gral para el que se ha producido un error</param>
    ''' <returns>Dataset con el error</returns>
    ''' <remarks>Llamada desde VisorInt.vb Duracion 0.1</remarks>
    ''' Revisado por ngo 
    Public Function INT_DevuelveErrorIntegracion(ByVal IdLogGral As Long) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.CommandText = "SELECT TEXTO,ARG1,ARG2 FROM LOG_GRAL WITH(NOLOCK) WHERE ID=@ID"
            cm.Connection = cn
            cm.CommandType = CommandType.Text
            cm.Parameters.AddWithValue("@ID", IdLogGral)
            da.SelectCommand = cm
            da.Fill(dr)

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Devuelve la longitud de los campos GMN1, GMN2, GMN3 y GMN4.
    ''' </summary>
    ''' <returns>Dataset con la longitud de los campos GMN1, GMN2, GMN3 y GMN4.</returns>
    ''' <remarks>Llamada desde Duracion 0.1</remarks>
    ''' Revisado por aam 
    Public Function INT_DevuelveLongitudGMN() As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "SELECT LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE IN ('GMN1', 'GMN2', 'GMN3', 'GMN4')"
            cm.Connection = cn
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
#End Region
#Region "Monitorizacion Servicio INT"
    ''' <summary>
    ''' Llama al stored que comprueba si se esta ejecutando correctamente el servicio de integracion.
    ''' 
    ''' Cada vez que se ejecuta el servicio de integracion, se inserta la fecha de inicio. Cuando termina la ejecucion, se inserta la fecha de fin de ejecucion.
    ''' Esto se hace tanto para la importacion de datos como para la exportacion.
    ''' Se considera que ha habido una parada o problema en el servicio de integracion si:
    ''' - No se esta lanzando la importacion de datos (Ha pasado mas del periodo de importacion desde la ultima fecha de inicio)
    ''' - No se esta lanzando la exportacion de datos (Ha pasado mas del periodo de exportacion desde la ultima fecha de inicio)
    ''' - No se graba la fecha de fin de importacion o exportacion (error inesperado en la ejecucion): 
    '''       Si desde el inicio de ejecucion ha pasado mas del tiempo habitual de procesamiento, y no hay fecha de fin de ejecucion, se considerará que ha habido un error.
    ''' </summary>
    ''' <param name="TiempoImp">Periodicidad importacion + margen de tiempo. Si la suma entre la fecha de inicio y este tiempo es mayor que la fecha y hora actual, se considera que ha habido una parada en el servicio de integracion</param>       
    ''' <param name="TiempoExp">Periodicidad exportacion + margen de tiempo. Si la suma entre la fecha de inicio y este tiempo es mayor que la fecha y hora actual, se considera que ha habido una parada en el servicio de integracion</param>       
    ''' <param name="TiempoEjecucion">Tiempo maximo de ejecucion de la integracion. Periodicidad exportacion + margen de tiempo. Si la suma entre la fecha de inicio y este tiempo es mayor que la fecha y hora actual, se considera que ha habido una parada en el servicio de integracion</param>       
    ''' <remarks>Llamada desde=EjecucionComprobarIntegracion ; Tiempo maximo=0,1seg.</remarks>
    ''' Revisado por ngo 21/06/2012
    Public Function INT_CompruebaParadaServicioIntegracion(ByVal TiempoImp As Double, ByVal TiempoExp As Double, ByVal TiempoEjecucion As Double, ByRef Fecha As Date) As Boolean
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim lError As Integer
        Try
            INT_CompruebaParadaServicioIntegracion = False

            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSINT_DETECTA_PARADA_SERVICIO "
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@TIEMPO_IMP", TiempoImp)
            cm.Parameters.AddWithValue("@TIEMPO_EXP", TiempoExp)
            cm.Parameters.AddWithValue("@TIEMPO_EJECUCION", TiempoEjecucion)
            cm.Parameters.Add("@ERROR", SqlDbType.TinyInt).Direction = ParameterDirection.Output
            cm.Parameters.Add("@FECINI", SqlDbType.DateTime).Direction = ParameterDirection.Output
            cm.ExecuteNonQuery()
            lError = cm.Parameters("@ERROR").Value
            Fecha = cm.Parameters("@FECINI").Value
            If lError = 1 Then
                'Hay errores: Se envía un email a los notificados
                INT_CompruebaParadaServicioIntegracion = True
            End If
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Obtiene un listado de los usuarios a los que se notifica que se ha producido una parada en el servicio de integracion
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Llamada desde PMNotificador.Notificar.vb --> NotificacionINTParadaServicioIntegracion. Tiempo maximo: 0 sg </remarks>
    ''' Revisado por ngo 21/06/2012
    Public Function Notificador_INTServicio_DevolverUsuariosNotificar(Optional ByVal iErp As Integer = 0) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSINT_NOTIFICAR_PARADASERVICIO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@ERP", iErp)
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Consulta la tabla de chequeo de integracion: obtiene los dos ultimos registros existentes (para importacion y para exportacion)
    ''' y, en caso de que uno de ellos esté en parada, devuelve la fecha de parada 
    ''' </summary>
    ''' <returns>Dataset con el indicador de parada, la fecha, y el indicador de exportar/importar para los registros mas recientes</returns>
    ''' <remarks>Llamada desde VisorInt.vb (y a su vez, desde ManuMaster.vb). Duracion 0.1</remarks>
    ''' Revisado por ngo 20/06/2012
    Public Function INT_HayParadaServicioInt() As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter
        Dim dr As New DataSet

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SELECT EXPORTAR,PARADA,FECHA FROM " &
             " (SELECT TOP 1 EXPORTAR,PARADA, CASE WHEN PARADA=0 THEN NULL ELSE ISNULL(FECHA_FIN,FECHA_INI ) END FECHA FROM INT_CHEQUEO WITH(NOLOCK) WHERE EXPORTAR=0 AND TIPO=0 ORDER BY ID DESC) AS A " &
             " UNION SELECT * FROM (SELECT TOP 1 EXPORTAR,PARADA, CASE WHEN PARADA=0 THEN NULL ELSE ISNULL(FECHA_FIN,FECHA_INI ) END FECHA FROM INT_CHEQUEO WITH(NOLOCK) WHERE EXPORTAR=1 AND TIPO=0 ORDER BY ID DESC) AS B "
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Consulta la tabla de chequeo de integracion para ver si ha habido alguna parada, en cuyo caso debe habilitar el visor del histórico de paradas
    ''' </summary>
    ''' <returns>Dataset con el indicador de parada</returns>
    ''' <remarks>Llamada desde VisorInt.vb (y a su vez, desde ManuMaster.vb). Duracion 0.1</remarks>
    ''' Revisado por ngo 20/06/2012
    Public Function INT_MostrarHistoricoParadasINT() As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter
        Dim dr As New DataSet

        Try
            ''Solo se van a mostrar las paradas de Tipo=0 que serían las correspondientes a las paradas de integración, las de Tipo = 1 quedan excluidas porque se trata de alertas

            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SELECT ID, FECHA_INI, FECHA_FIN, EXPORTAR, PARADA, MOTIVO "
            cm.CommandText = cm.CommandText & "FROM INT_CHEQUEO WITH(NOLOCK) WHERE PARADA=1 AND TIPO=0 "
            cm.CommandText = cm.CommandText & "ORDER BY FECHA_INI DESC"
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Se comprueba se ha detectado un tiempo de espera excesivo en la generacion de ficheros y/o el procesamienteo de acuses
    ''' </summary>
    ''' <param name="iErp">id del Erp</param>       
    ''' <param name="iTMonitorizacion">Timepo medio referencia para la monitorización de la integración</param> 
    ''' <param name="iTEsperaXML">Tiempo medio referencia para la generación de xmls</param> 
    ''' <param name="iTEsperaAcuse">Tiempo medio referencia para el procesado de acuses</param> 
    ''' <param name="lError">Identificador del error producido</param> 
    ''' <param name="lNumRegXML">Num de xmls pendientes de generar</param> 
    ''' <param name="lNumRegAcuse">Num de registros sin acuse</param> 
    ''' <param name="lTMedioXML">Tiempo medio de generacion de los xml</param> 
    ''' <param name="lTMedioAcuse">Tiempo medio de procesado de acuses</param> 
    ''' <param name="lMonitAcuse">Tiempo de monitorización del procesado de acuses</param> 
    ''' <param name="lMonitXML">Tiempo de monitorización de la generación de xmls</param> 
    ''' <remarks>Llamada desde VisorInt.vb (y a su vez, desde ManuMaster.vb). Duracion 0.1</remarks>
    ''' Revisado por ijc 20/12/2016

    Public Sub INT_ComprobarAlertaTiempoExcedido(ByVal iErp As Integer, ByVal iTMonitorizacion As Integer, ByVal iTEsperaXML As Integer, ByVal iTEsperaAcuse As Integer,
                                                 ByRef lError As Integer, ByRef lNumRegXML As Integer, ByRef lNumRegAcuse As Integer, ByRef lTMedioXML As Double, ByRef lTMedioAcuse As Double,
                                                 ByRef lMonitAcuse As Double, ByRef lMonitXML As Double)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSINT_DETECTA_TIEMPO_MAX_EXCEDIDO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@ERP", iErp)
            cm.Parameters.AddWithValue("@T_MONITORIZACION", iTMonitorizacion)
            cm.Parameters.AddWithValue("@T_ESPERA", iTEsperaXML)
            cm.Parameters.AddWithValue("@T_ESPERA_ACUSE", iTEsperaAcuse)
            cm.Parameters.Add("@ERROR", SqlDbType.TinyInt).Direction = ParameterDirection.Output
            cm.Parameters.Add("@NUM_REG_XML", SqlDbType.Int).Direction = ParameterDirection.Output
            cm.Parameters.Add("@NUM_REG_ACUSE", SqlDbType.Int).Direction = ParameterDirection.Output
            cm.Parameters.Add("@T_MED_ESP_XML", SqlDbType.Float).Direction = ParameterDirection.Output
            cm.Parameters.Add("@T_MED_ESP_ACUSE", SqlDbType.Float).Direction = ParameterDirection.Output
            cm.Parameters.Add("@T_MONIT_ACUSE", SqlDbType.Float).Direction = ParameterDirection.Output
            cm.Parameters.Add("@T_MONIT_XML", SqlDbType.Float).Direction = ParameterDirection.Output
            cm.ExecuteNonQuery()
            lError = cm.Parameters("@ERROR").Value
            lNumRegXML = cm.Parameters("@NUM_REG_XML").Value
            lNumRegAcuse = cm.Parameters("@NUM_REG_ACUSE").Value
            lTMedioXML = cm.Parameters("@T_MED_ESP_XML").Value
            lTMedioAcuse = cm.Parameters("@T_MED_ESP_ACUSE").Value
            lMonitAcuse = cm.Parameters("@T_MONIT_ACUSE").Value
            lMonitXML = cm.Parameters("@T_MONIT_XML").Value

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub

    ''' <summary>
    ''' Se obtiene el tiempo máximo de espera en la generacion de ficheros y/o el procesamienteo de acuses
    ''' </summary>
    ''' <remarks>Llamada desde VisorInt.vb (y a su vez, desde ManuMaster.vb). Duracion 0.1</remarks>
    ''' Revisado por ijc 20/12/2016

    Public Function INT_ObtenerTiemposMaxEspera() As DataSet

        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter
        Dim dr As New DataSet

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SELECT T_ESPERA, T_ESPERA_ACUSE, T_MONITORIZACION, ERP FROM PARGEN_INTEGRACION WITH(NOLOCK)"
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Se comprueba si ha habido una parada de integracion previa y el motivo
    ''' </summary>
    ''' <param name="FechaParada">Fecha de la parada</param> 
    ''' <param name="TRecordAlerta">Tiempo de espera para eviar el mail recordatorio de alerta por tiempos excedidos</param> 
    ''' <param name="fRecordAlerta">Frecuencia con la que se produce el recordatorio de alerta</param> 
    ''' <param name="bEnviarRecordatorio">TRUE si hay que enviar un recordatorio de que la integracion sigue parada y FALSE en el caso contrario</param> 
    ''' <param name="bParadaSolucionada">TRUE si la parada y ha sido solventada y FALSE en el caso contrario</param> 
    ''' <remarks>Llamada desde VisorInt.vb (y a su vez, desde ManuMaster.vb). Duracion 0.1</remarks>
    ''' Revisado por ijc 20/12/2016

    Public Sub INT_ComprobarParadaPrevia(ByVal FechaParada As DateTime, ByVal TRecordAlerta As Double, ByVal fRecordAlerta As String,
                                              ByRef bEnviarRecordatorio As Boolean, ByRef bParadaSolucionada As Boolean)

        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr, dr2 As New DataSet
        Dim da As New SqlDataAdapter

        Try
            bParadaSolucionada = False

            cn.Open()
            cm.Connection = cn

            cm.CommandText = "SELECT EXPORTAR, FECHA_ACT FROM INT_CHEQUEO WITH(NOLOCK) WHERE PARADA=1 AND TIPO = 0 AND FECHA_FIN IS NULL"
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)

            If dr.Tables(0).Rows.Count > 0 Then

                For Each filas As DataRow In dr.Tables(0).Rows

                    FechaParada = filas("FECHA_ACT")

                    cm.CommandText = "SELECT FECHA_INI,FECHA_FIN,TIPO,EXPORTAR FROM INT_CHEQUEO WITH(NOLOCK) WHERE PARADA=0 AND TIPO = 0 AND EXPORTAR =" & filas("EXPORTAR").ToString()

                    cm.CommandType = CommandType.Text
                    da.SelectCommand = cm
                    da.Fill(dr2)

                    If dr2.Tables(0).Rows.Count < 1 Then
                        '' Siguen paradas la integracion o la exportacion

                        If DateDiff(fRecordAlerta, FechaParada, Now) > TRecordAlerta Then

                            cm.CommandText = "UPDATE INT_CHEQUEO SET FECHA_ACT = GETDATE() WHERE FECHA_FIN Is NULL AND TIPO = 0 AND PARADA=1 "
                            cm.CommandType = CommandType.Text
                            cm.ExecuteNonQuery()

                            '' Se va a enviar un mail recordatorio
                            bEnviarRecordatorio = True

                        End If
                    Else
                        '' Ya se ha solventado la parada
                        bParadaSolucionada = True

                        cm.CommandText = "UPDATE INT_CHEQUEO SET FECHA_FIN = GETDATE() WHERE FECHA_FIN Is NULL AND TIPO = 0 AND PARADA=1 AND EXPORTAR =" & filas("EXPORTAR").ToString()
                        cm.CommandType = CommandType.Text
                        cm.ExecuteNonQuery()

                    End If
                Next

            End If

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try



    End Sub

    ''' <summary>
    ''' Se actualiza la tabla INT_CHEQUEO en función de si ha habido errores por tiempo excedido en la generación de ficheros y/o procesado de acuses
    ''' </summary>
    ''' <param name="lError">Identificador del error</param> 
    ''' <param name="iErp">Id del ERP</param> 
    ''' <param name="fRecordAlerta">Frecuencia con la que se produce el recordatorio de alerta</param> 
    ''' <param name="bEnviarRecordatorio">TRUE si hay que enviar un recordatorio de que la integracion sigue parada y FALSE en el caso contrario</param> 
    ''' <param name="TRecordAlerta">Tiempo de espera para eviar el mail recordatorio de alerta por tiempos excedidos</param>  
    ''' <param name="dFechaIniXml">Fecha de inicio de la alerta de tiempo excedido en la generación de xml</param> 
    ''' <param name="dFechaIniAcuse">Fecha de inicio de la alerta de tiempo excedido en el procesado acuses</param> 
    ''' <remarks>Llamada desde VisorInt.vb (y a su vez, desde ManuMaster.vb). Duracion 0.1</remarks>
    ''' Revisado por ijc 20/12/2016

    Public Function INT_ComprobarRegistroTiempoExcedido(ByVal lError As Integer, ByVal iErp As Integer, ByRef bEnviarRecordatorio As Boolean, ByVal TRecordAlerta As Double, ByVal FRecordAlerta As String,
                                                        ByRef dFechaIniXml As String, ByRef dFechaIniAcuse As String) As DataSet

        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Dim iRegMod As Integer

        Try

            cn.Open()
            cm.Connection = cn

            If lError = 0 Then
                cm.CommandText = "UPDATE INT_CHEQUEO Set FECHA_FIN = GETDATE() WHERE FECHA_FIN Is NULL And TIPO In (1,2) And ERP=" & iErp
                cm.CommandType = CommandType.Text
                iRegMod = cm.ExecuteNonQuery()

                If iRegMod = 0 Then
                    INT_ComprobarRegistroTiempoExcedido = Nothing
                Else
                    cm.CommandText = "Select FECHA_INI,FECHA_FIN,TIPO,ERP FROM INT_CHEQUEO With(NOLOCK) WHERE FECHA_FIN Is Not NULL And PARADA=1 "
                    cm.CommandText = cm.CommandText & " And TIPO In (1,2) And ERP=" & iErp
                    cm.Connection = cn
                    cm.CommandType = CommandType.Text
                    da.SelectCommand = cm
                    da.Fill(dr)

                    INT_ComprobarRegistroTiempoExcedido = dr

                End If

            Else

                cm.CommandText = "Select COALESCE(FECHA_ACT,FECHA_INI) As FECHA_INI, TIPO FROM INT_CHEQUEO With(NOLOCK) WHERE FECHA_FIN Is NULL And PARADA=1 "
                cm.CommandText = cm.CommandText & " And TIPO In (1,2) And ERP=" & iErp
                cm.Connection = cn
                cm.CommandType = CommandType.Text
                da.SelectCommand = cm
                da.Fill(dr)

                For Each filas As DataRow In dr.Tables(0).Rows
                    Select Case CInt(filas("TIPO").ToString())
                        Case 1
                            dFechaIniXml = filas("FECHA_INI").ToString()
                        Case 2
                            dFechaIniAcuse = filas("FECHA_INI").ToString()
                    End Select
                Next

                If lError = 2 Or lError = 4 Then ''Tipo 2: Error tiempo espera acuse excedido / Tipo 4: Error tiempo espera acuse excedido y Error tiempo espera integracion excedido
                    If dFechaIniAcuse = "" Then
                        cm.CommandText = "INSERT INTO INT_CHEQUEO(FECHA_INI,FECHA_ACT,EXPORTAR,PARADA,TIPO,ERP) VALUES (GETDATE(),GETDATE(),1,1,2," & iErp & ")"
                        cm.CommandType = CommandType.Text
                        cm.ExecuteNonQuery()
                        bEnviarRecordatorio = True
                    Else

                        If DateDiff(FRecordAlerta, CDate(dFechaIniAcuse), Now) > TRecordAlerta Then

                            cm.CommandText = "UPDATE INT_CHEQUEO Set FECHA_ACT = GETDATE() WHERE FECHA_FIN Is NULL And TIPO = 2 And ERP=" & iErp
                            cm.CommandType = CommandType.Text
                            cm.ExecuteNonQuery()

                            '' Se va a enviar un mail recordatorio
                            bEnviarRecordatorio = True
                        End If
                    End If
                End If

                If lError = 3 Or lError = 4 Then ''Tipo 1: Error tiempo espera integracion excedido / Tipo 4: Error tiempo espera acuse excedido y Error tiempo espera integracion excedido
                    If dFechaIniXml = "" Then
                        cm.CommandText = "INSERT INTO INT_CHEQUEO(FECHA_INI,FECHA_ACT,EXPORTAR,PARADA,TIPO,ERP) VALUES (GETDATE(),GETDATE(),1,1,1," & iErp & ")"
                        cm.CommandType = CommandType.Text
                        cm.ExecuteNonQuery()
                        bEnviarRecordatorio = True
                    Else

                        If DateDiff(FRecordAlerta, CDate(dFechaIniXml), Now) > TRecordAlerta Then

                            cm.CommandText = "UPDATE INT_CHEQUEO Set FECHA_ACT = GETDATE() WHERE FECHA_FIN Is NULL And TIPO = 1 And ERP=" & iErp
                            cm.CommandType = CommandType.Text
                            cm.ExecuteNonQuery()

                            '' Se va a enviar un mail recordatorio
                            bEnviarRecordatorio = True
                        End If
                    End If
                End If

                INT_ComprobarRegistroTiempoExcedido = Nothing

            End If


        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Se obtienen el número de registros afectados por las alertas de tiempos excesivos en la generación de ficheros y procesado de acuses 
    ''' una vez que se ha resuelto la alerta
    ''' </summary>
    ''' <param name="dsFechas">Dataset con la fechas de las alertas y paradas de la integración</param> 
    ''' <param name="iNumTotalXML">Num total de registros afectados en la alerta por tiempo excedido en la generación de xml</param> 
    ''' <param name="iNumTotalAcuse">Num total de registros afectados en la alerta por tiempo excedido en el procesado de acuses</param> 
    ''' <remarks>Llamada desde VisorInt.vb (y a su vez, desde ManuMaster.vb). Duracion 0.1</remarks>
    ''' Revisado por ijc 20/12/2016

    Public Sub INT_ObtenerNumRegistros(ByVal dsFechas As DataSet, ByRef iNumTotalXML As Integer, ByRef iNumTotalAcuse As Integer)

        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn


            For Each filas As DataRow In dsFechas.Tables(0).Rows
                Select Case CInt(filas("TIPO").ToString())
                    Case 1
                        cm.CommandText = "Select COUNT(*) REGISTROS_XML FROM LOG_GRAL L With(NOLOCK) " &
                            " inner Join tablas_integracion_erp t With(nolock) On t.erp=l.erp And t.tabla=l.tabla And activa=1 And t.sentido In (1,3) " &
                            " Left Join LOG_ORDEN_ENTREGA LO With(NOLOCK) On ID_TABLA = LO.ID And l.TABLA In (11,13) And LO.ACCION ='I' " &
                            " Left Join LOG_PEDIDO_RECEP Lr WITH(NOLOCK) ON ID_TABLA = Lr.ID And l.TABLA IN (12,14) And Lr.ACCION ='I' " &
                            " Left Join LOG_INSTANCIA Li WITH(NOLOCK) ON ID_TABLA = Li.ID And l.TABLA IN (18) And Li.ACCION ='I' " &
                            " WHERE L.FECACT Is Not NULL And L.FECACT > '" & Format(filas("FECHA_INI"), "MM/dd/yyy HH:mm:ss") & "' AND L.FECACT < '" & Format(filas("FECHA_FIN"), "MM/dd/yyy HH:mm:ss") & "' AND L.ORIGEN IN (0,2)"
                        cm.CommandType = CommandType.Text
                        da.SelectCommand = cm
                        da.Fill(dr)
                        iNumTotalXML = CInt(dr.Tables(0).Rows(0).Item("REGISTROS_XML").ToString())
                        dr.Clear()
                    Case 2
                        cm.CommandText = "SELECT COUNT(*) REGISTROS_ACUSE FROM LOG_GRAL WITH(NOLOCK) WHERE FECENV IS NOT NULL AND FEC_ACUSE > '" & Format(filas("FECHA_INI"), "MM/dd/yyy HH:mm:ss") & "' AND FECENV < '" & Format(filas("FECHA_FIN"), "MM/dd/yyy HH:mm:ss") & "' AND ORIGEN IN (0,2)"
                        cm.CommandType = CommandType.Text
                        da.SelectCommand = cm
                        da.Fill(dr)
                        iNumTotalAcuse = CInt(dr.Tables(0).Rows(0).Item("REGISTROS_ACUSE").ToString())
                        dr.Clear()
                End Select
            Next
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try


    End Sub


#End Region
#Region " FSIS "

    '''<summary>FunciÃ³n que obtiene el tipo de destino de la integraciÃ³n.</summary>
    '''<param name="iEntidad">La entidad a integrar.</param>
    '''<returns>Integer con el tipo de destino de la integraciÃ³n.</returns>
    '''<remarks>Llamada desde: EP_ValidacionesIntegracion.ObtenerTipoInt </remarks>
    Public Function INT_ObtenerTipoInt(ByVal iEntidad As Integer, ByVal iErp As Integer) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cm.CommandText = "Select TOP 1 DESTINO_TIPO FROM TABLAS_INTEGRACION_ERP With(NOLOCK)"
            cm.CommandText = cm.CommandText & " WHERE ENTIDAD=" & iEntidad & " And ACTIVA=1 And ERP=" & iErp
            cm.CommandText = cm.CommandText & " And SENTIDO In (" & SentidoIntegracion.EntradaYSalida & ", "
            cm.CommandText = cm.CommandText & SentidoIntegracion.Salida & ")"
            cm.Connection = cn
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    '''<summary>Función que obtiene los credenciales necesarios para la conexión con el ERP del cliente para la integración vía WCF.</summary>
    '''<param name="iEntidadIntegracion">La entidad a integrar.</param>
    '''<param name="iErp">El ERP de la entidad a integrar.</param>
    '''<returns>DataSet con el nombre, contraseña y fecha de la contraseña.</returns>
    '''<remarks>Llamada desde: Integracion.ObtenerCredencialesWCF. </remarks>
    Public Function INT_ObtenerCredencialesWCF(ByVal iEntidadIntegracion As Integer, ByVal iErp As Integer) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            If iErp <> 0 Then
                cm.CommandText = "Select WCF_ORIG_USU, WCF_ORIG_PWD, WCF_ORIG_FECPWD FROM TABLAS_INTEGRACION_ERP With(NOLOCK) "
                cm.CommandText = cm.CommandText & "WHERE TABLA=@TABLA And ERP=@ERP"
                cm.Connection = cn
                cm.CommandType = CommandType.Text
                cm.Parameters.AddWithValue("@TABLA", iEntidadIntegracion)
                cm.Parameters.AddWithValue("@ERP", iErp)
                da.SelectCommand = cm
                da.Fill(dr)
            Else
                cm.CommandText = "Select TOP 1 WCF_ORIG_USU, WCF_ORIG_PWD, WCF_ORIG_FECPWD FROM TABLAS_INTEGRACION_ERP With(NOLOCK) "
                cm.CommandText = cm.CommandText & "WHERE TABLA=@TABLA"
                cm.Connection = cn
                cm.CommandType = CommandType.Text
                cm.Parameters.AddWithValue("@TABLA", iEntidadIntegracion)
                da.SelectCommand = cm
                da.Fill(dr)
            End If
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function

    '''<summary>FunciÃ³n que obtiene los IDs necesarios de una recepciÃ³n (ID_LOG_ENTIDAD y ERP).</summary>
    '''<param name="lIdLineaRecep">El ID de la lÃ­nea de la recepciÃ³n.</param>
    '''<returns>Dataset con los identificadores necesarios.</returns>
    '''<remarks>Llamada desde: Integracion.ObtenerIDentificadoresRecep.
    '''Tiempo máximo: menos de 1seg.
    '''Revisado por: aam 30/01/2014</remarks> 
    Public Function INT_ObtenerIdentificadoresRecep(ByVal lIdLineaRecep As Long, ByVal cAccion As Char) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cm.CommandText = "Select TOP 1 LOG_PEDIDO_RECEP.ID, LOG_PEDIDO_RECEP.ERP FROM LOG_GRAL With(NOLOCK)"
            cm.CommandText = cm.CommandText & " INNER JOIN LOG_PEDIDO_RECEP With(NOLOCK) On LOG_GRAL.ID_TABLA=LOG_PEDIDO_RECEP.ID"
            cm.CommandText = cm.CommandText & " And LOG_GRAL.TABLA In(@TABLA1,@TABLA2)"
            cm.CommandText = cm.CommandText & " INNER JOIN LOG_LINEAS_RECEP With(NOLOCK) On LOG_PEDIDO_RECEP.ID="
            cm.CommandText = cm.CommandText & "LOG_LINEAS_RECEP.ID_LOG_PEDIDO_RECEP INNER JOIN LINEAS_RECEP With(NOLOCK)"
            cm.CommandText = cm.CommandText & " On LOG_LINEAS_RECEP.ID_PEDIDO_RECEP=LINEAS_RECEP.PEDIDO_RECEP"
            cm.CommandText = cm.CommandText & " WHERE LINEAS_RECEP.ID=@ID And LOG_PEDIDO_RECEP.ACCION=@ACCION"
            cm.CommandText = cm.CommandText & " And LOG_GRAL.ORIGEN In (@ORIGEN1, @ORIGEN2)"
            cm.CommandText = cm.CommandText & " And LOG_GRAL.ESTADO=@ESTADO"
            cm.Parameters.AddWithValue("@ID", lIdLineaRecep)
            cm.Parameters.AddWithValue("@ACCION", cAccion)
            cm.Parameters.AddWithValue("@TABLA1", TablasIntegracion.Rec_Aprov)
            cm.Parameters.AddWithValue("@TABLA2", TablasIntegracion.Rec_Directo)
            cm.Parameters.AddWithValue("@ORIGEN1", OrigenIntegracion.FSGSInt)
            cm.Parameters.AddWithValue("@ORIGEN2", OrigenIntegracion.FSGSIntReg)
            cm.Parameters.AddWithValue("@ESTADO", EstadoIntegracion.ParcialmenteIntegrado)
            cm.Connection = cn
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    '''<summary>FunciÃ³n que obtiene los IDs necesarios de una recepciÃ³n (ID_LOG_ENTIDAD y ERP).</summary>
    '''<param name="lIdPedRecep">El ID de la recepciÃ³n.</param>
    '''<param name="lIdOrden">El ID de la orden de entrega.</param>
    '''<returns>Dataset con los identificadores necesarios.</returns>
    '''<remarks>Llamada desde: Integracion.ObtenerIdentificadoresRecep.</remarks>
    Public Function INT_ObtenerIdentificadoresRecepPorIDs(ByVal lIdPedRecep As Long, ByVal lIdOrden As Long) _
            As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cm.CommandText = "Select TOP 1 LOG_PEDIDO_RECEP.ID As ID, LOG_PEDIDO_RECEP.ERP FROM LOG_PEDIDO_RECEP With(NOLOCK) INNER JOIN LOG_GRAL With(NOLOCK) On LOG_GRAL.ID_TABLA=LOG_PEDIDO_RECEP.ID " &
            " And TABLA In (" & TablasIntegracion.Rec_Directo & "," & TablasIntegracion.Rec_Aprov & ") And LOG_GRAL.ORIGEN In (" & OrigenIntegracion.FSGSInt & ", " & OrigenIntegracion.FSGSIntReg & ")  WHERE ID_PEDIDO_RECEP=" & lIdPedRecep & " And ID_ORDEN_ENTREGA=" & lIdOrden & " And LOG_gRAL.ESTADO=" & EstadoIntegracion.ParcialmenteIntegrado & " ORDER BY LOG_GRAL.ID DESC"
            cm.Connection = cn
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    '''<summary>FunciÃ³n que obtiene los IDs necesarios de una recepciÃ³n (ID_ORDEN, ID_PEDIDO y ID_PEDIDO_RECEP).</summary>
    '''<param name="lIdLineaRecep">El ID de la línea de la recepción</param>
    '''<returns>Dataset con los identificadores necesarios.</returns>
    '''<remarks>Llamada desde: Integracion.ObtenerIdentificadoresRecepPorIdLinea.</remarks>
    Public Function INT_ObtenerIdentificadoresRecepPorIdLinea(ByVal lIdLineaRecep As Long) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cm.CommandText = "Select TOP 1 OE.ID As ID_ORDEN, LR.PEDIDO_RECEP As ID_PEDIDO_RECEP FROM LINEAS_RECEP LR With (NOLOCK) "
            cm.CommandText = cm.CommandText & "INNER JOIN ORDEN_ENTREGA OE With (NOLOCK) On OE.ID=LR.ORDEN "
            cm.CommandText = cm.CommandText & "INNER JOIN LINEAS_PEDIDO LP With (NOLOCK) On LP.ID=LR.LINEA "
            cm.CommandText = cm.CommandText & "WHERE LR.ID=@ID_LINEA_RECEP"
            cm.Connection = cn
            cm.CommandType = CommandType.Text
            cm.Parameters.AddWithValue("@ID_LINEA_RECEP", lIdLineaRecep)
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    '''<summary>FunciÃ³n que obtiene los IDs necesarios de un pedido (ID_LOG_ENTIDAD y ERP).</summary>
    '''<param name="lIdOrden">El ID de la TABLA orden de entrega.</param>
    '''<returns>Dataset con los identificadores necesarios.</returns>
    '''<remarks>Llamada desde: Integracion.ObtenerIdentificadoresPedido.</remarks>
    Public Function INT_ObtenerIdentificadoresPedido(ByVal lIdOrden As Long) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cm.CommandText = "Select LOG_ORDEN_ENTREGA.ID, LOG_ORDEN_ENTREGA.ERP, EST, TABLA FROM LOG_ORDEN_ENTREGA With(NOLOCK) INNER JOIN LOG_GRAL With(NOLOCK) On ID_TABLA=LOG_ORDEN_ENTREGA.ID And TABLA In (" & TablasIntegracion.PED_directo & "," & TablasIntegracion.PED_Aprov & ")" &
        " And LOG_GRAL.ORIGEN In (" & OrigenIntegracion.FSGSInt & ", " & OrigenIntegracion.FSGSIntReg & ") WHERE ID_ORDEN_ENTREGA=" & lIdOrden & " And LOG_GRAL.ESTADO=" & EstadoIntegracion.ParcialmenteIntegrado & " ORDER BY LOG_ORDEN_ENTREGA.ID DESC"
            cm.Connection = cn
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function


    '''<summary>Función que obtiene los IDs necesarios del borrado de una Adjudicación (ID_TABLA).</summary>
    '''<param name="sAnyo">El año en el que se realizó la adjudicación.</param>
    '''<param name="sGMN1">El nivel 1 de la estructura de materiales perteneciente al proceso.</param>
    '''<param name="iProce">El ID del proceso.</param>
    '''<param name="iErp">El ERP del cliente.</param>
    '''<returns>Dataset con los identificadores necesarios.</returns>
    '''<remarks>Llamada desde: Integracion.ObtenerIdentificadoresBorradosADJ.</remarks>
    Public Function INT_ObtenerIdentificadoresBorradosADJ(ByVal sAnyo As Short, ByVal sGMN1 As String, ByVal iProce As Integer, ByVal iErp As Integer) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cm.CommandText = "Select ID_TABLA FROM LOG_GRAL As LG With(NOLOCK) INNER JOIN LOG_ITEM_ADJ As LIA With(NOLOCK) "
            cm.CommandText &= "On LG.ID_TABLA=LIA.ID And LG.TABLA=@TABLA WHERE LG.ESTADO=@ESTADO And LG.ORIGEN In (@ORIGEN1, @ORIGEN2) And LG.ERP=@ERP And LIA.ACCION=@ACCION "
            cm.CommandText &= "And LIA.ANYO=@ANYO And LIA.GMN1_PROCE=@GMN1 And LIA.PROCE=@PROCE "
            cm.Connection = cn
            cm.CommandType = CommandType.Text
            cm.Parameters.AddWithValue("@TABLA", TablasIntegracion.Adj)
            cm.Parameters.AddWithValue("@ESTADO", EstadoIntegracion.ParcialmenteIntegrado)
            cm.Parameters.AddWithValue("@ORIGEN1", OrigenIntegracion.FSGSInt)
            cm.Parameters.AddWithValue("@ORIGEN2", OrigenIntegracion.FSGSIntReg)
            cm.Parameters.AddWithValue("@ERP", iErp)
            cm.Parameters.AddWithValue("@ACCION", AccionIntegracion.Baja)
            cm.Parameters.AddWithValue("@ANYO", sAnyo)
            cm.Parameters.AddWithValue("@GMN1", sGMN1)
            cm.Parameters.AddWithValue("@PROCE", iProce)
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Esta función indica si la integración vía WCF está activa o no para la entidad deseada.
    ''' </summary>
    ''' <param name="iEntidad">La entidad a comprobar si la integración vía WCF está activa o no.</param>
    ''' <param name="iErp">El ERP actual.</param>
    ''' <returns>DataSet con el resultado.</returns>
    ''' <remarks>
    ''' Llamada desde: Integracion.IntegracionWCFActiva
    ''' Revisado por aam (28/01/2015)
    ''' Tiempo máximo: menos de 1seg.
    ''' </remarks>
    Public Function INT_IntegracionWCFActiva(ByVal iEntidad As Integer, ByVal iErp As Integer) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cm.CommandText = "Select TOP 1 ACTIVA FROM TABLAS_INTEGRACION_ERP With(NOLOCK) WHERE ACTIVA=1 "
            cm.CommandText = cm.CommandText & "And TABLA=@TABLA And DESTINO_TIPO=@DESTINO_TIPO And ERP=@ERP"
            cm.Connection = cn
            cm.CommandType = CommandType.Text
            cm.Parameters.AddWithValue("@TABLA", iEntidad)
            cm.Parameters.AddWithValue("@DESTINO_TIPO", TipoDestinoIntegracion.WCF)
            cm.Parameters.AddWithValue("@ERP", iErp)
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' FunciÃ³n que obtiene el valor del parÃ¡metro IntPedTrasAcep. Este parÃ¡metro de integraciÃ³n indica que se integran
    ''' los pedidos despuÃ©s de que se aceptenpor parte del proveedor (1) o como se ha hecho siempre en FULLSTEP (0).
    ''' </summary>
    ''' <param name="iErp">El ERP actual.</param>
    ''' <returns>DataSet con el valor del parÃ¡metro de integraciÃ³n.</returns>
    ''' <remarks>Llamada desde: Integracion.ObtenerIntPedTrasAcep.</remarks>
    Public Function INT_ObtenerIntPedTrasAcep(ByVal iErp As Integer) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cm.CommandText = "Select INT_PED_TRAS_ACEP FROM PARGEN_INTEGRACION With(NOLOCK) WHERE ERP=" & iErp
            cm.Connection = cn
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    '''<summary>FunciÃ³n que obtiene los IDs necesarios de una recepciÃ³n (ID_LOG_ENTIDAD y ERP).</summary>
    '''<param name="lIDInstancia">El ID de la lÃ­nea de la recepciÃ³n.</param>
    '''<returns>Dataset con los identificadores necesarios.</returns>
    '''<remarks>Llamada desde: Integracion.ObtenerIDentificadoresRecep.
    '''Tiempo máximo: menos de 1seg.
    '''Revisado por: aam 10/09/2014</remarks> 
    Public Function INT_DatosWSValidosFSIS(ByVal lIDInstancia As Long) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cm.CommandText = "IS_DatosWSValidosFSIS"

            cm.Parameters.AddWithValue("@INSTANCIA", lIDInstancia)
            cm.Parameters.AddWithValue("@TABLA", TablasIntegracion.PM)
            cm.Parameters.AddWithValue("@SENTIDO1", SentidoIntegracion.Salida)
            cm.Parameters.AddWithValue("@SENTIDO2", SentidoIntegracion.EntradaYSalida)
            cm.Parameters.AddWithValue("@ORIGEN1", OrigenIntegracion.FSGSInt)
            cm.Parameters.AddWithValue("@ORIGEN2", OrigenIntegracion.FSGSIntReg)
            cm.Parameters.AddWithValue("@ESTADO", EstadoIntegracion.ParcialmenteIntegrado)
            cm.Parameters.AddWithValue("@DESTINO_TIPO", TipoDestinoIntegracion.WCF)
            cm.Parameters.AddWithValue("@ACTIVA", 1)
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    '''<summary>Función que obtiene la ruta de instalación de FSIS.</summary>
    '''<returns>Dataset con la ruta de instalación de FSIS.</returns>
    '''<remarks>Llamada desde: Integracion.ObtenerRutaInstalacionFSIS.
    '''Tiempo máximo: menos de 1seg.
    '''Revisado por: aam 22/12/2014</remarks> 
    Public Function INT_ObtenerRutaInstalacionFSIS() As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cm.CommandText = "Select TOP 1 RUTAXBAP FROM PARGEN_RUTAS With(NOLOCK) WHERE NOMBREPROY='FullstepIS'"
            cm.Connection = cn
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function

    Public Function INT_ObtenerDatosConexionFSIS(ByVal iErp As Integer) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            If iErp <> 0 Then
                cm.CommandText = "SELECT SERVICEBINDINGTYPE, SERVICESECURITYMODE, SERVICECLIENTCREDENTIALTYPE, SERVICEPROXYCREDENTIALTYPE "
                cm.CommandText &= "FROM PARGEN_INTEGRACION WITH(NOLOCK) WHERE ERP=@ERP"
                cm.Connection = cn
                cm.CommandType = CommandType.Text
                cm.Parameters.AddWithValue("@ERP", iErp)
                da.SelectCommand = cm
                da.Fill(dr)
            Else
                cm.CommandText = "SELECT TOP 1 SERVICEBINDINGTYPE, SERVICESECURITYMODE, SERVICECLIENTCREDENTIALTYPE, SERVICEPROXYCREDENTIALTYPE "
                cm.CommandText &= "FROM PARGEN_INTEGRACION WITH(NOLOCK)"
                cm.Connection = cn
                cm.CommandType = CommandType.Text
                da.SelectCommand = cm
                da.Fill(dr)
            End If
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' FunciÃ³n que obtiene el valor del parÃ¡metro TRASPASO_ADJ_ERP. Este parÃ¡metro de general indica que se puede realizar el traspaso de la adjudicaciÃ³n al ERP.
    ''' </summary>
    ''' <returns>DataSet con el valor del parÃ¡metro general.</returns>
    ''' <remarks>Llamada desde: Integracion.HayTraspasoADJERP.</remarks>
    Public Function INT_HayTraspasoADJERP() As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cm.CommandText = "SELECT TRASPASO_ADJ_ERP FROM PARGEN_INTERNO WITH(NOLOCK)"
            cm.Connection = cn
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
#End Region
#End Region
End Class