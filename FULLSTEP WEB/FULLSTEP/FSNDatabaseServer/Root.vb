﻿Imports System.Configuration
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Exception
Imports System.IO
Imports System.Object
Imports Fullstep.FSNLibrary
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports Fullstep.FSNLibrary.Operadores

<Serializable()> _
Public Class Root
#Region "Propiedades Conexión a BD/Authenticate"
    Private mDBLogin As String = ""
    Property DBLogin() As String
        Get
            Return mDBLogin
        End Get
        Set(value As String)
            mDBLogin = value
        End Set
    End Property
    Private mDBPassword As String = ""
    Property DBPassword() As String
        Get
            Return mDBPassword
        End Get
        Set(value As String)
            mDBPassword = value
        End Set
    End Property
    Private mDBName As String = ""
    Property DBName() As String
        Get
            Return mDBName
        End Get
        Set(value As String)
            mDBName = value
        End Set
    End Property
    Private mDBServer As String = ""
    Property DBServer() As String
        Get
            Return mDBServer
        End Get
        Set(value As String)
            mDBServer = value
        End Set
    End Property
    'La he tenido que añadir porque en el EP se necesita para acceder a BD desde la CAPA DE negocio corregir ep!!
    Private mDBConnection As String = ""
    Property DBConnection() As String
        Get
            Return mDBConnection
        End Get
        Set(value As String)
            mDBConnection = value
        End Set
    End Property
    Private mIsAuthenticated As Boolean = False
    Private Sub Authenticate()
        If Not mIsAuthenticated Then Throw New System.Security.SecurityException("User not authenticated!")
    End Sub
    Private mImpersonateLogin As String = ""
    Property ImpersonateLogin() As String
        Get
            Return mImpersonateLogin
        End Get
        Set(value As String)
            mImpersonateLogin = value
        End Set
    End Property
    Private mImpersonatePassword As String = ""
    Property ImpersonatePassword() As String
        Get
            Return mImpersonatePassword
        End Get
        Set(value As String)
            mImpersonatePassword = value
        End Set
    End Property
    Private mImpersonateDominio As String = ""
    Property ImpersonateDominio() As String
        Get
            Return mImpersonateDominio
        End Get
        Set(value As String)
            mImpersonateDominio = value
        End Set
    End Property


#End Region
#Region "Constructor"
    Public Sub New()
        'Comprobar tema de licencias y cargar datos de conexión
        Dim sDB As String = ConfigurationManager.AppSettings("DB:FS_WS")
        If sDB Is Nothing Then
            Throw New Exception("Configuration file corrupted. Missing FS_WS entry !")
        End If
        'Tenemos que extraer el login y la contraseña de conexión.
        Dim doc As New XmlDocument
        Try
            doc.Load(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory(), "License.xml"))

        Catch e As Exception
            Throw New Exception("LICENSE file missing!")
        End Try
        Dim child As XmlNode
        child = doc.SelectSingleNode("/LICENSE/DB_LOGIN")
        If Not (child Is Nothing) Then
            mDBLogin = FSNLibrary.Encrypter.Encrypt(child.InnerText, , False, FSNLibrary.Encrypter.TipoDeUsuario.Administrador)
            'mDBLogin = child.InnerText
        End If
        child = Nothing
        child = doc.SelectSingleNode("/LICENSE/DB_PASSWORD")
        If Not (child Is Nothing) Then
            mDBPassword = FSNLibrary.Encrypter.Encrypt(child.InnerText, , False, FSNLibrary.Encrypter.TipoDeUsuario.Administrador)
            'mDBPassword = child.InnerText
        Else
            mDBPassword = ""
        End If
        child = doc.SelectSingleNode("/LICENSE/DB_NAME")
        If Not (child Is Nothing) Then
            mDBName = FSNLibrary.Encrypter.Encrypt(child.InnerText, , False, FSNLibrary.Encrypter.TipoDeUsuario.Administrador)
        End If
        child = Nothing
        child = doc.SelectSingleNode("/LICENSE/DB_SERVER")
        If Not (child Is Nothing) Then
            mDBServer = FSNLibrary.Encrypter.Encrypt(child.InnerText, , False, FSNLibrary.Encrypter.TipoDeUsuario.Administrador)
        End If
        child = Nothing
        child = doc.SelectSingleNode("/LICENSE/IMP_USER")
        If Not (child Is Nothing) Then
            mImpersonateLogin = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        End If
        child = Nothing
        child = doc.SelectSingleNode("/LICENSE/IMP_PWD")
        If Not (child Is Nothing) Then
            mImpersonatePassword = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        Else
            mImpersonatePassword = ""
        End If
        child = Nothing
        child = doc.SelectSingleNode("/LICENSE/IMP_DOM")
        If Not (child Is Nothing) Then
            mImpersonateDominio = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        End If
        If mDBServer = "" Or mDBName = "" Or mDBLogin = "" Then
            Throw New Exception("Corrupted LICENSE file !")
        End If

        'PENDIENTE METER ENCRIPTACIÓN
        sDB = sDB.Replace("DB_LOGIN", mDBLogin)
        sDB = sDB.Replace("DB_PASSWORD", mDBPassword)
        sDB = sDB.Replace("DB_NAME", mDBName)
        sDB = sDB.Replace("DB_SERVER", mDBServer)
        doc = Nothing
        mDBConnection = sDB
    End Sub
#End Region
#Region "Common. Data Access Methods "
#Region "Root data access methods "
#Region "Genrales. Logitude, Tipo Acceso"
    ''' <summary>
    ''' Procedimiento que carga las longitudes de los campos utilizados en todo el producto
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: FSNServer/Root/Load_LongitudesDeCodigos
    ''' Tiempo máximo: 0,2 seg</remarks>
    Public Function LongitudesCampo_Get() As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SELECT ID,LONGITUD FROM DIC WITH(NOLOCK) ORDER BY ID"
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Funcion que devuelve el parámetro de Pargen_interno WIN_Sec_Web, no hace falta autenticate
    ''' </summary>
    ''' <returns>El valor de ese parámetro</returns>
    ''' <remarks>
    ''' Llamada desde: FsnServer/Root.vb/DevolverTipoAcceso
    ''' Tiempo máximo: Automático</remarks>
    Public Function TipoDeAcceso_Get() As TiposDeAutenticacion
        Dim SqlCmd As New SqlCommand
        Dim sConexion As String
        sConexion = mDBConnection
        sConexion = sConexion & ";Max Pool Size=" & ConfigurationManager.AppSettings("MaxPoolLogin") & ";"
        Dim SqlCn As New SqlConnection(sConexion)
        Dim DS As New DataSet
        Dim DA As New SqlDataAdapter
        Try
            SqlCmd.CommandText = "SELECT WIN_SEC_WEB FROM PARGEN_INTERNO WITH(NOLOCK)"
            SqlCmd.CommandType = CommandType.Text
            SqlCn.Open()
            SqlCmd.Connection = SqlCn
            DA.SelectCommand = SqlCmd
            DA.Fill(DS)

            Return DS.Tables(0).Rows(0).Item("WIN_SEC_WEB")
        Catch e As Exception
            Throw e
        Finally
            SqlCn.Dispose()
            SqlCmd.Dispose()
            DA.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Funcion que devuelve el parametro de Pargen_interno ACCESO_LCX, no hace falta autenticate
    ''' </summary>
    ''' <returns>El valor de ese parametro</returns>
    ''' <remarks>
    ''' Llamada desde: FsnServer/Root.vb/TipoAccesoLCX
    ''' Tiempo maximo: Automatico</remarks>
    Public Function TipoDeAccesoLCX_Get() As Boolean
        Dim SqlCmd As New SqlCommand
        Dim sConexion As String
        sConexion = mDBConnection
        sConexion = sConexion & ";Max Pool Size=" & ConfigurationManager.AppSettings("MaxPoolLogin") & ";"
        Dim SqlCn As New SqlConnection(sConexion)

        Dim DS As New DataSet
        Dim DA As New SqlDataAdapter
        Try
            SqlCmd.CommandText = "SELECT ACCESO_LCX FROM PARGEN_INTERNO WITH(NOLOCK)"
            SqlCmd.CommandType = CommandType.Text
            SqlCn.Open()
            SqlCmd.Connection = SqlCn
            DA.SelectCommand = SqlCmd
            DA.Fill(DS)
            Return (DS.Tables(0).Rows(0).Item("ACCESO_LCX") = 1)
        Catch e As Exception
            Throw e
        Finally
            SqlCn.Dispose()
            SqlCmd.Dispose()
            DA.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Funcion que devuelve que comprueba si una empresa es valida en FULLSTEP, no hace falta autenticate
    ''' </summary>
    ''' <returns>Contador para saber si esa empresa esta en FULLSTEP </returns>
    ''' <remarks>
    ''' Llamada desde: FsnServer/Root.vb/ComprobarEmpresaLCX
    ''' Tiempo maximo: Automatico</remarks>
    Public Function ComprobarEMpresa(ByVal Sociedad As String) As Boolean
        Dim SqlCmd As New SqlCommand
        Dim SqlCn As New SqlConnection(mDBConnection)
        Dim DS As New DataSet
        Dim DA As New SqlDataAdapter
        Try
            SqlCmd.CommandText = "SELECT COUNT(SOCIEDAD)SOCIEDAD FROM EMP WITH(NOLOCK) WHERE SOCIEDAD = '" & Sociedad & "'"
            SqlCmd.CommandType = CommandType.Text
            SqlCn.Open()
            SqlCmd.Connection = SqlCn
            DA.SelectCommand = SqlCmd
            DA.Fill(DS)
            Return (DS.Tables(0).Rows(0).Item("SOCIEDAD") >= 1)
        Catch e As Exception
            Throw e
        Finally
            SqlCn.Dispose()
            SqlCmd.Dispose()
            DA.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Funcion que devuelve que comprueba si un centro de coste es valido en FULLSTEP, no hace falta autenticate
    ''' </summary>
    ''' <returns>Contador para saber si el centro esta en FULLSTEP </returns>
    ''' <remarks>
    ''' Llamada desde: FsnServer/Root.vb/ComprobarCentroCosteLCX
    ''' Tiempo maximo: Automatico</remarks>
    Public Function ComprobarCentroCosteLCX(ByVal CentroCoste As String) As Boolean
        Dim SqlCmd As New SqlCommand
        Dim SqlCn As New SqlConnection(mDBConnection)
        Dim DS As New DataSet
        Dim DA As New SqlDataAdapter
        Try
            SqlCmd.CommandText = "SELECT "
            SqlCmd.CommandText += "(SELECT COUNT(COD)CENTRODECOSTE FROM UON3 WITH(NOLOCK) WHERE COD = '" & CentroCoste & "')"
            SqlCmd.CommandText += " + "
            SqlCmd.CommandText += "(SELECT COUNT(COD)CENTRODECOSTE FROM UON4 WITH(NOLOCK) WHERE COD = '" & CentroCoste & "')"
            SqlCmd.CommandText += " CENTRODECOSTE"
            SqlCmd.CommandType = CommandType.Text
            SqlCn.Open()
            SqlCmd.Connection = SqlCn
            DA.SelectCommand = SqlCmd
            DA.Fill(DS)
            Return (DS.Tables(0).Rows(0).Item("CENTRODECOSTE") >= 1)
        Catch e As Exception
            Throw e
        Finally
            SqlCn.Dispose()
            SqlCmd.Dispose()
            DA.Dispose()
        End Try
    End Function
    ''' Devolvemos un dataset con el remitente y la carpeta de la plantilla
    ''' </summary>
    ''' <param name="lEmpresa">Id de la empresa</param>
    ''' <returns></returns>
    Public Function SacarRemitente_y_CarpetaPlantilla(ByVal lEmpresa As Long) As Data.DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_DEVOLVER_REMITENTE_Y_CARPETA_EMPRESA"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@EMPRESA", lEmpresa)
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            If Not cn Is Nothing Then
                cn.Dispose()
            End If
            If Not cm Is Nothing Then
                cm.Dispose()
            End If
            If Not da Is Nothing Then
                da.Dispose()
            End If
        End Try
    End Function

    Public Function SacarRemitente_y_CarpetaPlantilla_Portal(ByVal sProve As String) As Data.DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_DEVOLVER_REMITENTE_Y_CARPETA_EMPRESA_PORTAL"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@PROVE", sProve)
            cm.Parameters.AddWithValue("@SRV_PORTAL", mMailPortalFSP_SRV)
            cm.Parameters.AddWithValue("@BBDD_PORTAL", mMailPortalFSP_BD)
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            If Not cn Is Nothing Then
                cn.Dispose()
            End If
            If Not cm Is Nothing Then
                cm.Dispose()
            End If
            If Not da Is Nothing Then
                da.Dispose()
            End If
        End Try
    End Function
    ''' <summary>
    ''' Dado un Codigo de portal devuelve en un dataset el codigo, el remitente y la url
    ''' </summary>
    ''' <param name="sPortal"></param>
    ''' <returns></returns>
    Public Function DevolverDatosPortal(ByVal sPortal As String) As Data.DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            Dim sFSP As String = mMailPortalFSP_SRV & "." & mMailPortalFSP_BD & ".dbo."
            cm.Connection = cn
            cm.CommandText = sFSP & "FSP_DEVOLVER_DATOS_PORTAL"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@PORTAL", sPortal)
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            If Not cn Is Nothing Then
                cn.Dispose()
            End If
            If Not cm Is Nothing Then
                cm.Dispose()
            End If
            If Not da Is Nothing Then
                da.Dispose()
            End If
        End Try
    End Function

    Public Function SacarEmpresaPorInstancia(ByRef idInstancia As Long, ByRef iTipoSolicitud As TiposDeDatos.TipoDeSolicitud) As Data.DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_DEVOLVER_EMPRESA_POR_INSTANCIA"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@INSTANCIA", idInstancia)
            cm.Parameters.AddWithValue("@TIPO", iTipoSolicitud)
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            If Not cn Is Nothing Then
                cn.Dispose()
            End If
            If Not cm Is Nothing Then
                cm.Dispose()
            End If
            If Not da Is Nothing Then
                da.Dispose()
            End If
        End Try
    End Function

    Public Function User_ParticipaSolicitud(ByVal sUserCod As String, ByVal Tipo As TiposDeDatos.TipoDeSolicitud, ByVal TipoParticipacion As TipoParticipacionSolicitud) As Boolean
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_USU_PARTICIPA_SOLICITUD"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USUCOD", sUserCod)
            cm.Parameters.AddWithValue("@TIPOSOLICITUD", Tipo)
            cm.Parameters.AddWithValue("@TIPOPARTICIPACION", TipoParticipacion)
            da.SelectCommand = cm
            da.Fill(dr)
            Return CType(dr.Tables(0).Rows(0)("TIENESOLICITUD"), Boolean)
        Catch e As Exception
            Throw e
        Finally
            If Not cn Is Nothing Then cn.Dispose()
            If Not cm Is Nothing Then cm.Dispose()
            If Not da Is Nothing Then da.Dispose()
        End Try
    End Function
#End Region
#Region "Login"
    ''' <summary>
    ''' Accedemos a bd y comparamos el login, si es correcto 
    ''' </summary>
    ''' <param name="usercode">Usuario</param>
    ''' <param name="userpassword">password</param>
    ''' <param name="Portal">Usuario portal/ no portal</param>
    ''' <param name="OmitirPassword">Comprobar o no la password</param>
    ''' <param name="BDPassword">password bd</param>
    ''' <returns>si es correcto o no</returns>
    ''' <remarks>Llamada desde: FSnServer/Root.vb/login FSnServer/Root.vb/loginPortal ; Tiempo maximo: 0,2</remarks>
    Public Function Login(Optional ByVal usercode As String = "", Optional ByVal userpassword As String = "",
                Optional ByVal Portal As Boolean = False, Optional ByVal OmitirPassword As Boolean = False,
                Optional ByVal BDPassword As Boolean = False) As TipoResultadoLogin
        'Accedemos a bd y comparamos el login, si es correcto
        Dim sConexion As String
        sConexion = mDBConnection
        sConexion = sConexion & ";Max Pool Size=" & ConfigurationManager.AppSettings("MaxPoolLogin") & ";"

        Dim cn As New SqlConnection(sConexion)
        Dim cm As New SqlCommand
        Dim fecha As Date
        Dim pwd As String = String.Empty
        Dim decrypteduserpassword As String = String.Empty
        Dim bLoginOK = False
        Dim sNombrePOOL As String
        Dim dr As SqlDataReader = Nothing
        Dim oResLogin As TipoResultadoLogin

        Try
            mUsuarioConectado = usercode

            If Not Portal Then
                cn.Open()
                If Not OmitirPassword AndAlso Not BDPassword Then
                    decrypteduserpassword = FSNLibrary.Encrypter.Encrypt(userpassword, , False)
                End If
                cm.Connection = cn
                cm.CommandText = "FSWS_LOGIN" 'Obtenemos unicamento lo necesario para logearnos, no las propiedades del usuario
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@COD", usercode)

                dr = cm.ExecuteReader()
                If dr.Read() Then
                    If SQLBinaryToBoolean(dr.Item("USUBLOQUEADO")) Then
                        'Usuario bloqueado                      
                        oResLogin = TipoResultadoLogin.UsuBloqueado
                    Else
                        'Desencriptamos la pwd y la comparamos con la del login
                        pwd = dr.Item("PWD").ToString()
                        fecha = dr.Item("FECPWD")
                        If BDPassword Then
                            decrypteduserpassword = FSNLibrary.Encrypter.Encrypt(userpassword, dr.Item("COD"), False, Encrypter.TipoDeUsuario.Persona, fecha) ' desencripta lo mandado
                        End If
                        pwd = FSNLibrary.Encrypter.Encrypt(pwd, usercode, False, FSNLibrary.Encrypter.TipoDeUsuario.Persona, fecha)
                        If (pwd.Equals(decrypteduserpassword) And OmitirPassword = False) OrElse OmitirPassword Then
                            oResLogin = TipoResultadoLogin.LoginOk

                            'Resetear nº de bloqueos
                            If dr("BLOQ") > 0 Then LoginResetNumeroBloqueos(usercode)

                            mIsAuthenticated = True
                                'Asignar al usuario el POOL que le corresponde
                                If IsDBNull(dr.Item("ID_POOL")) Then
                                    'No tiene ningun POOL asignado.
                                    sNombrePOOL = ""
                                Else
                                    sNombrePOOL = comprobarPOOL(dr.Item("ID_POOL"))
                                End If
                                If sNombrePOOL <> "" Then
                                    sNombrePOOL = "summitapp" & sNombrePOOL
                                    mDBConnection = Replace(mDBConnection, mDBLogin, sNombrePOOL)
                                    mDBLogin = sNombrePOOL
                                End If
                            Else
                                'Pwd incorrecto                        
                                oResLogin = TipoResultadoLogin.PwdIncorrecto
                        End If
                    End If

                    dr.Close()
                Else
                    'No existe el usuario
                    oResLogin = TipoResultadoLogin.UsuIncorrecto
                End If
            Else
                oResLogin = TipoResultadoLogin.LoginOk
                mIsAuthenticated = True
            End If

            Return oResLogin
        Catch e As Exception
            Throw e
        Finally
            If Not dr Is Nothing AndAlso Not dr.IsClosed Then
                dr.Close()
            End If
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    Public Sub LoginWindowsServiceTratamientoInstancias()
        mIsAuthenticated = True
    End Sub
    ''' <summary>Gestión el bloqueo de la cuenta por intentos de login fallidos</summary>
    ''' <param name="UserCode">Código de usuario</param>
    ''' <returns>Datatable con los datos de bloqueo</returns>
    Public Function LoginGestionBloqueoCuenta(ByVal UserCode As String) As DataTable
        Dim sConexion As String = mDBConnection
        sConexion &= ";Max Pool Size=" & ConfigurationManager.AppSettings("MaxPoolLogin") & ";"

        Dim cn As New SqlConnection(sConexion)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSWS_LOGIN_GESTION_BLOQUEO_CUENTA"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USUCOD", UserCode)

            da.SelectCommand = cm
            da.Fill(ds)
            Return ds.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>Pone a 0 USU.BLOQ</summary>
    ''' <param name="UserCode">Código de usuario</param>
    ''' <remarks>Llamada desde: Login</remarks>
    Private Sub LoginResetNumeroBloqueos(ByVal UserCode As String)
        Dim sConexion As String = mDBConnection
        sConexion &= ";Max Pool Size=" & ConfigurationManager.AppSettings("MaxPoolLogin") & ";"

        Dim cn As New SqlConnection(sConexion)
        Dim cm As New SqlCommand

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSWS_LOGIN_RESET_NUMERO_BLOQUEOS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USUCOD", UserCode)

            cm.ExecuteNonQuery()

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
#End Region
#Region "Pool de conexiones"
    '------------------------------------------------------------------------------------------------------------------------
    'POOL--------------------------------------------------------------------------------------------------------------------
    '------------------------------------------------------------------------------------------------------------------------
    ''' <summary>
    ''' Función que devuelve el POOL del usuario
    ''' </summary>
    ''' <param name="UserCod">código de usuario</param>
    ''' <remarks>
    ''' Llamada desde: FSNServer.Root/Login
    ''' Tiempo máximo: 0,1 seg
    ''' </remarks>
    Public Sub ObtenerPool(ByVal UserCod As String)
        Dim sConexion As String
        sConexion = mDBConnection
        sConexion = sConexion & ";Max Pool Size=" & ConfigurationManager.AppSettings("MaxPoolLogin") & ";"

        Dim cn As New SqlConnection(sConexion)
        Dim cm As New SqlCommand
        Dim dr As SqlDataReader
        Dim sNombrePool As String
        cn.Open()
        cm.Connection = cn
        cm.CommandText = "FSPM_OBTENER_POOL"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@UserCod", UserCod)
        dr = cm.ExecuteReader()
        If dr.Read() Then
            'Asignar al usuario el POOL que le corresponde
            If IsDBNull(dr.Item("ID_POOL")) Then
                'No tiene ningun POOL asignado. Usuario de QA
                sNombrePool = ""
            Else
                sNombrePool = comprobarPOOL(dr.Item("ID_POOL"))
            End If
            If sNombrePool <> "" Then
                sNombrePool = "summitapp" & sNombrePool
                mDBConnection = Replace(mDBConnection, mDBLogin, sNombrePool)
                mDBLogin = sNombrePool
            End If
            mIsAuthenticated = True
        Else
            mIsAuthenticated = False
        End If

        cn.Close()
        cn.Dispose()
        cm.Dispose()
    End Sub
    Private Function comprobarPOOL(ByVal idPOOL As Long) As String
        Dim sConexion As String
        sConexion = mDBConnection
        sConexion = sConexion & ";Max Pool Size=" & ConfigurationManager.AppSettings("MaxPoolLogin") & ";"

        Dim cn As New SqlConnection(sConexion)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cm.Connection = cn
            cm.CommandText = "SELECT NOMBRE FROM PM_POOLS WITH (NOLOCK) WHERE ID = " & idPOOL
            cm.CommandType = CommandType.Text

            da.SelectCommand = cm
            da.Fill(dr)
            If dr.Tables(0).Rows.Count = 0 Then
                Return ""
            Else
                If IsDBNull(dr.Tables(0).Rows(0).Item("NOMBRE")) Then
                    Return ""
                Else
                    Return dr.Tables(0).Rows(0).Item("NOMBRE")
                End If
            End If
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
#End Region
    ''' <summary>
    ''' Devuelve el listado de bases de datos secundarias, o la de un id determinado
    ''' </summary>
    ''' <param name="id">id de la base de datos</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function obtenerBasesDatosSecundarias(Optional ByVal id As Integer = 0) As DataTable
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim dt As DataTable
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSGS_DEVOLVER_BASES_DATOS_SECUNDARIAS"
            cm.CommandType = CommandType.StoredProcedure
            If id > 0 Then
                cm.Parameters.AddWithValue("@ID", id)
            End If
            da.SelectCommand = cm
            da.Fill(dr)

            If dr.Tables(0).Rows.Count > 0 Then
                dt = dr.Tables(0)
            Else
                dt = New DataTable
            End If
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
        Return dt
    End Function
#Region "Cambio Password"
    '------------------------------------------------------------------------------------------------------------------------
    'PASSWORD----------------------------------------------------------------------------------------------------------------
    '------------------------------------------------------------------------------------------------------------------------
    ''' <summary>
    ''' Función que cambia la Password del usuario, comprobando previamente que cumple con las restricciones de seguridad establecidas, y devolviendo un código de error en caso de que no se cumpla alguna e ellas o 0 si todo ha ido bien
    ''' </summary>
    ''' <param name="ADM">0 si el usuario no es administrador, y 1 si lo es</param>
    ''' <param name="Usuario">Codigo de usuario</param>
    ''' <param name="PWD_NUEVA">Nueva Pwd del usuario</param>
    ''' <param name="fecha_Pwd">Fecha de la contraseña</param>
    ''' <param name="ichange">Variable booleana que indica si se debe cambiar la contraseña en el siguiente inicio de sesión</param>
    ''' <param name="Usu_nuevo">Código de usuario nuevo</param>
    ''' <param name="HISTORICO_PWD">Número de contraseñas a comprobar en caso de que este activada la opción Complejidad de contraseña</param>
    ''' <returns>Un valor entero con el código de respuesta</returns>
    ''' <remarks>
    ''' Llamada desde: PmServer/Root/CambiarPWD
    ''' Tiempo máximo: 0,4 seg</remarks>
    Public Function CambiarPwdDB(ByVal ADM As Integer, ByVal Usuario As String, ByVal PWD_NUEVA As String, ByVal fecha_Pwd As Date, ByVal ichange As Boolean, _
                ByVal Usu_nuevo As String, ByVal HISTORICO_PWD As Integer) As Integer
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "USU_LOGIN"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@ADM", ADM)
            cm.Parameters.AddWithValue("@USUOLD", Usuario)
            cm.Parameters.AddWithValue("@USUNEW", Usu_nuevo)
            cm.Parameters.AddWithValue("@PWDNEW", PWD_NUEVA)
            cm.Parameters.AddWithValue("@FECPWD", fecha_Pwd)
            cm.Parameters.AddWithValue("@CHANGE", ichange)
            cm.Parameters.AddWithValue("@HISTORICO_PWD", HISTORICO_PWD)
            cm.ExecuteNonQuery()
            cn.Close()
            cm.Dispose()
            Return 0
            Exit Function
        Catch e As Exception
            Throw e
            cn.Close()
            cm.Dispose()
            Return 1
        Finally
            cn.Close()
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Funcion que comprueba si la contraseña pasada se encuentra en el histórico de contraseñas
    ''' </summary>
    ''' <param name="usu">Codigo de usuario</param>
    ''' <param name="PWD">Contraseña del usuario</param>
    ''' <returns>Una variable booleana que será true si la contraseña está repetida y 0 si no lo esta</returns>
    ''' <remarks>
    ''' Llamada desde: PmServer/Root/ComprobarPWDRepetido
    ''' Tiempo máximo: 0,2 seg</remarks>
    Public Function ComprobarPWDRepetido(ByVal usu As String, ByVal PWD As String) As Boolean
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim DS As New DataSet
        Dim DA As New SqlDataAdapter
        Dim fecha As Date
        Dim PWDBBDD As String
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSPM_GETHISTORICOPWD"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", usu)
            DA.SelectCommand = cm
            DA.Fill(DS)
            If Not DS Is Nothing AndAlso DS.Tables.Count > 0 AndAlso DS.Tables(0).Rows.Count > 0 Then
                Dim ContraseñaComprobar As String
                For Each FILA As DataRow In DS.Tables(0).Rows
                    fecha = DBNullToStr(FILA.Item("Fec_Pwd"))
                    PWDBBDD = DBNullToStr(FILA.Item("Pwd"))
                    ContraseñaComprobar = Encrypter.Encrypt(PWDBBDD, usu, False, Encrypter.TipoDeUsuario.Persona, fecha)
                    If ContraseñaComprobar = PWD Then
                        If Not cn.State = ConnectionState.Closed Then
                            cn.Close()
                        End If
                        cm.Dispose()
                        DS = Nothing
                        Return True
                    End If
                Next
                DS = Nothing
                If Not cn.State = ConnectionState.Closed Then
                    cn.Close()
                End If
                cm.Dispose()
                Return False
            Else
                DS = Nothing
                If Not cn.State = ConnectionState.Closed Then
                    cn.Close()
                End If
                cm.Dispose()
                Return False
            End If
        Catch e As Exception
            Throw e
        Finally
            DS = Nothing
            If Not cn.State = ConnectionState.Closed Then
                cn.Close()
            End If
            cm.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Función que comprueba si se cumple la vigencia de la contrasenya de un usuario
    ''' </summary>
    ''' <param name="USU">Codigo de usuario</param>
    ''' <returns>Una variable booleana que devolvera true si se cumple la vigencia y false en caso contrario</returns>
    ''' <remarks>
    ''' Llamada desde: FSNServer/Root/CumpleVigenciaPWD
    ''' Tiempo máximo: 0,2 seg</remarks>
    Public Function ComprobarVigenciaPWD(ByVal USU As String) As Boolean
        Authenticate()
        Dim FechaActual As Date
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Dim Param As New SqlParameter
        Dim FechaPWD As Date
        Dim Edad_min_pwd As Integer
        Dim VigenciaPWD As Boolean
        Try
            Param = cm.Parameters.AddWithValue("@USU", USU)
            cm.Connection = cn
            cm.CommandText = "SELECT FECPWD FROM USU WITH (NOLOCK) WHERE COD =@USU"
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            FechaPWD = dr.Tables(0).Rows(0).Item("FECPWD")
        Catch e As Exception
            Throw e
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
        Try
            cm = New SqlCommand
            da = New SqlDataAdapter
            dr = New DataSet
            cm.Connection = cn
            cm.CommandText = "SELECT EDAD_MIN_PWD FROM PARGEN_GEST WITH (NOLOCK)"
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Edad_min_pwd = dr.Tables(0).Rows(0).Item("EDAD_MIN_PWD")
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
        If Edad_min_pwd = 0 Then
            Return True
            Exit Function
        End If
        Dim FechaAux As Date
        FechaAux = DateAdd("d", Edad_min_pwd, FechaPWD)
        FechaActual = Date.Now
        If FechaAux <= FechaActual Then
            VigenciaPWD = True
        Else
            VigenciaPWD = False
        End If
        Return VigenciaPWD
        Return True
    End Function
#End Region
#End Region
#Region "Dictionary data access methods "
    ''' <summary>
    ''' Carga los textos de un módulo en un dataset.
    ''' </summary>
    ''' <param name="iModule">Módulo de textos a recuperar</param>
    ''' <param name="sLanguage">Idioma</param>
    ''' <returns>Un Dataset con una tabla de dos columnas (ID, con el ID del término y TEXT_[idioma] con el texto)</returns>
    ''' <remarks>Tiempo estimado: 0.0 segundos (sin autenticación de usuario)</remarks>
    Public Function Dictionary_GetData(ByVal iModule As Integer, ByVal sLanguage As String) As Data.DataSet
        Return Me.Dictionary_GetData(iModule, sLanguage, "", "")
    End Function
    ''' <summary>
    ''' Carga los textos de un módulo en un dataset.
    ''' </summary>
    ''' <param name="iModule">Módulo de textos a recuperar</param>
    ''' <param name="sLanguage">Idioma</param>
    ''' <param name="Aplicacion">Aplicación para la que se recuperan los textos. Miembro de la enumeración FSNLibrary.FSNTipos.Aplicacion</param>
    ''' <returns>Un Dataset con una tabla de dos columnas (ID, con el ID del término y TEXT_[idioma] con el texto)</returns>
    ''' <remarks>Tiempo estimado: 0.0 segundos (sin autenticación de usuario)</remarks>
    Public Function Dictionary_GetData(ByVal iModule As Integer, ByVal sLanguage As String, ByVal Aplicacion As TiposDeDatos.Aplicaciones) As Data.DataSet
        Return Me.Dictionary_GetData(iModule, sLanguage, "", "", Aplicacion)
    End Function
    ''' <summary>
    ''' Carga los textos de un módulo en un dataset.
    ''' </summary>
    ''' <param name="iModule">Módulo de textos a recuperar</param>
    ''' <param name="sLanguage">Idioma</param>
    ''' <param name="UserCode">Código de usuario</param>
    ''' <param name="UserPassword">Contraseña</param>
    ''' <returns>Un Dataset con una tabla de dos columnas (ID, con el ID del término y TEXT_[idioma] con el texto)</returns>
    ''' <remarks>Tiempo estimado: 0.0 segundos (sin autenticación de usuario)</remarks>
    Public Function Dictionary_GetData(ByVal iModule As Integer, ByVal sLanguage As String, ByVal UserCode As String, ByVal UserPassword As String) As Data.DataSet
        Return Me.Dictionary_GetData(iModule, sLanguage, "", "", TiposDeDatos.Aplicaciones.PM)
    End Function
	''' <summary>
	''' Carga los textos de un mÃ³dulo en un dataset.
	''' </summary>
	''' <param name="iModule">MÃ³dulo de textos a recuperar</param>
	''' <param name="sLanguage">Idioma</param>
	''' <param name="UserCode">CÃ³digo de usuario</param>
	''' <param name="UserPassword">ContraseÃ±a</param>
	''' <param name="Aplicacion">AplicaciÃ³n para la que se recuperan los textos. Miembro de la enumeraciÃ³n FSNLibrary.FSNTipos.Aplicacion</param>
	''' <returns>Un Dataset con una tabla de dos columnas (ID, con el ID del tÃ©rmino y TEXT_[idioma] con el texto)</returns>
	''' <remarks>Tiempo estimado: 0.0 segundos (sin autenticaciÃ³n de usuario)</remarks>
	Public Function Dictionary_GetData(ByVal iModule As Integer, ByVal sLanguage As String, ByVal UserCode As String, ByVal UserPassword As String, ByVal Aplicacion As TiposDeDatos.Aplicaciones) As Data.DataSet
		Dim cn As New SqlConnection(mDBConnection)
		Dim cm As New SqlCommand
		Dim dr As New DataSet
		Dim da As New SqlDataAdapter
		Try
			cn.Open()
			cm.Connection = cn

			Select Case Aplicacion
				Case TiposDeDatos.Aplicaciones.PM, TiposDeDatos.Aplicaciones.QA
					cm.CommandText = "FSWS_GETDICTIONARYDATA"
				Case TiposDeDatos.Aplicaciones.EP
					cm.CommandText = "FSEP_GETDICTIONARYDATA"
			End Select
			cm.CommandType = CommandType.StoredProcedure
			cm.Parameters.AddWithValue("@MODULEID", iModule And Aplicacion)
			cm.Parameters.AddWithValue("@LANGUAGE", sLanguage)
			da.SelectCommand = cm
			da.Fill(dr)
			Return dr
		Catch e As Exception
			Throw e
		Finally
			cn.Dispose()
			cm.Dispose()
			da.Dispose()
		End Try
	End Function
	''' <summary>
	''' Funcion que devuelve el texto que se corresponde con un modulo, termino y en el lenguaje especifico
	''' </summary>
	''' <param name="iModule">Identificador del módulo</param>
	''' <param name="iTexto">Identificador del término</param>
	''' <param name="sLanguage">Idioma del texto a devolver</param>
	''' <returns>Una variable de tipo string con el texto buscado</returns>
	''' <remarks>
	''' Llamada desde: FSNServer/Dictionary/Texto
	''' Tiempo máximo: 0,1 seg</remarks>
	Public Function Dictionary_GetText(ByVal iModule As Integer, ByVal iTexto As Integer, ByVal sLanguage As String) As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cn.Open()
            cm.Connection = cn
            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = "FSWS_GETTEXTO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@MODULEID", iModule)
            cm.Parameters.AddWithValue("@TERMINOID", iTexto)
            cm.Parameters.AddWithValue("@LANGUAGE", sLanguage)
            Return DBNullToSomething(cm.ExecuteScalar())
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
#End Region
#Region "Error data access methods"
    ''' <summary>
    ''' Procedimiento que crea un objeto de tipo error
    ''' </summary>
    ''' <param name="sPagina">Página donde se ha generado el error</param>
    ''' <param name="sUsuario">Usuario que ha generado el error</param>
    ''' <param name="sEx_FullName">Nombre de la excepcion generada</param>
    ''' <param name="sEx_Message">Mensaje de la excepcion generada</param>
    ''' <param name="sEx_StackTrace">Datos de la pila de errores</param>
    ''' <param name="sSv_Query_String">Datos del Query String pasado</param>
    ''' <param name="sSv_Http_User_Agent">Tipo de excepcion devuelta</param>
    ''' <remarks>
    ''' Llamada desde: PMServer/Errores/Create
    ''' Tiempo máximo: 0,4 seg</remarks>
    Public Function Errores_Create(ByVal sPagina As String, ByVal sUsuario As String, ByVal sEx_FullName As String, ByVal sEx_Message As String, ByVal sEx_StackTrace As String, ByVal sSv_Query_String As String, ByVal sSv_Http_User_Agent As String) As Long
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cn.Open()
            With cm
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "FSPM_INSERTAR_ERROR"
                .Parameters.AddWithValue("@PAGINA", IIf(String.IsNullOrEmpty(sPagina), String.Empty, sPagina))
                .Parameters.AddWithValue("@USUARIO", IIf(String.IsNullOrEmpty(sUsuario), String.Empty, sUsuario))
                .Parameters.AddWithValue("@FULLNAME", IIf(String.IsNullOrEmpty(sEx_FullName) OrElse sEx_FullName = "", String.Empty, sEx_FullName))
                .Parameters.AddWithValue("@MESSAGE", IIf(String.IsNullOrEmpty(sEx_Message), String.Empty, sEx_Message))
                .Parameters.AddWithValue("@STACKTRACE", IIf(String.IsNullOrEmpty(sEx_StackTrace) OrElse sEx_StackTrace = "", String.Empty, sEx_StackTrace))
                .Parameters.AddWithValue("@QUERYSTRING", IIf(String.IsNullOrEmpty(sSv_Query_String) OrElse sSv_Query_String = "", String.Empty, sSv_Query_String))
                .Parameters.AddWithValue("@USERAGENT", IIf(String.IsNullOrEmpty(sSv_Http_User_Agent) OrElse sSv_Http_User_Agent = "", String.Empty, sSv_Http_User_Agent))
                .Parameters.AddWithValue("@ID", SqlDbType.Int).Direction = ParameterDirection.Output
                .ExecuteNonQuery()
                Errores_Create = DBNullToSomething(cm.Parameters("@ID").Value)
            End With
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' - Los errores no solo se producen en paginas tambien en servicios. En este caso no hay pagina
    '''   y en consecuencia muchos de los campos a grabar en la tabla Errores tienen poco sentido, tiene
    '''   mas sentido grabar datos relacionados con el fallo de bbdd.
    ''' - Tambien podría interesar grabar en la tabla Errores los errores en bbdd producidos en paginas
    '''   (tal vez, si deja de interesar escribir todo en log) para este caso esta el parametro opcional Usuario.
    ''' </summary>
    ''' <param name="DondeError">String libre indicador de donde se produjo el error</param>
    ''' <param name="MsgError">Descripción del error producido</param>
    ''' <param name="UsuarioError">Usuario conectado en el momento de producirse el error</param>
    ''' <param name="usercode">usuario</param>
    ''' <param name="userpassword">contraseña</param>
    ''' <returns>Id del registro insertado</returns>
    ''' <remarks>Llamada desde: Errores.vb/Create    Updatar_NotificarObtenerCertificadosExpirados ;Tiempo maximo:0,1</remarks>
    Public Function Errores_BD(ByVal DondeError As String, ByVal MsgError As String, Optional ByVal UsuarioError As String = "", Optional ByVal usercode As String = "", Optional ByVal userpassword As String = "") As Long
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cn.Open()
            With cm
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "FSPM_INSERTAR_ERROR"
                .Parameters.AddWithValue("@PAGINA", IIf(String.IsNullOrEmpty(DondeError), String.Empty, DondeError))
                .Parameters.AddWithValue("@USUARIO", IIf(String.IsNullOrEmpty(UsuarioError) OrElse UsuarioError = "", String.Empty, UsuarioError))
                .Parameters.AddWithValue("@FULLNAME", String.Empty)
                .Parameters.AddWithValue("@MESSAGE", IIf(String.IsNullOrEmpty(MsgError), String.Empty, MsgError))
                .Parameters.AddWithValue("@STACKTRACE", String.Empty)
                .Parameters.AddWithValue("@QUERYSTRING", String.Empty)
                .Parameters.AddWithValue("@USERAGENT", String.Empty)
                .Parameters.AddWithValue("@ID", SqlDbType.Int).Direction = ParameterDirection.Output
                .ExecuteNonQuery()

                Errores_BD = DBNullToSomething(cm.Parameters("@ID").Value)
            End With
            'Tratamiento de errores:
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
#End Region
#Region "Idiomas data access methods "
    ''' <summary>
    ''' Función que devuelve un dataset con los idiomas de la aplicación
    ''' </summary>
    ''' <returns>Dataset con el codigo y la denominacion de los idiomas</returns>
    ''' <remarks>
    ''' Llamada desde: El evento Load de Opciones.aspx.vb
    ''' Tiempo: 0 seg
    ''' </remarks>
    Public Function Idiomas_Load() As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SELECT COD,DEN, OFFSET,APLICACION,LANGUAGETAG FROM IDIOMAS WITH (NOLOCK) WHERE APLICACION=1"
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
#End Region
#Region "Listados data access methods "
    ''' <summary>
    ''' Función que devuelve un dataset con una tabla que contendrá los datos solicitudos para el parámetro
    ''' </summary>
    ''' <param name="sTabla">Nombre de la tabla a la que debemos acceder en la consulta</param>
    ''' <param name="sCampo1">Nombre del primer campo a traer de la tabla </param>
    ''' <param name="sCampo2">Nombre del segundo campo a traer de la tabla (puede ser vacío)</param>
    ''' <returns>Un dataset con la tabla y los valores</returns>
    ''' <remarks>
    ''' Llamada desde: La función CargarParametrosEnPagina de Report.aspx
    ''' Tiempo máximo: Variable en función de la tabla a acceder
    ''' </remarks>
    Function Listados_Cargar_DatosTablaIndefinida(ByVal sTabla As String, ByVal sCampo1 As String, ByVal sCampo2 As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Dim sql As String
        Using cn
            cn.Open()
            cm.Connection = cn
            sql = "SELECT " & sCampo1

            If sCampo2 <> "" Then
                'Si sCampo2 no está vacío, lo sacamos concatenado con sCampo1
                sql = sql & ", " & "CONVERT(NVARCHAR," & sCampo1 & ") + '  -  ' + CONVERT(NVARCHAR," & sCampo2 & ") AS " & sCampo2
            End If
            sql = sql & " FROM " & sTabla & " WITH (NOLOCK)"
            sql = sql & " ORDER BY " & sCampo1

            cm.CommandText = sql
            cm.CommandType = CommandType.Text

            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        End Using
    End Function
#End Region
#Region "Monedas data access methods "
    Public Function Monedas_Get(ByVal sIdi As String, ByVal sCod As String, ByVal sDen As String, ByVal bCoincid As Boolean, Optional ByVal bAllDatos As Boolean = False) As DataSet
        'jbg160309
        'Que hace: 
        '   Devolver en un dataset 2(cod + den ) o todos los campos de la tabla monedas
        'Param. Entrada:
        '   sIdi    idioma
        '   sCod    codigo de moneda
        '   sDen    denominación de moneda
        '   bCoincid    coincidencia total o por like 'sCod%'
        '   bAllDatos   opcional.2(cod + den ) o todos los campos de la tabla
        'Param. Salida:
        '   Lo dicho en "que hace", un recordset
        'Llamada desde: 
        '   Todo combo de monedas. Carga de monedas para comprobar/traer datos...Vamos N aspx, pmserver, pmdatab...
        'Tiempo:    
        '   Instantaneo
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSWS_MONEDAS"
            cm.Parameters.AddWithValue("@COD", sCod)
            cm.Parameters.AddWithValue("@DEN", sDen)
            cm.Parameters.AddWithValue("@COINCID", IIf(bCoincid, 1, 0))
            cm.Parameters.AddWithValue("@IDIOMA", sIdi)
            cm.Parameters.AddWithValue("@ALLDATOS", IIf(bAllDatos, 1, 0))

            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Devuelve código y denominación de la moneda central
    ''' </summary>
    ''' <param name="sIdi">Idioma en la que se devuelve la denominación de la moneda</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Monedas_Get_MonedaCentral(ByVal sIdi As String) As DataRow
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataTable
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            With cm
                .Connection = cn
                .CommandText = "FSWS_GET_MONEDACENTRAL"
                .Parameters.AddWithValue("@IDIOMA", sIdi)
                .CommandType = CommandType.StoredProcedure
            End With

            da.SelectCommand = cm
            da.Fill(dr)

            Return dr.Rows(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
#End Region
#Region "Personalización data access methods"
    ''' <summary>
    ''' Devuelve el número de filas del almacén de datos subyacente que existen dentro del ámbito especificado.
    ''' </summary>
    ''' <param name="Aplicacion">Nombre de la aplicación configurada para el proveedor.</param>
    ''' <param name="Path">La ruta de acceso para información de personalización que se va a utilizar como clave de almacenamiento de datos.</param>
    ''' <param name="FechaInactividad">Especifica una fecha máxima de antigüedad de los datos</param>
    ''' <param name="Usuario">El nombre de usuario para información de personalización que se va a utilizar como clave.</param>
    ''' <returns>El número de filas del almacén de datos subyacente que existen para los parámetros especificados.</returns>
    ''' <remarks>Utilizado en: FSNServer.ProveedorPersonalizacion.GetCountOfState</remarks>
    Public Function Personalizacion_NumRegs(ByVal Aplicacion As String, ByVal Path As String, ByVal FechaInactividad As Date, ByVal Usuario As String) As Integer
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Using cn
            Dim cm As SqlCommand = New SqlCommand("FSN_PERSONALIZACION_NUMREGS", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@COUNT", SqlDbType.Int)
            cm.Parameters("@COUNT").Direction = ParameterDirection.Output
            cm.Parameters.Add("@APLICACION", SqlDbType.NVarChar, 256)
            cm.Parameters("@APLICACION").Value = Aplicacion
            cm.Parameters.Add("@PATH", SqlDbType.NVarChar, 256)
            cm.Parameters("@PATH").Value = Path
            cm.Parameters.Add("@USUARIO", SqlDbType.VarChar)
            If TipoDeAcceso_Get() = TiposDeAutenticacion.Windows Then
                Dim UserIdentityInfo As System.Security.Principal.WindowsIdentity
                UserIdentityInfo = System.Security.Principal.WindowsIdentity.GetCurrent()
                Usuario = UserIdentityInfo.Name.Split("\")(1)
                cm.Parameters("@USUARIO").Value = Usuario
            Else
                cm.Parameters("@USUARIO").Value = Usuario
            End If
            cm.Parameters.Add("@FECHAINACTIVO", SqlDbType.DateTime)
            cm.Parameters("@FECHAINACTIVO").Value = FechaInactividad
            cn.Open()
            cm.ExecuteNonQuery()
            cn.Close()
            Return cm.Parameters("@COUNT").Value
        End Using
    End Function
    ''' <summary>
    ''' Carga datos de personalización sin formato del almacén de datos subyacente.
    ''' </summary>
    ''' <param name="Aplicacion">Nombre de la aplicación configurada para el proveedor.</param>
    ''' <param name="Path">La ruta de acceso para información de personalización que se va a utilizar como clave de almacenamiento de datos.</param>
    ''' <param name="Usuario">El nombre de usuario para información de personalización que se va a utilizar como clave.</param>
    ''' <returns>Los datos devueltos para el usuario.</returns>
    ''' <remarks>Utilizado en: FSNServer.ProveedorPersonalizacion.LoadPersonalizationBlobs</remarks>
    Public Function Personalizacion_GetUserData(ByVal Aplicacion As String, ByVal Path As String, ByVal Usuario As String) As Byte()
        Authenticate()
        Dim arrData() As Byte
        Dim cn As New SqlConnection(mDBConnection)
        Using cn
            Dim cm As SqlCommand = New SqlCommand("FSN_PERSONALIZACION_GET_USERDATA", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@APLICACION", SqlDbType.NVarChar, 256)
            cm.Parameters("@APLICACION").Value = Aplicacion
            cm.Parameters.Add("@PATH", SqlDbType.NVarChar, 256)
            cm.Parameters("@PATH").Value = Path
            cm.Parameters.Add("@USUARIO", SqlDbType.VarChar)
            If TipoDeAcceso_Get() = TiposDeAutenticacion.Windows Then
                Dim UserIdentityInfo As System.Security.Principal.WindowsIdentity
                UserIdentityInfo = System.Security.Principal.WindowsIdentity.GetCurrent()
                Usuario = UserIdentityInfo.Name.Split("\")(1)
                cm.Parameters("@USUARIO").Value = Usuario
            Else
                cm.Parameters("@USUARIO").Value = Usuario
            End If
            cn.Open()
            arrData = DBNullToSomething(cm.ExecuteScalar())
            cn.Close()
            Return arrData
        End Using
    End Function
    ''' <summary>
    ''' Guarda datos de personalización sin formato en el almacén de datos subyacente.
    ''' </summary>
    ''' <param name="Aplicacion">Nombre de la aplicación configurada para el proveedor.</param>
    ''' <param name="Path">La ruta de acceso para información de personalización que se va a utilizar como clave de almacenamiento de datos.</param>
    ''' <param name="Usuario">El nombre de usuario para información de personalización que se va a utilizar como clave.</param>
    ''' <param name="UserData">La matriz de bytes de datos que se va a guardar.</param>
    ''' <remarks>Utilizado en: FSNServer.ProveedorPersonalizacion.SavePersonalizationBlob</remarks>
    Public Sub Personalizacion_SetUserData(ByVal Aplicacion As String, ByVal Path As String, ByVal Usuario As String, ByVal UserData() As Byte)
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Using cn
            Dim cm As SqlCommand = New SqlCommand("FSN_PERSONALIZACION_SET_USERDATA", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@APLICACION", SqlDbType.NVarChar, 256)
            cm.Parameters("@APLICACION").Value = Aplicacion
            cm.Parameters.Add("@PATH", SqlDbType.NVarChar, 256)
            cm.Parameters("@PATH").Value = Path
            cm.Parameters.Add("@USUARIO", SqlDbType.VarChar)
            If TipoDeAcceso_Get() = TiposDeAutenticacion.Windows Then
                Dim UserIdentityInfo As System.Security.Principal.WindowsIdentity
                UserIdentityInfo = System.Security.Principal.WindowsIdentity.GetCurrent()
                Usuario = UserIdentityInfo.Name.Split("\")(1)
                cm.Parameters("@USUARIO").Value = Usuario
            Else
                cm.Parameters("@USUARIO").Value = Usuario
            End If
            cm.Parameters.Add("@SETTINGS", SqlDbType.Image)
            cm.Parameters("@SETTINGS").Value = UserData
            cn.Open()
            cm.ExecuteNonQuery()
            cn.Close()
        End Using
    End Sub
    ''' <summary>
    ''' Elimina datos de personalización sin formato del almacén de datos subyacente.
    ''' </summary>
    ''' <param name="Aplicacion">Nombre de la aplicación configurada para el proveedor.</param>
    ''' <param name="Path">La ruta de acceso para información de personalización que se va a utilizar como clave de almacenamiento de datos.</param>
    ''' <param name="Usuario">El nombre de usuario para información de personalización que se va a utilizar como clave.</param>
    ''' <remarks>Utilizado en: FSNServer.ProveedorPersonalizacion.ResetPersonalizationBlob</remarks>
    Public Sub Personalizacion_DelUserData(ByVal Aplicacion As String, ByVal Path As String, ByVal Usuario As String)
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Using cn
            Dim cm As SqlCommand = New SqlCommand("FSN_PERSONALIZACION_DEL_USERDATA", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@APLICACION", SqlDbType.NVarChar, 256)
            cm.Parameters("@APLICACION").Value = Aplicacion
            cm.Parameters.Add("@PATH", SqlDbType.NVarChar, 256)
            cm.Parameters("@PATH").Value = Path
            cm.Parameters.Add("@USUARIO", SqlDbType.VarChar)
            If TipoDeAcceso_Get() = TiposDeAutenticacion.Windows Then
                Dim UserIdentityInfo As System.Security.Principal.WindowsIdentity
                UserIdentityInfo = System.Security.Principal.WindowsIdentity.GetCurrent()
                Usuario = UserIdentityInfo.Name.Split("\")(1)
                cm.Parameters("@USUARIO").Value = Usuario
            Else
                cm.Parameters("@USUARIO").Value = Usuario
            End If
            cn.Open()
            cm.ExecuteNonQuery()
            cn.Close()
        End Using
    End Sub
#End Region
#Region "Personas data access methods"
    ''' <summary>Procedimiento que carga los datos de una persona</summary>
    ''' <param name="sCod">Código de la persona</param>   
    ''' <param name="bBajaLog">Incluir o no bajas lógicas</param>
    ''' <remarks>Llamada desde: FSNServer.Persona.LoadData</remarks>
    Public Function Persona_Load(ByVal sCod As String, Optional ByVal bBajaLog As Boolean = False) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSWS_DEVOLVER_DATOS_PERSONA"
            cm.Parameters.AddWithValue("@COD", sCod)
            cm.Parameters.AddWithValue("@BAJALOG", bBajaLog)

            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(ds)
            Return ds.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Procedimiento que carga los datos de las personas
    ''' </summary>
    ''' <param name="sCod">Codigo de la persona</param>
    ''' <param name="sNom">Nombre de la persona</param>
    ''' <param name="sApe">Apellido de la persona</param>
    ''' <param name="sUON1">Codigo de la unidad organizativa de nivel 1 de la persona</param>
    ''' <param name="sUON2">Codigo de la unidad organizativa de nivel 2 de la persona</param>
    ''' <param name="sUON3">Codigo de la unidad organizativa de nivel 3 de la persona</param>
    ''' <param name="sDEP">Departamento de la persona</param>
    ''' <param name="bPM">Variable boolenaa que indica si la persona tiene acceso a PM o no</param>
    ''' <param name="bSoloUsuariosQa">Variable booleana que indica si sólo se deben cargar usuarios de QA</param>
    ''' <param name="bCtlBajaLog">si se controla o no la bajalogica</param> 
    ''' <param name="lDesdeFila">Posicion de la primera fila a partir de la cual se debe mostrar (no incluida)</param>
    ''' <param name="lNumFilas">Numero de filas a extraer de la BDD</param>
    ''' <param name="sUO">Si hay UOns en el formulario, sus valores</param> 
    ''' <param name="sMaterial">Si hay materiales en el formulario, sus valores</param>
    ''' <param name="bCM">Variable boolena que indica si la persona tiene acceso a CM o no</param> 
    ''' <remarks>
    ''' Llamada desde: PmServer/Personas/Load
    ''' Tiempo máximo: 0,5 seg</remarks>
    Public Function Personas_Load(Optional ByVal sCod As String = Nothing, Optional ByVal sNom As String = Nothing, Optional ByVal sApe As String = Nothing, Optional ByVal sUON1 As String = Nothing,
                                  Optional ByVal sUON2 As String = Nothing, Optional ByVal sUON3 As String = Nothing, Optional ByVal sDEP As String = Nothing, Optional ByVal bPM As Boolean = False,
                                  Optional ByVal bSoloUsuariosQa As Boolean = False, Optional ByVal bCtlBajaLog As Boolean = True, Optional ByVal lDesdeFila As Nullable(Of Long) = Nothing,
                                  Optional ByVal lNumFilas As Nullable(Of Long) = Nothing, Optional ByVal sEqp As String = Nothing, Optional ByVal sUO As String = Nothing,
                                  Optional ByVal sMaterial As String = Nothing, Optional ByVal bCM As Boolean = False) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSWS_PERSONAS"
            cm.Parameters.AddWithValue("@COD", sCod)
            cm.Parameters.AddWithValue("@NOM", sNom)
            cm.Parameters.AddWithValue("@APE", sApe)
            cm.Parameters.AddWithValue("@UON1", sUON1)
            cm.Parameters.AddWithValue("@UON2", sUON2)
            cm.Parameters.AddWithValue("@UON3", sUON3)
            cm.Parameters.AddWithValue("@DEP", sDEP)
            If bPM Then cm.Parameters.AddWithValue("@PM", 1)
            If bSoloUsuariosQa Then cm.Parameters.AddWithValue("@SOLOQA", 1)
            cm.Parameters.AddWithValue("@BAJALOG", IIf(bCtlBajaLog, 1, 0))
            cm.Parameters.AddWithValue("@FILAS", lNumFilas)
            cm.Parameters.AddWithValue("@DESDEFILA", lDesdeFila)
            If Not sEqp Is Nothing Then cm.Parameters.AddWithValue("@EQP", sEqp)
            If Not sUO Is Nothing Then cm.Parameters.AddWithValue("@SOL_UON", sUO)
            If Not sMaterial Is Nothing Then cm.Parameters.AddWithValue("@SOL_MAT", sMaterial)
            If bCM Then cm.Parameters.AddWithValue("@CM", 1)
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
#End Region
#Region "User data access methods "
    Public Function User_LoadListados(ByVal Usu As String, Optional ByVal bPM As Boolean = False, Optional ByVal bQA As Boolean = False, Optional ByVal bEP As Boolean = False) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSPM_DEVOLVER_LIST_PERSONALIZADOS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", Usu)
            cm.Parameters.AddWithValue("@PM", bPM)
            cm.Parameters.AddWithValue("@QA", bQA)
            cm.Parameters.AddWithValue("@EP", bEP)
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function User_LoadUserData(ByVal sCod As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = "FSWS_USU"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@COD", sCod)
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function

    Public Function User_LoadUserDataCN(ByVal sCod As String) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = "CN_OBTENER_USUARIO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@COD", sCod)
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Para determinar si hay acceso al panel tanto en VAR_CAL_USU como en USU_UNQA debe el usuario tener registros.
    ''' </summary>
    ''' <param name="sCod">Codigo de la persona que se ha validado en la aplicacion.</param>   
    ''' <returns>Un Dataset con una tabla de dos columnas (FSQA_PUNTPROVE, con el count de la tabla VAR_CAL_USU 
    ''' y FSQA_PUNTPROVEUNQA con el count de la tabla USU_UNQA)</returns>
    ''' <remarks>Llamada desde:User.vb --> PanelCalidadAccesible ; Tiempo maximo: 0,1</remarks>
    Public Function User_LoadUserDataPanel(ByVal sCod As String) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = "FSQA_ACCESO_PANEL_CALIDAD"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@COD", sCod)
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Procedimiento que guarda las opciones de usuario
    ''' </summary>
    ''' <param name="sCod">Codigo de usuario</param>
    ''' <param name="sThousanFmt">Formato de miles</param>
    ''' <param name="sDecimalFmt">Formato decimal</param>
    ''' <param name="iPrecisionFmt">Precision decimal</param>
    ''' <param name="sDateFmt">Formato de fechas</param>
    ''' <param name="iTipoEmail">Tipo de correo electronico</param>
    ''' <param name="sIdioma">Idioma de la aplicación</param>
    ''' <remarks>Llamada desde: FSNServer/USer/SaveUserData Tiempo máximo: 0,15 seg</remarks>
    Public Sub User_SaveUserData(ByVal sCod As String, ByVal sThousanFmt As String, ByVal sDecimalFmt As String, ByVal iPrecisionFmt As Integer, ByVal sDateFmt As String, ByVal iTipoEmail As Integer,
                                 ByVal sIdioma As String, Optional ByVal bProvMat As Boolean = Nothing)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim da As New SqlDataAdapter
        Dim sql As String
        cn.Open()
        cm = New SqlCommand("BEGIN TRANSACTION", cn)
        cm.ExecuteNonQuery()
        Try

            Dim sqlProvMat As String
            If Not IsNothing(bProvMat) Then
                sqlProvMat = ", MOSTRAR_DEF_PROV_MAT=" & IIf(bProvMat, 1, 0) & " "
            Else
                sqlProvMat = ""
            End If

            sql = "UPDATE USU SET DECIMALFMT='" & sDecimalFmt & "'," _
                 & "   THOUSANFMT=" & If(sThousanFmt = "", "NULL", "'" & sThousanFmt & "'") & "," _
                & "   DATEFMT='" & sDateFmt & "', " _
                & "   PRECISIONFMT=" & iPrecisionFmt & ", " _
                & "   TIPOEMAIL=" & iTipoEmail & ", " _
                & "   IDIOMA='" & sIdioma & "' " _
                & sqlProvMat _
               & "WHERE COD ='" & sCod & "' "

            cm = New SqlCommand(sql, cn)
            cm.ExecuteNonQuery()

            cm = New SqlCommand("COMMIT TRANSACTION", cn)
            cm.ExecuteNonQuery()
        Catch e As SqlException
            cm = New SqlCommand("ROLLBACK TRANSACTION", cn)
            cm.ExecuteNonQuery()
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Sub
    Public Function User_LoadUserPanelEscalacion(ByVal sCod As String) As Boolean
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = "FSQA_PANEL_ESCALACION_PROVEEDORES_ACCESO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@COD", sCod)
            da.SelectCommand = cm

            User_LoadUserPanelEscalacion = cm.ExecuteScalar
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
#Region "EP User"
    ''' <summary>
    ''' Devuelve los datos actuales de la cesta del usuario especificado
    ''' </summary>
    ''' <param name="Usuario">Código de usuario</param>
    ''' <returns>Un datarow con el número de artículos("NUMARTICULOS"), el importe total("IMPORTE"),
    ''' el código de moneda ("MON") y el cambio actual("EQUIV")</returns>
    ''' <remarks></remarks>
    Public Function User_InfoCesta(ByVal Usuario As String) As DataRow
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Using cn
            Dim cm As SqlCommand = New SqlCommand("FSEP_INFOCESTA", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@USU", SqlDbType.VarChar, 50)
            cm.Parameters("@USU").Value = Usuario
            Dim da As New SqlDataAdapter
            Dim ds As New DataSet
            da.SelectCommand = cm
            da.Fill(ds)
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)
            Else
                Return Nothing
            End If
        End Using
    End Function
    ''' <summary>
    ''' Compruba con el codigo de persona, si ese usuario esta en algun flujo de una solicitud de tipo pedido, ya sea como persona, como persona dentro de un departamento o persona dentro de una UON
    ''' </summary>
    ''' <param name="codPer">codigo de persona</param>
    ''' <returns>True si es aprobador, false si no lo es</returns>
    ''' <remarks></remarks>
    Public Function User_EsAprobadorSolicitudPedidoEP(ByVal codPer As String) As Boolean

        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter
        Try
            cm.Connection = cn
            cm.CommandText = "FSEP_COMPROBAR_ES_APROBADOR"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@CODPER", codPer)
            cm.Parameters.AddWithValue("@TIPOSOL", TipoDeSolicitud.SolicitudDePedidoCatalogo)
            cn.Open()
            User_EsAprobadorSolicitudPedidoEP = cm.ExecuteScalar
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
        Return User_EsAprobadorSolicitudPedidoEP
    End Function

    ''' <summary>
    ''' Comprueba con el codigo de persona, si ese usuario esta en algun flujo de una solicitud de tipo pedido abierto, ya sea como persona, como persona dentro de un departamento o persona dentro de una UON
    ''' </summary>
    ''' <param name="codPer">codigo de persona</param>
    ''' <returns>True si es aprobador, false si no lo es</returns>
    ''' <remarks></remarks>
    Public Function User_EsAprobadorSolicitudContraPedidoAbierto(ByVal codPer As String) As Boolean

        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter
        Try
            cm.Connection = cn
            cm.CommandText = "FSEP_COMPROBAR_ES_APROBADOR"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@CODPER", codPer)
            cm.Parameters.AddWithValue("@TIPOSOL", TipoDeSolicitud.SolicitudDePedidoContraAbierto)
            cn.Open()
            User_EsAprobadorSolicitudContraPedidoAbierto = cm.ExecuteScalar
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
        Return User_EsAprobadorSolicitudContraPedidoAbierto
    End Function
    ''' Revisado por: blp. Fecha: 07/03/2012
    ''' <summary>
    ''' Función que modifica en BBDD los parámetros de usuario introducidos a traves de la página ParametrosEP.aspx
    ''' </summary>
    ''' <param name="Dest">Destino Seleccionado</param>
    ''' <param name="mostrarimgart">Si se muestra o no la imagen del artículo</param>
    ''' <param name="MostrarAtrib">Si se muestran o no los atributos de articulo</param>
    ''' <param name="MostrarCantMin">Si se muestran o no las cantidades mínimas de artículo</param>
    ''' <param name="MostrarCodProve">Si se muestra o no el código de proveedor</param>
    ''' <param name="MostrarCodArt">Si se muestra o no el código de artículo</param>
    ''' <param name="UsuCod">Código de usuario a actualizar</param>
    ''' <param name="MostrarBarraSM">Si se muestra o no la barra de porcentajes</param>
    ''' <param name="bSeguimientoVerPedidosUsuario">Para indicar en el seguimiento de pedidos, si el usuario quiere cargar unicamente sus pedidos de su CC</param>
    ''' <param name="bSeguimientoVerPedidosCCImputables">Para indicar en el seguimiento de pedidos, si el usuario quiere cargar los pedidos de su CC y que no haya emitido el </param>
    ''' <param name="bCargarCantPtesRecep">True-> rellenar en recepciones las cantidades pendientes de recepcionar en los textbox de cantidad</param>
    ''' <param name="bRecepcionVerPedidosUsuario">Para indicar si en la pantalla de recepción de pedidos, el usuario quiere que en la carga inicial salgan sus pedidos</param>
    ''' <param name="bRecepcionVerPedidosCCImputables">Para indicar si en la pantalla de recepción de pedidos, el usuario quiere que en la carga inicial salgan los pedidos de sus centros de coste (en los que el receptor no sea él)</param>
    ''' <returns>Un valor entero que será 1 si todo ha ido bien y 0 si ha habido algún error</returns>
    ''' <remarks>
    ''' Llamada desde: El objeto User de la clase EPServer
    ''' Tiempo máximo: 0,1 seg</remarks>
    Public Function User_GuardarOpcionesUsuario(ByVal Dest As String, _
  ByVal mostrarimgart As Integer, ByVal MostrarAtrib As Integer, _
  ByVal MostrarCantMin As Integer, ByVal MostrarCodProve As Integer, _
  ByVal MostrarCodArt As Integer, ByVal UsuCod As String, _
  ByVal MostrarBarraSM As Integer, ByVal bSeguimientoVerPedidosUsuario As Boolean, ByVal bSeguimientoVerPedidosCCImputables As Boolean, ByVal bCargarCantPtesRecep As Boolean, ByVal bRecepcionVerPedidosUsuario As Boolean, ByVal bRecepcionVerPedidosCCImputables As Boolean) As Integer
        Authenticate()

        Dim sConsulta As String
        Dim Con As New SqlConnection(mDBConnection)
        Dim SqlCmd As New SqlCommand
        Dim SqlParam As New SqlParameter
        SqlParam = SqlCmd.Parameters.AddWithValue("@SEGUIMIENTOVERPEDIDOSUSUARIO", bSeguimientoVerPedidosUsuario)
        SqlParam = SqlCmd.Parameters.AddWithValue("@SEGUIMIENTOVERPEDIDOSCCIMP", bSeguimientoVerPedidosCCImputables)
        SqlParam = SqlCmd.Parameters.AddWithValue("@MOSTIMGART", mostrarimgart)
        SqlParam = SqlCmd.Parameters.AddWithValue("@MOSTATRIB", MostrarAtrib)
        SqlParam = SqlCmd.Parameters.AddWithValue("@MOSTCANTMIN", MostrarCantMin)
        SqlParam = SqlCmd.Parameters.AddWithValue("@MOSTCODPROVE", MostrarCodProve)
        SqlParam = SqlCmd.Parameters.AddWithValue("@MOSTCODART", MostrarCodArt)
        If Dest <> "" Then
            SqlParam = SqlCmd.Parameters.AddWithValue("@DEST", Dest)
        Else
            SqlParam = SqlCmd.Parameters.AddWithValue("@DEST", System.DBNull.Value)
        End If
        'SqlParam = SqlCmd.Parameters.AddWithValue("@DEST", Dest)
        SqlParam = SqlCmd.Parameters.AddWithValue("@FSEP_SM_BAR", MostrarBarraSM)
        SqlParam = SqlCmd.Parameters.AddWithValue("@COD", UsuCod)
        SqlParam = SqlCmd.Parameters.AddWithValue("@CARGAR_CANT_PEND_RECEP", IIf(bCargarCantPtesRecep, 1, 0))
        SqlParam = SqlCmd.Parameters.AddWithValue("@RECEPCIONVERPEDIDOSUSUARIO", bRecepcionVerPedidosUsuario)
        SqlParam = SqlCmd.Parameters.AddWithValue("@RECEPCIONVERPEDIDOSCCIMP", bRecepcionVerPedidosCCImputables)
        sConsulta = "UPDATE USU SET MOSTIMGART= @MOSTIMGART, " _
            & "   MOSTATRIB = @MOSTATRIB, " _
            & "   MOSTCANTMIN = @MOSTCANTMIN, " _
            & "   MOSTCODPROVE = @MOSTCODPROVE, " _
            & "   MOSTCODART = @MOSTCODART, " _
            & "   DEST=@DEST " _
            & ", FSEP_SM_BAR=@FSEP_SM_BAR" _
            & ", FSEP_SEG_VER_PED_USU=@SEGUIMIENTOVERPEDIDOSUSUARIO" _
            & ", FSEP_SEG_VER_PED_CCIMP=@SEGUIMIENTOVERPEDIDOSCCIMP" _
            & ", CARGAR_CANT_PEND_RECEP=@CARGAR_CANT_PEND_RECEP" _
            & ", FSEP_RECEP_VER_PED_USU=@RECEPCIONVERPEDIDOSUSUARIO" _
            & ", FSEP_RECEP_VER_PED_CCIMP=@RECEPCIONVERPEDIDOSCCIMP" _
            & " WHERE COD=@COD"
        Con.Open()
        SqlCmd.CommandText = sConsulta
        SqlCmd.CommandType = CommandType.Text
        SqlCmd.Connection = Con
        Dim Resul As Integer
        Resul = SqlCmd.ExecuteNonQuery
        If Resul > 0 Then
            Return 0
        Else
            Return 1
        End If
    End Function
    ''' <summary>
    ''' Comprueba si el usuario es aprovisionador para las categorías dadas.
    ''' </summary>
    ''' <param name="codUsu">código del usuario</param>
    ''' <param name="idFav">Id del pedido favorito de cuyos artículos se va a comprobar si el usuario es aprovisionador</param>
    ''' <returns>
    ''' Es aprovisionador -> devuelve TRUE
    ''' No es aprovisionador -> devuelve FALSE
    ''' </returns>
    ''' <remarks></remarks>
    Public Function User_CanIssueItemsCategories(ByVal codUsu As String, ByVal idFav As Integer) As DataTable
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter

        Dim bRes As Boolean
        Try
            bRes = False

            cm.Connection = cn

            cm.CommandText = "FSEP_USU_COMPROBARAPROVISIONADOR"
            cm.CommandType = CommandType.StoredProcedure

            cm.Parameters.AddWithValue("@USU", codUsu)
            cm.Parameters.AddWithValue("@IDFAV", idFav)

            da.SelectCommand = cm
            da.Fill(ds)

            If ds.Tables(0) IsNot Nothing Then
                Return ds.Tables(0)
            Else
                Return Nothing
            End If
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
#End Region
#Region "PM User"
    ''' <summary>
    ''' Funcion que actualiza los parámetros de usuario de PM
    ''' </summary>
    ''' <param name="bMostrar">Variable boolena que determina si se debe mostrar el material del proveedor por defecto</param>
    ''' <param name="iNumFilas">Número de filas a cargar por página para el usuario</param>
    ''' <param name="sCod">Código de usuario</param>
    ''' <remarks>Llamada desde: Pmserver/User/Actualizar_ParametrosPM
    ''' Tiempo máximo: 0,35 seg</remarks>
    Public Sub User_Actualizar_ParametrosPM(ByVal sCod As String, ByVal bMostrar As Boolean, ByVal iNumFilas As Integer)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSPM_MODIFICAR_PARAMETROS_PM"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 60

            cm.Parameters.AddWithValue("@USU", sCod)
            cm.Parameters.AddWithValue("@MOSTRAR", bMostrar)
            cm.Parameters.AddWithValue("@NUMFILAS", iNumFilas)
            cm.ExecuteNonQuery()
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Actualiza la tabla usu indicando si hay presupuestos favoritos o no para el usuario
    ''' </summary>
    ''' <param name="sCod">Código del usuario</param>
    ''' <param name="iTipo">Tipo de presupuesto</param>
    ''' <param name="bAnyadirPresFav">Indicador de si tiene favorito o no(True/False)</param>
    ''' <remarks></remarks>
    Public Sub User_Actualizar_AnyadirPresFav(ByVal sCod As String, ByVal iTipo As Integer, ByVal bAnyadirPresFav As Boolean)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cn.Open()
            With cm
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "FSPM_ESTADO_ANYADIR_PRES_FAVORITO"
                .Parameters.AddWithValue("@TIPO", iTipo)
                .Parameters.AddWithValue("@ANYADIR", IIf(bAnyadirPresFav, 1, 0))
                .Parameters.AddWithValue("@USU", sCod)
                .ExecuteNonQuery()
            End With
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Actualiza la tabla usu indicando si hay proveedores favoritos o no para el usuario
    ''' </summary>
    ''' <param name="sCod">Código de usuario</param>
    ''' <param name="bAnyadirProvFav">si tiene favoritos o no</param>
    ''' <remarks>
    ''' Llamada desdE: PmServer/User/AnyadirProvFav
    ''' Tiempo máximo: 0,25 seg
    ''' </remarks>
    Public Sub User_Actualizar_AnyadirProvFav(ByVal sCod As String, ByVal bAnyadirProvFav As Boolean)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cn.Open()
            With cm
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "FSPM_ESTADO_ANYADIR_FAVORITO"
                .Parameters.AddWithValue("@USU", sCod)
                .Parameters.AddWithValue("@ANYADIR", bAnyadirProvFav)
                .ExecuteNonQuery()
            End With
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Actualiza la tabla usu indicando si hay proveedores favoritos o no para el usuario
    ''' </summary>
    ''' <param name="sCod">Código de usuario</param>
    ''' <param name="bAnyadirArticuloFav">si tiene favoritos o no</param>
    ''' <remarks>
    ''' Llamada desdE: PmServer/User/AnyadirProvFav
    ''' Tiempo máximo: 0,25 seg
    ''' </remarks>
    Public Sub User_Actualizar_AnyadirArticuloFav(ByVal sCod As String, ByVal bAnyadirArticuloFav As Boolean)
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cn.Open()
            With cm
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "FSPM_ESTADO_ANYADIR_ART4_FAVORITO"
                .Parameters.AddWithValue("@USU", sCod)
                .Parameters.AddWithValue("@ANYADIR", bAnyadirArticuloFav)
                .ExecuteNonQuery()
            End With
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Actualiza la tabla usu indicando si hay proveedores favoritos o no para el usuario
    ''' </summary>
    ''' <param name="sCod">Código de usuario</param>
    ''' <param name="bMostrarArticuloFavMat">si tiene favoritos o no</param>
    ''' <remarks>
    ''' Llamada desdE: PmServer/User/AnyadirProvFav
    ''' Tiempo máximo: 0,25 seg
    ''' </remarks>
    Public Sub User_Actualizar_MostrarArticuloFavMat(ByVal sCod As String, ByVal bMostrarArticuloFavMat As Boolean)
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cn.Open()
            With cm
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "FSPM_ESTADO_MOSTRAR_ART4_FAVORITO_MAT"
                .Parameters.AddWithValue("@USU", sCod)
                .Parameters.AddWithValue("@MOSTRAR", bMostrarArticuloFavMat)
                .ExecuteNonQuery()
            End With
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub

    ''' <summary>
    ''' Procedmiento que se encarga de actualizar el PP de forma manual o automática
    ''' </summary>
    ''' <param name="bManual">Incluir el manual o no</param>
    ''' <param name="bAuto">Actualizar automático</param>
    ''' <param name="iNivel">Nivel del PP</param>
    ''' <param name="sCod">Codigo de usuario</param>
    ''' <remarks>
    ''' Llamada desde: PmServer/User/Actualizar_PPManualAuto
    ''' Tiempo máximo: 0,2 seg</remarks>
    Public Sub User_Actualizar_PPManualAuto(ByVal sCod As String, ByVal bManual As Boolean, ByVal bAuto As Boolean, ByVal iNivel As Integer)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cn.Open()
            With cm
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "FSPM_MODIFICAR_PP_MANUAL_AUTO"
                .Parameters.AddWithValue("@USU", sCod)
                .Parameters.AddWithValue("@MANUAL", IIf(bManual, 1, 0))
                .Parameters.AddWithValue("@AUTO", IIf(bAuto, 1, 0))
                .Parameters.AddWithValue("@NIVEL", iNivel)
                .ExecuteNonQuery()
            End With
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' Revisado por: Jbg. Fecha: 20/10/2011
    ''' <summary>
    ''' Actualiza la configuración del panel de calidad del usuario con la lista de Unidades de negoico y variables de calidad chequeadas en el filtro, y con los campos ocultos en el panel.
    ''' </summary>
    ''' <param name="sUserCode">Código del usuario</param>
    ''' <param name="FiltroUNQA">Lista de unqas chequeadas en el filtro(lista de IDs)</param>
    ''' <param name="FiltroVarCal">Lista de variables de calidad chequeadas en el filtro(lista de pares NIVEL-ID)</param>
    ''' <param name="CamposOcultos">Lista de campos ocultos en el panel (nombre de los campos)</param>
    ''' <param name="Ordenacion">orden de los datos del panel</param>
    ''' <remarks>Llamada desde=User.vb; Tiempo máximo=0.01 seg.</remarks>
    Public Sub User_Actualizar_ConfiguracionPanelCalidad(ByVal sUserCode As String, ByVal FiltroUNQA As String, ByVal FiltroVarCal As String, ByVal CamposOcultos As String, ByVal Ordenacion As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSQA_ACTUALIZA_CONF_PANELCALIDAD"
            cm.CommandType = CommandType.StoredProcedure

            cm.Parameters.AddWithValue("@USU", sUserCode)
            cm.Parameters.AddWithValue("@FiltroUNQA", strToDBNull(FiltroUNQA))
            cm.Parameters.AddWithValue("@FiltroVarCal", strToDBNull(FiltroVarCal))
            cm.Parameters.AddWithValue("@CamposOcultos", strToDBNull(CamposOcultos))
            cm.Parameters.AddWithValue("@Ordenacion", strToDBNull(Ordenacion))
            cm.ExecuteNonQuery()
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Guarda las modificaciones realizadas en la opción de los parámetros de QA respecto a si
    ''' se debe notificar al usuario que se ha producido un error en el calculo de puntuaciones
    ''' </summary>
    ''' <param name="sCod"></param>
    ''' <param name="bNotificar"></param>
    ''' <remarks>
    ''' Llamada desde: PmServer/User/Actualizar_QANotifErrorCalculoPuntuaciones
    ''' Tiempo máximo: 0,2 seg</remarks>
    Public Sub User_Actualizar_QANotifErrorCalculoPuntuaciones(ByVal sCod As String, ByVal bNotificar As Boolean)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cn.Open()
            With cm
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "FSQA_NOTIFICAR_ERROR_CALCULO_PUNTUACIONES"
                .Parameters.AddWithValue("@USU", sCod)
                .Parameters.AddWithValue("@NOTIFICARERROR", IIf(bNotificar, 1, 0))
                .ExecuteNonQuery()
            End With
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' Revisado por: Jbg. Fecha: 20/10/2011
    ''' <summary>
    ''' Obtiene la configuración del panel de calidad para un usuario determinado.
    ''' </summary>
    ''' <param name="sUserCode">Código del usuario para obtener su configuración del panel</param>
    ''' <param name="FiltroUNQA">Lista de unidades de negocio que el usurio tenia chequeadas en el filtro cuando guardó su configuración</param>
    ''' <param name="FiltroVarCal">Lista de variables de calidad que el usurio tenia chequeadas en el filtro cuando guardó su configuración</param>
    ''' <param name="CamposOcultos">Lista de campos de los datos del proveedor del panel de proveedores que el usurio tenía ocultas cuando guardó su configuración</param>
    ''' <param name="Ordenacion">orden de los datos del panel</param>
    ''' <remarks>Llamada desde=User.vb; Tiempo máximo=0,0 seg.</remarks>
    Public Sub User_Cargar_ConfiguracionPanelCalidad(ByVal sUserCode As String, ByRef FiltroUNQA As String, ByRef FiltroVarCal As String, ByRef CamposOcultos As String, ByRef Ordenacion As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As SqlDataReader = Nothing
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSQA_CARGAR_CONF_PANELCALIDAD"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", sUserCode)
            dr = cm.ExecuteReader(CommandBehavior.CloseConnection)
            If dr.Read Then
                FiltroUNQA = DBNullToStr(dr("FiltroUNQA"))
                FiltroVarCal = DBNullToStr(dr("FiltroVarCal"))
                CamposOcultos = DBNullToStr(dr("CamposOcultos"))
                Ordenacion = DBNullToStr(dr("Ordenacion"))
            End If
        Catch e As Exception
            If Not dr Is Nothing AndAlso Not dr.IsClosed Then dr.Close()
            Throw e
        End Try
    End Sub
#End Region
#Region "SM User"
    ''' <summary>
    ''' Función que devuelve los datos de la persona asociada al usuario
    ''' </summary>
    ''' <param name="CodUser">Código de usuario</param>    
    ''' <param name="CodPersona">Código de la persona</param>
    ''' <returns>Un dataset con los datos de la persona</returns>
    ''' <remarks>
    ''' Llamada desde: FSNServer.User.Devolver_Contratos_Y_Centros
    ''' Tiempo máximo: 5 seg</remarks>
    Public Function User_GetPerson(ByVal CodUser As String, ByVal CodPersona As String) As DataSet
        Dim SqlCn As New SqlConnection(mDBConnection)
        SqlCn.Open()

        Dim SqlCm As New SqlCommand
        Dim dA As New SqlDataAdapter
        Try
            SqlCm.Connection = SqlCn
            SqlCm.CommandText = "FSSM_DEVOLVER_DATOS_PERSONA"
            SqlCm.CommandType = CommandType.StoredProcedure
            SqlCm.Parameters.AddWithValue("@USU", CodUser)
            If Not CodPersona Is Nothing AndAlso CodPersona <> String.Empty Then
                SqlCm.Parameters.AddWithValue("@PER", CodPersona)
            Else
                SqlCm.Parameters.AddWithValue("@PER", System.DBNull.Value)
            End If


            dA.SelectCommand = SqlCm
            Dim dsDatos As New DataSet
            dA.Fill(dsDatos)
            Return dsDatos
        Catch e As Exception
            Throw e
        Finally
            SqlCn.Dispose()
            SqlCm.Dispose()
            dA.Dispose()
        End Try
    End Function
#End Region
#End Region
#Region "UserIdentity data access methods "
    ''' <summary>
    ''' Procedimiento que instancia un objeto de la clase UserIdentity
    ''' </summary>
    ''' <returns>Una variable de tipo UserIdentity ya creada</returns>
    ''' <remarks>
    ''' Llamada desde. FSNServer/UserIdentity/New
    ''' Llamada desde: 0 seg</remarks>
    Public Function UserIdentity_GetActions() As UserIdentity_Actions
        Authenticate()
        Dim Act As New UserIdentity_Actions
        ReDim Act.ID(1)
        Act.ID(0) = 1
        ReDim Preserve Act.ID(UBound(Act.ID) + 1)
        Act.ID(1) = 2
        Return Act
    End Function
    <Serializable()> _
    Public Structure UserIdentity_Actions
        Public ID() As Integer
    End Structure
#End Region
#Region "Centros de coste data access methods "
    ''' <summary>
    ''' Obtiene la denominación del centro a través del código que se pasa como parámetro.
    ''' </summary>
    ''' <param name="sUsuario">Código de la persona</param>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <param name="sCodigo">Código del centro a buscar</param>
    ''' <param name="bUON">Indica si hay que buscar el cod en las UONS o en la tabla CENTRO_SM</param>
    ''' <returns>Código - Denominación si se ha encontrado un centro de coste</returns>
    ''' <remarks>Llamada desde: PMServer --> CentrosCoste.vb --> BuscarCentrosCoste; Tiempo máximo: 1 sg.</remarks>
    Public Function CentrosCoste_Busqueda(ByVal sUsuario As String, ByVal sIdioma As String, ByVal sCodigo As String, Optional ByVal bUON As Boolean = True) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn
            cm.CommandText = "FSSM_BUSCAR_CENTRO_COSTE"
            cm.Parameters.AddWithValue("@UON", bUON)
            cm.Parameters.AddWithValue("@IDI", sIdioma)
            cm.Parameters.AddWithValue("@USU", sUsuario)
            cm.Parameters.AddWithValue("@COD", sCodigo)
            cm.CommandType = CommandType.StoredProcedure

            da.SelectCommand = cm
            da.Fill(dr)

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Devuelve los centros de coste wcon toda la estructura organizativa o solo los centros de coste para mostrarlas en un treeview. Añade las relaciones a las tablas del dataset.
    ''' </summary>
    ''' <param name="sUsuario">Cod de la persona</param>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <param name="bVerUON">true: se ve la estructura organizativa; false: sólo los centros de coste</param>
    ''' <returns>Dataset con los centros de coste y su estructura</returns>
    ''' <remarks>Llamada desde: SMServer --> CentrosCoste.vb --> LoadData; Tiempo máximo: 1 sg;</remarks>
    Public Function CentrosCoste_LoadEP(ByVal sUsuario As String, ByVal sIdioma As String, ByVal Empresa As String, Optional ByVal bVerUON As Boolean = False, Optional ByVal sDesde As String = "", Optional ByVal sTextoFiltro As String = "") As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn
            cm.CommandText = "EP_OBT_CENTROS_SEGUIMIENTO_EP"

            cm.Parameters.AddWithValue("@IDI", sIdioma)
            cm.Parameters.AddWithValue("@VERUON", bVerUON)
            If sUsuario <> Nothing Then
                cm.Parameters.AddWithValue("@USU", sUsuario)
            End If
            If Empresa <> "" Then
                cm.Parameters.AddWithValue("@EMP", Empresa)
            Else
                cm.Parameters.AddWithValue("@EMP", 0)
            End If
            If sTextoFiltro <> Nothing Then
                cm.Parameters.AddWithValue("@TEXTO", sTextoFiltro)
            Else
                cm.Parameters.AddWithValue("@TEXTO", DBNull.Value)
            End If
            cm.CommandType = CommandType.StoredProcedure

            da.SelectCommand = cm
            da.Fill(dr)

            '******************************************************************
            Dim parentCols(0) As DataColumn
            Dim childCols(0) As DataColumn

            parentCols(0) = dr.Tables(0).Columns("COD0")
            childCols(0) = dr.Tables(1).Columns("COD0")

            dr.Relations.Add("REL_NIV0_NIV1", parentCols, childCols, False)

            If dr.Tables.Count > 2 Then

                ReDim parentCols(0)
                ReDim childCols(0)

                parentCols(0) = dr.Tables(1).Columns("COD")
                childCols(0) = dr.Tables(2).Columns("UON1")

                dr.Relations.Add("REL_NIV1_NIV2", parentCols, childCols, False)

                ReDim parentCols(1)
                ReDim childCols(1)

                parentCols(0) = dr.Tables(2).Columns("UON1")
                parentCols(1) = dr.Tables(2).Columns("COD")
                childCols(0) = dr.Tables(3).Columns("UON1")
                childCols(1) = dr.Tables(3).Columns("UON2")
                dr.Relations.Add("REL_NIV2_NIV3", parentCols, childCols, False)

                ReDim parentCols(2)
                ReDim childCols(2)

                parentCols(0) = dr.Tables(3).Columns("UON1")
                parentCols(1) = dr.Tables(3).Columns("UON2")
                parentCols(2) = dr.Tables(3).Columns("COD")
                childCols(0) = dr.Tables(4).Columns("UON1")
                childCols(1) = dr.Tables(4).Columns("UON2")
                childCols(2) = dr.Tables(4).Columns("UON3")
                dr.Relations.Add("REL_NIV3_NIV4", parentCols, childCols, False)
            End If

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Devuelve los centros de coste wcon toda la estructura organizativa o solo los centros de coste para mostrarlas en un treeview. Añade las relaciones a las tablas del dataset.
    ''' </summary>
    ''' <param name="sUsuario">Cod de la persona</param>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <param name="bVerUON">true: se ve la estructura organizativa; false: sólo los centros de coste</param>
    ''' <returns>Dataset con los centros de coste y su estructura</returns>
    ''' <remarks>Llamada desde: SMServer --> CentrosCoste.vb --> LoadData; Tiempo máximo: 1 sg;</remarks>
    Public Function CentrosCoste_Load(ByVal sUsuario As String, ByVal sIdioma As String, Optional ByVal bVerUON As Boolean = False, Optional ByVal sDesde As String = "", Optional ByVal iIdEmpresa As Integer = 0) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn
            cm.CommandText = "FSSM_DEVOLVER_CENTROS_COSTE"

            cm.Parameters.AddWithValue("@IDI", sIdioma)
            cm.Parameters.AddWithValue("@VERUON", bVerUON)
            If sUsuario <> Nothing Then
                cm.Parameters.AddWithValue("@USU", sUsuario)
            End If
            cm.Parameters.AddWithValue("@DESDE", sDesde)

            cm.Parameters.AddWithValue("@IDEMPRESA", iIdEmpresa)

            cm.CommandType = CommandType.StoredProcedure

            da.SelectCommand = cm
            da.Fill(dr)

            '******************************************************************
            Dim parentCols(0) As DataColumn
            Dim childCols(0) As DataColumn

            parentCols(0) = dr.Tables(0).Columns("COD0")
            childCols(0) = dr.Tables(1).Columns("COD0")

            dr.Relations.Add("REL_NIV0_NIV1", parentCols, childCols, False)

            If dr.Tables.Count > 2 Then

                ReDim parentCols(0)
                ReDim childCols(0)

                parentCols(0) = dr.Tables(1).Columns("COD")
                childCols(0) = dr.Tables(2).Columns("UON1")

                dr.Relations.Add("REL_NIV1_NIV2", parentCols, childCols, False)

                ReDim parentCols(1)
                ReDim childCols(1)

                parentCols(0) = dr.Tables(2).Columns("UON1")
                parentCols(1) = dr.Tables(2).Columns("COD")
                childCols(0) = dr.Tables(3).Columns("UON1")
                childCols(1) = dr.Tables(3).Columns("UON2")
                dr.Relations.Add("REL_NIV2_NIV3", parentCols, childCols, False)

                ReDim parentCols(2)
                ReDim childCols(2)

                parentCols(0) = dr.Tables(3).Columns("UON1")
                parentCols(1) = dr.Tables(3).Columns("UON2")
                parentCols(2) = dr.Tables(3).Columns("COD")
                childCols(0) = dr.Tables(4).Columns("UON1")
                childCols(1) = dr.Tables(4).Columns("UON2")
                childCols(2) = dr.Tables(4).Columns("UON3")
                dr.Relations.Add("REL_NIV3_NIV4", parentCols, childCols, False)
            End If

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function CentrosCoste_LoadEnIM(ByVal sUsuario As String, ByVal sIdioma As String, Optional ByVal bVerUON As Boolean = False, Optional ByVal sDesde As String = "", Optional ByVal usercode As String = "", Optional ByVal userpassword As String = "") As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm = New SqlCommand

            cm.Connection = cn
            cm.CommandText = "FSSM_DEVOLVER_CENTROS_COSTE"

            cm.Parameters.AddWithValue("@IDI", sIdioma)
            cm.Parameters.AddWithValue("@VERUON", bVerUON)
            If sUsuario <> "" Then
                cm.Parameters.AddWithValue("@USU", sUsuario)
            End If
            cm.Parameters.AddWithValue("@DESDE", sDesde)

            cm.CommandType = CommandType.StoredProcedure

            da.SelectCommand = cm
            da.Fill(dr)

            '******************************************************************
            Dim i As Byte
            For Each dt As DataTable In dr.Tables
                dt.TableName = "Tabla_" & i.ToString
                i += 1
            Next
            Dim parentCols(0) As DataColumn
            Dim childCols(0) As DataColumn
            Dim dt0(0) As DataColumn
            Dim dt1(1) As DataColumn

            dt0(0) = dr.Tables(0).Columns("COD0")
            dt1(0) = dr.Tables(1).Columns("COD0")
            dt1(1) = dr.Tables(1).Columns("ID_CENTRO_SM")

            dr.Tables(0).PrimaryKey = dt0
            dr.Tables(1).PrimaryKey = dt1

            parentCols(0) = dr.Tables(0).Columns("COD0")
            childCols(0) = dr.Tables(1).Columns("COD0")

            dr.Relations.Add("REL_NIV0_NIV1", parentCols, childCols, False)

            If dr.Tables.Count > 2 Then

                ReDim parentCols(0)
                ReDim childCols(0)

                parentCols(0) = dr.Tables(1).Columns("COD")
                childCols(0) = dr.Tables(2).Columns("UON1")

                dr.Relations.Add("REL_NIV1_NIV2", parentCols, childCols, False)

                ReDim parentCols(1)
                ReDim childCols(1)

                parentCols(0) = dr.Tables(2).Columns("UON1")
                parentCols(1) = dr.Tables(2).Columns("COD")
                childCols(0) = dr.Tables(3).Columns("UON1")
                childCols(1) = dr.Tables(3).Columns("UON2")
                dr.Relations.Add("REL_NIV2_NIV3", parentCols, childCols, False)

                ReDim parentCols(2)
                ReDim childCols(2)

                parentCols(0) = dr.Tables(3).Columns("UON1")
                parentCols(1) = dr.Tables(3).Columns("UON2")
                parentCols(2) = dr.Tables(3).Columns("COD")
                childCols(0) = dr.Tables(4).Columns("UON1")
                childCols(1) = dr.Tables(4).Columns("UON2")
                childCols(2) = dr.Tables(4).Columns("UON3")
                dr.Relations.Add("REL_NIV3_NIV4", parentCols, childCols, False)
            End If

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function Obtener_Centros_SM(ByVal CodUsuario As String, ByVal Idioma As String, ByVal listaUONs As String,
                                       ByVal EsAutocompletar As Boolean, ByVal filtroCodigo As String, ByVal filtroDenominacion As String) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = "FSSM_OBTENER_CENTROS_COSTE"
            cm.Parameters.AddWithValue("@USU", CodUsuario)
            cm.Parameters.AddWithValue("@IDIOMA", Idioma)
            If Not String.IsNullOrEmpty(listaUONs) Then cm.Parameters.AddWithValue("@LISTAUON", listaUONs)
            cm.Parameters.AddWithValue("@ESAUTOCOMPLETAR", EsAutocompletar)
            If Not String.IsNullOrEmpty(filtroCodigo) Then cm.Parameters.AddWithValue("@FILTROCODIGO", filtroCodigo)
            If Not String.IsNullOrEmpty(filtroDenominacion) Then cm.Parameters.AddWithValue("@FILTRODENOMINACION", filtroDenominacion)
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
#End Region
#Region "Textos GS"
    Public Function TextoGs_Carga(ByVal Modulo As Integer, ByVal ID As Integer, ByVal Idioma As String) As String
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter

        Dim sRes As String
        Try
            sRes = ""

            cm.Connection = cn

            cm.CommandText = "SELECT TEXT_" & Idioma & " FROM TEXTOS_GS WITH(NOLOCK) WHERE MODULO=" & Modulo & " AND ID=" & ID

            cm.CommandType = CommandType.Text

            da.SelectCommand = cm
            da.Fill(ds)

            If ds.Tables(0).Rows.Count > 0 Then
                sRes = "" & ds.Tables(0).Rows(0).Item(0)
            End If
            Return sRes
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
#End Region
#Region "Validaciones integracion"
    ''' <summary>
    ''' Función que nos dice si hay integracion Activa para una entidad concreta
    ''' </summary>
    ''' <param name="iTabla">Id de Tabla en TABLAS_INTEGrACION_ERP </param>
    ''' <returns>Devuelve booleano</returns>
    ''' <remarks>Llamada desde Integracion.vb\HayIntegracion
    ''' Tiempo maximo 0 sec</remarks>
    Public Function HayIntegracion(ByVal iTabla As Integer, Optional ByVal iSentido As Short = SentidoIntegracion.SinSentido) As Boolean
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter

        Dim bRes As Boolean
        Try
            bRes = False

            cm.Connection = cn

            'Si está activa la integración en sentido:
            Dim sSentido As String = String.Empty
            Select Case iSentido
                Case SentidoIntegracion.Salida
                    sSentido = "AND SENTIDO IN (" & SentidoIntegracion.Salida & "," & SentidoIntegracion.EntradaYSalida & ")"
                Case SentidoIntegracion.Entrada
                    sSentido = "AND SENTIDO IN (" & SentidoIntegracion.Entrada & "," & SentidoIntegracion.EntradaYSalida & ")"
            End Select

            cm.CommandText = "SELECT TOP 1 ACTIVA FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=@TABLA " & sSentido & " AND ACTIVA=1"
            cm.CommandType = CommandType.Text
            cm.Parameters.AddWithValue("@TABLA", iTabla)
            da.SelectCommand = cm
            da.Fill(ds)

            bRes = (ds.Tables(0).Rows.Count > 0)
            Return bRes
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
#End Region
#End Region
#Region "FILESTREAM"
    ''' <summary>
    ''' Realiza el guardado del adjunto a traves de FILESTREAM
    ''' </summary>
    ''' <param name="lIdAdjunto">ID adjunto</param>
    ''' <param name="Buffer">Buffer que ha cogido el adjunto</param>
    ''' <remarks>Llamada desde:Create; Tiempo máximo:0,6</remarks>
    Public Sub GrabarNuevoAdjuntoPM(ByVal lIdAdjunto As Long, ByVal Buffer As Byte(), Optional ByVal Tipo As Byte = Nothing)
        Dim cn As New SqlConnection
        Dim mi_impersionationContext As Security.Principal.WindowsImpersonationContext

        cn = NuevaConexionFileStream()
        mi_impersionationContext = RealizarSuplantacion()
        cn.Open()

        Dim tx As SqlTransaction = cn.BeginTransaction()
        Dim oSqlFileStream As SqlTypes.SqlFileStream = GetSqlFileStreamForWriting(lIdAdjunto, cn, tx, , Tipo)
        If Not oSqlFileStream Is Nothing Then
            oSqlFileStream.Write(Buffer, 0, Buffer.Length)
            oSqlFileStream.Close()
        End If
        tx.Commit()
        cn.Close()
        DeshacerSuplantacion(mi_impersionationContext)
    End Sub
    ''' <summary>
    ''' Realiza el guardado del adjunto a traves de FILESTREAM
    ''' </summary>
    ''' <param name="lIdEFactura">ID adjunto</param>
    ''' <param name="Buffer">Buffer que ha cogido el adjunto</param>
    ''' <remarks>Llamada desde:Create; Tiempo máximo:0,6</remarks>
    Public Sub GrabarNuevaEFactura(ByVal lIdEFactura As Long, ByVal Buffer As Byte())
        Dim cn As New SqlConnection
        Dim mi_impersionationContext As Security.Principal.WindowsImpersonationContext

        cn = NuevaConexionFileStream()
        mi_impersionationContext = RealizarSuplantacion()
        cn.Open()

        Dim tx As SqlTransaction = cn.BeginTransaction()
        Dim oSqlFileStream As SqlTypes.SqlFileStream = GetSqlFileStreamForWriting(lIdEFactura, cn, tx, TipoFicheroFileStream.EFactura)
        If Not oSqlFileStream Is Nothing Then
            oSqlFileStream.Write(Buffer, 0, Buffer.Length)
            oSqlFileStream.Close()
        End If
        tx.Commit()
        cn.Close()
        DeshacerSuplantacion(mi_impersionationContext)
    End Sub
    ''' <summary>
    ''' Para poder trabajar con FILESTREAM necesitamos suplantacion de identidad, es decir, necesitamos un usuario SQL que pueda
    ''' trabajar con este tipo de ficheros. Para ellos necesitamos que el usuario este creado con esos permisos en SQL
    ''' </summary>
    ''' <returns>Contexto de la suplantacion</returns>
    ''' <remarks>Llamada desde:GrabarNuevoAdjunto; Tiempo maximo:0seg.</remarks>
    Private Function GetSqlFileStreamForWriting(ByVal lId As Long, ByVal cn As SqlConnection, ByVal tx As SqlTransaction, Optional ByVal TipoFichero As TipoFicheroFileStream = TipoFicheroFileStream.Adjunto, Optional ByVal Tipo As Byte = Nothing) As SqlTypes.SqlFileStream
        Dim sPath As String = String.Empty
        Dim arTransactionContext() As Byte = Nothing
        GetSqlFileStreamForWriting = Nothing
        Select Case TipoFichero
            Case TipoFicheroFileStream.Adjunto
                GetPathNameAndTxContextAdjuntoPM(lId, cn, tx, sPath, arTransactionContext, Tipo)
            Case TipoFicheroFileStream.EFactura
                GetPathNameAndTxContextEFactura(lId, cn, tx, sPath, arTransactionContext)
        End Select

        '4 - open the filestream to the blob
        If Not sPath Is Nothing And Not arTransactionContext Is Nothing Then
            GetSqlFileStreamForWriting = New SqlTypes.SqlFileStream(sPath, arTransactionContext, FileAccess.Write)
        End If
    End Function
    ''' <summary>
    ''' Obtiene la referencia del adjunto en BBDD para poder trabajar (Escribir) con el 
    ''' de tablas adjuntos de PM
    ''' </summary>
    ''' <param name="lIdAdjunto">Explicacion parametro 1</param>
    ''' <param name="cn">Conexion</param>        
    ''' <param name="tx">Transaccion lectura adjunto</param>
    ''' <param name="Path">Path del adjunto</param>
    ''' <param name="TxContext">TRANSACTIONCONTEXT del adjunto</param>
    ''' <remarks>Llamada desde:GetSqlFileStreamForWriting; Tiempo maximo:0,35seg.</remarks>
    Private Sub GetPathNameAndTxContextAdjuntoPM(ByVal lIdAdjunto As Long, ByVal cn As SqlConnection, ByVal tx As SqlTransaction, ByRef Path As String, ByRef TxContext As Byte(), Optional ByVal Tipo As Byte = Nothing)
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSPM_LEER_ADJUNTO"

        With cm.Parameters
            .Add("@ID", SqlDbType.Int)
            cm.Parameters("@ID").Value = lIdAdjunto
            If Not Tipo = Nothing Then
                cm.Parameters.AddWithValue("@TIPO", Tipo)
            End If
        End With
        cm.Connection = cn
        cm.Transaction = tx

        Dim da As New SqlDataAdapter
        da.SelectCommand = cm
        Dim ds As New System.Data.DataSet
        da.Fill(ds)

        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim oRow As DataRow = ds.Tables(0).Rows(0)

            If Not oRow.IsNull("PATHNAME") Then Path = oRow("PATHNAME")
            If Not oRow.IsNull("TRANSACTIONCONTEXT") Then TxContext = CType(oRow("TRANSACTIONCONTEXT"), Byte()) 'reader.GetSqlBytes(1).Buffer;            

            If Path Is Nothing Then
                'Si se inserta un registro con la columna varbinary(max) a NULL no se crea archivo y FilePath devuelve nulo, con lo que no se puede
                'crear el sqlFileStream.
                'Insertando '' en la columna varbinary(max) se crea un registro de longitud cero y ya se puede obtener un handle para el archivo
                Dim oCom As New SqlCommand
                oCom.CommandType = CommandType.Text
                oCom.CommandText = "UPDATE PM_COPIA_ADJUN SET DATA=cast('' as varbinary(max)) WHERE ID = " & lIdAdjunto
                oCom.Connection = cn
                oCom.Transaction = tx
                oCom.ExecuteNonQuery()

                Dim obj As Object = cm.ExecuteScalar()
                If Not obj.Equals(DBNull.Value) Then Path = DirectCast(obj, String)
            End If
        End If
    End Sub
    ''' <summary>
    ''' Obtiene la referencia del adjunto en BBDD para poder trabajar (Escribir) con el 
    ''' </summary>
    ''' <param name="lIdEFactura">Explicación parámetro 1</param>
    ''' <param name="cn">Conexion</param>        
    ''' <param name="tx">Transaccion lectura adjunto</param>
    ''' <param name="Path">Path del adjunto</param>
    ''' <param name="TxContext">TRANSACTIONCONTEXT del adjunto</param>
    ''' <remarks>Llamada desde:GetSqlFileStreamForWriting; Tiempo máximo:0,35seg.</remarks>
    Private Sub GetPathNameAndTxContextEFactura(ByVal lIdEFactura As Long, ByVal cn As SqlConnection, ByVal tx As SqlTransaction, ByRef Path As String, ByRef TxContext As Byte())
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSPM_LEER_E_FACTURA"

        With cm.Parameters
            .Add("@ID", SqlDbType.Int)
            cm.Parameters("@ID").Value = lIdEFactura
        End With
        cm.Connection = cn
        cm.Transaction = tx

        Dim da As New SqlDataAdapter
        da.SelectCommand = cm
        Dim ds As New System.Data.DataSet
        da.Fill(ds)

        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim oRow As DataRow = ds.Tables(0).Rows(0)

            If Not oRow.IsNull("PATHNAME") Then Path = oRow("PATHNAME")
            If Not oRow.IsNull("TRANSACTIONCONTEXT") Then TxContext = CType(oRow("TRANSACTIONCONTEXT"), Byte())

            If Path Is Nothing Then
                'Si se inserta un registro con la columna varbinary(max) a NULL no se crea archivo y FilePath devuelve nulo, con lo que no se puede
                'crear el sqlFileStream.
                'Insertando '' en la columna varbinary(max) se crea un registro de longitud cero y ya se puede obtener un handle para el archivo
                Dim oCom As New SqlCommand
                oCom.CommandType = CommandType.Text
                oCom.CommandText = "UPDATE FACTURA_E SET DATA=cast('' as varbinary(max)) WHERE ID = " & lIdEFactura
                oCom.Connection = cn
                oCom.Transaction = tx
                oCom.ExecuteNonQuery()

                Dim obj As Object = cm.ExecuteScalar()
                If Not obj.Equals(DBNull.Value) Then Path = DirectCast(obj, String)
            End If
        End If
    End Sub
    ''' <summary>
    ''' Realiza el guardado del adjunto de tipo Especificacion a traves de FILESTREAM
    ''' </summary>
    ''' <param name="lIdAdjunto">ID adjunto</param>
    ''' <remarks>Llamada desde:Create; Tiempo máximo:0,6</remarks>
    Public Function GrabarNuevoAdjuntoEspecificacion(ByVal lIdAdjunto As Long, ByVal sNombreFichero As String, ByVal Tipo As Byte, ByVal Grupo As Long, ByVal Proce As Long,
                                                     ByVal Gmn1 As String, ByVal Anyo As Long) As Long
        Dim cn As New SqlConnection
        Dim mi_impersionationContext As Security.Principal.WindowsImpersonationContext

        cn = NuevaConexionFileStream()
        mi_impersionationContext = RealizarSuplantacion()
        cn.Open()

        Dim tx As SqlTransaction = cn.BeginTransaction()
        Dim oSqlFileStream As SqlTypes.SqlFileStream = Nothing
        oSqlFileStream = GetSqlFileStreamForWritingEspecificacion(lIdAdjunto, cn, tx, Tipo, Grupo, Proce, Gmn1, Anyo)

        Dim lFileSize As Long = GrabarEnBDEnChunks(oSqlFileStream, sNombreFichero)

        tx.Commit()
        cn.Close()
        DeshacerSuplantacion(mi_impersionationContext)

        Return lFileSize
    End Function
    Private Function GrabarEnBDEnChunks(ByRef oSqlFileStream As SqlTypes.SqlFileStream, ByVal sNombreFichero As String) As Long
        Dim input As System.IO.FileStream = New System.IO.FileStream(sNombreFichero, System.IO.FileMode.Open, System.IO.FileAccess.Read)
        Dim lFileSize As Long = input.Length

        If Not oSqlFileStream Is Nothing Then
            Dim Chunks As Long
            Dim Fragment As Long

            Chunks = input.Length \ 10485760
            Fragment = input.Length Mod 10485760
            If Fragment > 0 Then Chunks = Chunks + 1

            Dim buffer(10485759) As Byte
            For i = 1 To Chunks
                Dim iBytesRead As Integer = input.Read(buffer, 0, buffer.Length)
                ReDim Preserve buffer(iBytesRead - 1)

                oSqlFileStream.Write(buffer, 0, buffer.Length)
            Next
            oSqlFileStream.Close()
        End If

        input.Close()

        Return lFileSize
    End Function
    ''' <summary>
    ''' Para poder trabajar con FILESTREAM necesitamos suplantacion de identidad, es decir, necesitamos un usuario SQL que pueda
    ''' trabajar con este tipo de ficheros. Para ellos necesitamos que el usuario este creado con esos permisos en SQL
    ''' </summary>
    ''' <returns>Contexto de la suplantacion</returns>
    ''' <remarks>Llamada desde:GrabarNuevoAdjunto; Tiempo maximo:0seg.</remarks>
    Private Function GetSqlFileStreamForWritingEspecificacion(ByVal lId As Long, ByVal cn As SqlConnection, ByVal tx As SqlTransaction, ByVal Tipo As Long, ByVal Grupo As Long, ByVal Proce As Long, ByVal Gmn1 As String, ByVal Anyo As Long) As SqlTypes.SqlFileStream
        Dim sPath As String = String.Empty
        Dim arTransactionContext() As Byte = Nothing

        GetSqlFileStreamForWritingEspecificacion = Nothing
        GetPathNameAndTxContextAdjuntoEspecificacion(lId, cn, tx, sPath, arTransactionContext, Tipo, Grupo, Proce, Gmn1, Anyo)

        '4 - open the filestream to the blob
        If Not sPath Is Nothing And Not arTransactionContext Is Nothing Then
            GetSqlFileStreamForWritingEspecificacion = New SqlTypes.SqlFileStream(sPath, arTransactionContext, FileAccess.Write)
        End If
    End Function
    Private Sub GetPathNameAndTxContextAdjuntoEspecificacion(ByVal lId As Long, ByVal cn As SqlConnection, ByVal tx As SqlTransaction, ByRef Path As String, ByRef TxContext As Byte(), ByVal Tipo As Long, ByVal Grupo As Long, ByVal Proce As Long, ByVal Gmn1 As String, ByVal Anyo As Long)
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_LEER_ESPECIFICACION"

        With cm.Parameters
            .AddWithValue("@ID", lId)
            .AddWithValue("@TIPO", Tipo)

            Select Case Tipo
                Case 0
                    .AddWithValue("@PROCE", Proce)
                    .AddWithValue("@GMN1", Gmn1)
                    .AddWithValue("@ANYO", Anyo)
                Case 1
                    .AddWithValue("@PROCE", Proce)
                    .AddWithValue("@GMN1", Gmn1)
                    .AddWithValue("@ANYO", Anyo)
                    .AddWithValue("@GRUPO", Grupo)
                Case 2
                    .AddWithValue("@PROCE", Proce)
                    .AddWithValue("@GMN1", Gmn1)
                    .AddWithValue("@ANYO", Anyo)
                    .AddWithValue("@ITEM", Grupo)
                Case 3
                    .AddWithValue("@ART4", Gmn1)
                Case 4
                    .AddWithValue("@PROVE", Gmn1)
                Case 7
                    .AddWithValue("@PLANT", Grupo)
                Case 8
                    .AddWithValue("@PROCE", Proce)
                    .AddWithValue("@GMN1", Gmn1)
                    .AddWithValue("@ANYO", Anyo)
                Case 9
                    .AddWithValue("@GRUPO", Grupo)
            End Select
        End With
        cm.Connection = cn
        cm.Transaction = tx

        Dim da As New SqlDataAdapter
        da.SelectCommand = cm
        Dim ds As New System.Data.DataSet
        da.Fill(ds)

        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim oRow As DataRow = ds.Tables(0).Rows(0)

            If Not oRow.IsNull("PATHNAME") Then Path = oRow("PATHNAME")
            If Not oRow.IsNull("TRANSACTIONCONTEXT") Then TxContext = CType(oRow("TRANSACTIONCONTEXT"), Byte())

            If Path Is Nothing Then
                'Si se inserta un registro con la columna varbinary(max) a NULL no se crea archivo y FilePath devuelve nulo, con lo que no se puede
                'crear el sqlFileStream.
                'Insertando '' en la columna varbinary(max) se crea un registro de longitud cero y ya se puede obtener un handle para el archivo
                Dim oCom As New SqlCommand
                oCom.CommandType = CommandType.StoredProcedure
                oCom.CommandText = "FSGS_UPD_ESPECIFICACION"
                With oCom.Parameters
                    .AddWithValue("@ID", lId)
                    .AddWithValue("@TIPO", Tipo)
                    .AddWithValue("@PROCE", Proce)
                    .AddWithValue("@GMN1", Gmn1)
                    .AddWithValue("@ANYO", Anyo)
                    .AddWithValue("@GRUPO", Grupo)
                    .AddWithValue("@ITEM", Grupo)
                    .AddWithValue("@ART4", Gmn1)
                    .AddWithValue("@PROVE", Gmn1)
                    .AddWithValue("@PLANT", Grupo)
                End With
                oCom.Connection = cn
                oCom.Transaction = tx
                oCom.ExecuteNonQuery()

                Dim obj As Object = cm.ExecuteScalar()
                If Not obj.Equals(DBNull.Value) Then Path = DirectCast(obj, String)
            End If
        End If
    End Sub
    ''' <summary>
    ''' Guardar adjunto en tabla ADJUN del Portal a través de FSNWebService, la conexión puede ser a GS o a Portal, dependiendo como se haya instalado el webservice
    ''' </summary>
    ''' <param name="lIdAdjunto">id adjunto</param>
    ''' <param name="Cia">compania</param>
    ''' <param name="sNombreFichero">nombre adjunto</param>
    ''' <returns>Error de haberlo</returns>
    ''' <remarks>Llamada desde: PMQA_Root.Adjunto_GrabarAdjuntoPortal; Tiempo máximo: 0,1</remarks>
    ''' Revisada EPB 31/08/2015
    Public Function GrabarNuevoAdjuntoPortal(ByVal lIdAdjunto As Long, ByVal sNombreFichero As String, ByVal Cia As Long) As Long
        Dim cn As New SqlConnection
        Dim mi_impersionationContext As Security.Principal.WindowsImpersonationContext = Nothing
        Dim tx As SqlTransaction = Nothing
        Dim lFileSize As Long
        Try
            cn = NuevaConexionFileStream()
            mi_impersionationContext = RealizarSuplantacion()
            cn.Open()

            tx = cn.BeginTransaction()
            Dim oSqlFileStream As SqlTypes.SqlFileStream = GetSqlFileStreamForWritingAdjuntoPortal(lIdAdjunto, cn, tx, , Cia)

            lFileSize = GrabarEnBDEnChunks(oSqlFileStream, sNombreFichero)

            tx.Commit()
        Catch ex As Exception
            tx.Rollback()
        Finally
            cn.Close()
            DeshacerSuplantacion(mi_impersionationContext)
        End Try
        Return lFileSize
    End Function
    ''' <summary>
    ''' Get SqlFileStream For Writing de tabla ADJUN del Portal, la conexión puede ser a GS o a Portal, dependiendo como se haya instalado el webservice
    ''' </summary>
    ''' <param name="lId">id </param>
    ''' <param name="Cia">compania</param>
    ''' <returns>Error de haberlo</returns>
    ''' <remarks>Llamada desde: Root.GrabarNuevoAdjuntoPortal; Tiempo máximo: 0,1</remarks>
    ''' Revisada EPB 31/08/2015
    Private Function GetSqlFileStreamForWritingAdjuntoPortal(ByVal lId As Long, ByVal cn As SqlConnection, ByVal tx As SqlTransaction, Optional ByVal TipoFichero As TipoFicheroFileStream = TipoFicheroFileStream.Especificacion, _
                                                             Optional ByVal Cia As Long = 0) As SqlTypes.SqlFileStream
        Dim sPath As String = String.Empty
        Dim arTransactionContext() As Byte = Nothing
        GetSqlFileStreamForWritingAdjuntoPortal = Nothing

        GetPathNameAndTxContextAdjuntoAdjuntoPortal(lId, cn, tx, sPath, arTransactionContext, 0, Cia)

        '4 - open the filestream to the blob
        If Not sPath Is Nothing And Not arTransactionContext Is Nothing Then
            GetSqlFileStreamForWritingAdjuntoPortal = New SqlTypes.SqlFileStream(sPath, arTransactionContext, FileAccess.Write)
        End If
    End Function
    ''' <summary>
    ''' GetPathNameAndTxContext de tabla ADJUN del Portal, la conexión puede ser a GS o a Portal, dependiendo como se haya instalado el webservice
    ''' </summary>
    ''' <param name="lIdEFactura">id lIdEFactura</param>
    ''' <param name="Cia">compania</param>
    ''' <remarks>Llamada desde: Root.GetSqlFileStreamForWritingAdjuntoPortal; Tiempo máximo: 0,1</remarks>
    ''' Revisada EPB 31/08/2015
    Private Sub GetPathNameAndTxContextAdjuntoAdjuntoPortal(ByVal lIdEFactura As Long, ByVal cn As SqlConnection, ByVal tx As SqlTransaction, ByRef Path As String, ByRef TxContext As Byte(), _
                                                            Optional ByVal Tipo As Long = 0, Optional ByVal Cia As Long = 0)
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSPM_LEER_ADJUNTOPORTAL"

        With cm.Parameters
            .Add("@ID", SqlDbType.Int)
            .Add("@CIA", SqlDbType.Int)
            cm.Parameters("@ID").Value = lIdEFactura
            cm.Parameters("@CIA").Value = Cia
        End With

        cm.Connection = cn
        cm.Transaction = tx

        Dim da As New SqlDataAdapter
        da.SelectCommand = cm
        Dim ds As New System.Data.DataSet
        da.Fill(ds)

        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim oRow As DataRow = ds.Tables(0).Rows(0)

            If Not oRow.IsNull("PATHNAME") Then Path = oRow("PATHNAME")
            If Not oRow.IsNull("TRANSACTIONCONTEXT") Then TxContext = CType(oRow("TRANSACTIONCONTEXT"), Byte())

            If Path Is Nothing Then 'EPB(31/08/15) Esto estaría mal porque tiene que updatar la tabla ADJUN de portal, creo que no se mete nunca por aqui, ahora funcionaria cuando el webservice este en portal pq se conectará a bd de portal 
                'Si se inserta un registro con la columna varbinary(max) a NULL no se crea archivo y FilePath devuelve nulo, con lo que no se puede
                'crear el sqlFileStream.
                'Insertando '' en la columna varbinary(max) se crea un registro de longitud cero y ya se puede obtener un handle para el archivo
                Dim oCom As New SqlCommand
                oCom.CommandType = CommandType.Text
                oCom.CommandText = "UPDATE ADJUN SET DATA=cast('' as varbinary(max)) WHERE ID = " & lIdEFactura & " AND CIA = " & Cia

                oCom.Connection = cn
                oCom.Transaction = tx
                oCom.ExecuteNonQuery()

                Dim obj As Object = cm.ExecuteScalar()
                If Not obj.Equals(DBNull.Value) Then Path = DirectCast(obj, String)
            End If
        End If
    End Sub
    Public Function GrabarNuevoAdjuntoEspecificacionCia(ByVal lIdAdjunto As Long, ByVal sNombreFichero As String, ByVal Cia As Long) As Long
        Dim cn As New SqlConnection
        Dim mi_impersionationContext As Security.Principal.WindowsImpersonationContext

        cn = NuevaConexionFileStream()
        mi_impersionationContext = RealizarSuplantacion()
        cn.Open()

        Dim tx As SqlTransaction = cn.BeginTransaction()
        Dim oSqlFileStream As SqlTypes.SqlFileStream = GetSqlFileStreamForWritingAdjuntoEspecificacionCia(lIdAdjunto, cn, tx, , Cia)

        Dim lFileSize As Long = GrabarEnBDEnChunks(oSqlFileStream, sNombreFichero)

        tx.Commit()
        cn.Close()
        DeshacerSuplantacion(mi_impersionationContext)

        Return lFileSize
    End Function
    Private Function GetSqlFileStreamForWritingAdjuntoEspecificacionCia(ByVal lId As Long, ByVal cn As SqlConnection, ByVal tx As SqlTransaction, Optional ByVal TipoFichero As TipoFicheroFileStream = TipoFicheroFileStream.Especificacion, Optional ByVal Cia As Long = 0) As SqlTypes.SqlFileStream
        Dim sPath As String = String.Empty
        Dim arTransactionContext() As Byte = Nothing
        GetSqlFileStreamForWritingAdjuntoEspecificacionCia = Nothing

        GetPathNameAndTxContextAdjuntoAdjuntoEspecificacionCia(lId, cn, tx, sPath, arTransactionContext, 0, Cia)

        '4 - open the filestream to the blob
        If Not sPath Is Nothing And Not arTransactionContext Is Nothing Then
            GetSqlFileStreamForWritingAdjuntoEspecificacionCia = New SqlTypes.SqlFileStream(sPath, arTransactionContext, FileAccess.Write)
        End If
    End Function
    ''' <summary>
    ''' GetPathNameAndTxContext de tabla CIAS_ESP del Portal, la conexión puede ser a GS o a Portal, dependiendo como se haya instalado el webservice
    ''' </summary>
    ''' <param name="lId">id </param>
    ''' <param name="Cia">compania</param>
    ''' <remarks>Llamada desde: Root.GetSqlFileStreamForWritingAdjuntoPortal; Tiempo máximo: 0,1</remarks>
    ''' Revisada EPB 31/08/2015
    Private Sub GetPathNameAndTxContextAdjuntoAdjuntoEspecificacionCia(ByVal lId As Long, ByVal cn As SqlConnection, ByVal tx As SqlTransaction, ByRef Path As String, ByRef TxContext As Byte(), Optional ByVal Tipo As Long = 0, Optional ByVal Cia As Long = 0)
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSP_LEER_ESPECIFICACION_CIA"

        With cm.Parameters
            .AddWithValue("@ID", lId)
            .AddWithValue("@CIA", Cia)
        End With

        cm.Connection = cn
        cm.Transaction = tx

        Dim da As New SqlDataAdapter
        da.SelectCommand = cm
        Dim ds As New System.Data.DataSet
        da.Fill(ds)

        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim oRow As DataRow = ds.Tables(0).Rows(0)

            If Not oRow.IsNull("PATHNAME") Then Path = oRow("PATHNAME")
            If Not oRow.IsNull("TRANSACTIONCONTEXT") Then TxContext = CType(oRow("TRANSACTIONCONTEXT"), Byte())

            If Path Is Nothing Then
                'Si se inserta un registro con la columna varbinary(max) a NULL no se crea archivo y FilePath devuelve nulo, con lo que no se puede crear el sqlFileStream.
                'Insertando '' en la columna varbinary(max) se crea un registro de longitud cero y ya se puede obtener un handle para el archivo
                Dim oCom As New SqlCommand
                oCom.CommandType = CommandType.StoredProcedure
                oCom.CommandText = "FSPM_UPD_ESPECIFICACIONCIA"
                With oCom.Parameters
                    .AddWithValue("@ID", lId)
                    .AddWithValue("@CIA", Cia)
                End With
                oCom.Connection = cn
                oCom.Transaction = tx
                oCom.ExecuteNonQuery()

                Dim obj As Object = cm.ExecuteScalar()
                If Not obj.Equals(DBNull.Value) Then Path = DirectCast(obj, String)
            End If
        End If
    End Sub


    Public Function GrabarNuevoAdjuntoOferta(ByVal lIdAdjunto As Long, ByVal sNombreFichero As String) As Long
        Dim cn As New SqlConnection
        Dim mi_impersionationContext As Security.Principal.WindowsImpersonationContext

        cn = NuevaConexionFileStream()
        mi_impersionationContext = RealizarSuplantacion()
        cn.Open()

        Dim tx As SqlTransaction = cn.BeginTransaction()
        Dim oSqlFileStream As SqlTypes.SqlFileStream = GetSqlFileStreamForWritingAdjuntoNuevoAdjuntoOferta(lIdAdjunto, cn, tx, , )

        Dim lFileSize As Long = GrabarEnBDEnChunks(oSqlFileStream, sNombreFichero)

        tx.Commit()
        cn.Close()
        DeshacerSuplantacion(mi_impersionationContext)

        Return lFileSize
    End Function

    Private Function GetSqlFileStreamForWritingAdjuntoNuevoAdjuntoOferta(ByVal lId As Long, ByVal cn As SqlConnection, ByVal tx As SqlTransaction, Optional ByVal TipoFichero As TipoFicheroFileStream = TipoFicheroFileStream.Especificacion, Optional ByVal Cia As Long = 0) As SqlTypes.SqlFileStream
        Dim sPath As String = String.Empty
        Dim arTransactionContext() As Byte = Nothing
        GetSqlFileStreamForWritingAdjuntoNuevoAdjuntoOferta = Nothing
        GetPathNameAndTxContextAdjuntoAdjuntoNuevoAdjuntoOferta(lId, cn, tx, sPath, arTransactionContext, 0, )

        '4 - open the filestream to the blob
        If Not sPath Is Nothing And Not arTransactionContext Is Nothing Then
            GetSqlFileStreamForWritingAdjuntoNuevoAdjuntoOferta = New SqlTypes.SqlFileStream(sPath, arTransactionContext, FileAccess.Write)
        End If
    End Function


    'Lee de ADJUN de GS
    Private Sub GetPathNameAndTxContextAdjuntoAdjuntoNuevoAdjuntoOferta(ByVal lIdEFactura As Long, ByVal cn As SqlConnection, ByVal tx As SqlTransaction, ByRef Path As String, ByRef TxContext As Byte(), Optional ByVal Tipo As Long = 0, Optional ByVal Cia As Long = 0)
        Dim cm As New SqlCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSGS_LEER_ADJUNTONUEVAOFERTA"

        With cm.Parameters
            .Add("@ID", SqlDbType.Int)
            cm.Parameters("@ID").Value = lIdEFactura
        End With


        cm.Connection = cn
        cm.Transaction = tx

        Dim da As New SqlDataAdapter
        da.SelectCommand = cm
        Dim ds As New System.Data.DataSet
        da.Fill(ds)

        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim oRow As DataRow = ds.Tables(0).Rows(0)

            If Not oRow.IsNull("PATHNAME") Then Path = oRow("PATHNAME")
            If Not oRow.IsNull("TRANSACTIONCONTEXT") Then TxContext = CType(oRow("TRANSACTIONCONTEXT"), Byte())

            If Path Is Nothing Then
                'Si se inserta un registro con la columna varbinary(max) a NULL no se crea archivo y FilePath devuelve nulo, con lo que no se puede
                'crear el sqlFileStream.
                'Insertando '' en la columna varbinary(max) se crea un registro de longitud cero y ya se puede obtener un handle para el archivo
                Dim oCom As New SqlCommand
                oCom.CommandType = CommandType.Text
                oCom.CommandText = "UPDATE ADJUN SET DATA=cast('' as varbinary(max)) WHERE ID = " & lIdEFactura

                oCom.Connection = cn
                oCom.Transaction = tx
                oCom.ExecuteNonQuery()

                Dim obj As Object = cm.ExecuteScalar()
                If Not obj.Equals(DBNull.Value) Then Path = DirectCast(obj, String)
            End If
        End If
    End Sub


    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''''''''             NUEVOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Public Function Adjunto_GrabarAdjunto(ByVal sNom As String, ByVal sPath As String, ByVal Tipo As TiposDeDatos.Adjunto.Tipo, ByVal ID_Rel As String, ByVal sComentario As String, ByVal DataSize As Long) As Long()
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim arrReturn(1) As Long
        Dim lIdAdjunto As Long
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_GRABAR_ADJUNTO" 'NUEVO!!!!!!!!!!!!!!!!!!
            cm.Parameters.AddWithValue("@NOM", sNom)
            cm.Parameters.AddWithValue("@TIPO", Tipo)
            cm.Parameters.AddWithValue("@ID_REL", ID_Rel)
            cm.Parameters.AddWithValue("@COMENTARIO", sComentario)
            cm.Parameters.AddWithValue("@DATASIZE", DataSize)
            cm.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output
            cm.CommandType = CommandType.StoredProcedure
            cm.ExecuteNonQuery()

            lIdAdjunto = cm.Parameters("@ID").Value

            Dim sCarpeta As String = sPath
            Dim sNombreFichero As String = sCarpeta & "\" & sNom
            arrReturn(0) = lIdAdjunto
            If Not (sCarpeta = "" And sNom = "") Then
                arrReturn(1) = GrabarNuevoAdjunto(lIdAdjunto, sNombreFichero, Tipo, ID_Rel)
            End If
            Return arrReturn
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function

    Public Function GrabarNuevoAdjunto(ByVal lIdAdjunto As Long, ByVal sNombreFichero As String, ByVal Tipo As TiposDeDatos.Adjunto.Tipo, Optional ID_Rel As String = "") As Long
        Dim cn As New SqlConnection
        Dim mi_impersionationContext As Security.Principal.WindowsImpersonationContext

        cn = NuevaConexionFileStream()
        mi_impersionationContext = RealizarSuplantacion()
        cn.Open()

        Dim tx As SqlTransaction = cn.BeginTransaction()
        Dim oSqlFileStream As SqlTypes.SqlFileStream = GetSqlFileStreamForWritingAdjunto(lIdAdjunto, cn, tx, Tipo, ID_Rel)

        Dim lFileSize As Long = GrabarEnBDEnChunks(oSqlFileStream, sNombreFichero)

        tx.Commit()
        cn.Close()
        DeshacerSuplantacion(mi_impersionationContext)

        Return lFileSize
    End Function
    ''' <summary>
    ''' Copia archivos adjuntos de un tipo a otro tipo
    ''' </summary>
    ''' <param name="IdAdjun"> ID del adjunto a copiar</param>
    ''' <param name="TipoDestino">tipo de adjunto del destino</param>
    ''' <param name="TipoOrigen">tipo de adjunto del Origen</param>
    ''' <param name="ID_Rel">Id de la tabla de la que copiar</param>
    ''' <returns></returns>
    Public Function Adjunto_CopiarAdjunto(ByVal IdAdjun As Long, ByVal TipoDestino As TiposDeDatos.Adjunto.Tipo, ByVal TipoOrigen As TiposDeDatos.Adjunto.Tipo, ByVal ID_Rel As Long, Optional ByVal Linea As Long = 0) As Long
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_COPIAR_ADJUNTO" 'NUEVO!!!!!!!!!!!!!!!!!!
            cm.Parameters.AddWithValue("@IDORIGEN", IdAdjun)
            cm.Parameters.AddWithValue("@TIPOORIGEN", TipoOrigen)
            cm.Parameters.AddWithValue("@TIPODESTINO", TipoDestino)
            cm.Parameters.AddWithValue("@LINEA", Linea)
            cm.Parameters.AddWithValue("@ID_REL_DESTINO", ID_Rel)
            cm.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output
            cm.CommandType = CommandType.StoredProcedure
            cm.ExecuteNonQuery()
            Return cm.Parameters("@ID").Value
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    Private Function GetSqlFileStreamForWritingAdjunto(ByVal lId As Long, ByVal cn As SqlConnection, ByVal tx As SqlTransaction, ByVal Tipo As TiposDeDatos.Adjunto.Tipo, ByVal ID_Rel As String) As SqlTypes.SqlFileStream
        Dim sPath As String = String.Empty
        Dim arTransactionContext() As Byte = Nothing
        GetSqlFileStreamForWritingAdjunto = Nothing
        GetPathNameAndTxContextAdjunto(lId, cn, tx, sPath, arTransactionContext, Tipo, ID_Rel)

        '4 - open the filestream to the blob
        If Not sPath Is Nothing And Not arTransactionContext Is Nothing Then
            GetSqlFileStreamForWritingAdjunto = New SqlTypes.SqlFileStream(sPath, arTransactionContext, FileAccess.Write)
        End If
    End Function

    Private Sub GetPathNameAndTxContextAdjunto(ByVal lIdAdjunto As Long, ByVal cn As SqlConnection, ByVal tx As SqlTransaction, ByRef Path As String, ByRef TxContext As Byte(), ByVal Tipo As TiposDeDatos.Adjunto.Tipo, ByVal ID_Rel As String)
        Dim cm As New SqlCommand

        cm.CommandType = CommandType.StoredProcedure
        cm.CommandText = "FSN_LEER_ADJUNTO"        'NUEVO!!!!!!!!!!!!!!!!!!!!!!!!!!!!! MISMO STORED QUE Adjunto_LeerAdjunto
        cm.Parameters.AddWithValue("@ID", lIdAdjunto)
        cm.Parameters.AddWithValue("@TIPO", Tipo)
        cm.Parameters.AddWithValue("@ID_REL", ID_Rel)
        cm.Connection = cn
        cm.Transaction = tx

        Dim da As New SqlDataAdapter
        da.SelectCommand = cm
        Dim ds As New System.Data.DataSet
        da.Fill(ds)

        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim oRow As DataRow = ds.Tables(0).Rows(0)

            If Not oRow.IsNull("PATHNAME") Then Path = oRow("PATHNAME")
            If Not oRow.IsNull("TRANSACTIONCONTEXT") Then TxContext = CType(oRow("TRANSACTIONCONTEXT"), Byte())

            If Path Is Nothing Then
                'Si se inserta un registro con la columna varbinary(max) a NULL no se crea archivo y FilePath devuelve nulo, con lo que no se puede crear el sqlFileStream.
                'Insertando '' en la columna varbinary(max) se crea un registro de longitud cero y ya se puede obtener un handle para el archivo
                Dim oCom As New SqlCommand
                oCom.CommandType = CommandType.StoredProcedure
                oCom.CommandText = "FSN_UPD_ADJUNTO"
                oCom.Parameters.AddWithValue("@ID", lIdAdjunto)
                oCom.Parameters.AddWithValue("@TIPO", Tipo)
                oCom.Parameters.AddWithValue("@ID_REL", ID_Rel)
                oCom.Connection = cn
                oCom.Transaction = tx
                oCom.ExecuteNonQuery()

                Dim obj As Object = cm.ExecuteScalar()
                If Not obj.Equals(DBNull.Value) Then Path = DirectCast(obj, String)
            End If
        End If
    End Sub


    Public Function Adjunto_LeerAdjunto(ByVal Id As Long, ByVal Tipo As TiposDeDatos.Adjunto.Tipo, Optional ByVal ChunkNumber As Long = 0, Optional ByVal ChunkSize As Long = 0, Optional ByVal ID_Rel As String = "") As Byte()
        Authenticate()

        Dim cn As New SqlConnection
        Dim cm As SqlCommand = New SqlCommand
        Dim mi_impersionationContext As Security.Principal.WindowsImpersonationContext

        cn = NuevaConexionFileStream()
        mi_impersionationContext = RealizarSuplantacion()
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_LEER_ADJUNTO"  'NUEVO!!!!!!!!!!!!!!!!!! MISMO STORED QUE GetPathNameAndTxContextAdjunto
            cm.Parameters.AddWithValue("@ID", Id)
            cm.Parameters.AddWithValue("@TIPO", Tipo)
            cm.Parameters.AddWithValue("@ID_REL", ID_Rel)
            cm.CommandType = CommandType.StoredProcedure

            'Retrieve the file path of the SQL FILESTREAM BLOB in the first row. 
            Dim filePath As String = Nothing
            Dim pathObj As Object = cm.ExecuteScalar()
            If Not pathObj.Equals(DBNull.Value) Then
                filePath = DirectCast(pathObj, String)
            Else
                Throw New System.Exception(" failed to read the path name en Adjunto_ReadData.")
            End If

            ' Obtain a transaction context. All FILESTREAM BLOB operations occur within a transaction context to maintain data consistency. 
            Dim transaction As SqlTransaction = cn.BeginTransaction("mainTransaction")
            cm.Transaction = transaction
            cm.CommandType = CommandType.Text
            cm.CommandText = "SELECT GET_FILESTREAM_TRANSACTION_CONTEXT()"
            Dim txContext As Byte() = Nothing
            Dim obj As Object = cm.ExecuteScalar()

            If Not obj.Equals(DBNull.Value) Then
                txContext = DirectCast(obj, Byte())
            Else
                Throw New System.Exception("GET_FILESTREAM_TRANSACTION_CONTEXT() failed")
            End If

            Dim sqlFileStream As New SqlTypes.SqlFileStream(filePath, txContext, FileAccess.Read)
            Dim buffer() As Byte
            If sqlFileStream.Length > ChunkSize And ChunkNumber > 0 Then
                ReDim buffer(ChunkSize - 1)
                sqlFileStream.Seek((ChunkNumber - 1) * ChunkSize, 0)

                Dim iBytesRead As Integer = sqlFileStream.Read(buffer, 0, buffer.Length)
                ReDim Preserve buffer(iBytesRead - 1)
            Else
                ReDim buffer(sqlFileStream.Length - 1)
                sqlFileStream.Read(buffer, 0, buffer.Length)
            End If

            ' Close the FILESTREAM handle. 
            sqlFileStream.Close()

            cm.Transaction.Commit()

            DeshacerSuplantacion(mi_impersionationContext)

            Adjunto_LeerAdjunto = buffer
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function


    ''' <summary>
    ''' Abre una nueva conexion con seguridad integrada para que podamos trabajar con SQLFILESTREAM
    ''' </summary>      
    ''' <returns>Nueva conexion</returns>
    ''' <remarks>Llamada desde:GrabarNuevoAdjunto; Tiempo máximo:0seg.</remarks>
    Private Function NuevaConexionFileStream() As SqlConnection
        '*********** SQLFILESTREAM necesita seguridad integrada *****************
        Dim sDBConnection As String = "Integrated Security=true;server=" & DBServer & ";initial catalog=" & DBName
        Dim cn As SqlConnection = New SqlConnection(sDBConnection)
        Return cn
    End Function
    ''' <summary>
    ''' Para poder trabajar con FILESTREAM necesitamos suplantacion de identidad, es decir, necesitamos un usuario SQL que pueda
    ''' trabajar con este tipo de ficheros. Para ellos necesitamos que el usuario este creado con esos permisos en SQL
    ''' </summary>
    ''' <returns>Contexto de la suplantacion</returns>
    ''' <remarks>Llamada desde:GrabarNuevoAdjunto; Tiempo máximo:0seg.</remarks>
    Private Function RealizarSuplantacion() As Security.Principal.WindowsImpersonationContext
        Dim impersonationContext As Security.Principal.WindowsImpersonationContext
        If mImpersonateLogin = "" Or mImpersonatePassword = "" Then
            Err.Raise(10000, , "Corrupted LICENSE file !")
        End If
        impersonationContext = modUtilidades.RealizarSuplantacion(ImpersonateLogin, ImpersonatePassword, ImpersonateDominio)
        Return impersonationContext
    End Function
    ''' <summary>
    ''' Deshace la suplantacion
    ''' </summary>
    ''' <remarks>Llamada desde:GrabarNuevoAdjunto; Tiempo máximo:0seg.</remarks>
    Private Sub DeshacerSuplantacion(ByVal impersonationContext As Security.Principal.WindowsImpersonationContext)
        impersonationContext.Undo()
    End Sub
#End Region
#Region "Seguridad/Administrador"
    Public Function SG_Perfiles_Load(ByVal Idioma As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SG_PERFILES_LOAD"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDI", Idioma)

            da.SelectCommand = cm
            da.Fill(dr)

            dr.Tables(0).TableName = "PERFILESSEGURIDAD"
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function SG_Perfiles_Add(ByVal CodigoPerfil As String, ByVal DenominacionesPerfil As DataTable,
       ByVal AccesosPerfil As Dictionary(Of String, Boolean), ByVal AccionesPerfil As DataTable,
       ByVal UONsPerfil As DataTable, ByVal QlikUONsPerfil As DataTable, ByVal QlikGMNsPerfil As DataTable) As Integer
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter

        cn.Open()
        Dim transaccion As SqlTransaction = cn.BeginTransaction()

        Try
            cm = New SqlCommand("SG_PERFILES_ADD", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion

            With cm.Parameters
                .AddWithValue("@COD", CodigoPerfil)
                If AccesosPerfil.ContainsKey(TipoAccesoModulos.FSGS) Then .AddWithValue("@FSGS", AccesosPerfil(TipoAccesoModulos.FSGS))
                If AccesosPerfil.ContainsKey(TipoAccesoModulos.FSPM) Then .AddWithValue("@FSPM", AccesosPerfil(TipoAccesoModulos.FSPM))
                If AccesosPerfil.ContainsKey(TipoAccesoModulos.FSQA) Then .AddWithValue("@FSQA", AccesosPerfil(TipoAccesoModulos.FSQA))
                If AccesosPerfil.ContainsKey(TipoAccesoModulos.FSEP) Then .AddWithValue("@FSEP", AccesosPerfil(TipoAccesoModulos.FSEP))
                If AccesosPerfil.ContainsKey(TipoAccesoModulos.FSSM) Then .AddWithValue("@FSSM", AccesosPerfil(TipoAccesoModulos.FSSM))
                If AccesosPerfil.ContainsKey(TipoAccesoModulos.FSCM) Then .AddWithValue("@FSCM", AccesosPerfil(TipoAccesoModulos.FSCM))
                If AccesosPerfil.ContainsKey(TipoAccesoModulos.FSIM) Then .AddWithValue("@FSIM", AccesosPerfil(TipoAccesoModulos.FSIM))
                If AccesosPerfil.ContainsKey(TipoAccesoModulos.FSIS) Then .AddWithValue("@FSIS", AccesosPerfil(TipoAccesoModulos.FSIS))
                If AccesosPerfil.ContainsKey(TipoAccesoModulos.FSBI) Then .AddWithValue("@FSBI", AccesosPerfil(TipoAccesoModulos.FSBI))
                If AccesosPerfil.ContainsKey(TipoAccesoModulos.FSAL) Then .AddWithValue("@FSAL", AccesosPerfil(TipoAccesoModulos.FSAL))
                .Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output
            End With
            cm.ExecuteNonQuery()
            Dim Id As Integer = cm.Parameters("@ID").Value

            cm = New SqlCommand("SG_PERFILES_ADD_DENOMINACIONES", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@PERF", Id)
                .Add("@IDI", SqlDbType.NVarChar, 20, "IDI")
                .Add("@DEN", SqlDbType.NVarChar, 200, "DEN")
            End With
            da.InsertCommand = cm
            da.Update(DenominacionesPerfil)

            cm = New SqlCommand("SG_PERFILES_DELETE_ACCIONES", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@PERF", Id)
            End With
            cm.ExecuteNonQuery()

            cm = New SqlCommand("SG_PERFILES_UPDATE_ACCIONES", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@PERF", Id)
                .Add("@ACC", SqlDbType.Int, 20, "ACC")
            End With
            da.InsertCommand = cm
            da.Update(AccionesPerfil)

            cm = New SqlCommand("SG_PERFILES_UPDATE_UONS", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@PERF", Id)
                .Add("@UON1", SqlDbType.NVarChar, 100, "UON1")
                .Add("@UON2", SqlDbType.NVarChar, 100, "UON2")
                .Add("@UON3", SqlDbType.NVarChar, 100, "UON3")
            End With
            da.InsertCommand = cm
            da.Update(UONsPerfil)

            cm = New SqlCommand("SG_PERFILES_UPDATE_QLIKUONS", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@PERF", Id)
                .Add("@UON1", SqlDbType.NVarChar, 100, "UON1")
                .Add("@UON2", SqlDbType.NVarChar, 100, "UON2")
                .Add("@UON3", SqlDbType.NVarChar, 100, "UON3")
            End With
            da.InsertCommand = cm
            da.Update(QlikUONsPerfil)

            cm = New SqlCommand("SG_PERFILES_UPDATE_QLIKGMNS", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@PERF", Id)
                .Add("@GMN1", SqlDbType.NVarChar, 100, "GMN1")
                .Add("@GMN2", SqlDbType.NVarChar, 100, "GMN2")
                .Add("@GMN3", SqlDbType.NVarChar, 100, "GMN3")
                .Add("@GMN4", SqlDbType.NVarChar, 100, "GMN4")
            End With
            da.InsertCommand = cm
            da.Update(QlikGMNsPerfil)

            transaccion.Commit()
            Return Id
        Catch e As Exception
            transaccion.Rollback()
            Return 0
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    Public Sub SG_Perfiles_Update(ByVal IdPerfil As Integer, ByVal CodigoPerfil As String, ByVal DenominacionesPerfil As DataTable,
       ByVal AccesosPerfil As Dictionary(Of String, Boolean), ByVal AccionesPerfil As DataTable,
       ByVal UONsPerfil As DataTable, ByVal QlikUONsPerfil As DataTable, ByVal QlikGMNsPerfil As DataTable, ByVal UNQA As DataTable)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter

        cn.Open()
        Dim transaccion As SqlTransaction = cn.BeginTransaction()

        Try
            cm = New SqlCommand("SG_PERFILES_UPDATE", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion

            With cm.Parameters
                .AddWithValue("@IDPERF", IdPerfil)
                .AddWithValue("@COD", CodigoPerfil)
                If AccesosPerfil.ContainsKey(TipoAccesoModulos.FSGS) Then .AddWithValue("@FSGS", AccesosPerfil(TipoAccesoModulos.FSGS))
                If AccesosPerfil.ContainsKey(TipoAccesoModulos.FSPM) Then .AddWithValue("@FSPM", AccesosPerfil(TipoAccesoModulos.FSPM))
                If AccesosPerfil.ContainsKey(TipoAccesoModulos.FSQA) Then .AddWithValue("@FSQA", AccesosPerfil(TipoAccesoModulos.FSQA))
                If AccesosPerfil.ContainsKey(TipoAccesoModulos.FSEP) Then .AddWithValue("@FSEP", AccesosPerfil(TipoAccesoModulos.FSEP))
                If AccesosPerfil.ContainsKey(TipoAccesoModulos.FSSM) Then .AddWithValue("@FSSM", AccesosPerfil(TipoAccesoModulos.FSSM))
                If AccesosPerfil.ContainsKey(TipoAccesoModulos.FSCM) Then .AddWithValue("@FSCM", AccesosPerfil(TipoAccesoModulos.FSCM))
                If AccesosPerfil.ContainsKey(TipoAccesoModulos.FSIM) Then .AddWithValue("@FSIM", AccesosPerfil(TipoAccesoModulos.FSIM))
                If AccesosPerfil.ContainsKey(TipoAccesoModulos.FSIS) Then .AddWithValue("@FSIS", AccesosPerfil(TipoAccesoModulos.FSIS))
                If AccesosPerfil.ContainsKey(TipoAccesoModulos.FSBI) Then .AddWithValue("@FSBI", AccesosPerfil(TipoAccesoModulos.FSBI))
                If AccesosPerfil.ContainsKey(TipoAccesoModulos.FSAL) Then .AddWithValue("@FSAL", AccesosPerfil(TipoAccesoModulos.FSAL))
            End With
            cm.ExecuteNonQuery()

            cm = New SqlCommand("SG_PERFILES_DELETE_DENOMINACIONES", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@PERF", IdPerfil)
            End With
            cm.ExecuteNonQuery()

            cm = New SqlCommand("SG_PERFILES_ADD_DENOMINACIONES", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@PERF", IdPerfil)
                .Add("@IDI", SqlDbType.NVarChar, 20, "IDI")
                .Add("@DEN", SqlDbType.NVarChar, 200, "DEN")
            End With
            da.InsertCommand = cm
            da.Update(DenominacionesPerfil)

            cm = New SqlCommand("SG_PERFILES_DELETE_ACCIONES", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@PERF", IdPerfil)
            End With
            cm.ExecuteNonQuery()

            cm = New SqlCommand("SG_PERFILES_UPDATE_ACCIONES", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@PERF", IdPerfil)
                .Add("@ACC", SqlDbType.Int, 20, "ACC")
            End With
            da.InsertCommand = cm
            da.Update(AccionesPerfil)

            cm = New SqlCommand("SG_PERFILES_DELETE_UONS", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@PERF", IdPerfil)
            End With
            cm.ExecuteNonQuery()

            cm = New SqlCommand("SG_PERFILES_UPDATE_UONS", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@PERF", IdPerfil)
                .Add("@UON1", SqlDbType.NVarChar, 100, "UON1")
                .Add("@UON2", SqlDbType.NVarChar, 100, "UON2")
                .Add("@UON3", SqlDbType.NVarChar, 100, "UON3")
            End With
            da.InsertCommand = cm
            da.Update(UONsPerfil)

            cm = New SqlCommand("SG_PERFILES_DELETE_QLIKUONS", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@PERF", IdPerfil)
            End With
            cm.ExecuteNonQuery()

            cm = New SqlCommand("SG_PERFILES_UPDATE_QLIKUONS", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@PERF", IdPerfil)
                .Add("@UON1", SqlDbType.NVarChar, 100, "UON1")
                .Add("@UON2", SqlDbType.NVarChar, 100, "UON2")
                .Add("@UON3", SqlDbType.NVarChar, 100, "UON3")
            End With
            da.InsertCommand = cm
            da.Update(QlikUONsPerfil)

            cm = New SqlCommand("SG_PERFILES_DELETE_QLIKGMNS", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@PERF", IdPerfil)
            End With
            cm.ExecuteNonQuery()

            cm = New SqlCommand("SG_PERFILES_UPDATE_QLIKGMNS", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@PERF", IdPerfil)
                .Add("@GMN1", SqlDbType.NVarChar, 100, "GMN1")
                .Add("@GMN2", SqlDbType.NVarChar, 100, "GMN2")
                .Add("@GMN3", SqlDbType.NVarChar, 100, "GMN3")
                .Add("@GMN4", SqlDbType.NVarChar, 100, "GMN4")
            End With
            da.InsertCommand = cm
            da.Update(QlikGMNsPerfil)

            cm = New SqlCommand("SG_PERFILES_DELETE_UNQA", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@PERF", IdPerfil)
            End With
            cm.ExecuteNonQuery()

            cm = New SqlCommand("SG_PERFILES_UPDATE_UNQA", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@PERF", IdPerfil)
                .Add("@UNQA", SqlDbType.NVarChar, 100, "UNQA")
            End With
            da.InsertCommand = cm
            da.Update(UNQA)

            transaccion.Commit()
        Catch e As Exception
            transaccion.Rollback()
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    Public Sub SG_Perfiles_Update_Acceso(ByVal IdPerfil As Integer, ByVal TipoAcceso As String, ByVal TieneAcceso As Boolean)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SG_PERFILES_UPDATE_ACCESO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDPERFIL", IdPerfil)
            cm.Parameters.AddWithValue("@TIPOACCESO", TipoAcceso)
            cm.Parameters.AddWithValue("@TIENEACCESO", TieneAcceso)
            cm.ExecuteNonQuery()
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Sub
    Public Function SG_Perfiles_ComprobarPosibleEliminarPerfil(ByVal IdPerfil As Integer) As Boolean
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SG_PERFILES_COMPROBAR_POSIBLEELIMINARPERFIL"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDPERFIL", IdPerfil)
            cm.Parameters.Add("@POSIBLEELIMINAR", SqlDbType.Int).Direction = ParameterDirection.Output
            cm.ExecuteNonQuery()

            Return CType(cm.Parameters("@POSIBLEELIMINAR").Value, Boolean)
        Catch e As Exception
            Return False
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Sub SG_Perfiles_Delete(ByVal IdPerfil As Integer)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SG_PERFILES_DELETE"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDPERFIL", IdPerfil)
            cm.ExecuteNonQuery()
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Sub
    Public Function SG_Perfiles_Obtener_Detalle(ByVal idPerfil As Integer, ByVal Idioma As String, ByVal Modulos As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SG_PERFILES_DETALLE_PERFIL"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDPERFIL", idPerfil)
            cm.Parameters.AddWithValue("@IDI", Idioma)
            cm.Parameters.AddWithValue("@MODULOS", Modulos)

            da.SelectCommand = cm
            da.Fill(dr)

            'RELATIONS
            Dim parentCols(0) As DataColumn
            Dim childCols(0) As DataColumn
            parentCols(0) = dr.Tables(7).Columns("COD")
            childCols(0) = dr.Tables(8).Columns("UON1")
            dr.Relations.Add("REL_NIV1_NIV2", parentCols, childCols, False)

            ReDim parentCols(1)
            ReDim childCols(1)
            parentCols(0) = dr.Tables(8).Columns("UON1")
            parentCols(1) = dr.Tables(8).Columns("COD")
            childCols(0) = dr.Tables(9).Columns("UON1")
            childCols(1) = dr.Tables(9).Columns("UON2")
            dr.Relations.Add("REL_NIV2_NIV3", parentCols, childCols, False)

            ReDim parentCols(0)
            ReDim childCols(0)
            parentCols(0) = dr.Tables(13).Columns("COD")
            childCols(0) = dr.Tables(14).Columns("GMN1")
            dr.Relations.Add("REL_NIV1_NIV2_GMNS", parentCols, childCols, False)

            ReDim parentCols(1)
            ReDim childCols(1)
            parentCols(0) = dr.Tables(14).Columns("GMN1")
            parentCols(1) = dr.Tables(14).Columns("COD")
            childCols(0) = dr.Tables(15).Columns("GMN1")
            childCols(1) = dr.Tables(15).Columns("GMN2")
            dr.Relations.Add("REL_NIV2_NIV3_GMNS", parentCols, childCols, False)

            ReDim parentCols(2)
            ReDim childCols(2)
            parentCols(0) = dr.Tables(15).Columns("GMN1")
            parentCols(1) = dr.Tables(15).Columns("GMN2")
            parentCols(2) = dr.Tables(15).Columns("COD")
            childCols(0) = dr.Tables(16).Columns("GMN1")
            childCols(1) = dr.Tables(16).Columns("GMN2")
            childCols(2) = dr.Tables(16).Columns("GMN3")
            dr.Relations.Add("REL_NIV3_NIV4_GMNS", parentCols, childCols, False)

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function SG_Perfiles_CompararPerfiles(ByVal IdsPerfiles As String, ByVal ModulosActivos As String,
                ByVal Idioma As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SG_PERFILES_COMPARAR"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDIOMA", Idioma)
            cm.Parameters.AddWithValue("@IDSPERFILES", IdsPerfiles)
            cm.Parameters.AddWithValue("@MODULOSACTIVOS", ModulosActivos)

            da.SelectCommand = cm
            da.Fill(dr)

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function SG_Comprobar_CodigoPerfilValido(ByVal IdPerfil As Integer, ByVal CodPerfil As String) As DataTable
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SG_PERFILES_COMPROBAR_CODIGOVALIDO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@ID", IdPerfil)
            cm.Parameters.AddWithValue("@COD", CodPerfil)

            da.SelectCommand = cm
            da.Fill(dr)
            Return dr.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
#End Region
#Region "Propiedades Mail"
    Private mSmtpPortal As Boolean = False
    ''' <summary>
    ''' Da acceso la propiedad mSmtpPortal
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb/New ; Tiempo maximo: 0,2</remarks>
    Property SmtpPortal() As Boolean
        Get
            Return mSmtpPortal
        End Get
        Set(ByVal Value As Boolean)
            mSmtpPortal = Value
        End Set
    End Property
    Private mSmtpAuthentication As Integer = 0
    ''' <summary>
    ''' Da acceso la propiedad mSmtpAuthentication y carga desde bbdd todos los datos del servidor correo si mSmtpServer es vacio
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb/New ; Tiempo maximo: 0,2</remarks>
    ReadOnly Property SmtpAuthentication() As Integer
        Get
            If mSmtpServer = "" Then
                ParametrosSmtp()
            End If
            Return mSmtpAuthentication
        End Get
    End Property
    Private mSmtpUsu As String = ""
    ''' <summary>
    ''' Da acceso la propiedad mSmtpUsu y carga desde bbdd todos los datos del servidor correo si mSmtpServer es vacio
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb/New ; Tiempo maximo: 0,2</remarks>
    ReadOnly Property SmtpUsu() As String
        Get
            If mSmtpServer = "" Then
                ParametrosSmtp()
            End If
            Return mSmtpUsu
        End Get
    End Property
    Private mSmtpPassword As String = ""
    ''' <summary>
    ''' Da acceso la propiedad mSmtpPassword y carga desde bbdd todos los datos del servidor correo si mSmtpServer es vacio
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb/New ; Tiempo maximo: 0,2</remarks>
    ReadOnly Property SmtpPassword() As String
        Get
            If mSmtpServer = "" Then
                ParametrosSmtp()
            End If
            Return mSmtpPassword
        End Get
    End Property
    Private mSmtpCuentaServicio As String = ""
    ''' <summary>
    ''' Da acceso la propiedad mSmtpCuentaServicio y carga desde bbdd todos los datos del servidor correo si mSmtpServer es vacio
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb/New ; Tiempo maximo: 0,2</remarks>
    ReadOnly Property SmtpCuentaServicio() As String
        Get
            If mSmtpServer = "" Then
                ParametrosSmtp()
            End If
            Return mSmtpCuentaServicio
        End Get
    End Property
    Private mSmtpDominio As String = ""
    ''' <summary>
    ''' Da acceso la propiedad mSmtpDominio y carga desde bbdd todos los datos del servidor correo si mSmtpServer es vacio
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb/New ; Tiempo maximo: 0,2</remarks>
    ReadOnly Property SmtpDominio() As String
        Get
            If mSmtpServer = "" Then
                ParametrosSmtp()
            End If
            Return mSmtpDominio
        End Get
    End Property
    Private mSmtpServer As String = ""
    ''' <summary>
    ''' Da acceso la propiedad mSmtpServer y carga desde bbdd todos los datos del servidor correo si mSmtpServer es vacio
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb/New ; Tiempo maximo: 0,2</remarks>
    ReadOnly Property SmtpServer() As String
        Get
            If mSmtpServer = "" Then
                ParametrosSmtp()
            End If
            Return mSmtpServer
        End Get
    End Property
    Private mSmtpPort As Integer = 25
    ''' <summary>
    ''' Da acceso la propiedad mSmtpPort y carga desde bbdd todos los datos del servidor correo si mSmtpServer es vacio
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb/New ; Tiempo maximo: 0,2</remarks>
    ReadOnly Property SmtpPort() As Integer
        Get
            If mSmtpServer = "" Then
                ParametrosSmtp()
            End If
            Return mSmtpPort
        End Get
    End Property
    Private mSmtpEnableSsl As Integer = 0
    ''' <summary>
    ''' Da acceso la propiedad mSmtpEnableSsl y carga desde bbdd todos los datos del servidor correo si mSmtpServer es vacio
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb/New ; Tiempo maximo: 0,2</remarks>
    ReadOnly Property SmtpEnableSsl() As Integer
        Get
            If mSmtpServer = "" Then
                ParametrosSmtp()
            End If
            Return mSmtpEnableSsl
        End Get
    End Property
    Private mSmtpDeliveryMethod As Integer = 0
    ''' <summary>
    ''' Da acceso la propiedad mSmtpDeliveryMethod y carga desde bbdd todos los datos del servidor correo si mSmtpServer es vacio
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb/New ; Tiempo maximo: 0,2</remarks>
    ReadOnly Property SmtpDeliveryMethod() As Integer
        Get
            If mSmtpServer = "" Then
                ParametrosSmtp()
            End If
            Return mSmtpDeliveryMethod
        End Get
    End Property
#Region "Sacado de EpNotificar"
    Private mMailPortalIDIOMA As String = ""
    ''' <summary>
    ''' Da acceso la propiedad mMailPortalIDIOMA
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb/New ; Tiempo maximo: 0,2</remarks>
    ReadOnly Property MailPortalIDIOMA() As String
        Get
            Return mMailPortalIDIOMA
        End Get
    End Property
    Private mMailPortalMON As String = ""
    ''' <summary>
    ''' Da acceso la propiedad mMailPortalMON
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb/New ; Tiempo maximo: 0,2</remarks>
    ReadOnly Property MailPortalMON() As String
        Get
            Return mMailPortalMON
        End Get
    End Property
    Private mMailPortalMONDEN As String = ""
    ''' <summary>
    ''' Da acceso la propiedad mMailPortalMONDEN
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb/New ; Tiempo maximo: 0,2</remarks>
    ReadOnly Property MailPortalMONDEN() As String
        Get
            Return mMailPortalMONDEN
        End Get
    End Property
    Private mMailPortalEQUIV As Long = 1
    ''' <summary>
    ''' Da acceso la propiedad mMailPortalEQUIV
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb/New ; Tiempo maximo: 0,2</remarks>
    ReadOnly Property MailPortalEQUIV() As Long
        Get
            Return mMailPortalEQUIV
        End Get
    End Property
    Private mMailPortalINSTWEB As Integer = 1
    ''' <summary>
    ''' Da acceso la propiedad mMailPortalINSTWEB
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb/New ; Tiempo maximo: 0,2</remarks>
    ReadOnly Property MailPortalINSTWEB() As Integer
        Get
            Return mMailPortalINSTWEB
        End Get
    End Property
    Private mMailPortalFSP_BD As String = ""
    ''' <summary>
    ''' Da acceso la propiedad mMailPortalFSP_BD
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb/New ; Tiempo maximo: 0,2</remarks>
    ReadOnly Property MailPortalFSP_BD() As String
        Get
            Return mMailPortalFSP_BD
        End Get
    End Property
    Private mMailPortalFSP_SRV As String = ""
    ''' <summary>
    ''' Da acceso la propiedad mMailPortalFSP_SRV
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb/New ; Tiempo maximo: 0,2</remarks>
    ReadOnly Property MailPortalFSP_SRV() As String
        Get
            Return mMailPortalFSP_SRV
        End Get
    End Property
    Private mMailPortalFSP_CIA As String = ""
    ''' <summary>
    ''' Da acceso la propiedad mMailPortalFSP_CIA
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb/New ; Tiempo maximo: 0,2</remarks>
    ReadOnly Property MailPortalFSP_CIA() As String
        Get
            Return mMailPortalFSP_CIA
        End Get
    End Property
    Private mMailPortalFSP_CIAID As Long = Nothing
    ''' <summary>
    ''' Da acceso la propiedad mMailPortalFSP_CIAID
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb/New ; Tiempo maximo: 0,2</remarks>
    ReadOnly Property MailPortalFSP_CIAID() As Long
        Get
            Return mMailPortalFSP_CIAID
        End Get
    End Property
    Private mUsuarioConectado As String = ""
    ''' <summary>
    ''' Da acceso la propiedad mUsuarioConectado
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb/New ; Tiempo maximo: 0,2</remarks>
    ReadOnly Property UsuarioConectado() As String
        Get
            Return mUsuarioConectado
        End Get
    End Property
#End Region
    ''' <summary>
    ''' Cargar los parametros necesarios para notificar
    ''' </summary>
    ''' <remarks>Llamada desde: FSNServer/Notificar.vb/New (Realmente dbserver.SmtpServer pq ve q mSmtpServer es vacio y entiende q hay q cargar) ; Tiempo maximo: 0,2</remarks>
    Private Sub ParametrosSmtp()
        'General
        If Not mSmtpPortal Then CargaPargenMail()

        'Sacado de EP
        Dim dr As DataRow = Parametros_Portal()
        mMailPortalIDIOMA = dr.Item("IDIOMA")
        mMailPortalMON = dr.Item("MON")
        mMailPortalMONDEN = dr.Item("MONDEN")
        mMailPortalEQUIV = dr.Item("EQUIV")
        mMailPortalINSTWEB = dr.Item("INSTWEB")
        mMailPortalFSP_BD = IIf(IsDBNull(dr.Item("FSP_BD")), String.Empty, dr.Item("FSP_BD"))
        mMailPortalFSP_SRV = IIf(IsDBNull(dr.Item("FSP_SRV")), String.Empty, dr.Item("FSP_SRV"))
        mMailPortalFSP_CIA = IIf(IsDBNull(dr.Item("FSP_CIA")), String.Empty, dr.Item("FSP_CIA"))
        mMailPortalFSP_CIAID = IIf(IsDBNull(dr.Item("FSP_CIAID")), Nothing, dr.Item("FSP_CIAID"))

        'General
        If mSmtpPortal Then CargaPargenMail_Portal()
    End Sub
    ''' <summary>
    ''' Cargar los parametros necesarios para notificar desde bbdd del GS 
    ''' </summary>
    ''' <remarks>Llamada desde: ParametrosSmtp ; Tiempo maximo: 0,2</remarks>
    Private Sub CargaPargenMail()
        Dim cn As New SqlConnection(mDBConnection)
        Dim dr As SqlDataReader = Nothing
        Dim cm As New SqlCommand
        Try
            cm.Connection = cn
            cm.CommandText = "FSGS_DEVOLVER_PARAM_CONFIG_SERVICIOEMAIL"
            cm.CommandType = CommandType.StoredProcedure
            cn.Open()
            dr = cm.ExecuteReader()

            dr.Read()

            mSmtpAuthentication = DBNullToInteger(dr.Item("AUTENTICACION"))
            If mSmtpAuthentication = 2 Then 'windows
                mSmtpUsu = dr.Item("USU")
                mSmtpPassword = Encrypter.Encrypt(dr.Item("PWD"), dr.Item("USU"), False, Encrypter.TipoDeUsuario.Persona, dr.Item("FECPWD"))
            ElseIf mSmtpAuthentication = 1 Then 'basica
                mSmtpUsu = dr.Item("CUENTAMAIL")
                mSmtpPassword = Encrypter.Encrypt(dr.Item("PWD"), dr.Item("CUENTAMAIL"), False, Encrypter.TipoDeUsuario.Persona, dr.Item("FECPWD"))
            Else
                mSmtpUsu = ""
                mSmtpPassword = ""
            End If
            mSmtpCuentaServicio = DBNullToStr(dr.Item("CUENTAMAIL"))
            mSmtpDominio = DBNullToStr(dr.Item("DOMINIO"))
            mSmtpServer = DBNullToStr(dr.Item("SERVIDOR"))
            mSmtpPort = DBNullToDbl(dr.Item("PUERTO"))
            mSmtpEnableSsl = DBNullToInteger(dr.Item("SSL"))
            mSmtpDeliveryMethod = DBNullToInteger(dr.Item("METODO_ENTREGA"))
        Catch ex As Exception
            Throw ex
        Finally
            dr.Close()
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub

    ''' <summary>
    ''' Cargar los parametros necesarios para notificar desde bbdd del Portal 
    ''' </summary>
    ''' <remarks>Llamada desde: ParametrosSmtp ; Tiempo maximo: 0,2</remarks>
    Private Sub CargaPargenMail_Portal()
        Dim cn As New SqlConnection(mDBConnection)
        Dim dr As SqlDataReader = Nothing
        Dim cm As New SqlCommand
        Try
            Dim sFSP As String = mMailPortalFSP_SRV & "." & mMailPortalFSP_BD & ".dbo."
            cm.Connection = cn
            cm.CommandText = sFSP & "FSP_DEVOLVER_PARAM_CONFIG_SERVICIOEMAIL"
            cm.CommandType = CommandType.StoredProcedure
            cn.Open()
            dr = cm.ExecuteReader()

            dr.Read()

            mSmtpAuthentication = DBNullToInteger(dr.Item("P_AUTENTICACION"))
            If mSmtpAuthentication = 1 Then
                mSmtpUsu = dr.Item("P_FROM")
                mSmtpPassword = Encrypter.DesEncriptarServidorCorreo("PWD", dr.Item("P_PWD"))
            ElseIf mSmtpAuthentication = 2 Then
                mSmtpUsu = dr.Item("P_FROM")
                mSmtpPassword = Encrypter.DesEncriptarServidorCorreo("PWD", dr.Item("P_PWD"))
            Else
                mSmtpUsu = ""
                mSmtpPassword = ""
            End If
            mSmtpCuentaServicio = DBNullToStr(dr.Item("P_FROM"))
            mSmtpDominio = DBNullToStr(dr.Item("P_DOMINIO"))
            mSmtpServer = DBNullToStr(dr.Item("P_SERVIDOR"))
            mSmtpPort = DBNullToDbl(dr.Item("P_PUERTO"))
            mSmtpEnableSsl = DBNullToInteger(dr.Item("P_SSL"))
            mSmtpDeliveryMethod = DBNullToInteger(dr.Item("P_METODO_ENTREGA"))
        Catch ex As Exception
            Throw ex
        Finally
            dr.Close()
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
#End Region
#Region "Servicio"
    ''' <summary>
    ''' Devuelve los datos de un servicio externo
    ''' </summary>
    ''' <param name="Servicio">Id del servicio</param>
    ''' <param name="CampoId">Id del campo</param>
    ''' <param name="Instancia">Instancia</param>
    ''' <param name="Peticionario">Usuario conectado</param>
    ''' <returns>DataSet</returns>
    ''' <remarks>Llamada desde: FSNserver/Servicio.vb/LoadData; Tiempo mÃ¡ximo: 0,2sg</remarks>
    Public Function Servicio_Load(ByVal Servicio As String, ByVal CampoId As String, ByVal Instancia As Long, Optional ByVal Peticionario As String = "") As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSPM_OBTENER_SERVICIO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@SERVICIO", Servicio)
            cm.Parameters.AddWithValue("@CAMPOID", CampoId)
            cm.Parameters.AddWithValue("@INSTANCIA", Instancia)
            cm.Parameters.AddWithValue("@PETICIONARIO", Peticionario)
            da.SelectCommand = cm
            da.Fill(dr)

            dr.Tables(0).TableName = "URL"
            dr.Tables(1).TableName = "PARAM_ENTRADA"
            dr.Tables(2).TableName = "PARAM_SALIDA"
            If dr.Tables.Count > 3 Then
                dr.Tables(3).TableName = "PARAM_PETICIONARIO"
                dr.Tables(4).TableName = "PARAMETROS_ENTRADA_GENERICOS"
            End If

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
#End Region
    ''' <summary>
    ''' Actualiza la cadena de conexiÓn a partir de los datos en BD
    ''' </summary>
    ''' <remarks></remarks>
    Sub ActualizarCadenaConexion()
        mDBConnection = "data source=" & DBServer & ";initial catalog=" & DBName & ";user id=" & DBLogin & ";password=" & DBPassword
    End Sub

    ''' <summary>Genera un nuevo registro en la tabla LOG con los datos indicados</summary>
    ''' <param name="sCodUsu">Cod. usuario</param>
    ''' <param name="Acc">Id accion</param>
    ''' <param name="sDat">Datos</param>
    Public Sub RegistrarAccion(ByVal sCodUsu As String, ByVal Acc As Integer, ByVal sDat As String)
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_REGISTRAR_ACCION"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", sCodUsu)
            cm.Parameters.AddWithValue("@ACC", Acc)
            cm.Parameters.AddWithValue("@DAT", sDat)
            cm.Parameters.AddWithValue("@FEC", Date.Now)

            cm.ExecuteNonQuery()

        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
End Class