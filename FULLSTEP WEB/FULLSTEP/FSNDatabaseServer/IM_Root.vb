﻿Imports System.Configuration
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Exception
Imports System.IO
Imports System.Object
Imports Fullstep.FSNLibrary
Partial Public Class Root
#Region "IM. Data Access Methods "
#Region "CFacturae data access methods "
    ''' <summary>
    ''' Se extrae información general de la factura
    ''' </summary>
    ''' <param name="idFact">Identificador de la factura de la cual se extraerán los datos</param>
    ''' <returns>DataTable con la información solicitada</returns>
    ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.obtDatosBD
    ''' Tiempo máximo: menor a 0,5 seg Revisado por: auv. 30/05/2012</remarks>
    Public Function obtDatosFactura(ByRef idFact As String) As DataTable
        Dim cnn As New SqlConnection(mDBConnection)
        Dim dt As DataTable
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter = Nothing
        Try
            cnn.Open()
            cmd.Connection = cnn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "FSPM_GET_DATOS_GENE_FACTURA"

            cmd.Parameters.Add("@FACT_ID", SqlDbType.Int)
            cmd.Parameters("@FACT_ID").Direction = ParameterDirection.Input
            cmd.Parameters("@FACT_ID").Value = idFact

            da = New SqlDataAdapter(cmd)
            dt = New DataTable
            da.Fill(dt)
            Return dt
        Catch e As Exception
            Throw e
        Finally
            cnn.Dispose()
            cmd.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Se extrae información relativa a los costes y descuentos a nivel de factura de la factura parámetro
    ''' </summary>
    ''' <param name="idFact">Identificador de la factura de la cual se extraerán los datos</param>
    ''' <returns>DataTable con la información solicitada</returns>
    ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.obtDatosBD
    ''' Tiempo máximo: menor a 0,5 seg Revisado por: auv. 30/05/2012</remarks>
    Public Function obtCostesYDescuentosFactura(ByVal idFact As String) As DataTable
        Dim cnn As New SqlConnection(mDBConnection)
        Dim dt As DataTable
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter = Nothing
        Try
            cnn.Open()
            cmd.Connection = cnn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "FSPM_GET_COSTYDESC_FACTURA"

            cmd.Parameters.Add("@FACT_ID", SqlDbType.Int)
            cmd.Parameters("@FACT_ID").Direction = ParameterDirection.Input
            cmd.Parameters("@FACT_ID").Value = idFact

            da = New SqlDataAdapter(cmd)
            dt = New DataTable
            da.Fill(dt)
            Return dt
        Catch e As Exception
            Throw e
        Finally
            cnn.Dispose()
            cmd.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Se extrae información relativa a los impuestos de la factura parámetro
    ''' </summary>
    ''' <param name="idFact">Identificador de la factura de la cual se extraerán los datos</param>
    ''' <returns>DataTable con la información solicitada</returns>
    ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.obtDatosBD
    ''' Tiempo máximo: menor a 0,5 seg Revisado por: auv. 30/05/2012</remarks>
    Public Function obtImpuestosRepercutidosRetenidosFactura(ByVal idFact As String) As DataTable
        Dim cnn As New SqlConnection(mDBConnection)
        Dim dt As DataTable
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter= Nothing
        Try
            cnn.Open()
            cmd.Connection = cnn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "FSPM_GET_IMP_REPYRET_FACTURA"

            cmd.Parameters.Add("@FACT_ID", SqlDbType.Int)
            cmd.Parameters("@FACT_ID").Direction = ParameterDirection.Input
            cmd.Parameters("@FACT_ID").Value = idFact

            da = New SqlDataAdapter(cmd)
            dt = New DataTable
            da.Fill(dt)
            Return dt
        Catch e As Exception
            Throw e
        Finally
            cnn.Dispose()
            cmd.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Se extrae la información general del proveedor, el comprador, de la factura parámetro
    ''' </summary>
    ''' <param name="idFact">Identificador de la factura de la cual se extraerán los datos</param>
    ''' <param name="psIdioma">Algunos datos a extraer son multiidioma y deben extraerse en el idioma parámetro</param>
    ''' <returns>DataTable con la información solicitada</returns>
    ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.obtDatosBD
    ''' Tiempo máximo: menor a 0,5 seg Revisado por: auv. 30/05/2012</remarks>
    Public Function obtDatosGeneralesProveedor(ByVal idFact As String, ByVal psIdioma As String) As DataTable
        Dim cnn As New SqlConnection(mDBConnection)
        Dim dt As DataTable
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter= Nothing
        Try
            cnn.Open()
            cmd.Connection = cnn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "FSPM_GET_DATOS_GENE_PROVE_FACTURA"

            cmd.Parameters.Add("@FACT_ID", SqlDbType.Int)
            cmd.Parameters("@FACT_ID").Direction = ParameterDirection.Input
            cmd.Parameters("@FACT_ID").Value = idFact

            cmd.Parameters.Add("@FS_IDI_COD", SqlDbType.NVarChar, 3)
            cmd.Parameters("@FS_IDI_COD").Direction = ParameterDirection.Input
            cmd.Parameters("@FS_IDI_COD").Value = psIdioma

            da = New SqlDataAdapter(cmd)
            dt = New DataTable
            da.Fill(dt)
            Return dt
        Catch e As Exception
            Throw e
        Finally
            cnn.Dispose()
            cmd.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Se extrae la información de contacto del proveedor, el comprador, de la factura parámetro
    ''' </summary>
    ''' <param name="idFact">Identificador de la factura de la cual se extraerán los datos</param>
    ''' <returns>DataTable con la información solicitada</returns>
    ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.obtDatosBD
    ''' Tiempo máximo: menor a 0,5 seg Revisado por: auv. 30/05/2012</remarks>
    Public Function obtDatosDeContactoProveedor(ByVal idFact As String)
        Dim cnn As New SqlConnection(mDBConnection)
        Dim dt As DataTable
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter= Nothing
        Try
            cnn.Open()
            cmd.Connection = cnn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "FSPM_GET_DATOS_CONT_PROVE_FACTURA"

            cmd.Parameters.Add("@FACT_ID", SqlDbType.Int)
            cmd.Parameters("@FACT_ID").Direction = ParameterDirection.Input
            cmd.Parameters("@FACT_ID").Value = idFact

            da = New SqlDataAdapter(cmd)
            dt = New DataTable
            da.Fill(dt)
            Return dt
        Catch e As Exception
            Throw e
        Finally
            cnn.Dispose()
            cmd.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    '''  Se extrae la información general de la empresa, el comprador, de la factura parámetro
    ''' </summary>
    ''' <param name="idFact">Identificador de la factura de la cual se extraerán los datos</param>
    ''' <param name="psIdioma">Algunos datos a extraer son multiidioma y deben extraerse en el idioma parámetro</param>
    ''' <returns>DataTable con la información solicitada</returns>
    ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.obtDatosBD
    ''' Tiempo máximo: menor a 0,5 seg Revisado por: auv. 30/05/2012</remarks>
    Public Function obtDatosGeneralesComprador(ByVal idFact As String, ByVal psIdioma As String) As DataTable
        Dim cnn As New SqlConnection(mDBConnection)
        Dim dt As DataTable
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter= Nothing
        Try
            cnn.Open()
            cmd.Connection = cnn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "FSPM_GET_DATOS_GENE_COMP_FACTURA"

            cmd.Parameters.Add("@FACT_ID", SqlDbType.Int)
            cmd.Parameters("@FACT_ID").Direction = ParameterDirection.Input
            cmd.Parameters("@FACT_ID").Value = idFact

            cmd.Parameters.Add("@FS_IDI_COD", SqlDbType.NVarChar, 3)
            cmd.Parameters("@FS_IDI_COD").Direction = ParameterDirection.Input
            cmd.Parameters("@FS_IDI_COD").Value = psIdioma

            da = New SqlDataAdapter(cmd)
            dt = New DataTable
            da.Fill(dt)
            Return dt
        Catch e As Exception
            Throw e
        Finally
            cnn.Dispose()
            cmd.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    '''  Se extrae la información general de los items de la factura parámetro
    ''' </summary>
    ''' <param name="idFact">Identificador de la factura de la cual se extraerán los datos</param>
    ''' <returns>DataTable con la información solicitada</returns>
    ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.obtDatosBD
    ''' Tiempo máximo: menor a 0,5 seg Revisado por: auv. 30/05/2012</remarks>
    Public Function obtDatosGeneralesItems(ByVal idFact As String)
        Dim cnn As New SqlConnection(mDBConnection)
        Dim dt As DataTable
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter= Nothing
        Try
            cnn.Open()
            cmd.Connection = cnn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "FSPM_GET_DATOS_GENE_ITEMS_FACTURA"

            cmd.Parameters.Add("@FACT_ID", SqlDbType.Int)
            cmd.Parameters("@FACT_ID").Direction = ParameterDirection.Input
            cmd.Parameters("@FACT_ID").Value = idFact

            da = New SqlDataAdapter(cmd)
            dt = New DataTable
            da.Fill(dt)
            Return dt
        Catch e As Exception
            Throw e
        Finally
            cnn.Dispose()
            cmd.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Se extrae la información relativa a costes y descuentos de la línea parámetro de la factura parámetro.
    ''' </summary>
    ''' <param name="idFact">Identificador de la factura de la cual se extraerán los datos</param>
    ''' <param name="numLineaFact">Número de la línea de la factura para la cual se deben extraer datos</param>
    ''' <returns>DataTable con la información solicitada</returns>
    ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.construirItems
    ''' Tiempo máximo: menor a 0,5 seg Revisado por: auv. 30/05/2012</remarks>
    Public Function obtDatosCostesDescuentosItem(ByVal idFact As String, ByVal numLineaFact As String)
        Dim cnn As New SqlConnection(mDBConnection)
        Dim dt As DataTable
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter= Nothing
        Try
            cnn.Open()
            cmd.Connection = cnn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "FSPM_GET_COSTYDESC_ITEMS_FACTURA"
            cmd.Parameters.Add("@FACT_ID", SqlDbType.Int)
            cmd.Parameters("@FACT_ID").Direction = ParameterDirection.Input
            cmd.Parameters("@FACT_ID").Value = idFact
            cmd.Parameters.Add("@FACT_LIN", SqlDbType.Int)
            cmd.Parameters("@FACT_LIN").Direction = ParameterDirection.Input
            cmd.Parameters("@FACT_LIN").Value = numLineaFact

            da = New SqlDataAdapter(cmd)
            dt = New DataTable
            da.Fill(dt)
            Return dt
        Catch e As Exception
            Throw e
        Finally
            cnn.Dispose()
            cmd.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Se extrae la información relativa al albarán al que pertenece el artículo parámetro de la factura parámetro
    ''' </summary>
    ''' <param name="idFact">Identificador de la factura de la cual se extraerán los datos</param>
    ''' <param name="codArticulo">Código del artículo de cuyo albarán se deben sacar datos</param>
    ''' <returns>DataTable con la información solicitada</returns>
    ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.construirItems
    ''' Tiempo máximo: menor a 0,5 seg Revisado por: auv. 30/05/2012</remarks>
    Public Function obtDatosAlbaranes(ByVal idFact As String, ByVal codArticulo As String) As DataTable
        Dim cnn As New SqlConnection(mDBConnection)
        Dim dt As DataTable
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter= Nothing
        Try
            cnn.Open()
            cmd.Connection = cnn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "FSPM_GET_DATOS_ALBARANES_FACTURA"
            cmd.Parameters.Add("@FACT_ID", SqlDbType.Int)
            cmd.Parameters("@FACT_ID").Direction = ParameterDirection.Input
            cmd.Parameters("@FACT_ID").Value = idFact
            cmd.Parameters.Add("@ART", SqlDbType.VarChar, 20)
            cmd.Parameters("@ART").Direction = ParameterDirection.Input
            cmd.Parameters("@ART").Value = codArticulo

            da = New SqlDataAdapter(cmd)
            dt = New DataTable
            da.Fill(dt)
            Return dt
        Catch e As Exception
            Throw e
        Finally
            cnn.Dispose()
            cmd.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Este método devuelve un datatable que contiene el código  válido para facturae del idioma. Se le pasa como parámetro el código
    ''' FS del idioma
    ''' </summary>
    ''' <param name="psIdioma">Idioma de cuyo código se quiere extraer el mapeo para obtener el código válido para facturae</param>
    ''' <returns>DataTable con la información solicitada</returns>
    ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.construirInvoiceIssueData
    ''' Tiempo máximo: menor a 0,5 seg Revisado por: auv. 11/06/2012</remarks>
    Public Function obtCodigoFacturaeIdioma(ByRef psIdioma As String) As DataTable
        Dim cnn As New SqlConnection(mDBConnection)
        Dim dt As DataTable
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter= Nothing
        Try
            cnn.Open()
            cmd.Connection = cnn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "FSPM_GET_COD_IDI_FACTURAE"
            cmd.Parameters.Add("@FS_IDI_COD", SqlDbType.NVarChar, 3)
            cmd.Parameters("@FS_IDI_COD").Direction = ParameterDirection.Input
            cmd.Parameters("@FS_IDI_COD").Value = psIdioma

            da = New SqlDataAdapter(cmd)
            dt = New DataTable
            da.Fill(dt)
            Return dt
        Catch e As Exception
            Throw e
        Finally
            cnn.Dispose()
            cmd.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Se extrae información la denominación de la moneda.
    ''' </summary>
    ''' <param name="isoA3CurrencyCode">Código ISO A3 de la moneda</param>
    ''' <param name="psIdioma">Idioma en el que se publica la factura en pdf</param>
    ''' <returns>Datatable con la información solicitada</returns>
    ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.obtDatosBD
    ''' Tiempo máximo: menor a 0,5 seg Revisado por: auv. 11/06/2012</remarks>
    Public Function obtNombreMoneda(ByRef isoA3CurrencyCode As String, ByVal psIdioma As String) As DataTable
        Dim cnn As New SqlConnection(mDBConnection)
        Dim dt As DataTable
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter= Nothing
        Try
            cnn.Open()
            cmd.Connection = cnn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "FSPM_GET_COD_MON_FACTURA"
            cmd.Parameters.Add("@FACTURAE_MON_A3", SqlDbType.VarChar, 20)
            cmd.Parameters("@FACTURAE_MON_A3").Direction = ParameterDirection.Input
            cmd.Parameters("@FACTURAE_MON_A3").Value = isoA3CurrencyCode
            cmd.Parameters.Add("@FS_IDI_COD", SqlDbType.NVarChar, 20)
            cmd.Parameters("@FS_IDI_COD").Direction = ParameterDirection.Input
            cmd.Parameters("@FS_IDI_COD").Value = psIdioma

            da = New SqlDataAdapter(cmd)
            dt = New DataTable
            da.Fill(dt)
            Return dt
        Catch e As Exception
            Throw e
        Finally
            cnn.Dispose()
            cmd.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Se extrae información la denominación del país.
    ''' </summary>
    ''' <param name="isoA3CountryCode">Código ISO A3 del país</param>
    ''' <param name="psIdioma">Idioma en el que se publica la factura en pdf</param>
    ''' <returns>Datatable con la información solicitada</returns>
    ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.obtDatosBD
    ''' Tiempo máximo: menor a 0,5 seg Revisado por: auv. 11/06/2012</remarks>
    Public Function obtNombrePais(ByRef isoA3CountryCode As String, ByVal psIdioma As String) As DataTable
        Dim cnn As New SqlConnection(mDBConnection)
        Dim dt As DataTable
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter= Nothing
        Try
            cnn.Open()
            cmd.Connection = cnn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "FSPM_GET_COD_PAI_FACTURA"
            cmd.Parameters.Add("@FACTURAE_PAI_A3", SqlDbType.VarChar, 20)
            cmd.Parameters("@FACTURAE_PAI_A3").Direction = ParameterDirection.Input
            cmd.Parameters("@FACTURAE_PAI_A3").Value = isoA3CountryCode
            cmd.Parameters.Add("@FS_IDI_COD", SqlDbType.NVarChar, 20)
            cmd.Parameters("@FS_IDI_COD").Direction = ParameterDirection.Input
            cmd.Parameters("@FS_IDI_COD").Value = psIdioma

            da = New SqlDataAdapter(cmd)
            dt = New DataTable
            da.Fill(dt)
            Return dt
        Catch e As Exception
            Throw e
        Finally
            cnn.Dispose()
            cmd.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Se extrae información la denominación de la unidad de medida.
    ''' </summary>
    ''' <param name="isoUniCode">Código ISO de la unidad de medida</param>
    ''' <param name="psIdioma">Idioma en el que se publica la factura en pdf</param>
    ''' <returns>Datatable con la información solicitada</returns>
    ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.obtDatosBD
    ''' Tiempo máximo: menor a 0,5 seg Revisado por: auv. 11/06/2012</remarks>
    Public Function obtNombreUnidad(ByRef isoUniCode As String, ByVal psIdioma As String) As DataTable
        Dim cnn As New SqlConnection(mDBConnection)
        Dim dt As DataTable
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter= Nothing
        Try
            cnn.Open()
            cmd.Connection = cnn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "FSPM_GET_COD_UNI_FACTURA"
            cmd.Parameters.Add("@FACTURAE_UNI", SqlDbType.VarChar, 20)
            cmd.Parameters("@FACTURAE_UNI").Direction = ParameterDirection.Input
            cmd.Parameters("@FACTURAE_UNI").Value = isoUniCode
            cmd.Parameters.Add("@FS_IDI_COD", SqlDbType.NVarChar, 20)
            cmd.Parameters("@FS_IDI_COD").Direction = ParameterDirection.Input
            cmd.Parameters("@FS_IDI_COD").Value = psIdioma

            da = New SqlDataAdapter(cmd)
            dt = New DataTable
            da.Fill(dt)
            Return dt
        Catch e As Exception
            Throw e
        Finally
            cnn.Dispose()
            cmd.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Se extrae información relativa a los certificados para firma digital.
    ''' </summary>
    ''' <param name="idFact">Identificador de la factura de la cual se extraerán los datos</param>
    ''' <returns>Datatable con la información solicitada</returns>
    ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.crearFacV3_2
    ''' Tiempo máximo: menor a 0,5 seg Revisado por: auv. 21/09/2012</remarks>
    Public Function obtDatosFirma(ByRef idFact As String) As DataTable
        Dim cnn As New SqlConnection(mDBConnection)
        Dim dt As DataTable
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter= Nothing
        Try
            cnn.Open()
            cmd.Connection = cnn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "FSPM_GET_DATOS_FIRMA_FACTURA"
            cmd.Parameters.Add("@FACT_ID", SqlDbType.Int)
            cmd.Parameters("@FACT_ID").Direction = ParameterDirection.Input
            cmd.Parameters("@FACT_ID").Value = idFact

            da = New SqlDataAdapter(cmd)
            dt = New DataTable
            da.Fill(dt)
            Return dt
        Catch e As Exception
            Throw e
        Finally
            cnn.Dispose()
            cmd.Dispose()
            da.Dispose()
        End Try
    End Function
#End Region
#End Region
End Class