﻿Imports System.Configuration
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Exception
Imports System.IO
Imports System.Object
Imports Fullstep.FSNLibrary

Partial Public Class Root
#Region "BI"
    ''' <summary>
    ''' Carga todos los cubos de la BBDD, activos o no, con sus respectivas denominaciones
    ''' </summary>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Public Function Cubos_FullLoad(ByVal idi As String) As DataSet
        Authenticate()
        Dim cm = New SqlCommand()
        Dim cn As New SqlConnection(mDBConnection)
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            Using cn
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSBI_CARGAR_CUBOS"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@IDIOMA", idi)
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Inserta un nuevo Cubo en la BBDD
    ''' </summary>
    ''' <param name="activo">Cubo activo o no</param>
    ''' <param name="url">Url del cubo</param>
    ''' <param name="usuario">Usuario del cubo</param>
    ''' <param name="password">Contraseña del cubo</param>
    ''' <param name="dominio">Dominio del cubo</param>
    ''' <param name="url2">Url SSAS del cubo</param>
    ''' <param name="usuario2">Usuario SSAS del cubo</param>
    ''' <param name="password2">Contraseña SSAS del cubo</param>
    ''' <param name="dominio2">Dominio SSAS del cubo</param>
    ''' <param name="BD">BD SSAS del cubo</param>
    ''' <param name="cubo">Cubo SSAS</param>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Public Function Cubos_Insert(ByVal url As String, ByVal usuario As String, ByVal password As String, ByVal dominio As String, ByVal url2 As String, ByVal usuario2 As String, ByVal password2 As String, ByVal dominio2 As String, ByVal BD As String, ByVal cubo As String, ByVal activo As Boolean, ByVal availableMeasures As String, ByVal defaultMeasures As String) As Integer
        Authenticate()
        Dim cm = New SqlCommand()
        Dim cn As New SqlConnection(mDBConnection)
        Dim Id As Integer
        Try
            Using cn
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSBI_INSERTAR_CUBO"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output
                cm.Parameters.AddWithValue("@SVC_URL", url)
                cm.Parameters.AddWithValue("@SVC_USU", usuario)
                cm.Parameters.AddWithValue("@SVC_PWD", password)
                cm.Parameters.AddWithValue("@SVC_DOM", dominio)
                cm.Parameters.AddWithValue("@SSAS_SERVER", url2)
                cm.Parameters.AddWithValue("@SSAS_USU", usuario2)
                cm.Parameters.AddWithValue("@SSAS_PWD", password2)
                cm.Parameters.AddWithValue("@SSAS_DOM", dominio2)
                cm.Parameters.AddWithValue("@SSAS_BD", BD)
                cm.Parameters.AddWithValue("@SSAS_CUBE", cubo)
                cm.Parameters.AddWithValue("@ACTIVO", activo)
                cm.Parameters.AddWithValue("@AVAILABLE_MEASURES", availableMeasures)
                cm.Parameters.AddWithValue("@DEFAULT_MEASURES", defaultMeasures)
                cm.CommandType = CommandType.StoredProcedure
                cm.ExecuteNonQuery()
                Id = cm.Parameters("@ID").Value
                Return Id
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Modifica un cubo de la BBDD
    ''' </summary>
    ''' <param name="idCubo">id Cubo</param>
    ''' <param name="activo">Cubo activo o no</param>
    ''' <param name="url">Url del cubo</param>
    ''' <param name="usuario">Usuario del cubo</param>
    ''' <param name="password">Contraseña del cubo</param>
    ''' <param name="dominio">Dominio del cubo</param>
    ''' <param name="url2">Url SSAS del cubo</param>
    ''' <param name="usuario2">Usuario SSAS del cubo</param>
    ''' <param name="password2">Contraseña SSAS del cubo</param>
    ''' <param name="dominio2">Dominio SSAS del cubo</param>
    ''' <param name="BD">BD SSAS del cubo</param>
    ''' <param name="cubo">Cubo SSAS</param>
    ''' <param name="availableMeasures">Medidas disponibles</param>
    ''' <param name="defaultMeasures">Medidas predeterminadas</param>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Public Sub Cubos_Update(ByVal idCubo As Integer, ByVal url As String, ByVal usuario As String, ByVal password As String, ByVal dominio As String, ByVal url2 As String, ByVal usuario2 As String, ByVal password2 As String, ByVal dominio2 As String, ByVal BD As String, ByVal cubo As String, ByVal activo As Boolean, ByVal availableMeasures As String, ByVal defaultMeasures As String)
        Authenticate()
        Dim cm = New SqlCommand()
        Dim cn As New SqlConnection(mDBConnection)
        Try
            Using cn
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSBI_MODIFICAR_CUBO"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@ID", idCubo)
                cm.Parameters.AddWithValue("@SVC_URL", url)
                cm.Parameters.AddWithValue("@SVC_USU", usuario)
                cm.Parameters.AddWithValue("@SVC_PWD", password)
                cm.Parameters.AddWithValue("@SVC_DOM", dominio)
                cm.Parameters.AddWithValue("@SSAS_SERVER", url2)
                cm.Parameters.AddWithValue("@SSAS_USU", usuario2)
                cm.Parameters.AddWithValue("@SSAS_PWD", password2)
                cm.Parameters.AddWithValue("@SSAS_DOM", dominio2)
                cm.Parameters.AddWithValue("@SSAS_BD", BD)
                cm.Parameters.AddWithValue("@SSAS_CUBE", cubo)
                cm.Parameters.AddWithValue("@ACTIVO", activo)
                cm.Parameters.AddWithValue("@AVAILABLE_MEASURES", availableMeasures)
                cm.Parameters.AddWithValue("@DEFAULT_MEASURES", defaultMeasures)
                cm.CommandType = CommandType.StoredProcedure
                cm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Borra un Cubo de la BBDD
    ''' </summary>
    ''' <param name="idCubo">id Cubo</param>
    Public Sub Cubos_Delete(ByVal idCubo As Integer)
        Authenticate()
        Dim cm = New SqlCommand()
        Dim cn As New SqlConnection(mDBConnection)
        Try
            Using cn
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSBI_ELIMINAR_CUBO"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@IDCUBO", idCubo)
                cm.CommandType = CommandType.StoredProcedure
                cm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Devuelve un Cubo de la BBDD
    ''' </summary>
    ''' <param name="idCubo">id Cubo</param>
    Public Function Cubos_GetCubo(ByVal idCubo As Integer)
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSBI_CARGAR_CUBO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@ID_CUBO", idCubo)
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Inserta las denominaciones de los cubos en la BBDD
    ''' </summary>
    ''' <param name="idCubo">Id Cubo</param>
    ''' <param name="DenominacionesCubo">IdiomaCubo, Denominacion del cubo</param>
    Public Sub CubosDen_Add(ByVal IdCubo As String, ByVal DenominacionesCubo As DataTable)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim da As New SqlDataAdapter

        cn.Open()
        Dim transaccion As SqlTransaction = cn.BeginTransaction()
        Try
            cm = New SqlCommand("FSBI_INSERTAR_CUBO_DEN", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@ID_CUBO", IdCubo)
                .Add("@IDIOMA", SqlDbType.NVarChar, 20, "IDIOMA")
                .Add("@DEN", SqlDbType.NVarChar, 200, "DEN")
            End With
            da.InsertCommand = cm
            da.Update(DenominacionesCubo)
            transaccion.Commit()
        Catch e As Exception
            transaccion.Rollback()
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Recupera las denominaciones de los cubos de la BBDD
    ''' </summary>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Public Function CubosDen_Load() As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SELECT ID_CUBO, IDIOMA, DEN FROM CUBOS_DEN WITH (NOLOCK)"
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera las denominaciones de los cubos de la BBDD con el idioma seleccionado
    ''' </summary>
    ''' <param name="idioma">idioma del cubo</param>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Public Function CubosDen_Load(ByVal idioma As String) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SELECT ID_CUBO, IDIOMA, DEN FROM CUBOS_DEN WITH (NOLOCK) WHERE IDIOMA = @IDIOMA"
            cm.Parameters.AddWithValue("@IDIOMA", idioma)
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera las denominaciones de los cubos de la BBDD con el id del cubo seleccionado
    ''' </summary>
    ''' <param name="idCubo">Codigo del cubo</param>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Public Function CubosDen_Load_By_ID(ByVal idCubo As String) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SELECT ID_CUBO, IDIOMA, DEN FROM CUBOS_DEN WITH (NOLOCK) WHERE ID_CUBO = @IDCUBO"
            cm.Parameters.AddWithValue("@IDCUBO", idCubo)
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Modifica las denominaciones de los cubos en la BBDD
    ''' </summary>
    ''' <param name="idCubo">Id Cubo</param>
    ''' <param name="DenominacionesCubo">IdiomaCubo, Denominacion del cubo</param>
    Public Sub CubosDen_Update(ByVal IdCubo As String, ByVal DenominacionesCubo As DataTable)
        Authenticate()
        Dim i As Integer
        For i = 0 To DenominacionesCubo.Rows.Count - 1
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As SqlCommand = New SqlCommand()
            cn.Open()
            cm.Connection = cn
            Try
                cm = New SqlCommand("FSBI_MODIFICAR_CUBO_DEN", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                With cm.Parameters
                    .AddWithValue("@ID_CUBO", IdCubo)
                    .AddWithValue("@IDIOMA", DenominacionesCubo.Rows(i).Item("IDIOMA"))
                    .AddWithValue("@DEN", DenominacionesCubo.Rows(i).Item("DEN"))
                End With
                cm.CommandType = CommandType.StoredProcedure
                cm.ExecuteNonQuery()

            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        Next
    End Sub
    ''' <summary>
    ''' Elimina denominaciones de los cubos de la BBDD
    ''' </summary>
    ''' <param name="idCubo">Id Cubo</param>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Public Sub CubosDen_Delete(ByVal idCubo As Integer)
        Authenticate()
        Dim cm = New SqlCommand()
        Dim cn As New SqlConnection(mDBConnection)
        Try
            Using cn
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSBI_ELIMINAR_CUBO_DEN"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@IDCUBO", idCubo)
                cm.CommandType = CommandType.StoredProcedure
                cm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Recupera los permisos de los cubos respecto a una unidad organizativa de la BBDD
    ''' </summary>
    ''' <param name="idCubo">Codigo del cubo</param>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Public Function CubosUon_Load(ByVal idCubo As String) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SELECT CUBO, CUBOS_UON.UON1, CUBOS_UON.UON2, CUBOS_UON.UON3, UON1.DEN AS DEN1, UON2.DEN AS DEN2, UON3.DEN AS DEN3 FROM CUBOS_UON WITH (NOLOCK) LEFT JOIN UON1 WITH (NOLOCK) ON UON1.COD=CUBOS_UON.UON1 LEFT JOIN UON2 WITH (NOLOCK) ON UON2.COD=CUBOS_UON.UON2 LEFT JOIN UON3 WITH (NOLOCK) ON UON3.COD=CUBOS_UON.UON3 WHERE CUBO = @IDCUBO"
            cm.Parameters.AddWithValue("@IDCUBO", idCubo)
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los permisos de los cubos respecto a las unidades organizativas de la BBDD
    ''' </summary>
    ''' <param name="idCubo">Codigo del cubo</param>
    ''' <param name="uon1">unidad organizativa nivel 1</param>
    ''' <param name="uon2">unidad organizativa nivel 2</param>
    ''' <param name="uon3">unidad organizativa nivel 3</param>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Public Function CubosUon_Load(ByVal idCubo, ByVal uon1, ByVal uon2, ByVal uon3) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SELECT CUBO, UON1 FROM CUBOS_UON WITH (NOLOCK) WHERE CUBOS_UON.CUBO= @IDCUBO AND CUBOS_UON.UON1= @UON1 AND CUBOS_UON.UON2=@UON2 AND CUBOS_UON.UON3=@UON3"
            cm.Parameters.AddWithValue("@IDCUBO", idCubo)
            cm.Parameters.AddWithValue("@UON1", uon1)
            cm.Parameters.AddWithValue("@UON2", uon2)
            cm.Parameters.AddWithValue("@UON3", uon3)
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los permisos de los usuarios respecto a los cubos de la BBDD
    ''' </summary>
    ''' <param name="idUsu">usuario</param>
    ''' <param name="idi">idioma</param>
    ''' <param name="uon1">unidad organizativa nivel 1</param>
    ''' <param name="uon2">unidad organizativa nivel 2</param>
    ''' <param name="uon3">unidad organizativa nivel 3</param>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Public Function CubosUon_Load(ByVal idUsu, ByVal idi, ByVal uon1, ByVal uon2, ByVal uon3) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim dr2 As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn

            cm.CommandText = "SELECT CUBO AS ID_CUBO, CUBOS_DEN.DEN AS DEN, CUBOS_UON.UON1, CUBOS_UON.UON2, CUBOS_UON.UON3 FROM CUBOS_UON WITH (NOLOCK) LEFT JOIN CUBOS_DEN ON CUBOS_DEN.ID_CUBO= CUBOS_UON.CUBO LEFT JOIN CUBOS ON CUBOS_DEN.ID_CUBO = CUBOS.ID WHERE CUBOS_DEN.IDIOMA=@IDIOMA AND ACTIVO=1"
            cm.Parameters.AddWithValue("@IDIOMA", idi)
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)

            dr2 = dr.Clone()
            For i = 0 To dr.Tables(0).Rows.Count - 1
                If (Not dr.Tables(0).Rows(i).Item("UON1") = "") And dr.Tables(0).Rows(i).Item("UON2") = "" And dr.Tables(0).Rows(i).Item("UON3") = "" Then
                    If (dr.Tables(0).Rows(i).Item("UON1") = uon1) Then
                        dr2.Tables(0).Rows.Add(dr.Tables(0).Rows(i).Item("ID_CUBO"), dr.Tables(0).Rows(i).Item("DEN"))
                    End If
                ElseIf (Not dr.Tables(0).Rows(i).Item("UON1") = "") And Not (dr.Tables(0).Rows(i).Item("UON2") = "") And dr.Tables(0).Rows(i).Item("UON3") = "" Then
                    If (dr.Tables(0).Rows(i).Item("UON1") = uon1 And dr.Tables(0).Rows(i).Item("UON2") = uon2) Then
                        dr2.Tables(0).Rows.Add(dr.Tables(0).Rows(i).Item("ID_CUBO"), dr.Tables(0).Rows(i).Item("DEN"))
                    End If
                ElseIf (Not dr.Tables(0).Rows(i).Item("UON1") = "") And Not (dr.Tables(0).Rows(i).Item("UON2") = "") And Not (dr.Tables(0).Rows(i).Item("UON3") = "") Then
                    If (dr.Tables(0).Rows(i).Item("UON1") = uon1 And dr.Tables(0).Rows(i).Item("UON2") = uon2 And dr.Tables(0).Rows(i).Item("UON3") = uon3) Then
                        dr2.Tables(0).Rows.Add(dr.Tables(0).Rows(i).Item("ID_CUBO"), dr.Tables(0).Rows(i).Item("DEN"))
                    End If
                End If
            Next

            Return dr2
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Inserta los permisos de acceso al cubo para las unidad organizativa
    ''' </summary>
    ''' <param name="idCubo">Id Cubo</param>
    ''' <param name="uon1">Unidad organizativa</param>
    ''' <param name="uon2">Unidad organizativa nivel 2</param>
    ''' <param name="uon3">Unidad organizativa nivel 3</param>
    Public Sub CubosUon_Add(ByVal IdCubo As Integer, ByVal uon1 As String, ByVal uon2 As String, ByVal uon3 As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        cn.Open()
        Dim transaccion As SqlTransaction = cn.BeginTransaction()
        Try
            dr = CubosUon_Load(IdCubo, uon1, uon2, uon3)
            If (dr.Tables(0).Rows.Count = 0) Then
                cm = New SqlCommand("FSBI_INSERTAR_CUBOS_UON", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion
                With cm.Parameters
                    .AddWithValue("@CUBO", IdCubo)
                    .AddWithValue("@UON1", uon1)
                    .AddWithValue("@UON2", uon2)
                    .AddWithValue("@UON3", uon3)
                End With
                cm.CommandType = CommandType.StoredProcedure
                cm.ExecuteNonQuery()
            End If
            transaccion.Commit()
        Catch e As Exception
            transaccion.Rollback()
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Recupera los permisos de los cubos respecto a un usuario de la BBDD
    ''' </summary>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Public Function CubosUsu_Load(ByVal idCubo As Integer) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            Dim s As String = "SELECT USU.COD, PER.NOM, PER.APE, PER.EMAIL FROM CUBOS_USU WITH (NOLOCK) LEFT JOIN USU WITH (NOLOCK) ON CUBOS_USU.USU = USU.COD LEFT JOIN PER WITH (NOLOCK) ON USU.PER=PER.COD LEFT JOIN CUBOS WITH (NOLOCK) ON CUBOS_USU.CUBO=CUBOS.ID WHERE CUBOS_USU.CUBO = @IDCUBO AND ACTIVO=1"
            cm.Parameters.AddWithValue("@IDCUBO", idCubo)
            cm.CommandText = s
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los permisos de los cubos respecto a un usuario de la BBDD
    ''' </summary>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Public Function CubosUsu_Load(ByVal idUsu As String, ByVal idi As String) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            Dim s As String = "SELECT CUBOS_DEN.ID_CUBO, CUBOS_DEN.DEN FROM CUBOS_USU WITH (NOLOCK) LEFT JOIN USU WITH (NOLOCK) ON CUBOS_USU.USU = USU.COD LEFT JOIN CUBOS_DEN ON ID_CUBO=CUBOS_USU.CUBO LEFT JOIN CUBOS WITH (NOLOCK) ON CUBOS_USU.CUBO=CUBOS.ID WHERE USU.COD = @IDUSU AND CUBOS_DEN.IDIOMA = @IDIOMA AND ACTIVO=1 ORDER BY CUBOS_DEN.DEN"
            cm.Parameters.AddWithValue("@IDUSU", idUsu)
            cm.Parameters.AddWithValue("@IDIOMA", idi)
            cm.CommandText = s
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los permisos de los cubos respecto a los usuarios de la BBDD
    ''' </summary>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Public Function CubosUsu_Load(ByVal idCubo As Integer, ByVal idUsu As String) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SELECT CUBO, USU FROM CUBOS_USU  WITH (NOLOCK) LEFT JOIN CUBOS WITH (NOLOCK) ON CUBOS_USU.CUBO=CUBOS.ID WHERE CUBO=@IDCUBO AND USU=@IDUSU AND ACTIVO=1"
            cm.Parameters.AddWithValue("@IDCUBO", idCubo)
            cm.Parameters.AddWithValue("@IDUSU", idUsu)
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Inserta los permisos de acceso al cubo para el usuario.
    ''' </summary>
    ''' <param name="idCubo">Id Cubo</param>
    ''' <param name="usu">usuario</param>
    Public Sub CubosUsu_Add(ByVal IdCubo As Integer, ByVal usu As String)
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        cn.Open()
        Dim transaccion As SqlTransaction = cn.BeginTransaction()
        Try
            dr = CubosUsu_Load(IdCubo, usu)
            If (dr.Tables(0).Rows.Count = 0) Then
                cm = New SqlCommand("FSBI_INSERTAR_CUBOS_USU", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion
                With cm.Parameters
                    .AddWithValue("@CUBO", IdCubo)
                    .AddWithValue("@USU", usu)
                End With
                cm.CommandType = CommandType.StoredProcedure
                cm.ExecuteNonQuery()
            End If
            transaccion.Commit()
        Catch e As Exception
            transaccion.Rollback()
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Carga todas los UON1.
    ''' </summary>
    Public Function CubosUon_FullLoad()
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SELECT COD FROM UON1 WITH (NOLOCK) "
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Inserta los permisos de acceso al cubo para todos las UONS.
    ''' </summary>
    ''' <param name="idCubo">Id Cubo</param>
    Public Sub CubosUon_FullAdd(ByVal IdCubo As Integer)
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        dr = CubosUon_FullLoad()
        cn.Open()
        Dim transaccion As SqlTransaction = cn.BeginTransaction()
        Try
            For Each item As DataRow In dr.Tables(0).Rows
                Dim uon1 As String = item("COD")
                cm = New SqlCommand("FSBI_INSERTAR_TODOS_CUBOS_UON", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion
                With cm.Parameters
                    .AddWithValue("@CUBO", IdCubo)
                    .AddWithValue("@UON1", uon1)
                End With

                cm.CommandType = CommandType.StoredProcedure
                cm.ExecuteNonQuery()
            Next
            transaccion.Commit()
        Catch e As Exception
            transaccion.Rollback()
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Recupera los usuarios y las Uons correspondientes
    ''' </summary>
    ''' <param name="Idioma"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function BI_Obtener_UONsPara(ByVal Idioma As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSBI_PERMISO_CUBO_USUARIO_UON"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDI", Idioma)
            da.SelectCommand = cm
            da.Fill(dr)

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Sub CubosUon_Delete(ByVal idCubo As Integer, ByVal uon1 As String, ByVal uon2 As String, ByVal uon3 As String)
        Authenticate()
        Dim cm = New SqlCommand()
        Dim cn As New SqlConnection(mDBConnection)
        Try
            Using cn
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSBI_ELIMINAR_PERMISO_CUBO_UON"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@IDCUBO", idCubo)
                cm.Parameters.AddWithValue("@UON1", uon1)
                cm.Parameters.AddWithValue("@UON2", uon2)
                cm.Parameters.AddWithValue("@UON3", uon3)
                cm.CommandType = CommandType.StoredProcedure
                cm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    Public Sub CubosUsu_Delete(ByVal idCubo As Integer, ByVal idUsu As String)
        Authenticate()
        Dim cm = New SqlCommand()
        Dim cn As New SqlConnection(mDBConnection)
        Try
            Using cn
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSBI_ELIMINAR_PERMISO_CUBO_USU"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@IDCUBO", idCubo)
                cm.Parameters.AddWithValue("@IDUSU", idUsu)
                cm.CommandType = CommandType.StoredProcedure
                cm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' Recupera el primer entorno de la tabla FSAL_ENTORNO
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function RecuperarEntorno()
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SELECT TOP 1 COD FROM FSAL_ENTORNOS WITH (NOLOCK) ORDER BY ID ASC"
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr.Tables(0).Rows(0).Item("COD")
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los entornos diponibles con la denominación en el idioma seleccionado
    ''' </summary>
    ''' <param name="Idioma"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Entornos_Load(ByVal Idioma As String)
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SELECT ID, COD, ID_CLIENTE, LOCALIZACION, VERSION, FULTINS, RESPINS, RESPIMP, INTEGRACION, BAJA, FECACT, DEN FROM FSAL_ENTORNOS  WITH (NOLOCK) INNER JOIN FSAL_ENT_DEN ON ID=FSAL_ENTORNO WHERE IDIOMA= @IDIOMA AND BAJA=0 ORDER BY DEN ASC"
            cm.Parameters.AddWithValue("@IDIOMA", Idioma)
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera las pymes disponibles para el entorno seleccionado con la denominación en el idioma seleccionado
    ''' </summary>
    ''' <param name="entorno"></param>
    ''' <param name="Idioma"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Pymes_Load(ByVal entorno As String, ByVal Idioma As String)
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SELECT ID, ENTORNO, COD, ID_CLIENTE, LOCALIZACION, VERSION, FULTINS, RESPINS, RESPIMP, INTEGRACION, BAJA, FECACT, DEN FROM FSAL_PYMES  WITH (NOLOCK) INNER JOIN FSAL_PYMES_DEN ON ID=FSAL_PYME WHERE IDIOMA= @IDIOMA AND ENTORNO= @ENTORNO"
            cm.Parameters.AddWithValue("@ENTORNO", entorno)
            cm.Parameters.AddWithValue("@IDIOMA", Idioma)
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los accesos de usuario para cada periodo de 5 minutos entre las 2 fechas seleccionadas para el entorno y pyme seleccionadas
    ''' </summary>
    ''' <param name="fechaDesde"></param>
    ''' <param name="fechaHasta"></param>
    ''' <param name="entorno"></param>
    ''' <param name="pyme"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Accesos_Load_Por_Cada_Cinco_Minutos(ByVal fechaDesde As DateTime, ByVal fechaHasta As DateTime, ByVal entorno As String, ByVal pyme As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOS_LOAD_POR_CADA_CINCO_MINUTOS"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", fechaDesde)
            cm.Parameters.AddWithValue("@fechasta", fechaHasta)
            cm.Parameters.AddWithValue("@entorno", entorno)
            If (Not pyme Is Nothing) Then
                cm.Parameters.AddWithValue("@pyme", pyme)
            End If
            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera el total de accesos de usuarios para cada periodo de 5 minutos entre las 2 fechas seleccionadas para el entorno y pyme seleccionadas
    ''' </summary>
    ''' <param name="fechaDesde"></param>
    ''' <param name="fechaHasta"></param>
    ''' <param name="entorno"></param>
    ''' <param name="pyme"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Accesos_Load_Por_Cada_Cinco_Minutos_Totales(ByVal fechaDesde As DateTime, ByVal fechaHasta As DateTime, ByVal entorno As String, ByVal pyme As String)
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            If (pyme Is Nothing) Then
                cm.CommandText = "SELECT TOTAL_GS, TOTAL_WEB, TOTAL_PORTAL FROM FSAL_USUARIOS_HORA WHERE ENTORNO=@ENTORNO AND (PYME='' OR PYME IS NULL) AND DIA=@DIA AND MES=@MES and ANYO=@ANYO AND HORA=@HORA"
            Else
                cm.CommandText = "SELECT TOTAL_GS, TOTAL_WEB, TOTAL_PORTAL FROM FSAL_USUARIOS_HORA WHERE ENTORNO=@ENTORNO AND ISNULL(PYME,'')=@PYME AND DIA=@DIA AND MES=@MES and ANYO=@ANYO AND HORA=@HORA"
            End If
            cm.Parameters.AddWithValue("@ENTORNO", entorno)
            If (Not pyme Is Nothing) Then
                cm.Parameters.AddWithValue("@PYME", pyme)
            End If
            cm.Parameters.AddWithValue("@DIA", fechaDesde.Day)
            cm.Parameters.AddWithValue("@MES", fechaDesde.Month)
            cm.Parameters.AddWithValue("@ANYO", fechaDesde.Year)
            cm.Parameters.AddWithValue("@HORA", fechaDesde.Hour)
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera el total de accesos de usuarios para cada periodo de 5 minutos entre las 2 fechas seleccionadas para el entorno y pyme seleccionadas
    ''' </summary>
    ''' <param name="fechaDesde"></param>
    ''' <param name="fechaHasta"></param>
    ''' <param name="entorno"></param>
    ''' <param name="pyme"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Accesos_Load_Por_Cada_Cinco_Minutos_Totales_Now(fechaDesde As DateTime, fechaHasta As DateTime, entorno As String, pyme As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOS_LOAD_POR_CADA_CINCO_MINUTOS_TOTALES"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", fechaDesde)
            cm.Parameters.AddWithValue("@fechasta", fechaHasta)
            cm.Parameters.AddWithValue("@entorno", entorno)
            If (Not pyme Is Nothing) Then
                cm.Parameters.AddWithValue("@pyme", pyme)
            End If
            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los accesos de usuarios para cada periodo de 1 hora entre las 2 fechas seleccionadas para el entorno y pyme seleccionadas
    ''' </summary>
    ''' <param name="horaInicio"></param>
    ''' <param name="horaFin"></param>
    ''' <param name="entorno"></param>
    ''' <param name="pyme"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Accesos_Load_Por_Hora(horaInicio As DateTime, horaFin As DateTime, entorno As String, pyme As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOS_USUARIOS_POR_HORA"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", horaInicio)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@fechasta", horaFin)
            If (Not pyme Is Nothing) Then
                cm.Parameters.AddWithValue("@pyme", pyme)
            End If
            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function Accesos_Load_Por_Hora_Totales(horaInicio As DateTime, horaFin As DateTime, entorno As String, pyme As String)
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            If (pyme Is Nothing) Then
                cm.CommandText = "SELECT TOTAL_GS, TOTAL_WEB, TOTAL_PORTAL FROM FSAL_USUARIOS_DIA WHERE ENTORNO=@ENTORNO AND (PYME='' OR PYME IS NULL) AND FECHA BETWEEN @FECINI AND @FECFIN"
            Else
                cm.CommandText = "SELECT TOTAL_GS, TOTAL_WEB, TOTAL_PORTAL FROM FSAL_USUARIOS_DIA WHERE ENTORNO=@ENTORNO AND PYME =@PYME AND FECHA BETWEEN @FECINI AND @FECFIN"
            End If
            cm.Parameters.AddWithValue("@ENTORNO", entorno)
            If (Not pyme Is Nothing) Then
                cm.Parameters.AddWithValue("@PYME", pyme)
            End If
            cm.Parameters.AddWithValue("@FECINI", horaInicio)
            cm.Parameters.AddWithValue("@FECFIN", horaFin)
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los accesos de usuarios para cada periodo de 1 dia entre las 2 fechas seleccionadas para el entorno y pyme seleccionadas(1 semana)
    ''' </summary>
    ''' <param name="fechaInicio"></param>
    ''' <param name="fechaFin"></param>
    ''' <param name="entorno"></param>
    ''' <param name="pyme"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Accesos_Load_Por_Dia_Semana(ByVal fechaInicio As DateTime, ByVal fechaFin As DateTime, ByVal entorno As String, ByVal pyme As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOS_USUARIOS_POR_DIA_SEMANA"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", fechaInicio)
            cm.Parameters.AddWithValue("@fechasta", fechaFin)
            cm.Parameters.AddWithValue("@entorno", entorno)
            If (Not pyme Is Nothing) Then
                cm.Parameters.AddWithValue("@pyme", pyme)
            End If
            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function Accesos_Load_Por_Dia_Semana_Totales(horaInicio As DateTime, horaFin As DateTime, entorno As String, pyme As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            If (pyme Is Nothing) Then
                cm.CommandText = "SELECT TOTAL_GS, TOTAL_WEB, TOTAL_PORTAL FROM FSAL_USUARIOS_SEMANA WHERE ENTORNO=@ENTORNO AND (PYME='' OR PYME IS NULL) AND DIA= @DIA AND MES= @MES and ANYO=@ANYO"
            Else
                cm.CommandText = "SELECT TOTAL_GS, TOTAL_WEB, TOTAL_PORTAL FROM FSAL_USUARIOS_SEMANA WHERE ENTORNO=@ENTORNO AND PYME= @PYME AND DIA=@DIA AND MES=@MES and ANYO=@ANYO"
            End If
            cm.Parameters.AddWithValue("@ENTORNO", entorno)
            If (Not pyme Is Nothing) Then
                cm.Parameters.AddWithValue("@PYME", pyme)
            End If
            cm.Parameters.AddWithValue("@DIA", horaInicio.Day)
            cm.Parameters.AddWithValue("@MES", horaInicio.Month.ToString())
            cm.Parameters.AddWithValue("@ANYO", horaInicio.Year.ToString())
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los accesos de usuarios para cada periodo de 1 dia entre las 2 fechas seleccionadas para el entorno y pyme seleccionadas(1 mes)
    ''' </summary>
    ''' <param name="fechaInicio"></param>
    ''' <param name="fechaFin"></param>
    ''' <param name="entorno"></param>
    ''' <param name="pyme"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Accesos_Load_Por_Dia_Mes(ByVal fechaInicio As DateTime, ByVal fechaFin As DateTime, ByVal entorno As String, ByVal pyme As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOS_USUARIOS_POR_DIA_MES"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", fechaInicio)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@fechasta", fechaFin)
            If (Not pyme Is Nothing) Then
                cm.Parameters.AddWithValue("@pyme", pyme)
            End If
            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function Accesos_Load_Por_Dia_Mes_Totales(horaInicio As DateTime, horaFin As DateTime, entorno As String, pyme As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            If (pyme Is Nothing) Then
                cm.CommandText = "SELECT TOTAL_GS, TOTAL_WEB, TOTAL_PORTAL FROM FSAL_USUARIOS_MES WHERE ENTORNO= @ENTORNO AND (PYME='' OR PYME IS NULL) AND MES= @MES and ANYO= @ANYO"
            Else
                cm.CommandText = "SELECT TOTAL_GS, TOTAL_WEB, TOTAL_PORTAL FROM FSAL_USUARIOS_MES WHERE ENTORNO= @ENTORNO AND PYME = @PYME AND MES= @MES and ANYO= @ANYO"
            End If
            cm.Parameters.AddWithValue("@ENTORNO", entorno)
            If (Not pyme Is Nothing) Then
                cm.Parameters.AddWithValue("@PYME", pyme)
            End If
            cm.Parameters.AddWithValue("@DIA", horaInicio.Day)
            cm.Parameters.AddWithValue("@MES", horaInicio.Month.ToString())
            cm.Parameters.AddWithValue("@ANYO", horaInicio.Year.ToString())
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los accesos de usuarios para cada periodo de 1 mes entre las 2 fechas seleccionadas para el entorno y pyme seleccionadas
    ''' </summary>
    ''' <param name="year"></param>
    ''' <param name="entorno"></param>
    ''' <param name="pyme"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Accesos_Load_Por_Mes(ByVal year As String, ByVal entorno As String, ByVal pyme As String)
        Authenticate()

        Dim fecIni = New Date(year, 1, 1)
        Dim fecFin = New Date(year, 12, 31)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOS_USUARIOS_POR_MES"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", fecIni)
            cm.Parameters.AddWithValue("@fechasta", fecFin)
            cm.Parameters.AddWithValue("@entorno", entorno)
            If (Not pyme Is Nothing) Then
                cm.Parameters.AddWithValue("@pyme", pyme)
            End If
            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function Accesos_Load_Por_Mes_Totales(year As Integer, entorno As String, pyme As String)
        Authenticate()

        Dim fecIni = New Date(year, 1, 1)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            If (pyme Is Nothing) Then
                cm.CommandText = "SELECT TOTAL_GS, TOTAL_WEB, TOTAL_PORTAL FROM FSAL_USUARIOS_ANYO WHERE ENTORNO=@ENTORNO AND (PYME='' OR PYME IS NULL) AND ANYO = @YEAR"
            Else
                cm.CommandText = "SELECT TOTAL_GS, TOTAL_WEB, TOTAL_PORTAL FROM FSAL_USUARIOS_ANYO WHERE ENTORNO=@ENTORNO AND PYME= @PYME AND ANYO = @YEAR"
            End If
            cm.Parameters.AddWithValue("@ENTORNO", entorno)
            If (Not pyme Is Nothing) Then
                cm.Parameters.AddWithValue("@PYME", pyme)
            End If
            cm.Parameters.AddWithValue("@YEAR", fecIni.Year)
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los eventos producidos para cada periodo de 5 minutos entre las 2 fechas seleccionadas para el entorno y pyme seleccionadas
    ''' </summary>
    ''' <param name="fechaDesde"></param>
    ''' <param name="fechaHasta"></param>
    ''' <param name="entorno"></param>
    ''' <param name="tipoEvento"></param>
    ''' <param name="pyme"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Activity_Load_Por_Cada_Cinco_Minutos(ByVal fechaDesde As DateTime, ByVal fechaHasta As DateTime, ByVal entorno As String, ByVal tipoEvento As String, ByVal pyme As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACTIVITY_LOAD_POR_CADA_CINCO_MINUTOS"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", fechaDesde)
            cm.Parameters.AddWithValue("@fechasta", fechaHasta)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@tipoEvento", tipoEvento)
            If (Not pyme Is Nothing) Then
                cm.Parameters.AddWithValue("@pyme", pyme)
            End If
            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los eventos producidos para cada periodo de 1 hora entre las 2 fechas seleccionadas para el entorno y pyme seleccionadas
    ''' </summary>
    ''' <param name="fechaDesde"></param>
    ''' <param name="fechaHasta"></param>
    ''' <param name="entorno"></param>
    ''' <param name="tipoEvento"></param>
    ''' <param name="pyme"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Activity_Load_Por_Hora(ByVal fechaDesde As DateTime, ByVal fechaHasta As DateTime, ByVal entorno As String, ByVal tipoEvento As String, ByVal pyme As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACTIVITY_USUARIOS_POR_HORA"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", fechaDesde)
            cm.Parameters.AddWithValue("@fechasta", fechaHasta)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@tipoEvento", tipoEvento)
            If (Not pyme Is Nothing) Then
                cm.Parameters.AddWithValue("@pyme", pyme)
            End If
            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los eventos producidos para cada periodo de 1 dia entre las 2 fechas seleccionadas para el entorno y pyme seleccionadas (1 semana)
    ''' </summary>
    ''' <param name="fechaDesde"></param>
    ''' <param name="fechaHasta"></param>
    ''' <param name="entorno"></param>
    ''' <param name="tipoEvento"></param>
    ''' <param name="pyme"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Activity_Load_Por_Dia_Semana(ByVal fechaDesde As DateTime, ByVal fechaHasta As DateTime, ByVal entorno As String, ByVal tipoEvento As String, ByVal pyme As String)
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACTIVITY_USUARIOS_POR_DIA_SEMANA"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", fechaDesde)
            cm.Parameters.AddWithValue("@fechasta", fechaHasta)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@tipoEvento", tipoEvento)
            If (Not pyme Is Nothing) Then
                cm.Parameters.AddWithValue("@pyme", pyme)
            End If
            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los eventos producidos para cada periodo de 1 dia entre las 2 fechas seleccionadas para el entorno y pyme seleccionadas(1 mes)
    ''' </summary>
    ''' <param name="fechaDesde"></param>
    ''' <param name="fechaHasta"></param>
    ''' <param name="entorno"></param>
    ''' <param name="tipoEvento"></param>
    ''' <param name="pyme"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Activity_Load_Por_Dia_Mes(ByVal fechaDesde As DateTime, ByVal fechaHasta As DateTime, ByVal entorno As String, ByVal tipoEvento As String, ByVal pyme As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACTIVITY_USUARIOS_POR_DIA_MES"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", fechaDesde)
            cm.Parameters.AddWithValue("@fechasta", fechaHasta)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@tipoEvento", tipoEvento)
            If (Not pyme Is Nothing) Then
                cm.Parameters.AddWithValue("@pyme", pyme)
            End If
            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los eventos producidos para cada periodo de 1 mes entre las 2 fechas seleccionadas para el entorno y pyme seleccionadas
    ''' </summary>
    ''' <param name="fechaDesde"></param>
    ''' <param name="fechaHasta"></param>
    ''' <param name="entorno"></param>
    ''' <param name="tipoEvento"></param>
    ''' <param name="pyme"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Activity_Load_Por_Mes(ByVal fechaDesde As DateTime, ByVal fechaHasta As DateTime, ByVal entorno As String, ByVal tipoEvento As String, ByVal pyme As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACTIVITY_USUARIOS_POR_MES"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", fechaDesde)
            cm.Parameters.AddWithValue("@fechasta", fechaHasta)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@tipoEvento", tipoEvento)
            If (Not pyme Is Nothing) Then
                cm.Parameters.AddWithValue("@pyme", pyme)
            End If
            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los errores producidos entre las 2 fechas seleccionadas para el entorno y pyme seleccionadas
    ''' </summary>
    ''' <param name="fechaInicio"></param>
    ''' <param name="fechaFin"></param>
    ''' <param name="entorno"></param>
    ''' <param name="pyme"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ErroresLoad(fechaInicio As DateTime, fechaFin As DateTime, entorno As String, pyme As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_CARGAR_ERRORES"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", fechaInicio)
            cm.Parameters.AddWithValue("@fechasta", fechaFin)
            cm.Parameters.AddWithValue("@entorno", entorno)
            If (Not pyme Is Nothing) Then
                cm.Parameters.AddWithValue("@pyme", pyme)
            End If
            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los eventos producidos por hora para cargar el grid de eventos
    ''' </summary>
    ''' <param name="fechaInicio"></param>
    ''' <param name="fechaFin"></param>
    ''' <param name="entorno"></param>
    ''' <param name="pyme"></param>
    ''' <param name="tipoEvento"></param>
    ''' <param name="idioma"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function EventosLoad_Hora(fechaInicio As DateTime, fechaFin As DateTime, entorno As String, pyme As String, tipoEvento As String, idioma As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_CARGAR_EVENTOS_POR_HORA"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", fechaInicio)
            cm.Parameters.AddWithValue("@fechasta", fechaFin)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@pyme", pyme)
            cm.Parameters.AddWithValue("@idioma", idioma)
            If (Not tipoEvento = "") And (Not tipoEvento = "TODOS") And (Not tipoEvento = "ALL") Then
                cm.Parameters.AddWithValue("@tipoEvento", Integer.Parse(tipoEvento))
            End If
            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los eventos producidos por dia para cargar el grid de eventos
    ''' </summary>
    ''' <param name="fechaInicio"></param>
    ''' <param name="fechaFin"></param>
    ''' <param name="entorno"></param>
    ''' <param name="pyme"></param>
    ''' <param name="tipoEvento"></param>
    ''' <param name="idioma"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function EventosLoad_Dia(fechaInicio As DateTime, fechaFin As DateTime, entorno As String, pyme As String, tipoEvento As String, idioma As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_CARGAR_EVENTOS_POR_DIA"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", fechaInicio)
            cm.Parameters.AddWithValue("@fechasta", fechaFin)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@pyme", pyme)
            cm.Parameters.AddWithValue("@idioma", idioma)
            If (Not tipoEvento = "") And (Not tipoEvento = "TODOS") And (Not tipoEvento = "ALL") Then
                cm.Parameters.AddWithValue("@tipoEvento", Integer.Parse(tipoEvento))
            End If
            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los eventos producidos por semana para cargar el grid de eventos
    ''' </summary>
    ''' <param name="fechaInicio"></param>
    ''' <param name="fechaFin"></param>
    ''' <param name="entorno"></param>
    ''' <param name="pyme"></param>
    ''' <param name="tipoEvento"></param>
    ''' <param name="idioma"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function EventosLoad_Semana(fechaInicio As DateTime, fechaFin As DateTime, entorno As String, pyme As String, tipoEvento As String, idioma As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_CARGAR_EVENTOS_POR_SEMANA"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", fechaInicio)
            cm.Parameters.AddWithValue("@fechasta", fechaFin)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@pyme", pyme)
            cm.Parameters.AddWithValue("@idioma", idioma)
            If (Not tipoEvento = "") And (Not tipoEvento = "TODOS") And (Not tipoEvento = "ALL") Then
                cm.Parameters.AddWithValue("@tipoEvento", Integer.Parse(tipoEvento))
            End If
            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los eventos producidos por mes para cargar el grid de eventos
    ''' </summary>
    ''' <param name="fechaInicio"></param>
    ''' <param name="fechaFin"></param>
    ''' <param name="entorno"></param>
    ''' <param name="pyme"></param>
    ''' <param name="tipoEvento"></param>
    ''' <param name="idioma"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function EventosLoad_Mes(fechaInicio As DateTime, fechaFin As DateTime, entorno As String, pyme As String, tipoEvento As String, idioma As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_CARGAR_EVENTOS_POR_MES"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", fechaInicio)
            cm.Parameters.AddWithValue("@fechasta", fechaFin)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@pyme", pyme)
            cm.Parameters.AddWithValue("@idioma", idioma)
            If (Not tipoEvento = "") And (Not tipoEvento = "TODOS") And (Not tipoEvento = "ALL") Then
                cm.Parameters.AddWithValue("@tipoEvento", Integer.Parse(tipoEvento))
            End If
            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los eventos producidos por anyo para cargar el grid de eventos
    ''' </summary>
    ''' <param name="fechaInicio"></param>
    ''' <param name="fechaFin"></param>
    ''' <param name="entorno"></param>
    ''' <param name="pyme"></param>
    ''' <param name="tipoEvento"></param>
    ''' <param name="idioma"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function EventosLoad_Anyo(fechaInicio As DateTime, fechaFin As DateTime, entorno As String, pyme As String, tipoEvento As String, idioma As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_CARGAR_EVENTOS_POR_ANYO"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", fechaInicio)
            cm.Parameters.AddWithValue("@fechasta", fechaFin)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@pyme", pyme)
            cm.Parameters.AddWithValue("@idioma", idioma)
            If (Not tipoEvento = "") And (Not tipoEvento = "TODOS") And (Not tipoEvento = "ALL") Then
                cm.Parameters.AddWithValue("@tipoEvento", Integer.Parse(tipoEvento))
            End If
            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los tipos de eventos que existen para el combo de tipos de evento
    ''' </summary>
    ''' <param name="idi"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function TipoEventoLoad(idi As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SELECT ID, DEN FROM FSAL_TIPO_EVENTO_DEN WHERE IDI = @IDIOMA"
            cm.Parameters.AddWithValue("@IDIOMA", idi)
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Recupera los textos para el modulo seleccionado (Visor de actividades => modulo=181)
    ''' </summary>
    ''' <param name="Modulo"></param>
    ''' <param name="Idioma"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function TextoBi_Carga(ByVal Modulo As Integer, ByVal Idioma As String) As ArrayList
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim sRes As New ArrayList
        Try
            cm.Connection = cn
            cm.CommandText = "SELECT CASE WHEN @IDIOMA='SPA' THEN TEXT_SPA WHEN @IDIOMA='GER' THEN TEXT_GER WHEN @IDIOMA='ENG' THEN TEXT_ENG WHEN @IDIOMA='FRA' THEN TEXT_FRA END FROM WEBFSWSTEXT WITH(NOLOCK) WHERE MODULO=@MODULO"
            'cm.CommandText = "SELECT CASE TEXT_@IDIOMA FROM WEBFSWSTEXT WITH(NOLOCK) WHERE MODULO=@MODULO"
            cm.Parameters.AddWithValue("@IDIOMA", Idioma)
            cm.Parameters.AddWithValue("@MODULO", Modulo)
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(ds)
            If ds.Tables(0).Rows.Count > 0 Then
                sRes = New ArrayList()
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    sRes.Add("" & ds.Tables(0).Rows(i).Item(0))
                Next
            End If
            Return sRes
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function KPI_Load(ByVal idi As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_KPI_LOAD"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDIOMA", idi)
            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function AccesosKPI_Load_Por_Hora(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOSKPI_LOAD_POR_CADA_CINCO_MINUTOS"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", horaInicio)
            cm.Parameters.AddWithValue("@fechasta", horaFin)
            cm.Parameters.AddWithValue("@ENTORNO", entorno)
            cm.Parameters.AddWithValue("@PYME", pyme)

            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function AccesosKPI_Load_Por_Dia(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOSKPI_LOAD_POR_HORA"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", horaInicio)
            cm.Parameters.AddWithValue("@fechasta", horaFin)
            cm.Parameters.AddWithValue("@ENTORNO", entorno)
            cm.Parameters.AddWithValue("@PYME", pyme)

            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function AccesosKPI_Load_Por_Semana(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOSKPI_LOAD_POR_DIA"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", horaInicio)
            cm.Parameters.AddWithValue("@fechasta", horaFin)
            cm.Parameters.AddWithValue("@ENTORNO", entorno)
            cm.Parameters.AddWithValue("@PYME", pyme)

            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function AccesosKPI_Load_Por_Mes(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOSKPI_LOAD_POR_DIA"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", horaInicio)
            cm.Parameters.AddWithValue("@fechasta", horaFin)
            cm.Parameters.AddWithValue("@ENTORNO", entorno)
            cm.Parameters.AddWithValue("@PYME", pyme)

            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function AccesosKPI_Load_Por_Anyo(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOSKPI_LOAD_POR_MES"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", horaInicio)
            cm.Parameters.AddWithValue("@fechasta", horaFin)
            cm.Parameters.AddWithValue("@ENTORNO", entorno)
            cm.Parameters.AddWithValue("@PYME", pyme)

            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function AccesosKPI_Load_Gauge_Hora(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOSKPI_LOAD_GAUGE_HORA"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", horaInicio)
            cm.Parameters.AddWithValue("@fechasta", horaFin)
            cm.Parameters.AddWithValue("@ENTORNO", entorno)
            cm.Parameters.AddWithValue("@PYME", pyme)

            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function AccesosKPI_Load_Gauge_Dia(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOSKPI_LOAD_GAUGE_DIA"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", horaInicio)
            cm.Parameters.AddWithValue("@fechasta", horaFin)
            cm.Parameters.AddWithValue("@ENTORNO", entorno)
            cm.Parameters.AddWithValue("@PYME", pyme)

            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function AccesosKPI_Load_Gauge_Semana(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOSKPI_LOAD_GAUGE_SEMANA"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", horaInicio)
            cm.Parameters.AddWithValue("@fechasta", horaFin)
            cm.Parameters.AddWithValue("@ENTORNO", entorno)
            cm.Parameters.AddWithValue("@PYME", pyme)

            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function AccesosKPI_Load_Gauge_Mes(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOSKPI_LOAD_GAUGE_MES"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", horaInicio)
            cm.Parameters.AddWithValue("@fechasta", horaFin)
            cm.Parameters.AddWithValue("@ENTORNO", entorno)
            cm.Parameters.AddWithValue("@PYME", pyme)

            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function AccesosKPI_Load_Gauge_Anyo(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOSKPI_LOAD_GAUGE_ANYO"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", horaInicio)
            cm.Parameters.AddWithValue("@fechasta", horaFin)
            cm.Parameters.AddWithValue("@ENTORNO", entorno)
            cm.Parameters.AddWithValue("@PYME", pyme)

            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function AccesosKPI_Load_Por_Hora_KPI(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String, ByVal kpi As Integer, ByVal idioma As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOSKPI_LOAD_POR_CADA_CINCO_MINUTOS_KPI"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", horaInicio)
            cm.Parameters.AddWithValue("@fechasta", horaFin)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@pyme", pyme)
            cm.Parameters.AddWithValue("@kpi", kpi)
            cm.Parameters.AddWithValue("@idioma", idioma)

            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function AccesosKPI_Load_Por_Dia_KPI(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String, ByVal kpi As Integer, ByVal idioma As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOSKPI_LOAD_POR_HORA_KPI"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", horaInicio)
            cm.Parameters.AddWithValue("@fechasta", horaFin)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@pyme", pyme)
            cm.Parameters.AddWithValue("@kpi", kpi)
            cm.Parameters.AddWithValue("@idioma", idioma)

            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function AccesosKPI_Load_Por_Semana_KPI(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String, ByVal kpi As Integer, ByVal idioma As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOSKPI_LOAD_POR_DIA_KPI"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", horaInicio)
            cm.Parameters.AddWithValue("@fechasta", horaFin)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@pyme", pyme)
            cm.Parameters.AddWithValue("@kpi", kpi)
            cm.Parameters.AddWithValue("@idioma", idioma)

            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function AccesosKPI_Load_Por_Mes_KPI(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String, ByVal kpi As Integer, ByVal idioma As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOSKPI_LOAD_POR_DIA_KPI"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", horaInicio)
            cm.Parameters.AddWithValue("@fechasta", horaFin)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@pyme", pyme)
            cm.Parameters.AddWithValue("@kpi", kpi)
            cm.Parameters.AddWithValue("@idioma", idioma)

            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function AccesosKPI_Load_Por_Anyo_KPI(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String, ByVal kpi As Integer, ByVal idioma As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOSKPI_LOAD_POR_MES_KPI"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", horaInicio)
            cm.Parameters.AddWithValue("@fechasta", horaFin)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@pyme", pyme)
            cm.Parameters.AddWithValue("@kpi", kpi)
            cm.Parameters.AddWithValue("@idioma", idioma)

            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function AccesosKPI_Load_List_Por_Hora_KPI(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String, ByVal idioma As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOSKPI_LOAD_POR_HORA_NOW_KPI"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", horaInicio)
            cm.Parameters.AddWithValue("@fechasta", horaFin)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@pyme", pyme)
            cm.Parameters.AddWithValue("@idioma", idioma)

            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function AccesosKPI_Load_List_Por_Dia_KPI(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String, ByVal idioma As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOSKPI_LOAD_POR_HORA_NOW_KPI"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", horaInicio)
            cm.Parameters.AddWithValue("@fechasta", horaFin)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@pyme", pyme)
            cm.Parameters.AddWithValue("@idioma", idioma)

            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function AccesosKPI_Load_List_Por_Semana_KPI(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String, ByVal idioma As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOSKPI_LOAD_POR_SEMANA_KPI"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", horaInicio)
            cm.Parameters.AddWithValue("@fechasta", horaFin)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@pyme", pyme)
            cm.Parameters.AddWithValue("@idioma", idioma)

            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function AccesosKPI_Load_List_Por_Mes_KPI(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String, ByVal idioma As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOSKPI_LOAD_POR_MES_KPI"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", horaInicio)
            cm.Parameters.AddWithValue("@fechasta", horaFin)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@pyme", pyme)
            cm.Parameters.AddWithValue("@idioma", idioma)

            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function AccesosKPI_Load_List_Por_Anyo_KPI(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String, ByVal idioma As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOSKPI_LOAD_POR_ANYO_KPI"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", horaInicio)
            cm.Parameters.AddWithValue("@fechasta", horaFin)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@pyme", pyme)
            cm.Parameters.AddWithValue("@idioma", idioma)

            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function AccesosKPI_En_Tabla_Accesos_Por_Rango_KPI(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String, ByVal kpi As Integer)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = "SELECT TOP 1 ID FROM FSAL_ACCESOS WITH(NOLOCK) WHERE FISERVER BETWEEN @FECDESDE AND @FECHASTA AND FSAL_ENTORNO=@ENTORNO AND ISNULL(FSAL_PYME,'')=@PYME AND KPI=@KPI AND CATALOGADO=1"
            cm.Parameters.AddWithValue("@FECDESDE", horaInicio)
            cm.Parameters.AddWithValue("@FECHASTA", horaFin)
            cm.Parameters.AddWithValue("@ENTORNO", entorno)
            If Not pyme Is Nothing Then
                cm.Parameters.AddWithValue("@PYME", pyme)
            Else
                cm.Parameters.AddWithValue("@PYME", "")
            End If
            cm.Parameters.AddWithValue("@KPI", kpi)
            cm.CommandType = CommandType.Text
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function AccesosKPI_De_Tabla_Accesos_Por_Rango_KPI(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String, ByVal kpi As Integer)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.CommandText = "FSAL_ACCESOSKPI_LOAD_DESDE_ACCESOS_POR_RANGO_KPI"
            cm.Connection = cn
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecdesde", horaInicio)
            cm.Parameters.AddWithValue("@fechasta", horaFin)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@pyme", pyme)
            cm.Parameters.AddWithValue("@kpi", kpi)

            cm.ExecuteNonQuery()
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Carga todos las QlikApps de la tabla QLIK_APPS, o sólo activas o todas
    ''' </summary>
    ''' <param name="bSoloActivas">Booleano para indicar si obtener todas o solo las activas</param>
    ''' <remarks>Llamada desde: FSNServer.FSNQlikApps, Tiempo maximo: 0</remarks>
    Public Function QlikApps_Lista_Obtener(ByVal bSoloActivas As Boolean) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSBI_QLIKAPPS_LISTA_OBTENER"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@SOLOACTIVAS", bSoloActivas)
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Inserta una QlikApp en la tabla QLIK_APPS
    ''' </summary>
    ''' <param name="lId">Identificador de la QlikApp en la tabla QLIK_APPS</param>
    ''' <param name="sNombre">Nombre de la QlikApp en la tabla QLIK_APPS</param>
    ''' <param name="bActiva">Esta o no activa la QlikApp en la plataforma</param>
    ''' <param name="sUrl">Url del servidor Qlik Sense</param>
    ''' <param name="lPuertoRestAPI">Puerto de la API de autenticacion Qlik Sense</param>
    ''' <param name="sIdApp">Id de la aplicacion de Qlik Sense</param>
    ''' <param name="sApp">Nombre de la aplicacion Qlik Sense</param>
    ''' <param name="sIdSheet">Id de la hoja de aplicacion Qlik Sense</param>
    ''' <param name="sSheet">Nombre de la hoja de aplicacion Qlik Sense</param>
    ''' <returns>Identificador del registro creado</returns>
    ''' <remarks>Llamada desde: FSNServer.FSNQlikApps, Tiempo maximo: 0</remarks>
    Public Function QlikApps_QlikApp_Insertar(ByVal lId As Long, ByVal sNombre As String, ByVal bActiva As Boolean, ByVal sUrl As String, ByVal lPuertoRestAPI As Long, ByVal sIdApp As String, ByVal sApp As String, ByVal sIdSheet As String, ByVal sSheet As String) As Long
        Authenticate()

        Dim cm = New SqlCommand()
        Dim cn As New SqlConnection(mDBConnection)
        Dim lIdCreado As Long = 0

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSBI_QLIKAPPS_QLIKAPP_INSERTAR"
            cm.CommandType = CommandType.StoredProcedure

            cm.Parameters.Add("@ID_CREADO", SqlDbType.Int).Direction = ParameterDirection.Output
            cm.Parameters.AddWithValue("@NOMBRE", sNombre)
            cm.Parameters.AddWithValue("@URL", sUrl)
            cm.Parameters.AddWithValue("@ID_APP", sIdApp)
            cm.Parameters.AddWithValue("@APP", sApp)
            cm.Parameters.AddWithValue("@ID_SHEET", sIdSheet)
            cm.Parameters.AddWithValue("@SHEET", sSheet)
            cm.Parameters.AddWithValue("@REST_API_PORT", lPuertoRestAPI)
            cm.Parameters.AddWithValue("@BAJA", Not bActiva)

            cm.ExecuteNonQuery()
            lIdCreado = cm.Parameters("@ID_CREADO").Value
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try

        Return lIdCreado
    End Function
    ''' <summary>
    ''' Modifica una QlikApp en la tabla QLIK_APPS
    ''' </summary>
    ''' <param name="lId">Identificador de la QlikApp en la tabla QLIK_APPS</param>
    ''' <param name="sNombre">Nombre de la QlikApp en la tabla QLIK_APPS</param>
    ''' <param name="bActiva">Esta o no activa la QlikApp en la plataforma</param>
    ''' <param name="sUrl">Url del servidor Qlik Sense</param>
    ''' <param name="lPuertoRestAPI">Puerto de la API de autenticacion Qlik Sense</param>
    ''' <param name="sIdApp">Id de la aplicacion de Qlik Sense</param>
    ''' <param name="sApp">Nombre de la aplicacion Qlik Sense</param>
    ''' <param name="sIdSheet">Id de la hoja de aplicacion Qlik Sense</param>
    ''' <param name="sSheet">Nombre de la hoja de aplicacion Qlik Sense</param>
    ''' <returns>Identificador del registro modificado</returns>
    ''' <remarks>Llamada desde: FSNServer.FSNQlikApps, Tiempo maximo: 0</remarks>
    Public Function QlikApps_QlikApp_Modificar(ByVal lId As Long, ByVal sNombre As String, ByVal bActiva As Boolean, ByVal sUrl As String, ByVal lPuertoRestAPI As Long, ByVal sIdApp As String, ByVal sApp As String, ByVal sIdSheet As String, ByVal sSheet As String) As Long
        Authenticate()

        Dim cm = New SqlCommand()
        Dim cn As New SqlConnection(mDBConnection)
        Dim lIdModificado As Long = 0

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSBI_QLIKAPPS_QLIKAPP_MODIFICAR"
            cm.CommandType = CommandType.StoredProcedure

            cm.Parameters.Add("@ID_MODIFICADO", SqlDbType.Int).Direction = ParameterDirection.Output
            cm.Parameters.AddWithValue("@ID", lId)
            cm.Parameters.AddWithValue("@NOMBRE", sNombre)
            cm.Parameters.AddWithValue("@URL", sUrl)
            cm.Parameters.AddWithValue("@ID_APP", sIdApp)
            cm.Parameters.AddWithValue("@APP", sApp)
            cm.Parameters.AddWithValue("@ID_SHEET", sIdSheet)
            cm.Parameters.AddWithValue("@SHEET", sSheet)
            cm.Parameters.AddWithValue("@REST_API_PORT", lPuertoRestAPI)
            cm.Parameters.AddWithValue("@BAJA", Not bActiva)

            cm.ExecuteNonQuery()
            lIdModificado = cm.Parameters("@ID_MODIFICADO").Value
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try

        Return lIdModificado
    End Function
    ''' <summary>
    ''' Obtiene la QlikApp indicada de la tabla QLIK_APPS
    ''' </summary>
    ''' <param name="lId">Identificador de la QlikApp en la tabla QLIK_APPS</param>
    ''' <remarks>Llamada desde: FSNServer.FSNQlikApps, Tiempo maximo: 0</remarks>
    Public Function QlikApps_QlikApp_Obtener(ByVal lId As Long) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSBI_QLIKAPPS_QLIKAPP_OBTENER"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@ID", lId)
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Elimina la Qlik App indicada de la tabla QLIK_APPS
    ''' </summary>
    ''' <param name="lId">Identificador de la QlikApp en la tabla QLIK_APPS</param>
    ''' <returns>Identificador del registro eliminado</returns>
    ''' <remarks>Llamada desde: FSNServer.FSNQlikApps, Tiempo maximo: 0</remarks>
    Public Function QlikApps_QlikApp_Eliminar(ByVal lId As Long) As Long
        Authenticate()

        Dim cm = New SqlCommand()
        Dim cn As New SqlConnection(mDBConnection)
        Dim lIdEliminado As Long = 0

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSBI_QLIKAPPS_QLIKAPP_ELIMINAR"
            cm.CommandType = CommandType.StoredProcedure

            cm.Parameters.Add("@ID_ELIMINADO", SqlDbType.Int).Direction = ParameterDirection.Output
            cm.Parameters.AddWithValue("@ID", lId)

            cm.ExecuteNonQuery()
            lIdEliminado = cm.Parameters("@ID_ELIMINADO").Value
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try

        Return lIdEliminado
    End Function
    ''' <summary>
    ''' Obtiene el primer codigo de cliente de la tabla CLIENTES
    ''' </summary>
    ''' <returns>Codigo de cliente de la tabla CLIENTES</returns>
    ''' <remarks>Llamada desde: FSNServer.FSNQlikConfig, Tiempo maximo: 0</remarks>
    Public Function QlikApps_Cliente_Codigo_Obtener() As String
        Authenticate()

        Dim cm = New SqlCommand()
        Dim cn As New SqlConnection(mDBConnection)
        Dim sCodCliente As String = ""

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSBI_QLIKAPPS_CLIENTE_CODIGO_OBTENER"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@COD_CLIENTE", SqlDbType.NVarChar, 3).Direction = ParameterDirection.Output
            cm.ExecuteNonQuery()
            sCodCliente = cm.Parameters("@COD_CLIENTE").Value
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try

        Return sCodCliente
    End Function
    ''' <summary>
    ''' Obtiene el identificador del Usuario Qlik correspondiente al identificador de Perfil FSN pasado y la URL del su servidor Qlik Sense, desde la tabla QLIK_PERF_USUQLIK
    ''' </summary>
    ''' <param name="sCodUsuFSN">Codigo del Usuario FSN para el que se quiere obtener el Usuario Qlik</param>
    ''' <returns>Par clave/valor con el identificador del Usuario Qlik y la Url de su servidor Qlik Sense</returns>
    ''' <remarks>Llamada desde: FSNServer.FSNQlikConfig, Tiempo maximo: 0</remarks>
    Public Function QlikApps_Usuario_Qlik_Obtener(ByVal sCodUsuFSN As String) As Dictionary(Of String, String)
        Authenticate()

        Dim cm = New SqlCommand()
        Dim cn As New SqlConnection(mDBConnection)
        Dim dicIdUrlUsuQlik As New Dictionary(Of String, String)
        dicIdUrlUsuQlik.Add("", "")
        Dim sIdUsuQlik As String = ""
        Dim sUrlUsuQlik As String = ""

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSBI_QLIKAPPS_USUARIO_QLIK_OBTENER"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USUCOD", sCodUsuFSN)
            cm.Parameters.Add("@ID_USU_QLIK", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output
            cm.Parameters.Add("@URL_USU_QLIK", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output
            cm.ExecuteNonQuery()
            sIdUsuQlik = cm.Parameters("@ID_USU_QLIK").Value
            sUrlUsuQlik = cm.Parameters("@URL_USU_QLIK").Value
            dicIdUrlUsuQlik.Clear()
            dicIdUrlUsuQlik.Add(sIdUsuQlik, sUrlUsuQlik)
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try

        Return dicIdUrlUsuQlik
    End Function
    ''' <summary>
    ''' Obtiene los registros de perfiles de la plataforma y usuarios Qlik asignados desde la tabla QLIK_PERF_USUQLIK
    ''' </summary>
    ''' <param name="sIdiomaCod">Codigo de idioma del usuario de la plataforma</param>
    ''' <returns>Dataset con los registros</returns>
    ''' <remarks>Llamada desde: FSNServer.FSNQlikConfig, Tiempo maximo: 0</remarks>
    Public Function ObtenerPerfilesUsuariosQlikAsignados(ByVal sIdiomaCod As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            If (sIdiomaCod <> "SPA") AndAlso _
                (sIdiomaCod <> "ENG") AndAlso _
                (sIdiomaCod <> "GER") AndAlso _
                (sIdiomaCod <> "FRA") Then
                sIdiomaCod = "ENG"
            End If
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSBI_QLIKAPPS_PERF_USUQLIK_OBTENER"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDIOMA", sIdiomaCod)
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Obtiene los servidores Qlik configurados en la tabla QLIK_APPS
    ''' </summary>
    ''' <returns>Dataset con los registros</returns>
    ''' <remarks>Llamada desde: FSNServer.FSNQlikConfig, Tiempo maximo: 0</remarks>
    Public Function ObtenerServidoresQlikConfigurados() As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSBI_QLIKAPPS_SERVIDORES_QLIK_OBTENER"
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Obtiene los perfiles de la plataforma que todavia no tienen asignado un usuario Qlik
    ''' </summary>
    ''' <param name="sIdiomaCod">Codigo de idioma del usuario de la plataforma</param>
    ''' <returns>Dataset con los registros</returns>
    ''' <remarks>Llamada desde: FSNServer.FSNQlikConfig, Tiempo maximo: 0</remarks>
    Public Function ObtenerPerfilesFSNNoAsignados(ByVal sIdiomaCod As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            If (sIdiomaCod <> "SPA") AndAlso _
                (sIdiomaCod <> "ENG") AndAlso _
                (sIdiomaCod <> "GER") AndAlso _
                (sIdiomaCod <> "FRA") Then
                sIdiomaCod = "ENG"
            End If
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSBI_QLIKAPPS_PERF_NO_ASIGNADOS_OBTENER"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDIOMA", sIdiomaCod)
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Inserta las asignaciones de perfiles de la plataforma y usuarios de Qlik en la tabla QLIK_PERF_USUQLIK
    ''' </summary>
    ''' <param name="dtPerfUsuQlik">Datatable con los registros a insertar</param>
    ''' <returns>Numero de registros creados</returns>
    ''' <remarks>Llamada desde: FSNServer.FSNQlikConfig, Tiempo maximo: 0</remarks>
    Public Function QlikApps_Perf_UsuQlik_Insertar(ByVal dtPerfUsuQlik As DataTable) As Integer
        Authenticate()

        Dim cm = New SqlCommand()
        Dim cn As New SqlConnection(mDBConnection)
        Dim iNumRegCreados As Integer = -2

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSBI_QLIKAPPS_PERF_USUQLIK_INSERTAR"
            cm.CommandType = CommandType.StoredProcedure

            cm.Parameters.Add("@NUM_REG_CREADOS", SqlDbType.Int).Direction = ParameterDirection.Output
            cm.Parameters.AddWithValue("@TABLE_PERF_USUQLIK", dtPerfUsuQlik)

            cm.ExecuteNonQuery()
            iNumRegCreados = cm.Parameters("@NUM_REG_CREADOS").Value
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try

        Return iNumRegCreados
    End Function
#End Region
End Class
