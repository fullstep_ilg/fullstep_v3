﻿Imports System.Configuration
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Exception
Imports System.IO
Imports System.Object
Imports Fullstep.FSNLibrary
Partial Public Class Root
#Region "FSN"
    ''' <summary>
    ''' Obtiene los escenarios que el usuario puede utilizar por tipo de visor
    ''' </summary>
    ''' <param name="UsuCod">CÃ³digo de usuario </param>
    ''' <param name="TipoVisor">El tipo de visor para obtener los escenarios (1-solicitudes,2-no conf,3-certif,4-contratos)</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function FSN_Get_Escenarios_Usuario(ByVal UsuCod As String, ByVal Idioma As String, ByVal TipoVisor As FSNLibrary.TipoVisor,
                    ByVal IdEscenario As Integer, ByVal OcultarEscenarioDefecto As Boolean, ByVal Edicion As Boolean,
                    ByVal RestricUsuDep As Boolean, ByVal RestricUsuUON As Boolean, ByVal RestricUsuPerfUON As Boolean) As DataSet
        Authenticate()

        Dim TipoSolicitud As Integer?
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_GET_ESCENARIOS_USU"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@IDI", Idioma)
            cm.Parameters.AddWithValue("@TIPOVISOR", TipoVisor)
            Select Case TipoVisor
                Case FSNLibrary.TipoVisor.NoConformidades
                    TipoSolicitud = FSNLibrary.TiposDeDatos.TipoDeSolicitud.NoConformidad
                Case FSNLibrary.TipoVisor.Certificados
                    TipoSolicitud = FSNLibrary.TiposDeDatos.TipoDeSolicitud.Certificado
                Case FSNLibrary.TipoVisor.Contratos
                    TipoSolicitud = FSNLibrary.TiposDeDatos.TipoDeSolicitud.Contrato
                Case FSNLibrary.TipoVisor.Facturas
                    TipoSolicitud = FSNLibrary.TiposDeDatos.TipoDeSolicitud.Factura
                Case FSNLibrary.TipoVisor.SolicitudesQA
                    TipoSolicitud = FSNLibrary.TiposDeDatos.TipoDeSolicitud.SolicitudQA
                Case FSNLibrary.TipoVisor.Encuestas
                    TipoSolicitud = FSNLibrary.TiposDeDatos.TipoDeSolicitud.Encuesta
                Case Else
                    TipoSolicitud = Nothing
            End Select
            cm.Parameters.AddWithValue("@TIPOSOLICITUD", TipoSolicitud)
            If Not IdEscenario = 0 Then cm.Parameters.AddWithValue("@IDESCENARIO", IdEscenario)
            cm.Parameters.AddWithValue("@OCULTARESCENARIODEFECTO", IIf(IdEscenario = -1, True, OcultarEscenarioDefecto))
            cm.Parameters.AddWithValue("@EDICION", Edicion)
            cm.Parameters.AddWithValue("@RESTRIC_EDIT_USU_DEP", RestricUsuDep)
            cm.Parameters.AddWithValue("@RESTRIC_EDIT_USU_UON", RestricUsuUON)
            cm.Parameters.AddWithValue("@RESTRIC_EDIT_USU_PERFUON", RestricUsuPerfUON)
            da.SelectCommand = cm
            da.Fill(ds)

            ds.Tables(0).TableName = "ESCENARIOS_USUARIO"
            ds.Tables(1).TableName = "ESCENARIO_SOLICITUD_FORMULARIOS_USUARIO"
            ds.Tables(2).TableName = "ESCENARIO_FILTROS_USUARIO"
            ds.Tables(3).TableName = "ESCENARIO_FILTRO_CAMPOS_USUARIO"
            ds.Tables(4).TableName = "ESCENARIO_FILTRO_CONDICIONES_USUARIO"
            ds.Tables(5).TableName = "ESCENARIO_VISTAS_USUARIO"
            ds.Tables(6).TableName = "ESCENARIO_VISTA_CAMPOS_USUARIO"
            If Edicion Then
                ds.Tables(7).TableName = "SOLICITUD_FORMULARIO_USUARIO"
                ds.Tables(8).TableName = "CARPETAS_USUARIO"
            End If

            Return ds
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Obtiene las carpetas creadas por el usuario para organizar los escenarios.
    ''' </summary>
    ''' <param name="UsuCod">CÃ³digo de usuario </param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function FSN_Get_Carpetas_Usuario(ByVal UsuCod As String, ByVal TipoVisor As Integer) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_GET_CARPETAS_USU"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@TIPOVISOR", TipoVisor)
            da.SelectCommand = cm
            da.Fill(ds)

            Return ds.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Agrega una nueva carpeta para el usuario para organizar los escenarios
    ''' </summary>
    ''' <param name="UsuCod">CÃ³digo del usuario</param>
    ''' <param name="Nombre">Nombre dado a la carpeta</param>
    ''' <param name="Carpeta">El id de la carpeta contenedora. Si no la contiene ninguna se pasa 0</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function FSN_Insert_Carpeta_Usuario(ByVal UsuCod As String, ByVal Nombre As String, ByVal Carpeta As Integer, ByVal TipoVisor As Integer) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_CARPETA_USU_INSERT"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@NOMBRE", Nombre)
            cm.Parameters.AddWithValue("@TIPOVISOR", TipoVisor)
            If Not Carpeta = 0 Then cm.Parameters.AddWithValue("@CARPETA", Carpeta)
            da.SelectCommand = cm
            da.Fill(ds)

            Return ds.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Modifica el nombre de la carpeta
    ''' </summary>
    ''' <param name="IdCarpeta">Id de la carpeta modificada</param>
    ''' <param name="UsuCod">CÃ³digo del usuario de la carpeta</param>
    ''' <param name="Nombre">Nuevo nombre para la carpeta</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function FSN_Update_Carpeta_Usuario(ByVal IdCarpeta As Integer, ByVal UsuCod As String, ByVal Nombre As String, ByVal TipoVisor As Integer) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_CARPETA_USU_UPDATE"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@ID", IdCarpeta)
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@NOMBRE", Nombre)
            cm.Parameters.AddWithValue("@TIPOVISOR", TipoVisor)
            da.SelectCommand = cm
            da.Fill(ds)

            Return ds.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Elimina la carpeta y sus subcarpetas. Los escenarios vinculados a cualquiera de ellas pasan a mostrarse en la carpeta raiz
    ''' </summary>
    ''' <param name="IdCarpeta">Id de la carpeta a borrar</param>
    ''' <param name="UsuCod">CÃ³digo del usuario</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function FSN_Delete_Carpeta_Usuario(ByVal IdCarpeta As Integer, ByVal UsuCod As String, ByVal TipoVisor As Integer) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_CARPETA_USU_DELETE"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@ID", IdCarpeta)
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@TIPOVISOR", TipoVisor)
            da.SelectCommand = cm
            da.Fill(ds)

            Return ds.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Mueve la ubicaciÃ³n de la carpeta en el arbol del usuario. Se mueve con sus subcarpetas
    ''' </summary>
    ''' <param name="IdCarpeta">Id de la carpeta a mover</param>
    ''' <param name="Id_Destino">Id de la carpeta que contendra la carpeta a mover. Si es la raiz sera cero.</param>    
    ''' <param name="UsuCod">CÃ³digo del usuario propietario de la carpeta</param>
    ''' <param name="TipoVisor">Visor al que pertenecen las carpetas</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function FSN_Paste_Carpeta_Usuario(ByVal IdCarpeta As Integer, ByVal Id_Destino As Integer, ByVal UsuCod As String, ByVal TipoVisor As Integer) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_CARPETA_USU_PASTE"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@ID", IdCarpeta)
            If Not Id_Destino = 0 Then cm.Parameters.AddWithValue("@ID_DESTINO", Id_Destino)
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@TIPOVISOR", TipoVisor)
            da.SelectCommand = cm
            da.Fill(ds)

            Return ds.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' Obtiene los campos que pertenecen al formulario, incluidos los campos de desglose.
    ''' </summary>
    ''' <param name="IdFormulario">Id del formulario a obtener los campos</param>
    ''' <param name="bSacarNumLinea">Si se saca NumLinea en el desglose o no . Tarea 3369</param> 
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function FSN_Get_Campos_Formulario(ByVal PerCod As String, ByVal Idioma As String, ByVal IdFormulario As Integer, Optional ByVal bSacarNumLinea As Boolean = False) As DataTable
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_GET_CAMPOS_FORMULARIO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@PER", PerCod)
            cm.Parameters.AddWithValue("@IDI", Idioma)
            cm.Parameters.AddWithValue("@IDFORMULARIO", IdFormulario)
            cm.Parameters.AddWithValue("@SALENUMLINEA", bSacarNumLinea)
            da.SelectCommand = cm
            da.Fill(ds)

            Return ds.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function FSN_Insert_Escenario_Usuario(ByVal Usuario As String, ByVal Idioma As String, ByVal EscenarioCarpeta As Integer, ByVal EscenarioNombre As String,
                ByVal EscenarioDefecto As Boolean, ByVal EscenarioFavorito As Boolean, ByVal EscenarioAplicaTodas As Boolean,
                ByVal EscenarioTipoVisor As Integer, ByVal EscenarioFormularios As DataTable, ByVal dsFiltroVistas As DataSet,
                ByVal OcultarEscenarioDefecto As Boolean, ByVal EscenarioPosicion As Integer,
                ByVal RestricEdicionUsuDep As Boolean, ByVal RestricEdicionUsuUON As Boolean, ByVal RestricEdicionUsuPerfUON As Boolean) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim da As New SqlDataAdapter
        Dim drFiltroCampos(), drFiltroCondiciones(), drVistaCampos() As DataRow
        cn.Open()
        Dim transaccion As SqlTransaction = cn.BeginTransaction()
        Try
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_INSERT_ESCENARIO"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@USUARIO", Usuario)
                If Not EscenarioCarpeta = 0 Then .AddWithValue("@CARPETA", EscenarioCarpeta)
                .AddWithValue("@NOMBRE", EscenarioNombre)
                .AddWithValue("@DEFECTO", EscenarioDefecto)
                .AddWithValue("@FAVORITO", EscenarioFavorito)
                .AddWithValue("@APLICATODAS", EscenarioAplicaTodas)
                .AddWithValue("@TIPOVISOR", EscenarioTipoVisor)
                .AddWithValue("@POSICION", EscenarioPosicion)
                .Add("@IDESCENARIO", SqlDbType.Int).Direction = ParameterDirection.Output
            End With
            cm.ExecuteNonQuery()

            Dim IdEscenario As Integer = cm.Parameters("@IDESCENARIO").Value
            If Not EscenarioFormularios.Rows.Count = 0 Then
                cm = New SqlCommand("FSN_ESCENARIOS_ASOCIAR_SOLICITUD_FORMULARIO", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion
                With cm.Parameters
                    .AddWithValue("@ESCENARIO", IdEscenario)
                    .Add("@SOLICITUD", SqlDbType.Int, 0, "SOLICITUD")
                    .Add("@FORMULARIO", SqlDbType.Int, 0, "FORMULARIO")
                End With
                da.InsertCommand = cm
                da.Update(EscenarioFormularios)
            End If
            Dim IdFiltro As Integer
            For Each filtro As DataRow In dsFiltroVistas.Tables("FILTROS").Rows
                cm = New SqlCommand("FSN_ESCENARIOS_INSERT_FILTRO", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion
                With cm.Parameters
                    .AddWithValue("@ESCENARIO", IdEscenario)
                    .AddWithValue("@USUARIO", Usuario)
                    .AddWithValue("@NOMBRE", filtro("NOMBRE"))
                    .AddWithValue("@DEFECTO", filtro("DEFECTO"))
                    .AddWithValue("@FORMULAAVANZADA", filtro("FORMULAAVANZADA"))
                    .AddWithValue("@POSICION", 0)
                    .Add("@IDFILTRO", SqlDbType.Int).Direction = ParameterDirection.Output
                End With
                cm.ExecuteNonQuery()
                IdFiltro = cm.Parameters("@IDFILTRO").Value
                drFiltroCampos = dsFiltroVistas.Tables("FILTRO_CAMPOS").Select("FILTRO=" & filtro("ID"))
                If Not drFiltroCampos.Length = 0 Then
                    cm = New SqlCommand("FSN_ESCENARIOS_ASOCIAR_FILTRO_CAMPOS", cn)
                    cm.CommandType = CommandType.StoredProcedure
                    cm.CommandTimeout = 300
                    cm.Transaction = transaccion
                    With cm.Parameters
                        .AddWithValue("@FILTRO", IdFiltro)
                        .Add("@CAMPO", SqlDbType.Int, 0, "CAMPO")
                        .Add("@ESGENERAL", SqlDbType.Bit, 0, "ESGENERAL")
                        .Add("@VISIBLE", SqlDbType.Bit, 0, "VISIBLE")
                    End With
                    da.InsertCommand = cm
                    da.Update(drFiltroCampos)
                End If
                drFiltroCondiciones = dsFiltroVistas.Tables("FILTRO_CONDICIONES").Select("FILTRO=" & filtro("ID"))
                If Not drFiltroCondiciones.Length = 0 Then
                    cm = New SqlCommand("FSN_ESCENARIOS_ASOCIAR_FILTRO_CONDICIONES", cn)
                    cm.CommandType = CommandType.StoredProcedure
                    cm.CommandTimeout = 300
                    cm.Transaction = transaccion
                    With cm.Parameters
                        .AddWithValue("@FILTRO", IdFiltro)
                        .Add("@ORDEN", SqlDbType.Int, 0, "ORDEN")
                        .Add("@CAMPO", SqlDbType.Int, 0, "CAMPO")
                        .Add("@ESGENERAL", SqlDbType.Bit, 0, "ESGENERAL")
                        .Add("@DENOMINACION", SqlDbType.NVarChar, 500, "DENOMINACION")
                        .Add("@DENOMINACION_BD", SqlDbType.NVarChar, 100, "DENOMINACION_BD")
                        .Add("@OPERADOR", SqlDbType.Int, 0, "OPERADOR")
                        .Add("@VALOR", SqlDbType.NVarChar, 0, "VALOR")
                    End With
                    da.InsertCommand = cm
                    da.Update(drFiltroCondiciones)
                End If
            Next

            Dim IdVista As Integer
            For Each vista As DataRow In dsFiltroVistas.Tables("VISTAS").Rows
                cm = New SqlCommand("FSN_ESCENARIOS_INSERT_VISTA", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion
                With cm.Parameters
                    .AddWithValue("@ESCENARIO", IdEscenario)
                    .AddWithValue("@USUARIO", Usuario)
                    .AddWithValue("@NOMBRE", vista("NOMBRE"))
                    .AddWithValue("@DEFECTO", vista("DEFECTO"))
                    .AddWithValue("@ORDEN", vista("ORDEN"))
                    .AddWithValue("@POSICION", 0)
                    .Add("@IDVISTA", SqlDbType.Int).Direction = ParameterDirection.Output
                End With
                cm.ExecuteNonQuery()
                IdVista = cm.Parameters("@IDVISTA").Value
                drVistaCampos = dsFiltroVistas.Tables("VISTA_CAMPOS").Select("VISTA=" & vista("ID"))
                If Not drVistaCampos.Length = 0 Then
                    cm = New SqlCommand("FSN_ESCENARIOS_ASOCIAR_VISTA_CAMPOS", cn)
                    cm.CommandType = CommandType.StoredProcedure
                    cm.CommandTimeout = 300
                    cm.Transaction = transaccion
                    With cm.Parameters
                        .AddWithValue("@VISTA", IdVista)
                        .Add("@CAMPO", SqlDbType.Int, 0, "CAMPO")
                        .Add("@ESGENERAL", SqlDbType.Bit, 0, "ESGENERAL")
                        .Add("@ESDESGLOSE", SqlDbType.Bit, 0, "ESDESGLOSE")
                        .Add("@NOMBREPERSONALIZADO", SqlDbType.NVarChar, 100, "NOMBREPERSONALIZADO")
                        .Add("@ANCHO", SqlDbType.Float, 0, "ANCHO")
                        .Add("@POSICION", SqlDbType.Int, 0, "POSICION")
                    End With
                    da.InsertCommand = cm
                    da.Update(drVistaCampos)
                End If
            Next

            transaccion.Commit()

            Return FSN_Get_Escenarios_Usuario(Usuario, Idioma, EscenarioTipoVisor, IdEscenario, True, False, RestricEdicionUsuDep, RestricEdicionUsuUON, RestricEdicionUsuPerfUON)
        Catch e As Exception
            transaccion.Rollback()
            Return Nothing
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function FSN_Edit_Escenario_Usuario(ByVal Usuario As String, ByVal Idioma As String, ByVal IdEscenario As Integer, ByVal EscenarioCarpeta As Integer,
                ByVal EscenarioNombre As String, ByVal EscenarioDefecto As Boolean, ByVal EscenarioFavorito As Boolean,
                ByVal EscenarioAplicaTodas As Boolean, ByVal EscenarioTipoVisor As Integer,
                ByVal RestricEdicionUsuDep As Boolean, ByVal RestricEdicionUsuUON As Boolean, ByVal RestricEdicionUsuPerfUON As Boolean) As DataSet
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        cn.Open()

        Try
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_EDIT_ESCENARIO"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            With cm.Parameters
                .AddWithValue("@IDESCENARIO", IdEscenario)
                .AddWithValue("@USU", Usuario)
                .AddWithValue("@IDI", Idioma)
                If Not EscenarioCarpeta = 0 Then .AddWithValue("@CARPETA", EscenarioCarpeta)
                .AddWithValue("@NOMBRE", EscenarioNombre)
                .AddWithValue("@DEFECTO", EscenarioDefecto)
                .AddWithValue("@FAVORITO", EscenarioFavorito)
                .AddWithValue("@APLICATODAS", EscenarioAplicaTodas)
                .AddWithValue("@TIPOVISOR", EscenarioTipoVisor)
            End With
            cm.ExecuteNonQuery()

            Return FSN_Get_Escenarios_Usuario(Usuario, Idioma, EscenarioTipoVisor, IdEscenario, True, False, RestricEdicionUsuDep, RestricEdicionUsuUON, RestricEdicionUsuPerfUON)
        Catch e As Exception
            Return Nothing
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    Public Sub FSN_Delete_Escenario_Usuario(ByVal Usuario As String, ByVal IdEscenario As Integer, ByVal Carpeta As Integer)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_DELETE_ESCENARIO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDESCENARIO", IdEscenario)
            If Not Carpeta = 0 Then cm.Parameters.AddWithValue("@CARPETA", Carpeta)
            cm.Parameters.AddWithValue("@USU", Usuario)
            cm.ExecuteNonQuery()
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    Public Function FSN_Insert_Escenario_Filtro_Usuario(ByVal Usuario As String, ByVal Idioma As String, ByVal IdEscenario As Integer, ByVal EscenarioTipoVisor As Integer,
                ByVal EscenarioFiltroNombre As String, ByVal EscenarioFiltroDefecto As Boolean, ByVal EscenarioFiltroFormulaAvanzada As Boolean,
                ByVal EscenarioFiltroPosicion As Integer, ByVal EscenarioFiltroCampos As DataTable, ByVal EscenarioFiltroCondiciones As DataTable) As Object
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim da As New SqlDataAdapter
        cn.Open()
        Dim transaccion As SqlTransaction = cn.BeginTransaction()
        Try
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_INSERT_FILTRO"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@ESCENARIO", IdEscenario)
                .AddWithValue("@USUARIO", Usuario)
                .AddWithValue("@NOMBRE", EscenarioFiltroNombre)
                .AddWithValue("@DEFECTO", EscenarioFiltroDefecto)
                .AddWithValue("@FORMULAAVANZADA", EscenarioFiltroFormulaAvanzada)
                .AddWithValue("@POSICION", EscenarioFiltroPosicion)
                .Add("@IDFILTRO", SqlDbType.Int).Direction = ParameterDirection.Output
            End With
            cm.ExecuteNonQuery()

            Dim IdFiltro As Integer = cm.Parameters("@IDFILTRO").Value
            cm = New SqlCommand("FSN_ESCENARIOS_ASOCIAR_FILTRO_CAMPOS", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion

            With cm.Parameters
                .AddWithValue("@FILTRO", IdFiltro)
                .Add("@CAMPO", SqlDbType.Int, 0, "CAMPO")
                .Add("@ESGENERAL", SqlDbType.Bit, 0, "ESGENERAL")
                .Add("@VISIBLE", SqlDbType.Bit, 0, "VISIBLE")
            End With

            da.InsertCommand = cm
            da.Update(EscenarioFiltroCampos)

            cm = New SqlCommand("FSN_ESCENARIOS_ASOCIAR_FILTRO_CONDICIONES", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion

            With cm.Parameters
                .AddWithValue("@FILTRO", IdFiltro)
                .Add("@ORDEN", SqlDbType.Int, 0, "ORDEN")
                .Add("@CAMPO", SqlDbType.Int, 0, "CAMPO")
                .Add("@ESGENERAL", SqlDbType.Bit, 0, "ESGENERAL")
                .Add("@DENOMINACION", SqlDbType.NVarChar, 500, "DENOMINACION")
                .Add("@DENOMINACION_BD", SqlDbType.NVarChar, 100, "DENOMINACION_BD")
                .Add("@OPERADOR", SqlDbType.Int, 0, "OPERADOR")
                .Add("@VALOR", SqlDbType.NVarChar, 0, "VALOR")
            End With

            da.InsertCommand = cm
            da.Update(EscenarioFiltroCondiciones)

            transaccion.Commit()

            Return {FSN_Get_Escenarios_Usuario(Usuario, Idioma, EscenarioTipoVisor, IdEscenario, True, False, False, False, False), IdFiltro}
        Catch e As Exception
            transaccion.Rollback()
            Return Nothing
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function FSN_Edit_Escenario_Filtro_Usuario(ByVal Usuario As String, ByVal Idioma As String, ByVal IdEscenario As Integer, ByVal EscenarioTipoVisor As Integer,
                ByVal IdFiltro As Integer, ByVal EscenarioFiltroNombre As String, ByVal EscenarioFiltroDefecto As Boolean, ByVal EscenarioFiltroFormulaAvanzada As Boolean,
                ByVal EscenarioFiltroCampos As DataTable, ByVal EscenarioFiltroCondiciones As DataTable) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim da As New SqlDataAdapter
        cn.Open()
        Dim transaccion As SqlTransaction = cn.BeginTransaction()
        Try
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_EDIT_FILTRO"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@IDFILTRO", IdFiltro)
                .AddWithValue("@ESCENARIO", IdEscenario)
                .AddWithValue("@NOMBRE", EscenarioFiltroNombre)
                .AddWithValue("@FORMULAAVANZADA", EscenarioFiltroFormulaAvanzada)
            End With
            cm.ExecuteNonQuery()

            cm = New SqlCommand("FSN_ESCENARIOS_DESASOCIAR_FILTRO_CAMPOS_CONDICIONES", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@IDFILTRO", IdFiltro)
            End With
            cm.ExecuteNonQuery()

            cm = New SqlCommand("FSN_ESCENARIOS_ASOCIAR_FILTRO_CAMPOS", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@FILTRO", IdFiltro)
                .Add("@CAMPO", SqlDbType.Int, 0, "CAMPO")
                .Add("@ESGENERAL", SqlDbType.Bit, 0, "ESGENERAL")
                .Add("@VISIBLE", SqlDbType.Bit, 0, "VISIBLE")
            End With

            da.InsertCommand = cm
            da.Update(EscenarioFiltroCampos)

            cm = New SqlCommand("FSN_ESCENARIOS_ASOCIAR_FILTRO_CONDICIONES", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@FILTRO", IdFiltro)
                .Add("@ORDEN", SqlDbType.Int, 0, "ORDEN")
                .Add("@CAMPO", SqlDbType.Int, 0, "CAMPO")
                .Add("@ESGENERAL", SqlDbType.Bit, 0, "ESGENERAL")
                .Add("@DENOMINACION", SqlDbType.NVarChar, 500, "DENOMINACION")
                .Add("@DENOMINACION_BD", SqlDbType.NVarChar, 100, "DENOMINACION_BD")
                .Add("@OPERADOR", SqlDbType.Int, 0, "OPERADOR")
                .Add("@VALOR", SqlDbType.NVarChar, 0, "VALOR")
            End With

            da.InsertCommand = cm
            da.Update(EscenarioFiltroCondiciones)

            transaccion.Commit()

            Return FSN_Get_Escenarios_Usuario(Usuario, Idioma, EscenarioTipoVisor, IdEscenario, True, False, False, False, False)
        Catch e As Exception
            transaccion.Rollback()
            Return Nothing
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Sub FSN_Delete_Escenario_Filtro_Usuario(ByVal IdEscenario As Integer, ByVal IdEscenarioFiltro As Integer)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_DELETE_ESCENARIO_FILTRO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDESCENARIO", IdEscenario)
            cm.Parameters.AddWithValue("@IDESCENARIOFILTRO", IdEscenarioFiltro)
            cm.ExecuteNonQuery()
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    Public Function FSN_Insert_Escenario_Vista_Usuario(ByVal Usuario As String, ByVal Idioma As String, ByVal IdEscenario As Integer, ByVal EscenarioTipoVisor As FSNLibrary.TipoVisor,
                ByVal EscenarioVistaNombre As String, ByVal EscenarioVistaDefecto As Boolean, ByVal EscenarioVistaPosicion As Integer,
                ByVal EscenarioVistaCampos As DataTable, ByVal EscenarioVistaOrden As String) As Object
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim da As New SqlDataAdapter
        cn.Open()
        Dim transaccion As SqlTransaction = cn.BeginTransaction()
        Try
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_INSERT_VISTA"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@ESCENARIO", IdEscenario)
                .AddWithValue("@USUARIO", Usuario)
                .AddWithValue("@NOMBRE", EscenarioVistaNombre)
                .AddWithValue("@DEFECTO", EscenarioVistaDefecto)
                .AddWithValue("@ORDEN", EscenarioVistaOrden)
                .AddWithValue("@POSICION", EscenarioVistaPosicion)
                .Add("@IDVISTA", SqlDbType.Int).Direction = ParameterDirection.Output
            End With
            cm.ExecuteNonQuery()

            Dim IdVista As Integer = cm.Parameters("@IDVISTA").Value
            cm = New SqlCommand("FSN_ESCENARIOS_ASOCIAR_VISTA_CAMPOS", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion

            With cm.Parameters
                .AddWithValue("@VISTA", IdVista)
                .Add("@CAMPO", SqlDbType.Int, 0, "CAMPO")
                .Add("@ESGENERAL", SqlDbType.Bit, 0, "ESGENERAL")
                .Add("@ESDESGLOSE", SqlDbType.Bit, 0, "ESDESGLOSE")
                .Add("@NOMBREPERSONALIZADO", SqlDbType.NVarChar, 100, "NOMBREPERSONALIZADO")
                .Add("@ANCHO", SqlDbType.Float, 0, "ANCHO")
                .Add("@POSICION", SqlDbType.Int, 0, "POSICION")
            End With

            da.InsertCommand = cm
            da.Update(EscenarioVistaCampos)

            transaccion.Commit()

            Return {FSN_Get_Escenarios_Usuario(Usuario, Idioma, EscenarioTipoVisor, IdEscenario, True, False, False, False, False), IdVista}
        Catch e As Exception
            transaccion.Rollback()
            Return Nothing
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function FSN_Edit_Escenario_Vista_Usuario(ByVal Usuario As String, ByVal Idioma As String, ByVal IdEscenario As Integer, ByVal EscenarioTipoVisor As Integer,
                ByVal IdVista As Integer, ByVal EscenarioVistaNombre As String, ByVal EscenarioVistaDefecto As Boolean,
                ByVal Orden As String, ByVal EscenarioVistaCampos As DataTable) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim da As New SqlDataAdapter
        cn.Open()
        Dim transaccion As SqlTransaction = cn.BeginTransaction()
        Try
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_EDIT_VISTA"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@IDVISTA", IdVista)
                .AddWithValue("@ESCENARIO", IdEscenario)
                .AddWithValue("@NOMBRE", EscenarioVistaNombre)
                .AddWithValue("@ORDEN", Orden)
            End With
            cm.ExecuteNonQuery()

            cm = New SqlCommand("FSN_ESCENARIOS_DESASOCIAR_VISTA_CAMPOS", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@IDVISTA", IdVista)
            End With
            cm.ExecuteNonQuery()

            cm = New SqlCommand("FSN_ESCENARIOS_ASOCIAR_VISTA_CAMPOS", cn)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion

            With cm.Parameters
                .AddWithValue("@VISTA", IdVista)
                .Add("@CAMPO", SqlDbType.Int, 0, "CAMPO")
                .Add("@ESGENERAL", SqlDbType.Bit, 0, "ESGENERAL")
                .Add("@ESDESGLOSE", SqlDbType.Bit, 0, "ESDESGLOSE")
                .Add("@NOMBREPERSONALIZADO", SqlDbType.NVarChar, 100, "NOMBREPERSONALIZADO")
                .Add("@ANCHO", SqlDbType.Float, 0, "ANCHO")
                .Add("@POSICION", SqlDbType.Int, 0, "POSICION")
            End With

            da.InsertCommand = cm
            da.Update(EscenarioVistaCampos)

            transaccion.Commit()

            Return FSN_Get_Escenarios_Usuario(Usuario, Idioma, EscenarioTipoVisor, IdEscenario, True, False, False, False, False)
        Catch e As Exception
            transaccion.Rollback()
            Return Nothing
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Sub FSN_Delete_Escenario_Vista_Usuario(ByVal IdEscenarioVista As Integer, ByVal IdEscenario As Integer)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_DELETE_ESCENARIO_VISTA"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDESCENARIOVISTA", IdEscenarioVista)
            cm.Parameters.AddWithValue("@IDESCENARIO", IdEscenario)
            cm.ExecuteNonQuery()
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub

    Public Sub FSN_Update_Escenario_Vista(ByVal IdEscenarioVista As Integer, ByVal Orden As String)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_UPDATE_ESCENARIO_VISTA_ORDEN"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDESCENARIOVISTA", IdEscenarioVista)
            cm.Parameters.AddWithValue("@ORDEN", Orden)
            cm.ExecuteNonQuery()
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub

    Public Function FSN_Get_Escenarios_TiposSolicitud_Formulario(ByVal Usuario As String, ByVal Idioma As String, ByVal IdsSolicitud As String, ByVal TipoSolicitud As Nullable(Of Integer))
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_GET_TIPOS_SOLICITUD"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", Usuario)
            cm.Parameters.AddWithValue("@IDI", Idioma)
            cm.Parameters.AddWithValue("@TIPOSOLICITUD", TipoSolicitud)
            If IdsSolicitud IsNot String.Empty Then cm.Parameters.AddWithValue("@IDSSOLICITUD", IdsSolicitud)
            da.SelectCommand = cm
            da.Fill(ds)

            Return ds.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    Public Function FSN_Get_Opciones_Lista_Campo(ByVal IdCampo As Integer)
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_GET_OPCIONES_LISTA_CAMPO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDCAMPO", IdCampo)
            da.SelectCommand = cm
            da.Fill(ds)

            Return ds.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    Public Function FSN_Get_Opciones_Lista_EstadoHomologacion(SolicitudFormularioVinculados As Dictionary(Of String, SerializableKeyValuePair(Of Integer, String)))
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_GET_OPCIONES_LISTA_ESTADOHOMOLOGACION"
            cm.CommandType = CommandType.StoredProcedure

            Dim dtSolicitudes As New DataTable("SOLICITUDES")
            dtSolicitudes.Columns.Add("SOLICITUD", GetType(Integer))
            For Each oSol As KeyValuePair(Of String, SerializableKeyValuePair(Of Integer, String)) In SolicitudFormularioVinculados
                Dim drSol As DataRow = dtSolicitudes.NewRow
                drSol("SOLICITUD") = CType(oSol.Key, Integer)
                dtSolicitudes.Rows.Add(drSol)
            Next
            Dim ParamOrdenesId As New SqlParameter("SOLICITUDES", SqlDbType.Structured)
            ParamOrdenesId.TypeName = "ORDENESID"
            ParamOrdenesId.Value = dtSolicitudes
            cm.Parameters.Add(ParamOrdenesId)

            da.SelectCommand = cm
            da.Fill(ds)

            Return ds.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Function
    Public Function FSN_Get_EscenariosFavoritos_Usuario(ByVal UsuCod As String, ByVal TipoVisor As Integer, ByVal OcultarEscenarioDefecto As Boolean) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_GET_ESCENARIOSFAVORITOS_USU"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@TIPOVISOR", TipoVisor)
            cm.Parameters.AddWithValue("@OCULTARESCENARIODEFECTO", OcultarEscenarioDefecto)
            da.SelectCommand = cm
            da.Fill(ds)

            Return ds
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Sub FSN_Establecer_Escenario_Favorito(ByVal UsuCod As String, ByVal TipoVisor As Integer, ByVal IdEscenario As Integer, ByVal Favorito As Boolean)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_ESTABLECER_ESCENARIO_FAVORITO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@TIPOVISOR", TipoVisor)
            cm.Parameters.AddWithValue("@IDESCENARIO", IdEscenario)
            cm.Parameters.AddWithValue("@FAVORITO", Favorito)
            da.SelectCommand = cm
            da.Fill(ds)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Sub



















































    Public Sub FSN_Ordenar_Filtros(ByVal IdEscenario As Integer, ByVal IdFiltro As Integer,
                                      ByVal PosicionAnterior As Integer, ByVal PosicionActual As Integer)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_ORDENAR_FILTROS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDESCENARIO", IdEscenario)
            cm.Parameters.AddWithValue("@IDFILTRO", IdFiltro)
            cm.Parameters.AddWithValue("@POSICIONANTERIOR", PosicionAnterior)
            cm.Parameters.AddWithValue("@POSICIONACTUAL", PosicionActual)
            cm.ExecuteNonQuery()
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    Public Sub FSN_Ordenar_Vistas(ByVal IdEscenario As Integer, ByVal IdVista As Integer,
                                      ByVal PosicionAnterior As Integer, ByVal PosicionActual As Integer)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_ORDENAR_VISTAS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDESCENARIO", IdEscenario)
            cm.Parameters.AddWithValue("@IDVISTA", IdVista)
            cm.Parameters.AddWithValue("@POSICIONANTERIOR", PosicionAnterior)
            cm.Parameters.AddWithValue("@POSICIONACTUAL", PosicionActual)
            cm.ExecuteNonQuery()
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    Public Sub FSN_Establecer_VistaDefecto_Filtro(ByVal IdFiltro As Integer, ByVal IdVista As Integer)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_ESTABLECER_VISTADEFECTO_FILTRO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@IDFILTRO", IdFiltro)
            cm.Parameters.AddWithValue("@IDVISTA", IdVista)
            cm.ExecuteNonQuery()
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
    Public Function FSN_Get_Escenario_Compartir_Usu(ByVal IdEscenario As Integer) As DataTable
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_GET_COMPARTIR_USU"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@ESCENARIO", IdEscenario)
            da.SelectCommand = cm
            da.Fill(ds)

            Return ds.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function FSN_Get_Escenario_Compartir_UON(ByVal IdEscenario As Integer, ByVal Idioma As String) As DataTable
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_GET_COMPARTIR_UON"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@ESCENARIO", IdEscenario)
            cm.Parameters.AddWithValue("@IDIOMA", Idioma)
            da.SelectCommand = cm
            da.Fill(ds)

            Return ds.Tables(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Sub FSN_Edit_Escenario_Compartir(ByVal IdEscenario As Integer, ByVal CompartirUON0 As Boolean,
                                            ByVal Usuarios As String, ByVal UONs As String, ByVal CompartirPortal As Boolean)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim da As New SqlDataAdapter
        cn.Open()
        Dim transaccion As SqlTransaction = cn.BeginTransaction()
        Try
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_EDIT_COMPARTIR"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion


            With cm.Parameters
                .AddWithValue("@ESCENARIO", IdEscenario)
                .AddWithValue("@UON0", CompartirUON0)
                .AddWithValue("@USUARIOS", Usuarios)
                .AddWithValue("@UONS", UONs)
                .AddWithValue("@PORTAL", CompartirPortal)
            End With
            cm.ExecuteNonQuery()

            transaccion.Commit()
        Catch e As Exception
            transaccion.Rollback()
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Sub
    Public Function FSN_Get_TextoLargo_Instancia(ByVal Instancia As Integer, ByVal IdCampoOrigen As Integer, ByVal EsDesglose As Boolean) As String
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_GET_TEXTOLARGO_INSTANCIA"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@INSTANCIA", Instancia)
            cm.Parameters.AddWithValue("@IDCAMPOORIGEN", IdCampoOrigen)
            cm.Parameters.AddWithValue("@ESDESGLOSE", EsDesglose)
            da.SelectCommand = cm
            da.Fill(ds)

            Return ds.Tables(0).Rows(0)(0)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Sub FSN_Establecer_Escenario_Opciones_Usu(ByVal dtEscenarios As DataTable)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_ESTABLECER_ESCENARIO_OPCIONES_USU"
            cm.CommandType = CommandType.StoredProcedure
            With cm.Parameters
                .Add("@ESCENARIO", SqlDbType.Int, 0, "ESCENARIO")
                .Add("@USU", SqlDbType.NVarChar, 50, "USU")
                .Add("@CARPETA", SqlDbType.Int, 0, "CARPETA")
                .Add("@DEFECTO", SqlDbType.Bit, 1, "DEFECTO")
                .Add("@FAVORITO", SqlDbType.Bit, 0, "FAVORITO")
                .Add("@POSICION", SqlDbType.Int, 0, "POSICION")
                .Add("@TIPOVISOR", SqlDbType.Int, 0, "TIPOVISOR")
            End With

            da.InsertCommand = cm
            da.Update(dtEscenarios)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="Idioma"></param>
    ''' <param name="UON1"></param>
    ''' <param name="UON2"></param>
    ''' <param name="UON3"></param>
    ''' <param name="DEP"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function FSN_Obtener_UONsCompartir(ByVal UsuCod As String, ByVal Idioma As String,
                ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, ByVal DEP As String,
                ByVal Restr_UON As Boolean, ByVal Restr_Dep As Boolean, ByVal Restr_UON_Perf As Boolean,
                ByVal Busqueda As String, ByVal Nivel As Integer) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_UONS_COMPARTIR_USU"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@IDI", Idioma)
            cm.Parameters.AddWithValue("@UON1", UON1)
            cm.Parameters.AddWithValue("@UON2", UON2)
            cm.Parameters.AddWithValue("@UON3", UON3)
            cm.Parameters.AddWithValue("@DEP", DEP)
            cm.Parameters.AddWithValue("@RESTR_UON", Restr_UON)
            cm.Parameters.AddWithValue("@RESTR_DEP", Restr_Dep)
            cm.Parameters.AddWithValue("@RESTR_UON_PERF", Restr_UON_Perf)
            If Not Busqueda Is String.Empty Then cm.Parameters.AddWithValue("@BUSQUEDA", Busqueda)
            If Not Nivel = -1 Then cm.Parameters.AddWithValue("@NIVEL", Nivel)

            da.SelectCommand = cm
            da.Fill(dr)

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Function FSN_Obtener_USUCompartir(ByVal UsuCod As String, ByVal Idioma As String,
                ByVal codigo As String, ByVal nombre As String, ByVal apellido As String, ByVal busqueda As String,
                ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, ByVal DEP As String,
                ByVal Restr_UON As Boolean, ByVal Restr_Dep As Boolean, ByVal Restr_UON_Perf As Boolean,
                ByVal UON1Buscado As String, ByVal UON2Buscado As String,
                ByVal UON3Buscado As String, ByVal DepBuscado As String) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand()
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_USU_COMPARTIR_USU"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@IDI", Idioma)
            If Not codigo Is String.Empty Then cm.Parameters.AddWithValue("@CODIGO", codigo)
            If Not nombre Is String.Empty Then cm.Parameters.AddWithValue("@NOMBRE", nombre)
            If Not apellido Is String.Empty Then cm.Parameters.AddWithValue("@APELLIDO", apellido)
            If Not busqueda Is String.Empty Then cm.Parameters.AddWithValue("@BUSQUEDA", busqueda)
            cm.Parameters.AddWithValue("@UON1", UON1)
            cm.Parameters.AddWithValue("@UON2", UON2)
            cm.Parameters.AddWithValue("@UON3", UON3)
            cm.Parameters.AddWithValue("@DEP", DEP)
            cm.Parameters.AddWithValue("@RESTR_UON", Restr_UON)
            cm.Parameters.AddWithValue("@RESTR_DEP", Restr_Dep)
            cm.Parameters.AddWithValue("@RESTR_UON_PERF", Restr_UON_Perf)
            If Not UON1Buscado Is String.Empty Then cm.Parameters.AddWithValue("@UON1BUSCADO", UON1Buscado)
            If Not UON2Buscado Is String.Empty Then cm.Parameters.AddWithValue("@UON2BUSCADO", UON2Buscado)
            If Not UON3Buscado Is String.Empty Then cm.Parameters.AddWithValue("@UON3BUSCADO", UON3Buscado)
            If Not DepBuscado Is String.Empty Then cm.Parameters.AddWithValue("@DEPBUSCADO", DepBuscado)

            da.SelectCommand = cm
            da.Fill(dr)

            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Sub FSN_Establecer_Filtro_Opciones_Usu(ByVal UsuCod As String, ByVal dtFiltros As DataTable)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_ESTABLECER_FILTRO_OPCIONES_USU"
            cm.CommandType = CommandType.StoredProcedure
            With cm.Parameters
                .AddWithValue("@USU", UsuCod)
                .Add("@FILTRO", SqlDbType.Int, 0, "FILTRO")
                .Add("@DEFECTO", SqlDbType.Bit, 1, "DEFECTO")
                .Add("@POSICION", SqlDbType.Int, 0, "POSICION")
                .Add("@IDVISTADEFECTO", SqlDbType.Int, 0, "IDVISTADEFECTO")
            End With

            da.InsertCommand = cm
            da.Update(dtFiltros)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Sub
    Public Function FSN_Edit_Escenario_Filtro_Usuario_Condiciones(ByVal Usuario As String, ByVal Idioma As String, ByVal IdEscenario As Integer, ByVal EscenarioTipoVisor As Integer,
                ByVal IdFiltro As Integer, ByVal EscenarioFiltroCondiciones As DataTable) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim da As New SqlDataAdapter
        cn.Open()
        Dim transaccion As SqlTransaction = cn.BeginTransaction()
        Try
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_ELIMINAR_FILTRO_CONDICIONES"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            cm.Parameters.AddWithValue("@USU", Usuario)
            cm.Parameters.AddWithValue("@FILTRO", IdFiltro)
            cm.ExecuteNonQuery()

            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_ACTUALIZAR_FILTRO_CONDICIONES"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            cm.Transaction = transaccion
            With cm.Parameters
                .AddWithValue("@FILTRO", IdFiltro)
                .AddWithValue("@USU", Usuario)
                .Add("@ORDEN", SqlDbType.Int, 0, "ORDEN")
                .Add("@CAMPO", SqlDbType.Int, 0, "CAMPO")
                .Add("@ESGENERAL", SqlDbType.Bit, 0, "ESGENERAL")
                .Add("@DENOMINACION", SqlDbType.NVarChar, 500, "DENOMINACION")
                .Add("@DENOMINACION_BD", SqlDbType.NVarChar, 100, "DENOMINACION_BD")
                .Add("@OPERADOR", SqlDbType.Int, 0, "OPERADOR")
                .Add("@VALOR", SqlDbType.NVarChar, 0, "VALOR")
            End With

            da.InsertCommand = cm
            da.Update(EscenarioFiltroCondiciones)

            transaccion.Commit()
            Return FSN_Get_Escenarios_Usuario(Usuario, Idioma, EscenarioTipoVisor, IdEscenario, True, False, False, False, False)
        Catch e As Exception
            transaccion.Rollback()
            Return Nothing
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Sub FSN_Establecer_Vista_Opciones_Usu(ByVal UsuCod As String, ByVal dtFiltros As DataTable)
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_ESTABLECER_VISTA_OPCIONES_USU"
            cm.CommandType = CommandType.StoredProcedure
            With cm.Parameters
                .AddWithValue("@USU", UsuCod)
                .Add("@VISTA", SqlDbType.Int, 0, "VISTA")
                .Add("@DEFECTO", SqlDbType.Bit, 1, "DEFECTO")
                .Add("@ORDEN", SqlDbType.NVarChar, 500, "ORDEN")
                .Add("@POSICION", SqlDbType.Int, 0, "POSICION")
            End With

            da.InsertCommand = cm
            da.Update(dtFiltros)
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Sub
    Public Function FSN_Edit_Escenario_Vista_Usuario_Configuracion(ByVal Usuario As String, ByVal Idioma As String, ByVal IdEscenario As Integer,
                ByVal EscenarioTipoVisor As Integer, ByVal IdVista As Integer, ByVal EscenarioVistaCampos As DataTable) As DataSet
        Authenticate()

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Dim da As New SqlDataAdapter
        cn.Open()
        Try
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_ACTUALIZAR_VISTA_CONFIGURACION"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300

            With cm.Parameters
                .AddWithValue("@VISTA", IdVista)
                .AddWithValue("@USU", Usuario)
                .Add("@CAMPO", SqlDbType.Int, 0, "CAMPO")
                .Add("@ESGENERAL", SqlDbType.Bit, 0, "ESGENERAL")
                .Add("@NOMBREPERSONALIZADO", SqlDbType.NVarChar, 100, "NOMBREPERSONALIZADO")
                .Add("@ANCHO", SqlDbType.Float, 0, "ANCHO")
                .Add("@POSICION", SqlDbType.Int, 0, "POSICION")
            End With

            da.InsertCommand = cm
            da.Update(EscenarioVistaCampos)

            Return FSN_Get_Escenarios_Usuario(Usuario, Idioma, EscenarioTipoVisor, IdEscenario, True, False, False, False, False)
        Catch e As Exception
            Return Nothing
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    Public Sub FSN_Establecer_Filtro_Defecto(ByVal UsuCod As String, ByVal TipoVisor As Integer, ByVal IdFiltro As Integer, ByVal Defecto As Boolean)
        Authenticate()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand = New SqlCommand
        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSN_ESCENARIOS_ESTABLECER_FILTRO_DEFECTO"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@USU", UsuCod)
            cm.Parameters.AddWithValue("@TIPOVISOR", TipoVisor)
            cm.Parameters.AddWithValue("@IDFILTRO", IdFiltro)
            cm.Parameters.AddWithValue("@DEFECTO", Defecto)
            cm.ExecuteNonQuery()
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
#End Region
End Class
