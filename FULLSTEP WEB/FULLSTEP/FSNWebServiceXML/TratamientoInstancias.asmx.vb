﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.IO

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/TratamientoInstancias/TratamientoInstancias")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class TratamientoInstancias
    Inherits System.Web.Services.WebService

    ''' <summary>Almacena el contenido en un archivo XML en la carpeta indicada en el web.config</summary>
    ''' <param name="ContenidoXML">Contenido del archivo</param>
    ''' <param name="NombreArchivoXML">Nombre del archivo</param>
    ''' <remarks></remarks>
    <WebMethod()>
    Public Sub TransferirXMLEnEpera(ByVal ContenidoXML As String, ByVal NombreArchivoXML As String)
        Try
            Dim oXML As XElement = XElement.Parse(ContenidoXML)
            oXML.Save(Path.Combine(ConfigurationManager.AppSettings("rutaXMLEnEspera"), NombreArchivoXML & "#FSNWEB.xml"))

            'Lo renombramos    
            If File.Exists(Path.Combine(ConfigurationManager.AppSettings("rutaXMLEnEspera"), NombreArchivoXML & ".xml")) Then File.Delete(Path.Combine(ConfigurationManager.AppSettings("rutaXMLEnEspera"), NombreArchivoXML & ".xml"))
            FileSystem.Rename(Path.Combine(ConfigurationManager.AppSettings("rutaXMLEnEspera"), NombreArchivoXML & "#FSNWEB.xml"), Path.Combine(ConfigurationManager.AppSettings("rutaXMLEnEspera"), NombreArchivoXML & ".xml"))
        Catch ex As Exception
            TratarError(ex)
            Throw ex
        End Try
    End Sub

    ''' <summary>Guardado del error producido en BD</summary>
    ''' <param name="oError">Objeto exception</param>
    ''' <remarks>Llamada desde: TransferirXMLEnEpera</remarks>
    Private Sub TratarError(ByRef oError As Exception)
        Dim oErrores As FSNServer.Errores
        Dim tmpExcepcion As Exception = oError
        Dim sEx_FullName As String = tmpExcepcion.GetType().FullName
        Dim sEx_Message As String = tmpExcepcion.Message
        Dim sEx_StackTrace As String = tmpExcepcion.StackTrace

        Dim FSNServer As FSNServer.Root = New FSNServer.Root(True)
        oErrores = FSNServer.Get_Object(GetType(FSNServer.Errores))
        oErrores.Create("TratamientoInstancias.asmx.vb", String.Empty, sEx_FullName, sEx_Message, sEx_StackTrace, String.Empty, String.Empty)
        oErrores = Nothing
    End Sub


End Class