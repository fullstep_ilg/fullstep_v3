﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Serialization

<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class FSN_Services
    Inherits System.Web.Services.WebService

    Private FSNServer As FSNServer.Root
    Private m_sUsu As String

    <WebMethod()>
    Public Function Obtener_Centros_Coste(ByVal CodUsuario As String, ByVal Idioma As String, ByVal listaUON As String,
                                     ByVal EsAutocompletar As Boolean, ByVal filtroCodigo As String, ByVal filtroDenominacion As String)
        Dim oUser As FSNServer.User
        Try
            Dim serializer As New JavaScriptSerializer
            FSNServer = New FSNServer.Root(True)
            oUser = FSNServer.Login(CodUsuario, True)

            Dim lListaUON As List(Of String())
            lListaUON = serializer.Deserialize(Of List(Of String()))(listaUON)
            Dim xmlInfoUONs As XElement
            xmlInfoUONs = New XElement("InfoUONSeleccionadas", lListaUON.AsEnumerable().Select(Function(x) _
                                                                                                    New XElement("UONS", {
                                                                                                        New XElement("UON1", x(0)),
                                                                                                        If(x.Length > 1, New XElement("UON2", x(1)), Nothing),
                                                                                                        If(x.Length > 2, New XElement("UON3", x(2)), Nothing),
                                                                                                        If(x.Length > 3, New XElement("UON4", x(3)), Nothing)
                                                                                                    })))

            Dim oCentros_SM As FSNServer.Centros_SM = FSNServer.Get_Object(GetType(FSNServer.Centros_SM))
            Return serializer.Serialize(oCentros_SM.Obtener_Centros_Coste(CodUsuario, Idioma, If(lListaUON.Count = 0, Nothing, xmlInfoUONs.ToString), EsAutocompletar, filtroCodigo, filtroDenominacion) _
                .Select(Function(x) New With {.Codigo = x.Codigo, .Denominacion = x.Denominacion}))
        Catch ex As Exception
            Dim sError As String = ex.Message
            Dim oErrores As FSNServer.Errores
            oErrores = FSNServer.Get_Object(GetType(FSNServer.Errores))
            Try
                oErrores.Create("FSSM\Obtener_Centros_Coste", "", ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
                oErrores = Nothing
            Catch exError As Exception
                Throw New Exception("Se ha producido un error.", ex)
            End Try
            Throw New Exception("Se ha producido un error.", ex)
        End Try
    End Function
    <WebMethod()>
    Public Function Obtener_Partidas_Presupuestarias(ByVal CodUsuario As String, ByVal Idioma As String, ByVal CodCentroCoste As String, ByVal RaizPresupuestaria As String,
                                                ByVal EsAutocompletar As Boolean, ByVal filtroCodigo As String, ByVal filtroDenominacion As String)
        Dim oUser As FSNServer.User
        Try
            Dim serializer As New JavaScriptSerializer
            FSNServer = New FSNServer.Root(True)
            oUser = FSNServer.Login(CodUsuario, True)

            Dim oPartidaPRES5 As FSNServer.PartidasPRES5 = FSNServer.Get_Object(GetType(FSNServer.PartidasPRES5))
            Return serializer.Serialize(oPartidaPRES5.Obtener_Partidas_Presupuestarias(CodUsuario, Idioma, CodCentroCoste, RaizPresupuestaria, EsAutocompletar, filtroCodigo, filtroDenominacion))
        Catch ex As Exception
            Dim sError As String = ex.Message
            Dim oErrores As FSNServer.Errores
            oErrores = FSNServer.Get_Object(GetType(FSNServer.Errores))
            Try
                oErrores.Create("FSSM\Obtener_Partidas_Presupuestarias", "", ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
                oErrores = Nothing
            Catch exError As Exception
                Throw New Exception("Se ha producido un error.", ex)
            End Try
            Throw New Exception("Se ha producido un error.", ex)
        End Try
    End Function
End Class