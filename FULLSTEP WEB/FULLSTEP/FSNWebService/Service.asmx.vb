﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Threading
Imports System.IO
Imports Fullstep.FSNServer


' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/WebServicePM/Service")> _
<ToolboxItem(False)> _
Public Class Service1
    Inherits System.Web.Services.WebService

    Private Shared oMutex As Mutex = New Mutex(False)
    Private Shared oMutexVal As Mutex = New Mutex(False)
    Dim FSNServer As FSNServer.Root
    Dim oError As Exception
    Dim strMensajeError As String
    Private m_ds As DataSet
    'Variables que se utilizan en ServiceThread y  TratarXML
    Private m_bPortal As Boolean
    Private m_sFile As String
    Private m_sProveGS As String
    Private m_sPer As String
    Private m_sEmail As String
    Private m_oXMLFolder As DirectoryInfo

    Private Enum TiempoProcesamiento
        InicioDisco = 1
        InicioBD = 2
        Fin = 3
    End Enum
    ''' <summary>
    ''' Almacena el fichero XML que se pasa con el nombre correcto. Guardamos la fecha de inicio en la tablas
    ''' de tiempos de procesamiento y obtenemos su identity para almacenarlo en el XML. Una vez guardado el XML 
    ''' registramos también el tiempo, y llamamos asíncronamente a ServiceThread para procesar los XML que haya.
    ''' </summary>
    ''' <param name="ContenidoXML">Documento XML</param>
    ''' <param name="sXMLName">Nombre del XML</param>
    ''' <param name="sUser">Usuario</param>
    ''' <param name="sPassword">ContraseÃ±a</param>
    ''' <param name="sCia">Compañía</param>
    ''' <remarks>LLamada desde: guardarinstancia.aspx.vb; Tiempo máximo: 1 sg.</remarks>
    <WebMethod()> _
    Public Sub ProcesarXML(ByVal ContenidoXML As String, ByVal sXMLName As String, ByVal sUser As String, ByVal sPassword As String, ByVal sCia As String)
        Dim oXMLDoc As New System.Xml.XmlDocument

        oXMLDoc.InnerXml = ContenidoXML

        Dim bPortal As Boolean
        If sCia <> "" Then   'LLAMAMOS DESDE PORTAL
            bPortal = True
            FSNServer = New FSNServer.Root
            FSNServer.LoginPortal(sUser, sPassword)
        Else
            bPortal = False
            FSNServer = New FSNServer.Root
            FSNServer.Login(sUser, sPassword)
        End If


        'Almacenar el fichero XML que se pasa al WebService con el nombre correcto
        Dim oXMLFolder As DirectoryInfo = New DirectoryInfo(ConfigurationManager.AppSettings("rutaXMLService"))
        Dim oXMLFile As FileInfo

        Dim sSplitXMLName() As String = sXMLName.Split("#")
        Dim sSplitoXMLFile() As String

        'Podrían haber varios ficheros de la misma instancia y bloque luego hay que numerarlos por orden de llegada
        'NOTA: los nombres de los ficheros XML que llegan de PM portal tienen un dato más.
        Dim intNumFile As Integer = 0
        If sSplitXMLName.Length = 4 Then
            For Each oXMLFile In oXMLFolder.GetFiles(sSplitXMLName(0) & "#" & sSplitXMLName(1) & "#" & sSplitXMLName(2) & "#" & sSplitXMLName(3) & "#*.xml")
                sSplitoXMLFile = oXMLFile.Name.Split("#")
                intNumFile = Math.Max(CLng(sSplitoXMLFile(4)), intNumFile)
            Next
        Else
            For Each oXMLFile In oXMLFolder.GetFiles(sSplitXMLName(0) & "#" & sSplitXMLName(1) & "#" & sSplitXMLName(2) & "#*.xml")
                sSplitoXMLFile = oXMLFile.Name.Split("#")
                intNumFile = Math.Max(CLng(sSplitoXMLFile(3)), intNumFile)
            Next
        End If
        intNumFile = intNumFile + 1

        'Obtenemos el id del tiempo de procesamiento
        Dim lIDTiempoProc As Long
        Dim oInstancia As FSNServer.Instancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        If bPortal Then
            oInstancia.ID = CLng(sSplitXMLName(2))
        Else
            oInstancia.ID = CLng(sSplitXMLName(1))
        End If
        oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, sSplitXMLName(0))

        'Lo añadimos al XML
        Dim objNode As System.Xml.XmlNode
        Dim objAttrib As System.Xml.XmlAttribute
        objNode = oXMLDoc.CreateElement("TIEMPO_PROC")
        objAttrib = oXMLDoc.CreateAttribute("ID")
        objAttrib.Value = lIDTiempoProc
        objNode.Attributes.Append(objAttrib)
        oXMLDoc.DocumentElement.AppendChild(objNode)

        'Guardamos el nuevo fichero 
        Dim oFileW As System.IO.StreamWriter

        If bPortal Then
            oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("rutaXMLService") & "\" & sXMLName & "#" & intNumFile & "#P#.xml")
        Else
            oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("rutaXMLService") & "\" & sXMLName & "#" & intNumFile & "#.xml")
        End If

        oFileW.Write(oXMLDoc.InnerXml)
        oFileW.Close()
        oFileW.Dispose()
        oFileW = Nothing

        'Realizar Asíncronamente el procesamiento de los XML. 
        Dim oThread As Thread = New Thread(AddressOf ServiceThread)
        oThread.Start()

    End Sub
    ''' <summary>
    ''' Se llama a esta fución desde el popup de detalle de la solicitud cuando se pulsa en aprobar/rechazar.
    ''' Actualiza el estado de validación de la instancia y genera un XML con el id de tiempo de procesamiento, 
    ''' la acción y el comentario, y lanza el procesamiento de acción asíncronamente.
    ''' </summary>
    ''' <param name="lInstancia">Id de la instancia</param>
    ''' <param name="lAccion">Id de la acción</param>
    ''' <param name="lBloque">Id del bloque</param>
    ''' <param name="sUser">Usuario</param>
    ''' <param name="sPassword">Contraseña</param>
    ''' <param name="sComentario">Comentario que puede introducir el usuario</param>
    ''' <param name="iTipoSolicitud">Tipo de solicitud: factura, contrato, ...</param>	
    ''' <remarks>Llamada desde: detallesolicconsulta.aspx; Tiempo máximo: 2 sg.</remarks>
    <WebMethod()> _
    Public Sub ProcesarAccion(ByVal lInstancia As Long, ByVal lAccion As Long, ByVal lBloque As Long, ByVal sUser As String, ByVal sPassword As String, ByVal sComentario As String, ByVal iTipoSolicitud As Integer)
        Dim lIDTiempoProc As Long
        ActualizarEstadoValidacion(lInstancia, lAccion, lBloque, lIDTiempoProc, sUser, sPassword)
        GenerarXML(lInstancia, lAccion, lBloque, sComentario, sUser, lIDTiempoProc, iTipoSolicitud)
        RealizarValidacionesAsinc()
    End Sub
    ''' <summary>
    ''' Se llama a esta función desde el visor de solicitudes al realizar aprobación múltiple.
    ''' Actualiza el estado de validación de la instancia y genera un XML con el id de tiempo de procesamiento, 
    ''' la acción y el comentario por cada una de las solicitudes, y lanza el procesamiento de acción asíncronamente.
    ''' </summary>
    ''' <param name="sAccionesValidar">Cadena con las instancias, con su bloque y acción.</param>
    ''' <param name="sUser">Usuario</param>
    ''' <param name="sPassword">Contraseña</param>
    ''' <remarks>Llamada desde: VisorSolicitudes.aspx. Tiempo máximo: 2 sg.</remarks>
    <WebMethod()> _
    Public Sub ProcesarAcciones(ByVal sAccionesValidar As String, ByVal sUser As String, ByVal sPassword As String)
        Dim arrAcciones As String() = sAccionesValidar.Split("#")
        Dim lIDTiempoProc As Long
        For i As Integer = 0 To UBound(arrAcciones)
            lIDTiempoProc = 0
            Dim arrAccion As String() = arrAcciones(i).Split("|")
            ActualizarEstadoValidacion(CLng(arrAccion(0)), CLng(arrAccion(2)), CLng(arrAccion(1)), lIDTiempoProc, sUser, sPassword)
            GenerarXML(CLng(arrAccion(0)), CLng(arrAccion(2)), CLng(arrAccion(1)), arrAccion(3), sUser, lIDTiempoProc, If(arrAccion.Length > 4, CType(arrAccion(4), TiposDeDatos.TipoDeSolicitud), TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras))
        Next
        RealizarValidacionesAsinc()
    End Sub
    ''' <summary>
    ''' Actualiza el estado de la validación de la instancia a estado "en cola" y guardamos la fecha de inicio en la tablas
    ''' de tiempos de procesamiento y obtenemos su identity para almacenarlo en el XML.
    ''' </summary>
    ''' <param name="lInstancia">Id de la instancia</param>
    ''' <param name="lAccion">Id de la acción</param>
    ''' <param name="lBloque">Id del bloque</param>
    ''' <param name="sUser">Usuario</param>
    ''' <param name="sPassword">Contraseña</param>
    ''' <param name="lIDTiempoProc">Id del tiempo de procesamiento que se pasa por referencia</param>
    ''' <remarks>Llamada desde: ProcesarAccion y ProcesarAcciones. Tiempo máximo: 1 sg.</remarks>
    Private Sub ActualizarEstadoValidacion(ByVal lInstancia As Long, ByVal lAccion As Long, ByVal lBloque As Long, ByRef lIDTiempoProc As Long, ByVal sUser As String, Optional ByVal sPassword As String = "")
        If FSNServer Is Nothing Then
            FSNServer = New FSNServer.Root
            FSNServer.Login(sUser, sPassword)
        End If
        Dim oInstancia As FSNServer.Instancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        oInstancia.ID = lInstancia
        oInstancia.ActualizarEstadoValidacion(EstadoValidacion.EnCola, lAccion & "," & lBloque & "," & sUser)
        'Obtenemos el id del tiempo de procesamiento e introducimos la fecha de inicio
        oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, sUser)
    End Sub
    Private Sub RealizarValidacionesAsinc()
        'Lanzar el procesamiento de acción asíncronamente
        Dim oThread As Thread = New Thread(AddressOf RealizarValidacionesThread)
        oThread.Start()

    End Sub
    ''' <summary>
    ''' Realiza las validaciones correspondientes (próxima etapa, próximos participantes, ...) por cada una de las instancias pendientes.
    ''' </summary>
    ''' <remarks>Llamada desde: RealizarValidacionesAsinc; Tiempo máximo: depende del nº de instancias pendientes.</remarks>
    Private Sub RealizarValidacionesThread()
        Dim oInstancia As Instancia
        Dim lBloque As Long
        Dim bEjecutarAcciones As Boolean = False
        oMutexVal.WaitOne(-1, True)
        Try

            Dim oInstancias As Instancias
            oInstancias = FSNServer.Get_Object(GetType(FSNServer.Instancias))
            Dim ds As DataSet = oInstancias.InstanciasPdtesValidacion()
            Dim oUsuario As User
            For Each dr As DataRow In ds.Tables(0).Rows
                Try
                    Dim i As Integer = InStr(dr.Item("ERROR_VALIDACION"), ",")
                    Dim lAccion As Long = Left(dr.Item("ERROR_VALIDACION"), i - 1)
                    Dim j As Integer = InStr(i + 1, dr.Item("ERROR_VALIDACION"), ",")
                    lBloque = Mid(dr.Item("ERROR_VALIDACION"), i + 1, j - i - 1)
                    Dim sUser As String = Right(dr.Item("ERROR_VALIDACION"), CType(dr.Item("ERROR_VALIDACION"), String).Length - j)
                    If oUsuario Is Nothing OrElse oUsuario.Cod <> sUser Then
                        oUsuario = FSNServer.Get_Object(GetType(FSNServer.User))
                        oUsuario.LoadUserData(sUser)
                        If String.IsNullOrEmpty(oUsuario.Idioma) Then
                            oUsuario.Idioma = ConfigurationManager.AppSettings("idioma")
                        End If
                    End If
                    oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                    oInstancia.ID = dr.Item("ID")
                    oInstancia.Cargar(oUsuario.Idioma)
                    If oInstancia.Grupos Is Nothing Then oInstancia.CargarCamposInstancia(oUsuario.Idioma, oUsuario.CodPersona, , True)
                    Dim oAccion As Accion = FSNServer.Get_Object(GetType(FSNServer.Accion))
                    oAccion.Id = lAccion
                    oAccion.CargarAccion(oUsuario.Idioma)
                    Dim bValidacion As Boolean = True
                    ' Si es autoasignación, asignarle el rol y comprobar que la solicitud no está asignada ya a otro usuario
                    oInstancia.ActualizarEstadoValidacion(EstadoValidacion.YaAsignadaAOtroUsuario, Nothing)
                    bValidacion = oInstancia.ComprobarAsignacion(oUsuario.CodPersona, oUsuario.Idioma)
                    ' Comprobar Próxima Etapa
                    Dim sRolPorWebService As String = "-1"
                    Dim dsSiguienteEtapa As DataSet = Nothing
                    If bValidacion Then
                        oInstancia.ActualizarEstadoValidacion(EstadoValidacion.SiguienteEtapa, Nothing)
                        dsSiguienteEtapa = oInstancia.ComprobarTieneSiguienteEtapa(lAccion, oUsuario.CodPersona, oUsuario.Idioma, sRolPorWebService)
                        bValidacion = (dsSiguienteEtapa.Tables.Count > 1)
                    End If
                    ' Comprobar Próximos participantes
                    If bValidacion Then
                        oInstancia.ActualizarEstadoValidacion(EstadoValidacion.ProximosParticipantes, Nothing)
                        If Not (sRolPorWebService = "-1") Then ' Los participantes se deciden en Web Service.
                            Dim sBloqueRolPorWebService() As String
                            sBloqueRolPorWebService = Split(sRolPorWebService, ",")
                            Dim sArrRolPorWebService() As String
                            Dim lRolPorWebService As String

                            Dim oDTPMParticipantes As DataTable

                            If Not ds.Tables.Contains("TEMP_PARTICIPANTES_EXTERNOS") Then
                                oDTPMParticipantes = ds.Tables.Add("TEMP_PARTICIPANTES_EXTERNOS")
                                oDTPMParticipantes.Columns.Add("ID", System.Type.GetType("System.Int32"))
                                oDTPMParticipantes.Columns.Add("ROL", System.Type.GetType("System.Int32"))
                                oDTPMParticipantes.Columns.Add("PER", System.Type.GetType("System.String"))
                                'esto es un caso muy concreto de gestamp, nunca habra proveedor implicado

                                Dim keys(1) As DataColumn
                                keys(0) = oDTPMParticipantes.Columns("ROL")
                                keys(1) = oDTPMParticipantes.Columns("PER")
                                oDTPMParticipantes.PrimaryKey = keys
                            Else
                                oDTPMParticipantes = ds.Tables("TEMP_PARTICIPANTES_EXTERNOS")
                                oDTPMParticipantes.Rows.Clear()
                            End If

                            For Each oRow As DataRow In dsSiguienteEtapa.Tables(0).Rows

                                lRolPorWebService = -1

                                For pos As Integer = 0 To UBound(sBloqueRolPorWebService)

                                    sArrRolPorWebService = Split(sBloqueRolPorWebService(pos), "@")

                                    If sArrRolPorWebService(0) = CStr(oRow.Item("BLOQUE")) Then
                                        lRolPorWebService = CLng(sArrRolPorWebService(1))
                                        Exit For
                                    End If
                                Next

                                If lRolPorWebService > -1 Then
                                    bValidacion = Instancia_LlamadaWebService(lRolPorWebService, oInstancia.ID, oRow.Item("BLOQUE"), oDTPMParticipantes)
                                Else 'Los participantes se deciden en pantalla.  Paralelo: 1 por web y 1 ahora/campo/pestaÃ±a/etc
                                    bValidacion = oInstancia.ComprobarParticipantes(oUsuario.Idioma, oUsuario.CodPersona, lBloque)
                                End If

                                If Not bValidacion Then Exit For
                            Next

                            If bValidacion Then                                
                                'Comprobar si hay algún rol asignable directamente (si tiene sólo una persona participante)
                                'Obtenemos los distintos roles
                                Dim dvParticipantes As New DataView(oDTPMParticipantes)
                                Dim dtRoles As DataTable = dvParticipantes.ToTable(True, "ROL")
                                'Se mira si para cada rol hay una única persona
                                For Each drRol As DataRow In dtRoles.Rows
                                    'Primero se limpia una posible asignación hecha durante una validación que haya resultado negativa 
                                    Dim oRol As FSNServer.Rol
                                    oRol = FSNServer.Get_Object(GetType(FSNServer.Rol))
                                    oRol.Id = drRol("ROL")
                                    oRol.IdInstancia = oInstancia.ID
                                    oRol.AsignarRol(Nothing)

                                    Dim drPersonas As DataRow() = oDTPMParticipantes.Select("ROL=" & drRol("ROL"))
                                    If drPersonas.Count = 1 Then
                                        'Si sólo hay una persona para ese rol se asigna directamente
                                        oRol.AsignarRol(drPersonas(0)("PER"))
                                    End If
                                Next

                                'El resultado de dicha llamada se guardarÃ¡ en PM_COPIA_PARTICIPANTES y se pasarÃ¡ adelante con las demÃ¡s validaciones. 
                                oInstancia.Actualizar_ParticipantesExternos(oDTPMParticipantes)
                            End If

                        Else 'Los participantes se deciden en pantalla
                            bValidacion = oInstancia.ComprobarParticipantes(oUsuario.Idioma, oUsuario.CodPersona, lBloque)
                        End If
                    End If
                    ' Comprobar Campos Obligatorios
                    If bValidacion AndAlso oAccion.CumpOblRol Then
                        oInstancia.CargarCamposDesglose(oUsuario.Idioma, oUsuario.CodPersona)
                        oInstancia.ActualizarEstadoValidacion(EstadoValidacion.CamposObligatorios, Nothing)
                        bValidacion = oInstancia.ComprobarCamposObligatorios(oUsuario.Idioma, oUsuario.CodPersona)
                    End If
                    ' Comprobar Precondiciones
                    If bValidacion Then
                        oInstancia.ActualizarEstadoValidacion(EstadoValidacion.Precondiciones, Nothing)
                        bValidacion = oInstancia.ComprobarPrecondiciones(lAccion, oUsuario.Idioma)
                    End If
                    ' Comprobar Bloqueos Solicitudes Padre
                    If bValidacion Then
                        oInstancia.ActualizarEstadoValidacion(EstadoValidacion.ControlImportes, Nothing)
                        bValidacion = oInstancia.ComprobarBloqueosSolicitudesPadre(oUsuario.Idioma, oUsuario.NumberFormat)
                    End If
                    ' Comprobar Validaciones Integración
                    If bValidacion Then
                        oInstancia.ActualizarEstadoValidacion(EstadoValidacion.Integracion, Nothing)
                        bValidacion = oInstancia.ComprobarMapper(oUsuario.Idioma, oUsuario.CodPersona, lAccion, dsSiguienteEtapa)
                    End If
                    If bValidacion Then
                        oInstancia.ActualizarEstadoValidacion(EstadoValidacion.Completada, lAccion & "," & lBloque & "," & oUsuario.Cod)
                        oInstancia.Actualizar_En_proceso(1, lBloque)
                        bEjecutarAcciones = True
                    Else
                        oInstancia.Actualizar_En_proceso(0, lBloque)
                        oInstancia.Actualizar_En_proceso(0)
                        BorrarXML(oInstancia.ID, lBloque, sUser)
                        Try
                            oInstancia.NotificarErrorValidacion(oUsuario)
                        Catch ex As Exception
                        End Try
                    End If
                    oInstancia = Nothing
                Catch ex As Exception
                    If Not oInstancia Is Nothing Then
                        ' Error general
                        oInstancia.ActualizarEstadoValidacion(EstadoValidacion.ErrorGeneral, "Estado: " & oInstancia.EstadoValidacion & " - Error: " & ex.Message)
                        oInstancia.Actualizar_En_proceso(0, lBloque)
                        oInstancia.Actualizar_En_proceso(0)
                        BorrarXML(oInstancia.ID, lBloque, oUsuario.Cod)
                    End If
                End Try
            Next
            If bEjecutarAcciones Then EjecutarAcciones()
            oMutexVal.ReleaseMutex()
        Catch ex As Exception
            oMutexVal.ReleaseMutex()
        End Try
        ServiceThread()
    End Sub
    ''' <summary>
    ''' Las instancias validadas pasan de XMLTemporal a rutaXML y se cambia el estado de validación.
    ''' </summary>
    ''' <remarks>Llamada desde: RealizarValidacionesThread; Tiempo máximo: 1 sg</remarks>
    Private Sub EjecutarAcciones()
        Dim oInstancias As Instancias
        oInstancias = FSNServer.Get_Object(GetType(FSNServer.Instancias))
        Dim ds As DataSet = oInstancias.InstanciasValidadas()
        For Each dr As DataRow In ds.Tables(0).Rows
            Dim i As Integer = InStr(dr.Item("ERROR_VALIDACION"), ",")
            Dim lAccion As Long = Left(dr.Item("ERROR_VALIDACION"), i - 1)
            Dim j As Integer = InStr(i + 1, dr.Item("ERROR_VALIDACION"), ",")
            Dim lBloque As Long = Mid(dr.Item("ERROR_VALIDACION"), i + 1, j - i - 1)
            Dim sCodUsu As String = Right(dr.Item("ERROR_VALIDACION"), CType(dr.Item("ERROR_VALIDACION"), String).Length - j)
            Dim XMLName As String = sCodUsu & "#" & dr.Item("ID") & "#" & lBloque.ToString() & "#1#V#.xml"
            If Not File.Exists(ConfigurationManager.AppSettings("rutaXMLService") & "\" & XMLName) AndAlso File.Exists(ConfigurationManager.AppSettings("rutaXMLTemporal") & "\" & XMLName) Then
                File.Copy(ConfigurationManager.AppSettings("rutaXMLTemporal") & "\" & XMLName, ConfigurationManager.AppSettings("rutaXMLService") & "\" & XMLName)
                File.Delete(ConfigurationManager.AppSettings("rutaXMLTemporal") & "\" & XMLName)
            End If
            Dim oInstancia As Instancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
            oInstancia.ID = dr.Item("ID")
            oInstancia.ActualizarEstadoValidacion(EstadoValidacion.SinEstado, Nothing)
        Next
    End Sub
    ''' <summary>
    ''' Procedimiento que se encarga de recorrer la carpeta del webservice y procesar los xml de solicitudes de PM, aprobaciones múltiples y QA
    ''' </summary>
    ''' <remarks>Llamada desde ProcesarXML; tiempo máximo: 10 seg</remarks>
    Private Sub ServiceThread()
        Dim oXMLFile As FileInfo
        Dim ListaNombres As String()
        Dim ListaFechas As String()
        Dim Fichero As String
        Dim Fecha As String
        Dim IndiceLista As Integer
        Dim oFileW As System.IO.StreamWriter

        Try
            oMutex.WaitOne(-1, True)

            'Una vez que se entra en el MUTEX se procesan todos los XML
            m_oXMLFolder = New DirectoryInfo(ConfigurationManager.AppSettings("rutaXMLService"))

            IndiceLista = 0

            For Each oXMLFile In m_oXMLFolder.GetFiles("*.xml")
                If oXMLFile.Exists Then
                    ReDim Preserve ListaNombres(IndiceLista)
                    ReDim Preserve ListaFechas(IndiceLista)

                    ListaNombres(IndiceLista) = oXMLFile.Name
                    ListaFechas(IndiceLista) = CStr(oXMLFile.CreationTime)

                    IndiceLista = IndiceLista + 1
                End If
            Next

            For j As Integer = 0 To IndiceLista - 2
                For k As Integer = j + 1 To IndiceLista - 1
                    If CDate(ListaFechas(j)) > CDate(ListaFechas(k)) Then
                        Fichero = ListaNombres(j)
                        ListaNombres(j) = ListaNombres(k)
                        ListaNombres(k) = Fichero

                        Fecha = ListaFechas(j)
                        ListaFechas(j) = ListaFechas(k)
                        ListaFechas(k) = Fecha
                    End If
                Next
            Next
            If Not ListaNombres Is Nothing Then
                ''ThreadPool.SetMaxThreads(iMaxThreads, iMaxThreads)
                For Each Fichero In ListaNombres
                    Try
                        If Not (m_oXMLFolder.GetFiles(Fichero).Length = 0) Then
                            TratarXML(Fichero)
                            ''ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf TratarXML), Fichero)
                        End If
                    Catch ex As Exception
                        If m_bPortal Then
                            oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & m_sFile & m_sProveGS & ".txt", True)
                        Else
                            oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & m_sFile & m_sPer & ".txt", True)
                        End If

                        oFileW.WriteLine()
                        oFileW.WriteLine("Error ServiceThread For: " & ex.Message & ". Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
                        oFileW.Close()

                        'En caso de que un fichero produzca un error mover ese fichero a la carpeta de ERROR
                        File.Copy(ConfigurationManager.AppSettings("rutaXMLService") & Fichero, ConfigurationManager.AppSettings("rutaXMLerror") & Fichero, True)
                        File.Delete(ConfigurationManager.AppSettings("rutaXMLService") & Fichero)
                    End Try
                Next
            End If

            oMutex.ReleaseMutex()

            Exit Sub

        Catch ex As Exception

            strMensajeError = ex.Message

            'Liberar el Mutex
            oMutex.ReleaseMutex()
            If m_bPortal Then
                oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & m_sFile & m_sProveGS & ".txt", True)
            Else
                oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & m_sFile & m_sPer & ".txt", True)
            End If

            oFileW.WriteLine()
            oFileW.WriteLine("Error ServiceThread: " & ex.Message & ". Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
            oFileW.Close()

            'En caso de que un fichero produzca un error mover ese fichero a la carpeta de ERROR
            If Fichero <> Nothing AndAlso File.Exists(ConfigurationManager.AppSettings("rutaXMLService") & "\" & Fichero) Then
                File.Copy(ConfigurationManager.AppSettings("rutaXMLService") & Fichero, ConfigurationManager.AppSettings("rutaXMLerror") & Fichero, True)
                File.Delete(ConfigurationManager.AppSettings("rutaXMLService") & Fichero)
            End If
        End Try

    End Sub
    ''' <summary>
    ''' Procedimiento que se encarga de procesar el xml pasado como parámetro
    ''' </summary>
    ''' <param name="obj">Nombre del fichero</param>
    ''' <remarks>Llamada desde ServiceThread; Tiempo máximo: 10 seg</remarks>
    Private Sub TratarXML(ByVal obj As Object)
        Dim sFichero As String = CType(obj, String)
        Dim arFichero As String() = sFichero.Split("#")
        Dim sStoredEnEjecucion As String = ""

        If Not (UBound(arFichero) > 4 AndAlso arFichero(4) = "V") Then
            If EstaEnTratamiento(sFichero) Then Exit Sub

            MeterEnTratamiento(sFichero)
        Else 'los xmls de aprobaciÃ³n mÃºltiple llevan el elemento "EN_TRATAMIENTO" como texto en la 1ra linea
            If EstaEnTratamiento(sFichero, True) Then Exit Sub

            MeterEnTratamiento(sFichero, True)
        End If

        Dim dsRoles As DataSet = Nothing
        Dim sAccion As String = ""
        Dim sTrasladoUsuario As String = ""
        Dim sTrasladoProveedor As String = ""
        Dim oTrasladoFecha As Object = Nothing
        Dim sTrasladoComentario As String = ""
        Dim sTrasladoProveedorContacto As String = ""
        Dim sDevolucionComentario As String = ""
        Dim sComentario As String = ""
        Dim sBloqueOrigen As String = ""
        Dim sNuevoIdInstancia As String = ""
        Dim oInstancia As FSNServer.Instancia
        Dim oread As StreamReader
        Dim sIdi As String = ""
        Dim iAccion As Integer = 0
        Dim lNoConformidad As Long = 0
        Dim lCertificado As Long = 0
        Dim lContrato As Long = 0
        Dim lFactura As Long = 0
        Dim lSolicitudDePedido As Byte = 0
        Dim iTipoSolicitud As TiposDeDatos.TipoDeSolicitud
        Dim idArchivoContrato As Long = 0
        Dim lIDTiempoProc As Long = 0

        m_bPortal = False

        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))

        'Actualizamos el tiempo de procesamiento
        If UBound(arFichero) > 4 AndAlso arFichero(4) = "V" Then 'xml de aprobación múltiple
            oread = File.OpenText(ConfigurationManager.AppSettings("rutaXMLService") & "\" & sFichero)
            oread.ReadLine() 'En_Tratamiento
            lIDTiempoProc = CType(oread.ReadLine(), Long)
        Else 'resto
            lIDTiempoProc = ObtenerIdTiempoProcesamiento(sFichero)
        End If

        If lIDTiempoProc <> Nothing Then oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, , TiempoProcesamiento.InicioBD)

        If arFichero.Length > 5 AndAlso arFichero(5) = "P" Then 'desde PORTAL PM
            Dim ds As DataSet = New DataSet
            Dim arrDatosSolicitud(26) As Object
            Dim lInstancia As Long
            Dim lCiaComp As Long
            Dim sCodProve As String
            Dim sCodCia As String
            Dim sDenCia As String
            Dim sNIFCia As String
            Dim lIdCia As Long
            Dim lIdUsu As Long
            Dim sNombre As String
            Dim sApellidos As String
            Dim sTelefono As String
            Dim sEmail As String
            Dim sFax As String
            Dim dtotal As Double
            Dim sGuardar As String
            Dim sBloquesDestino As String
            Dim bNotificar As Boolean
            Dim sNotificar As String

            m_bPortal = True
            m_sFile = "\PM_P_log_"
            m_sProveGS = ""

            'Cargar el datset con el XML
            ds.ReadXml(ConfigurationManager.AppSettings("rutaXMLService") & "\" & sFichero)
            dsRoles = New DataSet
            If Not ds.Tables("ROLES") Is Nothing Then
                dsRoles.Tables.Add(ds.Tables("ROLES").Copy)
            End If

            If Not ds.Tables("TEMPADJUN") Is Nothing Then
                Dim dr() As System.Data.DataRow
                dr = ds.Tables("TEMPADJUN").Select("TIPO=5")
                If dr.Count > 0 Then
                    Dim arrDatosAdjuntos(0) As Object
                    arrDatosAdjuntos = ds.Tables("TEMPADJUN").Rows(0).ItemArray
                    idArchivoContrato = arrDatosAdjuntos(2)
                End If
            End If

            'Obtener los datos de la solicitud transportada en el dataset
            arrDatosSolicitud = ds.Tables("Solicitud").Rows(0).ItemArray

            lInstancia = arrDatosSolicitud(0)
            lCiaComp = arrDatosSolicitud(2)
            sCodProve = arrDatosSolicitud(3)
            sCodCia = arrDatosSolicitud(5)
            sDenCia = arrDatosSolicitud(6)
            sNIFCia = arrDatosSolicitud(7)
            lIdCia = arrDatosSolicitud(8)
            lIdUsu = arrDatosSolicitud(9)
            sNombre = arrDatosSolicitud(10)
            sApellidos = arrDatosSolicitud(11)
            sTelefono = arrDatosSolicitud(12)
            sEmail = arrDatosSolicitud(13)
            sFax = arrDatosSolicitud(14)
            sIdi = arrDatosSolicitud(15)
            m_sProveGS = arrDatosSolicitud(16)
            If Not IsDBNull(arrDatosSolicitud(17)) Then
                dtotal = arrDatosSolicitud(17)
            End If
            sAccion = arrDatosSolicitud(18)
            If arrDatosSolicitud(19) <> Nothing Then
                iAccion = arrDatosSolicitud(19)
            End If
            sGuardar = arrDatosSolicitud(20)
            sDevolucionComentario = arrDatosSolicitud(21)
            sComentario = arrDatosSolicitud(22)
            sBloqueOrigen = arrDatosSolicitud(23)
            sBloquesDestino = arrDatosSolicitud(24)
            sNotificar = arrDatosSolicitud(25)
            bNotificar = (sNotificar = "1")
            lFactura = arrDatosSolicitud(26)

            oInstancia.ID = lInstancia
            'Solo en la devolución o cuando la acción tiene marcado el check guardar
            If sGuardar = "1" Or sAccion = "devolversolicitud" Then
                'NOTA: en el campo idContrato se le pasa la variable lContrato (0/1). En el caso de portal solo se utiliza para grabar los filtros.
                oError = oInstancia.Save(ds, "", False, dtotal, , , , sEmail, bNotificar, True, m_sProveGS, sNombre & " " & sApellidos, _
                                         sIdi, , sStoredEnEjecucion, sIdi, lContrato, idArchivoContrato, lFactura)
                If Not oError Is Nothing Then
                    Dim oErrores As FSNServer.Errores
                    Dim tmpExcepcion As Exception = oError
                    Dim sEx_FullName As String = tmpExcepcion.GetType().FullName
                    Dim sEx_Message As String = sStoredEnEjecucion & ": " & tmpExcepcion.Message
                    Dim sEx_StackTrace As String = tmpExcepcion.StackTrace

                    oErrores = FSNServer.Get_Object(GetType(FSNServer.Errores))
                    oErrores.Create("Instancia.vb", m_sProveGS, sEx_FullName, sEx_Message, sEx_StackTrace, CStr(oInstancia.ID), "")
                    oErrores = Nothing

                    Err.Raise(10000)
                End If
            End If

            If Not lFactura = 0 Then
                iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.Factura
            ElseIf Not lContrato = 0 Then
                iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.Contrato
            Else
                iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras
            End If

            Select Case sAccion
                Case "devolversolicitud", "trasladadadevolvercontrato"
                    oInstancia.Devolucion("", sEmail, sDevolucionComentario, sBloquesDestino, m_sProveGS, sCodCia, sDenCia, sNIFCia, _
                                          sNombre, sApellidos, sTelefono, sEmail, sFax, iTipoSolicitud, True)
                Case "guardarsolicitud"
                Case Else '"realizarAccionPortal"
                    oError = oInstancia.RealizarAccion(iAccion, , sEmail, sComentario, dsRoles, m_sProveGS, sCodCia, sDenCia, sNIFCia, _
                                                       sNombre, sApellidos, sTelefono, sEmail, sFax, iTipoSolicitud, , True, ConfigurationManager.AppSettings("idioma"))
                    If Not oError Is Nothing Then Err.Raise(10000)
            End Select
        ElseIf UBound(arFichero) > 4 AndAlso arFichero(4) = "V" Then
            Dim oUser As FSNServer.User
            m_sFile = "\PM_log_"
            oUser = FSNServer.Get_Object(GetType(FSNServer.User))
            oUser.LoadUserData(arFichero(0))
            m_sPer = oUser.CodPersona
            m_sEmail = oUser.Email
            sIdi = oUser.Idioma
            If String.IsNullOrEmpty(sIdi) Then sIdi = ConfigurationManager.AppSettings("idioma")

            iTipoSolicitud = CType(oread.ReadLine(), TiposDeDatos.TipoDeSolicitud)
            iAccion = CType(oread.ReadLine(), Integer)
            sComentario = CType(oread.ReadToEnd(), String)
            If Trim(sComentario) = "" Then sComentario = Nothing
            oread.Close()
            oread = Nothing
            sNuevoIdInstancia = arFichero(1)
            sBloqueOrigen = arFichero(2)
            oInstancia.ID = CType(arFichero(1), Long)

            oInstancia.Load(sIdi, True)
            oInstancia.DevolverEtapaActual(sIdi, m_sPer)
        ElseIf UBound(arFichero) > 4 AndAlso arFichero(4) = "P" Then 'desde PORTAL
            m_bPortal = True
            m_sFile = "\QA_P_log_"
            m_sProveGS = ""

            'Cargar el datset con el XML
            Dim ds As DataSet = New DataSet
            ds.ReadXml(ConfigurationManager.AppSettings("rutaXMLService") & "\" & sFichero)
            dsRoles = New DataSet
            If Not ds.Tables("ROLES") Is Nothing Then dsRoles.Tables.Add(ds.Tables("ROLES").Copy)

            'Obtener los datos de la solicitud transportada en el dataset
            Dim arrDatosSolicitud(15) As Object
            arrDatosSolicitud = ds.Tables("Solicitud").Rows(0).ItemArray

            Dim lInstancia As Long = arrDatosSolicitud(0)
            lCertificado = arrDatosSolicitud(1)
            lNoConformidad = arrDatosSolicitud(2)
            Dim lSolicitud As Long = arrDatosSolicitud(3)
            Dim lCiaComp As Long = arrDatosSolicitud(4)
            Dim sCodProve As String = arrDatosSolicitud(5)
            Dim sCodCia As String = arrDatosSolicitud(7)
            Dim lIdCia As Long = arrDatosSolicitud(8)
            Dim lIdUsu As Long = arrDatosSolicitud(9)
            sAccion = arrDatosSolicitud(10)
            Dim sNombre As String = arrDatosSolicitud(11)
            Dim sEmail As String = arrDatosSolicitud(12)
            sIdi = arrDatosSolicitud(13)
            m_sProveGS = arrDatosSolicitud(14)
            Dim dtotal As Double
            Dim bNotificar As Boolean
            Dim sNotificar As String = arrDatosSolicitud(15)
            Dim bCertPdteEnviar As Boolean

            bNotificar = (sNotificar = "1")

            oInstancia.ID = lInstancia
            oInstancia.Load(sIdi, True)

            If Not lSolicitud = 0 Then
                'Creamos la instancia. Este metodo lo vamos a tener que cambiar 
                'para que ataque directamente a las tablas de GS.                                
                oError = oInstancia.Create(oDs:=ds, bGuardar:=True, bPedidoAut:=False, lNoconformidad:=lNoConformidad, lCertificado:=lCertificado, bPortal:=True, _
                                           sProveGS:=m_sProveGS, lSolicitud:=lSolicitud, sStoredEnEjecucion:=sStoredEnEjecucion, sIdioma:=sIdi, sUsuNom:=sNombre, _
                                           tipo:=IIf(sAccion = "guardarcertificado", 3, IIf(sAccion = "enviarCertificadoYtodosPdtesdeEnviar", 1, IIf(sAccion = "enviarCertificadosPortal", 1, IIf(sAccion = "guardarCertificadoPdteEnviar", 2, 0)))))
                If Not oError Is Nothing Then
                    Dim oErrores As FSNServer.Errores

                    Dim tmpExcepcion As Exception = oError
                    Dim sEx_FullName As String = tmpExcepcion.GetType().FullName
                    Dim sEx_Message As String = sStoredEnEjecucion & ": " & tmpExcepcion.Message
                    Dim sEx_StackTrace As String = tmpExcepcion.StackTrace

                    oErrores = FSNServer.Get_Object(GetType(FSNServer.Errores))
                    oErrores.Create("Instancia.vb", m_sProveGS, sEx_FullName, sEx_Message, sEx_StackTrace, CStr(oInstancia.ID), "")
                    oErrores = Nothing

                    Err.Raise(10000)
                End If
            Else
                If Not lNoConformidad = 0 OrElse Not lCertificado = 0 Then
                    If sAccion = "guardarCertificadoPdteEnviar" Then
                        bCertPdteEnviar = True
                    Else
                        bCertPdteEnviar = False
                    End If
                    'Se trata de una NoConformidad o un Certificado                                   
                    oError = oInstancia.Save(ds, "", sAccion = "enviarCertificadosPortal" Or sAccion = "enviarCertificadoYtodosPdtesdeEnviar" Or sAccion = "enviarNoConformidadesPortal", dtotal, False, lCertificado, lNoConformidad, sEmail, bNotificar, True, m_sProveGS, sNombre, , , sStoredEnEjecucion, sIdi, , , , , bCertPdteEnviar)
                    If Not oError Is Nothing Then
                        Dim oErrores As FSNServer.Errores

                        Dim tmpExcepcion As Exception = oError
                        Dim sEx_FullName As String = tmpExcepcion.GetType().FullName
                        Dim sEx_Message As String = sStoredEnEjecucion & ": " & tmpExcepcion.Message
                        Dim sEx_StackTrace As String = tmpExcepcion.StackTrace

                        oErrores = FSNServer.Get_Object(GetType(FSNServer.Errores))
                        oErrores.Create("Instancia.vb", m_sProveGS, sEx_FullName, sEx_Message, sEx_StackTrace, CStr(oInstancia.ID), "")
                        oErrores = Nothing

                        Err.Raise(10000)
                    End If
                End If
            End If

            Select Case sAccion
                Case "altaCertificadoPortal", "enviarCertificadosPortal"
                    Dim oCertificado As FSNServer.Certificado
                    oCertificado = FSNServer.Get_Object(GetType(FSNServer.Certificado))

                    With oCertificado
                        .ID = lCertificado
                        .EmailEnvioNotificacion = sEmail
                        .ProveedorEnvioNotificacion = m_sProveGS
                        .NombreEnvioNotificacion = sNombre
                        .IdInstanciaNotificacion = lInstancia

                        .NotificarEnvioCertificadoPortal()
                    End With

                Case "enviarNoConformidadesPortal"
                    Dim oNoConformidad As FSNServer.NoConformidad
                    oNoConformidad = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))

                    With oNoConformidad
                        .ID = lNoConformidad
                        .ProveedorEnvioNotificacion = m_sProveGS
                        .EmailEnvioNotificacion = sEmail
                        .NombreEnvioNotificacion = sNombre
                        .IdInstanciaNotificacion = lInstancia

                        .NotificarEnvioNoConformidadPortal()
                    End With

                Case "enviarCertificadoYtodosPdtesdeEnviar"
                    Dim oCertificado As FSNServer.Certificado

                    oCertificado = FSNServer.Get_Object(GetType(FSNServer.Certificado))
                    With oCertificado
                        .ID = lCertificado
                        .EmailEnvioNotificacion = sEmail
                        .ProveedorEnvioNotificacion = m_sProveGS
                        .NombreEnvioNotificacion = sNombre
                        .IdInstanciaNotificacion = lInstancia
                        .NotificarEnvioCertificadoPortal()
                    End With

                    Dim oCertificados As FSNServer.Certificados
                    Dim dCertificadosPendientes As DataSet

                    oCertificados = FSNServer.Get_Object(GetType(FSNServer.Certificados))

                    dCertificadosPendientes = oCertificados.ObtenerCertPendientesEnviar(lCertificado, m_sProveGS)

                    For Each rowCertificado In dCertificadosPendientes.Tables(0).Rows
                        With oCertificado
                            .ID = rowCertificado.Item("IDCERTIFICADO")
                            .EmailEnvioNotificacion = sEmail
                            .ProveedorEnvioNotificacion = m_sProveGS
                            .NombreEnvioNotificacion = sNombre
                            .IdInstanciaNotificacion = rowCertificado.Item("IDINSTANCIA")
                            .FinalizarCertPendienteEnviar()

                            .NotificarEnvioCertificadoPortal()
                        End With
                    Next
            End Select

            'Comprobar si existen tipos de certificados que le corresponden al proveedor que dependan del certificado para ser solicitados
            'Las acciones guardarcertificado y guardarCertificadoPdteEnviar crean versiones de tipo borrador
            If lCertificado <> 0 And sAccion <> "guardarcertificado" And sAccion <> "guardarCertificadoPdteEnviar" Then
                Dim oCertificado As FSNServer.Certificado = FSNServer.Get_Object(GetType(FSNServer.Certificado))
                With oCertificado
                    .ID = lCertificado
                    .Instancia = lInstancia

                    Dim oThread As New Thread(AddressOf .SolicitudAutomaticaTiposCertificadosCondiciones)
                    oThread.Start()
                End With
            End If
        Else
            'Cargar el dataset con el XML
            Dim ds As DataSet = New DataSet
            ds.ReadXml(ConfigurationManager.AppSettings("rutaXMLService") & "\" & sFichero)
            dsRoles = New DataSet
            If Not ds.Tables("ROLES") Is Nothing Then
                dsRoles.Tables.Add(ds.Tables("ROLES").Copy)
            End If

            If Not ds.Tables("TEMPADJUN") Is Nothing Then
                Dim dr() As System.Data.DataRow
                dr = ds.Tables("TEMPADJUN").Select("TIPO=5")
                If dr.Count > 0 Then
                    Dim arrDatosAdjuntos(0) As Object
                    arrDatosAdjuntos = ds.Tables("TEMPADJUN").Rows(0).ItemArray
                    idArchivoContrato = arrDatosAdjuntos(2)
                End If
            End If

            'Obtener los datos de la solicitud transportada en el dataset
            Dim arrDatosSolicitud(30) As Object
            arrDatosSolicitud = ds.Tables("Solicitud").Rows(0).ItemArray

            Dim sInstancia As String = arrDatosSolicitud(0)
            Dim sSolicitud As String = arrDatosSolicitud(1)
            m_sPer = arrDatosSolicitud(2)
            m_sEmail = IIf(IsDBNull(arrDatosSolicitud(3)), "", arrDatosSolicitud(3))
            sIdi = arrDatosSolicitud(4)
            If sIdi = Nothing Then sIdi = ConfigurationManager.AppSettings("idioma")
            Dim sPedidoDirecto As String = arrDatosSolicitud(5)
            Dim sFormulario As String = arrDatosSolicitud(6)
            'En las NoConformidades /Certificados esta variable se utiliza para saber si es un alta o un guardado
            Dim sWorkflow As String = arrDatosSolicitud(7)
            Dim dTotal As Double = arrDatosSolicitud(8)
            Dim sComentAltaNoConf As String = arrDatosSolicitud(9)
            sAccion = arrDatosSolicitud(10)
            Dim sIdAccion As String = arrDatosSolicitud(11)
            Dim sIdAccionForm As String = arrDatosSolicitud(12)

            'sGuardar en NoConformidades/Certificados = Enviar !!!!!
            Dim sGuardar As String = arrDatosSolicitud(13)
            Dim sNotificar As String = arrDatosSolicitud(14)
            sTrasladoUsuario = arrDatosSolicitud(15)
            sTrasladoProveedor = arrDatosSolicitud(16)
            oTrasladoFecha = arrDatosSolicitud(17)
            sTrasladoComentario = arrDatosSolicitud(18)
            sTrasladoProveedorContacto = arrDatosSolicitud(19)
            sDevolucionComentario = arrDatosSolicitud(20)
            sComentario = arrDatosSolicitud(21)
            sBloqueOrigen = arrDatosSolicitud(22)
            sNuevoIdInstancia = arrDatosSolicitud(23)
            Dim sBloquesDestino As String = arrDatosSolicitud(24)
            Dim RolActual As Long = strToLong(arrDatosSolicitud(25).ToString)

            'Procesar el XML
            lNoConformidad = arrDatosSolicitud(26)
            lCertificado = arrDatosSolicitud(27)
            lContrato = arrDatosSolicitud(28)
            lFactura = arrDatosSolicitud(29)
            lSolicitudDePedido = arrDatosSolicitud(30)

            Dim iRet As Integer
            Dim bPedidoDirecto As Boolean = False
            If Not String.IsNullOrEmpty(sPedidoDirecto) Then bPedidoDirecto = (sPedidoDirecto = 1)

            With oInstancia
                .ID = Val(sInstancia)
                .Peticionario = m_sPer
                .IdFormulario = strToLong(sFormulario)
                .IdWorkflow = strToLong(sWorkflow)
                .Importe = dTotal
                .PedidoDirecto = strToDbl(sPedidoDirecto)
                'oInstancia.Load(sIdi, True) 'XXXXXXXXXXXXXXXXXXXXXXXX MIRAR SI ES NECESARIO ANTES DE QUITAR
            End With

            If Not lFactura = 0 Then
                iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.Factura
            ElseIf Not lContrato = 0 Then
                iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.Contrato
            ElseIf Not lCertificado = 0 Then
                iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.Certificado
            ElseIf Not lNoConformidad = 0 Then
                iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad
            ElseIf Not lSolicitudDePedido = 0 Then
                iTipoSolicitud = lSolicitudDePedido '10 (catalogo) o 11 (contraabierto)
            Else
                iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras
            End If

            Dim iAccionForm As Integer
            iAccion = Val(sIdAccion)
            iAccionForm = Val(sIdAccionForm)

            If iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad OrElse iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.Certificado Then
                'Se trata de una NoConformidad o un Certificado
                m_sFile = "\QA_log_"

                If sWorkflow = "1" Then
                    oError = oInstancia.Create(oDs:=ds, bGuardar:=(sGuardar AndAlso Not sAccion = "emitirsolicitud"), bPedidoAut:=False, _
                                               sComentario:=sComentAltaNoConf, lNoconformidad:=lNoConformidad, lCertificado:=lCertificado, _
                                               sStoredEnEjecucion:=sStoredEnEjecucion, sIdioma:=sIdi)

                    If Not oError Is Nothing Then
                        Dim oErrores As FSNServer.Errores
                        Dim tmpExcepcion As Exception = oError
                        Dim sEx_FullName As String = tmpExcepcion.GetType().FullName
                        Dim sEx_Message As String = sStoredEnEjecucion & ": " & tmpExcepcion.Message
                        Dim sEx_StackTrace As String = tmpExcepcion.StackTrace

                        oErrores = FSNServer.Get_Object(GetType(FSNServer.Errores))
                        oErrores.Create("Instancia.vb", arFichero(0), sEx_FullName, sEx_Message, sEx_StackTrace, CStr(oInstancia.ID), "")
                        oErrores = Nothing

                        Err.Raise(10000)
                    Else
                        'Turno de las notificaciones. Controlaremos desde aqui si tenemos que realizar notificaciones o no.
                        'Hemos creado la instancia, pero si la instancia se guarda y no se emite, no hay que notificar nada.
                        'Si se emite, realizaremos las notificaciones
                        If iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.Certificado Then
                            'Si la instancia no pasa por el workflow entonces los certificados se han emitido.Se envía el email a cada proveedor:
                            If oInstancia.Estado = TipoEstadoSolic.CertificadoPub Then
                                Dim oCertificado As FSNServer.Certificado = FSNServer.Get_Object(GetType(FSNServer.Certificado))
                                With oCertificado
                                    .ID = lCertificado
                                    .EmailEnvioNotificacion = m_sEmail
                                    .NotificarEnvioCertificadoQA()
                                End With
                            End If
                        End If

                        If iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad Then
                            If sGuardar = "1" Then
                                Dim oNoConformidad As FSNServer.NoConformidad
                                oNoConformidad = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))

                                With oNoConformidad
                                    .ID = lNoConformidad
                                    .IdInstanciaNotificacion = oInstancia.ID
                                    .EmailEnvioNotificacion = m_sEmail
                                    .TipoNotificacion = 1
                                    'Llamamos a método de las notificaciones 
                                    .NotificarNoConformidadQA()
                                End With
                            End If
                        End If
                    End If
                Else
                    If sGuardar = "1" Then   'si la solicitud ha sido despublicada no se podrá emitir.
                        If oInstancia.Estado = TipoEstadoSolic.Guardada Then
                            oInstancia.Solicitud.Load(sIdi)
                            If Not oInstancia.Solicitud.Pub Then iRet = -1
                        End If
                    End If

                    If Not iRet = -1 Then
                        Dim bNotificar As Boolean
                        bNotificar = (sNotificar = "1")
                        oError = oInstancia.Save(ds, m_sPer, sGuardar AndAlso Not sAccion = "emitirsolicitud", dTotal, False, _
                                                 lCertificado, lNoConformidad, m_sEmail, bNotificar, sComentario:=sComentAltaNoConf, _
                                                 sStoredEnEjecucion:=sStoredEnEjecucion, sIdioma:=sIdi)

                        If Not oError Is Nothing Then
                            Dim oErrores As FSNServer.Errores
                            Dim tmpExcepcion As Exception = oError
                            Dim sEx_FullName As String = tmpExcepcion.GetType().FullName
                            Dim sEx_Message As String = sStoredEnEjecucion & ": " & tmpExcepcion.Message
                            Dim sEx_StackTrace As String = tmpExcepcion.StackTrace

                            oErrores = FSNServer.Get_Object(GetType(FSNServer.Errores))
                            oErrores.Create("Instancia.vb", arFichero(0), sEx_FullName, sEx_Message, sEx_StackTrace, CStr(oInstancia.ID), "")
                            oErrores = Nothing

                            Err.Raise(10000)
                        Else
                            Dim oNoConformidad As FSNServer.NoConformidad
                            oNoConformidad = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))

                            With oNoConformidad
                                .ID = lNoConformidad
                                .IdInstanciaNotificacion = oInstancia.ID
                                If oInstancia.Estado = TipoEstadoSolic.NoConformidadEnviada Then
                                    .TipoNotificacion = IIf(sGuardar = "1", 1, IIf(bNotificar, 2, 0))
                                    .EmailEnvioNotificacion = m_sEmail
                                    .NotificarNoConformidadQA()
                                End If
                            End With
                        End If
                    End If
                End If

                'Comprobar si existen tipos de certificados que le corresponden al proveedor que dependan del certificado para ser solicitados
                'Desde aquÃƒÂ­ no se crean versiones de tipo borrador
                If lCertificado <> 0 Then
                    Dim oCertificado As FSNServer.Certificado = FSNServer.Get_Object(GetType(FSNServer.Certificado))
                    With oCertificado
                        .ID = lCertificado
                        .Instancia = oInstancia.ID

                        Dim oThread As New Thread(AddressOf .SolicitudAutomaticaTiposCertificadosCondiciones)
                        oThread.Start()
                    End With
                End If

            ElseIf iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.Contrato Then
                m_sFile = "\PM_log_"
                If sGuardar = 1 OrElse sAccion = "guardarcontrato" OrElse String.IsNullOrEmpty(sSolicitud) Then
                    If Not String.IsNullOrEmpty(sSolicitud) Then
                        'HEMOS ENTRADO DESDE NWALTA
                        iAccion = iAccionForm
                        FSNServer.Impersonate()
                        oError = oInstancia.Create(oDs:=ds, bGuardar:=Val(sGuardar), bPedidoAut:=bPedidoDirecto, sComentario:=sComentAltaNoConf, _
                                                   lIdContrato:=lContrato, sStoredEnEjecucion:=sStoredEnEjecucion, sIdioma:=sIdi, lIdArchivoContrato:=idArchivoContrato)
                        If Not oError Is Nothing Then
                            Dim oErrores As FSNServer.Errores
                            Dim tmpExcepcion As Exception = oError
                            Dim sEx_FullName As String = tmpExcepcion.GetType().FullName
                            Dim sEx_Message As String = sStoredEnEjecucion & ": " & tmpExcepcion.Message
                            Dim sEx_StackTrace As String = tmpExcepcion.StackTrace

                            oErrores = FSNServer.Get_Object(GetType(FSNServer.Errores))
                            oErrores.Create("Instancia.vb", arFichero(0), sEx_FullName, sEx_Message, sEx_StackTrace, CStr(oInstancia.ID), "")
                            oErrores = Nothing

                            Err.Raise(10000)
                        End If
                    Else
                        'si la solicitud ha sido despublicada no se podrá emitir.
                        If Not sAccion = "guardarcontrato" AndAlso oInstancia.Estado = TipoEstadoSolic.Guardada Then
                            oInstancia.Load(sIdi, True)
                            If Not oInstancia.Solicitud.Pub Then iRet = -1
                        End If

                        Dim bNotificar As Boolean = False
                        If Not String.IsNullOrEmpty(sNotificar) Then bNotificar = Val(sNotificar)

                        If Not iRet = -1 Then
                            oInstancia.DevolverEtapaActual(sIdi, IIf(lSolicitudDePedido > 0, Nothing, m_sPer))
                            oError = oInstancia.Save(ds, m_sPer, False, oInstancia.Importe, , lCertificado, lNoConformidad, _
                                                     m_sEmail, bNotificar, , , , , , sStoredEnEjecucion, sIdi, lContrato, _
                                                     idArchivoContrato, lFactura, lSolicitudDePedido)
                            If Not oError Is Nothing Then
                                Dim oErrores As FSNServer.Errores
                                Dim tmpExcepcion As Exception = oError
                                Dim sEx_FullName As String = tmpExcepcion.GetType().FullName
                                Dim sEx_Message As String = sStoredEnEjecucion & ": " & tmpExcepcion.Message
                                Dim sEx_StackTrace As String = tmpExcepcion.StackTrace

                                oErrores = FSNServer.Get_Object(GetType(FSNServer.Errores))
                                oErrores.Create("Instancia.vb", arFichero(0), sEx_FullName, sEx_Message, sEx_StackTrace, CStr(oInstancia.ID), "")
                                oErrores = Nothing

                                Err.Raise(10000)
                            End If
                        End If
                    End If
                End If
            Else
                m_sFile = "\PM_log_"
                If sGuardar = 1 OrElse sAccion = "guardarsolicitud" OrElse Not String.IsNullOrEmpty(sSolicitud) Then
                    If Not String.IsNullOrEmpty(sSolicitud) Then
                        'HEMOS ENTRADO DESDE NWALTA
                        iAccion = iAccionForm
                        oError = oInstancia.Create(oDs:=ds, bGuardar:=Val(sGuardar), bPedidoAut:=bPedidoDirecto, sComentario:=sComentAltaNoConf, _
                                                   sStoredEnEjecucion:=sStoredEnEjecucion, sIdioma:=sIdi, tipoSolicitud:=iTipoSolicitud, Rol:=RolActual)
                        If Not oError Is Nothing Then
                            Dim oErrores As FSNServer.Errores
                            Dim tmpExcepcion As Exception = oError
                            Dim sEx_FullName As String = tmpExcepcion.GetType().FullName
                            Dim sEx_Message As String = sStoredEnEjecucion & ": " & tmpExcepcion.Message
                            Dim sEx_StackTrace As String = tmpExcepcion.StackTrace

                            oErrores = FSNServer.Get_Object(GetType(FSNServer.Errores))
                            oErrores.Create("Instancia.vb", arFichero(0), sEx_FullName, sEx_Message, sEx_StackTrace, CStr(oInstancia.ID), "")
                            oErrores = Nothing

                            Err.Raise(10000)
                        End If
                    Else
                        'si la solicitud ha sido despublicada no se podrá emitir.
                        If Not sAccion = "guardarsolicitud" AndAlso oInstancia.Estado = TipoEstadoSolic.Guardada Then
                            oInstancia.Load(sIdi, True)
                            If Not oInstancia.Solicitud.Pub Then iRet = -1
                        End If

                        Dim bNotificar As Boolean = False
                        If Not String.IsNullOrEmpty(sNotificar) Then bNotificar = Val(sNotificar)

                        If Not iRet = -1 Then
                            oInstancia.DevolverEtapaActual(sIdi, IIf(lSolicitudDePedido > 0, Nothing, m_sPer))
                            oError = oInstancia.Save(ds, m_sPer, False, oInstancia.Importe, , lCertificado, lNoConformidad, _
                                                     m_sEmail, bNotificar, , , , , , sStoredEnEjecucion, sIdi, , , lFactura, lSolicitudDePedido)
                            If Not oError Is Nothing Then
                                Dim oErrores As FSNServer.Errores
                                Dim tmpExcepcion As Exception = oError
                                Dim sEx_FullName As String = tmpExcepcion.GetType().FullName
                                Dim sEx_Message As String = sStoredEnEjecucion & ": " & tmpExcepcion.Message
                                Dim sEx_StackTrace As String = tmpExcepcion.StackTrace

                                oErrores = FSNServer.Get_Object(GetType(FSNServer.Errores))
                                oErrores.Create("Instancia.vb", arFichero(0), sEx_FullName, sEx_Message, sEx_StackTrace, CStr(oInstancia.ID), "")
                                oErrores = Nothing

                                Err.Raise(10000)
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If Not m_bPortal Then
            Select Case sAccion
                Case "trasladarinstancia", "trasladadatrasladarinstancia", "trasladarcontrato", "trasladadatrasladarcontrato"
                    ''Acciones para los traslados
                    Dim sPerInicialTraslado As String
                    Dim oBloque As FSNServer.Bloque
                    Dim dsEstadoBloque As New DataSet

                    oBloque = FSNServer.Get_Object(GetType(FSNServer.Bloque))
                    dsEstadoBloque = oBloque.Devolver_Persona_Inicial_Traslado(oInstancia.ID, oInstancia.InstanciaBloque)

                    If oInstancia.InstanciaBloqueTrasladada Then
                        sPerInicialTraslado = dsEstadoBloque.Tables(0).Rows(0)("PER")
                    Else
                        sPerInicialTraslado = m_sPer
                    End If

                    If String.IsNullOrEmpty(sTrasladoUsuario) Then 'Traslada a un proveedor:
                        oInstancia.Trasladar(m_sPer, m_sEmail, sTrasladoProveedor, sTrasladoComentario, oTrasladoFecha, True, sTrasladoProveedorContacto, oInstancia.InstanciaBloque, sPerInicialTraslado, iTipoSolicitud, ConfigurationManager.AppSettings("idioma"))
                    Else 'Traslada a un usuario
                        oInstancia.Trasladar(m_sPer, m_sEmail, sTrasladoUsuario, sTrasladoComentario, oTrasladoFecha, False, , oInstancia.InstanciaBloque, sPerInicialTraslado, iTipoSolicitud, ConfigurationManager.AppSettings("idioma"))
                    End If
                Case "trasladadadevolverinstancia", "trasladadadevolvercontrato"
                    ''Caso de devolucion de instancia, contrato
                    oInstancia.DevolverEtapaActual(sIdi, m_sPer)
                    oInstancia.Devolucion(m_sPer, m_sEmail, sDevolucionComentario, oInstancia.InstanciaBloque, , , , , , , , , , iTipoSolicitud)
                Case "guardarsolicitud"
                    oError = oInstancia.RealizarAccion(iAccion, m_sPer, m_sEmail, sComentario, dsRoles, , , , , , , , , , iTipoSolicitud, True, , ConfigurationManager.AppSettings("idioma"))
                Case "guardarcontrato"
                    oError = oInstancia.RealizarAccion(iAccion, m_sPer, m_sEmail, sComentario, dsRoles, , , , , , , , , , iTipoSolicitud, True, , ConfigurationManager.AppSettings("idioma"))
                Case Else 'Accion
                    If lNoConformidad = 0 AndAlso lCertificado = 0 Then
                        oInstancia.NuevoID = Val(sNuevoIdInstancia)

                        oError = oInstancia.RealizarAccion(iAccion, m_sPer, m_sEmail, sComentario, dsRoles, , , , , , , , , , iTipoSolicitud, , , ConfigurationManager.AppSettings("idioma"))
                        If Not oError Is Nothing Then Err.Raise(10000)
                    End If
            End Select
        End If

        If iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoCatalogo OrElse iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoContraAbierto Then
            'Si es una solicitud de pedido miraremos si la orden de entrega esta emitida al proveedor, si es el caso la integraremos
            Dim dsOrden As DataSet = oInstancia.ComprobarEstadoOrdenEntrega
            If Not dsOrden Is Nothing AndAlso dsOrden.Tables.Count > 0 AndAlso dsOrden.Tables(0).Rows.Count > 0 Then
                If dsOrden.Tables(0).Rows(0)("EST") = TipoEstadoOrdenEntrega.EmitidoAlProveedor Then
                    'Esta emitida al proveedor
                    'Sumamos el importe comprometido del SM
                    Dim obDetallePedido As cDetallePedidos = FSNServer.Get_Object(GetType(cDetallePedidos))
                    obDetallePedido.ActualizarImputacion(dsOrden.Tables(0).Rows(0)("ORDEN"))
                    'Llamamos al servicio de integraciÃƒÂ³n WCF.
                    Dim obIntegracion As Integracion = FSNServer.Get_Object(GetType(Integracion))
                    obIntegracion.LlamarFSIS(TablasIntegracion.PED_Aprov, dsOrden.Tables(0).Rows(0)("ORDEN"))
                End If
                Dim oOrden As COrden = FSNServer.Get_Object(GetType(COrden))
                oOrden.ID = dsOrden.Tables(0).Rows(0)("ORDEN")
                oOrden.PedidoId = dsOrden.Tables(0).Rows(0)("PEDIDO")
                Select Case dsOrden.Tables(0).Rows(0)("EST") 'Notificaciones dependiendo del estado de la orden de entrega
                    Case TipoEstadoOrdenEntrega.DenegadoParcialAprob
                        oOrden.NotificarDenegacionParcialOrden(m_sEmail)
                    Case TipoEstadoOrdenEntrega.DenegadoTotalAprobador
                        oOrden.NotificarDenegacionTotalOrden(m_sEmail)
                    Case TipoEstadoOrdenEntrega.EmitidoAlProveedor
                        oOrden.NotificarOrden(m_sEmail, dsOrden.Tables(0).Rows(0)("NOTIFICADO"))
                End Select
            End If
        End If

        'Realizamos el proceso si la accion tiene una llamada externa
        Dim listaXml As List(Of Xml.XmlNode)
        Dim oAccion As FSNServer.Accion

        oAccion = CargarDatosAccion(iAccion)

        If oAccion.NomXmlLlamadaExterna <> "" Then
            Select Case oAccion.LlamadaExterna
                Case 1 'Portafirmas
                    'La acciÃ³n tiene una llamada externa
                    Dim oWs_LlamadaExterna As New PM_LlamadaExterna
                    listaXml = oWs_LlamadaExterna.LeerXmlLlamadaExterna(oAccion.NomXmlLlamadaExterna)
                    'Se comprueba que los parametros y el metodo a llamar son correctos
                    Dim bValidarWSDL As Boolean
                    bValidarWSDL = oWs_LlamadaExterna.LlamadaValidacionParametrosWS(listaXml)
                    If bValidarWSDL Then 'Si la validacion del WSDL es correcta se continua
                        'Descargo los adjuntos que se tienen que enviar
                        Dim listAdjuntos As List(Of String(,)) = DevolverAdjuntos(oInstancia.ID)
                        'Sacamos la etapa actual de la instancia
                        oInstancia.DevolverEtapaActual(sIdi, m_sPer)
                        oWs_LlamadaExterna.LlamadaWSLlamadaExterna(listAdjuntos, listaXml, oAccion.PlantillaSOAP, oAccion.LlamadaExterna, oInstancia, oAccion.NomXmlLlamadaExterna)
                    Else
                        'Se informa del error
                        Err.Raise(10000, , "Error en la Validacion del WSDL de la llamada Externa")
                    End If
            End Select
        End If

        If oAccion.GenerarEFactura Then
            Dim oFactura As Factura = FSNServer.Get_Object(GetType(FSNServer.Factura))
            Dim oCFacturaE As CFacturae = FSNServer.Get_Object(GetType(FSNServer.CFacturae))
            Dim sNumFactura As String

            sNumFactura = oFactura.DevolverNumeroFactura(oInstancia.ID)
            oCFacturaE.crearFacV3_2(lFactura.ToString, ConfigurationManager.AppSettings("temp") & sNumFactura & ".xml", sIdi)
            oFactura.Save_EFactura(sNumFactura & ".xml", ConfigurationManager.AppSettings("temp"), lFactura)
        End If

        ''''Una vez procesada ls solicitud Enviar el XML a la carpeta de BackUp
        Dim sSplitoXMLFile() As String
        Dim sSplitXMLName() As String = sFichero.Split("#")
        Dim oXMLFile2 As FileInfo
        Dim FicheroBackup As String
        Dim bBorrarFichero As Boolean = False

        For Each oXMLFile2 In m_oXMLFolder.GetFiles("*.xml")
            sSplitoXMLFile = oXMLFile2.Name.Split("#")

            If m_bPortal And lNoConformidad = 0 And lCertificado = 0 Then 'PM Portal
                If (sSplitXMLName(0) = sSplitoXMLFile(0)) AndAlso (sSplitXMLName(1) = sSplitoXMLFile(1)) AndAlso (sSplitXMLName(2) = sSplitoXMLFile(2)) AndAlso (sSplitXMLName(3) = sSplitoXMLFile(3)) AndAlso (CLng(sSplitoXMLFile(4)) <= CLng(sSplitXMLName(4))) Then
                    FicheroBackup = sSplitXMLName(0) & "#" & sSplitXMLName(1) & "#" & sSplitXMLName(2) & "#" & sSplitXMLName(3) & "#.xml"
                    bBorrarFichero = True
                End If
            Else
                If (sSplitXMLName(0) = sSplitoXMLFile(0)) And (sSplitXMLName(1) = sSplitoXMLFile(1)) And (sSplitXMLName(2) = sSplitoXMLFile(2)) Then
                    If sSplitoXMLFile(3) = "F" OrElse CLng(sSplitoXMLFile(3)) <= CLng(sSplitXMLName(3)) Then 'En el alta de facturas: 37HA#150039#12338#F#1#.xml
                        FicheroBackup = sSplitXMLName(0) & "#" & sSplitXMLName(1) & "#" & sSplitXMLName(2) & "#.xml"
                        bBorrarFichero = True
                    End If
                End If
            End If

            If bBorrarFichero Then
                File.Copy(ConfigurationManager.AppSettings("rutaXMLService") & sFichero, ConfigurationManager.AppSettings("rutaXMLbackup") & FicheroBackup, True)

                Try
                    File.Delete(ConfigurationManager.AppSettings("rutaXMLService") & sFichero)
                Catch ex As Exception
                    'Antes de este cambio:
                    '- Este error en el delete dejaria en carpeta backup y Error. Y tras meter en 
                    '  carpeta error. Intenta otro delete. Y por lo visto lo consigue.
                    '  ¿Sera lo que a veces pasa en windows de abres fichero, escribes y 
                    '  cierras, pero windows dice que esta piyado?
                    '- Hablando con Asier. Reintentar el delete este tras un tiempo.
                    '3 veces tras 3 segundos
                    Dim ireintentos As Integer = 0
                    Dim bAcabaste As Boolean = False
                    While ireintentos < 3 And (Not bAcabaste)
                        subDelay(3)
                        Try
                            File.Delete(ConfigurationManager.AppSettings("rutaXMLService") & sFichero)
                            bAcabaste = True
                        Catch ex1 As Exception
                            bAcabaste = False
                        End Try
                        ireintentos = ireintentos + 1
                    End While

                    If (Not bAcabaste) Then
                        Dim oFileW As System.IO.StreamWriter
                        oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & m_sFile & m_sPer & ".txt", True)
                        oFileW.WriteLine()
                        oFileW.WriteLine("Error TratarXML: " & ex.Message & ". Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
                        oFileW.Close()

                        File.Copy(ConfigurationManager.AppSettings("rutaXMLService") & sFichero, ConfigurationManager.AppSettings("rutaXMLerror") & sFichero, True)
                    End If
                End Try
                Exit For
            End If
        Next

        'Guardamos la hora de fin de procesamiento
        If lIDTiempoProc <> Nothing Then
            oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, , TiempoProcesamiento.Fin)
        End If
        'Si no hay más XML de esa instancia, se pone en proceso a 0
        If m_oXMLFolder.GetFiles(sSplitXMLName(0) & "#" & sSplitXMLName(1) & "#" & sSplitXMLName(2) & "#*.xml").Length = 0 Then
            If Not m_bPortal OrElse (m_bPortal And lNoConformidad = 0 And lCertificado = 0) Then
                If sBloqueOrigen <> "" Then
                    oInstancia.Actualizar_En_proceso(0, sBloqueOrigen)
                End If
            End If
            oInstancia.Actualizar_En_proceso(0)
        End If

        Dim oIntegracion As FSNServer.Integracion
        'Llamada a FSIS.
        oIntegracion = FSNServer.Get_Object(GetType(FSNServer.Integracion))
        oIntegracion.LlamarFSIS(TablasIntegracion.PM, oInstancia.ID)
    End Sub

    ''' <summary>
    ''' Para saber si la instancia ya ha sido puesta a procesar, se mira si tiene un nodo o un texto EN_TRATAMIENTO
    ''' </summary>
    ''' <param name="sFichero">Fichero con la instancia a procesar</param>
    ''' <param name="AprobacionMultiple">True-> Aprobacion multiple desde Visor solictudes. False-> en otro caso</param>
    ''' <returns>Si ya ha sido puesta a procesar o no</returns>
    ''' <remarks>Llamada desde: TratarXML ; Tiempo mÃ¡ximo: 0</remarks>
    Private Function EstaEnTratamiento(ByVal sFichero As String, Optional ByVal AprobacionMultiple As Boolean = False) As Boolean
        Dim bEnTratamiento As Boolean

        If Not AprobacionMultiple Then
            Dim xDoc As New Xml.XmlDocument
            Dim xMiReader As Xml.XmlNodeReader

            xDoc.Load(ConfigurationManager.AppSettings("rutaXMLService") & sFichero)
            xMiReader = New Xml.XmlNodeReader(xDoc)

            Try
                While xMiReader.Read
                    If xMiReader.NodeType = Xml.XmlNodeType.Element AndAlso xMiReader.Name = "EN_TRATAMIENTO" Then
                        bEnTratamiento = True
                        Exit While
                    End If
                End While
            Catch exX As Xml.XmlException
            Catch ex As Exception
            Finally
                xMiReader.Close()
            End Try
        Else
            Dim stringBuffer As String() = File.ReadAllLines(ConfigurationManager.AppSettings("rutaXMLService") & sFichero)
            If stringBuffer(0) = "EN_TRATAMIENTO" Then
                bEnTratamiento = True
            Else
                bEnTratamiento = False
            End If
        End If
        EstaEnTratamiento = bEnTratamiento
    End Function

    ''' <summary>
    ''' Para indicar q, la instancia ya ha sido puesta a procesar, se mete un nodo o un texto EN_TRATAMIENTO
    ''' </summary>
    ''' <param name="Fichero">Fichero con la instancia a procesar</param>
    ''' <param name="AprobacionMultiple">True-> Aprobacion multiple desde Visor solictudes. False-> en otro caso</param>
    ''' <remarks>Llamada desde: TratarXML ; Tiempo mÃ¡ximo: 0</remarks>
    Private Sub MeterEnTratamiento(ByVal Fichero As String, Optional ByVal AprobacionMultiple As Boolean = False)
        If Not AprobacionMultiple Then
            Dim oXMLDoc As New Xml.XmlDocument
            Dim objNode As System.Xml.XmlNode

            oXMLDoc.Load(ConfigurationManager.AppSettings("rutaXMLService") & Fichero)
            objNode = oXMLDoc.CreateElement("EN_TRATAMIENTO")
            oXMLDoc.DocumentElement.AppendChild(objNode)
            oXMLDoc.Save(ConfigurationManager.AppSettings("rutaXMLService") & Fichero)
        Else
            If File.Exists(ConfigurationManager.AppSettings("rutaXMLService") & "\" & Fichero) Then
                Dim stringBuffer As String() = File.ReadAllLines(ConfigurationManager.AppSettings("rutaXMLService") & "\" & Fichero)

                Dim ofs As StreamWriter = File.CreateText(ConfigurationManager.AppSettings("rutaXMLService") & "\" & Fichero)
                ofs.WriteLine("EN_TRATAMIENTO")
                ofs.WriteLine(stringBuffer(0))
                ofs.WriteLine(stringBuffer(1))
                ofs.WriteLine(stringBuffer(2))

                ofs.Close()
                ofs = Nothing
            End If
        End If
    End Sub

    ''' <summary>
    ''' Se guarda en un XML en XMLTemporal el id del tiempo de procesamiento, la acciÃ³n y el comentario.
    ''' Actualizamos el tiempo de disco.
    ''' </summary>
    ''' <param name="lInstancia">Id de la instancia</param>
    ''' <param name="lAccion">Id de la acciÃ³n</param>
    ''' <param name="lBloque">Id del bloque</param>
    ''' <param name="sComentario">Comentario que ha podido introducir el usuairo</param>
    ''' <param name="sUser">Usuario</param>
    ''' <param name="lIDTiempoProc">Id del tiempo de procesamiento</param>
    ''' <param name="iTipoSolicitud">Tipo de solicitud: factura, contrato, ...</param>
    ''' <remarks></remarks>
    Private Sub GenerarXML(ByVal lInstancia As Long, ByVal lAccion As Long, ByVal lBloque As Long, ByVal sComentario As String, ByVal sUser As String, ByVal lIDTiempoProc As Long, ByVal iTipoSolicitud As TiposDeDatos.TipoDeSolicitud)
        Dim XMLName As String = sUser & "#" & lInstancia & "#" & lBloque.ToString() & "#1#V#.xml"
        If Not File.Exists(ConfigurationManager.AppSettings("rutaXMLTemporal") & "\" & XMLName) Then
            Dim ofs As StreamWriter = File.CreateText(ConfigurationManager.AppSettings("rutaXMLTemporal") & "\" & XMLName)
            ofs.WriteLine(lIDTiempoProc.ToString()) 'En TratarXML leemos 1º el tiempo de procesamiento. Siempre tiene que estar en la primera línea.
            ofs.WriteLine(iTipoSolicitud) 'si pongo ToString(), me pone por ejemplo, Contrato, en vez de 5.
            ofs.WriteLine(lAccion.ToString())
            If sComentario <> "" Then
                ofs.WriteLine(sComentario)
            End If
            ofs.Close()
            ofs = Nothing

            Dim oInstancia As Instancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
            oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, , TiempoProcesamiento.InicioDisco)
        End If
    End Sub

    Private Sub BorrarXML(ByVal lInstancia As Long, ByVal lBloque As Long, ByVal sUser As String)
        Dim XMLName As String = sUser & "#" & lInstancia & "#" & lBloque.ToString() & "#1#V#.xml"
        If File.Exists(ConfigurationManager.AppSettings("rutaXMLTemporal") & "\" & XMLName) Then
            File.Delete(ConfigurationManager.AppSettings("rutaXMLTemporal") & "\" & XMLName)
        End If
    End Sub

    Public Sub subDelay(ByVal sngDelay As Single)
        'Const cSecondsInDay = 1 '86400        ' # of seconds in a day.
        Dim sngStart As DateTime             ' Start time.
        Dim sngStop As DateTime              ' Stop time.
        Dim sngNow As DateTime               ' Current time.

        sngStart = System.DateTime.Now ' Get current timer.
        sngStop = DateAdd(DateInterval.Second, sngDelay, sngStart)    ' Set up stop time based on
        ' delay.
        While sngNow < sngStop
            sngNow = System.DateTime.Now ' Get current timer again.
            'If sngNow < sngStart Then      ' Has midnight passed?
            ' sngStop = sngStart - cSecondsInDay  ' If yes, reset end.
            'End If

        End While        ' Has time elapsed?

    End Sub

    ''' <summary>
    ''' Obtiene del XML el id de procesamiento para actualizar la fecha de inicio de BD y de fin
    ''' </summary>
    ''' <param name="Fichero">Nombre del fichero</param>
    ''' <returns>Devuelve el id de procesamiento</returns>
    ''' <remarks>Llamada desde: ServiceThread; Tiempo máximo: 0, 1 sg.</remarks>
    Private Function ObtenerIdTiempoProcesamiento(ByVal Fichero As String) As Long
        Dim xDoc As New Xml.XmlDocument
        Dim xMiReader As Xml.XmlNodeReader
        Dim lID As Long

        xDoc.Load(ConfigurationManager.AppSettings("rutaXMLService") & Fichero)
        xMiReader = New Xml.XmlNodeReader(xDoc)
        Try
            While xMiReader.Read
                If xMiReader.HasAttributes AndAlso xMiReader.NodeType = Xml.XmlNodeType.Element AndAlso xMiReader.Name = "TIEMPO_PROC" Then
                    Try
                        lID = xMiReader.GetAttribute("ID")
                        Exit While
                    Catch ex As Exception
                    End Try
                End If
            End While
        Catch exX As Xml.XmlException
        Catch ex As Exception
        Finally
            xMiReader.Close()
        End Try
        Return lID
    End Function

    ''' <summary>
    ''' Funcion que carga propiedades de la acción y datos de la llamada externa
    ''' </summary>
    ''' <param name="idAccion"></param>
    ''' <returns>Devuelve el nombre del Xml que contiene la llamada externa</returns>
    ''' <remarks></remarks>
    Private Function CargarDatosAccion(ByVal idAccion As Int32) As FSNServer.Accion
        Dim oAccion As Accion = FSNServer.Get_Object(GetType(FSNServer.Accion))

        oAccion.Id = idAccion
        oAccion.CargarAccion(ConfigurationManager.AppSettings("idioma"))

        Return oAccion

    End Function

    ''' <summary>
    ''' Devuelve los adjuntos a enviar a la llamada externa
    ''' </summary>
    ''' <param name="lInstancia">Instancia de la que obtener los adjuntos</param>
    ''' <returns>Retorna la lista de adjuntos</returns>
    ''' <remarks></remarks>
    Private Function DevolverAdjuntos(ByVal lInstancia As Long) As List(Of String(,))
        Dim oAdjuntos As FSNServer.Adjuntos
        Dim listaAdjuntos As New List(Of String(,))

        Try

            oAdjuntos = FSNServer.Get_Object(GetType(FSNServer.Adjuntos))
            oAdjuntos.LoadAdjuntosPorInstancia(lInstancia)
            Dim dsAdjuntos As Data.DataSet = oAdjuntos.Data
            Dim sAdjuntos As String = String.Empty
            Dim sAdjuntosDesglose As String = String.Empty
            For Each dr As Data.DataRow In dsAdjuntos.Tables(0).Rows
                Select Case dr("TIPO")
                    Case "1"
                        'Adjuntos normales
                        sAdjuntos += dr("ID") & ","
                    Case "3"
                        'Adjuntos de desglose
                        sAdjuntosDesglose += dr("ID") & ","
                End Select
            Next
            'borro la ultima coma
            If sAdjuntos.Length > 0 Then sAdjuntos = sAdjuntos.Remove(sAdjuntos.Length - 1, 1)
            If sAdjuntosDesglose.Length > 0 Then sAdjuntosDesglose = sAdjuntosDesglose.Remove(sAdjuntosDesglose.Length - 1, 1)

            Dim Adjuntos() As String
            Dim AdjuntosDesglose() As String
            Dim sPath As String = ConfigurationManager.AppSettings("temp") + "\" + FSNLibrary.modUtilidades.GenerateRandomPath()

            Adjuntos = Split(sAdjuntos, ",")
            Dim oAdjunto As FSNServer.Adjunto

            oAdjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))
            For i As Short = 0 To Adjuntos.GetUpperBound(0)
                Dim adjun(,) As String = {{"", "", "", ""}}
                If IsNumeric(Adjuntos(i)) Then
                    oAdjunto.Id = Adjuntos(i)
                Else
                    Exit For
                End If

                oAdjunto.LoadInstFromRequest("1")

                'Si es una adjunto metido desde AÃ±adir por valor por defecto. La informaciÃ³n
                'esta en LINEA_DESGLOSE_ADJUN
                If oAdjunto.DataSize = -1000 Then
                    oAdjunto.LoadFromRequest("1")
                    oAdjunto.SaveAdjunToDiskZip("1", sPath)
                    adjun(0, 0) = sPath & "\" & oAdjunto.Nom
                    adjun(0, 1) = oAdjunto.Id
                    adjun(0, 2) = "1"
                    adjun(0, 3) = oAdjunto.Nom
                    listaAdjuntos.Add(adjun)
                Else
                    oAdjunto.SaveAdjunToDiskZip("1", sPath, True)
                    adjun(0, 0) = sPath & "\" & oAdjunto.Nom
                    adjun(0, 1) = oAdjunto.Id
                    adjun(0, 2) = "1"
                    adjun(0, 3) = oAdjunto.Nom
                    listaAdjuntos.Add(adjun)
                End If
            Next

            AdjuntosDesglose = Split(sAdjuntosDesglose, ",")

            For i As Short = 0 To AdjuntosDesglose.GetUpperBound(0)
                Dim adjunDesg(,) As String = {{"", "", "", ""}}
                If IsNumeric(AdjuntosDesglose(i)) Then
                    oAdjunto.Id = AdjuntosDesglose(i)
                Else
                    Exit For
                End If

                oAdjunto.LoadInstFromRequest("3")

                'Si es una adjunto metido desde AÃ±adir por valor por defecto. La informaciÃ³n
                'esta en LINEA_DESGLOSE_ADJUN
                If oAdjunto.DataSize = -1000 Then
                    oAdjunto.LoadFromRequest("3")
                    oAdjunto.SaveAdjunToDiskZip("3", sPath)
                    adjunDesg(0, 0) = sPath & "\" & oAdjunto.Nom
                    adjunDesg(0, 1) = oAdjunto.Id
                    adjunDesg(0, 2) = "3"
                    adjunDesg(0, 3) = oAdjunto.Nom
                    listaAdjuntos.Add(adjunDesg)
                Else
                    oAdjunto.SaveAdjunToDiskZip("3", sPath, True)
                    adjunDesg(0, 0) = sPath & "\" & oAdjunto.Nom
                    adjunDesg(0, 1) = oAdjunto.Id
                    adjunDesg(0, 2) = "3"
                    adjunDesg(0, 3) = oAdjunto.Nom
                    listaAdjuntos.Add(adjunDesg)
                End If
            Next
            Return listaAdjuntos
        Catch ex As Exception

        End Try
    End Function

#Region "Tarea 3400: Asignación de roles relacionado con un servicio externo"
    ''' <summary>
    ''' Gestamp tiene 100 etapas a las q ir desde peticionario en varios flujos. Se quitan para q solo sea una etapa y un web 
    ''' service desde integraciÃ³n nos dice cuales son las personas de autoasignaciÃ³n.
    ''' </summary>
    ''' <param name="lRolPorWebService">Rol para 100 etapas.</param>
    ''' <param name="Instancia">Instancia</param>
    ''' <param name="BloqueDestino">esa unica etapa q antes eran 100</param>
    ''' <param name="oDTPMParticipantes">Por si tiene q ir a varias etapas, los participantes cambian de uno a otro, hay q ir guardandolos para updatar mas tarde.</param> 
    ''' <returns>Si ha podido o no devolver una lista de personas separadas por #</returns>
    ''' <remarks>Llamada desde: RealizarAccion_Guardar ; Tiempo maximo: 0,3</remarks>
    Private Function Instancia_LlamadaWebService(ByVal lRolPorWebService As Long, ByVal Instancia As Long, ByVal BloqueDestino As Long _
                                                 , ByRef oDTPMParticipantes As DataTable) As Boolean
        Dim oValorCampo As Object
        oValorCampo = LlamarWSServicio(ConfigurationManager.AppSettings("UrlParticipantesExterno"), "oXMLDocCadena", "", "oXMLDocInstancia", Instancia, "oXMLDocBLoque", BloqueDestino, "DevolverAprobadorResponse")

        'oValorCampo sera una lista de personas separadas por #. Ejemplo: 37HA#C227K#
        Dim dtNewRow As DataRow
        Dim sAux() As String = Split(oValorCampo, "#")

        For i As Integer = 0 To UBound(sAux) - 1 Step 1
            dtNewRow = oDTPMParticipantes.NewRow

            dtNewRow.Item("ID") = i + 1
            dtNewRow.Item("ROL") = lRolPorWebService
            dtNewRow.Item("PER") = sAux(i)

            oDTPMParticipantes.Rows.Add(dtNewRow)
        Next

        Return (oValorCampo <> "")
    End Function

    ''' <summary>
    ''' Ejecuta la llamada a un webservice q no incluimos en el proyecto.
    ''' </summary>
    ''' <param name="sUrl">url del servicio con la funciÃ³n a realizar. Ejemplo:http://localhost/FSNWebService_31900_9/GES_Tallent_WS.asmx/DevolverAprobador </param>
    ''' <param name="nomEntradaXml">Nombre de la variable Fichero q usa el webservice a realizar.</param>
    ''' <param name="valorXml">Para gestamp Tarea 3400, es vacio</param> 
    ''' <param name="nomEntradaInstancia">Nombre de la variable Instancia q usa el webservice a realizar.</param>
    ''' <param name="valorInstancia">Instancia q usa el webservice a realizar.</param>
    ''' <param name="nomEntradaBloque">Nombre de la variable "Bloque destino q usa el webservice a realizar"</param>
    ''' <param name="valorBloque">Bloque q usa el webservice a realizar</param>  
    ''' <param name="nomResult">Nombre de la variable del webservice donde se deja el resultado</param>
    ''' <returns>Lo q responde el webservice</returns>
    ''' <remarks>Llamada desde: ComprobarCondiciones    Instancia_LlamadaWebService ; Tiempo mÃƒÂ¡ximo: 0,3</remarks>
    Private Function LlamarWSServicio(ByVal sUrl As String, ByVal nomEntradaXml As String, ByVal valorXml As String _
                                      , ByVal nomEntradaInstancia As String, ByVal valorInstancia As String _
                                      , ByVal nomEntradaBloque As String, ByVal valorBloque As String, ByVal nomResult As String) As String
        Dim valorResult As String = ""
        Dim url, metodo, sSoapAction, sXml As String
        Dim num As Integer

        num = sUrl.IndexOf(".asmx")
        url = sUrl.Substring(0, num + 5)
        metodo = sUrl.Substring(num + 6)
        sSoapAction = "http://tempuri.org/" + metodo

        sXml = "<?xml version=""1.0"" encoding=""utf-8""?>"
        sXml = sXml + "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">"
        sXml = sXml + "<soap:Body>"
        sXml = sXml + "<" + metodo + " xmlns=""http://tempuri.org/"">"
        sXml = sXml + "<" + nomEntradaXml + ">" + valorXml + "</" + nomEntradaXml + ">"
        sXml = sXml + "<" + nomEntradaInstancia + ">" + valorInstancia + "</" + nomEntradaInstancia + ">"
        sXml = sXml + "<" + nomEntradaBloque + ">" + valorBloque + "</" + nomEntradaBloque + ">"
        sXml = sXml + "</" + metodo + ">"
        sXml = sXml + "</soap:Body>"
        sXml = sXml + "</soap:Envelope>"

        Dim targetURI As New Uri(url)
        Dim req As System.Net.HttpWebRequest = DirectCast(System.Net.WebRequest.Create(targetURI), System.Net.HttpWebRequest)
        req.Headers.Add("SOAPAction", sSoapAction)
        req.ContentType = "text/xml; charset=""utf-8"""
        req.Accept = "text/xml"
        req.Method = "POST"
        Dim stm As Stream = req.GetRequestStream
        Dim stmw As New StreamWriter(stm)
        stmw.Write(sXml)
        stmw.Close()
        stm.Close()

        'Se realiza la llamada al WS
        Dim response As System.Net.WebResponse
        response = req.GetResponse()
        Dim responseStream As Stream = response.GetResponseStream

        Dim sr As New StreamReader(responseStream)
        Dim soapResult As String
        'Recogemos la respuesta del WS
        soapResult = sr.ReadToEnd
        Dim xmlResponse As New System.Xml.XmlDocument
        xmlResponse.LoadXml(soapResult)
        Dim xmlResult As System.Xml.XmlNode
        xmlResult = xmlResponse.GetElementsByTagName(nomResult).Item(0)
        If Not xmlResult Is Nothing Then
            'Recojo el valor del indicador de error
            valorResult = xmlResult.InnerText
        End If
        Return valorResult
    End Function

#End Region
End Class