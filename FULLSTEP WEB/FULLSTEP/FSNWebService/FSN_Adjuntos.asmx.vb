﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.IO

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/FSN_Adjuntos/FSN_Adjuntos")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class FSN_Adjuntos
    Inherits System.Web.Services.WebService

    Dim m_sFile As String = "\FSGS_log"

    'Función común que sirve para guardar el archivo que se va almacenar en BD
    <WebMethod(EnableSession:=True)>
    Public Function GrabarCarpetaTemporal(ByVal Adjunto As Byte(), ByVal Nombre As String, ByVal RutaTemporal As String) As String
        Dim rutaTemp As String
        If RutaTemporal <> "" Then
            rutaTemp = RutaTemporal
        Else '1ª vez
            rutaTemp = ConfigurationManager.AppSettings("temp") & "\" & modUtilidades.GenerateRandomPath()
        End If
        If Not Directory.Exists(rutaTemp) Then
            Directory.CreateDirectory(rutaTemp)
        End If

        'Guardo el fichero en la carpeta temporal
        Dim oFileStream As New System.IO.FileStream(rutaTemp & "\" & Nombre, IO.FileMode.Append)
        oFileStream.Write(Adjunto, 0, Adjunto.Length)
        oFileStream.Close()

        Return "1#" & rutaTemp
    End Function

#Region "Especificaciones"

    'Se llama desde GS/CEspecificacion/LeerAdjunto
    <WebMethod(EnableSession:=True)>
    Public Function LeerEspecificacion(ByVal Id As Long, ByVal Tipo As Byte, ByVal Grupo As Long, ByVal Proce As Long, ByVal Gmn1 As String, ByVal Anyo As Long, ByVal ChunkNumber As Long, ByVal ChunkSize As Long) As Byte()
        Try
            Dim FSNServer As New FSNServer.Root(True)
            Dim oEspecificacion As FSNServer.Adjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))
            Return oEspecificacion.LeerEspecificacion(Id, Tipo, Grupo, Proce, Gmn1, Anyo, ChunkNumber, ChunkSize)
        Catch ex As Exception
            Dim oFileW As System.IO.StreamWriter
            oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & m_sFile & ".txt", True)
            oFileW.WriteLine()
            oFileW.WriteLine("Error FSN_Adjuntos.asmx(LeerEspecificacion): " & ex.Message & ". Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
            oFileW.Close()
            Return Nothing
        End Try
    End Function

    'Se llama desde GS/CEspecificacion/GrabarAdjunto
    <WebMethod(EnableSession:=True)>
    Public Function GrabarEspecificacion(ByVal RutaTemporal As String, ByVal Nombre As String, ByVal Id As Long, ByVal Grupo As Long, ByVal Coment As String, ByVal DataSize As Long, ByVal Tipo As Long, ByVal Proce As Long, ByVal Gmn1 As String, ByVal Anyo As Long) As String
        Dim FSNServer As New FSNServer.Root(True)
        Dim oEspecificacion As FSNServer.Adjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))
        Dim arrEspecificacion(1) As Long

        arrEspecificacion = oEspecificacion.GrabarEspecificacion(Nombre, RutaTemporal, Id, Coment, Grupo, DataSize, "", Tipo, Nothing, Proce, Gmn1, Anyo)
        Return arrEspecificacion(0).ToString & "#" & arrEspecificacion(1).ToString
    End Function

    'Se llama desde PORTAL
    <WebMethod(EnableSession:=True)>
    Public Function GrabarEspecificacionCia(ByVal RutaTemporal As String, ByVal Nombre As String, ByVal Id As Long, ByVal Cia As Long, ByVal Coment As String) As String
        Dim FSNServer As New FSNServer.Root(True)
        Dim oEspecificacion As FSNServer.Adjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))
        Dim arrEspecificacion(1) As Long

        arrEspecificacion = oEspecificacion.GrabarEspecificacionCia(Id, Cia, Nombre, Coment, RutaTemporal)
        Return arrEspecificacion(0).ToString & "#" & arrEspecificacion(1).ToString
    End Function

    ''' <summary>
    ''' Devuelve el Contenido de la Especificacion de la Cia
    ''' </summary>
    ''' <param name="Id">Id del adjunto en bbdd</param>
    ''' <param name="Cia">Cia del adjunto</param>
    ''' <returns>Contenido de la Especificacion de la Cia</returns>
    ''' <remarks>Se le llama desde GS/CEspecificacion/LeerAdjuntoCia y desde PORTAL</remarks>
    <WebMethod(EnableSession:=True)>
    Public Function LeerEspecificacionCia(ByVal Id As Long, ByVal Cia As Long, ByVal ChunkNumber As Long, ByVal ChunkSize As Long) As Byte()
        Dim FSNServer As New FSNServer.Root(True)
        Dim oEspecificacion As FSNServer.Adjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))
        Return oEspecificacion.LeerEspecificacionCia(Id, Cia, ChunkNumber, ChunkSize)
    End Function

#End Region

#Region "Adjunto Oferta"
    'Se llama desde GS/CAdjunto/GrabarAdjuntoOferta
    <WebMethod(EnableSession:=True)>
    Public Function GrabarAdjuntoOferta(ByVal RutaTemporal As String, ByVal Nombre As String, ByVal Id As Long, ByVal DataSize As Long) As String
        Dim FSNServer As New FSNServer.Root(True)
        Dim oAdjunto As FSNServer.Adjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))
        Dim arrAdjunto(1) As Long

        arrAdjunto = oAdjunto.Grabar_Adjunto_Oferta(Id, Nombre, RutaTemporal)
        Return arrAdjunto(0).ToString & "#" & arrAdjunto(1).ToString
    End Function

    'Se llama desde GS/CAdjunto/LeerAdjuntoOferta
    <WebMethod(EnableSession:=True)>
    Public Function LeerAdjuntoOferta(ByVal Id As Long, ByVal ChunkNumber As Long, ByVal ChunkSize As Long) As Byte()
        Try
            Dim FSNServer As New FSNServer.Root(True)
            Dim oOferta As FSNServer.Ofertas = FSNServer.Get_Object(GetType(FSNServer.Ofertas))

            Return oOferta.LeerAdjuntoOferta(Id, ChunkNumber, ChunkSize)
        Catch ex As Exception
            Dim oFileW As System.IO.StreamWriter
            oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & m_sFile & ".txt", True)
            oFileW.WriteLine()
            oFileW.WriteLine("Error FSN_Adjuntos.asmx(LeerAdjuntoOferta): " & ex.Message & ". Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
            oFileW.Close()
            Return Nothing
        End Try
    End Function

    'Antes se llamaba GrabarAdjuntoPortal. Se llama desde PORTAL
    <WebMethod(EnableSession:=True)>
    Public Function GrabarAdjuntoOfertaPortal(ByVal RutaTemporal As String, ByVal Nombre As String, ByVal Id As Long, ByVal Cia As Long) As String
        Dim FSNServer As New FSNServer.Root(True)
        Dim oAdjunto As FSNServer.Adjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))
        Dim arrAdjunto(1) As Long

        arrAdjunto = oAdjunto.Grabar_Adjunto_Portal(Id, Cia, Nombre, RutaTemporal)
        Return arrAdjunto(0).ToString & "#" & arrAdjunto(1).ToString
    End Function

    ''' <summary>
    ''' Devuelve el Contenido del Adjunto del Portal si FSGS=falso, del adjunto de oferta en GS si es true
    ''' </summary>
    ''' <param name="Id">Id del adjunto en bbdd</param>
    ''' <param name="Cia">Cia del adjunto</param>
    ''' <returns>Contenido del Adjunto del Portal </returns>
    ''' <remarks>Antes se llamaba LeerAdjuntoPortal</remarks>
    <WebMethod(EnableSession:=True)>
    Public Function LeerAdjuntoOfertaPortal(ByVal Id As Long, ByVal Cia As Long, ByVal FSGS As Boolean, ByVal ChunkNumber As Long, ByVal ChunkSize As Long) As Byte()
        Dim FSNServer As New FSNServer.Root(True)
        If FSGS Then
            Dim oOferta As FSNServer.Ofertas
            oOferta = FSNServer.Get_Object(GetType(FSNServer.Ofertas))
            Return oOferta.LeerAdjuntoOferta(Id, ChunkNumber, ChunkSize)
        Else
            Dim oAdjunto As FSNServer.Adjunto
            oAdjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))
            Return oAdjunto.LeerAdjuntoPortal(Id, Cia, ChunkNumber, ChunkSize)
        End If
    End Function

#End Region

#Region "Adjuntos NUEVA"

    <WebMethod(EnableSession:=True)>
    Public Function GrabarAdjunto(ByVal RutaTemporal As String, ByVal Nombre As String, ByVal Tipo As Short, ByVal ID_Rel As String, ByVal Comentario As String, ByVal DataSize As Long) As String
        Dim FSNServer As New FSNServer.Root(True)
        Dim oAdjunto As FSNServer.Adjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))
        Dim arrAdjunto(1) As Long

        arrAdjunto = oAdjunto.GrabarAdjunto(Nombre, RutaTemporal, Tipo, ID_Rel, Comentario, DataSize)
        Return arrAdjunto(0).ToString & "#" & arrAdjunto(1).ToString
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function LeerAdjunto(ByVal Id As Long, ByVal ChunkNumber As Long, ByVal ChunkSize As Long, ByVal Tipo As Short, ByVal ID_Rel As String) As Byte()
        Try
            Dim FSNServer As New FSNServer.Root(True)
            Dim oAdjunto As FSNServer.Adjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))

            Return oAdjunto.LeerAdjunto(Id, Tipo, ChunkNumber, ChunkSize, ID_Rel)
        Catch ex As Exception
            Dim oFileW As System.IO.StreamWriter
            oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & m_sFile & ".txt", True)
            oFileW.WriteLine()
            oFileW.WriteLine("Error FSN_Adjuntos.asmx(LeerAdjunto): " & ex.Message & ". Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
            oFileW.Close()
            Return Nothing
        End Try
    End Function
#End Region

#Region "Adjuntos Solicitudes/Instancias"

    '''' <summary>
    '''' Devuelve el Contenido del adjunto
    '''' </summary>
    '''' <param name="Id">Id del adjunto en bbdd</param>
    '''' <param name="Tipo">0-solic 1-instancia campo 6-instancia desglose 3-eoc</param>
    '''' <param name="Instancia">Instancia</param>
    '''' <returns>Contenido del adjunto</returns>
    '''' <remarks>Llamada desde: GS/CCampoAdjunto.cls/LeerAdjunto y PORTAL</remarks>
    <WebMethod(EnableSession:=True)>
    Public Function LeerAdjuntoPM(ByVal Id As Long, ByVal Tipo As Byte, ByVal Instancia As Boolean) As Byte()
        Try
            Dim FSNServer As New FSNServer.Root(True)
            Dim oAdjunto As FSNServer.Adjunto
            oAdjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))

            Return oAdjunto.Leer_Adjun(Id, Tipo, Instancia)
        Catch ex As Exception
            Dim oFileW As System.IO.StreamWriter
            oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & m_sFile & ".txt", True)
            oFileW.WriteLine()
            oFileW.WriteLine("Error LeerAdjunto: " & ex.Message & ". Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
            oFileW.Close()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Devuelve el buffer de bytes de un adjunto que se solicita desde PORTAL
    ''' </summary>
    ''' <param name="sUser">usuario de portal</param>
    ''' <param name="sPassword">password del usuario</param>
    ''' <param name="Id">Id del adjunto</param>
    ''' <param name="Tipo">Tipo del adjunto</param>
    ''' <param name="bInstancia">True si es una instancia</param>
    ''' <returns></returns>
    ''' <remarks>Se podría ELIMINAR y utilizar LeerAdjunto</remarks>
    <WebMethod(EnableSession:=True)>
    Public Function LeerAdjuntoPortal(ByVal sUser As String, ByVal sPassword As String, ByVal Id As Long, ByVal Tipo As Byte, ByVal bInstancia As Boolean) As Byte()
        Dim FSNServer As New FSNServer.Root
        FSNServer.LoginPortal(sUser, sPassword)

        Dim oAdjunto As FSNServer.Adjunto
        oAdjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))
        Return oAdjunto.Leer_Adjun(Id, Tipo, bInstancia)
    End Function

    ''' <summary>
    ''' Graba el adjunto que se ha adjuntado desde PORTAL
    ''' </summary>
    ''' <param name="Adjunto">Buffer de bytes del fichero</param>
    ''' <param name="sUser">usuario del portal</param>
    ''' <param name="sPassword">password del usuario del portal</param>
    ''' <param name="sProve">Codigo del proveedor</param>
    ''' <param name="sNombre">Nombre del fichero</param>
    ''' <param name="sComent">Comentario del adjunto</param>
    ''' <param name="sPer">Codigo de la persona</param>
    ''' <param name="DataSize">Tamaño del fichero</param>
    ''' <param name="Idioma">Idioma del usuario</param>
    ''' <param name="FecAct"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True)>
    Public Function GrabarAdjuntoPortal(ByVal Adjunto As Byte(), ByVal sUser As String, ByVal sPassword As String, ByVal sProve As String, ByVal sNombre As String, ByVal sComent As String, ByVal sPer As String, ByVal DataSize As Long, ByVal Idioma As String, ByVal FecAct As DateTime) As String
        Dim FSNServer As New FSNServer.Root
        FSNServer.LoginPortal(sUser, sPassword)

        Dim rutaTemp As String = ConfigurationManager.AppSettings("temp") & "\" & modUtilidades.GenerateRandomPath()
        If Not Directory.Exists(rutaTemp) Then
            Directory.CreateDirectory(rutaTemp)
        End If

        'Guardo el fichero en la carpeta temporal
        System.IO.File.WriteAllBytes(rutaTemp & "\" & sNombre, Adjunto)
        Dim oAdjunto As FSNServer.Adjunto
        oAdjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))

        Dim arrAdjunto(1) As Long

        arrAdjunto = oAdjunto.Save_Adjun(Nothing, sProve, sNombre, rutaTemp, , sComent, , , , , , , DataSize, Idioma, , FecAct)

        Return arrAdjunto(0).ToString & "#" & arrAdjunto(1).ToString
    End Function

    '''' <summary>
    '''' Graba un adjunto 
    '''' </summary>
    '''' <param name="Adjunto">Contenido del adjunto</param>
    '''' <param name="Prove">Proveedor</param>
    '''' <param name="Nombre">Nombre del adjunto</param>
    '''' <param name="Solicitud">Solicitud</param>
    '''' <param name="Instancia">Instancia</param>
    '''' <param name="Campo">Id de copia_campo</param>
    '''' <param name="Linea">Linea desglose</param>
    '''' <param name="CampoPadre">Campo Padre desglose</param>
    '''' <param name="CampoHijo">Campo Hijo desglose</param>
    '''' <param name="Coment">Comentario adjunto</param>
    '''' <param name="Per">Persona</param>
    '''' <param name="DataSize">Tamaño adjunto</param>
    '''' <param name="Idioma">Idioma</param>
    '''' <param name="FecAct">FecAct</param>
    '''' <returns>datos para acceder al adjunto</returns>
    '''' <remarks>Llamada desde: GS/CCampoAdjunto.cls/GrabarAdjunto</remarks>
    <WebMethod(EnableSession:=True)>
    Public Function GrabarAdjuntoPM(ByVal Adjunto As Byte(), ByVal Prove As String, ByVal Nombre As String, ByVal Solicitud As Long, ByVal Instancia As Byte, ByVal Campo As Long, ByVal Linea As Long, ByVal CampoPadre As Long, ByVal CampoHijo As Long, ByVal Coment As String, ByVal Per As String, ByVal DataSize As Long, ByVal Idioma As String, ByVal FecAct As DateTime) As String
        Try
            Dim FSNServer As New FSNServer.Root(True)
            Dim rutaTemp As String = ConfigurationManager.AppSettings("temp") & "\" & modUtilidades.GenerateRandomPath()
            If Not Directory.Exists(rutaTemp) Then
                Directory.CreateDirectory(rutaTemp)
            End If

            'Guardo el fichero en la carpeta temporal
            System.IO.File.WriteAllBytes(rutaTemp & "\" & Nombre, Adjunto)
            Dim oAdjunto As FSNServer.Adjunto
            oAdjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))

            Dim arrAdjunto(1) As Long
            Dim Tipo As Byte

            If Solicitud > 0 Then
                Tipo = 1 'Se grabara en SOLICITUD_ADJUN
            Else
                If Instancia > 0 Then
                    If Campo > 0 Then
                        Tipo = 4 'Se grabara en PM_COPIA_ADJUN
                    ElseIf Linea > 0 Then
                        Tipo = 6 'Adjunto de desglose en una instancia
                    End If
                Else
                    If Campo > 0 Then
                        Tipo = 2 'Se grabara en CAMPO_ADJUN
                    Else
                        If Linea > 0 Then
                            Tipo = 3 'Se grabara en LINEA_DESGLOSE_ADJUN
                        Else
                            Tipo = Nothing 'Se grabara en PM_COPIA_ADJUN
                        End If
                    End If
                End If
            End If
            arrAdjunto = oAdjunto.Save_Adjun(Per, Prove, Nombre, rutaTemp, , Coment, Solicitud, Instancia, Campo, Linea, CampoPadre, CampoHijo, DataSize, Idioma, Tipo, FecAct)

            Return arrAdjunto(0).ToString & "#" & arrAdjunto(1).ToString
        Catch ex As Exception
            Dim oFileW As System.IO.StreamWriter
            oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & m_sFile & ".txt", True)
            oFileW.WriteLine()
            oFileW.WriteLine("Error GrabarAdjunto: " & ex.Message & ". Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
            oFileW.Close()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Sustituir un adjunto
    ''' </summary>
    ''' <param name="Id">Id del adjunto en bbdd</param>
    ''' <param name="Adjunto">Contenido del adjunto</param>
    ''' <param name="Nombre">Nombre del adjunto</param>
    ''' <param name="Coment">Comentario adjunto</param>
    ''' <param name="DataSize">Tamaño adjunto</param>
    ''' <param name="Tipo">1-solic 4-instancia campo 6-instancia desglose 2-campo (sin indicar instancia) 3-eoc</param>
    ''' <param name="Instancia">Instancia</param>
    ''' <returns>datos para acceder al adjunto</returns>
    ''' <remarks>Llamada desde: GS/CCampoAdjunto.cls/SustituirAdjunto, Tiempo maximo: 0</remarks>
    <WebMethod(EnableSession:=True)>
    Public Function SustituirAdjuntoPM(ByVal Id As Long, ByVal Adjunto As Byte(), ByVal Nombre As String, ByVal Coment As String, ByVal DataSize As Long, ByVal Tipo As Byte, ByVal Instancia As Boolean) As String
        Try
            Dim FSNServer As New FSNServer.Root(True)
            Dim rutaTemp As String = ConfigurationManager.AppSettings("temp") & "\" & modUtilidades.GenerateRandomPath()
            If Not Directory.Exists(rutaTemp) Then
                Directory.CreateDirectory(rutaTemp)
            End If

            'Guardo el fichero en la carpeta temporal
            System.IO.File.WriteAllBytes(rutaTemp & "\" & Nombre, Adjunto)
            Dim oAdjunto As FSNServer.Adjunto
            oAdjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))

            Dim arrAdjunto(1) As Long
            arrAdjunto = oAdjunto.Update_Adjun(Id, Tipo, Nombre, rutaTemp, Coment, DataSize)

            Return arrAdjunto(0).ToString & "#" & arrAdjunto(1).ToString
        Catch ex As Exception
            Dim oFileW As System.IO.StreamWriter
            oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & m_sFile & ".txt", True)
            oFileW.WriteLine()
            oFileW.WriteLine("Error SustituirAdjunto: " & ex.Message & ". Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
            oFileW.Close()
            Return Nothing
        End Try
    End Function
#End Region

End Class