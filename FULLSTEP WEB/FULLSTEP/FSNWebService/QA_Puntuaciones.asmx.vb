﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Threading
' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/WebServicePM/QA_Puntuaciones")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class QA_Puntuaciones
    Inherits System.Web.Services.WebService

    Private m_FSNServer As FSNServer.Root    
    Private m_dsVariablesSimulacion As DataSet
    Private m_sUsu As String

    ''' <summary>
    ''' Proceso que ejecuta la el calculo de las puntuaciones.
    ''' Metodo propio para la llamada del webService
    ''' </summary>
    ''' <param name="sUsu">Codigo de Usuario para la identificacion</param>
    ''' <param name="spwd">Contraseña encriptada del usuario para la identificacion</param>        
    ''' <remarks>Llamada desde=Cada vez que se llame al webService --> QAPuntuaciones.vb; Tiempo máximo=0,2seg.</remarks>
    <WebMethod()>
    Public Sub CalcularPuntuaciones(ByVal sUsu As String, ByVal spwd As String)
        Dim oUser As FSNServer.User
        Dim oFileW As System.IO.StreamWriter
        Dim sFile As String = "\log_QAPuntuaciones"
        Try
            m_FSNServer = New FSNServer.Root
            spwd = URLDecode(spwd)
            oUser = m_FSNServer.Login(sUsu, spwd, True, False, False, True)
            m_sUsu = sUsu

            If oUser.ResultadoLogin = TipoResultadoLogin.LoginOk Then
                Dim oPuntuaciones As FSNServer.Puntuaciones = m_FSNServer.Get_Object(GetType(FSNServer.Puntuaciones))
                oPuntuaciones.CalcularPuntuaciones()
            Else
                oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & sFile & ".txt", True)
                oFileW.WriteLine()
                oFileW.WriteLine("Error QA_Puntuaciones Login webService, Usuario mal logeado: Usuario=" & sUsu & " pwd= " & spwd & " . Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
                oFileW.Close()
            End If
        Catch ex As Exception
            oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & sFile & ".txt", True)
            oFileW.WriteLine()
            oFileW.WriteLine("Error QA_Puntuaciones Login webService: " & ex.Message & ". Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
            oFileW.Close()
        End Try
    End Sub
    ''' <summary>
    ''' Proceso que ejecuta la el calculo de las puntuacio del proveedor indicado.
    ''' Metodo propio para la llamada del webService
    ''' </summary>
    ''' <param name="sUsu">Codigo de Usuario para la identificacion</param>
    ''' <param name="spwd">Contraseña encriptada del usuario para la identificacion</param>    
    ''' <param name="sCodProve">Codigo del Proveedor para el que calcular la puntuacion</param> 
    ''' <remarks>Llamada desde = Integracion; Tiempo máximo=0,2seg.</remarks>
    <WebMethod()>
    Public Sub CalcularPuntuacionProveedor(ByVal sUsu As String, ByVal spwd As String, ByVal sCodProve As String)
        Dim oUser As FSNServer.User
        Dim oFileW As System.IO.StreamWriter
        Dim sFile As String = "\log_QAPuntuaciones"
        Try
            m_FSNServer = New FSNServer.Root
            spwd = URLDecode(spwd)
            oUser = m_FSNServer.Login(sUsu, spwd, True, False, False, True)
            m_sUsu = sUsu

            If oUser.ResultadoLogin = TipoResultadoLogin.LoginOk Then
                Dim oPuntuaciones As FSNServer.Puntuaciones = m_FSNServer.Get_Object(GetType(FSNServer.Puntuaciones))
                oPuntuaciones.CalcularPuntuacionProveedor(sCodProve)
            Else
                oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & sFile & ".txt", True)
                oFileW.WriteLine()
                oFileW.WriteLine("Error QA_Puntuaciones Login webService, Usuario mal logeado: Usuario=" & sUsu & " pwd= " & spwd & " . Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
                oFileW.Close()
            End If
        Catch ex As Exception
            oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & sFile & ".txt", True)
            oFileW.WriteLine()
            oFileW.WriteLine("Error QA_Puntuaciones Calculo Puntuacion Proveedor: " & ex.Message & ". Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
            oFileW.Close()
        End Try
    End Sub
    ''' <summary>
    ''' function que ejecuta la simulacion del calculo de las puntuaciones.
    ''' Metodo propio para la llamada del webService
    ''' </summary>
    ''' <param name="sUsu">Codigo de Usuario para la identificacion</param>
    ''' <param name="spwd">Contraseña encriptada del usuario para la identificacion</param>        
    ''' <remarks>Llamada desde=Cada vez que se llame al webService --> QAPuntuaciones.vb; Tiempo máximo=0,2seg.</remarks>
    <WebMethod()>
    Public Function SimularPuntuacion(ByVal dsVariablesManuales As DataSet, ByVal sIdi As String, ByVal bGuardar As Boolean, ByVal sUsu As String, ByVal sPwd As String) As DataSet
        Dim oUser As FSNServer.User

        m_FSNServer = New FSNServer.Root
        oUser = m_FSNServer.Login(sUsu, sPwd)
        If oUser.ResultadoLogin = TipoResultadoLogin.LoginOk Then
            SimularPuntuacion = SimularPuntuacionWeb(dsVariablesManuales, sIdi, bGuardar)
        Else
            Return Nothing
        End If
        oUser = Nothing
    End Function
    ''' <summary>
    ''' function que llama al proceso de Simulacion de la puntuacion de variables
    ''' </summary>
    ''' <param name="dsVariablesManuales">Dataset con las variables manuales</param>
    ''' <param name="sIdi">Código Idioma para las calificaciones</param>               
    ''' <param name="bGuardar">Almacenar o no la simulacion</param>        
    ''' <returns>Dataset con las Variables de calidad que sus puntuaciones han variado.</returns>
    ''' <remarks>Llamada desde=QA-->FichaProveedor.aspx; Tiempo máximo=0,1seg.</remarks>
    Private Function SimularPuntuacionWeb(ByVal dsVariablesManuales As DataSet, ByVal sIdi As String, ByVal bGuardar As Boolean) As DataSet
        Dim dsVariables As DataSet
        dsVariables = EjecutarSimularPuntuacion(dsVariablesManuales, sIdi, bGuardar)
        SimularPuntuacionWeb = dsVariables
    End Function
#Region "Calculo simulacion"
    ''' <summary>
    ''' Calcula las nuevas puntuaciones con la simulacion que le pasamos con los valores en el dataset.
    ''' Ordena el dataset por Unidades de negocio, para calcular la simulacion por unidad de negocio
    ''' Cuando cambia de unidad de negocio se recalculan las puntuaciones
    ''' Y cuando termina y tiene la opcion de guadar=true almacena la simulacion
    ''' </summary>
    ''' <param name="dsVariablesManuales">Dataset con las variables manuales que se han modificado</param>
    ''' <param name="sIdi">Código idioma para almacenar las calificaciones</param>
    ''' <param name="bGuardar">Almacenar o no la puntuacion de la simulacion</param>
    ''' <returns>DataSet con las variables de calidad que han cambiado de puntuacion</returns>
    ''' <remarks>Llamada desde=QA_Puntuaciones.asmx; Tiempo máximo= 3seg. </remarks>
    Private Function EjecutarSimularPuntuacion(ByVal dsVariablesManuales As DataSet, ByVal sIdi As String, ByVal bGuardar As Boolean) As DataSet
        Dim i As Long
        Dim lUNQA, lUNQA_ant As Long
        Dim lNivel As Integer
        Dim lVarCal As Long
        Dim dPunt As Double
        Dim sProve As String = ""
        Dim dsVariables As New DataSet
        Dim bEjecutar As Boolean
        Dim dNewRow As DataRow
        Dim oProve As FSNServer.Proveedor

        oProve = m_FSNServer.Get_Object(GetType(FSNServer.Proveedor))
        dsVariablesManuales.Tables(0).DefaultView.Sort = "UNQA ASC"
        m_dsVariablesSimulacion = Nothing

        For i = 0 To dsVariablesManuales.Tables(0).DefaultView.Table.Rows.Count - 1
            lUNQA = dsVariablesManuales.Tables(0).DefaultView(i).Item("UNQA")
            sProve = dsVariablesManuales.Tables(0).DefaultView(i).Item("PROVE")
            lNivel = dsVariablesManuales.Tables(0).DefaultView(i).Item("NIVEL")
            lVarCal = dsVariablesManuales.Tables(0).DefaultView(i).Item("VARCAL")
            dPunt = dsVariablesManuales.Tables(0).DefaultView(i).Item("VALOR")
            If lUNQA_ant <> lUNQA Then
                If lUNQA_ant <> 0 Then
                    CalcularSimulacion(sProve, sIdi, lUNQA_ant, dsVariables)
                    bEjecutar = False
                End If
                oProve.Cod = sProve
                oProve.CargarVariablesSimulacion(lUNQA)
                lUNQA_ant = lUNQA
                dsVariables = oProve.Variables
                bEjecutar = True
            End If

            If dsVariables.Tables(lNivel).Select("ID=" & lVarCal).Length > 0 Then
                'Si encuentra el valor Sustituir el valor Manual introducido
                dNewRow = dsVariables.Tables(lNivel).Select("ID=" & lVarCal)(0)
                dNewRow.Item("PUNT_NUE") = dPunt
            End If
        Next
        If bEjecutar Then
            CalcularSimulacion(sProve, sIdi, lUNQA_ant, dsVariables)
        End If
        If bGuardar Then
            oProve.GuardarVariablesSimulacion(m_dsVariablesSimulacion)
        End If
        EjecutarSimularPuntuacion = m_dsVariablesSimulacion
        oProve = Nothing
    End Function
    ''' <summary>
    ''' Proceso que lleva a cabo La simulacion del calculo de la puntuacion se un proveedor.
    ''' Recorre todas las variables de calidad y si se ha cambiado la puntuacion se calcula la puntuacion con la formula y tb el de sus padres.
    ''' </summary>
    ''' <param name="sProve">Código de proveedor con el que se esta calculando</param>
    ''' <param name="sIdi">Idioma en el que se obtiene las calificaciones</param>        
    ''' <param name="lIdUnqa">ID de la Unidad de Negocio</param>        
    ''' <param name="dsVariables">Dataset con las variables de calidad</param>
    ''' <remarks>Llamada desde=QA_Puntuaciones.asmx; Tiempo máximo=3seg</remarks>
    Private Sub CalcularSimulacion(ByVal sProve As String, ByVal sIdi As String, ByVal lIdUnqa As Long, ByVal dsVariables As DataSet)
        Dim oOrigRow1, oOrigRow2, oOrigRow3, oOrigRow4, oOrigRow5 As DataRow
        Dim buenas1, buenas2, buenas3, buenas4, buenas5 As Short 'Va a contener cuantas variables de ese nivel tiene con valor
        Dim ind0, ind1, ind2, ind3, ind4 As Short
        Dim sVariables0() As String = Nothing
        Dim sVariables1() As String
        Dim sVariables2() As String
        Dim sVariables3() As String
        Dim sVariables4() As String = Nothing
        Dim dValues0() As Double = Nothing
        Dim dValues1() As Double
        Dim dValues2() As Double
        Dim dValues3() As Double
        Dim dValues4() As Double = Nothing
        Dim Res() As Object
        Dim iEq As New USPExpress.USPExpression
        Dim oDT As DataTable
        Dim dNewRow As DataRow

        If m_dsVariablesSimulacion Is Nothing Then
            'Si no esta creada el dataset se crea sino se va añadiendo...
            m_dsVariablesSimulacion = New DataSet
            oDT = m_dsVariablesSimulacion.Tables.Add("VARIABLES")
            oDT.Columns.Add("PROVE", System.Type.GetType("System.String"))
            oDT.Columns.Add("UNQA", System.Type.GetType("System.Int32"))
            oDT.Columns.Add("VARCAL", System.Type.GetType("System.Int32"))
            oDT.Columns.Add("NIVEL", System.Type.GetType("System.Int32"))
            oDT.Columns.Add("VALOR", System.Type.GetType("System.Double"))
            oDT.Columns.Add("CALID", System.Type.GetType("System.Int32"))
            oDT.Columns.Add("CAL", System.Type.GetType("System.String"))
        Else
            oDT = m_dsVariablesSimulacion.Tables("VARIABLES")
        End If
        buenas1 = 0

        'Recorremos todas las variables recalculando
        Dim oPuntuaciones As FSNServer.Puntuaciones = m_FSNServer.Get_Object(GetType(FSNServer.Puntuaciones))
        'Nivel 1
        For Each oOrigRow1 In dsVariables.Tables(1).Rows
            ReDim sVariables1(0)
            ReDim dValues1(0)
            ind1 = 0
            buenas2 = 0
            'Nivel 2
            For Each oOrigRow2 In oOrigRow1.GetChildRows("REL_VAR1_VAR2")
                If oOrigRow2.Item("TIPO") = 0 Then 'Si es simple y se puede modificar
                    If oOrigRow2.Item("PUNT") <> oOrigRow2.Item("PUNT_NUE") Then
                        dNewRow = oDT.NewRow
                        dNewRow.Item("VALOR") = oOrigRow2.Item("PUNT_NUE")
                        Res = oPuntuaciones.ObtenerCalificacion(oOrigRow2.Item("ID"), 2, dNewRow.Item("VALOR"), sIdi)
                        dNewRow.Item("CALID") = Res(0)
                        dNewRow.Item("CAL") = Res(1)

                        ''Cambio de variable de calidad
                        dNewRow.Item("PROVE") = sProve
                        dNewRow.Item("UNQA") = lIdUnqa
                        dNewRow.Item("VARCAL") = oOrigRow2.Item("ID")
                        dNewRow.Item("NIVEL") = 2
                        ''-----------------------------
                        oOrigRow2.Item("PUNT") = oOrigRow2.Item("PUNT_NUE")
                        buenas2 = buenas2 + 1
                        oDT.Rows.Add(dNewRow)
                    End If
                Else 'Si es compuesta
                    ReDim sVariables2(0)
                    ReDim dValues2(0)
                    ind2 = 0
                    buenas3 = 0
                    For Each oOrigRow3 In oOrigRow2.GetChildRows("REL_VAR2_VAR3")
                        If oOrigRow3.Item("TIPO") = 0 Then 'Si es simple y se puede modificar
                            If oOrigRow3.Item("PUNT") <> oOrigRow3.Item("PUNT_NUE") Then
                                dNewRow = oDT.NewRow
                                dNewRow.Item("VALOR") = oOrigRow3.Item("PUNT_NUE")
                                Res = oPuntuaciones.ObtenerCalificacion(oOrigRow3.Item("ID"), 3, dNewRow.Item("VALOR"), sIdi)
                                dNewRow.Item("CALID") = Res(0)
                                dNewRow.Item("CAL") = Res(1)

                                ''Cambio de variable de calidad
                                dNewRow.Item("PROVE") = sProve
                                dNewRow.Item("UNQA") = lIdUnqa
                                dNewRow.Item("VARCAL") = oOrigRow3.Item("ID")
                                dNewRow.Item("NIVEL") = 3
                                ''-----------------------------
                                oOrigRow3.Item("PUNT") = oOrigRow3.Item("PUNT_NUE")
                                buenas3 = buenas3 + 1
                                oDT.Rows.Add(dNewRow)
                            End If
                        Else 'Si es compuesta
                            ReDim sVariables3(0)
                            ReDim dValues3(0)
                            ind3 = 0
                            buenas4 = 0
                            For Each oOrigRow4 In oOrigRow3.GetChildRows("REL_VAR3_VAR4")
                                If oOrigRow4.Item("TIPO") = 0 Then 'Si es simple y se puede modificar
                                    If oOrigRow4.Item("PUNT") <> oOrigRow4.Item("PUNT_NUE") Then
                                        dNewRow = oDT.NewRow
                                        dNewRow.Item("VALOR") = oOrigRow4.Item("PUNT_NUE")
                                        Res = oPuntuaciones.ObtenerCalificacion(oOrigRow4.Item("ID"), 4, dNewRow.Item("VALOR"), sIdi)
                                        dNewRow.Item("CALID") = Res(0)
                                        dNewRow.Item("CAL") = Res(1)

                                        ''Cambio de variable de calidad
                                        dNewRow.Item("PROVE") = sProve
                                        dNewRow.Item("UNQA") = lIdUnqa
                                        dNewRow.Item("VARCAL") = oOrigRow4.Item("ID")
                                        dNewRow.Item("NIVEL") = 4
                                        ''-----------------------------
                                        oOrigRow4.Item("PUNT") = oOrigRow4.Item("PUNT_NUE")
                                        buenas4 = buenas4 + 1
                                        oDT.Rows.Add(dNewRow)
                                    End If
                                Else 'Si es compuesta
                                    ReDim sVariables4(0)
                                    ReDim dValues4(0)
                                    ind4 = 0
                                    buenas5 = 0
                                    For Each oOrigRow5 In oOrigRow4.GetChildRows("REL_VAR4_VAR5")
                                        If oOrigRow5.Item("PUNT") <> oOrigRow5.Item("PUNT_NUE") Then
                                            dNewRow = oDT.NewRow
                                            dNewRow.Item("VALOR") = oOrigRow5.Item("PUNT_NUE")
                                            Res = oPuntuaciones.ObtenerCalificacion(oOrigRow5.Item("ID"), 5, dNewRow.Item("VALOR"), sIdi)
                                            dNewRow.Item("CALID") = Res(0)
                                            dNewRow.Item("CAL") = Res(1)

                                            ''Cambio de variable de calidad
                                            dNewRow.Item("PROVE") = sProve
                                            dNewRow.Item("UNQA") = lIdUnqa
                                            dNewRow.Item("VARCAL") = oOrigRow5.Item("ID")
                                            dNewRow.Item("NIVEL") = 5
                                            ''-----------------------------
                                            oOrigRow5.Item("PUNT") = oOrigRow5.Item("PUNT_NUE")
                                            buenas5 = buenas5 + 1
                                            oDT.Rows.Add(dNewRow)
                                        End If
                                        ReDim Preserve sVariables4(ind4)
                                        sVariables4(ind4) = oOrigRow5.Item("COD")
                                        ReDim Preserve dValues4(ind4)
                                        dValues4(ind4) = DBNullToDbl(oOrigRow5.Item("PUNT"))
                                        ind4 += 1
                                    Next '5

                                    If buenas5 > 0 Then 'Ha cambiado alguna variable de valor
                                        If Not IsDBNull(oOrigRow4.Item("FORMULA")) Then
                                            Try
                                                dNewRow = oDT.NewRow
                                                iEq.Parse(oOrigRow4.Item("FORMULA"), sVariables4)
                                                dNewRow.Item("VALOR") = iEq.Evaluate(dValues4)
                                                Res = oPuntuaciones.ObtenerCalificacion(oOrigRow4.Item("ID"), 4, dNewRow.Item("VALOR"), sIdi)
                                                dNewRow.Item("CALID") = Res(0)
                                                dNewRow.Item("CAL") = Res(1)

                                                ''Cambio de variable de calidad
                                                dNewRow.Item("PROVE") = sProve
                                                dNewRow.Item("UNQA") = lIdUnqa
                                                dNewRow.Item("VARCAL") = oOrigRow4.Item("ID")
                                                dNewRow.Item("NIVEL") = 4
                                                ''-----------------------------
                                                oOrigRow4.Item("PUNT") = dNewRow.Item("VALOR")
                                                buenas4 = buenas4 + 1
                                                oDT.Rows.Add(dNewRow)
                                            Catch ex As USPExpress.UnknownVariableException
                                            Catch ex0 As Exception
                                            End Try
                                        End If
                                    End If
                                End If

                                If buenas5 > 0 Then 'Ha cambiado alguna variable de valor
                                    If Not IsDBNull(oOrigRow4.Item("FORMULA")) Then
                                        Try
                                            dNewRow = oDT.NewRow
                                            iEq.Parse(oOrigRow4.Item("FORMULA"), sVariables4)
                                            dNewRow.Item("VALOR") = iEq.Evaluate(dValues4)
                                            Res = oPuntuaciones.ObtenerCalificacion(oOrigRow4.Item("ID"), 4, dNewRow.Item("VALOR"), sIdi)
                                            dNewRow.Item("CALID") = Res(0)
                                            dNewRow.Item("CAL") = Res(1)

                                            ''Cambio de variable de calidad
                                            dNewRow.Item("PROVE") = sProve
                                            dNewRow.Item("UNQA") = lIdUnqa
                                            dNewRow.Item("VARCAL") = oOrigRow4.Item("ID")
                                            dNewRow.Item("NIVEL") = 4
                                            ''-----------------------------
                                            oOrigRow4.Item("PUNT") = dNewRow.Item("VALOR")
                                            buenas4 = buenas4 + 1
                                            oDT.Rows.Add(dNewRow)
                                        Catch ex As USPExpress.UnknownVariableException
                                        Catch ex0 As Exception
                                        End Try
                                    End If
                                End If

                                ReDim Preserve sVariables3(ind3)
                                sVariables3(ind3) = oOrigRow4.Item("COD")
                                ReDim Preserve dValues3(ind3)
                                dValues3(ind3) = DBNullToDbl(oOrigRow4.Item("PUNT"))
                                ind3 += 1
                            Next '4

                            If buenas4 > 0 Then 'Ha cambiado alguna variable de valor
                                If Not IsDBNull(oOrigRow3.Item("FORMULA")) Then
                                    Try
                                        dNewRow = oDT.NewRow
                                        iEq.Parse(oOrigRow3.Item("FORMULA"), sVariables3)
                                        dNewRow.Item("VALOR") = iEq.Evaluate(dValues3)
                                        Res = oPuntuaciones.ObtenerCalificacion(oOrigRow3.Item("ID"), 3, dNewRow.Item("VALOR"), sIdi)
                                        dNewRow.Item("CALID") = Res(0)
                                        dNewRow.Item("CAL") = Res(1)

                                        ''Cambio de variable de calidad
                                        dNewRow.Item("PROVE") = sProve
                                        dNewRow.Item("UNQA") = lIdUnqa
                                        dNewRow.Item("VARCAL") = oOrigRow3.Item("ID")
                                        dNewRow.Item("NIVEL") = 3
                                        ''-----------------------------
                                        oOrigRow3.Item("PUNT") = dNewRow.Item("VALOR")
                                        buenas3 = buenas3 + 1
                                        oDT.Rows.Add(dNewRow)
                                    Catch ex As USPExpress.UnknownVariableException
                                    Catch ex0 As Exception
                                    End Try
                                End If
                            End If
                        End If
                        ReDim Preserve sVariables2(ind2)
                        sVariables2(ind2) = oOrigRow3.Item("COD")
                        ReDim Preserve dValues2(ind2)
                        dValues2(ind2) = DBNullToDbl(oOrigRow3.Item("PUNT"))
                        ind2 += 1
                    Next '3

                    If buenas3 > 0 Then 'Ha cambiado alguna variable de valor
                        If Not IsDBNull(oOrigRow2.Item("FORMULA")) Then
                            Try
                                dNewRow = oDT.NewRow
                                iEq.Parse(oOrigRow2.Item("FORMULA"), sVariables2)
                                dNewRow.Item("VALOR") = iEq.Evaluate(dValues2)
                                Res = oPuntuaciones.ObtenerCalificacion(oOrigRow2.Item("ID"), 2, dNewRow.Item("VALOR"), sIdi)
                                dNewRow.Item("CALID") = Res(0)
                                dNewRow.Item("CAL") = Res(1)

                                ''Cambio de variable de calidad
                                dNewRow.Item("PROVE") = sProve
                                dNewRow.Item("UNQA") = lIdUnqa
                                dNewRow.Item("VARCAL") = oOrigRow2.Item("ID")
                                dNewRow.Item("NIVEL") = 2
                                ''-----------------------------
                                oOrigRow2.Item("PUNT") = dNewRow.Item("VALOR")
                                buenas2 = buenas2 + 1
                                oDT.Rows.Add(dNewRow)
                            Catch ex As USPExpress.UnknownVariableException
                            Catch ex0 As Exception
                            End Try
                        End If
                    End If
                End If

                ReDim Preserve sVariables1(ind1)
                sVariables1(ind1) = oOrigRow2.Item("COD")
                ReDim Preserve dValues1(ind1)
                dValues1(ind1) = DBNullToDbl(oOrigRow2.Item("PUNT"))
                ind1 += 1
            Next '2

            If buenas2 > 0 Then 'Ha cambiado alguna variable de valor
                If Not IsDBNull(oOrigRow1.Item("FORMULA")) Then
                    Try
                        dNewRow = oDT.NewRow
                        iEq.Parse(oOrigRow1.Item("FORMULA"), sVariables1)
                        dNewRow.Item("VALOR") = iEq.Evaluate(dValues1)
                        Res = oPuntuaciones.ObtenerCalificacion(oOrigRow1.Item("ID"), 1, dNewRow.Item("VALOR"), sIdi)
                        dNewRow.Item("CALID") = Res(0)
                        dNewRow.Item("CAL") = Res(1)

                        ''Cambio de variable de calidad
                        dNewRow.Item("PROVE") = sProve
                        dNewRow.Item("UNQA") = lIdUnqa
                        dNewRow.Item("VARCAL") = oOrigRow1.Item("ID")
                        dNewRow.Item("NIVEL") = 1
                        ''-----------------------------
                        oOrigRow1.Item("PUNT") = dNewRow.Item("VALOR")
                        buenas1 = buenas1 + 1
                        oDT.Rows.Add(dNewRow)
                    Catch ex As USPExpress.UnknownVariableException
                    Catch ex0 As Exception
                    End Try
                End If
            End If

            ReDim Preserve sVariables0(ind0)
            sVariables0(ind0) = oOrigRow1.Item("COD")
            ReDim Preserve dValues0(ind0)
            dValues0(ind0) = DBNullToDbl(oOrigRow1.Item("PUNT"))
            ind0 += 1
        Next '1

        Dim sFormulaProve As String
        If buenas1 > 0 Then
            sFormulaProve = dsVariables.Tables(0).Rows(0).Item("FORMULA")
            If Not IsDBNull(sFormulaProve) Then
                Try
                    dNewRow = oDT.NewRow
                    iEq.Parse(sFormulaProve, sVariables0)
                    dNewRow.Item("VALOR") = iEq.Evaluate(dValues0)
                    Res = oPuntuaciones.ObtenerCalificacion(dsVariables.Tables(0).Rows(0).Item("ID"), 0, dNewRow.Item("VALOR"), sIdi)
                    dNewRow.Item("CALID") = Res(0)
                    dNewRow.Item("CAL") = Res(1)

                    ''Cambio de variable de calidad
                    dNewRow.Item("PROVE") = sProve
                    dNewRow.Item("UNQA") = lIdUnqa
                    dNewRow.Item("VARCAL") = dsVariables.Tables(0).Rows(0).Item("ID")
                    dNewRow.Item("NIVEL") = 0
                    ''-----------------------------
                    oDT.Rows.Add(dNewRow)
                Catch ex As USPExpress.ParseException
                Catch ex0 As Exception
                End Try
            End If
        End If
    End Sub
#End Region
    ''' <summary>
    ''' Funcion que trata de simular server.URLDecode transforma la contraseña que viene ya transformada desde el GS
    ''' y del servicio y la trasforma a contraseña correcta de BBDD
    ''' </summary>
    ''' <param name="sEncodedURL">Contraseña a transformar</param>
    ''' <returns>String con cadena transformada</returns>
    ''' <remarks>Llamada desde=propia pagina; Tiempo máximo=0seg.</remarks>
    ''' 
    Public Function URLDecode(ByVal sEncodedURL As String) As String
        Dim iLoop As Integer
        Dim sRtn As String = ""
        Dim sTmp As String = ""

        If Len(sEncodedURL) > 0 Then
            For iLoop = 1 To Len(sEncodedURL)
                sTmp = Mid(sEncodedURL, iLoop, 1)
                sTmp = Replace(sTmp, "+", " ")
                If sTmp = "%" And Len(sEncodedURL) + 1 > iLoop + 2 Then
                    sTmp = Mid(sEncodedURL, iLoop + 1, 2)
                    sTmp = Chr(CDec("&H" & sTmp))
                    ' Increment loop by 2
                    iLoop = iLoop + 2
                End If
                sRtn = sRtn & sTmp
            Next iLoop
        End If
        URLDecode = sRtn
    End Function
End Class