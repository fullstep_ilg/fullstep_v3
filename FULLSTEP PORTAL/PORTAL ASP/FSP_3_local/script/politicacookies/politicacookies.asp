﻿<!--#include file="../common/XSS.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<%
    'Detectar idioma
    Idioma = Request.QueryString("Idioma")
    Idioma =trim(Idioma)
    if Request.QueryString("mostrar")="true" then
        mostrar=true
    else
        mostrar=false
    end if

    If Idioma="" then
	    Idioma="ENG"
    end if
    'Detectar navegador
    nav=Request.ServerVariables("HTTP_USER_AGENT")
    
    ''Textos de la página
    text=devolverTextos(Idioma,132)
    '''instrucciones
    instrucciones=text(17)
    select case getNavegador
        case "MOZ"
            instrucciones=text(18)
        case "CHR"
            instrucciones=text(19)
        Case "OPR"
            instrucciones=text(20)
        case "SAF"
            instrucciones=text(21)
    end select
    ''imagen navegador
    imagen="noseleccionada"
    imagen= application("RUTANORMAL") & "custom/" & application("NOMPORTAL") & "/images/" & Idioma & "_" & getNavegador() & ".png"
    cabecera=  application("RUTASEGURA") &  "custom/" & application("NOMPORTAL") & "/top.htm"
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title><%=text(2) %></title>   
        <script type="text/javascript" src="<%=application("RUTASEGURA")%>script/js/jquery.min.js"></script>           
        <script type="text/javascript" src="<%=application("RUTASEGURA")%>script/politicacookies/js/politicacookies.js"></script>
        
        <script type="text/javascript">
            $('document').ready(function () {
                if (areCookiesEnabled()) {
                    $(".alerta").hide();
                }
            });        
        </script>
        <link href="../../script/login/<%=application("NOMPORTAL")%>/estilos.css" rel="stylesheet" type="text/css"/>
    
        <style type="text/css">
            #contenedor
            {
                margin:0 auto;width:80%;text-align:justify;
            }
            .alerta
            {
                padding:0.8em;border:1px solid;background-color:#ffc;
            }
            .alerta img
            {
                margin:0.5em;
            }
        </style>
    </head>

    <body >
        
        <div id="contenedor" class="textos">
            <iframe src="<%=cabecera%>" width="100%" frameborder="0" scrolling="no" height="90px"></iframe>
            <div class="solutions alerta" style="border:1px solid;">
                    <img src="../images/Icono_Error_Amarillo_40x40.gif" alt="alerta" class="solutions" style="float:left" />
                    <p class="textos"><%=text(1) %> </p>
                </div>
            <div style="clear:both;"></div>
            
            <div >
                <h2 class="titulo1"><%=text(2)%></h2>            
                <p><%=text(3)%></p>
                <p><%=text(4)%></p>
                <p><%=text(5)%></p>
                <p><%=text(6)%></p>
                <p><span class="subtitulo"><%=text(7) %></span> <%=text(8)%></p>
                <p><%=text(9)%></p>
                <p><%=text(10)%></p>
                <p><span class="subtitulo"><%=text(11) %> </span><%=text(12)%></p>
                <p><%=text(13)%></p>
                <p><%=text(14)%></p>
                <div class="solutions" style="width:80%;border:2px solid;margin:1em auto;padding:1em;text-align:center;" >
                    <p class="textos"><strong><%=text(15)%><%=getNavegadorNombre()%></strong></p>
                    <p class="textos" style="text-align:left;margin:0 8em;"><%=instrucciones%></p>
                    <p><img src="<%=imagen%>" width="60%" alt="Explorer"  /></p>
                </div>
                <p style="text-align:center;"><a href="javascript:window.history.back();" ><%=text(16) %></a></p>
            </div>
            
           
        </div>
    </body>
</html>
