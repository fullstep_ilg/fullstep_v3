﻿var rObjeto

var str

str=""

str+="A.lnkArbol:link {color:black;font-size:10px;font-weight:normal;text-decoration:none;};"
str += "A.lnkArbol:active {color:black;font-size:10px;font-weight:normal;text-decoration:none};"
str += "A.lnkArbol:visited {color:black;font-size:10px;font-weight:normal;text-decoration:none};"
str += "A.lnkArbol:hover {color:black;font-size:10px;font-weight:bold;text-decoration:none};"

stySelect = "1px dashed blue"
styUnSel = "1px"
document.write("<style>" + str + "</style>")

document.write("<div id=divNada name=divNada></div>")

function nada()
{
return
}

var curSelected
curSelected=null

function Rama (id,cod,texto,expandido,hojascargadas,img,color,chequeable,multiple,async,carpeta,funcion,selected)
{

this.id = id
this.cod = cod
this.text = "<nobr>" + texto + "</nobr>"
this.hojas = new Array()
this.hojasCargadas = hojascargadas
this.parent=null
this.expanded = expandido
this.imagen = img;

if (color!=null)
	{
	this.color = color;
	}
else
	{
	this.color = "DarkGoldenrod"
	}
this.chequeable=chequeable
this.checked=false
this.multiple=multiple
this.async = async
this.carpeta = carpeta
this.selected = selected

if (this.selected==true)
	{
	if (curSelected!=null)
		{
		curSelected.selected=false
		if(document.getElementById("tdT" + curSelected.id))
		    document.getElementById("tdT" + curSelected.id).style.border = 'styUnSel'
		}
	curSelected=this
	}
		
this.funcion=funcion	

this.add = addRama
this.write = writeRama
this.asyncWrite = asyncWriteRama
this.expcont = expcontRama
this.expandir = expandirRama
this.contraer =contraerRama
this.cargarHojas = cargarHojasRama
this.check = checkRama
this.nested = nestedRama
this.select = selectRama

this.click = clickRama
}

function addRama (nRama)
{
	var i = this.hojas.length
	this.hojas[i]=new Object()
	this.hojas[i]=nRama;
	this.hojas[i].parent=this
	this.hojas[i].multiple=this.multiple
	if (this.hojas[i].async==null)
		{
		this.hojas[i].async=this.async
		}
	
	if (this.id!="Root" && this.chequeable && this.multiple)
		{
		this.hojas[i].checked=this.checked
		}
}

function writeRama(parentDiv)
{
	
	var str
	var schk
	var masmenos
	var img

	if (this.imagen==null)
		{
		if (this.carpeta==null)
			{
			//img="<font size=2 face=wingdings color=" + this.color + "><strong>1</strong></font>"
			img="<img align=baseline border=0 src=../images/carpeta" + this.color + ".gif>"
			}
		else
			{
			//img="<font size=3 face=wingdings color=black><strong>" + this.carpeta + "</strong></font>"
			img="<img align=baseline border=0 src=../images/check.gif>"
			}
		}
	else
		{
		img="<img border=0 src=" + this.imagen.src + ">"
		}





	sOnclick = ' onclick="return rama' + this.id + '.select()"'
	
	if (this.chequeable)
		{
		
		schktxt= "<a id=aText'+ this.id +'  class=lnkArbol href=\"javascript:nada()\" " + sOnclick + ">" + this.text + "</A>"
		schkimg= img 
		if (!this.checked)
			{
			schk='<td><input name="chkRama' + this.id + '" class="negro checkbox" type=checkbox UNCHECKED onclick="rama' + this.id + '.check()"></td>'
			}
		else
			{
			schk='<td><input name="chkRama' + this.id + '" class="negro checkbox" type=checkbox CHECKED onclick="rama' + this.id + '.check()"></td>'
			}
		}
	else
		{
		schk=""
		schktxt= '<a id=aText'+ this.id +' class=lnkArbol  href="javascript:nada()" ' + sOnclick + ' >' + this.text + '</A>'
		schkimg= img 
		}

	var pStilo
	if (this.selected!=true)
		{
		pStilo=" style ='border:" + styUnSel + "'"
		}
	else
		{
		pStilo=" style ='border:" + stySelect + "'"
		}
		
	if (this.hojas.length>0 || this.hojasCargadas==false)
		{
		if (this.expanded)
			{
			masmenos="<font size=2 face=courier>-</font>"
			}
		else
			{
			masmenos="<font size=2 face=courier>+</font>"
			}
		str = '<DIV style="position:relative;" name=divTree' + this.id + ' id=divTree' + this.id + '><table border=0 cellpadding=2 cellspacing=0><tr><td><a id=ranc' + this.id + ' class=negro href="javascript:nada()" onclick="rama' + this.id + '.expcont()">' + masmenos + '</a></td><td width=20px>' + schkimg + '</td>' + schk +'<td ' + pStilo + ' name=tdT' + this.id + ' id=tdT' + this.id + '>' + schktxt + '</td></tr></table></DIV>'
		
		}
	else
		{
		masmenos = "&nbsp;"
		str = '<DIV style="position:relative;" name=divTree' + this.id + ' id=divTree' + this.id + '><table border=0 cellpadding=2 cellspacing=0><tr><td><font face=courier>&nbsp</font></td><td width=20px>' + schkimg + '</td>' + schk + '<td  ' + pStilo + ' name=tdT' + this.id + ' id=tdT' + this.id + '>' + schktxt + '</td></tr></table></DIV>'
		}
	if (parentDiv==null)
		{
		document.getElementById("divArbol").innerHTML=str
		}
	else
		{
		    document.getElementById('divExp' + parentDiv.id).innerHTML = document.getElementById('divExp' + parentDiv.id).innerHTML + str 
		}

}

function expcontRama()
{
	var i
	if (this.expanded==false)
		{
		this.expanded=true
		this.expandir()
		}
	else
		{
		this.contraer()
		}
}


function expandirRama()
{
var i
if (this.expanded)
	{
	if (this.hojasCargadas==false)
		{
		this.cargarHojas()
		if (this.async)
			{
			return
			}
		}
	if (this.hojas.length>0)
		{
		    document.getElementById('ranc' + this.id).innerHTML = '<font size=2 face=courier>-</font>'

		var str='<DIV style="position:relative;top:0;left:30px;" id=divExp' +this.id + '></DIV>'
		document.getElementById('divTree' + this.id).innerHTML = document.getElementById('divTree' + this.id).innerHTML + str
		for (i=0;i<this.hojas.length;i++)
			{
			this.hojas[i].write(this)
			this.hojas[i].expandir()
			}
		}
	}
}


function contraerRama()
{
var i
var inhtml
var inhtmlexp
var newhtml
var str
document.getElementById('ranc' + this.id).innerHTML = '<font size=2 face=courier>+</font>'
if (this.hojas.length>0)
	{

 	if (navigator.userAgent.indexOf('MSIE 5.5')>0)
		{

		inhtml = document.getElementById('divTree' + this.id).innerHTML
		inhtmlexp = '<DIV style="position:relative;top:0;left:30px;" id=divExp' + this.id + '>' + document.getElementById('divExp' + this.id).innerHTML + '</DIV></DIV>'
		newhtml=inhtml.substr(0,inhtml.length-inhtmlexp.length) + '</DIV>'
		var n
		n=eval(this.nested())
		n.innerHTML=newhtml
		//document.getElementById['divTree' + this.id ].innerHTML = newhtml
		
		}
	else
		{
		    document.getElementById('divExp' + this.id).outerHTML = ''
		}
	}
this.expanded=false
}

function cargarHojasRama()
{
var esRama
var str
var schk
var schktxt
var schkimg
var img
var i
esRama = obtener(this.id)
if (this.async)
	{
	return
	}

this.hojasCargadas=true

if (!esRama)
	{
	if (this.image==null)
		{
		if (this.carpeta==null)
			{
			//img="<font size=2 face=wingdings color=" + this.color + ">1</font>"
			img="<img align=baseline border=0 src=../images/carpeta" + this.color + ".gif>"
			}
		else
			{
			//img="<font size=3 face=wingdings color=black>" + this.carpeta + "</font>"
			img="<img align=baseline border=0 src=../images/check.gif>"
			}
		}
	else
		{
		img="<img border=0 src=' + this.imagen.src + '>"
		}
	sOnclick = ' onclick="return rama' + this.id + '.select()"'
		
	if (this.chequeable)
		{
		schktxt= '<a  id=aText'+ this.id +' class=lnkArbol href="javascript:nada()" ' + sOnclick + '>' + this.text + '</A>'
		schkimg= img 
		if (!this.checked)
			{
			schk='<td><input name="chkRama' + this.id + '" class="negro checkbox" type=checkbox  UNCHECKED onclick="rama' + this.id + '.check()"></td>'
			}
		else
			{
			schk='<td><input name="chkRama' + this.id + '" class="negro checkbox" type=checkbox CHECKED onclick="rama' + this.id + '.check()"></td>'
			}
		}
	else
		{
		schk=""
		schktxt= '<a  id=aText'+ this.id +' class=lnkArbol  href="javascript:nada()" ' + sOnclick + ' >' + this.text + '</A>'
		schkimg= img 
		}

	var pStilo
	if (this.selected!=true)
		{
		pStilo=" style ='border:" + styUnSel + "'"
		}
	else
		{
		pStilo=" style ='border:" + stySelect + "'"
		}

	str = '<table border=0 cellpadding=2 cellspacing=0><tr><td><font face=courier>&nbsp</font></td><td width=20px>' + schkimg + '</td>' + schk +'<td ' + pStilo + ' name=tdT' + this.id + ' id=tdT' + this.id + '>' + schktxt + '</td></tr></table>'

	document.getElementById('divTree' + this.id).innerHTML = str
	}
}

function checkRama()
{
if (this.checked)
{
    this.checked = false
	desseleccionar(this.id)
	}
else
{
    var marcar = seleccionar(this.id)
    if (marcar == undefined || marcar == true)
    {
        this.checked = true
    }
	}
}

function asyncWriteRama(esRama)
{
this.hojasCargadas=true

if (!esRama)
	{
	if (this.image==null)
		{
		if (this.carpeta == null)
			{
			//img="<font size=2 face=wingdings color=" + this.color + ">1</font>"
			img="<img align=baseline border=0 src=../images/carpeta" + this.color + ".gif>"
			}
		else
			{
			//img="<font size=3 face=wingdings color=black>" + this.carpeta + "</font>"
			img="<img align=baseline border=0 src=../images/check.gif>"
			}
		}
	else
		{
		img="<img border=0 src=' + this.imagen.src + '>"
		}
	if (this.chequeable)
		{
		schktxt= '<a  id=aText'+ this.id +' class=lnkArbol  href="javascript:nada()" ' + sOnclick + ' >' + this.text + '</A>'
		schkimg= img 
		if (!this.checked)
			{
			schk='<td><input name="chkRama' + this.id + '" class="negro checkbox" type=checkbox  UNCHECKED onclick="rama' + this.id + '.check()"></td>'
			}
		else
			{
			schk='<td><input name="chkRama' + this.id + '" class="negro checkbox" type=checkbox  CHECKED onclick="rama' + this.id + '.check()"></td>'
			}
		}
	else
		{
		schk=""
		schktxt= '<a  id=aText'+ this.id +' class=lnkArbol  href="javascript:nada()" ' + sOnclick + ' >' + this.text + '</A>'
		schkimg= img 
		}
	var pStilo
	if (this.selected!=true)
		{
		pStilo=" style ='border:" + styUnSel + "'"
		}
	else
		{
		pStilo=" style ='border:" + stySelect + "'"
		}

	str = '<table border=0 cellpadding=2 cellspacing=0><tr><td><font face=courier>&nbsp</font></td><td width=20px>' + schkimg + '</td>' + schk +'<td ' + pStilo + ' name=tdT' + this.id + ' id=tdT' + this.id + '>' + schktxt + '</td></tr></table>'

	document.getElementById('divTree' + this.id).innerHTML = str
	}

if (this.hojas.length>0)
	{
	    document.getElementById('ranc' + this.id).innerHTML = '<font size=2 face=courier>-</font>'

	var str='<DIV style="position:relative;top:0;left:30px;" id=divExp' +this.id + '></DIV>'
	document.getElementById('divTree' + this.id).innerHTML = document.getElementById('divTree' + this.id).innerHTML + str
	for (i=0;i<this.hojas.length;i++)
		{
		this.hojas[i].write(this)
		this.hojas[i].expandir()
		}
	}
}




function nestedRama()
	{
	if (this.id=="Root")
		{
		    return ("document.getElementById('divArbol').document.getElementById('divTreeRoot')")
		}
	else
		{
		    return (this.parent.nested() + ".document.getElementById('divTree" + this.id + "')")
		}
	}


// Comprueba que la rama esté visible o no
function dessel(rama)
{
if (rama.parent.id!="Root")
	{
	if (rama.parent.expanded)
		{
		return (true & dessel(rama.parent))
		}
	else
		{
		return false
		}
	}
else
	{
	return true
	}
}	

//''' <summary>
//''' Recuadra la rama selecionada
//''' </summary>
//''' <remarks>Llamada desde: clickRama() y es la funcion asociada al select del objeto rama; Tiempo máximo: 0</remarks>
function selectRama() {

    if (PuedesClicarItems == 0) {
        //esta con precambiarMoneda o CambiarMoneda. 
        return false
    }

    if (this.selected!=true)
	{
	    this.selected=true
	    if (curSelected!=null)
		{
		    curSelected.selected = false            
            if (document.getElementById("tdT" + curSelected.id))
		        document.getElementById("tdT" + curSelected.id).style.border = styUnSel
		}
	    curSelected=this
	    document.getElementById("tdT" + this.id).style.border = stySelect
	}
	if (this.funcion!=null)
		{
		eval(this.funcion +  "('" + this.id + "')")
		}

    document.getElementById("divNada").write = "<!-- Un comentario -->"

    return
}

//Selecciona o Desselecciona una rama incluyendo todas las hojas
function selHojas(rama,valor)
{
var i
var chkSel
var expanded

expanded = dessel(rama)
if (expanded)
	{
	chkSel = eval("document.forms['frmArbol'].chkRama" + rama.id )
	chkSel.checked=valor
	}
rama.checked=valor
for (i=0;i<rama.hojas.length;i++)
	{
	if (rama.hojas[i].chequeable)
		{
		selHojas(rama.hojas[i],valor)
		}
		
	}

}

//Desselecciona una rama desde la raiz hasta la rama
function desselParent(rama)
{
var chkDesel
var expanded
if (rama.id!='Root' && rama.chequeable)
	{
	rama.checked=false
	expanded = dessel(rama)
	if (expanded)
		{
		chkDesel = eval("document.forms['frmArbol'].chkRama" + rama.id )
		chkDesel.checked=false
		}
	desselParent(rama.parent)
	}
return
}

function clickRama()
{
selectRama()
}