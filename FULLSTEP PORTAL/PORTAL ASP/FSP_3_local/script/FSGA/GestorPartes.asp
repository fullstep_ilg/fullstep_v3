﻿<%@ Language=VBScript %>
<!--#include file="../common/XSS.asp"-->
<%
''' <summary>
''' Gestiona los partes
''' </summary>
''' <remarks>Llamada desde: common\menu.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request("Idioma")
Idioma = trim(Idioma)
if Idioma="" then
	Idioma = "SPA"
end if

set oRaiz = createobject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")    
i = oraiz.Conectar(Application("INSTANCIA"))
if i = 1 then 
    set oRaiz = nothing
    Response.Redirect application("RUTASEGURA") & "script/imposibleconectar.asp?Idioma=" & Idioma
    Response.End 
end if

IPDir=Request.ServerVariables("LOCAL_ADDR")
PersistID=Request.Cookies("SESPP")
	
set oRaizVal =  oraiz.ValidarSesion(Request.Cookies("USU_SESIONID"),IPDir,PersistID,false,false,0,"") 
set oraiz = nothing

if oRaizVal is nothing then    
	Response.Redirect Application ("RUTASEGURA") & "script/imposibleconectar.asp?Idioma=" & Idioma
	Response.End 
end if

 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title></title>
	<meta name="GENERATOR" content="Microsoft FrontPage 4.0" />
	<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp" />
	<script type="text/javascript" src="../js/Silverlight.js"></script>    
	<script type="text/javascript" src="../common/menu.asp"></script>    
	<script type="text/javascript">        dibujaMenu(7)</script>
    <script language="JavaScript" type="text/JavaScript">
        /*''' <summary>
        ''' Iniciar la pagina.
        ''' </summary>     
        ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
		function init() {
		    document.getElementById('tablemenu').style.display = 'block';
		}
	</script>
</head>
<body onload="init()">
	<div style="margin-top:10px; z-index:1;">
        <object data="data:application/x-silverlight-2," type="application/x-silverlight-2" width="100%" height="100%">
		  <param name="source" value="FSGAClient.xap"/>
		  <param name="onError" value="onSilverlightError" />
		  <param name="background" value="white" />
		  <param name="minRuntimeVersion" value="4.0.50826.0" />
		  <param name="autoUpgrade" value="true" />  
          <param name="windowless" value="true" />      
          <param name="initParams" value="SessionId=<%=Request.Cookies ("USU_SESIONID")%>,Idioma=<%=Request("Idioma")%>,Theme=<%=application("NOMPORTAL")%>"/>  
		  <a href="http://go.microsoft.com/fwlink/?LinkID=149156&v=4.0.50826.0" style="text-decoration:none">
			  <img src="http://go.microsoft.com/fwlink/?LinkId=161376" alt="Get Microsoft Silverlight" style="border-style:none"/>
		  </a>
		</object>
	</div>    
</body>
</html>

