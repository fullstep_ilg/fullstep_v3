﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<%
if Request("disconnect1")="true" then
    ''' XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    ''' 1
    ''' <summary>
    ''' Cierra la sesion del usuario obligandole a logearse de nuevo
    ''' </summary>
    ''' <remarks>Llamada desde: common\menu.asp ; Tiempo máximo: 0,2</remarks>
 
    if Request.ServerVariables("CONTENT_LENGTH")>0 then
        dim oRaiz
        set oRaiz = createobject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
	
        if (oRaiz.Conectar(Application("INSTANCIA"))) = 1 then
	        set oraiz = nothing
	        set validarUsuario=nothing
	        Response.Redirect Application ("RUTASEGURA") & "script/imposibleconectar.asp?Idioma=" & Idioma
	        Response.End 
        end if
    
        oRaiz.DesconectarUsuario Request.Cookies("USU_SESIONID"),Request.ServerVariables("LOCAL_ADDR"),Request.Cookies("SESPP")

        'Aunque parezca extraño, para BORRAR la cookie debes darle algún contenido.         
        Fecha=Date
        sDia=cstr(day(Fecha))
        sUrte = cstr(year(Fecha))

        sDia = left("00",2-len(sDia)) & sDia

        select case Weekday(Fecha)
        case 1:
            sDiaWeek = "Sun, "
        case 2:
            sDiaWeek = "Mon, "
        case 3: 
            sDiaWeek = "Tue, "
        case 4: 
            sDiaWeek = "Wed, "
        case 5: 
            sDiaWeek = "Thu, "
        case 6:
            sDiaWeek = "Fri, "
        case 7:
            sDiaWeek = "Sat, "
        end select 

        select case month(Fecha)
        case 1:
            sMes= "Jan"
        case 2:
            sMes= "Feb"
        case 3:
            sMes= "Mar"
        case 4:
            sMes= "Apr"
        case 5:
            sMes= "May"
        case 6:
            sMes= "Jun"
        case 7:
            sMes= "Jul"
        case 8:
            sMes= "Aug"
        case 9:
            sMes= "Sep"
        case 10:
            sMes= "Oct"
        case 11:
            sMes= "Nov"
        case 12:
            sMes= "Dec"
        end select
                    
        VisualizacionFecha = sDiaWeek + sDia + "-" + sMes + "-" + sUrte + " 00:00:00 GMT"    

        Response.AddHeader "Set-Cookie","USU_SESIONID=" & "disconnet" & "; path=/; HttpOnly; Expires=" + VisualizacionFecha 
        Response.AddHeader "Set-Cookie","USU_MOSTRARFMT=" & "0"  & "; path=/; HttpOnly; Expires=" + VisualizacionFecha 

        Response.AddHeader "Set-Cookie","USU_DECIMALFMT=" & ","  & "; path=/; HttpOnly; Expires=" + VisualizacionFecha 
        Response.AddHeader "Set-Cookie","USU_THOUSANFMT=" & "."  & "; path=/; HttpOnly; Expires=" + VisualizacionFecha 
        Response.AddHeader "Set-Cookie","USU_DATEFMT=" & "dd/mm/yyyy"  & "; path=/; HttpOnly; Expires=" + VisualizacionFecha 
        Response.AddHeader "Set-Cookie","USU_PRECISIONFMT=" & "2" & "; path=/; HttpOnly; Expires=" + VisualizacionFecha 

        Response.AddHeader "Set-Cookie","USU_TIPOMAIL=" & "1" & "; path=/; HttpOnly; Expires=" + VisualizacionFecha 
        Response.AddHeader "Set-Cookie","USU_IDIOMA=" & "SPA" & "; path=/; HttpOnly; Expires=" + VisualizacionFecha 

        Response.AddHeader "Set-Cookie","USU_CSRFTOKEN=""; path=/; HttpOnly; Expires=" + VisualizacionFecha 
        Response.AddHeader "Set-Cookie","USU_CADPASSWORD=0; path=/; HttpOnly; Expires=" + VisualizacionFecha 
        
        Response.AddHeader "Set-Cookie","SESPP=" & "SESPP" & "; path=/; HttpOnly; Expires=" + VisualizacionFecha

        Response.AddHeader "Set-Cookie","EMPRESAPORTAL=""; path=/; HttpOnly; Expires=" + VisualizacionFecha
        Response.AddHeader "Set-Cookie","NOM_PORTAL=""; path=/; HttpOnly; Expires=" + VisualizacionFecha
    end if
    
    ''' XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    ''' 2
    ''' Vamos a una página .NET para eliminar todos los datos de sesión de usuario de .NET
    Response.Redirect Application("RUTASEGURA") & "script/PMPortal/disconnect/disconnect.aspx?Qry=" & sQry & "&Idioma=" & Idioma
end if
if Request("disconnect2")="true" then
    if Request("Qry") <>"" then
        sQry = Request("Qry")
    End if
	if trim(Request.ServerVariables("HTTPS"))="on" then
		Response.Redirect Application ("RUTASEGURA") & "script/proveedor?" & sQry
	else
        if Application("LOGIN_EXT_VENT_NUEVA") = "0" then
		    Response.Redirect Application ("RUTANORMAL") & "custom/" & Application ("NOMPORTAL") & "/public/default.asp?" & sQry
        else
            Dim Idi
	        Idi = Request("Idioma")
	        if Idi = "" then
		        Idi = "SPA"
	        end if
            Dim Den
            Den=devolverTextos(Idi,137)
            %>         
            <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                <link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp"/>
                <script src="../common/formatos.js"></script>
            </head>
            <body>
            <H1>
            <%=den(1)%>
            </H1>
            <P>
            <BR>
            <%=den(2)%>
            <BR><BR>
            <center><a href ="javascript:void(null)" onclick ="cerrar()"><%=den(3)%></a></center>
            </body>
            </html>
        <%
        end if
	end if
end if
%>
<script>
function cerrar() {
    window.close()
}
</script>


