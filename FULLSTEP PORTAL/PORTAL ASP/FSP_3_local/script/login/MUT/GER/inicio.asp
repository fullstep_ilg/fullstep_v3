<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="css/responsive.css">  
<title>::KONTRATATU MUTUALIAREKIN::</title>
<!--[if lte IE 9]>
<link href="../estilos_ie9.css" rel="stylesheet" type="text/css" />
<![endif]-->
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
}
-->
</style>
</head>
<script>
dibujaMenu(1)
</script>
<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function Init() {
    document.getElementById('tablemenu').style.display = 'block';

	MM_preloadImages('../images/icono_docs_sm.gif','../images/icono_docs_sm.gif')
	p = window.top.document.getElementById("frSet")
	vRows = p.rows
	vArrRows = vRows.split(",")
	vRows = vArrRows[0] + ",*,0,0"
	p.rows = vRows

}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">

 
      
<section id="servicios"> 
            
            <div class="container-inicio">
            
                  <div class="columna-izda">
                  <div class="cuerpo-menu-inicio">
                    <ul>
       	  <li> <a href="docs/Mutualia-Hornitzailearen gida.pdf" target="_blank">Ziurtagiriak nola kudeatu           </a></li>
				 <li> <a href="docs/Mutualia-Lankidetzarako eskuliburua.pdf" target="_blank">Lankidetza </a></li>
            <li><a href="docs/Mutualia-Baldintza teknikoak.pdf" target="_blank">Betebehar teknikoak</a></li>
            <li><a href="docs/Mutualia-Datuak mantentzea.pdf" target="_blank">Datuak mantentzea</a></li>
        </ul>
            <div class="limpia"></div>
            <div class="manual_consulta_inicio">
            <img src="../img/ayuda_2.jpg" width="183" height="171" />
<p>
Atariarekin lan egiteko zalantzaren bat edo zailtasunen bat baduzu, jar zaitez gurekin harremanetan <a href="mailto: hornitzaileak@mutualia.es" title=" hornitzaileak@mutualia.es" class="azul"> hornitzaileak@mutualia.es</a></p>
                  
                  
                    </div>
               
           	  </div>
                 </div>
  
                    <div class="centro inicio">
                      <div style="text-align">
                        
                        <h4>ONGI ETORRI KONTRATATU MUTUALIAREKIN PLATAFORMARA</h4>
                        
                     <p> Goiko aldean ikusiko duzun menuko aukeren bitartez sar zaitezke nahi duzun ataletara.  </p>
			<br /><br />
           <ul class="lista-enum">
        <li><p><b>Kalitatea:</b> kudea itzazu zure kalitate-ziurtagiriak, adostasun ezak, eta sar zaitez zure kalitate-puntuazioetara.</p></li>
      
        <li><p><b>Lankidetza:</b> zure zalantzak argitu edota edozein informazio eskatu atari honetatik bertatik.</p></li>
     
        <li><p><b>Zure datuak/ zure konpainia:</b> nahi baduzu alda ditzakezu zure enpresako datuak, erabiltzaileak, edota zure enpresa homologatuta dagoen jarduera-arloak. </p></li>
      </ul>
      
     
			<!--
           
            <div class="raya_separa"></div>
            <p>     Si es la primera vez que va a realizar una oferta a trav&eacute;s del portal, siga atentamente los siguientes pasos:<br></p>
			<ol class="lista-enum">
                <li><p>Pulse en "solicitudes&quot;</b> para ver las peticiones de ofertas que tiene abiertas su empresa. </p></li>
                <li><p>Seleccione la solicitud de oferta</b> a la que quiera responder, pulsando sobre el c&oacute;digo de la misma. </p></li>
                 <li><p> Realice su oferta completando toda la informaci&oacute;n necesaria: desde el &aacute;rbol de navegaci&oacute;n que encontrar&aacute; en la parte izquierda, podr&aacute; desplazarse por los distintos apartados que conforman la oferta. Para introducir los precios deber&aacute; ir al apartado "items/precios". Recuerde introducir el plazo de validez de la oferta, en el apartado "Datos Generales de la oferta".  </p></li>
                  <li><p>Comunique su oferta pulsando sobre el bot&oacute;n de enviar</b> <IMG height=14 src="../img/sobre_tr.gif" width=30 align=absBottom></p></li>

			</ol>-->
                      	</div>
                    </div>
                    
                          </div>   
         	</div>
              
    	</section>
<!-- INICIO PAGINA ANTIGUA-->
<!-- 
<div id="inicio-left">
  <h1><span class="blue">Bienvenido al Portal de Proveedores</span></h1>
  <!--<ul class="square">
  <li><p>Puede acceder a las distintas áreas pulsando las opciones de menú situadas en la parte superior.</p></li>
  <ul>
    <li><span class="blue">Sus datos/ su compa&ntilde;&iacute;a :</span> si lo desea puede modificar los datos de su empresa, usuarios, así como las áreas de actividad en las que su empresa se encuentra homologada.  </li>
    <li><span class="blue">Homologación :</span> procedimiento para darse de alta como proveedor de Banco Popular Español, S.A. </li>
    <li><span class="blue">Facturación :</span> visor de facturas donde podrá consultar el estado de las mismas </li>
   <li><span class="blue">Solicitudes :</span> acceda a las solicitudes (RFQ y otras solicitudes) y gestiónelas desde este mismo apartado.   
      <li><span class="blue">Pedidos :</span> gestione desde aquí los pedidos que le han realizado los compradores, siga su evolución y acceda a su histórico.   </li></li>
  </ul>
<br><br>

<li class="padding">  Si es la primera vez que va a realizar una oferta a través del portal, siga atentamente los siguientes pasos (cuando la opción de “solicitudes” esté operativa):</li>
  <ul>
  <li><span class="rojo">Pulse en "solicitudes"</span> para ver las peticiones de ofertas que tiene abiertas su empresa. </li><br>
  <li><span class="rojo"> Seleccione la solicitud de oferta a la que quiera responder </span>, pulsando sobre el código de la misma.</li><br>
  <li><span class="rojo">Realice su oferta completando toda la información necesaria:  </span> desde el árbol de navegación que encontrará en la parte izquierda, podrá desplazarse por los distintos apartados que conforman la oferta. Para introducir los precios deberá ir al apartado "items/precios". Recuerde introducir el plazo de validez de la oferta, en el apartado "Datos Generales de la oferta".</li><br>
  <li> <span class="rojo">Comunique su oferta </span>  pulsando sobre el botón de enviar <IMG height=14 src="../img/sobre_tr.gif" width=30 align=absBottom></li>
 </ul>
 </ul>
</div>
<div id="inicio-right">
<h2>INSTRUCCIONES</h2>
<p>Desde esta página puede descargarse los siguientes manuales:</p>
<ul class="square">
<li><a href="docs/FSN_MAN_ATC_Cómo ofertar.pdf" target="_blank">Cómo ofertar</a></li>
<li><a href="docs/FSN_MAN_ATC_Requisitos_tecnicos.pdf" target="_blank">Requisitos técnicos</a></li>
<li><a href="docs/FSN_MAN_ATC_Mantenimiento_de_datos.pdf" target="_blank">Mantenimiento de datos</a></li>
</ul>
<div id="img-atc"><img src="../img/imagen-ATC.jpg"></div>
<p>Si tiene alguna duda o dificultad para operar con el Portal, póngase en contacto con nostros a través del mail: <a href="mailto: atencionalcliente@fullstep.com" class="contacto"> <span class="contacto">atencionalcliente@fullstep.com</span></a></p>
</div>
-->
<!-- FIN PAGINA ANTIGUA-->

</body>
</html>
