﻿<%@ Language=VBScript %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/fsal_1.asp"-->
<meta charset="utf-8" />  
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />  
        <meta name="viewport" content="width=device-width, initial-scale=1" />  
        
        <title>Mutualia</title>
        
        <meta name="title" content=""/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        
        <link rel="canonical" href="/" />

		<link href="../css/reset.css" rel="stylesheet" type="text/css" />        
    	<link href="../css/font-awesome.min.css" rel="stylesheet" />  
        
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
        <link href="../css/style.css" rel="stylesheet" />  
        <link rel="stylesheet" type="text/css" href="../css/responsive.css">  
        <script src="../js/jquery-1.9.0.js"></script>
<script src="../js/jquery-1.9.0.min.js"></script>

<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
<script src="../../../common/formatos.js"></script>
<script language="JavaScript" src="../../../common/ajax.js"></script>
<!--#include file="../../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=GER", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body onload="init()">

<header id="heading"> 
        
        	<div class="container"> 
                       
             	<div class="logotipo">
            		<a href="http://www.mutualia.es" title="Mutualia" target="_blank"><img src="../img/mutualia.png" alt="Mutualia" width="210" height="71" /></a>
                </div> 

               <nav>                      
                    <ul class="idiomas">
                         <li><a href="../registro.asp" title="CAST"><strong>Castellano</strong> | </a></li> 
                        <li><a href="#" title="EUSK"><strong>Euskera</strong> | </a></li>
                       <!-- <li><a href="#" title="ENG"><strong>ENG</strong></a> </li>-->
                    </ul> 
                  </nav> 
                   	  
                     
    		
            
           <h1>KONTRATATU MUTUALIAREKIN</h1>      
                     
    		</div>
        
        
            <div class="header">
            
                 
                <div class="menu">  
        
                    <a class="menu-rsp" href="#" title="Men&uacute;"><span>Men&uacute;</span></a>
        
                    <div class="menu-container">
                    
                         <ul>
                            <li class="menu-padre activo"><a href="default.asp" title="Inicio">HASIERA</a> </li>   <span class="oculta">|</span>
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ger/quienes-somos.html" title="Quiénes somos">Nor gara</a></li>  <span class="oculta">|</span> 
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ger/que-buscamos.html" title="Qué buscamos">Zer nahi dugun</a></li> <span class="oculta">|</span>  
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ger/politicas.html" title="Políticas">Politikak</a></li> <span class="oculta">|</span> 
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ger/ayuda.html" title="Ayuda">LAGUNTZA</a></li>
                            
                        </ul>
                                      <!--       
                        <ol>
                            <li class="activo"><a href="#" title="CAST">Castellano</a></li> 
                            
                            <li><a href="#" title="ENG">English</a> </li>
                        </ol> -->
                    
                   	</div>    
            
                </div> 
                
            </div>
            
        </header>     
    	<section id="home"> 
            
            <div class="container">
            
           		<div class="text">
                
                	
                    <div class="columna sol">
                        <div class="imagen">
                      <img src="../img/shim.gif" width="100">
                         </div>  
                        
                   	</div>
                    
                    <div class="centro sin solicitud">
                      <div>
                        
                        <h2><em>HORNITZAILEEN ERREGISTROA</em></h2>
        <div class="int">
        	<p>Izena emateko prozesuarekin jarraitzeko, ezinbestekoa da KONTRATATU MUTUALIAREKIN
plataformara sartzeko baldintzak irakurtzea eta onartzea.</p>
            <div class="caja_registro">
                 <h4>GARRANTZITSUA, IRAKURRI ARRETAZ</h4><br>

<h3>MUTUALIA - Gizarte Segurantzarekin lankidetza duen 2 zb.ko mutua</h3>
                        
                        <h4>KONTRATATU MUTUALIAREKIN - Lege-oharra </h4>

                       <strong class=" blue">Informazio Orokorra</strong><br />

Ohar hau nahitaez irakurri eta onartu beharrekoa da, bertan arautzen baitira atari honen erabiltzaileak diren enpresek eta pertsonek (aurrerantzean, ERABILTZAILEA edo HORNITZAILEA) aintzat hartu eta bete behar dituzten erabilera-baldintzak. Garrantzitsua da oharra arretaz irakurtzea, hain zuzen ere hara sartzeak eta bertako edukiak eta baliabideak ikusi edo erabiltzeak adierazten baitu erabiltzaileak irakurri eta ontzat ematen dituela, inolako erreserbarik gabe, LEGE-OHAR honetako baldintzak, eta horrek berekin dakar dokumentu honetan azaltzen diren ERABILERA-BALDINTZA ORORRAK onartzea.<br><br>

Erabiltzaileak ezingo du atari honetan izena eman Lege Ohar honekin eta bere eduki osoarekin ados dagoela adierazi gabe. Atari honetan sartu eta hura erabiltzen denean, ulertuko da erabiltzailea ados dagoela, espresuki, oso-osorik eta erreserbarik gabe, Lege-Ohar honen eduki osoarekin.<br><br>

Era berean, atari honetan izena eman duen pertsona izango denez berak ordezkatzen duen enpresako Erabiltzaile Nagusia, beharturik dago, Lege Ohar honi men eginez, arduratzera berak ordezkatzen duen enpresan alta ematen duten erabiltzaile guztiek ere hau bete dezaten , eta MUTUALIA ezein erantzukizunetik libre geratzen da erabiltzaile horiek enpresari eragin diezaioketen balizko kalteengatik edo atarian eginiko beste edozein jarduketarengatik.<br><br>

Jakinarazten da webgunearen titularra dela MUTUALIA, GIZARTE SEGURANTZAREKIN LANKIDETZA DUEN 2 ZB.KO MUTUA, eta haren egoitza operatiboa dela Henao kalea, 26; BILBO, PK 48009.<br><br>

MUTUALIAk, bere jarduketaren esparruan, hainbat eduki jartzen ditu erabiltzaileen esku, haiek zentzuz erabil ditzaten eta informazio-beharrak ase ditzaten, hornitzaileekin eta kontratistekin harremanak errazteko helburuarekin betiere.<br><br>

Erabiltzailea fede onaren printzipioa aintzat hartuz sartzen da atari honetan, eta ordena publikoaren arauak, erabilerako baldintza orokor hauek eta, kasuari dagokionean, aplikagarriak izan daitezkeen baldintza bereziak betez. Debekatuta dago atari honen erabiltzaileak eskuratzen dituen informazioa eta materialak merkataritzarako ustiatzea.<br><br>

Erabiltzailea bere eta ez beste inoren erantzukizunaren pean sartzen da atari honetan, eta betiere hirugarrenei edo MUTUALIAri eragin diezazkiokeen kalteen erantzulea izango da.<br><br>

Erabiltzaileari espresuki debekatzen zaio atari honetan eskaintzen diren zerbitzuak eta edukiak erabiltzea eta lortzea ez bada erabilera-baldintza orokor hauetan jasota dauden prozeduren bidez, eta, hala dagokionean, jarduera edo zerbitzu jakinen bat arautzen duten baldintza berezien bidez (berariaz zehaztuko dira horiek).<br><br>

Erabiltzaileak ematen duen informazioa MUTUALIAren hornitzaileen datu-basean jasoko da. Erabiltzaileak eskubidea du edozein unetan bere datuak kontsultatu eta aldatzeko, edota datu-basean baja emateko. <br><br>

Nolanahi ere, indarrean dagoen legeria betez, batez ere gardentasunari buruzko araudiari eta sektore publikoaren kontratazioari buruzko araudiari men eginez, litekeena da emandako informazioa, edo kontratuak lizitatzeko, prestatzeko eta esleitzeko prozesuetan datu-prozesamenduetatik eratortzen dena, bere osotasunean edo zati batean, argitara eman behar izatea legearen aginduz. Erabiltzaileak, betebehar horren jakitun denez, baimena ematen du hartarako.<br><br>

MUTUALIAk konpromisoa hartzen du datuak sortzen den harremanerako soil-soilik erabiltzeko eta, legearen aginduz ez bada, baimenik gabe hirugarrenei ez emateko. Erabiltzaileak edozein komunikabidetatik, baita telematikotik ere, jaso dezake bere intereseko informazioa. Halaz eta guztiz, banaketa-zerrendan baja eman dezake edozein unetan, salbu eta legearen aginduz edo betebeharrez MUTUALIAk bere datuak gorde behar baditu.<br><br>

Atari honetan ari den HORNITZAILEAk bere adostasuna adierazi eta ontzat ematen du sisteman alta ematen duen egunetik aurrera MUTUALIAko hornitzaile bezala egiten dituen komunikazio eta informazio, eskabide, horniketa, fakturazio eta edonolako harreman guztiak MUTUALIAko hornitzaileen plataforma edo atari honen bidez egin ahal izateko. <br><br>

Erabiltzaile-gako baten eta sarbide-kode (pasahitz) baten bidez egingo da atariko konexioa; hitz horiek pantailan sortuko dira, alta ematerakoan hornitzailearen datuak sartu eta bidaltzen direnean. Zerbitzu honen bidez egindako operazio guztiak, sarbide-kodearekin eta segurtasun-pasahitzarekin egiaztatuta datozenez, baliozkotzat eta eraginkortzat hartuko dira betiere, eta hornitzaileak baimendutakoak eta bidalitakoak direla iritziko da; beraz, pasahitz horien erabilera, atarian alta emanda dauden hornitzaileen sinadura elektronikotzat hartuko da eta, MUTUALIArekin egiten dituen kontratuetarako, eskuzko sinaduraren baliokidetzat joko da.<br><br>

Horretarako, hornitzaile erabiltzaileak ziurtatzen du bere datu-erregistroan agertzen diren datuak egiazkoak eta bere enpresari dagozkionak direla.<br><br>

HORNITZAILEAren erantzukizuna da MUTUALIAri jakinaraztea hornitzaileak eman dituen informazioen edo datu pertsonalen edozein aldaketa.<br><br>

Atariaren eta haren bidez ematen diren zerbitzuen erabilera dela eta MUTUALIA eta HORNITZAILEAren artean sortzen diren harremanak Espainiako legeriaren eta jurisdikzioaren mendekoak izango dira.

<br><br>


<strong class=" blue">Baldintza Orokorrak</strong><br><br>

<p><strong>1. Mutualiarekin kontratatu atariaren xedea</strong> </p>

Mutualiarekin kontratatu ataria da MUTUALIAk bere hornitzaileekin komunikatzeko erabiltzen duen bidea, biltzeko, eskuratzeko edota eskatzeko informazioa, eskabideak, aurrekontuak, dokumentazioa, hala nola egoki deritzon zeinahi informazio/dokumentazio administratibo, tekniko edota komertzial. Aldi berean, MUTUALIAk ataria erabil dezake hornitzailearen interesekotzat jotzen duen informazioa helarazteko.<br><br>

Atariaren eta bertan eskaintzen diren Zerbitzuen erabilerak edota haietarako sarbideak ez dio ematen HORNITZAILEAri inolako eskubiderik han ager daitezkeen marka, izen komertzial edo ikur bereizgarriei dagokienez, haiek MUTUALIArenak edo hirugarrenenak direnean. Hortaz, atariaren Edukiak MUTUALIAren edo hirugarrenen jabetza intelektualekoak dira, eta ezingo dira HORNITZAILEAri lagatakoak direla ulertu. <br><br>
Atariaren eta bertan eskaintzen diren Zerbitzuen erabilerak edota haietarako sarbideak ez dio ematen HORNITZAILEAri inolako eskubiderik han edo MUTUALIAren informazio-sistemetan ager daitezkeen bere datuen iraunkortasun mugagabeari buruz, eta MUTUALIAk ez du bere egiten atari honetako erabiltzaileekin inolako kontratu-betebeharrik zerbitzuaren iraunkortasunerako, eta edozein unetan eman diezaioke baja atariaren edozein erabiltzaileri, arrazoizko zergatiengatik, esate baterako, LEGE-OHAR honetako arauak ez betetzeagatik, MUTUALIAren Balio-Sistema edo Gardentasun irizpide/arauak ez betetzeagatik, eta erabiltzaileak ezingo dio MUTUALIAri eskatu inolako kalte-ordainik halako bajarengatik. <br><br>


<p><strong>2. Hornitzailearen betebeharrak</strong> </p>

Akordio honen xedea da MUTUALIA eta HORNITZAILEAren arteko harremanak arautzea, atariaren erabilpenari dagokion guztirako. Hornitzaileak honako betebehar hauek izango ditu:<br><br>

 a)	Sistemak egoki funtzionatzeko beharrezkoak diren datu guztiak ematea eta eguneratuta izatea, eta haietan izan daiteken edozein aldaketa ahalik azkarren jakinaraztea.<br><br>

b) Egiazkotasuna bermatzea zerbitzuak egiteko beharrezkoak diren formularioak betetzerakoan, edo geroagoko edo etorkizuneko informazio gehigarrietan emandako datuetan eta dokumentuetan. Halaber, HORNITZAILEAk eguneratu egin beharko du emandako informazioa, une bakoitzean bere egoera erreala adieraz dezan. Horregatik, HORNITZAILEA izango da adierazpen okerren edo faltsuen ondorioz MUTUALIAk jasan ditzakeen kalte-galeren erantzule bakarra.<br><br>

c)	Konfidentzialtasun osoa gordetzea HORNITZAILEA eta MUTUALIAren arteko harremanetan sor daitekeen informazio guztiari buruz.<br><br>

d)	Atariaren erabilera informatiko/teknikoetan manipulaziorik ez egitea, eta hura aurreikusitakoez bestelako xedeetarako ez erabiltzea. Bidenabar, HORNITZAILEA ez da sartuko baimenik ez duen atariaren ataletara.<br><br>

e)	Zintzoki betetzea bere konpromisoak, atariaren bidez helarazten zaio informazioari dagokionez. HORNITZAILEAk behar besteko prestasun komertziala eta gardentasuna erakusten ez duenean edo onartu dituen betebeharrak betetzen ez dituenean, MUTUALIAk berezko eskubidea izango du HORNITZAILEA ataritik kanpo ezartzeko aldi baterako edo behin betiko.<br><br>

f)	HORNITZAILEAk onartzen du atariaren bitartez bidaltzen dituen eskaintza/dokumentuek beste edozein paperezko euskarri fisiko tradizionaletan (gutunez, faxez, e.a.) bidalitakoen maila eta baliagarritasun bera dutela.<br><br>

g)	HORNITZAILEAK bere egiten du ataria eta bertan eskaintzen diren Zerbitzuak zuzen erabiltzeko betebeharra, legearen, Lege-Ohar honen eta bere ezagueran diren erabilera-araudien eta jarraibideen arabera, hala nola gizartean oro har ontzat ematen diren gizalegezko usadio, portaera eta ohiturei men eginez, eta ordena publikoa zainduz.<br><br>

h)	Ataria zuzen erabiltzeko betebeharrak HORNITZAILEA behartzen du ataria edo haren baitako ezein Zerbitzu ez erabiltzera bidezkoak ez diren xedeetarako, Lege-Ohar honetan debeku direnetako , eta hirugarrenen eskubide eta interesen kalterako direnetako. Bereziki, eta adibide modura soil-soilik, HORNITZAILEAk konpromisoa hartzen du ez transmititzeko, zabaltzeko edo hirugarrenei helarazteko informaziorik, daturik, edukirik, grafikorik, soinu- edota irudi-artxibategitik, argazkirik, grabaziorik, softwarerik eta, oro har, edozein material-motarik baldin eta:<br><br>

1.Konstituzioan, Nazioarteko tratatuetan eta aplikagarriak diren legeetan onetsita dauden oinarrizko eskubide eta askatasunen aurkakoak badira;  <br>
2.Sustatzen, eragiten edo bultzatzen badituzte delitu-jarduketak, legearen aurkakoak, eta usadio moralaren eta ohitura onen kontrakoak edo ordena publikoaren aurkakoak;  <br>
3.Faltsuak, okerrak edo okerrerako ulerpidea eman dezaketenak badira, edo publizitate okerra, iruzurtia edo desleiala badarabilte; <br>
4.Hirugarrenen jabetza intelektual edo industrialeko eskubideen mendekoak izanik haien baimenik gabe erabiltzen badira;<br>
5.Ohorerako, intimitaterako eta norberaren irudirako eskubideak urratzen badituzte;<br>
6.Komunikazioen sekretuari buruzko arauak urratzen badituzte;<br>
7.Lehia desleiala egiten badute, edo MUTUALIAren edo hirugarrenen enpresa-irudiari kalte egiten badiote.<br>
8.Atariaren, haren informatika-ekipoen edo artxibategi eta dokumentuen funtzionamendu egokia kaltetu edo eragotzi dezaketen birusak edo antzeko elementuak badaramatza.

<br><br>

<p><strong>3. HORNITZAILEAren eskubideak</strong> </p>

HORNITZAILEAren eskubideak honako hauek dira:<br><br>

1. MUTUALIAren datu-basean etengabe agertzea hornitzaile moduan, atarian alta emanda dagoen bitartean.<br><br>

2. Ezarritako arauen arabera, eskaintza-eskaerak jasotzea.  <br><br>


<p><strong>4. MUTUALIAren betebeharrak</strong> </p>

Honako hauek dira MUTUALIAren betebeharrak:<br><br>

1. Egoki deritzon informazioa eguneratuta edukitzea, ezinbestez edo halabeharrez sor daitezkeen akatsen erantzule izan gabe;<br><br>

2. Hornitzaileari buruzko informazio guztiaren konfidentzialtasuna zorrozki gordetzea, bai hark berak emandakoarena eta bai HORNITZAILEA eta MUTUALIAren arteko harremanetan sortutakoarena;<br><br>

3. Hornitzaileari ematea, edozein unetan, datu-basean hari buruz diren datuen berri, eta hirugarrenei ez ematea, salbu eta horrela hornitzaileari bere eskaintzak aurkezteko aukera ematen bazaio eta haren produktuak hornikuntzako alternatiba bezala aintzat hartzeko bide egiten bazaio.  <br><br>

4. Era berean, hornitzaileari ematea, edozein unetan, datu-basean hari buruz diren datuen berri, hark, idatziz jakinarazita, aldatu edo ken ditzan. 
<br><br>

<p><strong>5. Erantzukizuna mugatzea</strong> </p>


MUTUALIAk ez du bere gain hartzen inolako erantzukizunik sistema ezin bada erabili, sarean erroreak badaude edo akats teknikoak gertatzen badira eta horien ondorioz ataria eteten bada. Era berean, MUTUALIAk ez du bere gain hartzen erantzukizunik birus informatikoen edo sistemako beste elementu batzuen zabalkundeak eragin ditzakeen kalte-galerengatik.<br><br>

MUTUALIAk ez du bere gain hartzen inolako erantzukizunik atari honen erabiltzailea den HORNITZAILEAren eta beste bezero/erabiltzaile batzuen arteko jarduketatik erator litekeen ordainketa edota erreklamazioen inguruan.<br><br>

MUTUALIAk ez du bere gain hartzen inolako erantzukizunik MUTUALIAko norbaitek, HORNITZAILEAk berak edo sarea erabiltzen duen hirugarren batek legearen, usadioen eta ohituren aurkakoak diren edo Lege Ohar honetan ezartzen diren betebeharrak urratuz egin ditzaketen jarduketengatik. Nolanahi ere, aurreko paragrafo horrek aipatzen dituen jokabide horietako edozeinen berri jasotzean, MUTUALIAk behar diren neurriak hartuko ditu sortu diren gorabeherak ahalik azkarren eta zuzenen konpon daitezen.  <br><br>

MUTUALIAk ez du aintzat hartzen inolako erantzukizunik baimenik gabeko hirugarrenek ezagutzen badituzte HORNITZAILEek ataria eta bertako Zerbitzuak erabiltzeko dituzten baldintzak eta inguruabarrak.

<br><br>

<h3>Datu pertsonalen tratamendua</h3>

DATUAK BABESTEARI BURUZKO OINARRIZKO INFORMAZIOA: Datu pertsonalen babesari buruz indarrean dagoen lege-araudia betez, jakinarazten dizugu zure datu pertsonalen Tratamenduaren arduraduna honako hau dela: MUTUALIA, GIZARTE SEGURANTZAREKIN LANKIDETZA DUEN 2 ZB.KO MUTUA. Prozesamenduaren Xede nagusia da legezko obligazioa betetzea, eta erabiltzaileak bere borondatez izena eman duen plataformako zerbitzuen kudeaketa; eta erabiltzailea eta MUTUALIAren arteko harremana egikaritzean datza haren Legitimazioa. Zure datu pertsonalen Hartzaileak dira Legeak agintzen dituenak eta tratamenduaren ardura duten eta harremana betearazteko beharrezkoak diren hirugarrenak. Datuak eskuratzeko, zuzentzeko, aurkatzeko, mugatzeko, eramateko eta ezabatzeko Eskubideak balia ditzakezu, baita informazio osagarrian azaldutako beste eskubide batzuk ere, helbide honetan: http://www.MUTUALIA.es/datospersonales.  <br><br>

Noiznahi jarri ahalko zara harremanetan Datuen babeseko ordezkariarekin helbide elektroniko honetan: <a href="mailto:dpd@MUTUALIA.es">dpd@MUTUALIA.es</a>; edota posta arruntaz: MUTUALIA, Henao 26, 48009 BILBO.<br><br>

MUTUALIAk abian dauzka segurtasun-prozedura proaktiboak datu pertsonalak babesteko eta segurtasun-neurriak aplikatzen ditu automatikoki eta edozein prozesamenduren diseinutik bertatik.  <br><br>

Erabiltzaileak, hasierako alta emateko edo sarbidea lortzeko datuak sartzen dituenean, baimena ematen du bere datu pertsonalen prozesamendurako, erreserbarik eta salbuespenik gabe, modu librean, jakinaren gainean, berariaz eta ezbairik gabe, hornitzailea eta MUTUALIAren arteko harremanerako. 

<br><br>

Erabiltzaileak atari honetan sartzea erabaki izana libretzat hartzen da, bere esku baitago ez sartzea, hala nahi izanez gero. Bere borondatea adierazten duen egintza bat da.<br><br>

Erabiltzaileak atari honetan sartzea erabaki izana jakinaren gainekotzat hartzen da, dokumentu honetan azaltzen baitzaizkio aintzat hartu behar dituen betebehar eta arau guztiak eta haien ondorioak, baldin eta azkenean erabakitzen badu atarian sartzea.<br><br>

Erabiltzaileak atari honetan sartzea erabaki izana berariazkotzat hartzen da, .webgune honetan sartzearen eta nabigatzearen ondorioz egin daitezkeen datu pertsonalen tratamenduak hornitzailea eta MUTUALIAren artean sortzen den harremana betetzeko izango baitira soil-soilik.<br><br>

Erabiltzaileak atari honetan sartzea erabaki izana ezbairik gabekotzat hartzen da, erabiltzaileari uneoro behar bezala jakinarazten zaiolako bere datuen tratamendua, eta erabiltzaileak bere baimena ematen duelako, bere datu pertsonalekin egingo denari buruzko testuetatik edo argibideetatik ezin baita eratorri berak onartzen duena ez beste helbururik.<br><br>

GARRANTZITSUA: Erabiltzaileak, atari honetan artxibategiak atxikitzeko aurkitzen diren tresnak erabiliz, eman dezakeen edozein informazio borondatezkoa da erabiltzearen aldetik, eta hark aintzatesten du behar besteko lege-eskumena baduela dokumentu horiek bidaltzeko eta, atxikitako dokumentuek edo informazioek erabiltzailearen edo hirugarrenen datu pertsonalak edukiko balituzte, espresuki adierazten du datu-lagatze horretan barnebilduta daudenen oniritzia edo hori egiteko lege-babes nahikoa eta gaitua baduela. Zentzu horretan, MUTUALIAk bere interesak gordetzeko egoki diren legezko auzibideei ekingo die kalteak pairatzen baditu erabiltzaileek dokumentuak helarazten badizkiote bidezkoak diren lege-baldintzak bete gabe, esate baterako, hirugarrenen baimenik gabe.
<br><br>
 
<h3>Esteken gaineko informazioa</h3>

MUTUALIAk ez du bere gain hartuko esteken bidez joan daitekeen beste web orrien gaineko erantzukizunik, ez eta hirugarren batek eskuragarri jarritako edukiaren gainekorik ere.<br><br>
 Beste norbaiten esteka edo webgunea erabiltzea edo bertan sartzea borondatezkoa da eta norberaren ardurapean egingo da. MUTUALIAk ez du gomendatzen ez bermatzen MUTUALIAren webguneaz kanpoko esteka baten bidez bildutako edozein informazio; esteka erabiltzeagatik edo gaizki erabiltzeagatik gertaturiko galerak, erreklamazioak edo kalteak ere ez dira bere erantzukizunekoak izango; ezta haren bitartez lorturiko informazioa ere, beste esteka edo webgune batzuk barne. Halaber, ez du erantzungo zerbitzua edota sarbidea eteteagatik, esteka bat erabiltzen ahalegintzeagatik nahiz gaizki erabiltzeagatik, ez MUTUALIAren webgunera konektatzeko garaian, ezta MUTUALIAren webgunetik edo atari honetatik beste bateko informazioa baliatzeagatik ere.<br><br>

<h3>Cookieen erabilerari buruzko informazioa</h3>

Atari honetan cookieak erabiltzen dira zenbait orritan. Aipaturiko cookieen helburua bezero eta bisitariei eskainitako zerbitzua hobetzea da.<br><br>
Cookieak erabiltzailearen ordenagailuan sortzen diren fitxategi txikiak dira eta honako informazio hau eskuratzeko aukera ematen dute:<br><br>
• Erabiltzaileak webgunea azkeneko aldiz bisitatu zueneko data eta ordua.<br>
• Webgunea aurrenekoz bisitatu zuenean erabiltzaileak aukeraturiko edukiaren diseinua.<br>
• Erabilera mugatuko sailen sarbide-kontrolean parte hartzen duten segurtasun elementuak<br>

<br><br>
<h3>Erantzukizunaren ukoa eta mugapena</h3>

Atari honetako web orrietan eskura daitezkeen informazio eta zerbitzuek hutsegiteak eta akats tipografikoak izan ditzakete. Aldizka, zenbait aldaketa egiten dira edukietan. MUTUALIAk edozein unetan aldaketak edo hobekuntzak egin ditzake edukietan edo zerbitzuetan.<br><br>

MUTUALIAk ziurtzat dituen iturrietatik eskuratu ditu informazioa eta webguneko edukia; hala ere, emandako informazioaren egiazkotasuna ziurtatzeko neurriak hartu dituen arren, ezin da zehatza eta eguneratua denik bermatu.<br><br>

Halaber, jakinarazten da atari honetako edukien helburua kalitate, kokapen eta zerbitzuari buruzko informazioa ematea dela.<br><br>


<h3>Akats tekniko eta edukiaren akats batetik eratorritako edozein erantzukizunen ukoari buruzko informazioa</h3>

MUTUALIAk ez du bere gain hartuko Interneten eskainitako zerbitzuak edo edukiak eteteari edo gaizki funtzionatzeari buruzko erantzukizunik, arrazoia edozein dela ere. Era berean, MUTUALIAk ez du bere gain hartuko sareak erortzearen, erortzeen ondorioz negozioak galtzearen, edota fluxu elektrikoaren zein bestelakoen aldi baterako etenen gaineko erantzukizunik. <br><br>

MUTUALIAk ez du adierazten ez bermatzen zerbitzuak edo edukiak etenik edo akatsik gabeak izango direnik, okerrak zuzenduko direnik, ezta zerbitzua edo zerbitzaria birus nahiz bestelako osagai kaltegarririk gabeak direnik, MUTUALIAk, honelako gertaerak saihesteko ahal duen guztia egin arren.  <br><br>

Webguneetan dagoen informazioan oinarriturik zenbait erabaki hartu edo ekintza gauzatu aurretik, gomendagarria da erabiltzaileak jasotako informazioa bestelako iturriekin alderatzea.<br><br>


<h3>Jabetza industriala eta intelektuala</h3>

MUTUALIAk eskainitako edukiek zein bere web orrien edo atari honen bidez zabaldutako edukiek, jabetza intelektualari buruzko legearen arabera sortze-lanak direnez, gaiari dagozkion nazioarteko hitzarmen eta legeen babesa daukate.<br><br>

MUTUALIAk titulartasuna du edo bidezko eskubideak ditu atari honetako eduki guztien jabetza intelektual eta industrialen ustiapenerako.<br><br>
 
Debekatuta dago zeinahi modutan erreproduzitzea, banatzea, jendaurrean erakustea, eraldatzea, eskuragarri jartzea, eta, oro har, webguneen zein bertako eduki eta informazioaren ustiapen publikoa egitea, MUTUALIAk aldez aurretik espresuki eta idatziz baimenik eman gabe.<br><br>

Ondorioz, webguneetan eta atari honetan ikusgai den eduki ororen, eta batez ere, diseinuen, testuen, grafikoen, logotipoen, ikonoen, botoien, softwarearen, izen komertzialen, marken, marrazki industrialen edo salmenta zein industrian erabil daitekeen bestelako zeinahi ikurren jabetza intelektual eta industrialaren eskubideak MUTUALIAren edo webguneetan horiek erabiltzeko baimena eman duten hirugarren titularren mende daude.<br><br>

Webgunea osatzen duten eduki, irudi, formatu, iritzi, indize eta gainerako espresio formalak, baita horiek erabili eta ikusi ahal izateko behar den softwarea ere, egile-eskubideak dauzkaten sortze-lanak dira eta, horrenbestez, Jabetza intelektualaren alorreko nazioarteko zein herrialde bakoitzeko legedia eta hitzarmen aplikagarrien babespean daude. Aipaturikoa ez betetzea legez kontrako ekintza larria da eta legedia zibil eta penalaren zigorra dakar.
<br><br>
Debekatua dago eduki edo zerbitzuen erabiltzaileek, zuzenean nahiz zeharka, osotasunean zein zati batean, web orriko eduki, irudi, formatu, indize eta gainerako espresio formalak ustiatzea edo komertzialki erabiltzea, MUTUALIAren aurretiko baimen idatzirik gabe.<br><br>

Hain zuzen, debekatua dago erreproduzitzea, banatzea, erakustea, transmititzea, erretransmititzea, edozein modutan emititzea, euskarri fisikoetan zein logikoetan (esate baterako, disketeak eta ordenagailuetako disko gogorrak) gordetzea, digitalizatzea zein MUTUALIAk baimendu gabeko datu-baseetan eskuragarri jartzea, hala nola eduki edo zerbitzuen bidez erabiltzaileen eskura dauden iritzi, irudi, forma, indize eta gainerako espresio formalak itzultzea, egokitzea, moldatzea nahiz aipatutako osagaien zeinahi eraldaketa egitea, ekintza horiek guztiak Jabetza intelektual eta industrialaren alorreko edo irudiaren babeserako legediaren mende baitaude.<br><br>

MUTUALIAk nahieran muga dezake web orrietarako sarbidea, eta bertan eskainitako eduki eta zerbitzuak, campus birtual hau bera erabiltzea ere, baita posta elektronikoaren bidez erabiltzaileek bidal ditzaketen iritzi, ohar, irudi edo iruzkin oro argitaratzea ere.<br><br>

Horrela, MUTUALIAk, egoki irizten badio, erabiltzaileen erantzukizun oso eta bakarra azpimarratuz, bere webguneetan erakutsiko diren eduki edota iritzien gainean iragazkiak ezarriko ditu. Eduki eta iritzi arrazistak, xenofoboak, baztertzaileak, pornografikoak, iraingarriak, edota, nolabait, bortizkeria sustatzen dutenak nahiz legez kontrakoak edo kaltegarriak direnak zabaltzea ekidin nahi du honela.<br><br>

MUTUALIAren webgunera edo iradokizun sailera oharrak, iritziak edo iruzkinak posta elektroniko zerbitzuaren bidez helarazten dituzten erabiltzaileek modu argi eta garbian kontrakoa adierazi ezean, zerbitzu edo edukien izaerak ahalbidetzen duen kasuetan, aipaturiko iritzi, ohar edo iruzkinak erreproduzitzeko, banatzeko, erakusteko, transmititzeko, erretransmititzeko, edonola emititzeko, euskarri fisikoan edo logikoan (esate baterako, disketetan edo ordenagailuko disko gogorrean) gordetzeko, digitalizatzeko, MUTUALIAren datu basetan eskuragarri jartzeko, itzultzeko, moldatzeko, txukuntzeko zein nolabait eraldatzeko baimena duela ulertuko du MUTUALIAk. Baimen horrek legeak aurreikusten duen egile-eskubideen babeserako epea amaitu artean iraungo du.<br><br>

Era berean, aipatutako baimena doan ematen duela erabiltzaileak, eta posta elektronikoaren bidez ohar, iritzi edo iruzkinak bidaltzearekin batera, trukean dirua jasotzeari uko egiten diola ulertuko du MUTUALIAk.<br><br>

Aurreko paragrafoan esandakoaren arabera, MUTUALIAk ohar, iritzi edo iruzkinak moldatu eta aldatzeko baimena edukiko du, betiere, webguneetako argitalpen-formatuetara egokitzeko bakarrik. Jokabide horren ondorioz ez dago pentsatzerik inola ere haien gaineko Erabiltzailearen egile-eskubideak nolabait urratzen direla.<br><br>

Debekatua dago edozein baliabide tekniko, logiko edo teknologiko erabiliz hirugarren baten onura bilatzea, zuzenean ala zeharka, irabaziekin ala gabe, webgunea osatzen duten eduki, forma, indize eta gainerako espresio formalen kontura edo hura martxan jartzeko MUTUALIAk egindako ahaleginen kontura.<br><br>

Hain zuzen ere, debekatua dago esteka, hiperesteka, framing edota MUTUALIAren webguneetara bideratuz ezarri daitekeen lotura oro, MUTUALIAk aldez aurretik, espresuki eta idatziz baimenik ematen ez badu. Puntu honetan zehaztutakoa betetzen ez bada, webguneen eta haien edukien jabetza intelektualaren gainean MUTUALIAk dauzkan legezko eskubideak urratzen direla ebatziko da.<br><br>

MUTUALIAk ez du bere gain hartuko inolako erantzukizunik arestian aipaturiko jarrera eta jokabideetatik eratorri daitezkeen ondorioei dagokienez. Era berean, MUTUALIAren webguneetatik hirugarren pertsonen eduki, zerbitzu, produktu eta abarrera zuzenean edota iragarki, lotune, esteka, hiperesteka, framing zein antzeko loturen bidez sartzearen erantzukizunik ez du bere gain hartuko.<br><br>

MUTUALIAk erreserbatu egiten du campus birtual honetako sarbidea aldi batez eteteko eskubidea, beronen hobekuntza, mantentze-lan edo edukien edo programazioaren berrikuspenak egiteko, baita beste edozein arrazoirengatik ere.<br><br>

Lege Ohar hau 2017/11/28an eguneratu da, eta aldaketak izan ditzake etorkizunean. Horregatik, erabiltzaileak haiek eskuratu, arretaz irakurri eta onartu egin behar ditu atari honekin konexioa egiten duen bakoitzean.  <br><br>


		  </div>

<div class="caja_acepto">
<form name="frmAlta" id="frmAlta" method="post" style="max-width:740px;">
  <span id="sprycheckbox1">

<span class="checkboxRequiredMsg">Aukeratu jarraitzeko</span>
</span>
<br>
<br>

  <input type="checkbox" name="opcion1" id="opcion1" required>

<span class="black">KONTRATATU MUTUALIAREKIN plataformara atxikitzeko kontratua irakurri eta onartu dut.</span></label>
<p class="imprimir"><a href="registro_texto.html" class="" target="blank">Inprimatu</a> </p>

<br />
<p><br /><br /><button type="button" class="bt_registro" id="submit1" name="submit1">Jarraitu</button></p>
<br><br />
<br>
</form>


</div>
<div style="clear:both;"></div>
    	</div>    
<p>&nbsp;</p></div>
</div>
<script type="text/javascript">
// <!--
// var sprycheckbox1 = new Spry.Widget.ValidationCheckbox("sprycheckbox1");
// //-->
$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked')) {
        $('#frmAlta').submit();
        alta();
	}
	else
       alert("Erregistratu ahal izateko atariaren erabilera baldintzak onartu behar dituzu");
    
});

</script>
                      	</div>
                    </div>
                    
                 
               
           	  </div>
                    
         	</div>
              
    	</section>
         <footer id="footer">
        
        	<div class="container">            
                    
            	<div class="footer-menu">
                
                    <ul>
                    	<li><a href="registro_texto.html">Lege-oharra</a></li>
                    	<li><a href="<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=GER" align="right">Cookiei buruzko politika</a></li>
                    </ul>
                    
                                       <p>Web optimizada para 1280 x 1024. Navegadores soportados: Internet Explorer, Mozilla Firefox y Google Chrome</p>

                   <a href="http://www.fullstep.com" title="Fullstep" target="_blank">   <img class="fullstep" src="../img/fullstep.png" /></a>
                    
              	</div>
            
           		
               
            </div>
           
        </footer>
        
         <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/main.js"></script>        
</body>

<!--#include file="../../../common/fsal_2.asp"-->

</html>
