<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />  
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />  
        <meta name="viewport" content="width=device-width, initial-scale=1" />  
        
        <title>Mutualia</title>
        
        <meta name="title" content=""/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        
        <link rel="canonical" href="/" />

		<link href="../css/reset.css" rel="stylesheet" type="text/css" />        
    	<link href="../css/font-awesome.min.css" rel="stylesheet" />  
        
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
        <link href="../css/style.css" rel="stylesheet" />  
        <link rel="stylesheet" type="text/css" href="../css/responsive.css">   
        
        <!--[if lt IE 9]>
          	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body class="popup"> 
 <header id="heading">             
            
      		<div class="container">  
            
           		<div class="text"> 
            
                  <div class="logotipo">
            		<a href="http://www.mutualia.es" title="Mutualia" target="_blank"><img src="../img/mutualia.png" alt="Mutualia" width="210" height="71" /></a>
                  </div> 
                    <div class="cierra-popup"><img src="../img/cerrar.gif" alt="Cerrar" class="cerrar"></div>                  
                                
       		  </div>   
                    
           	</div>
            
        </header>        
        <section id="ayuda"> 
            
            <div class="container">
            
           		<div class="text blue">
                
                	<div class="columna">
                        <div class="imagen">
                        <img src="../img/imagen_password.jpg" alt="imagen_password">
                         </div>  
               	  </div>
                    
                    <div class="centro sin">
                    
                    	<h4>Recuperaci&oacute;n de contrase&ntilde;a</h4>
                    
                    	<p> Si ha olvidado su contraseña, introduzca su código de compañía, de usuario y la dirección de e-mail de su cuenta de acceso y pulse "Enviar". Recibirá un e-mail con el enlace a la página donde podrá crear una nueva contraseña.</p>
                         
                        <form name="frmRecuerdo" id="frmRecuerdo" class="recovery" method="post" onsubmit="return Validar()">
                            <label>
                            <span>Código de Compañía</span>    <input type="text" name="txtCia" id="txtCia" size="30" maxlength="30"/>
                            </label>
                            <label>
                             <span>Usuario</span>
                                <input type="text" name="txtUsu" id="txtUsu" size="30" maxlength="30" placeholder=""/>
                            </label>
                            <label>
                            <span>Email</span>
                               <input type="text" name="txtEmail" id="txtEmail" size="30" maxlength="100" placeholder=""/>
                            </label>
                            <button class="btn" name="cmdEnviar" type="submit">Entrar</button>
                            <input type="hidden" name="idioma" value="spa"/>
                        </form>
                    <div class="capa-cierre-recup">
                    
                    <div class="texto-capa-cierre">Si continúa con dificultades para acceder al Portal o su dirección de email ha cambiado, consulte con el <strong>servicio de atención a proveedores </strong><a href="mailto: contrataconmutualia@mutualia.es" title=" contrataconmutualia@mutualia.es" class="azul">contrataconmutualia@mutualia.es</a>.
                    </div>
                    
                    </div> 
                             
               		</div>
                    
                    
               	</div>
                    
         	</div>
              
    	</section>
    			     
   
</body>

</html>
