﻿<%@ Language=VBScript %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/idioma.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->
<meta charset="utf-8" />  
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />  
        <meta name="viewport" content="width=device-width, initial-scale=1" />  
        
        <title>Mutualia</title>
        
        <meta name="title" content=""/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        
        <link rel="canonical" href="/" />

		<link href="css/reset.css" rel="stylesheet" type="text/css" />        
    	<link href="css/font-awesome.min.css" rel="stylesheet" />  
        
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
        <link href="css/style.css" rel="stylesheet" />  
        <link rel="stylesheet" type="text/css" href="css/responsive.css">  
        <script src="js/jquery-1.9.0.js"></script>
<script src="js/jquery-1.9.0.min.js"></script>

<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
<script src="../../common/formatos.js"></script>
<script language="JavaScript" src="../../common/ajax.js"></script>
<!--#include file="../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=SPA", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body onload="init()">

<header id="heading"> 
        
        	<div class="container"> 
                       
             	<div class="logotipo">
            		<a href="http://www.mutualia.es" title="Mutualia" target="_blank"><img src="img/mutualia.png" alt="Mutualia" width="210" height="71" /></a>
                </div> 

               <nav>                      
                    <ul class="idiomas">
                         <li><a href="#" title="CAST"><strong>Castellano</strong> | </a></li> 
                        <li><a href="#" title="EUSK"><strong>Euskera</strong> | </a></li>
                       <!-- <li><a href="#" title="ENG"><strong>ENG</strong></a> </li>-->
                    </ul> 
                  </nav> 
                   	  
                     
    		
            
           <h1>CONTRATA CON MUTUALIA</h1>      
                     
    		</div>
        
        
            <div class="header">
            
                 
                <div class="menu">  
        
                    <a class="menu-rsp" href="#" title="Men&uacute;"><span>Men&uacute;</span></a>
        
                    <div class="menu-container">
                    
                         <ul>
                            <li class="menu-padre activo"><a href="default.asp" title="Inicio">Inicio</a> </li>   <span class="oculta">|</span>
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/quienes-somos.html" title="Quiénes somos">Quiénes Somos</a></li>  <span class="oculta">|</span> 
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/que-buscamos.html" title="Qué buscamos">Qué buscamos</a></li> <span class="oculta">|</span>  
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/politicas.html" title="Políticas">Pol&iacute;ticas</a></li> <span class="oculta">|</span> 
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ayuda.html" title="Ayuda">Ayuda</a></li>
                            
                        </ul>
                                      <!--       
                        <ol>
                            <li class="activo"><a href="#" title="CAST">Castellano</a></li> 
                            
                            <li><a href="#" title="ENG">English</a> </li>
                        </ol> -->
                    
                   	</div>    
            
                </div> 
                
            </div>
            
        </header>     
    	<section id="home"> 
            
            <div class="container">
            
           		<div class="text">
                
                	
                    <div class="columna sol">
                        <div class="imagen">
                      <img src="img/shim.gif" width="100">
                         </div>  
                        
                   	</div>
                    
                    <div class="centro sin solicitud">
                      <div>
                        
                        <h2><em>SOLICITAR REGISTRO</em></h2>
        <div class="int">
        	<p>Para continuar con el proceso de alta es imprescindible que lea y acepte las condiciones de acceso a la plataforma CONTRATA CON MUTUALIA.</p>
            <div class="caja_registro">
                 <h4>IMPORTANTE LEA ATENTAMENTE</h4><br>

<h3>MUTUALIA - Mutua Colaboradora con la Seguridad Social Nº 2</h3>
                        
                        <h4>CONTRATA CON MUTUALIA - Aviso Legal</h4>

                        <strong class=" blue">Información General</strong><br />

El presente es un aviso de obligada lectura y aceptación, ya que regula las condiciones de uso que deben ser observadas y respetadas por las empresas y personas usuarias de este portal (en adelante, EL USUARIO o PROVEEDOR). Es importante leerlo atentamente, ya que el hecho de acceder a ella y visualizar o utilizar sus contenidos y funcionalidades implica que el usuario ha leído y acepta, sin reservas, las condiciones de este AVISO LEGAL que implica la aceptación de las CONDICIONES GENERALES DE USO, explicitadas en este documento.<br><br>

El usuario no podrá registrarse en este portal sin expresar su conformidad con este Aviso Legal y todo su contenido. Siempre que se acceda y utiliza el Portal, se entenderá que el usuario está de acuerdo, de forma expresa, plena y sin reservas, con la totalidad del contenido de este Aviso Legal.<br><br>

Asimismo, al ser la persona registrada en este portal el Usuario Principal de la empresa que representa, queda obligado al aceptar la conformidad de este Aviso Legal a velar por su cumplimiento por parte de todos los usuarios que se den de alta en la empresa que representa, liberando a MUTUALIA de toda responsabilidad por los posibles perjuicios que dichos usuarios pudieran causar a la empresa o cualquier otra debido a sus actuaciones en el Portal.<br><br>

Se informa de que el titular del sitio web es MUTUA COLABORADORA CON LA SEGURIDAD SOCIAL Nº 2, con domicilio operativo en calle Henao, nº 26 de BILBAO,  CP 48009.<br><br>

MUTUALIA, en el marco de su actividad, pone diferentes contenidos a disposición de los usuarios de este portal a fin de que puedan hacer un uso racional de ellos y atender sus necesidades de información con la finalidad de facilitar la relación con los proveedores y contratistas.<br><br>

El usuario accede a este portal de acuerdo con el principio de buena fe, cumpliendo las normas de orden público, las presentes condiciones generales de uso y, en su caso, las condiciones específicas que sean aplicables. Queda prohibido hacer una explotación comercial de la información y los materiales a los que accede el usuario de este portal.<br><br>

El acceso a este portal se hace bajo la responsabilidad propia y exclusiva del usuario, que responderá en todo caso de los daños y perjuicios que cause a terceros o a MUTUALIA.<br><br>

El usuario tiene expresamente prohibida la utilización y obtención de los servicios y contenidos ofrecidos en este portal por procedimientos distintos a los estipulados en estas condiciones de uso y, en su caso, en las condiciones particulares que regulen alguna actividad o servicio concreto, el cual estará debidamente especificado.<br><br>

La información que sea facilitada por el usuario, pasará a formar parte de la base de datos de proveedores de MUTUALIA. El usuario tiene derecho a consultar y modificar sus datos o darse de baja de dicha base de datos, en cualquier momento. <br><br>

No obstante, y en cumplimiento de la legalidad vigente, especialmente en lo referente a la normativa sobre transparencia y normativa de contratación del sector público, es posible que la totalidad o parte de la información aportada o la que se deduzca del tratamiento de datos en los procesos de licitación, preparación y adjudicación de contratos, deba ser publicada por exigencia legal. El usuario es conocedor de esa obligación y consiente a ello.<br><br>

MUTUALIA se compromete a utilizar los datos exclusivamente para la relación establecida y a no facilitar los datos a terceros sin consentimiento, salvo imperativo legal. El usuario puede recibir información de su interés por cualquier medio, incluido el telemático. No obstante, puede darse  de baja de la lista de distribución en cualquier momento, salvo que sus datos ya deban ser conservados por MUTUALIA por mandato u obligación legal.<br><br>

El PROVEEDOR de este portal manifiesta su conformidad y acepta que a partir de la fecha de alta como usuario en el sistema todas las comunicaciones e informaciones, pedidos, suministros, facturaciones y todo tipo de relación como proveedor de MUTUALIA, puedan ser realizadas a través de esta plataforma CONTRATA CON MUTUALIA.<br><br> 

La conexión con el portal se realizará mediante una clave de usuario y un código de acceso (contraseña) que serán generados en pantalla al cumplimentar y enviar los datos de proveedor que se dé de alta. Toda la operativa realizada a través de éste servicio, contrastadas por el código de acceso y las claves de seguridad, se deberán considerar en todo caso válidas y eficaces, considerándose que han sido autorizadas y cursadas por el proveedor, debiendo considerarse el uso de las mencionadas claves como firma electrónica de los proveedores dados de alta en el portal, equiparable, respecto a los contratos que suscriba con MUTUALIA por medios electrónicos, a la firma manuscrita.<br><br>

A tal fin, el proveedor usuario manifiesta que los datos que figuran en su registro son ciertos y corresponden a su empresa.<br><br>

Es responsabilidad del PROVEEDOR, el notificar a MUTUALIA cualquier cambio en la información o en los datos personales facilitados por el Proveedor.<br><br>

 Las relaciones entre MUTUALIA y el PROVEEDOR derivadas del uso del Portal y de los servicios prestados a través del mismo, se someterán a la legislación y jurisdicción españolas.<br><br>


<strong class=" blue">Condiciones Generales</strong><br><br>

<p><strong>1. Objeto de Contrata con Mutualia</strong> </p>

Contrata con Mutualia es el medio a través del cual MUTUALIA se comunica con sus proveedores para recabar, obtener y/o solicitar información, ofertas, presupuestos, documentación, así como cualquier información/documentación administrativa, técnica y/o comercial que estime oportuna. Al mismo tiempo MUTUALIA puede utilizar el portal para remitirle aquella información que considere de su interés.<br><br>

El uso o acceso al Portal y/o a los Servicios que este ofrece no atribuyen al PROVEEDOR ningún derecho sobre la permanencia indefinida de sus datos en este portal ni en los sistemas de información de MUTUALIA, no asumiendo MUTUALIA con los usuarios de este portal ninguna obligación contractual de mantenimiento del servicio, pudiendo en cualquier momento MUTUALIA dar de baja a cualquier usuario del portal, por razones justificadas, tales como, incumplimiento de las normas de este AVISO LEGAL, del Sistema de Valores de MUTUALIA o principios/normas de Trasparencia, no pudiendo reclamar el usuario por tal baja en el portal ninguna indemnización a MUTUALIA por daños o perjuicios. <br><br>


<p><strong>2. Obligaciones del proveedor</strong> </p>

El presente acuerdo tiene por objeto regular las relaciones entre MUTUALIA y el PROVEEDOR, en todo lo relacionado con el uso del Portal. Son obligaciones del proveedor las siguientes:<br><br>

 a)	Proporcionar cuantos datos sean necesarios para el adecuado funcionamiento del sistema, así como mantenerlos actualizados, comunicando a la mayor brevedad posible todo cambio que se produzca en los mismos.<br><br>

b) Garantizar la autenticidad de los datos y documentos facilitados a consecuencia de la cumplimentación de los formularios necesarios para la suscripción de los Servicios o durante la aportación posterior o futura de nueva información. Asimismo, el PROVEEDOR actualizará la información facilitada para que refleje, en cada momento, su situación real. Por todo ello, el PROVEEDOR será el único responsable de los daños y perjuicios ocasionados a MUTUALIA como consecuencia de declaraciones inexactas o falsas.<br><br>

c)	Guardar absoluta confidencialidad en relación con toda la información que se genere en las relaciones entre el PROVEEDOR y MUTUALIA.<br><br>

d)	Abstenerse de cualquier manipulación en la utilización informática/técnica del Portal, así como el uso del mismo con fines distintos de aquellos para los que fue previsto. Asimismo, el PROVEEDOR se abstendrá de acceder a zonas no autorizadas del Portal.<br><br>

e)	Cumplir fielmente sus compromisos en la información remitida a través del portal. En el caso de que el PROVEEDOR no demuestre la necesaria diligencia comercial, trasparencia o incumpla las obligaciones contraídas, MUTUALIA se reserva el derecho de excluir temporal o permanentemente al PROVEEDOR del portal.<br><br>

f)	El PROVEEDOR deberá indicar/aportar solamente aquellos grupos de contenidos y/o materiales, que se refieran a bienes y/o servicios que, en el momento de la aceptación del acuerdo, el proveedor comercialice, oferte, fabrique o distribuya; y que sean o puedan ser del interés comercial de MUTUALIA.<br><br>

g)	El PROVEEDOR acepta que las ofertas/documentos introducidas en el Portal sean consideradas con el mismo rango y validez que una oferta enviada por cualquier otro medio tradicional en soporte físico papel (carta, fax, etc).<br><br>

h)	El PROVEEDOR se obliga a hacer un uso correcto del Portal y de los Servicios conforme a la ley, el presente Aviso Legal y demás reglamentos de uso e instrucciones puestos en su conocimiento, así como con los buenos usos, comportamientos y costumbres general y socialmente aceptadas, y el orden público.<br><br>

La obligación del uso correcto del Portal implica por parte del PROVEEDOR, de abstenerse de utilizar el Portal o cualquiera de los Servicios con fines ilícitos, prohibidos en el presente Aviso Legal, lesivos de los derechos e intereses de terceros. En particular, y a título meramente indicativo, el PROVEEDOR se compromete a no transmitir, difundir o poner a disposición de terceros informaciones, datos, contenidos, gráficos, archivos de sonido y/o imagen, fotografías, grabaciones, software y, en general, cualquier clase de material que:<br><br>

1.	Sea contrario a los derechos fundamentales y libertades públicas reconocidas en la Constitución, Tratados internacionales y leyes aplicables; <br>
2.	Induzca, incite o promueva actuaciones delictivas, contrarias a la ley, a la moral y buenas costumbres generalmente aceptadas o al orden público; <br>
3.	Sea falso, inexacto o pueda inducir a error, o constituya publicidad ilícita, engañosa o desleal;<br>
4.	Esté protegido por derechos de propiedad intelectual o industrial pertenecientes a terceros, sin autorización de los mismos;<br>
5.	Vulnere derechos al honor, a la intimidad o a la propia imagen;<br>
6.	Vulnere las normas sobre secreto de las comunicaciones;<br>
7.	Constituya competencia desleal, o perjudique la imagen empresarial de MUTUALIA o terceros;<br>
8.	Esté afectado por virus o elementos similares que puedan dañar o impedir el funcionamiento adecuado del Portal, los equipos informáticos o sus archivos y documentos.

<br><br>

<p><strong>3. Derechos del PROVEEDOR</strong> </p>

Son derechos del PROVEEDOR los siguientes:<br><br>

1. Mantener una presencia constante en la base de datos de MUTUALIA, en su calidad de proveedor, mientras continúe dado de alta en el portal. <br><br>

2. Recibir peticiones de ofertas en virtud de las normas establecidas.  <br><br>


<p><strong>4. Obligaciones de MUTUALIA</strong> </p>

Son obligaciones de MUTUALIA las siguientes:<br><br>

1.	Mantener la información que estime oportuna actualizada, no siendo responsable de los errores que se produzcan por fuerza mayor o caso fortuito;<br><br>

2.	Guardar absoluta confidencialidad de toda la información relativa al proveedor: bien la proporcionada por éste, bien la generada en las relaciones entre el PROVEEDOR y MUTUALIA;<br><br>

3.	Facilitar al proveedor, en cualquier momento, la situación de sus datos en la base de datos, y no facilitarla a terceros si no es para permitir al proveedor la presentación de sus ofertas y la toma en consideración de sus productos como alternativa de suministro. <br><br>

4.	Asimismo, facilitar al proveedor, en cualquier momento, la situación de sus datos en la base de datos, para que éste, previa notificación por escrito, los modifique o elimine. 
<br><br>

<p><strong>5. Limitación de responsabilidad</strong> </p>


MUTUALIA no asume ninguna responsabilidad derivada de la falta de disponibilidad del sistema, fallos de la red o técnicos que provoquen un corte o una interrupción en el Portal. Igualmente MUTUALIA no se hace responsable de los daños y perjuicios que puedan ocasionar la propagación de virus informáticos u otros elementos en el sistema.<br><br>

MUTUALIA no asume ninguna responsabilidad de pagos y/o reclamaciones que deriven de las actuaciones de otros clientes/proveedores en relación a acuerdos entre dichos y el PROVEEDOR usuario de este portal.<br><br>

MUTUALIA no asume ninguna responsabilidad sobre cualquier actuación que sea contraria a las leyes, usos y costumbres, o bien que vulnere las obligaciones establecidas en el presente Aviso Legal, por parte de ninguna persona de MUTUALIA, del PROVEEDOR o cualquier usuario de la red. No obstante, cuando MUTUALIA tenga conocimiento de cualquier conducta a la que se refiere el párrafo anterior, adoptará las medidas necesarias para resolver con la máxima urgencia y diligencia los conflictos planteados. <br><br>

MUTUALIA se exime de cualquier responsabilidad derivada de la intromisión de terceros no autorizados en el conocimiento de las condiciones y circunstancias de uso que los PROVEEDORES hagan del Portal y de los Servicios.

<br><br>

<h3>Tratamiento de datos personales</h3>

INFORMACIÓN BÁSICA SOBRE PROTECCIÓN DE DATOS: En cumplimiento de la normativa legal vigente en materia de protección de datos personales, se le informa que el Responsable del Tratamiento de sus datos personales es MUTUALIA, MUTUA COLABORADORA CON LA SEGURIDAD SOCIAL Nº 2. La Finalidad principal del tratamiento es el cumplimiento de una obligación legal y la gestión de los servicios de la plataforma en la que se da de alta voluntariamente el usuario; y la Legitimación del mismo es dar cumplimiento a la relación comercial/contractual entre usuario y MUTUALIA. Los Destinatarios de sus datos personales son aquellos a los que la Ley obliga y terceros encargados de tratamiento, que sean necesarios para dar cumplimiento a esta relación. Puede ejercer sus Derechos  a acceder, rectificar, oponerse, limitar, portar y suprimir los datos, así como otros derechos explicados en la Información adicional, visitando: http://www.MUTUALIA.es/datospersonales. <br><br>

Puede contactar en cualquier momento con el Delegado de Protección de datos en dpd@MUTUALIA.es, o escribir a MUTUALIA, calle Henao nº 26,  48009 BILBAO.<br><br>

MUTUALIA tiene implementados procedimientos de seguridad proactiva en materia de protección de datos personales y aplica medidas de seguridad por defecto y desde el diseño de cualquier tratamiento. <br><br>

El usuario, al introducir sus datos de alta inicial  y /o acceso, presta su consentimiento para el tratamiento de sus datos personales, sin reservas y sin excepción, de forma libre, informada, específica e inequívoca, para la finalidad de la relación entre el proveedor y MUTUALIA. <br><br>

La aceptación de acceder a este portal por parte del usuario es una decisión considerada como libre, por cuanto este puede decidir no entrar, si así lo desea. Es un acto de manifestación de su voluntad.<br><br>

La aceptación de acceder a este portal por parte del usuario es una decisión considerada como informada, por cuanto se le explica en este documento todas las obligaciones y normas que debe contemplar y sus consecuencias, en caso de que finalmente decida acceder a este portal.<br><br>

La aceptación de acceder a este portal por parte del usuario es una decisión considerada como específica, ya que el tratamiento de datos personales que pueda ir asociado al acceso y navegación por este sitio, solo lo será para dar cumplimiento a la relación que se establece entre el proveedor y MUTUALIA.<br><br>

La aceptación de acceder a este portal por parte del usuario es una decisión considerada como inequívoca, porque el usuario es debidamente informado del tratamiento de sus datos en cada momento, y el usuario lo consiente sin que de los textos o explicaciones sobre lo que se hará con sus datos personales, pueda entenderse otra finalidad distinta a la que presta su consentimiento.<br><br>

IMPORTANTE: Cualquier información aportada por el usuario mediante los mecanismos para adjuntar archivos presentes en este portal, es de carácter voluntaria por parte del usuario, y este reconoce que dispone de la legitimación legal suficiente para realizar tales entregas de documentos y que, en caso de que los documentos o informaciones adjuntadas contengan datos de carácter personal del usuario o de terceras personas, manifiesta expresamente que dispone del consentimiento de los afectados para tal cesión o amparo legal suficiente y habilitante para hacerlo. En este sentido, MUTUALIA emprenderá las acciones legales oportunas en defensa de sus intereses si resulta perjudicada por la entrega de documentos por parte de los usuarios sin que se disponga de los requisitos que legalmente le sean exigibles, como por ejemplo, el consentimiento de terceros.
<br><br>
 
<h3>Información sobre los links</h3>

MUTUALIA no se hace responsable de las webs no propias a las que se puede acceder mediante vínculos “links” o de cualquier contenido puesto a disposición por terceros.<br><br>
 Cualquier uso de un vínculo o acceso a una web no propia es realizado por voluntad y riesgo exclusivo del usuario y MUTUALIA no recomienda ni garantiza ninguna información obtenida a través de un vínculo ajeno a la web de MUTUALIA ni se responsabiliza de ninguna pérdida, reclamación o perjuicio derivada del uso o mal uso de un vínculo, o de la información obtenida a través de él, incluyendo otros vínculos o webs, de la interrupción en el servicio o en el acceso, o del intento de usar o usar mal un vínculo, tanto al conectar a la Web de MUTUALIA como al acceder a la información de otras webs desde la Web de MUTUALIA o este portal.<br><br>

<h3>Información sobre la utilización de cookies</h3>

En este portal se pueden estar utilizando cookies en algunas páginas. La finalidad de dichas cookies es mejorar el servicio que ofrecen a sus clientes y a nuestros visitantes.<br><br>
Las cookies son pequeños ficheros de datos que se generan en el ordenador del usuario y que permiten obtener la siguiente información:<br><br>
•	Fecha y hora de la última vez que el usuario visito la Web.<br>
•	Diseño de contenido que el usuario escogió en su primera visita a la web.<br>
•	Elementos de seguridad que intervienen en el control de acceso a las áreas restringidas<br>


<h3>Renuncia y limitación de la responsabilidad</h3>

La información y servicios incluidos o disponibles a través de las páginas web de este portal pueden incluir incorrecciones o errores tipográficos. De forma periódica se incorporan cambios a la información contenida. MUTUALIA puede introducir en cualquier momento mejoras y/o cambios en los servicios o contenidos.<br><br>

MUTUALIA ha obtenido la información y los materiales incluidos en la web de fuentes consideradas como fiables, pero, si bien se han tomado las medidas correspondientes para asegurar que la información contenida sea correcta, no garantiza que sea exacta y actualizada.<br><br>

También se advierte que los contenidos de este portal tienen una finalidad informativa en cuanto a calidad, situación y servicios.<br><br>


<h3>Información sobre la exención de toda la responsabilidad derivada de un fallo técnico y de contenido</h3>

MUTUALIA declina cualquier responsabilidad en caso de que existan interrupciones o un mal funcionamiento de los servicios o contenidos ofrecidos en Internet, cualquiera que sea su causa. Asimismo, MUTUALIA no se hace responsable por caídas de la red, pérdidas de negocio a consecuencia de dichas caídas, suspensiones temporales del fluido eléctrico o cualquier otro tipo. <br><br>

MUTUALIA no declara ni garantiza que los servicios o contenidos no sean interrumpidos o que estén libres de errores, que los defectos sean corregidos, o que el servicio o el servidor que lo pone a disposición estén libres de virus u otros componentes nocivos, sin perjuicio de que MUTUALIA realiza sus mejores esfuerzos en evitar este tipo de incidentes.  <br><br>

En caso de que el Usuario tomara determinadas decisiones o realizara acciones con base a la información incluida en cualquiera de los websites, se recomienda la comprobación de la información recibida con otras fuentes. <br><br>


<h3>Propiedad industrial e intelectual</h3>

Los contenidos prestados por MUTUALIA así como los contenidos vertidos en la red a través de sus páginas web o de este portal, constituyen una obra en el sentido de la legislación sobre propiedad intelectual por lo que se hallan protegidos por las leyes y convenios internacionales aplicables en la materia.<br><br>

MUTUALIA es titular o dispone de los legítimos derechos de explotación de propiedad intelectual e industrial de la totalidad de los contenidos de este portal.<br><br>
 
Queda prohibida cualquier forma de reproducción, distribución, comunicación pública, transformación, puesta a disposición y, en general, cualquier otro acto de explotación pública referido tanto a las páginas Web como a sus contenidos e información, sin el expreso y previo consentimiento y por escrito de MUTUALIA.<br><br>

En consecuencia, todos los contenidos que se muestran en los diferentes websites y en este portal, y en especial, diseños, textos, gráficos, logos, iconos, botones, software, nombres comerciales, marcas, dibujos industriales o cualesquiera otros signos susceptibles de utilización industrial y comercial están sujetos a derechos de propiedad intelectual e industrial de MUTUALIA o de terceros titulares de los mismos que han autorizado debidamente su inclusión en los diferentes websites.<br><br>

Los contenidos, imágenes, formas, opiniones, índices y demás expresiones formales que formen parte de las páginas Web, así como el software necesario para el funcionamiento y visualización de las mismas, constituyen asimismo una obra en el sentido del Derecho de Autor y quedan, por lo tanto, protegidas por las convenciones internacionales y legislaciones nacionales en materia de Propiedad intelectual que resulten aplicables. El incumplimiento de lo señalado implica la comisión de graves actos ilícitos y su sanción por la legislación civil y penal.
<br><br>
Queda prohibido todo acto por virtud del cual los Usuarios de los servicios o contenidos puedan explotar o servirse comercialmente, directa o indirectamente, en su totalidad o parcialmente, de cualquiera de los contenidos, imágenes, formas, índices y demás expresiones formales que formen parte de las páginas Web sin permiso previo y por escrito de MUTUALIA.<br><br>

En concreto, y sin carácter exhaustivo, quedan prohibidos los actos de reproducción, distribución, exhibición, transmisión, retransmisión, emisión en cualquier forma, almacenamiento en soportes físicos o lógicos (por ejemplo, disquetes o disco duro de ordenadores), digitalización o puesta a disposición desde bases de datos distintas de las pertenecientes a las autorizadas por MUTUALIA, así como su traducción, adaptación, arreglo o cualquier otra transformación de dichas opiniones, imágenes, formas, índices y demás expresiones formales que se pongan a disposición de los Usuarios a través de los servicios o contenidos, en tanto tales actos estén sometidos a la legislación aplicable en materia de Propiedad intelectual, industrial o de protección de la imagen.<br><br>

MUTUALIA es libre de limitar el acceso a las páginas Web, y a los productos y/o servicios en ella ofrecidos, incluyendo este campus virtual,  así como la consiguiente publicación de las opiniones, observaciones, imágenes o comentarios que los Usuarios puedan hacerle llegar a través del correo electrónico.<br><br>

MUTUALIA en este sentido, podrá establecer, si lo considera oportuno, sin perjuicio de la única y exclusiva responsabilidad de los Usuarios, los filtros necesarios a fin de evitar que a través de sus páginas Web puedan verterse en la red contenidos u opiniones, considerados como racistas, xenófobos, discriminatorios, pornográficos, difamatorios o que, de cualquier modo, fomenten la violencia o la diseminación de contenidos claramente ilícitos o nocivos.<br><br>

Aquellos Usuarios que envíen a las páginas Web de MUTUALIA, a su departamento de sugerencias, observaciones, opiniones o comentarios por medio del servicio de correo electrónico, salvo que expresen de manera cierta e inconfundible lo contrario, en los casos en los que por la naturaleza de los servicios o contenidos ello sea posible, se entiende que autorizan a MUTUALIA para la reproducción, distribución, exhibición, transmisión, retransmisión, emisión en cualquier formato, almacenamiento en soportes físicos o lógicos (por ejemplo, disquetes o disco duro de ordenadores), digitalización, puesta a disposición desde bases de datos pertenecientes a MUTUALIA, traducción, adaptación, arreglo o cualquier otra transformación de tales observaciones, opiniones o comentarios, por todo el tiempo de protección de derecho de autor que esté previsto legalmente.<br><br>

Asimismo, se entiende que esta autorización se hace a título gratuito, y que por el solo hecho de enviar por correo electrónico tales observaciones, opiniones o comentarios, los Usuarios declinan cualquier pretensión remuneratoria por parte de MUTUALIA.<br><br>

De acuerdo con lo señalado en el párrafo anterior, MUTUALIA queda autorizada igualmente para proceder a la modificación o alteración de tales observaciones, opiniones o comentarios, a fin de adaptarlos a las necesidades de formato editorial de las páginas Web, sin que por ello pueda entenderse que existe en absoluto cualquier tipo de lesión de cualesquiera de las facultades morales de derecho de autor que los Usuarios pudieran ostentar sobre aquéllas.<br><br>

Quedan prohibidos cualesquiera de los recursos técnicos, lógicos o tecnológicos por virtud de los cuales un tercero pueda beneficiarse, directa o indirectamente, con o sin lucro, de todos y cada uno de los contenidos, formas, índices y demás expresiones formales que formen parte de las páginas Web, o del esfuerzo llevado a cabo por MUTUALIA para su funcionamiento.<br><br>

En concreto, queda prohibido todo link, hyperlink, framing o vínculo similar que pueda establecerse en dirección a las páginas Web de MUTUALIA sin el consentimiento previo, expreso y por escrito de MUTUALIA Cualquier trasgresión de lo dispuesto en este punto será considerada como lesión de los legítimos derechos de Propiedad intelectual de MUTUALIA sobre las páginas Web y todos los contenidos de las mismas.<br><br>

MUTUALIA no asumirá responsabilidad alguna ante consecuencias derivadas de las conductas y actuaciones antes citadas, del mismo modo que no asumirá responsabilidad alguna por los contenidos, servicios, productos, etc., de terceros a los que se pueda acceder directamente o a través de banners, enlaces, links, hyperlinks, framing o vínculos similares desde los websites de MUTUALIA.<br><br>

MUTUALIA se reserva el derecho de suspender temporalmente el acceso a este campus virtual por causas relacionadas con la mejora, el mantenimiento o la revisión de los contenidos o de la programación, y también por cualquier otro motivo.<br><br>

El presente aviso legal ha sido actualizado a fecha 28/11/2017 y puede tener variaciones futuras. Por ello, el usuario debe accederlas, leerlas atentamente y aceptarlas, en cada conexión a este portal. 


		  </div>

<div class="caja_acepto">
<form name="frmAlta" id="frmAlta" method="post" style="max-width:740px;">
  <span id="sprycheckbox1">

<span class="checkboxRequiredMsg">Seleccione para continuar.</span>
</span>
<br>
<br>

  <input type="checkbox" name="opcion1" id="opcion1" required>

<span class="black">He le&iacute;do y acepto el contrato de adhesi&oacute;n a la plataforma CONTRATA CON MUTUALIA.</span></label>
<p class="imprimir"><a href="registro_texto.html" class="" target="blank">Imprimir</a> </p>

<br />
<p><br /><br /><button type="button" class="bt_registro" id="submit1" name="submit1">Continuar</button></p>
<br><br />
<br>
</form>


</div>
<div style="clear:both;"></div>
    	</div>    
<p>&nbsp;</p></div>
</div>
<script type="text/javascript">
// <!--
// var sprycheckbox1 = new Spry.Widget.ValidationCheckbox("sprycheckbox1");
// //-->
$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked')) {
        $('#frmAlta').submit();
        alta();
	}
	else
       alert("Debe aceptar las condiciones de uso del portal para poder registrarse");
    
});

</script>
                      	</div>
                    </div>
                    
                 
               
           	  </div>
                    
         	</div>
              
    	</section>
         <footer id="footer">
        
        	<div class="container">            
                    
            	<div class="footer-menu">
                
                    <ul>
                    	<li><a href="registro_texto.html">Aviso Legal</a></li>
                    	<li><a href="<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=SPA" align="right">Política de Cookies</a></li>
                    </ul>
                    
                                       <p>Web optimizada para 1280 x 1024. Navegadores soportados: Internet Explorer, Mozilla Firefox y Google Chrome</p>

                   <a href="http://www.fullstep.com" title="Fullstep" target="_blank">   <img class="fullstep" src="img/fullstep.png" /></a>
                    
              	</div>
            
           		
               
            </div>
           
        </footer>
        
         <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>        
</body>

<!--#include file="../../common/fsal_2.asp"-->

</html>
