﻿<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="../estilos.css" rel="stylesheet" type="text/css">
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
    <style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style>
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body>
    <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
    <input type="hidden" name="idioma" value="eng" />
    <table width="640" border="0" cellspacing="0" cellpadding="0" class="textos">
        <tr>
            <td height="100" colspan="2" background="../images/fondo_L.gif">
                <div align="right">
                    <a href="http://www.fullstep-ase.com/" target="_blank">
                        <img src="../images/logo.jpg" border="0"></a></div>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;
                
            </td>
        </tr>
        <tr>
            <td width="20">&nbsp;
                
            </td>
            <td width="615" align="left" class="textos" style="color:#666666; font-family:Verdana">
                <br />
                
                <span style="color: #666666;">If you forgot your login data, fill in the registered e-mail address and you will receive an e-mail with your login and password.<br><br />
                </span>
                <br />
                <table class="textos">
                        <tr>
                            <td>
                                Company code:
                            </td>
                            <td>
                                <input type="text" name="txtCia" id="txtCia" size="80" maxlength="100" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                User Code:
                            </td>
                            <td>
                                <input type="text" name="txtUsu" id="txtUsu" size="80" maxlength="100" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                E-mail address:
                            </td>
                            <td>
                                <input type="text" name="txtEmail" id="txtEmail" size="80" maxlength="100" />
                            </td>
                        </tr>
                    </table>
            </td>
        </tr>
        <tr>
            <td width="20">&nbsp;
                
            </td>
            <td class="textos" style="color:#666666; font-family:Verdana">
                <br />
                <input type="submit" value="Enviar" name="cmdEnviar" id="cmdEnviar" />
                <br />
                <br />
                If you have problems while accessing the portal, or your e-mail address has changed
                please call the Supplier call centre,<br>
                Phone +34 902 996 926
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
