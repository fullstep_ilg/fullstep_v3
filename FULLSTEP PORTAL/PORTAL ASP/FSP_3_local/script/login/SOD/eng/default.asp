﻿<%@ Language=VBScript %>
<!--#include file="../../login.asp"-->
<%		
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if
if Not IdOrden = "" then
    if Not IsNumeric(IdOrden) then
        IdOrden=""
    end if
end if

if Not CiaComp = "" then
    if Not IsNumeric(CiaComp) then
        CiaComp=""
    end if
end if
If Idioma="" then
	Idioma="ENG"
end if

%>


<html>
<head>
<title>:: Supplier Portal ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />  
<meta name="viewport" content="width=device-width, initial-scale=1" />  

<link rel="canonical" href="/" />

<link href="../css/reset.css" rel="stylesheet" type="text/css" />        
<link href="../css/font-awesome.min.css" rel="stylesheet" />  
        
<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
<link href="../css/style.css" rel="stylesheet" />  
<link rel="stylesheet" type="text/css" href="../css/responsive.css">   
        
<!--[if lte IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script language="javascript">
<!--

function ventanaLogin (IDI){

   window.open ("registro.asp?Idioma="+IDI,"","width=700,height=540,resizable=yes")

}
function ventanaSecundaria (URL){

   window.open(URL,"ventana1","width=700,height=420,scrollbars=no")

}
function ventanaAyuda (URL){

   window.open(URL,"ayuda","width=700,height=600,scrollbars=yes")

}

function popup (URL) {
 window.open("popup.htm","","width=700,height=400,scrollbars=NO")
 }
function recuerdePWD()
{
   window.open("recuerdo.asp","_blank","width=818,height=500,scrollbars=NO")
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
//-->
</script>
<script>
$(document).ready(function(){
	//---------------------
	
	$("#txtCIA").val("Company Code");
  	$("#txtCIA").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Company Code"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Company Code");
  			$(this).removeClass('focus');
  		}
  	});	
	//---------------------
  	$("#txtUSU").val("User");
  	$("#txtUSU").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="User"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("User");
  			$(this).removeClass('focus');
  		}
  	});
	//---------------------
  	$("#txtPWD").val("Password");
	$("#txtPWD").focus(function(){
/*  	$("#txtPWD").bind('focus',function(){*/
  		$(this).addClass('focus');
  		if($(this).val()=="Password"){
  			$(this).val('');}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Password");
  			$(this).removeClass('focus');
  		}
  	});
});
</script>

</head>
<body>
<header id="heading">             
            
      		<div class="container">  
            
           		<div class="text"> 
            
                    <div class="logotipo">
                        <a href="http://www.sodecia.com" title="Sodecia"><img src="../img/logo.jpg" alt="Sodecia" /></a>
                    </div>  
                    
                    <div id="titulo">SUPPLIER PORTAL</div> 
                    
                         
                    <nav>                      
                        <ul>
                            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ayuda.html" target="_blank">Help</a></li>
                            <li>|</li>
                            <li>
                            	<strong class="trigger">Select language <i class="fa fa-caret-down"></i></strong>
                            	<ul>
	                            	<li><a href="../default.asp" title="Portugués">Portugués</a></li>
	                            	<li><a href="#" title="English">English</a></li>
                            	</ul>
                            </li>                                 
                        </ul> 
                    </nav>                   
                                
           		</div>   
                    
           	</div>
            
        </header>        
    	<div id="main">
            <div class="container">
           		<div class="section-acceso">
                	<div class="acceso">
                    	<h2>SUPPLIERS ACCESS</h2>
                        
                             <form action="default.asp" method="post" name="frmLogin"  id="frmLogin">
        <input type="text" class="label" id="txtCIA" name="txtCIA" maxlength="20" />
        <input type="text" class="label" id="txtUSU" name="txtUSU" maxlength="20" />
        <input type="password" class="label" id="txtPWD" name="txtPWD" maxlength="20" autocomplete="off" />
        <div id="bt_acceso">
          <input class="btn" name="cmdEntrar" type="submit" value="LOGIN" />
        </div>
        <input type="hidden" id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
        <input type="hidden" id="CiaComp" name="CiaComp" value="<%=CiaComp%>">
      </form>
			            <div class="acceso-recordar-claves"><a href="javascript:void(null)" onClick="recuerdePWD()" >Forgot your password?</a></div>
                 <div id="registro">
                 <div id="registro">
            <h4>NOT YET REGISTERED?</h4>
                            
                                 <p> <a class="btn" href="javascript:ventanaLogin('ENG')" >Register</a></p>
                            </div>
                   	</div>
               	</div>
         	</div>
        
    	</div>
   <!--      <div class="clearfix"></div>-->
        
        <footer id="footer">
        
        	<div class="container">            
            
            	<div class="footer-soporte">
                	
               	  <div class="textos">
                        <p>FULLSTEP offers its suppliers a direct communications channel which they can use to respond to requests for quotations made by the purchasing department.</p>
                        </div>
                        <div class="textos">
        <p>If you have already registered in this portal and have been authorised as a supplier, you can access the private suppliers' area by entering your passwords. <br>
<br>
If you have still not registered in the portal, you can apply via the "Register" link </p>
      </div>
                
                    <div class="soporte">
                    
                        <div class="soporte-titular">
                            <h3>SUPPLIER SUPPORT</h3>
                            <p><strong>Tel. +34 902 996 926</strong><br /><a href="mailto:atencionalcliente@fullstep.com">atencionalcliente@fullstep.com</a></p>
                        </div>
                        
                        <div class="soporte-horario">
                            <p><strong>Opening hours:</strong></p>
                            <p>Monday to Thursday: 08:00 - 21:00</p>
                            <p>Friday: 08:00 - 19:00</p>
                        </div>
                        
                    </div>
                    
              	</div>
            
           		<div class="text">                
                    
                    <a href="#"><img src="../img/fullstep.png" alt="fullstep" /></a> 
                    
                    <p><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/aviso-legal.html" target="_blank">Legal Notice</a> | <a href="<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=ENG" >Cookies Policy</a></p>                    
                    <p>&copy; FULLSTEP All rights reserved.</p>                           
                    
               	</div>
               
            </div>
            
        </footer>
                       
                       
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/main.js"></script>    




</body>

</html>
