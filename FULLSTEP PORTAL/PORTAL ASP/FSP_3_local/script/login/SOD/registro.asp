﻿<%@ Language=VBScript %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/idioma.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="css/reset.css" rel="stylesheet" type="text/css" />        
<link href="css/font-awesome.min.css" rel="stylesheet" />  
        
<!--<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />-->
<link href="css/style.css" rel="stylesheet" />  
<!--[if lte IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <link href="css/estilo_custom_ie9.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!--<link rel="stylesheet" type="text/css" href="css/responsive.css"/>   -->
<script src="js/jquery-1.9.0.js"></script>
<script src="js/jquery-1.9.0.min.js"></script>
<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
<script src="../../common/formatos.js"></script>
<script language="JavaScript" src="../../common/ajax.js"></script>
<!--#include file="../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=SPA", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body onload="init()">

<header id="heading-registro"> 
        
        	<div class="container"> 
                       
             	<div class="logotipo">
            		<a href="<%=application("RUTASEGURA")%>" target="_blank"><img src="img/logo.jpg" alt="Sodecia" /></a>
                </div>  

                
            
           	  <div id="titulo">PORTAL DE FORNECEDORES</div>      
                     
    		</div>
        
        
          
            
        </header>           
    			
        
        
    	<section id="servicios"> 
            
            <div class="container">
            
           		<div class="text">
                
                	<!--
                    <div class="columna sol">
                        <div class="imagen">
                      <img src="img/shim.gif" width="100" >
                         </div>  
                        
                   	</div>
                    -->
                    <div class="centro sin solicitud">
                      <div>
                        
                        <h2><em>SOLICITAR REGISTRO</em></h2>
        <div class="int">
        	<p>Para prosseguir com o registo, o seguinte contrato deve ser lido e aceito </p>
            <div class="caja_registro">
                <strong>IMPORTANTE LEIA COM ATENÇÃO</strong><br><br>



Registo de fornecedores - Aviso Legal<br><br>

Acesso ao Portal<br><br>

O registo como fornecedor no Portal de compras da SODECIA está condicionado à leitura prévia e aceitação das seguintes cláusulas. Se não expressar a sua conformidade com as mesmas, não poderá efetuar o registo. Sempre que aceder e utilizar o Portal, entender-se-á que concorda, de forma expressa, plena e sem reservas, com a totalidade do conteúdo do presente Aviso Legal. Para além disso, ao ser o Utilizador Principal da sua empresa e ao aceitar a conformidade com o presente Aviso Legal, fica obrigado a garantir o respetivo cumprimento por parte de todos os utilizadores que se registem na sua empresa, isentando a SODECIA de qualquer responsabilidade pelos prejuízos que esses utilizadores possam causar à sua empresa ou a qualquer outra devido às suas ações no Portal.<br><br>

CLÁUSULAS<br><br>
1. Objeto do Portal de compras da SODECIA<br>
O portal de compras da SODECIA é o meio através do qual a SODECIA comunica com os seus fornecedores para lhes solicitar orçamentos e documentos, assim como as informações comerciais que considere adequadas. Ao mesmo tempo, a SODECIA pode utilizar o portal para lhe enviar as informações que considere que são do seu interesse.<br><br>
A SODECIA age como comprador direto ou como gestor de compras dos seus clientes e irá agir em nome destes clientes durante o processo de compra.<br><br>

A utilização ou o acesso ao Portal e/ou aos Serviços não concedem ao FORNECEDOR qualquer direito sobre as marcas, designações comerciais ou sinais distintivos que apareçam no mesmo, sendo que estes são propriedade da SODECIA ou de terceiros. Como consequência, os Conteúdos são propriedade intelectual da SODECIA ou de terceiros e não se pode considerar que foram cedidos ao FORNECEDOR.<br><br>


2. Objeto<br>
O presente acordo visa regulamentar as relações entre a SODECIA e o FORNECEDOR quanto a todos os aspetos que estão relacionados com a utilização do Portal.<br><br>

3. Obrigações do FORNECEDOR<br>
O fornecedor tem as seguintes obrigações:<br><br>

a. Proporcionar os dados que sejam necessários para o funcionamento correto do sistema, assim como mantê-los atualizados, comunicando o mais rápido possível qualquer alteração que os mesmos sofram.<br><br>

b. Garantir a autenticidade dos dados facultados como consequência do preenchimento dos formulários necessários para a subscrição dos Serviços. Para além disso, o FORNECEDOR irá atualizar as informações facultadas de modo a refletir sempre a sua situação real. Assim, o FORNECEDOR será o único responsável pelos danos e prejuízos causados à SODECIA como consequência de declarações inexatas ou falsas.<br><br>
c. Manter a plena confidencialidade em relação a todas as informações que sejam geradas nas relações entre o FORNECEDOR e a SODECIA.<br><br>

d. Não realizar qualquer modificação nas tecnologias de informação utilizadas no Portal, assim como não utilizar o mesmo com fins diferentes daqueles para os que foi previsto. Além disso, o FORNECEDOR não irá aceder a áreas não autorizadas do Portal.<br><br>

e. Cumprir fielmente os seus compromissos nas informações enviadas através do portal. No caso de o FORNECEDOR não demonstrar a diligência comercial necessária, ou não cumprir as obrigações celebradas, a SODECIA reserva-se o direito de excluir permanente ou temporariamente o FORNECEDOR do portal.<br><br>

f. O FORNECEDOR apenas deverá indicar os grupos de materiais referentes a bens ou serviços que, aquando da celebração do acordo, sejam comercializados, fabricados ou distribuídos pelo mesmo e que sejam do interesse comercial da SODECIA.<br><br>

g. O FORNECEDOR aceita que os orçamentos introduzidos no Portal sejam considerados como tendo o mesmo nível e validade de qualquer orçamento enviado por qualquer outro meio tradicional (carta, fax).<br><br>
h. O FORNECEDOR está obrigado a utilizar corretamente o Portal e os Serviços em conformidade com a legislação, o presente Aviso Legal e outra regulamentação relativa à utilização e às instruções que tenham chegado ao seu conhecimento, assim como com a moral e os bons costumes geralmente aceites e a ordem pública.
Assim, não irá utilizar o Portal ou qualquer um dos Serviços para fins ilícitos, proibidos no presente Aviso Legal, em prejuízo dos direitos ou interesses de terceiros. Em particular, e a título meramente indicativo, o FORNECEDOR compromete-se a não transmitir, divulgar ou colocar à disposição de terceiros as informações, dados, conteúdos, gráficos, ficheiros de áudio e/ou imagem, fotografias, gravações, software e, de forma geral, qualquer tipo de material que:<br><br>
1. Seja contrário aos direitos fundamentais e liberdades públicas reconhecidos na Constituição, em Tratados Internacionais e na legislação aplicável; <br>
2. Induza, incite ou promova atividades criminosas, contrárias à lei, à moral e bons costumes geralmente aceites ou à ordem pública; <br>
3. Seja falso, inexato ou possa induzir em erro, ou represente publicidade ilícita, enganosa ou desleal;<br>
4. Esteja protegido por direitos de propriedade intelectual ou industrial que pertençam a terceiros, sem a autorização dos mesmos;<br>
5. Viole os direitos à honra, à privacidade ou à própria imagem;<br>
6. Viole as normas relativas ao sigilo das comunicações;<br>
7. Represente concorrência desleal, ou prejudique a imagem empresarial da SODECIA ou de terceiros;<br>
8. Esteja infetado com vírus ou elementos semelhantes que possam danificar ou impedir o funcionamento correto do Portal, dos equipamentos informáticos ou dos seus ficheiros e documentos.
<br><br>

4. Direitos do FORNECEDOR<br>
O FORNECEDOR tem os seguintes direitos:<br><br>

1. Manter uma presença constante na base de dados da SODECIA, na sua qualidade de fornecedor registado. <br><br>

2. Receber pedidos de orçamentos ao abrigo das normas estabelecidas. 
<br><br>

5. Obrigações da SODECIA <br>
A SODECIA tem as seguintes obrigações:<br><br>

a. Manter atualizadas as informações que considere adequadas, não se responsabilizando pelos erros que se possam produzir por força maior ou caso fortuito.<br><br>

b. Manter a total confidencialidade de todas as informações relativas ao fornecedor, quer se tratem de informações proporcionadas pelo mesmo ou geradas nas relações entre o FORNECEDOR e a SODECIA.<br><br>

c. Facultar em qualquer momento ao fornecedor o estado dos seus dados na base de dados e não facultar o mesmo a terceiros salvo para permitir que o fornecedor apresente os seus orçamentos e os seus produtos sejam tidos em consideração como alternativa de fornecimento. Além disso, facultar em qualquer momento ao fornecedor o estado dos respetivos dados na base de dados, para que este, mediante notificação prévia por escrito, os altere ou elimine. 
<br><br>

6. Direitos da SODECIA<br>

a. O Utilizador dá o seu consentimento para que a SODECIA ceda os respetivos dados às suas empresas associadas ou do grupo a que pertence, assim como a outras com as quais celebre acordos, com o único fim de prestar o melhor serviço, respeitando sempre a legislação espanhola relativa à proteção dos dados pessoais. Além disso, o Utilizador aceita que a SODECIA ou as suas empresas associadas, filiais e empresas comuns (joint ventures) lhe enviem informações sobre quaisquer bens ou serviços comercializados, ou que possam ser comercializados no futuro, direta ou indiretamente pelas mesmas. A aceitação por parte do Utilizador para que os seus dados possam ser cedidos da forma estabelecida no presente parágrafo tem sempre um carácter revogável sem efeitos retroativos, em conformidade com o disposto na Lei Orgânica espanhola 15/1999, de 13 de dezembro. <br><br>

b. Tomar a opção de, caso o FORNECEDOR não cumpra as suas obrigações, exigir o devido cumprimento das mesmas ou rescindir o contrato, nos termos da cláusula 8 do presente Aviso Legal. <br><br>

7. Limitação de responsabilidade<br>

a. A SODECIA não assume qualquer responsabilidade que derive da indisponibilidade do sistema, de falhas da rede ou falhas técnicas que provoquem um corte uma interrupção do Portal. Além disso, a SODECIA não se responsabiliza pelos danos e prejuízos que resultem da propagação de vírus informáticos ou outros elementos no sistema.<br><br>
b. A SODECIA não assume qualquer responsabilidade quanto a pagamentos e/ou reclamações que resultem de ações pelos clientes em relação ao acordo entre esses clientes e o FORNECEDOR.<br><br>

c. A SODECIA não assume qualquer responsabilidade quanto a qualquer ação que seja contrária às leis, usos e costumes, ou que viole as obrigações estabelecidas no presente Aviso Legal, por parte de alguma pessoa da SODECIA, do FORNECEDOR ou de qualquer utilizador da rede. No entanto, sempre que tiver conhecimento de qualquer conduta referida no parágrafo anterior, a SODECIA irá adotar as medidas necessárias para resolver os possíveis litígios com a maior urgência e diligência. <br><br>
d. A SODECIA considera-se isenta de qualquer responsabilidade que resulte da interferência por parte de terceiros não autorizados no conhecimento das condições e circunstâncias de utilização do Portal e dos Serviços pelos FORNECEDORES.<br><br>


8. Rescisão do acordo<br>

Caso alguma das obrigações incluídas no presente Aviso Legal não seja cumprida, o FORNECEDOR ou a SODECIA terá de notificar esse incumprimento à parte que o tenha cometido, concedendo-lhe um prazo de quinze dias úteis após a notificação para retificar o mesmo. Se o incumprimento não tiver sido retificado após esse prazo, a outra parte poderá decidir que o presente acordo seja cumprido ou rescindido, com uma indemnização por danos e prejuízos em qualquer um dos casos, em conformidade com o que se encontra estabelecido no artigo 1.124 do Código Civil espanhol. No caso de a decisão tomada ser a rescisão, o FORNECEDOR e a SODECIA concordam que a respetiva notificação será suficiente para que esta produza todos os seus efeitos.<br><br>


9. Duração <br>

O presente acordo terá uma duração indefinida desde que não ocorra uma denúncia por escrito por qualquer uma das partes com, pelo menos, 1 mês de antecedência.
O procedimento para o cancelamento do registo será através do envio de um e-mail com o Assunto "Cancelamento do registo no portal" para o endereço atencionalcliente@(COMPANY).com. Caso não seja possível enviar um e-mail, será enviado um fax para o n.º 91 296 20 25 com a indicação dos dados básicos para identificar o FORNECEDOR.<br><br>


10. Notificações<br>

a. As notificações entre as partes poderão ser realizadas por qualquer via de direito que permita ter conhecimento da receção, incluindo o fax.<br><br>

b. As mudanças de domicílio e os faxes serão notificados por escrito e não produzirão efeitos até que tenham passado dois dias úteis após a sua receção. Em qualquer caso, o novo domicílio e fax estará necessariamente dentro do território espanhol.<br><br>

c. As relações entre a SODECIA e o FORNECEDOR que derivem da utilização do Portal e dos serviços prestados através do mesmo serão sujeitas à legislação e jurisdição espanholas. <br><br>



<br><br>
 
</div>
<br><br>
<div>
    <a href="../SOD/Eng/docs/SODECIA SUPPLIER MANUAL.pdf" class="btn" style="border:solid 0.1em #000; padding:0.3em; text-decoration:none;" target="_blank">MANUAL DO FORNECEDOR</a>
    <a href="../SOD/spa/docs/Imp 01 PS05 GRD 00 - Acordo Confidencialidade.pdf" class="btn" style="border:solid 0.1em #000; padding:0.3em; text-decoration:none; margin-left:1em;" target="_blank">CLÁUSULA DE CONFIDENCIALIDADE</a>
    <p class="imprimir"><a href="registro_texto.html" class="" target="blank">Imprimir</a> </p>
</div>

           
<div class="caja_acepto">
<form name="frmAlta" id="frmAlta" method="post" style="max-width:740px;">
  <span id="sprycheckbox1">

<span class="checkboxRequiredMsg">Seleccione para continuar.</span>
</span>
<br>
<br>

  <input type="checkbox" name="opcion1" id="opcion1" required>

Eu li e aceito as condiçoes gerais de utilizaçao do Portal de Fornecedores.</label><br />


<br />
<button type="button" class="bt_registro" id="submit1" name="submit1">Continuar</button>
<br>
</form>


</div>
<div style="clear:both;"></div>
    	</div>    
<p>&nbsp;</p></div>
</div>
<script type="text/javascript">
// <!--
// var sprycheckbox1 = new Spry.Widget.ValidationCheckbox("sprycheckbox1");
// //-->
$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked')) {
        $('#frmAlta').submit();
        alta();
	}
	else
       alert("Você deve aceitar os termos de uso do portal para registrar");
    
});

</script>
                      	</div>
                    </div>
                    
                 
               
           	  </div>
                    
         	</div>
              
    	</section>
</body>

<!--#include file="../../common/fsal_2.asp"-->

</html>
