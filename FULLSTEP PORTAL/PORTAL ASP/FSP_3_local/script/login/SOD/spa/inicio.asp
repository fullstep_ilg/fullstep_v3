<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<link href="../css/estilos.css" rel="stylesheet" type="text/css">
<title>::Portal de proveedores::</title>
<!--[if lte IE 9]>
<link href="../estilos_ie9.css" rel="stylesheet" type="text/css" />
<![endif]-->
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
}
-->
</style>
</head>
<script>
dibujaMenu(1)
</script>
<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function Init() {
    document.getElementById('tablemenu').style.display = 'block';

	MM_preloadImages('../images/icono_docs_sm.gif','../images/icono_docs_sm.gif')
	p = window.top.document.getElementById("frSet")
	vRows = p.rows
	vArrRows = vRows.split(",")
	vRows = vArrRows[0] + ",*,0,0"
	p.rows = vRows

}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">
<div id="inicio-left">
  <h1><span class="blue">Bem-vindo ao Portal de Fornecedores</span></h1>
  <!--<ul class="square">
  <li>--><p>Pode aceder às diferentes áreas através das opções de menu que se encontram na parte superior.  </p><!--</li>-->
  <ul>
   <li><span class="blue">Qualidade:</span> efetue a gestão dos seus certificados, Não Conformidades e aceda aos seus índices de qualidade.  </li>
    <li><span class="blue">Pedidos: </span> aceda aos pedidos (RFQ e outros pedidos) e efetue a respetiva gestão a partir desta secção. </li>
    <li><span class="blue">Encomendas:</span>  efetue a gestão das encomendas que lhe tenham sido realizadas pelos compradores, acompanhe a sua evolução e aceda ao seu histórico a partir desta secção.</li>
    
    <li><span class="blue"> Os seus dados/a sua empresa:</span> tem a possibilidade de alterar os dados da sua empresa, dos utilizadores e das áreas de atividade em que a sua empresa participa. </li>
   
 
   <!--   <li><span class="blue">Pedidos :</span> gestione desde aquí los pedidos que le han realizado los compradores, siga su evolución y acceda a su histórico.   </li></li>-->
  </ul>
<br><br>

<p>  Se for a primeira vez que irá efetuar uma oferta através do portal, siga cuidadosamente os seguintes passos:</p>
  <ol>
  <li><span class="rojo">Clique em "pedidos"</span>  para visualizar os pedidos de orçamento que a sua empresa tem pendentes.</li><br>
  <li><span class="rojo">Selecione o pedido de orçamento ao qual pretende responder  </span>ao clicar sobre o respetivo código.</li><br>
  <li><span class="rojo">Efetue o seu orçamento através do preenchimento de todas as informações necessárias: </span>  poderá deslocar-se pelas diferentes secções que compõem o orçamento a partir da árvore de navegação que se encontra no lado esquerdo. Para introduzir os preços, deverá aceder à secção "itens/preços". Não se esqueça de introduzir o prazo de validade do orçamento na secção "Dados Gerais do orçamento". </li><br>
  <li> <span class="rojo">Comunique o seu orçamento  </span> ao clicar no botão de enviar.<IMG height=14 src="../img/sobre_tr.gif" width=30 align=absBottom></li>
 </ol>
 
</div>
<div id="inicio-right">
<h2>INSTRUÇÕES</h2>
<p>Transfira as instruções sobre o modo como efetuar um orçamento, aceitar uma encomenda, acompanhamento, etc.</p>
<ul class="square">
<li><a href="docs/FSN_MAN_ATC_Como apresentar ofertas.pdf" target="_blank">Como efetuar o orçamento</a></li>
<li><a href="docs/FSN_MAN_ATC_Requisitos técnicos do fornecedor.pdf" target="_blank">Requisitos técnicos</a></li>
<li><a href="docs/FSN_MAN_ATC_Manutenção dos dados.pdf" target="_blank">Manutenção dos dados</a></li>
</ul>
<div id="img-atc"><img src="../img/imagen-ATC.jpg"></div>
<p>Caso não tenha conseguido resolver as suas dúvidas, entre em contacto connosco através do correio: 
				
				<a href="mailto:atencionalcliente@fullstep.com">atencionalcliente@fullstep.com</a></p>
</div>

</body>
</html>
