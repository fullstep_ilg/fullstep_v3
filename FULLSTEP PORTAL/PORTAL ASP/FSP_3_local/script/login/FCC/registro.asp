﻿<%@ Language=VBScript %>
<!--#include file="../../common/XSS.asp"-->
<html>


<head>

<title>Logiters - Solicitud de registro</title>

<link href="estilos.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--



function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
<style type="text/css">
<!--
.Estilo6 {color: #999999}
-->
</style>
<link href="estilos2.css" rel="stylesheet" type="text/css">
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=SPA", "_blank")
window.close()

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=550,height=600,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>


<body bgcolor="#FFFFFF" leftmargin="5" topmargin="5">
<form name="frmAlta" id="frmAlta" method="post">

<table border="0" width="100%">
  <tr>
    <td width="100%" valign="top"><font face="Verdana" size="1">
        
    <table width="100%" border="0">
      <tr>
            <td width="35%" valign="top"> 
             <table width="100%" cellspacing="0" cellpadding="0">
    <tr>
      <td width="10" height="72" rowspan="2">&nbsp;</td>
      <td width="249" valign="top"><a href="http://www.logiters.com" target="_blank"><img src="images/logo.jpg" width="200" height="68" border="0"></a></td>
    </tr>
    <tr>
      <td valign="top"><hr size="1"  color="#008136"></td>
    </tr>
    <tr>
      <td rowspan="5">&nbsp;</td>
      <td height="216" class="textos"><p><strong>CL&Aacute;USULA DE DEBER DE INFORMACI&Oacute;N A POTENCIALES PROVEEDORES</strong><br>
      </p>
        <p>En cumplimiento con la Ley Org&aacute;nica 15/1999, de Protecci&oacute;n de Datos de Car&aacute;cter Personal, le informamos que Logiters es responsable de un fichero de datos de car&aacute;cter personal, en el cual se encuentran incluidos los datos recabados de POTENCIALES PROVEEDORES, as&iacute; como los datos de las personas de contacto en los mismos, incluida su direcci&oacute;n de correo electr&oacute;nico, cuya finalidad es la gesti&oacute;n de las relaciones comerciales con Vds. y la realizaci&oacute;n de solicitudes de ofertas de contrataci&oacute;n. </p>
        <p>Resulta necesario que Vds. nos faciliten todos los datos solicitados, garantizando que los mismos son verdaderos, exactos, completos y actualizados y que se encuentra facultado para facilitar a Logiters los datos de la/s persona/s de contacto en la entidad.</p>
        <p>Le informamos que sus datos podr&aacute;n ser comunicados a las entidades integrantes de Logiters<font size="1" face="Verdana"> </font>cuyas direcciones figuran en la p&aacute;gina web <a href="http://www.logiters.com" target="_blank"><font size="1" face="Verdana">www.logiters.com</font></a>, para el cumplimiento de los mismos fines informados y en la medida en que dichas entidades necesiten contactar con empresas proveedoras de bienes y/o servicios como los ofrecidos por Vds. </p>
        <p>Al facilitarnos los datos referidos anteriormente, Vds. consienten expresamente el uso, tratamiento y comunicaci&oacute;n de los mismos para las finalidades informadas. </p>
        <p>A&uacute;n en el caso de que la oferta de contrataci&oacute;n remitida por Vds. no sea aceptada por la empresa que haya solicitado la misma, Vds. consienten expresamente que sus datos contin&uacute;en registrados en nuestros ficheros a los efectos de poder entrar nuevamente en contacto y mantener futuras relaciones comerciales con Vds., salvo que nos comunique expresamente lo contrario.</p>
        <p>En caso de tener lugar alguna modificaci&oacute;n y/o variaci&oacute;n en alguno de los datos existentes en nuestra base de datos, en particular, de los relativos a las personas de contacto, Vds. deber&aacute;n poner en nuestro conocimiento dicha circunstancia a fin de proceder a su actualizaci&oacute;n.</p>
        <p>Para el ejercicio de los derechos de acceso, rectificaci&oacute;n, cancelaci&oacute;n u oposici&oacute;n, deber&aacute; remitirse un escrito identificado con la referencia &ldquo;Protecci&oacute;n de Datos&rdquo;, en el que se concrete la solicitud correspondiente y al que acompa&ntilde;e fotocopia del Documento Nacional de Identidad del interesado, a la siguiente direcci&oacute;n: Logiters, calle Buenos Aires, nº 10 Polígono Industrial Camporroso, 28806 Alcalá de Henares de Madrid.  </p>        <p class="textos"><br>
        </p>
        </td>
    </tr>
    
    <tr>
      <td height="15"><hr size="1"  color="#909c9c"></td>
    </tr>
    <tr>
      <td height="29"><font face="Verdana" size="1"><em></em><span class="textos"><em>Para continuar con el proceso de alta es imprescindible que pulse el botón correspondiente:</em></span></font></td>
      </tr>
    <tr>
      <td><table width="50%" border="0" align="right">
        <tr>
          <td width="33%" align="left"><p align="left">
            <input type="submit" onClick="return alta()" value="Acepto" id="submit1" name="submit1">
</p></td>
          <td width="33%"><input type="button" onclick="no_alta()" value="No acepto" id="button1" name="button1">
          </td>
          <td width="33%" valign="middle">
            <div align="right">
              <input type="button" onclick="imprimir()" value="Imprimir" id="button2" name="button2">
            </div></td>
        </tr>
      </table></td>
    </tr>
  </table>
            </td>
        </tr>
    </table>
      </font>
           
      <p align="center" class="Estilo6">      
      </td>
  </tr>
</table>

	<input type="hidden" name="ACEPTADO">
	
</form>

</body>

</html>
