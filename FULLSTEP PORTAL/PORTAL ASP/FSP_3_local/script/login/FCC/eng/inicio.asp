﻿<%@ Language=VBScript %>
<!--#include file="../../../common/acceso.asp"-->
<%
Idioma = Request.Cookies("USU_IDIOMA")

set oRaiz = ValidarUsuario(Idioma,false,false,0)

DIM FICHERO_ENG 
FICHERO_ENG= Application("PATHFICHERONOTICIAS") & ".ENG"
sNoticias = oRaiz.leerFicheroTexto(FICHERO_ENG)

sNoticias = "<span style=""letter-spacing: 1pt""><font size=""2"" face=""verdana"" color=""#008136""><b>News</b></font></span><hr color=""#008136"">"
sNoticias = snoticias & oRaiz.leerFicheroTexto(FICHERO_ENG)

set oRaiz = nothing

%>

<html>

<head>
<script SRC="../../../common/menu.asp"></script>
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<link rel="stylesheet" href="../estilos.css">

<style>
.Estilo1 {font:Verdana, Arial, Helvetica, sans-serif; size:10px;
	color: #008136;
	
}
</style>
<script>
function init()
{
    document.getElementById('tablemenu').style.display = 'block';

    p = window.top.document.getElementById("frSet")
    vRows = p.rows
    vArrRows = vRows.split(",")
    vRows = vArrRows[0] + ",*,0,0"
    p.rows = vRows

}
</script>
<title>Logiters</title></head>
<body topmargin="0" leftmargin="0" scroll="yes" onload="init()">
<script>
dibujaMenu(1)
window.open("popup.htm","","width=550,height=700,scrollbars=YES")
</script>

<!-------------------------- BEGIN COPYING THE HTML HERE ---------------------------->
<table border="0" cellpadding="0" cellspacing="0" style="BORDER-COLLAPSE: collapse" bordercolor="#111111" width="100%" id="AutoNumber1" bgcolor="#ffffff" height="194">
  <tr>
    <td width="5%"></td>
    <td height="50" width="50%"></td>
    <td height="50" width="45%"></td>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="50%" height="1"><strong><font face="Verdana,arial" color="#00438C" size="2px"><b>&gt; Welcome to the Area Restricted to Suppliers</font></span></strong></td>
    <td rowspan="3" valign="top" width="45%" align="left"><div name="divNoticiasright" id="divNoticias" >
      <table align=center width=80%>
        <tr>
          <td><%=sNoticias%> </td>
        </tr>
      </table>
    </div></td>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="50%" height="180"><hr color="#008136">
    <p class="textosinicio">Here you will find the request for quotations and orders that Logiters has for your company.</p>
    <p class="textosinicio">You can access the various areas by clicking on the links on the bar at the top.</p>
    <ul>
    <li class="textosinicio"><b>Request for quotations :</b>You can access the processes open by Logiters, for which your company has been invited to tender.</li>
    <li class="textosinicio"><b>Orders: </b>You can monitor the orders made by Logiters for your company (only in respect to articles in its e-catalogue).</li>
    <li class="textosinicio"><b>Your details/ your company :</b> You can manage your company´s details and the trade areas in respect to which your company is compliant.</li></ul></td>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="41%" height="18"><hr color="#008136" size="1">
    <p class="textosinicio">If this is the first time you are accessing this page, you can download the instructions on how to submit bids,requirements,etc. ...by clicking on the corresponding icon below.</p>
    <hr color="#008136" size="1">
        <table cellSpacing="1" cellPadding="1" width="75%" border="0">
        <tr>
		  <td>&nbsp;</td>	
          <td width="4%"><a href="docs/MAN_206_master%20data.pdf" target="_blank"><img height="26" src="../images/pdf.gif" width="26" border="0"></a></td>
          <td><font face="verdana,arial" size="1px" color="#00438C">Supplier details maintenance</font></td>
          <td width="4%"><a href="docs/MAN_205_how%20to%20offer.pdf" target="_blank"><img height="26" src="../images/pdf.gif" width="26" border="0"></a></td>
          <td><font face="verdana,arial" size="1px" color="#00438C">How&nbsp;to&nbsp; offer&nbsp;</font></td>
          <td width="2%"><a href="docs/MAN_202_overview.pdf" target="_blank"><img height="26" src="../images/pdf.gif" width="26" border="0"></a></td>
          <td><font face="verdana,arial" size="1px" color="#00438C">General overview</font></td>
          <td width="3%"><a href="docs/MAN_203_technical%20requirements.pdf" target="_blank"><img height="26" src="../images/pdf.gif" width="26" border="0"></a></td>
          <td><font face="verdana,arial" size="1px" color="#00438C">Tecnical requirements</font></td></tr>
         </table>
    </td>
  </tr>
  <tr>
    <td width="1%"></td>
    <td width="50%" height="18">
    </td>
  <tr>
  <tr bgcolor="#00438C">
    <td colspan="3" height="6">
    <p align="center">&nbsp;<span class="textoblanco">For any further clarifications,
    please call  + 34 91 878 3845 or send an e-mail to</span><b><font face="verdana,arial" size="1px" color="#ffffff"> <a href="mailto:%20manoli.aguilar@fcclogistica.com" class="textoblanco">manoli.aguilar@fcclogistica.com</a></font></b></p></td>
  </tr>
</table>
</body></html>