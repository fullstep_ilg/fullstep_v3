﻿<%@ Language=VBScript %>
<!--#include file="../../../common/acceso.asp"-->
<%
Idioma = Request.Cookies("USU_IDIOMA")

set oRaiz = ValidarUsuario(Idioma,false,false,0)

DIM FICHERO_SPA 
FICHERO_SPA= Application("PATHFICHERONOTICIAS") & ".SPA"
sNoticias = oRaiz.leerFicheroTexto(FICHERO_SPA)

sNoticias = "<span style=""letter-spacing: 1pt""><font size=""2px"" face=""verdana,Arial"" color=""#00438C"" align=""left""><b>Noticias</b></font></span><hr color=""#008136"">"
sNoticias = snoticias & oRaiz.leerFicheroTexto(FICHERO_SPA)

set oRaiz = nothing

%>

<script SRC="../../../common/menu.asp"></script>
<html>
<head>
<link rel="stylesheet" href="../estilos.css">

<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<link rel="stylesheet" href="../estilos.css">
<style>
.Estilo1 {
	color: #008136;
	font-weight: bold;
}
.Estilo2 {color: #00438C}
.Estilo5 {font-family: Verdana, Arial, Helvetica, sans-serif}
</style>
<title>Logiters</title></head>
<script>
function init()
{
    document.getElementById('tablemenu').style.display = 'block';

    p = window.top.document.getElementById("frSet")
    vRows = p.rows
    vArrRows = vRows.split(",")
    vRows = vArrRows[0] + ",*,0,0"
    p.rows = vRows

}
</script>
<body topmargin="0" leftmargin="0" scroll="yes" onload="init()">
<script>
dibujaMenu(1)
window.open("popup.htm","","width=550,height=700,scrollbars=YES")

</script>

<!-------------------------- BEGIN COPYING THE HTML HERE ---------------------------->
<table width="100%" height="475" border="0" cellpadding="0" cellspacing="0" bordercolor="#111111" bgcolor="#ffffff" id="AutoNumber1" style="BORDER-COLLAPSE: collapse" color="#00438C">
  <tr>
    <td width="10"></td>
    <td height="50" width="50%"></td>
    <td height="50" width="45%"></td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td width="50%"><font face="Verdana,arial" color="#00438C"><b><font size="2">&gt; 
    </font></b></font><font face="Verdana,arial"><b><font size="2">        <span class="Estilo2">Bienvenido a la ZONA PRIVADA DE 
    PROVEEDORES</span></font></b></font></td>
    <td width="45%" rowspan="3" align="left" valign="top">
	  <div  id="divNoticias" name="divNoticias" >
      <table align=center width=80%>
        <tr>
          <td ><%=sNoticias%> </td>
        </tr>
      </table>
    </div>
	</td>
  </tr>
  <tr>
    <td width="10" class="textosinicio">&nbsp;</td>
    <td width="50%" height="180" class="textosinicio"><hr color="#008136">
    <p class="textosinicio"> Aquí encontrará las solicitudes de oferta y pedidos que tiene ID Logistics para su empresa.</p>
    <p class="textosinicio">Puede acceder a las distintas áreas a través de los vínculos situados en la zona superior</p>
<ul class="textosinicio">    
	<li ><b>Solicitudes de oferta :</b> Accederá a los procesos de compra abiertos por  Logiters para los que su empresa ha sido invitada.</li><br>
	<li><b>Pedidos: </b>Podrá realizar el seguimiento de los pedidos del catálogo electrónico cursados por Logiters.</li>    <br>
  <li><b>Sus datos/ su compañía :</b> Puede gestionar los datos de su empresa, así como las áreas de actividad en la que su empresa se encuentra homologada.</li>
  </ul></td>
  </tr>
  <tr>
    <td class="textosinicio">&nbsp;</td>
    <td class="textosinicio"><hr color="#008136" size="1">Si es la primera vez que accede a esta zona puede descargar las instrucciones sobre como ofertar, requisitos etc...pulsando en el icono correspondiente</p>
    <hr color="#008136" size="1">
      <table cellSpacing=1 cellPadding=1 width="100%" border="0" face="verdana,arial" size="1px">
        <TR>
          <TD width="3%"><a href="docs/FSN_MAN_ATC_Mantenimiento de datos.pdf" target="_blank"><IMG height=26 src="../images/pdf.gif" width="26" border="0"></a></TD>
          <TD><font face="verdana,arial" size="1px" color="#00438C">Mantenimiento de datos</font></TD>
          <TD width="3%"><A href="docs/FSN_MAN_ATC_Cómo ofertar.pdf" target=_blank><IMG height=26 src="../images/pdf.gif" width=26 border="0"></a></TD>
          <TD><font face="verdana,arial" size="1px" color="#00438C">Cómo ofertar</font></TD>
          <TD width="2%">&nbsp;</TD>
          <TD>&nbsp;</TD>
          <TD width="3%"><A href="docs/FSN_MAN_ATC_Requisitos técnicos.pdf" target=_blank><IMG height=26 src="../images/pdf.gif" width=26 border="0"></a></TD>
          <TD><font face="verdana,arial" size="1px" color="#00438C">Requisitos técnicos</font></TD>
           <TD width="3%"><A href="docs/FSN_MAN_ATC_Gestión de pedidos.pdf" target=_blank><IMG height=26 src="../images/pdf.gif" width=26 border="0"></a></TD>
          <TD><font face="verdana,arial" size="1px" color="#00438C">Gestión de pedidos</font></TD>

  </TR>
      </table>
    </td>
  </tr>
  <tr>
    <td width="10"></td>
    <td width="50%" height="18">
    </td>
  <tr bgcolor="#00438C">
    <td width="100%" height="6" colspan="3">
    <p align="center" class="textoblanco">&nbsp;Para cualquier aclaración env&iacute;enos un e-mail a <a href="mailto:ajurado@id-logistics.com" class="textoblanco">ajurado@id-logistics.com</a></p>
    </td>
  </tr>
</table>
</body></html>