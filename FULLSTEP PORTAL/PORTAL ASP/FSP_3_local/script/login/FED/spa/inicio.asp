﻿<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->

<script SRC="../../../common/menu.asp"></script>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<style>
#divNoticias {position:relative;top:1px;width:100%;height:95%;overflow-y:scroll;color:"#000000";padding-right:10px;}
</style>
<style type="text/css">
<!--
.textos {  font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-style: normal; font-weight: normal; color: #000000; text-decoration: none}
-->
</style>
<link href="../estilos2.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {font-size: 11px; font-style: normal; font-weight: normal; text-decoration: none; font-family: Arial, Helvetica, sans-serif;}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
	/*''' <summary>
	''' Iniciar la pagina.
	''' </summary>     
	''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
	function Init() {
		document.getElementById('tablemenu').style.display = 'block';
	}
</script>
</head>

<body topmargin="0" leftmargin="0" scroll="yes" onLoad="Init()">
<script>
dibujaMenu(1)

	p = window.top.document.getElementById("frSet")
	vRows = p.rows
	vArrRows = vRows.split(",")
	vRows = vArrRows[0] + ",*,0,0"
	p.rows = vRows


</script>
<%
''' <summary>
''' Pantalla inicial tras hacer un login valido
''' </summary>
''' <remarks>Llamada desde: login\FEDERLAN\inicio.asp ; Tiempo mÃ¡ximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,false,false,0)

DIM FICHERO_SPA 
FICHERO_SPA= Application("PATHFICHERONOTICIAS") & ".SPA"
sNoticias = oRaiz.leerFicheroTexto(FICHERO_SPA)

set oRaiz = nothing

%>

<!-------------------------- BEGIN COPYING THE HTML HERE ---------------------------->

<table width="100%" height="438" border="0" bordercolor="0" vspace="0" hspace="0" cellspacing="0">
  <tr> 
	<td width="1%" rowspan="10"></td>
	<td colspan="12" width="70%"><font face="arial" size="5" COLOR="#b1d2e2"><b>&gt;&nbsp;Bienvenido 
	  a la zona privada de proveedores</b></font></td>
	<td width="5"><b><font face="arial" size="5" color="#B1D2E2">&gt;&nbsp;Noticias</font></b></td>
  </tr>
  <tr> 
	<td colspan="12" width="70%"><span class="textos">
	<font color="#05143F">Aquí encontrará 
	  las solicitudes de oferta que</font></span><span class="Estilo1"><font color="#05143F"> el Grupo Fagor Ederlan </font></span><span class="textos"><font color="#05143F">tiene para su compañ</font></span><font color="#05143F">í</font><span class="textos"><font color="#05143F">a.<BR>
	  <BR>
	  Puede acceder a las distintas áreas através de los vínculos situados 
	en la parte superior.</font></span></td>
	<td width="50%" rowspan="9"><div name="divNoticias" id="divNoticias"><%=sNoticias%></div></td>
  </tr>
  <tr> 
  <td colspan="1" width="16"> 
		<img src="../images/bolita.gif" width="16" height="17" align="center">
  </td>
	<td colspan="11"> 
		<span class="textos">
		<font color="#05143F"><b>Sus datos/ su compañia :</b>
		Puede gestionar los datos de su empresa, así como las áreas de actividad 
		en las que su empresa se encuentra homologada. </font></span>
	</td>
  </tr>
  <tr> 
	  <td colspan="1" width="16"> 
		<img src="../images/bolita.gif" width="16" height="17" align="center">
  </td>
   <td height="22" colspan="11"> 
		<span class="textos">
		<font color="#05143F"><B>Solicitudes de ofertas 
		  :</B> Puede acceder a los procesos de compra abiertos por </font></span><span class="Estilo1"><font color="#05143F">el Grupo Fagor Ederlan,</font></span><span class="textos"><font color="#05143F"> 
		  para los que su empresa ha sido invitada. </font></span>
	</td>
  </tr>
  <tr> 
  <td colspan="1" width="16"> 
		<img src="../images/bolita.gif" width="16" height="17" align="center">
  </td>
	<td colspan="11"> 
		<span class="textos">
		<font color="#05143F"><B>Pedidos:</B> Podrá 
		  realizar el seguimiento de los pedidos del catálogo electrónico cursados 
		  por </font></span><span class="Estilo1"><font color="#05143F">el Grupo Fagor Ederlan.</font></span></td>
  </tr>
  <tr>
	<td colspan="1"><img src="../images/bolita.gif" width="16" height="17" align="center"></td>
	<td colspan="11"> <span class="Estilo1"><font color="#05143F"><B>Calidad</B></font></span><span class="textos"><font color="#05143F"><B>:</B> Podr&aacute;</font></span><span class="Estilo1"><font color="#05143F"> adjuntar sus certificados de calidad, gestionar sus No Conformidades y consultar sus calificaciones.</font></span> </td>
  </tr>
	  <td colspan="12" align="left" bordercolor="0" width="30%"><hr color="#B1D2E2" size="2"></td>
  <tr> 
	<td colspan="12" bordercolor="0" width="50%">
	<span class="textos">
	<font color="#05143F">Si es la primera vez que accede a esta zona, puede descargar las instrucciones 
	  sobre c&oacute;mo ofertar, &nbsp;requisitos, etc. ...pulsando en el icono correpondiente.</font>    </span> 
   </td>
  </tr>
  <tr> 
	<td colspan="12" bordercolor="0" width="50%"><hr color="#B1D2E2" size="2"></td>
  </tr>
  <tr> 
	<td width="2%" bordercolor="0"><div align="left"><a href="docs/MAN_206_Mantenimiento%20datos_3_0.pdf" target=_blank><img src="../images/pdf.gif" width="26" height="26" border="0" alt="Manual de Mantenimiento de datos"></a></div></td>
	<td width="8%" align="left" bordercolor="0"><div align="left"><span class="textos"><font color="#05143F"><a href="docs/MAN_206_Mantenimiento%20datos_3_0.pdf" target="_blank">Mantenimiento de datos</a></font></span></div></td>
	<td width="2%" bordercolor="0"><div align="left"><a href="docs/MAN_205_C&#243;mo%20ofertar_3_0.pdf" target=_blank><img src="../images/pdf.gif" width="26" height="26" border="0" alt="Manual sobre c&oacute;mo ofertar"></a></div></td>
	<td width="8%" align="left" bordercolor="0"><div align="left"><span class="textos"><font color="#05143F"><a href="docs/MAN_205_C&#243;mo%20ofertar_3_0.pdf" target="_blank">C&oacute;mo ofertar</a></font></span></div></td>
	<td width="2%" bordercolor="0"><div align="left"><a href="docs/MAN_202_visi&#243;n%20general%20del%20proveedor_3_0.pdf" target=_blank><img src="../images/pdf.gif" width="26" height="26" border="0" alt="Manual sobre visi&oacute;n general del proveedor"></a></div></td>
	<td width="8%" align="left" bordercolor="0"><div align="left"><span class="textos"><font color="#05143F"><a href="docs/MAN_202_visi&#243;n%20general%20del%20proveedor_3_0.pdf" target="_blank">Visi&oacute;n General </a></font></span></div></td>
	<td width="2%" bordercolor="0"><div align="left"><a href="docs/MAN_203_Requisitos%20t&#233;cnicos%20proveedor_3_0.pdf" target=_blank><img src="../images/pdf.gif" width="26" height="26" border="0" alt="Manual de Requisitos t&eacute;cnicos"></a></div></td>
	<td width="8%" align="left" bordercolor="0"><div align="left"><span class="textos"><font color="#05143F"><a href="docs/MAN_203_Requisitos%20t&#233;cnicos%20proveedor_3_0.pdf" target="_blank">Requisitos T&eacute;cnicos</a></font></span></div></td>
	<td width="2%" align="left" bordercolor="0"><div align="left"><a href="docs/MAN_208_Como%20adjuntar%20un%20certificado.pdf" target=_blank><img src="../images/pdf.gif" width="26" height="26" border="0" alt="Manual de certificados"></a></div></td>
	<td width="8%" align="left"><div align="left"><span class="textos"><font color="#05143F"><a href="docs/MAN_208_Como%20adjuntar%20un%20certificado.pdf" target="_blank">Cómo adjuntar un certificado</a></font></span></div></td>
	<td width="8%" align="left"><a href="docs/MAN_209_%20Como%20gestionar%20una%20no%20conformidad.pdf" target=_blank><img src="../images/pdf.gif" width="26" height="26" border="0" alt="Manual de No Conformidades"></a></td>
	<td width="8%" align="left"><div align="left"><span class="textos"><font color="#05143F"><a href="docs/MAN_209_%20Como%20gestionar%20una%20no%20conformidad.pdf" target="_blank">C&oacute;mo cumplimentar No Conformidades </a></font></span></div></td>
  </tr>
  <tr> 
	<td width="1%">&nbsp;</td>
	<td width="98%" colspan="13" align="center" valign="top" bordercolor="0" bgcolor="#B1D2E2" class="textos"> 
		<font color="#000000"> Para resolver sus dudas o hacernos llegar sus comentarios, p&oacute;ngase en contacto con nosotros en el tel&eacute;fono</font> 943 719 037 o 
		<font color="#000000">enviando un correo electr&oacute;nico a: <a href="mailto:compras@fagorederlan.es">compras@fagorederlan.es</a>  </font></td>
  </tr>
</table>
</body></html>