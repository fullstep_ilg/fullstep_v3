﻿<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="../estilos2.css" rel="stylesheet" type="text/css">
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body>
    <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
    <input type="hidden" name="idioma" value="spa" />
    <table width="620" border="0" cellspacing="0" cellpadding="0" class="textos">
        <tr>
            <td width="20">&nbsp;
                
            </td>
            <td width="615" align="left"><a href="https://compras.fagorederlan.es" target="_blank"><img src="../images/logo.jpg" border="0" alt=""/></a>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;
                
            </td>
        </tr>
        <tr>
            <td width="20">&nbsp;</td>
            <td width="615" align="left">
                <b><u>Recordatorio de claves de acceso </u></b>
                <br/>
                <br />
                <span>
                    Si ha olvidado su contraseña introduzca su código de compañia, de usuario y la dirección de e-mail de su cuenta de acceso y pulse "Enviar". Recibirá un e-mail con el enlace a la página donde podrá crear una nueva contraseña.<br>
                </span>
                <br/>
                <table class="textos">
                    <tr>
                        <td>Cód. Compañia:</td>
                        <td><input type="text" name="txtCia" id="txtCia" size="80" maxlength="100"/></td>
                    </tr>
                    <tr>
                        <td>Cód. Usuario:</td>
                        <td><input type="text" name="txtUsu" id="txtUsu" size="80" maxlength="100"/></td>
                    </tr>
                    <tr>
                        <td>Dirección de email:</td>
                        <td><input type="text" name="txtEmail" id="txtEmail" size="80" maxlength="100"/></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="20">&nbsp;</td>
            <td>
                <br /><input type="submit" value="Enviar" name="cmdEnviar" id="cmdEnviar"/>
                <br /><br />Si continúa con dificultades para poder acceder al portal o su dirección de email
                    ha cambiado, consulte con el servicio de atención a proveedores: Tel. 943 719 037
                    o envíe un correo electrónico a: <a href="mailto:%20compras@fagorederlan.es">compras@fagorederlan.es</a>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
