﻿<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="../estilos2.css" rel="stylesheet" type="text/css">
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body>
    <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
    <input type="hidden" name="idioma" value="eng">
    <table width="620" border="0" cellspacing="0" cellpadding="0" class="textos">
        <tr>
            <td width="20">&nbsp;
                
            </td>
            <td width="615" align="left"><a href="https://compras.fagorederlan.es" target="_blank"><img src="../images/logo.jpg" border="0" alt=""/></a></td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;
                
            </td>
        </tr>
        <tr>
            <td width="20">&nbsp;
                
            </td>
            <td width="615" align="left">
                <b><u>Access data reminder</u></b>
                <br />
                <br />
                <br>
                    If you forgot your login data, fill in the registered e-mail address and you will
                    receive an e-mail with your login and password.
                    <br>
                    <br>
                    <br>
                    <table class="textos">
                        <tr>
                            <td>Company code:</td>
                            <td><input type="text" name="txtCia" id="txtCia" size="80" maxlength="100"></td>
                        </tr>
                        <tr>
                            <td>User Code:</td>
                            <td><input type="text" name="txtUsu" id="txtUsu" size="80" maxlength="100"></td>
                        </tr>
                        <tr>
                            <td>E-mail address:</td>
                            <td><input type="text" name="txtEmail" id="txtEmail" size="80" maxlength="100"></td>
                        </tr>
                    </table>
            </td>
        </tr>
        <tr>
            <td width="20">&nbsp;
                
            </td>
            <td>
                <br />
                <input type="submit" value="Enviar" name="cmdEnviar" id="cmdEnviar" />
                <br />
                <br />
                If you continue to have trouble accessing the platform, , or your e-mail address
                has changed contact the supplier call center (tel. +34 943 719 037) or send an e-mail
                to: <a href="mailto:%20compras@fagorederlan.es">compras@fagorederlan.es</a><br />
                <br />
                Regards,
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
