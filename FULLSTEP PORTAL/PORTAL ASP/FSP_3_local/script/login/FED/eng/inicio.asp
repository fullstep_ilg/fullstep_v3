﻿<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->

<script SRC="../../../common/menu.asp"></script>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<style>
#divNoticias {position:relative;top:1px;width:100%;height:95%;overflow-y:scroll;color:"#000000";padding-right:10px;}
</style>
<style type="text/css">
<!--
.textos {  font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-style: normal; font-weight: normal; color: #000000; text-decoration: none}
.Estilo2 {font-size: 11px; font-style: normal; font-weight: normal; text-decoration: none; font-family: Arial, Helvetica, sans-serif;}
-->
</style>
<link href="../estilos2.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
    function Init() {
        document.getElementById('tablemenu').style.display = 'block';
        p = window.top.document.getElementById("frSet")
        vRows = p.rows
        vArrRows = vRows.split(",")
        vRows = vArrRows[0] + ",*,0,0"
        p.rows = vRows
    }
</script>
</head>

<body topmargin="0" leftmargin="0" scroll="yes" onload="Init()">
<script>
dibujaMenu(1)
</script>
<%
''' <summary>
''' Pantalla inicial tras hacer un login valido
''' </summary>
''' <remarks>Llamada desde: login\FEDERLAN\inicio.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,false,false,0)


DIM FICHERO_SPA 
FICHERO_SPA= Application("PATHFICHERONOTICIAS") & ".SPA"
sNoticias = oRaiz.leerFicheroTexto(FICHERO_SPA)

set oRaiz = nothing

%>
<script language=javascript>

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}

</script>
<!-------------------------- BEGIN COPYING THE HTML HERE ---------------------------->

<table width="100%" height="438" border="0" bordercolor="0" vspace="0" hspace="0" cellspacing="0">
  <tr> 
    <td width="1%" rowspan="10"></td>
    <td colspan="12" width="50%"><font face="arial" size="4" COLOR="#b1d2e2"><b>&gt;&nbsp;Welcome to the AREA RESTRICTED TO SUPPLIERS</b></font></td>
    <td width="50%"><b><font face="arial" size="4" color="#B1D2E2">&gt;&nbsp;News</font></b></td>
  </tr>
  <tr> 
    <td colspan="12" width="50%"><span class="textos">
    <font color="#05143F">Here you will find the request for quotations that </font></span><span class="Estilo2"><font color="#05143F">Fagor Ederlan Group</font></span> <span class="textos"><font color="#05143F">has for your company.<BR>
      <BR>
    You can access the various areas by clicking on the links on the bar at the top.</font> </span></td>
    <td width="50%" rowspan="9"><div name="divNoticias" id="divNoticias"><%=sNoticias%></div></td>
  </tr>
  <tr> 
  <td colspan="1" width="16"> 
        <img src="../images/bolita.gif" width="16" height="17" align="center">
  </td>
    <td colspan="11"> 
        <span class="textos">
        <font color="#05143F"><b>Your details/ your company :</b>
        You can manage your company´s details and the trade areas in respect of which your company is compliant.</font></span>
    </td>
  </tr>
  <tr> 
      <td colspan="1" width="16"> 
        <img src="../images/bolita.gif" width="16" height="17" align="center">
  </td>
   <td height="22" colspan="11"> 
        <span class="textos">
        <font color="#05143F"><b>Request for quotations :</b> You can access the processes opened by F</font></span><span class="Estilo2"><font color="#05143F">agor Ederlan Group</font></span><span class="textos"><font color="#05143F">, for which your company has been invited to tender. </font></span>
    </td>
  </tr>
  <tr> 
  <td colspan="1" width="16"> 
        <img src="../images/bolita.gif" width="16" height="17" align="center">
  </td>
    <td colspan="11"> 
        <span class="textos">
        <font color="#05143F"><b>Orders:</b> You can monitor the orders made by</font></span><font color="#05143F"> Fagor Ederlan Group f</font><span class="textos"><font color="#05143F">or your company (only in respect of articles in</font></span><span class="Estilo2"><font color="#05143F"> their </font></span><span class="textos"><font color="#05143F"> e-catalogue).</font></span>
    </td>
  </tr>
  <tr>
    <td colspan="1"><img src="../images/bolita.gif" width="16" height="17" align="center"></td>
    <td colspan="11"><span class="textos"> <font color="#05143F"><b>Quality:</b> You</font></span><span class="Estilo2"><font color="#05143F"> will be able to attach quality certificates, manage your Non-conformities and check your ratings </font></span><span class="textos"><font color="#05143F">.</font></span> </td>
  </tr>
      <td colspan="12" align="left" bordercolor="0" width="30%"><hr color="#B1D2E2" size="2"></td>
  <tr> 
    <td colspan="12" bordercolor="0" width="50%">
    <span class="textos">
    <font color="#05143F">&nbsp;If this is the first time you are accessing this page, you can download the instructions on how to submit bids, requirements, etc. ...by clicking on the corresponding icon below.</font></span> 
   </td>
  </tr>
  <tr> 
    <td colspan="12" bordercolor="0" width="50%"><hr color="#B1D2E2" size="2"></td>
  </tr>
  <tr> 
    <td width="2%" bordercolor="0"><a href="docs/MAN_206_master%20data_3_0.pdf" target=_blank><img src="../images/pdf.gif" width="26" height="26" border="0" alt="Manual de Mantenimiento de datos"></a></td>
    <td width="8%" align="left" bordercolor="0"><span class="textos"><font color="#05143F"><a href="docs/MAN_206_master%20data_3_0.pdf" target="_blank">Supplier details maintenance</a></font></span></td>
    <td width="2%" bordercolor="0"><a href="docs/MAN_205_how%20to%20offer_3_0.pdf" target=_blank><img src="../images/pdf.gif" width="26" height="26" border="0" alt="Manual sobre c&oacute;mo ofertar"></a></td>
    <td width="8%" align="left" bordercolor="0"><span class="textos"><font color="#05143F"><a href="docs/MAN_205_how%20to%20offer_3_0.pdf" target="_blank">How&nbsp;to&nbsp; offer&nbsp;</a></font></span></td>
    <td width="2%" bordercolor="0"><a href="docs/01%20overview.pdf" target=_blank><img src="../images/pdf.gif" width="26" height="26" border="0" alt="Manual sobre visi&oacute;n general del proveedor"></a></td>
    <td width="8%" align="left" bordercolor="0"><span class="textos"><font color="#05143F" ><a href="docs/01%20overview.pdf" target="_blank">General overview</a></font></span></td>
    <td width="2%" bordercolor="0"><a href="docs/MAN_203_technical%20requirements_3_0.pdf" target=_blank><img src="../images/pdf.gif" width="26" height="26" border="0" alt="Manual de Requisitos t&eacute;cnicos"></a></td>
    <td width="8%" align="left" bordercolor="0"><span class="textos"><font color="#05143F" ><a href="docs/MAN_203_technical%20requirements_3_0.pdf" target="_blank">Technical requirements</a></font></span></td>
    <td width="2%" align="left" bordercolor="0"><a href="docs/MAN_208_Sending%20a%20certificate.pdf" target=_blank><img src="../images/pdf.gif" width="26" height="26" border="0" alt="Manual de Requisitos t&eacute;cnicos"></a></td>
    <td width="8%" align="left"><div align="left"><span class="textos"><font color="#05143F" ><a href="docs/MAN_208_Sending%20a%20certificate.pdf" target="_blank">How to attach a certificate</a></font></span></div></td>
    <td width="2%" align="left"><a href="docs/MAN_209_How%20to%20complete%20a%20Non%20Conformity.pdf" target="_blank"><img src="../images/pdf.gif" width="26" height="26" border="0" alt="Manual de Requisitos t&eacute;cnicos"></a></td>
    <td width="8%" align="left"><div align="left"><span class="textos"><font color="#05143F" ><a href="docs/MAN_209_How%20to%20complete%20a%20Non%20Conformity.pdf" target="_blank">How to complete a Non Conformity</a></font></span> </div></td>
  </tr>
  <tr> 
    <td width="1%">&nbsp;</td>
    <td width="98%" colspan="13" align="center" valign="top" bordercolor="0" bgcolor="#b1d2e2" class="textos"> 
        <font color="#000000">For any further clarifications, please call us at +34 </font>
        943 719 037, or <font color="#000000">send an email to : compras@fagorederlan.es</font></td>
  </tr>
</table>
</body></html>