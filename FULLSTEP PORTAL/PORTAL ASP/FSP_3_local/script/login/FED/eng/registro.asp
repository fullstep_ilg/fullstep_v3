﻿<%@ Language=VBScript %>
<!--#include file="../../../common/XSS.asp"-->
<html>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Legal notice</title>

<link href="../estilos2.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=ENG", "_blank")
window.close()

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=550,height=600,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>


<body bgcolor="#FFFFFF" leftmargin="5" topmargin="5" onLoad="MM_preloadImages('../images/fichero_r.gif','../images/form_r.gif')">
<form name="frmAlta" id="frmAlta" method="post">
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><a href="https://compras.fagorederlan.es" target="_blank"><img src="../images/logo.jpg" border="0" alt=""/></a></td>
    </tr>
  <tr>
    <td><div align="center"><font face="Verdana" size="1">
        </font>
      </div>      
      <hr width="100%" size="1" color="#4f86ae">
      <p align="center"><font size="1" face="Verdana"><b><font color="#8AB3C7">To proceed with the sign up proccess, the following legal disclaimer must be read and accepted.</font> </b> </font></p></td>
    </tr>
</table>
<table border="0" width="100%">
  <tr>
    <td width="100%"><font face="Verdana" size="1">
        
    <table width="100%" border="0">
      <tr>
            <td width="40%" valign="top"> 
              <p align="center"> 
            </td>
        <td width="60%"><font face="arial" size="1">
        <textarea readonly rows="9" name="S1" cols="63" style="font-family: arial; font-size: 8pt; text-align:justify; display:block; height:135px; line-height: 150%; list-style-type: lower-alpha">
CONDITIONS OF USE SOURCING PLATFORM:

Preliminary note: The word "supplier" is also used in the following for such companies which are not currently suppliers of Fagor Ederlan Group, but who merely endeavour to establish a business relationship.
Companies who are active suppliers to Fagor Ederlan Group  or would like to make applications as such can take part in future invitations to tender via the supply portal http://compras.fagorederlan.es. A pre-condition for new suppliers is a self-description of the company data.
There exists no legal right on the part of the supplier to participate in invitations to tender.
The Fagor Ederlan Group reserves the right to select and decline suppliers or to exclude them from the platform according to its own criteria.
For the supply portal, one key user per company is established.
The key user acts in the name of the company, which is obliged to keep the data continually up-to-date.
This user is responsible for, as coordinator for the company, the registration and deletion of further users.
The maintenance of user data, the registration and deletion of users as well as the maintenance of the data about the company (addresses, product groups etc.) is carried out by the key user of the individual suppliers.
The key user acts in the name of the company, which is obliged to keep the data continually up-to-date.
The supplier will be informed about the access data for the key-user access.
This data is to be treated confidentially, the same applies to the access data of further users. In particular, it should be noted that access data should not be published and access is to be restricted only to those persons directly involved in the business relationship.
Passwords of adequate complexity must be chosen and changed regularly.
All information and documents exchanged in the context of the business relationship must be dealt with in strict confidence.
Drawings, descriptions, specifications and similar may be saved and duplicated only in the context of the operational requirements.
In the event of data being passed on to a third party by suppliers, (e.g. to sub-suppliers) a corresponding obligation to confidentiality has to be guaranteed.
No guarantee is made for the permanent, fault-free availability of the electronic systems. The Fagor Ederlan Group reserves the right to reduce or to cease operation of the platform at any time.
The supplier is responsible for the accessibility to the platform ("Internet access") and no costs will granted for this.
All participants commit themselves to carry out the required safety measures in accordance with the respective technological standards.
The supplier commits himself not to put any documents, files and data into the platform which could endanger and/or impair its operation.
In particular, measures must be taken not to spread any computer viruses.
Electronically transmitted declarations of intention are fully valid, and declarations made per fax or telephone are legally binding.
For the contractual components as well as for the tendering, the details entered by the respective supplier into the platform apply solely and exclusively. Any written supplements are not valid, unless these are reconfirmed by the Fagor Ederlan Group  in writing.

All information sent by you through this purchasing portal will become part of the Fagor Ederlan Group database. You are able to consult and modify your data or cancel your registration. The owner and responsible for the database is: 
  Fagor Ederlan S.Coop. 
  Paseo Torrebaso 7
  20540 Eskoriatza 
  CIF: F-20025292
 
The Fagor Ederlan Group makes a commitment to use your data exclusively for offers requests and to send communications related to the group purchasing function and not to provide your data to a third party without your consent. If you want to cancel your registration and you DO NOT want to 
 receive neither our offers requests nor our information, please send an email to compras@fagorederlan.es with the subject "cancel registration".
 Upon registration, the supplier company recognises the conditions listed above. 
        </textarea>
</font>
        </td>
      </tr>
    </table>
      </font>
           
      <table width="616" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="12" rowspan="2" bgcolor="#999999"></td>
          <td width="218" height="102" bgcolor="#999999"><p class="textosbl">In order to complete the registration of your company, you need to fill a form regarding the anti-virus used by your company. Please download the file from the &ldquo;download button&rdquo;, fill the required information and send it to the following e-mail address: <a href="mailto:compras@fagorederlan.es">compras@fagorederlan.es</a>.</p>            
            <FONT face=Verdana color=#FFFFFF size=1><SPAN 
style="FONT-FAMILY: Verdana"><o:p></o:p></SPAN></FONT> </td>
          <td width="34" rowspan="2" bgcolor="#999999" class="textos">&nbsp;</td>
          <td width="37" rowspan="2">&nbsp;</td>
          <td width="315" rowspan="2"><font size="1" face="Verdana" class="textos"><b>Do you accept the sign up legal disclaimer to the Fagor Ederlan Group purchasing portal?<br>
          </b> Shall you click on &#145;No, I refuse&#146;, the proccess will terminate and your company will not become a Fagor Ederlan Group supplier. <br>
          In order to continue the sign up proccess, you may click on &#145;Yes, I accept&#146;. <br>
          If you wish to print out the legal disclaimer, you may do so by clicking on the following buttom.</font></td>
        </tr>
        <tr>
          <td height="4" align="center" bgcolor="#999999"></td>
        </tr>
        <tr>
          <td bgcolor="#999999">&nbsp;</td>
          <td align="center" bgcolor="#999999"><a href="../eng/docs/antivirus_eng.doc" target="_blank" onMouseOver="MM_swapImage('Image1','','../images/fichero_r.gif',1);MM_swapImage('Image1','','../images/form_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../images/form.gif" name="Image1" width="80" height="14" border="0" id="Image1"></a></td>
          <td bgcolor="#999999">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="4">&nbsp;</td>
          <td><table border="0" width="37%">
            <tr>
              <td width="41%"><p align="center">
                  <input type="submit" onclick="return alta()" value="Yes, I Accept" id=submit1 name=submit1>
              </p></td>
              <td width="38%"><input type="button" onclick="no_alta()" value="No, I refuse" id=button1 name=button1>
              </td>
              <td width="21%"><input type="button" onclick="imprimir()" value="Print" id=button2 name=button2>
              </td>
            </tr>
          </table></td>
        </tr>
      </table>
          </td>
  </tr>
</table>

	<input type="hidden" name="ACEPTADO">
	
</form>

</body>

</html>
