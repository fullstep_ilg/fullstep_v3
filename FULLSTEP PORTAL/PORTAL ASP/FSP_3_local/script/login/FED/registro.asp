﻿<%@ Language=VBScript %>
<html>

<head>

<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/idioma.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->

<%
''' <summary>
''' Mostrar el detalle del registro
''' </summary>
''' <remarks>Llamada desde: login/login.asp		login/codigo instalaci�n/registro.asp	login/codigo instalaci�n/codigo idioma/registro.asp; Tiempo m�ximo: 0</remarks>
%>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Condiciones de uso del portal</title>

<link href="estilos2.css" rel="stylesheet" type="text/css">

<script src="../../common/formatos.js"></script>
<script language="JavaScript" src="../../common/ajax.js"></script>
<!--#include file="../../common/fsal_3.asp"-->

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_preloadImages() { //v3.0
    if (<%=Application("FSAL")%> == 1){
        Ajax_FSALActualizar3();
    }
    var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>

</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=SPA", "_blank")
window.close()

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=550,height=600,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>


<body bgcolor="#FFFFFF" leftmargin="5" topmargin="5" onLoad="MM_preloadImages('images/fichero_r.gif')">
<form name="frmAlta" id="frmAlta" method="post">
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><a href="https://compras.fagorederlan.es" target="_blank"><img src="images/logo.jpg" border="0" alt=""/></a></td>
    </tr>
  <tr>
    <td><div align="center"><font face="Verdana" size="1">
        </font>
      </div>      
      <hr width="100%" size="1" color="#4f86ae">
      <p align="center"><font size="1" face="Verdana"><b><font color="#8AB3C7">Para continuar con el proceso de alta es imprescindible la aceptaci&oacute;n de las siguientes condiciones :</font> </b> </font></p></td>
    </tr>
</table>

<table border="0" width="100%">
  <tr>
    <td width="100%" valign="top"><font face="Verdana" size="1">
        <table width="100%" border="0">
      <tr>
            <td width="40%" height="200" valign="top">&nbsp; 
            </td>
        <td width="60%"><font face="arial" size="1">
    <textarea readonly rows="10" name="S1" cols="63" style="font-family: arial; font-size: 8pt; text-align:justify; display:block; height:200px; line-height: 150%; list-style-type: lower-alpha">
CONDICIONES DE USO DE LA PLATAFORMA DE COMPRAS

Nota preliminar: La palabra “proveedor” también se usa a continuación para compañías que no son actualmente proveedores del Grupo Fagor Ederlan pero que desean establecer una relación de trabajo con las compañías.

Una condición a cumplir por las compañías que son proveedores activos del Grupo Fagor Ederlan, o que desean registrarse para tomar parte en futuras peticiones de oferta a través del portal de compras, es la introducción en él, el portal, de la descripción general de la compañía.
No existe derecho legal por parte del proveedor a participar en las peticiones de oferta.
El Grupo Fagor Ederlan se reserva el derecho de seleccionar, deseleccionar o excluir proveedores de la plataforma de acuerdo a criterios internos.
Se establecerá un usuario principal por proveedor registrado en el portal de compras.
El usuario principal actúa en nombre del proveedor, que está obligada a mantener los datos actualizados de manera continua.
Este usuario es responsable, como coordinador de la compañía, del alta y baja de futuros usuarios.
El mantenimiento de los datos de los usuarios, el alta y baja de usuarios, así como el mantenimiento de los datos de la compañía (direcciones, materiales, etc…) será realizado por el usuario principal de cada proveedor.
El proveedor será informado de los datos de acceso para el usuario principal de la compañía.
Estos datos serán tratados de forma confidencial, al igual que los de futuros usuarios, los datos de acceso de los usuarios son personales e intransferibles y el acceso a la plataforma debe restringirse a las personas involucradas en la relación comercial.
Las contraseñas de acceso elegidas deben ser de una complejidad adecuada y cambiadas de forma regular.
Toda la información y documentos intercambiados en el contexto de nuestra relación comercial deben ser tratados con estricta confidencialidad.
Dibujos, descripciones, especificaciones y similares pueden ser únicamente guardadas y duplicadas en el contexto de requerimientos operacionales.
En el caso de que la información se comparta con una tercera parte por los proveedores (p.e. subcontratación) se debe garantizar la confidencialidad de la misma.
No se garantiza la disponibilidad permanente y libre de fallos de los sistemas electrónicos. Fagor Ederlan se reserva el derecho de reducir o cesar la operatividad de la plataforma.
El proveedor es responsable de su acceso a la plataforma (“Acceso a Internet”) y no se le cargará con coste alguno por este acceso.
Todas las partes involucradas se comprometen a mantener las medidas de seguridad adecuadas de acuerdo con los estándares tecnológicos actuales.
El proveedor se compromete a no adjuntar en la plataforma cualquier documento, archivo o dato que pueda poner en peligro o impedir su funcionamiento.
De forma particular, debe tomar medidas para no expandir cualquier clase de virus.
El proveedor se compromete a utilizar, y mantener actualizado, un antivirus de prestigio reconocido.
Las ofertas transmitidas en formato electrónico son totalmente válidas, y las realizadas vía fax o teléfono son legalmente vinculantes.
Únicamente los términos contractuales, así como las ofertas, introducidas en la plataforma serán tenidos en cuenta. Cualquier comentario adicional no será vinculante a no ser que sea confirmado por escrito por Fagor Ederlan.
La información que usted nos remite a través de este área de compras pasa a formar parte de la base de datos de proveedores del Grupo Fagor Ederlan.
Tiene derecho a consultar y modificar sus datos o darse de baja de dicha base de datos. El propietario y responsable de la base de datos es:
Fagor Ederlan S. Coop.
Paseo Torrebaso nº7 
2080 Eskoriatza 
CIF: F-20025292 
El Grupo Fagor Ederlan se compromete a utilizar sus datos exclusivamente para solicitar ofertas y remitir comunicados relativos a la función de compras del grupo, y a no facilitar sus datos a terceros sin su previo consentimiento. Si desea darse de baja y NO recibir nuestras solicitudes de oferta ni nuestra información, envíe un correo electrónico a compras@fagorederlan.es indicando en el asunto: "Dar de baja". 
Al enviar su solicitud de registro, la compañía proveedora reconoce las condiciones arriba ennumeradas.
</textarea>
      </font>
        </td>
      </tr>
    </table>
      </font>
           
      <table width="657" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="12" rowspan="2" bgcolor="#999999"></td>
          <td width="200" height="102" bgcolor="#999999" class="textosbl">Para completar de forma adecuada el registro de su compa&ntilde;&iacute;a, deber&aacute; completar una ficha acerca del antivirus que utiliza su empresa. Desc&aacute;rguese el fichero aqu&iacute;&nbsp; y devu&eacute;lvalo cumplimentado a nuestra direcci&oacute;n de correo electr&oacute;nico de atenci&oacute;n a proveedores: </span></SPAN></FONT><FONT face=Verdana color=#FFFFFF size=1><SPAN 
style="FONT-FAMILY: Verdana"><A 
href="mailto:compras@fagorederlan.es">compras@fagorederlan.es</A><o:p></o:p></SPAN></FONT> </td>
          <td width="15" rowspan="2" bgcolor="#999999" class="textos">&nbsp;</td>
          <td width="24" rowspan="2">&nbsp;</td>
          <td width="365" rowspan="2"><span class="textos"><b>&iquest;Acepta las condiciones de adhesi&oacute;n al portal de compras del Grupo Fagor Ederlan?</b></span><font face="Verdana" size="1"><b><br>
                <br>
          </b> <span class="textos">Si pulsa &quot;No Acepto&quot;, su empresa no podr&aacute; ser registrada como proveedor autorizado del Grupo Fagor Ederlan.<br>
          Para continuar el proceso de alta debe pulsar &quot;Acepto&quot;. <br>
          Si quiere imprimir las condiciones puede hacerlo haciendo pulsando el bot&oacute;n &quot;Imprimir&quot;. </span></font></td>
        </tr>
        <tr>
          <td height="5" align="center" bgcolor="#999999"></td>
        </tr>
        <tr>
          <td bgcolor="#999999">&nbsp;</td>
          <td align="center" bgcolor="#999999"><a href="spa/docs/antivirus.doc" target="_blank" onMouseOver="MM_swapImage('Image1','','images/fichero_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="images/fichero.gif" name="Image1" height="14" border="0" id="Image1"></a></td>
          <td bgcolor="#999999">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="4">&nbsp;</td>
          <td><table border="0" width="77%">
            <tr>
              <td width="36%"><p align="center">
                  <input type="submit" onclick="return alta()" value="Acepto" id=submit1 name=submit1>
              </p></td>
              <td width="35%"><input type="button" onclick="no_alta()" value="No acepto" id=button1 name=button1>
              </td>
              <td width="29%" valign="middle"><input type="button" onclick="imprimir()" value="Imprimir" id=button2 name=button2>
              </td>
            </tr>
          </table></td>
        </tr>
      </table>
          </td>
  </tr>
</table>

	<input type="hidden" name="ACEPTADO">
	
</form>

</body>
<!--#include file="../../common/fsal_2.asp"-->
</html>
