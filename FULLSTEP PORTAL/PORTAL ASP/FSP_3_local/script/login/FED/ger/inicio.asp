﻿<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->

<script SRC="../../../common/menu.asp"></script>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<link href="../estilos2.css" rel="stylesheet" type="text/css">

<style>
#divNoticias {position:relative;top:1px;width:100%;height:95%;overflow-y:scroll;color:"#000000";padding-right:10px;}
</style>
<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
    function Init() {
        document.getElementById('tablemenu').style.display = 'block';
        p = window.top.document.getElementById("frSet")
        vRows = p.rows
        vArrRows = vRows.split(",")
        vRows = vArrRows[0] + ",*,0,0"
        p.rows = vRows
    }
</script>
</head>

<body topmargin="0" leftmargin="0" scroll="yes" onLoad="Init()">
<script>
dibujaMenu(1)
</script>
<%
''' <summary>
''' Pantalla inicial tras hacer un login valido
''' </summary>
''' <remarks>Llamada desde: login\FEDERLAN\inicio.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,false,false,0)


DIM FICHERO_GER 
FICHERO_GER= Application("PATHFICHERONOTICIAS") & ".GER"
sNoticias = oRaiz.leerFicheroTexto(FICHERO_GER)

set oRaiz = nothing

%>

<!-------------------------- BEGIN COPYING THE HTML HERE ---------------------------->

<table width="100%" height="438" border="0" bordercolor="0" vspace="0" hspace="0" cellspacing="0">
  <tr> 
    <td width="1%" rowspan="10"></td>
    <td colspan="10" width="50%"><font face="arial" size="5" COLOR="#b1d2e2"><b>&gt;&nbsp;Willkommen auf dem Lieferanten-Portal</b></font></td>
    <td width="20%"><b><font face="arial" size="5" color="#B1D2E2">&gt;&nbsp;Neuigkeiten</font></b></td>
  </tr>
  <tr> 
    <td colspan="10" width="50%"><span class="textos">
    <font  color="#05143F">&nbsp;&nbsp;Hier finden Sie die für Sie bestimmten Anfragen der Fagor Ederlan </font></span><span style="font-size: 11px; font-style: normal; font-weight: normal; text-decoration: none; font-family: Arial, Helvetica, sans-serif;"><font  color="#05143F">Group</font></span><span class="textos"><font  color="#05143F">.<BR>
      <BR>
      &nbsp;&nbsp;Sie erhalten durch anklicken der Links im oberen Abschnitt Zugang zu den verschiedenen Bereichen.</font> </span></td>
    <td width="20%" rowspan="9"><div name="divNoticias" id="divNoticias"><%=sNoticias%></div></td>
  </tr>
  <tr> 
  <td colspan="1" width="16"> 
        <img src="../images/bolita.gif" width="16" height="17" align="center">
  </td>
    <td colspan="9"> 
        <span class="textos">
        <font  color="#05143F"><b>Ihre Details / Ihre Firma :</b>
        Verwaltung von Informationen bezüglich Ihrer Firma. </font></span>
    </td>
  </tr>
  <tr> 
      <td colspan="1" width="16"> 
        <img src="../images/bolita.gif" width="16" height="17" align="center">
  </td>
   <td height="22" colspan="9"> 
        <span class="textos">
        <font  color="#05143F"><b>Anfragen :</b> Ihr Zugang zu den für Sie bestimmten Anfrageprozessen. </font></span>
    </td>
  </tr>
  <tr> 
  <td width="16" colspan="1" valign="middle"> 
        <img src="../images/bolita.gif" width="16" height="17" align="center">
  </td>
    <td colspan="9"> 
        <span class="textos">
        <font  color="#05143F"><b>Bestellungen:</b> Sie können Ihre Bestellungen aus dem elektronischen Katalog für </font></span><span style="font-size: 11px; font-style: normal; font-weight: normal; text-decoration: none; font-family: Arial, Helvetica, sans-serif;"><font  color="#05143F">Fagor Ederlan Group</font></span><span class="textos"><font  color="#05143F"> nachverfolgen.</font></span>
    </td>
  </tr>
  <tr>
    <td colspan="1" valign="middle"><img src="../images/bolita.gif" width="16" height="17" align="center"></td>
    <td colspan="9"><span class="textos"> <font  color="#05143F"><b>Qualität:</b>  Sie können ihre Qualitätszertifikate hinzufügen, ihre Lieferabweichungen nachverfolgen und ihre Bewertungen als Lieferant einsehen.</font></span></td>
  </tr>
      <td colspan="10" align="left" bordercolor="0" width="30%"><hr color="#B1D2E2" size="2"></td>
  <tr> 
    <td colspan="10" bordercolor="0" width="50%">
    <span class="textos">
    <font  color="#05143F">Falls dies Ihr erster Zugang zu dieser Internetseite ist, können Sie sich eine Anleitung zur Bedienung durch anklicken der entsprechenden Schaltfläche herunterladen. Die Handbücher sind in Englisch.</font></span> 
   </td>
  </tr>
  <tr> 
    <td colspan="10" bordercolor="0" width="50%"><hr color="#B1D2E2" size="2"></td>
  </tr>
  <tr>
    <td width="2%"><a href="docs/MAN_206_master%20data_3_0.pdf" target=_blank><img src="../images/pdf.gif" width="26" height="26" border="0" alt="Manual de Mantenimiento de datos"></a></td> 
    <td width="8%" align="left"><span class="textos"><font color="#05143F"><a href="docs/MAN_206_master%20data_3_0.pdf" target="_blank">Supplier details maintenance</a></font></span></td>
    <td width="2%"><a href="docs/MAN_205_how%20to%20offer_3_0.pdf" target=_blank><img src="../images/pdf.gif" width="26" height="26" border="0" alt="Manual sobre c&oacute;mo ofertar"></a></td>
    <td width="8%" align="left"><span class="textos"><font color="#05143F"><a href="docs/MAN_205_how%20to%20offer_3_0.pdf" target="_blank">How&nbsp;to&nbsp; offer&nbsp;</a></font></span></td>
    <td width="2%"><a href="docs/01%20overview.pdf" target=_blank><img src="../images/pdf.gif" width="26" height="26" border="0" alt="Manual sobre visi&oacute;n general del proveedor"></a></td>
    <td width="8%" align="left"><span class="textos"><font color="#05143F" ><a href="docs/01%20overview.pdf" target="_blank">General overview</a></font></span></td>
    <td width="2%"><a href="docs/MAN_203_technical%20requirements_3_0.pdf" target=_blank><img src="../images/pdf.gif" width="26" height="26" border="0" alt="Manual de Requisitos t&eacute;cnicos"></a></td>
    <td width="8%" align="left"><span class="textos"><font color="#05143F" ><a href="docs/MAN_203_technical%20requirements_3_0.pdf" target="_blank">Technical requirements</a></font></span></td>
    <td width="2%" align="left"><a href="docs/MAN_208_Sending%20a%20certificate.pdf" target=_blank><img src="../images/pdf.gif" width="26" height="26" border="0" alt="Manual de Requisitos t&eacute;cnicos"></a></td>
    <td width="8%" align="left"><div align="left"><span class="textos"><font color="#05143F" ><a href="docs/MAN_208_Sending%20a%20certificate.pdf" target="_blank">How to attach a certificate</a></font></span></div></td>
  </tr>
  <tr> 
    <td width="1%">&nbsp;</td>
    <td width="98%" colspan="11" align="center" valign="middle" bordercolor="0" bgcolor="#B1D2E2" class="textos"> 
        <font  color="#000000">Bei Fragen und Problemen senden Sie bitte ein E-Mail an : <a href="mailto:%20compras@fagorederlan.es">compras@fagorederlan.es</a></font></td>
  </tr>
</table>
</body></html>