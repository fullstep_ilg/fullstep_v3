﻿<%@ Language=VBScript %>
<!--#include file="../login.asp"-->
<%						
''' <summary>
''' Pantalla inicial de la personalización
''' </summary>
''' <remarks>Llamada desde: default.asp (de la instalacion) ; Tiempo máximo:0 </remarks>	
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if

If Idioma="" then
	Idioma="ENG"
end if


public sub formLogin
%>
<style>
INPUT {font-family:"Arial";font-size:10px;}
</style>
</script>
<%
if Idioma = "SPA" then%>
<form name="frmLogin" id="frmLogin" method="post" action="default.asp">
    <table width="173" border="0" cellspacing="0" cellpadding="0">
      <tr>
      <input type="hidden" id="Idioma" name="Idioma" value="SPA">
        <td width="5">&nbsp;</td>
        <td width="69" class="formulario">Cód.Compañia</td>
        <td width="88"> 
          <input id="txtCia" name="txtCIA" maxlength="20" size="13">
        </td>
        <td width="11">&nbsp;</td>
      </tr>
      <tr>
        <td width="5">&nbsp;</td>
        <td width="69" class="formulario">Código Usuario</td>
        <td width="88"> 
          <input type="text" name="txtUSU" size="13">
        </td>
        <td width="11">&nbsp;</td>
      </tr>
      <tr>
        <td width="5">&nbsp;</td>
        <td width="69" class="formulario">Password</td>
        <td width="88"> 
          <input name="txtPWD" type="password" maxlength="20" size="13" autocomplete="off">
        </td>
        <td width="11">&nbsp;</td>
      </tr>
      <tr>
        <td width="5">&nbsp;</td>
        <td width="69">&nbsp;</td>
        <td width="88">
        <input type="image" name="cmdEntrar" src="images/entrar1f_spa.gif" width="50" height="20" border="0" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cmdEntrar','','images/entrar2f_spa.gif',1)">
        <td width="11">&nbsp;</td>
      </tr>
    <tr> 
      <td colspan=3 height="37"><div align="center"><a class="formulario" href="javascript:void(null)" onclick="recuerdePWD()"><u>¿Olvidó sus claves de acceso?</u></a></div></td>
    </tr>
    </table>
<%elseif Idioma= "ENG" then%>
<form name="frmLogin" id="frmLogin" method="post" action="default.asp">
    <table width="173" border="0" cellspacing="0" cellpadding="0">
      <tr>
      <input type="hidden" id="Idioma" name="Idioma" value="ENG">
        <td width="5">&nbsp;</td>
        <td width="69" class="formulario">Company code</td>
        <td width="88"> 
          <input id="txtCia" name="txtCIA" maxlength="20" size="13">
        </td>
        <td width="11">&nbsp;</td>
      </tr>
      <tr>
        <td width="5">&nbsp;</td>
        <td width="69" class="formulario">User's Code</td>
        <td width="88"> 
          <input type="text" name="txtUSU" size="13">
        </td>
        <td width="11">&nbsp;</td>
      </tr>
      <tr>
        <td width="5">&nbsp;</td>
        <td width="69" class="formulario">Password</td>
        <td width="88"> 
          <input name="txtPWD" type="password" maxlength="20" size="13" autocomplete="off">
        </td>
        <td width="11">&nbsp;</td>
      </tr>
      <tr>
        <td width="5">&nbsp;</td>
        <td width="69">&nbsp;</td>
        <td width="88">
        <input type="image" name="cmdEntrar" src="../images/entrar1f_eng.gif" width="50" height="20" border="0" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cmdEntrar','','../images/entrar2f_eng.gif',1)">
        <td width="11">&nbsp;</td>
      </tr>
        <tr> 
          <td colspan=3 height="37"><div align="center"><a class="formulario" href="javascript:void(null)" onclick="recuerdePWD()"><u>Forgotten your login details?</u></a></div></td>
        </tr>
    </table>
    <%else%>
<form name="frmLogin" id="frmLogin" method="post" action="default.asp">
    <table width="173" border="0" cellspacing="0" cellpadding="0">
      <tr>
      <input type="hidden" id="Idioma" name="Idioma" value="GER">
        <td width="5">&nbsp;</td>
        <td width="69" class="formulario">Firmencode</td>
        <td width="88"> 
          <input id="txtCia" name="txtCIA" maxlength="20" size="13">
        </td>
        <td width="11">&nbsp;</td>
      </tr>
      <tr>
        <td width="5">&nbsp;</td>
        <td width="69" class="formulario">Benutzercode</td>
        <td width="88"> 
          <input type="text" name="txtUSU" size="13">
        </td>
        <td width="11">&nbsp;</td>
      </tr>
      <tr>
        <td width="5">&nbsp;</td>
        <td width="69" class="formulario">Passwort</td>
        <td width="88"> 
          <input name="txtPWD" type="password" maxlength="20" size="13" autocomplete="off">
        </td>
        <td width="11">&nbsp;</td>
      </tr>
      <tr>
        <td width="5">&nbsp;</td>
        <td width="69">&nbsp;</td>
        <td width="88">
        <input type="image" name="cmdEntrar" src="images/entrar1f_ger.gif" width="50" height="20" border="0" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cmdEntrar','','images/entrar2f_ger.gif',1)">
        <td width="11">&nbsp;</td>
      </tr>
        <tr> 
          <td colspan=3 height="37"><div align="center"><a class="formulario" href="javascript:void(null)" onclick="recuerdePWD()"><u>Forgotten your login details?</u></a></div></td>
        </tr>
    </table> 
    <%end if%>
<input type="hidden" id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
		<input type="hidden" id="CiaComp" name="CiaComp" value="<%=CiaComp%>">
	</form>
<%
end sub
%>
