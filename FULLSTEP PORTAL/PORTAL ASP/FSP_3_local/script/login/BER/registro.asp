﻿<%@ Language=VBScript %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/idioma.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="css/reset.css" rel="stylesheet" type="text/css" />        
<link href="css/font-awesome.min.css" rel="stylesheet" />  
        
<!--<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />-->
<link href="css/style.css" rel="stylesheet" />  
<!--[if lte IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <link href="css/estilo_custom_ie9.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!--<link rel="stylesheet" type="text/css" href="css/responsive.css"/>   -->
<script src="js/jquery-1.9.0.js"></script>
<script src="js/jquery-1.9.0.min.js"></script>
<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
<script src="../../common/formatos.js"></script>
<script language="JavaScript" src="../../common/ajax.js"></script>
<!--#include file="../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=SPA", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body onload="init()">

<header id="heading-registro"> 
        
        	<div class="container"> 
                       
             <div class="logotipo">
            		<a href="<%=application("RUTASEGURA")%>" target="_blank"><img src="img/logo.png" alt="Berge" /></a>
                </div>  

                
            
           	  <div id="titulo">PORTAL DE PROVEEDORES</div>      
                     
    		</div>
        
        
          
            
        </header>           
    			
        
        
    	<section id="servicios"> 
            
            <div class="container">
            
           		<div class="text">
                
                	<!--
                    <div class="columna sol">
                        <div class="imagen">
                      <img src="img/shim.gif" width="100" >
                         </div>  
                        
                   	</div>
                    -->
                    <div class="centro sin solicitud">
                      <div>
                        
                        <h2><em>SOLICITAR REGISTRO</em></h2>
        <div class="int">
        	<p>Para continuar con el proceso de alta es imprescindible que lea y acepte las condiciones de acceso al portal de Proveedores.</p>
            <div class="caja_registro">
                <strong>IMPORTANTE LEA ATENTAMENTE</strong><br><br>



Alta de proveedores - Aviso Legal<br><br>

En cumplimiento de la Ley Orgánica 15/1999, de 13 de diciembre, de protección de datos de carácter personal le informamos que los datos que nos proporcione pasarán a formar parte de un fichero titularidad de BERGÉ. con la exclusiva finalidad de gestionar las relaciones establecidas con usted. 
BERGÉ se reserva el derecho de comprobar todos los datos que usted nos proporcione, así como verificar el cumplimiento de las obligaciones que le marque la distinta legislación aplicable en las distintas fuentes a disposición de BERGÉ, así como solicitarle información que acredite las manifestaciones o declaraciones realizadas. 
Podrá ejercitar los derechos de acceso, rectificación, cancelación y oposición que legalmente se le reconocen mediante escrito a Calle Alcalá, nº 65. 28014 Madrid, o al correo electrónico soportesuministros@bergelogistics.com indicando en el asunto “PROTECCIÓN DE DATOS”, y en ambos casos adjuntando copia del DNI o documento identificativo equivalente.<br><br>


ACCESO AL PORTAL
El alta como proveedor en el Portal de compras de BERGÉ está condicionado a la previa lectura y aceptación de las siguientes cláusulas. Sin expresar su conformidad con las mismas no podrá registrarse. Siempre que se acceda y utilice el Portal, se entenderá que está de acuerdo, de forma expresa, plena y sin reservas, con la totalidad del contenido de este Aviso Legal. Asimismo, al ser Ud el Usuario Principal de su empresa, queda obligado al aceptar la conformidad de este Aviso Legal a velar por su cumplimiento por parte de todos los usuarios que se den de alta en su empresa, liberando a BERGÉ de toda responsabilidad por los posibles perjuicios que dichos usuarios pudieran causar a su empresa o cualquier otra debido a sus actuaciones en el Portal.<br><br>


Cláusulas <br><br>
Obligaciones del PROVEEDOR<br><br>
Son obligaciones del proveedor las siguientes:<br><br>
Proporcionar cuantos datos sean necesarios para el adecuado funcionamiento del sistema, así como mantenerlos actualizados, comunicando a la mayor brevedad posible todo cambio que se produzca en los mismos.<br><br>
El PROVEEDOR acepta que las transacciones realizadas a través del portal de proveedores sean consideradas con el mismo rango y validez que los pedidos enviadas por cualquier otro medio tradicional (carta, fax, mail).<br><br>
Garantizar la autenticidad de los datos facilitados a consecuencia de la cumplimentación de los formularios necesarios para la suscripción de los Servicios y alta en el portal de proveedores.<br><br>
Asimismo, el PROVEEDOR actualizará la información, general de la compañía y de interlocutores/usuarios, facilitada para que refleje en cada momento su situación real. Por todo ello, el PROVEEDOR será el único responsable de los daños y perjuicios ocasionados a BERGÉ como consecuencia de declaraciones inexactas o falsas.
Guardar absoluta confidencialidad en relación con toda la información que se genere en las relaciones entre el PROVEEDOR y BERGÉ.<br><br>
Abstenerse de cualquier manipulación en la utilización informática del portal de proveedores, así como el uso del mismo con fines distintos de aquellos para los que fue previsto.<br><br>
Asimismo, el PROVEEDOR se abstendrá de acceder a zonas no autorizadas del portal de proveedores.<br><br>
Cumplir fielmente sus compromisos en la relación establecida entre BERGÉ y el proveedor a través del portal de proveedores:<br><br>
Aceptar / rechazar los pedidos de los usuarios y servir los bienes y servicios de acuerdo a las condiciones establecidas a través del portal de proveedores.<br><br>
Resolver las posibles incidencias declaradas en la recepción de los pedidos por los usuarios.<br><br>
El PROVEEDOR se obliga a hacer un uso correcto del portal de proveedores y de los Servicios conforme a la ley, las presentes Condiciones de Uso y demás reglamentos de uso e instrucciones puestos en su conocimiento, así como con la moral y las buenas costumbres generalmente aceptadas y el orden público.<br><br>
Por ello se abstendrá de utilizar el portal de proveedores o cualquiera de los Servicios con fines ilícitos, prohibidos en las presentes condiciones de uso, lesivos de los derechos e intereses de terceros. En particular, y a título meramente indicativo, el PROVEEDOR se compromete a no transmitir, difundir o poner a disposición de terceros informaciones, datos, contenidos, gráficos, archivos de sonido y/o imagen, fotografías, grabaciones, software y, en general, cualquier clase de material que:<br><br>
Sea contrario a los derechos fundamentales y libertades públicas reconocidas en la Constitución, Tratados internacionales y leyes aplicables; <br>
Induzca, incite o promueva actuaciones delictivas, contrarias a la ley, a la moral y buenas costumbres generalmente aceptadas o al orden público; <br>
Sea falso, inexacto o pueda inducir a error, o constituya publicidad ilícita, engañosa o desleal;<br>
Esté protegido por derechos de propiedad intelectual o industrial pertenecientes a terceros, sin autorización de los mismos;<br>
Vulnere derechos al honor, a la intimidad o a la propia imagen o las normas sobre secreto de las comunicaciones;<br>
Constituya competencia desleal, o perjudique la imagen empresarial de BERGÉ o terceros;<br>
Esté afectado por virus o elementos similares que puedan dañar o impedir el funcionamiento adecuado del portal de proveedores, los equipos informáticos o sus archivos y documentos.<br><br>

Derechos del PROVEEDOR<br><br>

Son derechos del PROVEEDOR los siguientes:<br><br>
Mantener una presencia constante en la base de datos del portal de proveedores de BERGÉ, en su calidad de proveedor dado de alta. <br><br>
Acceder y gestionar los pedidos en virtud de las condiciones establecidas a través del portal de proveedores. <br><br>
El uso o acceso a el portal de proveedores y/o a los Servicios no atribuyen al PROVEEDOR ningún derecho sobre las marcas, nombres comerciales o signos distintivos que aparezcan en el mismo, siendo estos propiedad de BERGÉ o de terceros. En consecuencia, los Contenidos son propiedad intelectual de BERGÉ o de terceros, sin que puedan entenderse cedidos al PROVEEDOR.<br><br>

Obligaciones de BERGÉ<br><br>
Son obligaciones de BERGÉ las siguientes:<br><br>
Mantener la información que estime oportuna actualizada, no siendo responsable de los errores que se produzcan en la misma.<br><br>
Gestionar a través del portal de proveedores los pedidos de los bienes y servicios acordados por ambas partes.<br><br>
Guardar absoluta confidencialidad de toda la información relativa al proveedor: bien la proporcionada por éste, bien la generada en las relaciones entre el PROVEEDOR y BERGÉ.<br><br>
Facilitar al proveedor, en cualquier momento, la situación de sus datos en la base de datos, y no facilitarla a terceros si no es para permitir al proveedor la gestión de sus órdenes de entrega y la toma en consideración de sus productos como alternativa de suministro. Asimismo, facilitar al proveedor, en cualquier momento, la situación de sus datos en la base de datos, para que éste, previa notificación por escrito, los modifique o elimine. <br><br>

Derechos de BERGÉ<br><br>
El Usuario presta su consentimiento para que BERGÉ ceda sus datos a las empresas a ella asociadas o del grupo al que pertenece, así como a otras con las que concluya acuerdos con la única finalidad de la mejor prestación del servicio, respetando, en todo caso, la legislación española sobre protección de los datos de carácter personal. <br><br>
Optar, en caso de incumplimiento de sus obligaciones por el PROVEEDOR, por exigir el debido cumplimiento de las mismas, o por la resolución del contrato, en los términos de la cláusula 8 de este Aviso Legal.
<br><br>
Limitación de responsabilidad <br><br>

BERGÉ no asume ninguna responsabilidad derivada de la falta de disponibilidad del sistema, fallos de la red o técnicos que provoquen un corte o una interrupción en el portal de proveedores. Igualmente BERGÉ no se hace responsable de los daños y perjuicios que puedan ocasionar la propagación de virus informáticos u otros elementos en el sistema.<br><br>
BERGÉ no asume ninguna responsabilidad sobre cualquier actuación que sea contraria a las leyes, usos y costumbres, o bien que vulnere las obligaciones establecidas en las presentes condiciones de uso, por parte de ninguna persona de BERGÉ, del PROVEEDOR o cualquier usuario de la red. <br><br>

No obstante, cuando BERGÉ tenga conocimiento de cualquier conducta a la que se refiere el párrafo anterior, adoptará las medidas necesarias para resolver con la máxima urgencia y diligencia los conflictos planteados. <br><br>
BERGÉ se exime de cualquier responsabilidad derivada de la intromisión de terceros no autorizados en el conocimiento de las condiciones y circunstancias de uso que los PROVEEDORES hagan del portal de proveedores y de los Servicios.<br><br>

Resolución del acuerdo <br><br>
En caso de incumplimiento de cualquiera de las obligaciones contenidas en el presente Aviso legal, el PROVEEDOR o BERGÉ habrán de notificar dicho incumplimiento a la parte que lo hubiera cometido, otorgándole un plazo de quince días hábiles, contando desde la notificación, para que lo subsane. <br><br>
Transcurrido dicho plazo sin que se haya producido la subsanación del incumplimiento, la otra parte podrá optar por el cumplimiento o la resolución del presente acuerdo, con la indemnización de daños y perjuicios en ambos casos, de conformidad con lo establecido en el artículo 1.124 del Código Civil. En caso de optar por la resolución, tanto el PROVEEDOR como BERGÉ aceptan que la simple notificación será suficiente, para que la misma tenga plenos efectos.<br><br>
Duración <br><br>
El presente acuerdo tendrá una duración anual, prorrogándose la misma tácitamente por periodos anuales, siempre y cuando no se produzca denuncia por escrito de cualquiera de las partes, con al menos 1 mes de antelación.<br><br>
El procedimiento de baja será enviando un mail con Asunto “Baja del portal de proveedores” a la dirección  soportesuministros@bergelogistics.com.<br><br>

Notificaciones <br><br>
Las notificaciones entre las partes para la gestión de los pedidos (aceptación, seguimiento, recepción y resolución de incidencias) se realizarán a través de la zona privada del portal de proveedores. <br><br>
Los cambios de los datos generales o de los interlocutores / usuarios del portal de proveedores serán gestionados por el usuario principal del proveedor a través de la sección habilitada a tal efecto en la zona privada del portal de proveedores.<br><br>
Las relaciones entre BERGÉ y el PROVEEDOR derivadas del uso del portal de proveedores y de los servicios prestados a través del mismo, se someterán a la legislación y jurisdicción españolas. 

<br><br>
 
		  </div>

            <p class="imprimir"><a href="registro_texto.html" class="" target="blank">Imprimir</a> </p>
<div class="caja_acepto">
<form name="frmAlta" id="frmAlta" method="post" style="max-width:740px;">
  <span id="sprycheckbox1">

<span class="checkboxRequiredMsg">Seleccione para continuar.</span>
</span>
<br>
<br>

  <input type="checkbox" name="opcion1" id="opcion1" required>

He leído y acepto el contrato de adhesión al portal de proveedores de BERGÉ.</label><br />


<br />
<button type="button" class="bt_registro" id="submit1" name="submit1">Continuar</button>
<br>
</form>


</div>
<div style="clear:both;"></div>
    	</div>    
<p>&nbsp;</p></div>
</div>
<script type="text/javascript">
// <!--
// var sprycheckbox1 = new Spry.Widget.ValidationCheckbox("sprycheckbox1");
// //-->
$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked')) {
        $('#frmAlta').submit();
        alta();
	}
	else
       alert("Debe aceptar las condiciones de uso del portal para poder registrarse");
    
});

</script>
                      	</div>
                    </div>
                    
                 
               
           	  </div>
                    
         	</div>
              
    	</section>
</body>

<!--#include file="../../common/fsal_2.asp"-->

</html>
