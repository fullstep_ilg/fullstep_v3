﻿<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400' rel='stylesheet' type='text/css'>
        
        <!--[if lt IE 9]>
          	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body>
    <div id="cont_gen">
        <div id="izd_gen">
           <a href="http://www.bergeycia.es" title="Berge"><img src="../img/logo.png" alt="Bergé" /></a>
        </div>
        <div id="drc_gen">
        <h1>RECORDATORIO DE CLAVES</h1>
            <div class="int">
                <p>
                    Si ha olvidado su contraseña, introduzca su código de compañía, de usuario y la dirección de e-mail de su cuenta de acceso y pulse "Enviar". Recibirá un e-mail con el enlace a la página donde podrá crear una nueva contraseña.
                </p>
                <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
                  <table class="claves">
                    <tr>
                        <td>Cód. Compañía:</td>
                        <td><input type="text" name="txtCia" id="txtCia" size="30" maxlength="30"/></td>
                    </tr>
                    <tr>
                        <td>Usuario:</td>
                        <td><input type="text" name="txtUsu" id="txtUsu" size="30" maxlength="30"/></td>
                    </tr>
                    <tr>
                        <td>Dirección de email:</td>
                        <td><input type="text" name="txtEmail" id="txtEmail" size="30" maxlength="100"/></td>
                    </tr>
                </table>
                    <br /><input class="bt_registro" name="cmdEnviar" type="submit" value="Enviar" />
                    <input type="hidden" name="idioma" value="spa"/>
                </form>
                            <div style="clear:both;"></div>

                <div class="recordar-claves-2">
                    <span class="rojo_24"style="display:block; padding: 10px 5px;"><strong>Atención a proveedores</strong><br /><br /><!-- <strong><br />
                    Tel. 910 770 156</strong>--> <a href="mailto:soportesuministros@bergelogistics.com">soportesuministros@bergelogistics.com</a></span>
                    <br />
                </div>
            </div>
        </div>
    </div>
</body>

</html>
