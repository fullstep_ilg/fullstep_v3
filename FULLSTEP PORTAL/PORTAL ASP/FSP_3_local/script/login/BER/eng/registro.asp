﻿<%@ Language=VBScript %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/fsal_1.asp"-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/reset.css" rel="stylesheet" type="text/css" />        
<link href="../css/font-awesome.min.css" rel="stylesheet" />  
        
<!--<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />-->
<link href="../css/style.css" rel="stylesheet" />  
<!--[if lte IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <link href="../css/estilo_custom_ie9.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!--<link rel="stylesheet" type="text/css" href="../css/responsive.css"/>   -->
<script src="../js/jquery-1.9.0.js"></script>
<script src="../js/jquery-1.9.0.min.js"></script>
<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
<script src="../../../common/formatos.js"></script>
<script language="JavaScript" src="../../../common/ajax.js"></script>
<!--#include file="../../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=ENG", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body onload="init()">

<header id="heading-registro"> 
        
        	<div class="container"> 
                       
             <div class="logotipo">
            		<a href="<%=application("RUTASEGURA")%>" target="_blank"><img src="../img/logo.png" alt="Berge" /></a>
                </div>  

                
            
           	  <div id="titulo">SUPPLIER PORTAL</div>      
                     
    		</div>
        
        
          
            
        </header>           
    			
        
        
    	<section id="servicios"> 
            
            <div class="container">
            
           		<div class="text">
                
                	<!--
                    <div class="columna sol">
                        <div class="imagen">
                      <img src="img/shim.gif" width="100" >
                         </div>  
                        
                   	</div>
                    -->
                    <div class="centro sin solicitud">
                      <div>
                        
                        <h2><em>SUPPLIER REGISTRATION</em></h2>
        <div class="int">
        	<p>To proceed with the registration proccess, the following contract must be read and accepted.</p>
            <div class="caja_registro">
                <strong>PLEASE, READ CAREFULLY</strong><br><br>



Supplier Registration - Legal notice<br><br>

In compliance with Organic Law 15/1999, dated 13 December, on the protection of personal data, we inform you that the data you provide us with will form part of a file that is the property of BERGÉ INFRAESTRUCTURAS Y SERVICIOS LOGÍSTICOS S.L. (hereafter BERGÉ) for the exclusive purpose of managing the relations we have established with you. 
BERGÉ reserves the right to check the data that you provide, as well as check your compliance with obligations established by the different applicable laws in different sources that are available for BERGÉ, as well as requesting information that proves the statements and affirmations you make. 
You may exercise your right of access, rectification, cancellation and opposition as legally acknowledged by writing to Calle Alcalá, 65, 28014 Madrid (spain), or to the email address soportesuministros@bergelogistics.com indicating "DATA PROTECTION" as the subject, and in both cases attaching a copy of your ID card or equivalent identification document.<br><br>


PORTAL LOGIN
Registration as a supplier in the purchasing portal of BERGÉ is subject to prior lecture and acceptance of the following clauses. Registration is not possible unless agreement has been expressed. Whenever you login and use the Portal, it is understood that you are expressing full, unreserved acceptance of all the content of this Legal Notice. Because you are the Main User of your company, when you accept compliance with this Legal Notice you are under the obligation of ensuring the compliance of all users registered in your company, absolving BERGÉ from all responsibility for any possible damage that these users may cause the company or any other through their activities in the Portal.<br><br>


CLAUSES <br><br>
1. Supplier obligations<br>
The following are the obligations of the Supplier:<br><br>
To provide any details necessary for the correct functioning of the system, and to keep them up to date, communicating any changes to the same as quickly as possible.<br><br>
The supplier accepts that transactions carried out on the supplier portal shall be considered to have the same validity and condition as those using other traditional means (letter, fax, mail).<br><br>
To guarantee the truthfulness of the data provided in compliance with the completion of the forms required for subscribing to the Services and registration in the supplier portal.<br><br>
The supplier shall also update the general information provided about the company and the users/contacts, which must always reflect their real situation. For all these reasons, the supplier shall have sole responsibility for any damage or loss caused to BERGÉ as a result of inaccurate or false statements. <br><br>
Maintain full secrecy in relation with the information generated in the course of the relations between the supplier and BERGÉ.<br><br>
Refrain from any alteration in the use of the supplier portal, or the use of the same for purposes other than those that are intended.<br><br>
The supplier shall also refrain from entering unauthorized areas of the supplier portal.<br><br>
To strictly fulfil all commitments in the relations established between BERGÉ and the supplier through the supplier portal:<br><br>
Accept/reject orders from users and provide the goods and services in accordance with the conditions established in the supplier portal.<br><br>
Resolve any possible incidents in the reception of orders reported by users.<br><br>
The supplier shall use the supplier portal correctly and the Services in accordance with the law, these Conditions of Use and other regulations on its use that he is made aware of, as well as generally acceptable good behavior and moral standards and public order.<br><br>
Especially, and simply as an illustration of this point, the supplier shall not transmit, distribute or make available to third parties any information, data, content, sound or image files, photographs, recordings, software and any other kind of material that:<br><br>
- Is contrary to fundamental rights and civil liberties recognized under the Spanish Constitution or any applicable international treaties or laws;<br>
- Incites or encourages criminal or illicit acts against generally accepted public standards of decency, good conduct and public order; <br>
- Is false, inaccurate or misleading, or which constitutes false advertising;<br>
- Is protected by intellectual or industrial copyright belonging to third parties, without their authorization;<br>
- Infringes rights in relation with reputation, privacy, one's own image or laws on confidentiality in communications;<br>
- Constitutes unfair competition, or damages the professional reputation of BERGÉ or third parties;<br>
- Is affected by viruses or similar elements that may damage or impede the proper functioning of the supplier portal, computer equipment or its files and documents.<br><br>
2. Supplier rights<br>
The following are the rights of the Supplier:<br><br>
To maintain a constant presence in the database of the supplier portal of BERGÉ, as a registered supplier. <br><br>
To login and manage orders in accordance with the conditions established by the supplier portal. <br><br>
Use of and access to the supplier portal and/or the Services do not confer upon the supplier any rights in relation with the brands, trade names or logos that appear in the same, which remain the property of BERGÉ or third parties. The Contents therefore form part of the intellectual copyright belonging to BERGÉ or third parties, and shall not be considered leased to the supplier.<br><br>
3. Obligations of BERGÉ <br>
The following are the obligations of BERGÉ:<br><br>
To maintain the information considered relevant up to date, without liability for errors that may arise in the same.<br><br>
To manage the orders for goods and services agreed by both parties through the supplier portal.<br><br>
To keep all information about the supplier in strict confidence: whether provided by the supplier, or generated through the relations between the supplier and BERGÉ.<br><br>
To enable the supplier to consult his data in the database at any time, and not provide this information to third parties unless to enable the supplier to manage his delivery orders and to take into consideration his products as an alternative supply. Also, to provide the supplier with the status of his data in the database at any time so that he may modify or delete it after written notice.<br><br>
4. Rights of BERGÉ<br>
The user consents to BERGÉ disclosing his details to associated companies or those belonging to its group, or others with which it enters into agreements with the sole purpose of improving its services, complying always with Spanish laws on the protection of private data. <br><br>
Should a supplier be in breach of its obligations, BERGÉ shall have the choice to demand compliance of the same, or to terminate the contract, under the terms of clause 8 of this Legal Notice. <br><br>
5. Limitation of liability<br>
BERGÉ shall accept no liability derived from the non-availability of the system, network outages or technical issues that result in an interruption or cut-off of the supplier portal. Neither shall BERGÉ accept liability for damages or loss caused by the spread of computer viruses or other similar elements in the system.
BERGÉ shall not accept any liability for any action contrary to law, usages or customs, or which is in breach of the obligations established in these conditions of use taken by persons employed by BERGÉ, the supplier or any other user of the network. <br><br>
However, should BERGÉ be made aware of any conduct referred to in the previous paragraph, it shall adopt the measures considered necessary to resolve the issue with all due speed and diligence. <br><br>
BERGÉ shall not accept any liability arising from the interference of unauthorized third parties in the knowledge of the conditions of use and circumstances in which suppliers use the supplier portal and its Services.<br><br>
6. Termination of the agreement<br>
In the event of a breach of any of the obligations contained in this Legal Notice, the supplier or BERGÉ shall immediately notify the party responsible for this breach, granting a period of fifteen working days, dating from the notification, to rectify the same. <br><br>
Should this period expire without rectification of the breach, the other party may choose to terminate this agreement or not, with compensation for damages and loss in both cases, in accordance with the provisions of Article 1,124 of the Spanish Civil Code. Should termination be applied, both the supplier and BERGÉ will accept the simple announcement as sufficient for this action to have full legal effect.<br><br>
7. Duration<br>
The present agreement shall be of one year's duration, with tacit extensions of one year, on condition that no written refusal is submitted by either party, with at least one month's notice.<br><br>
The deregistration procedure involves sending a mail with the Subject "Deregistration from the supplier portal” to the address soportesuministros@bergelogistics.com. <br><br>
8. Notifications<br>
Any notifications between the parties for managing orders (acceptance, monitoring, reception and resolution of issues) shall be made in the private area of the supplier portal. <br><br>
Any changes in the general details of the contacts/users of the supplier portal shall be channeled through the supplier's main user in the section prepared for this purpose in the private area of the supplier portal.<br><br>
All relations between BERGÉ and the supplier arising from the use of the supplier portal and the services provided through the same shall be subject to Spanish laws and jurisdiction.

<br><br>
 
		  </div>

            <p class="imprimir"><a href="registro_texto.html" class="" target="blank">Print</a> </p>
<div class="caja_acepto">
<form name="frmAlta" id="frmAlta" method="post" style="max-width:740px;">
  <span id="sprycheckbox1">

<span class="checkboxRequiredMsg">Select to continue.</span>
</span>
<br>
<br>

  <input type="checkbox" name="opcion1" id="opcion1" required>

I have read and accept the sign up contract to the BERGÉ Supplier Portal.</label><br />


<br />
<button type="button" class="bt_registro" id="submit1" name="submit1">Next</button>
<br>
</form>


</div>
<div style="clear:both;"></div>
    	</div>    
<p>&nbsp;</p></div>
</div>
<script type="text/javascript">
// <!--
// var sprycheckbox1 = new Spry.Widget.ValidationCheckbox("sprycheckbox1");
// //-->
$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked')) {
        $('#frmAlta').submit();
        alta();
	}
	else
       alert("Debe aceptar las condiciones de uso del portal para poder registrarse");
    
});

</script>
                      	</div>
                    </div>
                    
                 
               
           	  </div>
                    
         	</div>
              
    	</section>
</body>

<!--#include file="../../../common/fsal_2.asp"-->

</html>
