﻿<%@ Language=VBScript %>
<!--#include file="../login.asp"-->
<%				
''' <summary>
''' Pantalla inicial de la personalización
''' </summary>
''' <remarks>Llamada desde: default.asp (de la instalacion) ; Tiempo máximo:0 </remarks>
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if

if Not IdOrden = "" then
    if Not IsNumeric(IdOrden) then
        IdOrden=""
    end if
end if

if Not CiaComp = "" then
    if Not IsNumeric(CiaComp) then
        CiaComp=""
    end if
end if

If Idioma="" then
	Idioma="SPA"
end if

public sub formLogin
%>
<style>
INPUT {font-family:"Verdana";font-size:10px;}
.Estilo1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
	color: #FFFFFF;
}
</style>
<style>
INPUT {font-family:"Verdana";font-size:10px;}
</style>
<script language="javascript">
<!--
function ventanaLogin (IDI){

   window.open ("registro.asp?Idioma="+IDI,"","width=650,height=400,resizable=yes")

}
function MM_showHideLayers() { //v6.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<%
if Idioma = "SPA" then
%>
	<body onLoad="MM_preloadImages('images/formulario/solicitudESr.gif')">
	<form name="frmLogin" id="frmLogin" method="post" action="<%if desde="0.asp" or desde="26.asp" then%><%else%>default.asp<%end if%>">

      <table width="184" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td rowspan="10" width="12" background="images/formulario/fgris1.gif"><img src="images/formulario/fgris1.gif" width="12" height="129"></td>
          <td bgcolor="#9C9A9C" class="subtitulo" colspan="4" height="24"> 
            <div align="center"><img src="images/formulario/accesoES.gif" width="172" height="24"></div>
          </td>
          <td rowspan="10" width="1" background="images/formulario/fgris2.gif" class="form"> 
            <div align="right"><img src="images/formulario/fgris2.gif" width="1" height="129"></div>
          </td>
        </tr>
        <tr> 
          <td class="formulario" colspan="4" height="4">&nbsp;</td>
        </tr>
        <tr> 
          <td width="86" class="formulario"> 
            <div align="right" class="formulario">Cód. Compañía</div>
          </td>
          <td width="5" class="form">&nbsp; </td>
          <td width="77" class="form"> 
				<input id="txtCia" name="txtCIA" maxlength="20" size="10">
          </td>
          <td width="5" class="form">&nbsp;</td>
        </tr>
        <tr> 
          <td width="86" class="formulario"> 
            <div align="right">Cód. usuario</div>
          </td>
          <td width="5" class="form">&nbsp; </td>
          <td width="77" class="form"> 
			<input name="txtUSU" maxlength="20" size="10">      
          </td>
          <td width="5" class="form">&nbsp;</td>
        </tr>
        <tr> 
          <td width="86" class="formulario"> 
            <div align="right">Contraseña</div>
          </td>
          <td width="5" class="form">&nbsp; </td>
          <td width="77" class="form"> 
				<input name="txtPWD" type="password" maxlength="20" size="10" autocomplete="off">      
          </td>
          <td width="5" class="form">&nbsp;</td>
        </tr>
        <tr> 
          <td width="86" class="form" height="0">&nbsp;</td>
          <td width="5" class="form" height="0">&nbsp;</td>
          <td width="77" class="form" height="0">&nbsp;</td>
          <td width="5" class="form" height="0">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="4" class="form" height="17" align="center"> 
          <input type="image" name="cmdEntrar" src="images/formulario/fentrar.gif" width="45" height="17" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cmdEntrar','','images/formulario/fentrarR.gif',1)"></td>
          </td>
        </tr>
        <tr> 
      <td colspan="3" height="37"><div align="center"><a class="formulario" href="javascript:void(null)" onclick="recuerdePWD()"><u>¿Olvidó sus claves de acceso?</u></a></div></td>
    </tr>
        <tr> 
          <td colspan="4" class="form" height="23">
                    <a href="javascript:ventanaLogin('SPA')"><img src="images/formulario/solicitudES.gif" alt="solicitar registro" name="Image151" width="172" height="24" border="0" id="Image15"></a></td>
        </tr>
        <tr> 
          <td colspan="4" class="form" height="1"><img src="images/formulario/lineagris.gif" width="172" height="3"></td>
        </tr>
      </table>
      <div id="texto" style="position:absolute; width:180px; height:115px; z-index:3; visibility: hidden; left: 615; top:130; background-color: #666666; layer-background-color: #666666; border: 1px none #000000;">
        <div id="Layer1" style="position:absolute; width:164px; height:86px; z-index:2; left: 12px; background-color: #666666; layer-background-color: #666666; border: 1px none #000000;">
        <p class="Estilo1">Es imprescindible para ser proveedor de FICOSA INTERNATIONAL estar certificado en la norma ISO 9001:2000. Además FICOSA INTERNATIONAL potenciará aquellos proveedores certificados en la norma ISOTS: 16949 versión 2002.</p></div>
        
        <p class="Estilo1">&nbsp;</p>
</div>
<%else%>
	<form name="frmLogin" id="frmLogin" method="post">
      <table width="184" border="0" cellspacing="0" cellpadding="0">
        <tr>
		   <input type="hidden" id="Idioma" name="Idioma" value="ENG">
          <td rowspan="10" width="12" background="../images/formulario/fgris1.gif"><img src="../images/formulario/fgris1.gif" width="12" height="129"></td>
          <td bgcolor="#9C9A9C" class="subtitulo" colspan="4" height="24"> 
            <div align="center"><img src="../images/formulario/accesoen.gif" width="172" height="24"></div>
          </td>
          <td rowspan="10" width="1" background="../images/formulario/fgris2.gif" class="form"> 
            <div align="right"><img src="../images/formulario/fgris2.gif" width="1" height="129"></div>
          </td>
        </tr>
        <tr> 
          <td class="formulario" colspan="4" height="4">&nbsp;</td>
        </tr>
        <tr> 
          <td width="80" class="formulario"> 
            <div align="right" class="formulario">Company Code</div>
          </td>
          <td width="5" class="form">&nbsp; </td>
          <td width="80" class="form"> 
				<input id="txtCia" name="txtCIA" maxlength="20" size="10">
          </td>
          <td width="5" class="form">&nbsp;</td>
        </tr>
        <tr> 
          <td width="78" class="formulario"> 
            <div align="right">User Code</div>
          </td>
          <td width="5" class="form">&nbsp; </td>
          <td width="80" class="form"> 
			<input name="txtUSU" maxlength="20" size="10">      
          </td>
          <td width="5" class="form">&nbsp;</td>
        </tr>
        <tr> 
          <td width="78" class="formulario"> 
            <div align="right">Password</div>
          </td>
          <td width="5" class="form">&nbsp; </td>
          <td width="80" class="form"> 
				<input name="txtPWD" type="password" maxlength="20" size="10" autocomplete="off">      
         </td>
          <td width="5" class="form">&nbsp;</td>
        </tr>
        <tr> 
          <td width="78" class="form" height="0">&nbsp;</td>
          <td width="5" class="form" height="0">&nbsp;</td>
          <td width="80" class="form" height="0">&nbsp;</td>
          <td width="5" class="form" height="0">&nbsp;</td>
        </tr>
        <tr> 
          <td align="center" colspan="4" class="form" height="17"> 
           <input type="image" name="cmdEntrar" src="../images/formulario/FENTER.GIF" WIDTH="45" HEIGHT="17" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cmdEntrar','','../images/formulario/FenterR.gif',1)"></td>
          </td>
        </tr>
        <tr> 
          <td colspan="3" height="37"><div align="center"><a class="formulario" href="javascript:void(null)" onclick="recuerdePWD()"><u>Forgot your login data?</u></a></div></td>
        </tr>
        <tr> 
          <td colspan="4" class="form" height="23">
            <a href="javascript:ventanaLogin('ENG')"><img src="../images/formulario/solicituden.gif" alt="ask for registration" name="Image15" border="0" id="Image15"></a></td>
        </tr>
        <tr> 
          <td colspan="4" class="form" height="1"><img src="../images/formulario/lineagris.gif" width="172" height="3"></td>
        </tr>
      </table>
      <div id="texteng" style="position:absolute; width:180px; height:115px; z-index:3; visibility: hidden; left: 615; top:130; background-color: #666666; layer-background-color: #666666; border: 1px none #000000;">
        <div id="Layer1" style="position:absolute; width:164px; height:86px; z-index:2; left: 12px; background-color: #666666; layer-background-color: #666666; border: 1px none #000000;">
        <p class="Estilo1">To be a FICOSA INTERNATIONAL supplier, it is mandatory to become registered to ISO 9001:2000. In addition, FICOSA INTERNATIONAL will promote suppliers registered to ISOTS: 16949 version 2002.</p></div>
        
        <p class="Estilo1">&nbsp;</p>
</div>
<%end if%>


		<input type="hidden" id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
		<input type="hidden" id="CiaComp" name="CiaComp" value="<%=CiaComp%>">
	</form>
<%
end sub
%>