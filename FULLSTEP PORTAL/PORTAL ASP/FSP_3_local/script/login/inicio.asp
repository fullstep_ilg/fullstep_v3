﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<html>
<script src="../js/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var login = false;
    var url, paginaCustom;
    $(document).ready(function () {
        $('#fraJDELogin').load(function () {
            if (login) {
                $('#fraJDELogin').attr('src', url + paginaCustom);
                login = false;
                top.loaded = true;
            }
        });
    });
</script>
<%if lcase(application("NOMPORTAL")) = "pri" then%>
<frameset rows="90,15,*,0,0"  frameborder="0" border="0" name=frSet id=frSet>
<%elseif lcase(application("NOMPORTAL")) = "huf" then%>
<frameset rows="75,15,*,0,0"  frameborder="0" border="0" name=frSet id=frSet>
<%elseif lcase(application("NOMPORTAL")) = "rsi" then%>
<frameset rows="95,15,*,0,0"  frameborder="0" border="0" name=frSet id=frSet>
<%elseif lcase(application("NOMPORTAL")) = "lib" then%>
<frameset rows="95,15,*,0,0"  frameborder="0" border="0" name=frSet id=frSet>
<%elseif lcase(application("NOMPORTAL")) = "bcv" then%>
<frameset rows="124,15,*,0,0"  frameborder="0" border="0" name=frSet id=frSet>
<%elseif lcase(application("NOMPORTAL")) = "rea" then%>
<frameset rows="115,15,*,0,0"  frameborder="0" border="0" name=frSet id=frSet>
<%elseif lcase(application("NOMPORTAL")) = "cac" then%>
<frameset rows="90,15,*,0,0"  frameborder="0" border="0" name=frSet id=frSet>
<%elseif lcase(application("NOMPORTAL")) = "avn" then%>
<frameset rows="82,15,*,0"  frameborder="0" border="0" name=frSet id=frSet>
<%elseif lcase(application("NOMPORTAL")) = "oca" then%>
<frameset rows="84,15,*,0"  frameborder="0" border="0" name=frSet id=frSet>
<%elseif lcase(application("NOMPORTAL")) = "ser" then%>
<frameset rows="84,15,*,0"  frameborder="0" border="0" name=frSet id=frSet>
<%elseif lcase(application("NOMPORTAL")) = "ete" then%>
<frameset rows="84,15,*,0"  frameborder="0" border="0" name=frSet id=frSet>
<%elseif lcase(application("NOMPORTAL")) = "des" then%>
<frameset rows="59,15,*,0"  frameborder="0" border="0" name=frSet id=frSet>
<%elseif lcase(application("NOMPORTAL")) = "ibc" then%>
<frameset rows="85,15,*,0"  frameborder="0" border="0" name=frSet id=frSet>
<%elseif lcase(application("NOMPORTAL")) = "tgl" then%>
<frameset rows="85,15,*,0"  frameborder="0" border="0" name=frSet id=frSet>
<%elseif lcase(application("NOMPORTAL")) = "hre" then%>
<frameset rows="164,15,*,0"  frameborder="0" border="0" name=frSet id=frSet>
<%elseif lcase(application("NOMPORTAL")) = "mor" then%>
<frameset rows="112,15,*,0"  frameborder="0" border="0" name=frSet id=frSet>
<%elseif lcase(application("NOMPORTAL")) = "ppl" then%>
<frameset rows="77,15,*,0"  frameborder="0" border="0" name=frSet id=frSet>
<%else%>
<frameset rows="70,15,*,0,0"  frameborder="0" border="0" name=frSet id=frSet>
<%end if%>
<%if lcase(application("NOMPORTAL")) = "huf"  then
	Idioma = Request.Cookies("USU_IDIOMA")
	if Idioma = "ENG" then%>
	<frame name="fraTop" scrolling="no" noresize src=<% Response.Write "" &  Application("RUTASEGURA") & "custom/" & Application("NOMPORTAL") & "/eng/top.htm"%> marginwidth="0" marginheight="0" frameborder="0" framespacing="0" height="0">
	<%elseif Idioma = "GER" then%>
	<frame name="fraTop" scrolling="no" noresize src=<% Response.Write "" &  Application("RUTASEGURA") & "custom/" & Application("NOMPORTAL") & "/ger/top.htm"%> marginwidth="0" marginheight="0" frameborder="0" framespacing="0" height="0">
	<%else%>
		<frame name="fraTop" scrolling="no" noresize src=<% Response.Write "" &  Application("RUTASEGURA") & "custom/" & Application("NOMPORTAL") & "/spa/top.htm"%> marginwidth="0" marginheight="0" frameborder="0" framespacing="0" height="0">
	<%end if
  else%>
	<frame id="fraTop" name="fraTop" scrolling="no" noresize src=<% Response.Write "" &  Application("RUTASEGURA") & "custom/" & Application("NOMPORTAL") & "/top.htm"%> marginwidth="0" marginheight="0" frameborder="0" framespacing="0" height="0"> 	
 <%end if%>

<%
''' <summary>
''' Pantalla inicial tras hacer un login valido
''' </summary>
''' <remarks>Llamada desde: login/login.asp; Tiempo máximo: 0,1</remarks>	
	IdOrden=Request("IdOrden") 
	CiaComp=Request("CiaComp") 
	
	if IdOrden="" then
		target=""
	else
		target="pedidos"	
	end if
	Idioma = Request.Cookies("USU_IDIOMA")
    
    if Request.Cookies("USU_CADPASSWORD") ="" then
        cadpasword=false
    else
        cadpasword=cbool(Request.Cookies("USU_CADPASSWORD"))
    end if
	select case target
		case "":
            if cadpasword or request.QueryString("crc").Count>0 then%>
                <frame name="default_main" scrolling="auto" noresize src=<% Response.Write "" & Application("RUTASEGURA") & "script/login/changePassword.asp?t=1&u=" & server.URLEncode(Request.QueryString("u")) & "&c=" & Request.QueryString("c") & "&crc=" & Request.QueryString("crc")%> marginheight="0" marginwidth="0" frameborder="0" framespacing="0" height="100%">
            <%else %>
			    <frame name="default_main" scrolling="auto" noresize src=<% Response.Write "" & Application("RUTASEGURA") & "script/login/" & Application("NOMPORTAL") & "/inicio.asp"%> marginheight="0" marginwidth="0" frameborder="0" framespacing="0" height="100%">
		    <%end if 
        case "pedidos":%>
			<frame name="default_main" scrolling="auto" noresize src=<% Response.Write "" & Application("RUTASEGURA") & "script/pedidos/pedidos11dir.asp?IdOrden=" & IdOrden & "&CiaComp=" & CiaComp%> marginheight="0" marginwidth="0" frameborder="0" framespacing="0" height="100%"> 
	<%end select
    if not cadpasword and request.QueryString("crc").Count=0 then%>     
	    <frame name="" scrolling="auto" src=<% = Application("RUTASEGURA") & "script/common/framespera.asp"%> marginheight="0" marginwidth="0" frameborder="0" framespacing="0" height="100%"> 
        <%if cbool(application("CN")) then%>
            <frame name="fraCN" scrolling="auto" src=<% = Application("RUTASEGURA") & "script/PMPortal/script/_common/cabecera_master.aspx"%> marginheight="0" marginwidth="0" frameborder="0" framespacing="0" height="100%">
        <%end if
    end if%>
    <frame name="fraJDELogin" id="fraJDELogin" src="">
	<noframes>
	<body topmargin="0" leftmargin="0" rightMargin="0" bottomMargin="0">
	<p>This page uses frames, but your browser doesn't support them.</p>
    <div style="position:absolute; top:0px; left:0px; height:10px; width:10px;"></div>
	</body>
	</noframes>
</frameset>


</html>