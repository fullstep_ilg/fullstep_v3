﻿<%@ Language=VBScript %>
<!--#include file="../../login.asp"-->
<%		
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if

if Not IdOrden = "" then
    if Not IsNumeric(IdOrden) then
        IdOrden=""
    end if
end if

if Not CiaComp = "" then
    if Not IsNumeric(CiaComp) then
        CiaComp=""
    end if
end if

If Idioma="" then
	Idioma="ENG"
end if

%>


<html class="no-js" lang="es">
<head>
<title>:: Supplier Portal ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />  
<meta name="viewport" content="width=device-width, initial-scale=1" />  
<!--
<link rel="canonical" href="/" />

<link href="../css/reset.css" rel="stylesheet" type="text/css" />        
<link href="../css/font-awesome.min.css" rel="stylesheet" />  
<link href="../css/style.css" rel="stylesheet" />  
<link rel="stylesheet" type="text/css" href="../css/responsive.css">   

<link rel="stylesheet" href="../css/estilos.css">-->
<link rel="stylesheet" href="../css/login.css">      
        
<!--[if lte IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script language="javascript">


function ventanaLogin (IDI){

   window.open ("registro.asp?Idioma="+IDI,"","width=700,height=540,resizable=yes")

}
function ventanaSecundaria (URL){

   window.open(URL,"ventana1","width=700,height=420,scrollbars=no")

}
function ventanaAyuda (URL){

   window.open(URL,"ayuda","width=700,height=600,scrollbars=yes")

}

function popup (URL) {
 window.open("popup.htm","","width=700,height=400,scrollbars=NO")
 }
function recuerdePWD()
{
   window.open("recuerdo.asp","_blank","width=800,height=480,scrollbars=NO")
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
//-->
</script>
<script>
$(document).ready(function(){
	//---------------------
	
	$("#txtCIA").val("Company Code");
  	$("#txtCIA").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Company Code"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Company Code");
  			$(this).removeClass('focus');
  		}
  	});	
	//---------------------
  	$("#txtUSU").val("User");
  	$("#txtUSU").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="User"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("User");
  			$(this).removeClass('focus');
  		}
  	});
	//---------------------
  	$("#txtPWD").val("Password");
	$("#txtPWD").focus(function(){
/*  	$("#txtPWD").bind('focus',function(){*/
  		$(this).addClass('focus');
  		if($(this).val()=="Password"){
  			$(this).val('');}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Password");
  			$(this).removeClass('focus');
  		}
  	});
});
</script>

</head>

<body>
        <div class="container-login">
            <div class="compound" style="background-image: url('../css/img/bg-Pescanova.JPG')">
                <header class="cf">
					 <div class="nav"><div href="" class="nav_item language">
                                <a href="../default.asp" >ESP</a> | <a href="" class="active">ENG</a>
	  </a></div> </div>
                    <div class="(6/12) col"><div class="logo" style="background-image: url('../css/img/logo.png');"></div></div>
                    <div class="(6/12) col"><h2 class="text text--right">SUPPLIER PORTAL</h2></div>
			
                </header>
                <main>

                    <div class="compound_layer-login">
                        <div class="login login--proveedores">
                            <div class="login_container">
                                <div class="login_header">
                                    LOGIN <div>Not yet registered? <a href="registro.asp?Idioma=ENG">Ask for registration</a></div>
                                </div>
                                <div class="hr"></div>
                                <div class="login_body">
                                    <span>FILL IN THE DETAILS TO ENTER YOUR ACCOUNT</span>
                                    <form action="default.asp" method="post" name="frmLogin"  id="frmLogin">
                                        <div class="input-group">
                                            <label for="txtCIA">COMPANY<span>*</span></label>
                                            <input class="input input--login" type="text" id="txtCIA" name="txtCIA" placeholder="Your Company" required />
                                        </div>
                                        <div class="input-group">
                                            <label for="txtUSU">USER<span>*</span></label>
                                            <input class="input input--login" type="text" id="txtUSU" name="txtUSU" placeholder="Your User" required />
                                        </div>
                                        <div class="input-group">
                                            <div class="grid">
                                                <div class="(5/12) col"> <label for="txtPWD">PASSWORD<span>*</span></label> </div>
                                            </div>
                                            <input class="input input--login" type="password" id="txtPWD" name="txtPWD" placeholder="Su contraseña" required />
                                        </div>
                                        <div class="input-group">
                                            <a class="forgotten text" href="javascript:void(null)" onClick="recuerdePWD()">Have you forgotten your password?</a>
                                            <input class="input input--login" type="submit" value="ENTRAR" style="background-color: #d6001c">
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="compound_layer-text">
                        <div class="shape"></div>
                        <div class="content">
                            <div class="content_text">
                                <p>
                                   <strong> Nueva Pescanova </strong>is making available an online platform for its suppliers that responds to their requirements in one place. A direct communications channel to the tools and services required for agile and efficient management of daily operations.
                                </p>
                                <p>
                                      This portal is a key element in the construction of a relation of trust and mutual benefit: simplicity, control and transparency.
                                </p>
                                <p>
                                    <strong>Welcome to our Supplier Portal.</strong>
                                </p>
                            </div>
                            <div class="content_soporte content_soporte--proveedores">
                                <h6>SUPPLIER SUPPORT</h6>
                                <div>
                                  <!--  <span>Tel.<a href="tel:902996926"></a>+34 902 996 926</span>-->
                                    <span><a href="mailto:portalproveecompras@nuevapescanova.com">portalproveecompras@nuevapescanova.com</a></span>
                                    <div class="hr"></div>
                                </div>
                            </div>
                            <div class="content_footer">
                                <div class="col align align--bottom"><a class="cookies" href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/ayuda.html">Frequently Asked Questions</a><a class="cookies" href="<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=ENG" >Cookies Policy</a><a class="cookies" href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/aviso-legal.html">Legal notice</a></div>
                                <div class="col text text--right"><img src="../css/img/FullStepPro.svg" alt="Powered by Fullstep Pro"></div>
                            </div>
                        </div>
                    </div>

                </main>
            </div>

        </div>

    </body>

</html>

