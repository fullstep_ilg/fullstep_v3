﻿<%@ Language=VBScript %>
<html>

<head>

<!--#include file="../../common/XSS.asp"-->
<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/idioma.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->

<%
''' <summary>
''' Mostrar el detalle del registro
''' </summary>
''' <remarks>Llamada desde: login/login.asp		login/codigo instalaci�n/registro.asp	login/codigo instalaci�n/codigo idioma/registro.asp; Tiempo m�ximo: 0</remarks>
%>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>::Portal de proveedores::</title>

<link href="estilos.css" rel="stylesheet" type="text/css">

<script src="../../common/formatos.js"></script>
<script language="JavaScript" src="../../common/ajax.js"></script>
<!--#include file="../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=SPA", "_blank", "width=730,height=635")
window.close()

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=550,height=600,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>


<body bgcolor="#FFFFFF" topmargin="5" leftmargin="5" onload="init()">
<form name="frmAlta" id="frmAlta" method="post">

  <div align="center">
  <table border="0" width="80%">
      <tr>
        <td><a href="http://seacompetitivo.com/" target="_blank"><img src="images/logo.png" height="130" border="0"></a></td>
      </tr>
      <tr>
        <td width="100%" height="33" align="left">        
	    <p align="left" class="subtextos"> Para continuar con el proceso 
              de alta es imprescindible la aceptación de las siguientes condiciones :</p>        
               
      </td>
      </tr>
      <tr>
        <td align="left"><table border="0" align="left">
          <tr>
            <td valign="bottom"><div align="right"><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"></font></div>          <font color="#666666" size="1" face="Verdana">
              <textarea readonly rows="11" name="S1" cols="63" style="font-family: Verdana; font-size: 8pt; text-align:justify; display:block; height:135px; line-height: 150%; list-style-type: lower-alpha">
IMPORTANTE LEA ATENTAMENTE

Alta de proveedores

Aviso Legal

Acceso al Portal

El alta como proveedor en el Portal de compras de Sea Competitivo está condicionado a la previa lectura y aceptación de las siguientes cláusulas. Sin expresar su conformidad con las mismas no podrá registrarse. Siempre que se acceda y utilice el Portal, se entenderá que está de acuerdo, de forma expresa, plena y sin reservas, con la totalidad del contenido de este Aviso Legal. Asimismo, al ser Ud el Usuario Principal de su empresa, queda obligado al aceptar la conformidad de este Aviso Legal a velar por su cumplimiento por parte de todos los usuarios que se den de alta en su empresa, liberando a Sea Competitivo de toda responsabilidad por los posibles perjuicios que dichos usuarios pudieran causar a su empresa o cualquier otra debido a sus actuaciones en el Portal.


Cláusulas

1. Objeto del Portal de compras de Sea Competitivo 

El portal de compras de Sea Competitivo es el medio a través del cual Sea Competitivo se comunica con sus proveedores para solicitarles ofertas, documentos así como aquella información comercial que estime oportuna. Al mismo tiempo Sea Competitivo puede utilizar el portal para remitirle aquella información que considere de su interés.

Sea Competitivo actúa tanto como comprador directo como de gestor de compras de sus clientes, y actuará en nombre de ellos durante el proceso de compra.

l uso o acceso al Portal y/o a los Servicios no atribuyen al PROVEEDOR ningún derecho sobre las marcas, nombres comerciales o signos distintivos que aparezcan en el mismo, siendo estos propiedad de Sea Competitivo o de terceros. En consecuencia, los Contenidos son propiedad intelectual de Sea Competitivo o de terceros, sin que puedan entendersecedidos al PROVEEDOR.



2. Objeto 

El presente acuerdo tiene por objeto regular las relaciones entre Sea Competitivo y el PROVEEDOR, en todo lo relacionado con el uso del Portal.


3. Obligaciones del PROVEEDOR

Son obligaciones del proveedor las siguientes:

a. Proporcionar cuantos datos sean necesarios para el adecuado funcionamiento del sistema, así como mantenerlos actualizados, comunicando a la mayor brevedad posible todo cambio que se produzca en los mismos.

b. Garantizar la autenticidad de los datos facilitados a consecuencia de la cumplimentación de los formularios necesarios para la suscripción de los Servicios. Asimismo, el PROVEEDOR actualizará la información facilitada para que refleje , en cada momento, su situación real. Por todo ello, el PROVEEDOR será el único responsable de los daños y perjuicios ocasionados a Sea Competitivo como consecuencia de declaraciones inexactas o falsas.


c. Guardar absoluta confidencialidad en relación con toda la información que se genere en las relaciones entre el PROVEEDOR y Sea Competitivo

d. Abstenerse de cualquier manipulación en la utilización informática del Portal, así como el uso del mismo con fines distintos de aquellos para los que fue previsto. Asimismo, el PROVEEDOR se abstendrá de acceder a zonas no autorizadas del Portal.

e. Cumplir fielmente sus compromisos en la información remitida a través del portal. En el caso de que el PROVEEDOR no demuestre la necesaria diligencia comercial, o incumpla las obligaciones contraídas, Sea Competitivo se reserva el derecho de excluir temporal o permanentemente al PROVEEDOR del portal.

f. El PROVEEDOR deberá indicar solamente aquellos grupos de materiales, que se refieran a bienes o servicios que, en el momento de la aceptación del acuerdo, el proveedor comercialice, fabrique o distribuya y que sean del interés comercial de Sea Competitivo.

g. El PROVEEDOR acepta que las ofertas introducidas en el Portal sean consideradas con el mismo rango y validez que una oferta enviada por cualquier otro medio tradicional (carta, fax).

h. El PROVEEDOR se obliga a hacer un uso correcto del Portal y de los Servicios conforme a la ley, el presente Aviso Legal y demás reglamentos de uso e instrucciones puestos en su conocimiento, así como con la moral y las buenas costumbres generalmente aceptadas y el orden público.

Por ello, se abstendrá de utilizar el Portal o cualquiera de los Servicios con fines ilícitos, prohibidos en el presente Aviso Legal, lesivos de los derechos e intereses de terceros. En particular, y a título meramente indicativo, el PROVEEDOR se compromete a no transmitir, difundir o poner a disposición de terceros informaciones, datos, contenidos, gráficos, archivos de sonido y/o imagen, fotografías, grabaciones, software y, en general, cualquier clase de material que:

1. Sea contrario a los derechos fundamentales y libertades públicas reconocidas en la Constitución, Tratados internacionales y leyes aplicables; 

2. Induzca, incite o promueva actuaciones delictivas, contrarias a la ley, a la moral y buenas costumbres generalmente aceptadas o al orden público; 

3. Sea falso, inexacto o pueda inducir a error, o constituya publicidad ilícita, engañosa o desleal;

4. Esté protegido por derechos de propiedad intelectual o industrial pertenecientes a terceros, sin autorización de los mismos;

5. Vulnere derechos al honor, a la intimidad o a la propia imagen;

6. Vulnere las normas sobre secreto de las comunicaciones;

7. Constituya competencia desleal, o perjudique la imagen empresarialde Sea Competitivo o terceros;

8. Esté afectado por virus o elementos similares que puedan dañar o impedir el funcionamiento adecuado del Portal, los equipos informáticos o sus archivos y documentos.


4. Derechos del PROVEEDOR

Son derechos del PROVEEDOR los siguientes:

1. Mantener una presencia constante en la base de datos de Sea Competitivo, en su calidad de proveedor dado de alta. 

2. Recibir peticiones de ofertas en virtud de las normas establecidas. 


5. Obligaciones de Sea Competitivo 

Son obligaciones de Sea Competitivo las siguientes:

a. Mantener la información que estime oportuna actualizada, no siendo responsable de los errores que se produzcan por fuerza mayor o caso fortuito.

b. Guardar absoluta confidencialidad de toda la información relativa al proveedor: bien la proporcionada por éste, bien la generada en las relaciones entre el PROVEEDOR y Sea Competitivo

c. Facilitar al proveedor, en cualquier momento, la situación de sus datos en la base de datos, y no facilitarla a terceros si no es para permitir al proveedor la presentación de sus ofertas y la toma en consideración de sus productos como alternativa de suministro. Asimismo, facilitar al proveedor, en cualquier momento, la situación de sus datos en la base de datos, para que éste, previa notificación por escrito, los modifique o elimine. 


6. Derechos de Sea Competitivo

a. El Usuario presta su consentimiento para que Sea Competitivo ceda sus datos a las empresas a ella asociadas o del grupo al que pertenece, así como a otras con las que concluya acuerdos con la única finalidad de la mejor prestación del servicio, respetando, en todo caso, la legislación española sobre protección de los datos de carácter personal. Asimismo, el Usuario acepta que Sea Competitivo o sus empresas a ella asociadas, sociedades filiales y participadas, le remitan información sobre cualesquiera bienes o servicios que comercialicen, directa o indirectamente, o que en el futuro puedan comercializar. La aceptación del Usuario para que puedan ser cedidos sus datos en la forma establecida en este párrafo, tiene siempre carácter revocable sin efectos retroactivos, conforme a lo que dispone la Ley Orgánica 15/1999, de 13 de diciembre.) 

b. Optar, en caso de incumplimiento de sus obligaciones por el PROVEEDOR, por exigir el debido cumplimiento de las mismas, o por la resolución del contrato, en los términos de la cláusula 8 de este Aviso Legal. 

7. Limitación de responsabilidad

a. Sea Competitivo no asume ninguna responsabilidad derivada de la falta de disponibilidad del sistema, fallos de la red o técnicos que provoquen un corte o una interrupción en el Portal. Igualmente Sea Competitivo no se hace responsable de los daños y perjuicios que pueda ocasionar la propagación de virus informáticos u otros elementos en el sistema.

b. Sea Competitivo no asume ninguna responsabilidad de pagos y/o reclamaciones que deriven de las actuaciones de los clientes en relación al acuerdo entre dicho/s cliente/s y el PROVEEDOR

c. Sea Competitivo no asume ninguna responsabilidad sobre cualquier actuación que sea contraria a las leyes, usos y costumbres, o bien que vulnere las obligaciones establecidas en el presente Aviso Legal, por parte de ninguna persona de Sea Competitivo, del PROVEEDOR o cualquier usuario de la red. No obstante, cuando Sea Competitivo tenga conocimiento de cualquier conducta a la que se refiere el párrafo anterior, adoptará las medidas necesarias para resolver con la máxima urgencia y diligencia los conflictos planteados. 

d. Sea Competitivo se exime de cualquier responsabilidad derivada de la intromisión de terceros no autorizados en el conocimiento de las condiciones y circunstancias de uso que los PROVEEDORES hagan del Portal y de los Servicios.



8. Resolución del acuerdo

En caso de incumplimiento de cualquiera de las obligaciones contenidas en el presente Aviso legal, el PROVEEDOR o Sea Competitivo habrán de notificar dicho incumplimiento a la parte que lo hubiera cometido, otorgándole un plazo de quince días hábiles, contando desde la notificación, para que lo subsane. Transcurrido dicho plazo sin que se haya producido la subsanación del incumplimiento, la otra parte podrá optar por el cumplimiento o la resolución del presente acuerdo, con la indemnización de daños y perjuicios en ambos casos, de conformidad con lo establecido en el artículo 1.124 del Código Civil. En caso de optar por la resolución, tanto el PROVEEDOR como Sea Competitivo aceptan que la simple notificación será suficiente, para que la misma tenga plenos efectos.


9. Duración 

El presente acuerdo tendrá una duración indefinida siempre y cuando no se produzca denuncia por escrito de cualquiera de las partes, con al menos 1 mes de antelación.

El procedimiento de baja será enviando un mail con Asunto “Baja del portal” a la dirección soporte@seacompetitivo.com. 



10. Notificaciones

a. Las notificaciones entre las partes podrán hacerse por cualquier medio de los admitidos en Derecho, que permita tener constancia de la recepción, incluido el fax.

b. Los cambios de domicilios y faxes serán notificados por escrito y no producirán efectos hasta transcurridos dos días hábiles desde su recepción. En todo caso, el nuevo domicilio y fax será necesariamente dentro del territorio español.

c. Las relaciones entre Sea Competitivo y el PROVEEDOR derivadas del uso del Portal y de los servicios prestados a través del mismo, se someterán a la legislación y jurisdicción españolas. 

  </textarea>
              </font> </td>
            </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td align="left" class="textos"><p><span class="subtextos">&iquest;Acepta las condiciones de adhesi&oacute;n al portal de compras de Sea Competitivo?</span><br>
                <br>
           Si pulsa &quot;No Acepto&quot;, su empresa no podr&aacute; ser registrada como proveedor autorizado de Sea Competitivo.<br>
      Para continuar el proceso de alta debe pulsar &quot;Acepto&quot;. <br>
      Si quiere imprimir las condiciones puede hacerlo haciendo pulsando el bot&oacute;n &quot;Imprimir&quot;. <br>
      <br>
          
      <div align="center"></div>
          <table border="0" width="39%">
            <tr>
              <td width="41%" valign="middle"><p align="center">
                  <input type="submit" onclick="return alta()" value="Acepto" id=submit1 name=submit1>
              </p></td>
              <td width="38%" valign="middle"><input type="button" onclick="no_alta()" value="No acepto" id=button1 name=button1>
              </td>
              <td width="21%" valign="middle"><input type="button" onclick="imprimir()" value="Imprimir" id=button2 name=button2>
              </td>
            </tr>
          </table>
        <p align="center"></td>
      </tr>
  </table>
  <input type="hidden" name="ACEPTADO">
	  
</div>
</form>

</body>
<!--#include file="../../common/fsal_2.asp"-->
</html>
