﻿<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../../estilos.css" rel="stylesheet" type="text/css">


<title>Supplier Portal</title>
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
}
-->
</style>
</head>

<script>
dibujaMenu(1)
</script>

<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function Init() {
    document.getElementById('tablemenu').style.display = 'block';

	MM_preloadImages('../images/icono_docs_sm.gif','../images/icono_docs_sm.gif')

	p = window.top.document.getElementById("frSet")
	var vRows = p.rows
	var vArrRows = vRows.split(",")
	vRows = vArrRows[0] + ",*,0,0"
	p.rows = vRows

	
	
}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">
<!-------------------------- BEGIN COPYING THE HTML HERE ----------------------------> 
<table width="100%" border="0" cellpadding="5" cellspacing="2" bordercolor="0" hspace="0" vspace="0">
  <tr> 
    <td width="12" rowspan="2" align="right" valign="top" bordercolor="0"> <div align="left" class="textos"></div>
    </td>
    <td width="422" height="74" align="left" valign="middle"><font size="2" face="verdana" class="titulopr">Bienvenido/a al Portal de Proveedores</font></td>
    <td width="262" colspan="2" align="left" valign="bottom">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" bgcolor="#e6dbd1" class="textos">
	<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="textos">Para navegar por las distintas áreas del Portal, utilice la barra de navegación en la parte superior de la página.  Desde aquí tendrá acceso a:
      <ul>
        <li><b>Solicitudes de oferta:</b> acceda a las RFQ realizadas por by Campofrio Food Group. </li>
      </ul>
      <ul>
        <li><b>Sus datos :</b> actualice su información personal y configure a su medida los formatos (número de decimales, formatos de fecha, etc.) que quiere que muestre la aplicación.</li>
      </ul>
      <ul>
        <li><b>Su compañía:</b> actualice la información general de su empresa, indique los productos y servicios que puede ofrecer y registre nuevos usuarios en el Portal. </li>
      </ul>
      <br>
<br>
Si es la primera vez que va a realizar una oferta a trav&eacute;s del portal, siga atentamente los siguientes pasos:<br>
<br>
<br>
<b>1. Entre en el Módulo “Solicitudes de oferta”</b> para ver  para ver las peticiones de ofertas que tiene abiertas su empresa.<br>
<br>
<b>2. Seleccione la solicitud de oferta</b> a la que quiera responder,<b> pulsando sobre el c&oacute;digo de la misma. </b><br>
<br>
<b>3. Realice su oferta completando toda la informaci&oacute;n necesaria: </b>desde el &aacute;rbol de navegaci&oacute;n que encontrar&aacute; en la parte izquierda, podr&aacute; desplazarse por los distintos apartados que conforman la oferta. Para introducir los precios deber&aacute; ir al apartado "items/precios". Recuerde introducir el plazo de validez de la oferta, en el apartado "Datos Generales de la oferta". <br>
<br>
<b>4. Comunique su oferta pulsando sobre el bot&oacute;n de enviar</b><b><IMG height=14 src="../images/sobre_tr.gif" width=30 align=absBottom></b>    </td>
  </tr>
</table>

	</td>
    <td colspan="2" align="left" valign="top"><table width="100%" height="272" border="0" background="../images/fondo.jpg">
    <tr>
      <td height="170" valign="top" class="subtit"><div align="center">
        <table width="100%" border="0" class="textos">
          <tr>
            <td valign="top" class="textos">Puede descargarse los siguientes manuales:</td>
          </tr>
          <tr>
            <td width="273" class="logo"><a href="docs/FSN_MAN_ATC_Cómo ofertar.pdf" target="_blank" class="logo"><img src="../images/circulo.jpg" width="13" height="15" border="0" >Cómo ofertar</a></td>
          </tr>
          <tr>
            <td class="logo"><a href="docs/FSN_MAN_ATC_Requisitos técnicos.pdf" target="_blank" class="logo"><img src="../images/circulo.jpg" width="13" height="15" border="0" >Requisitos técnicos</a></td>
          </tr>
          <tr>
            <td class="logo"><a href="docs/Requisitos%20t&#233;cnicos%20proveedor.pdf" target="_blank"><img src="../images/circulo.jpg" width="13" height="15" border="0" ></a><a href="docs/FSN_MAN_ATC_Mantenimiento de datos.pdf" target="_blank" class="logo">Mantenimiento de datos </a></td>
          </tr>
        </table>
      </div></td>
    </tr>
    <tr>
      <td valign="bottom" class="textos"><table width="100%" border="0" align="center"  cellpadding="0" cellspacing="0" class="textos">
        <tr>
          <td height="30" valign="bottom"><div align="center"><img src="../images/soporte.gif" height="27"></div></td>
        </tr>
        <tr>
          <td height="30" valign="middle" class="textos"><div align="center">+34 902 999 755</div></td>
        </tr>
        <tr>
          <td height="30"><div align="center"><a href="mailto:sourcing@campofriofg.com" class="logo">sourcing@campofriofg.com</a></div></td>
        </tr>
      </table>
       </td>
    </tr>
    </table></td>
  </tr>

</table>
</body></html>
