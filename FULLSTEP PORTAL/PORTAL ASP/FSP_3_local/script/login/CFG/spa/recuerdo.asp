﻿<%@  language="VBScript" %>
<!--#include file="../../recuerdo.asp"-->
<html>
<head>
    <link href="../estilos.css" rel="stylesheet" type="text/css">
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style>
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body>
    <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
    <input type="hidden" name="idioma" value="eng">
    <table width="635" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="20">&nbsp;
                
            </td>
            <td width="615" align="left">
                 <img src="../images/logo.jpg" width="124" height="70">
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;
                
            </td>
        </tr>
        <tr>
            <td width="20">&nbsp;
                
            </td>
            <td width="615" align="left">
                <font color="#666666" size="1" face="Verdana" class="textos"><b><u>Recordatorio de datos de acceso
                </u></b>
                    <br>
                    <br>
                     Si ha olvidado su contraseña introduzca su código de compañía, usuario y la dirección de e-mail de su cuenta de acceso y pulse "Enviar". Recibirá un e-mail con el enlace a la página donde podrá crear una nueva contraseña.
                    <br>
                    <br>
                    <table class="textos">
                        <tr>
                            <td>
                                Código Compañía:
                            </td>
                            <td>
                                <input type="text" name="txtCia" id="txtCia" size="80" maxlength="100" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Usuario:
                            </td>
                            <td>
                                <input type="text" name="txtUsu" id="txtUsu" size="80" maxlength="100" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                E-mail:
                            </td>
                            <td>
                                <input type="text" name="txtEmail" id="txtEmail" size="80" maxlength="100" />
                            </td>
                        </tr>
                    </table>
                    <br />
                    <input type="submit" value="Enviar" name="cmdEnviar" id="cmdEnviar">
                    <br>
                    <br>
                    <br>
                   Si continúa con dificultades para acceder al portal o su dirección de email ha cambiado, consulte con el servicio de atención al cliente Tel. +34 902 999 755.  </font>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
