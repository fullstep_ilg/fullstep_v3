﻿<%@ Language=VBScript %>
<html>

<head>

<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/idioma.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->

<%
''' <summary>
''' Mostrar el detalle del registro
''' </summary>
''' <remarks>Llamada desde: login/login.asp		login/codigo instalaci�n/registro.asp	login/codigo instalaci�n/codigo idioma/registro.asp; Tiempo m�ximo: 0</remarks>
%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sign up contract - Campofrio Food Group Supplier Portal</title>

<link href="estilos.css" rel="stylesheet" type="text/css">

<script src="../../common/formatos.js"></script>
<script language="JavaScript" src="../../common/ajax.js"></script>
<!--#include file="../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>

function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=ENG", "_blank")
window.close()

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=550,height=600,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
  </script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>


<body bgcolor="#FFFFFF" topmargin="5" leftmargin="5" onload="init()">
<form name="frmAlta" id="frmAlta" method="post">

<table border="0" width="100%">
  <tr>
    <td><a href="http://www.campofriofoodgroup.com" target="_blank"><img src="images/logo.jpg" WIDTH="124" HEIGHT="70" border="0"></a>
    </td>
  </tr>
  <tr>
    <td><font face="Verdana" size="1">
      <p align="left" class="textos"> <b>To proceed with the sign up proccess, the following legal disclaimer must be read and accepted. </b> </p>
      <table border="0">
        <tr>
          <td valign="top"><p align="center"> </td>
          <td><font face="Verdana" size="1">
            <textarea readonly="true" rows="11" name="S1" cols="80" style="font-family: Verdana; font-size: 8pt; text-align:justify; display:block; height:135px; line-height: 150%; list-style-type: lower-alpha">
VERY IMPORTANT, READ CAREFULLY! 
Supplier register
Legal Notice

Portal access

Registering in the purchasing Portal of CAMPOFRIO FOOD GROUP, S.A. (from hereon referred to as CAMPOFRIO) implies previous reading and acceptance of the following clauses. Without expressing full compliance with the clauses you will not be allowed to register. Every time you access and use the Portal, it is understood you agree, fully and unreservedly, with the contents of these Legal Notice. Also, being the main user of your company, you are obliged by accepting this Legal Notice to ensure compliance by all users registered in your company, fully exonerating CAMPOFRIO from any responsibility of the damage those users may cause to your company or any other because of their interactions with the Portal.
 
Clauses

1. Purpose of the CAMPOFRIO purchasing Portal
The CAMPOFRIO purchasing portal is the means trough which CAMPOFRIO communicates with its suppliers to request offers, documents or any other commercial information it deems fit. At the same time CAMPOFRIO reserves the right to use the portal to communicate any information it considers to be of its interest.
CAMPOFRIO works both as a direct purchaser and as a purchasing broker for its clients, and shall act in their name during the purchasing process.
Access or use of Portal and / or Services does not give the SUPPLIER any rights on trademarks, commercial denominations or any other distinctive sign appearing in the portal, being CAMPOFRIO or third party’s property. Therefore, all contents are intellectual property of CAMPOFRIO or third party, and will not be understood as ceded to the SUPPLIER.

2. Purpose 
The purpose of this contract is to regulate relations between CAMPOFRIO and the SUPPLIER, in all aspects concerning use of the PORTAL.

3. SUPPLIER obligations 
The following are obligations of the SUPPLIER:
a. Provide whatever data is necessary for the adequate functioning of the system, and maintain them updated, communicating at the earliest possible time any modifications.
b. Guaranteeing the authenticity of data provided as a consequence of formularies necessary to subscribe to the Services offered by the PORTAL. Also, the SUPPLIER will update all information provided to ensure it reflects, at all times, its real situation. Therefore, the SUPPLIER is solely responsible of damages caused to CAMPOFRIO as a consequence of inexact or false statements.
c. Keeping absolute confidentiality in relation to all information derived from the relations between the SUPPLIER and CAMPOFRIO 
d. Abstaining from doing any modification in the information-technology use of the Portal, as well as using it with purposes different from those for which it is intended. Also, the SUPPLIER will abstain from accessing unauthorized zones of the Portal.
e. Comply loyally to the commitments expressed in the information sent through the portal. If the SUPPLIER does not demonstrate due commercial diligence, or does not comply with contracted duties, CAMPOFRIO reserves the right to exclude temporarily or permanently the SUPPLIER from the Portal.
f. The SUPPLIER will indicate only those material groups referring to goods or services that, at the time of acceptance of the contract, are commercialized, manufactured or distributed by the SUPPLIER and of commercial interest to CAMPOFRIO. 
g. The SUPPLIER accepts that offers sent through the portal are considered to be of the same rank and validity as offers sent through any other traditional means (letters, fax).
h. The SUPPLIER is obliged to use the Portal and its Services in compliance with the Law, this Legal Notice, and the rest of regulations and instructions of which it is informed, as well as in accordance to generally accepted morals and customs, and public order.
Thus, the SUPPLIER will abstain from using the Portal or any of its Services with illegal purposes, prohibited in this Legal Notice, and/or damaging to rights and interests of third parties. 
In particular, and merely as a matter of indication, the SUPPLIER agrees not to transmit, broadcast, or make available to others or third parties any information, data, contents, graphics, image and/or sound files, photographs, recordings, software and, in general, any type of material that:
Is contrary to fundamental rights and public liberties under the Constitution, International treaties and applicable laws;
Induces, incites or promotes criminal acts, contrary to law, moral and generally accepted customs or public order;
Is false, inexact or may induce to error, or constitutes illegal, deceitful or disloyal publicity;
Is protected under intellectual or industrial property rights belonging to third parties without prior explicit and written consent.
Contravenes laws and statutes for civil protection against defamation and invasion of privacy.
Contravenes regulations about privacy and/or secrecy of communications
Can be considered as unfair competition, or damages in any way the image of CAMPOFRIO or third parties. 
Is affected by viruses or similar elements that can harm or stop the Portal’s adequate performance, the electronic equipments or their files and documents.
 
4. Rights of the SUPPLIER
The following are rights of the SUPPLIER:
1. Maintain a constant presence in CAMPOFRIO’s database, as a registered supplier.
2. Receive offers according to the established rules. 

5. Obligations of CAMPOFRIO
The following are CAMPOFRIO obligations:
a. Maintain the information it deems fit constantly updated, without being liable or responsible of any mistakes that may happen by chance or due to circumstances beyond its control
b. Keep absolute confidentiality of all information concerning the SUPPLIER, both if supplied directly or generated as a consequence of relationships between the SUPPLIER and CAMPOFRIO 
c. Provide the SUPPLIER, at any time, the situation of its data inside the database, and not to provide it to any third parties except to allow the SUPPLIER to present offers to be taken into consideration as supply alternatives. Also, to provide the SUPPLIER, at any time, the situation of its data inside the database, so that the SUPPLIER, with previous written notification, modifies or deletes them.

6. Rights of CAMPOFRIO
a. The SUPPLIER gives its explicit consent for CAMPOFRIO to communicate information concerning the SUPPLIER to associated companies or companies belonging to the same group, as well as others with which it may sign agreements with the only aim of providing the best services, in compliance, in any case, with Spanish legislation on personal data protection. Also, the SUPPLIER agrees on CAMPOFRIO or its associates, subsidiaries or affiliates, or investee companies, sending information about goods or services they commercialize, directly or indirectly, or that will be commercialized. Acceptance by the SUPPLIER in the terms established in this paragraph, is always revocable without retroactive effect under the terms of the "Ley Orgánica 15/1999, de Protección de Datos de Carácter Personal" (Personal data protection Law)
b. Choose, in case of infringement by the SUPPLIER of its obligations, between demanding full compliance with its obligations or cancellation of the contract, in the terms expressed in clause 8 of this Legal Notice.

7.Limitation of responsibility
a. CAMPOFRIO shall not be liable or responsible for the availability of the service, network or technical failures that might cause an interruption or cuts in the Portal. 
Equally, CAMPOFRIO shall not be liable of any damages caused by the spreading of IT viruses or other elements in the system.
b. CAMPOFRIO shall not be liable or responsible of any payments and /or complaints that derive from the acts of clients concerning the agreement between the client(s) and the SUPPLIER.
c. CAMPOFRIO shall not be liable or responsible of any infringement of the Laws or generally agreed Customs, or of acts that contravene obligations established in this Legal Notice, by any employee of CAMPOFRIO, by the SUPPLIER, or by any Internet or Network user.
However, where CAMPOFRIO to have knowledge of any conduct the previous paragraph refers to, it shall adopt the necessary measures to resolve with the upmost diligence and urgency the conflicts caused. 
d. CAMPOFRIO shall not be held liable or responsible of any interference by unauthorized third parties in the knowledge of the conditions and circumstances of use that the SUPPLIER shall do of the Portal and its Services.

8. Contract cancellation 
In the case of failure to comply with the obligations contained in this Legal Notice, the SUPPLIER or CAMPOFRIO must notify the breach to the infringing party, with a fifteen working day period to repair it. After that period has passed by and, should the infringement not be repaired, the other party can choose between the fulfilling and cancellation of this agreement, with a compensation for damages in both cases, under the terms of the artículo 1.124 del Código Civil (article 1.124 of the Spanish civil code). Should it choose to cancel the contract, both the SUPPLIER and CAMPOFRIO agree that simple notification shall be enough for the cancellation to have full effect. 

9. Duration 
The present agreement shall have indefinite duration as long as no party express willingness to cancel the contract with a prior notice not shorter than a month.
The proceeding to cancel the contract will be to send an e-mail indicating “Baja del portal” (leave from the portal) to the address sourcing@campofriofg.com. Should the sending of the mail be impossible, a fax will be sent to (0034) + 91 296 20 25 indicating the basic data to identify the SUPPLIER.

10. Notifications, Legislation and Jurisdiction
a. Notifications between the parties may be carried out through any of the means admitted by the Law, that allows to have a record of reception, including fax.
b. Changes in addresses and fax numbers shall be notified in writing, and shall not have effect until two working days from reception.
c. The relationships between CAMPOFRIO and the SUPPLIER derived from the use of the Portal and the Services carried out inside it, will submit to Spanish legislation and jurisdiction. 

  </textarea>
          </font> </td>
        </tr>
      </table>
      </font>
        <p class="textos"> <b>Do you accept the sign up legal disclaimer to the CAMPOFRIO FOOD GROUP purchasing portal?<br>
                <br>
          </b> Shall you click on &lsquo;No, I refuse&rsquo;, the proccess will terminate and your company will not become a CAMPOFRIO FOOD GROUP supplier. <br>
          In order to continue the sign up proccess, you may click on &lsquo;Yes, I accept&rsquo;. <br>
          If you wish to print out the legal disclaimer, you may do so by clicking on the following buttom.<br>
      <br>
         
        <div align="right">
          <table width="39%" border="0" align="left">
            <tr>
              <td width="41%"><p align="left">
                  <input type="submit" onclick="return alta()" value="Yes, I accept" id="submit1" name="submit1">
              </p></td>
              <td width="38%"><input type="button" onclick="no_alta()" value="No, I refuse" id="button1" name="button1">
              </td>
              <td width="21%"><input type="button" onclick="imprimir()" value="Print" id="button2" name="button2">
              </td>
            </tr>
          </table>
        </div><br><br>
        <p align="left" class="textos">        
        <hr width="100%" size="1" color="#d71a21">
    </td>
  </tr>
</table>

	<input type="hidden" name="ACEPTADO">
	
</form>

</body>
<!--#include file="../../common/fsal_2.asp"-->
</html>
