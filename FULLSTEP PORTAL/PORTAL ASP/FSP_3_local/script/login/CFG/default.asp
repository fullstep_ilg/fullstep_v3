﻿<%@ Language=VBScript %>
<!--#include file="../login.asp"-->
<%		
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if

if Not IdOrden = "" then
    if Not IsNumeric(IdOrden) then
        IdOrden=""
    end if
end if

if Not CiaComp = "" then
    if Not IsNumeric(CiaComp) then
        CiaComp=""
    end if
end if

If Idioma="" then
	Idioma="ENG"
end if

%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Campofrio Food Group - Supplier Portal</title>
<script language="javascript">
<!--
var mesos=new Array(13);
  mesos[1]="de enero";
  mesos[2]="de febrero";
  mesos[3]="de marzo";
  mesos[4]="de abril";
  mesos[5]="de mayo";
  mesos[6]="de junio";
  mesos[7]="de julio";
  mesos[8]="de agosto";
  mesos[9]="de septiembre";
  mesos[10]="de octubre";
  mesos[11]="de noviembre";
  mesos[12]="de diciembre";

var semana=new Array(8);
  semana[1]="Domingo";
  semana[2]="Lunes";
  semana[3]="Martes";
  semana[4]="Miércoles";
  semana[5]="Jueves";
  semana[6]="Viernes";
  semana[7]="Sábado";

var ladata=new Date();

var elmes=mesos[ladata.getMonth() + 1];

var eldia=semana[ladata.getDay() + 1];

var numero=ladata.getDate();

var any=ladata.getYear();

if (any < 2000)
any = any + 1900;

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function ventanaLogin (IDI){

   window.open ("registro.asp?Idioma="+IDI,"","width=700,height=500,resizable=yes")

}
function ventanaSecundaria (URL){

   window.open(URL,"ventana1","width=750,height=700,scrollbars=YES")

}
function popup (URL) {
 window.open("popup.htm","","width=500,height=650,scrollbars=NO")
 }
function recuerdePWD()
{
   window.open("eng/recuerdo.asp","_blank","width=641,height=380,scrollbars=NO")
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<link href="estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {color: #333333}
-->
</style>
<body style="MARGIN: 0px" leftMargin="0" topMargin="0">
</head>

<div align="center">
  <table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="932" border="0" align="center">
          <tr>
            <td rowspan="2"><a href="http://www.campofriofoodgroup.com"><img src="images/logo.jpg" width="124" height="70" border="0"></a></td>
            <td><table width="350" border="0" align="right" cellpadding="0" cellspacing="0">
              <tr class="textosr">
                <td align="right" class="textos"><a href="javascript:ventanaSecundaria('<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/aviso legal.htm')" class="textosr">Legal notice </a> |&nbsp;<a href="<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=ENG" class="textosr"">Cookies policy</a>&nbsp;|&nbsp;<a href="spa/default.asp" class="textosr"">Español</a></td>
                <td width="4">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td class="titulopr"><div align="right">
              <table width="80%" border="0">
                <tr>
                  <td class="titulopr"><div align="right" class="titulopr">Supplier Portal</div></td>
                </tr>
              </table>
            </div></td>
          </tr>
          <tr>
            <td colspan="2"><img src="images/colores.gif" width="932" height="24"></td>
          </tr>
      </table></td>
    </tr>
    <tr>
      <td><table width="932" border="0" align="center">
          <tr>
            <td width="39%" valign="top"><table width="200" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td colspan="3" valign="top"><img src="images/suppliers.jpg" width="367"></td>
              </tr>
              <tr>
                <td width="1" rowspan="2" align="right" valign="top"><div align="right"><img src="images/form_izq.gif" width="1" height="145"></div></td>
                <td align="center"><form name="frmLogin" id="frmLogin" method="post" action="default.asp">
                    <div align="center">
                      <table width="250" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <input type="hidden" id="Idioma" name="Idioma" value="SPA">
                          <td width="5">&nbsp;</td>
                          <td height="24" class="textos">Company Code</td>
                          <td width="88" bgcolor="edece8"><input id="txtCia" name="txtCIA" maxlength="20" size="25" style="border: ridge #cccccc 1px; background-color: white; font-size: 10px; font-family: Arial">
                          </td>
                          <td width="11">&nbsp;</td>
                        </tr>
                        <tr>
                          <td width="5">&nbsp;</td>
                          <td height="24" class="textos">User</td>
                          <td width="88" bgcolor="edece8"><input type="text" name="txtUSU" size="25" style="border: ridge #cccccc 1px; background-color: white; font-size: 10px; font-family: Arial">
                          </td>
                          <td width="11">&nbsp;</td>
                        </tr>
                        <tr>
                          <td width="5" height="24">&nbsp;</td>
                          <td height="24" class="textos">Password</td>
                          <td width="88" bgcolor="edece8">                          <input name="txtPWD" type="password" maxlength="20" size="25" style="border: ridge #cccccc 1px; background-color: white; font-size: 10px; font-family: Arial">

                          </td>
                          <td width="11">&nbsp;</td>
                        </tr>
                        <tr>
                          <td width="5">&nbsp;</td>
                          <td height="24">&nbsp;</td>
                          <td align="right"><input type="image" name="cmdEntrar" src="images/login.gif" border="0" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cmdEntrar','','images/login.gif',1)" WIDTH="54" HEIGHT="15">
                          <td width="11">&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="center"></td>
                          <td colspan="2" class="logo"><div align="left"><a href="javascript:void(null)" class="logo" onClick="recuerdePWD()">Forgotten your login details?</a></div></td>
                          <td align="center">&nbsp;</td>
                        </tr>
                      </table>
                      <input type="hidden" id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
                      <input type="hidden" id="CiaComp" name="CiaComp" value="<%=CiaComp%>">
                    </div>
                </form></td>
                <td rowspan="2" align="right" valign="top"><img src="images/form_dcha.gif" width="1" height="145"></td>
              </tr>
              <tr>
                <td><table width="66%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                      <td align="center" valign="top">
                          <div align="left" class="logo"><a href="javascript:ventanaLogin('ENG')" class="logo">Register</a> </div>
                      </td>
                    </tr>
                </table></td>
              </tr>
              <tr>
                <td colspan="3" valign="top"><img src="images/bottom.jpg" width="367" height="7"></td>
              </tr>
            </table></td>
            <td width="61%" rowspan="2" align="right"><img src="images/mapa.jpg" width="537" height="372"></td>
          </tr>
          <tr>
            <td align="center" valign="top"><table width="50%" border="0">
              <tr>
                <td height="2"></td>
              </tr>
              <tr>
                <td valign="top" class="titulo"><img src="images/support.gif" height="27"></td>
              </tr>
              <tr>
                <td class="textos">Phone +34 902 999 755</td>
              </tr>
              <tr>
                <td class="textos"><a href="mailto:suppliersupport@fullstep.com" class="textos">suppliersupport@fullstep.com</a></td>
              </tr>
              <tr>
                <td class="logo"><a href="javascript:ventanaSecundaria('<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/manuales.htm')" class="logo">About the portal </a></td>
              </tr>
            </table></td>
          </tr>
      </table></td>
    </tr>
    <tr>
      <td><table width="932" border="0" align="center">
          <tr>
            <td width="38%"><table width="200" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="4"></td>
                <td width="47"></td>
                <td width="149" rowspan="3" align="center"><a href="http://www.fullstep.com/portal-de-compras/" target="_blank"><img src="images/logo_fullstep.png" width="100" height="24" border="0" hspace="5" alt="FULLSTEP NETWORKS,  S.L."></a></td>
              </tr>
              <tr>
                <td width="4">&nbsp;</td>
                <td class="subtextos" width="47"><div align="left"><font color="#666666">powered</font></div></td>
              </tr>
              <tr>
                <td width="4">&nbsp;</td>
                <td class="subtextos" valign="top" width="47"><div align="left"><font color="#666666">by</font></div></td>
              </tr>
            </table></td>
            <td width="62%" align="right" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td class="textosres">Website optimized for 1280 x 1024<br>
              Supported browsers: <a href="http://windows.microsoft.com/en-US/internet-explorer/download-ie" target="_blank">Internet Explorer</a> and <a href="http://www.mozilla.org/en-US/firefox/all/" target="_blank">Mozilla Firefox</a></td>
            <td align="right" class="textosres">Campofrio Food Group, S.A. - Avda. Europa, 24 - Parque empresarial La Moraleja  28108 Alcobendas, Madrid (Spain) </td>
          </tr>
      </table></td>
    </tr>
  </table>
</div>
</body>
</html>

