﻿<%@ Language=VBScript %>
<!--#include file="../login.asp"-->
<%			
''' <summary>
''' Pantalla inicial de la personalización, es la q llama a la pantalla de login
''' </summary>
''' <remarks>Llamada desde: registro/registrarproveedor.asp		proveedor/default.asp
'''		login/personalizacion/login.asp		; Tiempo máximo: 0,1</remarks>	
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if

If Idioma="" then
	Idioma="SPA"
end if

%>

<html>
<head>
<title>:: Portal de Proveedores de Queser&iacute;as Entrepinares::</title>
<script language="javascript">
<!--
var mesos=new Array(13);
  mesos[1]="de enero";
  mesos[2]="de febrero";
  mesos[3]="de marzo";
  mesos[4]="de abril";
  mesos[5]="de mayo";
  mesos[6]="de junio";
  mesos[7]="de julio";
  mesos[8]="de agosto";
  mesos[9]="de septiembre";
  mesos[10]="de octubre";
  mesos[11]="de noviembre";
  mesos[12]="de diciembre";

var semana=new Array(8);
  semana[1]="Domingo";
  semana[2]="Lunes";
  semana[3]="Martes";
  semana[4]="Miércoles";
  semana[5]="Jueves";
  semana[6]="Viernes";
  semana[7]="Sábado";

var ladata=new Date();

var elmes=mesos[ladata.getMonth() + 1];

var eldia=semana[ladata.getDay() + 1];

var numero=ladata.getDate();

var any=ladata.getYear();

if (any < 2000)
any = any + 1900;

function ventanaLogin (IDI){

   window.open ("registro.asp?Idioma="+IDI,"","width=650,height=520,resizable=yes")

}
function ventanaSecundaria (URL){

   window.open(URL,"ventana1","width=700,height=400,scrollbars=no")

}
function ventanaAyuda (URL){

   window.open(URL,"ayuda","width=900,height=700,scrollbars=yes")

}

function popup (URL) {
 window.open("popup.htm","","width=500,height=650,scrollbars=NO")
 }
function recuerdePWD()
{
   window.open("spa/recuerdo.asp","_blank","width=641,height=400,scrollbars=NO")
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
//-->
</script>
<link href="estilos.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
body {
	background-image: url();
	background-color: #f4f5e9;
	margin-top: 0px;
}
.Estilo1 {color: #666666}
-->
</style><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></head>
<body>
<div align="center">
  <table width="700" border="0" bgcolor="ffffff">
    <!--DWLayoutTable-->
    <tr>
      <td width="100%" height="117"><table width="100%" border="0" cellspacing="0">
          <tr>
            <td height="95"><div align="left">
                <table width="100%"  border="0">
                  <tr>
                    <td><a href="http://www.entrepinares.es" target="_blank"><img src="images/cabecera.png" width="700" height="166" border="0"></a>                      <div align="center"></div></td>
                  </tr>
                </table>
</div></td>
          </tr>
          <tr>
            <td><img src="images/punto.gif" width="700" height="4"></td>
          </tr>
          <tr>
            <td align="right" valign="middle"><table width="360" border="0">
              
              <tr>
                <td width="82"><div align="center"><a href="javascript:ventanaAyuda('<%=application("RUTANORMAL")%>custom/<%=application("NOMPORTAL")%>/public/documentos.htm')" class="textos Estilo1">Documentos</a></div></td>
                <td width="16"><div align="center"><span class="subtit">|</span></div></td>
                <td width="59"><div align="center"><a href="javascript:ventanaAyuda('<%=application("RUTANORMAL")%>custom/<%=application("NOMPORTAL")%>/public/manuales.htm')" class="textos Estilo1">ayuda</a></div></td>
                <td width="16"><div align="center" class="subtit">|</div></td>
                <td width="61"><div align="center"><a href="eng/default.asp?Idioma=ENG" class="textos">English</a></div></td>
                <td width="16"><div align="center" class="subtit">|</div></td>
                <td width="77"><a href="javascript:ventanaSecundaria('<%=application("RUTANORMAL")%>custom/<%=application("NOMPORTAL")%>/public/aviso legal.htm')" class="textos">aviso legal</a></td>
                <td>|</td>
                <td width="87"><a href="<%=application("RUTANORMAL")%>script/politicacookies/politicacookies.asp?Idioma=SPA" class="textos Estilo1">Pol&iacute;tica de Cookies</a></td>
              </tr>
            </table></td>
          </tr>
      </table></td>
    </tr>
    <tr>
      <td><table width="100%" border="0"><!--DWLayoutTable-->
          <tr>
            <td width="15" align="left" valign="top" class="titulo"><!--DWLayoutEmptyCell-->&nbsp;</td>
            <td width="42%" align="left" valign="top" >          <p class="textos">Queser&iacute;as Entrepinares ofrece a sus proveedores un canal &aacute;gil de comunicaci&oacute;n que garantiza la privacidad total en sus procesos de compra. Si desea formar parte del panel de proveedores de Queser&iacute;as Entrepinares y todav&iacute;a no se ha registrado en el portal, solicite el registro a trav&eacute;s del enlace &quot;solicitar registro&quot;. </p>
			  <p class="textos">Si tiene alguna duda en cuanto al funcionamiento de este portal de proveedores, consulte el apartado "ayuda" o p&oacute;ngase en contacto con nosotros a trav&eacute;s del email <a href="mailto:%20compras@entrepinares.es">compras@entrepinares.es</a> </p></td>
            <td width="100%" rowspan="2" align="center" valign="top"><table width="100%" border="0"><!--DWLayoutTable-->
                <tr>
                  <td valign="middle"><!--DWLayoutEmptyCell-->&nbsp;</td>
                  <td align="center"><table width="100%" border="0">
                    <tr>
                      <td align="right"><table width="200" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td colspan="3" valign="top"><img src="images/acceso.gif" width="300" height="22"></td>
            </tr>
            <tr>
              <td width="1" rowspan="2" align="right" valign="top"><div align="right"><img src="images/form_izq.gif" width="1" height="145"></div></td>
              <td align="center"><form name="frmLogin" id="frmLogin" method="post" action="default.asp">
                  <div align="center">
                    <table width="250" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <input type="hidden" id="Idioma" name="Idioma" value="SPA">
                        <td width="5">&nbsp;</td>
                        <td height="24" class="textos">Código Compañía </td>
                        <td width="88" bgcolor="edece8"><input id="txtCia" name="txtCIA" maxlength="20" size="25" style="border: ridge #cccccc 1px; background-color: white; font-size: 10px; font-family: Arial">
                        </td>
                        <td width="11">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="5">&nbsp;</td>
                        <td height="24" class="textos">Usuario</td>
                        <td width="88" bgcolor="edece8"><input type="text" name="txtUSU" size="25" style="border: ridge #cccccc 1px; background-color: white; font-size: 10px; font-family: Arial">
                        </td>
                        <td width="11">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="5" height="24">&nbsp;</td>
                        <td height="24" class="textos">Contraseña</td>
                        <td width="88" bgcolor="edece8"><input name="txtPWD" type="password" maxlength="20" size="25" style="border: ridge #cccccc 1px; background-color: white; font-size: 10px; font-family: Arial" autocomplete="off">
                        </td>
                        <td width="11">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="5">&nbsp;</td>
                        <td height="24">&nbsp;</td>
                        <td align="right"><input type="image" name="cmdEntrar" src="images/entrar1.gif" border="0" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cmdEntrar','','images/login.gif',1)" width="56" height="13">
                        <td width="11">&nbsp;</td>
                      </tr>
                      <tr>
                        <td align="center"></td>
                        <td colspan="2" class="logo"><div align="left"><a href="javascript:void(null)" class="registro" onClick="recuerdePWD()">&iquest;Ha olvidado sus claves ?</a></div></td>
                        <td align="center">&nbsp;</td>
                      </tr>
                    </table>
                    <input type="hidden" id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
                    <input type="hidden" id="CiaComp" name="CiaComp" value="<%=CiaComp%>">
                  </div>
              </form></td>
              <td rowspan="2" align="right" valign="top"><img src="images/form_dcha.gif" width="1" height="145"></td>
            </tr>
            <tr>
              <td><table width="80%" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tr>
                    <td align="center" valign="top"><div align="left" class="logo"><a href="javascript:ventanaLogin('SPA')" class="registro">Solicitar registro </a> </div></td>
                  </tr>
              </table></td>
            </tr>
            <tr>
              <td colspan="3" valign="top"><img src="images/bottom.gif" width="300" height="17"></td>
            </tr>
          </table></td>
                    </tr>
                    <tr>
                      <td align="right"><!--
                        <table width="126" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td width="4"></td>
                              <td width="47"></td>
                              <td rowspan="3" width="149"><div align="left"><a href="http://www.fullstep.com" target="_blank"><img src="images/fullgiro_web_blanco.gif" alt="FULLSTEP" width="40" height="40" hspace="5" border="0"></a></div></td>
                            </tr>
                            <tr>
                              <td width="4">&nbsp;</td>
                              <td class="textos" width="47"><div align="left"><font color="#666666">powered</font></div></td>
                            </tr>
                            <tr>
                              <td width="4">&nbsp;</td>
                              <td class="textos" valign="top" width="47"><div align="left"><font color="#666666">by</font></div></td>
                            </tr>
                          </table>
                         --></td>
                    </tr>
                  </table></td>
                </tr><tr><td height="3"></td><td><!--DWLayoutEmptyCell-->&nbsp;</td></tr>
                  </table></td>
          </tr>
          <tr>
            <td width="15" align="left" valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
            <td width="42%" align="left" valign="top" class="textos"><!--DWLayoutEmptyCell-->&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
            <td colspan="2" align="left" valign="top" class="textos"><div align="center" class="subtit"> Queserias ENTREPINARES, S.A.U. - C/Vázquez de Menchaca 9, parcela 137, Pol&iacute;gono Argales 47008, Valladolid (Espa&ntilde;a)<br>
                 Tf. +34 983 457 725, Fax +34 983 457 727, <a href="http://www.entrepinares.es" target="_blank">www.entrepinares.es</a>              | <a href="http://www.microsoft.com/spain/windows/internet-explorer/download-ie.aspx" target="_blank" class="subtit">Internet explorer 6.0 &oacute; superior</a> | Powered by <a href="http://www.fullstep.com" target="_blank">FULLSTEP</a> </div></td>
          </tr>

      </table></td>
    </tr>
    
  </table>
</div>
</body>
</html>
