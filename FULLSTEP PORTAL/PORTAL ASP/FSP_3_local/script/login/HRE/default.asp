﻿<%@ Language=VBScript %>
<!--#include file="../login.asp"-->
<%		
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if

if Not IdOrden = "" then
    if Not IsNumeric(IdOrden) then
        IdOrden=""
    end if
end if

if Not CiaComp = "" then
    if Not IsNumeric(CiaComp) then
        CiaComp=""
    end if
end if

If Idioma="" then
	Idioma="SPA"
end if

%>


<html lang="es-ES">
<head>
<title>:: Portal de Proveedores ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />  
<meta name="viewport" content="width=device-width, initial-scale=1" />  

<link rel="canonical" href="/" />

<link href="css/reset.css" rel="stylesheet" type="text/css" />        
<link href="css/font-awesome.min.css" rel="stylesheet" />  
<link href="css/style.css" rel="stylesheet" />  
<link rel="stylesheet" type="text/css" href="css/responsive.css">   
        
<!--[if lte IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script language="javascript">
<!--

function ventanaLogin (IDI){

   window.open ("registro.asp?Idioma="+IDI,"","width=700,height=540,resizable=yes")

}
function ventanaSecundaria (URL){

   window.open(URL,"ventana1","width=700,height=420,scrollbars=no")

}
function ventanaAyuda (URL){

   window.open(URL,"ayuda","width=700,height=600,scrollbars=yes")

}

function popup (URL) {
 window.open("popup.htm","","width=700,height=400,scrollbars=NO")
 }
function recuerdePWD()
{
   window.open("spa/recuerdo.asp","_blank","width=1030,height=622,scrollbars=NO")
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
//-->
</script>
<script>
$(document).ready(function(){
	//---------------------
	
	$("#txtCIA").val("Código Compañía");
  	$("#txtCIA").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Código Compañía"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Código Compañía");
  			$(this).removeClass('focus');
  		}
  	});	
	//---------------------
  	$("#txtUSU").val("Código de Usuario");
  	$("#txtUSU").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Código de Usuario"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Código de Usuario");
  			$(this).removeClass('focus');
  		}
  	});
	//---------------------
  	$("#txtPWD").val("Contraseña");
	$("#txtPWD").focus(function(){
/*  	$("#txtPWD").bind('focus',function(){*/
  		$(this).addClass('focus');
  		if($(this).val()=="Contraseña"){
  			$(this).val('');}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Contraseña");
  			$(this).removeClass('focus');
  		}
  	});
});
</script>

</head>
<body>
<header id="heading">             
            
      		<div class="container_haya">  
            
           		
            
                    <div class="logotipo">
            			<a href="http://www.haya.es" title="Haya Real Estate"><img src="img/logo_haya.png" alt="Haya" width="160" height="156"/></a>
                    </div>  
                    
                
                    
                         
                    <nav>                      
                        <ul class="idiomas">
                                                  
                            <li>
                            	<strong class="trigger">Idioma <i class="fa fa-caret-down"></i></strong>
                            	<ul>
	                            	<li><a href="#" title="Español">Español</a></li>
	                            	<li><a href="eng/default.asp" title="English">English</a></li>
                            	</ul>
                            </li>                                 
                        </ul> 
                    </nav>                   
                                
           		</div>   
         <div class="header">
            
                <div class="menu">  
        
                    <a class="menu-rsp" href="#" title="Men&uacute;"><span>Men&uacute;</span></a>
        
                    <div class="menu-container_haya">
                    
                        <ul>
                            <li class="menu-padre activo"><a href="#" title="Inicio">Inicio</a></li>   
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/politica-de-compras.html" title="Pol&iacute;tica de compras">Pol&iacute;tica de compras</a></li>  
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/quienes-somos.html" title="Quienes somos">Quienes somos</a></li>  
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/nuestros-valores.html" title="Nuestros valores">Nuestros valores</a></li>
                            
                        </ul>
                    <!--                   
                        <ol>
                            <li class="activo"><a href="#" title="CAST">Castellano</a></li> 
                            
                            <li><a href="#" title="ENG">English</a> </li>
                        </ol> 
                        -->
                    
                   	</div>    
            
                </div> 
                
            </div>
            
        </header>              
        <section id="home"> 
            
            <div class="container_haya">
            
           		<div class="section-acceso">
                
                	<div class="acceso">
                    
                    	<h2>ACCESO PROVEEDORES</h2>
                        
                        <div>
                        
                            <form action="default.asp" method="post" name="frmLogin"  id="frmLogin">
                              <label>  <input type="text" class="label" id="txtCIA" name="txtCIA" maxlength="20" /> </label>
        						<label><input type="text" class="label" id="txtUSU" name="txtUSU" maxlength="20" /> </label>
      							<label><input type="password" class="label" id="txtPWD" name="txtPWD" maxlength="20" /> </label>
                                <input class="btn btn-left" name="cmdEntrar" type="submit" value="ENTRAR" />
                                 <span class="acceso-recordar-claves"><a href="javascript:void(null)" onClick="recuerdePWD()" >¿Olvidó su clave de acceso?</a></span>
                           	<input type="hidden" id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
        					<input type="hidden" id="CiaComp" name="CiaComp" value="<%=CiaComp%>">
                            </form>
                            
                            
                            <h4>¿AÚN NO ESTÁ REGISTRADO?</h4>
                            
                            <p><a class="btn" href="registro.asp?Idioma=SPA">SOLICITAR REGISTRO</a></p>
                            
                   		</div>
               
               		</div>
                    
                    <div class="soporte">
                    
                    	<div>
							<h2>SOPORTE A PROVEEDORES</h2>
							<h3>Tel. 910 770 421</h3>
	                        <h3>Tel. 902 039 818</h3>
    	                    <a href="mailto:soporteproveedores@haya.es">soporteproveedores@haya.es</a>
        				</div>                
                        <div>
                        	<p><strong>HORARIO DE ATENCIÓN TELEFÓNICA</strong></p>
                            <p>Lunes a Jueves: 8:00 a 21:00h<br />Viernes: 8:00 a 19:00h</p>
                        </div>
                    
                    </div>
                    
         		</div>
         	
            </div>
              
    	</section>
      
        
       <footer id="footer">
        
        	<div class="container_haya">            
                    
            	<div class="footer-menu">
                
                    <ul>
                    	<li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ayuda.html" target="_blank">FAQ</a></li>
                    	<li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/aviso-legal.html" target="_blank">Aviso Legal</a></li>
                    	<li><a href="<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=SPA" >Política de Cookies</a></li>
                    </ul>
                    
                    <p>Web optimizada para 1280 x 1024. Navegadores soportados: Internet Explorer, Google Chrome y Mozilla Firefox<br />© FULLSTEP  Todos los Derechos Reservados.</p>
                    
                    <a href="http://www.fullstep.com" target="_blank"><img class="fullstep" src="img/fullstep.png" /></a>
                    
              	</div>
            
           	
               
            </div>
           
        </footer>
                       
                            
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>    
 		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>        



</body>

</html>
