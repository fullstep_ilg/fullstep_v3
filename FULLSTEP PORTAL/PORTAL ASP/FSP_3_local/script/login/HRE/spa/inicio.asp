﻿<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
  <!--#include file="../../../common/idioma.asp"-->
  <!--#include file="../../../common/formatos.asp"-->
  <!--#include file="../../../common/acceso.asp"-->
  <!--#include file="../../../common/Pendientes.asp"-->
  <script SRC="../../../common/menu.asp"></script>
  <html>

  <head>
    <link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <link href="../css/style.css" rel="stylesheet" type="text/css">
    <link href="../css/responsive.css" rel="stylesheet" type="text/css">
    <title>::Portal de proveedores::</title>
    <!--[if lte IE 9]>
<link href="../estilos_ie9.css" rel="stylesheet" type="text/css" />
<![endif]-->
    <script language="JavaScript">
      
<!-- 
        function MM_preloadImages() { //v3.0
          var d = document; if (d.images) {
            if (!d.MM_p) d.MM_p = new Array();
            var i, j = d.MM_p.length, a = MM_preloadImages.arguments; for (i = 0; i < a.length; i++)
              if (a[i].indexOf("#") != 0) { d.MM_p[j] = new Image; d.MM_p[j++].src = a[i]; }
          }
        }

      function MM_swapImgRestore() { //v3.0
        var i, x, a = document.MM_sr; for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) x.src = x.oSrc;
      }

      function MM_swapImage() { //v3.0
        var i, j = 0, x, a = MM_swapImage.arguments; document.MM_sr = new Array; for (i = 0; i < (a.length - 2); i += 3)
          if ((x = MM_findObj(a[i])) != null) { document.MM_sr[j++] = x; if (!x.oSrc) x.oSrc = x.src; x.src = a[i + 2]; }
      }
//-->
    </script>
    <style type="text/css">
      <!-- body {
        margin-left: 0px;
      }
      -->
    </style>

    <link href="../css/modals.css" rel="stylesheet" type="text/css">
    <script src="../js/jsmodal.js"></script>


  </head>

  <script>
    dibujaMenu(1)
  </script>

  <script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function Init() {
  document.getElementById('tablemenu').style.display = 'block';

	MM_preloadImages('../images/icono_docs_sm.gif','../images/icono_docs_sm.gif');
	p = window.top.document.getElementById("frSet");
	vRows = p.rows;
	vArrRows = vRows.split(",");
	vRows = vArrRows[0] + ",*,0,0";
	p.rows = vRows; 

}

</script>

  <body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">
    <div id="servicios">

      <div class="container_haya">


        <div class="centro centro_bienvenidos_haya">
          <div>

            <div class="bienvenidos_haya"> Bienvenido al Portal de Proveedores</div>

            Puede acceder a las distintas áreas a través de las opciones de menú situadas en la parte superior:
            <br />
            <br />

            <ul class="lista-enum">
              <li>
                <p>
                  <strong>Calidad:</strong> En esta zona deberá contestar y actualizar la información y documentos que Haya Real
                  Estate le solicite.</p>
              </li>
              <li>
                <p>
                  <strong>Contratos:</strong> Puede acceder a los contratos que Haya Real Estate ha publicado para su gestión.</p>
              </li>
              <li>
                <p>
                  <strong>Facturas:</strong> Registre sus facturas y haga un seguimiento de las mismas desde esta sección.</p>
              </li>
              </li>
            </ul>



          </div>

          <div style="margin-top:10px;font-size: 18px">
            <strong style="color:red">IMPORTANTE:</strong> el nuevo CIF de Haya Real Estate, S.A.U. es
            <strong>A-86744349</strong>
          </div>

        </div>

        <div class="columna-dcha-Haya">
          <div class="cuerpo-menu-inicio">
            <div class="manual_consulta_inicio">
              <p>
                <strong>INSTRUCCIONES</strong>
                <br /> Desde este apartado puede descargarse los siguientes manuales:</p>
            </div>

            <ul>
              
              <li>
                <a href="docs/FSN_MAN_ATC_Requisitos técnicos.pdf" target="_blank">Requisitos t&eacute;cnicos</a>
              </li>
              <li>
                <a href="docs/FSN_MAN_ATC_Mantenimiento de datos.pdf" target="_blank">Mantenimiento de datos</a>
              </li>
              <li>
                <a href="docs/FSN_MAN_SOP_Manual de gestión de facturas.pdf" target="_blank">Gestión de facturas</a>
              </li>
              <li>
                <a href="docs/Homologacion_Proveedores_HRE_06_2018_.pdf" target="_blank">Gestión de calidad</a>
              </li>

            </ul>
            <!--<div class="limpia"></div>
            <div style="clear:both"></div>-->
            <div class="manual_consulta_inicio">
              <div class="imagen_help">
                <img src="../img/telefonista_peq.jpg" width="80" height="70" />
              </div>
              <div class="txt_help">
                Si tiene alguna duda o dificultad para operar con el Portal, póngase en contacto con nosotros a través del mail:
                <a href="mailto:soporteproveedores@haya.es" title="soporteproveedores@haya.es" class="blue">soporteproveedores@haya.es</a>
              </div>


            </div>

          </div>
        </div>
      </div>
    </div>

    </div>
    <script>
      document.addEventListener('DOMContentLoaded', function () {

        var alertMsn = getCookie('alertMsnRead');
        console.log(alertMsn);

        if (alertMsn != true) {

          jsModal('miventana', {
            title: 'AVISO IMPORTANTE',
            width: 400,
            height: 400,
            content: 'Le informamos que tras el cambio en la forma jurídica de nuestra compañía el nuevo CIF de <br> Haya Real Estate, S.A.U. es ' +
              '<br><br> <strong style="font-size:22px;color:red;">A-86744349</strong><br><br>' +
              'Por favor, tenga en cuenta esta modificación de cara a la emisión de las facturas que registre a través del Portal.<br>' +
              'Muchas gracias'
          },
            ['Aceptar', function () {
              setCookie('alertMsnRead', 'true', 60);
            }]);

        }

      });
    </script>
  </body>

  </html>