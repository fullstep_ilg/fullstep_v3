﻿<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link href="../css/responsive.css" rel="stylesheet" type="text/css">
<title>::Portal de proveedores::</title>
<!--[if lte IE 9]>
<link href="../estilos_ie9.css" rel="stylesheet" type="text/css" />
<![endif]-->
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
}
-->
</style>
</head>
<script>
dibujaMenu(1)
</script>
<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function Init() {
    document.getElementById('tablemenu').style.display = 'block';

	MM_preloadImages('../images/icono_docs_sm.gif','../images/icono_docs_sm.gif')
	p = window.top.document.getElementById("frSet")
	vRows = p.rows
	vArrRows = vRows.split(",")
	vRows = vArrRows[0] + ",*,0,0"
	p.rows = vRows

}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">
<div id="servicios"> 
            
            <div class="container_haya">
            
                    
                    <div class="centro centro_bienvenidos_haya">
                      <div>
                        
                      <div class="bienvenidos_haya"> Welcome to the Supplier Portal</div>
                        
                 To navigate through the different areas of the portal, use the navigation bar in the topside of the page. From here you will be able to access: 
			<br /><br />
           
			<ul class="lista-enum">
    			<li><p><strong> Quality:</strong> Manage and update the information and documents requested by Haya Real Estate.</p></li>
				<li><p><strong>Contracts:</strong> You can access the contracts that Haya Real Estate has published for its management.</p></li>
<li>
  <p><strong>Invoices:</strong>:  Register your invoices and follow them from this section.</p></li>
				</li>
            </ul>
			<p><strong>For the time being, only invoices linked to Information Technology services may be registered in this way. You shall receive an email as soon as it becomes available to upload your invoices through this channel. Until that moment, please send your invoices as you had been doing in the past</strong>.</p>
           
          
                      </div>
                    </div>
                    
                  <div class="columna-dcha-Haya">
                  <div class="cuerpo-menu-inicio">
      <div class="manual_consulta_inicio">
                  <p><strong>INSTRUCTIONS</strong><br />
You can download the following tutorials:</p>
      </div>

      <ul>
            <li><a href="docs/FSN_MAN_ATC_Requisitos técnicos.pdf" target="_blank">Technical Requirements</a></li>
            <li><a href="docs/FSN_MAN_ATC_Mantenimiento de datos.pdf" target="_blank">Master Data</a></li>
		  <li><a href="docs/FSN_MAN_SOP_Manual de gestión de facturas.pdf" target="_blank">Gestión de facturas</a></li>
        </ul>
            <!--<div class="limpia"></div>
            <div style="clear:both"></div>-->
            <div class="manual_consulta_inicio">
           <div class="imagen_help"><img src="../img/telefonista_peq.jpg" width="80" height="70" /></div>
<div class="txt_help">
If you have any doubt or difficulty to operate with the Portal, please contact us through the mail: <a href="mailto:soporteproveedores@haya.es" title="soporteproveedores@haya.es" class="blue">soporteproveedores@haya.es</a></div>
                  
                  
                    </div>
               
           	  </div>
                 </div>
        </div>   
         	</div>
              
    	</div>

</body>
</html>
