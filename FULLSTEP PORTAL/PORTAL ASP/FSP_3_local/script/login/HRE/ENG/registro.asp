﻿<%@ Language=VBScript %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <!--#include file="../../../common/acceso.asp"-->
        <!--#include file="../../../common/idioma.asp"-->
        <!--#include file="../../../common/formatos.asp"-->
        <!--#include file="../../../common/fsal_1.asp"-->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Portal de Proveedores Haya Real Estate</title>
        <link href="../css/reset.css" rel="stylesheet" type="text/css" />
        <link href="../css/font-awesome.min.css" rel="stylesheet" />

        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'
        />
        <link href="../css/style.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="../css/responsive.css" />
        <script src="../js/jquery-1.9.0.js"></script>
        <script src="../js/jquery-1.9.0.min.js"></script>
        <!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
        <script src="../../../common/formatos.js"></script>
        <script language="JavaScript" src="../../common/ajax.js"></script>
        <!--#include file="../../../common/fsal_3.asp"-->
        <script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
    </head>

    <script>
        function no_alta() {
            window.close()

        }
    </script>

    <script>
        /*
        ''' <summary>
        ''' Lanzar la pantalla de registro de nuevos proveedores de Portal
        ''' </summary>     
        ''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
        function alta() {
            setCookie("CONTRATOACEPTADO", 1, new Date())
            window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=ENG", "_blank", "width=730,height=635")

            return true
        }
        function imprimir() {
            window.open("registro_texto.htm", "_blank", "width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

        }

        function setCookie(name, value, expires) {
            //If name is the empty string, it places a ; at the beginning
            //of document.cookie, causing clearCookies() to malfunction.
            document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();

        }

        function clearCookie(name) {
            expires = new Date();
            expires.setYear(expires.getYear() - 1);

            document.cookie = name + '=null' + '; expires=' + expires.toGMTString();
        }
    </script>


    <script language="javascript">< !--

var msg = "Comando incorrecto.";

        function RClick(boton) {
            if (document.layers && (boton.which == 3 || boton.which == 2)) { alert(msg); return false }
            if (document.all && event.button == 2 || event.button == 3) alert(msg)
            return false
        }

        document.onmousedown = RClick

//--></script>

    <body onload="init()">

        <header id="heading">

            <div class="container_haya">

                <div class="logotipo">
                    <a href="http://www.haya.es" title="Haya Real Estate">
                        <img src="../img/logo_haya.png" alt="Logo Haya" width="160" height="156" />
                    </a>
                </div>

                <nav>
                    <ul class="idiomas">
                        <li>
                            <strong class="trigger">Language
                                <i class="fa fa-caret-down"></i>
                            </strong>
                            <ul>
                                <li>
                                    <a href="../registro.asp" title="Español">Español</a>
                                </li>
                                <li>
                                    <a href="#" title="English">English</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>



            </div>

            <!-- 	  <h1>PORTAL DE PROVEEDORES</h1>      
                     
    		</div>
        
        
            <div class="header">
            
                 
                
  </div>
            
        </header>           
    			
        -->
            <div class="header">

                <div class="menu">

                    <a class="menu-rsp" href="#" title="Men&uacute;">
                        <span>Men&uacute;</span>
                    </a>

                    <div class="menu-container_haya">

                        <ul>
                            <li class="menu-padre">
                                <a href="default.asp" title="Inicio">Home</a>
                            </li>
                            <li class="menu-padre">
                                <a href="<%=application(" RUTASEGURA ")%>custom/<%=application("NOMPORTAL
                                    ")%>/public/eng/politica-de-compras.html" title="Pol&iacute;tica de compras">Purchasing Policy</a>
                            </li>
                            <li class="menu-padre">
                                <a href="<%=application(" RUTASEGURA ")%>custom/<%=application("NOMPORTAL
                                    ")%>/public/eng/quienes-somos.html" title="Quienes somos">Who we are</a>
                            </li>
                            <li class="menu-padre">
                                <a href="<%=application(" RUTASEGURA ")%>custom/<%=application("NOMPORTAL
                                    ")%>/public/eng/nuestros-valores.html" title="Nuestros valores">Our values</a>
                            </li>

                        </ul>

                        <ol>
                            <li class="activo">
                                <a href="#" title="CAST">Castellano</a>
                            </li>

                            <li>
                                <a href="#" title="ENG">English</a>
                            </li>
                        </ol>

                    </div>

                </div>

            </div>

        </header>
        <section id="servicios">

            <div class="container_haya">

                <div class="text">

 
                    <div class="columna sol">
                        <div class="imagen">
                            <img src="../img/shim.gif">
                        </div>

                    </div>

                    <div class="centro sin solicitud">
                        <div>

                            <h2>
                                <em>REGISTRATION</em>
                            </h2>
                            <div class="int">
                                <p>To proceed with the registration proccess, the following contract must be read and accepted:</p>
                                
                                
                                <div class="caja_registro">
                                    <h4>IMPORTANT, PLEASE READ CAREFULLY</h4>
                                    <br>

                                    <strong class=" blue">Supplier Registration
                                        <br> Legal notice</strong>
                                    <br>
                                    <br>
                                    <p>In compliance with the Organic Law 15/1999, of December 13, for the protection of personal
                                        data we inform you that the data you provide us will become part of a file owned
                                        by HAYA REAL ESTATE, S.L.U. with the sole purpose of managing the relationships established
                                        with you.</p>
                                    <p>HAYA REAL ESTATE, S.L.U. reserves the right to check all the data you provide, as well
                                        as verify compliance with the obligations set by the different legislation applicable
                                        in the different sources available to HAYA REAL ESTATE, SL.U., as well as request
                                        information that credit demonstrations or statements made. </p>
                                    <p>You can exercise the rights of access, rectification, cancellation and opposition that
                                        are legally recognized by writing to Via de los Poblados, 3, Edificio Cristalia,
                                        28033 Madrid (Spain), or email
                                        <a href="mailto:protecciondedatos@haya.es">protecciondedatos@haya.es</a> indicating in the subject "PROTECTION OF DATA ", and
                                        in both cases attaching a copy of the ID or equivalent identification document.</p>
                                    <br>
                                    <br>
                                    <strong class=" blue">PORTAL ACCESS</strong>
                                    <br>

                                    <p>Registering in the purchasing Portal of HAYA REAL ESTATE (from here on referred to as
                                        HAYA) implies previous reading and acceptance of the following clauses. Without expressing
                                        full compliance with the clauses you will not be allowed to register. Every time
                                        you access and use the Portal, it is understood you agree, fully and unreservedly,
                                        with the contents of these Legal Notice. Also, being the main user of your company,
                                        you are obliged by accepting this Legal Notice to ensure compliance by all users
                                        registered in your company, fully exonerating HAYA from any responsibility of the
                                        damage those users may cause to your company or any other because of their interactions
                                        with the Portal.</p>


                                    <p>
                                        <strong class=" blue">CLAUSES</strong>
                                        <br />
                                        <strong class="green">1. Purpose of the HAYA purchasing Portal</strong>
                                        <br /> The HAYA purchasing portal is the means trough which HAYA communicates with its
                                        suppliers to request offers, documents or any other commercial information it deems
                                        fit. At the same time HAYA reserves the right to use the portal to communicate any
                                        information it considers to be of its interest.</p>

                                    <p>HAYA works both as a direct purchaser and as a purchasing broker for its clients, and
                                        shall act in their name during the purchasing process.</p>
                                    <p>Access or use of Portal and / or Services does not give the SUPPLIER any rights on trademarks,
                                        commercial denominations or any other distinctive sign appearing in the portal, being
                                        HAYA or third party’s property. Therefore, all contents are intellectual property
                                        of HAYA or third party, and will not be understood as ceded to the SUPPLIER.</p>


                                    <p>
                                        <strong class="green">2. Purpose</strong>
                                        <br> The purpose of this contract is to regulate relations between HAYA, HAYA’s client
                                        and the SUPPLIER, in all aspects concerning use of the PORTAL.</p>


                                    <p>
                                        <strong class="green">3. SUPPLIER obligations </strong>
                                        <br> The following are obligations of the SUPPLIER:
                                        <br /> a. Provide whatever data is necessary for the adequate functioning of the system,
                                        and maintain them updated, communicating at the earliest possible time any modifications.
                                        <br
                                        /> b. Guaranteeing the authenticity of data provided as a consequence of forms necessary
                                        to subscribe to the Services offered by the PORTAL. Also, the SUPPLIER will update
                                        all information provided to ensure it reflects, at all times, its real situation.
                                        Therefore, the SUPPLIER is solely responsible of damages caused to HAYAor HAYA’s
                                        client as a consequence of inexact or false statements.
                                        <br /> c. Keeping absolute confidentiality in relation to all information derived from
                                        the relations between the SUPPLIER and HAYA or HAYA’s client.
                                        <br /> d. Abstaining from doing any modification in the information-technology use of the
                                        Portal, as well as using it with purposes different from those for which it is intended.
                                        Also, the SUPPLIER will abstain from accessing unauthorized zones of the Portal.
                                        <br
                                        /> e. Comply loyally to the commitments expressed in the information sent through the
                                        portal. If the SUPPLIER does not demonstrate due commercial diligence, or does not
                                        comply with contracted duties, HAYA and its clients reserves the right to exclude
                                        temporarily or permanently the SUPPLIER from the Portal.
                                        <br /> f. The SUPPLIER will indicate only those material groups referring to goods or services
                                        that, at the time of acceptance of the contract, are commercialized, manufactured
                                        or distributed by the SUPPLIER and of commercial interest to HAYA and its client.
                                        <br /> g. The SUPPLIER accepts that offers sent through the portal are considered to be
                                        of the same rank and validity as offers sent through any other traditional means
                                        (letters, fax).
                                        <br /> h. The SUPPLIER is obliged to use the Portal and its Services in compliance with
                                        the Law, this Legal Notice, and the rest of regulations and instructions of which
                                        it is informed, as well as in accordance to generally accepted morals and customs,
                                        and public order.
                                        <br /> Thus, the SUPPLIER will abstain from using the Portal or any of its Services with
                                        illegal purposes, prohibited in this Legal Notice, and/or damaging to rights and
                                        interests of third parties.
                                        <br /> In particular, and merely as a matter of indication, the SUPPLIER agrees not to
                                        transmit, broadcast, or make available to others or third parties any information,
                                        data, contents, graphics, image and/or sound files, photographs, recordings, software
                                        and, in general, any type of material that:

                                    </p>
                                    <p>1. Is contrary to fundamental rights and public liberties under the Constitution, International
                                        treaties and applicable laws.</p>
                                    <p>2. Induces, incites or promotes criminal acts, contrary to law, moral and generally accepted
                                        customs or public order. </p>
                                    <p>3. Is false, inexact or may induce to error, or constitutes illegal, deceitful or disloyal
                                        publicity.</p>
                                    <p>4. Is protected under intellectual or industrial property rights belonging to third parties
                                        without prior explicit and written consent.</p>
                                    <p>5. Contravenes laws and statutes for civil protection against defamation and invasion
                                        of privacy.</p>
                                    <p>6. Contravenes regulations about privacy and/or secrecy of communications.</p>
                                    <p>7. Can be considered as unfair competition or damages in any way the image of HAYA or
                                        third parties. </p>
                                    <p>8. Is affected by viruses or similar elements that can harm or stop the Portal’s adequate
                                        performance, the electronic equipment or their files and documents.</p>


                                    <p>
                                        <strong class="green">4. Rights of the SUPPLIER </strong>
                                        <br> The following are rights of the SUPPLIER:
                                        <br /> 1. Maintain a constant presence in HAYA’s database, as a registered supplier.
                                        <br
                                        /> 2. Receive offers according to the established rules.
                                        <br />
                                    </p>


                                    <p>
                                        <strong class="green">5. Obligations of HAYA </strong>
                                        <br> The following are HAYA obligations:
                                        <br /> a. Maintain the information it deems fit constantly updated, without being liable
                                        or responsible of any mistakes that may happen by chance or due to circumstances
                                        beyond its control.
                                        <br /> b. Keep absolute confidentiality of all information concerning the SUPPLIER, both
                                        if supplied directly or generated as a consequence of relationships between the SUPPLIER
                                        and HAYA.
                                        <br /> c. Provide the SUPPLIER, at any time, the situation of its data inside the database,
                                        and not to provide it to any third parties except to allow the SUPPLIER to present
                                        offers to be taken into consideration as supply alternatives. Also, to provide the
                                        SUPPLIER, at any time, the situation of its data inside the database, so that the
                                        SUPPLIER, with previous written notification, modifies or deletes them.
                                        <br />
                                    </p>



                                    <p>
                                        <strong class="green">6. Rights of HAYA and its clients </strong>
                                        <br> a. The SUPPLIER gives its explicit consent for HAYA to communicate information concerning
                                        the SUPPLIER to associated companies or companies belonging to the same group, as
                                        well as others with which it may sign agreements with the only aim of providing the
                                        best services, in compliance, in any case, with Spanish legislation on personal data
                                        protection. Also, the SUPPLIER agrees on HAYA or its associates, subsidiaries or
                                        affiliates, or investee companies, sending information about goods or services they
                                        commercialize, directly or indirectly, or that will be commercialized.
                                        <br />
                                        <br /> b. Choose, in case of infringement by the SUPPLIER of its obligations, between demanding
                                        full compliance with its obligations or cancellation of the contract, in the terms
                                        expressed in clause 8 of this Legal Notice.
                                        <br />
                                    </p>


                                    <p>
                                        <strong class="green">7. Limitation of responsibility </strong>
                                        <br> a. HAYA shall not be liable or responsible for the availability of the service,
                                        network or technical failures that might cause an interruption or cuts in the Portal.
                                        <br>
                                        <br> Equally, HAYA shall not be liable of any damages caused by the spreading of IT viruses
                                        or other elements in the system. b. HAYA shall not be liable or responsible of any
                                        payments and /or complaints that derive from the acts of clients concerning the agreement
                                        between the client(s) and the SUPPLIER.
                                        <br>
                                        <br> c. HAYA shall not be liable or responsible of any infringement of the Laws or generally
                                        agreed Customs, or of acts that contravene obligations established in this Legal
                                        Notice, by any employee of HAYA, by the SUPPLIER, or by any Internet or Network user.
                                        <br>
                                        <br> However, were HAYA or its clients to have knowledge of any conduct the previous
                                        paragraph refers to, it shall adopt the necessary measures to resolve with the upmost
                                        diligence and urgency the conflicts caused.
                                        <br>
                                        <br> d. HAYA shall not be held liable or responsible of any interference by unauthorized
                                        third parties in the knowledge of the conditions and circumstances of use that the
                                        SUPPLIER shall do of the Portal and its Services.
                                    </p>

                                    <p>
                                        <strong class="green">8. Contract cancellation </strong>
                                        <br> In the case of failure to comply with the obligations contained in this Legal Notice,
                                        the SUPPLIER or HAYA and its clients must notify the breach to the infringing party,
                                        with a fifteen working day period to repair it. After that period has passed by and,
                                        should the infringement not be repaired, the other party can choose between the fulfilling
                                        and cancellation of this agreement. Should it choose to cancel the contract, both
                                        the SUPPLIER and HAYA or its clients agree that simple notification shall be enough
                                        for the cancellation to have full effect.
                                        <br />
                                    </p>

                                    <p>
                                        <strong class="green">9. Duration </strong>
                                        <br> The present agreement shall have indefinite duration as long as no party express
                                        willingness to cancel the contract with a prior notice not shorter than a month.
                                        <br> The proceeding to cancel the contract will be to send an e-mail indicating “PORTAL
                                        UNSUSCRIBE” to the address
                                        <a href="mailto:soporteproveedores@haya.es" title="soporteproveedores@haya.es"
                                            class="blue">soporteproveedores@haya.es</a>, indicating the basic data to identify the SUPPLIER.</p>

                                    <p>
                                        <strong class="green">10. Notifications </strong>
                                        <br> Notifications between the parties may be carried out through any of the means admitted
                                        by the Law, that allows having a record of reception, including fax.
                                        <br />
                                    </p>


                                </div>
                                <br />
                                <p class="imprimir">
                                    <a href="registro_texto.html" class="" target="_blank">Imprimir</a>
                                </p>
                                <br />
                                <br />
                                <div style="clear: both;margin:30px 0;"></div>

                                <div class="caja_registro">
                                    ...
                                </div>
                                <br />
                                <p class="imprimir">
                                    <a href="politica_texto.html" class="" target="_blank">Imprimir</a>
                                </p>


                                <!--        -->
                                <div class="caja_acepto">
                                    <form name="frmAlta" id="frmAlta" method="post" style="max-width:1000px;">
                                        <span id="sprycheckbox1">

                                            <span class="checkboxRequiredMsg">Seleccione para continuar.</span>
                                        </span>
                                        <br>
                                        <br>

                                        <input type="checkbox" name="opcion1" id="opcion1" required>

                                        <span class="black">I have read and accept the General terms and conditions of use of the Supplier Portal</span>
                                        </label>


                                        <br />

                                        <p style="text-align: center">
                                            <button type="button" class="bt_registro" id="submit1" name="submit1">Continue</button>
                                        </p>
                                        <br>
                                        <br />
                                        <br>
                                    </form>


                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                    <script type="text/javascript">
                        // <!--
                        // var sprycheckbox1 = new Spry.Widget.ValidationCheckbox("sprycheckbox1");
                        // //-->
                        $('#submit1').on('click', function () {
                            if ($('#opcion1').is(':checked')) {
                                $('#frmAlta').submit();
                                alta();
                            }
                            else
                                alert("You must accept the general terms and conditions of use of the Portal");

                        });

                    </script>
                </div>
            </div>



            </div>

            </div>

        </section>
        <footer id="footer">

            <div class="container_haya">

                <div class="footer-menu">

                    <ul>
                        <li>
                            <a href="<%=application(" RUTASEGURA ")%>custom/<%=application("NOMPORTAL
                                ")%>/public/eng/ayuda.html" target="_blank">FAQ</a>
                        </li>
                        <li>
                            <a href="<%=application(" RUTASEGURA ")%>custom/<%=application("NOMPORTAL
                                ")%>/public/eng/aviso-legal.html" target="_blank">Legal notice</a>
                        </li>
                        <li>
                            <a href="<%=application(" RUTASEGURA ")%>script/politicacookies/politicacookies.asp?Idioma=ENG">Cookies Policy</a>
                        </li>
                    </ul>

                    <p>Web optimized for 1280 x 1024. Supported browsers: Internet Explorer, Google Chrome and Mozilla Firefox
                        <br
                        />© FULLSTEP All rights reserved.</p>

                    <a href="http://www.fullstep.com" target="_blank">
                        <img class="fullstep" src="../img/fullstep.png" />
                    </a>

                </div>



            </div>

        </footer>
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/main.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    </body>

    <!--#include file="../../../common/fsal_2.asp"-->

    </html>