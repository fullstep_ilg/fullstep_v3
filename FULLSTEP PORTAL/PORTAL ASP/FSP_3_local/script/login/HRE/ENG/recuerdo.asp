<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/responsive.css" rel="stylesheet" type="text/css">
<!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400' rel='stylesheet' type='text/css'>-->
        
        <!--[if lt IE 9]>
          	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body class="popup">
 <header id="heading">             
            
      		<div class="container_haya">  
            
           		<div class="text"> 
            
                  <div class="logotipo">
           			<a href="http://www.haya.es" title="Haya"><img src="../img/logo_haya.png" alt="Haya" width="160" height="156" /></a>
                  </div> 
                    <div class="cierra-popup"><img src="../img/cerrar.gif" alt="Cerrar" class="cerrar"></div>                  
                                
       		  </div>   
                    
           	</div>
            
        </header>        
    			
        
        
    	<section id="ayuda"> 
            
            <div class="container_haya">
            
           		<div class="text_password blue">
                
                	<div class="columna">
                        <div class="imagen">
                        <img src="../img/imagen_password.jpg" alt="imagen_password" width="198" height="396">
                         </div>  
               	  </div>
                    
                    <div class="centro">
                    
                    	<h2>LOGIN INFORMATION</h2>
                    
                    	<p>If you forgot your password, please fill in your Company Code, User and email and click on "Submit". You will receive a link to a page to create a new password.</p>
                         
                        <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
                            <label>
                                <input class="recordar" type="text" name="txtCia" id="txtCia" placeholder="Código de Compañía" />
                            </label>
                            <label>
                                <input class="recordar"  type="text" name="txtUsu" id="txtUsu" placeholder="Usuario" />
                            </label>
                            <label>
                                <input class="recordar"  type="text" name="txtEmail" id="txtEmail" placeholder="E-mail" />
                            </label>
                            <button class="btn recordar" name="cmdEnviar" type="submit">Submit</button>
                             <input type="hidden" name="idioma" value="eng"/>
                        </form>
                   
                            
                             
               		</div>
                     <div class="columna-dcha-Haya"> <div class="capa-cierre-recup">
                    <div class="imagen-capa-cierre"><img src="../img/telefonista.jpg"></div>
                    <div class="texto-capa-cierre">Si continúa con dificultades para acceder al Portal o su dirección de email ha cambiado, consulte con <strong>servicio de atención a proveedores</strong>.
                      <strong class="telefono">902 039 818</strong></div>
                    
                    </div>
                    </div>
                    
               	</div>
                    
         	</div>
              
    	</section>
        
<script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/main.js"></script>        
    
</body>

</html>
