﻿<%@ Language=VBScript %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/idioma.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://fast.fonts.net/jsapi/f5dbd368-355c-4f28-bc2f-217b51498d80.js"></script>
<script src="http://code.jquery.com/jquery-1.9.0.js"></script>
<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->

<script src="../../common/formatos.js"></script>
<script language="JavaScript" src="../../common/ajax.js"></script>
<!--#include file="../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
</head>

<%
''' <summary>
''' Mostrar el detalle del registro
''' </summary>
''' <remarks>Llamada desde: login/login.asp		login/codigo instalaci�n/registro.asp	login/codigo instalaci�n/codigo idioma/registro.asp; Tiempo m�ximo: 0</remarks>
%>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=SPA", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body onload="init()">

<div id="cont_gen">
	<div id="izd_gen"> <a href="http://www.mahou-sanmiguel.com" target="_blank"><img src="images/logo_gen.jpg" alt="Mahou San Miguel" /></a>
    </div>
    <div id="drc_gen">
    	<h1>SOLICITAR REGISTRO</h1>
        <div class="int">
        	<p> 
                Para continuar con el proceso de alta es imprescindible la aceptación de las
                <span class="rojo">CONDICIONES DE USO DEL PORTAL DE PROVEEDORES</span>.
            </p>
            <p style="height:150px; padding:0 5px; overflow:auto; background:#FFF; border:#CCC 1px solid">
               CONDICIONES GENERALES DE USO DEL PORTAL DE PROVEEDORES DE MAHOU - SAN MIGUEL<br />
1.	INFORMACIÓN GENERAL <br />
2.	CONDICIONES GENERALES DE USO DEL PORTAL DE PROVEEDORES<br />
3.	MENORES DE EDAD <br />
4.	RESPONSABILIDAD DE LOS USUARIOS POR EL USO DEL PORTAL DE PROVEEDORES <br />
5.	IDENTIFICADORES DE USUARIO Y CONTRASEÑA <br />
6.	CONDICIONES PARTICULARES<br />
7.	PROPIEDAD INTELECTUAL E INDUSTRIAL <br />
8.	INFORMACIÓN Y ELEMENTOS DEL PORTAL DE PROVEEDORES<br />
9.	ENLACES O HIPERENLACES AL PORTAL DE PROVEEDORES <br />
10.	USO DE "COOKIES" <br />
11.	GARANTÍAS <br />
12.	RESPONSABILIDADES <br />
13.	GENERAL <br />
I.	CONDICIONES GENERALES DE USO DEL PORTAL DE PROVEEDORES<br />
1.	INFORMACIÓN GENERAL <br />
Los términos y condiciones que se indican a continuación (en adelante, “Condiciones Generales”), regulan el acceso, registro, navegación, descarga y el uso de todas y cada una de las páginas web y funcionalidades (pedidos, petición de oferta, etc.) localizables o accesibles a través del dominio www.Mahou-SanMiguel.com, y sus respectivos subdominios y subdirectorios implementados por Mahou-San Miguel para la relación entre sus proveedores y cada una de las siguientes empresas: MAHOU, S.A., SAN MIGUEL FÁBRICAS DE CERVEZA Y MALTA, S.A., CERVEZAS ANAGA, S.A. MSM GRUPO COMERCIALIZADOR, S.A., TRANSPORTES AGRO INDUSTRIALES, S.A., AGUAS DE SOLÁN DE CABRAS, S.A., CERVEZAS ALHAMBRA, S.L.U., ALHAMBRA DISTRIBUIDORA MERIDIONAL, S.L., ANDALUZA DE CERVEZAS Y BEBIDAS, S.L. y PENIBÉTICA DE CERVEZAS Y BEBIDAS, S.L. <br />En cumplimiento de lo dispuesto en el artículo 10 de la Ley 34/2002, de 11 de julio, de servicios de la sociedad de la información y de comercio electrónico, las sociedades arriba enumeradas (en adelante, la “COMPAÑÍA”) pone a disposición de sus usuarios la siguiente información registral del titular del sitio web:<br />
Denominación social: Mahou, S.A.<br />
Número de identificación Fiscal: A28078202<br />
Datos Mercantiles: Inscrita en el Registro Mercantil de Madrid, en el Tomo 54, folio 225, Sección 8ª, Hoja M-1076.<br />
Domicilio social: calle Titán, 15, 28045 de Madrid.<br />
Contacto: pueden ponerse en contacto con la COMPAÑÍA mediante correo ordinario a la dirección del domicilio social, o a través de la siguiente dirección de correo electrónico: info@mahou-sanmiguel.info.<br />
POR EL HECHO DE ACCEDER, VER O UTILIZAR LOS MATERIALES O SERVICIOS ACCESIBLES EN O A TRAVÉS DEL “PORTAL DE PROVEEDORES” U OTRAS PÁGINAS WEB O APLICACIONES INFORMÁTICAS, EL USUARIO PONE DE MANIFIESTO QUE COMPRENDE Y ACEPTA ESTAS “CONDICIONES GENERALES” COMO EQUIVALENTE LEGAL DE UN DOCUMENTO FIRMADO POR ESCRITO Y VINCULANTE.<br />
2.	CONDICIONES GENERALES DE USO DEL “PORTAL DE PROVEEDORES”<br />
Las presentes Condiciones Generales regulan el uso del sitio web que la COMPAÑÍA pone a disposición de su red de proveedores a través del Portal de Proveedores, con el fin de proporcionarles una herramienta ágil y eficaz a través de la cual puedan solicitarles ofertas, documentos u otra información comercial que la COMPAÑÍA estime oportuno, o remitirle aquella información que considere de su interés. Asimismo, el Portal de Proveedores le permitirá gestionar sus pedidos, realizar consultas técnicas, remitir solicitudes, consultar su cuenta de usuario, etc.<br /> 
El acceso y el uso del Portal de Proveedores de la COMPAÑÍA quedará sujeto a las presentes Condiciones Generales. La utilización del Portal de Proveedores, titularidad de la COMPAÑÍA, atribuye la condición de usuario y supone la aceptación plena y sin reservas por el usuario de todas las Condiciones vigentes en cada momento en que el usuario acceda al mismo.<br />
La COMPAÑÍA se reserva el derecho a modificar en cualquier momento las presentes Condiciones Generales. Cualquier cambio que se implemente en estas Condiciones Generales que pueda afectar a los derechos del usuario será comunicado en el Portal de Proveedores de su titularidad durante la primera semana posterior a la implementación de dicha modificación. Además de esta notificación que le informará a usted respecto de dicha modificación, le sugerimos que revise estas Condiciones Generales frecuentemente para estar al corriente de su alcance y de cualquier modificación que se haya llevado a cabo. Al acceder al Portal de Proveedores titularidad de la COMPAÑÍA con posterioridad a la publicación del aviso de dichas modificaciones, alteraciones o actualizaciones, estará aceptando cumplir con los nuevos términos. El usuario es consciente de que el acceso y utilización del Portal de Proveedores se realiza bajo su única y exclusiva responsabilidad. <br />
Algunos servicios de las páginas web de la COMPAÑÍA pueden estar sometidos a condiciones particulares, reglamentos e instrucciones que, en su caso, sustituyen, completan y/o modifican los presentes términos y condiciones (en adelante, “Condiciones Particulares”), y que deberán ser aceptados por el usuario antes de utilizar el Portal de Proveedores. La prestación del servicio a solicitud del usuario a través del Portal de Proveedores, implica la aceptación expresa de las Condiciones Particulares que fueran de aplicación.<br />
Las presentes Condiciones Generales y las Condiciones Particulares son documentos independientes a las condiciones generales o particulares aplicables a las compras o servicios en particular, que puedan ser aplicables en cada caso en función del tipo de prestación contratada por la COMPAÑÍA.<br />
Dentro de la expresión "Portal de Proveedores” se comprenden, con carácter enunciativo pero no limitativo todos los contenidos, los datos, gráficos, textos, logos, marcas, software, imágenes, animaciones, creaciones musicales, vídeos, sonidos, dibujos, fotografías, expresiones e informaciones y otros incluidos en el mismo, y, en general, todas las creaciones expresadas por cualquier medio o soporte, tangible o intangible con independencia de que sean susceptibles o no de propiedad intelectual de acuerdo al Texto Refundido de la Ley de Propiedad Intelectual española.<br />
El usuario utilizará los servicios y materiales disponibles en el Portal de Proveedores exclusivamente para fines profesionales, en el ámbito de su actividad como proveedor de la COMPAÑÍA, con exclusión de cualquier modalidad de utilización distinta a ésta. Las presentes Condiciones Generales, así como las Condiciones Particulares que puedan aplicarse previo aviso en casos concretos, son aceptadas expresamente y sin reservas por el usuario por el mero hecho de acceder al Portal de Proveedores, utilizando de cualquier forma los materiales y servicios del Portal de Proveedores.<br />
Cuando el acceso y la utilización de ciertos materiales y/o servicios del Portal de Proveedores se encuentre sometido a Condiciones Particulares, estas Condiciones Particulares serán previamente puestas en conocimiento del usuario, y según los casos, sustituirán, completarán y/o modificarán las Condiciones Generales aquí recogidas. El acceso y la utilización de dichos materiales o servicios sujetos a Condiciones Particulares implicará, por consiguiente, la plena adhesión a las Condiciones Particulares que los regula en la versión publicada en el momento que el usuario acceda a los mismos, quedando dichas Condiciones Particulares incorporadas automáticamente a las presentes Condiciones Generales.<br />
En caso de contradicción entre los términos y condiciones manifestados en estas Condiciones Generales y las Condiciones Particulares, prevalecerán siempre y en todo caso, los términos acordados en las Condiciones Particulares aunque únicamente en lo que respecta a las disposiciones incompatibles y tan sólo respecto de aquellos materiales o servicios del Portal de Proveedores sometidos a dicha específica regulación.<br />
La COMPAÑÍA podrá modificar de forma unilateral y sin aviso previo, la prestación, configuración, contenido y servicios del Portal de Proveedores, así como sus Condiciones Generales. Si estas Condiciones Generales fueran sustituidas por otras en todo o en parte, dichas nuevas condiciones generales o en su caso las particulares, se entenderán aceptadas de forma idéntica a la expuesta. No obstante, el usuario del Portal de Proveedores deberá acceder a estas Condiciones Generales y a las Condiciones Particulares de los servicios del Portal de Proveedores que utilice, de forma periódica para tener conocimiento de la actualización de las mismas, si la hubiera.<br />
En caso de que el usuario no acepte estas Condiciones Generales o, las Condiciones Particulares, el usuario deberá abstenerse de acceder al Portal de Proveedores o, en caso de haber accedido, abandonarlos.<br />
El usuario deberá establecer las medidas de seguridad de carácter técnico adecuadas para evitar acciones no deseadas en su sistema de información, archivos y equipos informáticos empleados para acceder a Internet y, en especial, al Portal de Proveedores, siendo consciente de que Internet no es un medio totalmente seguro.<br />
Con carácter general, los servicios y materiales ofrecidos a través del Portal de Proveedores estarán disponibles en español, pudiendo la COMPAÑÍA discrecionalmente presentar dichos servicios y materiales adicionalmente en otros idiomas.<br />
El coste del acceso telefónico u otro tipo de gasto necesarios para acceder al Portal de Proveedores correrá exclusivamente a cargo del usuario. <br />
3.	MENORES DE EDAD.  <br />
Queda prohibido el acceso, registro, navegación, utilización, alojamiento y/o descarga de materiales y/o uso de los servicios del Portal de Proveedores de la COMPAÑÍA a cualquier persona física o jurídica que no sea potencial o actual proveedor de Mahou-San Miguel y, en particular, queda expresamente prohibido el acceso a menores de edad (menores de 18 años). <br />
4.	RESPONSABILIDAD DE LOS USUARIOS POR USO DEL PORTAL DE PROVEEDORES. <br />
El usuario no podrá en ningún caso modificar o suprimir los datos identificativos que existan de la COMPAÑÍA. El usuario únicamente podrá acceder a los servicios y materiales del Portal de Proveedores mediante los medios o procedimientos que se hayan puesto a su disposición a este efecto en el propio Portal de Proveedores, o que se utilicen habitualmente en Internet para ese fin, siempre que no impliquen violación de derechos de Propiedad Intelectual/Industrial, o algún tipo de daño para el Portal de Proveedores, o a su información o a los servicios ofrecidos.<br />
El usuario se obliga a utilizar los servicios, la información y los materiales del Portal de Proveedores de la COMPAÑÍA de conformidad con la Ley y con estas Condiciones Generales. En ningún caso la utilización del Portal de Proveedores por el usuario podrá atentar contra la legislación vigente, la moral, las buenas costumbres y el orden público, y deberá en todo momento hacer un uso correcto y lícito de los servicios, información y materiales del Portal de Proveedores.<br />
Asimismo, el usuario garantiza que dispone de la capacidad de representación necesaria para actuar frente a la COMPAÑÍA en nombre de la sociedad a la que representa, así como para asumir derechos y obligaciones en nombre de tal sociedad, debiendo mantener indemne y eximir de responsabilidad a la COMPAÑÍA por cualesquiera daños y perjuicios que hubiera podido causar la anulación o resolución de los actos que se hubieran realizado a través del Portal de Proveedores.<br />
El usuario deberá:<br />
o	Proporcionar cuantos datos sean necesarios para el registro en el Portal de Proveedores y el funcionamiento adecuado del mismo, así como mantenerlos actualizados, comunicando a la mayor brevedad posible todo cambio que se produzca en los mismos.<br />
o	Garantizar la autenticidad de los datos facilitados a consecuencia de la cumplimentación de los formularios necesarios para la suscripción de los servicios. Asimismo, actualizará la información facilitada para que refleje, en cada momento, su situación real. Por todo ello, el usuario y el proveedor al que representa serán los únicos responsables de los daños y perjuicios ocasionados a la COMPAÑÍA como consecuencia de declaraciones inexactas o falsas.<br />
o	Cumplir sus compromisos en la información remitida a través del Portal de Proveedores. En el caso de que el usuario o el proveedor al que representa no demuestren la necesaria diligencia comercial, o incumplan las obligaciones contraídas, la COMPAÑÍA se reserva el derecho de excluir temporal o permanentemente a los mismos del Portal de Proveedores.<br />
o	Aceptar las Condiciones Particulares respecto de determinados servicios, según se determine en estas Condiciones Generales o, en su caso, en las Condiciones Particulares de dichos servicios.<br />
o	Utilizar los servicios y materiales del Portal de Proveedores para uso exclusivamente profesional, en el ámbito de su actividad como proveedor de la COMPAÑÍA. <br />
o	Garantizar que las ofertas introducidas en el Portal de Proveedores serán consideradas con el mismo rango y validez que una oferta enviada por medios tradicionales.
o	Hacer un uso correcto y lícito del Portal de Proveedores, de conformidad con la legislación vigente, la moral, las buenas costumbres y el orden público.<br /> 
El usuario en ningún caso podrá realizar las siguientes actividades:<br />
a)	Difundir contenidos o propaganda de carácter racista, xenófobo, pornográfico, obsceno, denigratorio o que incite o promueva la realización de actos delictivos, violentos difamatorios o degradantes por razón de edad, sexo, religión o creencias; o que haga, promueva o incite directa o indirectamente a la apología del terrorismo o que sea contraria a los derechos humanos y los derechos fundamentales y libertades de terceros, a la legislación vigente, la moral, las buenas costumbres y el orden público, o con fines lesivos que puedan perjudicar, dañar o impedir de cualquier forma, el acceso a los mismos, en perjuicio de la COMPAÑÍA o de terceros. <br />
b)	Realizar actos contrarios a los derechos de Propiedad Intelectual y/o Industrial de sus legítimos titulares. <br />
c)	Provocar daños en los sistemas informáticos de la COMPAÑÍA, de sus proveedores o de terceros y/o introducir o difundir virus informáticos, código o software nocivo u otro tipo de sistemas que puedan causar daños o alteraciones en sistemas informáticos, o alteraciones no autorizadas de los contenidos, programas o sistemas accesibles a través de los materiales o servicios del Portal de Proveedores, o en los sistemas de información, archivos y equipos informáticos de los usuarios de los mismos, o el acceso no autorizado a cualesquiera materiales y servicios del Portal de Proveedores. <br />
d)	Transmitir publicidad a través de cualquier medio y, en especial, a través del envío de mensajes electrónicos, cuando el envío de la publicidad no haya sido solicitado o autorizado por el receptor. <br />
e)	Utilizar el Portal de Proveedores, total o parcialmente, para promocionar, vender, contratar, divulgar publicidad o información propia o de terceras personas sin autorización previa y por escrito de la COMPAÑÍA, distintos de los productos o servicios ofrecidos por el proveedor a la COMPAÑÍA, o incluir hipervínculos en sus páginas web particulares o comerciales al Portal de Proveedores, salvo autorización expresa de la COMPAÑÍA.<br />
f)	Utilizar los servicios y materiales ofrecidos a través del Portal de Proveedores de forma contraria a las Condiciones Generales y/o las Condiciones Particulares que regulen el uso de un determinado servicio y/o contenido, y en perjuicio o con menoscabo de los derechos del resto de usuarios. <br />
g)	Eliminar o modificar de cualquier modo los dispositivos de protección o identificación de la COMPAÑÍA o sus legítimos titulares que puedan contener el Portal de Proveedores, o los símbolos, logos o marcas que la COMPAÑÍA o los terceros legítimos titulares de los derechos incorporen a sus creaciones y que puedan ser objeto de propiedad intelectual o industrial. <br />
h)	Incluir, sin autorización previa y por escrito de la COMPAÑÍA, en páginas web de responsabilidad o propiedad del usuario o de terceros no autorizados "metatags" correspondientes a marcas, logos, nombres comerciales o signos distintivos propiedad de la COMPAÑÍA o utilizar marcas, logos, nombres comerciales, o cualquier otro signo identificativo que se encuentre sujeto a derechos de propiedad intelectual o industrial, sin la previa autorización expresa y por escrito de su legítimo titular. <br />
i)	Reproducir total o parcialmente, copiar, distribuir, alquilar, trasformar o permitir el acceso del público, a través de cualquier modalidad de comunicación pública, los materiales e información del Portal de Proveedores, o incluirlos en otro sitio web distinto sin autorización previa y por escrito de la COMPAÑÍA.<br /> 
j)	Incluir en un sitio web de su responsabilidad o propiedad un hiperenlace que genere una ventana o sesión del software de navegación empleado por un usuario de su sitio web, en que se incluyan marcas, nombres comerciales o signos distintivos de su propiedad y a través del cual se muestre el Portal de Proveedores. <br />
El Usuario responderá frente a la COMPAÑÍA, o frente a terceros, de cualesquiera daños y perjuicios de cualquier clase que pudieran causarse como consecuencia del incumplimiento o inobservancia, directa o indirecta, de las presentes Condiciones Generales. La COMPAÑÍA velará en todo momento por el respeto al ordenamiento jurídico vigente, y se reserva el derecho a denegar discrecionalmente total o parcialmente, en cualquier momento y sin necesidad de preaviso, el acceso de cualquier usuario al Portal de Proveedores, cuando concurra una o varias circunstancias descritas en la presente cláusula.<br />
5.	IDENTIFICADORES DE USUARIO Y CONTRASEÑA <br />
La COMPAÑÍA solicitará el registro de los usuarios para el acceso a ciertos servicios o información del Portal de Proveedores, a través de la elección de una contraseña que permita la identificación personal del usuario ("nombre" y "contraseña"). <br />
Las claves de acceso asignadas serán personales e intransferibles, no estando permitida la cesión, ni siquiera temporal, a terceros. En tal sentido, el usuario se compromete a hacer un uso diligente y a mantener en secreto las contraseña/s y nombre/s de usuario/s asignado/s, en su caso, para acceder al Portal de Proveedores. En el supuesto de que el usuario conozca o sospeche de la pérdida, robo o uso de su contraseña por terceros, deberá poner tal circunstancia en conocimiento de la COMPAÑÍA a la mayor brevedad posible.<br />
El usuario será responsable de los gastos y de los daños y perjuicios ocasionados por el acceso y utilización del Portal de Proveedores por cualquier tercero que emplee al efecto la/s contraseña/s y nombre/s del usuario debido a una utilización no diligente o a la pérdida de los mismos por el usuario.<br />
6.	CONDICIONES PARTICULARES<br />
Las presentes Condiciones de uso del Portal se aplican a las órdenes de pedido realizadas por los proveedores a través del Portal de Proveedores, siendo de aplicación en todo lo no regulado en las mismas los contratos firmados entre la COMPAÑÍA y el proveedor correspondiente. <br />
Como prueba del conocimiento y aceptación de las Condiciones Generales de Uso del presente servicio, el proveedor, deberá manifestar su conformidad con la versión aplicable al Portal de Proveedores en ese momento. Si durante la vigencia de los contratos entre la COMPAÑÍA y el proveedor se realizasen modificaciones a las presentes Condiciones Generales que afecten de manera relevante a los derechos y obligaciones de las partes, las nuevas condiciones serán comunicadas con antelación al proveedor, entendiéndose automáticamente aceptadas las nuevas condiciones por el proveedor si, transcurrido el plazo de diez días desde el envío de la modificación la COMPAÑÍA no recibe oposición por escrito del proveedor o si el proveedor realiza un uso del Portal aceptando las referidas modificaciones. La no aceptación de las modificaciones propuestas permitirá a la COMPAÑÍA resolver automáticamente el acuerdo con el proveedor para el uso por su parte del Portal, pudiendo éste seguir realizando los pedidos por el resto de los canales habituales. <br />
Los trámites de utilización del servicio son aquellos que se describen en las presentes Condiciones Generales, así como aquellos otros específicos que se indiquen en pantalla durante la navegación, de manera que el usuario declara conocer y aceptar dichos trámites como necesarios para acceder al servicio. Tal y como establecen los artículos 27 y 28 de la Ley 34/2002 de Servicios de la Sociedad de la Información y Comercio Electrónico, el proveedor y la COMPAÑÍA acuerdan la no aplicación de las obligaciones de información previa y posterior de los citados preceptos por considerar suficiente la documentación recogida al respecto en los contratos entre la COMPAÑÍA y el proveedor y en el Portal de Proveedores.<br />
Para la utilización del servicio, los usuarios deberán seguir las instrucciones que aparezcan en pantalla y que se requerirán.<br />
En caso de incumplimiento de cualquiera de las obligaciones contenidas en las presentes Condiciones Generales y Condiciones Particulares, el proveedor o la COMPAÑÍA deberán notificar dicho incumplimiento a la parte que lo hubiera cometido, otorgándole un plazo de quince (15) días hábiles, contando desde la notificación, para que lo subsane. Transcurrido dicho plazo sin que se haya producido la subsanación del incumplimiento, la otra parte podrá optar por el cumplimiento o la resolución del presente acuerdo, con la indemnización de daños y perjuicios en ambos casos. En caso de optar por la resolución, tanto el proveedor como la COMPAÑÍA aceptan que la simple notificación será suficiente para que la misma tenga plenos efectos.<br />
El presente acuerdo tendrá una duración indefinida siempre y cuando no se produzca denuncia por escrito de cualquiera de las partes, con al menos un (1) mes de antelación. El procedimiento de baja se realizará solicitando a través del email soporte@proveedoresmahou-sanmiguel.com con asunto “Baja del portal”. En caso de no ser posible el envío del email, se enviará la solicitud por fax al 91 296 20 25 indicando los datos básicos para identificar al proveedor.<br />
7.	PROPIEDAD INTELECTUAL E INDUSTRIAL <br />
Todos los materiales e información del Portal de Proveedores de la COMPAÑÍA están sometidos a la normativa vigente sobre Propiedad Intelectual y/o Industrial. Los derechos sobre los materiales y demás elementos que se muestran en el Portal de Proveedores (incluyendo, a título meramente ejemplificativo y sin limitación, dibujos, textos, gráficos, fotografías, audio, video, software, signos distintivos, etc.) pertenecen a la COMPAÑÍA o, en su caso, a terceros que han consentido la cesión de los mismos a la COMPAÑÍA. Igualmente pertenecen a la COMPAÑÍA los logos, nombres comerciales, dominios y marcas de su titularidad. <br />
Los materiales e información que pudiere aportar el usuario al Portal de Proveedores de la COMPAÑÍA (fotografía, audio, video, etc.) deberán respetar los derechos de imagen y propiedad intelectual (de existir) de los mismos, siendo el usuario el único responsable de cualquier reclamación que pudiere existir frente a la COMPAÑÍA como consecuencia de la utilización y difusión de dichos materiales e información. <br />
El acceso, navegación, utilización, ubicación y/o descarga de materiales y/o uso de servicios del Portal de Proveedores por el usuario, en ningún caso se entenderá como una renuncia, transmisión, licencia o cesión total o parcial de los derechos antes indicados por parte de la COMPAÑÍA o, en su caso, por parte del titular de los derechos correspondientes.<br />
En consecuencia, no está permitido suprimir, eludir o manipular el aviso de derechos de autor ("copyright") y cualesquiera otros datos de identificación de los derechos de la COMPAÑÍA o de sus respectivos titulares incorporados a los contenidos y/o servicios, así como los dispositivos técnicos de protección o cualesquiera mecanismos de información y/o de identificación que pudieren contenerse en los mismos.<br />
En particular, queda terminantemente prohibida la utilización de cualesquiera materiales o elementos del Portal de Proveedores para su inclusión, total o parcial, en otros sitios web ajenos al Portal de Proveedores sin contar con la autorización previa y por escrito de los titulares del Portal de Proveedores. <br />
Las referencias a nombres y marcas comerciales o registradas, logotipos u otros signos distintivos, ya sean titularidad de la COMPAÑÍA o de terceras empresas, llevan implícitas la prohibición sobre su uso sin el consentimiento de la COMPAÑÍA o de sus legítimos propietarios. En ningún momento, salvo manifestación expresa, el acceso al o uso del Portal de Proveedores, confiere al usuario derecho alguno sobre las marcas, logotipos y/o signos distintivos allí incluidos y protegidos por Ley. <br />
Quedan reservados todos los derechos de propiedad intelectual e industrial y, en particular, queda prohibido modificar, fijar, copiar, reutilizar, explotar de cualquier forma, reproducir, transformar, doblar, subtitular, ceder, vender, alquilar, prestar, comunicar públicamente, hacer segundas o posteriores publicaciones, cargar archivos, enviar por correo, transmitir, usar, tratar o distribuir de cualquier forma la totalidad o parte de los contenidos, elementos, y productos en su caso, incluidos en el Portal de Proveedores para propósitos públicos o comerciales distintos del objeto de la prestación por parte del proveedor a la COMPAÑÍA, si no se cuenta con la autorización expresa y por escrito de la COMPAÑÍA o, en su caso, del correspondiente titular de los derechos.<br />
Si la actuación u omisión, culpable o negligente, directa o indirectamente imputable al usuario del Portal de Proveedores que origine la infracción de los derechos de propiedad intelectual e industrial de la COMPAÑÍA o de terceros, originase a la COMPAÑÍA daños, pérdidas, obligaciones solidarias, gastos de cualquier naturaleza, sanciones, medidas coercitivas, multas y otras cantidades surgidas o derivadas de cualquier reclamación, demanda, acción, pleito o procedimiento, ya sea civil, penal o administrativo, la COMPAÑÍA tendrá derecho a dirigirse contra dicho usuario por todos los medios legales a su alcance y reclamar cualesquiera cantidades indemnizatorias, daños morales o daños a la propia imagen, daño emergente y lucro cesante, costes publicitarios o de cualquier otra índole que pudieran resultar para su reparación, importes de sanciones o sentencias condenatorias, intereses de demora, el coste de la financiación de todas las cantidades en que pudiera resultar perjudicada la COMPAÑÍA, las costas judiciales y el importe de la defensa (incluidos procuradores y abogados) en cualquier proceso en el que pudiera resultar demandada la COMPAÑÍA por las causas anteriormente expuestas, por los daños y perjuicios ocasionados por razón de su actuación u omisión, sin perjuicio de ejercer cualesquiera otras acciones que en derecho le correspondan a la COMPAÑÍA.<br />
Las reclamaciones que pudieran interponerse por el usuario en relación con posibles incumplimientos de los derechos de propiedad intelectual o industrial en relación con el Portal de Proveedores deberán dirigirse al Departamento de Asesoría Jurídica de la COMPAÑÍA, sito en Calle Titán 15, 28045 de Madrid.<br />
8.	INFORMACIÓN Y ELEMENTOS DEL PORTAL DE PROVEEDORES<br />
a)	Información Corporativa de la COMPAÑÍA <br />
El usuario conoce y acepta que cualquier dato relativo a la COMPAÑÍA o a las empresas que integran dicho grupo empresarial, y cuya naturaleza sea económica, financiera y/o estratégica (en adelante, "Información Corporativa") se lleva a cabo con fines meramente informativos.<br />
La Información Corporativa se ha obtenido de fuentes fiables, pero, pese a haber tomado medidas razonables para asegurarse de que dicha información es veraz, real y puede mostrar los resultados empresariales de la COMPAÑÍA, este no manifiesta ni garantiza que sea exacta, completa o actualizada, y no debe confiarse en la misma en términos absolutos. <br />
La Información Corporativa que se puede encontrar en el Portal de Proveedores no supone ningún tipo de recomendación de inversión ni puede ser considerado como tal, ni asesoramiento financiero ni de otra clase, y nada de lo que en dicha información se incluye debe ser tomado como base para realizar inversiones o tomar decisiones societario-mercantiles de ningún tipo. <br />
b) Información suministrada o publicada por usuarios y/o terceros <br />
El Portal de Proveedores puede incluir información o contenidos facilitados por otras fuentes distintas de la COMPAÑÍA, incluyendo información proporcionada por los propios usuarios del Portal de Proveedores. La COMPAÑÍA no garantiza, ni asume responsabilidad alguna sobre la certeza, integridad o exactitud de tal información y/o contenidos, incluyendo los supuestos expuestos en el apartado relativo a la "Responsabilidad de los Usuarios por uso y contenido".<br />
El usuario no podrá introducir, almacenar o difundir mediante el Portal de Proveedores, cualquier contenido o material que infrinja derechos de propiedad intelectual o industrial, ni en general ningún contenido respecto del cual no ostenten, de conformidad con la ley, el derecho a reproducirlo, distribuirlo, adaptarlo, copiarlo, fijarlo o ponerlo a disposición de terceros. Se entiende por información, elementos o contenidos recibidos por el usuario en el Portal de Proveedores, aquellos recibidos por cualquier medio, ya sean comentarios, sugerencias o ideas, incluso aquellos que contengan vídeos, textos, fotografías, imágenes, audio, software, etc. Dichos contenidos se considerarán cedidos a la COMPAÑÍA a título gratuito, por el máximo tiempo permitido y para todo el mundo, pudiendo ser utilizados por la COMPAÑÍA dentro de los límites establecidos por la normativa de aplicación, sin que aplique ninguna obligación en materia de confidencialidad sobre dichos contenidos o información, con excepción de la información propietaria del proveedor. Se entiende incluida en esta cesión la autorización al uso de la propia imagen del usuario, sin contraprestación, cuando el contenido, en cualquier medio o formato contenga esta última, para fines comerciales de la COMPAÑÍA. Está totalmente prohibido el envío por parte de los usuarios de información que no pueda ser tratada de esta forma o que contenga elementos o contenidos cuya titularidad corresponda a terceros. <br />
Debido a la gran cantidad de material que se puede alojar en el Portal de Proveedores, resulta imposible por parte de la COMPAÑÍA verificar la originalidad o no infracción de derechos de terceros sobre los contenidos suministrados por el usuario, siendo este el último el único responsable a todos los efectos de las infracciones que eventualmente podrían cometerse como consecuencia del suministro de dicha información. <br />
La COMPAÑÍA podrá modificar los materiales suministrados por los usuarios, a fin de adaptarlos a las necesidades de formato del Portal de Proveedores.<br />
La COMPAÑÍA no se hará responsable de las opciones de disposición que elija el usuario respecto de los contenidos que aloje, poniendo a disposición de los mismos un espacio para que éstos incorporen sus contenidos y los compartan con otros usuarios, no controlando tampoco si los contenidos infringen o no los derechos mencionados en los párrafos anteriores.<br />
Sin perjuicio de lo contemplado en estas condiciones generales, cuando la publicación de los contenidos efectuada por el usuario sea como consecuencia la solicitud de servicios particulares del Portal de Proveedores, serán de aplicación preferente las condiciones legales en cada caso establecidas en sus Condiciones Particulares, quedando las presentes Condiciones Generales como complementarias de las anteriores.<br />
9.	ENLACES O HIPERENLACES AL PORTAL DE PROVEEDORES<br />
El usuario que quiera introducir enlaces o hiperenlaces desde sus propias páginas Web al Portal de Proveedores de la COMPAÑÍA deberá cumplir con las condiciones que se detallan a continuación, sin que el desconocimiento de las mismas evite las responsabilidades derivadas de la ley:<br />
a)	El enlace o hiperenlace únicamente vinculará con la “home page” o página principal del Portal de Proveedores pero no podrá reproducirla de ninguna forma ("inline", "links", "deep- links", "browser" o "border environment", copia de los textos, gráficos, etc.). <br />
b)	Quedará en todo caso prohibido, de acuerdo con la legislación aplicable y vigente en cada momento, establecer "frames" o marcos de cualquier tipo que envuelvan el Portal de Proveedores o permitan la visualización de parte o de la totalidad del Portal de Proveedores a través de direcciones de Internet distintas a las del Portal de Proveedores y, en cualquier caso, cuando permitan la visualización de elementos del Portal de Proveedores conjuntamente con contenidos ajenos al Portal de Proveedores de forma que: (i) induzca o sea susceptible de inducir a error, confusión o engaño a los usuarios sobre la verdadera procedencia de los elementos que se visualizan o los servicios que se utilizan; (ii) suponga un acto de comparación o imitación desleal; (iii) sirva para aprovechar la reputación de la marca y prestigio de la COMPAÑÍA; o, (iv) de cualquier otra forma, resulte prohibido por la legislación vigente. <br />
c)	No se realizarán desde la página que introduce el enlace ningún tipo de manifestación o indicación falsa, inexacta o incorrecta sobre la COMPAÑÍA, sus empleados, clientes o sobre la calidad de los servicios que presta. 
d)	En ningún caso, se expresará ni se dará a entender en la página donde se ubique el enlace que la COMPAÑÍA ha prestado su consentimiento para la inserción del enlace o que de otra forma patrocina, colabora, verifica o supervisa los servicios del remitente. <br />
e)	Está prohibida la utilización de cualquier marca denominativa, gráfica o mixta o cualquier otro signo distintivo de la COMPAÑÍA dentro de la página del remitente, salvo en los casos permitidos por la ley o expresamente autorizados por la COMPAÑÍA y siempre que se permita, en estos casos, un enlace directo con el Portal de Proveedores en la forma establecida en esta cláusula. <br />
f)	La página que establezca el enlace o el hiperenlace deberá cumplir fielmente con la ley y no podrá en ningún caso disponer o enlazar con contenidos propios o de terceros que: (i) sean ilícitos, nocivos o contrarios a la moral y a las buenas costumbres (incluyendo a título meramente ejemplificativo y sin limitación, contenidos pornográficos, violentos, racistas, etc.); (ii) induzcan o puedan inducir al usuario a la falsa creencia de que la COMPAÑÍA suscribe, respalda, se adhiere o de cualquier manera apoya las ideas, manifestaciones o expresiones, lícitas o ilícitas, del remitente; (iii) resulten inapropiados o no pertinentes con la actividad de la COMPAÑÍA en atención al lugar, contenidos y temática de la página web del remitente.<br />
g)	La autorización para insertar un enlace o hiperenlace no presupone, en ningún caso, un consentimiento para reproducir los aspectos visuales y funcionales ("look and feel") del Portal de Proveedores y/o Contenido de la COMPAÑÍA. En especial, la autorización para la inserción de hiperenlaces del Portal de Proveedores estará condicionada al respeto por la dignidad y la libertad humana. El sitio web en el que se establezca el hiperenlace no contendrá informaciones o contenidos ilícitos, contrarios a la moral y a las buenas costumbres y al orden público, así como tampoco contendrá contenidos contrarios a cualesquiera derechos de terceros. <br />
h)	El establecimiento del enlace no implica en ningún caso la existencia de relaciones entre la COMPAÑÍA y el propietario de la página web en la que se establezca, ni la aceptación y aprobación por parte de la COMPAÑÍA de los contenidos o servicios allí ofrecidos puestos a disposición del público. La COMPAÑÍA podrá solicitar, en cualquier momento y sin necesidad de aportar las razones de dicha solicitud, que se elimine cualquier enlace o hiperenlace al Portal de Proveedores, estando obligado el responsable del sitio web que publica el enlace a proceder de inmediato a su eliminación.<br />
10.	USO DE "COOKIES" <br />
La COMPAÑÍA utiliza "cookies" para personalizar la navegación del usuario por el Portal de Proveedores. Las “cookies” son ficheros enviados a un navegador por medio de un servidor web para el mantenimiento de la sesión de navegación almacenando su dirección IP (de su ordenador) y otros posibles datos de navegación. Las “beacons" son imágenes electrónicas que permiten al Portal de Proveedores contar el número de visitantes y usuarios que han ingresado al Portal de Proveedores en particular y acceder a ciertas "cookies". Gracias a las “cookies”, resulta posible que el servidor de la COMPAÑÍA reconozca el navegador del ordenador utilizado por el usuario con la finalidad de que la navegación sea más sencilla así como para medir la audiencia y parámetros del tráfico (mediante “beacons”), controlar el progreso y número de entradas, utilizándose la información contenida en las “cookies” de forma desvinculada respecto de cualquier otro dato de carácter personal que pudiera constar del usuario. Puede obtener más información sobre "cookies" consultando la cláusula 14 de la Política de Privacidad de la COMPAÑÍA.<br />
11.	GARANTÍAS <br />
La COMPAÑÍA declara que ha adoptado todas las medidas necesarias, dentro de sus posibilidades y del estado de la tecnología de la que dispone, para garantizar el funcionamiento del Portal de Proveedores y evitar la existencia y transmisión de virus y demás componentes dañinos o maliciosos a los usuarios.<br />
Si el usuario tuviera conocimiento de la existencia de algún contenido ilícito, ilegal, contrario a las leyes o que pudiera suponer una infracción de derechos de propiedad intelectual y/o industrial, deberá notificarlo inmediatamente a la COMPAÑÍA para que éste pueda proceder a la adopción de las medidas oportunas.<br />
12.	RESPONSABILIDADES <br />
La COMPAÑÍA queda exenta de cualquier tipo de responsabilidad por daños y perjuicios en cualquier concepto y naturaleza en los siguientes casos:<br />
a)	Por la imposibilidad o dificultades de conexión empleadas para acceder al Portal de Proveedores, interrupciones del servicio, demoras, errores, mal funcionamiento del mismo independientemente de la clase de conexión o medio técnico utilizado por el usuario. <br />
b)	Por la interrupción, suspensión o cancelación del acceso al Portal de Proveedores, así como por la disponibilidad y continuidad del funcionamiento del Portal de Proveedores o de los servicios y/o elementos de los mismos, cuando ello se deba a la interrupción del servicio por mantenimiento técnico del Portal de Proveedores, o bien a una causa ajena al ámbito de control de la COMPAÑÍA, o bien por los servicios de los prestadores de servicios de la información. <br />
c)	Por actuaciones dolosas o culposas del usuario, o que tengan su origen en causas de fuerza mayor y cualesquiera otras que se escapen del control de la COMPAÑÍA. <br />
d)	Por ataques de los denominados "hackers" o terceros especializados a la seguridad o integridad del sistema informático, siempre que la COMPAÑÍA haya adoptado todas las medidas de seguridad existentes según sus posibilidades técnicas. <br />
e)	Por los daños o perjuicios que pudieran causar la información, contenidos, productos y servicios prestados, comunicados, alojados, transmitidos, exhibidos u ofertados por terceros ajenos a la COMPAÑÍA, incluidos los prestadores de servicios de la sociedad de la información, a través de un sitio web al que pueda accederse mediante un enlace existente en este sitio. <br />
f)	Por cualquier daño o perjuicio en el software o hardware del usuario que se derive del acceso al Portal de Proveedores de la COMPAÑÍA o del uso de la información o elementos en ellos contenidos. <br />
g)	Por la idoneidad, fiabilidad, disponibilidad, oportunidad o exactitud de la información o servicios contenidos en el Portal de Proveedores, ni por los daños directos o indirectos en relación con el uso de la información o elementos en ellos contenidos.<br /> 
h)	Por el tratamiento y utilización posterior de datos personales realizados por terceros ajenos a la COMPAÑÍA, así como la pertinencia de la información solicitada por éstos. <br />
El usuario del Portal de Proveedores responderá personalmente de los daños y perjuicios de cualquier naturaleza causados a la COMPAÑÍA directa o indirectamente, por el incumplimiento de cualquiera de las obligaciones derivadas de estas Condiciones Generales o, en su caso, de las Condiciones Particulares. En cualquier caso, sea cual fuere la causa, la COMPAÑÍA no asumirá responsabilidad alguna, ya sea por daños directos o indirectos, daño emergente ni por lucro cesante.<br />
El usuario será el único responsable de las infracciones en que pueda incurrir o de los perjuicios que pueda causar por el uso del Portal de Proveedores de la COMPAÑÍA, quedando esta exonerada de cualquier responsabilidad. El usuario es el único responsable frente a cualquier reclamación o acción legal, judicial o extrajudicial, iniciada por terceras personas contra la COMPAÑÍA o bien contra el usuario basada en la utilización por éste del servicio, o por la información que haya podido remitir a la COMPAÑÍA por cualquier medio. El usuario asume cuantos gastos, costes e indemnizaciones sean irrogados a la COMPAÑÍA con motivo de tales reclamaciones o acciones legales.<br />
La COMPAÑÍA pone a disposición de los usuarios una serie de links, banners u otro tipo de enlaces que podrán dar acceso al usuario a sitios web de terceros. El acceso a otras páginas web de terceros a través de dichas conexiones o "links" se realizará bajo la exclusiva responsabilidad de los usuarios, no siendo la COMPAÑÍA responsable, en ningún caso, de los daños o perjuicios que puedan derivarse de dichos usos o actividades. En materia de responsabilidad por contenidos, la COMPAÑÍA nunca será responsable:<br />
a)	Por los daños o perjuicios que pudieran causar la información, contenidos, productos y servicios prestados, comunicados, alojados, transmitidos, exhibidos u ofertados por terceros ajenos a la COMPAÑÍA, incluidos los prestadores de servicios de la sociedad de la información, a través de un sitio web al que pueda accederse mediante un enlace existente en este sitio.<br /> 
b)	Por cualquier daño o perjuicio en el software o hardware del usuario que se derive del acceso al Portal de Proveedores de la COMPAÑÍA o del uso de la información o aplicaciones en ellos contenidos. <br />
c)	Por la idoneidad, fiabilidad, disponibilidad, oportunidad o exactitud de la información o servicios del Portal de Proveedores, ni por los daños directos o indirectos en relación con el uso de la información o aplicaciones en ellos contenidos.<br />
La COMPAÑÍA dispone sus contenidos en el ámbito territorial de España. Habida cuenta de la propia naturaleza "no territorial" inherente a los accesos a Internet, la COMPAÑÍA no garantiza que el Portal de Proveedores sea apto o esté disponible fuera del territorio de España. Si alguno o todos los contenidos o elementos alojados en el Portal de Proveedores de la COMPAÑÍA fueran considerados ilegales en otros países, queda prohibido el acceso a los mismos y su utilización por los usuarios y, en el caso de que estos se produzcan, será exclusivamente bajo la responsabilidad de los usuarios, estando estos obligados al cumplimiento y observancia de las leyes aplicables de estos países.<br />
13.	GENERAL <br />
El acceso, los contenidos y servicios ofrecidos a través del Portal de Proveedores tienen, en principio, una duración indefinida, salvo que se disponga lo contrario en las Condiciones Generales, las Condiciones Particulares o en la legislación aplicable en cada momento. La COMPAÑÍA se reserva no obstante, sin necesidad de previo aviso y en cualquier momento, el derecho a suspender, denegar o restringir temporal o definitivamente el acceso al Portal de Proveedores, a efectuar las modificaciones que considere oportunas en el Portal de Proveedores, en los servicios o informaciones ofrecidas, en la presentación o localización de los mismos así como en las Condiciones Generales. Todo ello sin que ello de lugar a indemnización alguna al usuario.<br />
Cualquier cláusula o disposición de las presentes Condiciones Generales que sea o devenga ilegal, inválida o inexigible será excluida y será considerada inaplicable en lo que alcance a tal ilegalidad, invalidez o inexigibilidad, y será sustituida por otra que se asemeje lo más posible a la anterior, pero no que no afecte o perjudique a las restantes disposiciones, las cuales quedarán al margen de cualquier cláusula o disposición ilegal, inválida o inexigible y permanecerán, por el contrario, plenamente vigentes.<br />
La COMPAÑÍA excluye cualquier tipo de garantía, y, por tanto, queda libre de toda responsabilidad derivada de los puntos expresados con anterioridad, así como de otros aspectos que pueden no estar contemplados en el presente documento. Toda la información que se reciba en el Portal de Proveedores se considerará cedida a la COMPAÑÍA a título gratuito. <br />
El email o correo electrónico no será considerado como medio válido a los efectos de presentación de reclamaciones por razón de contenidos. Para ello, deberán dirigirse por escrito al Departamento de Asesoría Jurídica de la COMPAÑÍA, sito en la Calle Titán 15, 28045 de Madrid, que indicará en cada caso los cauces a seguir. Estas Condiciones Generales se rigen por la ley española. Las partes se someten, a su elección, para la resolución de los conflictos y con renuncia a cualquier otro fuero, a los juzgados y tribunales de Madrid Capital. 
			</p>
            <p style="text-align:right"><a href="registro_texto.htm" class="bt_registro" target="blank">Imprimir</a> </p>
<div class="caja_acepto">


<!--<form action="" method="post" style="width:400px;">//-->
<form name="frmAlta" id="frmAlta" method="post" style="width:400px;">
  <span id="sprycheckbox1">
<label>
  <input type="checkbox" name="opcion1" id="opcion1" required>
</label>
<span class="checkboxRequiredMsg">Seleccione para continuar.</span></span><span class="rojo">He leído y Acepto</span> las <a href="spa/docs/Condiciones_Generales_Portal_Proveedores.pdf" target="_blank" class="rojo">Condiciones Generales de Uso del Portal de Proveedores</a> y la <a href="spa/docs/Política de privacidad y protección de datos personales.pdf" target="_blank" class="rojo">Politica de Privacidad</a>.<br />
<br />
<span id="sprycheckbox2">
<label>
  <input type="checkbox" name="opcion2" id="opcion2" required>
</label>
<span class="checkboxRequiredMsg">Seleccione para continuar.</span></span><span class="rojo">Acepto la Cesión de los Datos</span> proporcionados a favor de las compañías que conforman Mahou-San Miguel, [<a href="spa/docs/Cláusula_de_protección_de_datos.pdf" target="_blank" class="rojo">Claúsula de protección de datos</a>], con las mismas finalidades.<br />
<br />
<span id="sprycheckbox3">
<label>
  <input type="checkbox" name="opcion3" id="opcion3" required>
</label>
<span class="checkboxRequiredMsg">Seleccione para continuar.</span></span><span class="rojo">Acepto Recibir Información</span> relacionada con el Portal de Proveedores vía email.<br />
<br />
<button type="button" class="bt_registro" id="submit1" name="submit1">Siguiente</button>
<br>
</form>


</div>
<div style="clear:both;"></div>
    	</div>    
    </div>
</div>
<script type="text/javascript">
// <!--
// var sprycheckbox1 = new Spry.Widget.ValidationCheckbox("sprycheckbox1");
// var sprycheckbox2 = new Spry.Widget.ValidationCheckbox("sprycheckbox2");
// var sprycheckbox3 = new Spry.Widget.ValidationCheckbox("sprycheckbox3");
// //-->
$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked') && $('#opcion2').is(':checked') && $('#opcion3').is(':checked')) {
        $('#frmAlta').submit();
        alta();
    }
});
</script>
 			
</body>
<!--#include file="../../common/fsal_2.asp"-->
</html>
