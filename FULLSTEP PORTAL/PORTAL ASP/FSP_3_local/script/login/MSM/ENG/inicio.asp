﻿<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="../estilos.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">



<title>Portal de proveedores</title>
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
}
-->
</style>
</head>

<script>
dibujaMenu(1)
</script>

<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function Init() {
    document.getElementById('tablemenu').style.display = 'block';

    MM_preloadImages('../images/icono_docs_sm.gif', '../images/icono_docs_sm.gif')

    p = window.top.document.getElementById("frSet")
    vRows = p.rows
    vArrRows = vRows.split(",")
    vRows = vArrRows[0] + ",*,0"
    p.rows = vRows
}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">
<!-------------------------- BEGIN COPYING THE HTML HERE ----------------------------> 
<table width="100%" border="0" cellpadding="5" cellspacing="2" bordercolor="0" hspace="0" vspace="0">
  <tr> 
    <td width="12" rowspan="3" align="right" valign="top" bordercolor="0"> <div align="left" ></div>
    </td>
    <td height="74" colspan="2" align="left" valign="middle" class="rojo_24">Welcome </td>
    <td colspan="2" align="left" valign="bottom">&nbsp;</td>
  </tr>
  <tr>

    <td rowspan="2" valign="top" bgcolor="#EEEEEE" class="contenido_texto arial">
	<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>

    To navigate through the different areas of the portal, use the navigation bar in the topside of the page.
      <ul>
        <li><span><b>Quality:</b> Access all requests for information sent by Mahou-San Miguel.</span></li>
      </ul>
        <ul>
        <li><span><b>Requests for quotations:</b> Access all RFQ received and manage them from this section.</span></li>
      </ul>
      <ul>
        <li><span><b>Your data :</b> In order to modify your personal data, please click on &quot;Change personal data&quot;. You can also  change your personal formats configuration from &quot;User options&quot;.</span></li></ul>
      
       <ul>
        <li><span><b>Other actions</b>:</span> </li>
        <ul>
        <li><span><b> Orders/Invoices/Basic data:</b> Access to know the status of your orders, invoices and basic data of your company. </span></li>
      
	       
        <li><span><b>Prevention management</b>: Access the prevention management system, for those services that require prevention management. </span></li>
      </ul></ul>
      <ul>
        <li><span><b>Definitions :</b></span> </li>
        <ul>
        <li><span><strong>Registered supplier</strong>: registered supplier in the Portal.</span>
        </li>
        <li><span><strong>o	Pre-certified supplier</strong>: : registered supplier in the Portal that has successfully completed the requested forms with information aimed to understand whether, overall, your company and products or services meet Mahou-San Miguel’s standards in order to be invited to a bidding process or call for tenders.</span>
        </li>
        <li><span><strong>Certified supplier</strong>: certified supplier that has obtained an y award by Mahou-San Miguel.</span></li>
        </ul>
      </ul>

      
If this is the first time you are posting an offer through the portal, please follow these steps<br>
<br>
<b>1. Click on &ldquo;Requests for quotation&rdquo;</b> to display the requests for quotations pending for your company.<br>
<br>
<strong>2. Select the request for quotation</strong> for which you want to send an offer <strong>by clicking on its code</strong>.<br>
<br>
<strong>3. Configure your offers</strong> by filling in the appropriate data. From the navigation tree on the left side of the screen, you will be able to navigate through the different sections of the offer. To fill in the prices, click on the &ldquo;items/prices&rdquo; section. Don&rsquo;t forget to fill in the validity dates of your offer in the &ldquo;General data of the offer&rdquo; section. <br>
<br>
<b>4. Post your offers by clicking on Send </b>><b><IMG height=14 src="../images/sobre_tr.gif" width=30 align=absBottom></b>   </div>    </td>
  </tr>
</table>

	</td>

    <td rowspan="2" valign="top" >&nbsp;</td>
    <td colspan="2" align="left" valign="top"><table width="100%" border="0" class="textos">
        <tr>
          <td height="26" valign="top" class="textos"><span class="textos"><strong>TUTORIALS</strong></span></td>
        </tr>
        <tr>
          <td height="62" class="textos">You can download the following tutorials in pdf format: </td>
        </tr>
        <tr>
          <td width="273" class="textos"><a href="docs/FSN_MAN_ATC_Cómo ofertar.pdf" class="textos" target="_blank"><img src="../images/punto_rojo.jpg" width="6" height="6" border="0"> How to offer</a></td>
        </tr>
        <tr>
          <td><a href="docs/FSN_MAN_ATC_Requisitos técnicos.pdf" class="textos" target="_blank"><img src="../images/punto_rojo.jpg" width="6" height="6" border="0"> Technical Requirements </a></td>
        </tr>
        <tr>
          <td><a href="docs/Requisitos%20t&#233;cnicos%20proveedor.pdf" class="textos" target="_blank"><img src="../images/punto_rojo.jpg" width="6" height="6" border="0"></a><a href="docs/FSN_MAN_ATC_Mantenimiento de datos.pdf" class="textos" target="_blank"> Master data</a></td>
        </tr>
        <tr>
          <td>&nbsp;   </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
    </table>
	</td>
  </tr>
  <tr>
    <td align="left" valign="bottom"><table width="100%" height="220" border="0" background="../images/fondo.jpg">
  <tr>
    <td class="subtit"><div class="recordar-claves">
                <span class="rojo"><strong>Supplier Call Center</strong></span><br />
                <span class="rojo_24"><strong>902 043 065</strong></span>
            </div></td>
      </tr>
      <tr>
        <td height="21"><div align="center"><a href="mailto:atenciontecnicaproveedores@mahou-sanmiguel.com" class="textos">soporte@proveedoresmahou-sanmiguel.com</a></div></td>
      </tr>
    </table>
      <div align="center"></div></td>
  </tr>
</table>
    </td>
  </tr>

</table>
</body></html>
