﻿<%@ Language=VBScript %>
<!--#include file="../../login.asp"-->
<%		
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if

if Not IdOrden = "" then
    if Not IsNumeric(IdOrden) then
        IdOrden=""
    end if
end if

if Not CiaComp = "" then
    if Not IsNumeric(CiaComp) then
        CiaComp=""
    end if
end if

If Idioma="" then
	Idioma="ENG"
end if

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://fast.fonts.net/jsapi/f5dbd368-355c-4f28-bc2f-217b51498d80.js"></script>
<script language="javascript">
<!--


function ventanaLogin (IDI){

   window.open ("registro.asp?Idioma="+IDI,"","width=700,height=600,resizable=yes")

}
function ventanaSecundaria (URL){

   window.open(URL,"ventana1","width=700,height=420,scrollbars=no")

}
function ventanaAyuda (URL){

   window.open(URL,"ayuda","width=700,height=600,scrollbars=yes")

}

function popup (URL) {
 window.open("popup.htm","","width=700,height=400,scrollbars=NO")
 }
function recuerdePWD()
{
   window.open("recuerdo.asp","_blank","width=715,height=345,scrollbars=NO")
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
//-->
</script>
<script src="js/jquery-1.9.0.min.js"></script>
<!--ventana modal-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" href="../js/colorbox.css" />
<script src="../js/jquery.colorbox.js"></script>
<script>
    $(document).ready(function () {
        $(".modal").colorbox();
    });
</script>
<!--slide de imagenes-->
<link rel="stylesheet" href="../nivo/nivo-slider.css" type="text/css" media="screen" />
<script type="text/javascript" src="../nivo/jquery.nivo.slider.js"></script>
<!--*****************-->
<script>
$(document).ready(function(){
	//---------------------
	
	//---------------------
	$('#slider').nivoSlider({
		effect: 'fade',
		directionNav: false,
		controlNav: false,
		pauseOnHover: false	
	});
	//---------------------
  	$("#txtCIA").val("Company Code");
  	$("#txtCIA").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Company Code"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Company Code");
  			$(this).removeClass('focus');
  		}
  	});	
	//---------------------
  	$("#txtUSU").val("User Code");
  	$("#txtUSU").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="User Code"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("User Code");
  			$(this).removeClass('focus');
  		}
  	});
	//---------------------
  	$("#txtPWD").val("Password");
  	$("#txtPWD").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Password"){
  			$(this).val('');
			$(this).removeAttr("type");
			$(this).prop('type', 'password');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Password");
			$(this).removeAttr("type");
			$(this).prop('type', 'text');
  			$(this).removeClass('focus');
  		}
  	});
});
</script>
</head>
<body>
<div id="contenedor">
	<!--cabecera-->
	<div id="cabecera">
    	<div id="logo">
        	<a href="http://www.mahou-sanmiguel.com" target="_blank"><img src="../images/logo.jpg" alt="Mahou" /></a>
        </div>
        <div id="nav">
        	<div id="nav_sup">
            	<ul>
                	<li><a href="../default.asp">ESP</a></li>
                    <li><a href="">ENG</a></li>
                </ul>
            </div>
            <ul id="nav_inf">
            	<li><a href="" >HOME</a></li>
            	<li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/politica-de-compras.html" >PURCHASE POLICY</a></li>
                <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/ser-proveedor.html" >BE A SUPPLIER</a></li>
                <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/que-compramos.html" >WHAT WE PURCHASE</a></li>
                <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/compra-responsable.html" >RESPONSIBLE PURCHASING</a></li>
                <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/ayuda.html" class="modal">HELP</a></li>
            </ul>
        </div>
    </div>
    <!--********-->
    <!--titulo-->
    <div id="titulo">
        <h1>SUPPLIERS</h1>
        <p>
       Mahou San Miguel offers this portal as a direct means for communicating and exchanging information with our suppliers, including offers, orders, invoices, etc. <br />
        You can also find here the forms you need to register as a Mahou San Miguel supplier, our purchase policies, and other useful information for suppliers. 
        </p>
        <div style="clear:both;"></div>
    </div>
    <!--******-->
    <!--formulario acceso-->
    <div id="acceso">
    	<div id="slider" class="nivoSlider">
            <img src="../images/fondo-vasos.jpg" alt="" width="890" height="300"/>
			<img src="../images/solan-cabras.jpg" alt="" width="890" height="300"/>
            <img src="../images/san-miguel.jpg" alt="" width="890" height="300"/>
        </div>
    	<div class="wform">
            <h2>SUPPLIER ACCESS</h2>
            <form action="default.asp" method="post" name="frmLogin"  id="frmLogin">
                <%if Application ("ACCESO_SERVIDOR_EXTERNO")=1 then%>
                    <div style="text-align:center;"><span class="bt">No es posible iniciar la sesión. Está activa la autenticación integrada.</span></div>
                <%else%>
                    <input type="text" class="text" id="txtCIA" name="txtCIA" maxlength="20">
                    <input type="text" class="text" id="txtUSU" name="txtUSU" maxlength="20">
                    <input type="password" class="text" id="txtPWD" name="txtPWD" maxlength="20">
                    <div class="bt_entrar">
                        <input class="bt" name="cmdEntrar" type="submit" value="LOGIN" />
                        <a href="javascript:void(null)" onClick="recuerdePWD()" >Forgot your access codes?</a>
                    </div>
                    <h3>NOT YET REGISTERED?</h3>
                    <div align="center">
                        <a href="javascript:ventanaLogin('ENG')" >REGISTER</a>
                    </div>
                    <input type="hidden" id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
                    <input type="hidden" id="CiaComp" name="CiaComp" value="<%=CiaComp%>">
                <%end if%>
            </form>
        </div>
    </div>
    <!--*****************-->
    <!--navegador iconos-->
    <div id="nav_home">
    	<ul>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/politica-de-compras.html"><img src="../images/ico_politica.png" alt="Política de compras" /><span>PURCHASE<br />POLICY</span></a></li>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/ser-proveedor.html"><img src="../images/ico_proveedor.png" alt="Ser proveedor" /><span>BE A<br />SUPPLIER</span></a></li>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/que-compramos.html"><img src="../images/ico_compramos.png" alt="Qué compramos" /><span>WHAT WE<br />PURCHASE</span></a></li>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/compra-responsable.html"><img src="../images/ico_compras.png" alt="Compra responsable" /><span>RESPONSIBLE<br />
                  PURCHASING</span></a></li>
        </ul>
        <div class="datos_fullstep">
        	SUPPORT FOR PORTAL USAGE<br />
            <span class="rojo_24" style="line-height:24px;">Tel. 902 043 065</span><br />
            <a href="mailto:atenciontecnicaproveedores@mahou-sanmiguel.com" class="rojo">soporte@proveedoresmahou-sanmiguel.com</a>
            <img src="../images/linea_roja.jpg" alt="" style="margin:15px 0;" />
            SUPPLIER CALL CENTER:
          <p>
            MONDAY TO THURSDAY<br />
            <span class="rojo">8:00 - 20:00</span>
            </p>
            <p>FRIDAYS<br />
            <span class="rojo">8:00 - 19:00</span>
            </p>
        </div>
        <div style="clear:both"></div>
    </div>
    <!--****************--> 
    <!--footer-->
    <div id="pie">
   	  <div class="datos_pie">
        	<span class="arial">&copy;Mahou San Miguel</span><br />
            <a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/aviso-legal.html" class="modal">Legal notice<img src="../images/ico_aviso.jpg" alt="Aviso legal" style="margin-left:5px;" /></a><br />
            <a href="docs/MSM_General_Terms_and_Conditions.pdf" target="_blank">Legal conditions<img src="../images/ico_aviso.jpg" alt="Condiciones legales" style="margin-left:5px;" /></a><br />
            <a href="<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=ENG" class="modal"">Cookies Policy<img src="../images/ico_aviso.jpg" alt="Cookies Policy" style="margin-left:5px;" /></a>        
            <div id="logoFull"><a href="http://www.fullstep.com/" target="_blank"><img src="../images/logoFullStep.jpg" width="200" height="51" alt=""/></a>
        </div> 
        <div id="optimized"> Website optimized for 1280 x 1024.
                Supported browsers: <a href="https://windows.microsoft.com/es-ES/internet-explorer/download-ie" target="_blank" class="formulario">Internet Explorer</a> y <a href="https://www.mozilla.org/en-EN/firefox/new/" target="_blank" class="formulario">Mozilla Firefox</a>
        </div>
      </div>

<!--        <div class="datos_pie">
			<a href="">Contacta con nosotros</a><br />
            <a href="">Visita nuestras fábricas</a><br />
            <a href="">Mapa web</a>
        </div>
        <div class="datos_pie" style="border:none;">
        	SIGUENOS EN:<br />
			<a href=""><img src="images/ico_fcb.jpg" alt="Facebook" /></a>
            <a href=""><img src="images/ico_twt.jpg" alt="Twitter" /></a>
            <a href=""><img src="images/ico_you.jpg" alt="Youtube" /></a>
        </div>-->
        <div style="clear:both"></div>
    </div>
    <!--******-->
</div>
</body>

</html>
