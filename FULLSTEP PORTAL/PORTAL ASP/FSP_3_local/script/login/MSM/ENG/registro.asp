﻿<%@ Language=VBScript %>
<!--#include file="../../../common/XSS.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://fast.fonts.net/jsapi/f5dbd368-355c-4f28-bc2f-217b51498d80.js"></script>
<script src="http://code.jquery.com/jquery-1.9.0.js"></script>
<!-- <script src="../SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=ENG", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body>

<div id="cont_gen">
	<div id="izd_gen"> <a href="http://www.mahou-sanmiguel.com" target="_blank"><img src="../images/logo_gen.jpg" alt="Mahou San Miguel" /></a>
    </div>
    <div id="drc_gen">
    	<h1>ASK FOR REGISTRATION</h1>
        <div class="int">
        	<p>To proceed with the sign up 
          proccess, the following 
                <span class="rojo">GENERAL CONDITIONS OF USE OF THE SUPPLIER PORTAL</span> must be read and accepted.
            </p>
            <p style="height:150px; padding:0 5px; overflow:auto; background:#FFF; border:#CCC 1px solid">
               GENERAL TERMS AND CONDITIONS OF USE OF THE MAHOU-SAN MIGUEL SUPPLIER PORTAL<br />
               1.	GENERAL INFORMATION <br />
2.	GENERAL TERMS AND CONDITIONS OF USE OF THE SUPPLIER PORTAL<br />
3.	MINORS <br />
4.	USER LIABILITY FOR THE USE OF THE SUPPLIER PORTAL <br />
5.	USER IDENTIFIERS AND PASSWORD <br />
6.	PARTICULAR TERMS AND CONDITIONS<br />
7.	INTELLECTUAL AND INDUSTRIAL PROPERTY <br />
8.	INFORMATION AND ELEMENTS OF THE SUPPLIER PORTAL<br />
9.	LINKS OR HYPERLINKS TO THE SUPPLIER PORTAL <br />
10.	USE OF COOKIES <br />
11.	WARRANTIES <br />
12.	LIABILITIES <br />
13.	GENERAL <br />


I.	I.	GENERAL TERMS AND CONDITIONS OF THE SUPPLIER PORTAL<br />
1.	1.	GENERAL INFORMATION  <br />
The terms and conditions indicated below (hereinafter, the “General Terms and Conditions”), control the access, login, navigation, downloading and use of each and every one of the portal pages and functionalities (orders, requests for proposals, etc.) localisable or accessible through the domain WWW.MAHOU-SANMIGUEL.ES and its respective subdomains and subdirectories implemented by the Mahou-San Miguel to manage the relationship between its suppliers and each of the companies: MAHOU, S.A., SAN MIGUEL FÁBRICAS DE CERVEZA Y MALTA, S.A., CERVEZAS ANAGA, S.A., MSM GRUPO COMERCIALIZADOR, S.A., TRANSPORTES AGRO INDUSTRIALES, S.A., AGUAS DE SOLÁN DE CABRAS, S.A., CERVEZAS ALHAMBRA, S.L.U., ALHAMBRA DISTRIBUIDORA MERIDIONAL, S.L., ANDALUZA DE CERVEZAS Y BEBIDAS, S.L. and PENIBÉTICA DE CERVEZAS Y BEBIDAS, S.L. (hereinafter, the "Supplier Portal ").
In accordance with the provisions of Article 10 of Law 34/2002, of 11 July, on Information Society Services and Electronic Commerce, the aforementioned companies (hereinafter, the “COMPANY”) place the following registry information of the website's owner at its users' disposal: 
<br />
Company Name: Mahou, S.A.<br />
Tax ID number: A28078202<br />
Mercantile data: Registered in the Mercantile Registry of Madrid, under Volume 54, folio 225, Section 8, Sheet M-1076 <br />
Registered address: Calle Titán, 15, 28045 Madrid (Spain)<br />
Contact: You can contact the COMPANY by ordinary post at its registered address or at the following email address:  info@mahou-sanmiguel.info.<br />
BY ACCESSING, VIEWING OR USING THE MATERIALS OR SERVICES ACCESSIBLE IN OR THROUGH THE "SUPPLIER PORTAL" OR OTHER WEB PAGES OR COMPUTER APPLICATIONS, THE USER CONFIRMS THAT HE OR SHE UNDERSTANDS AND ACCEPTS THESE "GENERAL CONDITIONS" AS THE LEGAL EQUIVALENT OF A WRITTEN, SIGNED AND BINDING DOCUMENT.<br />
2.	GENERAL TERMS AND CONDITIONS FO USE OF THE "SUPPLIER PORTAL"<br />
These General Terms and Conditions control the use of the website that the COMPANY places at the disposal of its supplier network through the Supplier Portal, for the purpose of providing an agile and effective tool through which to request proposals, documents or other commercial information that the COMPANY deems necessary, or send you the information it considers may be of use to you. Likewise, the Supplier Portal will allow you to manage your orders, make technical consultations, send requests, consult your user account, etc. The COMPANY acts both as direct purchaser and purchasing manager of its clients and distributors, and will act on their behalf during the purchasing process. <br /> 
Access and use of the COMPANY's Supplier Portal shall be subject to these General Terms and Conditions. The use of the Supplier Portal owned by the COMPANY attributes user status and implies the user's full and unreserved acceptance of all the Terms and Conditions applicable whenever the user logs in thereto. The COMPANY reserves the right to modify these Terms and Conditions at any time. Any change made to these General Terms and Conditions that can affect the user's rights shall be communicated in the Supplier Portal property thereof during the first week subsequent to the implementation of said change. In addition to this notice that will inform you of said change, we suggest that you review these General Terms and Conditions periodically to stay abreast of any modification made thereto. On accessing the Supplier Portal property of the COMPANY subsequently to the publication of the notice of said changes, alterations or updates, you are accepting the fulfilment of the new terms and conditions. Users are aware that access to and use of the Supplier Portal is performed under their sole and exclusive responsibility. 

 <br />
Some services of the COMPANY's web pages may be subject to Particular terms and conditions, rules and instructions which, as the case may be, replace, complete and/or modify these terms and conditions (hereinafter, the “Particular Terms and Conditions”), which must be accepted by the user before using the Supplier Portal. The provision of the service at the user's request through the Supplier Portal implies the express acceptance of the applicable Particular Terms and Conditions. <br />
These General Terms and Conditions and Particular Terms and Conditions are independent documents to the General or Particular terms and conditions applicable to particular purchases or services, which may be applicable in each case in accordance with the type of service contracted by the COMPANY. <br/>
The expression "Supplier Portal” comprises, in an illustrative and non-limiting manner, all the content, data, graphics, texts, logos, trademarks, software, pictures, animations, musical creations, videos, sounds, drawings, photographs, expressions and information and other elements included therein and, in general, all the creations expressed by any means or medium, tangible or intangible, regardless of whether they are susceptible or not susceptible to intellectual property in accordance with the Consolidated Text of the Spanish Intellectual Property Law.<br />
Users shall use the services and materials available in the Supplier Portal exclusively for professional purposes, within the scope of their activity as supplier of the COMPANY, excluding any use modality other than this one. <br />
These General Terms and Conditions and the Particular Terms and Conditions that can be applied with advance notice in Particular cases are expressly accepted without reservation by the user for merely accessing the Supplier Portal and using the materials and services of the Supplier Portal in any manner. When access to and use of certain materials and/or services of the Supplier Portal is subject to Particular Terms and Conditions, the user shall be informed of said Terms and Conditions and, as the case may be, shall replace, complete and/or modify the General Terms and Conditions set forth herein. Access to and use of said materials or services subject to Particular Terms and Conditions shall therefore imply full compliance with the Particular Terms and Conditions that regulate them in the version published at the time the user accesses them, said Particular Terms and Conditions being automatically incorporated to these General Terms and Conditions. In the event of contradiction between the terms and conditions expressed in these General Terms and Conditions and the Particular Terms and Conditions, the terms agreed in the Particular Terms and Conditions shall prevail in all cases and at all times, although only as regards the incompatible provisions and only with respect to those materials or services of the Supplier Portal subject to said Particular regulation. 

<br />
The COMPANY may modify, unilaterally and without notice, the provision, configuration, content and services of the Supplier Portal and the General Terms and Conditions thereof. If these General Terms and Conditions were replaced by others fully or partially, said new General Terms and Conditions or, where applicable, Particular Terms and Conditions, shall be understood to be accepted in the same manner as those set forth herein. However, Supplier Portal users must periodically review these General Terms and Conditions and the Particular Terms and Conditions of the Supplier Portal services they use in order to stay abreast of any updates. <br />
Should the user not accept these General Terms and Conditions or the Particular Terms and Conditions, the user must abstain from accessing the Supplier Portal or, in the case of being already logged in, close the session. <br />
Users shall establish the appropriate technical security measures to avoid unwanted actions in their information systems, files and computer equipment used to access the Internet and, particularly, the Supplier Portal, being aware that the Internet is not a completely secure environment. In general, the services and materials offered through the Supplier Portal shall be available in Spanish, although the COMPANY may discretionally present said services and materials in other languages. The cost of the telephone access or other type of cost required to access the Supplier Portal shall be borne exclusively by the user. <br />
3.	MINORS.  <br />
Access, login, navigation, use, uploading and/or downloading of materials and/or use of the COMPANY's Supplier Portal services shall be prohibited to any natural or legal person that is not a potential or current supplier of the Mahou-San Miguel and, in particular, access is strictly prohibited to minors (under 18 years of age).  <br />
4.	4.	USER LIABILITY FOR THE USE OF THE SUPPLIER PORTAL <br />
Users shall in no case modify or delete the COMPANY's identification details. Users may only access the services and materials of the Supplier Portal through the means or procedures placed at their disposal for such purpose in the Supplier Portal itself or which are normally used on the Internet for such purpose, provided that they do not violate intellectual/industrial property rights or imply any kind of damage to the Supplier Portal or to its information or to the services offered. <br />
Users undertake to use the services, the information and materials of the COMPANY's Supplier Portal in accordance with the law and with these General Terms and Conditions. In no case shall the use of the Supplier Portal by users infringe current legislation, morality, good practices and public order, and shall use the services, information and materials of the Supplier Portal in a correct and lawful manner at all times. <br />
Likewise, the user guarantees that it has the necessary representation capacity to act with respect to the COMPANY on behalf of the company they represent, and to assume rights and obligations on behalf of said company, holding the COMPANY harmless and exempt from liability for any damages and losses caused by the cancellation or termination of the acts performed through the Supplier Portal.<br />
The user shall:<br />
o	Provide the necessary data to sign up on the Supplier Portal and ensure the proper functioning thereof, as well as keeping said data updated, communicating any change therein in the shortest possible time.<br />
o	Guarantee the authenticity of the data provided upon completing the necessary forms to subscribe to the services. Likewise, the user and the supplier he or she represents shall be solely responsible for the damages and losses caused to the COMPANY as a result of inaccurate or false statements.<br />
o	Fulfil his or her commitments with respect to the information sent via the Supplier Portal. In the event that the user or the supplier that he or she represents does not demonstrate the necessary commercial diligence or nonfulfil the obligations acquired, the COMPANY reserves the right to temporarily or permanently exclude them from the Supplier Portal.<br />
o	Accept the Particular Terms and Conditions with respect to certain services, as determined in these General Terms and Conditions or, where applicable, in the Particular Terms and Conditions of said services.<br />
o	Use the services and materials of the Supplier Portal for exclusively professional use, within the scope of its activity as the COMPANY's supplier. <br />
o	Guarantee that the proposals uploaded into the Supplier Portal are given the same consideration and validity as proposals sent via traditional means.<br />
o	Make proper and lawful use of the Supplier Portal, in accordance with current legislation, morality, good practices and public order. 
<br /> 
Users shall in no case perform the following activities:<br />
a)	a)	Disseminate content or propaganda of a nature that is pornographic, obscene, denigratory or incites or promotes the commitment of criminal, violent, defamatory or degrading acts on the grounds of age, religion or beliefs; or that directly or indirectly encourages, promotes or incites terrorism or that is of a nature that is contrary to human rights and the basic rights and freedoms of third parties, to current legislation, morality, good practices and public order, or with injurious intentions that could in any manner prejudice, damage or prevent access thereto, to the detriment of the COMPANY or third parties. <br />
b)	Perform acts contrary to the intellectual and/or industrial property rights of their legitimate owners. <br />
c)	Cause damage to the COMPANY's computer systems, those of its suppliers or third parties and/or introduce or disseminate computer viruses, malicious code or software or other type of systems that could cause damage or alterations in computer systems, or unauthorised alteration of the contents, programs or systems accessible through the materials or services of the Supplier Portal, or in the information systems, files and computer equipment of the users thereof, or the unauthorised access to any materials and services of the Supplier Portal. <br />
d)	Transmit advertising via any means, particularly via electronic messages, when the remittance of said advertising has not been authorised by the recipient. <br />
e)	Use the Supplier Portal, wholly or in part, to promote, sell, contract, disseminate proprietary or third-party advertising or information without the COMPANY's prior written authorisation, in relation to products or services other than those offered by the supplier to the COMPANY, or include hyperlinks on their private or commercial websites to the Supplier Portal without the express authorisation of the COMPANY.<br />
f)	Use the services and materials offered through the Supplier Portal in a manner contrary to the General Terms and Conditions and/or Particular Terms and Conditions that control the use of a certain service and/or content, to the detriment of or undermining the rights of other users. <br />
g)	Delete or modify in any way the protection or identification devices of the COMPANY or its legitimate owners contained in the Supplier Portal, or the symbols, logos or trademarks that the COMPANY or third parties that legitimately own the rights thereto incorporate in their creation and may be subject to intellectual or industrial property. <br />
h)	Include, without the COMPANY's prior written authorisation, "meta tags" corresponding to trademarks, logos, commercial names or distinctive signs property of the COMPANY on web pages controlled or owned by unauthorised third parties. Or use trademarks, logos, commercial names or any other identifying sign that is subject to intellectual or industrial property rights, without the prior express written authorisation of their legitimate owner. <br />
i)	Reproduce, wholly or in part, copy, distribute, rent, transform or grant public access to, through any public communication medium, the materials and information contained in the Supplier Portal, or include them in a different website without the COMPANY's prior written authorisation. <br />
j)	Include, in a website owned or controlled by the COMPANY, a hyperlink that generates a window or session of the navigation software used by a user of said website, in which proprietary trademarks, commercial names or distinctive signs are included and wherethrough the Supplier Portal is shown. 
 <br />
Users shall be liable to the COMPANY, or third parties, for any damages or losses of any kind arising from the direct or indirect nonfulfillment of or noncompliance with these General Terms and Conditions. The COMPANY shall ensure compliance with the current legal system at all times and reserves the right to fully or partially refuse, at its sole discretion, at any time and without prior notice, access to any user to the Supplier Portal if one or several of the circumstances described in this clause are fulfilled.<br />
5.	USER IDENTIFIERS AND PASSWORD <br />
The COMPANY shall request users to log in to access certain services or information contained in the Supplier Portal by choosing a password that will enable the personal identification of the user ("name" and "password"). The access codes assigned shall be personal and non-transferable. Transfer thereof, even temporary, to third parties is prohibited. In this regard, users undertake to make diligent use thereof and to keep the password(s) and username(s) assigned, as the case may be, to access the Supplier Portal, confidential. In the event that a user becomes aware of or suspects the loss, theft or use of his or her password by third parties, he or she shall notify the COMPANY of said circumstance in the shortest possible time. Users shall be ultimately responsible for the costs, damages and losses arising from the access to and use of the Supplier Portal by any third party that uses the user's password(s) and username(s) due to the non-diligent use or loss of these by the user.<br />
6.	PARTICULAR TERMS AND CONDITIONS<br />
These Particular Terms and Conditions are applicable to the orders placed by suppliers through the Supplier Portal, to which end the agreements entered into between the COMPANY and the corresponding supplier shall be applicable to everything not envisaged therein.  <br />
As proof of the awareness and acceptance of the General Terms and Conditions of this service, the supplier, prior to accessing the Supplier Portal for the first time, will have received the Particular Terms and Conditions together with their login details, in the version applicable to the Supplier Portal at any given time, and shall express their conformity therewith. If during the term of the agreements entered into between the COMPANY and the supplier changes are made to these General Terms and Conditions that significantly affect the rights and obligations of the parties, the new terms and conditions shall be notified to the supplier in advance, and shall automatically be understood to be accepted by the supplier if, after ten (10) days have elapsed from the date of notification of the changes by the COMPANY, it does not receive written opposition from the supplier or if the supplier makes use of the Portal accepting the aforementioned changes. The non-acceptance of the modifications proposed will allow the COMPANY to automatically terminate the agreement entered into with the supplier for the use of the Portal, whereupon the supplier may continue to place orders via the usual channels. <br />
The service use formalities are those described in these General Terms and Conditions, in addition to those indicated on screen during navigation. The user declares that he or she is aware of and accepts said required formalities to access the service. As established in articles 27 and 28 of Law 34/2002 on Information Society Services and Electronic Commerce, the supplier and the COMPANY agree to the non-application of the obligation of information prior and subsequent to the aforementioned provisions on considering the related documentation included in the agreements entered into between the COMPANY and the supplier and in the Supplier Portal.<br />
In the event of nonfulfillment of any of the obligations envisaged in these General Terms and Conditions and Particular Terms and Conditions, the supplier or the COMPANY shall notify said nonfulfillment to the nonfulfilling party, granting a period of fifteen (15) business days from the notification date in which to remedy said nonfulfillment. If said period elapses without having remedied the nonfulfillment, the other party may either execute or terminate this agreement, claiming damages in both cases. If termination is preferred, both the supplier and the COMPANY accept that simple notification shall be sufficient for said termination to take full effect.
This agreement shall be of indefinite duration provided that there is no written withdrawal therefrom by any of the parties, with at least one (1) month's notice. The withdrawal procedure by sending an email writing "Deregistration from the portal" on the subject line to soporte@proveedoresmahou-sanmiguel.com. If remittance of an email is not possible, the request shall be sent via fax to the number +34 91 296 20 25, indicating the supplier's basic identification details.<br />

7.	INTELLECTUAL AND INDUSTRIAL PROPERTY  <br />
All the materials and information contained in the COMPANY's Supplier Portal shall be subject to current intellectual and/or industrial property legislation. Rights over materials and other elements displayed on the Supplier Portal (including, for merely illustrative and non-limiting purposes, drawings, texts, graphics, photographs, audio, video, software, distinctive signs, etc.) are the property of the COMPANY or, as the case may be, to third parties who have consented to the transfer thereof to the COMPANY. Likewise, the COMPANY is the owner of the proprietary logos, commercial names, domains and trademarks. <br />The materials and information uploaded into the COMPANY's Supplier Portal (photographs, audio, video, etc.) shall respect image and intellectual property rights (if any). The user shall be solely responsible for any claim filed against the COMPANY as a result of the use and dissemination of said materials and information. 

  <br />
Access, navigation, use, uploading and/or downloading of materials and/or use of the services contained in the Supplier Portal by the user shall in no case be deemed to be a waiver, transmission, licence or total or partial transfer of the aforementioned rights by the COMPANY or, as the case may be, by the owner of the corresponding rights. Consequently, users shall not delete, ignore or manipulate the copyright warning and any other data identifying the rights of the COMPANY or its respective owners incorporated to the contents and/or services, as well as the technical protection devices or any information and/or identification mechanisms that may be contained therein. In particular, the use of any materials or elements of the Supplier Portal for total or partial inclusion thereof in other websites outside of the Supplier Portal is strictly prohibited without the prior written authorisation of the Supplier Portal's owners. 

 <br />
References to commercial or registered names and trademarks, logos or other distinctive signs, whether owned by the COMPANY or third-party companies, is implicitly prohibited without the consent of the COMPANY or their legitimate owners. In no case, unless expressly stated otherwise, shall the access to or use of the Supplier Portal confer any rights on the user over the trademarks, logos and/or distinctive signs included therein and protected by law. <br />
All intellectual and/or property rights are reserved and, in particular, modifying, fixing, copying, reusing, exploiting in any way, reproducing, transforming, dubbing, subtitling, transferring, selling, renting, lending, publishing, making second or subsequent publications, uploading files, sending by email, transmitting, using, processing or distributing in any way all or part of the contents, elements and products, where applicable, included in the Supplier Portal for public or commercial purposes other than those that are the object of the provision by the supplier to the COMPANY is strictly prohibited without the express written authorisation of the COMPANY or, as the case may be, the corresponding rights owner. If the action or omission, fault or negligence directly or indirectly attributable to the Supplier Portal user that gives rise to the infringement of the intellectual and industrial property rights of the COMPANY or third parties, gives rise to damage, losses, joint and several obligations, costs of any nature, penalties, coercive measures, fines and other amounts arising from any claim, demand, action, lawsuit or proceeding, whether civil, criminal or administrative, the COMPANY shall be entitled to bring legal action against said user and claim any indemnity amounts, moral damages or damage to its reputation, consequential damages and loss of profit, advertising or any other costs by way of compensation, arising from penalties or judgments, late payment interests, cost of financing the total sum of the damages caused to the COMPANY, legal and defence costs (including prosecutors and lawyers) in any proceedings in which the COMPANY is sued for the previously expounded reasons, for the damages and losses arising from wrongful action or omission, notwithstanding the right to take any other action to which the COMPANY is entitled.<BR /> Any claims that could be filed by the user in relation to possible infringements of the intellectual or industrial property rights relating to the Supplier Portal shall be addressed to the COMPANY's Legal Advisory Department, located at Calle Titán 15, 28045 Madrid (Spain).<br />

8.	INFORMATION AND ELEMENTS OF THE SUPPLIER PORTAL<br />
a)	Company's corporate information <br />
The user is aware of and accepts that any data relating to the COMPANY or to the companies comprising said business group of an economic, financial and/or strategic nature (hereinafter, "corporate information") is provided solely for information purposes. The corporate information has been obtained from reliable sources. However, despite having taken reasonable measures to ensure that said information is truthful, actual and can reveal the COMPANY's corporate results, the COMPANY does not declare or guarantee that it is accurate, comprehensive or updated, and should not be relied upon in absolute terms. The corporate information that can be found in the Supplier Portal does not imply any kind of recommendation or investment and shall not be considered as such, or financial assessment of any kind, and no part of its content shall be taken as a basis for carrying out investments or making corporate decisions of any kind. 

<br />
Notwithstanding the foregoing, those users who decide to invest in the COMPANY's shares shall bear in mind that the value of their investment may undergo fluctuations either upwards or downwards,  and that they may not be able to recover the amount invested, either in whole or in part. Yields obtained in the past are not indicative of future yields. Exchange rates and fluctuations therein can affect the value of the investments. <br />
b) Information provided or published by users and/or third parties <br />
The Supplier Portal may include information or contents provided by sources other than the COMPANY, including information provided by the Supplier Portal users themselves. The COMPANY does not guarantee or assume any responsibility for the accuracy, integrity or exactness of such information and/or contents, including the cases expounded in the chapter on "User Liability for the Use of the Supplier Portal."<br />
Users shall not introduce, store or disseminate, through the Supplier Portal, any content or material that infringes intellectual or industrial property rights or, in general, any content which they do not have the right, pursuant to law, to reproduce, distribute, adapt, copy, fix or make it available to third parties. Information shall be understood to be elements or contents received by the user in the Supplier Portal, those received by any means, whether comments, suggestions or ideas, including those containing videos, texts, photographs, images, audio, software, etc. Said contents shall be considered to be transferred to the COMPANY free of charge, for the maximum time allowed and for everyone, and can be updated by the COMPANY within the limits established by the applicable legislation, there being no applicable obligation of confidentiality in relation to said contents or information, except the supplier's proprietary information. The authorisation for the COMPANY to use the user's brand image, without compensation, for commercial purposes is understood to be included in this transfer. The remittance by users of information that cannot be processed in this manner or that contains elements or contents owned by third parties. <br />
Due to the large amount of material that can be housed in the Supplier Portal, it is impossible for the COMPANY to verify the originality or non-infringement of third-party rights over the contents supplied by the user, who shall be ultimately responsible for all the effects of the infringements that could eventually be committed as a result of supplying said information. <br />
The COMPANY may modify the materials supplied by the users in order to adapt them to the formatting requirements of the Supplier Portal.
The COMPANY is not responsible for the use made by the user of the contents housed in the Supplier Portal and shall provide a space where they can upload their contents and share them with other users. Neither will it be responsible for controlling whether the contents infringe or do not infringe the rights mentioned in the preceding paragraphs.
<br />

Notwithstanding the provisions of these General Terms and Conditions, when contents are published by the user as a result of a request for Particular services of the Supplier Portal, the legal terms established in the Particular Terms and Conditions for each case shall prevail, and these General Terms and Conditions shall be complementary thereto.<br />
9.	LINKS OR HYPERLINKS TO THE SUPPLIER PORTAL<br />
Those users who wish to introduce links or hyperlinks from their own website to the COMPANY's Supplier Portal shall fulfil the terms and conditions set out below, the ignorance of which does not exempt the users from fulfilling the legal obligations arising therefrom:<br />
a)	The link or hyperlink shall only provide a link to the homepage or main page of the Supplier Portal, whereas it shall not reproduce it in any way (inline, links, deep links, browser or border environment, copy of texts, graphics, etc.).  <br />
b)	The establishment of frames of any kind that wrap the Supplier Portal or allow the visualisation of part or all of the Supplier Portal through Internet addresses other than that of the Supplier Portal and, in any case, that allow the visualisation of elements of the Supplier Portal jointly with contents outside of the Supplier Portal, in such a manner as to: (i) induce or be susceptible to error, confusion or deceit of the users about the truthfulness of the origin of the elements displayed or the services used; 
(ii) represent an act of disloyal comparison or imitation; (iii) serve to take advantage of the COMPANY's trademark and prestige; or (iv) in any other way, be prohibited by current legislation. 
<br />
c)	The page housing the link shall not carry out any type of false, inaccurate or incorrect manifestations about the COMPANY, its employees, clients or about the quality of the services it renders. 
d)	In no case shall the page housing the link suggest or state that the COMPANY has given its consent for inserting the link or otherwise sponsors, collaborates, verifies or supervises the publisher's services. <br />
e)	The use of any pictorial or mixed trademark or any other distinctive sign of the COMPANY on the publisher's website is prohibited except in the cases envisaged by law or expressly authorised by the COMPANY and provided that, in these cases, a direct link with the Portal is permitted in the manner established in this clause.  <br />
f)	The page establishing the link must faithfully comply with the law and cannot in any case lint to contents of its own or of third parties that:<br /> (i) are illegal, harmful or immoral and indecent (including for example, but not limited to pornography, violent, racist, etc.); <br />
(ii) induce or may induce in the user a false conception that the COMPANY subscribes, backs or adheres or in any supports the publisher's ideas, manifestations or expressions, whether legal or illegal; and<br /> (iii) may be inappropriate or irrelevant to the COMPANY's activity regarding the place, contents and subject matter of the publisher's website.<br />
g)	The authorisation to insert a link or hyperlink does not imply, in any case, the COMPANY's consent to reproduce the visual and functional aspects ("look and feel") of the COMPANY's Supplier Portal and/or contents. In particular, the authorisation to insert hyperlinks in the Supplier Portal shall be subject to the respect for human dignity and freedom. The website in which the hyperlink is established shall not contain unlawful information or content contrary to morality and good practices and to public order, neither shall it contain content contrary to any third-party rights. <br />
h)	The establishment of the link does not imply, in any case, the existence of a relationship between the COMPANY and the owner of the page in which it is established, nor the COMPANY's acceptance and approval of the contents or services offered therein and made available to the public. The COMPANY may request, at any time and without need to justify said request, the elimination of any link or hyperlink to the Supplier Portal, whereupon the website owner publishing the link to eliminate it immediately.
.<br />
10.	USE OF COOKIES  <br />
The COMPANY uses cookies to customise user navigation through its Supplier Portal. Cookies are files sent by a browser by means of a web server for maintaining the browsing session, storing their IP address (of their computer) and other possible navigation data. Beacons are electronic images that allow the Supplier Portal to count the number of visitors and users who have logged into the Supplier Portal and access certain cookies. These cookies allow the COMPANY's server to recognise the browser of the user's computer for the purpose of facilitating navigation and measuring the number of hits and traffic parameters (by means of beacons), and controlling the progress and number of logins, by using the information contained in the cookies in an unrelated manner with respect to any other existing user data. For further information on cookies, please read Clause 14 of the COMPANY's Privacy Policy. <br />
11.	WARRANTIES  <br />
The COMPANY declares that it has adopted all the necessary means, within the scope of its possibilities and the current state of the art, to guarantee the proper functioning of the Supplier Portal and to avoid the existence and transmission of viruses and other harmful or malicious components to users. If the user is aware of the existence of any unlawful or illegal content, contrary to copyright laws or which infringes intellectual and/or industrial property rights, he or she shall notify the COMPANY immediately so that it may adopt the relevant measures.<br />
12.	LIABILITIES  <br />
The COMPANY is released from all liability for damages and losses of any kind and nature in the following cases:<br />
a)	Impossibility or difficulty in the connection used to access the Supplier Portal, service interruptions, delays, errors and malfunctions thereof, regardless of the type of connection or technical means used by the user.  <br />
b)	Interruption, suspension or cancellation of the access to the Supplier Portal and for the availability and continued operation of the Supplier Portal or of the services and/or elements thereof, due to the interruption of the technical maintenance service of the Supplier Portal or for causes beyond the COMPANY's control or due to the services of the information service providers. <br />
c)	Malicious or negligent actions of the user or due to force majeure and any other causes beyond the COMPANY's control. <br />
d)	Specialised attacks by so-called "hackers" or third parties to the security or integrity of the computer system, provided that the COMPANY has adopted all the existing security measures within its technical possibilities. <br />
e)	Damages or losses caused to the information, contents, products and services provided, communicated, housed, transmitted, displayed or offered by third parties external to the COMPANY, including information society service providers, through a website which can be accessed through a link provided on this site. <br />
f)	Any loss or damage to the user's software or hardware arising from the access to the COMPANY's Supplier Portal or from the use of the information or elements contained therein. <br />
g)	Suitability, reliability, availability, timeliness or accuracy of the information or services contained in the Supplier Portal, or the direct or indirect damages caused in relation to the use of the information or elements contained therein. <br />
h)	Processing and subsequent use of personal data carried out by third parties external to the COMPANY, as well as the ownership of the information requested by said third parties. 
 <br />
The users of the Supplier Portal shall be personally liable for the damages and losses of any nature caused to the COMPANY, directly or indirectly, as a result of the nonfulfillment of any of the obligations arising from these General Terms and Conditions or, as the case may be, from the Particular Terms and Conditions. In any case, regardless of the cause, the COMPANY shall not assume any responsibility whatsoever, whether for direct or indirect damages, consequential damages and loss of profit. The user shall be solely responsible for the infringements incurred or the damages he or she causes to the Supplier Portal of the COMPANY, which is exonerated from any liability. The user is solely responsible for any claim or legal, court or out-of-court proceedings initiated by third parties against the COMPANY or against the user on the grounds of the use of the service or for the information sent to the COMPANY by any means. The user assumes all the expenses, costs and indemnities incurred by the COMPANY arising from such claims or legal actions. <br />
The COMPANY places a series of links, banners or other type of links that could give the user access to third-party websites at the user's disposal. Access to other third-party websites through said connections or links shall be provided under the users' sole responsibility. The COMPANY shall not be responsible, in any case, for the damages or losses arising from said use or activities. 
<br />
a)	For the damages or losses caused by the information, contents, products and services provided, communicated, housed, transmitted, displayed or offered by third parties external to the COMPANY, including information society service providers, through a website that can be accessed via a link on that site. <br />
b)	For any damage or loss in the user's software or hardware arising from the access to the COMPANY's Supplier Portal or from the use of the information or applications contained therein. <br />
c)	For the suitability, reliability, availability, timeliness or accuracy of the information or services of the Supplier Portal, or for the direct or indirect damages related to the use of the information or applications contained therein.
<br />
The COMPANY publishes its contents in Spain. Given the "non-territorial" nature of Internet connections, the COMPANY does not guarantee the suitability or availability of the Supplier Portal outside Spain. Should any or all of the contents or elements housed in the COMPANY's Supplier Portal be considered illegal in other countries, access thereto and use thereof by the users is prohibited and, in the event that these occur, shall be exclusively under the users' responsibility, whereupon the users undertake to fulfil and comply with the applicable laws of these countries.<br />
13.	GENERAL <br />
The access to, contents and services offered through the Supplier Portal are initially indefinite, unless established otherwise in the General Terms and Conditions, the Particular Terms and Conditions or the legislation applicable at any given time. However, the COMPANY reserves, without need for advanced notice and at any time, the right to suspend, refuse or temporarily or definitively restrict access to the Supplier Portal, to make the changes it deems relevant to the Supplier Portal, services or information offered, to the presentation or location thereof and to the General Terms and Conditions without having to indemnify the user in any way. 

<br />
Any clause or provision of these General Terms and Conditions that is or is deemed to be illegal, invalid or unenforceable shall be excluded and replaced by another similar one, without affecting or altering the other provisions, which shall not be affected by any illegal, invalid or unenforceable clause or provision, but rather shall be completely valid. The COMPANY excludes any type of warranty and, therefore, is released from all liability arising from the preceding points and from other aspects that may not be envisaged herein. <br />
All the information received in the Supplier Portal shall be deemed to be transferred to the COMPANY free of charge. Email or electronic mail shall not be deemed to be a valid means for filing content-related claims. For such purpose, users shall write to the COMPANY's Legal Advisory Department, located at Calle Titán 15, 28045 Madrid (Spain), which will indicate the procedure to be followed.<br />

These General Terms and Conditions are governed by Spanish law. The parties freely subject themselves, for conflict resolution and waiving any other jurisdiction, to the courts and tribunals of the City of Madrid. 

			</p>
            <p style="text-align:right"><a href="docs/MSM_General_Terms_and_Conditions.pdf" class="bt_registro" target="blank">Print</a> </p>
<div class="caja_acepto">


<!--<form action="" method="post" style="width:400px;">//-->
<form name="frmAlta" id="frmAlta" method="post" style="width:400px;">
  <span id="sprycheckbox1">
<label>
  <input type="checkbox" name="opcion1" id="opcion1" required>
</label>
<span class="checkboxRequiredMsg">Select to continue.</span></span><span class="rojo">I have read and accept</span> the <a href="docs/MSM_General_Terms_and_Conditions.pdf" target="_blank" class="rojo">General terms and conditions of use of the Supplier Portal</a> and the <a href="docs/MSM_Privacy_Policy.pdf" target="_blank" class="rojo">Privacy Policy</a>.<br />
<br />
<span id="sprycheckbox2">
<label>
  <input type="checkbox" name="opcion2" id="opcion2" required>
</label>
<span class="checkboxRequiredMsg">Select to continue.</span></span><span class="rojo">I accept the transfer of the data </span> provided to the companies of the Mahou-San Miguel Group, [<a href="docs/MSM_Data Protection Clause.pdf" target="_blank" class="rojo">Data Protection Clause</a>], for the same purpose.<br />
<br />
<span id="sprycheckbox3">
<label>
  <input type="checkbox" name="opcion3" id="opcion3" required>
</label>
<span class="checkboxRequiredMsg">Select to continue.</span></span><span class="rojo"> I agree to receive information </span>related to the supplier portal by mail.<br />
<br />
<button type="button" class="bt_registro" id="submit1" name="submit1">Register</button>
<br>
</form>


</div>
<div style="clear:both;"></div>
    	</div>    
    </div>
</div>
<script type="text/javascript">
// <!--
// var sprycheckbox1 = new Spry.Widget.ValidationCheckbox("sprycheckbox1");
// var sprycheckbox2 = new Spry.Widget.ValidationCheckbox("sprycheckbox2");
// var sprycheckbox3 = new Spry.Widget.ValidationCheckbox("sprycheckbox3");
// //-->
$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked') && $('#opcion2').is(':checked') && $('#opcion3').is(':checked')) {
        $('#frmAlta').submit();
        alta();
    }
});
</script>
 			
</body>

</html>
