﻿<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="../estilos.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">



<title>Portal de proveedores</title>
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
}
-->
</style>
</head>

<script>
dibujaMenu(1)
</script>

<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function Init() {
    document.getElementById('tablemenu').style.display = 'block';

    MM_preloadImages('../images/icono_docs_sm.gif', '../images/icono_docs_sm.gif')

    p = window.top.document.getElementById("frSet")
    vRows = p.rows
    vArrRows = vRows.split(",")
    vRows = vArrRows[0] + ",*,0"
    p.rows = vRows
}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">
<!-------------------------- BEGIN COPYING THE HTML HERE ----------------------------> 
<table width="100%" border="0" cellpadding="5" cellspacing="2" bordercolor="0" hspace="0" vspace="0">
  <tr> 
    <td width="12" rowspan="3" align="right" valign="top" bordercolor="0"> <div align="left" ></div>
    </td>
    <td height="74" colspan="2" align="left" valign="middle" class="rojo_24">Bienvenido/a </td>
    <td colspan="2" align="left" valign="bottom">&nbsp;</td>
  </tr>
  <tr>

    <td rowspan="2" valign="top" bgcolor="#EEEEEE" class="contenido_texto arial">
	<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>

    Puede acceder a las distintas &aacute;reas a trav&eacute;s de las opciones de men&uacute; situadas en la parte superior.
      <ul>
        <li><span><b>Calidad:</b> Acceda a contestar los <b>formularios</b> de petición de información requerida por Mahou-San Miguel.</span></li>
      </ul>
        <ul>
        <li><span><b>Solicitudes de oferta:</b> Acceda a las solicitudes de oferta recibidas y gesti&oacute;nelas desde este mismo apartado.</span></li>
      </ul>
      <ul>
        <li><span><b>Sus datos :</b> Para modificar sus datos personales, pulse sobre la opci&oacute;n &quot;Modificar datos personales&quot;. Puede modificar tambi&eacute;n su configuraci&oacute;n personal de formatos desde &quot;Opciones de usuario&quot;.</span></li></ul>
      
       <ul>
        <li><span><b>Otras acciones</b>:</span> </li>
        <ul>
        <li><span><b> Pedidos/Facturas/Datos básicos:</b> Acceda a conocer el estado de sus pedidos, facturas y datos básicos de su compañía. </span></li>
      
	       
        <li><span><b>Gestión prevención</b>: Acceda al sistema de gestión de prevención, para aquellas prestaciones de servicio que así lo requieran. </span></li>
      </ul></ul>
      <ul>
        <li><span><b>Definiciones :</b></span> </li>
        <ul>
        <li><span><strong>Proveedor registrado</strong>: Proveedor registrado en el Portal.</span>
        </li>
        <li><span><strong>Proveedor prehomologado</strong>: Proveedor registrado en el Portal y que ha cumplimentado con éxito los formularios con petición de información encaminados a conocer si, a nivel general, su compañía y productos o servicios cumplen con los estándares de Mahou-San Miguel para poder ser invitado a procesos de licitación o petición de ofertas.</span>
        </li>
        <li><span><strong>Proveedor homologado</strong>: Proveedor prehomologado que ha obtenido alguna adjudicación de pedido por parte de Mahou-San Miguel.</span></li>
        </ul>
      </ul>

      
Si es la primera vez que va a realizar una oferta a trav&eacute;s del portal, siga atentamente los siguientes pasos:<br>
<br>
<b>1. Pulse en "solicitudes de oferta &quot;</b> para ver las peticiones de ofertas que tiene abiertas su empresa.<br>
<br>
<b>2. Seleccione la solicitud de oferta</b> a la que quiera responder,<b> pulsando sobre el c&oacute;digo de la misma. </b><br>
<br>
<b>3. Realice su oferta completando toda la informaci&oacute;n necesaria: </b>desde el &aacute;rbol de navegaci&oacute;n que encontrar&aacute; en la parte izquierda, podr&aacute; desplazarse por los distintos apartados que conforman la oferta. Para introducir los precios deber&aacute; ir al apartado "items/precios". Recuerde introducir el plazo de validez de la oferta, en el apartado "Datos Generales de la oferta". <br>
<br>
<b>4. Comunique su oferta pulsando sobre el bot&oacute;n de enviar</b><b><IMG height=14 src="../images/sobre_tr.gif" width=30 align=absBottom></b>   </div>    </td>
  </tr>
</table>

	</td>

    <td rowspan="2" valign="top" >&nbsp;</td>
    <td colspan="2" align="left" valign="top"><table width="100%" border="0" class="textos">
        <tr>
          <td height="26" valign="top" class="textos"><span class="textos"><strong>INSTRUCCIONES</strong></span></td>
        </tr>
        <tr>
          <td height="62" class="textos">Descárguese las instrucciones 
      sobre cómo realizar una oferta, seguimiento, etc. </td>
        </tr>
        <tr>
          <td width="273" class="textos"><a href="docs/FSN_MAN_MSM_Cómo ofertar.pdf" class="textos" target="_blank"><img src="../images/punto_rojo.jpg" width="6" height="6" border="0"> Cómo ofertar</a></td>
        </tr>
        <tr>
          <td><a href="docs/FSN_MAN_MSM_Requisitos técnicos.pdf" class="textos" target="_blank"><img src="../images/punto_rojo.jpg" width="6" height="6" border="0"> Requisitos t&eacute;cnicos </a></td>
        </tr>
        <tr>
          <td><a href="docs/Requisitos%20t&#233;cnicos%20proveedor.pdf" class="textos" target="_blank"><img src="../images/punto_rojo.jpg" width="6" height="6" border="0"></a><a href="docs/FSN_MAN_MSM_Mantenimiento de datos.pdf" class="textos" target="_blank"> Mantenimiento de datos </a></td>
        </tr>
        <tr>
          <td><a href="docs/Requisitos%20t&#233;cnicos%20proveedor.pdf" class="textos" target="_blank"><img src="../images/punto_rojo.jpg" width="6" height="6" border="0"></a><a href="docs/MSM_GUI_Proveedor.pdf" class="textos" target="_blank"> Guía del Proveedor </a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
    </table>
	</td>
  </tr>
  <tr>
    <td align="left" valign="bottom"><table width="100%" height="220" border="0" background="../images/fondo.jpg">
  <tr>
    <td class="subtit"><div class="recordar-claves"><span class="rojo"><strong>Servicio de ayuda al uso del Portal.</strong></span><br />
                <span class="rojo_24"><strong>902 043 065</strong></span>
            </div></td>
      </tr>
      <tr>
        <td height="21"><div align="center"><a href="mailto:atenciontecnicaproveedores@mahou-sanmiguel.com" class="textos">soporte@proveedoresmahou-sanmiguel.com</a></div></td>
      </tr>
    </table>
      <div align="center"></div></td>
  </tr>
</table>
    </td>
  </tr>

</table>
</body></html>
