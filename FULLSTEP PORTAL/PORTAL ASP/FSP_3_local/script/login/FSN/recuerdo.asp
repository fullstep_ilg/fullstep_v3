﻿<%@  language="VBScript" %>
<!--#include file="../recuerdo.asp"-->
<html>
<head>
    <link href="estilos.css" rel="stylesheet" type="text/css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;
        window.close();
    };
</script>
<body>
    <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
    <input type="hidden" name="idioma" value="spa">
    <table width="620" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="20">
                &nbsp;
            </td>
            <td width="615" align="left">
                <img src="images/fullstep_logo.jpg" width="200" height="43">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width="20">
                &nbsp;
            </td>
            <td width="615" align="left">
                <font color="#666666" size="1" face="Verdana" class="textos"><b><u>Recordatorio de claves
                    de acceso </u></b>
                    <br>
                    <br>
                    Si ha olvidado sus claves de acceso, introduza la direcci&oacute;n de e-mail registrada
                    en el portal, y a continuaci&oacute;n recibir&aacute; un e-mail con sus claves.<br>
                    <br>
                    <table class="textos">
                        <tr>
                            <td>Cód. Compañia:</td>
                            <td><input type="text" name="txtCia" id="txtCia" size="80" maxlength="100"/></td>
                        </tr>
                        <tr>
                            <td>Cód. Usuario:</td>
                            <td><input type="text" name="txtUsu" id="txtUsu" size="80" maxlength="100"/></td>
                        </tr>
                        <tr>
                            <td>Dirección de email:</td>
                            <td><input type="text" name="txtEmail" id="txtEmail" size="80" maxlength="100"/></td>
                        </tr>
                    </table>
                    <br />
                    <input type = submit value="Enviar" name=cmdEnviar id=cmdEnviar></nobr>
                    <br>
                    <br>
                    <br>
                    Si continúa con dificultades para acceder al portal o su dirección de email ha cambiado,
                    consulte con el servicio de atención al cliente. Tlfno.: 902 996 926 (Horario de
                    atenci&oacute;n de lunes a jueves de 9 a 18 hrs. y viernes de 8 a 14 hrs.) </font>
            </td>
        </tr>
    </table>
</body>
</html>
