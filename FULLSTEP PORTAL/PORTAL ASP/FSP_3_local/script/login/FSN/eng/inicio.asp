﻿<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../estilos.css" rel="stylesheet" type="text/css">


<title>::Fullstep Networks - Sistemas y Gestión de Compras::</title>
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
}
-->
</style>
</head>

<script>
dibujaMenu(1)
</script>

<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}

/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function Init() {
    document.getElementById('tablemenu').style.display = 'block';

    MM_preloadImages('../images/icono_docs_sm.gif', '../images/icono_docs_sm.gif')

    p = window.top.document.getElementById("frSet")
    vRows = p.rows
    vArrRows = vRows.split(",")
    vRows = vArrRows[0] + ",*,0"
    p.rows = vRows
}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">
<!-------------------------- BEGIN COPYING THE HTML HERE ----------------------------> 
<table width="100%" border="0" cellpadding="5" cellspacing="2" bordercolor="0" hspace="0" vspace="0">
  <tr> 
    <td width="12" rowspan="3" align="right" valign="top" bordercolor="0"> <div align="left" class="textos"></div>
    </td>
    <td height="74" colspan="2" align="left" valign="middle"><font size="2" face="verdana" class="titulo">Welcome</font></td>
    <td colspan="2" align="left" valign="bottom">&nbsp;</td>
  </tr>
  <tr>
    <td rowspan="2" valign="top" bgcolor="#EEEEEE" class="textos">
	<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="textos">To navigate through the different areas of the portal, use the navigation bar in the topside of the page. From here you will be able to access:
      <ul>
        <li><b>Request for quotations:</b> access the RFQ and other requests sent by the company. </li>
      </ul>
      <ul>
        <li><b>Your details :</b> update all the personal information stored in the application, and configure the formats (number of decimal places, date formats, etc) that you want the application to display.</li>
      </ul>
      <ul>
        <li><b>Your company:</b> update the general details about your company, point out the materials and services your company can supply and register new users of your company inside the Portal. </li>
      </ul>
      <br>
If this is the first time you are posting an offer through the portal, please follow these steps:<br>
<br>
<blockquote>
  <ol>
    <li> <strong>Click on &ldquo;Requests for quotation&rdquo;</strong> to display the requests for quotations pending for your company. <br>
        <br>
    <li> <strong>Select the request for quotation</strong> for which you want to send an offer <strong>by clicking on its code</strong>.<br>
        <br>
    <li> <strong>Configure your offers</strong> by filling in the appropriate data. From the navigation tree on the left side of the screen, you will be able to navigate through the different sections of the offer. To fill in the prices, click on the &ldquo;items/prices&rdquo; section. Don&rsquo;t forget to fill in the validity dates of your offer in the &ldquo;General data of the offer&rdquo; section. <br>
        <br>
    <li> <b>Post your offers by clicking on Send </b><img src="../images/sobre_tr.gif" align="absbottom" width="30" height="14"> </li>
  </ol>
</blockquote></td>
  </tr>
</table>

	</td>
    <td rowspan="2" valign="top" class="textos">&nbsp;</td>
    <td colspan="2" align="left" valign="top"><table width="100%" border="0" class="textos">
        <tr>
          <td height="26" valign="top" class="textos"><span class="textos"><strong>INSTRUCTIONS</strong></span></td>
        </tr>
        <tr>
          <td height="62" class="textos">You can download the following tutorials</td>
        </tr>
        <tr>
          <td width="273" class="textos"><a href="docs/C&#243;mo%20ofertar.pdf" class="textos" target="_blank"><img src="../images/circulo.jpg" width="13" height="15" border="0"> How to offer </a></td>
        </tr>
        <tr>
          <td><a href="docs/Requisitos%20t&#233;cnicos%20proveedor.pdf" class="textos" target="_blank"><img src="../images/circulo.jpg" width="13" height="15" border="0"> Technical requirements </a></td>
        </tr>
        <tr>
          <td><a href="docs/Requisitos%20t&#233;cnicos%20proveedor.pdf" class="textos" target="_blank"><img src="../images/circulo.jpg" width="13" height="15" border="0"></a><a href="docs/Mantenimiento%20datos.pdf" class="textos" target="_blank"> Master data </a></td>
        </tr>
        <tr>
          <td>&nbsp;   </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
    </table>
	</td>
  </tr>
  <tr>
    <td align="left" valign="bottom"><table width="100%" height="220" border="0" background="../images/fondo.jpg">
  <tr>
    <td class="subtit"><div align="center"><img src="../images/img4.jpg" width="195" height="146"></div></td>
  </tr>
  <tr>
    <td valign="bottom" class="textos"><table width="35%" border="0" align="center"  cellpadding="0" cellspacing="0" class="subtexto">
      <tr>
        <td height="21"><div align="center">Supplier call centre </div></td>
      </tr>
      <tr>
        <td height="21"><div align="center">Tel. +34 902 996 926 </div></td>
      </tr>
      <tr>
        <td height="21"><div align="center"><a href="mailto:atencionalcliente@fullstep.com" class="textos">atencionalcliente@fullstep.com</a></div></td>
      </tr>
    </table>
      <div align="center"></div></td>
  </tr>
</table>
    </td>
  </tr>

</table>
</body></html>
