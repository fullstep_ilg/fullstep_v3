﻿<%@  language="VBScript" %>
<!--#include file="../../recuerdo.asp"-->
<html>
<head>
    <link href="../estilos.css" rel="stylesheet" type="text/css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body>
    <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
    <input type="hidden" name="idioma" value="eng">
    <table width="700" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td height="100" colspan="2" background="../images/fondo_L.gif">
                <div align="right">
                    <a href="http://www.fullstep.com" target="_blank">
                        <img src="../images/logo_small.jpg" width="200" height="43" border="0"></a></div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width="20">
                &nbsp;
            </td>
            <td width="615" align="left">
                <font color="#666666" size="1" face="Verdana" class="textos">
                    <br>
                    <br>
                    If you forgot your login data, fill in the registered e-mail address and you will
                    receive an e-mail with your login and password.<br>
                    <br>
                    <table class="textos">
                        <tr>
                            <td>Company code:</td>
                            <td><input type="text" name="txtCia" id="txtCia" size="80" maxlength="100"/></td>
                        </tr>
                        <tr>
                            <td>User Code:</td>
                            <td><input type="text" name="txtUsu" id="txtUsu" size="80" maxlength="100"/></td>
                        </tr>
                        <tr>
                            <td>E-mail address:</td>
                            <td><input type="text" name="txtEmail" id="txtEmail" size="80" maxlength="100"/></td>
                        </tr>
                    </table>

                    <br />
                    <input type ="submit" value="Submit" name="cmdEnviar" id="cmdEnviar"/></nobr>
                    <br>
                    <br>
                    <br>
                    If you have problems while accessing the portal, or your e-mail address has changed
                    call the Supplier call centre,<br>
                    Phone +34 902 996 926 </font>
            </td>
        </tr>
    </table>
</body>
</html>
