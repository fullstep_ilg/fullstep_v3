﻿<%@ Language=VBScript %>
<!--#include file="../../../common/XSS.asp"-->
<html>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>::Fullstep Networks ::</title>

</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo m�ximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=GER", "_blank", "width=730,height=635")
window.close()

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=550,height=600,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>


<body bgcolor="#FFFFFF" topmargin="5" leftmargin="5">
<form name="frmAlta" id="frmAlta" method="post">

<table border="0" width="100%">
  <tr>
    <td width="100%">        <p align="left"> <font size="1" face="Verdana"><b><font color="#666666">Um mit dem Anmeldungsprozess fortzufahren, bitte &quot;Akzeptieren&quot; dr&uuml;cken:</font> 
          </b> </font></p>        
      <table border="0">
        <tr>
            <td width="50%" height="109" valign="bottom"> 
              <p align="left"><font color="#666666" size="1" face="Verdana"><img src="../images/fullgiro_web_blanco.gif" WIDTH="40" HEIGHT="40"> 
            </font></td>
          <td width="50%" rowspan="2"><font color="#666666" size="1" face="Verdana">
          <textarea readonly="true" rows="11" name="S1" cols="63" style="font-family: Verdana; font-size: 8pt; text-align:justify; display:block; height:135px; line-height: 150%; list-style-type: lower-alpha">
Nutzungsbedingungen f&uuml;r die FULLSTEP Sourcing-Plattform:

Vorbemerkung: Im Folgenden wird das Wort Lieferant auch f&uuml;r solche Unternehmen verwendet, die aktuell keine Lieferanten von Huf sind, sondern sich lediglich um die Aufnahme einer Gesch&auml;ftsbeziehung bem&uuml;hen.

Unternehmen die f&uuml;r FULLSTEP NETWORKS als Lieferanten aktiv sind oder sich als solche bewerben m&ouml;chten, k&ouml;nnen &uuml;ber das Zuliefererportal zuk&uuml;nftig an Ausschreibungen teilnehmen.

Vorraussetzung daf&uuml;r ist bei neuen Lieferanten eine Selbstauskunft &uuml;ber die Daten des Unternehmens. 

Es besteht seitens des Lieferanten kein Rechtsanspruch auf die Beteiligung an Ausschreibungen. 

FULLSTEP beh&auml;lt es sich vor, nach eigenen Kriterien auszuw&auml;hlen und Lieferanten abzulehnen oder von der Plattform auszuschlie&szlig;en. 

F&uuml;r das Zuliefererportal wird pro Unternehmen ein Hauptbenutzer eingef&uuml;hrt.

Dieser &uuml;bernimmt, als koordinierende Stelle f&uuml;r das Unternehmen, die Zulassung und L&ouml;schung von weiteren Benutzern.

Die Pflege von Benutzerdaten, die Zulassung und L&ouml;schung von Benutzern sowie die Pflege der Daten &uuml;ber das Unternehmen (Adressen, Warengruppen, etc.) geschieht damit in Eigenverantwortung des einzelnen Lieferanten durch den Hauptbenutzer. 

Der Hauptbenutzer handelt im Namen des Unternehmens, dieses verpflichtet sich die Daten fortlaufend aktuell zu halten.

Die Zugangsdaten f&uuml;r diesen Hauptbenutzer-Zugang werden dem Lieferanten mitgeteilt. 

Sie sind vertraulich zu behandeln, gleiches gilt f&uuml;r die Zugangsdaten der weiteren Benutzer. Insbesondere ist zu beachten, dass Zugangsdaten nicht ver&ouml;ffentlicht werden und nur einem beschr&auml;nkten, unmittelbar an der Gesch&auml;ftsbeziehung beteiligten Personenkreis zug&auml;nglich sind. 

Kennw&ouml;rter sind hinreichend komplex zu w&auml;hlen und regelm&auml;&szlig;ig zu &auml;ndern.

Alle im Rahmen der Gesch&auml;ftsbeziehung ausgetauschten Informationen und Dokumente sind strikt vertraulich zu behandeln.

Zeichnungen, Beschreibungen, Spezifikationen und &Auml;hnliches d&uuml;rfen nur im Rahmen der betrieblichen Erfordernisse gespeichert und vervielf&auml;ltigt werden.

Bei Weitergabe von Daten an Dritte durch Lieferanten (z.B. an deren Unterlieferanten) ist eine entsprechende Verpflichtung zur Vertraulichkeit sicher zu stellen.

F&uuml;r die st&auml;ndige, fehlerfreie Verf&uuml;gbarkeit der elektronischen Systeme wird keine Gew&auml;hr geleistet. Huf beh&auml;lt es sich vor, den Betrieb der Plattform jederzeit einzuschr&auml;nken oder einzustellen.

F&uuml;r die Zugangsm&ouml;glichkeit zur Plattform &#40;Internetzugang&#41; ist der Lieferant selbst verantwortlich und es werden keinerlei Kosten hierf&uuml;r erstattet.

Alle Beteiligten verpflichten sich, die erforderlichen Sicherheitsvorkehrungen nach dem jeweiligen Stand der Technik vorzunehmen.

Der Lieferant verpflichtet sich, keine Dokumente, Dateien und Daten in die Plattform einzustellen die den Betrieb gef&auml;hrden und/oder beeintr&auml;chtigen k&ouml;nnten. 

Insbesondere sind Vorkehrungen zu treffen, um keine Computerviren zu verbreiten.

Elektronisch &uuml;bermittelte Willenserkl&auml;rungen haben volle G&uuml;ltigkeit, wie auch Erkl&auml;rungen per Fax oder per Telefon rechtsverbindlich sind.

F&uuml;r die vertraglichen Bestandteile sowie f&uuml;r die Angebotsabgabe gelten nur und ausschlie&szlig;lich die vom jeweiligen Lieferanten in die Plattform eingestellten Angaben. Etwaige schriftliche Nachtr&auml;ge haben keinerlei Geltung, es sei denn, diese werden von Huf schriftlich r&uuml;ckbest&auml;tigt. 

Mit der Anmeldung erkennt das Lieferanten-Unternehmen die oben aufgef&uuml;hrten Bedingungen an.

</textarea>
          </font>
          </td>
        </tr>
        <tr>
          <td valign="top"><div align="left"><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>FULLSTEP NETWORKS, S.L. </strong></font></div></td>
        </tr>
        </table>
      <p><font face="Verdana" size="1">
    <b>Akzeptieren Sie die Aufnahmebedingungen f&uuml;r das Einkaufsportal von FULLSTEP NETWORKS, S.L.?<br>
    <br>
          </b> Wenn Sie &quot;Nicht akzeptieren&quot; dr&uuml;cken, kann Ihre Firma nicht als autorisierter Lieferant von FULLSTEP NETWORKS registriert werden.<br> 
Um mit dem Anmeldungsprozess fortzufahren, bitte &quot;Akzeptieren&quot; dr&uuml;cken. <br>
Wenn Sie die Bedingungen drucken m&ouml;chten, bitte den Knopf &quot;Drucken&quot; dr&uuml;cken.<br>
<br>
      </font>
    
      <div align="right">
      <table border="0" width="37%">
        <tr>
          <td width="41%">
            <p align="center">
            <input type="submit" onclick="return alta()" value="Akzeptieren" id=submit1 name=submit1>
            </p>
          </td>
          <td width="38%">
          <input type="button" onclick="no_alta()" value="Nicht Akzeptieren" id=button1 name=button1>
          </td>
          <td width="21%">
          <input type="button" onclick="imprimir()" value="Drucken" id=button2 name=button2>
          </td>
        </tr>
      </table>
      </div>
        <p align="center"><font face="Verdana" color="#666666" size="1">copyright 
          &copy; 2004 FULLSTEP NETWORKS, S.L.</font> 
        <hr width="100%" size="1" color="#666666">
    </td>
  </tr>
</table>

	<input type="hidden" name="ACEPTADO">
	
</form>

</body>

</html>
