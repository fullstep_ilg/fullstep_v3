﻿<%@  language="VBScript" %>
<!--#include file="../../recuerdo.asp"-->
<html>
<head>
    <link href="../estilos.css" rel="stylesheet" type="text/css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
    <style type="text/css">
<!--
.Estilo1 {font-size: large}
-->
</style>
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body>
    <form name="frmRecuerdo" id="frmRecuerdo" method="post">
    <input type="hidden" name="idioma" value="ger">
    <table width="635" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="20">
                &nbsp;
            </td>
            <td width="615" align="left" class="titulo Estilo1 Estilo1">
                Demo-Portal f&uuml;r Einkauf und Beschaffung
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width="20">
                &nbsp;
            </td>
            <td width="615" align="left">
                <font color="#666666" size="1" face="Verdana" class="textos"><b><u>Übermittlung der
                    Zugangsdaten </u></b>
                    <br>
                    <br>
                    Wenn Sie Ihre Login-Daten vergessen haben, geben Sie bitte Ihre E-Mail Adresse an,
                    die Sie bei der Registrierung verwendet haben. Wir senden Ihnen dann umgehend Ihre
                    Login-Daten zu.<br>
                    <br>
                    <table class="textos">
                        <tr>
                            <td>Firmencode:</td>
                            <td><input type="text" name="txtCia" id="txtCia" size="80" maxlength="100"/></td>
                        </tr>
                        <tr>
                            <td>Benutzercode:</td>
                            <td><input type="text" name="txtUsu" id="txtUsu" size="80" maxlength="100/"></td>
                        </tr>
                        <tr>
                            <td>E-Mail Adresse:</td>
                            <td><input type="text" name="txtEmail" id="txtEmail" size="80" maxlength="100"/></td>
                        </tr>
                    </table>

                    <br />
                    <input type = submit value="Absenden" name=cmdEnviar id=cmdEnviar>
                    <br>
                    <br>
                    <br>
                    Wenn Sie noch Schwierigkeiten mit dem Zugang zum Portal haben oder eine neue E-Mail
                    Adresse nutzen, schicken Sie uns bitte eine E-Mail an: <a href="mailto:compras@fullstep.com">
                        compras@fullstep.com</a></font>
            </td>
        </tr>
    </table>
</body>
</html>
