﻿<%@ Language=VBScript %>
<!--#include file="../../login.asp"-->
<html>
<head>
<%						
''' <summary>
''' Pantalla inicial de la personalización, es la q llama a la pantalla de login
''' </summary>
''' <remarks>Llamada desde: registro/registrarproveedor.asp		proveedor/default.asp
'''		login/personalizacion/login.asp		; Tiempo máximo: 0,1</remarks>

Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if


If Idioma="" then
	Idioma="SPA"
end if


Dim Den


den=devolverTextos(Idioma,1 )


lblIdioma= Den(1)
lblCia= Den(2) 
lblUsuario= Den(3)
lblPwd=  Den(4) 
cmdEntrar =Den(5)


%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<style type="text/css">
<!--
.Estilo3 {color: #FFFFFF}
-->
</style><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>::Fullstep Networks - Sistemas y Gestión de Compras::</title>
<link href="../estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: 5px;
	margin-top: 0px;
	background-image:url(../images/fondo.gif);
}
.Estilo1 {font-size: large}
.Estilo2 {	font-size: 12px;
	font-weight: bold;
	font-style: italic;
}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function ventanaLogin (IDI){

   window.open ("registro.asp?Idioma="+IDI,"","width=700,height=400,resizable=yes")

}
function ventanaSecundaria (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}

function recuerdePWD()
{
   window.open("recuerdo.asp","_blank","width=641,height=280,scrollbars=NO")
}
//-->
</script>
</head>

<body onLoad="MM_preloadImages('../images/imagen%20_chica.jpg','../images/imagen%20_carros.jpg','../images/imagen%20_mundo.jpg','../images/imagen_cuadro.gif','../images/deutsch_r.gif','../images/english_r.gif','../images/contact_r.gif','../images/entrar2f_spa.gif','../images/espa&ntilde;ol_r.gif','../images/tutorials_r.gif','../images/contact_r.gif');MM_preloadImages('images/aviso%20legal_r.gif')">
		<form name="frmLogin" id="frmLogin" method="post" action="login.asp">
		<input type="hidden" id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
		<input type="hidden" id="CiaComp" name="CiaComp" value="<%=CiaComp%>">						
		<input type="hidden" name="Idioma" value="<%=Idioma%>">
<table width="802" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <!--DWLayoutTable-->
  <tr>
    <td rowspan="2"><img src="../images/logo.jpg" name="Image5" width="150" height="89" id="Image5"></td>
    <td width="355" height="79"></td>
    <td width="136"></td>
    <td width="150">&nbsp;</td>
    <td width="4"></td>
    <td width="1"></td>
  </tr>
  <tr>
    <td height="10"></td>
    <td></td>
    <td colspan="3" align="right" valign="bottom"><table width="145" border="0" align="right" cellpadding="0" cellspacing="0">
      <!--DWLayoutTable-->
        <tr>
          <td><a href="javascript:ventanaSecundaria('../../../../CUSTOM/fulldemo/public/ger/manuales.htm')" onMouseOver="MM_swapImage('Image8','','../images/hilfe_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../images/hilfe.gif" name="Image8" width="77" height="10" border="0" id="Image8"></a></td>
          <td><a href="mailto:info@fullstep.com" onMouseOver="MM_swapImage('Image2','','../images/kontakt_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../images/kontakt.gif" name="Image2" width="77" height="10" border="0" id="Image2"></a></td>
          <td width="1"></td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td height="1" colspan="5" align="center"><img src="../images/linearoja.gif" width="800" height="1"></td>
    <td></td>
  </tr>
  <tr>
    <td width="156" height="19"></td>
    <td colspan="2"></td>
    <td></td>
    <td>&nbsp;</td>
    <td></td>
  </tr>
  <tr>
    <td width="156" height="145" valign="top"><img src="../images/imagen%20_mundo.jpg" name="Image9" width="145" height="145" border="0" usemap="#Map" id="Image9"></td>
    <td colspan="4" rowspan="2" valign="top"><table width="645" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="20">&nbsp;</td>
          <td width="615" align="left" class="titulo">Portal f&uuml;r Einkauf und Beschaffung </td>
        </tr>
        <tr>
          <td width="20" height="223"></td>
          <td width="625" rowspan="2"><table width="638" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td height="5" align="right"><div align="left"><span class="textos"><br>
                </span></div></td>
                <td height="5" align="right">&nbsp;</td>
                <td colspan="2"><div align="center"></div></td>
              </tr>
              <tr>
                <td rowspan="3" valign="top"><font color="#666666" size="1" face="Verdana" class="textos">
                  
Als ein auf den Bereich Einkauf und Beschaffung spezialisiertes Unternehmen arbeiten wir bei FULLSTEP daran, eine fl&uuml;ssige und effiziente Beziehung zwischen Lieferanten und K&auml;ufern herzustellen. <br>
<br>
Hierzu stellen wir unseren Lieferanten einen bequemen, transparenten und agilen Kommunikationskanal zur Einkaufsabteilung zur Verf&uuml;gung, wobei ein vollst&auml;ndiger Datenschutz bei der Bearbeitung der Einkaufsprozesse sichergestellt ist.</font> </td>
                <td width="21" rowspan="3" align="right">&nbsp;</td>
                <td colspan="2" align="left" bgcolor="#E4E4E4"><div align="center"><img src="../images/lieferanten_r.gif" alt="Geben Sie Ihre Zugangsdaten an" name="Image4" height="14" border="0" id="Image4"> </div></td>
              </tr>
              <tr>
                <td height="123" colspan="2" align="left" bgcolor="#E4E4E4"><div align="center">
                    <table width="173" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="16">&nbsp;</td>
                        <td width="67" class="formulario">Firmencode</td>
                        <td width="80">
					<input id="txtCia" name="txtCIA" maxlength="20" size="10">
                        </td>
                        <td width="10">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="16">&nbsp;</td>
                        <td width="67" class="formulario">Benutzercode </td>
                        <td width="80">
					<input name="txtUSU" maxlength="20" size="10">      
                        </td>
                        <td width="10">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="16">&nbsp;</td>
                        <td width="67" class="formulario">Passwort</td>
                        <td width="80">
					<input name="txtPWD" type="password" maxlength="20" size="10" autocomplete="off">      
                        </td>
                        <td width="10">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="16">&nbsp;</td>
                        <td width="67">&nbsp;</td>
                        <td width="80"><div align="center">
							<input type="hidden" name="cmdEntrar" value="<%=cmdEntrar%>">
							<input type="hidden" name="txtEntrar" value="<%=cmdEntrar%>">
							<input type="image" name="imgEntrar" value="<%=cmdEntrar%>" src="../images/entrar2_eng.gif" WIDTH="50" HEIGHT="20">
						</div></td>
                        <td width="10">&nbsp;</td>
                      </tr><tr>
                        <td colspan=3 align=center>
							<a class="formulario" href="javascript:void(null)" onclick="recuerdePWD()"><u>Haben Sie Ihre Login-Daten vergessen?</u></a>
						</td>
                        <td width="10">&nbsp;</td>
                      </tr>
                    </table>
                </div></td>
              </tr>
              <tr>
                <td colspan="2" align="left" bgcolor="#E4E4E4"><table width="275" cellspacing="0" cellpadding="0">
  <tr>
    <td width="273"><div align="left" class="textos">
                    <div align="center">Haben Sie eine Frage ?<br>
                    Rufen Sie uns an unter +34 912 962 000</div>
                </div></td>
  </tr>
  <tr>
    <td height="29"><div align="left" class="registro">
                    <div align="center" class="textogris"><a href="javascript:ventanaLogin('GER')">Registrierung anmelden</a></div>
                </div></td>
  </tr>
</table></td>
              </tr>

          </table></td>
        </tr>
        <tr>
          <td></td>
        </tr>
    </table></td>
    <td></td>
  </tr>
  <tr>
    <td width="156" height="107" valign="top"><table width="155" cellspacing="0" cellpadding="0">
        <tr>
          <td width="10">&nbsp;</td>
          <td width="149">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><a href="../eng/login.asp" onMouseOver="MM_swapImage('Image11','','../images/english_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../images/english.gif" name="Image11" width="64" height="12" border="0" id="Image11"></a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><a href="../login.asp" onMouseOver="MM_swapImage('Image21','','../images/español_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../images/español.gif" name="Image21" width="64" height="12" border="0" id="Image21"></a></td>
        </tr>
    </table></td>
    <td></td>
  </tr>
  <tr>
    <td height="35" colspan="5"><div align="right"><a href="javascript:ventanaSecundaria('../../../CUSTOM/fulldemo/public/ger/aviso legal.htm')" onMouseOver="MM_swapImage('Image3','','../images/gesetzlicher_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../images/gesetzlicher.gif" name="Image3" width="113" height="10" border="0" id="Image3"></a></div></td>
    <td></td>
  </tr>
    <tr>
    <td height="40" bgcolor="#E4E4E4" class="subtitulo"><div align="left">FULLSTEP NETWORKS, S.L.<br>
Antonio de Cabezón, 83, 4º <br>
28700 Madrid (Spanien)</div></td>
    <td valign="top" bgcolor="#E4E4E4" class="subtitulo"><div align="center" class="Estilo2"></div></td>
    <td colspan="3" bgcolor="#E4E4E4" class="subtitulo"><div align="center"><span class="subtextos"><a href="http://www.microsoft.com/windows/ie_intl/de/default.asp" target="_blank">Internet Explorer 6.0 </a>oder h&ouml;her ben&ouml;tigt.</span><br>
    </div></td>
    <td></td>
  </tr>
  <tr>
    <td height="20" colspan="5" bgcolor="#6D93A2" class="registro">
      <div align="center" class="subtit">COPYRIGHT &copy; 2006 FULLSTEP NETWORKS </div>
    </td>
    <td></td>
  </tr>
  <tr>
    <td height="17"></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>
<map name="Map">
  
  <area shape="rect" coords="72,3,145,74" alt="Any questions? Call us  +34 912 962 000"  onMouseOver="MM_swapImage('Image9','','../images/imagen%20_chica.jpg',1)" onMouseOut="MM_swapImgRestore()">
  <area shape="rect" coords="2,1,73,72"  onMouseOver="MM_swapImage('Image9','','../images/imagen%20_carros.jpg',1)" onMouseOut="MM_swapImgRestore()">
  <area shape="rect" coords="1,73,72,144" onMouseOver="MM_swapImage('Image9','','../images/imagen%20_mundo.jpg',1)" onMouseOut="MM_swapImgRestore()">
  <area shape="rect" coords="72,73,1241764,6989616" onMouseOver="MM_swapImage('Image9','','../images/imagen_cuadro.gif',1)" onMouseOut="MM_swapImgRestore()">
</map>
</body>
</html>
