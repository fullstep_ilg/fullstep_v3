﻿<%@ Language=VBScript %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/idioma.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="estilos.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.9.0.js"></script>
<script src="js/jquery-1.9.0.min.js"></script>
<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
<script src="../../common/formatos.js"></script>
<script language="JavaScript" src="../../common/ajax.js"></script>
<!--#include file="../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=SPA", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body onload="init()">

<div id="cont_gen">
	<div id="izd_gen"> <a href="http://www.ambar.com" target="_blank"><img src="images/logo-Ambar.png" alt="logo-Ambar" border="0"></a>
    </div>
    <div id="drc_gen">
    	<h1>SOLICITAR REGISTRO</h1>
        <div class="int">
        	<p> 
                Para continuar con el proceso de alta es imprescindible la aceptación de las
                <span class="rojo">CONDICIONES DE USO DEL PORTAL DE PROVEEDORES</span>.
            </p>
            <p style="height:150px; padding:0 5px; overflow:auto; background:#FFF; border:#CCC 1px solid">
               CONDICIONES GENERALES DE USO DEL PORTAL DE PROVEEDORES<br /><br />
IMPORTANTE LEA ATENTAMENTE<br /><br />

Alta de proveedores<br /><br />

 Aviso Legal
<br /><br />
 Acceso al Portal<br /><br />

 El alta como proveedor en el Portal de proveedores de Ambar está condicionado a la previa lectura y aceptación de las siguientes cláusulas. Sin expresar su conformidad con las mismas no podrá registrarse. Siempre que se acceda y utilice el Portal, se entenderá que está de acuerdo, de forma expresa, plena y sin reservas, con la totalidad del contenido de este Aviso Legal. Asimismo, al ser Ud el usuario principal de su empresa, queda obligado al aceptar la conformidad de este Aviso Legal a velar por su cumplimiento por parte de todos los usuarios que se den de alta en su empresa, liberando a Ambar de toda responsabilidad por los posibles perjuicios que dichos usuarios pudieran causar a su empresa o cualquier otra debido a sus actuaciones en el Portal.<br />


Cláusulas<br />

1. Objeto del Portal de proveedores de Ambar <br />

 El portal de proveedores de Ambar es el medio a través del cual Ambar se comunica con sus proveedores para solicitarles ofertas, documentos así como aquella información comercial que estime oportuna. Al mismo tiempo Ambar puede utilizar el portal para remitirle aquella información que considere de su interés.<br /><br />

 El uso o acceso al Portal y/o a los Servicios no atribuyen al proveedor ningún derecho sobre las marcas, nombres comerciales o signos distintivos que aparezcan en el mismo, siendo estos propiedad de Ambar o de terceros. En consecuencia, los Contenidos son propiedad intelectual de Ambar o de terceros, sin que puedan entenderse cedidos al proveedor.<br /><br />



2. Objeto <br /><br />

El presente acuerdo tiene por objeto regular las relaciones entre Ambar y el proveedor, en todo lo relacionado con el uso del Portal.<br /><br />


3. Obligaciones del proveedor<br /><br />

Son obligaciones del proveedor las siguientes:<br /><br />

a. Proporcionar cuantos datos sean necesarios para el adecuado funcionamiento del sistema, así como mantenerlos actualizados, comunicando a la mayor brevedad posible todo cambio que se produzca en los mismos.<br /><br />

b. Garantizar la autenticidad de los datos facilitados a consecuencia de la cumplimentación de los formularios necesarios para la suscripción de los Servicios. Asimismo, el proveedor actualizará la información facilitada para que refleje, en cada momento, su situación real. Por todo ello, el proveedor será el único responsable de los daños y perjuicios ocasionados a Ambar como consecuencia de declaraciones inexactas o falsas.<br /><br />

c. Guardar absoluta confidencialidad en relación con toda la información que se genere en las relaciones entre el proveedor e Ambar.<br /><br />

d. Abstenerse de cualquier manipulación en la utilización informática del Portal, así como el uso del mismo con fines distintos de aquellos para los que fue previsto. Asimismo, el proveedor se abstendrá de acceder a zonas no autorizadas del Portal.<br /><br />

e. Cumplir fielmente sus compromisos en la información remitida a través del portal. En el caso de que el proveedor no demuestre la necesaria diligencia comercial, o incumpla las obligaciones contraídas, Ambar se reserva el derecho de excluir temporal o permanentemente al proveedor del portal.<br /><br />

f. El proveedor deberá indicar solamente aquellos grupos de materiales, que se refieran a bienes o servicios que, en el momento de la aceptación del acuerdo, el proveedor comercialice, fabrique o distribuya y que sean del interés comercial de Ambar.<br /><br />

g. El proveedor acepta que las ofertas introducidas en el Portal sean consideradas con el mismo rango y validez que una oferta enviada por cualquier otro medio tradicional (carta, correo electrónico).<br /><br />
h. El proveedor se obliga a hacer un uso correcto del Portal y de los Servicios conforme a la ley, el presente Aviso Legal y demás reglamentos de uso e instrucciones puestos en su conocimiento, así como con la moral y las buenas costumbres generalmente aceptadas y el orden público.<br /><br />
Por ello, se abstendrá de utilizar el Portal o cualquiera de los Servicios con fines ilícitos, prohibidos en el presente Aviso Legal, lesivos de los derechos e intereses de terceros. En particular, y a título meramente indicativo, el proveedor se compromete a no transmitir, difundir o poner a disposición de terceros informaciones, datos, contenidos, gráficos, archivos de sonido y/o imagen, fotografías, grabaciones, software y, en general, cualquier clase de material que:<br /><br />
1. Sea contrario a los derechos fundamentales y libertades públicas reconocidas en la Constitución, tratados internacionales y leyes aplicables; <br /><br />
2. Induzca, incite o promueva actuaciones delictivas, contrarias a la ley, a la moral y buenas costumbres generalmente aceptadas o al orden público;<br /><br /> 
3. Sea falso, inexacto o pueda inducir a error, o constituya publicidad ilícita, engañosa o desleal;<br /><br />
4. Esté protegido por derechos de propiedad intelectual o industrial pertenecientes a terceros, sin autorización de los mismos;<br /><br />
5. Vulnere derechos al honor, a la intimidad o a la propia imagen;<br /><br />
6. Vulnere las normas sobre secreto de las comunicaciones;<br /><br />
7. Constituya competencia desleal, o perjudique la imagen empresarial de Ambar o terceros;<br /><br />
8. Esté afectado por virus o elementos similares que puedan dañar o impedir el funcionamiento adecuado del Portal, los equipos informáticos o sus archivos y documentos.<br /><br />

4. Derechos del proveedor<br /><br />

Son derechos del proveedor los siguientes:<br /><br />

1. Mantener una presencia constante en la base de datos de Ambar, en su calidad de proveedor dado de alta. <br /><br />

2. Recibir peticiones de ofertas en virtud de las normas establecidas. <br /><br />


5. Obligaciones de Ambar <br /><br />

Son obligaciones de Ambar las siguientes:<br /><br />

a. Mantener la información que estime oportuna actualizada, no siendo responsable de los errores que se produzcan por fuerza mayor o caso fortuito.<br /><br />

b. Guardar absoluta confidencialidad de toda la información relativa al proveedor: bien la proporcionada por éste, bien la generada en las relaciones entre el proveedor e Ambar<br /><br />

c. Facilitar al proveedor, en cualquier momento, la situación de sus datos en la base de datos, y no facilitarla a terceros si no es para permitir al proveedor la presentación de sus ofertas y la toma en consideración de sus productos como alternativa de suministro. Asimismo, facilitar al proveedor, en cualquier momento, la situación de sus datos en la base de datos de este Portal de Proveedores de Ambar, para que éste, previa notificación por escrito, los modifique o elimine. <br /><br />


6. Derechos de Ambar<br /><br />

a. El Usuario presta su consentimiento para que Ambar ceda sus datos a las empresas a ella asociadas o del grupo al que pertenece, así como a otras con las que concluya acuerdos con la única finalidad de la mejor prestación del servicio, respetando, en todo caso, la legislación española sobre protección de los datos de carácter personal. Asimismo, el Usuario acepta que Ambar o sus empresas a ella asociadas, sociedades filiales y participadas, le remitan información sobre cualesquiera bienes o servicios que comercialicen, directa o indirectamente, o que en el futuro puedan comercializar. La aceptación del Usuario para que puedan ser cedidos sus datos en la forma establecida en este párrafo, tiene siempre carácter revocable sin efectos retroactivos, conforme a lo que dispone la Ley Orgánica 15/1999, de 13 de diciembre. <br /><br />

b. Optar, en caso de incumplimiento de cualquiera de las obligaciones contenidas en el presente Aviso legal por el PROVEEDOR, por exigir el debido cumplimiento de las mismas, o por la resolución del contrato, en los términos de la cláusula 8 de este Aviso Legal. <br /><br />

7. Limitación de responsabilidad<br /><br />

a. Ambar no asume ninguna responsabilidad derivada de la falta de disponibilidad del sistema, fallos de la red o técnicos que provoquen un corte o una interrupción en el Portal. Igualmente Ambar no se hace responsable de los daños y perjuicios que puedan ocasionar la propagación de virus informáticos u otros elementos en el sistema.<br /><br />
b. Ambar no asume ninguna responsabilidad de pagos y/o reclamaciones que deriven de las actuaciones de los clientes en relación al acuerdo entre dicho/s cliente/s y el proveedor.<br /><br />

c. Ambar no asume ninguna responsabilidad sobre cualquier actuación que sea contraria a las leyes, usos y costumbres, o bien que vulnere las obligaciones establecidas en el presente Aviso Legal, por parte de ninguna persona de Ambar, del proveedor o cualquier usuario de la red. No obstante, cuando Ambar tenga conocimiento de cualquier conducta a la que se refiere el párrafo anterior, adoptará las medidas necesarias para resolver con la máxima urgencia y diligencia los conflictos planteados. <br /><br />
d. Ambar se exime de cualquier responsabilidad derivada de la intromisión de terceros no autorizados en el conocimiento de las condiciones y circunstancias de uso que los proveedores hagan del Portal y de los Servicios.<br /><br />

8. Cumplimiento<br /><br />

En cumplimiento de lo dispuesto en la Ley Orgánica 15/1999, de 13 de Diciembre, de Protección de Datos de Carácter Personal y su normativa de desarrollo, le informamos que sus datos personales aquí facilitados pasarán a formar parte de un fichero de proveedores cuyo responsable es Ambar Banco, SA, ubicado en su sede social, Pza. Paraíso, 2 de Zaragoza. Estos datos serán tratados con la finalidad de solicitar ofertas y remitir comunicados relativos a la función de compras del Grupo Ambar, así como para dirigirle comunicaciones publicitarias sobre productos y servicios de Ambar y de las entidades del Grupo Ambar, cuya actividad figura en el tablón de anuncios existente en cada una de las oficinas de Ambar y en la web ibercaja.es, que puedan resultar del interés de la persona jurídica de cuya organización el Usuario forma parte.<br /><br />
El Usuario autoriza el tratamiento de sus datos personales suministrados para las finalidades señaladas.<br /><br />
De acuerdo con lo dispuesto en la Ley 34/2002, de Servicios de la Sociedad de la información, el usuario presta su consentimiento a que Ambar pueda remitirle las comunicaciones comerciales indicadas en el párrafo anterior a través del correo electrónico o de cualquier otro medio de comunicación electrónica equivalente (e-mail, sms, …).<br /><br />
Usted podrá ejercitar los derechos de acceso, rectificación, cancelación y oposición al tratamiento de sus datos personales dirigiéndose por escrito, con acreditación de su identidad, al Servicio de Atención al Cliente de Ambar, Plaza Basilio Paraíso, 2 50008 ZARAGOZA.<br /><br />


9. Resolución del acuerdo<br /><br />

En caso de incumplimiento de cualquiera de las obligaciones contenidas en el presente Aviso legal, el proveedor o Ambar habrán de notificar dicho incumplimiento a la parte que lo hubiera cometido, otorgándole un plazo de quince días hábiles, contando desde la notificación, para que lo subsane. Transcurrido dicho plazo sin que se haya producido la subsanación del incumplimiento, la otra parte podrá optar por el cumplimiento o la resolución del presente acuerdo, con la indemnización de daños y perjuicios en ambos casos, de conformidad con lo establecido en el artículo 1.124 del Código Civil. En caso de optar por la resolución, tanto el PROVEEDOR como IBERCAJA aceptan que la simple notificación por cualquier medio que acredite su recepción, será suficiente para que la misma tenga plenos efectos.<br /><br />


10. Duración <br /><br />

El presente acuerdo tendrá una duración indefinida siempre y cuando no se produzca denuncia por escrito de cualquiera de las partes, con al menos 1 mes de antelación.<br /><br />
El procedimiento de baja será enviando un mail con Asunto “Baja del portal” a la dirección <a href="mailto:atencionalcliente@fullstep.com">atencionalcliente@fullstep.com</a> 

11. Notificaciones<br /><br />

a. Las notificaciones entre las partes podrán hacerse por cualquier medio de los admitidos en Derecho, que permita tener constancia de la recepción, incluido el correo electrónico.<br /><br />

b. Los cambios de domicilios y correos electrónicos serán notificados por escrito y no producirán efectos hasta transcurridos dos días hábiles desde su recepción. En todo caso, el nuevo domicilio y correo electrónico será necesariamente dentro del territorio español.<br /><br />

c. Las relaciones entre Ambar y el proveedor derivadas del uso del Portal y de los servicios prestados a través del mismo, se someterán a la legislación y jurisdicción españolas. <br /><br />



			</p>
            <p style="text-align:right"><a href="registro_texto.htm" class="bt_registro" target="blank">Imprimir</a> </p>
<div class="caja_acepto">


<!--<form action="" method="post" style="width:400px;">//-->
<form name="frmAlta" id="frmAlta" method="post" style="width:400px;">
  <span id="sprycheckbox1">
<label>
  <input type="checkbox" name="opcion1" id="opcion1" required>
</label>
<span class="checkboxRequiredMsg">Seleccione para continuar.</span></span><span class="rojo">He leído y Acepto</span> las Condiciones Generales de Uso del Portal de Proveedores.<br />
<br />

<button type="button" class="bt_registro" id="submit1" name="submit1">Siguiente</button>
<br>
</form>


</div>
<div style="clear:both;"></div>
    	</div>    
    </div>
</div>
<script type="text/javascript">
// <!--
// var sprycheckbox1 = new Spry.Widget.ValidationCheckbox("sprycheckbox1");
// //-->
$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked')) {
        $('#frmAlta').submit();
        alta();
    }
});
</script>
 			
</body>
<!--#include file="../../common/fsal_2.asp"-->
</html>
