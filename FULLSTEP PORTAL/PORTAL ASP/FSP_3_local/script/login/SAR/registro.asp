﻿<%@ Language=VBScript %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/idioma.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://fast.fonts.net/jsapi/f5dbd368-355c-4f28-bc2f-217b51498d80.js"></script>
<script src="http://code.jquery.com/jquery-1.9.0.js"></script>
<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->

<script src="../../common/formatos.js"></script>
<script language="JavaScript" src="../../common/ajax.js"></script>
<!--#include file="../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=SPA", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body onload="init()">

<div id="cont_gen">
	<div id="izd_gen"> <a href="http://www.grupotopgel.es/"><img src="images/logo.png" border="0"></a> </a>
    </div>
    <div id="drc_gen">
    	<h1>SOLICITAR REGISTRO</h1>
        <div class="int">
        	<p> 
                Para continuar con el proceso 
              de alta es imprescindible la aceptación de las siguientes condiciones:
                
            </p>
            <p style="height:150px; padding:0 5px; overflow:auto; background:#FFF; border:#CCC 1px solid">
               CONDICIONES GENERALES DE USO DEL PORTAL DE PROVEEDORES<br /><br />
IMPORTANTE LEA ATENTAMENTE<br /><br />

El alta como PROVEEDOR en este website está condicionada a la previa lectura y aceptación de las presentes condiciones, no pudiendo proceder al registro sin la previa aceptación de las mismas, así como de aquellas contenidas en el aviso legal (http://sareb.fullstep.net/script/login/SAR/spa/docs/Aviso-legal.pdf) del presente site.<br /><br />

Siempre que se acceda y utilice el portal, se entenderá que está de acuerdo, de forma expresa, plena y sin reservas, con la totalidad del contenido del presente documento y el Aviso Legal.<br /><br />
 
Se entiende por usuario principal de un PROVEEDOR, la persona física con poder suficiente que actuando en nombre y representación de la sociedad procede a su registro en el presente portal. El usuario principal, queda obligado a aceptar la conformidad de estas condiciones de legales y velar por su cumplimiento por parte de cualquier usuario del PROVEEDOR en cuestión. <br /><br />

Sareb no se hace responsable de los posibles perjuicios que los usuarios del PROVEEDOR en cuestión pudieran causar a su empresa o cualquier otra debido a sus actuaciones en el portal.<br /><br />

El portal de compras de Sareb es un medio de comunicación con PROVEEDORES, a través del cual puede comunicarles y solicitarles entre otros, ofertas, documentos información comercial que estime oportuna y remitirles aquella información que Sareb considere de interés.<br /><br />

1.	Objeto <br /><br />
El presente acuerdo tiene por objeto establecer las bases de las relaciones entre Sareb y el PROVEEDOR, en todo lo relacionado con el uso del portal.<br /><br />
El PROVEEDOR tiene a su disposición las CONDICIONES GENERALES DE COMPRA (http://sareb.fullstep.net/script/login/SAR/spa/docs/Condiciones-generales-de-compra.pdf) de Sareb para su consulta en el siguiente enlace, en las que se especifica las características de los productos y servicios en cuestión así como los derechos y obligaciones de las partes que se derivan de la prestación de los mismos. <br /><br />

2.	Obligaciones del PROVEEDOR<br /><br />
Son obligaciones del proveedor las siguientes:<br /><br />
a.	Atender y cumplir con las condiciones establecidas en el Aviso Legal  (http://sareb.fullstep.net/script/login/SAR/spa/docs/Aviso-legal.pdf) del presente website, relativas a: <br /><br />I.- Contenido de la página web; (i) contenidos y usos, (ii) hipervínculos y enlaces a contenidos, (iii) información y comentarios emitidos por usuarios;<br /><br /> II.-Navegación; <br /><br />III.- Seguridad;<br /><br /> IV.- Política de Cookies; <br /><br />V.- Protección de Datos; <br /><br />VI Propiedad Intelectual e Industrial; <br /><br />VII.- Legislación aplicables y jurisdicción competente.<br /><br />

b.	Proporcionar cuantos datos sean necesarios para el correcto desenvolvimiento de las relaciones entre las partes, así como mantenerlos actualizados, comunicando a la mayor brevedad posible todo cambio que se produzca en los mismos. <br /><br />

c.	Garantizar la autenticidad de los datos facilitados a consecuencia de la cumplimentación de los formularios necesarios para la suscripción de los Servicios. Asimismo, el PROVEEDOR actualizará la información facilitada para que refleje, en cada momento, su situación real. Por todo ello, el PROVEEDOR será el único responsable de los daños y perjuicios ocasionados a Sareb como consecuencia de declaraciones inexactas o falsas, pudiendo Sareb dar por finalizadas las relaciones entre las partes.<br /><br />

d.	Guardar absoluta confidencialidad de toda la información que se genere en las relaciones entre el PROVEEDOR y Sareb.<br /><br />

e.	Abstenerse de cualquier manipulación en la utilización informática del portal, así como el uso del mismo con fines distintos de aquellos para los que fue previsto. Asimismo, el PROVEEDOR se abstendrá de acceder a zonas no autorizadas del portal.<br /><br />

f.	Cumplir fielmente sus compromisos en la información remitida a través del portal. En el caso de que el PROVEEDOR no demuestre la necesaria diligencia comercial, o incumpla las obligaciones contraídas, Sareb se reserva el derecho de finalizar las relaciones entre las partes o excluir temporal al PROVEEDOR del portal.
<br /><br />
g.	El PROVEEDOR deberá indicar solamente aquellos grupos de materiales, que se refieran a bienes o servicios que, en el momento de la aceptación del acuerdo, el proveedor comercialice, fabrique o distribuya y que sean del interés comercial de Sareb.<br /><br />

h.	El PROVEEDOR acepta que las ofertas introducidas en el portal sean consideradas con el mismo rango y validez que una oferta enviada por cualquier otro medio tradicional (carta, fax).<br /><br />

i.	El PROVEEDOR se obliga a hacer un uso correcto del portal y de los Servicios conforme a la ley, las presentes condiciones de uso, el aviso legal del website y demás reglamentos de uso e instrucciones puestos en su conocimiento, así como con la moral y las buenas costumbres generalmente aceptadas y el orden público. Por ello, el PROVEEDOR se abstendrá de utilizar el portal o cualquiera de los Servicios con fines ilícitos, prohibidos en las presentes condiciones de uso, lesivos de los derechos e intereses de terceros.<br /><br />

A título meramente indicativo, el PROVEEDOR se compromete a no transmitir, difundir o poner a disposición de terceros informaciones, datos, contenidos, gráficos, archivos de sonido y/o imagen, fotografías, grabaciones, software y, en general, cualquier clase de material que:<br /><br />
1. Sea contrario a los derechos fundamentales y libertades públicas reconocidas en la Constitución, Tratados internacionales y leyes aplicables; <br /><br />
2. Induzca, incite o promueva actuaciones contrarias a la ley, a la moral y buenas costumbres generalmente aceptadas o al orden público; <br /><br />
3. Sea falso, inexacto o pueda inducir a error, o constituya publicidad ilícita, engañosa o desleal;<br /><br />
4. Esté protegido por derechos de propiedad intelectual o industrial pertenecientes a terceros, sin autorización de los mismos;<br /><br />
5. Vulnere derechos al honor, a la intimidad o a la propia imagen;<br /><br />
6. Vulnere las normas sobre secreto de las comunicaciones;<br /><br />
7. Constituya competencia desleal, o perjudique la imagen empresarial de Sareb o terceros;<br /><br />
8. Esté afectado por virus o elementos similares que puedan dañar o impedir el funcionamiento adecuado del portal, los equipos informáticos o sus archivos y documentos.<br /><br />
3.	Derechos del PROVEEDOR<br /><br />
Son derechos del PROVEEDOR los siguientes:<br /><br />
a.	Mantener una presencia en la base de datos de Sareb, en su calidad de proveedor. <br /><br />

b.	Recibir peticiones de ofertas en base al cumplimiento de las normas que determine establecidas por Sareb. <br /><br />

4.	Obligaciones de Sareb <br /><br />
Son obligaciones de Sareb las siguientes:<br /><br />
a.	Mantener la información que estime oportuna actualizada, no siendo responsable de los errores que se produzcan por fuerza mayor o caso fortuito.<br /><br />

b.	Guardar la confidencialidad de la información relativa al PROVEEDOR: bien la proporcionada por éste, bien la generada en las relaciones entre las partes<br /><br />

c.	Informar al proveedor, cuando lo solicite de la situación de sus datos para que éste, previa notificación por escrito, los modifique o elimine. Sareb no facilitará datos a terceros si no es para permitir al PROVEEDOR la presentación de sus ofertas y la toma en consideración de sus productos como alternativa de suministro. <br /><br />

5.	Derechos de Sareb<br /><br />
Son derechos de Sareb los siguientes:<br /><br />
a.	El Usuario principal, en el momento de la suscripción del presente documento, actuando en nombre y representación del PROVEEDOR presta el consentimiento para que Sareb trate sus datos y los facilite en su caso a las sociedades con las que, en su caso pudiera concluir acuerdos de prestación de servicios con la única finalidad de la mejor prestación del servicio, respetando, en todo caso, la legislación española sobre protección de los datos de carácter personal.<br /><br />

b.	Asimismo, el Usuario acepta que Sareb, le remita información sobre cualquier proceso o invitación a concursos o licitaciones que organice Sociedad y que pudiera afectar a los bienes o servicios que comercialicen, directa o indirectamente, o que en el futuro puedan comercializar. <br /><br />
El proveedor puede darse de baja del portal si desea que su información desaparezca del portal de proveedores de Sareb.<br /><br />
c.	Exigir el cumplimiento de las obligaciones establecidas en el presente documento al PROVEEDOR y en caso de incumplimiento, podrá solicitar la resolución de los acuerdos según los términos de la cláusula 8 de este Aviso Legal. 
<br /><br />
6.	Limitación de responsabilidad<br /><br />

a.	Respecto de los fallos en el sistema, red o técnicos que provoquen un corte o una interrupción en el portal, y en general las cuestiones sobre navegación se estará a lo dispuesto en el Aviso Legal del presente website.<br /><br />

b.	Sareb no asume ninguna responsabilidad de pagos y/o reclamaciones que deriven de las actuaciones de los clientes en relación al acuerdo entre dicho/s cliente/s y el PROVEEDOR.<br /><br />

c.	Sareb no asume ninguna responsabilidad sobre cualquier actuación que sea contraria a las leyes, usos y costumbres, o bien que vulnere las obligaciones establecidas en el presente documento, por parte del PROVEEDOR o terceros ajenos a Sareb. <br /><br />

Si Sareb tuviese conocimiento de cualquier conducta a la que se refiere el párrafo anterior, podrá adoptar las medidas que estime necesarias para resolver con la máxima urgencia y diligencia los conflictos planteados. <br /><br />

d.	Sareb no asumirá responsabilidades derivadas de la intromisión de terceros no autorizados en el conocimiento de las condiciones y circunstancias de uso que los PROVEEDORES hagan del portal y de los Servicios.<br /><br />

7.	Resolución del acuerdo<br /><br />
En caso de incumplimiento de cualquiera de las obligaciones contenidas en el presente documento de condiciones de uso del website o del Aviso Legal contenido en el website, la parte afectada podrá comunicar a la otra dicho incumplimiento detectado, otorgándole un plazo de quince días hábiles, contando desde la notificación, para que lo subsane.
Transcurrido dicho plazo sin que se haya producido la subsanación del incumplimiento, la otra parte podrá optar por el cumplimiento o la resolución del presente acuerdo, con la indemnización de daños y perjuicios en ambos casos le confiera las leyes.<br /><br />
En caso de optar por la resolución, tanto el PROVEEDOR como Sareb aceptan que la simple notificación será suficiente, para que la misma tenga plenos efectos.
<br /><br />
8.	Duración <br /><br />
El presente acuerdo tendrá una duración indefinida siempre y cuando no se produzca denuncia por escrito de cualquiera de las partes, con al menos 1 mes de antelación.
El procedimiento de baja será enviando un mail con Asunto “Baja del portal” a la dirección atencionalcliente@fullstep.com. En caso de no ser posible el envío del mail, se enviará un fax al 91 296 20 25 indicando los datos básicos para identificar a PROVEEDOR.<br /><br />

9.	Notificaciones<br /><br />

a.	Las notificaciones entre las partes podrán hacerse por cualquier medio de los admitidos en Derecho, que permita tener constancia de la recepción, incluido el fax.<br /><br />

b.	Los cambios de domicilios y faxes serán notificados por escrito y no producirán efectos hasta transcurridos dos días hábiles desde su recepción. En todo caso, el nuevo domicilio y fax será necesariamente dentro del territorio español.
<br /><br />
c.	Las relaciones entre Sareb y el PROVEEDOR derivadas del uso del portal y de los servicios prestados a través del mismo, se someterán a la legislación y jurisdicción españolas. <br /><br />


			</p>
            <p style="text-align:right"><a href="registro_texto.htm" class="bt_registro" target="blank">Imprimir</a> </p>
<div class="caja_acepto">


<!--<form action="" method="post" style="width:400px;">//-->
<form name="frmAlta" id="frmAlta" method="post" style="width:400px;">
  <span id="sprycheckbox1">
<label>
  <input type="checkbox" name="opcion1" id="opcion1" required>
</label>
<span class="checkboxRequiredMsg">Seleccione para continuar.</span></span>He leído y acepto las Condiciones de Adhesión al Portal de Sareb, así como <a href="spa/docs/SAREB_Código_de_conducta.pdf" target="_blank" >el Código de Conducta</a>.<br />
<br />

<button type="button" class="bt_registro" id="submit1" name="submit1">Siguiente</button>
<br>
</form>


</div>
<div style="clear:both;"></div>
    	</div>    
    </div>
</div>
<script type="text/javascript">
// <!--
// var sprycheckbox1 = new Spry.Widget.ValidationCheckbox("sprycheckbox1");
// //-->
$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked')) {
        $('#frmAlta').submit();
        alta();
    }
	 else
       alert("Debe aceptar las condiciones de uso del portal para poder registrarse");
});
</script>
 			
</body>
<!--#include file="../../common/fsal_2.asp"-->
</html>
