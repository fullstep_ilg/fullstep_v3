﻿<%@ Language=VBScript %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--#include file="../../../common/XSS.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/fsal_1.asp"-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" href="../css/estilos.css">
        <link rel="stylesheet" href="../css/stylesheet.css">
<script src="../js/jquery-1.9.0.js"></script>
<script src="../js/jquery-1.9.0.min.js"></script>
<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
<script src="../../../common/formatos.js"></script>
<script language="JavaScript" src="../../common/ajax.js"></script>
<!--#include file="../../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=ENG", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body onload="init()">
	<!--

<header id="heading"> 
        
        	<div class="container"> 
                       
             	<div class="logotipo">
            		<a href="<%=application("RUTASEGURA")%>"><img src="img/logo_fullstep.gif" alt="Fullstep" /></a>
                </div>  

                
            
           	  <div id="titulo">PORTAL DE PROVEEDORES</div>      
                     
    		</div>
        
        
          
            
        </header>           
    			
        
        
    	<section id="servicios"> 
            
            <div class="container">
            
           		<div class="text">
                
                	<!--
                    <div class="columna sol">
                        <div class="imagen">
                      <img src="img/shim.gif" width="100" >
                         </div>  
                        
                   	</div>
                   
                    <div class="centro sin solicitud">
                      <div>
                        
                        <h2><em>SOLICITAR REGISTRO</em></h2>
        <div class="int">
        	<p>Para continuar con el proceso de alta es imprescindible que lea y acepte las condiciones de acceso al Portal de Proveedores.</p>
            <div class="caja_registro">
                <strong>IMPORTANTE LEA ATENTAMENTE</strong><br><br>



Alta de proveedores - Aviso Legal<br><br>

Acceso al Portal<br><br>

El alta como proveedor en el Portal de compras de FULLSTEP NETWORKS S.L.U. (en adelante FULLSTEP) está condicionado a la previa lectura y aceptación de las siguientes cláusulas. Sin expresar su conformidad con las mismas no podrá registrarse. Siempre que se acceda y utilice el Portal, se entenderá que está de acuerdo, de forma expresa, plena y sin reservas, con la totalidad del contenido de este Aviso Legal. Asimismo, al ser Ud el Usuario Principal de su empresa, queda obligado al aceptar la conformidad de este Aviso Legal a velar por su cumplimiento por parte de todos los usuarios que se den de alta en su empresa, liberando a FULLSTEP de toda responsabilidad por los posibles perjuicios que dichos usuarios pudieran causar a su empresa o cualquier otra debido a sus actuaciones en el Portal.<br>
<br>


Cláusulas<br><br>

1. Objeto del Portal de compras de FULLSTEP<br><br>

El portal de compras de FULLSTEP es el medio a través del cual FULLSTEP se comunica con sus proveedores para solicitarles ofertas, documentos así como aquella información comercial que estime oportuna. Al mismo tiempo FULLSTEP puede utilizar el portal para remitirle aquella información que considere de su interés.<br><br>

FULLSTEP actúa tanto como comprador directo como de gestor de compras de sus clientes, y actuará en nombre de ellos durante el proceso de compra.<br><br>

El uso o acceso al Portal y/o a los Servicios no atribuyen al PROVEEDOR ningún derecho sobre las marcas, nombres comerciales o signos distintivos que aparezcan en el mismo, siendo estos propiedad de FULLSTEP o de terceros. En consecuencia, los Contenidos son propiedad intelectual de FULLSTEP o de terceros, sin que puedan entendersecedidos al PROVEEDOR.<br><br>



2. Objeto<br><br>

El presente acuerdo tiene por objeto regular las relaciones entre FULLSTEP y el PROVEEDOR, en todo lo relacionado con el uso del Portal.<br><br>


3. Obligaciones del PROVEEDOR<br><br>

Son obligaciones del proveedor las siguientes:<br><br>

a. Proporcionar cuantos datos sean necesarios para el adecuado funcionamiento del sistema, así como mantenerlos actualizados, comunicando a la mayor brevedad posible todo cambio que se produzca en los mismos.<br><br>

b. Garantizar la autenticidad de los datos facilitados a consecuencia de la cumplimentación de los formularios necesarios para la suscripción de los Servicios. Asimismo, el PROVEEDOR actualizará la información facilitada para que refleje , en cada momento, su situación real. Por todo ello, el PROVEEDOR será el único responsable de los daños y perjuicios ocasionados a FULLSTEP como consecuencia de declaraciones inexactas o falsas.<br><br>


c. Guardar absoluta confidencialidad en relación con toda la información que se genere en las relaciones entre el PROVEEDOR y FULLSTEP.<br><br>

d. Abstenerse de cualquier manipulación en la utilización informática del Portal, así como el uso del mismo con fines distintos de aquellos para los que fue previsto. Asimismo, el PROVEEDOR se abstendrá de acceder a zonas no autorizadas del Portal.<br><br>

e. Cumplir fielmente sus compromisos en la información remitida a través del portal. En el caso de que el PROVEEDOR no demuestre la necesaria diligencia comercial, o incumpla las obligaciones contraídas, FULLSTEP se reserva el derecho de excluir temporal o permanentemente al PROVEEDOR del portal.<br><br>

f. El PROVEEDOR deberá indicar solamente aquellos grupos de materiales, que se refieran a bienes o servicios que, en el momento de la aceptación del acuerdo, el proveedor comercialice, fabrique o distribuya y que sean del interés comercial de FULLSTEP.<br><br>

g. El PROVEEDOR acepta que las ofertas introducidas en el Portal sean consideradas con el mismo rango y validez que una oferta enviada por cualquier otro medio tradicional (carta, fax).<br><br>

h. El PROVEEDOR se obliga a hacer un uso correcto del Portal y de los Servicios conforme a la ley, el presente Aviso Legal y demás reglamentos de uso e instrucciones puestos en su conocimiento, así como con la moral y las buenas costumbres generalmente aceptadas y el orden público.<br><br>

Por ello, se abstendrá de utilizar el Portal o cualquiera de los Servicios con fines ilícitos, prohibidos en el presente Aviso Legal, lesivos de los derechos e intereses de terceros. En particular, y a título meramente indicativo, el PROVEEDOR se compromete a no transmitir, difundir o poner a disposición de terceros informaciones, datos, contenidos, gráficos, archivos de sonido y/o imagen, fotografías, grabaciones, software y, en general, cualquier clase de material que:<br><br>

1. Sea contrario a los derechos fundamentales y libertades públicas reconocidas en la Constitución, Tratados internacionales y leyes aplicables;<br><br>

2. Induzca, incite o promueva actuaciones delictivas, contrarias a la ley, a la moral y buenas costumbres generalmente aceptadas o al orden público;<br><br>

3. Sea falso, inexacto o pueda inducir a error, o constituya publicidad ilícita, engañosa o desleal;<br><br>

4. Esté protegido por derechos de propiedad intelectual o industrial pertenecientes a terceros, sin autorización de los mismos;<br><br>

5. Vulnere derechos al honor, a la intimidad o a la propia imagen;<br><br>

6. Vulnere las normas sobre secreto de las comunicaciones;<br><br>

7. Constituya competencia desleal, o perjudique la imagen empresarialde FULLSTEP o terceros;<br><br>

8. Esté afectado por virus o elementos similares que puedan dañar o impedir el funcionamiento adecuado del Portal, los equipos informáticos o sus archivos y documentos.<br><br>


4. Derechos del PROVEEDOR<br><br>

Son derechos del PROVEEDOR los siguientes:<br><br>

1. Mantener una presencia constante en la base de datos de FULLSTEP, en su calidad de proveedor dado de alta.<br><br>

2. Recibir peticiones de ofertas en virtud de las normas establecidas.<br><br>


5. Obligaciones de FULLSTEP<br><br>

Son obligaciones de FULLSTEP las siguientes:<br><br>

a. Mantener la información que estime oportuna actualizada, no siendo responsable de los errores que se produzcan por fuerza mayor o caso fortuito.<br><br>

b. Guardar absoluta confidencialidad de toda la información relativa al proveedor: bien la proporcionada por éste, bien la generada en las relaciones entre el PROVEEDOR y FULLSTEP.<br><br>

c. Facilitar al proveedor, en cualquier momento, la situación de sus datos en la base de datos, y no facilitarla a terceros si no es para permitir al proveedor la presentación de sus ofertas y la toma en consideración de sus productos como alternativa de suministro. Asimismo, facilitar al proveedor, en cualquier momento, la situación de sus datos en la base de datos, para que éste, previa notificación por escrito, los modifique o elimine.<br><br>


6. Derechos de FULLSTEP<br><br>

a. El Usuario presta su consentimiento para que FULLSTEP ceda sus datos a las empresas a ella asociadas o del grupo al que pertenece, así como a otras con las que concluya acuerdos con la única finalidad de la mejor prestación del servicio, respetando, en todo caso, la legislación española sobre protección de los datos de carácter personal. Asimismo, el Usuario acepta que FULLSTEP o sus empresas a ella asociadas, sociedades filiales y participadas, le remitan información sobre cualesquiera bienes o servicios que comercialicen, directa o indirectamente, o que en el futuro puedan comercializar. La aceptación del Usuario para que puedan ser cedidos sus datos en la forma establecida en este párrafo, tiene siempre carácter revocable sin efectos retroactivos, conforme a lo que dispone la Ley Orgánica 15/1999, de 13 de diciembre.)<br><br>

b. Optar, en caso de incumplimiento de sus obligaciones por el PROVEEDOR, por exigir el debido cumplimiento de las mismas, o por la resolución del contrato, en los términos de la cláusula 8 de este Aviso Legal.<br><br>

7. Limitación de responsabilidad<br><br>

a. FULLSTEP no asume ninguna responsabilidad derivada de la falta de disponibilidad del sistema, fallos de la red o técnicos que provoquen un corte o una interrupción en el Portal. Igualmente FULLSTEP no se hace responsable de los daños y perjuicios que pueda ocasionar la propagación de virus informáticos u otros elementos en el sistema.<br><br>

b. FULLSTEP no asume ninguna responsabilidad de pagos y/o reclamaciones que deriven de las actuaciones de los clientes en relación al acuerdo entre dicho/s cliente/s y el PROVEEDOR.<br><br>

c. FULLSTEP no asume ninguna responsabilidad sobre cualquier actuación que sea contraria a las leyes, usos y costumbres, o bien que vulnere las obligaciones establecidas en el presente Aviso Legal, por parte de ninguna persona de FULLSTEP, del PROVEEDOR o cualquier usuario de la red. No obstante, cuando FULLSTEP tenga conocimiento de cualquier conducta a la que se refiere el párrafo anterior, adoptará las medidas necesarias para resolver con la máxima urgencia y diligencia los conflictos planteados.<br><br>
d. FULLSTEP se exime de cualquier responsabilidad derivada de la intromisión de terceros no autorizados en el conocimiento de las condiciones y circunstancias de uso que los PROVEEDORES hagan del Portal y de los Servicios.<br><br>



8. Resolución del acuerdo<br><br>

En caso de incumplimiento de cualquiera de las obligaciones contenidas en el presente Aviso legal, el PROVEEDOR o FULLSTEP habrán de notificar dicho incumplimiento a la parte que lo hubiera cometido, otorgándole un plazo de quince días hábiles, contando desde la notificación, para que lo subsane. Transcurrido dicho plazo sin que se haya producido la subsanación del incumplimiento, la otra parte podrá optar por el cumplimiento o la resolución del presente acuerdo, con la indemnización de daños y perjuicios en ambos casos, de conformidad con lo establecido en el artículo 1.124 del Código Civil. En caso de optar por la resolución, tanto el PROVEEDOR como FULLSTEP aceptan que la simple notificación será suficiente, para que la misma tenga plenos efectos.<br><br>


9. Duración<br><br>

El presente acuerdo tendrá una duración indefinida siempre y cuando no se produzca denuncia por escrito de cualquiera de las partes, con al menos 1 mes de antelación.
El procedimiento de baja será enviando un mail con Asunto “Baja del portal” a la dirección atencionalcliente@fullstep.com. En caso de no ser posible el envío del mail, se enviará un fax al 91 296 20 25 indicando los datos básicos para identificar a PROVEEDOR.<br><br>



10. Notificaciones<br><br>

a. Las notificaciones entre las partes podrán hacerse por cualquier medio de los admitidos en Derecho, que permita tener constancia de la recepción, incluido el fax.<br><br>

b. Los cambios de domicilios y faxes serán notificados por escrito y no producirán efectos hasta transcurridos dos días hábiles desde su recepción. En todo caso, el nuevo domicilio y fax será necesariamente dentro del territorio español.
<br><br>
c. Las relaciones entre FULLSTEP y el PROVEEDOR derivadas del uso del Portal y de los servicios prestados a través del mismo, se someterán a la legislación y jurisdicción españolas. 
		  </div>

            <p class="imprimir"><a href="registro_texto.html" class="" target="blank">Imprimir</a> </p>
<div class="caja_acepto">
<!--<form action="" method="post" style="width:400px;">//-->
	 <div class="pag-solicitar-registro">
            <div class="compound" style="background-image: url(../css/img/bg2.jpg)">
                
                <header class="cf">
                    <div class="(6/12) col"><div class="logo" style="background-image: url('../css/img/FullStep.svg');"></div></div>
                    <div class="(6/12) col"><h2 class="text text--right">SUPPLIER PORTAL</h2></div>
                </header>              
                <main>                                                
                    <div class="compound_layer-text -left">                                               
                        <div class="solicitar-registro">
                            <div class="solicitar-registro_container">
                                <div class="solicitar-registro_header">
                                    <div class="grid">
                                        <div class="col"> SUPPLIER REGISTRATION </div>
                                    </div>                                    
                                </div>
                                <div class="hr"></div>
                                <div class="solicitar-registro_body">
                                    <div class="solicitar-registro_info">
                                       To proceed with the registration proccess, the following contract must be read and accepted.
                                    </div>
                                    <div class="solicitar-registro_window">
                                        <div class="solicitar-registro_aviso">
                                            <strong>PLEASE READ CAREFULLY</strong>
                                        </div>
                                        <div id="legal" class="solicitar-registro_aviso-legal">
                                            <h1>Supplier register - Legal Notice</h1>
                                            <h2>Portal access</h2>
                                            <p>Registering in the purchasing Portal of FULLSTEP NETWORKS SLU (from here on referred to as FULLSTEP) implies previous reading and acceptance of the following clauses. Without expressing full compliance with the clauses you will not be allowed to register. Every time you access and use the Portal, it is understood you agree, fully and unreservedly, with the contents of these Legal Notice. Also, being the main user of your company, you are obliged by accepting this Legal Notice to ensure compliance by all users registered in your company, fully exonerating FULLSTEP from any responsibility of the damage those users may cause to your company or any other because of their interactions with the Portal.<br>
<br>

Clauses<br><br>

1. Purpose of the FULLSTEP purchasing Portal<br><br>

The FULLSTEP purchasing portal is the means trough which FULLSTEP communicates with its suppliers to request offers, documents or any other commercial information it deems fit. At the same time FULLSTEP reserves the right to use the portal to communicate any information it considers to be of its interest.<br><br>

FULLSTEP works both as a direct purchaser and as a purchasing broker for its clients, and shall act in their name during the purchasing process.
<br><br>
Access or use of Portal and / or Services does not give the SUPPLIER any rights on trademarks, commercial denominations or any other distinctive sign appearing in the portal, being FULLSTEP or third party’s property. Therefore, all contents are intellectual property of FULLSTEP or third party, and will not be understood as ceded to the SUPPLIER.<br><br>


2. Purpose<br><br>

The purpose of this contract is to regulate relations between FULLSTEP, FULLSTEP’s client and the SUPPLIER, in all aspects concerning use of the PORTAL.<br><br>


3.  SUPPLIER obligations <br><br>

The following are obligations of the SUPPLIER:<br><br>

a. Provide whatever data is necessary for the adequate functioning of the system, and maintain them updated, communicating at the earliest possible time any modifications.<br><br>

b. Guaranteeing the authenticity of data provided as a consequence of forms necessary to subscribe to the Services offered by the PORTAL. Also, the SUPPLIER will update all information provided to ensure it reflects, at all times, its real situation. Therefore, the SUPPLIER is solely responsible of damages caused to FULLSTEPor FULLSTEP’s client as a consequence of inexact or false statements.<br><br>

c. Keeping absolute confidentiality in relation to all information derived from the relations between the SUPPLIER and FULLSTEP or FULLSTEP’s client.<br><br>

d. Abstaining from doing any modification in the information-technology use of the Portal, as well as using it with purposes different from those for which it is intended. Also, the SUPPLIER will abstain from accessing unauthorized zones of the Portal.<br><br>

e. Comply loyally to the commitments expressed in the information sent through the portal. If the SUPPLIER does not demonstrate due commercial diligence, or does not comply with contracted duties, FULLSTEP and its clients reserves the right to exclude temporarily or permanently the SUPPLIER from the Portal.<br><br>

f. The SUPPLIER will indicate only those material groups referring to goods or services that, at the time of acceptance of the contract, are commercialized, manufactured or distributed by the SUPPLIER and of commercial interest to FULLSTEP and its client.<br><br>

g. The SUPPLIER accepts that offers sent through the portal are considered to be of the same rank and validity as offers sent through any other traditional means (letters, fax).<br><br>

h. The SUPPLIER is obliged to use the Portal and its Services in compliance with the Law, this Legal Notice, and the rest of regulations and instructions of which it is informed, as well as in accordance to generally accepted morals and customs, and public order.<br><br>

Thus, the SUPPLIER will abstain from using the Portal or any of its Services with illegal purposes, prohibited in this Legal Notice, and/or damaging to rights and interests of third parties.<br><br>

In particular, and merely as a matter of indication, the SUPPLIER agrees not to transmit, broadcast, or make available to others or third parties any information, data, contents, graphics, image and/or sound files, photographs, recordings, software and, in general, any type of material that:
<br><br>
1.Is contrary to fundamental rights and public liberties under the Constitution, International treaties and applicable laws;<br><br>

2.Induces, incites or promotes criminal acts, contrary to law, moral and generally accepted customs or public order;<br><br>

3.Is false, inexact or may induce to error, or constitutes illegal, deceitful or disloyal publicity;<br><br>

4.Is protected under intellectual or industrial property rights belonging to third parties without prior explicit and written consent.<br><br>

5.Contravenes laws and statutes for civil protection against defamation and invasion of privacy.<br><br>

6.Contravenes regulations about privacy and/or secrecy of communications.<br><br>

7.Can be considered as unfair competition or damages in any way the image of FULLSTEP or third parties.<br><br>

8.Is affected by viruses or similar elements that can harm or stop the Portal’s adequate performance, the electronic equipment or their files and documents.<br><br>


4. Rights of the SUPPLIER <br><br>

The following are rights of the SUPPLIER:<br><br>

1. Maintain a constant presence in FULLSTEP’s database, as a registered supplier.<br><br>

2. Receive offers according to the established rules. <br><br>


5. Obligations of FULLSTEP<br><br>

The following are FULLSTEP obligations:<br><br>

a. Maintain the information it deems fit constantly updated, without being liable or responsible of any mistakes that may happen by chance or due to circumstances beyond its control.<br><br>

b. Keep absolute confidentiality of all information concerning the SUPPLIER, both if supplied directly or generated as a consequence of relationships between the SUPPLIER and FULLSTEP.<br><br>

c. Provide the SUPPLIER, at any time, the situation of its data inside the database, and not to provide it to any third parties except to allow the SUPPLIER to present offers to be taken into consideration as supply alternatives. Also, to provide the SUPPLIER, at any time, the situation of its data inside the database, so that the SUPPLIER, with previous written notification, modifies or deletes them.<br><br>


6. Rights of FULLSTEP and its clients<br><br>

a. The SUPPLIER gives its explicit consent for FULLSTEP to communicate information concerning the SUPPLIER to associated companies or companies belonging to the same group, as well as others with which it may sign agreements with the only aim of providing the best services, in compliance, in any case, with Spanish legislation on personal data protection. Also, the SUPPLIER agrees on FULLSTEP or its associates, subsidiaries or affiliates, or investee companies, sending information about goods or services they commercialize, directly or indirectly, or that will be commercialized.<br><br>

b. Choose, in case of infringement by the SUPPLIER of its obligations, between demanding full compliance with its obligations or cancellation of the contract, in the terms expressed in clause 8 of this Legal Notice.<br><br>

7. Limitation of responsibility <br><br>

a. FULLSTEP shall not be liable or responsible for the availability of the service, network or technical failures that might cause an interruption or cuts in the Portal.

Equally, FULLSTEP shall not be liable of any damages caused by the spreading of IT viruses or other elements in the system.<br><br>

b. FULLSTEP shall not be liable or responsible of any payments and /or complaints that derive from the acts of clients concerning the agreement between the client(s) and the SUPPLIER.<br><br>

c. FULLSTEP shall not be liable or responsible of any infringement of the Laws or generally agreed Customs, or of acts that contravene obligations established in this Legal Notice, by any employee of FULLSTEP, by the SUPPLIER, or by any Internet or Network user.<br><br>

However, were FULLSTEP or its clients to have knowledge of any conduct the previous paragraph refers to, it shall adopt the necessary measures to resolve with the upmost diligence and urgency the conflicts caused.<br><br>

d. FULLSTEP shall not be held liable or responsible of any interference by unauthorized third parties in the knowledge of the conditions and circumstances of use that the SUPPLIER shall do of the Portal and its Services.<br><br>



8. Contract cancellation <br><br>

In the case of failure to comply with the obligations contained in this Legal Notice, the SUPPLIER or FULLSTEP and its clients must notify the breach to the infringing party, with a fifteen working day period to repair it. After that period has passed by and, should the infringement not be repaired, the other party can choose between the fulfilling and cancellation of this agreement. Should it choose to cancel the contract, both the SUPPLIER and FULLSTEP or its clients agree that simple notification shall be enough for the cancellation to have full effect.<br><br>


9. Duration<br><br>

The present agreement shall have indefinite duration as long as no party express willingness to cancel the contract with a prior notice not shorter than a month.

The proceeding to cancel the contract will be to send an e-mail indicating “PORTAL UNSUSCRIBE” to the address <a href="mailto:atencionalcliente@fullstep.com">atencionalcliente@fullstep.com</a><br><br>



10. Notifications, Legislation and Jurisdiction<br><br>

a. Notifications between the parties may be carried out through any of the means admitted by the Law, that allows having a record of reception, including fax.<br><br>

b. Changes in addresses and fax numbers shall be notified in writing, and shall not have effect until two working days from reception.<br><br>

c. The relationships between FULLSTEP and the SUPPLIER derived from the use of the Portal and the Services carried out inside it, will submit to adequate legislation and jurisdiction. 
 </p>
                                        </div>
                                    </div>
									
									<form name="frmAlta" id="frmAlta" method="post">                                      
                                      
											<div class="input-group">
                                            <label class="checkbox">
                                                    I have read and accept the sign up contract to this Supplier Portal.
                                                <input class="input -registro" type="checkbox" name="opcion1" id="opcion1" required>
                                                <span class="check">
                                            </label>
                                        </div> 
													<div class="input-group">
                                            <label class="checkbox">
                                                    He leído y Acepto el documento: "<a href="http://www.ree.es/sites/default/files/codigo_conducta_proveedores_nov2015.pdf" target="_blank" class="rojo">Código de Conducta para proveedores</a>" del Grupo Red Eléctrica y me comprometo a cumplir con los dispuesto en él en el momento de la aceptación del contrato.
                                                <input class="input -registro" type="checkbox" name="opcion2" id="opcion2" required>
                                                <span class="check">
                                            </label>
                                        </div>   
															<div class="input-group">
                                            <label class="checkbox">
                                                     He leído y acepto el documento: "<a href="http://www.ree.es/sites/default/files/08_PROVEEDORES/Documentos/20170711_cgc_equiposv3.pdf" target="_blank" class="rojo">Condiciones Generales de Contratación de equipos y materiales</a>” del Grupo Red Eléctrica y me comprometo a cumplir con lo dispuesto en él en el momento de la aceptación del contrato.
                                                <input class="input -registro" type="checkbox" name="opcion3" id="opcion3" required>
                                                <span class="check">
                                            </label>
                                        </div>   
																	<div class="input-group">
                                            <label class="checkbox">
                                                    He leído y acepto el documento: ”</span><a href="http://www.ree.es/sites/default/files/08_PROVEEDORES/Documentos/20170711_cgc_serviciosv1.pdf" target="_blank" class="rojo">Condiciones Generales de Contratación de servicios</a>”  del Grupo Red Eléctrica y me comprometo a cumplir con lo dispuesto en él en el momento de la aceptación del contrato.
                                                <input class="input -registro" type="checkbox" name="opcion4" id="opcion4" required>
                                                <span class="check">
                                            </label>
                                        </div>   
																			<div class="input-group">
                                            <label class="checkbox">
                                                    He leído y acepto el documento: ”<a href="http://www.ree.es/sites/default/files/08_PROVEEDORES/Documentos/20170711_cgc_serviciosv1.pdf" target="_blank" class="rojo">Condiciones Generales de Contratación de obras</a>”  del Grupo Red Eléctrica y me comprometo a cumplir con lo dispuesto en él en el momento de la aceptación del contrato.
                                                <input class="input -registro" type="checkbox" name="opcion5" id="opcion5" required>
                                                <span class="check">
                                            </label>
                                        </div>   
																					<div class="input-group">
                                            <label class="checkbox">
                                                    He leído y acepto el documento: ”</span><a href="http://www.ree.es/sites/default/files/08_PROVEEDORES/Documentos/Politica_de_Garantias_a_favor_de_REE_SAU_ed2.pdf" target="_blank" class="rojo">Política de garantías</a>”  del Grupo Red Eléctrica y me comprometo a cumplir con lo dispuesto en él en el momento de la aceptación del contrato.
                                                <input class="input -registro" type="checkbox" name="opcion6" id="opcion6" required>
                                                <span class="check">
                                            </label>
                                        </div>   
																							<div class="input-group">
                                            <label class="checkbox">
                                                    He leído y acepto el documento: ”</span><a href="http://www.ree.es/sites/default/files/20161019_guia_gestionsubcontrataciones.pdf" target="_blank" class="rojo">Guía de usuario para la gestión de subcontrataciones</a>”  del Grupo Red Eléctrica y me comprometo a cumplir con lo dispuesto en él en el momento de la aceptación del contrato.
                                                <input class="input -registro" type="checkbox" name="opcion7" id="opcion7" required>
                                                <span class="check">
                                            </label>
                                        </div>   
																									<div class="input-group">
                                            <label class="checkbox">
                                                    Declaro que las informaciones y documentos que facilite a al Grupo Red Eléctrica son veraces.
                                                <input class="input -registro" type="checkbox" name="opcion8" id="opcion8" required>
                                                <span class="check">
                                            </label>
                                        </div>   
																											<div class="input-group">
                                            <label class="checkbox">
                                                    Declaro que en caso de cualquier modificación de la información proporcionada me comprometo a comunicárselo al Grupo Red Eléctrica.
                                                <input class="input -registro" type="checkbox" name="opcion9" id="opcion9" required>
                                                <span class="check">
                                            </label>
                                        </div>   
                                                                                                                                                              
                                                                                                                                                           
                                        <div class="grid">
                                            <div class="(6/12) col"><input class="btn" type="submit" value="CONTINUAR" id="submit1" name="submit1"></div>
                                            <div class="(6/12) col">
                                                <div class="imprimir-container">
                                                    <a class="imprimir" href="../css/img/Condiciones-eng.pdf" target="_blank"><img src="../css/img/imprimir.png" alt=""></a> 
                                                </div>
                                            </div>
                                        </div>  
                                    </form>
								

<script type="text/javascript">

$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked') && $('#opcion2').is(':checked') && $('#opcion3').is(':checked') && $('#opcion4').is(':checked') && $('#opcion5').is(':checked') && $('#opcion6').is(':checked') && $('#opcion7').is(':checked') && $('#opcion8').is(':checked') && $('#opcion9').is(':checked')) {
        $('#frmAlta').submit();
        alta();
	}
	else
       alert("You must accept all the conditions to register");
    
});

</script>
                       </div>
                            </div>
                        </div>                        
                    </div>                   
                </main>
            </div>
        </div>  
</body>

<!--#include file="../../../common/fsal_2.asp"-->

</html>
