<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
 <link rel="stylesheet" href="../css/estilos.css">
 <link rel="stylesheet" href="../css/stylesheet-2.css">
        
        <!--[if lt IE 9]>
          	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body>
	<div class="container-portal-proveedores-2">
            <div class="compound" style="background-image: url(css/img/fondo.jpg)">
                <div class="shape-container -tablet">
                    <div class="shape"></div> 
                </div>                   
                <header class="cf">
                    <div class="(6/12) col"><div class="logo" style="background-image: url('../css/img/FullStep.svg');"></div></div>
                    <div class="(6/12) col"><h2 class="text text--right">SUPPLIER PORTAL</h2></div>
                   <div class="nav">
                            <a href="" class="nav_item active">
                                HOME
                            </a>
                            <a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/politica-de-compras.html" title="Pol&iacute;tica de compras" class="nav_item">
                                PURCHASE POLICY
                            </a>
                            <a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/ser-proveedor.html" title="Ser proveedor" class="nav_item">
                                BE A SUPPLIER
                            </a>
                            <a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/que-compramos.html" title="Qué compramos" class="nav_item">
                                WHAT WE PURCHASE
                            </a>
                            <a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/compra-responsable.html" title="Compra responsable" class="nav_item">
                                RESPONSIBLE PURCHASING
                            </a>
                            <a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/ayuda.html" title="Ayuda" class="nav_item">
                                HELP
                            </a>
				
                            <div href="" class="nav_item language">
                                <a href="../default.asp" >ESP</a> | <a href="" class="active">ENG</a>
                            </a></div> 
					</div>                     
                </header>              
                <main>                    
                    <div class="shape-container">
                        <div class="shape"></div> 
                    </div>       
                    <div class="compound_layer-text -right">  
                        <div class="login">
                            <div class="login_container">
                                <div class="login_header">
                                    <div class="grid">
                                        <div class="col col--left"> LOGIN</div>
                                    </div>
                                </div>
                                <div class="hr"></div>
                                <div class="login_body">
                                    <span>FILL IN THE DETAILS TO CHANGE YOUR PASSWORD</span>
                                    <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
                                        <div class="input-group">
                                            <label for="user">COMPANY<span>*</span></label>
                                            <input class="input input--login" type="text" name="txtCia" id="txtCia" placeholder="Your Company" required />
                                        </div>
                                        <div class="input-group">
                                            <label for="user">USER<span>*</span></label>
                                            <input class="input input--login" type="text" name="txtUsu" id="txtUsu" placeholder="Your User" required />
                                        </div>
                                        <div class="input-group">
                                            <div class="grid">
                                                <div class="(5/12) col"> <label for="mail">EMAIL<span>*</span></label> </div>
                                            </div>
                                            <input class="input input--login" type="text" name="txtEmail" id="txtEmail" placeholder="Your email" required />
                                        </div>
                                        <div class="input-group">
                                            <input class="input input--login" name="cmdEnviar" type="submit" value="SUBMIT"> 
                                        </div>
<input type="hidden" name="idioma" value="eng"/>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>                                 
                    <div class="compound_layer-text -left">                                               
                        <div class="content">
                            <div class="content_text">
                                <h1>LOGIN INFORMATION</h1>
                                <p>
                                    If you forgot your password, please fill in your user and email and click on "Submit". 
                                </p>
                                <p>
                                    You will receive a link to a page to create a new password.
                                </p>
                           
                            <div class="content_soporte content_soporte--proveedores">
                                <h6>SUPPLIER SUPPORT</h6>
                                <div class="container">
                                    <span>Tel. <a href="tel:902996926"></a>+34 902 996 926</span>
                                    <span><a href="mailto:atencionalcliente@fullstep.com">atencionalcliente@fullstep.com</a></span>
                                    <div class="hr"></div>
                                    <span>Customer service schedule</span>
                                    <span>Monday - Thursday: 8:00 - 21:00</span>
                                    <span>Friday: 8:00 - 19:00</span>
                                </div>
                            </div>                                                      
                        <!--    <div class="content_footer">                                
                                <div class="col align align--bottom">
                                    <a class="links fz--12" href="">Ayuda</a>
                                    <a class="links fz--12" href="">Aviso legal</a>
                                    <a class="links fz--12" href="">Política de Cookies</a>
                                </div>
                                <div class="col text text--right align--bottom"><img src="css/img/FullStepPro.svg" alt="Powered by Fullstep Pro"></div>
                            </div>-->
                        </div>
                    </div>
               
            </div>
					 </main>
        </div> 
	 </div> 
</body>

</html>
