﻿<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>
<html class="no-js" lang="es">
<head>
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, user-scalable=no">
 		<title>::Espacio de proveedores::</title>
        <link rel="icon" type="image/gif" href="favicon.ico">
        <link rel="image_src" href="tile.png">
		<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
		<link rel="stylesheet" href="css/estilos.css">
		<link rel="stylesheet" href="css/stylesheet.css">
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
	ul{
		margin:5px 20px !important;
	}
	li{
		line-height: 22px;
		margin-bottom: 20px;
		font-size: 14px;
	}
	b{
	font-weight: 600 !important;
	}
</style>
</head>
<script>
dibujaMenu(1)
</script>
<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function Init() {
    document.getElementById('tablemenu').style.display = 'block';

	p = window.top.document.getElementById("frSet")
	vRows = p.rows
	vArrRows = vRows.split(",")
	vRows = vArrRows[0] + ",*,0,0"
	p.rows = vRows

}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">
	

	<!-- en revisión-->
	<div class="container-portal-proveedores">
            <div class="compound">
        
               <main>
                    <div class="shape-container">
                        <div class="shape"></div> 
                    </div>                    
                    <div class="compound_layer-text -left">                                               
                        <div class="content">
                            <div class="content_text">
							  <h1>Bienvenido al Espacio de Proveedores de Red Eléctrica</h1>
							  <p>Puede acceder a las distintas áreas a través de las opciones del menú situado en la parte superior:</p>
							  <ul>
								<li><b>Cuestionarios:</b> Acceda y gestione los cuestionarios y la información contenida en ellos, tanto de los cumplimentados como de los pendientes de cumplimentar.</li>							
								<li><b>Sus datos / Su compañía:</b> Si lo desea puede modificar los datos de su empresa, usuarios, así como las áreas de actividad de su empresa.</li>
								<li><b>Calificación:</b> A través de este espacio puede crear un expediente de calificación, acceder a los expedientes en curso o finalizados y solicitar la cancelación de un expediente en curso. </li>
								<li><b>Ficha de proveedor:</b> Desde aquí puede consultar toda la información que ha aportado sobre su compañía, así como otros aspectos legales, documentales y financieros que nos han facilitado las empresas Achilles y Axesor. </li>
							  </ul>
								
							</div>
                          <div class="content_footer">                                
                            </div>
                        </div>
                    </div>
                    <div class="compound_layer-text -right">  
                        <div class="content">
                            <div class="content_text">
                                <div class="push">
                                    <h2 class="color -red">Instrucciones</h2>
                                    <p>Descárguese las instrucciones sobre cómo proceder en las distintas áreas del espacio de proveedores</p>
                                    <ul>
                                        <li><a href="../spa/docs/FSN_MAN_ATC_Cómo ofertar.pdf" target="_blank">Calificación</a></li>
                                        <li><a href="../spa/docs/FSN_MAN_ATC_Requisitos técnicos.pdf" target="_blank">Requisitos técnicos</a></li>
                                        <li><a href="../spa/docs/FSN_MAN_ATC_Mantenimiento de datos.pdf" target="_blank">Mantenimiento de datos</a></li>
                                    </ul>                                    
                                </div>
                                <div class="img full-width" style="background-image: url('css/img/telefono.jpg');"></div>
                                <div class="push">
                                    <p>Si tiene alguna duda o dificultad para operar con el Espacio de Proveedores, póngase en contacto con nosotros a través del mail: <a href="mailto:soporteprored@fullstep.com">soporteprored@fullstep.com</a> o llamando al teléfono: <a href="tel:+34 91 077 03 04">+34 91 077 03 04</a></p>                                
                                </div>                                
                            </div>
                        </div>
                    </div>
              </main>
            </div>
        </div>     

</body>
</html>
