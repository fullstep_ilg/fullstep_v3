﻿<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
 <link rel="stylesheet" href="css/estilos.css">
 <link rel="stylesheet" href="css/stylesheet-2.css">
        
        <!--[if lt IE 9]>
          	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body>
	<div class="container-portal-proveedores-2">
            <div class="compound" style="background-image: url(css/img/fondo.jpg)">
                <div class="shape-container -tablet">
                    <div class="shape"></div> 
                </div>                   
                <header class="cf">
                    <div class="(6/12) col"><div class="logo" style="background-image: url('css/img/logo.png');"></div></div>
                    <div class="(6/12) col"><h2 class="text text--right">ESPACIO DE PROVEEDORES</h2></div>
				   <div class="nav">
					<a href="../default.asp" class="nav_item active">
						INICIO
					</a>
					<a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ser-proveedor-re.html" title="Ser proveedor" class="nav_item">
						 Ser proveedor del grupo RE
					</a>
					<a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/evaluacion-prov.html" title="Qué compramos" class="nav_item">
						 Evaluación de nuestros proveedores
					</a>
					<a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/sostenibilidad-sum.html" title="Compra responsable" class="nav_item">
						 Sostenibilidad en la cadena de suministro
					</a>
				</div>                     
                </header>              
                <main>                    
                    <div class="shape-container">
                        <div class="shape"></div> 
                    </div>       
                    <div class="compound_layer-text -right">  
                        <div class="login">
                            <div class="login_container">
                                <div class="login_header">
                                    <div class="grid">
                                        <div class="col col--left"> RECORDAR </div>
                                    </div>
                                </div>
                                <div class="hr"></div>
                                <div class="login_body">
                                    <span>RELLENE LOS DATOS PARA CAMBIAR SU CLAVE</span>
                                    <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
                                        <div class="input-group">
                                            <label for="user">COMPAÑÍA<span>*</span></label>
                                            <input class="input input--login" type="text" name="txtCia" id="txtCia" placeholder="Código de Compañía" required />
                                        </div>
                                        <div class="input-group">
                                            <label for="user">USUARIO<span>*</span></label>
                                            <input class="input input--login" type="text" name="txtUsu" id="txtUsu" placeholder="Usuario" required />
                                        </div>
                                        <div class="input-group">
                                            <div class="grid">
                                                <div class="(5/12) col"> <label for="mail">EMAIL<span>*</span></label> </div>
                                            </div>
                                            <input class="input input--login" type="text" name="txtEmail" id="txtEmail" placeholder="Tu dirección de email" required />
                                        </div>
                                        <div class="input-group">
                                            <input class="input input--login" name="cmdEnviar" type="submit" value="ENVIAR"> 
                                        </div>
<input type="hidden" name="idioma" value="spa"/>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>                                 
                    <div class="compound_layer-text -left">                                               
                        <div class="content">
                            <div class="content_text">
                                <h1>RECORDATORIO DE CLAVES</h1>
                                <p>
                                    Si ha olvidado su contraseña, introduzca su código de compañía, su usuario y la dirección de e-mail de su cuenta de acceso y pulse "Enviar". 
                                </p>
                                <p>
                                    Recibirá un e-mail con el enlace a la página donde podrá crear una nueva contraseña.
                                </p>
                           
                            <div class="content_soporte content_soporte--proveedores">
                                <h6>ATENCIÓN A PROVEEDORES</h6>
                                <div class="container">
                                    <span>Tel. <a href="tel:+34 91 077 03 04"></a>+34 91 077 03 04</span>
                                    <span><a href="mailto:soporteprored@fullstep.com">soporteprored@fullstep.com</a></span>
                                    <div class="hr"></div>
                                    <span>Horario de Atención al Cliente</span>
                                    <span>Lunes a Jueves: 9:00 - 21:00</span>
                                    <span>Viernes: 9:00 - 19:00</span>
                                </div>
                            </div>                                                      
                        <!--    <div class="content_footer">                                
                                <div class="col align align--bottom">
                                    <a class="links fz--12" href="">Ayuda</a>
                                    <a class="links fz--12" href="">Aviso legal</a>
                                    <a class="links fz--12" href="">Política de Cookies</a>
                                </div>
                                <div class="col text text--right align--bottom"><img src="css/img/FullStepPro.svg" alt="Powered by Fullstep Pro"></div>
                            </div>-->
                        </div>
                    </div>
                </main>
            </div>
        </div> 
	
</body>

</html>
