﻿<!--<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>
<html class="no-js" lang="es">
<head>
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, user-scalable=no">
 		<title>::Espacio de proveedores::</title>
        <link rel="icon" type="image/gif" href="favicon.ico">
        <link rel="image_src" href="tile.png">
		<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
		<link rel="stylesheet" href="css/estilos.css">
		<link rel="stylesheet" href="css/stylesheet.css">
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
	ul{
		margin:5px 20px !important;
	}
	li{
		line-height: 22px;
		margin-bottom: 20px;
		font-size: 14px;
	}
	b{
	font-weight: 600 !important;
	}
</style>
</head>
<script>
dibujaMenu(1)
</script>
<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function Init() {
    document.getElementById('tablemenu').style.display = 'block';

	p = window.top.document.getElementById("frSet")
	vRows = p.rows
	vArrRows = vRows.split(",")
	vRows = vArrRows[0] + ",*,0,0"
	p.rows = vRows

}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">
	

	<!-- en revisión-->
	<div class="container-portal-proveedores">
            <div class="compound">
        
               <main>
                    <div class="shape-container">
                        <div class="shape"></div> 
                    </div>                    
                    <div class="compound_layer-text -left">                                               
                        <div class="content">
                            <div class="content_text">
							  <h1>Bienvenido al Espacio de Proveedores de Red Eléctrica</h1>
							  <p>Puede acceder a las distintas áreas a través de las opciones del menú situado en la parte superior:</p>
							  <ul>
								<li><b>Cuestionarios:</b> Acceda y gestione los cuestionarios y la información contenida en ellos, tanto de los cumplimentados como de los pendientes de cumplimentar.</li>							
								<li><b>Sus datos / Su compañía:</b> Si lo desea puede modificar los datos de su empresa, usuarios, así como las áreas de actividad de su empresa.</li>
								<li><b>Calificación:</b> A través de este espacio puede crear un expediente de calificación, acceder a los expedientes en curso o finalizados y solicitar la cancelación de un expediente en curso. </li>
								<li><b>Ficha de proveedor:</b> Desde aquí puede consultar toda la información que ha aportado sobre su compañía, así como otros aspectos legales, documentales y financieros que nos han facilitado las empresas Achilles y Axesor. </li>
							  </ul>
							  
							<!--
                                <p>Si es la primera vez que va a realizar una oferta a través del portal, siga atentamente los siguientes pasos:</p>
                                <div class="steps">
                                    <div class="step">
                                        <div class="step_number">1</div>
                                        <div class="step_content">
                                            <div class="step_description">
                                                    <p>Pulse en "solicitudes de oferta" para ver las peticiones de oferta que tiene abiertas su empresa.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="step">
                                        <div class="step_number">2</div>
                                        <div class="step_content">
                                            <div class="step_description">
                                                <p>Seleccione la solicitud de oferta que quiera responder, pulsando sobre el código de la misma.</p>
                                            </div>
                                        </div>
                                    </div>                                         
                                    <div class="step">
                                        <div class="step_number">3</div>
                                        <div class="step_content">
                                            <div class="step_description">
                                                <p>Realice su oferta completando toda la información necesaria: desde el árbol de navegación que encontrará en la parte izquierda, podrá desplazarse por los distintos apartados que conforman la oferta. Para introducir los precios deberá ir al apartado "items/precios". Recuerde introducir el plazo de validez de la oferta, en el apartado "Datos Generales de la oferta".</p>
                                                
                                            </div>
                                        </div>
                                    </div>                                               
                                    <div class="step">
                                        <div class="step_number">4</div>
                                        <div class="step_content">
                                            <div class="step_description">
                                                <p>Comunique su oferta pulsando sobre el botón de enviar.</p>
                                                
                                            </div>
                                        </div>
                                    </div>                                               
                                </div>                                
							-->
							
							</div>
                          <div class="content_footer">                                
                               <!--   <div class="col align align--bottom">
                                    <a class="links fz--12" href="">Ayuda</a>
                                    <a class="links fz--12" href="">Aviso legal</a>
                                    <a class="links fz--12" href="">Política de Cookies</a>
                                </div>
                                <div class="col text text--right align--bottom"><img src="css/img/FullStepPro.svg" alt="Powered by Fullstep Pro"></div>-->
                            </div>
                        </div>
                    </div>
                    <div class="compound_layer-text -right">  
                        <div class="content">
                            <div class="content_text">
                                <div class="push">
                                    <h2 class="color -red">Instrucciones</h2>
                                    <p>Descárguese las instrucciones sobre cómo proceder en las distintas áreas del espacio de proveedores</p>
                                    <ul>
                                        <li><a href="docs/Guia_Uso_Proveedor.pdf" target="_blank">Calificación</a></li>
                                        <li><a href="docs/FSN_MAN_ATC_Requisitos técnicos.pdf" target="_blank">Requisitos técnicos</a></li>
                                        <li><a href="docs/FSN_MAN_ATC_Mantenimiento de datos.pdf" target="_blank">Mantenimiento de datos</a></li>
                                    </ul>                                    
                                </div>
                                <div class="img full-width" style="background-image: url('css/img/telefono.jpg');"></div>
                                <div class="push">
                                    <p>Si tiene alguna duda o dificultad para operar con el Espacio de Proveedores, póngase en contacto con nosotros a través del mail: <a href="mailto:soporteprored@fullstep.com">soporteprored@fullstep.com</a> o llamando al teléfono: <a href="tel:+34 91 077 03 04">+34 91 077 03 04</a></p>                                
                                </div>                                
                            </div>
                        </div>
                    </div>
              </main>
            </div>
        </div>     

</body>
</html>-->
