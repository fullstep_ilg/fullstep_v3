﻿<%@ Language=VBScript %>
<!--#include file="../../../common/acceso.asp"-->
<script SRC="../../../common/menu.asp"></script>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<style>
#divNoticias {position:relative;top:1px;width:100%;height:95%;overflow-y:scroll;color:"#000000";padding-right:10px;}
</style>
</head>
<script>
function init()
{
        document.getElementById('tablemenu').style.display = 'block';
        p = window.top.document.getElementById("frSet")
        vRows = p.rows
        vArrRows = vRows.split(",")
        vRows = vArrRows[0] + ",*,0,0"
        p.rows = vRows

}
</script>

<body topmargin="0" leftmargin="0" scroll="no" onload="init()">
<script>
dibujaMenu(1)
</script>

<%
Idioma = Request.Cookies("USU_IDIOMA")

set oRaiz = ValidarUsuario(Idioma,false,false,0)

DIM FICHERO_SPA 
FICHERO_SPA= Application("PATHFICHERONOTICIAS") & ".SPA"
sNoticias = oRaiz.leerFicheroTexto(FICHERO_SPA)

set oRaiz = nothing

%>
<!-------------------------- BEGIN COPYING THE HTML HERE ---------------------------->

<table align = center border="0" cellpadding="0" cellspacing="0" style="BORDER-COLLAPSE: collapse;" bordercolor="#111111" width="95%" height=80%  bgcolor="#ffffff">
	<tr>
		<td colspan = 3>
		<span style="font-size:3px"><BR><BR></SPAN>
		</td>
	</tr>
	<tr>
		<td width="50%">
			<font face="Arial" size="2"><b>&gt;Bienvenido a la zona privada de proveedores del grupo Huf</b></font>
		</td>
		<td width = 5px rowspan =6>&nbsp;
		
		</td>
		<td width="50%" align="left">
		<font size="2" face="Arial" color="#000000"><b>Noticias</b></font></span>
		</td>
	</tr>
	<tr>
		<td valign=top><hr color="#000000">
		<font face="Arial" size="2">Aquí encontrará las solicitudes de oferta que el grupo Huf tiene para su compañia.<span style="font-size:3px"><BR><BR></SPAN>
		Puede acceder a las distintas áreas através de los vínculos situados en la parte superior.</font>
		</td>
		<td width="50%" valign="top" rowspan =5 align="left" height= 100%>
			<hr color="#000000">
			<div name="divNoticias" id="divNoticias"><%=sNoticias%></div>
		</td>
	</tr>
	<tr>
		<td valign = top>
		<font face="Arial" size="2">
		<li><b>Sus datos/ su compañia :</b> Puede gestionar los datos de su empresa, así como las áreas de actividad en las que su empresa se encuentra homologada.
		</font>
		</td>
	
	</tr>
	<tr>
		<td valign = top>
		<font face="Arial" size="2">
		<li><b>Solicitudes de ofertas :</b> Puede acceder a los procesos de compra abiertos por el grupo Huf, para los que su empresa ha sido invitada.
		</font>
		</td>
	
	</tr>
	<tr>
		<td width="50%" valign=bottom>
			<hr color="#000000" size="1">
			<font face="Arial" size="2">Si es la primera vez que accede a esta zona, puede descargar las instrucciones sobre como ofertar, requisitos, etc. ...pulsando en el icono correpondiente.</font>
			<hr color="#000000" size="1">
		</td>
	</tr>
	<tr>
		<td>
			<table cellSpacing="1" cellPadding="1" width="100%" border="0">
				<tr>
					<td>&nbsp;</td>	
					<td width="4%"><a href="docs/FSN_SOP_MAN_ 3_Mantenimiento_de_datos.pdf" target="_blank"><img height="26" src="../images/pdf.gif" width="26" border="0"></a></td>
					<td><font face="Arial" size="2">Mantenimiento de datos</font></td>
					<td width="4%"><a href="docs/FSN_SOP_MAN_ 4_Como_ofertar.pdf" target="_blank"><img height="26" src="../images/pdf.gif" width="26" border="0"></a></td>
					<td><font face="Arial" size="2">Cómo&nbsp;ofertar&nbsp;</font></td>
					<td width="2%"><a href="docs/FSN_SOP_MAN_ 1_Vision_General_del_Proveedor.pdf" target="_blank"><img height="26" src="../images/pdf.gif" width="26" border="0"></a></td>
					<td><font face="Arial" size="2">Visión General</font></td>
					<td width="3%"><a href="docs/FSN_SOP_MAN_ 2_Requisitos_tecnicos.pdf" target="_blank"><img height="26" src="../images/pdf.gif" width="26" border="0"></a></td>
					<td><font face="Arial" size="2">Requisitos técnicos</font></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="50%" colspan=3>
		<span style="font-size:2px"><BR><BR></SPAN>
		</td>
	<tr>
	<tr>
		<td width="100%" height="6" bgcolor="#e5e5e5" colspan="3">
		<p align="center">&nbsp;<b><font face="Arial" size="2" color="#FF0000">Para resolver sus dudas o hacernos llegar sus comentarios, póngase en contacto con nosotros enviando un correo electrónico  a: supplier@huf-group.com</font></b></p></td>
	</tr>
</table>
</body></html>
