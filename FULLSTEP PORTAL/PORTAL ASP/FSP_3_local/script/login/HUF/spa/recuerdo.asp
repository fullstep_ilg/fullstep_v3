﻿<%@  language="VBScript" %>
<!--#include file="../../recuerdo.asp"-->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="../estilos.css" rel="stylesheet" type="text/css">
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body>
    <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
    <input type="hidden" name="idioma" value="spa">
    <table width="620" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="20">&nbsp;
                
            </td>
            <td width="615" align="left">
                <img height="90" alt src="../images/banner_en_520.gif" width="520" border="0"><img
                    src="../images/huf_2000.gif" width="60" height="75" align="top">
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;
                
            </td>
        </tr>
        <tr>
            <td width="20">&nbsp;
                
            </td>
            <td width="615" align="left">
                <font color="#666666" size="1" face="Verdana" class="textos"><b><u>Recordatorio de claves
                    de acceso</u></b>
                    <br>
                    <br>
                    Si ha olvidado sus claves de acceso, introduzca su código de compañía, código de usuario y la direcci&oacute;n de e-mail registrada
                    en el Portal, y a continuaci&oacute;n recibir&aacute; un e-mail con sus claves.<br>
                    <br>
                    <table class="textos">
                        <tr>
                            <td>Cód. Compañia:</td>
                            <td><input type="text" name="txtCia" id="txtCia" size="80" maxlength="100"/></td>
                        </tr>
                        <tr>
                            <td>Cód. Usuario:</td>
                            <td><input type="text" name="txtUsu" id="txtUsu" size="80" maxlength="100/"></td>
                        </tr>
                        <tr>
                            <td>Dirección de email:</td>
                            <td><input type="text" name="txtEmail" id="txtEmail" size="80" maxlength="100"/></td>
                        </tr>
                    </table>
                    <input  style="font-size:10px" type = submit value="Enviar" name=cmdEnviar id=cmdEnviar>
                    <br>
                    <br>
                    <br>
                    Si continua con dificultades para acceder al portal, o su direcci&oacute;n de email
                ha cambiado, env&iacute;enos un email a la direcci&oacute;n: <a href="mailto:supplier@huf-group.com">supplier@huf-group.com</a> </font>
            </td>
        </tr>
    </table>
</body>
</html>

