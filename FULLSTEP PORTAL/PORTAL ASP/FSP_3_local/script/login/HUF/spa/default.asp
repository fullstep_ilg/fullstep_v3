﻿<%@ Language=VBScript %>
<!--#include file="../../login.asp"-->
<%
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden")
CiaComp=Request.QueryString ("CiaComp")

if IdOrden = "" then
        IdOrden = Request.Form ("IdOrden")
        CiaComp = Request.Form ("CiaComp")
end if

if Not IdOrden = "" then
    if Not IsNumeric(IdOrden) then
        IdOrden=""
    end if
end if

if Not CiaComp = "" then
    if Not IsNumeric(CiaComp) then
        CiaComp=""
    end if
end if

Idioma="SPA"

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Huf Hülsbeck &amp; Fürst: Suppliers  Area</title><!-- Icons --><link href="/favicon.ico" rel="SHORTCUT ICON"><link href="/favicon.ico" rel="ICON"><!-- Metatags normal -->
<meta content="Huf Hülsbeck &amp; Fürst GmbH &amp; Co. KG, elektronische und mechanische Schließsysteme für die Automobilindustrie, electronic and mechanical lock systems for the automotive industry" name="description">
<meta content="Huf Hülsbeck &amp; Fürst" name="author">
<meta content="Huf, Automobilzulieferer, elektronische mechanische Schließsysteme, Automotive, Keyless Go, Passive Entry, Locks, CASIM, Autozubehör, Autoschlüssel, Fernbedienung, Türschloss, Lenkschloss, Heckschloss, Autotürgriff, Türgriff, car keys, latch, grip, electronic mechanical lock systems" name="keywords">
<meta content name="date">
<meta content="index,follow" name="robots">
<meta http-equiv="expires" content="43200"><!-- Metatags nach Dublin Core -->
<meta content="Huf Hülsbeck &amp; Fürst" name="DC.Title">
<meta content="Huf Hülsbeck &amp; Fürst" name="DC.Creator">
<meta content="Huf Hülsbeck &amp; Fürst GmbH &amp; Co. KG, elektronische und mechanische Schließsysteme für die Automobilindustrie, electronic and mechanical lock systems for the automotive industry" name="DC.Subject">
<meta content="Huf Hülsbeck &amp; Fürst GmbH &amp; Co. KG, elektronische und mechanische Schließsysteme für die Automobilindustrie, electronic and mechanical lock systems for the automotive industry" name="DC.Description">
<meta content="Huf Hülsbeck &amp; Fürst" name="DC.Publisher">
<meta content="Huf Hülsbeck &amp; Fürst" name="DC.Contributor">
<meta content="Text" name="DC.Type">
<meta content="text/html" name="DC.Format">
<meta content="http://www.huf-group.com/" name="DC.Identifier">
<!-- Stylesheetdefinitionen-->
<meta content="Microsoft FrontPage 4.0" name="GENERATOR">
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
function ventanaLogin (IDI){

   window.open ("<%=application("RUTASEGURA")%>/script/registro/registro.asp?Idioma="+IDI,"","top=10,width=700,height=700,resizable=yes")

}
function ventanaSecundaria (URL){

   window.open(URL,"ventana1","width=750,height=500,scrollbars=YES")
}

function recuerdePWD()
{
   window.open("recuerdo.asp","_blank","width=641,height=360,scrollbars=NO")
}//-->
</script>
<link href="../estilos.css" rel="stylesheet" type="text/css">
</head>
<body bgColor="#ffffff" leftMargin="0" topMargin="0" onLoad="MM_preloadImages('../images/FENTERr2.gif')" marginwidth="0" marginheight="0">

<div align="center">
<table border="0" cellspacing="0" cellpadding="0">
 <tr>
  <td><img src="../images/logo_schrift_de_en.gif" width="557" height="77" border="0" alt="Elektronische und mechanische Schlie&szlig;systeme f&uuml;r die Autoindustrie" title="Elektronische und mechanische Schlie&szlig;systeme f&uuml;r die Autoindustrie"></td>
  <td><img src="../images/logo.gif" width="63" height="77" border="0" alt="Huf H&uuml;lsbeck und F&uuml;rst"></td>
 </tr>
 <tr>
  <td><br></td>
 </tr>
</table>

<table width="620" cellspacing="0" cellpadding="0">
 <tr>
   <td width="" valign="middle"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal">Bienvenido al portal de compras de Huf</span><br><br>
   </td>
   <td width=140>&nbsp;</td>
   <td width="" align="right">
    <a href="../default.asp"><img src="../images/flag_eng%20portal.gif"" alt="Supplier Portal" width="102" height="15" border="0"></a>
   </td>
   <td width="" align="right">
    <a href="../ger/default.asp"><img src="../images/flag_ger_portal.gif" alt="Lieferantenportal" height="15" border="0"></a>
   </td>
 </tr>
</table>

<table width="620" border="0" cellspacing="0" cellpadding="0">
 <tr>
  <td colspan="1"><div align="center"><img src="../images/foto1.jpg" width="150" height="98"></div>
  </td>
  <td width="19">&nbsp;</td>
  <td colspan="1"><div align="center"><img src="../images/foto2.jpg" width="150" height="103"></div>
  </td>
  <td width="19">&nbsp;</td>
  <td colspan="1"><div align="center"><img src="../images/foto3.jpg" width="170" height="98"></div>
  </td>
 </tr>
 <tr>
  <td colspan="1">&nbsp;</td>
  <td width="19">&nbsp;</td>
  <td colspan="1">&nbsp;</td>
  <td width="19">&nbsp;</td>
  <td colspan="1">&nbsp;</td>
 </tr>
 <tr>
  <td width="" valign="baseline" class="textos">¿Todavía no se ha dado de alta? Si desea ser proveedor del Grupo Huf, por favor solicite el alta desde esta sección.
  </td>
  <td width="19">&nbsp;</td>
  <td width="" valign="baseline" class="textos">Si ya está dado de alta como proveedor, puede acceder al portal de compras a través de esta sección:
  </td>
  <td width="19">&nbsp;</td>
  <td width="" valign="baseline" class="textos">Si quiere más información sobre nosotros, por favor visite nuestra página web:
  </td>
 </tr>
 <tr>
  <td onMouseOver="this.bgColor='#bfbfbf'" onMouseOut="this.bgColor='#e5e5e5'" bgcolor="#e5e5e5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/navipfeil4.gif" width="11" height="12"><span class="titulo"><a href="javascript:ventanaLogin('ENG')">Solicitar el alta</a></span></td>
  <td width="19" bgcolor="#e5e5e5">&nbsp;</td>
  <td onMouseOver="this.bgColor='#bfbfbf'" onMouseOut="this.bgColor='#e5e5e5'" bgcolor="#e5e5e5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/navipfeil4.gif" width="11" height="12"><span class="titulo" style="color: #c00">Acceso al portal</span></td>
  <td width="19" bgcolor="#e5e5e5">&nbsp;</td>
  <td onMouseOver="this.bgColor='#bfbfbf'" onMouseOut="this.bgColor='#e5e5e5'" bgcolor="#e5e5e5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/navipfeil4.gif" width="11" height="12"><span class="titulo"><a href="http://www.huf-group.com">Huf Group</a></span> </td>
 </tr>
</table>

<table width="620" border="0" cellspacing="0" cellpadding="0">
 <tr>
  <td width="">&nbsp;</td>
  <td width="">&nbsp;</td>
  <form name="frmLogin" id="frmLogin" method="post" action="default.asp">
   <table width="173" border="0" cellspacing="0" cellpadding="0">
    <tr>
     <input type="hidden" id="Idioma" name="Idioma" value="SPA">
     <input type="hidden" id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
     <input type="hidden" id="CiaComp" name="CiaComp" value="<%=CiaComp%>">
     <td width="69" class="formulario">Cód.Compañia</td>
     <td width="88"><input id="txtCia" name="txtCIA" maxlength="20" size="10"></td>
    </tr>
    <tr>
     <td width="69" class="formulario">Código Usuario</td>
     <td width="88"><input type="text" name="txtUSU" size="10"></td>
    </tr>
    <tr>
     <td width="69" class="formulario">Password</td>
     <td width="88"><input name="txtPWD" type="password" maxlength="20" size="10"></td>
    </tr>
    <tr>
     <td width="69">&nbsp;</td>
     <td width="88"><input type="image" name="cmdEntrar" src="../images/entrar1f_spa.gif" width="50" height="20" border="0" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cmdEntrar','','../images/entrar2f_eng.gif',1)"></td>
    </tr>
    <tr>
     <td colspan="2" align=center><div align="left"><a class="formulario" href="javascript:void(null)"  onclick="recuerdePWD()">¿Olvidó sus claves de acceso? </a></div>                           </td>
    </tr>
   </table>
  </form>
  </td>
  <td width=""></td>
 </tr>
</table>

<table width="620" border="0" cellspacing="0" cellpadding="0" vspace="0">
 <tr>
  <td align="center" class="subtextos">Sitio web optimizado para 1280 x 1024<br>
              Navegadores soportados: <a href="http://windows.microsoft.com/es-ES/internet-explorer/download-ie" target="_blank">Internet Explorer</a> y <a href="http://www.ez-download.com/mozilla-firefox/?kw=firefox&subid=EZFFES&cust=firefox+download+mozilla&type=firefox&gclid=CN6MtdLfprQCFaTKtAodDWAAgQ&utm_campaign=EZFFES&fwd=1" target="_blank">Mozilla Firefox</a></td>
 </tr>
</table>

<table cellSpacing="0" cellPadding="0" border="0">
 <tbody>
  <tr>
   <td width="428" align="middle" valign="middle" noWrap><p class="bottom">© 2015 Huf Hülsbeck &amp; Fürst GmbH &amp; Co. KG - <a href="http://www.huf-group.com/es/servicenavi/contact.html" target="_blank" class="bottom" style="color: #c00">Contacto</a> - <a href="http://www.huf-group.com/es/servicenavi/imprint.html" target="_blank" class="bottom" style="color: #c00">Aviso Legal</a> - <a href="javascript:ventanaSecundaria('../../../../custom/huf/spa/terms.htm')" class="bottom" style="color: #c00">Condiciones de uso</a>
		- <a href="<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=SPA" class="formulario">Pol&iacute;tica de Cookies</a> 
   </p>   
   </td>
   <td align="left" width="192"><div align="center"><img alt="... the intelligent touch to cars." src="../images/slogan.gif" border="0" WIDTH="145" HEIGHT="90"></div>
   </td>
  </tr>
 </tbody>
</table>
<table>
  <tr>
    <td class="subtitulo" rowspan="3" align="right" valign="top" width="13%">&nbsp;</td>
    <td class="subtitulo" valign="bottom" width="3%">&nbsp;</td>
    <td width="31%" rowspan="2" valign="bottom" class="subtitulo">powered by</td>
    <td rowspan="2" valign="bottom" class="subtitulo"><a href="http://www.fullstep.com/eng/index.htm" target="_blank"><img src="../images/logo_fullstep.png" alt="Fullstep" width="100" height="24" border="0"></a></td>
    <td class="subtitulo" align="left" valign="bottom" width="5%">&nbsp;</td>
    <td rowspan="2"  valign="bottom" width="12%">&nbsp;</td>
  </tr>
  <tr>
    <td class="subtitulo" height="17" valign="top" width="3%">&nbsp;</td>
    <td class="subtitulo" align="left" height="17" valign="top" width="5%">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top" width="5%">&nbsp;</td>
    <td align="right" valign="bottom" width="12%">&nbsp;</td>
  </tr>
</table>
</div>
</body>
</html>
