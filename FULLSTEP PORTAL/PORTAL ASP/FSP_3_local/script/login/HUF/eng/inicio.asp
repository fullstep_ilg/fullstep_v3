﻿<%@ Language=VBScript %>
<!--#include file="../../../common/acceso.asp"-->
<script SRC="../../../common/menu.asp"></script>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<style>
#divNoticias {position:relative;top:1px;width:100%;height:95%;overflow-y:scroll;color:"#000000";padding-right:10px;}
</style>
</head>
<script>
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo mÃ¡ximo:0</remarks>*/
function init() {
    document.getElementById('tablemenu').style.display = 'block';

    p = window.top.document.getElementById("frSet")
    vRows = p.rows
    vArrRows = vRows.split(",")
    vRows = vArrRows[0] + ",*,0"
    p.rows = vRows

}
</script>
<body topmargin="0" leftmargin="0" scroll="no"  onload="init()">
<script>
dibujaMenu(1)
</script>
<%
Idioma = Request.Cookies("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,false,false,0)

DIM FICHERO_ENG 
FICHERO_ENG= Application("PATHFICHERONOTICIAS") & ".ENG"
sNoticias = oRaiz.leerFicheroTexto(FICHERO_ENG)

set oRaiz = nothing

%>
<!-------------------------- BEGIN COPYING THE HTML HERE ---------------------------->

<table align = center border="0" cellpadding="0" cellspacing="0" style="BORDER-COLLAPSE: collapse;" bordercolor="#111111" width="95%" height=80%  bgcolor="#ffffff">
	<tr>
		<td colspan = 3>
		<span style="font-size:3px"><BR><BR></SPAN>
		</td>
	</tr>
	<tr>
		<td width="50%">
			<font face="Arial" size="2"><b>&gt;Welcome to the Huf Supplier Portal</b></font>
		</td>
		<td width = 5px rowspan =6>&nbsp;
		
		</td>
		<td width="50%" align="left">
		<font size="2" face="Arial" color="#000000"><b>News</b></font></span>
		</td>
	</tr>
	<tr>
		<td valign=top><hr color="#000000">
		<font face="Arial" size="2">Here you will find the request for quotations that Huf Group has for its company.<span style="font-size:3px"><BR><BR></SPAN>
		You can access the various areas by clicking on the links on the bar at the top.</font>
		</td>
		<td width="50%" valign="top" rowspan =5 align="left" height= 100%>
			<hr color="#000000">
			<div name="divNoticias" id="divNoticias"><%=sNoticias%></div>
		</td>
	</tr>
	<tr>
		<td valign = top>
		<font face="Arial" size="2">
		<li><b>Your details/ your company :</b> You can manage your company´s details and the trade areas in respect of which your company is compliant.
		</font>
		</td>
	
	</tr>
	<tr>
		<td valign = top>
		<font face="Arial" size="2">
		<li><b>Request for quotations :</b>You can access the processes open by Huf Group, for which your company has been invited to tender.
		</font>
		</td>
	
	</tr>
	<tr>
		<td width="50%" valign=bottom>
			<hr color="#000000" size="1">
			<font face="Arial" size="2">If this is the first time you are accessing this page, you can download the instructions on how to submit bids, requirements, etc. ...by clicking on the corresponding icon below.</font>
			<hr color="#000000" size="1">
		</td>
	</tr>
	<tr>
		<td>
			<table cellSpacing="1" cellPadding="1" width="100%" border="0">
				<tr>
					<td>&nbsp;</td>	
					<td width="4%"><a href="docs/FSN_SOP_MAN_ 3_Masterdata.pdf" target="_blank"><img height="26" src="../images/pdf.gif" width="26" border="0"></a></td>
					<td><font face="Arial" size="2">Supplier details maintenance</font></td>
					<td width="4%"><a href="docs/FSN_SOP_MAN_ 4_ How_to_offer.pdf" target="_blank"><img height="26" src="../images/pdf.gif" width="26" border="0"></a></td>
					<td><font face="Arial" size="2">How&nbsp;to&nbsp; offer&nbsp;</font></td>
					<td width="2%"><a href="docs/FSN_SOP_MAN_ 1_Users_Guide.pdf" target="_blank"><img height="26" src="../images/pdf.gif" width="26" border="0"></a></td>
					<td><font face="Arial" size="2">General overview</font></td>
					<td width="3%"><a href="docs/FSN_SOP_MAN_ 2_Technical_supplier_requirements.pdf" target="_blank"><img height="26" src="../images/pdf.gif" width="26" border="0"></a></td>
					<td><font face="Arial" size="2">Technical requirements</font></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="50%" colspan=3>
		<span style="font-size:2px"><BR><BR></SPAN>
		</td>
	<tr>
	<tr>
		<td width="100%" height="6" bgcolor="#e5e5e5" colspan="3">
		<p align="center">&nbsp;<b><font face="Arial" size="2" color="#FF0000">For any further clarifications, please send an email to supplier@huf-group.com</font></b></p></td>
	</tr>
</table>
</body></html>
