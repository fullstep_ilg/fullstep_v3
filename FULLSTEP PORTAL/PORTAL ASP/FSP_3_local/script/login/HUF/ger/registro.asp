﻿<%@ Language=VBScript %>
<!--#include file="../../../common/XSS.asp"-->
<html>


<head>

<title>::Supplier Portal::</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="../estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo2 {font-weight: normal; color: #666666; text-decoration: none; font-style: normal;}
-->
</style>
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=GER", "_blank")
window.close()

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=550,height=600,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>


<body bgcolor="#FFFFFF" topmargin="5" leftmargin="5">
<form name="frmAlta" id="frmAlta" method="post">

  <div align="center">
  <table border="0" width="80%">
      <tr>
        <td background="../images/fondotop_c.gif">&nbsp;</td>
      </tr>
      <tr>
        <td><a href="http://www.huf-group.com" target="_blank"><img src="../images/logo.gif" border="0" ></a></td>
      </tr>
      <tr>
        <td width="100%" height="33" align="left">        
	    <p align="left" class="subtextos"> Um mit dem Anmeldungsprozess fortzufahren, bitte &quot;Akzeptieren&quot; dr&uuml;cken:</p>        
               
      </td>
      </tr>
      <tr>
        <td align="left"><table border="0" align="left">
          <tr>
            <td valign="bottom"><div align="right"><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"></font></div>          <font color="#666666" size="1" face="Verdana">
              <textarea readonly rows="11" name="S1" cols="63" style="font-family: Verdana; font-size: 8pt; text-align:justify; display:block; height:135px; line-height: 150%; list-style-type: lower-alpha">
NUTZUNGSBEDINGUNGEN FÜR DIE HUF GROUP SOURCING-PLATFORM: 

Vorbemerkung: Im Folgenden wird das Wort „Lieferant“ auch für solche Unternehmen verwendet, die aktuell keine Lieferanten von Huf sind, sondern sich lediglich um die Aufnahme einer Geschäftsbeziehung bemühen.  

Unternehmen die für die Huf Gruppe als Lieferanten aktiv sind oder sich als solche bewerben möchten, können über das Zuliefererportal http://supplier.huf-group.com zukünftig an Ausschreibungen teilnehmen.

Vorraussetzung dafür ist bei neuen Lieferanten eine Selbstauskunft über die Daten des Unternehmens.

Es besteht seitens des Lieferanten kein Rechtsanspruch auf die Beteiligung an Ausschreibungen.

Huf behält es sich vor, nach eigenen Kriterien auszuwählen und Lieferanten abzulehnen oder von der Plattform auszuschließen.

Für das Zuliefererportal wird pro Unternehmen ein Hauptbenutzer eingeführt.

Dieser übernimmt, als koordinierende Stelle für das Unternehmen, die Zulassung und Löschung von weiteren Benutzern.

Die Pflege von Benutzerdaten, die Zulassung und Löschung von Benutzern sowie die Pflege der Daten über das Unternehmen (Adressen, Warengruppen, etc.) geschieht damit in Eigenverantwortung des einzelnen Lieferanten durch den Hauptbenutzer.

Der Hauptbenutzer handelt im Namen des Unternehmens, dieses verpflichtet sich die Daten fortlaufend aktuell zu halten.

Die Zugangsdaten für diesen Hauptbenutzer-Zugang werden dem Lieferanten mitgeteilt.

Sie sind vertraulich zu behandeln, gleiches gilt für die Zugangsdaten der weiteren Benutzer. Insbesondere ist zu beachten, dass Zugangsdaten nicht veröffentlicht werden und nur einem beschränkten, unmittelbar an der Geschäftsbeziehung beteiligten Personenkreis zugänglich sind.

Kennwörter sind hinreichend komplex zu wählen und regelmäßig zu ändern.

Alle im Rahmen der Geschäftsbeziehung ausgetauschten Informationen und Dokumente sind strikt vertraulich zu behandeln.

Zeichnungen, Beschreibungen, Spezifikationen und Ähnliches dürfen nur im Rahmen der betrieblichen Erfordernisse gespeichert und vervielfältigt werden.

Bei Weitergabe von Daten an Dritte durch Lieferanten (z.B. an deren Unterlieferanten) ist eine entsprechende Verpflichtung zur Vertraulichkeit sicher zu stellen.

Für die ständige, fehlerfreie Verfügbarkeit der elektronischen Systeme wird keine Gewähr geleistet. Huf behält es sich vor, den Betrieb der Plattform jederzeit einzuschränken oder einzustellen.

Für die Zugangsmöglichkeit zur Plattform („Internetzugang“) ist der Lieferant selbst verantwortlich und es werden keinerlei Kosten hierfür erstattet.

Alle Beteiligten verpflichten sich, die erforderlichen Sicherheitsvorkehrungen nach dem jeweiligen Stand der Technik vorzunehmen.

Der Lieferant verpflichtet sich, keine Dokumente, Dateien und Daten in die Plattform einzustellen die den Betrieb gefährden und/oder beeinträchtigen könnten.

Insbesondere sind Vorkehrungen zu treffen, um keine Computerviren zu verbreiten.

Elektronisch übermittelte Willenserklärungen haben volle Gültigkeit, wie auch Erklärungen per Fax oder per Telefon rechtsverbindlich sind.

Für die vertraglichen Bestandteile sowie für die Angebotsabgabe gelten nur und ausschließlich die vom jeweiligen Lieferanten in die Plattform eingestellten Angaben. Etwaige schriftliche Nachträge haben keinerlei Geltung, es sei denn, diese werden von Huf schriftlich rückbestätigt.

Mit der Anmeldung erkennt das Lieferanten-Unternehmen die oben aufgeführten Bedingungen an.

        </textarea>
              </font> </td>
            </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td align="left" class="textos"><p><span class="subtextos">Akzeptieren Sie die Aufnahmebedingungen f&uuml;r das Einkaufsportal von HUF GROUP?</span><br>
                <br>
          Wenn Sie &quot;Nicht akzeptieren&quot; dr&uuml;cken, kann Ihre Firma nicht als autorisierter Lieferant von HUF GROUP registriert werden.<br> 
Um mit dem Anmeldungsprozess fortzufahren, bitte &quot;Akzeptieren&quot; dr&uuml;cken. <br>
Wenn Sie die Bedingungen drucken m&ouml;chten, bitte den Knopf &quot;Drucken&quot; dr&uuml;cken. <br>
      <br>
          
      <div align="center"></div>
          <table border="0" width="39%">
            <tr>
              <td width="41%" valign="middle"><p align="center">
                  <input type="submit" onclick="return alta()" value="Akzeptieren" id="submit1" name="submit1">
              </p></td>
              <td width="38%" valign="middle"><input type="button" onclick="no_alta()" value="Nicht Akzeptieren" id="button1" name="button1">
              </td>
              <td width="21%" valign="middle"><input type="button" onclick="imprimir()" value="Drucken" id="button2" name="button2">
              </td>
            </tr>
          </table>
        <p align="center"></td>
      </tr>
  </table>
  <input type="hidden" name="ACEPTADO">
	  
</div>
</form>

</body>

</html>
