﻿<%@ Language=VBScript %>
<!--#include file="../../login.asp"-->
<%				
''' <summary>
''' Pantalla inicial de la personalización, es la q llama a la pantalla de login
''' </summary>
''' <remarks>Llamada desde: registro/registrarproveedor.asp		proveedor/default.asp
'''		login/personalizacion/login.asp		; Tiempo máximo: 0,1</remarks>
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if

If Idioma="" then
	Idioma="ENG"
end if

%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Gestamp Automoción - Purchasing Portal </title>
<script language="javascript">
<!--
var mesos=new Array(13);
  mesos[1]="de enero";
  mesos[2]="de febrero";
  mesos[3]="de marzo";
  mesos[4]="de abril";
  mesos[5]="de mayo";
  mesos[6]="de junio";
  mesos[7]="de julio";
  mesos[8]="de agosto";
  mesos[9]="de septiembre";
  mesos[10]="de octubre";
  mesos[11]="de noviembre";
  mesos[12]="de diciembre";

var semana=new Array(8);
  semana[1]="Domingo";
  semana[2]="Lunes";
  semana[3]="Martes";
  semana[4]="Miércoles";
  semana[5]="Jueves";
  semana[6]="Viernes";
  semana[7]="Sábado";

var ladata=new Date();

var elmes=mesos[ladata.getMonth() + 1];

var eldia=semana[ladata.getDay() + 1];

var numero=ladata.getDate();

var any=ladata.getYear();

if (any < 2000)
any = any + 1900;

function ventanaLogin (IDI){

   window.open ("registro.asp?Idioma="+IDI,"","width=650,height=485,resizable=yes")

}
function ventanaSecundaria (URL){

   window.open(URL,"ventana1","width=700,height=370,scrollbars=no")

}
function ventanaAyuda (URL){

   window.open(URL,"ayuda","width=700,height=600,scrollbars=yes")

}
function popup (URL) {
 window.open("popup.htm","","width=500,height=650,scrollbars=NO")
 }
function recuerdePWD()
{
   window.open("recuerdo.asp","_blank","width=641,height=350,scrollbars=NO")
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<link href="../estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {color: #FFFFFF}
.Estilo2 {color: #333333}
.Estilo3 {FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: none; font-size: 10px;}
body {
	background-color: #BDD4DC;
}
-->
</style>
</head>
<body onLoad="MM_preloadImages('../images/login_off.gif','../images/login_on.gif')">

<table width="760" height="100%" style="border:1px solid #0071B8" align="center" cellpadding="0" cellspacing="0"  bgcolor="#FFFFFF">
    

    <tr>
      <td valign="top"><table width="760" height="534" border="0" cellpadding="0" cellspacing="0">
          
          <tr>
            <td height="45" colspan="7" valign="top"><img src="../images/bannerdefault.jpg" width="760" height="110" border="0" usemap="#Map"></td>
          </tr>
          <tr>
            <td height="25" colspan="7" valign="top">&nbsp;</td>
          </tr>
          <tr>
            
            <td width="38" height="394" align="right" valign="top" class="textos">&nbsp;             
            </td>
            <td width="432" align="left" valign="top" class="textos"><table width="376" height="290" border="0">
              <tr>
                <td align="left" valign="bottom" background="../images/images4_eng.jpg"><table width="47%" border="0" class="nav">
                    <tr>
                      <td><a href="../default.asp" class="nav">Spanish</a></td>
                    </tr>
                    <tr>
                      <td><a href="../ger/default.asp" class="nav">Deutsch</a></td>
                    </tr>
                    <tr>
                      <td><a href="../fra/default.asp" class="nav">Français</a></td>
                    <tr>
                      <td><a href="javascript:ventanaAyuda('<%=application("RUTANORMAL")%>custom/<%=application("NOMPORTAL")%>/public/eng/manuales.htm')" class="nav">Help</a> </td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                </table></td>
              </tr>
            </table></td>
            <td width="12" valign="top">&nbsp;</td>
            <td width="29" valign="top">&nbsp;</td>
            <td width="236" align="center" valign="top">
			<table width="100%" border="0">
  <tr>
    <td height="135" valign="top" class="textos">If you are a registered supplier, please fill in your access data and click on &quot;login&quot; to access the suppliers private area.<br>
<br>
If you are not yet registered, please ask for registration through the link below.</td>
  </tr>
  <tr>
    <td><table width="200" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td colspan="3" valign="bottom"><img src="../images/form_sup.gif" name="Image1" width="200" height="3" id="Image1"></td>
                  </tr>
                  <tr>
                    <td rowspan="2" align="right" valign="top"><div align="right"><img src="../images/form_izq.gif" width="5" height="145"></div></td>
                    <td width="201"><form name="frmLogin" id="frmLogin" method="post" action="default.asp">
                        <div align="center">
                          <table width="173" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <input type="hidden" id="Idioma" name="Idioma" value="ENG">
                              <td width="6">&nbsp;</td>
                              <td width="79" class="formulario">Company code </td>
                              <td width="77"><input name="txtCIA" id="txtCia" size="12" maxlength="25">
                              </td>
                              <td width="11">&nbsp;</td>
                            </tr>
                            <tr>
                              <td width="6">&nbsp;</td>
                              <td width="79" class="formulario">User's code </td>
                              <td width="77"><input name="txtUSU" type="text" size="12">
                              </td>
                              <td width="11">&nbsp;</td>
                            </tr>
                            <tr>
                              <td width="6">&nbsp;</td>
                              <td width="79" class="formulario">Password</td>
                              <td width="77"><input name="txtPWD" type="password" size="12" maxlength="20" autocomplete="off">
                              </td>
                              <td width="11">&nbsp;</td>
                            </tr>
                            <tr>
                              <td width="6">&nbsp;</td>
                              <td width="79">&nbsp;</td>
                              <td width="77" bgcolor="#FFFFFF">
                                  <div align="left">
                                    <input name="cmdEntrar" type="image" onMouseOver="MM_swapImage('cmdEntrar','','../images/login_on.gif',1)" onMouseOut="MM_swapImgRestore()" src="../images/login_off.gif" border="0" WIDTH="50" HEIGHT="13">
                                  </div>
                              <td width="11">&nbsp;</td>
                            </tr>
                            <tr>
                              <td align="center"></td>
                              <td colspan="2" class="formulario" align="center"><div align="center"><a href="javascript:void(null)" class="formulario" onClick="recuerdePWD()">Forgot your login data?</a></div></td>
                              <td align="center">&nbsp;</td>
                            </tr>
                          </table>
                          <input type="hidden" id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
                          <input type="hidden" id="CiaComp" name="CiaComp" value="<%=CiaComp%>">
                        </div>
                    </form></td>
                    <td rowspan="2" valign="top"><img src="../images/form_dcha.gif" width="2" height="145"></td>
                  </tr>
                  <tr>
                    <td bgcolor="#BDD4DC"><table width="150" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                          <td align="center" valign="top"><div align="center" class="formulario">
                              <div align="center"><a href="javascript:ventanaLogin('ENG')" class="formulario">Ask for registration </a> </div>
                          </div></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td colspan="3" valign="top"><img src="../images/form_inf.gif" width="200" height="2"></td>
                  </tr>
                </table></td>
  </tr>
  <tr>
    <td><div align="center">
      <table width="126" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="4"></td>
                        <td width="47"></td>
                        <td rowspan="3" width="149"><div align="left"><a href="http://www.fullstep.com" target="_blank"><img src="../images/logo_fullstep.png" width="100" height="24" border="0" hspace="5" alt="FULLSTEP NETWORKS,  S.L."></a></div></td>
                      </tr>
                      <tr>
                        <td width="4">&nbsp;</td>
                        <td class="formulario" width="47"><div align="left">powered</div></td>
                      </tr>
                      <tr>
                        <td width="4">&nbsp;</td>
                        <td class="formulario" valign="top" width="47"><div align="left">by</div></td>
                      </tr>
                </table>
    </div></td>
  </tr><!--  -->
  <tr>
    <td height="50"><div align="left"><span class="formulario">Website optimised for 1280 x 1024<br>
                Supported browsers: <a href="http://windows.microsoft.com/es-ES/internet-explorer/download-ie" target="_blank" class="formulario">Internet Explorer</a> and <a href="http://www.ez-download.com/mozilla-firefox/?kw=firefox&subid=EZFFES&cust=firefox+download+mozilla&type=firefox&gclid=CN6MtdLfprQCFaTKtAodDWAAgQ&utm_campaign=EZFFES&fwd=1" target="_blank" class="formulario">Mozilla Firefox</a></span></div></td>
  </tr>
  <tr>
    <td class="formulario">
        &copy;  FULLSTEP | <a href="<%=application("RUTANORMAL")%>script/politicacookies/politicacookies.asp?Idioma=ENG" class="formulario">Cookies policy</a>
    </td>
  </tr>
</table>

			<div align="right">
                
            </div>              
			</td>
            <td width="13" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td height="2" colspan="6" valign="top" class="textos"></td>
          </tr>
     
          <tr>
            <td height="10"></td>
            <td height="10" colspan="4">
			<table width="98%" bgcolor="#2C4FA0"">
  <tr>
    <td height="15" bgcolor="#2C4FA0"><div align="center" class="blanco"> 
        <div align="right" class="blancobottom">
          <div align="center">Suppliers call center: 902 02 6000 - Calls from outside Spain +34 917 291 218<BR>
		  Opening hours(*): Monday to Thursday from 8:30 to 21:00.  Fridays from 8:00 to 19:00<br>
(*)   Central European Time (CET)  - Email: <a href="mailto:%20suppliersarea@gestamp.com" class="blancobottom">suppliersarea@gestamp.com</a></div>
        </div>
    </div></td>
    </tr>
</table></td>
            <td height="1" colspan="2"></td>
          </tr>
          <tr>
            <td colspan="7" class="blanco"><table width="100%" border="0">
  
</table>
            </td>
          </tr>
          <tr>
            <td height="1" colspan="7" class="blanco"></td>
          </tr>
          </table></td>
    </tr>
</table>

<map name="Map">
  <area shape="rect" coords="33,14,122,88" href="http://www.gestamp.com" target="_blank">
</map>
</body>
</html>
