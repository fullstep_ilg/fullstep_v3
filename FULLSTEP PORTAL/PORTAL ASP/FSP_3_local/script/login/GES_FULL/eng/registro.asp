﻿<%@ Language=VBScript %>
<!--#include file="../../../common/XSS.asp"-->

<html>


<head>

<title>Sign up contract to GESTAMP AUTOMOCIÓN</title>

<link href="../estilos.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
<!--
body {
	margin-top: 0px;
}
-->
</style></head>


<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=ENG", "_blank", "width=730,height=635" )
window.close()

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=550,height=600,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
  </script>


<script>
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=550,height=600,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>


<body bgcolor="#FFFFFF" leftmargin="5">
<form name="frmAlta" id="frmAlta" method="post">

<table border="0" width="100%">
  <tr>
    <td><a href="http://www.gestamp.com" target="_blank"><img src="../images/logo.gif" border="0" HEIGHT="62"></a>
	<hr width="100%" size="1" color="#0051BA">
    </td>
  </tr>
  <tr>
    <td><font face="Verdana" size="1">
      <p align="left" class="textos"> <b>To proceed with the sign up 
          proccess, the following contract must be read and accepted.</b> </p>
      <table border="0">
        <tr>
          <td valign="top"><p align="center"> </td>
          <td><font face="Verdana" size="1">
          <textarea readonly rows="11" name="S1" cols="80" style="font-family: Verdana; font-color: #2C4FA0; font-size: 8pt; text-align: Justify; line-height: 150%; list-style-type: lower-alpha">
VERY IMPORTANT, READ CAREFULLY! 
		  
PURCHASING PORTAL CONDITIONS OF USE
			
Subscription as a SUPPLIER in the GESTAMP AUTOMOCIÓN Purchasing Portal is subject to prior reading and acceptance of the following clauses. You cannot register without giving your consent. Whenever you access and use the Portal, it is assumed that you expressly and fully agree, with no reservations whatsoever, to the full contents of these Conditions and to the Legal Notice and Privacy Policy of the www.gestamp.com website.  Likewise, as your company&#146;s Main User, when subscribing as a user of this Portal, you are required to ensure that all the users who subscribe in your company comply, leaving GESTAMP AUTOMOCION free of any liability for possible damage that said users may cause your company or any other as a result of its actions in the Portal.

Through the GESTAMP AUTOMOCIÓN Purchasing Portal, GESTAMP AUTOMOCIÓN communicates with its suppliers to request offers, documents and any commercial information it deems fit. At the same time, GESTAMP AUTOMOCIÓN may use the portal to send any information it considers of interest.

GESTAMP AUTOMOCIÓN&#146;s computerized capture and treatment of the Personal Data voluntarily registered by the SUPPLIER is for the sole purpose of contract relation maintenance where established with GESTAMP AUTOMOCIÓN, i.e., requesting offers and sending information regarding GESTAMP AUTOMOCIÓN purchase tasks.

The SUPPLIER is required to:

-	Maintain strict confidentiality regarding all the information generated in the SUPPLIER and GESTAMP AUTOMOCIÓN relations.

-	Faithfully fulfil its commitments in the information sent through the portal. Should the SUPPLIER fail to demonstrate the necessary commercial diligence or to meet contract obligations, GESTAMP AUTOMOCIÓN reserves the right to temporarily or permanently exclude the SUPPLIER from the portal.

-	The SUPPLIER should indicate only those groups of materials referring to goods or services that the SUPPLIER sells, manufactures or distributes at the time of acceptance of the agreement, which are of GESTAMP AUTOMOCIÓN&#146;s commercial interest.

-	The SUPPLIER accepts that the offers included in the Portal shall be taken into consideration to the same degree and validity as an offer sent by any other traditional method (letter, fax).

In return, the SUPPLIER shall be entitled to:

-	Maintain a constant presence in GESTAMP AUTOMOCIÓN&#146;s database, as a registered SUPPLIER. 

-	Receive offer requests by virtue of the established regulations.

Subscription as a SUPPLIER to the GESTAMP AUTOMOCIÓN Purchasing Portal does not grant the SUPPLIER any right to necessarily participate in GESTAMP AUTOMOCIÓN&#146;s offer requests.

Should the SUPPLIER fail to fulfil its obligations, GESTAMP AUTOMOCIÓN may opt to demand proper fulfilment thereof, or to proceed to unsubscribe the SUPPLIER in the database of the Portal.


</textarea>
          </font> </td>
        </tr>
      </table>
      <span class="noticias">In order to supply products/services to any company of the Gestamp Automocion Group, it is mandatory to accept the <a href="docs/General%20Purchase%20and%20contracting%20conditions.pdf" target="_blank" class="registro">General Purchase and contracting conditions</a> and the GESTAMP AUTOMOCIÓN <a href="docs/Social responsibility.pdf" target="_blank" class="registro">requirements regarding social responsibility</a> towards its suppliers.</span><br>
      <br>
      <span class="noticias">      Do you accept the sign up contract to the GESTAMP AUTOMOCION purchasing portal, the <a href="http://www.gestamp.com" target="_blank" class="registro">legal notice</a> on <a href="http://www.gestamp.com" target="_blank" class="registro">www.gestamp.com</a>, the <a href="clausulac.htm" target="_blank" class="registro">confidentiality clause</a>, the <a href="docs/General%20Purchase%20and%20contracting%20conditions.pdf" target="_blank" class="registro">General purchase and contracting conditions</a> as well as the <a href="docs/Social responsibility.pdf" target="_blank" class="registro">requirements regarding social responsibility</a>?<br>
      </span>                <br>
          <span class="noticias">Shall you click on &#145;No, I refuse&#146;, the proccess will terminate and your company will not become a GESTAMP AUTOMOCION supplier.<br>
          In order to continue the sign up proccess, you must click on &#145;Yes, I accept&#146;.<br>
If you wish to print out the contract, you may do so by clicking on the following button. </span><br>
       
          </font>
        <div align="right">
          <table width="39%" border="0" align="left">
            <tr>
              <td width="41%"><p align="center">
                  <input type="submit" onclick="return alta()" value="Yes, I accept" id="submit1" name="submit1">
              </p></td>
              <td width="35%"><input type="button" onclick="no_alta()" value="No, I refuse" id="button1" name="button1">
              </td>
              <td width="24%"><input type="button" onclick="imprimir()" value="Print" id="button2" name="button2">
              </td>
            </tr>
          </table>
        </div><br><br>
        <p align="left" class="textos">        
        <hr width="100%" size="1" color="#0051BA">
    </td>
  </tr>
</table>

	<input type="hidden" name="ACEPTADO">
	
</form>

</body>

</html>
