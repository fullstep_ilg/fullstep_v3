﻿<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<html>
<head>
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style>
    <link href="../estilos.css" rel="stylesheet" type="text/css">
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;
    };
</script>
<body>
    <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
    <input type="hidden" name="idioma" value="spa">
    <table width="620" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="20">&nbsp;
                
            </td>
            <td width="615" align="left">
                <a href="http://www.gestamp.com" target="_blank">
                    <img src="../images/logo.gif" height="62" border="0"></a>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;
                
            </td>
        </tr>
        <tr>
            <td width="20">&nbsp;
                
            </td>
            <td width="615" align="left">
                <br/>
                <span style="color:#666666; font-family:Verdana;" class="textos">
                    Si ha olvidado su contraseña introduzca su código de compañia, de usuario y la dirección de e-mail de su cuenta de acceso y pulse "Enviar". Recibirá un e-mail con el enlace a la página donde podrá crear una nueva contraseña.<br>
                </span>
                <br/>
                <table class="textos">
                    <tr>
                        <td>Cód. Compañia:</td>
                        <td><input type="text" name="txtCia" id="txtCia" size="80" maxlength="100"></td>
                    </tr>
                    <tr>
                        <td>Cód. Usuario:</td>
                        <td><input type="text" name="txtUsu" id="txtUsu" size="80" maxlength="100"></td>
                    </tr>
                    <tr>
                        <td>Dirección de email:</td>
                        <td><input type="text" name="txtEmail" id="txtEmail" size="80" maxlength="100"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="20">&nbsp;</td>
            <td style="color:#666666; font-family:Verdana;" class="textos">
                <br /><input type="submit" value="Enviar" name="cmdEnviar" id="cmdEnviar"/>
                <br /><br />
                
                Tel&eacute;fono atenci&oacute;n a proveedores:<strong> 902 02 6000 </strong>- LLamadas desde fuera de Espa&ntilde;a: <strong>+34 917 291 218 </strong><BR>
		  Horario (*) de atención: Lunes a Jueves de 8:30 a 21:00 hrs.  Viernes de 8:00 a 19:00<br>
<em>(*)   Central European Time (CET) - Email: <a href="mailto:%20suppliersarea@gestamp.com">suppliersarea@gestamp.com</a>  </em>
            </td>
        </tr>
    </table>
</body>
</html>
