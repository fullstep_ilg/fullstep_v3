﻿<%@ Language=VBScript %>
<!--#include file="../../../common/XSS.asp"-->

<html>


<head>

<title>Anbieter Portal</title>

<link href="../estilos.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
<!--
body {
	margin-top: 0px;
}
-->
</style></head>


<script>
function no_alta()
{
window.close()

}
</script>

<script>

/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=GER", "_blank", "width=730,height=635" )
window.close()

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=550,height=600,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
  </script>


<script>
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=550,height=600,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>


<body bgcolor="#FFFFFF" leftmargin="5">
<form name="frmAlta" id="frmAlta" method="post">

<table border="0" width="100%">
  <tr>
    <td><a href="http://www.gestamp.com" target="_blank"><img src="../images/logo.gif" border="0" HEIGHT="62"></a>
	<hr width="100%" size="1" color="#0051BA">
    </td>
  </tr>
  <tr>
    <td><font face="Verdana" size="1">
      <p align="left" class="textos"> <b>Damit der Anmeldungsprozess fortgesetzt werden kann, müssen Sie die Zugangsbedingungen zum Portal lesen und akzeptieren:</b> </p>
      <table border="0">
        <tr>
          <td valign="top"><p align="center"> </td>
          <td><font face="Verdana" size="1">
          <textarea readonly rows="11" name="S1" cols="80" style="font-family: Verdana; font-color: #2C4FA0; font-size: 8pt; text-align: Justify; line-height: 150%; list-style-type: lower-alpha">
WICHTIG, AUFMERKSAM LESEN!
NUTZUNGSBEDINGUNGEN DES EINKAUFSPORTALS VON GESTAMP AUTOMOCIÓN
Für die Anmeldung als ANBIETER im Einkaufsportal von GESTAMP AUTOMOCIÓN ist es erforderlich, dass sie folgende Klauseln lesen und akzeptieren. Wenn Sie sich nicht damit einverstanden erklären, können Sie sich nicht registrieren lassen. Bei jedem Einloggen und jeder Nutzung des Portals wird davon ausgegangen, dass Sie ausdrücklich, vollständig und vorbehaltslos mit dem gesamten Inhalt dieser Nutzungsbedingungen, den rechtlichen Hinweisen und den Datenschutzrichtlinien der Webseite www.gestamp.com einverstanden sind. Als Hauptnutzer Ihres Unternehmens sind sie bei Anmeldung in diesem Portal auch dazu verpflichtet, auf deren Erfüllung seitens aller Nutzer, die sich in ihrem Unternehmen anmelden, zu achten, womit Sie GESTAMP AUTOMOCIÓN von der Haftpflicht für etwaige Schäden befreien, die diese Nutzer Ihrem Unternehmen oder einem anderen mit ihren Handlungen im Portal verursachen.
Das Einkaufsportal von GESTAMP AUTOMOCIÓN ist der Kanal, über den GESTAMP AUTOMOCIÓN mit ihren Anbietern kommuniziert, um sie um Angebote, Unterlagen sowie um als zweckmäßig betrachtete Informationen zu bitten. Darüber hinaus kann GESTAMP AUTOMOCIÓN das Portal nutzen, um Ihnen Informationen zu vermitteln, von denen GESTAMP glaubt, dass sie für Sie von Interesse sind.
Wenn GESTAMP AUTOMOCIÓN personenbezogene Daten, die der ANBIETER freiwillig angegeben hat, automatisch einholt und bearbeitet, dient dies nur dem Zweck, die eventuell bestehende vertragliche Beziehung mit GESTAMP AUTOMOCIÓN aufrechtzuerhalten, das heißt, Angebote zu erbitten und Mitteilungen bezüglich der Einkaufsfunktion von GESTAMP AUTOMOCIÓN weiterzuleiten.
DER ANBIETER ist zu Folgendem verpflichtet:
- Alle Informationen, die in den Beziehungen zwischen dem ANBIETER und GESTAMP AUTOMOCIÓN vermittelt werden, absolut vertraulich zu behandeln.
- Bei den über das Portal übermittelten Informationen seine Verpflichtungen genau einzuhalten. Falls der ANBIETER nicht die erforderliche geschäftliche Sorgfalt walten lassen sollte oder seinen Verpflichtungen nicht nachkommen sollte, behält sich GESTAMP AUTOMOCIÓN das Recht vor, den ANBIETER vorübergehend oder ständig aus dem Portal auszuschließen.
- Der ANBIETER darf nur jene Materialgruppen angeben, die sich auf Güter oder Dienstleistungen beziehen, die der ANBIETER zum Zeitpunkt des Einverständnisses mit der Vereinbarung vermarktet, herstellt oder vertreibt und die für GESTAMP AUTOMOCIÓN von geschäftlichem Interesse sind. 
- Der ANBIETER akzeptiert, dass die in das Portal eingestellten Angebote denselben Stellenwert und dieselbe Gültigkeit haben wie über andere traditionelle Kanäle (Brief, Fax) übermittelte Angebote.
Als Gegenleistung hat der ANBIETER Anspruch auf Folgendes:
- In den Datenbanken von GESTAMP AUTOMOCIÓN in seiner Eigenschaft als angemeldeter ANBIETER ständig geführt zu werden. 
- Gemäß den festgesetzten Regeln Angebotsanfragen zu erhalten.
Mit der Anmeldung im Portal hat der ANBIETER nicht automatisch ein Anrecht darauf, an den Angebotsanfragen von GESTAMP AUTOMOCIÓN teilzuhaben.

GESTAMP AUTOMOCIÓN kann bei Nichterfüllung der Pflichten seitens des ANBIETERS entweder die gebührende Erfüllung dieser Pflichten fordern oder den ANBIETER in der Datenbank dieses Portals abmelden.


</textarea>
          </font> </td>
        </tr>
      </table>
      <span class="noticias">Um an Betriebe von Gestamp Produkte und/oder Dienstleistungen liefern zu können, müssen Sie die <a href="docs/Allgemeine%20Geschaefts%20und%20Vertragsbedingungen.pdf" target="_blank" class="registro"> Allgemeinen Geschäftsbedingungen </a>und die <a href="docs/Social responsibility.pdf" target="_blank" class="registro">Sozialverantwortung</a> akzeptieren. </span><br>
      <br>
      <span class="noticias">      Akzeptieren Sie den Beitrittsvertrag zum Einkaufsportal von GESTAMP AUTOMOCI&Oacute;N, die <a href="http://www.gestamp.com" target="_blank" class="registro">rechtlichen Hinweise </a>von  <a href="http://www.gestamp.com" target="_blank" class="registro">www.gestamp.com</a>, die <a href="clausulac.htm" target="_blank" class="registro"> Datenschutzklausel</a>, die <a href="docs/Allgemeine%20Geschaefts%20und%20Vertragsbedingungen.pdf" target="_blank" class="registro">Allgemeinen Geschäftsbedingungen</a> und die <a href="docs/Social responsibility.pdf" target="_blank" class="registro">Sozialverantwortung</a>?<br>
      </span>                <br>
          <span class="noticias">Wenn Sie &ldquo;<strong>Ich akzeptiere nicht</strong>&rdquo; anklicken, kann Ihr Unternehmen nicht als zugelassener Anbieter von GESTAMP AUTOMOCI&Oacute;N registriert werden.<br>
          Um den Anmeldungsprozess fortzusetzen, m&uuml;ssen Sie &ldquo;<strong>Ich akzeptiere</strong>&rdquo; anklicken.<br>
Wenn Sie den Vertrag ausdrucken möchten, klicken Sie bitte auf &ldquo;<strong>Drucken</strong>&rdquo;.</span><br><br>
       
          </font>
        <div align="right">
          <table width="39%" border="0" align="left">
            <tr>
              <td width="41%" height="38"><p align="center">
                  <input type="submit" onclick="return alta()" value="Ich akzeptiere" id="submit1" name="submit1">
              </p></td>
              <td width="35%"><input type="button" onclick="no_alta()" value="Ich akzeptiere nicht" id="button1" name="button1">
              </td>
              <td width="24%"><div align="right">
                <input type="button" onclick="imprimir()" value="Drucken" id="button2" name="button2">
              </div></td>
            </tr>
          </table>
        </div><br><br>
        <p align="left" class="textos">        
            </td>
  </tr>
</table>

	<input type="hidden" name="ACEPTADO">
	
</form>

</body>

</html>
