﻿<%@ Language=VBScript %>
<!--#include file="../../../common/acceso.asp"-->
<%
''' <summary>
''' Pantalla inicial tras hacer un login valido
''' </summary>
''' <remarks>Llamada desde: login\GESTAMP\inicio.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,false,false,0)


DIM FICHERO_SPA 
FICHERO_SPA= Application("PATHFICHERONOTICIAS") & ".FRA"
sNoticias = oRaiz.leerFicheroTexto(FICHERO_SPA)

set oRaiz = nothing
%>
<script SRC="../../../common/menu.asp"></script>

<html>

<head>
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="../estilos.css" rel="stylesheet" type="text/css">
<title>Gestamp Automoción - Portail Fournisseurs</title>
<style type="text/css">
<!--
body {
	margin-top: 0px;
	margin-left: 0px;
}
.Estilo1 {
	color: #E62132;
	font-size: 10px;
}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
	/*''' <summary>
	''' Iniciar la pagina.
	''' </summary>     
	''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
	function Init() {
		document.getElementById('tablemenu').style.display = 'block';

		p = window.top.document.getElementById("frSet")
		vRows = p.rows
		vArrRows = vRows.split(",")
		vRows = vArrRows[0] + ",*,0,0"
		p.rows = vRows
	}
</script>
</head>
<body scroll="yes" onload="Init()">
<script>
dibujaMenu(1)
</script>
<script language=javascript>

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}

</script>

<!-------------------------- BEGIN COPYING THE HTML HERE ---------------------------->

<table width="105%" border="0" cellpadding="5" cellspacing="2" bordercolor="0" hspace="0" vspace="0">
  <tr> 
	<td width="48" rowspan="3" align="right" valign="top" bordercolor="0"> <div align="left" class="textos"></div>
	</td>
	<td height="47" colspan="2" align="left" valign="middle"><font size="2" face="verdana" class="titulo"><b>Bienvenu</b></font></td>
	<td colspan="2" align="left" valign="bottom"><span class="textos"><strong>NOUVELLES</strong></span></td>
  </tr>
  <tr>
	<td width="304" valign="top" bgcolor="#EEEEEE" class="textos"> Veuillez trouver ici tous les appels d’offre que GESTAMP AUTOMOCIÓN vous a lancé. <BR>
		  <BR>
Vous pouvez accéder aux différentes sections du site Web à l’aide du menu situé dans la partie supérieure de la page. 

<ul>
		<li><b>Appels d’offre : 
		  :</b> vous pouvez accéder aux processus d’achat de GESTAMP AUTOMOCIÓN auxquels votre société a été invitée. </li>
	</ul>	
<ul>
		<li><b> Vos coordonnées / votre société :</b>ici vous pouvez gérer les données de votre société, des utilisateurs du Portail et des activités homologuées menées par votre entreprise. </li>
	  </ul>

</td>
	<td width="112" rowspan="2" valign="top" class="textos">&nbsp;</td>
	<td colspan="2" align="left" valign="top">
<div name="divNoticiasright" id="divNoticiasright" class="noticias" style="width:400 px">
	  <table align=center width=98%>
		<tr>
		  <td class="noticias"><%=sNoticias%> </td>
		</tr>
	  </table>
	</div>
</td>
  </tr>
  <tr>
	<td width="304" valign="middle" class="textos">N’hésitez pas à nous contacter pour tout renseignent complémentaire: Tél 902 02 6000 (appels depuis l’étranger : +34 917 291 218) – Email <a href="mailto:suppliersarea@gestamp.com" class="textos">suppliersarea@gestamp.com</a><br></td>
	<td width="245" align="left" valign="bottom"><span class="textos"><strong>DOCUMENTS SIGNIFICATIFS<br>
		  <br>
	  </strong></span>
	  <table width="355" border="0" class="textos">
		<tr>
		  <td><a href="docs/FSN_MAN_SOP_Configuration requise pour les fournisseurs.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> Configuration requise pour les fournisseurs</a></td>
		</tr>
		<tr>
		  <td width="349" class="textos" ><a href="docs/FSN_MAN_SOP_Presenter des offres.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> Présenter des offres </a></td>
		</tr>
		<tr>
		  <td><a href="docs/FSN_MAN_SOP_Conservation des donnees des fournisseurs.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> Comment actualiser les données fournisseur </a></td>
		</tr>
		<tr>
		  <td><a href="docs/2016_05_31_GTC__FR__EXECUTION_VERSION.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> Conditions générales d’achat de biens et de marchés de services </a><span class="Estilo1"> (NEW!)</span></td>
		</tr>
		<tr>
	      <td><a href="docs/Data_Protection_Policy_for_Suppliers.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> Politique de protection des donn&eacute;es personnelles</a><span class="Estilo1"> (NEW!)</span></td>
        </tr>		
		<tr>
		  <td><a href="docs/Social responsibility.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> Social responsibility  </a></td>
		</tr>
        <tr>
		  <td><a href="../eng/docs/Gift and benefits acceptance - GESTAMP-.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> Gift and benefits acceptance </a></td>
		</tr>		
		<tr>
		  <td><a href="../spa/docs/GES_Manual_Calidad_Proveedores.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> Supplier quality manual </a></td>
		</tr>
		<tr>
		  <td><a href="../eng/docs/GES_QA_GUI_Supplier.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> User guide - quality aspects </a></td>
		</tr>
        <tr>
	      <td><a href="../eng/docs/GES_Supplier_Performance_Indicators_Guide.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> Supplier performance indicator guide </a><span class="Estilo1"> (NEW!)</span></td>
        </tr>		
         <tr>
	      <td><a href="../eng/docs/GES_CON_QA_Safety Parts_Form Manual_Supplier.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> Safety & regulations parts supplier´s manual </a></td>
        </tr>
		<tr>
	      <td><a href="../eng/docs/Letter encouraging direct material suppliers to get the IATF 16949 Certification.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> Letter encouraging direct material suppliers to get IATF 16949 certified</a></td>
        </tr>		
	  </table>
	</a></span>    <font face="verdana" size="1" ><b></b></font></span>
</td>
	<td width="30" align="left" valign="bottom">&nbsp;</td>
  </tr>

</table>
</body>
</html>
