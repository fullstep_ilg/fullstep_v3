﻿<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<html>
<head>
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style>
    <link href="../estilos.css" rel="stylesheet" type="text/css">
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;
    };
</script>
<body>
    <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
    <input type="hidden" name="idioma" value="eng">
    <table width="620" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="20">&nbsp;
                
            </td>
            <td width="615" align="left">
                <a href="http://www.gestamp.com" target="_blank">
                    <img src="../images/logo.gif" height="62" border="0"></a>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;
                
            </td>
        </tr>
        <tr>
            <td width="20">&nbsp;
                
            </td>
            <td width="615" align="left">
                <br/>
                <span style="color:#666666; font-family:Verdana;" class="textos">
                   Si vous avez oublié votre mot de passe, merci de saisir le code de votre société, votre nom d’utilisateur et l’adresse mail relié à votre compte d’accès privé au Portail puis cliquez sur « Envoyer ». Vous allez recevoir automatiquement un courrier avec un lien vers le site où vous allez pouvoir créer un nouveau mot de passe.<br>
                </span>
                <br/>
                <table class="textos">
                    <tr>
                        <td>Code société:</td>
                        <td><input type="text" name="txtCia" id="txtCia" size="80" maxlength="100"></td>
                    </tr>
                    <tr>
                        <td>Code utilisateur:</td>
                        <td><input type="text" name="txtUsu" id="txtUsu" size="80" maxlength="100"></td>
                    </tr>
                    <tr>
                        <td>Adresse mail:</td>
                        <td><input type="text" name="txtEmail" id="txtEmail" size="80" maxlength="100"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="20">&nbsp;</td>
            <td style="color:#666666; font-family:Verdana;" class="textos">
                <br /><input type="submit" value="Valider" name="cmdEnviar" id="Submit1"/>
                <br /><br />Assistance téléphonique : <strong>902 02 6000</strong> – appels en dehors de l’Espagne:<strong> +34 917 291 218</strong><BR>
		  Horaire d’assistance aux fournisseurs (*): de lundi à jeudi de 8:30 à 21:00<br>
<em>(*) Central European Time (CET) - Email:  <a href="mailto:%20suppliersarea@gestamp.com">suppliersarea@gestamp.com</a></em>
            </td>
        </tr>
    </table>
</body>
</html>
