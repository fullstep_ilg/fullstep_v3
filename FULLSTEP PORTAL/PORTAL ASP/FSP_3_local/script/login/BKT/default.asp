﻿<%@ Language=VBScript %>
<!--#include file="../login.asp"-->
<%		
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if

If Idioma="" then
	Idioma="SPA"
end if

Idioma="SPA"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Portal de Proveedores de Bankinter</title>
<link href="css/general.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 9]>
<link href="css/estilo_custom_ie9.css" rel="stylesheet" type="text/css" />
<![endif]-->
<script src="js/jquery-1.9.0.min.js"></script>
<script language="javascript">

function recuerdePWD()
{
   window.open("spa/recuerdo.asp","_blank","width=815,height=510,scrollbars=NO")
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
</script>
<script>
$(document).ready(function(){
	//---------------------
	
	$("#txtCIA").val("Código Compañía");
  	$("#txtCIA").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Código Compañía"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Código Compañía");
  			$(this).removeClass('focus');
  		}
  	});	
	//---------------------
  	$("#txtUSU").val("Código de Usuario");
  	$("#txtUSU").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Código de Usuario"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Código de Usuario");
  			$(this).removeClass('focus');
  		}
  	});
	//---------------------
  	$("#txtPWD").val("Contraseña");
	$("#txtPWD").focus(function(){
/*  	$("#txtPWD").bind('focus',function(){*/
  		$(this).addClass('focus');
  		if($(this).val()=="Contraseña"){
  			$(this).val('');
			/*$(this).removeAttr("type");
			$(this).prop('type', 'password');*/
  		}
	}).blur(function(){
/*  	}).bind('blur',function(){*/
  		if($(this).val()==""){
  			$(this).val("Contraseña");
			/*$(this).removeAttr("type");
			$(this).prop('type', 'text');
  			$(this).removeClass('focus');*/
  		}
  	});
});
</script>
</head>
<body class="home">
<div class="cabecera">
  <div class="centrado"><a href="http://www.bankinter.es" title="" target="_blank" class="logo"><img src="img/logo-bankinter.gif" alt="logo-Bankinter"  border="0" /></a>
    <h1>PORTAL DE PROVEEDORES</h1>
    
   <ul class="menu-navegacion">
        	<li><a href="index" class="activo">INICIO</a></li> <span>|</span>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/quienes-somos.html" >QUIÉNES SOMOS</a></li> <span>|</span>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/que-buscamos.html" >QUÉ BUSCAMOS</a></li> <span>|</span>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/politicas.html">POLÍTICAS</a></li> <span>|</span>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ayuda.html" title="" >AYUDA</a></li>
        </ul>
    
    <ul class="idiomas">
      <li class=""><a href="eng/default.asp" title="English version">English version</a></li>
    </ul>
    
  </div>
</div>
<div class="cuerpo">
  <div class="centrado home">
    <div class="wform">
      <h2>ACCESO PROVEEDORES</h2>
      <form action="#" method="post" name="frmLogin"  id="frmLogin">
        <input name="txtCIA" type="text" class="text" id="txtCIA" value="C&oacute;digo de Compa&ntilde;&iacute;a" maxlength="20"/>
        <input name="txtUSU" type="text" class="text" id="txtUSU" value="C&oacute;digo de Usuario" maxlength="20"/>
        <input type="password" class="text" id="txtPWD" name="txtPWD" maxlength="20" />
        <div class="bt_entrar"> 
         <input type="image" name="cmdEntrar" src="img/btn_conectar.png" width="99" height="28" border="0"></div>
          <div class="bt_claves"> <a href="javascript:void(null)" onClick="recuerdePWD()"class="bt_claves" >¿Olvidó sus claves de acceso?</a></div>
        
        <h3>¿AÚN NO ESTÁ REGISTRADO?</h3>
        <div align="center">
          <a href="registro.asp"><img src="img/btn_solicitar_registro.png" alt="Solicitar registro" title="Solicitar registro" width="169" height="28" id="registro" border="0" /></a> </div>
      </form>
      
    </div>
  </div>
</div>
<div class="pie">
  <div class="centrado">
    <div class="margen"> </div>
    <div class="tit_soporte"><h2>SOPORTE A PROVEEDORES</h2></div>
    <div class="datos_fullstep">
      <span class="tlf">Tel. 902 041 577</span><br />
      <div class="mail"> <a href="mailto:bankinter@fullstep.com" class="mail" title="bankinter@fullstep.com">bankinter@fullstep.com  </a></div>
      <hr />
      <div class="horario">HORARIO DE ATENCIÓN TELEFÓNICA:
      <p> <strong>Lunes a jueves:</strong> 8:00 - 21:00 <br />
      <strong>Viernes:</strong> 8:00 a 19:00 </p>
      </div>
    </div>
    <p class="izquierda"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/aviso-legal.html" title="" >Aviso legal <span> | </span></a></p>
    <p class="izquierda" id="cookies"><a href="<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=SPA" target="_blank">Pol&iacute;tica de Cookies</a></p>
    
    <p class="copyright">© Bankinter, S.A. Todos los derechos reservados.</p>
    
        <p class="powered"><a href="http://www.fullstep.com/portal-de-compras/"  target="_blank"><img src="img/logo-fullstep.png" alt="logo Fullstep" width="148" height="22" title="logo Fullstep" border="0" /></a></p>


  </div>
  
</div>
</body>
</html>

