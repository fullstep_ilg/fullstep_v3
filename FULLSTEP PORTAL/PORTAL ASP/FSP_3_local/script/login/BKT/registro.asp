﻿<%@ Language=VBScript %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/idioma.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="css/general.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.9.0.js"></script>
<script src="js/jquery-1.9.0.min.js"></script>
<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
<script src="../../common/formatos.js"></script>
<script language="JavaScript" src="../../common/ajax.js"></script>
<!--#include file="../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
</head>
<%
''' <summary>
''' Mostrar el detalle del registro
''' </summary>
''' <remarks>Llamada desde: login/login.asp		login/codigo instalaci�n/registro.asp	login/codigo instalaci�n/codigo idioma/registro.asp; Tiempo m�ximo: 0</remarks>
%>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta() {
    if (document.getElementById('opcion1')){
        if (!document.getElementById('opcion1').checked){
            alert("Debe aceptar las condiciones de uso del portal para poder registrarse");
            return false;
        }
    }

    setCookie("CONTRATOACEPTADO",1,new Date())
    window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=SPA", "_blank", "width=730,height=635")

    return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
		 
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}		 
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all) {
    if (event.button == 2 || event.button == 3) alert(msg)
}
return false}

document.onmousedown = RClick


//--></script>
<body onload="init()">
<div class="cabecera">
	<div class="centrado"><a href="http://www.bankinter.es" target="_blank" alt="logo Bankinter" title="logo Bankinter" class="logo"><img src="img/logo-bankinter.gif"  border="0" /></a>
    <h1>PORTAL DE PROVEEDORES</h1>
    
   <ul class="menu-navegacion">
        	<li><a href="default.asp" >INICIO</a></li> <span>|</span>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/quienes-somos.html" >QUIÉNES SOMOS</a></li> <span>|</span>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/que-buscamos.html" >QUÉ BUSCAMOS</a></li> <span>|</span>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/politicas.html">POLÍTICAS</a></li> <span>|</span>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ayuda.html" title="" >AYUDA</a></li>
        </ul>
    
    <ul class="idiomas">
      <li class=""><a href="eng/registro.asp" title="English version">English version</a></li>
    </ul>
    
  </div>
</div>
<div id="cont_gen" style="width:900px;">
    <div id="drc_gen">
    	<h1>SOLICITAR REGISTRO</h1>
        <div class="int">
        	<p>Para continuar con el registro debe aceptar las condiciones de adhesión al <span class="rojo">Portal de Proveedores</span>.</p>
            <p style="height:150px; padding:0 5px; overflow:auto; background:#FFF; border:#CCC 1px solid">
                Estimados Sres:<br /><br />
 
Les manifiesto mi voluntad de contratar los productos y servicios que en cada momento estén disponibles a través del servicio de la Extranet de Bankinter y con las condiciones generales y especiales de los diferentes pedidos contenidos en su web. Así mismo les manifiesto mi conformidad en que a partir de esta fecha todas las comunicaciones e informaciones, pedidos, suministros, facturaciones y todo tipo de relación como proveedor, puedan ser realizadas a través de esta Extranet de Bankinter.<br /><br />

La conexión con el servicio se realizará mediante una clave de usuario y un código de acceso (contraseña) que serán generados en pantalla al cumplimentar y enviar mis datos, quedando registrado de forma provisional. Posteriormente la compañía me confirmará de forma confidencial mi contraseña, quedando así mi registro como definitivo y siendo desde ese momento proveedor HOMOLOGADO por Bankinter.<br /><br />
 
Toda la operativa realizada a través de éste servicio, contrastadas por el código de acceso y las claves de seguridad, las deberán considerar en todo caso válidas y eficaces, considerándose que han sido autorizadas y cursadas por mí rogándoles que consideren el uso de las mencionadas claves como mi firma electrónica, equiparable, respecto a los contratos que suscriba con Bankinter por medios electrónicos, a mi firma manuscrita. Todo ello sin perjuicio de mi derecho a solicitar la entrega de los correspondientes contratos escritos en soporte papel en cualquier momento al área o departamento con el que se haya suscrito el contrato de suministro o servicio.<br /><br />
 
Así mismo manifiesto que los datos que figuran en mi registro son ciertos y corresponden a mi empresa.<br /><br />
 
Clausulas medioambientales<br /><br />
 
Condiciones generales:<br /><br />
 
LA EMPRESA se comprometerá, en todo momento, a minimizar las molestias que sobre el entorno puedan producir sus actividades, disponiendo de las medidas precisas.<br /><br />
 
Se dispondrán las medidas necesarias para prevenir accidentes o incidentes con repercusión medioambiental.<br /><br />

Todas las medidas adoptadas para el cumplimiento de los requisitos legales y demás obligaciones contempladas en las cláusulas que se insertan a continuación, serán costeadas por LA EMPRESA, salvo que se disponga lo contrario.<br /><br />
 
En caso de subcontratación de determinados servicios u oficios por parte de LA EMPRESA, las empresas subcontratadas bajo su control, deberán asumir las obligaciones, cumplir los requisitos en materia medioambiental y seguir las pautas de actuación existentes, en las actividades que se realicen, siendo aplicables las estipuladas en estas cláusulas.<br /><br />

LA EMPRESA informará al Comité de Medio Ambiente de BANKINTER sobre todos los incidentes con repercusión medioambiental que tengan lugar en el desarrollo de las actividades.<br /><br />
 
LA EMPRESA está obligada a que todo su personal, así como el de las empresas subcontratadas, conozca las normas medioambientales establecidas, y en ningún caso se podrá alegar ignorancia o desconocimiento de las mismas.<br /><br />
BANKINTER podrá efectuar inspecciones sobre los aspectos medioambientales de la actividad.<br /><br />
 
Requisitos de gestión medioambiental:<br /><br />

Todos los residuos deberán ser retirados y gestionados de acuerdo a la normativa aplicable, prestando especial atención a los residuos peligrosos que puedan generarse.<br /><br />
 
LA EMPRESA tendrá la obligación de gestionar los residuos peligrosos generados de forma adecuada, de acuerdo con la legislación vigente. LA EMPRESA deberá adjuntar al parte de los trabajos una copia de los documentos de control y seguimiento derivados de la gestión de los residuos peligrosos o cualquier otra información que considere oportuna referida a los mismos.<br /><br />

En caso de la gestión de los residuos peligrosos por parte de LA EMPRESA, esta deberá disponer de la inscripción en el registro de productores de residuos peligrosos en la Consejería de Medio Ambiente de la Comunidad Autónoma.<br /><br />

LA EMPRESA será responsable de mantener identificados y etiquetados los contenedores de residuos peligrosos.<br /><br />

Se deberán mantener los contenedores de residuos de forma ordenada y las instalaciones exentas de basuras.<br /><br />

Se promoverá el uso racional de los recursos naturales (agua, energía, etc) y la minimización, reutilización, reciclado de los residuos.<br /><br />
 
No realizar ningún vertido de productos peligrosos a la red de saneamiento, que no esté autorizado en las especificaciones del producto.<br /><br />
 
Se deberán Implantar las medidas necesarias para prevenir y reducir al mínimo los escapes de las sustancias reguladas que agotan la capa de ozono (p.e. HCFC?s).
 
Se tomarán las medidas necesarias para minimizar la generación de residuos o reducir su peligrosidad mediante: la compra de productos a granel, la sustitución de materiales o la utilización de productos o materiales susceptibles de reciclaje o reutilización, mejoras en los procesos, etc. 
 
Realizar las operaciones adoptando las medidas adecuadas para prevenir accidentes por fugas, derrames o vertidos prohibidos al suelo o a la red de saneamiento.

		  </p>
 <p style="text-align:right"><a href="registro_texto.html" class="" target="blank">Imprimir</a> </p>
            <div class="caja_documentos">
<ul>
<li><a href="spa/docs/Bankinter_Politica_medioambiental.pdf" target="_blank">Política de medioambiente</a></li>
<li><a href="spa/docs/Bankinter_Compras_eticas.pdf" target="_blank">Política de compras éticas</a></li>
<li><a href="spa/docs/Bankinter_Reglamento LOPD_0608_ Registro.pdf" target="_blank">Reglamento LOPD</a></li>

</ul>

</div>
<div class="caja_acepto">
<!--<form action="" method="post" style="width:400px;">//-->
<form name="frmAlta" id="frmAlta" method="post" style="width:800px;">
  <span id="sprycheckbox1">
<label>
  <input type="checkbox" name="opcion1" id="opcion1" required>
</label>
<span class="checkboxRequiredMsg">Seleccione para continuar.</span>
</span>
<span class="rojo">He leído y Acepto</span> las <a href="spa/docs/Bankiter_Contrato_Adhesion_Portal.pdf" target="_blank" class="rojo">Condiciones Generales de Uso del Portal de Proveedores</a>, así como los documentos relacionados anteriormente<br />
<br />
<br />

<br />
<button type="button" class="bt_registro" id="submit1" name="submit1" >Continuar</button>
<br>
</form>


</div>
<div style="clear:both;"></div>
    	</div>    
    <p>&nbsp;</p><p>&nbsp;</p></div>
</div>
<script type="text/javascript">

$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked') )
       {
        $('#frmAlta').submit();
        alta();}
    else
       alert("Debe aceptar las condiciones de uso del portal para poder registrarse");
    
});

</script>
 			
<div class="pie">
  <div class="centrado">
   <p class="powered"><a href="http://www.fullstep.com" target="_blank" title=""><img src="img/logo-fullstep2.png" alt="logo Fullstep" width="107" height="51" border="0" /></a></p>
    	 <p class="izquierda"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/aviso-legal.html" title="" >Aviso legal <span> | </span></a></p>
    <p class="izquierda" id="cookies"><a href="<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=SPA" target="_blank">Pol&iacute;tica de Cookies</a></p>
       
  </div>
  </div>
</body>
<!--#include file="../../common/fsal_2.asp"-->
</html>
