<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<link href="../css/inicio.css" rel="stylesheet" type="text/css">
<title>::Portal Proveedores Bankinter::</title>
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
}
-->
</style>
</head>
<script>
dibujaMenu(1)
</script>
<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function Init() {
    document.getElementById('tablemenu').style.display = 'block';

	MM_preloadImages('../images/icono_docs_sm.gif','../images/icono_docs_sm.gif')
	p = window.top.document.getElementById("frSet")
	vRows = p.rows
	vArrRows = vRows.split(",")
	vRows = vArrRows[0] + ",*,0,0"
	p.rows = vRows

}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">
<div class="cuerpo">
	<div class="centrado">
        <div class="contenido">
          <div class="cuerpo-contenido">
              <h1>Bienvenido al Portal de Proveedores</h1>
              Puede acceder a las distintas &aacute;reas a trav&eacute;s de las opciones de men&uacute; situadas en la parte superior,
			<br /><br />
           
			<ul class="bienvenido">
    			<li><span><strong>Solicitudes:</strong> acceda a las solicitudes (RFQ y otras solicitudes) y gesti&oacute;nelas desde este mismo apartado.</span></li>
				
				<li><span><strong>Sus datos:</strong> si lo desea puede modificar sus datos de contacto, cambiar su contraseña o la configuración de los formatos de la solicitud.</span></li>
                <li><span><strong>Su compa&ntilde;&iacute;a:</strong> si usted es el contacto principal, puede modificar los datos de su empresa, usuarios, así como las áreas de actividad en las que su empresa se encuentra homologada. </span></li>
            </ul>
            <div class="limpia">&nbsp;</div>
			<div class="fin"></div>
           
            <div class="raya_separa"></div>
            <p><strong>Si es la primera vez </strong>que va realizar una oferta a trav&eacute;s del portal, siga atentamente los siguientes pasos:</p>
			<ol>
                <li><span><strong>Pulse en &ldquo;solicitudes&rdquo;</strong> para ver las peticiones de ofertas que tiene abiertas su empresa.</span></li>
                <li><span><strong>Seleccione la solicitud de oferta</strong> a la que quiera responder, pulsando sobre el c&oacute;digo de la misma.</span></li>
                <li><span><strong>Realice su oferta completando toda la informaci&oacute;n necesaria:</strong> desde el &aacute;rbol de navegaci&oacute;n que encontrar&aacute; en la parte izquierda, podr&aacute; desplazarse por los distintos apartados que conforman la oferta. Para introducir los precios deber&aacute; ir al apartado &ldquo;items/precios&rdquo;. Recuerde introducir el plazo de v&aacute;lidez de la oferta, en el apartado &ldquo;Datos Generales de la oferta&rdquo;.</span></li>
                <li><span><strong>Comunique su oferta</strong> pulsando sobre el bot&oacute;n de enlace:<a href="#"><img src="../img/btn_enlace.gif" width="26" height="14" /></a>.</span></li>
			</ol>
          
			<div class="fin">&nbsp;</div>            
        </div>
    </div>
    <div class="cuerpo-menu-inicio">
      <div class="manual_consulta_inicio">
        <p><strong>Instrucciones</strong><br />
Desc&aacute;rguese las instrucciones sobre c&oacute;mo operar en el Portal:</p>
      </div>

      <ul>
       	  <li><a href="docs/FSN_MAN_ATC_Como_ofertar.pdf" target="blank">Cómo ofertar  </a></li>
            <li><a href="docs/FSN_MAN_ATC_Requisitos_tecnicos.pdf" target="blank">Requisitos t&eacute;cnicos</a></li>
            <li><a href="docs/FSN_MAN_ATC_Mantenimiento_de_ datos.pdf" target="blank">Mantenimiento de datos</a></li>
        </ul>
            <div class="limpia"></div>
            <div class="manual_consulta_inicio">
        <p><strong>Condiciones Generales</strong><br />
Descárguese las condiciones generales de necesaria aceptación en los futuros contratos marco de Bankinter:</p>
      </div>
      <ul>
       	  <li><a href="docs/Bankinter_Reglamento LOPD_0608_ Registro.pdf" class="textos" target="_blank">Reglamento LOPD</a></li>
            <li><a href="docs/Bankinter_Politica_medioambiental.pdf" class="textos" target="_blank">Política medioambiental</a></li>
            <li><a href="docs/Bankinter_Compras_eticas.pdf" class="textos" target="_blank">Política compras éticas</a></li>
        </ul>
            <div class="manual_consulta_inicio">
            <img src="../img/ayuda_2.jpg" alt="info proveedores Bankinter" width="268" height="140" />
<p><br />
Si tiene alguna duda o dificultad para operar con el Portal, p&oacute;ngase en contacto con nosotros en el teléfono 902 041 577 o a trav&eacute;s del mail: <a href="mailto:bankinter@fullstep.com" title="bankinter@fullstep.com" class="orange">bankinter@fullstep.com</a></p>
            </div>
            <div class="limpia"></div>
            <div class="manual_consulta_inicio">
           <span class="logo_left"><img src="../img/Certificado NORMA.jpg" /></span><span class="logo_right"><img src="../img/logo pacto.jpg" /></span>

            </div>
            
            
      </div>
        </div>
        
</div>
</body>
</html>
