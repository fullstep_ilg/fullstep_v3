﻿<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/general.css" rel="stylesheet" type="text/css" />
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body>
    <div class="centrado_claves">
    <div id="cont_gen">
      <div id="izd_gen">
            <a href="http://www.bankinter.es" title="Bankinter" target="_blank" class="logo"><img src="../img/logo-bankinter.gif" alt="Logo Bankinter" title="Logo Bankinter"  border="0" /></a>
        </div>
        <div id="imagen-recurso-pop"><img src="../img/img-recuerdo.jpg" alt="recuerde sus claves de acceso" width="257" height="417" title="recuerde sus claves de acceso" /></div>
      <div id="drc_gen" class="claves">
      		
            <div class="interior-pop">
          	<div class="tit">RECUPERACI&Oacute;N DE CONTRASE&Ntilde;A</div>
                    Si ha olvidado su contraseña introduzca su código de compañía, su usuario y la dirección de e-mail de su cuenta de acceso y pulse "Enviar". Recibirá un e-mail con el enlace a la página desde donde podrá crear una nueva contraseña.
              
                    <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
                    <table class="recupera">
                    <tr>
                        <td>Código de Compañía:</td>
                        <td><input name="txtCIA" type="text" class="text-pop" id="txtCIA" maxlength="20" autocomplete="off"/></td>
                    </tr>
                    <tr>
                        <td>Código de Usuario:</td>
                        <td><input type="text" name="txtUsu" id="txtUsu" size="30" maxlength="30" class="text-pop" autocomplete="off"/></td>
                    </tr>
                    <tr>
                        <td>Dirección de e-mail:</td>
                        <td><input type="text" name="txtEmail" id="txtEmail" class="text-pop" size="30" autocomplete="off"/></td>
                    </tr>
                </table>
                    <br /><input class="bt_registro_pop" name="cmdEnviar" type="submit" value="Enviar" />
                    
                </form>
                <div class="limpia">&nbsp;</div>
                <div class="recordar-claves">
                <div>
                    <span style="display:block;">Si continúa con dificultades para acceder al Portal o su dirección de email ha cambiado, consulte con el</span>                    
                    <span class="orange" style="display:block;"><strong>servicio de atención a proveedores.</strong></span>
                    <span><strong class="tam_14">902 041 577</strong></span>
                    <br /></div>
                </div>
            </div>
        </div>
    </div>
    </div>
</body>

</html>