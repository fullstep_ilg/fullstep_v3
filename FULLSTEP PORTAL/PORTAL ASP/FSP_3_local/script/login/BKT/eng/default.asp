﻿<%@ Language=VBScript %>
<!--#include file="../../login.asp"-->
<%		
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if

If Idioma="" then
	Idioma="ENG"
end if
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Supplier Portal - Bankinter</title>
<link href="../css/general.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 9]>
<link href="../css/estilo_custom_ie9.css" rel="stylesheet" type="text/css" />
<![endif]-->
<script src="../js/jquery-1.9.0.min.js"></script>
<script language="javascript">

function recuerdePWD()
{
   window.open("recuerdo.asp","_blank","width=815,height=510,scrollbars=NO")
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
</script>
<script>
$(document).ready(function(){
	//---------------------
	
	$("#txtCIA").val("Company Code");
  	$("#txtCIA").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Company Code"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Company Code");
  			$(this).removeClass('focus');
  		}
  	});	
	//---------------------
  	$("#txtUSU").val("User Code");
  	$("#txtUSU").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="User Code"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("User Code");
  			$(this).removeClass('focus');
  		}
  	});
	//---------------------
  	$("#txtPWD").val("Password");
	$("#txtPWD").focus(function(){
/*  	$("#txtPWD").bind('focus',function(){*/
  		$(this).addClass('focus');
  		if($(this).val()=="Password"){
  			$(this).val('');
			/*$(this).removeAttr("type");
			$(this).prop('type', 'password');*/
  		}
	}).blur(function(){
/*  	}).bind('blur',function(){*/
  		if($(this).val()==""){
  			$(this).val("Password");
			/*$(this).removeAttr("type");
			$(this).prop('type', 'text');
  			$(this).removeClass('focus');*/
  		}
  	});
});
</script>
</head>
<body class="home">
<div class="cabecera">
  <div class="centrado"><a href="http://www.bankinter.es" title="" class="logo"><img src="../img/logo-bankinter.gif"  border="0" /></a>
    <h1>SUPPLIER PORTAL</h1>
    
   <ul class="menu-navegacion">
        	<li><a href="" class="activo">HOME</a></li> <span>|</span>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/about-us.html" >ABOUT US</a></li> <span>|</span>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/what-we-are-looking-for.html" >WHAT WE ARE LOOKING FOR</a></li> <span>|</span>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/policies.html">POLICIES</a></li> <span>|</span>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/help.html" title="" >HELP</a></li>
        </ul>
    
    <ul class="idiomas">
      <li class=""><a href="../default.asp" title="español">Español</a></li>
    </ul>
    
  </div>
</div>
<div class="cuerpo">
  <div class="centrado home">
    <div class="wform">
      <h2>SUPPLIER ACCESS</h2>
     <form action="default.asp" method="post" name="frmLogin"  id="frmLogin">
        <input type="text" class="text" id="txtCIA" name="txtCIA" maxlength="20" />
        <input type="text" class="text" id="txtUSU" name="txtUSU" maxlength="20" />
        <input type="password" class="text" id="txtPWD" name="txtPWD" maxlength="30" />
        <div class="bt_entrar"> 
         <input type="image" name="cmdEntrar" src="../img/btn_login.png" width="99" height="28" border="0"></div>
          <div class="bt_claves"> <a href="javascript:void(null)" onClick="recuerdePWD()"class="bt_claves" >Forgot your access codes?</a></div>
        
        <h3>NOT YET REGISTERED?</h3>
        <div align="center">
          <a href="registro.asp"><img src="../img/btn_register.png" alt="Solicitar registro" title="Solicitar registro" width="169" height="28" id="registro" border="0" /></a> </div>
      </form>
      
    </div>
  </div>
</div>
<div class="pie">
  <div class="centrado">
    <div class="margen"> </div>
    <div class="tit_soporte"><h2>SUPPLIER HELP DESK</h2></div>
    <div class="datos_fullstep">
      <span class="tlf">Tel. +34 902 041 577</span><br />
      <div class="mail"> <a href="mailto:bankinter@fullstep.com " class="mail" title="bankinter@fullstep.com">bankinter@fullstep.com  </a></div>
      <hr />
      <div class="horario">OPENING HOURS:
      <p> <strong>Monday to Thursday:</strong> 8:00 - 21:00 <br />
      <strong>Fridays:</strong> 8:00 - 19:00 </p>
      </div>
    </div>
    <p class="izquierda"> <a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/legal-notice.html" target="_blank">Legal notice <span> | </span></a></p>
    <p class="izquierda" id="cookies"><a href="<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=ENG" target="_blank">Cookies Policy</a></p>
    
    <p class="copyright">© Bankinter, S.A. All rights reserved.</p>
    
        <p class="powered"><a href="http://www.fullstep.com" target="blank" title="logo Fullstep"><img src="../img/logo-fullstep.png" width="148" height="22" border="0" /></a></p>


  </div>
  
</div>
</body>
</html>

