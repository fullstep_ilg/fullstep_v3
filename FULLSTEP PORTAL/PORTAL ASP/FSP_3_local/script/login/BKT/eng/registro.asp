﻿<%@ Language=VBScript %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/fsal_1.asp"-->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/general.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.9.0.js"></script>
<script src="../js/jquery-1.9.0.min.js"></script>
<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
<script src="../../../common/formatos.js"></script>
<script language="JavaScript" src="../../../common/ajax.js"></script>
<!--#include file="../../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
</head>
<%
''' <summary>
''' Mostrar el detalle del registro
''' </summary>
''' <remarks>Llamada desde: login/login.asp		login/codigo instalaci�n/registro.asp	login/codigo instalaci�n/codigo idioma/registro.asp; Tiempo m�ximo: 0</remarks>
%>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta() {
    if (document.getElementById('opcion1')){
        if (!document.getElementById('opcion1').checked){
            alert("You must accept the conditions in order to register into the Supplier Portal");
            return false;
        }
    }

    setCookie("CONTRATOACEPTADO",1,new Date())
    window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=ENG", "_blank", "width=730,height=635")

    return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
		 
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}		 
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all) {
    if (event.button == 2 || event.button == 3) alert(msg)
}
return false}

document.onmousedown = RClick


//--></script>
<body onload="init()">
<div class="cabecera">
	 <div class="centrado"><a href="http://www.bankinter.es" title="" class="logo"><img src="../img/logo-bankinter.gif"  border="0" /></a>
    <h1>SUPPLIER PORTAL</h1>
    
   <ul class="menu-navegacion">
        	<li><a href="default.asp">HOME</a></li> <span>|</span>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/about-us.html" >ABOUT US</a></li> <span>|</span>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/what-we-are-looking-for.html" >WHAT WE ARE LOOKING FOR</a></li> <span>|</span>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/policies.html">POLICIES</a></li> <span>|</span>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/help.html" title="" >HELP</a></li>
        </ul>
    
    <ul class="idiomas">
      <li class=""><a href="../registro.asp" title="español">Español</a></li>
    </ul>
    
  </div>
</div>
<div id="cont_gen" style="width:900px;">
    <div id="drc_gen">
    	<h1>REQUEST FOR REGISTRATION</h1>
        <div class="int">
        	<p>To proceed with the sign up proccess, the following contract must be read and accepted.</p>
            <p style="height:150px; padding:0 5px; overflow:auto; background:#FFF; border:#CCC 1px solid">
               Dear Sirs, <br /><br />
I hereby state my desire to contract the products and services available at any stage through the Bankinter Extranet service pursuant to the general and special conditions of the different orders contained on their web page. I likewise declare that as of this date I agree to all communications and information, orders, supplies, billing and all types of relationships as as supplier being conducted through this Bankinter Extranet. <br /><br />

Connection with the service will be through a user name and access code (password) which will be generated on screen when filling out and sending my details, leaving them registered in a provisional manner. Subsequently the company will confirm my password to me confidentially, thereby definitively saving my registration and making me as of that moment a Bankinter APPROVED supplier.<br /><br />

All operations carried out using this service, verified by the access code and security keys, must be considered in all cases valid and effective, understood as having been duly authorized and processed by me and requesting that you consider use of the aforesaid keys to be my electronic signature, and equivalent, in relation to other contracts I may sign with Bankinter via electronic media, to be my written signature. All of the above without prejudice to my right to request delivery of the corresponding contracts in paper format at any time from the area or department with which the supply or service contract has been signed.<br /><br />
I likewise declare that the details appearing in my registration file are true and correct and correspond to my company.<br /><br />
Environmental clauses<br /><br />
General conditions:<br /><br />
THE COMPANY will undertake at all times to minimize the harm that its activities may cause to the environment, having the necessary measures to this effect.<br /><br />
Measures necessary to prevent accidents or incidents with an environmental repercussion will be duly available.<br /><br />
All measures adopted towards fulfilment of legal requirements and all other obligations provided for in the clauses inserted below, will be for the account of THE COMPANY, unless otherwise agreed.<br /><br />
In the case of THE COMPANY sub-contracting determined services or jobs, the subcontracted companies under its control must assume all environmentally-related obligations and requirements, and follow the existing action guidelines in the activities performed, including those stipulated in the present clauses.<br /><br />
THE COMPANY will report to the BANKINTER Environmental Committee all incidents having an environmental repercussion that take place in carrying out its activities.<br /><br />
THE COMPANY will be obligated to ensure that all its personnel, including that of subcontracted companies, is familiar with the established environmental rules and in no circumstances will it be possible to allege unfamiliarity or lack of knowledge of same.<br /><br />
BANKINTER will be entitled to carry out inspections in relation to environmental aspects of the activity.<br /><br />
Environmental management requirements:<br /><br />
All waste must be removed and managed pursuant to applicable legislation, paying special attention to hazardous waste that may be generated.
THE COMPANY will be obliged to manage any hazardous waste generated in an adequate fashion, pursuant to current legislation. THE COMPANY must attach to the works notice a copy of control and monitoring documents associated to the management of hazardous waste or any other information considered relevant in relation thereto.<br /><br />
In the case of THE COMPANY managing hazardous waste, it must be duly registered in the register of hazardous waste producers of the Autonomous Community's Environmental Board.<br /><br />
THE COMPANY will be responsible for keeping hazardous waste containers duly labelled and identified.<br /><br />
Waste containers must be kept in a clean and orderly fashion in installations free of waste.<br /><br />
Rational use of natural resources (water, power, etc.,) will be promoted along with the minimization, reuse, or recycling of waste.<br /><br />
No hazardous products will be discharged into the public drainage system if the product specifications do not allow it.<br /><br />
The necessary measures must be implemented to prevent and reduce to a minimum leaks of regulated substances that damage the ozone layer (e.g. HCFCs)<br /><br />
The necessary measures will be taken to minimize waste generation or reduce its hazardous nature by means of: bulk buying, replacing materials or using recyclable or reusable products or materials, improving processes, etc.<br /><br />
All operations will be carried out adopting the appropriate measures to prevent accidents due to prohibited leaks, spillages or discharges into the ground or drainage network.


		  </p>

             <p style="text-align:right"><a href="registro_texto.html" class="" target="blank">Print</a> </p>
<div class="caja_documentos">
<ul>
<li><a href="docs/BKT_Environmental_Policy.pdf" target="_blank">Environmental Policy</a></li>
<li><a href="docs/BKT_Ethical_Procurement_Policy.pdf" target="_blank">Ethical Procurement Policy</a></li>
<li><a href="../spa/docs/Bankinter_Reglamento LOPD_0608_ Registro.pdf" target="_blank">Reglamento LOPD</a></li>

</ul>

</div>
<div class="caja_acepto">
<!--<form action="" method="post" style="width:400px;">//-->
<form name="frmAlta" id="frmAlta" method="post" style="width:800px;">
  <span id="sprycheckbox1">
<label>
  <input type="checkbox" name="opcion1" id="opcion1" required>
</label>
<span class="checkboxRequiredMsg">Seleccione para continuar.</span>
</span>
<span class="rojo">I have read and accept</span> the <a href="docs/Bankinter_Conditions_of_use_of_the_supplier_portal.pdf" target="_blank" class="rojo">General terms and conditions of use of the Supplier Portal</a>, as well as the before referred documents.<br />
<br />
<br />

<br />
<button type="button" class="bt_registro" id="submit1" name="submit1" >Next</button>
<br>
</form>


</div>
<div style="clear:both;"></div>
    	</div>    
    <p>&nbsp;</p><p>&nbsp;</p></div>
</div>
<script type="text/javascript">

$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked') )
       {
        $('#frmAlta').submit();
        alta();}
    else
       alert("Debe aceptar las condiciones de uso del portal para poder registrarse");
    
});

</script>
 			
<div class="pie">
  <div class="centrado">
   <p class="powered"><a href="http://www.fullstep.com" target="blank" title=""><img src="../img/logo-fullstep2.png" width="107" height="51" border="0" /></a></p>
    	 <p class="izquierda"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/legal-notice.html" title="legal notice" >Legal notice <span> | </span></a></p>
    <p class="izquierda" id="cookies"><a href="<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=ENG" target="_blank">Cookies Policy</a></p>
       
  </div>
</div>
</body>
<!--#include file="../../../common/fsal_2.asp"-->
</html>
