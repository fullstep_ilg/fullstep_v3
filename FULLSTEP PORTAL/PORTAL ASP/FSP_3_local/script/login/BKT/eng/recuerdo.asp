﻿<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/general.css" rel="stylesheet" type="text/css" />
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body>
    <div class="centrado_claves">
    <div id="cont_gen">
      <div id="izd_gen">
            <a href="http://www.bankinter.com" title="Bankinter" target="blank" class="logo"><img src="../img/logo-bankinter.gif"  border="0" /></a>
        </div>
        <div id="imagen-recurso-pop"><img src="../img/img-recuerdo.jpg" width="257" height="417" /></div>
      <div id="drc_gen" class="claves">
      		
            <div class="interior-pop">
          	<div class="tit">RESET PASSWORD</div>
                    If you forgot your password, please fill in your user and email and click on "Submit". You will receive a link to a page to create a new password.
              
              <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
                    <table class="recupera">
                    <tr>
                        <td>Company Code:</td>
                        <td><input name="txtCIA" type="text" class="text-pop" id="txtCIA" maxlength="20" autocomplete="off"/></td>
                    </tr>
                    <tr>
                        <td>User Code:</td>
                        <td><input type="text" name="txtUsu" id="txtUsu" size="30" maxlength="30" class="text-pop" autocomplete="off"/></td>
                    </tr>
                    <tr>
                        <td>Email:</td>
                        <td><input type="text" name="txtEmail" id="txtEmail" class="text-pop" size="30" autocomplete="off"/></td>
                    </tr>
                </table>
                    <br /><input class="bt_registro_pop" name="cmdEnviar" type="submit" value="Enviar" />
                    
                </form>
                <div class="limpia">&nbsp;</div>
                <div class="recordar-claves">
                <div>
                    <span style="display:block;">If you have problems while accessing the portal, or your e-mail address has changed,
                    please contact the <strong>Portal Administrator, Tel. 902 041 577</strong></span>
                    <br /></div>
                </div> 
            </div>
        </div>
    </div>
    </div>
</body>

</html>