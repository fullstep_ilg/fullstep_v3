<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<link href="../css/inicio.css" rel="stylesheet" type="text/css">
<title>::Portal Proveedores Bankinter::</title>
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
}
-->
</style>
</head>
<script>
dibujaMenu(1)
</script>
<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function Init() {
    document.getElementById('tablemenu').style.display = 'block';

	MM_preloadImages('../images/icono_docs_sm.gif','../images/icono_docs_sm.gif')
	p = window.top.document.getElementById("frSet")
	vRows = p.rows
	vArrRows = vRows.split(",")
	vRows = vArrRows[0] + ",*,0,0"
	p.rows = vRows

}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">
<div class="cuerpo">
	<div class="centrado">
        <div class="contenido">
          <div class="cuerpo-contenido">
              <h1>Welcome to the suppliers private area </h1>
             You can access the various areas by clicking on the links on the menu options.
			<br /><br />
           
			<ul class="bienvenido">
    			<li><span><strong>Requests:</strong> Access Bankinter's requests (RFQ and others) and manage them from this section. .</span></li>
				
				<li><span><strong>Your details:</strong> Update your contact information, modify your password or customize your account. </span></li>
                <li><span><strong>Your company:</strong> update the general details about your company, point out the materials and services your company can supply and register new users of your company inside the Portal. </span></li>
            </ul>
            <div class="limpia">&nbsp;</div>
			<div class="fin"></div>
           
            <div class="raya_separa"></div>
            <p><strong>If this is the first time 
 </strong>you are posting an offer through the portal, please follow these steps:</p>
			<ol>
                <li><span><strong>Click on “Requests for quotation” &rdquo;</strong>to display the requests  pending for your company.</span></li>
                <li><span><strong>Select the request for quotation </strong> for which you want to send an offer by clicking on its code.</span></li>
                  <li><span><strong> Configure your offers</strong> by filling in the appropriate data. From the navigation tree on the left side of the screen, you will be able to navigate through the different sections of the offer. To fill in the prices, click on the &quot;items/prices&quot; section. Don't forget to fill in the validity dates of your offer in the &quot;General data of the offer&quot; section.</span></li><br>
  <li> <span><strong>Post your offers </strong>by clicking on Send <img src="../img/btn_enlace.gif" width="26" height="14" /></span></li>
              
			</ol>
          
			<div class="fin">&nbsp;</div>            
        </div>
    </div>
    <div class="cuerpo-menu-inicio">
      <div class="manual_consulta_inicio">
        <p><strong>Tutorials</strong><br />
Please download the instructions on how to make an offer, technical requirements, ..:</p>
      </div>

      <ul>
       	  <li><a href="docs/FSN_MAN_ATC_How to offer.pdf" class="textos" target="_blank">How to offer </a></li>
            <li><a href="docs/FSN_MAT_ATC_Technical Requirements.pdf" class="textos" target="_blank">Technical requests</a></li>
            <li><a href="docs/FSN_MAT_ATC_Master data.pdf" class="textos" target="_blank">Master data</a></li>
        </ul>
            <div class="limpia"></div>
         <div class="manual_consulta_inicio">
        <p><strong>General Conditions</strong><br />
Please download the general conditions that you have accepted by registering into Bankinter Supplier Portal:</p>
      </div>
      <ul>
       	  <li><a href="../spa/docs/Bankinter_Reglamento LOPD_0608_ Registro.pdf" class="textos" target="_blank">Reglamento LOPD</a></li>
            <li><a href="docs/BKT_Environmental_Policy.pdf" class="textos" target="_blank">Environmental Policy</a></li>
            <li><a href="docs/BKT_Ethical_Procurement_Policy.pdf" class="textos" target="_blank">Ethical Procurement Policy</a></li>
        </ul>
      
            <div class="manual_consulta_inicio">
            <img src="../img/ayuda_2.jpg" alt="Bankinter supplier support" width="268" height="140" />
<p><br />
If you have any doubt, please contact us on  <a href="mailto:bankinter@fullstep.com" title="bankinter@fullstep.com" class="orange">bankinter@fullstep.com</a> or call us at +34 902 041 577.</p>
            </div>
            <div class="limpia"></div>
            
            
            
      </div>
        </div>
        
</div>
</body>
</html>
