<%@ Language=VBScript %>
<!--#include file="../login.asp"-->
<%		
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if

if Not IdOrden = "" then
    if Not IsNumeric(IdOrden) then
        IdOrden=""
    end if
end if

if Not CiaComp = "" then
    if Not IsNumeric(CiaComp) then
        CiaComp=""
    end if
end if

If Idioma="" then
	Idioma="SPA"
end if

%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>=Portal de Proveedores</title>
<link rel="stylesheet" type="text/css" href="estilos.css">
<link href='http://fonts.googleapis.com/css?family=Istok+Web' rel='stylesheet' type='text/css'>
<!--[if lte IE 9]>
<link href="estilos_ie9.css" rel="stylesheet" type="text/css" />
<![endif]-->
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700' rel='stylesheet' type='text/css'>
<script language="javascript">
function ventanaLogin (IDI){
   window.open ("registro.asp?Idioma="+IDI,"","width=800,height=600,resizable=yes")
}
function recuerdePWD()
{
   window.open("spa/recuerdo.asp","_blank","width=800,height=400,scrollbars=NO")
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
</script>
<script src="js/jquery-1.9.0.min.js"></script>
<!--ventana modal-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<!--*****************-->
<script>
$(document).ready(function(){
	
	//---------------------
  	$("#txtCIA").val("Código Compañía");
  	$("#txtCIA").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Código Compañía"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Código Compañía");
  			$(this).removeClass('focus');
  		}
  	});	
	//---------------------
  	$("#txtUSU").val("Código de Usuario");
  	$("#txtUSU").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Código de Usuario"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Código de Usuario");
  			$(this).removeClass('focus');
  		}
  	});
	//---------------------
  	$("#txtPWD").val("Contraseña");
  	$("#txtPWD").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Contraseña"){
  			$(this).val('');
			$(this).removeAttr("type");
			$(this).prop('type', 'password');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Contraseña");
			$(this).removeAttr("type");
			$(this).prop('type', 'text');
  			$(this).removeClass('focus');
  		}
  	});
});
</script>
</head>

<body>
<div class="wrapper">
  <div id="cabecera">
    <div id="logo"><a href="http://www.moritz.com" target="_blank"><img src="images/logo-Moritz.png" alt="logo-Moritz" border="0"></a></div>
    <div id="titulo">PORTAL DE PROVEEDORES</div>
    <div id="nav">
      <ul>
       <li> ESP </li>
       
        <li>|<a href="ger/default.asp"> CAT</a></li>
               <li> |<a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ayuda.html" target="_blank"> ayuda</a></li>
      </ul>
    </div>
  </div>
  <div class="clearfix"></div>
  <div id="container"> 
    <!--  <div id="linea"></div>!-->
    
    <div id="image-main"><img src="images/portal-de-proveedores.jpg" alt="Portal-de-proveedores" /></div>
    <div id="acceso">
      <h2>ACCESO PROVEEDORES</h2>
      <form action="default.asp" method="post" name="frmLogin"  id="frmLogin">
        <input type="text" class="text" id="txtCIA" name="txtCIA" maxlength="20" />
        <input type="text" class="text" id="txtUSU" name="txtUSU" maxlength="20" />
        <input type="password" class="text" id="txtPWD" name="txtPWD" maxlength="20" />
        <div id="bt_acceso">
          <input class="bt" name="cmdEntrar" type="submit" value="ENTRAR" />
        </div>
        <input type="hidden" id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
        <input type="hidden" id="CiaComp" name="CiaComp" value="<%=CiaComp%>">
      </form>
      <div class="clearfix"></div>
      <div id="claves"><a href="javascript:void(null)" onClick="recuerdePWD()" >¿Olvidó sus claves de acceso?</a></div>
      <div class="clearfix"></div>
      <div id="registro">
        <h3>¿Aún no está registrado?</h3>
        <div id="bt_registro"><a href="javascript:ventanaLogin('SPA')" >Solicitar registro</a></div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div id="cont_textos">
      <div class="textos">
        <p>MORITZ pone a disposición de sus proveedores 
          un canal de comunicación directo, a través del cual 
          podrán responder a las peticiones de oferta 
          solicitadas por el departamento de compras.</p>
      </div>
      <div class="textos">
        <p>Si ud. ya se ha registrado en este portal y ha sido autorizado 
          como proveedor, puede acceder a la zona privada de 
          proveedores introduciendo sus claves de acceso. </p>
        <p> Si todavía no se ha registrado, solicite el 
          registro a través del enlace "solicitar registro". </p>
      </div>
    </div>
    <div id="soporte">
      <h3>Soporte a proveedores</h3>
      <p>Tel. 902 039 817<br> <a href="mailto:atencionalcliente@fullstep.com">atencionalcliente@fullstep.com</a></p>
      <h4>Horario de atención telefónica</h4>
      <p>Lunes a jueves:
        8:00 a 21:00 h.<br>
        Viernes: 
        8:00 a 19:00 h.</p>
    </div>
  </div>
  <div class="clearfix"></div>
  <div id="pie">
    <div id="footer_left">
      <div id="enlaces"> <a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/aviso-legal.html" target="_blank">Aviso legal </a> | <a href="<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=SPA" target="_blank">Política de cookies</a></div>
            <div id="powered"> <a href="http://www.fullstep.com/portal-de-compras/" target="_blank"><img src="images/logoFullStep.png"  alt="logo-Fullstep"/></a> </div>

    </div>
    <div id="footer_right">
      <p>Sitio web optimizado para 1280 x 1024<br>
        Navegadores soportados: Internet Explorer y Mozilla Firefox</p>
    </div>
  

  
  </div>
  

</div>
</body>
</html>
