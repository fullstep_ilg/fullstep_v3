<%@ Language=VBScript %>
<!--#include file="../../login.asp"-->
<%		
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if

if Not IdOrden = "" then
    if Not IsNumeric(IdOrden) then
        IdOrden=""
    end if
end if

if Not CiaComp = "" then
    if Not IsNumeric(CiaComp) then
        CiaComp=""
    end if
end if

If Idioma="" then
	Idioma="GER"
end if

%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>=Portal de Proveedores</title>
<link rel="stylesheet" type="text/css" href="../estilos.css">
<link href='http://fonts.googleapis.com/css?family=Istok+Web' rel='stylesheet' type='text/css'>
<!--[if lte IE 9]>
<link href="estilos_ie9.css" rel="stylesheet" type="text/css" />
<![endif]-->
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700' rel='stylesheet' type='text/css'>
<script language="javascript">
function ventanaLogin (IDI){
   window.open ("registro.asp?Idioma="+IDI,"","width=800,height=600,resizable=yes")
}
function recuerdePWD()
{
   window.open("recuerdo.asp","_blank","width=800,height=400,scrollbars=NO")
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
</script>
<script src="..//js/jquery-1.9.0.min.js"></script>
<!--ventana modal-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<!--*****************-->
<script>
$(document).ready(function(){
	
	//---------------------
  	$("#txtCIA").val("Codi Empresa");
  	$("#txtCIA").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Codi Empresa"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Codi Empresa");
  			$(this).removeClass('focus');
  		}
  	});	
	//---------------------
  	$("#txtUSU").val("Usuari");
  	$("#txtUSU").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Usuari"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Usuari");
  			$(this).removeClass('focus');
  		}
  	});
	//---------------------
  	$("#txtPWD").val("Contrasenya");
  	$("#txtPWD").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Contrasenya"){
  			$(this).val('');
			$(this).removeAttr("type");
			$(this).prop('type', 'password');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Contrasenya");
			$(this).removeAttr("type");
			$(this).prop('type', 'text');
  			$(this).removeClass('focus');
  		}
  	});
});
</script>
</head>

<body>
<div class="wrapper">
  <div id="cabecera">
    <div id="logo"><a href="http://www.moritz.com" target="_blank"><img src="../images/logo-Moritz.png" alt="logo-Moritz" border="0"></a></div>
    <div id="titulo">PORTAL DE PROVEÏDORS</div>
    <div id="nav">
      <ul>
       <li> <a href="../default.asp">ESP </a></li>
       
        <li>| CAT</li>
               <li> |<a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ger/ayuda.html" target="_blank"> ajuda</a></li>
      </ul>
    </div>
  </div>
  <div class="clearfix"></div>
  <div id="container"> 
    <!--  <div id="linea"></div>!-->
    
    <div id="image-main"><img src="../images/portal-de-proveedores.jpg" alt="Portal-de-proveedores" /></div>
    <div id="acceso">
      <h2>ACCÉS PROVEÏDORS</h2>
      <form action="default.asp" method="post" name="frmLogin"  id="frmLogin">
        <input type="text" class="text" id="txtCIA" name="txtCIA" maxlength="20" />
        <input type="text" class="text" id="txtUSU" name="txtUSU" maxlength="20" />
        <input type="password" class="text" id="txtPWD" name="txtPWD" maxlength="20" />
        <div id="bt_acceso">
          <input class="bt" name="cmdEntrar" type="submit" value="ENTRAR" />
        </div>
        <input type="hidden" id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
        <input type="hidden" id="CiaComp" name="CiaComp" value="<%=CiaComp%>">
      </form>
      <div class="clearfix"></div>
      <div id="claves"><a href="javascript:void(null)" onClick="recuerdePWD()" >Ha oblidat les claus d'accés?</a></div>
      <div class="clearfix"></div>
      <div id="registro">
        <h3>Encara no està registrat?</h3>
        <div id="bt_registro"><a href="javascript:ventanaLogin('SPA')" >Sol•licitud de registre</a></div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div id="cont_textos">
      <div class="textos">
        <p>MORITZ posa a disposició dels seus proveïdors un canal de comunicació directe, a través del qual podran respondre a les peticions d'oferta sol·licitades pel departament de compres.</p>
      </div>
      <div class="textos">
        <p>Si vostè ja s'ha registrat en aquest portal i ha estat autoritzat com a proveïdor , pot accedir a la zona privada de proveïdors introduint les seves claus d'accés. </p>
        <p> Si encara no s'ha registrat, sol·liciti el
          registre a través de l'enllaç "Sol·licitar registre" </p>
      </div>
    </div>
    <div id="soporte">
      <h3>Suport a proveÏdors</h3>
      <p>Tel. 902 039 817<br> <a href="mailto:atencionalcliente@fullstep.com">atencionalcliente@fullstep.com</a></p>
      <h4>Horari d'atenció telefónica</h4>
      <p>Dilluns a dijous:
        8:00 - 21:00 h.<br>
        Divendres: 
        8:00 - 19:00 h.</p>
    </div>
  </div>
  <div class="clearfix"></div>
  <div id="pie">
    <div id="footer_left">
      <div id="enlaces"> <a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ger/aviso-legal.html" target="_blank">Avís legal </a> | <a href="<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=GER" target="_blank">Política de cookies</a></div>
            <div id="powered"> <a href="http://www.fullstep.com/portal-de-compras/" target="_blank"><img src="../images/logoFullStep.png"  alt="logo-Fullstep"/></a> </div>

    </div>
    <div id="footer_right">
     <p>Lloc web optimitzat per a  1280 x 1024<br>
        Navegadors suportats: Internet Explorer i Mozilla Firefox</p>
    </div>
  

  
  </div>
  

</div>
</body>
</html>
