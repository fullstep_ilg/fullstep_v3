<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>
<html>
<head>

<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
   <link rel="canonical" href="/" />

		<link href="../css/reset.css" rel="stylesheet" type="text/css" />        
    	<link href="../css/font-awesome.min.css" rel="stylesheet" />  
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
        <link href="../css/style.css" rel="stylesheet" />  
        
        <!--[if lt IE 9]>
          	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
<title>::Portal de Proveedores - Prisa::</title>
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
}
-->
</style>
</head>
<script>
dibujaMenu(1)
</script>
<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function Init() {
    document.getElementById('tablemenu').style.display = 'block';

	MM_preloadImages('../images/icono_docs_sm.gif','../images/icono_docs_sm.gif')
	p = window.top.document.getElementById("frSet")
	vRows = p.rows
	vArrRows = vRows.split(",")
	vRows = vArrRows[0] + ",*,0,0"
	p.rows = vRows

}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">
    
    			
        
        
    	<section id="inicio"> 
            
            <div class="container">
            
           		<div class="text">
                
                	<div class="intro">
                    
                        
                    	<h2>Bienvenido al Portal de Proveedores</h2>
                    
                    </div>
                    
                    <div class="centro  col-d">
                    
                    	
                    
                    	<div class="ancla-wrapper">
                        
                           <!-- <h3>Bienvenido al Portal del Proveedores</h3>-->
                            
                            <p>Puede acceder a las distintas áreas a través de las opciones de menú situadas en la parte superior,</p>
                            
                            <ol class="lista-enum">
                                <li><p>Solicitudes: acceda a las solicitudes (RFQ y otras solicitudes) y gestiónelas desde este mismo apartado.</p></li>
                                <li><p>Pedidos: gestione desde aquí los pedidos que le han realizado los compradores, siga su evolución y acceda a su histórico.</p></li>
                                <li><p>Sus datos / su compañía: si lo desea puede modificar los datos de su empresa, usuarios, así como las áreas de actividad en las que su empresa se encuentra homologada.</p></li>
                            </ol>
                       
                      	</div>
                        
                        
                    	<div class="ancla-wrapper">
                        
                            <p>Si es la primera vez que va realizar una oferta a través del portal, siga atentamente los siguientes pasos:</p>
                            
                            <ol class="lista-enum">
                                <li><p>Pulse en “solicitudes” para ver las peticiones de ofertas que tiene abiertas su empresa.</p></li>
                                <li><p>Seleccione la solicitud de oferta a la que quiera responder, pulsando sobre el código de la misma.</p></li>
                                <li><p>Realice su oferta completando toda la información necesaria: desde el árbol de navegación que encontrará en la parte izquierda, podrá desplazarse por los distintos apartados que conforman la oferta. Para introducir los precios deberá ir al apartado “items/precios”. Recuerde introducir el plazo de válidez de la oferta, en el apartado “Datos Generales de la oferta”.</p></li>
                                <li><p>Comunique su oferta pulsando sobre el botón de enviar <a class="sobre" href="#"><i class="fa fa-envelope"></i></a></p></li>
                            </ol>
                       
                      	</div>                       
                              
                    </div>  
                    
                    <div class="columna col-i">
                    
                    	<div class="bloque-info">
                        	
                            <h2>Instrucciones</h2>
                            <p>Descárguese las instrucciones sobre cómo realizar una oferta, aceptar un pedido, seguimiento, etc:</p>
                            <div>
                           
                        	<ul>
                            	<li><a href="docs/FSN_MAN_ATC_Como ofertar.pdf" target="_blank">Cómo ofertar</a></li>
<li><a href="docs/FSN_MAN_ATC_Requisitos tecnicos.pdf" target="_blank">Requisitos técnicos</a></li>
<li><a href="docs/FSN_MAN_ATC_Mantenimiento de datos.pdf" target="_blank">Mantenimiento de datos</a></li>

                    		</ul>
	                   
</div>
                        </div>
                    
                    	
                        
                        <div class="atencion-proveedor">
                        
                        	<h2>Atención al proveedor</h2>
                            <p>Si no ha conseguido resolver sus dudas, póngase en contacto con nosotros en el teléfono: <strong>913 582 787</strong> o mándenos un email a: <a href="mailto:proveedores@prisa.com">proveedores@prisa.com</a></p>
                            
                        </div>
                        
                   	</div>                  
               
               	</div>
                    
         	</div>
              
    	</section>
        

</body>
</html>
