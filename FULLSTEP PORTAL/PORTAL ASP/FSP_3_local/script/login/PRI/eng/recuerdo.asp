<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>

<link rel="canonical" href="/" />

		<link href="../css/reset.css" rel="stylesheet" type="text/css" />        
    	<link href="../css/font-awesome.min.css" rel="stylesheet" />  
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
        <link href="../css/style.css" rel="stylesheet" />  
        
        <!--[if lt IE 9]>
          	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
           
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body>
 <header id="heading">             
            
      		<div class="container">  
            
           		<div class="text"> 
            
                    <div class="logotipo">
            			<a href="http://www.prisa.com" title="Prisa" target="_blank"><img src="../img/prisa.png" alt="Prisa" /></a>
                    </div>  
                    
                    <h1>SUPPLIER PORTAL</h1>                    
                                
           		</div>   
                    
           	</div>
            
        </header>        
    			
        
        
    	<section id="password"> 
            
            <div class="container">
            
           		<div class="text">
                
                	<div class="intro">
                        
                    	<h2>Password reset</h2>
                    
                    </div>
                    
                    
                    <div class="columna">

                        <div class="atencion-proveedor-recuerdo">
                        
                        	<h2>Supplier Support</h2>
                            <p>If you have problems while accessing the portal, or your e-mail address has changed,
                    please   </span> <strong class="alt">Tel. +34 913 582 787</strong></p>
                            
                        </div>
                        
                   	</div>
                    
                    <div class="centro">
                    
                    	<p>If you forgot your password, please fill in your user and email and click on "Submit". You will receive a link to a page to create a new password. </p>
                         
                        <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
                            <label>
                                <input type="text" name="txtCia" id="txtCia" size="30" maxlength="30" placeholder="Company Code"/>
                            </label>
                            <label>
                                <input type="text" name="txtUsu" id="txtUsu" size="30" maxlength="30" placeholder="User Code"/>
                            </label>
                            <label>
                               <input type="text" name="txtEmail" id="txtEmail" size="30" maxlength="30" placeholder="Email"/>
                            </label>
                              <input class="btn-dcha" name="cmdEnviar" type="submit" style="height:22px; padding:0;" value="Send" />
                              <input type="hidden" name="idioma" value="eng"/>
                        </form>
                    
                             
               		</div>
                     
                    
               	</div>
                    
         	</div>
              
    	</section>
        
                               
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/main.js"></script>        
    
</body>

</html>
