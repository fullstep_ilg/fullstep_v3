﻿<%@ Language=VBScript %>
<!--#include file="../../../common/XSS.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Prisa</title>
<link rel="canonical" href="/" />

		<link href="../css/reset.css" rel="stylesheet" type="text/css" />        
    	<link href="../css/font-awesome.min.css" rel="stylesheet" />  
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
        <link href="../css/style.css" rel="stylesheet" />  
        
        <!--[if lt IE 9]>
          	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
           
<script type="text/javascript" src="http://fast.fonts.net/jsapi/f5dbd368-355c-4f28-bc2f-217b51498d80.js"></script>
<script src="http://code.jquery.com/jquery-1.9.0.js"></script>
<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=ENG", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body>

				<header id="heading">             
            
      		<div class="container">  
            
           		<div class="text"> 
            
                    <div class="logotipo">
            			<a href="http://www.prisa.com" title="Prisa" target="_blank"><img src="../img/prisa.png" alt="Prisa" /></a>
                    </div>  
                    
                    <h1>SUPPLIER PORTAL</h1> 
                    
                         
                    <nav>                      
                        <ul>
                            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/ayuda.html" title="Ayuda">Help</a></li>
                            <li>
                            	<strong class="trigger"><i class="fa fa-caret-down"></i> Select language</strong>
                            	<ul>
	                            	<li><a href="../registro.asp" title="Español">Español</a></li>
	                            	<li><a href="#" title="English">English</a></li>
	                            	<li><a href="../ger/registro.asp" title="Portugues">Portugués</a></li>
                            	</ul>
                            </li>                                 
                        </ul> 
                    </nav>                   
                                
           		</div>   
                    
           	</div>
            
        </header>   
    			
        
        
    	<section id="ayuda"> 
            
            <div class="container">
            
           		<div class="text">
                
                	<div class="intro">
                    
                    	<ul class="migas">
                        	<li><a href="default.asp">Home</a></li>
                            <li>/</li>
                        	<li><a href="#">Register</a></li>
                        </ul>
                        
                    	<h2>Register</h2>
                    
                    </div>
                    
                    
                    <div class="condiciones-acceso">

                        <p>To proceed with the registration proccess, the following contract must be read and accepted </p>
    
                        <div class="info">
                        
                            <h3>Terms of Use of the Electronic Purchasing Platform </h3>
                        
                                                     <p>  Preliminary Note:  The word &quot;supplier&quot; is also used in the current document for those companies which are not actually suppliers of Grupo Promotora de Informaciones, S.A. (from now on PRISA) but who merely endeavour to establish a business relationship.<br>
<br>

One main condition to fulfil for the companies that are active suppliers of PRISA or companies that wish to register to take part in future purchasing negotiations through the web portal is the introduction, into the portal, of the company's general description.<br>
<br>

There is no legal right by the supplier to participate in the requests for quotation.<br><br>

According to internal decision criteria, PRISA reserves the right to select or exclude suppliers out of the platform.<br><br>

A main user will be established for every supplier registered into the purchasing portal.<br><br>

The main user acts on behalf of the supplier and he has to maintain his data constantly updated.

Acting as the company’s coordinator, he is in charge of the registration and delete of future users as well as keeping updated the company and users data.<br><br>

The supplier will be informed of the access data for the company's main user.<br>
<br>

This data is to be treated confidentially as well as to further users. Access data is personal and untransferable and access to the platform is to be restricted only to people directly involved in the business relationship.<br><br>

Access passwords chosen must have an adequate complexity and be changed regularly by the main user.<br><br>

All information and documents exchanged in the context of our business relationship must be treated with extreme confidentiality.<br><br>

Drawings, descriptions, specifications and similar may be saved and duplicated only in the context of operational requirements.<br><br>

En el caso de que la información se comparta con una tercera parte por los proveedores (p.e. subcontratación) se debe garantizar la confidencialidad de la misma.<br><br>

In the event of sharing information with third partiesby suppliers (e.g. outsourcing) confidentiality of the information shared must be guaranteed.<br><br>

The supplier is responsible for his access to the platform (&quot;Internet Access&quot;) which does not have any cost.<br>
<br>

All parties involved, agree to maintain the appropriate security measures according to the current technological standards.<br><br>

The supplier commits himself not to put into the platform any documents, file or data that may damage or prevent its correct functioning.<br><br>

In particular, measures must be taken to prevent any kind of virus contamination.<br><br>

The supplier agrees to use and keep updated a well known anti virus software.<br><br>

Electronically format offers transmitted are fully valid and those sent via phone or fax are legally binding.<br><br>

Only contractual terms as well as the offers introduced into the platform, will be taken into account. Any additional comment will not be binding unless it is confirmed by  PRISA by writing.<br><br>

All personal information submitted by the supplier through the purchasing area will become part of Promotora de Informaciones, S.A. (hereinafter, PRISA) database (with address in C/ Gran Vía 32, 6ª, Madrid 28013) to keep the business relationship as well as to manage other related services.<br>
<br>

Should you provide personal data belonging to third parties, you hereby guarantee that you have duly informed them and obtained their consent for the inclusion of their data in a file to be held by PRISA, with the purposes and rights referred to herein, and in accordance with Article 5.4. of the Data Protection Act (Ley Orgánica de Protección de Datos).<br><br>

The supplier, as well, gives his consent to hand over his personal data exclusively to other companies part of PRISA and in the same terms stated in the previous paragraph. PRISA as well as their companies, compromise to use this data only to request quotations and send communications related to PRISA’s purchasing functions using all ways including e-mail and not to share this information with third parties without the supplier previous consent.<br><br>

Sending an e-mail to the following address: <a href:"mailto: dcompras@prisa.com"> dcompras@prisa.com </a>  PRISA gives you the possibility to exercise your access, correction, cancellation or change of personal data rights. Also, according to the law 34/2002 of Information Society and Electronic Commerce Services, if you want to cancel your registration and do not want to receive neither our request for quotations nor our information, please send an e-mail to <a href:"mailto: dcompras@prisa.com"> dcompras@prisa.com </a> with the subject &quot;Cancel Registration&quot;.<br>
<br>

Upon registration, the supplier company accepts all the conditions listed above.</p>
                            
                        </div>
                        
                        <p class="text-derecha"><a href="registro_texto.htm">print</a></p>
                          
                    	
                        <form name="frmAlta" id="frmAlta" method="post" >
                        
                        	<span id="sprycheckbox1">
<label class="condiciones">
  <input class="checkbox" type="checkbox" name="opcion1" id="opcion1" required>

<span class="checkboxRequiredMsg">Select to continue.</span><span> I have read anc accept the general terms of use of the Electronic Purchasing Platform.</span></label></span>
<br />
                            <button class="btn btn-dcha" type="submit" id="submit1" name="submit1">register</button>
                        </form>
                        
                             
               		</div>
                     
                    
               	</div>
                    
         	</div>
              
    	</section>
        
        
        <footer id="footer">
        
        	<div class="container">            
            
            	<div class="footer-soporte">
                
                    <div class="soporte">
                    
                        <div class="soporte-titular">
                            <h3>SUPPLIER SUPPORT</h3>
                            <p><strong>Tel. +34 913 582 787</strong><br /><a href="mailto:proveedores@prisa.com">proveedores@prisa.com</a></p>
                        </div>
                        
                        <div class="soporte-horario">
                            <p><strong>Customer Service Hours</strong></p>
                            <p>Monday to Thursday: 9:00 - 18:00</p>
                            <p>Fridays: 8:00 - 14:00</p>
                        </div>
                        
                    </div>
                    
              	</div>
            
           		<div class="text">                
                    
                    <a href="http://www.fullstep.com" target="_blank"><img src="../img/fullstep.png" alt="fullstep" /></a> 
                    
                    <p><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/aviso-legal.html" title="" >Legal notice</a> | <a href="<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=ENG" >Cookies Policy</a></p>                    
                    <p>&copy; PRISA S.A. All rights reserved.</p>                           
                    
               	</div>
               
            </div>
            
        </footer>
                       
          <script type="text/javascript">
// <!--
// var sprycheckbox1 = new Spry.Widget.ValidationCheckbox("sprycheckbox1");
// //-->
$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked')) {
        $('#frmAlta').submit();
        alta();
    }
});
</script>             
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/main.js"></script>       

</body>

</html>
