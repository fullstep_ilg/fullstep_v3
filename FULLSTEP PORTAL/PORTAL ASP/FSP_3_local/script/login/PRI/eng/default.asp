<%@ Language=VBScript %>
<!--#include file="../../login.asp"-->
<%		
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if

if Not IdOrden = "" then
    if Not IsNumeric(IdOrden) then
        IdOrden=""
    end if
end if

if Not CiaComp = "" then
    if Not IsNumeric(CiaComp) then
        CiaComp=""
    end if
end if

If Idioma="" then
	Idioma="ENG"
end if

%>
<!doctype html>
<html>
<head>
        <meta charset="utf-8" />  
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />  
        <meta name="viewport" content="width=device-width, initial-scale=1" />  
        
        <title>Prisa</title>
        
        <meta name="title" content=""/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        
        <link rel="canonical" href="/" />

		<link href="../css/reset.css" rel="stylesheet" type="text/css" />        
    	<link href="../css/font-awesome.min.css" rel="stylesheet" /> 
       <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
        <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700"/>-->
        <link href="../css/style.css" rel="stylesheet" />  
        
        <!--[if lt IE 9]>
          	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
           
<script language="javascript">
function ventanaLogin (IDI){
   window.open ("registro.asp?Idioma="+IDI, "_self","width=950,height=600,resizable=yes")
}
function recuerdePWD()
{
   window.open("recuerdo.asp","_blank","width=950,height=550,scrollbars=NO")
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
</script>
<script src="js/jquery-1.9.0.min.js"></script>

<!--*****************-->
<script>
$(document).ready(function(){
	
	//---------------------
  	$("#txtCIA").val("Company Code");
  	$("#txtCIA").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Company Code"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Company Code");
  			$(this).removeClass('focus');
  		}
  	});	
	//---------------------
  	$("#txtUSU").val("User Code");
  	$("#txtUSU").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="User Code"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("User Code");
  			$(this).removeClass('focus');
  		}
  	});
	//---------------------
  	$("#txtPWD").val("Password");
  	$("#txtPWD").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Password"){
  			$(this).val('');
			$(this).removeAttr("type");
			$(this).prop('type', 'password');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Password");
			$(this).removeAttr("type");
			$(this).prop('type', 'text');
  			$(this).removeClass('focus');
  		}
  	});
});
</script>
</head>

<body>
<header id="heading">             
            
      		<div class="container">  
            
           		<div class="text-default"> 
            
                    <div class="logotipo">
            			<a href="http://www.prisa.com" title="Prisa" target="_blank"><img src="../img/prisa.png" alt="Prisa" /></a>
                    </div>  
                    
                    <h1>SUPPLIER PORTAL</h1> 
                    
                         
                    <nav>                      
                        <ul>
                            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/ayuda.html" title="Ayuda">Help</a></li>
                            <li>
                            	<strong class="trigger"><i class="fa fa-caret-down"></i> Select language</strong>
                            	<ul>
	                            	<li><a href="../default.asp" title="Español">Español</a></li>
	                            	<li><a href="#" title="English">English</a></li>
                                    <li><a href="../ger/default.asp" title="Portugues">Portugués</a></li>
                            	</ul>
                            </li>                                 
                        </ul> 
                    </nav>                   
                                
           		</div>   
                    
           	</div>
            
        </header>        
    			
        
        
    	<section id="home"> 
            
            <div class="container">
            
           		<div class="section-acceso">
                
                	<div class="acceso">
                    
                    	<h2>SUPPLIER ACCESS</h2>
                        
                        <div>
                        
                              <form action="default.asp" method="post" name="frmLogin"  id="frmLogin">
                                <label>
                                    <input type="text" id="txtCIA" name="txtCIA" maxlength="20"/>
                                </label>
                                <label>
                                    <input type="text" id="txtUSU" name="txtUSU" maxlength="20" />
                                </label>
                                <label>
                                    <input type="password" id="txtPWD" name="txtPWD" maxlength="32" />
                                </label>
                                <button class="btn" name="cmdEntrar"  type="submit">login</button>
                                <input type="hidden" id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
        						<input type="hidden" id="CiaComp" name="CiaComp" value="<%=CiaComp%>">
                            </form>
                            
                            <p class="acceso-recordar"><a href="javascript:void(null)" onClick="recuerdePWD()" >Forgot your access codes?</a></p>
                            
                            <h4>NOT YET REGISTERED?</h4>
                            
                            <p><a class="btn" href="javascript:ventanaLogin('ENG')">Register</a>
                       
                       	</p>
                            
                   	</div>
               
               	</div>
                    
         	</div>
            </div>
              
    	</section>
        
        
        <footer id="footer">
        
        	<div class="container">            
            
            	<div class="footer-soporte">
                
                    <div class="soporte">
                    
                        <div class="soporte-titular">
                            <h3>SUPPLIER SUPPORT</h3>
                            <p><strong>Tel. +34 913 582 787</strong><br /><a href="mailto:proveedores@prisa.com">proveedores@prisa.com</a></p>
                        </div>
                        
                         <div class="soporte-horario">
                            <p><strong>Customer Service Hours</strong></p>
                            <p>Monday to Thursday: 9:00 - 18:00</p>
                            <p>Fridays: 8:00 - 14:00</p>
                        </div>
                        
                    </div>
                    
              	</div>
            
           		<div class="text">                
                    
                    <a href="http://www.fullstep.com" target="blank"><img src="../img/fullstep.png" alt="fullstep" /></a> 
                    
                    <p><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/aviso-legal.html" title="" >Legal notice</a> | <a href="<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=ENG">Cookies Policy</a></p>                    
                    <p>&copy; PRISA S.A. All rights reserved.</p>                           
                    
               	</div>
               
            </div>
            
        </footer>
                       
                       
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/main.js"></script>     

</body>
</html>
