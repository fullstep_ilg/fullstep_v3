<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>
<html>
<head>

<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
   <link rel="canonical" href="/" />

		<link href="../css/reset.css" rel="stylesheet" type="text/css" />        
    	<link href="../css/font-awesome.min.css" rel="stylesheet" />  
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
        <link href="../css/style.css" rel="stylesheet" />  
        
        <!--[if lt IE 9]>
          	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
<title>::Portal de Proveedores - Prisa::</title>
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
}
-->
</style>
</head>
<script>
dibujaMenu(1)
</script>
<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function Init() {
    document.getElementById('tablemenu').style.display = 'block';

	MM_preloadImages('../images/icono_docs_sm.gif','../images/icono_docs_sm.gif')
	p = window.top.document.getElementById("frSet")
	vRows = p.rows
	vArrRows = vRows.split(",")
	vRows = vArrRows[0] + ",*,0,0"
	p.rows = vRows

}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">
    
    			
        
        
    	<section id="inicio"> 
            
            <div class="container">
            
           		<div class="text">
                
                	<div class="intro">
                    
                        
                    	<h2>Welcome to the Supplier Portal</h2>
                    
                    </div>
                    
                    <div class="centro  col-d">
                    
                    	
                    
                    	<div class="ancla-wrapper">
                        
                           <!-- <h3>Bienvenido al Portal del Proveedores</h3>-->
                            
                            <p>To navigate through the different areas of the portal, use the navigation bar in the topside of the page. From here you will be able to access: </p>
                            
                            <ol class="lista-enum">
                                <li><p>Requests: access your requests (RFQ and other requests) and manage them from this section.</p></li>
                                <li><p>Orders: manage your  orders,  monitor their evolution and access your orders history.</p></li>
                                <li><p>Your details/ your company: update the general details about your company, point out the materials and services your company can supply and register new users of your company inside the Portal.</p></li>
                            </ol>
                       
                      	</div>
                        
                        
                    	<div class="ancla-wrapper">
                        
                            <p>If this is the first time you are posting an offer through the portal, please follow these steps:</p>
                            
                            <ol class="lista-enum">
                                <li><p>	Click on “Requests for quotation” to display the requests for quotations pending for your company. </p></li>
                                <li><p>	Select the request for quotation for which you want to send an offer by clicking on its code.</p></li>
                                <li><p>	Configure your offers by filling in the appropriate data. From the navigation tree on the left side of the screen, you will be able to navigate through the different sections of the offer. To fill in the prices, click on the “items/prices” section. Don’t forget to fill in the validity dates of your offer in the “General data of the offer” section. </p></li>
                                <li><p>	Post your offers by clicking on Send <a class="sobre" href="#"><i class="fa fa-envelope"></i></a></p></li>
                            </ol>
                       
                      	</div>                       
                              
                    </div>  
                    
                    <div class="columna col-i">
                    
                    	<div class="bloque-info">
                        	
                            <h2>Instructions</h2>
                            <p>You can download the following tutorials:</p>
                            <div>
                           
                        	<ul>
                            	<li><a href="docs/FSN_MAN_ATC_How to offer.pdf" target="_blank">How to offer</a></li>
<li><a href="docs/FSN_MAT_ATC_Technical Requirements.pdf" target="_blank">Technical requirements</a></li>
<li><a href="docs/FSN_MAT_ATC_Master data.pdf" target="_blank">Master data</a></li>

                    		</ul>
	                   
</div>
                        </div>
                    
                    	
                        
                        <div class="atencion-proveedor">
                        
                        	<h2>Supplier Support</h2>
                            <p>If you have problems while accessing the portal, or your e-mail address has changed,
                    please contact us at <strong>+34 913 582 787</strong> or send us an email to: <a href="mailto:proveedores@prisa.com">proveedores@prisa.com</a></p>
                            
                        </div>
                        
                   	</div>                  
               
               	</div>
                    
         	</div>
              
    	</section>
        

</body>
</html>
