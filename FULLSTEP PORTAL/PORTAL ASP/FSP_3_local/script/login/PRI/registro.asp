﻿<%@ Language=VBScript %>
<!--#include file="../../common/XSS.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Prisa</title>


		<link href="css/reset.css" rel="stylesheet" type="text/css" />        
    	<link href="css/font-awesome.min.css" rel="stylesheet" />  
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
        <link href="css/style.css" rel="stylesheet" />  
        
        <!--[if lt IE 9]>
          	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
           
<script type="text/javascript" src="http://fast.fonts.net/jsapi/f5dbd368-355c-4f28-bc2f-217b51498d80.js"></script>
<script src="js/jquery-1.9.0.js"></script>
<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=SPA", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body>

				<header id="heading">             
            
      		<div class="container">  
            
           		<div class="text"> 
            
                    <div class="logotipo">
            			<a href="https://www.prisa.com" title="Prisa" target="_blank"><img src="img/prisa.png" alt="Prisa" /></a>
                    </div>  
                    
                    <h1>PORTAL DE PROVEEDORES</h1> 
                    
                         
                    <nav>                      
                        <ul>
                            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ayuda.html" title="Ayuda">Ayuda</a></li>
                            <li>
                            	<strong class="trigger"><i class="fa fa-caret-down"></i> Elegir idioma</strong>
                            	<ul>
	                            	<li><a href="#" title="Español">Español</a></li>
	                            	<li><a href="eng/registro.asp" title="English">English</a></li>
	                            	<li><a href="ger/registro.asp" title="Portugues">Portugués</a></li>
                            	</ul>
                            </li>                                 
                        </ul> 
                    </nav>                   
                                
           		</div>   
                    
           	</div>
            
        </header>   
    			
        
        
    	<section id="ayuda"> 
            
            <div class="container">
            
           		<div class="text">
                
                	<div class="intro">
                    
                    	<ul class="migas">
                        	<li><a href="default.asp">Inicio</a></li>
                            <li>/</li>
                        	<li><a href="#">Solicitar registro</a></li>
                        </ul>
                        
                    	<h2>Solicitar registro</h2>
                    
                    </div>
                    
                    
                    <div class="condiciones-acceso">

                        <p>Para continuar con el registro debe aceptar las condiciones de adhesión al Portal de Proveedores.</p>
    
                        <div class="info">
                        
                            <h3>Condiciones de uso del Portal de Proveedores</h3>
                        
                            <p>Acceso al Portal</p>
                            <p>Condiciones de uso de la plataforma electrónica de compras<BR /><BR />

Nota preliminar. La palabra “proveedor” también se usa a continuación para compañías que no son actualmente proveedores del Grupo Promotora de Informaciones, S.A., (en adelante PRISA) pero que desean establecer una relación de trabajo con las compañías.<BR /><BR />

Una condición a cumplir por las compañías que son proveedores activos de Grupo PRISA, o que desean registrarse para tomar parte en futuras peticiones de oferta a través del portal de compras, es la introducción en él, el portal, de la descripción general de la compañía.<BR /><BR />

No existe derecho legal por parte del proveedor a participar en las peticiones de oferta.<BR /><BR />

Grupo PRISA se reserva el derecho de seleccionar, deseleccionar o excluir proveedores de la plataforma de acuerdo a criterios internos.<BR /><BR />

Se establecerá un usuario principal por proveedor registrado en el portal de compras.<BR /><BR />

El usuario principal actúa en nombre del proveedor, que está obligado a mantener los datos actualizados de manera continua.<BR /><BR />

Este usuario es responsable, como coordinador de la compañía, del alta y baja de futuros usuarios.
<BR /><BR />
El mantenimiento de los datos de los usuarios, el alta y baja de usuarios, así como el mantenimiento de los datos de la compañía (direcciones, materiales, etc…) será realizado por el usuario principal de cada proveedor.<BR /><BR />

El proveedor será informado de los datos de acceso para el usuario principal de la compañía.
<BR /><BR />
Estos datos serán tratados de forma confidencial, al igual que los de futuros usuarios, los datos de acceso de los usuarios son personales e intransferibles y el acceso a la plataforma debe restringirse a las personas involucradas en la relación comercial.
<BR /><BR />
Las contraseñas de acceso elegidas deben ser de una complejidad adecuada y cambiadas periódicamente por el propio usuario en el mismo portal de compras.<BR /><BR />

Toda la información y documentos intercambiados en el contexto de la relación comercial deben ser tratados con estricta confidencialidad.<BR /><BR />

Dibujos, descripciones, especificaciones y similares pueden ser únicamente guardadas y duplicadas en el contexto de requerimientos operacionales.<BR /><BR />

En el caso de que la información se comparta con una tercera parte por los proveedores (p.e. subcontratación) se debe garantizar la confidencialidad de la misma.<BR /><BR />

No se garantiza la disponibilidad permanente y libre de fallos de los sistemas electrónicos. Grupo PRISA se reserva el derecho de reducir o cesar la operatividad de la plataforma.<BR /><BR />

El proveedor es responsable de su acceso a la plataforma (“Acceso a Internet”) y no se le cargará con coste alguno por este acceso.<BR /><BR />

Todas las partes involucradas se comprometen a mantener las medidas de seguridad conformes a los estándares tecnológicos actuales.<BR /><BR />

El proveedor se compromete a no adjuntar en la plataforma cualquier documento, archivo o dato que pueda poner en peligro o impedir su funcionamiento.<BR /><BR />

De forma particular, debe tomar medidas para no expandir cualquier clase de virus.<BR /><BR />

El proveedor se compromete a utilizar, y mantener actualizado, un antivirus de prestigio reconocido.<BR /><BR />

Las ofertas transmitidas en formato electrónico son totalmente válidas, y las realizadas vía fax o teléfono son legalmente vinculantes.<BR /><BR />

Únicamente los términos contractuales, así como las ofertas, introducidas en la plataforma serán tenidos en cuenta. Cualquier comentario adicional no será vinculante a no ser que sea confirmado por escrito por Grupo PRISA.<BR /><BR />

La información de carácter personal que el proveedor remita a través del área de compras pasará a formar parte de un fichero titularidad de Promotora de Informaciones, S.A. (en adelante, PRISA) con domicilio en la calle Gran Vía 32, 6ª, 28013 de Madrid, con la finalidad del mantenimiento de la relación negocial así como para gestionar otros servicios relacionados con la misma.<BR /><BR />

Asimismo el proveedor otorga su consentimiento para que sus datos personales sean cedidos exclusivamente a otras empresas del Grupo PRISA con las mismas finalidades expresadas en el párrafo anterior. En este sentido, tanto PRISA como el resto de empresas pertenecientes a su grupo empresarial se comprometen a utilizar sus datos exclusivamente para solicitar ofertas y remitir comunicados relativos a la función de compras del Grupo por cualquier medio incluyendo el correo electrónico y a no facilitar sus datos a terceros sin su previo consentimiento.<BR /><BR />

En caso de que facilite datos personales de terceras personas, usted garantiza haberles informado y obtenido su consentimiento para la incorporación de sus datos a un fichero responsabilidad de PRISA, con las finalidades y derechos indicados en el presente documento, todo ello conforme al artículo 5.4. de la ley Orgánica de Protección de Datos.<BR /><BR />

Grupo PRISA le otorga la posibilidad de ejercitar sus derechos de acceso, rectificación, cancelación u oposición de sus datos, notificándolo mediante el envío de un e-mail a la siguiente dirección dcompras@prisa.com .<BR /><BR />

Asimismo, en cumplimiento de la Ley 34/2002 de Servicios de la Sociedad de la Información y Comercio Electrónico, si desea darse de baja y no recibir nuestras solicitudes de ofertas ni nuestra información, envíe un correo electrónico a dcompras@prisa.com indicando en el asunto:”Dar de Baja”.<BR /><BR />

Al enviar su solicitud de registro, la compañía proveedora reconoce las condiciones arriba enumeradas.</p>
                            
                        </div>
                        
                        <p class="text-derecha"><a href="registro_texto.htm">imprimir</a></p>
                          
                    	
                        <form name="frmAlta" id="frmAlta" method="post" >
                        
                        	<span id="sprycheckbox1">
<label class="condiciones">
  <input class="checkbox" type="checkbox" name="opcion1" id="opcion1" required>

<span class="checkboxRequiredMsg">Seleccione para continuar.</span><span> He leído y Acepto las Condiciones Generales de Uso del Portal de Proveedores.</span></label></span>
<br />
                            <button class="btn btn-dcha" type="submit" id="submit1" name="submit1">continuar</button>
                        </form>
                        
                             
               		</div>
                     
                    
               	</div>
                    
         	</div>
              
    	</section>
        
        
        <footer id="footer">
        
        	<div class="container">            
            
            	<div class="footer-soporte">
                
                    <div class="soporte">
                    
                        <div class="soporte-titular">
                            <h3>SOPORTE A PROVEEDORES</h3>
                            <p><strong>Tel. 913 582 787</strong><br /><a href="mailto:proveedores@prisa.com">proveedores@prisa.com</a></p>
                        </div>
                        
                        <div class="soporte-horario">
                            <p><strong>Horario de Atención al Cliente</strong></p>
                            <p>Lunes a Jueves: 9:00 - 18:00</p>
                            <p>Viernes: 8:00 - 14:00</p>
                        </div>
                        
                    </div>
                    
              	</div>
            
           		<div class="text">                
                    
                      <a href="http://www.fullstep.com/portal-de-compras/" target="blank"><img src="img/fullstep.png" alt="fullstep" /></a>  
                    
                    <p><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/aviso-legal.html" title="" >Aviso Legal</a> | <a href="<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=SPA" target="_blank">Política de Cookies</a></p>                    
                    <p>&copy; PRISA S.A. Todos los derechos reservados.</p>                           
                    
               	</div>
               
            </div>
            
        </footer>
                       
          <script type="text/javascript">
// <!--
// var sprycheckbox1 = new Spry.Widget.ValidationCheckbox("sprycheckbox1");
// //-->
$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked')) {
        $('#frmAlta').submit();
        alta();
    }
});
</script>             
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>       

</body>

</html>
