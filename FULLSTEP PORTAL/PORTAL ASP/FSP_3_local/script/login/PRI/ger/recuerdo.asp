<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>

<link rel="canonical" href="/" />

		<link href="../css/reset.css" rel="stylesheet" type="text/css" />        
    	<link href="../css/font-awesome.min.css" rel="stylesheet" />  
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
        <link href="../css/style.css" rel="stylesheet" />  
        
        <!--[if lt IE 9]>
          	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
           
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body>
 <header id="heading">             
            
      		<div class="container">  
            
           		<div class="text"> 
            
                    <div class="logotipo">
            			<a href="http://www.prisa.com" title="Prisa" target="_blank"><img src="../img/prisa.png" alt="Prisa" /></a>
                    </div>  
                    
                    <h1>PORTAL DE FORNECEDORES</h1>                    
                                
           		</div>   
                    
           	</div>
            
        </header>        
    			
        
        
    	<section id="password"> 
            
            <div class="container">
            
           		<div class="text">
                
                	<div class="intro">
                        
                    	<h2>Resetar a senha </h2>
                    
                    </div>
                    
                    
                    <div class="columna">

                        <div class="atencion-proveedor-recuerdo">
                        
                        	<h2>Suporte Fornecedores</h2>
                            <p>Caso não tenha conseguido resolver as suas dúvidas, entre em contacto connosco através do telefone   </span> <strong class="alt">Tel. +34 913 582 787</strong></p>
                            
                        </div>
                        
                   	</div>
                    
                    <div class="centro">
                    
                    	<p>Se você esqueceu sua senha, preencha o código de empresa, utilizador, e-mail e clique em " Enviar". Você receberá um link para uma página para criar uma nova senha. </p>
                         
                        <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
                            <label>
                                <input type="text" name="txtCia" id="txtCia" size="30" maxlength="30" placeholder="Código empresa"/>
                            </label>
                            <label>
                                <input type="text" name="txtUsu" id="txtUsu" size="30" maxlength="30" placeholder="Utilizador"/>
                            </label>
                            <label>
                               <input type="text" name="txtEmail" id="txtEmail" size="30" maxlength="30" placeholder="Email"/>
                            </label>
                              <input class="btn-dcha" name="cmdEnviar" type="submit" style="height:22px; padding:0;" value="Enviar" />
                              <input type="hidden" name="idioma" value="ger"/>
                        </form>
                    
                             
               		</div>
                     
                    
               	</div>
                    
         	</div>
              
    	</section>
        
                               
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/main.js"></script>        
    
</body>

</html>
