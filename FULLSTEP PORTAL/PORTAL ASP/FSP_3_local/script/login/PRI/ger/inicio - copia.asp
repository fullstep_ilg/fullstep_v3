﻿<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>
<html>
<head>

<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
   <link rel="canonical" href="/" />

		<link href="../css/reset.css" rel="stylesheet" type="text/css" />        
    	<link href="../css/font-awesome.min.css" rel="stylesheet" />  
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
        <link href="../css/style.css" rel="stylesheet" />  
        
        <!--[if lt IE 9]>
          	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
<title>::Portal de Proveedores - Prisa::</title>
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
}
-->
</style>
</head>
<script>
dibujaMenu(1)
</script>
<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function Init() {
    document.getElementById('tablemenu').style.display = 'block';

	MM_preloadImages('../images/icono_docs_sm.gif','../images/icono_docs_sm.gif')
	p = window.top.document.getElementById("frSet")
	vRows = p.rows
	vArrRows = vRows.split(",")
	vRows = vArrRows[0] + ",*,0,0"
	p.rows = vRows

}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">
    
    			
        
        
    	<section id="inicio"> 
            
            <div class="container">
            
           		<div class="text">
                
                	<div class="intro">
                    
                        
                    	<h2>Bem-vindo ao Portal de Fornecedores</h2>
                    
                    </div>
                    
                    <div class="centro  col-d">
                    
                    	
                    
                    	<div class="ancla-wrapper">
                        
                           <!-- <h3>Bienvindo ao Portal de Fornecedores</h3>-->
                            
                            <p>Pode aceder às diferentes áreas através das opções de menu que se encontram na parte superior.  </p>
                            
                            <ol class="lista-enum">
                                <li><p> Encomendas: efetue a gestão das encomendas que lhe tenham sido realizadas pelos compradores, acompanhe a sua evolução e aceda ao seu histórico a partir desta secção.</p></li>
                                <li> <p>Pedidos: aceda aos pedidos (RFQ e outros pedidos) e efetue a respetiva gestão a partir desta secção.</li>
                                <li>
                                  <p>Os seus dados/a sua empresa: tem a possibilidade de alterar os dados da sua empresa, dos utilizadores e das áreas de atividade em que a sua empresa participa. </p></li>
                            </ol>
                       
                      	</div>
                        
                        
                    	<div class="ancla-wrapper">
                        
                            <p>Se for a primeira vez que irá efetuar uma oferta através do portal, siga cuidadosamente os seguintes passos:</p>
                            
                            <ol class="lista-enum">
                                <li><p> Clique em "pedidos" para visualizar os pedidos de orçamento que a sua empresa tem pendentes. </p></li>
                                <li><p> Selecione o pedido de orçamento ao qual pretende responder ao clicar sobre o respetivo código. </p></li>
                                <li><p> Efetue o seu orçamento através do preenchimento de todas as informações necessárias: poderá deslocar-se pelas diferentes secções que compõem o orçamento a partir da árvore de navegação que se encontra no lado esquerdo. Para introduzir os preços, deverá aceder à secção "itens/preços". Não se esqueça de introduzir o prazo de validade do orçamento na secção "Dados Gerais do orçamento". </p></li>
                                <li><p> Comunique o seu orçamento ao clicar no botão de enviar <a class="sobre" href="#"><i class="fa fa-envelope"></i></a></p></li>
                            </ol>
                       
                      	</div>                       
                              
                    </div>  
                    
                    <div class="columna col-i">
                    
                    	<div class="bloque-info">
                        	
                            <h2>INSTRUÇÕES</h2>
                            <p>Transfira as instruções sobre o modo como efetuar um orçamento, aceitar uma encomenda, acompanhamento, etc.:</p>
                            <div>
                           
                        	<ul>
                            	<li><a href="../spa/docs/FSN_MAN_ATC_Como ofertar.pdf" target="_blank">Como efetuar o orçamento</a></li>
<li><a href="../spa/docs/FSN_MAN_ATC_Requisitos tecnicos.pdf" target="_blank">Requisitos técnicos</a></li>
<li><a href="../spa/docs/FSN_MAN_ATC_Mantenimiento de datos.pdf" target="_blank">Manutenção dos dados</a></li>

                    		</ul>
	                   
</div>
                        </div>
                    
                    	
                        
                        <div class="atencion-proveedor">
                        
                        	<h2>Suporte Fornecedores</h2>
                            <p>Se você tiver problemas ao acessar o portal,
                    entre em contato conosco <strong>+34 913 582 787</strong> ou envie-nos um e-mail para: <a href="mailto:proveedores@prisa.com">proveedores@prisa.com</a></p>
                            
                        </div>
                        
                   	</div>                  
               
               	</div>
                    
         	</div>
              
    	</section>
        

</body>
</html>
