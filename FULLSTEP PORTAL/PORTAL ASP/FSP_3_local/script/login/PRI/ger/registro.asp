﻿<%@ Language=VBScript %>
<!--#include file="../../../common/XSS.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Prisa</title>
<link rel="canonical" href="/" />

		<link href="../css/reset.css" rel="stylesheet" type="text/css" />        
    	<link href="../css/font-awesome.min.css" rel="stylesheet" />  
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
        <link href="../css/style.css" rel="stylesheet" />  
        
        <!--[if lt IE 9]>
          	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
           
<script type="text/javascript" src="http://fast.fonts.net/jsapi/f5dbd368-355c-4f28-bc2f-217b51498d80.js"></script>
<script src="http://code.jquery.com/jquery-1.9.0.js"></script>
<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=GER", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body>

				<header id="heading">             
            
      		<div class="container">  
            
           		<div class="text"> 
            
                    <div class="logotipo">
            			<a href="http://www.prisa.com" title="Prisa" target="_blank"><img src="../img/prisa.png" alt="Prisa" /></a>
                    </div>  
                    
                    <h1>PORTAL DE FORNECEDORES</h1> 
                    
                         
                    <nav>                      
                        <ul>
                            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ger/ayuda.html" title="Ayuda">Ajuda</a></li>
                            <li>
                            	<strong class="trigger"><i class="fa fa-caret-down"></i> Select language</strong>
                            	<ul>
	                            	<li><a href="../registro.asp" title="Español">Español</a></li>
	                            	<li><a href="../eng/registro.asp" title="English">English</a></li>
	                            	<li><a href="#" title="Portugues">Portugués</a></li>
                            	</ul>
                            </li>                                 
                        </ul> 
                    </nav>                   
                                
           		</div>   
                    
           	</div>
            
        </header>   
    			
        
        
    	<section id="ayuda"> 
            
            <div class="container">
            
           		<div class="text">
                
                	<div class="intro">
                    
                    	<ul class="migas">
                        	<li><a href="default.asp">Inicio</a></li>
                            <li>/</li>
                        	<li><a href="#">Registo</a></li>
                        </ul>
                        
                    	<h2>Registo</h2>
                    
                    </div>
                    
                    
                    <div class="condiciones-acceso">

                        <p>Para prosseguir com o registo, o seguinte contrato deve ser lido e aceito </p>
    
                        <div class="info">
                        
                            <h3>Condições de utilização da plataforma eletrónica de compras
</h3>
                        
                                                     <p>  Nota preliminar: O termo "fornecedor" também se utilizará em referência a empresas que não são atualmente fornecedores do Grupo Promotora de Informaciones, S.A., (doravante PRISA) mas que desejem estabelecer uma relação de trabalho com as companhias.<br>
<br>
Uma condição a ser cumprida pelas empresas que são fornecedores ativos da PRISA, ou que desejem registar-se para participar em futuros pedidos de propostas através do portal de compras, é a introdução no dito portal da descrição geral da companhia.<br>
<br>
Não existe direito legal por parte do fornecedor a participar nos pedidos de propostas.<br>
<br>
 A PRISA reserva-se o direito de selecionar, desselecionar ou excluir fornecedores da plataforma de acordo com os seus critérios internos.<br>
<br>
Estabelecer-se-á um utilizador principal por fornecedor registado no portal de compras.<br>
<br>
O utilizador principal atuará em nome do fornecedor, que está obrigado a manter os dados atualizados de forma contínua.<br>
<br>
Este utilizador é responsável, enquanto coordenador da empresa, pela admissão e exclusão de futuros utilizadores.<br>
<br>
A manutenção dos dados dos utilizadores, o registo e exclusão de utilizadores, assim como a manutenção dos dados da empresa (moradas, materiais, etc.) será realizada pelo utilizador principal de cada fornecedor.<br>
<br>
O fornecedor será informado dos dados de acesso para o utilizador principal da empresa.<br>
<br>
Estes dados serão tratados de forma confidencial, tal como os dos futuros utilizadores; os dados de acesso dos utilizadores são pessoais e intransmissíveis e o acesso à plataforma deve restringir-se às pessoas envolvidas na relação comercial.<br>
<br>
As senhas de acesso escolhidas devem ser de uma complexidade adequada e mudadas periodicamente pelo próprio utilizador no dito portal de compras.<br>
<br>
Toda a informação e documentos trocados no contexto da relação comercial devem ser tratados com a máxima confidencialidade.<br>
<br>
Desenhos, descrições, especificações e documentos semelhantes só podem ser guardados e reproduzidos no contexto de requerimentos operacionais.<br>
<br>
No caso de a informação ser partilhada com uma terceira parte pelos fornecedores (por exemplo, subcontratação), deve-se garantir a confidencialidade da mesma.
Não se garante a disponibilidade permanente e infalível dos sistemas eletrónicos. A PRISA reserva-se o direito de reduzir ou de cessar a operatividade da plataforma.<br>
<br>
O fornecedor é responsável pelo seu acesso à plataforma ("Acesso à Internet") e não lhe será cobrado qualquer custo por este acesso.<br>
<br>
Todas as partes envolvidas comprometem-se a manter as medidas de segurança de acordo com os padrões tecnológicos atuais.<br>
<br>
O fornecedor compromete-se a não carregar para a plataforma qualquer documento, arquivo ou dado que possa colocar em risco ou impedir o seu funcionamento.<br>
<br>
Em particular, deve tomar medidas para não espalhar qualquer género de vírus.<br>
<br>
O fornecedor compromete-se a utilizar e a manter atualizado um antivírus de qualidade reconhecida.<br>
<br>
As ofertas transmitidas em formato eletrónico são totalmente válidas, e as que forem realizadas por fax ou telefone são legalmente vinculativas.<br>
<br>
Apenas os termos contratuais, assim como as ofertas, introduzidos na plataforma serão tidos em conta. Qualquer complemento adicional não será vinculativo, a não ser que seja confirmado por escrito pela PRISA.<br>
<br>
A informação de caráter pessoal que o fornecedor remeta através da área de compras passará a fazer parte de um ficheiro automatizado, propriedade da PRISA, cuja morada legal é Calle Gran Vía 32, 6ª, 28013, Madrid, com a finalidade de manter a relação negocial, assim como para administrar outros serviços relacionados com a mesma.<br>
<br>
Caso nos forneça dados pessoais de terceiros, fá-lo-á com a garantia de os ter informado e de ter obtido o seu consentimento para a inclusão dos seus dados num ficheiro da responsabilidade da PRISA, com as finalidades e direitos indicados no presente documento, tudo de acordo com o artigo 5.4. da Lei Orgânica de Proteção de Dados.<br>
<br>
Do mesmo modo, o fornecedor outorga o seu consentimento para que os seus dados pessoais sejam cedidos exclusivamente a outras empresas da PRISA, com as mesmas finalidades expressas no parágrafo anterior. Neste sentido, tanto a PRISA como as restantes empresas pertencentes ao seu grupo empresarial comprometem-se a utilizar os seus dados exclusivamente para pedidos de propostas e para remeter comunicados relativos à função de compras do Grupo por qualquer meio, incluindo o correio eletrónico, e a não facultar os seus dados a terceiros sem o seu consentimento prévio.<br>
<br>
A PRISA oferece a possibilidade de exercer os seus direitos de acesso, retificação, cancelamento ou revisão dos seus dados, notificando-o através do envio de um e-mail com o seguinte endereço eletrónico: dcompras@prisa.com. Do mesmo modo, de acordo com a Lei 34/2002 dos Serviços da Sociedade da Informação e Comércio Eletrónico, se quiser cancelar e não receber os nossos pedidos de proposta nem as nossas informações, envie um correio eletrónico para dcompras@prisa.com, indicando no assunto: "Cancelamento de conta".<br>
<br>
Ao enviar o seu pedido de registo, a empresa fornecedora aceita as condições acima enumeradas.</p>
                            
                        </div>
                        
                        <p class="text-derecha"><a href="registro_texto.htm">Imprimir</a></p>
                          
                    	
                        <form name="frmAlta" id="frmAlta" method="post" >
                        
                        	<span id="sprycheckbox1">
<label class="condiciones">
  <input class="checkbox" type="checkbox" name="opcion1" id="opcion1" required>

<span class="checkboxRequiredMsg">Selecione para continuar.</span><span> Eu li e aceito as condições gerais de utilização do Portal de Fornecedores.</span></label></span>
<br />
                            <button class="btn btn-dcha" type="submit" id="submit1" name="submit1">registrar</button>
                        </form>
                        
                             
               		</div>
                     
                    
               	</div>
                    
         	</div>
              
    	</section>
        
        
        <footer id="footer">
        
        	<div class="container">            
            
            	<div class="footer-soporte">
                
                    <div class="soporte">
                    
                        <div class="soporte-titular">
                            <h3>SUPORTE A FORNECEDORES</h3>
                            <p><strong>Tel. +34 913 582 787</strong><br /><a href="mailto:proveedores@prisa.com">proveedores@prisa.com</a></p>
                        </div>
                        
                        <div class="soporte-horario">
                            <p><strong>Horário de Atendimento</strong></p>
                            <p>Segunda a Quinta-feira: 9:00 - 18:00</p>
                            <p>Sexta-feira: 8:00 - 14:00</p>
                        </div>
                        
                    </div>
                    
              	</div>
            
           		<div class="text">                
                    
                    <a href="http://www.fullstep.com" target="_blank"><img src="../img/fullstep.png" alt="fullstep" /></a> 
                    
                    <p><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ger/aviso-legal.html" title="" >Aviso Legal </a> | <a href="<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=GER">Cookies Policy</a></p>                    
                    <p>&copy; PRISA S.A. All rights reserved.</p>                           
                    
               	</div>
               
            </div>
            
        </footer>
                       
          <script type="text/javascript">
// <!--
// var sprycheckbox1 = new Spry.Widget.ValidationCheckbox("sprycheckbox1");
// //-->
$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked')) {
        $('#frmAlta').submit();
        alta();
    }
});
</script>             
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/main.js"></script>       

</body>

</html>
