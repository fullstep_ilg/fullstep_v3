<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400' rel='stylesheet' type='text/css'>
        
        <!--[if lt IE 9]>
          	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;
        window.close();
    };
</script>
<body>
    <div id="cont_gen">
        <div id="izd_gen">
          <a href="http://www.fullstep.com" title="Fullstep"><img src="../img/logo.png" alt="Suntory" /></a>
        </div>
        <div id="drc_gen">
        <h1>LOGIN INFORMATION</h1>
            <div class="int">
                <p>
                    If you forgot your password, please fill in your user and email and click on "Submit". You will receive a link to a page to create a new password.
              </p>
                <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
                    <table class="claves">
                    <tr>
                        <td>Company Code:</td>
                        <td><input type="text" name="txtCia" id="txtCia" size="30" maxlength="30"/></td>
                    </tr>
                    <tr>
                        <td>User Code:</td>
                        <td><input type="text" name="txtUsu" id="txtUsu" size="30" maxlength="30"/></td>
                    </tr>
                    <tr>
                        <td>Email:</td>
                        <td><input type="text" name="txtEmail" id="txtEmail" size="30" maxlength="100"/></td>
                    </tr>
                </table>
                    <br /><input class="bt_registro" name="cmdEnviar" type="submit" style="height:22px; padding:0;" value="Submit" />
                     <input type="hidden" name="idioma" value="eng"/>
                </form>
                            <div style="clear:both;"></div>

                <div class="recordar-claves-2">
                    <span class="rojo_24"style="display:block;"><strong>Supplier Help Desk.</strong></span>
                  <span style="display:block;"><strong><br />Tel. +34 902 996 926</strong><br /> <a href="mailto:atencionalcliente@fullstep.com ">atencionalcliente@fullstep.com </a></span>
                    <br />
                </div>
            </div>
        </div>
    </div>
</body>

</html>
