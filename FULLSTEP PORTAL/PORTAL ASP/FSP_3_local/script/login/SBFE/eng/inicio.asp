<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<link href="../css/style.css" rel="stylesheet" type="text/css">
<title>::Supplier Portal::</title>
<!--[if lte IE 9]>
<link href="../estilos_ie9.css" rel="stylesheet" type="text/css" />
<![endif]-->
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
}
-->
</style>
</head>
<script>
dibujaMenu(1)
</script>
<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function Init() {
    document.getElementById('tablemenu').style.display = 'block';

	MM_preloadImages('../images/icono_docs_sm.gif','../images/icono_docs_sm.gif')
	p = window.top.document.getElementById("frSet")
	vRows = p.rows
	vArrRows = vRows.split(",")
	vRows = vArrRows[0] + ",*,0,0"
	p.rows = vRows

}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">
<div id="inicio-left">
  <h1><span class="blue">Welcome to the Supplier Portal</span></h1>
  <ul>
  <li>To navigate through the different areas of the portal, use the navigation bar in the topside of the page. From here you will be able to access:</li>
  <ul><!--
    <li><b>Calidad :</b> gestione sus certificados de calidad, No Conformidades y acceda a sus puntuaciones de calidad.</li>-->
    <li class="padding"><span class="blue">Request for quotations:</span> access the RFQ and other requests sent by the company. </li>
    <!--<li><b>Pedidos:</b> gestione desde aqu&iacute; los pedidos que le han realizado los compradores, siga su evoluci&oacute;n y acceda a su hist&oacute;rico.</li>-->
    <li class="padding"><span class="blue">Your details:</span> update all the personal information stored in the application, and configure the formats (number of decimal places, date formats, etc) that you want the application to display.</li>
    <li class="padding"><span class="blue">Your company:</span> update the general details about your company, point out the materials and services your company can supply and register new users of your company inside the Portal.</li>
  </ul>
<br><br>
  
<li >  If this is the first time you are posting an offer through the portal, please follow these steps:</li>
  <ul>
  <li class="padding"><span class="blue">Click on "Requests for quotation"</span> to display the requests for quotations pending for your company.</li><br>
  <li class="padding"><span class="blue"> Select the request for quotation</span>, for which you want to send an offer by clicking on its code</li><br>
  <li class="padding"><span class="blue">Configure your offers:</span> by filling in the appropriate data. From the navigation tree on the left side of the screen, you will be able to navigate through the different sections of the offer. To fill in the prices, click on the "items/prices" section. Don't forget to fill in the validity dates of your offer in the "General data of the offer" section. </li><br>
  <li class="padding"> <span class="blue">Post your offers </span> by clicking on Send  <IMG height=14 src="../img/sobre_tr.gif" width=30 align=absBottom></li>
 </ul>
 </ul>
</div>
<div id="inicio-right">
<h2>INSTRUCTIONS</h2>
<p>You can download the following tutorials:</p>
<ul class="square">
<li><a href="docs/FSN_MAN_ATC_How to offer.pdf" target="_blank">How to offer</a></li>
<li><a href="docs/FSN_MAT_ATC_Technical Requirements.pdf" target="_blank">Technical Requirements</a></li>
<li><a href="docs/FSN_MAT_ATC_Master data.pdf" target="_blank">Master Data</a></li>
</ul>
<div id="img-atc"><img src="../img/imagen-ATC.jpg"></div>
<p>If you have any doubt or difficulty to operate with the Portal, please contact us through the mail: <a href="mailto:atencionalcliente@fullstep.com" class="contacto"> <span class="contacto">atencionalcliente@fullstep.com</span></a> or calling +34 902 996 926</p>
</div>

</body>
</html>
