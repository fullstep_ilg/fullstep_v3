﻿<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<link href="../css/style.css" rel="stylesheet" type="text/css">
<title>::Portal de proveedores::</title>
<!--[if lte IE 9]>
<link href="../estilos_ie9.css" rel="stylesheet" type="text/css" />
<![endif]-->
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
}
-->
</style>
</head>
<script>
dibujaMenu(1)
</script>
<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function Init() {
    document.getElementById('tablemenu').style.display = 'block';

	MM_preloadImages('../images/icono_docs_sm.gif','../images/icono_docs_sm.gif')
	p = window.top.document.getElementById("frSet")
	vRows = p.rows
	vArrRows = vRows.split(",")
	vRows = vArrRows[0] + ",*,0,0"
	p.rows = vRows

}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">
<div id="inicio-left">
  <h1><span class="blue">Bienvenido al Portal de Proveedores</span></h1>
  <ul>
  <li >Puede acceder a las distintas &aacute;reas a trav&eacute;s de las opciones de men&uacute; situadas en la parte superior:</li>
  <ul list-style-position:inside;><!--
    <li><b>Calidad :</b> gestione sus certificados de calidad, No Conformidades y acceda a sus puntuaciones de calidad.</li>-->
    <li class="padding"><span class="blue">Solicitudes de oferta:</span> acceda a las solicitudes de oferta y gesti&oacute;nelas desde este mismo apartado.</li>
    <!--<li><b>Pedidos:</b> gestione desde aqu&iacute; los pedidos que le han realizado los compradores, siga su evoluci&oacute;n y acceda a su hist&oacute;rico.</li>-->
    <li class="padding"><span class="blue">Sus datos/ su compa&ntilde;&iacute;a :</span> si lo desea puede modificar los datos de su empresa, usuarios, as&iacute; como las &aacute;reas de actividad en las que su empresa se encuentra homologada. </li>
  </ul>
<br><br>
  
<li >  Si es la primera vez que va a realizar una oferta a trav&eacute;s del portal, siga atentamente los siguientes pasos:</li>
  <ul>
  <li class="padding"><span class="blue">Pulse en"solicitudes de oferta</span>&quot; para ver las peticiones de oferta que tiene abiertas su empresa.</li><br>
  <li class="padding"><span class="blue"> Seleccione la solicitud de oferta a la que quiera responder</span>, pulsando sobre el c&oacute;digo de la misma.</li><br>
  <li class="padding"><span class="blue">Realice su oferta completando toda la  informaci&oacute;n necesaria:</span> desde el &aacute;rbol de navegaci&oacute;n que encontrar&aacute; en la parte izquierda, podr&aacute; desplazarse por los distintos apartados que conforman la oferta. Para introducir los precios deber&aacute; ir al apartado "items/precios". Recuerde introducir el plazo de validez de la oferta, en el apartado "Datos Generales de la oferta". </li><br>
  <li class="padding"> <span class="blue">Comunique su oferta </span> pulsando sobre el bot&oacute;n de enviar <IMG height=14 src="../img/sobre_tr.gif" width=30 align=absBottom></li>
 </ul>
 </ul>
</div>
<div id="inicio-right">
<h2>INSTRUCCIONES</h2>
<p>Descárguese las instrucciones sobre cómo realizar una oferta, aceptar un pedido, seguimiento, etc.</p>
<ul class="square">
<li><a href="docs/FSN_MAN_ATC_Cómo ofertar.pdf" target="_blank">Cómo ofertar</a></li>
<li><a href="docs/FSN_MAN_ATC_Requisitos técnicos.pdf" target="_blank">Requisitos técnicos</a></li>
<li><a href="docs/FSN_MAN_ATC_Mantenimiento de datos.pdf" target="_blank">Mantenimiento de datos</a></li>
</ul>
<div id="img-atc"><img src="../img/imagen-ATC.jpg"></div>
<p>Si tiene alguna duda o dificultad para operar con el Portal, póngase en contacto con nostros a través del mail: <a href="mailto: atencionalcliente@fullstep.com" class="contacto"> <span class="contacto">atencionalcliente@fullstep.com</span></a> o llamando al teléfono: 902 996 926</p>
</div>

</body>
</html>
