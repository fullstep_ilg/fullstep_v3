﻿<%@ Language=VBScript %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/idioma.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="css/reset.css" rel="stylesheet" type="text/css" />        
<link href="css/font-awesome.min.css" rel="stylesheet" />  
        
<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
<link href="css/style.css" rel="stylesheet" />  
<link rel="stylesheet" type="text/css" href="css/responsive.css"/>   
<script src="js/jquery-1.9.0.js"></script>
<script src="js/jquery-1.9.0.min.js"></script>
<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
<script src="../../common/formatos.js"></script>
<script language="JavaScript" src="../../common/ajax.js"></script>
<!--#include file="../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=SPA", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body onload="init()">

<header id="heading"> 
        
        	<div class="container"> 
                       
             	<div class="logotipo">
            		<a href="https://www.caixabank.com/index_es.html" title="CaixaBank"><img src="img/caixabank.png" alt="CaixaBank" width="202" height="71" /></a>
                </div>  

                <nav>                      
                    <ul class="idiomas">
                        <li><a href="#" title="CAST"><strong>CAST</strong> | </a></li> 
                        <li><a href="ger/registro.asp" title="CAT"><strong>CAT</strong> | </a></li>
                        <li><a href="eng/registro.asp" title="ENG"><strong>ENG</strong></a> </li>
                    </ul> 
                </nav>  
            
            	<h1>PORTAL DE PROVEEDORES</h1>      
                     
    		</div>
        
        
            <div class="header">
            
                <div class="menu">  
        
                    <a class="menu-rsp" href="#" title="Men&uacute;"><span>Men&uacute;</span></a>
        
                    <div class="menu-container">
                    
                        <ul>
                            <li class="menu-padre"><a href="default.asp" title="Inicio">Inicio</a> </li>   <span class="oculta">|</span>
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/spa/servicios-del-portal.asp" title="Servicios del portal">Servicios del portal</a></li>  <span class="oculta">|</span> 
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/spa/como-ser-proveedor.asp" title="Cómo ser proveedor">Cómo ser proveedor</a></li> <span class="oculta">|</span>  
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/spa/responsabilidad-corporativa.html" title="Responsabilidad corporativa">Responsabilidad corporativa</a></li> <span class="oculta">|</span> 
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/spa/excelencia-servicio.html" title="Excelencia en el servicio">Excelencia en el servicio</a></li> <span class="oculta">|</span>  
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/spa/actualidad.html" title="Actualidad">Actualidad</a></li>
                            
                        </ul>
                                        
                        <ol>
                            <li class="activo"><a href="#" title="CAST">Castellano</a></li> 
                            <li><a href="#" title="CAT">Catalán</a></li>
                            <li><a href="#" title="ENG">English</a> </li>
                        </ol> 
                    
                   	</div>    
            
                </div> 
                
            </div>
            
        </header>           
    			
        
        
    	<section id="servicios"> 
            
            <div class="container">
            
           		<div class="text">
                
                	
                    <div class="columna sol">
                        <div class="imagen">
                      <img src="img/shim.gif" width="100" >
                         </div>  
                        
                   	</div>
                    
                    <div class="centro sin solicitud">
                      <div>
                        
                        <h2><em>SOLICITAR REGISTRO</em></h2>
        <div class="int">
        	<p>Para continuar con el proceso de alta es imprescindible que lea y acepte las condiciones de acceso al Portal de Proveedores.</p>
            <div class="caja_registro">
                <strong class=" blue">IMPORTANTE LEA ATENTAMENTE<br>

Registro de proveedores<br>

Aviso Legal<br>
<br>
Acceso al Portal</strong><br>

El registro como Proveedor en el Portal de Proveedores del Grupo &quot;la Caixa&quot; est&aacute; sujeto a la previa lectura y aceptaci&oacute;n de las siguientes Condiciones Generales. Sin expresar su conformidad con las mismas no podr&aacute; registrarse. Siempre que acceda y utilice el Portal, se entender&aacute; que est&aacute; de acuerdo, de forma expresa, plena y sin reservas, con la totalidad del contenido de las Condiciones Generales vigentes.</p>
<p>El acceso y, en su caso, registro en este Portal no atribuye al Proveedor derecho a exigir a &quot;la Caixa&quot; el otorgamiento de encargo alguno, que &uacute;nicamente se entender&aacute; celebrado cuando as&iacute; se decida y se formalice mediante la suscripci&oacute;n del correspondiente instrumento contractual.</p>
<p> <strong class=" blue">Condiciones Generales</strong><br />
 <strong> 1. Definiciones</strong><br />
  <strong>Grupo &quot;la Caixa&quot;</strong> (en adelante, &ldquo;la Caixa&rdquo;): Caixabank, S.A. es la entidad a trav&eacute;s de la cual la Caixa d&rsquo;Estalvis i Pensions de Barcelona, entidad dominante del Grupo &ldquo;la Caixa&rdquo;, ejerce su actividad de forma indirecta. A los fines de las presentes Condiciones Generales, el t&eacute;rmino Grupo &ldquo;la Caixa&rdquo; (&ldquo;la Caixa&rdquo;) significar&aacute; el grupo de sociedades formado por la Caixa d&rsquo;Estalvis i Pensions de Barcelona y sus sociedades participadas, y el t&eacute;rmino &ldquo;sociedades participadas&rdquo; designar&aacute; a todas y cada una de las integrantes del grupo. Se considerar&aacute; que existe grupo de sociedades cuando concurra alguno de los supuestos establecidos legalmente al efecto.</p>
<p><strong>Proveedor</strong> (en adelante el &ldquo;Proveedor&rdquo; o los &ldquo;Proveedores&rdquo;):empresa o profesional que se registra en el Portal con la finalidad de ofrecer sus Productos y/o Servicios a &quot;la Caixa&quot; y con el cual, en su caso, &quot;la Caixa&quot; contratar&aacute; la prestaci&oacute;n del mencionado Producto o Servicio mediante la formalizaci&oacute;n del instrumento contractual correspondiente.</p>
<p><strong>Portal de Proveedores de &ldquo;la Caixa&rdquo;</strong> (en adelante, el &ldquo;Portal&rdquo;): plataforma que &quot;la Caixa&quot; pone a disposici&oacute;n de los Proveedores que deseen ofertarle sus Productos y/o Servicios. Asimismo, una vez registrado como Proveedor, y para el caso en que el mismo sea contratado por &quot;la Caixa&quot; mediante la formalizaci&oacute;n del correspondiente instrumento contractual, el Portal constituir&aacute; una herramienta de comunicaci&oacute;n entre &quot;la Caixa&quot; y el Proveedor a los efectos de enviar y/o recibir cualquier informaci&oacute;n y documentaci&oacute;n derivada de la relaci&oacute;n contractual. Igualmente, &ldquo;la Caixa&rdquo; podr&aacute; utilizar el Portal para remitir al proveedor aquella informaci&oacute;n que considere de su inter&eacute;s.</p>
<p><strong>Registro:</strong> operaci&oacute;n en virtud de la cual el Proveedor procede a darse de alta en el Portal. En dicha operaci&oacute;n, el Proveedor deber&aacute; introducir 3 claves mediante las cuales se identificar&aacute; en sus posteriores accesos al Portal.</p>
<p><strong>Usuario Registrado:</strong> Proveedor registrado en el Portal de acuerdo con el procedimiento indicado en la definici&oacute;n de Registro. En tanto que usuario registrado, tiene la capacidad de autorizar nuevos usuarios.</p>
<p><strong>Usuario o Usuarios:</strong> persona f&iacute;sica autorizada por el Usuario Registrado para utilizar y operar, en su nombre, en el Portal de Proveedores.</p>
<p><strong>Usuario Principal:</strong> persona f&iacute;sica autorizada por el Proveedor efectivamente contratado por &quot;la Caixa&quot; y que como tal figura designado en el instrumento contractual suscrito entre el Proveedor y &quot;la Caixa&quot; para la prestaci&oacute;n del servicio y/o producto. El cambio de la persona f&iacute;sica as&iacute; autorizada en el correspondiente instrumento contractual deber&aacute; ser comunicado a &quot;la Caixa&quot;, por parte del Proveedor, y por persona con poder suficiente para ello, por un medio que permita acreditar su recepci&oacute;n.</p>
<p><strong>Claves de acceso:</strong> c&oacute;digos de identificaci&oacute;n (&ldquo;c&oacute;digo de Proveedor&rdquo;, nombre de &ldquo;usuario&rdquo; y &ldquo;contrase&ntilde;a&rdquo;) personales e intransferibles que debe introducir cualquier usuario para acceder al Portal.</p>
<p><strong>2. Objeto y &aacute;mbito de aplicaci&oacute;n</strong><br />
  Las presentes Condiciones Generales tienen como objeto regular los t&eacute;rminos y condiciones en los cuales los Proveedores de Productos y/o Servicios, una vez registrados en el Portal de Proveedores de &quot;la Caixa&quot;, suministrar&aacute;n los datos y/o documentaci&oacute;n necesarios para que &quot;la Caixa&quot; pueda estudiar y evaluar su inter&eacute;s en el Producto y/o Servicio ofrecido, as&iacute; como los principios que regir&aacute;n la posible negociaci&oacute;n y ejecuci&oacute;n de los instrumentos contractuales oportunos para, en su caso, contratar, adquirir, implementar, desarrollar y/o gestionar el Producto y/o Servicio.</p>
<p>Igualmente, las presentes Condiciones Generales regular&aacute;n los t&eacute;rminos y condiciones en los cu&aacute;les &quot;la Caixa&quot; y el Proveedor se comunicar&aacute;n a trav&eacute;s del Portal para el caso que se haya producido la efectiva contrataci&oacute;n mediante la formalizaci&oacute;n del correspondiente instrumento contractual.</p>
<p><strong>3. Obligaciones del PROVEEDOR</strong><br />
  Son obligaciones del Proveedor las siguientes:</p>
<p>a) En relaci&oacute;n con el Registro:</p>
<p> a.1. Cumplimentar debidamente la solicitud de Registro</p>
<p> a.2. No utilizar identidades falsas ni suplantar la identidad de otros</p>
<p>b) En relaci&oacute;n con la utilizaci&oacute;n del Portal</p>
<p> b.1. Custodiar diligentemente por parte de cualquier usuario del Portal las claves de acceso al Portal, asumiendo la responsabilidad por los da&ntilde;os y perjuicios que pudieran derivarse de un uso indebido de las mismas.</p>
<p> b.2. No utilizar contrase&ntilde;as o claves de acceso de terceros.</p>
<p> b.3. Hacer un uso correcto del Portal de acuerdo con las presentes Condiciones Generales. En este sentido, el Proveedor se abstendr&aacute; de utilizar el Portal con fines il&iacute;citos, prohibidos, o distintosa los previstos en las presentes Condiciones Generales o lesivos de derechos e intereses de &quot;la Caixa&quot; y/o terceros. En particular, y a t&iacute;tulo meramente indicativo, el Proveedor se compromete a no transmitir, difundir o poner a disposici&oacute;n del Portal y/o terceros informaciones, datos, contenidos, gr&aacute;ficos, archivos de sonido y/o imagen, fotograf&iacute;as, grabaciones, software y, en general, cualquier clase de material que:<br />
</p>



        (i) Sea contrario a los derechos fundamentales y libertades públicas reconocidas en la Constitución, Tratados internacionales y leyes aplicables; a modo enunciativo y no limitativo, que vulnere los derechos al honor, a la intimidad o a la propia imagen;

        (ii) Induzca, incite o promueva actuaciones delictivas, contrarias a la ley, a la moral y buenas costumbres generalmente aceptadas o al orden público;

        (iii) Sea falso, inexacto o pueda inducir a error, o constituya publicidad ilícita, engañosa o desleal;

        (iv) Esté protegido por derechos de propiedad intelectual o industrial pertenecientes a terceros, sin autorización de los mismos;

        (v)Vulnere las normas de Protección de Datos de carácter personal o aquellas relativas al secreto de las comunicaciones;

        (vi)Constituya competencia desleal y/o perjudique la imagen empresarial de “la Caixa” o terceros;

        (vii) Esté afectado por virus o elementos similares que puedan dañar o impedir el funcionamiento adecuado del Portal, los equipos informáticos o sus archivos y documentos.

    b.4. A actuar dentro de los límites de los poderes conferidos en representación del Proveedor.

    b.5. Abstenerse de cualquier manipulación en la utilización informática del Portal.

    b.6. Abstenerse de utilizar las marcas, logos, nombres comerciales, nombres de dominio de internet o signos distintivos que aparezcan en el Portal, ya que son propiedad de “la Caixa” o de terceros, sin que, en ningún caso ni bajo ninguna circunstancia, puedan entenderse cedidos al Proveedor.

 

c) En relación con los datos y la documentación facilitada

    c.1. Indicar solamente aquellas actividades, que se refieran a productos y/o servicios que, en el momento de la aceptación de las presentes Condiciones Generales, el Proveedor comercialice, fabrique o distribuya.

    c.2. Proporcionar cuantos datos y documentos sean necesarios para el adecuado funcionamiento y gestión del sistema, así como mantenerlos actualizados, comunicando a la mayor brevedad posible todo cambio que se produzca en los mismos y, todo ello, tanto en el momento de su registro en el Portal como, si es el caso, una vez se haya producido su contratación por "la Caixa".

    c.3. Garantizar en todo momento la autenticidad y veracidad de los mencionados datos y documentos, actualizándolos para que reflejen, en cada momento, la situación real del Proveedor, siendo el mismo el único responsable de los daños y perjuicios ocasionados a “la Caixa” y/o a terceros como consecuencia de declaraciones inexactas, falsas o no actualizadas.

    c.4. Guardar absoluta confidencialidad en relación con todos los datos y la información que se genere en las relaciones entre el Proveedor y “la Caixa” así como cumplir con lo dispuesto en la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de datos de carácter personal. A estos efectos, el Proveedor mantendrá indemne a "la Caixa" facultándola para repercutirle el importe de todo tipo de indemnizaciones, sanciones y gastos derivados de reclamaciones de las personas afectadas, o promovidas de oficio por la autoridad competente, por negligencia y/o falta de confidencialidad, uso, tratamiento o comunicación indebidos de los datos de carácter personal, o cualquier otra infracción de las normas de protección de datos, siempre que le sea imputable.

    c.5. Los mencionados deberes de secreto y no difusión subsistirán incluso después de haberse dado de baja el Proveedor del Portal.

    c.6. El Proveedor declara conocer y aceptar que los datos y la documentación facilitados al Portal estarán disponibles para el Grupo "la Caixa", tal y como queda definido en el correspondiente apartado de Definiciones.

d) En relación con los estándares éticos, sociales y medioambientales que guían la actividad responsable de "la Caixa"

El Proveedor manifiesta que comprende y acepta el presente documento {icono} que forma parte de estas Condiciones Generales.

 

4. Obligaciones de “la Caixa”

Son obligaciones de “la Caixa” las siguientes:

    Mantener reservada y confidencial toda la información y/o, en su caso, documentación que el Proveedor le suministre a través del Portal.
    Facilitar al Proveedor el acceso al Portal. No obstante lo anterior, “la Caixa” no asumirá responsabilidad alguna derivada de: (i) la falta de disponibilidad del sistema, fallos de la red o técnicos que provoquen un corte o una interrupción en el Portal; (ii) los daños y perjuicios que pueda ocasionar la propagación de virus informáticos u otros elementos en el sistema; o iii) cualquier actuación del del Proveedor o de cualquier usuario que sea contraria a las leyes, usos y costumbres o que bien vulnere las obligaciones establecidas en las presentes Condiciones Generales. Asimismo, "la Caixa" se reserva el derecho de determinar y modificar las condiciones de acceso al Portal.
    Eliminar los datos que sobre el Proveedor figuren en el Portal, para el caso que el Proveedor tramite su baja del mencionado Portal en los términos establecidos en la clausula 5 de estas Condiciones Generales.

 

5. Baja del portal

En caso que el Proveedor desee darse de baja del Portal, deberá enviar un correo electrónico a la dirección portal.proveedores@caixabank.com especificando en el Asunto “Baja del portal”.

Sin perjuicio de lo anterior, no procederá la baja del Portal en los casos en que el Proveedor mantenga vigentes relaciones contractuales con "la Caixa", hasta que se resuelvan las mencionadas relaciones contractuales con el Proveedor, ya que el Portal constituye la herramienta que permite a "la Caixa" la gestión de las mismas.

La falta de la necesaria diligencia o el incumplimiento de las obligaciones establecidas en las condiciones generales por parte del Proveedor, otorgará a “la Caixa” el derecho a excluirlo temporal o permanentemente del Portal. Asimismo, en el supuesto de que el Proveedor hubiera sido contratado por "la Caixa", la citada falta de diligencia o el mencionado incumplimiento, constituirán causa suficiente para la resolución de la relación contractual correspondiente.

6. Comunicaciones

Las comunicaciones entre las partes se realizarán a través del Portal y también por cualquier medio de los admitidos en Derecho, que permita tener constancia de la recepción, incluido el fax.

Los cambios de domicilios y faxes serán notificados por escrito y no producirán efectos hasta transcurridos dos días hábiles desde su recepción. En todo caso, el nuevo domicilio y fax será necesariamente dentro del territorio español.

 

7. Otros

"la Caixa" se reserva el derecho a modificar las presentes Condiciones Generales, en cuyo caso lo comunicará al Proveedor a través del Portal. Las nuevas Condiciones Generales se entenderán tácitamente aceptadas. En el supuesto que el Proveedor no acepte las nuevas Condiciones Generales, deberá comunicarlo por medio fehaciente. La no aceptación de las nuevas Condiciones Generales significará la desactivación de todas aquellas aplicaciones del Portal que impliquen la participación del Proveedor en nuevas ofertas de productos y/o servicios a "la Caixa", quedando limitado el uso por parte de ese Proveedor del Portal a la comunicación con "la Caixa" a los efectos de enviar y/o recibir información y/o documentación derivada de la relación contractual que en su caso exista.

Las relaciones entre “la Caixa” y el proveedor derivadas del uso del Portal se someterán a la legislación española y a la jurisdicción de los Juzgados y Tribunales de la ciudad de Barcelona, con renuncia expresa por parte del Proveedor a su propio fuero y domicilio si fueren otros.

		  </div>

            <p class="imprimir"><a href="registro_texto.html" class="" target="blank">Imprimir</a> </p>
<div class="caja_acepto">
<form name="frmAlta" id="frmAlta" method="post" style="max-width:740px;">
  <span id="sprycheckbox1">

<span class="checkboxRequiredMsg">Seleccione para continuar.</span>
</span>
<br>
<br>

  <input type="checkbox" name="opcion1" id="opcion1" required>

He le&iacute;do y acepto el contrato de adhesi&oacute;n al portal de proveedores de CaixaBank, as&iacute; como los <a href="spa/docs/Portal_Caixa_Estandares.pdf" target="_blank">est&aacute;ndares &eacute;ticos, sociales y medioambientales</a> del Grupo &quot;la Caixa&quot;</label><br />


<br />
<button type="button" class="bt_registro" id="submit1" name="submit1">Continuar</button>
<br>
</form>


</div>
<div style="clear:both;"></div>
    	</div>    
<p>&nbsp;</p></div>
</div>
<script type="text/javascript">
// <!--
// var sprycheckbox1 = new Spry.Widget.ValidationCheckbox("sprycheckbox1");
// //-->
$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked')) {
        $('#frmAlta').submit();
        alta();
	}
	else
       alert("Debe aceptar las condiciones de uso del portal para poder registrarse");
    
});

</script>
                      	</div>
                    </div>
                    
                 
               
           	  </div>
                    
         	</div>
              
    	</section>
        
        
    	
 			
 <footer id="footer">
        
        	<div class="container">            
                    
            	<div class="footer-menu">
                
                    <ul>
                    	<li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/spa/faq.html">Preguntas Frecuentes</a></li>
                    	<li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/spa/aviso-legal.html">Aviso Legal</a></li>
                   <!-- 	<li><a href="#">Información Legal</a></li>
                    	<li>Política de Cookies</li>-->
                    </ul>
                    
                    <p>Web optimizada para 1280 x 1024. Navegadores soportados: Internet Explorer y Mozilla Firefox<br />Caixabank, S.A. ©  Todos los Derechos Reservados.</p>
                    
                    <a href="http://www.fullstep.com" title="Fullstep" target="_blank"><img class="fullstep" src="img/fullstep.png" /></a>
                    
              	</div>
            
           		<div class="text">                                 
                    <p>Caixabank, S.A. Av. Diagonal, 621 08028-Barcelona NIF A08663619. Inscrita en el Registro Administrativo Especial del Banco de España con el número 2100 e inscrita
en el Registro Mercantil de Barcelona, tomo 42.657, folio 33, hoja B-41232, inscripción 109ª y con número de identificación fiscal A-08663619.</p>    
               	</div>
               
            </div>
           
        </footer>
                       
                       
        </body>

<!--#include file="../../common/fsal_2.asp"-->

</html>
