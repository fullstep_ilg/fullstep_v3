﻿<%@  language="VBScript" %>
<!--#include file="../../recuerdo.asp"-->
<html>
<head>
    <link href="../estilos.css" rel="stylesheet" type="text/css">
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
	 <link rel="canonical" href="/" />

		<link href="../css/reset.css" rel="stylesheet" type="text/css" />        
    	<link href="../css/font-awesome.min.css" rel="stylesheet" />  
        
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
        <link href="../css/style.css" rel="stylesheet" />  
        <link rel="stylesheet" type="text/css" href="../css/responsive.css">   
        
        <!--[if lt IE 9]>
          	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
       


</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
  <body class="popup">       
        
        
        <header id="heading">             
            
      		<div class="container">  
            
           		<div class="text"> 
            
                  <div class="logotipo">
           			<a href="https://www.caixabank.com/index_es.html" title="CaixaBank"><img src="../img/caixabank.png" alt="CaixaBank" width="202" height="71" /></a>
                  </div> 
                    <div class="cierra-popup"><img src="../img/cerrar.gif" alt="Cerrar" class="cerrar"></div>                  
                                
       		  </div>   
                    
           	</div>
            
        </header>        
    			
        
        
    	<section id="ayuda"> 
            
            <div class="container">
    <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
    <input type="hidden" name="idioma" value="spa">

            
           		<div class="text blue">
                
                	<div class="columna">
                        <div class="imagen">
                        <img src="../img/imagen_password.jpg" alt="imagen_password">
                         </div>  
               	  </div>
                    
                    <div class="centro sin">
                    
                    	<h4>Recuperaci&oacute; de contrasenya</h4>
                    
                    	<p>Si ha oblidat les seves claus d'accés, introdueixi l’adreça de correu electrònic
                    registrada en el portal, i a continuació rebrà un correu electrònic amb les seves
                    claus.</p>
                         <table>
                    <tr>
                        <td>Codi Companyia:</td>
                        <td><input type="text" name="txtCia" id="txtCia" size="30" maxlength="30"/></td>
                    </tr>
                    <tr>
                        <td>Codi Usuari:</td>
                        <td><input type="text" name="txtUsu" id="txtUsu" size="30" maxlength="30"/></td>
                    </tr>
                    <tr>
                        <td>Adreça  email:</td>
                        <td><input type="text" name="txtEmail" id="txtEmail" size="30" maxlength="100"/></td>
                    </tr>
                </table>
                    <br /><input class="btn" name="cmdEnviar" type="submit" style="height:22px; padding:0;" value="Enviar" />
                         
                       <!-- <form>
                            <label>
                                <input type="text" placeholder="Código de Compañía" />
                            </label>
                            <label>
                                <input type="text" placeholder="Código de Usuario" />
                            </label>
                            <label>
                                <input type="password" placeholder="Contraseña" />
                            </label>
                            <button class="btn" type="submit">Entrar</button>
                        </form>-->
                    <div class="capa-cierre-recup">
                    <div class="imagen-capa-cierre"><img src="../img/telefonista.jpg"></div>
                    <div class="texto-capa-cierre"> Si continua amb dificultats per poder accedir al portal o la seva adreça de correu
                    electrònic ha canviat, consulti amb el servei d'atenció a proveïdors</strong>.
                      <strong class="telefono">Tel. 902 053 218</strong></div>
                    
                    </div> 
                             
               		</div>
                    
                    
               	</div>
                    </form>
         	</div>
              
    	</section>
        
                               
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/main.js"></script>        
    
    
    </body>
<!--
    <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
    <input type="hidden" name="idioma" value="spa">
    <table width="640" border="0" cellpadding="0" cellspacing="0" class="textos">
        <tr>
            <td width="3">&nbsp;
                
            </td>
            <td colspan="3" align="left">
                <a href="http://www.caixabank.com" target="_blank">
                    <img src="../images/logo.png" width="280" height="81" border="0"></a>
            </td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;
                
            </td>
        </tr>
        <tr>
            <td width="3">&nbsp;
                
            </td>
            <td width="11" align="left">
                <td width="620" align="left">
                    <b><u>Recordatorio de claves de acceso </u></b>
                    <br>
                    <br>
                    Si ha olvidado sus claves de acceso, introduza la dirección de e-mail registrada
                    en el portal, y a continuación recibirá un e-mail con sus claves.<br>
                    <br>
                    <br>
                    <table class="textos">
                        <tr>
                            <td>Cód. Compañia:</td>
                            <td><input type="text" name="txtCia" id="txtCia" size="80" maxlength="100"/></td>
                        </tr>
                        <tr>
                            <td>Cód. Usuario:</td>
                            <td><input type="text" name="txtUsu" id="txtUsu" size="80" maxlength="100/"></td>
                        </tr>
                        <tr>
                            <td>Dirección de email:</td>
                            <td><input type="text" name="txtEmail" id="txtEmail" size="80" maxlength="100"/></td>
                        </tr>
                    </table>
                    <br /> 
                    <input type="submit" value="Enviar" name="cmdEnviar" id="cmdEnviar"/></nobr>
                    <br>
                    <br>
                    <br>
                    Si continúa con dificultades para poder acceder al portal o su dirección de email
                    ha cambiado, consulte con el servicio de atención a proveedores.<br>
                    <br>
                    <br>
                </td>
            </td>
        </tr>
    </table>
    -->

</html>
