﻿<%@ Language=VBScript %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/fsal_1.asp"-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/reset.css" rel="stylesheet" type="text/css" />        
<link href="../css/font-awesome.min.css" rel="stylesheet" />  
        
<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
<link href="../css/style.css" rel="stylesheet" />  
<link rel="stylesheet" type="text/css" href="../css/responsive.css"/>   
<script src="../js/jquery-1.9.0.js"></script>
<script src="../js/jquery-1.9.0.min.js"></script>
<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
<script src="../../../common/formatos.js"></script>
<script language="JavaScript" src="../../../common/ajax.js"></script>
<!--#include file="../../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=GER", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body onload="init()">

<header id="heading"> 
        
        	<div class="container"> 
                       
             	<div class="logotipo">
            		<a href="https://www.caixabank.com/index_es.html" title="CaixaBank"><img src="../img/caixabank.png" alt="CaixaBank" width="202" height="71" /></a>
                </div>  

                <nav>                      
                    <ul class="idiomas">
                        <li><a href="../registro.asp" title="CAST"><strong>CAST</strong> | </a></li> 
                        <li><a href="#" title="CAT"><strong>CAT</strong> | </a></li>
                        <li><a href="../eng/registro.asp" title="ENG"><strong>ENG</strong></a> </li>
                    </ul> 
                </nav>  
            
            	<h1>PORTAL DE PROVEÏDORS</h1>      
                     
    		</div>
        
        
            <div class="header">
            
                <div class="menu">  
        
                    <a class="menu-rsp" href="#" title="Men&uacute;"><span>Men&uacute;</span></a>
        
                    <div class="menu-container">
                    
                         <ul>
                            <li class="menu-padre"><a href="default.asp" title="Inicio">Inici</a> </li>   <span class="oculta">|</span>
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ger/servicios-del-portal.asp" title="Serveis del portal">Serveis del portal</a></li>  <span class="oculta">|</span> 
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ger/como-ser-proveedor.asp" title="Com ser proveïdor">Com ser proveïdor</a></li> <span class="oculta">|</span>  
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ger/responsabilidad-corporativa.html" title="Responsabilitat corporativa">Responsabilitat corporativa</a></li> <span class="oculta">|</span> 
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ger/excelencia-servicio.html" title="Excel·lència en el servei">Excel·lència en el servei</a></li> <span class="oculta">|</span>  
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ger/actualidad.html" title="Actualitat">Actualitat</a></li>
                            
                        </ul>
                                        
                        <ol>
                            <li class="activo"><a href="#" title="CAST">Castellano</a></li> 
                            <li><a href="#" title="CAT">Catalán</a></li>
                            <li><a href="#" title="ENG">English</a> </li>
                        </ol> 
                    
                   	</div>    
            
                </div> 
                
            </div>
            
        </header>           
    			
        
        
    	<section id="servicios"> 
            
            <div class="container">
            
           		<div class="text">
                
                	
                    <div class="columna sol">
                        <div class="imagen">
                      <img src="../img/shim.gif" width="100" >
                         </div>  
                        
                   	</div>
                    
                    <div class="centro sin solicitud">
                      <div>
                        
                        <h2><em>SOL•LICITAR REGISTRE</em></h2>
        <div class="int">
        	<p>Per continuar amb el procés d'alta és imprescindible que llegeixi i accepti les condicions d'accés al Portal:</p>
            <div class="caja_registro">
                <strong class=" blue">IMPORTANT LLEGEIXI ATENTAMENT<br>

Registre de proveïdors<br>

Avís Legal<br>
<br>
Accés al Portal</strong><br>

El registre com a proveïdor al Portal de proveïdors del Grup "la Caixa" està condicionat a la lectura prèvia i acceptació de les següents condicions generals. Sense expressar la seva conformitat amb les dites condicions no podrà registrar-se. Sempre que accedeixi i utilitzi el Portal, s’entendrà que està d’acord, de manera expressa, plena i sense reserves, amb la totalitat del contingut de les condicions generals vigents.</p>
<p>L’accés i, si escau, registre en aquest Portal no atribueix al proveïdor dret a exigir a "la Caixa" l’atorgament de cap encàrrec, que únicament s’entendrà celebrat quan així es decideixi i es formalitzi mitjançant la subscripció del corresponent instrument contractual.</p>
<p> <strong class=" blue">Condicions Generals</strong><br />
 <strong> 1. Definicions</strong><br />
 <strong>Grup &quot;la Caixa&quot;</strong> (d’ara endavant, “la Caixa”): Caixabank, S.A. és l’entitat a través de la qual la Caixa d’Estalvis i Pensions de Barcelona, entitat dominant del Grup “la Caixa”, exerceix la seva activitat de forma indirecta. A aquestes condicions generals, el terme Grup “la Caixa” (“la Caixa”) significarà el grup de societats format per la Caixa d’Estalvis i Pensions de Barcelona i les seves societats participades, i el terme “societats participades” designarà a totes i cadascuna de les integrants del grup. Es considerarà que existeix grup de societats quan concorri algun dels supòsits establerts legalment a l’efecte.</p>
<p><strong>Proveïdor</strong>  (d’ara endavant el “proveïdor” o els “proveïdors”): empresa o professional que es registra al Portal amb la finalitat d’oferir els seus productes i/o serveis a "la Caixa" i amb el qual, si escau, "la Caixa" contractarà la prestació del susdit producte o servei mitjançant la formalització de l’instrument contractual corresponent.</p>
<p><strong>Portal de proveïdors de “la Caixa” </strong> (d’ara endavant, el “Portal”): plataforma que "la Caixa" posa a disposició dels proveïdors que vulguin oferir-li els seus productes i/o serveis. Així mateix, una vegada registrat com a proveïdor, i per al cas en que aquest sigui contractat per "la Caixa" mitjançant la formalització del corresponent instrument contractual, el Portal constituirà una eina de comunicació entre "la Caixa" i el proveïdor als efectes d’enviar i/o rebre qualsevol informació i documentació derivada de la relació contractual. “la Caixa” també podrà utilitzar el Portal per remetre al proveïdor aquella informació que consideri del seu interès.</p>
<p><strong>Registre:</strong>  operació en virtut de la qual el proveïdor procedeix a donar-se d’alta al Portal. En aquesta operació, el proveïdor haurà d’introduir 3 claus mitjançant les quals s’identificarà en els seus accessos posteriors al Portal.</p>
<p><strong>Usuari Registrat:</strong> proveïdor registrat al Portal d’acord amb el procediment indicat en la definició de Registre. Com a usuari registrat té la capacitat d’autoritzar nous usuaris.</p>
<p><strong>Usuari Principal:</strong> persona física autoritzada pel proveïdor efectivament contractat per "la Caixa" i que com a tal figura designat en l’instrument contractual subscrit entre el proveïdor i "la Caixa" per a la prestació del servei i/o producte. El canvi de la persona física així autoritzada en el corresponent instrument contractual haurà de ser comunicat a "la Caixa", per part del proveïdor, i per persona amb poder suficient per a fer-ho, per un mitjà que permeti acreditar la seva recepció.</p>
<p><strong>Claus d'accés:</strong> codis d’identificació (“codi de proveïdor”, nom d'“usuari” i “clau d’accés”) personals i intransferibles que ha d’introduir qualsevol usuari per accedir al Portal.</p>
<p><strong>2. Objecte i àmbit d’aplicació</strong><br />
  Aquestes condicions generals tenen com a objecte regular els termes i condicions amb els quals els proveïdors de productes i/o serveis, una vegada registrats al Portal de proveïdors de "la Caixa", subministraran les dades i/o documentació que calguin perquè "la Caixa" pugui estudiar i avaluar el seu interès en el producte i/o servei ofert, així com els principis que regiran la possible negociació i execució dels instruments contractuals oportuns per a contractar, adquirir, implementar, desenvolupar i/o gestionar el producte i/o servei.
</p>
<p>Aquestes condicions generals també regularan els termes i condicions perquè "la Caixa" i el proveïdor es comuniquin a través del Portal si s’ha produït l'efectiva contractació mitjançant la formalització del corresponent instrument contractual.</p>
<p><strong>3. Obligacions del PROVEÏDOR</strong><br />
  Les obligacions del proveïdor són les següents:</p>
<p>a) En relació amb el registre:</p>
<p class="sang"> a.1. Emplenar degudament la sol•licitud de registre.</p>
<p class="sang"> a.2. No utilitzar identitats falses ni suplantar la identitat d’altres persones.</p>
<p>b) En relació amb la utilització del Portal</p>
<p class="sang"> b.1. Custodiar diligentment per part de qualsevol usuari del Portal les claus d’accés al Portal, assumint la responsabilitat pels danys i perjudicis que poguessin derivar-se d’un ús indegut d’aquestes.</p>
<p class="sang"> b.2. No utilitzar contrasenyes o claus d’accés de tercers.</p>
<p class="sang"> b.3. Fer un ús correcte del Portal d’acord amb aquestes condicions generals. En aquest sentit, el proveïdor s’abstindrà d’utilitzar el Portal amb fins il•lícits, prohibits, o diferents als previstos en aquestes condicions generals o lesius de drets i interessos de "la Caixa" i/o tercers. En particular, i a títol merament indicatiu, el proveïdor es compromet a no transmetre, difondre o posar a disposició del Portal i/o tercers informacions, dades, continguts, gràfics, arxius de so i/o imatge, fotografies, enregistraments, programari i, en general, qualsevol classe de material que:</p>


        (i)  Sigui contrari als drets fonamentals i llibertats públiques reconegudes en la Constitució, tractats internacionals i lleis aplicables; a tall enunciatiu i no limitador, que vulneri els drets a l’honor, a la intimitat o a la pròpia imatge;<br><br>

        (ii)  Indueixi, inciti o promogui actuacions delictives, contràries a la llei, a la moral i bons costums generalment acceptats o a l’ordre públic;<br><br>

        (iii) Sigui fals, inexacte o pugui induir a error, o constitueixi publicitat il•lícita, enganyosa o deslleial;<br><br>

        (iv) Estigui protegit per drets de propietat intel•lectual o industrial que pertanyen a tercers, sense autorització d’aquests;<br><br>

        (v)Vulneri les normes de protecció de dades de caràcter personal o aquelles relatives al secret de les comunicacions;<br><br>
        (vi)Constitueixi competència deslleial i/o perjudiqui la imatge empresarial de “la Caixa” o tercers;<br><br>

        (vii) Estigui afectat per virus o elements similars que puguin danyar o impedir el funcionament adequat del Portal, els equips informàtics o els seus arxius i documents.<br><br>

    b.4. A actuar dins els límits dels poders conferits en representació del proveïdor.<br><br>

    b.5. Abstenir-se de qualsevol manipulació en la utilització informàtica del Portal.<br><br>

    b.6. Abstenir-se d’utilitzar les marques, logotips, noms comercials, noms de domini d’Internet o signes distintius que apareguin al Portal, atès que són propietat de “la Caixa” o de tercers, sense que, en cap cas ni sota cap circumstància, puguin entendre’s cedits al proveïdor.<br><br>

 

c) En relació amb les dades i la documentació facilitada <br><br>

    c.1. Indicar només aquelles activitats que es refereixin a productes i/o serveis que, en el moment de l’acceptació d’aquestes condicions generals, el proveïdor comercialitzi, fabriqui o distribueixi.
<br><br>
    c.2. Proporcionar totes les dades i documents que calguin per a l'adequat funcionament i gestió del sistema, així com mantenir-los actualitzats, comunicant tan aviat com sigui possible qualsevol canvi que es produeixi en aquests tant en el moment del seu registre al Portal com, si és el cas, una vegada s’hagi produït la seva contractació per "la Caixa".<br><br>

   c.3. Garantir en qualsevol moment l’autenticitat i veracitat de les dades i documents esmentats, actualitzant-los perquè reflecteixin, en tot moment, la situació real del proveïdor. Aquest serà l’únic responsable dels danys i perjudicis ocasionats a “la Caixa” i/o a tercers com a conseqüència de declaracions inexactes, falses o no actualitzades.<br><br>

   c.4. Guardar confidencialitat absoluta en relació amb totes les dades i la informació que es generi en les relacions entre el proveïdor i “la Caixa” així com complir amb el que disposa la Llei Orgànica 15/1999, de 13 de desembre, de Protecció de dades de caràcter personal. A aquests efectes, el proveïdor mantindrà indemne a "la Caixa" facultant-la per repercutir-li l'import de tot tipus d’indemnitzacions, sancions i despeses derivades de reclamacions de les persones afectades, o promogudes d’ofici per l’autoritat competent, per negligència i/o falta de confidencialitat, ús, comunicació o tractament indeguts de les dades de caràcter personal, o qualsevol altra infracció de les normes de protecció de dades, sempre que li sigui imputable.<br><br>

    c.5. Els susdits deures de secret i no difusió subsistiran fins i tot després d'haver-se donat de baixa el proveïdor del Portal.<br><br>

   c.6. El proveïdor declara conèixer i acceptar que les dades i la documentació facilitades al Portal estaran disponibles per al Grup "la Caixa", tal com queda definit en el corresponent apartat de definicions.<br><br>

d) En relació amb els estàndards ètics, socials i mediambientals que guien l’activitat responsable de "la Caixa"
El proveïdor manifesta que comprèn i accepta aquest document {icona} que forma part d’aquestes condicions generals.<br><br>

 

<strong>4. Obligacions de “la Caixa”</strong><br><br>

Les obligacions de “la Caixa” són les següents:<br><br>

   a. Mantenir reservada i confidencial tota la informació i/o, si escau, documentació que el proveïdor li subministri a través del Portal.<br><br>
   b. Facilitar al proveïdor l’accés al Portal. Malgrat tot el que s’ha esmentat anteriorment, “la Caixa” no assumirà cap responsabilitat derivada de: (i) la falta de disponibilitat del sistema, errades de la xarxa o tècnics que provoquin un tall o una interrupció al Portal; (ii) els danys i perjudicis que pugui ocasionar la propagació de virus informàtics o uns altres elements en el sistema; o iii) qualsevol actuació del proveïdor o de qualsevol usuari que sigui contrària a les lleis, usos i costums o que bé vulneri les obligacions establertes en aquestes condicions generals. Així mateix, "la Caixa" es reserva el dret de determinar i modificar les condicions d’accés al Portal. <br><br>
 c. Eliminar les dades que sobre el proveïdor figurin al Portal en cas que el proveïdor tramiti la seva baixa de l'esmentat Portal en els termes establerts en la clàusula 5 d’aquestes condicions generals.<br><br>

 

<strong>5. Baixa del portal</strong><br><br>

Si el proveïdor vol donar-se de baixa del Portal, haurà d’enviar un correu electrònic a l’adreça <a href="mailto: portal.proveedores@caixabank.com">portal.proveedores@caixabank.com</a> especificant en l’assumpte “Baixa del portal”. <br><br>

Sense perjudici de tot l’anterior, no procedirà la baixa del Portal en els casos en els quals el proveïdor mantingui vigents relacions contractuals amb "la Caixa", fins que es resolguin aquestes relacions contractuals amb el proveïdor, ja que el Portal constitueix l’eina que permet a "la Caixa" la gestió d’aquestes.<br><br>

La falta de la diligència necessària o l'incompliment de les obligacions establertes en les condicions generals per part del proveïdor, atorgarà a “la Caixa” el dret a excloure’l temporalment o permanentment del Portal. Així mateix, en cas que el proveïdor hagués estat contractat per "la Caixa", la dita falta de diligència o l'esmentat incompliment constituiran causa suficient per a la resolució de la relació contractual corresponent.
<br><br>
<strong>6. Comunicacions</strong><br><br>

Les comunicacions entre les parts es realitzaran a través del Portal i també per qualsevol mitjà dels admesos en dret, que permeti tenir constància de la recepció, inclòs el fax.<br><br>

Els canvis de domicili i fax s’han de notificar per escrit i no produiran efectes fins a transcorreguts dos dies hàbils des de la seva recepció. En qualsevol cas, el nou domicili i fax serà necessàriament dins el territori espanyol.<br><br>

 

<strong>7. Altres</strong><br><br>

"La Caixa" es reserva el dret a modificar aquestes condicions generals, en aquest cas ho comunicarà al proveïdor a través del Portal. Les noves condicions generals s’entendran tàcitament acceptades. En cas que el proveïdor no accepti les noves condicions generals, haurà de comunicar-ho per mitjà fefaent. La no acceptació de les noves condicions generals significarà la desactivació de totes aquelles aplicacions del Portal que impliquin la participació del proveïdor en noves ofertes de productes i/o serveis a "la Caixa", l’ús per part d’aquest proveïdor del Portal quedarà limitat a la comunicació amb "la Caixa" als efectes d’enviar i/o rebre informació i/o documentació derivada de la relació contractual existent.<br><br>

Les relacions entre “la Caixa” i el proveïdor derivades de l’ús del Portal se sotmetran a la legislació espanyola i a la jurisdicció dels Jutjats i Tribunals de la ciutat de Barcelona, amb renúncia expressa per part del proveïdor al seu propi fur i domicili si fossin altres.<br><br>

		  </div>

            <p class="imprimir"><a href="registro_texto.html" class="" target="blank">Imprimir</a> </p>
<div class="caja_acepto">
<form name="frmAlta" id="frmAlta" method="post" style="max-width:740px;">
  <span id="sprycheckbox1">

<span class="checkboxRequiredMsg">Seleccione para continuar.</span>
</span>
<br>
<br>

  <input type="checkbox" name="opcion1" id="opcion1" required>

He llegit i accepto el contracte d'adhesió al portal de proveïdors de CaixaBank, així com els  <a href="../spa/docs/Portal_Caixa_Estandares.pdf" target="_blank">estàndards ètics, socials i mediambientals del Grup "la Caixa"</a><br />


<br />
<button type="button" class="bt_registro" id="submit1" name="submit1">Continuar</button>
<br>
</form>


</div>
<div style="clear:both;"></div>
    	</div>    
<p>&nbsp;</p></div>
</div>
<script type="text/javascript">
// <!--
// var sprycheckbox1 = new Spry.Widget.ValidationCheckbox("sprycheckbox1");
// //-->
$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked')) {
        $('#frmAlta').submit();
        alta();
	}
	else
       alert("Ha d'acceptar les condicions d'ús del portal per poder registrar-se com a proveïdor");
    
});

</script>
                      	</div>
                    </div>
                    
                 
               
           	  </div>
                    
         	</div>
              
    	</section>
        
        
    	
 			
 <footer id="footer">
        
        	<div class="container">            
                    
            	<div class="footer-menu">
                
                    <ul>
                    	<li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ger/faq.html">Preguntes Freqüents</a></li>
                    	<li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ger/aviso-legal.html">Avís Legal</a></li>
                   <!-- 	<li><a href="#">Información Legal</a></li>
                    	<li>Política de Cookies</li>-->
                    </ul>
                    
                    <p>Web optimitzada per 1280 x 1024. Navegadors suportats: Internet Explorer i Mozilla Firefox<br />Caixabank, S.A. ©  Tots els Drets Reservats.</p>
                    
                   <a href="http://www.fullstep.com" title="Fullstep" target="_blank"><img class="fullstep" src="../img/fullstep.png" /></a>
                    
              	</div>
            
           		<div class="text">                                 
                    <p>Caixabank, S.A. Av. Diagonal, 621 08028-Barcelona NIF A08663619. Inscrita en el Registro Administrativo Especial del Banco de España con el número 2100 e inscrita
en el Registro Mercantil de Barcelona, tomo 42.657, folio 33, hoja B-41232, inscripción 109ª y con número de identificación fiscal A-08663619.</p>    
               	</div>
               
            </div>
           
        </footer>
                       
                       
        </body>

<!--#include file="../../../common/fsal_2.asp"-->

</html>
