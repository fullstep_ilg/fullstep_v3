﻿<%@ Language=VBScript %>
<!--#include file="../../../common/acceso.asp"-->
<script SRC="../../../common/menu.asp"></script>

<html>

<head>
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>:: Portal de Proveïdors ::</title>

<style type="text/css">
<!--
body {
	margin-top: 0px;
	margin-left: 0px;
}
-->
</style>
<link href="../estilos.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
    function Init() {
        document.getElementById('tablemenu').style.display = 'block';

        p = window.top.document.getElementById("frSet")
        vRows = p.rows
        vArrRows = vRows.split(",")
        vRows = vArrRows[0] + ",*,0,0"
        p.rows = vRows
    }
</script>
</head>
<body scroll="yes" onLoad="Init()">
<script>
dibujaMenu(1)
</script>
<script language="javascript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}

</script>

<!-------------------------- BEGIN COPYING THE HTML HERE ---------------------------->

<table width="100%" border="0" cellpadding="5" cellspacing="0" bordercolor="0" hspace="0" vspace="0">
  <tr> 
    <td width="1%" rowspan="2" align="right" valign="top" bordercolor="0"> <div align="left" class="textos"></div>
    </td>
    <td height="57" colspan="3" align="left" valign="middle" class="titulo3P"><font size="2" face="verdana" class="titulo1P"><b> Benvingut a la zona privada de proveïdors</b></font></td>
  </tr>
  <tr>
    <td width="60%" valign="top" class="textos"><table width="100%" border="0" align="center" cellpadding="1" cellspacing="0" bgcolor="#eeeeee" class="textos">
  <tr>
    <td><p>Aquí trobar&agrave; contractes, informaci&oacute; i documentaci&oacute; que CaixaBank necessita per un adequat registre com a prove&iuml;dor.</p>
      <p>        Pot accedir a les diferents àrees a través de les opcions de menú situades en la part superior.<br>
      </p>
	   <ul>
	   	<li>
	   	  <p><strong>Qualitat</strong> : En aquest apartat vost&egrave; podr&agrave; contestar i actualitzar la informaci&oacute; que el Grup Caixa li sol.liciti.</p>
	   	</li>
		<li>
	   	  <p><strong>Contractes</strong> : Pot accedir als contractes que  CaixaBank ha publicat per al seu coneixement.</p>
	   	</li>
		<li>
		  <p><strong>Les seves dades/ la seva empresa</strong>: Pot gestionar les dades de la seva empresa, usuaris, així com les àrees d'activitat en les quals la seva empresa s'ofereix per treballar en el Grup Caixa. </p>
		</li>
     	 </ul>
      <p>Premi en la secci&oacute; del seu inter&egrave;s, en el men&uacute; superior, i procedeixi a complimentar la informaci&oacute; sol.licitada.<br>
        <br>
      </p>
      </td> 
  </tr>
</table>
    </td>
    <td width="39%" colspan="2" align="right" valign="top"> 
      <table width="100%" border="0" align="right" class="textos">
          <tr>
            <td width="273" height="26" valign="top" class="textos"><p><strong>INSTRUCCIONS</strong></p></td>
          </tr>
          <tr>
            <td height="62" class="textos"><p>Descarregui les instruccions sobre l'&uacute;s del Portal:</p></td>
          </tr>
          <tr>
            <td class="textos"><p><a href="docs/Portal_Caixa_Requisits.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif" width="11" height="12" border="0"> Requisits t&egrave;cnics </a></p></td>
          </tr>
          <tr>
            <td class="textos"><p><a href="docs/Portal_Caixa_Manteniment_Dades.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif" width="11" height="12" border="0"> Manteniment de dades</a></p></td>
          </tr>
          <tr>
            <td class="textos"><p><a href="docs/Portal_Caixa_Guia_ajuda_Proveidor.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif" width="11" height="12" border="0"> Guia d'ajuda al proveïdor </a></p></td>
          </tr>
          <tr>
            <td height="36">&nbsp;</td>
          </tr>
          <tr>
            <td height="110" align="center"><img src="../images/img_int.jpg"></td>
          </tr>
          <tr>
            <td align="center"><table width="100" border="0" align="center" class="textos">
              <tr>
                <td align="center"><a href="mailto:portal.proveedores@caixabank.com"> portal.proveedores@caixabank.com</a><br><br>Tel. 902 053 218 </td>
              </tr>
            </table></td>
          </tr>
      </table>
    </td>
  </tr>

</table>
</body>
</html>
