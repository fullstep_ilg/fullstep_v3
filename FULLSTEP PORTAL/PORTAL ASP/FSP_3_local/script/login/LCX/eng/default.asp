﻿<%@ Language=VBScript %>
<!--#include file="../../login.asp"-->
<%		
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if

if Not IdOrden = "" then
    if Not IsNumeric(IdOrden) then
        IdOrden=""
    end if
end if

if Not CiaComp = "" then
    if Not IsNumeric(CiaComp) then
        CiaComp=""
    end if
end if

If Idioma="" then
	Idioma="GER"
end if

%>


<html>
<head>
<title>:: Portal de Proveedores ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />  
<meta name="viewport" content="width=device-width, initial-scale=1" />  

<link rel="canonical" href="/" />

<link href="../css/reset.css" rel="stylesheet" type="text/css" />        
<link href="../css/font-awesome.min.css" rel="stylesheet" />  
        
<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
<link href="../css/style.css" rel="stylesheet" />  
<link rel="stylesheet" type="text/css" href="../css/responsive.css">   
        
<!--[if lt IE 9]>
          	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script language="javascript">
<!--

function ventanaLogin (IDI){

   window.open ("registro.asp?Idioma="+IDI,"","width=700,height=540,resizable=yes")

}
function ventanaSecundaria (URL){

   window.open(URL,"ventana1","width=700,height=420,scrollbars=no")

}
function ventanaAyuda (URL){

   window.open(URL,"ayuda","width=700,height=600,scrollbars=yes")

}

function popup (URL) {
 window.open("popup.htm","","width=700,height=400,scrollbars=NO")
 }
function recuerdePWD()
{
   window.open("recuerdo.asp","_blank","width=818,height=481,scrollbars=NO")
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
//-->
</script>
<script>
$(document).ready(function(){
	//---------------------
	
	$("#txtCIA").val("Company Code");
  	$("#txtCIA").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Company Code"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Company Code");
  			$(this).removeClass('focus');
  		}
  	});	
	//---------------------
  	$("#txtUSU").val("User");
  	$("#txtUSU").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="User"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("User");
  			$(this).removeClass('focus');
  		}
  	});
	//---------------------
  	$("#txtPWD").val("Password");
	$("#txtPWD").focus(function(){
/*  	$("#txtPWD").bind('focus',function(){*/
  		$(this).addClass('focus');
  		if($(this).val()=="Password"){
  			$(this).val('');
			/*$(this).removeAttr("type");
			$(this).prop('type', 'password');*/
  		}
	}).blur(function(){
/*  	}).bind('blur',function(){*/
  		if($(this).val()==""){
  			$(this).val("Password");
			/*$(this).removeAttr("type");
			$(this).prop('type', 'text');
  			$(this).removeClass('focus');*/
  		}
  	});
});
</script>
</head>
<body>
 <header id="heading"> 
        
        	<div class="container"> 
                       
             	<div class="logotipo">
            		<a href="https://www.caixabank.com/index_es.html" title="CaixaBank" target="_blank"><img src="../img/caixabank.png" alt="CaixaBank" width="202" height="71" /></a>
                </div>  

                <nav>                      
                    <ul class="idiomas">
                        <li><a href="../default.asp" title="CAST"><strong>CAST</strong> | </a></li> 
                        <li><a href="../ger/default.asp" title="CAT"><strong>CAT</strong> | </a></li>
                        <li><a href="#" title="ENG"><strong>ENG</strong></a> </li>
                    </ul> 
                </nav>  
            
            	<h1>SUPPLIER PORTAL</h1>      
                     
    		</div>
        
        
            <div class="header">
            
                <div class="menu">  
        
                    <a class="menu-rsp" href="#" title="Men&uacute;"><span>Men&uacute;</span></a>
        
                    <div class="menu-container">
                    
                        <ul>
                            <li class="menu-padre activo"><a href="index.html" title="Home">Home</a> </li>   <span class="oculta">|</span>
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/servicios-del-portal.asp" title="Portal services">Portal services</a></li>  <span class="oculta">|</span> 
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/como-ser-proveedor.asp" title="How to become a supplier">How to become a supplier</a></li> <span class="oculta">|</span>  
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/responsabilidad-corporativa.html" title="Corporate responsability">Corporate responsability</a></li> <span class="oculta">|</span> 
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/excelencia-servicio.html" title="Service Excellence">Service Excellence</a></li> <span class="oculta">|</span>  
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/actualidad.html" title="Present">Present</a></li>
                            
                        </ul>
                                        
                        <ol>
                            <li class="activo"><a href="#" title="CAST">Castellano</a></li> 
                            <li><a href="#" title="CAT">Català</a></li>
                            <li><a href="#" title="ENG">English</a> </li>
                        </ol> 
                    
                   	</div>    
            
                </div> 
                
            </div>
            
        </header>        
    			
        
        
    	<section id="home"> 
            
            <div class="container">
            
           		<div class="section-acceso-eng">
                
                	<div class="acceso">
                    
                    	<h2>SUPPLIER ACCESS</h2>
                        
                        <div>
                        <!-- Formulario diseño Original
                            <form>
                                <label>
                                    <input type="text" placeholder="Código de Compañía" />
                                </label>
                                <label>
                                    <input type="text" placeholder="Código de Usuario" />
                                </label>
                                <label>
                                    <input type="password" placeholder="Contraseña" />
                                </label>
                                <button class="btn btn-left" type="submit">Entrar</button> <span class="acceso-recordar"><a href="#">¿Olvidó su clave de acceso?</a></span>
                            </form>
                            -->
                            <form action="default.asp" method="post" name="frmLogin"  id="frmLogin">
        <input type="text" class="text" id="txtCIA" name="txtCIA" maxlength="20" />
        <input type="text" class="text" id="txtUSU" name="txtUSU" maxlength="20" />
        <input type="password" class="text" id="txtPWD" name="txtPWD" maxlength="20" />
        <div id="bt_acceso">
          <input class="bt" name="cmdEntrar" type="submit" value="LOGIN" />
        </div>
        <input type="hidden" id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
        <input type="hidden" id="CiaComp" name="CiaComp" value="<%=CiaComp%>">
      </form>
      <div class="clearfix"></div>
      <div id="claves"><a href="javascript:void(null)" onClick="recuerdePWD()" >Forgot your access codes?</a></div>
     <!-- <div class="clearfix"></div>-->
                            <h4>REGISTER</h4>
                            
                            <p><a class="btn" href="registro.asp?Idioma=ENG">Register</a></p>
                            
                   		</div>
               
               		</div>
                    
                    <div class="soporte">
                    
                    	<div>
                        	<h2>SUPPLIER SUPPORT</h2>
	                        <h3>Tel. +34 902 053 218</h3>
    	                    <a href="mailto:portal.proveedores@caixabank.com">portal.proveedores@caixabank.com</a>
        				</div>                
                        <div>
                        	<p><strong>HELP DESK</strong></p>
                            <p>Monday to Thursday: 8:00 - 21:00h<br />Friday: 8:00 - 19:00h</p>
                        </div>
                    
                    </div>
                    
         		</div>
         	
            </div>
              
    	</section>
       
        
        <footer id="footer">
        
        	<div class="container">            
                    
            	<div class="footer-menu">
                
                    <ul>
                    	<li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/faq.html">Frequently asked questions</a></li>
                    	<li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/aviso-legal.html">Legal notice</a></li>
                   <!-- 	<li><a href="#">Información Legal</a></li>-->
                    	<li><a href="<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=ENG" align="right">Cookies Policy</a></li>
                    </ul>
                    
                     <p>Website optimized for 1280 x 1024. Supported browsers: Internet Explorer and Mozilla Firefox<br />Caixabank, S.A. ©  All rights reserved.</p>
                    
                   <a href="http://www.fullstep.com" title="Fullstep" target="_blank"><img class="fullstep" src="../img/fullstep.png" /></a>
                    
              	</div>
            
           		<div class="text">                                 
                    <p>Caixabank, S.A. Av. Diagonal, 621 08028-Barcelona NIF A08663619. Inscrita en el Registro Administrativo Especial del Banco de España con el número 2100 e inscrita
en el Registro Mercantil de Barcelona, tomo 42.657, folio 33, hoja B-41232, inscripción 109ª y con número de identificación fiscal A-08663619.</p>    
               	</div>
               
            </div>
           
        </footer>
                       
                       
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/main.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>       

</body>

</html>
