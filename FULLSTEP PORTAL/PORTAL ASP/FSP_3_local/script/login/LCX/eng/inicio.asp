﻿<%@ Language=VBScript %>
<!--#include file="../../../common/acceso.asp"-->
<script SRC="../../../common/menu.asp"></script>

<html>

<head>
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>:: Supplier Portal ::</title>

<style type="text/css">
<!--
body {
	margin-top: 0px;
	margin-left: 0px;
}
-->
</style>
<link href="../estilos.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
    function Init() {
        document.getElementById('tablemenu').style.display = 'block';

        p = window.top.document.getElementById("frSet")
        vRows = p.rows
        vArrRows = vRows.split(",")
        vRows = vArrRows[0] + ",*,0,0"
        p.rows = vRows
    }
</script>
</head>
<body scroll="yes" onLoad="Init()">
<script>
dibujaMenu(1)
</script>
<script language="javascript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}

</script>

<!-------------------------- BEGIN COPYING THE HTML HERE ---------------------------->

<table width="100%" border="0" cellpadding="5" cellspacing="0" bordercolor="0" hspace="0" vspace="0">
  <tr> 
    <td width="1%" rowspan="2" align="right" valign="top" bordercolor="0"> <div align="left" class="textos"></div>
    </td>
    <td height="57" colspan="3" align="left" valign="middle" class="titulo3P"><font size="2" face="verdana" class="titulo1P"><b> Welcome to the Suppliers Private area</b></font></td>
  </tr>
  <tr>
    <td width="60%" valign="top" class="textos"><table width="100%" border="0" align="center" cellpadding="1" cellspacing="0" bgcolor="#eeeeee" class="textos">
  <tr>
    <td><p>To navigate through the different areas of the portal, use the navigation bar in the topside of the page. From here you will be able to access:</p>
      <ul><li><p><strong>Quality</strong> : in this section you can answer and update the information requested by Caixabank.</p>
	   	</li>
		<li>
	   	  <p><strong>Contracts</strong>: you can access the contracts published by CaixaBank.</p>
	   	</li>
		<li>
		  <p><strong>Your details /your company</strong>: You can manage your company details and the trade areas in respect of which your company is compliant. </p>
		</li>
     	 </ul>
      <p><br>
        <br>
      </p>
      </td>
  </tr>
</table>
    </td>
    <td width="39%" colspan="2" align="right" valign="top">
      <table width="100%" border="0" align="right" class="textos">
          <tr>
            <td width="273" height="26" valign="top" class="textos"><p><strong>TUTORIALS</strong></p></td>
          </tr>
          <tr>
            <td height="62" class="textos"><p>You can download the following tutorials:</p></td>
          </tr>
          <tr>
            <td class="textos"><p><a href="docs/Portal_Caixa_Requisitos_Tecnicos.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif" width="11" height="12" border="0">Technical requirements</a> (in Spanish)</p></td>
          </tr>
          <tr>
            <td class="textos"><p><a href="docs/Portal_Caixa_Mantenimiento_Datos.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif" width="11" height="12" border="0"> Master data</a> (in Spanish)</p></td>
          </tr>
            <tr>
            <td class="textos"><p><a href="docs/Portal_Caixa_Supplier_Help_Guide.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif" width="11" height="12" border="0"> Suppliers Guide</a> </p></td>
          </tr>
          <tr>
            <td height="36">&nbsp;</td>
          </tr>
          <tr>
            <td height="110" align="center"><img src="../images/img_int.jpg"></td>
          </tr>
          <tr>
            <td align="center"><table width="100" border="0" align="center" class="textos">
              <tr>
                <td align="center"><a href="mailto:portal.proveedores@caixabank.com"> portal.proveedores@caixabank.com</a><br><br>Tel. 902 053 218</td>
              </tr>
            </table></td>
          </tr>
      </table>
    </td>
  </tr>

</table>
</body>
</html>
