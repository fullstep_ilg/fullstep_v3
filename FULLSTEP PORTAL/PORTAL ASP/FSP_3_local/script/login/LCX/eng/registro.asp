﻿<%@ Language=VBScript %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/fsal_1.asp"-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../css/reset.css" rel="stylesheet" type="text/css" />        
<link href="../css/font-awesome.min.css" rel="stylesheet" />  
        
<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
<link href="../css/style.css" rel="stylesheet" />  
<link rel="stylesheet" type="text/css" href="../css/responsive.css"/>   
<script src="../js/jquery-1.9.0.js"></script>
<script src="../js/jquery-1.9.0.min.js"></script>
<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
<script src="../../../common/formatos.js"></script>
<script language="JavaScript" src="../../../common/ajax.js"></script>
<!--#include file="../../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=ENG", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body onload="init()">

<header id="heading"> 
        
        	<div class="container"> 
                       
             	<div class="logotipo">
            		<a href="https://www.caixabank.com/index_es.html" title="CaixaBank"><img src="../img/caixabank.png" alt="CaixaBank" width="202" height="71" /></a>
                </div>  

                <nav>                      
                    <ul class="idiomas">
                        <li><a href="../registro.asp" title="CAST"><strong>CAST</strong> | </a></li> 
                        <li><a href="../ger/registro.asp" title="CAT"><strong>CAT</strong> | </a></li>
                        <li><a href="#" title="ENG"><strong>ENG</strong></a> </li>
                    </ul> 
                </nav>  
            
            	<h1>SUPPLIER PORTAL</h1>      
                     
    		</div>
        
        
            <div class="header">
            
                <div class="menu">  
        
                    <a class="menu-rsp" href="#" title="Men&uacute;"><span>Men&uacute;</span></a>
        
                    <div class="menu-container">
                    
                         <ul>
                            <li class="menu-padre"><a href="default.asp" title="Home">Home</a> </li>   <span class="oculta">|</span>
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/servicios-del-portal.asp" title="Portal services">Portal services</a></li>  <span class="oculta">|</span> 
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/como-ser-proveedor.asp" title="How to become a supplier">How to become a supplier</a></li> <span class="oculta">|</span>  
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/responsabilidad-corporativa.html" title="Corporate responsability">Corporate responsability</a></li> <span class="oculta">|</span> 
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/excelencia-servicio.html" title="Service Excellence">Service Excellence</a></li> <span class="oculta">|</span>  
                            <li class="menu-padre"><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/actualidad.html" title="Present">Present</a></li>
                            
                        </ul>
                                        
                        <ol>
                            <li class="activo"><a href="#" title="CAST">Castellano</a></li> 
                            <li><a href="#" title="CAT">Catalán</a></li>
                            <li><a href="#" title="ENG">English</a> </li>
                        </ol> 
                    
                   	</div>    
            
                </div> 
                
            </div>
            
        </header>           
    			
        
        
    	<section id="servicios"> 
            
            <div class="container">
            
           		<div class="text">
                
                	
                    <div class="columna sol">
                        <div class="imagen">
                      <img src="../img/shim.gif" width="100" >
                         </div>  
                        
                   	</div>
                    
                    <div class="centro sin solicitud">
                      <div>
                        
                        <h2><em>REGISTER</em></h2>
        <div class="int">
        	<p>To proceed with the registration proccess, the following contract must be read and accepted:</p>
            <div class="caja_registro">
                <strong class=" blue">IMPORTANT. PLEASE READ CAREFULLY<br>

Supplier registration<br>

Legal notice<br>
<br>
Access to the Portal</strong><br>

The registry as Supplier in the Suppliers’ Portal of the Group “la Caixa” is subjected to prior reading and acceptance of the following General Terms and Conditions. No Supplier may register without express acceptance of same. If the Supplier enters and uses the Portal, it will be understood that he expressly agrees, fully and without reservations, with all the contents of the General Terms and Conditions in force.</p>
<p>The access and, if applicable, registry in this Portal does not grant the Supplier the right to require being granted orders, which shall only be deemed to be made when it is so decided and formalised through the signature of the corresponding contractual instrument.</p>
<p> <strong class=" blue">GENERAL TERMS AND CONDITIONS</strong><br />
 <strong> 1. Definitions</strong><br />
  <strong>Group &quot;la Caixa&quot;</strong> (hereinafter, “la Caixa”): Caixabank, S.A. is the entity through which the Caixa d’Estalvis i Pensions de Barcelona, controlling company of the Group “la Caixa”, indirectly exercises its activity. For the purposes of these General Terms and Conditions, the term Group “la Caixa” (“la Caixa”) shall mean the group of companies comprising the Caixa d’Estalvis i Pensions de Barcelona and its affiliated companies, and the term “affiliated companies” shall mean each and all of the companies integrating the group. A group of companies shall be deemed to exist in the cases legally set forth for this purpose.</p>
<p><strong>Supplier</strong>   (hereinafter, the “Supplier” or the “Suppliers”): company or professional who registers in the Portal in order to offer its Products and/or Services to “la Caixa” and with which “la Caixa”, if applicable, shall contract the said Product or Service through the formalisation of the corresponding contractual instrument.</p>
<p><strong>“La Caixa" Supplier Portal  </strong> (hereinafter, the “Portal”): platform which “la Caixa” makes available to the Suppliers who wish to offer their Products and/or Services. The Portal, once the Supplier is registered and in case it is contracted by “la Caixa” through the formalisation of the corresponding contractual instrument, shall also be a tool for communication between “la Caixa” and the Supplier for the purposes of sending and/or receiving any information and documents relating to the contractual relationship. Likewise, “la Caixa may use the Portal to send to the Supplier the information which it deems to be interesting for the Supplier.</p>
<p><strong>Registry:</strong>  operation through which the Supplier registers in the Portal. In this operation, the Supplier must introduce 3 keys which he will use for his identification in subsequent accesses to the Portal.</p>
<p><strong>Registered User:</strong> Supplier registered in the Portal according to the procedure indicated in the definition of Registry. As a registered user, he shall be able to authorise new users.</p>
<p><strong>User or Users:</strong> natural person authorised by the Registered User to use and operate the Suppliers’ Portal in the name of the Registered User.</p>
<p><strong>Main User:</strong> natural person authorised by the Supplier effectively contracted by “la Caixa”, who is designated as such in the instrument of the agreement made between the Supplier and “la Caixa” for the supply of the service and/or product. The change of the natural person having been so authorised in the corresponding contractual instrument must be notified to “la Caixa” by the Supplier, and by a person with sufficient powers to do so, through a means which allows to provide proof of its receipt.</p>
<p><strong>Access keys:</strong> codes for identification (Supplier code, username and password) which are personal and untransferable and must be introduced by any user in order to access the Portal.</p>
<p><strong>2. Subject-matter and scope of application</strong><br />
  The subject-matter of these General Terms and Conditions is the regulation of the terms and conditions under which the Suppliers of Products and/or Services, after registering in the Suppliers’ Portal of “la Caixa”, shall provide the details and/or documents needed so that “la Caixa” may study and evaluate its interest in the Product and/or Service being offered, as well as the principles which shall govern the eventual negotiation and performance of the corresponding contractual instruments in order to contract, acquire, implement, develop and/or manage the Product and/or Service, if applicable.
</p>
<p>These General Terms and Conditions shall also regulate the terms and conditions under which “la Caixa” and the Supplier shall communicate with each other through the Portal whenever an agreement has been made between them through the formalisation of the corresponding contractual instrument.</p>
<p><strong>3. Obligations of the Supplier</strong><br />
 The Supplier shall be obliged:</p>
<p>a) Regarding the Registry:</p>
<p class="sang"> a.1. To duly fill in the Registry application.</p>
<p class="sang"> a.2. Not to use false identities or impersonate another person’s identity.</p>
<p>b) Regarding the use of the Portal</p>
<p class="sang"> b.1. To diligently guard and make any user guard the Portal access keys, assuming the responsibility for the damages which may arise from misuse of same.</p>
<p class="sang"> b.2. Not to use passwords or access keys from third parties.</p>
<p class="sang"> b.3. To make correct use of the Portal in accordance with these General Terms and Conditions. In this regard, the Supplier shall refrain from using the Portal for illicit or forbidden purposes or for purposes other than those set forth in these General Terms and Conditions or which may damage the rights and interests of “la Caixa” and/or third parties. In particular, and by way of example, the Supplier undertakes not to transfer, disclose or provide the Portal and/or third parties with information, data, contents, graphics, sound and/or image files, photographs, recordings, software and, in general, any kind of material which:</p>


        (i)  Is against the fundamental rights and civil liberties recognised by the Constitution, international treaties and applicable laws; including but not limited to violations of the right to honour, privacy and one’s own image;<br><br>

        (ii)   Induces, incites or promotes criminal activities or activities against the law, against the generally accepted morality and decency or against public order;<br><br>

        (iii) Is false, inaccurate or misleading or constitutes illicit, misleading or unfair advertising;<br><br>

        (iv) Is protected by intellectual or industrial property rights belonging to third parties, without authorisation of same;<br><br>

        (v)Breaches the Personal Data Protection regulations or regulations relating to secrecy of communications;<br><br>
        (vi) Constitutes unfair competition and/or damages the corporate image of “la Caixa” or third parties;<br><br>

        (vii)  Is affected by viruses or similar elements which may damage or prevent the appropriate operation of the Portal, of the computer equipments or their files and documents.<br><br>

    b.4. To act within the limits of the representative powers granted by the Supplier.<br><br>

    b.5. To refrain from any manipulation of the Portal technology.<br><br>

    b.6. To refrain from using trademarks, logos, trade names, internet domain names or distinctive signs appearing in the Portal, since they are the property of “la Caixa” or of third parties and shall not, under any circumstances, be understood to have been assigned to the Supplier.<br><br>

 

c)  Regarding the data and documents provided<br><br>

    c.1. To indicate only those activities relating to products and/or services which, at the moment of acceptance of these General Terms and Conditions, are marketed, manufactured or distributed by the Supplier.
<br><br>
    c.2. To provide any data and documents which may be necessary for the appropriate operation and management of the system, and to keep them up-to-date, communicating as soon as possible any change of same, both at the moment of registry in the Portal and, if applicable, after being contracted by “la Caixa”.<br><br>

   c.3. To guarantee at all moments the authenticity and veracity of the said data and documents, updating them so that they reflect, at all moments, the real situation of the Supplier, which shall be solely responsible for the damages caused to “la Caixa” and/or to third parties as a consequence of declarations which are inaccurate, false or not up-to-date.<br><br>

   c.4. Keep under strict confidentiality all the data and information generated within the relations between the Supplier and “la Caixa”, as well as observing the provisions of Organic Law 15/1999, of 13 December, on Personal Data Protection. For these purposes, the Supplier shall hold “la Caixa” harmless and entitle the latter to claim to the former the amount of all kind of compensations, sanctions and expenses arising from claims from affected persons or initiated on the initiative of the competent authority, due to negligence, lack of confidentiality, misuse and/or undue processing or communication of personal data, or to any other breach of the data protection regulations, provided they are attributable to the Supplier.<br><br>

    c.5. The said duties of secrecy and non-disclosure shall remain in force even after the Supplier has unregistered from the Portal.<br><br>

   c.6. The Supplier declares that he knows and accepts that the data and documents provided to the Portal shall be available to the Group “la Caixa” as defined in the corresponding Definitions section.<br><br>

d) Regarding the ethical, social and environmental standards guiding the responsible activity of “la Caixa”, the Supplier declares that he understands and accepts the Ethical, Social and Environmental Standards, which form an integral part of these General Terms and Conditions.
<br><br>

 

<strong>4. Obligations of “la Caixa”</strong><br><br>

“La Caixa” shall be obliged:<br><br>

   a. To keep under strict confidentiality all information and/or, if applicable, documents provided by the Supplier through the Portal.<br><br>
   b. To grant the Supplier access to the Portal. However, “la Caixa” shall not assume any responsibility arising from: (i) unavailability of the system, network failure or technical problems which cause the Portal to stop working; (ii) the damages caused by the spread of computer viruses or other elements in the system; o iii) any action of the Supplier or any other user which is against the law, the uses or customs or which violates the obligations provided for in these General Terms and Conditions. “la Caixa” also reserves the right to determine and modify the conditions under which access is granted to the Portal. <br><br>
 c. To eliminate the Supplier details recorded in the Portal when the Supplier unregisters from it according to the terms provided for in clause 5 of these General Terms and Conditions.<br><br>

 

<strong>5. Unregistration from the Portal</strong><br><br>

In case that the Supplier wishes to unregister from the Portal, he must send an e-mail to the address <a href="mailto: portal.proveedores@caixabank.com">portal.proveedores@caixabank.com</a> specifying “Unregistration from the Portal” as Subject. <br><br>

Without prejudice to the foregoing, unregistration shall not be available as long as the Supplier has ongoing contractual relations with “la Caixa”, since the Portal is the tool which allows “la Caixa” to manage same.<br><br>

The lack of the necessary diligence or the breach of the obligations provided for in the general terms and conditions on the part of the Supplier shall entitle “la Caixa” to exclude him temporarily or permanently from the Portal. Likewise, in case that the Supplier had been contracted by “la Caixa”, the said lack of diligence or breach shall constitute sufficient cause for the termination of the corresponding contractual relation.
<br><br>
<strong>6. Communications</strong><br><br>

Communications between the parties shall take place through the Portal and also through any means accepted by the Law which can provide proof of receipt, including fax.<br><br>

Changes of address and faxes shall be notified in writing and shall not bear effect until expiration of two working days from the date of receipt. In any case, the new address and fax must be within Spanish territory.<br><br>

 

<strong>7. Other</strong><br><br>

"La Caixa” reserves the right to modify these General Terms and Conditions, in which case it shall be notified to the Supplier through the Portal. The new General Terms and Conditions shall be understood to be tacitly accepted. In case that the Supplier does not accept the new General Terms and Conditions, he must notify it through reliable means. Non-acceptance of the new General Terms and Conditions shall involve the interruption of all the Portal applications which involve the participation of the Supplier in new offers of products and/or services to “la Caixa”, the use of the Portal by that Supplier being thereafter limited to communication with “la Caixa” for the purposes of sending and/or receiving information and/or documents relating to the contractual relation which may exist.<br><br>

The relations between “la Caixa” and the supplier deriving from the use of the Portal shall be subjected to Spanish law and to the jurisdiction of the Courts and Tribunals of the city of Barcelona, and the Supplier expressly waives its own jurisdiction and address if different from these.<br><br>

		  </div>

            <p class="imprimir"><a href="registro_texto.html" class="" target="blank"> Print</a> </p>
<div class="caja_acepto">
<form name="frmAlta" id="frmAlta" method="post" style="max-width:740px;">
  <span id="sprycheckbox1">

<span class="checkboxRequiredMsg">Seleccione para continuar.</span>
</span>
<br>
<br>

  <input type="checkbox" name="opcion1" id="opcion1" required>

I have read and accept the General terms and conditions to access the Portal, as well as the  <a href="docs/Portal_Caixa_Standards.pdf" target="_blank"> ethical, social and environmental standards</a><br />


<br />
<button type="button" class="bt_registro" id="submit1" name="submit1">Next</button>
<br>
</form>


</div>
<div style="clear:both;"></div>
    	</div>    
<p>&nbsp;</p></div>
</div>
<script type="text/javascript">
// <!--
// var sprycheckbox1 = new Spry.Widget.ValidationCheckbox("sprycheckbox1");
// //-->
$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked')) {
        $('#frmAlta').submit();
        alta();
	}
	else
       alert("You must accept the conditions in order to register into the Portal");
    
});

</script>
                      	</div>
                    </div>
                    
                 
               
           	  </div>
                    
         	</div>
              
    	</section>
        
        
    	
 			
 <footer id="footer">
        
        	<div class="container">            
                    
            	<div class="footer-menu">
                
                    <ul>
                    	<li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/faq.html">Frequently asked questions</a></li>
                    	<li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/aviso-legal.html">Legal notice</a></li>
                   <!-- 	<li><a href="#">Información Legal</a></li>
                    	-->
                    </ul>
                    
                     <p>Website optimized for 1280 x 1024. Supported browsers: Internet Explorer and Mozilla Firefox<br />Caixabank, S.A. ©  All rights reserved.</p>
                    
                    <a href="http://www.fullstep.com" title="Fullstep" target="_blank"><img class="fullstep" src="../img/fullstep.png" /></a>
                    
              	</div>
            
           		<div class="text">                                 
                    <p>Caixabank, S.A. Av. Diagonal, 621 08028-Barcelona NIF A08663619. Inscrita en el Registro Administrativo Especial del Banco de España con el número 2100 e inscrita
en el Registro Mercantil de Barcelona, tomo 42.657, folio 33, hoja B-41232, inscripción 109ª y con número de identificación fiscal A-08663619.</p>    
               	</div>
               
            </div>
           
        </footer>
                       
                       
        </body>

<!--#include file="../../../common/fsal_2.asp"-->

</html>
