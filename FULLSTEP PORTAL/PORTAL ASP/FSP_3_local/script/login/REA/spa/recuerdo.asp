﻿<%@  language="VBScript" %>
<!--#include file="../../recuerdo.asp"-->
<html>
<head>
    <link href="../estilos.css" rel="stylesheet" type="text/css">
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style type="text/css">
<!--
body {
	margin-top: 0px;
	margin-left: 0px;
}
-->
</style>
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body>
    <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
    <input type="hidden" name="idioma" value="spa" />
    <table width="640" border="0" cellpadding="0" cellspacing="0" class="textos">
        <tr>
            <td width="3">&nbsp;
                
            </td>
            <td colspan="3" align="left">
                <a href="http://www.reale.es" target="_blank">
                    <img src="../images/logo_reale.gif"  border="0" />
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;
                
            </td>
        </tr>
        <tr>
            <td width="3">&nbsp;
                
            </td>
            <td width="11" align="left">
                <td width="620" align="left">
                    <b><u>Recordatorio de claves de acceso </u></b>
                    <br>
                    <br>
                    Si ha olvidado sus claves de acceso, introduza la dirección de e-mail registrada
                    en el portal, y a continuación recibirá un e-mail con sus claves.<br>
                    <br>
                    <table class="textos">
                        <tr>
                            <td>
                                Company code:
                            </td>
                            <td>
                                <input type="text" name="txtCia" id="txtCia" size="80" maxlength="100" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                User Code:
                            </td>
                            <td>
                                <input type="text" name="txtUsu" id="txtUsu" size="80" maxlength="100" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                E-mail address:
                            </td>
                            <td>
                                <input type="text" name="txtEmail" id="txtEmail" size="80" maxlength="100" />
                            </td>
                        </tr>
                    </table>
                    <br />
                    <input type="submit" value="Enviar" name="cmdEnviar" id="cmdEnviar">
                    <br>
                    <br>
                    <br>
                    Si continúa con dificultades para poder acceder al portal o su dirección de email
                    ha cambiado, consulte con el servicio de atención a proveedores Tel. 902 611 022.<br>
                    <br>
                    <br>
                    <td width="15" align="left">
                </td>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
