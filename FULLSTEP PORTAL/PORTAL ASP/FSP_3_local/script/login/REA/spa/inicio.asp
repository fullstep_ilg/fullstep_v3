﻿<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<link href="../estilos.css" rel="stylesheet" type="text/css">
<title>::Fullstep Networks - Sistemas y Gestión de Compras::</title>
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
}
-->
</style>
</head>
<script>
dibujaMenu(1)
</script>
<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function Init() {
    document.getElementById('tablemenu').style.display = 'block';

	MM_preloadImages('../images/icono_docs_sm.gif','../images/icono_docs_sm.gif')
	p = window.top.document.getElementById("frSet")
	vRows = p.rows
	vArrRows = vRows.split(",")
	vRows = vArrRows[0] + ",*,0,0"
	p.rows = vRows

}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">

<!-------------------------- BEGIN COPYING THE HTML HERE ---------------------------->

<table width="100%" border="0" cellpadding="5" cellspacing="0" bordercolor="0" hspace="0" vspace="0">
  <tr> 
	<td width="1%" rowspan="2" align="right" valign="top" bordercolor="0"> <div align="left" class="textos"></div>
	</td>
	<td height="57" colspan="3" align="left" valign="middle" ><font size="2" face="verdana" class="titulo1P">Bienvenido a la zona privada de proveedores</font></td>
  </tr>
  <tr>
	<td width="60%" valign="top" bgcolor="#eeeeee" class="textos"><table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" bgcolor="#eeeeee" class="textos">
  <tr>
	<td class="textos"> 
	  Aquí encontrará la información que Reale solicita para establecerse como proveedor homologado, así como las solicitudes de oferta que Reale tiene para su compañía. 
			<BR>
			<BR>
		  Puede acceder a las distintas áreas a través de las opciones de menú situadas en la parte superior.
	  </p>
	  <ul>
		<li>
		<strong>Calidad</strong>: Puede cumplimentar los formularios activos, y desde los mismos adjuntar las certificaciones que sean solicitadas para la homologación como proveedor. </li>
	  
		<li>
		  <strong>Solicitudes de oferta</strong>: Puede acceder a los procesos de compra abiertos por Reale, a los que su empresa ha sido invitada.<br>
		</li>
		<li>
		  <strong>Sus datos/ su compañía</strong>: 
Puede gestionar los datos de su empresa, usuarios, así como las áreas de actividad en las que su empresa se encuentra homologada.	</li>
		 </ul>
	  Si es la primera vez que va a realizar una oferta a través del portal, siga atentamente los siguientes pasos:
	  <BR>
	  <BR>1. Pulse en &quot;solicitudes&quot; para ver las peticiones de oferta que tiene abiertas su empresa.
	  <BR><BR>2. Seleccione la solicitud de oferta a la que quiera responder, pulsando sobre el código de la misma. 
	  <BR><BR>3. Realice su oferta completando toda la información necesaria: desde el árbol de navegación que encontrará en la parte izquierda, podrá desplazarse por los distintos apartados que conforman la oferta. Para introducir los precios deberá ir al apartado &quot;items/precios&quot;. Recuerde introducir el plazo de validez de la oferta, en el apartado &quot;Datos Generales de la oferta&quot;. 
	  <BR><BR>4. Comunique su oferta pulsando sobre el botón de enviar <img src="../images/sobre_tr.gif" width="30" height="14"><br><br>
	  </td>
  </tr>
</table>
	</td>
	<td width="39%" colspan="2" align="right" valign="top">
	  <table width="100%" border="0" align="right" class="textos">
		  <tr>
			<td height="26" valign="top" class="textos"><p><strong>INSTRUCCIONES</strong></p></td>
		  </tr>
		  <tr>
			<td height="62" class="textos"><p>Descárguese las instrucciones sobre cómo enviar una oferta, actualizar sus datos,  etc. </p></td>
		  </tr>
		  <tr>
			<td width="273" class="textos"><p><a href="docs/FSN_MAN_ATC_Cómo ofertar.pdf" class="textos" target="_blank"><img src="../images/flecha.gif" width="15" height="19" border="0"> Cómo ofertar</a></p></td>
		  </tr>
		  <tr>
			<td class="textos"><p><a href="docs/FSN_MAN_ATC_Requisitos técnicos.pdf" class="textos" target="_blank"><img src="../images/flecha.gif" width="15" height="19" border="0"> Requisitos técnicos </a></p></td>
		  </tr>
		  <tr>
			<td class="textos"><p><a href="docs/FSN_MAN_ATC_Mantenimiento de datos.pdf" class="textos" target="_blank"><img src="../images/flecha.gif" width="15" height="19" border="0"> Mantenimiento de datos </a></p></td>
		  </tr>
		  <tr>
			<td height="36">&nbsp;</td>
		  </tr>
		  <tr>
			<td height="110" align="center"><img src="../images/img_int.png" width="300" height="200"></td>
		  </tr>
		  <tr>
			<td align="center"><table width="311" border="0" align="center" class="textos">
			  <tr>
				<td width="257" align="center">Tel. 902 611 022 - <a href="mailto:soporte.proveedores@reale.es">soporte.proveedores@reale.es</a></td>
			  </tr>
			</table></td>
		  </tr>
	  </table>
	</td>
  </tr>

</table>
</body>
</html>
