﻿<%@ Language=VBScript %>
<!--#include file="../../../common/acceso.asp"-->
<%
''' <summary>
''' Pantalla inicial tras hacer un login valido
''' </summary>
''' <remarks>Llamada desde: login\REA\inicio.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,false,false,0)


DIM FICHERO_SPA 
FICHERO_SPA= Application("PATHFICHERONOTICIAS") & ".SPA"
sNoticias = oRaiz.leerFicheroTexto(FICHERO_SPA)
set oRaiz = nothing

%>
<script SRC="../../../common/menu.asp"></script>

<html>

<head>
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Portal de Proveedores</title>

<style type="text/css">
<!--
body {
	margin-top: 0px;
	margin-left: 0px;
}
-->
</style>
<link href="../estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {font-size: 28px; font-style: normal; color: ##bd0000; text-decoration: none; font-family: Arial, Helvetica, sans-serif;}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
	/*''' <summary>
	''' Iniciar la pagina.
	''' </summary>     
	''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
	function Init() {
		document.getElementById('tablemenu').style.display = 'block';

		p = window.top.document.getElementById("frSet")
		vRows = p.rows
		vArrRows = vRows.split(",")
		vRows = vArrRows[0] + ",*,0,0"
		p.rows = vRows
	}
</script>
</head>
<body scroll="yes" onload="Init()">
<script>
dibujaMenu(1)
</script>
<script language="javascript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}

</script>

<!-------------------------- BEGIN COPYING THE HTML HERE ---------------------------->

<table width="100%" border="0" cellpadding="5" cellspacing="0" bordercolor="0" hspace="0" vspace="0">
  <tr> 
	<td width="1%" rowspan="2" align="right" valign="top" bordercolor="0"> <div align="left" class="textos"></div>
	</td>
	<td height="57" colspan="3" align="left" valign="middle" ><font size="2" face="verdana" class="titulo1P">Welcome to the Supplier Portal</font></td>
  </tr>
  <tr>
	<td width="60%" valign="top" bgcolor="#eeeeee" class="textos"><table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" bgcolor="#eeeeee" class="textos">
  <tr>
	<td class="textos"> 
	  To navigate through the different areas of the portal, use the navigation bar in the topside of the page. From here you will be able to access:
			
	  </p>
	  <ul>
		<li><b>Quality:</b> manage your quality certificates, non-conformities and access your quality scores.</li>
    <li><b>Requests:</b> access your requests (RFQ and other requests) and manage them from this section. </li>
    <li><b>Orders:</b> manage your orders, monitor their evolution and access your orders history.</li>
    <li><b>Your details/your company :</b> update the general details about your company, point out the materials and services your company can supply and register new users of your company inside the Portal.  </li>
		 </ul>
	  If this is the first time your are sending an offer through the portal, please follow these steps carefully:</p>
  <ol>
  <li><strong>Click on &quot;Requests for quotation&quot;</strong> to display the requests for quotations pending for your company. </li><br>
  <li><strong> Select the request for quotation</strong> for which you want to send an offer by clicking on its code</li><br>
  <li><strong> Configure your offers</strong> by filling in the appropriate data. From the navigation tree on the left side of the screen, you will be able to navigate through the different sections of the offer. To fill in the prices, click on the &quot;items/prices&quot; section. Don't forget to fill in the validity dates of your offer in the &quot;General data of the offer&quot; section.</li><br>
  <li> <strong>Post your offers </strong>by clicking on Send <IMG height=14 src="../images/sobre_tr.gif" width=30 align=absBottom></li>
 </ol>
	  
	  </td>
  </tr>
</table>
	</td>
	<td width="39%" colspan="2" align="right" valign="top">
	  <table width="100%" border="0" align="right" class="textos">
		  <tr>
			<td height="26" valign="top" class="textos"><p><strong>TUTORIALS</strong></p></td>
		  </tr>
		  <tr>
			<td height="62" class="textos"><p>Please download the instructions on how to make an offer, technical requirements, ..</p></td>
		  </tr>
		  <tr>
			<td width="273" class="textos"><p><a href="docs/FSN_MAN_ATC_How to offer.pdf" target="_blank"><img src="../images/flecha.gif" width="15" height="19" border="0"> How to offer</a></p></td>
		  </tr>
		  <tr>
			<td class="textos"><p><a href="docs/FSN_MAT_ATC_Technical Requirements.pdf" target="_blank"><img src="../images/flecha.gif" width="15" height="19" border="0"> Requisitos técnicos </a></p></td>
		  </tr>
		  <tr>
			<td class="textos"><p><a href="docs/FSN_MAT_ATC_Master data.pdf" target="_blank"><img src="../images/flecha.gif" width="15" height="19" border="0"> Mantenimiento de datos </a></p></td>
		  </tr>
		  <tr>
			<td height="36">&nbsp;</td>
		  </tr>
		  <tr>
			<td height="110" align="center"><img src="../images/img_int.png" width="300" height="200"></td>
		  </tr>
		  <tr>
			<td align="center"><table width="311" border="0" align="center" class="textos">
			  <tr>
				<td width="257" align="center">Tel. 902 611 022 - <a href="mailto:soporte.proveedores@reale.es">soporte.proveedores@reale.es</a></td>
			  </tr>
			</table></td>
		  </tr>
	  </table>
	</td>
  </tr>

</table>
</body>
</html>
