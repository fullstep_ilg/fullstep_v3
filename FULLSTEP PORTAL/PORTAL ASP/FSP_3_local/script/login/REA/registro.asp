<%@ Language=VBScript %>
<!--#include file="../../common/XSS.asp"-->

<html>


<head>

<title>Contrato Adhesi&oacute;n al portal de proveedores </title>

<link href="estilos.css" rel="stylesheet" type="text/css">
</head>


<script>
function no_alta()
{
window.close()

}
</script>

<script>

function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=SPA", "_blank")
window.close()

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=700,height=600,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
  </script>


<script>
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=550,height=600,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>


<body bgcolor="#FFFFFF" topmargin="5" leftmargin="5">
<form name="frmAlta" id="frmAlta" method="post">

<table border="0" width="100%">
  <tr>
    <td><a href="http://www.reale.es" target="_blank"><img src="images/logo_reale.gif" alt="Logo Reale" width="287" height="70" border="0"></a>
    </td>
  </tr>
  <tr>
    <td><font face="Verdana" size="1">
      <p align="left" class="textos"> <b>Para continuar con el proceso de alta es imprescindible que lea y acepte las condiciones de acceso al Portal</b> </p>
      <table border="0">
        <tr>
          <td valign="top"><p align="center"> </td>
          <td><font face="Verdana" size="1">
          <textarea readonly rows="11" name="S1" cols="80" style="font-family: Verdana; font-size: 8pt; text-align:justify; display:block; height:135px; line-height: 150%; list-style-type: lower-alpha">
Contrato de adhesi�n al portal de proveedores de Reale

IMPORTANTE LEA ATENTAMENTE

Alta de Proveedores y Acceso al Portal

El alta como proveedor en el Portal de Proveedores de Reale Seguros est� condicionado a la previa lectura y aceptaci�n del Aviso Legal y Condiciones Generales de Compra, cuya descarga se facilita en el Portal de Proveedores (http://compras.reale.es) con sus correspondientes links de descarga, as� como la lectura y aceptaci�n de las siguientes cl�usulas. Sin expresar su conformidad con las mismas no podr� registrarse. Siempre que se acceda y utilice el Portal, se entender� que est� de acuerdo, de forma expresa, plena y sin reservas, con la totalidad del contenido de este Aviso. Asimismo, al ser Ud. el Usuario Principal de su empresa, queda obligado al aceptar la conformidad de este Aviso, a velar por su cumplimiento por parte de todos los usuarios que se den de alta en su empresa, liberando a Reale Seguros de toda responsabilidad por los posibles perjuicios que dichos usuarios pudieran causar a su empresa o cualquier otra debido a sus actuaciones en el Portal.

Cl�usulas

1. Objeto del Portal de Proveedores de Reale Seguros

El Portal de Proveedores de Reale Seguros es el medio a trav�s del cual Reale Seguros se comunica con sus proveedores para solicitarles ofertas, documentos as� como aquella informaci�n comercial que estime oportuna. Al mismo tiempo Reale Seguros puede utilizar el portal para remitirle aquella informaci�n que considere de su inter�s.
Reale Seguros act�a tanto como comprador directo como de gestor de compras de sus clientes, y actuar� en nombre de ellos durante el proceso de compra.

El uso o acceso al Portal y/o a los Servicios no atribuyen al PROVEEDOR ning�n derecho sobre las marcas, nombres comerciales o signos distintivos que aparezcan en el mismo, siendo �stos propiedad de Reale Seguros o de terceros. En consecuencia, los Contenidos son propiedad intelectual de Reale Seguros o de terceros, sin que puedan entenderse cedidos al PROVEEDOR.


2. Objeto 

El presente acuerdo tiene por objeto regular las relaciones entre Reale Seguros y el PROVEEDOR, en todo lo relacionado con el uso del Portal.


3. Obligaciones del PROVEEDOR

Son obligaciones del proveedor las siguientes:

a. Proporcionar cuantos datos sean necesarios para el adecuado funcionamiento del sistema, as� como mantenerlos actualizados, comunicando a la mayor brevedad posible todo cambio que se produzca en los mismos.

b. Garantizar la autenticidad de los datos facilitados a consecuencia de la cumplimentaci�n de los formularios necesarios para la suscripci�n de los Servicios. Asimismo, el PROVEEDOR actualizar� la informaci�n facilitada para que refleje, en cada momento, su situaci�n real. Por todo ello, el PROVEEDOR ser� el �nico responsable de los da�os y perjuicios ocasionados a Reale Seguros como consecuencia de declaraciones inexactas o falsas.
c. Guardar absoluta confidencialidad en relaci�n con toda la informaci�n que se genere en las relaciones entre el PROVEEDOR y Reale Seguros.

d. Abstenerse de cualquier manipulaci�n en la utilizaci�n inform�tica del Portal, as� como el uso del mismo con fines distintos de aquellos para los que fue previsto. Asimismo, el PROVEEDOR se abstendr� de acceder a zonas no autorizadas del Portal.

e. Cumplir fielmente sus compromisos en la informaci�n remitida a trav�s del portal. En el caso de que el PROVEEDOR no demuestre la necesaria diligencia comercial, o incumpla las obligaciones contra�das, Reale Seguros se reserva el derecho de excluir temporal o permanentemente al PROVEEDOR del portal.

f. El PROVEEDOR deber� indicar solamente aquellos grupos de materiales, que se refieran a bienes o servicios que, en el momento de la aceptaci�n del acuerdo, el proveedor comercialice, fabrique o distribuya y que sean del inter�s comercial de Reale Seguros.

g. El PROVEEDOR acepta que las ofertas introducidas en el Portal sean consideradas con el mismo rango y validez que una oferta enviada por cualquier otro medio tradicional (carta, fax).
h. El PROVEEDOR se obliga a hacer un uso correcto del Portal y de los Servicios conforme a la ley, el presente Aviso Legal y dem�s reglamentos de uso e instrucciones puestos en su conocimiento, as� como con la moral y las buenas costumbres generalmente aceptadas y el orden p�blico.
Por ello, se abstendr� de utilizar el Portal o cualquiera de los Servicios con fines il�citos, prohibidos en el presente Aviso Legal, lesivos de los derechos e intereses de terceros. En particular, y a t�tulo meramente indicativo, el PROVEEDOR se compromete a no transmitir, difundir o poner a disposici�n de terceros informaciones, datos, contenidos, gr�ficos, archivos de sonido y/o imagen, fotograf�as, grabaciones, software y, en general, cualquier clase de material que:
1. Sea contrario a los derechos fundamentales y libertades p�blicas reconocidas en la Constituci�n, Tratados internacionales y leyes aplicables; 
2. Induzca, incite o promueva actuaciones delictivas, contrarias a la ley, a la moral y buenas costumbres generalmente aceptadas o al orden p�blico; 
3. Sea falso, inexacto o pueda inducir a error, o constituya publicidad il�cita, enga�osa o desleal;
4. Est� protegido por derechos de propiedad intelectual o industrial pertenecientes a terceros, sin autorizaci�n de los mismos;
5. Vulnere derechos al honor, a la intimidad o a la propia imagen;
6. Vulnere las normas sobre secreto de las comunicaciones;
7. Constituya competencia desleal, o perjudique la imagen empresarial de Reale Seguros o terceros;
8. Est� afectado por virus o elementos similares que puedan da�ar o impedir el funcionamiento adecuado del Portal, los equipos inform�ticos o sus archivos y documentos.

4. Derechos del PROVEEDOR

Son derechos del PROVEEDOR los siguientes:

1. Mantener una presencia constante en la base de datos de Reale Seguros, en su calidad de proveedor dado de alta. 

2. Recibir peticiones de ofertas en virtud de las normas establecidas. 


5. Obligaciones de Reale Seguros

Son obligaciones de Reale Seguros las siguientes:

a. Mantener la informaci�n que estime oportuna actualizada, no siendo responsable de los errores que se produzcan por fuerza mayor o caso fortuito.

b. Guardar absoluta confidencialidad de toda la informaci�n relativa al proveedor: bien la proporcionada por �ste, bien la generada en las relaciones entre el PROVEEDOR y Reale Seguros

c. Facilitar al proveedor, en cualquier momento, la situaci�n de sus datos en la base de datos, y no facilitarla a terceros si no es para permitir al proveedor la presentaci�n de sus ofertas y la toma en consideraci�n de sus productos como alternativa de suministro. Asimismo, facilitar al proveedor, en cualquier momento, la situaci�n de sus datos en la base de datos, para que �ste, previa notificaci�n por escrito, los modifique o elimine. 


6. Derechos de Reale Seguros

a. El Usuario presta su consentimiento para que Reale Seguros ceda sus datos a las empresas a ella asociadas o del grupo al que pertenece, as� como a otras con las que concluya acuerdos con la �nica finalidad de la mejor prestaci�n del servicio, respetando, en todo caso, la legislaci�n espa�ola sobre protecci�n de los datos de car�cter personal. Asimismo, el Usuario acepta que Reale Seguros o sus empresas a ella asociadas, sociedades filiales y participadas, le remitan informaci�n sobre cualesquiera bienes o servicios que comercialicen, directa o indirectamente, o que en el futuro puedan comercializar. La aceptaci�n del Usuario para que puedan ser cedidos sus datos en la forma establecida en este p�rrafo, tiene siempre car�cter revocable sin efectos retroactivos, conforme a lo que dispone la Ley Org�nica 15/1999, de 13 de diciembre.) 

b. Optar, en caso de incumplimiento de sus obligaciones por el PROVEEDOR, por exigir el debido cumplimiento de las mismas, o por la resoluci�n del contrato, en los t�rminos de la cl�usula 8 de este Aviso Legal. 

7. Limitaci�n de responsabilidad

a. Reale Seguros no asume ninguna responsabilidad derivada de la falta de disponibilidad del sistema, fallos de la red o t�cnicos que provoquen un corte o una interrupci�n en el Portal. Igualmente Reale Seguros no se hace responsable de los da�os y perjuicios que pueda ocasionar la propagaci�n de virus inform�ticos u otros elementos en el sistema.
b. Reale Seguros no asume ninguna responsabilidad sobre cualquier actuaci�n que sea contraria a las leyes, usos y costumbres, o bien que vulnere las obligaciones establecidas en el presente Aviso Legal, por parte de ninguna persona de Reale Seguros, del PROVEEDOR o cualquier usuario de la red. No obstante, cuando Reale Seguros tenga conocimiento de cualquier conducta a la que se refiere el p�rrafo anterior, adoptar� las medidas necesarias para resolver con la m�xima urgencia y diligencia los conflictos planteados. 
c. Reale Seguros se exime de cualquier responsabilidad derivada de la intromisi�n de terceros no autorizados en el conocimiento de las condiciones y circunstancias de uso que los PROVEEDORES hagan del Portal y de los Servicios.


8. Resoluci�n del acuerdo

En caso de incumplimiento de cualquiera de las obligaciones contenidas en el presente Aviso legal, el PROVEEDOR o Reale Seguros habr�n de notificar dicho incumplimiento a la parte que lo hubiera cometido, otorg�ndole un plazo de quince d�as h�biles, contando desde la notificaci�n, para que lo subsane. Transcurrido dicho plazo sin que se haya producido la subsanaci�n del incumplimiento, la otra parte podr� optar por el cumplimiento o la resoluci�n del presente acuerdo, con la indemnizaci�n de da�os y perjuicios en ambos casos, de conformidad con lo establecido en el art�culo 1.124 del C�digo Civil. En caso de optar por la resoluci�n, tanto el PROVEEDOR como Reale Seguros aceptan que la simple notificaci�n ser� suficiente, para que la misma tenga plenos efectos.


9. Duraci�n 

El presente acuerdo tendr� una duraci�n indefinida siempre y cuando no se produzca denuncia por escrito de cualquiera de las partes, con al menos 1 mes de antelaci�n.
El procedimiento de baja ser� enviando un mail con Asunto �Baja del Portal� a la direcci�n soporte.proveedores@reale.es En caso de no ser posible el env�o del email, se enviar� una solicitud por correo ordinario dirigido a Santa Engracia, 14-16 � 28010 Madrid, acompa�ando copia del D.N.I. indicando la referencia �protecci�n de datos�.

10. Notificaciones

a. Las notificaciones entre las partes podr�n hacerse por cualquier medio de los admitidos en Derecho, que permita tener constancia de la recepci�n, incluido el fax.

b. Los cambios de domicilios y faxes ser�n notificados por escrito y no producir�n efectos hasta transcurridos dos d�as h�biles desde su recepci�n. En todo caso, el nuevo domicilio y fax ser� necesariamente dentro del territorio espa�ol.

c. Las relaciones entre Reale Seguros y el PROVEEDOR derivadas del uso del Portal y de los servicios prestados a trav�s del mismo, se someter�n a la legislaci�n y jurisdicci�n espa�olas.



          </textarea>
</font> </td>
        </tr>
      </table>
      </font>
        <p class="textos"> <b>&iquest;Acepta el contrato de adhesi&oacute;n al portal de proveedores de Reale?<br>
                <br>
        </b> Si pulsa &quot;No Acepto&quot;, su empresa no podr&aacute; ser registrada como proveedor autorizado de Reale.        
        <p class="textos">          Para continuar el proceso de alta debe pulsar &quot;Acepto&quot;. <br>
          Si quiere imprimir el contrato puede hacerlo pulsando el bot&oacute;n &quot;Imprimir&quot;. <br>
          <br>
          
        <div align="right">
          <table width="39%" border="0" align="left">
            <tr>
              <td width="41%"><p align="center">
                  <input type="submit" onclick="return alta()" value="Acepto" id=submit1 name=submit1>
              </p></td>
              <td width="38%"><input type="button" onclick="no_alta()" value="No acepto" id=button1 name=button1>
              </td>
              <td width="21%"><input type="button" onclick="imprimir()" value="Imprimir" id=button2 name=button2>
              </td>
            </tr>
          </table>
        </div><br><br>
        <p align="left" class="textos">        
        <hr width="100%" size="1" color="#e3006a">
    </td>
  </tr>
</table>

	<input type="hidden" name="ACEPTADO">
	
</form>

</body>

</html>
