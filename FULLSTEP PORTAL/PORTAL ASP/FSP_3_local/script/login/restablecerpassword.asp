﻿<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader "Pragma", "no-cache" %>
<% Response.charset="UTF-8"%>
<% Response.Expires = -1 %> 
<!--#include file="../common/XSS.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<%
Idioma = Request.QueryString("Idioma")
Idioma = trim(Idioma)
if Idioma="" then
	Idioma = "SPA"
end if
'Idiomas
dim den
den = devolverTextos(Idioma,134)
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp"/>
</head>
<body style="text-align:left;">
    <form name="frmrestablecerpassword" id="frmrestablecerpassword" method="post">
        <img src="<%=application("RUTASEGURA")%>/custom/<%=application("NOMPORTAL")%>/images/logo.jpg" style="float:left; width:200px; height:54px;" border="0"/>
<%
dim IsSecure
IsSecure = left(Application ("RUTASEGURA"),8)
if IsSecure = "https://" then                       
    IsSecure = "; secure"
else
    IsSecure =""
end if

Response.AddHeader "Set-Cookie","USU_IDIOMA=" & Idioma  & "; path=/; HttpOnly" + IsSecure
''' <summary>
''' Si has olvidado el password puedes pedir q te lo recuerden por mail.
'''	Las personalizaciones llaman a esta, por lo q se controla el XSS sin tocar las personalizaciones.
''' </summary>
''' <remarks>Llamada desde: default.asp de cada personalización; Tiempo máximo: 0,1</remarks>
if Request.QueryString("crc").Count>0 then    
	set oRaiz = createobject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
	i = oraiz.Conectar(Application("INSTANCIA"))
	
	if i = 1 then
		set oRaiz = nothing
		Response.Redirect application("RUTASEGURA") & "script/imposibleconectar.asp?Idioma=" & Idioma
	else
		set TESError = oRaiz.ComprobarRecuperarPassword (Request.QueryString("crc"), Request.QueryString("Pyme"))
		
        select case TESError.NumError
            case 0
                set oRaiz = nothing
		        Response.Redirect application("RUTASEGURA") & "/script/login/inicio.asp?Idioma=" & Idioma & "&u=" & TESError.Arg1 & "&c=" & TESError.Arg2 & "&crc=" & Request.QueryString("crc")
            case 1 'Usuario no valido, no existe ese hash
            %>
                <div style="clear:both; float:left; border:solid 1px Red; margin-top:20px; padding:5px;">
                    <img alt="" src="../images/Icono_Error_Amarillo_40x40.gif" style="float:left; margin:1em;"/>
                    <p>
                        <%=JSText(Den(1))%>
                        <br />
                        <%=JSText(Den(2))%> <a style="text-decoration:underline" href="<%=application("RUTASEGURA")%>/script/login/<%=application("NOMPORTAL")%>"><%=JSText(Den(3))%></a>
                    </p>
                </div>
            <%case 3 'Hash caducado 
            %>
                <div style="clear:both; float:left; border:solid 1px Red; margin-top:20px; padding:5px;">
                    <img alt="" src="../images/Icono_Error_Amarillo_40x40.gif" style="float:left; margin:1em;"/>
                    <p>
                        <%=JSText(Den(4))%>
                        <br />
                        <%=JSText(Den(2))%> <a style="text-decoration:underline" href="<%=application("RUTASEGURA")%>/script/login/<%=application("NOMPORTAL")%>/<%=Idioma%>/recuerdo.asp" onclick="window.open(this.href,'_blank','width=641,height=300,scrollbars=NO');return false;"><%=JSText(Den(5))%></a>
                    </p>
                </div>
        <%end select
	end if		
end if
%>
    </form>
</body>
</html>