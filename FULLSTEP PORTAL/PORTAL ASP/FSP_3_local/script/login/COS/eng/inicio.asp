<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<link href="../estilos.css" rel="stylesheet" type="text/css">
<title>::Fullstep Networks - Sistemas y Gesti�n de Compras::</title>
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
}
-->
</style>
</head>
<script>
dibujaMenu(1)
</script>
<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo m�ximo:0</remarks>*/
function Init() {
    document.getElementById('tablemenu').style.display = 'block';

	MM_preloadImages('../images/icono_docs_sm.gif','../images/icono_docs_sm.gif')
	p = window.top.document.getElementById("frSet")
	vRows = p.rows
	vArrRows = vRows.split(",")
	vRows = vArrRows[0] + ",*,0,0"
	p.rows = vRows

}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">
<div id="inicio-left">
  <h1>Welcome to the Supplier Portal</h1>
  <p>To navigate through the different areas of the portal, use the navigation bar in the topside of the page. From here you will be able to access: </p>
  <ul>
    <li><b>Quality:</b> manage your quality certificates, non-conformities and access your quality scores.</li>
    <li><b>Requests:</b> access your requests (RFQ and other requests) and manage them from this section. </li>
    <li><b>Orders:</b> manage your orders, monitor their evolution and access your orders history.</li>
    <li><b>Your details/your company :</b> update the general details about your company, point out the materials and services your company can supply and register new users of your company inside the Portal.  </li>
  </ul>
  
<p> If this is the first time your are sending an offer through the portal, please follow these steps carefully:</p>
  <ol>
  <li><strong>Click on &quot;Requests for quotation&quot;</strong> to display the requests for quotations pending for your company. </li><br>
  <li><strong> Select the request for quotation</strong> for which you want to send an offer by clicking on its code</li><br>
  <li><strong> Configure your offers</strong> by filling in the appropriate data. From the navigation tree on the left side of the screen, you will be able to navigate through the different sections of the offer. To fill in the prices, click on the &quot;items/prices&quot; section. Don't forget to fill in the validity dates of your offer in the &quot;General data of the offer&quot; section.</li><br>
  <li> <strong>Post your offers </strong>by clicking on Send <IMG height=14 src="../images/sobre_tr.gif" width=30 align=absBottom></li>
 </ol>
</div>
<div id="inicio-right">
<h2>TUTORIALS</h2>
<p>Please download the instructions on how to make an offer, technical requirements, ..</p>
<ul>
<li><a href="docs/FSN_MAN_ATC_How to offer.pdf" target="_blank">How to offer</a></li>
<li><a href="docs/FSN_MAT_ATC_Technical Requirements.pdf" target="_blank">Technical Requirements</a></li>
<li><a href="docs/FSN_MAT_ATC_Master data.pdf" target="_blank">Master data</a></li>
</ul>
<div id="img-atc"><img src="../images/imagen-ATC.jpg"></div>
<p>If you have any doubt or difficulty operating through the Portal, please contact the <span>Supplier Help Desk<br>
 +34 902 996 926</span><br> <a href="mailto:atencionalcliente@fullstep.com"> atencionalcliente@fullstep.com </a></p>
</div>

</body>
</html>
