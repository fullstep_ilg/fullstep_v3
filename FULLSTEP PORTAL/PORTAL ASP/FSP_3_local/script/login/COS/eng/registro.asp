﻿<%@ Language=VBScript %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://fast.fonts.net/jsapi/f5dbd368-355c-4f28-bc2f-217b51498d80.js"></script>
<script src="http://code.jquery.com/jquery-1.9.0.js"></script>
<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=ENG", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body>

<div id="cont_gen">
	<div id="izd_gen"> <a href="http://www.cosentino.es" target="_blank"><img src="../images/logo.png" border="0"></a>
    </div>
    <div id="drc_gen">
    	<h1>REGISTRATION</h1>
        <div class="int">
        	<p> 
                To proceed with the registration proccess, the <span class="rojo">following contract must be read and accepted</span> :
               
            </p>
            <p style="height:150px; padding:0 5px; overflow:auto; background:#FFF; border:#CCC 1px solid">
               PORTAL TERMS AND CONDITIONS<br/><br/>
Supplier register<br/><br/>
Legal Notice - Portal access<br><br>
Registering in the Supplier Portal of GRUPO COSENTINO (from here on referred to as GRUPO COSENTINO) implies previous reading and acceptance of the following clauses. Without expressing full compliance with the clauses you will not be allowed to register. Every time you access and use the Portal, it is understood you agree, fully and unreservedly, with the contents of these Legal Notice. Also, being the main user of your company, you are obliged by accepting this Legal Notice to ensure compliance by all users registered in your company, fully exonerating GRUPO COSENTINO from any responsibility of the damage those users may cause to your company or any other because of their interactions with the Portal.<br>
<br><br>

<b>Clauses</b><br><br>

<b>1. Purpose of the GRUPO COSENTINO Supplier Portal</b><br><br>
The GRUPO COSENTINO Supplier Portal is the means through which GRUPO COSENTINO communicates with its suppliers to request offers, documents or any other commercial information it deems fit. At the same time GRUPO COSENTINO reserves the right to use the Portal to communicate any information it considers to be of its interest.<br><br>
GRUPO COSENTINO works both as a direct purchaser and as a purchasing broker for its clients, and shall act in their name during the purchasing process.<br><br>
Access or use of Portal and / or Services does not give the SUPPLIER any rights on trademarks, commercial denominations or any other distinctive sign appearing in the Portal, being GRUPO COSENTINO or third party’s property. Therefore, all contents are intellectual property of GRUPO COSENTINO or third party, and will not be understood as ceded to the SUPPLIER.<br>
  <br>
  
  <b>2. Purpose </b><br>
  <br>
The purpose of this contract is to regulate relations between GRUPO COSENTINO, GRUPO COSENTINO’s clients and the SUPPLIER, in all aspects concerning use of the PORTAL.<br>
<br>
  <br>
  
<b>3. SUPPLIER obligations </b><br>
  <br>
  The following are obligations of the SUPPLIER:<br>
  <br>
  a. Provide whatever data is necessary for the adequate functioning of the system, and maintain them updated, communicating at the earliest possible time any modifications.<br>
  <br>
b. Guaranteeing the authenticity of data provided as a consequence of forms necessary to subscribe to the Services offered by the PORTAL. Also, the SUPPLIER will update all information provided to ensure it reflects, at all times, its real situation. Therefore, the SUPPLIER is solely responsible of damages caused to GRUPO COSENTINO or GRUPO COSENTINO’s clients as a consequence of inexact or false statements.<br><br>
  c. Keeping absolute confidentiality in relation to all information derived from the relations between the SUPPLIER and GRUPO COSENTINO or GRUPO COSENTINO’s clients.<br>
  <br>
  d. Abstaining from doing any modification in the information-technology use of the Portal, as well as using it with purposes different from those for which it is intended. Also, the SUPPLIER will abstain from accessing unauthorized zones of the Portal.<br>
  <br>
 e. Comply loyally to the commitments expressed in the information sent through the Portal. If the SUPPLIER does not demonstrate due commercial diligence, or does not comply with contracted duties, GRUPO COSENTINO and its clients reserves the right to exclude temporarily or permanently the SUPPLIER from the Portal.<br>
  <br>
 f. The SUPPLIER will indicate only those material groups referring to goods or services that, at the time of acceptance of the contract, are commercialized, manufactured or distributed by the SUPPLIER and of commercial interest to GRUPO COSENTINO and its clients.  <br>
  <br>
  g. The SUPPLIER accepts that offers sent through the Portal are considered to be of the same rank and validity as offers sent through any other traditional means (letters, fax).<br><br>h. The SUPPLIER is obliged to use the Portal and its Services in compliance with the Law, this Legal Notice, and the rest of regulations and instructions of which it is informed, as well as in accordance to generally accepted morals and customs, and public order.<br><br>Thus, the SUPPLIER will abstain from using the Portal or any of its Services with illegal purposes, prohibited in this Legal Notice, and/or damaging to rights and interests of third parties.  <br><br>In particular, and merely as a matter of indication, the SUPPLIER agrees not to transmit, broadcast, or make available to others or third parties any information, data, contents, graphics, image and/or sound files, photographs, recordings, software and, in general, any type of material that:<br><br>1. Is contrary to fundamental rights and public liberties under the Constitution, International treaties and applicable laws;<br><br>2. Induces, incites or promotes criminal acts, contrary to law, moral and generally accepted customs or public order;
<br><br>3. Is false, inexact or may induce to error, or constitutes illegal, deceitful or disloyal publicity;<br><br>4. Is protected under intellectual or industrial property rights belonging to third parties without prior explicit and written consent.<br><br>5. Contravenes laws and statutes for civil protection against defamation and invasion of privacy.<br><br>6. Contravenes regulations about privacy and/or secrecy of communications.<br><br>7. Can be considered as unfair competition or damages in any way the image of GRUPO COSENTINO or third parties.<br><br>8. Is affected by viruses or similar elements that can harm or stop the Portal’s adequate performance, the electronic equipment or their files and documents.<br>
    
  <b>4. Rights of the SUPPLIER </b><br>
  <br>
  The following are rights of the SUPPLIER:<br>
  <br>
1. Maintain a constant presence in GRUPO COSENTINO’s database, as a registered supplier. <br>
<br>
2. Receive offers according to the established rules. <br>
<br>
  <br>
    
  <b>5. Obligations of GRUPO COSENTINO </b><br>
  <br>
 The following are GRUPO COSENTINO obligations:<br>
  <br>
  a. Maintain the information it deems fit constantly updated, without being liable or responsible of any mistakes that may happen by chance or due to circumstances beyond its control.<br>
  <br>
  b. Keep absolute confidentiality of all information concerning the SUPPLIER, both if supplied directly or generated as a consequence of relationships between the SUPPLIER and GRUPO COSENTINO.<br>
  <br>
 c. Provide the SUPPLIER, at any time, the situation of its data inside the database, and not to provide it to any third parties except to allow the SUPPLIER to present offers to be taken into consideration as supply alternatives. Also, to provide the SUPPLIER, at any time, the situation of its data inside the database, so that the SUPPLIER, with previous written notification, modifies or deletes them.<br>
  <br> <br>
  <b>6. Rights of GRUPO COSENTINO and its clients</b><br>
  <br>
 a. The SUPPLIER gives its explicit consent for GRUPO COSENTINO to communicate information concerning the SUPPLIER to associated companies or companies belonging to the same group, as well as others with which it may sign agreements with the only aim of providing the best services, in compliance, in any case, with Spanish legislation on data protection. Also, the SUPPLIER agrees on GRUPO COSENTINO, sending information about goods or services they commercialize, directly or indirectly, or that will be commercialized.
<BR>
<BR>
b. Choose, in case of infringement by the SUPPLIER of its obligations, between demanding full compliance with its obligations or cancellation of the contract, in the terms expressed in clause 8 of this Legal Notice.
  <BR><BR>
  <b>7. Limitation of responsibility </b><br>
  <br>
a. GRUPO COSENTINO shall not be liable or responsible for the availability of the service, network or technical failures that might cause an interruption or cuts in the Portal.<br>Equally, GRUPO COSENTINO shall not be liable of any damages caused by the spreading of IT viruses or other elements in the system.  <br /><br />
b. GRUPO COSENTINO shall not be liable or responsible of any payments and /or complaints that derive from the acts of clients concerning the agreement between the client(s) and the SUPPLIER.<br>
  <br>
 c. GRUPO COSENTINO shall not be liable or responsible of any infringement of the Laws or generally agreed Customs, or of acts that contravene obligations established in this Legal Notice, by any employee of GRUPO COSENTINO, by the SUPPLIER, or by any Internet or Network user.<br>However, were GRUPO COSENTINO or its clients to have knowledge of any conduct the previous paragraph refers to, it shall adopt the necessary measures to resolve with the upmost diligence and urgency the conflicts caused. <br /><br />
d. GRUPO COSENTINO shall not be held liable or responsible of any interference by unauthorized third parties in the knowledge of the conditions and circumstances of use that the SUPPLIER shall do of the Portal and its Services.<br>

<br>
  <br>
      
  <b>8. Contract cancellation </b><br>
  <br>
  In the case of failure to comply with the obligations contained in this Legal Notice, the SUPPLIER or GRUPO COSENTINO and its clients must notify the breach to the infringing party, with a fifteen working day period to repair it. After that period has passed, should the infringement not be repaired, the other party can choose between the fulfilling and cancellation of this agreement. Should it choose to cancel the contract, both the SUPPLIER and GRUPO COSENTINO or its clients agree that simple notification shall be enough for the cancellation to have full effect.  <br>
<br>
  <br>
      
  <b>9. Duration</b>
  
The proceeding to cancel the contract will be to send an e-mail indicating “PORTAL UNSUSCRIBE” to the address   <a href="mailto:atencionalcliente@fullstep.com"> atencionalcliente@fullstep.com </a> or by fax at +34 91 296 20 25. <br>
<br>
      <br>
        
    <b>10. Notifications, Legislation and Jurisdiction</b><br>
      <br>
      a. Notifications between the parties may be carried out through any of the means admitted by the Law that allows having a record of reception, including fax.<br>
      
      b. Changes in addresses and fax numbers shall be notified in writing, and shall not have effect until two working days from reception.<br>c. The relationships between GRUPO COSENTINO and the SUPPLIER derived from the use of the Portal and the Services carried out inside it, will comply with adequate Spanish legislation. 
			</p>
            <p style="text-align:right"><a href="registro_texto.htm" class="bt_registro" target="blank">Print</a> </p>
<div class="caja_acepto">


<!--<form action="" method="post" style="width:400px;">//-->
<form name="frmAlta" id="frmAlta" method="post" style="width:400px;">
  <span id="sprycheckbox1">
<label>
  <input type="checkbox" name="opcion1" id="opcion1" required>
</label>
<span class="checkboxRequiredMsg">Select to continue.</span></span><span class="rojo">I have read and accept</span> the General terms and conditions of use of the Supplier Portal.<br />
<br />

<button type="button" class="bt_registro" id="submit1" name="submit1">Next</button>
<br>
</form>


</div>
<div style="clear:both;"></div>
    	</div>    
    </div>
</div>
<script type="text/javascript">
// <!--
// var sprycheckbox1 = new Spry.Widget.ValidationCheckbox("sprycheckbox1");
// //-->
$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked')) {
        $('#frmAlta').submit();
        alta();
    }
});
</script>
 			
</body>

</html>
