<%@ Language=VBScript %>
<!--#include file="../../login.asp"-->
<%		
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if

If Idioma="" then
	Idioma="ENG"
end if

%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Supplier Portal</title>
<link rel="stylesheet" type="text/css" href="../estilos.css">
<!--[if lte IE 9]>
<link href="../estilos_ie9.css" rel="stylesheet" type="text/css" />
<![endif]-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400' rel='stylesheet' type='text/css'>
<script language="javascript">
function ventanaLogin (IDI){
   window.open ("registro.asp?Idioma="+IDI,"","width=800,height=600,resizable=yes")
}
function recuerdePWD()
{
   window.open("recuerdo.asp","_blank","width=715,height=400,scrollbars=NO")
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
</script>
<script src="js/jquery-1.9.0.min.js"></script>
<!--ventana modal-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" href="js/colorbox.css" />
<script src="../js/jquery.colorbox.js"></script>
<script>
    $(document).ready(function () {
        $(".modal").colorbox();
    });
</script>
<!--*****************-->
<script>
$(document).ready(function(){
	
	//---------------------
  	$("#txtCIA").val("Company Code");
  	$("#txtCIA").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Company Code"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Company Code");
  			$(this).removeClass('focus');
  		}
  	});	
	//---------------------
  	$("#txtUSU").val("User Code");
  	$("#txtUSU").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="User Code"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("User Code");
  			$(this).removeClass('focus');
  		}
  	});
	//---------------------
  	$("#txtPWD").val("Password");
  	$("#txtPWD").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Password"){
  			$(this).val('');
			$(this).removeAttr("type");
			$(this).prop('type', 'password');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Password");
			$(this).removeAttr("type");
			$(this).prop('type', 'text');
  			$(this).removeClass('focus');
  		}
  	});
});
</script>
</head>

<body>
<div class="wrapper">
  <div id="cabecera">
    <div id="logo"><a href="http://www.cosentino.es" target="_blank"><img src="../images/logo.png" border="0"></a></div>
    <div id="titulo">Supplier Portal</div>
    <div id="nav">
      <ul>
        <li><a href=""> ENG</a></li>
        <li> |<a href="../default.asp"> ESP</a> </li>
        <li> |<a href="<%=application("RUTANORMAL")%>custom/<%=application("NOMPORTAL")%>/public/eng/ayuda.html" target="_blank"> Help</a></li>
      </ul>
    </div>
  </div>
  <div class="clearfix"></div>
  <div id="container"> 
    <!--  <div id="linea"></div>!-->
    
    <div id="image-main"><img src="../images/image.jpg" alt="image"/></div>
    <div id="acceso">
      <h2>SUPPLIER ACCESS</h2>
      <form action="default.asp" method="post" name="frmLogin"  id="frmLogin">
        <input type="text" class="text" id="txtCIA" name="txtCIA" maxlength="20" />
        <input type="text" class="text" id="txtUSU" name="txtUSU" maxlength="20" />
        <input type="password" class="text" id="txtPWD" name="txtPWD" maxlength="20" />
        <div id="bt_acceso">
          <input class="bt" name="cmdEntrar" type="submit" value="LOGIN" />
        </div>
        <input type="hidden" id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
        <input type="hidden" id="CiaComp" name="CiaComp" value="<%=CiaComp%>">
      </form>
      <div class="clearfix"></div>
      <div id="claves"><a href="javascript:void(null)" onClick="recuerdePWD()" >Forgot your access codes?</a></div>
      <div class="clearfix"></div>
      <div id="registro">
        <h3>Not yet registered?</h3>
        <div id="bt_registro"><a href="javascript:ventanaLogin('ENG')" >Register</a></div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div id="cont_textos">
      <div class="textos">
        <p>GRUPO COSENTINO offers its suppliers a direct communications channel which they can use to respond to requests for quotations made by the purchasing department.</p>
      </div>
      <div class="textos">
        <p>If you have already registered in this portal and have been authorized as a supplier, you can access the private suppliers' area by entering your passwords.  </p>
        <p>If you have still not registered in the portal, you can apply via the "Register" link.</p>
      </div>
    </div>
    <div id="soporte">
      <h3>Supplier Help Desk</h3>
      <p>Tel. +34 902 996 926<BR> <a href="mailto:atencionalcliente@fullstep.com"> atencionalcliente@fullstep.com </a></p>
    <h4>Opening hours</h4>
      <p>Monday to thursday:
        8:00 - 21:00 h.<br>
        Fridays: 
        8:00 - 19:00 h.</p>
    </div>
  </div>
  <div class="clearfix"></div>
  <div id="pie">
    <div id="footer_left">
      <div id="enlaces"> <a href="<%=application("RUTANORMAL")%>custom/<%=application("NOMPORTAL")%>/public/eng/aviso-legal.html" target="_blank">Legal notice </a> | <a href="<%=application("RUTANORMAL")%>script/politicacookies/politicacookies.asp?Idioma=ENG" target="_blank">Cookies policy</a></div>
        <div id="powered"> <a href="http://www.fullstep.com" target="_blank"><img src="../images/logoFullStep.png"  alt=""/></a> </div>
    </div>
    <div id="footer_right">
      <p>Website optimized for 1280 x 1024<br>
        Supported browsers: Internet Explorer and Mozilla Firefox</p>
    </div>
  </div>
</div>
</body>
</html>
