﻿<%@ Language=VBScript %>
<!--#include file="../login.asp"-->
<%		
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if

If Idioma="" then
	Idioma="SPA"
end if

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="estilos.css" rel="stylesheet" type="text/css" />

<script language="javascript">
<!--


function ventanaLogin (IDI){

   window.open ("registro.asp?Idioma="+IDI,"","width=750,height=760,resizable=yes")

}
function ventanaSecundaria (URL){

   window.open(URL,"ventana1","width=700,height=420,scrollbars=no")

}
function ventanaAyuda (URL){

   window.open(URL,"ayuda","width=700,height=600,scrollbars=yes")

}

function popup (URL) {
 window.open("popup.htm","","width=700,height=400,scrollbars=NO")
 }
function recuerdePWD()
{
   window.open("spa/recuerdo.asp","_blank","width=715,height=345,scrollbars=NO")
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
//-->
</script>
<script src="js/jquery-1.9.0.min.js"></script>
<!--ventana modal-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" href="js/colorbox.css" />
<script src="js/jquery.colorbox.js"></script>
<script>
    $(document).ready(function () {
        $(".modal").colorbox();
    });
</script>
<!--slide de imagenes-->
<link rel="stylesheet" href="nivo/nivo-slider.css" type="text/css" media="screen" />
<script type="text/javascript" src="nivo/jquery.nivo.slider.js"></script>
<!--*****************-->
<script>
$(document).ready(function(){
	//---------------------
	
	//---------------------
	$('#slider').nivoSlider({
		effect: 'fade',
		directionNav: false,
		controlNav: false,
		pauseOnHover: false	
	});
	//---------------------
  	$("#txtCIA").val("Código Compañía");
  	$("#txtCIA").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Código Compañía"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Código Compañía");
  			$(this).removeClass('focus');
  		}
  	});	
	//---------------------
  	$("#txtUSU").val("Código de Usuario");
  	$("#txtUSU").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Código de Usuario"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Código de Usuario");
  			$(this).removeClass('focus');
  		}
  	});
	//---------------------
  	$("#txtPWD").val("Contraseña");
  	$("#txtPWD").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Contraseña"){
  			$(this).val('');
			$(this).removeAttr("type");
			$(this).prop('type', 'password');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Contraseña");
			$(this).removeAttr("type");
			$(this).prop('type', 'text');
  			$(this).removeClass('focus');
  		}
  	});
});
</script>
</head>
<body>
<div id="contenedor">
	<!--cabecera-->
	<div id="cabecera">
    	<div id="logo">
        	<a href="http://www.compass-group.es"><img src="images/logo-compass.png" alt="Compass Group" /></a>
        </div>
       <!-- <div id="nav_sup">
            	<ul>
                	<li><a href="">English</a></li>
                    <li><a href="">Portugués</a></li>
                </ul>
      </div>-->
        <div id="nav">
        	
           <ul id="nav_inf">
            <li><a href="" class="sel">Inicio</a></li>
            <li><a href="<%=application("RUTANORMAL")%>custom/<%=application("NOMPORTAL")%>/public/politica-de-compras.html" >Política<br /> de compras</a></li>
            <li><a href="<%=application("RUTANORMAL")%>custom/<%=application("NOMPORTAL")%>/public/ambito-de-compras.html" >Ámbito de<br /> compras</a></li>
            <li><a href="<%=application("RUTANORMAL")%>custom/<%=application("NOMPORTAL")%>/public/condiciones-de-compras.html" >Condiciones <br />de compras</a></li>           	<li><a href="<%=application("RUTANORMAL")%>custom/<%=application("NOMPORTAL")%>/public/ser-proveedor.html" >Ser<br /> Proveedor</a></li>          
            </ul>
        </div>
    </div>
    <div id="breadcrumb">
    <a href="">inicio</a> > <a href="">Acceso Proveedores</a></p>
    </div>
    
    <!--********-->
    <!--titulo
<div id="titulo">
        <h1>Área <strong>proveedores</strong></h1>
        
    </div>-->
    <!--******-->
    <!--formulario acceso-->
    <div id="acceso">
    	<h1>Área de Proveedores</h1>
    	<div class="wform">
            <h2>ACCESO PROVEEDORES</h2>
            <form action="default.asp" method="post" name="frmLogin"  id="frmLogin">
                <input type="text" class="text" id="txtCIA" name="txtCIA" maxlength="20">
                <input type="text" class="text" id="txtUSU" name="txtUSU" maxlength="20">
                <input type="password" class="text" id="txtPWD" name="txtPWD" maxlength="20">
                <div class="bt_entrar">
                    <input class="bt" name="cmdEntrar" type="submit" value="ENTRAR" />
                    <a href="javascript:void(null)" onClick="recuerdePWD()" class="bt_entrar">¿Olvidó sus claves de acceso?</a></div>
                    <input type="hidden" id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
                <input type="hidden" id="CiaComp" name="CiaComp" value="<%=CiaComp%>">
            </form>
                <h3>¿AÚN NO ESTÁ REGISTRADO?</h3>
                <div align="center">
                   <a href="javascript:ventanaLogin('SPA')" >SOLICITAR REGISTRO</a>
                </div>
             
        </div>
    </div>
    <!--*****************-->
    <!--navegador iconos-->
<div id="nav_home">
    	<div class="contenido-home">
   		  <h2>¿Qué es Compass Group Spain?</h2>
          <p>Compass Group Spain ha recorrido un largo camino desde sus comienzos hace más de 60 años, hasta convertirse en la empresa líder de Restauración y Support Services que es hoy.</p>
                
          <p>Millones de personas en todo el mundo cada día nos confían sus desayunos, sus almuerzos y sus cenas. Nos sentimos orgullosos de prepararles sus cafés, bocadillos y de gestionar, cada vez más, sus servicios de recepción, limpieza y mantenimiento.</p>
                
                <ul>
                	<li><img src="images/Vitarest.png" width="85" height="85" /></li>
                    <li><img src="images/EurestServices.png" width="85" height="85" /></li>
                    <li><img src="images/Medirest.png" width="85" height="85" /></li>
                    <li><img src="images/Scolarest.png" width="85" height="85" /></li>
                    <li><img src="images/Vilaplana.png" width="85" height="85" /></li>
                    <li><img src="images/Eurest.png" width="85" height="85" /></li>
                </ul>
                
                
        </div>
        <div class="datos_fullstep">
        	ATENCIÓN A PROVEEDORES<br />
            <span class="rojo_24" style="line-height:24px;">Tel. 902 043 063</span><br />
            <a href="mailto:proveedores@compass-group.es " class="rojo">proveedores@compass-group.es </a>
            <img src="images/linea.png" alt="" style="margin:15px 0;" />
            HORARIO DE ATENCIÓN TELEFÓNICA:
            <p>
            LUNES A JUEVES<br />
            <span class="rojo">8:00 a 21:00</span>
            </p>
            <p>
            VIERNES<br />
            <span class="rojo">8:00 a 19:00</span>
            </p>
        </div>
        <div style="clear:both"></div>
    </div>
    <!--****************--> 
     <!--footer-->
    <div id="pie">
        <div class="datos_pie diez">
			<img src="images/logo-compass-smal.png" width="99" height="45" />
            <span>&copy; Compass Group 2013</span>
            <ul>
            <li><a href="<%=application("RUTANORMAL")%>custom/<%=application("NOMPORTAL")%>/public/aviso-legal.html" class="modal">Aviso legal</a></li>
            <li><a href="<%=application("RUTANORMAL")%>script/politicacookies/politicacookies.asp?Idioma=SPA" target="_blank" class="registro">Pol&iacute;tica de Cookies</a></li>
           </ul>
        </div>
        <!--
        <div class="datos_pie largo">
			<ul>
            <li><a href="">Contacta con nosotros</a></li>
            <li><a href="">Visita nuestras fábricas</a></li>
            <li><a href="">Mapa web</a></li>
            </ul>
        </div>
        -->
        <div class="datos_pie full" style="border:none;">
       	<img src="images/logo-fullstep.png" width="220" height="32" />
        </div>
        
    </div>
    <!--******-->
 </div>
</body>

</html>
