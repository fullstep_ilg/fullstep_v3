<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<link href="../estilos.css" rel="stylesheet" type="text/css">
<title>::Supplier Portal::</title>
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
}
-->
</style>
</head>
<script>
dibujaMenu(1)
</script>
<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo m�ximo:0</remarks>*/
function Init() {
    document.getElementById('tablemenu').style.display = 'block';

	MM_preloadImages('../images/icono_docs_sm.gif','../images/icono_docs_sm.gif')
	p = window.top.document.getElementById("frSet")
	vRows = p.rows
	vArrRows = vRows.split(",")
	vRows = vArrRows[0] + ",*,0,0"
	p.rows = vRows

}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">
<div id="inicio-left">
  <h1>Welcome to the ILUNION Supplier Website</h1>
  <p>You may access the various areas via the menu options found above.  </p>
  <ul>
    <li><b>Your data/ your company data</b> if you wish, you may change the data for your company, users, as well as the business activity areas of your company. </li>
    <li><b>Quality:</b> manage quality certificates, non-conformities and access your quality ratings. </li>
    <li><b>Requests:</b> Access requests (RFQ and other requests) and manage them from this very section.</li>
  
  </ul>
  
<p> If this is the first time you are to submit a quote through the website, carefully follow these steps:</p>
  <ol>
  <li>Click on “quotes” to see the RFQs that your company has opened. </li><br>
  <li>Select the quote request that you wish to reply to, by clicking the corresponding code. </li><br>
  <li>Complete your quote by filling out all the necessary information. From the navigation tree which is found on the left hand side, you may scroll through the different sections of the quote. To enter the prices, you must go to the section “items/prices.” Remember to enter the period of validity for the quote under the section “General Quote Data”. </li><br>
  <li>Communicate your quote by clicking on the submit button<IMG height=14 src="../images/sobre_tr.gif" width=30 align=absBottom></li>
 </ol>
</div>
<div id="inicio-right">
<h2>TUTORIALS</h2>
<p>Please download the instructions on how to make a quote, accept an order, tracking, etc.</p>
<ul>
<li><a href="docs/FSN_MAN_ATC_How to offer.pdf" target="_blank">How to quote</a></li>
<li><a href="docs/FSN_MAT_ATC_Technical Requirements.pdf" target="_blank">Technical Requirements</a></li>
<li><a href="docs/FSN_MAT_ATC_Master data.pdf" target="_blank">Data maintenance</a></li>
</ul>
<div id="img-atc"><img src="../images/imagen-ATC.jpg"></div>
<p>If you have any doubt or difficulty operating through the Portal, please contact the <span>Supplier Help Desk<br>
 +34 91 327 85 00 </span><br> <a href="mailto:atencionalcliente@fullstep.com"> atencionalcliente@fullstep.com </a></p>
</div>

</body>
</html>
