﻿<%@ Language=VBScript %>

<html>

<head>

<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/idioma.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->

<%
''' <summary>
''' Mostrar el detalle del registro
''' </summary>
''' <remarks>Llamada desde: login/login.asp		login/codigo instalaci�n/registro.asp	login/codigo instalaci�n/codigo idioma/registro.asp; Tiempo m�ximo: 0</remarks>
%>

<title>Condiciones de Uso del Portal de Compras de GESTAMP AUTOMOCI&Oacute;N</title>

<link href="estilos.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
<!--
body {
	margin-top: 5px;
}
.Estilo4 {font-weight: normal; color: #2C4FA0; text-decoration: none; line-height: 14px; font-style: normal;}
-->
</style>

<script src="../../common/formatos.js"></script>
<script language="JavaScript" src="../../common/ajax.js"></script>
<!--#include file="../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
</head>


<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=SPA", "_blank", "width=730,height=635")
window.close()

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=550,height=600,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
  </script>


<script>
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=550,height=600,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>


<body bgcolor="#FFFFFF" leftmargin="5" onload="init()">
<form name="frmAlta" id="frmAlta" method="post">

<table border="0" width="100%">
  <tr>
    <td><a href="www.gestamp.com" target="_blank"><img src="images/logo.gif" HEIGHT="62" border="0"></a>
	<hr width="100%" size="1" color="#0051BA">
    </td>
  </tr>
  <tr>
    <td valign="top">
      <p align="left" class="noticias"> <b>Para continuar con el proceso de alta es imprescindible que lea y acepte las condiciones de acceso al Portal</b> </p>
      <table border="0">
        <tr>
          <td width="6" valign="top"><p align="center"> </td>
          <td width="683"><font face="Verdana" size="1">
          <textarea name="S1" cols="85" rows="11" readonly class="textos" style="font-family: Verdana; font-color: #2C4FA0; font-size: 8pt; text-align:justify; display:block; height:135px; line-height: 150%; list-style-type: lower-alpha">
IMPORTANTE, LEA ATENTAMENTE

CONDICIONES DE USO DEL PORTAL DE COMPRAS

El alta como PROVEEDOR en el Portal de Compras de GESTAMP AUTOMOCIÓN está condicionada a la previa lectura y aceptación de las siguientes cláusulas. Sin expresar su conformidad con las mismas no podrá registrarse. Siempre que se acceda y utilice el Portal, se entenderá que está de acuerdo, de forma expresa, plena y sin reservas, con la totalidad del contenido de estas Condiciones y con el Aviso Legal y la Política de Privacidad del Sito Web www.gestamp.com. Asimismo, al ser Vd. el Usuario Principal de su empresa, queda obligado, al darse de alta como usuario de este Portal, a velar por su cumplimiento por parte de todos los usuarios que se den de alta en su empresa, liberando a GESTAMP AUTOMOCIÓN de toda responsabilidad por los posibles perjuicios que dichos usuarios pudieran causar a su empresa o cualquier otra debido a sus actuaciones en el Portal.

El Portal de Compras de GESTAMP AUTOMOCIÓN es el medio a través del cual GESTAMP AUTOMOCIÓN se comunica con sus proveedores para solicitarles ofertas, documentos así como aquella información comercial que estime oportuna. Al mismo tiempo GESTAMP AUTOMOCIÓN puede utilizar el portal para remitirle aquella información que considere de su interés.

La recogida y tratamiento automatizado de los Datos Personales por GESTAMP AUTOMOCIÓN, inscritos voluntariamente por el PROVEEDOR, tiene como finalidad únicamente el mantenimiento de la relación contractual en su caso establecida con GESTAMP AUTOMOCIÓN, esto es, solicitar ofertas y remitir comunicados relativos a la función de compras de GESTAMP AUTOMOCIÓN.

El PROVEEDOR estará obligado a:

-	Guardar absoluta confidencialidad en relación con toda la información que se genere en las relaciones entre el PROVEEDOR y GESTAMP AUTOMOCIÓN.

-	Cumplir fielmente sus compromisos en la información remitida a través del portal. En el caso de que el PROVEEDOR no demuestre la necesaria diligencia comercial, o incumpla las obligaciones contraídas, GESTAMP AUTOMOCIÓN se reserva el derecho de excluir temporal o permanentemente al PROVEEDOR del portal.

-	El PROVEEDOR deberá indicar solamente aquellos grupos de materiales, que se refieran a bienes o servicios que, en el momento de la aceptación del acuerdo, el PROVEEDOR comercialice, fabrique o distribuya y que sean del interés comercial de GESTAMP AUTOMOCIÓN.

-	El PROVEEDOR acepta que las ofertas introducidas en el Portal sean consideradas con el mismo rango y validez que una oferta enviada por cualquier otro medio tradicional (carta, fax).

Como contraprestación, el PROVEEDOR tendrá derecho a:

-	Mantener una presencia constante en la base de datos de GESTAMP AUTOMOCIÓN, en su calidad de PROVEEDOR dado de alta. 

-	Recibir peticiones de ofertas en virtud de las normas establecidas.

El alta como PROVEEDOR en el Portal de Compras de GESTAMP AUTOMOCIÓN no otorga derecho alguno al PROVEEDOR a participar necesariamente en las peticiones de oferta de GESTAMP AUTOMOCIÓN.

GESTAMP AUTOMOCIÓN podrá optar, en caso de incumplimiento de sus obligaciones por el PROVEEDOR, por exigir el debido cumplimiento de las mismas, o por proceder a dar de baja al PROVEEDOR en la base de datos de este Portal.





          </textarea>
</font> </td>
        </tr>
      </table>
     
      <span class="noticias">Para suministrar productos y/o servicios a cualquier empresa de Gestamp, es obligatoria la aceptación de las <a href="spa/docs/2016_05_31_GTC__ES__EXECUTION_VERSION.PDF" target="_blank" class="registro">Condiciones generales de compra y contratación</a> y las <a href="spa/docs/Gestamp_Responsabilidad_Social.pdf" target="_blank" class="registro">exigencias en materia de responsabilidad social corporativa</a> de GESTAMP AUTOMOCIÓN hacia sus proveedores.</span><br>   
      <br>
<span class="noticias">¿Acepta el contrato de adhesión al portal de compras de GESTAMP AUTOMOCIÓN, el <a href="http://www.gestamp.com" target="_blank" class="registro">aviso legal</a> de <a href="http://www.gestamp.com" target="_blank" class="registro">www.gestamp.com</a>,  la <a href="2018.05.25 Politica de Proteccion de Datos Proveedores.pdf" target="_blank" class="registro">Política de Protección de Datos Proveedores</a>,  las <a href="spa/docs/2016_05_31_GTC__ES__EXECUTION_VERSION.PDF" target="_blank" class="registro">Condiciones generales de compra y contrataci&oacute;n</a>y las <a href="spa/docs/Gestamp_Responsabilidad_Social.pdf" target="_blank" class="registro">exigencias en materia de responsabilidad social corporativa</a>?<b>
                <br>
        
          </b> Si pulsa &quot;No Acepto&quot;, su empresa no podrá ser registrada como proveedor autorizado de GESTAMP AUTOMOCIÓN.<br>
        Para continuar el proceso de alta debe pulsar &quot;Acepto&quot;. <br>
        Si quiere imprimir el contrato puede hacerlo pulsando el botón &quot;Imprimir&quot;. </span>   <br>  <br>  
		<div align="right">
          <table width="39%" border="0" align="left">
            <tr>
              <td width="27%"><p align="left">
                  <input type="submit" onClick="return alta()" value="Acepto" id="submit1" name="submit1">
              </p></td>
              <td width="37%"><input type="button" onClick="no_alta()" value="No acepto" id="button1" name="button1">
              </td>
              <td width="36%"><input type="button" onClick="imprimir()" value="Imprimir" id="button2" name="button2">
              </td>
            </tr>
          </table>
        </div><br><br>
        <p align="left" class="textos">        
        <hr width="100%" size="1" color="#0051BA">
    </td>
  </tr>
</table>

	<input type="hidden" name="ACEPTADO">
	
</form>

</body>
<!--#include file="../../common/fsal_2.asp"-->
</html>
