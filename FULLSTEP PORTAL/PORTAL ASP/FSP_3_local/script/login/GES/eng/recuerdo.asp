﻿<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<html>
<head>
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style>
    <link href="../estilos.css" rel="stylesheet" type="text/css">
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;
    };
</script>
<body>
    <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
    <input type="hidden" name="idioma" value="eng">
    <table width="620" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="20">&nbsp;
                
            </td>
            <td width="615" align="left">
                <a href="http://www.gestamp.com" target="_blank">
                    <img src="../images/logo.gif" height="62" border="0"></a>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;
                
            </td>
        </tr>
        <tr>
            <td width="20">&nbsp;
                
            </td>
            <td width="615" align="left">
                <br/>
                <span style="color:#666666; font-family:Verdana;" class="textos">
                   If you forgot your password, please fill in your user and email and click on "Submit". You will receive a link to a page to create a new password.<br>
                </span>
                <br/>
                <table class="textos">
                    <tr>
                        <td>Company Code:</td>
                        <td><input type="text" name="txtCia" id="txtCia" size="80" maxlength="100"></td>
                    </tr>
                    <tr>
                        <td>User Code:</td>
                        <td><input type="text" name="txtUsu" id="txtUsu" size="80" maxlength="100"></td>
                    </tr>
                    <tr>
                        <td>E-mail address:</td>
                        <td><input type="text" name="txtEmail" id="txtEmail" size="80" maxlength="100"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="20">&nbsp;</td>
            <td style="color:#666666; font-family:Verdana;" class="textos">
                <br /><input type="submit" value="Submit" name="cmdEnviar" id="Submit1"/>
                <br /><br />Suppliers call center: <strong>902 02 6000 </strong>- Calls from outside Spain <strong>+34 917 291 218</strong><BR>
		  Opening hours(*): Monday to Thursday from 8:30 to 21:00.  Fridays from 8:00 to 19:00<br>
<em>(*)   Central European Time (CET)  - Email: <a href="mailto:%20suppliersarea@gestamp.com">suppliersarea@gestamp.com</a></em>
            </td>
        </tr>
    </table>
</body>
</html>
