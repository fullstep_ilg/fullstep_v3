﻿<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<html>
<head>
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style>
    <link href="../estilos.css" rel="stylesheet" type="text/css">
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;
    };
</script>
<body>
    <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
    <input type="hidden" name="idioma" value="eng">
    <table width="620" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="20">&nbsp;
                
            </td>
            <td width="615" align="left">
                <a href="http://www.gestamp.com" target="_blank">
                    <img src="../images/logo.gif" height="62" border="0"></a>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;
                
            </td>
        </tr>
        <tr>
            <td width="20">&nbsp;
                
            </td>
            <td width="615" align="left">
                <br/>
                <span style="color:#666666; font-family:Verdana;" class="textos">
                  Wenn Sie Ihr Passwort vergessen haben, geben Sie den Unternehmenscode sowie den Benutzercode und die E-Mail-Adresse Ihres Zugriffskontos ein und klicken Sie auf “Abschicken”. Sie erhalten dann eine E-Mail mit dem Link zu der Seite, wo Sie ein neues Passwort einrichten können.<br>
                </span>
                <br/>
                <table class="textos">
                    <tr>
                        <td>Firmencode:</td>
                        <td><input type="text" name="txtCia" id="txtCia" size="80" maxlength="100"></td>
                    </tr>
                    <tr>
                        <td>Benutzercode:</td>
                        <td><input type="text" name="txtUsu" id="txtUsu" size="80" maxlength="100"></td>
                    </tr>
                    <tr>
                        <td>E-mail addresse:</td>
                        <td><input type="text" name="txtEmail" id="txtEmail" size="80" maxlength="100"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="20">&nbsp;</td>
            <td style="color:#666666; font-family:Verdana;" class="textos">
                <br /><input type="submit" value="Abschicken" name="cmdEnviar" id="Submit1"/>
                <br /><br />                
                Telefon Anbieterservice: <strong>902 02 6000 </strong>&ndash; Anrufe von au&szlig;erhalb Spaniens:<strong> +34 917 291 218</strong><BR>
		  B&uuml;rozeiten (*): Montag bis Donnerstag 8.30 bis 21.00 Uhr. Freitags von 8.00 bis 19 Uhr<br>
<em>(*) Mitteleurop&auml;ische Zeit (MEZ)  - Email: <a href="mailto:%20suppliersarea@gestamp.com">suppliersarea@gestamp.com</a> </em>
            </td>
        </tr>
    </table>
</body>
</html>
