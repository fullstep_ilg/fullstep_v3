﻿<%@ Language=VBScript %>
<!--#include file="../../../common/acceso.asp"-->
<%
''' <summary>
''' Pantalla inicial tras hacer un login valido
''' </summary>
''' <remarks>Llamada desde: login\GESTAMP\inicio.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,false,false,0)

DIM FICHERO_SPA
FICHERO_SPA= Application("PATHFICHERONOTICIAS") & ".SPA"
sNoticias = oRaiz.leerFicheroTexto(FICHERO_SPA)
set oRaiz = nothing

%>
<script SRC="../../../common/menu.asp"></script>

<html>

<head>
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="../estilos.css" rel="stylesheet" type="text/css">
<title>Portal de compras de Gestamp Automoci&oacute;n</title>
<style type="text/css">
<!--
body {
	margin-top: 0px;
	margin-left: 0px;
}
.Estilo1 {
	color: #E62132;
	font-size: 10px;
}
.Estilo11 {text-decoration: none; line-height: 14px; font-weight: normal; color: #E62132;}
.Estilo12 {	font-size: 10px;
	color: #FF0033;
}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
	/*''' <summary>
	''' Iniciar la pagina.
	''' </summary>
	''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
	function Init() {
		document.getElementById('tablemenu').style.display = 'block';

		p = window.top.document.getElementById("frSet")
		vRows = p.rows
		vArrRows = vRows.split(",")
		vRows = vArrRows[0] + ",*,0,0"
		p.rows = vRows
	}
</script>
</head>
<body scroll="yes" onload="Init()">
<script>
dibujaMenu(1)
</script>
<script language=javascript>

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}

</script>

<!-------------------------- BEGIN COPYING THE HTML HERE ---------------------------->

<table width="105%" border="0" cellpadding="5" cellspacing="2" bordercolor="0" hspace="0" vspace="0">
  <tr>
	<td width="48" rowspan="3" align="right" valign="top" bordercolor="0"> <div align="left" class="textos"></div>
	</td>
	<td height="47" colspan="2" align="left" valign="bottom"><font size="2" face="verdana" class="titulo"><strong>Bienvenido</strong></font></td>
	<td colspan="2" align="left" valign="bottom"><span class="textos"><strong>NOTICIAS</strong></span></td>
  </tr>
  <tr>
	<td width="304" valign="top" bgcolor="#EEEEEE" class="textos"> Aqu&iacute; encontrar&aacute; las solicitudes de oferta que GESTAMP AUTOMOCI&Oacute;N tiene para su compa&ntilde;ia.<BR>
	  <BR>
Puede acceder a las distintas &aacute;reas a trav&eacute;s de las opciones de men&uacute; situadas en la parte superior.
<ul>
  <li><b>Solicitudes de ofertas :</b> Puede acceder a los procesos de compra abiertos por GESTAMP AUTOMOCI&Oacute;N, a los que su empresa ha sido invitada.</li>
</ul>
<ul>
  <li><b>Sus datos/ su compa&ntilde;&iacute;a :</b> Puede gestionar los datos de su empresa, usuarios, as&iacute; como las &aacute;reas de actividad en las que su empresa se encuentra homologada. </li>
</ul></td>
	<td width="112" rowspan="2" valign="top" class="textos">&nbsp;</td>
	<td colspan="2" align="left" valign="top">
<div name="divNoticiasright" id="divNoticiasright" class="noticias" style="width:400 px">
	  <table align=center width=98%>
		<tr>
		  <td class="noticias"><%=sNoticias%> </td>
		</tr>
	  </table>
	</div>
</td> 
  </tr>
  <tr>
	<td width="304" valign="middle" class="textos">	Para resolver sus dudas o hacernos llegar sus comentarios, pngase en contacto con nosotros en el telfono 902 02 6000  o enviando un correo electrónico a<a href="mailto:suppliersarea@gestamp.com" class="textos"> suppliersarea@gestamp.com</a><br></td>
	<td width="245" align="left" valign="bottom"><span class="textos"><strong>DOCUMENTACI&Oacute;N RELEVANTE<br>
		  <br>
	  </strong></span>
	  <table width="521" border="0" class="textos">
		<tr>
		  <td><a href="docs/FSN_MAN_SOP_Requisitos_tecnicos.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> Requisitos t&eacute;cnicos</a></td>
		</tr>
		<tr>
		  <td width="273" class="textos" ><a href="docs/FSN_MAN_SOP_Como_ofertar.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> C&oacute;mo ofertar</a></td>
		</tr>
		<tr>
		  <td><a href="docs/FSN_MAN_SOP_Mantenimiento_de_datos.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> Cómo actualizar los datos del proveedor</a></td>
		</tr>
		<tr>
		  <td><a href="docs/2016_05_31_GTC__ES__EXECUTION_VERSION.PDF" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> Condiciones generales de compra </a><span class="Estilo1"> (¡NUEVO!)</span></td>
		</tr>
		<tr>
	      <td><a href="docs/Politica_de_Proteccion_de_Datos_Proveedores.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> Pol&iacute;tica de protecci&oacute;n de datos</a><span class="Estilo1"> (¡NUEVO!)</span></td>
        </tr>		
		<tr>
		  <td><a href="docs/Gestamp_Responsabilidad_Social.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> Responsabilidad social corporativa </a></td>
		</tr>
        <tr>
		  <td height="16"><a href="docs/Normativa regalos y obsequios - GESTAMP.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> Normativa regalos y obsequios </a></td>
		</tr>		
		<tr>
		  <td><a href="docs/GES_Manual_Calidad_Proveedores.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> Manual de calidad  de proveedores </a></td>
		</tr>
		<tr>
		  <td height="16"><a href="docs/GES_QA_GUI_Proveedor.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> Gu&iacute;a del usuario (aspectos de calidad) </a></td>
		</tr>
        <tr>
	      <td><a href="../eng/docs/GES_Supplier_Performance_Indicators_Guide.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> Guía de desempeño del proveedor </a><span class="Estilo1"> (¡NUEVO!)</span></td>
        </tr>		
         <tr>
	      <td><a href="docs/GES_CON_QA_Safety Parts_Form Manual_Supplier.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> Manual de piezas de seguridad & reglamentación para proveedor </a></td>
        </tr>
		   <tr>
	      <td><a href="../eng/docs/Letter encouraging direct material suppliers to get the IATF 16949 Certification.pdf" class="textos" target="_blank"><img src="../images/flecha_azul.gif"  width="12" height="8" border="0"> Carta animando a los proveedores de material directo a obtener la certificación IATF 16949 </a></td>
        </tr>		
	  </table>
	  </a></span>    <font face="verdana" size="1" ><b></b></font></span>
</td>
	<td width="30" align="left" valign="bottom">&nbsp;</td>
  </tr>

</table>
</body>
</html>
