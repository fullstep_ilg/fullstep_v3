﻿<%@ Language=VBScript %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/idioma.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="css/reset.css" rel="stylesheet" type="text/css" />        
<link href="css/font-awesome.min.css" rel="stylesheet" />  
        
<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
<link href="css/style.css" rel="stylesheet" />  
<link rel="stylesheet" href="css/login.css">   
<link rel="stylesheet" type="text/css" href="css/responsive.css"/>   
<script src="js/jquery-1.9.0.js"></script>
<script src="js/jquery-1.9.0.min.js"></script>
<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
<script src="../../common/formatos.js"></script>
<script language="JavaScript" src="../../common/ajax.js"></script>
<!--#include file="../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=SPA", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body onload="init()">
   <div class="container-login">
            <div class="compound">

   <header class="cf">
                    <div class="(6/12) col"><a href="<%=application("RUTASEGURA")%>"><div class="logo" style="background-image: url('css/img/logo.png');"></div></a></div>
                    <div class="(6/12) col"><h2 class="text text--right">PORTAL DE PROVEEDORES</h2></div>
                </header>

    			
        
        
<section id="servicios"> 
            
            <div class="container">
            
           		<div class="text">
                
                	<!--
                    <div class="columna sol">
                        <div class="imagen">
                      <img src="img/shim.gif" width="100" >
                         </div>  
                        
                   	</div>
                    -->
                    <div class="centro sin solicitud">
                      <div>
                        
                        <h3>ALTA DE PROVEEDORES</h3>
        <div class="int">
        	<p>Para continuar con el proceso de alta es imprescindible la aceptación de las siguientes condiciones de uso:</p>
            <div class="caja_registro">
                <strong>POR FAVOR LEA ATENTAMENTE</strong><br><br>



Alta de proveedores - Aviso legal<br><br>

Acceso al Portal<br><br>

El alta como proveedor en el Portal de compras de Samsung Electronics Iberia, S.A., Sociedad Unipersonal (en adelante SAMSUNG) está condicionado a la previa lectura y aceptación de las siguientes cláusulas. Sin expresar su conformidad con las mismas no podrá registrarse. Siempre que se acceda y utilice el Portal, se entenderá que está de acuerdo, de forma expresa, plena y sin reservas, con la totalidad del contenido de este Aviso Legal. Asimismo, al ser usted el Usuario Principal de su empresa, queda obligado al aceptar la conformidad de este Aviso Legal a velar por su cumplimiento por parte de todos los usuarios que se den de alta en su empresa, liberando a SAMSUNG de toda responsabilidad por los posibles perjuicios que dichos usuarios pudieran causar a su empresa o cualquier otra debido a sus actuaciones en el Portal.<br><br>


Cláusulas<br><br>

1. Objeto del Portal de compras de SAMSUNG <br><br>

El portal de compras de SAMSUNG es el medio a través del cual SAMSUNG se comunica con sus proveedores para solicitarles ofertas, documentos así como aquella información comercial que estime oportuna. Al mismo tiempo SAMSUNG puede utilizar el portal para remitirle aquella información que considere de su interés.
SAMSUNG actúa tanto como comprador directo como de gestor de compras de sus clientes, y actuará en nombre de ellos durante el proceso de compra.<br><br>

El uso o acceso al Portal y/o a los Servicios no atribuyen al PROVEEDOR ningún derecho sobre las marcas, nombres comerciales o signos distintivos que aparezcan en el mismo, siendo éstos propiedad de SAMSUNG o de terceros. En consecuencia, los Contenidos son propiedad intelectual de SAMSUNG o de terceros, sin que puedan entenderse cedidos al PROVEEDOR.<br><br>


2. Objeto <br><br>

El presente acuerdo tiene por objeto regular las relaciones entre SAMSUNG y el PROVEEDOR, en todo lo relacionado con el uso del Portal.<br><br>


3. Obligaciones del PROVEEDOR<br><br>

Son obligaciones del proveedor las siguientes:<br><br>

a. Proporcionar cuantos datos sean necesarios para el adecuado funcionamiento del sistema, así como mantenerlos actualizados, comunicando a la mayor brevedad posible todo cambio que se produzca en los mismos.<br><br>

b. Garantizar la autenticidad de los datos facilitados a consecuencia de la cumplimentación de los formularios necesarios para la suscripción de los Servicios. Asimismo, el PROVEEDOR actualizará la información facilitada para que refleje, en cada momento, su situación real. Por todo ello, el PROVEEDOR será el único responsable de los daños y perjuicios ocasionados a SAMSUNG como consecuencia de declaraciones inexactas o falsas.<br><br>

c. Guardar absoluta confidencialidad en relación con toda la información que se genere en las relaciones entre el PROVEEDOR y SAMSUNG.<br><br>

d. Abstenerse de cualquier manipulación en la utilización informática del Portal, así como el uso del mismo con fines distintos de aquellos para los que fue previsto. Asimismo, el PROVEEDOR se abstendrá de acceder a zonas no autorizadas del Portal.<br><br>

e. Cumplir fielmente sus compromisos en la información remitida a través del portal. En el caso de que el PROVEEDOR no demuestre la necesaria diligencia comercial, o incumpla las obligaciones contraídas, SAMSUNG se reserva el derecho de excluir temporal o permanentemente al PROVEEDOR del portal.<br><br>

f. El PROVEEDOR deberá indicar solamente aquellos grupos de materiales, que se refieran a bienes o servicios que, en el momento de la aceptación del acuerdo, el proveedor comercialice, fabrique o distribuya y que sean del interés comercial de SAMSUNG.<br><br>

g. El PROVEEDOR acepta que las ofertas introducidas en el Portal sean consideradas con el mismo rango y validez que una oferta enviada por cualquier otro medio tradicional (carta, fax).<br><br>
h. El PROVEEDOR se obliga a hacer un uso correcto del Portal y de los Servicios conforme a la ley, el presente Aviso Legal y demás reglamentos de uso e instrucciones puestos en su conocimiento, así como con la moral y las buenas costumbres generalmente aceptadas y el orden público.<br><br>
Por ello, se abstendrá de utilizar el Portal o cualquiera de los Servicios con fines ilícitos, prohibidos en el presente Aviso Legal, lesivos de los derechos e intereses de terceros. En particular, y a título meramente indicativo, el PROVEEDOR se compromete a no transmitir, difundir o poner a disposición de terceros informaciones, datos, contenidos, gráficos, archivos de sonido y/o imagen, fotografías, grabaciones, software y, en general, cualquier clase de material que:<br><br>
1. Sea contrario a los derechos fundamentales y libertades públicas reconocidas en la Constitución, Tratados internacionales y leyes aplicables; <br><br>
2. Induzca, incite o promueva actuaciones delictivas, contrarias a la ley, a la moral y buenas costumbres generalmente aceptadas o al orden público; <br><br>
3. Sea falso, inexacto o pueda inducir a error, o constituya publicidad ilícita, engañosa o desleal;<br><br>
4. Esté protegido por derechos de propiedad intelectual o industrial pertenecientes a terceros, sin autorización de los mismos;
5. Vulnere derechos al honor, a la intimidad o a la propia imagen;
6. Vulnere las normas sobre secreto de las comunicaciones;
7. Constituya competencia desleal, o perjudique la imagen empresarial de SAMSUNG o terceros;
8. Esté afectado por virus o elementos similares que puedan dañar o impedir el funcionamiento adecuado del Portal, los equipos informáticos o sus archivos y documentos.<br><br>

4. Derechos del PROVEEDOR<br><br>

Son derechos del PROVEEDOR los siguientes:<br><br>

1. Mantener una presencia constante en la base de datos de SAMSUNG, en su calidad de proveedor dado de alta. <br><br>

2. Recibir peticiones de ofertas en virtud de las normas establecidas. 


5. Obligaciones de SAMSUNG 

Son obligaciones de SAMSUNG las siguientes:

a. Mantener la información que estime oportuna actualizada, no siendo responsable de los errores que se produzcan por fuerza mayor o caso fortuito.

b. Guardar absoluta confidencialidad de toda la información relativa al proveedor: bien la proporcionada por éste, bien la generada en las relaciones entre el PROVEEDOR y SAMSUNG

c. Facilitar al proveedor, en cualquier momento, la situación de sus datos en la base de datos, y no facilitarla a terceros si no es para permitir al proveedor la presentación de sus ofertas y la toma en consideración de sus productos como alternativa de suministro. Asimismo, facilitar al proveedor, en cualquier momento, la situación de sus datos en la base de datos, para que éste, previa notificación por escrito, los modifique o elimine. 


6. Derechos de SAMSUNG

a. El Usuario presta su consentimiento para que SAMSUNG ceda sus datos a las empresas a ella asociadas o del grupo al que pertenece, así como a otras con las que concluya acuerdos con la única finalidad de la mejor prestación del servicio, respetando, en todo caso, la legislación española sobre protección de los datos de carácter personal. Asimismo, el Usuario acepta que SAMSUNG o sus empresas a ella asociadas, sociedades filiales y participadas, le remitan información sobre cualesquiera bienes o servicios que comercialicen, directa o indirectamente, o que en el futuro puedan comercializar. La aceptación del Usuario para que puedan ser cedidos sus datos en la forma establecida en este párrafo, tiene siempre carácter revocable sin efectos retroactivos, conforme a lo que dispone la Ley Orgánica 15/1999, de 13 de diciembre.) 

b. Optar, en caso de incumplimiento de sus obligaciones por el PROVEEDOR, por exigir el debido cumplimiento de las mismas, o por la resolución del contrato, en los términos de la cláusula 8 de este Aviso Legal. 

7. Limitación de responsabilidad

a. SAMSUNG no asume ninguna responsabilidad derivada de la falta de disponibilidad del sistema, fallos de la red o técnicos que provoquen un corte o una interrupción en el Portal. Igualmente SAMSUNG no se hace responsable de los daños y perjuicios que pueda ocasionar la propagación de virus informáticos u otros elementos en el sistema.
b. SAMSUNG no asume ninguna responsabilidad de pagos y/o reclamaciones que deriven de las actuaciones de los clientes en relación al acuerdo entre dicho/s cliente/s y el PROVEEDOR

c. SAMSUNG no asume ninguna responsabilidad sobre cualquier actuación que sea contraria a las leyes, usos y costumbres, o bien que vulnere las obligaciones establecidas en el presente Aviso Legal, por parte de ninguna persona de SAMSUNG, del PROVEEDOR o cualquier usuario de la red. No obstante, cuando SAMSUNG tenga conocimiento de cualquier conducta a la que se refiere el párrafo anterior, adoptará las medidas necesarias para resolver con la máxima urgencia y diligencia los conflictos planteados. 
d. SAMSUNG se exime de cualquier responsabilidad derivada de la intromisión de terceros no autorizados en el conocimiento de las condiciones y circunstancias de uso que los PROVEEDORES hagan del Portal y de los Servicios.


8. Resolución del acuerdo

En caso de incumplimiento de cualquiera de las obligaciones contenidas en el presente Aviso legal, el PROVEEDOR o SAMSUNG habrán de notificar dicho incumplimiento a la parte que lo hubiera cometido, otorgándole un plazo de quince días hábiles, contando desde la notificación, para que lo subsane. Transcurrido dicho plazo sin que se haya producido la subsanación del incumplimiento, la otra parte podrá optar por el cumplimiento o la resolución del presente acuerdo, con la indemnización de daños y perjuicios en ambos casos, de conformidad con lo establecido en el artículo 1.124 del Código Civil. En caso de optar por la resolución, tanto el PROVEEDOR como SAMSUNG aceptan que la simple notificación será suficiente, para que la misma tenga plenos efectos.


9. Duración 

El presente acuerdo tendrá una duración indefinida siempre y cuando no se produzca denuncia por escrito de cualquiera de las partes, con al menos 1 mes de antelación.
El procedimiento de baja será enviando un mail con Asunto “Baja del portal” a la dirección samsung@fullstep.com. En caso de no ser posible el envío del mail, se enviará un fax al 91 296 20 25 indicando los datos básicos para identificar a PROVEEDOR.


10. Notificaciones

a. Las notificaciones entre las partes podrán hacerse por cualquier medio de los admitidos en Derecho, que permita tener constancia de la recepción, incluido el fax.

b. Los cambios de domicilios y faxes serán notificados por escrito y no producirán efectos hasta transcurridos dos días hábiles desde su recepción. En todo caso, el nuevo domicilio y fax será necesariamente dentro del territorio español.

c. Las relaciones entre SAMSUNG y el PROVEEDOR derivadas del uso del Portal y de los servicios prestados a través del mismo, se someterán a la legislación y jurisdicción españolas. 

		  </div>

            <p class="imprimir"><a href="registro_texto.html" target="blank">Imprimir</a> </p>
<div class="caja_acepto">
<form name="frmAlta" id="frmAlta" method="post" style="max-width:740px;">
  <span id="sprycheckbox1">

<span class="checkboxRequiredMsg">Seleccione para continuar</span>
</span>
<br>
<br>

  <input type="checkbox" name="opcion1" id="opcion1" required>

He leído y acepto las condiciones de uso del Portal.</label><br />


<br />
<button type="button" class="bt_registro" id="submit1" name="submit1">Siguiente</button>
<br>
</form>


</div>
<div style="clear:both;"></div>
    	</div>    
<p><a class="back" href="javascript:history.back()">Volver</a></p> </div>
</div>
<script type="text/javascript">
// <!--
// var sprycheckbox1 = new Spry.Widget.ValidationCheckbox("sprycheckbox1");
// //-->
$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked')) {
        $('#frmAlta').submit();
        alta();
	}
	else
       alert("You must accept the sign up contract to this Supplier Portal to continue");
    
});

</script>
                      	</div>
                    </div>
                    
                 
               
   	  </div>
    	</section>
    	  </div>

        </div>
</body>

<!--#include file="../../common/fsal_2.asp"-->

</html>
