﻿<%@ Language=VBScript %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--#include file="../../../common/XSS.asp"-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos.css" rel="stylesheet" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Istok+Web' rel='stylesheet' type='text/css'>
<script src="../js/jquery-1.9.0.js"></script>
<script src="../js/jquery-1.9.0.min.js"></script>
<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=GER", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body>

<div id="cont_gen">
	<div id="izd_gen"> <a href="https://www.segurcaixaadeslas.es" target="_blank"><img src="../images/logo.png" alt="logo-Adeslas" border="0"></a>
    </div>
    <div id="drc_gen">
    	<h1>SOL·LICITAR REGISTRE</h1>
        <div class="int">
        	<p> 
            Per continuar amb el procés d'alta és imprescindible l'acceptació de les  <span class="rojo">CONDICIONS D'ÚS DEL PORTAL DE COMPRES</span>: </p>
            <p style="height:150px; padding:0 5px; overflow:auto; background:#FFF; border:#CCC 1px solid">
               CONDICIONS GENERALS D'ÚS DEL PORTAL DE COMPRES<br/><br/>
Registre de proveïdors<br/><br/>

L'alta com a proveïdor al Portal de compres de SegurCaixa Adeslas està condicionat a la prèvia lectura i acceptació de les següents clàusules . Sense expressar la seva conformitat amb les mateixes no podrà registrar-se. Sempre que s'accedeixi i utilitzi el Portal , s'entendrà que està d'acord, de forma expressa , plena i sense reserves, amb la totalitat del contingut d'aquest Avís Legal. Així mateix , en ser vosté l'Usuari Principal de la seva empresa, queda obligat al acceptar la conformitat d'aquest Avís Legal a vetllar pel seu compliment per part de tots els usuaris que es donin d'alta a la seva empresa , alliberant a SegurCaixa Adeslas de tota responsabilitat per els possibles perjudicis que aquests usuaris poguessin causar a la seva empresa o qualsevol altra causa de les seves actuacions al Portal.<br/><br/>

Clàusules:<br/><br/>

1. Objecte del Portal de compres de SegurCaixa Adeslas<br/><br/>

El portal de compres de SegurCaixa Adeslas és el mitjà a través del qual SegurCaixa Adeslas es comunica amb els seus proveïdors per sol·licitar- ofertes , documents així com aquella informació comercial que estimi oportuna . Al mateix temps SegurCaixa Adeslas pot utilitzar el portal per remetre- aquella informació que consideri del seu interès .<br/><br/>
SegurCaixa Adeslas actua tant com a comprador directe com de gestor de compres dels seus clients , i actuarà en nom d'ells durant el procés de compra. <br /> <br />

L'ús o accés al Portal i / o als Serveis no atribueixen al proveïdor cap dret sobre les marques , noms comercials o signes distintius que apareguin en el mateix , sent aquests propietat de SegurCaixa Adeslas o de tercers . En conseqüència, els Continguts són propietat intel·lectual de SegurCaixa Adeslas o de tercers , sense que puguin entendersecedidos al proveïdor .<br/><br/>

2. Objecte <br/><br/>

El present acord té per objecte regular les relacions entre SegurCaixa Adeslas i el proveïdor , en tot el relacionat amb l'ús del Portal.<br/><br/>

3. Obligacions del proveïdor <br/><br/>

Són obligacions del proveïdor les següents:<br/><br/>
a. Proporcionar totes les dades siguin necessàries per a l'adequat funcionament del sistema, així com mantenir-les actualitzades , comunicant al més aviat possible tot canvi que es produeixi en els mateixos<br/><br/>
b. Garantir l'autenticitat de les dades facilitades a conseqüència de l'emplenament dels formularis necessaris per a la subscripció dels Serveis. Així mateix , el proveïdor actualitzarà la informació facilitada perquè reflecteixi , en cada moment, la seva situació real . Per tot això, el proveïdor serà l'únic responsable dels danys i perjudicis ocasionats a SegurCaixa Adeslas com a conseqüència de declaracions inexactes o falses .<br/><br/>
c. Guardar absoluta confidencialitat en relació amb tota la informació que es generi en les relacions entre el proveïdor i SegurCaixa Adeslas.<br/><br/>
d. Abstenir-se de qualsevol manipulació en la utilització informàtica del Portal, així com l'ús del mateix amb fins diferents d'aquells per als quals va ser previst. Així mateix , el proveïdor s'abstindrà d'accedir a zones no autoritzades del Portal.<br/><br/>
e. Complir fidelment els seus compromisos en la informació remesa a través del portal . En el cas que el proveïdor no demostri la necessària diligència comercial , o incompleixi les obligacions contretes , SegurCaixa Adeslas es reserva el dret d'excloure temporal o permanentment al proveïdor del portal.<br/><br/>
f. El proveïdor haurà d'indicar només aquells grups de materials, que es refereixin a béns o serveis que, en el moment de l'acceptació de l'acord, el proveïdor comercialitzi , fabriqui o distribueixi i que siguin de l'interès comercial de SegurCaixa Adeslas. <br/><br/>
g. El proveïdor accepta que les ofertes introduïdes al Portal siguin considerades amb el mateix rang i validesa que una oferta enviada per qualsevol altre mitjà tradicional ( carta , fax ) .<br/><br/>
h. El proveïdor s'obliga a fer un ús correcte del Portal i dels Serveis conforme a la llei , el present Avís Legal i la resta de reglaments d'ús i instruccions posats en el seu coneixement , així com amb la moral i els bons costums generalment acceptats i l'ordre públic.<br/><br/>

Per això , s'abstindrà d'utilitzar el Portal o qualsevol dels Serveis amb fins il·lícits , prohibits en el present Avís Legal, lesius dels drets i interessos de tercers .<br/><br/> 
En particular , ia títol merament indicatiu , el proveïdor es compromet a no transmetre , difondre o posar a disposició de tercers informacions , dades , continguts , gràfics , arxius de so i / o imatge , fotografies , enregistraments , programari i , en general , qualsevol classe de material que:<br/><br/>

1.Sigui contrari als drets fonamentals i llibertats públiques reconegudes en la Constitució , Tractats internacionals i lleis aplicables;<br/><br/>
2.Indueixi, inciti o promogui actuacions delictives , contràries a la llei, a la moral i bons costums generalment acceptats oa l' ordre públic;<br/><br/>
3.Sigui fals , inexacte o pugui induir a error , o constitueixi publicitat il·lícita, enganyosa o deslleial; <br/><br/>
4. Estigui protegit per drets de propietat intel·lectual o industrial pertanyents a tercers , sense autorització dels mateixos; <br />

5. vulneri drets a l'honor , a la intimitat oa la pròpia imatge; <br />

6. vulneri les normes sobre secret de les comunicacions; <br />

7. Constitueixi competència deslleial , o perjudiqui la imatge empresarialde SegurCaixa Adeslas o tercers; <br />

8. Estigui afectat per virus o elements similars que puguin danyar o impedir el funcionament adequat del Portal, els equips informàtics o els seus arxius i documents . <br />

4. Drets del proveïdor <br/><br/>

Són drets del proveïdor els següents:<br/><br/>

1. Mantenir una presència constant a la base de dades de SegurCaixa Adeslas , en la seva qualitat de proveïdor donat d'alta. <br /> <br />

2. Rebre peticions d'ofertes en virtut de les normes establertes. <br /> <br />

5. Obligacions de SegurCaixa Adeslas<br/><br/>

Són obligacions de SegurCaixa Adeslas les següents: <br/><br/>
a . Mantenir la informació que consideri oportuna actualitzada , i no és responsable dels errors que es produeixin per força major o cas fortuït . <br />

b . Guardar absoluta confidencialitat de tota la informació relativa al proveïdor : bé la proporcionada per aquest, bé la generada en les relacions entre el proveïdor i SegurCaixa Adeslas <br />

c . Facilitar al proveïdor, en qualsevol moment , la situació de les seves dades a la base de dades , i no facilitar-la a tercers si no és per permetre al proveïdor la presentació de les seves ofertes i la presa en consideració dels seus productes com a alternativa de subministrament . Així mateix , facilitar al proveïdor, en qualsevol moment , la situació de les seves dades a la base de dades , perquè aquest, prèvia notificació per escrit , els modifiqui o elimini . <br /> <br />

6. Drets de SegurCaixa Adeslas<br/><br/>

a. L'Usuari presta el seu consentiment perquè SegurCaixa Adeslas cedeixi les seves dades a les empreses a ella associades o del grup al qual pertany, així com a altres amb les quals conclogui acords amb l'única finalitat de la millor prestació del servei, respectant, en tot cas , la legislació espanyola sobre protecció de les dades de caràcter personal. Així mateix, l'Usuari accepta que SegurCaixa Adeslas o les seves empreses a ella associades, societats filials i participades, li remetin informació sobre qualssevol béns o serveis que comercialitzin, directament o indirectament, o que en el futur puguin comercialitzar. L'acceptació de l'Usuari perquè puguin ser cedides les seves dades en la forma establerta en aquest paràgraf, té sempre caràcter revocable sense efectes retroactius, conforme al que disposa la Llei Orgànica 15/1999, de 13 de desembre.)
<br /> <br />
b. Optar, en cas d'incompliment de les seves obligacions pel proveïdor, per exigir el degut compliment de les mateixes, o per la resolució del contracte, en els termes de la clàusula 8 d'aquest Avís Legal. <br /> <br />

7. Limitació de responsabilitat <br/><br/>

a. SegurCaixa Adeslas no assumeix cap responsabilitat derivada de la falta de disponibilitat del sistema, fallades de la xarxa o tècnics que provoquin un tall o una interrupció en el Portal. Igualment SegurCaixa Adeslas no es fa responsable dels danys i perjudicis que pugui ocasionar la propagació de virus informàtics o altres elements en el sistema. <br />

b. SegurCaixa Adeslas no assumeix cap responsabilitat de pagaments i / o reclamacions que derivin de les actuacions dels clients en relació a l'acord entre aquest / s client / es i el proveïdor <br />

c. SegurCaixa Adeslas no assumeix cap responsabilitat sobre qualsevol actuació que sigui contrària a les lleis, usos i costums, o bé que vulneri les obligacions establertes en el present Avís Legal, per part de cap persona de SegurCaixa Adeslas, del proveïdor o qualsevol usuari de la xarxa . No obstant això, quan SegurCaixa Adeslas tingui coneixement de qualsevol conducta a què es refereix el paràgraf anterior, adoptarà les mesures necessàries per resoldre amb la màxima urgència i diligència els conflictes plantejats. <br />

d. SegurCaixa Adeslas s'eximeix de qualsevol responsabilitat derivada de la intromissió de tercers no autoritzats en el coneixement de les condicions i circumstàncies d'ús que els PROVEÏDORS facin del Portal i dels Serveis. <br /> <br />

8. Resolució de l'acord<br/><br/>

En cas d'incompliment de qualsevol de les obligacions contingudes en el present Avís legal, el proveïdor o SegurCaixa Adeslas hauran de notificar l'incompliment a la part que l'hagi comès , atorgant-li un termini de quinze dies hàbils , comptats des de la notificació , perquè ho esmeni. Transcorregut aquest termini sense que s'hagi produït l'esmena de l'incompliment , l'altra part podrà optar pel compliment o la resolució del present acord , amb la indemnització de danys i perjudicis en ambdós casos, de conformitat amb el que estableix l'article 1.124 del Codi civil . En cas d'optar per la resolució, tant el proveïdor com SegurCaixa Adeslas accepten que la simple notificació serà suficient, perquè aquesta tingui plens efectes. <br/><br/>

9. Durada <br/><br/>

El present acord tindrà una durada indefinida sempre que no es produeixi denúncia per escrit de qualsevol de les parts , amb almenys 1 mes d' antelació.
El procediment de baixa serà enviant un mail amb Assumpte " Baixa del portal" a l'adreça soportecompras@segurcaixaadeslas.es. <br/><br/>

10. Notificacions, Legislació y jurisdicció<br/><br/>

a . Les notificacions entre les parts es poden fer per qualsevol mitjà dels admesos en Dret, que permeti tenir constància de la recepció , inclòs el fax . <br /> <br />

b . Els canvis de domicilis i faxos seran notificats per escrit i no tenen efectes fins transcorreguts dos dies hàbils des de la recepció . En tot cas , el nou domicili i fax serà necessàriament dins del territori espanyol. <br /> <br />

c . Les relacions entre SegurCaixa Adeslas i el proveïdor derivades de l'ús del Portal i dels serveis prestats a través del mateix , se sotmetran a la legislació i jurisdicció espanyoles. <br /> 

			</p>
            <p style="text-align:right"><a href="registro_texto.htm" class="bt_registro" target="blank">Imprimir</a> </p>
<div class="caja_acepto">


<!--<form action="" method="post" style="width:400px;">//-->
<form name="frmAlta" id="frmAlta" method="post" style="width:400px;">
  <span id="sprycheckbox1">
<label>
  <input type="checkbox" name="opcion1" id="opcion1" required>
</label>
<span class="checkboxRequiredMsg">Marqui per continuar.</span></span><span class="rojo">He llegit i accepto </span> les Condicions Generals d'Ús del Portal de Compres, així com les <a href="docs/SCA_Condicions Generals de Compra i Contractacio.pdf">Condicions Generals de Compra i Contratactació de SegurCaixa Adeslas</a>.<br />
<br />

<button type="button" class="bt_registro" id="submit1" name="submit1">Següent</button>
<br>
</form>


</div>
<div style="clear:both;"></div>
    	</div>    
    </div>
</div>
<script type="text/javascript">

$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked') )
       {
        $('#frmAlta').submit();
        alta();}
    else
       alert("Té que acceptar les condicions d'us del portal per poder registrar-se");
    
});

</script>
 			
</body>

</html>
