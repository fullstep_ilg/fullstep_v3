﻿<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<link href="../estilos.css" rel="stylesheet" type="text/css">
<title>Portal de Compres</title>
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
}
-->
</style>
</head>
<script>
dibujaMenu(1)
</script>
<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo m�ximo:0</remarks>*/
function Init() {
    document.getElementById('tablemenu').style.display = 'block';

	MM_preloadImages('../images/icono_docs_sm.gif','../images/icono_docs_sm.gif')
	p = window.top.document.getElementById("frSet")
	vRows = p.rows
	vArrRows = vRows.split(",")
	vRows = vArrRows[0] + ",*,0,0"
	p.rows = vRows

}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">
<div id="inicio-left">
  <h1>Benvingut al Portal de Compres</h1>
  <p>Podeu accedir a les diferents àrees a través de les opcions de menú situades a la part superior.</p>
  <ul>
       <li><b>Qualitat: </b>gestioni els seus certificats de qualitat, No Conformitats i accedeixi a les seves puntuacions de qualitat .</li>
    <li><b>Sol·licituds:</b>  accedeixi a les sol·licituds d'oferta i gestioni-les des d'aquest mateix apartat.</li>
    <li><b>Comandes:</b>  gestioni des d'aquí les comandes que li han realitzat els compradors, segueixi la seva evolució i accedeixi al seu històric.</li>
    <li><b>Les seves dades / la seva companyia:</b>  si ho desitja pot modificar les dades de la seva empresa, usuaris, així com les àrees d'activitat en les que la seva empresa es troba homologada . </li>
  </ul>
  
<p> Si és la primera vegada que va a realitzar una oferta a través del portal , seguiu atentament els següents passos :</p>
  <ol>
  <li><strong>Premi a "sol·licituds" </strong> per veure les peticions d'ofertes que té obertes la seva empresa. </li><br>
  <li><strong> Seleccioneu la sol·licitud d'oferta</strong>  a la qual vulgui respondre , fent clic sobre el codi de la mateixa.</li><br>
  <li><strong> Faci la seva oferta</strong> completant tota la informació necessària : des de l'arbre de navegació que trobarà a la part esquerra , pot desplaçar-se pels diferents apartats que conformen l'oferta. Per introduir els preus haurà d'anar a l'apartat " ítems / preus ". Recordeu introduir el termini de validesa de l'oferta, a l'apartat "Dades Generals de l'oferta" .</li><br>
  <li> <strong>Comuniqui la seva oferta</strong> fent clic sobre el botó d'enviar <IMG height=14 src="../images/sobre_tr.gif" width=30 align=absBottom></li>
 </ol>
</div>
<div id="inicio-right">
<h2>INSTRUCCIONES</h2>
<p>Descarregueu-vos les instruccions sobre com realitzar una oferta , acceptar una comanda , seguiment, etc.</p>
<ul>
<li><a href="docs/FSN_MAN_ATC_Com fer una oferta.pdf" target="_blank">Cóm ofertar</a></li>
<li><a href="docs/FSN_MAN_ATC_Requisits tècnics.pdf" target="_blank">Requisits tècnics</a></li>
<li><a href="docs/FSN_MAN_ATC_Manteniment de dades.pdf" target="_blank">Manteniment de dades</a></li>
</ul>
<div id="img-atc"><img src="../images/imagen-ATC.jpg"></div>
<p>Si té algun dubte o dificultat per operar amb el Portal , poseu-vos en contacte amb nosaltres al telèfon 912 900 227 o per mail <br> <a href="mailto:soporte@compras.segurcaixaadeslas.es">soporte@compras.segurcaixaadeslas.es</a></p>
</div>

</body>
</html>
