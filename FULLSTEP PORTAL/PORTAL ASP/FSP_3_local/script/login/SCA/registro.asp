﻿<%@ Language=VBScript %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <!--#include file="../../common/acceso.asp"-->
        <!--#include file="../../common/idioma.asp"-->
        <!--#include file="../../common/formatos.asp"-->
        <!--#include file="../../common/fsal_1.asp"-->

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link href="estilos.css" rel="stylesheet" type="text/css" />
        <link href='https://fonts.googleapis.com/css?family=Istok+Web' rel='stylesheet' type='text/css'>
        <script src="js/jquery-1.9.0.js"></script>
        <script src="js/jquery-1.9.0.min.js"></script>
        <!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
        <link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->

        <script src="../../common/formatos.js"></script>
        <script language="JavaScript" src="../../common/ajax.js"></script>
        <!--#include file="../../common/fsal_3.asp"-->
        <script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
    </head>

    <script>
        function no_alta() {
            window.close()

        }
    </script>

    <script>
        /*
        ''' <summary>
        ''' Lanzar la pantalla de registro de nuevos proveedores de Portal
        ''' </summary>
        ''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
        function alta() {
            setCookie("CONTRATOACEPTADO", 1, new Date())
            window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=SPA", "_blank", "width=730,height=635")

            return true
        }
        function imprimir() {
            window.open("registro_texto.htm", "_blank", "width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

        }

        function setCookie(name, value, expires) {
            //If name is the empty string, it places a ; at the beginning
            //of document.cookie, causing clearCookies() to malfunction.
            document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();

        }

        function clearCookie(name) {
            expires = new Date();
            expires.setYear(expires.getYear() - 1);

            document.cookie = name + '=null' + '; expires=' + expires.toGMTString();
        }
    </script>


    <script language="javascript">< !--

var msg = "Comando incorrecto.";

        function RClick(boton) {
            if (document.layers && (boton.which == 3 || boton.which == 2)) { alert(msg); return false }
            if (document.all && event.button == 2 || event.button == 3) alert(msg)
            return false
        }

        document.onmousedown = RClick

//--></script>


    <body onload="init()">

        <div id="cont_gen">
            <div id="izd_gen">
                <a href="https://www.segurcaixaadeslas.es" target="_blank">
                    <img src="images/logo.png" alt="logo-Adeslas" border="0">
                </a>
            </div>

            <div id="drc_gen">
                <h1>SOLICITAR REGISTRO</h1>
                <div class="int">
                    <p>
                        Para continuar con el proceso de alta es imprescindible la aceptación de las
                        <span class="rojo">CONDICIONES DE USO DEL PORTAL DE PROVEEDORES, así como las CONDICIONES GENERALES DE COMPRA DE SEGURCAIXA
                            ADESLAS</span>.
                    </p>

                    <div style="height:150px; padding:0 5px; overflow:auto; background:#FFF; border:#CCC 1px solid">

 
                        <!-- INICIO TEXTO CONDICIONES GENERALES -->
                        <div style="font-family: Verdana, Geneva, Tahoma, sans-serif;font-size: 12px;">
                            <h2 align="CENTER" style="color:#009DDD">
                                <span lang="es-ES">Condiciones Generales de Compra y Contratación</span>
                            </h2>

                            <div style="margin:20px;"></div>

                            <h4>1. DEFINICIONES</h4>
                            <ul>
                                <li>
                                    SegurCaixa Adeslas: Segurcaixa Adeslas de Seguros y Reaseguros, S.A. y/o cualquier entidad participada por ésta.
                                </li>
                                <li>
                                    Condiciones Particulares: Condiciones aplicables que modifican las presentes condiciones generales.
                                </li>
                                <li>
                                    Contrato: Documento en el que se formalizan las condiciones de contratación de un determinado bien y/o servicio.
                                </li>
                                <li>
                                    Pedido: Documento que se emite para formalizar la petición de un servicio o de un bien.
                                </li>
                                <li>
                                    Plataforma de Compras: Canal de SegurCaixa Adeslas para realizar las comunicaciones relativas a las transacciones comerciales
                                    de forma electrónica con los proveedores.
                                </li>
                                <li>
                                    Proveedor: Persona física o jurídica que acepta la prestación de servicios o suministro de bienes a SegurCaixa Adeslas.
                                </li>
                            </ul>

                            <div style="margin:20px;"></div>


                            <h4>2. AMBITO DE APLICACIÓN</h4>
                            <div style="padding-left: 15px;">
                                <p>
                                    <b>2.1</b>
                                    Las presentes Condiciones Generales de Compra y Contratación (en lo sucesivo, “Condiciones Generales”) serán de aplicación
                                    a las transacciones comerciales realizadas entre el proveedor y SegurCaixa Adeslas, en
                                    ausencia de cualquier contrato negociado. En caso de que el proveedor tenga un contrato
                                    negociado con SegurCaixa Adeslas, prevalecerá lo establecido en el mismo.
                                </p>

                                <p>
                                    <b>2.2</b>
                                    SegurCaixa Adeslas pondrá a disposición del proveedor una copia de las presentes Condiciones Generales en el momento de efectuar
                                    el pedido, pasando a formar parte integrante del mismo.
                                </p>

                                <p>
                                    <b>2.3</b>
                                    Estas Condiciones Generales podrán ser complementadas por unas Condiciones Particulares establecidas específicamente para
                                    uno o varios pedidos concretos efectuados por cualquier entidad del grupo SegurCaixa
                                    Adeslas. En caso de discrepancia entre unas y otras, prevalecerá lo dispuesto en dichas
                                    Condiciones Particulares.
                                </p>

                                <p>
                                    <b>2.4</b>
                                    Cualquier modificación o excepción a estas Condiciones Generales realizada por parte del proveedor deberá ser aceptada, de
                                    forma previa y por escrito, por SegurCaixa Adeslas, y únicamente será de aplicación al
                                    pedido concreto para el que hubiere sido propuesta.
                                </p>
                            </div>

                            <div style="margin:20px;"></div>

                            <h4>3. ACEPTACIÓN DEL PEDIDO Y CONDICIONES DE ENTREGA/EJECUCIÓN.</h4>
                            <div style="padding-left: 15px;">
                                <p>
                                    <b>3.1</b>
                                    El proveedor aceptará en la Plataforma de Compras de SegurCaixa Adeslas el pedido publicado, en un plazo máximo de siete
                                    (7) días naturales desde la fecha de publicación.
                                </p>

                                <p>
                                    <b>3.2.</b>
                                    Tanto la aceptación en la Plataforma de Compras, como la recepción por SegurCaixa Adeslas del pedido por el proveedor, implican
                                    la aceptación expresa del mismo de las presentes Condiciones Generales, así como, en
                                    su caso, de lo dispuesto en las Condiciones Particulares.
                                </p>

                                <p>
                                    <b>3.3.</b>
                                    La simple ejecución del pedido sin la previa aceptación expresa del mismo por el proveedor, de conformidad con lo dispuesto
                                    en los párrafos 3.1 y 3.2 anteriores, implica, del mismo modo, la aceptación de las presentes
                                    Condiciones Generales.
                                    <br /> El pedido efectuado por SegurCaixa Adeslas podrá ser desestimado en caso de no resultar
                                    acorde, total o parcialmente, con lo solicitado inicialmente en el mismo, sin que ello
                                    otorgue derecho al proveedor a exigir indemnización alguna por tal concepto. En este
                                    caso, el proveedor correrá con todos los gastos inherentes a la ejecución del citado
                                    pedido.
                                </p>

                                <p>
                                    <b>3.4.</b>
                                    Cuando así se determine, toda mercancía deberá venir acompañada de su certificado de calidad correspondiente.
                                </p>

                                <p>
                                    <b>3.5.</b>
                                    La entrega de la mercancía o la prestación del servicio deberán realizarse en la fecha y lugar establecidos en el pedido.
                                    En caso contrario, o en caso de entrega o prestación parcial, Segurcaixa Adeslas podrá
                                    optar entre el cumplimiento o la rescisión del pedido, aplicando en ambos casos las indemnizaciones
                                    y penalizaciones, en su caso, establecidas.
                                </p>

                                <p>
                                    <b>3.6.</b>
                                    Hasta el momento de recepción de la mercancía por parte de SegurCaixa Adeslas, los riesgos de pérdida o deterioro de la misma
                                    correrán por cuenta del proveedor.
                                </p>

                                <p>
                                    <b>3.7.</b>
                                    Segurcaixa Adeslas se reserva el derecho a examinar la mercancía, y comprobar que cumple con las condiciones establecidas
                                    en el pedido. En ese caso, el proveedor deberá proveer los medios técnicos y humanos
                                    necesarios para poder realizar dicha verificación.
                                </p>

                                <p>
                                    <b>3.8.</b>
                                    En caso de disconformidad, Segurcaixa Adeslas comunicará al proveedor los defectos encontrados, de forma inmediata, desde
                                    que tuviera conocimiento de los mismos. El proveedor deberá reponer o reparar la mercancía
                                    objeto de devolución en el plazo de 15 días desde su notificación, haciéndose cargo de
                                    todos los gastos de transporte necesarios hasta su completa reparación o reposición en
                                    las dependencias de SegurCaixa Adeslas.
                                </p>

                                <p>
                                    <b>3.9.</b>
                                    En casos de urgencia así como para evitar posibles perjuicios mayores, de índole económica y/o productiva, se faculta a Segurcaixa
                                    Adeslas para subsanar, a cargo del proveedor, los defectos encontrados.
                                </p>

                                <p>
                                    <b>3.10.</b>
                                    El proveedor se hará cargo de todos los gastos en los que Segurcaixa Adeslas pudiera incurrir como consecuencia de deficiencias
                                    en los bienes suministrados o en los servicios prestados por la misma.
                                </p>

                                <p>
                                    <b>3.11.</b>
                                    Toda la mercancía objeto del pedido deberá ser embalada de forma adecuada para su correcto transporte y almacenamiento, a
                                    fin de evitar desperfectos en la misma que, en caso de producirse, serán de cuenta del
                                    proveedor.
                                </p>

                                <p>
                                    <b>3.12.</b>
                                    Todo material que se entregue en las instalaciones de Segurcaixa Adeslas deberá ir acompañado del correspondiente albarán
                                    de la mercancía, en el cual se detallarán los siguientes conceptos: Número de pedido,
                                    Sociedad de Destino, Datos Sociales del proveedor, Fecha de Albarán de entrega, Referencia
                                    de SegurCaixa Adeslas, Unidades Entregadas y Precio. Asimismo, en el caso de que en el
                                    producto figure el Marcado CE, se especificará en el Albarán de Entrega la información
                                    completa del mismo.
                                </p>
                            </div>

                            <div style="margin:20px;"></div>

                            <h4>4. PRECIOS</h4>
                            <div style="padding-left: 15px;">
                                <p>
                                    Los precios señalados en el pedido se entienden fijos e inalterables, salvo pacto escrito en contrario, e incluyen la totalidad
                                    de los bienes o servicios objeto de contratación, así como cualquier gasto que deba soportar
                                    el proveedor para hacer efectivo el suministro o prestación de aquéllos.
                                </p>

                                <p>
                                    Los precios fijados en el pedido sí incluyen el Impuesto sobre el Valor Añadido o Impuesto General Indirecto Canario, según
                                    corresponda.
                                </p>


                                <h4>5. FACTURACIÓN Y ALBARANES</h4>

                                <p>
                                    <b>5.1</b>
                                    Las facturas deberán contener la referencia completa al pedido emitido por Segurcaixa Adeslas, entendiéndose como tal, la
                                    Referencia en factura y la Referencia presupuestaria (Orden SAP - Cuenta Contable), procediéndose
                                    en caso contrario a su devolución.
                                </p>

                                <p>
                                    <b>5.2</b>
                                    Los albaranes de entrega de los Bienes o de la Prestación del Servicio serán firmados por el proveedor en el momento en que
                                    se realice la entrega, de todo o parte de los productos o servicios, en la ubicación
                                    pactada, debiendo incluir, entre otros, la referencia de pedido, fecha del pedido, el
                                    número de paquetes y contenidos.
                                </p>

                                <p>
                                    <b>5.3</b>
                                    El proveedor expedirá una factura por cada uno de los pagos que deba hacerse por el suministro de los Bienes o la Prestación
                                    de los Servicios contenidos en el pedido.
                                </p>
                            </div>

                            <div style="margin:20px;"></div>

                            <h4>6. PAGO</h4>
                            <div style="padding-left: 15px;">
                                <p>
                                    <b>6.1</b>
                                    Las condiciones de pago a aplicar de forma general serán las establecidas por la legislación vigente en cada caso, y en particular
                                    por la Ley de Morosidad, y siempre que se haya recibido por parte del proveedor la correspondiente
                                    factura conforme a los términos del pedido.
                                </p>

                                <p>
                                    <b>6.2</b>
                                    El proveedor deberá entregar a Segurcaixa Adeslas la correspondiente factura el día de la entrega de los bienes o de la prestación
                                    del servicio. En caso de facturación periódica, el proveedor facturará el día siguiente
                                    a la finalización del periodo a facturar. Segurcaixa Adeslas procederá a abonar dicha
                                    factura a los 30 días siguientes a su entrega, coincidiendo con el final de mes.
                                </p>

                                <p>
                                    <b>6.3</b>
                                    El método de pago estándar será por medio de transferencia bancaria.
                                </p>

                                <p>
                                    <b>6.4</b>
                                    Cualquier otra fórmula de pago o variación sobre estas condiciones se expresarán por escrito y serán válidas exclusivamente
                                    en acuerdo puntual para el cual se firman.
                                </p>

                                <p>
                                    <b>6.5</b>
                                    Sin perjuicio de cualquier otro derecho o recurso, Segurcaixa Adeslas se reserva el derecho de compensar cualquier cantidad
                                    adeudada por el proveedor a Segurcaixa Adeslas.
                                </p>

                                <p>
                                    <b>6.6</b>
                                    Si alguna cantidad en virtud del pedido no es atendida a su vencimiento y sin perjuicio de los derechos que puedan ejercer
                                    cada una de las partes, en aplicación de la legislación vigente, el proveedor no tiene
                                    derecho a suspender el suministro de los productos y/o servicios, como resultado de las
                                    sumas adeudadas.
                                </p>
                            </div>

                            <div style="margin:20px;"></div>

                            <h4>7. OBLIGACIONES DEL PROVEEDOR</h4>
                            <div style="padding-left: 15px;">
                                <p>
                                    <b>7.1.</b>
                                    El proveedor se compromete a cumplir y hacer cumplir a sus empleados y, en su caso, a sus contratistas y cesionarios, la
                                    legislación vigente en materia Fiscal, Laboral, de Seguridad Social, de Seguridad y Salud
                                    en el Trabajo y de Medio Ambiente, y cualquier otra de índole legal que le resulte de
                                    aplicación, así como a respetar, en el caso de actividades desarrolladas en las instalaciones
                                    de SegurCaixa Adeslas, las Políticas de Seguridad y Salud en el Trabajo y de Medio Ambiente
                                    adoptadas por las distintas compañías de SegurCaixa Adeslas.
                                </p>

                                <p>
                                    <b>7.2.</b>
                                    A tal efecto, Segurcaixa Adeslas podrá solicitar al proveedor cualquier documentación, de carácter técnico y/o legal, que
                                    pudiere resultar necesaria para la prestación del suministro o servicio, de conformidad
                                    con la legislación vigente en cada momento.
                                </p>

                                <p>
                                    <b>7.3.</b>
                                    Asimismo, de forma previa a la aceptación de un pedido, el proveedor deberá presentar a SegurCaixa Adeslas Certificación
                                    Administrativa, expedida por la Agencia Estatal de Administración Tributaria de España
                                    o del organismo equivalente del país del domicilio social del proveedor, de que éste
                                    se encuentra al corriente de sus obligaciones tributarias (original o fotocopia compulsada).
                                    El proveedor deberá mantener en vigor dicha certificación durante el plazo de suministro
                                    /prestación del servicio contratado.
                                </p>

                                <p>
                                    <b>7.4.</b>
                                    Por otro lado, el proveedor deberá presentar a SegurCaixa Adeslas, en virtud de lo dispuesto en las directivas europeas de
                                    armonización técnica para el suministro y comercialización de determinados productos,
                                    el Certificado de Conformidad CE sobre el producto así como la Declaración de Conformidad
                                    por parte del proveedor, al objeto de poder comprobar el Marcado CE que deberá figurar
                                    tanto en el producto suministrado como en el Albarán de Entrega, junto con toda la información
                                    relativa a dicho Marcado que se deba adjuntar al mismo.
                                </p>

                                <p>
                                    <b>7.5.</b>
                                    Asimismo, el proveedor manifiesta y garantiza que cumple con la legislación laboral y de seguridad social que le resulta
                                    de aplicación, y que tanto él como el resto de las empresas de su Grupo respetan la igualdad
                                    de trato y de oportunidades en el ámbito laboral y, con esta finalidad, deberán adoptar
                                    medidas dirigidas a evitar cualquier discriminación laboral entre mujeres y hombres o
                                    de cualquier otra índole. También manifiesta y garantiza tanto él como el resto de las
                                    entidades de su grupo evitan de forma activa cualesquiera formas de esclavitud moderna
                                    tales como, aunque no limitada a, trata de seres humanos, servidumbre por deudas o el
                                    trabajo forzoso.
                                </p>

                                <p>
                                    <b>7.6.</b>
                                    El proveedor se compromete expresamente a cumplir en su actividad comercial de acuerdo con el Código Ético del Proveedor
                                    de SegurCaixa Adeslas y con los Principios del Pacto Mundial de las Naciones Unidas.
                                </p>

                                <p>
                                    <b>7.7.</b>
                                    El proveedor cumplirá en todo momento con la normativa que le sea de aplicación y desarrollará su actividad comercial según
                                    lo previsto en el presente acuerdo y de forma profesional. El proveedor cumplirá asimismo
                                    con la normativa anticorrupción, antifraude y de prevención del blanqueo de capitales,
                                    que le sea de aplicación y no venderá productos ni prestará servicios a cualquier país,
                                    entidad o persona prohibido por la legislación nacional o internacional aplicable.
                                </p>

                                <p>
                                    <b>7.7.</b>
                                    Cualquier pago efectuado por el proveedor a cualesquiera terceras partes en relación con el cumplimiento de sus obligaciones
                                    con Segurcaixa Adeslas deberá estar debidamente documentado mediante factura escrita,
                                    veraz, correcta, y detallada.
                                </p>

                                <p>
                                    <b>7.8.</b>
                                    Por medio de la presente cláusula, el proveedor manifiesta que dispone en su organización interna de medidas suficientes
                                    de control, prevención y detección de la comisión de cualquier tipo de conducta que pudiera
                                    ser considerada como ilícito penal, cometida con los medios o bajo la cobertura de la
                                    propia compañía y/o a través de cualquier persona física integrante o dependiente de
                                    la misma.
                                    <br /> A los efectos de lo expuesto en el párrafo anterior, el proveedor manifiesta que su
                                    actuación en el ámbito del presente acuerdo comercial estará regida en todo momento por
                                    los principios de la buena fe contractual y convenientemente sujeta a Derecho, de manera
                                    que en ningún momento participará ni colaborará en la comisión de ninguna conducta que
                                    pudiera encontrarse tipificada penalmente en el ordenamiento jurídico.
                                </p>

                                <p>
                                    <b>7.9</b>
                                    El proveedor se compromete expresamente a denunciar en todo momento ante las autoridades policiales y/o judiciales competentes
                                    cualquier conducta que pudiera apreciar tanto en la actuación de las personas físicas
                                    dependientes de la compañía, como de aquellas otras personas físicas y/o jurídicas con
                                    las que mantenga cualquier tipo de relación directa o indirecta como consecuencia de
                                    la ejecución de este acuerdo, y que puedan considerarse delictivas de conformidad con
                                    lo dispuesto en el Código Penal.
                                    <br> En el caso previsto en el párrafo anterior, el proveedor colaborará en lo posible con
                                    las autoridades policiales y/o judiciales, para esclarecer las responsabilidades penales
                                    dimanantes de los hechos denunciados.
                                    <br> Por su parte, el proveedor manifiesta, asimismo, que cuenta con medidas de prevención
                                    y detección de conductas que puedan ser consideradas como ilícitas de conformidad con
                                    lo dispuesto en el Código Penal, por lo que en caso de detectar cualquier conducta de
                                    la referida naturaleza cometida por sus colaboradores y proveedores, lo denunciará ante
                                    las autoridades policiales y/o judiciales competentes.
                                    <br> El ejercicio por cualquiera de las partes contratantes y/o por cualquiera de las personas
                                    físicas integrantes o dependientes de las mismas, de alguna de las conductas que pudieran
                                    ser calificadas como ilícitas y constitutivas de responsabilidad penal, podrá constituir
                                    un incumplimiento contractual y, por tanto, erigirse en causa de resolución del presente
                                    acuerdo, dando lugar a la indemnización que pudiera resultar procedente en concepto de
                                    daños y perjuicios.
                                </p>

                                <p>
                                    <b>7.10.</b>
                                    El incumplimiento de dichas obligaciones o su cumplimiento parcial constituirá causa suficiente para la resolución de la
                                    relación contractual entre las partes.
                                </p>

                            </div>

                            <div style="margin:20px;"></div>

                            <h4>8. SEGUROS</h4>
                            <div style="padding-left: 15px;">
                                <p>
                                    <b>8.1.</b>
                                    Se establece un periodo de garantía estándar de un año para los bienes suministrados, salvo aquellos casos en que se establezca
                                    una plazo superior por ley o por oferta comercial del proveedor, que comenzará a contar
                                    desde la fecha de entrega o ejecución efectivas, comprometiéndose el proveedor a reparar
                                    o sustituir la mercancía defectuosa.
                                    <br /> Este período de garantía se entenderá prorrogado por el tiempo que se dedique en las
                                    correspondientes reparaciones o sustituciones necesarias, que a su vez se garantizarán,
                                    una vez finalizadas, por tiempo igual al periodo de garantía inicial.
                                </p>

                                <p>
                                    <b>8.2.</b>
                                    El proveedor deberá indemnizar por todos los daños y perjuicios, personales o materiales, que, como consecuencia de la ejecución
                                    del pedido, cause a Segurcaixa Adeslas o a terceros o, en su caso, deberá reparar o sustituir
                                    los bienes dañados, siempre que la naturaleza y finalidad de los mismos así lo permitan.
                                </p>

                                <p>
                                    <b>8.3.</b>
                                    En aquellos casos en que sea requerido por SegurCaixa Adeslas, el proveedor deberá presentar una copia de la póliza de seguro
                                    de responsabilidad civil o certificado acreditativo de la misma antes de la aceptación
                                    del pedido. El proveedor deberá mantener en vigor dicha póliza y acreditar dicha vigencia
                                    a SegurCaixa Adeslas durante el plazo de suministro /prestación del servicio contratado.
                                </p>
                                <p>
                                    <b>8.4.</b>
                                    En las Condiciones Particulares o en el propio pedido, y atendiendo al objeto y características propias del mismo, se podrá
                                    fijar la cuantía mínima del seguro antes citado de responsabilidad civil, así como exigir
                                    al proveedor la contratación de seguros adicionales a los citados cuando éstos sean necesarios.
                                </p>
                            </div>

                            <div style="margin:20px;"></div>

                            <h4>9. CESIONES Y SUBCONTRATACIONES</h4>
                            <div style="padding-left: 15px;">
                                <p>
                                    Se prohíbe la cesión o subcontratación del bien o servicio contratado, salvo que sea expresamente autorizado por Segurcaixa
                                    Adeslas al proveedor.
                                </p>
                            </div>

                            <div style="margin:20px;"></div>

                            <h4>10. CONFIDENCIALIDAD</h4>
                            <div style="padding-left: 15px;">
                                <p>
                                    El proveedor se compromete a no revelar ni difundir a terceros la información a la que pueda tener acceso en relación y/o
                                    como consecuencia del cumplimiento y/o desarrollo del pedido, la cual tendrá en todo
                                    momento el carácter de privada y confidencial.
                                    <br /> Asimismo, el proveedor se compromete a destinar dicha información únicamente a los fines
                                    para los que fue facilitada, conforme a lo establecido en las presentes Condiciones Generales
                                    y, en su caso, en las Condiciones Particulares, quedando expresamente prohibida cualquier
                                    comunicación de la misma a terceras personas.
                                    <br /> Segurcaixa Adeslas podrá solicitar en cualquier momento al proveedor la íntegra restitución
                                    de dicha información.
                                </p>
                            </div>

                            <div style="margin:20px;"></div>

                            <h4>11. ACCESO Y TRATAMIENTO DE LOS DATOS PERSONALES</h4>
                            <div style="padding-left: 15px;">
                                <p>
                                    En el supuesto de que el proveedor tuviera que tratar datos de los que es responsable Segurcaixa Adeslas, es de aplicación
                                    la presente cláusula, en los términos siguientes:
                                    <br /> En la ejecución del objeto del presente pedido, el proveedor tendrá la condición de
                                    Encargado del Tratamiento de Datos por cuenta de SegurCaixa Adeslas, que será Responsable
                                    del Tratamiento de Datos. A este fin, el Encargado tratará datos personales en nombre
                                    del Responsable del Tratamiento de Datos y/o del Grupo del Responsable del Tratamiento
                                    de Datos, dado que es necesario el acceso a datos personales por parte del Encargado.
                                    <br /> La finalidad del tratamiento es la prestación de los servicios que son objeto del presente
                                    pedido, para lo cual tendrá acceso a las categorías de datos personales indicadas en
                                    el mismo o en el contrato asociado.
                                    <br /> Mediante las presentes estipulaciones se habilita al Encargado del tratamiento, para
                                    tratar por cuenta del Responsable del fichero y del tratamiento, los datos de carácter
                                    personal necesarios para prestar el servicio. Tanto el Responsable como el Encargado
                                    deberán ajustarse en su actuación, tanto a la presente cláusula como a lo dispuesto en
                                    el Reglamento UE 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016
                                    sobre Protección de Datos Personales de las Personas Físicas (RGPD) y a la normativa
                                    española de protección de datos en vigor.
                                    <br /> El acceso por parte del Encargado del Tratamiento a los datos del Responsable del fichero
                                    y de tratamiento, no se considera comunicación de datos ya que el mismo resulta indispensable
                                    para la prestación de los servicios encargados.
                                    <br /> El Encargado únicamente accederá a los datos personales del Responsable para cumplir
                                    con las obligaciones establecidas para el Encargado en el presente acuerdo, y se compromete
                                    a:
                                </p>

                                <div style="margin:20px;"></div>

                                <ul>
                                    <li>
                                        <b>TRATAR LOS DATOS EXCLUSIVAMENTE PARA LA FINALIDAD DEL ENCARGO</b>
                                        <p>
                                            El encargado deberá utilizar los datos de carácter personal a los que tenga acceso, única y exclusivamente, para cumplir
                                            con sus obligaciones contractuales para con el Responsable, en concreto para
                                            la estricta prestación de los servicios encomendados, configurándose, por tanto,
                                            como Encargado del tratamiento.
                                            <br /> Dichos datos serán los estrictamente necesarios para el cumplimiento de los
                                            servicios y única y exclusivamente podrán ser aplicados o utilizados para el
                                            cumplimiento de los fines acordados, no pudiendo realizarse copia de los mismos,
                                            ni ser trasladados a otra aplicación informática, ni ser cedidos o entregados
                                            a terceros, bajo título alguno, ni siquiera a efectos de mera conservación, a
                                            salvo lo indicado en el punto sobre la posibilidad de subcontratación. En ningún
                                            caso, podrán utilizarse los datos para fines propios del Encargado.
                                        </p>
                                    </li>

                                    <li>
                                        <b>CUMPLIR LAS INSTRUCCIONES DADAS POR EL RESPONSABLE</b>
                                        <p>
                                            Las decisiones que tome el Encargado deben respetar en todo caso las instrucciones dadas por el Responsable del tratamiento,
                                            recogidas en este documento.
                                            <br /> La sujeción a las instrucciones del Responsable deberá producirse igualmente
                                            en el caso de las transferencias internacionales de datos que puedan producirse
                                            como consecuencia de la prestación del servicio. Si el Encargado del tratamiento
                                            está obligado legalmente por el Derecho de la Unión o de un Estado miembro, a
                                            transferir datos a un tercer país deberá informar al Responsable antes de llevar
                                            a cabo el tratamiento, salvo que tal derecho lo prohíba por razones importantes
                                            de interés público.
                                            <br /> A salvo lo dispuesto en el apartado anterior, garantiza que los datos de carácter
                                            personal del Responsable a los que éste les dé acceso serán alojados en servidores
                                            ubicados en España o en cualquier otro país de la Unión Europea, no autorizándose
                                            ningún cambio de ubicación fuera del territorio europeo.
                                            <br /> Asimismo, si el Encargado del tratamiento considera que alguna de las instrucciones
                                            dadas por el Responsable infringe el RGPD o cualquier otra disposición en materia
                                            de protección de datos de la Unión o de los Estados miembros, deberá informar
                                            inmediatamente al Responsable.
                                        </p>
                                    </li>

                                    <li>
                                        <b>DEBER DE CONFIDENCIALIDAD</b>
                                        <p>
                                            El encargado debe garantizar que las personas autorizadas para tratar datos personales se comprometan a respetar, de forma
                                            expresa y por escrito, la confidencialidad o que estén sujetas a una obligación
                                            de confidencialidad de naturaleza estatutaria, y a cumplir las medidas de seguridad
                                            correspondientes, de las que deberá informarles convenientemente.
                                            <br /> Se obliga, asimismo, a mantener permanentemente actualizada la lista de usuarios
                                            que accedan a datos de carácter personal del Responsable y a dar acceso al Responsable
                                            a la mencionada lista siempre que el mismo lo requiera, así como a poner a disposición
                                            del Responsable la documentación acreditativa del cumplimiento de las obligaciones
                                            establecidas en este apartado.
                                        </p>
                                    </li>

                                    <li>
                                        <b>NO COMUNICAR DATOS A TERCEROS</b>
                                        <p>
                                            El Encargado no podrá comunicar datos personales del Responsable a terceros, salvo que cuenten con la autorización expresa
                                            del Responsable, en los supuestos legalmente admisibles.
                                        </p>
                                    </li>

                                    <li>
                                        <b>MEDIDAS DE SEGURIDAD</b>
                                        <p>
                                            Adoptar las medidas de índole técnica y organizativa necesarias, y en especial, las medidas de seguridad establecidas por
                                            el artículo 32 del
                                            <b>RGPD</b>, para garantizar la seguridad presente y futura de los datos de carácter
                                            personal y evitar su alteración, pérdida, tratamiento o acceso no autorizado,
                                            habida cuenta del estado de la tecnología, los costes de aplicación y la naturaleza,
                                            el alcance, el contexto y los fines del tratamiento, así como los riesgos de
                                            probabilidad y gravedad variables para los derechos y libertades de las personas
                                            físicas, ya provengan de la acción humana o del medio físico o natural conforme
                                            a lo establecido en la normativa sobre protección de datos de carácter personal.
                                            <br /> Para ello, se obliga a observar, adoptar y mantener cuantas medidas de seguridad
                                            sean necesarias y adecuadas al riesgo existente; medidas que en su caso incluyan,
                                            entre otras:
                                        </p>
                                        <ul>
                                            <li>
                                                <p>
                                                    La capacidad para garantizar la confidencialidad, secreto e integridad, disponibilidad y resiliencia permanentes de los sistemas
                                                    y servicios de tratamiento de los datos de carácter personal a los que
                                                    tenga acceso.
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    La capacidad de restaurar la disponibilidad y el acceso a los datos personales de forma rápida, en caso de incidente físico
                                                    o técnico.
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Un proceso de verificación, evaluación y valoración, de forma regular, de la eficacia de las medidas técnicas y organizativas
                                                    implantadas para garantizar la seguridad del tratamiento.
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    La seudoanonimización y el cifrado de los datos personales.
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Y, en especial, las que le sean notificadas o le sean impuestas por el Responsable en cualquier momento, mientras permanezca
                                                    vigente el tratamiento de los datos.
                                                </p>
                                            </li>
                                        </ul>
                                        <p>
                                            El Encargado informará al Responsable, a su requerimiento, sobre la implantación efectiva de las medidas de seguridad adoptadas.
                                        </p>
                                    </li>

                                    <li>
                                        <b>REGIMEN DE SUBCONTRATACION</b>
                                        <p>
                                            El Responsable, NO autoriza al Encargado del tratamiento a subcontratar ninguna de las prestaciones que formen parte del
                                            objeto de este acuerdo que comporten el tratamiento de datos personales, salvo
                                            los servicios auxiliares necesarios para el normal funcionamiento de los servicios
                                            del Encargado.
                                            <br/> Si fuera necesario subcontratar algún tratamiento, este hecho se deberá comunicar
                                            previamente por escrito al Responsable o por correo electrónico a la siguiente
                                            dirección:
                                            <a href="mailto:encargadostratamiento@segurcaixaadeslas.es">
                                                <i>encargadostratamiento@segurcaixaadeslas.es</i>
                                            </a>
                                            con una antelación de quince días, indicando los tratamientos que se pretende subcontratar e identificando de forma clara
                                            e inequívoca la empresa subcontratista y sus datos de contacto (sub-encargado).
                                            La subcontratación podrá llevarse a cabo si el Responsable no manifiesta su oposición
                                            en el plazo establecido, diez (10) días hábiles siguientes a la recepción de
                                            la petición de autorización.
                                            <br/> El subcontratista, que también tendrá la condición de encargado del tratamiento,
                                            está obligado igualmente a cumplir las obligaciones establecidas en este documento
                                            para el Encargado del tratamiento y a las instrucciones que dicte el Responsable.
                                            Corresponde al Encargado inicial regular la nueva relación de forma que el nuevo
                                            encargado quede sujeto a las mismas condiciones (instrucciones, obligaciones,
                                            medidas de seguridad, etc.) y con los mismos requisitos formales que él, en lo
                                            referente al adecuado tratamiento de los datos personales y a la garantía de
                                            los derechos de las personas afectadas. En el caso de incumplimiento por parte
                                            del sub-encargado, el Encargado inicial seguirá siendo plenamente responsable
                                            ante el Responsable en lo referente al cumplimiento de las obligaciones.
                                        </p>
                                    </li>
                                    <li>
                                        <b>LOS DERECHOS DE LOS INTERESADOS</b>
                                        <p>
                                            El Encargado debe asistir al Responsable del tratamiento en la respuesta al ejercicio de los derechos de los interesados
                                            establecidos en el capítulo III del
                                            <b>RGPD:</b>
                                        </p>

                                        <ul>
                                            <li>Acceso</li>
                                            <li>Rectificación</li>
                                            <li>Supresión (Derecho al Olvido)</li>
                                            <li>Oposición</li>
                                            <li>Limitación del tratamiento</li>
                                            <li>Portabilidad de los datos</li>
                                            <li>A no ser objeto de decisiones individualizadas automatizadas (incluida la elaboración
                                                de perfiles)</li>
                                        </ul>
                                        <p>
                                            Cuando las personas afectadas ejerzan los derechos antedichos ante el Encargado del tratamiento, éste debe comunicarlo por
                                            correo electrónico a la dirección:
                                            <a href:="mailto:">
                                                <i>lopd@segurcaixaadeslas.es</i>
                                            </a>
                                            La comunicación debe hacerse de forma inmediata y en ningún caso más allá del día laborable siguiente al de la recepción
                                            de la solicitud, juntamente, en su caso, con otras informaciones que puedan ser
                                            relevantes para resolver la solicitud.
                                        </p>
                                    </li>

                                    <li>
                                        <b>DEBER DE INFORMAR A LOS INTERESADOS</b>
                                        <p>
                                            En el momento de la recogida de los datos, en su caso, el Encargado debe facilitar la información relativa de los tratamientos
                                            de datos que se van a realizar a los titulares de los datos. La redacción y el
                                            formato en que se facilitará la información se debe consensuar con el Responsable
                                            antes del inicio de la recogida de datos.
                                        </p>
                                    </li>
                                    <li>
                                        <b>COLABORACION EN LA OBLIGACION DE NOTIFICAR LAS BRECHAS DE SEGURIDAD</b>
                                        <p>
                                            El Encargado del tratamiento notificará al Responsable del tratamiento, sin dilación indebida, y en cualquier caso antes
                                            del plazo máximo de 24 horas a través de correo electrónico, a la siguiente dirección:
                                            <a href="mailto:"></a>incidenciasprivacidad@segurcaixaadeslas.es</i>
                                            </a>. Las violaciones de la seguridad de los datos personales a su cargo de las que
                                            tenga conocimiento, juntamente con toda la información relevante para la documentación
                                            y comunicación de la incidencia, cuando sea probable que la violación suponga
                                            un alto riesgo para los derechos y libertades de las personas físicas.
                                            <br /> La comunicación debe realizarse en un lenguaje claro y sencillo y, si se dispone
                                            de ella, se facilitará, como mínimo, la información siguiente:
                                        </p>
                                        <ol type="a">
                                            <li>
                                                <p>
                                                    Descripción de la naturaleza de la violación de la seguridad de los datos personales, inclusive, cuando sea posible, las
                                                    categorías y el número aproximado de interesados afectados, y las categorías
                                                    y el número aproximado de registros de datos personales afectados.
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    El nombre y los datos de contacto del delegado de protección de datos o de otro punto de contacto en el que pueda obtenerse
                                                    más información.
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Descripción de las posibles consecuencias de la violación de la seguridad de los datos personales.
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Descripción de las medidas de seguridad adoptadas o propuestas para poner remedio a la violación de la seguridad de los datos
                                                    personales, incluyendo, si procede, las medidas adoptadas para mitigar
                                                    los posibles efectos negativos.
                                                </p>
                                            </li>
                                        </ol>
                                        <p>
                                            Si no es posible facilitar la información simultáneamente, y en la medida en que no lo sea, la información se facilitará
                                            de manera gradual sin dilación indebida.
                                            <br /> En aquellos casos en los que la brecha de seguridad afecta exclusivamente al
                                            Encargado, el Responsable podrá atribuir al Encargado la obligación de comunicar
                                            la violación de los datos personales a la Autoridad de control en materia de
                                            protección de datos.
                                        </p>
                                    </li>

                                    <li>
                                        <b>COLABORACION EN LA REALIZACION DE LAS EVALUACIONES DE IMPACTO</b>
                                        <p>El Encargado debe dar apoyo al Responsable del tratamiento en la realización de las
                                            evaluaciones de impacto relativas a la protección de datos, cuando proceda, así
                                            como en la realización de las consultas previas a la Autoridad de Control, en
                                            su caso.
                                        </p>
                                    </li>

                                    <li>
                                        <b>COLABORACION CON EL RESPONSABLE PARA GARANTIZAR EL CUMPLIMIENTO</b>
                                        <p>
                                            El Encargado debe poner a disposición del Responsable toda la información necesaria para demostrar el cumplimiento de sus
                                            obligaciones, así como consentir cuantas inspecciones o auditorias considere
                                            preciso efectuar el Responsable, u otro auditor autorizado por él, en los ficheros
                                            que contengan los datos de carácter personal entregados para la prestación del
                                            servicio encomendado, a fin de acreditar el cumplimiento de lo establecido en
                                            el presente Acuerdo y de la legalidad vigente.
                                        </p>
                                    </li>

                                    <li>
                                        <b>MANTENER EL DEBER DE SECRETO</b>
                                        <p>
                                            El Encargado mantendrá el secreto respecto de los datos de carácter personal a los que haya tenido acceso en virtud del presente
                                            encargo, incluso después de que finalice su objeto.
                                        </p>
                                    </li>

                                    <li>
                                        <b>GARANTIZAR LA FORMACION DE LAS PERSONAS AUTORIZADAS A TRATAR LOS DATOS</b>
                                        <p>
                                            El Encargado debe garantizar la formación necesaria en materia de protección de datos personales de las personas autorizadas
                                            a para tratar datos personales que se encuentren a su cargo.
                                        </p>
                                    </li>

                                    <li>
                                        <b>EL DESTINO DE LOS DATOS UNA VEZ CONCLUIDA LA PRESTACION</b>
                                        <p>
                                            Una vez concluida la prestación de los servicios encomendados que motivó el acceso a los datos, o finalizado el plazo establecido
                                            para el tratamiento de los datos por cuenta del Responsable, el Encargado se
                                            compromete a devolver los datos, así como cualquier copia, soporte o documento
                                            en el que conste cualquier dato de carácter personal objeto de tratamiento, en
                                            el plazo de una semana desde la conclusión de la prestación, salvo que el Responsable
                                            le indique que proceda a su destrucción o autorice a su conservación de forma
                                            expresa y por escrito, si las partes prevén la ejecución de nuevos servicios
                                            que impliquen de forma necesaria el acceso a datos personales del Responsable
                                            por parte del Encargado del tratamiento.
                                            <br /> Una vez destruidos, el Encargado debe certificar su destrucción por escrito
                                            y debe entregar el certificado de destrucción al Responsable del tratamiento.,
                                            <br /> No obstante, el Encargado puede conservar una copia, con los datos debidamente
                                            bloqueados, mientras puedan derivarse responsabilidades derivadas de la ejecución
                                            de la prestación.
                                            <br /> En todo caso, los datos deberán ser devueltos al Responsable cuando se requiera
                                            la conservación de los datos personales, en virtud del Derecho de la Unión o
                                            de los Estados miembros.
                                            <br />

                                            <b>Supuesto específico de las grabaciones de voz (aplicable solamente si se efectúan
                                                grabaciones de voz)</b>
                                            - El Encargado se obliga a grabar todas y cada una de las conversaciones telefónicas que, en su caso, mantenga con clientes
                                            y potenciales clientes del Responsable o de otras personas cuyos datos sean responsabilidad
                                            del Responsable y mantener un archivo de cada llamada, incluso después de finalizado
                                            el contrato, así como, a hacer entrega de las mismas, y, si procede, los soportes
                                            donde consten en cualquier momento a requerimiento del Responsable, y en todo
                                            caso, en el plazo máximo de una semana desde la conclusión de la prestación.
                                            La devolución debe comportar el borrado total de los datos existentes en los
                                            equipos informáticos utilizados por el Encargado.
                                            <br /> No obstante, el Encargado puede conservar una copia, con los datos debidamente
                                            bloqueados, mientras puedan derivarse responsabilidades de la ejecución de la
                                            prestación.
                                        </p>
                                    </li>

                                    <li>
                                        <b>LLEVANZA DE UN REGISTRO DE ACTIVIDADES DE TRATAMIENTO</b>
                                        <p>
                                            En caso de estar obligado, el Encargado deberá llevar, por escrito, un registro de todas las actividades de tratamiento efectuadas
                                            por cuenta del Responsable, que contenga:
                                        </p>

                                        <ul>
                                            <li>
                                                1. El nombre y los datos de contacto del Encargado y de cada Responsable por cuenta del cual actúe el Encargado y, en su
                                                caso, del representante del Responsable o del Encargado y del delegado de
                                                protección de datos.
                                            </li>
                                            <li>
                                                2. Las categorías de tratamientos efectuados por cuenta de cada Responsable.
                                            </li>
                                            <li>
                                                3. En su caso, las transferencias de datos personales a un tercer país u organización internacional, incluida la identificación
                                                de dicho tercer país u organización internacional y, en el caso de las transferencias
                                                indicadas en el artículo 49 apartado 1, párrafo segundo del RGPD, la documentación
                                                de garantías adecuadas.
                                            </li>
                                            <li>
                                                4. Una descripción general de las medidas técnicas y organizativas de seguridad relativas a:
                                            </li>
                                            <li>
                                                a) La seudoanonimización y el cifrado de datos personales.
                                            </li>
                                            <li>
                                                b) La capacidad de garantizar la confidencialidad, integridad, disponibilidad y resiliencia permanentes de los sistemas y
                                                servicios de tratamiento.
                                            </li>
                                            <li>
                                                c) La capacidad de restaurar la disponibilidad y el acceso a los datos personales de forma rápida, en caso de incidente físico
                                                o técnico.
                                            </li>
                                            <li>
                                                d) El proceso de verificación, evaluación y valoración regulares de la eficacia de las medidas técnicas y organizativas para
                                                garantizar la seguridad del tratamiento.
                                            </li>
                                        </ul>
                                        <div style="margin:20px;"></div>
                                    </li>

                                    <li>
                                        <b>DESIGNACION, EN SU CASO, DE UN DELEGADO DE PROTECCION DE DATOS</b>
                                        <p>
                                            El Encargado se obliga a designar, si se dan las condiciones establecidas en la normativa sobre protección de datos para
                                            ello, a un Delegado de Protección de Datos y comunicar su identidad y datos de
                                            contacto al Responsable.
                                        </p>
                                    </li>
                                </ul>


                                <p>
                                    En el caso de que la Agencia Española de Protección de Datos o cualquier otra Autoridad Administrativa, se dirigiese al Encargado
                                    en relación a este Acuerdo, el Encargado lo pondrá inmediatamente en conocimiento del
                                    Responsable.
                                    <br /> El Encargado será responsable de cuantas sanciones, multas o reclamaciones por daños
                                    y perjuicios se deriven del incumplimiento de lo anteriormente expuesto a lo largo de
                                    este acuerdo, y resarcirá al Responsable de los importes que por tal motivo hubiera tenido
                                    que abonar éste, incluidos gastos jurídicos, extrajudiciales y costas que la defensa
                                    del Responsable ocasionare.
                                    <br /> Sin perjuicio de lo anterior, el incumplimiento por el Encargado de cualquiera de las
                                    obligaciones contenidas en la presente cláusula facultará al Responsable para resolver
                                    el “Contrato” del que esta cláusula forma parte integrante, sin perjuicio de su derecho
                                    a exigir los daños y perjuicios que se le hubiesen ocasionado.
                                    <br /> Las obligaciones establecidas para el Encargado en el presente acuerdo, serán también
                                    de obligado cumplimiento para sus empleados y colaboradores, tanto externos como internos,
                                    por lo que el Encargado responderá frente al Responsable si tales obligaciones son incumplidas
                                    por tales empleados y/o colaboradores.
                                    <br /> A tal efecto, el Encargado informará a su personal y colaboradores de las obligaciones
                                    establecidas en el presente Acuerdo sobre confidencialidad, así como de las obligaciones
                                    relativas al tratamiento de datos de carácter personal. El Encargado realizará cuantas
                                    advertencias y suscribirá cuantos documentos sean necesarios con su personal y colaboradores,
                                    con el fin de asegurar el cumplimiento de tales obligaciones.
                                </p>

                            </div>

                            <div style="margin:20px;"></div>

                            <h4>12. FINALIZACIÓN</h4>
                            <div style="padding-left: 15px;">
                                <p>
                                    <b>12.1.</b>
                                    El acuerdo comercial se extinguirá por su vencimiento o bien por resolución anticipada del mismo.
                                </p>
                                <br> Segurcaixa Adeslas tendrá la facultad de resolver de forma anticipada el acuerdo en los
                                siguientes casos:
                                <p>
                                    <b>-</b>El incumplimiento por el proveedor de la legislación vigente y, en especial, de
                                    las obligaciones laborales, sociales o fiscales relativas al personal destinado a la
                                    ejecución del pedido.
                                </p>
                                <p>
                                    <b>-</b>El incumplimiento de las presentes Condiciones Generales o de los demás documentos
                                    que forman parte del pedido, considerando como tal incumplimiento, el retraso injustificado
                                    en la ejecución del suministro o servicio objeto del pedido.
                                </p>
                                <p>
                                    <b>-</b>La extinción de la personalidad jurídica del proveedor o la venta o transmisión
                                    de la compañía o su transformación en otra entidad jurídica, por los medios legalmente
                                    establecidos, sin previo consentimiento, por escrito, de SegurCaixa Adeslas.
                                </p>
                                <p>
                                    <b>-</b>La cesión del contrato, en todo o en parte, sin la previa autorización, expresa
                                    y por escrito, de SegurCaixa Adeslas.
                                </p>
                                <p>
                                    <b>-</b>La solicitud de declaración de situación concursal del proveedor.
                                </p>
                                <p>
                                    <b>-</b>El mutuo acuerdo entre las partes.
                                </p>
                                <p>
                                    <b>-</b>El cumplimiento defectuoso en la ejecución del suministro. La resolución del contrato
                                    por dichas causas no llevará aparejada indemnización alguna para la parte incumplidora.
                                </p>

                                <p>
                                    <b>12.2.</b>
                                    En los supuestos de resolución anticipada, Segurcaixa Adeslas podrá reclamar los bienes o servicios sin más condición que
                                    el pago correspondiente a los trabajos efectivamente realizados hasta la fecha de la
                                    resolución, tras el cual el proveedor efectuará la entrega de la mercancía de forma inmediata.
                                </p>
                            </div>

                            <div style="margin:20px;"></div>

                            <h4>13. DEPÓSITO DE MATERIALES</h4>
                            <div style="padding-left: 15px;">
                                <p>
                                    <b>13.1.</b>
                                    En el caso de compra de útiles y/o maquetas, que permanezcan en las instalaciones del proveedor, éstos estarán sometidos
                                    a un régimen de depósito, siendo Segurcaixa Adeslas el propietario de los mismos, en
                                    el estado en que se encuentren, incluyendo los proyectos de los mismos.
                                </p>

                                <p>
                                    <b>13.2.</b>
                                    El depositario, cuando sean puestos a su disposición los útiles que se le consignen, deberá cuidar de ellos con la diligencia
                                    conveniente, siendo responsable de cualquier daño o merma que, por cualquier causa, pudieran
                                    producirse en los mismos. Deberá guardarlos y conservarlos en el lugar referido en el
                                    punto anterior, sin que puedan ser trasladados del lugar sin autorización, expresa y
                                    por escrito, de SegurCaixa Adeslas.
                                </p>

                                <p>
                                    <b>13.3.</b>
                                    En el caso de declaración concursal del depositario, éste se verá obligado, en el supuesto de que se pretenda practicar un
                                    embargo sobre los utillajes objeto del depósito, a efectuar la pertinente protesta y
                                    a adoptar cuantas medidas fuesen necesarias para hacer valer la propiedad de Segurcaixa
                                    Adeslas sobre dichos bienes, sin perjuicio de las acciones que la propia Segurcaixa Adeslas
                                    pudiera ejercitar en defensa de sus intereses. En todo caso, el depositario comunicará
                                    de forma inmediata al depositante dicha situación, para que ésta pueda realizar la oportuna
                                    defensa de sus derechos, siendo de cuenta del depositario los gastos que se pudieran
                                    producir y que se deriven de las referidas situaciones, y la responsabilidad por daños
                                    y perjuicios que en su caso ocasionare la emisión de dicha comunicación.
                                </p>
                            </div>
                            <div style="margin:20px;"></div>

                            <h4>14. RÉGIMEN JURÍDICO</h4>
                            <div style="padding-left: 15px;">
                                <p>
                                    Ambas partes, con renuncia expresa a cualquier legislación o fuero que pudiera corresponderles, se someten a la legislación
                                    común española y a la jurisdicción de los juzgados y tribunales de la ciudad de Madrid,
                                    para la resolución de cualquier controversia que pudiera surgir relacionada con la interpretación,
                                    la aplicación y/o la ejecución del presente contrato.
                                </p>
                            </div>
                        </div>
                        <!-- FINE TEXTO -->







                    </div>

                    <p style="text-align:right">
                        <a href="registro_texto.htm" class="bt_registro" target="blank">Imprimir</a>
                    </p>

                    <div class="caja_acepto">
                        <form name="frmAlta" id="frmAlta" method="post" style="width:400px;">
                            <span id="sprycheckbox1">
                                <label>
                                    <input type="checkbox" name="opcion1" id="opcion1" required>
                                </label>
                                <span class="checkboxRequiredMsg">Seleccione para continuar.</span>
                            </span>
                            <p>
                                <span class="rojo">He leído y Acepto</span> las Condiciones Generales de Uso del Portal de Proveedores, así
                                como las
                                <a href="spa/docs/SCA_Condiciones Generales de Compra y Contratacion.pdf">Condiciones generales de compra y contratación de SegurCaixa Adeslas</a>.
                            </p>

                            <br />
                            <button type="button" class="bt_registro" id="submit1" name="submit1">Siguiente</button>
                            <br>
                        </form>
                    </div>

                    <div style="clear:both;"></div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $('#submit1').on('click', function () {
                if ($('#opcion1').is(':checked')) {
                    $('#frmAlta').submit();
                    alta();
                }
                else
                    alert("Debe aceptar las condiciones de uso del portal para poder registrarse");

            });
        </script>

    </body>

    <!--#include file="../../common/fsal_2.asp"-->

    </html>