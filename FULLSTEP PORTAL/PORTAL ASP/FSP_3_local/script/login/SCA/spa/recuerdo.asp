﻿<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos.css" rel="stylesheet" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Istok+Web' rel='stylesheet' type='text/css'>
<script src="../js/jquery-1.9.0.js"></script>
<script src="../js/jquery-1.9.0.min.js"></script>
<!--<script type="text/javascript" src="http://fast.fonts.net/jsapi/f5dbd368-355c-4f28-bc2f-217b51498d80.js"></script>!-->
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body>
    <div id="cont_gen">
        <div id="izd_gen">
           <a href="https://www.segurcaixaadeslas.es" target="_blank"><img src="../images/logo.png" alt="logo-Adeslas" border="0"></a>
        </div>
        <div id="drc_gen">
            <div class="int">
                <p>
                    Si ha olvidado su contraseña, introduzca su código de compañía, de usuario y la dirección de e-mail de su cuenta de acceso y pulse "Enviar". Recibirá un e-mail con el enlace a la página donde podrá crear una nueva contraseña.
                </p>
                <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
                    <table>
                    <tr>
                        <td>Cód. Compañía:</td>
                        <td><input type="text" name="txtCia" id="txtCia" size="30" maxlength="30" autocomplete="off"/></td>
                    </tr>
                    <tr>
                        <td>Cód. Usuario:</td>
                        <td><input type="text" name="txtUsu" id="txtUsu" size="30" maxlength="30" autocomplete="off"/></td>
                    </tr>
                    <tr>
                        <td>Dirección de email:</td>
                        <td><input type="text" name="txtEmail" id="txtEmail" size="30" maxlength="45" autocomplete="off"/></td>
                    </tr>
                </table>
                    <br /><input class="bt_registro" name="cmdEnviar" type="submit" style="height:22px; padding:0;" value="Enviar" />
                    <input type="hidden" name="idioma" value="spa"/>
                </form>
                            <div style="clear:both;"></div>

                <div class="recordar-claves-2">
                    <span style="display:block;">Si continúa con dificultades para acceder al Portal, póngase en contacto con el <strong>Servicio de atención a proveedores.</strong></span>
                    <span class="rojo_24" style="display:block;"><strong>912 900 227 </strong></span>
                    <br />
                </div>
            </div>
        </div>
    </div>
</body>

</html>
