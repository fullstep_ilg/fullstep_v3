﻿<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader "Pragma", "no-cache" %>
<% Response.Expires = -1 %> 
<% Response.Charset = "UTF-8"%>
<!--#include file="../common/XSS.asp"-->
<!--#include file="../common/idioma.asp"-->


<%		
''' <summary>
''' Cargar en cookies todo lo q necesita menu.asp y otras varias pantallas. Son datos sobre el usuario, si tiene 
'''	acceso a, por ejemplo, certificados, etc.
''' Al final llama a la pantalla de inicio si el usuario es valido.
'''	Las personalizaciones llaman a esta, por lo q se controla el XSS sin tocar las personalizaciones.
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: default.asp (de todas las instalaciones) ; Tiempo máximo:0 </remarks>
Idioma = Request("Idioma")
Idioma = trim(Idioma)
if Idioma="" then
	Idioma = "SPA"
end if

den1=devolverTextos(Idioma,1 )
avisoPopup=den1(12)

%>
<script type="text/javascript" src="<%=application("RUTASEGURA")%>script/common/formatos.js"></script>
<script type="text/javascript" src="<%=application("RUTASEGURA")%>script/js/jquery.min.js"></script>
<script type="text/javascript" src="<%=application("RUTASEGURA")%>script/politicacookies/js/politicacookies.js"></script>
<script>
    $(document).ready(function () {
        $("#frmLogin").submit(function (event) {
            if (!areCookiesEnabled()) {
                event.preventDefault();
                location.href = '<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=<%=Idioma%>';
            } else {
                frmLogin.submit;
            }
        });

        
    });

    function trim(sValor) {
        var re = / /

        var i

        var sTmp = sValor

        while (sTmp.search(re) == 0)
            sTmp = sTmp.replace(re, "")

        var sRev = ""
        for (i = 0; i < sTmp.length; i++)
            sRev = sTmp.charAt(i) + sRev


        while (sRev.search(re) == 0)
            sRev = sRev.replace(re, "")

        sTmp = ""
        for (i = 0; i < sRev.length; i++)
            sTmp = sRev.charAt(i) + sTmp

        return (sTmp)

    }

    var windowOpen = window.open;
    //override de window open para que el popup se abra centrado
    //y advierta si está activado el bloqueo de popup
    window.open = function (url, name, features) {
        var params = new Array();
        //extraer especificaciones
        if (features) {
            var specArr = features.split(",");
            for (i = 0; i < specArr.length; i++) {
                var spec = specArr[i];
                var key = trim(spec.split("=")[0]);
                var value = trim(spec.split("=")[1]);
                params[key] = value;
            }
            if ((params["width"] || params["height"])) {
                if (params["left"] == undefined) params["left"] = (screen.width - (params["width"] == undefined ? (screen.width / 2) : params["width"])) / 2;
                if (params["top"] == undefined) params["top"] = (screen.height - (params["height"] == undefined ? (screen.height / 2) : params["height"])) / 2;
            }
        }
        features = "";
        for (i in params) {
            features += i + "=" + params[i] + ",";
        }
        features = features.substring(0, features.length - 1);
        var popUp = windowOpen(url, name, features);
        //Tarea 3301 : Si el bloqueo de popups está activado mostramos una alerta ya que no se van a poder abrir
        if (popUp == null || typeof (popUp) == 'undefined') {
			if (navigator.userAgent.toLowerCase().indexOf('safari/') > -1)
            	alert(sBloqueoPopupActivado);
            return false;
        }
        popUp.focus();
        return popUp;
    };

    function windowopen(url, name, specs) {
        return window.open(url, name, specs);
    }
function windowopen_UsuNoValido(url, name, specs) {
    if ("<%=application("LOGIN_EXT_VENT_NUEVA")%>" == 1){
        window.location.href = url;
    } else {
        return window.open(url, name, specs);
    }     
}     
</script>



<%
IdOrden=Request("IdOrden") 
CiaComp=Request("CiaComp") 

IPDir=Request.ServerVariables("LOCAL_ADDR")

dim IsSecure
IsSecure = left(Application ("RUTASEGURA"),8)
if IsSecure = "https://" then                       
    IsSecure = "; secure"
else
    IsSecure =""
end if

Response.AddHeader "Set-Cookie","USU_CADPASSWORD=0; path=/; HttpOnly" + IsSecure
if Request.ServerVariables("CONTENT_LENGTH")>0 or (Request.ServerVariables("CONTENT_LENGTH")=0 and Session.Contents.Item("DesdeCondiciones")=1) then         
    if Request.ServerVariables("CONTENT_LENGTH")>0 then
        txtCIA=request("txtCIA")
        txtUSU=request("txtUSU")
        txtPWD=request("txtPWD")
    else
        'Caso de que se entre en esta página procedente de condicionesAceptacion.asp. El usuario ya habrá metido previamente losd atos de login y en condicionesAceptacion.asp se han guardado en session.contents.
        txtCIA=session.contents.item("txtCIA")
        txtUSU=session.contents.item("txtUSU")
        txtPWD=session.contents.item("txtPWD")

        session.contents.remove("txtCIA")
        session.contents.remove("txtUSU")
        session.contents.remove("txtPWD")
        Session.Contents.remove("DesdeCondiciones")
    end if

    UsuSesionId=Request.Cookies("USU_SESIONID")
    PersistID=Request.Cookies("SESPP")

	set oRaiz = createobject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
	i = oraiz.Conectar(Application("INSTANCIA"))
    path = application("RUTASEGURA")
	if i = 1 then
		Response.Redirect application("RUTASEGURA") & "script/imposibleconectar.asp?Idioma=" & Idioma
	else
        If IdOrden="" then
            set Sesion = oraiz.ValidarLogin (txtCIA,txtUSU,txtPWD,cint(Application("PORTAL")),Application("PYME"),false, _
                                            Application("APROVISIONAMIENTO"),Application("SOLICITUDESPM"),Application("CERTIFICADOS"),Application("NOCONFORMIDADES"), _
                                            Application("CN"),IPDir,PersistID,UsuSesionId,Application("NOMPORTAL"))		
		else
            set Sesion = oraiz.ValidarLogin (txtCIA,txtUSU,txtPWD,cint(Application("PORTAL")),Application("PYME"),false, _
                                            Application("APROVISIONAMIENTO"),Application("SOLICITUDESPM"),Application("CERTIFICADOS"),Application("NOCONFORMIDADES"), _
                                            Application("CN"),IPDir,PersistID,UsuSesionId,Application("NOMPORTAL"),CiaComp)		
		end if       
        'Si la contraseña esta caducada redireccionamos a pantalla de cambio de contraseña
        Response.AddHeader "Set-Cookie","USU_CADPASSWORD=0; path=/; HttpOnly" + IsSecure   
        select case oraiz.UsuarioYaLogeado
            case -1
                Response.AddHeader "Set-Cookie","USU_IDIOMA=" & oraiz.IdiomaUsuario & "; path=/; HttpOnly" + IsSecure 
                Response.AddHeader "Set-Cookie","USU_CADPASSWORD=1; path=/; HttpOnly" + IsSecure
                'Ponemos esto porque lo otro no funciona debido al redireccionamiento al puerto 8080 de Ormazabal
		        'en gestamp si que sale por otro puerto
		        if Application("PUERTO80")=0 then
			        Response.Redirect "/script/login/inicio.asp?u=" & txtUSU & "&c=" & txtCIA
		        else
			        Response.Redirect path & "script/login/inicio.asp?u=" & txtUSU & "&c=" & txtCIA
		        end if
            case -2 'EL USUARIO HA INTENTADO ACCEDER A LA PLATAFORMA CON UNAS CREDENCIALES INCORRECTAS%>
                <script type="text/javascript" language="javascript">
                    windowopen_UsuNoValido("<%=application("RUTASEGURA")%>script/usuarionovalido.asp?Idioma=<%=Idioma%>&ErrorSesion=0&bloqueo=<%=oraiz.NumeroIntentosRestanteBloqueo%>", "_blank", "width=450,height=240,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no")
                </script>
            <%case -3 'EL USUARIO ESTA BLOQUEADO%>
                <script type="text/javascript" language="javascript">
                    windowopen_UsuNoValido("<%=application("RUTASEGURA")%>script/usuarionovalido.asp?Idioma=<%=Idioma%>&ErrorSesion=0&bloqueo=-1", "_blank", "width=450,height=240,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no")
                </script>
            <%case -4 'Hay condiciones de aceptación y el usuario no las ha aceptado 
                'Se pone la ruta relativa porque si se pone completa (con application("RUTASEGURA")) da un error de servidor
                server.transfer "/script/common/condicionesAceptacion.asp"                                     
            case else                                              	
		        if Sesion.Id ="" then 
                    if oraiz.UsuarioYaLogeado = 0 then
			            if IdOrden="" Then%>
				            <script type="text/javascript" language="javascript">
				                //Usuario-contraseÃ±a incorrecta
				                windowopen_UsuNoValido("<%=application("RUTASEGURA")%>script/usuarionovalido.asp?Idioma=<%=Idioma%>&ErrorSesion=0", "_blank", "width=450,height=240,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no")
				            </script>
			            <%
			            else
				            set oCompania = oRaiz.devolverDatosCia (CiaComp, Application("PORTAL"), false)
			            %>
				            <script type="text/javascript" language="javascript">
					            windowopen_UsuNoValido ("<%=application("RUTASEGURA")%>script/usuarionovalido.asp?Idioma=<%=Idioma%>&NomCiaComp=<%=oCompania.Den%>&ErrorSesion=0","_blank","width=400,height=1000,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no")
				            </script>
				            <%set oCompania=nothing%>
			            <%end if
                    else 
                        IF IdOrden="" Then%>
				            <script type="text/javascript" language="javascript">
					            //Se ha borrado la cookie habiendo una sesiÃ³n activa
					            windowopen_UsuNoValido ("<%=application("RUTASEGURA")%>script/usuarionovalido.asp?Idioma=<%=Idioma%>&ErrorSesion=1","_blank","width=600,height=900,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no,scrollbars=yes")
				            </script>                   
			            <%
			            else
				            set oCompania = oRaiz.devolverDatosCia (CiaComp, Application("PORTAL"), false)
			            %>
				            <script type="text/javascript" language="javascript">
				                windowopen_UsuNoValido("<%=application("RUTASEGURA")%>script/usuarionovalido.asp?Idioma=<%=Idioma%>&NomCiaComp=<%=oCompania.Den%>&ErrorSesion=1", "_blank", "width=400,height=240,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no")
				            </script>
				            <%set oCompania=nothing%>
                        <%end if%>			
                    <%end if%>
		        <%else                   
                    'En la cookie USU se escribirá el ID de sesión generado y los datos actuales MOSTRARFMT, THOUSANFMT, DECIMALFMT, PRECISIONFMT, DATEFMT, IDIOMA, TIPOMAIL
                    Response.AddHeader "Set-Cookie","USU_SESIONID=" &  server.URLEncode(Sesion.Id) & "; path=/; HttpOnly" + IsSecure
                    Response.AddHeader "Set-Cookie","USU_USUID=" &  server.URLEncode(Sesion.UsuId) & "; path=/; HttpOnly" + IsSecure
                    Response.AddHeader "Set-Cookie","USU_CIAID=" &  server.URLEncode(Sesion.CiaId) & "; path=/; HttpOnly" + IsSecure
                    Response.AddHeader "Set-Cookie","USU_MOSTRARFMT=" &  Sesion.MostrarFMT  & "; path=/; HttpOnly" + IsSecure
                    Response.AddHeader "Set-Cookie","USU_DECIMALFMT=" & Sesion.DecimalFMT  & "; path=/; HttpOnly" + IsSecure
                    Response.AddHeader "Set-Cookie","USU_THOUSANFMT=" & Sesion.ThousanFMT  & "; path=/; HttpOnly" + IsSecure
                    Response.AddHeader "Set-Cookie","USU_DATEFMT=" & Sesion.DateFMT  & "; path=/; HttpOnly" + IsSecure
                    Response.AddHeader "Set-Cookie","USU_PRECISIONFMT=" & Sesion.PrecisionFMT  & "; path=/; HttpOnly" + IsSecure
                    Response.AddHeader "Set-Cookie","USU_TIPOMAIL=" & Sesion.TipoMail  & "; path=/; HttpOnly" + IsSecure
                    Response.AddHeader "Set-Cookie","USU_IDIOMA=" & Sesion.Idioma  & "; path=/; HttpOnly" + IsSecure                    
                    Response.AddHeader "Set-Cookie","ACCESO_EXTERNO=" & Application("REGISTRO_EXTERNO") &"; path=/; HttpOnly" + IsSecure
					If Sesion.Parametros.EmpresaPortal Then
				        Response.AddHeader "Set-Cookie","EMPRESAPORTAL=1; path=/; HttpOnly" + IsSecure
					Else
				        Response.AddHeader "Set-Cookie","EMPRESAPORTAL=0; path=/; HttpOnly" + IsSecure
					End If
                    Response.AddHeader "Set-Cookie","NOM_PORTAL=" & Application("NOMPORTAL") & "; path=/; HttpOnly" + IsSecure
                    %>
                    <script type="text/javascript" >
                    /****
                    /*Rellena el tÃ­tulo segÃºn idioma seleccionado por el usuario:
                    /*En la pÃ¡gina de login si no estÃ¡ logeado
                    /*En el idioma seleccionado por el usuario en la aplicaciÃ³n si estÃ¡ logeado
                    **/
                    $('document').ready(function () {
                        <% 
                            titulo="TITULOVENTANAS_" & Sesion.Idioma
                        %>
                        document.title = '<%=application(titulo) %>';
                    });
                    </script>
                    <%
                    Response.AddHeader "Set-Cookie","USU_CSRFTOKEN=""; path=/; HttpOnly" + IsSecure

                    if Sesion.Parametros.unaCompradora then
				        application("UNACOMPRADORA") = 1
				        application("IDCIACOMPRADORA") = Sesion.Parametros.IdCiaCompradora
			        else
				        application("UNACOMPRADORA") = 0
                    end if

                    'El Id generado para la cookie persistente se almacenará en una cookie de nombre SESPP (sesión portal persistente) a la que pondremos una caducidad de 24 horas.                                        
                    Fecha=Date+1

                    sDia=cstr(day(Fecha))
                    sUrte = cstr(year(Fecha))

                    sDia = left("00",2-len(sDia)) & sDia
                                    
                    select case Weekday(Fecha)
                    case 1:
                        sDiaWeek = "Sun, "
                    case 2:
                        sDiaWeek = "Mon, "
                    case 3: 
                        sDiaWeek = "Tue, "
                    case 4: 
                        sDiaWeek = "Wed, "
                    case 5: 
                        sDiaWeek = "Thu, "
                    case 6:
                        sDiaWeek = "Fri, "
                    case 7:
                        sDiaWeek = "Sat, "
                    end select 

                    select case month(Fecha)
                    case 1:
                        sMes= "Jan"
                    case 2:
                        sMes= "Feb"
                    case 3:
                        sMes= "Mar"
                    case 4:
                        sMes= "Apr"
                    case 5:
                        sMes= "May"
                    case 6:
                        sMes= "Jun"
                    case 7:
                        sMes= "Jul"
                    case 8:
                        sMes= "Aug"
                    case 9:
                        sMes= "Sep"
                    case 10:
                        sMes= "Oct"
                    case 11:
                        sMes= "Nov"
                    case 12:
                        sMes= "Dec"
                    end select
                    
                    VisualizacionFecha = sDiaWeek + sDia + "-" +  sMes + "-" + sUrte + " 00:00:00 GMT"                                                                      

                    Response.AddHeader "Set-Cookie","SESPP=" & server.URLEncode(Sesion.PersistID)  & "; path=/; HttpOnly; Expires=" + VisualizacionFecha + IsSecure


			        servidor = Request.servervariables("SERVER_NAME")
			        path = replace(path,"http://" & servidor,"")
			        path = replace(path,"https://" & servidor,"")
            
                    'Ponemos esto porque lo otro no funciona debido al redireccionamiento al puerto 8080 de Ormazabal
		            'en gestamp si que sale por otro puerto
		            if Application("PUERTO80")=0 then
			            server.transfer "/script/login/inicio.asp"
		            else
			            server.transfer path & "script/login/inicio.asp"
		            end if            
		        end if
            end select
	end if
end if
if application("NOMPORTAL")<> "huf" then
%>
<script language="javascript">

function Alta(){
    window.open("../../registro/registro.asp?Idioma=<%=Idioma%>","_blank","top=15,left=20,width=700,height=500,location=no,resizable=yes,scrollbars=yes,toolbar=yes")
};
</script>
<%end if%>
