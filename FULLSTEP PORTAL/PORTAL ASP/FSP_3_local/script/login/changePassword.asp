﻿<%@ Language=VBScript %>
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader "Pragma", "no-cache" %>
<% Response.charset="UTF-8"%>
<% Response.Expires = -1 %>
<!DOCTYPE HTML PUBLIC >
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/popEspera.asp"-->
<%        
    Idioma = Request.Cookies("USU_IDIOMA")
	Idioma = trim(Idioma)        	
	'Idiomas
	dim den
	den = devolverTextos(Idioma,133)
    
    if Request.ServerVariables("CONTENT_LENGTH")>0 then            
        if request.QueryString("t").Count=0 then
            set oRaiz=validarUsuario(Idioma,true,false,0)
            set TESError = oRaiz.ChangePassword(oRaiz.Sesion.CiaCod,oRaiz.Sesion.UsuCod,request("txtPwdActual"),request("txtNewPwd"), Application("PYME"))            
        else
            set oRaiz = createobject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
            if (oRaiz.Conectar(Application("INSTANCIA"))) = 1 then
	            set oraiz = nothing
	            Response.Redirect Application ("RUTASEGURA") & "script/imposibleconectar.asp?Idioma=" & Idioma
	            Response.End  
            end if
            if request.QueryString("crc")="" then
                set TESError = oRaiz.ChangePassword(request.QueryString("c"),request.QueryString("u"),request("txtPwdActual"),request("txtNewPwd"), Application("PYME"))
            else
                set TESError = oRaiz.SetPassword(request.QueryString("c"),request.QueryString("u"),request("txtNewPwd"), Application("PYME"))                         
            end if            
        end if

        dim IsSecure
        IsSecure = left(Application ("RUTASEGURA"),8)
        if IsSecure = "https://" then                       
            IsSecure = "; secure"
        else
            IsSecure =""
        end if
        
        select case TESError.NumError 
            case 0
                if not  request.QueryString("t").Count=0 then
                    'Validar login e ir a inicio
                    IPDir=Request.ServerVariables("LOCAL_ADDR")
                    UsuSesionId=Request.Cookies("USU_SESIONID")
                    PersistID=Request.Cookies("SESPP")

                    set oRaiz = createobject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
	                i = oraiz.Conectar(Application("INSTANCIA"))
                    path = application("RUTASEGURA")
	                if not i = 1 then
                        set Sesion = oraiz.ValidarLogin (request.QueryString("c"),request.QueryString("u"),request("txtNewPwd"),cint(Application("PORTAL")),Application("PYME"), _
                                                            false,Application("APROVISIONAMIENTO"),Application("SOLICITUDESPM"),Application("CERTIFICADOS"),Application("NOCONFORMIDADES"), _
                                                            Application("CN"),IPDir,PersistID,UsuSesionId,Application("NOMPORTAL"))
                    end if

                    'En la cookie USU se escribirá el ID de sesión generado y los datos actuales MOSTRARFMT, THOUSANFMT, DECIMALFMT, PRECISIONFMT, DATEFMT, IDIOMA, TIPOMAIL
                    Response.AddHeader "Set-Cookie","USU_SESIONID=" &  server.URLEncode(Sesion.Id) & "; path=/; HttpOnly" + IsSecure

                    Response.AddHeader "Set-Cookie","USU_MOSTRARFMT=" &  Sesion.MostrarFMT  & "; path=/; HttpOnly" + IsSecure

                    Response.AddHeader "Set-Cookie","USU_DECIMALFMT=" & Sesion.DecimalFMT  & "; path=/; HttpOnly" + IsSecure
                    Response.AddHeader "Set-Cookie","USU_THOUSANFMT=" & Sesion.ThousanFMT  & "; path=/; HttpOnly" + IsSecure
                    Response.AddHeader "Set-Cookie","USU_DATEFMT=" & Sesion.DateFMT  & "; path=/; HttpOnly" + IsSecure
                    Response.AddHeader "Set-Cookie","USU_PRECISIONFMT=" & Sesion.PrecisionFMT  & "; path=/; HttpOnly" + IsSecure

                    Response.AddHeader "Set-Cookie","USU_TIPOMAIL=" & Sesion.TipoMail  & "; path=/; HttpOnly" + IsSecure
                    Response.AddHeader "Set-Cookie","USU_IDIOMA=" &   Sesion.Idioma  & "; path=/; HttpOnly" + IsSecure

                    Response.AddHeader "Set-Cookie","USU_CSRFTOKEN=; path=/; HttpOnly" + IsSecure

                    if Sesion.Parametros.unaCompradora then
				        application("UNACOMPRADORA") = 1
				        application("IDCIACOMPRADORA") = Sesion.Parametros.IdCiaCompradora
			        else
				        application("UNACOMPRADORA") = 0
                    end if
                    'El Id generado para la cookie persistente se almacenará en una cookie de nombre SESPP (sesión portal persistente) a la que pondremos una caducidad de 24 horas.
                    FechaCP=Date+1

                    sDia=cstr(day(FechaCP))
                    sUrte = cstr(year(FechaCP))

                    sDia = left("00",2-len(sDia)) & sDia

                    select case Weekday(FechaCP)
                    case 1:
                        sDiaWeek = "Sun, "
                    case 2:
                        sDiaWeek = "Mon, "
                    case 3: 
                        sDiaWeek = "Tue, "
                    case 4: 
                        sDiaWeek = "Wed, "
                    case 5: 
                        sDiaWeek = "Thu, "
                    case 6:
                        sDiaWeek = "Fri, "
                    case 7:
                        sDiaWeek = "Sat, "
                    end select 

                    select case month(FechaCP)
                    case 1:
                        sMes= "Jan"
                    case 2:
                        sMes= "Feb"
                    case 3:
                        sMes= "Mar"
                    case 4:
                        sMes= "Apr"
                    case 5:
                        sMes= "May"
                    case 6:
                        sMes= "Jun"
                    case 7:
                        sMes= "Jul"
                    case 8:
                        sMes= "Aug"
                    case 9:
                        sMes= "Sep"
                    case 10:
                        sMes= "Oct"
                    case 11:
                        sMes= "Nov"
                    case 12:
                        sMes= "Dec"
                    end select
                    
                     VisualizacionFechaCP = sDiaWeek + sDia + "-" + sMes + "-" + sUrte + " 00:00:00 GMT"   
                    
                    Response.AddHeader "Set-Cookie","SESPP=" & server.URLEncode(Sesion.PersistID)  & "; path=/; HttpOnly; Expires=" + VisualizacionFechaCP + IsSecure

                end if
                Response.Redirect Application("RUTASEGURA") & "script/login/" & Application("NOMPORTAL") & "/inicio.asp?idioma=" & Idioma
            case 1 'Usuario no validado
                %>
                    <script type="text/javascript">
                        alert('<%=JSText(Den(17))%>');
                    </script>
                <%
            case 10000
                'Error servicio
                %>
                    <script type="text/javascript">
                        alert('<%=JSText(Den(16))%>');
                    </script>
                <%
            case 10001
                'Password corta
                %>
                    <script type="text/javascript">
                        alert('<%=JSText(Replace(Replace(Den(12),"\n",chr(10)),"###",TESError.Arg1))%>');
                    </script>
                <%
            case 10002
                'Edad min password
                %>
                    <script type="text/javascript">
                        alert('<%=JSText(Den(13))%>');
                    </script>
                <%
            case 10003
                'No cumple condiciones complejidad
                %>
                    <script type="text/javascript">
                        alert('<%=JSText(Replace(Replace(Den(14),"\n",chr(10)),"###",TESError.Arg1))%>');
                    </script>
                <%
            case 10004
                'No cumple historico. Hay una password igual entre las ultimas
                %>
                    <script type="text/javascript">
                        alert('<%=JSText(Replace(Replace(Den(15),"\n",chr(10)),"###",TESError.Arg1))%>');
                    </script>
                <%
        end select        
    end if
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <style type="text/css">
        div label
        {
    	    display:inline-block;
    	    width:20em;
        }
        div input
        {
    	    margin-left:1em;
    	    width:25em;
        }
        #divChangePasswordButtons span
        {
        	cursor:pointer;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp"/>
    <script src="../js/jquery.min.js" type="text/javascript"></script>
</head>
<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
    $(document).ready(function () {
        $('#tablemenu').show();
        p = window.top.document.getElementById("frSet")
        vRows = p.rows
        vArrRows = vRows.split(",")
        vRows = vArrRows[0] + ",*,0,0"
        p.rows = vRows
    });
    function ChangePassword() {
        if ($('#txtPwdActual').val()==""){
            alert('<%=JSText(Den(3))%>');
            return false;
        };
        if ($('#txtNewPwd').val()=="" || $('#txtConfirmPwd').val()==""){
            alert('<%=JSText(Den(4))%>');
            return false;
        };
        if ($('#txtNewPwd').val() !== $('#txtConfirmPwd').val()) {
            alert('<%=JSText(Den(10))%>');
            return false;
        };
        showMensajeEspera('<%=JSText(Den(1))%>...');
        document.forms["frmChangePassword"].submit();     
    };
    function CancelChangePassword() {
        window.parent.location.href('<%=application("RUTASEGURA")%>/script/proveedor');
    };
</script>
<%
if Request.Cookies("USU_CADPASSWORD") ="" then
        cadpasword=false
    else
        cadpasword=cbool(Request.Cookies("USU_CADPASSWORD"))
    end if
if not cadpasword and request.QueryString("crc").Count=0 then%>
    <script type="text/javascript" src="../common/menu.asp"></script>
    <script type="text/javascript">dibujaMenu(5);</script>
<%end if%>
<body style="text-align:left;">
    <form name="frmChangePassword" id="frmChangePassword" method="post">
        <div style="margin:2em; text-align:left; float:left;">
            <img alt="" src="../images/ChangePassword.jpg" style="float:left; margin-right:2em;" />                
            <%if cadpasword then%>
                <h1 style="white-space:nowrap;"><%=JSText(Den(2))%></h1>
                <span style="width:40em;"><%=JSText(Den(9))%></span>
            <%elseif not request.QueryString("crc").Count=0 then%>
                <h1 style="white-space:nowrap;"><%=JSText(Den(11))%></h1>
            <%else%>
                <h1 style="white-space:nowrap;"><%=JSText(Den(1))%></h1>
            <%end if%>
            <%if cadpasword then%>
                <input type="hidden" id="hUsu" value="<%=Request.QueryString("u")%>" />
                <input type="hidden" id="hCia" value="<%=Request.QueryString("c")%>" />
            <%end if%>
        </div>
        <%if request.QueryString("crc")="" then%>
            <div class="SeparadorInf" style="clear:both; width:50em; margin:0em 5em; padding-bottom:1em; text-align:left;">
                <label for="txtPwdActual"><%=JSText(Den(3))%></label>
                <input type="password" id="txtPwdActual" name="txtPwdActual" />
            </div>
        <% end if%>
        <div style="clear:both; margin:1em 5em; text-align:left;">
            <label for="txtNewPwd"><%=JSText(Den(4))%></label>
            <input type="password" id="txtNewPwd" name="txtNewPwd" />
        </div>
        <div style="margin:1em 5em; text-align:left;">
            <label for="txtConfirmPwd"><%=JSText(Den(5))%></label>
            <input type="password" id="txtConfirmPwd" name="txtConfirmPwd" />
        </div>
        <div id="divChangePasswordButtons" style="margin:2em 5em; overflow:hidden;">
            <%if cadpasword then%>
                <span class="botonRedondeado" style="float:left;" onclick="ChangePassword()"><%=JSText(Den(6))%></span>
                <span class="botonRedondeado" style="float:left; margin-left:1%;" onclick="CancelChangePassword()"><%=JSText(Den(7))%></span>
            <%else %>
                <span class="botonRedondeado" style="float:left;" onclick="ChangePassword()"><%=JSText(Den(8))%></span>
            <%end if%>
        </div>
    </form>
</body>
</html>
