﻿<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="../estilos.css" rel="stylesheet" type="text/css">
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
    <style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style>
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body>
    <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
    <input type="hidden" name="idioma" value="spa" />
    <table width="640" border="0" cellspacing="0" cellpadding="0" class="textos">
        <tr>
            <td height="100" colspan="2" background="../images/fondo_R.gif">
                <div align="right">
                    <a href="http://www.aztecafoods-europe.com/" target="_blank">
                        <img src="../images/logo.gif" height="85" border="0" /></a></div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width="20">
                &nbsp;
            </td>
            <td width="615" align="left" class="textos" style="color:#666666; font-family:Verdana">
                <br />
                <span style="color: #666666;">Si ha olvidado su contraseña introduzca su código de compañia,
                    de usuario y la dirección de e-mail de su cuenta de acceso y pulse "Enviar". Recibirá
                    un e-mail con el enlace a la página donde podrá crear una nueva contraseña.<br />
                </span>
                <br />
                <table class="textos" style="color:#666666; font-family:Verdana">
                    <tr>
                        <td>
                            Cód. Compañia:
                        </td>
                        <td>
                            <input type="text" name="txtCia" id="txtCia" size="80" maxlength="100" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Cód. Usuario:
                        </td>
                        <td>
                            <input type="text" name="txtUsu" id="txtUsu" size="80" maxlength="100" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Dirección de email:
                        </td>
                        <td>
                            <input type="text" name="txtEmail" id="txtEmail" size="80" maxlength="100" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="20">
                &nbsp;
            </td>
            <td class="textos" style="color:#666666; font-family:Verdana">
                <br />
                <input type="submit" value="Enviar" name="cmdEnviar" id="cmdEnviar" />
                <br />
                <br />
                Si continúa con dificultades para acceder al portal o su dirección de email ha cambiado,
                consulte con el servicio de atención al cliente. Tlfno.: 902 996 926
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
