﻿<%@ Language=VBScript %>
<!--#include file="../../common/XSS.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="estilos.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.9.0.js"></script>
<script src="js/jquery-1.9.0.min.js"></script>
<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=SPA", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body>

	<div id="cont_gen">
	<div id="izd_gen"> <a href="http://www.servisa.es" target="_blank"><img src="images/logo.gif" alt="Servisa" /></a>
    </div>
    <div id="drc_gen">
    	<h1 class="tit">SOLICITAR REGISTRO</h1>
        <div class="int">
        	
                                
            <div class="caja_acepto">
            <p> Para continuar con el proceso de alta es imprescindible aceptar  las
                 <a href="spa/docs/Ocaso-Servisa-Eterna-Condiciones-Generales-de-compra-y-contratacion.pdf" target="_blank">Condiciones Generales de Compra y Contratación</a>.</p>
<!--<form action="" method="post" style="width:400px;">//-->
     <form name="frmAlta" id="frmAlta" method="post" style="width:100%;">
  	<span id="sprycheckbox1">
	<label>
  		<input type="checkbox" name="opcion1" id="opcion1" required>
	</label>
	<span class="checkboxRequiredMsg">Seleccione para continuar.</span>
	</span>
	He leído y acepto las <a href="spa/docs/Ocaso-Servisa-Eterna-Condiciones-Generales-de-compra-y-contratacion.pdf" target="_blank" class="rojo">Condiciones Generales de Compra y contratación</a><br />
<br />
<br />

<br />

	<input type="image" id="submit1" name="submit1" src="images/continuar.png" width="120" height="30" >
	<!-- **Estilo sin boton**
	<button type="button" class="bt_registro" id="submit1" name="submit1" >Continuar</button>-->
	<br>
	</form>
           </div>
<div style="clear:both;"></div>
    	  
   </div>
   </div>
   
</div>

<script type="text/javascript">

$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked') )
       {
        $('#frmAlta').submit();
        alta();}
    else
       alert("Debe aceptar las condiciones de uso del portal para poder registrarse");
    
});

</script>
</body>

</html>
