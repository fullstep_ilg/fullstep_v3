﻿<%@ Language=VBScript %>
<!--#include file="../../../common/XSS.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://fast.fonts.net/jsapi/f5dbd368-355c-4f28-bc2f-217b51498d80.js"></script>
</head>
<script>
function Validar()
	{
	f=document.forms("frmRecuerdo")
	if (f.txtEmail.value=="")
		return false
	}
</script>

<body>

<div id="cont_gen">
	<div id="izd_gen"> <a href="http://www.servisa.es" target="_blank"><img src="../images/logo.jpg" alt="Ocaso" /></a>
    </div>
    <div id="drc_gen">
    	<h1>¡Su petición ha sido procesada!</h1>
        <div class="int">
        	<p> 
                En breve recibirá un mail con respuesta a su petición.
           </p>
            
<div class="recordar-claves">
            	Si continúa con dificultades para acceder al portal o su dirección de email ha cambiado, consulte con el<br />
                <span class="rojo"><strong>servicio de atención al cliente.</strong></span><br />
                <span class="rojo_24"><strong>901 01 01 01 </strong></span>
            </div>
    	</div>    
    </div>
</div>
</body>

</html>
