﻿<%@ Language=VBScript %>
<!--#include file="../login.asp"-->
<%		
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if

if Not IdOrden = "" then
    if Not IsNumeric(IdOrden) then
        IdOrden=""
    end if
end if

if Not CiaComp = "" then
    if Not IsNumeric(CiaComp) then
        CiaComp=""
    end if
end if

If Idioma="" then
	Idioma="SPA"
end if

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="estilos.css" rel="stylesheet" type="text/css" />
<script language="javascript">
<!--


function ventanaLogin (IDI){

   window.open ("registro.asp?Idioma="+IDI,"","width=700,height=540,resizable=yes")

}
function ventanaSecundaria (URL){

   window.open(URL,"ventana1","width=700,height=420,scrollbars=no")

}
function ventanaAyuda (URL){

   window.open(URL,"ayuda","width=700,height=600,scrollbars=yes")

}

function popup (URL) {
 window.open("popup.htm","","width=700,height=400,scrollbars=NO")
 }
function recuerdePWD()
{
   window.open("spa/recuerdo.asp","_blank","width=715,height=450,scrollbars=NO")
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
//-->
</script>
<script src="js/jquery-1.9.0.min.js"></script>
<!--*****************-->
<script>
$(document).ready(function(){
	//---------------------
  	$("#txtCIA").val("NIF Compañía");
  	$("#txtCIA").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="NIF Compañía"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("NIF Compañía");
  			$(this).removeClass('focus');
  		}
  	});	
	//---------------------
  	$("#txtUSU").val("Código de Usuario");
  	$("#txtUSU").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Código de Usuario"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Código de Usuario");
  			$(this).removeClass('focus');
  		}
  	});
	//---------------------
  	$("#txtPWD").val("Contraseña");
  	$("#txtPWD").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Contraseña"){
  			$(this).val('');
			$(this).removeAttr("type");
			$(this).prop('type', 'password');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Contraseña");
			$(this).removeAttr("type");
			$(this).prop('type', 'text');
  			$(this).removeClass('focus');
  		}
  	});
});
</script>
</head>
<body class="home">
<div id="contenedor">
	<!--cabecera-->
	<div id="cabecera">
    	<div id="logo">
        	<a href="www.ree.es"><img src="images/logo_ree.png" alt="Red Eléctrica de España" /></a>
        </div>
        <div id="nav">
            <ul id="nav_inf">
            	<li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/codigo-de-conducta.htm">Código de conducta</a></li>
                <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/condiciones-de-uso.htm">Condiciones de uso</a></li>
                <li> <a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/soporte-tecnico.htm" target="_blank">Ayuda y preguntas frecuentes</a></li>
                
            </ul>
        </div>
    </div>
    <!--********-->
    <!--titulo-->
    <div id="titulo">
        INICIO &nbsp;&nbsp; | &nbsp;&nbsp; TIENDA ON-LINE
    </div>
    <!--******-->
    <!--formulario acceso-->
    <div id="acceso">
    	<h1>Area de Proveedores<br /><strong>Tienda On-line</strong></h1>
    	<div class="wform">
            <h2>ACCESO PROVEEDORES</h2>
            <form action="default.asp" method="post" name="frmLogin"  id="frmLogin">
                <input type="text" class="text" id="txtCIA" name="txtCIA" maxlength="20">
                <input type="text" class="text" id="txtUSU" name="txtUSU" maxlength="20">
                <input type="password" class="text" id="txtPWD" name="txtPWD" maxlength="20">
                <div class="bt_entrar">
                    <input class="bt" name="cmdEntrar" type="submit" value="ENTRAR" />
                    <a href="javascript:void(null)" onClick="recuerdePWD()" class="bt_claves">¿Olvidó sus claves<br /> de acceso?</a>
                </div>
                <input type="hidden" id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
                <input type="hidden" id="CiaComp" name="CiaComp" value="<%=CiaComp%>">
            </form>
        </div>
    </div>
    <!--*****************-->
    <!--navegador iconos-->
    <div id="nav_home">
    	<ul>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/codigo-de-conducta.htm"><img src="images/img_codigo.jpg" alt="Código de conducta" /><span>CÓDIGO DE CONDUCTA</span></a></li>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/condiciones-de-contratacion.htm"><img src="images/img_condiciones.jpg" alt="Condiciones de contratación" /><span>CONDICIONES DE USO</span></a></li>
            <li><a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/soporte-tecnico.htm"><img src="images/img_soporte.jpg" alt="Soporte técnico del área de proveedores" /><span>PREGUNTAS FRECUENTES</span></a></li>
        </ul>
        <div class="datos_fullstep gris_10">
        	<p>
        	ATENCIÓN A PROVEEDORES<br />
            <span class="azul_18" style="line-height:18px;"><strong>Tel. 902 043 067</strong></span><br />
            <a href="mailto:soportetol.ree@fullstep.com" class="azul">soportetol.ree@fullstep.com</a>
            </p>
            <p style="border-top:#E42D13 1px solid; margin-top:2px; padding-top:2px;">
            HORARIO DE ATENCIÓN TELEFÓNICA:
            </p>
            <p>
            LUNES A JUEVES<br />
            <span class="azul">8:30 a 13:30 y de 14:30 a 17:30</span>
            </p>
            <p>
            VIERNES<br />
            <span class="azul">8:00 a 14:00</span>
            </p>
        </div>
        <div style="clear:both"></div>
    </div>
    <!--****************--> 
    <!--footer-->
    <div id="pie">
   	  <div class="datos_pie"><br /><br />
        	<img src="images/logo_ree_pq.png" alt="Red Eléctrica Española" style="margin:0;" /><br />
        	<span class="gris_10">&copy;RED ELÉCTRICA DE ESPAÑA</span>
            
        </div>
      <div class="datos_pie" style="padding-top:45px; height:55px;"><br /><br />
			
       <a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/aviso-legal.htm">AVISO LEGAL</a>
       |&nbsp;<a href="<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=SPA" class="subtitulo"">Pol&iacute;tica de Cookies</a>
         
        </div>
      <div class="datos_pie" style="padding-top:45px; height:55px;">
        	SIGUENOS
            <br />
			<a href="https://www.facebook.com/RedElectricaREE" target="_blank"><img src="images/ico_fcb.png" alt="Facebook" /></a>
            <a href="https://twitter.com/RedElectricaREE" target="_blank"><img src="images/ico_twt.png" alt="Twitter" /></a>        <a href="https://www.youtube.com/user/RedElectricaREE" target="_blank"><img src="images/ico_you.png" alt="Youtube" /></a>
            <a href="https://plus.google.com/photos/102590846538819763590/albums?banner=pwa&amp;gpsrc=pwrd1#photos/102590846538819763590/albums?banner=pwa&amp;gpsrc=pwrd1" target="_blank"><img src="images/ico_pic.png" alt="Picasa" /></a>
        </div>
        <div style="clear:both"></div>
    </div>
    <!--******-->
</div>
</body>

</html>
