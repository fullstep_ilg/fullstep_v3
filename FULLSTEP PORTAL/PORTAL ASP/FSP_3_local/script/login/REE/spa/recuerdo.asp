﻿<%@  language="VBScript" %>
<!--#include file="../../recuerdo.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../estilos.css" rel="stylesheet" type="text/css" />
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body>
    <div id="cont_gen">
        <div id="izd_gen">
            <img src="../images/logo_gen.jpg" alt="Red Eléctrica de España" />
        </div>
        <div id="drc_gen">
            <h1>
                RECORDATORIO DE CLAVES</h1>
            <div class="int">
                <p>
                    Si ha olvidado sus claves de acceso, introduzca la dirección de e-mail registrada
                    en la tienda online, y a continuación recibirá un e-mail con sus claves
                </p>
                <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
                    <input type="hidden" name="idioma" value="spa" />
                    <table class="textos">
                        <tr>
                            <td>
                                Cód. Compañia:
                            </td>
                            <td>
                                <input type="text" name="txtCia" id="txtCia" size="80" maxlength="100" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Cód. Usuario:
                            </td>
                            <td>
                                <input type="text" name="txtUsu" id="txtUsu" size="80" maxlength="100" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Dirección de email:
                            </td>
                            <td>
                                <input type="text" name="txtEmail" id="txtEmail" size="80" maxlength="100" />
                            </td>
                        </tr>
                    </table>
                    <br />
                    <input class="bt_registro" name="cmdEnviar" type="submit" style="height: 22px; padding: 0;"
                        value="Enviar" />
                </form>
                <div class="recordar-claves">
                    Si continúa con dificultades para acceder a la tienda online o su dirección de email
                    ha cambiado, consulte con el<br />
                    <span class="rojo"><strong>servicio de atención a proveedores.</strong></span><br />
                    <span class="rojo_24"><strong>902 043 067</strong></span>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
