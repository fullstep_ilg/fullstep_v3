﻿<%@ Language=VBScript %>
<!--#include file="../../common/XSS.asp"-->
<%		


''' <summary>
''' Pantalla inicial de la personalización, es la q llama a la pantalla de login
''' </summary>
''' <remarks>Llamada desde: registro/registrarproveedor.asp		proveedor/default.asp
'''		login/personalizacion/login.asp		; Tiempo máximo: 0,1</remarks>

Idioma = request("Idioma")
If Idioma="" then
	Idioma="SPA"
end if


if Request.ServerVariables("CONTENT_LENGTH") >0 or Request.QueryString<>"" then
	IdOrden = request("IdOrden")
	CiaComp = request("CiaComp")

    if Not IdOrden = "" then
        if Not IsNumeric(IdOrden) then
            IdOrden=""
        end if
    end if

    if Not CiaComp = "" then
        if Not IsNumeric(CiaComp) then
            CiaComp=""
        end if
    end if

	sQuery = "?Idioma=" & Idioma & "&IdOrden=" & IdOrden & "&CiaComp=" & CiaComp
else
	sQuery = "?Idioma=" & Idioma
end if

if Idioma="SPA" then
	Response.REDIRECT "" & Application("RUTASEGURA") & "script/login/" & Application("NOMPORTAL") & "/login.asp"  & sQuery
else
	Response.REDIRECT "" & Application("RUTASEGURA") & "script/login/" & Application("NOMPORTAL") & "/" & Idioma & "/login.asp"  & sQuery
end if
%>
