﻿<%@ Language=VBScript %>
<!--#include file="../../../common/XSS.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700"></script>
<script src="../js/jquery-1.9.0.js"></script>
<script src="../js/jquery-1.9.0.min.js"></script>
<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=ENG", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body>

<div id="cont_gen">
	<div id="izd_gen"> <a href="http://www.ajegroup.com" target="_blank"><img src="../images/logo-aje.png" border="0"></a>
    </div>
    <div id="drc_gen">
    	<h1>REGISTRATION</h1>
        <div class="int">
        	<p> 
                To proceed with the registration proccess, the <span class="rojo">following contract must be read and accepted</span> :
               
            </p>
            <p style="height:150px; padding:0 5px; overflow:auto; background:#FFF; border:#CCC 1px solid">
               PORTAL TERMS AND CONDITIONS<br/><br/>
Supplier register<br/><br/>
Legal Notice - Portal access<br/><br/>
Registering in the purchasing Portal of AJE (from here on referred to as AJE) implies previous reading and acceptance of the following clauses. Without expressing full compliance with the clauses you will not be allowed to register. Every time you access and use the Portal, it is understood you agree, fully and unreservedly, with the contents of these Legal Notice. Also, being the main user of your company, you are obliged by accepting this Legal Notice to ensure compliance by all users registered in your company, fully exonerating AJE from any responsibility of the damage those users may cause to your company or any other because of their interactions with the Portal.


Clauses

1. Purpose of the AJE purchasing Portal

 The AJE purchasing Portal is the means through which AJE communicates with its suppliers to request offers, documents or any other commercial information it deems fit. At the same time AJE reserves the right to use the Portal to communicate any information it considers to be of its interest.

AJE works both as a direct purchaser and as a purchasing broker for its clients, and shall act in their name during the purchasing process.

 Access or use of Portal and / or Services does not give the SUPPLIER any rights on trademarks, commercial denominations or any other distinctive sign appearing in the Portal, being AJE or third party’s property. Therefore, all contents are intellectual property of AJE or third party, and will not be understood as ceded to the SUPPLIER.



2. Purpose 

 The purpose of this contract is to regulate relations between AJE, AJE’s clients and the SUPPLIER, in all aspects concerning use of the PORTAL.


3. SUPPLIER obligations 

 The following are obligations of the SUPPLIER:

 a. Provide whatever data is necessary for the adequate functioning of the system, and maintain them updated, communicating at the earliest possible time any modifications.

 b. Guaranteeing the authenticity of data provided as a consequence of forms necessary to subscribe to the Services offered by the PORTAL. Also, the SUPPLIER will update all information provided to ensure it reflects, at all times, its real situation. Therefore, the SUPPLIER is solely responsible of damages caused to AJE or AJE’s clients as a consequence of inexact or false statements.

c. Keeping absolute confidentiality in relation to all information derived from the relations between the SUPPLIER and AJE or AJE’s clients.

 d. Abstaining from doing any modification in the information-technology use of the Portal, as well as using it with purposes different from those for which it is intended. Also, the SUPPLIER will abstain from accessing unauthorized zones of the Portal.

 e. Comply loyally to the commitments expressed in the information sent through the Portal. If the SUPPLIER does not demonstrate due commercial diligence, or does not comply with contracted duties, AJE and its clients reserves the right to exclude temporarily or permanently the SUPPLIER from the Portal.

 f. The SUPPLIER will indicate only those material groups referring to goods or services that, at the time of acceptance of the contract, are commercialized, manufactured or distributed by the SUPPLIER and of commercial interest to AJE and its clients. 

 g. The SUPPLIER accepts that offers sent through the Portal are considered to be of the same rank and validity as offers sent through any other traditional means (letters, fax).

h. The SUPPLIER is obliged to use the Portal and its Services in compliance with the Law, this Legal Notice, and the rest of regulations and instructions of which it is informed, as well as in accordance to generally accepted morals and customs, and public order.

Thus, the SUPPLIER will abstain from using the Portal or any of its Services with illegal purposes, prohibited in this Legal Notice, and/or damaging to rights and interests of third parties. 

In particular, and merely as a matter of indication, the SUPPLIER agrees not to transmit, broadcast, or make available to others or third parties any information, data, contents, graphics, image and/or sound files, photographs, recordings, software and, in general, any type of material that:

1. Is contrary to fundamental rights and public liberties under the Constitution, International treaties and applicable laws;

2. Induces, incites or promotes criminal acts, contrary to law, moral and generally accepted customs or public order; 

3. Is false, inexact or may induce to error, or constitutes illegal, deceitful or disloyal publicity;

4. Is protected under intellectual or industrial property rights belonging to third parties without prior explicit and written consent.

5. Contravenes laws and statutes for civil protection against defamation and invasion of privacy.

6. Contravenes regulations about privacy and/or secrecy of communications.

7. Can be considered as unfair competition or damages in any way the image of AJE or third parties. 

8. Is affected by viruses or similar elements that can harm or stop the Portal’s adequate performance, the electronic equipment or their files and documents.


4. Rights of the SUPPLIER 

 The following are rights of the SUPPLIER:

 1. Maintain a constant presence in AJE’s database, as a registered supplier. 

 2. Receive offers according to the established rules. 


5. Obligations of AJE 

 The following are AJE obligations:

 a. Maintain the information it deems fit constantly updated, without being liable or responsible of any mistakes that may happen by chance or due to circumstances beyond its control.

 b. Keep absolute confidentiality of all information concerning the SUPPLIER, both if supplied directly or generated as a consequence of relationships between the SUPPLIER and AJE.

 c. Provide the SUPPLIER, at any time, the situation of its data inside the database, and not to provide it to any third parties except to allow the SUPPLIER to present offers to be taken into consideration as supply alternatives. Also, to provide the SUPPLIER, at any time, the situation of its data inside the database, so that the SUPPLIER, with previous written notification, modifies or deletes them.


6. Rights of AJE and its clients

 a. The SUPPLIER gives its explicit consent for AJE to communicate information concerning the SUPPLIER to associated companies or companies belonging to the same group, as well as others with which it may sign agreements with the only aim of providing the best services, in compliance, in any case, with Spanish legislation on data protection. Also, the SUPPLIER agrees on AJE, sending information about goods or services they commercialize, directly or indirectly, or that will be commercialized. 

 b. Choose, in case of infringement by the SUPPLIER of its obligations, between demanding full compliance with its obligations or cancellation of the contract, in the terms expressed in clause 8 of this Legal Notice. 

7. Limitation of responsibility 

 a. AJE shall not be liable or responsible for the availability of the service, network or technical failures that might cause an interruption or cuts in the Portal.
Equally, AJE shall not be liable of any damages caused by the spreading of IT viruses or other elements in the system. 

b. AJE shall not be liable or responsible of any payments and /or complaints that derive from the acts of clients concerning the agreement between the client(s) and the SUPPLIER.

 c. AJE shall not be liable or responsible of any infringement of the Laws or generally agreed Customs, or of acts that contravene obligations established in this Legal Notice, by any employee of AJE, by the SUPPLIER, or by any Internet or Network user.
However, were AJE or its clients to have knowledge of any conduct the previous paragraph refers to, it shall adopt the necessary measures to resolve with the upmost diligence and urgency the conflicts caused. 
d. AJE shall not be held liable or responsible of any interference by unauthorized third parties in the knowledge of the conditions and circumstances of use that the SUPPLIER shall do of the Portal and its Services.



8. Contract cancellation 

 In the case of failure to comply with the obligations contained in this Legal Notice, the SUPPLIER or AJE and its clients must notify the breach to the infringing party, with a fifteen working day period to repair it. After that period has passed, should the infringement not be repaired, the other party can choose between the fulfilling and cancellation of this agreement. Should it choose to cancel the contract, both the SUPPLIER and AJE or its clients agree that simple notification shall be enough for the cancellation to have full effect. 


9. Duration 

The proceeding to cancel the contract will be to send an e-mail indicating “PORTAL UNSUSCRIBE” to the address procurementsupport@AJEGroup.com or by fax at +34 91 296 20 25. 




10. Notifications, Legislation and Jurisdiction

 a. Notifications between the parties may be carried out through any of the means admitted by the Law that allows having a record of reception, including fax.
 b. Changes in addresses and fax numbers shall be notified in writing, and shall not have effect until two working days from reception.
c. The relationships between AJE and the SUPPLIER derived from the use of the Portal and the Services carried out inside it, will comply with adequate Spanish legislation. 


			</p>
            <p style="text-align:right"><a href="registro_texto.htm" class="bt_registro" target="blank">Print</a> </p>
<div class="caja_acepto">


<!--<form action="" method="post" style="width:400px;">//-->
<form name="frmAlta" id="frmAlta" method="post" style="width:400px;">
  <span id="sprycheckbox1">
<label>
  <input type="checkbox" name="opcion1" id="opcion1" required>
</label>
<span class="checkboxRequiredMsg">Select to continue.</span></span><span class="rojo">I have read and accept</span> the General terms and conditions of use of the Supplier Portal.<br />
<br />

<button type="button" class="bt_registro" id="submit1" name="submit1">Next</button>
<br>
</form>


</div>
<div style="clear:both;"></div>
    	</div>    
    </div>
</div>
<script type="text/javascript">

$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked') )
       {
        $('#frmAlta').submit();
        alta();}
    else
       alert("You must accept the general terms and conditions of use of the Portal");
    
});

</script>
 			
</body>

</html>
