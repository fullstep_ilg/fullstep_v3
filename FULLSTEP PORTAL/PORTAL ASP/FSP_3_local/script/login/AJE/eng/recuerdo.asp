﻿<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700"></script>
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;
    };
</script>
<body>
    <div id="cont_gen">
        <div id="izd_gen">
           <a href="http://www.ajegroup.com" target="_blank"><img src="../images/logo-aje.png" border="0"></a>
        </div>
        <div id="drc_gen">
            <div class="int">
                <p>
                    If you forgot your password, please fill in your user and email and click on "Submit". You will receive a link to a page to create a new password. 
                </p>
                <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
                    <table>
                    <tr>
                        <td>Company Code:</td>
                        <td><input type="text" name="txtCia" id="txtCia" size="30" maxlength="30"/></td>
                    </tr>
                    <tr>
                        <td>User Code:</td>
                        <td><input type="text" name="txtUsu" id="txtUsu" size="30" maxlength="30"/></td>
                    </tr>
                    <tr>
                        <td>Email address:</td>
                        <td><input type="text" name="txtEmail" id="txtEmail" size="30" maxlength="100"/></td>
                    </tr>
                </table>
                    <br /><input class="bt_registro" name="cmdEnviar" type="submit" style="height:22px; padding:0;" value="Submit" />
                     <input type="hidden" name="idioma" value="eng"/>
                </form>
                            <div style="clear:both;"></div>

                <div class="recordar-claves-2">
                    <span style="display:block;">  If you have problems while accessing the portal, or your e-mail address has changed,
                    please    </span><span class="rojo" style="display:block;"><strong>contact the Portal Administrator  </strong></span>
                   <a href="mailto:procurementsupport@ajegroup.com">procurementsupport@ajegroup.com</a>
                    <br />
                </div>
            </div>
        </div>
    </div>
</body>

</html>
