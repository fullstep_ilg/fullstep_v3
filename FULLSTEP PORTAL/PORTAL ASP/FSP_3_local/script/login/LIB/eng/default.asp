﻿<%@ Language=VBScript %>
<!--#include file="../../login.asp"-->
<%		
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if

if Not IdOrden = "" then
    if Not IsNumeric(IdOrden) then
        IdOrden=""
    end if
end if

if Not CiaComp = "" then
    if Not IsNumeric(CiaComp) then
        CiaComp=""
    end if
end if

If Idioma="" then
	Idioma="ENG"
end if

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Portal de Proveedores</title>
<link href="../estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: 5px;
	margin-top: 0px;
	background-image: url();
	background-color: #EBEBEB;
}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function ventanaLogin (IDI){

   window.open ("registro.asp?Idioma="+IDI,"","width=600,height=450,resizable=yes")

}
function ventanaSecundaria (URL){

   window.open(URL,"ventana1","width=700,height=400,scrollbars=NO")

}
function ventanaAyuda (URL){

   window.open(URL,"ventana1","width=735,height=800,scrollbars=yes")

}
function recuerdePWD()
{
   window.open("recuerdo.asp","_blank","width=641,height=390,scrollbars=NO")
}
//-->
</script>
</head>

<body>
		<form name="frmLogin" id="frmLogin" method="post" action="default.asp">
		<input type="hidden" id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
		<input type="hidden" id="CiaComp" name="CiaComp" value="<%=CiaComp%>">						
		<input type="hidden" name="Idioma" value="<%=Idioma%>">
<br><br>
<div align="center">
    <table width="955" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="955" >
		  <table width="955" height="110" border="0" cellpadding="0" cellspacing="0" >
            <tr bgcolor="#e9e4dc" >
          
              <td width="20%" valign="bottom" ><div align="left"><a href="http://www.liberbank.es" target="_blank"><img src="../images/logo.png" border="0" WIDTH="200" HEIGHT="58"></a></div></td>
              <td width="78%" align="right" valign="bottom" class="titulo" >SUPPLIERS</td>
            </tr>
        </table></td>
        <td width="9" rowspan="4" background="images/sombra_dcha.gif" bgcolor="#FFFFFF">&nbsp;</td>
      </tr>
      <tr>
        <td><table width="955" border="0" cellpadding="0" cellspacing="0">
            <tr bgcolor="#FFFFFF">
              <td width="382" rowspan="3" align="center" valign="top" bgcolor="#FFFFFF"><table width="100%"  border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td height="51">&nbsp;</td>
  <td><table width="80%" border="0" align="left" cellpadding="0" cellspacing="0">
    <tr>
      <td width="15%"><div align="left"><a href="javascript:ventanaAyuda('<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/manuales.htm')" class="textosres">Help</a></div></td>
      <td width="3%" class="textosres">|</td>
      <td width="14%" class="textosres"><a href="../default.asp?Idioma=SPA">Español</a></td>
    <td width="3%" class="textosres">|</td>
    <td width="20%" class="textosres"><a href="javascript:ventanaSecundaria('<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/eng/aviso legal.htm')" class="textosres">Legal notice</a></td>
     <td width="3%" class="textosres">|</td>
    <td width="35%" class="textosres"><a href="<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=ENG" class="textosres">Cookies Policy</a></td>
    </tr>
  </table>    <div align="left"></div></td>
  </tr>
  <tr>
    <td colspan="2"><img src="../images/central.jpg" width="425" height="282"></td>
  </tr>
</table>

  <table width="338" border="0" align="left" cellpadding="0" cellspacing="0">
    <tr>
      <td width="14"></td>
      <td colspan="2"></td>
      <td rowspan="3" width="110"><div align="left"><a href="http://www.fullstep.com" target="_blank"><img src="../images/logo_fullstep.png" alt="FULLSTEP" width="100" height="24" hspace="5" border="0"></a></div></td>
    <td width="173" rowspan="3" class="textos10"></td>
      </tr>
    <tr>
      <td width="14">&nbsp;</td>
      <td colspan="2" class="textos"><div align="left"><font color="#666666">powered</font></div></td>
    </tr>
    <tr>
      <td width="14">&nbsp;</td>
      <td class="textos" valign="top" width="14"><div align="left"><font color="#666666">by</font></div></td>
      <td class="textos" valign="top" width="27">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="4" valign="top" class="textos10">Website optimized for 1280 x 1024<br>
                Supported Browsers: <a href="http://windows.microsoft.com/es-ES/internet-explorer/download-ie" target="_blank" class="formulario">Internet Explorer</a> and <a href="http://www.ez-download.com/mozilla-firefox/?kw=firefox&subid=EZFFES&cust=firefox+download+mozilla&type=firefox&gclid=CN6MtdLfprQCFaTKtAodDWAAgQ&utm_campaign=EZFFES&fwd=1" target="_blank" class="formulario">Mozilla Firefox</a></td>
    </tr>
   
  </table>
</td>
              <td width="282" rowspan="3" align="left" valign="top" bgcolor="#FFFFFF"><table width="96%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="51" valign="middle">&nbsp;</td>
                    <td valign="middle">&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="9%" valign="middle">&nbsp;</td>
                    <td width="89%" valign="middle" class="textos"><strong>Liberbank</strong> offers its suppliers a direct communications channel which they can use to respond to requests for quotations made by the purchasing department.<br>
                        <br>
                  If you have already registered in this portal and have been authorised as a supplier, you can access the private suppliers' area by entering your login information.
                  <p>If you have still not registered in the portal, you can apply via the &quot;Ask for registration&quot; link. </p></td>
                    <td width="2%">&nbsp;</td>
                  </tr>
              </table></td>
              <td width="20" rowspan="3">&nbsp;</td>
              <td width="234" height="52" align="left" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
              <td width="7" rowspan="3" align="left" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>
            <tr bgcolor="#FFFFFF">
              <td align="left" valign="middle" bgcolor="#FFFFFF"><table width="250" border="0" align="left" cellpadding="0" cellspacing="0">
                        <tr valign="middle" bgcolor="#FFFFFF">
                          <td height="20" colspan="4"><div align="center" class="titulo2P">
                              <div align="left" class="titulo2">
                                <div align="center">SUPPLIER ACCESS</div>
                              </div>
                          </div></td>
                        </tr>
                        
                        <tr>
                          <td width="6" rowspan="7" bgcolor="#fafafa" >&nbsp;</td>
                          <td bgcolor="#fafafa" class="formulario">&nbsp;</td>
                          <td width="111" bgcolor="#fafafa">&nbsp;</td>
                          <td width="14" rowspan="7" bgcolor="#fafafa" >&nbsp;</td>
                        </tr>
                        <tr>
                         
                          <td width="119" height="25" bgcolor="#fafafa" class="textos">Company Code</td>
                          <td bgcolor="#fafafa" ><div align="right">
                              <input name="txtCIA" class="caja"  id="txtCia" size="15" maxlength="15" height="20" border="#cccccc">
                          </div></td>
                        </tr>
                        <tr>
                          <td width="119" height="25" bgcolor="#fafafa" class="textos" >User</td>
                          <td bgcolor="#fafafa" ><div align="right">
                              <input name="txtUSU" type="text" class="caja" size="15" maxlength="15"  height="20" >
                          </div></td>
                        </tr>
                        <tr>
                          <td width="119" height="25" bgcolor="#fafafa" class="textos">Password</td>
                          <td bgcolor="#fafafa"><div align="right">
                              <input name="txtPWD" type="password" class="caja" size="15" maxlength="15"  height="20" autocomplete="off">
                          </div></td>
                        </tr>
                        <tr>
                          <td height="28" colspan="2" bgcolor="#fafafa"><div align="right">
                              <input name="cmdEntrar" type="image" src="../images/entrar_off_eng.gif" width="96" height="22" >
                          </div></td>
                        </tr>
                        <tr>
                          <td height="31" colspan="2" bgcolor="#fafafa"><div align="center"><a href="javascript:void(null)" class="textos" onClick="recuerdePWD()">Forgot your access details?</a></div></td>
                        </tr>
                        <tr>
                          <td height="36" colspan="2" align="center" valign="middle" bgcolor="#fafafa" class="textos" ><a href="javascript:ventanaLogin('ENG')" class="textos" > Ask for registration</a></td>
                        </tr>
                      </table></td>
            </tr>
            <tr bgcolor="#FFFFFF">
              <td align="center" valign="top" bgcolor="#FFFFFF"><table width="185" border="0" align="center" cellpadding="0" cellspacing="0" class="subtexto">
                  
                  <tr>
                    <td width="16" height="21"><div align="left"></div></td>
                    <td width="169" height="21">Supplier call centre </td>
                  </tr>
                  <tr>
                    <td height="21"><div align="left"></div></td>
                    <td height="21">Tel. +34 902 996 926<br>
                        <a href="mailto:compras@liberbank.es" class="textos">compras@liberbank.es</a> </td>
                  </tr>
                  <tr>
                    <td class="subtexto">&nbsp;</td>
                    <td class="subtexto"><span class="textos10">Customer service schedule:</span><br>
                        <span class="textos10">Monday to thursday: 8:00 - 21:00<br>
                  Friday:  8:00-19:00</span></td>
                  </tr>
              </table></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="955" border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
            <tr>
              <td width="193" colspan="2">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
      
    </table>
</div>
</form>
</body>
</html>
