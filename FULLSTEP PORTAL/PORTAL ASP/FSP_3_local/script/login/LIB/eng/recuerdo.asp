﻿<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<html>
<head>
<link href="../estilos.css" rel="stylesheet" type="text/css"/>
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style>
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body>
<form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">    
    <table width="640" border="0" cellspacing="0" cellpadding="0" class="textos">
       <tr >
            <td height="100" colspan="2" bgcolor="#e9e4dc">
                <div align="right"><a href="http://www.liberbank.es" target="_blank"> <img src="../images/logo.png" border="0"/></a>
                </div>
            </td>
        </tr>
        <tr>
            <td width="20">&nbsp;</td>
            <td width="615" align="left">
                <br/>
                <span style="color:#666666;">
                     If you forgot your password, please fill in your user and email and  click on "Submit". You will receive a link to a page to create a new password.<br>
                </span>
                <br/>
                <table class="textos">
                    <tr>
                        <td>Company code:</td>
                        <td><input type="text" name="txtCia" id="txtCia" size="80" maxlength="100"/></td>
                    </tr>
                    <tr>
                        <td>User code:</td>
                        <td><input type="text" name="txtUsu" id="txtUsu" size="80" maxlength="100"/></td>
                    </tr>
                    <tr>
                        <td>Email address:</td>
                        <td><input type="text" name="txtEmail" id="txtEmail" size="80" maxlength="100"/></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="20">&nbsp;</td>
            <td>
                <br /><input type="submit" value="Submit" name="cmdEnviar" id="cmdEnviar"/>
                <br /><br />If you continue with difficulties to access the Portal or your email address has changed please contact our  Supplier Support Service. Phone # +34 902 996 926 
            </td>
        </tr>
    </table> 
    <input type="hidden" name="idioma" value="spa"/>
</form>
</body>
</html>
