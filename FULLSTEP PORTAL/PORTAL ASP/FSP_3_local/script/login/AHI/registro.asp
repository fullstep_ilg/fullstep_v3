﻿<%@ Language=VBScript %>
<html>

<head>

<!--#include file="../../common/XSS.asp"-->
<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/idioma.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->

<%
''' <summary>
''' Mostrar el detalle del registro
''' </summary>
''' <remarks>Llamada desde: login/login.asp		login/codigo instalaci�n/registro.asp	login/codigo instalaci�n/codigo idioma/registro.asp; Tiempo m�ximo: 0</remarks>
%>

<title>::Supplier Portal::</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo2 {font-weight: normal; color: #666666; text-decoration: none; font-style: normal;}
-->
</style>

<script src="../../common/formatos.js"></script>
<script language="JavaScript" src="../../common/ajax.js"></script>
<!--#include file="../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=ENG", "_blank")
window.close()

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=550,height=600,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>


<body bgcolor="#FFFFFF" topmargin="5" leftmargin="5" onload="init()">
<form name="frmAlta" id="frmAlta" method="post">

  <div align="center">
  <table border="0" width="80%">
      <tr>
        <td background="../images/fondotop_c.gif">&nbsp;</td>
      </tr>
      <tr>
        <td><a href="http://www.ajinomoto.com/" target="_blank"><img src="images/AHI__Logo.jpg" border="0" WIDTH="212" HEIGHT="70"></a></td>
      </tr>
      <tr>
        <td width="100%" height="33" align="left">        
	    <p align="left" class="subtextos"> To proceed with the registration proccess, the following contract must be read and accepted. :</p>        
               
      </td>
      </tr>
      <tr>
        <td align="left"><table border="0" align="left">
          <tr>
            <td valign="bottom"><div align="right"><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"></font></div>          <font color="#666666" size="1" face="Verdana">
              <textarea readonly="true" rows="11" name="S1" cols="63" style="font-family: Verdana; font-size: 8pt; text-align:justify; display:block; height:135px; line-height: 150%; list-style-type: lower-alpha">
FULLSTEP PORTAL TERMS AND CONDITIONS 
Revision: 9/20/2012 
Supplier register
 
Legal Notice 

Portal access 

By registering in the FULLSTEP purchasing Portal of AJINOMOTO HEARTLAND INC. (from here on referred to as AJINOMOTO HEARTLAND INC.) you acknowledge that you have read and accept the following terms and conditions. If you do not agree to all of the terms and conditions herein you will not be allowed to register. Every time you access and use the Portal, it is understood that you, fully and unreservedly accept and agree to, the contents of the terms and conditions in this Legal Notice.  As the primary user of the portal for your company, you are obliged by accepting the terms and conditions of this Legal Notice to ensure compliance by all users registered in your company, and agree to fully defend and indemnify AJINOMOTO HEARTLAND INC. from any claims of responsibility for damage those users may cause to your company or any third party because of their interactions with the Portal. 

Clauses 

1. Purpose of the AJINOMOTO HEARTLAND INC. FULLSTEP purchasing Portal 
The AJINOMOTO HEARTLAND INC. FULLSTEP purchasing portal is the means through which AJINOMOTO HEARTLAND INC. communicates with its suppliers to request offers, documents or any other commercial information it deems fit. At the same time AJINOMOTO HEARTLAND INC. reserves the right to use the portal to communicate any information it considers to be of its interest. 
AJINOMOTO HEARTLAND INC. works as a direct purchaser.  
Access or use of Portal and / or Services does not grant the SUPPLIER any rights in trademarks, commercial denominations or any other distinctive sign or symbol appearing in the portal, owned by AJINOMOTO HEARTLAND INC. or any third party. Therefore, all contents are the intellectual property of AJINOMOTO HEARTLAND INC. and nothing in these terms and conditions will be understood as granting any such rights to the SUPPLIER. 

2.  Purpose  
The purpose of this contract is to regulate relations between AJINOMOTO HEARTLAND INC. and the SUPPLIER in all aspects concerning use of the PORTAL. 

3. SUPPLIER obligations  
The following are obligations of the SUPPLIER: 

a. To provide whatever data is necessary for the adequate functioning of the system, and keep that data updated, communicating at the earliest possible time any modifications. 
b. To guarantee the authenticity of data provided as required to subscribe to the Services offered by the PORTAL. Also, the SUPPLIER will update all information provided to ensure it reflects, at all times, its accuracy. Therefore, the SUPPLIER is solely responsible for damages caused to AJINOMOTO HEARTLAND INC. as a consequence of inaccurate or false statements. 
c. To keep absolute confidentiality in relation to all information derived from the relations between the SUPPLIER and AJINOMOTO HEARTLAND INC.. 
d. To abstain from making any modification to the information-technology used by the Portal, or using it for purposes other than those for which it is intended. Also, the SUPPLIER will abstain from accessing unauthorized zones of the Portal. 
e. To comply with the commitments expressed in the information sent through the portal. If the SUPPLIER does not demonstrate due commercial diligence, or does not comply with contracted duties, AJINOMOTO HEARTLAND INC. reserves the right to exclude the SUPPLIER temporarily or permanently from the Portal. 
f. The SUPPLIER will indicate only material groups referring to goods or services that, at the time of acceptance of the contract, are commercialized, manufactured or distributed by the SUPPLIER and of commercial interest to AJINOMOTO HEARTLAND INC..  
g. The SUPPLIER agrees that offers sent through the Portal are considered to be valid written offers comparable to offers sent through any other traditional means (letters, e-mail). 
h. To use the Portal and its Services in compliance with the Law, this Legal Notice, and any applicable rules, regulations and orders, any instructions of which it is informed, and in accordance with generally accepted morals and customs, and public order.  The SUPPLIER will abstain from using the Portal or any of its Services for purposes which are illegal, prohibited in this Legal Notice, and/or damaging to rights and interests of third parties.   The SUPPLIER further agrees not to transmit, broadcast, or make available to others or third parties any information, data, contents, graphics, image and/or sound files, photographs, recordings, software and, in general, any type of material that: 
1. is contrary to fundamental rights and public liberties under the Constitution, International treaties and applicable laws; 
2. Induces, incites or promotes criminal acts, contrary to law, moral and generally accepted customs or public order; 
3. is false, inexact or may induce error, or constitutes illegal, deceitful or disparaging publicity; 
4. Is protected under intellectual or industrial property rights belonging to third parties without prior explicit and written consent. 
5. Contravenes laws and statutes for civil protection against defamation and invasion of privacy. 
6. Contravenes regulations about privacy and/or secrecy of communications. 
7. Can be considered as unfair competition or damages in any way the image of AJINOMOTO HEARTLAND INC. or third parties.  
8. Is affected by viruses or similar elements that can harm or stop the adequate performance of the Portal, the electronic equipment or their files and documents. 

4. Rights of the SUPPLIER  
The following are rights of the SUPPLIER: 
1.  To maintain a constant presence in AJINOMOTO HEARTLAND INC.’s database, as a registered supplier. 
2.  To receive offers according to the established rules.  

5. Obligations of AJINOMOTO HEARTLAND INC.  
The following are AJINOMOTO HEARTLAND INC.’s obligations: 
a. To maintain the information it deems fit constantly updated, provided that AJINOMOTO HEARTLAND INC. shall not be liable or responsible for any mistakes that may happen by chance or due to circumstances beyond its control. 
b. To keep absolute confidentiality of all information concerning the SUPPLIER, whether supplied directly or generated as a consequence of relationships between the SUPPLIER and AJINOMOTO HEARTLAND INC.. 
c. To provide the SUPPLIER, at any time, access to its data in the database, and not to provide the Supplier’s data to any third parties except to allow the SUPPLIER to present offers to be taken into consideration as supply alternatives. Also, to provide the SUPPLIER, at any time, access to its data inside the database, so that the SUPPLIER, upon prior written notice to AJINOMOTO HEARTLAND INC., may modify or delete it.    

6. Rights of AJINOMOTO HEARTLAND INC.   
a. The SUPPLIER hereby consents to AJINOMOTO HEARTLAND INC.’s sharing information concerning the SUPPLIER with its associated companies or companies belonging to the same group, in compliance, in any case, with legislation on personal data protection.  
b. In the event of breach by the SUPPLIER of its obligations, AJINOMOTO HEARTLAND INC. may demand full compliance with its obligations or cancel the contract, in accordance with the terms expressed in clause 8 of this Legal Notice. 

7. Limitation of responsibility  
a. AJINOMOTO HEARTLAND INC. shall not be liable or responsible for the availability of the service, network or technical failures that might cause an interruption or cuts in the Portal, nor shall AJINOMOTO HEARTLAND INC. be liable for any damages caused by the spreading of IT viruses or other elements in the system. 
b. AJINOMOTO HEARTLAND INC. shall not be liable or responsible for any violation of the Laws or generally agreed Customs, or of acts that contravene obligations established in this Legal Notice, by any employee of AJINOMOTO HEARTLAND INC., by the SUPPLIER, or by any Internet or Network user (hereafter a “Breach”). 
However, if AJINOMOTO HEARTLAND INC. has knowledge of any Breach AJINOMOTO HEARTLAND INC. shall take diligent, reasonable and appropriate measures to prevent or seek the discontinuance of the Breach.  
c. AJINOMOTO HEARTLAND INC. shall not be held liable or responsible for any unauthorized third parties’ interference with the use by the SUPPLIER of the Portal and its Services or access to information about the conditions and circumstances of such use by the SUPPLIER. 

8. Contract cancellation  
In the case of failure to comply with the obligations contained in this Legal Notice, the SUPPLIER or AJINOMOTO HEARTLAND INC. and its clients must notify the infringing party of such failure to comply, and allow a fifteen working day period to remedy the failure.  If the failure to comply is not remedied within that fifteen day period, the notifying party can choose between fulfillment or cancellation of this agreement. Should the notifying party choose to cancel the contract; both the SUPPLIER and AJINOMOTO HEARTLAND INC. or its clients agree that simple notification shall be enough for the cancellation to have full effect.  

9. Duration  
The present agreement shall have indefinite duration, provided that either party may terminate the agreement by send an e-mail indicating “PORTAL UNSUSCRIBE” to the address contact@fullstep.com. 

10. Notifications, Legislation and Jurisdiction 
a. Notifications between the parties may be carried out through any of the means permitted by the Law that allows having a record of reception including e-mail.  


        </textarea>
              </font> </td>
            </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td align="left" class="textos"><p><span class="subtextos">Do you accept the sign up contract to this  Purchasing Portal? ?</span><br>
                <br>
           Should you click on `No, I refuse´, the proccess will terminate and your company will not become an AJINOMOTO HEARTLAND INC. supplier. In order to continue with the registration proccess, you must click on `Yes, I accept´. If you wish to print out the contract, you may do so by clicking on the print button. <br>
      <br>
          
      <div align="center"></div>
          <table border="0" width="39%">
            <tr>
              <td width="41%" valign="middle"><p align="center">
                  <input type="submit" onclick="return alta()" value="Yes, I accept" id="submit1" name="submit1">
              </p></td>
              <td width="38%" valign="middle"><input type="button" onclick="no_alta()" value="No, I refuse" id="button1" name="button1">
              </td>
              <td width="21%" valign="middle"><input type="button" onclick="imprimir()" value="Print" id="button2" name="button2">
              </td>
            </tr>
          </table>
        <p align="center"></td>
      </tr>
  </table>
  <input type="hidden" name="ACEPTADO">
	  
</div>
</form>

</body>
<!--#include file="../../common/fsal_2.asp"-->
</html>
