﻿<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader "Pragma", "no-cache" %>
<% Response.charset="UTF-8"%>
<% Response.Expires = -1 %> 
<!--#include file="../common/XSS.asp"-->
<%
''' <summary>
''' Si has olvidado el password puedes pedir q te lo recuerden por mail.
'''	Las personalizaciones llaman a esta, por lo q se controla el XSS sin tocar las personalizaciones.
''' </summary>
''' <remarks>Llamada desde: default.asp de cada personalización; Tiempo máximo: 0,1</remarks>
if Request.ServerVariables("CONTENT_LENGTH")>0 then
    ciacod = request("txtCia")
    usucod = request("txtUsu")
	email = request("txtEmail")
	idioma = request("idioma")
	set oRaiz = createobject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
	i = oraiz.Conectar(Application("INSTANCIA"))
	
	if i = 1 then
		set oRaiz = nothing
		Response.Redirect application("RUTASEGURA") & "script/imposibleconectar.asp?Idioma=" & Idioma
	else
        CodPortal = cstr(Application ("NOMPORTAL"))
		Resul = oRaiz.RecuperarUsuario (APPLICATION("PORTAL"), ciacod, usucod, email, idioma, Application("PYME"),0,0,CodPortal) 
		set oRaiz = nothing

        if Resul = 0 then
            Response.Redirect application("RUTASEGURA") & "script/login/recuerdoKo.asp?idioma=" & Idioma
        else
		    if idioma = "" then
			    Response.Redirect application("RUTASEGURA") & "script/login/" & application("NOMPORTAL")  & "/recuerdoOk.asp"
		    else
			    Response.Redirect application("RUTASEGURA") & "script/login/" & application("NOMPORTAL")  & "/" & Idioma & "/recuerdoOk.asp"
		    end if
        end if
	end if		
end if
%>