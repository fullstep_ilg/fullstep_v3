﻿<%@ Language=VBScript %>
<!--#include file="../../login.asp"-->
<%		
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if

If Idioma="" then
	Idioma="ENG"
end if

%>

<html>
<head>
<META http-equiv=Content-Type content="text/html; charset=UTF-8">
<title>Purchasing Portal RSI</title>
<script language="javascript">
<!--
var mesos=new Array(13);
  mesos[1]="de enero";
  mesos[2]="de febrero";
  mesos[3]="de marzo";
  mesos[4]="de abril";
  mesos[5]="de mayo";
  mesos[6]="de junio";
  mesos[7]="de julio";
  mesos[8]="de agosto";
  mesos[9]="de septiembre";
  mesos[10]="de octubre";
  mesos[11]="de noviembre";
  mesos[12]="de diciembre";

var semana=new Array(8);
  semana[1]="Domingo";
  semana[2]="Lunes";
  semana[3]="Martes";
  semana[4]="Miércoles";
  semana[5]="Jueves";
  semana[6]="Viernes";
  semana[7]="Sábado";

var ladata=new Date();

var elmes=mesos[ladata.getMonth() + 1];

var eldia=semana[ladata.getDay() + 1];

var numero=ladata.getDate();

var any=ladata.getYear();

if (any < 2000)
any = any + 1900;

function ventanaLogin (IDI){

   window.open ("registro.asp?Idioma="+IDI,"","width=650,height=470,resizable=yes")

}
function ventanaSecundaria (URL){

   window.open(URL,"ventana1","width=650,height=400,scrollbars=no")

}
function ventanaAyuda (URL){

   window.open(URL,"ayuda","width=900,height=700,scrollbars=yes")

}

function popup (URL) {
 window.open("popup.htm","","width=500,height=650,scrollbars=NO")
 }
function recuerdePWD()
{
   window.open("recuerdo.asp","_blank","width=641,height=300,scrollbars=NO")
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
//-->
</script>
<link href="../estilos.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
body {
	background-image: url();
	background-color: #efefef;
	margin-top: 0px;
}
.Estilo1 {color: #666666}
-->
</style><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<div align="center">
  <table width="800" border="0" bgcolor="ffffff">
    <!--DWLayoutTable-->
    <tr>
      <td width="100%"><table width="100%" border="0">
          <tr>
            <td width="20%" rowspan="2"><a href="http://www.ono.es" target="_blank"><img src="../images/logo.gif" width="138" height="59" border="0"></a></td>
            <td width="80%">&nbsp;</td>
          </tr>
          <tr>
            <td align="right"><table width="290" border="0" background="../images/fondo_top.gif">
              <tr>
                <td width="171"><div align="right"><a href="javascript:ventanaAyuda('<%=application("RUTANORMAL")%>custom/<%=application("NOMPORTAL")%>/public/eng/manuales.htm')" class="textos Estilo1">Help</a></div></td>
                <td width="15"><div align="center" class="subtit">|</div></td>
                <td width="91"><a href="../default.asp" class="textos">Spanish</a></td>
              </tr>
            </table></td>
          </tr>
      </table></td>
    </tr>
    <tr>
      <td><table width="100%" border="0"><!--DWLayoutTable-->
          <tr>
            <td width="15" align="left" valign="top" class="titulo"><!--DWLayoutEmptyCell-->&nbsp;</td>
            <td align="left" valign="top" class="titulo">Supplier Portal </td>
            <td width="100%" rowspan="2" align="center" valign="top"><table width="100%" border="0"><!--DWLayoutTable-->
                <tr>
                  <td valign="middle"><table width="100%" height="379" border="0"><!--DWLayoutTable-->
                    <tr>
                      <td width="100%" valign="top"><span class="textos"><strong>RSI</strong> offers its suppliers a direct communications channel which they can use to respond to requests for quotations made by the purchasing department.
                        If you have still not registered in the portal, you can apply via the &quot;Register&quot; link</span></td>
                    </tr>
                    <tr>
                      <td height="161" align="left"><table width="126" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                          <td width="4"></td>
                          <td width="47"></td>
                          <td rowspan="3" width="149"><div align="left"><a href="http://www.fullstep.com" target="_blank"><img src="../images/fullgiro_web_blanco.gif" alt="FULLSTEP" width="40" height="40" hspace="5" border="0"></a></div></td>
                        </tr>
                        <tr>
                          <td width="4">&nbsp;</td>
                          <td class="textos" width="47"><div align="left"><font color="#666666">powered</font></div></td>
                        </tr>
                        <tr>
                          <td width="4">&nbsp;</td>
                          <td class="textos" valign="top" width="47"><div align="left"><font color="#666666">by</font></div></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table>
                  </td>
                  <td valign="top"><table width="100%" border="0">
                    <tr>
                      <td><table width="200" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td colspan="3" valign="top"><img src="../images/acceso_eng.gif" width="300" height="22"></td>
            </tr>
            <tr>
              <td width="1" rowspan="2" align="right" valign="top"><div align="right"><img src="../images/form_izq.gif" width="1" height="145"></div></td>
              <td align="center"><form name="frmLogin" id="frmLogin" method="post" action="default.asp">
                  <div align="center">
                    <table width="250" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <input type="hidden" id="Idioma" name="Idioma" value="SPA">
                        <td width="5">&nbsp;</td>
                        <td height="24" class="textos">Company Code </td>
                        <td width="88" bgcolor="edece8"><input id="txtCia" name="txtCIA" maxlength="20" size="25" style="border: ridge #cccccc 1px; background-color: white; font-size: 10px; font-family: Arial">
                        </td>
                        <td width="11">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="5">&nbsp;</td>
                        <td height="24" class="textos">User</td>
                        <td width="88" bgcolor="edece8"><input type="text" name="txtUSU" size="25" style="border: ridge #cccccc 1px; background-color: white; font-size: 10px; font-family: Arial">
                        </td>
                        <td width="11">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="5" height="24">&nbsp;</td>
                        <td height="24" class="textos">Password</td>
                        <td width="88" bgcolor="edece8"><input name="txtPWD" type="password" maxlength="20" size="25" style="border: ridge #cccccc 1px; background-color: white; font-size: 10px; font-family: Arial">
                        </td>
                        <td width="11">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="5">&nbsp;</td>
                        <td height="24">&nbsp;</td>
                        <td align="right"><input type="image" name="cmdEntrar" src="../images/login.gif" border="0" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cmdEntrar','','../images/login.gif',1)" width="56" height="13">
                        <td width="11">&nbsp;</td>
                      </tr>
                      <tr>
                        <td align="center"></td>
                        <td colspan="2" class="logo"><div align="left"><a href="javascript:void(null)" class="registro" onClick="recuerdePWD()">Forgotten your login details  ?</a></div></td>
                        <td align="center">&nbsp;</td>
                      </tr>
                    </table>
                    <input type="hidden" id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
                    <input type="hidden" id="CiaComp" name="CiaComp" value="<%=CiaComp%>">
                  </div>
              </form></td>
              <td rowspan="2" align="right" valign="top"><img src="../images/form_dcha.gif" width="1" height="145"></td>
            </tr>
            <tr>
              <td><table width="80%" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tr>
                    <td align="center" valign="top"><div align="left" class="logo"><a href="javascript:ventanaLogin('SPA')" class="registro">Register</a> </div></td>
                  </tr>
              </table></td>
            </tr>
            <tr>
              <td colspan="3" valign="top"><img src="../images/bottom.gif" width="300" height="17"></td>
            </tr>
          </table></td>
                    </tr>
                    <tr>
                      <td><table width="200" height="163" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td colspan="3" valign="top"><img src="../images/soporte_eng.gif" width="300" height="22"></td>
                        </tr>
                        <tr>
                          <td width="28" rowspan="2" align="right" valign="top"><div align="left"><img src="../images/form_izq.gif" width="1" height="145"></div></td>
                          <td width="259" height="36" align="center"><div align="center" class="subtitulo"><strong>Tel. 902 996 926</strong> </div></td>
                          <td width="13" rowspan="2" align="right" valign="top"><img src="../images/form_dcha.gif" width="1" height="145"></td>
                        </tr>
                        <tr>
                          <td height="86" valign="top"><span class="textos">Opening hours (Central European Time) :<br>
                                <br>
            Monday to Thursday:<br>
            from 9:00 to 14:00 and 15:00 to 18:00<br>
            Fridays: from 9:00 to 15:00<br>
            <a href="mailto:rsi_proveedores@cajarural.com">rsi_proveedores@cajarural.com</a></span>
                              </td>
                        </tr>
                        <tr>
                          <td colspan="3" valign="top"><img src="../images/bottom.gif" width="300" height="17"></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr><tr><td height="3"></td><td><!--DWLayoutEmptyCell-->&nbsp;</td></tr>
                  </table></td>
          </tr>
          <tr>
            <td width="15" align="left" valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
            <td width="42%" align="left" valign="top"><img src="../images/imagen-portal.jpg" WIDTH="300" HEIGHT="340">          <p class="textos">&nbsp;</p></td>
          </tr>

      </table></td>
    </tr>
    
  </table>
<table width="800" border="0">
  <tr>
    <td bgcolor="#000000"><table width="100%" border="0">
      <tr>
        <td width="30%"><span class="subtit">©2009 |</span><span class="subtitulo"> <a href="javascript:ventanaSecundaria('<%=application("RUTANORMAL")%>custom/<%=application("NOMPORTAL")%>/public/aviso legal.htm')" class="textos"></a></span><span class="subtit"><a href="javascript:ventanaSecundaria('<%=application("RUTANORMAL")%>custom/<%=application("NOMPORTAL")%>/public/eng/aviso legal.htm')" class="subtit">Legal notice</a></span></td>
        <td width="32%">&nbsp;</td>
        <td width="38%" class="subtit">Optimised for<a href="http://www.microsoft.com/spain/windows/internet-explorer/download-ie.aspx" target="_blank" class="subtit"> Internet Explorer 6.0 or higher</a></td>
      </tr>
    </table></td>
  </tr>
</table>

</div>
</body>
</html>
