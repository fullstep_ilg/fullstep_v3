﻿<%@ Language=VBScript %>
<!--#include file="../../login.asp"-->
<%				
''' <summary>
''' Pantalla inicial de la personalizaci�n, es la q llama a la pantalla de login
''' </summary>
''' <remarks>Llamada desde: registro/registrarproveedor.asp		proveedor/default.asp
'''		login/personalizacion/login.asp		; Tiempo m�ximo: 0,1</remarks>
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if

If Idioma="" then
	Idioma="ENG"
end if

%>

<html>
<head>
<META http-equiv=Content-Type content="text/html; charset=UTF-8">
<title>:: Purchasing Portal RSI ::</title>
<script language="javascript">
<!--
var mesos=new Array(13);
  mesos[1]="de enero";
  mesos[2]="de febrero";
  mesos[3]="de marzo";
  mesos[4]="de abril";
  mesos[5]="de mayo";
  mesos[6]="de junio";
  mesos[7]="de julio";
  mesos[8]="de agosto";
  mesos[9]="de septiembre";
  mesos[10]="de octubre";
  mesos[11]="de noviembre";
  mesos[12]="de diciembre";

var semana=new Array(8);
  semana[1]="Domingo";
  semana[2]="Lunes";
  semana[3]="Martes";
  semana[4] = "Miércoles";
  semana[5] = "Jueves";
  semana[6] = "Viernes";
  semana[7] = "Sábado";

var ladata=new Date();

var elmes=mesos[ladata.getMonth() + 1];

var eldia=semana[ladata.getDay() + 1];

var numero=ladata.getDate();

var any=ladata.getYear();

if (any < 2000)
any = any + 1900;

function ventanaLogin (IDI){

   window.open ("registro.asp?Idioma="+IDI,"","width=650,height=520,resizable=yes")

}
function ventanaSecundaria (URL){

   window.open(URL,"ventana1","width=650,height=400,scrollbars=no")

}
function ventanaAyuda (URL){

   window.open(URL,"ayuda","width=900,height=700,scrollbars=yes")

}

function popup (URL) {
 window.open("popup.htm","","width=500,height=650,scrollbars=NO")
 }
function recuerdePWD()
{
   window.open("recuerdo.asp","_blank","width=641,height=370,scrollbars=NO")
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
//''' <summary>
//''' Obtener el objeto
//''' </summary>
//''' <remarks>Llamada desde: MM_swapImage ; Tiempo máximo: 0</remarks>
function MM_findObj(n, d) { //v4.01
    var p, i, x; if (!d) d = document; if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
        d = parent.frames[n.substring(p + 1)].document; n = n.substring(0, p);
    }
    if (!(x = d[n]) && d.all) x = d.all[n]; for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
    for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
    if (!x && d.getElementById) x = d.getElementById(n); return x;
}
//''' <summary>
//''' onMouseOver cambiar la imagen
//''' </summary>
//''' <remarks>Llamada desde: cmdEntrar.onMouseOver ; Tiempo máximo: 0</remarks>
function MM_swapImage() { //v3.0
    var i, j = 0, x, a = MM_swapImage.arguments; document.MM_sr = new Array; for (i = 0; i < (a.length - 2); i += 3)
        if ((x = MM_findObj(a[i])) != null) { document.MM_sr[j++] = x; if (!x.oSrc) x.oSrc = x.src; x.src = a[i + 2]; }
}
//-->
</script>
<link href="../estilos.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
body {
	background-image: url();
	background-color: #efefef;
	margin-top: 0px;
}
.Estilo1 {color: #666666}
-->
</style><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></head>
<body>
<div align="center">
  <table width="980" border="0" bgcolor="ffffff">
    <!--DWLayoutTable-->
    <tr>
      <td width="100%" height="117"><table width="100%" border="0" cellspacing="0">
          <tr>
            <td height="95" colspan="2"><div align="left">
              <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="950" height="95">
                <param name="movie" value="../images/cabecera.swf">
                <param name="quality" value="high">
                <embed src="../images/cabecera.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="950" height="95"></embed>
              </object>
            </div></td>
          </tr>
          <tr>
            <td colspan="2"><img src="../images/punto.gif" width="980" height="4"></td>
          </tr>
          <tr>
            <td height="139" align="right" valign="middle"><table width="98%"  border="0">
              <tr>
                <td height="30" class="subtitulo">Procurement and Purchasing Platform</td>
              </tr>
              <tr>
                <td height="70" valign="bottom" class="textos">RSI offers its suppliers a direct communications channel which they can use to respond to requests for quotations made by the purchasing department. If you have still not registered in the portal, you can apply via the &quot;Register&quot; link</td>
              </tr>
            </table></td>
            <td width="39%" align="right"><table width="140" height="127" border="0">
              <tr>
                <td colspan="3" valign="top" bgcolor="#FFFFFF"><img src="../images/logo_pac.gif" width="138" height="81"></td>
                </tr>
              <tr>
                <td width="59"><div align="right"><a href="javascript:ventanaAyuda('<%=application("RUTANORMAL")%>custom/<%=application("NOMPORTAL")%>/public/eng/manuales.htm')" class="textos Estilo1">Help</a></div></td>
                <td width="15"><div align="center" class="subtit">|</div></td>
                <td width="52"><a href="../default.asp?Idioma=SPA" class="textos">Español</a></td>
              </tr>
            </table></td>
          </tr>
      </table></td>
    </tr>
    <tr>
      <td><table width="100%" border="0"><!--DWLayoutTable-->
          <tr>
            <td width="15" align="left" valign="top" class="titulo"><!--DWLayoutEmptyCell-->&nbsp;</td>
            <td width="42%" align="left" valign="top" class="titulo">          <p class="textos"><img src="../images/images.png"></p></td>
            <td width="100%" rowspan="2" align="center" valign="top"><table width="100%" border="0"><!--DWLayoutTable-->
                <tr>
                  <td valign="middle"><!--DWLayoutEmptyCell-->&nbsp;</td>
                  <td align="center"><table width="100%" border="0">
                    <tr>
                      <td align="right"><table width="200" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td colspan="3" valign="top"><img src="../images/acceso_eng.gif" width="300" height="22"></td>
            </tr>
            <tr>
              <td width="1" rowspan="2" align="right" valign="top"><div align="right"><img src="../images/form_izq.gif" width="1" height="145"></div></td>
              <td align="center"><form name="frmLogin" id="frmLogin" method="post" action="default.asp">
                  <div align="center">
                    <table width="250" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <input type="hidden" id="Idioma" name="Idioma" value="SPA">
                        <td width="5">&nbsp;</td>
                        <td height="24" class="textos">Company Code</td>
                        <td width="88" bgcolor="edece8"><input id="txtCia" name="txtCIA" maxlength="20" size="25" style="border: ridge #cccccc 1px; background-color: white; font-size: 10px; font-family: Arial">
                        </td>
                        <td width="11">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="5">&nbsp;</td>
                        <td height="24" class="textos">User</td>
                        <td width="88" bgcolor="edece8"><input type="text" name="txtUSU" size="25" style="border: ridge #cccccc 1px; background-color: white; font-size: 10px; font-family: Arial">
                        </td>
                        <td width="11">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="5" height="24">&nbsp;</td>
                        <td height="24" class="textos">Password</td>
                        <td width="88" bgcolor="edece8"><input name="txtPWD" type="password" maxlength="20" size="25" style="border: ridge #cccccc 1px; background-color: white; font-size: 10px; font-family: Arial" autocomplete="off">
                        </td>
                        <td width="11">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="5">&nbsp;</td>
                        <td height="24">&nbsp;</td>
                        <td align="right"><input type="image" name="cmdEntrar" src="../images/login.gif" border="0" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cmdEntrar','','../images/login.gif',1)" width="56" height="13">
                        <td width="11">&nbsp;</td>
                      </tr>
                      <tr>
                        <td align="center"></td>
                        <td colspan="2" class="logo"><div align="left"><a href="javascript:void(null)" class="registro" onClick="recuerdePWD()">Forgotten your login details?</a></div></td>
                        <td align="center">&nbsp;</td>
                      </tr>
                    </table>
                    <input type="hidden" id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
                    <input type="hidden" id="CiaComp" name="CiaComp" value="<%=CiaComp%>">
                  </div>
              </form></td>
              <td rowspan="2" align="right" valign="top"><img src="../images/form_dcha.gif" width="1" height="145"></td>
            </tr>
            <tr>
              <td><table width="80%" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tr>
                    <td align="center" valign="top"><div align="left" class="logo"><a href="javascript:ventanaLogin('ENG')" class="registro">Register </a> </div></td>
                  </tr>
              </table></td>
            </tr>
            <tr>
              <td colspan="3" valign="top"><img src="../images/bottom.gif" width="300" height="17"></td>
            </tr>
          </table></td>
                    </tr>
                    <tr>
                      <td align="right"><table width="200" height="163" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td colspan="3" valign="top"><img src="../images/soporte_eng.gif" width="300" height="22"></td>
                        </tr>
                        <tr>
                          <td width="28" rowspan="2" align="right" valign="top"><div align="left"><img src="../images/form_izq.gif" width="1" height="125"></div></td>
                          <td width="259" height="36" align="center"><div align="center" class="subtitulo"><strong>Tel. +34 918 070 171</strong> </div></td>
                          <td width="13" rowspan="2" align="right" valign="top"><img src="../images/form_dcha.gif" width="1" height="125"></td>
                        </tr>
                        <tr>
                          <td valign="top"><span class="textos">Opening hours (Central European Time) :<br>
                              <br>
Monday to Thursday:<br>
from 9:00 to 14:00 and 15:00 to 18:00<br>
Fridays: from 9:00 to 15:00<br>
<a href="mailto:rsi_proveedores@cajarural.com">rsi_proveedores@cajarural.com</a> 
            </span></td>
                        </tr>
                        <tr>
                          <td align="right" valign="top"><div align="left"><img src="../images/form_izq.gif" width="1" height="45"></div></td>
                          <td valign="top"><table width="126" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                              <td width="4"></td>
                              <td width="47"></td>
                              <td rowspan="3" width="149"><div align="left"><a href="http://www.fullstep.com/portal-de-compras/" target="_blank"><img src="../images/logoFullStep.png"  alt="logo-Fullstep"/></a> </div></td>
                            </tr>
                           
                          </table></td>
                          <td align="right" valign="top"><img src="../images/form_dcha.gif" width="1" height="45"></td>
                        </tr>
                        <tr>
                          <td colspan="3" valign="top"><img src="../images/bottom.gif" width="300" height="17"></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr><tr><td height="3"></td><td><!--DWLayoutEmptyCell-->&nbsp;</td></tr>
                  </table></td>
          </tr>
          <tr>
            <td width="15" align="left" valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
            <td width="42%" align="left" valign="top" class="textos"><!--DWLayoutEmptyCell-->&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
            <td colspan="2" align="left" valign="top" class="textos"><div align="center" class="subtit"><a href="javascript:ventanaSecundaria('<%=application("RUTANORMAL")%>custom/<%=application("NOMPORTAL")%>/public/eng/aviso legal.htm')">Legal notice</a> 
            | <a href="<%=application("RUTANORMAL")%>script/politicacookies/politicacookies.asp?Idioma=ENG" class="subtit">Cookies policy</a>
            | AVDA. DE LA INDUSTRIA, 23 / TRES CANTOS, 28760 (MADRID) - Spain / TEL. +34 91 80 70 100 | <a href="http://www.microsoft.com/spain/windows/internet-explorer/download-ie.aspx" target="_blank" class="subtit">Internet explorer 6.0 &oacute; superior</a></div></td>
          </tr>

      </table></td>
    </tr>
    
  </table>
</div>
</body>
</html>
