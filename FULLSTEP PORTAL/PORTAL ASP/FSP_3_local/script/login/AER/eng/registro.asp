﻿<%@ Language=VBScript %>
<!--#include file="../../../common/XSS.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700"></script>
<script src="../js/jquery-1.9.0.js"></script>
<script src="../js/jquery-1.9.0.min.js"></script>
<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=ENG", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body>

<div id="cont_gen">
	<div id="izd_gen"> <a href="http://www.aernnova.com" target="_blank"><img src="../images/logo.png" border="0"></a>
    </div>
    <div id="drc_gen">
    	<h1>REGISTRATION</h1>
        <div class="int">
        	<p> 
                To proceed with the registration proccess, the <span class="rojo">following contract must be read and accepted</span> :
               
            </p>
            <p style="height:150px; padding:0 5px; overflow:auto; background:#FFF; border:#CCC 1px solid">
               PORTAL TERMS AND CONDITIONS<br/><br/>
Supplier register<br/><br/>
Legal Notice - Supplier Portal access<br/><br/>
Definitions
<br/><br/>
Supplier: Natural or legal person subject to make and deliver offers and/or any other activity on the Supplier Portal, to supply materials and/or provide services to Aernnova Aerospace, SAU or any company that belongs to the same group that Aernnova Aerospace, SAU pertains to (hereinafter AERNNOVA) which is subject to the terms and conditions set forth herein.
<br/><br/>
User: Person with authority to use the application and acting with a purpose commended by the SUPPLIER (previously authorized) to operate through the Portal. The SUPPLIER may authorize one or more persons. 
<br/><br/>
Registering in the Supplier Portal implies previous reading and acceptance of the following clauses. Without expressing full compliance with the clauses you will not be allowed to register. Every time you access and use the Portal, it is understood you agree, fully and unreservedly, with the entire content of the present Terms and Conditions. Also, being the main user of your company, you are obliged by accepting this Legal Notice to ensure compliance by all users registered authorized in your company, fully exonerating AERNNOVA from any responsibility of the damage those users may cause to the SUPPLIER or any other because of their interactions with/in the Portal.<br/><br/>
Clauses<br/><br/>
1.Purpose of the AERNNOVA’S Supplier Portal<br/><br/>
Supplier Portal is the mean through which AERNNOVA communicates with its SUPPLIERS to request offers, documents or any other commercial information it has been deemed necessary. At the same time AERNNOVA reserves the right to use the Portal to communicate to any person and any information considered of interest.<br/><br/>
Access or use of Portal and/or Services does not give the SUPPLIER any rights on trademarks, commercial denominations or any other distinctive sign appearing in the Portal, being in all cases AERNNOVA or third party’s property. Therefore, all contents are AERNNOVA’s or third party intellectual property, and will not be understood as transferred to the SUPPLIER.<br/><br/>
Full or partial reproduction, in any form or mean, of the content of this site is forbidden to the SUPPLIER, including mechanic, electronic, reprographic or any other, also any act of diffusion, public communication or distribution without the previous written authorization of AERNNOVA or third party owners of the belonging rights.  <br/><br/>
The use and access to the Portal does not affect the contractual relationship between the Parties, particularly, the applicable law in the contractual relationship between AERNNOVA and the SUPPLIER through the corresponding agreement neither to the competent Court or Tribunal determined from the contractual relationship.<br /><br />
2. Purpose <br/><br/>
The purpose of this document is to regulate relations between AERNNOVA and the SUPPLIER, in all aspects concerning the use of the Portal.<br/><br/>
3. SUPPLIER obligations <br/><br/>
The following are obligations of the SUPPLIER:<br/><br/>
a. Provide the required information for the adequate functioning of the system, and maintain it updated, communicating any modification as soon as possible. <br/><br/>
b. Guarantee the authenticity of data provided as a consequence of forms necessary to subscribe to the Services. Also, the SUPPLIER will update all information provided to ensure it reflects, at all times, its real situation. Therefore, the SUPPLIER is solely responsible of damages caused to AERNNOVA or AERNNOVA’s Clients as a consequence of inexact or false statements.<br/><br/>
c. Keep absolute confidentiality in relation to all information derived from the relations between the SUPPLIER and AERNNOVA.<br/><br/>
d. Abstain from doing any technological modification or manipulation in the use of the Portal, as well as using it with different purposes from those for which it is intended. Also, the SUPPLIER will abstain from accessing unauthorized areas of the Portal.<br/><br/>
e. Comply loyally the commitments expressed in the information sent through the Portal. If the SUPPLIER does not demonstrate due commercial diligence, or does not comply with contracted duties, AERNNOVA reserves the right to exclude temporarily or permanently the SUPPLIER from the Portal.<br/><br/>
f. The SUPPLIER will indicate only those material groups referring to goods or services that, at the time of acceptance of the contract, are commercialized, manufactured or distributed by the SUPPLIER and of commercial interest to AERNNOVA and its Clients. <br/><br/>
g. The SUPPLIER accepts that offers sent through the Portal are considered to be of the same rank and validity as offers sent through any other traditional means (letters, fax).<br/><br/>
h. The SUPPLIER is obliged to use the Portal and its Services in compliance with Laws, this Legal Notice, and the rest of regulations and instructions of which it is informed, as well as in accordance to generally accepted morals and customs, and public order.
<br/><br/>
Therefore, the SUPPLIER will abstain from using the Portal or any of its Services with illegal purposes, prohibited in this Legal Notice, and/or damaging to rights and interests of third parties. In particular, and merely as a matter of indication, SUPPLIER agrees not to transmit, broadcast, or make available to others or third parties any information, data, contents, graphics, image and/or sound files, photographs, recordings, software and, in general, any type of material that:<br/><br/> 
In particular, and merely as a matter of indication, the SUPPLIER agrees not to transmit, broadcast, or make available to others or third parties any information, data, contents, graphics, image and/or sound files, photographs, recordings, software and, in general, any type of material that:<br/><br/>
1. Is contrary to fundamental rights and public liberties under the Spanish Constitution, International treaties and applicable laws;<br/><br/>
2. Induces, incites or promotes criminal acts, contrary to law, moral and generally accepted customs or public order;<br/><br/>
3. Is false, inexact or may induce to error, or constitutes illegal, deceitful or disloyal publicity;<br/><br/>
4. Is protected under intellectual or industrial property rights belonging to third parties without prior explicit and written consent;<br/><br/>
5. Contravenes laws and statutes for civil protection against defamation and invasion of privacy;<br/><br/>
6. Contravenes regulations about privacy and/or secrecy of communications;<br/><br/>
7. Can be considered as unfair competition or damages in any way the image of AERNNOVA or third parties; and <br/><br/>
8. Is affected by viruses or similar elements that can harm or stop the Portal’s adequate performance, the electronic equipment or their files and documents.
<br/><br/>
4. Rights of the SUPPLIER <br/><br/>
The following are rights of the SUPPLIER:<br/><br/>
1. Maintain a constant presence in AERNNOVA’s database, as a registered supplier.<br/><br/>
2. Receive offers according to the established rules. <br/><br/>
5. Obligations of AERNNOVA<br/><br/>
The following are AERNNOVA obligations:<br/><br/>
a. Maintain in the Portal the information that has being deem convenient, without being liable or responsible of any mistakes in the same and neither about the decisions taken by SUPPLIERS using this information. SUPPLIER shall ensure that the information has been updated.     <br/><br/>
b. Keep absolute confidentiality of all information concerning the SUPPLIER, both if supplied or generated as a consequence of relationships between the SUPPLIER and AERNNOVA.<br/><br/>
c. Provide the SUPPLIER, at any time, the situation of its data inside the database, and not to provide it to any third party except to allow the SUPPLIER to present offers to be taken into consideration as supply alternatives. Also, provide the SUPPLIER, at any time, the situation of its data inside the database, so that the SUPPLIER, with previous written notification, modifies or deletes them.<br/><br/>
6. Rights of AERNNOVA -PERSONAL DATA PROTECTION- <br/><br/>
a. The User gives its explicit consent to AERNNOVA or any company that belongs to the same group that Aernnova Aerospace, SAU pertains to communicate information concerning the SUPPLIER to associated companies or companies belonging to the same group it belongs to, as well as others with which it may sign agreements with the only aim of providing the best services, in compliance, in any case, with Spanish legislation on personal data protection. Also, the User agrees on AERNNOVA or its associates, subsidiaries or affiliates, or companies belonging to the same group, sending information about goods or services they commercialize, directly or indirectly, or that will be commercialized. User acceptance of the data may be transferred in the way described in this paragraph is always revocable, without retroactive effect, in accordance with Law 15/1999 of 13th of December.<br/><br/>
b. Choose, in case of infringement by the SUPPLIER of its obligations, between demanding full compliance with its obligations or cancellation of the contract, in the terms expressed in Clause 8 of this Legal Notice.

<br/><br/>
7. Limitation of responsibility <br/><br/>
a. AERNNOVA shall not be liable or responsible for the availability of the service, network or technical failures that might cause an interruption or cuts in the Portal. 
Equally, AERNNOVA shall not be liable of any damages or prejudices caused by the spreading of IT viruses or other elements in the system.<br/><br/>
b. AERNNOVA shall not be liable or responsible of any payments and/or complaints that derive from the acts of Clients concerning the agreement between the Client(s) and the SUPPLIER.<br/><br/>
c. AERNNOVA shall not be liable or responsible of any infringement of the Laws or generally agreed Customs, or of acts that contravene obligations established in this Legal Notice, by any employee of AERNNOVA, by the SUPPLIER, or by any Internet or Network User.<br/><br/>
However, when AERNNOVA have knowledge of any conduct the previous paragraph refers to, it shall adopt the necessary measures to resolve with the upmost diligence and urgency the conflicts caused. <br/><br/>
d. AERNNOVA shall not be held liable or responsible of any interference by unauthorized third parties in the knowledge of the conditions and circumstances of use that the SUPPLIER shall do of the Portal and its Services.
<br/><br/>
8. Contract cancellation <br/><br/>
In the case of failure to comply with the obligations contained in this Legal Notice, the SUPPLIER or AERNNOVA must notify the breach to the infringing party, with a fifteen (15) working day period to repair it. After that period and if the infringement has not be repaired, the other party can choose between the fulfilling or cancellation of this agreement, with the corresponding indemnification of damages and prejudices in both cases according with article 1.124 of the Spanish Civil Code. Should it choose to cancel the contract, both the SUPPLIER and AERNNOVA agree that simple notification shall be enough for the cancellation to have full effect.  <br/><br/>
9. Duration <br/><br/>
The present agreement shall have indefinite duration as long as no party express willingness to cancel the contract with a prior written notice not shorter than one (1) month.<br/><br/>
The proceeding to cancel will be to send an e-mail indicating “Cancel registration” to the address help_desk@aernnova.com<br/><br/>
10. Notifications, Legislation and Jurisdiction<br/><br/>
a. Notifications between the parties may be carried out through any of the means admitted by the Law, which allows having a record of reception, including fax.<br/><br/>
b. Changes in addresses and fax numbers shall be notified in writing, and shall not have effect until two (2) working days from reception. In any case, the new address and fax will necessarily be inside Spanish territory.<br/><br/>
c. The relationships between AERNNOVA and the SUPPLIER derived from the use of the Portal and the Services carried out inside it, will submit to adequate Spanish legislation and jurisdiction. 
 

			</p>
            <p style="text-align:right"><a href="registro_texto.htm" class="bt_registro" target="blank">Print</a> </p>
<div class="caja_acepto">


<!--<form action="" method="post" style="width:400px;">//-->
<form name="frmAlta" id="frmAlta" method="post" style="width:400px;">
  <span id="sprycheckbox1">
<label>
  <input type="checkbox" name="opcion1" id="opcion1" required>
</label>
<span class="checkboxRequiredMsg">Select to continue.</span></span><span class="rojo">I have read and accept</span> the General terms and conditions of use of the Supplier Portal.<br />
<br />

<button type="button" class="bt_registro" id="submit1" name="submit1">Next</button>
<br>
</form>


</div>
<div style="clear:both;"></div>
    	</div>    
    </div>
</div>
<script type="text/javascript">

$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked') )
       {
        $('#frmAlta').submit();
        alta();}
    else
       alert("You must accept the general terms and conditions of use of the Portal");
    
});

</script>
 			
</body>

</html>
