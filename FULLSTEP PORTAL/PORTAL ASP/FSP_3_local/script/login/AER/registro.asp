﻿<%@ Language=VBScript %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/idioma.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700"></script>
<script src="js/jquery-1.9.0.min.js"></script>
<!-- <script src="SpryAssets/SpryValidationCheckbox.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationCheckbox.css" rel="stylesheet" type="text/css" /> -->
<script src="../../common/formatos.js"></script>
<script language="JavaScript" src="../../common/ajax.js"></script>
<!--#include file="../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
/*
''' <summary>
''' Lanzar la pantalla de registro de nuevos proveedores de Portal
''' </summary>     
''' <remarks>Llamada desde: submit1/onclick ; Tiempo máximo: 0</remarks>*/
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=SPA", "_blank", "width=730,height=635")

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=760,height=700,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>
<body  onload="init()">

<div id="cont_gen">
	<div id="izd_gen"> <a href="http://www.aernnova.com" target="_blank"><img src="images/logo.png" border="0"></a>
    </div>
    <div id="drc_gen">
    	<h1>SOLICITAR REGISTRO</h1>
        <div class="int">
        	<p> 
                Para continuar con el proceso de alta es imprescindible la aceptación de las
                <span class="rojo">CONDICIONES DE USO DEL PORTAL DE PROVEEDORES</span>.
            </p>
            <p style="height:150px; padding:0 5px; overflow:auto; background:#FFF; border:#CCC 1px solid">
               CONDICIONES GENERALES DE USO DEL PORTAL DE PROVEEDORES<br /><br />
IMPORTANTE LEA ATENTAMENTE<br /><br />

Alta de proveedores<br /><br />

 Aviso Legal
<br /><br />
 Acceso al Portal de Proveedores<br /><br />
Definiciones<br /><br />
Proveedor: Persona física o jurídica sujeta a realizar y emitir ofertas y/o cualquier otra actividad disponible en el Portal, a través del Portal de Proveedores, con el objeto de suministrar materiales y/o prestar servicios, a cualquiera de las empresas pertenecientes al mismo Grupo al que Aernnova Aerospace, SAU pertenece (en adelante AERNNOVA) y que queda sujeta a los términos y condiciones fijadas en el presente.<br /><br />
Usuario: Persona física que actúa con un propósito encomendado por el PROVEEDOR y que habrá sido previamente autorizado por éste para operar a través del Portal. El PROVEEDOR podrá autorizar a uno o varios usuarios.

<br /><br />
El alta como PROVEEDOR en el Portal de Proveedores de AERNNOVA está condicionada a la previa lectura y aceptación de las siguientes cláusulas. Sin expresar su conformidad con las mismas no podrá registrarse. Siempre que se acceda y utilice el Portal, se entenderá que está de acuerdo, de forma expresa, plena y sin reservas, con la totalidad del contenido de este Aviso Legal. Asimismo, al ser Ud. el Usuario Principal del Proveedor, queda obligado a aceptar la conformidad de este Aviso Legal y a velar por el cumplimiento por parte de todos los usuarios que se den de alta en su empresa, liberando a AERNNOVA de toda responsabilidad por los posibles perjuicios que dichos usuarios pudieran causar al PROVEEDOR o cualquier otra debido a su actuación o actuaciones en el Portal.<br />


Cláusulas<br />

1. Objeto del Portal de Proveedores de AERNNOVA  <br />

 El Portal de Proveedores es el medio a través del cual AERNNOVA se comunica con sus PROVEEDORES para solicitarles ofertas y/o documentos, así como aquella información comercial que estime oportuna. Al mismo tiempo, AERNNOVA puede utilizar el Portal para remitirle aquella información que considere de su interés.<br /><br />

El uso o acceso al Portal y/o a los Servicios, no atribuyen al Proveedor ningún derecho sobre las marcas, nombres comerciales o signos distintivos que aparezcan en el mismo, siendo éstos propiedad de AERNNOVA o de sus legítimos propietarios. En consecuencia, los contenidos son propiedad intelectual de AERNNOVA o de sus legítimos propietarios, sin que puedan entenderse cedidos al Proveedor.<br /><br />

Queda prohibido cualquier acto de reproducción por parte del PROVEEDOR de los contenidos, en todo o en parte, en cualquier forma o medio, sea mecánico, electrónico, reprográfico u otro, así como cualquier acto de difusión, comunicación pública o distribución, sin la previa autorización por escrito de AERNNOVA o de los legítimos propietarios de los correspondientes derechos. <br /><br />
El uso y acceso al Sitio Web no afectará a la relación contractual existente entre las partes, en especial,  la normativa legal aplicable a la relación contractual establecida entre AERNNOVA y PROVEEDOR a través del correspondiente soporte en papel, ni a los Juzgados y Tribunales competentes para cualquier controversia derivada de dicha relación contractual.
<br /><br />

2. Objeto <br />

El presente acuerdo tiene por objeto regular las relaciones entre AERNNOVA y el PROVEEDOR, en todo lo relacionado con el uso del Portal.<br /><br />


3. Obligaciones del PROVEEDOR<br /><br />

 Son obligaciones del proveedor las siguientes:<br /><br />

 a. Proporcionar cuantos datos sean necesarios para el adecuado funcionamiento del sistema, así como mantenerlos actualizados, comunicando a la mayor brevedad posible todo cambio que se produzca en los mismos.<br /><br />
b. Garantizar la autenticidad de los datos facilitados a consecuencia de la cumplimentación de los formularios necesarios para la suscripción de los Servicios. Asimismo, el PROVEEDOR actualizará la información facilitada para que refleje en cada momento, su situación real. Por todo ello, el PROVEEDOR será el único responsable de los daños y perjuicios ocasionados a AERNNOVA como consecuencia de declaraciones inexactas o falsas.<br /><br />
c. Guardar absoluta confidencialidad en relación con toda la información que se genere en las relaciones entre el PROVEEDOR y AERNNOVA.<br /><br />
d. Abstenerse de cualquier manipulación en la utilización informática del Portal, así como el uso del mismo con fines distintos de aquellos para los que fue previsto. Asimismo, el PROVEEDOR se abstendrá de acceder a zonas no autorizadas del Portal.<br /><br />
e. Cumplir fielmente sus compromisos en la información remitida a través del Portal. En el caso de que el PROVEEDOR no demuestre la necesaria diligencia comercial, o incumpla las obligaciones contraídas, AERNNOVA se reserva el derecho de excluir temporal o permanentemente al PROVEEDOR del Portal.<br /><br />
f. El PROVEEDOR deberá indicar solamente aquellos grupos de materiales, que se refieran a bienes o servicios que, en el momento de la aceptación del acuerdo, el PROVEEDOR comercialice, fabrique o distribuya y que sean del interés comercial de AERNNOVA.<br /><br />

g. El PROVEEDOR acepta que las ofertas introducidas en el Portal sean consideradas con el mismo rango y validez que una oferta enviada por cualquier otro medio tradicional (carta, fax).<br /><br />
h. El PROVEEDOR se obliga a hacer un uso correcto del Portal y de los Servicios conforme a la ley, el presente Aviso Legal y demás reglamentos de uso e instrucciones puestos en su conocimiento, así como con la moral y las buenas costumbres generalmente aceptadas y el orden público.
<br />

Por ello, se abstendrá de utilizar el Portal o cualquiera de los Servicios con fines ilícitos, prohibidos en el presente Aviso Legal, lesivos de los derechos e intereses de terceros. En particular, y a título meramente indicativo, el PROVEEDOR se compromete a no transmitir, difundir o poner a disposición de terceros informaciones, datos, contenidos, gráficos, archivos de sonido y/o imagen, fotografías, grabaciones, software y, en general, cualquier clase de material que:<br /><br />

1. Sea contrario a los derechos fundamentales y libertades públicas reconocidas en la Constitución, Tratados internacionales y leyes aplicables; <br />

2. Induzca, incite o promueva actuaciones delictivas, contrarias a la ley, a la moral y buenas costumbres generalmente aceptadas o al orden público; <br />

3. Sea falso, inexacto o pueda inducir a error, o constituya publicidad ilícita, engañosa o desleal;<br />

4. Esté protegido por derechos de propiedad intelectual o industrial pertenecientes a terceros, sin autorización de los mismos;<br />

5. Vulnere derechos al honor, a la intimidad o a la propia imagen;<br />

6. Vulnere las normas sobre secreto de las comunicaciones;<br />

7. Constituya competencia desleal, o perjudique la imagen empresarialde AERNNOVA o terceros;<br />

8. Esté afectado por virus o elementos similares que puedan dañar o impedir el funcionamiento adecuado del Portal, los equipos informáticos o sus archivos y documentos.<br />

<br />
4. Derechos del PROVEEDOR<br /><br />

 Son derechos del PROVEEDOR los siguientes:<br /><br />

 1. Mantener una presencia constante en la base de datos de AERNNOVA, en su calidad de proveedor dado de alta.<br /><br /> 

 2. Recibir peticiones de ofertas en virtud de las normas establecidas. <br /><br />


5. Obligaciones de AERNNOVA <br /><br />

 Son obligaciones de AERNNOVA las siguientes:<br /><br />

 a. Mantener la información que estime oportuna en el Portal, no siendo responsable de los errores que se produzcan en la misma y de las decisiones que tome el PROVEEDOR en base a esa información. El PROVEEDOR deberá asegurarse en todo momento de que esa información ha sido actualizada. <br />

 b. Guardar absoluta confidencialidad de toda la información relativa al PROVEEDOR: bien la proporcionada por éste, bien la generada en las relaciones entre el PROVEEDOR y AERNNOVA.<br />

 c. Facilitar al PROVEEDOR, en cualquier momento, la situación de sus datos en la base de datos, y no facilitarla a terceros, si no es para permitir al PROVEEDOR la presentación de sus ofertas y la toma en consideración de sus productos como alternativa de suministro. Asimismo, facilitar al PROVEEDOR, en cualquier momento, la situación de sus datos en la base de datos, para que éste, previa notificación por escrito, los modifique o elimine. <br /><br />


6. Derechos de AERNNOVA -PROTECCIÓN DE DATOS PERSONALES-<br /><br />

 a. El Usuario presta su consentimiento para que AERNNOVA o cualquiera de las Sociedades pertenecientes al Grupo al que pertenece cedan sus datos a las empresas del mismo grupo al que pertenece, así como a otras con las que concluya acuerdos, con la única finalidad de contribuir a la mejor prestación del servicio, respetando, en todo caso, la legislación española sobre protección de los datos de carácter personal. Asimismo, el Usuario acepta que AERNNOVA o sus empresas a ella asociadas, sociedades filiales, participadas o pertenecientes al mismo Grupo al que  pertenece, le remitan información sobre cualesquiera bienes o servicios que comercialicen, directa o indirectamente, o que en el futuro puedan comercializar. La aceptación del Usuario para que puedan ser cedidos sus datos en la forma establecida en este párrafo, tiene siempre carácter revocable sin efectos retroactivos, conforme a lo que dispone la Ley Orgánica 15/1999, de 13 de diciembre). 
<br /><br />
 b. Optar, en caso de incumplimiento de sus obligaciones por el PROVEEDOR, por exigir el debido cumplimiento de las mismas, o por la resolución del contrato, en los términos de la Cláusula 8 de este Aviso Legal.  <br /><br />

7. Limitación de responsabilidad<br /><br />

 a. AERNNOVA no asume ninguna responsabilidad derivada de la falta de disponibilidad del sistema, fallos de la red o  técnicos, que provoquen un corte o una interrupción en el Portal. Igualmente AERNNOVA no se hace responsable de los daños y perjuicios que pueda ocasionar la propagación de virus informáticos u otros elementos en el sistema.<br />

b. AERNNOVA no asume ninguna responsabilidad de pagos y/o reclamaciones que deriven de las actuaciones de los Clientes en relación al acuerdo entre dicho/s Cliente/s y el PROVEEDOR.<br />

 c. AERNNOVA no asume ninguna responsabilidad sobre cualquier actuación que sea contraria a las leyes, usos y costumbres, o bien que vulnere las obligaciones establecidas en el presente Aviso Legal, por parte de ninguna persona de AERNNOVA, del PROVEEDOR o cualquier usuario de la red. No obstante, cuando AERNNOVA tenga conocimiento de cualquier conducta a la que se refiere el párrafo anterior, adoptará las medidas necesarias para resolver con la máxima urgencia y diligencia posible, los conflictos planteados.  <br />
d. AERNNOVA se exime de cualquier responsabilidad derivada de la intromisión de terceros no autorizados en el conocimiento de las condiciones y circunstancias de uso que los PROVEEDORES hagan del Portal y de los Servicios.<br /><br />



8. Resolución del acuerdo<br /><br />

 En caso de incumplimiento de cualquiera de las obligaciones contenidas en el presente Aviso Legal, el PROVEEDOR o AERNNOVA habrán de notificar dicho incumplimiento a la parte que lo hubiera cometido, otorgándole un plazo de quince (15) días hábiles, contando desde la notificación, para que lo subsane. Transcurrido dicho plazo sin que se haya producido la subsanación del incumplimiento, la otra parte podrá optar por el cumplimiento o la resolución del presente acuerdo, con la indemnización de daños y perjuicios que corresponda en ambos casos, de conformidad con lo establecido en el artículo 1.124 del Código Civil. En caso de optar por la resolución, tanto el PROVEEDOR como AERNNOVA aceptan que la simple notificación será suficiente, para que la misma surta plenos efectos.<br /><br />


9. Duración <br /><br />

El presente acuerdo tendrá una duración indefinida siempre y cuando no se produzca denuncia por escrito de cualquiera de las partes, con al menos un (1) mes de antelación.<br /><br />
<br /><br />
El procedimiento de baja se realizará mediante el envío de un mail con Asunto “Dar de baja” a la dirección help_desk@aernnova.com

10. Notificaciones<br /><br />

 a. Las notificaciones entre las partes podrán hacerse por cualquier medio de los admitidos en Derecho, que permita tener constancia de la recepción, incluido el fax.<br /><br />

 b. Los cambios de domicilios y faxes serán notificados por escrito y no producirán efectos hasta transcurridos dos días hábiles desde su recepción. En todo caso, el nuevo domicilio y fax será necesariamente dentro del territorio español.<br /><br />

c. Las relaciones entre AERNNOVA y el PROVEEDOR derivadas del uso del Portal y de los servicios prestados a través del mismo, se someterán a la legislación y jurisdicción española.  <br />

			</p>
            <p style="text-align:right"><a href="registro_texto.htm" class="bt_registro" target="blank">Imprimir</a> </p>
<div class="caja_acepto">


<!--<form action="" method="post" style="width:400px;">//-->
<form name="frmAlta" id="frmAlta" method="post" style="width:400px;">
  <span id="sprycheckbox1">
<label>
  <input type="checkbox" name="opcion1" id="opcion1" required>
</label>
<span class="checkboxRequiredMsg">Seleccione para continuar.</span></span><span class="rojo">He leído y Acepto</span> las Condiciones Generales de Uso del Portal de Proveedores.<br />
<br />

<button type="button" class="bt_registro" id="submit1" name="submit1">Siguiente</button>
<br>
</form>


</div>
<div style="clear:both;"></div>
    	</div>    
    </div>
</div>
<script type="text/javascript">

$('#submit1').on('click',function() {
    if($('#opcion1').is(':checked') )
       {
        $('#frmAlta').submit();
        alta();}
    else
       alert("Debe aceptar las condiciones de uso del portal para poder registrarse");
    
});

</script>
 			
</body>
<!--#include file="../../common/fsal_2.asp"-->
</html>
