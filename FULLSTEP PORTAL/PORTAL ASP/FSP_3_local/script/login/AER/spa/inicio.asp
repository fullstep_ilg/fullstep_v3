<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<link href="../estilos.css" rel="stylesheet" type="text/css">
<title>::Fullstep Networks - Sistemas y Gestión de Compras::</title>
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
}
-->
</style>
</head>
<script>
dibujaMenu(1)
</script>
<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function Init() {
    document.getElementById('tablemenu').style.display = 'block';

	MM_preloadImages('../images/icono_docs_sm.gif','../images/icono_docs_sm.gif')
	p = window.top.document.getElementById("frSet")
	vRows = p.rows
	vArrRows = vRows.split(",")
	vRows = vArrRows[0] + ",*,0,0"
	p.rows = vRows

}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">
<div id="inicio-left">
  <h1>Bienvenido al Portal de Proveedores</h1>
  <p>Puede acceder a las distintas &aacute;reas a trav&eacute;s de las opciones de men&uacute; situadas en la parte superior.</p>
  <ul>
    
    <li><b>Solicitudes:</b> acceda a las solicitudes (RFQ y otras solicitudes) y gestiónelas desde este mismo apartado.</li>
    <li><b>Pedidos:</b> gestione desde aquí los pedidos que le han realizado los compradores, siga su evolución y acceda a su histórico.</li>
    <li><b>Sus datos/ su compa&ntilde;&iacute;a :</b> si lo desea puede modificar los datos de su empresa, usuarios, así como las áreas de actividad en las que su empresa se encuentra homologada. </li>
  </ul>
  
<p>  Si es la primera vez que va a realizar una oferta a trav&eacute;s del portal, siga atentamente los siguientes pasos:</p>
  <ol>
  <li><strong>Pulse en "solicitudes&quot;</strong> para ver las peticiones de ofertas que tiene abiertas su empresa.</li><br>
  <li><strong> Seleccione la solicitud de oferta</strong> a la que quiera responder, pulsando sobre el c&oacute;digo de la misma.</li><br>
  <li><strong> Realice su oferta completando toda la informaci&oacute;n necesaria:</strong> desde el &aacute;rbol de navegaci&oacute;n que encontrar&aacute; en la parte izquierda, podr&aacute; desplazarse por los distintos apartados que conforman la oferta. Para introducir los precios deber&aacute; ir al apartado "items/precios". Recuerde introducir el plazo de validez de la oferta, en el apartado "Datos Generales de la oferta". </li><br>
  <li> <strong>Comunique su oferta</strong> pulsando sobre el bot&oacute;n de enviar<IMG height=14 src="../images/sobre_tr.gif" width=30 align=absBottom></li>
 </ol>
</div>
<div id="inicio-right">
<h2>INSTRUCCIONES</h2>
<p>Descárguese las instrucciones sobre cómo realizar una oferta, aceptar un pedido, seguimiento, etc.</p>
<ul>
<li><a href="docs/FSN_MAN_ATC_Cómo ofertar.pdf" target="_blank">Cómo ofertar</a></li>
<li><a href="docs/FSN_MAN_ATC_Requisitos técnicos.pdf" target="_blank">Requisitos técnicos</a></li>
<li><a href="docs/FSN_MAN_ATC_Mantenimiento de datos.pdf" target="_blank">Mantenimiento de datos</a></li>
</ul>
<div id="img-atc"><img src="../images/imagen-ATC.jpg"></div>
<p>Si tiene alguna duda o dificultad para operar con el Portal, póngase en contacto con nostros a través del mail <a href="mailto:help_desk@aernnova.com" class="rojo">help_desk@aernnova.com</a></p>
</div>

</body>
</html>
