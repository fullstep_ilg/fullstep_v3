﻿<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://fast.fonts.net/jsapi/f5dbd368-355c-4f28-bc2f-217b51498d80.js"></script>
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;
        return true;
    };
</script>
<body>
    <div id="cont_gen">
        <div id="izd_gen"> <a href="https://www.ocaso.es" target="_blank"><img src="../images/logo.jpg" alt="Ocaso" border="0"/></a>
        </div>
        <div id="drc_gen">
            <div class="int">
                <p>
                    Si ha olvidado su contraseña introduzca su código de compañía, de usuario y la dirección de e-mail de su cuenta de acceso y pulse "<strong>Enviar</strong>". Recibirá un e-mail con el enlace a la página donde podrá crear una nueva contraseña.
                </p>
                <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
                    <input type="hidden" name="idioma" value="spa">
                    <table class="textos">
                    <tr>
                        <td>Cód. Compañia:</td>
                        <td><input type="text" name="txtCia" id="txtCia" size="30" maxlength="30"/></td>
                    </tr>
                    <tr>
                        <td>Cód. Usuario:</td>
                        <td><input type="text" name="txtUsu" id="txtUsu" size="30" maxlength="30"/></td>
                    </tr>
                    <tr>
                        <td>Dirección de email:</td>
                        <td><input type="text" name="txtEmail" id="txtEmail" size="30"/></td>
                    </tr>
                </table>
                    <br /><input class="bt_registro" name="cmdEnviar" type="submit" style="height:22px; padding:0;" value="Enviar" />
                    
                </form>
                <div class="recordar-claves">
                    <span style="display:block;">Si continúa con dificultades para acceder al Portal o su dirección de email ha cambiado, consulte con el</span>                    
                    <span class="rojo" style="display:block;"><strong>servicio de atención a proveedores.</strong></span>
                    <span class="rojo_24" style="display:block;"><strong>910 770 245</strong></span>
                    <br />
                </div>
            </div>
        </div>
    </div>
</body>

</html>
