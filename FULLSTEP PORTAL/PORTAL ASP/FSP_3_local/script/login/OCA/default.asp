﻿<%@ Language=VBScript %>
<!--#include file="../login.asp"-->
<%		
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if

if Not IdOrden = "" then
    if Not IsNumeric(IdOrden) then
        IdOrden=""
    end if
end if

if Not CiaComp = "" then
    if Not IsNumeric(CiaComp) then
        CiaComp=""
    end if
end if

If Idioma="" then
	Idioma="SPA"
end if

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8">
<title>Portal de proveedores Ocaso</title>
<link href="estilos.css" rel="stylesheet" type="text/css">
<!--[if lte IE 9]>
<link href="estilos_ie9.css" rel="stylesheet" type="text/css" />
<![endif]-->
<script language="javascript">
<!--


function ventanaLogin (IDI){

   window.open ("registro.asp?Idioma="+IDI,"","width=750,height=364,resizable=yes")

}
function ventanaSecundaria (URL){

   window.open(URL,"ventana1","width=700,height=420,scrollbars=no")

}
function ventanaAyuda (URL){

   window.open(URL,"ayuda","width=700,height=600,scrollbars=yes")

}

function popup (URL) {
 window.open("popup.htm","","width=700,height=400,scrollbars=NO")
 }
function recuerdePWD()
{
   window.open("spa/recuerdo.asp","_blank","width=715,height=400,scrollbars=NO")
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
//-->
</script>
<script src="js/jquery-1.9.0.min.js"></script>
<!--ventana modal-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" href="js/colorbox.css" />
<script src="js/jquery.colorbox.js"></script>
<script>
    $(document).ready(function () {
        $(".modal").colorbox();
    });
</script>
<!--slide de imagenes-->
<link rel="stylesheet" href="nivo/nivo-slider.css" type="text/css" media="screen" />
<script type="text/javascript" src="nivo/jquery.nivo.slider.js"></script>
<!--*****************-->
<script>
$(document).ready(function(){
	//---------------------
	
	//---------------------
	$('#slider').nivoSlider({
		effect: 'fade',
		directionNav: false,
		controlNav: false,
		pauseOnHover: false	
	});
	//---------------------
  	$("#txtCIA").val("Código Compañía");
  	$("#txtCIA").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Código Compañía"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Código Compañía");
  			$(this).removeClass('focus');
  		}
  	});	
	//---------------------
  	$("#txtUSU").val("Código de Usuario");
  	$("#txtUSU").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Código de Usuario"){
  			$(this).val('');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Código de Usuario");
  			$(this).removeClass('focus');
  		}
  	});
	//---------------------
  	$("#txtPWD").val("Contraseña");
  	$("#txtPWD").bind('focus',function(){
  		$(this).addClass('focus');
  		if($(this).val()=="Contraseña"){
  			$(this).val('');
			$(this).removeAttr("type");
			$(this).prop('type', 'password');
  		}
  	}).bind('blur',function(){
  		if($(this).val()==""){
  			$(this).val("Contraseña");
			$(this).removeAttr("type");
			$(this).prop('type', 'text');
  			$(this).removeClass('focus');
  		}
  	});
});
</script>
</head>
<body>
<div id="principal"> 
  <!--cabecera-->
  <div id="cabecera">
    <div id="logotipo"><a href="http://www.ocaso.es/" target="_blank"><img src="images/logo.gif" width="121" height="78" border="0" alt=""/></a></div>
    <div id="titulo">ÁREA DEL PROVEEDOR</div>
  </div>
  <!--cuerpo central-->
  <div id="cuerpo">
    <div id="imagen-principal"></div>
    <div id="formulario">
      <div class="tit"></div>
      <form action="default.asp" method="post" name="frmLogin"  id="frmLogin">
        <input type="text" class="text" id="txtCIA" name="txtCIA" maxlength="20">
        <input type="text" class="text" id="txtUSU" name="txtUSU" maxlength="20">
        <input type="password" class="text" id="txtPWD" name="txtPWD" maxlength="20" autocomplete="off">
        <div class="bt"> 
          <!--
                    <input class="bt" name="cmdEntrar" type="submit" value="Acceder" />-->
          <input type="image" name="cmdEntrar" onMouseOver="MM_swapImage('cmdEntrar','','images/acceder.png',1)" onMouseOut="MM_swapImgRestore()" src="images/acceder.png" width="130" height="30" border="0">
        </div>
        <input type="hidden" id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
        <input type="hidden" id="CiaComp" name="CiaComp" value="<%=CiaComp%>">
        <div> <a href="javascript:void(null)" onClick="recuerdePWD()" class="bt_claves">¿Olvidó sus claves de acceso?</a></div>
      </form>
      <div id="registro">
      <div class="sep"></div>
        <div class="tit_registro">¿Aún no está registrado?</div>
        <div class="bt_solicitar"> 
          <!--
                   <a href="javascript:ventanaLogin('SPA')" >Solicitar registro</a>--> 
          <a href="javascript:ventanaLogin('SPA')" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('registro','','images/registro.png',1)"><img src="images/registro.png" alt="" width="130" height="30" id="bt_solicitar" border="0" /></a> </div>
      </div>
    </div>
  </div>
  <!--fin cuerpo central--> 
</div>
<!--Opcional
  <div id="nav_home">
    	<div class="contenido-home">
   		  <h2>Bienvenido</h2>
          

 <p>Texto opcional para Ocaso.<br />
   <br /> 
   En este Portal también podrá encontrar información sobre los procesos abiertos de licitación, pedidos, homologación y resto de gestiones del Área de Compras.</p>
                
        
                
              
                
                
    </div>
        <div class="datos_fullstep">
        	ATENCIÓN A PROVEEDORES<br />
            <span class="rojo_24" style="line-height:24px;">Tel. 902 043 066</span><br />
            <a href="mailto:atencionalcliente@fullstep.com " class="rojo">atencionalcliente@fullstep.com </a>
            <img src="images/linea.png" alt="" style="margin:15px 0;" />
           <p> HORARIO DE ATENCIÓN TELEFÓNICA:</p>
            <p>
            LUNES A JUEVES<br />
            <span class="rojo">8:30 a 13:30 y de 14:30 a 17:30</span>
            </p>
            <p>
            VIERNES<br />
            <span class="rojo">8:00 a 14:00</span>
            </p>
        </div>
        <div style="clear:both"></div>
    </div>
    -->
<div id="pie-pagina">
  <div id="pie-pagina-superior"> &copy; Ocaso Seguros - <a href="http://www.ocaso.es/es/html/ocaso-avisoLegal.htm" target="_blank">Aviso legal</a> - <a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ayuda.html" target="_blank" >Preguntas frecuentes</a> - <a href="<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/Politica_cookies.html" target="_blank" >Pol&iacute;tica de Cookies</a> </div>
  <div id="pie-pagina-inf-izq"><img src="images/ico-tel.png" width="25" alt=""/> 910 770 245  | <img src="images/ico-mail.png" width="25" alt=""/> <a href="mailto:atencionproveedores@ocaso.es">atencionproveedores@ocaso.es</a> </div>
  <div id="pie-pagina-inf-dcha">
    <p>Sitio web optimizado para 1280 x 1024<br>
      Navegadores soportados: Internet Explorer, Google Chrome y Mozilla Firefox</p>
    <!--  <p class="powered derecha"><a href="" title=""><img src="images/logo_fullstep.png" width="147" height="19" border="0" /></a></p>--> 
  </div>
</div>
</body>
</html>
