﻿<%@ Language=VBScript %>
<html>

<head>

<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/idioma.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->

<%
''' <summary>
''' Mostrar el detalle del registro
''' </summary>
''' <remarks>Llamada desde: login/login.asp		login/codigo instalaci�n/registro.asp	login/codigo instalaci�n/codigo idioma/registro.asp; Tiempo m�ximo: 0</remarks>
%>

<title>::Fullstep - Sistemas y Gesti&oacute;n de Compras::</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilos.css" rel="stylesheet" type="text/css">

<script src="../../common/formatos.js"></script>
<script language="JavaScript" src="../../common/ajax.js"></script>
<!--#include file="../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=SPA", "_blank")
window.close()

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=550,height=600,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){

if (document.all) if (event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>


<body bgcolor="#FFFFFF" topmargin="5" leftmargin="5" onload="init()">
<form name="frmAlta" id="frmAlta" method="post">

  <div align="center">
  <table border="0" width="80%">
      <tr>
        <td><a href="http://www.fullstep.com" target="_blank"><img src="images/logo.gif" width="200" height="54" border="0"></a></td>
      </tr>
      <tr>
        <td width="100%" height="33" align="left">        
	    <p align="left" class="subtexto"> Para continuar con el proceso 
              de alta es imprescindible la aceptación de las siguientes condiciones :</p>        
               
      </td>
      </tr>
      <tr>
        <td align="left"><table border="0" align="left">
          <tr>
            <td valign="bottom"><div align="right"><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"></font></div>          <font color="#666666" size="1" face="Verdana">
              <textarea readonly="true" name="S1" cols="63" style="height:120pt;font-family: Verdana; font-size: 8pt; text-align: Justificar; line-height: 150%; list-style-type: lower-alpha">
CONDICIONES DE USO DE LA PLATAFORMA DE COMPRAS

Nota preliminar: La palabra &ldquo;proveedor&rdquo; tambi&eacute;n se usa a continuaci&oacute;n para compa&ntilde;&iacute;as que no son actualmente proveedores de FULLSTEP pero que desean establecer una relaci&oacute;n de trabajo con la compa&ntilde;&iacute;a.

Una condici&oacute;n a cumplir por las compa&ntilde;&iacute;as que son proveedores activos de FULLSTEP, o que desean registrarse para tomar parte en futuras peticiones de oferta a trav&eacute;s del portal de compras, es la introducci&oacute;n en &eacute;l, el portal, de la descripci&oacute;n general de la compa&ntilde;&iacute;a.
No existe derecho legal por parte del proveedor a participar en las peticiones de oferta.
FULLSTEP se reserva el derecho de seleccionar, deseleccionar o excluir proveedores de la plataforma de acuerdo a criterios internos.
Se establecer&aacute; un usuario principal por proveedor registrado en el portal de compras.
El usuario principal act&uacute;a en nombre del proveedor, que est&aacute; obligada a mantener los datos actualizados de manera continua.
Este usuario es responsable, como coordinador de la compa&ntilde;&iacute;a, del alta y baja de futuros usuarios.
El mantenimiento de los datos de los usuarios, el alta y baja de usuarios, as&iacute; como el mantenimiento de los datos de la compa&ntilde;&iacute;a (direcciones, materiales, etc&hellip;) ser&aacute; realizado por el usuario principal de cada proveedor.
El proveedor ser&aacute; informado de los datos de acceso para el usuario principal de la compa&ntilde;&iacute;a.
Estos datos ser&aacute;n tratados de forma confidencial, al igual que los de futuros usuarios, los datos de acceso de los usuarios son personales e intransferibles y el acceso a la plataforma debe restringirse a las personas involucradas en la relaci&oacute;n comercial.
Las contrase&ntilde;as de acceso elegidas deben ser de una complejidad adecuada y cambiadas de forma regular.
Toda la informaci&oacute;n y documentos intercambiados en el contexto de nuestra relaci&oacute;n comercial deben ser tratados con estricta confidencialidad.
Dibujos, descripciones, especificaciones y similares pueden ser &uacute;nicamente guardadas y duplicadas en el contexto de requerimientos operacionales.
En el caso de que la informaci&oacute;n se comparta con una tercera parte por los proveedores (p.e. subcontrataci&oacute;n) se debe garantizar la confidencialidad de la misma.
No se garantiza la disponibilidad permanente y libre de fallos de los sistemas electr&oacute;nicos. FULLSTEP se reserva el derecho de reducir o cesar la operatividad de la plataforma.
El proveedor es responsable de su acceso a la plataforma (&ldquo;Acceso a Internet&rdquo;) y no se le cargar&aacute; con coste alguno por este acceso.
Todas las partes involucradas se comprometen a mantener las medidas de seguridad adecuadas de acuerdo con los est&aacute;ndares tecnol&oacute;gicos actuales.
El proveedor se compromete a no adjuntar en la plataforma cualquier documento, archivo o dato que pueda poner en peligro o impedir su funcionamiento.
De forma particular, debe tomar medidas para no expandir cualquier clase de virus.
El proveedor se compromete a utilizar, y mantener actualizado, un antivirus de prestigio reconocido.
Las ofertas transmitidas en formato electr&oacute;nico son totalmente v&aacute;lidas, y las realizadas v&iacute;a fax o tel&eacute;fono son legalmente vinculantes.
&Uacute;nicamente los t&eacute;rminos contractuales, as&iacute; como las ofertas, introducidas en la plataforma ser&aacute;n tenidos en cuenta. Cualquier comentario adicional no ser&aacute; vinculante a no ser que sea confirmado por escrito por FULLSTEP.
La informaci&oacute;n que usted nos remite a trav&eacute;s de este &aacute;rea de compras pasa a formar parte de la base de datos de proveedores de FULLSTEP.
Tiene derecho a consultar y modificar sus datos o darse de baja de dicha base de datos. El propietario y responsable de la base de datos es:
FULLSTEP, S.L. 
Antonio de Cabez&oacute;n, 83, 4&ordm; 
28700 MADRID 
CIF: B-82661935 
FULLSTEP se compromete a utilizar sus datos exclusivamente para solicitar ofertas y remitir comunicados relativos a la funci&oacute;n de compras del grupo, y a no facilitar sus datos a terceros sin su previo consentimiento. Si desea darse de baja y NO recibir nuestras solicitudes de oferta ni nuestra informaci&oacute;n, env&iacute;e un correo electr&oacute;nico a compras@fullstep.com indicando en el asunto: "Dar de baja". 
Al enviar su solicitud de registro, la compa&ntilde;&iacute;a proveedora reconoce las condiciones arriba enumeradas.
        </textarea>
              </font> </td>
            </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td align="left" class="textos"><p><span class="subtexto">&iquest;Acepta las condiciones de adhesi&oacute;n al portal de compras de FULLSTEP?</span><br>
                <br>
           Si pulsa &quot;No Acepto&quot;, su empresa no podr&aacute; ser registrada como proveedor autorizado de FULLSTEP.<br>
      Para continuar el proceso de alta debe pulsar &quot;Acepto&quot;. <br>
      Si quiere imprimir las condiciones puede hacerlo haciendo pulsando el bot&oacute;n &quot;Imprimir&quot;. <br>
      <br>
          
      <div align="center"></div>
          <table border="0" width="39%">
            <tr>
              <td width="41%" valign="middle"><p align="center">
                  <input type="submit" onClick="return alta()" value="Acepto" id=submit1 name=submit1>
              </p></td>
              <td width="38%" valign="middle"><input type="button" onClick="no_alta()" value="No acepto" id=button1 name=button1>
              </td>
              <td width="21%" valign="middle"><input type="button" onClick="imprimir()" value="Imprimir" id=button2 name=button2>
              </td>
            </tr>
          </table>
        <p align="center"></td>
      </tr>
  </table>
  <input type="hidden" name="ACEPTADO">
	  
</div>
</form>

</body>
<!--#include file="../../common/fsal_2.asp"-->
</html>
