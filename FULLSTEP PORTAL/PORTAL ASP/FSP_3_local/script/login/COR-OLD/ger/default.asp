﻿<%@ Language=VBScript %>
<!--#include file="../../login.asp"-->
<%			
''' <summary>
''' Pantalla inicial de la personalización, es la q llama a la pantalla de login
''' </summary>
''' <remarks>Llamada desde: registro/registrarproveedor.asp		proveedor/default.asp
'''		login/personalizacion/login.asp		; Tiempo máximo: 0,1</remarks>	
Idioma = Request("Idioma")
Idioma = trim(Idioma)

IdOrden=Request.QueryString ("IdOrden") 
CiaComp=Request.QueryString ("CiaComp") 

if IdOrden = "" then
	IdOrden = Request.Form ("IdOrden")
	CiaComp = Request.Form ("CiaComp")
end if

If Idioma="" then
	Idioma="GER"
end if

Dim Den


den=devolverTextos(Idioma,1 )


lblIdioma= Den(1)
lblCia= Den(2) 
lblUsuario= Den(3)
lblPwd=  Den(4) 
cmdEntrar =Den(5)


%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Suppliers Portal</title>
<script language="javascript">
<!--
var mesos=new Array(13);
  mesos[1]="de enero";
  mesos[2]="de febrero";
  mesos[3]="de marzo";
  mesos[4]="de abril";
  mesos[5]="de mayo";
  mesos[6]="de junio";
  mesos[7]="de julio";
  mesos[8]="de agosto";
  mesos[9]="de septiembre";
  mesos[10]="de octubre";
  mesos[11]="de noviembre";
  mesos[12]="de diciembre";

var semana=new Array(8);
  semana[1]="Domingo";
  semana[2]="Lunes";
  semana[3]="Martes";
  semana[4]="Miércoles";
  semana[5]="Jueves";
  semana[6]="Viernes";
  semana[7]="Sábado";

var ladata=new Date();

var elmes=mesos[ladata.getMonth() + 1];

var eldia=semana[ladata.getDay() + 1];

var numero=ladata.getDate();

var any=ladata.getYear();

if (any < 2000)
any = any + 1900;

function ventanaLogin (IDI){

   window.open ("registro.asp?Idioma="+IDI,"","width=650,height=430,resizable=yes")

}
function ventanaSecundaria (URL){

   window.open(URL,"ventana1","width=710,height=370,scrollbars=no")

}
function ventanaAyuda (URL){

   window.open(URL,"ayuda","width=720,scrollbars=yes")

}

function popup (URL) {
 window.open("popup.htm","","width=500,height=650,scrollbars=NO")
 }
function recuerdePWD()
{
   window.open("recuerdo.asp","_blank","width=641,height=300,scrollbars=NO")
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
//-->
</script>
<link href="../estilos.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
body {
	background-image: url();
	background-color: #EBEBEB;
	margin-top: 0px;
}
.Estilo2 {font-size: x-large}
-->
</style></head>
<body>		<form name="frmLogin" id="frmLogin" method="post" action="default.asp">
		<input type="hidden" id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
		<input type="hidden" id="CiaComp" name="CiaComp" value="<%=CiaComp%>">						
		<input type="hidden" name="Idioma" value="<%=Idioma%>">
<br><br>
<div align="center">
    <table width="934" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td width="9" rowspan="5" background="../images/sombra_dcha.gif" bgcolor="#FFFFFF">&nbsp;</td>
      </tr>
      <tr>
        <td width="925" height="72" align="right" valign="top" >
        <table width="924" height="72" border="0" cellpadding="0" cellspacing="0">
          <tr bgcolor="#FFFFFF">
            <td rowspan="2" align="center" class="titulo">PORTAL DE FORNECEDORES</td>
            <td width="35%" rowspan="2"><div align="center" class="titulo">
              <div align="left" class="titulo Estilo2"></div>
            </div></td>
            <td width="26%" valign="bottom"><a href="http://www.fullstep.com" target="_blank"><img src="../images/logo.jpg" border="0" WIDTH="200" HEIGHT="54"></a></td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td valign="bottom"><table width="80%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td width="52%" bgcolor="#FFFFFF"><div align="center"><a href="javascript:ventanaAyuda('<%=application("RUTANORMAL")%>custom/<%=application("NOMPORTAL")%>/public/ger/manuales.htm')" class="textosres">Ajuda</a></div></td>
                <td width="8%" class="textosres">|</td>
                <td width="40%" class="textosres"><a href="../default.asp?Idioma=SPA">Español</a></td>
                 <td width="8%" class="textosres">|</td>
                    <td width="22%" class="textosres"><a href="../eng/default.asp?Idioma=ENG">English</a></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      
      <tr>
        <td><table width="925" border="0" cellpadding="0" cellspacing="0">
            <tr bgcolor="#FFFFFF">
              <td width="382" rowspan="3" align="left" valign="top" bgcolor="#FFFFFF"><div align="left"><img src="../images/central.jpg" WIDTH="425" HEIGHT="282"></div></td>
              <td width="282" rowspan="3" align="left" valign="top" background="../images/fondo.jpg" bgcolor="#FFFFFF"><table width="96%" border="0" cellspacing="0" cellpadding="0">
                
                <tr>
                  <td height="15" valign="middle">&nbsp;</td>
                  <td valign="middle">&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td width="13%" valign="middle">&nbsp;</td>
                  <td width="83%" valign="middle" class="textos"><p>A FULLSTEP disponibiliza um canal de comunicação direto aos seus fornecedores, através do qual poderão responder aos pedidos de orçamento realizados pelo departamento de compras.<br><br>
Caso já se tenha registado neste portal e tenha sido autorizado como fornecedor, pode aceder à área privada de fornecedores ao introduzir os seus códigos de acesso. <br>
<br>
Caso ainda não se tenha registado, solicite o registo através da hiperligação para solicitar registo.</p></td>
                  <td width="4%">&nbsp;</td>
                </tr>
              </table></td>
              <td width="20" rowspan="3">&nbsp;</td>
              <td width="234" height="19" align="left" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
              <td width="7" rowspan="3" align="left" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>
            <tr bgcolor="#FFFFFF">
              <td align="left" valign="top" bgcolor="#FFFFFF"><table width="188" border="0" cellpadding="0" cellspacing="0" bgcolor="#D8D8D8">
                <tr>
                  <td bgcolor="#EDEDED"><div align="center">
                      <table width="173" height="147" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td>&nbsp;</td>
                          <td class="formulario">&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td width="16">&nbsp;</td>
                          <td width="76" class="formulario">Código empresa</td>
                          <td width="71"><div align="center">
                            <input id="txtCia" name="txtCIA" maxlength="20" size="10">
                          </div></td>
                          <td width="10">&nbsp;</td>
                        </tr>
                        <tr>
                          <td width="16">&nbsp;</td>
                          <td width="76" class="formulario">Utilizador </td>
                          <td width="71"><div align="center">
                            <input name="txtUSU" maxlength="20" size="10">
                          </div></td>
                          <td width="10">&nbsp;</td>
                        </tr>
                        <tr>
                          <td width="16">&nbsp;</td>
                          <td width="76" class="formulario">Senha</td>
                          <td width="71"><div align="center">
                            <input name="txtPWD" type="password" maxlength="20" size="10" autocomplete="off">
                          </div></td>
                          <td width="10">&nbsp;</td>
                        </tr>
                        <tr>
                          <td width="16">&nbsp;</td>
                          <td width="76">&nbsp;</td>
                          <td width="71"><div align="center">
                              <input type="hidden" name="cmdEntrar" value="<%=cmdEntrar%>">
                              <input type="hidden" name="txtEntrar" value="<%=cmdEntrar%>">
                              <input type="image" name="imgEntrar" value="<%=cmdEntrar%>" src="../images/entrar2_eng.gif" width="50" height="20">
                          </div></td>
                        </tr>
                        <tr>
                          <td height="28" colspan="3" align="center"><a class="registro" href="javascript:void(null)" onClick="recuerdePWD()"><u>Esqueceu a sua senha ?</u></a> </td>
                          <td width="10">&nbsp;</td>
                        </tr>
                      </table>
                  </div></td>
                </tr>
                <tr>
                  <td class="textosres"><div align="center">
                      <table width="188" height="23" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td height="23" background="../images/fondo_gris2.gif" bgcolor="#FFFFFF"><div align="left" class="registro">
                                <div align="center" class="textogris"><a href="javascript:ventanaLogin('SPA')" class="subtexto">registrar</a></div>
                          </div></td>
                          </tr>
                      </table>
                  </div></td>
                </tr>
              </table></td>
            </tr>
            <tr bgcolor="#FFFFFF">
              <td align="left" valign="top" bgcolor="#FFFFFF"><table width="185" border="0" align="left" cellpadding="0" cellspacing="0" class="subtexto">
                <tr>
                  <td height="21">&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td width="16" height="21"><div align="left"></div></td>
                  <td width="169" height="21">Suporte Fornecedores</td>
                </tr>
                <tr>
                  <td height="21"><div align="left"></div></td>
                  <td height="21">+34 902 996 926
                    <br>
                    <a href="mailto:atencionalcliente@fullstep.com" class="textos">atencionalcliente@fullstep.com</a> </td>
                </tr>
                <tr>
                  <td class="subtexto">&nbsp;</td>
                  <td class="subtexto"><span class="textos10">Horário (*) do serviço de atenção:<br>
                    Segunda a quinta-feira: 8:00-21:00.<br>
                     Sexta-feira: 8:00- 19:00<br></span></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><table width="926" border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
          <tr>
            <td width="12" height="40" rowspan="2">&nbsp;</td>
            <td colspan="8" align="left" valign="bottom" class="subtexto"><table width="126" border="0" align="left" cellpadding="0" cellspacing="0">
              <tr>
                <td width="4"></td>
                <td width="47"></td>
                <td rowspan="3" width="149"><div align="left"><a href="http://www.fullstep.com" target="_blank"><img src="../images/logo_fullstep.png" alt="FULLSTEP" width="100" height="24" hspace="5" border="0"></a></div></td>
              </tr>
              <tr>
                <td width="4">&nbsp;</td>
                <td class="textos" width="47"><div align="left"><font color="#666666">powered</font></div></td>
              </tr>
              <tr>
                <td width="4">&nbsp;</td>
                <td class="textos" valign="top" width="47"><div align="left"><font color="#666666">by</font></div></td>
              </tr>
            </table></td>
            <td colspan="2" align="left" class="subtexto">&nbsp;</td>
            <td width="28" rowspan="2" align="left" bgcolor="#FFFFFF" class="subtexto">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="8" align="left" valign="bottom" class="subtexto"><div align="left">
              <table width="97%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="40%" valign="bottom" class="textos">© FULLSTEP | 
                    <a href="javascript:ventanaSecundaria('<%=application("RUTASEGURA")%>custom/<%=application("NOMPORTAL")%>/public/ger/aviso legal.htm')" class="registro">Aviso legal</a>
                    |&nbsp;
                    <a href="<%=application("RUTASEGURA")%>script/politicacookies/politicacookies.asp?Idioma=GER" class="subtitulo">Política de cookies</a>
                            
                     </td>
                  <td width="3%" align="left" class="textos">&nbsp;</td>
                  <td width="57%" align="left" valign="bottom" class="textos10">&nbsp;</td>
                </tr>
              </table>
            </div></td>
            <td width="13" height="23" align="center" class="textos10">&nbsp;</td>
            <td width="193" align="left" valign="bottom" class="textos10">Website otimizado para 1280 x 1024<br>
             Navegadores suportados: Internet Explorer e Firefox <a href="http://windows.microsoft.com/en-US/internet-explorer/download-ie" target="_blank">Internet Explorer</a> and <a href="http://www.mozilla.org/en-US/firefox/all/" target="_blank">Mozilla Firefox</a></td>
          </tr>
          
        </table>
		</td>
      </tr>
      <tr>
        <td colspan="2"><img src="../images/recuadro_inf.jpg" width="934" height="22"></td>
      </tr>
    </table>
    
  </div><map name="Map">
  <area shape="rect" coords="24,42,216,87" href="http://www.fullstep.com" target="_blank">
</map>

</form>
</body>
</html>
