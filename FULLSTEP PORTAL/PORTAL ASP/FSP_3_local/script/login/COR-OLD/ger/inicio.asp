﻿<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<link href="../estilos.css" rel="stylesheet" type="text/css">


<title>::FULLSTEP Purchasing Portal::</title>
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
}
-->
</style>
</head>

<script>
dibujaMenu(1)
</script>

<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function Init() {
    document.getElementById('tablemenu').style.display = 'block';

    MM_preloadImages('../images/icono_docs_sm.gif', '../images/icono_docs_sm.gif')

    p = window.top.document.getElementById("frSet")
    vRows = p.rows
    vArrRows = vRows.split(",")
    vRows = vArrRows[0] + ",*,0,0"
    p.rows = vRows
}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onload="Init()">
<!-------------------------- BEGIN COPYING THE HTML HERE ----------------------------> 
<table width="100%" border="0" cellpadding="5" cellspacing="2" bordercolor="0" hspace="0" vspace="0">
  <tr> 
    <td width="12" rowspan="3" align="right" valign="top" bordercolor="0"> <div align="left" class="textos"></div>
    </td>
    <td height="74" colspan="2" align="left" valign="middle"><font size="2" face="verdana" class="titulo">Bem-vindo ao Portal de Fornecedores</font></td>
    <td colspan="2" align="left" valign="bottom">&nbsp;</td>
  </tr>
  <tr>
    <td rowspan="2" valign="top" bgcolor="#EEEEEE" class="textos">
	<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="textos">Pode aceder às diferentes áreas através das opções de menu que se encontram na parte superior. 
      <ul>
        <li><strong>Qualidade</strong>: efetue a gestão dos seus certificados, Não Conformidades e aceda aos seus índices de qualidade. </li>
             <li><strong>Pedidos</strong>: aceda aos pedidos (RFQ e outros pedidos) e efetue a respetiva gestão a partir desta secção.</li>
           <li><strong>Encomendas</strong>: efetue a gestão das encomendas que lhe tenham sido realizadas pelos compradores, acompanhe a sua evolução e aceda ao seu histórico a partir desta secção. </li>
        <li><strong>Os seus dados/a sua empresa</strong>: tem a possibilidade de alterar os dados da sua empresa, dos utilizadores e das áreas de atividade em que a sua empresa participa. </li>
      </ul>
      <br>
Se for a primeira vez que irá efetuar uma oferta através do portal, siga cuidadosamente os seguintes passos:<br>
<br>
<blockquote>
  <ol>
    <li>Clique em "pedidos" para visualizar os pedidos de orçamento que a sua empresa tem pendentes. </li>
        
    <li> Selecione o pedido de orçamento ao qual pretende responder ao clicar sobre o respetivo código. </li>
    <li>  Efetue o seu orçamento através do preenchimento de todas as informações necessárias: poderá deslocar-se pelas diferentes secções que compõem o orçamento a partir da árvore de navegação que se encontra no lado esquerdo. Para introduzir os preços, deverá aceder à secção "itens/preços". Não se esqueça de introduzir o prazo de validade do orçamento na secção "Dados Gerais do orçamento". </li>
    <li>  Comunique o seu orçamento ao clicar no botão de enviar. </b><img src="../images/sobre_tr.gif" align="absbottom" width="30" height="14"> </li>
  </ol>
</blockquote></td>
  </tr>
</table>

	</td>
    <td rowspan="2" valign="top" class="textos">&nbsp;</td>
    <td colspan="2" align="left" valign="top"><table width="100%" border="0" class="textos">
        <tr>
          <td height="26" valign="top" class="textos"><span class="textos"><strong>INSTRUÇÕES</strong></span></td>
        </tr>
        <tr>
          <td height="62" class="textos">Transfira as instruções sobre o modo como efetuar um orçamento, aceitar uma encomenda, acompanhamento, etc.</td>
        </tr>
        <tr>
          <td width="273" class="textos"><a href="docs/MAN_205_how to offer_3_0.pdf" class="textos" target="_blank"><img src="../images/circulo.jpg" width="13" height="15" border="0"> Como efetuar o orçamento </a></td>
        </tr>
        <tr>
          <td><a href="docs/MAN_203_technical requirements_3_0.pdf" class="textos" target="_blank"><img src="../images/circulo.jpg" width="13" height="15" border="0"> Requisitos técnicos </a></td>
        </tr>
        <tr>
          <td><a href="docs/MAN_206_master data_3_0.pdf" class="textos" target="_blank"><img src="../images/circulo.jpg" width="13" height="15" border="0"> Manutenção dos dados </a></td>
        </tr>
        <tr>
          <td>&nbsp;   </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
    </table>
	</td>
  </tr>
  <tr>
    <td align="left" valign="bottom"><table width="100%" height="220" border="0" background="../images/fondo.jpg">
  <tr>
    <td class="subtit"><div align="center"><img src="../images/img4.jpg" width="195" height="146"></div></td>
  </tr>
  <tr>
    <td valign="bottom" class="textos"><table width="55%" border="0" align="center"  cellpadding="0" cellspacing="0" class="subtexto">
      <tr>
        <td height="21"><div align="center">Suporte Fornecedores</div></td>
      </tr>
      <tr>
        <td height="21"><div align="center">+34 902 996 926 </div></td>
      </tr>
      <tr>
        <td height="21"><div align="center" class="textos"><a href="mailto:contact@fullstep.com">atencionalcliente@fullstep.com</a></div></td>
      </tr>
    </table>
      <div align="center"></div></td>
  </tr>
</table>
    </td>
  </tr>

</table>
</body></html>
