﻿<%@ Language=VBScript %>
<!--#include file="../../recuerdo.asp"-->
<html>
<head>
<link href="../estilos.css" rel="stylesheet" type="text/css"/>
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style>
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body>
<form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">    
    <table width="640" border="0" cellspacing="0" cellpadding="0" class="textos">
        <tr >
            <td height="100" colspan="2" background="images/fondo_R.gif">
                <div align="right">
                    <a href="http://www.fullstep.com/" target="_blank">
                        <img src="../images/logo.gif" width="200" HEIGHT="54" border="0"/>
                    </a>
                </div>
            </td>
        </tr>
        <tr>
            <td width="20">&nbsp;</td>
            <td width="615" align="left">
                <br/>
                <span style="color:#666666;">
                    Se você esqueceu sua senha, preencha o código de empresa, utilizador, e-mail e clique em " Enviar". Você receberá um link para uma página para criar uma nova senha<br>
                </span>
                <br/>
                <table class="textos">
                    <tr>
                        <td>Código empresa:</td>
                        <td><input type="text" name="txtCia" id="txtCia" size="80" maxlength="100"/></td>
                    </tr>
                    <tr>
                        <td>Utilizador:</td>
                        <td><input type="text" name="txtUsu" id="txtUsu" size="80" maxlength="100"/></td>
                    </tr>
                    <tr>
                        <td>Email:</td>
                        <td><input type="text" name="txtEmail" id="txtEmail" size="80" maxlength="100"/></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="20">&nbsp;</td>
            <td>
                <br /><input type="submit" value="Enviar" name="cmdEnviar" id="cmdEnviar"/>
                <br /><br />Caso não tenha conseguido resolver as suas dúvidas, entre em contacto connosco através do telefone. Tel. +34 902 996 926 
            </td>
        </tr>
    </table> 
    <input type="hidden" name="idioma" value="ger"/>
</form>
</body>
</html>
