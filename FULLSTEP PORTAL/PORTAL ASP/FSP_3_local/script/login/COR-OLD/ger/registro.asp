﻿<%@ Language=VBScript %>
<!--#include file="../../../common/XSS.asp"-->
<html>


<head>

<title>::Supplier Portal::</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="../estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo2 {font-weight: normal; color: #666666; text-decoration: none; font-style: normal;}
-->
</style>
</head>

<script>
function no_alta()
{
window.close()

}
</script>

<script>
function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=GER", "_blank")
window.close()

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=550,height=600,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>


<body bgcolor="#FFFFFF" topmargin="5" leftmargin="5">
<form name="frmAlta" id="frmAlta" method="post">

  <div align="center">
  <table border="0" width="80%">
      <tr>
        <td background="../images/fondotop_c.gif">&nbsp;</td>
      </tr>
      <tr>
        <td><a href="http://www.fullstep.com" target="_blank"><img src="../images/logo.jpg" border="0" WIDTH="200" HEIGHT="54"></a></td>
      </tr>
      <tr>
        <td width="100%" height="33" align="left">        
	    <p align="left" class="subtextos"> Para prosseguir com o registo, o seguinte contrato deve ser lido e aceito :</p>        
               
      </td>
      </tr>
      <tr>
        <td align="left"><table border="0" align="left">
          <tr>
            <td valign="bottom"><div align="right"><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"></font></div>          <font color="#666666" size="1" face="Verdana">
              <textarea readonly rows="11" name="S1" cols="63" style="font-family: Verdana; font-size: 8pt; text-align:justify; display:block; height:135px; line-height: 150%; list-style-type: lower-alpha">
IMPORTANTE LEIA COM ATENÇÃO

Registo de fornecedores

Aviso Legal

Acesso ao Portal

O registo como fornecedor no Portal de compras da (EMPRESA) NETWORKS S.L. (doravante a (EMPRESA)) está condicionado à leitura prévia e aceitação das seguintes cláusulas. Se não expressar a sua conformidade com as mesmas, não poderá efetuar o registo. Sempre que aceder e utilizar o Portal, entender-se-á que concorda, de forma expressa, plena e sem reservas, com a totalidade do conteúdo do presente Aviso Legal. Para além disso, ao ser o Utilizador Principal da sua empresa e ao aceitar a conformidade com o presente Aviso Legal, fica obrigado a garantir o respetivo cumprimento por parte de todos os utilizadores que se registem na sua empresa, isentando a (EMPRESA) de qualquer responsabilidade pelos prejuízos que esses utilizadores possam causar à sua empresa ou a qualquer outra devido às suas ações no Portal.


Cláusulas

1. Objeto do Portal de compras da (EMPRESA) 

O portal de compras da (EMPRESA) é o meio através do qual a (EMPRESA) comunica com os seus fornecedores para lhes solicitar orçamentos e documentos, assim como as informações comerciais que considere adequadas. Ao mesmo tempo, a (EMPRESA) pode utilizar o portal para lhe enviar as informações que considere que são do seu interesse.
A (EMPRESA) age como comprador direto ou como gestor de compras dos seus clientes e irá agir em nome destes clientes durante o processo de compra.

A utilização ou o acesso ao Portal e/ou aos Serviços não concedem ao FORNECEDOR qualquer direito sobre as marcas, designações comerciais ou sinais distintivos que apareçam no mesmo, sendo que estes são propriedade da (EMPRESA) ou de terceiros. Como consequência, os Conteúdos são propriedade intelectual da (EMPRESA) ou de terceiros e não se pode considerar que foram cedidos ao FORNECEDOR.


2. Objeto 

O presente acordo visa regulamentar as relações entre a (EMPRESA) e o FORNECEDOR quanto a todos os aspetos que estão relacionados com a utilização do Portal.


3. Obrigações do FORNECEDOR

O fornecedor tem as seguintes obrigações:

a. Proporcionar os dados que sejam necessários para o funcionamento correto do sistema, assim como mantê-los atualizados, comunicando o mais rápido possível qualquer alteração que os mesmos sofram.

b. Garantir a autenticidade dos dados facultados como consequência do preenchimento dos formulários necessários para a subscrição dos Serviços. Para além disso, o FORNECEDOR irá atualizar as informações facultadas de modo a refletir sempre a sua situação real. Assim, o FORNECEDOR será o único responsável pelos danos e prejuízos causados à (EMPRESA) como consequência de declarações inexatas ou falsas.

c. Manter a plena confidencialidade em relação a todas as informações que sejam geradas nas relações entre o FORNECEDOR e a (EMPRESA)

d. Não realizar qualquer modificação nas tecnologias de informação utilizadas no Portal, assim como não utilizar o mesmo com fins diferentes daqueles para os que foi previsto. Além disso, o FORNECEDOR não irá aceder a áreas não autorizadas do Portal.

e. Cumprir fielmente os seus compromissos nas informações enviadas através do portal. No caso de o FORNECEDOR não demonstrar a diligência comercial necessária, ou não cumprir as obrigações celebradas, a (EMPRESA) reserva-se o direito de excluir permanente ou temporariamente o FORNECEDOR do portal.

f. O FORNECEDOR apenas deverá indicar os grupos de materiais referentes a bens ou serviços que, aquando da celebração do acordo, sejam comercializados, fabricados ou distribuídos pelo mesmo e que sejam do interesse comercial da (EMPRESA).

g. O FORNECEDOR aceita que os orçamentos introduzidos no Portal sejam considerados como tendo o mesmo nível e validade de qualquer orçamento enviado por qualquer outro meio tradicional (carta, fax).
h. O FORNECEDOR está obrigado a utilizar corretamente o Portal e os Serviços em conformidade com a legislação, o presente Aviso Legal e outra regulamentação relativa à utilização e às instruções que tenham chegado ao seu conhecimento, assim como com a moral e os bons costumes geralmente aceites e a ordem pública.
Assim, não irá utilizar o Portal ou qualquer um dos Serviços para fins ilícitos, proibidos no presente Aviso Legal, em prejuízo dos direitos ou interesses de terceiros. Em particular, e a título meramente indicativo, o FORNECEDOR compromete-se a não transmitir, divulgar ou colocar à disposição de terceiros as informações, dados, conteúdos, gráficos, ficheiros de áudio e/ou imagem, fotografias, gravações, software e, de forma geral, qualquer tipo de material que:
1. Seja contrário aos direitos fundamentais e liberdades públicas reconhecidos na Constituição, em Tratados Internacionais e na legislação aplicável; 
2. Induza, incite ou promova atividades criminosas, contrárias à lei, à moral e bons costumes geralmente aceites ou à ordem pública; 
3. Seja falso, inexato ou possa induzir em erro, ou represente publicidade ilícita, enganosa ou desleal;
4. Esteja protegido por direitos de propriedade intelectual ou industrial que pertençam a terceiros, sem a autorização dos mesmos;
5. Viole os direitos à honra, à privacidade ou à própria imagem;
6. Viole as normas relativas ao sigilo das comunicações;
7. Represente concorrência desleal, ou prejudique a imagem empresarial da (EMPRESA) ou de terceiros;
8. Esteja infetado com vírus ou elementos semelhantes que possam danificar ou impedir o funcionamento correto do Portal, dos equipamentos informáticos ou dos seus ficheiros e documentos.

4. Direitos do FORNECEDOR

O FORNECEDOR tem os seguintes direitos:

1. Manter uma presença constante na base de dados da (EMPRESA), na sua qualidade de fornecedor registado. 

2. Receber pedidos de orçamentos ao abrigo das normas estabelecidas. 


5. Obrigações da (EMPRESA) 

A (EMPRESA) tem as seguintes obrigações:

a. Manter atualizadas as informações que considere adequadas, não se responsabilizando pelos erros que se possam produzir por força maior ou caso fortuito.

b. Manter a total confidencialidade de todas as informações relativas ao fornecedor, quer se tratem de informações proporcionadas pelo mesmo ou geradas nas relações entre o FORNECEDOR e a (EMPRESA)

c. Facultar em qualquer momento ao fornecedor o estado dos seus dados na base de dados e não facultar o mesmo a terceiros salvo para permitir que o fornecedor apresente os seus orçamentos e os seus produtos sejam tidos em consideração como alternativa de fornecimento. Além disso, facultar em qualquer momento ao fornecedor o estado dos respetivos dados na base de dados, para que este, mediante notificação prévia por escrito, os altere ou elimine. 


6. Direitos da (EMPRESA)

a. O Utilizador dá o seu consentimento para que a (EMPRESA) ceda os respetivos dados às suas empresas associadas ou do grupo a que pertence, assim como a outras com as quais celebre acordos, com o único fim de prestar o melhor serviço, respeitando sempre a legislação espanhola relativa à proteção dos dados pessoais. Além disso, o Utilizador aceita que a (EMPRESA) ou as suas empresas associadas, filiais e empresas comuns (joint ventures) lhe enviem informações sobre quaisquer bens ou serviços comercializados, ou que possam ser comercializados no futuro, direta ou indiretamente pelas mesmas. A aceitação por parte do Utilizador para que os seus dados possam ser cedidos da forma estabelecida no presente parágrafo tem sempre um carácter revogável sem efeitos retroativos, em conformidade com o disposto na Lei Orgânica espanhola 15/1999, de 13 de dezembro. 

b. Tomar a opção de, caso o FORNECEDOR não cumpra as suas obrigações, exigir o devido cumprimento das mesmas ou rescindir o contrato, nos termos da cláusula 8 do presente Aviso Legal. 

7. Limitação de responsabilidade

a. A (EMPRESA) não assume qualquer responsabilidade que derive da indisponibilidade do sistema, de falhas da rede ou falhas técnicas que provoquem um corte uma interrupção do Portal. Além disso, a (EMPRESA) não se responsabiliza pelos danos e prejuízos que resultem da propagação de vírus informáticos ou outros elementos no sistema.
b. A (EMPRESA) não assume qualquer responsabilidade quanto a pagamentos e/ou reclamações que resultem de ações pelos clientes em relação ao acordo entre esses clientes e o FORNECEDOR

c. A (EMPRESA) não assume qualquer responsabilidade quanto a qualquer ação que seja contrária às leis, usos e costumes, ou que viole as obrigações estabelecidas no presente Aviso Legal, por parte de alguma pessoa da (EMPRESA), do FORNECEDOR ou de qualquer utilizador da rede. No entanto, sempre que tiver conhecimento de qualquer conduta referida no parágrafo anterior, a (EMPRESA) irá adotar as medidas necessárias para resolver os possíveis litígios com a maior urgência e diligência. 
d. A (EMPRESA) considera-se isenta de qualquer responsabilidade que resulte da interferência por parte de terceiros não autorizados no conhecimento das condições e circunstâncias de utilização do Portal e dos Serviços pelos FORNECEDORES.


8. Rescisão do acordo

Caso alguma das obrigações incluídas no presente Aviso Legal não seja cumprida, o FORNECEDOR ou a (EMPRESA) terá de notificar esse incumprimento à parte que o tenha cometido, concedendo-lhe um prazo de quinze dias úteis após a notificação para retificar o mesmo. Se o incumprimento não tiver sido retificado após esse prazo, a outra parte poderá decidir que o presente acordo seja cumprido ou rescindido, com uma indemnização por danos e prejuízos em qualquer um dos casos, em conformidade com o que se encontra estabelecido no artigo 1.124 do Código Civil espanhol. No caso de a decisão tomada ser a rescisão, o FORNECEDOR e a (EMPRESA) concordam que a respetiva notificação será suficiente para que esta produza todos os seus efeitos.


9. Duração 

O presente acordo terá uma duração indefinida desde que não ocorra uma denúncia por escrito por qualquer uma das partes com, pelo menos, 1 mês de antecedência.
O procedimento para o cancelamento do registo será através do envio de um e-mail com o Assunto "Cancelamento do registo no portal" para o endereço atencionalcliente@(COMPANY).com. Caso não seja possível enviar um e-mail, será enviado um fax para o n.º 91 296 20 25 com a indicação dos dados básicos para identificar o FORNECEDOR.


10. Notificações

a. As notificações entre as partes poderão ser realizadas por qualquer via de direito que permita ter conhecimento da receção, incluindo o fax.

b. As mudanças de domicílio e os faxes serão notificados por escrito e não produzirão efeitos até que tenham passado dois dias úteis após a sua receção. Em qualquer caso, o novo domicílio e fax estará necessariamente dentro do território espanhol.

c. As relações entre a (EMPRESA) e o FORNECEDOR que derivem da utilização do Portal e dos serviços prestados através do mesmo serão sujeitas à legislação e jurisdição espanholas. 


        </textarea>
              </font> </td>
            </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td align="left" class="textos"><p><span class="subtextos">Você aceita os termos de uso do portal de compras?</span><br>
                <br>
           Se você clicar em "Não aceito" , o processo vai acabar.Para continuar o processo de registro deve clicar em "Sim, aceito". Se você deseja imprimir o contrato, clique em "Imprimir". <br>
      <br>
          
      <div align="center"></div>
          <table border="0" width="39%">
            <tr>
              <td width="41%" valign="middle"><p align="center">
                  <input type="submit" onclick="return alta()" value="Sim, aceito" id="submit1" name="submit1">
              </p></td>
              <td width="38%" valign="middle"><input type="button" onclick="no_alta()" value="Não aceito" id="button1" name="button1">
              </td>
              <td width="21%" valign="middle"><input type="button" onclick="imprimir()" value="Imprimir" id="button2" name="button2">
              </td>
            </tr>
          </table>
        <p align="center"></td>
      </tr>
  </table>
  <input type="hidden" name="ACEPTADO">
	  
</div>
</form>

</body>

</html>
