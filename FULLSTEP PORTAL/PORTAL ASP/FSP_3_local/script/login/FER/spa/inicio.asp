﻿<%@ Language=VBScript %>
<!--#include file="../../../common/idioma.asp"-->
<!--#include file="../../../common/formatos.asp"-->
<!--#include file="../../../common/acceso.asp"-->
<!--#include file="../../../common/Pendientes.asp"-->
<script SRC="../../../common/menu.asp"></script>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../../../common/estilo.asp">
<link href="../estilos.css" rel="stylesheet" type="text/css">


<title>::Portal de Compras de Ferroser ::</title>
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
	if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
}
-->
</style>
</head>

<script>
dibujaMenu(1)
</script>

<script language="JavaScript" type="text/JavaScript">

function ventanaSecundaria2 (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function Init(){
	document.getElementById('tablemenu').style.display = 'block';

	MM_preloadImages('../images/icono_docs_sm.gif', '../images/icono_docs_sm.gif')

	p = window.top.document.getElementById("frSet")
	vRows = p.rows
	vArrRows = vRows.split(",")
	vRows = vArrRows[0] + ",*,0,0"
	p.rows = vRows
}

</script>

<body topmargin="0" scroll="yes" bgcolor="#ffffff" onLoad="Init()">
<!-------------------------- BEGIN COPYING THE HTML HERE ----------------------------> 
<table width="100%" border="0" cellpadding="5" cellspacing="2" bordercolor="0" hspace="0" vspace="0">
  <tr> 
	<td width="12" rowspan="3" align="right" valign="top" bordercolor="0"> <div align="left" class="textos"></div>
	</td>
	<td height="74" colspan="2" align="left" valign="middle"><font size="2" face="verdana" class="titulo"><b>Bienvenido a la zona privada de proveedores </b></font></td>
	<td colspan="2" align="left" valign="bottom">&nbsp;</td>
  </tr>
  <tr>
	<td rowspan="2" valign="top" bgcolor="#EEEEEE" class="textos">
	<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td class="textos">Puede acceder a las distintas &aacute;reas a trav&eacute;s de las opciones de men&uacute; situadas en la parte superior.
	  
	  <ul>
      <li><b>Calidad:</b> Conozca qué documentos debe aportar para ser proveedor de Ferrovial Servicios España. Cárguelos y manténgalos actualizados.</li></ul>
      <ul>
		<li><b>Solicitudes de oferta:</b> acceda a las solicitudes de oferta  y gesti&oacute;nelas desde este mismo apartado.</li>
	  </ul>
       <ul>
		<li><b>Pedidos:</b> consulte el estado de sus pedidos.</li>
	  </ul>
	  
	  <ul>
		<li><b>Sus datos:</b> si lo desea puede modificar sus datos de contacto, cambiar su contrase&ntilde;a o la configuraci&oacute;n de los formatos de la solicitud.</li>
	  </ul>
	   <ul>
		<li><b>Su compa&ntilde;&iacute;a :</b> si usted es el contacto principal, puede modificar los datos de su empresa, usuarios, as&iacute; como las &aacute;reas de actividad en las que su empresa se encuentra homologada. </li>
	  </ul>
	  <br>
Si es la primera vez que va a realizar una oferta a trav&eacute;s del portal, siga atentamente los siguientes pasos:<br>
<br>
<br>
<b>1. Pulse en el Módulo “Calidad”</b>, y adjunte la documentación solicitada<br>
<br>
<b>2. Entre en el Módulo “Solicitudes de oferta”</b> para ver  para ver las peticiones de ofertas que tiene abiertas su empresa.<br>
<br>
<b>3. Seleccione la solicitud de oferta</b> a la que quiera responder,<b> pulsando sobre el c&oacute;digo de la misma. </b><br>
<br>
<b>4. Realice su oferta completando toda la informaci&oacute;n necesaria: </b>desde el &aacute;rbol de navegaci&oacute;n que encontrar&aacute; en la parte izquierda, podr&aacute; desplazarse por los distintos apartados que conforman la oferta. Para introducir los precios deber&aacute; ir al apartado "items/precios". Recuerde introducir el plazo de validez de la oferta, en el apartado "Datos Generales de la oferta". <br>
<br>
<b>5. Comunique su oferta pulsando sobre el bot&oacute;n de enviar</b><b><IMG height=14 src="../images/sobre_tr.gif" width=30 align=absBottom></b>       </td>
  </tr>
</table>

	</td>
	<td rowspan="2" valign="top" class="textos">&nbsp;</td>
	<td colspan="2" align="left" valign="top"><table width="100%" height="433" border="0" class="textos">
		<tr>
		  <td height="26" valign="top" class="textos"><span class="textos"><strong>INSTRUCCIONES</strong></span></td>
		</tr>
		<tr>
		  <td height="62" class="textos">Descárguese las instrucciones 
	  sobre cómo realizar una oferta, modificaci&oacute;n de datos, etc. </td>
		</tr>
      
		
		<tr>
		  <td><a href="docs/FSN_MAN_ATC_Requisitos tecnicos.pdf" class="textos" target="_blank"><img src="../images/flecha1.gif" width="12" height="8" border="0"> Requisitos t&eacute;cnicos para acceder al Portal </a></td>
		</tr>
		<tr>
		  <td><a href="docs/FSN_MAN_ATC_Mantenimiento de datos.pdf" class="textos" target="_blank"><img src="../images/flecha1.gif" width="12" height="8" border="0"> Modificación de datos del proveedor </a></td>
		</tr>
		<tr>
		  <td><a href="docs/FER_GUI_Proveedor.pdf" class="textos" target="_blank"><img src="../images/flecha1.gif" width="12" height="8" border="0"> Guía del Módulo de Calidad </a> (Cómo adjuntar la documentación solicitada para trabajar con FSE) </td>
	    </tr>
        <tr>
		  <td class="textos"><a href="docs/FSN_MAN_ATC_Como ofertar.pdf" class="textos" target="_blank"><img src="../images/flecha1.gif" width="12" height="8" border="0"> Guía del Módulo de Solicitudes de oferta</a>(cómo ofertar)</td>
		</tr>
        <tr>
		  <td><a href="docs/FER_Declaracion_Responsable.pdf" class="textos" target="_blank"><img src="../images/flecha1.gif" width="12" height="8" border="0"> Declaración Responsable</a> <span class="solutions">¡Nuevo!</span></td>
	    </tr>
        <tr>
		  <td><a href="docs/FER_Normas_de_comportamiento_medioambiental.pdf" class="textos" target="_blank"><img src="../images/flecha1.gif" width="12" height="8" border="0"> Normas de comportamiento medioambiental </a> <span class="solutions">¡Nuevo!</span></td>
	    </tr>
        <tr>
		  <td><a href="docs/FER_Politica_de_calidad_medioambiente_y_energia.pdf" class="textos" target="_blank"><img src="../images/flecha1.gif" width="12" height="8" border="0"> Política de Calidad, Medioambiente y Energía </a> <span class="solutions">¡Nuevo!</span></td>
	    </tr>
          <tr>
		  <td><a href="docs/FER_Condiciones_generales_de_compra_y_contratacion.pdf" class="textos" target="_blank"><img src="../images/flecha1.gif" width="12" height="8" border="0"> Condiciones generales de compra </a> <span class="solutions">¡Nuevo!</span> </td>
		</tr>
		<tr>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td height="113" valign="top" class="textos"><strong>VIDEOS - TUTORIALES </strong><BR><BR>
		  Puede acceder a los videos tutoriales sobre &quot;<span class="registro">Recepciones de pedidos desde el Portal</span>&quot; y &quot;<span class="registro">Módulo calidad</span>&quot;, haciendo click sobre la imagen correspondiente. Para ver el video a pantalla completa, por favor pulse sobre el bot&oacute;n de maximizar pantalla en la parte inferior derecha.  </td>
	    </tr>
		<tr>
		  <td><iframe src="http://player.vimeo.com/video/43025803?byline=0&amp;portrait=0&amp;color=ffffff" width="400" height="226" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></td>
	    </tr>
		<tr>
		  <td><iframe src="http://player.vimeo.com/video/62614486?byline=0&amp;portrait=0&amp;color=ffffff" width="400" height="226" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></td>
	    </tr>
	</table>
	</td>
  </tr>
  <tr>
	<td align="left" valign="bottom"><table width="100%" border="0" background="images/fondo.jpg">
  <tr>
	<td valign="bottom"><span class="subtit">Si tiene alguna duda, puede contactar con nosotros en el tel&eacute;fono
		<strong>902 760 700 </strong> o enviarnos un e-mail a la direcci&oacute;n: <a href="mailto:soporteproveedores.ferroser@fullstep.com">soporteproveedores.ferroser@fullstep.com</a></span></td>
  </tr>
  <tr>
    <td height="40" valign="bottom" class="textos"><div align="right"><a href="aviso%20legal.htm" target="_blank">aviso legal </a></div></td>
  </tr>
</table></td>
  </tr>

</table>
</body></html>
