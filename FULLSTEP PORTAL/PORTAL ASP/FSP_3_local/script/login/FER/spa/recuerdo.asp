﻿<%@  language="VBScript" %>
<!--#include file="../../recuerdo.asp"-->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
    <style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style>
    <link href="../estilos.css" rel="stylesheet" type="text/css">
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body>
    <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
    <input type="hidden" name="idioma" value="spa">
    <table width="620" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="20">
                &nbsp;
            </td>
            <td width="615" align="left">
                <img src="../images/logo.jpg" width="184" border="0">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width="20">
                &nbsp;
            </td>
            <td width="615" align="left">
                <span class="subtitulo"><u>Recordatorio de claves de acceso </u></font> </span><font
                    color="#666666" size="1" face="Verdana" class="textos">
                    <br>
                    <br>
                    Si ha olvidado sus claves de acceso, introduza la dirección de e-mail registrada
                    en el portal, y a continuación recibirá un e-mail con sus claves.<br>
                    <br>
                    <table class="textos">
                        <tr>
                            <td>Cód. Compañia:</td>
                            <td><input type="text" name="txtCia" id="txtCia" size="80" maxlength="100"/></td>
                        </tr>
                        <tr>
                            <td>Cód. Usuario:</td>
                            <td><input type="text" name="txtUsu" id="txtUsu" size="80" maxlength="100/"></td>
                        </tr>
                        <tr>
                            <td>Dirección de email:</td>
                            <td><input type="text" name="txtEmail" id="txtEmail" size="80" maxlength="100"/></td>
                        </tr>
                    </table>
                    <br />
                    <input type="submit" value="Enviar" name="cmdEnviar" id="cmdEnviar">
           
                    <br>
                    <br>
                    <br>
                    Si continúa con dificultades para acceder al portal o su dirección de email ha cambiado,
                    consulte con el servicio de atención a proveedores (Tel. 902 760 700)</font><font
                        color="#666666" size="1" face="Verdana">.</font>
            </td>
        </tr>
    </table>
</body>
</html>
