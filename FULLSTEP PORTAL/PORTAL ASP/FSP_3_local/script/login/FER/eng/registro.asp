﻿<%@ Language=VBScript %>
<!--#include file="../../../common/XSS.asp"-->

<html>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Sign up contract to Ferroser' Purchasing Portal</title>

<link href="../estilos.css" rel="stylesheet" type="text/css">
</head>


<script>
function no_alta()
{
window.close()

}
</script>

<script>

function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=ENG", "_blank")
window.close()

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=550,height=600,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
  </script>


<script>
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=550,height=600,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>


<body bgcolor="#FFFFFF" topmargin="5" leftmargin="5">
<form name="frmAlta" id="frmAlta" method="post">

<table border="0" width="100%">
  <tr>
    <td><img src="../images/logo.jpg" border="0" WIDTH="184">
	<hr width="100%" size="1" color="#FF7E00">
    </td>
  </tr>
  <tr>
    <td><font face="Verdana" size="1">
      <p align="left" class="textos"> To proceed with the sign up proccess, the following contract must be read and accepted.</p>
      <table border="0">
        <tr>
          <td valign="top"><p align="center"> </td>
          <td><font face="Verdana" size="1">
            <textarea readonly rows="11" name="S1" cols="80" style="font-family: Verdana; font-size: 8pt; text-align:justify; display:block; height:135px; line-height: 150%; list-style-type: lower-alpha">
TERMS AND CONDITIONS


1. INTRODUCTION AND COMPANY INFORMATION

The present text constitutes the Terms  and Conditions of the Website owned by Ferrovial Servicios, S.A.U. (hereinafter, THE COMPANY), URL www.ferrovialservicios.es.

THE COMPANY, with registered office at C/Príncipe de Vergara, 135, Tax ID/VAT A-80241789, and registered in the Commercial Registry of Madrid, T. 2081, f. 134, h.n. M-36972.
Also part of the same group of companies, Ferroser Servicios Auxiliares, S.A. with Tax ID/VAT A-28672038 and registered in the Commercial Registry of Madrid, V. 5802, F. 95, S. 3, h.n.  M-47681 and Ferroser Infraestructuras, S.A. Tax ID/VAT A-28423853 and registered in the Commercial Registry of Madrid, T. 25793, f. 168, h.n. M-36963. To contact THE COMPANY directly and effectively, you may contact us by email soporte.proveedores.ferroser@ferrovial.es or at +34 91 338 83 00.

2. PURPOSE AND SCOPE

These Terms and Conditions govern the access and use of the content offered by THE COMPANY through their Websites. However, THE COMPANY reserves the right to modify the presentation, layout and content of the Website and the services, as well as the conditions for its access and/or use. Access and use of the content and services after the entry into force of the amendments or changes in the conditions imply acceptance thereof.

However, access to certain content and use of certain services can be subject to particular conditions, which, depending on the case, substitute, complete and/or modify these Terms and Conditions. Nevertheless these general Terms and Conditions shall prevail over any conflicting terms within the particular conditions. 

Simple access, navigation and use of the Website entails and implies acceptance by the user of these Terms and Conditions, releasing THE COMPANY from any liability for the damage caused to the user or to his/her company or any other due to their performances in the portal.

In this sense, the User refers to the person accessing, browsing or viewing the content on the Website and Register User refers to the person that can access, navigate and register in order to use, store and/or download the Content and/or use the Website Services.

3. USER OBLIGATIONS
Access and navigation through the Website does not require registration except when, in order to use of certain features, data is required, such as name and email address.
Children under 14 years of age is prohibited to access and browse the Website, except with the prior express consent of their parents, guardians or legal representatives, who will be held responsible for the acts carried out by the children in their care, according to current regulations. In any case, it is presumed that access made by a child to the Website has been made with the express prior authorization of their parents, guardians or legal representatives.
The User agrees to:
• Accept and enforce the provisions established within these Terms and Conditions, and within any document which forms part thereof.
• Do not store or communicate through the Website, content that is contrary to applicable law, morality, public order, as well as those with defamatory, injurious, obscene, explicit sexually, offensive, violent or inciting violence, racism and xenophobia, or generally illegal or damaging the rights and/or physical and/or moral of people.
• Do not disclose personal information about other users of the Website, without their express prior authorization .
• Do not defame, abuse, harass, stalk, threaten or otherwise violate any rights of other users or any other persons.
• Do not remove or attempt to remove any security measures adopted and implemented in the Website.
• Comply with the provisions of law.

4. COPYRIGHT AND INDUSTRIAL.
THE COMPANY owns or has obtained the necessary license rights to use the intellectual property of the Website, as well as intellectual and industrial property rights of the information and materials, structure, selection, arrangement and presentation of content, services and products available within it, and the computer programs used in connection therewith.

Access, navigation, use, lodging and/or download of content and/or use of the Website services by the User, in any case will be constider a waiver, license or transfer of all or part of the aforementioned rights by THE COMPANY or of the holder of the corresponding rights. The user only has the right to private use, solely in order to enjoy the benefits of the service.

Therefore, it is not allowed to remove, ignore or manipulate the copyright notice ("copyright") and any other data identifying the rights of THE COMPANY or its respective owners included in the content and/or services and any technical protection devices or any information mechanisms and/or identification that might be contained in them.

In particular, it is strictly prohibited to use the Website content for inclusion, in whole or in part, on other websites without the prior written consent of the owner of this Website. Reference to names and trademarks or registered trademarks, logos or other distinctive signs, which are owned by THE COMPANY or by third parties, is implicitly forbid to use without the consent of THE COMPANY or its rightful owners. At no time, unless otherwise expressly stated, access or use of the Website and/or its content and/or services, gives the user the right on trademarks, logos and/or distinctive signs protected by the law
The Company reserves all rights of intellectual property, in particular, is forbidden to modify, copy, reuse, exploit, reproduce, alter, publicly communicate, make second or subsequent publications, upload files, post, transmit, use, process or otherwise distribute all or part of the content and products included on the Website for public or commercial purposes, if you do not have the express written permission of THE COMPANY or, where appropriate, the corresponding rightful holder.

5. LICENSE ON COMMUNICATIONS
In the event that the User sends any information to THE COMPANY through the Website, through the channels provided for this purpose on the Webpage, the User declares, warrants and agrees that it is entitled to do so freely, that such information not infringe any intellectual property, trademark, patent, trade secret, or other rights of third parties, the information is not confidential and that such information is not harmful to others.
The User acknowledges and will assume responsibility and holds THE COMPANY harmless for any communication provided personally or his name, reaching such liability without restriction the accuracy, legality, originality and ownership of it.

6. RESPONSIBILITIES AND WARRANTIES
THE COMPANY cannot guarantee the reliability, usefulness or accuracy of the information or services provided through the Website, nor the usefulness or accuracy of the documentation of the events that can be acquired through the Website, prepared by professionals across many industries.
Consequently, THE COMPANY does not guarantee or is responsible for: 
(i) the continuity of the contents of the Website, 
(ii) the absence of errors in the content, or products, 
(iii) the absence of viruses and/or other components harmful to the Website or the server that supplies, 
(iv) the invulnerability of the Website and/or the impregnability of the security measures taken in the same, (v) the lack of utility or performance of the contents Website and products, 
(vi) the damage or harm caused to himself or a third party, by any person who breaches the conditions, rules and instructions that THE COMPANY set within the Website or through the violation of Website security systems.

Nevertheless, THE COMPANY states that it has taken all necessary measures, within its capabilities and state of the art to ensure the operation of the Website and avoid the existence and transmission of viruses and other harmful components to Users.

If the User becomes aware of the existence of any content unlawful, illegal, contrary to the laws or that could result in a violation of intellectual and/or industrial property rights,, must immediately notify THE COMPANY so that it can proceed with the adoption of appropriate action.

7. LINKS
7.1. Links to other websites
When in the Website, the User may find links to other Webpages via buttons, links, banners, etc., These would be managed by third parties. The Company does not have the power or human or technical resources to know, control or endorse any information, content, products or services provided by other Websites that can establish links from THE COMPANY´s Website.

Consequently, THE COMPANY shall not assume any responsibility for any aspect of the Webpage to which it could establish a link from the Website, in particular, by way of example and without limitation, its operation, access, data, information, files, quality and reliability of its products and services, their own links and/or any of its contents in general.
In this sense, if users were aware of the illegality of activities through these third party websites, must immediately notify THE COMPANY to which the effects appropriate to disable the access link to it.
The establishment of any link from the Website to another Website does not imply that there is some relationship, collaboration or dependence between THE COMPANY and the responsible third party.

7.2. Links to other Websites to THE COMPANY´s Website
If any User, Company or Website wants to establish some kind of link to the Website must comply with the following conditions:

 The link will only be made to the Home or Home Website, unless express written authorization of THE COMPANY.
 The link must be absolute and complete, that is, must take the user through a click to the URL address of the Website and must include the full extent of the Home screen of the Website. In any case, unless THE COMPANY expressly authorized in writing, the Website providing the link may reproduced, in any manner, the website, include it as part of your website or in one of its "frames" or create a "browser" on any page of the Website.
 On the third party Webpage that display the link it is forbid to, in any way, declare that THE COMPANY has authorized this link, unless THE COMPANY has done so expressly and in writing. If the company providing the link from its page to the Website wishs to include on its Website the brand name, trademark, label, logo, slogan or other identifying element of THE COMPANY and/or the Website must obtain prior express permission in writing.
 THE COMPANY does not authorize the establishment of a link to the Website from Webpages containing illicit, illegal, degrading, obscene materials or information, and that contravene morality, public order or social standards generally accepted.

7.3. Services provided by third parties through the Website
THE COMPANY does not guarantee the legality, reliability and usefulness of the services provided and the content supplied by third parties via this page or where THE COMPANY only acts as an advertising channel.

THE COMPANY is not liable for damages of any kind caused by the services, by third-party content advertised within the Website, and in particular, without limitation, those caused by:

• Failure to comply with the law, morality or public order.
• The incorporation of virus or any other computer code, files or programs that may damage, interrupt or prevent the normal functioning of any software, hardware or telecommunications equipment.
• Violation of the rights of intellectual property, or contractual commitments of any kind.
• The performance of acts that constitute unlawful, unfair and deceptive or generally constitute unfair competition.
• A lack of truthfulness, accuracy, quality, relevance and/or timeliness of the content transmitted, distributed, stored, received, obtained, made available or accessible.
• Violation of the rights to honor, personal and family privacy and image of persons or, in general, any type of third party rights.
• The unsuitability for any purpose and fulfillment of expectations generated, or the vices and defects that may arise in relation to third.
• The failure, delay in performance, defective performance or termination for any reason of the obligations assumed by third parties and contracts with third parties.
• The data communication to occur between users.
THE COMPANY shall not be liable, in cases where third parties advertise their services and/or content on the Website, for the accuracy of the information provided by the supplier of the services and/or contents, for obtaining the authorizations to fulfill administrative requirements established to provide services, for the infringement of rights of third parties by the service provider and, in general, of any required obligation or warrant that the services supplier has towards Users.
THE COMPANY does not have the power or human and technical resources to know, control or approve all information, content, products or services provided by other Websites that have established links to THE COMPANY´s Website. The Company does not assume any responsibility for any aspect of third Websites that display a link to the Website, in particular, by way of example and without limitation, its operation, access, data, information, files, quality and reliability of their products and services, their own links and/or any of its contents in general.

8. COOKIES
THE COMPANY could process information about visitors to the Website. To that end, this Website may use cookies or other usual invisible systems, through which information is obtained regarding the frequency of visits, the most content, geographic location, as well as other data in order to optimize and improve navigating within Website. By navigating through the Website, you acknowledge acceptance to the collection and processing of such information.

Specifically, our Website uses Google Analytics, a web analytics service provided by Google, Inc., a Delaware company whose main office is at 1600 Amphitheatre Parkway, Mountain View (California), CA 94043, USA ("Google"). Google Analytics uses "cookies", which text files are placed on your computer, to help the website analyze how users use the site.

The information generated by the cookie about your use of the website (including your IP address) will be transmitted to and stored by Google on servers in the United States. Google will use this information on our behalf in order to keep track of your use of the website, compiling reports on website activity and providing other services relating to website activity and internet usage. Google may also transfer this information to third parties where required to do so by law, or where such third parties process the information on Google's behalf. Google will not associate your IP address with any other data held by Google.
You may refuse processing of the data or information by rejecting the use of cookies by selecting the appropriate settings on your browser, however, you should know that by doing so you may not be able to use the full functionality of this Website. By using this Website you consent the processing of data about you by Google in the manner and for the purposes described above.

9. PROFILE
The privacy policy of THE COMPANY is established by the provisions stated on the document PRIVACY POLICY. 

10. DURATION AND MODIFICATION
THE COMPANY may modify the terms and conditions herein, wholly or partially, publishing any change in the same way it appears these Terms and Conditions or through any type of communication to the Users.

Therefore, the validity of these Terms and Conditions correspond with the time of display, until modified in whole or in part, when will be in effect the amended Terms and Conditions.

Notwithstanding the provisions of the particular conditions, THE COMPANY may terminate, suspend or discontinue at any time without prior notice, access to the contents of the page, without possibility for the user to demand compensation. Upon such termination, the prohibitions regarding the use of contents as stated in these Terms and Conditions shall remain.

The withdrawal procedure may be done by sending an email with subject "Unsubscribe Portal" address soporte.proveedores.ferroser@ferrovial.es

11. GENERAL
The titles of the various clauses are for information only and do not affect, qualify or extend the interpretation of these Terms and Conditions.

In case of any conflict between the provisions of these Terms and Conditions and particular Conditions of each specific service, shall prevail the latter.
In the event that any provision or provisions of these Terms and Conditions were considered null or unenforceable, in whole or in part by any court, tribunal or competent administrative body, such invalidity or inapplicability shall not affect the other provisions of these Terms and Conditions 
The failure to exercise or performance by THE COMPANY of any right or provision of these Terms and Conditions shall not constitute a waiver thereof, unless expressly stated in writing on your part.
12. JURISDICTION
The relations between THE COMPANY and the User shall be governed by the provisions of the current regulations regarding the applicable law and jurisdiction. However, for cases in which the rules provide the opportunity to the parties to submit to a jurisdiction, THE COMPANY and the User, expressly waiving any other jurisdiction that may apply, submit any controversy and/or knowledge of litigation the Courts and Tribunals of the city of Madrid.
PRIVACY POLICY
This Privacy Policy is an integral part of the legal notice and the Web www.ferrovialservicios.es.

In compliance with the provisions of Ley Orgánica 15/1999 de 13 de Diciembre, de Protección de Datos de Carácter Personal, FERROVIAL SERVICIOS, S.A.U., FERROSER SERVICIOS AUXILIARES, S.A. Y FERROSER INFRAESTRUCTURAS, S.A. (hereinafter, THE COMPANY) inform you and you expressly consent that:
1. Personal data provided by you through this Website, is included in a file owned by THE COMPANY, duly registered in the General Registry of Data Protection. The purpose of this file is to manage the users of the Website, the provision of the requested services and, in general, the management, development and implementation of the established relationship between THE COMPANY and the User who provides personal information through the Website.
Finally please note that the information may be disclosed to the tax authorities and other public authorities, to comply with tax obligations, as well as financial institutions for the management of collections and payments.
2. You assure that the information submitted is true, accurate, complete and current, and you are responsible for any damages, direct or indirect, that may arise as a result of a breach of that obligation. In the event that the data provided correspond to a third party, you guarantee that such party has been informed of the terms contained in this document and has obtained permission to submit his/her information to THE COMPANY for the purpose stated above.
3. In the case of personal data collected through the form provided on the Website, you will need to provide, at least, the information on the fields marked with an asterisk, otherwise, THE COMPANY is not able to accept and manage the Website service or the query.
4. To exercise your rights of access, rectification, erasure and objection of your data, you can write to the following address soporte.proveedores.ferroser@ferrovial.es.
5. In response to the concern of THE COMPANY to ensure the security and confidentiality of your data, we have adopted the levels of security required to protect personal data and installed the technical means available to prevent the loss, misuse, alteration, unauthorized access and theft of personal data provided through the Website.
6. In accordance with the provisions of Ley 34/2002 de 11 de julio, de Servicios de la Sociedad de la Información y del Comercio Electrónico, in the event that you do not wish to receive commercial electronic communications  in the future, by THE COMPANY, you may express it by sending an email to the following email address: soporte.proveedores.ferroser@ferrovial.es.

</textarea>
          </font> </td>
        </tr>
      </table>
      </font>
        <p class="textos"> <b>Do you accept the  <a href="aviso%20legal.htm" target="_blank">terms and conditions,  the privacy policy </a>and the  <a href="docs/FER_Condiciones%20generales%20de%20compra_20090928%20_2_.pdf" target="_blank">Purchasing General Conditions</a>?<br>
                <br>
          </b> Shall you click on &quot;No, I refuse&quot;, the proccess will terminate and your company will not become a Ferroser supplier.<br>
          In order to continue the sign up proccess, you may click on &quot;Yes, I accept&quot;.<br>
          If you wish to print out the contract, you may do so by clicking on the following button. <br>
      <br>
          
        <div align="right">
          <table width="39%" border="0" align="left">
            <tr>
              <td width="41%"><p align="center">
                  <input type="submit" onClick="return alta()" value="Yes, I accept" id="submit1" name="submit1">
              </p></td>
              <td width="38%"><input type="button" onClick="no_alta()" value="No, I refuse" id="button1" name="button1">
              </td>
              <td width="21%"><input type="button" onClick="imprimir()" value="Print" id="button2" name="button2">
              </td>
            </tr>
          </table>
        </div><br><br>
        <p align="left" class="textos">        
        <hr width="100%" size="1" color="#FF7E00">
    </td>
  </tr>
</table>

	<input type="hidden" name="ACEPTADO">
	
</form>

</body>

</html>
