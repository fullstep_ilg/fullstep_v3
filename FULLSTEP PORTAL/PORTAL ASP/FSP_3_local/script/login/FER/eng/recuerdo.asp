﻿<%@  language="VBScript" %>
<!--#include file="../../recuerdo.asp"-->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
    <style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style>
    <link href="../estilos.css" rel="stylesheet" type="text/css" />
</head>
<script type="text/javascript">
    function Validar() {
        f = document.forms["frmRecuerdo"];
        if (f.txtCia.value == "") return false;
        if (f.txtUsu.value == "") return false;
        if (f.txtEmail.value == "") return false;        
    };
</script>
<body>
    <form name="frmRecuerdo" id="frmRecuerdo" method="post" onsubmit="return Validar()">
    <input type="hidden" name="idioma" value="spa">
    <table width="620" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="20">
                &nbsp;
            </td>
            <td width="615" align="left">
                <img src="../images/logo.jpg" border="0" width="184" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width="20">
                &nbsp;
            </td>
            <td width="615" align="left">
                <font color="#666666" size="1" face="Verdana" class="textos"><b><u>Access data reminder
                </u></b>
                    <br>
                    <br>
                    If you forgot your login data, fill in the registered e-mail address and you will
                    receive an e-mail with your login and password.<br>
                    <br>
                    <table class="textos">
                        <tr>
                            <td>
                                Company Code:
                            </td>
                            <td>
                                <input type="text" name="txtCia" id="txtCia" size="80" maxlength="100"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                User Code:
                            </td>
                            <td>
                                <input type="text" name="txtUsu" id="txtUsu" size="80" maxlength="100"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                E-mail address:
                            </td>
                            <td>
                                <input type="text" name="txtEmail" id="txtEmail" size="80" maxlength="100"/>
                            </td>
                        </tr>
                    </table>
                    <input type="submit" value="Submit" name="cmdEnviar" id="cmdEnviar"/>
                    <br/>
                    <br/>
                    <br/>
                    If you have problems while accessing the portal, or your e-mail address has changed,
                    please contact the portal administrato (Tel. +34 902 760 700). </font>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
