﻿<%@ Language=VBScript %>

<html>

<head>

<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/idioma.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->

<%
''' <summary>
''' Mostrar el detalle del registro
''' </summary>
''' <remarks>Llamada desde: login/login.asp		login/codigo instalaci�n/registro.asp	login/codigo instalaci�n/codigo idioma/registro.asp; Tiempo m�ximo: 0</remarks>
%>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Contrato Adhesi&oacute;n al portal de compras de Ferroser</title>

<link href="estilos.css" rel="stylesheet" type="text/css">

<script src="../../common/formatos.js"></script>
<script language="JavaScript" src="../../common/ajax.js"></script>
<!--#include file="../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
</head>


<script>
function no_alta()
{
window.close()

}
</script>

<script>

function alta()
{
setCookie("CONTRATOACEPTADO",1,new Date())
window.open("<%=Application("RUTASEGURA")%>script/registro/registro.asp?idioma=SPA", "_blank")
window.close()

return true
}
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=550,height=600,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}

function setCookie(name, value, expires)
         {
         //If name is the empty string, it places a ; at the beginning
         //of document.cookie, causing clearCookies() to malfunction.
		document.cookie = name + '=' + value + '; expires=' + expires.toGMTString();
		    
         }

function clearCookie(name)
         {                  
         expires = new Date();
         expires.setYear(expires.getYear() - 1);

         document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
         }
  </script>


<script>
function imprimir()
{
window.open("registro_texto.htm", "_blank","width=550,height=600,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no,top=15,left=15")

}
</script>


<script language="javascript"><!--

var msg = "Comando incorrecto.";

function RClick(boton){
if (document.layers && (boton.which == 3 || boton.which == 2)){alert(msg);return false}
if (document.all && event.button == 2 || event.button == 3)alert(msg)
return false}

document.onmousedown = RClick

//--></script>


<body bgcolor="#FFFFFF" topmargin="5" leftmargin="5" onload="init()">
<form name="frmAlta" id="frmAlta" method="post">

<table border="0" width="100%">
  <tr>
    <td><img src="images/logo.jpg" width="184" border="0">
    </td>
  </tr>
  <tr>
    <td><font face="Verdana" size="1">
      <p align="left" class="textos"> <b>Para continuar con el proceso de alta es imprescindible que lea y acepte las condiciones de acceso al Portal</b> </p>
      <table border="0">
        <tr>
          <td valign="top"><p align="center"> </td>
          <td><font face="Verdana" size="1">
            <textarea readonly rows="11" name="S1" cols="80" style="font-family: Verdana; font-size: 8pt; text-align:justify; display:block; height:135px; line-height: 150%; list-style-type: lower-alpha;">
AVISO LEGAL

 

1. INTRODUCCIÓN Y DATOS DE LA COMPAÑÍA

El presente texto se constituye como el Aviso Legal de la Web propiedad de Ferrovial Servicios, S.A.U. (en adelante, LA ENTIDAD), ubicada en la URL www.ferrovialservicios.es.

LA ENTIDAD es una entidad cuyo domicilio social se encuentra sito en C/Príncipe de Vergara, 135, con CIF A-80241789 e inscrita en el Registro Mercantil de MADRID tomo 2081, folio 134, hoja número M-36972.

También forman parte del mismo grupo de empresas de la Entidad la mercantil Ferroser Servicios Auxiliares, S.A. con CIF A-28672038 en inscrita en el Registro Mercantil de MADRID tomo 5802, folio 95, Sección 3 hoja número M-47681, así como Ferroser Infraestructuras, S.A. con CIF A-28423853 en inscrita en el Registro Mercantil de MADRID tomo 25793, folio 168, hoja número M-63963. Para comunicarse conLA ENTIDAD de manera directa y efectiva, podrá dirigirse a la dirección de correo electrónico soporte.proveedores.ferroser@ferrovial.eso ponerse en contacto en el teléfono+34 91 338 83 00

 2. OBJETO Y ÁMBITO DE APLICACIÓN

El presente Aviso Legal regula el acceso y uso a los Contenidos ofrecidos por LA ENTIDAD a través de sus Sitios Web. No obstante, LA ENTIDAD se reserva el derecho a modificar la presentación, configuración y contenido del Sitio Web y de los Servicios, así como también las condiciones requeridas para su acceso y/o utilización. El acceso y utilización de los Contenidos y Servicios tras la entrada en vigor de sus modificaciones o los cambios en las condiciones suponen la aceptación de las mismas. 

No obstante el acceso a determinados contenidos y la utilización de determinados servicios pueden encontrarse sometidos a determinadas Condiciones Particulares, que, según los casos, sustituirán, completarán y/o modificarán el presente Aviso Legal, o las Condiciones Generales y en su caso Particulares, prevalecerán los términos contradictorios de las Condiciones Particulares sobre las Condiciones Generales y el Aviso Legal.

El simple acceso, navegación y uso del Sitio Web conlleva y supone la aceptación por el Usuario del presente Aviso Legal, liberando a la ENTIDAD de toda responsabilidad por las posibles perjuicios que dichos usuarios pudieran causar a su empresa o cualquier otra debido a sus actuaciones en el portal.

En este sentido, se entenderá por Usuario a la persona que acceda, navegue o visualice los contenidos alojados en el Sitio Web y se entenderá por Usuario Registrado el que acceda, navegue y se registre para, utilizar, alojar y/o descargar los Contenidos y/o utilizar los Servicios del Sitio Web.

 

3. OBLIGACIONES DEL USUARIO

El acceso y navegación a través del Sitio Web no requiere Registro con la salvedad de que, para hacer uso de ciertas funcionalidades, pudiera requerirse la aportación de datos, tales como, nombre y dirección de correo electrónico.

Queda prohibido el acceso y navegación en el Sitio Web por parte de los menores de 14 años de edad, salvo que cuenten con la autorización previa y expresa de sus padres, tutores o representantes legales, los cuales serán considerados como responsables de los actos que lleven a cabo los menores a su cargo, conforme a la normativa vigente. En todo caso, se presumirá que el acceso realizado por un menor al Sitio Web se ha realizado con autorización previa y expresa de sus padres, tutores o representantes legales.

El Usuario se compromete a:

Aceptar y cumplir lo establecido en el presente Aviso Legal, y en cualquier documento que forme parte integrante del mismo. 
No almacenar ni comunicar a través del Sitio Web, Contenidos que sean contrarios a la legislación vigente, la moral, el orden público, así como aquellos que tengan carácter difamatorio, agresivo, obsceno, sexualmente explícito, ofensivo, violento o con incitación a la violencia, racista o xenófobo, o en general de carácter ilegal o lesivo de derechos y/o de la integridad física y/o moral de las personas. 
No difundir datos personales de otros Usuarios del Sitio Web, sin la previa y expresa autorización de éstos. 
No difamar, abusar, molestar, acosar, amenazar o vulnerar de cualquier otra manera cualquier derecho de otros Usuarios o de cualesquiera otras personas. 
No eliminar o intentar suprimir cualesquiera medidas de seguridad adoptadas e implantadas en el Sitio Web. 
Cumplir con lo establecido en la legislación vigente. 
 

4. DERECHOS DE PROPIEDAD INTELECTUAL E INDUSTRIAL.

LA ENTIDAD es titular o ha obtenido la correspondiente licencia sobre los derechos de explotación de propiedad intelectual e industrial del Sitio Web, así como de los derechos de propiedad intelectual e industrial sobre la información y materiales contenidos, la estructura, selección, ordenación y presentación de sus Contenidos, Serviciosy productos disponibles a través del mismo, así como los programas de ordenador utilizados en relación con el mismo.

El acceso, navegación, utilización, alojamiento y/o descarga de Contenidos y/o uso de Servicios del Sitio Web por el Usuario, en ningún caso se entenderá como una renuncia, transmisión, licencia o cesión total ni parcial de los derechos antes indicados por parte de LA ENTIDAD o, en su caso, del titular de los derechos correspondientes. El Usuario tan sólo dispone de un derecho de uso estrictamente privado, exclusivamente con la finalidad de disfrutar de las prestaciones del Servicio.

En consecuencia, no está permitido suprimir, eludir o manipular el aviso de derechos de autor ("copyright") y cualesquiera otros datos de identificación de los derechos de LA ENTIDAD o de sus respectivos titulares incorporados a los Contenidos y/o Servicios, así como los dispositivos técnicos de protección o cualesquiera mecanismos de información y/o de identificación que pudieren contenerse en los mismos. 

En particular, queda terminantemente prohibida la utilización de los Contenidos del Sitio Web para su inclusión, total o parcial, en otros sitios web ajenos al Sitio Web sin contar con la autorización previa y por escrito de los titulares del Sitio Web. Las referencias a nombres y marcas comerciales o registradas, logotipos u otros signos distintivos, ya sean titularidad de LA ENTIDAD o de terceras empresas, llevan implícitas la prohibición sobre su uso sin el consentimiento de LA ENTIDAD o de sus legítimos propietarios. En ningún momento, salvo manifestación expresa, el acceso o uso del Sitio Web y/o de sus Contenidos y/o Servicios, confiere al Usuario derecho alguno sobre las marcas, logotipos y/o signos distintivos en él incluidos protegidos por Ley.

Quedan reservados todos los derechos de propiedad intelectual e industrial y, en particular, queda prohibido modificar, copiar, reutilizar, explotar, reproducir, transformar, comunicar públicamente, hacer segundas o posteriores publicaciones, cargar archivos, enviar por correo, transmitir, usar, tratar o distribuir de cualquier forma la totalidad o parte de los Contenidos y productos incluidos en el Sitio Web para propósitos públicos o comerciales, si no se cuenta con la autorización expresa y por escrito de LA ENTIDAD o, en su caso, del titular de los derechos a que corresponda. 

 

5. LICENCIA SOBRE LAS COMUNICACIONES

En el caso de que el Usuario envíe información de cualquier tipo aLA ENTIDAD a través del Sitio Web, mediante los canales dispuestos a tal fin en la propia Página, el Usuario declara, garantiza y acepta que tiene derecho a hacerlo libremente, que dicha información no infringe ningún derecho de propiedad intelectual, de marca, de patente, secreto comercial, o cualquier otro derecho de tercero, que dicha información no tiene carácter confidencial y que dicha información no es perjudicial para terceros. 

El Usuario reconoce asumir la responsabilidad y dejará indemne a LA ENTIDAD por cualquier comunicación que suministre personalmente o a su nombre, alcanzando dicha responsabilidad sin restricción alguna la exactitud, legalidad, originalidad y titularidad de la misma. 

 

6. RESPONSABILIDADES Y GARANTÍAS

LA ENTIDAD no puede garantizar la fiabilidad, utilidad o veracidad de los servicios o de la información que se preste a través del Sitio Web, ni tampoco de la utilidad o veracidad de la documentación de los eventos que pueden ser adquiriros a través del Sitio Web, preparada por profesionales de muy diversos sectores. 

En consecuencia, LA ENTIDAD no garantiza ni se hace responsable de: (i) la continuidad de los contenidos del Sitio Web; (ii) la ausencia de errores en dichos contenidos o productos; (iii) la ausencia de virus y/o demás componentes dañinos en el Sitio Web o en el servidor que lo suministra; (iv) la invulnerabilidad del Sitio Web y/o la inexpugnabilidad de las medidas de seguridad que se adopten en el mismo; (v) la falta de utilidad o rendimiento de los contenidos y productos del Sitio Web; (vi) los daños o perjuicios que cause, a sí mismo o a un tercero, cualquier persona que infringiera las condiciones, normas e instrucciones queLA ENTIDADestablece en el Sitio Web o a través de la vulneración de los sistemas de seguridad del Sitio Web. 

Ello no obstante, LA ENTIDADdeclara que ha adoptado todas las medidas necesarias, dentro de sus posibilidades y del estado de la tecnología, para garantizar el funcionamiento del Sitio Web y evitar la existencia y transmisión de virus y demás componentes dañinos a los Usuarios. 

Si el Usuario tuviera conocimiento de la existencia de algún contenido ilícito, ilegal, contrario a las leyes o que pudiera suponer una infracción de derechos de propiedad intelectual y/o industrial, deberá notificarlo inmediatamente a LA ENTIDADpara que ésta pueda proceder a la adopción de las medidas oportunas.

 

7. ENLACES

7.1. Enlaces a otras páginas Web

En caso de que en el Sitio Web, el Usuario pudiera encontrar enlaces a otras páginas Web mediante diferentes botones, links, banners, etc., éstos serían gestionados por terceros. LA ENTIDAD no tiene facultad ni medios humanos ni técnicos para conocer, controlar ni aprobar toda la información, contenidos, productos o servicios facilitados por otros sitios Web a los que se puedan establecer enlaces desde el Sitio Web. 

En consecuencia, LA ENTIDADno podrá asumir ningún tipo de responsabilidad por cualquier aspecto relativo a la página Web a la que se pudiera establecer un enlace desde el Sitio Web, en concreto, a título enunciativo y no taxativo, sobre su funcionamiento, acceso, datos, información, archivos, calidad y fiabilidad de sus productos y servicios, sus propios enlaces y/o cualquiera de sus contenidos, en general. 

En este sentido, si los Usuarios tuvieran conocimiento efectivo de la ilicitud de actividades desarrolladas a través de estas páginas Web de terceros, deberán comunicarlo inmediatamente a LA ENTIDAD alos efectos de que se proceda a deshabilitar el enlace de acceso a la misma.

El establecimiento de cualquier tipo de enlace desde el Sitio Web a otro Sitio Web ajeno no implicará que exista algún tipo de relación, colaboración o dependencia entre LA ENTIDAD y el responsable del Sitio Web ajeno. 

 

7.2. Enlaces en otras páginas Web con destino al Sitio Web

Si cualquier Usuario, entidad o Sitio Web deseara establecer algún tipo de enlace con destino al Sitio Web deberá atenerse a las siguientes estipulaciones: 

El enlace solamente se podrá dirigir a la Página Principal o Home del Sitio Web,salvo autorización expresa y por escrito deLA ENTIDAD. 
El enlace debe ser absoluto y completo, es decir, debe llevar al Usuario, mediante un clic, a la propia dirección URL del Sitio Web y debe abarcar completamente toda la extensión de la pantalla de la Página Principal del Sitio Web. En ningún caso, salvo que LA ENTIDAD lo autorice de manera expresa y por escrito, el Sitio Web que realiza el enlace podrá reproducir, de cualquier manera, el Sitio Web, incluirlo como parte de su Web o dentro de uno de sus "frames" o crear un "browser" sobre cualquiera de las páginas del Sitio Web. 
En la página que establece el enlace no se podrá declarar de ninguna manera que LA ENTIDAD ha autorizado tal enlace, salvo que LA ENTIDAD lo haya hecho expresamente y por escrito. Si la entidad que realiza el enlace desde su página al Sitio Web correctamente deseara incluir en su página Web la marca, denominación, nombre comercial, rótulo, logotipo, slogan o cualquier otro tipo de elemento identificativo de LA ENTIDADy/o de el Sitio Web, deberá contar previamente con su autorización expresa y por escrito. 
LA ENTIDAD no autoriza el establecimiento de un enlace al Sitio Web desde aquellas páginas Web que contengan materiales, información o contenidos ilícitos, ilegales, degradantes, obscenos, y en general, que contravengan la moral, el orden público o las normas sociales generalmente aceptadas. 
 

7.3. Servicios prestados por terceros a través del Sitio Web

LA ENTIDAD no garantiza la licitud, fiabilidad y utilidad de los servicios prestados, de los Contenidos suministrados por terceros a través de esta página o sobre los que LA ENTIDAD únicamente actúe como cauce publicitario. 

LA ENTIDAD no será responsable de los daños y perjuicios de toda naturaleza causados por los servicios prestados, los Contenidos de terceros que se publiciten a través del Sitio Web, y en particular, a título meramente enunciativo, los causados por: 

El incumplimiento de la ley, la moral o el orden público. 
La incorporación de virus o cualquier otro código informático, archivo o programa que pueda dañar, interrumpir o impedir el normal funcionamiento de cualquier software, hardware o equipo de telecomunicaciones. 
La infracción de los derechos de Propiedad Intelectual e Industrial, o de compromisos contractuales de cualquier clase. 
La realización de actos que constituyan publicidad ilícita, engañosa o desleal y, en general, que constituyan competencia desleal. 
La falta de veracidad, exactitud, calidad, pertinencia y/o actualidad de los Contenidos transmitidos, difundidos, almacenados, recibidos, obtenidos, puestos a disposición o accesibles. 
La infracción de los derechos al honor, a la intimidad personal y familiar y a la imagen de las personas o, en general, cualquier tipo de derechos de terceros. 
La inadecuación para cualquier clase de propósito y la defraudación de las expectativas generadas, o los vicios y defectos que pudieran generarse en la relación con terceros. 
El incumplimiento, retraso en el cumplimiento, cumplimiento defectuoso o terminación por cualquier causa de las obligaciones contraídas por terceros y contratos realizados con terceros. 
La comunicación de datos que se produzca entre Usuarios. 
LA ENTIDAD no será responsable, en los casos en los que terceras entidades publiciten sus servicios y/o Contenidos en el Sitio Web, de la veracidad de la información aportada por el proveedor sobre dichos servicios y/o Contenidos, de la obtención de las autorizaciones administrativas que pudieran serle exigidas al proveedor para la prestación de sus servicios, de la vulneración por el proveedor de derechos de terceros y, en general, de cualquier obligación o garantía exigible al proveedor frente a los Usuarios.

LA ENTIDAD no tiene facultad ni medios humanos y técnicos para conocer, controlar ni aprobar toda la información, contenidos, productos o servicios facilitados por otros sitios Web que tengan establecidos enlaces con destino al Sitio Web. LA ENTIDAD no asume ningún tipo de responsabilidad por cualquier aspecto relativo al Sitio Web que establece ese enlace con destino al Sitio Web, en concreto, a título enunciativo y no taxativo, sobre su funcionamiento, acceso, datos, información, archivos, calidad y fiabilidad de sus productos y servicios, sus propios enlaces y/o cualquiera de sus contenidos, en general.

 

8. COOKIES

LA ENTIDAD podría tratar información acerca de los visitantes del Sitio Web. A tal fin, esta Web puede utilizar cookies u otros sistemas invisibles habituales, a través de los cuales se obtenga información sobre frecuencia de visitas, de los contenidos más seleccionados, ubicación geográfica, así como, de otros datos con el fin de optimizar y mejorar la navegación por el Sitio Web. Con la navegación por el Sitio Web, el usuario manifiesta la aceptación a la recogida y tratamiento de tal información. 

En concreto, nuestra página web utiliza Google Analytics, un servicio analítico de web prestado por Google, Inc., una compañía de Delaware cuya oficina principal está en 1600 Amphitheatre Parkway, Mountain View (California), CA 94043, Estados Unidos ("Google"). Google Analytics utiliza "cookies", que son archivos de texto ubicados en su ordenador, para ayudar al website a analizar el uso que hacen los usuarios del sitio web.

La información que genera la cookie acerca de su uso del website (incluyendo su dirección IP) será directamente transmitida y archivada por Google en los servidores de Estados Unidos. Google usará esta información por cuenta nuestra con el propósito de seguir la pista de su uso del website, recopilando informes de la actividad del website y prestando otros servicios relacionados con la actividad del website y el uso de Internet. Google podrá transmitir dicha información a terceros cuando así se lo requiera la legislación, o cuando dichos terceros procesen la información por cuenta de Google. Google no asociará su dirección IP con ningún otro dato del que disponga Google.

Puede Usted rechazar el tratamiento de los datos o la información rechazando el uso de cookies mediante la selección de la configuración apropiada de su navegador, sin embargo, debe Usted saber que si lo hace puede ser que no pueda usar la plena funcionabilidad de este website. Al utilizar este website Usted consiente el tratamiento de información acerca de Usted por Google en la forma y para los fines arriba indicados.

  

9. DATOS PERSONALES

La Política de privacidad de la ENTIDADqueda determinada por lo establecido en el documento POLÍTICA DE PRIVACIDAD. 

 

10. DURACIÓN Y MODIFICACIÓN 

LA ENTIDAD podrá modificar los términos y condiciones aquí estipulados, total o parcialmente, publicando cualquier cambio en la misma forma en que aparece este Aviso Legal o a través de cualquier tipo de comunicación dirigida a los Usuarios. 

La vigencia temporal de este Aviso Legal coincide, por lo tanto, con el tiempo de su exposición, hasta que sean modificadas total o parcialmente, momento en el cual pasará a tener vigencia el Aviso Legal modificado. 

Con independencia de lo dispuesto en las condiciones particulares, LA ENTIDAD podrá dar por terminado, suspender o interrumpir, en cualquier momento sin necesidad de preaviso, el acceso a los contenidos de la página, sin posibilidad por parte del Usuario de exigir indemnización alguna. Tras dicha extinción, seguirán vigentes las prohibiciones de uso de los contenidos expuestos anteriormente en el presente Aviso Legal. 

El procedimiento de baja será enviando un email con Asunto “Baja del Portal” a la dirección soporte.proveedores.ferroser@ferrovial.es.

 

11. GENERALIDADES

Los encabezamientos de las distintas cláusulas son sólo informativos, y no afectarán, calificarán o ampliarán la interpretación del Aviso Legal. 

En caso de existir discrepancia entre lo establecido en este Aviso Legal y las condiciones particulares de cada servicio específico, prevalecerá lo dispuesto en éstas últimas. 

En el caso de que cualquier disposición o disposiciones de este Aviso Legal fuera(n) considerada(s) nula(s) o inaplicable(s), en su totalidad o en parte, por cualquier Juzgado, Tribunal u órgano administrativo competente, dicha nulidad o inaplicación no afectará a las otras disposiciones del Aviso Legal. 

El no ejercicio o ejecución por parte de LA ENTIDAD de cualquier derecho o disposición contenida en este Aviso Legal no constituirá una renuncia al mismo, salvo reconocimiento y acuerdo por escrito por su parte. 

 

12. JURISDICCIÓN

Las relaciones establecidas entre LA ENTIDAD y el Usuario se regirán por lo dispuesto en la normativa vigente acerca de la legislación aplicable y la jurisdicción competente. No obstante, para los casos en los que la normativa prevea la posibilidad a las partes de someterse a un fuero, LA ENTIDAD y el Usuario, con renuncia expresa a cualquier otro fuero que pudiera corresponderles, someterán cualesquiera controversias y/o litigios al conocimiento de los Juzgados y Tribunales de la ciudad de Madrid Capital.

 

POLITICA DE PRIVACIDAD

 

La presente POLÍTICA DE PRIVACIDAD es parte integrante del AVISO LEGAL de la Web www.ferrovialservicios.es.

En cumplimiento de lo dispuesto en la Ley Orgánica 15/1999 de 13 de Diciembre, de Protección de Datos de Carácter Personal, Ferrovial Servicios, S.A, Ferroser Servicios Auxiliares, S.A. y Ferroser Infraestructuras, S.A. (en adelante,LA ENTIDAD) le comunica y usted consiente expresamente que:

 

1. Los datos personales facilitados por Usted a través del presente Sitio Web, se incorporarán a un fichero de titularidad de LA ENTIDAD, debidamente inscrito en el Registro General de Protección de Datos. La finalidad de dicho fichero es la gestión de los Usuarios del Sitio Web, la prestación de los servicios solicitados y, en general, la gestión, desarrollo y cumplimiento de la relación establecida entre LA ENTIDAD y quienes aporten sus datos personales a través del Sitio Web.

Por último le informamos que los datos podrán ser cedidos a la Agencia Tributaria y demás administraciones públicas, para el cumplimiento de obligaciones fiscales, así como a entidades financieras para la gestión de cobros y pagos.

2. Usted garantiza que los datos aportados son verdaderos, exactos, completos y actualizados, siendo responsable de cualquier daño o perjuicio, directo o indirecto, que pudiera ocasionarse como consecuencia del incumplimiento de tal obligación. En el caso de que los datos aportados pertenecieran a un tercero, Usted garantiza que ha informado a dicho tercero de los aspectos contenidos en este documento y obtenido su autorización para facilitar sus datos a LA ENTIDAD para los fines señalados.

3. En el caso de los datos personales recabados a través del formulario habilitado al efecto en el Sitio Web, será necesario que Usted aporte, al menos, aquellos marcados con un asterisco, ya que, si no se suministraran estos datos considerados necesarios, LA ENTIDAD no podrá aceptar y gestionar el Servicio Web o la consulta formulada.

4. Para ejercitar los derechos de acceso, rectificación, cancelación y oposición sobre sus datos, usted puede dirigirse por escrito a la siguiente dirección soporte.proveedores.ferroser@ferrovial.es.

5. En respuesta a la preocupación de LA ENTIDAD por garantizar la seguridad y confidencialidad de sus datos, se han adoptado los niveles de seguridad requeridos de protección de los datos personales y se han instalado los medios técnicos a su alcance para evitar la pérdida, mal uso, alteración, acceso no autorizado y robo de los datos personales facilitados a través del Sitio Web.

6. De conformidad con lo establecido en la Ley 34/2002 de 11 de julio, de Servicios de la Sociedad de la Información y del Comercio Electrónico, en el supuesto de que usted no desee recibir comunicaciones comerciales electrónica en el futuro, por parte de LA ENTIDAD, podrá manifestar tal deseo enviando un mail a la siguiente dirección de correo electrónico: soporte.proveedores.ferroser@ferrovial.es.

-------------

DECLARO RESPONSABLEMENTE que: 

1.- La empresa que represento dispone de la documentación probatoria del cumplimiento de los requisitos y de otras acreditaciones relacionadas, que presentará inmediatamente a Ferrovial Servicios cuando ésta la pueda requerir para su control o inspección.

2.- La empresa que represento dispone de los profesionales capacitados para el ejercicio de las especialidades, categorías y modalidades declaradas, con carné profesional, en su caso, o con los conocimientos teórico-prácticos requeridos en los correspondientes reglamentos, normas reguladoras o instrucciones técnicas, cuyo listado presentará inmediatamente a Ferrovial Servicios cuando ésta la pueda requerir para su control o inspección, junto con la documentación probatoria de la capacitación de los profesionales.

3.- Los datos y manifestaciones que figuran en este documento son ciertos y que la empresa es conocedora de que, en caso de resultar adjudicataria:

• Se compromete a mantener el cumplimiento de los requisitos exigidos durante la vigencia de la actividad, así como a ejercer su actividad cumpliendo con las normas y requisitos que se establezcan en los correspondientes reglamentos o normas reguladoras y, en su caso, en las respectivas instrucciones técnicas y ordenes de desarrollo, así como cumpliendo con las disposiciones establecidas por la Comunidad Autónoma donde realice sus actuaciones.
• Deberá remitir la documentación solicitada por Ferrovial Servicios de forma previa a la firma del contrato. En caso de no ser así, se establecerá como plazo máximo para remitir la documentación la fecha de vencimiento de cobro de la primera factura. Superado este plazo, se procederá a la resolución del contrato y no se abonarán los servicios prestados.
• Deberá mantener actualizada la documentación cargada en la Plataforma de Compras.
• Entregará en el centro de trabajo la siguiente documentación de todos los productos suministrados o empleados durante la prestación del servicio: 
	o Ficha Técnica de producto.
	o Ficha de Datos de Seguridad (Para los Productos de carácter peligroso)
	o Certificado de Marcado CE. (Si aplica).
	o Certificado de Producto (Marca N o similar) (Si aplica)

• Entregará en el centro de trabajo la siguiente documentación de los equipos empleados durante la prestación del servicio (si aplica): 
	o Certificados de Calibración de equipos de medida. 

• Cualquier hecho que suponga la modificación de alguno de los datos de carácter esencial incluidos en esta declaración, así como el cese de las actividades, deberá ser comunicado mediante una nueva declaración responsable a Ferrovial Servicios, en el plazo de un mes.

• La inexactitud, falsedad u omisión de los datos y manifestaciones de carácter esencial, faculta a Ferrovial Servicios para resolver el contrato. 


4.- La empresa que represento se compromete a cumplir en el desarrollo de sus actividades con los 10 Principios de Responsabilidad Social Corporativa y Desarrollo sostenible, recogidos en el Pacto Mundial (www.pactomundial.org). 

5.- La empresa que represento no ha sido sancionada por infracción grave en materia profesional o de integración laboral de minusválidos o muy grave en materia social o medioambiental.

6.- Que la empresa que represento no utiliza mano de obra infantil.

7.-Que la empresa a la que represento cumple lo recogido en los Convenios Fundamentales de la OIT (Organización Internacional del Trabajo) a lo largo de la cadena de producción y suministro, ya sea de forma directa o mediante subcontratación.

Y, para que así conste a los efectos de la homologación para el ejercicio de la actividad en las especialidades, categorías y modalidades clasificados en la Plataforma de Compras de Ferrovial Servicios, el declarante acepta esta Declaración Responsable. 

-------------------------
NORMAS CMA PARA PROVEEDORES

En FERROVIAL SERVICIOS estamos comprometidos con la prevención de la contaminación y el cumplimiento de la legislación ambiental aplicable a las actividades que realizamos y, además, queremos trabajar con Empresas que también lo estén. 

Por ello, la empresa que suscribe se compromete a cumplir las siguientes Normas de Comportamiento Medioambiental:


Generación de Residuos (Inertes, No peligrosos y Peligrosos)
• Reutilizar los residuos, siempre que sea posible. 
• Recoger selectivamente los residuos inertes (escombros, arenas y materiales cerámicos), no peligrosos (maderas, envases y embalajes, ropas y EPI desechados, etc.) y los residuos peligrosos (aceites usados, envases de materias primas peligrosas, trapos contaminados, disolventes, pinturas, baterías, etc.).
• Si procede, acopiar los residuos de forma inmediata, durante o tras la finalización de la actividad que los ha generado, en los contenedores y lugares que señale FERROVIAL SERVICIOS.  No se justificará, bajo ningún concepto, el acopio de residuos no peligrosos en las instalaciones fuera de sus contenedores. 
• Si procede, en caso de que el proveedor/subcontrata tenga que retirar los residuos generados en el centro, éstos deben ser entregados a gestores autorizados, evitando expresamente su abandono o vertido.  
• Respecto a los RAEE´s (Residuos eléctricos y electrónicos): En toda operación en la que se manipulen, sustituyan y/o almacenen estos elementos, se ha de evitar, en todo momento, el escape de los gases contenidos en su interior por rotura del tubo fluorescente o la lámpara que lo contiene. Se introducirá cada fluorescente usado en su envase de cartón. El almacenamiento de estos tubos o lámparas se realizará en contenedores y lugares que garanticen la integridad física de los mismos, informando cuando proceda, de la peligrosidad de los mismos. La gestión externa de los mismos se hará a través del proveedor que los suministró / a través de un gestor externo autorizado o bien mediante un SIG (ej. AMBILAMP).

Vertidos 
• Recoger y verter las aguas residuales (tanto sanitarias como industriales) al colector de saneamiento correspondiente, cumpliendo los límites de vertido autorizados en cada caso y evitando que se produzca cualquier vertido contaminante sobre el suelo o el cauce público.

Emisiones a la atmósfera
Gases de Combustión:
• Emplear vehículos, equipos y maquinaria con el preceptivo marcado CE y mantenerlos en buen estado de funcionamiento y limpieza, así como estar al día en las ITV reglamentarias.  
• Realizar un mantenimiento periódico completo de la maquinaria empleada en la prestación del servicio. 
Toda la maquinaria propiedad de Ferroser deberá ir provista del marcado CE. Esto garantiza que la maquinaria cumple unas determinadas exigencias en cuanto a posibles riesgos derivados de las emisiones de gases.

Partículas:
• Se seguirán las directrices establecidas por FERROVIAL SERVICIOS en cuanto a las posibles medidas a tomar para reducir el impacto de las partículas en el resto de la instalación y sobre las personas. Entre esas posibles medidas puede incluirse la obligación de realizar el cerramiento de la zona con una lona o manta o el riego del suelo para humedecerlo en las obras que se lleven a cabo en el exterior.
• Si durante la actividad, se detectara la presencia de amianto se paralizará de forma inmediata la actividad y se pondrá en conocimiento del Responsable del Contrato, quién pondrá en marcha los protocolos de actuación que rigen estos casos.

Ruido y Vibraciones:
• Durante las operaciones de mantenimiento puede generarse ruido y vibraciones en el exterior de las instalaciones. En tal caso se respetarán las franjas horarias y seguirán las directrices que marque FERROVIAL SERVICIOS y la legislación municipal, para minimizar el posible impacto que se pueda producir.

Gases Refrigerantes: (Asociado con las cargas, descargas y vaciado de los equipos que componen las instalaciones de Climatización, siempre y cuando vayan provistos de este tipo de gases)
• Contemplar la posibilidad de trabajar y manipular el producto en fase líquida.
• Utilizar todas las herramientas y accesorios necesarios que garanticen la ejecución de dichas actividades de la forma más segura.
• Entregar a un gestor autorizado los gases extraidos.
• Evitar siempre y atajar de forma inmediata los escapes libres de dichos gases a la atmósfera, incluso llegando a la parada de la instalación si la avería no puede ser solucionada rápidamente, siempre informando a FERROVIAL SERVICIOS previamente.

Impacto visual
• Establecer los medios necesarios, en cada caso, para minimizar las molestias y el impacto visual por levantamiento de polvo, ensuciamiento de viales, dispersión de materiales ligeros, etc. asociados a sus actividades.

Productos empleados 
• Disponer y tener en cuenta la información de las Fichas de Datos de Seguridad de las materias primas utilizadas.
• Fomentar la adquisición y utilización de productos y servicios que tengan características de Compra Verde (es decir, que dispongan de etiqueta ecológica, o que estén fabricados con material reciclado, o que sean reciclables o biodegradables, etc.)


En el caso de que FERROVIAL SERVICIOS se lo solicite, facilitar las evidencias documentales del cumplimiento de estos compromisos adquiridos.

Además, cuando trabaje en las instalaciones de FERROVIAL SERVICIOS o utilice los recursos materiales de FERROVIAL SERVICIOS, se compromete a:
a)Comunicar estos compromisos al personal que vaya a realizar el trabajo, y asegurarse de su cumplimiento.
b)Tener en cuenta las indicaciones complementarias que, en su caso, le haga llegar el personal de FERROVIAL SERVICIOS para mejorar o concretar el cumplimiento de dichos compromisos.
c)Comunicar a FERROVIAL SERVICIOS cualquier situación de riesgo ambiental que pudiera aparecer durante el trabajo.
d)Responsabilizarse de subsanar y reparar cualquier posible daño ambiental que se origine como consecuencia de los trabajos realizados para FERROVIAL SERVICIOS.
</textarea>
          </font> </td>
        </tr>
      </table>
      </font>
        <p class="textos"> <b>&iquest;Acepta el presente <a href="spa/aviso%20legal.htm" target="_blank">aviso legal y política de privacidad</a>, as&iacute; como las <a href="spa/docs/FER_Condiciones_generales_de_compra_y_contratacion.pdf" target="_blank">Condiciones Generales de Compra</a>, la <a href="spa/docs/FER_Declaracion Responsable.pdf" target="_blank">Declaración responsable</a>, la <a href="spa/docs/FER_Politica_de_calidad_medioambiente_y_energia.pdf" target="_blank">Política de calidad, medioambiente y energía</a> y las <a href="spa/docs/FER_Normas CMA para Proveedores.pdf" target="_blank">Normas CMA para proveedores</a>?<br>
                <br>
        </b> Si pulsa &quot;No Acepto&quot;, su empresa no podr&aacute; ser registrada como proveedor autorizado de Ferroser.        
        <p class="textos">          Para continuar el proceso de alta debe pulsar &quot;Acepto&quot;. <br>
          Si quiere imprimir el contrato puede hacerlo pulsando el bot&oacute;n &quot;Imprimir&quot;. <br>
          <br>
          
        <div align="right">
          <table width="39%" border="0" align="left">
            <tr>
              <td width="41%"><p align="center">
                  <input type="submit" onclick="return alta()" value="Acepto" id=submit1 name=submit1>
              </p></td>
              <td width="38%"><input type="button" onclick="no_alta()" value="No acepto" id=button1 name=button1>
              </td>
              <td width="21%"><input type="button" onclick="imprimir()" value="Imprimir" id=button2 name=button2>
              </td>
            </tr>
          </table>
        </div><br><br>
        <p align="left" class="textos">        
        <hr width="100%" size="1" color="#0051BA">
    </td>
  </tr>
</table>

	<input type="hidden" name="ACEPTADO">
	
</form>

</body>
<!--#include file="../../common/fsal_2.asp"-->
</html>
