﻿<%@ Language=VBScript %>
<!--#include file="../common/XSS.asp"-->
<%
''' <summary>
''' Pantalla inicial de la personalización, es la q llama a la pantalla de login
''' </summary>
''' <remarks>Llamada desde: custom/prisa		; Tiempo máximo: 0,1</remarks>

sQuery = Request.QueryString

if sQuery<>"" then
	sQuery = "?" & sQuery
else
	sQuery = "?Idioma=SPA"
end if

if not(application("NOMPORTAL") = "prisa") then
	Response.redirect Application ("RUTASEGURA") & "script/login/" & Application ("NOMPORTAL") & "/default.asp" & sQuery
	Response.End 
end if


%>
 

<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><%=Application ("TITLE")%></title>

<META HTTP-EQUIV="Content-Type" CONTENT="text/html">
</head>
 

<frameset rows="*,0" border=0 frameborder="0" framespacing="0">	
	<frame name="default_main" src="<%=Application("RUTASEGURA")%>script/login/<%=Application("NOMPORTAL")%>/login.asp<%=sQuery%>" scrolling="auto">
    <frame name="default_header" scrolling="no" noresize target="default_main" src="../script/blank.htm">
	<noframes>
		<body topmargin="0" leftmargin="0" rightMargin=0 bottomMargin=0>
		<p>This page uses frames, but your browser doesn't support them.</p>
		</body>
	</noframes>
</frameset>

</html>
