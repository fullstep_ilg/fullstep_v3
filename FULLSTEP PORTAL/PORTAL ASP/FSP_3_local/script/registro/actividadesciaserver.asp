﻿<%@ Language=VBScript %>

<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/fsal_1.asp"-->

<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader "Pragma", "no-cache" %>
<% Response.Expires = -1 %> 

<html>
<head>
 <title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script language="JavaScript" src="../common/ajax.js"></script>
<script src="../common/formatos.js"></script>
<!--#include file="../common/fsal_3.asp"-->
<script>

var p
//p=window.parent.frames["fraRegistroClient"].document.frames["fraActividad"]

p=window.parent.frames["fraRegistroClient"].frames["fraActividad"]
</script>
<%

''' <summary>
''' Segunda pantalla del registro de nuevos proveedores: actividadescia.asp. Esta carga los datos de 
'''	actividades de la empresa de nivel mayor de 1. La pantalla actividades.asp indica q actividad debes mostrar.
''' </summary>
''' <remarks>Llamada desde: registro/actividadescia.asp; Tiempo máximo: 0,1</remarks>

dim Idioma
dim ActN4
dim ActN3
dim ActN2
dim ActN1

Idioma=Request("Idioma")
ActN4=Request("ACTN4")
ActN3=Request("ActN3")
ActN2=Request("ActN2")
ActN1=Request("ActN1")

function obtenerActN2(Idioma,ActN1)

set oRaiz = createobject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
	
if (oRaiz.Conectar(Application("INSTANCIA"))) = 1 then
	set oraiz = nothing
	Response.End 
end if

set paramgen = oraiz.devolverParametrosGenerales

set oAct1 = oraiz.generar_CActividadNivel1
oact1.Id = cint(ActN1)

oAct1.Cargartodaslasactividadesnivel2 Idioma
	
%>
<script>
//''' <summary>
//''' Carga las actividades de nivel 2 de la actividad de nivel 1 de la funcion obtenerActN2
//''' </summary>
//''' <remarks>Llamada desde: init() ; Tiempo máximo: 0,2</remarks>
function obtenerAct()
{
var esRama
esRama=false
<%
for each oAct2 in oAct1.ActividadesNivel2
%>	

	esRama=true
	p.crearRama('<%=ActN1%>','<%=ActN1%>_<%=oAct2.id%>','<%=JSText(oAct2.cod)%>','<%=JSText(oAct2.den)%>',false,<%if oAct2.ExistenActividadesHijas then%>false<%else%>true<%end if%>,null,'green',<%if paramgen.NivelMinAct > 2 then%>false<%else%>true<%end if%>)
	

<%
next
%>
return (esRama)
}
</script>
<%
set oact1 = nothing 
set paramgen =nothing
set oraiz=nothing
	
end function


function obtenerActN3(Idioma,ActN1,ActN2)

	
set oRaiz = createobject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
	
if (oRaiz.Conectar(Application("INSTANCIA"))) = 1 then
	set oraiz = nothing
	Response.End 
end if
set paramgen = oRaiz.devolverParametrosGenerales
	
set oAct2 = oraiz.generar_CActividadNivel2
oact2.ACN1 = cint(ActN1)
oact2.Id = cint(ActN2)
	
oAct2.Cargartodaslasactividadesnivel3 Idioma
	

%>
<script>
//''' <summary>
//''' Carga las actividades de nivel 3 de la actividad de nivel 2 de la funcion obtenerActN3
//''' </summary>
//''' <remarks>Llamada desde: init() ; Tiempo máximo: 0,2</remarks>
function obtenerAct()
{
var esRama
esRama=false
<%
for each oAct3 in oAct2.ActividadesNivel3
%>	
	esRama=true
	p.crearRama('<%=ActN1%>_<%=ActN2%>','<%=ActN1%>_<%=ActN2%>_<%=oAct3.id%>','<%=JSText(oAct3.cod)%>','<%=JSText(oAct3.den)%>',false,<%if oAct3.ExistenActividadesHijas then%>false<%else%>true<%end if%>,null,'blue',<%if paramgen.NivelMinAct > 3 then%>false<%else%>true<%end if%>)

<%
next
%>
return (esRama)
}
</script>
<%


set oact2 = nothing 
set paramgen =nothing
set oraiz=nothing

end function


function obtenerActN4(Idioma,ActN1,ActN2,ActN3)

set oRaiz = createobject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
	
if (oRaiz.Conectar(Application("INSTANCIA"))) = 1 then
	set oraiz = nothing
	Response.End 
end if
set paramgen = oraiz.devolverParametrosGenerales

	
set oAct3 = oraiz.generar_CActividadNivel3
oact3.ACN1 = cint(ActN1)
oact3.ACN2 = cint(ActN2)
oact3.Id = cint(ActN3)
	
oAct3.Cargartodaslasactividadesnivel4 Idioma


%>
<script>
//''' <summary>
//''' Carga las actividades de nivel 4 de la actividad de nivel 3 de la funcion obtenerActN4
//''' </summary>
//''' <remarks>Llamada desde: init() ; Tiempo máximo: 0,2</remarks>
function obtenerAct()
{
var esRama
esRama=false



<%
for each oAct4 in oAct3.ActividadesNivel4
%>	
	esRama=true
	p.crearRama('<%=ActN1%>_<%=ActN2%>_<%=ActN3%>','<%=ActN1%>_<%=ActN2%>_<%=ActN3%>_<%=oAct4.id%>','<%=JSText(oAct4.cod)%>','<%=JSText(oAct4.den)%>',false,<%if oAct4.ExistenActividadesHijas then%>false<%else%>true<%end if%>,null,'brown',<%if paramgen.NivelMinAct > 4 then%>false<%else%>true<%end if%>)

<%
next
%>
return (esRama)
}
</script>
<%


set oact3 = nothing 
set paramgen =nothing
set oraiz=nothing
	


end function


function obtenerActN5(Idioma,ActN1,ActN2,ActN3,ActN4)

set oRaiz = createobject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
	
if (oRaiz.Conectar(Application("INSTANCIA"))) = 1 then
	set oraiz = nothing
	Response.End 
end if
set paramgen = oraiz.devolverParametrosGenerales

set oAct4 = oraiz.generar_CActividadNivel4
oact4.ACN1 = cint(ActN1)
oact4.ACN2 = cint(ActN2)
oact4.ACN3 = cint(ActN3)
oact4.Id = cint(ActN4)
	
oAct4.Cargartodaslasactividadesnivel5 Idioma
	


%>
<script>
function obtenerAct()
{
var esRama
esRama=false
<%
for each oAct5 in oAct4.ActividadesNivel5
%>	
	esRama=true
	p.crearRama('<%=ActN1%>_<%=ActN2%>_<%=ActN3%>_<%=ActN4%>','<%=ActN1%>_<%=ActN2%>_<%=ActN3%>_<%=ActN4%>_<%=oAct5.id%>','<%=JSText(oAct5.cod)%>','<%=JSText(oAct5.den)%>',false,true,null,'brown',<%if paramgen.NivelMinAct > 5 then%>false<%else%>true<%end if%>)
<%
next
%>
return (esRama)
}
</script>
<%

	
set oact4 = nothing 
set paramgen =nothing
set oraiz=nothing

end function



dim codPadre

if ActN4<>"" then
	obtenerActN5 Idioma,ActN1,ActN2,ActN3,ActN4
	codPadre=ActN1 & "_" & ActN2 & "_" & ActN3 & "_" & ActN4 
	
elseif ActN3<>"" then
	obtenerActN4 Idioma,ActN1,ActN2,ActN3
	codPadre=ActN1 & "_" & ActN2 & "_" & ActN3  
elseif ActN2<>"" then
	obtenerActN3 Idioma,ActN1,ActN2
	codPadre=ActN1 & "_" & ActN2 
else
	obtenerActN2 Idioma,ActN1
	codPadre=ActN1 
end if

	
%>	

<script>


function init() 
    {
    if (<%=application("FSAL")%> == 1 )
    {
	    Ajax_FSALActualizar3(); //registro de acceso
	}
	var esRama
	esRama=obtenerAct()
	p.yaEsta('<%=codPadre%>',esRama)
	
	}


</script>
</head>
<body onload=init()>
<p>&nbsp;</p>
</body>
<!--#include file="../common/fsal_2.asp"-->
</html>
<%
set oraiz=nothing
set paramgen =nothing
%>