﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ Language=VBScript %>
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/fsal_1.asp"-->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<script src="../js/arbol.js"></script>
<script src="../common/ajax.js"></script>
<script src="../common/formatos.js"></script>
<!--#include file="../common/fsal_3.asp"-->
</head>
<style>
A {font-family:Tahoma;font-size:12px;text-decoration:none}
TD {font-family:Tahoma;font-size:12px;}
.negro {font-family:Tahoma;font-size:12px;color:black;text-decoration:none;}
.azul {font-family:Tahoma;font-size:12px;color:blue;text-decoration:none;}
#divArbol {position:absolute;top:0;left:0;visibility:visible;z-index:1;}
#divEspera {position:absolute;top:0;left:0;width:400;clip:rect(0 400 20 0);visibility:hidden;z-index:200;}
</style>

<%
''' <summary>
''' Segunda pantalla del registro de nuevos proveedores: actividadescia.asp. Esta carga los datos de 
'''	actividades de la empresa de nivel 1.
''' </summary>
''' <remarks>Llamada desde: registro/actividadescia.asp; Tiempo máximo: 0,1</remarks>

set oraiz = CreateObject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
	
if (oRaiz.Conectar(Application("INSTANCIA"))) = 1 then
	set oraiz = nothing
	Response.Redirect Application ("RUTASEGURA") & "script/imposibleconectar.asp?Idioma=" & Request.QueryString ("Idioma")
			
	Response.End 
end if

Idioma = Request("Idioma")
Idioma = trim(Idioma)

Dim Den

den=devolverTextos(Idioma,38)

txtEsperaCarga=den(11)
txtEspera=den(12)

set paramgen = oraiz.devolverParametrosGenerales

set ador=nothing

%>


<script>

var p
p=window.parent

rObjeto=window.parent.parent

var recienCargada=true
var numMaxActividades=<%=paramgen.MaxActividades%>

function obtener(codigo)
{
var codigos
var l
var hayHojas
var ret
if (codigo=="Root")
	{
	hayHojas=obtenerNivel1()
	return(hayHojas)
	}
	
codigos= codigo.split("_")

l=codigos.length
var imgcolor

recienCargada=false
ret=""
var h
switch(l)
	{
	case 1:
		h=window.open("<%=Application("RUTASEGURA")%>script/registro/actividadesciaserver.asp?Idioma=<%=Idioma%>&ACTN1=" + codigos[0],"fraRegistroServer") 
		break;
	case 2:
		window.open("<%=Application("RUTASEGURA")%>script/registro/actividadesciaserver.asp?Idioma=<%=Idioma%>&ACTN1=" + codigos[0] + "&ACTN2=" + codigos[1],"fraRegistroServer") 
		break;
	case 3:
		window.open("<%=Application("RUTASEGURA")%>script/registro/actividadesciaserver.asp?Idioma=<%=Idioma%>&ACTN1=" + codigos[0] + "&ACTN2=" + codigos[1] + "&ACTN3=" + codigos[2],"fraRegistroServer")
		break;
	case 4:
		window.open("<%=Application("RUTASEGURA")%>script/registro/actividadesciaserver.asp?Idioma=<%=Idioma%>&ACTN1=" + codigos[0] + "&ACTN2=" + codigos[1] + "&ACTN3=" + codigos[2] + "&ACTN4=" + codigos[3],"fraRegistroServer")
		break;
	default:
		return false
		break;
	}
document.getElementById("divEspera").style.top=document.body.scrollTop + 50
document.getElementById("divEspera").style.left=50
document.getElementById("divEspera").style.visibility="visible"
document.getElementById("divArbol").style.visibility="hidden"

return true
}


function crearRama(padre,hijo,cod,desc,nada,eshoja,nulo,color,chequeable)
{
var rPadre
rPadre=eval("rama" + padre)
if (!rPadre.hojasCargadas)
	{
	eval("rama" + hijo + "=new Rama ('" + hijo + "',cod,desc,false,eshoja,null,color,chequeable)")
	eval("rama" + padre + ".add(rama" + hijo +")")
	}
}

function yaEsta(padre,esRama)
{
var rPadre
rPadre=eval("rama" + padre)
document.getElementById("divEspera").style.visibility="hidden"
document.getElementById("divArbol").style.visibility="visible"
if (!rPadre.hojasCargadas)
	{
	eval("rama" + padre + ".asyncWrite(esRama)")
	}
}
/*''' <summary>
''' Crear las ramas de nivel 1 para el arbol de actividades.
''' </summary>
''' <remarks>Llamada desde: actividades.asp/obtener ; Tiempo máximo: 0</remarks>*/
function obtenerNivel1()
{
<%
dim i
i=0

set oActsN1 = oraiz.Generar_CActividadesNivel1
if Application("PYME") <> "" then
	oActsN1.CargarTodasLasActividades Idioma,"","",0,false,false,Application("PYME")
else
	oActsN1.CargarTodasLasActividades Idioma
end if

for each oAct1 in oActsN1
%>
rama<%=oAct1.id%>=new Rama ('<%=oAct1.id%>','<%=JSText(oAct1.Cod)%>','<%=JSText(oAct1.Den)%>',false,<%if oAct1.ExistenActividadesHijas then%>false<%else%>true<%end if%>,null,'DarkGoldenrod',<%if paramgen.NivelMinAct = 1 then%>true<%else%>false<%end if%>,true,true)

ramaRoot.add(rama<%=oAct1.Id%>)
			
<%
i=i+1
next
%>

ramaRoot.hojasCargadas=true
<%if i=0 then%>return false<%else%>return true<%end if%>
}

function cargarArbol(r)
{
var i
eval("rama" + r.id + " = copiarRama(r)")
if (r.parent!=null)
	{
	eval("rama" + r.parent.id + ".add(rama" + r.id + ")")
	}
eval("rama" + r.id + ".checked = r.checked")

if (r.hojasCargadas==true && r.hojas.length>0)
	{
	for (i=0;i<r.hojas.length;i++)
		{
		cargarArbol(r.hojas[i])
		}
	}
}

function copiarRama(r)
{
var tmp
tmp = new Rama ()
tmp.id = r.id
tmp.cod = r.cod
tmp.text = r.text
tmp.hojas = new Array()
tmp.hojasCargadas = r.hojasCargadas
tmp.parent=null
tmp.expanded = r.expanded
tmp.imagen = r.imagen;

tmp.color = r.color;
tmp.chequeable=r.chequeable
tmp.checked=r.checked
tmp.multiple=r.multiple
tmp.async = r.async
tmp.carpeta = r.carpeta

return (tmp)

}

function guardarArbol()
{
rObjeto.arbol = ramaRoot

}

function init()
{
if (<%=application("FSAL")%> == 1 )
{
Ajax_FSALActualizar3(); //registro de acceso
}
if (rObjeto.arbol==null)
{
ramaRoot=new Rama ('Root','Root','<%=den(14)%>',false,false,null,"black",false,true,false)
}
else
{
cargarArbol(rObjeto.arbol)
}
ramaRoot.expanded = false
ramaRoot.write(null)

ramaRoot.expcont()

}


function obtenerActividadesSeleccionadas()
{
p.document.forms["frmActividades"].txtNumActs.value=0
p.document.getElementById("divActividades").innerHTML=""
recorrerArbol(ramaRoot)
}

function recorrerArbol(rama)
{
var i
var Acts
if (rama.checked)
	{
	if (!rama.parent.checked)
		{
		p.document.forms["frmActividades"].txtNumActs.value = parseInt(p.document.forms["frmActividades"].txtNumActs.value) + 1
		Acts=p.document.forms["frmActividades"].txtNumActs.value
	
		str="<input type=hidden name=txtAct" + Acts + " value='" + rama.id + "'>"
		p.document.getElementById("divActividades").innerHTML +=str
		}
	}
for (i=0;i<rama.hojas.length;i++)
	{
	recorrerArbol(rama.hojas[i])
	}
}

var antSeleccionada
antSeleccionada=null

function desselParent(rama)
{
var chkDesel

if (rama.id!='Root' && rama.chequeable)
	{
	rama.checked=false
	expanded = dessel(rama)
	if (expanded)
		{
		chkDesel = eval("document.forms['frmArbol'].chkRama" + rama.id )
		chkDesel.checked=false
		}
	desselParent(rama.parent)
	}
return
}

function selHojas(rama,valor)
{
var i
var chkSel
var expanded

expanded = dessel(rama)
if (expanded)
	{
	chkSel = eval("document.forms['frmArbol'].chkRama" + rama.id )
	chkSel.checked=valor
	}
rama.checked=valor
for (i=0;i<rama.hojas.length;i++)
	{
	if (rama.hojas[i].chequeable)
		{
		selHojas(rama.hojas[i],valor)
		}
	}
}


function seleccionar(codigo)
{
obtenerActividadesSeleccionadas()
var rama
rama=eval("rama"+ codigo)
    debugger;
if ((numMaxActividades == 0) || (numMaxActividades > p.document.forms["frmActividades"].txtNumActs.value))
{
    selHojas(rama,true) 
    return true
}
else
{
    alert('<%=JSAlertText(Replace(den(18),"#Num",paramgen.MaxActividades))%>')
    desseleccionar(codigo)
    return false
}    
}


function desseleccionar(codigo)
{
var rama
rama=eval("rama"+ codigo)
desselParent(rama)
selHojas(rama,false)
}


</script>


<body onload="init()" topmargin="1" leftmargin="1">
<form name="frmArbol">
<div name="divArbol" id="divArbol">
<font size="1" face="tahoma" color="darkblue"><%=txtEspera%></font>
</div>

<div name="divEspera" id="divEspera">
<font size="1.5" face="tahoma" color="darkblue"><%=txtEsperaCarga%></font>
</div>
</form>
</body>
<!--#include file="../common/fsal_2.asp"-->
</html>
<%
set oraiz=nothing
set paramgen =nothing	
%>
