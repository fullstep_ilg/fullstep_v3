﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ Language=VBScript %>
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader "Pragma", "no-cache" %>
<% Response.Expires = -1 %> 
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/fsal_7.asp"-->

<%
''' <summary>
''' Segunda pantalla del registro de nuevos proveedores. Le indicas los datos de actividades de la empresa.
''' </summary>
''' <remarks>Llamada desde: registro/actividades.asp registro/compania.asp; Tiempo máximo: 0,1</remarks>
Response.Expires=0 
Idioma=Request("Idioma")
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script src="../common/formatos.js"></script>
<script language="JavaScript" src="../common/ajax.js"></script>
<style>
.boton {font-family:tahoma;font-weight:bold;font-size:10}
#divActividades {position:absolute;visibility:hidden;top:0;left:0;}
</style>
<!--#include file="../common/fsal_3.asp"-->
<!--#include file="../common/fsal_5.asp"-->
<%

Dim Den

den=devolverTextos(Idioma,38 )

lblResumen1 = (Den(2))
lblResumen2 = (Den(3))
lblResumen3 = (Den(4))
lblResumen4 = (Den(5))
lblResumen5 = (Den(6))
msgError  = (Den(7))
msgExplic = (Den(8))
lblSel = (Den(9))
msgExplic2 = (Den(15))
msgError2  = (Den(16))


set oraiz = CreateObject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
if (oRaiz.Conectar(Application("INSTANCIA"))) = 1 then
	set oraiz = nothing
	Response.Redirect Application ("RUTASEGURA") & "script/imposibleconectar.asp?Idioma=" & Idioma
	Response.End  
end if

if Request.Form("nif")<>"" then
	'Comprueba si la compania existe o no
	if Application("PYME") <> "" then
		codigo = Application("PYME") & Request.Form("codigo")
	else
		codigo = Request.Form("codigo")
	end if
	set TESError = oRaiz.CompaniaValida(codigo, Request.Form("nif"), Application("PYME"))
	if tesError.numError = 1 then 'Si existe el codigo de la compania, vuelve atras
		codigo = "errorExisteCompania"
		Response.Redirect Application ("RUTASEGURA") & "script/registro/compania.asp?Idioma=" & Idioma & "&codigo=" & codigo
		Response.End
	elseif tesError.numError = 2 then 'Si existe el NIF de la compania, vuelve atras
		codigo = "errorExisteNIFCompania"
		Response.Redirect Application ("RUTASEGURA") & "script/registro/compania.asp?Idioma=" & Idioma & "&codigo=" & codigo
		Response.End
	else
		codigo = "noError"
	end if
end if


'Llama a las validaciones de la mapper:
//	1.- Obtiene el nombre de la mapper
Dim  NomMapper
Dim oValidacionMapper
dim oMapper
dim sDireccion 
 
Set oValidacionMapper=oRaiz.generar_CValidacionesMapper
NomMapper=""
NomMapper= oValidacionMapper.ReadMapper()

iNumError=0
if  NomMapper <> "" then 

      On Error Resume Next
      set oMapper = nothing
      set oMapper = CreateObject (NomMapper & ".CValidacionesPortal")
      If Not oMapper is Nothing Then
         sDireccion=Request.Form("direccion")
         if oMapper.TratarValidacionPortal(sdireccion,iNumError)=false then 
            codigo= oValidacionMapper.DevolverTexto(Idioma,iNumError)
            Response.Redirect Application ("RUTASEGURA") & "script/registro/compania.asp?Idioma=" & Idioma & "&codigo=" & codigo
            Response.End
          end if 
       end if 
       
end if

set paramgen = oraiz.devolverParametrosGenerales
if paramGen.ActivarRegistroActiv then
	lblTitulo = (Den (17))
else
	lblTitulo = (Den (1))
end if	
%>

<script>
/*
''' <summary>
''' Vuelve a la primera pantalla de registro
''' </summary>   
''' <remarks>Llamada desde: cmdAtras.onclick ; Tiempo máximo: 0</remarks>*/
function init()
{
if (<%=Application("FSAL")%> == 1)
    {
        Ajax_FSALActualizar3(); //registro de acceso
    }
}

function atras()
{
window.frames["fraActividad"].guardarArbol()
location.href="<%=application("RUTASEGURA")%>script/registro/compania.asp?Idioma=<%=Idioma%>"
}


function guardar ()
{
if (<%=application("FSAL")%> == 1)
{
fechaIniUsu=devolverUTCFechaFSAL();  //se registra la fecha FICLIPREPAR
sIdRegistroUsu=devolverIdRegistroFSAL();
}
var i
var idAct
var Act
var j
window.frames["fraActividad"].obtenerActividadesSeleccionadas()
if (document.forms["frmActividades"].txtNumActs.value==0)
	{
	alert('<%=msgError & " " & paramgen.NivelMinAct & " " & msgError2%>')
	return false
	}
if ((<%=paramgen.MaxActividades%> > 0) && (document.forms["frmActividades"].txtNumActs.value > <%=paramgen.MaxActividades%>))
	{
	alert('<%=JSAlertText(Replace(den(18),"#Num",paramgen.MaxActividades))%>')
	return false
	}
window.frames["fraActividad"].guardarArbol()

p = window.parent
var f
f = document.forms["frmActividades"]
p.oActividades.numAct = f.txtNumActs.value
p.oActividades.Acts = new Array()
if (<%=application("FSAL")%> == 1)
{
f.fechaIniFSAL7.value = fechaIniUsu; //pasar fecha inicio
f.sIdRegistroFSAL7.value = sIdRegistroUsu; //pasar idregistro inicio
}
for (i=1;i<=f.txtNumActs.value;i++)
  {	 
	Act=f.elements["txtAct" + i].value	
	j=p.oActividades.Acts.length
	p.oActividades.Acts[i]=Act
  }
 
if (<%=application("FSAL")%> == 1)
{
Ajax_FSALRegistrar5(fechaIniUsu,"<%=sIDPagina%>".replace("actividadesCia.asp","usuario.asp"),sIdRegistroUsu); //FFCLIPREPAR al final de ‘guardar’  
}
return true
}

</script>
</head>


<body bgColor="#ffffff" LANGUAGE="javascript" topmargin=0 leftmargin=0 onload="init()">



<h1><%=lblTitulo%></h1>

<h3><%=lblResumen1%></h3>
<h3><%=lblResumen2%></h3>
<h3><%=lblResumen4%></h3>
<h4><%=lblSel%>&nbsp;(<%=msgExplic%>&nbsp;<%=paramgen.NivelMinAct%><%=msgExplic2%>)</h4>

<table class=principal border="0" height="300px" width="95%" cellPadding="1" cellSpacing="2">
	<tr>
		<td height="294px">
			<iframe name="fraActividad" id="fraActividad" src="actividades.asp?Idioma=<%=request("Idioma")%>" frameborder="1" width="100%" height="100%"></iframe>
		</td>
	</tr>
</table>

<form name="frmActividades" method="post" action="usuario.asp" target="fraRegistroClient">

	<table width=100%>
		<tr>
			<td align = left>
				<INPUT class=boton type=button onclick="atras()" name="cmdAtras" id="cmdAtras" value=" << <%=Den(13)%> " >
			</td>
			<td align = right>
				<INPUT class=boton type=submit onclick="return guardar()" name="cmdRegistrar" id="cmdRegistrar" value="<%=lblResumen5%> >>" >
			</td>
		</tr>
	</table>
	
	<input type="hidden" name="Idioma" value="<%=trim (Request("Idioma"))%>">
	<input type="hidden" name="txtNumActs" value="0">
	<%if Application("FSAL")=1 then%>   
    <input type="hidden" name="fechaIniFSAL7">
    <input type="hidden" name="sIdRegistroFSAL7">
    <%end if%>

<div name="divActividades" id="divActividades">

</div>
</form>
</body>
<%
set paramgen =nothing
set oraiz=nothing
%>
<!--#include file="../common/fsal_8.asp"-->
</html>
