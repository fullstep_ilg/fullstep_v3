﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ Language=VBScript %>
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader "Pragma", "no-cache" %>
<% Response.Expires = -1 %> 

<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/fsal_7.asp"-->

<%
''' <summary>
''' Tras rellenar todos los datos para el registro de un nuevo proveedor da opci�n a enviar, grabar
'''	en bbdd, � volver atras.
''' </summary>
''' <remarks>Llamada desde: registro/usuario.asp; Tiempo m�ximo: 0</remarks>

Idioma=Request("Idioma")
	
Dim Den

den=devolverTextos(Idioma,43)

set oraiz = CreateObject ("FSPServer_" & Application ("NUM_VERSION") &".CRaiz")
if (oRaiz.Conectar(Application("INSTANCIA"))) = 1 then
	set oraiz = nothing
	Response.Redirect Application ("RUTASEGURA") & "script/imposibleconectar.asp?Idioma=" & Idioma
	Response.End  
end if
set paramgen = oraiz.devolverParametrosGenerales
%>

<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script src="../common/formatos.js"></script>
<script language="JavaScript" src="../common/ajax.js"></script>
<!--#include file="../common/fsal_3.asp"-->
</HEAD>
<script>
var p
var f
function init()
{
if (<%=application("FSAL")%> == 1)
{
    Ajax_FSALActualizar3(); //registro de acceso
}
p=window.parent
f=document.forms["frmResumen"]
vdecimalfmt = ","
vthousanfmt = "."
vprecisionfmt = 2

f.txtCodCia.value=p.oCompania.cod
f.txtDen.value=p.oCompania.den
f.txtDir.value=p.oCompania.dir
f.txtCP.value=p.oCompania.cp
f.txtPob.value=p.oCompania.pob


f.lstPai.value=p.oCompania.idpai
f.lstProvi.value=p.oCompania.idprovi
f.lstMon.value=p.oCompania.idmon
f.txtNIF.value=p.oCompania.nif
f.txtVolFac.value=num2str(p.oCompania.volFact,vdecimalfmt,vthousanfmt,vprecisionfmt)
f.txtCliRef1.value=p.oCompania.cliRef1
f.txtCliRef2.value=p.oCompania.cliRef2
f.txtCliRef3.value=p.oCompania.cliRef3
f.txtCliRef4.value=p.oCompania.cliRef4
f.txtHom1.value=p.oCompania.hom1
f.txtHom2.value=p.oCompania.hom2
f.txtHom3.value=p.oCompania.hom3
f.txtURLCia.value=p.oCompania.url
f.txtComent.value=p.oCompania.coment

f.txtCodUsu.value = p.oUsuario.cod
f.txtpwd.value = p.oUsuario.pwd
f.txtape.value = p.oUsuario.ape
f.txtNifUsu.value = p.oUsuario.nif
f.txtnombre.value = p.oUsuario.nom
f.txtdep.value = p.oUsuario.dep
f.txtcargo.value = p.oUsuario.cargo
f.txttfno.value = p.oUsuario.tlfno1
f.txttfno2.value = p.oUsuario.tlfno2
f.txttfnomv.value = p.oUsuario.movil
f.txtfax.value = p.oUsuario.fax
f.txtemail.value = p.oUsuario.email
f.lstIdiUsu.value = p.oUsuario.codidi
f.optTipoEMail.value = p.oUsuario.TipoMail

<%if paramGen.ActivarRegistroActiv then%>
f.txtNumActs.value=p.oActividades.numAct

var i
var str
str=""
for (i=1;i<=f.txtNumActs.value;i++)
  {	 
	str +="<input type=hidden name=txtAct" + i + " value='" + p.oActividades.Acts[i] + "'>"		
  }
document.getElementById("divActividad").innerHTML +=str
<%end if%>
}

/*
''' <summary>
''' Vuelve a la primera pantalla de registro
''' </summary>   
''' <remarks>Llamada desde: cmdAtras.onclick ; Tiempo máximo: 0</remarks>*/
function atras()
{
location.href="<%=application("RUTASEGURA")%>script/registro/usuario.asp?Idioma=<%=request("Idioma")%>"

}
</script>
<BODY onload="init()">
<h1><%if paramGen.ActivarRegistroActiv then%><%=Den(31)%><%else%><%=Den(1)%><%end if%></h1>
<h3><%=Den(4)%></h3>
<h3><%=Den(2)%></h3>
<h3><%=Den(3)%></h3>

<%if application("NOMPORTAL") = "huf" then%>

<h1><%=den(7)%></h1>

<h3 style="line-height:150%"><%=den(8)%></h3>
<h3 style="line-height:150%"><%=den(9)%></h3>
<h3 style="line-height:150%"><%=den(10)%></h3>
<h3 style="line-height:150%"><%=den(11)%></h3>
<h3 style="line-height:150%"><%=den(12)%></h3>
<h3 style="line-height:150%"><%=den(13)%></h3>
<h3 style="line-height:150%"><%=den(14)%></h3>
<h3 style="line-height:150%"><%=den(15)%></h3>
<h3 style="line-height:150%"><%=den(16)%></h3>
<h3 style="line-height:150%"><%=den(17)%></h3>
<h3 style="line-height:150%"><%=den(18)%></h3>
<h3 style="line-height:150%"><%=den(19)%></h3>
<h3 style="line-height:150%"><%=den(20)%></h3>
<h3 style="line-height:150%"><%=den(21)%></h3>
<h3 style="line-height:150%"><%=den(22)%></h3>
<h3 style="line-height:150%"><%=den(23)%></h3>
<h3 style="line-height:150%"><%=den(24)%></h3>
<h3 style="line-height:150%"><%=den(25)%></h3>
<h3 style="line-height:150%"><%=den(26)%></h3>
<h3 style="line-height:150%"><%=den(27)%></h3>
<h3 style="line-height:150%"><%=den(28)%></h3> 
<h3 style="line-height:150%"><%=den(29)%></h3>
<h3 style="line-height:150%"><%=den(30)%></h3>
<%end if%>
<form name="frmResumen" method="post" target="fraRegistroClient" action=registrarproveedor.asp>
	<table class=principal width=95%>
		<tr>
			<td align = left>
				<INPUT class=button type=button onclick="atras()" name="cmdAtras" id="cmdAtras" value=" << <%=den(5)%> " >
			</td>
			<td align = right>
				<INPUT class=button type=submit name="cmdRegistrar" id="cmdRegistrar" value="<%=den(6)%>" >
			</td>
		</tr>
	</table>
	
<input type="hidden" name="Idioma" value="<%=trim (Request("Idioma"))%>">

<INPUT  type="hidden" id="txtCodCia" name="txtCodCia">
<INPUT  type="hidden" id="txtDen" name="txtDen">
<INPUT  type="hidden" id="txtDir" name="txtDir">
<INPUT  type="hidden" id="txtCP" name="txtCP" >
<INPUT  type="hidden" id="txtPob" name="txtPob">
<INPUT  type="hidden" id="lstPai" name="lstPai">
<INPUT  type="hidden" id="lstProvi" name="lstProvi">
<INPUT  type="hidden" id="lstMon" name="lstMon">
<INPUT  type="hidden" id="txtNIF" name="txtNIF">
<INPUT  type="hidden" id="txtVolFac" name="txtVolFac" >
<INPUT  type="hidden" id="txtCliRef1" name="txtCliRef1">
<INPUT  type="hidden" id="txtCliRef2" name="txtCliRef2">
<INPUT  type="hidden" id="txtCliRef3" name="txtCliRef3">
<INPUT  type="hidden" id="txtCliRef4" name="txtCliRef4">
<INPUT  type="hidden" id="txtHom1" name="txtHom1">
<INPUT  type="hidden" id="txtHom2" name="txtHom2">
<INPUT  type="hidden" id="txtHom3" name="txtHom3">
<INPUT  type="hidden" id="txtURLCia" name="txtURLCia">
<INPUT  type="hidden" id="txtComent" name="txtComent">
<INPUT type="hidden" id=txtCodUsu name=txtCodUsu> 
<INPUT type="hidden" id=txtpwd name=txtpwd>
<INPUT type="hidden" id=txtape name=txtape>
<INPUT type="hidden" id=txtNifUsu name=txtNifUsu>
<INPUT type="hidden" id=txtnombre name=txtnombre>
<INPUT type="hidden" id=txtdep name=txtdep >
<INPUT type="hidden" id=txtcargo  name=txtcargo>
<INPUT type="hidden" id=txttfno name=txttfno>
<INPUT type="hidden" id=txttfno2 name=txttfno2>
<INPUT type="hidden" id=txttfnomv name=txttfnomv>
<INPUT type="hidden" id=txtfax name=txtfax>
<INPUT type="hidden" id=txtemail name=txtemail>
<INPUT type="hidden" id="optTipoEMail" name="optTipoEMail">
<INPUT type="hidden" id=lstIdiUsu name=lstIdiUsu>
<INPUT type="hidden" id=txtNumActs name=txtNumActs>

<div name="divActividad" id="divActividad">
</div>

</form>
</BODY>
<%

set paramgen =nothing
set oraiz=nothing

%>
<!--#include file="../common/fsal_8.asp"-->
</HTML>

