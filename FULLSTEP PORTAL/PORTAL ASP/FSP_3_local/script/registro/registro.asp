﻿<%@ Language=VBScript %>
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader "Pragma", "no-cache" %>
<% Response.Expires = -1 %> 
<%
dim IsSecure
IsSecure = left(Application ("RUTASEGURA"),8)
if IsSecure = "https://" then                       
    IsSecure = "; secure"
else
    IsSecure =""
end if

Response.AddHeader "Set-Cookie","USU_DECIMALFMT=,; path=/; HttpOnly" + IsSecure
Response.AddHeader "Set-Cookie","USU_THOUSANFMT=.; path=/; HttpOnly" + IsSecure
Response.AddHeader "Set-Cookie","USU_PRECISIONFMT=2; path=/; HttpOnly" + IsSecure
Response.AddHeader "Set-Cookie","USU_DATEFMT=dd/mm/yyyy; path=/; HttpOnly" + IsSecure
%>
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/fsal_1.asp"-->

<%
''' <summary>
''' Mostrar el detalle del registro
''' </summary>
''' <remarks>Llamada desde: login/login.asp		login/codigo instalaci�n/registro.asp	login/codigo instalaci�n/codigo idioma/registro.asp; Tiempo m�ximo: 0</remarks>

Idioma = trim(request("idioma"))

set oraiz = CreateObject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
	
if (oRaiz.Conectar(Application("INSTANCIA"))) = 1 then
	set oraiz = nothing
	Response.Redirect Application ("RUTASEGURA") & "script/imposibleconectar.asp?Idioma=" & Idioma
	Response.End  
end if
	
set ParamGen = oraiz.devolverParametrosGenerales() 
				
Application("NOTIF_SOLICREG")=ParamGen.NotifSolicReg
Application("TIPOEMAIL")=ParamGen.TipoEmail
Application("EMAIL")=ParamGen.Email
Application("PREMIUM") = ParamGen.Premium			
Application("IDPAI") = ParamGen.Pais
Application("IDMONCEN") = ParamGen.MonCen
application("COMPRUEBANIF") = paramGen.CompruebaNif
set ParamGen = nothing

dim idxPais
%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script language="JavaScript" src="../common/ajax.js"></script>
<script src="../common/formatos.js"></script>
<!--#include file="../common/fsal_3.asp"-->
</HEAD>
    <script>
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo m�ximo:0</remarks>*/
    </script>
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=application("FSAL")%> == 1)
        {
            Ajax_FSALActualizar3(); //registro de acceso
        }
    }

function Compania ()
{
this.cod = ""
this.den = ""
this.dir = ""
this.cp = ""
this.pob = ""
this.pai = ""
this.idpai = ""
this.provi = ""
this.idprovi
this.mon = ""
this.idmon
this.idi = ""
this.codidi = ""
this.nif = ""
this.volFact = null
this.premium = ""
this.cliRef1 = ""
this.cliRef2 = ""
this.cliRef3 = ""
this.cliRef4 = ""
this.hom1 = ""
this.hom2 = ""
this.hom3 = ""
this.url = ""
this.coment = ""
}


function Usuario ()
{
this.cod = ""
this.pwd = ""
this.nom = ""
this.ape = ""
this.nif = ""
this.dep = ""
this.cargo = ""
this.tlfno1 = ""
this.tlfno2 = ""
this.movil = ""
this.fax = ""
this.email = ""
this.idi = ""
this.codidi = ""
}


oCompania = new Compania()

oUsuario = new Usuario()
ramaRoot = null


function Pais()
{
this.cod = ""
this.den = ""
this.id = ""
this.index = ""
this.validNif = 0
}
oPaises = new Array()

<%

set oPaises = oraiz.Generar_CPaises 
			
oPaises.CargarTodosLosPaises Idioma,,,,,true
i = 1
pais = Application("IDPAI")
for each oPais in oPaises%>
oPaises[<%=i%>]=new Pais()
oPaises[<%=i%>].cod = "<%=jstext(oPais.cod)%>"
oPaises[<%=i%>].den = "<%=jstext(oPais.den)%>"
oPaises[<%=i%>].id = "<%=oPais.id%>"
oPaises[<%=i%>].index = <%=i%>
oPaises[<%=i%>].validNif = <%=oPais.validNif%>
<%
if oPais.id=pais then%>
	oCompania.pai = <%=i%>
<%
	idxpais=oPais.cod
	response.write "//" & idxpais
end if
i = i + 1
next

%>


function Moneda()
{
this.cod = ""
this.den = ""
this.id = ""
this.index = ""
}
oMonedas = new Array()
<%
set oPais = oPaises.Item(idxpais)

if not oPais.Idioma is nothing then
	IdiPais = oPais.Idioma.cod
else
	IdiPais = "SPA"
end if
	

set oMonedas = oraiz.Generar_CMonedas
			
oMonedas.CargarTodasLasMonedasDelPortalDesde Idioma,1000,true				

MonPais = Application("IDMONCEN")

i = 1
for each oMoneda in oMonedas
		%>
		oMonedas[<%=i%>]=new Moneda()
		oMonedas[<%=i%>].cod = "<%=jstext(oMoneda.cod)%>"
		oMonedas[<%=i%>].den = "<%=jstext(oMoneda.den)%>"
		oMonedas[<%=i%>].id = "<%=oMoneda.id%>"
		oMonedas[<%=i%>].index = <%=i%>
		<%if oMoneda.id=MonPais then%>
		oCompania.mon = <%=i%>
		<%end if
		i = i + 1
next


%>

function Provincia()
{
this.cod = ""
this.den = ""
this.id = ""
this.index = ""
}
oProvincias = new Array()
<%

oPais.CargarTodasLasProvincias

i = 1
for each oProvincia in oPais.provincias%>
oProvincias[<%=i%>]=new Provincia()
oProvincias[<%=i%>].cod = "<%=jstext(oProvincia.cod)%>"
oProvincias[<%=i%>].den = "<%=jstext(oProvincia.den)%>"
oProvincias[<%=i%>].id = "<%=oProvincia.id%>"
oProvincias[<%=i%>].index = <%=i%>
<%i = i + 1
next

%>

function Actividades()
{
	this.NumAct=""
	this.Acts = ""
}
oActividades = new Array()

</script>

<frameset rows="100%,0" border=0 frameborder="0" framespacing="0" onload="init()">
  <frame name="fraRegistroClient" scrolling="yes" noresize src="compania.asp?Idioma=<%=Idioma%>">
  <frame name="fraRegistroServer" src="../blank.htm" scrolling="no">
</frameset>

<!--#include file="../common/fsal_2.asp"-->
</html>
<%

set oraiz=nothing

%>