﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ Language=VBScript %>
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader "Pragma", "no-cache" %>
<% Response.Expires = -1 %> 

<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/fsal_7.asp"-->

<%
''' <summary>
''' Mostrar la pantalla para introducir/ver los datos del usuario mientras se registra un proveedor
''' </summary>
''' <remarks>Llamada desde: registro/actividadesCia.asp		registro/compania.asp	
'''			registro/registrarproveedor.asp			registro/resumen.asp; Tiempo m?ximo: 0,1</remarks>
Idioma=Request("Idioma")
Dim Den

den=devolverTextos(Idioma,44)

set oraiz = CreateObject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
if (oRaiz.Conectar(Application("INSTANCIA"))) = 1 then
	set oraiz = nothing
	Response.Redirect Application ("RUTASEGURA") & "script/imposibleconectar.asp?Idioma=" & Idioma
	Response.End  
end if

'Si se le pasa el NIF se tiene que comprobar si existe la compa?ia
if Request.Form("nif")<>"" then
	'Comprueba si la compania existe o no
	if Application("PYME") <> "" then
		codigo = Application("PYME") & Request.Form("codigo")
	else
		codigo = Request.Form("codigo")
	end if
	set TESError = oRaiz.CompaniaValida(codigo, Request.Form("nif"), Application("PYME"))
	if tesError.numError = 1 then 'Si existe el codigo de compania, vuelve atras
		codigo = "errorExisteCompania"
		Response.Redirect Application ("RUTASEGURA") & "script/registro/compania.asp?Idioma=" & Idioma & "&codigo=" & codigo
		Response.End
	elseif tesError.numError = 2 then 'Si existe el NIF de compania, vuelve atras
		codigo = "errorExisteNIFCompania"
		Response.Redirect Application ("RUTASEGURA") & "script/registro/compania.asp?Idioma=" & Idioma & "&codigo=" & codigo
		Response.End
	else
		codigo = "noError"
	end if
end if

set paramgen = oraiz.devolverParametrosGenerales

%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script src="../common/formatos.js"></script>
<script language="JavaScript" src="../common/ajax.js"></script>
<!--#include file="../common/fsal_3.asp"-->
<!--#include file="../common/fsal_5.asp"-->
<script >



function helpWindow(theUrl)
{
winHelp=window.open("<%=application("RUTASEGURA")%>custom/help/<%=Idioma%>/" + theUrl  ,"popup","width=500,height=150,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
return true
}

function ValidarNIFCIF(){
    p= window.parent
    f = document.forms["frmUsuario"]
    switch(p.oPaises[p.oCompania.pai].validNif)
	    {
	    case 0:
		    return true
		    break;
	    case 1:
		    if(f.txtnif.value=="") 
			    return true
		    if (VALIDAR_CIF(f.txtnif.value))
			    return true
			
		    if (VALIDAR_NIF(f.txtnif.value))
			    return true

		    alert("<%=JSAlertText(den(33))%>")
		    return false
		    break;
	    default:
		    return true
	    }
}



function Validar()
{
f=document.forms["frmUsuario"]
p = window.parent
f.txtCod.value = trim(f.txtCod.value)
f.txtpwd.value = trim(f.txtpwd.value)
f.txtpwd2.value = trim(f.txtpwd2.value)
if (trim(f.txtCod.value) == "" || trim(f.txtpwd.value) == "" || trim(f.txtpwd2.value) == ""  || trim(f.txtape.value) == ""
|| trim(f.txtnombre.value) == "" || trim(f.txtdep.value)=="" || trim(f.txtcargo.value) =="" || trim(f.txttfno.value) == "" 
|| trim(f.txtemail.value) == "")
	{	   
	   alert("<%=Den(16)%>")
	   return false
	}	 
if (f.txtpwd.value!=f.txtpwd2.value)
	{
		alert("<%=Den(17)%>")
		return false
	}	 
if (f.txtCod.value.length<6)	 
	{
		alert("<%=Den(18)%>")
		return false
	}	 

if (f.txtpwd.value.length<6)
	{
		alert("<%=Den(19)%>")
		return false
	}	 
	
if (!validarEmail(f.txtemail.value))
	{
		alert("<%=Den(28)%>")
		return false
	}
<%if paramgen.CompruebaTfno then%>
    if (!ValidarTelefono(trim(f.txttfno.value),p.oCompania.denpai))
	    {
		    alert("<%=Den(29)%>")
		    return false
	    }
<%end if%> 
<%if paramgen.CompruebaTfno then%>
    if (trim(f.txttfno2.value) != "")
    {
	    if (!ValidarTelefono(trim(f.txttfno2.value),p.oCompania.denpai)){
		    alert("<%=Den(29)%>")
		    return false
	    }
    }
<%end if%> 
<%if paramgen.CompruebaTfno then%>
    if (trim(f.txttfnomv.value) != "")
    {
	    if (!ValidarTelefono(trim(f.txttfnomv.value),p.oCompania.denpai)){
		    alert("<%=Den(30)%>")
		    return false
	    }
    }	
<%end if%> 
<%if paramgen.CompruebaTfno then%>
    if (trim(f.txtfax.value) != "")
    {
	    if (!ValidarTelefono(trim(f.txtfax.value),p.oCompania.denpai)){
		    alert("<%=Den(31)%>")
		    return false
	    }
    }	


<%end if%> 	
	 
  
//Comprueba que el c?digo contrase?a no tenga caracteres que tengan que ser soportados por el unicode
var b
var i 
for (var i = 0; i < f.txtCod.value.length; i++) {
		var b = f.txtCod.value.charCodeAt(i); 
		if (b>254)
		 {
			alert("<%=Den(27)%>\n" + f.txtCod.value.charAt(i))  //"No registrado. La password no puede contener carateres especiales")
			return false
		 }		 
		 
  }

//Comprueba que la contrase?a no tenga caracteres que tengan que ser soportados por el unicode
for (var i = 0; i < f.txtpwd.value.length; i++) {
		var b = f.txtpwd.value.charCodeAt(i); 
		if (b>254)
		 {
			alert("<%=Den(26)%>\n" + f.txtpwd.value.charAt(i))  //"No registrado. La password no puede contener carateres especiales")
			return false
		 }
   }	

if (!ValidarNIFCIF())
	{
	f.txtnif.focus()
	return false
	}

return true
}

/*
''' <summary>
''' Vuelve a la segunda pantalla de registro
''' </summary>   
''' <remarks>Llamada desde: btnAtras.onclick ; Tiempo m?ximo: 0</remarks>*/
function atras()
{
guardar()
<%if paramGen.ActivarRegistroActiv then %>
	location.href="<%=application("RUTASEGURA")%>script/registro/actividadesCia.asp?Idioma=<%=request("Idioma")%>"
<%else%>
	location.href="<%=application("RUTASEGURA")%>script/registro/compania.asp?Idioma=<%=request("Idioma")%>"
<%end if%>
}

function siguiente()
{
if (<%=application("FSAL")%> == 1)
{
fechaIniResumen=devolverUTCFechaFSAL();  //se registra la fecha FICLIPREPAR
sIdRegistroResumen=devolverIdRegistroFSAL();
var f;
f = document.forms["frmUsuario"];
f.fechaIniFSAL7.value = fechaIniResumen; //pasar fecha inicio
f.sIdRegistroFSAL7.value = sIdRegistroResumen; //pasar idregistro inicio
}
if (!Validar()) return false

guardar()

if (<%=application("FSAL")%> == 1)
{
Ajax_FSALRegistrar5(fechaIniResumen,"<%=sIDPagina%>".replace("usuario.asp","resumen.asp"),sIdRegistroResumen); //FFCLIPREPAR al final de la ejecuci?n de ?siguiente? 
}

return true
}

function guardar()
{
p = window.parent

f = document.forms["frmUsuario"]

p.oUsuario.cod = f.txtCod.value
p.oUsuario.pwd = f.txtpwd.value 
p.oUsuario.pwd = f.txtpwd2.value 
p.oUsuario.ape = f.txtape.value 
p.oUsuario.nif = f.txtnif.value
p.oUsuario.nom = f.txtnombre.value 
p.oUsuario.dep = f.txtdep.value 
p.oUsuario.cargo = f.txtcargo.value 
p.oUsuario.tlfno1 = f.txttfno.value 
p.oUsuario.tlfno2 = f.txttfno2.value 
p.oUsuario.movil = f.txttfnomv.value 
p.oUsuario.fax = f.txtfax.value 
p.oUsuario.email = f.txtemail.value 
p.oUsuario.idi = f.lstIdi.selectedIndex 
p.oUsuario.codidi = f.lstIdi.options[f.lstIdi.selectedIndex].value
if (f.optTipoEMail[0].checked==true)
	{p.oUsuario.TipoMail = 1}
else
	{p.oUsuario.TipoMail = 0}
}


function init()
{
if (<%=application("FSAL")%> == 1)
    {
    Ajax_FSALActualizar3(); //registro de acceso
    }
p = window.parent
f = document.forms["frmUsuario"]

f.txtCod.value = p.oUsuario.cod
f.txtpwd.value = p.oUsuario.pwd
f.txtpwd2.value = p.oUsuario.pwd
f.txtape.value = p.oUsuario.ape
f.txtnif.value = p.oUsuario.nif
f.txtnombre.value = p.oUsuario.nom
f.txtdep.value = p.oUsuario.dep
f.txtcargo.value = p.oUsuario.cargo
f.txttfno.value = p.oUsuario.tlfno1
f.txttfno2.value = p.oUsuario.tlfno2
f.txttfnomv.value = p.oUsuario.movil
f.txtfax.value = p.oUsuario.fax
f.txtemail.value = p.oUsuario.email
if (p.oUsuario.idi!="") 
	f.lstIdi.selectedIndex = p.oUsuario.idi
if (p.oUsuario.TipoMail == 1)
	f.optTipoEMail[1].checked = true
else
	f.optTipoEMail[0].checked = true
}

</script>

</head>
<body onload="init();" topmargin="0" leftmargin="0">
<h1><%if paramGen.ActivarRegistroActiv then%><%=Den(25)%><%else%><%=Den(1)%><%end if%></h1>
<h3><%=Den(2)%></h3>
<h4><%=Den(3)%></h4>

<form name="frmUsuario" onsubmit="return siguiente()" action="resumen.asp" method="post">

<input type="hidden" name="Idioma" value="<%=Idioma%>">

<table class="principal" border="0" bgcolor="white" cellpadding="3" cellspacing="0" width="95%">
	<tr height="1">
		<td class="filaImpar" width="200">
			<%=Den(4)%><a TABINDEX="-1" href="javascript:void(null);" onclick="helpWindow('usuario.asp')"><img border="0" SRC="../images/ico_help.gif" WIDTH="15" HEIGHT="15"></a>
		</td>
		<td class="filaImpar" width="500">
			<input maxlength="20" type="text" id="txtCod" name="txtCod" size="20" autocomplete="off"> 
		</td>
	</tr>
	<tr>
		<td class="filaPar" width="200">
			<%=Den(5)%><a TABINDEX="-1" href="javascript:void(null);" onclick="helpWindow('contrasena.asp')"><img border="0" SRC="../images/ico_help.gif" WIDTH="15" HEIGHT="15"></a>
		</td>
		<td class="filaPar" width="500">
			<input id="txtpwd" name="txtpwd" type="password" maxLength="20" size="20" autocomplete="off">
		</td>
	</tr>
	<tr>
		<td class="filaImpar" width="200">
			<%=Den(6)%>
		</td>
		<td class="filaImpar" width="500">
			<input id="txtpwd2" name="txtpwd2" type="password" maxLength="20" size="20" autocomplete="off">
		</td>
	</tr>
	<tr>
		<td class="filaPar" width="200">
			<%=Den(8)%>
		</td>
		<td class="filaPar" width="500">
			<input id="txtnombre" name="txtnombre" maxLength="20" size="20">			
		</td>
	</tr>
	<tr>
		<td class="filaImpar" width="200">
			<%=Den(7)%>
		</td>
		<td class="filaImpar" width="500">
			<input id="txtape" name="txtape" maxLength="100" size="50">
		</td>
	</tr>
    <tr>
		<td class="filaPar" width="200">
			<%=Den(32)%>
		</td>
		<td class="filaPar" width="500">
			<input id="txtnif" name="txtnif" maxLength="100" size="50" onchange="ValidarNIFCIF();">
		</td>
	</tr>
	<tr>
		<td class="filaImpar" width="200">
			<%=Den(9)%>
		</td>
		<td class="filaImpar" width="500">
			<input id="txtdep" name="txtdep" maxLength="100" size="50">
		</td>
	</tr>
	<tr>
		<td class="filaPar" width="200">
			<%=Den(10)%>
		</td>
		<td class="filaPar" width="500">
			<input id="txtcargo" maxLength="100" size="50" name="txtcargo">
		</td>
	</tr>
	<tr>
		<td class="filaImpar" width="200">
			<%=Den(11)%>
		</td>
		<td class="filaImpar" width="500">
			<input id="txttfno" maxLength="20" size="20" name="txttfno">
		</td>
	</tr>
	<tr>
		<td class="filaPar" width="200">
		&nbsp;
		</td>
		<td class="filaPar" width="550">
			<input id="txttfno2" maxLength="20" size="20" name="txttfno2">
		</td>
	</tr>
	<tr>
		<td class="filaImpar" width="200">
			<%=Den(20)%>
		</td>
		<td class="filaImpar" width="500">
			<input id="txttfnomv" maxLength="20" size="20" name="txttfnomv">
		</td>
	</tr>
	<tr>
		<td class="filaPar" width="200">
			<%=Den(12)%>
		</td>
		<td class="filaPar" width="500">
			<input id="txtfax" maxLength="20" size="20" name="txtfax">
		</td>
	</tr>
	<tr>
		<td class="filaImpar" width="200">
			<%=Den(13)%><a TABINDEX="-1" href="javascript:void(null);" onclick="helpWindow('email.asp')"><img border="0" SRC="../images/ico_help.gif" WIDTH="15" HEIGHT="15"></a>
		</td>
		<td class="filaImpar" width="500">
			<input id="txtemail" maxLength="100" size="50" name="txtemail">
		</td>
	</tr>
	<tr>
		<td class="filaPar" width="200">
			<%=Den(14)%><a TABINDEX="-1" href="javascript:void(null);" onclick="helpWindow('idioma.asp')"><img border="0" SRC="../images/ico_help.gif" WIDTH="15" HEIGHT="15"></a>
		</td>
		<td class="filaPar" width="500">
			<select name="lstIdi" size="1">
			<%
			set oRaiz = createobject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
			i = oraiz.Conectar(Application("INSTANCIA"))
			set adorIdi = oRaiz.DevolverIdiomas
			while not adorIdi.eof%>
				<OPTION value="<%=adorIdi.fields("COD").value%>"<%if idioma=adorIdi.fields("COD").value then%> selected<%end if%>><%=adorIdi.fields("DEN").value%></OPTION>
				<%adorIdi.movenext
			wend
			adorIdi.close
			set adorIdi = nothing
			set oRaiz = nothing%>

			</select>
		</td>
	</tr>
	<tr>
		<td class="filaImpar" width="200">
			<%=Den(22)%><a TABINDEX="-1" href="javascript:void(null);" onclick="helpWindow('tipoemail.asp')"><img border="0" SRC="../images/ico_help.gif" WIDTH="15" HEIGHT="15"></a>
		</td>		
		<td class="filaImpar" width="500">
			<input type="radio" class="radio" id="optTipoEMail" name="optTipoEMail" value="1" checked><%=Den(24)%>
			<input type="radio" class="radio" id="optTipoEMail" name="optTipoEMail" value="0"><%=Den(23)%>
		</td>		
	</tr>	
</table>


<table class="principal" width="95%" style="margin-top:3px;">
	<tr>
		<td align="left">
			<input class="button" id="btnAtras" name="btnAtras" type="button" onclick="atras()" value="  &lt;&lt;  <% = Den(21) %>">
		</td>
		<td align="right">
			<input class="button" id="Registrar" name="registrar" type="submit" value="<% = Den(15) %>  &gt;&gt;">
		</td>
	</tr>
</table>
<%if Application("FSAL")=1 then%>   
    <input type="hidden" name="fechaIniFSAL7">
    <input type="hidden" name="sIdRegistroFSAL7">
    <%end if%>
</form>
</body>
<!--#include file="../common/fsal_8.asp"-->
</html>
