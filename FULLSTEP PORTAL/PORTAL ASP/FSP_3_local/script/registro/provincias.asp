﻿<%@ Language=VBScript %>
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader "Pragma", "no-cache" %>
<% Response.Expires = -1 %> 

<!--#include file="../common/XSS.asp"-->
<!--#include file="../common/formatos.asp"-->
<html>
<%

''' <summary>
''' Para registrar a un nuevo proveedor hay q indicar de q provincia es. Esta pantalla carga de bbdd
'''	las provincias del pais indicado.
''' </summary>
''' <remarks>Llamada desde: registro/compania.asp; Tiempo máximo: 0,1</remarks>

set oraiz = CreateObject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
	
if (oRaiz.Conectar(Application("INSTANCIA"))) = 1 then
	set oraiz = nothing
	Response.Redirect Application ("RUTASEGURA") & "script/imposibleconectar.asp?Idioma=" & Idioma
	Response.End  
end if

Pais= cint(request("Pais"))

	set opaises = oraiz.generar_CPaises
	oPaises.add  "","","","",null,Pais
	set oPais = oPaises.item(1)
	
	opais.CargarTodasLasProvincias ,,,true

%>
<script>
function obtenerProvi()
{
var f
f=window.parent.frames["fraRegistroClient"]
p=window.parent

f.borrarProvi()

f.anyadirProvi('','')
p.oProvincias = new Array()
<%
i=1
	for each oProvi in oPais.Provincias
%>	
p.oProvincias[<%=i%>]=new p.Provincia()
p.oProvincias[<%=i%>].cod = "<%=oProvi.cod%>"
p.oProvincias[<%=i%>].den = "<%=JSAlertText(oProvi.den)%>"
p.oProvincias[<%=i%>].id = "<%=oProvi.id%>"
p.oProvincias[<%=i%>].index = <%=i%>
<%i = i + 1
	next
%>
f.cargarProvincias()
}
</script>
<%
set oPais=nothing
set oPaises=nothing

set oRaiz=nothing


%>
<head>
     <title><%=title%></title>
</head>
<body onload="obtenerProvi();">
</body>
</html>