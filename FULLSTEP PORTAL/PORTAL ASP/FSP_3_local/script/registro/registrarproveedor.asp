﻿<%@ Language=VBScript %>
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader "Pragma", "no-cache" %>
<% Response.Expires = -1 %> 

<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/colores.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/fsal_1.asp"-->

<%
''' <summary>
''' Se graba en bbdd los datos del nuevo proveedor
''' </summary>
''' <remarks>Llamada desde: registro/resumen.asp; Tiempo máximo: 0,2</remarks>

set oRaiz = createobject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
	
if (oRaiz.Conectar(Application("INSTANCIA"))) = 1 then
	set oraiz = nothing
	Response.Redirect Application ("RUTASEGURA") & "script/imposibleconectar.asp?Idioma=" & Idioma
	Response.End 
end if

Dim Den

Idioma = request("Idioma")
den=devolverTextos(Idioma,42)

set paramgen = oraiz.devolverParametrosGenerales

lIdUsu = 0 

%>

<HTML>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<STYLE>
#divAccion {position:relative;font-weight:bold;font-size:12px;color:<%=cACCIONPROGRESO%>;}
#divSalir {position:relative;}
</STYLE>
<script language="JavaScript" src="../common/ajax.js"></script>
<script src="../common/formatos.js"></script>
<!--#include file="../common/fsal_3.asp"-->
<script language="Javascript">
function init()
{
if (<%=application("FSAL")%> == 1 )
{
    Ajax_FSALActualizar3();  //registro de acceso
}
}
</script>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="GENERATOR" content="Microsoft FrontPage 4.0"/>
</head>
<body topmargin=0 leftmargin=0 onload="init()">
    <h1><%=den(1)%></h1>
    <div class="principal" style="float:left; width:70%; text-align:left;">
        <div id="divAccion" name="divAccion"></div>
    </div>
    <div id="divProgreso" name="divProgreso" style="float:left; width:70%;"></div>
    <div style="float:left; width:70%; margin-top:1em; text-align:center;">
        <div id="divSalir" name="divSalir"></div>
    </div>   
<%


dim numBlocks

numBlocks = 4


incr = 40 / numBlocks

function Progreso()

dim i
dim s
S="<table class=principal align = left width = 100% cellspacing=1 cellpadding = 1><tr><td bgcolor=\'" & cBORDEPROGRESO & "\'>"
S=S & "<table width = 100% bgcolor=white><tr>"


for i = 1 to 40
	if i <=Actual then
		S=S & "<td bgcolor=\'" & cBLOQUEPROGRESO & "\'>&nbsp</td>"
	else
		S=S & "<td bgcolor=white>&nbsp</td>"
	end if
next
s=s & "</tr></table></td></tr></table>"

Progreso = s

end function

%>
<SCRIPT>


document.getElementById('divAccion').innerHTML='<%=JSText(den(2))%>'</script>
<%

Actual = incr

oraiz.comenzarTransaccion()

%>
<SCRIPT>document.getElementById('divProgreso').innerHTML='<%=Progreso()%>'</script>

<%


Idi = request("lstIdiUsu")
IdPortal = cint(Application ("PORTAL"))
CodPortal = cstr(Application ("NOMPORTAL"))
if Application("PYME") <> "" then
	CodCia = Application("PYME") & request("txtCodCia") 
else
	CodCia = request("txtCodCia") 
end if

Pyme=Application("PYME")
DenCia = request("txtDen")
Dir = request("txtDir")
CP = request("txtCP")
Pob = request("txtPob")
Pai = request("lstPai")
Mon= request("lstMon")
NIF = request("txtNIF")
VolFact = numero(request("txtVolFac"))
TipoAcc = 0
Provi = request("lstprovi")
	
if Provi= "-1" or Provi= "" then
	Provi = null
end if
CliRef1 =request("txtCliRef1")
CliRef2 =request("txtCliRef2")
CliRef3 =request("txtCliRef3")
CliRef4 =request("txtCliRef4")

Hom1 = request("txtHom1")
Hom2 = request("txtHom2")
Hom3 = request("txtHom3")

URLCia = request("txtURLCia")
Coment = request("txtComent")
NumActs= request("txtNumActs")

HOST = Request.ServerVariables("REMOTE_ADDR") & " : " & Request.ServerVariables("REMOTE_HOST")



Response.Write "<SCRIPT>document.getElementById('divAccion').innerHTML='" & JSText(den(3)) & "'</script>"


Actual = Actual + incr

set TESError = oRaiz.RegistrarCiaEnPortal (Idi,IdPortal,CodCia , denCia , dir , cp , pob , pai ,Mon,nif,volfact,TipoAcc,provi,cliref1,CliRef2,CliRef3,CliRef4,Hom1,Hom2,Hom3,URLCia,Coment,host,0,Pyme,CodPortal)

%>
<SCRIPT>document.getElementById('divProgreso').innerHTML='<%=Progreso()%>'</script>

<%
if tesError.numError <> 0 then
	If TESError.NumError = 200 Then
	    oRaiz.CancelarTransaccion
		set oraiz = nothing
        %>
		<SCRIPT>
		document.getElementById('divAccion').innerHTML='<%=JSText(den(9))%>'
		document.getElementById('divSalir').innerHTML = '<A LANGUAGE=javascript HREF=<%=application("RUTASEGURA")%>script/registro/compania.asp?Idioma=<%=Idioma%>><%=Den (14)%></A>'
		</script>
		<%
		Response.End		
	else
		set oraiz = nothing
		if TESError.NumError=301 then
        %>
		<SCRIPT>
		document.getElementById('divAccion').innerHTML='<%=JSText(den(9))%> <%=JSText(den(15))%>'
		document.getElementById('divSalir').innerHTML = '<A LANGUAGE=javascript HREF=<%=application("RUTASEGURA")%>script/registro/compania.asp?Idioma=<%=Idioma%>><%=Den (14)%></A>'
		</script>
		<%
		elseif TESError.NumError=2 then
        %>
		<SCRIPT>
		document.getElementById('divAccion').innerHTML='<%=JSText(den(9))%> <%=JSText(den(18))%>'
		document.getElementById('divSalir').innerHTML = '<A LANGUAGE=javascript HREF=<%=application("RUTASEGURA")%>script/registro/compania.asp?Idioma=<%=Idioma%>><%=Den (14)%></A>'
		</script>
		<%
		else
        %>
		<SCRIPT>
		document.getElementById('divAccion').innerHTML='<%=JSText(den(9))%>'
		document.getElementById('divSalir').innerHTML = '<A LANGUAGE=javascript HREF=<%=application("RUTASEGURA")%>script/registro/compania.asp?Idioma=<%=Idioma%>><%=Den (14)%></A>'
		</script>
		<%
		end if
		Response.End		
	end if
else
	IDCiaProve = TesError.Arg1
end if


Cod = Request("txtCodUsu")
Pwd = Request("txtpwd")
Ape = Request("txtApe")
Nif = Request("txtNifUsu")
Nom = Request("txtNombre")
Dep = Request("txtDep")
Cargo = Request("txtCargo")
Tfno = Request("txtTfno")
Tfno2 = Request("txtTfno2")
TfnoMv = Request("txtTfnoMv")
Fax = Request("txtFax")
Email = Request("txtEmail")
TipoMail = Request("optTipoEMail")	

Response.Write "<SCRIPT>document.getElementById('divAccion').innerHTML='" & JSText(den(4)) & "'</script>"

Actual = Actual + incr
	
set TESError = oRaiz.RegistrarUsuarioEnPortal (IdPortal,IDCiaProve,Cod,Ape,Nif,Nom,Dep,Cargo,Tfno,Tfno2,Fax,Email,Idi,HOST,true,1,TfnoMv,TipoMail,Pwd)

%>
<SCRIPT>document.getElementById('divProgreso').innerHTML='<%=Progreso()%>'</script>

<%
if tesError.numError <> 0 then    
    select case TESError.NumError            
        case 10001,10002,10003,10004
             oRaiz.CancelarTransaccion
		    set oraiz = nothing%>
		    <script type="text/javascript">
		        document.getElementById('divAccion').innerHTML = '<%=JSText(den(10))%>' + '<br/><%=JSText(Replace(Replace(den(18+(TESError.NumError-10000)),"\n",chr(10)),"###",TESError.Arg1))%>'
		        document.getElementById('divSalir').innerHTML = '<A LANGUAGE=javascript HREF=<%=application("RUTASEGURA")%>script/registro/usuario.asp?Idioma=<%=Idioma%>><%=Den (14)%></A>'
		    </script>
		    <%Response.End
        case 200,10000
            oRaiz.CancelarTransaccion
		    set oraiz = nothing%>
		    <script type="text/javascript">
		        document.getElementById('divAccion').innerHTML = '<%=JSText(den(10))%>'
		        document.getElementById('divSalir').innerHTML = '<A LANGUAGE=javascript HREF=<%=application("RUTASEGURA")%>script/registro/usuario.asp?Idioma=<%=Idioma%>><%=Den (14)%></A>'
		    </script>
		    <%Response.End
        case else
            set oraiz = nothing%>
		    <script type="text/javascript">
		        document.getElementById('divAccion').innerHTML = '<%=JSText(den(10))%>'
		        document.getElementById('divSalir').innerHTML = '<A LANGUAGE=javascript HREF=<%=application("RUTASEGURA")%>script/registro/usuario.asp?Idioma=<%=Idioma%>><%=Den (14)%></A>'
		    </script>
		    <%Response.End
    end select
end if

'Registra las actividades de la compañía
if paramGen.ActivarRegistroActiv then
 dim oActsN1
 dim oActsN2
 dim oActsN3
 dim oActsN4
 dim oActsN5
	
 set oActsN1 = oraiz.generar_cactividadesNivel1()
 set oActsN2 = oraiz.generar_cactividadesNivel2()
 set oActsN3 = oraiz.generar_cactividadesNivel3()
 set oActsN4 = oraiz.generar_cactividadesNivel4()
 set oActsN5 = oraiz.generar_cactividadesNivel5()
	
		
 for i = 1 to NumActs
	IdentAct=request("txtAct" & cstr(i))
	Idents=Split(IdentAct,"_",-1,1)
	if ubound(Idents)= 0 then
		ACN1 = cint(Idents(0))
		oactsn1.add acn1,"codact","No Importa ahora"
	else
		if ubound(Idents)= 1 then
			ACN1 = cint(Idents(0))
			ACN2 = cint(Idents(1))
			oactsn2.add acn1,acn2,"codact","No Importa ahora"
		else
			if ubound(Idents)= 2 then
				ACN1 = cint(Idents(0))
				ACN2 = cint(Idents(1))
				ACN3 = cint(Idents(2))
				oactsn3.add acn1,acn2,acn3,"codact","No Importa ahora"
			else
				if ubound(Idents)= 3 then
					ACN1 = cint(Idents(0))
					ACN2 = cint(Idents(1))
					ACN3 = cint(Idents(2))
					ACN4 = cint(Idents(3))
					oactsn4.add acn1,acn2,acn3,acn4,"codact","No Importa ahora"
				else
					ACN1 = cint(Idents(0))
					ACN2 = cint(Idents(1))
					ACN3 = cint(Idents(2))
					ACN4 = cint(Idents(3))
					ACN5 = cint(Idents(4))
					oactsn5.add acn1,acn2,acn3,acn4,acn5,"codact","No Importa ahora"
				end if
			end if
		end if
	end if
 next

 Response.Write "<SCRIPT>document.getElementById('divAccion').innerHTML='" & JSText(den(5)) & "'</script>"

 Actual = Actual + incr

 set oError = oRaiz.RegistrarActividadesEnPortal(IDCiaProve,oactsN1 , oactsN2 , oactsN3 , oactsN4 , oactsN5 )
	
 %>
<SCRIPT>document.getElementById('divProgreso').innerHTML='<%=Progreso()%>'</script>

 <%

 if tesError.numError <> 0 then
	If TESError.NumError = 200 Then
	    oRaiz.CancelarTransaccion
		set oraiz = nothing
        %>
		<SCRIPT>
		document.getElementById('divAccion').innerHTML='<%=JSText(den(11))%>'
		document.getElementById('divSalir').innerHTML = '<A LANGUAGE=javascript HREF=<%=application("RUTASEGURA")%>script/registro/usuario.asp?Idioma=<%=Idioma%>><%=Den (14)%></A>'
		</script>
		<%
		Response.End		
		
	else
		set oraiz = nothing
        %>
		<SCRIPT>
		document.getElementById('divAccion').innerHTML='<%=JSText(den(11))%>'
		document.getElementById('divSalir').innerHTML = '<A LANGUAGE=javascript HREF=<%=application("RUTASEGURA")%>script/registro/usuario.asp?Idioma=<%=Idioma%>><%=Den (14)%></A>'
		</script>
		<%
		Response.End		
	end if
 end if
end if
oRaiz.confirmarTransaccion

set oCia=oraiz.generar_CCompania()
oCia.Id=IDCiaProve
oCia.PYME=Application("PYME")
if Application("NOTIF_SOLICREG")=1 then
	oCia.LanzarEmailAdmRegistro Application("EMAIL"), Application("TIPOEMAIL"), den(16), lIdUsu, CodPortal
end if
oCia.LanzarEmailProveConfirmacionAlta lIdUsu, CodPortal

set oCia=nothing
set paramgen =nothing
set oraiz = nothing

Actual = Actual + incr
%>
<SCRIPT>document.getElementById('divProgreso').innerHTML='&nbsp'</script>
<SCRIPT>
document.getElementById('divAccion').innerHTML='<%=JSText(den(13))%>'
if (document.referrer != "") {
    p = window.parent;
    document.getElementById('divSalir').innerHTML = '<A align = left LANGUAGE=javascript HREF="javascript:p.window.close(); "><%=Den (17)%></A>'
} else {
    document.getElementById('divSalir').innerHTML = '<A align = left target="_top" LANGUAGE=javascript HREF="<%=application("RUTASEGURA")%>default.asp?Idioma=<%=Idioma%>"><%=Den (8)%></A>'
}
</script>
</body>
<!--#include file="../common/fsal_2.asp"-->
</html>
