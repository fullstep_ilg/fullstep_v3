﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ Language=VBScript %>
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader "Pragma", "no-cache" %>
<% Response.Expires = -1 %> 

<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/fsal_1.asp"-->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script src="../common/formatos.js"></script>
<script language="JavaScript" src="../common/ajax.js"></script>
<!--#include file="../common/fsal_3.asp"-->
<!--#include file="../common/fsal_5.asp"-->
<%
''' <summary>
''' Primera pantalla del registro de nuevos proveedores. Le indicas los datos "personales" de la empresa, por 
'''	ejemplo, razón social, nif, pais, etc.
''' </summary>
''' <remarks>Llamada desde: registro/usuario.asp	registro/registro.asp	registro/registrarproveedor.asp
'''			registro/actividadescia.asp; Tiempo máximo: 0,1</remarks>

''' Cargamos los literales según el idioma actual
Idioma = Request("Idioma")
Idioma = trim(Idioma)	

''' Para consultar si el código de la companía es valida
codigo = Request("codigo")

Dim Den

den=devolverTextos(Idioma,39)

	''' Asignamos los literales a cada elemento
	lblRellene = Den(2)
	lblOblig = Den (3)
	lblCod = Den (4)
	lblAyudaCod = Den(5)
	lblDen = Den (6)
	lblDir = Den (7)
	lblCP = den (8)
	lblPob = Den (9)
	lblPai = Den (10)
	lblProvi = Den (11)
	lblMoneda = Den (12)
	lblIdioma = Den (13)
	lblNIF = Den(14)
	lblVolFact = Den(15)
	lblMEuros = Den(16)

    lblProviOblig = Den(42)  ' Provincia (*) :

'	if application("NOMPORTAL") = "huf" then'
'		lblClientesRef = Den(34)
'	else
		lblClientesRef = Den(17)
'	end if
	
	lblHom = Den(18)
	lblURLCia = Den(19)
	Error1=Den(24)
	Error2=Den(25)
	Error3=Den(26)
	cmdRegistrar = den(27)
	lblPremium = den(28)
	Error4=den(40)
	
	
	set oraiz = CreateObject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
	if (oRaiz.Conectar(Application("INSTANCIA"))) = 1 then
		set oraiz = nothing
		Response.Redirect Application ("RUTASEGURA") & "script/imposibleconectar.asp?Idioma=" & Idioma
		Response.End  
	end if
	set paramgen = oraiz.devolverParametrosGenerales

    if UCase(application("NOMPORTAL"))="GES" then
        lblActivi = Den(45)	
        lblComent = Den(46)		
    else
        if paramGen.ActivarRegistroActiv then		    
		    lblActivi = Den(36)	
            lblComent = Den(35)		
	    else		    
		    lblActivi = Den(29)	
            lblComent = Den(20)		
	    end if	
    end if
	if paramGen.ActivarRegistroActiv then
		lblTitulo = Den(37)		
	else
		lblTitulo = Den(1)		
	end if		

%>

<script language="Javascript">
<!--
vdecimalfmt = ","
vthousanfmt = "."
vprecisionfmt = 2
vdatefmt = "dd/mm/yyyy"

var provinciaObligatoria

function helpWindow(theUrl)
{
winHelp=window.open("<%=application("RUTASEGURA")%>custom/help/<%=Idioma%>/" + theUrl  ,"popup","width=500,height=250,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
return true
}


function obtenerProvi()
{
Pais=document.forms["frmCompania"].lstPai[document.forms["frmCompania"].lstPai.selectedIndex].value
if (Pais=="")
	{
	borrarProvi()

	document.forms["frmCompania"].lstProvi.options[0]=new Option ("",-1)
	return
	}
window.open("<%=Application("RUTASEGURA")%>script/registro/Provincias.asp?Pais=" + Pais,"fraRegistroServer")

}

function borrarProvi()
{
while(document.forms["frmCompania"].lstProvi.options.length>0)
	{
	document.forms["frmCompania"].lstProvi.options.remove(document.forms["frmCompania"].lstProvi.options.length - 1)
	}
}

function ValidarCP()
{
    f = document.forms["frmCompania"]
    var urlregex = new RegExp("^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$");
    return urlregex.test(f.txtCP.value);
}

function ValidarDirWebEmpresa()
{
    f = document.forms["frmCompania"]
    if (f.txtURLCia.value=="")
    {
        return true;
    }
    else
    {
    var urlregex = new RegExp("^(https?://)?(([\\w!~*'().&=+$%-]+: )?[\\w!~*'().&=+$%-]+@)?(([0-9]{1,3}\\.){3}[0-9]{1,3}|([\\w!~*'()-]+\\.)*([\\w^-][\\w-]{0,61})?[\\w]\\.[a-z]{2,6})(:[0-9]{1,4})?((/*)|(/+[\\w!~*'().;?:@&=+$,%#-]+)+/*)$");
    return urlregex.test(f.txtURLCia.value);
    }
}

function anyadirProvi(Id,Den)
{
document.forms["frmCompania"].lstProvi.options[document.forms["frmCompania"].lstProvi.options.length]=new Option(Den,Id)
}

/*
''' <summary>
''' realiza las validaciones del formulario 
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: compania.asp Tiempo máximo: 0</remarks>
''' <revision>JVS 16/06/2011</revision>
*/
function validar()
{
if (<%=application("FSAL")%> == 1)
{
fechaIniActCia=devolverUTCFechaFSAL();  //se registra la fecha FICLIPREPAR al inicio de la ejecución de ‘validar’
sIdRegistroActCia=devolverIdRegistroFSAL();
}
var f
var errorProvincia = false
f = document.forms["frmCompania"]
f.txtCod.value = trim(f.txtCod.value)

var s
s = document.forms["frmSiguiente"]
s.codigo.value = f.txtCod.value
s.nif.value = f.txtNIF.value
s.direccion.value = f.txtDir.value
if (<%=application("FSAL")%> == 1){
   s.fechaIniFSAL7.value = fechaIniActCia; //pasar fecha inicio
   s.sIdRegistroFSAL7.value = sIdRegistroActCia; //pasar idregistro inicio  
}
//Si la provincia es obligatoria, se comprueba si en la lista de provincias se ha seleccionado alguna
if (provinciaObligatoria && (document.forms["frmCompania"].lstProvi.value == "" || document.forms["frmCompania"].lstProvi.value == "-1"))
    errorProvincia = true

if (trim(f.txtCod.value)=="" || trim(f.txtDen.value)=="" || trim(f.txtDir.value)=="" || trim(f.txtCP.value)=="" 
	|| trim(f.txtPob.value)=="" || trim(f.txtNIF.value)=="" <%if not paramGen.ActivarRegistroActiv or UCase(application("NOMPORTAL"))="GES" then%> || trim(f.txtComent.value)==""  <%end if%>
	|| f.lstMon.selectedIndex==0 || errorProvincia) 
	{	
	alert("<%=Error1%>");
	return false
	}
else 
	{
	if (f.txtComent.length>710) 
		{
		alert("<%=Error3%>");
		return false;
		}
	}
if (!ValidarNIFCIF())
	{
	f.txtNIF.focus()
	return false
	}
if (!ValidarDirWebEmpresa())
    {
    alert("<%=den(44)%>");
    return false;
    }

if (document.forms["frmCompania"].lstPai[document.forms["frmCompania"].lstPai.selectedIndex].value==198)
{
if (!ValidarCP())
    {
    alert("<%=den(43)%>");
    return false;
    }
}
	
//Comprueba que el código contraseña no tenga caracteres que tengan que ser soportados por el unicode
var b
var i 
for (var i = 0; i < f.txtCod.value.length; i++) {
		var b = f.txtCod.value.charCodeAt(i); 
		if (b>254)
		 {
			alert("<%=Error4%>\n" + f.txtCod.value.charAt(i))  //"No registrado. La password no puede contener carateres especiales")
			return false
		 }
    }
    
siguiente()
if (<%=Application("FSAL")%> == 1){
    Ajax_FSALRegistrar5(fechaIniActCia,"<%=sIDPagina%>".replace("compania.asp","actividadesCia.asp"),sIdRegistroActCia); //FFCLIPREPAR al final de ‘validar’
}
return true;
}


function siguiente()
{
p = window.parent

f = document.forms["frmCompania"]
p.oCompania.cod = f.txtCod.value
p.oCompania.den = f.txtDen.value
p.oCompania.dir = f.txtDir.value
p.oCompania.cp = f.txtCP.value
p.oCompania.pob = f.txtPob.value
p.oCompania.pai = f.lstPai.selectedIndex
p.oCompania.idpai = f.lstPai.options[f.lstPai.selectedIndex].value
p.oCompania.denpai = f.lstPai.options[f.lstPai.selectedIndex].text.toUpperCase()
p.oCompania.provi = f.lstProvi.selectedIndex
p.oCompania.idprovi = f.lstProvi.options[f.lstProvi.selectedIndex].value
p.oCompania.mon = f.lstMon.selectedIndex
p.oCompania.idmon = f.lstMon.options[f.lstMon.selectedIndex].value
p.oCompania.nif = trim(f.txtNIF.value)


p.oCompania.volFact  = str2num(f.txtVolFac.value,vdecimalfmt,vthousanfmt,vprecisionfmt)
p.oCompania.cliRef1 = f.txtCliRef1.value
p.oCompania.cliRef2 = f.txtCliRef2.value
p.oCompania.cliRef3 = f.txtCliRef3.value
p.oCompania.cliRef4 = f.txtCliRef4.value
p.oCompania.hom1 = f.txtHom1.value
p.oCompania.hom2 = f.txtHom2.value
p.oCompania.hom3 = f.txtHom3.value
p.oCompania.url = f.txtURLCia.value
p.oCompania.coment = f.txtComent.value

}

function cargarPaises()
{
p=window.parent

for (i=1;i<p.oPaises.length;i++)
{
document.forms["frmCompania"].lstPai.options[document.forms["frmCompania"].lstPai.options.length]=new Option(p.oPaises[i].den,p.oPaises[i].id)
}
}

/*
''' <summary>
''' carga de la lista de provincias 
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: compania.asp Tiempo máximo: 0</remarks>
''' <revision>JVS 16/06/2011</revision>
*/
function cargarProvincias()
{
p=window.parent

// El literal de Provincia cambia en función de si hay provincias cargadas o no
if(p.oProvincias.length > 0)
{
    provinciaObligatoria = true
    document.getElementById("prov").innerHTML = "<%=lblProviOblig %>"
}
else
{
    provinciaObligatoria = false
    document.getElementById("prov").innerHTML    = "<%=lblProvi %>"
}
for (i=1;i<p.oProvincias.length;i++)
{
document.forms["frmCompania"].lstProvi.options[document.forms["frmCompania"].lstProvi.options.length]=new Option(p.oProvincias[i].den,p.oProvincias[i].id)
}

}

function cargarMonedas()
{
p=window.parent

for (i=1;i<p.oMonedas.length;i++)
{
document.forms["frmCompania"].lstMon.options[document.forms["frmCompania"].lstMon.options.length]=new Option(p.oMonedas[i].den,p.oMonedas[i].id)
}

}

function init(){
if (<%=Application("FSAL")%> == 1)
    {
        Ajax_FSALActualizar3(); //registro de acceso
    }
p = window.parent

f = document.forms["frmCompania"]

cargarPaises()
cargarProvincias()
cargarMonedas()
f.txtCod.value=p.oCompania.cod
f.txtDen.value=p.oCompania.den
f.txtDir.value=p.oCompania.dir
f.txtCP.value=p.oCompania.cp
f.txtPob.value=p.oCompania.pob
f.lstPai.selectedIndex=p.oCompania.pai
f.lstProvi.selectedIndex=p.oCompania.provi
f.lstMon.selectedIndex=p.oCompania.mon
f.txtNIF.value=p.oCompania.nif
f.txtVolFac.value=num2str(p.oCompania.volFact,vdecimalfmt,vthousanfmt,vprecisionfmt)
f.txtCliRef1.value=p.oCompania.cliRef1
f.txtCliRef2.value=p.oCompania.cliRef2
f.txtCliRef3.value=p.oCompania.cliRef3
f.txtCliRef4.value=p.oCompania.cliRef4
f.txtHom1.value=p.oCompania.hom1
f.txtHom2.value=p.oCompania.hom2
f.txtHom3.value=p.oCompania.hom3
f.txtURLCia.value=p.oCompania.url
f.txtComent.value=p.oCompania.coment

if ("<%=codigo%>" == "errorExisteCompania")
	{
		alert("<%=den(38)%>");
	}
else if ("<%=codigo%>" == "errorExisteNIFCompania")
	{
		alert("<%=den(41)%>");
	}
else if ("<%=codigo%>" !== "")
	{
		alert("<%=codigo%>");
	}
}
<%if application("COMPRUEBANIF") then%>
function ValidarNIFCIF()

{
p= window.parent


f = document.forms["frmCompania"]
switch(p.oPaises[f.lstPai.selectedIndex].validNif)
	{
	case 0:
		return true
		break;
	case 1:
		if(f.txtNIF.value=="") 
			return true
		if (VALIDAR_CIF(f.txtNIF.value))
			return true
			
		if (VALIDAR_NIF(f.txtNIF.value))
			return true

		alert("<%=JSAlertText(den(30))%>")
		return false
		break;
	default:
		return true
	}
}

//Funcion que comprueba el codigo de compañia
function ValidarCompania(txtcompania)
{
	if ((VALIDAR_CIF(txtcompania)) || (VALIDAR_NIF(txtcompania)))
	{
		return true
	}
	else
	{
		alert("<%=JSAlertText(den(39))%>")
		return false
	}
}
<%else%>
function ValidarNIFCIF()
{
return true
}
<%end if%>

function ValidarCaracteresValidos(cadena)
{
	varChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZáéíóúÁÉÍÓÚ0123456789";
	var isCorrect = true;
	var index = 0;
	while ((index < cadena.length) && (isCorrect))
     {
     isCorrect = (varChars.indexOf(cadena.charAt(index)) != -1);
     index ++;
     }
	if (!isCorrect)
     {	
		alert("<%=JSAlertText(den(39))%>")
		f.txtCod.focus();
		return false
     }
    return true;
}

function QuitarEspacioYTabulador(cadena)
{
	cadena = cadena.replace("	"," ")
	return trim (cadena)
}

function ValidarCodigoCia()
{
	f.txtCod.value=QuitarEspacioYTabulador(f.txtCod.value)
	
	var text = f.txtCod.value.toUpperCase()
		 	
	<%if paramgen.ComprobarCodCia=true then%>				
			if ((text.charCodeAt(0) =='69') && (text.charCodeAt(1) =='83'))			
			{					
				if (ValidarCaracteresValidos(f.txtCod.value))
				{
					if (ValidarCompania(f.txtCod.value.substr(2)))
						f.txtNIF.value = f.txtCod.value.substr(2);			
					else
						f.txtCod.focus();
				}
			}
			else
			{
				ValidarCaracteresValidos(f.txtCod.value);
			}
	<%end if%>
}
//-->
</script>
</head>
<body bgColor="#ffffff" LANGUAGE="javascript" onload="init()">
<h1><%=lblTitulo%></h1>
<h3><%=lblRellene%></h3>
<h4><%=lblOblig%></h4>
<form name="frmCompania">
<table class="principal" border="0" cellPadding="1" cellSpacing="2" width="95%">
	<tr>
		<td class="filaImpar">
			<%if UCase(application("NOMPORTAL"))=UCase("federlan") or UCase(application("NOMPORTAL"))=UCase("ormazabal") or UCase(application("NOMPORTAL"))=UCase("gestamp") or UCase(application("NOMPORTAL"))=UCase("yell") then%>
			<nobr><%=lblCod%>&nbsp;<a TABINDEX="-1" href="javascript:void(null);" onclick="helpWindow('nif_comunitario.asp')"><img border="0" SRC="../images/ico_help.gif" WIDTH="15" HEIGHT="15"></a></nobr>
			<%else%>
			<nobr><%=lblCod%>&nbsp;<a TABINDEX="-1" href="javascript:void(null);" onclick="helpWindow('cod_compania.asp')"><img border="0" SRC="../images/ico_help.gif" WIDTH="15" HEIGHT="15"></a></nobr>
			<%end if%>
		</td>
		<td class="filaImpar">
			<input type="text" onfocusout="ValidarCodigoCia()" id="txtCod" name="txtCod" size="14" maxLength="20" value="" autocomplete="off">

		</td>
	</tr>
	<tr>
		<td class="filaPar">
			<%=lblDen%>
		</td>
		<td class="filaPar">
			<input type="text" id="txtDen" name="txtDen" size="72" maxLength="100" value>
		</td>
	</tr>
	<tr>
		<td class="filaImpar">
			<%=lblDir%>
		</td>
		<td class="filaImpar">
			<input type="text" id="txtDir" name="txtDir" size="72" maxLength="255" value>
		</td>
	</tr>
	<tr>
		<td class="filaPar">
			<%=lblCP%>
		</td>
		<td class="filaPar">
			<input type="text" id="txtCP" name="txtCP" size="14" maxLength="20" value>
		</td>
	</tr>
	<tr>
		<td class="filaImpar">
			<%=lblPob%>
		</td>
		<td class="filaImpar">
			<input type="text" id="txtPob" name="txtPob" size="36" maxLength="100" value>
		</td>
	</tr>
	<tr>
		<td class="filaPar">
			<%=lblPai%>
		</td>
		<td class="filaPar">
			<select name="lstPai" size="1" onchange="obtenerProvi()">
				<option value="-1" selected></option>
			</select>
		</td>
	</tr>
	<tr>
		<td class="filaImpar">
			<label id="prov" name="prov"><%=lblProvi%></label>
		</td>
		<td class="filaImpar">
			<select name="lstProvi" size="1">
				<option value="-1" selected></option>
			</select>
		</td>
	</tr>
	<tr>
		<td class="filaPar">
			<%=lblMoneda%>
		</td>
		<td class="filaPar">
			<select name="lstMon" size="1">
				<option value="-1" selected></option>
			</select>
		</td>
	</tr>
	<tr>
		<td class="filaImpar">
			<%=lblNIF%>&nbsp;<a TABINDEX="-1" href="javascript:void(null);" onclick="helpWindow('nif.asp')"><img border="0" SRC="../images/ico_help.gif" WIDTH="15" HEIGHT="15"></a>
		</td>
		<td class="filaImpar">
			<input type="text" id="txtNIF" name="txtNIF" size="20" maxLength="20" onchange="return ValidarNIFCIF()">
		</td>
	</tr>
	<tr>
		<td class="filaPar">
			<%=lblVolFact%>
		</td>
		<td class="filaPar">
			<input language="javascript" onchange="return validarNumero(this,vdecimalfmt,vthousanfmt,vprecisionfmt,null,null,'<%=JSText(den(31))%>\n<%=JSText(den(32))%> ' + vdecimalfmt + '\n<%=JSText(den(33))%> ' + vthousanfmt);" onkeypress="return mascaraNumero(this,',','.',event);" ID="txtVolFac" NAME="txtVolFac" TYPE="text" SIZE="20" maxlength="20">
			<font size="1">&nbsp;
				<%=lblMEuros%>
			</font>
		</td>
	</tr>
	<tr>
		<td class="filaImpar">
	<%if UCase(application("NOMPORTAL")) = UCase("huf") then%>
			<nobr><%=lblClientesRef%>&nbsp;<a TABINDEX="-1" href="javascript:void(null);" onclick="helpWindow('clirefHUF.asp')"><img border="0" SRC="../images/ico_help.gif" WIDTH="15" HEIGHT="15"></a></nobr>
	<%else
	     if UCase(application("NOMPORTAL"))=UCase("gestamp") then%>
			<nobr><%=lblClientesRef%>&nbsp;<a TABINDEX="-1" href="javascript:void(null);" onclick="helpWindow('cliref_gestamp.asp')"><img border="0" SRC="../images/ico_help.gif" WIDTH="15" HEIGHT="15"></a></nobr>
		<%else%>
			<nobr><%=lblClientesRef%>&nbsp;<a TABINDEX="-1" href="javascript:void(null);" onclick="helpWindow('cliref.asp')"><img border="0" SRC="../images/ico_help.gif" WIDTH="15" HEIGHT="15"></a></nobr>
       <%end if%>
	<%end if%>
		</td>
		<td class="filaImpar">
			<input type="text" id="txtCliRef1" name="txtCliRef1" size="71" maxLength="50" value>
		</td>
	</tr>
	<tr>
		<td class="filaPar">&nbsp;</td>
		<td class="filaPar">
			<input type="text" id="txtCliRef2" name="txtCliRef2" size="71" maxLength="50" value>
		</td>
	</tr>
	<tr>
		<td class="filaImpar">
			&nbsp;</td>
		<td class="filaImpar">
			<input type="text" id="txtCliRef3" name="txtCliRef3" size="71" maxLength="50" value>
		</td>
	</tr>
	<tr>
		<td class="filaPar">
			&nbsp;</td>
		<td class="filaPar">
			<input type="text" id="txtCliRef4" name="txtCliRef4" size="71" maxLength="50" value>
		</td>
	</tr>
	<tr>
		<td class="filaImpar">
			<nobr><%=lblHom%>&nbsp;<a TABINDEX="-1" href="javascript:void(null);" onclick="helpWindow('homologaciones.asp')"><img border="0" SRC="../images/ico_help.gif" WIDTH="15" HEIGHT="15"></a></nobr>
		</td>
		<td class="filaImpar">
            <input type="text" id="txtHom1" name="txtHom1" size="20" maxLength="50" value>
            <input type="text" id="txtHom2" name="txtHom2" size="20" maxLength="50" value>
            <input type="text" id="txtHom3" name="txtHom3" size="21" maxLength="50" value>
		</td>
	</tr>
	<tr>
		<td class="filaPar">
			<%=lblURLCia%>
		</td>
		<td class="filaPar">
			<input type="text" id="txtURLCia" name="txtURLCia" size="51" maxLength="255" value>
		</td>
	</tr>
	<tr>
		<td class="filaImpar" valign="top">
		<%if UCase(application("NOMPORTAL"))=UCase("gestamp") then%>
			<nobr><%=lblComent%>&nbsp;<a TABINDEX="-1" href="javascript:void(null);" onclick="helpWindow('comentarios_gestamp.asp')"><img border="0" SRC="../images/ico_help.gif" WIDTH="15" HEIGHT="15"></a></nobr><br><br>
		<%else%>
			<nobr><%=lblComent%>&nbsp;<a TABINDEX="-1" href="javascript:void(null);" onclick="helpWindow(<%if paramGen.ActivarRegistroActiv then %>'comentariosRegActiv.asp'<%else%>'comentarios.asp'<%end if%>)"><img border="0" SRC="../images/ico_help.gif" WIDTH="15" HEIGHT="15"></a></nobr><br><br>
		<%end if%>
			<span class="fpequena"><%=lblActivi%></span>
		</td>
		<td class="filaImpar">
			<textarea id="txtComent" name="txtComent" rows="3" cols="71" style="width:95%;"></textarea>
		</td>
	</tr>
</table>
</form>
<form name="frmSiguiente" style="height:21px;" method="post" action=<%if paramGen.ActivarRegistroActiv then %>"actividadesCia.asp"<%else%>"usuario.asp"<%end if%> language="javascript" onsubmit="return validar()" target="_self">
<input type="hidden" name="Idioma" value="<%=Idioma%>">
<input type="hidden" name="codigo">
<input type="hidden" name="nif">
<input type="hidden" name="direccion">
<%if Application("FSAL")=1 then%>   
<input type="hidden" name="fechaIniFSAL7">
<input type="hidden" name="sIdRegistroFSAL7">
<%end if%>
<table width="95%">
	<tr>
		<td align="right">
			<input class="button" type="submit" name="cmdRegistrar" id="cmdRegistrar" value="<%=cmdRegistrar%> &gt;&gt;">
		</td>
	</tr>
</table>
</form>
</body>
<%
set paramgen =nothing
set oraiz=nothing
%>
<!--#include file="../common/fsal_2.asp"-->
</html>
