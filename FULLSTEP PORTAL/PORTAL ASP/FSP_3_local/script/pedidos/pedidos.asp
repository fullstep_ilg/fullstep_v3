﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<%
''' <summary>
''' Muestra los pedidos
''' </summary>
''' <remarks>Llamada desde: common\menu.asp     common\introformatos.asp ; Tiempo máximo: 0,2</remarks>

	Idioma = Request("Idioma")
	Idioma = trim(Idioma)	

	AccesoExterno=cbool(Application("ACCESO_SERVIDOR_EXTERNO"))    
    if AccesoExterno then		
        set oRaiz=validarUsuarioAccesoExterno(Idioma,true,true,0,request("txtCIA"),request("txtUSU"),request("CodigoSesion"))
    else
	    set oRaiz=validarUsuario(Idioma,true,true,0)
	end if	
		
	'Idiomas
	Idioma = Request("Idioma")
	Idioma = trim(Idioma)
	dim den
	den = devolverTextos(Idioma,31)		
	
	'Obtiene las compañías con pedidos
	DelProveedor = clng(oRaiz.Sesion.CiaId)
	set oCias = oraiz.DevolverCompaniasConPedidos (Idioma,DelProveedor,cint(Application("PORTAL")),clng(oRaiz.Sesion.CiaComp),Application("PYME"),oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"))

	set paramgen = oRaiz.Sesion.Parametros
	
	if paramGen.unaCompradora  and not oCias is nothing then
		set oCia = oCias.item(1)
		ciaComp=ocia.id
		ciaCod = oCia.cod
		ciaDen = ocia.den
		set ocia = nothing
		set oCias = nothing
		set paramgen = nothing
        
		set oRaiz = nothing
		
		response.redirect Application ("RUTASEGURA") & "script/pedidos/pedidos1.asp?Idioma=" & Idioma & "&CiaComp=" & Server.URLEncode(ciaComp) & "&CiaCod=" & Server.URLEncode(ciaCod) & "&CiaDen=" & Server.URLEncode(ciaDen)
		Response.End 
	end if
set paramgen = nothing

%>
<html>

<head>
<% 
    
%>
<title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta NAME="GENERATOR" Content="Microsoft FrontPage 4.0">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/menu.asp"></script>
<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init()
{
    document.getElementById('tablemenu').style.display = 'block';
}
</script>
</head>
<body topMargin=0 leftmargin=0 onload="init()">
<SCRIPT>dibujaMenu(4)</script>

<h1><% = Den (1)%></h1>

<%
bHayOrdenes = false
if not oCias is nothing then              
	for each oCia in oCias
					
		if oCia.NumOrdenNuevas > 0 then
			bHayOrdenes = true
		end if
	next
end if

if not bHayOrdenes then
%>

<p id=pError class=Error><%=den(7)%></P>
<p>&nbsp;</p>


<%else%>
	<h3><% = Den(2) %></h3>
	<h3><% = Den (3) %></h3>

	<div align="center">
	<table border="0" width="70%" cellspacing="2" cellpadding="1">
	  <tr>
	    <th width="40%" class="cabecera"><% = Den (4) %></th>
	    <th width="20%" class="cabecera"><% = Den (6) %></th>
	  </tr>
		  
	<%
		mclass="filaImpar"
		if not oCias is nothing then              
			for each oCia in oCias
					
				if oCia.NumOrdenNuevas > 0 then%>
					
				<tr>  
					<td class=<%=mclass%>>
						<A ID=option0 LANGUAGE=javascript onmousemove="window.status='<%=HTMLEncode(oCia.Den)%>'" onmouseout="window.status=''" href="<%=application("RUTASEGURA")%>script/pedidos/pedidos1.asp?Idioma=<%=Idioma%>&CiaComp=<%=Server.URLEncode(ocia.id)%>&CiaCod=<%=Server.URLEncode(ocia.Cod)%>&CiaDen=<%=Server.URLEncode(ocia.den)%>">
						<%=HTMLEncode(oCia.den)%>
						</A>
					</td>
					<td class=<%=mclass%> align="right">
						<%=ocia.NumOrdenNuevas%>
					</td>
				</tr>
						
				<%
				if mclass="filaImpar" then
					mclass="filaPar"
				else
					mclass="filaImpar" 
				end if
				end if
			next 
		end if
			
	  %>
		</table>
	</div>
<%end if%>

</body>
</html>

