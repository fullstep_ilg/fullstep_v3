﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->

<%

''' <summary>
''' Cargar las lineas de un pedido
''' </summary>
''' <remarks>Llamada desde: Nadie ; Tiempo máximo: 0,2</remarks>

		
Idioma = Request("Idioma")
CiaComp=request("CiaComp")


set oRaiz=validarUsuario(Idioma,false,false,0)


OrdenId = request("Orden")
	
set oOrden = oRaiz.Generar_COrden()

oOrden.id = clng(OrdenId)

set ador = oOrden.Devolverlineas(CiaComp,Idioma)


dim varEquiv
set adorEquiv=oRaiz.ObtenerMonedaCentral(CiaComp)
if not adorEquiv is nothing then
	varEquiv=adorEquiv("EQUIV").value
	adorEquiv.close
else
		set oRaiz = nothing
        
		%>
		<html>
		<body onload="window.open('<%=Application("RUTASEGURA")%>script/errormonedas.asp?Idioma=<%=Idioma%>','default_main')">
		</body>
		</html>
		<%
		Response.End 
end if
set adorEquiv=nothing

%>

<script>

p=window.parent.frames["fraPedidosClient"]

function init()
{
<%
if not ador is nothing then
	while not ador.eof
%>	
			p.anyadirLinea(<%=ordenId%>, <%=ador("lineaid").value%>, '<%=JSText(ador("codart").value)%>', '<%=JSText(ador("art_ext").value)%>', '<%=JSText(ador("denart").value)%>','<%=JSText(ador("DESTINO").value)%>','<%=JSText(ador("DESTDEN").value)%>','<%=JSText(ador("UNIDAD").value)%>','<%=JSText(ador("UNIDEN").value)%>', <%=JSNum(ador("PRECIOUNITARIO").value/varEquiv)%>,<%=JSNum(ador("CANTIDAD").value)%>, <%=ador("EST").value%>, <%=ador("ENTREGA_OBL").value%>,<%=JSDate(ador("FECENTREGA").value)%>,'<%=JSAlertText(ador("OBS").value)%>','<%=JSText(ador("CATEGORIA").value)%>', '<%=JSText(ador("CATEGORIARAMA").value)%>','<%=JSText(ador("GMN1"))%>',<%if not isnull (ador("ADJUNTOS").value) then %><%=ador("ADJUNTOS").value%><%else%>0<%end if%>,'<%=JSText(ador("VALOR1").value)%>','<%=JSText(ador("VALOR2").value)%>','<%=JSText(ador("DENCAMPO1").value)%>','<%=JSText(ador("DENCAMPO2").value)%>',<%=JSNum(ador("ANYOPROCE").value)%>,<%=JSNum(ador("CODPROCE").value)%>,'<%=JSText(ador("DENPROCE").value)%>','<%=JSAlertText(ador("OBSADJUN").value)%>')
<%	
	
		ador.movenext
	wend
	ador.close
	set ador = nothing
end if

set oRaiz=nothing


%>
p.mostrarOrdenes()
}
</SCRIPT>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
</HEAD>
<BODY onload="init()">




<P>&nbsp;</P>

</BODY>
</HTML>
