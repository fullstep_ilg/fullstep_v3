﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/colores.asp"-->


<%
''' <summary>
''' Rechaza un pedido
''' </summary>
''' <remarks>Llamada desde: pedidos\pedidos22.asp ; Tiempo máximo: 0,2</remarks>

	Idioma=Request("txtIdioma")
	Idioma = trim(Idioma)

	set oRaiz=validarUsuario(Idioma,true,false,0)
    
	
	'Obtiene los datos
	IdOrden=Request("txtIdOrden")
	coment=Request("txtObserv")
	IdPedido=Request("txtIdPedido")
	CiaComp =Request("txtCiaComp")
	CiaProv=clng(oRaiz.Sesion.CiaId)
	CiaComp =Request("txtCiaComp")
	Anyo =Request("txtAnyo")
	NumPedido =Request("txtNumPedido")
	NumOrden =Request("txtNumOrden")
	
	CodCiaProv=oRaiz.Sesion.CiaCodGs
	CodCiaComp=oRaiz.Sesion.CiaCod
		

	dim den
	den = devolverTextos(Idioma,36)	
	

	Dim TESError 
	dim SinError
	SinError = true
	'1-Comienza la transacción
	oraiz.ComenzarTransaccion()
	'2-Actualiza ORDEN_ENTREGA y ORDEN_EST
	set oOrden = oRaiz.Generar_COrden()
	oOrden.id = clng(IdOrden)
	oOrden.PedidoId=clng(IdPedido)
	Set TESError =oOrden.RechazarPorProveedor(CiaComp,coment)
	If TESError.NumError <> 0 Then
		oraiz.CancelarTransaccion()
		server.ScriptTimeout=timeout
		SinError = false
	end if
	
	if sinError then	
		oraiz.ConfirmarTransaccion()
	end if
	
	on error resume next

	'Notifica a los aprovisionadores
	lIdUsu = oRaiz.Sesion.UsuId 
	lCiaProve = oRaiz.Sesion.CiaId
	'Llamada a FSIS.
	oOrden.LlamarFSIS IdOrden, IdPedido, CiaComp
	
    if sinError then
        CodPortal = cstr(Application ("NOMPORTAL"))
	    oOrden.NotifRechazoAAprovisionador CiaComp,lCiaProve, lIdUsu, CodPortal
	
		response.redirect Application ("RUTASEGURA") & "script/pedidos/pedidorechazado.asp?Idioma=" & Idioma & "&CiaComp=" & CiaComp & "&CiaCod=" & CodCiaComp & "&Anyo=" & Anyo & "&NumPedido=" & NumPedido & "&NumOrden=" & NumOrden
	else
%>

 
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/menu.asp"></script>
<script SRC="../common/formatos.js"></script>

<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
</HEAD>


<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
    function init() {
        document.getElementById('tablemenu').style.display = 'block';
    }
</script>



<BODY onload="init()" topmargin="0" leftmargin="0">

<script>dibujaMenu(4)</script>

</body>

<P></P>

<h1><%=den(4)%></h1>

		<h3 class=error><%=den(2)%></h3>

		<script>
		if (top.winEspera)
		    top.winEspera.close()
		</script>

		</HTML>

<% 
	
	end if	
	server.ScriptTimeout=timeout
	set oPedido=nothing	
	
	set oRaiz = nothing
%>

</HTML>
