﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/fsal_1.asp"-->

<HTML>
<HEAD>
<% 
    Idioma = Request.Cookies("USU_IDIOMA")
    
%>
<title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">

<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/menu.asp"></script>
<script SRC="../common/formatos.js"></script>
<script src="../common/ajax.js"></script>
<!--#include file="../common/fsal_3.asp"-->

</HEAD>

<body bgcolor="#ffffff" topMargin=0 leftmargin=0 onload="init(); MarcarPedidoVisitado()">

<%

''' <summary>
''' Mostrar una lista de pedidos a rechazar
''' </summary>
''' <remarks>Llamada desde: pedidos\pedidos11.asp pedidos\pedidos11dir.asp pedidos\pedidos41.asp; Tiempo m�ximo: 0,2</remarks>    
	'Idiomas
	Idioma = Request("Idioma")
	Idioma = trim(Idioma)
	set oRaiz=validarUsuario(Idioma,true,true,0)
    
	
	'Obtiene los datos del pedido
	IdOrden=Request("Id")
	Anyo=Request("Anyo") 
	NumPedido=Request("pedido")
	NumOrden=Request("orden")
	CiaComp = Request("CiaComp")

	IdPedido=Request("IdPedido")
	Referencia=request("Referencia")
	Mon=request("Mon")
	Estado=4
	
	CiaProv=oRaiz.Sesion.CiaCodGs
	
	bAccesoFSSM=oRaiz.Sesion.Parametros.AccesoFSSM	
	
	dim den
	den = devolverTextos(Idioma,34)	

	decimalfmt=request.Cookies ("USU_DECIMALFMT")
	thousandfmt=Request.Cookies ("USU_THOUSANFMT")
	precisionfmt=cint(Request.Cookies ("USU_PRECISIONFMT"))
	datefmt=Request.Cookies("USU_DATEFMT") 
%>
    
<SCRIPT>    dibujaMenu(4)</SCRIPT>

<SCRIPT>
var requiereRefresco
requiereRefresco=true

var vdecimalfmt 
var vthousanfmt
var	vprecisionfmt
var vdatefmt

vdecimalfmt='<%=decimalfmt%>'
vthousanfmt='<%=thousandfmt%>'
vprecisionfmt='<%=precisionfmt%>'
vdatefmt='<%=datefmt%>'

function aplicarFormatos(vdec,vtho,vpre,vdat)
{
	var vdatetmp
	vdatetmp=vdatefmt
	vdecimalfmt=vdec
	vthousanfmt=vtho
	vprecisionfmt=vpre
	vdatefmt=vdat
	
	dibujarPage()
	if(vdatefmtold==vdatetmp)
	  {
		vdatefmtold=vdatetmp
	  }
}

var vdatefmtold
vdatefmtold=vdatefmt
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo m�ximo:0</remarks>*/
function init()
{
    if (<%=application("FSAL")%> == 1)
    {
        Ajax_FSALActualizar3(); //registro de acceso
    }
    document.getElementById('tablemenu').style.display = 'block';

	dibujarPage()
}

<%
	set oOrdenes = oRaiz.Generar_COrdenes()
	set adorOrden=oOrdenes.BuscarTodasOrdenes (CiaComp,Idioma,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"),0,null,null,null,null,null,null,null,null,null,null,null,null,null,null,1,,,,,,,,IdOrden)

    dImporteTotal=adorOrden("IMPORTE")*adorOrden("CAMBIO")
    if not isnull(adorOrden("COSTES")) then dCostes=adorOrden("COSTES")*adorOrden("CAMBIO")
    if not isnull(adorOrden("DESCUENTOS")) then dDescuentos=adorOrden("DESCUENTOS")*adorOrden("CAMBIO")

	bMostrarDirFact=false
	for each oField in adorOrden.fields
		if ucase(oField.name)="DIRFAC" then
			bMostrarDirFact=true
			exit for
		end if
	next	

	set oOrden = oRaiz.Generar_COrden()
	oOrden.id = clng(IdOrden)
	RefValor=adorOrden("REFERENCIA").value
	Importe=adorOrden("IMPORTE").value*adorOrden("CAMBIO").value
	Obs=adorOrden("OBS").value
	Petic=adorOrden("PER").value
	Mon=adorOrden("MON").value
	IdPedido=adorOrden.fields("PEDIDO").value
	if adorOrden.fields("RECEPTOR").value <> adorOrden.fields("PER").value then
		NotificarReceptor = 1
	else
		NotificarReceptor = 0
	end if	
	
	set ador = oOrden.Devolverlineas(CiaComp,Idioma)
    if not ador.eof then
	    while not ador.eof
            dImporteBrutoLin=ador("IMPORTE").value
            if not isnull(ador("COSTES")) then dCostesLin=ador("COSTES")*adorOrden("CAMBIO")
            if not isnull(ador("DESCUENTOS")) then dDescuentosLin=ador("DESCUENTOS")*adorOrden("CAMBIO")
            dImporteBruto=dImporteBruto+dImporteBrutoLin+dCostesLin-dDescuentosLin
            ador.movenext
        wend
    end if
    ador.movefirst
	set adorAtr = oOrden.DevolverAtributos(CiaComp)
    set oLineasDesgloseImpuesto = oRaiz.Generar_CLineasDesgImpuesto
    oLineasDesgloseImpuesto.cargarLineasDesgloseImpuestoOrden oOrden.id,CiaComp,Idioma
	sAdjuntosOrden=""
	set oAdjuntosOrden = oOrden.cargarAdjuntos(CiaComp,false)
	for each oAdjunto in oAdjuntosOrden
		sAdjuntosOrden=sAdjuntosOrden & oAdjunto.Nombre & " ; "
	next
	if sAdjuntosOrden<>"" then
		sAdjuntosOrden=mid(sAdjuntosOrden,1,len(sAdjuntosOrden)-3)
	end if
	
	Set oPersonas = oRaiz.Generar_CPersonas()
	oPersonas.CargarTodasLasPersonas CiaComp,adorOrden.fields("PER").value
	If Not oPersonas.Item(adorOrden.fields("PER").value) Is Nothing Then
		set oPersona = oPersonas.Item(adorOrden.fields("PER").value)
	else
		set oPersona=nothing
    End If
	oPersonas.CargarTodasLasPersonas CiaComp,adorOrden.fields("RECEPTOR").value
	If Not oPersonas.Item(adorOrden.fields("RECEPTOR").value) Is Nothing Then
		set oPersonaRec = oPersonas.Item(adorOrden.fields("RECEPTOR").value)
	else
		set oPersonaRec=nothing
    End If    
    set oPersonas=nothing
  
%>

function MarcarPedidoVisitado()
{
	<% 
	Dim oOrden
	set oOrden = oRaiz.Generar_COrden()
	oOrden.PedidoVisitado IdOrden, CiaComp
    %>
}

/*''' <summary>
''' Comprueba q se haya pasado toda la informaci�n necesaria para rechazar un pedido
''' </summary>      
''' <returns>Si pasa o no las validaciones</returns>
''' <remarks>Llamada desde: cmdAceptar.onClick() ; Tiempo m�ximo: 0</remarks>*/
function validar()
{
	var f
	f=document.forms["frmAceptar"]

	if (f.txtObserv.value=="")
		{
		alert("<%=JSAlertText(den(17))%>")
		return false
		}

<%
	If bAccesoFSSM Then
		Response.Write "if (!confirm('" & JSAlertText(den(29)) & "\n" & JSAlertText(den(30)) & "')) return false;"
	End If
%>
	top.winEspera = window.open ("<%=Application("RUTASEGURA")%>script/pedidos/rechazandopedido.asp?Idioma=<%=Idioma%>", "_blank", "top=200,left=360,width=450,height=150,location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars=no")
//	while (!top.winEspera.document.images["cursor-espera_grande"])
//		{
//		}

}

function DenominacionDest(dest,LineaPedido)
{

	winDest=window.open("<%=application("RUTASEGURA")%>script/common/DenominacionDest.asp?Idioma=<%=Idioma%>&Dest=" + Var2Param(dest) + "&LineaPedido=" + Var2Param(LineaPedido),"_blank","width=400,height=200,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no scrollbars=no")
	winDest.focus()
}

function DenominacionUP(uni)
{
	winUP=window.open("<%=application("RUTASEGURA")%>script/common/DenominacionUP.asp?Idioma=<%=Idioma%>&Cod=" + Var2Param(uni) ,"_blank","width=400,height=130,location=no,menubar=no,toolbar=no,resizable=no,addressbar=no scrollbars=yes")
	winUP.focus()
}

function MostrarPlanesEntrega(IdLinea,CodArt,DenArt,Uni)
{    
	winPE=window.open("<%=application("RUTASEGURA")%>script/common/PlanesEntrega.asp?Idioma=<%=Idioma%>&Anyo=<%=Anyo%>&Pedido=<%=NumPedido%>&Orden=<%=NumOrden%>&IdLinea=" + 
        IdLinea + "&CodArt=" + Var2Param(CodArt) + "&DenArt=" + Var2Param(DenArt) + "&Uni="+ Var2Param(Uni),"_blank","location=no,menubar=no,width=400,height=200,toolbar=no,resizable=yes,addressbar=no")        
	winPE.focus()      
}

function MostrarCostesDescuentos(Ambito,Id,moneda,cambio,ImporteBruto,ImporteCostes,ImporteDescuentos,ImporteNeto)
{        
    winPE=window.open("<%=application("RUTASEGURA")%>script/common/CostesDescuentos.asp?Idioma=<%=Idioma%>&Ambito=" + Ambito + "&Id=" + Id + "&Moneda=" + Var2Param(moneda) +
        "&Cambio=" + cambio + "&ImporteBruto=" + ImporteBruto + "&ImporteCostes=" + ImporteCostes + "&ImporteDescuentos=" + ImporteDescuentos + 
        "&ImporteNeto=" + ImporteNeto,"_blank","location=no,menubar=no,width=400,height=250,toolbar=no,resizable=yes,addressbar=no")        
	winPE.focus()   
}
function MostrarDetalleImpuesto(IdOrden,ImpuestoDen,ImpuestoValor,ImpuestoMoneda,Cuota)
{   
	winPE=window.open("<%=application("RUTASEGURA")%>script/common/DetalleImpuesto.asp?Idioma=<%=Idioma%>&IdOrden=" + IdOrden + "&ImpDen=" + ImpuestoDen + "&ImpVal=" + ImpuestoValor + "&ImpMon=" + ImpuestoMoneda + "&cuota=" + Cuota ,
                    "_blank","location=no,menubar=no,width=400,height=200,toolbar=no,resizable=yes,addressbar=no")        
	winPE.focus()      
}

<%
	set oOrdenes = oRaiz.Generar_COrdenes()
	set adorOrden=oOrdenes.BuscarTodasOrdenes (CiaComp,Idioma,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"),Estado,null,null,null,null,null,null,null,null,CiaProv,null,null,anyo,NumPedido,NumOrden)
	CambioPedido=adorOrden("CAMBIO").value
	set oOrden = oRaiz.Generar_COrden()
	oOrden.id = clng(IdOrden)

	set ador = oOrden.Devolverlineas(CiaComp,Idioma)
	
    'Ocultar columna fecha de entrega
    iNumLinPlanEnt=0
    iNumLinPlanEntPub=0
    bOcultarFecEntrega=false
    if not ador is nothing then
        if ador.recordcount>0 then
            while not ador.eof
                if ador("HAYPLANES")>0 then
                    iNumLinPlanEnt=iNumLinPlanEnt+1
                    if ador("PUB_PLAN_ENTREGA")=1 then iNumLinPlanEntPub=iNumLinPlanEntPub+1
                end if
                ador.movenext
            wend
        end if
        ador.movefirst

        if (iNumLinPlanEnt=ador.recordcount) and iNumLinPlanEntPub=0 then
            bOcultarFecEntrega=true
        end if
    end if      

	Set oPersonas = oRaiz.Generar_CPersonas()

	oPersonas.CargarTodasLasPersonas CiaComp,adorOrden.fields("PER").value
	If Not oPersonas.Item(adorOrden.fields("PER").value) Is Nothing Then
		set oPersona = oPersonas.Item(adorOrden.fields("PER").value)
	else
		set oPersona=nothing
    End If
    set oPersonas=nothing
%>


function dibujarPage()
{
var str
var i

str=""
str+="<TABLE border=0 width=80% cellspacing=2 cellpadding=1>"

//IMPORTES
//Importes totales
str+="	<TR><TD width=35% class=cabecera colspan='4'><%=Den(34)%></TD></TR>"
//Importe bruto
str+="	<TR>"
str+="	<TD width=35% class=cabecera><%=Den(40)%></TD>"
str+="  <TD class=filaImpar align='right'>"
str+="<TABLE width=30%><TR>"
str+="<TD width=65% style='text-align:right;'>" + num2str(<%=replace(dImporteBruto,",",".")%> ,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</TD>"           
str+="<TD width=20% style='text-align:left;'><%=JSText(adorOrden.fields("MON").value)%></TD>"
<%if adorOrden("HAYCOSTES")>0 or adorOrden("HAYDESCUENTOS")>0 then%>  
    str+="<td width=15%></td>" 
<%end if%>
str+="</td></TD></TR></TABLE>"
str+="</TD>"
str+="  <TD class=filaImpar colspan='2'></TD>"
str+="	</TR>"
//Costes
str+="	<TR>"
str+="	<TD width=25% class=cabecera><%=Den(37)%></TD>"
str+="  <TD class=filaImpar align='right'>"
str+="<TABLE width=30%><TR>"
str+="<TD width=65% style='text-align:right;'>" + num2str(<%=replace(dCostes,",",".")%> ,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</TD>"           
str+="<TD width=20% style='text-align:left;'><%=JSText(adorOrden.fields("MON").value)%></TD>" 
<%if adorOrden("HAYCOSTES")>0 or adorOrden("HAYDESCUENTOS")>0 then%>
    str+="<TD width=15% style='text-align:left;'>"
    <%if adorOrden("HAYCOSTES")>0 then%>    
        str+="<a href='javascript:void(null)' onclick='MostrarCostesDescuentos(0," + "<%=adorOrden("ID").value%>" + ",\"" + "<%=adorOrden.fields("MON").value%>" 
        str+="\"," + "<%=replace(adorOrden.fields("CAMBIO").value,",",".")%>" + "," + "<%=replace(dImporteBruto,",",".")%>" + "," 
        str+="<%=replace(dCostes,",",".")%>" + "," + "<%=replace(dDescuentos,",",".")%>" + "," + "<%=replace(dImporteTotal,",",".")%>" + ")' title='<%=Den(39)%>'><IMG border=0 SRC='../images/masinform.gif'></a>"     
    <%end if%>
    str+="</TD>" 
<%end if%>   
str+="</TR></TABLE>"
str+="  </TD>"
str+="  <TD class=filaImpar colspan='2'></TD>"
str+="	</TR>"
//Descuentos
str+="	<TR>"
str+="	<TD width=25% class=cabecera><%=Den(38)%></TD>"
str+="  <TD class=filaImpar align='right'>"
str+="<TABLE width=30%><TR>"
str+="<TD width=65% style='text-align:right;'>" + num2str(<%=replace(dDescuentos,",",".")%> ,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</TD>"
str+="<TD width=20% style='text-align:left;'><%=JSText(adorOrden.fields("MON").value)%></TD>"
<%if adorOrden("HAYCOSTES")>0 or adorOrden("HAYDESCUENTOS")>0 then%>    
    str+="<TD width=15% style='text-align:left;'>"
    <%if adorOrden("HAYDESCUENTOS")>0 then%>    
        str+="<a href='javascript:void(null)' onclick='MostrarCostesDescuentos(0," + "<%=adorOrden("ID").value%>" + ",\"" + "<%=adorOrden.fields("MON").value%>" 
        str+="\"," + "<%=replace(adorOrden.fields("CAMBIO").value,",",".")%>" + "," + "<%=replace(dImporteBruto,",",".")%>" + "," 
        str+="<%=replace(dCostes,",",".")%>" + "," + "<%=replace(dDescuentos,",",".")%>" + "," + "<%=replace(dImporteTotal,",",".")%>" + ")' title='<%=Den(39)%>'><IMG border=0 SRC='../images/masinform.gif'></a>"     
    <%end if%>
    str+="</TD>"
<%end if%>
str+="</TR></TABLE>"
str+="</TD>"
str+="  <TD class=filaImpar colspan='2'></TD>"
str+="	</TR>"
//Impuestos
<%for each oLineaDesgloseImpuesto in oLineasDesgloseImpuesto%>
str+="	<TR>"
str+="      <TD width=25% class=cabecera><%=Den(41)%>&nbsp;<%=oLineaDesgloseImpuesto.tipoimpuesto%>&nbsp;<%=oLineaDesgloseImpuesto.TipoValor%>%&nbsp;"
str+=           "<a href='javascript:void(null)' onclick='MostrarDetalleImpuesto(" + "<%=adorOrden("ID").value%>" +  ",\"" +"<%=oLineaDesgloseImpuesto.tipoimpuesto%>" + "\"," +"<%=oLineaDesgloseImpuesto.TipoValor%>" + ",\"" +"<%=adorOrden.fields("MON").value%>" + "\"," +"<%=replace(FormatNumber(oLineaDesgloseImpuesto.cuota,2),",",".")%>" +")' "
str+="          style='color:white; text-decoration:underline;' title='<%=oLineaDesgloseImpuesto.tipoimpuesto%>&nbsp;<%=oLineaDesgloseImpuesto.TipoValor%>%'>"
str+="          (<%=Den(42)%>:&nbsp;<%=replace(FormatNumber(oLineaDesgloseImpuesto.BaseImponible,2),",",".")%>&nbsp;<%=adorOrden.fields("MON").value%>)</a></TD>"
str+="      <TD class=filaImpar align='right'>"
str+="          <TABLE width=30%><TR>"
str+="              <TD width=65% style='text-align:right;'><%=replace(FormatNumber(oLineaDesgloseImpuesto.cuota,2),",",".")%></TD>"
str+="              <TD width=20% style='text-align:left;'><%=JSText(adorOrden.fields("MON").value)%></TD>"
str+="              <TD width=15% style='text-align:left;'>"
str+=                   "<a href='javascript:void(null)' onclick='MostrarDetalleImpuesto(" + "<%=adorOrden("ID").value%>" +  ",\"" +"<%=oLineaDesgloseImpuesto.tipoimpuesto%>" + "\"," +"<%=oLineaDesgloseImpuesto.TipoValor%>" + ",\"" +"<%=adorOrden.fields("MON").value%>" + "\"," +"<%=replace(FormatNumber(oLineaDesgloseImpuesto.cuota,2),",",".")%>" + ")' "
str+="                   title='<%=oLineaDesgloseImpuesto.tipoimpuesto%>&nbsp;<%=oLineaDesgloseImpuesto.TipoValor%>%'><IMG border=0 SRC='../images/masinform.gif'></a>" 
str+="          </TD></TR></TABLE>"
str+="      </TD>"
str+="  <TD class=filaImpar colspan='2'></TD>"
str+="	</TR>"
<% next%>
//Importe total
str+="	<TR>"
str+="	<TD width=25% class=cabecera><%=UCase(Den(36))%></TD>"
str+="  <TD class=filaImpar align='right'>"
str+="<TABLE width=30%><TR>"
str+="<TD width=65% style='text-align:right;'>"+ num2str(<%=replace(dImporteTotal,",",".")%>,vdecimalfmt,vthousanfmt,vprecisionfmt) +"</TD>"    
str+="<TD width=20% style='text-align:left;'><%=JSText(adorOrden.fields("MON").value)%></TD>" 
<%if adorOrden("HAYCOSTES")>0 or adorOrden("HAYDESCUENTOS")>0 then%> 
    str+="<td width=15%></td>" 
<%end if%>
str+="</TD></TR></TABLE>"
str+="</TD>"
str+="  <TD class=filaImpar colspan='2'></TD>"
str+="	</TR>"

//DATOS GENERALES
//n� pedido y F.Emisi�n
str+="	<TR><TD width=25% class=cabecera colspan='4'><%=Den(33)%></TD></TR>"
str+="	<TR>"
str+="      <TD width=25% class=cabecera><%=Den(26)%></TD>"

str+="      <TD colspan=<%if Referencia<>"" then%>3<%else%>2<%end if%> class=cabecera><%=JSText(adorOrden.fields("DENEMPRESA").value)%></TD>"
str+="	</TR>"
<%if bMostrarDirFact then%>
	str+="	<TR>"
	str+="      <TD width=25% class=cabecera><%=den(31)%></TD>"
	str+="      <TD colspan=<%if Referencia<>"" then%>3<%else%>2<%end if%> class=cabecera><%=adorOrden("DIRFAC")%></TD>"
	str+="	</TR>"
<%end if%>
str+="	<TR>"
str+="      <TD width=25% class=cabecera><%=Den(2)%></TD>"
<%if Referencia<>"" then%>
	str+="		<TD width=50% class=cabecera><%=Referencia%></TD>"
<%end if%>
str+="      <TD width=25% class=cabecera><%=Den(3)%></TD>"
str+="	</TR>"

str+="	<TR>"
str+="		<TD class=filaImpar><%=adorOrden("anyo").value%> / <%=adorOrden("numpedido").value%> / <%=adorOrden("numorden").value%> </TD>"
<%if Referencia<>"" then%>
	str+="		<TD class=filaImpar><div style='width:100px;height:auto;overflow:hidden'><%=JSText(adorOrden.fields("REFERENCIA").value)%></DIV></TD>"
<%end if%>
<%if isnull(adorOrden("FECHA").value) then%>
	str+="		<TD class=filaImpar>&nbsp;</TD>"
<%else%>
	str+="		<TD class=filaImpar>" + date2str(<%=JSDate(adorOrden("FECHA").value)%>,vdatefmt) + "</TD>"
<%end if%>
str+="	</TR>"

<%
if not adorAtr is nothing then
	while not adorAtr.eof
%>
	//------------------------------------------------------------------------------------
	//jpa Solo saldran las lineas cuando exista valor
	//solo si NO  es blanco
	var esblanco
	esblanco = false
	<%if adorAtr("TIPO").value=1 then%> //texto
		<%if  isnull(adorAtr("VALOR_TEXT").value) then%>
			<%if  JSText(adorAtr("VALOR_TEXT").value) = "" then%>
				esblanco=true
			<%end if%>
		<%end if%>		
	<%else%>		  
		<%if adorAtr("TIPO").value=2 then%> //NUMERO
			<%if  isnull(adorAtr("VALOR_NUM").value) then%>
			  esblanco=true
			<%end if%>
		<%else%>
			<%if adorAtr("TIPO").value=3 then%> //NUMERO
				<%if  isnull(adorAtr("VALOR_FEC").value) then%>
				  esblanco=true
				<%end if%>
			<%else%>
				<%if adorAtr("TIPO").value=4 then%>
					<%if  isnull(adorAtr("VALOR_BOOL").value) then%>
					  esblanco=true
					<%end if%>
				<%end if%>
			<%end if%>
		<%end if%>
	<%end if%>			  
	if (esblanco==false)
	{
		str+="  <TR>"
		str+="		<TD width=25% class=cabecera><%=JSText(adorAtr("DEN").value)%></td>"
		<%if adorAtr("TIPO").value=1 then%> //texto
		   str+="      <TD class=filaImpar><%=JSText(adorAtr("VALOR_TEXT").value)%></td>"
		<%end if%>
		<%if adorAtr("TIPO").value=2 then%> //NUMERO
		   str+="      <TD class=filaImpar>" + num2str(<%=JSNum(adorAtr("VALOR_NUM").value)%>,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</TD>"
		<%end if%>
		<%if adorAtr("TIPO").value=3 then%>
		   str+="      <TD class=filaImpar>" + date2str(<%=JSDate(adorAtr("VALOR_FEC").value)%>,vdatefmt) + "</TD>"
		<%end if%>
		<%if adorAtr("TIPO").value=4 then%>
		  <%if adorAtr("VALOR_BOOL").value=0 then%>
		    str+="      <TD class=filaImpar><%=JSText(den(28))%></td>"  
		  <%else%>
		    str+="      <TD class=filaImpar><%=JSText(den(27))%></td>"  
		  <%end if%>
		<%end if%>
		str += "	</TR>"		
	}
	<%adorAtr.movenext
	wend
	 end if%>
  
//Observaciones del aprovisionador
<%if not isnull(adorOrden("OBS").value) then%>
str+="	<TR>" 
str+="	  <table border=0 width=80% cellspacing=1 cellpadding=1>"
str+="		<TR>"
str+="			<TD colspan=4 class=cabecera><%=den(7)%></TD>"
str+="		</TR>"
str+="		<TR bgcolor=white>"
str+="			<TD><%=JSText(adorOrden("OBS").value)%></TD>"
str+="		</TR>"
str+="	  </table>"
str+="	</TR>" 
<%end if%>

<%if not sAdjuntosOrden="" then%>
str+="	<TR>" 
str+="	  <table border=0 width=80% cellspacing=1 cellpadding=1>"
str+="		<TR>"
str+="			<TD colspan=4 class=cabecera><%=den(44)%>"
str+="          <a href='javascript:void(null)' onclick='verAdjuntosOrden(" + "<%=IdOrden%>" + ")'><img border = 0 src= '../images/clip.gif'></A>"
str+="		</TD>"
str+="		</TR>"
str+="		<TR bgcolor=white>"
str+="			<TD><%=JSText(sAdjuntosOrden)%></TD>"
str+="		</TR>"
str+="	  </table>"
str+="	</TR>" 
<%end if%>

//Datos del peticionario
<%If not oPersona Is Nothing then%>
str+="	<TR>" 
str+="	  <table border=0 width=80% cellspacing=1 cellpadding=1>"
str+="		<TR>"
str+="			<TD colspan=4 class=cabecera><%=den(19)%></TD>"
str+="		</TR>"
str+="		<TR bgcolor=white>"
str+="			<TD width=15% class=cabecera><%=den(20)%></TD>"
str+="			<TD width=35%><%=JSText(opersona.Nombre)%> &nbsp; <%=JSText(opersona.Apellidos)%></TD>"
str+="			<TD width=15% class=cabecera><%=den(22)%></TD>"
str+="			<TD width=35%><%=JSText(opersona.Tfno)%></TD>"
str+="		</TR>"
str+="		<TR bgcolor=white>"
str+="			<TD width=15% class=cabecera><%=den(21)%></TD>"
str+="			<TD><%=JSText(opersona.Fax)%></TD>"
str+="			<TD width=15% class=cabecera><%=den(23)%></TD>"
str+="			<TD><%=JSText(opersona.Email)%></TD>"
str+="		</TR>"
str+="	  </table>"
str+="	</TR>" 
<%end if%>

//Observaciones
str+="	<TR>"
str+="	  <table border=0 width=80% cellspacing=1 cellpadding=1>"
str+="		<TR>"
str+="			<TD colspan=3 class=cabecera><%=den(7)%></TD>"
str+="		</TR>"
str+="		<TR bgcolor=white>"
str+="			<TD align=right>"
str += textArea("txtObserv", "500px", 90,255,"<%=JSText(den(25))%>",null,null)
str+="			</TD>"
str+="		</TR>"
str+="	  </table>"
str+="	</TR>"

str+="	<TR>" 
str+="	  <table border=0 width=80% cellspacing=1 cellpadding=1>" 
str+="		<TR>" 
str+="			<td width=3% class=cabecera><% = Den (43) %></td>"   //Num. de linea de pedido
str+="			<td width=10% class=cabecera><% = Den (8) %></td>"   //Cod.art(comp)
str+="			<td width=10% class=cabecera><% = Den (9) %></td>"   //Cod.art(prov)
str+="			<td width=25% class=cabecera><% = Den (10) %></td>"  //Denominaci�n
str+="			<td width=7% class=cabecera><% = Den (11) %></td>"   //Dest.
<%if not bOcultarFecEntrega then %>
    str+="			<td width=13% class=cabecera><% = Den (13) %></td>" //F.Entrega
    str+="			<td width=11% class=cabecera><% = Den (14) %></td>" //Cantidad
    str+="			<td width=7% class=cabecera><% = Den (12) %></td>"  //UP
    str+="			<td width=11% class=cabecera><% = Den (15) %></td>" //PU
    str+="			<td width=7% class=cabecera><% = Den (37) %> (<%=JSText(adorOrden.fields("MON").value)%>)</td>" //Costes
    str+="			<td width=7% class=cabecera><% = Den (38) %> (<%=JSText(adorOrden.fields("MON").value)%>)</td>" //Descuentos
    str+="			<td width=11% class=cabecera><% = Den (18) %> (<%=Mon%>)</td>" //Importe
    <%if not (ador.recordcount=iNumLinPlanEntPub and iNumLinPlanEnt=iNumLinPlanEntPub) then%>
        str+="			<td width=13% class=cabecera><% = Den (13) %></td>" //F.Entrega
    <%end if%>
<%else%>
    str+="			<td width=17% class=cabecera><% = Den (14) %></td>" //Cantidad
    str+="			<td width=13% class=cabecera><% = Den (12) %></td>"  //UP
    str+="			<td width=17% class=cabecera><% = Den (15) %></td>" //PU
    str+="			<td width=7% class=cabecera><% = Den (37) %> (<%=JSText(adorOrden.fields("MON").value)%>)</td>" //Costes
    str+="			<td width=7% class=cabecera><% = Den (38) %> (<%=JSText(adorOrden.fields("MON").value)%>)</td>" //Descuentos
    str+="			<td width=170% class=cabecera><% = Den (18) %> (<%=Mon%>)</td>" //Importe
<%end if%>
str+="		</TR>"

<%
mclass="filaImpar"
i=0
if not ador.eof then
	while not ador.eof
        dImporteBrutoLin=ador("IMPORTE").value
        if not isnull(ador("COSTES")) then dCostesLin=ador("COSTES")*adorOrden("CAMBIO")
        if not isnull(ador("DESCUENTOS")) then dDescuentosLin=ador("DESCUENTOS")*adorOrden("CAMBIO")
        dImporteTotalLin=dImporteBrutoLin+dCostesLin-dDescuentosLin
%>

<%
    numLineaPedidoTmp=ador("NUMLINEAPEDIDO").value
    if numLineaPedidoTmp < 10 then
        numLineaPedido = "00" & numLineaPedidoTmp
    elseif numLineaPedidoTmp >= 100 then
        numLineaPedido = numLineaPedidoTmp
    else
        numLineaPedido = "0" & numLineaPedidoTmp
    end if
%>

str+="		<TR>"
str+="			<td class=<%=mclass%>><%=numLineaPedido%></td>"
str+="			<td class=<%=mclass%>><%=JSText(ador("codart").value)%></td>"
str+="			<td class=<%=mclass%>><%=JSText(ador("art_ext").value)%></td>"
str+="			<td class=<%=mclass%>><%=JSText(ador("denart").value)%></td>"

str += "       <TD class=<%=mclass%>><div style='height:auto;overflow:hidden'><table width=100%><tr><td width=100%><a href='javascript:void(null)' onclick='DenominacionDest(\"" + "<%=JSText(ador("DESTINO").value)%>" + "\",\"" + "<%=ador("LINEAID").value%>" + "\")' title='<%=JSText(ador("DESTDEN").value)%>'><%=JSText(ador("DESTINO").value)%></a></td>"
str += "       <td><a href='javascript:void(null)' onclick='DenominacionDest(\"" + "<%=JSText(ador("DESTINO").value)%>" + "\",\"" + "<%=ador("LINEAID").value%>" + "\")'  title='<%=JSText(ador("DESTDEN").value)%>'><IMG border=0 SRC='../images/masinform.gif'></a></td></tr></table></div></TD>"

//Planes de entrega � Fecha de entrega
<%if not bOcultarFecEntrega then %>
    <%if ador("HAYPLANES")>0 then%>
        <%if ador("PUB_PLAN_ENTREGA")=1 then%>
            str+="			<td class=<%=mclass%>><a href='javascript:void(null)' onclick='MostrarPlanesEntrega(" + "<%=ador("LINEAID").value%>" + ",\"" + "<%=ador("CODART").value%>" + "\",\"" + "<%=JSText(ador("DENART").value)%>" + "\",\"" + "<%=ador("UNIDAD").value%>" + "\")'><%=Den(32)%></a></td>"	           
        <%else%>
            str+="			<td class=<%=mclass%>>&nbsp;</td>"	
        <%end if%>
    <%else%>
        <%if isnull(ador("FECENTREGA")) then%>
	        str+="			<td align='left' class=<%=mclass%>>&nbsp;</td>"	
        <%else%>
	        str+="			<td align='left' class=<%=mclass%>>" + date2str(str2date('<%=VisualizacionFecha(ador("FECENTREGA").value,datefmt)%>',vdatefmtold),vdatefmt) + "</td>"	
        <%end if%>
    <%end if%>
<%end if%>
<%if ador("TIPORECEPCION").value=1 then %>
    str+="			<td class=<%=mclass%> align=right>&nbsp;</td>"	 
<%else %>  
    str+="			<td class=<%=mclass%> align=right>" + num2str(<%=replace(ador("CANTIDAD").value,",",".")%>,vdecimalfmt,vthousanfmt,vprecisionfmt) +"</td>"	
<%end if %>
str += "       <TD class=<%=mclass%>><table width=100%><tr><td width=100%><a href='javascript:void(null)' onclick='DenominacionUP(\"" + "<%=JSText(ador("UNIDAD").value)%>" + "\")' title='<%=JSText(ador("UNIDEN").value)%>'><NOBR><%=JSText(ador("UNIDAD").value)%></NOBR></a></td>"
str += "       <td><a href='javascript:void(null)' onclick='DenominacionUP(\"" + "<%=JSText(ador("UNIDAD").value)%>" + "\")' title='<%=JSText(ador("UNIDEN").value)%>'><IMG border=0 SRC='../images/masinform.gif'></a></td></tr></table>"
str += "</TD>"
<%if ador("TIPORECEPCION").value=1 then %>
    str+="			<td class=<%=mclass%> align=right>&nbsp;</td>"	 
<%else %> 
    str+="			<td class=<%=mclass%> align=right>" + num2str((<%=replace(ador("PRECIOUNITARIO").value ,",",".")%>) ,vdecimalfmt,vthousanfmt,vprecisionfmt) +"</td>"	
<%end if %>
//Importes de línea
str+="			<td class=<%=mclass%> align=right><table><tr><td>" + num2str((<%=replace(dCostesLin,",",".")%>) ,vdecimalfmt,vthousanfmt,vprecisionfmt) +"</td>"
<%if ador("HAYCOSTES")>0 then%>
    str+="<td><a href='javascript:void(null)' onclick='MostrarCostesDescuentos(1," + "<%=ador("LINEAID").value%>" + ",\"" + "<%=adorOrden.fields("MON").value%>" + "\"," 
    str+="<%=replace(CambioPedido,",",".")%>" + "," + "<%=replace(dImporteBrutoLin,",",".")%>" + "," + "<%=replace(dCostesLin,",",".")%>" + "," + "<%=dDescuentosLin%>" + "," 
    str+="<%=replace(dImporteTotalLin,",",".")%>" + ")' title='<%=Den(38)%>'><IMG border=0 SRC='../images/masinform.gif'></a></td>"     
<%end if%>	
str+="			</tr></table></td>"	
str+="			<td class=<%=mclass%> align=right><table><tr><td>" + num2str((<%=replace(dDescuentosLin,",",".")%>) ,vdecimalfmt,vthousanfmt,vprecisionfmt) +"</td>"
<%if ador("HAYDESCUENTOS")>0 then%>
    str+="<td><a href='javascript:void(null)' onclick='MostrarCostesDescuentos(1," + "<%=ador("LINEAID").value%>" + ",\"" + "<%=adorOrden.fields("MON").value%>" + "\"," 
    str+="<%=replace(CambioPedido,",",".")%>" + "," + "<%=replace(dImporteBrutoLin,",",".")%>" + "," + "<%=replace(dCostesLin,",",".")%>" + "," + "<%=dDescuentosLin%>" + "," 
    str+="<%=replace(dImporteTotalLin,",",".")%>" + ")' title='<%=Den(38)%>'><IMG border=0 SRC='../images/masinform.gif'></a></td>"     
<%end if%>	
str+="			</tr></table></td>"	
str+="			<td class=<%=mclass%> align=right>" + num2str((<%=replace(dImporteTotalLin,",",".")%>) ,vdecimalfmt,vthousanfmt,vprecisionfmt) +"</td>"

str+="<TD><input TYPE=hidden id='Linea" + <%=i%> + "' NAME='Linea" + <%=i%> + "' VALUE=<%=ador("LINEAID").value%>></TD>"

str+="		</TR>"		

//den(29) CASCA para ("ANYOPROCE")
//he puesto 9  para ("ANYOPROCE")
// Ahora muestra el proceso,las observaciones y los campos personalizados
<%set adorAtrL = oOrden.DevolverAtributosLinea(ador("LINEAID").value,CiaComp)
  if not (adorAtrL is nothing) or not isnull(ador("CODPROCE").value) or not isnull(ador("OBS").value)  or not isnull(ador("DENCAMPO1").value)  or not isnull(ador("DENCAMPO2").value) then %>
	  str += "	<TR><TD>&nbsp;</TD><TD colspan=11><TABLE width='100%' CELLSPACING=0>"

	  <%if not isnull(ador("CODPROCE").value) then %>
		str += "	<TR>"
		str += "	<TD class='tablasubrayadaright'><div style='width:100px;height:auto;overflow:hidden'><%=den(24)%></div></TD>"
		str += "	<TD class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'>&nbsp;<%=JSText(ador("ANYOPROCE").value)%>/<%=JSText(ador("GMN1").value)%>/<%=JSText(ador("CODPROCE").value)%> &nbsp; <%=JSText(ador("DENPROCE").value)%></div></TD>"
		str += "	</TR>"
	  <%end if%>
	
	  str += "	<TR>"
	  str += "	<TD class='tablasubrayadaright'><div style='width:100px;height:auto;overflow:hidden'><%=den(7)%></div></TD>"
	  str += "	<TD class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'>&nbsp;<%=JSText(ador("OBS").value)%></div></TD>"
	  str += "	</TR>"
	
	  <%if not isnull(ador("DENCAMPO1").value) then %>
		  str += "	<TR>"
		  str += "	<TD class='tablasubrayadaright'><div style='width:100px;height:auto;overflow:hidden'><%=JSText(ador("DENCAMPO1").value)%></div></TD>"
		  str += "	<TD class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'>&nbsp;<%=JSText(ador("VALOR1").value)%></div></TD>"
		  str += "	</TR>"
	  <%end if%>
	
	  <%if not isnull(ador("DENCAMPO2").value) then %>
		  str += "	<TR>"
		  str += "	<TD class='tablasubrayadaright'><div style='width:100px;height:auto;overflow:hidden'><%=JSText(ador("DENCAMPO2").value)%></div></TD>"
		  str += "	<TD class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'>&nbsp;<%=JSText(ador("VALOR2").value)%></div></TD>"
		  str += "	</TR>"
	  <%end if%>

	  <%if not  (adorAtrL is nothing) then 
	  while  not adorAtrL.EOF%>
		//------------------------------------------------------------------------------------
		//jpa Solo saldran las lineas cuando exista valor
		//solo si NO  es blanco
			var esblanco
			esblanco = false
			<%if adorAtrL("TIPO").value=1 then%> //texto
				<%if  isnull(adorAtrL("VALOR_TEXT").value) then%>
					<%if  JSText(adorAtrL("VALOR_TEXT").value) = "" then%>
						esblanco=true
					<%end if%>
				<%end if%>						
			<%else%>		  
				<%if adorAtrL("TIPO").value=2 then%> //NUMERO
					<%if  isnull(adorAtrL("VALOR_NUM").value) then%>
					  esblanco=true
					<%end if%>
				<%else%>
					<%if adorAtrL("TIPO").value=3 then%> //NUMERO
						<%if  isnull(adorAtrL("VALOR_FEC").value) then%>
						  esblanco=true
						<%end if%>
					<%else%>
						<%if adorAtrL("TIPO").value=4 then%>
							<%if  isnull(adorAtrL("VALOR_BOOL").value) then%>
							  esblanco=true
							<%end if%>
						<%end if%>
					<%end if%>
				<%end if%>
			<%end if%>			  
			if (esblanco==false)
			{
			  str += "	<TR>"
			  str += "	<TD class='tablasubrayadaright'><div style='width:100px;height:auto;overflow:hidden'><%=JSText(adorAtrL("DEN").value)%></div></TD>"
			  <%if adorAtrL("TIPO").value=1 then%> //texto
			   str += "	<TD class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'>&nbsp;<%=JSText(adorAtrL("VALOR_TEXT").value)%></div></TD>"
			  <%end if%>
			  <%if adorAtrL("TIPO").value=2 then%> //NUMERO
			   str += "	<TD class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'>&nbsp;" + num2str(<%=JSNum(adorAtrL("VALOR_NUM").value)%>,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</div></TD>"
			  <%end if%>
			  <%if adorAtrL("TIPO").value=3 then%>
			   str += "	<TD class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'>&nbsp;" + date2str(<%=JSDate(adorAtrL("VALOR_FEC").value)%>,vdatefmt) + "</div></TD>"
			  <%end if%>
			  <%if adorAtrL("TIPO").value=4 then%>
			  <%if adorAtrL("VALOR_BOOL").value=0 then%>
			    str += "	<TD class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'>&nbsp;<%=JSText(den(28))%></div></TD>"
			  <%else%>
			    str += "	<TD class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'>&nbsp;<%=JSText(den(27))%></div></TD>"
			  <%end if
			  end if%>		  
			  str += "	</TR>"
			}
		  <%adorAtrL.movenext
      wend
      adorAtrL.close
	  end if%>

    	<% 
		sAdjuntosLinea=""
		set oLinea = oRaiz.Generar_CLinea()
		oLinea.ID = ador("LINEAID").value
		set oAdjuntos = oLinea.cargarAdjuntos(CiaComp,false)
		if oAdjuntos.count > 0 then    
        %>
		    str += "	<tr>"
		    str += "	<td class='tablasubrayadaright'><div style='width:100px;height:auto;overflow:hidden'><%=den(44)%>"            
        <%if isempty(oLinea.ObsAdjun) then%>
            str += "        <a href='javascript:void(null)' onclick='verAdjuntosLinea(" + "<%=IdOrden%>" + "," + "<%=oLinea.ID%>" + ",\"\")'><img border = 0 src= '../images/clip.gif'></A>"
        <%else%>
            str += "        <a href='javascript:void(null)' onclick='verAdjuntosLinea(" + "<%=IdOrden%>" + "," + "<%=oLinea.ID%>" + "," + <%=nulltostr(oLinea.ObsAdjun)%> + ")'><img border = 0 src= '../images/clip.gif'></A>"
        <%end if%>
            str += "	</div></td>"
        <%
            for each oAdjunto in oAdjuntos 
				sAdjuntosLinea=sAdjuntosLinea & oAdjunto.Nombre & " ; "
			next
			sAdjuntosLinea=mid(sAdjuntosLinea,1,len(sAdjuntosLinea)-3)
		%>
          	str += "	<td class='tablasubrayadaleft'><div style='width:400px;height:auto;overflow:hidden'>&nbsp;<%=JSText(sAdjuntosLinea)%></div></td>"
		    str += "	</tr>"
		<%
			set oAdjuntos=nothing
		end if
		%>
	  
	  str += "	</TABLE></TD></TR>"
<%end if
set adorAtrL=nothing%>


<%		if mclass="filaImpar" then
			mclass="filaPar"
		else
			mclass="filaImpar"
		end if
		
		i=i+1
		ador.movenext
	wend
end if	
%>

str+="	   </table>"
str+="	  </tr>"
str+="</table>"	

str+="<p align=center><input class=button type=submit name=cmdAceptar value='<%=den(4)%>' onclick='return validar()'></p>"

document.getElementById("divAceptar").innerHTML=str	
}

function verAdjuntosOrden(id)
{
   window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/adjuntosorden.asp?Orden=" + id, "_blank", "top=5,left=5,width=600, height=250,directories=no,menubar=no,scrollbars=yes,resizable=yes,toolbar=yes")
}

function verAdjuntosLinea(orden,id,obs)
{
   window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/adjuntoslinea.asp?Orden=" + orden + "&Linea=" + id + "&Obs=" + obs, "_blank", "top=5,left=5,width=600, height=250,directories=no,menubar=no,scrollbars=yes,resizable=yes,toolbar=yes")
}
    
</SCRIPT>
<h1><%=den(1)%></h1>
<h3><%=den(5)%></h3>
<h3><%=den(16)%></h3>

<form name=frmAceptar method=post target action="pedidos32.asp">

<div align="center" name=divAceptar id=divAceptar>
</div>

<input type=hidden name=txtIdioma value=<%=Idioma%>>
<input type=hidden name=txtIdOrden value=<%=IdOrden%>>
<input type=hidden name=txtIdPedido value=<%=IdPedido%>>
<input type=hidden name=txtCiaComp value=<%=CiaComp%>>
<input type=hidden name=txtAnyo value=<%=Anyo%>>
<input type=hidden name=txtNumPedido value=<%=NumPedido%>>
<input type=hidden name=txtNumOrden value=<%=NumOrden%>>

</form>

</BODY>
<!--#include file="../common/fsal_2.asp"-->
</HTML>

<%
adorOrden.close
set adorOrden = nothing
ador.close
set oPersona=nothing
set ador=nothing
set oOrden=nothing
set oraiz = nothing

%>
