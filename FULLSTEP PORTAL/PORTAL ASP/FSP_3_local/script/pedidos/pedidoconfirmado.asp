﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/colores.asp"-->

<html>
<head>
<title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">

<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script src="../common/calendar.asp"></script>
<%  
''' <summary>
''' Muestra q se aceptó correctamente un pedido
''' </summary>
''' <remarks>Llamada desde: pedidos\pedidos31.asp ; Tiempo máximo: 0,2</remarks>

	Idioma=Request("Idioma")
	Idioma = trim(Idioma)
	set oRaiz=validarUsuario(Idioma,false,false,0)
	
	dim denPrincipal
	denPrincipal = devolverTextos(Idioma,107)
	
	NumPedProve=Request("NumPedProve")	
	CiaCod=Request("CiaCod")
	CiaComp=Request("CiaComp")
	Anyo=Request("Anyo")
	Pedido=Request("NumPedido")
	Orden=Request("NumOrden")
    
	Referencia = oRaiz.DevolverCodPedido(CiaComp,Idioma)
%>
<script SRC="../common/menu.asp"></script>
<script SRC="../common/formatos.js"></script>


</head>

<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init()
{
    document.getElementById('tablemenu').style.display = 'block';
}

function Imprimir()
{	
	document.getElementById("cmdImprimir").style.visibility = "hidden"
	window.print()
	document.getElementById("cmdImprimir").style.visibility = "visible"
}
</script>

<body onload="init()" topmargin="0" leftmargin="0">
<script>dibujaMenu(4)</script>


<h1><%=denPrincipal(1)%></h1>
	
<br>
<h2><%=denPrincipal(2)%>&nbsp;<%=Anyo%>/<%=Pedido%>/<%=Orden%>&nbsp;<%=denPrincipal(3)%> </h2>
<br>

<h3><%=denPrincipal(4)%></h3>

<table border="0" width="80%">
<tr>
	<td width="70%">
		<h3><%=denPrincipal(5)%> <a HREF="mailto:<%=Application ("MAILPORTAL")%>"><span class="fmedia"><%=Application ("MAILPORTAL")%></span></a></h3>
	</td>
	<td ROWSPAN="2" align="left" valign="middle">
		<input id="cmdImprimir" style="font-size:15px;font-weight:bold;border=0" type="image" src="<%=APPLICATION("RUTASEGURA")%>/script/images/impresora.gif" onclick="javascript:Imprimir()">
	</td>
</tr>
<tr>
	<td>
		<a STYLE="margin-left:20px;" HREF="<%=application("RUTASEGURA")%>script/pedidos/pedidos1.asp?Idioma=<%=Idioma%>&amp;CiaCod=<%=CiaCod%>&amp;CiaComp=<%=CiaComp%>" target="default_main"><%=denPrincipal(6)%></a>
	</td>	
</tr>

</table>
<br><br><br>
<script>
if (top.winEspera)
    top.winEspera.close()
</script>

<div align="center" name="divCentrar" id="divCentrar">

<table border="0" width="90%">
<tr><td><h1><%=denPrincipal(10)%></h1></td>
</tr>
<tr>
	<td><!--#include file="rptPedidoDetalle.asp"--></td>
</tr>
</table>
</div>
</body>
</html>
