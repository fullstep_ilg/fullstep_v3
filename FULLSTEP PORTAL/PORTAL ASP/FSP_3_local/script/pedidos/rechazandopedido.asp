﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/idioma.asp"-->

<html>
<%
''' <summary>
''' Muestra la espera del Rechazo de un pedido
''' </summary>
''' <remarks>Llamada desde: pedidos\pedidos22.asp ; Tiempo máximo: 0,2</remarks>

	Idioma=Request("Idioma")
	Idioma = trim(Idioma)
	set oRaiz=validarUsuario(Idioma,false,false,0)
	
	dim den
	den = devolverTextos(Idioma,106)	
%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">

<title><%=title%></title>

<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
</head>
<body topmargin="1" leftmargin="1">
&nbsp;
<center>
<H2><%=den(5)%>...</b>&nbsp;<img NAME="cursor-espera_grande" SRC="../images/cursor-espera_grande.gif" WIDTH="31" HEIGHT="28"></H2>
</center>
<H3><%=den(6)%></H3>
<H3><%=den(4)%></H3>

</body>
</html>