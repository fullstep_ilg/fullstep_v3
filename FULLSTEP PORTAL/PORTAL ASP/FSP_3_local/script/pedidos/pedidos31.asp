﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/colores.asp"-->

<%
''' <summary>
''' Acepta un pedido
''' </summary>
''' <remarks>Llamada desde: pedidos\pedidos21.asp ; Tiempo máximo: 0,2</remarks>

	
	Idioma=Request("txtIdioma")
	Idioma = trim(Idioma)
	set oRaiz=validarUsuario(Idioma,true,false,0)
	

	IdOrden=Request("txtIdOrden")
	NumPedProve=Request("txtNumPedProve")
	coment=Request("txtObserv")
	IdPedido=Request("txtIdPedido")
	CiaComp =Request("txtCiaComp")
	Anyo =Request("txtAnyo")
	NumPedido =Request("txtNumPedido")
	NumOrden =Request("txtNumOrden")
	NotifReceptor = request("txtNotificarRecept")
	
	CiaProv=clng(oRaiz.Sesion.CiaId)
	CodCiaProv=oRaiz.Sesion.CiaCodGs
	CodCiaComp=oRaiz.Sesion.CiaCod
	

	dim den
	den = devolverTextos(Idioma,35)	

	dim SinError 
	dim iIndice
	Dim TESError 
	
	SinError=true
	
	'1-Comienza la transacción
	oraiz.ComenzarTransaccion()
	
	'2-Actualiza ORDEN_ENTREGA y ORDEN_EST
	set oOrden = oRaiz.Generar_COrden()
	oOrden.id = clng(IdOrden)
 
	oOrden.PedidoId=clng(IdPedido)
	Set TESError =oOrden.AceptarPorProveedor(CiaComp,NumPedProve,coment)
	
	If TESError.NumError <> 0 Then
		oraiz.CancelarTransaccion()
		server.ScriptTimeout=timeout
		'Response.End 
		SinError=false
	end if
	
	
	if SinError=true then
		'3-Actualiza LINEAS_PEDIDO
		iIndice=0
	
		for contador=0 to Request.Form("txtcontador")-1
			if Request.Form("Obl" & iIndice)="1" then
				if cstr(Request.Form("txtFentrega" & iIndice))="" then
					Set TESError =oOrden.RegistrarFechaEntrega(CiaComp,clng(Request.Form("Linea" & iIndice)),Fechaddmmyyyy(Request.Form("FecEntrega" & iIndice),datefmt))	
				else
					Set TESError =oOrden.RegistrarFechaEntrega(CiaComp,clng(Request.Form("Linea" & iIndice)),Fechaddmmyyyy(Request.Form("txtFentrega" & iIndice),datefmt))	
				end if
				If TESError.NumError <> 0 Then
					oraiz.CancelarTransaccion()
					server.ScriptTimeout=timeout
					'Response.End
					SinError=false
				end if
			end if
		    
			iIndice=iIndice+1
			
			if SinError=false then 
				exit for
			end if
		next 
	end if
	
	
	if SinError=true then
		'6- finaliza la transacción
		oraiz.ConfirmarTransaccion()
	
		'7-Notifica a los aprovisionadores
		lIdUsu = oRaiz.Sesion.UsuId 
		lCiaProve = oRaiz.Sesion.CiaId
        CodPortal = cstr(Application ("NOMPORTAL"))
		oOrden.NotifAceptacionAAprovisionador CiaComp,lCiaProve, lIdUsu, false, CodPortal
		if NotifReceptor = 1 then
			oOrden.NotifAceptacionAAprovisionador CiaComp,lCiaProve, lIdUsu, true, CodPortal
		end if
		'Llamada a FSIS.
		oOrden.LlamarFSIS IdOrden, IdPedido, CiaComp
		%>
		<script>
		window.open ("<%=Application("RUTASEGURA")%>script/pedidos/pedidoconfirmado.asp?Idioma=<%=Idioma%>&CiaComp=<%=CiaComp%>&CiaCod=<%=CodCiaComp%>&Anyo=<%=Anyo%>&NumPedido=<%=NumPedido%>&NumOrden=<%=NumOrden%>","_self")
		</script>
		<%
	else
%>

		<HTML>

		<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
		<script SRC="../common/menu.asp"></script>
		<script SRC="../common/formatos.js"></script>

		<HEAD>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
		</head>
	
		<script language="JavaScript" type="text/JavaScript">
		/*''' <summary>
		''' Iniciar la pagina.
		''' </summary>     
		''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
		function init()
		{
		    document.getElementById('tablemenu').style.display = 'block';
		}
		</script>
	
		<body onload="init()" topmargin="0" leftmargin="0">

		<script>
		dibujaMenu(4)
		</script>
	
		<h1><%=den(4)%></h1>
		<br>
		<h3 class=error><%=den(2)%></h3>

		<script>
        if (top.winEspera)
		    top.winEspera.close()
		</script>
		</BODY>

		</HTML>

<% 
	end if
	
    'ESTA FUNCION CONVIERTE UNA FECHA DEL FORMATO DEL USUARIO AL FORMATO dd/mm/yyyy
'
'mFecha: La fecha a convertir
'datefmt: El formato de la fecha en el que se suministra

Function Fechaddmmyyyy(mFecha,datefmt)
dim datesep
dim returnFecha
dim mdia
dim mmes
dim manyo

if isnull(mFecha) or mFecha="" then
	Fechaddmmyyyy=null
	exit function
end if
returnFecha=mFecha
datesep = mid(datefmt,3,1)
		
		
if left(datefmt,1)="d" then 
	mFecha = split(returnFecha,datesep)
				
	mdia=clng(mFecha(0))
	mmes=clng(mFecha(1))
	manyo=clng(mFecha(2))

else
	mFecha = split(returnFecha,datesep)
				
	mdia=clng(mFecha(1))
	mmes=clng(mFecha(0))
	manyo=clng(mFecha(2))
end if


on error resume next
returnFecha = mdia & "/" & mmes & "/" & manyo

Fechaddmmyyyy=returnFecha


end function

	server.ScriptTimeout=timeout
	set oPedido=nothing	
    
	set oRaiz = nothing
%>

