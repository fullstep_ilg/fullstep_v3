﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/colores.asp"-->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">

<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script src="../common/calendar.asp"></script>
<%  
''' <summary>
''' Muestra q se rechazó correctamente un pedido
''' </summary>
''' <remarks>Llamada desde: pedidos\pedidos32.asp ; Tiempo máximo: 0,2</remarks>

	Idioma=Request("Idioma")
	Idioma = trim(Idioma)
	set oRaiz=validarUsuario(Idioma,false,false,0)
	
	dim den
	den = devolverTextos(Idioma,107)
	
	NumPedProve=Request("NumPedProve")	
	CiaCod=Request("CiaCod")
	CiaComp=Request("CiaComp")
	Anyo=Request("Anyo")
	NumPedido=Request("NumPedido")
	NumOrden=Request("NumOrden")
%>
<script SRC="../common/menu.asp"></script>
<script SRC="../common/formatos.js"></script>

</head>

<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init()
{

    document.getElementById('tablemenu').style.display = 'block';
	
}


</script>
<body onload="init()" topmargin="0" leftmargin="0">
<script>dibujaMenu(4)</script>




<h1><%=den(7)%></h1>
	
<br>
<h2><%=Anyo%>/<%=NumPedido%>/<%=NumOrden%>&nbsp;<%=den(8)%></h2>
<br>

<h3><%=den(9)%></h3>

<h3><%=den(5)%> <A HREF=mailto:<%=Application ("MAILPORTAL")%>><span class=fmedia><%=Application ("MAILPORTAL")%></span></A></h3>

<a STYLE="margin-left:20px;" HREF="<%=application("RUTASEGURA")%>script/pedidos/pedidos11.asp?Idioma=<%=Idioma%>&amp;CiaCod=<%=CiaCod%>&amp;CiaComp=<%=CiaComp%>"><%=Den(6)%></a>

<p>&nbsp;</p>
<script>
if (top.winEspera)
    top.winEspera.close()
</script>

</body>

<% 
	
%>

</html>
