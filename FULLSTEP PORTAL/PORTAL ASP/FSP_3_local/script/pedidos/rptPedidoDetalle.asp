﻿<%
''' <summary>
''' Muestra el detalle de un pedido
''' </summary>
''' <remarks>Llamada desde: pedidos\pedidoconfirmado.asp    pedidos\rptpedido.asp ; Tiempo máximo: 0,2</remarks>
	
	dim esblanco
	
	'Idiomas
	dim den
	den = devolverTextos(Idioma,85)	

    set oRaiz=validarUsuario(Idioma,true,true,0)
    
    set ParamGen = oRaiz.Sesion.Parametros
	
	'obtiene los formatos del usuario
	decimalfmt=request.Cookies ("USU_DECIMALFMT")
	thousanfmt=Request.Cookies ("USU_THOUSANFMT")
	precisionfmt=cint(Request.Cookies ("USU_PRECISIONFMT"))
	datefmt=Request.Cookies("USU_DATEFMT") 
	
	set oOrdenes=oRaiz.Generar_cOrdenes()
	set oDestinos=oRaiz.Generar_cDestinos()	
	
	
	CiaProv=oRaiz.Sesion.CiaCodGs
	set ador=oOrdenes.BuscarTodasOrdenes (CiaComp,Idioma,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"),,null,null,null,null,null,null,null,null,CiaProv,null,null,anyo,pedido,orden)
	
	sDirFac=""
	bMostrarDirFact=false
	for each oField in ador.fields
		if ucase(oField.name)="DIRFAC" then
			bMostrarDirFact=true
			sDirFac=oField.value
			exit for
		end if
	next	
	
	set oOrden = oRaiz.Generar_COrden()
	oOrden.id = clng(ador("ID").value)
	set adorAtr = oOrden.DevolverAtributos(CiaComp)
	
	
	dim sAdjuntosLinea
	dim sAdjuntosOrden
%>

		<table width="100%">
			<tr>
				<td>
					<table width="100%">
						<tr>
							<td rowspan="2" width="33%" class="cabecera"><%=den(41)%></td>
							<td class="filaPar" colspan="3">
								<%=ador.fields("NIFEMPRESA").value%>
							</td>
						</tr>
						<tr>
							<td class="filaPar" colspan="3">
								<%=HTMLEncode(ador.fields("DENEMPRESA").value)%>
							</td>
						</tr>
						<tr id="DirFac">
							<td rowspan="2" width="33%" class="cabecera"><%=den(59)%></td>
							<td class="filaPar" colspan="3"><%=HTMLEncode(sDirFac)%></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
<SCRIPT>
	function dibujarDirFac()
	{
		var Row
		Row=document.getElementById("DirFac")
		<%if bMostrarDirFact then%>
			Row.style.display='inline'
		<%else%>
			Row.style.display='none'		
		<%end if%>
	}

	dibujarDirFac()

    function verAdjuntosOrden(id)
    {
       window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/adjuntosorden.asp?Orden=" + id, "_blank", "top=5,left=5,width=600, height=250,directories=no,menubar=no,scrollbars=yes,resizable=yes,toolbar=yes")
    }

    function verAdjuntosLinea(orden,id,obs)
    {
       window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/adjuntoslinea.asp?Orden=" + orden + "&Linea=" + id + "&Obs=" + obs, "_blank", "top=5,left=5,width=600, height=250,directories=no,menubar=no,scrollbars=yes,resizable=yes,toolbar=yes")
    }
</SCRIPT>

<%
	'Aprovisionador
	set oPersonas = oRaiz.generar_CPersonas()
	if  isnull(ador.Fields("PER").Value) then
		set oPersona =NOTHING
	else
		Set adoRSPer = oPersonas.DevolverDatosPersona(CiaComp, ador.Fields("PER").Value)
	
		set oPersona = oPersonas.Add( adoRSPer.Fields("COD").Value, adoRSPer.Fields("NOM").Value, adoRSPer.Fields("APE").Value, _
           NullToStr(adoRSPer.Fields("CAR").Value), adoRSPer.Fields("DEP").Value, NullToStr(adoRSPer.Fields("EMAIL").Value), _
           NullToStr(adoRSPer.Fields("TFNO").Value), adoRSPer.Fields("TFNO2").Value, adoRSPer.Fields("FAX").Value, _
           adoRSPer.Fields("UON1").Value, adoRSPer.Fields("UON2").Value, adoRSPer.Fields("UON3").Value, _
           adoRSPer.Fields("DENU1").Value, adoRSPer.Fields("DENU2").Value, adoRSPer.Fields("DENU3").Value, _
           adoRSPer.Fields("EQP").Value, adoRSPer.Fields("DENDEP").Value)
		adoRSPer.Close
	end if
	'Receptor
	if isnull(ador.Fields("RECEPTOR").Value) then
		set oPersonaR =NOTHING
	else
		bLeer=false
		if  not isnull(ador.Fields("PER").Value) then
			if ador.Fields("RECEPTOR").Value = ador.Fields("PER").Value then
				set oPersonaR =oPersona
			else
				bLeer=true
			end if
		else
			bLeer=true
		end if
		if bLeer then
			Set adoRSPer = oPersonas.DevolverDatosPersona(CiaComp, ador.Fields("RECEPTOR").Value)
			set oPersonaR = oPersonas.Add( adoRSPer.Fields("COD").Value, adoRSPer.Fields("NOM").Value, adoRSPer.Fields("APE").Value, _
			   NullToStr(adoRSPer.Fields("CAR").Value), adoRSPer.Fields("DEP").Value, NullToStr(adoRSPer.Fields("EMAIL").Value), _
			   NullToStr(adoRSPer.Fields("TFNO").Value), adoRSPer.Fields("TFNO2").Value, adoRSPer.Fields("FAX").Value, _
			   adoRSPer.Fields("UON1").Value, adoRSPer.Fields("UON2").Value, adoRSPer.Fields("UON3").Value, _
			   adoRSPer.Fields("DENU1").Value, adoRSPer.Fields("DENU2").Value, adoRSPer.Fields("DENU3").Value, _
			   adoRSPer.Fields("EQP").Value, adoRSPer.Fields("DENDEP").Value)
			adoRSPer.Close
		end if
	end if
	
	'Carga los adjuntos
	if not isnull(ador("ADJUNTOS").value) then
		sAdjuntosOrden=""
		set oOrden = oRaiz.Generar_COrden()
		oOrden.ID=ador("ID").value
		set oAdjuntosOrden = oOrden.cargarAdjuntos(CiaComp,false)
		for each oAdjunto in oAdjuntosOrden
			sAdjuntosOrden=sAdjuntosOrden & oAdjunto.Nombre & " ; "
		next
		sAdjuntosOrden=mid(sAdjuntosOrden,1,len(sAdjuntosOrden)-3)
	end if
	
    'Costes y descuentos
    if ador("HAYCOSTES")>0 or ador("HAYDESCUENTOS")>0 then
        set adorCostesDesc=oOrden.DevolverCostesDescuentosCab(CiaComp)        
    end if	
    dImporteTotal=ador("IMPORTE")*ador("CAMBIO")
    set adorImp = oOrden.Devolverlineas(CiaComp,Idioma)
    if not adorImp.eof then
	    while not adorImp.eof
            dImporteBrutoLin=adorImp("IMPORTE").value
            if not isnull(adorImp("COSTES")) then dCostesLin=adorImp("COSTES")*ador("CAMBIO")
            if not isnull(adorImp("DESCUENTOS")) then dDescuentosLin=adorImp("DESCUENTOS")*ador("CAMBIO")
            dImporteBruto=dImporteBruto+dImporteBrutoLin+dCostesLin-dDescuentosLin
            adorImp.movenext
        wend
    end if
    set oLineasDesgloseImpuesto = oRaiz.Generar_CLineasDesgImpuesto
    oLineasDesgloseImpuesto.cargarLineasDesgloseImpuestoOrden oOrden.id,CiaComp,Idioma
	
%>
		<table width="100%" style="border:solid 1 black;page-break-after:always;">
			<tr>
			<td colspan="2">
		<table width="100%">
		    <!--DATOS GENERALES-->
		    <tr><td class="cabecera" colspan="4"><%=Den(64)%></td></tr>
			<tr>
				<td width="30%" class="cabecera"><%=den(1)%></td>
				<td width="20%" class="filaPar">
					<%=ador("anyo").value%>/<%=ador("numpedido").value%>/<%=ador("numorden").value%>
				</td>
				<td width="20%" class="cabecera"><%=den(51)%></td>
				<td width="30%" class="filaPar">
					<%=ador("NUMEXT").value%>
				</td>				
			</tr>
			
			<%if Referencia="" then%>
				<tr>
					<td width="30%" class="cabecera"><%=den(2)%></td>
					<td width="20%" class="filaPar">
						<%
						if not isnull(ador("FECHA").value) then
							Response.write VisualizacionFecha(ador("FECHA").value,datefmt)
						else
							Response.Write "&nbsp;"
						end if%>
					</td>
					<td width="20%" class="cabecera"><%=den(52)%></td>
					<td width="30%" class="filaPar" colspan="5">
						<%=VB2HTML(ador.fields("PAG").value & " - " & ador.fields("PAGDEN").value)%>
					</td>
				</tr>
				<tr>														
					<td width="30%" class="cabecera"><%=den(32)%></td>
					<td class="filaPar" colspan=3>
						<%
						select case ador("EST").value
							case 2:
								Response.Write den(47)
							case 3:
								Response.Write den(36)
							case 4:
								Response.Write den(37)
							case 5:
								Response.Write den(38)
							case 6:
								Response.Write den(39)
							case 21:
								Response.Write den(40)
						end select%>
					</td>					
				</tr>
			<%else%>
				<tr>
					<td width="30%" class="cabecera"><%=Referencia%></td>
					<td width="20%" class="filaPar"><%=HTMLEncode(ador("REFERENCIA").value)%></td>
					<td width="20%" class="cabecera"><%=den(2)%></td>
					<td width="30%" class="filaPar">
						<%
						if not isnull(ador("FECHA").value) then
							Response.write VisualizacionFecha(ador("FECHA").value,datefmt)
						else
							Response.Write "&nbsp;"
						end if%>
					</td>
				</tr>					
				<tr>
					<td width="30%" class="cabecera"><%=den(52)%></td>
					<td width="20%" class="filaPar">
						<%=VB2HTML(ador.fields("PAG").value & " - " & ador.fields("PAGDEN").value)%>
					</td>						
				    <td width="20%" class="cabecera"><%=den(32)%></td>
				    <td class="filaPar" colspan="3">
					    <%
					    select case ador("EST").value
						    case 2:
							    Response.Write den(47)
						    case 3:
							    Response.Write den(36)
						    case 4:
							    Response.Write den(37)
						    case 5:
							    Response.Write den(38)
						    case 6:
							    Response.Write den(39)
						    case 21:
							    Response.Write den(40)
					    end select%>
				    </td>
			    </tr>			
			<%end if			
			if not isnull(ador("ADJUNTOS").value) then%>
			    <tr>	
				    <td class="cabecera" width="30%"><%=den(49)%><a href='javascript:void(null)' onclick='verAdjuntosOrden(<%=oOrden.id%>)'><img border = 0 src= '../images/clip.gif'></A></td>
				    <td class="filaPar" colspan="3"><%=sAdjuntosOrden%></td>
			    </tr>
			<%end if%>
			<%if not isnull(ador("OBS").value) then%>
			    <tr>	
				    <td class="cabecera" width="30%"><%=den(21)%></td>
				    <td class="filaPar" colspan="3"><%=HTMLEncode(ador("OBS").value)%></td>
			    </tr>
			<%end if%>
			<%if not adorAtr is nothing then
				while not adorAtr.eof				
				    esblanco = false
				    if adorAtr("TIPO").value=1 then
					    if  isnull(adorAtr("VALOR_TEXT").value) then
						    esblanco=true
					    elseif  Len(adorAtr("VALOR_TEXT").value) = 0 then
							    esblanco=true
					    end if												
				    else		  
					    if adorAtr("TIPO").value=2 then
						    if isnull(adorAtr("VALOR_NUM").value) then
						      esblanco=true
						    end if
					    else
						    if adorAtr("TIPO").value=3 then
							    if isnull(adorAtr("VALOR_FEC").value) then
							      esblanco=true
							    end if
						    else
							    if adorAtr("TIPO").value=4 then
								    if isnull(adorAtr("VALOR_BOOL").value) then
								      esblanco=true
								    end if
							    end if
						    end if
					    end if
				    end if			  
				    if (esblanco=false) then %>
					    <tr>
						    <td width="30%" class="cabecera"><%=HTMLEncode(adorAtr("DEN").value)%></td>
					    <%if adorAtr("TIPO").value=1 then%> 
					        <td class="filaPar" colspan="3"><%=VB2HTML(adorAtr("VALOR_TEXT").value)%></td>
					    <%end if%>
					    <%if adorAtr("TIPO").value=2 then%> 
					        <td class="filaPar" colspan="3"><%=visualizacionNumero(adorAtr("VALOR_NUM").value,decimalfmt,thousanfmt,precisionfmt)%></td>
					    <%end if%>
					    <%if adorAtr("TIPO").value=3 then%>
					        <td class="filaPar" colspan="3"><%=visualizacionFecha(adorAtr("VALOR_FEC").value,datefmt)%></td>
					    <%end if%>
					    <%if adorAtr("TIPO").value=4 then%>
					        <%if isnull(adorAtr("VALOR_BOOL").value) then%>
					            <td class="filaPar" colspan="3">&nbsp;</td>
					        <%elseif adorAtr("VALOR_BOOL").value=0 then%>
					        <td class="filaPar" colspan="3"><%=den(53)%></td>
					        <%else%>
					        <td class="filaPar" colspan="3"><%=den(54)%></td>
					        <%end if
					    end if%>
					    </tr>
			        <%end if					
			        adorAtr.movenext
			    wend
			end if%>
			
            <!--IMPORTES-->
            <tr><td class="cabecera" colspan="4"><%=Den(61)%></td></tr>
            <tr>
                <td class="cabecera"><%=Den(62)%></td>
                <td class="filaPar" style="text-align:right;">
					<%=replace(FormatNumber(dImporteBruto,2),",",".")%>&nbsp;<%=ador("MON").value%>
                    <%MonedaPedido=ador("MON").value%>
					<%CambioPedido=ador("CAMBIO").value%>					
				</td>
                <td class="filaPar" colspan="2"/>
            </tr>
            <!--Costes/descuentos-->
            <%if ador("HAYCOSTES")>0 or ador("HAYDESCUENTOS")>0 then
                if not adorCostesDesc is nothing then
                    adorCostesDesc.movefirst
                    while not adorCostesDesc.eof                    
                        sDescCD=VisualizacionCosteDesc(iif(isnull(adorCostesDesc("DEN")),"",adorCostesDesc("DEN")),adorCostesDesc("TIPO"),adorCostesDesc("OPERACION"),adorCostesDesc("VALOR"),ador("MON").value,CambioPedido)%>
                        <tr>
                            <td class="cabecera"><%=sDescCD%></td>
                            <td class="filaPar" style="text-align:right;">
                                <%=replace(FormatNumber((adorCostesDesc("IMPORTE")*CambioPedido),2),",",".")%>&nbsp;<%=ador("MON").value%>
                            </td>
                            <td class="filaPar" colspan="2"/>
                        </tr>
                        <%adorCostesDesc.movenext
                    wend
                end if
            end if%> 
            <!--Impuestos-->
            <%for each oLineaDesgloseImpuesto in oLineasDesgloseImpuesto%>
            <tr>
                <td class="cabecera">
                    <%=Den(73)%>&nbsp;<%=oLineaDesgloseImpuesto.tipoimpuesto%>&nbsp;<%=oLineaDesgloseImpuesto.TipoValor%>%&nbsp;
                    (<%=Den(74)%>:&nbsp;<%=replace(FormatNumber(oLineaDesgloseImpuesto.BaseImponible,2),",",".")%>&nbsp;<%=ador("MON").value%>)
                </td>
                <td class="filaPar" style="text-align:right;">
                    <%=replace(FormatNumber(oLineaDesgloseImpuesto.cuota,2),",",".")%>&nbsp;<%=ador("MON").value%>
                </td>
                <td class="filaPar" colspan="2"/>
            </tr>
            <% next%>
            <!--Importe total-->            
            <tr>
                <td class="cabecera"><%=Ucase(Den(63))%></td>                
				<td class="filaPar" style="text-align:right;">
					<%=replace(FormatNumber(dImporteTotal,2),",",".")%>&nbsp;<%=ador("MON").value%>
				</td>
                <td class="filaPar" colspan="2"/>	
            </tr>

			<%if not oPersona is nothing then %>			
			<tr colspan="2"><td class="cabecera" colspan="4"><%=den(55)%></td></tr>
			<tr>
				<td class="cabecera" width="20%"><%=den(44)%></td>			
				<td width="30%" class="filaPar">		
					<%=HTMLEncode(oPersona.nombre)%>&nbsp;<%=HTMLEncode(oPersona.Apellidos)%>
				</td>
				<td width="20%" rowspan="3" class="cabecera" valign="top"><%=den(48)%></td>
				<td width="30%" class="filaPar"><%=HTMLEncode(oPersona.UON1DEN)%></td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(45)%></td>
				<td class="filaPar"><%=HTMLEncode(oPersona.tfno)%></td>
				<td class="filaPar">
					<%if not isnull(oPersona.uon2) then%>
						<%=HTMLEncode(oPersona.UON2DEN)%>
					<%end if%>
				</td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(46)%></td>
				<td class="filaPar">
						<a href="mailto:<%=oPersona.email%>"><%=VB2HTML(oPersona.email)%></a>
				</td>
				<td class="filaPar">
					<%if not isnull(oPersona.uon3) then%>
						<%=HTMLEncode(oPersona.UON3DEN)%>
					<%end if%>
				</td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(57)%></td>
				<td class="filaPar"><%=VB2HTML(oPersona.fax)%></td>			
				<td class="cabecera"><%=den(42)%></td>
				<td class="filaPar"><%=VB2HTML(oPersona.depden)%></td>
			 </tr>		
		<%end if%>
		<%if not oPersonaR is nothing then %>			
			<tr><td class="cabecera" colspan="4"><%=den(56)%></td></tr>
			<tr>
				<td class="cabecera" width="20%"><%=den(44)%></td>			
				<td width="30%" class="filaPar">		
					<%=HTMLEncode(oPersonaR.nombre)%>&nbsp;<%=HTMLEncode(oPersonaR.Apellidos)%>
				</td>
				<td width="20%" rowspan="3" class="cabecera" valign="top"><%=den(48)%></td>
				<td width="30%" class="filaPar"><%=VB2HTML(oPersonaR.UON1DEN)%></td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(45)%></td>
				<td class="filaPar"><%=oPersonaR.tfno%></td>
				<td class="filaPar">
					<%if not isnull(oPersonaR.uon2) then%>
						<%=HTMLEncode(oPersonaR.UON2DEN)%>
					<%end if%>
				</td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(46)%></td>
				<td class="filaPar">
						<a href="mailto:<%=oPersonaR.email%>"><%=VB2HTML(oPersonaR.email)%></a>
				</td>
				<td class="filaPar">
					<%if not isnull(oPersonaR.uon3) then%>
						<%=HTMLEncode(oPersonaR.UON3DEN)%>
					<%end if%>
				</td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(57)%></td>
				<td class="filaPar"><%=VB2HTML(oPersonaR.fax)%></td>			
				<td class="cabecera"><%=den(42)%></td>
				<td class="filaPar"><%=VB2HTML(oPersonaR.depden)%></td>
			 </tr>		
		<%end if%>
	</table>
	<br>
	<br>	
	<%
	
	OrdenId=ador("id").value
	ador.close
	set ador = nothing
	
		
	set oOrden = oRaiz.Generar_COrden()

	oOrden.id = clng(OrdenId)

	set ador = oOrden.Devolverlineas(CiaComp,Idioma)
    
    'Ocultar columna fecha de entrega
    iNumLinPlanEnt=0
    iNumLinPlanEntPub=0
    bOcultarFecEntrega=false
    if not ador is nothing then
        if ador.recordcount>0 then
            while not ador.eof               
                if ador("HAYPLANES")>0 then
                    iNumLinPlanEnt=iNumLinPlanEnt+1
                    if ador("PUB_PLAN_ENTREGA")=1 then iNumLinPlanEntPub=iNumLinPlanEntPub+1
                end if
                ador.movenext
            wend
        end if

        if iNumLinPlanEnt>0  then
            if iNumLinPlanEntPub>0 then set adorPlanes=oOrden.DevolverPlanesEntrega(CiaComp)            
            if (iNumLinPlanEnt=ador.recordcount) then bOcultarFecEntrega=true                    
        end if

         ador.movefirst
    end if     
    %>
	<table width="100%">
		<tr>
        	<td class="cabecera"><%=den(75)%></td>
			<td class="cabecera"><%=den(4)%></td>
			<td class="cabecera"><%=den(5)%></td>
			<td class="cabecera"><%=den(6)%></td>
			<td class="cabecera"><%=den(7)%></td>
			<td class="cabecera"><%=den(8)%></td>
			<%if not bOcultarFecEntrega then %>
			    <td class="cabecera"><%=den(9)%></td>
                <td class="cabecera"><%=den(72)%></td>
                <td class="cabecera"><%=den(10)%></td>
            <%end if%>	
			<td class="cabecera"><%=den(11)%></td>
			<td class="cabecera"><%=den(12)%></td>
            <td class="cabecera"><%=den(65)%>&nbsp;(<%=MonedaPedido%>)</td>
            <td class="cabecera"><%=den(66)%>&nbsp;(<%=MonedaPedido%>)</td>
			<td class="cabecera"><%=den(20)%>&nbsp;(<%=MonedaPedido%>)</td>
		</tr>
<%	

	fila = "filaImpar"
	while not ador.eof%>
		
           <%
                    numLineaPedidoTmp=ador("NUMLINEAPEDIDO").value
                    if numLineaPedidoTmp < 10 then
                        numLineaPedido = "00" & numLineaPedidoTmp
                    elseif numLineaPedidoTmp >= 100 then
                        numLineaPedido = numLineaPedidoTmp
                    else
                        numLineaPedido = "0" & numLineaPedidoTmp
                    end if
                %>

		<tr>
   			<td class="<%=fila%>">
				<%=numLineaPedido%>
			</td>
			<td class="<%=fila%>">
				<%=HTMLEncode(ador("codart").value)%>
			</td>
			<td class="<%=fila%>">
				<%=ador("art_ext").value%>
			</td>
			<td class="<%=fila%>">
				<%=HTMLEncode(ador("denart").value)%>
			</td>
			<td class="<%=fila%>">
				<%=HTMLEncode(ador("DESTINO").value)%>
				
				<%if oDestinos.item(ador("DESTINO").value) is nothing then
					set adorDest = oDestinos.CargarDatosDestino(Idioma,Ciacomp, ador("DESTINO").value, ador("LINEAID").value)
					oDestinos.Add adorDest("COD").Value, adorDest("DEN").Value, false, adorDest("DIR"), adorDest("POB"), adorDest("CP"), adorDest("PROVI"), adorDest("PAI")
					adorDest.close
					set adorDest = nothing
				end if%>
				
			</td>
			<td class="<%=fila%>">
				<nobr><%=ador("UNIDAD").value%>-<%=HTMLEncode(ador("UNIden").value)%></nobr>
			</td>
			<%if not bOcultarFecEntrega then %>
                <%if ador("HAYPLANES")>0 then%>                                           
                    <td class="<%=fila%>">&nbsp;</td>                                             
                    <td class="<%=fila%>">&nbsp;</td> 
                <%else%>
			        <td class="<%=fila%>">
				        <%
				        if not isnull(ador("FECENTREGA").value) then                            
					        Response.write VisualizacionFecha(ador("FECENTREGA").value,datefmt)
				        else
					        Response.Write "&nbsp;"
				        end if%>
			        </td>
                    <td class="<%=fila%>">
				        <%
				        if not isnull(ador("FECENTREGAPROVE").value) then                            
					        Response.write VisualizacionFecha(ador("FECENTREGAPROVE").value,datefmt)
				        else
					        Response.Write "&nbsp;"
				        end if%>
			        </td>
                    <td class="<%=fila%>">
				        <%if ador("ENTREGA_OBL").value=1 then
					        Response.Write den(18)
				        else
					        Response.Write den(19)
				        end if%>
			        </td>
                <%end if%>                 
            <%end if%>
		    <%if ador("TIPORECEPCION").value=1 then %>
                <td class=<%=fila%> align=right>&nbsp;</td>	 
            <%else %> 
		        <td class="<%=fila%> numero">
			        <%=visualizacionNumero(ador("CANTIDAD").value,decimalfmt,thousanfmt,precisionfmt)%>
		        </td>
            <%end if %>
            <%if ador("TIPORECEPCION").value=1 then %>
                <td class=<%=fila%> align=right>&nbsp;</td>	 
            <%else %> 
			    <td class="<%=fila%> numero">
				    <%=visualizacionNumero(ador("PRECIOUNITARIO").value ,decimalfmt,thousanfmt,precisionfmt)%>
			    </td>
            <%end if %>
            <td class="<%=fila%> numero">
				<%=visualizacionNumero(ador("COSTES").value * CambioPedido ,decimalfmt,thousanfmt,precisionfmt)%>
			</td>
            <td class="<%=fila%> numero">
				<%=visualizacionNumero(ador("DESCUENTOS").value * CambioPedido ,decimalfmt,thousanfmt,precisionfmt)%>
			</td>
            
			<td class="<%=fila%> numero">
				<%=visualizacionNumero(ador("IMPORTE").value+(ador("COSTES").value-ador("DESCUENTOS").value) * CambioPedido ,decimalfmt,thousanfmt,precisionfmt)%>
			</td>
            
		</tr>
        
        <!--COSTES y DESCUENTOS-->
        <%if ador("HAYCOSTES")>0 or ador("HAYDESCUENTOS")>0 then
            set adorCDs = oOrden.DevolverCostesDescuentosLinea(ador("LINEAID").value,CiaComp)   
            
            if not adorCDs is nothing then%>
                <tr>
                    <%if not bOcultarFecEntrega then %>
                        <td colspan="10"></td>
                    <%else%>
                        <td colspan="7"></td>
                    <%end if%>
                    <td class="cabecera" colspan="3"><%=Den(67)%></td>
                </tr>
                <%if ador("HAYCOSTES")>0 then%>
                    <tr>
                        <%if not bOcultarFecEntrega then %>
                            <td colspan="10"></td>
                        <%else%>
                            <td colspan="7"></td>
                        <%end if%>
                        <td class="cabecera" colspan="2"><%=Den(68)%></td>
                        <td class="cabecera"><%=Den(70)%>&nbsp;(<%=MonedaPedido%>)</td>                   
                    </tr>
                    <%adorCDs.movefirst
                    adorCDs.filter="TIPO=0"
                    while not adorCDs.eof
                        sDescCD=VisualizacionCosteDesc(iif(isnull(adorCDs("DEN")),"",adorCDs("DEN")),adorCDs("TIPO"),adorCDs("OPERACION"),adorCDs("VALOR"),MonedaPedido,CambioPedido)%>
                        <tr>
                            <%if not bOcultarFecEntrega then %>
                                <td colspan="10"></td>
                            <%else%>
                                <td colspan="7"></td>
                            <%end if%>
                            <td class=<%=fila%> colspan="2"><%=HTMLEncode(sDescCD)%></td>
                            <td class=<%=fila%> style="text-align:right;"><%=visualizacionNumero((adorCDs("IMPORTE")*CambioPedido),decimalfmt,thousanfmt,precisionfmt)%></td>                            
                        </tr>
                        <%adorCDs.movenext
                    wend
                end if
                if ador("HAYDESCUENTOS")>0 then%>
                    <tr>
                        <%if not bOcultarFecEntrega then %>
                            <td colspan="10"></td>
                        <%else%>
                            <td colspan="7"></td>
                        <%end if%>
                        <td class="cabecera" colspan="2"><%=Den(69)%></td>
                        <td class="cabecera"><%=Den(71)%>&nbsp;(<%=MonedaPedido%>)</td>
                    </tr>
                    <%adorCDs.movefirst
                    adorCDs.filter="TIPO=1"
                    while not adorCDs.eof
                        sDescCD=VisualizacionCosteDesc(iif(isnull(adorCDs("DEN")),"",adorCDs("DEN")),adorCDs("TIPO"),adorCDs("OPERACION"),adorCDs("VALOR"),MonedaPedido,CambioPedido)%>
                        <tr>
                            <%if not bOcultarFecEntrega then %>
                                <td colspan="10"></td>
                            <%else%>
                                <td colspan="7"></td>
                            <%end if%>
                            <td class=<%=fila%> colspan="2"><%=HTMLEncode(sDescCD)%></td>
                            <td class=<%=fila%> style="text-align:right;"><%=visualizacionNumero((adorCDs("IMPORTE")*CambioPedido),decimalfmt,thousanfmt,precisionfmt)%></td>                            
                        </tr>
                        <%adorCDs.movenext
                    wend
                end if
            end if  
        end if%>
        		
		<%set adorAtrL = oOrden.DevolverAtributosLinea(ador("LINEAID").value,CiaComp)
		if not (adorAtrL is nothing) or not isnull(ador("CODPROCE").value) or (not isnull(ador("OBS").value) AND ador("OBS").value<>"") or not isnull(ador("DENCAMPO1").value) or not isnull(ador("DENCAMPO2").value) or not isnull(ador("ADJUNTOS").value) then %>

		  <tr><td>&nbsp;</td><td colspan="12"><table width="100%" CELLSPACING="0">
            <%if ador("HAYPLANES")>0 then
                if ador("PUB_PLAN_ENTREGA")=1 then%> 
                    <tr>
                        <td class="tablasubrayadaright" valign="top"><div style="width:120px;height:auto;overflow:hidden;text-align:top;"><%=den(60)%></div></td>
                        
                        <%if not adorPlanes is nothing then
                            adorPlanes.filter="ID=" & ador("LINEAID")%>
                            <td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">
                            <%if adorPlanes.recordcount>0 then%> 
                                <table>                                        
                                    <%while not adorPlanes.eof%>
                                        <tr>
                                        <td>&nbsp;</td>
                                        <td><%=VisualizacionFecha(adorPlanes("FECHA").value,datefmt)%></td>
                                        <td> -**** </td>
                                        <% if adorPlanes("TIPORECEPCION")=0 then %>
                                        <td><%=adorPlanes("CANTIDAD").value%></td>
                                        <td><%=ador("UNIDAD").value%></td>
                                        <%else %>
                                            <td><%=adorPlanes("IMPORTE").value%></td>
                                            <td><%=adorPlanes("MON").value%></td>
                                        <%end if %>
                                        </td></tr>
                                        <%adorPlanes.movenext
                                    wend%>
                                </table>
                            <%end if%>
                            </div></td>
                        <%end if%>
                    </tr>
                <%end if
            end if%>

			<%if not isnull(ador("CODPROCE").value) then%>
			<tr>
			<td class="tablasubrayadaright"><div style="width:100px;height:auto;overflow:hidden"><%=den(50)%></div></td>
			<td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">&nbsp;<%=ador("ANYOPROCE").value%>/<%=ador("GMN1").value%>/<%=ador("CODPROCE").value%>&nbsp;<%=HTMLEncode(ador("DENPROCE").value)%></div></td>
			</tr>
			<%end if%>
	
			<tr>
			<td class="tablasubrayadaright"><div style="width:100px;height:auto;overflow:hidden"><%=den(21)%></div></td>
			<td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">&nbsp;<%=VB2HTML(ador("OBS").value)%></div></td>
			</tr>
			
			<%if not isnull(ador("DENCAMPO1").value) and ador("DENCAMPO1").value<>"" then%>
			<tr>
			<td class="tablasubrayadaright"><div style="width:100px;height:auto;overflow:hidden"><%=HTMLEncode(ador("DENCAMPO1").value)%></div></td>
			<td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">&nbsp;<%=HTMLEncode(ador("VALOR1").value)%></div></td>
			</tr>
			<%end if%>
	
			<%if not isnull(ador("DENCAMPO2").value) and ador("DENCAMPO2").value<>"" then%>
			<tr>
			<td class="tablasubrayadaright"><div style="width:100px;height:auto;overflow:hidden"><%=HTMLEncode(ador("DENCAMPO2").value)%></div></td>
			<td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">&nbsp;<%=HTMLEncode(ador("VALOR2").value)%></div></td>
			</tr>
			<%end if%>
			<%if not  (adorAtrL is nothing) then 
			while  not adorAtrL.EOF				
				esblanco = false
				if adorAtrL("TIPO").value=1 then
					if  isnull(adorAtrL("VALOR_TEXT").value) then
						esblanco=true
					elseif  Len(adorAtrL("VALOR_TEXT").value) = 0 then
						esblanco=true
					end if	
				else		  
					if adorAtrL("TIPO").value=2 then
						if isnull(adorAtrL("VALOR_NUM").value) then
						  esblanco=true
						end if
					else
						if adorAtrL("TIPO").value=3 then
							if isnull(adorAtrL("VALOR_FEC").value) then
							  esblanco=true
							end if
						else
							if adorAtrL("TIPO").value=4 then
								if isnull(adorAtrL("VALOR_BOOL").value) then
								  esblanco=true
								end if
							end if
						end if
					end if
				end if			  
				if (esblanco=false) then %>
					<tr>
					  <td class="tablasubrayadaright"><div style="width:100px;height:auto;overflow:hidden"><%=HTMLEncode(adorAtrL("DEN").value)%></div></td>
					<%if adorAtrL("TIPO").value=1 then%> 
					   <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;<%=HTMLEncode(adorAtrL("VALOR_TEXT").value)%></div></td>
					<%end if%>
					<%if adorAtrL("TIPO").value=2 then%> 
					   <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;<%=visualizacionNumero(adorAtrL("VALOR_NUM").value,decimalfmt,thousanfmt,precisionfmt)%></div></td>
					<%end if%>
					<%if adorAtrL("TIPO").value=3 then%>
					   <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;<%=visualizacionFecha(adorAtrL("VALOR_FEC").value,datefmt)%></div></td>
					<%end if%>
					<%if adorAtrL("TIPO").value=4 then%>
					  <%if isnull(adorAtrL("VALOR_BOOL").value) then%>
					    <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;</div></td>
					  <%elseif adorAtrL("VALOR_BOOL").value=0 then%>
					    <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;<%=den(53)%></div></td>
					  <%else%>
					    <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;<%=den(54)%></div></td>
					  <%end if
					end if%>		  
					  </tr>
				 <%end if
				  adorAtrL.movenext
			wend
			adorAtrL.close
			end if%>
		
			<%if not isnull(ador("ADJUNTOS").value) then
				sAdjuntosLinea=""
				set oLinea = oRaiz.Generar_CLinea()
				oLinea.ID = ador("LINEAID").value
				set oAdjuntos = oLinea.cargarAdjuntos(CiaComp,false)
				if oAdjuntos.count > 0 then
					for each oAdjunto in oAdjuntos 
						sAdjuntosLinea=sAdjuntosLinea & oAdjunto.Nombre & " ; "
					next
					sAdjuntosLinea=mid(sAdjuntosLinea,1,len(sAdjuntosLinea)-3)
			%>
					<tr>
					<td class="tablasubrayadaright">
                        <div style="width:100px;height:auto;overflow:hidden"><%=den(49)%>
                        <%if isempty(oLinea.ObsAdjun) then%>
                            <a href='javascript:void(null)' onclick='verAdjuntosLinea(<%=oOrden.id%>,<%=oLinea.ID%>,"")'><img border = 0 src= '../images/clip.gif'></A>
                        <%else%>
                            <a href='javascript:void(null)' onclick='verAdjuntosLinea(<%=oOrden.id%>,<%=oLinea.ID%>,<%=nulltostr(oLinea.ObsAdjun)%>)'><img border = 0 src= '../images/clip.gif'></A>
                        <%end if%>
                        </div>
					</td>
					<td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">&nbsp;<%=sAdjuntosLinea%></div></td>
					</tr>
				<%
					set oAdjuntos=nothing
				end if
				%>
			<%end if%>
			
	  </table></td></tr>
	 <%end if%>
	 
		<%
		if fila="filaImpar" then
			fila ="filaPar"
		else
			fila ="filaImpar"
		end if
		
		
		ador.movenext
	wend
ador.close	
set ador=nothing
	
	
	
	%>
	</table>
	<br>	
	<br>	
	<%
	
if oDestinos.count>0 then%>

	<table width="100%">
		<tr>
			<td colspan="7" class="cabecera"><%=den(31)%></td>
		</tr>
		<tr>
			<td class="cabecera"><%=den(24)%></td>
			<td class="cabecera"><%=den(25)%></td>
			<td class="cabecera"><%=den(26)%></td>
			<td class="cabecera"><%=den(27)%></td>
			<td class="cabecera"><%=den(28)%></td>
			<td class="cabecera"><%=den(29)%></td>
			<td class="cabecera"><%=den(30)%></td>
		</tr>

<%
fila = "filaImpar"
for each oDest in oDestinos	%>

		<tr>
			<td class="<%=fila%>"><%=HTMLEncode(oDest.cod)%></td>
			<td class="<%=fila%>"><%=HTMLEncode(oDest.den)%></td>
			<td class="<%=fila%>"><%=HTMLEncode(oDest.dir)%></td>
			<td class="<%=fila%>"><%=HTMLEncode(oDest.cp)%></td>
			<td class="<%=fila%>"><%=HTMLEncode(oDest.pob)%></td>
			<td class="<%=fila%>"><%=HTMLEncode(oDest.provi)%></td>
			<td class="<%=fila%>"><%=HTMLEncode(oDest.pai)%></td>
	</tr>

<%

next
%>
</table><br><br>
<%
end if
'  SE COMPRUEBA SI SE IMPRIME LA POLITICA DE PEDIDOS
if ParamGen.ImprimirPoliticaPedidos then
	'Linea de separacion
%>
	<HR class="P_acep_pedidos"><BR>
	
<%
	Response.Write oRaiz.PoliticaAceptacionPedidos(Idioma)
end if
set oDesinos = nothing

set oRaiz=nothing
	
%>

