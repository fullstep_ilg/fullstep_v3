﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/fsal_1.asp"-->

<%
''' <summary>
''' Busca los pedidos del usuario
''' </summary>
''' <remarks>Llamada desde: pedidos\pedidos11.asp ; Tiempo máximo: 0,2</remarks>


	Idioma = Request("Idioma")
	set oRaiz=validarUsuario(Idioma,true,true,0)
    
	
	datefmt = Request.Cookies("USU_DATEFMT")	

	CiaComp=request("txtCiaComp")
	
	set oOrdenes=oRaiz.Generar_cOrdenes()
	
	Estado=4
	
	CiaProv=oRaiz.Sesion.CiaCodGs

dim varEquiv
set adorEquiv=oRaiz.ObtenerMonedaCentral(CiaComp)
if not adorEquiv is nothing then
	varEquiv=adorEquiv("EQUIV").value
	sMonPortal = adorEquiv("MON_PORTAL").value
	adorEquiv.close
else
		set oRaiz = nothing
        
		%>
		<html>
		<body onload="window.open('<%=Application("RUTASEGURA")%>script/errormonedas.asp?Idioma=<%=Idioma%>','default_main')">
		</body>
		</html>
		<%
		Response.End 
end if
set adorEquiv=nothing

%>

<script>

p=window.parent.frames["fraPedidosClient"]

function init()
{
if (<%=application("FSAL")%> == 1 )
{
    Ajax_FSALActualizar3(); //registro de acceso
}
<%	
    
    set paramGen = oRaiz.DevolverParamGSPedidos(CiaComp)
	pedDirectoEnvioEmail = paramGen(1)
	if pedDirectoEnvioEmail then
		set ador=oOrdenes.BuscarTodasOrdenes (CiaComp,Idioma,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"),Estado,null,null,null,null,null,null,null,null,CiaProv,null,null,null,null,null,null,0,null,0,null,null,null,null,null,null,1)
	else
		set ador=oOrdenes.BuscarTodasOrdenes (CiaComp,Idioma,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"),Estado,null,null,null,null,null,null,null,null,CiaProv,null,null,null,null,null,null,0,null,0,null,null,null,null,null,1,1)
	end if
%>

p.ordenes = new Array()
<%
if not ador is nothing then
	while not ador.eof
%>
		p.anyadirOrden(<%=ador("id").value%>, <%=ador("anyo").value%>,<%=ador("PEDIDO")%>,<%=ador("numpedido").value%>, <%=ador("numorden").value%>, '<%=JSText(ador("NUMEXT").value)%>', '<%=JSText(ador("PROVECOD").value)%>', '<%=JSText(ador("PROVEDEN").value)%>', <%=JSDate(ador("FECHA").value)%>, <%=ador("EST").value%>, <%=JSNum(ador("IMPORTE").value*ador("CAMBIO").value)%>, '<%=JSAlertText(ador("OBS").value)%>','<%=JSText(ador("REFERENCIA").value)%>',<%if not isnull(ador("ADJUNTOS").value) then%><%=ador("ADJUNTOS").value%><%else%>0<%end if%>,'<%=JSText(ador("APROVISIONADOR").value)%>','<%=JSText(ador("PER").value)%>','<%=JSText(ador("MON").value)%>','<%=JSText(ador("DENEMPRESA").value)%>','<%=JSText(ador("NOMRECEPTOR").value)%>','<%=JSText(ador("COD_PROVE_ERP").value)%>','<%=JSText(ador("RECEPTOR").value)%>', <%=JSNum(ador("EMPRESA").value)%>)
<%	
		ador.movenext
	wend
	ador.close
	set ador = nothing
end if


set oRaiz=nothing
%>
p.mostrarOrdenes()
}

</SCRIPT>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<script src="../common/ajax.js"></script>
<script src="../common/formatos.js"></script>
<!--#include file="../common/fsal_3.asp"-->
</HEAD>

<BODY onload="init()">

</BODY>
<!--#include file="../common/fsal_2.asp"-->
</HTML>
