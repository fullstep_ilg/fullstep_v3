﻿<!DOCTYPE HTML PUBLIC >
<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/fsal_1.asp"-->

<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/menu.asp"></script>
<script SRC="../common/formatos.js"></script>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<script src="../common/ajax.js"></script>
<!--#include file="../common/fsal_3.asp"-->
</head>

<script>
var requiereRefresco

requiereRefresco=true

</script>

<body topmargin="0" leftmargin="0" onload="init()">
<%
''' <summary>
''' Muestra los pedidos permitiendo aceptarlos o rechazarlos
''' </summary>
''' <remarks>Llamada desde: pedidos\pedidos1.asp    pedidos\pedidorechazado.asp ; Tiempo máximo: 0,2</remarks>

	CiaComp = Request("CiaComp")
	CiaCod = Request("CiaCod")
	CiaDen = Request("CiaDen")

	Idioma = Request("Idioma")
	Idioma = trim(Idioma)	

    AccesoExterno=cbool(Application("ACCESO_SERVIDOR_EXTERNO"))    
    if AccesoExterno then
        set oRaiz=validarUsuarioAccesoExterno(Idioma,true,true,0,request.cookies("USU_CIACOD"),request.cookies("USU_USUCOD"),request.cookies("USU_SESIONID_EXTERNO"))
    else
	    set oRaiz=validarUsuario(Idioma,true,true,0)
    end if	
    
	Set paramgen = oRaiz.Sesion.Parametros

%>
<SCRIPT>dibujaMenu(4)</script>


<%

	'Idiomas
	dim den
	den = devolverTextos(Idioma,32)	

	Referencia=oRaiz.DevolverCodPedido(CiaComp,Idioma)

	'obtiene los formatos del usuario
	decimalfmt=request.Cookies ("USU_DECIMALFMT")
	thousandfmt=Request.Cookies ("USU_THOUSANFMT")
	precisionfmt=cint(Request.Cookies ("USU_PRECISIONFMT"))
	datefmt=Request.Cookies("USU_DATEFMT") 
	
	set oProve=nothing
	set oRaiz = nothing
    
%>
<script>

var vdecimalfmt 
var vthousanfmt
var	vprecisionfmt
var vdatefmt

vdecimalfmt='<%=decimalfmt%>'
vthousanfmt='<%=thousandfmt%>'
vprecisionfmt='<%=precisionfmt%>'
vdatefmt='<%=datefmt%>'

/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/	
function init()
{
    if (<%=application("FSAL")%> == 1)
    {
        Ajax_FSALActualizar3(); //registro de acceso
    }
	if (<%=Application("ACCESO_SERVIDOR_EXTERNO")%>==0){
            document.getElementById('tablemenu').style.display = 'block';
        }    
	document.forms["frmPedidos"].txtCiaComp.value="<%=CiaComp%>"
	document.forms["frmPedidos"].submit()   
}

function Aceptar(i)
{
	window.open("<%=Application("RUTASEGURA")%>script/pedidos/pedidos21.asp?Idioma=<%=Idioma%>&Id=" + ordenes[i].id + "&Anyo=" + ordenes[i].anyo + "&pedido=" + ordenes[i].pedido + "&orden=" + ordenes[i].orden + "&Prove=" + ordenes[i].provecod  + "&CiaComp=<%=CiaComp%>&IdPedido=" + ordenes[i].idPedido + "&Referencia=<%=Referencia%>&Mon=" + ordenes[i].mon,"fraPedidosClient")  
}

function Rechazar(i)
{
	window.open("<%=Application("RUTASEGURA")%>script/pedidos/pedidos22.asp?Idioma=<%=Idioma%>&Id=" + ordenes[i].id + "&Anyo=" + ordenes[i].anyo + "&pedido=" + ordenes[i].pedido + "&orden=" + ordenes[i].orden + "&Prove=" + ordenes[i].provecod + "&CiaComp=<%=CiaComp%>&IdPedido=" + ordenes[i].idPedido + "&Referencia=<%=Referencia%>&Mon=" + ordenes[i].mon,"fraPedidosClient") 
}

function Registrar()
{
}

function Orden(id, anyo, idPedido, pedido, numorden, numext, provecod,proveden, fecemision, est,importe,obs,referencia,adjuntos,aprov,codaprov,mon,empresa,receptor,codErp,codReceptor,idEmpresa)
{
	this.id=id
	this.anyo = anyo
	this.idPedido=idPedido
	this.pedido = pedido
	this.orden = numorden
	this.numext = numext
	this.provecod = provecod
	this.proveden = proveden
	this.importe = importe
	this.fecemision = fecemision
	this.est = est
	this.obs=obs
	this.referencia=referencia
	this.adjuntos=adjuntos
	this.aprov=aprov
	this.codaprov=codaprov
	this.mon=mon
	this.empresa = empresa
	this.receptor = receptor
	this.codErp = codErp
	this.codReceptor = codReceptor
	this.idEmpresa = idEmpresa
	
	this.expandido = false
	this.expandir = expandirOrden
	this.lineas = new Array()
}
/*
''' <summary>
''' Expandir una orden
''' </summary>
''' <param name="i">indice del orden a expandir dentro del objeto ordenes</param>
''' <remarks>Llamada desde: pedidos11.asp ; Tiempo máximo: 0,2</remarks>*/
function expandirOrden(i)
{
	if (ordenes[i].lineas.length>0)
		{
		mostrarOrdenes()
		}
	else
		{		
		window.open("<%=Application("RUTASEGURA")%>script/pedidos/pedidos41.asp?Idioma=<%=Idioma%>&CiaComp=<%=CiaComp%>&OrdenId=" + ordenes[i].id + "&Anyo=" + ordenes[i].anyo + "&Pedido=" + ordenes[i].pedido + "&Orden=" + ordenes[i].orden + "&Prove=" + ordenes[i].provecod + "&Referencia=<%=Referencia%>&CiaDen=<%=CiaDen%>", "_self")		
		}
}

function anyadirOrden(id, anyo, idPedido ,pedido, orden, numext, provecod, proveden, fecemision, est, importe,obs,referencia,adjuntos,aprov,codaprov,mon,empresa,receptor,codErp,codReceptor,idEmpresa)
{
	var i

	i = ordenes.length
	ordenes[i]=new Orden(id,anyo,idPedido,pedido,orden, numext,provecod,proveden,fecemision,est,importe,obs,referencia,adjuntos,aprov,codaprov,mon,empresa,receptor,codErp,codReceptor,idEmpresa)
}

function mostrarOrdenes()
{	
	var str
	var i
	var j
	var idActual	

	if (ordenes.length==0)
	  {
		str='<h2><%=den(17)%></h2>'
	  }
	  
	else
	  {
		str = '<table class="principal" border="0" width="95%" cellspacing="2" cellpadding="1">'
		str += '<tr><TD width=5% class=cabecera></TD>'
		str += '<TD width=5% class=cabecera ><%=Den (31)%></TD>'
		str += '<TD width=10% class=cabecera ><%=Den (1)%></TD>'
		<%If paramgen.VerNumeroProveedor Then %>
		str += '<TD width=10% class=cabecera><%=Den(28)%></TD>'  //Nº pedido (Proveedor)
		<%End If %>
		<%if Referencia<>"" then %>
			str += '<TD width=20% class=cabecera><%=Referencia%></TD>'  //referencia
		<%end if%>
		str += '<TD width=15% class=cabecera ><%=Den (2)%></TD>'
		str += '<TD width=20% class=cabecera><%=den(23)%></TD>'  //peticionario
		str += '<TD width=20% class=cabecera><%=den(32)%></TD>'  //receptor
		str += '<TD width=15% class=cabecera ><%=Den (20)%></TD>'
		str += '<TD width=4% class=cabecera><%=den(21)%></TD>'  //obs
		str += '<TD width=4% class=cabecera><%=den(24)%></TD>'  //arch.
		str += '<TD width=4% class=cabecera >&nbsp;</TD>'
		str += '<TD width=4% class=cabecera >&nbsp;</TD>'
		str += '<TD width=1% class=cabecera >&nbsp;</TD>'
		str += '<TD width=1% class=cabecera >&nbsp;</TD></tr>'

		mclass="filaPar"
		
		for (i=0;i<ordenes.length;i++)
		  {
			str += '<TR>'
			
			str += "<TD class=" + mclass + " align=center><a href='javascript:expandirOrden(" + i + ")'>+</a></TD>"
			
			str += "<TD class=" + mclass + "><table width=100%><tr><td width=100%><a href='javascript:void(null)' onclick='DetalleEmpresa(" + i + ")'>" + ordenes[i].empresa + "</a></td>"
			str += "       <td><a href='javascript:void(null)' onclick='DetalleEmpresa(" + i + ")'><IMG border=0 SRC='../images/masinform.gif'></a></td></tr></table></TD>"
			str += "<TD class=" + mclass + ">" + ordenes[i].anyo + "/" + ordenes[i].pedido + "/" + ordenes[i].orden + "</TD>"
			
			<%If paramgen.VerNumeroProveedor Then %>
			str += "<TD class=" + mclass + ">" + ordenes[i].numext + "</TD>"
			<%End If %>
			
			<%if Referencia<>"" then %>	
				str += "<TD class=" + mclass + "><div style='width:100px;height:auto;overflow:hidden'>" + ordenes[i].referencia + "</DIV></TD>"
			<%end if%>
		
			str += "<TD class=" + mclass + ">" + date2str(ordenes[i].fecemision,vdatefmt) + "</TD>"
			
			if (ordenes[i].codaprov != "")
			  {
				str += "<TD class=" + mclass + "><table width=100%><tr><td width=100%><a href='javascript:void(null)' onclick='DetallePet(" + i + ")'>" + ordenes[i].aprov + "</a></td>"
				str += "       <td><a href='javascript:void(null)' onclick='DetallePet(" + i + ")'><IMG border=0 SRC='../images/masinform.gif'></a></td></tr></table></TD>"  //Peticionario
			  }
			else
			  {
			    str += "<TD class=" + mclass + "><table width=100%><tr><td width=100%> </td>"
			    str += "       <td></td></tr></table></TD>"  //Peticionario
			  }

			if (ordenes[i].codReceptor != "")
			  {
				str += "<TD class=" + mclass + "><table width=100%><tr><td width=100%><a href='javascript:void(null)' onclick='DetalleRec(" + i + ")'>" + ordenes[i].receptor + "</a></td>"
				str += "       <td><a href='javascript:void(null)' onclick='DetalleRec(" + i + ")'><IMG border=0 SRC='../images/masinform.gif'></a></td></tr></table></TD>"  //REceptor
			  }
			else
			  {
			    str += "<TD class=" + mclass + "><table width=100%><tr><td width=100%> </td>"
			    str += "       <td></td></tr></table></TD>"  //Receptor
			  }
			
			str += "<TD align=right class=" + mclass + ">" + num2str(ordenes[i].importe ,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + ordenes[i].mon +   "</TD>"
			
			str += "<TD class=" + mclass + ">"   //las observaciones
			if (ordenes[i].obs!="")
				{
				str +="<a href='javascript:void(null)' onclick='alert(ordenes[" + i + "].obs)'><img border = 0 src= '../images/coment.gif'></a>" }
			else
				{str +="&nbsp;"}
			str +="</TD>"
		
			str += "       <TD class=" + mclass + ">"  //los archivos adjuntos
			if (ordenes[i].adjuntos!=0)
				{
				str+= "<a href='javascript:void(null)' onclick='verAdjuntosOrden(ordenes[" + i + "].id)'><img border = 0 src= '../images/clip.gif'></A>"
				}
			else
				{str +="&nbsp;"}
			str +="</TD>"
		
			str += '<TD class=' + mclass + ' width=5%>'
			str += '<a href="javascript:Aceptar(' + i + ')"><%=den(13)%></a>' 
			str += '</TD>'
			str += '<TD class=' + mclass + ' width=5%>'
			str += '<a href="javascript:Rechazar(' + i + ')"><%=den(14)%></a>' 
			str += '</TD>'
			
			str += "<TD align=center class=" + mclass + "><a href='javascript:void(null)' onclick ='exportarPedido(" + ordenes[i].anyo + "," + ordenes[i].pedido + "," + ordenes[i].orden + ")'><IMG border=0 SRC='../images/excelp.gif'></a> </TD>"
			str += "<TD align=center class=" + mclass + "><a href='javascript:void(null)' onclick ='imprimirPedido(" + ordenes[i].anyo + "," + ordenes[i].pedido + "," + ordenes[i].orden + ")'><IMG border=0 SRC='../images/impresorap.gif'></a> </TD>"
			str += '</TR>'
		
				
			if (mclass=="filaPar")
				{
				mclass="filaImpar"
				}
			else
				{
				mclass="filaPar"
				}
			
		   }
		str += '</table>'
		}
	
	document.getElementById("divPedidos").style.visibility="visible"
	document.getElementById("divPedidos").innerHTML=str 

}

function aplicarFormatos(vdec,vthou,vprec,vdate)
{
	vdecimalfmt=vdec
	vthousanfmt=vthou
	vprecisionfmt = vprec
	vdatefmt = vdate
	
	if (document.getElementById("divPedidos").style.visibility=="visible")
	{
		mostrarOrdenes()
	}		
}

var ordenes
ordenes = new Array()


function DetalleEmpresa(i)
{
	winEmp = window.open("<%=application("RUTASEGURA")%>script/common/detalleempresa.asp?Emp=" + ordenes[i].idEmpresa ,"_blank","width=400,height=180,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no,scrollbars=no")
	winEmp.focus()
}

function DetallePet(i)
{
	winDest=window.open("<%=application("RUTASEGURA")%>script/common/detallepet.asp?Idioma=<%=Idioma%>&Pet=" + ordenes[i].codaprov + "&Recep=0" ,"_blank","width=400,height=130,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no,scrollbars=no")
	winDest.focus()
}

function DetalleRec(i)
{
	winDest=window.open("<%=application("RUTASEGURA")%>script/common/detallepet.asp?Idioma=<%=Idioma%>&Pet=" + ordenes[i].codReceptor + "&Recep=1" ,"_blank","width=400,height=130,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no,scrollbars=no")
	winDest.focus()
}


var winEspera
/*
''' <summary>
''' Imprimir los pedidos
''' </summary>     
''' <remarks>Llamada desde: A HREF imprimir ; Tiempo máximo: 0</remarks>*/
function imprimirPedidos()
{

winEspera = window.open ("<%=Application("RUTASEGURA")%>script/common/winEspera.asp?msg=<%=server.urlEncode(den(30))%>&msg2=<%=server.urlEncode(den(29))%>&continua=continuarCargandoPedidos", "_blank", "top=100,left=150,width=400,height=100,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
var acceso = false
while (acceso == false) {
    try {
        while (typeof(winEspera.document.images["imgReloj"])!= 'undefined') {
        }
        acceso = true
    }
    catch (e) {
        acceso =false
    }
	}

}
/*
''' <summary>
''' Imprime los pedidos
''' </summary>
''' <remarks>Llamada desde: seguimclt.asp ; Tiempo máximo: 0,2</remarks>*/
function continuarCargandoPedidos()
{
	window.open("<%=Application("RUTASEGURA")%>script/pedidos/rptpedidos.asp?Idioma=<%=Idioma%>&Referencia=<%=Referencia%>&CiaDen=<%=CiaDen%>","blank","top=100,left=150,width=400,height=100,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no,scrollbars=yes")
	winEspera.focus()
}

/*
''' <summary>
''' Imprime el pedido
''' </summary>
''' <param name="anyo">Anyo de pedido</param>
''' <param name="pedido">Id de pedido</param>   
''' <param name="orden">Orden de pedido</param>        
''' <remarks>Llamada desde: pedidos11.asp ; Tiempo máximo: 0,2</remarks>*/
function imprimirPedido(anyo,pedido,orden)
{
	window.open("<%=Application("RUTASEGURA")%>script/pedidos/rptpedido.asp?Idioma=<%=Idioma%>&anyo=" + anyo + "&pedido=" + pedido + "&orden=" + orden + "&Referencia=<%=Referencia%>&CiaDen=<%=CiaDen%>" ,"blank","top=50,left=50,width=700,height=500,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no,scrollbars=yes")
}
/*
''' <summary>
''' Exportar los pedidos
''' </summary>     
''' <remarks>Llamada desde: A HREF Exportar ; Tiempo máximo: 0</remarks>*/
function exportarPedidos()
{
var re
var re2
re=/MSIE 5.5/
re2=/Q299618/
	
re3 = /SP1/

if (navigator.userAgent.search(re)!=-1 && navigator.appMinorVersion.search(re2)==-1 && navigator.appMinorVersion.search(re3)!=-1)
	{
	window.open("<%=Application("RUTASEGURA")%>script/common/mspatch.asp","_blank","top=100,left=150,width=300,height=250,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
	window.close()
	}
else
	{
	top.winEspera = window.open ("<%=Application("RUTASEGURA")%>script/common/winEspera.asp", "_blank", "top=100,left=150,width=400,height=100,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
    while (typeof(top.winEspera.document.images["imgReloj"])!= 'undefined') 
		{
		}
	window.open("<%=Application("RUTASEGURA")%>script/pedidos/xlspedidos.asp" ,"fraPedidosServer")
	}
}
/*
''' <summary>
''' Exportar el pedido
''' </summary>     
''' <param name="anyo">Anyo de pedido</param>
''' <param name="pedido">Id de pedido</param>   
''' <param name="orden">Orden de pedido</param>   
''' <remarks>Llamada desde: A HREF Exportar ; Tiempo máximo: 0</remarks>*/
function exportarPedido(anyo,pedido,orden)
{
var re
var re2
re=/MSIE 5.5/
re2=/Q299618/
	
re3 = /SP1/

if (navigator.userAgent.search(re)!=-1 && navigator.appMinorVersion.search(re2)==-1 && navigator.appMinorVersion.search(re3)!=-1)
	{
	window.open("<%=Application("RUTASEGURA")%>script/common/mspatch.asp","_blank","top=100,left=150,width=300,height=250,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
	window.close()
	}
else
	{
	top.winEspera = window.open ("<%=Application("RUTASEGURA")%>script/common/winEspera.asp", "_blank", "top=100,left=150,width=400,height=100,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
    while (typeof(top.winEspera.document.images["imgReloj"])!= 'undefined') 
		{
		}
	window.open("<%=Application("RUTASEGURA")%>script/pedidos/xlspedidos.asp?anyo=" + anyo + "&pedido=" + pedido + "&orden=" + orden ,"fraPedidosServer")
	}

}

function verAdjuntosOrden(id)
{
   window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/adjuntosorden.asp?Orden=" + id, "_blank", "top=5,left=5,width=600, height=250,directories=no,menubar=no,scrollbars=yes,resizable=yes,toolbar=yes")
}


</script>



<table width="100%">
	<tr>
		<td width="98%">
			<h1><%=den(15)%>&nbsp;<span class="negro"><%=Request("CiaDen")%></span></h1>
		</td>
		<td width="2%" align="right">
			<a href="javascript:void(null)" onclick="exportarPedidos()"><img border="0" SRC="../images/excelg.GIF" WIDTH="28" HEIGHT="28"></a>
		</td>
		<td width="2%" align="right">
			<a href="javascript:void(null)" onclick="imprimirPedidos()"><img border="0" SRC="../images/impresora.GIF" WIDTH="41" HEIGHT="38"></a>
		</td>
	</tr>
</table>

<h3><%=Den (3)%></h3>

<div name="divPedidos" id="divPedidos">
<form name="frmPedidos" method="post" action="buscarpedidos.asp?Idioma=<%=Idioma%>" target="fraPedidosServer">
<input type="hidden" name="txtCiaComp">

</form>
</div>
</body>
<!--#include file="../common/fsal_2.asp"-->
</html>

