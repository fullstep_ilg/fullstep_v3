﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/formatos.asp"-->
 
<% 
''' <summary>
''' Exporta a excel los pedidos
''' </summary>
''' <remarks>Llamada desde: pedidos\pedidos11.asp   pedidos\pedidos11dir.asp    pedidos\pedidos41.asp; Tiempo máximo: 0,2</remarks>

OldTimeOut = Server.ScriptTimeout 
Server.ScriptTimeout=600
	
datefmt = Request.Cookies("USU_DATEFMT")	
Idioma = Request.Cookies("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,true,true,0)

ciacomp=oRaiz.Sesion.CiaComp
CiaProv=oRaiz.Sesion.CiaCodGs
Prove = clng(oRaiz.Sesion.CiaId)
set oProve = oraiz.DevolverProveedorEnCiaComp(Idioma,Prove,CiaComp)


anyo = request("anyo")
pedido= request("pedido")
orden= request("orden")

set oXLS = oRaiz.Generar_CExcel()
est=4

s=""
lowerbound=65
upperbound=90
randomize
for i = 1 to 10
	s= s & chr(Int((upperbound - lowerbound + 1) * Rnd + lowerbound))
next	


strFECHAHORA="FULLSTEP_EP_" & day(now()) & "_" & month(now()) & "_" & year(now()) & "_" & hour(now()) & "_" & minute(now()) & "_" & second(now()) & ".xls"

sPath = Application ("CARPETAUPLOADS") & "\" & s & "\" & strFECHAHORA

Equiv = 1

oXLS.GenerarExcelOrdenes Idioma,ciaComp,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"), est,null,null,null,null,null,CiaProv,null,0, anyo, pedido, orden, sPath, null,0,null,1,0

datasize = oRaiz.DataSizeFile(sPath)

svPath = s

Server.ScriptTimeout=OldTimeout
%>
<html>
    <head>
        <% 
            Idioma = Request.Cookies("USU_IDIOMA")
            
        %>
        <title><%=title%></title>
    </head>
    
<script>

var p


top.winEspera.close()


window.open("<%=application("RUTASEGURA")%>script/common/download.asp?path=<%=server.urlencode(svPath)%>&nombre=<%=server.urlencode(strFECHAHORA)%>&datasize=<%=server.urlencode(dataSize)%>","_blank","top=100,left=150,width=630,height=300,location=no,menubar=yes,resizable=yes,scrollbars=auto,toolbar=no")


</script>


</BODY>
</HTML>
<%


set oRaiz = nothing

Response.End
%>

