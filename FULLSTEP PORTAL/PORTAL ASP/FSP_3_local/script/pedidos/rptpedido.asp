﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<%
''' <summary>
''' Imprime un pedido
''' </summary>
''' <remarks>Llamada desde: pedidos\pedidos11.asp   pedidos\pedidos11dir.asp    pedidos\pedidos41.asp; Tiempo máximo: 0,2</remarks>

	Idioma = Request("Idioma")
	Idioma = trim(Idioma)

	set oRaiz=validarUsuario(Idioma,true,false,0)
	
	CiaComp = oRaiz.Sesion.CiaComp
	CiaCod = oRaiz.Sesion.CiaCod
	anyo=request("anyo")
	pedido=request("pedido")
	orden=request("orden")
	Referencia=request("Referencia")
	'Idiomas
	dim denPrincipal
	denPrincipal = devolverTextos(Idioma,85)	

    CiaDen = Request("CiaDen")
%>
<html>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/formatos.js"></script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<% 
    
%>
<title><%=title%></title>

<script>
function Imprimir()
{	
	document.getElementById("cmdImprimir").style.visibility = "hidden"
	window.print()
	document.getElementById("cmdImprimir").style.visibility = "visible"
}

</script>
</head>

<body>
<h1><%=denPrincipal(15)%>&nbsp;&nbsp;&nbsp;&nbsp;<span class="negro"><%=CiaDen%></span></h1>
<!--#include file="rptPedidoDetalle.asp"-->
<br>
<center><input id="cmdImprimir" style="font-size:15px;font-weight:bold;" type="image" src="<%=APPLICATION("RUTASEGURA")%>/script/images/impresora.gif" onclick="javascript:Imprimir()"></center>

</body>
</html>