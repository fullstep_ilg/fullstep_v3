﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->

<%
''' <summary>
''' Imprime los pedidos
''' </summary>
''' <remarks>Llamada desde: pedidos\pedidos11.asp   pedidos\pedidos11dir.asp; Tiempo máximo: 0,2</remarks>

	Idioma = Request("Idioma")
	Idioma = trim(Idioma)

	set oRaiz=validarUsuario(Idioma,true,true,0)
    
    set ParamGen = oRaiz.Sesion.Parametros
	
	CiaComp = oRaiz.Sesion.CiaComp
	CiaCod = oRaiz.Sesion.CiaCod

	'jpa
	dim esblanco
	
	'Idiomas
	dim den
	den = devolverTextos(Idioma,85)	
	
	'obtiene los formatos del usuario
	decimalfmt=request.Cookies ("USU_DECIMALFMT")
	thousanfmt=Request.Cookies ("USU_THOUSANFMT")
	precisionfmt=cint(Request.Cookies ("USU_PRECISIONFMT"))
	datefmt=Request.Cookies("USU_DATEFMT") 	
	Referencia=request("Referencia")
    CiaDen = Request("CiaDen")

	set oOrdenes=oRaiz.Generar_cOrdenes()
	
	Estado=4
	
	CiaProv=oRaiz.Sesion.CiaCodGs
	
	dim sAdjuntosLinea
	dim sAdjuntosOrden
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><%=title%></title>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/formatos.js"></script>

<script>

function Imprimir()
{	
	document.getElementById("cmdImprimir").style.visibility = "hidden"
	window.print()
	document.getElementById("cmdImprimir").style.visibility = "visible"
}
function resize()
	{
	p = window.opener 
	p.winEspera.close()
	window.moveTo(60,20)
	window.resizeTo (700,550)
	}

function verAdjuntosOrden(id)
{
    window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/adjuntosorden.asp?Orden=" + id, "_blank", "top=5,left=5,width=600, height=250,directories=no,menubar=no,scrollbars=yes,resizable=yes,toolbar=yes")
}

function verAdjuntosLinea(orden,id,obs)
{
    window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/adjuntoslinea.asp?Orden=" + orden + "&Linea=" + id + "&Obs=" + obs, "_blank", "top=5,left=5,width=600, height=250,directories=no,menubar=no,scrollbars=yes,resizable=yes,toolbar=yes")
}

</script>
</head>

<body onload="resize()">
<h1><%=den(15)%>&nbsp;&nbsp;&nbsp;&nbsp;<span class="negro"><%=HTMLEncode(CiaDen)%></span></h1>
<%

Set oPersonas = oRaiz.Generar_CPersonas()

oPersonas.CargarTodasLasPersonas CiaComp
set paramGen = oRaiz.DevolverParamGSPedidos(CiaComp)
pedDirectoEnvioEmail = paramGen(1)
pedDirFac = paramGen(2)
	if pedDirectoEnvioEmail then
		set ador=oOrdenes.BuscarTodasOrdenes (CiaComp,Idioma,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"),Estado,null,null,null,null,null,null,null,null,CiaProv,null,null,null,null,null,null,0,null,0,null,null,null,null,null,null,1)
	else
		set ador=oOrdenes.BuscarTodasOrdenes (CiaComp,Idioma,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"),Estado,null,null,null,null,null,null,null,null,CiaProv,null,null,null,null,null,null,0,null,0,null,null,null,null,null,1,1)
	end if


if not ador is nothing then
	
	while not ador.eof 

	%>
		<table width="100%">
			<tr>
				<td>
					<table width="100%">
						<tr>
							<td rowspan="2" width="33%" class="cabecera"><%=den(41)%></td>
							<td class="filaPar" colspan="3">
								<%=ador.fields("NIFEMPRESA").value%>
							</td>
						</tr>
						<tr>
							<td class="filaPar" colspan="3">
								<%=HTMLEncode(ador.fields("DENEMPRESA").value)%>
							</td>
						</tr>
						<% if pedDirFac then %>
						<tr>
							<td class="cabecera"><%=den(59)%></td>
							<td class="filaPar" colspan="3">
								<%=HTMLEncode(ador.fields("DIRFAC").value)%>
							</td>							
						</tr>
						<%end if%>

					</table>
				</td>
			</tr>
		</table>
	<%
		OrdenId=ador("id").value			
		set oOrden = oRaiz.Generar_COrden()
		oOrden.id = clng(OrdenId)

		set adorAtr = oOrden.DevolverAtributos(CiaComp)

        If Not oPersonas.Item(ador.Fields("PER").Value) Is Nothing Then
			set oPersona = oPersonas.Item(ador.Fields("PER").Value)
		else
			set oPersona =nothing
        End If
        If Not oPersonas.Item(ador.Fields("RECEPTOR").Value) Is Nothing Then
			set oPersonaR = oPersonas.Item(ador.Fields("RECEPTOR").Value)
		else
			set oPersonaR =nothing
        End If

		set oDestinos=oRaiz.Generar_cDestinos()	

        'Costes y descuentos
        if ador("HAYCOSTES")>0 or ador("HAYDESCUENTOS")>0 then
            set adorCostesDesc=oOrden.DevolverCostesDescuentosCab(CiaComp)        
        end if	
        dImporteTotal=ador("IMPORTE")*ador("CAMBIO")
        dImporteBruto=0
        set adorImp = oOrden.Devolverlineas(CiaComp,Idioma)
        if not adorImp.eof then
	        while not adorImp.eof
                dImporteBrutoLin=adorImp("IMPORTE").value
                if not isnull(adorImp("COSTES")) then dCostesLin=adorImp("COSTES")*ador("CAMBIO")
                if not isnull(adorImp("DESCUENTOS")) then dDescuentosLin=adorImp("DESCUENTOS")*ador("CAMBIO")
                dImporteBruto=dImporteBruto+dImporteBrutoLin+dCostesLin-dDescuentosLin
                adorImp.movenext
            wend
        end if
        set oLineasDesgloseImpuesto = oRaiz.Generar_CLineasDesgImpuesto
        oLineasDesgloseImpuesto.cargarLineasDesgloseImpuestoOrden oOrden.id,CiaComp,Idioma
	
    %>
		<table width="100%" style="border:solid 1 black;page-break-after:always;">
			<tr>
				<td>
		<table width="100%">
			<tr>
				<td width="30%" class="cabecera"><%=den(1)%></td>
				<td width="20%" class="filaPar">
					<%=ador("anyo").value%>/<%=ador("numpedido").value%>/<%=ador("numorden").value%>
				</td>
				<td width="20%" class="cabecera"><%=den(51)%></td>
				<td width="30%" class="filaPar">
					<%=ador("NUMEXT").value%>
				</td>				
			</tr>
			
			<%if Referencia="" then%>
				<tr>
					<td width="30%" class="cabecera"><%=den(2)%></td>
					<td width="20%" class="filaPar">
						<%
						if not isnull(ador("FECHA").value) then
							Response.write VisualizacionFecha(ador("FECHA").value,datefmt)
						else
							Response.Write "&nbsp;"
						end if%>
					</td>
					<td width="20%" class="cabecera"><%=den(52)%></td>
					<td width="30%" class="filaPar" colspan="5">
						<%VB2HTML(ador.fields("PAG").value & " - " & ador.fields("PAGDEN").value)%>
					</td>
				</tr>
				<tr>													
					<td width="30%" class="cabecera"><%=den(32)%></td>
					<td width="20%" class="filaPar" colspan="3">
						<%
						select case ador("EST").value
							case 2:
								Response.Write den(47)
							case 3:
								Response.Write den(36)
							case 4:
								Response.Write den(37)
							case 5:
								Response.Write den(38)
							case 6:
								Response.Write den(39)
							case 21:
								Response.Write den(40)
						end select%>
					</td>					
				</tr>
			<%else%>
				<tr>
					<td width="30%" class="cabecera"><%=HTMLEncode(Referencia)%></td>
					<td width="20%" class="filaPar"><%=HTMLEncode(ador("REFERENCIA").value)%></td>
					<td width="20%" class="cabecera"><%=den(2)%></td>
					<td width="30%" class="filaPar">
						<%
						if not isnull(ador("FECHA").value) then
							Response.write VisualizacionFecha(ador("FECHA").value,datefmt)
						else
							Response.Write "&nbsp;"
						end if%>
					</td>
				</tr>					
				<tr>
					<td width="30%" class="cabecera"><%=den(52)%></td>
					<td width="20%" class="filaPar">
						<%VB2HTML(ador.fields("PAG").value & " - " & ador.fields("PAGDEN").value)%>
					</td>						
				    <td width="20%" class="cabecera"><%=den(32)%></td>
				    <td class="filaPar">
					    <%
					    select case ador("EST").value
						    case 2:
							    Response.Write den(47)
						    case 3:
							    Response.Write den(36)
						    case 4:
							    Response.Write den(37)
						    case 5:
							    Response.Write den(38)
						    case 6:
							    Response.Write den(39)
						    case 21:
							    Response.Write den(40)
					    end select%>
				    </td>
			    </tr>			
			<%end if			
			if not isnull(ador("ADJUNTOS").value) then
				'Carga los adjuntos
				sAdjuntosOrden=""
				set oAdjuntosOrden = oOrden.cargarAdjuntos(CiaComp,false)
				for each oAdjunto in oAdjuntosOrden
					sAdjuntosOrden=sAdjuntosOrden & oAdjunto.Nombre & " ; "
				next
				sAdjuntosOrden=mid(sAdjuntosOrden,1,len(sAdjuntosOrden)-3)
			
			%>
			    <tr>	
				    <td class="cabecera" width="20%"><%=den(49)%><a href='javascript:void(null)' onclick='verAdjuntosOrden(<%=oOrden.id%>)'><img border = 0 src= '../images/clip.gif'></A></td>
				    <td class="filaPar" colspan="3"><%=sAdjuntosOrden%></td>
			    </tr>
			<%end if%>
			<%if not isnull(ador("OBS").value) then%>
			<tr>	
				<td class="cabecera" width="20%"><%=den(21)%></td>
				<td class="filaPar" colspan="3"><%=HTMLEncode(ador("OBS").value)%></td>
			</tr>
			<%end if%>
			<%if not adorAtr is nothing then
				while not adorAtr.eof												    
					esblanco = false
					if adorAtr("TIPO").value=1 then
						if  isnull(adorAtr("VALOR_TEXT").value) then
							esblanco=true
						elseif  Len(adorAtr("VALOR_TEXT").value) = 0 then
							esblanco=true
						end if						
					else		  
						if adorAtr("TIPO").value=2 then
							if isnull(adorAtr("VALOR_NUM").value) then
							  esblanco=true
							end if
						else
							if adorAtr("TIPO").value=3 then
								if isnull(adorAtr("VALOR_FEC").value) then
								  esblanco=true
								end if
							else
								if adorAtr("TIPO").value=4 then
									if isnull(adorAtr("VALOR_BOOL").value) then
									  esblanco=true
									end if
								end if
							end if
						end if
					end if			  
					if (esblanco=false) then %>
						<tr>
							<td width="30%" class="cabecera"><%=HTMLEncode(adorAtr("DEN").value)%></td>
						<%if adorAtr("TIPO").value=1 then%> 
						    <td class="filaPar" colspan="3"><%=VB2HTML(adorAtr("VALOR_TEXT").value)%></td>
						<%end if%>
						<%if adorAtr("TIPO").value=2 then%> 
						    <td class="filaPar" colspan="3"><%=visualizacionNumero(adorAtr("VALOR_NUM").value,decimalfmt,thousanfmt,precisionfmt)%></td>
						<%end if%>
						<%if adorAtr("TIPO").value=3 then%>
						    <td class="filaPar" colspan="3"><%=visualizacionFecha(adorAtr("VALOR_FEC").value,datefmt)%></td>
						<%end if%>
						<%if adorAtr("TIPO").value=4 then%>
						  <%if isnull(adorAtr("VALOR_BOOL").value) then%>
						      <td class="filaPar" colspan="3">&nbsp;</td>
						  <%elseif adorAtr("VALOR_BOOL").value=0 then%>
						    <td class="filaPar" colspan="3"><%=den(53)%></td>
						  <%else%>
						    <td class="filaPar" colspan="3"><%=den(54)%></td>
						  <%end if
						end if%>
						</tr>
					<%end if				
				    adorAtr.movenext
				wend
			end if%>

            <!--IMPORTES-->
            <tr><td class="cabecera" colspan="4"><%=Den(61)%></td></tr>
            <tr>
                <td class="cabecera"><%=Den(62)%></td>
                <td class="filaPar" style="text-align:right;">
					<%=replace(FormatNumber(dImporteBruto,2),",",".")%>&nbsp;<%=ador("MON").value%>
                    <%MonedaPedido=ador("MON").value%>
					<%CambioPedido=ador("CAMBIO").value%>					
				</td>
                <td class="filaPar" colspan="2"/>
            </tr>
            <!--Costes/descuentos-->
            <%if ador("HAYCOSTES")>0 or ador("HAYDESCUENTOS")>0 then
                if not adorCostesDesc is nothing then
                    adorCostesDesc.movefirst
                    while not adorCostesDesc.eof                    
                        sDescCD=VisualizacionCosteDesc(iif(isnull(adorCostesDesc("DEN")),"",adorCostesDesc("DEN")),adorCostesDesc("TIPO"),adorCostesDesc("OPERACION"),adorCostesDesc("VALOR"),ador("MON").value,CambioPedido)%>
                        <tr>
                            <td class="cabecera"><%=sDescCD%></td>
                            <td class="filaPar" style="text-align:right;">
                                <%=replace(FormatNumber((adorCostesDesc("IMPORTE")*CambioPedido),2),",",".")%>&nbsp;<%=ador("MON").value%>
                            </td>
                            <td class="filaPar" colspan="2"/>
                        </tr>
                        <%adorCostesDesc.movenext
                    wend
                end if
            end if%>  
            <!--Impuestos-->
            <%for each oLineaDesgloseImpuesto in oLineasDesgloseImpuesto%>
            <tr>
                <td class="cabecera">
                    <%=Den(73)%>&nbsp;<%=oLineaDesgloseImpuesto.tipoimpuesto%>&nbsp;<%=oLineaDesgloseImpuesto.TipoValor%>%&nbsp;
                    (<%=Den(74)%>:&nbsp;<%=replace(FormatNumber(oLineaDesgloseImpuesto.BaseImponible,2),",",".")%>&nbsp;<%=ador("MON").value%>)
                </td>
                <td class="filaPar" style="text-align:right;">
                    <%=replace(FormatNumber(oLineaDesgloseImpuesto.cuota,2),",",".")%>&nbsp;<%=ador("MON").value%>
                </td>
                <td class="filaPar" colspan="2"/>
            </tr>
            <% next%>   
            <!--Importe total-->        
            <tr>
                <td class="cabecera"><%=Ucase(Den(63))%></td>                
				<td class="filaPar" style="text-align:right;">
					<%=replace(FormatNumber(dImporteTotal,2),",",".")%>&nbsp;<%=ador("MON").value%>					
				</td>
                <td class="filaPar" colspan="2"/>	
            </tr>
			
			<%if not oPersona is nothing then %>			
			<tr colspan="2"><td class="cabecera" colspan="4"><%=den(55)%></td></tr>
			<tr>
				<td class="cabecera" width="20%"><%=den(44)%></td>			
				<td width="30%" class="filaPar">		
					<%=HTMLEncode(oPersona.nombre)%>&nbsp;<%=HTMLEncode(oPersona.Apellidos)%>
				</td>
				<td width="20%" rowspan="3" class="cabecera" valign="top"><%=den(48)%></td>
				<td width="30%" class="filaPar"><%=HTMLEncode(oPersona.UON1DEN)%></td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(45)%></td>
				<td class="filaPar"><%=HTMLEncode(oPersona.tfno)%></td>
				<td class="filaPar">
					<%if not isnull(oPersona.uon2) then%>
						<%=HTMLEncode(oPersona.UON2DEN)%>
					<%end if%>
				</td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(46)%></td>
				<td class="filaPar">
						<a href="mailto:<%=oPersona.email%>"><%=VB2HTML(oPersona.email)%></a>
				</td>
				<td class="filaPar">
					<%if not isnull(oPersona.uon3) then%>
						<%=HTMLEncode(oPersona.UON3DEN)%>
					<%end if%>
				</td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(57)%></td>
				<td class="filaPar"><%=VB2HTML(oPersona.fax)%></td>			
				<td class="cabecera"><%=den(42)%></td>
				<td class="filaPar"><%=VB2HTML(oPersona.depden)%></td>
			 </tr>		
		<%end if%>
		<%if not oPersonaR is nothing then %>			
			<tr><td class="cabecera" colspan="4"><%=den(56)%></td></tr>
			<tr>
				<td class="cabecera" width="20%"><%=den(44)%></td>			
				<td width="30%" class="filaPar">		
					<%=HTMLEncode(oPersonaR.nombre)%>&nbsp;<%=HTMLEncode(oPersonaR.Apellidos)%>
				</td>
				<td width="20%" rowspan="3" class="cabecera" valign="top"><%=den(48)%></td>
				<td width="30%" class="filaPar"><%=VB2HTML(oPersonaR.UON1DEN)%></td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(45)%></td>
				<td class="filaPar"><%=HTMLEncode(oPersonaR.tfno)%></td>
				<td class="filaPar">
					<%if not isnull(oPersonaR.uon2) then%>
						<%=HTMLEncode(oPersonaR.UON2DEN)%>
					<%end if%>
				</td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(46)%></td>
				<td class="filaPar">
						<a href="mailto:<%=oPersonaR.email%>"><%=VB2HTML(oPersonaR.email)%></a>
				</td>
				<td class="filaPar">
					<%if not isnull(oPersonaR.uon3) then%>
						<%=HTMLEncode(oPersonaR.UON3DEN)%>
					<%end if%>
				</td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(57)%></td>
				<td class="filaPar"><%=VB2HTML(oPersonaR.fax)%></td>			
				<td class="cabecera"><%=den(42)%></td>
				<td class="filaPar"><%=VB2HTML(oPersonaR.depden)%></td>
			 </tr>		
		<%end if%>

		</table>
		<br>
		<br>	
		<%
				
		set adorLinea = oOrden.Devolverlineas(CiaComp,Idioma)

		iNumLinPlanEnt=0
        iNumLinPlanEntPub=0
        bOcultarFecEntrega=false
		if not adorLinea is nothing then
            'Ocultar columna fecha de entrega               
            if adorLinea.recordcount>0 then
                while not adorLinea.eof               
                    if adorLinea("HAYPLANES")>0 then
                        iNumLinPlanEnt=iNumLinPlanEnt+1
                        if adorLinea("PUB_PLAN_ENTREGA")=1 then iNumLinPlanEntPub=iNumLinPlanEntPub+1
                    end if
                    adorLinea.movenext
                wend
            end if

            if iNumLinPlanEnt>0  then
                if iNumLinPlanEntPub>0 then set adorPlanes=oOrden.DevolverPlanesEntrega(CiaComp)            
                if (iNumLinPlanEnt=ador.recordcount) then bOcultarFecEntrega=true                    
            end if

            adorLinea.movefirst 
		%>
				
		<table width="100%">
			<tr>
				<td class="cabecera"><%=den(4)%></td>
				<td class="cabecera"><%=den(5)%></td>
				<td class="cabecera"><%=den(6)%></td>
				<td class="cabecera"><%=den(7)%></td>
				<td class="cabecera"><%=den(8)%></td>
				<%if not bOcultarFecEntrega then %>
				    <td class="cabecera"><%=den(9)%></td>
				    <td class="cabecera"><%=den(10)%></td>
                <%end if%>
				<td class="cabecera"><%=den(11)%></td>
				<td class="cabecera"><%=den(12)%></td>
                <td class="cabecera"><%=den(65)%>&nbsp;(<%=MonedaPedido%>)</td>
                <td class="cabecera"><%=den(66)%>&nbsp;(<%=MonedaPedido%>)</td>
				<td class="cabecera"><%=den(20)%>&nbsp;(<%=MonedaPedido%>)</td>
			</tr>
			<%	

		fila = "filaImpar"
		
		while not adorLinea.eof%>
					
			<tr>
				<td class="<%=fila%>">
					<%=adorLinea("codart").value%>
				</td>
				<td class="<%=fila%>">
					<%=adorLinea("art_ext").value%>
				</td>
				<td class="<%=fila%>">
					<%=adorLinea("denart").value%>
				</td>
				<td class="<%=fila%>">
					<%=adorLinea("DESTINO").value%>
							
					<%if oDestinos.item(adorLinea("DESTINO").value) is nothing then
						set adorDest = oDestinos.CargarDatosDestino(Idioma,Ciacomp, adorLinea("DESTINO").value, adorLinea("LINEAID").value)
						oDestinos.Add adorDest("COD").Value, adorDest("DEN").Value, false, adorDest("DIR"), adorDest("POB"), adorDest("CP"), adorDest("PROVI"), adorDest("PAI")
						adorDest.close
						set adorDest = nothing
					end if%>
							
				</td>
				<td class="<%=fila%>">
					<nobr><%=adorLinea("UNIDAD").value%>-<%=adorLinea("UNIden").value%></nobr>
				</td>
				<%if not bOcultarFecEntrega then %>
                    <%if adorLinea("HAYPLANES")>0 then%>                                           
                        <td class="<%=fila%>">&nbsp;</td>                                             
                        <td class="<%=fila%>">&nbsp;</td> 
                    <%else%>
			            <td class="<%=fila%>">
				            <%
				            if not isnull(adorLinea("FECENTREGA").value) then                            
					            Response.write VisualizacionFecha(adorLinea("FECENTREGA").value,datefmt)
				            else
					            Response.Write "&nbsp;"
				            end if%>
			            </td>
                        <td class="<%=fila%>">
				            <%if adorLinea("ENTREGA_OBL").value=1 then
					            Response.Write den(18)
				            else
					            Response.Write den(19)
				            end if%>
			            </td>
                    <%end if%>                 
                <%end if%>
				<%if adorLinea("TIPORECEPCION").value=1 then %>
                    <td class="<%=fila%> numero">
					    &nbsp;
				    </td> 
                <%else %> 	
				    <td class="<%=fila%> numero">
					    <%=visualizacionNumero(adorLinea("CANTIDAD").value,decimalfmt,thousanfmt,precisionfmt)%>
				    </td>
                <%end if %>
				<td class="<%=fila%> numero">
					<%=visualizacionNumero(adorLinea("PRECIOUNITARIO").value ,decimalfmt,thousanfmt,precisionfmt)%>			
				</td>
                <td class="<%=fila%> numero">
				    <%=visualizacionNumero(adorLinea("COSTES").value * CambioPedido ,decimalfmt,thousanfmt,precisionfmt)%>
			    </td>
                <td class="<%=fila%> numero">
				    <%=visualizacionNumero(adorLinea("DESCUENTOS").value * CambioPedido ,decimalfmt,thousanfmt,precisionfmt)%>
			    </td>
				<td class="<%=fila%> numero">
					<%=visualizacionNumero(adorLinea("IMPORTE").value+(adorLinea("COSTES").value-adorLinea("DESCUENTOS").value) * CambioPedido ,decimalfmt,thousanfmt,precisionfmt)%>
				</td>
			</tr>
        
        <!--COSTES y DESCUENTOS-->
        <%if adorLinea("HAYCOSTES")>0 or adorLinea("HAYDESCUENTOS")>0 then
            set adorCDs = oOrden.DevolverCostesDescuentosLinea(adorLinea("LINEAID").value,CiaComp)   
            
            if not adorCDs is nothing then%>
                <tr>
                    <%if not bOcultarFecEntrega then %>
                        <td colspan="9"></td>
                    <%else%>
                        <td colspan="7"></td>
                    <%end if%>
                    <td class="cabecera" colspan="3"><%=Den(67)%></td>
                </tr>
                <%if adorLinea("HAYCOSTES")>0 then%>
                    <tr>
                        <%if not bOcultarFecEntrega then %>
                            <td colspan="9"></td>
                        <%else%>
                            <td colspan="7"></td>
                        <%end if%>
                        <td class="cabecera" colspan="2"><%=Den(68)%></td>
                        <td class="cabecera"><%=Den(70)%>&nbsp;(<%=MonedaPedido%>)</td>                   
                    </tr>
                    <%adorCDs.movefirst
                    adorCDs.filter="TIPO=0"
                    while not adorCDs.eof
                        sDescCD=VisualizacionCosteDesc(iif(isnull(adorCDs("DEN")),"",adorCDs("DEN")),adorCDs("TIPO"),adorCDs("OPERACION"),adorCDs("VALOR"),MonedaPedido,CambioPedido)%>
                        <tr>
                            <%if not bOcultarFecEntrega then %>
                                <td colspan="9"></td>
                            <%else%>
                                <td colspan="7"></td>
                            <%end if%>
                            <td class=<%=fila%> colspan="2"><%=sDescCD%></td>
                            <td class=<%=fila%> style="text-align:right;"><%=visualizacionNumero((adorCDs("IMPORTE")*CambioPedido),decimalfmt,thousanfmt,precisionfmt)%></td>                            
                        </tr>
                        <%adorCDs.movenext
                    wend
                end if
                if adorLinea("HAYDESCUENTOS")>0 then%>
                    <tr>
                        <%if not bOcultarFecEntrega then %>
                            <td colspan="9"></td>
                        <%else%>
                            <td colspan="7"></td>
                        <%end if%>
                        <td class="cabecera" colspan="2"><%=Den(69)%></td>
                        <td class="cabecera"><%=Den(71)%>&nbsp;(<%=MonedaPedido%>)</td>
                    </tr>
                    <%adorCDs.movefirst
                    adorCDs.filter="TIPO=1"
                    while not adorCDs.eof
                        sDescCD=VisualizacionCosteDesc(iif(isnull(adorCDs("DEN")),"",adorCDs("DEN")),adorCDs("TIPO"),adorCDs("OPERACION"),adorCDs("VALOR"),MonedaPedido,CambioPedido)%>
                        <tr>
                            <%if not bOcultarFecEntrega then %>
                                <td colspan="9"></td>
                            <%else%>
                                <td colspan="7"></td>
                            <%end if%>
                            <td class=<%=fila%> colspan="2"><%=HTMLEncode(sDescCD)%></td>
                            <td class=<%=fila%> style="text-align:right;"><%=visualizacionNumero((adorCDs("IMPORTE")*CambioPedido),decimalfmt,thousanfmt,precisionfmt)%></td>                            
                        </tr>
                        <%adorCDs.movenext
                    wend
                end if
            end if  
        end if%>
        		
		<%set adorAtrL = oOrden.DevolverAtributosLinea(adorLinea("LINEAID").value,CiaComp)	
			if not (adorAtrL is nothing) or not isnull(adorLinea("CODPROCE").value) or (not isnull(adorLinea("OBS").value) AND adorLinea("OBS").value<>"") or not isnull(adorLinea("DENCAMPO1").value) or not isnull(adorLinea("DENCAMPO2").value) or not isnull(adorLinea("ADJUNTOS").value) then %>

			  <tr><td>&nbsp;</td><td colspan="11"><table width="100%" CELLSPACING="0">
                <%if adorLinea("HAYPLANES")>0 then
                    if adorLinea("PUB_PLAN_ENTREGA")=1 then%> 
                        <tr>
                            <td class="tablasubrayadaright" valign="top"><div style="width:120px;height:auto;overflow:hidden;text-align:top;"><%=den(60)%></div></td>
                        
                            <%if not adorPlanes is nothing then
                                adorPlanes.filter="ID=" & adorLinea("LINEAID")%>
                                <td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">
                                <%if adorPlanes.recordcount>0 then%> 
                                    <table>                                        
                                        <%while not adorPlanes.eof%>
                                            <tr>
                                            <td>&nbsp;</td>
                                            <td><%=VisualizacionFecha(adorPlanes("FECHA").value,datefmt)%></td>
                                            <td> - </td>
                                            <% if adorPlanes("TIPORECEPCION")=0 then %>
                                            <td><%=adorPlanes("CANTIDAD").value%></td>
                                            <td><%=ador("UNIDAD").value%></td>
                                            <%else %>
                                                <td><%=adorPlanes("IMPORTE").value%></td>
                                                <td><%=adorPlanes("MON").value%></td>
                                            <%end if %>
                                            </td></tr>
                                            <%adorPlanes.movenext
                                        wend%>
                                    </table>
                                <%end if%>
                                </div></td>
                            <%end if%>
                        </tr>
                    <%end if
                end if%>

				<%if not isnull(adorLinea("CODPROCE").value) then%>
				<tr>
				<td class="tablasubrayadaright"><div style="width:100px;height:auto;overflow:hidden"><%=den(50)%></div></td>
				<td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">&nbsp;<%=adorLinea("ANYOPROCE").value%>/<%=adorLinea("GMN1").value%>/<%=adorLinea("CODPROCE").value%>&nbsp;<%=HTMLEncode(adorLinea("DENPROCE").value)%></div></td>
				</tr>
				<%end if%>
	
				<tr>
				<td class="tablasubrayadaright"><div style="width:100px;height:auto;overflow:hidden"><%=den(21)%></div></td>
				<td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">&nbsp;<%=VB2HTML(adorLinea("OBS").value)%></div></td>
				</tr>
				
				<%if not isnull(adorLinea("DENCAMPO1").value) and adorLinea("DENCAMPO1").value<>"" then%>
				<tr>
				<td class="tablasubrayadaright"><div style="width:100px;height:auto;overflow:hidden"><%=adorLinea("DENCAMPO1").value%></div></td>
				<td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">&nbsp;<%=adorLinea("VALOR1").value%></div></td>
				</tr>
				<%end if%>
	
				<%if not isnull(adorLinea("DENCAMPO2").value) and adorLinea("DENCAMPO2").value<>"" then%>
				<tr>
				<td class="tablasubrayadaright"><div style="width:100px;height:auto;overflow:hidden"><%=adorLinea("DENCAMPO2").value%></div></td>
				<td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">&nbsp;<%=adorLinea("VALOR2").value%></div></td>
				</tr>
				<%end if%>
				<%if not  (adorAtrL is nothing) then 
				while  not adorAtrL.EOF				
					esblanco = false
					if adorAtrL("TIPO").value=1 then
						if  isnull(adorAtrL("VALOR_TEXT").value) then
							esblanco=true
						elseif  Len(adorAtrL("VALOR_TEXT").value)= 0 then
								esblanco=true
						end if												
					else		  
						if adorAtrL("TIPO").value=2 then
							if isnull(adorAtrL("VALOR_NUM").value) then
							  esblanco=true
							end if
						else
							if adorAtrL("TIPO").value=3 then
								if isnull(adorAtrL("VALOR_FEC").value) then
								  esblanco=true
								end if
							else
								if adorAtrL("TIPO").value=4 then
									if isnull(adorAtrL("VALOR_BOOL").value) then
									  esblanco=true
									end if
								end if
							end if
						end if
					end if			  
					if (esblanco=false) then %>
						<tr>
						<td class="tablasubrayadaright"><div style="width:100px;height:auto;overflow:hidden"><%=adorAtrL("DEN").value%></div></td>
						<%if adorAtrL("TIPO").value=1 then%> 
						   <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;<%=adorAtrL("VALOR_TEXT").value%></div></td>
						<%end if%>
						<%if adorAtrL("TIPO").value=2 then%> 
						   <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;<%=visualizacionNumero(adorAtrL("VALOR_NUM").value,decimalfmt,thousanfmt,precisionfmt)%></div></td>
						<%end if%>
						<%if adorAtrL("TIPO").value=3 then%>
						   <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;<%=visualizacionFecha(adorAtrL("VALOR_FEC").value,datefmt)%></div></td>
						<%end if%>
						<%if adorAtrL("TIPO").value=4 then%>
						  <%if isnull(adorAtrL("VALOR_BOOL").value) then%>
						    <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;</div></td>
						  <%elseif adorAtrL("VALOR_BOOL").value=0 then%>
						    <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;<%=den(53)%></div></td>
						  <%else%>
						    <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;<%=den(54)%></div></td>
						  <%end if
						end if%>		  
						</tr>					
					<%end if
				adorAtrL.movenext
				wend
				adorAtrL.close
				end if%>
				
				<%if not isnull(adorLinea("ADJUNTOS").value) then
					sAdjuntosLinea=""
					set oLinea = oRaiz.Generar_CLinea()
					oLinea.ID = adorLinea("LINEAID").value
					set oAdjuntos = oLinea.cargarAdjuntos(CiaComp,false)
					if oAdjuntos.count > 0 then
						for each oAdjunto in oAdjuntos 
							sAdjuntosLinea=sAdjuntosLinea & oAdjunto.Nombre & " ; "
						next
						sAdjuntosLinea=mid(sAdjuntosLinea,1,len(sAdjuntosLinea)-3)
				%>
						<tr>
						<td class="tablasubrayadaright">
                            <div style="width:100px;height:auto;overflow:hidden"><%=den(49)%>
                                <%if isempty(oLinea.ObsAdjun) then%>
                                    <a href='javascript:void(null)' onclick='verAdjuntosLinea(<%=oOrden.id%>,<%=oLinea.ID%>,"")'><img border = 0 src= '../images/clip.gif'></A>
                                <%else%>
                                    <a href='javascript:void(null)' onclick='verAdjuntosLinea(<%=oOrden.id%>,<%=oLinea.ID%>,<%=nulltostr(oLinea.ObsAdjun)%>)'><img border = 0 src= '../images/clip.gif'></A>
                                <%end if%>
                            </div>
						</td>
						<td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">&nbsp;<%=sAdjuntosLinea%></div></td>
						</tr>
					<%
						set oAdjuntos=nothing
					end if
					%>
				<%end if%>
				
				</table></td></tr>
			<%end if%>

			<%
			if fila="filaImpar" then
				fila ="filaPar"
			else
				fila ="filaImpar"
			end if
					
					
			adorLinea.movenext
		wend
		adorLinea.close	
		set adorLinea=nothing
		end if
				
				
				
		%>
		</table>
		<br>	
		<br>	
		<%
				
		if oDestinos.count>0 then%>

			<table width="100%">
				<tr>
					<td colspan="7" class="cabecera"><%=den(31)%></td>
				</tr>
				<tr>
					<td class="cabecera"><%=den(24)%></td>
					<td class="cabecera"><%=den(25)%></td>
					<td class="cabecera"><%=den(26)%></td>
					<td class="cabecera"><%=den(27)%></td>
					<td class="cabecera"><%=den(28)%></td>
					<td class="cabecera"><%=den(29)%></td>
					<td class="cabecera"><%=den(30)%></td>
				</tr>

		<%
		fila = "filaImpar"
		for each oDest in oDestinos	%>

				<tr>
					<td class="<%=fila%>"><%=HTMLEncode(oDest.cod)%></td>
					<td class="<%=fila%>"><%=HTMLEncode(oDest.den)%></td>
					<td class="<%=fila%>"><%=HTMLEncode(oDest.dir)%></td>
					<td class="<%=fila%>"><%=HTMLEncode(oDest.cp)%></td>
					<td class="<%=fila%>"><%=HTMLEncode(oDest.pob)%></td>
					<td class="<%=fila%>"><%=HTMLEncode(oDest.provi)%></td>
					<td class="<%=fila%>"><%=HTMLEncode(oDest.pai)%></td>
			</tr>

		<%
		set oOrden=nothing
		next
		%>
		</table>
		<%
		end if

	%>
				</td>
			</tr>
		</table>
	<br>	
	<br>	
	<%
	set oDestinos = nothing
	ador.movenext
	wend
	
	ador.close
	set ador = nothing

	if oRaiz.Sesion.Parametros.ImprimirPoliticaPedidos then
		'Linea de separacion
		%>
			<hr class="P_acep_pedidos"><br>
			
		<%
		Response.Write oRaiz.PoliticaAceptacionPedidos(Idioma)
	end if
end if

set oPersonas = nothing


set oRaiz=nothing
	
%>
<br>
<center><input id="cmdImprimir" style="font-size:15px;font-weight:bold;" type="image" src="<%=APPLICATION("RUTASEGURA")%>/script/images/impresora.gif" onclick="javascript:Imprimir()"></center>
</body>
</html>
