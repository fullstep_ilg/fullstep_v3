<%@ Language=VBScript %>

<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/colores.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/idioma.asp"-->

<%
    ''' <summary>Lleva a cabo el cambio de la cantidad de una l�nea de pedido</summary>
    ''' <remarks>Llamada desde: pedidos41.asp</remarks>            
    bGuardar=CBool(Request("Guardar"))
    IdOrden=Request("IdOrden") 
    CiaComp = Request("CiaComp")    
    Prove = Request("Prove")
    TipoPedido=Request("TipoPedido")
    UsuId=Request.Cookies("USU_USUID")
    CiaID = Request.Cookies("USU_CIAID")
    Idioma = Request.Cookies("USU_IDIOMA")
    if Idioma="" then Idioma = "SPA"    

    dim den
	den = devolverTextos(Idioma,37)	

    set oRaiz=validarUsuario(Idioma,true,false,0)    
    set oOrden = oRaiz.Generar_COrden()
	oOrden.id = clng(IdOrden) 
    oOrden.Prove=Prove 
    oOrden.Tipo=TipoPedido    

    set ador = oOrden.Devolverlineas(CiaComp,Idioma)
    oOrden.CargarLineasDePedido ador     
    oOrden.CargarCostesDescuentos(Clng(CiaComp))
    oOrden.cargarImpuestos Idioma,CiaComp               

    'Actualizar las cantidades de las l�neas de pedido
    arRequest=split(Request.QueryString.Item,"&")  
    bHayCambiosCant=false  
    for i=7 to ubound(arRequest)
        dCantPedida=CDbl(split(arRequest(i),"=")(1))
        set oLinea=oOrden.LineasPedido.Item(cstr(split(arRequest(i),"=")(0)))
        oLinea.CantModificada=(oLinea.CantidadPedida<>dCantPedida)
        oLinea.CantidadPedida=dCantPedida 
        bHayCambiosCant=true 
    next   

    if bGuardar then  
        if bHayCambiosCant then            
            oOrden.Cambio=CDbl(replace(Request("Cambio"),".",","))
            oOrden.Moneda=Request("Mon")
            set oRes=oOrden.CalcularImportesPorCambioCantidadLinea(CiaComp) 
            if oRes.numError=0 then set oRes=oOrden.GuardarImportes(CiaComp,UsuId,CiaID,Idioma)
        end if

        Response.ContentType="text/xml" 
	    Response.Write ("<?xml version='1.0' encoding='UTF-8'?>") 
        if oRes.numError<>0 then                    
            Response.Write ("<Error>")
            Response.write ("<DescError>" & den(60) & "</DescError>")
            Response.Write ("</Error>")
        else
            Response.Write ("<Resultado>") 
            Response.Write ("</Resultado>") 
        end if   
        Response.end      
    else         
        IdLinea=Request("IdLinea")                                          

        set oLinea = oOrden.LineasPedido.Item(cstr(IdLinea))             
        set oRes=oLinea.CambioCantidadLineaPedido(CiaComp,Idioma)    
        Response.ContentType="text/xml" 
	    Response.Write ("<?xml version='1.0' encoding='UTF-8'?>") 
        if oRes.numError<>0 then 
            Response.Write ("<Error>")
            Response.write ("<DescError>" & den(60) & "</DescError>")
            Response.Write ("</Error>")
        else
            'oRes.Arg1:
            'importe de lineas de la orden|importe de costes de la orden|importe de descuentos de la orden|importe con costes/descuentos de la orden|importe de impuestos de la orden|importe final de la orden|
            'importe neto l�nea(con costes y descuentos)|importe costes l�nea|importe descuentos l�nea
            arImportes=Split(oRes.Arg1,"|")          
            Response.Write ("<Resultado>")  
            Response.write ("<ImporteLineasOrden>" & arImportes(0) & "</ImporteLineasOrden>")
            Response.write ("<ImporteCostesOrden>" & arImportes(1) & "</ImporteCostesOrden>")
            Response.write ("<ImporteDescuentosOrden>" & arImportes(2) & "</ImporteDescuentosOrden>")
            Response.write ("<ImporteNetoOrden>" & arImportes(3) & "</ImporteNetoOrden>")
            Response.write ("<ImporteImpuestosOrden>" & arImportes(4) & "</ImporteImpuestosOrden>")
            Response.write ("<ImporteFinalOrden>" & arImportes(5) & "</ImporteFinalOrden>")        
            Response.write ("<ImporteNetoLinea>" & arImportes(6) & "</ImporteNetoLinea>")
            Response.write ("<ImporteCostesLinea>" & arImportes(7) & "</ImporteCostesLinea>")
            Response.write ("<ImporteDescuentosLinea>" & arImportes(8) & "</ImporteDescuentosLinea>")
            Response.Write ("</Resultado>")         
        end if
        Response.end 
                
        set oRes=nothing
    end if

    set oLinea=nothing
    set oRaiz=nothing
    set oOrden=nothing
%>