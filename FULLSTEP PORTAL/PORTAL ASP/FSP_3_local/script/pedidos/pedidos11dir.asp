﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->

<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/menu.asp"></script>
<script SRC="../common/formatos.js"></script>
    
</HEAD>

<body topmargin="0" leftmargin="0" onload="init()">

<%
''' <summary>
''' Muestra los pedidos directos permitiendo aceptarlos o rechazarlos
''' </summary>
''' <remarks>Llamada desde: login\inicio.asp ; Tiempo máximo: 0,2</remarks>

	Idioma = Request.Cookies("USU_Idioma")
	Idioma = trim(Idioma)

	set oRaiz=validarUsuario(Idioma,true,true,0)
    
		
	IdOrden=Request("IdOrden")
	CiaComp = Request("CiaComp")
	
	dim den
	den = devolverTextos(Idioma,32)	
		
	Referencia=oRaiz.DevolverCodPedido(CiaComp,Idioma)

	'obtiene los formatos del usuario
	decimalfmt=request.Cookies ("USU_DECIMALFMT")
	thousandfmt=Request.Cookies ("USU_THOUSANFMT")
	precisionfmt=cint(Request.Cookies ("USU_PRECISIONFMT"))
	datefmt=Request.Cookies("USU_DATEFMT") 
	
	'Obtiene el código de la cia prov en REL_CIAS
	Prove = clng(oRaiz.Sesion.CiaId)
	set oProve = oraiz.DevolverProveedorEnCiaComp(Idioma,Prove,CiaComp)
	CodCiaProv=oProve.Cod
	set oProve=nothing

	dim varEquiv
	set adorEquiv=oRaiz.ObtenerMonedaCentral(CiaComp)
	if not adorEquiv is nothing then
		varEquiv=adorEquiv("EQUIV").value
		sMonPortal = adorEquiv("MON_PORTAL").value
		adorEquiv.close
	else
			set oRaiz = nothing
            
			%>
			<html>
			<body onload="window.open('<%=Application("RUTASEGURA")%>script/errormonedas.asp?Idioma=<%=Idioma%>','default_main')">
			</body>
			</html>
			<%
			Response.End 
	end if
	set adorEquiv=nothing
	
    'Obtiene las compañías con pedidos
    ciaDen=""
	set oCias = oraiz.DevolverCompaniasConPedidos (Idioma,oRaiz.Sesion.CiaId,cint(Application("PORTAL")),clng(oRaiz.Sesion.CiaComp),Application("PYME"),oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"))
    if oRaiz.Sesion.Parametros.unaCompradora  and not oCias is nothing then
        set oCia = oCias.item(1)
		ciaDen = oCia.den
		set ocia = nothing
    end if

	set oOrdenes=oRaiz.Generar_cOrdenes()
	
	Estado=4
	sOrden = "2" & Left("00000000", 8 - Len(IdOrden)) + IdOrden
	set ador=oOrdenes.BuscarTodasOrdenes (clng(CiaComp),Idioma,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"),Estado,sOrden,null,null,null,null,null,null,null,CodCiaProv,null,null,null,null,null,1)	
%>

<script>    dibujaMenu(4)</script>

<script>

function cargarOrden()
{

ordenes = new Array()
<%
bExiste=false
if not ador is nothing then
  if clng(ador("ID"))=clng(IdOrden) then 
	 while not ador.eof
%>
		anyadirOrden(<%=ador("id").value%>, <%=ador("anyo").value%>,<%=ador("PEDIDO")%>,<%=ador("numpedido").value%>, <%=ador("numorden").value%>, '<%=JSText(ador("NUMEXT").value)%>', '<%=JSText(ador("PROVECOD").value)%>', '<%=JSText(ador("PROVEDEN").value)%>', <%=JSDate(ador("FECHA").value)%>, <%=ador("EST").value%>, <%=JSNum(ador("IMPORTE").value*ador("CAMBIO").value/varEquiv)%>, '<%=JSAlertText(ador("OBS").value)%>','<%=JSText(ador("REFERENCIA").value)%>',<%if not isnull(ador("ADJUNTOS").value) then%><%=ador("ADJUNTOS").value%><%else%>0<%end if%>,'<%=JSText(ador("APROVISIONADOR").value)%>','<%=JSText(ador("PER").value)%>','<%=JSText(ador("MON").value)%>','<%=JSText(ador("DENEMPRESA").value)%>','<%=JSText(ador("NOMRECEPTOR").value)%>','<%=JSText(ador("COD_PROVE_ERP").value)%>','<%=JSText(ador("RECEPTOR").value)%>', <%=JSNum(ador("EMPRESA").value)%>)
<%	
		bExiste=true
		ador.movenext
	 wend
	ador.close
	set ador = nothing
  end if
end if

if bExiste=true then
	set oOrden = oRaiz.Generar_COrden()
	oOrden.id = clng(IdOrden)

	set ador = oOrden.Devolverlineas(CiaComp,Idioma)

	if not ador is nothing then
	  while not ador.eof
%>	
		anyadirLinea(<%=IdOrden%>, <%=ador("lineaid").value%>, '<%=JSText(ador("codart").value)%>', '<%=JSText(ador("art_ext").value)%>', '<%=JSText(ador("denart").value)%>','<%=JSText(ador("DESTINO").value)%>','<%=JSText(ador("DESTDEN").value)%>','<%=JSText(ador("UNIDAD").value)%>','<%=JSText(ador("UNIDEN").value)%>', <%=JSNum(ador("PRECIOUNITARIO").value )%>,<%=JSNum(ador("CANTIDAD").value)%>, <%=ador("EST").value%>, <%=ador("ENTREGA_OBL").value%>,<%=JSDate(ador("FECENTREGA").value)%>,'<%=JSText(ador("OBS").value)%>','<%=JSText(ador("CATEGORIA").value)%>', '<%=JSText(ador("CATEGORIARAMA").value)%>','<%=JSText(ador("GMN1"))%>',<%if not isnull (ador("ADJUNTOS").value) then %><%=ador("ADJUNTOS").value%><%else%>0<%end if%>,'<%=JSText(ador("VALOR1").value)%>','<%=JSText(ador("VALOR2").value)%>','<%=JSText(ador("DENCAMPO1").value)%>','<%=JSText(ador("DENCAMPO2").value)%>',<%=JSNum(ador("ANYOPROCE").value)%>,<%=JSNum(ador("CODPROCE").value)%>,'<%=JSText(ador("DENPROCE").value)%>','<%=JSText(ador("OBSADJUN").value)%>')
<%	
	
		ador.movenext
	  wend
	  ador.close
	  set ador = nothing
    end if

end if

set oOrden=nothing
set oOrdenes=nothing
set oRaiz=nothing

%>

mostrarOrdenes()
}

</script>

<SCRIPT>
var requiereRefresco

requiereRefresco=true

var vdecimalfmt 
var vthousanfmt
var	vprecisionfmt
var vdatefmt

vdecimalfmt='<%=decimalfmt%>'
vthousanfmt='<%=thousandfmt%>'
vprecisionfmt='<%=precisionfmt%>'
vdatefmt='<%=datefmt%>'

/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/	
function init()
{
    document.getElementById('tablemenu').style.display = 'block';
	cargarOrden()

	p = window.top.document.getElementById("frSet")

    if (p) {
	    vRows = p.rows
	    vArrRows = vRows.split(",")
	    vRows = vArrRows[0] + ",*,0,0"
	    p.rows = vRows
    }
}


function Aceptar(i)
{
	window.open("<%=Application("RUTASEGURA")%>script/pedidos/pedidos21.asp?Idioma=<%=Idioma%>&Id=" + ordenes[i].id + "&Anyo=" + ordenes[i].anyo + "&pedido=" + ordenes[i].pedido + "&orden=" + ordenes[i].orden + "&Prove=" + ordenes[i].provecod + "&CiaComp=<%=CiaComp%>&IdPedido=" + ordenes[i].idPedido + "&Referencia=<%=Referencia%>&Mon=" + ordenes[i].mon,"default_main") 
	
}

function Rechazar(i)
{
	window.open("<%=Application("RUTASEGURA")%>script/pedidos/pedidos22.asp?Idioma=<%=Idioma%>&Id=" + ordenes[i].id + "&Anyo=" + ordenes[i].anyo + "&pedido=" + ordenes[i].pedido + "&orden=" + ordenes[i].orden + "&Prove=" + ordenes[i].provecod + "&CiaComp=<%=CiaComp%>&IdPedido=" + ordenes[i].idPedido + "&Referencia=<%=Referencia%>&Mon=" + ordenes[i].mon,"default_main") 
}



function Orden(id, anyo, idPedido, pedido, numorden, numext, provecod,proveden, fecemision, est,importe,obs,referencia,adjuntos,aprov,codaprov,mon,empresa,receptor,codErp,codReceptor,idEmpresa)
{
	this.id=id
	this.anyo = anyo
	this.idPedido=idPedido
	this.pedido = pedido
	this.orden = numorden
	this.numext = numext
	this.provecod = provecod
	this.proveden = proveden
	this.importe = importe
	this.fecemision = fecemision
	this.est = est
	this.obs=obs
	this.referencia=referencia
	this.adjuntos=adjuntos
	this.aprov=aprov
	this.codaprov=codaprov
	this.mon=mon
	this.empresa = empresa
	this.receptor = receptor
	this.codErp = codErp
	this.codReceptor = codReceptor
	this.idEmpresa = idEmpresa
	
	this.expandido = false
	this.expandir = expandirOrden
	this.contraer = contraerOrden
	this.lineas = new Array()
}


function Linea (id, artInt, artExt, artDen, codDest, denDest, codUni, denUni, PU, cantPed, est, obl,FecEntrega,obs,categoria, categoriaRama, gmn1, adjuntos,valor1,valor2,dencampo1,dencampo2,anyoproce,codproce,denproce,obsadjun)
{
	this.id = id
	this.artInt = artInt	
	this.artExt = artExt
	this.artDen = artDen
	this.codDest = codDest
	this.denDest = denDest
	this.codUni = codUni
	this.denUni = denUni
	this.PU = PU
	this.cantPed = cantPed
	this.est = est
	this.obl = obl
	this.FecEntrega=FecEntrega
	this.obs = obs
	this.categoria=categoria
	this.categoriaRama=categoriaRama
	this.gmn1=gmn1
	this.adjuntos=adjuntos
	this.valor1=valor1
	this.valor2=valor2
	this.dencampo1=dencampo1
	this.dencampo2=dencampo2
	this.anyoproce=anyoproce
	this.codproce=codproce
	this.denproce=denproce
	this.obsadjun=obsadjun
}



function contraerOrden()
{
	this.expandido=false
	mostrarOrdenes()
}


function expandirOrden()
{
	this.expandido=true
	
	if (this.lineas.length>0)
		{
		mostrarOrdenes()
		}
}

function anyadirOrden(id, anyo, idPedido ,pedido, orden, numext, provecod, proveden, fecemision, est, importe,obs,referencia,adjuntos,aprov,codaprov,mon,empresa,receptor,codErp,codReceptor,idEmpresa)
{
	var i

	i = ordenes.length
	ordenes[i]=new Orden(id,anyo,idPedido,pedido,orden, numext,provecod,proveden,fecemision,est,importe,obs,referencia,adjuntos,aprov,codaprov,mon,empresa,receptor,codErp,codReceptor,idEmpresa)
}

function anyadirLinea (ordenId, id, artInt, artExt, artDen, codDest, denDest, codUni, denUni, PU, cantPed, est, obl,FecEntrega,obs,categoria, categoriaRama, gmn1, adjuntos,valor1,valor2,dencampo1,dencampo2,anyoproce,codproce,denproce,obsadjun)
{
	var l
	var i
	var orden
	
	for (orden=0;orden<=ordenes.length;orden++)
	{
		if (ordenes[orden].id == ordenId)
			{
			break;
			}
	}

	l=ordenes[orden].lineas.length
	ordenes[orden].lineas[l] = new Linea (id, artInt, artExt, artDen, codDest, denDest, codUni, denUni, PU, cantPed, est, obl,FecEntrega, obs,categoria, categoriaRama, gmn1, adjuntos,valor1,valor2,dencampo1,dencampo2,anyoproce,codproce,denproce,obsadjun)

}

function detalleOrden (indice)
{
	var str
	var j
	var scat
	
	str=""
	str += "<TABLE class=principal style='border: solid 1 black;'>"
	str += "	<TR>"
	str += "       <TD width=12% class=cabecera><%=den(4)%></TD>"  //Cod.Art (comp)
	str += "       <TD width=12% class=cabecera><%=den(5)%></TD>"  //Cod.Art (prov)
	str += "       <TD width=25% class=cabecera><%=den(6)%></TD>"  //Denominación
	str += "       <TD width=5% class=cabecera><%=den(24)%></TD>"   //Archivo
	str += "       <TD width=5% class=cabecera><%=den(25)%></TD>"   //categoría
	str += "       <TD width=5% class=cabecera><%=den(7)%></TD>"   //Destino
	str += "       <TD width=10% class=cabecera><%=den(9)%></TD>"  //f.Entrega
	str += "       <TD width=3% class=cabecera><%=den(10)%></TD>"  //obl
	str += "       <TD width=10% class=cabecera><%=den(11)%></TD>"  //cantidad
	str += "       <TD width=5% class=cabecera><%=den(8)%></TD>"   //UP
	str += "       <TD width=10% class=cabecera><%=den(12)%></TD>"  //PU
	str += "       <TD width=5% class=cabecera><%=den(20)%></TD>"   //importe
	
	
	str += "</TR>"
	
	clase="filaPar" 
		
	for (j=0;j<ordenes[indice].lineas.length;j++)
		{
		str += "	<TR>"
		str += "       <TD class=" + mclass + ">" + ordenes[indice].lineas[j].artInt + "</TD>"
		str += "       <TD class=" + mclass + ">" + ordenes[indice].lineas[j].artExt + "</TD>"
		str += "       <TD class=" + mclass + "><div style='width:200px;height:auto;overflow:hidden'>" + ordenes[indice].lineas[j].artDen + "</DIV></TD>"
		
		//Archivos adjuntos
		str += "       <TD class=" + mclass + ">"  //los archivos adjuntos
		if (ordenes[indice].lineas[j].adjuntos!=0 || ordenes[indice].lineas[j].obsadjun!="")
			{
			str+= "<a href='javascript:void(null)' onclick='verAdjuntosLinea(ordenes[" + indice + "].id, ordenes[" + indice + "].lineas[" + j + "].id,ordenes[" + indice + "].lineas[" + j + "].obsadjun)'><img border = 0 src= '../images/clip.gif'></A>"
			}
		else
			{str +="&nbsp;"}
		str +="</TD>"
		
		str += "       <TD class=" + mclass + "><a href='javascript:void(null)' title='" + ordenes[indice].lineas[j].categoriaRama + "'>" + ordenes[indice].lineas[j].categoria + "</a></TD>"
		
		var dest1
		var dest2
		dest1=ordenes[indice].lineas[j].denDest
		dest2=dest1.substr(0,20)
		str += "       <TD class=" + mclass + "><div style='height:auto;overflow:hidden'><table width=100%><tr><td width=100%><a href='javascript:void(null)' onclick='DenominacionDest(" + indice + "," + j + ")' title='" + ordenes[indice].lineas[j].denDest + "'>" + dest2  + "</a></td>"
		str += "       <td><a href='javascript:void(null)' onclick='DenominacionDest(" + indice + "," + j + ")' title='" + ordenes[indice].lineas[j].denDest + "'><IMG border=0 SRC='../images/masinform.gif'></a></td></tr></table></div></TD>"
		
		str += "       <TD align=right class=" + mclass + ">" + date2str(ordenes[indice].lineas[j].FecEntrega,vdatefmt) + "</TD>"
		
		if (ordenes[indice].lineas[j].obl==1)
			str += "<TD align=center class=" + mclass + "><%=den(18)%></TD>"
		else
			str += "<TD align=center class=" + mclass + "><%=den(19)%></TD>"
			
		str += "       <TD align=right class=" + mclass + ">" + num2str(ordenes[indice].lineas[j].cantPed ,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</TD>"
		str += "       <TD class=" + mclass + "><table width=100%><tr><td width=100%><a href='javascript:void(null)' onclick='DenominacionUP(" + indice + "," + j + ")' title='" + ordenes[indice].lineas[j].denUni + "'><NOBR>" + ordenes[indice].lineas[j].codUni + "</NOBR></a></td>"
		str += "       <td><a href='javascript:void(null)' onclick='DenominacionUP(" + indice + "," + j + ")' title='" + ordenes[indice].lineas[j].denUni + "'><IMG border=0 SRC='../images/masinform.gif'></a></td></tr></table>"
		str += "</TD>"
		
		str += "       <TD align=right class=" + mclass + ">" + num2str(ordenes[indice].lineas[j].PU ,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</TD>"
		
		str += "       <TD align=right class=" + mclass + ">" + num2str(ordenes[indice].lineas[j].cantPed * ordenes[indice].lineas[j].PU  ,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</TD>"
		
		str += "	</TR>"
		
		if ((ordenes[indice].lineas[j].codproce != null) ||  ((ordenes[indice].lineas[j].obs!= null) && (ordenes[indice].lineas[j].obs!= ""))  || ((ordenes[indice].lineas[j].dencampo1 != null) && (ordenes[indice].lineas[j].dencampo1 != "")) || ((ordenes[indice].lineas[j].dencampo2 != null) && (ordenes[indice].lineas[j].dencampo2 != "")))
	 {
	  // Ahora muestra el proceso,las observaciones y los campos personalizado
	  str += "	<TR><TD>&nbsp;</TD><TD colspan=10><TABLE width='100%' CELLSPACING=0>"

	  if (ordenes[indice].lineas[j].codproce != null)
	  {
		str += "	<TR>"
		str += "	<TD class='tablasubrayadaright'><div style='width:100px;height:auto;overflow:hidden'><%=den(26)%></div></TD>"
		str += "	<TD class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'>&nbsp;" + ordenes[indice].lineas[j].anyoproce + "/" + ordenes[indice].lineas[j].gmn1 +  "/" + ordenes[indice].lineas[j].codproce + "  " + ordenes[indice].lineas[j].denproce + "</div></TD>"
		str += "	</TR>"
	  }
	
	  str += "	<TR>"
	  str += "	<TD class='tablasubrayadaright'><div style='width:100px;height:auto;overflow:hidden'><%=den(27)%></div></TD>"
	  str += "	<TD class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'><a style='color:black' href='javascript:void(null)' onclick='alert(ordenes[" + indice + "].lineas[" +j + "].obs)'>&nbsp;" + ordenes[indice].lineas[j].obs + "</a></div></TD>"
	  str += "	</TR>"
	
	  if ((ordenes[indice].lineas[j].dencampo1 != null) && (ordenes[indice].lineas[j].dencampo1 != ""))
	  {
		  str += "	<TR>"
		  str += "	<TD class='tablasubrayadaright'><div style='width:100px;height:auto;overflow:hidden'>" + ordenes[indice].lineas[j].dencampo1 + "</div></TD>"
		  str += "	<TD class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'>&nbsp;" + ordenes[indice].lineas[j].valor1 + "</div></TD>"
		  str += "	</TR>"
	  }
	
	  if ((ordenes[indice].lineas[j].dencampo2 != null) && (ordenes[indice].lineas[j].dencampo2 != ""))
	  {
	  	  str += "	<TR>"
		  str += "	<TD class='tablasubrayadaright'><div style='width:100px;height:auto;overflow:hidden'>" + ordenes[indice].lineas[j].dencampo2 + "</div></TD>"
		  str += "	<TD class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'>&nbsp;" + ordenes[indice].lineas[j].valor2 + "</div></TD>"
		  str += "	</TR>"
	  }
	  str += "	</TABLE></TD></TR>"
	 }
	}
	
	str += "	</TABLE>"
	return (str)

}


function mostrarOrdenes()
{
	var str
	var i
	var j
	var idActual	

	if (ordenes.length==0)
	  {
		str='<h2><%=den(17)%></h2>'
	  }
	  
	else
	  {
		str = '<table class="principal" border="0" width="95%" cellspacing="2" cellpadding="1">'
		str += '<tr><TD width=5% class=cabecera></TD>'
		str += '<TD width=5% class=cabecera ><%=Den (31)%></TD>'
		str += '<TD width=10% class=cabecera ><%=Den (1)%></TD>'
		str += '<TD width=10% class=cabecera ><%=Den(28)%></TD>'  //Nº pedido (Proveedor)
		<%if Referencia<>"" then %>
			str += '<TD width=20% class=cabecera><%=Referencia%></TD>'  //referencia
		<%end if%>
		str += '<TD width=15% class=cabecera ><%=Den (2)%></TD>'
		str += '<TD width=20% class=cabecera ><%=den(23)%></TD>'  //peticionario
		str += '<TD width=15% class=cabecera ><%=Den (20)%></TD>'
		str += '<TD width=4% class=cabecera ><%=den(21)%></TD>'  //obs
		str += '<TD width=4% class=cabecera ><%=den(24)%></TD>'  //arch.
		str += '<TD width=4% class=cabecera >&nbsp;</TD>'
		str += '<TD width=4% class=cabecera >&nbsp;</TD>'
		str += '<TD width=1% class=cabecera >&nbsp;</TD>'
		str += '<TD width=1% class=cabecera >&nbsp;</TD></tr>'

		mclass="filaPar"
		
		for (i=0;i<ordenes.length;i++)
		  {
			str += '<TR>'
			
			if (ordenes[i].expandido)
			  {
				str += '<TD class=' + mclass + ' align=center><a href="javascript:void(null)" onclick="ordenes[' + i.toString() + '].contraer()"><font face=courier>-</font></a></TD>'
			  }
			else
			  {
				str += "<TD class=" + mclass + " align=center><a href='javascript:void(null)' onclick='ordenes[" + i.toString() + "].expandir()'>+</a></TD>"
			  }
			
			str += "<TD class=" + mclass + "><table width=100%><tr><td width=100%><a href='javascript:void(null)' onclick='DetalleEmpresa(" + i + ")'>" + ordenes[i].empresa + "</a></td>"
			str += "       <td><a href='javascript:void(null)' onclick='DetalleEmpresa(" + i + ")'><IMG border=0 SRC='../images/masinform.gif'></a></td></tr></table></TD>"
			str += "<TD class=" + mclass + ">" + ordenes[i].anyo + "/" + ordenes[i].pedido + "/" + ordenes[i].orden + "</TD>"
			str += "<TD class=" + mclass + ">" + ordenes[i].numext + "</TD>"
			
			<%if Referencia<>"" then %>	
				str += "<TD class=" + mclass + "><div style='width:100px;height:auto;overflow:hidden'>" + ordenes[i].referencia + "</DIV></TD>"
			<%end if%>
		
			str += "<TD class=" + mclass + ">" + date2str(ordenes[i].fecemision,vdatefmt) + "</TD>"
			
			if (ordenes[i].codaprov != "")
			 {
				str += "<TD class=" + mclass + "><table width=100%><tr><td width=100%><a href='javascript:void(null)' onclick='DetallePet(" + i + ")'>" + ordenes[i].aprov + "</a></td>"
				str += "       <td><a href='javascript:void(null)' onclick='DetallePet(" + i + ")'><IMG border=0 SRC='../images/masinform.gif'></a></td></tr></table></TD>"  //Peticionario
			 }
			else
			 {
				str += "<TD class=" + mclass + "><table width=100%><tr><td width=100%> </td>"
				str += "       <td></td></tr></table></TD>"  //Peticionario
			 }
			str += "<TD align=right class=" + mclass + ">" + num2str(ordenes[i].importe ,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + ordenes[i].mon + "</TD>"
			
			str += "<TD class=" + mclass + ">"   //las observaciones
			if (ordenes[i].obs!="")
				{
				str +="<a href='javascript:void(null)' onclick='alert(ordenes[" + i + "].obs)'><img border = 0 src= '../images/coment.gif'></a>" }
			else
				{str +="&nbsp;"}
			str +="</TD>"
		
			str += "       <TD class=" + mclass + ">"  //los archivos adjuntos
			if (ordenes[i].adjuntos!=0)
				{
				str+= "<a href='javascript:void(null)' onclick='verAdjuntosOrden(ordenes[" + i + "].id)'><img border = 0 src= '../images/clip.gif'></A>"
				}
			else
				{str +="&nbsp;"}
			str +="</TD>"
		
			str += '<TD class=' + mclass + ' width=5%>'
			str += '<a href="javascript:Aceptar(' + i + ')"><%=den(13)%></a>' 
			str += '</TD>'
			str += '<TD class=' + mclass + ' width=5%>'
			str += '<a href="javascript:Rechazar(' + i + ')"><%=den(14)%></a>' 
			str += '</TD>'
			
			str += "<TD align=center class=" + mclass + "><a href='javascript:void(null)' onclick ='exportarPedido(" + ordenes[i].anyo + "," + ordenes[i].pedido + "," + ordenes[i].orden + ")'><IMG border=0 SRC='../images/excelp.gif'></a> </TD>"
			str += "<TD align=center class=" + mclass + "><a href='javascript:void(null)' onclick ='imprimirPedido(" + ordenes[i].anyo + "," + ordenes[i].pedido + "," + ordenes[i].orden + ")'><IMG border=0 SRC='../images/impresorap.gif'></a> </TD>"
			str += '</TR>'
		
			if (ordenes[i].expandido)
				{
				str += '<TR><TD colspan=10>'
				str += detalleOrden(i)
				str += '</TD></TR>'
				}
				
			if (mclass=="filaPar")
				{
				mclass="filaImpar"
				}
			else
				{
				mclass="filaPar"
				}
			
		   }
		str += '</table>'
		}
	
	document.getElementById("divPedidos").style.visibility="visible"
	document.getElementById("divPedidos").innerHTML=str 

}

function aplicarFormatos(vdec,vthou,vprec,vdate)
{
	vdecimalfmt=vdec
	vthousanfmt=vthou
	vprecisionfmt = vprec
	vdatefmt = vdate
	
	if (document.getElementById("divPedidos").style.visibility=="visible")
	{
		mostrarOrdenes()
	}		
}

var ordenes
ordenes = new Array()

function DenominacionUP(i,j)
{
	winUP=window.open("<%=application("RUTASEGURA")%>script/common/DenominacionUP.asp?Idioma=<%=Idioma%>&Cod=" + Var2Param(ordenes[i].lineas[j].codUni) ,"_blank","width=400,height=130,location=no,menubar=no,toolbar=no,resizable=no,addressbar=no scrollbars=yes")
	winUP.focus()
}

function DenominacionDest(i,j)
{

	winDest=window.open("<%=application("RUTASEGURA")%>script/common/DenominacionDest.asp?Idioma=<%=Idioma%>&Dest=" + ordenes[i].lineas[j].codDest + "&LineaPedido=" + ordenes[i].lineas[j].Id,"_blank","width=400,height=200,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no scrollbars=no")
	winDest.focus()
}

function DetalleEmpresa(i)
{
	winEmp = window.open("<%=application("RUTASEGURA")%>script/common/detalleempresa.asp?Emp=" + ordenes[i].idEmpresa ,"_blank","width=400,height=180,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no scrollbars=no")
	winEmp.focus()
}

function DetallePet(i)
{
	winDest=window.open("<%=application("RUTASEGURA")%>script/common/detallepet.asp?Idioma=<%=Idioma%>&Pet=" + ordenes[i].codaprov ,"_blank","width=400,height=130,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no scrollbars=no")
	winDest.focus()
}

function Observaciones (i,j)
{
	alert(ordenes[i].lineas[j].obs)
}


var winEspera
/*
''' <summary>
''' Imprimir los pedidos
''' </summary>     
''' <remarks>Llamada desde: A HREF imprimir ; Tiempo máximo: 0</remarks>*/
function imprimirPedidos()
{

winEspera = window.open ("<%=Application("RUTASEGURA")%>script/common/winEspera.asp?msg=<%=server.urlEncode(den(30))%>&msg2=<%=server.urlEncode(den(29))%>&continua=continuarCargandoPedidos", "_blank", "top=100,left=150,width=400,height=100,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
while (winEspera.document.images["imgReloj"])
{
}

}
/*
''' <summary>
''' Imprime los pedidos
''' </summary>
''' <remarks>Llamada desde: seguimclt.asp ; Tiempo máximo: 0,2</remarks>*/
function continuarCargandoPedidos()
{
	window.open("<%=Application("RUTASEGURA")%>script/pedidos/rptpedidos.asp?Idioma=<%=Idioma%>&Referencia=<%=Referencia%>&CiaDen=<%=CiaDen%>","blank","top=50,left=50,width=700,height=500,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no scrollbars=yes")
	winEspera.focus()
}
/*
''' <summary>
''' Imprime el pedido
''' </summary>
''' <param name="anyo">Anyo de pedido</param>
''' <param name="pedido">Id de pedido</param>   
''' <param name="orden">Orden de pedido</param>        
''' <remarks>Llamada desde: pedidos11dir.asp ; Tiempo máximo: 0,2</remarks>*/
function imprimirPedido(anyo,pedido,orden)
{
	window.open("<%=Application("RUTASEGURA")%>script/pedidos/rptpedido.asp?Idioma=<%=Idioma%>&anyo=" + anyo + "&pedido=" + pedido + "&orden=" + orden + "&Referencia=<%=Referencia%>&CiaDen=<%=CiaDen%>" ,"blank","top=50,left=50,width=700,height=500,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no scrollbars=yes")
}
/*
''' <summary>
''' Exportar los pedidos
''' </summary>     
''' <remarks>Llamada desde: A HREF Exportar ; Tiempo máximo: 0</remarks>*/
function exportarPedido(anyo,pedido,orden)
{
var re
var re2
re=/MSIE 5.5/
re2=/Q299618/
	
re3 = /SP1/

if (navigator.userAgent.search(re)!=-1 && navigator.appMinorVersion.search(re2)==-1 && navigator.appMinorVersion.search(re3)!=-1)
	{
	window.open("<%=Application("RUTASEGURA")%>script/common/mspatch.asp","_blank","top=100,left=150,width=300,height=250,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
	window.close()
	}
else
	{
	top.winEspera = window.open ("<%=Application("RUTASEGURA")%>script/common/winEspera.asp", "_blank", "top=100,left=150,width=400,height=100,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
    while (typeof(top.winEspera.document.images["imgReloj"])!= 'undefined') 
		{
		}
	window.open("<%=Application("RUTASEGURA")%>script/pedidos/xlspedidos.asp?anyo=" + anyo + "&pedido=" + pedido + "&orden=" + orden ,"fraPedidosServer")
	}
}


function verAdjuntosOrden(id)
{
   window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/adjuntosorden.asp?Orden=" + id, "_blank", "top=5,left=5,width=600, height=250,directories=no,menubar=no,scrollbars=yes,resizable=yes,toolbar=yes")
}

function verAdjuntosLinea(orden,id,obs)
{
   window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/adjuntoslinea.asp?Orden=" + orden + "&Linea=" + id + "&Obs=" + obs, "_blank", "top=5,left=5,width=600, height=250,directories=no,menubar=no,scrollbars=yes,resizable=yes,toolbar=yes")
}

</script>



<table width="100%">
	<tr>
		<td>
			<h1><%=den(15)%>&nbsp;<span class="negro"><%=HTMLEncode(ciaDen)%></span></h1>
		</td>
		<td align="right">
			<a href="javascript:void(null)" onclick="imprimirPedidos()"><img border="0" SRC="../images/impresora.GIF" WIDTH="41" HEIGHT="38"></a>
		</td>
	</tr>
</table>


<h3><%=Den (3)%></h3>

<div name="divPedidos" id="divPedidos">

</div>

</BODY>

</HTML>
