﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/colores.asp"-->
<!--#include file="../common/formatos.asp"-->
<%
''' <summary>
''' Mostrar el comentario de una no conformidad
''' </summary>
''' <remarks>Llamada desde: noconformidades\noconformidades11.asp; Tiempo máximo: 0,1</remarks>
	Idioma = Request.Cookies("USU_IDIOMA")
	texto=Request("texto")
	titulo=Request("titulo")
	dim den
	den = devolvertextos(Idioma,125)
%>

<HTML>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">

<HEAD>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">

    <title><%=title%></title>

    <SCRIPT>

    function Cerrar()
    {
	    window.close()
    }

    </SCRIPT>

</HEAD>
<BODY topmargin="10" leftmargin="0">
<TABLE class="normal" align="center" WIDTH="98%" border="0" cellspacing="1" cellpadding="1">
	<TR><TD width="5%" class="cabecera"><%=den(10)%></td>
		<TD class="filaPar" ><TEXTAREA NAME="comments" COLS=60 ROWS=6><%=HTMLEncode(texto)%></TEXTAREA></td>
	</TR>
	<TR>
		<TD height=40px align=center COLSPAN=2>
			<INPUT TYPE="button" class="button" ID=Cerrar LANGUAGE=javascript class="Aprov" value="<%=Den(11)%>" onclick="Cerrar()">
		</TD>
	</TR>
	
</TABLE>	
</BODY>
</HTML>

