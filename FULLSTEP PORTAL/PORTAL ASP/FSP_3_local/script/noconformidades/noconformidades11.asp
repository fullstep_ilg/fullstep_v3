﻿<!DOCTYPE HTML PUBLIC >
<%@ Language=VBScript %>
<% Response.Charset = "UTF-8"%>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/fsal_1.asp"-->

<%
''' <summary>
''' Mostrar la lista de no conformidades abiertas ó pendientes de revisar cierre
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: noconformidades1.asp; Tiempo máximo:0,3</remarks>

Idioma=Request.Cookies ("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,true,true,0)

CiaComp = oRaiz.Sesion.CiaComp

Prove = clng(oRaiz.Sesion.CiaId)
	
set oProve = oraiz.DevolverProveedorEnCiaComp(Idioma,Prove,CiaComp)
	
if not oProve is nothing then
	with oProve
		if isnull(.CodMon) then
			set oRS = oRaiz.ObtenerMonedaCentral(CiaComp)
			if not oRS is nothing then
				sCodMon = oRS.fields("MON_GS").value
				oRS.close
				set oRs = nothing
			else
				sCodMon = "EUR"
			end if
		else
			sCodMon = .CodMon
		end if
		set oMonedas = oRaiz.Generar_CMonedas()
		oMonedas.CargarTodasLasMonedasDesde Idioma, CiaComp , 1, sCodMon, , , true, , true
		set oMonedas = nothing
	end with
end if
	
set oProve = nothing 

%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script src="../common/formatos.js"></script>
<script SRC="../common/menu.asp"></script>
<script type="text/javascript" src="<%=application("RUTASEGURA")%>script/js/jquery.min.js"></script>
<script language="JavaScript" src="../common/ajax.js"></script>
<script>
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
    </script>

<!--#include file="../common/fsal_3.asp"-->

<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
        if (<%=Application("ACCESO_SERVIDOR_EXTERNO")%>==0){
            document.getElementById('tablemenu').style.display = 'block';
        }
    }
</script>

</head>
<body bgColor="#ffffff" topmargin="0" leftmargin="0" onload="init()">
<iframe id="iframeWSServer" style="Z-INDEX: 106; LEFT: 8px; VISIBILITY: hidden; POSITION: absolute; TOP: 200px" name="iframeWSServer" src="<%=Application("RUTASEGURA")%>script/blank.htm"></iframe>
<script>dibujaMenu(2)</script>



<%
datefmt=Request.Cookies("USU_DATEFMT")

ProveCod = oRaiz.Sesion.CiaCodGs

dim den

den = devolverTextos(Idioma,125)	

%>

<script>
function detallePersona(cod)
{
window.open("<%=Application("RUTASEGURA")%>script/common/detallepersona.asp?Per=" + Var2Param(cod) + "&v3=1","_blank", "top=150, left=60, width=350, height=160, resizable=yes,status=no,toolbar=no,menubar=no,location=no")
}

function irANoConformidad(noconformidad,inst,tipo, instSinEncryt)
{	
    params = { contextKey: instSinEncryt };
    var enProceso;
    $.when($.ajax({
        type: "POST",
        url: '<%=Application("RUTASEGURA")%>script/PMPortal/Consultas.asmx/ComprobarEnProceso',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        enProceso = msg.d;
        if (enProceso != 0) { //En PROCESO            
            window.open("<%=Application("RUTASEGURA")%>script/common/QAenProceso.asp?ID="+instSinEncryt+"&Tipo="+tipo,((<%=Application("ACCESO_SERVIDOR_EXTERNO")%>==1)?"_self":"_blank"), "width=600,height=200,status=yes,resizable=no,top=200,left=200");            
        } else {                                               
            window.open("<%=Application("RUTASEGURA")%>script/common/pasarelaPMPortalWeb.asp?Idioma=<%=Idioma%>&Pagina=6&noconformidad=" + noconformidad + "&instancia=" + inst,((<%=Application("ACCESO_SERVIDOR_EXTERNO")%>==1)?"_self":"default_main"));                 
        }
    });
}

function mostrarVentana(enProceso,noconformidad,inst,tipo){
	if (enProceso != 0 ) //En PROCESO 
		window.open("<%=Application("RUTASEGURA")%>script/common/QAenProceso.asp?ID="+inst+"&Tipo="+tipo,"_blank", "width=600,height=200,status=yes,resizable=no,top=200,left=200")	
    else
		window.open("<%=Application("RUTASEGURA")%>script/PMPortal/script/noconformidad/extFrames.aspx?noconformidad=" + noconformidad + "&instancia=" + inst, "default_main")
}

function irANoConformidadCerradas()
{    		        
    window.open("<%=Application("RUTASEGURA")%>script/common/pasarelaPMPortalWeb.asp?Idioma=<%=Idioma%>&Pagina=7",((<%=Application("ACCESO_SERVIDOR_EXTERNO")%>==1)?"_self":"default_main"));        
}
/*''' <summary>
''' Mostrar el comentario de una no conformidad
''' </summary>     
''' <remarks>Llamada desde: A HREF Comentario; Tiempo mÃ¡ximo:0</remarks>*/
function abrirPopUpComentario(titulo,comentario)
{
	window.open("<%=Application("RUTASEGURA")%>script/noconformidades/popupComentario.asp?titulo="+titulo+"&texto="+comentario,"_blank", "top=150, left=60, width=400, height=160, resizable=yes,status=no,toolbar=no,menubar=no,location=no")	
}
</script>

<table width="100%">
<td width="50%" align="left">
<h1>
<%=den(1)%>
</h1>
</td>
<%
dim oNoConformidades

set oNoConformidades = oRaiz.Generar_CNoConformidades()

set ador = oNoConformidades.DevolverNoConformidades(CiaComp,ProveCod,Idioma,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"))
%>

<td width="50%" align="right">
<a href="javascript:void(null)" onclick="irANoConformidadCerradas()"><u><%=den(9)%></u></a>
</td>
</table>
<%
if not ador is nothing then %>




<p><%=den(2)%></p>

<table class="principal"  id="tblEmitidas">

<tr>
<td align="center" colspan="7" class="cabecera">
	<%=den(8)%>
</td>
</tr>
<tr>
<td width="5%" class="cabecera">
	<%=den(3)%>
</td>
<td width="5%" class="cabecera">
	<%=den(4)%>
</td>
<td width="30%" class="cabecera">
	<%=den(5)%>
</td>
<td width="35%" class="cabecera">
	<%=den(7)%>
</td>
<td width="20%" class="cabecera">
	<%=den(6)%>
</td>
<td width="5%" class="cabecera">
	<%=den(10)%>
</td>
<td width="2%" class="cabecera"></td>
</tr>

<%
NRevisar =0
dim InstanciaPrevia
InstanciaPrevia=0
while not ador.eof
	if ador("ESTADO").value=1 And ador("INSTANCIA").value<>InstanciaPrevia then
		InstanciaPrevia=ador("INSTANCIA").value
%>
<tr>
<td>
	<a href="javascript:void(null)" onclick="irANoConformidad('<%=server.URLEncode(ador("ID_ENCRIPTED").value)%>','<%=server.URLEncode(ador("INSTANCIA_ENCRIPTED").value)%>','<%=ador("DEN").value%>',<%=ador("INSTANCIA").value%>)"><nobr><%=VisualizacionFecha(ador("FECALTA"),datefmt)%></nobr></a>
</td>
<td>
	<a href="javascript:void(null)" onclick="irANoConformidad('<%=server.URLEncode(ador("ID_ENCRIPTED").value)%>','<%=server.URLEncode(ador("INSTANCIA_ENCRIPTED").value)%>','<%=ador("DEN").value%>',<%=ador("INSTANCIA").value%>)"><%=ador("INSTANCIA").value%></a>
</td>
<td>
	<a href="javascript:void(null)" onclick="irANoConformidad('<%=server.URLEncode(ador("ID_ENCRIPTED").value)%>','<%=server.URLEncode(ador("INSTANCIA_ENCRIPTED").value)%>','<%=ador("DEN").value%>',<%=ador("INSTANCIA").value%>)"><%=ador("DEN").value%></a>
</td>
<td>
	<a href="javascript:void(null)" onclick="irANoConformidad('<%=server.URLEncode(ador("ID_ENCRIPTED").value)%>','<%=server.URLEncode(ador("INSTANCIA_ENCRIPTED").value)%>','<%=ador("DEN").value%>',<%=ador("INSTANCIA").value%>)"><%=ador("TITULO").value%></a>
</td>
<td>
								<a href="javascript:void(null)" onclick="detallePersona(&quot;<%=ador("PER").value%>&quot;)">
									<%=ador("NOMBRE").value%>
								</a>
</td>

<td align="center">
<%If Not isNull(ador("COMENT_ALTA")) Then
	If Not isNull(ador("TITULO")) Then %>
		<a href="javascript:void(null)" onclick="abrirPopUpComentario('<%=ador("INSTANCIA").value%> - <%=Server.URLEncode(ador("TITULO"))%>','<%=Server.URLEncode(ador("COMENT_ALTA"))%>')"><img src="../images/coment.gif" border="0" WIDTH="18" HEIGHT="16"></a>
	<%else%>
		<a href="javascript:void(null)" onclick="abrirPopUpComentario('<%=ador("INSTANCIA").value%>','<%=Server.URLEncode(ador("COMENT_ALTA"))%>')"><img src="../images/coment.gif" border="0" WIDTH="18" HEIGHT="16"></a>
	<%End If
End If%></td>
<td align="center"><%If (ador("EN_PROCESO")=1) Then%><img src="../images/icono_error_amarillo.gif" alt="<%=den(12)%>" WIDTH="16" HEIGHT="16"><%End If%></td>
</tr>

<%
	else
		NRevisar =NRevisar+1
	end if
	ador.movenext
wend

end if
%>
</table>


<%
if not ador is nothing and NRevisar>0 then 
	ador.movefirst%>


<p></p>
<table class="principal" id="tblRevisar">
<tr>
<td align="center" colspan="7" class="cabecera">
	<%=den(14)%>
</td>
</tr>
<tr>
<td width="5%" class="cabecera">
	<%=den(3)%>
</td>
<td width="5%" class="cabecera">
	<%=den(4)%>
</td>
<td width="30%" class="cabecera">
	<%=den(5)%>
</td>
<td width="35%" class="cabecera">
	<%=den(7)%>
</td>
<td width="20%" class="cabecera">
	<%=den(6)%>
</td>
<td width="5%" class="cabecera">
	<%=den(10)%>
</td>
<td width="2%" class="cabecera"></td>
</tr>

<%
dim InstanciaPendienteRevisarPrevia
InstanciaPendienteRevisarPrevia=0
while not ador.eof
	if ador("ESTADO").value>1 and ador("INSTANCIA").value<>InstanciaPendienteRevisarPrevia then
		InstanciaPendienteRevisarPrevia=ador("INSTANCIA").value
%>
<tr>
<td>
	<a href="javascript:void(null)" onclick="irANoConformidad('<%=server.URLEncode(ador("ID_ENCRIPTED").value)%>','<%=server.URLEncode(ador("INSTANCIA_ENCRIPTED").value)%>','<%=ador("DEN").value%>',<%=ador("INSTANCIA").value%>)"><nobr><%=VisualizacionFecha(ador("FECALTA"),datefmt)%></nobr></a>
</td>
<td>
	<a href="javascript:void(null)" onclick="irANoConformidad('<%=server.URLEncode(ador("ID_ENCRIPTED").value)%>','<%=server.URLEncode(ador("INSTANCIA_ENCRIPTED").value)%>','<%=ador("DEN").value%>',<%=ador("INSTANCIA").value%>)"><%=ador("INSTANCIA").value%></a>
</td>
<td>
	<a href="javascript:void(null)" onclick="irANoConformidad('<%=server.URLEncode(ador("ID_ENCRIPTED").value)%>','<%=server.URLEncode(ador("INSTANCIA_ENCRIPTED").value)%>','<%=ador("DEN").value%>',<%=ador("INSTANCIA").value%>)"><%=ador("DEN").value%></a>
</td>
<td>
	<a href="javascript:void(null)" onclick="irANoConformidad('<%=server.URLEncode(ador("ID_ENCRIPTED").value)%>','<%=server.URLEncode(ador("INSTANCIA_ENCRIPTED").value)%>','<%=ador("DEN").value%>',<%=ador("INSTANCIA").value%>)"><%=ador("TITULO").value%></a>
</td>
<td>
<a href="javascript:void(null)" onclick="detallePersona(&quot;<%=ador("PER").value%>&quot;)">
	<%=ador("NOMBRE").value%>
</a>
</td>

<td align="center">
<%If Not isNull(ador("COMENT_ALTA")) Then
	If Not isNull(ador("TITULO")) Then%>
		<a href="javascript:void(null)" onclick="abrirPopUpComentario('<%=ador("INSTANCIA").value%> - <%=Server.URLEncode(ador("TITULO"))%>','<%=Server.URLEncode(ador("COMENT_ALTA"))%>')"><img src="../images/coment.gif" border="0" WIDTH="18" HEIGHT="16"></a>
	<%else%>
		<a href="javascript:void(null)" onclick="abrirPopUpComentario('<%=ador("INSTANCIA").value%>','<%=Server.URLEncode(ador("COMENT_ALTA"))%>')"><img src="../images/coment.gif" border="0" WIDTH="18" HEIGHT="16"></a>
	<%End if
End If%></td>
<td align="center"><%If (ador("EN_PROCESO")=1) Then%><img src="../images/icono_error_amarillo.gif" alt="<%=den(12)%>" WIDTH="16" HEIGHT="16"><%End If%></td>
</tr>

<%
	end if
	ador.movenext
wend
ador.close
set ador=nothing
end if
%>
</table>
</body>
<%
set oNoConformidades = nothing
set ousuario = nothing 
set oraiz = nothing 
%>
<!--#include file="../common/fsal_2.asp"-->
</html>
