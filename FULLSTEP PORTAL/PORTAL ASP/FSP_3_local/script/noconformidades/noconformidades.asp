﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<%
''' <summary>
''' Dependiendo de si el proveedor conectado es de una compania, digamos, unica (paramGen.unaCompradora) carga una pantalla
''	detallada de no conformidades. En caso de ser una compania, digamos, no unica (1 empresa fisica N empresas logicas)
''	muestra la lista de empresas con hipervinculos a pantalla detallada de las no conformidades de cada una.
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: menu.asp ; Tiempo máximo:0</remarks>

	Idioma = Request("Idioma")
	Idioma = trim(Idioma)
    
    AccesoExterno=cbool(Application("ACCESO_SERVIDOR_EXTERNO"))    
    if AccesoExterno then
        set oRaiz=validarUsuarioAccesoExterno(Idioma,true,true,0,request("txtCIA"),request("txtUSU"),request("CodigoSesion"))
    else
	    set oRaiz=validarUsuario(Idioma,true,true,0)
    end if
    	
	'Idiomas
	dim den
	den = devolverTextos(Idioma,126)		

	
	'Obtiene las compañías con pedidos
	DelProveedor = clng(oRaiz.Sesion.CiaId)
	set oCias = oraiz.DevolverCompaniasConNoConformidades (DelProveedor,cint(Application("PORTAL")),clng(oRaiz.Sesion.CiaComp),Application("PYME"))

	set paramgen = oRaiz.Sesion.Parametros
	
	if paramGen.unaCompradora  then
		set paramgen = nothing
        
		set oRaiz = nothing
		
		response.redirect Application ("RUTASEGURA") & "script/noconformidades/noconformidades1.asp" 
		Response.End 
	end if
set paramgen = nothing



%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta NAME="GENERATOR" Content="Microsoft FrontPage 4.0">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/menu.asp"></script>
<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init()
{
    document.getElementById('tablemenu').style.display = 'block';
}
</script>
</head>
<body topMargin=0 leftmargin=0 onload="init()">
<SCRIPT>dibujaMenu(2)</script>

<h1><% = Den (1)%></h1>

<%
bHayReclamaciones = false
if not oCias is nothing then              
	for each oCia in oCias
					
		if oCia.NumNoConfNuevas > 0 then
			bHayReclamaciones = true
		end if
	next
end if

if not bHayReclamaciones then
%>

<p id=pError class=Error><%=den(7)%></P>
<p>&nbsp;</p>


<%else%>
	<h3><% = Den(1) %></h3>

	<div align="center">
	<table border="0" width="70%" cellspacing="2" cellpadding="1">
	  <tr>
	    <th width="40%" class="cabecera"><% = Den (4) %></th>
	    <th width="20%" class="cabecera"><% = Den (6) %></th>
	  </tr>
		  
	<%
		mclass="filaImpar"
		if not oCias is nothing then              
			for each oCia in oCias
					
				if oCia.NumNoConfNuevas > 0 then%>
					
				<tr>  
					<td class=<%=mclass%>>
						<A ID=option0 LANGUAGE=javascript onmousemove="window.status='<%=oCia.Den%>'" onmouseout="window.status=''" href="<%=application("RUTASEGURA")%>script/noconformidades/noconformidades1.asp">
						<%=oCia.den%>
						</A>
					</td>
					<td class=<%=mclass%> align="right">
						<%=ocia.NumNoConfNuevas%>
					</td>
				</tr>
						
				<%
				if mclass="filaImpar" then
					mclass="filaPar"
				else
					mclass="filaImpar" 
				end if
				end if
			next 
		end if
			
	  %>
		</table>
	</div>
<%end if


set oRaiz = nothing

%>

</body>
</html>

