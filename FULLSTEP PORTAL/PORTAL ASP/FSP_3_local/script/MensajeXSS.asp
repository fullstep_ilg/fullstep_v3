﻿<%@ Language=VBScript %>
<!--#include file="common/idioma.asp"-->
<% Response.Expires=0 %>
<HTML>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</HEAD>
<BODY>

<% 

''' <summary>
''' Tras controlar el cross-site scripting (XSS), si lo hay da esta pantalla de error
''' </summary> 
''' <remarks>Llamada desde: XSS.asp; Tiempo máximo:0</remarks>

	Idioma = Request("Idioma")
	Idioma = trim(Idioma)

	Dim Den
	den=devolverTextos(Idioma,127)
	
%>
<p class=error>
	<%=Den(1)%>
</p>
<P >
	<%=Den(2)%>
	<BR>
	<%=Den(3)%><A HREF=mailto:<%=Application ("MAILPORTAL")%>><span class=fmedia><%=Application ("MAILPORTAL")%></span></A>
</p>	
</BODY>
</HTML>
