﻿<%@  language="VBScript" %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/colores.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/fsal_1.asp"-->

<%
	Function salvarFichero(byref oStream, byVal ruta)
		On Error Resume Next
		
		dim writeToFileFailed
		dim adSaveCreateNotExist
		
		writeToFileFailed = 3004
		adSaveCreateOverWrite = 2
		oStream.SaveToFile ruta, adSaveCreateOverWrite
		salvarFichero=err.number
		if err.number=writeToFileFailed then
			err.Clear
			salvarFichero=-1
		else
			err.Clear
			salvarFichero=0
		end if		
	end function
%>
<%
''' <summary>
''' Controlar los obligatorios antes de confirmar oferta. 
''' Si todo esta bien se da opción a confirmar sino todo esta bien no da opción a confirmar oferta. En ambos casos muestra por pantalla los datos 
''' introducidos, bien los q se van a confirmar o bien los q faltan.
''' </summary>     
''' <remarks>Llamada desde: GuardarOferta.asp ; Tiempo máximo: 0</remarks>

OldTimeOut = server.ScriptTimeout
server.ScriptTimeout = 3600

anyoproceso=Request.Cookies ("PROCE_ANYO") 
gmn1proceso=Request.Cookies ("PROCE_GMN1") 
codproceso=Request.Cookies ("PROCE_COD") 

if Request("enviar")="1" then
	Idioma = Request.cookies("USU_IDIOMA")
	den = devolverTextos(Idioma,54)
	set oRaiz=validarUsuario(Idioma,true,false,0)
		
	'Obtiene los datos de la oferta
	CiaComp = clng(oRaiz.Sesion.CiaComp)
	CodProve = oRaiz.Sesion.CiaCodGs
	Set oProceso = oRaiz.Generar_CProceso

	oProceso.cargarConfiguracion CiaComp, anyoproceso, gmn1proceso, codproceso, CodProve
	oProceso.cargarProceso CiaComp, Idioma, anyoproceso, gmn1proceso, codproceso

	porFecha=0

	procesocerrado = false	
	if not isnull(oProceso.FechaMinimoLimOfertas) then
		if oRaiz.ObtenerSQLUTCDate(CiaComp)>oProceso.FechaMinimoLimOfertas then
			procesocerrado = true
			porFecha =1
		end if
	end if%>
    <%if procesocerrado = true then

		set oProceso = nothing
		set oRaiz = nothing%>
        <script>
            window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/procesocerrado.asp?PorFecha=<%=porFecha%>&Idioma=<%=Idioma%>&Anyo=<%=anyoproceso%>&Gmn1=<%=gmn1proceso%>&Proc=<%=codproceso%>","fraOfertaClient")
            window.close()
        </script>
      <%if Application("FSAL")=1 then
            GuardarFSAL2
        end if
        Response.End
	end if
    
	NumOfe = request("NumOfe")
	
	Set oOferta = oRaiz.Generar_COferta
	tipoFirmaDig = CInt(oOferta.devolverParamIntFirmDigOfe(CiaComp))
	
	set oOferta = nothing
	set oProceso = nothing
	set oRaiz = nothing

    dim rutHtml
    dim rutPdf

    rutHtml = Application("CARPETAUPLOADS") & "\" & Request("hRutHtml")
    rutPdf = Application("CARPETAUPLOADS") & "\" & Request("hRutPdf")

    Set oFsoLocal = Server.CreateObject("Scripting.FileSystemObject")

    If not (rutHtml="" or rutPdf="") and oFsoLocal.FileExists(rutHtml) and oFsoLocal.FileExists(rutPdf) Then    %>
<script>
		    window.open("confirmoferta2.asp?codMon=<%=Request.QueryString("codMon")%>&NumOfe=<%=Request.QueryString("NumOfe")%>&IDPortal=<%=Request.QueryString("IDPortal")%>&NumObj=<%=Request.QueryString("NumObj")%>&RutHtml=<%=Replace(rutHtml,"\","\\")%>&RutPdf=<%=Replace(rutPdf,"\","\\")%>","fraCOConfirmacion")
</script>
<%	
	    If tipoFirmaDig<=0 Then    'No hay que firmar, por lo que el usuario no debe tener la opción de elegir certificado.  %>
<script>
				window.open("<%=Application("RUTASEGURA")%>script/PMPortal/script/ofertas/OfertasPDF.aspx?rutaHtml=<%=Replace(rutHtml,"\","\\")%>&rutaPdf=<%=Replace(rutPdf,"\","\\")%>", "fraCOConfirmacion2")
</script>
<%
%><script>
			    window.parent.resizeTo(400,150)
			    window.open("<%=Application("RUTASEGURA")%>script/common/winespera.asp?msg=<%=den(153)%>&msg2=<%=den(65)%>", "fraCOEspera")
</script><%
	    else
		    'Hay que firmar, por lo que se le debe dar al usuario la opción de elegir certificado.
		    'Ahora lo que se hace es llamar a dos ventanas que se ejecutan en dos iframes distintos de la ventana de 
		    '/solicitudesoferta/winEspera. En el iframe fraCOEspera se ejecuta una página asp.net y en el iframe fraCOConfirmacion
		    'se ejecuta una página asp classic. Cuando la página asp.net acabe, la página asp.classic recogerá sus
		    'resultados y continuará.
%><script>
				window.parent.resizeTo(700,400)							
				window.open("<%=Application("RUTASEGURA")%>script/PMPortal/script/ofertas/OfertasPDF_Java.aspx?rutaHtml=<%=Replace(rutHtml,"\","\\")%>&rutaPdf=<%=Replace(rutPdf,"\","\\")%>", "fraCOEspera")
</script><%
	    End if
    else
        If oFsoLocal.FileExists(rutHtml) Then
            oFsoLocal.DeleteFile(rutHtml)
        End if
        rutHtml=""
        
        If oFsoLocal.FileExists(rutPdf) Then
            oFsoLocal.DeleteFile(rutPdf)
        End if
        rutPdf=""
%><script>
			window.parent.opener.open("ofertaenviada.asp?error=1000&NumObj=<%=request("NumObj")%>","default_main")
			window.parent.close()
</script>
    <%
    end if
    if Application("FSAL")=1 then
        GuardarFSAL2
    end if
	Response.End
end if

dim den
Idioma = Request.Cookies("USU_IDIOMA")
den = devolverTextos(Idioma,101)

set oRaiz=validarUsuario(Idioma,true,false,0)
	
'Obtiene los datos de la oferta
CiaComp = clng(oRaiz.Sesion.CiaComp)
CodProve = oRaiz.Sesion.CiaCodGs
anyoproceso=Request.Cookies ("PROCE_ANYO") 
gmn1proceso=Request.Cookies ("PROCE_GMN1") 
codproceso=Request.Cookies ("PROCE_COD") 

NumOfe = request("NumOfe")
codMon=Request("codMon")
denMon=Request("denMon")

'obtiene los formatos del usuario
decimalfmt = Request.Cookies ("USU_DECIMALFMT")
thousanfmt = Request.Cookies ("USU_THOUSANFMT") 
precisionfmt = CINT(Request.Cookies ("USU_PRECISIONFMT") )
datefmt = Request.Cookies ("USU_DATEFMT") 

dim IsSecure
IsSecure = left(Application ("RUTASEGURA"),8)
if IsSecure = "https://" then                       
    IsSecure = "; secure"
else
    IsSecure =""
end if

if request("idportal") = "null" then
    Response.AddHeader "Set-Cookie","PROCE_IDPORTAL=" &  null & "; path=/; HttpOnly" + IsSecure
else
    Response.AddHeader "Set-Cookie","PROCE_IDPORTAL=" &  request("idportal") & "; path=/; HttpOnly" + IsSecure
end if

dim bFalta
dim bFaltaElemento
bFalta = false
%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>
        <%=title%></title>
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
    <link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
    <script language="JavaScript" src="../common/ajax.js"></script>
    <script src="../common/formatos.js"></script>
    <!--#include file="../common/fsal_3.asp"-->
    <script>
        function Confirmar() {
            document.forms["frmConfirmar"].elements["cmdConfirmar"].disabled = true
            document.body.style.cursor = "wait"
            document.forms["frmConfirmar"].submit()
        }

        function init() {
            if (<%=application("FSAL")%> == 1)
            {
	            Ajax_FSALActualizar3(); //registro de acceso
	        }
            p = window.parent
            fr = p.document.getElementById("frEspera")

            if (fr) {
                fr.rows = "0,*"
            }

            p.resizeTo(700, 550)
        }
    </script>
</head>
<%
Set oProceso = oRaiz.Generar_CProceso

oProceso.cargarConfiguracion CiaComp, anyoproceso, gmn1proceso, codproceso, CodProve
oProceso.cargarProceso CiaComp, Idioma

If oProceso.DefEspecificaciones And oProceso.EspAdj Then
    oProceso.CargarEspecificaciones CiaComp
End If

set oOferta = oProceso.CargarOfertaProveedor(Idioma,ciacomp,codprove)
equiv = oProceso.cambio * oOferta.cambio
oOferta.IDPortal = request("IDPortal")
set oOferta.proceso = oProceso

' Carga de los datos de Costes / Descuentos
oProceso.CargarCostesDescuentos CiaComp
	
oProceso.CargarAtributos CiaComp
oProceso.Oferta.CargarAtributosOfertados CiaComp,oOferta.codmon
oOferta.cargarAdjuntos CiaComp

	
Set oProceso.Oferta.OfertaGrupos = oRaiz.Generar_COfertaGrupos()
Dim oGrupo
if not oProceso.Grupos is nothing then
	For Each oGrupo In oProceso.Grupos
	    oGrupo.cargarGrupo Idioma,CiaComp,,,,,codprove,oOferta.num,oOferta.codmon,oProceso.SolicitarCantMax
	    oGrupo.CargarEspecificaciones CiaComp
	    oGrupo.CargarAtributos CiaComp, 2
	    Set oOfeGrupo = oProceso.Oferta.OfertaGrupos.Add(oProceso.Oferta, oGrupo)
	    oGrupo.CargarAtributos CiaComp, 3
	Next
end if
%>
<style>
    .rellenado
    {
        background-color: #dcdcdc;
    }
</style>
<body topmargin="0" onload="init()">
    <form name="frmConfirmar" id="frmConfirmar" method="post">
    <input type="hidden" name="enviar" value="1">
    <input type="hidden" name="numObj" value='<%=request("NumOBJ")%>'>
    <input type="hidden" id="hRutHtml" name="hRutHtml" value="">
    <input type="hidden" id="hRutPdf" name="hRutPdf" value="">
    <div name="divConfirmacion" id="divConfirmacion">
    </div>
    </form>
    <hr>
    <div name="divObligatorio" id="divObligatorio" style="display: none; border: solid 3pt #FF0000">
    </div>
    <%
'esta variable no se muestra en la pantalla, pero se incluye en el fichero html para que se incluya en el PDF
dim sEstilos
sEstilos = "<style>TD {font-family:""Verdana"";font-size:10px;} .fgrande {font-size:14px;} .negrita {font-weight:bold;} .fmedia {font-size:12px;line-height:25px}</style>"

dim sCabeceraOferta

sCabeceraOferta = "<table width=""100%"">"
sCabeceraOferta = sCabeceraOferta & "<tr><td width=50% valign=top class=""negrita fgrande"">"
sCabeceraOferta = sCabeceraOferta & oProceso.anyo & "/" & oProceso.gmn1Cod & "/" & oProceso.cod & "&nbsp;" & oProceso.den & "</td>"
sCabeceraOferta = sCabeceraOferta & "<td width=50% valign=top class=""negrita fmedia"">"
sCabeceraOferta = sCabeceraOferta & "<div name=divTotalOferta id=divTotalOferta></div>"
sCabeceraOferta = sCabeceraOferta & "</td></tr></table>"

response.write sCabeceraOferta

'esta variable no se muestra en la pantalla, pero se incluye en el fichero html para que se incluya en el PDF
dim sMasDatosOferta

sMasDatosOferta = "<table width=""50%""><tr><td width=""15%"" class=negrita nowrap>" & den(65) & ":</td>"
sMasDatosOferta = sMasDatosOferta & "<td width=""10%"">" & visualizacionfecha(Date(),datefmt) & "</td>"
sMasDatosOferta = sMasDatosOferta & "<td width=""15%"" class=negrita nowrap>" & den(66) & ":</td>"
sMasDatosOferta = sMasDatosOferta & "<td width=""10%"">" & oOferta.Num + 1 & "</td></tr></table>"

dim sdivContenido
    %>
    <div name="divContenido" id="divContenido">
        <script>
            var cabecera = ""
            var cabeceraPrinted = false
            var subCabecera = ""
            var subCabeceraPrinted = false
        </script>
        <%if not oProceso.adminpublica then%>
        <script>
        cabecera += "<hr>"
        cabecera += "    <table>"
        cabecera += "		<tr>"
        cabecera += "			<td class=\"fmedia negrita\">"
        cabecera += "				<%=Replace(JSText(den(17)),"\","")%>"
        cabecera += "			</td>"
        cabecera += "		</tr>"
        cabecera += "	</table>"
        cabecera += "	"
        </script>
        <%	sdivContenido = sdivContenido & "<hr><table><tr><td class=""fmedia negrita"">" & Replace(JSText(den(17)),"\","") & "</td></tr></table><hr>"
	
	sdivContenido = sdivContenido & "<table width=""80%""><tr>"
	sdivContenido = sdivContenido & "<td width=20% class=negrita>" & den(18) & ":</td>"
	sdivContenido = sdivContenido & "<td width=""10%"" nowrap><span class=rellenado>" & visualizacionFecha(oOferta.FechaHasta,datefmt) & "</span></td>"	
	sdivContenido = sdivContenido & "<td width=""20%"">&nbsp;</td>"
	sdivContenido = sdivContenido & "<td width=""25%"" class=negrita nowrap>" & den(19) & ":</td>"
	sdivContenido = sdivContenido & "<td width=""25%""><span class=rellenado >" & oOferta.codmon & " - " & oOferta.monden & "</span></td>"
	sdivContenido = sdivContenido & "</tr></table>"

	if not isnull(oOferta.obs) and oOferta.obs <>"" then
		sdivContenido = sdivContenido & "<table width=""100%"">"
		sdivContenido = sdivContenido & "<tr><td class=negrita>" & den(20) & "</td></tr>"
		sdivContenido = sdivContenido & "<tr><td><span class=rellenado>" & VB2HTML(oOferta.obs) & "</span></td></tr>"
		sdivContenido = sdivContenido & "</table>"
	end if

	if oProceso.DefAdjunOferta then
		if not isnull(oOferta.ObsAdjuntos) and oOferta.obsAdjuntos<>"" then
			sdivContenido = sdivContenido & "<table width=""100%"">"
			sdivContenido = sdivContenido & "<tr><td class=negrita>" & den(21) & "</td></tr>"
			sdivContenido = sdivContenido & "<tr><td><span class=rellenado>" & VB2HTML(oOferta.ObsAdjuntos) & "</span></td></tr>"
			sdivContenido = sdivContenido & "</table>"
		end if
		
		if oOferta.adjuntos.count>0 then
			sdivContenido = sdivContenido & "<table width=""100%"">"
			sdivContenido = sdivContenido & "<tr><td colspan=2 class=negrita>" & den(22) & "</td></tr>"
			
			for each oAdjun in oOferta.adjuntos
				sdivContenido = sdivContenido & "<tr>"
				sdivContenido = sdivContenido & "<td width=25% valign=top><span class=rellenado>" & oAdjun.nombre & "</span></td>"
				sdivContenido = sdivContenido & "<td width=75% valign=top><span class=rellenado>" & VB2HTML(oAdjun.comentario) & "</span></td>"
				sdivContenido = sdivContenido & "</tr>"
			next
			sdivContenido = sdivContenido & "</table>"
		end if
	end if
	
    noAtributos = false
	if oProceso.atributos is nothing then
		noAtributos = true
	else
		if oProceso.atributos.count=0 then
			noAtributos = true
		end if
	end if
	if not noAtributos then %>
        <script>
            subCabecera = ""
            subCabecera += "<table width=100%>"
            subCabecera += "	<tr>"
            subCabecera += "		<td class=negrita><%=JSText(den(57))%></td>"
            subCabecera += " 	</tr>"
            subCabecera += "</table>"

            subCabeceraPrinted = false
        </script>
        <%	sdivContenido = sdivContenido & "<table width=""100%"">"
		sdivContenido = sdivContenido & "<tr><td class=negrita>" & JSText(den(23)) & "</td></tr>"
		sdivContenido = sdivContenido & "</table>"
			
		sdivContenido = sdivContenido & "<table width=""100%"">"
        %>
        <script>
		<%
	    for each oAtrib in oProceso.atributos
			bfaltaElemento = false
            sdivContenido = sdivContenido & "<tr>"
            %>
			var tr = ""
			tr = tr + "<tr>"
				<%if oAtrib.obligatorio then
                    sdivContenido = sdivContenido & "<td nowrap width=5% class=negrita>(*)&nbsp;" & JSText(oAtrib.Cod) & "</td>"
                    sdivContenido = sdivContenido & "<td width=30% class=negrita>" & JSText(oAtrib.den) & "</td>"
                    %>
					tr = tr + "<td nowrap width=5% class=negrita>(*)&nbsp;<%=JSText(oAtrib.Cod)%></td>"
					tr = tr + "<td width=30% class=negrita><%=JSText(oAtrib.den)%></td>"
				<%else
                    sdivContenido = sdivContenido & "<td nowrap width=""5%"">" & JSText(oAtrib.Cod) & "</td>"
                    sdivContenido = sdivContenido & "<td width=""30%"">" & JSText(oAtrib.den) & "</td>"
                    %>
					tr = tr + "<td nowrap width=5%><%=JSText(oAtrib.Cod)%></td>"
					tr = tr + "<td width=30%><%=JSText(oAtrib.den)%></td>"
				<%end if
                sdivContenido = sdivContenido & "<td width=""65%"">"%>
				tr = tr + "<td width=65%>"				
				<%
				if not oOferta.AtribsOfertados is nothing then
					if not oOferta.AtribsOfertados.Item(cstr(oAtrib.PAID)) is nothing then
						if oAtrib.obligatorio and isnull(oOferta.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor) then
							sdivContenido = sdivContenido & "<span class=error>****"
                            Response.Write "tr = tr + ""<span class=error>****"""
							bFalta = true
							bFaltaElemento = true
						else
							select case oAtrib.Tipo
								case 1
                                    sdivContenido = sdivContenido & "<span class=rellenado>" & VB2HTML(oOferta.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor)
									response.write "tr = tr + ""<span class=rellenado>" & VB2HTML(oOferta.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor) & """"
								case 2
									'no aplicamos la condición de si es precio por que el cambio es 1 siempre al estar los atributos generales siempre en moneda de la oferta
									vValor = oOferta.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor
									sdivContenido = sdivContenido & "<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt)
                                    response.write "tr = tr + ""<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt) & """"
								case 3
                                    sdivContenido = sdivContenido & "<span class=rellenado>" & visualizacionFecha(oOferta.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor,datefmt)
									response.write "tr = tr + ""<span class=rellenado>" &  visualizacionFecha(oOferta.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor,datefmt) & """"
								case 4
									if oOferta.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor then
                                        sdivContenido = sdivContenido & "<span class=rellenado>" & JSText(den(27))
										Response.write  "tr = tr + ""<span class=rellenado>" & JSText(den(27)) & """"
									else
                                        if isnull(oOferta.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor) then
                                            sdivContenido = sdivContenido & "<span class=rellenado>" & ""
										    Response.write  "tr = tr + ""<span class=rellenado>" & "" & """"                                        
                                        else
                                            sdivContenido = sdivContenido & "<span class=rellenado>" & JSText(den(28))
										    Response.write  "tr = tr + ""<span class=rellenado>" & JSText(den(28)) & """"
									    end if
                                    end if
							end select
						end if
					else
						if oAtrib.obligatorio  then
                            sdivContenido = sdivContenido & "<span class=error>****"
							Response.Write "tr = tr + ""<span class=error>****"""
							bFalta = true
							bFaltaElemento = true
						else					
                            sdivContenido = sdivContenido & "<span>&nbsp;"
							Response.Write "tr = tr + ""<span>&nbsp;"""
						end if
					end if
				else
					
					if oAtrib.obligatorio then
                        sdivContenido = sdivContenido & "<span class=error>****"
						Response.Write "tr = tr + ""<span class=error>****"""
						bFalta = true
						bFaltaElemento = true
					else					
                        sdivContenido = sdivContenido & "<span>&nbsp;"
						Response.Write "tr = tr + ""<span>&nbsp;"""
					end if
				end if
				sdivContenido = sdivContenido & "</span></td></tr>" %>				
				tr = tr + "</span></td>"
			tr = tr + "</tr>"			
				//document.write (tr)
			tr = tr.replace("width=30%","width=50%")
			tr = tr.replace("width=65%","width=45%")
			<%if bFaltaElemento then %>
				if (!cabeceraPrinted)
				{
					document.getElementById("divObligatorio").innerHTML += cabecera
					cabeceraPrinted = true
				}
				if (!subCabeceraPrinted)
				{
					document.getElementById("divObligatorio").innerHTML += subCabecera
					subCabeceraPrinted = true
				}
				document.getElementById("divObligatorio").innerHTML += "<table width=97% style=\"margin-left:10pt;\">" + tr + "</table>"
			<%end if%>
			<%next%>
			
        </script>
        <%sdivContenido = sdivContenido & "</table>"
	end if

    '''''''''Tratamiento para los costes/descuentos'''''''''

   'Comprobamos si hay costes/descuentos que tratar
	noCostesDescuentosOferta = true
	if  oProceso.HayCostesDescuentos then
		noCostesDescuentosOferta = false
	end if

    ' Si hay costes/descuentos de oferta se sacan 
   	if not noCostesDescuentosOferta then%>
        <script>
		subCabecera = ""		
		subCabecera += "<table width=100%>"
		subCabecera += "	<tr>"
		subCabecera += "		<td class=negrita><%=Replace(JSText(den(50)),"\","")%></td>"  <!-- Costes/descuentos adicionales de la oferta-->
		subCabecera += " 	</tr>"
		subCabecera += "</table>"
				
		subCabeceraPrinted = false
	
        </script>
        <%
   		sdivContenido = sdivContenido & "<table width=""100%""><tr>"
   		sdivContenido = sdivContenido & "<td class=negrita>" & Replace(JSText(den(50)),"\","") & "</td>"
   		sdivContenido = sdivContenido & "</tr></table>"
   	
   		sdivContenido = sdivContenido & "<table width=""100%"">"	
        %>
        <script>
	    <%if not oProceso.CostesDescTotalOferta is nothing then
		for each oCosteDesc in oProceso.CostesDescTotalOferta
			bfaltaElemento = false
            sdivContenido = sdivContenido & "<tr>"
            %>
			var tr = ""
			tr += "<tr>"
				<%if oCosteDesc.obligatorio then
                    sdivContenido = sdivContenido & "<td nowrap width=5% class=negrita>(*)&nbsp;" & JSText(oCosteDesc.Cod) & "</td>"
                    sdivContenido = sdivContenido & "<td width=30% class=negrita>" & JSText(oCosteDesc.den) & "</td>"
                    %>
					tr+="<td nowrap width=5% class=negrita>(*)&nbsp;<%=JSText(oCosteDesc.Cod)%></td>"
					tr +="<td width=30% class=negrita><%=JSText(oCosteDesc.den)%></td>"
				<%else
                    sdivContenido = sdivContenido & "<td nowrap width=""5%"">" & JSText(oCosteDesc.Cod) & "</td>"
                    sdivContenido = sdivContenido & "<td width=""30%"">" & JSText(oCosteDesc.den) & "</td>" %>
					tr+="<td nowrap width=5%><%=JSText(oCosteDesc.Cod)%></td>"
					tr+="<td width=30%><%=JSText(oCosteDesc.den)%></td>"
				<%end if
                sdivContenido = sdivContenido & "<td width=""65%"">" %>
				tr +="<td width=65%>"
				<%
				if not oOferta.AtribsOfertados is nothing then				
					if not oOferta.AtribsOfertados.Item(cstr(oCosteDesc.PAID)) is nothing then						
						if oCosteDesc.obligatorio and isnull(oOferta.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor) then
							sdivContenido = sdivContenido & "<span class=error>****"
                            Response.Write "tr = tr + ""<span class=error>****"""
							bFalta = true
							bFaltaElemento = true
						else
							vValor = oOferta.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor
                            sdivContenido = sdivContenido & "<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt)
							response.write "tr = tr + ""<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt) & """"
						end if
					else						
						if oCosteDesc.obligatorio  then
                            sdivContenido = sdivContenido & "<span class=error>****"
							Response.Write "tr = tr + ""<span class=error>****"""
							bFalta = true
							bFaltaElemento = true
						else
                            sdivContenido = sdivContenido & "<span>&nbsp;"					
							Response.Write "tr = tr + ""<span>&nbsp;"""
						end if
					end if
				else
					if oCosteDesc.obligatorio then
                        sdivContenido = sdivContenido & "<span class=error>****"
						Response.Write "tr = tr + ""<span class=error>****"""
						bFalta = true
						bFaltaElemento = true
					else					
                        sdivContenido = sdivContenido & "<span>&nbsp;"					
						Response.Write "tr = tr + ""<span>&nbsp;"""
					end if
				end if
				sdivContenido = sdivContenido & "</span></td></tr>" %>	
				tr +="</span></td>"
			tr +="</tr>"		
			//document.write (tr)
			tr = tr.replace("width=30%","width=50%")
			tr = tr.replace("width=65%","width=45%")

			<%if bFaltaElemento then %>
				if (!cabeceraPrinted)
				{
					document.getElementById("divObligatorio").innerHTML += cabecera
					cabeceraPrinted = true
				}
				if (!subCabeceraPrinted)
				{
					document.getElementById("divObligatorio").innerHTML += subCabecera
					subCabeceraPrinted = true
				}
				document.getElementById("divObligatorio").innerHTML += "<table width=97% style=\"margin-left:10pt;\">" + tr + "</table>"
			<%end if
		next
        end if        %>
        </script>
        <%  
        '''''''''Tratamiento para los costes/descuentos de Grupo ámbito oferta'''''''''
    dim noCostesDescuentosGrupoOfe
   'Comprobamos si hay costes/descuentos de Item que tratar
	noCostesDescuentosGrupoOfe = false
	if  oProceso.CostesDescTotalGrupo is nothing then
		noCostesDescuentosGrupoOfe= true
	else
        if  oProceso.CostesDescTotalGrupo.count=0 then
            noCostesDescuentosGrupoOfe = true
		end if
	end if


    ' Si hay costes/descuentos de grupo para la oferta se sacan 
   	if not noCostesDescuentosGrupoOfe then%>
        <script>
			
		<%for each oCosteDesc in oProceso.CostesDescTotalGrupo
			bfaltaElemento = false
            if oCosteDesc.ambito = 1 then
                sdivContenido = sdivContenido & "<tr>" %>
				var tr=""
			    tr +="<tr>"
				    <%if oCosteDesc.obligatorio then
                        sdivContenido = sdivContenido & "<td nowrap width=5% class=negrita>(*)&nbsp;" & JSText(oCosteDesc.Cod) & "</td>"
                        sdivContenido = sdivContenido & "<td width=30% class=negrita>" & JSText(oCosteDesc.den) & "</td>" %>
					    tr +="<td nowrap width=5% class=negrita>(*)&nbsp;<%=JSText(oCosteDesc.Cod)%></td>"
					    tr +="<td width=30% class=negrita><%=JSText(oCosteDesc.den)%></td>"
				    <%else
                        sdivContenido = sdivContenido & "<td nowrap width=""5%"">" & JSText(oCosteDesc.Cod) & "</td>"
                        sdivContenido = sdivContenido & "<td width=""30%"">" & JSText(oCosteDesc.den) & "</td>" %>
					    tr +="<td nowrap width=5%><%=JSText(oCosteDesc.Cod)%></td>"
					    tr +="<td width=30%><%=JSText(oCosteDesc.den)%></td>"
				    <%end if
                    sdivContenido = sdivContenido & "<td width=""65%"">"%>
				    tr +="<td width=65%>"
				    <%
				    if not oOferta.AtribsOfertados is nothing then
					    if not oOferta.AtribsOfertados.Item(cstr(oCosteDesc.PAID)) is nothing then
						    if oCosteDesc.obligatorio and isnull(oOferta.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor) then
							    sdivContenido = sdivContenido & "<span class=error>****"
                                Response.Write "tr = tr + ""<span class=error>****"""
							    bFalta = true
								bFaltaElemento = true
						    else
							    vValor = oOferta.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor
                                sdivContenido = sdivContenido & "<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt)
							    response.write "tr = tr + ""<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt) & """"
						    end if
					    else
						    if oCosteDesc.obligatorio  then
                                sdivContenido = sdivContenido & "<span class=error>****"
							    Response.Write "tr = tr + ""<span class=error>****"""
							    bFalta = true
								bFaltaElemento = true
						    else		
                                sdivContenido = sdivContenido & "<span>&nbsp;"			
							    Response.Write "tr = tr + ""<span>&nbsp;"""
						    end if
					    end if
				    else
					    if oCosteDesc.obligatorio then
                            sdivContenido = sdivContenido & "<span class=error>****"
						    Response.Write "tr = tr + ""<span class=error>****"""
						    bFalta = true
							bFaltaElemento = true
					    else	
                            sdivContenido = sdivContenido & "<span>&nbsp;"						
						    Response.Write "tr = tr + ""<span>&nbsp;"""
					    end if
				    end if
				    sdivContenido = sdivContenido & "</span></td></tr>" %>
				    tr +="</span></td>"
			    tr +="</tr>"				
				//document.write (tr)
				tr = tr.replace("width=30%","width=50%")
				tr = tr.replace("width=65%","width=45%")
				
				<%if bFaltaElemento then %>
					if (!cabeceraPrinted)
					{
						document.getElementById("divObligatorio").innerHTML += cabecera
						cabeceraPrinted = true
					}
					if (!subCabeceraPrinted)
					{
						document.getElementById("divObligatorio").innerHTML += subCabecera
						subCabeceraPrinted = true
					}
					document.getElementById("divObligatorio").innerHTML += "<table width=97% style=\"margin-left:10pt;\">" + tr + "</table>"
				<%end if
            end if
		next
      end if     %>
        </script>
        <%

    '''''''''Tratamiento para los costes/descuentos de Item ámbito oferta'''''''''
    dim noCostesDescuentosTotItemOfe, noCostesDescuentosPreItemOfe
   'Comprobamos si hay costes/descuentos de Item que tratar
	noCostesDescuentosTotItemOfe = false
	if  oProceso.CostesDescTotalItem is nothing then
		noCostesDescuentosTotItemOfe = true
	else
        if  oProceso.CostesDescTotalItem.count=0 then
            noCostesDescuentosTotItemOfe = true
		end if
	end if

   'Comprobamos si hay costes/descuentos de Item que tratar
	noCostesDescuentosPreItemOfe = false
	if  oProceso.CostesDescPrecItem is nothing then
		noCostesDescuentosPreItemOfe = true
	else
        if  oProceso.CostesDescPrecItem.count=0 then
            noCostesDescuentosPreItemOfe = true
		end if
	end if


    ' Si hay costes/descuentos de item para la oferta se sacan 
   	if not noCostesDescuentosTotItemOfe then%>
        <script>
		<%for each oCosteDesc in oProceso.CostesDescTotalItem%>
			<%bfaltaElemento = false%>		
            <%if oCosteDesc.ambito = 1 then
                sdivContenido = sdivContenido & "<tr>" %>
				var tr = ""
			    tr+="<tr>"
				    <%if oCosteDesc.obligatorio then
                        sdivContenido = sdivContenido & "<td nowrap width=5% class=negrita>(*)&nbsp;" & JSText(oCosteDesc.Cod) & "</td>"
                        sdivContenido = sdivContenido & "<td width=30% class=negrita>" & JSText(oCosteDesc.den) & "</td>"
                        sdivContenido = sdivContenido & "<td width=30% class=negrita>" & JSText(den(54)) & "</td>"
                    %>
					    tr+="<td nowrap width=5% class=negrita>(*)&nbsp;<%=JSText(oCosteDesc.Cod)%></td>"
					    tr+="<td width=30% class=negrita><%=JSText(oCosteDesc.den)%></td>"
                        tr+="<td width=30% class=negrita><%=JSText(den(54))%></td>"
				    <%else
                        sdivContenido = sdivContenido & "<td nowrap width=""5%"">" & JSText(oCosteDesc.Cod) & "</td>"
                        sdivContenido = sdivContenido & "<td width=""30%"">" & JSText(oCosteDesc.den) & "</td>"
                        sdivContenido = sdivContenido & "<td width=""30%"">" & JSText(den(54)) & "</td>"
                        %>
					    tr+="<td nowrap width=5%><%=JSText(oCosteDesc.Cod)%></td>"
					    tr+="<td width=30%><%=JSText(oCosteDesc.den)%></td>"
                        tr+="<td width=30%><%=JSText(den(54))%></td>"
				    <%end if
                    sdivContenido = sdivContenido & "<td width=""65%"">" %>
				    tr+="<td width=65%>"
				    <%
				    if not oOferta.AtribsOfertados is nothing then
					    if not oOferta.AtribsOfertados.Item(cstr(oCosteDesc.PAID)) is nothing then
						    if oCosteDesc.obligatorio and isnull(oOferta.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor) then
							    sdivContenido = sdivContenido & "<span class=error>****"
                                Response.Write "tr = tr + ""<span class=error>****"""
							    bFalta = true
								bFaltaElemento = true
						    else
							    vValor = oOferta.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor
                                sdivContenido = sdivContenido & "<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt)
							    response.write "tr = tr + ""<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt) & """"
						    end if
					    else
						    if oCosteDesc.obligatorio  then
                                sdivContenido = sdivContenido & "<span class=error>****"
							    Response.Write "tr = tr + ""<span class=error>****"""
							    bFalta = true
								bFaltaElemento = true
						    else					
                                sdivContenido = sdivContenido & "<span>&nbsp;"
							    Response.Write "tr = tr + ""<span>&nbsp;"""
						    end if
					    end if
				    else
					    if oCosteDesc.obligatorio then
                            sdivContenido = sdivContenido & "<span class=error>****"
						    Response.Write "tr = tr + ""<span class=error>****"""
						    bFalta = true
							bFaltaElemento = true
					    else
                            sdivContenido = sdivContenido & "<span>&nbsp;"					
						    Response.Write "tr = tr + ""<span>&nbsp;"""
					    end if
				    end if
				    sdivContenido = sdivContenido & "</span></td></tr>" %>
				    tr+="</span></td>"
			    tr+="</tr>"
				//document.write (tr)
				tr = tr.replace("width=30%","width=50%")
				tr = tr.replace("width=65%","width=45%")
				
				<%if bFaltaElemento then %>
					if (!cabeceraPrinted)
					{
						document.getElementById("divObligatorio").innerHTML += cabecera
						cabeceraPrinted = true
					}
					if (!subCabeceraPrinted)
					{
						document.getElementById("divObligatorio").innerHTML += subCabecera
						subCabeceraPrinted = true
					}
					document.getElementById("divObligatorio").innerHTML += "<table width=97% style=\"margin-left:10pt;\">" + tr + "</table>"
				<%end if
            end if
		next%>
        </script>
        <%end if
    
    if not noCostesDescuentosPreItemOfe then%>
        <script>
				
			
		    <%for each oCosteDesc in oProceso.CostesDescPrecItem%>
				<%bfaltaElemento = false%>		
                <%if oCosteDesc.ambito = 1 then
                    sdivContenido = sdivContenido & "<tr>" %>
					var tr = ""
			        tr+="<tr>"
				        <%if oCosteDesc.obligatorio then
                            sdivContenido = sdivContenido & "<td nowrap width=5% class=negrita>(*)&nbsp;" & JSText(oCosteDesc.Cod) & "</td>" 
                            sdivContenido = sdivContenido & "<td width=30% class=negrita>" & JSText(oCosteDesc.den) & "</td>"
                            sdivContenido = sdivContenido & "<td width=30% class=negrita>" & JSText(den(55)) & "</td>"
                            %>
					        tr+="<td nowrap width=5% class=negrita>(*)&nbsp;<%=JSText(oCosteDesc.Cod)%></td>"
					        tr+="<td width=30% class=negrita><%=JSText(oCosteDesc.den)%></td>"
                            tr+="<td width=30% class=negrita><%=JSText(den(55))%></td>"
				        <%else
                            sdivContenido = sdivContenido & "<td nowrap width=""5%"">" & JSText(oCosteDesc.Cod) & "</td>"
                            sdivContenido = sdivContenido & "<td width=""30%"">" & JSText(oCosteDesc.den) & "</td>"
                            sdivContenido = sdivContenido & "<td width=""30%"">" & JSText(den(55)) & "</td>"
                            %>
					        tr+="<td nowrap width=5%><%=JSText(oCosteDesc.Cod)%></td>"
					        tr+="<td width=30%><%=JSText(oCosteDesc.den)%></td>"
                            tr+="<td width=30%><%=JSText(den(55))%></td>"
				        <%end if
                        sdivContenido = sdivContenido & "<td width=""65%"">" %>
				        tr+="<td width=65%>"
				        <%
				        if not oOferta.AtribsOfertados is nothing then
					        if not oOferta.AtribsOfertados.Item(cstr(oCosteDesc.PAID)) is nothing then
						        if oCosteDesc.obligatorio and isnull(oOferta.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor) then
							        sdivContenido = sdivContenido & "<span class=error>****"
                                    Response.Write "tr = tr + ""<span class=error>****"""
							        bFalta = true
									bFaltaElemento = true
						        else
							        vValor = oOferta.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor
                                    sdivContenido = sdivContenido & "<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt)
							        response.write "tr = tr + ""<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt) & """"
						        end if
					        else
						        if oCosteDesc.obligatorio  then
                                    sdivContenido = sdivContenido & "<span class=error>****"
							        Response.Write "tr = tr + ""<span class=error>****"""
							        bFalta = true
									bFaltaElemento = true
						        else
                                    sdivContenido = sdivContenido & "<span>&nbsp;"										
							        Response.Write "tr = tr + ""<span>&nbsp;"""
						        end if
					        end if
				        else
					        if oCosteDesc.obligatorio then
                                sdivContenido = sdivContenido & "<span class=error>****"
						        Response.Write "tr = tr + ""<span class=error>****"""
						        bFalta = true
								bFaltaElemento = true
					        else
                                sdivContenido = sdivContenido & "<span>&nbsp;"															
						        Response.Write "tr = tr + ""<span>&nbsp;"""
					        end if
				        end if
				        sdivContenido = sdivContenido & "</span></td></tr>" %>
				        tr+="</span></td>"
			        tr+="</tr>"
					//document.write (tr)
					tr = tr.replace("width=30%","width=50%")
					tr = tr.replace("width=65%","width=45%")
					
					<%if bFaltaElemento then %>
						if (!cabeceraPrinted)
						{
							document.getElementById("divObligatorio").innerHTML += cabecera
							cabeceraPrinted = true
						}
						if (!subCabeceraPrinted)
						{
							document.getElementById("divObligatorio").innerHTML += subCabecera
							subCabeceraPrinted = true
						}
						document.getElementById("divObligatorio").innerHTML += "<table width=97% style=\"margin-left:10pt;\">" + tr + "</table>"
					<%end if	
                end if
		     next%>
        </script>
        <%end if

        sdivContenido = sdivContenido & "</table>" 
    end if
end if

dim oDestinos 
set oDestinos = nothing
if not oProceso.Grupos is nothing then
for each oGrupo in oProceso.grupos

if oProceso.DefDestino=3 or (oProceso.DefDestino=2 and oGrupo.DefDestino=false) then
	if oDestinos is nothing then
		set oDestinos = oRaiz.Generar_CDestinos()

		oDestinos.CargarTodosLosDestinosDesde Idioma,ciaComp, 10000
	end if
end if

sDivContenido = sDivContenido & "<hr>"
sDivContenido = sDivContenido & "<table>"
sDivContenido = sDivContenido & "<tr>"
sDivContenido = sDivContenido & "<td class=""fmedia negrita"">"
sDivContenido = sDivContenido & JSText(den(24)) & "&nbsp;" & JSText(oGrupo.codigo) & "&nbsp;-&nbsp;" & JSText(oGrupo.den)
if oProceso.adminpublica then
	sDivContenido = sDivContenido & "&nbsp;(" & JSText(den(25)) & " " & JSText(oGrupo.sobre) & ":" & JSText(den(29)) & visualizacionfecha(oProceso.sobres.item(oGrupo.sobre).FechaApertura,datefmt) & ")"
end if
sDivContenido = sDivContenido & "</td>"
sDivContenido = sDivContenido & "<td width=30% class=""negrita fmedia numero"">"
sDivContenido = sDivContenido & "<nobr>"
sDivContenido = sDivContenido & Replace(JSText(den(63)),"\","") & "&nbsp;" & visualizacionNumero(oGrupo.importeTotal,decimalfmt,thousandfmt,precisionfmt) & "&nbsp;" & JSText(oOferta.codmon)
sDivContenido = sDivContenido & "</nobr>"
sDivContenido = sDivContenido & "</td>"
sDivContenido = sDivContenido & "</tr>"
sDivContenido = sDivContenido & "</table>"
sDivContenido = sDivContenido & "	"

        %>
        <script>

cabecera="<hr>"
var cabecera1 = ""
var cabecera2 = ""
cabecera +=	"    <table>"
cabecera +=	"		<tr>"
cabecera +=	"			<td class=\"fmedia negrita\">"
cabecera +=	"				<%=JSText(den(24))%>&nbsp;<%=JSText(oGrupo.codigo)%>&nbsp;-&nbsp;<%=JSText(oGrupo.den)%><%if oProceso.adminpublica then%>&nbsp;(<%=JSText(den(25))%><%=JSText(oGrupo.sobre)%>:<%=JSText(den(29))%><%=visualizacionfecha(oProceso.sobres.item(oGrupo.sobre).FechaApertura,datefmt)%>)<%end if%>"
cabecera +=	"			</td>"
cabecera1 +=	"		<td width=30% class=\"negrita fmedia numero\">"
cabecera1 +=	"			<nobr>"
cabecera1 +=	"				<%=Replace(JSText(den(63)),"\","")%>&nbsp;<%=visualizacionNumero(oGrupo.importeTotal,decimalfmt,thousandfmt,precisionfmt)%>&nbsp;<%=JSText(oOferta.codmon)%>"
cabecera1 +=	"			</nobr>"
cabecera1 +=	"		</td>"
cabecera2 +=	"		</tr>"
cabecera2 +=	"	</table>"
cabecera2 +=	"	"

cabecera = cabecera + cabecera2


cabeceraPrinted = false

        </script>
        <%

set oOfeGrupo = oOferta.OfertaGrupos.item(oGrupo.codigo)
oOfeGrupo.CargarAtributosOfertados CiaComp,oOferta.codmon
oGrupo.cargarItems Idioma,CiaComp,codmon,codprove,oOferta.num


if oGrupo.hayEscalados then
	oGrupo.CargarEscalados CiaComp
end if

oOfeGrupo.cargarPrecios ciacomp,oOferta.codmon
if oGrupo.hayEscalados then
	oOfegrupo.cargarPreciosEscalados CiaComp
end if

oOfeGrupo.CargarTodosAdjuntos CiaComp

if oProceso.DefAdjunGrupo then
	if not isnull(oGrupo.ObsAdjuntos) and oGrupo.obsAdjuntos<>"" then
		sDivContenido = sDivContenido & "<table width=""100%"">"
		sDivContenido = sDivContenido & "<tr><td class=negrita>" & den(21) & "</td></tr>"
		sDivContenido = sDivContenido & "<tr><td><span class=rellenado>" & VB2HTML(oGrupo.ObsAdjuntos) & "</span></td></tr>"
		sDivContenido = sDivContenido & "</table>"
	end if
	
	if oOfeGrupo.adjuntos.count>0 then
		sDivContenido = sDivContenido & "<table width=""100%"">"
		sDivContenido = sDivContenido & "<tr><td colspan =2 class=negrita>" & den(22) & "</td></tr>"
					
		for each oAdjun in oOfeGrupo.adjuntos
			sDivContenido = sDivContenido & "<tr>"
			sDivContenido = sDivContenido & "<td width=25% valign=top>"
			sDivContenido = sDivContenido & "<span class=rellenado>" & oAdjun.nombre & "</span>"
			sDivContenido = sDivContenido & "</td>"
			sDivContenido = sDivContenido & "<td width=75% valign=top>"
			sDivContenido = sDivContenido & "<span class=rellenado>" & VB2HTML(oAdjun.comentario) & "</span>"
			sDivContenido = sDivContenido & "</td>"
			sDivContenido = sDivContenido & "</tr>"
		next
		sDivContenido = sDivContenido & "</table>"
	end if%>
        <%end if%>
        <%
noAtributos = false
if oGrupo.atributos is nothing then
	noAtributos = true
else
	if oGrupo.atributos.count=0 then
		noAtributos = true
	end if
end if



if not noAtributos then
	sDivContenido = sDivContenido & "<table width=97% style=""margin-left:10px;"">"
	sDivContenido = sDivContenido & "<tr><td class=negrita>" & JSText(den(23)) & "</td></tr>"
	sDivContenido = sDivContenido & "</table>"
        %>
        <script>
            subCabecera = ""
            subCabecera += "<table width=97% style=\"margin-left:10px;\">"
            subCabecera += "	<tr>"
            subCabecera += "		<td class=negrita><%=JSText(den(57))%></td>"
            subCabecera += " 	</tr>"
            subCabecera += "</table>"

            subCabeceraPrinted = false
        </script>
        <%sDivContenido = sDivContenido & "<table width=""100%"">" %>
        <script>


<%noAtributos = true
if oGrupo.items.count >0 then    
    for each oItem in oGrupo.items
        if not (Isnull(oItem.precioItem.PrecioOferta)) then
            noAtributos = false
        end if
    next
end if

for each oAtrib in oGrupo.atributos%>
	<%bfaltaElemento = false%>			
	var tr = ""
	tr+="<tr>"
		<%if oAtrib.obligatorio then
            sdivContenido = sdivContenido & "<td nowrap width=5% class=negrita>(*)&nbsp;" & JSText(oAtrib.Cod) & "</td>"
            sdivContenido = sdivContenido & "<td width=30% class=negrita>" & JSText(oAtrib.den) & "</td>"
            %>
			tr+="<td nowrap width=5% class=negrita>(*)&nbsp;<%=JSText(oAtrib.Cod)%></td>"
			tr+="<td width=30% class=negrita><%=JSText(oAtrib.den)%></td>"
		<%else
            sdivContenido = sdivContenido & "<td nowrap width=""5%"">" & JSText(oAtrib.Cod) & "</td>" 
            sdivContenido = sdivContenido & "<td width=""30%"">" & JSText(oAtrib.den) & "</td>"
            %>
			tr+="<td nowrap width=5%><%=JSText(oAtrib.Cod)%></td>"
			tr+="<td width=30%><%=JSText(oAtrib.den)%></td>"
		<%end if
        sdivContenido = sdivContenido & "<td width=""65%"">" %>
		tr+="<td width=65%>"
		<%
			if not oOfeGrupo.AtribsOfertados is nothing then
				if not oOfeGrupo.AtribsOfertados.Item(cstr(oAtrib.PAID)) is nothing then
					if oAtrib.obligatorio and isnull(oOfeGrupo.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor) and not noAtributos then
						sdivContenido = sdivContenido & "<span class=error>****"
                        Response.Write "tr = tr + ""<span class=error>****"""
						bFalta = true
						bFaltaElemento = true
					else
						select case oAtrib.Tipo
							case 1
                                sdivContenido = sdivContenido & "<span class=rellenado>" & VB2HTML(oOfeGrupo.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor)
								response.write "tr = tr + ""<span class=rellenado>" & VB2HTML(oOfeGrupo.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor) & """"
							case 2
								if (oAtrib.operacion = "+" or  oAtrib.operacion = "-") then
									vValor = oOfeGrupo.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor  / oOfeGrupo.cambio * equiv
								else
									vValor = oOfeGrupo.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor
								end if
                                sdivContenido = sdivContenido & "<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt)
								response.write "tr = tr + ""<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt) & """"
							case 3
                                sdivContenido = sdivContenido & "<span class=rellenado>" & visualizacionFecha(oOfeGrupo.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor,datefmt)
								response.write "tr = tr + ""<span class=rellenado>" &  visualizacionFecha(oOfeGrupo.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor,datefmt) & """"
							case 4
								if oOfeGrupo.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor then
                                    sdivContenido = sdivContenido & "<span class=rellenado>" & JSText(den(27))
									Response.write  "tr = tr + ""<span class=rellenado>" & JSText(den(27)) & """"
                                else
                                    if isnull(oOfeGrupo.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor) then
                                        sdivContenido = sdivContenido & "<span class=rellenado>" & "" 
									    Response.write  "tr = tr + ""><span class=rellenado>" & "" & """"
								    else
                                        sdivContenido = sdivContenido & "<span class=rellenado>" & JSText(den(28)) 
									    Response.write  "tr = tr + ""><span class=rellenado>" & JSText(den(28)) & """"
                                    end if
								end if
						end select
					end if
				else
					if oAtrib.obligatorio and not noAtributos then
                        sdivContenido = sdivContenido & "<span class=error>****"
						Response.Write "tr = tr + ""<span class=error>****"""
						bFalta = true
						bFaltaElemento = true
					else
                        sdivContenido = sdivContenido & "<span>&nbsp;"										
						Response.Write "tr = tr + ""<span>&nbsp;"""
					end if
				end if
			else
				if oAtrib.obligatorio and not noAtributos then
                    sdivContenido = sdivContenido & "<span class=error>****"
					Response.Write "tr = tr + ""<span class=error>****"""
					bFalta = true
					bFaltaElemento = true
				else
                    sdivContenido = sdivContenido & "<span>&nbsp;"										
					Response.Write "tr = tr + ""<span>&nbsp;"""
				end if
			end if
		sdivContenido = sdivContenido & "</span></td></tr>" %>
		tr+="</span></td>"
	tr+="</tr>"
	//document.write(tr)
	tr = tr.replace("width=30%","width=50%")	
	tr = tr.replace("width=65%","width=45%")
	
	<%if bFaltaElemento then %>
		if (!cabeceraPrinted)
		{
			document.getElementById("divObligatorio").innerHTML += cabecera
			cabeceraPrinted = true
		}
		if (!subCabeceraPrinted)
		{
			document.getElementById("divObligatorio").innerHTML += subCabecera
			subCabeceraPrinted = true
		}
		document.getElementById("divObligatorio").innerHTML += "<table width=94% style=\"margin-left:20pt;\">" + tr + "</table>"
	<%end if
next%>

        </script>
        <% sdivContenido = sdivContenido & "</table>"%>
        <%end if%>
        <%
'''''''''Tratamiento para los costes/descuentos de Total Grupo'''''''''

'Comprobamos si hay costes/descuentos de ambito Grupo que tratar
noCostesDescuentosGrupo = true
if oGrupo.HayCostesDescuentos then
	noCostesDescuentosGrupo = false
end if


if not noCostesDescuentosGrupo then

    sDivContenido = sDivContenido & "<table width=97% style=""margin-left:10pt;"">"
    sDivContenido = sDivContenido & "<tr><td class=negrita>" & JSText(den(51)) & "</td></tr>"
    sDivContenido = sDivContenido & "</table>"
        %>
        <script>
		subCabecera = ""		
		subCabecera += "<table width=97% style=\"margin-left:10pt;\">"
		subCabecera += "	<tr>"
		subCabecera += "		<td class=negrita><%=JSText(den(51))%></td>"  <!--Costes/descuentos adicionales del grupo -->
		subCabecera += " 	</tr>"
		subCabecera += "</table>"
		
		document.write (subCabecera)
		subCabeceraPrinted = false
        </script>
        <%
    sdivContenido = sdivContenido & "<table width=""100%"">"
    
    if not oProceso.CostesDescTotalGrupo is nothing then%>
        <script>		
<%for each oCosteDesc in oProceso.CostesDescTotalGrupo
    bfaltaElemento = false%>
	var tr=""
    <%if oCosteDesc.ambito = 2 and not oCosteDesc.Grupos.Item(oGrupo.ID) is nothing and (isnull(oCosteDesc.Alcance) or oCosteDesc.Alcance=oGrupo.Codigo) then
        sDivContenido = sDivContenido & "<tr>" %>
	    tr+="<tr>"
		<%if oCosteDesc.obligatorio then
            sDivContenido = sDivContenido & "<td nowrap width=5% class=negrita>(*)&nbsp;" & JSText(oCosteDesc.Cod) & "</td>"
            sDivContenido = sDivContenido & "<td width=30% class=negrita>" & JSText(oCosteDesc.den) & "</td>"
            %>
			tr+="<td nowrap width=5% class=negrita>(*)&nbsp;<%=JSText(oCosteDesc.Cod)%></td>"
			tr+="<td width=30% class=negrita><%=JSText(oCosteDesc.den)%></td>"
		<%else
            sDivContenido = sDivContenido & "<td nowrap width=""5%"">" & JSText(oCosteDesc.Cod) & "</td>"
            sDivContenido = sDivContenido & "<td width=""30%"">" & JSText(oCosteDesc.den) & "</td>"
            %>
			tr+="<td nowrap width=5%><%=JSText(oCosteDesc.Cod)%></td>"
			tr+="<td width=30%><%=JSText(oCosteDesc.den)%></td>"
		<%end if
        sDivContenido = sDivContenido & "<td width=""65%"">" %>
		tr+="<td width=65%>"
		<%
			if not oOfeGrupo.AtribsOfertados is nothing then
				if not oOfeGrupo.AtribsOfertados.Item(cstr(oCosteDesc.PAID)) is nothing then
					if oCosteDesc.obligatorio and isnull(oOfeGrupo.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor) then
						sdivContenido = sdivContenido & "<span class=error>****"
                        Response.Write "tr = tr + ""<span class=error>****"""
						bFalta = true
						bFaltaElemento = true
					else
						if (oCosteDesc.operacion = "+" or  oCosteDesc.operacion = "-") then
							vValor = oOfeGrupo.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor  / oOfeGrupo.cambio * equiv
						else
							vValor = oOfeGrupo.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor
						end if
                        sdivContenido = sdivContenido & "<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt)
						response.write "tr = tr + ""<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt) & """"
					end if
				else
					if oCosteDesc.obligatorio then
                        sdivContenido = sdivContenido & "<span class=error>****"
						Response.Write "tr = tr + ""<span class=error>****"""
						bFalta = true
						bFaltaElemento = true
					else		
                        sdivContenido = sdivContenido & "<span>&nbsp;"								
						Response.Write "tr = tr + ""<span>&nbsp;"""
					end if
				end if
			else
				if oCosteDesc.obligatorio then
                    sdivContenido = sdivContenido & "<span class=error>****"
					Response.Write "tr = tr + ""<span class=error>****"""
					bFalta = true
					bFaltaElemento = true
				else					
                    sdivContenido = sdivContenido & "<span>&nbsp;"					
					Response.Write "tr = tr + ""<span>&nbsp;"""
				end if
			end if
		sdivContenido = sdivContenido & "</span></td></tr>"  %>
		tr+="</span></td>"
	tr+="</tr>"
	//document.write(tr)
	tr = tr.replace("width=30%","width=50%")
	tr = tr.replace("width=65%","width=45%")	
	<%if bFaltaElemento then %>
		if (!cabeceraPrinted)
		{
			document.getElementById("divObligatorio").innerHTML += cabecera
			cabeceraPrinted = true
		}
		if (!subCabeceraPrinted)
		{
			document.getElementById("divObligatorio").innerHTML += subCabecera
			subCabeceraPrinted = true
		}
		document.getElementById("divObligatorio").innerHTML += "<table width=94% style=\"margin-left:20pt;\">" + tr + "</table>"
	<%end if		
	end if
next%>

        </script>
        <%end if
'-----------------------------
    '''''''''Tratamiento para los costes/descuentos de Item ámbito oferta'''''''''
    dim noCostesDescuentosTotItemGru, noCostesDescuentosPreItemGru
   'Comprobamos si hay costes/descuentos de Item que tratar
	noCostesDescuentosTotItemGru = false
	if  oProceso.CostesDescTotalItem is nothing then
		noCostesDescuentosTotItemGru = true
	else
        if  oProceso.CostesDescTotalItem.count=0 then
            noCostesDescuentosTotItemGru = true
		end if
	end if

   'Comprobamos si hay costes/descuentos de Item que tratar
	noCostesDescuentosPreItemGru = false
	if  oProceso.CostesDescPrecItem is nothing then
		noCostesDescuentosPreItemGru = true
	else
        if  oProceso.CostesDescPrecItem.count=0 then
            noCostesDescuentosPreItemGru = true
		end if
	end if

     ' Si hay costes/descuentos de item para la oferta se sacan 
   	if not noCostesDescuentosTotItemGru then%>
        <script>
		<%for each oCosteDesc in oProceso.CostesDescTotalItem
            bfaltaElemento = false%>
			var tr=""
            <%if oCosteDesc.ambito = 2 and (isnull(oCosteDesc.Alcance) or oCosteDesc.Alcance=oGrupo.Codigo) then
                sdivContenido = sdivContenido & "<tr>"
                %>
			    tr+="<tr>"
				    <%if oCosteDesc.obligatorio then
                        sdivContenido = sdivContenido & "<td nowrap width=5% class=negrita>(*)&nbsp;" & JSText(oCosteDesc.Cod) & "</td>"
                        sdivContenido = sdivContenido & "<td width=30% class=negrita>" & JSText(oCosteDesc.den) & "</td>"
                        sdivContenido = sdivContenido & "<td width=30% class=negrita>" & JSText(den(54)) & "</td>"
                        %>
					    tr+="<td nowrap width=5% class=negrita>(*)&nbsp;<%=JSText(oCosteDesc.Cod)%></td>"
					    tr+="<td width=30% class=negrita><%=JSText(oCosteDesc.den)%></td>"
                        tr+="<td width=30% class=negrita><%=JSText(den(54))%></td>"
				    <%else
                        sdivContenido = sdivContenido & "<td nowrap width=""5%"">" & JSText(oCosteDesc.Cod) & "</td>"
                        sdivContenido = sdivContenido & "<td width=""30%"">" & JSText(oCosteDesc.den) & "</td>"
                        sdivContenido = sdivContenido & "<td width=""30%"">" & JSText(den(54)) & "</td>"
                        %>
					    tr+="<td nowrap width=5%><%=JSText(oCosteDesc.Cod)%></td>"
					    tr+="<td width=30%><%=JSText(oCosteDesc.den)%></td>"
                        tr+="<td width=30%><%=JSText(den(54))%></td>"
				    <%end if
                    sdivContenido = sdivContenido & "<td width=""65%"">" %>
				    tr+="<td width=65%>"
				    <%
				    if not oOfeGrupo.AtribsOfertados is nothing then
					    if not oOfeGrupo.AtribsOfertados.Item(cstr(oCosteDesc.PAID)) is nothing then
						    if oCosteDesc.obligatorio and isnull(oOfeGrupo.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor) then
							    sdivContenido = sdivContenido & "<span class=error>****"
                                Response.Write "tr = tr + ""<span class=error>****"""
							    bFalta = true
								bFaltaElemento = true
						    else
							    vValor = oOfeGrupo.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor
                                sdivContenido = sdivContenido & "<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt)
							    response.write "tr = tr + ""<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt) & """"
						    end if
					    else
						    if oCosteDesc.obligatorio  then
                                sdivContenido = sdivContenido & "<span class=error>****"
							    Response.Write "tr = tr + ""<span class=error>****"""
							    bFalta = true
								bFaltaElemento = true
						    else
                                sdivContenido = sdivContenido & "<span>&nbsp;"										
							    Response.Write "tr = tr + ""<span>&nbsp;"""
						    end if
					    end if
				    else
					    if oCosteDesc.obligatorio then
                            sdivContenido = sdivContenido & "<span class=error>****"
						    Response.Write "tr = tr + ""<span class=error>****"""
						    bFalta = true
							bFaltaElemento = true
					    else
                            sdivContenido = sdivContenido & "<span>&nbsp;"										
						    Response.Write "tr = tr + ""<span>&nbsp;"""
					    end if
				    end if
				    sdivContenido = sdivContenido & "</span></td></tr>" %>	
				    tr+="</span></td>"
			    tr+="</tr>"
				//document.write(tr)
				tr = tr.replace("width=30%","width=50%")
				tr = tr.replace("width=65%","width=45%")
				
				<%if bFaltaElemento then %>
					if (!cabeceraPrinted)
					{
						document.getElementById("divObligatorio").innerHTML += cabecera
						cabeceraPrinted = true
					}
					if (!subCabeceraPrinted)
					{
						document.getElementById("divObligatorio").innerHTML += subCabecera
						subCabeceraPrinted = true
					}
					document.getElementById("divObligatorio").innerHTML += "<table width=94% style=\"margin-left:20pt;\">" + tr + "</table>"
				<%end if
			end if
	    next%>
        </script>
        <%      end if
      if not noCostesDescuentosPreItemGru then%>
        <script>
		    <%for each oCosteDesc in oProceso.CostesDescPrecItem
                bfaltaElemento = false%>
				var tr = ""
                <%if oCosteDesc.ambito = 2 and (isnull(oCosteDesc.Alcance) or oCosteDesc.Alcance=oGrupo.Codigo) then
                    sdivContenido = sdivContenido & "<tr>" %>
			        tr+="<tr>"
				        <%if oCosteDesc.obligatorio then
                            sdivContenido = sdivContenido & "<td nowrap width=5% class=negrita>(*)&nbsp;" & JSText(oCosteDesc.Cod) & "</td>"
                            sdivContenido = sdivContenido & "<td  width=30% class=negrita>" & JSText(oCosteDesc.den) & "</td>"
                            sdivContenido = sdivContenido & "<td  width=30% class=negrita>" & JSText(den(55)) & "</td>"
                            %>
					        tr+="<td nowrap width=5% class=negrita>(*)&nbsp;<%=JSText(oCosteDesc.Cod)%></td>"
					        tr+="<td  width=30% class=negrita><%=JSText(oCosteDesc.den)%></td>"
                            tr+="<td  width=30% class=negrita><%=JSText(den(55))%></td>"
				        <%else
                            sdivContenido = sdivContenido & "<td nowrap width=""5%"">" & JSText(oCosteDesc.Cod) & "</td>"
                            sdivContenido = sdivContenido & "<td width=""30%"">" & JSText(oCosteDesc.den) & "</td>"
                            sdivContenido = sdivContenido & "<td width=""30%"">" & JSText(den(55)) & "</td>"
                            %>
					        tr+="<td nowrap width=5%><%=JSText(oCosteDesc.Cod)%></td>"
					        tr+="<td width=30%><%=JSText(oCosteDesc.den)%></td>"
                            tr+="<td width=30%><%=JSText(den(55))%></td>"
				        <%end if
                        sdivContenido = sdivContenido & "<td width=""65%"">"   %>
				        tr+="<td width=65%>"
				        <%
				        if not oOfeGrupo.AtribsOfertados is nothing then
					        if not oOfeGrupo.AtribsOfertados.Item(cstr(oCosteDesc.PAID)) is nothing then
						        if oCosteDesc.obligatorio and isnull(oOfeGrupo.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor) then
							        sdivContenido = sdivContenido & "<span class=error>****"
                                    Response.Write "tr = tr + ""<span class=error>****"""
							        bFalta = true
									bFaltaElemento = true
						        else
							        vValor = oOfeGrupo.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor
                                    sdivContenido = sdivContenido & "<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt)
							        response.write "tr = tr + ""<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt) & """"
						        end if
					        else
						        if oCosteDesc.obligatorio  then
                                    sdivContenido = sdivContenido & "<span class=error>****"
							        Response.Write "tr = tr + ""<span class=error>****"""
							        bFalta = true
									bFaltaElemento = true
						        else
                                    sdivContenido = sdivContenido & "<span>&nbsp;"									
							        Response.Write "tr = tr + ""<span>&nbsp;"""
						        end if
					        end if
				        else
					        if oCosteDesc.obligatorio then
                                sdivContenido = sdivContenido & "<span class=error>****"
						        Response.Write "tr = tr + ""<span class=error>****"""
						        bFalta = true
								bFaltaElemento = true
					        else
                                sdivContenido = sdivContenido & "<span>&nbsp;"									
						        Response.Write "tr = tr + ""<span>&nbsp;"""
					        end if
				        end if
				        sdivContenido = sdivContenido & "</span></td></tr>" %>	
				        tr+="</span></td>"
			        tr+="</tr>"
					//document.write(tr)
					tr = tr.replace("width=30%","width=50%")
					tr = tr.replace("width=65%","width=45%")					
					<%if bFaltaElemento then %>
						if (!cabeceraPrinted)
						{
							document.getElementById("divObligatorio").innerHTML += cabecera
							cabeceraPrinted = true
						}
						if (!subCabeceraPrinted)
						{
							document.getElementById("divObligatorio").innerHTML += subCabecera
							subCabeceraPrinted = true
						}
						document.getElementById("divObligatorio").innerHTML += "<table width=94% style=\"margin-left:20pt;\">" + tr + "</table>"
					<%end if
                end if
		     next%>
        </script>
        <%end if
        sDivContenido = sDivContenido & "</table>"
end if%>
        <%
if oGrupo.items.count >0 then
	for each oItem in oGrupo.items
        sDivContenido = sDivContenido & "<table width=""100%"">"
        sDivContenido = sDivContenido & "<tr><td style=""font-size:1px;border-bottom:1 dashed black"">&nbsp;</td></tr>"
        sDivContenido = sDivContenido & "</table>"
        %>
        <script>
            var dash = ""

            dash += "<table width=100%>"
            dash += "	<tr>"
            dash += "		<td style=\"font-size:1px;border-bottom:1 dashed black\">"
            dash += "			&nbsp;"
            dash += "		</td>"
            dash += "	</tr>"
            dash += "</table>"
        </script>
        <% 
	'sDivContenido = sDivContenido & "<table width=""100%"">"
	'sDivContenido = sDivContenido & "<tr><td width=25% valign=top>"
        %>
        <script>
	subCabecera = ""		
	subCabecera+=dash
    
    subCabecera+="<table width=97% style=\"margin-left:10pt;\">"
	subCabecera+="<tr>"
	subCabecera+="<td width=25% valign=top>"
				
	subCabecera +="<span class=negrita>"
	subCabecera +="(<%=JSText(oGrupo.codigo)%>)&nbsp;"
	<%
	if isnull(oItem.ArticuloCod) then
		Response.Write "subCabecera +=""" & VB2HTML(oItem.Descr) & """"
	else
		Response.Write "subCabecera +=""" & JSText(oItem.ArticuloCod) & " - " & JSText(oItem.Descr) & """"
	end if
	%>
	subCabecera +="</span>"
	subCabecera +="<BR>"
	subCabecera +="</td></tr></table>"																				
	subCabeceraPrinted = false
        </script>
        <% 
    sDivContenido = sDivContenido & "<table width=97% style=""margin-left:10pt;"" border=0>"
	sDivContenido = sDivContenido & "<tr>"
	sDivContenido = sDivContenido & "<td width=45% valign=top>"
    
    sDivContenido = sDivContenido & "<span class=""negrita"">(" & oGrupo.codigo & ")&nbsp;"
    if isnull(oItem.ArticuloCod) then
		sDivContenido = sDivContenido & VB2HTML(oItem.Descr)
	else
	    sDivContenido = sDivContenido & oItem.ArticuloCod & " - " & oItem.Descr
	end if
    
    sDivContenido = sDivContenido & "</span><BR>"

    if oitem.anyoImputacion > 0 then
        sDivContenido = sDivContenido & "<span class=""negrita"">" & den(67) & "</span>:&nbsp;<span>" & oitem.anyoImputacion & "</span>"
    end if
    sDivContenido = sDivContenido & "<BR>"
	
    sDivContenido = sDivContenido & "<nobr><b>" & den(31) & "</b>&nbsp;" & visualizacionNumero(oItem.Cantidad,decimalfmt,thousandfmt,precisionfmt)
	sDivContenido = sDivContenido & "</nobr><BR>"
	if oProceso.SolicitarCantMax then
		sDivContenido = sDivContenido & "<nobr><b>Cant. Máx.</b>&nbsp;" & visualizacionNumero(oItem.precioItem.cantidad,decimalfmt,thousandfmt,precisionfmt) & "</nobr><BR>"
	end if
	sDivContenido = sDivContenido & "<nobr><b>" & den(49) & "</b>&nbsp;" & oItem.uniCod & " " &  oItem.uniDen & "</nobr></td>"
    sDivContenido = sDivContenido & "<td align=right><table width=""100%"">"
				
	if oProceso.SolicitarCantMax then
        if oGrupo.UsarEscalados=0 then
		    if not isnull(oItem.precioItem.PrecioOferta) then
		        sDivContenido = sDivContenido & "<tr><td width=""40%"">"
				sDivContenido = sDivContenido & "<nobr><b>" & den(30) & "</b>&nbsp;<span class=rellenado>" & visualizacionNumero(oItem.precioItem.PrecioOferta / oOferta.cambio * equiv,decimalfmt,thousandfmt,precisionfmt) & "</span></nobr></td>"
				if not isnull(oItem.Objetivo) and oProceso.objetivosPublicados then
				    sDivContenido = sDivContenido & "<td width=""30%""><nobr><b>" & den(36) & "</b>&nbsp;" & visualizacionNumero(oItem.Objetivo,decimalfmt,thousandfmt,precisionfmt) & "</nobr></td>"
				end if
			    sDivContenido = sDivContenido & "<td class=negrita><nobr>" & den(62) & "&nbsp;<span class=rellenado>" & visualizacionNumero(oItem.precioItem.ImporteNeto,decimalfmt,thousandfmt,precisionfmt) & "&nbsp;" & oOferta.codmon & "</span></nobr></td></tr>"
			else
				sDivContenido = sDivContenido & "<tr><td><span class=""error"" style=""font-weight:normal;font-size:10px;"">" & den(35) & "</span></td></tr>"
			end if					
        end if
    else
		if oGrupo.UsarEscalados=0 then
		    if not isnull(oItem.precioItem.PrecioOferta)  then
		        sDivContenido = sDivContenido & "<tr><td width=""40%""><nobr><b>" & den(30) & "</b>&nbsp;<span class=rellenado>" & visualizacionNumero(oItem.precioItem.PrecioOferta / oOferta.cambio * equiv,decimalfmt,thousandfmt,precisionfmt) & "</span></nobr></td>"
				if not isnull(oItem.Objetivo) and oProceso.objetivosPublicados then
			        sDivContenido = sDivContenido & "<td width=""30%""><nobr><b>" & den(36) & "</b>&nbsp;" & visualizacionNumero(oItem.Objetivo,decimalfmt,thousandfmt,precisionfmt) & "</nobr></td>"
                end if
			    sDivContenido = sDivContenido & "<td class=negrita><nobr>" & den(62) & "&nbsp;<span class=rellenado>" & visualizacionNumero(oItem.precioItem.ImporteNeto,decimalfmt,thousandfmt,precisionfmt) & "&nbsp;" & oOferta.codmon & "</span></nobr></td></tr>"
			else
				sDivContenido = sDivContenido & "<tr><td><span class=""error"" style=""font-weight:normal;font-size:10px;"">" & den(35) & "</span></td></tr>"
		    end if
		end if
	end if					
	sDivContenido = sDivContenido & "</table>"
	
	if oProceso.PedirAlterPrecios then
	    if oProceso.SolicitarCantMax then
			if not isnull(oItem.precioItem.Precio2) then
			    sDivContenido = sDivContenido & "<table width = ""100%"">"
				sDivContenido = sDivContenido & "<tr><td width=""40%""><nobr><b>" & den(33) & "</b>&nbsp;<span class=rellenado>" & visualizacionNumero(oItem.PrecioItem.Precio2 / oOferta.cambio * equiv,decimalfmt,thousandfmt,precisionfmt) & "</span></nobr></td>"
				if not isnull(oItem.Objetivo) and oProceso.objetivosPublicados then
					sDivContenido = sDivContenido & "<td width=""30%""><nobr>&nbsp;</nobr></td>"
				end if
				sDivContenido = sDivContenido & "<td class=negrita><nobr>" & den(62) & "&nbsp;<span class=rellenado>"
				if oItem.precioItem.cantidad <= oItem.cantidad And oItem.precioItem.cantidad > 0 then
					sDivContenido = sDivContenido & visualizacionNumero(oItem.PrecioItem.cantidad * (oItem.PrecioItem.Precio2 / oOferta.cambio * equiv),decimalfmt,thousandfmt,precisionfmt)
                else
					sDivContenido = sDivContenido & visualizacionNumero(oItem.cantidad * (oItem.PrecioItem.Precio2 / oOferta.cambio * equiv),decimalfmt,thousandfmt,precisionfmt)
				end if
			    sDivContenido = sDivContenido & "&nbsp;" & oOferta.codmon & "</span></nobr></td></tr></table>"
		    end if
		else
			if not isnull(oItem.precioItem.Precio2) then
				sDivContenido = sDivContenido & "<table width=""100%"">"
				sDivContenido = sDivContenido & "<tr><td width=""40%""><nobr><b>" & den(33) & "</b>&nbsp;<span class=rellenado>" & visualizacionNumero(oItem.PrecioItem.Precio2 / oOferta.cambio * equiv,decimalfmt,thousandfmt,precisionfmt) & "</span></nobr></td>"
				if not isnull(oItem.Objetivo) and oProceso.objetivosPublicados then
					sDivContenido = sDivContenido & "<td width=""30%""><nobr>&nbsp;</nobr></td>"
				end if
				sDivContenido = sDivContenido & "<td class=negrita><nobr>" & den(62) & "&nbsp;<span class=rellenado>" & visualizacionNumero(oItem.Cantidad * (oItem.PrecioItem.Precio2 / oOferta.cambio * equiv),decimalfmt,thousandfmt,precisionfmt) & "&nbsp;" & oOferta.codmon & "</span></nobr></td>"
				sDivContenido = sDivContenido & "</tr></table>"
			end if
		end if
				
		if oProceso.SolicitarCantMax then
			if not isnull(oItem.precioItem.Precio3) then
			    sDivContenido = sDivContenido & "<table width=""100%"">"
				sDivContenido = sDivContenido & "<tr><td width=""40%""><nobr><b>" & den(34) & "</b>&nbsp;<span class=rellenado>" & visualizacionNumero(oItem.PrecioItem.Precio3 / oOferta.cambio * equiv,decimalfmt,thousandfmt,precisionfmt) & "</span></nobr></td>"
				if not isnull(oItem.Objetivo) and oProceso.objetivosPublicados then
					sDivContenido = sDivContenido & "<td width=""30%"">&nbsp;</td>"
				end if
				sDivContenido = sDivContenido & "<td class=negrita><nobr>" & den(62) & "&nbsp;<span class=rellenado>"
				if oItem.precioItem.cantidad <= oItem.cantidad  And  oItem.precioItem.cantidad > 0 then
				    sDivContenido = sDivContenido & visualizacionNumero(oItem.PrecioItem.cantidad * (oItem.PrecioItem.Precio3 / oOferta.cambio * equiv),decimalfmt,thousandfmt,precisionfmt)
				else
					sDivContenido = sDivContenido & visualizacionNumero(oItem.Cantidad * (oItem.PrecioItem.Precio3 / oOferta.cambio * equiv),decimalfmt,thousandfmt,precisionfmt) 
			    end if
				sDivContenido = sDivContenido & "&nbsp;" & oOferta.codmon & "</span></nobr></td></tr></table>"
			end if
        else
			if not isnull(oItem.precioItem.Precio3) then
				sDivContenido = sDivContenido & "<table width=""100%"">"
				sDivContenido = sDivContenido & "<tr><td width=""40%""><nobr><b>" & den(34) & "</b>&nbsp;<span class=rellenado>" & visualizacionNumero(oItem.PrecioItem.Precio3 / oOferta.cambio * equiv,decimalfmt,thousandfmt,precisionfmt) & "</span></nobr></td>"
				if not isnull(oItem.Objetivo) and oProceso.objetivosPublicados then
					sDivContenido = sDivContenido & "<td width=""30%"">&nbsp;</td>"
				end if
				sDivContenido = sDivContenido & "<td class=negrita><nobr>" & den(62) & "&nbsp;<span class=rellenado>" & visualizacionNumero(oItem.Cantidad * (oItem.PrecioItem.Precio3 / oOferta.cambio * equiv),decimalfmt,thousandfmt,precisionfmt) & "&nbsp;" & oOferta.codmon & "</span></nobr></td>"
				sDivContenido = sDivContenido & "</tr></table>"
			end if
		end if
	end if
    sDivContenido = sDivContenido & "</td></tr>"
    
    if oGrupo.hayEscalados then
		oItem.cargarPresEscalados CiaComp
		Dim tablaEscalados
		tablaEscalados = ""
		Dim oEscalado
		for each oEscalado in oItem.escalados 
			if not isnull(oEscalado.precio) then
				tablaEscalados = tablaescalados & "<tr>"
				tablaEscalados = tablaescalados & "<td width=5% >&nbsp;</td>"
				tablaEscalados = tablaescalados & "<td align=right width=40% >"
				if isempty(oEscalado.final) then
					tablaEscalados = tablaescalados & oEscalado.inicial
				else
					if isnull(oEscalado.final) then
						tablaEscalados = tablaescalados & oEscalado.inicial
					else
						tablaEscalados = tablaescalados & oEscalado.inicial & " - " & oEscalado.final
				    end if
			    end if
			    tablaEscalados = tablaescalados & "</td>"
			    tablaEscalados = tablaescalados & "<td width=5% >&nbsp;</td>"
			    tablaEscalados = tablaescalados & "<td width=5% >&nbsp;</td>"
			    tablaEscalados = tablaescalados & "<td width=40% align=right>"
			    tablaEscalados = tablaescalados & oEscalado.precio
			    tablaEscalados = tablaescalados & "</td>"
			    tablaEscalados = tablaescalados & "<td width=5% >&nbsp;</td>"
			    tablaEscalados = tablaescalados & "</tr>"
		    end if					
		next
					
		if tablaescalados <> "" then
            sDivContenido = sDivContenido & "<tr><td>"
            sDivContenido = sDivContenido & "<table cellpadding=0 cellspacing=0>"
			sDivContenido = sDivContenido & "<tr><td colspan=6 align=""left"" valign=top><b>" & den(58) & "&nbsp;&nbsp;&nbsp;</b></td>"
			sDivContenido = sDivContenido & "<td><table cellspacing=0 cellpadding=0>"
			sDivContenido = sDivContenido & "<tr><td nowrap colspan=3 align=""center"">&nbsp;&nbsp;&nbsp;<u>" & den(59) & "</u>&nbsp;&nbsp;&nbsp;</td>"
			sDivContenido = sDivContenido & "<td nowrap colspan=3 align=""center"">&nbsp;&nbsp;&nbsp;<u>" & den(60) & "</u>&nbsp;&nbsp;&nbsp;</td></tr>"
			sDivContenido = sDivContenido & tablaEscalados
			sDivContenido = sDivContenido & "</table>"
			sDivContenido = sDivContenido & "</td></tr></table>"
			sDivContenido = sDivContenido & "</td></tr>"
		end if
    end if
    
    sDivContenido = sDivContenido & "</table>"
    sDivContenido = sDivContenido & "<table><tr>"
	sDivContenido = sDivContenido & "<td width=""20%"">&nbsp;</td>"
	sDivContenido = sDivContenido & "<td><table>"
	
    if not isnull(oItem.PrecioItem.Comentario1) and oItem.PrecioItem.Comentario1<>"" then
	    sDivContenido = sDivContenido & "<tr><td class=negrita>" & den(32) & "</td>"
		sDivContenido = sDivContenido & "<td><span class=rellenado>" & VB2HTML(oItem.PrecioItem.Comentario1) & "</span></td></tr>"
    end if
	
    if not isnull(oItem.PrecioItem.Comentario2) and oItem.PrecioItem.Comentario2<>"" then
		sDivContenido = sDivContenido & "<tr><td class=negrita>" & den(32) & "2</td>"
        sDivContenido = sDivContenido & "<td><span class=rellenado>" & VB2HTML(oItem.PrecioItem.Comentario2) & "</span></td></tr>"
	end if

	if not isnull(oItem.PrecioItem.Comentario3) and oItem.PrecioItem.Comentario3<>"" then
		sDivContenido = sDivContenido & "<tr><td class=negrita>" & den(32) & "3</td>"
        sDivContenido = sDivContenido & "<td><span class=rellenado>" & VB2HTML(oItem.PrecioItem.Comentario3) & "</span></td></tr>"
	end if
	sDivContenido = sDivContenido & "</table>"
	sDivContenido = sDivContenido & "</tr></table>"

	if oProceso.DefDestino=3 or (oProceso.DefDestino=2 and oGrupo.DefDestino=false) then
	    sDivContenido = sDivContenido & "<table width=""100%"">"
		sDivContenido = sDivContenido & "<tr><td width=10% class=negrita>" & den(11) & "</td>"
		sDivContenido = sDivContenido & "<td width=""15%"">" & oDestinos.item(oItem.destCod).cod & "&nbsp;" & oDestinos.item(oItem.destCod).den & "</td>"
		sDivContenido = sDivContenido & "<td width=""75%"">" & oDestinos.item(oItem.destCod).dir & "&nbsp;" & oDestinos.item(oItem.destCod).cp & "&nbsp;" & oDestinos.item(oItem.destCod).pob & "&nbsp;" & oDestinos.item(oItem.destCod).provi & "&nbsp;" & oDestinos.item(oItem.destCod).pai & "&nbsp;"
		sDivContenido = sDivContenido & "</td></tr></table>"
	end if

	if oProceso.defAdjunItem then
		if not isnull(oItem.PrecioItem.ObsAdjuntos) and oItem.PrecioItem.ObsAdjuntos<>"" then
			sDivContenido = sDivContenido & "<table width=""100%"">"
			sDivContenido = sDivContenido & "<tr><td class=negrita>" & den(21) & "</td></tr>"
			sDivContenido = sDivContenido & "<tr><td><span class=rellenado>" & VB2HTML(oItem.PrecioItem.ObsAdjuntos) & "</span></td></tr></table>"
		end if
		
        set oPrecioItem = oItem.PrecioItem
		if  not oPrecioItem.adjuntos is nothing then
			if oPrecioItem.adjuntos.count>0 then
				sDivContenido = sDivContenido & "<table width=""100%"">"
				sDivContenido = sDivContenido & "<tr><td colspan=2 class=negrita>" & den(22) & "</td></tr>"
					
				for each oAdjun in oPrecioItem.adjuntos
					sDivContenido = sDivContenido & "<tr><td width=25% valign=top><span class=rellenado>" & oAdjun.nombre & "</span></td>"
					sDivContenido = sDivContenido & "<td width=75% valign=top><span class=rellenado>" & VB2HTML(oAdjun.comentario) & "</span></td></tr>"
				next
			
            	sDivContenido = sDivContenido & "</table>"
			end if
		end if
    end if

	noAtributos = false
	if oGrupo.AtributosItem is nothing then
		noAtributos = true
	else
		if oGrupo.AtributosItem.count=0 then
			noAtributos = true
		end if
	end if

	if not noAtributos and not (Isnull(oItem.precioItem.PrecioOferta)) then
        sDivContenido = sDivContenido & "<table width=97% style=""margin-left:10pt;"">"
		sDivContenido = sDivContenido & "<tr><td class=negrita>" & JSText(den(23)) & "</td></tr>"
        sDivContenido = sDivContenido & "</table>"			
        %>
        <script>
            var cab3 = ""
            var cab3Printed = false
            cab3 += "<table width=97% style=\"margin-left:10pt;\">"
            cab3 += "	<tr>"
            cab3 += "		<td class=negrita><%=JSText(den(57))%></td>"
            cab3 += "	</tr>"
            cab3 += "</table>"			
        </script>
        <%	sDivContenido = sDivContenido & "<table width=""100%"">"		%>
        <script>
		<%for each oAtrib in oGrupo.AtributosItem
		    bFaltaElemento = false           %>
			var tr = ""
		    <%Set oAtribOfertado = oItem.PrecioItem.AtributosOfertado.Item(CStr(oAtrib.PAID)) 
            sDivContenido = sDivContenido & "<tr>" %>
				tr+="<tr>"
					<%if oAtrib.obligatorio then
                        sDivContenido = sDivContenido & "<td nowrap width=5% class=negrita>(*)&nbsp;" & JSText(oAtrib.Cod) & "</td>"
                        sDivContenido = sDivContenido & "<td width=30% class=negrita>" & JSText(oAtrib.den) & "</td>"
                        %>
						tr+="<td nowrap width=5% class=negrita>(*)&nbsp;<%=JSText(oAtrib.Cod)%></td>"
						tr+="<td width=30% class=negrita><%=JSText(oAtrib.den)%></td>"
					<%else
                        sDivContenido = sDivContenido & "<td nowrap width=""5%"">" & JSText(oAtrib.Cod) & "</td>"
                        sDivContenido = sDivContenido & "<td width=""30%"">" & JSText(oAtrib.den) & "</td>"
                        %>
						tr+="<td nowrap width=5%><%=JSText(oAtrib.Cod)%></td>"
						tr+="<td width=30%><%=JSText(oAtrib.den)%></td>"
					<%end if
                    sDivContenido = sDivContenido & "<td width=""65%"">" %>
					tr+="<td width=65%>"
					<%
					if oAtrib.obligatorio and isnull(oAtribOfertado.valor) then
                        sdivContenido = sdivContenido & "<span class=error>****"
						Response.Write "tr+=""<span class=error>****"""
						bFalta = true
						bFaltaElemento = true
					else
						select case oAtrib.Tipo
							case 1
                                sdivContenido = sdivContenido & "<span class=rellenado>" & VB2HTML(oAtribOfertado.Valor)
								response.write "tr+=""<span class=rellenado>" & VB2HTML(oAtribOfertado.Valor) & """"
							case 2
								if (oAtrib.operacion = "+" or  oAtrib.operacion = "-") then
									vValor = oAtribOfertado.valor / oOferta.cambio * equiv
								else
									vValor = oAtribOfertado.valor
								end if
                                sdivContenido = sdivContenido & "<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt)
								response.write "tr+=""<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt) & """"
							case 3
                                sdivContenido = sdivContenido & "<span class=rellenado>" & visualizacionFecha(oAtribOfertado.Valor,datefmt)
								response.write "tr+=""<span class=rellenado>" & visualizacionFecha(oAtribOfertado.Valor,datefmt) & """"
							case 4
								if oAtribOfertado.Valor then
                                    sdivContenido = sdivContenido & "<span class=rellenado>" & JSText(den(27))
									Response.write  "tr+=""<span class=rellenado>" & JSText(den(27)) & """"
                                else
                                    if isnull(oAtribOfertado.valor) then
                                        sdivContenido = sdivContenido & "<span class=rellenado>" & ""
									    Response.write  "tr+=""<span class=rellenado>" & "" & """"
								    else
                                        sdivContenido = sdivContenido & "<span class=rellenado>" & JSText(den(28))
									    Response.write  "tr+=""<span class=rellenado>" & JSText(den(28)) & """"
    								end if
                                end if
						end select
					end if
				sdivContenido = sdivContenido & "</span></td></tr>" %>
				tr+="</span></td>"
				tr+="</tr>"
				//document.write(tr)
				tr = tr.replace("width=30%","width=50%")
				tr = tr.replace("width=65%","width=45%")				
				<%if bFaltaElemento then %>
					if (!cabeceraPrinted)
					{
						document.getElementById("divObligatorio").innerHTML += cabecera
						cabeceraPrinted = true
					}
					if (!subCabeceraPrinted)
					{
						document.getElementById("divObligatorio").innerHTML += subCabecera
						subCabeceraPrinted = true
					}
					if (!cab3Printed)
					{
						document.getElementById("divObligatorio").innerHTML += cab3
						cab3Printed = true
					}
					
					document.getElementById("divObligatorio").innerHTML += "<table width=94% style=\"margin-left:20pt;\">" + tr + "</table>"
				<%end if						
			next%>
        </script>
        <% sDivContenido = sDivContenido & "</table>"
	end if%>
        <%
'''''''''Tratamiento para los costes/descuentos de Item: Total Item y Precio Item'''''''''
'Comprobamos si hay costes/descuentos de Item que tratar
noCostesDescuentosItem = false
if  oItem.PrecioItem.CostesDescOfertados is nothing then
	noCostesDescuentosItem = true
else
    if  oItem.PrecioItem.CostesDescOfertados.count=0 then
		noCostesDescuentosItem = true
	end if
end if

noCostesDescuentosTotalItem = false
if  oProceso.CostesDescTotalItem is nothing then
	noCostesDescuentosTotalItem = true
else
    if  oProceso.CostesDescTotalItem.count=0 then
		noCostesDescuentosTotalItem = true
	end if
end if

noCostesDescuentosPrecItem = false
if oProceso.CostesDescPrecItem is nothing then
	noCostesDescuentosPrecItem = true
else
    if  oProceso.CostesDescPrecItem.count=0 then
		noCostesDescuentosPrecItem = true
	end if
end if
        %>
        <%
PrimeraVez=True

if not noCostesDescuentosItem and  (not (Isnull(oItem.precioItem.PrecioOferta)) or tablaescalados<> "")   then
    if not noCostesDescuentosPrecItem then%>
        <script>
		
	<%for each oCosteDescOfertado in oItem.PrecioItem.CostesDescOfertados
        bfaltaElemento = false%>
		var tr = ""
		<%Set oCosteDesc = oProceso.CostesDescPrecItem.Item(CStr(oCosteDescOfertado.Atributo.PAID))
		If Not oCosteDesc Is Nothing Then
			If PrimeraVez Then
                sDivContenido = sDivContenido & "<table width=97% style=""margin-left:10pt;"">"
                sDivContenido = sDivContenido & "<tr><td class=negrita>" & Replace(JSText(den(53)),"\","") & "</td></tr>"
                sDivContenido = sDivContenido & "</table>"
                sDivContenido = sDivContenido & "<table width=""100%"">"
            %>

			subCabecera = ""		
			subCabecera +="<table width=97%  style=\"margin-left:10pt;\">"
			subCabecera +="    <tr>"
			subCabecera +="        <td class=negrita><%=Replace(JSText(den(53)),"\","")%></td>" <!-- Costes/descuentos adicionales del ítem: Precio del ítem-->
			subCabecera +="    </tr>"
			subCabecera +="</table>"
			subCabeceraPrinted = false
		
			//document.write (subCabecera)
			//document.write("<table width=100%>")
					
			<%PrimeraVez=false
			End If 
            sDivContenido = sDivContenido & "<tr>"    %>
		    tr+="<tr>"
			<%if oCosteDesc.obligatorio then
                sDivContenido = sDivContenido & "<td valign=top nowrap width=5% class=negrita>(*)&nbsp;" & JSText(oCosteDesc.Cod) & "</td>"
                sDivContenido = sDivContenido & "<td valign=top width=30% class=negrita>" & JSText(oCosteDesc.den) & "</td>"
                %>
				tr+="<td valign=top nowrap width=5% class=negrita>(*)&nbsp;<%=JSText(oCosteDesc.Cod)%></td>"
				tr+="<td valign=top width=30% class=negrita><%=JSText(oCosteDesc.den)%></td>"
			<%else
                sDivContenido = sDivContenido & "<td valign=top nowrap width=""5%"">" & JSText(oCosteDesc.Cod) & "</td>"
                sDivContenido = sDivContenido & "<td valign=top width=""30%"">" & JSText(oCosteDesc.den) & "</td>"
                %>
				tr+="<td valign=top nowrap width=5%><%=JSText(oCosteDesc.Cod)%></td>"
				tr+="<td valign=top width=30%><%=JSText(oCosteDesc.den)%></td>"
			<%end if
            sDivContenido = sDivContenido & "<td valign=top width=""15%"">" %>
			tr+="<td valign=top width=15%>"
			<%
				if oCosteDesc.obligatorio and isnull(oCosteDescOfertado.valor) and not oGrupo.HayEscalados then
					sdivContenido = sdivContenido & "<span class=error>****"
                    Response.Write "tr+=""<span class=error>****"""
					bFalta = true
					bFaltaElemento = true
				else
					if (oCosteDesc.operacion = "+" or  oCosteDesc.operacion = "-") then
						vValor = oCosteDescOfertado.valor  / oOfeGrupo.cambio * equiv
					else
						vValor = oCosteDescOfertado.valor
					end if
                    sdivContenido = sdivContenido & "<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt)
					response.write "tr+=""<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt) & """"
				end if
			sdivContenido = sdivContenido & "</span></td>" %>
			tr+="</span></td>"
	
		<%if oGrupo.hayEscalados then
			if oCosteDescOfertado.hayEscalados then %>
				var traux = ""
				<% 
                sdivContenido = sdivContenido & "<td width=1% nowrap valign=top>" & den(58) & "</td><td align=left colspan=3><table border=0 cellspacing=0 celpadding=0>"
				sdivContenido = sdivContenido & "<tr><td align=right>&nbsp;&nbsp;&nbsp;<u>" & den(59) & "</u>&nbsp;&nbsp;&nbsp;</td>"
				sdivContenido = sdivContenido & "<td align=right>&nbsp;&nbsp;&nbsp;<u>" & den(61) & "</u>&nbsp;&nbsp;&nbsp;</td></tr>"

                for each oEscalado in oCosteDescOfertado.escalados 
                    sdivContenido = sdivContenido & "<tr><td nowrap align=right>&nbsp;&nbsp;&nbsp;"
                    if isnull(oEscalado.final) then 
                        sdivContenido = sdivContenido & oEscalado.inicial 
                    else 
                        sdivContenido = sdivContenido & oEscalado.inicial & " - " & oEscalado.final & "&nbsp;&nbsp;&nbsp;</td>"
                    end if
                    sdivContenido = sdivContenido & "<td nowrap align=right>&nbsp;&nbsp;&nbsp;" & oEscalado.valorNum & "&nbsp;&nbsp;&nbsp;</td></tr>"
                    %>
					traux+=	"<tr><td nowrap align=right>&nbsp;&nbsp;&nbsp;<%if isnull(oEscalado.final) then response.write oEscalado.inicial else response.write oEscalado.inicial & " - " & oEscalado.final%>&nbsp;&nbsp;&nbsp;</td>"
					traux+=	"<td nowrap align=right>&nbsp;&nbsp;&nbsp;<%=oEscalado.valorNum%>&nbsp;&nbsp;&nbsp;</td></tr>"
				<%next
                sdivContenido = sdivContenido & "</td></tr></table></td>"
                %>
				
				if (traux!="")
				{
					tr+= "<td width=1% nowrap valign=top ><%=den(58)%> </td><td align=left colspan=3><table border=0 cellspacing=0 celpadding=0>"
					tr+= "<tr><td align=right>&nbsp;&nbsp;&nbsp;<u><%=den(59)%></u>&nbsp;&nbsp;&nbsp;</td>"
					tr+= "<td align=right>&nbsp;&nbsp;&nbsp;<u><%=den(61)%></u>&nbsp;&nbsp;&nbsp;</td></tr>"
					tr+= traux
					tr+= "</td></tr></table></td>"
				}
				
			<%end if
		else
            sdivContenido = sdivContenido & "<td width=""50%"">&nbsp;</td>" %>
			tr+="<td width=50%>&nbsp;</td>"
		<%end if
        sdivContenido = sdivContenido & "</tr>" %>
		tr+="</tr>"
		
		//	document.write(tr)
		tr = tr.replace("width=30%","width=50%")
		tr = tr.replace("width=65%","width=45%")
			
			<%if bFaltaElemento then %>
				if (!cabeceraPrinted)
				{
					document.getElementById("divObligatorio").innerHTML += cabecera
					cabeceraPrinted = true
				}
				if (!subCabeceraPrinted)
				{
					document.getElementById("divObligatorio").innerHTML += subCabecera
					subCabeceraPrinted = true
				}
				document.getElementById("divObligatorio").innerHTML += "<table width=94% style=\"margin-left:20pt;\">" + tr + "</table>"
			<%end if				
		End If 
	next%>
        </script>
        <%end if %>
        <%If Not PrimeraVez Then
	    sDivContenido = sDivContenido & "</table>"
	End If
    
	PrimeraVez=True
        
	if not noCostesDescuentosTotalItem then%>
        <script>
		<%for each oCosteDescOfertado in oItem.PrecioItem.CostesDescOfertados
			bFaltaElemento = false
			Set oCosteDesc = oProceso.CostesDescTotalItem.Item(CStr(oCosteDescOfertado.Atributo.PAID))%>	
			var tr=""
			<%If Not oCosteDesc Is Nothing Then
				If PrimeraVez Then
                    sdivContenido = sdivContenido & "<table width=97% style=""margin-left:10pt;"">"
                    sdivContenido = sdivContenido & "<tr><td class=negrita>" & Replace(JSText(den(52)),"\","") & "</td></tr>" 
                    sdivContenido = sdivContenido & "</table>"
                    sdivContenido = sdivContenido & "<table width=100% border=0>"
                   %>
					
				subCabecera = ""		
				subCabecera +="<table width=97% style=\"margin-left:10pt;\">"
				subCabecera +="    <tr>"
				subCabecera +="        <td class=negrita><%=Replace(JSText(den(52)),"\","")%></td>" <!-- Costes/descuentos adicionales del ítem: Total del ítem-->
				subCabecera +="    </tr>"
				subCabecera +="</table>"
				subCabeceraPrinted = false
			
				//document.write (subCabecera)
				//document.write("<table width=100% border=0>")

				<%PrimeraVez = False
				End If 
                sdivContenido = sdivContenido & "<tr>" %>
			    tr+="<tr>"
				<%if oCosteDesc.obligatorio then
                    sdivContenido = sdivContenido & "<td valign=top nowrap width=5% class=negrita>(*)&nbsp;" & JSText(oCosteDesc.Cod) & "</td>"
                    sdivContenido = sdivContenido & "<td valign=top width=30% class=negrita>" & JSText(oCosteDesc.den) & "</td>"
                    %>
					tr+="<td valign=top nowrap width=5% class=negrita>(*)&nbsp;<%=JSText(oCosteDesc.Cod)%></td>"
					tr+="<td valign=top width=30% class=negrita><%=JSText(oCosteDesc.den)%></td>"
				<%else
                    sdivContenido = sdivContenido & "<td valign=top nowrap width=""5%"">" & JSText(oCosteDesc.Cod) & "</td>"
                    sdivContenido = sdivContenido & "<td valign=top width=""30%"">" & JSText(oCosteDesc.den) & "</td>"
                    %>
					tr+="<td valign=top nowrap width=5%><%=JSText(oCosteDesc.Cod)%></td>"
					tr+="<td valign=top width=30%><%=JSText(oCosteDesc.den)%></td>"
				<%end if
                sdivContenido = sdivContenido & "<td valign=top width=""15%"">" %>
				tr+="<td valign=top width=15%>"
				<%
					if oCosteDesc.obligatorio and isnull(oCosteDescOfertado.valor) and not oGrupo.HayEscalados then
						sdivContenido = sdivContenido & "<span class=error>****"
                        Response.Write "tr+=""<span class=error>****"""
						bFalta = true
						bFaltaElemento = true
					else
						if (oCosteDesc.operacion = "+" or  oCosteDesc.operacion = "-") then
							vValor = oCosteDescOfertado.valor  / oOfeGrupo.cambio * equiv
						else
							vValor = oCosteDescOfertado.valor
						end if
                        sdivContenido = sdivContenido & "<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt)
						response.write "tr+=""<span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt) & """"
					end if
				sdivContenido = sdivContenido & "</span></td>" %>
				tr+="</span></td>"
			<%if oGrupo.hayEscalados then
				if oCosteDescOfertado.hayEscalados then 
                	sdivContenido = sdivContenido & "<td width=1% nowrap valign=top>Escalados: </td><td align=left colspan=3><table border=0 cellspacing=0 celpadding=0>"
					sdivContenido = sdivContenido & "<tr><td align=right>&nbsp;&nbsp;&nbsp;<u>Cantidad</u>&nbsp;&nbsp;&nbsp;</td>"
					sdivContenido = sdivContenido & "<td align=right>&nbsp;&nbsp;&nbsp;<u>Valor</u>&nbsp;&nbsp;&nbsp;</td></tr>"
                    %>
					var traux = ""
					<% for each oEscalado in oCosteDescOfertado.escalados 
                        sdivContenido = sdivContenido & "<tr><td nowrap align=right>&nbsp;&nbsp;&nbsp;"
                        if isnull(oEscalado.final) then 
                            sdivContenido = sdivContenido & oEscalado.inicial 
                        else 
                            sdivContenido = sdivContenido & oEscalado.inicial & " - " & oEscalado.final
                        end if
                        sdivContenido = sdivContenido & "&nbsp;&nbsp;&nbsp;</td>"
                        sdivContenido = sdivContenido & "<td nowrap align=right>&nbsp;&nbsp;&nbsp;" & oEscalado.valorNum & "&nbsp;&nbsp;&nbsp;</td></tr>"
                        %>
						traux+=	"<tr><td nowrap align=right>&nbsp;&nbsp;&nbsp;<%if isnull(oEscalado.final) then response.write oEscalado.inicial else response.write oEscalado.inicial & " - " & oEscalado.final%>&nbsp;&nbsp;&nbsp;</td>"
						traux+=	"<td nowrap align=right>&nbsp;&nbsp;&nbsp;<%=oEscalado.valorNum%>&nbsp;&nbsp;&nbsp;</td></tr>"
					<%next
                    sdivContenido = sdivContenido & "</td></tr></table></td>"
                    %>
					
					if (traux!="")
					{
						tr+= "<td width=1% nowrap valign=top >Escalados: </td><td align=left colspan=3><table border=0 cellspacing=0 celpadding=0>"
						tr+= "<tr><td align=right>&nbsp;&nbsp;&nbsp;<u>Cantidad</u>&nbsp;&nbsp;&nbsp;</td>"
						tr+= "<td align=right>&nbsp;&nbsp;&nbsp;<u>Valor</u>&nbsp;&nbsp;&nbsp;</td></tr>"
						tr+= traux
						tr+= "</td></tr></table></td>"
					}
					
				<%end if
			else
                sdivContenido = sdivContenido & "<td width=""50%"">&nbsp;</td>" %>
				tr+="<td width=50%>&nbsp;</td>"
			<%end if
            sdivContenido = sdivContenido & "</tr>" %>
			tr+="</tr>"
		
			//	document.write(tr)
			tr = tr.replace("width=30%","width=50%")
			tr = tr.replace("width=65%","width=45%")
				
			<%if bFaltaElemento then %>
					if (!cabeceraPrinted)
					{
						document.getElementById("divObligatorio").innerHTML += cabecera
						cabeceraPrinted = true
					}
					if (!subCabeceraPrinted)
					{
						document.getElementById("divObligatorio").innerHTML += subCabecera
						subCabeceraPrinted = true
					}
					document.getElementById("divObligatorio").innerHTML += "<table width=94% style=\"margin-left:20pt;\">" + tr + "</table>"
				<%end if
			End If
		next%>
        </script>
        <%end if %>
        <%If Not PrimeraVez Then
	    sDivContenido = sDivContenido & "</table>"
	End If 
end if

next

sDivContenido = sDivContenido & "<table width=""100%"">"
sDivContenido = sDivContenido & "<tr><td style=""font-size:1px;border-bottom:1 dashed black"">&nbsp;</td></tr>"
sDivContenido = sDivContenido & "</table>"

end if
next
end if

if not bFalta then%>
        <div name="divConfirmarOK" id="divConfirmarOK" style="visibility: hidden; height: 0;
            clip: rect(0 0 0 0);">
            <table width="100%">
                <tr>
                    <td class="error" width="80%">
                        <%=den(39)%>
                    </td>
                    <td colspan="2">
                        <table width="100%">
                            <tr>
                                <td align="center">
                                    <input type="button" class="button" style="border-width: 0; cursor: pointer;" onclick="Confirmar()"
                                        value="<%=den(43)%>" id="cmdConfirmar" name="cmdConfirmar">
                                </td>
                                <td align="center">
                                    <input type="button" class="button" style="border-width: 0; cursor: pointer;" onclick="window.parent.close()"
                                        value="<%=den(44)%>" id="cmdCerrar" name="cmdCerrar">
                                </td>
                                <td align="center">
                                    <input type="button" class="button" style="border-width: 0; cursor: pointer;" onclick="window.print()"
                                        value="<%=den(48)%>" id="cmdImprimir" name="cmdImprimir">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <%=den(40)%>&nbsp;<span class="rellenado" style="color: black; font-size: 10px; font-weight: normal">(<%=den(41)%>)</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <%=den(42)%>
                    </td>
                </tr>
                <%
	if oProceso.adminpublica then
		if iif(oProceso.MaximoNumeroOfertas<>0,oProceso.MaximoNumeroOfertas = oOferta.TotEnviadas + 1 ,true) then%>
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <%=den(45)%>
                    </td>
                </tr>
                <%end if
	end if%>
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
        <%else%>
        <div name="divError" id="divError" style="visibility: hidden; height: 0; clip: rect(0 0 0 0);">
            <table width="100%">
                <tr>
                    <td class="error" width="80%">
                        <%=den(40)%>&nbsp;
                    </td>
                    <td colspan="2">
                        <table width="100%">
                            <tr>
                                <td align="center">
                                    <input type="button" class="button" style="border-width: 0;" onclick="window.parent.close()"
                                        value="<%=den(64)%>" id="cmdCerrar" name="cmdCerrar">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="error" colspan="3" align="center">
                        <font size="3">
                            <%=den(56)%></font>
                    </td>
                </tr>
            </table>
        </div>
        <%end if%>
        <script>
<%if bFalta then%>
	document.getElementById("divConfirmacion").innerHTML=document.getElementById("divError").innerHTML
	document.getElementById("divError").outerHTML=""
	document.getElementById("divTotalOferta").outerHTML = ""
<%else%>
	document.getElementById("divConfirmacion").innerHTML=document.getElementById("divConfirmarOK").innerHTML
	document.getElementById("divConfirmarOK").outerHTML=""

    s="				<nobr>"
    s+="					<%=JSText(den(1))%>&nbsp;<%=visualizacionNumero(oOferta.ImporteTotal,decimalfmt,thousandfmt,precisionfmt)%>&nbsp;<%=JSText(oOferta.codmon)%>"
    s+="				</nobr>"

    document.getElementById("divTotalOferta").innerHTML = s
    <%
    dim sImporte
    sImporte = "<nobr>" & JSText(den(1)) & "&nbsp;" & visualizacionNumero(oOferta.ImporteTotal,decimalfmt,thousandfmt,precisionfmt) & "&nbsp;" & JSText(oOferta.codmon) & "</nobr>"
    sCabeceraOferta = Replace(sCabeceraOferta,"<div name=divTotalOferta id=divTotalOferta></div>","<div name=divTotalOferta id=divTotalOferta>" & sImporte & "</div>")
end if%>
        </script>
        <%=sDivContenido%>
    </div>
</body>
<%
dim nombrehtml
dim nombrePdf

'Se crean los nombres de los ficheros html y pdf
max=32767
min=1
Randomize
aleatorio=CStr(Int((max-min+1)*Rnd+min))

nombrehtml = anyoproceso & "_" & gmn1proceso & "_" & codproceso & "_" & CodProve & "_" & NumOfe & "_" & aleatorio & ".html"
nombrePdf =  anyoproceso & "_" & gmn1proceso & "_" & codproceso & "_" & CodProve & "_" & NumOfe & "_" & aleatorio & ".pdf"

rutHtml = ""
rutPdf = ""

Set oFsoLocal = Server.CreateObject("Scripting.FileSystemObject")
Set oStream= Server.CreateObject("ADODB.Stream")
	
'Se graba el fichero html con el contenido de la página
oStream.Open
oStream.Charset="UTF-8" 
oStream.writeText(sEstilos & sCabeceraOferta & sMasDatosOferta & sDivContenido)

rutHtml= Application("CARPETAUPLOADS") & "\" & nombrehtml
if ((salvarFichero(oStream,rutHtml))=-1) Then
	rutHtml = ""
End if

oStream.Close		

'Se graba el fichero pdf sin contenido
oStream.Open

rutPdf= Application("CARPETAUPLOADS") & "\" & nombrePdf
if ((salvarFichero(oStream,rutPdf))=-1) Then
	rutPdf = ""
End if
oStream.Close	

%>
<script>
    document.getElementById("hRutHtml").value = "<%=nombrehtml%>"
    document.getElementById("hRutPdf").value = "<%=nombrePdf%>"
</script>
<%
server.ScriptTimeout = 	OldTimeOut
	
set oProceso.grupos = nothing
set oProceso.atributos = nothing
set oProceso.adjuntos = nothing
set oProceso = nothing
set oOferta = nothing
set oRaiz = nothing
%>
<%Sub GuardarFSAL2() %>
<!--#include file="../common/fsal_2.asp"-->
<%End Sub %>
<%GuardarFSAL2 %>
</html>
<script>
<%if bFalta then %>
    document.getElementById("divContenido").innerHTML = document.getElementById("divObligatorio").innerHTML
<%else%>
    document.getElementById("divContenido").style.display = "block"
<%end if%>
</script>
