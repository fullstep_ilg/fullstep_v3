﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/colores.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/fsal_1.asp"-->
<HTML>
<%
''' <summary>
''' Exporta a excel la oferta
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cumpoferta.asp ; Tiempo máximo: 0,2</remarks>


Public Function ValidFilename(ByVal sFileName ) 
Dim s 

s = sFileName

s = Replace(s, "\", "_")
s = Replace(s, "/", "_")
s = Replace(s, ":", "_")
s = Replace(s, "*", "_")
s = Replace(s, "?", "_")
s = Replace(s, """", "_")
s = Replace(s, ">", "_")
s = Replace(s, "<", "_")
s = Replace(s, "|", "_")

ValidFilename = s


End Function


Idioma = Request.Cookies("USU_IDIOMA")
Idioma = trim(Idioma)


set oRaiz=validarUsuario(Idioma,true,false,0)
		
anyo = request.Cookies ("PROCE_ANYO")
gmn1 = request.Cookies ("PROCE_GMN1")
proce = request.Cookies ("PROCE_COD")

dim proveCia
proveCia = oRaiz.Sesion.CiaCodGs
if Application("PYME") <> "" then
	If InStr(1, proveCia, Application("PYME")) = 1 Then
		prove = Replace(proveCia, Application("PYME"), "", 1, 1)
	else
		prove = proveCia
	End If
else
	prove = proveCia
end if

dim den

den = devolverTextos(Idioma, 87)


%>


<%
Set UploadProgress = Server.CreateObject("Persits.UploadProgress")
PID = "PID=" & UploadProgress.CreateProgressID()
barref = Application("RUTASEGURA") & "script/solicitudesoferta/framebar.asp?to=10&" & PID
%> 

<HEAD>

<title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<script src="../common/ajax.js"></script>
<script src="../common/formatos.js"></script>
<!--#include file="../common/fsal_3.asp"-->
<!--#include file="../common/fsal_5.asp"-->
<SCRIPT LANGUAGE="JavaScript">
    
function init() { 
    if (<%=application("FSAL")%> == 1)
    {
    Ajax_FSALActualizar3(); //registro de acceso
    }
}
function ShowProgress()
{
  strAppVersion = navigator.appVersion;
  if (document.frmExcel.btnCarga.value != "")
  {
    if (strAppVersion.indexOf('MSIE') != -1 && strAppVersion.substr(strAppVersion.indexOf('MSIE')+5,1) > 4)
    {
      winstyle = "dialogWidth=375px; dialogHeight:130px; center:yes";
      window.showModelessDialog('<% = barref %>&b=IE',null,winstyle);
    }
    else
    {
      window.open('<% = barref %>&b=NN','','width=370,height=115', true);
    }
  }
  return true;
}
</SCRIPT> 


</HEAD>

<SCRIPT>
/*
''' <summary>
''' Iniciar la descarga
''' </summary>   
''' <param name="bConfirm">true</param>
''' <remarks>Llamada desde: cmddescargar ; Tiempo maximo: 0</remarks>*/
function descargar(bConfirm)
	{
	var re
	var re2
	re=/MSIE 5.5/
	re2=/Q299618/
	
	re3 = /SP1/

	if (navigator.userAgent.search(re)!=-1 && navigator.appMinorVersion.search(re2)==-1 && navigator.appMinorVersion.search(re3)!=-1)
		{
		window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/mspatch.asp","_blank","top=100,left=150,width=300,height=250,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
		window.close()
		}
	else
		{

		window.opener.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/avisoguardar.asp?target=excel","_blank","top=60,left=20,width=550,height=330,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no")
		window.close()
		}
	}
	
function Validar()
	{
	if (<%=application("FSAL")%> == 1)
	{
	fechaIniExcelServer=devolverUTCFechaFSAL();  //se registra la fecha FICLIPREPAR al inicio de la ejecución
    sIdRegistroExcelServer=devolverIdRegistroFSAL();
    var f;
    f = document.forms["frmExcel"];
    f.action = f.action + '&fechaIniFSAL7=' + fechaIniExcelServer + '&sIdRegistroFSAL7=' + sIdRegistroExcelServer;
	//excelserver.asp
	} 
	var sName
	
	
	sName=document.forms["frmExcel"].btnCarga.value.split("\\")

	sNameFichero = sName[sName.length-1]
	
	
	sExt=sName[sName.length-1].split(".")
	
	sExtFichero = sExt[sExt.length-1]
	bContinuar = true
	
	if (sExtFichero!="xls") 
		{
		bContinuar=confirm("<%=den(6)%>")
		}
	if (bContinuar && sNameFichero!="<%=ValidFilename(anyo & "_" & gmn1 & "_" & proce & "_" & prove & ".xls")%>")
		{
		bContinuar=confirm("<%=den(7)%>\n<%=anyo%>_<%=gmn1%>_<%=proce%>_<%=prove%>.xls\n<%=den(8)%>")
		}
	if( bContinuar)
        {
		bContinuar = ShowProgress()
	    if (<%=application("FSAL")%> == 1)
	        {
            Ajax_FSALRegistrar5(fechaIniExcelServer,"<%=sIDPagina%>".replace("excel.asp","excelserver.asp"),sIdRegistroExcelServer); //FFCLIPREPAR al final
            }
	    }
	return(bContinuar)
	}
</SCRIPT>
<BODY background="images/pantalla_excel.gif" onload="init()">
<form name = frmExcel id = frmExcel method = post  enctype="multipart/form-data" action=excelserver.asp?<%=PID%>  onsubmit="return Validar();">
<table width=100%>
	<tr>
		<td align=center>	
			<table style="border:solid 1 black;" width=90%>
				<tr>
					<td bgcolor=white><%=den(1)%></td>
				</tr>
				<tr>
					<td bgcolor=white align=center>
						<input type=button class=button value="<%=den(2)%>" onclick="descargar(true)" id=btnDescarga name=btnDescarga>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align=center>	
			&nbsp;
		</td>
	</tr>
	<tr>
		<td align=center>	
			<table style="border:solid 1 black;" width=90%>
				<tr>
					<td><%=den(3)%></td>
				</tr>
				<tr>
					<td>
						<input size=30 type=file class=negrita value="<%=den(5)%>" id=btnCarga name=btnCarga>
					</td>
				</tr>
				<tr>
					<td align=center>
						<input type=submit class=button value="<%=den(4)%>" id=btnAceptar name=btnAceptar>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
</BODY>
<!--#include file="../common/fsal_2.asp"-->
</HTML>
