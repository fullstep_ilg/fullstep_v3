﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/formatos.asp"-->

<%
''' <summary>
''' Descarga un adjunto
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\js\adjunto.asp ; Tiempo máximo: 0,2</remarks>

Idioma=request("Idioma")
Idioma = trim(Idioma)

set oRaiz=validarUsuario(Idioma,true,false,0)


CiaComp = clng(oRaiz.Sesion.CiaComp)
anyoproceso=Request.Cookies ("PROCE_ANYO") 
gmn1proceso=Request.Cookies ("PROCE_GMN1") 
codproceso=Request.Cookies ("PROCE_COD") 
CodProve = oRaiz.Sesion.CiaCodGs


ID=request("IDAdjun")
if ID="null" then
	ID = null
end if

IDPortal = Request("IDAdjunPortal")
if IDPortal = "null" then
	IDPortal = null
end if

if isnull(id) then
	sId= cstr(IDPortal)
else
	if isnull(idPortal) then
		sId = cstr(id) 
	else
		sId = cstr(id) & "_" & cstr(idportal) ' aunque esto no se de nunca
	end if
end if

nombre= request("nombre")
dataSize = clng(request("datasize"))

set oAdjunto=oraiz.generar_CAdjunto
oAdjunto.adjunID=ID
oAdjunto.adjunIDPortal = Idportal
oAdjunto.datasize = dataSize

s=""
lowerbound=65
upperbound=90
randomize
for i = 1 to 10
	s= s & chr(Int((upperbound - lowerbound + 1) * Rnd + lowerbound))
next	

sPath = Application ("CARPETAUPLOADS") & "\" & s & "\" & anyoproceso & "_" & gmn1proceso & "_" & codproceso & "_" & sId & "\"

OldTimeout=Server.ScriptTimeout

Server.ScriptTimeout= 40000

set oError = oAdjunto.escribirADisco(CiaComp,sPath, nombre)

Server.ScriptTimeout=OldTimeout


set oadjunto = nothing

if oError.numError=400 then
dim den(20)
den(1) = "El archivo adjunto ha sido eliminado en la compañía compradora. Es irrecuperable. Se eliminará del portal."
%>
<script>
p = parent.frames["fraOfertaClient"]

alert("<%=JSAlertText(den(1))%>")
eval("p." + p.sPosibleEliminar)


</script>

<%
else
%>
<script>
window.open("<%=application("RUTASEGURA")%>script/common/download.asp?path=<%=server.urlencode(s & "/" & anyoproceso & "_" & gmn1proceso & "_" & codproceso & "_" & sId)%>&nombre=<%=server.urlencode(oERror.arg1)%>&datasize=<%=server.urlencode(datasize)%>","_blank","top=100,left=150,width=630,height=300,location=no,menubar=yes,resizable=yes,scrollbars=no,toolbar=no")
</script>

<%
end if
set oraiz = nothing 

Response.End

	

%>
<title><%=title%></title>