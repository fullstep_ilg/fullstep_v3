﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->


<html>
<title><%=title%></title>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script src="../common/formatos.js"></script>
<script src="../common/calendar.asp"></script>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">

<%
''' <summary>
''' Indicar la fecha "valida hasta" para una oferta/subasta
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cumpoferta.asp ; Tiempo máximo: 0,2</remarks>

	Idioma = request.cookies("USU_IDIOMA")

	
	set oRaiz=validarUsuario(Idioma,false,false,0)

	Den=devolverTextos(idioma,97)
	dFecha = request("fecha")
%>

<title><%=den(1)%></title>

<script>
vdatefmt="<%=request.cookies("USU_DATEFMT")%>"

function validarFechaB(e,vdatefmt,minimo,maximo)
{
var ret

ret = validarFecha(e,vdatefmt,minimo,maximo)

if (!ret)
	alert("<%=den(4)%>" + vdatefmt)

return ret
}


function Cerrar()
{

	var p
	var vi
	var vFec = document.forms["frmFecha"].txtFecha
	
	var ret
	
	ret = validarFechaB(vFec,vdatefmt,null,null)
	
	if (!ret)
		return false
		
	
	
	if (vFec.value=="")
		{
		alert("<%=JSAlertText(den(4))%>" + vdatefmt)
		return false
		}
	p=window.opener 
	p.ponerFecha(vFec.value)
	
	
	window.close()
}
function Cancelar()
{
	p=window.opener 
	if (p) {
		if (p.bGuardando)
			p.bGuardando = false;

		p.bPujando = true;
	}
	window.close()
}
</script>

</head>

<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">

<body topmargin="5" leftmargin="5">

<form name="frmFecha" >
<table width="100%">
	<tr>
		<td colspan="2" class="cabecera"><%=den(1)%></td>
	</tr>
	<tr>
		<td align="center" colspan="2" class="filaImpar">
			<nobr>
			<input size="30" maxlength="10" onchange="return validarFechaB(this,vdatefmt,null,null)" id="txtFecha" name="txtFecha" value="<%=dFecha%>">
			<a href="javascript:void(null)" onclick="show_calendar(&quot;frmFecha.txtFecha&quot;,null,null,vdatefmt)"><img name="imgCalendario" border="0" SRC="../common/images/calendario.gif" WIDTH="15" HEIGHT="14"></a></nobr>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td align="center">
			<input TYPE="button" class="button" ID="btnCerrar" value="<%=Den(2)%>" onclick="return Cerrar()">
		</td>
		<td align="center">
			<input TYPE="button" class="button" ID="btnCancelar" value="<%=Den(3)%>" onclick="Cancelar()">
		</td>
	</tr>
</table>


</form>





</body>

</html>
