﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/colores.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/fsal_7.asp"-->
<script language="JavaScript" src="../common/ajax.js"></script>
<script src="../common/formatos.js"></script>
<!--#include file="../common/fsal_3.asp"-->
<%
''' <summary>
''' Guardar la oferta en el portal.
''	04052009 Cambio como se controla q la puja no es aceptable si se ha establecido una bajada minima en la puja
''' </summary>       
''' <returns>Nada</returns>
''' <remarks>Llamada desde: solicitudesoferta/cumpoferta.asp; Tiempo máximo:2</remarks>

oldTimeOut = server.ScriptTimeout
server.ScriptTimeout = 10000
iNumGrupos = request("ofeNumGrupos")
iEstamosEn = request("estamos")

dim den
Idioma = Request.Cookies("USU_IDIOMA")
den=devolvertextos(Idioma,102)

set oRaiz=validarUsuario(Idioma,true,false,0)

total = iNumGrupos*4 + 6

CiaComp = oRaiz.Sesion.CiaComp
CiaCod = oRaiz.Sesion.CiaCod

ProveCod=oRaiz.Sesion.CiaCodGs

anyoproce = Cint(Request.Cookies ("PROCE_ANYO") )
gmn1proce = Request.Cookies ("PROCE_GMN1")
codproce = Request.Cookies ("PROCE_COD")


CiaProve = oRaiz.Sesion.CiaId
sUsu = oRaiz.Sesion.UsuCod


datefmt=Request.Cookies("USU_DATEFMT")


Set oProceso = oRaiz.Generar_CProceso

oProceso.cargarConfiguracion CiaComp, anyoproce, gmn1proce, codproce, ProveCod
oProceso.cargarProceso CiaComp, Idioma, anyoproce, gmn1proce, codproce

	
'Si no devuelve nada se redirecciona a una página que informa que el plazo
'para pujar ha finalizado


IF oProceso.subasta then


    if (request("modoEnvio")="2"  or request("modoEnvio") = "3") and oProceso.subasta then
	    bPujando = true
    else
	    bPujando = false
    end if



    if bPujando then

	    if oProceso.FechaApertura > oRaiz.ObtenerSQLUTCDate(CiaComp) then
		    set oProceso = nothing
			
		    set oRaiz = nothing
		    
            GuardarFSAL8

		    %>
		    <script>
		    var p
		    p = parent.frames["fraOfertaClient"]
		    if (p.winEspera)
			    p.winEspera.close()
		    window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/pujacerrada.asp?noabierta=1&Idioma=<%=Idioma%>&Anyo=<%=anyoproce%>&Gmn1=<%=gmn1proce%>&Proce=<%=codproce%>","fraOfertaClient")
		
		    </script>
		
		    <%

		    server.ScriptTimeout = oldTimeOut
		    Response.End 
	    end if

	    if  oProceso.subastaActiva=false THEN
		    set oProceso = nothing
			
		    set oRaiz = nothing

            GuardarFSAL8
		    %>
		    <script>
		    var p
		    p = parent.frames["fraOfertaClient"]
		    if (p.winEspera)
		        p.winEspera.close()        
		    window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/pujacerrada.asp?Idioma=<%=Idioma%>&Anyo=<%=anyoproce%>&Gmn1=<%=gmn1proce%>&Proce=<%=codproce%>","fraOfertaClient")
		
		    </script>
		
		    <%
		    server.ScriptTimeout = oldTimeOut
		    Response.End 
	    end if

        if oProceso.estadoSubasta <> 2 then
            %>
            <script>
                var p
                p = parent.frames["fraOfertaClient"]
                if (p.winEspera)
                    p.winEspera.close()
                p.cambiarEstadoSubasta(<%=oProceso.estadoSubasta%>)
                p.mostrarCronometros()
                alert("<%=JSText(den(24))%>")
            </script>
            <%
		    server.ScriptTimeout = oldTimeOut
		    Response.End 
        end if
    end if
end if

procesoCerrado = false

if oProceso.estado = 12 or oProceso.estado = 13 or oProceso.estado = 20 then
	procesoCerrado = true
end if
porFecha=0

if not isnull(oProceso.FechaMinimoLimOfertas) then
	fechaLimite = oProceso.FechaMinimoLimOfertas
	if oRaiz.ObtenerSQLUTCDate(CiaComp)>fechaLimite then
		procesocerrado = true
		porFecha =1
	end if
end if

if procesocerrado = true then

	set oProceso = nothing
    
	set oRaiz = nothing
    
    GuardarFSAL8 %>
		<script>
		var p
		p = parent.frames["fraOfertaClient"]
		if (p.winEspera)
			p.winEspera.close()
		window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/procesocerrado.asp?PorFecha=<%=porFecha%>&Idioma=<%=Idioma%>&Anyo=<%=anyoproce%>&Gmn1=<%=gmn1proce%>&Proc=<%=codproce%>","fraOfertaClient")
		</script>
		<%
	server.ScriptTimeout = oldTimeOut
	Response.End
end if

obj = request("NumOBJ")
if obj ="" or isempty(obj) then
	obj = 0
end if

oProceso.ObjetivosCont = Numero(obj)

set oOferta = oraiz.Generar_COferta
Set oPrecioItems=oraiz.Generar_CPrecioItems

set oOferta.Proceso = oProceso

ooferta.prove = ProveCod

ooferta.codmon = request("ofeMoneda")
ooferta.obs = request("ofeObs")
ooferta.obsadjun = request("ofeObsAdjun")
ooferta.fechahasta = fecha(request("ofeFechaValidez"),datefmt)
oOferta.cambio = Numero(replace(request("ofeCambio"),".",decimalfmt))
ooferta.num = request("numOFE")
oOferta.EstUltOfe = request("ofeEstado")
oOferta.FechaAct = FechaHora(request("fecact"),datefmt)

set oOferta.Proceso = oProceso
set oProceso.oferta = oOferta


dim i
dim j



set oAtribs = oraiz.Generar_CAtributos
set oAtribsOfertados = oRaiz.Generar_CAtributosOfertado
for i = 0 to clng(request("ofeProcNumAtribs"))-1
	sNameAtrib = request("ofeProcInputAtrib" & i)
	if sNameAtrib <> "" then
		arrAtribId = split(sNameAtrib,"_")
		atribId = arrAtribId(Ubound(arrAtribId))
		atribTipo =request("tipoofeprocAtrib_" & atribId)
		select case atribTipo
			case 1:
				valor = request("ofeProcAtrib_" & atribId)
			case 2:
				valor = Numero(request("ofeProcAtrib_" & atribId))
			case 3:
				valor = Fecha(request("ofeProcAtrib_" & atribId),datefmt)
			case 4:
				valor = request("ofeProcAtrib_" & atribId)
				if valor= "1" then
					valor = true
				elseif valor="0" then
					valor = false
				else
					valor= null
				end if
		end select
		set oAtrib = oAtribs.Add(i, atribId, "", "", 0, atribTipo)
		oAtribsOfertados.Add valor, oAtrib
	end if
next


set oOferta.AtribsOfertados = oAtribsOfertados
'JVS guardar estructuras de costes / descuentos (precio item, total item, total grupo, total oferta) para comparar con GS
' Guardar la estructura de Total Oferta
set oCostesDescTotOferta = oraiz.Generar_CAtributos
for i = 0 to clng(request("ofeProcNumCostesDescTotOferta"))-1
	sNameCosteDesc = request("ofeProcInputCosteDescTotOferta" & i)
	if sNameCosteDesc <> "" then
		arrCosteDescId = split(sNameCosteDesc,"_")
		costeDescId = arrCosteDescId(Ubound(arrCosteDescId))
    	id = request("idofeprocCostesDescTotOferta_" & costeDescId)
        paid = request("paidofeprocCostesDescTotOferta_" & costeDescId)
        cod = request("codofeprocCostesDescTotOferta_" & costeDescId)
        denom = request("denofeprocCostesDescTotOferta_" & costeDescId)
        intro = request("introofeprocCostesDescTotOferta_" & costeDescId)
        tipo = request("tipoofeprocCostesDescTotOferta_" & costeDescId)
        valor = request("valorofeprocCostesDescTotOferta_" & costeDescId)
        obligatorio = request("obligatorioofeprocCostesDescTotOferta_" & costeDescId)
        valorMin = request("valorMinofeprocCostesDescTotOferta_" & costeDescId)
        valorMax = request("valorMaxofeprocCostesDescTotOferta_" & costeDescId)
        lista = request("listaofeprocCostesDescTotOferta_" & costeDescId)
        operacion = request("operacionofeprocCostesDescTotOferta_" & costeDescId)
        aplicar = request("aplicarofeprocCostesDescTotOferta_" & costeDescId)
        ambito = request("ambitoofeprocCostesDescTotOferta_" & costeDescId)
        importeParcial = request("importeParcialofeprocCostesDescTotOferta_" & costeDescId)
        alcance = request("alcanceofeprocCostesDescTotOferta_" & costeDescId)
		oCostesDescTotOferta.Add id, paid, cod, denom, intro, tipo, valor, valorMin, valorMax, obligatorio, ,operacion, aplicar, ,ambito, , alcance
	end if
next
set oProceso.CostesDescTotalOferta = oCostesDescTotOferta


' Guardar la estructura de Total Grupo
set oCostesDescTotGrupo = oraiz.Generar_CAtributos
for i = 0 to clng(request("ofeProcNumCostesDescTotGrupo"))-1
    set oGruposCosteDesc = oraiz.Generar_CGruposCosteDesc
	sNameCosteDesc = request("ofeProcInputCosteDescTotGrupo" & i)
	if sNameCosteDesc <> "" then
		arrCosteDescId = split(sNameCosteDesc,"_")
		costeDescId = arrCosteDescId(Ubound(arrCosteDescId))
    	id = request("idofeprocCostesDescTotGrupo_" & costeDescId)
        paid = request("paidofeprocCostesDescTotGrupo_" & costeDescId)
        cod = request("codofeprocCostesDescTotGrupo_" & costeDescId)
        denom = request("denofeprocCostesDescTotGrupo_" & costeDescId)
        intro = request("introofeprocCostesDescTotGrupo_" & costeDescId)
        tipo = request("tipoofeprocCostesDescTotGrupo_" & costeDescId)
        valor = request("valorofeprocCostesDescTotGrupo_" & costeDescId)
        obligatorio = request("obligatorioofeprocCostesDescTotGrupo_" & costeDescId)
        valorMin = request("valorMinofeprocCostesDescTotGrupo_" & costeDescId)
        valorMax = request("valorMaxofeprocCostesDescTotGrupo_" & costeDescId)
        lista = request("listaofeprocCostesDescTotGrupo_" & costeDescId)
        operacion = request("operacionofeprocCostesDescTotGrupo_" & costeDescId)
        aplicar = request("aplicarofeprocCostesDescTotGrupo_" & costeDescId)
        ambito = request("ambitoofeprocCostesDescTotGrupo_" & costeDescId)
        importeParcial = request("importeParcialofeprocCostesDescTotGrupo_" & costeDescId)
        alcance = request("alcanceofeprocCostesDescTotGrupo_" & costeDescId)
        for j = 0 to clng(request("numGruposofeprocCostesDescTotGrupo_" & costeDescId))-1
            grupo = request("codGrupo" & j & "ofeprocCostesDescTotGrupo_" & costeDescId)
            oGruposCosteDesc.Add grupo
        next
		oCostesDescTotGrupo.Add id, paid, cod, denom, intro, tipo, valor, valorMin, valorMax, obligatorio, ,operacion, aplicar, ,ambito, oGruposCosteDesc, alcance
	end if
next
set oProceso.CostesDescTotalGrupo = oCostesDescTotGrupo

' Guardar la estructura de Total Item
set oCostesDescTotItem = oraiz.Generar_CAtributos
for i = 0 to clng(request("ofeProcNumCostesDescTotItem"))-1
	sNameCosteDesc = request("ofeProcInputCosteDescTotItem" & i)
	if sNameCosteDesc <> "" then
		arrCosteDescId = split(sNameCosteDesc,"_")
		costeDescId = arrCosteDescId(Ubound(arrCosteDescId))
    	id = request("idofeprocCostesDescTotItem_" & costeDescId)
        paid = request("paidofeprocCostesDescTotItem_" & costeDescId)
        cod = request("codofeprocCostesDescTotItem_" & costeDescId)
        denom = request("denofeprocCostesDescTotItem_" & costeDescId)
        intro = request("introofeprocCostesDescTotItem_" & costeDescId)
        tipo = request("tipoofeprocCostesDescTotItem_" & costeDescId)
        valor = request("valorofeprocCostesDescTotItem_" & costeDescId)
        obligatorio = request("obligatorioofeprocCostesDescTotItem_" & costeDescId)
        valorMin = request("valorMinofeprocCostesDescTotItem_" & costeDescId)
        valorMax = request("valorMaxofeprocCostesDescTotItem_" & costeDescId)
        lista = request("listaofeprocCostesDescTotItem_" & costeDescId)
        operacion = request("operacionofeprocCostesDescTotItem_" & costeDescId)
        aplicar = request("aplicarofeprocCostesDescTotItem_" & costeDescId)
        ambito = request("ambitoofeprocCostesDescTotItem_" & costeDescId)
        importeParcial = request("importeParcialofeprocCostesDescTotItem_" & costeDescId)
        alcance = request("alcanceofeprocCostesDescTotItem_" & costeDescId)
    	oCostesDescTotItem.Add id, paid, cod, denom, intro, tipo, valor, valorMin, valorMax, obligatorio, ,operacion, aplicar, ,ambito, , alcance
	end if
next
set oProceso.CostesDescTotalItem = oCostesDescTotItem

' Guardar la estructura de Precio Item
set oCostesDescPreItem = oraiz.Generar_CAtributos
for i = 0 to clng(request("ofeProcNumCostesDescPreItem"))-1
	sNameCosteDesc = request("ofeProcInputCosteDescPreItem" & i)
	if sNameCosteDesc <> "" then
		arrCosteDescId = split(sNameCosteDesc,"_")
		costeDescId = arrCosteDescId(Ubound(arrCosteDescId))
    	id = request("idofeprocCostesDescPreItem_" & costeDescId)
        paid = request("paidofeprocCostesDescPreItem_" & costeDescId)
        cod = request("codofeprocCostesDescPreItem_" & costeDescId)
        denom = request("denofeprocCostesDescPreItem_" & costeDescId)
        intro = request("introofeprocCostesDescPreItem_" & costeDescId)
        tipo = request("tipoofeprocCostesDescPreItem_" & costeDescId)
        valor = request("valorofeprocCostesDescPreItem_" & costeDescId)
        obligatorio = request("obligatorioofeprocCostesDescPreItem_" & costeDescId)
        valorMin = request("valorMinofeprocCostesDescPreItem_" & costeDescId)
        valorMax = request("valorMaxofeprocCostesDescPreItem_" & costeDescId)
        lista = request("listaofeprocCostesDescPreItem_" & costeDescId)
        operacion = request("operacionofeprocCostesDescPreItem_" & costeDescId)
        aplicar = request("aplicarofeprocCostesDescPreItem_" & costeDescId)
        ambito = request("ambitoofeprocCostesDescPreItem_" & costeDescId)
        importeParcial = request("importeParcialofeprocCostesDescPreItem_" & costeDescId)
        alcance = request("alcanceofeprocCostesDescPreItem_" & costeDescId)
		oCostesDescPreItem.Add id, paid, cod, denom, intro, tipo, valor, valorMin, valorMax, obligatorio, ,operacion, aplicar, ,ambito, , alcance
	end if
next
set oProceso.CostesDescPrecItem = oCostesDescPreItem
' JVS Guardado de los costes / descuentos de la oferta
oOferta.ImporteTotal = Iif(IsNull(Numero(request("ofeProcImporte"))),0,Numero(request("ofeProcImporte")))
oOferta.ImporteBruto = Iif(IsNull(Numero(request("ofeProcImporteBruto"))),0,Numero(request("ofeProcImporteBruto")))

'Se recorren los grupos del proceso y a cada grupo se le va a actualizar el importe neto a partir del campo donde lo hemos guardado
if not oProceso.Grupos is nothing then
    for each oGrupo in oProceso.Grupos
        oGrupo.ImporteTotal  = Numero(request("gruProcImporte_" & oGrupo.Codigo))
    next
end if

set oCostesDesc = oraiz.Generar_CAtributos
set oCostesDescOfertados = oRaiz.Generar_CAtributosOfertado
for i = 0 to clng(request("ofeProcNumCostesDescOferta"))-1
	sNameCosteDesc = request("ofeProcInputCosteDescOferta" & i)
	if sNameCosteDesc <> "" then
		arrCosteDescId = split(sNameCosteDesc,"_")
		costeDescId = arrCosteDescId(Ubound(arrCosteDescId))
    	valor = Numero(request("ofeprocCostesDescOferta_" & costeDescId))
        costeDescImporte = Numero(request("impoofeprocCostesDescOferta_" & costeDescId))
		set oCosteDesc = oCostesDesc.Add(i, costeDescId, "", "", 0, 2)
		oCostesDescOfertados.Add valor, oCosteDesc, , , costeDescImporte
	end if
next
set oOferta.CostesDescOfertados = oCostesDescOfertados

' guardar los importes parciales de grupo por coste/descuento
set oCostesDescGrOfert = oRaiz.Generar_CCostesDescGrOfert
for i = 0 to clng(request("ofeProcNumImpoParcGrOferta"))-1
	sNameImpoParcGr  = request("ofeprocImpoParcGrOferta" & i)
	if sNameImpoParcGr <> "" then
		arrId = split(sNameImpoParcGr,"_")
		id = arrId(Ubound(arrId)-1)
        id = id + "_" + arrId(Ubound(arrId))
		costeDescId = request("ofeprocImpoParcGrOferta_" & id)
        importeParcial = Numero(request("impoofeprocImpoParcGrOferta_" & id))
        grupo = request("grupoofeprocImpoParcGrOferta_" & id)
		oCostesDescGrOfert.Add grupo, costeDescId, importeParcial
	end if
next
set oProceso.ImportesParcialesGrupo = oCostesDescGrOfert

' JVS



oOferta.ObsAdjuntos = request("ofeObsAdjun")

if request("ofeAdjuntosCargados")="1" then
	set oAdjuntos = oRaiz.Generar_CAdjuntos()

	for i = 0 to clng(request("ofeNumAdjuntos"))-1
		sNameAdjun = "ofeprocAdjun_" & i
		
		AdjunId = request("ID_" & sNameAdjun)
		AdjunNom = request("NOM_" & sNameAdjun)
		AdjunCom = request("COM_" & sNameAdjun)
		AdjunIdPortal = request("IDPortal_" & sNameAdjun)
		if AdjunIdPortal ="" then
			AdjunIdPortal=null
			
		end if
		AdjunDel = request("DEL_" & sNameAdjun)
		AdjunMod = request("MOD_" & sNameAdjun)
		AdjunSize = request("SIZE_" & sNameAdjun)

		sOP = "I"
		
		if adjunMod = "1" then
			sOP = "U"
		end if

		if adjunDel = "1" then
			sOP = "D"
		end if

		oAdjuntos.add oProceso, i, AdjunNom,AdjunSize,AdjunCom,adjunid,adjunidportal,sOP
	next

	set oProceso.adjuntos = oAdjuntos
end if



set oGrupos = oRaiz.Generar_CGrupos()
set	oOferta.OfertaGrupos = oRaiz.Generar_COfertaGrupos 

dim ip
ip = 6

dim oEscalados
Dim oEscalado


for i = 0 to clng(request("ofeNumGrupos"))-1
	codGrupo = request("ofeGrupoCodGrupo" & i)
	idGrupo = request("ofeGrupoIdGrupo" & i)

	numItems=0
	if not(isEmpty(request("numItemsGrupo" & codGrupo))) then 
		numItems = Numero(request("numItemsGrupo" & codGrupo))
	else
		if not(isEmpty(request("numItemsGrupoSubasta" & codGrupo))) then
			'numItems = Numero(request("numItemsGrupoSubasta" & codGrupo))	
            numItemsSubasta = Numero(request("numItemsGrupoSubasta" & codGrupo))	
		else
			numItems=0
		end if
	end if	


    set oGrupo = oGrupos.add(oProceso,codGrupo, , , , , , , , , , , , , , , , , , oproceso.conexion,,,idGrupo)
    ' JVS Guardado de los costes / descuentos de los grupos

    set oCostesDesc = oraiz.Generar_CAtributos
    set oOfeGrupo = oOferta.OfertaGrupos.Add(oOferta, oGrupo)
	set oOfeGrupo.CostesDescOfertados = oRaiz.Generar_CAtributosOfertado
    for j = 0 to clng(request("ofeProcNumCostesDescGrupo_" & oGrupo.Codigo))-1
	    sNameCosteDesc = request("ofeProcInputCosteDescGrupo" & j & "_" & oGrupo.Codigo)
	    if sNameCosteDesc <> "" then
		    arrCosteDescId = split(sNameCosteDesc,"_")
		    costeDescId = arrCosteDescId(Ubound(arrCosteDescId))
		    valor = Numero(request("ofeprocCostesDescGrupo_" & oGrupo.Codigo & "_" & costeDescId))
            costeDescImporte = Numero(request("impoofeprocCostesDescGrupo_" & oGrupo.Codigo & "_" & costeDescId))
		    set oCosteDesc = oCostesDesc.Add(i, costeDescId, "", "", 0, 2)
		    oOfeGrupo.CostesDescOfertados.Add valor, oCosteDesc, , , costeDescImporte
	    end if
    next
	if cint(request("numEscaladosGrupo_" & codGrupo)) > 0 then
	'Hay escalados para este grupo
		set oGrupo.escalados = oraiz.Generar_CEscalados
		for j=0 to cint(request("numEscaladosGrupo_" & codGrupo) -1)
			oGrupo.escalados.add request("EscaladoGrupo" & j & "_" & codGrupo)	,request("EscaladoGrupo" & j & "_" & codGrupo & "_ini"), request("EscaladoGrupo" & j & "_" & codGrupo & "_fin")
		next
		oGrupo.hayEscalados = true
	end if
	
    ogrupo.cargargrupo Idioma,CiaComp, ogrupo.id,oOferta.proceso.anyo,oOferta.proceso.gmn1cod,oOferta.proceso.cod,ProveCod

    oGrupo.ImporteTotal = Numero(request("ofeProcImporte_" & oGrupo.Codigo))
    oGrupo.ImporteBruto = Numero(request("ofeProcImporteBruto_" & oGrupo.Codigo))
    
' JVS

	set oOfeGrupo.AtribsOfertados = oRaiz.Generar_CAtributosOfertado
    
	set oGrupo.atributos = oraiz.Generar_CAtributos
	for j = 0 to clng(request("ofeGrupoNumAtribs" & i))-1
		nomTAtrib = request("ofeGrupoInputAtrib_" & codGrupo & "_" & j)
		if nomTAtrib <> "" then
			atribId = split(nomTAtrib,"_")
			atribTipo = request("tipo" & nomTAtrib)
			select case atribTipo
				case 1:
					valor = request(nomTAtrib)
				case 2:
					valor = Numero(request(nomTAtrib))
				case 3:
					valor = Fecha(request(nomTAtrib),datefmt)
				case 4:
					valor = request(nomTAtrib)
					if valor= "1" then
						valor = true
					elseif valor="0" then
						valor = false
					else
						valor= null
					end if
			end select
			
		    set oAtrib = oGrupo.atributos.Add (j, atribID(Ubound(atribId)), "","" ,0 , atribTipo)
			oOfeGrupo.AtribsOfertados.Add valor, oAtrib
		end if
	next


	numAtribsItems = request("numAtribsItem" & codGrupo)

	set oGrupo.atributosItem = oRaiz.Generar_CAtributos


	for j = 0 to clng(numAtribsItems) - 1


		idAtrib=request("txtIDAtribItem_" & codGrupo & "_" & j)
		tipoAtrib=request("txttipoIDAtribItem_" & codGrupo & "_" & j)

		oGrupo.atributosItem.Add idAtrib, idAtrib, "", "", 0, tipoAtrib, null,null, null
	next

	set oGrupo.items = oRaiz.Generar_CItems()

	modoSubastaCargada = request("txtModoSubasta")
	if modoSubastaCargada = "1" then
		for j= 1 to clng(request("numItemsGrupoSubasta" & codGrupo))
			idItem = request("txtPujaIDItem_" & codGrupo & "_" & j)
			sNamePuja = "txtPujaPUBruto_" & idItem
        	sNameCantidad = "txtPujaCantidad_" + codGrupo + "_" + idItem
			set oItem = oGrupo.items.Add (oProceso, oGrupo, idItem, , , , ,Numero(request(sNameCantidad)), , , , , , , , , , , , , , ,   , Numero(request(sNamePuja)), , ,null,,,,true)
            
            ' JVS Guardado de los costes / descuentos de los items
            set oCostesDesc = oraiz.Generar_CAtributos
            set oItem.precioItem.CostesDescOfertados = oRaiz.Generar_CAtributosOfertado()
                for indiceCostesDescItem = 0 to clng(request("ofeProcNumCostesDescItem_" & idItem))-1
	            sNameCosteDesc = request("ofeProcInputCosteDescItem" & indiceCostesDescItem & "_" & idItem)
	            if sNameCosteDesc <> "" then
		            arrCosteDescId = split(sNameCosteDesc,"_")
		            costeDescId = arrCosteDescId(Ubound(arrCosteDescId))
		            valor = Numero(request("ofeprocCostesDescItem_" & idItem & "_" & costeDescId))
                    costeDescImporte = Numero(request("impoofeprocCostesDescItem_" & idItem & "_" & costeDescId))
		            set oCosteDesc = oCostesDesc.Add(indiceCostesDescItem, costeDescId, "", "", 0, 2)
		            oItem.precioItem.CostesDescOfertados.Add valor, oCosteDesc, , , costeDescImporte
	            end if
            next
            oItem.precioItem.ImporteNeto = Numero(request("ofeProcImporteNeto_" & idItem))
            oItem.precioItem.PrecioNeto = Numero(request("ofeProcPrecioNeto_" & idItem))
		next 

	end if
	if numItems>0 or numItemsSubasta>0 then
		oOfeGrupo.ItemsCompletos= true
	else
		oOfeGrupo.ItemsCompletos= false
	end if

	
	for j = 0 to clng(numItems)-1

		idItem = request("txtIDItem_" & codGrupo & "_" & j)
		sNameCantMax = "txtCantMax_" & codGrupo & "_" & idItem
		sNamePU2 = "txtPU2Bruto_" & codGrupo & "_" & idItem
		sNamePU3 = "txtPU3Bruto_" & codGrupo & "_" & idItem
		sNameComent1 = "txtComent1_" + codGrupo + "_" + idItem
		sNameComent2 = "txtComent2_" + codGrupo + "_" + idItem
		sNameComent3 = "txtComent3_" + codGrupo + "_" + idItem
		sNameCantidad = "txtCantidad_" + codGrupo + "_" + idItem

		if oGrupo.hayEscalados then
			pu = ""
            
            puEscIdAux = -1
            puDif = -1
            if request(sNameCantidad) <> "" then
                puDif = Numero(request(sNameCantidad)) 
            end if

			'Escalados
			for each oEscalado in oGrupo.escalados				
				escid = oescalado.id
                if puEscIdAux= -1 then
                    puEscIdAux= escid
                end if
				if request(sNameCantidad) <> "" then				
					ini = request("EscaladoGrupo_" & oescalado.id & "_ini")
					ini = Numero(ini)					
					fin = request("EscaladoGrupo_" & oescalado.id & "_fin")					
					if isnumeric(fin) then ' Rango
						fin = Numero(fin)
						if Numero(request(sNameCantidad)) > ini AND Numero(request(sNameCantidad)) <= fin then
							sNamePU = "txtPUBruto_" & codGrupo & "_" & idItem & "_" & oescalado.id
							pu = request(sNamePU)						
							exit for
						end if						
					else ' Cantidades directas		                    			
						if cstr(request(sNameCantidad)) = cstr(ini) then
							sNamePU = "txtPUBruto_" & codGrupo & "_" & idItem & "_" & oescalado.id
							pu = request(sNamePU)						
							exit for
                        else
                            puDifAux = (Numero(request(sNameCantidad)) - ini)
                            
                            if Math.abs(puDifAux) < puDif then
                                puDif = puDifAux
                                puEscIdAux= escid
                            end if
						end if
					end if					
				end if			
			next				
			if pu="" and isnumeric(fin) then
				if Numero(request(sNameCantidad)) > fin  then ' El último rango inferior 
					sNamePU = "txtPUBruto_" & codGrupo & "_" & idItem & "_" & escid
					pu = request(sNamePU)						
				end if
			end if				
			if pu="" and not isnumeric(fin) then                
				sNamePU = "txtPUBruto_" & codGrupo & "_" & idItem & "_" & puEscIdAux
				pu = request(sNamePU)	
            end if
		else
			sNamePU = "txtPUBruto_" & codGrupo & "_" & idItem
			pu = request(sNamePU)
		end if		
		set oItem = oGrupo.items.item(idItem)
		if oItem is nothing then
			set oItem = oGrupo.items.Add (oProceso, oGrupo, idItem, , , , , Numero(request(sNameCantidad)), , , , , , , , , , , , , , ,   Numero(request(sNameCantMax)), Numero(pu), Numero(request(sNamePU2)), Numero(request(sNamePU3)),null,request(sNameComent1),request(sNameComent2),request(sNameComent3))
		else
			with oItem.precioitem
				.Cantidad = Numero(request(sNameCantMax))
				.Precio2 = Numero(request(sNamePU2))
				.Precio3 = Numero(request(sNamePU3))
				.Comentario1 = request(sNameComent1)
				.Comentario2 = request(sNameComent2)
				.Comentario3 = request(sNameComent3)
			end with
		end if
		' Precios escalados
		if (oGrupo.hayEscalados) then
			set oItem.escalados = oRaiz.generar_CEscalados
			for k=0 to cint(request("numEscaladosGrupo_" & codGrupo)) -1
				idesc = request("EscaladoGrupo" & k & "_" & oGrupo.codigo)
				sNamePUBruto = "txtPUBruto_" & codGrupo & "_" & idItem & "_" & idesc
				sNamePU = "txtPU_" & codGrupo & "_" & idItem & "_" & idesc	                		
				oItem.escalados.add idesc,request("EscaladoGrupo_" & idesc & "_ini"),request("EscaladoGrupo_" & idesc & "_fin"), Numero(request(sNamePUBruto)) , Numero(request(sNamePU))
			next		
		end if
		
		' Atributos
		set oItem.precioItem.AtributosOfertado = oRaiz.Generar_CAtributosOfertado()		
		for k = 0 to clng(numAtribsItems) - 1
			idAtrib=request("txtIDAtribItem_" & codGrupo & "_" & k)
			tipoAtrib=request("txttipoIDAtribItem_" & codGrupo & "_" & k)
			sNameAtrib = "itemAtrib_" & codGrupo & "_" & idItem & "_" & idAtrib
			select case tipoAtrib
				case 1:
					valor = request(sNameAtrib)
					if valor = "" then
						valor = null
					end if
					
				case 2:
					valor = Numero(request(sNameAtrib))
				case 3:
					valor = Fecha(request(sNameAtrib),datefmt)
				case 4:
					valor = request(sNameAtrib)
					if valor= "1" then
						valor = true
					elseif valor="0" then	
						valor = false
					else
						valor= null
					end if
			end select
				
			if oGrupo.HayEscalados then				
				if lcase(request("txtAplicarAtribItem_" & codGrupo & "_" & k )) <> "null" then						
					'los atributos "aplicables" también llevan escalados
					set oAtrib = oItem.precioItem.AtributosOfertado.add (null, oGrupo.atributosItem.Item(idAtrib))
					'Valor a nulo -> Se carga del escalado correspondiente
					set oAtrib.escalados = oRaiz.generar_CEscalados
					nEscalados = cint(request("numEscaladosGrupo_" & oGrupo.codigo))
					for ne=0 to nEscalados -1
						idesc = request("EscaladoGrupo" & ne & "_" & oGrupo.codigo)								
						oAtrib.escalados.add idesc,request("EscaladoGrupo" & j & "_" & codGrupo & "_ini"),request("EscaladoGrupo" & j & "_" & codGrupo & "_fin"), , ,Numero(request(sNameAtrib & "_" & idesc ))
						if oItem.Escalados.usar(idesc,oItem.cantidad) then
							oAtrib.valor = Numero(request(sNameAtrib & "_" & idesc ))
						end if
						oAtrib.hayEscalados = true
					next		
				else					
					'No hay escalados, el valor es el recogido del formulario.
					oItem.precioItem.AtributosOfertado.add  valor, oGrupo.atributosItem.Item(idAtrib) 
				end if
			else
			'No hay escalados, el valor es el recogido del formulario.
				oItem.precioItem.AtributosOfertado.add  valor, oGrupo.atributosItem.Item(idAtrib) 
			end if
		next
            if modoSubastaCargada <> "1" then
                ' JVS Guardado de los costes / descuentos de los items
                set oCostesDesc = oraiz.Generar_CAtributos
                set oItem.precioItem.CostesDescOfertados = oRaiz.Generar_CAtributosOfertado()
                    for indiceCostesDescItem = 0 to clng(request("ofeProcNumCostesDescItem_" & codGrupo & "_" & idItem))-1
	                sNameCosteDesc = request("ofeProcInputCosteDescItem" & indiceCostesDescItem & "_" & codGrupo & "_" & idItem)
	                if sNameCosteDesc <> "" then
		                arrCosteDescId = split(sNameCosteDesc,"_")
		                costeDescId = arrCosteDescId(Ubound(arrCosteDescId))
		                valor = Numero(request("ofeprocCostesDescItem_" & codGrupo & "_" & idItem & "_" & costeDescId))
                        costeDescImporte = Numero(request("impoofeprocCostesDescItem_" & codGrupo & "_" & idItem & "_" & costeDescId))
		                set oCosteDesc = oCostesDesc.Add(indiceCostesDescItem, costeDescId, "", "", 0, 2)

						if oGrupo.HayEscalados then
							set oCosteDesc.escalados = oRaiz.generar_CEscalados
							for k=0 to cint(request("numEscaladosGrupo_" & codGrupo)) -1
								idesc = request("EscaladoGrupo" & k & "_" & oGrupo.codigo)								
								oCosteDesc.escalados.add idesc,request("EscaladoGrupo" & j & "_" & codGrupo & "_ini"),request("EscaladoGrupo" & j & "_" & codGrupo & "_fin"), , ,Numero(request(sNameCosteDesc & "_" & idesc ))
								oCosteDesc.hayEscalados = true
							next		
						end if												
		                oItem.precioItem.CostesDescOfertados.Add valor, oCosteDesc, , , costeDescImporte
	                end if
                next
				oItem.precioItem.ImporteNeto = Numero(request("ofeProcImporteNeto_" & codGrupo & "_" & idItem))				
				oItem.precioItem.PrecioNeto = Numero(request("ofeProcPrecioNeto_" & codGrupo & "_" & idItem))
                ' JVS

            end if



	
		if request("ItemAdjuntosCargados_" & idItem)="1" then
			sNameInput = "numAdjunItem" & idItem
			oItem.precioItem.ObsAdjuntos=request("txtObsAdjunItem_" & idItem)
			if request(sNameInput)<>"" then
				iNumAdjuntosItem = clng(request(sNameInput))
			else
				iNumAdjuntosItem = 0
			end if
		
			set oItem.precioItem.adjuntos = oRaiz.generar_CAdjuntos()
			for k = 1 to iNumAdjuntosItem
				sNameAdjun = "ofeitemAdjun_" & idItem & "_" &  k
					
				AdjunId = request("ID_" & sNameAdjun)
				AdjunNom = request("NOM_" & sNameAdjun)
				AdjunCom = request("COM_" & sNameAdjun)
				AdjunIdPortal = request("IDPortal_" & sNameAdjun)
				if AdjunIdPortal ="" then
					AdjunIdPortal=null
				end if
				AdjunDel = request("DEL_" & sNameAdjun)
				AdjunMod = request("MOD_" & sNameAdjun)
				AdjunSize = request("SIZE_" & sNameAdjun)

				sOP = "I"
		
				if adjunMod = "1" then
					sOP = "U"
				end if
				if adjunDel = "1" then
					sOP = "D"
				end if
				oItem.precioItem.adjuntos.add oItem, k, AdjunNom,AdjunSize,AdjunCom,adjunid,adjunidportal,sOP
			next
		end if
		
	next

	oOfeGrupo.ObsAdjuntos = request("ofeGrupoObsAdjun" & i)
	if request("ofeGrupoAdjuntosCargados" & i) = "1" then

		set oOfeGrupo.adjuntos = oRaiz.Generar_CAdjuntos()
		for j = 0 to clng(request("ofeGrupoNumAdjuntos" & i))-1

			sNameAdjun = "ofeprocAdjun_" & codGrupo & "_" &  j
			
			AdjunId = request("ID_" & sNameAdjun)
			AdjunNom = request("NOM_" & sNameAdjun)
			AdjunCom = request("COM_" & sNameAdjun)
			AdjunIdPortal = request("IDPortal_" & sNameAdjun)
			if AdjunIdPortal ="" then
				AdjunIdPortal=null
			end if
			AdjunDel = request("DEL_" & sNameAdjun)
			AdjunMod = request("MOD_" & sNameAdjun)
			AdjunSize = request("SIZE_" & sNameAdjun)

			sOP = "I"
			
			if adjunMod = "1" then
				sOP = "U"
			end if

			if adjunDel = "1" then
				sOP = "D"
			end if
		                    
			oOfeGrupo.adjuntos.add oGrupo, j, AdjunNom,AdjunSize,AdjunCom,adjunid,adjunidportal,sOP
		next
	end if

next

set oProceso.Grupos = oGrupos
'Si hay que recalcular pasar un parametro a la función
if request("recalc")="1" then
    bRecalcular = True
else
    bRecalcular = False
end if





if bPujando then
    if  not oOferta.ComprobarOfertaPortal(CiaComp, CiaProve, sUsu, Idioma, bPujando, bRecalcular)  then        
        dim primeritem
        dim icono


        %>
        <script>
	        var sHTML = ""		
	        var fila = "filaImpar"
	        var p
	        p = parent.frames["fraOfertaClient"]
            var descError

            <% 
            dim fila
            fila = "filaImpar"

            select case oOFerta.condicionPuja            
            case 1' TDCSuperaPrecioSalida           
                descError = den(18)
                icono = "puja-supera-precio-salida.png"
                if not isempty (ooferta.proceso.importecondicion) then
                'Se supera el importe de salida del proceso
                 %>
				    sHTML += "<tr><td class=cabecera colspan=3><%=Server.HTMLEncode(den(10))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(11))%></td></tr>"
				    sHTML += "<tr><td class=<%=fila%> colspan=3><%=Server.HTMLEncode(oOferta.proceso.den)%></td><td class=<%=fila%> width=100 align=right><%=VisualizacionNumero(oOferta.proceso.importecondicion ,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td></tr>"                    
                <%
                end if
                if not isempty(oOferta.gruposIncumplenCondicion) then
                    for each oGrupo in  oOferta.gruposIncumplenCondicion
                        fila = "filaPar"    
                        if not isempty(oGrupo.importecondicion) then
                            %>
				            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td class=cabecera colspan=2><%=Server.HTMLEncode(den(9))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(11))%></td></tr>"
				            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td class=<%=fila%> colspan=2><%=Server.HTMLEncode(oGrupo.codigo) & " " & Server.HTMLEncode(oGrupo.den)%></td><td class=<%=fila%> width=100 align=right><%=VisualizacionNumero(ogrupo.importecondicion,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td></tr>"
                            <% 
                        else 
                            %>
				            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td class=cabecera colspan=3><%=Server.HTMLEncode(den(9))%></td></tr>"
				            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td class=<%=fila%> colspan=3><%=Server.HTMLEncode(oGrupo.codigo) & " " & Server.HTMLEncode(oGrupo.den)%></td></tr>"
                            <%
                        end if
                        primeritem = true

                        for each oItem in oGrupo.items
                            if fila = "filaPar" then fila = "filaImpar" else fila = "filaPar"

                            if primeritem then    
                                %>
				                sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td width=1% >&nbsp;&nbsp;</td><td class=cabecera><%=Server.HTMLEncode(den(3))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(12))%></td></tr>"
                                <%
                                primeritem = false
                            end if
                             %>
				            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td width=1% >&nbsp;&nbsp;</td><td class=<%=fila%>><%=Server.HTMLEncode(oItem.articulocod) & " " & Server.HTMLEncode(oItem.descr)%></td><td class=<%=fila%> width=100 align=right><%=VisualizacionNumero(oitem.importecondicion,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td></tr>"
                            <% 
                        next
                    next
                end if
            case 2' TDCSuperaPujaPropia
                descError = den(19)
                icono = "puja-supera-precio-salida.png"
                if not isempty (ooferta.proceso.importecondicion) then
                'Se supera el importe de salida del proceso
                 %>
				    sHTML += "<tr>"
                    sHTML += "<td class=cabecera colspan=3>"
                    sHTML += "<%=Server.HTMLEncode(den(10))%>"
                    sHTML += "</td>"
                    sHTML += "<td class=cabecera width=100>"
                    sHTML += "<%=Server.HTMLEncode(den(13))%>"
                    sHTML += "</td>"
                    sHTML += "<td class=cabecera width=100>"
                    sHTML += "<%=Server.HTMLEncode(den(14))%>"
                    sHTML += "</td>"
                    sHTML += "</tr>"

				    sHTML += "<tr>"
                    sHTML += "<td class=<%=fila%> colspan=3>"
                    sHTML += "<%=Server.HTMLEncode(oOferta.proceso.den)%>"
                    sHTML += "</td>"
                    sHTML += "<td class=<%=fila%> width=100 align=right>"
                    sHTML += "<%=VisualizacionNumero(oOferta.importetotal,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %>"
                    sHTML += "</td>"
                    sHTML += "<td class=<%=fila%> width=100 align=right>"
                    sHTML += "<%=VisualizacionNumero(oOferta.proceso.importecondicion,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %>"
                    sHTML += "</td>"
                    sHTML += "</tr>"
                    
                <%
                end if
                if not isempty(oOferta.gruposIncumplenCondicion) then
                    for each oGrupo in  oOferta.gruposIncumplenCondicion
                        fila = "filaPar"    
                        if not isempty(oGrupo.importecondicion) then
                            %>
				            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td class=cabecera colspan=2><%=Server.HTMLEncode(den(9))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(13))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(14))%></td></tr>"

                            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td class=<%=fila%> colspan=2><%=Server.HTMLEncode(oGrupo.codigo) & " " & Server.HTMLEncode(oGrupo.den)%></td><td class=<%=fila%> width=100 "
                            <%if isnull(ogrupo.importetotal) then %>
                                sHTML += " align=center><b><%=Server.HTMLEncode(den(27))%></b></td>"
                            <%else %>
                                sHTML += " align=right><%=VisualizacionNumero(ogrupo.importetotal,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td>
                            <%end if%>
                                sHTML += "<td class=<%=fila%> width=100 align=right><%=VisualizacionNumero(ogrupo.importecondicion,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td></tr>"
                            <% 
                        else 
                            %>
				            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td class=cabecera colspan=5><%=Server.HTMLEncode(den(9))%></td></tr>"
				            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td class=<%=fila%> colspan=5><%=Server.HTMLEncode(oGrupo.codigo) & " " & Server.HTMLEncode(oGrupo.den)%></td></tr>"
                            <%
                        end if
                        primeritem = true
                        if not oGrupo.items is nothing then
                        for each oItem in oGrupo.items
                            if fila = "filaPar" then fila = "filaImpar" else fila = "filaPar"
                            if primeritem then    
                                %>
				                sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td width=1% >&nbsp;&nbsp;</td><td class=cabecera><%=Server.HTMLEncode(den(3))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(15))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(13))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(14))%></td></tr>"
                                <%
                                primeritem = false
                            end if
                             %>
				            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td width=1% >&nbsp;&nbsp;</td><td class=<%=fila%> ><%=Server.HTMLEncode(oItem.articulocod) & " " & Server.HTMLEncode(oItem.descr)%></td><td class=<%=fila%> width=100 "
                            <%if isnull(oitem.precioitem.precioneto) then %>
                                sHTML += "align=center><b><%=Server.HTMLEncode(den(27))%></b>"
                            <%else %>
                                sHTML += "align=right><%=VisualizacionNumero(oitem.precioitem.precioneto,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %>"
                            <%end if %>
                            sHTML += "</td><td class=<%=fila%> width=100 "
                            <%if isnull(oitem.precioitem.precioneto) then %>
                                sHTML += "align=center><b><%=Server.HTMLEncode(den(27))%></b>"
                            <%else%>                                                        
                                sHTML += "align=right><%=VisualizacionNumero(oitem.precioitem.importeneto,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %>"
                            <%end if %>
                            sHTML += "</td><td class=<%=fila%> width=100 align=right><%=VisualizacionNumero(oitem.importecondicion,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td></tr>"
                            <% 
                        next
                        end if
                    next
                end if
            case 3' TDCBajadaMinimaProve
                descError = den(20)                
                icono = "puja-bajada-minima.png"
                if not isempty (ooferta.proceso.importecondicion) then
                'Se supera el importe de salida del proceso
                 %>
				    sHTML += "<tr><td class=cabecera colspan=4><%=Server.HTMLEncode(den(10))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(13))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(14))%></td></tr>"
				    sHTML += "<tr><td class=<%=fila%> colspan=4><%=Server.HTMLEncode(oOferta.proceso.den)%></td><td class=<%=fila%> width=100 align=right><%=VisualizacionNumero(oOferta.importetotal,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td><td class=<%=fila%> width=100 align=right><%=VisualizacionNumero(oOferta.proceso.importecondicion,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td></tr>"
                    
                <%
                end if
                if not isempty(oOferta.gruposIncumplenCondicion) then
                    for each oGrupo in  oOferta.gruposIncumplenCondicion
                        fila = "filaPar"    
                        if not isempty(oGrupo.importecondicion) then
                            %>
				            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td class=cabecera colspan=3><%=Server.HTMLEncode(den(9))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(13))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(14))%></td></tr>"
                            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td class=<%=fila%> colspan=3><%=Server.HTMLEncode(oGrupo.codigo) & " " & Server.HTMLEncode(oGrupo.den)%></td><td class=<%=fila%> width=100 align=right><%=VisualizacionNumero(ogrupo.importetotal,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td><td class=<%=fila%> width=100 align=right><%=VisualizacionNumero(ogrupo.importecondicion,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td></tr>"
                            <% 
                        else 
                            %>
				            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td class=cabecera colspan=4><%=Server.HTMLEncode(den(9))%></td></tr>"
				            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td class=<%=fila%> colspan=4><%=Server.HTMLEncode(oGrupo.codigo) & " " & Server.HTMLEncode(oGrupo.den)%></td></tr>"
                            <%
                        end if
                        primeritem = true
                        if not oGrupo.items is nothing then
                        for each oItem in oGrupo.items
                            if fila = "filaPar" then fila = "filaImpar" else fila = "filaPar"

                            if primeritem then    
                                %>
				                sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td width=1% >&nbsp;&nbsp;</td><td class=cabecera><%=Server.HTMLEncode(den(3))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(15))%>"
                                sHTML += "</td><td class=cabecera width=100><%=Server.HTMLEncode(den(13))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(14))%></td></tr>"
                                <%
                                primeritem = false
                            end if
                             %>
				            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td width=1% >&nbsp;&nbsp;</td><td class=<%=fila%> ><%=Server.HTMLEncode(oItem.articulocod) & " " & Server.HTMLEncode(oItem.descr)%></td><td class=<%=fila%> width=100 align=right><%=VisualizacionNumero(oitem.precioitem.precioneto,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td><td class=<%=fila%> width=100 align=right><%=VisualizacionNumero(oitem.precioitem.importeneto,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td><td class=<%=fila%> width=100 align=right><%=VisualizacionNumero(oitem.importecondicion,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td></tr>"
                            <% 
                        next
                        end if
                    next
                end if

            %>
            <%
            case 4' TDCBajadaMinima
                descError = den(21)
                icono = "puja-bajada-minima.png"
                if not isempty (ooferta.proceso.importecondicion) then
                'Se supera el importe de salida del proceso
                 %>
				    sHTML += "<tr><td class=cabecera colspan=4><%=Server.HTMLEncode(den(10))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(13))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(14))%></td></tr>"
				    sHTML += "<tr><td class=<%=fila%> colspan=4><%=Server.HTMLEncode(oOferta.proceso.den)%></td><td class=<%=fila%> width=100 align=right><%=VisualizacionNumero(oOferta.importetotal,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td><td class=<%=fila%> width=100 align=right><%=VisualizacionNumero(oOferta.proceso.importecondicion,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td></tr>"
                    
                <%
                end if
                if not isempty(oOferta.gruposIncumplenCondicion) then
                    for each oGrupo in  oOferta.gruposIncumplenCondicion
                        fila = "filaPar"    
                        if not isempty(oGrupo.importecondicion) then
                            %>
				            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td class=cabecera colspan=3><%=Server.HTMLEncode(den(9))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(13))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(14))%></td></tr>"
                            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td class=<%=fila%> colspan=3><%=Server.HTMLEncode(oGrupo.codigo) & " " & Server.HTMLEncode(oGrupo.den)%></td><td class=<%=fila%> width=100 align=right><%=VisualizacionNumero(ogrupo.importetotal,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td><td class=<%=fila%> width=100 align=right><%=VisualizacionNumero(ogrupo.importecondicion,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td></tr>"
                            <% 
                        else 
                            %>
				            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td class=cabecera colspan=5><%=Server.HTMLEncode(den(9))%></td></tr>"
				            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td class=<%=fila%> colspan=5><%=Server.HTMLEncode(oGrupo.codigo) & " " & Server.HTMLEncode(oGrupo.den)%></td></tr>"
                            <%
                        end if
                        primeritem = true
                        if not oGrupo.items is nothing then
                        for each oItem in oGrupo.items
                            if fila = "filaPar" then fila = "filaImpar" else fila = "filaPar"

                            if primeritem then    
                                %>
				                sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td width=1% >&nbsp;&nbsp;</td><td class=cabecera><%=Server.HTMLEncode(den(3))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(15))%>"
                                sHTML += "</td><td class=cabecera width=100><%=Server.HTMLEncode(den(13))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(14))%></td></tr>"
                                <%
                                primeritem = false
                            end if
                             %>
				            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td width=1% >&nbsp;&nbsp;</td><td class=<%=fila%> ><%=Server.HTMLEncode(oItem.articulocod) & " " & Server.HTMLEncode(oItem.descr)%></td><td class=<%=fila%> width=100 align=right><%=VisualizacionNumero(oitem.precioitem.precioneto,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td><td class=<%=fila%> width=100 align=right><%=VisualizacionNumero(oitem.precioitem.importeneto,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td><td class=<%=fila%> width=100 align=right><%=VisualizacionNumero(oitem.importecondicion,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td></tr>"
                            <% 
                        next
                        end if
                    next
                end if

            %>
            <%
            case 5' TDCIgualaPujaExistente

                descError = den(22)
                icono = "puja-igual.png"
                if not isempty (ooferta.proceso.importecondicion) then
                'Se supera el importe de salida del proceso
                 %>
				    sHTML += "<tr><td class=cabecera colspan=3><%=Server.HTMLEncode(den(10))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(13))%></td></tr>"
				    sHTML += "<tr><td class=<%=fila%> colspan=3><%=Server.HTMLEncode(oOferta.proceso.den)%></td><td class=<%=fila%> width=100 align=right><%=VisualizacionNumero(oOferta.proceso.importecondicion,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td></tr>"                    
                <%
                end if
                if not isempty(oOferta.gruposIncumplenCondicion) then
                    for each oGrupo in  oOferta.gruposIncumplenCondicion
                        fila = "filaPar"    
                        if not isempty(oGrupo.importecondicion) then
                            %>
				            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td class=cabecera colspan=2><%=Server.HTMLEncode(den(9))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(13))%></td></tr>"
				            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td class=<%=fila%> colspan=2><%=Server.HTMLEncode(oGrupo.codigo) & " " & Server.HTMLEncode(oGrupo.den)%></td><td class=<%=fila%> width=100 align=right><%=VisualizacionNumero(ogrupo.importecondicion,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td></tr>"
                            <% 
                        else 
                            %>
				            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td class=cabecera colspan=3><%=Server.HTMLEncode(den(9))%></td></tr>"
				            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td class=<%=fila%> colspan=3><%=Server.HTMLEncode(oGrupo.codigo) & " " & Server.HTMLEncode(oGrupo.den)%></td></tr>"
                            <%
                        end if
                        primeritem = true
                        if not oGrupo.items is nothing then
                        for each oItem in oGrupo.items
                            if fila = "filaPar" then fila = "filaImpar" else fila = "filaPar"

                            if primeritem then    
                                %>
				                sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td width=1% >&nbsp;&nbsp;</td><td class=cabecera><%=Server.HTMLEncode(den(3))%></td><td class=cabecera width=100><%=Server.HTMLEncode(den(12))%></td></tr>"
                                <%
                                primeritem = false
                            end if
                             %>
				            sHTML += "<tr><td width=1% >&nbsp;&nbsp;</td><td width=1% >&nbsp;&nbsp;</td><td class=<%=fila%>><%=Server.HTMLEncode(oItem.articulocod) & " " & Server.HTMLEncode(oItem.descr)%></td><td class=<%=fila%> width=100 align=right><%=VisualizacionNumero(oitem.importecondicion,decimalfmt,thousandfmt,precisionfmt) & " " &  oOferta.CodMon %></td></tr>"
                            <% 
                        next
                        end if
                    next
                end if

            %>
            <%
            case 6'TDCIgualaPujaPropia
                descError = den(23)
                icono = "puja-igual.png"
                %>
                sHTML = "<tr><td>&nbsp;</td></tr>"
                <%
            end select
            %>

	        <%
		        
	        %>

            
	        var p
	        p = parent.frames["fraOfertaClient"]
	        if (p.winEspera)
		        p.winEspera.close()
	        if (sHTML!="")
		        {
		        //DPD ¿Porqué se hace esto? -> p.cambiarMoneda(true)
		        vWin = window.open("<%=Application("RUTASEGURA")%>script/blank.htm", "items", "width=550,height=345,status=no,resizable=no,scrollbars=1,top=200,left=200");
		        HTML = "<html>"
		        HTML += "<link rel=stylesheet type='text/css' href='<%=application("RUTASEGURA")%>script/common/estilo.asp'>"
		        HTML += "<title><%=title%></title>"
		        HTML += "<body>"
                HTML += "<center>"
                HTML += "<table>"
                HTML += "<tr>"
                HTML += "<td><img src=images/<%=icono%>></td>"
                HTML += "<td><p class=error><%=Server.HTMLEncode(den(16))%>.<br><br><%=Server.HTMLEncode(descError)%>. <%=Server.HTMLEncode(den(17))%>. </p></td>"
                HTML += "</tr>"
                HTML += "</table>"
                HTML += "<br>"
		        HTML += "<table width=100% border=0>"
		        HTML += sHTML
		        HTML += "</table>"
		        HTML += "<center><br><a href='javascript:void(null)' onclick='window.close()'><%=Server.HTMLEncode(den(7))%></a></center>"
		        HTML += "</body>"
		        HTML += "</html>"
				
		        vWin.document.write(HTML)
		        }

            p.bGuardando=false
        </script>

        <%	

        response.End 
    end if
end if
set TESError = oOferta.AlmacenarOfertaPortal(CiaComp, CiaProve, sUsu, bPujando, bRecalcular) 

idPortal = oOferta.idPortal


if not isnull(oOferta.idPortal) then
    dim IsSecure
    IsSecure = left(Application ("RUTASEGURA"),8)
    if IsSecure = "https://" then
        IsSecure = "; secure"
    else
        IsSecure =""
    end if

    Response.AddHeader "Set-Cookie","PROCE_IDPORTAL=" & oOferta.IDPortal & "; path=/; HttpOnly" + IsSecure
end if

mensaje="0"
If TESError.NumError <> 0 Then
'Este error es cuando han cambiado los costes y descuentos, cantidad de Ã­tems, items o grupos y se recarga toda la pÃ¡gina
  if  TESError.NumError = 46 then  'Han cambiado las condiciones de la oferta, pero la operaciÃ³n se ha efectuado con las nuevas condiciones. Revise su oferta.
    mensaje=TESError.Arg1 
  else 'Aqui sÃ­ que hay error de verdad
	on error resume next
	set oOferta.AtribsOfertados = nothing
	set	oOferta.OfertaGrupos = nothing
	set oOferta.Proceso = nothing
	set oProceso.oferta = nothing
	set oProceso.adjuntos = nothing
	Set oProceso = nothing
	set oOferta = nothing
	Set oPrecioItems=nothing
	set oAtribs = nothing
	set oAtrib = nothing
	set oAtribsOfertados = nothing
	set oAdjuntos = nothing

	set oGrupos = nothing

	set oOfeGrupo.adjuntos = nothing
	set oOfeGrupo.AtribsOfertados = nothing
	set oOfeGrupo = nothing

	set oGrupo.atributosItem = nohting
	set oGrupo.items = nothing

	set oItem.precioItem.adjuntos = nothing
	set oItem.precioItem=nothing
	set oItem = nothing
    
	set oRaiz = nothing%>
			<script>
			var p
			p = parent.frames["fraOfertaClient"]
			p.bPujando = true
			p.bGuardando = false
			//p.cargarOfertasMinimas()
		
		
		
			</script>
	
	<%
	if TESError.NumError =51 OR TESError.NumError =52 OR TESError.NumError= 53 OR TESError.NumError= 54 then
        select case TESError.NumError
		  case 51
		    porFecha=3
		  case 52
		    porFecha=2
		  case 53
		    porFecha=1
		  case 54
		    porFecha=4
		 end select
	
         GuardarFSAL8
	%>
	
		<script>
		var p
		p = parent.frames["fraOfertaClient"]
		if (p.winEspera)
			p.winEspera.close()
		window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/procesocerrado.asp?PorFecha=<%=porFecha%>&Idioma=<%=Idioma%>&Anyo=<%=anyoproce%>&Gmn1=<%=gmn1proce%>&Proc=<%=codproce%>","fraOfertaClient")
		</script>
    <%
    else
    if TESError.NumError <> 4 then

	%>
		<script>
			var p
			p = parent.frames["fraOfertaClient"]
			if (p.winEspera)
				p.winEspera.close()
			alert("<%=JSText(den(25))%>")
		</script>
	<%
	end if
	end if
	server.ScriptTimeout = oldTimeOut
	Response.End 
  end if
end if

if request("modoEnvio")<>"2" then
%>
<script>
	var p
	p = parent.frames["fraOfertaClient"]
	if (p.winEspera)
		p.winEspera.close()
</script>
<%

end if

if request("modoEnvio")="2" then
    GuardarFSAL8%>
	<script>
			var p
			p = parent.frames["fraOfertaClient"]
			window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/confirmoferta.asp?codMon=<%=ooferta.codmon%>&NumOfe=<%=oOferta.num%>&IDPortal=<%=oOferta.IDPortal%>&NumObj=<%=request("NumOBJ")%>","fraCOEspera","top=100,left=150,width=400,height=100,location=no,menubar=no,resizable=yes,scrollbars=yes,toolbar=no")
	</script>
	<%
	sePudoEnviar=10
    response.end
end if
if request("modoEnvio")="2"  or request("modoEnvio") = "3" then
	if oProceso.subasta then

        'Antes de enviarla, verificamos que 
        'Comprobamos que se han rellenado todos los atributos obligatorios
        
        '@@ Comentado temporalmente hasta comprobar que los atributos se cargan correctamente
        'set TESError = oOferta.ValidarAtributosObligatorios(CiaComp)
        '@@

        If TESError.NumError = 0 then
            if oOferta.ErrorEnvio <> 0 then
                ' No se han rellenado todos los atributos
                icono = "puja-supera-precio-salida.png"
                GuardarFSAL8
                %>

            <script>
	        var p
            var icono
            
	        p = parent.frames["fraOfertaClient"]
	        if (p.winEspera)
                try {
		            p.winEspera.close()
                    }
                catch(e)
                    {}
		        vWin = window.open("<%=Application("RUTASEGURA")%>script/blank.htm", "items", "width=400,height=200,status=no,resizable=no,scrollbars=1,top=200,left=200");
		        HTML = "<html>"
		        HTML += "<link rel=stylesheet type='text/css' href='<%=application("RUTASEGURA")%>script/common/estilo.asp'>"
		        HTML += "<title><%=Server.HTMLEncode(den(1))%></title>"
		        HTML += "<body>"
                HTML += "<center>"
                HTML += "<table>"
                HTML += "<tr>"
                HTML += "<td><img src=images/<%=icono%>></td>"
                HTML += "<td><p class=error><%=Server.HTMLEncode(den(27))%></p></td>"
                HTML += "</tr>"
                HTML += "</table>"
                HTML += "<br>"
		        HTML += "<table width=100% border=0>"
		        HTML += "</table>"
		        HTML += "<center><br><a href='javascript:void(null)' onclick='window.close()'><%=Server.HTMLEncode(den(7))%></a></center>"
		        HTML += "</body>"
		        HTML += "</html>"
				
		        vWin.document.write(HTML)
                </script>

                <%
                response.end 
            else
                ' OK. Continuamos
            end if
        else
            'Se ha producido un error al validar los atributosObligatorios
        end if
        CodPortal = cstr(Application ("NOMPORTAL"))
		set tesError = oOferta.enviar(Idioma,CiaComp, Application("PYME"),True,CodPortal)
		If TESError.NumError <> 0 or oOferta.errorEnvio>=100 Then
		End If

		sePudoEnviar = oOferta.ErrorEnvio
		if sePudoEnviar = 0 then		
            
			oOferta.NotificarPujasSuperadas CodPortal

			set oOferta = oProceso.CargarOfertaProveedor(Idioma,ciacomp,ProveCod)

			%>
			<script>
			p.bPujando = true
			p.bGuardando = false
			p.proceso.estado = <%=JSText(oOferta.EstUltOfe)%>			
			p.generaCabecera(p.tituloActual)
            p.actualizarSubasta()
			</script>
			<%
		else
			GuardarFSAL8
            %>
			<script>
				window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/ofertaenviada.asp?popup=1&error=<%=sePudoEnviar%>","_blank","scrollbars=yes, top=60,left=20,width=400,height=300,location=no,menubar=no,toolbar=yes,resizable=yes,addressbar=no")
			</script>
			<%
		end if		
	end if
else 'Guardado sin enviar
'Si se ha recalculado recargo la página, sino no
    if TESError.Arg2 = "RECALCULO" then
    GuardarFSAL8 %>
	<script>	
    window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/solicitudesoferta21.asp?Idioma=<%=Idioma%>&Cia=<%=Server.URLEncode(CiaComp)%>&Anyo=<%=Server.URLEncode(anyoproce)%>&Gmn1=<%=Server.URLEncode(gmn1proce)%>&Cod=<%=Server.URLEncode(codproce)%>&IrA=<%=Server.URLEncode(iEstamosEn)%>&Mensaje=<%=Server.URLEncode(mensaje)%>","default_main")
	</script>    
    <%	    
    else
        set oOferta = oProceso.CargarOfertaProveedor(Idioma,ciacomp,ProveCod)
    %>
        <script>                 
            p.actualizarDatosGenerales(<%=JSNum(oOferta.TotEnviadas)%>,<%=JSDate(oOferta.FechaUltOfe)%>,<%=JSText(oOferta.EstUltOfe)%>,<%=JSNum(oOferta.ImporteTotal)%>,<%=JSNum(oOferta.ImporteBruto)%>,<%=JSHora(TESError.Arg2)%>)
	</script>
    <%
    end if
end if
%>

<SCRIPT>
function init() {
    if (<%=Application("FSAL")%> == 1)
    {
        Ajax_FSALActualizar3(); //registro de acceso
    }
}
init()
</SCRIPT>

<%
on error resume next
set oOferta.AtribsOfertados = nothing
set	oOferta.OfertaGrupos = nothing
set oOferta.Proceso = nothing
set oProceso.oferta = nothing
set oProceso.adjuntos = nothing
Set oProceso = nothing
set oOferta = nothing   
Set oPrecioItems=nothing
set oAtribs = nothing
set oAtrib = nothing
set oAtribsOfertados = nothing
set oAdjuntos = nothing

set oGrupos = nothing

set oOfeGrupo.adjuntos = nothing
set oOfeGrupo.AtribsOfertados = nothing
set oOfeGrupo = nothing

set oGrupo.atributosItem = nohting
set oGrupo.items = nothing

set oItem.precioItem.adjuntos = nothing
set oItem.precioItem=nothing
set oItem = nothing
set oRaiz = nothing

server.ScriptTimeout = oldTimeOut
%>
</BODY>
<%Sub GuardarFSAL8() %>
<!--#include file="../common/fsal_8.asp"-->
<%End Sub %>
<%GuardarFSAL8 %>
</HTML>