﻿<%@ Language=VBScript %>
<%
''' <summary>
''' Muestra los adjuntos del item
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cumpoferta.asp ; Tiempo máximo: 0,2</remarks> 
%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel=stylesheet type='text/css' href='<%=application("RUTASEGURA")%>script/common/estilo.asp'>
<title><%=title%></title>
</HEAD>
<script>
var p

p = window.opener

function uploadRealizado(iSize)
{
p.uploadRealizado(iSize)
}

function cerrar()
{
p.wAdjunItem = null
}

function downloadAdjunto(adjun, grupo, item)
{
p.downloadAdjunto(adjun,grupo,item)
}
function eliminaAdjunto(adjun, grupo, item)
{
p.eliminaAdjunto(adjun,grupo,item)
}
function modificarObsAdjun(obs)
{
p.modificarObsAdjun(obs)
}

function modificarAdjuntoItem(sName)
{

var nombre=document.forms["frmAdjuntos"].item(sName).value

var comentario=document.forms["frmAdjuntos"].elements["com_" + sName].value
var indice = document.forms["frmAdjuntos"].elements["index_" + sName].value
p.modificarAdjuntoItem(nombre,comentario,indice)
}

function DesdeAdjuntosItemRellenar(item)
{
    p.DesdeAdjuntosItemRellenar(item);
}


</script>

<BODY onunload="cerrar()">

<div id="divAdjuntosItem" name="divAdjuntosItem">

</div>
</BODY>
</HTML>
