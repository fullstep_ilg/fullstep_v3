﻿<!--#include file="../../common/acceso.asp"-->
<%
''' <summary>
''' Guarda el coste /desc modificado que se aplica antes, para usarlo a la hora de efectuar el recálculo
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cargarresultadopuja.asp    solicitudesoferta\cumpoferta.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")
set oRaiz=validarUsuario(Idioma,false,false,0)

%>

//''' <summary>
//''' Guarda el coste /desc modificado que se aplica antes, para usarlo a la hora de efectuar el recálculo
//''' </summary>
//''' <param name="numOrden">número de orden de aplicación del coste / desc más bajo</param>
//''' <param name="nombreCosteDesc">nombre del coste / desc</param>
//''' <param name="valor">valor del coste / desc</param>
//''' <param name="idCosteDescuento">id. del coste / desc</param>
//''' <param name="importeParcial">importe parcial neto una vez aplicado el coste / desc</param>
//''' <param name="aplica">a qué aplica el coste / desc (total oferta, total grupo, total item o precio item)</param>
//''' <param name="ambito">ámbito del coste / desc(oferta, grupo o item)</param>
//''' <param name="grupo">a qué grupo aplica el coste / desc</param>
//''' <remarks>Llamada desde: cumpOferta.asp; Tiempo máximo:0,1</remarks>*/
//''' <author>JVS</author>

function  CosteDescRecalculo(numOrden, nombreCosteDesc, valor, idCosteDescuento, importeParcial, aplica, ambito, grupo)
{
this.numOrden = numOrden
this.nombreCosteDesc = nombreCosteDesc
this.valor = valor
this.idCosteDescuento = idCosteDescuento
this.importeParcial = importeParcial
this.aplica = aplica
this.ambito = ambito
this.grupo = grupo
}


