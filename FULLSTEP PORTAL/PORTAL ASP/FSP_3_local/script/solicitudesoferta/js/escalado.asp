﻿<!--#include file="../../common/acceso.asp"-->
<%
''' <summary>
''' Crea el objeto Escalado
''' </summary>
''' <remarks>Llamada desde: js\cargaritems.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")
set oRaiz=validarUsuario(Idioma,false,false,0)

%>

/* Crea el objeto Escalado */
function  Escalado(id, inicio, fin, precio,precioValido,valorNum,precioGS,precioOferta,cantidad, objetivo)
{
	this.id = id 						
	this.inicio = inicio 				/* Valor de inicio o de cantidad directa */
	this.fin = fin 						/* valor de fin para rangos*/
	this.precio = precio				/* Precio escalado (Bruto)*/
	this.precioValido = precioValido    /* Precio Escalado (Neto)*/
	this.valorNum = valorNum	        /* Valor para los costes-Descuentos Escalados*/
	this.precioGS = precioGS	        /* Precio de la ultima oferta enviada (Bruto)*/
	this.precioOferta = precioOferta    /* Precio de la ultima oferta enviada (Neto)*/
	this.usar = false                    /* Indica si el escalado es eñl adecuado para la cantidad establecida */
	this.cant = cantidad                /* Cantidad establecida en caso de ser el escalado adecuado.  null en caso contrario */
	this.objetivo = objetivo            /* Objetivo de precio establecido */

}

