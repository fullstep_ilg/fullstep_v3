﻿<!--#include file="../../common/acceso.asp"-->
<%
''' <summary>
''' Crea el objeto Atributo
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cumpoferta.asp    solicitudesoferta\cargarresultadopuja.asp   js\cargaritems.asp; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")
set oRaiz=validarUsuario(Idioma,false,false,0)

%>


function  Atributo(id, paid, cod, den, intro, tipo, valor, obligatorio, valorMin, valorMax, lista, obj,operacion,aplicar,ambito,importeParcial,grupos,alcance)
{
this.id = id
this.paid = paid
this.cod = cod
this.den = den
this.intro = intro
this.tipo = tipo
this.valor = valor
this.obligatorio = obligatorio
this.valorMin = valorMin
this.valorMax = valorMax
this.lista = lista
this.operacion = operacion
this.aplicar = aplicar
this.ambito = ambito
this.importeParcial = importeParcial
this.grupos = grupos
this.alcance = alcance

this.parent = obj
this.anyaValor = anyaValorAtributo
this.escalados = new Array()
this.anyaEscalado = anyaEscalado
}


function anyaValorAtributo (valor)
{
var i
i = this.lista.length
this.lista[i]= valor
}



function valorAtribItem(item,atrib,valor)
{
this.item = item
this.atrib = atrib
this.valor = valor
}

function valorCosteDescItem(item,costeDesc,valor)
{
this.item = item
this.costeDesc = costeDesc
this.valor = valor
}

function anyaGrupoCosteDesc (grupo)
{
var i
i = this.grupos.length
this.grupos[i]= grupo
}


function anyaEscalado (esc)
{
var i
i = this.escalados.length
this.escalados[i]= esc
}


