﻿<%@ Language=VBScript %>
<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->

<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<script language="JavaScript" src="../../common/ajax.js"></script>
<script src="../../common/formatos.js"></script>
<!--#include file="../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
function init() { 
    if (<%=Application("FSAL")%> == 1){
        Ajax_FSALActualizar3(); //registro de acceso
    }
}
</script>
</HEAD>
<BODY onload="init()">

<%
''' <summary>
''' Cargar los atributos de la oferta a nivel de proceso o de grupo
''' </summary>
''' <remarks>Llamada desde: js\grupo.asp    js\proceso.asp; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")
if Idioma="" then
	Idioma = "SPA"
end if

set oRaiz=validarUsuario(Idioma,true,false,0)


CiaComp = clng(oRaiz.Sesion.CiaComp)
anyo = request("anyo")
gmn1 = request("gmn1")
proce = request("proce")
grupo = request("grupo")
idGrupo = request("IDGrupo")
ofe = request("ofe")
prove = oRaiz.Sesion.CiaCodGs

mon = request("mon")

decimalfmt = Request.Cookies ("USU_DECIMALFMT")
thousanfmt = Request.Cookies ("USU_THOUSANFMT") 
precisionfmt = CINT(Request.Cookies ("USU_PRECISIONFMT") )
datefmt = Request.Cookies ("USU_DATEFMT") 

set oProceso = oRaiz.Generar_CProceso()
with oProceso
	.Anyo = anyo
	.gmn1Cod = gmn1
	.cod = proce
end with
oProceso.cargarProceso CiaComp, Idioma
set oOferta = nothing
set oAtribOfe = nothing
if grupo="" then
	oProceso.cargarAtributos CiaComp
	
	set oOferta = oRaiz.Generar_COferta()
	if request("IdPortal")="null" then
		oOferta.IDPortal = null
	else	
		oOferta.IDPortal = request("IdPortal")
	end if
	
	set oOferta.Proceso = oProceso
	oOferta.Prove = prove
	oOferta.Num = ofe
	oOferta.CargarAtributosOfertados CiaComp, mon
	set oAtribOfe = oOferta.AtribsOfertados
	set oObjeto = oProceso
else
	set oGrupo = oRaiz.Generar_CGrupo()
	set oGrupo.Proceso = oProceso
	
	
	oGrupo.codigo = grupo
	oGrupo.ID = idGrupo
	
	oGrupo.cargarAtributos CiaComp,2
	set oOferta = oRaiz.Generar_COferta()

	if request("IdPortal")="null" then
		oOferta.IDPortal = null
	else
		oOferta.IDPortal = request("IdPortal")
	end if
	

	set oOferta.Proceso = oProceso
	oOferta.Prove = prove
	oOferta.Num = ofe
	set oOfeGrupo = oRaiz.Generar_COfertaGrupo()
	set oOfeGrupo.Oferta = oOferta
	set oOfeGrupo.Grupo = oGrupo
	oOfeGrupo.CargarAtributosOfertados CiaComp, mon
	set oAtribOfe = oOfeGrupo.AtribsOfertados
	set oObjeto = oGrupo
end if

if oOferta.CodMon=mon then
    equiv = oProceso.cambio * oOferta.cambio
else
    equiv = Numero(replace(request("eq"),".", decimalfmt))
end if
%>

<script>
var p

p = parent.frames["fraOfertaClient"]

var i

var oObj
<%if grupo="" then%>
	oObj = p.proceso
	oObj.sinAtributos = true
<%else%>


	var i
	for (i=0;i<p.proceso.grupos.length && p.proceso.grupos[i].cod != '<%=grupo%>';i++)
		{
		}
	oObj = p.proceso.grupos[i]
	oObj.sinAtributos = true
<%end if

if not oObjeto.Atributos is nothing then%>

<%for each oAtrib in oObjeto.Atributos%>
	oObj.sinAtributos = false
	oObj.anyaAtrib(new p.Atributo (<%=oAtrib.id%>,<%=oAtrib.paid%>,'<%=JSText(oAtrib.cod)%>','<%=JSText(oAtrib.den)%>',<%=oAtrib.intro%>,<%=oAtrib.tipo%>,<%
    
	if oAtribOfe is nothing then
		response.write "null"
	else
		if oAtribOfe.Item(cstr(oAtrib.PAID)) is nothing then
			response.write "null"
		else
			select case oAtrib.tipo
				case 1:
					response.write "'" & JSText(oAtribOfe.Item(cstr(oAtrib.PAID)).valor) & "'"
				case 2:
					if (oAtrib.operacion = "+" or  oAtrib.operacion = "-") then
						vValor = oAtribOfe.Item(cstr(oAtrib.PAID)).valor / oOferta.cambio * equiv
					else
						vValor = oAtribOfe.Item(cstr(oAtrib.PAID)).valor 
					end if
					response.write  JSNum(vValor) 
				case 3:
					response.write  JSDate(oAtribOfe.Item(cstr(oAtrib.PAID)).valor)
				case 4:
					response.write  JSBool(oAtribOfe.Item(cstr(oAtrib.PAID)).valor)
			end select
		end if
	end if	

	response.write  "," + JSBool(oAtrib.obligatorio)

	select case oAtrib.tipo
		case 1:
			response.write ",null,null"
		case 2:
			response.write "," & JSNum(oAtrib.valorMin) &  "," & JSNum(oAtrib.valorMax) 
		case 3:
			response.write "," & JSDate(oAtrib.valorMin) & "," & JSDate(oAtrib.valorMax) 
		case 4:
			response.write ",null, null"
	end select
	
	if oAtrib.intro = 1 then
		str=",["
		for each oValor in oatrib.Valores
			if str<>",[" then
				str = str & ","
			end if
			select case oAtrib.tipo
				case 1:
					str = str & "'" & JSText(oValor.Valor) & "'"
				case 2:
					if (oAtrib.operacion = "+" or  oAtrib.operacion = "-") then
						vValor = oValor.Valor / oProceso.Cambio * equiv
					else
						vValor = oValor.Valor 
					end if

					str = str & JSNum(vValor)
				case 3:
					str = str & JSDate(oValor.Valor)
				case 4:
					str = str & JSBool(oValor.Valor)
			end select
		next
		response.write str & "],oObj,'" & JSText(oAtrib.operacion) & "'," & JSNum(oAtrib.aplicar) & ", null,null))"
	else
		response.write ",null,oObj,'" & JSText(oAtrib.operacion) & "',"  & JSNum(oAtrib.aplicar) & ", null,null))"
	end if
next 
end if
%>
oObj.cargarAtribs()
p.desbloquear()
</script>
<%
set oObjeto = nothing
set oOferta = nothing
set oGrupo = nothing
set oProceso = nothing
set oRaiz = nothing
%>

<P>&nbsp;</P>

</BODY>
<!--#include file="../../common/fsal_2.asp"-->
</HTML>
