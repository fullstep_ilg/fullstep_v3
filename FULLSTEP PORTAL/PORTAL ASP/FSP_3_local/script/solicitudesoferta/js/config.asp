﻿<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/idioma.asp"-->
<%
''' <summary>
''' Crea los objetos de configuracion
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cargarresultadopuja.asp    solicitudesoferta\cumpoferta.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")
set oRaiz=validarUsuario(Idioma,false,false,0)


dim den

den = devolverTextos(Idioma,95)

%>

function CfgProceso(dest,pag,fecSum,especs,cantMax,altPrec,adjunOfe,adjunGrp,adjunItem,subasta,subVerProve,subVerDet,cambiarMon,mostrarFinSum,costesDescuentos)
{
this.dest        = dest
this.pag         = pag
this.fecSum      = fecSum
this.especs      = especs
this.cantMax     = cantMax
this.altPrec     = altPrec
this.adjunOfe    = adjunOfe
this.adjunGrp    = adjunGrp
this.adjunItem   = adjunItem
this.subasta     = subasta
this.subVerProve = subVerProve
this.subVerDet   = subVerDet
this.cambiarMon = cambiarMon
this.mostrarFinSum = mostrarFinSum
this.costesDescuentos = costesDescuentos
}


function CfgGrupo(dest,pag,fecSum,especs,atribs,costesDescuentos)
{
this.dest   = dest
this.pag    = pag
this.fecSum = fecSum
this.especs = especs
this.atribs = atribs
this.costesDescuentos = costesDescuentos
}

function obtener(codigo)
{
return true
}

function mostrarConfigProceso(proc)
{
AtribImg = new Image()
AtribImg.src="images/be font.gif"
CosteDescuentoImg = new Image()
CosteDescuentoImg.src="images/be font.gif"
AdjuntosImg = new Image()
AdjuntosImg.src="images/clip07.gif"
GrupoImg = new Image()
GrupoImg.src="images/grupo.gif"
GrupoImgBlq = new Image()
GrupoImgBlq.src="images/grupo_bloqueado.gif"
ItemImg = new Image()
ItemImg.src="images/items.gif"
EspecImg = new Image()
EspecImg.src="images/Especificaciones.gif"
OfertaImg = new Image()
OfertaImg.src="images/Copland Documents.gif"
RellenarImg = new Image()
RellenarImg.src="images/rellenar.gif"
ModoSubastaImg= new Image()
ModoSubastaImg.src = "images/modosubasta.gif"


ramaRoot=new Rama ('Root','Root',proc.anyo + "/" + proc.gmn1 + "/" + proc.cod,false,false,OfertaImg,'DarkGoldenrod',false,true,null,null,'datosProceso',!proceso.cfg.subasta)



if(proceso.cfg.subasta)
	{
	rama3=new Rama ('3','03','<%=JSText(den(5))%>',true,true,ModoSubastaImg,'DarkGoldenrod',false,true,true,null,'modoSubasta',true)
	ramaRoot.add(rama3)
	}

rama1=new Rama ('1','01','<%=JSText(den(1))%>',true,true,RellenarImg,'DarkGoldenrod',false,true,true,null,'datosOferta',false)
ramaRoot.add(rama1)

if(proceso.cfg.costesDescuentos)
    {
    rama4=new Rama ('4','04','<%=JSText(den(8)) %>',true,true,RellenarImg,'DarkGoldenrod',false,true,true,null,'introducirCostesDesc',false) //JVS idioma Costes/Descuentos
    ramaRoot.add(rama4)
    }

if (proceso.cfg.adjunOfe)
	{
	rama2=new Rama ('2','02','<%=JSText(den(2))%>',true,true,RellenarImg,'DarkGoldenrod',false,true,true,null,'adjuntosOferta',false)
	ramaRoot.add(rama2)
	}
var i
for (i=0;i<proc.grupos.length;i++)
	{
    if (proc.grupos[i].Bloqueado == 1)
    {
        x = new Rama ("GR_" + proc.grupos[i].cod , proc.grupos[i].cod , proc.grupos[i].cod + " - " + proc.grupos[i].den ,false,false,GrupoImgBlq,'Gray',false,true,true,null,'datosGrupo',false)
	}
    if (proc.grupos[i].Bloqueado == 0)
    {
	    x = new Rama ("GR_" + proc.grupos[i].cod , proc.grupos[i].cod , proc.grupos[i].cod + " - " + proc.grupos[i].den ,false,false,GrupoImg,'DarkGoldenrod',false,true,true,null,'datosGrupo',false)
	}

	eval("ramaGR_" + proc.grupos[i].cod + " = x")
	eval("ramaRoot.add(ramaGR_" + proc.grupos[i].cod  + ")")
	eval("ramaGR_" + proc.grupos[i].cod + ".hojasCargadas = true")
	eval("ramaGR_" + proc.grupos[i].cod + ".expanded = true")

	if (proc.grupos[i].numItems>0)
		{
		eval("ramaGR_" + proc.grupos[i].cod + "_1 = new Rama ('GR_" + proc.grupos[i].cod + "_1','" + proc.grupos[i].cod + "','<%=JSText(den(3))%>',true,true,RellenarImg,'green',false,true,true,null,'introducirPrecios',false)")
		eval("ramaGR_" + proc.grupos[i].cod + ".add(ramaGR_" + proc.grupos[i].cod + "_1)")
		}

	if (proc.grupos[i].cfg.atribs==true)
		{
		eval("ramaGR_" + proc.grupos[i].cod + "_2 = new Rama ('GR_" + proc.grupos[i].cod + "_2','" + proc.grupos[i].cod + "','<%=JSText(den(9))%>',true,true,RellenarImg,'green',false,true,true,null,'introducirAtributos',false)")
		eval("ramaGR_" + proc.grupos[i].cod + ".add(ramaGR_" + proc.grupos[i].cod + "_2)")
		}
	if (proc.grupos[i].cfg.costesDescuentos==true)
		{
		eval("ramaGR_" + proc.grupos[i].cod + "_4 = new Rama ('GR_" + proc.grupos[i].cod + "_4','" + proc.grupos[i].cod + "','<%=JSText(den(8))%>',true,true,RellenarImg,'green',false,true,true,null,'introducirCostesDescGrupo',false)") //JVS idioma Costes/Descuentos
		eval("ramaGR_" + proc.grupos[i].cod + ".add(ramaGR_" + proc.grupos[i].cod + "_4)")
		}
	if (proc.cfg.adjunGrp==true)
		{
		eval("ramaGR_" + proc.grupos[i].cod + "_3 = new Rama ('GR_" + proc.grupos[i].cod + "_3','" + proc.grupos[i].cod + "','<%=JSText(den(2))%>',true,true,RellenarImg,'green',false,true,true,null,'introducirAdjuntos',false)")
		eval("ramaGR_" + proc.grupos[i].cod + ".add(ramaGR_" + proc.grupos[i].cod + "_3)")
		}
	}				


ramaRoot.write(null)
ramaRoot.expcont()

imgContraer= new Image()
imgContraer.src = "../images/contraer.gif"
imgDesplegar= new Image()
imgDesplegar.src = "../images/desplegar.gif"


}


<%

%>

