﻿<%@ Language=VBScript %>
<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->

<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<script language="JavaScript" src="../../common/ajax.js"></script>
<script src="../../common/formatos.js"></script>
<!--#include file="../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
function init() { 
    if (<%=application("FSAL")%> == 1) {
        Ajax_FSALActualizar3(); //registro de acceso
    }
}
</script>
</HEAD>
<BODY onload="init()">

<%

Idioma = Request.Cookies("USU_IDIOMA")
if Idioma="" then
	Idioma = "SPA"
end if

set oRaiz=validarUsuario(Idioma,true,false,0)


CiaComp = clng(oRaiz.Sesion.CiaComp)
anyo = request("anyo")
gmn1 = request("gmn1")
proce = request("proce")
grupo = request("grupo")
idgrupo = request("idgrupo")
item = request("item")
ofe = request("ofe")
prove = oRaiz.Sesion.CiaCodGs

dim bHayAdjuntos

set oProceso = oRaiz.Generar_CProceso()
with oProceso
	.Anyo = anyo
	.gmn1Cod = gmn1
	.cod = proce
end with

set oOferta = nothing
set oAtribOfe = nothing
set oObjeto = nothing
if grupo="" then
	set oOferta = oRaiz.Generar_COferta()
	set oOferta.Proceso = oProceso
	if request("IdPortal")="null" then
		oOferta.IDPortal = null
	else
		oOferta.IDPortal = request("IDPortal")
	end if
	
	oOferta.Prove = prove
	oOferta.Num = ofe
	oOferta.cargarAdjuntos CiaComp
	set oObjeto = oOferta
elseif item="" then

	set oGrupo = oRaiz.Generar_CGrupo()
	set oGrupo.Proceso = oProceso
	oGrupo.id = idgrupo
	oGrupo.codigo = grupo
	
	set oOferta = oRaiz.Generar_COferta()

	if request("IdPortal")="null" then
		oOferta.IDPortal = null
	else
		oOferta.IDPortal = request("IDPortal")
	end if

	set oOferta.Proceso = oProceso
	oOferta.Prove = prove
	oOferta.Num = ofe
	set oOfeGrupo = oRaiz.Generar_COfertaGrupo()
	set oOfeGrupo.Oferta = oOferta
	set oOfeGrupo.Grupo = oGrupo
	oOfeGrupo.cargarAdjuntos CiaComp
	set oObjeto = oOfeGrupo
else	


	set oGrupo = oRaiz.Generar_CGrupo()
	set oGrupo.Proceso = oProceso
	oGrupo.id = idgrupo
	oGrupo.codigo = grupo
		
	set oOferta = oRaiz.Generar_COferta()
	if request("IdPortal")="null" then
		oOferta.IDPortal = null
	else
		oOferta.IDPortal = request("IDPortal")
	end if
	set oOferta.Proceso = oProceso
	oOferta.Prove = prove
	oOferta.Num = ofe
	set oOfeGrupo = oRaiz.Generar_COfertaGrupo()
	set oOfeGrupo.Oferta = oOferta
	set oOfeGrupo.Grupo = oGrupo
	set oItem = oRaiz.Generar_CItem()
	oItem.id=item
	set oPrecioItem = oRaiz.Generar_CPrecioItem()
	set oPrecioItem.item = oItem
	set oPrecioItem.OfertaGrupo = oOfeGrupo
	oPrecioItem.cargarAdjuntos CiaComp
	set oObjeto = oPrecioItem

end if


%>

<script>
var p

p = parent.frames["fraOfertaClient"]




var i

var oObj
<%if grupo="" then%>
	oObj = p.proceso
<%elseif item="" then%>

	var i
	for (i=0;i<p.proceso.grupos.length && p.proceso.grupos[i].cod != '<%=grupo%>';i++)
		{
		}
	oObj = p.proceso.grupos[i]

<%else%>
	var i
	for (i=0;i<p.proceso.grupos.length && p.proceso.grupos[i].cod != '<%=grupo%>';i++)
		{
		}
	oObj = p.proceso.grupos[i]
	
	for (i=0;i<oObj.items.length && oObj.items[i].id != '<%=item%>';i++)
		{
		}
	oObj = oObj.items[i]


<%end if%>
oObj.sinAdjuntos = true
oObj.adjuntosCargados = true
<%if not oObjeto is nothing then%>
	<%for each oAdjunto in oObjeto.Adjuntos%>
		oObj.anyaAdjunto(new p.Adjunto (<%=JSNum(oAdjunto.AdjunID)%>, '<%=JSText(oAdjunto.nombre)%>','<%=JSText(oAdjunto.comentario)%>',<%=JSNum(oAdjunto.datasize)%>,<%=JSNum(oAdjunto.AdjunIDPortal)%>))
		oObj.sinAdjuntos = false
	<%	
	next 
end if
%>
oObj.cargarAdjuntos()
p.desbloquear()

</script>
<%
set oObjeto = nothing
set oRaiz = nothing	
%>
<P>&nbsp;</P>

</BODY>
<!--#include file="../../common/fsal_2.asp"-->
</HTML>
