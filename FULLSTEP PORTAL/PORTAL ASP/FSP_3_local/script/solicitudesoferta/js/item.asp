﻿<!--#include file="../../common/acceso.asp"-->
<%
''' <summary>
''' Crea el objeto Item
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cargarresultadopuja.asp    solicitudesoferta\cumpoferta.asp   js\actualizarsubasta.asp    js\cargarGanadora.asp
''  js\cargarsubasta.asp; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")
set oRaiz=validarUsuario(Idioma,false,false,0)

%>


//function Item (grupo, id, cod, descr, cant, uni, dest, pag,pagDen, finisum, ffinsum, obj, hayEsp, esp, precio, precio2, precio3, cantmax, numAdjuntos,ofertaMinima, proveCod,proveDen,oferta,objetivo,coment1,coment2, coment3, obsAdjun, atribs,costesDescuentos,hayEspI,precioNeto,importeBruto,importeNeto)
function Item (grupo, id, cod, descr, cant, uni, dest, pag,pagDen, finisum, ffinsum, obj, hayEsp, esp, precio, precio2, precio3, cantmax, numAdjuntos,ofertaMinima, proveCod,proveDen,oferta,objetivo,coment1,coment2, coment3, obsAdjun, atribs,hayEspI,precioNeto,importeBruto,importeNeto,usar,anyoImputacion)
{
this.grupo = grupo
this.id = id
this.cod = cod
this.descr = descr
this.cant = cant
this.uni = uni
this.dest = dest
this.pag = pag
this.pagDen = pagDen
this.finisum = finisum
this.ffinsum = ffinsum
this.obj = obj
this.hayEsp = hayEsp
this.hayEspI = hayEspI
this.esp = esp
this.precio = precio
this.oferta = oferta
this.objetivo = objetivo
this.precio2 = precio2
this.precio3 = precio3
this.cantmax = cantmax
this.adjuntosCargados = false
this.numAdjuntos = numAdjuntos
this.cargado = false
this.ofertaMinima = ofertaMinima
this.proveCod = proveCod
this.proveDen = proveDen
this.mirror = new Array()

this.sinAdjuntos = false
this.adjuntos = new Array()
this.anyaAdjunto = anyaAdjuntoItem
this.cargarAdjuntos = cargarAdjuntosItem
this.coment1 = coment1
this.coment2 = coment2
this.coment3 = coment3
this.obsAdjun = obsAdjun
this.atribs = atribs
//this.costesDescuentos = costesDescuentos
this.precioNeto = precioNeto
this.importeBruto = importeBruto
this.importeNeto = importeNeto
this.usar = usar

this.especs = new Array()
this.anyaEspec = anyaEspecItem
this.cargarEspecificaciones = cargarEspecificacionesItem

this.Aespecs = new Array()
this.anyaAEspecI = anyaAEspecItem


this.atributosOfertados = new Array()
this.anyaAtributoOfertado = anyaAtributoOfertadoItem

this.costesDescuentosOfertados = new Array()
this.anyaCosteDescuentoOfertado = anyaCosteDescuentoOfertadoItem

this.escalados = new Array()
this.anyaEscItem = anyaEscItem
this.estableceEscalados = estableceEscalados
this.anyoImputacion = anyoImputacion

}


function anyaAdjuntoItem(adjunto)
{
var i
i = this.adjuntos.length
this.adjuntos[i]=adjunto
i=arrAdjuntosItem.length
arrAdjuntosItem[i] = new ItemAdjunto(this,adjunto)

}


function cargarAdjuntosItem(target)
{
var features
features = ""

if (this.adjuntos.length==0 && this.sinAdjuntos == false)
	window.open("<%=replace(Request.servervariables("URL"),"item.asp","")%>cargaradjuntos.asp?anyo=" + this.grupo.proceso.anyo + "&gmn1=" + this.grupo.proceso.gmn1 + "&proce=" + this.grupo.proceso.cod + "&grupo=" + this.grupo.cod + "&item=" + this.id + "&ofe=" + this.grupo.proceso.num + "&IdPortal=" + this.grupo.proceso.idPortal ,target,features)
else
	mostrarAdjuntosItem(this)
this.numAdjuntos=0

for (i=0;i < this.adjuntos.length;i++)
	if (this.adjuntos[i].deleted==false)
		this.numAdjuntos++
}


function anyaAtributoOfertadoItem(atribOfertado)
{
var i

i = this.atributosOfertados.length
this.atributosOfertados[i]=atribOfertado


}


function anyaCosteDescuentoOfertadoItem(costeDescuentoOfertado)
{
var i

i = this.costesDescuentosOfertados.length
this.costesDescuentosOfertados[i]=costeDescuentoOfertado


}


function ItemAdjunto (item,adjunto)
{
this.oItem = item
this.oAdjunto = adjunto
}


function cargarEspecificacionesItem(target)
{
var features
features = ""

if (this.cargado == false)
	{
	window.open("<%=replace(Request.servervariables("URL"),"item.asp","")%>cargarespecificaciones.asp?anyo=" + this.grupo.proceso.anyo + "&gmn1=" + this.grupo.proceso.gmn1 + "&proce=" + this.grupo.proceso.cod + "&grupo=" + this.grupo.cod + "&item=" + this.id ,target,features)
	}
else
	{
	mostrarEspecificacionesItem(this)
	}

}


function anyaEspecItem(espec)
{
var i
i = this.especs.length
this.especs[i]=espec
}

function anyaAEspecItem(AespecI)
{
var i
i = this.Aespecs.length
this.Aespecs[i]=AespecI
}


function Unidad (cod, den)
{
this.cod = cod
this.den = den
}



// Añade el escalado al item
function anyaEscItem(esc)
{
var i
i = this.escalados.length
this.escalados[i]=esc

}




// <summary>comprueba cual es el escalado en uso</summary>
// Escalados directos: El escalado que coincida con la cantidad del item. Si no hay ninguno aquel cuya diferencia con la cantidad del item sea menor
// Escalados por rangos: Aquel para el que la cantidad del item este comprendida entre sus rangos. Si no es ninguno el de rango mayor o menor
// en función de si la cantidad es mayor que el rango mayor o menor que el rango menor.
// <revision>LTG 10/02/2012</revision>

function estableceEscalados()
{
    var iesc  
    var dif=0
    var iEscUsar=-1
    var iEscMin=-1
    var iEscMax=-1
    var dblMin=0
    var dblMax=0                  

    for (iesc=0;iesc<this.escalados.length;iesc++)
    {
        this.escalados[iesc].usar = false
		this.escalados[iesc].cant = null

        if (this.cant!=null)
        {
	        if (this.escalados[iesc].fin==null) // Cantidades directas
	        {
		        if (this.escalados[iesc].inicio==this.cant) 
		        {							        			        
                    iEscUsar=iesc  
                    break                  
		        }
		        else
		        {
                    if (dif==0)
                    {
                        iEscUsar=iesc
                        dif=Math.abs(this.cant-this.escalados[iesc].inicio)
                    }
                    else
                    {
			            if (Math.abs(this.cant-this.escalados[iesc].inicio)<dif)
                        {
                            iEscUsar=iesc
                            dif=Math.abs(this.cant-this.escalados[iesc].inicio) 
                        }
                    }
		        }
	        }
	        else // Cantidades por rangos
	        {                               
                if (iEscMin==-1)
                {
                    iEscMin = iesc
                    dblMin = this.escalados[iesc].inicio
                }
                else
                {
                    if (this.escalados[iesc].inicio<dblMin) 
                    {
                        iEscMin = iesc
                        dblMin = this.escalados[iesc].inicio
                    }
                }
                if (this.escalados[iesc].fin>dblMax) 
                {
                    iEscMax = iesc
                    dblMax = this.escalados[iesc].fin
                }

		        if (this.cant >= this.escalados[iesc].inicio && this.cant <= this.escalados[iesc].fin) 
		        {			        
			        iEscUsar=iesc	
                    break		
		        }	
		    }
	    }	
    }
    
   if (iEscUsar>-1)
    {
        this.escalados[iEscUsar].usar = true
		this.escalados[iEscUsar].cant = this.cant		
	}
	else
	{
        //Cantidades por rangos
        if (this.cant < dblMin)
        {
            this.escalados[iEscMin].usar = true
		    this.escalados[iEscMin].cant = this.cant
        }
        else
        {
            if (this.cant > dblMax)
            {
                this.escalados[iEscMax].usar = true
		        this.escalados[iEscMax].cant = this.cant
            }
        }		
    }
}



