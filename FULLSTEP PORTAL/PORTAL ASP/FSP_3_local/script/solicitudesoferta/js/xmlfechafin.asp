﻿

<%
	Response.ContentType="text/xml" 
	Response.Write ("<?xml version='1.0' encoding='UTF-8'?>") 
%>	

<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/colores.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/idioma.asp"-->



<%
''' <summary>
''' comprueba la fecha de fín de la subasta y devuelve cierto en caso de que haya cambiado se llama justo en el momento de fin de 
''' subasta para comprobar que no se ha cambiado la fecha en los últimos 10 segundos.
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cumpoferta.js ; Tiempo máximo: 0,2</remarks>


	Response.Write ("<fullstep>") 

Idioma = Request.Cookies("USU_IDIOMA")
if Idioma="" then
	Idioma = "SPA"
end if

set oRaiz=validarUsuario(Idioma,true,false,0)

CiaComp = clng(oRaiz.Sesion.CiaComp)
anyo = request("anyo")
gmn1 = request("gmn1")
proce = request("proce")
prove = oRaiz.Sesion.CiaCodGs
Codprove = prove

set oProceso = oRaiz.Generar_CProceso()

oProceso.cargarConfiguracion CiaComp, anyo, gmn1, proce, prove
oProceso.cargarProceso CiaComp, Idioma

	response.write ("<anyo>" & anyo & "</anyo>")
	response.write ("<gmn1>" & gmn1 & "</gmn1>")
	response.write ("<proce>" & proce & "</proce>")
	response.write ("<feclimofe>" & JSHora(oProceso.FechaMinimoLimOfertas) & "</feclimofe>")
	Response.Write ("</fullstep>") 

set oProceso = nothing
set oRaiz = nothing

%>