﻿<%@ Language=VBScript %>
<%
oldTimeOut = server.ScriptTimeout
server.ScriptTimeout =40000
%>

<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/idioma.asp"-->
<!--#include file="../../common/colores.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->

<HTML>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HEAD>
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<script src="../../common/formatos.js"></script>
<script SRC="item.asp"></script>
<script src="../../common/ajax.js"></script>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<!--#include file="../../common/fsal_3.asp"-->
</HEAD>
<script>
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo mÃ¡ximo:0</remarks>*/
function init()
{
    if (<%=Application("FSAL")%> == 1)
    {
            Ajax_FSALActualizar3(); //registro de acceso
    }
    var p
    p = parent.frames["fraOfertaClient"]
    p.window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/syncclock.asp","fraOfertaPujas")
}
</script>
<BODY onload="init()" onclick="return window_onclick()">

<%

''' <summary>
''' Cargar la puja de la oferta
''' </summary>
''' <remarks>Llamada desde: js\proceso.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")
if Idioma="" then
	Idioma = "SPA"
end if

dim den

den = devolvertextos(Idioma,94)

set oRaiz=validarUsuario(Idioma,true,false,0)

decimalfmt = Request.Cookies ("USU_DECIMALFMT")

CiaComp = clng(oRaiz.Sesion.CiaComp)
anyo = request("anyo")
gmn1 = request("gmn1")
proce = request("proce")
mon = request("mon")
equiv = Numero(replace(request("eq"),".",decimalfmt))
prove = oRaiz.Sesion.CiaCodGs
ofe = request("ofe")

decimalfmt = Request.Cookies ("USU_DECIMALFMT")
thousanfmt = Request.Cookies ("USU_THOUSANFMT") 
precisionfmt = CINT(Request.Cookies ("USU_PRECISIONFMT") )
datefmt = Request.Cookies ("USU_DATEFMT") 

%>

<script>


var vdecimalfmt 
vdecimalfmt ="<%=decimalfmt%>"
var vthousanfmt 
vthousanfmt ="<%=thousanfmt%>"
var vprecisionfmt
vprecisionfmt = <%=precisionfmt%>
var vdatefmt
vdatefmt = "<%=datefmt%>"

var p
p = parent.frames["fraOfertaClient"]
proc = p.proceso

p.itemsPuja = new Array()

function anyaItemPuja(oItem)
{
var l
l = p.itemsPuja.length

p.itemsPuja[l]= oItem

}

var i
lTotalItems = 0
for (i=0;i<p.proceso.grupos.length ;i++)
	{
	lTotalItems += p.proceso.grupos[i].numItems 
	}

lTotal = lTotalItems + 4
var lP
lP = 1
p.progreso (lP * 100/lTotal)

<%


Set oProceso = oRaiz.Generar_CProceso

oProceso.cargarConfiguracion CiaComp, anyo, gmn1, proce, prove
oProceso.cargarProceso CiaComp, Idioma
set oOferta = oProceso.CargarOfertaProveedor(Idioma,ciacomp,prove)

dim bConsulta
if iif(oProceso.MaximoNumeroOfertas<>0,oProceso.MaximoNumeroOfertas<> oOferta.TotEnviadas,true) then
	bConsulta = false
else
	bConsulta = true
end if
%>
</script>


<script>
lP++
p.progreso (lP * 100/lTotal)


var AnchoMaxTabla

AnchoMaxTabla = 940


//no dependen del grupo

p.wCCodArt = 80
p.wCDenArt = 0  // La denominación de Artículo la haremos lo mas grande que podamos
p.wCCantidad = 60
p.wCUnidad = 52

p.wCPU = 100
p.wCTotalLinea = 100

colspanGrupo = 6

if (p.bAbierta + '' == 'undefined'){
    //No me ha pasado con lo instalado pero en local salta a veces antes este codigo q cumpoferta.js/actualizarTiempoRestante
    //entonces las variables no estan definidas y en consecuencia no sale la posición/proveedor/oferta minima.
    p.actualizarTiempoRestante()
}

//variables
if (p.bAbierta && !p.bSobreCerrado && !p.sinPujas)
{
    p.wCPosicionPuja = 60
    colspanGrupo++
<%if oProceso.verDesdePrimeraPuja = false OR ((oProceso.verDesdePrimeraPuja = true) AND (oProceso.hayOfertaProveedor(CiaComp,Prove)=true)) then %>
    <%if oProceso.VerProveedor then %>
        p.wCProveedor = 160        
        colspanGrupo++
    <%else%>
        p.wCProveedor = 0
    <%end if%>

    <%if oProceso.VerPrecioPuja then %>
        p.wCOfertaMinima = 100
        colspanGrupo++
    <%else%>
        p.wCOfertaMinima = 0
    <%end if%>
<%end if%>
}
else
{
    p.wCPosicionPuja = 0
    p.wCProveedor = 0
    p.wCOfertaMinima = 0
}


p.hCFijo=25 //Alto de cada fila
p.wDesviacion = 4 // Cada columna es cuatro puntos más ancha que la wC correspondiente...
p.hDesviacion = 2
p.wBorde = 2 //Ancho de las separaciones de las columnas
p.wCSeparacion = 10 //Ancho de las columnas de separación

p.CIE =0 // Corrección Bordes 
if (p.document.all) p.CIE = -p.wBorde // IE


p.wCDenArt =  AnchoMaxTabla - (p.wCCodArt+p.wCCantidad+p.wCUnidad+p.wCPosicionPuja+p.wCProveedor+p.wCOfertaMinima+p.wCPU+p.wCTotalLinea) - colspanGrupo * (p.wDesviacion + p.wBorde + p.CIE)



p.hCabeceraGrupo=30
p.hCabeceraGrupoDetalle=30

/*
''' <summary>
''' Crear la estructura de divs donde se pondra el codigo html de la puja
''' </summary>     
''' <remarks>Llamada desde: cargarsubasta.asp ; Tiempo máximo: 0</remarks>*/
function prepararDivsPuja()
{


hAptdo = p.document.getElementById("divApartadoS").style.height
p.hCabecera = p.hCFijo + p.wDesviacion

if (!(p.document.getElementById("divPuja")))
{
//proc.wApArticulo = p.wCCodArt + p.wCDenArt + p.wCProveedor + p.wCOfertaMinima + p.wCPosicionPuja + p.wCPU + p.wCTotalLinea +  colspanGrupo * (p.wDesviacion + p.wBorde + p.CIE )
proc.wApArticulo = AnchoMaxTabla 
<%
'
'divArticuloC        
'+-----------------+ 
'¦                 ¦ 
'¦                 ¦ 
'¦                 ¦ 
'¦                 ¦ 
'¦                 ¦ 
'¦                 ¦ 
'¦                 ¦ 
'+-----------------+ 
'divArticuloScroll   
'+-----------------+ 
'¦divArticulo      ¦ 
'¦+---------------+¦ 
'¦¦               ¦¦ 
'¦¦               ¦¦ 
'¦¦               ¦¦ 
'¦¦               ¦¦ 
'¦¦               ¦¦ 
'¦¦               ¦¦ 
'¦¦               ¦¦ 
'¦+---------------+¦ 
'+-----------------+ 
                   
%>
strDivs  = "			<input type=hidden name=txtTextoPosicion  value='<%= den(18)%>'>"
strDivs += "<div name=divPuja id=divPuja style='position:absolute;top:0px;left:0px;visibility:hidden;BORDER:<%=cBORDEBLOQUE%> 1px solid;'>"
strDivs += "<table name=tblSubasta id=tblSubasta width=100% border=0>"

if (p.proceso.estado>1)
	{
	strDivs += "	<tr height=24px>"
	switch (p.proceso.estado)
		{
		case 1:
			break;
		case 2:
			sClase = "OFESINENVIAR"
			break;
		case 3:
		case 5:
			sClase = "OFEENVIADA"
			break;
		case 4:
			sClase = "OFESINGUARDAR"
			break
		}
		

	strDivs += "<td class=" + sClase +">"
	strDivs += "<div name=divTotalOfertaSubasta id=divTotalOfertaSubasta style='overflow:hidden; ' >" 
	sEstado = p.estadoOferta(p.proceso.estado)
	strDivs += sEstado + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +  (p.linkRecalculo==0?num2str(p.proceso.importe,vdecimalfmt,vthousanfmt,vprecisionfmt):"-") + "&nbsp;" + p.proceso.mon.cod + "&nbsp;&nbsp;&nbsp;&nbsp;<IMG SRC=../images/masinform.gif  WIDTH=11 HEIGHT=14 name=imgMasInform>"
	if(p.linkRecalculo==1)
                strDivs +="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(null)' onclick=\"linkRecalculoOferta()\"><%=JSText(den(24))%></a>" //JVS Idioma Recalcular totales de oferta
	strDivs += "</div>"
	strDivs += "</td>"
	strDivs += "</tr>"
	}


strDivs += "	<tr height=24px>"
strDivs += "		<td class=cabApartado style='text-align:left'>"
strDivs += "		    <table border=0 cellspacin=0 cellpadding=0>"
strDivs += "		        <tr>"
strDivs += "		            <td class=cabApartado style='text-align:left'>"

strDivs += "                        <div id='denproceso' style='overflow:hidden;height=14 pt' title='<%=JSText(den(6)) & "&nbsp;" & anyo & "/" & gmn1 & "/" & proce %>&nbsp;" + p.proceso.den + "'>"

strDivs += "                        <%=JSText(den(6)) & "&nbsp;" & anyo & "/" & gmn1 & "/" & proce %>&nbsp;" + p.proceso.den

strDivs += "                        </div>"

strDivs += "		            </td>"
strDivs += "		            <td class=cabApartado style='text-align:left'>"
strDivs += "                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
strDivs += "		            </td>"

// Información de proveedor ganador e importe en caso de tenerse que mostrar
strDivs += "		            <td class=cabecera style='text-align:left;padding-left:0px' valign=middle>"
strDivs += "		            <div id=DivGanadorProceso name=DivGanadorProceso style='overflow:hidden;height=10 pt"
var strDisplay="none"
<%if oProceso.modo = 1  then 'Modo Proceso %>
    <%if oProceso.verDesdePrimeraPuja = false OR ((oProceso.verDesdePrimeraPuja = true) AND (oProceso.hayOfertaProveedor(CiaComp,Prove)=true)) then %>
        <% if oProceso.verproveganador or oProceso.verprecioganador  then %>        
            strDisplay = "block"   
        <%end if%>
    <%end if%>
<%end if%>
strDivs += ";display:" + strDisplay + ";'>"
strDivs += "		            </div>"
strDivs += "		            </div>"
strDivs += "		            </td>"

strDivs += "		        </tr>"
strDivs += "		    </table>"
strDivs += "		</td>"

strDivs += "	</tr>"
strDivs += "</table>"

strDivs+= "	<table width=100% border=0>"
strDivs+= "		<tr>"
strDivs+= "			<td width=43% ><div name=divCronometroPuja id=divCronometroPuja></div>" 
strDivs+= "			</td>"

//Reglas de la subasta
strDivs += "       <td width=14% style='text-align:right'><a href=# onclick=\"ReglasSubasta();return false;\"><%=JSText(den(27))%></a></td>"
strDivs += "       <td width=3% style='text-align:left'><img src=\"images/reglassubasta.gif\" border=0 style=\"cursor:pointer\" onclick=\"ReglasSubasta();\"></td>"

strDivs += "       <td  width=40% style='text-align:left'>"
strDivs += "          <div id=DivBajadaProceso name=DivBajadaProceso>"
strDivs += "  		     <table cellspacing=0 cellpadding=0 border=0><tr><td><a href=# onclick='mostrarSlider(\"\");'><font color=black><b><%=JSText(den(26))%>&nbsp;&nbsp;</b></font></a></td><td><img id ='BajarPrecios' src='images/reducir-precios.gif' border=0 onclick='mostrarSlider(\"\")' style=\"cursor:pointer\" alt=\"<%=JSText(den(26))%> \" ></td></tr></table>"
strDivs += "          </div>"
strDivs += "       </td>"

strDivs+= "		</tr>" 
strDivs+= " </table>"

strDivs += "<div name=divApartadoSPuja id=divApartadoSPuja style='position:absolute;left:0px;"
if (p.proceso.estado==1)
	strDivs += "top:30px;"
else
	strDivs += "top:56px;" //Posicionamiento del div de ARTICULOS !!!!!!!!!!!!
strDivs += "'>"

		strDivs += "<DIV NAME=divArticuloPujaC id=divArticuloPujaC style='position:absolute;top:0px;left:0px;'></div>"

		strDivs += "<DIV NAME=divArticuloPujaScroll id=divArticuloPujaScroll onScroll='scrollFijasPuja()' style='position:absolute;top:" + p.hCabecera + "px;left:0px;clip:rect(0px auto auto 0px);overflow:scroll;z-index:200;'>"


		for (i=0;i<proc.grupos.length;i++)
			{
			if (i==0)
				vTop=0
			else                
				//vTop+=proc.grupos[i-1].numItems*(p.hCFijo + p.wBorde + p.hDesviacion + p.CIE ) + p.hCabeceraGrupo + p.hCabeceraGrupoDetalle + p.wBorde + p.CIE + p.hDesviacion + 10
                vTop+=proc.grupos[i-1].numItems*(p.hCFijo + p.wBorde + p.hDesviacion  ) + p.hCabeceraGrupo + p.hCabeceraGrupoDetalle + p.wBorde + p.hDesviacion + 10
			strDivs += "<DIV NAME=divArticuloPuja" + proc.grupos[i].cod + " id=divArticuloPuja" + proc.grupos[i].cod + "  style='position:absolute;top:" + vTop + "px;left:0px;'></div>"
			}

		strDivs += "</div>"
	strDivs += "</div>"
strDivs += "</div>"
p.document.getElementById("divItems").innerHTML = p.document.getElementById("divItems").innerHTML + strDivs



p.mostrarCronometros()

}

p.document.getElementById("divPuja").style.overflow="hidden"
p.document.getElementById("divPuja").style.top = p.document.getElementById("divApartado").style.top
p.document.getElementById("divPuja").style.left = p.document.getElementById("divApartado").style.left
p.document.getElementById("divPuja").style.height = p.document.getElementById("divApartado").style.height
p.document.getElementById("divPuja").style.width = p.document.getElementById("divApartado").style.width
p.document.getElementById("divPuja").style.clip = p.document.getElementById("divApartadoS").style.clip

		
p.document.getElementById("divArticuloPujaScroll").style.width =parseFloat(p.document.getElementById("divApartado").style.width) - 1
for (i=0;i<proc.grupos.length;i++)
	{
	proc.grupos[i].cont=0
	p.document.getElementById("divArticuloPuja" + proc.grupos[i].cod).style.width = proc.wApArticulo
	p.document.getElementById("divArticuloPuja" + proc.grupos[i].cod).style.height = proc.grupos[i].numItems*(p.hCFijo + 3) + p.hCabeceraGrupo + p.hCabeceraGrupoDetalle + 13
	}
p.document.getElementById("divArticuloPujaScroll").style.height=parseFloat(p.document.getElementById("divApartadoS").style.height) - p.hCabecera + p.hCabeceraGrupoDetalle - 30
p.document.getElementById("divPuja").style.visibility="visible"
p.document.getElementById("divApartadoS").style.visibility="hidden"
p.document.getElementById("divApartado").style.visibility="hidden"
}


function prepararCabeceraPuja(codMonedaGrupo,codgrupo)
{
var strCabArticulo

strCabArticulo = ""
strCabArticulo += "		<TD class='cabecera'>"
strCabArticulo += "			<div style='width:" + p.wCCodArt + "px;height:" + p.hCFijo + "px;overflow:hidden;'><%=JSText(den(1))%></div>"
strCabArticulo += "		</TD>"
strCabArticulo += "		<TD class='cabecera'>"
strCabArticulo += "			<div style='width:" + p.wCDenArt + "px;height:" + p.hCFijo + "px;overflow:hidden;'><%=JSText(den(2))%></div>"
strCabArticulo += "		</TD>"

strCabArticulo += "		<TD class='cabecera'>"
strCabArticulo += "			<div style='width:" + p.wCCantidad + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(10))%></nobr></div>"
strCabArticulo += "		</TD>"
strCabArticulo += "		<TD class='cabecera'>"
strCabArticulo += "			<div style='width:" + p.wCUnidad + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(9))%></nobr></div>"
strCabArticulo += "		</TD>"


if (p.bAbierta  && !p.bSobreCerrado  && !p.sinPujas)
{
    if (p.proceso.modo==3)
    {
       strCabArticulo += "		<TD class='POSICIONPUJAITEM'>"
    }
    else
    {
        strCabArticulo += "		<TD class='cabecera'>"
    }

strCabArticulo += "			<div style='width:" + p.wCPosicionPuja + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(20))%></nobr></div>"
strCabArticulo += "		</TD>"
}


<%
if oProceso.verDesdePrimeraPuja = false OR ((oProceso.verDesdePrimeraPuja = true) AND (oProceso.hayOfertaProveedor(CiaComp,Prove)=true)) then %>
    <%if oProceso.VerProveedor then %>

        if (p.bAbierta && !p.bSobreCerrado && !p.sinPujas)
        {
	    strCabArticulo += "		<TD class='cabecera'>"
	    strCabArticulo += "			<div style='width:" + p.wCProveedor + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(23))%></nobr></div>"
	    strCabArticulo += "		</TD>"
        }
    <%end if%>

    <%if oProceso.VerPrecioPuja then %>
        if (p.bAbierta  && !p.bSobreCerrado && !p.sinPujas)
        {

	    strCabArticulo += "		<TD class='cabecera'>"
	    strCabArticulo += "			<div style='width:" + p.wCOfertaMinima + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(25))%></nobr></div>"
	    strCabArticulo += "		</TD>"
        }
    <%end if%>
<%end if%>

strCabArticulo += "		<TD class='cabecera'>"
strCabArticulo += "			<div style='width:" + p.wCPU + "px;height:" + p.hCFijo + "px;overflow:hidden;'><table border=0 cellspacing=0 cellpadding=0 width=100% ><tr><td class='cabecera'><nobr><%=JSText(den(3))%></nobr></td><td align=left>"


strCabArticulo += "			     <table cellspacing=0 cellpadding=0 border=0><tr><td><img id ='BajarPrecios" + codgrupo+ "' src=images/reducir-precios.gif border=0 onclick='mostrarSlider(\"" + codgrupo + "\")' style=\"cursor:pointer\" alt=\"<%=JSText(den(26))%> \" ></td></tr></table>"
strCabArticulo += "			</td></tr></table></div>"
strCabArticulo += "		</TD>"
strCabArticulo += "		<TD class='cabecera' textAlign='rigth'>"
strCabArticulo += "			<div style='width:" + p.wCTotalLinea + "px;height:" + p.hCFijo + "px;overflow:hidden;'><table heigth='100%' cellspacing='0' cellpading='0'><tr><td class='cabecera' vAlign='top'><%=JSText(den(22))%></td><td class='cabecera' vAlign='bottom'>" + codMonedaGrupo + "</td></tr></table></div>"
strCabArticulo += "		</TD>"

strArticuloC="<TABLE cellspacing='0' cellpading='0' width='100%' border=0><TR>" + strCabArticulo + "</TR></TABLE>"

return strArticuloC
}



<%

oProceso.cargarItems CiaComp,mon,prove,ofe,Idioma
oProceso.oferta.cargarPrecios CiaComp

%>
</script>
<%'response.write "<p>Items cargados:" & now() & "</P>"%>
<script>

lP++
p.progreso (lP * 100/lTotal)




function insertarCelda (TR,sClase,sHTML,vAlign, iRowSpan)
	{
	var oTD
	
	oTD = TR.insertCell(-1)
	oTD.className=sClase

	if (vAlign)
		oTD.style.verticalAlign=vAlign
	
	if (iRowSpan)
		oTD.rowSpan = iRowSpan

    oTD.innerHTML = oTD.innerHTML + sHTML

	}



function contador(grupo)
{
var j

for (j=0;j<proc.grupos.length && proc.grupos[j].cod!=grupo;j++)
	{
	
	}
proc.grupos[j].cont++
return (proc.grupos[j].cont)
}




prepararDivsPuja()

//prepararCabeceraPuja()



lP++
p.progreso (lP * 100/lTotal)




var strDetArticulo


lMaxFilas = lTotalItems

strDetArticulo = ""

var x

var vGrp
fila='filaImpar'

for (i=0;i<proc.grupos.length;i++)
	{
	vGrp = proc.grupos[i]
	strTablaGrupo = "<TABLE name='tblArticuloPuja" + proc.grupos[i].cod + "' id='tblArticuloPuja" + proc.grupos[i].cod + "' border=0 width=100%  >"
	strTablaGrupo += "<tr>"
	strTablaGrupo += "<td class=cabecera colspan=" + colspanGrupo + " width='100%'>"
	//strTablaGrupo += "			<div name=divCabeceraPreciosSubasta_" + vGrp.cod + " id=divCabeceraPreciosSubasta_" + vGrp.cod + " style='width:" + (p.wCCodArt + p.wCDenArt + p.wCProveedor + p.wCOfertaMinima   + p.wCPU + p.wCTotalLinea) + ";height:" + p.hCabeceraGrupo + ";overflow:hidden;'>"
	strTablaGrupo += "			<div name=divCabeceraPreciosSubasta_" + vGrp.cod + " id=divCabeceraPreciosSubasta_" + vGrp.cod + " style='width:100%';height:" + p.hCabeceraGrupo + "px;overflow:hidden;'>"
	strTablaGrupo += "<a class=ayuda href='javascript:void(null)' onfocus='return OcultarDivCostesDescItem()' onclick='ocultarPujaGrupo(\"" + proc.grupos[i].cod + "\")' title = '" + proc.grupos[i].den + "'><IMG style='position:absolute;top:0px;left:0px;visibility:visible;'name=imgGrupoA" + proc.grupos[i].cod + " border=0 SRC=images/arribab.gif><IMG style='position:absolute;top:0px;left:0px;visibility:hidden;' name=imgGrupoB" + proc.grupos[i].cod + " border=0 SRC=images/abajob.gif>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
	
	strTablaGrupo += proc.grupos[i].cod + "&nbsp;&nbsp;<%=den(13)%> " + num2str(vGrp.importe,vdecimalfmt,vthousanfmt,vprecisionfmt) + "&nbsp;<%=mon%>"

	strTablaGrupo += "</a>"
	strTablaGrupo += "</div>"
	strTablaGrupo += "</td>"
	strTablaGrupo += "</tr>"
	
	//Nueva Cabecera Detalle (Codigo, denominacion...
	strTablaGrupo += "<tr>"
	strTablaGrupo += "<td class=cabecera colspan=" + colspanGrupo + " width='100%'>"
	//strTablaGrupo += "<DIV NAME=divArticuloPujaC_" + vGrp.cod + " id=divArticuloPujaC_" + vGrp.cod + " style='width:" + (p.wCCodArt + p.wCDenArt+p.wCCantidad+p.wCUnidad+ + p.wCProveedor + p.wCOfertaMinima + p.wCPU + p.wCTotalLinea+p.wCPosicionPuja) + ";height:" + p.hCabeceraGrupoDetalle + ";overflow:hidden;'>"
	strTablaGrupo += "<DIV NAME=divArticuloPujaC_" + vGrp.cod + " id=divArticuloPujaC_" + vGrp.cod + " style='width:100%;height:" + p.hCabeceraGrupoDetalle + "px;overflow:hidden;'>"
	
	strTablaGrupo += prepararCabeceraPuja ('<%=mon%>', proc.grupos[i].cod)
	strTablaGrupo += "</div>"
	strTablaGrupo += "</td>"
	strTablaGrupo += "</tr>"
	
	strTablaGrupo += "</TABLE>"
    p.document.getElementById("divArticuloPuja" + proc.grupos[i].cod).innerHTML = p.document.getElementById("divArticuloPuja" + proc.grupos[i].cod).innerHTML + strTablaGrupo
	}

j=0


/*
''' <summary>
''' Mostrar por pantalla la caja de precio unitario para el item
''' </summary>
''' <param name="sName">Nombre del elelmento</param>
''' <param name="lSize">Tamaño</param>
''' <param name="lLenMax">Longitud máxima</param>
''' <param name="dValue">valor</param>
''' <param name="valorMin">valor mínimo</param>
''' <param name="valorMax">valor máximo</param>
''' <param name="mas">nuevos atributos para el elemento. Por ejemplo: pu->1,2,3 (precio ofe 1/2/3) oldvalue-> valor en bbdd</param>
''' <param name="sFuncValidacion">Función de validación</param>
''' <param name="sFuncFoco">Función de Foco</param>
''' <param name="errMsg"></param>
''' <param name="errLimites"></param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cargaritems.asp; Tiempo máximo: 0</remarks>
config.asp
''' <autor>JVS</autor>
*/
function inputNumeroItem (sName, lSize, lLenMax, dValue, valorMin, valorMax, mas, sFuncValidacion, sFuncFoco, errMsg, errLimites,Bloqueado)
{
var s
if (!sFuncValidacion)
 sFuncValidacion = "validarNumero("
 if (Bloqueado != 1) 
 {
    s = "<input " + (mas?mas:"") + " language='javascript' class='numero' style='width:" + lSize + "px' type=text name=" + sName + " id =" + sName + " maxlength=" + lLenMax + " onkeypress='return mascaraNumero(this,vdecimalfmt,vthousanfmt,event);' onchange='return " + sFuncValidacion + "this,vdecimalfmt,vthousanfmt,100," + valorMin + "," + valorMax + ",\"" + errMsg + "\",\"" + errLimites + "\")' onfocus='return " + sFuncFoco + ")' value='" + num2str(dValue,vdecimalfmt,vthousanfmt,100) + "' >"
 }
 else
 {
    s = num2str(dValue,vdecimalfmt,vthousanfmt,100)
 }
return s
}


/*
''' <summary>
''' funcion que dibuja cada item de la subasta
''' </summary>
''' <param name="vGrupo">Grupo del proceso</param>
''' <param name="vIDItem">Id del item</param>  
''' <param name="vArt">Articulo</param>  
''' <param name="vDescr">Descripcion del articulo</param>  
''' <param name="vCant">Cantidad del item</param>
''' <param name="vUni">Unidad del articulo</param>
''' <param name="vUniDen">Denominacion de la unidad del articulo</param>
''' <param name="vPrec">Precio</param>                
''' <param name="vMin">Valor minimo</param>    
''' <param name="vPrec">Precio</param>
''' <param name="vProve">Proveedor</param>                
''' <param name="vProveDen">Denominacion del proveedor</param>                
''' <param name="vFilaS">Estilo de la fila</param>
''' <param name="vPosicionPuja">Posicion de la puja</param>                                                                
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cargaritems,cargarsubasta; Tiempo máximo:0</remarks>
*/ 

function dibujarItem(vGrupo, vIDItem, vArt, vDescr, vCant, vUni, vUniDen, vPrec, vMin, vProve, vProveDen, vFilaS,vPosicionPuja,importeNeto,precioNeto,Bloqueado)

{	
	oItem = new p.Item (vGrupo, vIDItem,vArt,vDescr,vCant,new p.Unidad(vUni,vUniDen),null,null,null,null,null,null,null,null,vPrec,null,null,null,null,vMin,vProve,vProveDen)	
	
	oTR = p.document.getElementById("tblArticuloPuja" + vGrupo).insertRow(-1)

	strDetArticulo = "			<input type=hidden name=txtPujaIDItem_" + vGrupo + "_" + contador(vGrupo) + " value='" + vIDItem + "'>"
	strDetArticulo += "			<input type=hidden name=txtPujaClaseFila_" + vIDItem + "  value='" +  fila + "'>"
	
	strDetArticulo += "			<div style='width:" + p.wCCodArt + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"
	strDetArticulo += oItem.cod
	strDetArticulo += "			</div>"


	insertarCelda(oTR,fila,strDetArticulo)


	strDetArticulo = "			<div style='width:" + p.wCDenArt + "px;height:" + p.hCFijo + "px;overflow:hidden;' >"
	strDetArticulo += oItem.descr
	strDetArticulo += "			</div>"
	
	oTD = oTR.insertCell(-1)
	oTD.className=fila
    oTD.title = oItem.descr
    oTD.innerHTML = oTD.innerHTML + strDetArticulo


	strDetArticulo = "			<div style='width:" + p.wCCantidad + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"
	strDetArticulo += num2str(oItem.cant,vdecimalfmt,vthousanfmt,vprecisionfmt) + '&nbsp;'
	strDetArticulo += "			<input type=hidden name=txtPujaCantidad_" + vGrupo + "_" + oItem.id + " value='" + num2str(oItem.cant,vdecimalfmt,vthousanfmt,100) + "'>"
	strDetArticulo += "			</div>"
	oTD = oTR.insertCell(-1)
	oTD.className=fila
    oTD.style.textAlign='right'
    oTD.innerHTML = oTD.innerHTML + strDetArticulo


	strDetArticulo = "			<div style='width:" + p.wCUnidad + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"
	strDetArticulo += "<table width=100% style='margin-top:-3px'><tr><td width=100%><a class=popUp href='javascript:void(null)' title='" + oItem.uni.den + "' onfocus='return OcultarDivCostesDescItem()' onclick='DenominacionUni(\"" + oItem.uni.cod + "\")'>" +  oItem.uni.cod +  "</A></td>"
	strDetArticulo += "<td><a class=popUp href='javascript:void(null)' title='" + oItem.uni.den + "' onfocus='return OcultarDivCostesDescItem()' onclick='DenominacionUni(\"" + oItem.uni.cod + "\")'><IMG border=0 SRC='../images/masinform.gif'></a></td></tr></table>"		
	strDetArticulo += "			</div>"

	oTD = oTR.insertCell(-1)
	oTD.className=fila
    oTD.innerHTML = oTD.innerHTML + strDetArticulo

    if (p.bAbierta  && !p.bSobreCerrado && !p.sinPujas)
    {	
	    //Posicion de la puja
	    strDetArticulo = "<div class='" + vFilaS + "' name=divPujaPosicion" + vIDItem + " id=divPujaPosicion" + vIDItem + " style='width:" + p.wCPosicionPuja  + "px;height:" + p.hCFijo + "px;overflow:hidden;align:center'>"
	    strDetArticulo +="<b>"
	    if (vPosicionPuja != null)
        {
    	        strDetArticulo += vPosicionPuja
        }
	    strDetArticulo += "</b></div>"	


	    oTD = oTR.insertCell(-1)
	    oTD.name = 'tdPujaPosicion_' + vIDItem
	    oTD.id = 'tdPujaPosicion_' + vIDItem
	    oTD.style.textAlign='center'
	    oTD.style.verticalAlign='middle'
	    oTD.className=fila
        oTD.innerHTML = oTD.innerHTML + strDetArticulo
    }

    <%if oProceso.verDesdePrimeraPuja = false OR ((oProceso.verDesdePrimeraPuja = true) AND (oProceso.hayOfertaProveedor(CiaComp,Prove)=true)) then %>
	    <%if oProceso.verProveedor then%>		


        if (p.bAbierta  && !p.bSobreCerrado && !p.sinPujas)
        {	
            //Proveedor
		    strDetArticulo = "<div class='" + vFilaS + "' name=divPujaProve" + vIDItem + " id=divPujaProve" + vIDItem + " style='width:" + p.wCProveedor  + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"


		    <%if oProceso.verDetallePujas AND oProceso.verPrecioPuja then%>
			    strDetArticulo += "<a  class='popUp ' name=ancO" + vIDItem + " id=ancO" + vIDItem + " href='javascript:void(null)' modo='3' sGrupo = '" + vGrupo + "' lItem = " + vIDItem + " sArt= '" +  vArt + "' sDescr = '' onfocus='return OcultarDivCostesDescItem()' onclick = 'verDetallePujas(this)' ><b>" + oItem.proveDen +"</a></b>"
		    <%else%>
			    strDetArticulo += "<b>" + oItem.proveDen + "</b>"
		    <%end if%>
		    strDetArticulo += "</div>"	
		    oTD = oTR.insertCell(-1)
		    oTD.className=fila
            oTD.innerHTML = oTD.innerHTML + strDetArticulo
        }
		
	    <%end if%>
	    <%if oProceso.verPrecioPuja then%>		

        if (p.bAbierta  && !p.bSobreCerrado && !p.sinPujas)
        {	
            //MEJOR OFERTA            
		    strDetArticulo = "<div name=divPujaOfeMin" + vIDItem + " id=divPujaOfeMin" + vIDItem + " style='width:" + p.wCOfertaMinima  + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"
		    <%if oProceso.verDetallePujas then%>
			    strDetArticulo += "<a  class='popUp ' name=ancO" + vIDItem + " id=ancO" + vIDItem + " href='javascript:void(null)' modo='3'  sGrupo = '" + vGrupo + "' lItem = " + vIDItem + " sArt= '" +  vArt + "' sDescr = '' onfocus='return OcultarDivCostesDescItem()' onclick = 'verDetallePujas(this)' ><b>" + num2str(oItem.ofertaMinima,vdecimalfmt,vthousanfmt,vprecisionfmt) +"</a></b>&nbsp;"
		    <%else%>
			    strDetArticulo += "<b>" + num2str(oItem.ofertaMinima,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>&nbsp;"
		    <%end if%>
		    strDetArticulo += "</div>"	
		    oTD = oTR.insertCell(-1)
		    oTD.className=fila
		    oTD.style.textAlign='right'
            oTD.innerHTML = oTD.innerHTML + strDetArticulo
        }
	    <%end if%>
    <%end if %>

	strDetArticulo = "			<div style='width:" + p.wCPU + "px;height:" + p.hCFijo + "px;overflow:hidden;z-index:1000;'>"
	sNamePU = "txtPujaPU_" + vIDItem
    sNamePUBruto = "txtPujaPUBruto_" + vIDItem
	
	sNamePUNormal = "txtPU_" + vGrupo +  "_" + vIDItem
	//if (p.document.forms["frmOfertaAEnviar"].item[sNamePUNormal])
    if (p.document.getElementById(sNamePUNormal))
		{
		var l
		var k
		for (l=0;l<p.proceso.grupos.length && p.proceso.grupos[l].cod!=vGrupo;l++)
			{}
		for (k=0;k<p.proceso.grupos[l].items.length && p.proceso.grupos[l].items[k].id != oItem.id;k++)
			{}
		oItem.precio=p.proceso.grupos[l].items[k].precio
		oItem.mirror = new Array("proceso.grupos[" + l + "].items[" + k + "]")
		arrLength = p.proceso.grupos[l].items[k].mirror.length
		p.proceso.grupos[l].items[k].mirror[arrLength] = "itemsPuja[" + p.itemsPuja.length + "]"

		}
	arrLength = oItem.mirror.length
	oItem.mirror[arrLength] = "itemsPuja[" + p.itemsPuja.length + "]"
	<%if bConsulta then%>
		strDetArticulo += num2str(precioNeto,vdecimalfmt,vthousanfmt,vprecisionfmt)
	<%else%>
		strDetArticulo += inputNumeroItem (sNamePU,p.wCPU+(2*p.CIE),20,precioNeto,null,null,"cant=" + oItem.cant + " pu=4 grupo='" + vGrupo + "' itemid=" + oItem.id + " oldValue=" + oItem.precioNeto,"validarNumeroB(\"" + sNamePUNormal + "\",itemsPuja[" + p.itemsPuja.length + "],", "MostrarDivCostesDescItem(" + oItem.id + ",\"" + vGrupo + "\", \"txtPujaPU_\"","<%=JSText(den(14))%>\\n<%=JSText(den(15))%> " + vdecimalfmt + "\\n<%=JSText(den(16))%> " + vthousanfmt,null,Bloqueado)
        strDetArticulo += "<input type=hidden name=" + sNamePUBruto + " id=" + sNamePUBruto  + " value='" + num2str(oItem.precio,vdecimalfmt, vthousanfmt,100) + "'>"
	<%end if%>
	strDetArticulo += "			</div>"

	insertarCelda(oTR,fila,strDetArticulo)
	strDetOferta = "			<div name=divTotalLinea4_" + oItem.id + " id=divTotalLinea4_" + oItem.id + " style='width:" + p.wCTotalLinea + "px;height:" + p.hCFijo + "px;overflow:hidden;z-index:1000;'>"

	if (oItem.precio!=null)
		{
//		strDetOferta += num2str(oItem.cant,vdecimalfmt,vthousanfmt,vprecisionfmt) 
//		strDetOferta += "&nbsp;<a class=popUp href='javascript:void(null)' title='" + oItem.uni.den + "'>" +  oItem.uni.cod +  "</A>"
//		strDetOferta += "<br>"
//		strDetOferta += "<b>" + num2str(oItem.precio * oItem.cant,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"
		strDetOferta += "<b>" + num2str(importeNeto,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>&nbsp;"
		}
	strDetOferta += "			</div>"

	insertarCelda(oTR,fila + " numero",strDetOferta)
	
	if (fila=="filaImpar")
		fila="filaPar"
	else
		fila="filaImpar"
	j++
	anyaItemPuja(oItem)
}



<%
cont = 0
for each oItem in oProceso.items

	if cont=10 then
		%>
		</script>
		<script>
		<%
		cont = 0
	else
		cont = cont + 1
	end if
	%>


	filaS = ""

	lP++
	p.progreso (lP * 100/lTotal)


	//dibujarItem("<%=oItem.grupo.codigo%>", <%=oItem.id%>,"<%=JSText(oItem.ArticuloCod)%>", "<%=JSText(oItem.Descr)%>", <%=JSNum(oItem.cantidad)%>,"<%=JSText(oItem.uniCod)%>", "<%=JSText(oItem.uniDen)%>", <%=JSNum(oItem.precioItem.PrecioOferta / oOferta.cambio * equiv)%>, <%=JSNum(oItem.minprecio / oOferta.cambio * equiv)%>, <%if oProceso.verProveedor then%>"<%=JSText(oItem.MinProveCod)%>","<%=JSText(oItem.MinProveDen)%>"<%else%>null,null<%end if%>, filaS,<%=JSNum(oItem.PosicionPuja)%>, <%=JSNum(oItem.PrecioItem.PrecioNeto)%>, <%=JSNum(oItem.PrecioItem.ImporteBruto / oOferta.cambio * equiv)%>, <%=JSNum(oItem.PrecioItem.ImporteNeto / oOferta.cambio * equiv)%>)
	<%

 next

 
 'JVS Carga de los datos de Costes / Descuentos
oProceso.CargarCostesDescuentos CiaComp


for each oGrupo in oProceso.grupos
    oGrupo.cargarGrupo Idioma,ciaComp,, anyo, gmn1, proce,prove,ofe,mon,oProceso.SolicitarCantMax
    oGrupo.cargarItems Idioma,CiaComp,mon,prove,ofe,false
    oGrupo.cargarAtributos CiaComp,3
    
    set oOfeGrupo = oRaiz.Generar_COfertaGrupo()
    set oOfeGrupo.Oferta = oOferta
    set oOfeGrupo.Grupo = oGrupo
    oOfeGrupo.cargarPrecios ciacomp,mon
    %>

    
       for (i=0;i<p.proceso.grupos.length && p.proceso.grupos[i].cod != '<%=oGrupo.Codigo%>';i++)
		    {
		    }
	oObj = p.proceso.grupos[i]
    oObj.costesDescuentosItems.length = 0

    <%
    if not (oGrupo.CostesDescItem is nothing) then 
        for each oCosDes in oGrupo.CostesDescItem
            with oCosDes%>
	        costedesc = new p.Atributo(<%=.id%>, <%=.paid%>, '<%=JSText(.cod)%>', '<%=JSText(.den)%>', <%=.intro%>, <%=.tipo%>, <%=JSNum(.valor)%>, <%=JSBool(.obligatorio)%>, <%=JSNum(.valorMin)%>, <%=JSNum(.valorMax)%>, null, p.proceso,'<%=JSText(.operacion)%>',<%=.aplicar %>,<%=.ambito %>,null,null,'<%=JSText(.Alcance)%>')
	        oObj.anyaCosteDescuentoItems(costedesc)
            <%end with
        next
    end if 

    cont = 0
    for each oItem in oGrupo.items


       
        %>
        dibujarItem("<%=oItem.grupo.codigo%>", <%=oItem.id%>,"<%=JSText(oItem.ArticuloCod)%>", "<%=JSText(oItem.Descr)%>", <%=JSNum(oItem.cantidad)%>,"<%=JSText(oItem.uniCod)%>", "<%=JSText(oItem.uniDen)%>", <%=JSNum(oItem.precioItem.PrecioOferta / oOferta.cambio * equiv)%>, <%=JSNum(oItem.minprecio / oOferta.cambio * equiv)%>, <%if oProceso.verDesdePrimeraPuja = false OR ((oProceso.verDesdePrimeraPuja = true) AND (oProceso.hayOfertaProveedor(CiaComp,Prove)=true)) then %><%if oProceso.verProveedor then%>"<%=JSText(oItem.MinProveCod)%>","<%=JSText(oItem.MinProveDen)%>"<%else%>null,null<%end if%><%else%>null,null<%end if%>, filaS,<%=JSNum(oItem.PosicionPuja)%>, <%=JSNum(oItem.PrecioItem.ImporteNeto / oOferta.cambio * equiv)%>, <%=JSNum(oItem.PrecioItem.PrecioNeto)%>,<%=JSNum(oItem.grupo.Bloqueado)%>)
        for (i=0;i<p.itemsPuja.length && p.itemsPuja[i].id != '<%=oItem.id%>';i++)
        {
        }
        vItem = p.itemsPuja[i]
               <%
        'JVS carga de los items
       	if cont=10 then
		    %>
		    </script>
		    <script>
		    <%
		    cont = 0
	    else
		    cont = cont + 1
	    end if
	    %>


	    filaS = ""

	    lP++
	    p.progreso (lP * 100/lTotal)

        vItem.precioNeto = <%=JSNum(oItem.PrecioItem.PrecioNeto)%>
        vItem.importeBruto = <%=JSNum(oItem.PrecioItem.ImporteBruto / oOferta.cambio * equiv)%>
        vItem.importeNeto = <%=JSNum(oItem.PrecioItem.ImporteNeto / oOferta.cambio * equiv)%>

        vItem.costesDescuentosOfertados.length = 0

	    <% 
        
        'JVS carga de los valores de los costes / descuentos de ámbito item
         if not (oGrupo.CostesDescItem is nothing) then
            for each  oCosDes in oGrupo.CostesDescItem
            with oCosDes
                 set oAtribOfe = oItem.PrecioItem.CostesDescOfertados
                if .Ambito=3 then%>
                     costedescvalor = new p.Atributo(<%=.id%>, <%=.paid%>, '<%=JSText(.cod)%>', '<%=JSText(.den)%>', <%=.intro%>, <%=.tipo%>,<%
                    if not oAtribOfe is nothing then
                        if not oAtribOfe.Item(cstr(.PAID)) is nothing then
                            if (.operacion = "+" or  .operacion = "-") then
        	                    vValor = oAtribOfe.Item(cstr(.PAID)).valor / oOferta.cambio * equiv
                            else
	                            vValor = oAtribOfe.Item(cstr(.PAID)).valor 
                            end if
                            vImporte = oAtribOfe.Item(cstr(.PAID)).ImporteParcial
                        else
                            vValor = null
                            vImporte = null
                        end if
                    else
                        vValor = null
                        vImporte = null
                    end if
                    response.write  JSNum(vValor) 
                    response.write  "," + JSBool(.obligatorio)
                    response.write "," & JSNum(.valorMin) &  "," & JSNum(.valorMax) 
                    if .intro = 1 then
        	            str=",["
	                    for each oValor in .Valores
    		                if str<>",[" then
        			            str = str & ","
            	            end if
                            if (.operacion = "+" or  .operacion = "-") then
			                    vValor = oValor.Valor / oProceso.Cambio * equiv
        		            else
		        	            vValor = oValor.Valor 
                            end if
                            str = str & JSNum(vValor)
                        next
                        response.write str & "],p.proceso,'" & JSText(.operacion) & "'," & JSNum(.aplicar) & ", null"
                        response.write ", " &  JSNum(vImporte) & ")"
	                else
    		            response.write ",null,p.proceso,'" & JSText(.operacion) & "',"  & JSNum(.aplicar) & ", null"
                        response.write ", " &  JSNum(vImporte) & ")"
            	    end if
                    %>
                    
    	            vItem.anyaCosteDescuentoOfertado(costedescvalor)
                    <%
                end if
            end with
            next
        end if
    next
next


%>



<%if oProceso.modo = 1  then 'Modo Proceso %>
p.MostrarGanadorProceso('<%=oProceso.proveGanador(CiaComp)%>',<%=JSNum(oProceso.importeGanador(CiaComp) * (oOferta.Cambio / oProceso.Cambio))%>)
<%end if %>






p.proceso.subastaCargada=true
p.MostrarPosicionPuja()
//p.recalcularTotales()

<%if oProceso.modo = 2  then 'Modo Grupo %>


<%for each oGrupo in oProceso.grupos %>

    p.MostrarGanadorGrupo('<%=ogrupo.codigo%>','<%=oGrupo.proveGanador(CiaComp)%>',<%=JSNum(oGrupo.importeGanador(CiaComp) * (oOferta.Cambio / oProceso.Cambio))%>)
<%next %>
<%end if %>



p.progreso (100)

p.bGuardando = false
p.desbloquear()

if (p.bAbierta  && !p.bSobreCerrado && !p.sinPujas)
{
     if (p.document.getElementById("Ganadora")) p.document.getElementById("Ganadora").style.display = "block"
}
p.resize()


function window_onclick() {

}

            </script>
<%

set oProceso = nothing
set oRaiz = nothing

server.ScriptTimeout =oldTimeOut

%>
</BODY>
<!--#include file="../../common/fsal_2.asp"-->
</HTML>
