﻿<%@ Language=VBScript %>
<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->

<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<script src="../../common/ajax.js"></script>
<script src="../../common/formatos.js"></script>
<!--#include file="../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
function init() { 
    if (<%=Application("FSAL")%> == 1){
        Ajax_FSALActualizar3(); //registro de acceso
    }
}
</script>
</HEAD>
<BODY onload="init()">

<%
''' <summary>
''' Cargar un grupo de la oferta
''' </summary>
''' <remarks>Llamada desde: js\grupo.asp ; Tiempo máximo: 0,2</remarks>


Idioma = Request.Cookies("USU_IDIOMA")
if Idioma="" then
	Idioma = "SPA"
end if

set oRaiz=validarUsuario(Idioma,true,false,0)


CiaComp = clng(oRaiz.Sesion.CiaComp)
CodProve = oRaiz.Sesion.CiaCodGs
anyo = request("anyo")
gmn1 = request("gmn1")
proce = request("proce")
grupo = request("grupo")

decimalfmt = Request.Cookies ("USU_DECIMALFMT")
thousanfmt = Request.Cookies ("USU_THOUSANFMT") 
precisionfmt = CINT(Request.Cookies ("USU_PRECISIONFMT") )
datefmt = Request.Cookies ("USU_DATEFMT") 

ofe = request("ofe")
if ofe="0" then
	ofe = null
end if

mon = request("mon")

prove = oRaiz.Sesion.CiaCodGs
equiv = Numero(replace(request("eq"),".", decimalfmt))

set oProceso = oRaiz.Generar_CProceso()

oProceso.cargarConfiguracion CiaComp, anyo, gmn1, proce, CodProve
set oOferta = oRaiz.Generar_COferta()
if request("IdPortal")="null" then
	oOferta.IDPortal = null
else
	oOferta.IDPortal = request("IdPortal")
end if

set oGrupo = oProceso.Grupos.item(grupo)

set oProceso.oferta = oOferta
set oGrupo.proceso = oProceso
oGrupo.cargarGrupo Idioma,ciaComp, , , , ,prove,ofe,mon,oProceso.SolicitarCantMax

'JORGE Cargamos en vb los atributos de especificaciones del grupo
oGrupo.CargarAEspecificacionesG CiaComp, oGrupo.id, anyo, gmn1, proce 


if request("defEspecs")= "-1" then
	oGrupo.cargarEspecificaciones ciaComp
end if

	
%>

<script>
var p

p = parent.frames["fraOfertaClient"]


var vGrp

var i

for (i=0;i<p.proceso.grupos.length && p.proceso.grupos[i].cod != "<%=grupo%>" ;i++)
	{
	}
vGrp = p.proceso.grupos[i]

<%
with oGrupo

%>
	vGrp.desc = '<%=JSText(.descripcion)%>'
	
	<%if not .Destino is nothing then%>
		vGrp.dest = new p.Destino('<%=JSText(.destCod)%>','<%=JSText(.Destino.Den)%>')
	<%end if%>

	vGrp.pag = '<%=JSText(.pagCod & " - " & .pagDen)%>'
	if (vGrp.items.length==0 && p.proceso.subastaCargada==false)
    {
		vGrp.importe = <%=JSNum(.importeTotal)%> 
        vGrp.importeBruto = <%=JSNum(.importeBruto)%> 
    }
	vGrp.finisum = <%=JSDate(.FechaInicioSuministro)%>
	vGrp.ffinsum = <%=JSDate(.FechaFinSuministro)%>
	vGrp.esp = '<%=JSText(.Esp)%>'
	vGrp.cfg.especs = <%=JSBool(request("defEspecs")="-1" and .EspAdj ) %>
    vGrp.posicionPuja = <%=JSNum(.posicionPuja)%> 


	//JORGE carga las espeficicaciones del grupo
	<%
	if not .Especificaciones is nothing then
	for each oEsp in .Especificaciones%>
		vGrp.anyaEspec(new p.Especificacion(<%=oEsp.id%>,'<%=JSText(oEsp.nombre)%>','<%=JSText(oEsp.Comentario)%>',<%=JSNum(oEsp.datasize)%>,vGrp))
	<%next
	end if%>
	
	//JORGE carga los atributos de especificacion del grupo en javascript
	<%
	if not .AEspecificacionesG is nothing then
	for each oAEspG in .AEspecificacionesG%>
		
		AespP = new p.AtributoEspecificacion(0,
			<%=JSNum(oAEspG.id)%>,		
			'<%=JSText(oAEspG.cod)%>',				
			'<%=JSText(oAEspG.descr)%>',				
		'<%=JSText(oAEspG.Desc)%>',
		<%=JSNum(oAEspG.Atributo)%>,
		<%=JSNum(oAEspG.Interno)%>,
		<%=JSNum(oAEspG.Pedido)%>,
		<%=JSNum(oAEspG.Orden)%>,
		<%=JSNum(oAEspG.Valor_num)%>,
		'<%=JSText(oAEspG.Valor_text)%>',
		<%=JSDate(oAEspG.Valor_fec)%>,
		<%=JSBool(oAEspG.Valor_bool)%>,vGrp)
		
		vGrp.anyaAEspecG(AespP)
		
	<%next
	end if%>

	vGrp.cargado = true
<%
end with
%>
vGrp.cargarDatos()
p.desbloquear()
</script>
<%

set oGrupo.especificaciones = nothing
set oGrupo = nothing
set oRaiz = nothing

%>

<P>&nbsp;</P>

</BODY>
<!--#include file="../../common/fsal_2.asp"-->
</HTML>
