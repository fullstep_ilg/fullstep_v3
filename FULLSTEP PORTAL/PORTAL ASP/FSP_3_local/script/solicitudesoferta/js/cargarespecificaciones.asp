﻿<%@ Language=VBScript %>
<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->

<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<script language="JavaScript" src="../../common/ajax.js"></script>
<script src="../../common/formatos.js"></script>
<!--#include file="../../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
function init() { 
    if (<%=Application("FSAL")%> == 1){
        Ajax_FSALActualizar3(); //registro de acceso
    }
}
</script>
</HEAD>
<BODY onload="init()">

<%
''' <summary>
''' Cargar las especificaciones de la oferta a nivel de item
''' </summary>
''' <remarks>Llamada desde: js\item.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")
if Idioma="" then
	Idioma = "SPA"
end if

set oRaiz=validarUsuario(Idioma,true,false,0)


CiaComp = clng(oRaiz.Sesion.CiaComp)
anyo = request("anyo")
gmn1 = request("gmn1")
proce = request("proce")
grupo = request("grupo")
item = request("item")


set oItem = oRaiz.Generar_Citem()

'JORGE aqui carga la especificaciones del item (VB)
oItem.cargarEspecificaciones ciaComp, anyo, gmn1, proce, item
'JORGE aqui cargamos los atributos de especificaciones del item (VB)
oItem.cargarAEspecificacionesI ciaComp, anyo, gmn1, proce, item

%>

<script>
var p

p = parent.frames["fraOfertaClient"]



var i
for (i=0;i<p.proceso.grupos.length && p.proceso.grupos[i].cod != '<%=grupo%>';i++)
	{
	}
oObj = p.proceso.grupos[i]
oObjProcesoGrupo = p.proceso.grupos[i]
	
for (i=0;i<oObj.items.length && oObj.items[i].id != '<%=item%>';i++)
	{
	}
oObj = oObj.items[i]


//JORGE a un item (javascritp) le añadimos sus espeficicaciones
<%
with oItem
	if not .Especificaciones is nothing then
	for each oEsp in .Especificaciones%>
		oObj.anyaEspec(new p.Especificacion(<%=oEsp.id%>,'<%=JSText(oEsp.nombre)%>','<%=JSText(oEsp.Comentario)%>',<%=JSNum(oEsp.datasize)%>,oObj))
	<%next
	end if%>

	oObj.cargado = true
<%
end with
%>

<%
with oItem
	if not .AEspecificacionesI is nothing then
	for each oAEspI in .AEspecificacionesI%>
		oObj.anyaAEspecI(new p.AtributoEspecificacion(<%=item%>,
			<%=JSNum(oAEspI.id)%>,		
			'<%=JSText(oAEspI.cod)%>',				
			'<%=JSText(oAEspI.descr)%>',		
			'<%=JSText(oAEspI.Desc)%>',
			<%=JSNum(oAEspI.Atributo)%>,
			<%=JSNum(oAEspI.Interno)%>,
			<%=JSNum(oAEspI.Pedido)%>,
			<%=JSNum(oAEspI.Orden)%>,
			<%=JSNum(oAEspI.Valor_num)%>,
			'<%=JSText(oAEspI.Valor_text)%>',
			<%=JSDate(oAEspI.Valor_fec)%>,
			<%=JSBool(oAEspI.Valor_bool)%>,
			oObjProcesoGrupo))
	<%next
	end if%>
	//oObj.cargado = true
<%
end with
%>


//JORGE aqui dibuja las especificaciones ?????????
oObj.cargarEspecificaciones()

p.desbloquear()
</script>

<%
set oItem.especificaciones = nothing
set oItem = nothing
set oRaiz = nothing
%>

<frame type=hidden onload="init()"> </frame>

</BODY>
<!--#include file="../../common/fsal_2.asp"-->
</HTML>
