﻿

<%
	Response.ContentType="text/xml" 
	Response.Write ("<?xml version='1.0' encoding='UTF-8'?>") 
%>	

<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/colores.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/idioma.asp"-->



<%
''' <summary>
''' comprueba que haya precios para alguno de los Ítems de un grupo y devuelve cierto en caso de que los haya
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cumpoferta.js ; Tiempo máximo: 0,2</remarks>


	Response.Write ("<fullstep>") 

Idioma = Request.Cookies("USU_IDIOMA")
if Idioma="" then
	Idioma = "SPA"
end if

set oRaiz=validarUsuario(Idioma,true,false,0)

CiaComp = clng(oRaiz.Sesion.CiaComp)
anyo = request("anyo")
codGrupo = request("grupo")
gmn1 = request("gmn1")
proce = request("proce")
idPortal = request("idPortal")
prove = oRaiz.Sesion.CiaCodGs
Codprove = prove

set oProceso = oRaiz.Generar_CProceso()

oProceso.cargarConfiguracion CiaComp, anyo, gmn1, proce, prove
oProceso.cargarProceso CiaComp, Idioma
bGrupoEncontrado = false
for each oGrupo in oProceso.Grupos
    if oGrupo.Codigo = codGrupo then
        bGrupoEncontrado = true
        exit for
    end if
next
if bGrupoEncontrado then 
    hayPrec = oGrupo.HayPrecios(CiaComp, prove, idPortal)
end if


	response.write ("<anyo>" & anyo & "</anyo>")
	response.write ("<gmn1>" & gmn1 & "</gmn1>")
	response.write ("<proce>" & proce & "</proce>")
	response.write ("<grupo>" & codGrupo & "</grupo>")
    response.write ("<hayPrecio>" & hayPrec & "</hayPrecio>")
	Response.Write ("</fullstep>") 

set oGrupo = nothing
set oProceso = nothing
set oRaiz = nothing

%>