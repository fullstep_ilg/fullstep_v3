﻿<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/idioma.asp"-->
<%
''' <summary>
''' Crea los objetos de configuracion (con permisos de administrador)
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cargarresultadopuja.asp    solicitudesoferta\cumpoferta.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")
set oRaiz=validarUsuario(Idioma,false,false,0)


dim den

den = devolverTextos(Idioma,95)

litSobre = "Sobre"
%>

function CfgProceso(dest,pag,fecSum,especs,cantMax,altPrec,adjunOfe,adjunGrp,adjunItem,subasta,subVerProve,subVerDet,cambiarMon,mostrarFinSum,costesDescuentos)
{
this.dest        = dest
this.pag         = pag
this.fecSum      = fecSum
this.especs      = especs
this.cantMax     = cantMax
this.altPrec     = altPrec
this.adjunOfe    = adjunOfe
this.adjunGrp    = adjunGrp
this.adjunItem   = adjunItem
this.subasta     = subasta
this.subVerProve = subVerProve
this.subVerDet   = subVerDet
this.cambiarMon = cambiarMon
this.mostrarFinSum = mostrarFinSum
this.costesDescuentos = costesDescuentos

}






function CfgGrupo(dest,pag,fecSum,especs,atribs,costesDescuentos)
{
this.dest   = dest
this.pag    = pag
this.fecSum = fecSum
this.especs = especs
this.atribs = atribs
this.costesDescuentos = costesDescuentos
}

function obtener(codigo)
{
return true
}

function mostrarConfigProceso(proc)
{
AtribImg = new Image()
AtribImg.src="images/be font.gif"
CosteDescuentoImg = new Image()
CosteDescuentoImg.src="images/be font.gif"
AdjuntosImg = new Image()
AdjuntosImg.src="images/clip07.gif"
GrupoImg = new Image()
GrupoImg.src="images/grupo.gif"
SobreCerradoImg = new Image()
SobreCerradoImg.src="images/sobrecerrado.gif"
SobreAbiertoImg = new Image()
SobreAbiertoImg.src="images/sobreabierto.gif"
ItemImg = new Image()
ItemImg.src="images/items.gif"
EspecImg = new Image()
EspecImg.src="images/Especificaciones.gif"
OfertaImg = new Image()
OfertaImg.src="images/Copland Documents.gif"
RellenarImg = new Image()
RellenarImg.src="images/rellenar.gif"
ModoSubastaImg= new Image()
ModoSubastaImg.src = "images/modosubasta.gif"


ramaRoot=new Rama ('Root','Root',proc.anyo + "/" + proc.gmn1 + "/" + proc.cod,false,false,OfertaImg,'DarkGoldenrod',false,true,null,null,'datosProceso',!proceso.cfg.subasta)

/*
if(proceso.cfg.costesDescuentos)
    {
    rama4=new Rama ('4','04','<%=JSText(den(8)) %>',true,true,RellenarImg,'DarkGoldenrod',false,true,true,null,'datosOferta',false) //JVS idioma Costes/Descuentos
    ramaRoot.add(rama4)
    }
*/

var i

for (j=0;j<proc.sobres.length;j++)
	{
	y = new Rama ("SOBRE_" + proc.sobres[j].cod , proc.sobres[j].cod , '<%=litSobre%>' + proc.sobres[j].cod ,false,false,(proc.sobres[j].estado==1?SobreAbiertoImg:SobreCerradoImg),'DarkGoldenrod',false,true,true,null,'datosSobre',false)
	eval("ramaSOBRE_" + proc.sobres[j].cod + " = y")
	eval("ramaRoot.add(ramaSOBRE_" + proc.sobres[j].cod  + ")")
	eval("ramaSOBRE_" + proc.sobres[j].cod + ".hojasCargadas = true")
	eval("ramaSOBRE_" + proc.sobres[j].cod + ".expanded = true")

	sobre = proc.sobres[j]
	for (i=0;i<sobre.grupos.length;i++)
		{
		x = new Rama ("GR_" + sobre.grupos[i].cod , sobre.grupos[i].cod , sobre.grupos[i].cod + " - " + sobre.grupos[i].den ,false,false,GrupoImg,'DarkGoldenrod',false,true,true,null,'datosGrupo',false)
		
		eval("ramaGR_" + sobre.grupos[i].cod + " = x")
		eval("ramaSOBRE_" + proc.sobres[j].cod + ".add(ramaGR_" + sobre.grupos[i].cod  + ")")
		eval("ramaGR_" + sobre.grupos[i].cod + ".hojasCargadas = true")
		eval("ramaGR_" + sobre.grupos[i].cod + ".expanded = true")

		if (sobre.grupos[i].numItems>0)
			{
			eval("ramaGR_" + sobre.grupos[i].cod + "_1 = new Rama ('GR_" + sobre.grupos[i].cod + "_1','" + sobre.grupos[i].cod + "','<%=JSText(den(3))%>',true,true,RellenarImg,'green',false,true,true,null,'introducirPrecios',false)")
			eval("ramaGR_" + sobre.grupos[i].cod + ".add(ramaGR_" + sobre.grupos[i].cod + "_1)")
			}

		if (sobre.grupos[i].cfg.atribs==true)
			{
			eval("ramaGR_" + sobre.grupos[i].cod + "_2 = new Rama ('GR_" + sobre.grupos[i].cod + "_2','" + sobre.grupos[i].cod + "','<%=JSText(den(9))%>',true,true,RellenarImg,'green',false,true,true,null,'introducirAtributos',false)")
			eval("ramaGR_" + sobre.grupos[i].cod + ".add(ramaGR_" + sobre.grupos[i].cod + "_2)")
			}
		if (sobre.grupos[i].cfg.costesDescuentos==true)
			{
			eval("ramaGR_" + sobre.grupos[i].cod + "_4 = new Rama ('GR_" + sobre.grupos[i].cod + "_4','" + sobre.grupos[i].cod + "','<%=JSText(den(8))%>',true,true,RellenarImg,'green',false,true,true,null,'introducirCostesDescuentos',false)") //JVS idioma Costes/Descuentos
			eval("ramaGR_" + sobre.grupos[i].cod + ".add(ramaGR_" + sobre.grupos[i].cod + "_4)")
			}			
		if (proc.cfg.adjunGrp==true)
			{
			eval("ramaGR_" + sobre.grupos[i].cod + "_3 = new Rama ('GR_" + sobre.grupos[i].cod + "_3','" + sobre.grupos[i].cod + "','<%=JSText(den(2))%>',true,true,RellenarImg,'green',false,true,true,null,'introducirAdjuntos',false)")
			eval("ramaGR_" + sobre.grupos[i].cod + ".add(ramaGR_" + sobre.grupos[i].cod + "_3)")
			}
		}				

	
	
	}


ramaRoot.write(null)
ramaRoot.expcont()

imgContraer= new Image()
imgContraer.src = "../images/contraer.gif"
imgDesplegar= new Image()
imgDesplegar.src = "../images/desplegar.gif"


}


<%

%>

