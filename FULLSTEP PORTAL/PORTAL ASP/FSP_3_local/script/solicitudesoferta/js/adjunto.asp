﻿<!--#include file="../../common/acceso.asp"-->
<%
''' <summary>
''' Crea el objeto Adjunto
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cumpoferta.asp    solicitudesoferta\cargarresultadopuja.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")
set oRaiz=validarUsuario(Idioma,false,false,0)

%>

function Adjunto(ID,nombre,comentario,size,idPortal,nuevo)
{
this.ID=ID
this.nombre=nombre
this.original=nombre
this.comentario=comentario
this.size=size
this.idPortal = idPortal

this.deleted=false

if (nuevo)
	this.modified =false
else
	this.modified=true


this.download = downloadAdjun
}


//''' <summary>
//''' Descarga un adjunto
//''' </summary>
//''' <param name="target">donde cargar la pagina</param> 
//''' <remarks>Llamada desde: function Adjunto ; Tiempo máximo: 0</remarks>
function downloadAdjun(target)
{

var re
var re2
re=/MSIE 5.5/
re2=/Q299618/
	
re3 = /SP1/

if (navigator.userAgent.search(re)!=-1 && navigator.appMinorVersion.search(re2)==-1 && navigator.appMinorVersion.search(re3)!=-1)
	{
	window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/mspatch.asp","_blank","top=100,left=150,width=300,height=250,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
	window.close()
	}
else
	{
	window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/downloadadjunto.asp?download=1&IDAdjun=" + this.ID + "&IDAdjunPortal=" + this.idPortal + "&nombre=" + this.nombre + "&datasize=" + this.size, target)
	}
}


