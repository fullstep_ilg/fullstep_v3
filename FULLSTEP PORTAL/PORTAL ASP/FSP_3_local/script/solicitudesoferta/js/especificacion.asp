﻿<!--#include file="../../common/acceso.asp"-->
<%
''' <summary>
''' Crea el objeto Especificacion
''' </summary>
''' <remarks>Llamada desde:  solicitudesoferta\cargarresultadopuja.asp    solicitudesoferta\cumpoferta.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")
set oRaiz=validarUsuario(Idioma,false,false,0)

%>

function Especificacion(cod, nombre, comentario, size, obj)
{

//Propiedades de la Especificacion

this.cod = cod
this.nombre = nombre
this.comentario = comentario
this.size = size

this.parent = obj

//Metodos de la Especificacion

this.download = downloadEspecificacion
}


//''' <summary>
//''' Descarga una Especificacio
//''' </summary>
//''' <param name="target">donde descargar</param> 
//''' <remarks>Llamada desde: function Especificacion ; Tiempo máximo: 0</remarks>
function downloadEspecificacion(target)
{

var re
var re2
re=/MSIE 5.5/
re2=/Q299618/
	
re3 = /SP1/

if (navigator.userAgent.search(re)!=-1 && navigator.appMinorVersion.search(re2)==-1 && navigator.appMinorVersion.search(re3)!=-1)
	{
	window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/mspatch.asp","_blank","top=100,left=150,width=300,height=250,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
	window.close()
	}
else
	{

	if (this.parent.grupo)
		window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/downloadespec.asp?download=1&anyo=" + this.parent.grupo.proceso.anyo + "&gmn1=" + this.parent.grupo.proceso.gmn1  + "&proce=" + this.parent.grupo.proceso.cod + "&Grupo=" + this.parent.grupo.id + "&item=" + this.parent.id + "&espec=" + this.cod, target)
	else
		if (this.parent.proceso)
			window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/downloadespec.asp?download=1&anyo=" + this.parent.proceso.anyo + "&gmn1=" + this.parent.proceso.gmn1  + "&proce=" + this.parent.proceso.cod + "&Grupo=" + this.parent.id + "&espec=" + this.cod, target)
		else
			window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/downloadespec.asp?download=1&anyo=" + this.parent.anyo + "&gmn1=" + this.parent.gmn1  + "&proce=" + this.parent.cod + "&espec=" + this.cod,target)
	}
}



