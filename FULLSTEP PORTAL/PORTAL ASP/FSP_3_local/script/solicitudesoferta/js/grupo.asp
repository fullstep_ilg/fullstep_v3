﻿<!--#include file="../../common/acceso.asp"-->
<%
''' <summary>
''' Crea el objeto Grupo
''' </summary>
''' <remarks>Llamada desde:  solicitudesoferta\cargarresultadopuja.asp    solicitudesoferta\cumpoferta.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")
set oRaiz=validarUsuario(Idioma,false,false,0)

%>
function Grupo(proceso, cod, den, desc, dest, pag, finisum, ffinsum, esp, numItems, cfg, sobre,importe,obsAdjun,posicionPuja,idGrupo,importeBruto,hayEscalados,Bloqueado){
    this.proceso = proceso
    this.sobre = sobre
    this.id = idGrupo
    this.cod = cod
    this.den = den
    this.desc = desc
    this.dest = dest
    this.pag = pag
    this.finisum = finisum
    this.ffinsum = ffinsum
    this.obsAdjun = obsAdjun
    this.esp = esp
    this.cfg = cfg
    this.cargado = false
    this.itemsCargados = false
    this.itemsDibujados = false
    this.numItems = numItems
    this.colspanArticulo = null
    this.colspanDatosItem = null
    this.colspanPrecios = null
    this.colspanMasPrecios = null
    this.colspanAtributos = null
    this.colspanCostesDescuentos = null
    this.lFilasMostradas = null
    this.sinAtributos = false
    this.importe = importe
    this.posicionPuja = posicionPuja
    this.importeBruto = importeBruto
    this.hayEscalados = hayEscalados
    this.Bloqueado = Bloqueado
    this.wApArticulo = null
    this.wApDatosItem = null
    this.wApSubasta = null
    this.wApproceso = null
    this.wApMasPrecios = null
    this.wApAtributos = null
    this.wApCostesDescuentos = null

    //Colecciones del grupo
    this.items = new Array()
    this.atribs = new Array()
    this.atribsItems = new Array()
    this.costesDescuentos = new Array()
    this.costesDescuentosItems = new Array()
    this.especs = new Array()
    this.Aespecs = new Array()
    this.sinAdjuntos = false
    this.adjuntosCargados = false
    this.adjuntos = new Array()
    this.valores = new Array()
    this.escalados = new Array()

    //Metodos del grupo
    this.cargarDatos = cargarDatosGrupo
    this.yaCargado = yaCargadoGrupo
    this.cargarItems = cargarItemsGrupo
    this.anyaItem = anyaItemGrupo
    this.mostrarItems = mostrarItemsGrupo
    this.cargarAtribs = cargarAtribsGrupo
    this.anyaAtrib = anyaAtribGrupo
    this.anyaAtribItems = anyaAtribItemsGrupo
    this.anyaCosteDescuento = anyaCosteDescuentoGrupo
    this.anyaCosteDescuentoItems = anyaCosteDescuentoItemsGrupo
    this.cargarEspecs = cargarEspecsGrupo
    this.anyaEspec = anyaEspecGrupo
    this.anyaAEspecG = anyaAEspecGrupo
    this.anyaAdjunto = anyaAdjuntoGrupo
    this.cargarAdjuntos = cargarAdjuntosGrupo
    this.mostrarCostesDesc = mostrarCostesDescGrupo
    this.anyaEscGrupo = anyaEscGrupo
}

function cargarDatosGrupo(target){
    var features
    features = ""
    if (this.cargado==false)
	    window.open("<%=replace(Request.servervariables("URL"),"grupo.asp","")%>cargargrupo.asp?anyo=" + this.proceso.anyo + "&gmn1=" + this.proceso.gmn1 + "&proce=" + this.proceso.cod + "&grupo=" + this.cod + "&defEspecs=" + ((this.cfg.especs) ? "-1" : "0") + "&ofe=" + this.proceso.num  + "&mon=" + this.proceso.mon.cod  + "&eq=" + this.proceso.mon.equiv  + "&IdPortal=" + this.proceso.idPortal ,target,features)
    else{
	    mostrarGrupo(this)
	    desbloquear()
    }
}

/*
function cargarDatosGrupo(target){
	mostrarGrupo(this)
	desbloquear()
}
*/

function yaCargadoGrupo(){
    this.cargado=true
}

function cargarItemsGrupo(SoloCargar,target,callbackFunction){
    var features
    features = ""
    
    if (!this.itemsCargados||!this.itemsDibujados) 
        window.open("<%=replace(Request.servervariables("URL"),"grupo.asp","")%>cargaritems.asp?anyo=" + this.proceso.anyo + "&gmn1=" + this.proceso.gmn1 + "&proce=" + this.proceso.cod + "&grupo=" + this.cod + "&ofe=" + this.proceso.num  + 
                    "&mon=" + this.proceso.mon.cod  + "&eq=" + this.proceso.mon.equiv + "&IdPortal=" + this.proceso.idPortal + "&SoloCargar=" + SoloCargar + "&ItemsCargados=" + this.itemsCargados + "&callbackFunction=" + callbackFunction,target,features);       
    else{
	    mostrarItemsGrupo(this)
	    desbloquear()
	}
}

function anyaItemGrupo(item){
    var i
    i=this.items.length
    this.items[i]=item
}

function mostrarItemsGrupo(){}

function cargarAtribsGrupo(target){
    var features
    features = ""
    if (this.atribs.length==0 && this.sinAtributos == false)
	    window.open("<%=replace(Request.servervariables("URL"),"grupo.asp","")%>cargaratributos.asp?anyo=" + this.proceso.anyo + "&gmn1=" + this.proceso.gmn1 + "&proce=" + this.proceso.cod + "&grupo=" + this.cod + "&ofe=" + this.proceso.num  + "&mon=" + this.proceso.mon.cod  + "&eq=" + this.proceso.mon.equiv  + "&IdPortal=" + this.proceso.idPortal + "&IDGrupo=" + this.id ,target,features)
    else{
	    mostrarAtributosGrupo(this)
	    desbloquear()
	}
}

function anyaAtribGrupo(atrib){
    var i
    i = this.atribs.length
    this.atribs[i]=atrib
}

function anyaAtribItemsGrupo(atrib){
    var i
    i = this.atribsItems.length
    this.atribsItems[i]=atrib
}

function anyaCosteDescuentoGrupo(costeDescuento){
    var i
    i = this.costesDescuentos.length
    this.costesDescuentos[i]=costeDescuento
}

function anyaCosteDescuentoItemsGrupo(costeDescuento){
    var i
    i = this.costesDescuentosItems.length
    this.costesDescuentosItems[i]=costeDescuento
}

//'''<summary>Descarga un especificaciones de un Grupo</summary>
//'''<param name="target">donde cargar la pagina</param>
//'''<remarks>Llamada desde: function Grupo ; Tiempo máximo: 0</remarks>
function cargarEspecsGrupo(target){
    var features
    features = ""
    window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/especs.asp?proce=" + this.cod,target,features)
}

function anyaEspecGrupo(espec){
    var i
    i = this.especs.length
    this.especs[i]=espec
}

function anyaAEspecGrupo(AespecG){
    var i
    i = this.Aespecs.length
    this.Aespecs[i]=AespecG
}

function anyaAdjuntoGrupo(adjunto){
    var i
    i = this.adjuntos.length
    this.adjuntos[i]=adjunto
}

//'''<summary>Mostrar los adjuntos de un grupo</summary>
//'''<param name="target">donde se va a mostrar</param>
//'''<remarks>Llamada desde: cumpOferta.asp; Tiempo máximo:0,1</remarks>
function cargarAdjuntosGrupo(target){
    var features
    features = ""
    if (this.adjuntos.length==0 && this.sinAdjuntos == false)
	    window.open("<%=replace(Request.servervariables("URL"),"grupo.asp","")%>cargaradjuntos.asp?anyo=" + this.proceso.anyo + "&gmn1=" + this.proceso.gmn1 + "&proce=" + this.proceso.cod + "&grupo=" + this.cod + "&idgrupo=" + this.id + "&ofe=" + this.proceso.num  + "&IdPortal=" + this.proceso.idPortal ,target,features)
    else{
	    mostrarAdjuntosGrupo(this)
	    desbloquear()
	}
}

//'''<summary>Mostrar los costes / descuentos de un grupo</summary>
//'''<param name="target">donde se va a mostrar</param>
//'''<remarks>Llamada desde: cumpOferta.asp; Tiempo máximo:0,1</remarks>
function mostrarCostesDescGrupo(target){
	mostrarFormCostesDescuentosAmbitoGrupo(this.cod,2)
	desbloquear()
}

//'''<summary>Guarda los datos del coste / descuento modificado con menor número de orden de aplicación dentro del proceso (para recálculo)</summary>
//'''<param name="costeDescRec">coste / descuento modificado</param>
//'''<remarks>Llamada desde: cumpOferta.asp; Tiempo máximo:0,1</remarks>
//'''<author>JVS</author>
function guardarCosteDescRecalculo(costeDescRec){
    this.costeDescRecalculo = costeDescRec
}

// Añade el escalado al grupo
function anyaEscGrupo(esc){
    var i
    i = this.escalados.length
    this.escalados[i]=esc
}

