﻿<%@ Language=VBScript %>
<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/colores.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/idioma.asp"-->
<!--#include file="../../common/fsal_1.asp"-->

<HTML>
<%
''' <summary>
''' Cargar la oferta minima del proceso
''' </summary>
''' <remarks>Llamada desde: js\proceso.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")
if Idioma="" then
	Idioma = "SPA"
end if
set oRaiz=validarUsuario(Idioma,true,false,0)

CiaComp = clng(oRaiz.Sesion.CiaComp)
anyo = request("anyo")
gmn1 = request("gmn1")
proce = request("proce")
mon = request("mon")
equiv = Numero(request("eq"))
prove = oRaiz.Sesion.CiaCodGs
bPujando = request("bPujando")
Codprove = prove

set oProceso = oRaiz.Generar_CProceso()

oProceso.cargarConfiguracion CiaComp, anyo, gmn1, proce, prove
oProceso.cargarProceso CiaComp, Idioma

'Método que actualica el registro de conexión en-linea.

oProceso.AnotarEventoEntrada CiaComp,Prove



if bPujando then


set oGrupo = oRaiz.Generar_CGrupo()

oProceso.anyo = anyo
oProceso.gmn1Cod = gmn1
oProceso.cod = proce

set oRS = oProceso.CargarOfertasMinimas(CiaComp,mon,prove)

set oRS2 = oProceso.CargarGruposSubasta(CiaComp)

PosicionProceso = oProceso.cargarPosicionProceso(CiaComp, prove)

decimalfmt = Request.Cookies ("USU_DECIMALFMT")
thousanfmt = Request.Cookies ("USU_THOUSANFMT") 
precisionfmt = CINT(Request.Cookies ("USU_PRECISIONFMT") )
datefmt = Request.Cookies ("USU_DATEFMT") 

dim den
den = devolverTextos(Idioma,129)

%>

<script src="../../common/formatos.js"></script>

<script>

    function UTCtoClient(FechaUTC) {
    //Convierte una fecha en formato UTC a la fecha en la franja horaria del navegador
        miFecha = new Date()
        tzo = miFecha.getTimezoneOffset()
        var fechaCliente
        fechaCliente = new Date(FechaUTC.getFullYear(), FechaUTC.getMonth(), FechaUTC.getDate(), FechaUTC.getHours(), FechaUTC.getMinutes() - tzo, FechaUTC.getSeconds())
        return fechaCliente
    }


var vdecimalfmt 
vdecimalfmt ="<%=decimalfmt%>"
var vthousanfmt 
vthousanfmt ="<%=thousanfmt%>"
var vprecisionfmt
vprecisionfmt = <%=precisionfmt%>
var vdatefmt
vdatefmt = "<%=datefmt%>"

var p


p = parent.frames["fraOfertaClient"]
var textoPosicion = "<%=JSText(den(1))%>"
if (p.document.forms["frmOfertaAEnviar"])
    
	//if (p.document.forms["frmOfertaAEnviar"].item["textoPosicion"])
		//textoPosicion = p.document.forms["frmOfertaAEnviar"].item["textoPosicion"].value

    if (p.document.getElementById("textoPosicion"))
	    textoPosicion = p.document.getElementById("textoPosicion").value
	
	
function ponerOferta(vProve, vPrecio, vIDItem, vGrupo, vArt, vDescr, sSuperada,vPosicionPuja)
{



if (p.document.getElementById("divProve" + vIDItem))
{

    <%if oProceso.verDetallePujas AND oProceso.verPrecioPuja then%>
	    p.document.getElementById("divProve" + vIDItem).innerHTML = "<a class='popUp " + sSuperada + "'  href='javascript:void(null)' modo='3' sGrupo = '" + vGrupo + "' lItem = " + vIDItem + " sArt= '" +  vArt + "' sDescr = '" + vDescr + "' onclick='verDetallePujas(this)'>" + vProve + "</a>"
    <%else%>
	    p.document.getElementById("divProve" + vIDItem).innerHTML = vProve
    <%end if%>
}


//Posicion Puja
if (p.document.getElementById("divPujaPosicion" + vIDItem)) {
	strPosicion = "<b>"
	if (vPosicionPuja != null)
		strPosicion += vPosicionPuja
	clase=p.document.forms["frmOfertaAEnviar"].elements["txtPujaClaseFila_" + vIDItem].value


	strPosicion += "</b>"

	p.document.getElementById("divPujaPosicion" + vIDItem).innerHTML = strPosicion
	p.document.getElementById("divPujaPosicion" + vIDItem).parentNode.className =clase	
	}

if (p.document.getElementById("divPujaProve" + vIDItem)) {
	

	<%if oProceso.verDetallePujas AND oProceso.VerPrecioPuja then%>
		p.document.getElementById("divPujaProve" + vIDItem).innerHTML = "<a class='popUp '  href='javascript:void(null)' modo='3' sGrupo = '" + vGrupo + "' lItem = " + vIDItem + " sArt= '" +  vArt + "' sDescr = '" + vDescr + "' onclick='verDetallePujas(this)'><b>" + vProve + "</b></a>"
	<%else%>
		p.document.getElementById("divPujaProve" + vIDItem).innerHTML = "<b>" + vProve + "</b>"
	<%end if%>
	p.document.getElementById("divPujaProve" + vIDItem).parentNode.className =p.document.forms["frmOfertaAEnviar"].elements["txtPujaClaseFila_" + vIDItem].value
}


if (p.document.getElementById("divOfeMin" + vIDItem))
	{
	<%if oProceso.verDetallePujas then%>
		p.document.getElementById("divOfeMin" + vIDItem).innerHTML = "<a class='popUp " + sSuperada + "'  href='javascript:void(null)' modo='3' sGrupo = '" + vGrupo + "' lItem = " + vIDItem + " sArt= '" +  vArt + "' sDescr = '" + vDescr + "' onclick='verDetallePujas(this)'>" + num2str(vPrecio,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</a>"
	<%else%>
		p.document.getElementById("divOfeMin" + vIDItem).innerHTML = num2str(vPrecio,vdecimalfmt,vthousanfmt,vprecisionfmt) 
	<%end if%>
	p.document.getElementById("divOfeMin" + vIDItem).precio = vPrecio
	}


<%if oProceso.VerPrecioPuja then%>
	if (p.document.getElementById("divPujaOfeMin" + vIDItem))
		{
		<%if oProceso.verDetallePujas then%>
			p.document.getElementById("divPujaOfeMin" + vIDItem).innerHTML = "<a  class='popUp ' href='javascript:void(null)' modo='3' sGrupo = '" + vGrupo + "' lItem = " + vIDItem + " sArt= '" +  vArt + "' sDescr = '" + vDescr + "' onclick='verDetallePujas(this)'><b>" + num2str(vPrecio,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b></a>&nbsp;"
		<%else%>
			p.document.getElementById("divPujaOfeMin" + vIDItem).innerHTML = "<b>" + num2str(vPrecio,vdecimalfmt, vthousanfmt, vprecisionfmt) +"</b>&nbsp;"
		<%end if%>
		p.document.getElementById("divPujaOfeMin" + vIDItem).precio = vPrecio
		p.document.getElementById("divPujaOfeMin" + vIDItem).parentNode.className =  p.document.forms["frmOfertaAEnviar"].elements["txtPujaClaseFila_" + vIDItem].value
		}
<%end if%>


if (p.document.getElementById("divOfeMin" + vIDItem))
	{
	if (sSuperada!="")
		p.document.getElementById("divOfeMin" + vIDItem).parentNode.className =  sSuperada  + " numero"
	else
		p.document.getElementById("divOfeMin" + vIDItem).parentNode.className =  p.document.forms["frmOfertaAEnviar"].elements["txtClaseFila_" + vIDItem].value + " numero"
	}


if (p.document.getElementById("divProve" + vIDItem))
	{
	if (sSuperada!="")
		p.document.getElementById("divProve" + vIDItem).parentNode.className =  sSuperada
	else
		p.document.getElementById("divProve" + vIDItem).parentNode.className =  p.document.forms["frmOfertaAEnviar"].elements["txtClaseFila_" + vIDItem].value
	}
}
fila="filaImpar"


function PonerGrupo(grupo,vPosicionPuja) {

//Actualiza la posicion del grupo	
if (p.document.getElementById("divPosicionPujaGrupo_" + grupo)) {

    if ((p.proceso.modo == 2) && (p.bAbierta && !p.bSobreCerrado))
    {
        strPosicion = "<table cellpadding='0' cellspacing='0' width='100%' height='20' class='POSICIONPUJAGRUPO'"
    }
    else
    {
        strPosicion = "<table cellpadding='0' cellspacing='0' width='100%' height='20'>"
    }


	strPosicion += "<tr>"
	strPosicion += "<td align=center class='cabecera' width='25%'> " 
    if (p.bAbierta && !p.bSobreCerrado)
    {
        strPosicion += textoPosicion	
	    strPosicion += "<b>"  
        strPosicion += vPosicionPuja 
        strPosicion += "</b>"
    }
	strPosicion += "</td></tr></table>"
	
	p.document.getElementById("divPosicionPujaGrupo_" + grupo).innerHTML = strPosicion
	}	

}

function PonerPosicionProceso (posicionProceso) {

//Actualiza la posicion del proceso	
    var strPosicionProceso
    if (p.document.getElementById("divPosicionPujaProceso")) 
    {
        if (parseInt(posicionProceso) > 0 ) 
        {
            if (p.bAbierta  && !p.bSobreCerrado)
            {
                if (p.document.getElementById("MostrarPosicionPujaProceso")) p.document.getElementById("MostrarPosicionPujaProceso").style.visibility="visible"
	            strPosicionProceso = "<b>&nbsp;" + posicionProceso + "</b>"
            }
            else
            {
                p.document.getElementById("MostrarPosicionPujaProceso").style.visibility="hidden"
                strPosicionProceso = "<b>&nbsp;</b>"
            }
        }
        else
        {
	        strPosicionProceso = "<b>&nbsp;</b>"
        }
	    
        if (p.document.getElementById("divPosicionPujaProceso")) p.document.getElementById("divPosicionPujaProceso").innerHTML = strPosicionProceso
	}	

}



<%



while not oRS.eof

    if oProceso.verDesdePrimeraPuja = false OR ((oProceso.verDesdePrimeraPuja = true) AND (oProceso.hayOfertaProveedor(CiaComp,CodProve)=true)) then 
	    if oProceso.verProveedor then
		    prove = JSText(oRS.fields("MINPROVEDEN").value)
	    end if
    end if
            precio = JSNum(oRS.fields("MINIMPORTE").value)
	
%>
<%if oRS.fields("PROPIA").value=0 then%>
	sSuperada = " superada"
<%else%>
	sSuperada = ""
<%end if


%>

ponerOferta("<%=prove%>", <%=precio%>, <%=JSNum(oRS.fields("ITEM").value)%>, "<%=JSText(oRS.fields("GRUPO").value)%>","<%=JSText(oRS.fields("ART").value)%>","<%=JSText(oRS.fields("DESCR").value)%>",sSuperada,<%=JSNum(oRS.fields("POSICIONPUJA").value)%>)

if (fila=="filaImpar")
	fila="filaPar"
else
	fila="filaImpar"

<%
oRS.movenext
wend
oRS.close
set oRS = nothing

while not oRS2.eof
Codgrupo = oRS2.fields("COD")
IDGrupo = oRS2.fields("ID")


posicionGrupo = oGrupo.DevolverPosicionPujaGrupo(CiaComp, anyo, proce, gmn1,Codprove,IDGrupo)
if posicionGrupo > 0 then
 %>
 PonerGrupo("<%=Codgrupo %>",<%=posicionGrupo %>)
<%
end if
oRS2.movenext
wend
oRS2.close
set oRS2 = nothing
%>
PonerPosicionProceso("<%=PosicionProceso %>")

if (p.proceso) 
if (p.proceso.grupos)
{

for (i=0;i<p.proceso.grupos.length;i++) {
	
	for (j=0;j<p.proceso.grupos[i].items.length;j++)
		if (p.document.getElementById("divOfeMin"  + p.proceso.grupos[i].items[j].id))
			p.proceso.grupos[i].items[j].ofertaMinima =p.document.getElementById("divOfeMin" + p.proceso.grupos[i].items[j].id ).precio
			
	//p.proceso.grupos[i].actualizarPosicionPujaGrupo()		
	}
<% if oProceso.verDesdePrimeraPuja = false OR ((oProceso.verDesdePrimeraPuja = true) AND (oProceso.hayOfertaProveedor(CiaComp,CodProve)=true)) then %>
    <%if oProceso.VerPrecioPuja then%>
    for (i=0;i<p.itemsPuja.length;i++)
	    if (p.document.getElementById("divPujaOfeMin" + p.itemsPuja[i].id)) p.itemsPuja[i].ofertaMinima = p.document.getElementById("divPujaOfeMin" + p.itemsPuja[i].id).precio
    <%end if%>
<%end if%>



<%if not isnull(oProceso.FechaMinimoLimOfertas) then%>
	p.cambiarFechaLimite(UTCtoClient(<%=JSHora(oProceso.FechaMinimoLimOfertas)%>))
<%end if%>
<%if not isnull(oProceso.FechaApertura) then%>
	p.cambiarFechaApertura(UTCtoClient(<%=JSHora(oProceso.FechaApertura)%>))
<%end if%>
<%if not isnull(oProceso.FecApeSobre) then%>
	p.cambiarFechaApeSobre(UTCtoClient(<%=JSHora(oProceso.FecApeSobre)%>))
<%end if%>
<%if not isnull(oProceso.FechaMinimoLimOfertas) then%>
	p.cambiarFechaLimite(UTCtoClient(<%=JSHora(oProceso.FechaMinimoLimOfertas)%>))
<%end if%>


<%

if not isnull(oProceso.estadoSubasta) then

    select case  oProceso.estadoSubasta
    case 1 ' No iniciada
    case 2 ' En curso
    case 3 ' Pausada
        ' Calcular el tiempo de reanudación
        if not isnull(oProceso.FechaReini) then 
        %>
            p.cambiarFechaReinicio(UTCtoClient(<%=JSHora(oProceso.FechaReini)%>))
        <%
        else
        %>
            p.cambiarFechaReinicio("")
        <%
        end if
    case 4 ' Detenida
    case 5 ' Finalizada
    end select
%>
	p.cambiarEstadoSubasta(<%=JSNum(oProceso.estadoSubasta)%>)
<%end if%>




<%if oProceso.modo = 1  then 
'Modo Proceso %>

p.MostrarGanadorProceso('<%=oProceso.proveGanador(CiaComp)%>',<%=JSNum(oProceso.importeGanador(CiaComp) * (equiv / oProceso.Cambio))%>)
<%end if %>



<%if oProceso.modo = 2  then 'Modo Grupo %>

<%for each oGrupo in oProceso.grupos %>
    p.MostrarGanadorGrupo('<%=ogrupo.codigo%>','<%=oGrupo.proveGanador(CiaComp)%>',<%=JSNum(oGrupo.importeGanador(CiaComp)* (equiv / oProceso.Cambio))%>)
<% next %>
<%end if %>


}


</script>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script src="../../common/ajax.js"></script>
<script language="JavaScript" type="text/JavaScript">
function init() { 
    if (<%=Application("FSAL")%> == 1){
        Ajax_FSALActualizar3(); //registro de acceso
    }
}
</script>
<!--#include file="../../common/fsal_3.asp"-->
</HEAD>
<BODY onload="init()" >




</BODY>
</HTML>

<%

end if '(bSubasta)

set oProceso = nothing
set oRaiz = nothing

 %>
 <!--#include file="../../common/fsal_2.asp"-->