﻿<!--#include file="../../common/acceso.asp"-->
<%
''' <summary>
''' Crea el objeto Proceso
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cumpoferta.asp      solicitudesoferta\cargarresultadopuja.as   ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")
set oRaiz=validarUsuario(Idioma,false,false,0)

%>

function Proceso(anyo, gmn1, proce, den, dest, pag, finisum, ffinsum, esp, prove, num, estado, fecValidez, mon , equiv, obs,  totOfertas, fecUltimaOfe, totalBytes, cfg, importe,obsAdjun,idPortal,posicionPuja,importeBruto,modo)
{
//Propiedades del proceso
this.anyo = anyo
this.gmn1 = gmn1
this.cod = proce
this.den = den
this.dest = dest
this.pag = pag
this.finisum = finisum
this.ffinsum = ffinsum
this.esp = esp
this.importe = importe
this.cfg = cfg
this.posicionPuja = posicionPuja
this.importeBruto = importeBruto

//DPD-> Se establece una nueva propiedad modo para determinar si las ofertas se evaluan a nivel de item, de grupo o de proceso completo.
this.modo = modo

//datos de la oferta
this.prove = prove
this.num = num
this.estado = estado
this.fecValidez = fecValidez
this.mon = mon
this.equiv = equiv
this.obs = obs
this.obsAdjun = obsAdjun
this.totOfertas = totOfertas
this.fecUltimaOfe = fecUltimaOfe
this.totalBytes = totalBytes
this.adjuntos = new Array()
this.sinAdjuntos = false
this.adjuntosCargados = false
this.anyaAdjunto = anyaAdjuntoOferta
this.cargarAdjuntos = cargarAdjuntosOferta

this.cargarOfertasMinimas = cargarOfertasMinimasProceso
this.igualaOfertasMinimas = igualarOfertasMinimasProceso

this.cargarSubasta = cargarSubastaProceso
this.subastaCargada = false

this.gruposCargados = false

this.sinAtributos = false
this.sinCostesDescuentos = false
this.idPortal = idPortal

//Colecciones del proceso
this.grupos = new Array()
this.sobres = new Array()
this.atribs = new Array()
this.CostesDescTotalOferta = new Array()
this.CostesDescTotalGrupo = new Array()
this.CostesDescTotalItem = new Array()
this.CostesDescPrecItem = new Array()
this.costesDescuentos = new Array()
this.ImportesParcialesGrupo = new Array()
this.especs = new Array()
this.Aespecs = new Array()



//Metodos del proceso
this.cargarGrupos = cargarGruposProceso
this.anyaGrupo = anyaGrupoProceso
this.anyaSobre = anyaSobreProceso
this.cargarAtribs = cargarAtribsProceso
this.anyaAtrib = anyaAtribProceso
this.mostrarCostesDesc = mostrarCostesDescProceso
this.anyaCosteDescuentoTotalOferta = anyaCosteDescuentoTotalOfertaProceso
this.anyaCosteDescuentoTotalGrupo = anyaCosteDescuentoTotalGrupoProceso
this.anyaCosteDescuentoTotalItem = anyaCosteDescuentoTotalItemProceso
this.anyaCosteDescuentoPrecItem = anyaCosteDescuentoPrecItemProceso
this.anyaCosteDescuento= anyaCosteDescuentoProceso
this.anyaImportesParcialesGrupo = anyaImportesParcialesGrupoProceso
this.cargarEspecs = cargarEspecsProceso
this.cargarAEspecsP = cargarAEspecsProceso
this.cargarAEspecsG = cargarAEspecsGrupo
this.anyaEspec = anyaEspecProceso
this.anyaAEspecP = anyaAEspecProceso
}

//''' <summary>
//''' Carga los Grupos de un proceso
//''' </summary>
//''' <param name="target">donde cargar la pagina</param> 
//''' <remarks>Llamada desde: function Proceso ; Tiempo máximo: 0</remarks>
function cargarGruposProceso(target)
{
var features
features = ""
window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/grupos.asp?proce=" + this.cod,target,features)
}

function anyaGrupoProceso(grupo)
{
var i
i=this.grupos.length
this.grupos[i]=grupo

}


function anyaSobreProceso(sobre)
{
var i
i=this.sobres.length
this.sobres[i]=sobre
}


function cargarAtribsProceso(target)
{
var features
features = ""
if (this.atribs.length==0 && this.sinAtributos == false)
	window.open("<%=replace(Request.servervariables("URL"),"proceso.asp","")%>cargaratributos.asp?anyo=" + this.anyo + "&gmn1=" + this.gmn1 + "&proce=" + this.cod + "&ofe=" + this.num  + "&mon=" + this.mon.cod   + "&eq=" + this.mon.equiv + "&IdPortal=" + this.idPortal   ,target,features)
else
	{
	mostrarOferta(this)
	desbloquear()
	}
}


function anyaAtribProceso(atrib)
{
var i
i = this.atribs.length
this.atribs[i]=atrib
}

function anyaCosteDescuentoTotalOfertaProceso(costeDescuento)
{
var i
i = this.CostesDescTotalOferta.length
this.CostesDescTotalOferta[i]=costeDescuento
}

function anyaCosteDescuentoTotalGrupoProceso(costeDescuento)
{
var i
i = this.CostesDescTotalGrupo.length
this.CostesDescTotalGrupo[i]=costeDescuento
}

function anyaCosteDescuentoTotalItemProceso(costeDescuento)
{
var i
i = this.CostesDescTotalItem.length
this.CostesDescTotalItem[i]=costeDescuento
}

function anyaCosteDescuentoPrecItemProceso(costeDescuento)
{
var i
i = this.CostesDescPrecItem.length
this.CostesDescPrecItem[i]=costeDescuento
}

function anyaCosteDescuentoProceso(costeDescuento)
{
var i
i = this.costesDescuentos.length
this.costesDescuentos[i]=costeDescuento
}

function anyaImportesParcialesGrupoProceso(costeDescGrOfert)
{
var i
i = this.ImportesParcialesGrupo.length
this.ImportesParcialesGrupo[i]=costeDescGrOfert
}

//''' <summary>
//''' Carga los especificaciones de un proceso
//''' </summary>
//''' <param name="target">donde cargar la pagina</param> 
//''' <remarks>Llamada desde: function Proceso ; Tiempo máximo: 0</remarks>
function cargarEspecsProceso(target)
{
var features
features = ""
window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/especs.asp?proce=" + this.cod,target,features)
}
//''' <summary>
//''' Carga los especificaciones de un proceso.
//''' </summary>
//''' <param name="target">donde cargar la pagina</param> 
//''' <remarks>Llamada desde: function Proceso ; Tiempo máximo: 0</remarks>
function cargarAEspecsProceso(target)
{
var features
features = ""
window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/AEspecsP.asp?proce=" + this.cod,target,features)
}
//''' <summary>
//''' Carga los especificaciones de un grupo
//''' </summary>
//''' <param name="target">donde cargar la pagina</param> 
//''' <remarks>Llamada desde: function Proceso ; Tiempo máximo: 0</remarks>
function cargarAEspecsGrupo(target)
{
var features
features = ""
window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/AEspecsG.asp?proce=" + this.cod,target,features)
}

function anyaEspecProceso(espec)
{
var i
i = this.especs.length
this.especs[i]=espec
}

function anyaAEspecProceso(AEspecsP)
{
var i
i = this.Aespecs.length
this.Aespecs[i]=AEspecsP
}


function anyaAdjuntoOferta(adjunto)
{
var i
i = this.adjuntos.length
this.adjuntos[i]=adjunto
}

function cargarAdjuntosOferta(target)
{
var features
features = ""
if (this.adjuntos.length==0 && this.sinAdjuntos == false)
	window.open("<%=replace(Request.servervariables("URL"),"proceso.asp","")%>cargaradjuntos.asp?anyo=" + this.anyo + "&gmn1=" + this.gmn1 + "&proce=" + this.cod  + "&ofe=" + this.num + "&IdPortal=" + this.idPortal,target,features)
else
	{
	mostrarAdjuntosProceso(this)
	desbloquear()
	}
}

function cargarOfertasMinimasProceso(target,bPujando)
{

var features
features = ""
window.open("<%=replace(Request.servervariables("URL"),"proceso.asp","")%>cargarofemin.asp?anyo=" + this.anyo + "&gmn1=" + this.gmn1 + "&proce=" + this.cod + "&mon=" + this.mon.cod   + "&eq=" + this.mon.equiv  + "&bPujando=" + bPujando ,target,features)
}


function igualarOfertasMinimasProceso(target)
{

var features
features = ""
	window.open("<%=replace(Request.servervariables("URL"),"proceso.asp","")%>cargarGanadora.asp?anyo=" + this.anyo + "&gmn1=" + this.gmn1 + "&proce=" + this.cod + "&ofe=" + this.num  + "&mon=" + this.mon.cod  + "&eq=" + this.mon.equiv + "&bPujando=" + bPujando,target,features)
}


function cargarSubastaProceso(target)
{
var features
features = ""
if (this.subastaCargada==false)
	window.open("<%=replace(Request.servervariables("URL"),"proceso.asp","")%>cargarsubasta.asp?anyo=" + this.anyo + "&gmn1=" + this.gmn1 + "&proce=" + this.cod + "&ofe=" + this.num  + "&mon=" + this.mon.cod  + "&eq=" + this.mon.equiv,target,features)
else
	{
	mostrarSubasta(this)
	desbloquear()
	}

}

function mostrarCostesDescProceso(target)
{
	mostrarFormCostesDescuentosAmbitoProceso(this,1)
	desbloquear()

}

function guardarCosteDescRecalculo(costeDescRec)
{
    this.costeDescRecalculo = costeDescRec
}