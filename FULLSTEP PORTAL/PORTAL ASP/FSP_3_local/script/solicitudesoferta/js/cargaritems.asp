﻿<%@  language="VBScript" %>
<%
oldTimeOut = server.ScriptTimeout
server.ScriptTimeout =40000
%>
<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/idioma.asp"-->
<!--#include file="../../common/colores.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
    <script src="../../common/formatos.js"></script>
    <script language="JavaScript" src="../../common/ajax.js"></script>
    <script language="JavaScript" type="text/JavaScript">
        function init(soloCargar) {             
            if (<%=Application("FSAL")%> == 1){
                Ajax_FSALActualizar3(); //registro de acceso
            }
            //if (soloCargar) window.opener.preCambiarMoneda(,false);    
            <%dim sCallbackFunction
            sCallbackFunction=request("callbackFunction")            
            if sCallbackFunction<>"" then%>
                //window.opener.<%=sCallbackFunction%>;                
                parent.frames["fraOfertaClient"].<%=sCallbackFunction%>;
            <%end if%>
        }
    </script>
    <script src="atributo.asp"></script>
    <script src="escalado.asp"></script>
    <link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
    <!--#include file="../../common/fsal_3.asp"-->
</head>
<body onload="init(<%=JSBool(request("SoloCargar"))%>)">
    <%
''' <summary>Cargar los items de un grupo de la oferta</summary>
''' <remarks>Llamada desde: js\grupo.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")
if Idioma="" then
	Idioma = "SPA"
end if
dim den
den = devolverTextos(Idioma,93)

set oRaiz=validarUsuario(Idioma,true,false,0)

CiaComp = clng(oRaiz.Sesion.CiaComp)
anyo = request("anyo")
gmn1 = request("gmn1")
proce = request("proce")
grupo = request("grupo")
ofe = request("ofe")
if ofe="0" then
	ofe = null
end if
mon = request("mon")
bSoloCargar=CBool(request("SoloCargar"))
bItemsCargados=CBool(request("ItemsCargados"))

decimalfmt = Request.Cookies ("USU_DECIMALFMT")
thousanfmt = Request.Cookies ("USU_THOUSANFMT") 
precisionfmt = CINT(Request.Cookies ("USU_PRECISIONFMT") )
datefmt = Request.Cookies ("USU_DATEFMT") 

prove = oRaiz.Sesion.CiaCodGs

    %>

    <script>

var vdecimalfmt 
vdecimalfmt ="<%=decimalfmt%>"
var vthousanfmt 
vthousanfmt ="<%=thousanfmt%>"
var vprecisionfmt
vprecisionfmt = <%=precisionfmt%>
var vdatefmt
vdatefmt = "<%=datefmt%>"

var p
var bAnyoImputacion;
bAnyoImputacion=false
p = parent.frames["fraOfertaClient"]        
var oObj
var i
for (i=0;i<p.proceso.grupos.length && p.proceso.grupos[i].cod != '<%=grupo%>';i++){}
oObj = p.proceso.grupos[i]

var iGrupo
iGrupo = i

lTotal = oObj.numItems  + 4

lP = 1
p.progreso (lP * 100/lTotal)

<%
Set oProceso = oRaiz.Generar_CProceso

oProceso.cargarConfiguracion CiaComp, anyo, gmn1, proce, prove
oProceso.cargarProceso CiaComp, Idioma

objetivosPublicados=oProceso.getObjetivosPublicados(ciacomp,prove)

'''Dato Objetivo proceso: nuevo param para saber mon gs ofe
'''Ejemplo: bbdd: 10,6182662494868        Moneda: Libra           Cambio al crear proceso libra: 0.70633         Cambio actual Libra: 1.5 ---> El objetivo es 5 Libras. Aplicar un cambio de 1.5 no da 5, se ha aplicado 0.70633.                
'''Si la oferta q tienes en GS, es en libras, tienes la cifra q se vería en GS, es decir, ya tienes el 5 esperado. Sin importar q haya cambiado el cambio de la moneda del proceso.
set oOferta = oProceso.CargarOfertaProveedor(Idioma,ciacomp,prove,,BBDDOferta)
if oOferta.CodMon=mon then
    equiv = oProceso.cambio * oOferta.cambio    
else
    equiv = Numero(replace(request("eq"),".", decimalfmt))
end if

set oMonedas = oRaiz.Generar_CMonedas()
oMonedas.CargarTodasLasMonedasDesde Idioma,CiaComp , 1, mon, , , true, , true
CambioUltimo=Numero(replace(oMonedas.item(mon).equiv,thousanfmt,decimalfmt))

if not BBDDOferta ="" then
    oMonedas.CargarTodasLasMonedasDesde Idioma,CiaComp , 1, BBDDOferta, , , true, , true
    CambioMonedaGS=Numero(replace(oMonedas.item(BBDDOferta).equiv,thousanfmt,decimalfmt))
else
    CambioMonedaGS = CambioUltimo
end if

set oMonedas = nothing

if oProceso.MonCod = mon then
    'Si estas leyendo una Oferta en moneda del proceso. El cambio es el del proceso.
    equivObj=oProceso.Cambio
else
    'Obtener los datos de la moneda para cargar el dato de objetivo con el último cambio y no con el cambio de la última oferta
    equivObj=CambioUltimo
end if

if request("IdPortal")="null" then
	oOferta.IDPortal = null
else
	oOferta.IDPortal = request("IdPortal")
end if

set oGrupo = oProceso.grupos.item(grupo)

set oOfeGrupo = oRaiz.Generar_COfertaGrupo()
set oOfeGrupo.Oferta = oOferta
set oOfeGrupo.Grupo = oGrupo

dim bConsulta
if iif(oProceso.MaximoNumeroOfertas<>0,oProceso.MaximoNumeroOfertas<> oOferta.TotEnviadas,true) then
	bConsulta = false
else
	bConsulta = true
end if
set oGrupo = oProceso.grupos.item(grupo)
oGrupo.cargarGrupo Idioma,ciaComp,, anyo, gmn1, proce,prove,ofe,mon,oProceso.SolicitarCantMax
oGrupo.cargarEscalados(CiaComp)

'JVS Carga de los datos de Costes / Descuentos
oProceso.CargarCostesDescuentos CiaComp

dim sobrecerrado
sobrecerrado = false 
if oProceso.tipo = 1 then
    sobrecerrado = (oProceso.fecApeSobre >= oRaiz.obtenerSQLUTCDate(CiaComp))
end if

dim sinpujas
if oProceso.verDesdePrimeraPuja = false OR ((oProceso.verDesdePrimeraPuja = true) AND (oProceso.hayOfertaProveedor(CiaComp,Prove)=true)) then
    sinpujas = false
else
    sinpujas = true
end if

if not bItemsCargados then
    if not oProceso.Subasta and not sobrecerrado then
        if not (oGrupo.CostesDescItem is nothing) then 
            for each oCosDes in oGrupo.CostesDescItem
                with oCosDes%>                    
	                costedesc = new Atributo(<%=.id%>, <%=.paid%>, '<%=JSText(.cod)%>', '<%=JSText(.den)%>', <%=.intro%>, <%=.tipo%>, <%=JSNum(.valor)%>, <%=JSBool(.obligatorio)%>, <%=JSNum(.valorMin)%>, <%=JSNum(.valorMax)%>, null, p.proceso,'<%=JSText(.operacion)%>',<%=.aplicar %>,<%=.ambito %>,null,null,'<%=JSText(.Alcance)%>');
	                oObj.anyaCosteDescuentoItems(costedesc);                    
                <%end with
            next
        end if
    end if 
end if%>
    </script>

    <%
if oProceso.DefDestino=3 or (oProceso.DefDestino=2 and oGrupo.DefDestino=false) then
	dim oDestinos 

	set oDestinos = oRaiz.Generar_CDestinos()

	oDestinos.CargarTodosLosDestinosDesde Idioma,ciaComp, 10000
end if
    %>

    <script>
lP++
p.progreso (lP * 100/lTotal)

function configurarItems(vGrp)
{
    <%set oGrupo.Proceso = oProceso
        oGrupo.cargarItems Idioma,CiaComp,mon,prove,ofe,false
        for each oItem in oGrupo.items
            if oItem.AnyoImputacion > 0 then%>
                bAnyoImputacion=true;
                <%exit for
            end if
        next
    %>
    <%if oGrupo.defEspecificacionesItem then%>
	    p.wCEsp = 30
	    vGrp.colspanArticulo = 3
    <%else%>
	    p.wCEsp = 0
	    vGrp.colspanArticulo = 2
    <%end if%>	
    if ( bAnyoImputacion == true) vGrp.colspanArticulo = vGrp.colspanArticulo+1
    vGrp.colspanDatosItem = 4
    vGrp.colspanSubasta = 1
    vGrp.colspanPrecios = 3
    vGrp.colspanMasPrecios = 6
    vGrp.colspanAtributos = vGrp.atribsItems.length

    <%if oGrupo.hayEscalados then%>
	    vGrp.colspanDatosItem = vGrp.colspanDatosItem - 2
	    vGrp.colspanPrecios = vGrp.colspanPrecios + 3 // Rango, Ultimo Precio y Objetivo
    <%end if%>

    <%if oProceso.DefDestino=3 or (oProceso.DefDestino=2 and oGrupo.DefDestino=false) then%>
	    p.wCDestino = 50
	    vGrp.colspanDatosItem ++
    <%end if%>
    <%if oProceso.DefFechasSum=3 or (oProceso.DefFechasSum=2 and oGrupo.DefFechasSum=false) then%>
	    vGrp.colspanDatosItem ++
	    p.wCFecIni = 70
	    <%if oProceso.MostrarFinSum then%>
	    vGrp.colspanDatosItem ++
	    p.wCFecFin = 70
	    <%end if
    end if
    if oProceso.DefFormaPago=3 or (oProceso.DefFormaPago=2 and oGrupo.DefFormaPago=false) then%>
	    vGrp.colspanDatosItem ++
	    p.wCFormaPago = 30
    <%end if%>

    <%if oProceso.subasta and oProceso.VerProveedor and not sobrecerrado then%>
	    p.wCProveedor = 120
	    vGrp.colspanSubasta = 2
    <%end if%>
	
	
    <%if oProceso.SolicitarCantMax then%>
	    vGrp.colspanPrecios++
	    p.wCCantMax=70
    <%end if%>
    <%if oProceso.DefAdjunItem then%>
	    vGrp.colspanPrecios++
	    p.wCAdjunItem =30
    <%end if%>
	
    <%if oProceso.PedirAlterPrecios then%>
	    p.wCPU2 = 80
	    p.wCTotalLinea2 = 90
	    p.wCPU3 = 80
	    p.wCTotalLinea3 = 90
    <%end if%>
    if (bAnyoImputacion==true )
    {
        p.wCDenAnyoImputacion = 50;
        p.wCDenArt = 220
    }
    else
    {
        p.wCDenAnyoImputacion = 0;
        p.wCDenArt = 270
    }
}
       
//no dependen del grupo

p.wCCodArt = 140

<% if objetivosPublicados then %>
    p.wCCantidad = 90
    p.wCUnidad = 80
    p.wCOfertaActual = 90
    p.wCObjetivo = 90
<% else %>
    p.wCCantidad = 112
    p.wCUnidad = 104
    p.wCOfertaActual = 112
    p.wCObjetivo = 0
<% end if %>


<% if objetivosPublicados then %>
p.wCRango = 90
<% else %>
p.wCRango = 112
<%end if %>

p.wCComentario = 40
p.wCPU = 80
p.wCTotalLinea = 90
p.wCAtributo = 200
p.wCOfertaMinima = 90
p.wCDestino = 0
p.wCFecIni = 0
p.wCFecFin = 0
p.wCFormaPago = 0
p.wCAdjunItem = 0
p.wCCantMax = 0
p.wCPU2 = 0
p.wCTotalLinea2 = 0
p.wCPU3 = 0
p.wCTotalLinea3 = 0
p.wCProveedor = 0
p.hCFijo=25 //Alto de cada fila
p.wDesviacion = 4 // Cada columna es cuatro puntos más ancha que la wC correspondiente...
p.wBorde = 2 //Ancho de las separaciones de las columnas

p.CIE =0 // Corrección bordes 
if (p.document.all) p.CIE = -p.wBorde // IE

p.wCSeparacion = 10 //Ancho de las columnas de separación


function prepararDivsItems(vGrp)
{
    hAptdo = p.document.getElementById("divApartadoS").style.height
    p.hCabecera = 2*(p.hCFijo + p.wDesviacion)

    if (!(p.document.getElementById("div" + vGrp.cod))){
        vGrp.wApArticulo = p.wCEsp + p.wCCodArt + p.wCDenArt + p.wCDenAnyoImputacion + vGrp.colspanArticulo * (p.wDesviacion + p.wBorde + p.CIE)
        <%if ogrupo.hayEscalados then %>
	        //los campos Ultimo precio y objetivos pasan a la sección de oferta
	        vGrp.wApDatosItem = p.wCSeparacion + p.wCDestino + p.wCCantidad + p.wCUnidad + p.wCFecIni + p.wCFecFin + p.wCFormaPago + (vGrp.colspanDatosItem +1)*(p.wDesviacion + p.wBorde + p.CIE)
        <%else%>
	        vGrp.wApDatosItem = p.wCSeparacion + p.wCDestino + p.wCCantidad + p.wCUnidad + p.wCOfertaActual + p.wCObjetivo + p.wCFecIni + p.wCFecFin + p.wCFormaPago + (vGrp.colspanDatosItem +1)*(p.wDesviacion + p.wBorde + p.CIE)
        <%end if%>

        <%if oProceso.Subasta and not sobrecerrado and (not sinpujas) and ((oProceso.VerPrecioPuja) or (oproceso.VerProveedor)) then%>
	        vGrp.wApSubasta =  p.wCSeparacion + p.wCProveedor + p.wCOfertaMinima + (vGrp.colspanSubasta + 1 ) * (p.wDesviacion + p.wBorde + p.CIE)
        <%end if%>

        <%if ogrupo.hayEscalados then %>
	        //los campos Ultimo precio y objetivos pasan a la sección de oferta
	        // y además aparece el campo Rango
	        vGrp.wApOferta = p.wCSeparacion + p.wCRango + p.wCOfertaActual + p.wCObjetivo + p.wCPU + p.wCTotalLinea + p.wCComentario + p.wCCantMax + p.wCAdjunItem + (vGrp.colspanPrecios +1) * (p.wDesviacion + p.wBorde + p.CIE)
        <%else%>
	        vGrp.wApOferta = p.wCSeparacion + p.wCPU + p.wCTotalLinea + p.wCComentario + p.wCCantMax + p.wCAdjunItem + (vGrp.colspanPrecios +1) * (p.wDesviacion + p.wBorde + p.CIE)
        <%end if%>

        <%if oProceso.PedirAlterPrecios then%>
	        vGrp.wApMasPrecios = p.wCSeparacion + p.wCPU2 + p.wCTotalLinea2 + p.wCComentario + p.wCPU3 + p.wCTotalLinea3 + p.wCComentario + (vGrp.colspanMasPrecios + 1)* (p.wDesviacion + p.wBorde + p.CIE)
        <%else%>
	        vGrp.wApMasPrecios = 0
        <%end if%>

        if (vGrp.atribsItems.length>0){
	        vGrp.wApAtributos = p.wCSeparacion + (vGrp.colspanAtributos + 1) * (p.wDesviacion + p.wBorde + p.CIE) + p.wCAtributo * vGrp.colspanAtributos
	    }

        <%
        '
        'divArticuloC        divCabeceraScroll
        '+-----------------+ +--------------------------------------------------------+
        '¦                 ¦ ¦divCabecera                                             ¦
        '¦                 ¦ ¦+------------------------------------------------------+¦
        '¦                 ¦ ¦¦divDatosItemC divOfertaC divMasPreciosC divAtributosC ¦¦
        '¦                 ¦ ¦¦+-----+       +-----+    +-----+        +-----+       ¦¦
        '¦                 ¦ ¦¦¦     ¦       ¦     ¦    ¦     ¦        ¦     ¦       ¦¦
        '¦                 ¦ ¦¦+-----+       +-----+    +-----+        +-----+       ¦¦
        '¦                 ¦ ¦+------------------------------------------------------+¦
        '+-----------------+ +--------------------------------------------------------+
        'divArticuloScroll   divScroll
        '+-----------------+ +--------------------------------------------------------+
        '¦divArticulo      ¦ ¦ divDatosItem  divOferta  divMasPrecios  divAtributos   ¦
        '¦+---------------+¦ ¦ +-----+       +-----+    +-----+        +-----+        ¦
        '¦¦               ¦¦ ¦ ¦     ¦       ¦     ¦    ¦     ¦        ¦     ¦        ¦
        '¦¦               ¦¦ ¦ ¦     ¦       ¦     ¦    ¦     ¦        ¦     ¦        ¦
        '¦¦               ¦¦ ¦ ¦     ¦       ¦     ¦    ¦     ¦        ¦     ¦        ¦
        '¦¦               ¦¦ ¦ +-----+       +-----+    +-----+        +-----+        ¦
        '¦¦               ¦¦ ¦ divMas                                                 ¦
        '¦¦               ¦¦ ¦+------------------------------------------------------+¦
        '¦¦               ¦¦ ¦¦                                                      ¦¦
        '¦+---------------+¦ ¦+------------------------------------------------------+¦
        '+-----------------+ +--------------------------------------------------------+                   
        %>

        var newEl = document.createElement("input");
        newEl.type = "hidden";
        newEl.name = "numItemsGrupo" + vGrp.cod;
        newEl.id = "numItemsGrupo" + vGrp.cod;
        newEl.value = vGrp.numItems;
        p.document.getElementById("divItems").appendChild(newEl);

        newEl = document.createElement("input");
        newEl.type = "hidden";
        newEl.name = "numAtribsItem" + vGrp.cod;
        newEl.id = "numAtribsItem" + vGrp.cod;
        newEl.value = vGrp.atribsItems.length;
        p.document.getElementById("divItems").appendChild(newEl);

        newEl = document.createElement("input");
        newEl.type = "hidden";
        newEl.name = "grupo";
        newEl.id = "grupo";
        newEl.value = vGrp.cod;
        p.document.getElementById("divItems").appendChild(newEl);

        var container = document.createElement("div");
        container.name ="div" + vGrp.cod;
        container.id ="div" + vGrp.cod;
        container.setAttribute("style","position:absolute;top:0px;left:0px;visibility:hidden;BORDER:<%=cBORDEBLOQUE%> 1px solid;");

        var newTbl = document.createElement("table");
        newTbl.name = "tbl_" + vGrp.cod;
        newTbl.id = "tbl_" + vGrp.cod;
        newTbl.setAttribute("width","100%");

        if (!p.proceso.importe) p.proceso.importe  = <%=jsnum(oOferta.importeTotal)%>

        if (p.proceso.estado>1){
            var newTr = document.createElement("tr");
            newTr.setAttribute("height","24px");

            var newTd = document.createElement("td");

	        switch (p.proceso.estado){
		        case 1:
			        break;
		        case 2:
			        sClase = "OFESINENVIAR"
			        break;
		        case 3:
		        case 5:
			        sClase = "OFEENVIADA"
			        break;
		        case 4:
			        sClase = "OFESINGUARDAR"
			        break
            }

            newTd.setAttribute("class",sClase);

            var containerEnTd = document.createElement("div");
            containerEnTd.name ="divTotalOferta_" + vGrp.cod;
            containerEnTd.id ="divTotalOferta_" + vGrp.cod;
            containerEnTd.setAttribute("style","overflow:hidden;");


            var newTblEstado  = document.createElement("table");
            newTblEstado.setAttribute("width","100%");
            var newTrEstado = document.createElement("tr");   
            newTrEstado.setAttribute("height","24px"); 
            var newTdEstado = document.createElement("td");
            newTdEstado.setAttribute("class",sClase);

            sEstado = p.estadoOferta(p.proceso.estado)
            sEstado = sEstado + "\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0" +  (p.linkRecalculo==0?num2str(p.proceso.importe,vdecimalfmt,vthousanfmt,vprecisionfmt):"-") + "\u00A0" + p.proceso.mon.cod + "\u00A0\u00A0\u00A0\u00A0"

            newTdEstado.innerHTML =sEstado; //texto divTotalOferta_vGrp.cod 

            newEl = document.createElement("a");
            newEl.setAttribute("href","javascript:void(null)");
            newEl.setAttribute("onclick","verAyuda(" + p.proceso.estado + ")");

            var newElImg = document.createElement("IMG");
            newElImg.name = "imgMasInform";
            newElImg.setAttribute("SRC","../images/masinform.gif");
            newElImg.setAttribute("WIDTH","11");
            newElImg.setAttribute("HEIGHT","14");

            newEl.appendChild(newElImg);

            newTdEstado.appendChild(newEl); //img divTotalOferta_vGrp.cod

            newTrEstado.appendChild(newTdEstado);
            newTblEstado.appendChild(newTrEstado)
            containerEnTd.appendChild(newTblEstado);                        

            if(p.linkRecalculo==1){
                text = document.createTextNode("\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0");
                containerEnTd.appendChild(text); //texto divTotalOferta_vGrp.cod

                newEl = document.createElement("a");
                newEl.setAttribute("href","javascript:void(null)");
                newEl.setAttribute("onclick","linkRecalculoOferta()");
                newEl.value="<%=JSText(den(34))%>";
                containerEnTd.appendChild(newEl); //href divTotalOferta_vGrp.cod
            }
            newTd.appendChild(containerEnTd); //divTotalOferta_vGrp.cod del 1er td
            newTr.appendChild(newTd);//1er td del 1er tr
            newTbl.appendChild(newTr); //1er tr
        }
        else{	
            var newTr = document.createElement("tr");
            newTr.setAttribute("height","24px");

            var newTd = document.createElement("td");

            //Tal y como funciona el programa, el 1er cambio te lo va a poner a estado 4. Oferta Sin Guardar.
			sClase = "OFESINGUARDAR"
			        
            newTd.setAttribute("class",sClase);

            var containerEnTd = document.createElement("div");
            containerEnTd.name ="divTotalOferta_" + vGrp.cod;
            containerEnTd.id ="divTotalOferta_" + vGrp.cod;
            containerEnTd.setAttribute("style","overflow:hidden;");

            var text = document.createTextNode("");
            containerEnTd.appendChild(text); //text del divTotalOferta_vGrp.cod

            newTd.appendChild(containerEnTd); //divTotalOferta_vGrp.cod del 1er td
            newTr.appendChild(newTd);//1er td del 1er tr
            newTbl.appendChild(newTr); //1er tr
        }

        newTr = document.createElement("tr");
        newTr.setAttribute("height","24px");

        newTd = document.createElement("td");
        newTd.setAttribute("class","cabApartado");
        newTd.setAttribute("style","text-align:left");

        newEl = document.createElement("nobr");
        var text = document.createTextNode("<%=den(21)%>&nbsp;&nbsp;");
        newEl.appendChild(text);

        newTr.appendChild(newTd);//1er td del 2do tr
        newTbl.appendChild(newTr); //2do tr

        container.appendChild(newTbl); //tbl_vGrp.cod

        var containerApartado = document.createElement("div");
        containerApartado.name ="divApartadoS" + vGrp.cod;
        containerApartado.id ="divApartadoS" + vGrp.cod;
        containerApartado.setAttribute("style","position:absolute;left:0px;top:52px;");
        
        var containerCabeceraScroll = document.createElement("div");
        containerCabeceraScroll.name ="divCabeceraScroll" + vGrp.cod;
        containerCabeceraScroll.id ="divCabeceraScroll" + vGrp.cod;
        containerCabeceraScroll.setAttribute("style","position:relative;top:0px;left:" + vGrp.wApArticulo + "px;height:" + p.hCabecera + "px;");

        var containerCabecera = document.createElement("div");
        containerCabecera.name ="divCabecera" + vGrp.cod;
        containerCabecera.id ="divCabecera" + vGrp.cod;
        containerCabecera.setAttribute("style","position:relative;top:0px;left:0px;height:" + p.hCabecera + "px;");

        var containerDatosItemC = document.createElement("div");
        containerDatosItemC.name ="divDatosItemC" + vGrp.cod;
        containerDatosItemC.id ="divDatosItemC" + vGrp.cod;
        containerDatosItemC.setAttribute("style","position:absolute;top:0px;left:0px;");

        containerCabecera.appendChild(containerDatosItemC); //divDatosItemCvGrp.cod del divCabeceravGrp.cod

	    <%if oProceso.subasta and not sobrecerrado and (not sinpujas) and ((oProceso.VerPrecioPuja) or (oproceso.VerProveedor)) then%>
            var containerSubastaC = document.createElement("div");
            containerSubastaC.name ="divSubastaC" + vGrp.cod;
            containerSubastaC.id ="divSubastaC" + vGrp.cod;
            containerSubastaC.setAttribute("style","position:absolute;top:0px;left:" + vGrp.wApDatosItem +  "px;");

            containerCabecera.appendChild(containerSubastaC); //divSubastaCvGrp.cod del divCabeceravGrp.cod

	    <%end if%>

        var containerOfertaC = document.createElement("div");
        containerOfertaC.name ="divOfertaC" + vGrp.cod;
        containerOfertaC.id ="divOfertaC" + vGrp.cod;
        containerOfertaC.setAttribute("style","position:absolute;top:0px;left:" + (vGrp.wApDatosItem + vGrp.wApSubasta) + "px;");

        containerCabecera.appendChild(containerOfertaC); //divOfertaCvGrp.cod del divCabeceravGrp.cod

	    <%if oProceso.PedirAlterPrecios then%>
            var containerMasPreciosC = document.createElement("div");
            containerMasPreciosC.name ="divMasPreciosC" + vGrp.cod;
            containerMasPreciosC.id ="divMasPreciosC" + vGrp.cod;
            containerMasPreciosC.setAttribute("style","position:absolute;top:0px;left:" + (vGrp.wApDatosItem + vGrp.wApSubasta + vGrp.wApOferta) +  "px;");

            containerCabecera.appendChild(containerMasPreciosC); //divMasPreciosCvGrp.cod del divCabeceravGrp.cod
	    <%end if%>

        if (vGrp.atribsItems.length>0){
            var containerAtributosC = document.createElement("div");
            containerAtributosC.name ="divAtributosC" + vGrp.cod;
            containerAtributosC.id ="divAtributosC" + vGrp.cod;
            containerAtributosC.setAttribute("style","position:absolute;top:0px;left:" + (vGrp.wApDatosItem + vGrp.wApSubasta + vGrp.wApOferta + vGrp.wApMasPrecios) +  "px;");

            containerCabecera.appendChild(containerAtributosC); //divAtributosCvGrp.cod del divCabeceravGrp.cod
	    }

        containerCabeceraScroll.appendChild(containerCabecera); //divCabeceravGrp.cod del divCabeceraScrollvGrp.cod

        containerApartado.appendChild(containerCabeceraScroll); //divCabeceraScrollvGrp.cod del divApartadoSvGrp.cod

        var containerArticuloC = document.createElement("div");
        containerArticuloC.name ="divArticuloC" + vGrp.cod;
        containerArticuloC.id ="divArticuloC" + vGrp.cod;
        containerArticuloC.setAttribute("style","position:absolute;top:0px;left:0px;background:#FFFFFF;");
        
        containerApartado.appendChild(containerArticuloC); //divArticuloCvGrp.cod del divApartadoSvGrp.cod

        var containerArticuloScroll = document.createElement("div");
        containerArticuloScroll.name ="divArticuloScroll" + vGrp.cod;
        containerArticuloScroll.id ="divArticuloScroll" + vGrp.cod;
        containerArticuloScroll.setAttribute("style","position:absolute;top:" + p.hCabecera + "px;left:0px;height:" + hAptdo + ";width:" + vGrp.wApArticulo + "px; overflow:hidden;");

        var containerArticulo = document.createElement("div");
        containerArticulo.name ="divArticulo" + vGrp.cod;
        containerArticulo.id ="divArticulo" + vGrp.cod;
        containerArticulo.setAttribute("style","position:absolute;top:0px;left:0px;height:" + hAptdo + "px;");

        containerArticuloScroll.appendChild(containerArticulo); //divArticulovGrp.cod del divArticuloScrollvGrp.cod

        containerApartado.appendChild(containerArticuloScroll); //divArticuloScrollvGrp.cod del divApartadoSvGrp.cod

        var containerScroll = document.createElement("div");
        containerScroll.name ="divScroll" + vGrp.cod;
        containerScroll.id ="divScroll" + vGrp.cod;
        containerScroll.setAttribute("onScroll","scrollFijas(\"" + vGrp.cod + "\")");
        containerScroll.setAttribute("style","position:absolute;top:" + p.hCabecera + "px;left:" + vGrp.wApArticulo + "px;z-index:200;overflow:scroll;");

        var containerDatosItem = document.createElement("div");
        containerDatosItem.name ="divDatosItem" + vGrp.cod;
        containerDatosItem.id ="divDatosItem" + vGrp.cod;
        containerDatosItem.setAttribute("style","position:absolute;top:0px;left:0px;");

        containerScroll.appendChild(containerDatosItem); //divDatosItemvGrp.cod del divScrollvGrp.cod

	    <%if oProceso.Subasta and not sobrecerrado and (not sinpujas) and ((oProceso.VerPrecioPuja) or (oproceso.VerProveedor)) then%>
            var containerSubasta = document.createElement("div");
            containerSubasta.name ="divSubasta" + vGrp.cod;
            containerSubasta.id ="divSubasta" + vGrp.cod;
            containerSubasta.setAttribute("style","position:absolute;top:0px;left:" + vGrp.wApDatosItem  +  "px;");

            containerScroll.appendChild(containerSubasta); //divSubastavGrp.cod del divScrollvGrp.cod
	    <%end if%>

        var containerOferta = document.createElement("div");
        containerOferta.name ="divOferta" + vGrp.cod;
        containerOferta.id ="divOferta" + vGrp.cod;
        containerOferta.setAttribute("style","position:absolute;top:0px;left:" + (vGrp.wApDatosItem + vGrp.wApSubasta) + "px;z-index:210;");

        containerScroll.appendChild(containerOferta); //divOfertavGrp.cod del divScrollvGrp.cod

	    <%if oProceso.PedirAlterPrecios then%>
            var containerMasPrecios = document.createElement("div");
            containerMasPrecios.name ="divMasPrecios" + vGrp.cod;
            containerMasPrecios.id ="divMasPrecios" + vGrp.cod;
            containerMasPrecios.setAttribute("style","position:absolute;top:0px;left:" + (vGrp.wApDatosItem + vGrp.wApSubasta + vGrp.wApOferta) +  "px;");

            containerScroll.appendChild(containerMasPrecios); //divMasPreciosvGrp.cod del divScrollvGrp.cod
	    <%end if%>

        if (vGrp.atribsItems.length>0){
            var containerAtributos = document.createElement("div");
            containerAtributos.name ="divAtributos" + vGrp.cod;
            containerAtributos.id ="divAtributos" + vGrp.cod;
            containerAtributos.setAttribute("style","position:absolute;top:0px;left:" + (vGrp.wApDatosItem + vGrp.wApSubasta + vGrp.wApOferta + vGrp.wApMasPrecios) +  "px;");

            containerScroll.appendChild(containerAtributos); //divAtributosvGrp.cod del divScrollvGrp.cod   
	    }

        containerApartado.appendChild(containerScroll); //divScrollvGrp.cod del divApartadoSvGrp.cod

        container.appendChild(containerApartado); //divApartadoSvGrp.cod del div_vGrp.cod

        p.document.getElementById("divItems").appendChild(container); //div_vGrp.cod

        p.document.getElementById("divCabeceraScroll" + vGrp.cod ).style.clip="rect(0px,auto,auto,0px)"
        p.document.getElementById("divCabecera" + vGrp.cod ).style.clip="rect(0px,auto,auto,0px)"
        //p.document.getElementById("divArticuloScroll" + vGrp.cod ).style.clip="rect(0px," + vGrp.wApArticulo + "px,auto,0px)"
        p.document.getElementById("divArticulo" + vGrp.cod ).style.clip="rect(0px," + vGrp.wApArticulo + "px,auto,0px)"	
    }

    p.document.getElementById("div" + vGrp.cod).style.overflow="hidden"
    p.document.getElementById("div" + vGrp.cod).style.top = p.document.getElementById("divApartado").style.top
    p.document.getElementById("div" + vGrp.cod).style.left = p.document.getElementById("divApartado").style.left
    p.document.getElementById("div" + vGrp.cod).style.height = p.document.getElementById("divApartado").style.height
    p.document.getElementById("div" + vGrp.cod).style.width = p.document.getElementById("divApartado").style.width
    p.document.getElementById("div" + vGrp.cod).style.clip = p.document.getElementById("divApartadoS").style.clip
    p.document.getElementById("divScroll" + vGrp.cod ).style.height=parseFloat(p.document.getElementById("divApartadoS").style.height) - p.hCabecera
    p.document.getElementById("divScroll" + vGrp.cod ).style.width=parseFloat(p.document.getElementById("div" + vGrp.cod).style.width)- vGrp.wApArticulo
    p.document.getElementById("divArticulo" + vGrp.cod ).style.width = vGrp.wApArticulo
    p.document.getElementById("divDatosItem" + vGrp.cod ).style.width = vGrp.wApDatosItem

    <%if oProceso.subasta and not sobrecerrado and (not sinpujas) and ((oProceso.VerPrecioPuja) or (oproceso.VerProveedor)) then%>
	    p.document.getElementById("divSubasta" + vGrp.cod ).style.width = vGrp.wApSubasta
    <%end if%>
    p.document.getElementById("divOferta" + vGrp.cod ).style.width = vGrp.wApOferta
    <%if oProceso.PedirAlterPrecios then%>
	    p.document.getElementById("divMasPrecios" + vGrp.cod ).style.width = vGrp.wApMasPrecios
    <%end if%>
    if (vGrp.atribsItems.length>0){
	    p.document.getElementById("divAtributos" + vGrp.cod ).style.width = vGrp.wApAtributos
    }

    p.document.getElementById("div" + vGrp.cod).style.visibility="visible"
    p.document.getElementById("divApartadoS").style.visibility="hidden"
    p.document.getElementById("divApartado").style.visibility="hidden"
}

/*
''' <summary>
''' Mostrar por pantalla la caja de precio unitario para el item
''' </summary>
''' <param name="sName">Nombre del elelmento</param>
''' <param name="lSize">Tamaño</param>
''' <param name="lLenMax">Longitud máxima</param>
''' <param name="dValue">valor</param>
''' <param name="valorMin">valor mínimo</param>
''' <param name="valorMax">valor máximo</param>
''' <param name="mas"></param>
''' <param name="sFuncValidacion">Función de validación</param>
''' <param name="sFuncFoco">Función de Foco</param>
''' <param name="errMsg"></param>
''' <param name="errLimites"></param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cargaritems.asp; Tiempo máximo: 0</remarks>
''' <autor>JVS</autor>
*/
function inputNumeroItem (sName, lSize, lLenMax, dValue, valorMin, valorMax, mas, sFuncValidacion, sFuncFoco, errMsg, errLimites)
{
    var s
    if (!sFuncValidacion)
     sFuncValidacion = "validarNumero("
 
        if (vGrp.Bloqueado != 1) 
        {
            s = "<input " + (mas?mas:"") + " language='javascript' class='numero' style='width:" + lSize + "px' type=text name=" + sName + " id =" + sName + " maxlength=" + lLenMax + " onkeypress='return mascaraNumero(this,vdecimalfmt,vthousanfmt,event);' onchange='return " + sFuncValidacion + "this,vdecimalfmt,vthousanfmt,100," + valorMin + "," + valorMax + ",\"" + errMsg + "\",\"" + errLimites + "\")' onfocus='return " + sFuncFoco + ")' value='" + num2str(dValue,vdecimalfmt,vthousanfmt,100) + "' >"
        }
        if (vGrp.Bloqueado == 1) 
        {
            s = num2str(dValue,vdecimalfmt,vthousanfmt,100)
        }
    return s
}

function prepararCabecera(vGrp)
{
    var strCabArticulo
    var strCabDatosItem
    var strCabOferta
    var strCabMasPrecios
    var strCabAtributos
    var strPrimArticulo
    var strPrimDatosItem
    var strPrimOferta
    var strPrimMasPrecios
    var strPrimAtributos

    strCabArticulo = ""
    strCabDatosItem = ""
    strCabSubasta = ""
    strCabOferta = ""
    strCabMasPrecios  = ""
    strCabAtributos = ""

    strPrimArticulo = ""
    strPrimDatosItem = ""
    strPrimSubasta = ""
    strPrimOferta = ""
    strPrimMasPrecios = ""
    strPrimAtributos = ""

    <%
    '@@@
    'ARTÍCULO
    %>

    <%if oGrupo.defEspecificacionesItem then%>
        strCabArticulo += "		<TD class='cabecera'>"
        strCabArticulo += "			<div style='width:" + p.wCEsp + "px;height:" + p.hCFijo + "px;overflow:hidden;'><%=JSText(den(6))%></div>"
        strCabArticulo += "		</TD>"
    <%end if%>
    strCabArticulo += "		<TD class='cabecera'>"
    strCabArticulo += "			<div style='width:" + p.wCCodArt + "px;height:" + p.hCFijo + "px;overflow:hidden;'><%=JSText(den(7))%></div>"
    strCabArticulo += "		</TD>"
    strCabArticulo += "		<TD class='cabecera'>"
    strCabArticulo += "			<div style='width:" + p.wCDenArt + "px;height:" + p.hCFijo + "px;overflow:hidden;'><%=JSText(den(8))%></div>"
    strCabArticulo += "		</TD>"

    if(p.wCDenAnyoImputacion > 0)
    {
        strCabArticulo += "		<TD class='cabecera'>"
        strCabArticulo += "			<div style='width:" + p.wCDenAnyoImputacion + "px;height:" + p.hCFijo + "px;overflow:hidden;'><%=JSText(den(36))%></div>"
        strCabArticulo += "		</TD>"
    }
    
    <%
    '@@@
    'DATOS DEL ITEM
    %>

    <%if oProceso.DefDestino=3 or (oProceso.DefDestino=2 and oGrupo.DefDestino=false) then%>
	    strCabDatosItem += "		<TD class='cabecera'>"
	    strCabDatosItem += "			<div style='width:" + p.wCDestino + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(9))%></nobr></div>"
	    strCabDatosItem += "		</TD>"
    <%end if%>

    strCabDatosItem += "		<TD class='cabecera' style='width:" + p.wCCantidad + "px;'>"
    strCabDatosItem += "			<div style='width:" + p.wCCantidad + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(10))%></nobr></div>"
    strCabDatosItem += "		</TD>"
    strCabDatosItem += "		<TD class='cabecera' style='width:" + p.wCUnidad + "px;'>"
    strCabDatosItem += "			<div style='width:" + p.wCUnidad + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(11))%></nobr></div>"
    strCabDatosItem += "		</TD>"

    <%if oGrupo.hayEscalados then%>
        //Si hay escalados, el íltimo precio y el objetivo
        // van junto con la comuna  de rangos en la sección de subasta   
    <%else%>
        strCabDatosItem += "		<TD class='cabecera' style='width:" + p.wCOfertaActual + "px;'>"
        strCabDatosItem += "			<div style='width:" + p.wCOfertaActual + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(12))%></nobr></div>" //Último precio
        strCabDatosItem += "		</TD>"
        <% 
        if objetivosPublicados then %>
            strCabDatosItem += "		<TD class='cabecera' style='width:" + p.wCObjetivo + "px;' >"
            strCabDatosItem += "			<div style='width:" + p.wCObjetivo + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(26))%></nobr></div>"
            strCabDatosItem += "		</TD>"    
        <% end if
    end if%>

    <%if oProceso.DefFechasSum=3 or (oProceso.DefFechasSum=2 and oGrupo.DefFechasSum=false) then%>
	    strCabDatosItem += "		<TD class='cabecera'>"
	    strCabDatosItem += "			<div style='width:" + p.wCFecIni + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(13))%></nobr></div>"
	    strCabDatosItem += "		</TD>"
	    <%if oProceso.MostrarFinSum then%>
		    strCabDatosItem += "		<TD class='cabecera'>"
		    strCabDatosItem += "			<div style='width:" + p.wCFecFin + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(14))%><nobr></div>"
		    strCabDatosItem += "		</TD>"
	    <%end if
    end if
    if oProceso.DefFormaPago=3 or (oProceso.DefFormaPago=2 and oGrupo.DefFormaPago=false) then%>
	    strCabDatosItem += "		<TD class='cabecera'>"
	    strCabDatosItem += "			<div style='width:" + p.wCFormaPago + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(15))%></nobr></div>"
	    strCabDatosItem += "		</TD>"
    <%end if%>

    <%
    '@@@
    'SUBASTAS
    %>

    <%if oProceso.Subasta and not sobrecerrado and (not sinpujas) and ((oProceso.VerPrecioPuja) or (oproceso.VerProveedor)) then%>
	    <%if oProceso.VerProveedor then %>
		    strCabSubasta += "		<TD class='cabecera'>"
		    strCabSubasta += "			<div style='width:" + p.wCProveedor + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(22))%></nobr></div>"
		    strCabSubasta += "		</TD>"
	    <%end if
        if (oProceso.VerPrecioPuja) then %>
	        strCabSubasta += "		<TD class='cabecera'>"  
	        strCabSubasta += "			<div style='width:" + p.wCOfertaMinima + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(23))%></nobr></div>"
	        strCabSubasta += "		</TD>"
        <%end if
    end if%>

    <%
    '@@@
    'OFERTA
    %>

    <%if oGrupo.hayEscalados then%>
        //Si hay escalados, el último precio y el objetivo
        // van junto con la comuna  de rangos en la sección de subasta

        strCabOferta += "		<TD class='cabecera'>"
        strCabOferta += "			<div style='width:" + p.wCOfertaActual + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(35))%></nobr></div>" //Rango
        strCabOferta += "		</TD>"


        strCabOferta += "		<TD class='cabecera'>"
        strCabOferta += "			<div style='width:" + p.wCOfertaActual + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(12))%></nobr></div>" //último precio
        strCabOferta += "		</TD>"

        <%if objetivosPublicados then %>
            strCabOferta += "		<TD class='cabecera'>"
            strCabOferta += "			<div style='width:" + p.wCObjetivo + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(26))%></nobr></div>"
            strCabOferta += "		</TD>"
        <%end if
    end if%>

    strCabOferta += "		<TD class='cabecera'>"
    strCabOferta += "			<div style='width:" + p.wCPU + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(16))%></nobr></div>"
    strCabOferta += "		</TD>"
    strCabOferta += "		<TD class='cabecera'>"
    strCabOferta += "			<div name=divPrecios1_" + vGrp.cod + " id=divPrecios1_" + vGrp.cod + " style='width:" + p.wCTotalLinea + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(27))%>&nbsp;<%=mon%></nobr></div>"
    strCabOferta += "		</TD>"
    strCabOferta += "		<TD class='cabecera' title='<%=JSText(den(24))%>'>"
    strCabOferta += "			<div style='width:" + p.wCComentario + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(30))%></nobr></div>"
    strCabOferta += "		</TD>"
    <%if oProceso.SolicitarCantMax then%>
	    strCabOferta += "		<TD class='cabecera'>"
	    strCabOferta += "			<div style='width:" + p.wCCantMax + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(17))%></nobr></div>"
	    strCabOferta += "		</TD>"
    <%end if
    if oProceso.defAdjunItem then%>
	    strCabOferta += "		<TD class='cabecera'>"
	    strCabOferta += "			<div style='width:" + p.wCAdjunItem + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(20))%></nobr></div>"
	    strCabOferta += "		</TD>"
    <%end if%>

    <%if oProceso.PedirAlterPrecios then%>
	    <%
	    '@@@
	    'MAS PRECIOS
	    %>
	    strCabMasPrecios += "		<TD class='cabecera'>"
	    strCabMasPrecios += "			<div style='width:" + p.wCPU2 + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(18))%></nobr></div>"
	    strCabMasPrecios += "		</TD>"
	    strCabMasPrecios += "		<TD class='cabecera'>"
	    strCabMasPrecios += "			<div name=divPrecios2_" + vGrp.cod + " id=divPrecios2_" + vGrp.cod + " style='width:" + p.wCTotalLinea2 + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(27))%>&nbsp;<%=mon%></nobr></div>"
	    strCabMasPrecios += "		</TD>"
	    strCabMasPrecios += "		<TD class='cabecera' title='<%=JSText(den(24))%>'>"
	    strCabMasPrecios += "			<div style='width:" + p.wCComentario + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(30))%></nobr></div>"
	    strCabMasPrecios += "		</TD>"
	    strCabMasPrecios += "		<TD class='cabecera'>"
	    strCabMasPrecios += "			<div style='width:" + p.wCPU3 + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(19))%></nobr></div>"
	    strCabMasPrecios += "		</TD>"
	    strCabMasPrecios += "		<TD class='cabecera'>"
	    strCabMasPrecios += "			<div name=divPrecios3_" + vGrp.cod + " id=divPrecios3_" + vGrp.cod + " style='width:" + p.wCTotalLinea3 + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(27))%>&nbsp;<%=mon%></nobr></div>"
	    strCabMasPrecios += "		</TD>"
	    strCabMasPrecios += "		<TD class='cabecera' title='<%=JSText(den(24))%>'>"
	    strCabMasPrecios += "			<div style='width:" + p.wCComentario + "px;height:" + p.hCFijo + "px;overflow:hidden;'><nobr><%=JSText(den(30))%></nobr></div>"
	    strCabMasPrecios += "		</TD>"
    <%end if%>

    <%
    '@@@
    'ATRIBUTOS
    %>

    var i
    var wCAtributo
    var wAptdoAtributos
    wAptdoAtributos=0
    for (i = 0;i<vGrp.atribsItems.length;i++){
	    wCAtributo= p.obtenAnchoAtributo(vGrp.atribsItems[i])
	    strCabAtributos += "		<TD class='cabecera'>"
	    strCabAtributos += "			<div style='width:" + wCAtributo + "px;height:" + p.hCFijo + "px;overflow:hidden;'><table width=100% cellspacing=0 cellpading=0><tr><td><a class=ayuda href='javascript:void(null)' onfocus='return OcultarDivCostesDescItem()' onclick=\"" + p.linkAtrib(vGrp.atribsItems[i],3) + "\">" + (vGrp.atribsItems[i].obligatorio?"(*)":"") + vGrp.atribsItems[i].den + "</a></td><td valign = top align = right><a class=ayuda href='javascript:void(null)' onclick=\"" + p.linkAtrib(vGrp.atribsItems[i],3) + "\"><IMG border = 0 SRC='../images/masinform.gif'></a></td></tr></table></div>"
	    strCabAtributos += "<input type=hidden name=txtIDAtribItem_" + vGrp.cod + "_" + i + " value='" + vGrp.atribsItems[i].paid + "'>"
	    strCabAtributos += "<input type=hidden name=txttipoIDAtribItem_" + vGrp.cod + "_" + i + " value='" + vGrp.atribsItems[i].tipo + "'>"
	    strCabAtributos += "<input type=hidden name=txtAplicarAtribItem_" + vGrp.cod + "_" + i + " value='" + vGrp.atribsItems[i].aplicar + "'>"
	    strCabAtributos += "		</TD>"
	    wAptdoAtributos += wCAtributo
	}

    <%
    '@@@
    'PRIMERA FILA
    %>

    strPrimArticulo += "		<TD class='cabecera' colspan=" + vGrp.colspanArticulo.toString() + ">"
    strPrimArticulo += "			<div style='width:" + (p.wCEsp + p.wCCodArt + p.wCDenArt + p.wCDenAnyoImputacion) + "px;height:" + p.hCFijo + "px;overflow:hidden;'><%=JSText(den(1))%></div>"
    strPrimArticulo += "		</TD>"
    strPrimDatosItem += "		<TD class='cabecera' style='vertical-align:top;' rowspan=2>"
    strPrimDatosItem += "			<div style='width:" + p.wCSeparacion + "px;height:" + (p.hCFijo*2) + "px;overflow:hidden;'><a href='javascript:void(null);' title='<%=JSText(den(2))%>' onfocus='return OcultarDivCostesDescItem()' onclick='omDatosItem(\"" + vGrp.cod + "\")'><IMG style='position:absolute;top:4px;left:4px;visibility:visible;'name=imgDatosItemI_" + vGrp.cod + " border=0 SRC=images/izquierdab.gif><IMG style='position:absolute;top:4px;left:4px;visibility:hidden;' name=imgDatosItemD_" + vGrp.cod + " border=0 SRC=images/derechab.gif></a></div>"
    strPrimDatosItem += "		</TD>"
    strPrimDatosItem += "		<TD class='cabecera' colspan=" + vGrp.colspanDatosItem.toString() + ">"

    <%if oGrupo.hayEscalados then%>
	    strPrimDatosItem += "			<div style='width:" + (p.wCDestino + p.wCCantidad + p.wCUnidad + p.wCFecIni + p.wCFecFin + p.wCFormaPago) + "px;height:" + p.hCFijo + "px;overflow:hidden;'><%=JSText(den(2))%></div>"
    <%else%>
	    strPrimDatosItem += "			<div style='width:" + (p.wCDestino + p.wCCantidad + p.wCUnidad + p.wCOfertaActual + p.wCObjetivo + p.wCFecIni + p.wCFecFin + p.wCFormaPago) + "px;height:" + p.hCFijo + "px;overflow:hidden;'><%=JSText(den(2))%></div>"
    <%end if%>
    strPrimDatosItem += "		</TD>"

    <%if oProceso.subasta and not sobrecerrado and (not sinpujas) and ((oProceso.VerPrecioPuja) or (oproceso.VerProveedor)) then%>
	    strPrimSubasta += "		<TD class='cabecera' style='vertical-align:top;' rowspan=2>"
	    strPrimSubasta += "			<div style='width:" + p.wCSeparacion + "px;height:" + (p.hCFijo*2) + "px;overflow:hidden;'><a  href='javascript:void(null);' title='<%=JSText(den(25))%>' onfocus='return OcultarDivCostesDescItem()' onclick='omSubasta(\"" + vGrp.cod + "\")'><IMG style='position:absolute;top:4px;left:4px;visibility:visible;'name=imgSubastaI_" + vGrp.cod + " border=0 SRC=images/izquierdab.gif><IMG style='position:absolute;top:4px;left:4px;visibility:hidden;' name=imgSubastaD_" + vGrp.cod + " border=0 SRC=images/derechab.gif></a></div>"
	    strPrimSubasta += "		</TD>"
	    strPrimSubasta += "		<TD class='cabecera' colspan=" + vGrp.colspanSubasta.toString() + ">"
	    strPrimSubasta += "			<div style='width:" + (p.wcProveedor + p.wCOfertaMinima) + "px;height:" + p.hCFijo + "px;overflow:hidden;'><%=JSText(den(25))%></div>"
	    strPrimSubasta += "		</TD>"
    <%end if%>

    strPrimOferta += "		<TD class='cabecera' style='vertical-align:top;' rowspan=2>"
    strPrimOferta += "			<div style='width:" + p.wCSeparacion + "px;height:" + (p.hCFijo*2) + "px;overflow:hidden;'><a href='javascript:void(null);' title='<%=JSText(den(3))%>' onfocus='return OcultarDivCostesDescItem()' onclick='omPrecios(\"" + vGrp.cod + "\")'><IMG style='position:absolute;top:4px;left:4px;visibility:visible;'name=imgOfertaI_" + vGrp.cod + " border=0 SRC=images/izquierdab.gif><IMG style='position:absolute;top:4px;left:4px;visibility:hidden;' name=imgOfertaD_" + vGrp.cod + " border=0 SRC=images/derechab.gif></a></div>"
    strPrimOferta += "		</TD>"
    strPrimOferta += "		<TD class='cabecera' colspan=" + vGrp.colspanPrecios.toString() + ">"
    if (!vGrp.importe) vGrp.importe = <%=JSNum(oGrupo.importeTotal * equiv)%> 
		
    <%if oGrupo.hayEscalados then%>
	    strPrimOferta += "			<div name=divCabeceraPrecios_" + vGrp.cod + " id=divCabeceraPrecios_" + vGrp.cod + " style='width:" + (p.wCPU + p.wCTotalLinea + p.wCComentario + p.wCCantMax + p.wCAdjunItem + p.wCOfertaActual + p.wCObjetivo) + "px;height:" + p.hCFijo + "px;overflow:hidden;'><%=den(28)%> " + vGrp.cod + "&nbsp;<%=den(27)%>: " + num2str(vGrp.importe,vdecimalfmt,vthousanfmt,vprecisionfmt) + "&nbsp;<%=mon%></div>"
    <%else%>
	    strPrimOferta += "			<div name=divCabeceraPrecios_" + vGrp.cod + " id=divCabeceraPrecios_" + vGrp.cod + " style='width:" + (p.wCPU + p.wCTotalLinea + p.wCComentario + p.wCCantMax + p.wCAdjunItem ) + "px;height:" + p.hCFijo + "px;overflow:hidden;'><%=den(28)%> " + vGrp.cod + "&nbsp;<%=den(27)%>: " + num2str(vGrp.importe,vdecimalfmt,vthousanfmt,vprecisionfmt) + "&nbsp;<%=mon%></div>"
    <%end if%>
    strPrimOferta += "		</TD>"

    <%if oProceso.PedirAlterPrecios then%>
	    strPrimMasPrecios += "		<TD class='cabecera' style='vertical-align:top;' rowspan=2>"
	    strPrimMasPrecios += "			<div style='width:" + p.wCSeparacion + "px;height:" + (p.hCFijo*2) + "px;overflow:hidden;'><a href='javascript:void(null);' title='<%=JSText(den(4))%>' onfocus='return OcultarDivCostesDescItem()' onclick='omMasPrecios(\"" + vGrp.cod + "\")'><IMG style='position:absolute;top:4px;left:4px;visibility:visible;'name=imgMasPreciosI_" + vGrp.cod + " border=0 SRC=images/izquierdab.gif><IMG style='position:absolute;top:4px;left:4px;visibility:hidden;' name=imgMasPreciosD_" + vGrp.cod + " border=0 SRC=images/derechab.gif></a></div>"
	    strPrimMasPrecios += "		</TD>"
	    strPrimMasPrecios += "		<TD class='cabecera' colspan=" + vGrp.colspanMasPrecios.toString() + ">"
	    strPrimMasPrecios += "			<div style='width:" + (p.wCPU2 + p.wCTotalLinea2 + p.wCComentario +  p.wCPU3 + p.wCTotalLinea3 + p.wCComentario) + "px;height:" + p.hCFijo + "px;overflow:hidden;'><%=JSText(den(4))%></div>"
	    strPrimMasPrecios += "		</TD>"
    <%end if%>

    if (vGrp.atribsItems.length>0){
	    strPrimAtributos += "		<TD class='cabecera' style='vertical-align:top;' rowspan=2>"
	    strPrimAtributos += "			<div style='width:" + p.wCSeparacion + "px;height:" + (p.hCFijo*2) + "px;overflow:hidden;'><a href='javascript:void(null);' title='<%=JSText(den(5))%>' onfocus='return OcultarDivCostesDescItem()' onclick='omAtributos(\"" + vGrp.cod + "\")'><IMG style='position:absolute;top:4px;left:4px;visibility:visible;'name=imgAtributosI_" + vGrp.cod + " border=0 SRC=images/izquierdab.gif><IMG style='position:absolute;top:4px;left:4px;visibility:hidden;' name=imgAtributosD_" + vGrp.cod + " border=0 SRC=images/derechab.gif></a></div>"
	    strPrimAtributos += "		</TD>"
	    strPrimAtributos += "		<TD class='cabecera' colspan=" + vGrp.colspanAtributos.toString() + ">"
	    strPrimAtributos += "			<div style='width:" + wAptdoAtributos + "px;height:" + p.hCFijo + "px;overflow:hidden;'><%=JSText(den(5))%></div>"
	    strPrimAtributos += "		</TD>"
    }

    strArticuloC="<TABLE><TR>" + strPrimArticulo + "</TR><TR>" + strCabArticulo + "</TR></TABLE>"
    strDatosItemC="<TABLE><TR>"  + strPrimDatosItem + "</TR><TR>" + strCabDatosItem + "</TR></TABLE>"
    strSubastaC="<TABLE><TR>"  + strPrimSubasta + "</TR><TR>" + strCabSubasta + "</TR></TABLE>"
    strOfertaC="<TABLE><TR>" + strPrimOferta + "</TR><TR>" + strCabOferta + "</TR></TABLE>"
    strMasPreciosC="<TABLE><TR>" + strPrimMasPrecios +  "</TR><TR>" + strCabMasPrecios +  "</TR></TABLE>"

    if (vGrp.atribsItems.length>0){
	    strAtributosC="<TABLE><TR>" + strPrimAtributos + "</TR><TR>" + strCabAtributos + "</TR></TABLE>"
    }

    p.document.getElementById("divArticuloC" + vGrp.cod).innerHTML += strArticuloC
    p.document.getElementById("divDatosItemC" + vGrp.cod).innerHTML +=strDatosItemC
    <%if oProceso.subasta and not sobrecerrado  and (not sinpujas) and ((oProceso.VerPrecioPuja) or (oproceso.VerProveedor)) then%>
        p.document.getElementById("divSubastaC" + vGrp.cod).innerHTML += strSubastaC
    <%end if%>
    p.document.getElementById("divOfertaC" + vGrp.cod).innerHTML += strOfertaC
    <%if oProceso.PedirAlterPrecios then%>
        p.document.getElementById("divMasPreciosC" + vGrp.cod).innerHTML +=strMasPreciosC
    <%end if%>
    if (vGrp.atribsItems.length>0){
        p.document.getElementById("divAtributosC" + vGrp.cod).innerHTML += strAtributosC
    }
}

<%if not bItemsCargados then oGrupo.cargarAtributos CiaComp,3%>
    </script>

    <%response.write "<p>Atributos del grupo cargados:" & now() & "</P>"%>

    <script>
<%
    set oGrupo.Proceso = oProceso
    oGrupo.cargarItems Idioma,CiaComp,mon,prove,ofe,false
    oOfeGrupo.cargarPrecios ciacomp,mon        
    if oOfeGrupo.grupo.hayEscalados then
	    oOfeGrupo.cargarPreciosEscalados ciacomp
    end if
%>
    </script>

    <%response.write "<p>Items cargados:" & now() & "</P>"%>

    <script>
lP++
p.progreso (lP * 100/lTotal)


function insertarCelda (TR,sClase,sHTML,vAlign, iRowSpan){
	var oTD
	oTD = TR.insertCell(-1)
	oTD.className=sClase

	if (vAlign)
		oTD.style.verticalAlign=vAlign
	
	if (iRowSpan)
		oTD.rowSpan = iRowSpan

    oTD.innerHTML += sHTML
}

<%
if not bSoloCargar then
    if not oGrupo.AtributosItem is nothing then
        for each oAtrib in oGrupo.AtributosItem%>
	        oObj.anyaAtribItems(new p.Atributo (<%=oAtrib.id%>,<%=oAtrib.paid%>,'<%=JSText(oAtrib.cod)%>','<%=JSText(oAtrib.den)%>',<%=oAtrib.intro%>,<%=oAtrib.tipo%>,<%
	        select case oAtrib.tipo
		        case 1:
			        response.write "'" & JSText(oAtrib.valor) & "'," & JSBool(oAtrib.obligatorio) & ",null,null"
		        case 2:
			        response.write  JSNum(oAtrib.valor) & "," & JSBool(oAtrib.obligatorio) & "," & JSNum(oAtrib.valorMin) &  "," & JSNum(oAtrib.valorMax) 
		        case 3:
			        response.write  JSDate(oAtrib.valor) & "," & JSBool(oAtrib.obligatorio) & "," & JSDate(oAtrib.valorMin) & "," & JSDate(oAtrib.valorMax) 
		        case 4:
			        response.write  JSBool(oAtrib.valor) & "," & JSBool(oAtrib.obligatorio) & ", null, null"
	        end select
	
	        if oAtrib.intro = 1 then
		        str=",["
		        for each oValor in oatrib.Valores
			        if str<>",[" then
				        str = str & ","
			        end if
			        select case oAtrib.tipo
				        case 1:
					        str = str & "'" & JSText(oValor.Valor) & "'"
				        case 2:
					        if (oAtrib.operacion = "+" or  oAtrib.operacion = "-") then
						        vValor = oValor.Valor / oProceso.Cambio * equiv
					        else
						        vValor = oValor.Valor 
					        end if
					        str = str & JSNum(vValor)
				        case 3:
					        str = str & JSDate(oValor.Valor)
				        case 4:
					        str = str & JSBool(oValor.Valor)
			        end select
		        next
		        response.write str & "],oObj,'" & JSText(oAtrib.operacion) & "',"  & JSNum(oAtrib.aplicar) & ", null,null))"
	        else
		        response.write ",null,oObj,'" & JSText(oAtrib.operacion) & "',"  & JSNum(oAtrib.aplicar) & ", null,null))"		
	        end if
        next 
    end if
end if
%>

configurarItems(oObj)

<%if not bSoloCargar then %>
    prepararDivsItems(oObj)
    prepararCabecera(oObj)
<%end if %>

    lP++
    p.progreso (lP * 100/lTotal)    

    vGrp =oObj

<%if not bSoloCargar then %>
    var strDetArticulo
    var strDetDatosItem
    var strDetOferta
    var strDetMasPrecios
    var strDetAtributos

    lMaxFilas = vGrp.numItems

    strDetArticulo = ""
    strDetDatosItem = ""
    strDetOferta = ""
    strDetMasPrecios = ""
    strDetAtributos = ""
    var x
    var fila
    fila='filaImpar'

    p.document.getElementById("divArticulo" + vGrp.cod ).innerHTML += "<TABLE name='tblArticulo" + vGrp.cod + "' id='tblArticulo" + vGrp.cod + "' style='border-bottom:<%=cCabeceraTablas%> 2px solid;'></TABLE>"
    p.document.getElementById("divDatosItem" + vGrp.cod ).innerHTML +="<TABLE name='tblDatosItem" + vGrp.cod + "' id='tblDatosItem" + vGrp.cod + "' style='border-bottom:<%=cCabeceraTablas%> 2px solid;'></TABLE>"
    <%if oProceso.subasta and not sobrecerrado and (not sinpujas) and ((oProceso.VerPrecioPuja) or (oproceso.VerProveedor)) then%>
        p.document.getElementById("divSubasta" + vGrp.cod ).innerHTML += "<TABLE name='tblSubasta" + vGrp.cod + "' id='tblSubasta" + vGrp.cod + "'style='border-bottom:<%=cCabeceraTablas%> 2px solid;'></TABLE>"
    <%end if%>
    p.document.getElementById("divOferta" + vGrp.cod ).innerHTML += "<TABLE name='tblOferta" + vGrp.cod + "' id='tblOferta" + vGrp.cod + "' style='border-bottom:<%=cCabeceraTablas%> 2px solid;'></TABLE>"
    <%if oProceso.PedirAlterPrecios then%>
        p.document.getElementById("divMasPrecios" + vGrp.cod ).innerHTML += "<TABLE name='tblMasPrecios" + vGrp.cod + "' id='tblMasPrecios" + vGrp.cod + "' style='border-bottom:<%=cCabeceraTablas%> 2px solid;'></TABLE>"
    <%end if%>
    if (p.document.getElementById("divAtributos" + vGrp.cod )){
        p.document.getElementById("divAtributos" + vGrp.cod ).innerHTML += "<TABLE name='tblAtributos" + vGrp.cod + "' id='tblAtributos" + vGrp.cod + "' style='border-bottom:<%=cCabeceraTablas%> 2px solid;'></TABLE>"
    }

    j=0
<%end if %>

function dibujarFilaVacia(){
	oTR = p.document.getElementById("tblArticulo" + vGrp.cod).insertRow(-1)
	
	strVacio = "			<div style='height:" + p.hCFijo + "px;overflow:hidden;'>"
	strVacio += "&nbsp;"
	strVacio += "			</div>"

	insertarCelda(oTR,"",strVacio)
	insertarCelda(oTR,"",strVacio)
	oTR = p.document.getElementById("tblDatosItem" + vGrp.cod).insertRow(-1)
	<%if oProceso.DefDestino=3 or (oProceso.DefDestino=2 and oGrupo.DefDestino=false) then%>
		insertarCelda(oTR,"",strVacio)
	<%end if%>
	insertarCelda(oTR,"",strVacio)
	insertarCelda(oTR,"",strVacio)
	insertarCelda(oTR,"",strVacio)
	insertarCelda(oTR,"",strVacio)
	<%if oProceso.DefFechasSum=3 or (oProceso.DefFechasSum=2 and oGrupo.DefFechasSum=false) then%>
		insertarCelda(oTR,"",strVacio)
		<%if oProceso.MostrarFinSum then%>
			insertarCelda(oTR,"",strVacio)
		<%end if
	end if
	if oProceso.DefFormaPago=3 or (oProceso.DefFormaPago=2 and oGrupo.DefFormaPago=false) then%>	
		insertarCelda(oTR,"",strVacio)
	<%end if%>

	<%if oProceso.Subasta and not sobrecerrado  and (not sinpujas) and ((oProceso.VerPrecioPuja) or (oproceso.VerProveedor)) then%>
		oTR = p.document.getElementById("tblSubasta" + vGrp.cod).insertRow(-1)

		<%if oProceso.verProveedor then%>
			insertarCelda(oTR,"",strVacio)
		<%end if%>
		insertarCelda(oTR,"",strVacio)
	<%end if%>
	oTR = p.document.getElementById("tblOferta" + vGrp.cod).insertRow(-1)
	
	insertarCelda(oTR,"",strVacio)
	insertarCelda(oTR,"",strVacio)
	insertarCelda(oTR,"",strVacio)
	<%if oProceso.SolicitarCantMax then%>
		insertarCelda(oTR,"",strVacio)
	<%end if%>
	<%if oProceso.defAdjunItem then%>
		insertarCelda(oTR,"",strVacio)
	<%end if%>

	<%if oProceso.PedirAlterPrecios then%>
		oTR = p.document.getElementById("tblMasPrecios" + vGrp.cod).insertRow(-1)
		insertarCelda(oTR,"",strVacio)
		insertarCelda(oTR,"",strVacio)
		insertarCelda(oTR,"",strVacio)
		insertarCelda(oTR,"",strVacio)
		insertarCelda(oTR,"",strVacio)
		insertarCelda(oTR,"",strVacio)
	<%end if%>

	<%if not oGrupo.AtributosItem is nothing then
		%>
		if (p.document.getElementById("divAtributos" + vGrp.cod )){
			oTR = p.document.getElementById("tblAtributos" + vGrp.cod).insertRow(-1)
		}
	    <%For Each oAtrib In oGrupo.AtributosItem%>
			insertarCelda(oTR,"",strVacio)
		<%next
	end if%>	
}

//<summary>Ordena el array de escalados por cantidad inicial</summary>
//<param name="escalados">array de escalados a ordenar</param>
//<remarks>Llamada desde: dibujarItems</remarks>
function OrdenarEscalados(escalados)
{
    var i 
    var k 
    var oEscAux    

    for (i=0;i<(escalados.length-1);i++){
        for (k=i+1;k<(escalados.length);k++){
            if (escalados[k].inicio<escalados[i].inicio){
                oEscAux=escalados[i]
                escalados[i]=escalados[k]
                escalados[k]=oEscAux
            }
        }
    }    
}

/*
''' <summary>
''' funcion que dibuja cada item de la oferta
''' </summary>
''' <param name="oObj">Grupo del item</param>
''' <param name="id">Id del item</param>        
''' <param name="art">Articulo del item</param>
''' <param name="descr">Descripcion del item</param>  
''' <param name="cant">Cantidad del item</param>
''' <param name="uni">Unidad del item</param>  
''' <param name="uniden">Descripcion Unidad del item</param>
''' <param name="dest">Destino del item</param>  
''' <param name="dendest">Descripcion Destino del item</param>
''' <param name="pag">Forma pago del item</param> 
''' <param name="denpag">Descripcion Forma pago del item</param>
''' <param name="fecini">Fecha inicio suministro del item</param>        
''' <param name="fecfin">Fecha fin suministro del item</param>
''' <param name="obj">Objetivo del item</param>  
''' <param name="especadj">Especificacion Adjudicación del proceso del item</param>
''' <param name="esp">Especificacion del item</param>  
''' <param name="precio">Precio 1 del item</param>
''' <param name="precio2">Precio 2 del item</param>  
''' <param name="precio3">Precio 3 del item</param>
''' <param name="cantmax">Cantidad maxima del item</param> 
''' <param name="numadjun">Numero de adjuntos del item</param>
''' <param name="minprecio">Minimo precio del item</param>        
''' <param name="minprove">Proveedor q ha hecho la oferta minimo del item. Segun un parametro de proceso se vera o no</param>
''' <param name="minproveden">Denominacion Proveedor q ha hecho la oferta minimo del item. Segun un parametro de proceso se vera o no</param>  
''' <param name="obj">Objetivo del item</param>
''' <param name="coment1">comentario1 del item</param>  
''' <param name="coment2">comentario2 del item</param>
''' <param name="coment3">comentario3 del item</param>  
''' <param name="obsadjun">Obsservaciones de adjunto del item</param>
''' <param name="atribs">Objeto atributos de oferta del item</param> 
''' <param name="ofertaActual">Precio del proceso GS del item</param>
''' <param name="especadjI">Especificacion Adjudicación del item</param>        
''' <param name="precioNeto">precio neto del item, es dcir, sin aplicar cambio a moneda central</param>
''' <param name="importeBruto">Importe bruto del item</param>  
''' <param name="importeNeto">Importe neto del item</param>
''' <param name="usar">Si se usa el item o no</param>  
''' <param name="escalados">Escalados del item</param>
''' <remarks>Llamada desde: js\cargaritems.asp: Bucle por cada item; Tiempo máximo: 0,3</remarks>*/
function dibujarItem(oObj, id, art, descr, cant, uni, uniden, dest, dendest, pag, denpag, fecini,fecfin,obj,especadj,esp,precio, precio2, precio3, cantmax, numadjun, minprecio, minprove, minproveden, obj, coment1, 
                     coment2, coment3, obsadjun, atribs,ofertaActual,especadjI,precioNeto,importeBruto,importeNeto,usar,escalados,anyoImputacion){	
	oItem = new p.Item (oObj, id,art, descr,cant,new p.Unidad(uni,uniden), <%if oProceso.DefDestino=3 or (oProceso.DefDestino=2 and oGrupo.DefDestino=false) then%>new p.Destino(dest,dendest)<%else%>null<%end if%>, 
	pag, denpag, fecini,  fecfin, obj, especadj, esp, precio,  precio2,	precio3,  
	cantmax, numadjun,minprecio,   minprove, minproveden,ofertaActual,obj,coment1,coment2,coment3,obsadjun,atribs,especadjI,precioNeto,importeBruto,importeNeto, usar, anyoImputacion)
	oItem.escalados = escalados	
	if (oObj.hayEscalados) oItem.estableceEscalados()	

    <%if oProceso.Subasta and not sobrecerrado then%>
        for (i=0;i<p.itemsPuja.length && p.itemsPuja[i].id!= oItem.id;i++){}
        vItemPuja = p.itemsPuja[i]

        oItem.precioNeto = vItemPuja.precioNeto
        oItem.importeBruto = vItemPuja.importeBruto
        oItem.importeNeto = vItemPuja.importeNeto
    <%end if %>
	
    <%if not bSoloCargar then %>
	    var iesc
	    var idEsc
	    var nEscalados = 1
	    var escalado
	    if (oObj.hayEscalados) nEscalados = oObj.escalados.length

	    for (iesc=0; iesc<nEscalados; iesc++){	
		    if (oObj.hayEscalados){
			    idEsc = oObj.escalados[iesc].id   
			    escalado = iesc
		    }
		    else{
			    idEsc = ""			
			    escalado = -1
		    }
	
	        oTR = p.document.getElementById("tblArticulo" + vGrp.cod).insertRow(-1)
   
            <%if oGrupo.defEspecificacionesItem then%>
	            strDetArticulo = "			<div style='width:" + p.wCEsp + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"
	
	            if (oItem.hayEsp!=0 || oItem.hayEspI!=0){
		            strDetArticulo += "			<input type=hidden name=txtEspItem_" + vGrp.cod + "_" + j + " value='" + oItem.esp + "'>"
		            strDetArticulo += "<a href='javascript:void(null)' onfocus='return OcultarDivCostesDescItem()' onclick='cargarEspecificaciones(\"" +  vGrp.cod + "\"," + oItem.id + ")'><IMG border = 0 SRC=images/clip.gif></A>"		
		        }
	            else
		            strDetArticulo += "&nbsp;"
	            strDetArticulo += "			</div>"
	            if (iesc == 0){
		            insertarCelda(oTR,fila,strDetArticulo)
	            }
	            else{
		            insertarCelda(oTR,"","")
	            }		
            <%end if%>	
	        strDetArticulo = "			<input type=hidden name=txtClaseFila_" + oItem.id + "  value='" + fila + "'>"
	        strDetArticulo += "			<input type=hidden name=txtIDItem_" + vGrp.cod + "_" + j + " value='" + oItem.id + "'>"
	        strDetArticulo += "			<div style='width:" + p.wCCodArt + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"
	        strDetArticulo += oItem.cod
	        strDetArticulo += "			</div>"

	        if (iesc==0) {insertarCelda(oTR,fila,strDetArticulo)}else{insertarCelda(oTR,"","")}
	
	        strDetArticulo = "			<div style='width:" + p.wCDenArt + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"
	        if (iesc==0 )	strDetArticulo += oItem.descr
	        strDetArticulo += "			</div>"
            if (iesc==0) {insertarCelda(oTR,fila,strDetArticulo)}else{insertarCelda(oTR,"","")}
            if(p.wCDenAnyoImputacion > 0)
            {
                strDetArticulo = "			<div style='width:" + p.wCDenAnyoImputacion + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"
	            if (iesc==0 )	if(oItem.anyoImputacion>0) {strDetArticulo +=  oItem.anyoImputacion;}
                strDetArticulo += "			</div>"
                if (iesc==0) {insertarCelda(oTR,fila,strDetArticulo)}else{insertarCelda(oTR,"","")}
            }

	        oTD = oTR.insertCell(-1)
            if (iesc==0 ) oTD.className=fila
	        oTD.title = oItem.descr
	        oTD.innerHTML += strDetArticulo
	        

	        <%
	        '@@@
	        'DATOS DEL ITEM
	        %>

	        oTR = p.document.getElementById("tblDatosItem" + vGrp.cod).insertRow(-1)

	        <%if oProceso.DefDestino=3 or (oProceso.DefDestino=2 and oGrupo.DefDestino=false) then%>
		        strDetDatosItem = "			<div style='width:" + p.wCDestino + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"
		        strDetDatosItem += "<table width=100%><tr><td width=100%><a class=popUp href='javascript:void(null)' title='" + oItem.dest.den + "' onfocus='return OcultarDivCostesDescItem()' onclick='detalleDestino(\"" + oItem.dest.cod + "\")'>" + oItem.dest.cod + "</a></td>"
		        strDetDatosItem += "<td><a class=popUp href='javascript:void(null)' title='" + oItem.dest.den + "' onfocus='return OcultarDivCostesDescItem()' onclick='detalleDestino(\"" + oItem.dest.cod + "\")'><IMG border=0 SRC='../images/masinform.gif'></a></td></tr></table>"
		        strDetDatosItem += "			</div>"
		        insertarCelda(oTR,fila,strDetDatosItem)
	        <%end if%>

	        strDetDatosItem = "			<div style='width:" + p.wCCantidad + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"

	        var mostrarCantidad = false
	        if (oObj.hayEscalados){		
		        if (oItem.escalados[iesc].usar){
			        mostrarCantidad = true
		        }	
	        }
	        else{
		        mostrarCantidad = true
	        }
	
	        if (mostrarCantidad){
		        strDetDatosItem += num2str(oItem.cant,vdecimalfmt,vthousanfmt,vprecisionfmt)
		        strDetDatosItem += "			<input type=hidden name=txtCantidad_" + vGrp.cod + "_" + oItem.id + " value='" + num2str(oItem.cant,vdecimalfmt,vthousanfmt,1000) + "'>"	
	        }
	        strDetDatosItem += "			</div>"
	        insertarCelda(oTR,fila + " numero",strDetDatosItem)

	        strDetDatosItem = "			<div style='width:" + p.wCUnidad + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"
	        strDetDatosItem += "<table width=100% style='margin-top:-3px'><tr><td width=100%><a class=popUp href='javascript:void(null)' title='" + oItem.uni.den + "' onfocus='return OcultarDivCostesDescItem()' onclick='DenominacionUni(\"" + oItem.uni.cod + "\")'>" +  oItem.uni.cod +  "</A></td>"
	        strDetDatosItem += "<td><a class=popUp href='javascript:void(null)' title='" + oItem.uni.den + "' onfocus='return OcultarDivCostesDescItem()' onclick='DenominacionUni(\"" + oItem.uni.cod + "\")'><IMG border=0 SRC='../images/masinform.gif'></a></td></tr></table>"	
	        strDetDatosItem += "			</div>"

	        insertarCelda(oTR,fila,strDetDatosItem)

            <% if oGrupo.hayEscalados then %>
	            // La oferta actual y el objetivo no se muestran aquí, se mostrarán en el siguiente apartado
            <% else %>
	            sNameOfertaActual = "divOfertaActual_" + oItem.id	
	
	            strDetDatosItem = "			<div name=" + sNameOfertaActual + " id=" + sNameOfertaActual + "  style='width:" + p.wCOfertaActual + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"
	            strDetDatosItem += num2str(oItem.oferta,vdecimalfmt,vthousanfmt,vprecisionfmt)
	            strDetDatosItem += "			</div>"

	            insertarCelda(oTR,fila + " numero",strDetDatosItem)

	            sNameObjetivo = "divObjetivo_" + oItem.id
	            <% if objetivosPublicados then %>
                    strDetDatosItem = "			<div name=" + sNameObjetivo + " id=" + sNameObjetivo + "  style='width:" + p.wCObjetivo + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"
	                strDetDatosItem += num2str(oItem.objetivo,vdecimalfmt,vthousanfmt,10)
	                strDetDatosItem += "			</div>"

	                insertarCelda(oTR,fila + " numero",strDetDatosItem)
                <% end if 
            end if
    
            if oProceso.DefFechasSum=3 or (oProceso.DefFechasSum=2 and oGrupo.DefFechasSum=false) then%>
		        strDetDatosItem = "			<div style='width:" + p.wCFecIni + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"
		        strDetDatosItem += date2str(oItem.finisum,vdatefmt)
		        strDetDatosItem += "			</div>"

		        insertarCelda(oTR,fila,strDetDatosItem)

		        <%if oProceso.MostrarFinSum then%>
			        strDetDatosItem = "			<div style='width:" + p.wCFecFin + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"
			        strDetDatosItem += date2str(oItem.ffinsum,vdatefmt)
			        strDetDatosItem += "			</div>"

			        insertarCelda(oTR,fila,strDetDatosItem)
		        <%end if
	        end if
	        if oProceso.DefFormaPago=3 or (oProceso.DefFormaPago=2 and oGrupo.DefFormaPago=false) then%>
		        strDetDatosItem = "			<div style='width:" + p.wCFormaPago + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"
		        strDetDatosItem += "<a class=popUp href='javascript:void(null)' title='" + oItem.pagDen + "'>" + oItem.pag + "</a>"
		        strDetDatosItem += "			</div>"

		        insertarCelda(oTR,fila,strDetDatosItem)
	        <%end if
    
            if oProceso.Subasta and not sobrecerrado and (not sinpujas) and ((oProceso.VerPrecioPuja) or (oproceso.VerProveedor)) then	%>
		        if (minprove=="<%=prove%>" || minprove==null)
			        filaS = ""
		        else
			        filaS = " superada"

		        oTR = p.document.getElementById("tblSubasta" + vGrp.cod).insertRow(-1)

		        <%if oProceso.verProveedor then%>
			        strDetSubasta = "			<div class='" + filaS + "' name=divProve" + oItem.id + " id=divProve" + oItem.id + " style='width:" + p.wCProveedor  + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"
			        strDetSubasta += "<a class='popUp" + filaS +"' href='javascript:void(null)' title='" + (oItem.proveCod!="" ? oItem.proveDen:"") + "'>" + (oItem.proveCod!="" ? oItem.proveDen:"")  + "</a>"
			        strDetSubasta += "			</div>"
								
			        insertarCelda(oTR,(filaS==""?fila:filaS),strDetSubasta)
		        <%end if%>
                <%if oProceso.VerPrecioPuja then%>
		            strDetSubasta = "			<div name=divOfeMin" + oItem.id + " id=divOfeMin" + oItem.id + " style='width:" + p.wCOfertaMinima + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"            
        
		            <%if oProceso.verDetallePujas then%>
		                strDetSubasta += "<a class='" + filaS + " popUp' href='javascript:void(null)' sGrupo = '" + vGrp.cod + "' lItem = " + oItem.id + " sArt= '" +  oItem.cod + "' sDescr = '" + oItem.descr + "' onfocus='return OcultarDivCostesDescItem()' onclick = 'verDetallePujas(this)'>" + num2str(oItem.ofertaMinima,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</a>"
		            <%else%>
		                strDetSubasta += num2str(oItem.ofertaMinima,vdecimalfmt,vthousanfmt,vprecisionfmt)		
		            <%end if%>
        
		            strDetSubasta += "			</div>"
		            insertarCelda(oTR,(filaS==""?fila:filaS) + " numero",strDetSubasta)
	            <%end if%>
	        <%end if%>

	        <%
	        '@@@
	        'OFERTA
	        %>
	        oTR = p.document.getElementById("tblOferta" + vGrp.cod).insertRow(-1)

            <% if oGrupo.hayEscalados then %>
	            sNameRango =  "divRango_" + oItem.id

	            strDetDatosItem = "			<div name=" + sNameRango + " id=" + sNameRango + "  style='width:" + p.wCRango + "px;height:" + p.hCFijo + "px;overflow:hidden; text-align:center'>"
	            if (oObj.escalados[iesc].fin == null){
		            strDetDatosItem +=  num2str(oObj.escalados[iesc].inicio,vdecimalfmt, vthousanfmt,vprecisionfmt)
	            }
	            else{
		            strDetDatosItem +=  num2str(oObj.escalados[iesc].inicio,vdecimalfmt, vthousanfmt,vprecisionfmt) + " - " + num2str(oObj.escalados[iesc].fin,vdecimalfmt, vthousanfmt,vprecisionfmt)
	            }
	            strDetDatosItem += "			</div>"
	
	            insertarCelda(oTR,fila + " numero",strDetDatosItem)
	
	            sNameOfertaActual = "divOfertaActual_" + oItem.id + "_" + vGrp.escalados[iesc].id	
	            strDetDatosItem = "			<div name=" + sNameOfertaActual + " id=" + sNameOfertaActual + "  style='width:" + p.wCOfertaActual + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"
	            strDetDatosItem += num2str(oItem.escalados[iesc].precioGS,vdecimalfmt,vthousanfmt,vprecisionfmt)
	            strDetDatosItem += "			</div>"

	            insertarCelda(oTR,fila + " numero",strDetDatosItem)

	            sNameObjetivo = "divObjetivo_" + oItem.id
	
                <% if objetivosPublicados then %>


	                strDetDatosItem = "			<div name=" + sNameObjetivo + " id=" + sNameObjetivo + "  style='width:" + p.wCObjetivo + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"
	                strDetDatosItem += num2str(oItem.escalados[iesc].objetivo,vdecimalfmt,vthousanfmt,10)
	                strDetDatosItem += "			</div>"

	                insertarCelda(oTR,fila + " numero",strDetDatosItem)
                <% end if 
            end if%>
		
	        strDetOferta = "			<div style='width:" + p.wCPU + "px;height:" + p.hCFijo + "px;overflow:hidden;z-index:1000;'>"

            <% if oGrupo.hayEscalados then%>
                if (iesc==0 ) {
                    strDetOferta +="<INPUT type=hidden name=ItemAdjuntosCargados_" + oItem.id + " value = 0>"
                }
            <%else%>
                strDetOferta +="<INPUT type=hidden name=ItemAdjuntosCargados_" + oItem.id + " value = 0>"
            <%end if%>
	
	        var precioUsar = oItem.usar // precio al que se pueden modificar sus c/dadm
	        sNamePU = "txtPU_" + vGrp.cod + "_" + oItem.id
	        sNamePUBruto = "txtPUBruto_" + vGrp.cod + "_" + oItem.id

	        if (vGrp.hayEscalados){
		        sNamePU +=  "_" + vGrp.escalados[iesc].id 
		        sNamePUBruto += "_" + vGrp.escalados[iesc].id 
	        }
	        <%if oProceso.subasta and not sobrecerrado then%>
		        sNamePUSubasta = "txtPujaPU_" + oItem.id
		        if (p.document.forms["frmOfertaAEnviar"].elements[sNamePUSubasta])
			        {
			        for (k=0;k<p.itemsPuja.length && p.itemsPuja[k].id != oItem.id;k++)
				        {}
			        oItem.precio=p.itemsPuja[k].precio
			        oItem.mirror = new Array("itemsPuja[" + k + "]") 
			        arrLength = p.itemsPuja[k].mirror.length
			        p.itemsPuja[k].mirror[arrLength] = "proceso.grupos[" + iGrupo + "].items[" + oObj.items.length + "]"
			        }
		        arrLength = oItem.mirror.length
		        oItem.mirror[arrLength] = "proceso.grupos[" + iGrupo + "].items[" + oObj.items.length + "]"
		        <%if bConsulta then%>
			        strDetOferta += "<input type=hidden name=" + sNamePU + " id=" + sNamePU + " value='" + num2str(oItem.precioNeto,vdecimalfmt, vthousanfmt,1000) + "'>" + num2str(oItem.precioNeto,vdecimalfmt, vthousanfmt,vprecisionfmt)
		        <%else%>
                    if(precioUsar==1 || precioUsar==null)			
			        {
				        strDetOferta += inputNumeroItem (sNamePU,p.wCPU+(2*p.CIE),20,oItem.precioNeto,null,null,"cant=" + oItem.cant + " uniden='" + oItem.uni.den + "' unicod='" + oItem.uni.cod + "' pu=1 grupo='" + vGrp.cod + "' itemid=" + oItem.id + " oldValue=" + oItem.precio,"validarNumeroB(\"" + sNamePUSubasta + "\",proceso.grupos[" + iGrupo + "].items[" + oObj.items.length + "],", "MostrarDivCostesDescItem(" + oItem.id + ",\"" + vGrp.cod + "\", \"txtPU_\",\"" +  escalado + "\"" ,"<%=JSText(den(31))%>\\n<%=JSText(den(32))%> " + vdecimalfmt + "\\n<%=JSText(den(33))%> " + vthousanfmt )			
			        }
                    else
			        {
                        strDetOferta += inputNumeroItem (sNamePU,p.wCPU+(2*p.CIE),20,oItem.precio,null,null,"cant=" + oItem.cant + " uniden='" + oItem.uni.den + "' unicod='" + oItem.uni.cod + "' pu=1 grupo='" + vGrp.cod + "' itemid=" + oItem.id + " oldValue=" + oItem.precio,"validarNumeroB(\"" + sNamePUSubasta + "\",proceso.grupos[" + iGrupo + "].items[" + oObj.items.length + "],", "MostrarDivCostesDescItem(" + oItem.id + ",\"" + vGrp.cod + "\", \"txtPU_\",\"" +  escalado + "\"" ,"<%=JSText(den(31))%>\\n<%=JSText(den(32))%> " + vdecimalfmt + "\\n<%=JSText(den(33))%> " + vthousanfmt )
			        }
			        strDetOferta += "<input type=hidden name=" + sNamePUBruto + " id=" + sNamePUBruto  + " value='" + num2str(oItem.precio,vdecimalfmt, vthousanfmt,1000) + "'>"
		        <%end if%>
	        <%else%>        
		        <%if bConsulta then%>
		
                     if(precioUsar==1 || precioUsar==null)
			            strDetOferta += "<input type=hidden name=" + sNamePU + " id=" + sNamePU + " value='" + num2str(oItem.precioNeto,vdecimalfmt, vthousanfmt,1000) + "'>" + num2str(oItem.precioNeto,vdecimalfmt, vthousanfmt,vprecisionfmt)
                    else
                        strDetOferta += "<input type=hidden name=" + sNamePU + " id=" + sNamePU + " value='" + num2str(oItem.precio,vdecimalfmt, vthousanfmt,1000) + "'>" + num2str(oItem.precio,vdecimalfmt, vthousanfmt,vprecisionfmt)		
		        <%else%>		
                    if(precioUsar==1 || precioUsar==null){
				        if (vGrp.hayEscalados){
					        strDetOferta += inputNumeroItem (sNamePU,p.wCPU + (2*p.CIE),20,oItem.escalados[iesc].precioValido,null,null,"cant=" + oItem.cant + " uniden='" + oItem.uni.den + "' unicod='" + oItem.uni.cod + "' pu=1 grupo='" + vGrp.cod + "' itemid=" + oItem.id + " oldValue=" + oItem.precioNeto,"validarNumeroB(null,new Array(\"proceso.grupos[" + iGrupo + "].items[" + oObj.items.length + "]\"),", "MostrarDivCostesDescItem(" + oItem.id + ",\"" + vGrp.cod + "\", \"txtPU_\",\"" +  escalado + "\"" ,"<%=JSText(den(31))%>\\n<%=JSText(den(32))%> " + vdecimalfmt + "\\n<%=JSText(den(33))%> " + vthousanfmt )
					        strDetOferta += "<input type=hidden name=" + sNamePUBruto + " id=" + sNamePUBruto  + " value='" + num2str(oItem.escalados[iesc].precio,vdecimalfmt, vthousanfmt,1000) + "'>"
				        }
				        else{
					        strDetOferta += inputNumeroItem (sNamePU,p.wCPU + (2*p.CIE),20,oItem.precioNeto,null,null,"cant=" + oItem.cant + " uniden='" + oItem.uni.den + "' unicod='" + oItem.uni.cod + "' pu=1 grupo='" + vGrp.cod + "' itemid=" + oItem.id + " oldValue=" + oItem.precioNeto,"validarNumeroB(null,new Array(\"proceso.grupos[" + iGrupo + "].items[" + oObj.items.length + "]\"),", "MostrarDivCostesDescItem(" + oItem.id + ",\"" + vGrp.cod + "\", \"txtPU_\",\"" +  escalado + "\"" ,"<%=JSText(den(31))%>\\n<%=JSText(den(32))%> " + vdecimalfmt + "\\n<%=JSText(den(33))%> " + vthousanfmt )
					        strDetOferta += "<input type=hidden name=" + sNamePUBruto + " id=" + sNamePUBruto  + " value='" + num2str(oItem.precio,vdecimalfmt, vthousanfmt,1000) + "'>"				
				        }
                    }
                    else{
			            strDetOferta += inputNumeroItem (sNamePU,p.wCPU + (2*p.CIE),20,oItem.precio,null,null,"cant=" + oItem.cant + " uniden='" + oItem.uni.den + "' unicod='" + oItem.uni.cod + "' pu=1 grupo='" + vGrp.cod + "' itemid=" + oItem.id + " oldValue=" + oItem.precio,"validarNumeroB(null,new Array(\"proceso.grupos[" + iGrupo + "].items[" + oObj.items.length + "]\"),", "MostrarDivCostesDescItem(" + oItem.id + ",\"" + vGrp.cod + "\", \"txtPU_\",\"" +  escalado + "\"" ,"<%=JSText(den(31))%>\\n<%=JSText(den(32))%> " + vdecimalfmt + "\\n<%=JSText(den(33))%> " + vthousanfmt )
                        strDetOferta += "<input type=hidden name=" + sNamePUBruto + " id=" + sNamePUBruto  + " value='" + num2str(oItem.precio,vdecimalfmt, vthousanfmt,1000) + "'>"
                    }
		        <%end if%>
	        <%end if%>
		
	        strDetOferta += "			</div>"
	        insertarCelda(oTR,fila,strDetOferta)
	
	        if (vGrp.hayEscalados){		
	            var mostrarDivLinea = false
	            if (oObj.hayEscalados){			
		            if (oItem.escalados[iesc].usar) mostrarDivLinea = true		   
	            }
	            else{
		            mostrarDivLinea = true
	            }
	
		        strDetOferta = "			<div name=divTotalLinea1_" + oItem.id + "_" + vGrp.escalados[iesc].id + " id=divTotalLinea1_" + oItem.id + "_" + vGrp.escalados[iesc].id + " style='width:" + p.wCTotalLinea + "px;height:" + p.hCFijo + "px;overflow:hidden;z-index:1000;'>"
	        }
	        else{
		        mostrarDivLinea = true
		        strDetOferta = "			<div name=divTotalLinea1_" + oItem.id + " id=divTotalLinea1_" + oItem.id + " style='width:" + p.wCTotalLinea + "px;height:" + p.hCFijo + "px;overflow:hidden;z-index:1000;'>"
	        }

            var iBloqueado
            iBloqueado=0
            if (vGrp.Bloqueado == 1){
                iBloqueado=1
            }

	        if (oItem.precio!=null){
                //		strDetOferta += num2str(oItem.cant,vdecimalfmt,vthousanfmt,vprecisionfmt) 
                //		strDetOferta += "&nbsp;<a class=popUp href='javascript:void(null)' title='" + oItem.uni.den + "'>" +  oItem.uni.cod +  "</A>"
                //		strDetOferta += "<br>"
                var strDetOfertaAux
        
		        <%if oProceso.SolicitarCantMax then%>
		            if (oItem.cantmax <= oItem.cant)
                        if(precioUsar==1 || precioUsar==null)
    			            strDetOfertaAux = "<b>" + num2str(oItem.importeNeto,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"
                        else
                            if(precioUsar==1 || precioUsar==null)
                                strDetOfertaAux = "<b>" + num2str(oItem.importeNeto,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"
                            else
                                //strDetOfertaAux = "<b>" + num2str(oItem.precio * oItem.cantmax,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"
                                strDetOfertaAux = "<b></b>"
		            else
                        if(precioUsar==1 || precioUsar==null)
			                strDetOfertaAux = "<b>" + num2str(oItem.importeNeto,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"
                        else
                            //strDetOfertaAux = "<b>" + num2str(oItem.precio * oItem.cant,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"			
                            strDetOfertaAux = "<b></b>"			
		        <%else%>
                    if(precioUsar==1 || precioUsar==null){
			            if (mostrarDivLinea){	
				            strDetOfertaAux = "<b>" + num2str(oItem.importeNeto,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"
			            }
			            else{
				            strDetOfertaAux = "<b></b>"
			            }			
		            }
                    else{
                        //strDetOfertaAux = "<b>" + num2str(oItem.precio * oItem.cant,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"
                        strDetOfertaAux = "<b></b>"
		            }
		        <%end if%>
                strDetOferta += strDetOfertaAux
	        }	

	        insertarCelda(oTR,fila + " numero",strDetOferta)

	        strDetOferta = "			<div style='width:" + p.wCComentario + "px;height:" + p.hCFijo + "px;overflow:hidden;z-index:1000;'>"
	        sNameComent1 = "txtComent1_" + vGrp.cod + "_" + oItem.id
	
	        sNameImgComent1 = "imgtxtComent1_" + vGrp.cod + "_" + oItem.id
	        if (iesc==0){
		        strDetOferta += " <input type=hidden name=" + sNameComent1 + " value='" + coment1 + "'>"
		        strDetOferta += "<a href='javascript:void(null)' onfocus='return OcultarDivCostesDescItem()' onclick='introComentario(\"" + sNameComent1 + "\"," + iBloqueado + ")'><IMG name=" + sNameImgComent1 + " BORDER =0 SRC='images/" + (coment1==""?"comentariovacio.gif":"comentariolleno.gif") + "'></A>"
	        }	
	        strDetOferta += "			</div>"

	        insertarCelda(oTR,fila,strDetOferta)

	        <%if oProceso.SolicitarCantMax then%>
		        strDetOferta = "			<div style='width:" + p.wCCantMax + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"
		        sNameCantMax = "txtCantMax_" + vGrp.cod + "_" + oItem.id
		        <%if bConsulta then%>
			        strDetOferta += "<input type=hidden name=" + sNameCantMax + " id=" + sNameCantMax + " value='" + num2str(oItem.cantmax,vdecimalfmt,vthousanfmt,1000) + "'>" + num2str(oItem.cantmax,vdecimalfmt,vprecisionfmt,vthousanfmt)
		        <%else%>
			        strDetOferta += inputNumeroItem (sNameCantMax,p.wCCantMax,20,oItem.cantmax,null,null,null ,"actualizarEstado(2,","OcultarDivCostesDescItem(","<%=JSText(den(31))%>\\n<%=JSText(den(32))%> " + vdecimalfmt + "\\n<%=JSText(den(33))%> " + vthousanfmt )			
                    //strDetOferta += inputNumero (sNameCantMax,p.wCCantMax,20,oItem.cantmax,null,null,null ,"actualizarEstado(2,","<%=JSText(den(31))%>\\n<%=JSText(den(32))%> " + vdecimalfmt + "\\n<%=JSText(den(33))%> " + vthousanfmt)			
		        <%end if%>
		        strDetOferta += "			</div>"

		        insertarCelda(oTR,fila,strDetOferta)
	        <%end if%>
	        <%if oProceso.defAdjunItem then%>
		        strDetOferta = "			<div name=divItemAdjun_" + oItem.id + " id=divItemAdjun_" + oItem.id + " style='width:" + p.wCAdjunItem + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"
		        if (iesc==0) 
		        {	strDetOferta += "<input type=hidden name=txtObsAdjunItem_" + oItem.id + " value='" + obsadjun + "'>"
			        strDetOferta += "<a href='javascript:void(null)' onfocus='return OcultarDivCostesDescItem()' onclick='adjuntosItem(\"" + vGrp.cod + "\"," + j + ")'>"
			        strDetOferta += (oItem.numAdjuntos>0 ? oItem.numAdjuntos : "&nbsp;") + "&nbsp;"
			        strDetOferta += "<IMG border=0 SRC=images/clip.gif>"
			        strDetOferta += "</a>"
		        }
		        strDetOferta += "			</div>"

		        insertarCelda(oTR,fila,strDetOferta)
	        <%end if%>
	
	        <%if oProceso.PedirAlterPrecios then%>  
		        <%
		        '@@@
		        'MAS PRECIOS

		        'Nunca aparecerá esta sección si hay escalados
		
		        %>

		        oTR = p.document.getElementById("tblMasPrecios" + vGrp.cod).insertRow(-1)
		
		        strDetMasPrecios = "			<div style='width:" + p.wCPU2 + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"
		        sNamePU2 = "txtPU2_" + vGrp.cod + "_" + oItem.id
		        sNamePU2Bruto = "txtPU2Bruto_" + vGrp.cod + "_" + oItem.id
		        <%if bConsulta then%>
                    if(precioUsar==2)
			            strDetMasPrecios += "<input type=hidden name=" + sNamePU2 + " id=" + sNamePU2 + " value='" + num2str(oItem.precioNeto,vdecimalfmt,vthousanfmt,1000) + "'>" + num2str(oItem.precioNeto,vdecimalfmt,vprecisionfmt,vthousanfmt)
                    else
                        strDetMasPrecios += "<input type=hidden name=" + sNamePU2 + " id=" + sNamePU2 + " value='" + num2str(oItem.precio2,vdecimalfmt,vthousanfmt,1000) + "'>" +  num2str(oItem.precio2,vdecimalfmt,vprecisionfmt,vthousanfmt)
                    strDetMasPrecios += "<input type=hidden name=" + sNamePU2Bruto + " id=" + sNamePU2Bruto  + " value='" + num2str(oItem.precio2,vdecimalfmt, vthousanfmt,1000) + "'>"
		        <%else%>
                    //			strDetMasPrecios += inputNumeroItem(sNamePU2,p.wCPU2,20,oItem.precio2,null,null," cant=" + oItem.cant + " uniden='" + oItem.uni.den + "' unicod='" + oItem.uni.cod + "' pu=2 itemid=" + oItem.id,"validarNumeroB(null,null,","OcultarDivCostesDescItem(","<%=JSText(den(31))%>\\n<%=JSText(den(32))%> " + vdecimalfmt + "\\n<%=JSText(den(33))%> " + vthousanfmt)
                    // JVS - Cambio precios
                    if(precioUsar==2)
			            strDetMasPrecios += inputNumeroItem(sNamePU2,p.wCPU2 + (2*p.CIE),20,oItem.precioNeto,null,null," cant=" + oItem.cant + " uniden='" + oItem.uni.den + "' unicod='" + oItem.uni.cod + "' pu=2 itemid=" + oItem.id,"validarNumeroB(null,new Array(\"proceso.grupos[" + iGrupo + "].items[" + oObj.items.length + "]\"),", "MostrarDivCostesDescItem(" + oItem.id + ",\"" + vGrp.cod + "\", \"txtPU2_\",\"" +  escalado + "\"" ,"<%=JSText(den(31))%>\\n<%=JSText(den(32))%> " + vdecimalfmt + "\\n<%=JSText(den(33))%> " + vthousanfmt )
                    else
                        strDetMasPrecios += inputNumeroItem(sNamePU2,p.wCPU2 + (2*p.CIE),20,oItem.precio2,null,null," cant=" + oItem.cant + " uniden='" + oItem.uni.den + "' unicod='" + oItem.uni.cod + "' pu=2 itemid=" + oItem.id,"validarNumeroB(null,new Array(\"proceso.grupos[" + iGrupo + "].items[" + oObj.items.length + "]\"),", "MostrarDivCostesDescItem(" + oItem.id + ",\"" + vGrp.cod + "\", \"txtPU2_\",\"" +  escalado + "\"" ,"<%=JSText(den(31))%>\\n<%=JSText(den(32))%> " + vdecimalfmt + "\\n<%=JSText(den(33))%> " + vthousanfmt )
                    strDetMasPrecios += "<input type=hidden name=" + sNamePU2Bruto + " id=" + sNamePU2Bruto  + " value='" + num2str(oItem.precio2,vdecimalfmt, vthousanfmt,1000) + "'>"
		        <%end if%>
		        strDetMasPrecios += "			</div>"

		        insertarCelda(oTR,fila,strDetMasPrecios)

		        strDetMasPrecios = "			<div name=divTotalLinea2_" + oItem.id + " id=divTotalLinea2_" + oItem.id + " style='width:" + p.wCTotalLinea2 + "px;height:" + p.hCFijo + "px;overflow:hidden;z-index:1000;'>"

		        if (oItem.precio2!=null){
		            <%if oProceso.SolicitarCantMax then%>
			            if (oItem.cantmax <= oItem.cant)
                            if(precioUsar==2)
				                strDetMasPrecios += "<b>" + num2str(oItem.importeNeto,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"
                            else
                                //strDetMasPrecios += "<b>" + num2str(oItem.precio2 * oItem.cantmax,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"
                                strDetMasPrecios += "<b></b>"
			            else
                             if(precioUsar==2)
				                strDetMasPrecios += "<b>" + num2str(oItem.importeNeto,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"
                            else
                                //strDetMasPrecios += "<b>" + num2str(oItem.precio2 * oItem.cant,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"
                                strDetMasPrecios += "<b></b>"
		            <%else%>
			            if(precioUsar==2)
			                strDetMasPrecios += "<b>" + num2str(oItem.importeNeto,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"
                        else
                            //strDetMasPrecios += "<b>" + num2str(oItem.precio2 * oItem.cant,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"
                            strDetMasPrecios += "<b></b>"
		            <%end if%>
		        }
		        strDetMasPrecios += "			</div>"

		        insertarCelda(oTR,fila + " numero",strDetMasPrecios)

		        strDetMasPrecios = "			<div style='width:" + p.wCComentario + "px;height:" + p.hCFijo + "px;overflow:hidden;z-index:1000;'>"
		        sNameComent2 = "txtComent2_" + vGrp.cod + "_" + oItem.id
		        sNameImgComent2 = "imgtxtComent2_" + vGrp.cod + "_" + oItem.id

		        strDetMasPrecios += " <input type=hidden name=" + sNameComent2 + " value='" + coment2 + "'>"
		        strDetMasPrecios += "<a href='javascript:void(null)' onfocus='return OcultarDivCostesDescItem()' onclick='introComentario(\"" + sNameComent2 + "\"," + iBloqueado + ")'><IMG name=" + sNameImgComent2 + " BORDER =0 SRC='images/" + (coment2==""?"comentariovacio.gif":"comentariolleno.gif") + "'></A>"

		        strDetMasPrecios += "			</div>"

		        insertarCelda(oTR,fila,strDetMasPrecios)

		        strDetMasPrecios = "			<div style='width:" + p.wCPU3 + "px;height:" + p.hCFijo + "px;overflow:hidden;'>"
		        sNamePU3 = "txtPU3_" + vGrp.cod + "_" + oItem.id
                sNamePU3Bruto = "txtPU3Bruto_" + vGrp.cod + "_" + oItem.id
		        <%if bConsulta then%>
                    if(precioUsar==3)
			            strDetMasPrecios += "<input type=hidden name=" + sNamePU3 + " id=" + sNamePU3 + " value='" + num2str(oItem.precioNeto,vdecimalfmt,vthousanfmt,1000) + "'>" +  num2str(oItem.precioNeto,vdecimalfmt,vprecisionfmt,vthousanfmt)
                    else
                        strDetMasPrecios += "<input type=hidden name=" + sNamePU3 + " id=" + sNamePU3 + " value='" + num2str(oItem.precio3,vdecimalfmt,vthousanfmt,1000) + "'>" +  num2str(oItem.precio2,vdecimalfmt,vprecisionfmt,vthousanfmt)
                    strDetMasPrecios += "<input type=hidden name=" + sNamePU3Bruto + " id=" + sNamePU3Bruto  + " value='" + num2str(oItem.precio3,vdecimalfmt, vthousanfmt,1000) + "'>"
		        <%else%>
                    if(precioUsar==3)
			            strDetMasPrecios += inputNumeroItem(sNamePU3,p.wCPU3 + (2*p.CIE),20,oItem.precioNeto,null,null," cant=" + oItem.cant + " uniden='" + oItem.uni.den + "' unicod='" + oItem.uni.cod + "' pu=3 itemid=" + oItem.id,"validarNumeroB(null,new Array(\"proceso.grupos[" + iGrupo + "].items[" + oObj.items.length + "]\"),", "MostrarDivCostesDescItem(" + oItem.id + ",\"" + vGrp.cod + "\", \"txtPU3_\",\"" +  escalado + "\"" ,"<%=JSText(den(31))%>\\n<%=JSText(den(32))%> " + vdecimalfmt + "\\n<%=JSText(den(33))%> " + vthousanfmt )
                    else
                        strDetMasPrecios += inputNumeroItem(sNamePU3,p.wCPU3 + (2*p.CIE),20,oItem.precio3,null,null," cant=" + oItem.cant + " uniden='" + oItem.uni.den + "' unicod='" + oItem.uni.cod + "' pu=3 itemid=" + oItem.id,"validarNumeroB(null,new Array(\"proceso.grupos[" + iGrupo + "].items[" + oObj.items.length + "]\"),", "MostrarDivCostesDescItem(" + oItem.id + ",\"" + vGrp.cod + "\", \"txtPU3_\",\"" +  escalado + "\"" ,"<%=JSText(den(31))%>\\n<%=JSText(den(32))%> " + vdecimalfmt + "\\n<%=JSText(den(33))%> " + vthousanfmt )
                    strDetMasPrecios += "<input type=hidden name=" + sNamePU3Bruto + " id=" + sNamePU3Bruto  + " value='" + num2str(oItem.precio3,vdecimalfmt, vthousanfmt,1000) + "'>"
		        <%end if%>
		        strDetMasPrecios += "			</div>"

		        insertarCelda(oTR,fila,strDetMasPrecios)

		        strDetMasPrecios = "			<div name=divTotalLinea3_" + oItem.id + " id=divTotalLinea3_" + oItem.id + " style='width:" + p.wCTotalLinea3 + "px;height:" + p.hCFijo + "px;overflow:hidden;z-index:1000;'>"

		        if (oItem.precio3!=null){
		            <%if oProceso.SolicitarCantMax then%>
			            if (oItem.cantmax <= oItem.cant)
                            if(precioUsar==3)
				                strDetMasPrecios += "<b>" + num2str(oItem.importeNeto,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"
                            else
                                //strDetMasPrecios += "<b>" + num2str(oItem.precio3 * oItem.cantmax,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"
                                strDetMasPrecios += "<b></b>"
			            else
                            if(precioUsar==3)
				                strDetMasPrecios += "<b>" + num2str(oItem.importeNeto,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"
                            else
                                strDetMasPrecios += "<b></b>"
		            <%else%>
			            if(precioUsar==3)
			                strDetMasPrecios += "<b>" + num2str(oItem.importeNeto,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"
                        else
                            strDetMasPrecios += "<b></b>"
		            <%end if%>
		        }
		        strDetMasPrecios += "			</div>"

		        insertarCelda(oTR,fila + " numero",strDetMasPrecios)

		        strDetMasPrecios = "			<div style='width:" + p.wCComentario + "px;height:" + p.hCFijo + "px;overflow:hidden;z-index:1000;'>"

		        sNameComent3 = "txtComent3_" + vGrp.cod + "_" + oItem.id
		        sNameImgComent3 = "imgtxtComent3_" + vGrp.cod + "_" + oItem.id


		        strDetMasPrecios += " <input type=hidden name=" + sNameComent3 + " value='" + coment3 + "'>"
		        strDetMasPrecios += "<a href='javascript:void(null)' onfocus='return OcultarDivCostesDescItem()' onclick='introComentario(\"" + sNameComent3 + "\"," + iBloqueado + ")'><IMG name=" + sNameImgComent3 + " BORDER =0 SRC='images/" + (coment3==""?"comentariovacio.gif":"comentariolleno.gif") + "'></A>"

		        strDetMasPrecios += "			</div>"

		        insertarCelda(oTR,fila,strDetMasPrecios)
	        <%end if%>

	        <%
	        '@@@
	        'ATRIBUTOS

            if not oGrupo.AtributosItem is nothing then%>
		        if (p.document.getElementById("divAtributos" + vGrp.cod )){
			        oTR = p.document.getElementById("tblAtributos" + vGrp.cod).insertRow(-1)
		        }
		        x=0
	            <%For Each oAtrib In oGrupo.AtributosItem %>
			        if (atribs[x] == null){
				        oAtribOfertado = new p.valorAtribItem ( oItem, oObj.atribsItems[x],atribs[x])
			        }
			        else{
				        if (atribs[x].constructor.toString().indexOf("Array") == -1)
					        oAtribOfertado = new p.valorAtribItem ( oItem, oObj.atribsItems[x],atribs[x])
				        else
					        oAtribOfertado = new p.valorAtribItem ( oItem, oObj.atribsItems[x],atribs[x][iesc])			
			        }
						
			        oItem.anyaAtributoOfertado(oAtribOfertado)
			        wCAtributo=p.obtenAnchoAtributo(oObj.atribsItems[x])
			        strDetAtributos = "			<div style='width:" + wCAtributo + "px;height:" + p.hCFijo + "px;overflow:hidden;'>" 
			        <%if isnull(oAtrib.aplicar) or isnull(oAtrib.operacion) then %>			
				        // Atributo que no se va a aplicar por lo tanto sin escalados				
				        if (iesc==0 ) strDetAtributos += p.mostrarAtributo(oObj.atribsItems[x],oAtribOfertado,"frmOfertaAEnviar",-1,vGrp)
			        <%else%>
				        // Atributo  que puede aplicarse, por lo tanto con escalados
				        if (oObj.hayEscalados)
					        strDetAtributos += p.mostrarAtributo(oObj.atribsItems[x],oAtribOfertado,"frmOfertaAEnviar",iesc,vGrp)
				        else
					        strDetAtributos += p.mostrarAtributo(oObj.atribsItems[x],oAtribOfertado,"frmOfertaAEnviar",-1,vGrp)
			        <%end if%>			
			        strDetAtributos += "</div>"
			        insertarCelda(oTR,fila,strDetAtributos)
			        x++
		        <%next
            end if%>
	
	        if (iesc!=(nEscalados-1)){
		        if (fila=="filaImpar")
			        fila="filaPar"
		        else
			        fila="filaImpar"
	        }
        }
    <%end if%>
	    
	if (!oObj.itemsCargados) oObj.anyaItem(oItem);
}

<%cont = 0%>

<%
'JVS carga de los valores de los costes / descuentos de Ámbito item
set oAtribOfe = nothing
set	oOferta.OfertaGrupos = oRaiz.Generar_COfertaGrupos

for each oItem in oGrupo.items    
	cont = cont + 1%>
    lP++
    p.progreso (lP * 100/lTotal)
    var atribsOfer
	<%if not oGrupo.AtributosItem is nothing then%>
	    x=0
	    atribsOfer = new Array()	
	    <%for each oAtributoOfertado in oItem.PrecioItem.AtributosOfertado%>	
		    l = atribsOfer.length
		    atribsOfer[l] = <%select case oAtributoOfertado.Atributo.Tipo
			    case 1
				    response.write """" & JSText(oAtributoOfertado.valor) & """"
			    case 2
				    if (oAtributoOfertado.Atributo.operacion = "+" or  oAtributoOfertado.Atributo.operacion = "-") then
					    vValor = oAtributoOfertado.valor / oOferta.cambio * equiv
				    else
					    vValor = oAtributoOfertado.valor
				    end if
				    if oAtributoOfertado.hayEscalados then
					    response.write " new Array()"
					    for each oEscalado in oAtributoOfertado.escalados
						    if (oAtributoOfertado.Atributo.operacion = "+" or  oAtributoOfertado.Atributo.operacion = "-") then
							    vValor = oEscalado.valorNum / oOferta.cambio * equiv
						    else
							    vValor = oEscalado.valorNum
						    end if
						
						    %>
						    m = atribsOfer[l].length
						    atribsOfer[l][m] = <%if isnull(vValor) or vValor="" then response.write "null" else response.write vValor%>
						    <%
					    next					
				    else				
				    response.write  JSNum(vValor) 
				    end if
			    case 3
				    response.write JSDate(oAtributoOfertado.valor)
			    case 4
				    response.write JSBool(oAtributoOfertado.valor)
		    end select
	    next    
    end if%>

	var escalados = new Array()
	
    <%if oGrupo.hayEscalados then
	    'if oitem.escalados is nothing then
		    'Si no se han cargado recumerando una oferta
		    oItem.cargarPresEscalados(CiaComp)
	    'end if

	    for each oEscalado in oItem.Escalados	%>	        
		    //Parametros de  Escalado(id, inicio, fin, precio,precioValido,valorNum,precioGS,precioOferta,cantidad)	
		    oEsc = new Escalado (<%=oEscalado.id%>,<%=JSNUM(oEscalado.inicial)%>,<%=JSNUM(oEscalado.final)%>,<%=JSNUM(oEscalado.precio)%>,<%=JSNUM(oEscalado.precioValido)%>,null,<%=JSNUM(oEscalado.precioGS)%>,<%=JSNUM(oEscalado.precioOferta)%>,<% if isnull(oItem.cantidad) then response.write "null" else response.write oitem.cantidad%>,<%=JSNUM(oEscalado.objetivo)%>)
		    escalados[escalados.length] = oEsc
		    //oItem.anyaEscItem(oEsc)				
	    <%next
    end if%>    

    OrdenarEscalados(escalados)

    if (!oObj.itemsCargados){              
        <%
        '''Dato Objetivo proceso: Si Oferta en moneda del proceso. El cambio es el del proceso.
        '''      Ejemplo: bbdd: 10,6182662494868        Moneda: Libra           Cambio al crear proceso libra: 0,70633         Cambio actual Libra: 1,5 ---> El objetivo es 5 Libras. No 10.6 libras.
        '''Si la oferta q has leido de bd GS viene en moneda de proceso. Ya tienes el objetivo correcto, es decir, el 5. No hay q hacer nada. 
        '''     Siguiendo con el ejemplo: Recibiras el 5 directamente. Nada de 10,6182662494868 como cuando no hay oferta GS y te hace el calculo con euros. Nada de 10,6182662494868 como cuando la oferta GS existe y NO es en libras.
        '''         ¿10,6182662494868 como cuando la oferta GS existe y NO es en libras? Si, oferta gs en euros -> 7,0788441663245 al entrar y si cambias a euros (cambio 1.5) pasas a tener  10,6182662494868
        if oProceso.MonCod = mon then                               
            if (not JSNum(oItem.Objetivo) = "null") then                               
                l_Aux = oItem.Objetivo
                if (BBDDOferta=mon) then
                    l_Aux = (oItem.Objetivo * CambioUltimo) / oProceso.cambio
                else    
                    l_Aux = (oItem.Objetivo * CambioUltimo) / CambioMonedaGS
                    oItem.Objetivo = (oItem.Objetivo * oProceso.cambio) / CambioMonedaGS                    
                end if                
            end if
            if (not JSNum(oItem.precioItem.PrecioGS) = "null") then 
                '''Si el precio Gs es euros y la oferta es Libras (moneda proceso): PrecioGS / 0.706 * 0.706 = PrecioGS. 
                if (not BBDDOferta="") and (not BBDDOferta=mon) then
                    oItem.precioItem.PrecioGS = (oItem.precioItem.PrecioGS * CambioUltimo) / CambioMonedaGS
                end if
            end if%>
            dibujarItem(oObj,<%=oItem.id%>,"<%=JSText(oItem.ArticuloCod)%>","<%=JSText(oItem.Descr)%>",<%=JSNum(oItem.cantidad)%>,"<%=JSText(oItem.uniCod)%>","<%=JSText(oItem.uniDen)%>","<%=JSText(oItem.DestCod)%>",
                    <%if oProceso.DefDestino=3 or (oProceso.DefDestino=2 and oGrupo.DefDestino=false) then%>"<%=JSText(oDestinos.item(oItem.DestCod).den)%>",<%else%>null,<%end if%>"<%=JSText(oItem.PagCod)%>","<%=JSText(oItem.PagDen)%>",
                    <%=JSDate(oItem.FechaInicioSuministro)%>,<%=JSDate(oItem.FechaFinSuministro)%>,<%=JSNum(oItem.Objetivo)%>,<%=oItem.EspAdj%>,"<%=JSText(oItem.Esp)%>",<%=JSNum(oItem.precioItem.PrecioOferta / oOferta.cambio * equiv)%>,
                    <%=JSNum(oItem.precioItem.Precio2 / oOferta.cambio * equiv)%>,<%=JSNum(oItem.precioItem.Precio3 / oOferta.cambio * equiv)%>,<%=JSNum(oItem.precioItem.cantidad)%>,<%=oItem.precioItem.NumAdjuntos%>,
                    <%=JSNum(oItem.minprecio / oOferta.cambio * equiv)%>,<%if oProceso.verProveedor then%>'<%=JSText(oItem.MinProveCod)%>',   '<%=JSText(oItem.MinProveDen)%>'<%else%>null,null<%end if%>,<%=JSNum(oItem.objetivo)%>,
                    "<%=JSText(oItem.precioItem.comentario1)%>","<%=JSText(oItem.precioItem.comentario2)%>","<%=JSText(oItem.precioItem.comentario3)%>","<%=JSText(oItem.precioItem.ObsAdjuntos)%>",atribsOfer,
                    <%=JSNum(oItem.precioItem.PrecioGS / oOferta.cambio * equiv)%>,<%=oItem.EspAdjI%>, <%=JSNum(oItem.PrecioItem.PrecioNeto)%>, <%=JSNum(oItem.PrecioItem.ImporteBruto / oOferta.cambio * equiv)%>, 
                    <%=JSNum(oItem.PrecioItem.ImporteNeto / oOferta.cambio * equiv)%>, <%=JSNum(oItem.usar)  %>,escalados,<%=JSNum(oItem.AnyoImputacion)  %>)
            <%if (not JSNum(oItem.Objetivo) = "null") then
                '''Objetivo: Previamente lo has cambiado para q al aplicar el cambio te de lo mismo q gs. 
                '''Esto hay q deshacerlo, para q si cambias de moneda, la cifra de partida sea la "esperada" en la moneda "anterior". "Esperada" pq se supone q el cambio es uno y has usado otro.
                '''Ejemplo: bbdd: 10,6182662494868        Moneda: Libra           Cambio al crear proceso libra: 0,70633         Cambio actual Libra: 1,5 ---> El objetivo es 5 Libras. Aplicar un cambio de 1.5 no da 5, se ha aplicado 0,70633. Lo "esperado" es 10.6
                oItem.Objetivo = l_Aux%>
                var item=oObj.items[<%=cont-1%>];       
                item.obj = <%=JSNum(oItem.Objetivo)%>
            <%end if
        else
            if (not JSNum(oItem.Objetivo) = "null") then
                if (BBDDOferta=mon) and (BBDDOferta=oProceso.MonCod) then
                    oItem.Objetivo = oItem.Objetivo * CambioUltimo / oProceso.cambio
                else
                    '''Si la oferta q has leido de bd GS viene en moneda de proceso. Ya tienes el objetivo correcto, es decir, el 5. No hay q hacer nada. 
                    '''     Siguiendo con el ejemplo: Recibiras el 5 directamente. Nada de 10,6182662494868 como cuando no hay oferta GS y te hace el calculo con euros. Nada de 10,6182662494868 como cuando la oferta GS existe y NO es en libras.
                    '''         ¿10,6182662494868 como cuando la oferta GS existe y NO es en libras? Si, oferta gs en euros -> 7,0788441663245 al entrar y si cambias a euros (cambio 1.5) pasas a tener  10,6182662494868
                    if (not BBDDOferta ="") and (oProceso.MonCod = BBDDOferta) then
                        oItem.Objetivo = (oItem.Objetivo * CambioMonedaGS) / oProceso.cambio
                    end if
                    '''Ejemplo. GS: DOBLE Portal: Euros.     Objetivo: 14. 14 segun GS pero eso es Moneda Doble. En moneda portal (euros) es 7.
                    if (not BBDDOferta="") and (not BBDDOferta=mon) then
                        oItem.Objetivo = oItem.Objetivo * CambioUltimo / CambioMonedaGS
                    end if    
                end if
            end if
            if (not JSNum(oItem.precioItem.PrecioGS) = "null") then 
                '''Si el precio Gs es euros y la oferta es Libras (moneda proceso): PrecioGS / 0.706 * 0.706 = PrecioGS. NO se muestra en libras.
                '''Si el precio Gs es libras y la oferta es Libras (moneda proceso): PrecioGS / 0.706 * 0.706 = PrecioGS. Se muestra en libras.
                if (not BBDDOferta="") and (not BBDDOferta=mon) then
                    oItem.precioItem.PrecioGS = (oItem.precioItem.PrecioGS * CambioUltimo) / CambioMonedaGS
                end if
            end if%>
            dibujarItem(oObj,<%=oItem.id%>,"<%=JSText(oItem.ArticuloCod)%>","<%=JSText(oItem.Descr)%>",<%=JSNum(oItem.cantidad)%>,"<%=JSText(oItem.uniCod)%>","<%=JSText(oItem.uniDen)%>","<%=JSText(oItem.DestCod)%>",
                    <%if oProceso.DefDestino=3 or (oProceso.DefDestino=2 and oGrupo.DefDestino=false) then%>"<%=JSText(oDestinos.item(oItem.DestCod).den)%>",<%else%>null,<%end if%>"<%=JSText(oItem.PagCod)%>","<%=JSText(oItem.PagDen)%>",
                    <%=JSDate(oItem.FechaInicioSuministro)%>,<%=JSDate(oItem.FechaFinSuministro)%>,<%=JSNum(oItem.Objetivo)%>,<%=oItem.EspAdj%>,"<%=JSText(oItem.Esp)%>",<%=JSNum(oItem.precioItem.PrecioOferta / oOferta.cambio * equiv)%>,
                    <%=JSNum(oItem.precioItem.Precio2 / oOferta.cambio * equiv)%>,<%=JSNum(oItem.precioItem.Precio3 / oOferta.cambio * equiv)%>,<%=JSNum(oItem.precioItem.cantidad)%>,<%=oItem.precioItem.NumAdjuntos%>,
                    <%=JSNum(oItem.minprecio / oOferta.cambio * equiv)%>,<%if oProceso.verProveedor then%>'<%=JSText(oItem.MinProveCod)%>',   '<%=JSText(oItem.MinProveDen)%>'<%else%>null,null<%end if%>,<%=JSNum(oItem.objetivo/ oOferta.cambio * equivObj)%>,
                    "<%=JSText(oItem.precioItem.comentario1)%>","<%=JSText(oItem.precioItem.comentario2)%>","<%=JSText(oItem.precioItem.comentario3)%>","<%=JSText(oItem.precioItem.ObsAdjuntos)%>",atribsOfer,
                    <%=JSNum(oItem.precioItem.PrecioGS / oOferta.cambio * equiv)%>,<%=oItem.EspAdjI%>, <%=JSNum(oItem.PrecioItem.PrecioNeto)%>, <%=JSNum(oItem.PrecioItem.ImporteBruto / oOferta.cambio * equiv)%>, 
                    <%=JSNum(oItem.PrecioItem.ImporteNeto / oOferta.cambio * equiv)%>, <%=JSNum(oItem.usar)  %>,escalados,<%=JSNum(oItem.AnyoImputacion)  %>)
        <%end if%>
        }
    else{
        var item=oObj.items[<%=cont-1%>];   

        <%if oProceso.MonCod = mon then%>            
            <%if not JSNum(oItem.Objetivo) = "null" then
                '''Objetivo: Si estas leyendo una Oferta en moneda del proceso. El cambio es el del proceso.
                l_Aux = oItem.Objetivo
                oItem.Objetivo = oItem.Objetivo / (equiv * oProceso.cambio)%>

                item.obj=item.obj/<%=JSNum(equiv / oProceso.cambio)%>
            <%end if%>
            
            dibujarItem(oObj,<%=oItem.id%>,"<%=JSText(oItem.ArticuloCod)%>","<%=JSText(oItem.Descr)%>",<%=JSNum(oItem.cantidad)%>,"<%=JSText(oItem.uniCod)%>","<%=JSText(oItem.uniDen)%>","<%=JSText(oItem.DestCod)%>",
                    <%if oProceso.DefDestino=3 or (oProceso.DefDestino=2 and oGrupo.DefDestino=false) then%>"<%=JSText(oDestinos.item(oItem.DestCod).den)%>",<%else%>null,<%end if%>"<%=JSText(oItem.PagCod)%>","<%=JSText(oItem.PagDen)%>",
                    <%=JSDate(oItem.FechaInicioSuministro)%>,<%=JSDate(oItem.FechaFinSuministro)%>,item.obj,<%=oItem.EspAdj%>,"<%=JSText(oItem.Esp)%>",item.precio,
                    item.precio2,item.precio3,<%=JSNum(oItem.precioItem.cantidad)%>,<%=oItem.precioItem.NumAdjuntos%>,
                    item.ofertaMinima,<%if oProceso.verProveedor then%>'<%=JSText(oItem.MinProveCod)%>','<%=JSText(oItem.MinProveDen)%>'<%else%>null,null<%end if%>,item.obj,
                    "<%=JSText(oItem.precioItem.comentario1)%>","<%=JSText(oItem.precioItem.comentario2)%>","<%=JSText(oItem.precioItem.comentario3)%>","<%=JSText(oItem.precioItem.ObsAdjuntos)%>",atribsOfer,
                    <%=JSNum(oItem.precioItem.PrecioGS)%>,<%=oItem.EspAdjI%>, item.precioNeto, item.importeBruto,item.importeNeto, <%=JSNum(oItem.usar)  %>,escalados,<%=JSNum(oItem.AnyoImputacion)  %>)
            <%if not JSNum(oItem.Objetivo) = "null" then
                '''Objetivo: Previamente lo has cambiado para q al hacer "xx / oferta.cambio * equivobj" te de lo mismo q gs. 
                '''Esto hay q deshacerlo, para q si cambias de moneda, la cifra de partida sea la "esperada" en la moneda "anterior". "Esperada" pq se supone q el cambio es uno y has usado otro.
                '''Ejemplo: bbdd: 10,6182662494868        Moneda: Libra           Cambio al crear proceso libra: 0.70633         Cambio actual Libra: 1.5 ---> El objetivo es 5 Libras. Aplicar un cambio de 1.5 no da 5, se ha aplicado 0.70633.             
                oItem.Objetivo = l_Aux%>

                item.obj=item.obj*<%=JSNum(equiv / oProceso.cambio)%>
            <%end if%>
        <%else%>
            dibujarItem(oObj,<%=oItem.id%>,"<%=JSText(oItem.ArticuloCod)%>","<%=JSText(oItem.Descr)%>",<%=JSNum(oItem.cantidad)%>,"<%=JSText(oItem.uniCod)%>","<%=JSText(oItem.uniDen)%>","<%=JSText(oItem.DestCod)%>",
                    <%if oProceso.DefDestino=3 or (oProceso.DefDestino=2 and oGrupo.DefDestino=false) then%>"<%=JSText(oDestinos.item(oItem.DestCod).den)%>",<%else%>null,<%end if%>"<%=JSText(oItem.PagCod)%>","<%=JSText(oItem.PagDen)%>",
                    <%=JSDate(oItem.FechaInicioSuministro)%>,<%=JSDate(oItem.FechaFinSuministro)%>,item.objetivo,<%=oItem.EspAdj%>,"<%=JSText(oItem.Esp)%>",item.precio,
                    item.precio2,item.precio3,<%=JSNum(oItem.precioItem.cantidad)%>,<%=oItem.precioItem.NumAdjuntos%>,
                    item.ofertaMinima,<%if oProceso.verProveedor then%>'<%=JSText(oItem.MinProveCod)%>','<%=JSText(oItem.MinProveDen)%>'<%else%>null,null<%end if%>,item.obj,
                    "<%=JSText(oItem.precioItem.comentario1)%>","<%=JSText(oItem.precioItem.comentario2)%>","<%=JSText(oItem.precioItem.comentario3)%>","<%=JSText(oItem.precioItem.ObsAdjuntos)%>",atribsOfer,
                    <%=JSNum(oItem.precioItem.PrecioGS)%>,<%=oItem.EspAdjI%>, item.precioNeto, item.importeBruto,item.importeNeto, <%=JSNum(oItem.usar)  %>,escalados,<%=JSNum(oItem.AnyoImputacion)  %>)
        <%end if%>
    }
  
    <%if not bSoloCargar then%>
	    if (fila=="filaImpar")
		    fila="filaPar"
	    else
		    fila="filaImpar"
	    j++

        for (i=0;i<p.proceso.grupos.length && p.proceso.grupos[i].cod != '<%=oGrupo.Codigo%>';i++){}

        vObj = p.proceso.grupos[i]
        for (i=0;i<vObj.items.length && vObj.items[i].id != '<%=oItem.id%>';i++){}
        vItem = vObj.items[i]

        <%if oProceso.Subasta  and not sobrecerrado then%>
            for (i=0;i<p.itemsPuja.length && p.itemsPuja[i].id != vItem.id;i++){}
            vItemPuja = p.itemsPuja[i]

            <%if not (oGrupo.CostesDescItem is nothing) then
                for each  oCosDes in oGrupo.CostesDescItem
                    with oCosDes                        
                            set oAtribOfe = oItem.PrecioItem.CostesDescOfertados
                            if .Ambito=3 then%>
                            for (i=0;i<vItemPuja.costesDescuentosOfertados.length && vItemPuja.costesDescuentosOfertados[i].paid != <%=.paid%>;i++){}

                            vValor = vItemPuja.costesDescuentosOfertados[i].valor
                            vImporte = vItemPuja.costesDescuentosOfertados[i].importeParcial
        
                            costedescvalor = new Atributo(<%=.id%>, <%=.paid%>, '<%=JSText(.cod)%>', '<%=JSText(.den)%>', <%=.intro%>, <%=.tipo%>,vValor<%
				 
                            response.write  "," + JSBool(.obligatorio)
                            response.write "," & JSNum(.valorMin) &  "," & JSNum(.valorMax) 
                            if .intro = 1 then
        	                    str=",["
	                            for each oValor in .Valores
    		                        if str<>",[" then
        			                    str = str & ","
            	                    end if
                                    if (.operacion = "+" or  .operacion = "-") then
			                            vValor = oValor.Valor / oProceso.Cambio * equiv
        		                    else
		        	                    vValor = oValor.Valor 
                                    end if
                                    str = str & JSNum(vValor)
                                next
                                response.write str & "],p.proceso,'" & JSText(.operacion) & "'," & JSNum(.aplicar) & ", null"
	                        else
		                        response.write ",null,p.proceso,'" & JSText(.operacion) & "',"  & JSNum(.aplicar) & ", null"
        	                end if
                            %>, vImporte)
    	                    vItem.anyaCosteDescuentoOfertado(costedescvalor)
                       <%end if
                    end with
                next
            end if
        else
            if not (oGrupo.CostesDescItem is nothing) then
                for each  oCosDes in oGrupo.CostesDescItem
                    with oCosDes
                            set oAtribOfe = oItem.PrecioItem.CostesDescOfertados
                            if .Ambito=3 then%>
                                costedescvalor = new Atributo(<%=.id%>, <%=.paid%>, '<%=JSText(.cod)%>', '<%=JSText(.den)%>', <%=.intro%>, <%=.tipo%>,<%
                                
                                if not oAtribOfe is nothing then
                                if not oAtribOfe.Item(cstr(.PAID)) is nothing then
                                    if (.operacion = "+" or  .operacion = "-") then
        	                            vValor = oAtribOfe.Item(cstr(.PAID)).valor / oOferta.cambio * equiv
                                        else
	                                    vValor = oAtribOfe.Item(cstr(.PAID)).valor 
                                        end if
                                        vImporte = oAtribOfe.Item(cstr(.PAID)).ImporteParcial
                                else
                                    vValor = null
                                    vImporte = null
                                end if
                            else
                                vValor = null
                                vImporte = null
                            end if
                            response.write  JSNum(vValor) 
                            response.write  "," + JSBool(.obligatorio)
                            response.write "," & JSNum(.valorMin) &  "," & JSNum(.valorMax) 
                            if .intro = 1 then
        	                    str=",["
	                            for each oValor in .Valores
    		                        if str<>",[" then
        			                    str = str & ","
            	                    end if
                                    if (.operacion = "+" or  .operacion = "-") then
			                            vValor = oValor.Valor / oProceso.Cambio * equiv
        		                    else
		        	                    vValor = oValor.Valor 
                                    end if
                                    str = str & JSNum(vValor)
                                next
                                response.write str & "],p.proceso,'" & JSText(.operacion) & "'," & JSNum(.aplicar) & ", null"
                                response.write ", " &  JSNum(vImporte) & ")"
	                        else
		                        response.write ",null,p.proceso,'" & JSText(.operacion) & "',"  & JSNum(.aplicar) & ", null"
                                response.write ", " &  JSNum(vImporte) & ")"
        	                end if
               			
				            if oGrupo.hayEscalados then
					            if oItem.PrecioItem.CostesDescOfertados is nothing then					
						            for each oEscalado in oGrupo.escalados
						            %>
							            //Parametros de  Escalado(id, inicio,fin,precio,precioValido,valorNum,precioGS,precioOferta,cantidad, objetivo)
							            oEsc = new Escalado (<%=oEscalado.id%>,<%=oEscalado.inicial%>,<%=JSNUM(oEscalado.final)%>,null,null,null,null,null,<%if isnull(oItem.cantidad) or oEscalado.valorNum="" then response.write "null" else response.write oitem.cantidad%>,<%=JSNUM(oEscalado.objetivo)%>)
							            // Carga de los posibles escalados para los costes-descuentos
							            costedescvalor.anyaEscalado(oEsc)				
						            <%
						            next					
					            else
						            if not oItem.PrecioItem.CostesDescOfertados.item(cstr(ocosdes.PAID)) is nothing then
							            if oItem.PrecioItem.CostesDescOfertados.item(cstr(ocosdes.PAID)).hayEscalados then					
								            for each oEscalado in oItem.PrecioItem.CostesDescOfertados.item(cstr(ocosdes.PAID)).escalados
								            %>
									            //Parametros de  Escalado(id, inicio, fin, precio,precioValido,valorNum,precioGS,precioOferta,cantidad)							
									            oEsc = new Escalado (<%=oEscalado.id%>,<%=oEscalado.inicial%>,<%=JSNUM(oEscalado.final)%>,null,null,<%if isnull(oEscalado.valorNum) or oEscalado.valorNum= ""  then response.write "null" else response.write oEscalado.valorNum%>,null,null,<%if isnull(oItem.cantidad) then response.write "null" else response.write oitem.cantidad%>,<%=JSNUM(oEscalado.objetivo)%>)
									            //Carga de los posibles escalados para los costes-descuentos
									            costedescvalor.anyaEscalado(oEsc)				
								            <%
								            next
							            end if
						            end if
					            end if
				            end if%>		

                	        vItem.anyaCosteDescuentoOfertado(costedescvalor)
                        <%end if
                    end with
                next
            end if
        end if
    end if
next%>

oObj.itemsCargados=true;

<%if not bSoloCargar then
    for i = oGrupo.items.count + 1 to 4%>
	    dibujarFilaVacia()
	    lMaxFilas++
    <%next%>

    oTD = p.document.getElementById("tblDatosItem" + vGrp.cod).rows[0].insertCell(0)
    strDetDatosItem = "			<div style='width:" + p.wCSeparacion + "px;height:" + (p.hCFijo*(lMaxFilas)) + "px;overflow:hidden;'><a href='javascript:void(null);' onfocus='return OcultarDivCostesDescItem()' onclick='omDatosItem(\"" + vGrp.cod + "\")'><IMG name=imgDatosItem_" + vGrp.cod + " style='position:absolute;top:4px;left:4px;' border=0 SRC=images/<%=Idioma%>/datosItem.GIF></a></div>"
    oTD.className="cabecera"
    oTD.style.verticalAlign="top"
    oTD.rowSpan = lMaxFilas  
    if (oObj.hayEscalados)oTD.rowSpan = lMaxFilas  * oObj.escalados.length
    oTD.innerHTML += strDetDatosItem

    <%if oProceso.Subasta and not sobrecerrado and (not sinpujas) and ((oProceso.VerPrecioPuja) or (oproceso.VerProveedor)) then%>
	    oTD = p.document.getElementById("tblSubasta" + vGrp.cod).rows[0].insertCell(0)
	    strDetSubasta = "			<div style='width:" + p.wCSeparacion + "px;height:" + (p.hCFijo*(lMaxFilas)) + "px;overflow:hidden;'><a href='javascript:void(null);' onfocus='return OcultarDivCostesDescItem()' onclick='omSubasta(\"" + vGrp.cod + "\")'><IMG name=imgSubasta_" + vGrp.cod + " style='position:absolute;top:4px;left:4px;' border=0 SRC=images/<%=Idioma%>/subasta.GIF></a></div>"
	    oTD.className="cabecera"
	    oTD.style.verticalAlign="top"
	    oTD.rowSpan = lMaxFilas 
	    if (oObj.hayEscalados)oTD.rowSpan = lMaxFilas  * oObj.escalados.length
        oTD.innerHTML += strDetSubasta
    <%end if%>

    oTD = p.document.getElementById("tblOferta" + vGrp.cod).rows[0].insertCell(0)
    strDetOferta = "			<div style='width:" + p.wCSeparacion + "px;height:" + (p.hCFijo*(lMaxFilas) ) + "px;overflow:hidden;'><a href='javascript:void(null);' onfocus='return OcultarDivCostesDescItem()' onclick='omPrecios(\"" + vGrp.cod + "\")'><IMG name=imgOferta_" + vGrp.cod + " style='position:absolute;top:4px;left:4px;' border=0 SRC=images/<%=Idioma%>/oferta.GIF></a></div>"
    oTD.className="cabecera"
    oTD.style.verticalAlign="top"
    oTD.rowSpan = lMaxFilas 
    if (oObj.hayEscalados)oTD.rowSpan = lMaxFilas  * oObj.escalados.length
    oTD.innerHTML += strDetOferta

    <%if oProceso.pedirAlterPrecios then%>
	    oTD = p.document.getElementById("tblMasPrecios" + vGrp.cod).rows[0].insertCell(0)
	    strDetMasPrecios = "			<div style='width:" + p.wCSeparacion + "px;height:" + (p.hCFijo*(lMaxFilas) ) + "px;overflow:hidden;'><a href='javascript:void(null);' onfocus='return OcultarDivCostesDescItem()' onclick='omMasPrecios(\"" + vGrp.cod + "\")'><IMG name=imgMasPrecios_" + vGrp.cod + " style='position:absolute;top:4px;left:4px;' border=0 SRC=images/<%=Idioma%>/masPrecios.GIF></a></div>"
	    oTD.className="cabecera"
	    oTD.style.verticalAlign="top"
	    oTD.rowSpan = lMaxFilas 
	    if (oObj.hayEscalados)oTD.rowSpan = lMaxFilas  * oObj.escalados.length
        oTD.innerHTML += strDetMasPrecios
    <%end if%>

    <%if not oGrupo.AtributosItem is nothing then%>
        if (p.document.getElementById("divAtributos" + vGrp.cod )){
	        oTD = p.document.getElementById("tblAtributos" + vGrp.cod).rows[0].insertCell(0)
	        strDetAtributos = "			<div style='width:" + p.wCSeparacion + "px;height:" + (p.hCFijo*(lMaxFilas)) + "px;overflow:hidden;'><a href='javascript:void(null);' onfocus='return OcultarDivCostesDescItem()' onclick='omAtributos(\"" + vGrp.cod + "\")'><IMG name=imgAtributos_" + vGrp.cod + " style='position:absolute;top:4px;left:4px;' border=0 SRC=images/<%=Idioma%>/atributos.GIF></a></div>"
	        oTD.className="cabecera"
	        oTD.style.verticalAlign="top"
	        oTD.rowSpan = lMaxFilas 
	        if (oObj.hayEscalados)oTD.rowSpan = lMaxFilas  * oObj.escalados.length
            oTD.innerHTML += strDetAtributos
	    }
    <%end if%>
    
    oObj.itemsDibujados=true;

    p.ocultaArbol()
    //p.recalcularTotales()
    p.progreso (100)
    p.desbloquear()
<%end if%>

    </script>

    <%
set oProceso = nothing
set oObjeto = nothing
set oRaiz = nothing

server.ScriptTimeout =oldTimeOut
    %>
    <p>&nbsp;</p>

</body>
<!--#include file="../../common/fsal_2.asp"-->
</html>
