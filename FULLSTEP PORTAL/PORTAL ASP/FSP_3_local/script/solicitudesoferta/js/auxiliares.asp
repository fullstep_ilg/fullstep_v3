﻿<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/formatos.asp"-->
<%
''' <summary>
''' Crea una serie de funciones javascript q son usadas por varias panatallas
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cumpoferta.asp    solicitudesoferta\cargarresultadopuja.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")
set oRaiz=validarUsuario(Idioma,true,false,0)

%>

function Moneda(cod, den, equiv)
{
this.cod = cod
this.den = den
this.equiv = equiv
this.index = ""
}

function cargarMonedas()
{
var oMonedas
oMonedas = new Array()
<%
ciacomp = oRaiz.Sesion.CiaComp
set oMonedas = oraiz.generar_Cmonedas
oMonedas.cargartodaslasmonedasdesde Idioma,ciacomp,1000
dim i
i = 0
for each oMoneda in oMonedas%>
oMonedas[<%=i%>]=new Moneda()
oMonedas[<%=i%>].cod = "<%=JSText(oMoneda.cod)%>"
oMonedas[<%=i%>].den = "<%=JSText(oMoneda.den)%>"
oMonedas[<%=i%>].equiv = <%=JSNum (oMoneda.equiv)%>
<%
i = i + 1
next


set oraiz = nothing

%>

return (oMonedas)
}


function Destino(cod,den,sintrans,dir,pob,cp,provi,pai)
{

this.cod = cod
this.den = den
this.sintrans =sintrans
this.dir = dir
this.pob = pob
this.cp = cp
this.provi = provi
this.pai = pai
}

/*
''' <summary>
''' Muetra el detalle de un destino
''' </summary>
''' <param name="cod">Destino</param>    
''' <remarks>Llamada desde: js\cargaritems.asp ; Tiempo máximo: 0</remarks>*/
function detalleDestino(cod)
{
window.open("<%=application("RUTASEGURA")%>script/common/destino.asp?cod=" + cod, "_blank","width=300,height=210,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no")

}
