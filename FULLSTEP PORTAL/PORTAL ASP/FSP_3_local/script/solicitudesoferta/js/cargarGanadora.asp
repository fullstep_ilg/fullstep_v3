﻿<%@ Language=VBScript %>
<%
oldTimeOut = server.ScriptTimeout
server.ScriptTimeout =40000
%>

<!--#include file="../../common/acceso.asp"-->
<!--#include file="../../common/idioma.asp"-->
<!--#include file="../../common/colores.asp"-->
<!--#include file="../../common/formatos.asp"-->
<!--#include file="../../common/fsal_1.asp"-->

<HTML>
    <HEAD>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
        <script language="JavaScript" src="../../common/ajax.js"></script>
        <script src="../../common/formatos.js"></script>
        <script SRC="item.asp"></script>
        <link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
        <!--#include file="../../common/fsal_3.asp"-->
    </HEAD>
    <script>
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
        function init() 
        {
            if (<%=Application("FSAL")%> == 1)
            {
                Ajax_FSALActualizar3(); //registro de acceso
            }
            var p
            p = parent.frames["fraOfertaClient"]
            p.window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/syncclock.asp", "fraOfertaPujas")
        }
    </script>
    <BODY onload="init()" onclick="return window_onclick()">

<%
''' <summary>
''' Cargar los puja ganadora de la oferta
''' </summary>
''' <remarks>Llamada desde: js\proceso.asp ; Tiempo máximo: 0,2</remarks>


    Idioma = Request.Cookies("USU_IDIOMA")

    if Idioma="" then
	    Idioma = "SPA"
    end if

    dim den
    den = devolvertextos(Idioma,128)
    set oRaiz=validarUsuario(Idioma,true,false,0)
    

    decimalfmt = Request.Cookies ("USU_DECIMALFMT")
    CiaComp = clng(oRaiz.Sesion.CiaComp)
    anyo = request("anyo")
    gmn1 = request("gmn1")
    proce = request("proce")
    mon = request("mon")
    equiv = Numero(replace(request("eq"),".",decimalfmt))
    prove = oRaiz.Sesion.CiaCodGs
    ofe = request("ofe")


    decimalfmt = Request.Cookies ("USU_DECIMALFMT")
    thousanfmt = Request.Cookies ("USU_THOUSANFMT") 
    precisionfmt = CINT(Request.Cookies ("USU_PRECISIONFMT") )
    datefmt = Request.Cookies ("USU_DATEFMT") 

%>

    <script>
        var vdecimalfmt 
        vdecimalfmt ="<%=decimalfmt%>"
        var vthousanfmt 
        vthousanfmt ="<%=thousanfmt%>"
        var vprecisionfmt
        vprecisionfmt = <%=precisionfmt%>
        var vdatefmt
        vdatefmt = "<%=datefmt%>"

        var p
        p = parent.frames["fraOfertaClient"]
        proc = p.proceso
        var i

<%
    Set oProceso = oRaiz.Generar_CProceso
    oProceso.cargarConfiguracion CiaComp, anyo, gmn1, proce, prove
    oProceso.cargarProceso CiaComp, Idioma

    prove = oProceso.ProveGanador(CiaComp,true)
    'oProceso.ProveGanador en caso de existir una oferta previa devuelve un string con el proveedor y la oferta separadas por la cadena "||" 
    'y en caso contrario devuelve string vacio
    if prove = "" then 
        'No hay oferta ganadora
        %>
            alert("<%=JSText(den(3))%>\n<%=JSText(den(4))%>")
            </script>
        <%
        response.end
    end if
    ofe = split(prove,"||")(1)
    prove = split(prove,"||")(0)

    Set oProceso = oRaiz.Generar_CProceso
    oProceso.cargarConfiguracion CiaComp, anyo, gmn1, proce, prove
    oProceso.cargarProceso CiaComp, Idioma

    set oOferta = oProceso.CargarOfertaProveedor(Idioma,ciacomp,prove,ofe)


    dim cambioProceso
    cambioProceso = oProceso.cambio

    dim cambioOferta
    cambioOferta = oOferta.cambio

    dim bConsulta
    if iif(oProceso.MaximoNumeroOfertas<>0,oProceso.MaximoNumeroOfertas<> oOferta.TotEnviadas,true) then
	    bConsulta = false
    else
	    bConsulta = true
    end if
    oProceso.CargarCostesDescuentos CiaComp
    oOferta.CargarAtributosOfertados CiaComp, mon
    oProceso.cargarItems CiaComp,mon,prove,ofe,Idioma
    oProceso.oferta.cargarPrecios CiaComp
    set	oOferta.OfertaGrupos = oRaiz.Generar_COfertaGrupos
    for each oGrupo in oProceso.Grupos
	    set oOfeGrupo= oOferta.OfertaGrupos.Add (oOferta,oGrupo)
	    oOfeGrupo.CargarAtributosOfertados CiaComp, mon
    next
    oProceso.CargarCostesDescuentos CiaComp
    for each oGrupo in oProceso.grupos
        oGrupo.cargarGrupo Idioma,ciaComp,, anyo, gmn1, proce,prove,ofe,mon,oProceso.SolicitarCantMax
        oGrupo.cargarItems Idioma,CiaComp,mon,prove,ofe,false
        oGrupo.cargarAtributos CiaComp,3
        set oOfeGrupo = oRaiz.Generar_COfertaGrupo()
        set oOfeGrupo.Oferta = oOferta
        set oOfeGrupo.Grupo = oGrupo
        oOfeGrupo.cargarPrecios ciacomp,mon
    next
    oOferta.cargarOfertaGanadora Idioma,CiaComp
%>


function contador(grupo)
{
    // devuelve e incrementa el nº de grupos
    var j
    for (j=0;j<proc.grupos.length && proc.grupos[j].cod!=grupo;j++)
	{	
	}
    proc.grupos[j].cont++
    return (proc.grupos[j].cont)
}

var x
fila='filaImpar'
j=0


/*
''' <summary>
''' funcion que dibuja cada item de la subasta
''' </summary>
''' <param name="vGrupo">Grupo del proceso</param>
''' <param name="vIDItem">Id del item</param>  
''' <param name="vArt">Articulo</param>  
''' <param name="vDescr">Descripcion del articulo</param>  
''' <param name="vCant">Cantidad del item</param>
''' <param name="vUni">Unidad del articulo</param>
''' <param name="vUniDen">Denominacion de la unidad del articulo</param>
''' <param name="vPrec">Precio</param>                
''' <param name="vMin">Valor minimo</param>    
''' <param name="vPrec">Precio</param>
''' <param name="vProve">Proveedor</param>                
''' <param name="vProveDen">Denominacion del proveedor</param>                
''' <param name="vFilaS">Estilo de la fila</param>
''' <param name="vPosicionPuja">Posicion de la puja</param>                                                                
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cargaritems,cargarsubasta; Tiempo máximo:0</remarks>
*/ 

function dibujarItem(vGrupo, vIDItem, vArt, vDescr, vCant, vUni, vUniDen, vPrec, vMin, vProve, vProveDen, vFilaS,vPosicionPuja,importeNeto,precioNeto)
{
    // Los items ya existen y la tabla ya está dibujada, así que deberán reemplazarse las propiedades de los objetos que ya existen

    var vprecisionfmt = 1000
    // no restringimos al precisión para los valores de la mejor oferta

	oItem = new p.Item (vGrupo, vIDItem,vArt,vDescr,vCant,new p.Unidad(vUni,vUniDen),null,null,null,null,null,null,null,null,vPrec,null,null,null,null,vMin,vProve,vProveDen)	
    p.document.getElementById("txtPujaIDItem_" + vGrupo + "_" + contador(vGrupo)).value = vIDItem
    p.document.getElementById("txtPujaCantidad_" + vGrupo + "_" + oItem.id).value = num2str(oItem.cant,vdecimalfmt,vthousanfmt,vprecisionfmt)
	sNamePU = "txtPujaPU_" + vIDItem
    sNamePUBruto = "txtPujaPUBruto_" + vIDItem	
	sNamePUNormal = "txtPU_" + vGrupo +  "_" + vIDItem	
	var l
	var k
	for (l=0;l<p.proceso.grupos.length && p.proceso.grupos[l].cod!=vGrupo;l++)
		{}
	for (k=0;k<p.proceso.grupos[l].items.length && p.proceso.grupos[l].items[k].id != oItem.id;k++)
		{}
	if (p.document.forms["frmOfertaAEnviar"].item(sNamePUNormal))
	{
		oItem.precio=p.proceso.grupos[l].items[k].precio
		oItem.mirror = new Array("proceso.grupos[" + l + "].items[" + k + "]")
		arrLength = p.proceso.grupos[l].items[k].mirror.length
		p.proceso.grupos[l].items[k].mirror[arrLength] = "itemsPuja[" + p.itemsPuja.length + "]"
	}
	arrLength = oItem.mirror.length
	oItem.mirror[arrLength] = "itemsPuja[" + p.itemsPuja.length + "]"
    precioUnitarioInicial = p.document.getElementById(sNamePUBruto).value
    p.document.getElementById(sNamePU).value = num2str(precioNeto,vdecimalfmt,vthousanfmt,100)
    p.document.getElementById(sNamePUBruto).value= num2str(oItem.precio,vdecimalfmt, vthousanfmt,vprecisionfmt) 
    oItemSecundario = oItem

    for (i=0;i<p.proceso.grupos.length && p.proceso.grupos[i].cod != vGrupo;i++)
	    {
	    }
    p.vGr = p.proceso.grupos[i]
    for (iItem=0;iItem<p.itemsPuja.length && p.itemsPuja[iItem].id != oItem.id;iItem++)
	{
	}
    if (p.vGr.costesDescuentosItems.length)
    {
        p.MostrarDivCostesDescItem(vIDItem, vGrupo, "txtPujaPU_")    
        var nombreinput = "txtPujaPUI_" + vIDItem
        //p.itemsPuja[vIDItem-1].precio = vPrec
        p.itemsPuja[iItem].precio = vPrec
        p.document.getElementById(nombreinput).value =  num2str(vPrec,vdecimalfmt, vthousanfmt, vprecisionfmt)        
        var objinput = p.document.getElementById(nombreinput)
        p.linkRecalcularItem("txtPujaPU_")
        p.proceso.estado = 4
        p.CerrarDivCostesDescItem(vIDItem, vGrupo, "txtPujaPU_")

    }
    else
    {
        var nombreinput = "txtPujaPU_"  + vIDItem
        //p.itemsPuja[vIDItem-1].precio = vPrec
        p.itemsPuja[iItem].precio = vPrec
        p.document.getElementById(nombreinput).value =  num2str(vPrec,vdecimalfmt, vthousanfmt, vprecisionfmt)        
        var objinput = p.document.getElementById(nombreinput)
        p.validarNumeroB("txtPujaPU_" + vGrupo + "_"  + vIDItem, p.itemsPuja[vIDItem-1], objinput,vdecimalfmt, vthousanfmt,vprecisionfmt)        
        p.proceso.estado = 4

    }

}

var costesdesc
costesdesc = false
var variosprove
variosprove = false 

<%
if oProceso.hayCostesDescuentos then

    'Costes-Descuentos de proceso
    if not oOferta.AtribsOfertados is nothing then
        for each cd in oOferta.AtribsOfertados
            if cd.tag then
            %>
            for (i=0;i<p.proceso.costesDescuentos.length -1 && p.proceso.costesDescuentos[i].paid != '<%=cd.atributo.PAID%>';i++)
		        {
		        }
            //CostesDescuentos de proceso
            p.proceso.costesDescuentos[i].valor =p.obtenerValorCambioMoneda('<%=cd.atributo.operacion %>', <%=JSNum(cd.valor)%>,<%=JSNum(equiv / (cambioOferta * cambioProceso)) %>)            
            costesdesc = true
            <%    
            else
            %>
            variosprove = true
            <%
            end if
        next
    end if

    'Costes-Descuentos de grupo
    if not oOferta.ofertaGrupos is nothing then
        for each gr in oOferta.ofertaGrupos
            %>
            var idGrupo
            for (idGrupo=0;idGrupo<p.proceso.grupos.length -1 && p.proceso.grupos[idGrupo].cod != '<%=gr.grupo.codigo%>';idGrupo++)
		        {
		        }
            <%
            if not gr.AtribsOfertados is nothing then
                for each cd in gr.AtribsOfertados
                    if cd.tag then
                    %>
                    for (i=0;i<p.proceso.grupos[idGrupo].costesDescuentos.length -1 && p.proceso.grupos[idGrupo].costesDescuentos[i].paid != '<%=cd.atributo.PAID%>';i++)
		                {
		                }
                    //CostesDescuentos de grupo
                    p.proceso.grupos[idGrupo].costesDescuentos[i].valor =p.obtenerValorCambioMoneda('<%=cd.atributo.operacion %>', <%=JSNum(cd.valor)%>,<%=JSNum(equiv / (cambioOferta * cambioProceso)) %>)
                    costesdesc = true
                    <%    
                    else
                    %>
                    variosprove = true
                    <%
                    end if
                next
            end if
        next
    end if
end if

 'Costes / Descuentos de Items
for each oGrupo in oProceso.grupos

    %>
        for (i=0;i<p.proceso.grupos.length && p.proceso.grupos[i].cod != '<%=oGrupo.Codigo%>';i++)
		{
		}
	    oObj = p.proceso.grupos[i]
        var contadorcostesdesc
        contadorcostesdesc= 0
    <%
    if not (oGrupo.CostesDescItem is nothing) then 
        for each oCosDes in oGrupo.CostesDescItem
            with oCosDes%>
	        costedesc = new p.Atributo(<%=.id%>, <%=.paid%>, '<%=JSText(.cod)%>', '<%=JSText(.den)%>', <%=.intro%>, <%=.tipo%>, <%=JSNum(.valor / (cambioOferta * cambioProceso) * equiv)%>, <%=JSBool(.obligatorio)%>, <%=JSNum(.valorMin)%>, <%=JSNum(.valorMax)%>, null, p.proceso,'<%=JSText(.operacion)%>',<%=.aplicar %>,<%=.ambito %>,null,null,'<%=JSText(.Alcance)%>')
            oObj.costesDescuentosItems[contadorcostesdesc] = costedesc
            contadorcostesdesc++
            <%end with
        next
    end if 
    %>
    // ponemos el contador a 0 para cada grupo
    var x
    for (x=0;x<proc.grupos.length && proc.grupos[x].cod!='<%=oGrupo.codigo%>';x++)
	{
	}
    proc.grupos[x].cont =0
    <%
    for each oItem in oGrupo.items
        %>
        for (i=0;i<p.itemsPuja.length && p.itemsPuja[i].id != '<%=oItem.id%>';i++)
        {
        }
        vItem = p.itemsPuja[i]
	    filaS = ""
        var contadorcd = 0

	    <% 

        if oProceso.haycostesdescuentos then

        'JVS carga de los valores de los costes / descuentos de ámbito item
             if not (oGrupo.CostesDescItem is nothing) then
                for each  oCosDes in oGrupo.CostesDescItem
                with oCosDes
                     set oAtribOfe = oItem.PrecioItem.CostesDescOfertados
                    if .Ambito=3 then%>
                        costesdesc = true
                         costedescvalor = new p.Atributo(<%=.id%>, <%=.paid%>, '<%=JSText(.cod)%>', '<%=JSText(.den)%>', <%=.intro%>, <%=.tipo%>,<%
                        if not oAtribOfe is nothing then
                            if not oAtribOfe.Item(cstr(.PAID)) is nothing then
                                if (.operacion = "+" or  .operacion = "-") then
        	                        vValor = oAtribOfe.Item(cstr(.PAID)).valor / (cambioOferta * cambioProceso) * equiv
                                else
	                                vValor = oAtribOfe.Item(cstr(.PAID)).valor 
                                end if
                                'if oAtribOfe.Item(cstr(.PAID)).ImporteParcial is nothing then
                                    vImporte = 0
                                'else
                                '    vImporte = oAtribOfe.Item(cstr(.PAID)).ImporteParcial
                                'end if
                            else
                                vValor = null
                                vImporte = null
                            end if
                        else
                            vValor = null
                            vImporte = null
                        end if
                        response.write  JSNum(vValor) 
                        response.write  "," + JSBool(.obligatorio)
                        response.write "," & JSNum(.valorMin) &  "," & JSNum(.valorMax) 
                        if .intro = 1 then
        	                str=",["
	                        for each oValor in .Valores
    		                    if str<>",[" then
        			                str = str & ","
            	                end if
                                if (.operacion = "+" or  .operacion = "-") then
			                        vValor = oValor.Valor / (cambioOferta * cambioProceso) * equiv
        		                else
		        	                vValor = oValor.Valor 
                                end if
                                str = str & JSNum(vValor)
                            next
                            response.write str & "],p.proceso,'" & JSText(.operacion) & "'," & JSNum(.aplicar) & ", null"
                            response.write ", " &  JSNum(vImporte) & ")"
	                    else
    		                response.write ",null,p.proceso,'" & JSText(.operacion) & "',"  & JSNum(.aplicar) & ", null"
                            response.write ", " &  JSNum(vImporte) & ")"
            	        end if
                        %>
                        vItem.costesDescuentosOfertados[contadorcd] = costedescvalor
                        contadorcd++
                        <%
                    end if
                end with
                next
            end if
        end if



        %>
        dibujarItem("<%=oItem.grupo.codigo%>", <%=oItem.id%>,"<%=JSText(oItem.ArticuloCod)%>", "<%=JSText(oItem.Descr)%>", <%=JSNum(oItem.cantidad)%>,"<%=JSText(oItem.uniCod)%>", "<%=JSText(oItem.uniDen)%>", <%=JSNum(oItem.precioItem.PrecioOferta / (cambioOferta * cambioProceso) * equiv)%>, <%=JSNum(oItem.minprecio / (cambioOferta * cambioProceso) * equiv)%>, <%if oProceso.verProveedor then%>"<%=JSText(oItem.MinProveCod)%>","<%=JSText(oItem.MinProveDen)%>"<%else%>null,null<%end if%>, filaS,<%=JSNum(oItem.PosicionPuja)%>, <%=JSNum(oItem.PrecioItem.ImporteNeto / (cambioOferta * cambioProceso) * equiv)%>, <%=JSNum(oItem.PrecioItem.PrecioNeto)%>)
        <%
    next
next

%>


if (costesdesc)
{
    alert("<%=JSText(den(1))%>")
}

if (variosprove)
{
    alert("<%=JSText(den(2))%>")
}

function window_onclick() 
{
}
</script>
<%

set oProceso = nothing
set oRaiz = nothing

server.ScriptTimeout =oldTimeOut

%>

<P>&nbsp;</P>

</BODY>
<!--#include file="../../common/fsal_2.asp"-->
</HTML>
